<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc4d1	malé
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
je	být	k5eAaImIp3nS	být
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
jeho	jeho	k3xOp3gInSc7	jeho
tvarem	tvar	k1gInSc7	tvar
<g/>
,	,	kIx,	,
nejjasnější	jasný	k2eAgFnPc1d3	nejjasnější
hvězdy	hvězda	k1gFnPc1	hvězda
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
tvoří	tvořit	k5eAaImIp3nP	tvořit
nepříliš	příliš	k6eNd1	příliš
dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
půlkruh	půlkruh	k1gInSc4	půlkruh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
severním	severní	k2eAgInSc7d1	severní
protějškem	protějšek	k1gInSc7	protějšek
Jižní	jižní	k2eAgFnSc2d1	jižní
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
48	[number]	k4	48
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
Almagest	Almagest	k1gMnSc1	Almagest
řecký	řecký	k2eAgMnSc1d1	řecký
astronom	astronom	k1gMnSc1	astronom
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
88	[number]	k4	88
moderních	moderní	k2eAgNnPc2d1	moderní
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
Severní	severní	k2eAgFnSc1d1	severní
koruna	koruna	k1gFnSc1	koruna
čelenkou	čelenka	k1gFnSc7	čelenka
či	či	k8xC	či
královskou	královský	k2eAgFnSc7d1	královská
korunou	koruna	k1gFnSc7	koruna
Ariadny	Ariadna	k1gFnSc2	Ariadna
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
řeckého	řecký	k2eAgMnSc2d1	řecký
krále	král	k1gMnSc2	král
Minóa	Minóus	k1gMnSc2	Minóus
a	a	k8xC	a
manželky	manželka	k1gFnSc2	manželka
boha	bůh	k1gMnSc4	bůh
Dionýsa	Dionýsos	k1gMnSc4	Dionýsos
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
čelenku	čelenka	k1gFnSc4	čelenka
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
vyhodil	vyhodit	k5eAaPmAgMnS	vyhodit
na	na	k7c4	na
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
žádná	žádný	k3yNgFnSc1	žádný
jiná	jiný	k2eAgFnSc1d1	jiná
žena	žena	k1gFnSc1	žena
nemohla	moct	k5eNaImAgFnS	moct
nosit	nosit	k5eAaImF	nosit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
kulturách	kultura	k1gFnPc6	kultura
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
zpodobňuje	zpodobňovat	k5eAaImIp3nS	zpodobňovat
orlí	orlí	k2eAgNnSc4d1	orlí
hnízdo	hnízdo	k1gNnSc4	hnízdo
či	či	k8xC	či
medvědí	medvědí	k2eAgNnSc4d1	medvědí
doupě	doupě	k1gNnSc4	doupě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejjasnější	jasný	k2eAgFnSc7d3	nejjasnější
hvězdou	hvězda	k1gFnSc7	hvězda
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
je	být	k5eAaImIp3nS	být
Gemma	Gemma	k1gFnSc1	Gemma
(	(	kIx(	(
<g/>
α	α	k?	α
CrB	CrB	k1gMnSc1	CrB
<g/>
)	)	kIx)	)
o	o	k7c6	o
hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
velikosti	velikost	k1gFnSc6	velikost
2,2	[number]	k4	2,2
<g/>
m.	m.	k?	m.
Žlutá	žlutat	k5eAaImIp3nS	žlutat
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
R	R	kA	R
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc2	Borealis
je	být	k5eAaImIp3nS	být
veleobrem	veleobr	k1gMnSc7	veleobr
6	[number]	k4	6
<g/>
.	.	kIx.	.
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
jasnost	jasnost	k1gFnSc1	jasnost
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
v	v	k7c6	v
nepravidelných	pravidelný	k2eNgInPc6d1	nepravidelný
intervalech	interval	k1gInPc6	interval
až	až	k9	až
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
vyvrhuje	vyvrhovat	k5eAaImIp3nS	vyvrhovat
obrovská	obrovský	k2eAgFnSc1d1	obrovská
mračna	mračna	k1gFnSc1	mračna
plynů	plyn	k1gInPc2	plyn
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
uhlík	uhlík	k1gInSc4	uhlík
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
zastiňují	zastiňovat	k5eAaImIp3nP	zastiňovat
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
pokles	pokles	k1gInSc4	pokles
jasnosti	jasnost	k1gFnSc2	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chudá	chudý	k2eAgFnSc1d1	chudá
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
termonukleární	termonukleární	k2eAgFnSc4d1	termonukleární
fúze	fúze	k1gFnSc1	fúze
z	z	k7c2	z
helia	helium	k1gNnSc2	helium
na	na	k7c4	na
uhlík	uhlík	k1gInSc4	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
T	T	kA	T
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
neobvyklé	obvyklý	k2eNgFnPc4d1	neobvyklá
proměnné	proměnný	k2eAgFnPc4d1	proměnná
hvězdy	hvězda	k1gFnPc4	hvězda
známé	známý	k2eAgFnPc4d1	známá
opakujícím	opakující	k2eAgInSc7d1	opakující
se	s	k7c7	s
zjasněním	zjasnění	k1gNnSc7	zjasnění
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
10	[number]	k4	10
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
posledním	poslední	k2eAgNnSc6d1	poslední
zjasnění	zjasnění	k1gNnSc6	zjasnění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byla	být	k5eAaImAgFnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
těsný	těsný	k2eAgInSc4d1	těsný
dvojhvězdný	dvojhvězdný	k2eAgInSc4d1	dvojhvězdný
systém	systém	k1gInSc4	systém
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
červeného	červený	k2eAgMnSc2d1	červený
obra	obr	k1gMnSc2	obr
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
M3	M3	k1gFnSc4	M3
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
přibližně	přibližně	k6eAd1	přibližně
0,7	[number]	k4	0,7
hmoty	hmota	k1gFnSc2	hmota
našeho	náš	k3xOp1gNnSc2	náš
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
bílého	bílý	k2eAgMnSc4d1	bílý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
přibližně	přibližně	k6eAd1	přibližně
1,35	[number]	k4	1,35
<g/>
násobku	násobek	k1gInSc2	násobek
sluneční	sluneční	k2eAgFnSc2d1	sluneční
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
ADS	ADS	kA	ADS
9731	[number]	k4	9731
a	a	k8xC	a
Sigma	sigma	k1gNnSc1	sigma
Coronae	Corona	k1gInSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
(	(	kIx(	(
<g/>
σ	σ	k?	σ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
hvězdné	hvězdný	k2eAgInPc1d1	hvězdný
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
šesti	šest	k4xCc2	šest
a	a	k8xC	a
pěti	pět	k4xCc2	pět
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
objektů	objekt	k1gInPc2	objekt
hlubokého	hluboký	k2eAgInSc2d1	hluboký
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
vysoce	vysoce	k6eAd1	vysoce
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
kupa	kupa	k1gFnSc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
Abell	Abell	k1gInSc4	Abell
2065	[number]	k4	2065
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
jedné	jeden	k4xCgFnSc2	jeden
miliardy	miliarda	k4xCgFnSc2	miliarda
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
od	od	k7c2	od
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
součástí	součást	k1gFnSc7	součást
nadkupy	nadkupa	k1gFnSc2	nadkupa
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Severní	severní	k2eAgFnSc2d1	severní
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
179	[number]	k4	179
čtverečných	čtverečný	k2eAgInPc2d1	čtverečný
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
0,433	[number]	k4	0,433
procenta	procento	k1gNnSc2	procento
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
88	[number]	k4	88
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
na	na	k7c4	na
73	[number]	k4	73
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
západě	západ	k1gInSc6	západ
má	mít	k5eAaImIp3nS	mít
hranici	hranice	k1gFnSc4	hranice
se	s	k7c7	s
souhvězdím	souhvězdí	k1gNnSc7	souhvězdí
Pastýře	pastýř	k1gMnSc2	pastýř
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlava	hlava	k1gFnSc1	hlava
Hada	had	k1gMnSc2	had
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Herkula	Herkul	k1gMnSc2	Herkul
<g/>
.	.	kIx.	.
</s>
<s>
Třípísmená	Třípísmený	k2eAgFnSc1d1	Třípísmený
zkratka	zkratka	k1gFnSc1	zkratka
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
přijatém	přijatý	k2eAgNnSc6d1	přijaté
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
astronomickou	astronomický	k2eAgFnSc7d1	astronomická
unií	unie	k1gFnSc7	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
CrB	CrB	k1gFnSc1	CrB
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
hranice	hranice	k1gFnPc1	hranice
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
vyznačil	vyznačit	k5eAaPmAgMnS	vyznačit
Eugè	Eugè	k1gFnSc4	Eugè
Delporte	Delport	k1gInSc5	Delport
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vymezeny	vymezit	k5eAaPmNgFnP	vymezit
polygonem	polygon	k1gInSc7	polygon
osmi	osm	k4xCc2	osm
segmentů	segment	k1gInPc2	segment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovníkových	rovníkový	k2eAgFnPc6d1	Rovníková
souřadnicích	souřadnice	k1gFnPc6	souřadnice
je	být	k5eAaImIp3nS	být
rektascenze	rektascenze	k1gFnSc1	rektascenze
mezi	mezi	k7c7	mezi
15	[number]	k4	15
h	h	k?	h
16,0	[number]	k4	16,0
m	m	kA	m
a	a	k8xC	a
16	[number]	k4	16
h	h	k?	h
25,1	[number]	k4	25,1
m	m	kA	m
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
deklinaci	deklinace	k1gFnSc4	deklinace
popisují	popisovat	k5eAaImIp3nP	popisovat
souřadnice	souřadnice	k1gFnPc4	souřadnice
mezi	mezi	k7c7	mezi
31,71	[number]	k4	31,71
<g/>
°	°	k?	°
a	a	k8xC	a
25,54	[number]	k4	25,54
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
nebeské	nebeský	k2eAgFnSc6d1	nebeská
polokouli	polokoule	k1gFnSc6	polokoule
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celé	celý	k2eAgNnSc1d1	celé
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
je	být	k5eAaImIp3nS	být
viditelné	viditelný	k2eAgNnSc1d1	viditelné
pro	pro	k7c4	pro
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
severně	severně	k6eAd1	severně
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
protějšek	protějšek	k1gInSc4	protějšek
-	-	kIx~	-
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
Jižní	jižní	k2eAgFnSc2d1	jižní
koruny	koruna	k1gFnSc2	koruna
-	-	kIx~	-
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
nebeské	nebeský	k2eAgFnSc6d1	nebeská
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nápadně	nápadně	k6eAd1	nápadně
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Severní	severní	k2eAgFnSc2d1	severní
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
objekty	objekt	k1gInPc1	objekt
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hvězdy	hvězda	k1gFnPc1	hvězda
===	===	k?	===
</s>
</p>
<p>
<s>
Tvar	tvar	k1gInSc1	tvar
koruny	koruna	k1gFnSc2	koruna
tvoří	tvořit	k5eAaImIp3nS	tvořit
sedm	sedm	k4xCc1	sedm
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
od	od	k7c2	od
západu	západ	k1gInSc2	západ
popořadě	popořadě	k6eAd1	popořadě
théta	théta	k1gNnSc1	théta
<g/>
,	,	kIx,	,
beta	beta	k1gNnSc1	beta
<g/>
,	,	kIx,	,
alfa	alfa	k1gNnSc1	alfa
(	(	kIx(	(
<g/>
Gemma	Gemma	k1gNnSc1	Gemma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gama	gama	k1gNnSc1	gama
<g/>
,	,	kIx,	,
delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
epsilon	epsilon	k1gNnSc1	epsilon
a	a	k8xC	a
ióta	ióta	k1gFnSc1	ióta
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
nejjasnější	jasný	k2eAgFnSc2d3	nejjasnější
hvězdy	hvězda	k1gFnSc2	hvězda
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Gemmy	Gemma	k1gFnSc2	Gemma
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hvězdy	hvězda	k1gFnPc1	hvězda
4	[number]	k4	4
<g/>
.	.	kIx.	.
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
kartograf	kartograf	k1gMnSc1	kartograf
Johann	Johann	k1gMnSc1	Johann
Bayer	Bayer	k1gMnSc1	Bayer
dal	dát	k5eAaPmAgMnS	dát
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
hvězdném	hvězdný	k2eAgInSc6d1	hvězdný
atlasu	atlas	k1gInSc6	atlas
Uranometria	Uranometrium	k1gNnSc2	Uranometrium
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
označení	označení	k1gNnSc2	označení
celkem	celkem	k6eAd1	celkem
dvaceti	dvacet	k4xCc3	dvacet
hvězdám	hvězda	k1gFnPc3	hvězda
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
<g/>
,	,	kIx,	,
řeckými	řecký	k2eAgNnPc7d1	řecké
písmeny	písmeno	k1gNnPc7	písmeno
od	od	k7c2	od
alfy	alfa	k1gFnSc2	alfa
po	po	k7c6	po
ypsilon	ypsilon	k1gNnSc6	ypsilon
<g/>
.	.	kIx.	.
</s>
<s>
Složky	složka	k1gFnPc1	složka
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
Zéta	Zéta	k1gFnSc1	Zéta
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
(	(	kIx(	(
<g/>
ζ	ζ	k?	ζ
CrB	CrB	k1gFnSc2	CrB
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
zeta1	zeta1	k4	zeta1
a	a	k8xC	a
zeta2	zeta2	k4	zeta2
<g/>
,	,	kIx,	,
a	a	k8xC	a
John	John	k1gMnSc1	John
Flamsteed	Flamsteed	k1gMnSc1	Flamsteed
dále	daleko	k6eAd2	daleko
označil	označit	k5eAaPmAgMnS	označit
hvězdy	hvězda	k1gFnSc2	hvězda
ný1	ný1	k4	ný1
a	a	k8xC	a
ný2	ný2	k4	ný2
jako	jako	k8xS	jako
20	[number]	k4	20
a	a	k8xC	a
21	[number]	k4	21
Coronae	Coronae	k1gNnPc2	Coronae
Borealis	Borealis	k1gFnPc2	Borealis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vymezené	vymezený	k2eAgFnSc2d1	vymezená
hranicemi	hranice	k1gFnPc7	hranice
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
je	být	k5eAaImIp3nS	být
37	[number]	k4	37
hvězd	hvězda	k1gFnPc2	hvězda
jasnějších	jasný	k2eAgFnPc2d2	jasnější
než	než	k8xS	než
nebo	nebo	k8xC	nebo
rovnající	rovnající	k2eAgFnSc3d1	rovnající
se	se	k3xPyFc4	se
zdánlivé	zdánlivý	k2eAgFnSc3d1	zdánlivá
hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
velikosti	velikost	k1gFnSc3	velikost
6,5	[number]	k4	6,5
<g/>
m.	m.	k?	m.
<g/>
Jméno	jméno	k1gNnSc4	jméno
nejjasnější	jasný	k2eAgFnSc2d3	nejjasnější
hvězdy	hvězda	k1gFnSc2	hvězda
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Gemmy	Gemma	k1gFnSc2	Gemma
(	(	kIx(	(
<g/>
α	α	k?	α
Crb	Crb	k1gFnSc1	Crb
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
drahokam	drahokam	k1gInSc1	drahokam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Arabové	Arab	k1gMnPc1	Arab
tuto	tento	k3xDgFnSc4	tento
hvězdu	hvězda	k1gFnSc4	hvězda
nazývali	nazývat	k5eAaImAgMnP	nazývat
Alphekka	Alphekek	k1gMnSc4	Alphekek
<g/>
.	.	kIx.	.
</s>
<s>
Gemma	Gemma	k1gFnSc1	Gemma
je	být	k5eAaImIp3nS	být
modrobílá	modrobílý	k2eAgFnSc1d1	modrobílá
hvězda	hvězda	k1gFnSc1	hvězda
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
2,2	[number]	k4	2,2
<g/>
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
zákrytovou	zákrytový	k2eAgFnSc7d1	zákrytová
proměnnou	proměnný	k2eAgFnSc7d1	proměnná
hvězdou	hvězda	k1gFnSc7	hvězda
typu	typ	k1gInSc2	typ
Algol	Algol	k1gInSc1	Algol
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
jasnost	jasnost	k1gFnSc1	jasnost
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
o	o	k7c4	o
0,1	[number]	k4	0,1
magnitudy	magnitud	k1gInPc4	magnitud
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
17,4	[number]	k4	17,4
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgFnSc1d1	primární
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
A	a	k9	a
<g/>
0	[number]	k4	0
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
2,91	[number]	k4	2,91
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
57	[number]	k4	57
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
prachoplynovým	prachoplynův	k2eAgInSc7d1	prachoplynův
diskem	disk	k1gInSc7	disk
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
60	[number]	k4	60
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
průvodce	průvodce	k1gMnSc1	průvodce
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
našemu	náš	k3xOp1gNnSc3	náš
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
žlutým	žlutý	k2eAgMnSc7d1	žlutý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
G	G	kA	G
<g/>
5	[number]	k4	5
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
0,9	[number]	k4	0,9
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
75,0	[number]	k4	75,0
<g/>
±	±	k?	±
<g/>
0,5	[number]	k4	0,5
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Gemma	Gemma	k1gFnSc1	Gemma
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
společný	společný	k2eAgInSc4d1	společný
původ	původ	k1gInSc4	původ
s	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
z	z	k7c2	z
obrazce	obrazec	k1gInSc2	obrazec
Velké	velký	k2eAgFnSc2d1	velká
Medvědice	medvědice	k1gFnSc2	medvědice
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
podobné	podobný	k2eAgNnSc1d1	podobné
stáří	stáří	k1gNnSc1	stáří
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
vesmírem	vesmír	k1gInSc7	vesmír
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
<g/>
Beta	beta	k1gNnSc1	beta
Coronae	Corona	k1gInSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
(	(	kIx(	(
<g/>
β	β	k?	β
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
Nusakan	Nusakan	k1gInSc1	Nusakan
je	být	k5eAaImIp3nS	být
spektroskopická	spektroskopický	k2eAgFnSc1d1	spektroskopická
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
112	[number]	k4	112
<g/>
±	±	k?	±
<g/>
3	[number]	k4	3
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
dvě	dva	k4xCgFnPc1	dva
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdáleny	vzdáleno	k1gNnPc7	vzdáleno
10	[number]	k4	10
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
oběhnou	oběhnout	k5eAaPmIp3nP	oběhnout
kolem	kolem	k7c2	kolem
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
10,5	[number]	k4	10,5
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jasnější	jasný	k2eAgFnSc1d2	jasnější
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
rychle	rychle	k6eAd1	rychle
pulzující	pulzující	k2eAgFnSc1d1	pulzující
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
16,2	[number]	k4	16,2
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
A5V	A5V	k1gFnSc2	A5V
s	s	k7c7	s
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
teplotou	teplota	k1gFnSc7	teplota
kolem	kolem	k7c2	kolem
7980	[number]	k4	7980
K	K	kA	K
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
2,09	[number]	k4	2,09
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
2,63	[number]	k4	2,63
slunečního	sluneční	k2eAgInSc2d1	sluneční
poloměru	poloměr	k1gInSc2	poloměr
a	a	k8xC	a
25,3	[number]	k4	25,3
svítivosti	svítivost	k1gFnSc2	svítivost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
hvězda	hvězda	k1gFnSc1	hvězda
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
F2V	F2V	k1gFnSc2	F2V
má	mít	k5eAaImIp3nS	mít
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
teplotu	teplota	k1gFnSc4	teplota
6750	[number]	k4	6750
K	K	kA	K
<g/>
,	,	kIx,	,
1,4	[number]	k4	1,4
hmotnost	hmotnost	k1gFnSc1	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
1,56	[number]	k4	1,56
slunečního	sluneční	k2eAgInSc2d1	sluneční
poloměru	poloměr	k1gInSc2	poloměr
a	a	k8xC	a
svítivost	svítivost	k1gFnSc4	svítivost
mezi	mezi	k7c7	mezi
4	[number]	k4	4
a	a	k8xC	a
5	[number]	k4	5
Slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Nusakanu	Nusakan	k1gInSc2	Nusakan
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Theta	Theta	k1gFnSc1	Theta
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
(	(	kIx(	(
<g/>
θ	θ	k?	θ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvojhvězdný	dvojhvězdný	k2eAgInSc1d1	dvojhvězdný
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
obě	dva	k4xCgFnPc1	dva
složky	složka	k1gFnPc1	složka
dohromady	dohromady	k6eAd1	dohromady
mají	mít	k5eAaImIp3nP	mít
svítivost	svítivost	k1gFnSc4	svítivost
4,13	[number]	k4	4,13
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
380	[number]	k4	380
<g/>
±	±	k?	±
<g/>
20	[number]	k4	20
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jasnější	jasný	k2eAgFnSc1d2	jasnější
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
Theta	Theta	k1gFnSc1	Theta
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
A	a	k9	a
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
modrobílá	modrobílý	k2eAgFnSc1d1	modrobílá
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
rychlostí	rychlost	k1gFnSc7	rychlost
rotace	rotace	k1gFnSc2	rotace
<g/>
,	,	kIx,	,
393	[number]	k4	393
km	km	kA	km
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Slabší	slabý	k2eAgFnSc1d2	slabší
složka	složka	k1gFnSc1	složka
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
prachoplynovým	prachoplynův	k2eAgInSc7d1	prachoplynův
diskem	disk	k1gInSc7	disk
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Gemmy	Gemma	k1gFnSc2	Gemma
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Gamma	Gamma	k1gFnSc1	Gamma
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
(	(	kIx(	(
<g/>
γ	γ	k?	γ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
složky	složka	k1gFnPc1	složka
oběhnou	oběhnout	k5eAaPmIp3nP	oběhnout
kolem	kolem	k7c2	kolem
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
92,94	[number]	k4	92,94
roku	rok	k1gInSc2	rok
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k8xS	jako
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Jasnější	jasný	k2eAgFnSc1d2	jasnější
složka	složka	k1gFnSc1	složka
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
proměnnou	proměnný	k2eAgFnSc4d1	proměnná
hvězdu	hvězda	k1gFnSc4	hvězda
typu	typ	k1gInSc2	typ
delta	delta	k1gFnSc1	delta
Scuti	Scuť	k1gFnSc2	Scuť
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nepanuje	panovat	k5eNaImIp3nS	panovat
mezi	mezi	k7c4	mezi
astronomy	astronom	k1gMnPc4	astronom
shoda	shoda	k1gFnSc1	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
hvězdami	hvězda	k1gFnPc7	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
spektrálních	spektrální	k2eAgInPc2d1	spektrální
typů	typ	k1gInPc2	typ
B9V	B9V	k1gFnSc2	B9V
a	a	k8xC	a
A	a	k9	a
<g/>
3	[number]	k4	3
<g/>
V.	V.	kA	V.
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
170	[number]	k4	170
<g/>
±	±	k?	±
<g/>
2	[number]	k4	2
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Delta	delta	k1gFnSc1	delta
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
(	(	kIx(	(
<g/>
δ	δ	k?	δ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
4,06	[number]	k4	4,06
<g/>
m	m	kA	m
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
žlutým	žlutý	k2eAgMnSc7d1	žlutý
obrem	obr	k1gMnSc7	obr
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
G	G	kA	G
<g/>
3.5	[number]	k4	3.5
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
2,4	[number]	k4	2,4
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
poloměr	poloměr	k1gInSc1	poloměr
7,4	[number]	k4	7,4
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
5180	[number]	k4	5180
K.	K.	kA	K.
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
byla	být	k5eAaImAgFnS	být
Delta	delta	k1gFnSc1	delta
Coronae	Corona	k1gFnSc2	Corona
Borealis	Borealis	k1gFnPc2	Borealis
modrobílou	modrobílý	k2eAgFnSc7d1	modrobílá
hvězdou	hvězda	k1gFnSc7	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
B	B	kA	B
<g/>
,	,	kIx,	,
než	než	k8xS	než
jí	on	k3xPp3gFnSc3	on
došel	dojít	k5eAaPmAgInS	dojít
vodík	vodík	k1gInSc4	vodík
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
jádru	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
světelnost	světelnost	k1gFnSc4	světelnost
a	a	k8xC	a
spektrum	spektrum	k1gNnSc4	spektrum
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
mění	měnit	k5eAaImIp3nS	měnit
svojí	svůj	k3xOyFgFnSc7	svůj
polohu	poloha	k1gFnSc4	poloha
na	na	k7c6	na
Hertzsprungově	Hertzsprungův	k2eAgInSc6d1	Hertzsprungův
diagramu	diagram	k1gInSc6	diagram
<g/>
,	,	kIx,	,
že	že	k8xS	že
skončila	skončit	k5eAaPmAgFnS	skončit
fúze	fúze	k1gFnSc1	fúze
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
jádru	jádro	k1gNnSc6	jádro
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
spalování	spalování	k1gNnSc1	spalování
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
<g/>
Hvězda	hvězda	k1gFnSc1	hvězda
Zéta	Zéta	k1gFnSc1	Zéta
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
(	(	kIx(	(
<g/>
ζ	ζ	k?	ζ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
modrobílými	modrobílý	k2eAgFnPc7d1	modrobílá
složkami	složka	k1gFnPc7	složka
vzdálenými	vzdálený	k2eAgFnPc7d1	vzdálená
od	od	k7c2	od
sebe	se	k3xPyFc2	se
6,3	[number]	k4	6,3
vteřiny	vteřina	k1gFnPc4	vteřina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
rozlišit	rozlišit	k5eAaPmF	rozlišit
již	již	k6eAd1	již
při	při	k7c6	při
100	[number]	k4	100
<g/>
násobném	násobný	k2eAgNnSc6d1	násobné
zvětšení	zvětšení	k1gNnSc6	zvětšení
v	v	k7c6	v
dalekohledu	dalekohled	k1gInSc6	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Jasnější	jasný	k2eAgFnSc1d2	jasnější
složka	složka	k1gFnSc1	složka
má	mít	k5eAaImIp3nS	mít
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
5,1	[number]	k4	5,1
<g/>
m	m	kA	m
a	a	k8xC	a
slabší	slabý	k2eAgFnSc1d2	slabší
složka	složka	k1gFnSc1	složka
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
6,0	[number]	k4	6,0
<g/>
m.	m.	k?	m.
Hvězda	hvězda	k1gFnSc1	hvězda
Ný	Ný	k1gFnSc1	Ný
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
(	(	kIx(	(
<g/>
ν	ν	k?	ν
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
optická	optický	k2eAgFnSc1d1	optická
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podobné	podobný	k2eAgFnSc6d1	podobná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc1d1	různý
radiální	radiální	k2eAgFnSc7d1	radiální
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
oddělené	oddělený	k2eAgFnPc4d1	oddělená
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jasnější	jasný	k2eAgFnSc1d2	jasnější
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
ný1	ný1	k4	ný1
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgMnSc1d1	červený
obr	obr	k1gMnSc1	obr
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
M2III	M2III	k1gFnSc2	M2III
a	a	k8xC	a
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
5,2	[number]	k4	5,2
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
640	[number]	k4	640
<g/>
±	±	k?	±
<g/>
30	[number]	k4	30
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
slabší	slabý	k2eAgFnSc1d2	slabší
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
ný2	ný2	k4	ný2
je	být	k5eAaImIp3nS	být
oranžově	oranžově	k6eAd1	oranžově
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
obr	obr	k1gMnSc1	obr
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
K5III	K5III	k1gFnSc2	K5III
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
5,4	[number]	k4	5,4
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
590	[number]	k4	590
<g/>
±	±	k?	±
<g/>
30	[number]	k4	30
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Flamsteed	Flamsteed	k1gMnSc1	Flamsteed
označil	označit	k5eAaPmAgMnS	označit
hvězdy	hvězda	k1gFnPc1	hvězda
ný1	ný1	k4	ný1
a	a	k8xC	a
ný2	ný2	k4	ný2
čísly	číslo	k1gNnPc7	číslo
20	[number]	k4	20
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Sigma	sigma	k1gNnSc1	sigma
Coronae	Corona	k1gInSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
(	(	kIx(	(
<g/>
σ	σ	k?	σ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
systém	systém	k1gInSc1	systém
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
více	hodně	k6eAd2	hodně
hvězd	hvězda	k1gFnPc2	hvězda
rozlišitelných	rozlišitelný	k2eAgFnPc2d1	rozlišitelná
již	již	k6eAd1	již
malými	malý	k2eAgInPc7d1	malý
amatérskými	amatérský	k2eAgInPc7d1	amatérský
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgInSc1d1	primární
systém	systém	k1gInSc1	systém
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvě	dva	k4xCgFnPc4	dva
hvězdy	hvězda	k1gFnPc4	hvězda
hmotnější	hmotný	k2eAgFnSc2d2	hmotnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
okolo	okolo	k7c2	okolo
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
1,14	[number]	k4	1,14
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
hvězda	hvězda	k1gFnSc1	hvězda
podobná	podobný	k2eAgFnSc1d1	podobná
Slunci	slunce	k1gNnSc3	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
primární	primární	k2eAgInSc4d1	primární
systém	systém	k1gInSc4	systém
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
726	[number]	k4	726
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
hvězdy	hvězda	k1gFnPc1	hvězda
obíhají	obíhat	k5eAaImIp3nP	obíhat
ještě	ještě	k9	ještě
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
a	a	k8xC	a
pátá	pátý	k4xOgFnSc1	pátý
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
červení	červený	k2eAgMnPc1d1	červený
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
14	[number]	k4	14
tisíc	tisíc	k4xCgInPc2	tisíc
AU	au	k0	au
vzdálení	vzdálení	k1gNnPc1	vzdálení
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
tří	tři	k4xCgFnPc2	tři
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
spektroskopickou	spektroskopický	k2eAgFnSc7d1	spektroskopická
dvojhvězdou	dvojhvězda	k1gFnSc7	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
ADS	ADS	kA	ADS
9731	[number]	k4	9731
je	být	k5eAaImIp3nS	být
vícenásobný	vícenásobný	k2eAgInSc4d1	vícenásobný
systém	systém	k1gInSc4	systém
složený	složený	k2eAgInSc4d1	složený
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
dvě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
spektroskopické	spektroskopický	k2eAgFnPc4d1	spektroskopická
dvojhvězdy	dvojhvězda	k1gFnPc4	dvojhvězda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
koruně	koruna	k1gFnSc6	koruna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgFnPc4	dva
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
proměnné	proměnný	k2eAgFnPc4d1	proměnná
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
T	T	kA	T
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
doposud	doposud	k6eAd1	doposud
pozorovaných	pozorovaný	k2eAgFnPc2d1	pozorovaná
rekurentních	rekurentní	k2eAgFnPc2d1	rekurentní
nov	nova	k1gFnPc2	nova
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
RS	RS	kA	RS
Ophiuchi	Ophiuchi	k1gNnSc1	Ophiuchi
<g/>
,	,	kIx,	,
T	T	kA	T
Pyxidis	Pyxidis	k1gFnSc4	Pyxidis
<g/>
,	,	kIx,	,
V1017	V1017	k1gFnSc4	V1017
Sagittarii	Sagittarie	k1gFnSc4	Sagittarie
a	a	k8xC	a
U	u	k7c2	u
Scorpii	Scorpie	k1gFnSc4	Scorpie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
je	být	k5eAaImIp3nS	být
hvězdou	hvězda	k1gFnSc7	hvězda
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
kolem	kolem	k7c2	kolem
10	[number]	k4	10
<g/>
.	.	kIx.	.
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
,	,	kIx,	,
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
minimální	minimální	k2eAgFnSc4d1	minimální
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
10,2	[number]	k4	10,2
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
okolo	okolo	k7c2	okolo
9,9	[number]	k4	9,9
<g/>
m.	m.	k?	m.
Jednou	jednou	k9	jednou
za	za	k7c4	za
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
zjasní	zjasnit	k5eAaPmIp3nS	zjasnit
na	na	k7c4	na
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
2	[number]	k4	2
<g/>
.	.	kIx.	.
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
zjasnění	zjasnění	k1gNnSc1	zjasnění
T	T	kA	T
Coronae	Corona	k1gMnSc2	Corona
Borealis	Borealis	k1gFnSc4	Borealis
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
<g/>
;	;	kIx,	;
její	její	k3xOp3gInSc1	její
druhý	druhý	k4xOgInSc1	druhý
výbuch	výbuch	k1gInSc1	výbuch
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
T	T	kA	T
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
těsná	těsný	k2eAgFnSc1d1	těsná
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
,	,	kIx,	,
jasnější	jasný	k2eAgFnSc1d2	jasnější
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgMnSc1d1	červený
obr	obr	k1gMnSc1	obr
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
M3	M3	k1gFnSc2	M3
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
asi	asi	k9	asi
0,7	[number]	k4	0,7
hmoty	hmota	k1gFnSc2	hmota
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Slabší	slabý	k2eAgFnSc1d2	slabší
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
1,35	[number]	k4	1,35
<g/>
násobku	násobek	k1gInSc2	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnSc1	jejich
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
červeného	červený	k2eAgMnSc2d1	červený
obra	obr	k1gMnSc2	obr
na	na	k7c4	na
bílého	bílý	k2eAgMnSc4d1	bílý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
akrečního	akreční	k2eAgInSc2d1	akreční
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
překročení	překročení	k1gNnSc6	překročení
kritické	kritický	k2eAgFnSc2d1	kritická
hranice	hranice	k1gFnSc2	hranice
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
prudké	prudký	k2eAgFnSc3d1	prudká
termonukleární	termonukleární	k2eAgFnSc3d1	termonukleární
reakci	reakce	k1gFnSc3	reakce
a	a	k8xC	a
vnější	vnější	k2eAgFnPc4d1	vnější
vrstvy	vrstva	k1gFnPc4	vrstva
bílého	bílý	k1gMnSc2	bílý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
explodují	explodovat	k5eAaBmIp3nP	explodovat
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zjasnění	zjasnění	k1gNnSc3	zjasnění
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
k	k	k7c3	k
nově	nova	k1gFnSc3	nova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
R	R	kA	R
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
je	být	k5eAaImIp3nS	být
žlutě	žlutě	k6eAd1	žlutě
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
přes	přes	k7c4	přes
7000	[number]	k4	7000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
prototypem	prototyp	k1gInSc7	prototyp
třídy	třída	k1gFnSc2	třída
hvězd	hvězda	k1gFnPc2	hvězda
známých	známý	k2eAgFnPc2d1	známá
jako	jako	k9	jako
proměnné	proměnná	k1gFnPc1	proměnná
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
R	R	kA	R
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
chudá	chudý	k2eAgFnSc1d1	chudá
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
termonukleární	termonukleární	k2eAgFnSc4d1	termonukleární
fúze	fúze	k1gFnSc1	fúze
z	z	k7c2	z
helia	helium	k1gNnSc2	helium
na	na	k7c4	na
uhlík	uhlík	k1gInSc4	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Normálně	normálně	k6eAd1	normálně
má	mít	k5eAaImIp3nS	mít
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
6	[number]	k4	6
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
jasnost	jasnost	k1gFnSc1	jasnost
klesá	klesat	k5eAaImIp3nS	klesat
nepravidelně	pravidelně	k6eNd1	pravidelně
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
15	[number]	k4	15
<g/>
m	m	kA	m
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
následujících	následující	k2eAgInPc2d1	následující
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
poklesy	pokles	k1gInPc4	pokles
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
oblak	oblak	k1gInSc1	oblak
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
bohatý	bohatý	k2eAgMnSc1d1	bohatý
na	na	k7c4	na
uhlík	uhlík	k1gInSc4	uhlík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vyvrhován	vyvrhovat	k5eAaImNgInS	vyvrhovat
z	z	k7c2	z
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
se	se	k3xPyFc4	se
zahalí	zahalit	k5eAaPmIp3nS	zahalit
do	do	k7c2	do
prachového	prachový	k2eAgInSc2d1	prachový
závoje	závoj	k1gInSc2	závoj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
až	až	k8xS	až
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
temný	temný	k2eAgInSc1d1	temný
oblak	oblak	k1gInSc1	oblak
tlakem	tlak	k1gInSc7	tlak
záření	záření	k1gNnSc2	záření
rozptýlí	rozptýlit	k5eAaPmIp3nS	rozptýlit
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
vrátí	vrátit	k5eAaPmIp3nS	vrátit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
původní	původní	k2eAgFnSc3d1	původní
jasnosti	jasnost	k1gFnSc3	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
zobrazování	zobrazování	k1gNnSc1	zobrazování
pomocí	pomocí	k7c2	pomocí
Hubblova	Hubblův	k2eAgInSc2d1	Hubblův
kosmického	kosmický	k2eAgInSc2d1	kosmický
dalekohledu	dalekohled	k1gInSc2	dalekohled
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
oblaka	oblaka	k1gNnPc4	oblaka
prachu	prach	k1gInSc2	prach
okolo	okolo	k7c2	okolo
hvězdy	hvězda	k1gFnSc2	hvězda
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
2	[number]	k4	2
tisíce	tisíc	k4xCgInSc2	tisíc
AU	au	k0	au
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
další	další	k2eAgFnPc4d1	další
proměnné	proměnný	k2eAgFnPc4d1	proměnná
hvězdy	hvězda	k1gFnPc4	hvězda
vhodné	vhodný	k2eAgFnPc1d1	vhodná
k	k	k7c3	k
pozorování	pozorování	k1gNnSc1	pozorování
amatérskými	amatérský	k2eAgMnPc7d1	amatérský
astronomy	astronom	k1gMnPc7	astronom
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tří	tři	k4xCgFnPc2	tři
dlouhoperiodických	dlouhoperiodický	k2eAgFnPc2d1	dlouhoperiodická
proměnných	proměnný	k2eAgFnPc2d1	proměnná
hvězd	hvězda	k1gFnPc2	hvězda
typu	typ	k1gInSc2	typ
Mira	Mira	k1gFnSc1	Mira
<g/>
.	.	kIx.	.
</s>
<s>
Jasnost	jasnost	k1gFnSc1	jasnost
S	s	k7c7	s
Coronae	Coronae	k1gFnSc7	Coronae
Borealis	Borealis	k1gFnSc2	Borealis
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
velikostí	velikost	k1gFnSc7	velikost
5,8	[number]	k4	5,8
<g/>
m	m	kA	m
až	až	k9	až
14,1	[number]	k4	14,1
<g/>
m	m	kA	m
v	v	k7c6	v
periodě	perioda	k1gFnSc6	perioda
360	[number]	k4	360
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
okolo	okolo	k7c2	okolo
1946	[number]	k4	1946
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
září	září	k1gNnSc4	září
svítivostí	svítivost	k1gFnPc2	svítivost
16643	[number]	k4	16643
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
teplotu	teplota	k1gFnSc4	teplota
3033	[number]	k4	3033
K.	K.	kA	K.
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejčervenějších	červený	k2eAgFnPc2d3	nejčervenější
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Coronae	Coronae	k1gNnSc6	Coronae
Borealis	Borealis	k1gFnSc2	Borealis
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
astrofyziků	astrofyzik	k1gMnPc2	astrofyzik
McDonalda	McDonaldo	k1gNnSc2	McDonaldo
<g/>
,	,	kIx,	,
Ziljstry	Ziljstra	k1gFnSc2	Ziljstra
a	a	k8xC	a
Boyera	Boyera	k1gFnSc1	Boyera
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
teplotou	teplota	k1gFnSc7	teplota
2877	[number]	k4	2877
K	K	kA	K
<g/>
,	,	kIx,	,
se	s	k7c7	s
svítivostí	svítivost	k1gFnSc7	svítivost
102831	[number]	k4	102831
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
8810	[number]	k4	8810
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
jasnost	jasnost	k1gFnSc1	jasnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
6,9	[number]	k4	6,9
<g/>
m	m	kA	m
až	až	k9	až
12,6	[number]	k4	12,6
<g/>
m	m	kA	m
v	v	k7c6	v
periodě	perioda	k1gFnSc6	perioda
357	[number]	k4	357
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
souřadnice	souřadnice	k1gFnSc2	souřadnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
hranice	hranice	k1gFnPc4	hranice
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
Severní	severní	k2eAgFnSc2d1	severní
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
Herkula	Herkul	k1gMnSc2	Herkul
a	a	k8xC	a
Pastýře	pastýř	k1gMnSc2	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
W	W	kA	W
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
1,5	[number]	k4	1,5
<g/>
°	°	k?	°
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Tau	tau	k1gNnSc2	tau
Coronae	Corona	k1gFnSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
(	(	kIx(	(
<g/>
τ	τ	k?	τ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
jasnost	jasnost	k1gFnSc1	jasnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
velikostí	velikost	k1gFnSc7	velikost
7,8	[number]	k4	7,8
<g/>
m	m	kA	m
a	a	k8xC	a
14,3	[number]	k4	14,3
<g/>
m	m	kA	m
v	v	k7c6	v
periodě	perioda	k1gFnSc6	perioda
238	[number]	k4	238
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
červený	červený	k2eAgMnSc1d1	červený
obr	obr	k1gMnSc1	obr
<g/>
,	,	kIx,	,
RR	RR	kA	RR
Coronae	Coronae	k1gInSc4	Coronae
Borealis	Borealis	k1gFnSc7	Borealis
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
M	M	kA	M
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
polopravidelnou	polopravidelný	k2eAgFnSc7d1	polopravidelná
proměnnou	proměnný	k2eAgFnSc7d1	proměnná
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
jasnost	jasnost	k1gFnSc1	jasnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
7,3	[number]	k4	7,3
<g/>
m	m	kA	m
a	a	k8xC	a
8,2	[number]	k4	8,2
<g/>
m	m	kA	m
v	v	k7c6	v
období	období	k1gNnSc6	období
60,8	[number]	k4	60,8
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Coronae	Corona	k1gFnSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
mění	měnit	k5eAaImIp3nS	měnit
svojí	svojit	k5eAaImIp3nS	svojit
jasnost	jasnost	k1gFnSc1	jasnost
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
7,7	[number]	k4	7,7
<g/>
m	m	kA	m
až	až	k9	až
8,8	[number]	k4	8,8
<g/>
m	m	kA	m
během	během	k7c2	během
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zákrytovou	zákrytový	k2eAgFnSc4d1	zákrytová
proměnnou	proměnná	k1gFnSc4	proměnná
hvězdou	hvězda	k1gFnSc7	hvězda
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
3	[number]	k4	3
dny	den	k1gInPc4	den
a	a	k8xC	a
19	[number]	k4	19
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Exoplanety	Exoplanet	k1gInPc4	Exoplanet
===	===	k?	===
</s>
</p>
<p>
<s>
Extrasolární	Extrasolární	k2eAgFnPc1d1	Extrasolární
planety	planeta	k1gFnPc1	planeta
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
u	u	k7c2	u
pěti	pět	k4xCc2	pět
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc1	čtyři
planety	planeta	k1gFnPc1	planeta
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
metodou	metoda	k1gFnSc7	metoda
měření	měření	k1gNnSc2	měření
radiálních	radiální	k2eAgFnPc2d1	radiální
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Analýzou	analýza	k1gFnSc7	analýza
spektra	spektrum	k1gNnSc2	spektrum
Epsilon	epsilon	k1gNnSc2	epsilon
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
(	(	kIx(	(
<g/>
ε	ε	k?	ε
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
během	během	k7c2	během
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
měření	měření	k1gNnSc2	měření
v	v	k7c6	v
letech	let	k1gInPc6	let
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
identifikována	identifikován	k2eAgFnSc1d1	identifikována
planeta	planeta	k1gFnSc1	planeta
6,7	[number]	k4	6,7
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
hvězdu	hvězda	k1gFnSc4	hvězda
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
418	[number]	k4	418
dní	den	k1gInPc2	den
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
cca	cca	kA	cca
1,3	[number]	k4	1,3
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
Epsilon	epsilon	k1gNnSc1	epsilon
Coronae	Corona	k1gInSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc1	hmotnost
1,7	[number]	k4	1,7
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oranžovým	oranžový	k2eAgMnSc7d1	oranžový
obrem	obr	k1gMnSc7	obr
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
21	[number]	k4	21
Sluncí	slunce	k1gNnPc2	slunce
a	a	k8xC	a
svítivosti	svítivost	k1gFnSc2	svítivost
151	[number]	k4	151
Sluncí	slunce	k1gNnPc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kappa	kappa	k1gNnSc1	kappa
Coronae	Corona	k1gInSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
(	(	kIx(	(
<g/>
κ	κ	k?	κ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oranžový	oranžový	k2eAgMnSc1d1	oranžový
podobr	podobr	k1gMnSc1	podobr
hmotnosti	hmotnost	k1gFnSc2	hmotnost
dvou	dva	k4xCgInPc2	dva
Sluncí	slunce	k1gNnPc2	slunce
se	se	k3xPyFc4	se
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
K	k	k7c3	k
<g/>
1	[number]	k4	1
<g/>
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
ji	on	k3xPp3gFnSc4	on
jedna	jeden	k4xCgFnSc1	jeden
potvrzená	potvrzený	k2eAgFnSc1d1	potvrzená
planeta	planeta	k1gFnSc1	planeta
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
asi	asi	k9	asi
3,4	[number]	k4	3,4
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
2,5	[number]	k4	2,5
hmotností	hmotnost	k1gFnPc2	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměra	k1gFnPc1	rozměra
trosek	troska	k1gFnPc2	troska
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
velice	velice	k6eAd1	velice
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
obíhá	obíhat	k5eAaImIp3nS	obíhat
druhý	druhý	k4xOgMnSc1	druhý
plynný	plynný	k2eAgMnSc1d1	plynný
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
</s>
<s>
Omicron	Omicron	k1gInSc1	Omicron
Coronae	Corona	k1gFnSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
(	(	kIx(	(
<g/>
ο	ο	k?	ο
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oranžovým	oranžový	k2eAgMnSc7d1	oranžový
obrem	obr	k1gMnSc7	obr
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
K	K	kA	K
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
ji	on	k3xPp3gFnSc4	on
jedna	jeden	k4xCgFnSc1	jeden
potvrzená	potvrzený	k2eAgFnSc1d1	potvrzená
planeta	planeta	k1gFnSc1	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
0,83	[number]	k4	0,83
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
za	za	k7c4	za
187	[number]	k4	187
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
hmotných	hmotný	k2eAgMnPc2d1	hmotný
známých	známý	k2eAgMnPc2d1	známý
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
obíhajících	obíhající	k2eAgMnPc2d1	obíhající
kolem	kolem	k7c2	kolem
veleobra	veleobr	k1gMnSc2	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
HD	HD	kA	HD
145457	[number]	k4	145457
je	být	k5eAaImIp3nS	být
oranžovým	oranžový	k2eAgMnSc7d1	oranžový
obrem	obr	k1gMnSc7	obr
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
K0III	K0III	k1gFnSc2	K0III
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
jedna	jeden	k4xCgFnSc1	jeden
planeta	planeta	k1gFnSc1	planeta
s	s	k7c7	s
2,9	[number]	k4	2,9
hmotnosti	hmotnost	k1gFnSc6	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
Dopplerovou	Dopplerův	k2eAgFnSc7d1	Dopplerova
metodou	metoda	k1gFnSc7	metoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
hvězdu	hvězda	k1gFnSc4	hvězda
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
za	za	k7c4	za
176	[number]	k4	176
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
XO-1	XO-1	k1gFnSc2	XO-1
je	být	k5eAaImIp3nS	být
hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
velikosti	velikost	k1gFnSc3	velikost
11	[number]	k4	11
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
žlutým	žlutý	k2eAgMnSc7d1	žlutý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
560	[number]	k4	560
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
poloměr	poloměr	k1gInSc4	poloměr
jsou	být	k5eAaImIp3nP	být
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
se	s	k7c7	s
slunečními	sluneční	k2eAgFnPc7d1	sluneční
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
hvězdy	hvězda	k1gFnSc2	hvězda
zjištěn	zjištěn	k2eAgInSc4d1	zjištěn
horký	horký	k2eAgInSc4d1	horký
Jupiter	Jupiter	k1gInSc4	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Exoplanetu	Exoplanet	k1gInSc2	Exoplanet
XO-	XO-	k1gFnSc7	XO-
<g/>
1	[number]	k4	1
<g/>
b	b	k?	b
obíhající	obíhající	k2eAgMnSc1d1	obíhající
XO-1	XO-1	k1gMnSc1	XO-1
objevil	objevit	k5eAaPmAgMnS	objevit
tranzitní	tranzitní	k2eAgFnSc7d1	tranzitní
metodou	metoda	k1gFnSc7	metoda
dalekohled	dalekohled	k1gInSc1	dalekohled
XO	XO	kA	XO
na	na	k7c4	na
Haleakalā	Haleakalā	k1gFnSc4	Haleakalā
na	na	k7c6	na
Maui	Mau	k1gFnSc6	Mau
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
Jupitera	Jupiter	k1gMnSc2	Jupiter
a	a	k8xC	a
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
svou	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
..	..	k?	..
<g/>
Pomocí	pomocí	k7c2	pomocí
analýzy	analýza	k1gFnSc2	analýza
radiální	radiální	k2eAgFnSc2d1	radiální
rychlosti	rychlost	k1gFnSc2	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
Ró	ró	k1gNnSc2	ró
Coronae	Corona	k1gFnSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
(	(	kIx(	(
<g/>
ρ	ρ	k?	ρ
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
objevena	objeven	k2eAgFnSc1d1	objevena
planeta	planeta	k1gFnSc1	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
dvojnásobné	dvojnásobný	k2eAgFnSc2d1	dvojnásobná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
57	[number]	k4	57
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgFnSc1d2	přesnější
analýza	analýza	k1gFnSc1	analýza
dat	datum	k1gNnPc2	datum
ze	z	k7c2	z
satelitu	satelit	k1gInSc2	satelit
Hipparcos	Hipparcosa	k1gFnPc2	Hipparcosa
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
domnělý	domnělý	k2eAgMnSc1d1	domnělý
planetární	planetární	k2eAgMnSc1d1	planetární
průvodce	průvodce	k1gMnSc1	průvodce
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
nejspíše	nejspíše	k9	nejspíše
o	o	k7c4	o
slabou	slabý	k2eAgFnSc4d1	slabá
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Možné	možný	k2eAgFnPc1d1	možná
stabilní	stabilní	k2eAgFnPc1d1	stabilní
dráhy	dráha	k1gFnPc1	dráha
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
byly	být	k5eAaImAgFnP	být
vypočteny	vypočíst	k5eAaPmNgFnP	vypočíst
u	u	k7c2	u
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
Eta	Eta	k1gFnSc1	Eta
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
(	(	kIx(	(
<g/>
ε	ε	k?	ε
CrB	CrB	k1gFnSc1	CrB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
žlutých	žlutý	k2eAgFnPc2d1	žlutá
hvězd	hvězda	k1gFnPc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
hvězd	hvězda	k1gFnPc2	hvězda
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
G1V	G1V	k1gFnSc2	G1V
a	a	k8xC	a
G	G	kA	G
<g/>
3	[number]	k4	3
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
majících	mající	k2eAgFnPc6d1	mající
tedy	tedy	k8xC	tedy
podobnou	podobný	k2eAgFnSc4d1	podobná
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
spektrum	spektrum	k1gNnSc4	spektrum
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
planeta	planeta	k1gFnSc1	planeta
zde	zde	k6eAd1	zde
nebyla	být	k5eNaImAgFnS	být
nalezena	nalézt	k5eAaBmNgFnS	nalézt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgMnS	být
objeven	objeven	k2eAgMnSc1d1	objeven
hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
63	[number]	k4	63
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
L	L	kA	L
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
dvojici	dvojice	k1gFnSc4	dvojice
hvězd	hvězda	k1gFnPc2	hvězda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
3640	[number]	k4	3640
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Objekty	objekt	k1gInPc1	objekt
hlubokého	hluboký	k2eAgInSc2d1	hluboký
vesmíru	vesmír	k1gInSc2	vesmír
===	===	k?	===
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
koruna	koruna	k1gFnSc1	koruna
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
galaxií	galaxie	k1gFnPc2	galaxie
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
amatérským	amatérský	k2eAgInSc7d1	amatérský
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
NGC	NGC	kA	NGC
6085	[number]	k4	6085
a	a	k8xC	a
6086	[number]	k4	6086
jsou	být	k5eAaImIp3nP	být
slabé	slabý	k2eAgInPc4d1	slabý
spirální	spirální	k2eAgInPc4d1	spirální
respektive	respektive	k9	respektive
eliptické	eliptický	k2eAgFnSc2d1	eliptická
galaxie	galaxie	k1gFnSc2	galaxie
ležící	ležící	k2eAgInSc4d1	ležící
dostatečně	dostatečně	k6eAd1	dostatečně
blízko	blízko	k6eAd1	blízko
u	u	k7c2	u
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
zorném	zorný	k2eAgNnSc6d1	zorné
poli	pole	k1gNnSc6	pole
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Kupa	kupa	k1gFnSc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
Abell	Abella	k1gFnPc2	Abella
2142	[number]	k4	2142
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
(	(	kIx(	(
<g/>
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
šest	šest	k4xCc4	šest
milionů	milion	k4xCgInPc2	milion
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
zdroj	zdroj	k1gInSc1	zdroj
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výsledek	výsledek	k1gInSc4	výsledek
stále	stále	k6eAd1	stále
pokračující	pokračující	k2eAgFnPc1d1	pokračující
srážky	srážka	k1gFnPc1	srážka
dvou	dva	k4xCgFnPc2	dva
kup	kupa	k1gFnPc2	kupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Abell	Abell	k1gInSc1	Abell
2142	[number]	k4	2142
má	mít	k5eAaImIp3nS	mít
rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
0,0909	[number]	k4	0,0909
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
27	[number]	k4	27
250	[number]	k4	250
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
a	a	k8xC	a
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
16	[number]	k4	16
<g/>
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
vzdálena	vzdálit	k5eAaPmNgNnP	vzdálit
asi	asi	k9	asi
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
320	[number]	k4	320
Mpc	Mpc	k1gFnPc2	Mpc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
kupou	kupa	k1gFnSc7	kupa
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
je	být	k5eAaImIp3nS	být
RX	RX	kA	RX
J	J	kA	J
<g/>
1532.0	[number]	k4	1532.0
<g/>
+	+	kIx~	+
<g/>
3021	[number]	k4	3021
<g/>
,	,	kIx,	,
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
přibližně	přibližně	k6eAd1	přibližně
3,9	[number]	k4	3,9
miliardy	miliarda	k4xCgFnSc2	miliarda
světelných	světelný	k2eAgMnPc2d1	světelný
let	let	k1gInSc4	let
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
kupy	kupa	k1gFnSc2	kupa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velká	velký	k2eAgFnSc1d1	velká
eliptická	eliptický	k2eAgFnSc1d1	eliptická
galaxie	galaxie	k1gFnSc1	galaxie
obsahující	obsahující	k2eAgFnSc4d1	obsahující
obří	obří	k2eAgFnSc4d1	obří
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
<g/>
.	.	kIx.	.
</s>
<s>
Abell	Abell	k1gInSc1	Abell
2065	[number]	k4	2065
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
kupa	kupa	k1gFnSc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgMnSc1d1	čítající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejjasnější	jasný	k2eAgFnSc7d3	nejjasnější
má	mít	k5eAaImIp3nS	mít
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
16	[number]	k4	16
<g/>
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
miliardu	miliarda	k4xCgFnSc4	miliarda
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Abell	Abell	k1gInSc1	Abell
2065	[number]	k4	2065
dále	daleko	k6eAd2	daleko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Abell	Abella	k1gFnPc2	Abella
2061	[number]	k4	2061
<g/>
,	,	kIx,	,
Abell	Abell	k1gInSc1	Abell
2067	[number]	k4	2067
<g/>
,	,	kIx,	,
Abell	Abell	k1gInSc1	Abell
2079	[number]	k4	2079
<g/>
,	,	kIx,	,
Abell	Abell	k1gInSc1	Abell
2089	[number]	k4	2089
a	a	k8xC	a
Abell	Abell	k1gInSc1	Abell
2092	[number]	k4	2092
tvoří	tvořit	k5eAaImIp3nS	tvořit
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
rozsáhlejší	rozsáhlý	k2eAgInSc4d2	rozsáhlejší
Nadkupu	Nadkup	k1gInSc3	Nadkup
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
koruně	koruna	k1gFnSc6	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
kupa	kupa	k1gFnSc1	kupa
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
Abell	Abella	k1gFnPc2	Abella
2162	[number]	k4	2162
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
členem	člen	k1gMnSc7	člen
Nadkupy	Nadkupa	k1gFnSc2	Nadkupa
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
Herkulovi	Herkul	k1gMnSc6	Herkul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mytologie	mytologie	k1gFnSc2	mytologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
Severní	severní	k2eAgFnSc1d1	severní
koruna	koruna	k1gFnSc1	koruna
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
legendou	legenda	k1gFnSc7	legenda
o	o	k7c6	o
Theseovi	Theseus	k1gMnSc6	Theseus
a	a	k8xC	a
Minotaurovi	Minotaur	k1gMnSc6	Minotaur
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
dal	dát	k5eAaPmAgMnS	dát
Ariadně	Ariadna	k1gFnSc3	Ariadna
<g/>
,	,	kIx,	,
dceři	dcera	k1gFnSc3	dcera
krétského	krétský	k2eAgMnSc2d1	krétský
krále	král	k1gMnSc2	král
Mínóa	Mínóus	k1gMnSc2	Mínóus
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byla	být	k5eAaImAgFnS	být
opuštěna	opuštěn	k2eAgNnPc1d1	opuštěno
athénským	athénský	k2eAgMnSc7d1	athénský
princem	princ	k1gMnSc7	princ
Theseem	Theseus	k1gMnSc7	Theseus
<g/>
.	.	kIx.	.
</s>
<s>
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
přenesl	přenést	k5eAaPmAgInS	přenést
korunu	koruna	k1gFnSc4	koruna
na	na	k7c4	na
nebesa	nebesa	k1gNnPc4	nebesa
jako	jako	k8xC	jako
připomínku	připomínka	k1gFnSc4	připomínka
svatby	svatba	k1gFnSc2	svatba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
alternativní	alternativní	k2eAgFnSc6d1	alternativní
verzi	verze	k1gFnSc6	verze
má	mít	k5eAaImIp3nS	mít
omámený	omámený	k2eAgMnSc1d1	omámený
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
dát	dát	k5eAaPmF	dát
korunu	koruna	k1gFnSc4	koruna
Ariadně	Ariadna	k1gFnSc3	Ariadna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
poté	poté	k6eAd1	poté
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
dá	dát	k5eAaPmIp3nS	dát
Theseovi	Theseus	k1gMnSc3	Theseus
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
zabije	zabít	k5eAaPmIp3nS	zabít
Minotaura	Minotaura	k1gFnSc1	Minotaura
<g/>
.	.	kIx.	.
<g/>
Arabové	Arab	k1gMnPc1	Arab
nazývali	nazývat	k5eAaImAgMnP	nazývat
souhvězdí	souhvězdí	k1gNnSc4	souhvězdí
Alphekka	Alphekek	k1gInSc2	Alphekek
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
název	název	k1gInSc4	název
hvězdy	hvězda	k1gFnSc2	hvězda
Alfa	alfa	k1gFnSc1	alfa
Coronae	Coronae	k1gFnSc1	Coronae
Borealis	Borealis	k1gFnSc1	Borealis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
oddělené	oddělený	k2eAgInPc4d1	oddělený
nebo	nebo	k8xC	nebo
rozebrané	rozebraný	k2eAgInPc4d1	rozebraný
(	(	kIx(	(
<g/>
ا	ا	k?	ا
al-Fakkah	al-Fakkah	k1gInSc1	al-Fakkah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
podobnost	podobnost	k1gFnSc4	podobnost
hvězd	hvězda	k1gFnPc2	hvězda
Severní	severní	k2eAgFnSc2d1	severní
koruny	koruna	k1gFnSc2	koruna
s	s	k7c7	s
volným	volný	k2eAgInSc7d1	volný
řetězem	řetěz	k1gInSc7	řetěz
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
též	též	k9	též
představovalo	představovat	k5eAaImAgNnS	představovat
rozbitou	rozbitý	k2eAgFnSc4d1	rozbitá
misku	miska	k1gFnSc4	miska
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
beduíny	beduín	k1gMnPc7	beduín
bylo	být	k5eAaImAgNnS	být
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
qaṣ	qaṣ	k?	qaṣ
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
al-masā	alasā	k?	al-masā
(	(	kIx(	(
<g/>
ق	ق	k?	ق
ا	ا	k?	ا
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
mísa	mísa	k1gFnSc1	mísa
chudých	chudý	k2eAgMnPc2d1	chudý
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Severní	severní	k2eAgFnSc1d1	severní
koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
starověcí	starověký	k2eAgMnPc1d1	starověký
čínští	čínský	k2eAgMnPc1d1	čínský
astronomové	astronom	k1gMnPc1	astronom
nakreslili	nakreslit	k5eAaPmAgMnP	nakreslit
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
západní	západní	k2eAgMnPc1d1	západní
astronomové	astronom	k1gMnPc1	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Čínští	čínský	k2eAgMnPc1d1	čínský
astronomové	astronom	k1gMnPc1	astronom
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
asterismus	asterismus	k1gInSc4	asterismus
devět	devět	k4xCc1	devět
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
dalšími	další	k2eAgFnPc7d1	další
hvězdami	hvězda	k1gFnPc7	hvězda
jsou	být	k5eAaImIp3nP	být
Pí	pí	k1gNnSc1	pí
a	a	k8xC	a
Ró	ró	k1gNnSc1	ró
Coronae	Corona	k1gInSc2	Corona
Borealis	Borealis	k1gFnSc2	Borealis
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
snadné	snadný	k2eAgNnSc1d1	snadné
ho	on	k3xPp3gInSc4	on
určit	určit	k5eAaPmF	určit
na	na	k7c6	na
čínských	čínský	k2eAgFnPc6d1	čínská
hvězdných	hvězdný	k2eAgFnPc6d1	hvězdná
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
viděli	vidět	k5eAaImAgMnP	vidět
jako	jako	k9	jako
vězení	vězení	k1gNnSc4	vězení
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
.	.	kIx.	.
<g/>
Lidé	člověk	k1gMnPc1	člověk
Skidi	Skid	k1gMnPc1	Skid
<g/>
,	,	kIx,	,
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
viděli	vidět	k5eAaImAgMnP	vidět
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
Severní	severní	k2eAgFnSc2d1	severní
koruny	koruna	k1gFnSc2	koruna
kmenovou	kmenový	k2eAgFnSc4d1	kmenová
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
Polárka	Polárka	k1gFnSc1	Polárka
pak	pak	k6eAd1	pak
představovala	představovat	k5eAaImAgFnS	představovat
jejího	její	k3xOp3gMnSc4	její
náčelníka	náčelník	k1gMnSc4	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Indiáni	Indián	k1gMnPc1	Indián
Mi	já	k3xPp1nSc3	já
<g/>
'	'	kIx"	'
<g/>
kmaq	kmaq	k?	kmaq
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Kanady	Kanada	k1gFnSc2	Kanada
si	se	k3xPyFc3	se
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
představovali	představovat	k5eAaImAgMnP	představovat
jako	jako	k8xS	jako
jámu	jáma	k1gFnSc4	jáma
nebeského	nebeský	k2eAgMnSc4d1	nebeský
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
,	,	kIx,	,
Mskegwǒ	Mskegwǒ	k1gFnSc4	Mskegwǒ
<g/>
.	.	kIx.	.
<g/>
Polynéské	polynéský	k2eAgInPc1d1	polynéský
národy	národ	k1gInPc1	národ
také	také	k6eAd1	také
uznávaly	uznávat	k5eAaImAgInP	uznávat
Severní	severní	k2eAgFnSc4d1	severní
korunu	koruna	k1gFnSc4	koruna
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
asterismus	asterismus	k1gInSc4	asterismus
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Tuamotu	Tuamot	k1gInSc2	Tuamot
ji	on	k3xPp3gFnSc4	on
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
Na	na	k7c4	na
Kaua-ki-tokerau	Kauaiokeraa	k1gFnSc4	Kaua-ki-tokeraa
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
Te	Te	k1gFnSc1	Te
Hetu	Hetus	k1gInSc2	Hetus
<g/>
.	.	kIx.	.
</s>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
zřejmě	zřejmě	k6eAd1	zřejmě
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Kaua-mea	Kauaeum	k1gNnSc2	Kaua-meum
<g/>
,	,	kIx,	,
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
Rangawhenua	Rangawhenu	k1gInSc2	Rangawhenu
a	a	k8xC	a
na	na	k7c6	na
Cookových	Cookův	k2eAgInPc6d1	Cookův
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
atolu	atol	k1gInSc6	atol
Pokapuka	Pokapuk	k1gMnSc4	Pokapuk
Te	Te	k1gMnSc4	Te
Wale-o-Awitu	Wale-Awita	k1gMnSc4	Wale-o-Awita
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
ostrovů	ostrov	k1gInPc2	ostrov
Tonga	Tonga	k1gFnSc1	Tonga
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
o	o	k7c4	o
Ao-o-Uvea	Ao-Uveus	k1gMnSc4	Ao-o-Uveus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c6	o
Kau-kupenga	Kauupenga	k1gFnSc1	Kau-kupenga
<g/>
.	.	kIx.	.
<g/>
Australští	australský	k2eAgMnPc1d1	australský
Aboriginci	Aboriginec	k1gMnPc1	Aboriginec
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
tvaru	tvar	k1gInSc3	tvar
nazývají	nazývat	k5eAaImIp3nP	nazývat
womera	womero	k1gNnPc4	womero
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
bumerang	bumerang	k1gInSc1	bumerang
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
kmene	kmen	k1gInSc2	kmen
Wailwun	Wailwun	k1gInSc1	Wailwun
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc1d1	obývající
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
část	část	k1gFnSc1	část
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
říkají	říkat	k5eAaImIp3nP	říkat
Severní	severní	k2eAgFnSc3d1	severní
koruně	koruna	k1gFnSc3	koruna
mullion	mullion	k1gInSc4	mullion
wollai	wolla	k1gFnSc2	wolla
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
orlí	orlí	k2eAgNnSc1d1	orlí
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
Altair	Altaira	k1gFnPc2	Altaira
a	a	k8xC	a
Vega	Vega	k1gFnSc1	Vega
-	-	kIx~	-
obě	dva	k4xCgFnPc1	dva
zvané	zvaný	k2eAgFnPc1d1	zvaná
mullion	mullion	k1gInSc4	mullion
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
dvojicí	dvojice	k1gFnSc7	dvojice
orlů	orel	k1gMnPc2	orel
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
hnízdo	hnízdo	k1gNnSc1	hnízdo
patří	patřit	k5eAaImIp3nP	patřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgInPc1d1	jiný
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Severní	severní	k2eAgFnSc2d1	severní
koruny	koruna	k1gFnSc2	koruna
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1730	[number]	k4	1730
Cobrinianem	Cobrinian	k1gMnSc7	Cobrinian
Thomasem	Thomas	k1gMnSc7	Thomas
ve	v	k7c6	v
hvězdném	hvězdný	k2eAgInSc6d1	hvězdný
atlase	atlas	k1gInSc6	atlas
Mercurii	Mercurie	k1gFnSc4	Mercurie
Philosophicii	Philosophicie	k1gFnSc4	Philosophicie
Firmamentum	Firmamentum	k1gNnSc4	Firmamentum
Firminianum	Firminianum	k1gInSc1	Firminianum
Descriptionem	Description	k1gInSc7	Description
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Corona	Coron	k1gMnSc4	Coron
Firmiana	Firmian	k1gMnSc4	Firmian
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
salcburského	salcburský	k2eAgMnSc2d1	salcburský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nové	nový	k2eAgNnSc1d1	nové
pojmenování	pojmenování	k1gNnSc1	pojmenování
nebylo	být	k5eNaImAgNnS	být
ostatními	ostatní	k2eAgMnPc7d1	ostatní
kartografy	kartograf	k1gMnPc7	kartograf
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
přijato	přijat	k2eAgNnSc1d1	přijato
<g/>
.	.	kIx.	.
<g/>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Severní	severní	k2eAgFnSc2d1	severní
koruny	koruna	k1gFnSc2	koruna
je	být	k5eAaImIp3nS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Hypnos	hypnosa	k1gFnPc2	hypnosa
od	od	k7c2	od
H.	H.	kA	H.
P.	P.	kA	P.
Lovercafta	Lovercafto	k1gNnPc4	Lovercafto
<g/>
,	,	kIx,	,
zveřejněné	zveřejněný	k2eAgInPc4d1	zveřejněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
kapela	kapela	k1gFnSc1	kapela
Cadacross	Cadacrossa	k1gFnPc2	Cadacrossa
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Corona	Corona	k1gFnSc1	Corona
Borealis	Borealis	k1gFnSc1	Borealis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HLAD	hlad	k1gMnSc1	hlad
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
;	;	kIx,	;
PAVLOUSEK	PAVLOUSEK	kA	PAVLOUSEK
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLECZEK	KLECZEK	kA	KLECZEK
<g/>
,	,	kIx,	,
Josif	Josif	k1gInSc1	Josif
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc1	náš
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Severní	severní	k2eAgFnSc1d1	severní
koruna	koruna	k1gFnSc1	koruna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Severní	severní	k2eAgFnSc2d1	severní
koruny	koruna	k1gFnSc2	koruna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
severní	severní	k2eAgFnSc2d1	severní
oblohy	obloha	k1gFnSc2	obloha
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
koruna	koruna	k1gFnSc1	koruna
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
"	"	kIx"	"
<g/>
Astronomia	Astronomia	k1gFnSc1	Astronomia
–	–	k?	–
astronomický	astronomický	k2eAgInSc1d1	astronomický
server	server	k1gInSc1	server
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
ZČU	ZČU	kA	ZČU
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
vesmíru	vesmír	k1gInSc2	vesmír
-	-	kIx~	-
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Corona	Corona	k1gFnSc1	Corona
Borealis	Borealis	k1gFnSc1	Borealis
</s>
</p>
