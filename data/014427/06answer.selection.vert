<s>
Území	území	k1gNnSc1	území
ohraničené	ohraničený	k2eAgFnSc2d1	ohraničená
těmito	tento	k3xDgFnPc7	tento
větvemi	větev	k1gFnPc7	větev
a	a	k8xC	a
vodní	vodní	k2eAgFnSc7d1	vodní
plochou	plocha	k1gFnSc7	plocha
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
vlévá	vlévat	k5eAaImIp3nS	vlévat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
shora	shora	k6eAd1	shora
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
přibližně	přibližně	k6eAd1	přibližně
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1	trojúhelníkový
tvar	tvar	k1gInSc1	tvar
jako	jako	k8xS	jako
řecké	řecký	k2eAgNnSc1d1	řecké
písmeno	písmeno	k1gNnSc1	písmeno
delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
