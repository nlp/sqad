<s desamb="1">
Řeka	řeka	k1gFnSc1
se	se	k3xPyFc4
před	před	k7c7
ním	on	k3xPp3gInSc7
často	často	k6eAd1
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
několik	několik	k4yIc4
větví	větev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6
se	se	k3xPyFc4
řeka	řeka	k1gFnSc1
vlévá	vlévat	k5eAaImIp3nS
do	do	k7c2
moře	moře	k1gNnSc2
nebo	nebo	k8xC
jezera	jezero	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
ústí	ústí	k1gNnSc1
<g/>
.	.	kIx.
</s>