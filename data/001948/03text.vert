<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
je	být	k5eAaImIp3nS	být
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
z	z	k7c2	z
pěti	pět	k4xCc2	pět
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
borough	borough	k1gInSc1	borough
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
územně	územně	k6eAd1	územně
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Kings	Kings	k1gInSc1	Kings
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Kings	Kings	k1gInSc1	Kings
County	Counta	k1gFnSc2	Counta
<g/>
)	)	kIx)	)
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
se	se	k3xPyFc4	se
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
z	z	k7c2	z
malého	malý	k2eAgNnSc2d1	malé
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
města	město	k1gNnSc2	město
"	"	kIx"	"
<g/>
Breuckelen	Breuckelna	k1gFnPc2	Breuckelna
<g/>
"	"	kIx"	"
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
samostatným	samostatný	k2eAgNnSc7d1	samostatné
městem	město	k1gNnSc7	město
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
městu	město	k1gNnSc3	město
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
měl	mít	k5eAaImAgInS	mít
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
2	[number]	k4	2
565	[number]	k4	565
635	[number]	k4	635
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
byli	být	k5eAaImAgMnP	být
první	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
konci	konec	k1gInSc6	konec
Long	Longa	k1gFnPc2	Longa
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
holandská	holandský	k2eAgFnSc1d1	holandská
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
Midwout	Midwout	k1gMnSc1	Midwout
(	(	kIx(	(
<g/>
Midwood	Midwood	k1gInSc1	Midwood
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Breuckelen	Breuckelna	k1gFnPc2	Breuckelna
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
městě	město	k1gNnSc6	město
Breukelen	Breukelna	k1gFnPc2	Breukelna
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Utrecht	Utrechta	k1gFnPc2	Utrechta
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
samosprávnou	samosprávný	k2eAgFnSc7d1	samosprávná
obcí	obec	k1gFnSc7	obec
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
Breuckelen	Breuckelen	k2eAgMnSc1d1	Breuckelen
součástí	součást	k1gFnSc7	součást
provincie	provincie	k1gFnSc2	provincie
Nové	Nová	k1gFnSc2	Nová
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
začleněny	začlenit	k5eAaPmNgFnP	začlenit
do	do	k7c2	do
Brooklynu	Brooklyn	k1gInSc2	Brooklyn
byly	být	k5eAaImAgFnP	být
Boswijk	Boswijk	k1gInSc4	Boswijk
(	(	kIx(	(
<g/>
Bushwick	Bushwick	k1gInSc1	Bushwick
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nieuw	Nieuw	k1gMnSc1	Nieuw
Utrecht	Utrecht	k1gMnSc1	Utrecht
(	(	kIx(	(
<g/>
Utrecht	Utrecht	k1gMnSc1	Utrecht
New	New	k1gMnSc1	New
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nieuw	Nieuw	k1gFnSc1	Nieuw
Amersfoort	Amersfoort	k1gInSc1	Amersfoort
(	(	kIx(	(
<g/>
Flatlands	Flatlands	k1gInSc1	Flatlands
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
domů	dům	k1gInPc2	dům
a	a	k8xC	a
hřbitovů	hřbitov	k1gInPc2	hřbitov
stále	stále	k6eAd1	stále
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
původu	původ	k1gInSc6	původ
čtvrti	čtvrt	k1gFnSc2	čtvrt
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
.	.	kIx.	.
</s>
<s>
Holanďané	Holanďan	k1gMnPc1	Holanďan
ztratili	ztratit	k5eAaPmAgMnP	ztratit
Breuckelen	Breuckelen	k2eAgInSc4d1	Breuckelen
při	při	k7c6	při
britském	britský	k2eAgNnSc6d1	Britské
dobytí	dobytí	k1gNnSc6	dobytí
Nového	Nového	k2eAgNnSc2d1	Nového
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1664	[number]	k4	1664
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1683	[number]	k4	1683
Britové	Brit	k1gMnPc1	Brit
reorganizovali	reorganizovat	k5eAaBmAgMnP	reorganizovat
provincii	provincie	k1gFnSc4	provincie
New	New	k1gFnPc2	New
York	York	k1gInSc4	York
do	do	k7c2	do
dvanácti	dvanáct	k4xCc2	dvanáct
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každý	každý	k3xTgInSc1	každý
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
názvu	název	k1gInSc2	název
Breuckelen	Breuckelen	k2eAgInSc4d1	Breuckelen
Brockland	Brockland	k1gInSc4	Brockland
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Brocklin	Brocklin	k1gInSc1	Brocklin
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Brookline	Brooklin	k1gInSc5	Brooklin
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
Brooklyn	Brooklyn	k1gInSc4	Brooklyn
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Kings	Kings	k1gInSc1	Kings
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
Brooklyn	Brooklyn	k1gInSc4	Brooklyn
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
měst	město	k1gNnPc2	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
..	..	k?	..
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
1776	[number]	k4	1776
se	se	k3xPyFc4	se
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kings	Kingsa	k1gFnPc2	Kingsa
bojovala	bojovat	k5eAaImAgFnS	bojovat
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Long	Long	k1gInSc4	Long
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
také	také	k9	také
někdy	někdy	k6eAd1	někdy
nazývána	nazývat	k5eAaImNgFnS	nazývat
jako	jako	k8xS	jako
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Brooklyn	Brooklyn	k1gInSc4	Brooklyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
americké	americký	k2eAgFnSc2d1	americká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
bitev	bitva	k1gFnPc2	bitva
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
schopnostech	schopnost	k1gFnPc6	schopnost
George	Georg	k1gMnSc2	Georg
Washingtona	Washington	k1gMnSc2	Washington
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taktický	taktický	k2eAgInSc4d1	taktický
ústup	ústup	k1gInSc4	ústup
přes	přes	k7c4	přes
East	East	k2eAgInSc4d1	East
River	River	k1gInSc4	River
je	být	k5eAaImIp3nS	být
viděn	vidět	k5eAaImNgMnS	vidět
historiky	historik	k1gMnPc7	historik
jako	jako	k9	jako
jeho	on	k3xPp3gInSc4	on
velký	velký	k2eAgInSc4d1	velký
praktický	praktický	k2eAgInSc4d1	praktický
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
růst	růst	k1gInSc1	růst
podél	podél	k7c2	podél
hospodářsky-strategického	hospodářskytrategický	k2eAgNnSc2d1	hospodářsky-strategický
pobřeží	pobřeží	k1gNnSc2	pobřeží
East	East	k1gMnSc1	East
River	River	k1gMnSc1	River
<g/>
,	,	kIx,	,
naproti	naproti	k6eAd1	naproti
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Brooklynská	brooklynský	k2eAgFnSc1d1	Brooklynská
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
trojnásobně	trojnásobně	k6eAd1	trojnásobně
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1800	[number]	k4	1800
a	a	k8xC	a
1820	[number]	k4	1820
<g/>
,	,	kIx,	,
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
během	během	k7c2	během
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
dvě	dva	k4xCgNnPc4	dva
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
Město	město	k1gNnSc4	město
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
a	a	k8xC	a
město	město	k1gNnSc1	město
Williamsburgh	Williamsburgha	k1gFnPc2	Williamsburgha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Brooklynu	Brooklyn	k1gInSc3	Brooklyn
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
Williamsburgh	Williamsburgh	k1gInSc1	Williamsburgh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Brighton	Brighton	k1gInSc1	Brighton
Beach	Beacha	k1gFnPc2	Beacha
Line	linout	k5eAaImIp3nS	linout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
předznamenala	předznamenat	k5eAaPmAgFnS	předznamenat
explozivní	explozivní	k2eAgInSc4d1	explozivní
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Brooklynu	Brooklyn	k1gInSc3	Brooklyn
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
New	New	k1gFnSc1	New
Lots	Lotsa	k1gFnPc2	Lotsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Flatbush	Flatbusha	k1gFnPc2	Flatbusha
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Gravesend	Gravesenda	k1gFnPc2	Gravesenda
a	a	k8xC	a
město	město	k1gNnSc1	město
New	New	k1gFnSc2	New
Utrecht	Utrechta	k1gFnPc2	Utrechta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
a	a	k8xC	a
město	město	k1gNnSc1	město
Flatlands	Flatlandsa	k1gFnPc2	Flatlandsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
svých	svůj	k3xOyFgFnPc2	svůj
přirozených	přirozený	k2eAgFnPc2d1	přirozená
hranic	hranice	k1gFnPc2	hranice
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
okresu	okres	k1gInSc2	okres
Kings	Kingsa	k1gFnPc2	Kingsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Bridge	Bridge	k1gInSc1	Bridge
a	a	k8xC	a
přeprava	přeprava	k1gFnSc1	přeprava
na	na	k7c4	na
Manhattan	Manhattan	k1gInSc4	Manhattan
nebyla	být	k5eNaImAgFnS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
byl	být	k5eAaImAgInS	být
nyní	nyní	k6eAd1	nyní
připraven	připravit	k5eAaPmNgInS	připravit
zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
stále	stále	k6eAd1	stále
většího	veliký	k2eAgInSc2d2	veliký
procesu	proces	k1gInSc2	proces
slučování	slučování	k1gNnSc2	slučování
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
brooklynští	brooklynský	k2eAgMnPc1d1	brooklynský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Manhattanu	Manhattan	k1gInSc3	Manhattan
<g/>
,	,	kIx,	,
Bronxu	Bronx	k1gInSc3	Bronx
<g/>
,	,	kIx,	,
Queensu	Queens	k1gInSc3	Queens
a	a	k8xC	a
Richmondu	Richmond	k1gInSc3	Richmond
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
referendum	referendum	k1gNnSc1	referendum
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Kings	Kings	k1gInSc1	Kings
si	se	k3xPyFc3	se
nicméně	nicméně	k8xC	nicméně
udržel	udržet	k5eAaPmAgMnS	udržet
stav	stav	k1gInSc4	stav
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
okresů	okres	k1gInPc2	okres
státu	stát	k1gInSc2	stát
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
New	New	k1gFnSc3	New
York	York	k1gInSc4	York
City	City	k1gFnSc2	City
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Centralizovaná	centralizovaný	k2eAgFnSc1d1	centralizovaná
vláda	vláda	k1gFnSc1	vláda
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
veřejné	veřejný	k2eAgNnSc4d1	veřejné
školství	školství	k1gNnSc4	školství
<g/>
,	,	kIx,	,
nápravná	nápravný	k2eAgNnPc4d1	nápravné
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
knihovny	knihovna	k1gFnPc4	knihovna
<g/>
,	,	kIx,	,
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
rekreační	rekreační	k2eAgNnPc1d1	rekreační
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
kanalizace	kanalizace	k1gFnPc1	kanalizace
<g/>
,	,	kIx,	,
vodovody	vodovod	k1gInPc1	vodovod
<g/>
,	,	kIx,	,
a	a	k8xC	a
sociální	sociální	k2eAgFnPc4d1	sociální
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
čtvrti	čtvrt	k1gFnSc2	čtvrt
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
rovnováhy	rovnováha	k1gFnSc2	rovnováha
centralizace	centralizace	k1gFnSc2	centralizace
s	s	k7c7	s
místními	místní	k2eAgInPc7d1	místní
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
drží	držet	k5eAaImIp3nS	držet
většinu	většina	k1gFnSc4	většina
veřejných	veřejný	k2eAgInPc2d1	veřejný
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
69,7	[number]	k4	69,7
<g/>
%	%	kIx~	%
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
voličů	volič	k1gMnPc2	volič
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
jsou	být	k5eAaImIp3nP	být
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Republikáni	republikán	k1gMnPc1	republikán
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
vv	vv	k?	vv
Bay	Bay	k1gFnSc2	Bay
Ridge	Ridge	k1gFnPc2	Ridge
a	a	k8xC	a
Dyker	Dykra	k1gFnPc2	Dykra
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
pěti	pět	k4xCc2	pět
městských	městský	k2eAgInPc2d1	městský
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
shodných	shodný	k2eAgInPc2d1	shodný
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
čtvrtí	čtvrt	k1gFnSc7	čtvrt
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
systém	systém	k1gInSc4	systém
trestního	trestní	k2eAgInSc2d1	trestní
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
okresního	okresní	k2eAgMnSc2d1	okresní
prokurátora	prokurátor	k1gMnSc2	prokurátor
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k2eAgMnSc2d1	vedoucí
státního	státní	k2eAgMnSc2d1	státní
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
volen	volen	k2eAgInSc1d1	volen
lidovým	lidový	k2eAgNnSc7d1	lidové
hlasováním	hlasování	k1gNnSc7	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Brooklynská	brooklynský	k2eAgFnSc1d1	Brooklynská
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
má	mít	k5eAaImIp3nS	mít
16	[number]	k4	16
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
pěti	pět	k4xCc2	pět
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Brooklynské	brooklynský	k2eAgNnSc1d1	Brooklynské
oficiální	oficiální	k2eAgNnSc1d1	oficiální
motto	motto	k1gNnSc1	motto
je	být	k5eAaImIp3nS	být
Een	Een	k1gMnPc3	Een
Draght	Draght	k2eAgInSc4d1	Draght
Mackt	Mackt	k1gInSc4	Mackt
Maght	Maghta	k1gFnPc2	Maghta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
ve	v	k7c6	v
(	(	kIx(	(
<g/>
staré	starý	k2eAgFnSc6d1	stará
<g/>
)	)	kIx)	)
nizozemštině	nizozemština	k1gFnSc6	nizozemština
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
V	v	k7c6	v
jednotě	jednota	k1gFnSc6	jednota
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Motto	motto	k1gNnSc1	motto
je	být	k5eAaImIp3nS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
na	na	k7c6	na
Brooklynské	brooklynský	k2eAgFnSc6d1	Brooklynská
pečeti	pečeť	k1gFnSc6	pečeť
a	a	k8xC	a
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
barvy	barva	k1gFnPc1	barva
Brooklynu	Brooklyn	k1gInSc2	Brooklyn
jsou	být	k5eAaImIp3nP	být
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
nehlasoval	hlasovat	k5eNaImAgInS	hlasovat
pro	pro	k7c4	pro
republikána	republikán	k1gMnSc4	republikán
při	při	k7c6	při
národních	národní	k2eAgFnPc6d1	národní
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
50	[number]	k4	50
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
demokrat	demokrat	k1gMnSc1	demokrat
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
obdržel	obdržet	k5eAaPmAgMnS	obdržet
79,4	[number]	k4	79,4
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
republikán	republikán	k1gMnSc1	republikán
John	John	k1gMnSc1	John
McCain	McCain	k1gMnSc1	McCain
obdržel	obdržet	k5eAaPmAgMnS	obdržet
20,0	[number]	k4	20,0
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
)	)	kIx)	)
<g/>
.	.	kIx.	.
44	[number]	k4	44
<g/>
%	%	kIx~	%
pracujících	pracující	k2eAgMnPc2d1	pracující
obyvatel	obyvatel	k1gMnPc2	obyvatel
Brooklynu	Brooklyn	k1gInSc2	Brooklyn
pracuje	pracovat	k5eAaImIp3nS	pracovat
uvnitř	uvnitř	k7c2	uvnitř
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
pracuje	pracovat	k5eAaImIp3nS	pracovat
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc4	ten
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
Booklynu	Booklyn	k1gInSc2	Booklyn
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
imigrace	imigrace	k1gFnSc1	imigrace
do	do	k7c2	do
Brooklynu	Brooklyn	k1gInSc2	Brooklyn
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
pracovní	pracovní	k2eAgNnPc1d1	pracovní
místa	místo	k1gNnPc1	místo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
maloobchodu	maloobchod	k1gInSc2	maloobchod
a	a	k8xC	a
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
čtvrti	čtvrt	k1gFnSc6	čtvrt
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
přeorientoval	přeorientovat	k5eAaPmAgInS	přeorientovat
z	z	k7c2	z
výrobní	výrobní	k2eAgFnSc2d1	výrobní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
215	[number]	k4	215
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Brooklynu	Brooklyn	k1gInSc2	Brooklyn
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
sektoru	sektor	k1gInSc6	sektor
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
27	[number]	k4	27
500	[number]	k4	500
pracovalo	pracovat	k5eAaImAgNnS	pracovat
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
výroba	výroba	k1gFnSc1	výroba
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
podstatný	podstatný	k2eAgInSc1d1	podstatný
základ	základ	k1gInSc1	základ
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
oděvnictví	oděvnictví	k1gNnSc6	oděvnictví
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
výrobních	výrobní	k2eAgInPc6d1	výrobní
odvětví	odvětví	k1gNnPc1	odvětví
jako	jako	k8xS	jako
nábytek	nábytek	k1gInSc1	nábytek
<g/>
,	,	kIx,	,
kovovýroba	kovovýroba	k1gFnSc1	kovovýroba
a	a	k8xC	a
potravinářské	potravinářský	k2eAgInPc1d1	potravinářský
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
společnost	společnost	k1gFnSc1	společnost
Pfizer	Pfizra	k1gFnPc2	Pfizra
má	mít	k5eAaImIp3nS	mít
výrobní	výrobní	k2eAgInSc4d1	výrobní
závod	závod	k1gInSc4	závod
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
990	[number]	k4	990
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
jsou	být	k5eAaImIp3nP	být
nejrychleji	rychle	k6eAd3	rychle
rostoucími	rostoucí	k2eAgNnPc7d1	rostoucí
odvětvími	odvětví	k1gNnPc7	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
a	a	k8xC	a
střední	střední	k2eAgInPc1d1	střední
podniky	podnik	k1gInPc1	podnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
91	[number]	k4	91
<g/>
%	%	kIx~	%
ze	z	k7c2	z
zhruba	zhruba	k6eAd1	zhruba
38.704	[number]	k4	38.704
obchodních	obchodní	k2eAgNnPc2d1	obchodní
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
mělo	mít	k5eAaImAgNnS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
20	[number]	k4	20
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
čtvrti	čtvrt	k1gFnSc2	čtvrt
5,9	[number]	k4	5,9
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005-2007	[number]	k4	2005-2007
bylo	být	k5eAaImAgNnS	být
43,7	[number]	k4	43,7
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
36,2	[number]	k4	36,2
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
a	a	k8xC	a
9,3	[number]	k4	9,3
<g/>
%	%	kIx~	%
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
.	.	kIx.	.
19,6	[number]	k4	19,6
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
byli	být	k5eAaImAgMnP	být
Hispánci	Hispánek	k1gMnPc1	Hispánek
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
byla	být	k5eAaImAgNnP	být
rozložena	rozložit	k5eAaPmNgNnP	rozložit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
26.9	[number]	k4	26.9
<g/>
%	%	kIx~	%
do	do	k7c2	do
věku	věk	k1gInSc2	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
10.3	[number]	k4	10.3
<g/>
%	%	kIx~	%
od	od	k7c2	od
18	[number]	k4	18
let	léto	k1gNnPc2	léto
do	do	k7c2	do
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
30.8	[number]	k4	30.8
<g/>
%	%	kIx~	%
od	od	k7c2	od
25	[number]	k4	25
do	do	k7c2	do
44	[number]	k4	44
<g/>
,	,	kIx,	,
20.6	[number]	k4	20.6
<g/>
%	%	kIx~	%
od	od	k7c2	od
45	[number]	k4	45
do	do	k7c2	do
64	[number]	k4	64
a	a	k8xC	a
11.5	[number]	k4	11.5
<g/>
%	%	kIx~	%
65	[number]	k4	65
roků	rok	k1gInPc2	rok
a	a	k8xC	a
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
byl	být	k5eAaImAgInS	být
33	[number]	k4	33
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
88,4	[number]	k4	88,4
mužů	muž	k1gMnPc2	muž
na	na	k7c4	na
100	[number]	k4	100
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
37,4	[number]	k4	37,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
byli	být	k5eAaImAgMnP	být
narozeni	narozen	k2eAgMnPc1d1	narozen
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
46,1	[number]	k4	46,1
<g/>
%	%	kIx~	%
mluvilo	mluvit	k5eAaImAgNnS	mluvit
doma	doma	k6eAd1	doma
jiným	jiný	k2eAgInSc7d1	jiný
jazykem	jazyk	k1gInSc7	jazyk
než	než	k8xS	než
angličtinou	angličtina	k1gFnSc7	angličtina
a	a	k8xC	a
27,4	[number]	k4	27,4
<g/>
%	%	kIx~	%
mělo	mít	k5eAaImAgNnS	mít
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
titul	titul	k1gInSc4	titul
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc4d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odhadu	odhad	k1gInSc6	odhad
amerického	americký	k2eAgInSc2d1	americký
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
2	[number]	k4	2
486	[number]	k4	486
235	[number]	k4	235
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
880	[number]	k4	880
727	[number]	k4	727
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
583	[number]	k4	583
922	[number]	k4	922
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byla	být	k5eAaImAgFnS	být
13480	[number]	k4	13480
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tam	tam	k6eAd1	tam
930	[number]	k4	930
866	[number]	k4	866
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hustoty	hustota	k1gFnSc2	hustota
5090	[number]	k4	5090
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtí	čtvrt	k1gFnSc7	čtvrt
prochází	procházet	k5eAaImIp3nS	procházet
18	[number]	k4	18
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
92,8	[number]	k4	92,8
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Brooklynu	Brooklyn	k1gInSc2	Brooklyn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
cestuje	cestovat	k5eAaImIp3nS	cestovat
metrem	metr	k1gInSc7	metr
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
autobusová	autobusový	k2eAgFnSc1d1	autobusová
síť	síť	k1gFnSc1	síť
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
celou	celý	k2eAgFnSc4d1	celá
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
také	také	k9	také
denní	denní	k2eAgFnSc1d1	denní
expresní	expresní	k2eAgFnSc1d1	expresní
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
na	na	k7c4	na
Manhattan	Manhattan	k1gInSc4	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Slavné	slavný	k2eAgInPc1d1	slavný
newyorské	newyorský	k2eAgInPc1d1	newyorský
žluté	žlutý	k2eAgInPc1d1	žlutý
taxíky	taxík	k1gInPc1	taxík
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
je	být	k5eAaImIp3nS	být
napojen	napojit	k5eAaPmNgInS	napojit
na	na	k7c4	na
Manhattan	Manhattan	k1gInSc4	Manhattan
třemi	tři	k4xCgInPc7	tři
mosty	most	k1gInPc7	most
<g/>
,	,	kIx,	,
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
Bridge	Bridge	k1gInSc1	Bridge
a	a	k8xC	a
Williamsburg	Williamsburg	k1gInSc1	Williamsburg
Bridge	Bridg	k1gInSc2	Bridg
<g/>
,	,	kIx,	,
automobilovým	automobilový	k2eAgInSc7d1	automobilový
tunelem	tunel	k1gInSc7	tunel
Brooklyn-Battery	Brooklyn-Batter	k1gInPc1	Brooklyn-Batter
Tunnel	Tunnel	k1gInSc1	Tunnel
a	a	k8xC	a
několika	několik	k4yIc7	několik
tunely	tunel	k1gInPc7	tunel
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Verrazano-Narrows	Verrazano-Narrows	k1gInSc1	Verrazano-Narrows
Bridge	Bridg	k1gInSc2	Bridg
spojuje	spojovat	k5eAaImIp3nS	spojovat
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
se	s	k7c7	s
State	status	k1gInSc5	status
Islandem	Island	k1gInSc7	Island
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Brooklyn	Brooklyn	k1gInSc4	Brooklyn
<g/>
,	,	kIx,	,
stránky	stránka	k1gFnPc4	stránka
čtvrti	čtvrt	k1gFnSc2	čtvrt
</s>
