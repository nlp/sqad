<p>
<s>
Gorila	gorila	k1gFnSc1	gorila
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
největších	veliký	k2eAgMnPc2d3	veliký
žijících	žijící	k2eAgMnPc2d1	žijící
primátů	primát	k1gMnPc2	primát
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
býložravé	býložravý	k2eAgFnPc1d1	býložravá
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
lesy	les	k1gInPc1	les
střední	střední	k2eAgFnSc2d1	střední
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Gorily	gorila	k1gFnPc1	gorila
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
poddruhy	poddruh	k1gInPc4	poddruh
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
biologů	biolog	k1gMnPc2	biolog
pět	pět	k4xCc4	pět
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DNA	dna	k1gFnSc1	dna
gorily	gorila	k1gFnSc2	gorila
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
98	[number]	k4	98
až	až	k9	až
99	[number]	k4	99
%	%	kIx~	%
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
DNA	DNA	kA	DNA
člověka	člověk	k1gMnSc4	člověk
</s>
</p>
<p>
<s>
a	a	k8xC	a
po	po	k7c6	po
šimpanzích	šimpanzí	k2eAgFnPc6d1	šimpanzí
jsou	být	k5eAaImIp3nP	být
gorily	gorila	k1gFnPc4	gorila
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
příbuzní	příbuzný	k1gMnPc1	příbuzný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gorily	gorila	k1gFnPc1	gorila
obývají	obývat	k5eAaImIp3nP	obývat
tropické	tropický	k2eAgFnPc1d1	tropická
a	a	k8xC	a
subtropické	subtropický	k2eAgInPc1d1	subtropický
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
omezuje	omezovat	k5eAaImIp3nS	omezovat
jen	jen	k9	jen
na	na	k7c4	na
několik	několik	k4yIc4	několik
procent	procento	k1gNnPc2	procento
rozlohy	rozloha	k1gFnSc2	rozloha
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
biotopy	biotop	k1gInPc1	biotop
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
velký	velký	k2eAgInSc4d1	velký
rozsah	rozsah	k1gInSc4	rozsah
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Gorila	gorila	k1gFnSc1	gorila
východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	(
<g/>
horská	horský	k2eAgFnSc1d1	horská
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Virunga	Virunga	k1gFnSc1	Virunga
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
2225	[number]	k4	2225
až	až	k8xS	až
4267	[number]	k4	4267
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Gorila	gorila	k1gFnSc1	gorila
západní	západní	k2eAgFnSc1d1	západní
(	(	kIx(	(
<g/>
nížinná	nížinný	k2eAgFnSc1d1	nížinná
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
hustých	hustý	k2eAgInPc6d1	hustý
lesích	les	k1gInPc6	les
a	a	k8xC	a
mokřinách	mokřina	k1gFnPc6	mokřina
i	i	k8xC	i
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
misionář	misionář	k1gMnSc1	misionář
Thomas	Thomas	k1gMnSc1	Thomas
Staughton	Staughton	k1gInSc4	Staughton
Savage	Savag	k1gInSc2	Savag
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Jeffries	Jeffries	k1gMnSc1	Jeffries
Wyman	Wyman	k1gMnSc1	Wyman
poprvé	poprvé	k6eAd1	poprvé
popsali	popsat	k5eAaPmAgMnP	popsat
Gorilu	gorila	k1gFnSc4	gorila
západní	západní	k2eAgFnSc4d1	západní
(	(	kIx(	(
<g/>
nazvali	nazvat	k5eAaBmAgMnP	nazvat
ji	on	k3xPp3gFnSc4	on
Gorilla	Gorilla	k1gFnSc1	Gorilla
Troglodytes	Troglodytes	k1gMnSc1	Troglodytes
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
podle	podle	k7c2	podle
exempláře	exemplář	k1gInSc2	exemplář
získaného	získaný	k2eAgInSc2d1	získaný
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
Gorillai	Gorillai	k1gNnSc2	Gorillai
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kmen	kmen	k1gInSc1	kmen
chlupatých	chlupatý	k2eAgFnPc2d1	chlupatá
žen	žena	k1gFnPc2	žena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
popsaném	popsaný	k2eAgNnSc6d1	popsané
kartaginským	kartaginský	k2eAgMnSc7d1	kartaginský
objevitelem	objevitel	k1gMnSc7	objevitel
Hannonem	Hannon	k1gMnSc7	Hannon
Mořeplavcem	mořeplavec	k1gMnSc7	mořeplavec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
480	[number]	k4	480
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
území	území	k1gNnSc4	území
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Evoluce	evoluce	k1gFnSc1	evoluce
a	a	k8xC	a
klasifikace	klasifikace	k1gFnSc1	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Nejbližším	blízký	k2eAgMnPc3d3	nejbližší
příbuzným	příbuzný	k1gMnPc3	příbuzný
gorily	gorila	k1gFnSc2	gorila
jsou	být	k5eAaImIp3nP	být
šimpanzi	šimpanz	k1gMnPc1	šimpanz
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
gorily	gorila	k1gFnPc1	gorila
oddělily	oddělit	k5eAaPmAgFnP	oddělit
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
sedmi	sedm	k4xCc7	sedm
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
genom	genom	k1gInSc1	genom
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jen	jen	k9	jen
o	o	k7c4	o
1,6	[number]	k4	1,6
%	%	kIx~	%
od	od	k7c2	od
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
genů	gen	k1gInPc2	gen
gorily	gorila	k1gFnSc2	gorila
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
rozdíl	rozdíl	k1gInSc1	rozdíl
však	však	k9	však
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
kopií	kopie	k1gFnPc2	kopie
stejných	stejný	k2eAgInPc2d1	stejný
genů	gen	k1gInPc2	gen
každý	každý	k3xTgInSc4	každý
genom	genom	k1gInSc4	genom
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
nedávna	nedávno	k1gNnSc2	nedávno
se	se	k3xPyFc4	se
gorily	gorila	k1gFnPc1	gorila
řadily	řadit	k5eAaImAgFnP	řadit
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
druhu	druh	k1gInSc2	druh
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
:	:	kIx,	:
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
západní	západní	k2eAgFnSc1d1	západní
<g/>
,	,	kIx,	,
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
východní	východní	k2eAgFnSc1d1	východní
a	a	k8xC	a
gorila	gorila	k1gFnSc1	gorila
horská	horský	k2eAgFnSc1d1	horská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
panuje	panovat	k5eAaImIp3nS	panovat
téměř	téměř	k6eAd1	téměř
konsensus	konsensus	k1gInSc4	konsensus
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
gorila	gorila	k1gFnSc1	gorila
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primatologové	Primatolog	k1gMnPc1	Primatolog
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
různými	různý	k2eAgFnPc7d1	různá
populacemi	populace	k1gFnPc7	populace
goril	gorila	k1gFnPc2	gorila
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
a	a	k8xC	a
poddruhy	poddruh	k1gInPc1	poddruh
uvedené	uvedený	k2eAgInPc1d1	uvedený
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
klasifikaci	klasifikace	k1gFnSc4	klasifikace
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
shoduje	shodovat	k5eAaImIp3nS	shodovat
většina	většina	k1gFnSc1	většina
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
gorila	gorila	k1gFnSc1	gorila
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
západní	západní	k2eAgFnSc1d1	západní
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
nigerijská	nigerijský	k2eAgFnSc1d1	nigerijská
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k6eAd1	gorilla
diehli	diehnout	k5eAaPmAgMnP	diehnout
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
beringei	beringe	k1gFnSc2	beringe
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
beringei	beringe	k1gFnSc2	beringe
beringei	beringe	k1gFnSc2	beringe
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
beringei	beringe	k1gFnSc2	beringe
graueri	grauer	k1gFnSc2	grauer
<g/>
)	)	kIx)	)
<g/>
Někteří	některý	k3yIgMnPc1	některý
biologové	biolog	k1gMnPc1	biolog
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
gorilu	gorila	k1gFnSc4	gorila
východní	východní	k2eAgFnSc4d1	východní
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
poddruh	poddruh	k1gInSc1	poddruh
zatím	zatím	k6eAd1	zatím
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc4	název
gorila	gorila	k1gFnSc1	gorila
bwindská	bwindský	k2eAgFnSc1d1	bwindský
a	a	k8xC	a
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
nového	nový	k2eAgInSc2d1	nový
poddruhu	poddruh	k1gInSc2	poddruh
by	by	kYmCp3nS	by
patřila	patřit	k5eAaImAgFnS	patřit
populace	populace	k1gFnSc1	populace
asi	asi	k9	asi
350	[number]	k4	350
horských	horský	k2eAgFnPc2d1	horská
goril	gorila	k1gFnPc2	gorila
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
lesích	les	k1gInPc6	les
Bwindi	Bwind	k1gMnPc1	Bwind
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nová	nový	k2eAgFnSc1d1	nová
klasifikace	klasifikace	k1gFnSc1	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Systematika	systematika	k1gFnSc1	systematika
zařazení	zařazení	k1gNnSc2	zařazení
goril	gorila	k1gFnPc2	gorila
se	se	k3xPyFc4	se
několikráte	několikráte	k6eAd1	několikráte
pozměnila	pozměnit	k5eAaPmAgFnS	pozměnit
<g/>
,	,	kIx,	,
vždyť	vždyť	k9	vždyť
původně	původně	k6eAd1	původně
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
nebyly	být	k5eNaImAgInP	být
vůbec	vůbec	k9	vůbec
sjednoceny	sjednocen	k2eAgInPc1d1	sjednocen
české	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
prostě	prostě	k6eAd1	prostě
kopírovaly	kopírovat	k5eAaImAgFnP	kopírovat
anglické	anglický	k2eAgFnPc1d1	anglická
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
používalo	používat	k5eAaImAgNnS	používat
např.	např.	kA	např.
toto	tento	k3xDgNnSc4	tento
zatřídění	zatřídění	k1gNnSc4	zatřídění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
gorila	gorila	k1gFnSc1	gorila
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gMnSc1	Gorilla
<g/>
)	)	kIx)	)
I.	I.	kA	I.
Geoffroy	Geoffroa	k1gFnSc2	Geoffroa
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
Savage	Savage	k1gNnSc1	Savage
<g/>
,	,	kIx,	,
1847	[number]	k4	1847
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
Savage	Savage	k1gInSc1	Savage
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
gorilla	gorilla	k6eAd1	gorilla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
nigerijská	nigerijský	k2eAgFnSc1d1	nigerijská
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
Savage	Savage	k1gInSc1	Savage
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
diehli	diehnout	k5eAaPmAgMnP	diehnout
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
bwindská	bwindský	k2eAgFnSc1d1	bwindský
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
Savage	Savage	k1gInSc1	Savage
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
bwindi	bwind	k1gMnPc1	bwind
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
beringei	beringe	k1gFnSc2	beringe
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
beringei	beringe	k1gFnSc2	beringe
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
beringei	beringei	k6eAd1	beringei
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
beringei	beringe	k1gFnSc2	beringe
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
graueri	graueri	k1gNnSc1	graueri
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
však	však	k9	však
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Václav	Václav	k1gMnSc1	Václav
Vančata	Vanče	k1gNnPc1	Vanče
<g/>
,	,	kIx,	,
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
CSc.	CSc.	kA	CSc.
<g/>
,	,	kIx,	,
evoluční	evoluční	k2eAgMnSc1d1	evoluční
antropolog	antropolog	k1gMnSc1	antropolog
<g/>
,	,	kIx,	,
paleoantropolog	paleoantropolog	k1gMnSc1	paleoantropolog
a	a	k8xC	a
primatolog	primatolog	k1gMnSc1	primatolog
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
Antropologického	antropologický	k2eAgNnSc2d1	antropologické
oddělení	oddělení	k1gNnSc2	oddělení
a	a	k8xC	a
zástupce	zástupce	k1gMnSc2	zástupce
vedoucího	vedoucí	k1gMnSc2	vedoucí
katedry	katedra	k1gFnSc2	katedra
na	na	k7c6	na
Katedře	katedra	k1gFnSc6	katedra
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
ekologické	ekologický	k2eAgFnSc2d1	ekologická
výchovy	výchova	k1gFnSc2	výchova
Pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
University	universita	k1gFnSc2	universita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nové	nový	k2eAgNnSc1d1	nové
revidované	revidovaný	k2eAgNnSc1d1	revidované
systematické	systematický	k2eAgNnSc1d1	systematické
zařazení	zařazení	k1gNnSc1	zařazení
goril	gorila	k1gFnPc2	gorila
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
přezkoumání	přezkoumání	k1gNnPc2	přezkoumání
starších	starý	k2eAgInPc2d2	starší
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
z	z	k7c2	z
muzejních	muzejní	k2eAgFnPc2d1	muzejní
sbírek	sbírka	k1gFnPc2	sbírka
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
nových	nový	k2eAgFnPc2d1	nová
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
molekulární	molekulární	k2eAgFnSc6d1	molekulární
genetice	genetika	k1gFnSc6	genetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
gorila	gorila	k1gFnSc1	gorila
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gMnSc1	Gorilla
<g/>
)	)	kIx)	)
I.	I.	kA	I.
Geoffroy	Geoffroa	k1gFnSc2	Geoffroa
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
Savage	Savage	k1gNnSc1	Savage
<g/>
,	,	kIx,	,
1847	[number]	k4	1847
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
západní	západní	k2eAgFnSc1d1	západní
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
Savage	Savage	k1gInSc1	Savage
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
gorilla	gorilla	k6eAd1	gorilla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
graueri	graueri	k1gNnSc1	graueri
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
nigerijská	nigerijský	k2eAgFnSc1d1	nigerijská
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
gorilla	gorilla	k1gFnSc1	gorilla
<g/>
)	)	kIx)	)
Savage	Savage	k1gInSc1	Savage
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
diehli	diehnout	k5eAaPmAgMnP	diehnout
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
beringei	beringe	k1gFnSc2	beringe
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
</s>
</p>
<p>
<s>
gorila	gorila	k1gFnSc1	gorila
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
Gorilla	Gorilla	k1gFnSc1	Gorilla
beringei	beringe	k1gFnSc2	beringe
<g/>
)	)	kIx)	)
Matschie	Matschie	k1gFnSc1	Matschie
<g/>
,	,	kIx,	,
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
beringei	beringei	k6eAd1	beringei
<g/>
)	)	kIx)	)
Na	na	k7c4	na
definitivní	definitivní	k2eAgInPc4d1	definitivní
závěry	závěr	k1gInPc4	závěr
nutno	nutno	k6eAd1	nutno
ještě	ještě	k9	ještě
chvíli	chvíle	k1gFnSc4	chvíle
počkat	počkat	k5eAaPmF	počkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Gorily	gorila	k1gFnPc1	gorila
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
chůzí	chůze	k1gFnSc7	chůze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
o	o	k7c4	o
klouby	kloub	k1gInPc4	kloub
předních	přední	k2eAgFnPc2d1	přední
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
měří	měřit	k5eAaImIp3nP	měřit
165	[number]	k4	165
<g/>
–	–	k?	–
<g/>
175	[number]	k4	175
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
140	[number]	k4	140
<g/>
-	-	kIx~	-
<g/>
204,5	[number]	k4	204,5
kg	kg	kA	kg
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
zaznamenán	zaznamenat	k5eAaPmNgMnS	zaznamenat
i	i	k9	i
jedinec	jedinec	k1gMnSc1	jedinec
vysoký	vysoký	k2eAgMnSc1d1	vysoký
přes	přes	k7c4	přes
183	[number]	k4	183
cm	cm	kA	cm
a	a	k8xC	a
vážící	vážící	k2eAgInSc1d1	vážící
225	[number]	k4	225
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Dospělé	dospělý	k2eAgFnPc1d1	dospělá
samice	samice	k1gFnPc1	samice
často	často	k6eAd1	často
váží	vážit	k5eAaImIp3nP	vážit
jen	jen	k9	jen
polovinu	polovina	k1gFnSc4	polovina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
plně	plně	k6eAd1	plně
vyvinutého	vyvinutý	k2eAgInSc2d1	vyvinutý
samce	samec	k1gInSc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
někdy	někdy	k6eAd1	někdy
obézní	obézní	k2eAgFnSc2d1	obézní
gorily	gorila	k1gFnSc2	gorila
přesáhnou	přesáhnout	k5eAaPmIp3nP	přesáhnout
270	[number]	k4	270
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
obličeje	obličej	k1gInSc2	obličej
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
podkusem	podkus	k1gMnSc7	podkus
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jejich	jejich	k3xOp3gFnSc7	jejich
spodní	spodní	k2eAgFnSc1d1	spodní
čelist	čelist	k1gFnSc1	čelist
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
čelist	čelist	k1gFnSc4	čelist
horní	horní	k2eAgFnSc4d1	horní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
gorile	gorila	k1gFnSc3	gorila
západní	západní	k2eAgFnSc1d1	západní
je	být	k5eAaImIp3nS	být
gorila	gorila	k1gFnSc1	gorila
východní	východní	k2eAgFnSc1d1	východní
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
gorila	gorila	k1gFnSc1	gorila
horská	horský	k2eAgFnSc1d1	horská
<g/>
)	)	kIx)	)
zbarvena	zbarvit	k5eAaPmNgFnS	zbarvit
tmavěji	tmavě	k6eAd2	tmavě
<g/>
.	.	kIx.	.
</s>
<s>
Gorila	gorila	k1gFnSc1	gorila
horská	horský	k2eAgFnSc1d1	horská
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
gorilích	gorilí	k2eAgInPc2d1	gorilí
poddruhů	poddruh	k1gInPc2	poddruh
nejmohutnější	mohutný	k2eAgFnSc4d3	nejmohutnější
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
nížinná	nížinný	k2eAgFnSc1d1	nížinná
gorila	gorila	k1gFnSc1	gorila
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zbarvena	zbarvit	k5eAaPmNgFnS	zbarvit
šedě	šedě	k6eAd1	šedě
nebo	nebo	k8xC	nebo
hnědě	hnědě	k6eAd1	hnědě
<g/>
,	,	kIx,	,
s	s	k7c7	s
čelem	čelo	k1gNnSc7	čelo
do	do	k7c2	do
červena	červeno	k1gNnSc2	červeno
<g/>
.	.	kIx.	.
</s>
<s>
Gorila	gorila	k1gFnSc1	gorila
nížinná	nížinný	k2eAgFnSc1d1	nížinná
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
gorilou	gorila	k1gFnSc7	gorila
horskou	horský	k2eAgFnSc7d1	horská
hbitější	hbitý	k2eAgFnSc7d2	hbitější
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
robustní	robustní	k2eAgNnSc1d1	robustní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
gorily	gorila	k1gFnPc1	gorila
mají	mít	k5eAaImIp3nP	mít
krevní	krevní	k2eAgFnSc4d1	krevní
skupinu	skupina	k1gFnSc4	skupina
B	B	kA	B
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
gorila	gorila	k1gFnSc1	gorila
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
otisky	otisk	k1gInPc7	otisk
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etologie	etologie	k1gFnSc2	etologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sociální	sociální	k2eAgNnPc1d1	sociální
chování	chování	k1gNnPc1	chování
===	===	k?	===
</s>
</p>
<p>
<s>
Stříbrohřbetý	Stříbrohřbetý	k2eAgMnSc1d1	Stříbrohřbetý
samec	samec	k1gMnSc1	samec
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Silverback	Silverback	k1gInSc1	Silverback
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
12	[number]	k4	12
letech	let	k1gInPc6	let
věku	věk	k1gInSc2	věk
objeví	objevit	k5eAaPmIp3nS	objevit
viditelný	viditelný	k2eAgInSc1d1	viditelný
pruh	pruh	k1gInSc1	pruh
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
srsti	srst	k1gFnSc2	srst
na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
výrazné	výrazný	k2eAgInPc4d1	výrazný
špičáky	špičák	k1gInPc4	špičák
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
i	i	k9	i
během	během	k7c2	během
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrohřbetí	Stříbrohřbetý	k2eAgMnPc1d1	Stříbrohřbetý
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
silní	silný	k2eAgMnPc1d1	silný
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
dominantními	dominantní	k2eAgInPc7d1	dominantní
sklony	sklon	k1gInPc7	sklon
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
vedou	vést	k5eAaImIp3nP	vést
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
30	[number]	k4	30
jedinců	jedinec	k1gMnPc2	jedinec
a	a	k8xC	a
vůdce	vůdce	k1gMnSc2	vůdce
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
těší	těšit	k5eAaImIp3nS	těšit
největší	veliký	k2eAgFnSc2d3	veliký
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
urovnává	urovnávat	k5eAaImIp3nS	urovnávat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
určuje	určovat	k5eAaImIp3nS	určovat
přesuny	přesun	k1gInPc4	přesun
skupiny	skupina	k1gFnSc2	skupina
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
bezpečí	bezpečí	k1gNnPc4	bezpečí
a	a	k8xC	a
prosperitu	prosperita	k1gFnSc4	prosperita
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černohřbetý	Černohřbetý	k2eAgMnSc1d1	Černohřbetý
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
sexuálně	sexuálně	k6eAd1	sexuálně
vyspělý	vyspělý	k2eAgInSc4d1	vyspělý
samec	samec	k1gInSc4	samec
do	do	k7c2	do
zhruba	zhruba	k6eAd1	zhruba
11	[number]	k4	11
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Černohřbetí	Černohřbetit	k5eAaPmIp3nS	Černohřbetit
samci	samec	k1gMnSc3	samec
vůdci	vůdce	k1gMnSc3	vůdce
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
začínají	začínat	k5eAaImIp3nP	začínat
opouštět	opouštět	k5eAaImF	opouštět
svou	svůj	k3xOyFgFnSc4	svůj
rodnou	rodný	k2eAgFnSc4d1	rodná
skupinu	skupina	k1gFnSc4	skupina
kolem	kolem	k7c2	kolem
svého	svůj	k3xOyFgInSc2	svůj
jedenáctého	jedenáctý	k4xOgInSc2	jedenáctý
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
sami	sám	k3xTgMnPc1	sám
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skupince	skupinka	k1gFnSc6	skupinka
dalších	další	k1gNnPc2	další
takových	takový	k3xDgMnPc2	takový
samců	samec	k1gMnPc2	samec
obvykle	obvykle	k6eAd1	obvykle
2-5	[number]	k4	2-5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
zaujmout	zaujmout	k5eAaPmF	zaujmout
samice	samice	k1gFnPc4	samice
a	a	k8xC	a
založit	založit	k5eAaPmF	založit
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
potomky	potomek	k1gMnPc4	potomek
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
matky	matka	k1gFnPc1	matka
<g/>
,	,	kIx,	,
po	po	k7c6	po
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
letech	let	k1gInPc6	let
své	svůj	k3xOyFgFnSc2	svůj
mladé	mladá	k1gFnSc2	mladá
odstavují	odstavovat	k5eAaImIp3nP	odstavovat
<g/>
.	.	kIx.	.
</s>
<s>
Péči	péče	k1gFnSc4	péče
o	o	k7c4	o
tyto	tento	k3xDgMnPc4	tento
odstavené	odstavený	k2eAgMnPc4d1	odstavený
jedince	jedinec	k1gMnPc4	jedinec
pak	pak	k6eAd1	pak
přebírá	přebírat	k5eAaImIp3nS	přebírat
vůdce	vůdce	k1gMnPc4	vůdce
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
péče	péče	k1gFnSc1	péče
nikdy	nikdy	k6eAd1	nikdy
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
úrovně	úroveň	k1gFnPc4	úroveň
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
malá	malý	k2eAgNnPc4d1	malé
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
mladých	mladý	k2eAgMnPc2d1	mladý
samců	samec	k1gMnPc2	samec
skupiny	skupina	k1gFnSc2	skupina
nebo	nebo	k8xC	nebo
nějaký	nějaký	k3yIgInSc1	nějaký
cizí	cizí	k2eAgInSc1d1	cizí
samec	samec	k1gInSc1	samec
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
stříbrohřbetého	stříbrohřbetý	k2eAgMnSc4d1	stříbrohřbetý
samce	samec	k1gMnSc4	samec
k	k	k7c3	k
boji	boj	k1gInSc3	boj
o	o	k7c4	o
vůdčí	vůdčí	k2eAgNnSc4d1	vůdčí
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
vydává	vydávat	k5eAaImIp3nS	vydávat
hlasité	hlasitý	k2eAgInPc4d1	hlasitý
skřeky	skřek	k1gInPc4	skřek
<g/>
,	,	kIx,	,
buší	bušit	k5eAaImIp3nS	bušit
do	do	k7c2	do
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
,	,	kIx,	,
láme	lámat	k5eAaImIp3nS	lámat
větve	větev	k1gFnPc4	větev
<g/>
,	,	kIx,	,
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
pak	pak	k6eAd1	pak
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
vyzyvatel	vyzyvatel	k?	vyzyvatel
neustoupí	ustoupit	k5eNaPmIp3nS	ustoupit
<g/>
)	)	kIx)	)
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mladší	mladý	k2eAgMnSc1d2	mladší
samec	samec	k1gMnSc1	samec
uspěje	uspět	k5eAaPmIp3nS	uspět
a	a	k8xC	a
převezme	převzít	k5eAaPmIp3nS	převzít
vedení	vedení	k1gNnSc1	vedení
skupiny	skupina	k1gFnSc2	skupina
od	od	k7c2	od
staršího	starý	k2eAgMnSc2d2	starší
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vůdce	vůdce	k1gMnSc1	vůdce
skupiny	skupina	k1gFnSc2	skupina
zahyne	zahynout	k5eAaPmIp3nS	zahynout
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
,	,	kIx,	,
nehodě	nehoda	k1gFnSc3	nehoda
nebo	nebo	k8xC	nebo
boji	boj	k1gInSc3	boj
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
rozptýlí	rozptýlit	k5eAaPmIp3nP	rozptýlit
a	a	k8xC	a
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
skupinám	skupina	k1gFnPc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
celou	celý	k2eAgFnSc4d1	celá
skupinu	skupina	k1gFnSc4	skupina
převezme	převzít	k5eAaPmIp3nS	převzít
jiný	jiný	k2eAgMnSc1d1	jiný
samec	samec	k1gMnSc1	samec
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
často	často	k6eAd1	často
zabije	zabít	k5eAaPmIp3nS	zabít
mláďata	mládě	k1gNnPc1	mládě
předchozího	předchozí	k2eAgInSc2d1	předchozí
vůdce	vůdce	k1gMnPc4	vůdce
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Jakožto	jakožto	k8xS	jakožto
býložravci	býložravec	k1gMnPc1	býložravec
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
gorily	gorila	k1gFnPc1	gorila
živí	živit	k5eAaImIp3nP	živit
hlavně	hlavně	k6eAd1	hlavně
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
listy	list	k1gInPc7	list
a	a	k8xC	a
výhonky	výhonek	k1gInPc7	výhonek
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
pozřou	pozřít	k5eAaPmIp3nP	pozřít
i	i	k9	i
nějaký	nějaký	k3yIgInSc4	nějaký
malý	malý	k2eAgInSc4d1	malý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
gorily	gorila	k1gFnPc1	gorila
věnují	věnovat	k5eAaImIp3nP	věnovat
konzumací	konzumace	k1gFnSc7	konzumace
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
mohutné	mohutný	k2eAgInPc1d1	mohutný
čelistní	čelistní	k2eAgInPc1d1	čelistní
svaly	sval	k1gInPc1	sval
a	a	k8xC	a
silné	silný	k2eAgInPc1d1	silný
zuby	zub	k1gInPc1	zub
jim	on	k3xPp3gFnPc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rozmělňovat	rozmělňovat	k5eAaImF	rozmělňovat
tuhé	tuhý	k2eAgFnPc1d1	tuhá
rostliny	rostlina	k1gFnPc1	rostlina
jako	jako	k9	jako
například	například	k6eAd1	například
bambus	bambus	k1gInSc4	bambus
<g/>
.	.	kIx.	.
</s>
<s>
Gorily	gorila	k1gFnPc1	gorila
nížinné	nížinný	k2eAgFnPc1d1	nížinná
se	se	k3xPyFc4	se
krmí	krmit	k5eAaImIp3nP	krmit
hlavně	hlavně	k6eAd1	hlavně
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
gorily	gorila	k1gFnPc1	gorila
horské	horský	k2eAgFnPc1d1	horská
spíše	spíše	k9	spíše
bylinami	bylina	k1gFnPc7	bylina
<g/>
,	,	kIx,	,
stonky	stonek	k1gInPc7	stonek
a	a	k8xC	a
kořínky	kořínek	k1gInPc7	kořínek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
březosti	březost	k1gFnSc2	březost
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
8,5	[number]	k4	8,5
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
porody	porod	k1gInPc4	porod
uplynou	uplynout	k5eAaPmIp3nP	uplynout
obvykle	obvykle	k6eAd1	obvykle
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
roky	rok	k1gInPc4	rok
a	a	k8xC	a
po	po	k7c4	po
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
matky	matka	k1gFnPc1	matka
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
starají	starat	k5eAaImIp3nP	starat
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
dospívají	dospívat	k5eAaImIp3nP	dospívat
mezi	mezi	k7c7	mezi
10	[number]	k4	10
a	a	k8xC	a
12	[number]	k4	12
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
okolo	okolo	k7c2	okolo
11	[number]	k4	11
až	až	k9	až
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Gorily	gorila	k1gFnPc1	gorila
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
dallaském	dallaský	k2eAgNnSc6d1	dallaský
ZOO	zoo	k1gNnSc6	zoo
se	se	k3xPyFc4	se
gorila	gorila	k1gFnSc1	gorila
Jenny	Jenna	k1gFnSc2	Jenna
dožila	dožít	k5eAaPmAgFnS	dožít
55	[number]	k4	55
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgFnSc7d3	nejstarší
gorilou	gorila	k1gFnSc7	gorila
byla	být	k5eAaImAgFnS	být
gorila	gorila	k1gFnSc1	gorila
Colo	cola	k1gFnSc5	cola
ze	z	k7c2	z
ZOO	zoo	k1gFnPc2	zoo
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Columbusu	Columbus	k1gInSc6	Columbus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
a	a	k8xC	a
dožila	dožít	k5eAaPmAgFnS	dožít
se	se	k3xPyFc4	se
tak	tak	k9	tak
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
gorila	gorila	k1gFnSc1	gorila
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
první	první	k4xOgFnSc7	první
gorilou	gorila	k1gFnSc7	gorila
nížinnou	nížinný	k2eAgFnSc7d1	nížinná
narozenou	narozený	k2eAgFnSc7d1	narozená
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Gorily	gorila	k1gFnPc1	gorila
občas	občas	k6eAd1	občas
provozují	provozovat	k5eAaImIp3nP	provozovat
sex	sex	k1gInSc4	sex
v	v	k7c6	v
poloze	poloha	k1gFnSc6	poloha
tváří	tvář	k1gFnPc2	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
u	u	k7c2	u
šimpanze	šimpanz	k1gMnSc2	šimpanz
bonobo	bonoba	k1gFnSc5	bonoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Inteligence	inteligence	k1gFnSc2	inteligence
==	==	k?	==
</s>
</p>
<p>
<s>
Gorily	gorila	k1gFnPc1	gorila
jsou	být	k5eAaImIp3nP	být
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
inteligentní	inteligentní	k2eAgMnPc4d1	inteligentní
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
gorila	gorila	k1gFnSc1	gorila
Koko	koka	k1gFnSc5	koka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naučila	naučit	k5eAaPmAgFnS	naučit
část	část	k1gFnSc1	část
znakového	znakový	k2eAgInSc2d1	znakový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Používání	používání	k1gNnSc1	používání
nástrojů	nástroj	k1gInPc2	nástroj
===	===	k?	===
</s>
</p>
<p>
<s>
Tým	tým	k1gInSc1	tým
vedený	vedený	k2eAgInSc1d1	vedený
Thomasem	Thomas	k1gMnSc7	Thomas
Breuerem	Breuer	k1gMnSc7	Breuer
z	z	k7c2	z
Wildlife	Wildlif	k1gInSc5	Wildlif
Conservation	Conservation	k1gInSc4	Conservation
Society	societa	k1gFnSc2	societa
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
v	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
gorily	gorila	k1gFnPc1	gorila
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Nouabalé-Ndoki	Nouabalé-Ndok	k1gFnSc2	Nouabalé-Ndok
v	v	k7c6	v
Kongu	Kongo	k1gNnSc6	Kongo
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
gorilí	gorilí	k2eAgFnSc3d1	gorilí
samici	samice	k1gFnSc3	samice
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
používá	používat	k5eAaImIp3nS	používat
klacek	klacek	k1gInSc4	klacek
ke	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
hloubky	hloubka	k1gFnSc2	hloubka
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
překračování	překračování	k1gNnSc6	překračování
bažiny	bažina	k1gFnSc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
samice	samice	k1gFnSc1	samice
používala	používat	k5eAaImAgFnS	používat
pahýl	pahýl	k1gInSc4	pahýl
stromu	strom	k1gInSc2	strom
jako	jako	k8xC	jako
most	most	k1gInSc4	most
a	a	k8xC	a
opěru	opěra	k1gFnSc4	opěra
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Tj.	tj.	kA	tj.
podle	podle	k7c2	podle
našich	náš	k3xOp1gFnPc2	náš
znalostí	znalost	k1gFnPc2	znalost
všichni	všechen	k3xTgMnPc1	všechen
lidoopi	lidoop	k1gMnPc1	lidoop
využívají	využívat	k5eAaImIp3nP	využívat
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Kongu	Kongo	k1gNnSc6	Kongo
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
stará	starat	k5eAaImIp3nS	starat
gorila	gorila	k1gFnSc1	gorila
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
používala	používat	k5eAaImAgFnS	používat
kámen	kámen	k1gInSc4	kámen
k	k	k7c3	k
rozbíjení	rozbíjení	k1gNnSc3	rozbíjení
palmových	palmový	k2eAgInPc2d1	palmový
ořechů	ořech	k1gInPc2	ořech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnPc1	první
takové	takový	k3xDgNnSc1	takový
pozorování	pozorování	k1gNnSc1	pozorování
gorily	gorila	k1gFnSc2	gorila
<g/>
,	,	kIx,	,
u	u	k7c2	u
šimpanzů	šimpanz	k1gMnPc2	šimpanz
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
nástrojů	nástroj	k1gInPc2	nástroj
známo	znám	k2eAgNnSc1d1	známo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
šimpanzi	šimpanz	k1gMnPc1	šimpanz
používají	používat	k5eAaImIp3nP	používat
klacek	klacek	k1gInSc4	klacek
při	při	k7c6	při
vytahování	vytahování	k1gNnSc6	vytahování
termitů	termit	k1gMnPc2	termit
z	z	k7c2	z
termitiště	termitiště	k1gNnSc2	termitiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
lidoopi	lidoop	k1gMnPc1	lidoop
jsou	být	k5eAaImIp3nP	být
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
schopností	schopnost	k1gFnPc2	schopnost
uchopit	uchopit	k5eAaPmF	uchopit
předměty	předmět	k1gInPc4	předmět
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
přesností	přesnost	k1gFnSc7	přesnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnPc4	on
mohli	moct	k5eAaImAgMnP	moct
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
jako	jako	k9	jako
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
příhodnou	příhodný	k2eAgFnSc4d1	příhodná
větev	větev	k1gFnSc4	větev
použijí	použít	k5eAaPmIp3nP	použít
jako	jako	k8xC	jako
kyj	kyj	k1gInSc4	kyj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výzkum	výzkum	k1gInSc1	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
gorila	gorila	k1gFnSc1	gorila
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
kartáginského	kartáginský	k2eAgMnSc2d1	kartáginský
objevitele	objevitel	k1gMnSc2	objevitel
Hannona	Hannon	k1gMnSc2	Hannon
Mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
při	při	k7c6	při
výpravě	výprava	k1gFnSc6	výprava
k	k	k7c3	k
západnímu	západní	k2eAgNnSc3d1	západní
africkému	africký	k2eAgNnSc3d1	africké
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
"	"	kIx"	"
<g/>
hrubé	hrubý	k2eAgMnPc4d1	hrubý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
byly	být	k5eAaImAgFnP	být
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
byla	být	k5eAaImAgFnS	být
zarostlá	zarostlý	k2eAgFnSc1d1	zarostlá
a	a	k8xC	a
která	který	k3yIgNnPc4	který
průvodci	průvodce	k1gMnPc1	průvodce
nazývali	nazývat	k5eAaImAgMnP	nazývat
Gorilami	gorila	k1gFnPc7	gorila
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
zoologického	zoologický	k2eAgInSc2d1	zoologický
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
setkali	setkat	k5eAaPmAgMnP	setkat
opravdu	opravdu	k6eAd1	opravdu
s	s	k7c7	s
gorilami	gorila	k1gFnPc7	gorila
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nějakým	nějaký	k3yIgInSc7	nějaký
jiným	jiný	k2eAgInSc7d1	jiný
živočišným	živočišný	k2eAgInSc7d1	živočišný
druhem	druh	k1gInSc7	druh
či	či	k8xC	či
snad	snad	k9	snad
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
misionář	misionář	k1gMnSc1	misionář
Thomas	Thomas	k1gMnSc1	Thomas
Staughton	Staughton	k1gInSc4	Staughton
Savage	Savage	k1gNnSc2	Savage
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgInPc4	první
vzorky	vzorek	k1gInPc4	vzorek
(	(	kIx(	(
<g/>
lebku	lebka	k1gFnSc4	lebka
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
kosti	kost	k1gFnPc4	kost
<g/>
)	)	kIx)	)
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Savage	Savage	k6eAd1	Savage
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přírodovědcem	přírodovědec	k1gMnSc7	přírodovědec
Jeffriesem	Jeffries	k1gMnSc7	Jeffries
Wymanem	Wyman	k1gMnSc7	Wyman
vědecky	vědecky	k6eAd1	vědecky
popsali	popsat	k5eAaPmAgMnP	popsat
druh	druh	k1gMnSc1	druh
Troglodytes	Troglodytes	k1gMnSc1	Troglodytes
gorilla	gorilla	k1gMnSc1	gorilla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
v	v	k7c4	v
Proceedings	Proceedings	k1gInSc4	Proceedings
of	of	k?	of
the	the	k?	the
Boston	Boston	k1gInSc1	Boston
Society	societa	k1gFnSc2	societa
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gInPc7	Histor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
gorila	gorila	k1gFnSc1	gorila
západní	západní	k2eAgFnSc1d1	západní
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
a	a	k8xC	a
poddruhy	poddruh	k1gInPc1	poddruh
gorily	gorila	k1gFnSc2	gorila
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
v	v	k7c6	v
několika	několik	k4yIc2	několik
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spatřil	spatřit	k5eAaPmAgInS	spatřit
živou	živý	k2eAgFnSc4d1	živá
gorilu	gorila	k1gFnSc4	gorila
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
cestovatel	cestovatel	k1gMnSc1	cestovatel
Paul	Paul	k1gMnSc1	Paul
du	du	k?	du
Chaillu	Chailla	k1gMnSc4	Chailla
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
procestoval	procestovat	k5eAaPmAgMnS	procestovat
rovníkovou	rovníkový	k2eAgFnSc4d1	Rovníková
Afriku	Afrika	k1gFnSc4	Afrika
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1856	[number]	k4	1856
a	a	k8xC	a
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
přivezl	přivézt	k5eAaPmAgInS	přivézt
mrtvé	mrtvý	k2eAgInPc4d1	mrtvý
exempláře	exemplář	k1gInPc4	exemplář
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
systematický	systematický	k2eAgInSc1d1	systematický
výzkum	výzkum	k1gInSc1	výzkum
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Carl	Carl	k1gMnSc1	Carl
Akeley	Akelea	k1gFnSc2	Akelea
z	z	k7c2	z
Amerického	americký	k2eAgNnSc2d1	americké
přírodovědného	přírodovědný	k2eAgNnSc2d1	Přírodovědné
muzea	muzeum	k1gNnSc2	muzeum
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
gorilu	gorila	k1gFnSc4	gorila
ulovil	ulovit	k5eAaPmAgMnS	ulovit
a	a	k8xC	a
vycpal	vycpat	k5eAaPmAgMnS	vycpat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
ho	on	k3xPp3gInSc4	on
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Mary	Mary	k1gFnSc1	Mary
Bradleyová	Bradleyová	k1gFnSc1	Bradleyová
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výpravě	výprava	k1gFnSc6	výprava
Bradleyová	Bradleyová	k1gFnSc1	Bradleyová
napsala	napsat	k5eAaPmAgFnS	napsat
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
gorily	gorila	k1gFnSc2	gorila
(	(	kIx(	(
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Gorilla	Gorilla	k1gMnSc1	Gorilla
Trail	Trail	k1gMnSc1	Trail
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Bradleyová	Bradleyová	k1gFnSc1	Bradleyová
stala	stát	k5eAaPmAgFnS	stát
zastáncem	zastánce	k1gMnSc7	zastánce
ochrany	ochrana	k1gFnSc2	ochrana
goril	gorila	k1gFnPc2	gorila
a	a	k8xC	a
napsala	napsat	k5eAaPmAgFnS	napsat
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
Robert	Robert	k1gMnSc1	Robert
Yerkes	Yerkes	k1gMnSc1	Yerkes
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Ava	Ava	k1gFnSc4	Ava
<g/>
,	,	kIx,	,
když	když	k8xS	když
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
vyslali	vyslat	k5eAaPmAgMnP	vyslat
Harolda	Harold	k1gMnSc4	Harold
Birghama	Birgham	k1gMnSc4	Birgham
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
Yerkes	Yerkes	k1gMnSc1	Yerkes
také	také	k9	také
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
lidoopech	lidoop	k1gMnPc6	lidoop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhě	druha	k1gFnSc6	druha
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznačnějších	význačný	k2eAgMnPc2d3	nejvýznačnější
vědců	vědec	k1gMnPc2	vědec
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
výzkumem	výzkum	k1gInSc7	výzkum
primátů	primát	k1gInPc2	primát
George	Georg	k1gFnSc2	Georg
Schaller	Schaller	k1gMnSc1	Schaller
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
systematický	systematický	k2eAgInSc1d1	systematický
výzkum	výzkum	k1gInSc1	výzkum
goril	gorila	k1gFnPc2	gorila
horských	horský	k2eAgFnPc2d1	horská
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
významnému	významný	k2eAgInSc3d1	významný
pokroku	pokrok	k1gInSc3	pokrok
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Louise	Louis	k1gMnSc2	Louis
Leakeyho	Leakey	k1gMnSc2	Leakey
a	a	k8xC	a
National	National	k1gMnSc2	National
Geographic	Geographice	k1gFnPc2	Geographice
<g/>
,	,	kIx,	,
když	když	k8xS	když
Dian	Diana	k1gFnPc2	Diana
Fosseyová	Fosseyová	k1gFnSc1	Fosseyová
provedla	provést	k5eAaPmAgFnS	provést
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
ucelenější	ucelený	k2eAgInSc4d2	ucelenější
výzkum	výzkum	k1gInSc4	výzkum
goril	gorila	k1gFnPc2	gorila
horských	horský	k2eAgFnPc2d1	horská
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
publikace	publikace	k1gFnPc1	publikace
Fosseyové	Fosseyová	k1gFnSc2	Fosseyová
vyvrátily	vyvrátit	k5eAaPmAgFnP	vyvrátit
mnoho	mnoho	k6eAd1	mnoho
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ke	k	k7c3	k
gorilám	gorila	k1gFnPc3	gorila
vztahovaly	vztahovat	k5eAaImAgFnP	vztahovat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mýtů	mýtus	k1gInPc2	mýtus
o	o	k7c4	o
násilí	násilí	k1gNnSc4	násilí
goril	gorila	k1gFnPc2	gorila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Oba	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
gorily	gorila	k1gFnSc2	gorila
jsou	být	k5eAaImIp3nP	být
ohrožené	ohrožený	k2eAgFnPc1d1	ohrožená
<g/>
.	.	kIx.	.
</s>
<s>
Gorily	gorila	k1gFnPc1	gorila
byly	být	k5eAaImAgFnP	být
dlouho	dlouho	k6eAd1	dlouho
cílem	cíl	k1gInSc7	cíl
pytláků	pytlák	k1gMnPc2	pytlák
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
gorily	gorila	k1gFnSc2	gorila
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
likvidace	likvidace	k1gFnSc1	likvidace
jejich	jejich	k3xOp3gInSc2	jejich
biotopu	biotop	k1gInSc2	biotop
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
divokou	divoký	k2eAgFnSc7d1	divoká
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
doslova	doslova	k6eAd1	doslova
vyhubil	vyhubit	k5eAaPmAgInS	vyhubit
populaci	populace	k1gFnSc4	populace
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
goril	gorila	k1gFnPc2	gorila
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Odzala	Odzala	k1gFnSc1	Odzala
v	v	k7c6	v
Kongu	Kongo	k1gNnSc6	Kongo
virus	virus	k1gInSc4	virus
Ebola	Ebolo	k1gNnSc2	Ebolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
uveřejněné	uveřejněný	k2eAgFnSc2d1	uveřejněná
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Science	Science	k1gFnSc2	Science
zabila	zabít	k5eAaPmAgFnS	zabít
Ebola	Ebola	k1gFnSc1	Ebola
okolo	okolo	k7c2	okolo
5000	[number]	k4	5000
goril	gorila	k1gFnPc2	gorila
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
přímo	přímo	k6eAd1	přímo
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
komerčním	komerční	k2eAgInSc7d1	komerční
lovem	lov	k1gInSc7	lov
virus	virus	k1gInSc1	virus
ebola	ebola	k6eAd1	ebola
přímo	přímo	k6eAd1	přímo
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
přežití	přežití	k1gNnSc4	přežití
goril	gorila	k1gFnPc2	gorila
jako	jako	k8xC	jako
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
zachování	zachování	k1gNnSc6	zachování
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
snaží	snažit	k5eAaImIp3nS	snažit
tyto	tento	k3xDgInPc1	tento
projekty	projekt	k1gInPc1	projekt
a	a	k8xC	a
organizace	organizace	k1gFnPc1	organizace
<g/>
:	:	kIx,	:
Projekt	projekt	k1gInSc1	projekt
přežití	přežití	k1gNnSc4	přežití
lidoopů	lidoop	k1gMnPc2	lidoop
(	(	kIx(	(
<g/>
Great	Great	k2eAgInSc1d1	Great
Apes	Apes	k1gInSc1	Apes
Survival	Survival	k1gMnSc1	Survival
Project	Project	k1gMnSc1	Project
<g/>
,	,	kIx,	,
GRASP	GRASP	kA	GRASP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
partnerský	partnerský	k2eAgInSc4d1	partnerský
projekt	projekt	k1gInSc4	projekt
Programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
organizace	organizace	k1gFnSc1	organizace
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
goril	gorila	k1gFnPc2	gorila
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
biotopu	biotop	k1gInSc6	biotop
(	(	kIx(	(
<g/>
Agreement	Agreement	k1gInSc1	Agreement
on	on	k3xPp3gInSc1	on
the	the	k?	the
Conservation	Conservation	k1gInSc1	Conservation
of	of	k?	of
Gorillas	Gorillas	k1gInSc1	Gorillas
and	and	k?	and
Their	Their	k1gInSc1	Their
Habitats	Habitats	k1gInSc1	Habitats
<g/>
)	)	kIx)	)
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
stěhovavých	stěhovavý	k2eAgInPc2d1	stěhovavý
druhů	druh	k1gInPc2	druh
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
Convention	Convention	k1gInSc1	Convention
on	on	k3xPp3gInSc1	on
the	the	k?	the
Conservation	Conservation	k1gInSc1	Conservation
of	of	k?	of
Migratory	Migrator	k1gMnPc4	Migrator
Species	species	k1gFnSc2	species
of	of	k?	of
Wild	Wild	k1gInSc1	Wild
Animals	Animals	k1gInSc1	Animals
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
právnický	právnický	k2eAgInSc1d1	právnický
instrument	instrument	k1gInSc1	instrument
zavazující	zavazující	k2eAgFnSc2d1	zavazující
specificky	specificky	k6eAd1	specificky
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
goril	gorila	k1gFnPc2	gorila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obraz	obraz	k1gInSc1	obraz
goril	gorila	k1gFnPc2	gorila
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
staly	stát	k5eAaPmAgInP	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
gorily	gorila	k1gFnPc1	gorila
opakovaně	opakovaně	k6eAd1	opakovaně
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
uměleckých	umělecký	k2eAgInPc6d1	umělecký
dílech	díl	k1gInPc6	díl
a	a	k8xC	a
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Gorily	gorila	k1gFnPc1	gorila
se	se	k3xPyFc4	se
například	například	k6eAd1	například
objevily	objevit	k5eAaPmAgFnP	objevit
ve	v	k7c6	v
fiktivních	fiktivní	k2eAgInPc6d1	fiktivní
filmech	film	k1gInPc6	film
King	King	k1gInSc1	King
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zpracování	zpracování	k1gNnSc3	zpracování
příběhů	příběh	k1gInPc2	příběh
o	o	k7c6	o
Tarzanovi	Tarzan	k1gMnSc6	Tarzan
a	a	k8xC	a
o	o	k7c6	o
Barbaru	barbar	k1gMnSc6	barbar
Conanovi	Conan	k1gMnSc6	Conan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
gorily	gorila	k1gFnPc1	gorila
staly	stát	k5eAaPmAgFnP	stát
protivníky	protivník	k1gMnPc7	protivník
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
dorazila	dorazit	k5eAaPmAgFnS	dorazit
gorila	gorila	k1gFnSc1	gorila
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
<g/>
,	,	kIx,	,
Konečně	konečně	k6eAd1	konečně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možná	možný	k2eAgFnSc1d1	možná
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gorila	gorila	k1gFnSc1	gorila
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gorila	gorila	k1gFnSc1	gorila
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc4	taxon
Gorila	gorila	k1gFnSc1	gorila
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
gorila	gorila	k1gFnSc1	gorila
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Odhaleni	odhalen	k2eAgMnPc1d1	odhalen
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
netradiční	tradiční	k2eNgFnSc2d1	netradiční
"	"	kIx"	"
<g/>
reality	realita	k1gFnSc2	realita
show	show	k1gFnSc2	show
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
níž	nízce	k6eAd2	nízce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
on-line	onin	k1gInSc5	on-lin
sledovat	sledovat	k5eAaImF	sledovat
život	život	k1gInSc1	život
goril	gorila	k1gFnPc2	gorila
nížinných	nížinný	k2eAgFnPc2d1	nížinná
v	v	k7c6	v
pavilonu	pavilon	k1gInSc6	pavilon
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Pulitzer	Pulitzer	k1gInSc1	Pulitzer
Center	centrum	k1gNnPc2	centrum
on	on	k3xPp3gMnSc1	on
Crisis	Crisis	k1gFnSc1	Crisis
Reporting	Reporting	k1gInSc4	Reporting
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
program	program	k1gInSc1	program
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
goril	gorila	k1gFnPc2	gorila
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Primate	Primat	k1gInSc5	Primat
Info	Info	k1gMnSc1	Info
Net	Net	k1gMnPc2	Net
Gorilla	Gorilla	k1gMnSc1	Gorilla
Factsheet	Factsheet	k1gMnSc1	Factsheet
-	-	kIx~	-
taxonomie	taxonomie	k1gFnSc1	taxonomie
<g/>
,	,	kIx,	,
ekologie	ekologie	k1gFnSc1	ekologie
<g/>
,	,	kIx,	,
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
San	San	k?	San
Diego	Diego	k1gMnSc1	Diego
Zoo	zoo	k1gFnSc2	zoo
Gorilla	Gorilla	k1gMnSc1	Gorilla
Factsheet	Factsheet	k1gMnSc1	Factsheet
-	-	kIx~	-
video	video	k1gNnSc1	video
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
<p>
<s>
Gorilla	Gorilla	k6eAd1	Gorilla
protection	protection	k1gInSc1	protection
-	-	kIx~	-
ochrana	ochrana	k1gFnSc1	ochrana
goril	gorila	k1gFnPc2	gorila
</s>
</p>
<p>
<s>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Year	Year	k1gInSc1	Year
of	of	k?	of
the	the	k?	the
Gorilla	Gorilla	k1gMnSc1	Gorilla
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Virunga	Virunga	k1gFnSc1	Virunga
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
-	-	kIx~	-
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Virunga	Virunga	k1gFnSc1	Virunga
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Gorilla	Gorillo	k1gNnSc2	Gorillo
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
