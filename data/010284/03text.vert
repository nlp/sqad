<p>
<s>
Brigadýrka	brigadýrka	k1gFnSc1	brigadýrka
je	být	k5eAaImIp3nS	být
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
se	s	k7c7	s
štítkem	štítek	k1gInSc7	štítek
<g/>
,	,	kIx,	,
umístěným	umístěný	k2eAgInSc7d1	umístěný
na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
straně	strana	k1gFnSc6	strana
této	tento	k3xDgFnSc2	tento
čepice	čepice	k1gFnSc2	čepice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
o	o	k7c4	o
pracovní	pracovní	k2eAgFnSc4d1	pracovní
důstojnickou	důstojnický	k2eAgFnSc4d1	důstojnická
čepici	čepice	k1gFnSc4	čepice
pracovníků	pracovník	k1gMnPc2	pracovník
státních	státní	k2eAgFnPc2d1	státní
struktur	struktura	k1gFnPc2	struktura
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
dost	dost	k6eAd1	dost
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
nad	nad	k7c7	nad
štítkem	štítek	k1gInSc7	štítek
umístěn	umístěn	k2eAgInSc1d1	umístěn
rozlišující	rozlišující	k2eAgInSc1d1	rozlišující
symbol	symbol	k1gInSc1	symbol
či	či	k8xC	či
znak	znak	k1gInSc1	znak
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Přilba	přilba	k1gFnSc1	přilba
(	(	kIx(	(
<g/>
vojenská	vojenský	k2eAgFnSc1d1	vojenská
výstroj	výstroj	k1gFnSc1	výstroj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brigadýrka	brigadýrka	k1gFnSc1	brigadýrka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
brigadýrka	brigadýrka	k1gFnSc1	brigadýrka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
