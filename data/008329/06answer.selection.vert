<s>
Clara	Clara	k1gFnSc1	Clara
Josephine	Josephin	k1gInSc5	Josephin
Schumannová	Schumannový	k2eAgNnPc1d1	Schumannový
<g/>
,	,	kIx,	,
nepřechýleně	přechýleně	k6eNd1	přechýleně
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gInSc1	Schumann
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
Wieck	Wieck	k1gInSc1	Wieck
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1819	[number]	k4	1819
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
klavíristka	klavíristka	k1gFnSc1	klavíristka
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc1d1	hudební
skladatelka	skladatelka	k1gFnSc1	skladatelka
a	a	k8xC	a
pedagožka	pedagožka	k1gFnSc1	pedagožka
<g/>
.	.	kIx.	.
</s>
