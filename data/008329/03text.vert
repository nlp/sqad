<p>
<s>
Clara	Clara	k1gFnSc1	Clara
Josephine	Josephin	k1gInSc5	Josephin
Schumannová	Schumannový	k2eAgNnPc1d1	Schumannový
<g/>
,	,	kIx,	,
nepřechýleně	přechýleně	k6eNd1	přechýleně
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gInSc1	Schumann
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
Wieck	Wieck	k1gInSc1	Wieck
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1819	[number]	k4	1819
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
klavíristka	klavíristka	k1gFnSc1	klavíristka
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc1d1	hudební
skladatelka	skladatelka	k1gFnSc1	skladatelka
a	a	k8xC	a
pedagožka	pedagožka	k1gFnSc1	pedagožka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
manželkou	manželka	k1gFnSc7	manželka
skladatele	skladatel	k1gMnSc2	skladatel
Roberta	Robert	k1gMnSc2	Robert
Schumanna	Schumann	k1gMnSc2	Schumann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
Clara	Clara	k1gFnSc1	Clara
Josephine	Josephin	k1gInSc5	Josephin
Wieck	Wiecko	k1gNnPc2	Wiecko
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
klavírního	klavírní	k2eAgMnSc2d1	klavírní
pedagoga	pedagog	k1gMnSc2	pedagog
a	a	k8xC	a
hudebního	hudební	k2eAgMnSc2d1	hudební
kritika	kritik	k1gMnSc2	kritik
Friedricha	Friedrich	k1gMnSc2	Friedrich
Wiecka	Wiecka	k1gFnSc1	Wiecka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Marianne	Mariann	k1gInSc5	Mariann
Wieck	Wiecko	k1gNnPc2	Wiecko
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Tromlitz	Tromlitz	k1gMnSc1	Tromlitz
<g/>
.	.	kIx.	.
</s>
<s>
Marianne	Mariannout	k5eAaPmIp3nS	Mariannout
Tromlitz	Tromlitz	k1gInSc4	Tromlitz
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zpívala	zpívat	k5eAaImAgFnS	zpívat
sóla	sólo	k1gNnPc4	sólo
na	na	k7c6	na
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
lipském	lipský	k2eAgInSc6d1	lipský
koncertním	koncertní	k2eAgInSc6d1	koncertní
sále	sál	k1gInSc6	sál
Gewandhaus	Gewandhaus	k1gInSc1	Gewandhaus
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
Clařiných	Clařin	k2eAgMnPc2d1	Clařin
rodičů	rodič	k1gMnPc2	rodič
nebylo	být	k5eNaImAgNnS	být
nijak	nijak	k6eAd1	nijak
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
otcově	otcův	k2eAgFnSc3d1	otcova
panovačné	panovačný	k2eAgFnSc3d1	panovačná
povaze	povaha	k1gFnSc3	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
milostné	milostný	k2eAgFnSc6d1	milostná
aféře	aféra	k1gFnSc6	aféra
mezi	mezi	k7c7	mezi
Klářinou	Klářin	k2eAgFnSc7d1	Klářina
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
otcovým	otcův	k2eAgMnSc7d1	otcův
přítelem	přítel	k1gMnSc7	přítel
Adolphem	Adolph	k1gInSc7	Adolph
Bargielem	Bargiel	k1gInSc7	Bargiel
se	se	k3xPyFc4	se
manželé	manžel	k1gMnPc1	manžel
Wieckovi	Wieckův	k2eAgMnPc1d1	Wieckův
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Marianne	Mariannout	k5eAaPmIp3nS	Mariannout
se	se	k3xPyFc4	se
za	za	k7c2	za
Bargiela	Bargiel	k1gMnSc2	Bargiel
provdala	provdat	k5eAaPmAgFnS	provdat
a	a	k8xC	a
Clara	Clara	k1gFnSc1	Clara
zůstala	zůstat	k5eAaPmAgFnS	zůstat
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
raného	raný	k2eAgNnSc2d1	rané
dětství	dětství	k1gNnSc2	dětství
byl	být	k5eAaImAgInS	být
Clařina	Clařin	k2eAgFnSc1d1	Clařina
kariéra	kariéra	k1gFnSc1	kariéra
plánována	plánován	k2eAgFnSc1d1	plánována
otcem	otec	k1gMnSc7	otec
do	do	k7c2	do
nejmenších	malý	k2eAgInPc2d3	nejmenší
detailů	detail	k1gInPc2	detail
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
měla	mít	k5eAaImAgFnS	mít
hodinovou	hodinový	k2eAgFnSc4d1	hodinová
lekci	lekce	k1gFnSc4	lekce
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
houslí	housle	k1gFnPc2	housle
<g/>
,	,	kIx,	,
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc2d1	hudební
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
harmonie	harmonie	k1gFnSc2	harmonie
a	a	k8xC	a
kontrapunktu	kontrapunkt	k1gInSc2	kontrapunkt
a	a	k8xC	a
dvouhodinové	dvouhodinový	k2eAgNnSc4d1	dvouhodinové
praktické	praktický	k2eAgNnSc4d1	praktické
cvičení	cvičení	k1gNnSc4	cvičení
podle	podle	k7c2	podle
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
otec	otec	k1gMnSc1	otec
sám	sám	k3xTgMnSc1	sám
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1828	[number]	k4	1828
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ernsta	Ernst	k1gMnSc2	Ernst
Caruse	Caruse	k1gFnSc2	Caruse
<g/>
,	,	kIx,	,
ředitele	ředitel	k1gMnSc2	ředitel
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
sídlící	sídlící	k2eAgFnSc1d1	sídlící
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c4	v
Colditz	Colditz	k1gInSc4	Colditz
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
mladým	mladý	k2eAgMnSc7d1	mladý
nadaným	nadaný	k2eAgMnSc7d1	nadaný
klavíristou	klavírista	k1gMnSc7	klavírista
pozvaným	pozvaný	k2eAgMnSc7d1	pozvaný
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
hudební	hudební	k2eAgInSc4d1	hudební
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
o	o	k7c4	o
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
starším	starší	k1gMnSc7	starší
Robertem	Robert	k1gMnSc7	Robert
Schumannem	Schumann	k1gMnSc7	Schumann
<g/>
.	.	kIx.	.
</s>
<s>
Schumanna	Schumanen	k2eAgFnSc1d1	Schumanen
hra	hra	k1gFnSc1	hra
malé	malý	k2eAgFnSc2d1	malá
Clary	Clara	k1gFnSc2	Clara
natolik	natolik	k6eAd1	natolik
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
svolení	svolení	k1gNnSc4	svolení
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
studia	studio	k1gNnSc2	studio
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
studovat	studovat	k5eAaImF	studovat
hudbu	hudba	k1gFnSc4	hudba
u	u	k7c2	u
Klářina	Klářin	k2eAgMnSc2d1	Klářin
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
ročního	roční	k2eAgNnSc2d1	roční
studia	studio	k1gNnSc2	studio
bydlel	bydlet	k5eAaImAgMnS	bydlet
ve	v	k7c6	v
Wieckově	Wieckův	k2eAgInSc6d1	Wieckův
domě	dům	k1gInSc6	dům
a	a	k8xC	a
s	s	k7c7	s
Clarou	Clara	k1gFnSc7	Clara
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
již	již	k9	již
v	v	k7c6	v
jejích	její	k3xOp3gInPc6	její
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
chtěl	chtít	k5eAaImAgMnS	chtít
vzít	vzít	k5eAaPmF	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wieck	Wieck	k1gMnSc1	Wieck
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
naprosto	naprosto	k6eAd1	naprosto
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
Clara	Clara	k1gFnSc1	Clara
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
uzavřít	uzavřít	k5eAaPmF	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vyjela	vyjet	k5eAaPmAgFnS	vyjet
Clara	Clara	k1gFnSc1	Clara
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
na	na	k7c4	na
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
první	první	k4xOgInSc1	první
sólový	sólový	k2eAgInSc1d1	sólový
koncert	koncert	k1gInSc1	koncert
konal	konat	k5eAaImAgInS	konat
ještě	ještě	k9	ještě
v	v	k7c6	v
domovském	domovský	k2eAgNnSc6d1	domovské
Lipsku	Lipsko	k1gNnSc6	Lipsko
v	v	k7c6	v
Gewandhausu	Gewandhaus	k1gInSc6	Gewandhaus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
předvedla	předvést	k5eAaPmAgFnS	předvést
bravurní	bravurní	k2eAgFnSc4d1	bravurní
skladbu	skladba	k1gFnSc4	skladba
klavíristy	klavírista	k1gMnSc2	klavírista
a	a	k8xC	a
skladatele	skladatel	k1gMnSc2	skladatel
Henriho	Henri	k1gMnSc2	Henri
Herze	Herze	k1gFnSc1	Herze
komponovanou	komponovaný	k2eAgFnSc4d1	komponovaná
pro	pro	k7c4	pro
Johanna	Johann	k1gMnSc4	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
von	von	k1gInSc4	von
Goethe	Goethe	k1gNnSc2	Goethe
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
pak	pak	k6eAd1	pak
daroval	darovat	k5eAaPmAgMnS	darovat
medaili	medaile	k1gFnSc4	medaile
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
portrétem	portrét	k1gInSc7	portrét
a	a	k8xC	a
s	s	k7c7	s
vyrytým	vyrytý	k2eAgNnSc7d1	vyryté
věnováním	věnování	k1gNnSc7	věnování
"	"	kIx"	"
<g/>
Nadané	nadaný	k2eAgFnSc3d1	nadaná
umělkyni	umělkyně	k1gFnSc3	umělkyně
Claře	Clara	k1gFnSc3	Clara
Wieckové	Wiecková	k1gFnSc3	Wiecková
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
slavný	slavný	k2eAgMnSc1d1	slavný
houslista	houslista	k1gMnSc1	houslista
Niccolò	Niccolò	k1gMnSc1	Niccolò
Paganini	Paganin	k2eAgMnPc1d1	Paganin
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
Claře	Clara	k1gFnSc3	Clara
Wieckové	Wieckové	k2eAgFnSc4d1	Wieckové
společné	společný	k2eAgNnSc4d1	společné
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncert	koncert	k1gInSc1	koncert
však	však	k9	však
přilákal	přilákat	k5eAaPmAgInS	přilákat
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k4c4	málo
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
opustilo	opustit	k5eAaPmAgNnS	opustit
Paříž	Paříž	k1gFnSc4	Paříž
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
epidemií	epidemie	k1gFnSc7	epidemie
cholery	cholera	k1gFnSc2	cholera
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1837	[number]	k4	1837
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1838	[number]	k4	1838
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
Clara	Clara	k1gFnSc1	Clara
Wieck	Wieck	k1gMnSc1	Wieck
sérii	série	k1gFnSc4	série
recitálů	recitál	k1gInPc2	recitál
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Franz	Franz	k1gMnSc1	Franz
Grillparzer	Grillparzer	k1gMnSc1	Grillparzer
napsal	napsat	k5eAaPmAgMnS	napsat
po	po	k7c6	po
vyslechnutí	vyslechnutí	k1gNnSc6	vyslechnutí
její	její	k3xOp3gFnSc2	její
interpretace	interpretace	k1gFnSc2	interpretace
Beethovenovy	Beethovenův	k2eAgFnSc2d1	Beethovenova
Sonáty	sonáta	k1gFnSc2	sonáta
č.	č.	k?	č.
23	[number]	k4	23
Appasionata	Appasionat	k1gMnSc2	Appasionat
báseň	báseň	k1gFnSc4	báseň
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
"	"	kIx"	"
<g/>
Clara	Clara	k1gFnSc1	Clara
Wieck	Wieck	k1gMnSc1	Wieck
und	und	k?	und
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Clara	Clara	k1gFnSc1	Clara
okouzlila	okouzlit	k5eAaPmAgFnS	okouzlit
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
i	i	k8xC	i
hudební	hudební	k2eAgFnSc2d1	hudební
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Benedict	Benedict	k1gMnSc1	Benedict
Randhartinger	Randhartinger	k1gMnSc1	Randhartinger
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
Franze	Franze	k1gFnSc1	Franze
Schuberta	Schubert	k1gMnSc2	Schubert
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc3	on
daroval	darovat	k5eAaPmAgMnS	darovat
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
kopii	kopie	k1gFnSc4	kopie
Schubertovy	Schubertův	k2eAgFnSc2d1	Schubertova
písně	píseň	k1gFnSc2	píseň
Erlkönig	Erlkönig	k1gMnSc1	Erlkönig
(	(	kIx(	(
<g/>
Král	Král	k1gMnSc1	Král
duchů	duch	k1gMnPc2	duch
<g/>
)	)	kIx)	)
s	s	k7c7	s
vepsaným	vepsaný	k2eAgNnSc7d1	vepsané
věnováním	věnování	k1gNnSc7	věnování
<g/>
:	:	kIx,	:
Slavné	slavný	k2eAgFnSc3d1	slavná
umělkyni	umělkyně	k1gFnSc3	umělkyně
Claře	Clara	k1gFnSc3	Clara
Wieckové	Wiecková	k1gFnSc3	Wiecková
<g/>
.	.	kIx.	.
</s>
<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
popsal	popsat	k5eAaPmAgMnS	popsat
své	svůj	k3xOyFgInPc4	svůj
dojmy	dojem	k1gInPc4	dojem
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
hry	hra	k1gFnSc2	hra
Franzi	Franze	k1gFnSc4	Franze
Lisztovi	Liszt	k1gMnSc3	Liszt
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
Revue	revue	k1gFnSc6	revue
et	et	k?	et
Gazette	Gazett	k1gInSc5	Gazett
Musicale	musical	k1gInSc5	musical
i	i	k9	i
v	v	k7c6	v
lipském	lipský	k2eAgInSc6d1	lipský
časopise	časopis	k1gInSc6	časopis
Neue	Neue	k1gFnSc1	Neue
Zeitschrift	Zeitschrift	k1gInSc1	Zeitschrift
für	für	k?	für
Musik	musika	k1gFnPc2	musika
<g/>
..	..	k?	..
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1838	[number]	k4	1838
byla	být	k5eAaImAgFnS	být
Clara	Clara	k1gFnSc1	Clara
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
Císařskou	císařský	k2eAgFnSc7d1	císařská
a	a	k8xC	a
královskou	královský	k2eAgFnSc7d1	královská
komorní	komorní	k2eAgFnSc7d1	komorní
virtuózkou	virtuózka	k1gFnSc7	virtuózka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
rakouské	rakouský	k2eAgNnSc1d1	rakouské
hudební	hudební	k2eAgNnSc1d1	hudební
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
jejích	její	k3xOp3gInPc6	její
prvních	první	k4xOgInPc6	první
uměleckých	umělecký	k2eAgInPc6d1	umělecký
letech	let	k1gInPc6	let
vybíral	vybírat	k5eAaImAgMnS	vybírat
repertoár	repertoár	k1gInSc4	repertoár
otec	otec	k1gMnSc1	otec
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
co	co	k9	co
možná	možná	k9	možná
okázalý	okázalý	k2eAgMnSc1d1	okázalý
a	a	k8xC	a
populární	populární	k2eAgMnSc1d1	populární
v	v	k7c6	v
oblíbeném	oblíbený	k2eAgInSc6d1	oblíbený
stylu	styl	k1gInSc6	styl
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
mj.	mj.	kA	mj.
díla	dílo	k1gNnPc4	dílo
Friedricha	Friedrich	k1gMnSc2	Friedrich
Kalkbrennera	Kalkbrenner	k1gMnSc2	Kalkbrenner
<g/>
,	,	kIx,	,
Adolfa	Adolf	k1gMnSc2	Adolf
von	von	k1gInSc1	von
Henselt	Henselt	k2eAgInSc1d1	Henselt
<g/>
,	,	kIx,	,
Sigismonda	Sigismond	k1gMnSc4	Sigismond
Thalberga	Thalberg	k1gMnSc4	Thalberg
<g/>
,	,	kIx,	,
Henriho	Henri	k1gMnSc4	Henri
Herze	Herze	k1gFnSc2	Herze
<g/>
,	,	kIx,	,
Johanna	Johanen	k2eAgFnSc1d1	Johanna
Pixise	Pixise	k1gFnSc1	Pixise
<g/>
,	,	kIx,	,
významného	významný	k2eAgMnSc2d1	významný
hudebního	hudební	k2eAgMnSc2d1	hudební
pedagoga	pedagog	k1gMnSc2	pedagog
Carla	Carl	k1gMnSc2	Carl
Czernyho	Czerny	k1gMnSc2	Czerny
a	a	k8xC	a
také	také	k9	také
její	její	k3xOp3gFnPc4	její
vlastní	vlastní	k2eAgFnPc4d1	vlastní
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Clara	Clara	k1gFnSc1	Clara
dospívala	dospívat	k5eAaImAgFnS	dospívat
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
sama	sám	k3xTgFnSc1	sám
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
sestavování	sestavování	k1gNnSc6	sestavování
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
více	hodně	k6eAd2	hodně
uvádět	uvádět	k5eAaImF	uvádět
skladby	skladba	k1gFnPc4	skladba
nových	nový	k2eAgMnPc2d1	nový
romantických	romantický	k2eAgMnPc2d1	romantický
autorů	autor	k1gMnPc2	autor
jako	jako	k8xC	jako
Chopina	Chopin	k1gMnSc2	Chopin
a	a	k8xC	a
Felixe	Felix	k1gMnSc2	Felix
Mendelssohna	Mendelssohn	k1gMnSc2	Mendelssohn
Bartholdyho	Bartholdy	k1gMnSc2	Bartholdy
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
méně	málo	k6eAd2	málo
efektní	efektní	k2eAgNnSc1d1	efektní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obtížnější	obtížný	k2eAgFnPc1d2	obtížnější
skladby	skladba	k1gFnPc1	skladba
velikánů	velikán	k1gMnPc2	velikán
minulosti	minulost	k1gFnSc2	minulost
jako	jako	k8xS	jako
Domenica	Domenicus	k1gMnSc4	Domenicus
Scarlattiho	Scarlatti	k1gMnSc4	Scarlatti
<g/>
,	,	kIx,	,
Johanna	Johann	k1gMnSc4	Johann
Sebastiana	Sebastian	k1gMnSc4	Sebastian
Bacha	Bacha	k?	Bacha
<g/>
,	,	kIx,	,
Wolfganga	Wolfgang	k1gMnSc4	Wolfgang
Amadea	Amadeus	k1gMnSc4	Amadeus
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
,	,	kIx,	,
Ludwiga	Ludwiga	k1gFnSc1	Ludwiga
van	van	k1gInSc1	van
Beethovena	Beethoven	k1gMnSc2	Beethoven
a	a	k8xC	a
Roberta	Robert	k1gMnSc2	Robert
Schuberta	Schubert	k1gMnSc2	Schubert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnSc1d1	další
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnSc1	její
vynikající	vynikající	k2eAgFnSc1d1	vynikající
pověst	pověst	k1gFnSc1	pověst
ji	on	k3xPp3gFnSc4	on
přinesla	přinést	k5eAaPmAgFnS	přinést
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
hudebníky	hudebník	k1gMnPc4	hudebník
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Mendelssohna	Mendelssohn	k1gMnSc2	Mendelssohn
<g/>
,	,	kIx,	,
Chopina	Chopin	k1gMnSc2	Chopin
a	a	k8xC	a
Liszta	Liszta	k1gFnSc1	Liszta
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
také	také	k9	také
se	s	k7c7	s
slavným	slavný	k2eAgMnSc7d1	slavný
houslistou	houslista	k1gMnSc7	houslista
Josephem	Joseph	k1gInSc7	Joseph
Joachimem	Joachim	k1gMnSc7	Joachim
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
častým	častý	k2eAgMnSc7d1	častý
koncertním	koncertní	k2eAgMnSc7d1	koncertní
partnerem	partner	k1gMnSc7	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
manžela	manžel	k1gMnSc2	manžel
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1856	[number]	k4	1856
se	se	k3xPyFc4	se
Clara	Clara	k1gFnSc1	Clara
Schumannová	Schumannová	k1gFnSc1	Schumannová
zcela	zcela	k6eAd1	zcela
věnovala	věnovat	k5eAaPmAgFnS	věnovat
interpretaci	interpretace	k1gFnSc4	interpretace
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
poprvé	poprvé	k6eAd1	poprvé
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Anglii	Anglie	k1gFnSc4	Anglie
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
anglického	anglický	k2eAgMnSc2d1	anglický
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
a	a	k8xC	a
Schumannova	Schumannův	k2eAgMnSc4d1	Schumannův
přítele	přítel	k1gMnSc4	přítel
Williama	William	k1gMnSc4	William
Bennetta	Bennett	k1gMnSc4	Bennett
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
anglická	anglický	k2eAgFnSc1d1	anglická
kritika	kritika	k1gFnSc1	kritika
dílu	díl	k1gInSc2	díl
Roberta	Robert	k1gMnSc2	Robert
Schumanna	Schumann	k1gMnSc2	Schumann
příliš	příliš	k6eAd1	příliš
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
téměř	téměř	k6eAd1	téměř
každoročně	každoročně	k6eAd1	každoročně
vracela	vracet	k5eAaImAgFnS	vracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
rovněž	rovněž	k9	rovněž
významou	významý	k2eAgFnSc4d1	významá
roli	role	k1gFnSc4	role
při	při	k7c6	při
rehabilitaci	rehabilitace	k1gFnSc6	rehabilitace
1	[number]	k4	1
<g/>
.	.	kIx.	.
klavírního	klavírní	k2eAgInSc2d1	klavírní
koncertu	koncert	k1gInSc2	koncert
d-moll	dolnout	k5eAaPmAgInS	d-molnout
Johannese	Johannese	k1gFnSc2	Johannese
Brahmse	Brahms	k1gMnSc2	Brahms
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
při	při	k7c6	při
premiéře	premiéra	k1gFnSc6	premiéra
zcela	zcela	k6eAd1	zcela
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jejímu	její	k3xOp3gNnSc3	její
úsilí	úsilí	k1gNnSc3	úsilí
se	se	k3xPyFc4	se
koncert	koncert	k1gInSc1	koncert
úspěšně	úspěšně	k6eAd1	úspěšně
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
světového	světový	k2eAgInSc2d1	světový
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	let	k1gInSc4	let
mladší	mladý	k2eAgMnSc1d2	mladší
Brahms	Brahms	k1gMnSc1	Brahms
byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
Clary	Clara	k1gFnSc2	Clara
Schumannové	Schumannová	k1gFnSc2	Schumannová
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
osobním	osobní	k2eAgMnSc7d1	osobní
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
zajímala	zajímat	k5eAaImAgFnS	zajímat
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
Franze	Franze	k1gFnSc2	Franze
Liszta	Liszta	k1gFnSc1	Liszta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
již	již	k6eAd1	již
odmítala	odmítat	k5eAaImAgFnS	odmítat
jeho	jeho	k3xOp3gFnPc4	jeho
skladby	skladba	k1gFnPc4	skladba
hrát	hrát	k5eAaImF	hrát
a	a	k8xC	a
při	při	k7c6	při
vydávání	vydávání	k1gNnSc6	vydávání
souborného	souborný	k2eAgNnSc2d1	souborné
díla	dílo	k1gNnSc2	dílo
svého	svůj	k3xOyFgMnSc2	svůj
manžela	manžel	k1gMnSc2	manžel
potlačila	potlačit	k5eAaPmAgFnS	potlačit
i	i	k8xC	i
dedikaci	dedikace	k1gFnSc4	dedikace
Schumannovy	Schumannův	k2eAgFnSc2d1	Schumannova
Fantasie	fantasie	k1gFnSc2	fantasie
C-dur	Cura	k1gFnPc2	C-dura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
skladatelem	skladatel	k1gMnSc7	skladatel
původně	původně	k6eAd1	původně
Lisztovi	Lisztův	k2eAgMnPc1d1	Lisztův
věnována	věnovat	k5eAaImNgFnS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c6	na
jubilejním	jubilejní	k2eAgInSc6d1	jubilejní
Beethovenově	Beethovenův	k2eAgInSc6d1	Beethovenův
festivalu	festival	k1gInSc6	festival
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
také	také	k9	také
Liszt	Liszt	k1gMnSc1	Liszt
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
nesnášela	snášet	k5eNaImAgNnP	snášet
hudbu	hudba	k1gFnSc4	hudba
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
opeře	opera	k1gFnSc6	opera
Tannhäuser	Tannhäusra	k1gFnPc2	Tannhäusra
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
libuje	libovat	k5eAaImIp3nS	libovat
v	v	k7c6	v
krutostech	krutost	k1gFnPc6	krutost
<g/>
,	,	kIx,	,
Lohengrin	Lohengrin	k1gInSc1	Lohengrin
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
hrůzný	hrůzný	k2eAgInSc1d1	hrůzný
a	a	k8xC	a
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Isolda	Isolda	k1gFnSc1	Isolda
"	"	kIx"	"
<g/>
nejstrašnější	strašný	k2eAgFnSc1d3	nejstrašnější
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kdy	kdy	k6eAd1	kdy
viděla	vidět	k5eAaImAgFnS	vidět
nebo	nebo	k8xC	nebo
slyšela	slyšet	k5eAaImAgFnS	slyšet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
profesorkou	profesorka	k1gFnSc7	profesorka
klavíru	klavír	k1gInSc2	klavír
na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
kteréžto	kteréžto	k?	kteréžto
postavení	postavení	k1gNnSc2	postavení
si	se	k3xPyFc3	se
udržela	udržet	k5eAaPmAgFnS	udržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
pedagogickou	pedagogický	k2eAgFnSc7d1	pedagogická
činností	činnost	k1gFnSc7	činnost
velice	velice	k6eAd1	velice
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
moderní	moderní	k2eAgFnSc2d1	moderní
klavírní	klavírní	k2eAgFnSc2d1	klavírní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
veřejný	veřejný	k2eAgInSc4d1	veřejný
koncert	koncert	k1gInSc4	koncert
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
hrála	hrát	k5eAaImAgFnS	hrát
Brahmsovy	Brahmsův	k2eAgInPc4d1	Brahmsův
Varice	Varice	k1gInPc4	Varice
na	na	k7c4	na
Haydnovo	Haydnův	k2eAgNnSc4d1	Haydnovo
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
života	život	k1gInSc2	život
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
sluch	sluch	k1gInSc4	sluch
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
upoutána	upoutat	k5eAaPmNgFnS	upoutat
na	na	k7c4	na
kolečkové	kolečkový	k2eAgNnSc4d1	kolečkové
křeslo	křeslo	k1gNnSc4	křeslo
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1896	[number]	k4	1896
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
záchvat	záchvat	k1gInSc4	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1896	[number]	k4	1896
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
76	[number]	k4	76
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
v	v	k7c6	v
Bonnu	Bonn	k1gInSc6	Bonn
na	na	k7c6	na
Starém	starý	k2eAgInSc6d1	starý
hřbitově	hřbitov	k1gInSc6	hřbitov
(	(	kIx(	(
<g/>
Alter	Alter	k1gMnSc1	Alter
Friedhof	Friedhof	k1gMnSc1	Friedhof
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gInSc1	její
rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
nebyl	být	k5eNaImAgInS	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
osmi	osm	k4xCc2	osm
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
ještě	ještě	k9	ještě
za	za	k7c2	za
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Manžel	manžel	k1gMnSc1	manžel
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
synů	syn	k1gMnPc2	syn
byl	být	k5eAaImAgMnS	být
umístěn	umístit	k5eAaPmNgMnS	umístit
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
pouhého	pouhý	k2eAgInSc2d1	pouhý
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
zemřela	zemřít	k5eAaPmAgFnS	zemřít
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
Julie	Julie	k1gFnSc1	Julie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zanechala	zanechat	k5eAaPmAgFnS	zanechat
dvě	dva	k4xCgFnPc4	dva
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Felix	Felix	k1gMnSc1	Felix
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
43	[number]	k4	43
let	léto	k1gNnPc2	léto
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
Clara	Clara	k1gFnSc1	Clara
Schumannová	Schumannová	k1gFnSc1	Schumannová
starala	starat	k5eAaImAgFnS	starat
i	i	k9	i
o	o	k7c4	o
jeho	jeho	k3xOp3gFnPc4	jeho
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
==	==	k?	==
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
klavíristů	klavírista	k1gMnPc2	klavírista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
celá	celý	k2eAgFnSc1d1	celá
svá	svůj	k3xOyFgNnPc4	svůj
vystoupení	vystoupení	k1gNnPc4	vystoupení
hráli	hrát	k5eAaImAgMnP	hrát
zpaměti	zpaměti	k6eAd1	zpaměti
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
běžné	běžný	k2eAgNnSc1d1	běžné
a	a	k8xC	a
taková	takový	k3xDgFnSc1	takový
schopnost	schopnost	k1gFnSc1	schopnost
byla	být	k5eAaImAgFnS	být
i	i	k9	i
předmětem	předmět	k1gInSc7	předmět
obdivu	obdiv	k1gInSc2	obdiv
hudebních	hudební	k2eAgMnPc2d1	hudební
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgMnS	být
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
životě	život	k1gInSc6	život
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
Song	song	k1gInSc1	song
of	of	k?	of
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
Píseň	píseň	k1gFnSc1	píseň
lásky	láska	k1gFnSc2	láska
<g/>
)	)	kIx)	)
s	s	k7c7	s
Katharine	Katharin	k1gInSc5	Katharin
Hephurnovou	Hephurnový	k2eAgFnSc7d1	Hephurnový
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Roberta	Robert	k1gMnSc4	Robert
Schumanna	Schumann	k1gMnSc4	Schumann
hrál	hrát	k5eAaImAgMnS	hrát
Paul	Paul	k1gMnSc1	Paul
Henreid	Henreida	k1gFnPc2	Henreida
a	a	k8xC	a
mladého	mladý	k1gMnSc2	mladý
Johannese	Johannese	k1gFnSc2	Johannese
Brahmse	Brahms	k1gMnSc4	Brahms
Robert	Robert	k1gMnSc1	Robert
Walker	Walker	k1gMnSc1	Walker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
francouzsko-německo-maďarské	francouzskoěmeckoaďarský	k2eAgFnSc6d1	francouzsko-německo-maďarský
koprodukci	koprodukce	k1gFnSc6	koprodukce
natočen	natočit	k5eAaBmNgInS	natočit
další	další	k2eAgInSc1d1	další
film	film	k1gInSc1	film
Geliebte	Geliebte	k1gFnSc1	Geliebte
Clara	Clara	k1gFnSc1	Clara
(	(	kIx(	(
<g/>
Milovaná	milovaný	k2eAgFnSc1d1	milovaná
Clara	Clara	k1gFnSc1	Clara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gInSc1	její
portrét	portrét	k1gInSc1	portrét
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
stomarkové	stomarkový	k2eAgFnSc6d1	stomarkový
bankovce	bankovka	k1gFnSc6	bankovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Labi	Labe	k1gNnSc6	Labe
se	se	k3xPyFc4	se
plaví	plavit	k5eAaImIp3nS	plavit
výletní	výletní	k2eAgFnSc1d1	výletní
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
názvem	název	k1gInSc7	název
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
zajíždí	zajíždět	k5eAaImIp3nS	zajíždět
i	i	k9	i
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Klavírní	klavírní	k2eAgFnPc4d1	klavírní
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Etuda	etuda	k1gFnSc1	etuda
As-dur	Asura	k1gFnPc2	As-dura
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
sonáta	sonáta	k1gFnSc1	sonáta
g-moll	goll	k1gMnSc1	g-moll
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Impromptu	impromptu	k1gNnSc1	impromptu
E-dur	Eura	k1gFnPc2	E-dura
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
à	à	k?	à
4	[number]	k4	4
voci	voci	k1gNnSc2	voci
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pochod	pochod	k1gInSc1	pochod
Es-dur	Esura	k1gFnPc2	Es-dura
(	(	kIx(	(
<g/>
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
fugy	fuga	k1gFnPc1	fuga
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
J.	J.	kA	J.
S.	S.	kA	S.
Bacha	Bacha	k?	Bacha
</s>
</p>
<p>
<s>
Preludium	preludium	k1gNnSc1	preludium
f-moll	folla	k1gFnPc2	f-molla
</s>
</p>
<p>
<s>
Quatre	Quatr	k1gMnSc5	Quatr
Polonaises	Polonaises	k1gMnSc1	Polonaises
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Scherzo	scherzo	k1gNnSc1	scherzo
d-moll	doll	k1gMnSc1	d-moll
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
</s>
</p>
<p>
<s>
Trois	Trois	k1gInSc1	Trois
Romances	Romances	k1gMnSc1	Romances
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
11	[number]	k4	11
</s>
</p>
<p>
<s>
Scherzo	scherzo	k1gNnSc1	scherzo
c-moll	coll	k1gMnSc1	c-moll
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
14	[number]	k4	14
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Quatre	Quatr	k1gMnSc5	Quatr
Piè	Piè	k1gMnSc1	Piè
fugitives	fugitives	k1gMnSc1	fugitives
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
15	[number]	k4	15
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tři	tři	k4xCgNnPc4	tři
preludia	preludium	k1gNnPc4	preludium
a	a	k8xC	a
fugy	fuga	k1gFnSc2	fuga
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
16	[number]	k4	16
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Caprices	Caprices	k1gInSc1	Caprices
en	en	k?	en
forme	format	k5eAaPmIp3nS	format
de	de	k?	de
valse	valse	k1gFnSc1	valse
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Variace	variace	k1gFnPc1	variace
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Roberta	Robert	k1gMnSc2	Robert
Schumanna	Schumann	k1gMnSc2	Schumann
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
20	[number]	k4	20
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
romance	romance	k1gFnSc1	romance
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
21	[number]	k4	21
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Romance	romance	k1gFnSc1	romance
variée	variée	k1gFnPc2	variée
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
3	[number]	k4	3
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Valses	Valses	k1gMnSc1	Valses
romantiques	romantiques	k1gMnSc1	romantiques
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
4	[number]	k4	4
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
Quatre	Quatr	k1gMnSc5	Quatr
piè	piè	k?	piè
caractéristiques	caractéristiques	k1gMnSc1	caractéristiques
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
5	[number]	k4	5
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Soirées	Soirées	k1gMnSc1	Soirées
musicales	musicales	k1gMnSc1	musicales
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
6	[number]	k4	6
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Variations	Variations	k1gInSc1	Variations
de	de	k?	de
concert	concert	k1gInSc1	concert
sur	sur	k?	sur
la	la	k1gNnSc1	la
Cavatine	Cavatin	k1gInSc5	Cavatin
du	du	k?	du
Pirate	Pirat	k1gInSc5	Pirat
de	de	k?	de
Bellini	Bellin	k2eAgMnPc1d1	Bellin
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
8	[number]	k4	8
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Souvenir	Souvenir	k1gInSc1	Souvenir
de	de	k?	de
Vienne	Vienn	k1gInSc5	Vienn
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
9	[number]	k4	9
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Romance	romance	k1gFnSc1	romance
h-moll	holl	k1gMnSc1	h-moll
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
posth	posth	k1gMnSc1	posth
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Písně	píseň	k1gFnPc1	píseň
===	===	k?	===
</s>
</p>
<p>
<s>
Drei	Drei	k6eAd1	Drei
Rückert-Lieder	Rückert-Lieder	k1gMnSc1	Rückert-Lieder
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
12	[number]	k4	12
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnSc2	píseň
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ihr	Ihr	k?	Ihr
Bildnis	Bildnis	k1gInSc1	Bildnis
</s>
</p>
<p>
<s>
Sie	Sie	k?	Sie
liebten	liebten	k2eAgMnSc1d1	liebten
sich	sich	k1gMnSc1	sich
beide	beide	k6eAd1	beide
</s>
</p>
<p>
<s>
Liebeszauber	Liebeszauber	k1gMnSc1	Liebeszauber
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Mond	Mond	k1gMnSc1	Mond
kommt	kommt	k1gMnSc1	kommt
still	stilnout	k5eAaPmAgMnS	stilnout
gegangen	gegangen	k1gInSc4	gegangen
</s>
</p>
<p>
<s>
Ich	Ich	k?	Ich
hab	hab	k?	hab
<g/>
'	'	kIx"	'
in	in	k?	in
deinem	dein	k1gMnSc7	dein
Auge	Aug	k1gFnSc2	Aug
</s>
</p>
<p>
<s>
Die	Die	k?	Die
stille	stille	k6eAd1	stille
Lotusblume	Lotusblum	k1gInSc5	Lotusblum
</s>
</p>
<p>
<s>
6	[number]	k4	6
Rollett-Lieder	Rollett-Lieder	k1gInSc1	Rollett-Lieder
aus	aus	k?	aus
"	"	kIx"	"
<g/>
Jucunde	Jucund	k1gInSc5	Jucund
<g/>
"	"	kIx"	"
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
23	[number]	k4	23
</s>
</p>
<p>
<s>
===	===	k?	===
Komorní	komorní	k2eAgFnPc4d1	komorní
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
Klavírní	klavírní	k2eAgNnSc1d1	klavírní
trio	trio	k1gNnSc1	trio
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
17	[number]	k4	17
</s>
</p>
<p>
<s>
Tři	tři	k4xCgFnPc1	tři
romance	romance	k1gFnPc1	romance
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
22	[number]	k4	22
</s>
</p>
<p>
<s>
===	===	k?	===
Klavír	klavír	k1gInSc1	klavír
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
===	===	k?	===
</s>
</p>
<p>
<s>
Premier	Premier	k1gMnSc1	Premier
Concert	Concert	k1gMnSc1	Concert
pour	pour	k1gMnSc1	pour
le	le	k?	le
Piano-Forte	Piano-Fort	k1gInSc5	Piano-Fort
avec	avec	k1gInSc1	avec
accompagnement	accompagnement	k1gInSc1	accompagnement
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orchestre	orchestr	k1gInSc5	orchestr
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
7	[number]	k4	7
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Bogousslavsky	Bogousslavsky	k6eAd1	Bogousslavsky
<g/>
,	,	kIx,	,
J.	J.	kA	J.
and	and	k?	and
M.	M.	kA	M.
G.	G.	kA	G.
Hennerici	Hennerik	k1gMnPc1	Hennerik
<g/>
,	,	kIx,	,
Bäzner	Bäzner	k1gMnSc1	Bäzner
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
,	,	kIx,	,
Bassetti	Bassetti	k1gNnSc2	Bassetti
<g/>
,	,	kIx,	,
C.	C.	kA	C.
<g/>
,	,	kIx,	,
Neurological	Neurological	k1gFnSc1	Neurological
disorders	disorders	k1gInSc1	disorders
in	in	k?	in
famous	famous	k1gInSc1	famous
artists	artists	k1gInSc1	artists
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
3	[number]	k4	3
<g/>
,	,	kIx,	,
Karger	Karger	k1gInSc1	Karger
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
101	[number]	k4	101
<g/>
–	–	k?	–
<g/>
118	[number]	k4	118
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boyd	Boyd	k1gInSc1	Boyd
<g/>
,	,	kIx,	,
Melinda	Melinda	k1gFnSc1	Melinda
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Gendered	Gendered	k1gMnSc1	Gendered
Voices	Voices	k1gMnSc1	Voices
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
'	'	kIx"	'
<g/>
Liebesfrüling	Liebesfrüling	k1gInSc1	Liebesfrüling
<g/>
'	'	kIx"	'
Lieder	Lieder	k1gMnSc1	Lieder
of	of	k?	of
Robert	Robert	k1gMnSc1	Robert
and	and	k?	and
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
19	[number]	k4	19
<g/>
th-Century	th-Centura	k1gFnSc2	th-Centura
Music	Musice	k1gInPc2	Musice
39	[number]	k4	39
(	(	kIx(	(
<g/>
Autumn	Autumn	k1gInSc1	Autumn
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
145	[number]	k4	145
<g/>
–	–	k?	–
<g/>
162	[number]	k4	162
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gould	Gould	k1gMnSc1	Gould
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
What	What	k2eAgMnSc1d1	What
Did	Did	k1gMnSc1	Did
They	Thea	k1gFnSc2	Thea
Play	play	k0	play
<g/>
?	?	kIx.	?
</s>
<s>
The	The	k?	The
Changing	Changing	k1gInSc1	Changing
Repertoire	Repertoir	k1gInSc5	Repertoir
of	of	k?	of
the	the	k?	the
Piano	piano	k1gNnSc1	piano
Recital	Recital	k1gMnSc1	Recital
from	from	k1gMnSc1	from
the	the	k?	the
Beginnings	Beginnings	k1gInSc1	Beginnings
to	ten	k3xDgNnSc1	ten
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
The	The	k1gFnSc1	The
Musical	musical	k1gInSc4	musical
Times	Times	k1gInSc1	Times
146	[number]	k4	146
(	(	kIx(	(
<g/>
Winter	Winter	k1gMnSc1	Winter
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
61	[number]	k4	61
<g/>
–	–	k?	–
<g/>
76	[number]	k4	76
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kopiez	Kopiez	k1gMnSc1	Kopiez
<g/>
,	,	kIx,	,
Reinhard	Reinhard	k1gMnSc1	Reinhard
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gMnSc1	Andreas
C.	C.	kA	C.
Lehmann	Lehmann	k1gMnSc1	Lehmann
and	and	k?	and
Janina	Janin	k2eAgFnSc1d1	Janina
Klassen	Klassen	k1gInSc1	Klassen
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
collection	collection	k1gInSc1	collection
of	of	k?	of
playbills	playbills	k1gInSc1	playbills
<g/>
:	:	kIx,	:
A	a	k8xC	a
historiometric	historiometric	k1gMnSc1	historiometric
analysis	analysis	k1gFnSc2	analysis
of	of	k?	of
life-span	lifepan	k1gMnSc1	life-span
development	development	k1gMnSc1	development
<g/>
,	,	kIx,	,
mobility	mobilita	k1gFnPc1	mobilita
<g/>
,	,	kIx,	,
and	and	k?	and
repertoire	repertoir	k1gMnSc5	repertoir
canonization	canonization	k1gInSc4	canonization
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poetics	Poetics	k1gInSc1	Poetics
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
37	[number]	k4	37
<g/>
,	,	kIx,	,
Issue	Issue	k1gFnSc1	Issue
1	[number]	k4	1
<g/>
,	,	kIx,	,
February	Februara	k1gFnPc1	Februara
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
73	[number]	k4	73
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litzmann	Litzmann	k1gMnSc1	Litzmann
<g/>
,	,	kIx,	,
Berthold	Berthold	k1gMnSc1	Berthold
<g/>
.	.	kIx.	.
</s>
<s>
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
:	:	kIx,	:
An	An	k1gMnSc1	An
Artist	Artist	k1gMnSc1	Artist
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Life	Life	k1gFnSc7	Life
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Da	Da	k1gFnSc1	Da
Capo	capa	k1gFnSc5	capa
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
306	[number]	k4	306
<g/>
-	-	kIx~	-
<g/>
79582	[number]	k4	79582
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Burstein	Burstein	k1gMnSc1	Burstein
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Poundie	Poundie	k1gFnSc1	Poundie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Their	Their	k1gInSc1	Their
Paths	Pathsa	k1gFnPc2	Pathsa
<g/>
,	,	kIx,	,
Her	hra	k1gFnPc2	hra
Ways	Waysa	k1gFnPc2	Waysa
<g/>
:	:	kIx,	:
Comparison	Comparison	k1gInSc1	Comparison
of	of	k?	of
Text	text	k1gInSc1	text
Settings	Settingsa	k1gFnPc2	Settingsa
by	by	kYmCp3nS	by
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gInSc4	Schumann
and	and	k?	and
Other	Other	k1gInSc1	Other
Composers	Composers	k1gInSc1	Composers
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Women	Women	k2eAgMnSc1d1	Women
and	and	k?	and
Music	Music	k1gMnSc1	Music
–	–	k?	–
A	a	k9	a
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Gender	Gender	k1gMnSc1	Gender
and	and	k?	and
Culture	Cultur	k1gMnSc5	Cultur
6	[number]	k4	6
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
11	[number]	k4	11
<g/>
ff	ff	kA	ff
<g/>
.	.	kIx.	.
</s>
<s>
Accessed	Accessed	k1gMnSc1	Accessed
through	through	k1gMnSc1	through
the	the	k?	the
International	International	k1gMnSc4	International
Index	index	k1gInSc1	index
to	ten	k3xDgNnSc1	ten
Music	Musice	k1gFnPc2	Musice
Periodicals	Periodicals	k1gInSc1	Periodicals
on	on	k3xPp3gMnSc1	on
29	[number]	k4	29
June	jun	k1gMnSc5	jun
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rattalino	Rattalina	k1gFnSc5	Rattalina
<g/>
,	,	kIx,	,
Piero	Piera	k1gFnSc5	Piera
<g/>
.	.	kIx.	.
</s>
<s>
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
&	&	k?	&
Clara	Clara	k1gFnSc1	Clara
<g/>
.	.	kIx.	.
</s>
<s>
Varese	Varese	k1gFnSc1	Varese
<g/>
:	:	kIx,	:
Zecchini	Zecchin	k1gMnPc1	Zecchin
Editore	editor	k1gInSc5	editor
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
87203	[number]	k4	87203
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
Nancy	Nancy	k1gFnSc1	Nancy
B.	B.	kA	B.
"	"	kIx"	"
<g/>
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
In	In	k1gFnSc7	In
Women	Women	k2eAgInSc4d1	Women
Making	Making	k1gInSc4	Making
Music	Musice	k1gInPc2	Musice
<g/>
:	:	kIx,	:
The	The	k1gMnPc2	The
Western	Western	kA	Western
Art	Art	k1gFnSc1	Art
Tradition	Tradition	k1gInSc1	Tradition
<g/>
,	,	kIx,	,
1150	[number]	k4	1150
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Urbana	Urban	k1gMnSc4	Urban
and	and	k?	and
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Illinois	Illinois	k1gInSc1	Illinois
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
252	[number]	k4	252
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1204	[number]	k4	1204
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
Nancy	Nancy	k1gFnSc1	Nancy
B.	B.	kA	B.
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Artist	Artist	k1gMnSc1	Artist
and	and	k?	and
the	the	k?	the
Woman	Woman	k1gInSc1	Woman
<g/>
.	.	kIx.	.
</s>
<s>
Ithaca	Ithaca	k1gMnSc1	Ithaca
and	and	k?	and
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Cornell	Cornell	k1gInSc1	Cornell
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-0-8014-3740-3	[number]	k4	978-0-8014-3740-3
/	/	kIx~	/
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8014	[number]	k4	8014
<g/>
-	-	kIx~	-
<g/>
8637	[number]	k4	8637
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
Susanna	Susanna	k1gFnSc1	Susanna
<g/>
.	.	kIx.	.
</s>
<s>
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gInSc1	Schumann
<g/>
:	:	kIx,	:
Piano	piano	k1gNnSc1	piano
Virtuoso	Virtuosa	k1gFnSc5	Virtuosa
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Reprint	reprint	k1gInSc1	reprint
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Houghton	Houghton	k1gInSc1	Houghton
Mifflin	Mifflina	k1gFnPc2	Mifflina
Harcourt	Harcourta	k1gFnPc2	Harcourta
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
618	[number]	k4	618
<g/>
-	-	kIx~	-
<g/>
55160	[number]	k4	55160
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kees	Kees	k6eAd1	Kees
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Vloed	Vloed	k1gInSc1	Vloed
<g/>
,	,	kIx,	,
Clara	Clara	k1gFnSc1	Clara
Schumann-Wieck	Schumann-Wieck	k1gInSc1	Schumann-Wieck
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
pijn	pijn	k1gInSc1	pijn
van	van	k1gInSc1	van
het	het	k?	het
gemis	gemis	k1gInSc1	gemis
<g/>
.	.	kIx.	.
</s>
<s>
Aspekt	aspekt	k1gInSc1	aspekt
<g/>
,	,	kIx,	,
Soesterberg	Soesterberg	k1gInSc1	Soesterberg
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9789461531773	[number]	k4	9789461531773
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Clara	Clara	k1gFnSc1	Clara
Schumannová	Schumannový	k2eAgFnSc1d1	Schumannový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Clara	Clara	k1gFnSc1	Clara
Schumannová	Schumannová	k1gFnSc1	Schumannová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Clara	Clara	k1gFnSc1	Clara
Schumannová	Schumannová	k1gFnSc1	Schumannová
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dělo	k1gNnPc2	dělo
od	od	k7c2	od
Clary	Clara	k1gFnSc2	Clara
Schumannové	Schumannová	k1gFnSc2	Schumannová
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
Clara	Clara	k1gFnSc1	Clara
Schumannová	Schumannový	k2eAgFnSc1d1	Schumannový
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Eugene	Eugen	k1gMnSc5	Eugen
Gates	Gates	k1gMnSc1	Gates
<g/>
:	:	kIx,	:
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumanna	k1gFnPc2	Schumanna
<g/>
:	:	kIx,	:
A	a	k9	a
Composer	Composer	k1gInSc1	Composer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wife	Wife	k1gFnPc7	Wife
as	as	k9	as
Composer	Composer	k1gInSc4	Composer
</s>
</p>
<p>
<s>
Soupis	soupis	k1gInSc1	soupis
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
o	o	k7c6	o
Claře	Clara	k1gFnSc6	Clara
Schumanové	Schumanová	k1gFnSc2	Schumanová
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ŽivotopisV	ŽivotopisV	k?	ŽivotopisV
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Clara	Clara	k1gFnSc1	Clara
Schumann	Schumann	k1gNnSc4	Schumann
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
