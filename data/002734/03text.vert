<s>
Musala	Musala	k1gFnSc1	Musala
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
М	М	k?	М
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
2925	[number]	k4	2925
m	m	kA	m
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
a	a	k8xC	a
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Balkánu	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Musala	Musala	k1gFnSc1	Musala
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Rila	Ril	k1gInSc2	Ril
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rilském	rilský	k2eAgInSc6d1	rilský
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
nedaleko	nedaleko	k7c2	nedaleko
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Sofie	Sofia	k1gFnSc2	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
hory	hora	k1gFnSc2	hora
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
z	z	k7c2	z
"	"	kIx"	"
<g/>
Mus	Musa	k1gFnPc2	Musa
Allah	Allah	k1gInSc1	Allah
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Alláhova	Alláhův	k2eAgFnSc1d1	Alláhova
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
součástí	součást	k1gFnPc2	součást
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
bulharské	bulharský	k2eAgNnSc1d1	bulharské
jméno	jméno	k1gNnSc1	jméno
hory	hora	k1gFnSc2	hora
je	být	k5eAaImIp3nS	být
Tangra	Tangra	k1gFnSc1	Tangra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
po	po	k7c6	po
sovětském	sovětský	k2eAgMnSc6d1	sovětský
vůdci	vůdce	k1gMnSc6	vůdce
Stalinovi	Stalin	k1gMnSc6	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Musala	Musala	k1gFnSc1	Musala
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
svojí	svůj	k3xOyFgFnSc7	svůj
bohatou	bohatý	k2eAgFnSc7d1	bohatá
florou	flora	k1gFnSc7	flora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
například	například	k6eAd1	například
makedonskou	makedonský	k2eAgFnSc4d1	makedonská
borovici	borovice	k1gFnSc4	borovice
a	a	k8xC	a
bulharský	bulharský	k2eAgInSc1d1	bulharský
smrk	smrk	k1gInSc1	smrk
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
faunou	fauna	k1gFnSc7	fauna
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
výskytem	výskyt	k1gInSc7	výskyt
různých	různý	k2eAgInPc2d1	různý
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Musala	Musala	k1gFnSc1	Musala
má	mít	k5eAaImIp3nS	mít
subarktické	subarktický	k2eAgNnSc4d1	subarktické
podnebí	podnebí	k1gNnSc4	podnebí
s	s	k7c7	s
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
chladnými	chladný	k2eAgFnPc7d1	chladná
zimami	zima	k1gFnPc7	zima
a	a	k8xC	a
krátkými	krátký	k2eAgNnPc7d1	krátké
léty	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
teploty	teplota	k1gFnSc2	teplota
obvykle	obvykle	k6eAd1	obvykle
nepřekračují	překračovat	k5eNaImIp3nP	překračovat
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
po	po	k7c4	po
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
dostávají	dostávat	k5eAaImIp3nP	dostávat
nad	nad	k7c7	nad
14-15	[number]	k4	14-15
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
období	období	k1gNnSc6	období
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
byla	být	k5eAaImAgFnS	být
absolutně	absolutně	k6eAd1	absolutně
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
18,7	[number]	k4	18,7
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
-31,2	-31,2	k4	-31,2
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejpohodlnější	pohodlný	k2eAgInSc1d3	nejpohodlnější
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Musaly	Musala	k1gFnSc2	Musala
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
lyžařského	lyžařský	k2eAgNnSc2d1	lyžařské
střediska	středisko	k1gNnSc2	středisko
Borovec	Borovec	k1gInSc1	Borovec
(	(	kIx(	(
<g/>
Б	Б	k?	Б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
vzdálen	vzdálen	k2eAgInSc4d1	vzdálen
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Borovce	Borovec	k1gMnSc2	Borovec
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnSc1	vesnice
Bistrica	Bistrica	k1gFnSc1	Bistrica
(	(	kIx(	(
<g/>
Б	Б	k?	Б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
8	[number]	k4	8
km	km	kA	km
nastoupá	nastoupat	k5eAaPmIp3nS	nastoupat
k	k	k7c3	k
chatě	chata	k1gFnSc3	chata
Musala	Musal	k1gMnSc2	Musal
(	(	kIx(	(
<g/>
х	х	k?	х
М	М	k?	М
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2389	[number]	k4	2389
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
občerstvit	občerstvit	k5eAaPmF	občerstvit
i	i	k8xC	i
přenocovat	přenocovat	k5eAaPmF	přenocovat
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
dál	daleko	k6eAd2	daleko
stoupá	stoupat	k5eAaImIp3nS	stoupat
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
Ledenoto	Ledenota	k1gFnSc5	Ledenota
(	(	kIx(	(
<g/>
Л	Л	k?	Л
е	е	k?	е
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
3	[number]	k4	3
km	km	kA	km
od	od	k7c2	od
chaty	chata	k1gFnSc2	chata
Musala	Musal	k1gMnSc2	Musal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
nějž	jenž	k3xRgNnSc2	jenž
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2709	[number]	k4	2709
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
stojí	stát	k5eAaImIp3nS	stát
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bulharský	bulharský	k2eAgInSc1d1	bulharský
horský	horský	k2eAgInSc1d1	horský
přístřešek	přístřešek	k1gInSc1	přístřešek
zvaný	zvaný	k2eAgInSc1d1	zvaný
Everest	Everest	k1gInSc4	Everest
(	(	kIx(	(
<g/>
х	х	k?	х
Л	Л	k?	Л
е	е	k?	е
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
doplnit	doplnit	k5eAaPmF	doplnit
tekutiny	tekutina	k1gFnPc4	tekutina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
přenocovat	přenocovat	k5eAaPmF	přenocovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
už	už	k6eAd1	už
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k9	jen
1	[number]	k4	1
km	km	kA	km
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2925	[number]	k4	2925
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
vrcholové	vrcholový	k2eAgFnSc2d1	vrcholová
mohyly	mohyla	k1gFnSc2	mohyla
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
a	a	k8xC	a
pozorovací	pozorovací	k2eAgFnSc1d1	pozorovací
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
výstup	výstup	k1gInSc1	výstup
trvá	trvat	k5eAaImIp3nS	trvat
6-7	[number]	k4	6-7
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Alternativou	alternativa	k1gFnSc7	alternativa
je	být	k5eAaImIp3nS	být
přiblížení	přiblížení	k1gNnSc1	přiblížení
lanovkou	lanovka	k1gFnSc7	lanovka
z	z	k7c2	z
Borovce	Borovec	k1gMnSc2	Borovec
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Jastrebec	Jastrebec	k1gInSc4	Jastrebec
(	(	kIx(	(
<g/>
Я	Я	k?	Я
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2369	[number]	k4	2369
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
po	po	k7c6	po
hřebeni	hřeben	k1gInSc6	hřeben
k	k	k7c3	k
chatě	chata	k1gFnSc3	chata
Musala	Musal	k1gMnSc2	Musal
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
po	po	k7c6	po
zmíněné	zmíněný	k2eAgFnSc6d1	zmíněná
červeně	červeně	k6eAd1	červeně
značené	značený	k2eAgFnSc6d1	značená
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Musala	Musal	k1gMnSc2	Musal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Výstup	výstup	k1gInSc1	výstup
na	na	k7c6	na
Musalu	Musal	k1gInSc6	Musal
na	na	k7c4	na
HoryInfo	HoryInfo	k1gNnSc4	HoryInfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Musalu	Musala	k1gFnSc4	Musala
na	na	k7c4	na
CestouNecestou	CestouNecestý	k2eAgFnSc4d1	CestouNecestý
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Musalu	Musala	k1gFnSc4	Musala
na	na	k7c4	na
SummitPost	SummitPost	k1gFnSc4	SummitPost
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Musalu	Musala	k1gFnSc4	Musala
na	na	k7c4	na
Virtualmountains	Virtualmountains	k1gInSc4	Virtualmountains
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
