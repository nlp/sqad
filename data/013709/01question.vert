<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
šifrovací	šifrovací	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
využívá	využívat	k5eAaImIp3nS
asymetrickou	asymetrický	k2eAgFnSc4d1
kryptografii	kryptografie	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
celočíselných	celočíselný	k2eAgFnPc6d1
mřížích	mříž	k1gFnPc6
<g/>
?	?	kIx.
</s>