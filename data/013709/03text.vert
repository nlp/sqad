<s>
NTRU	NTRU	kA
</s>
<s>
NTRU	NTRU	kA
je	být	k5eAaImIp3nS
patentovaný	patentovaný	k2eAgInSc1d1
šifrovací	šifrovací	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
zašifrování	zašifrování	k1gNnSc4
a	a	k8xC
dešifrování	dešifrování	k1gNnSc4
dat	data	k1gNnPc2
asymetrickou	asymetrický	k2eAgFnSc4d1
kryptografii	kryptografie	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
celočíselných	celočíselný	k2eAgFnPc6d1
mřížích	mříž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
NTRU	NTRU	kA
obsahuje	obsahovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
algoritmy	algoritmus	k1gInPc4
<g/>
:	:	kIx,
NTRUEncrypt	NTRUEncrypt	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
šifrování	šifrování	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
NTRUSign	NTRUSign	k1gInSc1
určený	určený	k2eAgInSc1d1
pro	pro	k7c4
elektronické	elektronický	k2eAgNnSc4d1
podepisování	podepisování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
nejpopulárnějších	populární	k2eAgInPc2d3
asymetrických	asymetrický	k2eAgInPc2d1
šifrovacích	šifrovací	k2eAgInPc2d1
algoritmů	algoritmus	k1gInPc2
založených	založený	k2eAgInPc2d1
například	například	k6eAd1
na	na	k7c6
problému	problém	k1gInSc6
hledání	hledání	k1gNnSc2
prvočíselného	prvočíselný	k2eAgInSc2d1
rozkladu	rozklad	k1gInSc2
nebo	nebo	k8xC
na	na	k7c6
problému	problém	k1gInSc6
nalezení	nalezení	k1gNnSc2
diskrétního	diskrétní	k2eAgInSc2d1
logaritmu	logaritmus	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
teoreticky	teoreticky	k6eAd1
prolomitelné	prolomitelný	k2eAgFnPc1d1
pomocí	pomocí	k7c2
kvantového	kvantový	k2eAgInSc2d1
počítače	počítač	k1gInSc2
Shorovým	Shorův	k2eAgInSc7d1
algoritmem	algoritmus	k1gInSc7
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
pro	pro	k7c4
NTRU	NTRU	kA
znám	znát	k5eAaImIp1nS
žádný	žádný	k3yNgInSc4
útok	útok	k1gInSc4
pomocí	pomocí	k7c2
kvantového	kvantový	k2eAgInSc2d1
počítače	počítač	k1gInSc2
(	(	kIx(
<g/>
postkvantová	postkvantový	k2eAgFnSc1d1
kryptografie	kryptografie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
jeho	jeho	k3xOp3gFnSc1
výkonnost	výkonnost	k1gFnSc1
je	být	k5eAaImIp3nS
prokazatelně	prokazatelně	k6eAd1
lepší	dobrý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
je	být	k5eAaImIp3nS
uzavřená	uzavřený	k2eAgFnSc1d1
placená	placený	k2eAgFnSc1d1
implementace	implementace	k1gFnSc1
NTRU	NTRU	kA
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
volně	volně	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
jako	jako	k8xS,k8xC
otevřený	otevřený	k2eAgInSc1d1
software	software	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
nazvaná	nazvaný	k2eAgFnSc1d1
NTRU	NTRU	kA
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
matematiky	matematika	k1gFnSc2
J.	J.	kA
Hoffsteinem	Hoffstein	k1gMnSc7
<g/>
,	,	kIx,
J.	J.	kA
Pipherem	Pipher	k1gMnSc7
a	a	k8xC
J.	J.	kA
H.	H.	kA
Silvermanem	Silverman	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejného	stejný	k2eAgInSc2d1
roku	rok	k1gInSc2
se	se	k3xPyFc4
tvůrci	tvůrce	k1gMnPc1
spojili	spojit	k5eAaPmAgMnP
s	s	k7c7
D.	D.	kA
Liemanem	Lieman	k1gInSc7
a	a	k8xC
založili	založit	k5eAaPmAgMnP
společnost	společnost	k1gFnSc4
NTRU	NTRU	kA
Cryptosystems	Cryptosystems	k1gInSc4
a	a	k8xC
tento	tento	k3xDgInSc4
kryptosystém	kryptosystý	k2eAgInSc6d1
nechali	nechat	k5eAaPmAgMnP
patentovat	patentovat	k5eAaBmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byla	být	k5eAaImAgFnS
firma	firma	k1gFnSc1
koupena	koupit	k5eAaPmNgFnS
společností	společnost	k1gFnSc7
Security	Securita	k1gFnSc2
Innovation	Innovation	k1gInSc1
zabývající	zabývající	k2eAgInSc1d1
se	s	k7c7
softwarovou	softwarový	k2eAgFnSc7d1
bezpečností	bezpečnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
2013	#num#	k4
přišli	přijít	k5eAaPmAgMnP
Damien	Damino	k1gNnPc2
Stehlé	Stehlý	k2eAgInPc1d1
a	a	k8xC
Ron	ron	k1gInSc1
Steinfeld	Steinfelda	k1gFnPc2
s	s	k7c7
novou	nový	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
algoritmu	algoritmus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
zdarma	zdarma	k6eAd1
(	(	kIx(
<g/>
bez	bez	k7c2
patentových	patentový	k2eAgNnPc2d1
omezení	omezení	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výkonnost	výkonnost	k1gFnSc1
</s>
<s>
NTRU	NTRU	kA
provádí	provádět	k5eAaImIp3nS
nákladné	nákladný	k2eAgFnPc4d1
operace	operace	k1gFnPc4
se	s	k7c7
soukromými	soukromý	k2eAgInPc7d1
klíči	klíč	k1gInPc7
rychleji	rychle	k6eAd2
než	než	k8xS
RSA	RSA	kA
při	při	k7c6
ekvivalentní	ekvivalentní	k2eAgFnSc6d1
kryptografické	kryptografický	k2eAgFnSc6d1
síle	síla	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
rostoucí	rostoucí	k2eAgFnSc7d1
velikostí	velikost	k1gFnSc7
klíče	klíč	k1gInSc2
počet	počet	k1gInSc1
vykonaných	vykonaný	k2eAgFnPc2d1
operací	operace	k1gFnPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
u	u	k7c2
RSA	RSA	kA
roste	růst	k5eAaImIp3nS
kubicky	kubicky	k6eAd1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
NTRU	NTRU	kA
roste	růst	k5eAaImIp3nS
jen	jen	k9
kvadraticky	kvadraticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Oddělení	oddělení	k1gNnSc2
Elektrotechniky	elektrotechnika	k1gFnSc2
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Lovani	Lovaň	k1gFnSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
S	s	k7c7
použitím	použití	k1gNnSc7
Grafické	grafický	k2eAgFnSc2d1
karty	karta	k1gFnSc2
GTX280	GTX280	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dosaženo	dosáhnout	k5eAaPmNgNnS
až	až	k9
200	#num#	k4
000	#num#	k4
šifrování	šifrování	k1gNnPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
na	na	k7c6
úrovni	úroveň	k1gFnSc6
zabezpečení	zabezpečení	k1gNnSc2
256	#num#	k4
bitů	bit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
to	ten	k3xDgNnSc4
porovnáme	porovnat	k5eAaPmIp1nP
s	s	k7c7
rychlostí	rychlost	k1gFnSc7
symetrické	symetrický	k2eAgFnSc2d1
šifry	šifra	k1gFnSc2
(	(	kIx(
<g/>
běžně	běžně	k6eAd1
se	se	k3xPyFc4
neporovnávají	porovnávat	k5eNaImIp3nP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
asi	asi	k9
20	#num#	k4
<g/>
krát	krát	k6eAd1
pomalejší	pomalý	k2eAgFnSc1d2
než	než	k8xS
poslední	poslední	k2eAgFnSc1d1
implementace	implementace	k1gFnSc1
AES	AES	kA
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odolnost	odolnost	k1gFnSc1
vůči	vůči	k7c3
útokům	útok	k1gInPc3
pomocí	pomoc	k1gFnPc2
kvantových	kvantový	k2eAgFnPc2d1
počítačů	počítač	k1gMnPc2
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
RSA	RSA	kA
a	a	k8xC
Kryptografie	kryptografie	k1gFnSc1
nad	nad	k7c7
eliptickými	eliptický	k2eAgFnPc7d1
křivkami	křivka	k1gFnPc7
(	(	kIx(
<g/>
angl.	angl.	k?
ECC	ECC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
u	u	k7c2
NTRU	NTRU	kA
známá	známý	k2eAgFnSc1d1
žádná	žádný	k3yNgFnSc1
zranitelnost	zranitelnost	k1gFnSc1
vůči	vůči	k7c3
útokům	útok	k1gInPc3
pomocí	pomoc	k1gFnPc2
kvantového	kvantový	k2eAgInSc2d1
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NIST	NIST	kA
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
přehledu	přehled	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
že	že	k8xS
<g/>
,	,	kIx,
„	„	k?
<g/>
NTRU	NTRU	kA
je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
alternativa	alternativa	k1gFnSc1
jak	jak	k8xC,k8xS
pro	pro	k7c4
šifrování	šifrování	k1gNnSc4
s	s	k7c7
veřejným	veřejný	k2eAgInSc7d1
klíčem	klíč	k1gInSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
pro	pro	k7c4
elektronické	elektronický	k2eAgInPc4d1
podpisy	podpis	k1gInPc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
není	být	k5eNaImIp3nS
zranitelná	zranitelný	k2eAgNnPc4d1
Shorovým	Shorův	k2eAgInSc7d1
algoritmem	algoritmus	k1gInSc7
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
z	z	k7c2
různých	různý	k2eAgInPc2d1
šifrovacích	šifrovací	k2eAgInPc2d1
systémů	systém	k1gInPc2
založených	založený	k2eAgInPc2d1
na	na	k7c6
mřížích	mříž	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
vyvinuty	vyvinout	k5eAaPmNgFnP
<g/>
,	,	kIx,
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
být	být	k5eAaImF
NTRU	NTRU	kA
nejpraktičtější	praktický	k2eAgFnSc7d3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Standardizace	standardizace	k1gFnSc1
</s>
<s>
Standard	standard	k1gInSc1
IEEE	IEEE	kA
1363.1	1363.1	k4
<g/>
,	,	kIx,
vydaný	vydaný	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
,	,	kIx,
standardizuje	standardizovat	k5eAaBmIp3nS
asymetrickou	asymetrický	k2eAgFnSc4d1
kryptografii	kryptografie	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
mřížích	mříž	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
NTRUEncrypt	NTRUEncrypt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Standard	standard	k1gInSc1
X	X	kA
<g/>
9.98	9.98	k4
standardizuje	standardizovat	k5eAaBmIp3nS
asymetrickou	asymetrický	k2eAgFnSc4d1
kryptografii	kryptografie	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
mřížích	mříž	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
NTRUEncrypt	NTRUEncrypt	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
část	část	k1gFnSc1
standardu	standard	k1gInSc2
X9	X9	k1gFnSc2
pro	pro	k7c4
odvětví	odvětví	k1gNnSc4
finančních	finanční	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Implementace	implementace	k1gFnSc1
</s>
<s>
NTRU	NTRU	kA
je	být	k5eAaImIp3nS
dostupné	dostupný	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
open	open	k1gNnSc1
source	sourec	k1gInSc2
knihovna	knihovna	k1gFnSc1
v	v	k7c6
jazyce	jazyk	k1gInSc6
C	C	kA
nebo	nebo	k8xC
Java	Java	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
uzavřená	uzavřený	k2eAgFnSc1d1
placená	placený	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
NTRU	NTRU	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
BUKTU	BUKTU	kA
<g/>
,	,	kIx,
Tim	Tim	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
NTRU	NTRU	kA
<g/>
:	:	kIx,
Quantum-Resistant	Quantum-Resistant	k1gMnSc1
cryptography	cryptographa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NTRU	NTRU	kA
Cryptosystems	Cryptosystemsa	k1gFnPc2
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ROBERTSON	ROBERTSON	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
D.	D.	kA
RE	re	k9
<g/>
:	:	kIx,
NTRU	NTRU	kA
Public	publicum	k1gNnPc2
Key	Key	k1gFnPc2
Algorithms	Algorithmsa	k1gFnPc2
IP	IP	kA
Assurance	Assurance	k1gFnPc1
Statement	Statement	k1gMnSc1
for	forum	k1gNnPc2
802.15	802.15	k4
<g/>
.3	.3	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
<g/>
,	,	kIx,
August	August	k1gMnSc1
1	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Security	Securit	k1gInPc1
Innovation	Innovation	k1gInSc1
<g/>
:	:	kIx,
Security	Securita	k1gFnSc2
Innovation	Innovation	k1gInSc1
acquires	acquires	k1gMnSc1
NTRU	NTRU	kA
Cryptosystems	Cryptosystems	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
leading	leading	k1gInSc1
security	securita	k1gFnSc2
solutions	solutionsa	k1gFnPc2
provider	provider	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
embedded	embedded	k1gInSc1
security	securita	k1gFnSc2
market	market	k1gInSc1
<g/>
,	,	kIx,
tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
4	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Dostupné	dostupný	k2eAgFnSc2d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
press	pressit	k5eAaPmRp2nS
release	release	k6eAd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
17	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
https://eprint.iacr.org/2013/004.pdf	https://eprint.iacr.org/2013/004.pdf	k1gInSc1
-	-	kIx~
Making	Making	k1gInSc1
NTRUEnrypt	NTRUEnrypt	k2eAgInSc1d1
and	and	k?
NTRUSign	NTRUSign	k1gInSc1
as	as	k1gInSc1
Seure	Seur	k1gInSc5
as	as	k9
Standard	standard	k1gInSc1
Worst-Case	Worst-Casa	k1gFnSc6
Problems	Problemsa	k1gFnPc2
over	over	k1gMnSc1
Ideal	Ideal	k1gMnSc1
Latties	Latties	k1gMnSc1
<g/>
↑	↑	k?
https://globenewswire.com/news-release/2017/03/28/945815/0/en/Security-Innovation-Makes-NTRUEncrypt-Patent-Free.html	https://globenewswire.com/news-release/2017/03/28/945815/0/en/Security-Innovation-Makes-NTRUEncrypt-Patent-Free.html	k1gInSc1
-	-	kIx~
Security	Securita	k1gFnSc2
Innovation	Innovation	k1gInSc1
Makes	Makes	k1gMnSc1
NTRUEncrypt	NTRUEncrypt	k1gMnSc1
Patent-Free	Patent-Fre	k1gFnSc2
<g/>
↑	↑	k?
HERMANS	HERMANS	kA
<g/>
,	,	kIx,
Jens	Jens	k1gInSc1
<g/>
;	;	kIx,
VERCAUTEREN	VERCAUTEREN	kA
<g/>
,	,	kIx,
Frederik	Frederik	k1gMnSc1
<g/>
;	;	kIx,
PRENEEL	PRENEEL	kA
<g/>
,	,	kIx,
Bart	Bart	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speed	Speed	k1gInSc1
Records	Records	k1gInSc4
for	forum	k1gNnPc2
NTRU	NTRU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Pieprzyk	Pieprzyka	k1gFnPc2
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Topics	Topics	k1gInSc1
in	in	k?
Cryptography	Cryptographa	k1gFnSc2
-	-	kIx~
CT-RSA	CT-RSA	k1gFnSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
San	San	k1gMnSc1
Francisco	Francisco	k1gMnSc1
<g/>
,	,	kIx,
CA	ca	kA
<g/>
:	:	kIx,
Springer	Springer	k1gMnSc1
Berlin	berlina	k1gFnPc2
Heidelberg	Heidelberg	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
73	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
4	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
302	#num#	k4
<g/>
-	-	kIx~
<g/>
9743	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
11924	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
11925	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
_	_	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PERLNER	PERLNER	kA
<g/>
,	,	kIx,
Ray	Ray	k1gMnSc1
A.	A.	kA
<g/>
;	;	kIx,
COOPER	COOPER	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
A.	A.	kA
Quantum	Quantum	k1gNnSc1
resistant	resistant	k1gMnSc1
public	publicum	k1gNnPc2
key	key	k?
cryptography	cryptographa	k1gFnSc2
<g/>
:	:	kIx,
a	a	k8xC
survey	survea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Seamons	Seamonsa	k1gFnPc2
Kent	Kenta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
8	#num#	k4
<g/>
th	th	k?
Symposium	symposium	k1gNnSc1
on	on	k3xPp3gMnSc1
Identity	identita	k1gFnPc4
and	and	k?
Trust	trust	k1gInSc1
on	on	k3xPp3gInSc1
the	the	k?
Internet	Internet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
NY	NY	kA
<g/>
:	:	kIx,
ACM	ACM	kA
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
85	#num#	k4
<g/>
–	–	k?
<g/>
93	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
3	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
60558	#num#	k4
<g/>
-	-	kIx~
<g/>
474	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.114	10.114	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1527017.1527028	1527017.1527028	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
middleware	middlewar	k1gMnSc5
<g/>
.	.	kIx.
<g/>
internet	internet	k1gInSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
grouper	grouper	k1gInSc1
<g/>
.	.	kIx.
<g/>
ieee	ieee	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.businesswire.com/news/home/20110411005309/en/Security-Innovation%E2%80%99s-NTRUEncrypt-Adopted-X9-Standard-Data	http://www.businesswire.com/news/home/20110411005309/en/Security-Innovation%E2%80%99s-NTRUEncrypt-Adopted-X9-Standard-Dat	k2eAgFnSc1d1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.securityinnovation.com	www.securityinnovation.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
