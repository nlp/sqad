<s>
NTRU	NTRU	kA
je	být	k5eAaImIp3nS
patentovaný	patentovaný	k2eAgInSc1d1
šifrovací	šifrovací	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
zašifrování	zašifrování	k1gNnSc4
a	a	k8xC
dešifrování	dešifrování	k1gNnSc4
dat	data	k1gNnPc2
asymetrickou	asymetrický	k2eAgFnSc4d1
kryptografii	kryptografie	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
celočíselných	celočíselný	k2eAgFnPc6d1
mřížích	mříž	k1gFnPc6
<g/>
.	.	kIx.
</s>