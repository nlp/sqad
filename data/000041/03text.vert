<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
Romů	Rom	k1gMnPc2	Rom
konaný	konaný	k2eAgMnSc1d1	konaný
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
je	být	k5eAaImIp3nS	být
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
romskou	romský	k2eAgFnSc4d1	romská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
povědomí	povědomí	k1gNnSc1	povědomí
o	o	k7c6	o
problémech	problém	k1gInPc6	problém
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
Romové	Rom	k1gMnPc1	Rom
čelí	čelit	k5eAaImIp3nP	čelit
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
Romů	Rom	k1gMnPc2	Rom
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
vzniku	vznik	k1gInSc2	vznik
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
romské	romský	k2eAgFnSc2d1	romská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
International	International	k1gMnSc2	International
Romani	Roman	k1gMnPc1	Roman
Union	union	k1gInSc4	union
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
World	World	k1gInSc4	World
Romani	Romaň	k1gFnSc3	Romaň
Union	union	k1gInSc1	union
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1971	[number]	k4	1971
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Grattana	Grattan	k1gMnSc2	Grattan
Puxona	Puxon	k1gMnSc2	Puxon
a	a	k8xC	a
Donalda	Donald	k1gMnSc2	Donald
Kenricka	Kenricko	k1gNnSc2	Kenricko
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Matéo	Matéo	k1gNnSc4	Matéo
Maximoffa	Maximoff	k1gMnSc2	Maximoff
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Jarko	Jarka	k1gFnSc5	Jarka
Jovanoviće	Jovanoviće	k1gFnPc7	Jovanoviće
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Delegáti	delegát	k1gMnPc1	delegát
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc6	který
byly	být	k5eAaImAgFnP	být
necelé	celý	k2eNgFnPc1d1	necelá
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc4	desítka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
vzhledu	vzhled	k1gInSc6	vzhled
romské	romský	k2eAgFnSc2d1	romská
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
romské	romský	k2eAgFnSc2d1	romská
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
preferenci	preference	k1gFnSc6	preference
označení	označení	k1gNnPc2	označení
Rom	Rom	k1gMnSc1	Rom
před	před	k7c4	před
Cikán	cikán	k2eAgInSc4d1	cikán
<g/>
.	.	kIx.	.
</s>
<s>
Romové	Rom	k1gMnPc1	Rom
slaví	slavit	k5eAaImIp3nP	slavit
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
Romů	Rom	k1gMnPc2	Rom
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
na	na	k7c6	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
romské	romský	k2eAgFnSc2d1	romská
unie	unie	k1gFnSc2	unie
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
Romů	Rom	k1gMnPc2	Rom
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Přehled	přehled	k1gInSc1	přehled
akcí	akce	k1gFnPc2	akce
konaných	konaný	k2eAgFnPc2d1	konaná
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
