<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
Romů	Rom	k1gMnPc2	Rom
konaný	konaný	k2eAgMnSc1d1	konaný
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
je	být	k5eAaImIp3nS	být
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
romskou	romský	k2eAgFnSc4d1	romská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
povědomí	povědomí	k1gNnSc1	povědomí
o	o	k7c6	o
problémech	problém	k1gInPc6	problém
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
Romové	Rom	k1gMnPc1	Rom
čelí	čelit	k5eAaImIp3nP	čelit
<g/>
.	.	kIx.	.
</s>
