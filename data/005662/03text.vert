<s>
Fantasy	fantas	k1gInPc4	fantas
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc1d1	umělecký
žánr	žánr	k1gInSc1	žánr
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
především	především	k9	především
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
především	především	k9	především
na	na	k7c4	na
užití	užití	k1gNnSc4	užití
magie	magie	k1gFnSc2	magie
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
nadpřirozených	nadpřirozený	k2eAgInPc2d1	nadpřirozený
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
bájné	bájný	k2eAgFnPc1d1	bájná
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
bohové	bůh	k1gMnPc1	bůh
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
z	z	k7c2	z
antických	antický	k2eAgFnPc2d1	antická
dob	doba	k1gFnPc2	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
vzezřením	vzezření	k1gNnSc7	vzezření
a	a	k8xC	a
chováním	chování	k1gNnSc7	chování
netypickým	typický	k2eNgNnSc7d1	netypické
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglosaském	anglosaský	k2eAgInSc6d1	anglosaský
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
však	však	k9	však
fantasy	fantas	k1gInPc1	fantas
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
dílo	dílo	k1gNnSc4	dílo
fantastického	fantastický	k2eAgInSc2d1	fantastický
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Žánr	žánr	k1gInSc1	žánr
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
žánry	žánr	k1gInPc1	žánr
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
a	a	k8xC	a
horror	horror	k1gInSc1	horror
definován	definovat	k5eAaBmNgInS	definovat
především	především	k6eAd1	především
rekvizitami	rekvizita	k1gFnPc7	rekvizita
a	a	k8xC	a
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
a	a	k8xC	a
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc1	dílo
situováno	situován	k2eAgNnSc1d1	situováno
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
často	často	k6eAd1	často
"	"	kIx"	"
<g/>
druhotný	druhotný	k2eAgInSc1d1	druhotný
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
alternativní	alternativní	k2eAgFnSc1d1	alternativní
realita	realita	k1gFnSc1	realita
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
příběh	příběh	k1gInSc1	příběh
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
naším	náš	k3xOp1gInSc7	náš
nějak	nějak	k6eAd1	nějak
spojený	spojený	k2eAgInSc1d1	spojený
(	(	kIx(	(
<g/>
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
)	)	kIx)	)
či	či	k8xC	či
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
nějak	nějak	k6eAd1	nějak
pozměněném	pozměněný	k2eAgInSc6d1	pozměněný
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
žánr	žánr	k1gInSc4	žánr
alternativní	alternativní	k2eAgFnSc2d1	alternativní
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
současné	současný	k2eAgInPc4d1	současný
fantasy	fantas	k1gInPc4	fantas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
žánru	žánr	k1gInSc2	žánr
fantasy	fantas	k1gInPc4	fantas
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
především	především	k9	především
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tvořivosti	tvořivost	k1gFnSc6	tvořivost
a	a	k8xC	a
lidské	lidský	k2eAgFnSc3d1	lidská
fantazii	fantazie	k1gFnSc3	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
literárních	literární	k2eAgInPc2d1	literární
žánrů	žánr	k1gInPc2	žánr
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
dřívější	dřívější	k2eAgInPc4d1	dřívější
typy	typ	k1gInPc4	typ
jako	jako	k8xC	jako
např.	např.	kA	např.
mýty	mýtus	k1gInPc4	mýtus
a	a	k8xC	a
báje	báj	k1gFnPc4	báj
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
středověké	středověký	k2eAgFnPc1d1	středověká
legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
rytířské	rytířský	k2eAgInPc1d1	rytířský
eposy	epos	k1gInPc1	epos
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
klasického	klasický	k2eAgInSc2d1	klasický
fantasy	fantas	k1gInPc4	fantas
však	však	k9	však
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
až	až	k9	až
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
u	u	k7c2	u
autorů	autor	k1gMnPc2	autor
jako	jako	k9	jako
George	George	k1gFnPc2	George
MacDonald	Macdonald	k1gMnSc1	Macdonald
a	a	k8xC	a
lord	lord	k1gMnSc1	lord
Dunsany	Dunsana	k1gFnSc2	Dunsana
<g/>
,	,	kIx,	,
masivnější	masivní	k2eAgInSc1d2	masivnější
nástup	nástup	k1gInSc1	nástup
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
díly	dílo	k1gNnPc7	dílo
J.	J.	kA	J.
<g/>
R.	R.	kA	R.
<g/>
R.	R.	kA	R.
Tolkiena	Tolkiena	k1gFnSc1	Tolkiena
a	a	k8xC	a
R.	R.	kA	R.
E.	E.	kA	E.
Howarda	Howard	k1gMnSc2	Howard
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
Tolkienova	Tolkienův	k2eAgFnSc1d1	Tolkienova
trilogie	trilogie	k1gFnSc1	trilogie
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zapsala	zapsat	k5eAaPmAgFnS	zapsat
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
populární	populární	k2eAgFnSc2d1	populární
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
pilířů	pilíř	k1gInPc2	pilíř
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Podžánry	Podžánra	k1gFnSc2	Podžánra
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
řada	řada	k1gFnSc1	řada
podžánrů	podžánr	k1gInPc2	podžánr
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
nepanují	panovat	k5eNaImIp3nP	panovat
ostré	ostrý	k2eAgFnPc1d1	ostrá
hranice	hranice	k1gFnPc1	hranice
a	a	k8xC	a
podžánry	podžánra	k1gFnPc1	podžánra
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinská	hrdinský	k2eAgFnSc1d1	hrdinská
fantasy	fantas	k1gInPc1	fantas
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc1	dílo
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkiena	k1gFnSc1	Tolkiena
nebo	nebo	k8xC	nebo
Kolo	kolo	k1gNnSc1	kolo
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
romány	román	k1gInPc4	román
od	od	k7c2	od
Davida	David	k1gMnSc2	David
Gemmella	Gemmell	k1gMnSc2	Gemmell
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
fantasy	fantas	k1gInPc1	fantas
<g/>
:	:	kIx,	:
Prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
reálnými	reálný	k2eAgFnPc7d1	reálná
historickými	historický	k2eAgFnPc7d1	historická
epochami	epocha	k1gFnPc7	epocha
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
středověkem	středověk	k1gInSc7	středověk
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
příběhy	příběh	k1gInPc1	příběh
dokonce	dokonce	k9	dokonce
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
během	během	k7c2	během
historie	historie	k1gFnSc2	historie
našeho	náš	k3xOp1gInSc2	náš
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Píseň	píseň	k1gFnSc1	píseň
ledu	led	k1gInSc2	led
a	a	k8xC	a
ohně	oheň	k1gInSc2	oheň
či	či	k8xC	či
Hraničářův	hraničářův	k2eAgMnSc1d1	hraničářův
učeň	učeň	k1gMnSc1	učeň
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
a	a	k8xC	a
magie	magie	k1gFnSc1	magie
<g/>
:	:	kIx,	:
Děj	děj	k1gInSc1	děj
většinou	většinou	k6eAd1	většinou
sleduje	sledovat	k5eAaImIp3nS	sledovat
příhody	příhoda	k1gFnPc4	příhoda
a	a	k8xC	a
výpravy	výprava	k1gFnSc2	výprava
hrdiny	hrdina	k1gMnSc2	hrdina
-	-	kIx~	-
bojovníka	bojovník	k1gMnSc2	bojovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
neohroženě	ohroženě	k6eNd1	ohroženě
překonává	překonávat	k5eAaImIp3nS	překonávat
hordy	horda	k1gFnPc4	horda
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Barbar	barbar	k1gMnSc1	barbar
Conan	Conan	k1gMnSc1	Conan
je	být	k5eAaImIp3nS	být
archetypem	archetyp	k1gInSc7	archetyp
takového	takový	k3xDgMnSc2	takový
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Komická	komický	k2eAgFnSc1d1	komická
fantasy	fantas	k1gInPc1	fantas
<g/>
:	:	kIx,	:
Fantastický	fantastický	k2eAgInSc1d1	fantastický
svět	svět	k1gInSc1	svět
většinou	většinou	k6eAd1	většinou
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
kulisa	kulisa	k1gFnSc1	kulisa
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
podivné	podivný	k2eAgFnPc1d1	podivná
a	a	k8xC	a
pitoreskní	pitoreskní	k2eAgFnPc1d1	pitoreskní
příhody	příhoda	k1gFnPc1	příhoda
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
se	s	k7c7	s
satirickým	satirický	k2eAgInSc7d1	satirický
či	či	k8xC	či
parodickým	parodický	k2eAgInSc7d1	parodický
podtextem	podtext	k1gInSc7	podtext
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
ke	k	k7c3	k
skutečnostem	skutečnost	k1gFnPc3	skutečnost
z	z	k7c2	z
našeho	náš	k3xOp1gInSc2	náš
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
Terryho	Terry	k1gMnSc4	Terry
Pratchetta	Pratchett	k1gMnSc4	Pratchett
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
fantasy	fantas	k1gInPc1	fantas
<g/>
:	:	kIx,	:
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
ve	v	k7c6	v
městě	město	k1gNnSc6	město
ze	z	k7c2	z
současnosti	současnost	k1gFnSc2	současnost
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
alternativním	alternativní	k2eAgInSc6d1	alternativní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Nádraží	nádraží	k1gNnSc1	nádraží
Perdido	Perdida	k1gFnSc5	Perdida
(	(	kIx(	(
<g/>
China	China	k1gFnSc1	China
Miéville	Miéville	k1gFnSc2	Miéville
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nikdykde	Nikdykd	k1gMnSc5	Nikdykd
(	(	kIx(	(
<g/>
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
trilogie	trilogie	k1gFnSc1	trilogie
Městské	městský	k2eAgFnSc2d1	městská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Renčín	Renčín	k1gMnSc1	Renčín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
(	(	kIx(	(
<g/>
či	či	k8xC	či
spekulativní	spekulativní	k2eAgFnSc1d1	spekulativní
<g/>
)	)	kIx)	)
historie	historie	k1gFnSc1	historie
<g/>
:	:	kIx,	:
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
úvahy	úvaha	k1gFnSc2	úvaha
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nebo	nebo	k8xC	nebo
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
historickém	historický	k2eAgInSc6d1	historický
okamžiku	okamžik	k1gInSc6	okamžik
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
odchylce	odchylka	k1gFnSc3	odchylka
od	od	k7c2	od
naši	náš	k3xOp1gFnSc4	náš
skutečné	skutečný	k2eAgFnPc1d1	skutečná
historie	historie	k1gFnPc1	historie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jiný	jiný	k2eAgInSc1d1	jiný
výsledek	výsledek	k1gInSc1	výsledek
bitvy	bitva	k1gFnSc2	bitva
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
předčasná	předčasný	k2eAgFnSc1d1	předčasná
smrt	smrt	k1gFnSc1	smrt
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
historické	historický	k2eAgFnPc4d1	historická
události	událost	k1gFnPc4	událost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
ráje	ráj	k1gInSc2	ráj
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnSc2	Harra
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
meteorit	meteorit	k1gInSc1	meteorit
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
vyhubení	vyhubení	k1gNnSc4	vyhubení
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc4	zem
minul	minout	k5eAaImAgMnS	minout
a	a	k8xC	a
děj	děj	k1gInSc4	děj
popisuje	popisovat	k5eAaImIp3nS	popisovat
střet	střet	k1gInSc4	střet
lidské	lidský	k2eAgFnSc2d1	lidská
civilizace	civilizace	k1gFnSc2	civilizace
s	s	k7c7	s
civilizací	civilizace	k1gFnSc7	civilizace
inteligentních	inteligentní	k2eAgMnPc2d1	inteligentní
potomků	potomek	k1gMnPc2	potomek
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Paralelní	paralelní	k2eAgFnSc1d1	paralelní
realita	realita	k1gFnSc1	realita
<g/>
:	:	kIx,	:
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
několika	několik	k4yIc6	několik
paralelních	paralelní	k2eAgInPc6d1	paralelní
vesmírech	vesmír	k1gInPc6	vesmír
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yQgInPc7	který
lze	lze	k6eAd1	lze
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
přecházet	přecházet	k5eAaImF	přecházet
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
paralelních	paralelní	k2eAgFnPc2d1	paralelní
realit	realita	k1gFnPc2	realita
může	moct	k5eAaImIp3nS	moct
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
náš	náš	k3xOp1gInSc4	náš
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
trilogie	trilogie	k1gFnSc1	trilogie
Jeho	jeho	k3xOp3gFnSc2	jeho
temné	temný	k2eAgFnSc2d1	temná
esence	esence	k1gFnSc2	esence
(	(	kIx(	(
<g/>
Philip	Philip	k1gMnSc1	Philip
Pullman	Pullman	k1gMnSc1	Pullman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fantasy	fantas	k1gInPc1	fantas
žánr	žánr	k1gInSc4	žánr
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
druzích	druh	k1gInPc6	druh
her	hra	k1gFnPc2	hra
od	od	k7c2	od
počítačových	počítačový	k2eAgFnPc2d1	počítačová
přes	přes	k7c4	přes
deskové	deskový	k2eAgInPc4d1	deskový
až	až	k9	až
po	po	k7c4	po
larpy	larp	k1gMnPc4	larp
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
FPS	FPS	kA	FPS
(	(	kIx(	(
<g/>
Heretic	Heretice	k1gFnPc2	Heretice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tahových	tahový	k2eAgFnPc6d1	tahová
strategiích	strategie	k1gFnPc6	strategie
(	(	kIx(	(
<g/>
Heroes	Heroesa	k1gFnPc2	Heroesa
of	of	k?	of
Might	Might	k1gMnSc1	Might
and	and	k?	and
Magic	Magic	k1gMnSc1	Magic
<g/>
,	,	kIx,	,
Warlords	Warlords	k1gInSc1	Warlords
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
realtimových	realtimův	k2eAgFnPc6d1	realtimův
strategiích	strategie	k1gFnPc6	strategie
(	(	kIx(	(
<g/>
Warcraft	Warcrafta	k1gFnPc2	Warcrafta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
RPG	RPG	kA	RPG
(	(	kIx(	(
<g/>
Baldur	Baldur	k1gMnSc1	Baldur
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gate	Gate	k1gFnSc7	Gate
<g/>
,	,	kIx,	,
legendární	legendární	k2eAgFnSc1d1	legendární
Diablo	Diablo	k1gFnSc1	Diablo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
MMORPG	MMORPG	kA	MMORPG
(	(	kIx(	(
<g/>
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
,	,	kIx,	,
Lineage	Lineage	k1gFnSc1	Lineage
2	[number]	k4	2
<g/>
,	,	kIx,	,
Ultima	ultimo	k1gNnSc2	ultimo
Online	Onlin	k1gInSc5	Onlin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
MUD	MUD	kA	MUD
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
deskové	deskový	k2eAgFnPc4d1	desková
hry	hra	k1gFnPc4	hra
s	s	k7c7	s
fantasy	fantas	k1gInPc7	fantas
tematikou	tematika	k1gFnSc7	tematika
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Magic	Magic	k1gMnSc1	Magic
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Gathering	Gathering	k1gInSc1	Gathering
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
též	též	k9	též
stolní	stolní	k2eAgFnPc4d1	stolní
hry	hra	k1gFnPc4	hra
založené	založený	k2eAgFnPc4d1	založená
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
knihách	kniha	k1gFnPc6	kniha
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takové	takový	k3xDgFnSc2	takový
stolní	stolní	k2eAgFnSc2d1	stolní
společenské	společenský	k2eAgFnSc2d1	společenská
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
Zeměplocha	Zeměploch	k1gMnSc4	Zeměploch
<g/>
:	:	kIx,	:
Ankh-Morpork	Ankh-Morpork	k1gInSc1	Ankh-Morpork
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ankh-Morpork	Ankh-Morpork	k1gInSc1	Ankh-Morpork
<g/>
)	)	kIx)	)
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
hry	hra	k1gFnPc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
do	do	k7c2	do
historických	historický	k2eAgFnPc2d1	historická
válečných	válečný	k2eAgFnPc2d1	válečná
her	hra	k1gFnPc2	hra
přidaly	přidat	k5eAaPmAgInP	přidat
fantasy	fantas	k1gInPc1	fantas
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
komerční	komerční	k2eAgFnSc1d1	komerční
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
fantasy	fantas	k1gInPc4	fantas
Dungeons	Dungeonsa	k1gFnPc2	Dungeonsa
&	&	k?	&
Dragons	Dragonsa	k1gFnPc2	Dragonsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc7	její
českou	český	k2eAgFnSc7d1	Česká
obdobou	obdoba	k1gFnSc7	obdoba
je	být	k5eAaImIp3nS	být
Dračí	dračí	k2eAgNnSc4d1	dračí
doupě	doupě	k1gNnSc4	doupě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Larpy	Larpa	k1gFnPc1	Larpa
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
hry	hra	k1gFnSc2	hra
naživo	naživo	k1gNnSc1	naživo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
fantasy	fantas	k1gInPc4	fantas
řadí	řadit	k5eAaImIp3nP	řadit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
mezi	mezi	k7c7	mezi
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
<g/>
.	.	kIx.	.
</s>
<s>
Podžánry	Podžánra	k1gFnPc1	Podžánra
fantasy	fantas	k1gInPc4	fantas
Fantastika	fantastika	k1gFnSc1	fantastika
Sci-fi	scii	k1gFnSc1	sci-fi
Horor	horor	k1gInSc4	horor
Seznam	seznam	k1gInSc1	seznam
spisovatelů	spisovatel	k1gMnPc2	spisovatel
fantasy	fantas	k1gInPc1	fantas
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
fantasy	fantas	k1gInPc7	fantas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
FANTAZEEN	FANTAZEEN	kA	FANTAZEEN
-	-	kIx~	-
Fantasy	fantas	k1gInPc4	fantas
magazín	magazín	k1gInSc1	magazín
publikující	publikující	k2eAgFnSc2d1	publikující
reportáže	reportáž	k1gFnSc2	reportáž
a	a	k8xC	a
pozvánky	pozvánka	k1gFnSc2	pozvánka
na	na	k7c4	na
Larpy	Larp	k1gMnPc4	Larp
<g/>
,	,	kIx,	,
Cony	Con	k1gMnPc4	Con
<g/>
,	,	kIx,	,
Dřevářny	Dřevářn	k1gMnPc4	Dřevářn
<g/>
.	.	kIx.	.
</s>
<s>
KALENDÁŘ	kalendář	k1gInSc1	kalendář
CONŮ	CONŮ	kA	CONŮ
Topzine	Topzin	k1gInSc5	Topzin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
kulturní	kulturní	k2eAgInSc1d1	kulturní
portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
magazínová	magazínový	k2eAgFnSc1d1	magazínová
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
i	i	k9	i
fantastice	fantastika	k1gFnSc3	fantastika
SFF	SFF	kA	SFF
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
o	o	k7c6	o
science	scienka	k1gFnSc6	scienka
fiction	fiction	k1gInSc4	fiction
a	a	k8xC	a
fantasy	fantas	k1gInPc4	fantas
Fantasy	fantas	k1gInPc1	fantas
Planet	planeta	k1gFnPc2	planeta
-	-	kIx~	-
web	web	k1gInSc4	web
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
literární	literární	k2eAgFnSc7d1	literární
<g/>
,	,	kIx,	,
filmovou	filmový	k2eAgFnSc7d1	filmová
<g/>
,	,	kIx,	,
herní	herní	k2eAgFnSc7d1	herní
a	a	k8xC	a
komiksovou	komiksový	k2eAgFnSc7d1	komiksová
fantastikou	fantastika	k1gFnSc7	fantastika
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
recenze	recenze	k1gFnPc1	recenze
<g/>
,	,	kIx,	,
rozhovory	rozhovor	k1gInPc1	rozhovor
<g/>
,	,	kIx,	,
reportáže	reportáž	k1gFnPc1	reportáž
<g/>
,	,	kIx,	,
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
kompletní	kompletní	k2eAgFnSc1d1	kompletní
databáze	databáze	k1gFnSc1	databáze
žánrových	žánrový	k2eAgFnPc2d1	žánrová
knih	kniha	k1gFnPc2	kniha
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
diskuze	diskuze	k1gFnPc4	diskuze
<g/>
)	)	kIx)	)
MFantasy	MFantas	k1gInPc4	MFantas
magazín	magazín	k1gInSc1	magazín
-	-	kIx~	-
on-line	onin	k1gInSc5	on-lin
magazín	magazín	k1gInSc1	magazín
s	s	k7c7	s
fantasy	fantas	k1gInPc7	fantas
povídkami	povídka	k1gFnPc7	povídka
<g/>
,	,	kIx,	,
magazíny	magazín	k1gInPc7	magazín
v	v	k7c6	v
PDF	PDF	kA	PDF
LEGIE	legie	k1gFnSc1	legie
-	-	kIx~	-
databáze	databáze	k1gFnSc1	databáze
knih	kniha	k1gFnPc2	kniha
fantasy	fantas	k1gInPc1	fantas
a	a	k8xC	a
sci-fi	scii	k1gNnSc1	sci-fi
Neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
pes	pes	k1gMnSc1	pes
<g/>
:	:	kIx,	:
Žánr	žánr	k1gInSc4	žánr
fantasy	fantas	k1gInPc7	fantas
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
</s>
