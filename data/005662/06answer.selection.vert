<s>
Fantasy	fantas	k1gInPc4	fantas
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc1d1	umělecký
žánr	žánr	k1gInSc1	žánr
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
především	především	k9	především
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
především	především	k9	především
na	na	k7c4	na
užití	užití	k1gNnSc4	užití
magie	magie	k1gFnSc2	magie
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
nadpřirozených	nadpřirozený	k2eAgInPc2d1	nadpřirozený
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
bájné	bájný	k2eAgFnPc1d1	bájná
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
bohové	bůh	k1gMnPc1	bůh
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
z	z	k7c2	z
antických	antický	k2eAgFnPc2d1	antická
dob	doba	k1gFnPc2	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
vzezřením	vzezření	k1gNnSc7	vzezření
a	a	k8xC	a
chováním	chování	k1gNnSc7	chování
netypickým	typický	k2eNgNnSc7d1	netypické
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
