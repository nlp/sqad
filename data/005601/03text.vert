<s>
Yperit	yperit	k1gInSc1	yperit
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgInSc1d1	zvaný
také	také	k9	také
hořčičný	hořčičný	k2eAgInSc1d1	hořčičný
plyn	plyn	k1gInSc1	plyn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vojenský	vojenský	k2eAgInSc1d1	vojenský
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
ho	on	k3xPp3gNnSc4	on
Brit	Brit	k1gMnSc1	Brit
Frederick	Frederick	k1gMnSc1	Frederick
Guthrie	Guthrie	k1gFnSc1	Guthrie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
boji	boj	k1gInSc6	boj
použit	použít	k5eAaPmNgInS	použít
v	v	k7c4	v
září	září	k1gNnSc4	září
1917	[number]	k4	1917
u	u	k7c2	u
belgického	belgický	k2eAgNnSc2d1	Belgické
města	město	k1gNnSc2	město
Yprè	Yprè	k1gFnSc2	Yprè
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
získal	získat	k5eAaPmAgInS	získat
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
zpuchýřujících	zpuchýřující	k2eAgFnPc2d1	zpuchýřující
bojových	bojový	k2eAgFnPc2d1	bojová
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
leptáním	leptání	k1gNnSc7	leptání
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Yperit	yperit	k1gInSc1	yperit
proniká	pronikat	k5eAaImIp3nS	pronikat
oděvem	oděv	k1gInSc7	oděv
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
působení	působení	k1gNnSc1	působení
se	se	k3xPyFc4	se
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
na	na	k7c6	na
vlhkých	vlhký	k2eAgNnPc6d1	vlhké
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
podpaží	podpaží	k1gNnPc1	podpaží
<g/>
,	,	kIx,	,
genitálie	genitálie	k1gFnPc1	genitálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oběť	oběť	k1gFnSc1	oběť
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cítí	cítit	k5eAaImIp3nS	cítit
vůni	vůně	k1gFnSc4	vůně
česneku	česnek	k1gInSc2	česnek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
sloučenina	sloučenina	k1gFnSc1	sloučenina
alicin	alicina	k1gFnPc2	alicina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc2	několik
vdechnutí	vdechnutí	k1gNnPc2	vdechnutí
se	se	k3xPyFc4	se
plíce	plíce	k1gFnPc1	plíce
začnou	začít	k5eAaPmIp3nP	začít
pálit	pálit	k5eAaImF	pálit
a	a	k8xC	a
obět	obět	k?	obět
většinou	většina	k1gFnSc7	většina
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
v	v	k7c6	v
těžkých	těžký	k2eAgFnPc6d1	těžká
bolestech	bolest	k1gFnPc6	bolest
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
spálené	spálený	k2eAgFnPc4d1	spálená
plíce	plíce	k1gFnPc4	plíce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
přivádět	přivádět	k5eAaImF	přivádět
kyslík	kyslík	k1gInSc4	kyslík
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sloučeninu	sloučenina	k1gFnSc4	sloučenina
bis	bis	k?	bis
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-chlorethyl	hlorethyl	k1gInSc1	-chlorethyl
<g/>
)	)	kIx)	)
<g/>
sulfid	sulfid	k1gInSc1	sulfid
(	(	kIx(	(
<g/>
též	též	k9	též
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
dichlorethylthioether	dichlorethylthioethra	k1gFnPc2	dichlorethylthioethra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
nádorové	nádorový	k2eAgFnPc4d1	nádorová
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
yperitu	yperit	k1gInSc2	yperit
vycházejí	vycházet	k5eAaImIp3nP	vycházet
první	první	k4xOgNnPc4	první
cytostatika	cytostatikum	k1gNnPc4	cytostatikum
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
thioether	thioethra	k1gFnPc2	thioethra
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
organický	organický	k2eAgInSc1d1	organický
sulfid	sulfid	k1gInSc1	sulfid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
strukturu	struktura	k1gFnSc4	struktura
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
1,1	[number]	k4	1,1
<g/>
-thio-bis-	hiois-	k?	-thio-bis-
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
-chlorethan	hlorethana	k1gFnPc2	-chlorethana
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
ClCH	ClCH	k1gFnSc1	ClCH
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
2,2	[number]	k4	2,2
<g/>
'	'	kIx"	'
<g/>
-dichlorodiethyl	ichlorodiethyl	k1gInSc1	-dichlorodiethyl
sulfid	sulfid	k1gInSc1	sulfid
nebo	nebo	k8xC	nebo
bis-	bis-	k?	bis-
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-chlorethyl	hlorethyl	k1gInSc1	-chlorethyl
<g/>
)	)	kIx)	)
<g/>
-sulfid	ulfid	k1gInSc1	-sulfid
<g/>
.	.	kIx.	.
</s>
<s>
Hořčičný	hořčičný	k2eAgInSc4d1	hořčičný
plyn	plyn	k1gInSc4	plyn
lze	lze	k6eAd1	lze
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
reakcí	reakce	k1gFnSc7	reakce
jednoho	jeden	k4xCgInSc2	jeden
molu	mol	k1gInSc2	mol
chloridu	chlorid	k1gInSc2	chlorid
sirnatého	sirnatý	k2eAgInSc2d1	sirnatý
(	(	kIx(	(
<g/>
SCl	SCl	k1gFnSc1	SCl
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
moly	molo	k1gNnPc7	molo
ethylenu	ethylen	k1gInSc2	ethylen
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
mechanismus	mechanismus	k1gInSc1	mechanismus
účinku	účinek	k1gInSc2	účinek
yperitu	yperit	k1gInSc2	yperit
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
alkylaci	alkylace	k1gFnSc6	alkylace
guaninu	guanin	k1gInSc2	guanin
DNA	DNA	kA	DNA
v	v	k7c6	v
poloze	poloha	k1gFnSc6	poloha
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc2	spojení
obou	dva	k4xCgInPc2	dva
řetězců	řetězec	k1gInPc2	řetězec
DNA	dno	k1gNnSc2	dno
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
buněčné	buněčný	k2eAgNnSc1d1	buněčné
dělení	dělení	k1gNnSc1	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
dusíkatá	dusíkatý	k2eAgFnSc1d1	dusíkatá
analogie	analogie	k1gFnSc1	analogie
yperitu	yperit	k1gInSc2	yperit
jako	jako	k8xS	jako
kancerostatika	kancerostatikum	k1gNnSc2	kancerostatikum
<g/>
.	.	kIx.	.
</s>
<s>
Puchýře	puchýř	k1gInPc1	puchýř
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
jen	jen	k9	jen
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
prasete	prase	k1gNnSc2	prase
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Sulfidický	sulfidický	k2eAgInSc1d1	sulfidický
yperit	yperit	k1gInSc1	yperit
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
sirný	sirný	k2eAgInSc1d1	sirný
yperit	yperit	k1gInSc1	yperit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bis	bis	k?	bis
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-chlorethyl	hlorethyl	k1gInSc1	-chlorethyl
<g/>
)	)	kIx)	)
sulfid	sulfid	k1gInSc1	sulfid
<g/>
,	,	kIx,	,
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
při	při	k7c6	při
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
olejovitá	olejovitý	k2eAgFnSc1d1	olejovitá
kapalina	kapalina	k1gFnSc1	kapalina
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
zápachem	zápach	k1gInSc7	zápach
po	po	k7c6	po
hořčici	hořčice	k1gFnSc6	hořčice
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
hořčičný	hořčičný	k2eAgInSc1d1	hořčičný
plyn	plyn	k1gInSc1	plyn
–	–	k?	–
mustard	mustard	k1gInSc1	mustard
gas	gas	k?	gas
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
křenu	křen	k1gInSc2	křen
<g/>
,	,	kIx,	,
cibuli	cibule	k1gFnSc6	cibule
či	či	k8xC	či
spálené	spálený	k2eAgFnSc3d1	spálená
gumě	guma	k1gFnSc3	guma
<g/>
.	.	kIx.	.
</s>
<s>
Značení	značení	k1gNnSc1	značení
pro	pro	k7c4	pro
destilovaný	destilovaný	k2eAgInSc4d1	destilovaný
yperit	yperit	k1gInSc4	yperit
je	být	k5eAaImIp3nS	být
HD	HD	kA	HD
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
molekulová	molekulový	k2eAgFnSc1d1	molekulová
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
159	[number]	k4	159
<g/>
.	.	kIx.	.
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1	chemická
struktura	struktura	k1gFnSc1	struktura
yperitu	yperit	k1gInSc2	yperit
<g/>
:	:	kIx,	:
Destilovaný	destilovaný	k2eAgInSc1d1	destilovaný
sulfidický	sulfidický	k2eAgInSc1d1	sulfidický
yperit	yperit	k1gInSc1	yperit
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
bojových	bojový	k2eAgFnPc2d1	bojová
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
německou	německý	k2eAgFnSc7d1	německá
císařskou	císařský	k2eAgFnSc7d1	císařská
armádou	armáda	k1gFnSc7	armáda
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
výzbroje	výzbroj	k1gFnSc2	výzbroj
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
zaváděn	zavádět	k5eAaImNgInS	zavádět
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
všemi	všecek	k3xTgInPc7	všecek
významnými	významný	k2eAgInPc7d1	významný
válčícími	válčící	k2eAgInPc7d1	válčící
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
bojovou	bojový	k2eAgFnSc4d1	bojová
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
aerosolu	aerosol	k1gInSc2	aerosol
<g/>
.	.	kIx.	.
</s>
<s>
Destilovaný	destilovaný	k2eAgInSc1d1	destilovaný
sulfidický	sulfidický	k2eAgInSc1d1	sulfidický
yperit	yperit	k1gInSc1	yperit
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
trvalou	trvalý	k2eAgFnSc4d1	trvalá
bojovou	bojový	k2eAgFnSc4d1	bojová
chemickou	chemický	k2eAgFnSc4d1	chemická
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
zamořeném	zamořený	k2eAgInSc6d1	zamořený
terénu	terén	k1gInSc6	terén
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
mnoha	mnoho	k4c2	mnoho
hodin	hodina	k1gFnPc2	hodina
až	až	k6eAd1	až
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
okolní	okolní	k2eAgFnSc6d1	okolní
teplotě	teplota	k1gFnSc6	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gInSc1	jeho
bod	bod	k1gInSc1	bod
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
je	být	k5eAaImIp3nS	být
14,40	[number]	k4	14,40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc4d1	známa
různé	různý	k2eAgFnPc4d1	různá
"	"	kIx"	"
<g/>
bojové	bojový	k2eAgFnPc4d1	bojová
receptury	receptura	k1gFnPc4	receptura
yperitu	yperit	k1gInSc2	yperit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
bod	bod	k1gInSc1	bod
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
účelově	účelově	k6eAd1	účelově
snížen	snížit	k5eAaPmNgInS	snížit
až	až	k9	až
na	na	k7c6	na
-	-	kIx~	-
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Přidávání	přidávání	k1gNnSc1	přidávání
různých	různý	k2eAgNnPc2d1	různé
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
však	však	k9	však
pochopitelně	pochopitelně	k6eAd1	pochopitelně
snižuje	snižovat	k5eAaImIp3nS	snižovat
účinnost	účinnost	k1gFnSc4	účinnost
takto	takto	k6eAd1	takto
upraveného	upravený	k2eAgInSc2d1	upravený
yperitu	yperit	k1gInSc2	yperit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zimní	zimní	k2eAgFnPc4d1	zimní
podmínky	podmínka	k1gFnPc4	podmínka
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zavedena	zaveden	k2eAgFnSc1d1	zavedena
směs	směs	k1gFnSc1	směs
sulfidického	sulfidický	k2eAgInSc2d1	sulfidický
yperitu	yperit	k1gInSc2	yperit
a	a	k8xC	a
lewisitu	lewisit	k1gInSc2	lewisit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
je	být	k5eAaImIp3nS	být
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
v	v	k7c6	v
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Yperit	yperit	k1gInSc1	yperit
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc4d1	stabilní
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
odmořování	odmořování	k1gNnSc4	odmořování
probíhá	probíhat	k5eAaImIp3nS	probíhat
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
dostatečně	dostatečně	k6eAd1	dostatečně
<g/>
,	,	kIx,	,
i	i	k9	i
meziprodukty	meziprodukt	k1gInPc1	meziprodukt
destrukčních	destrukční	k2eAgFnPc2d1	destrukční
razantních	razantní	k2eAgFnPc2d1	razantní
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
zpuchýřující	zpuchýřující	k2eAgInPc4d1	zpuchýřující
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
proniká	pronikat	k5eAaImIp3nS	pronikat
všemi	všecek	k3xTgFnPc7	všecek
cestami	cesta	k1gFnPc7	cesta
vstupu	vstup	k1gInSc6	vstup
–	–	k?	–
kůží	kůže	k1gFnPc2	kůže
<g/>
,	,	kIx,	,
dýchacími	dýchací	k2eAgInPc7d1	dýchací
orgány	orgán	k1gInPc7	orgán
<g/>
,	,	kIx,	,
očními	oční	k2eAgFnPc7d1	oční
spojivkami	spojivka	k1gFnPc7	spojivka
i	i	k8xC	i
zažívacím	zažívací	k2eAgNnSc7d1	zažívací
ústrojím	ústrojí	k1gNnSc7	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
smrtná	smrtný	k2eAgFnSc1d1	Smrtná
koncentrace	koncentrace	k1gFnSc1	koncentrace
sulfidického	sulfidický	k2eAgInSc2d1	sulfidický
yperitu	yperit	k1gInSc2	yperit
činí	činit	k5eAaImIp3nS	činit
inhalačně	inhalačně	k6eAd1	inhalačně
LCt	LCt	k1gFnSc4	LCt
<g/>
50	[number]	k4	50
<g/>
:	:	kIx,	:
1,5	[number]	k4	1,5
gmin-	gmin-	k?	gmin-
<g/>
1	[number]	k4	1
<g/>
m-	m-	k?	m-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
obvykle	obvykle	k6eAd1	obvykle
za	za	k7c4	za
4-6	[number]	k4	4-6
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
zasažení	zasažení	k1gNnSc6	zasažení
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
latence	latence	k1gFnSc2	latence
(	(	kIx(	(
<g/>
skryté	skrytý	k2eAgNnSc1d1	skryté
působení	působení	k1gNnSc1	působení
bojové	bojový	k2eAgFnSc2d1	bojová
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
<g/>
)	)	kIx)	)
značně	značně	k6eAd1	značně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
obdržené	obdržený	k2eAgFnSc6d1	obdržená
dávce	dávka	k1gFnSc6	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Yperit	yperit	k1gInSc1	yperit
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
značný	značný	k2eAgInSc1d1	značný
kumulativní	kumulativní	k2eAgInSc1d1	kumulativní
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Yperit	yperit	k1gInSc1	yperit
má	mít	k5eAaImIp3nS	mít
mnohostranný	mnohostranný	k2eAgInSc1d1	mnohostranný
účinek	účinek	k1gInSc1	účinek
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
poškozením	poškození	k1gNnSc7	poškození
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
dýchacích	dýchací	k2eAgInPc2d1	dýchací
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
zažívacího	zažívací	k2eAgNnSc2d1	zažívací
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
zasažení	zasažení	k1gNnSc2	zasažení
po	po	k7c6	po
4-6	[number]	k4	4-6
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nejcitlivějším	citlivý	k2eAgInSc7d3	nejcitlivější
orgánem	orgán	k1gInSc7	orgán
jsou	být	k5eAaImIp3nP	být
oči	oko	k1gNnPc1	oko
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
yperitové	yperitový	k2eAgFnPc1d1	yperitový
páry	pára	k1gFnPc1	pára
již	již	k9	již
v	v	k7c6	v
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
vyvolat	vyvolat	k5eAaPmF	vyvolat
vážná	vážný	k2eAgFnSc1d1	vážná
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
poškození	poškození	k1gNnSc4	poškození
dýchacích	dýchací	k2eAgInPc2d1	dýchací
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
otoku	otok	k1gInSc2	otok
očních	oční	k2eAgNnPc2d1	oční
víček	víčko	k1gNnPc2	víčko
<g/>
,	,	kIx,	,
zánětu	zánět	k1gInSc2	zánět
spojivek	spojivka	k1gFnPc2	spojivka
a	a	k8xC	a
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
světloplachosti	světloplachost	k1gFnSc2	světloplachost
<g/>
.	.	kIx.	.
</s>
<s>
Zasažení	zasažení	k1gNnSc1	zasažení
dýchacích	dýchací	k2eAgInPc2d1	dýchací
orgánů	orgán	k1gInPc2	orgán
parami	para	k1gFnPc7	para
destilovaného	destilovaný	k2eAgInSc2d1	destilovaný
yperitu	yperit	k1gInSc2	yperit
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
podrážděním	podráždění	k1gNnSc7	podráždění
v	v	k7c6	v
nose	nos	k1gInSc6	nos
<g/>
,	,	kIx,	,
v	v	k7c6	v
hrdle	hrdla	k1gFnSc6	hrdla
<g/>
,	,	kIx,	,
dostavují	dostavovat	k5eAaImIp3nP	dostavovat
se	se	k3xPyFc4	se
horečnaté	horečnatý	k2eAgInPc1d1	horečnatý
stavy	stav	k1gInPc1	stav
<g/>
,	,	kIx,	,
ztížené	ztížený	k2eAgNnSc1d1	ztížené
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc1	pocení
<g/>
,	,	kIx,	,
silné	silný	k2eAgNnSc1d1	silné
kašlání	kašlání	k1gNnSc1	kašlání
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
ochablost	ochablost	k1gFnSc1	ochablost
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
nechráněného	chráněný	k2eNgInSc2d1	nechráněný
povrchu	povrch	k1gInSc2	povrch
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
účinkem	účinek	k1gInSc7	účinek
par	para	k1gFnPc2	para
a	a	k8xC	a
aerosolů	aerosol	k1gInPc2	aerosol
kapalného	kapalný	k2eAgInSc2d1	kapalný
destilovaného	destilovaný	k2eAgInSc2d1	destilovaný
yperitu	yperit	k1gInSc2	yperit
<g/>
.	.	kIx.	.
</s>
<s>
Zasažení	zasažení	k1gNnSc1	zasažení
kůže	kůže	k1gFnSc2	kůže
není	být	k5eNaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
doprovázeno	doprovázen	k2eAgNnSc1d1	doprovázeno
žádnými	žádný	k3yNgInPc7	žádný
bolestivými	bolestivý	k2eAgInPc7d1	bolestivý
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc1	rozvoj
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	s	k7c7	s
zčervenáním	zčervenání	k1gNnSc7	zčervenání
zasažené	zasažený	k2eAgFnSc2d1	zasažená
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
pocitem	pocit	k1gInSc7	pocit
svědění	svědění	k1gNnSc2	svědění
a	a	k8xC	a
vytváření	vytváření	k1gNnSc2	vytváření
drobných	drobný	k2eAgInPc2d1	drobný
puchýřků	puchýřek	k1gInPc2	puchýřek
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
exudaci	exudace	k1gFnSc3	exudace
a	a	k8xC	a
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
strupů	strup	k1gInPc2	strup
(	(	kIx(	(
<g/>
krusty	krusta	k1gFnSc2	krusta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
doprovázeny	doprovázen	k2eAgInPc1d1	doprovázen
celkovou	celkový	k2eAgFnSc7d1	celková
ochablostí	ochablost	k1gFnSc7	ochablost
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
nechutenstvím	nechutenství	k1gNnSc7	nechutenství
<g/>
,	,	kIx,	,
rychlým	rychlý	k2eAgNnSc7d1	rychlé
ubýváním	ubývání	k1gNnSc7	ubývání
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
projevem	projev	k1gInSc7	projev
zasažení	zasažení	k1gNnSc2	zasažení
yperitem	yperit	k1gInSc7	yperit
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
svalové	svalový	k2eAgFnSc2d1	svalová
křeče	křeč	k1gFnSc2	křeč
vrcholící	vrcholící	k2eAgFnSc7d1	vrcholící
paralýzou	paralýza	k1gFnSc7	paralýza
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
zažívacích	zažívací	k2eAgInPc2d1	zažívací
orgánů	orgán	k1gInPc2	orgán
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
potravy	potrava	k1gFnSc2	potrava
zamořené	zamořený	k2eAgFnSc2d1	zamořená
yperitem	yperit	k1gInSc7	yperit
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
prudkými	prudký	k2eAgFnPc7d1	prudká
bolestmi	bolest	k1gFnPc7	bolest
v	v	k7c6	v
břiše	břicho	k1gNnSc6	břicho
<g/>
,	,	kIx,	,
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
,	,	kIx,	,
nevolností	nevolnost	k1gFnSc7	nevolnost
<g/>
,	,	kIx,	,
průjmem	průjem	k1gInSc7	průjem
<g/>
,	,	kIx,	,
celkovou	celkový	k2eAgFnSc7d1	celková
ochablostí	ochablost	k1gFnSc7	ochablost
a	a	k8xC	a
svalovými	svalový	k2eAgFnPc7d1	svalová
křečemi	křeč	k1gFnPc7	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Absorpce	absorpce	k1gFnPc1	absorpce
vlhkou	vlhký	k2eAgFnSc7d1	vlhká
kůží	kůže	k1gFnPc2	kůže
probíhá	probíhat	k5eAaImIp3nS	probíhat
daleko	daleko	k6eAd1	daleko
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
za	za	k7c2	za
sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlhkého	vlhký	k2eAgNnSc2d1	vlhké
počasí	počasí	k1gNnSc2	počasí
lze	lze	k6eAd1	lze
poškození	poškození	k1gNnSc4	poškození
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
při	při	k7c6	při
podstatně	podstatně	k6eAd1	podstatně
nižších	nízký	k2eAgFnPc6d2	nižší
koncentracích	koncentrace	k1gFnPc6	koncentrace
yperitu	yperit	k1gInSc2	yperit
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
pronikání	pronikání	k1gNnSc2	pronikání
yperitu	yperit	k1gInSc2	yperit
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Poškozený	poškozený	k2eAgInSc1d1	poškozený
organismus	organismus	k1gInSc1	organismus
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
citlivý	citlivý	k2eAgInSc4d1	citlivý
k	k	k7c3	k
sekundárním	sekundární	k2eAgFnPc3d1	sekundární
infekcím	infekce	k1gFnPc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplnost	úplnost	k1gFnSc4	úplnost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
sulfidického	sulfidický	k2eAgInSc2d1	sulfidický
yperitu	yperit	k1gInSc2	yperit
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
připraveny	připravit	k5eAaPmNgInP	připravit
i	i	k9	i
dusíkové	dusíkový	k2eAgInPc1d1	dusíkový
yperity	yperit	k1gInPc1	yperit
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
a	a	k8xC	a
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
popsány	popsat	k5eAaPmNgFnP	popsat
3	[number]	k4	3
dusíkové	dusíkový	k2eAgInPc4d1	dusíkový
yperity	yperit	k1gInPc4	yperit
<g/>
,	,	kIx,	,
NH-	NH-	k1gFnSc1	NH-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
NH-	NH-	k1gFnSc1	NH-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
NH-	NH-	k1gFnSc1	NH-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
jeho	jeho	k3xOp3gFnSc2	jeho
válečné	válečná	k1gFnSc2	válečná
použití	použití	k1gNnSc2	použití
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
němečtí	německý	k2eAgMnPc1d1	německý
chemici	chemik	k1gMnPc1	chemik
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Lommell	Lommell	k1gMnSc1	Lommell
a	a	k8xC	a
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Steinkopf	Steinkopf	k1gInSc4	Steinkopf
Yperit	yperit	k1gInSc1	yperit
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
konfliktech	konflikt	k1gInPc6	konflikt
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
použití	použitý	k2eAgMnPc1d1	použitý
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
1917	[number]	k4	1917
u	u	k7c2	u
belgického	belgický	k2eAgNnSc2d1	Belgické
města	město	k1gNnSc2	město
Ypres	Ypres	k1gMnSc1	Ypres
Španělsko	Španělsko	k1gNnSc1	Španělsko
proti	proti	k7c3	proti
Maroku	Maroko	k1gNnSc3	Maroko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923-1926	[number]	k4	1923-1926
Itálie	Itálie	k1gFnSc1	Itálie
proti	proti	k7c3	proti
Etiopii	Etiopie	k1gFnSc3	Etiopie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935-1940	[number]	k4	1935-1940
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Sin-ťiang	Sin-ťianga	k1gFnPc2	Sin-ťianga
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
republice	republika	k1gFnSc6	republika
1934	[number]	k4	1934
a	a	k8xC	a
1937	[number]	k4	1937
Japonsko	Japonsko	k1gNnSc4	Japonsko
proti	proti	k7c3	proti
Čínské	čínský	k2eAgFnSc3d1	čínská
republice	republika	k1gFnSc3	republika
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937-1945	[number]	k4	1937-1945
Německo	Německo	k1gNnSc1	Německo
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
-	-	kIx~	-
1945	[number]	k4	1945
Egypt	Egypt	k1gInSc1	Egypt
proti	proti	k7c3	proti
Severnímu	severní	k2eAgInSc3d1	severní
Jemenu	Jemen	k1gInSc3	Jemen
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
proti	proti	k7c3	proti
Íránu	Írán	k1gInSc3	Írán
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980-1988	[number]	k4	1980-1988
Irák	Irák	k1gInSc1	Irák
proti	proti	k7c3	proti
Kurdům	Kurd	k1gMnPc3	Kurd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
ISIS	Isis	k1gFnSc1	Isis
proti	proti	k7c3	proti
Kurdům	Kurd	k1gMnPc3	Kurd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Malijevská	Malijevský	k2eAgNnPc4d1	Malijevský
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Malijevský	Malijevský	k2eAgMnSc1d1	Malijevský
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Novák	Novák	k1gMnSc1	Novák
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Záhady	záhada	k1gFnSc2	záhada
<g/>
,	,	kIx,	,
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
zajímavosti	zajímavost	k1gFnSc2	zajímavost
-	-	kIx~	-
Očima	oko	k1gNnPc7	oko
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7080	[number]	k4	7080
<g/>
-	-	kIx~	-
<g/>
535	[number]	k4	535
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
184-6	[number]	k4	184-6
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
http://zahranicni.ihned.cz/evropa-slovensko/c1-56505010-pred-95-lety-nemci-poprve-pouzili-yperit-chemicke-zbrane-se-pouzivaji-dodnes	[url]	k1gMnSc1	http://zahranicni.ihned.cz/evropa-slovensko/c1-56505010-pred-95-lety-nemci-poprve-pouzili-yperit-chemicke-zbrane-se-pouzivaji-dodnes
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Yperit	yperit	k1gInSc1	yperit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
yperit	yperit	k1gInSc1	yperit
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Senfgas	Senfgasa	k1gFnPc2	Senfgasa
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
