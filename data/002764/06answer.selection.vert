<s>
K2	K2	k4	K2
(	(	kIx(	(
<g/>
baltsky	baltsky	k6eAd1	baltsky
Čhogori	Čhogori	k1gNnSc1	Čhogori
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Čchokori	Čchokor	k1gMnPc1	Čchokor
<g/>
,	,	kIx,	,
urdsky	urdsky	k6eAd1	urdsky
ک	ک	k?	ک
ٹ	ٹ	k?	ٹ
<g/>
,	,	kIx,	,
transliterováno	transliterován	k2eAgNnSc4d1	transliterován
Ke	k	k7c3	k
tū	tū	k?	tū
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Lambá	Lambý	k2eAgFnSc1d1	Lambý
Pahár	Pahár	k1gInSc1	Pahár
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
乔	乔	k?	乔
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc4d1	oficiální
přepis	přepis	k1gInSc4	přepis
Qogir	Qogir	k1gMnSc1	Qogir
<g/>
,	,	kIx,	,
pinyin	pinyin	k1gMnSc1	pinyin
Qiáogē	Qiáogē	k1gMnSc1	Qiáogē
Fē	Fē	k1gMnSc1	Fē
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
Čchiao-ke	Čchiao	k1gFnSc2	Čchiao-k
<g/>
-li	i	k?	-li
feng	feng	k1gInSc1	feng
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
pod	pod	k7c7	pod
anglickým	anglický	k2eAgInSc7d1	anglický
názvem	název	k1gInSc7	název
Mount	Mount	k1gInSc1	Mount
Godwin-Austen	Godwin-Austen	k2eAgInSc1d1	Godwin-Austen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Karákóram	Karákóram	k1gInSc1	Karákóram
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
