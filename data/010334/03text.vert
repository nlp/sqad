<p>
<s>
1Q84	[number]	k4	1Q84
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
い	い	k?	い
<g/>
,	,	kIx,	,
Hepburnův	Hepburnův	k2eAgInSc1d1	Hepburnův
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Ichi-kyū	Ichiyū	k1gFnSc1	Ichi-kyū
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
román	román	k1gInSc4	román
japonského	japonský	k2eAgMnSc2d1	japonský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Harukiho	Haruki	k1gMnSc2	Haruki
Murakamiho	Murakami	k1gMnSc2	Murakami
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
třísvazkové	třísvazkový	k2eAgFnSc6d1	třísvazková
formě	forma	k1gFnSc6	forma
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2009	[number]	k4	2009
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
bestsellerem	bestseller	k1gInSc7	bestseller
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
vyprodání	vyprodání	k1gNnSc3	vyprodání
prvních	první	k4xOgNnPc2	první
dvou	dva	k4xCgInPc2	dva
knih	kniha	k1gFnPc2	kniha
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c4	v
den	den	k1gInSc4	den
uvedení	uvedení	k1gNnSc2	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
pak	pak	k6eAd1	pak
prodejnost	prodejnost	k1gFnSc1	prodejnost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hranice	hranice	k1gFnSc2	hranice
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
<g/>
Česky	česky	k6eAd1	česky
román	román	k1gInSc4	román
vyšel	vyjít	k5eAaPmAgMnS	vyjít
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
svazcích	svazek	k1gInPc6	svazek
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
edice	edice	k1gFnSc2	edice
Světová	světový	k2eAgFnSc1d1	světová
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Českého	český	k2eAgInSc2d1	český
překladu	překlad	k1gInSc2	překlad
se	se	k3xPyFc4	se
zhostil	zhostit	k5eAaPmAgMnS	zhostit
dvorní	dvorní	k2eAgMnSc1d1	dvorní
Murakamiho	Murakami	k1gMnSc4	Murakami
překladatel	překladatel	k1gMnSc1	překladatel
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jurkovič	Jurkovič	k1gMnSc1	Jurkovič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Název	název	k1gInSc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
Murakami	Muraka	k1gFnPc7	Muraka
odhalil	odhalit	k5eAaPmAgInS	odhalit
úvodní	úvodní	k2eAgInSc1d1	úvodní
úryvek	úryvek	k1gInSc1	úryvek
a	a	k8xC	a
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
románu	román	k1gInSc6	román
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
slovní	slovní	k2eAgFnSc6d1	slovní
hříčkou	hříčka	k1gFnSc7	hříčka
k	k	k7c3	k
výslovnosti	výslovnost	k1gFnSc3	výslovnost
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
a	a	k8xC	a
také	také	k9	také
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
dystopické	dystopický	k2eAgNnSc4d1	dystopické
dílo	dílo	k1gNnSc4	dílo
George	Georg	k1gFnSc2	Georg
Orwella	Orwella	k1gFnSc1	Orwella
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
Q	Q	kA	Q
a	a	k8xC	a
japonské	japonský	k2eAgNnSc1d1	Japonské
číslo	číslo	k1gNnSc1	číslo
9	[number]	k4	9
(	(	kIx(	(
<g/>
standardně	standardně	k6eAd1	standardně
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
romanizované	romanizovaný	k2eAgFnSc2d1	romanizovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
kyū	kyū	k?	kyū
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
japonského	japonský	k2eAgNnSc2d1	Japonské
vydání	vydání	k1gNnSc2	vydání
knihy	kniha	k1gFnSc2	kniha
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
písmeny	písmeno	k1gNnPc7	písmeno
"	"	kIx"	"
<g/>
kew	kew	k?	kew
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
homofony	homofon	k1gInPc4	homofon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
se	se	k3xPyFc4	se
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
využívají	využívat	k5eAaPmIp3nP	využívat
v	v	k7c6	v
japonských	japonský	k2eAgFnPc6d1	japonská
slovních	slovní	k2eAgFnPc6d1	slovní
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
v	v	k7c6	v
názvu	název	k1gInSc6	název
"	"	kIx"	"
<g/>
1	[number]	k4	1
<g/>
Q	Q	kA	Q
<g/>
84	[number]	k4	84
<g/>
"	"	kIx"	"
lze	lze	k6eAd1	lze
také	také	k9	také
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
question	question	k1gInSc1	question
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
otázku	otázka	k1gFnSc4	otázka
<g/>
)	)	kIx)	)
–	–	k?	–
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
přináší	přinášet	k5eAaImIp3nS	přinášet
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgInPc1d1	kulturní
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Murakami	Muraka	k1gFnPc7	Muraka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
do	do	k7c2	do
děje	děj	k1gInSc2	děj
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
skladatele	skladatel	k1gMnPc4	skladatel
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnPc4	spisovatel
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaně	opakovaně	k6eAd1	opakovaně
se	se	k3xPyFc4	se
v	v	k7c6	v
textu	text	k1gInSc6	text
objevila	objevit	k5eAaPmAgFnS	objevit
Sinfonietta	Sinfonietta	k1gFnSc1	Sinfonietta
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Bachův	Bachův	k2eAgInSc1d1	Bachův
cyklus	cyklus	k1gInSc1	cyklus
Dobře	dobře	k6eAd1	dobře
temperovaný	temperovaný	k2eAgInSc1d1	temperovaný
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
Dowlandova	Dowlandův	k2eAgFnSc1d1	Dowlandův
Lachrimae	Lachrimae	k1gFnSc1	Lachrimae
či	či	k8xC	či
skladba	skladba	k1gFnSc1	skladba
Antonia	Antonio	k1gMnSc2	Antonio
Vivaldiho	Vivaldi	k1gMnSc2	Vivaldi
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
modernějších	moderní	k2eAgMnPc2d2	modernější
umělců	umělec	k1gMnPc2	umělec
byli	být	k5eAaImAgMnP	být
zmíněni	zmíněn	k2eAgMnPc1d1	zmíněn
Billie	Billie	k1gFnSc2	Billie
Holiday	Holida	k2eAgFnPc1d1	Holida
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Mingus	Mingus	k1gMnSc1	Mingus
<g/>
,	,	kIx,	,
či	či	k8xC	či
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
knize	kniha	k1gFnSc6	kniha
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
blues	blues	k1gNnSc4	blues
Louis	Louis	k1gMnSc1	Louis
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
.	.	kIx.	.
<g/>
Aomame	Aomam	k1gInSc5	Aomam
obdrží	obdržet	k5eAaPmIp3nP	obdržet
ke	k	k7c3	k
čtení	čtení	k1gNnSc3	čtení
v	v	k7c6	v
azylu	azyl	k1gInSc6	azyl
obsáhlý	obsáhlý	k2eAgInSc4d1	obsáhlý
Proustův	Proustův	k2eAgInSc4d1	Proustův
román	román	k1gInSc4	román
Hledání	hledání	k1gNnSc4	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Citován	citován	k2eAgInSc1d1	citován
je	být	k5eAaImIp3nS	být
také	také	k9	také
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
úryvek	úryvek	k1gInSc1	úryvek
japonského	japonský	k2eAgInSc2d1	japonský
středověkého	středověký	k2eAgInSc2d1	středověký
eposu	epos	k1gInSc2	epos
Příběh	příběh	k1gInSc1	příběh
rodu	rod	k1gInSc2	rod
Taira	Tairo	k1gNnSc2	Tairo
a	a	k8xC	a
kratší	krátký	k2eAgInSc4d2	kratší
text	text	k1gInSc4	text
o	o	k7c6	o
Giljacích	Giljak	k1gInPc6	Giljak
z	z	k7c2	z
cestovního	cestovní	k2eAgInSc2d1	cestovní
deníku	deník	k1gInSc2	deník
Antona	Anton	k1gMnSc2	Anton
Pavloviče	Pavlovič	k1gMnSc2	Pavlovič
Čechova	Čechov	k1gMnSc2	Čechov
Ostrov	ostrov	k1gInSc1	ostrov
Sachalin	Sachalin	k1gInSc1	Sachalin
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Později	pozdě	k6eAd2	pozdě
zavražděná	zavražděný	k2eAgFnSc1d1	zavražděná
policistka	policistka	k1gFnSc1	policistka
Ajumi	Aju	k1gFnPc7	Aju
vnáší	vnášet	k5eAaImIp3nS	vnášet
do	do	k7c2	do
děje	děj	k1gInSc2	děj
myšlenku	myšlenka	k1gFnSc4	myšlenka
kanadského	kanadský	k2eAgMnSc2d1	kanadský
filozofa	filozof	k1gMnSc2	filozof
Marshalla	Marshall	k1gMnSc2	Marshall
McLuhana	McLuhan	k1gMnSc2	McLuhan
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc2	vůdce
sekty	sekta	k1gFnSc2	sekta
Lídr	lídr	k1gMnSc1	lídr
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
antropologu	antropolog	k1gMnSc6	antropolog
Frazerovi	Frazer	k1gMnSc6	Frazer
<g/>
,	,	kIx,	,
Fjodoru	Fjodor	k1gInSc6	Fjodor
Dostojevském	Dostojevský	k2eAgInSc6d1	Dostojevský
i	i	k8xC	i
Carlu	Carl	k1gMnSc6	Carl
Gustavu	Gustav	k1gMnSc6	Gustav
Jungovi	Jung	k1gMnSc6	Jung
<g/>
,	,	kIx,	,
strážce	strážce	k1gMnSc1	strážce
Tamaru	Tamara	k1gFnSc4	Tamara
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
Čechovův	Čechovův	k2eAgInSc4d1	Čechovův
apel	apel	k1gInSc4	apel
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
střelná	střelný	k2eAgFnSc1d1	střelná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
musí	muset	k5eAaImIp3nP	muset
vystřelit	vystřelit	k5eAaPmF	vystřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
KoncepceUdálosti	KoncepceUdálost	k1gFnPc1	KoncepceUdálost
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1	[number]	k4	1
<g/>
Q	Q	kA	Q
<g/>
84	[number]	k4	84
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
paralelních	paralelní	k2eAgFnPc2d1	paralelní
realit	realita	k1gFnPc2	realita
–	–	k?	–
fiktivních	fiktivní	k2eAgInPc2d1	fiktivní
světů	svět	k1gInPc2	svět
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
příběh	příběh	k1gInSc4	příběh
mezi	mezi	k7c7	mezi
dubnem	duben	k1gInSc7	duben
až	až	k8xS	až
červnem	červen	k1gInSc7	červen
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
vyprávění	vyprávění	k1gNnSc1	vyprávění
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
říjnem	říjen	k1gInSc7	říjen
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fiktivní	fiktivní	k2eAgInSc1d1	fiktivní
svět	svět	k1gInSc1	svět
1Q84	[number]	k4	1Q84
je	být	k5eAaImIp3nS	být
nehostinné	hostinný	k2eNgNnSc1d1	nehostinné
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
vrženy	vržen	k2eAgFnPc4d1	vržena
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
příběhu	příběh	k1gInSc2	příběh
Tengo	Tengo	k6eAd1	Tengo
a	a	k8xC	a
Aomame	Aomam	k1gInSc5	Aomam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
procesu	proces	k1gInSc6	proces
postupného	postupný	k2eAgNnSc2d1	postupné
uvědomování	uvědomování	k1gNnSc2	uvědomování
si	se	k3xPyFc3	se
nové	nový	k2eAgFnPc1d1	nová
skutečnosti	skutečnost	k1gFnPc1	skutečnost
začínají	začínat	k5eAaImIp3nP	začínat
svádět	svádět	k5eAaImF	svádět
boj	boj	k1gInSc4	boj
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
<g/>
,	,	kIx,	,
vymanění	vymanění	k1gNnSc1	vymanění
se	se	k3xPyFc4	se
z	z	k7c2	z
osidel	osidlo	k1gNnPc2	osidlo
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
nalézt	nalézt	k5eAaPmF	nalézt
jeden	jeden	k4xCgMnSc1	jeden
druhého	druhý	k4xOgMnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
postavy	postava	k1gFnPc1	postava
nevykazují	vykazovat	k5eNaImIp3nP	vykazovat
známky	známka	k1gFnPc1	známka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
byly	být	k5eAaImAgFnP	být
vědomé	vědomý	k2eAgFnPc1d1	vědomá
změněné	změněný	k2eAgFnPc1d1	změněná
reality	realita	k1gFnPc1	realita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
kapitoly	kapitola	k1gFnPc4	kapitola
vyprávěné	vyprávěný	k2eAgFnPc4d1	vyprávěná
v	v	k7c6	v
er-formě	erorma	k1gFnSc6	er-forma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
–	–	k?	–
Aomame	Aomam	k1gInSc5	Aomam
<g/>
,	,	kIx,	,
Tenga	Teng	k1gMnSc4	Teng
či	či	k8xC	či
Ušikawu	Ušikawa	k1gMnSc4	Ušikawa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc4	děj
<g/>
30	[number]	k4	30
<g/>
letá	letý	k2eAgFnSc1d1	letá
masérka	masérka	k1gFnSc1	masérka
Aomame	Aomam	k1gInSc5	Aomam
jede	jet	k5eAaImIp3nS	jet
<g/>
,	,	kIx,	,
za	za	k7c2	za
poslechu	poslech	k1gInSc2	poslech
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
Sinfonietty	Sinfonietta	k1gFnSc2	Sinfonietta
<g/>
,	,	kIx,	,
po	po	k7c6	po
tokijské	tokijský	k2eAgFnSc6d1	Tokijská
dálnici	dálnice	k1gFnSc6	dálnice
vykonat	vykonat	k5eAaPmF	vykonat
smluvenou	smluvený	k2eAgFnSc4d1	smluvená
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Taxi	taxi	k1gNnSc1	taxi
však	však	k9	však
uvízne	uvíznout	k5eAaPmIp3nS	uvíznout
v	v	k7c6	v
zácpě	zácpa	k1gFnSc6	zácpa
a	a	k8xC	a
řidič	řidič	k1gMnSc1	řidič
pasažérce	pasažérka	k1gFnSc3	pasažérka
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
z	z	k7c2	z
únikového	únikový	k2eAgNnSc2d1	únikové
schodiště	schodiště	k1gNnSc2	schodiště
zavěšeného	zavěšený	k2eAgInSc2d1	zavěšený
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
magistrály	magistrála	k1gFnSc2	magistrála
<g/>
,	,	kIx,	,
u	u	k7c2	u
billboardu	billboard	k1gInSc2	billboard
Essa	Ess	k1gInSc2	Ess
s	s	k7c7	s
tygrem	tygr	k1gMnSc7	tygr
držícím	držící	k2eAgMnSc7d1	držící
tankovací	tankovací	k2eAgFnSc4d1	tankovací
pistoli	pistole	k1gFnSc4	pistole
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slezení	slezení	k1gNnSc6	slezení
se	s	k7c7	s
vlakem	vlak	k1gInSc7	vlak
přepravuje	přepravovat	k5eAaImIp3nS	přepravovat
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
v	v	k7c6	v
Šibuji	šibovat	k5eAaImIp1nS	šibovat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
masáže	masáž	k1gFnSc2	masáž
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
hosta	host	k1gMnSc2	host
vpichem	vpich	k1gInSc7	vpich
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
jehly	jehla	k1gFnSc2	jehla
do	do	k7c2	do
zátylku	zátylek	k1gInSc2	zátylek
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
tak	tak	k9	tak
výjimečné	výjimečný	k2eAgFnPc4d1	výjimečná
zručnosti	zručnost	k1gFnPc4	zručnost
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
hrotu	hrot	k1gInSc2	hrot
do	do	k7c2	do
center	centrum	k1gNnPc2	centrum
řídících	řídící	k2eAgNnPc2d1	řídící
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgFnPc4d1	důležitá
funkce	funkce	k1gFnPc4	funkce
pod	pod	k7c7	pod
bází	báze	k1gFnSc7	báze
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
komory	komora	k1gFnSc2	komora
prodloužené	prodloužený	k2eAgFnSc2d1	prodloužená
míchy	mícha	k1gFnSc2	mícha
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
vpichu	vpich	k1gInSc2	vpich
není	být	k5eNaImIp3nS	být
znatelné	znatelný	k2eAgNnSc1d1	znatelné
<g/>
,	,	kIx,	,
a	a	k8xC	a
policie	policie	k1gFnSc1	policie
tak	tak	k8xS	tak
případ	případ	k1gInSc1	případ
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
jako	jako	k9	jako
přirozené	přirozený	k2eAgNnSc4d1	přirozené
úmrtí	úmrtí	k1gNnSc4	úmrtí
na	na	k7c4	na
srdeční	srdeční	k2eAgFnPc4d1	srdeční
selhání	selhání	k1gNnPc4	selhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aomame	Aomam	k1gInSc5	Aomam
si	se	k3xPyFc3	se
začíná	začínat	k5eAaImIp3nS	začínat
všímat	všímat	k5eAaImF	všímat
neobvyklých	obvyklý	k2eNgInPc2d1	neobvyklý
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
odporují	odporovat	k5eAaImIp3nP	odporovat
jejím	její	k3xOp3gFnPc3	její
vzpomínkám	vzpomínka	k1gFnPc3	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Policisté	policista	k1gMnPc1	policista
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
nově	nově	k6eAd1	nově
nosí	nosit	k5eAaImIp3nP	nosit
poloautomatické	poloautomatický	k2eAgFnPc4d1	poloautomatická
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
starších	starý	k2eAgInPc2d2	starší
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Nesrovnalosti	nesrovnalost	k1gFnSc3	nesrovnalost
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
s	s	k7c7	s
archivními	archivní	k2eAgInPc7d1	archivní
výtisky	výtisk	k1gInPc7	výtisk
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc1	několik
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
si	se	k3xPyFc3	se
nevybavuje	vybavovat	k5eNaImIp3nS	vybavovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
měly	mít	k5eAaImAgFnP	mít
jiný	jiný	k2eAgInSc4d1	jiný
průběh	průběh	k1gInSc4	průběh
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
paralelní	paralelní	k2eAgFnSc6d1	paralelní
realitě	realita	k1gFnSc6	realita
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
slezením	slezení	k1gNnSc7	slezení
z	z	k7c2	z
magistrály	magistrála	k1gFnSc2	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Pozměněný	pozměněný	k2eAgInSc4d1	pozměněný
svět	svět	k1gInSc4	svět
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
rok	rok	k1gInSc4	rok
"	"	kIx"	"
<g/>
1	[number]	k4	1
<g/>
Q	Q	kA	Q
<g/>
84	[number]	k4	84
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inteligentní	inteligentní	k2eAgInSc4d1	inteligentní
30	[number]	k4	30
<g/>
letý	letý	k2eAgInSc4d1	letý
Tengo	Tengo	k1gNnSc1	Tengo
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
matematiku	matematika	k1gFnSc4	matematika
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Jojogi	Jojog	k1gFnSc6	Jojog
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Zkušený	zkušený	k2eAgMnSc1d1	zkušený
redaktor	redaktor	k1gMnSc1	redaktor
Komacu	Komacus	k1gInSc2	Komacus
<g/>
,	,	kIx,	,
zaměstnaný	zaměstnaný	k1gMnSc1	zaměstnaný
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
<g/>
,	,	kIx,	,
zná	znát	k5eAaImIp3nS	znát
jeho	jeho	k3xOp3gInSc4	jeho
talent	talent	k1gInSc4	talent
a	a	k8xC	a
tajně	tajně	k6eAd1	tajně
mu	on	k3xPp3gMnSc3	on
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
roli	role	k1gFnSc6	role
ghostwritera	ghostwriter	k1gMnSc2	ghostwriter
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
neobratně	obratně	k6eNd1	obratně
napsaný	napsaný	k2eAgInSc4d1	napsaný
příběh	příběh	k1gInSc4	příběh
17	[number]	k4	17
<g/>
leté	letý	k2eAgFnSc2d1	letá
Fukaeri	Fukaer	k1gFnSc2	Fukaer
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
silný	silný	k2eAgInSc4d1	silný
fantaskní	fantaskní	k2eAgInSc4d1	fantaskní
děj	děj	k1gInSc4	děj
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Kukly	Kukla	k1gMnSc2	Kukla
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
空	空	k?	空
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
má	mít	k5eAaImIp3nS	mít
ambice	ambice	k1gFnSc1	ambice
vyhrát	vyhrát	k5eAaPmF	vyhrát
literární	literární	k2eAgFnSc4d1	literární
soutěž	soutěž	k1gFnSc4	soutěž
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
zpeněžit	zpeněžit	k5eAaPmF	zpeněžit
<g/>
.	.	kIx.	.
</s>
<s>
Tengo	Tengo	k6eAd1	Tengo
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nabídce	nabídka	k1gFnSc3	nabídka
zdrženlivý	zdrženlivý	k2eAgInSc1d1	zdrženlivý
<g/>
.	.	kIx.	.
</s>
<s>
Souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
až	až	k9	až
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
dyslektickou	dyslektický	k2eAgFnSc7d1	dyslektická
dívkou	dívka	k1gFnSc7	dívka
podivného	podivný	k2eAgNnSc2d1	podivné
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
stylu	styl	k1gInSc2	styl
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
svolení	svolení	k1gNnSc4	svolení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krásná	krásný	k2eAgFnSc1d1	krásná
Fukaeri	Fukaeri	k1gNnSc7	Fukaeri
jej	on	k3xPp3gMnSc4	on
přivádí	přivádět	k5eAaImIp3nP	přivádět
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
opatrovníkovi	opatrovník	k1gMnSc3	opatrovník
<g/>
,	,	kIx,	,
profesoru	profesor	k1gMnSc3	profesor
Ebisunovi	Ebisun	k1gMnSc3	Ebisun
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
dovídá	dovídat	k5eAaImIp3nS	dovídat
základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
v	v	k7c6	v
náboženské	náboženský	k2eAgFnSc6d1	náboženská
sektě	sekta	k1gFnSc6	sekta
"	"	kIx"	"
<g/>
Předvoj	předvoj	k1gInSc1	předvoj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
さ	さ	k?	さ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
patřili	patřit	k5eAaImAgMnP	patřit
její	její	k3xOp3gMnPc4	její
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Tamocu	Tamocus	k1gInSc2	Tamocus
Fukada	Fukada	k1gFnSc1	Fukada
byl	být	k5eAaImAgInS	být
vědeckým	vědecký	k2eAgMnSc7d1	vědecký
kolegou	kolega	k1gMnSc7	kolega
a	a	k8xC	a
profesorovým	profesorův	k2eAgMnSc7d1	profesorův
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Ebisuno	Ebisuna	k1gFnSc5	Ebisuna
komunu	komuna	k1gFnSc4	komuna
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
hierarchickou	hierarchický	k2eAgFnSc4d1	hierarchická
despotickou	despotický	k2eAgFnSc4d1	despotická
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
stali	stát	k5eAaPmAgMnP	stát
roboti	robot	k1gMnPc1	robot
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgInSc2d1	vlastní
úsudku	úsudek	k1gInSc2	úsudek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fukada	Fukada	k1gFnSc1	Fukada
založil	založit	k5eAaPmAgMnS	založit
novou	nový	k2eAgFnSc4d1	nová
komunitu	komunita	k1gFnSc4	komunita
Předvoj	předvoj	k1gInSc4	předvoj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
třiceti	třicet	k4xCc7	třicet
členy	člen	k1gInPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obětavě	obětavě	k6eAd1	obětavě
pracovali	pracovat	k5eAaImAgMnP	pracovat
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následné	následný	k2eAgFnPc1d1	následná
neshody	neshoda	k1gFnPc1	neshoda
vyústily	vyústit	k5eAaPmAgFnP	vyústit
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
frakce	frakce	k1gFnPc4	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Radikálnější	radikální	k2eAgMnPc1d2	radikálnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
"	"	kIx"	"
<g/>
Akebono	Akebona	k1gFnSc5	Akebona
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
あ	あ	k?	あ
<g/>
)	)	kIx)	)
a	a	k8xC	a
utkala	utkat	k5eAaPmAgFnS	utkat
se	se	k3xPyFc4	se
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
v	v	k7c6	v
přestřelce	přestřelka	k1gFnSc6	přestřelka
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Motosu	Motos	k1gInSc2	Motos
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Jamanaši	Jamanaše	k1gFnSc6	Jamanaše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malá	malý	k2eAgNnPc1d1	malé
Fukaeri	Fukaeri	k1gNnPc1	Fukaeri
se	se	k3xPyFc4	se
před	před	k7c7	před
sedmi	sedm	k4xCc2	sedm
lety	let	k1gInPc7	let
objevila	objevit	k5eAaPmAgFnS	objevit
u	u	k7c2	u
Ebisunových	Ebisunův	k2eAgFnPc2d1	Ebisunův
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Nemluvila	mluvit	k5eNaImAgFnS	mluvit
a	a	k8xC	a
nesnažila	snažit	k5eNaImAgFnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
svých	svůj	k3xOyFgInPc2	svůj
prožitků	prožitek	k1gInPc2	prožitek
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
marně	marně	k6eAd1	marně
snažil	snažit	k5eAaImAgMnS	snažit
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
Fukadu	Fukada	k1gFnSc4	Fukada
v	v	k7c6	v
Předvoji	předvoj	k1gInSc6	předvoj
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnSc7	její
opatrovníkem	opatrovník	k1gMnSc7	opatrovník
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rodičích	rodič	k1gMnPc6	rodič
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
nic	nic	k6eAd1	nic
neslyšel	slyšet	k5eNaImAgMnS	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
dívka	dívka	k1gFnSc1	dívka
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
příběh	příběh	k1gInSc4	příběh
Kukel	kukla	k1gFnPc2	kukla
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
profesorově	profesorův	k2eAgFnSc3d1	profesorova
dceři	dcera	k1gFnSc3	dcera
Azami	Aza	k1gFnPc7	Aza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gNnSc4	on
zapisovala	zapisovat	k5eAaImAgFnS	zapisovat
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgNnSc7d1	ústřední
tématem	téma	k1gNnSc7	téma
byl	být	k5eAaImAgInS	být
osud	osud	k1gInSc1	osud
dívenky	dívenka	k1gFnSc2	dívenka
v	v	k7c6	v
sektě	sekta	k1gFnSc6	sekta
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
potkala	potkat	k5eAaPmAgFnS	potkat
malé	malý	k2eAgFnPc4d1	malá
mystické	mystický	k2eAgFnPc4d1	mystická
bytosti	bytost	k1gFnPc4	bytost
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
People	People	k1gFnSc1	People
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
リ	リ	k?	リ
<g/>
・	・	k?	・
<g/>
ピ	ピ	k?	ピ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vylézající	vylézající	k2eAgFnSc1d1	vylézající
z	z	k7c2	z
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
kozy	koza	k1gFnSc2	koza
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vzrůst	vzrůst	k1gInSc4	vzrůst
měnily	měnit	k5eAaImAgFnP	měnit
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
centimetrů	centimetr	k1gInPc2	centimetr
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
tvářích	tvář	k1gFnPc6	tvář
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nezračil	zračit	k5eNaImAgInS	zračit
žádný	žádný	k3yNgInSc1	žádný
výraz	výraz	k1gInSc1	výraz
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
plynutím	plynutí	k1gNnSc7	plynutí
času	čas	k1gInSc2	čas
začal	začít	k5eAaPmAgMnS	začít
Tengo	Tengo	k6eAd1	Tengo
přemítat	přemítat	k5eAaImF	přemítat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	s	k7c7	s
–	–	k?	–
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
–	–	k?	–
bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
story	story	k1gFnSc1	story
<g/>
,	,	kIx,	,
nemohla	moct	k5eNaImAgFnS	moct
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
aspektech	aspekt	k1gInPc6	aspekt
odehrát	odehrát	k5eAaPmF	odehrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aomame	Aomam	k1gInSc5	Aomam
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
cvičební	cvičební	k2eAgFnPc4d1	cvičební
lekce	lekce	k1gFnPc4	lekce
i	i	k8xC	i
zámožné	zámožný	k2eAgFnPc4d1	zámožná
staré	starý	k2eAgFnPc4d1	stará
paní	paní	k1gFnPc4	paní
z	z	k7c2	z
Azabu	Azab	k1gInSc2	Azab
<g/>
,	,	kIx,	,
vdově	vdova	k1gFnSc6	vdova
žijící	žijící	k2eAgFnSc2d1	žijící
ve	v	k7c6	v
střežené	střežený	k2eAgFnSc6d1	střežená
Vrbové	Vrbové	k2eAgFnSc6d1	Vrbové
vile	vila	k1gFnSc6	vila
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
velmi	velmi	k6eAd1	velmi
sblížily	sblížit	k5eAaPmAgInP	sblížit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
stál	stát	k5eAaImAgInS	stát
azylový	azylový	k2eAgInSc1d1	azylový
dům	dům	k1gInSc1	dům
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
oběťmi	oběť	k1gFnPc7	oběť
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
profesionální	profesionální	k2eAgFnSc4d1	profesionální
Tamaru	Tamara	k1gFnSc4	Tamara
<g/>
.	.	kIx.	.
</s>
<s>
Vdova	vdova	k1gFnSc1	vdova
cvičitelku	cvičitelka	k1gFnSc4	cvičitelka
příležitostně	příležitostně	k6eAd1	příležitostně
najímala	najímat	k5eAaImAgFnS	najímat
na	na	k7c4	na
zprovození	zprovození	k1gNnSc4	zprovození
osob	osoba	k1gFnPc2	osoba
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
provinily	provinit	k5eAaPmAgFnP	provinit
na	na	k7c4	na
svých	svůj	k3xOyFgMnPc2	svůj
bližních	bližní	k1gMnPc2	bližní
právě	právě	k9	právě
domácím	domácí	k2eAgNnSc7d1	domácí
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sexuální	sexuální	k2eAgFnSc4d1	sexuální
touhu	touha	k1gFnSc4	touha
Aomame	Aomam	k1gInSc5	Aomam
ventilovala	ventilovat	k5eAaImAgFnS	ventilovat
promiskuitním	promiskuitní	k2eAgInSc7d1	promiskuitní
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblíbeném	oblíbený	k2eAgInSc6d1	oblíbený
baru	bar	k1gInSc6	bar
vyhlížela	vyhlížet	k5eAaImAgFnS	vyhlížet
starší	starý	k2eAgMnPc4d2	starší
plešatící	plešatící	k2eAgMnPc4d1	plešatící
partnery	partner	k1gMnPc4	partner
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
výpadech	výpad	k1gInPc6	výpad
se	se	k3xPyFc4	se
poznala	poznat	k5eAaPmAgFnS	poznat
s	s	k7c7	s
policistkou	policistka	k1gFnSc7	policistka
Ajumi	Aju	k1gFnPc7	Aju
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nerozlučný	rozlučný	k2eNgInSc4d1	nerozlučný
tandem	tandem	k1gInSc4	tandem
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
dočetla	dočíst	k5eAaPmAgFnS	dočíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
někdo	někdo	k3yInSc1	někdo
uškrtil	uškrtit	k5eAaPmAgInS	uškrtit
páskem	pásek	k1gMnSc7	pásek
v	v	k7c6	v
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paní	paní	k1gFnSc1	paní
z	z	k7c2	z
Azabu	Azab	k1gInSc2	Azab
instruktorce	instruktorka	k1gFnSc6	instruktorka
představila	představit	k5eAaPmAgFnS	představit
desetiletou	desetiletý	k2eAgFnSc4d1	desetiletá
Cubasu	Cubasa	k1gFnSc4	Cubasa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
v	v	k7c6	v
Předvoji	předvoj	k1gInSc6	předvoj
<g/>
.	.	kIx.	.
</s>
<s>
Masérce	masérka	k1gFnSc3	masérka
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
holčička	holčička	k1gFnSc1	holčička
zakusila	zakusit	k5eAaPmAgFnS	zakusit
pohlavní	pohlavní	k2eAgNnSc4d1	pohlavní
zneužívání	zneužívání	k1gNnSc4	zneužívání
od	od	k7c2	od
vůdce	vůdce	k1gMnSc2	vůdce
sekty	sekta	k1gFnSc2	sekta
<g/>
,	,	kIx,	,
označovaného	označovaný	k2eAgInSc2d1	označovaný
jako	jako	k8xS	jako
Lídr	lídr	k1gMnSc1	lídr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
noci	noc	k1gFnSc2	noc
vylezli	vylézt	k5eAaPmAgMnP	vylézt
z	z	k7c2	z
Cubasiných	Cubasin	k2eAgNnPc2d1	Cubasin
úst	ústa	k1gNnPc2	ústa
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
People	People	k1gFnSc2	People
<g/>
"	"	kIx"	"
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
spřádat	spřádat	k5eAaImF	spřádat
kuklu	kukla	k1gFnSc4	kukla
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
se	se	k3xPyFc4	se
po	po	k7c6	po
holčičce	holčička	k1gFnSc6	holčička
slehla	slehnout	k5eAaPmAgFnS	slehnout
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
vlčáka	vlčák	k1gMnSc4	vlčák
před	před	k7c7	před
dveřmi	dveře	k1gFnPc7	dveře
nalezli	naleznout	k5eAaPmAgMnP	naleznout
roztrhaného	roztrhaný	k2eAgNnSc2d1	roztrhané
<g/>
.	.	kIx.	.
</s>
<s>
Vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ztratila	ztratit	k5eAaPmAgFnS	ztratit
dceru	dcera	k1gFnSc4	dcera
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
ji	on	k3xPp3gFnSc4	on
adoptovat	adoptovat	k5eAaPmF	adoptovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paní	paní	k1gFnSc1	paní
z	z	k7c2	z
Abasu	Abas	k1gInSc2	Abas
získala	získat	k5eAaPmAgFnS	získat
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
Lídrově	lídrův	k2eAgInSc6d1	lídrův
zneužívání	zneužívání	k1gNnSc2	zneužívání
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
dívenky	dívenka	k1gFnPc1	dívenka
měly	mít	k5eAaImAgFnP	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
kněžky	kněžka	k1gFnPc1	kněžka
<g/>
.	.	kIx.	.
</s>
<s>
Aomame	Aomam	k1gInSc5	Aomam
proto	proto	k8xC	proto
obdržela	obdržet	k5eAaPmAgFnS	obdržet
obtížný	obtížný	k2eAgInSc4d1	obtížný
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
zprovodit	zprovodit	k5eAaPmF	zprovodit
dalšího	další	k2eAgMnSc4d1	další
trýznitele	trýznitel	k1gMnSc4	trýznitel
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Lídrovu	lídrův	k2eAgFnSc4d1	lídrův
obrovitou	obrovitý	k2eAgFnSc4d1	obrovitá
postavu	postava	k1gFnSc4	postava
sužovaly	sužovat	k5eAaImAgFnP	sužovat
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
bolesti	bolest	k1gFnPc1	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
masérku	masérka	k1gFnSc4	masérka
s	s	k7c7	s
dobrými	dobrý	k2eAgFnPc7d1	dobrá
referencemi	reference	k1gFnPc7	reference
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
schopnosti	schopnost	k1gFnPc1	schopnost
patřily	patřit	k5eAaImAgFnP	patřit
telekineze	telekineze	k1gFnPc4	telekineze
i	i	k9	i
slyšení	slyšení	k1gNnSc1	slyšení
hlasů	hlas	k1gInPc2	hlas
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
People	People	k1gMnSc1	People
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
představovaly	představovat	k5eAaImAgInP	představovat
životně	životně	k6eAd1	životně
důležitou	důležitý	k2eAgFnSc4d1	důležitá
vlastnost	vlastnost	k1gFnSc4	vlastnost
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
sekty	sekta	k1gFnSc2	sekta
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
Fukaeřina	Fukaeřin	k2eAgMnSc4d1	Fukaeřin
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Zářijový	zářijový	k2eAgInSc1d1	zářijový
večer	večer	k1gInSc1	večer
jej	on	k3xPp3gMnSc4	on
Aomame	Aomam	k1gInSc5	Aomam
navštívila	navštívit	k5eAaPmAgFnS	navštívit
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolu	spolu	k6eAd1	spolu
vedli	vést	k5eAaImAgMnP	vést
podnětný	podnětný	k2eAgInSc4d1	podnětný
rozhovor	rozhovor	k1gInSc4	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Cvičitelku	cvičitelka	k1gFnSc4	cvičitelka
překvapil	překvapit	k5eAaPmAgInS	překvapit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lídr	lídr	k1gMnSc1	lídr
znal	znát	k5eAaImAgMnS	znát
její	její	k3xOp3gInSc4	její
vražedný	vražedný	k2eAgInSc4d1	vražedný
úmysl	úmysl	k1gInSc4	úmysl
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
vykonání	vykonání	k1gNnSc4	vykonání
tohoto	tento	k3xDgInSc2	tento
činu	čin	k1gInSc2	čin
dokonce	dokonce	k9	dokonce
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vysvobození	vysvobození	k1gNnSc4	vysvobození
od	od	k7c2	od
bolesti	bolest	k1gFnSc2	bolest
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
ochránit	ochránit	k5eAaPmF	ochránit
Tenga	Teng	k1gMnSc4	Teng
<g/>
.	.	kIx.	.
</s>
<s>
Šikovná	šikovný	k2eAgFnSc1d1	šikovná
ruka	ruka	k1gFnSc1	ruka
Aomame	Aomam	k1gInSc5	Aomam
tak	tak	k9	tak
zacílila	zacílit	k5eAaPmAgFnS	zacílit
jehlu	jehla	k1gFnSc4	jehla
do	do	k7c2	do
zátylku	zátylek	k1gInSc2	zátylek
a	a	k8xC	a
poslala	poslat	k5eAaPmAgFnS	poslat
vůdce	vůdce	k1gMnPc4	vůdce
na	na	k7c6	na
"	"	kIx"	"
<g/>
druhý	druhý	k4xOgInSc1	druhý
břeh	břeh	k1gInSc1	břeh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
pak	pak	k6eAd1	pak
informovala	informovat	k5eAaBmAgFnS	informovat
členy	člen	k1gMnPc4	člen
ochranky	ochranka	k1gFnSc2	ochranka
Ohona	Ohono	k1gNnSc2	Ohono
s	s	k7c7	s
Holohlavcem	holohlavec	k1gMnSc7	holohlavec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
nechali	nechat	k5eAaPmAgMnP	nechat
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
prospat	prospat	k5eAaPmF	prospat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čas	čas	k1gInSc4	čas
využila	využít	k5eAaPmAgFnS	využít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
do	do	k7c2	do
anonymního	anonymní	k2eAgInSc2d1	anonymní
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
dočasného	dočasný	k2eAgNnSc2d1	dočasné
útočiště	útočiště	k1gNnSc2	útočiště
před	před	k7c7	před
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
změnou	změna	k1gFnSc7	změna
identity	identita	k1gFnSc2	identita
včetně	včetně	k7c2	včetně
přeoperované	přeoperovaný	k2eAgFnSc2d1	přeoperovaná
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aomame	Aomam	k1gInSc5	Aomam
a	a	k8xC	a
Tengo	Tengo	k6eAd1	Tengo
se	se	k3xPyFc4	se
znali	znát	k5eAaImAgMnP	znát
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
čibské	čibský	k2eAgFnSc6d1	čibský
Ičikavě	Ičikavě	k1gFnSc6	Ičikavě
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
chodili	chodit	k5eAaImAgMnP	chodit
do	do	k7c2	do
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
a	a	k8xC	a
páté	pátá	k1gFnSc2	pátá
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
viděli	vidět	k5eAaImAgMnP	vidět
jako	jako	k8xS	jako
desetiletí	desetiletí	k1gNnSc4	desetiletí
<g/>
,	,	kIx,	,
častokrát	častokrát	k6eAd1	častokrát
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Samotářskou	samotářský	k2eAgFnSc4d1	samotářská
dívku	dívka	k1gFnSc4	dívka
ze	z	k7c2	z
sektářské	sektářský	k2eAgFnSc2d1	sektářská
rodiny	rodina	k1gFnSc2	rodina
dětský	dětský	k2eAgInSc1d1	dětský
kolektiv	kolektiv	k1gInSc1	kolektiv
nepřijal	přijmout	k5eNaPmAgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Tengem	Teng	k1gInSc7	Teng
však	však	k9	však
poznali	poznat	k5eAaPmAgMnP	poznat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
hřejivou	hřejivý	k2eAgFnSc4d1	hřejivá
náklonnost	náklonnost	k1gFnSc4	náklonnost
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
jen	jen	k9	jen
s	s	k7c7	s
přísným	přísný	k2eAgMnSc7d1	přísný
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
výběrčí	výběrčí	k1gFnSc4	výběrčí
televizních	televizní	k2eAgInPc2d1	televizní
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
paralelní	paralelní	k2eAgInPc1d1	paralelní
světy	svět	k1gInPc1	svět
se	se	k3xPyFc4	se
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgFnP	začít
přibližovat	přibližovat	k5eAaImF	přibližovat
až	až	k9	až
se	se	k3xPyFc4	se
protly	protnout	k5eAaPmAgFnP	protnout
<g/>
.	.	kIx.	.
</s>
<s>
Změněnou	změněný	k2eAgFnSc4d1	změněná
realitu	realita	k1gFnSc4	realita
si	se	k3xPyFc3	se
Tengo	Tengo	k6eAd1	Tengo
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
Kočičím	kočičí	k2eAgNnSc7d1	kočičí
městem	město	k1gNnSc7	město
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mazané	mazaný	k2eAgNnSc4d1	mazané
soukromé	soukromý	k2eAgNnSc4d1	soukromé
očko	očko	k1gNnSc4	očko
Ušikawa	Ušikawa	k1gMnSc1	Ušikawa
<g/>
,	,	kIx,	,
ošklivý	ošklivý	k2eAgMnSc1d1	ošklivý
chlap	chlap	k1gMnSc1	chlap
po	po	k7c6	po
vnější	vnější	k2eAgFnSc6d1	vnější
i	i	k8xC	i
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stránce	stránka	k1gFnSc6	stránka
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
napravit	napravit	k5eAaPmF	napravit
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
prověřoval	prověřovat	k5eAaImAgMnS	prověřovat
Aomame	Aomam	k1gInSc5	Aomam
před	před	k7c7	před
setkáním	setkání	k1gNnSc7	setkání
s	s	k7c7	s
Lídrem	lídr	k1gMnSc7	lídr
<g/>
,	,	kIx,	,
a	a	k8xC	a
selhal	selhat	k5eAaPmAgInS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Vražedkyni	vražedkyně	k1gFnSc4	vražedkyně
musel	muset	k5eAaImAgMnS	muset
vypátrat	vypátrat	k5eAaPmF	vypátrat
stůj	stát	k5eAaImRp2nS	stát
co	co	k8xS	co
stůj	stát	k5eAaImRp2nS	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zachránil	zachránit	k5eAaPmAgMnS	zachránit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Vychytralý	vychytralý	k2eAgMnSc1d1	vychytralý
exprávník	exprávník	k1gMnSc1	exprávník
odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
Tengem	Teng	k1gMnSc7	Teng
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
představoval	představovat	k5eAaImAgMnS	představovat
nejslibnější	slibní	k2eAgFnSc4d3	slibní
volbu	volba	k1gFnSc4	volba
pro	pro	k7c4	pro
nalezení	nalezení	k1gNnSc4	nalezení
ukryté	ukrytý	k2eAgFnSc2d1	ukrytá
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Tenga	Teng	k1gMnSc4	Teng
sledovat	sledovat	k5eAaImF	sledovat
z	z	k7c2	z
pronajatého	pronajatý	k2eAgInSc2d1	pronajatý
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nepřehlédnutelný	přehlédnutelný	k2eNgInSc1d1	nepřehlédnutelný
zjev	zjev	k1gInSc1	zjev
<g/>
,	,	kIx,	,
s	s	k7c7	s
šišatou	šišatý	k2eAgFnSc7d1	šišatá
hlavou	hlava	k1gFnSc7	hlava
panáčka	panáček	k1gMnSc2	panáček
Fukusukeho	Fukusuke	k1gMnSc2	Fukusuke
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
zachycen	zachytit	k5eAaPmNgInS	zachytit
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
kamerou	kamera	k1gFnSc7	kamera
u	u	k7c2	u
Vrbové	Vrbové	k2eAgFnSc2d1	Vrbové
lípy	lípa	k1gFnSc2	lípa
paní	paní	k1gFnSc2	paní
z	z	k7c2	z
Azabu	Azab	k1gInSc2	Azab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
sekty	sekta	k1gFnSc2	sekta
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zamezení	zamezení	k1gNnSc1	zamezení
dalšího	další	k2eAgNnSc2d1	další
šíření	šíření	k1gNnSc2	šíření
Fukaeřina	Fukaeřin	k2eAgInSc2d1	Fukaeřin
bestselleru	bestseller	k1gInSc2	bestseller
Kukel	kukla	k1gFnPc2	kukla
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
fascinoval	fascinovat	k5eAaBmAgMnS	fascinovat
čtenáře	čtenář	k1gMnSc4	čtenář
<g/>
,	,	kIx,	,
hltající	hltající	k2eAgFnSc4d1	hltající
útlou	útlý	k2eAgFnSc4d1	útlá
knihu	kniha	k1gFnSc4	kniha
jako	jako	k8xS	jako
smyšlený	smyšlený	k2eAgInSc4d1	smyšlený
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vydání	vydání	k1gNnSc1	vydání
však	však	k9	však
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
ztrátu	ztráta	k1gFnSc4	ztráta
Lídrovy	lídrův	k2eAgFnSc2d1	lídrův
schopnosti	schopnost	k1gFnSc2	schopnost
slyšet	slyšet	k5eAaImF	slyšet
hlasy	hlas	k1gInPc4	hlas
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
People	People	k1gMnSc1	People
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
pod	pod	k7c4	pod
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ušikawa	Ušikawa	k1gMnSc1	Ušikawa
si	se	k3xPyFc3	se
z	z	k7c2	z
pronajatého	pronajatý	k2eAgInSc2d1	pronajatý
bytu	byt	k1gInSc2	byt
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
osoby	osoba	k1gFnPc4	osoba
vstupující	vstupující	k2eAgFnPc4d1	vstupující
a	a	k8xC	a
vycházející	vycházející	k2eAgFnPc4d1	vycházející
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
vytušila	vytušit	k5eAaPmAgNnP	vytušit
citlivá	citlivý	k2eAgNnPc1d1	citlivé
Fukaeri	Fukaeri	k1gNnPc1	Fukaeri
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
Tenga	Teng	k1gMnSc2	Teng
skrývala	skrývat	k5eAaImAgFnS	skrývat
<g/>
.	.	kIx.	.
</s>
<s>
Sbalila	sbalit	k5eAaPmAgFnS	sbalit
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnPc4	svůj
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
nadobro	nadobro	k6eAd1	nadobro
odtud	odtud	k6eAd1	odtud
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
východu	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
zahleděla	zahledět	k5eAaPmAgFnS	zahledět
do	do	k7c2	do
skrytého	skrytý	k2eAgInSc2d1	skrytý
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
Ušikawovi	Ušikawův	k2eAgMnPc1d1	Ušikawův
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
věděl	vědět	k5eAaImAgMnS	vědět
že	že	k8xS	že
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
ránu	rána	k1gFnSc4	rána
na	na	k7c4	na
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
roku	rok	k1gInSc2	rok
1Q84	[number]	k4	1Q84
pluly	plout	k5eAaImAgInP	plout
dva	dva	k4xCgInPc1	dva
měsíce	měsíc	k1gInPc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
žlutou	žlutý	k2eAgFnSc4d1	žlutá
lunu	luna	k1gFnSc4	luna
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
malý	malý	k2eAgInSc1d1	malý
zelený	zelený	k2eAgInSc1d1	zelený
kotouč	kotouč	k1gInSc1	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Tengo	Tengo	k6eAd1	Tengo
tento	tento	k3xDgInSc4	tento
výjev	výjev	k1gInSc4	výjev
upřeně	upřeně	k6eAd1	upřeně
sledoval	sledovat	k5eAaImAgMnS	sledovat
z	z	k7c2	z
klouzačky	klouzačka	k1gFnSc2	klouzačka
před	před	k7c7	před
apartmánem	apartmán	k1gInSc7	apartmán
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
skrývala	skrývat	k5eAaImAgFnS	skrývat
Aomame	Aomam	k1gInSc5	Aomam
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
co	co	k9	co
bývalého	bývalý	k2eAgMnSc4d1	bývalý
spolužáka	spolužák	k1gMnSc4	spolužák
zahlédla	zahlédnout	k5eAaPmAgFnS	zahlédnout
z	z	k7c2	z
balkónu	balkón	k1gInSc2	balkón
a	a	k8xC	a
promarnila	promarnit	k5eAaPmAgFnS	promarnit
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
setkání	setkání	k1gNnSc4	setkání
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
neúnavně	únavně	k6eNd1	únavně
vyhlížela	vyhlížet	k5eAaImAgFnS	vyhlížet
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Ušikawův	Ušikawův	k2eAgInSc4d1	Ušikawův
výstřední	výstřední	k2eAgInSc4d1	výstřední
vzhled	vzhled	k1gInSc4	vzhled
znala	znát	k5eAaImAgFnS	znát
od	od	k7c2	od
Tamarua	Tamaru	k1gInSc2	Tamaru
<g/>
.	.	kIx.	.
</s>
<s>
Slídil	slídil	k1gMnSc1	slídil
zavěšený	zavěšený	k2eAgMnSc1d1	zavěšený
na	na	k7c4	na
Tenga	Teng	k1gMnSc4	Teng
nechápal	chápat	k5eNaImAgMnS	chápat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jen	jen	k9	jen
může	moct	k5eAaImIp3nS	moct
nahoře	nahoře	k6eAd1	nahoře
hledat	hledat	k5eAaImF	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
následujícího	následující	k2eAgInSc2d1	následující
večera	večer	k1gInSc2	večer
vylezl	vylézt	k5eAaPmAgMnS	vylézt
na	na	k7c4	na
klouzačku	klouzačka	k1gFnSc4	klouzačka
také	také	k9	také
a	a	k8xC	a
ohromeně	ohromeně	k6eAd1	ohromeně
spatřil	spatřit	k5eAaPmAgMnS	spatřit
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
masérka	masérka	k1gFnSc1	masérka
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
koho	kdo	k3yInSc4	kdo
má	mít	k5eAaImIp3nS	mít
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
využila	využít	k5eAaPmAgFnS	využít
šance	šance	k1gFnSc1	šance
a	a	k8xC	a
nerozvážně	rozvážně	k6eNd1	rozvážně
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
Ušikawu	Ušikawa	k1gFnSc4	Ušikawa
špehovat	špehovat	k5eAaImF	špehovat
<g/>
.	.	kIx.	.
</s>
<s>
Dorazila	dorazit	k5eAaPmAgFnS	dorazit
tak	tak	k9	tak
až	až	k9	až
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
vysněný	vysněný	k2eAgMnSc1d1	vysněný
přítel	přítel	k1gMnSc1	přítel
z	z	k7c2	z
mladí	mladý	k2eAgMnPc1d1	mladý
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
však	však	k9	však
nenacházel	nacházet	k5eNaImAgMnS	nacházet
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
zjištění	zjištění	k1gNnPc4	zjištění
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Tamaruovi	Tamaru	k1gMnSc3	Tamaru
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přijel	přijet	k5eAaPmAgMnS	přijet
slídila	slídil	k1gMnSc4	slídil
zvraždit	zvraždit	k5eAaPmF	zvraždit
a	a	k8xC	a
vytěžit	vytěžit	k5eAaPmF	vytěžit
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obdrženého	obdržený	k2eAgNnSc2d1	obdržené
čísla	číslo	k1gNnSc2	číslo
zavolal	zavolat	k5eAaPmAgMnS	zavolat
do	do	k7c2	do
Předvoje	předvoj	k1gInSc2	předvoj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
přijeli	přijet	k5eAaPmAgMnP	přijet
uklidit	uklidit	k5eAaPmF	uklidit
mrtvolu	mrtvola	k1gFnSc4	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
informovaly	informovat	k5eAaBmAgFnP	informovat
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
záměrech	záměr	k1gInPc6	záměr
a	a	k8xC	a
postojích	postoj	k1gInPc6	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Tamaru	Tamara	k1gFnSc4	Tamara
naznal	naznat	k5eAaPmAgMnS	naznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cílem	cíl	k1gInSc7	cíl
sekty	sekta	k1gFnSc2	sekta
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
likvidace	likvidace	k1gFnSc1	likvidace
vražedkyně	vražedkyně	k1gFnSc1	vražedkyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přesto	přesto	k6eAd1	přesto
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
součástí	součást	k1gFnSc7	součást
jejich	jejich	k3xOp3gInSc2	jejich
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Nenapadlo	napadnout	k5eNaPmAgNnS	napadnout
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
Předvoj	předvoj	k1gInSc4	předvoj
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zatelefonoval	zatelefonovat	k5eAaPmAgMnS	zatelefonovat
Tengovi	Teng	k1gMnSc3	Teng
a	a	k8xC	a
tlumočil	tlumočit	k5eAaImAgInS	tlumočit
mu	on	k3xPp3gMnSc3	on
žádost	žádost	k1gFnSc4	žádost
Aomame	Aomam	k1gInSc5	Aomam
o	o	k7c6	o
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
spolužáci	spolužák	k1gMnPc1	spolužák
se	se	k3xPyFc4	se
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
opět	opět	k6eAd1	opět
střetli	střetnout	k5eAaPmAgMnP	střetnout
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
klouzačce	klouzačka	k1gFnSc6	klouzačka
<g/>
,	,	kIx,	,
rozhodnuti	rozhodnout	k5eAaPmNgMnP	rozhodnout
opustit	opustit	k5eAaPmF	opustit
paralelní	paralelní	k2eAgInSc4d1	paralelní
svět	svět	k1gInSc4	svět
1	[number]	k4	1
<g/>
Q	Q	kA	Q
<g/>
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
<s>
Aomame	Aomam	k1gInSc5	Aomam
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
tušila	tušit	k5eAaImAgFnS	tušit
jen	jen	k9	jen
jediný	jediný	k2eAgInSc4d1	jediný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
vylézt	vylézt	k5eAaPmF	vylézt
únikovým	únikový	k2eAgNnSc7d1	únikové
schodištěm	schodiště	k1gNnSc7	schodiště
na	na	k7c4	na
magistrálu	magistrála	k1gFnSc4	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Tengo	Tengo	k6eAd1	Tengo
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
překvapivou	překvapivý	k2eAgFnSc4d1	překvapivá
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
otcem	otec	k1gMnSc7	otec
jejího	její	k3xOp3gNnSc2	její
dosud	dosud	k6eAd1	dosud
nenarozeného	narozený	k2eNgMnSc4d1	nenarozený
potomka	potomek	k1gMnSc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Fyzioterapeutka	fyzioterapeutka	k1gFnSc1	fyzioterapeutka
si	se	k3xPyFc3	se
byla	být	k5eAaImAgFnS	být
jistá	jistý	k2eAgFnSc1d1	jistá
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
oné	onen	k3xDgFnSc6	onen
zářijové	zářijový	k2eAgFnSc6d1	zářijová
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zprovodila	zprovodit	k5eAaPmAgFnS	zprovodit
Lídra	lídr	k1gMnSc4	lídr
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Styk	styk	k1gInSc1	styk
přitom	přitom	k6eAd1	přitom
s	s	k7c7	s
nikým	nikdo	k3yNnSc7	nikdo
neměla	mít	k5eNaImAgFnS	mít
od	od	k7c2	od
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Tengovi	Teng	k1gMnSc3	Teng
to	ten	k3xDgNnSc1	ten
však	však	k9	však
vposledku	vposledku	k?	vposledku
nepřipadalo	připadat	k5eNaImAgNnS	připadat
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	ten	k3xDgFnSc4	ten
tajemnou	tajemný	k2eAgFnSc4d1	tajemná
noc	noc	k1gFnSc4	noc
jedinkrát	jedinkrát	k6eAd1	jedinkrát
souložil	souložit	k5eAaImAgMnS	souložit
s	s	k7c7	s
Fukaeri	Fukaer	k1gMnPc7	Fukaer
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
dívka	dívka	k1gFnSc1	dívka
byla	být	k5eAaImAgFnS	být
asexuálně	asexuálně	k6eAd1	asexuálně
založena	založit	k5eAaPmNgFnS	založit
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
z	z	k7c2	z
neznámého	známý	k2eNgInSc2d1	neznámý
popudu	popud	k1gInSc2	popud
nasedla	nasednout	k5eAaPmAgFnS	nasednout
<g/>
,	,	kIx,	,
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
do	do	k7c2	do
ni	on	k3xPp3gFnSc4	on
silně	silně	k6eAd1	silně
ejakuloval	ejakulovat	k5eAaImAgInS	ejakulovat
<g/>
.	.	kIx.	.
</s>
<s>
Nepodobalo	podobat	k5eNaImAgNnS	podobat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
přitom	přitom	k6eAd1	přitom
klasickému	klasický	k2eAgInSc3d1	klasický
styku	styk	k1gInSc3	styk
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejím	její	k3xOp3gFnPc3	její
výjimečným	výjimečný	k2eAgFnPc3d1	výjimečná
schopnostem	schopnost	k1gFnPc3	schopnost
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
sehrála	sehrát	k5eAaPmAgFnS	sehrát
úlohu	úloha	k1gFnSc4	úloha
koridoru	koridor	k1gInSc2	koridor
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
dělohou	děloha	k1gFnSc7	děloha
Aomame	Aomam	k1gInSc5	Aomam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
město	město	k1gNnSc4	město
se	se	k3xPyFc4	se
té	ten	k3xDgFnSc2	ten
noci	noc	k1gFnSc2	noc
snesla	snést	k5eAaPmAgFnS	snést
nejprudší	prudký	k2eAgFnSc1d3	nejprudší
bouřka	bouřka	k1gFnSc1	bouřka
s	s	k7c7	s
hromy	hrom	k1gInPc7	hrom
a	a	k8xC	a
blesky	blesk	k1gInPc7	blesk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mladá	mladý	k2eAgFnSc1d1	mladá
Fukaeri	Fukaer	k1gMnPc7	Fukaer
komentovala	komentovat	k5eAaBmAgFnS	komentovat
typicky	typicky	k6eAd1	typicky
úsečným	úsečný	k2eAgNnSc7d1	úsečné
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Lidl	Lidl	k1gInSc1	Lidl
pipl	pipnout	k5eAaPmAgInS	pipnout
zase	zase	k9	zase
vivádějý	vivádějý	k?	vivádějý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Východ	východ	k1gInSc1	východ
na	na	k7c4	na
plošinu	plošina	k1gFnSc4	plošina
magistrály	magistrála	k1gFnSc2	magistrála
byl	být	k5eAaImAgInS	být
opravdu	opravdu	k6eAd1	opravdu
otevřen	otevřít	k5eAaPmNgInS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
na	na	k7c6	na
ni	on	k3xPp3gFnSc4	on
vystoupali	vystoupat	k5eAaPmAgMnP	vystoupat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přese	přese	k7c4	přese
všechno	všechen	k3xTgNnSc4	všechen
bylo	být	k5eAaImAgNnS	být
něco	něco	k3yInSc1	něco
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Tygr	tygr	k1gMnSc1	tygr
s	s	k7c7	s
tankovací	tankovací	k2eAgFnSc7d1	tankovací
pistolí	pistol	k1gFnSc7	pistol
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
na	na	k7c6	na
billboardu	billboard	k1gInSc6	billboard
od	od	k7c2	od
Essa	Ess	k1gInSc2	Ess
<g/>
,	,	kIx,	,
shlížel	shlížet	k5eAaImAgMnS	shlížet
na	na	k7c4	na
dvojici	dvojice	k1gFnSc4	dvojice
směrem	směr	k1gInSc7	směr
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
dubnovém	dubnový	k2eAgInSc6d1	dubnový
sestupu	sestup	k1gInSc6	sestup
Aomame	Aomam	k1gInSc5	Aomam
měl	mít	k5eAaImAgInS	mít
hlavu	hlava	k1gFnSc4	hlava
nakloněnou	nakloněný	k2eAgFnSc4d1	nakloněná
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
plul	plout	k5eAaImAgInS	plout
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
naštěstí	naštěstí	k6eAd1	naštěstí
opět	opět	k6eAd1	opět
jen	jen	k9	jen
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
si	se	k3xPyFc3	se
však	však	k9	však
náhle	náhle	k6eAd1	náhle
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
závažnost	závažnost	k1gFnSc1	závažnost
Lídrova	lídrův	k2eAgNnSc2d1	lídrův
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1Q84	[number]	k4	1Q84
nelze	lze	k6eNd1	lze
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
přišla	přijít	k5eAaPmAgFnS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
tušila	tušit	k5eAaImAgFnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Předvoj	předvoj	k1gInSc1	předvoj
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mohl	moct	k5eAaImAgInS	moct
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
nenarozeném	narozený	k2eNgMnSc6d1	nenarozený
potomku	potomek	k1gMnSc6	potomek
v	v	k7c6	v
očekávání	očekávání	k1gNnSc6	očekávání
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jednou	k6eAd1	jednou
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
pozici	pozice	k1gFnSc4	pozice
"	"	kIx"	"
<g/>
Toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
slyší	slyšet	k5eAaImIp3nP	slyšet
hlasy	hlas	k1gInPc4	hlas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Věřili	věřit	k5eAaImAgMnP	věřit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
alternativní	alternativní	k2eAgInSc4d1	alternativní
svět	svět	k1gInSc4	svět
navždy	navždy	k6eAd1	navždy
právě	právě	k6eAd1	právě
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Zamířili	zamířit	k5eAaPmAgMnP	zamířit
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
a	a	k8xC	a
objednali	objednat	k5eAaPmAgMnP	objednat
si	se	k3xPyFc3	se
pokoj	pokoj	k1gInSc4	pokoj
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
setmělým	setmělý	k2eAgInSc7d1	setmělý
horizontem	horizont	k1gInSc7	horizont
putoval	putovat	k5eAaImAgInS	putovat
stále	stále	k6eAd1	stále
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
osamocený	osamocený	k2eAgMnSc1d1	osamocený
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
milovat	milovat	k5eAaImF	milovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kukly	Kukla	k1gMnSc2	Kukla
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
===	===	k?	===
</s>
</p>
<p>
<s>
Krátký	krátký	k2eAgInSc1d1	krátký
fantaskní	fantaskní	k2eAgInSc1d1	fantaskní
příběh	příběh	k1gInSc1	příběh
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
světě	svět	k1gInSc6	svět
plném	plný	k2eAgInSc6d1	plný
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc2	Little
People	People	k1gFnSc2	People
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
autorkou	autorka	k1gFnSc7	autorka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
mladá	mladý	k2eAgFnSc1d1	mladá
dívka	dívka	k1gFnSc1	dívka
Fukaeri	Fukaer	k1gFnSc2	Fukaer
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
Lídra	lídr	k1gMnSc2	lídr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
knižní	knižní	k2eAgNnSc4d1	knižní
vydání	vydání	k1gNnSc4	vydání
a	a	k8xC	a
literární	literární	k2eAgFnSc4d1	literární
soutěž	soutěž	k1gFnSc4	soutěž
děj	děj	k1gInSc1	děj
přepsal	přepsat	k5eAaPmAgInS	přepsat
nadějný	nadějný	k2eAgMnSc1d1	nadějný
spisovatel	spisovatel	k1gMnSc1	spisovatel
Tengo	Tengo	k1gMnSc1	Tengo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
je	být	k5eAaImIp3nS	být
desetiletá	desetiletý	k2eAgFnSc1d1	desetiletá
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
komuně	komuna	k1gFnSc6	komuna
<g/>
.	.	kIx.	.
</s>
<s>
Záhadné	záhadný	k2eAgFnPc1d1	záhadná
bytosti	bytost	k1gFnPc1	bytost
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
People	People	k1gFnSc2	People
<g/>
"	"	kIx"	"
vylézají	vylézat	k5eAaImIp3nP	vylézat
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
z	z	k7c2	z
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
kozy	koza	k1gFnSc2	koza
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
splétaly	splétat	k5eAaImAgFnP	splétat
kukly	kukla	k1gFnPc1	kukla
<g/>
.	.	kIx.	.
</s>
<s>
Zámotek	zámotek	k1gInSc1	zámotek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
duplikát	duplikát	k1gInSc4	duplikát
malé	malý	k2eAgFnSc2d1	malá
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
daughter	daughter	k1gInSc4	daughter
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
pak	pak	k6eAd1	pak
hraje	hrát	k5eAaImIp3nS	hrát
úlohu	úloha	k1gFnSc4	úloha
mother	mothra	k1gFnPc2	mothra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nebi	nebe	k1gNnSc6	nebe
plují	plout	k5eAaImIp3nP	plout
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc4d1	velký
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odrážející	odrážející	k2eAgInSc4d1	odrážející
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c4	mezi
mother	mothra	k1gFnPc2	mothra
a	a	k8xC	a
daughter	daughtra	k1gFnPc2	daughtra
<g/>
.	.	kIx.	.
<g/>
Tengo	Tengo	k6eAd1	Tengo
o	o	k7c6	o
příběhu	příběh	k1gInSc6	příběh
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
a	a	k8xC	a
spekuloval	spekulovat	k5eAaImAgMnS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
autobiografii	autobiografie	k1gFnSc4	autobiografie
Fukaeri	Fukaer	k1gFnSc2	Fukaer
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nehodlala	hodlat	k5eNaImAgFnS	hodlat
dále	daleko	k6eAd2	daleko
plnit	plnit	k5eAaImF	plnit
roli	role	k1gFnSc4	role
mother	mothra	k1gFnPc2	mothra
a	a	k8xC	a
tak	tak	k6eAd1	tak
z	z	k7c2	z
komuny	komuna	k1gFnSc2	komuna
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
<g/>
.	.	kIx.	.
<g/>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
slyší	slyšet	k5eAaImIp3nS	slyšet
hlasy	hlas	k1gInPc4	hlas
zřejmě	zřejmě	k6eAd1	zřejmě
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
daughter	daughtra	k1gFnPc2	daughtra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
přijímala	přijímat	k5eAaImAgFnS	přijímat
zvukové	zvukový	k2eAgInPc4d1	zvukový
vjemy	vjem	k1gInPc4	vjem
od	od	k7c2	od
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc2	Little
People	People	k1gFnSc2	People
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lídr	lídr	k1gMnSc1	lídr
byl	být	k5eAaImAgMnS	být
nadán	nadat	k5eAaPmNgMnS	nadat
schopností	schopnost	k1gFnSc7	schopnost
slyšet	slyšet	k5eAaImF	slyšet
a	a	k8xC	a
porozumět	porozumět	k5eAaPmF	porozumět
obsahu	obsah	k1gInSc3	obsah
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
Fukaeri	Fukaeri	k6eAd1	Fukaeri
tento	tento	k3xDgInSc1	tento
nutný	nutný	k2eAgInSc1d1	nutný
vztah	vztah	k1gInSc1	vztah
dvou	dva	k4xCgFnPc2	dva
osob	osoba	k1gFnPc2	osoba
přirovnala	přirovnat	k5eAaPmAgFnS	přirovnat
k	k	k7c3	k
vazbě	vazba	k1gFnSc3	vazba
mezi	mezi	k7c7	mezi
perceiverem	perceivero	k1gNnSc7	perceivero
a	a	k8xC	a
receiverem	receivero	k1gNnSc7	receivero
–	–	k?	–
tedy	tedy	k9	tedy
ke	k	k7c3	k
vztahu	vztah	k1gInSc3	vztah
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
hlasy	hlas	k1gInPc4	hlas
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	on	k3xPp3gMnPc4	on
slyší	slyšet	k5eAaImIp3nS	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
správné	správný	k2eAgFnSc2d1	správná
daughter	daughtrum	k1gNnPc2	daughtrum
z	z	k7c2	z
kukly	kukla	k1gFnSc2	kukla
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nejdříve	dříve	k6eAd3	dříve
nutné	nutný	k2eAgNnSc1d1	nutné
zajistit	zajistit	k5eAaPmF	zajistit
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
mother	mothra	k1gFnPc2	mothra
<g/>
,	,	kIx,	,
domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
Tengo	Tengo	k1gMnSc1	Tengo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Aomame	Aomam	k1gInSc5	Aomam
</s>
</p>
<p>
<s>
Aomame	Aomam	k1gInSc5	Aomam
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
青	青	k?	青
<g/>
,	,	kIx,	,
Hepburn	Hepburn	k1gMnSc1	Hepburn
<g/>
:	:	kIx,	:
Aomame	Aomam	k1gInSc5	Aomam
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
fyzioterapeutka	fyzioterapeutka	k1gFnSc1	fyzioterapeutka
a	a	k8xC	a
organizátorka	organizátorka	k1gFnSc1	organizátorka
kurzů	kurz	k1gInPc2	kurz
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
je	být	k5eAaImIp3nS	být
najímána	najímat	k5eAaImNgFnS	najímat
tajemnou	tajemný	k2eAgFnSc7d1	tajemná
organizací	organizace	k1gFnSc7	organizace
na	na	k7c4	na
vraždy	vražda	k1gFnPc4	vražda
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
provinily	provinit	k5eAaPmAgFnP	provinit
násilím	násilí	k1gNnSc7	násilí
na	na	k7c6	na
bližních	bližní	k1gMnPc6	bližní
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
něž	jenž	k3xRgInPc4	jenž
je	být	k5eAaImIp3nS	být
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
technice	technika	k1gFnSc3	technika
provedení	provedení	k1gNnSc2	provedení
vypadají	vypadat	k5eAaPmIp3nP	vypadat
vraždy	vražda	k1gFnPc1	vražda
jako	jako	k8xC	jako
přirozená	přirozený	k2eAgNnPc1d1	přirozené
úmrtí	úmrtí	k1gNnPc1	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
zní	znět	k5eAaImIp3nS	znět
Masami	masa	k1gFnPc7	masa
Aomame	Aomam	k1gInSc5	Aomam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
je	být	k5eAaImIp3nS	být
dominantně	dominantně	k6eAd1	dominantně
použito	použít	k5eAaPmNgNnS	použít
jen	jen	k9	jen
příjmení	příjmení	k1gNnSc1	příjmení
Aomame	Aomam	k1gInSc5	Aomam
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgInSc1d1	znamenající
"	"	kIx"	"
<g/>
zelený	zelený	k2eAgInSc1d1	zelený
hrášek	hrášek	k1gInSc1	hrášek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vychována	vychován	k2eAgFnSc1d1	vychována
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
členka	členka	k1gFnSc1	členka
sekty	sekta	k1gFnSc2	sekta
Společenství	společenství	k1gNnSc2	společenství
svědků	svědek	k1gMnPc2	svědek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
obcházela	obcházet	k5eAaImAgFnS	obcházet
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
domy	dům	k1gInPc4	dům
a	a	k8xC	a
roznášela	roznášet	k5eAaImAgFnS	roznášet
náboženské	náboženský	k2eAgInPc4d1	náboženský
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
víry	víra	k1gFnSc2	víra
zřekla	zřeknout	k5eAaPmAgFnS	zřeknout
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
pro	pro	k7c4	pro
rodiče	rodič	k1gMnPc4	rodič
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
<g/>
Tengo	Tengo	k6eAd1	Tengo
Kawana	Kawana	k1gFnSc1	Kawana
</s>
</p>
<p>
<s>
Tengo	Tengo	k1gMnSc1	Tengo
Kawana	Kawan	k1gMnSc2	Kawan
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
川	川	k?	川
天	天	k?	天
<g/>
,	,	kIx,	,
Hepburn	Hepburn	k1gInSc1	Hepburn
<g/>
:	:	kIx,	:
Kawana	Kawana	k1gFnSc1	Kawana
Tengo	Tengo	k1gMnSc1	Tengo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
ke	k	k7c3	k
třem	tři	k4xCgMnPc3	tři
ústředním	ústřední	k2eAgMnPc3d1	ústřední
hrdinům	hrdina	k1gMnPc3	hrdina
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
nadějným	nadějný	k2eAgMnSc7d1	nadějný
začínajícím	začínající	k2eAgMnSc7d1	začínající
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
matematiky	matematika	k1gFnSc2	matematika
na	na	k7c6	na
jobikō	jobikō	k?	jobikō
(	(	kIx(	(
<g/>
予	予	k?	予
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typu	typa	k1gFnSc4	typa
přípravné	přípravný	k2eAgFnSc2d1	přípravná
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Udržoval	udržovat	k5eAaImAgInS	udržovat
občasný	občasný	k2eAgInSc1d1	občasný
sexuální	sexuální	k2eAgInSc1d1	sexuální
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
starší	starý	k2eAgInSc1d2	starší
<g/>
,	,	kIx,	,
než	než	k8xS	než
ho	on	k3xPp3gNnSc4	on
ukončil	ukončit	k5eAaPmAgMnS	ukončit
ženin	ženin	k2eAgMnSc1d1	ženin
manžel	manžel	k1gMnSc1	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
–	–	k?	–
dle	dle	k7c2	dle
Ušikawy	Ušikawa	k1gFnSc2	Ušikawa
–	–	k?	–
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
1,5	[number]	k4	1,5
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vrací	vracet	k5eAaImIp3nS	vracet
nejranější	raný	k2eAgInSc1d3	nejranější
výjev	výjev	k1gInSc1	výjev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
matka	matka	k1gFnSc1	matka
nabízela	nabízet	k5eAaImAgFnS	nabízet
prs	prs	k1gInSc4	prs
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
muži	muž	k1gMnSc6	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nebyl	být	k5eNaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Vychoval	vychovat	k5eAaPmAgMnS	vychovat
jej	on	k3xPp3gNnSc4	on
přisný	přisný	k2eAgMnSc1d1	přisný
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
zaměstnaný	zaměstnaný	k1gMnSc1	zaměstnaný
jako	jako	k8xS	jako
výběrčí	výběrčí	k1gFnSc1	výběrčí
televizních	televizní	k2eAgInPc2d1	televizní
poplatků	poplatek	k1gInPc2	poplatek
pro	pro	k7c4	pro
NHK	NHK	kA	NHK
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
neděli	neděle	k1gFnSc4	neděle
jej	on	k3xPp3gInSc4	on
musel	muset	k5eAaImAgMnS	muset
doprovázet	doprovázet	k5eAaImF	doprovázet
po	po	k7c6	po
domech	dům	k1gInPc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
vztah	vztah	k1gInSc1	vztah
byl	být	k5eAaImAgInS	být
chladný	chladný	k2eAgInSc1d1	chladný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
vynikal	vynikat	k5eAaImAgMnS	vynikat
a	a	k8xC	a
ve	v	k7c6	v
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
i	i	k8xC	i
páté	pátý	k4xOgFnSc3	pátý
třídě	třída	k1gFnSc3	třída
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
spolužákyni	spolužákyně	k1gFnSc4	spolužákyně
Aomame	Aomam	k1gInSc5	Aomam
<g/>
.	.	kIx.	.
<g/>
Ušikawa	Ušikawus	k1gMnSc4	Ušikawus
</s>
</p>
<p>
<s>
Ušikawa	Ušikawa	k1gFnSc1	Ušikawa
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
牛	牛	k?	牛
<g/>
,	,	kIx,	,
Hepburn	Hepburn	k1gMnSc1	Hepburn
<g/>
:	:	kIx,	:
Ushikawa	Ushikawa	k1gMnSc1	Ushikawa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
jako	jako	k9	jako
schopný	schopný	k2eAgMnSc1d1	schopný
právník	právník	k1gMnSc1	právník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
později	pozdě	k6eAd2	pozdě
rozvedeného	rozvedený	k2eAgNnSc2d1	rozvedené
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
dvě	dva	k4xCgFnPc1	dva
dcery	dcera	k1gFnPc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Nápadně	nápadně	k6eAd1	nápadně
ošklivý	ošklivý	k2eAgMnSc1d1	ošklivý
muž	muž	k1gMnSc1	muž
se	s	k7c7	s
šišatou	šišatý	k2eAgFnSc7d1	šišatá
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
zkaženého	zkažený	k2eAgInSc2d1	zkažený
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
soukromé	soukromý	k2eAgNnSc4d1	soukromé
očko	očko	k1gNnSc4	očko
<g/>
.	.	kIx.	.
</s>
<s>
Silnou	silný	k2eAgFnSc4d1	silná
stránku	stránka	k1gFnSc4	stránka
je	být	k5eAaImIp3nS	být
neodbytnost	neodbytnost	k1gFnSc1	neodbytnost
a	a	k8xC	a
získávání	získávání	k1gNnSc1	získávání
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Komunita	komunita	k1gFnSc1	komunita
Předvoj	předvoj	k1gInSc1	předvoj
jej	on	k3xPp3gMnSc4	on
najala	najmout	k5eAaPmAgFnS	najmout
na	na	k7c4	na
prověření	prověření	k1gNnSc4	prověření
Tenga	Teng	k1gMnSc2	Teng
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
k	k	k7c3	k
vypátrání	vypátrání	k1gNnSc3	vypátrání
Aomame	Aomam	k1gInSc5	Aomam
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
v	v	k7c6	v
románu	román	k1gInSc6	román
Kronika	kronika	k1gFnSc1	kronika
ptáčka	ptáček	k1gMnSc2	ptáček
na	na	k7c4	na
klíček	klíček	k1gInSc4	klíček
<g/>
.	.	kIx.	.
<g/>
Komacu	Komaca	k1gFnSc4	Komaca
</s>
</p>
<p>
<s>
Komacu	Komacu	k5eAaPmIp1nS	Komacu
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
小	小	k?	小
<g/>
,	,	kIx,	,
Hepburn	Hepburn	k1gMnSc1	Hepburn
<g/>
:	:	kIx,	:
Komatsu	Komats	k1gInSc2	Komats
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
45	[number]	k4	45
<g/>
letý	letý	k2eAgInSc1d1	letý
redaktor	redaktor	k1gMnSc1	redaktor
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
vlastním	vlastní	k2eAgInSc7d1	vlastní
rytmem	rytmus	k1gInSc7	rytmus
života	život	k1gInSc2	život
nehledícím	hledící	k2eNgInSc7d1	nehledící
na	na	k7c4	na
ostatní	ostatní	k2eAgNnSc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Tengovi	Teng	k1gMnSc3	Teng
často	často	k6eAd1	často
volá	volat	k5eAaImIp3nS	volat
za	za	k7c4	za
hluboké	hluboký	k2eAgFnPc4d1	hluboká
noci	noc	k1gFnPc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
dochází	docházet	k5eAaImIp3nS	docházet
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Předchází	předcházet	k5eAaImIp3nP	předcházet
jej	on	k3xPp3gInSc4	on
dobrá	dobrý	k2eAgFnSc1d1	dobrá
profesní	profesní	k2eAgFnSc1d1	profesní
reputace	reputace	k1gFnSc1	reputace
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
přátelská	přátelský	k2eAgFnSc1d1	přátelská
povaha	povaha	k1gFnSc1	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1	soukromý
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
zahalen	zahalit	k5eAaPmNgInS	zahalit
nedostatkem	nedostatek	k1gInSc7	nedostatek
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Tengem	Teng	k1gInSc7	Teng
<g/>
.	.	kIx.	.
</s>
<s>
Předvoj	předvoj	k1gInSc4	předvoj
jej	on	k3xPp3gInSc4	on
unesl	unést	k5eAaPmAgInS	unést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
zastavení	zastavení	k1gNnSc4	zastavení
dalšího	další	k2eAgNnSc2d1	další
šíření	šíření	k1gNnSc2	šíření
knihy	kniha	k1gFnSc2	kniha
Kukel	kukla	k1gFnPc2	kukla
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
bestseller	bestseller	k1gInSc1	bestseller
<g/>
.	.	kIx.	.
<g/>
Fukaeri	Fukaeri	k1gNnSc1	Fukaeri
</s>
</p>
<p>
<s>
Fukaeri	Fukaeri	k1gNnSc1	Fukaeri
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
ふ	ふ	k?	ふ
<g/>
,	,	kIx,	,
Hepburn	Hepburn	k1gInSc1	Hepburn
<g/>
:	:	kIx,	:
Fuka-Eri	Fuka-Eri	k1gNnSc1	Fuka-Eri
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
citlivá	citlivý	k2eAgFnSc1d1	citlivá
17	[number]	k4	17
<g/>
letá	letý	k2eAgFnSc1d1	letá
středoškolačka	středoškolačka	k1gFnSc1	středoškolačka
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
hubená	hubený	k2eAgFnSc1d1	hubená
s	s	k7c7	s
plnými	plný	k2eAgInPc7d1	plný
prsy	prs	k1gInPc7	prs
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
styl	styl	k1gInSc4	styl
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
úsečný	úsečný	k2eAgMnSc1d1	úsečný
a	a	k8xC	a
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
srozumitelný	srozumitelný	k2eAgInSc4d1	srozumitelný
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
flegmatickým	flegmatický	k2eAgInSc7d1	flegmatický
dojmem	dojem	k1gInSc7	dojem
a	a	k8xC	a
asexuálním	asexuální	k2eAgNnSc7d1	asexuální
založením	založení	k1gNnSc7	založení
<g/>
.	.	kIx.	.
</s>
<s>
Trpí	trpět	k5eAaImIp3nS	trpět
dyslexií	dyslexie	k1gFnPc2	dyslexie
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
autorku	autorka	k1gFnSc4	autorka
příběhu	příběh	k1gInSc2	příběh
Kukel	kukla	k1gFnPc2	kukla
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
空	空	k?	空
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
jméno	jméno	k1gNnSc1	jméno
zní	znět	k5eAaImIp3nS	znět
Eriko	Erika	k1gFnSc5	Erika
Fukada	Fukada	k1gFnSc1	Fukada
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
zkrácením	zkrácení	k1gNnSc7	zkrácení
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
krátká	krátký	k2eAgFnSc1d1	krátká
varianta	varianta	k1gFnSc1	varianta
<g/>
.	.	kIx.	.
<g/>
Lídr	lídr	k1gMnSc1	lídr
</s>
</p>
<p>
<s>
Lídr	lídr	k1gMnSc1	lídr
je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
sekty	sekta	k1gFnSc2	sekta
Předvoj	předvoj	k1gInSc1	předvoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slyší	slyšet	k5eAaImIp3nS	slyšet
hlasy	hlas	k1gInPc4	hlas
"	"	kIx"	"
<g/>
Little	Little	k1gFnSc1	Little
People	People	k1gFnSc2	People
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgNnSc1d1	skutečné
občanské	občanský	k2eAgNnSc1d1	občanské
jméno	jméno	k1gNnSc1	jméno
zní	znět	k5eAaImIp3nS	znět
Tamocu	Tamocus	k1gInSc2	Tamocus
Fukada	Fukada	k1gFnSc1	Fukada
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
je	být	k5eAaImIp3nS	být
Fukaeri	Fukaer	k1gFnSc2	Fukaer
<g/>
.	.	kIx.	.
</s>
<s>
Postihla	postihnout	k5eAaPmAgFnS	postihnout
jej	on	k3xPp3gNnSc4	on
záhadná	záhadný	k2eAgFnSc1d1	záhadná
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ataky	atak	k1gInPc4	atak
silných	silný	k2eAgFnPc2d1	silná
bolestí	bolest	k1gFnPc2	bolest
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc4d1	celková
ztuhlost	ztuhlost	k1gFnSc4	ztuhlost
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
důsledku	důsledek	k1gInSc6	důsledek
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
schopnost	schopnost	k1gFnSc4	schopnost
slyšet	slyšet	k5eAaImF	slyšet
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
život	život	k1gInSc4	život
sekty	sekta	k1gFnSc2	sekta
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc7d1	původní
profesí	profes	k1gFnSc7	profes
byl	být	k5eAaImAgMnS	být
vědec	vědec	k1gMnSc1	vědec
<g/>
.	.	kIx.	.
<g/>
Paní	paní	k1gFnSc1	paní
z	z	k7c2	z
Azabu	Azab	k1gInSc2	Azab
</s>
</p>
<p>
<s>
Paní	paní	k1gFnSc1	paní
z	z	k7c2	z
Azabu	Azab	k1gInSc2	Azab
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
老	老	k?	老
<g/>
,	,	kIx,	,
stará	starý	k2eAgFnSc1d1	stará
paní	paní	k1gFnSc1	paní
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zámožná	zámožný	k2eAgFnSc1d1	zámožná
vdova	vdova	k1gFnSc1	vdova
uprostřed	uprostřed	k7c2	uprostřed
osmého	osmý	k4xOgNnSc2	osmý
decenia	decenium	k1gNnSc2	decenium
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgNnSc1d1	skutečné
jméno	jméno	k1gNnSc1	jméno
zní	znět	k5eAaImIp3nS	znět
Šizue	Šizue	k1gFnSc1	Šizue
Ogata	Ogata	k1gFnSc1	Ogata
(	(	kIx(	(
<g/>
Hepburn	Hepburn	k1gInSc1	Hepburn
<g/>
:	:	kIx,	:
Ogata	Ogata	k1gFnSc1	Ogata
Shizue	Shizu	k1gFnSc2	Shizu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
manželově	manželův	k2eAgNnSc6d1	manželovo
úmrtí	úmrtí	k1gNnSc6	úmrtí
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
rodinného	rodinný	k2eAgInSc2d1	rodinný
podniku	podnik	k1gInSc2	podnik
a	a	k8xC	a
nastřádala	nastřádat	k5eAaPmAgFnS	nastřádat
velké	velký	k2eAgNnSc4d1	velké
jmění	jmění	k1gNnSc4	jmění
i	i	k8xC	i
důležité	důležitý	k2eAgInPc4d1	důležitý
kontakty	kontakt	k1gInPc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Vrbové	Vrbové	k2eAgFnSc6d1	Vrbové
vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
rezidenční	rezidenční	k2eAgFnSc6d1	rezidenční
oblasti	oblast	k1gFnSc6	oblast
Azabu	Azab	k1gInSc2	Azab
tokijské	tokijský	k2eAgFnSc2d1	Tokijská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Minato	Minat	k2eAgNnSc1d1	Minato
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
azylový	azylový	k2eAgInSc1d1	azylový
dům	dům	k1gInSc1	dům
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
oběťmi	oběť	k1gFnPc7	oběť
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
důsledku	důsledek	k1gInSc6	důsledek
sama	sám	k3xTgFnSc1	sám
ztratila	ztratit	k5eAaPmAgFnS	ztratit
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Cvičební	cvičební	k2eAgFnSc1d1	cvičební
lekce	lekce	k1gFnSc1	lekce
jí	on	k3xPp3gFnSc3	on
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
Aomame	Aomam	k1gInSc5	Aomam
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
spřátelila	spřátelit	k5eAaPmAgFnS	spřátelit
<g/>
.	.	kIx.	.
</s>
<s>
Fyzioterapeutku	fyzioterapeutka	k1gFnSc4	fyzioterapeutka
najímá	najímat	k5eAaImIp3nS	najímat
na	na	k7c4	na
vraždy	vražda	k1gFnPc4	vražda
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
provinili	provinit	k5eAaPmAgMnP	provinit
domácím	domácí	k2eAgNnSc7d1	domácí
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
<g/>
Tamaru	Tamara	k1gFnSc4	Tamara
</s>
</p>
<p>
<s>
Tamaru	Tamara	k1gFnSc4	Tamara
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
タ	タ	k?	タ
<g/>
,	,	kIx,	,
Hepburn	Hepburn	k1gMnSc1	Hepburn
<g/>
:	:	kIx,	:
Tamaru	Tamara	k1gFnSc4	Tamara
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
<g/>
letý	letý	k2eAgMnSc1d1	letý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
loajální	loajální	k2eAgMnSc1d1	loajální
bodyguard	bodyguard	k1gMnSc1	bodyguard
paní	paní	k1gFnSc2	paní
z	z	k7c2	z
Azabu	Azab	k1gInSc2	Azab
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
profesionála	profesionál	k1gMnSc4	profesionál
se	s	k7c7	s
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
z	z	k7c2	z
jednotky	jednotka	k1gFnSc2	jednotka
japonských	japonský	k2eAgFnPc2d1	japonská
obranných	obranný	k2eAgFnPc2d1	obranná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Otevřeně	otevřeně	k6eAd1	otevřeně
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
homosexuální	homosexuální	k2eAgFnSc3d1	homosexuální
orientaci	orientace	k1gFnSc3	orientace
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
Azabu	Azaba	k1gFnSc4	Azaba
s	s	k7c7	s
mladším	mladý	k2eAgMnSc7d2	mladší
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
plemenem	plemeno	k1gNnSc7	plemeno
psa	pes	k1gMnSc4	pes
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgMnSc1d1	německý
ovčák	ovčák	k1gMnSc1	ovčák
<g/>
.	.	kIx.	.
<g/>
Profesor	profesor	k1gMnSc1	profesor
Ebisuno	Ebisuna	k1gFnSc5	Ebisuna
</s>
</p>
<p>
<s>
Profesor	profesor	k1gMnSc1	profesor
Ebisuno	Ebisuna	k1gFnSc5	Ebisuna
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
戎	戎	k?	戎
<g/>
,	,	kIx,	,
Hepburn	Hepburn	k1gMnSc1	Hepburn
<g/>
:	:	kIx,	:
Professor	Professor	k1gMnSc1	Professor
Ebisuno	Ebisuna	k1gFnSc5	Ebisuna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vědec	vědec	k1gMnSc1	vědec
uprostřed	uprostřed	k7c2	uprostřed
sedmého	sedmý	k4xOgNnSc2	sedmý
decenia	decenium	k1gNnSc2	decenium
věku	věk	k1gInSc2	věk
a	a	k8xC	a
opatrovník	opatrovník	k1gMnSc1	opatrovník
Fukaeri	Fukaer	k1gMnPc1	Fukaer
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
Šinanomači	Šinanomač	k1gInSc6	Šinanomač
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
profesní	profesní	k2eAgFnSc2d1	profesní
kariéry	kariéra	k1gFnSc2	kariéra
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
akademii	akademie	k1gFnSc6	akademie
s	s	k7c7	s
Fukaeřiným	Fukaeřin	k2eAgMnSc7d1	Fukaeřin
otcem	otec	k1gMnSc7	otec
Tamocuem	Tamocu	k1gMnSc7	Tamocu
Fukadou	Fukada	k1gFnSc7	Fukada
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zanechal	zanechat	k5eAaPmAgInS	zanechat
vědecké	vědecký	k2eAgFnPc4d1	vědecká
dráhy	dráha	k1gFnPc4	dráha
a	a	k8xC	a
s	s	k7c7	s
třiceti	třicet	k4xCc7	třicet
studenty	student	k1gMnPc7	student
založil	založit	k5eAaPmAgMnS	založit
sektu	sekt	k1gInSc2	sekt
Předvoj	předvoj	k1gInSc1	předvoj
<g/>
.	.	kIx.	.
</s>
<s>
Desetiletá	desetiletý	k2eAgNnPc1d1	desetileté
Fukaeri	Fukaeri	k1gNnPc1	Fukaeri
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgMnSc4	jeden
dne	den	k1gInSc2	den
objevila	objevit	k5eAaPmAgFnS	objevit
u	u	k7c2	u
jeho	jeho	k3xOp3gFnPc2	jeho
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
okolnosti	okolnost	k1gFnPc4	okolnost
příchodu	příchod	k1gInSc2	příchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgNnPc4	první
vydání	vydání	k1gNnPc4	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republikaSvazek	republikaSvazek	k1gInSc1	republikaSvazek
1	[number]	k4	1
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
1456	[number]	k4	1456
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
768	[number]	k4	768
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Knihy	kniha	k1gFnSc2	kniha
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svazek	svazek	k1gInSc1	svazek
2	[number]	k4	2
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
1514	[number]	k4	1514
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
468	[number]	k4	468
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Knihu	kniha	k1gFnSc4	kniha
3	[number]	k4	3
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
<g/>
JaponskoKniha	JaponskoKniha	k1gFnSc1	JaponskoKniha
1	[number]	k4	1
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
353422	[number]	k4	353422
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
554	[number]	k4	554
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
2	[number]	k4	2
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
353423	[number]	k4	353423
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
501	[number]	k4	501
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
3	[number]	k4	3
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
353425	[number]	k4	353425
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
602	[number]	k4	602
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
<g/>
MaďarskoKniha	MaďarskoKniha	k1gFnSc1	MaďarskoKniha
1	[number]	k4	1
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
963	[number]	k4	963
<g/>
-	-	kIx~	-
<g/>
9973	[number]	k4	9973
<g/>
-	-	kIx~	-
<g/>
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
469	[number]	k4	469
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
2	[number]	k4	2
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
963	[number]	k4	963
<g/>
-	-	kIx~	-
<g/>
9973	[number]	k4	9973
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
510	[number]	k4	510
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
3	[number]	k4	3
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
963	[number]	k4	963
<g/>
-	-	kIx~	-
<g/>
9973	[number]	k4	9973
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
576	[number]	k4	576
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
<g/>
NorskoSvazek	NorskoSvazek	k1gInSc1	NorskoSvazek
1	[number]	k4	1
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
530	[number]	k4	530
<g/>
-	-	kIx~	-
<g/>
3380	[number]	k4	3380
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
751	[number]	k4	751
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Knihy	kniha	k1gFnSc2	kniha
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svazek	svazek	k1gInSc1	svazek
2	[number]	k4	2
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
530	[number]	k4	530
<g/>
-	-	kIx~	-
<g/>
3483	[number]	k4	3483
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
432	[number]	k4	432
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Knihu	kniha	k1gFnSc4	kniha
3	[number]	k4	3
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
<g/>
ŘeckoKniha	ŘeckoKniha	k1gFnSc1	ŘeckoKniha
1	[number]	k4	1
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
960	[number]	k4	960
<g/>
-	-	kIx~	-
<g/>
496	[number]	k4	496
<g/>
-	-	kIx~	-
<g/>
654	[number]	k4	654
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
480	[number]	k4	480
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
2	[number]	k4	2
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
960	[number]	k4	960
<g/>
-	-	kIx~	-
<g/>
496	[number]	k4	496
<g/>
-	-	kIx~	-
<g/>
833	[number]	k4	833
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
416	[number]	k4	416
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
3	[number]	k4	3
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
618	[number]	k4	618
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
101	[number]	k4	101
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
512	[number]	k4	512
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
královstvíSvazek	královstvíSvazek	k1gInSc1	královstvíSvazek
1	[number]	k4	1
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84655	[number]	k4	84655
<g/>
-	-	kIx~	-
<g/>
407	[number]	k4	407
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
624	[number]	k4	624
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Knihy	kniha	k1gFnSc2	kniha
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Jay	Jay	k1gMnSc1	Jay
Rubin	Rubin	k1gMnSc1	Rubin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svazek	svazek	k1gInSc1	svazek
2	[number]	k4	2
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84655	[number]	k4	84655
<g/>
-	-	kIx~	-
<g/>
405	[number]	k4	405
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
368	[number]	k4	368
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Knihu	kniha	k1gFnSc4	kniha
3	[number]	k4	3
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Philip	Philip	k1gMnSc1	Philip
Gabriel	Gabriel	k1gMnSc1	Gabriel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
svazek	svazek	k1gInSc1	svazek
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
957807	[number]	k4	957807
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
1	[number]	k4	1
342	[number]	k4	342
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
<g/>
Spojené	spojený	k2eAgFnSc2d1	spojená
státyJediný	státyJediný	k2eAgInSc4d1	státyJediný
svazek	svazek	k1gInSc4	svazek
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
307	[number]	k4	307
<g/>
-	-	kIx~	-
<g/>
59331	[number]	k4	59331
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
928	[number]	k4	928
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
<g/>
TureckoJediný	tureckojediný	k2eAgInSc1d1	tureckojediný
svazek	svazek	k1gInSc1	svazek
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
605	[number]	k4	605
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
638	[number]	k4	638
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
1	[number]	k4	1
256	[number]	k4	256
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citát	citát	k1gInSc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jurkovič	Jurkovič	k1gMnSc1	Jurkovič
<g/>
,	,	kIx,	,
doslov	doslov	k1gInSc1	doslov
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MURAKAMI	MURAKAMI	kA	MURAKAMI
<g/>
,	,	kIx,	,
Haruki	Haruki	k1gNnSc1	Haruki
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
Q	Q	kA	Q
<g/>
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
768	[number]	k4	768
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
1456	[number]	k4	1456
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MURAKAMI	MURAKAMI	kA	MURAKAMI
<g/>
,	,	kIx,	,
Haruki	Haruki	k1gNnSc1	Haruki
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
Q	Q	kA	Q
<g/>
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
468	[number]	k4	468
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
1514	[number]	k4	1514
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
NOVÁKOVÁ	Nováková	k1gFnSc1	Nováková
<g/>
,	,	kIx,	,
Šárka	Šárka	k1gFnSc1	Šárka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
Q	Q	kA	Q
<g/>
84	[number]	k4	84
<g/>
:	:	kIx,	:
Vábivý	vábivý	k2eAgInSc4d1	vábivý
skluz	skluz	k1gInSc4	skluz
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kulturissimo	Kulturissimo	k6eAd1	Kulturissimo
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BAXTER	BAXTER	kA	BAXTER
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Behind	Behind	k1gMnSc1	Behind
Murakami	Muraka	k1gFnPc7	Muraka
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Mirror	Mirrora	k1gFnPc2	Mirrora
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Review	Review	k1gFnSc1	Review
of	of	k?	of
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
