<s>
Mensa	mensa	k1gFnSc1
Select	Selecta	k1gFnPc2
</s>
<s>
Mensa	mensa	k1gFnSc1
Select	Selecta	k1gFnPc2
je	být	k5eAaImIp3nS
každoroční	každoroční	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
Americké	americký	k2eAgFnSc2d1
Mensy	mensa	k1gFnSc2
(	(	kIx(
<g/>
národní	národní	k2eAgFnSc1d1
pobočka	pobočka	k1gFnSc1
Mensy	mensa	k1gFnSc2
International	International	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
udíleno	udílen	k2eAgNnSc4d1
vždy	vždy	k6eAd1
pětici	pětice	k1gFnSc4
stolních	stolní	k2eAgFnPc2d1
(	(	kIx(
<g/>
deskových	deskový	k2eAgFnPc2d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
karetních	karetní	k2eAgFnPc2d1
<g/>
)	)	kIx)
her	hra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
„	„	k?
<g/>
jsou	být	k5eAaImIp3nP
originální	originální	k2eAgFnPc1d1
<g/>
,	,	kIx,
stimulující	stimulující	k2eAgFnPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
pěkné	pěkný	k2eAgNnSc4d1
provedení	provedení	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocenění	ocenění	k1gNnSc1
je	být	k5eAaImIp3nS
vyhlašováno	vyhlašovat	k5eAaImNgNnS
během	běh	k1gInSc7
Mensa	mensa	k1gFnSc1
Mind	Mind	k1gMnSc1
Games	Games	k1gMnSc1
–	–	k?
výročního	výroční	k2eAgInSc2d1
mensovního	mensovní	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
v	v	k7c6
deskových	deskový	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
(	(	kIx(
<g/>
místo	místo	k7c2
konání	konání	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
závorce	závorka	k1gFnSc6
za	za	k7c7
rokem	rok	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
oceněných	oceněný	k2eAgFnPc2d1
her	hra	k1gFnPc2
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Taboo	Taboo	k6eAd1
</s>
<s>
Scattergories	Scattergories	k1gMnSc1
</s>
<s>
Tribond	Tribond	k1gMnSc1
</s>
<s>
Abalone	Abalon	k1gInSc5
</s>
<s>
Trivial	Trivial	k1gMnSc1
Pursuit	Pursuit	k1gMnSc1
<g/>
:	:	kIx,
Genus	Genus	k1gInSc1
Edition	Edition	k1gInSc1
</s>
<s>
1991	#num#	k4
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Pyramis	Pyramis	k1gFnSc1
</s>
<s>
Lapis	lapis	k1gInSc1
</s>
<s>
Set	set	k1gInSc1
</s>
<s>
Master	master	k1gMnSc1
Labyrinth	Labyrinth	k1gMnSc1
</s>
<s>
Clue	Clue	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Great	Great	k1gInSc1
Museum	museum	k1gNnSc1
Caper	Caper	k1gInSc1
</s>
<s>
1992	#num#	k4
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kinesis	Kinesis	k1gFnSc1
</s>
<s>
Q4	Q4	k4
</s>
<s>
Terrace	Terrace	k1gFnSc1
</s>
<s>
Traverse	travers	k1gInSc5
</s>
<s>
Why	Why	k?
Not	nota	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s>
1993	#num#	k4
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Farook	Farook	k1gInSc1
</s>
<s>
Inklings	Inklings	k6eAd1
</s>
<s>
Overturn	Overturn	k1gMnSc1
</s>
<s>
Quadrature	Quadratur	k1gMnSc5
</s>
<s>
Quarto	Quarta	k1gFnSc5
</s>
<s>
1994	#num#	k4
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Char	Char	k1gMnSc1
</s>
<s>
Chung	Chung	k1gMnSc1
Toi	Toi	k1gMnSc1
</s>
<s>
Down	Down	k1gMnSc1
Fall	Fall	k1gMnSc1
</s>
<s>
Magic	Magic	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Gathering	Gathering	k1gInSc1
</s>
<s>
Pyraos	Pyraos	k1gMnSc1
</s>
<s>
1995	#num#	k4
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Wordspin	Wordspin	k1gMnSc1
Scramble	Scramble	k1gMnSc1
</s>
<s>
Continuo	Continuo	k6eAd1
</s>
<s>
Duo	duo	k1gNnSc1
</s>
<s>
Quixo	Quixo	k6eAd1
</s>
<s>
The	The	k?
Great	Great	k1gInSc1
Dalmuti	Dalmuť	k1gFnSc2
</s>
<s>
1996	#num#	k4
(	(	kIx(
<g/>
Atlanta	Atlanta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Rat-a-Tat	Rat-a-Tat	k2eAgMnSc1d1
Cat	Cat	k1gMnSc1
</s>
<s>
Pirateer	Pirateer	k1gMnSc1
</s>
<s>
Take	Take	k6eAd1
6	#num#	k4
</s>
<s>
Quadwrangle	Quadwrangle	k6eAd1
</s>
<s>
Touche	Touche	k6eAd1
</s>
<s>
1997	#num#	k4
(	(	kIx(
<g/>
Chicago	Chicago	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Hattrick	hattrick	k1gInSc1
</s>
<s>
Rush	Rush	k1gMnSc1
Hour	Hour	k1gMnSc1
</s>
<s>
Quoridor	Quoridor	k1gMnSc1
</s>
<s>
Sagarian	Sagarian	k1gMnSc1
</s>
<s>
Stops	Stops	k6eAd1
</s>
<s>
1998	#num#	k4
(	(	kIx(
<g/>
Phoenix	Phoenix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Avalam	Avalam	k6eAd1
</s>
<s>
Cube	Cube	k1gFnSc1
Checkers	Checkersa	k1gFnPc2
</s>
<s>
Kram	Kram	k6eAd1
</s>
<s>
Spy	Spy	k?
Alley	Alley	k1gInPc1
</s>
<s>
Wadjet	Wadjet	k1gMnSc1
</s>
<s>
1999	#num#	k4
(	(	kIx(
<g/>
Seattle	Seattle	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Apples	Apples	k1gMnSc1
to	ten	k3xDgNnSc4
Apples	Apples	k1gMnSc1
</s>
<s>
Bollox	Bollox	k1gInSc1
(	(	kIx(
<g/>
jinak	jinak	k6eAd1
též	též	k9
Bō	Bō	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Doubles	Doubles	k1gMnSc1
Wild	Wild	k1gMnSc1
</s>
<s>
Fluxx	Fluxx	k1gInSc1
</s>
<s>
Quiddler	Quiddler	k1gMnSc1
</s>
<s>
2000	#num#	k4
(	(	kIx(
<g/>
Atlanta	Atlanta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
Stones	Stones	k1gInSc1
</s>
<s>
Finish	Finish	k1gMnSc1
Lines	Lines	k1gMnSc1
</s>
<s>
Imaginiff	Imaginiff	k1gMnSc1
<g/>
...	...	k?
</s>
<s>
Time	Time	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Up	Up	k1gFnSc7
<g/>
!	!	kIx.
</s>
<s>
ZÈ	ZÈ	k?
(	(	kIx(
<g/>
GIPF	GIPF	kA
projekt	projekt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
(	(	kIx(
<g/>
Medina	Medina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Brainstrain	Brainstrain	k1gMnSc1
</s>
<s>
Dao	Dao	k?
</s>
<s>
Metro	metro	k1gNnSc1
</s>
<s>
Shapes	Shapes	k1gMnSc1
Up	Up	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s>
Thepollgame	Thepollgam	k1gInSc5
</s>
<s>
2002	#num#	k4
(	(	kIx(
<g/>
Minneapolis	Minneapolis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Curses	Curses	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s>
DVONN	DVONN	kA
(	(	kIx(
<g/>
GIPF	GIPF	kA
projekt	projekt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Muggins	Muggins	k6eAd1
</s>
<s>
Smart	Smart	k1gInSc1
Mouth	Moutha	k1gFnPc2
</s>
<s>
The	The	k?
Legend	legenda	k1gFnPc2
of	of	k?
Landlock	Landlock	k1gMnSc1
</s>
<s>
2003	#num#	k4
(	(	kIx(
<g/>
Houston	Houston	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Blokus	Blokus	k1gMnSc1
</s>
<s>
Cityscape	Cityscapat	k5eAaPmIp3nS
</s>
<s>
Fire	Fire	k1gFnSc1
and	and	k?
Ice	Ice	k1gFnSc1
</s>
<s>
Octiles	Octiles	k1gMnSc1
</s>
<s>
TransAmerica	TransAmerica	k6eAd1
</s>
<s>
2004	#num#	k4
(	(	kIx(
<g/>
Chicago	Chicago	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
Days	Days	k1gInSc1
in	in	k?
Africa	Africa	k1gFnSc1
</s>
<s>
Basari	Basari	k6eAd1
</s>
<s>
The	The	k?
Bridges	Bridges	k1gMnSc1
of	of	k?
Shangri-La	Shangri-La	k1gMnSc1
</s>
<s>
Rumis	Rumis	k1gFnSc1
</s>
<s>
YINSH	YINSH	kA
(	(	kIx(
<g/>
GIPF	GIPF	kA
projekt	projekt	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
(	(	kIx(
<g/>
Tampa	Tampa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Da	Da	k?
Vinci	Vinca	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Challenge	Challenge	k1gFnSc7
</s>
<s>
Ingenious	Ingenious	k1gMnSc1
</s>
<s>
Loot	Loot	k1gMnSc1
</s>
<s>
Niagara	Niagara	k1gFnSc1
</s>
<s>
Zendo	Zendo	k1gNnSc1
</s>
<s>
2006	#num#	k4
(	(	kIx(
<g/>
Portland	Portland	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Deflexion	Deflexion	k1gInSc1
</s>
<s>
Hive	Hive	k6eAd1
</s>
<s>
Keesdrow	Keesdrow	k?
</s>
<s>
Pentago	Pentago	k6eAd1
</s>
<s>
Wits	Wits	k1gInSc1
and	and	k?
Wagers	Wagers	k1gInSc1
</s>
<s>
2007	#num#	k4
(	(	kIx(
<g/>
Pittsburgh	Pittsburgh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Gemlok	Gemlok	k1gInSc1
</s>
<s>
Gheos	Gheos	k1gMnSc1
</s>
<s>
Hit	hit	k1gInSc1
or	or	k?
Miss	miss	k1gFnSc1
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
stejnojmenná	stejnojmenný	k2eAgFnSc1d1
karetní	karetní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
pro	pro	k7c4
jednoho	jeden	k4xCgMnSc4
<g/>
)	)	kIx)
</s>
<s>
Qwirkle	Qwirkle	k6eAd1
(	(	kIx(
<g/>
MindWare	MindWar	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Skullduggery	Skullduggera	k1gFnPc1
</s>
<s>
2008	#num#	k4
(	(	kIx(
<g/>
Phoenix	Phoenix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
AmuseAmaze	AmuseAmaha	k1gFnSc3
(	(	kIx(
<g/>
HL	HL	kA
Games	Games	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Eye	Eye	k?
Know	Know	k1gFnSc1
(	(	kIx(
<g/>
Wiggles	Wiggles	k1gInSc1
3D	3D	k4
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Jumbulaya	Jumbulaya	k1gMnSc1
(	(	kIx(
<g/>
Platypus	Platypus	k1gMnSc1
Games	Games	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Pixel	pixel	k1gInSc1
(	(	kIx(
<g/>
Educational	Educational	k1gFnSc1
Insights	Insightsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Tiki	Tiki	k1gNnSc1
Topple	Topple	k1gMnSc1
(	(	kIx(
<g/>
Gamewright	Gamewright	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
(	(	kIx(
<g/>
Cincinnati	Cincinnati	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Cornerstone	Cornerston	k1gInSc5
(	(	kIx(
<g/>
Good	Good	k1gMnSc1
Company	Compana	k1gFnSc2
Games	Games	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Dominion	dominion	k1gNnSc1
</s>
<s>
Marrakech	Marrakech	k1gInSc1
(	(	kIx(
<g/>
Gigamic	Gigamic	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Stratum	Stratum	k1gNnSc1
</s>
<s>
Tic-Tac-Ku	Tic-Tac-Ku	k5eAaPmIp1nS
(	(	kIx(
<g/>
Mad	Mad	k1gMnSc1
Cave	Cav	k1gFnSc2
Bird	Bird	k1gMnSc1
Games	Games	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
(	(	kIx(
<g/>
San	San	k1gMnSc1
Diego	Diego	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Anomia	Anomia	k1gFnSc1
</s>
<s>
Dizios	Dizios	k1gInSc4
(	(	kIx(
<g/>
MindWare	MindWar	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Forbidden	Forbiddna	k1gFnPc2
Island	Island	k1gInSc1
</s>
<s>
Word	Word	kA
on	on	k3xPp3gMnSc1
the	the	k?
Street	Street	k1gInSc1
</s>
<s>
Yikerz	Yikerz	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Wiggles	Wigglesa	k1gFnPc2
3D	3D	k4
Inc	Inc	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2011	#num#	k4
(	(	kIx(
<g/>
Albany	Albana	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
InStructures	InStructures	k1gMnSc1
</s>
<s>
Pastiche	Pastiche	k6eAd1
</s>
<s>
Pirate	Pirat	k1gMnSc5
versus	versus	k7c1
Pirate	Pirat	k1gMnSc5
</s>
<s>
Stomple	Stomple	k6eAd1
</s>
<s>
Uncle	Uncle	k6eAd1
Chestnut	Chestnut	k2eAgInSc1d1
<g/>
’	’	k?
<g/>
s	s	k7c7
Table	tablo	k1gNnSc6
Gype	Gyp	k1gFnSc2
</s>
<s>
2012	#num#	k4
(	(	kIx(
<g/>
Herndon	Herndon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Coerceo	Coerceo	k6eAd1
</s>
<s>
IOTA	IOTA	kA
</s>
<s>
Mind	Mind	k1gMnSc1
Shift	Shift	kA
</s>
<s>
Snake	Snake	k1gFnSc1
Oil	Oil	k1gFnSc2
</s>
<s>
Tetris	Tetris	k1gFnSc1
Link	Linka	k1gFnPc2
</s>
<s>
2013	#num#	k4
(	(	kIx(
<g/>
St.	st.	kA
Louis	Louis	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Forbidden	Forbiddna	k1gFnPc2
Desert	desert	k1gInSc4
</s>
<s>
Ghooost	Ghooost	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s>
KerFlip	KerFlip	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s>
Kulami	kula	k1gFnPc7
(	(	kIx(
<g/>
Foxmind	Foxmind	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Suburbia	Suburbia	k1gFnSc1
(	(	kIx(
<g/>
Bezier	Bezier	k1gMnSc1
Games	Games	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
(	(	kIx(
<g/>
Austin	Austin	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Gravwell	Gravwell	k1gInSc4
<g/>
:	:	kIx,
Escape	Escap	k1gMnSc5
from	from	k1gInSc4
the	the	k?
9	#num#	k4
<g/>
th	th	k?
Dimension	Dimension	k1gInSc1
</s>
<s>
Qwixx	Qwixx	k1gInSc1
</s>
<s>
Pyramix	Pyramix	k1gInSc1
</s>
<s>
The	The	k?
Duke	Duke	k1gInSc1
</s>
<s>
Euphoria	Euphorium	k1gNnPc1
<g/>
:	:	kIx,
Build	Build	k1gInSc1
a	a	k8xC
Better	Better	k1gInSc1
Dystopia	Dystopium	k1gNnSc2
</s>
<s>
2015	#num#	k4
(	(	kIx(
<g/>
San	San	k1gMnSc1
Diego	Diego	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Castles	Castles	k1gMnSc1
of	of	k?
Mad	Mad	k1gMnSc1
King	King	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
</s>
<s>
Dragonwood	Dragonwood	k1gInSc1
</s>
<s>
Lanterns	Lanterns	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Harvest	Harvest	k1gMnSc1
Festival	festival	k1gInSc1
</s>
<s>
Letter	Letter	k1gMnSc1
Tycoon	Tycoon	k1gMnSc1
</s>
<s>
Trekking	Trekking	k1gInSc1
the	the	k?
National	National	k1gFnSc2
Parks	Parksa	k1gFnPc2
</s>
<s>
2016	#num#	k4
(	(	kIx(
<g/>
Wheeling	Wheeling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Circular	Circular	k1gInSc1
Reasoning	Reasoning	k1gInSc1
</s>
<s>
Favor	Favor	k?
of	of	k?
the	the	k?
Pharaoh	Pharaoh	k1gInSc1
</s>
<s>
The	The	k?
Last	Last	k2eAgInSc4d1
Spike	Spike	k1gInSc4
</s>
<s>
New	New	k?
York	York	k1gInSc1
1901	#num#	k4
</s>
<s>
World	World	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Fair	fair	k6eAd1
1893	#num#	k4
</s>
<s>
2017	#num#	k4
(	(	kIx(
<g/>
Herndon	Herndon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
bude	být	k5eAaImBp3nS
oznámeno	oznámen	k2eAgNnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
List	list	k1gInSc1
of	of	k?
Mensa	mensa	k1gFnSc1
Select	Select	k1gMnSc1
recipients	recipients	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stránka	stránka	k1gFnSc1
Mensa	mensa	k1gFnSc1
Select	Select	k1gInSc4
na	na	k7c6
webu	web	k1gInSc6
Americké	americký	k2eAgFnSc2d1
Mensy	mensa	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Stránka	stránka	k1gFnSc1
Mind	Minda	k1gFnPc2
Games	Gamesa	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
