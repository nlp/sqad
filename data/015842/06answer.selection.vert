<s>
Mensa	mensa	k1gFnSc1
Select	Selecta	k1gFnPc2
je	být	k5eAaImIp3nS
každoroční	každoroční	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
Americké	americký	k2eAgFnSc2d1
Mensy	mensa	k1gFnSc2
(	(	kIx(
<g/>
národní	národní	k2eAgFnSc1d1
pobočka	pobočka	k1gFnSc1
Mensy	mensa	k1gFnSc2
International	International	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
udíleno	udílen	k2eAgNnSc4d1
vždy	vždy	k6eAd1
pětici	pětice	k1gFnSc4
stolních	stolní	k2eAgFnPc2d1
(	(	kIx(
<g/>
deskových	deskový	k2eAgFnPc2d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
karetních	karetní	k2eAgFnPc2d1
<g/>
)	)	kIx)
her	hra	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
„	„	k?
<g/>
jsou	být	k5eAaImIp3nP
originální	originální	k2eAgFnPc1d1
<g/>
,	,	kIx,
stimulující	stimulující	k2eAgFnPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
pěkné	pěkný	k2eAgNnSc4d1
provedení	provedení	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>