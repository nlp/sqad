<s>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
</s>
<s>
Mount	Mount	k1gMnSc1
Everestस	Everestस	k1gMnSc1
<g/>
ा	ा	k?
<g/>
थ	थ	k?
<g/>
ा	ा	k?
<g/>
ཇ	ཇ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
མ	མ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
ག	ག	k?
<g/>
ླ	ླ	k?
<g/>
ང	ང	k?
<g/>
་	་	k?
<g/>
མ	མ	k?
Severní	severní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
Mount	Mounta	k1gFnPc2
Everestu	Everest	k1gInSc2
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
z	z	k7c2
Tibetu	Tibet	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
8849	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
8	#num#	k4
848,86	848,86	k4
m	m	kA
Seznamy	seznam	k1gInPc4
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3
hory	hora	k1gFnPc1
#	#	kIx~
<g/>
1	#num#	k4
<g/>
Nejprominentnější	prominentní	k2eAgFnPc1d3
hory	hora	k1gFnPc1
#	#	kIx~
<g/>
1	#num#	k4
<g/>
Nejizolovanější	izolovaný	k2eAgFnPc1d3
hory	hora	k1gFnPc1
#	#	kIx~
<g/>
1	#num#	k4
<g/>
Osmitisícovky	Osmitisícovka	k1gFnPc1
#	#	kIx~
<g/>
1	#num#	k4
<g/>
Koruna	koruna	k1gFnSc1
planety	planeta	k1gFnSc2
#	#	kIx~
<g/>
1	#num#	k4
<g/>
Nejvyšší	vysoký	k2eAgFnPc1d3
hory	hora	k1gFnPc1
asijských	asijský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
#	#	kIx~
<g/>
1	#num#	k4
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc4
</s>
<s>
Asie	Asie	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Nepál	Nepál	k1gInSc1
Nepál	Nepál	k1gInSc1
</s>
<s>
Čína	Čína	k1gFnSc1
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Tibetská	tibetský	k2eAgFnSc1d1
autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Himálaj	Himálaj	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
27	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
86	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
40	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1953	#num#	k4
Povodí	povodí	k1gNnSc2
</s>
<s>
Kósí	Kósí	k1gFnSc1
(	(	kIx(
<g/>
Ganga	Ganga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
(	(	kIx(
<g/>
tibetsky	tibetsky	k6eAd1
ཇ	ཇ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
མ	མ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
ག	ག	k?
<g/>
ླ	ླ	k?
<g/>
ང	ང	k?
<g/>
་	་	k?
<g/>
མ	མ	k?
<g/>
,	,	kIx,
Džomolangma	Džomolangma	k1gFnSc1
<g/>
;	;	kIx,
nepálsky	nepálsky	k6eAd1
स	स	k?
<g/>
ा	ा	k?
<g/>
थ	थ	k?
<g/>
ा	ा	k?
<g/>
,	,	kIx,
Sagarmátha	Sagarmátha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
8848,86	8848,86	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
podle	podle	k7c2
starších	starý	k2eAgInPc2d2
údajů	údaj	k1gInPc2
i	i	k9
8850	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc4
vznikl	vzniknout	k5eAaPmAgInS
spolu	spolu	k6eAd1
se	s	k7c7
zbytkem	zbytek	k1gInSc7
Himálaje	Himálaj	k1gFnSc2
kolizí	kolize	k1gFnPc2
indické	indický	k2eAgFnSc2d1
a	a	k8xC
eurasijské	eurasijský	k2eAgFnSc2d1
kontinentální	kontinentální	k2eAgFnSc2d1
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hora	hora	k1gFnSc1
je	být	k5eAaImIp3nS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
britském	britský	k2eAgInSc6d1
geodetovi	geodet	k1gMnSc3
George	George	k1gNnSc2
Everestovi	Everesta	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Pohoří	pohoří	k1gNnSc1
Himálají	Himálaj	k1gFnPc2
stále	stále	k6eAd1
roste	růst	k5eAaImIp3nS
díky	díky	k7c3
neustálému	neustálý	k2eAgNnSc3d1
tlačení	tlačení	k1gNnSc3
Indické	indický	k2eAgFnSc2d1
desky	deska	k1gFnSc2
na	na	k7c4
Eurasijskou	Eurasijský	k2eAgFnSc4d1
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
čehož	což	k3yQnSc2,k3yRnSc2
se	se	k3xPyFc4
zvětšuje	zvětšovat	k5eAaImIp3nS
i	i	k9
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
Mount	Mounta	k1gFnPc2
Everestu	Everest	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
se	se	k3xPyFc4
tyčí	tyčit	k5eAaImIp3nS
v	v	k7c6
Mahálangurském	Mahálangurský	k2eAgInSc6d1
Himálaji	Himálaj	k1gFnSc6
v	v	k7c6
nepálském	nepálský	k2eAgInSc6d1
regionu	region	k1gInSc6
Khumbu	Khumb	k1gInSc2
na	na	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Čínou	Čína	k1gFnSc7
(	(	kIx(
<g/>
s	s	k7c7
Tibetskou	tibetský	k2eAgFnSc7d1
autonomní	autonomní	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
<g/>
;	;	kIx,
západní	západní	k2eAgMnSc1d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
hranici	hranice	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nepálské	nepálský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Sagarmatha	Sagarmath	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
zařazen	zařadit	k5eAaPmNgMnS
do	do	k7c2
seznamu	seznam	k1gInSc2
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Qomolangma	Qomolangma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
o	o	k7c6
rozloze	rozloha	k1gFnSc6
1	#num#	k4
823	#num#	k4
591	#num#	k4
ha	ha	kA
na	na	k7c6
čínském	čínský	k2eAgNnSc6d1
území	území	k1gNnSc6
byla	být	k5eAaImAgNnP
zapsána	zapsat	k5eAaPmNgNnP
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
na	na	k7c4
seznam	seznam	k1gInSc4
biosférických	biosférický	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Edmund	Edmund	k1gMnSc1
Hillary	Hillara	k1gFnSc2
a	a	k8xC
Tenzing	Tenzing	k1gInSc4
Norgay	Norgaa	k1gFnSc2
uskutečnili	uskutečnit	k5eAaPmAgMnP
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1953	#num#	k4
prvovýstup	prvovýstup	k1gInSc1
na	na	k7c4
horu	hora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mount	Mounta	k1gFnPc2
Everest	Everest	k1gInSc4
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
přitahuje	přitahovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
profesionálních	profesionální	k2eAgMnPc2d1
horolezců	horolezec	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
fyzicky	fyzicky	k6eAd1
zdatných	zdatný	k2eAgMnPc2d1
lezců	lezec	k1gMnPc2
a	a	k8xC
klientů	klient	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstup	výstup	k1gInSc1
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
cestou	cesta	k1gFnSc7
z	z	k7c2
Nepálu	Nepál	k1gInSc2
není	být	k5eNaImIp3nS
technicky	technicky	k6eAd1
příliš	příliš	k6eAd1
obtížný	obtížný	k2eAgInSc1d1
<g/>
,	,	kIx,
nebezpečím	bezpečit	k5eNaImIp1nS
však	však	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
akutní	akutní	k2eAgFnSc1d1
horská	horský	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
<g/>
,	,	kIx,
výkyvy	výkyv	k1gInPc1
počasí	počasí	k1gNnSc2
a	a	k8xC
vítr	vítr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jména	jméno	k1gNnPc1
hory	hora	k1gFnSc2
</s>
<s>
V	v	k7c6
Nepálu	Nepál	k1gInSc6
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
nazývána	nazýván	k2eAgFnSc1d1
Sagarmátha	Sagarmátha	k1gFnSc1
(	(	kIx(
<g/>
स	स	k?
<g/>
ा	ा	k?
<g/>
थ	थ	k?
<g/>
ा	ा	k?
<g/>
,	,	kIx,
Sagaramā	Sagaramā	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
nepálštiny	nepálština	k1gFnSc2
převzato	převzít	k5eAaPmNgNnS
ze	z	k7c2
sanskrtu	sanskrt	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
Tvář	tvář	k1gFnSc1
nebes	nebesa	k1gNnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tibetský	tibetský	k2eAgInSc4d1
název	název	k1gInSc4
zní	znět	k5eAaImIp3nS
Qomolangma	Qomolangma	k1gFnSc1
(	(	kIx(
<g/>
ཇ	ཇ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
མ	མ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
ག	ག	k?
<g/>
ླ	ླ	k?
<g/>
ང	ང	k?
<g/>
་	་	k?
<g/>
མ	མ	k?
<g/>
,	,	kIx,
wylie	wylie	k1gFnPc4
Jo	jo	k9
mo	mo	k?
glang	glang	k1gInSc1
ma	ma	k?
<g/>
,	,	kIx,
Džo-mo-lang-ma	Džo-mo-lang-ma	k1gFnSc1
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
Matka	matka	k1gFnSc1
světa	svět	k1gInSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkomolením	zkomolení	k1gNnSc7
tibetského	tibetský	k2eAgInSc2d1
názvu	název	k1gInSc2
vzniklo	vzniknout	k5eAaPmAgNnS
i	i	k9
čínské	čínský	k2eAgNnSc1d1
珠	珠	k?
(	(	kIx(
<g/>
pinyin	pinyin	k1gMnSc1
<g/>
:	:	kIx,
Zhū	Zhū	k1gMnSc1
Fē	Fē	k1gMnSc1
<g/>
,	,	kIx,
českým	český	k2eAgInSc7d1
přepisem	přepis	k1gInSc7
Ču-mu-lang-ma	Ču-mu-lang-ma	k1gFnSc1
feng	feng	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českým	český	k2eAgInSc7d1
standardizovaným	standardizovaný	k2eAgInSc7d1
exonymem	exonym	k1gInSc7
je	být	k5eAaImIp3nS
Everest	Everest	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
i	i	k9
jinde	jinde	k6eAd1
ve	v	k7c6
světě	svět	k1gInSc6
zdomácnělo	zdomácnět	k5eAaPmAgNnS
jméno	jméno	k1gNnSc1
Everest	Everest	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
hoře	hoře	k1gNnSc4
dal	dát	k5eAaPmAgMnS
Brit	Brit	k1gMnSc1
sir	sir	k1gMnSc1
Andrew	Andrew	k1gMnSc1
Scott	Scott	k1gMnSc1
Waugh	Waugh	k1gMnSc1
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgMnSc1d1
zeměměřič	zeměměřič	k1gMnSc1
pro	pro	k7c4
Indii	Indie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sir	sir	k1gMnSc1
George	Georg	k1gInSc2
Everest	Everest	k1gInSc1
byl	být	k5eAaImAgInS
předchůdcem	předchůdce	k1gMnSc7
sira	sir	k1gMnSc2
Andrewa	Andrewus	k1gMnSc2
Scotta	Scott	k1gMnSc2
Waugha	Waugh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
lze	lze	k6eAd1
slyšet	slyšet	k5eAaImF
nesprávnou	správný	k2eNgFnSc4d1
výslovnost	výslovnost	k1gFnSc4
[	[	kIx(
<g/>
ivrist	ivrist	k1gInSc4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Publikace	publikace	k1gFnSc1
britského	britský	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
BBC	BBC	kA
Pronouncing	Pronouncing	k1gInSc4
Dictionary	Dictionara	k1gFnSc2
of	of	k?
British	British	k1gMnSc1
Names	Names	k1gMnSc1
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
dva	dva	k4xCgInPc4
způsoby	způsob	k1gInPc4
výslovnosti	výslovnost	k1gFnSc2
[	[	kIx(
<g/>
everest	everest	k1gInSc1
<g/>
]	]	kIx)
a	a	k8xC
[	[	kIx(
<g/>
everist	everist	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
správná	správný	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
[	[	kIx(
<g/>
maunt	maunt	k1gMnSc1
everest	everest	k1gMnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
méně	málo	k6eAd2
často	často	k6eAd1
[	[	kIx(
<g/>
maunt	maunt	k1gMnSc1
everist	everist	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horolezci	horolezec	k1gMnPc7
hovoří	hovořit	k5eAaImIp3nS
vždy	vždy	k6eAd1
o	o	k7c6
[	[	kIx(
<g/>
everestu	everest	k1gInSc6
<g/>
]	]	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
R.	R.	kA
Messnera	Messner	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zdolal	zdolat	k5eAaPmAgInS
jako	jako	k8xS,k8xC
první	první	k4xOgFnPc1
všechny	všechen	k3xTgFnPc1
osmitisícovky	osmitisícovka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
deník	deník	k1gInSc1
People	People	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Daily	Dail	k1gMnPc7
publikoval	publikovat	k5eAaBmAgMnS
roku	rok	k1gInSc2
2002	#num#	k4
článek	článek	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
ohrazuje	ohrazovat	k5eAaImIp3nS
proti	proti	k7c3
přetrvávajícímu	přetrvávající	k2eAgNnSc3d1
používání	používání	k1gNnSc3
anglického	anglický	k2eAgInSc2d1
názvu	název	k1gInSc2
v	v	k7c6
západním	západní	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
doporučuje	doporučovat	k5eAaImIp3nS
používání	používání	k1gNnSc4
tibetského	tibetský	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Měření	měření	k1gNnSc1
hory	hora	k1gFnSc2
</s>
<s>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Nuptse	Nuptse	k1gFnSc1
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
z	z	k7c2
Kala	kalo	k1gNnSc2
Pataru	Patar	k1gInSc2
</s>
<s>
Udávaná	udávaný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
8848	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
je	být	k5eAaImIp3nS
oficiálně	oficiálně	k6eAd1
uznaná	uznaný	k2eAgNnPc4d1
Nepálem	Nepál	k1gInSc7
a	a	k8xC
Čínou	Čína	k1gFnSc7
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radhanath	Radhanath	k1gMnSc1
Sikdar	Sikdar	k1gMnSc1
<g/>
,	,	kIx,
indický	indický	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
a	a	k8xC
zeměměřič	zeměměřič	k1gMnSc1
z	z	k7c2
Bengálska	Bengálsko	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1852	#num#	k4
prvním	první	k4xOgMnSc6
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
určil	určit	k5eAaPmAgInS
Everest	Everest	k1gInSc4
jako	jako	k8xC,k8xS
nejvyšší	vysoký	k2eAgFnSc4d3
horu	hora	k1gFnSc4
světa	svět	k1gInSc2
pomocí	pomocí	k7c2
trigonometrických	trigonometrický	k2eAgInPc2d1
výpočtů	výpočet	k1gInPc2
na	na	k7c6
základě	základ	k1gInSc6
měření	měření	k1gNnSc2
teodolitem	teodolit	k1gInSc7
z	z	k7c2
240	#num#	k4
km	km	kA
vzdálené	vzdálený	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
průzkumným	průzkumný	k2eAgNnSc7d1
měřením	měření	k1gNnSc7
byl	být	k5eAaImAgInS
vrchol	vrchol	k1gInSc1
zeměměřiči	zeměměřič	k1gMnPc1
nazýván	nazývat	k5eAaImNgInS
jménem	jméno	k1gNnSc7
Peak	Peako	k1gNnPc2
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
indičtí	indický	k2eAgMnPc1d1
zeměměřiči	zeměměřič	k1gMnPc1
učinili	učinit	k5eAaPmAgMnP,k5eAaImAgMnP
přesnější	přesný	k2eAgNnSc4d2
měření	měření	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
přiblížilo	přiblížit	k5eAaPmAgNnS
dnešním	dnešní	k2eAgNnPc3d1
měřením	měření	k1gNnPc3
a	a	k8xC
výpočtům	výpočet	k1gInPc3
výšky	výška	k1gFnSc2
Everestu	Everest	k1gInSc2
8848	#num#	k4
m.	m.	k?
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
přijímána	přijímán	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
8848	#num#	k4
m.	m.	k?
Everest	Everest	k1gInSc1
stále	stále	k6eAd1
roste	růst	k5eAaImIp3nS
díky	díky	k7c3
pohybům	pohyb	k1gInPc3
tektonických	tektonický	k2eAgFnPc2d1
desek	deska	k1gFnPc2
<g/>
,	,	kIx,
předpokládaný	předpokládaný	k2eAgInSc1d1
růst	růst	k1gInSc1
je	být	k5eAaImIp3nS
3	#num#	k4
až	až	k9
5	#num#	k4
mm	mm	kA
do	do	k7c2
výšky	výška	k1gFnSc2
a	a	k8xC
27	#num#	k4
mm	mm	kA
k	k	k7c3
severovýchodu	severovýchod	k1gInSc3
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečně	konečně	k6eAd1
podle	podle	k7c2
čínských	čínský	k2eAgFnPc2d1
měření	měření	k1gNnSc4
z	z	k7c2
května	květen	k1gInSc2
2005	#num#	k4
je	být	k5eAaImIp3nS
výška	výška	k1gFnSc1
hory	hora	k1gFnSc2
8844	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
o	o	k7c4
celé	celý	k2eAgInPc4d1
4	#num#	k4
metry	metr	k1gInPc4
méně	málo	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
běžně	běžně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2015	#num#	k4
Nepál	Nepál	k1gInSc4
postihlo	postihnout	k5eAaPmAgNnS
zemětřesení	zemětřesení	k1gNnSc1
o	o	k7c6
síle	síla	k1gFnSc6
7,8	7,8	k4
stupně	stupeň	k1gInSc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vyvstaly	vyvstat	k5eAaPmAgFnP
debaty	debata	k1gFnPc1
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
nezměnila	změnit	k5eNaPmAgFnS
i	i	k9
výška	výška	k1gFnSc1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
horu	hora	k1gFnSc4
přeměřila	přeměřit	k5eAaPmAgFnS
nepálská	nepálský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
i	i	k9
čínská	čínský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
společného	společný	k2eAgNnSc2d1
prohlášení	prohlášení	k1gNnSc2
je	být	k5eAaImIp3nS
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc4
o	o	k7c4
86	#num#	k4
cm	cm	kA
vyšší	vysoký	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
dosud	dosud	k6eAd1
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
tedy	tedy	k8xC
8848,86	8848,86	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Everest	Everest	k1gInSc1
je	být	k5eAaImIp3nS
horou	hora	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
vrcholek	vrcholek	k1gInSc1
je	být	k5eAaImIp3nS
nejvýše	vysoce	k6eAd3,k6eAd1
nad	nad	k7c7
mořskou	mořský	k2eAgFnSc7d1
hladinou	hladina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nejvyšší	vysoký	k2eAgFnSc4d3
horu	hora	k1gFnSc4
světa	svět	k1gInSc2
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
pokládána	pokládat	k5eAaImNgFnS
také	také	k9
Mauna	Mauen	k2eAgFnSc1d1
Kea	Kea	k1gFnSc1
na	na	k7c6
Havaji	Havaj	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
od	od	k7c2
své	svůj	k3xOyFgFnSc2
základny	základna	k1gFnSc2
<g/>
,	,	kIx,
ukryté	ukrytý	k2eAgNnSc1d1
pod	pod	k7c7
mořskou	mořský	k2eAgFnSc7d1
hladinou	hladina	k1gFnSc7
na	na	k7c6
oceánském	oceánský	k2eAgNnSc6d1
dně	dno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mauna	Mauen	k2eAgFnSc1d1
Kea	Kea	k1gFnSc1
takto	takto	k6eAd1
přesahuje	přesahovat	k5eAaImIp3nS
10	#num#	k4
km	km	kA
<g/>
,	,	kIx,
ale	ale	k8xC
nad	nad	k7c4
moře	moře	k1gNnSc4
ční	čnět	k5eAaImIp3nP
pouhými	pouhý	k2eAgNnPc7d1
4205	#num#	k4
metry	metro	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
horou	hora	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
nejvyšší	vysoký	k2eAgFnSc1d3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Chimborazo	Chimboraza	k1gFnSc5
v	v	k7c6
Ekvádoru	Ekvádor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
Chimboraza	Chimboraza	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
o	o	k7c4
2168	#num#	k4
metrů	metr	k1gInPc2
dále	daleko	k6eAd2
než	než	k8xS
Everest	Everest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
Chimborazo	Chimboraza	k1gFnSc5
měří	měřit	k5eAaImIp3nS
6384,4	6384,4	k4
km	km	kA
a	a	k8xC
Everest	Everest	k1gInSc1
pouze	pouze	k6eAd1
6382,3	6382,3	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
rozdílu	rozdíl	k1gInSc2
je	být	k5eAaImIp3nS
zemská	zemský	k2eAgFnSc1d1
rotace	rotace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
deformuje	deformovat	k5eAaImIp3nS
tvar	tvar	k1gInSc4
Země	zem	k1gFnSc2
i	i	k8xC
mořské	mořský	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
výška	výška	k1gFnSc1
standardně	standardně	k6eAd1
měří	měřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
podle	podle	k7c2
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
není	být	k5eNaImIp3nS
Chimborazo	Chimboraza	k1gFnSc5
se	s	k7c7
svými	svůj	k3xOyFgInPc7
6310	#num#	k4
metry	metr	k1gInPc4
nad	nad	k7c7
mořem	moře	k1gNnSc7
ani	ani	k8xC
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
And	Anda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
zajímavost	zajímavost	k1gFnSc4
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
nejhlubší	hluboký	k2eAgNnSc4d3
místo	místo	k1gNnSc4
oceánského	oceánský	k2eAgNnSc2d1
dna	dno	k1gNnSc2
prohlubeň	prohlubeň	k1gFnSc4
Challenger	Challengra	k1gFnPc2
v	v	k7c6
Marianském	Marianský	k2eAgInSc6d1
příkopu	příkop	k1gInSc6
je	být	k5eAaImIp3nS
hlouběji	hluboko	k6eAd2
pod	pod	k7c7
mořskou	mořský	k2eAgFnSc7d1
hladinou	hladina	k1gFnSc7
než	než	k8xS
je	být	k5eAaImIp3nS
Everest	Everest	k1gInSc1
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
od	od	k7c2
Rombok	Rombok	k1gInSc4
Gompa	Gomp	k1gMnSc4
</s>
<s>
Edmund	Edmund	k1gMnSc1
Hillary	Hillara	k1gFnSc2
a	a	k8xC
Tenzing	Tenzing	k1gInSc1
Norgay	Norgaa	k1gFnSc2
jako	jako	k8xC,k8xS
první	první	k4xOgMnPc1
stanuli	stanout	k5eAaPmAgMnP
na	na	k7c6
Everestu	Everest	k1gInSc6
</s>
<s>
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
má	mít	k5eAaImIp3nS
dlouhou	dlouhý	k2eAgFnSc4d1
historii	historie	k1gFnSc4
pokusů	pokus	k1gInPc2
o	o	k7c4
překonání	překonání	k1gNnSc4
<g/>
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
dlouho	dlouho	k6eAd1
odolával	odolávat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kontextu	kontext	k1gInSc6
s	s	k7c7
dnešními	dnešní	k2eAgFnPc7d1
komerčními	komerční	k2eAgFnPc7d1
výpravami	výprava	k1gFnPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c4
něj	on	k3xPp3gMnSc4
lezou	lézt	k5eAaImIp3nP
i	i	k9
děti	dítě	k1gFnPc1
a	a	k8xC
důchodci	důchodce	k1gMnPc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
působit	působit	k5eAaImF
divně	divně	k6eAd1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
si	se	k3xPyFc3
uvědomit	uvědomit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
dnešní	dnešní	k2eAgMnPc1d1
návštěvníci	návštěvník	k1gMnPc1
mají	mít	k5eAaImIp3nP
oproti	oproti	k7c3
těm	ten	k3xDgInPc3
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
obrovské	obrovský	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
<g/>
:	:	kIx,
vyzkoušené	vyzkoušený	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
,	,	kIx,
pevně	pevně	k6eAd1
instalovaná	instalovaný	k2eAgNnPc4d1
lana	lano	k1gNnPc4
a	a	k8xC
žebříky	žebřík	k1gInPc4
<g/>
,	,	kIx,
lepší	dobrý	k2eAgNnSc4d2
vybavení	vybavení	k1gNnSc4
a	a	k8xC
mnohem	mnohem	k6eAd1
důvěryhodnější	důvěryhodný	k2eAgFnPc4d2
předpovědi	předpověď	k1gFnPc4
počasí	počasí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
ale	ale	k8xC
Everest	Everest	k1gInSc1
zůstává	zůstávat	k5eAaImIp3nS
nebezpečnou	bezpečný	k2eNgFnSc7d1
horou	hora	k1gFnSc7
i	i	k9
pro	pro	k7c4
klasické	klasický	k2eAgFnPc4d1
a	a	k8xC
maximálně	maximálně	k6eAd1
jištěné	jištěný	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
na	na	k7c4
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInPc1
pokusy	pokus	k1gInPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
průzkumná	průzkumný	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
George	Georg	k1gMnSc2
Malloryho	Mallory	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
kladla	klást	k5eAaImAgFnS
za	za	k7c4
cíl	cíl	k1gInSc4
zejména	zejména	k9
vylepšit	vylepšit	k5eAaPmF
geografické	geografický	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
o	o	k7c6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
výstup	výstup	k1gInSc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
nebyla	být	k5eNaImAgFnS
dostatečně	dostatečně	k6eAd1
vybavena	vybavit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
expedice	expedice	k1gFnSc1
vystoupila	vystoupit	k5eAaPmAgFnS
na	na	k7c4
severní	severní	k2eAgNnSc4d1
sedlo	sedlo	k1gNnSc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
7060	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
výše	vysoce	k6eAd2
však	však	k9
již	již	k6eAd1
nepokračovala	pokračovat	k5eNaImAgFnS
a	a	k8xC
vrátila	vrátit	k5eAaPmAgFnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
se	se	k3xPyFc4
pak	pak	k6eAd1
uskutečnila	uskutečnit	k5eAaPmAgFnS
velká	velký	k2eAgFnSc1d1
britská	britský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Charlese	Charles	k1gMnSc2
Granvilla	Granvill	k1gMnSc2
Bruce	Bruce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expedice	expedice	k1gFnSc1
čítala	čítat	k5eAaImAgFnS
13	#num#	k4
britských	britský	k2eAgMnPc2d1
horolezců	horolezec	k1gMnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
George	Georg	k1gMnSc2
Malloryho	Mallory	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
160	#num#	k4
nepálských	nepálský	k2eAgMnPc2d1
a	a	k8xC
tibetských	tibetský	k2eAgMnPc2d1
nosičů	nosič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gMnSc7
expedice	expedice	k1gFnSc2
byl	být	k5eAaImAgMnS
také	také	k9
britský	britský	k2eAgMnSc1d1
horolezec	horolezec	k1gMnSc1
a	a	k8xC
chemik	chemik	k1gMnSc1
George	George	k1gFnPc2
Finch	Finch	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
zde	zde	k6eAd1
zkoušel	zkoušet	k5eAaImAgMnS
výhody	výhoda	k1gFnPc4
využití	využití	k1gNnSc2
kyslíku	kyslík	k1gInSc2
z	z	k7c2
přenosných	přenosný	k2eAgFnPc2d1
kyslíkových	kyslíkový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
pokus	pokus	k1gInSc1
(	(	kIx(
<g/>
bez	bez	k7c2
použití	použití	k1gNnSc2
kyslíku	kyslík	k1gInSc2
<g/>
)	)	kIx)
uskutečnil	uskutečnit	k5eAaPmAgInS
Mallory	Mallor	k1gMnPc4
<g/>
,	,	kIx,
tři	tři	k4xCgMnPc1
další	další	k2eAgMnPc1d1
horolezci	horolezec	k1gMnPc1
a	a	k8xC
devět	devět	k4xCc1
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tomto	tento	k3xDgInSc6
pokusu	pokus	k1gInSc6
Mallory	Mallora	k1gFnSc2
a	a	k8xC
dva	dva	k4xCgMnPc1
horolezci	horolezec	k1gMnPc1
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
překročili	překročit	k5eAaPmAgMnP
hranici	hranice	k1gFnSc4
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
8000	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
když	když	k8xS
dosáhli	dosáhnout	k5eAaPmAgMnP
výšky	výška	k1gFnSc2
8225	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhého	druhý	k4xOgInSc2
pokusu	pokus	k1gInSc2
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
již	již	k6eAd1
s	s	k7c7
podporou	podpora	k1gFnSc7
kyslíku	kyslík	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
kromě	kromě	k7c2
nosičů	nosič	k1gInPc2
účastnili	účastnit	k5eAaImAgMnP
Bruce	Bruec	k1gInPc4
<g/>
,	,	kIx,
Finch	Finch	k1gMnSc1
a	a	k8xC
Gurkha	Gurkha	k1gMnSc1
Tejbir	Tejbir	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bruce	Bruec	k1gInSc2
a	a	k8xC
Finch	Fincha	k1gFnPc2
dosáhli	dosáhnout	k5eAaPmAgMnP
výšky	výška	k1gFnSc2
8326	#num#	k4
m.	m.	k?
Třetího	třetí	k4xOgInSc2
pokusu	pokus	k1gInSc2
se	se	k3xPyFc4
účastnili	účastnit	k5eAaImAgMnP
Mallory	Mallor	k1gInPc4
(	(	kIx(
<g/>
tentokrát	tentokrát	k6eAd1
již	již	k6eAd1
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Finch	Finch	k1gMnSc1
<g/>
,	,	kIx,
tři	tři	k4xCgMnPc1
další	další	k2eAgMnPc1d1
britští	britský	k2eAgMnPc1d1
horolezci	horolezec	k1gMnPc1
a	a	k8xC
16	#num#	k4
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
výstupu	výstup	k1gInSc6
však	však	k9
strhli	strhnout	k5eAaPmAgMnP
lavinu	lavina	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
zahynulo	zahynout	k5eAaPmAgNnS
7	#num#	k4
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
již	již	k6eAd1
nepokračovali	pokračovat	k5eNaImAgMnP
a	a	k8xC
celá	celý	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
tím	ten	k3xDgNnSc7
skončila	skončit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastnilo	účastnit	k5eAaImAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
13	#num#	k4
Britů	Brit	k1gMnPc2
(	(	kIx(
<g/>
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Bruce	Bruce	k1gFnSc2
a	a	k8xC
za	za	k7c2
účasti	účast	k1gFnSc2
Malloryho	Mallory	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
asi	asi	k9
150	#num#	k4
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
stavby	stavba	k1gFnSc2
základních	základní	k2eAgInPc2d1
táborů	tábor	k1gInPc2
zahynuli	zahynout	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
nosiči	nosič	k1gMnPc1
na	na	k7c4
omrzliny	omrzlina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
pokus	pokus	k1gInSc1
o	o	k7c4
výstup	výstup	k1gInSc4
byl	být	k5eAaImAgInS
neúspěšný	úspěšný	k2eNgInSc1d1
<g/>
,	,	kIx,
při	při	k7c6
druhém	druhý	k4xOgInSc6
pokusu	pokus	k1gInSc6
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
8573	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
třetí	třetí	k4xOgInSc4
pokus	pokus	k1gInSc4
vyrazili	vyrazit	k5eAaPmAgMnP
Mallory	Mallora	k1gFnPc4
a	a	k8xC
Andrew	Andrew	k1gMnSc5
Irvine	Irvin	k1gMnSc5
<g/>
,	,	kIx,
zpět	zpět	k6eAd1
se	se	k3xPyFc4
však	však	k9
nevrátili	vrátit	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaké	jaký	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
výšky	výška	k1gFnSc2
dosáhli	dosáhnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geolog	geolog	k1gMnSc1
výpravy	výprava	k1gFnSc2
Noel	Noel	k1gMnSc1
Odell	Odell	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
horolezce	horolezec	k1gMnSc2
doprovázel	doprovázet	k5eAaImAgMnS
do	do	k7c2
nejvyššího	vysoký	k2eAgInSc2d3
tábora	tábor	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
viděl	vidět	k5eAaImAgMnS
stoupat	stoupat	k5eAaImF
od	od	k7c2
druhého	druhý	k4xOgInSc2
výšvihu	výšvih	k1gInSc2
(	(	kIx(
<g/>
second	second	k1gInSc1
step	step	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
ve	v	k7c6
výšce	výška	k1gFnSc6
8605	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
expedici	expedice	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
byl	být	k5eAaImAgInS
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
Irvinův	Irvinův	k2eAgInSc1d1
cepín	cepín	k1gInSc1
pod	pod	k7c7
prvním	první	k4xOgInSc7
výšvihem	výšvih	k1gInSc7
(	(	kIx(
<g/>
first	first	k1gInSc1
step	step	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
8560	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
pak	pak	k6eAd1
Malloryho	Malloryha	k1gFnSc5
tělo	tělo	k1gNnSc1
v	v	k7c6
8155	#num#	k4
metrech	metr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
pravděpodobností	pravděpodobnost	k1gFnSc7
lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
době	doba	k1gFnSc6
nehody	nehoda	k1gFnSc2
oba	dva	k4xCgMnPc1
horolezci	horolezec	k1gMnPc1
již	již	k6eAd1
slaňovali	slaňovat	k5eAaImAgMnP
zpět	zpět	k6eAd1
do	do	k7c2
nejvyššího	vysoký	k2eAgInSc2d3
základního	základní	k2eAgInSc2d1
tábora	tábor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
Irvina	Irvin	k2eAgNnSc2d1
ani	ani	k8xC
fotoaparát	fotoaparát	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
měli	mít	k5eAaImAgMnP
s	s	k7c7
sebou	se	k3xPyFc7
a	a	k8xC
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
mnohé	mnohé	k1gNnSc4
objasnit	objasnit	k5eAaPmF
<g/>
,	,	kIx,
však	však	k9
nikdy	nikdy	k6eAd1
nalezeny	nalezen	k2eAgFnPc1d1
nebyly	být	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgFnPc2
teorií	teorie	k1gFnPc2
oba	dva	k4xCgMnPc1
horolezci	horolezec	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
zkušenými	zkušený	k2eAgMnPc7d1
horolezci	horolezec	k1gMnPc7
však	však	k9
tato	tento	k3xDgFnSc1
varianta	varianta	k1gFnSc1
příliš	příliš	k6eAd1
příznivců	příznivec	k1gMnPc2
nemá	mít	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Také	také	k9
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
uskutečnily	uskutečnit	k5eAaPmAgFnP
další	další	k2eAgFnPc1d1
expedice	expedice	k1gFnPc1
s	s	k7c7
pokusy	pokus	k1gInPc7
o	o	k7c4
výstup	výstup	k1gInSc4
<g/>
,	,	kIx,
žádný	žádný	k3yNgInSc1
však	však	k9
nedosáhl	dosáhnout	k5eNaPmAgInS
ani	ani	k8xC
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
druhého	druhý	k4xOgInSc2
pokusu	pokus	k1gInSc2
z	z	k7c2
britské	britský	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
povolil	povolit	k5eAaPmAgInS
Nepál	Nepál	k1gInSc1
vstup	vstup	k1gInSc4
cizinců	cizinec	k1gMnPc2
na	na	k7c4
své	svůj	k3xOyFgNnSc4
území	území	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
otevíralo	otevírat	k5eAaImAgNnS
další	další	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
se	se	k3xPyFc4
tak	tak	k6eAd1
uskutečnila	uskutečnit	k5eAaPmAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
švýcarská	švýcarský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
nebyl	být	k5eNaImAgInS
výstup	výstup	k1gInSc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
průzkum	průzkum	k1gInSc1
trasy	trasa	k1gFnSc2
výstupu	výstup	k1gInSc2
z	z	k7c2
Nepálu	Nepál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýše	vysoce	k6eAd3,k6eAd1
vystoupili	vystoupit	k5eAaPmAgMnP
Raymond	Raymond	k1gMnSc1
Lambert	Lambert	k1gMnSc1
a	a	k8xC
Šerpa	šerpa	k1gFnSc1
Tenzing	Tenzing	k1gInSc4
Norgay	Norgaa	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
nadmořské	nadmořský	k2eAgFnPc4d1
výšky	výška	k1gFnPc4
8595	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
nový	nový	k2eAgInSc4d1
výškový	výškový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
(	(	kIx(
<g/>
s	s	k7c7
výhradou	výhrada	k1gFnSc7
neznámé	známý	k2eNgFnSc2d1
výšky	výška	k1gFnSc2
dosažené	dosažený	k2eAgInPc4d1
Mallorym	Mallorym	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Norgay	Norgaum	k1gNnPc7
získal	získat	k5eAaPmAgMnS
zkušenosti	zkušenost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
již	již	k6eAd1
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
zúročil	zúročit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1
</s>
<s>
Jako	jako	k9
první	první	k4xOgMnSc1
stanul	stanout	k5eAaPmAgInS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Mount	Mounta	k1gFnPc2
Everestu	Everest	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1953	#num#	k4
Novozélanďan	Novozélanďan	k1gMnSc1
Edmund	Edmund	k1gMnSc1
Hillary	Hillara	k1gFnSc2
a	a	k8xC
nepálský	nepálský	k2eAgInSc1d1
Šerpa	šerpa	k1gFnSc1
Tenzing	Tenzing	k1gInSc4
Norgay	Norgaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
žena	žena	k1gFnSc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
</s>
<s>
První	první	k4xOgFnSc7
ženou	žena	k1gFnSc7
na	na	k7c6
Everestu	Everest	k1gInSc6
byla	být	k5eAaImAgFnS
Japonka	Japonka	k1gFnSc1
Džunko	džunka	k1gFnSc5
Tabeiová	Tabeiový	k2eAgFnSc1d1
dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1975	#num#	k4
po	po	k7c6
výstupu	výstup	k1gInSc6
klasickou	klasický	k2eAgFnSc7d1
jižní	jižní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
o	o	k7c4
11	#num#	k4
dní	den	k1gInPc2
později	pozdě	k6eAd2
ji	on	k3xPp3gFnSc4
následovala	následovat	k5eAaImAgFnS
Tibeťanka	Tibeťanka	k1gFnSc1
Phantog	Phantoga	k1gFnPc2
po	po	k7c6
výstupu	výstup	k1gInSc6
severní	severní	k2eAgNnSc1d1
cestou	cesta	k1gFnSc7
s	s	k7c7
čínskou	čínský	k2eAgFnSc7d1
expedicí	expedice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc7
ženou	žena	k1gFnSc7
a	a	k8xC
první	první	k4xOgFnSc7
Evropankou	Evropanka	k1gFnSc7
na	na	k7c6
Everestu	Everest	k1gInSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
asi	asi	k9
nejúspěšnější	úspěšný	k2eAgFnSc1d3
himálajská	himálajský	k2eAgFnSc1d1
horolezkyně	horolezkyně	k1gFnSc1
–	–	k?
Polka	Polka	k1gFnSc1
Wanda	Wanda	k1gFnSc1
Rutkiewiczová	Rutkiewiczová	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
výstup	výstup	k1gInSc1
</s>
<s>
První	první	k4xOgInSc4
výstup	výstup	k1gInSc4
v	v	k7c6
zimě	zima	k1gFnSc6
dokončila	dokončit	k5eAaPmAgFnS
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1980	#num#	k4
JV	JV	kA
hřebenem	hřeben	k1gInSc7
polská	polský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Andrejem	Andrej	k1gMnSc7
Zawadou	Zawadý	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
vystoupili	vystoupit	k5eAaPmAgMnP
Leszek	Leszek	k1gInSc4
Cichy	Cicha	k1gFnSc2
a	a	k8xC
Krzysztof	Krzysztof	k1gInSc4
Wielicki	Wielick	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polští	polský	k2eAgMnPc1d1
horolezci	horolezec	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
v	v	k7c6
Himálaji	Himálaj	k1gFnSc6
průkopníky	průkopník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
výstupu	výstup	k1gInSc2
na	na	k7c4
Everest	Everest	k1gInSc4
čelili	čelit	k5eAaImAgMnP
mrazům	mráz	k1gInPc3
kolem	kolem	k7c2
−	−	k?
°	°	k?
<g/>
C	C	kA
a	a	k8xC
prudkým	prudký	k2eAgFnPc3d1
vichřicím	vichřice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Výstup	výstup	k1gInSc1
bez	bez	k7c2
použití	použití	k1gNnSc2
kyslíku	kyslík	k1gInSc2
</s>
<s>
První	první	k4xOgInSc1
výstup	výstup	k1gInSc1
bez	bez	k7c2
použití	použití	k1gNnSc2
kyslíku	kyslík	k1gInSc2
na	na	k7c4
Mount	Mount	k1gInSc4
Everest	Everest	k1gInSc4
úspěšně	úspěšně	k6eAd1
zakončili	zakončit	k5eAaPmAgMnP
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1978	#num#	k4
Reinhold	Reinhold	k1gMnSc1
Messner	Messner	k1gMnSc1
a	a	k8xC
Peter	Peter	k1gMnSc1
Habeler	Habeler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Messner	Messner	k1gInSc1
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
předvedl	předvést	k5eAaPmAgMnS
první	první	k4xOgInSc4
kompletní	kompletní	k2eAgInSc4d1
sólovýstup	sólovýstup	k1gInSc4
<g/>
,	,	kIx,
samozřejmě	samozřejmě	k6eAd1
opět	opět	k6eAd1
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
kyslíku	kyslík	k1gInSc2
devalvuje	devalvovat	k5eAaBmIp3nS
podaný	podaný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
Messnera	Messner	k1gMnSc2
odpovídá	odpovídat	k5eAaImIp3nS
výstup	výstup	k1gInSc4
na	na	k7c4
Everest	Everest	k1gInSc4
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
výstupu	výstup	k1gInSc2
do	do	k7c2
výšky	výška	k1gFnSc2
6400	#num#	k4
metrů	metr	k1gInPc2
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horolezci	horolezec	k1gMnPc1
stoupající	stoupající	k2eAgMnPc1d1
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
jsou	být	k5eAaImIp3nP
v	v	k7c6
případě	případ	k1gInSc6
vypotřebování	vypotřebování	k1gNnSc2
lahve	lahev	k1gFnSc2
ve	v	k7c6
velkém	velký	k2eAgNnSc6d1
nebezpečí	nebezpečí	k1gNnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
nejsou	být	k5eNaImIp3nP
aklimatizování	aklimatizování	k1gNnSc4
na	na	k7c4
skutečnou	skutečný	k2eAgFnSc4d1
výšku	výška	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takováto	takovýto	k3xDgFnSc1
událost	událost	k1gFnSc4
končí	končit	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
tragicky	tragicky	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
evidovaných	evidovaný	k2eAgMnPc2d1
výstupů	výstup	k1gInPc2
na	na	k7c4
Everest	Everest	k1gInSc4
je	být	k5eAaImIp3nS
výstupů	výstup	k1gInPc2
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
jen	jen	k9
něco	něco	k3yInSc4
přes	přes	k7c4
dvě	dva	k4xCgNnPc4
procenta	procento	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Československý	československý	k2eAgInSc1d1
a	a	k8xC
český	český	k2eAgInSc1d1
výstup	výstup	k1gInSc1
</s>
<s>
Dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1984	#num#	k4
vystoupili	vystoupit	k5eAaPmAgMnP
na	na	k7c4
vrchol	vrchol	k1gInSc4
první	první	k4xOgMnPc1
českoslovenští	československý	k2eAgMnPc1d1
horolezci	horolezec	k1gMnPc1
–	–	k?
Slováci	Slovák	k1gMnPc1
Zoltán	Zoltán	k1gMnSc1
Demján	Demján	k1gMnSc1
a	a	k8xC
Jozef	Jozef	k1gMnSc1
Psotka	Psotka	k1gMnSc1
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gInPc7
i	i	k9
Šerpa	šerpa	k1gFnSc1
Ang	Ang	k1gFnSc1
Rita	Rita	k1gFnSc1
(	(	kIx(
<g/>
varianta	varianta	k1gFnSc1
Polské	polský	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
na	na	k7c6
JZ	JZ	kA
pilíři	pilíř	k1gInSc6
<g/>
,	,	kIx,
bez	bez	k7c2
použití	použití	k1gNnSc2
kyslíku	kyslík	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jozef	Jozef	k1gMnSc1
Psotka	Psotka	k1gMnSc1
však	však	k9
při	při	k7c6
sestupu	sestup	k1gInSc6
z	z	k7c2
Jižního	jižní	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
zahynul	zahynout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
český	český	k2eAgMnSc1d1
horolezec	horolezec	k1gMnSc1
stanul	stanout	k5eAaPmAgMnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
Leopold	Leopold	k1gMnSc1
Sulovský	Sulovský	k1gMnSc1
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
Italem	Ital	k1gMnSc7
Battistou	Battista	k1gMnSc7
Bonalim	Bonalima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tragické	tragický	k2eAgFnPc1d1
události	událost	k1gFnPc1
</s>
<s>
Ve	v	k7c6
dnech	den	k1gInPc6
10	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1996	#num#	k4
zemřelo	zemřít	k5eAaPmAgNnS
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
osm	osm	k4xCc4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
tři	tři	k4xCgMnPc4
horští	horský	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
<g/>
:	:	kIx,
Rob	roba	k1gFnPc2
Hall	Hall	k1gInSc1
<g/>
,	,	kIx,
Scott	Scott	k1gInSc1
Fisher	Fishra	k1gFnPc2
a	a	k8xC
Andy	Anda	k1gFnSc2
Harris	Harris	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mrtvých	mrtvý	k1gMnPc2
by	by	kYmCp3nP
bylo	být	k5eAaImAgNnS
ještě	ještě	k6eAd1
víc	hodně	k6eAd2
<g/>
,	,	kIx,
nebýt	být	k5eNaImF
dočasného	dočasný	k2eAgNnSc2d1
utišení	utišení	k1gNnSc2
bouře	bouř	k1gFnSc2
a	a	k8xC
výkonu	výkon	k1gInSc2
vůdce	vůdce	k1gMnSc2
Anatolije	Anatolije	k1gMnSc2
Bukrejeva	Bukrejev	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
částí	část	k1gFnSc7
kolegů	kolega	k1gMnPc2
později	pozdě	k6eAd2
kritizován	kritizovat	k5eAaImNgMnS
<g/>
,	,	kIx,
že	že	k8xS
lezl	lézt	k5eAaImAgMnS
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
<g/>
,	,	kIx,
nesetrval	setrvat	k5eNaPmAgMnS
s	s	k7c7
výpravou	výprava	k1gMnSc7
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
dřív	dříve	k6eAd2
připravit	připravit	k5eAaPmF
pro	pro	k7c4
navracející	navracející	k2eAgInSc4d1
se	se	k3xPyFc4
čaj	čaj	k1gInSc4
a	a	k8xC
kyslík	kyslík	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
někteří	některý	k3yIgMnPc1
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
nezodpovědné	zodpovědný	k2eNgMnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příběhem	příběh	k1gInSc7
se	se	k3xPyFc4
šťastným	šťastný	k2eAgInSc7d1
koncem	konec	k1gInSc7
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
výstup	výstup	k1gInSc1
Izraelce	Izraelec	k1gMnSc2
Nadava	Nadava	k1gFnSc1
Ben	Ben	k1gInSc1
Yehudy	Yehuda	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
horu	hora	k1gFnSc4
zdolat	zdolat	k5eAaPmF
v	v	k7c6
květnu	květen	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekord	rekord	k1gInSc1
nejmladšího	mladý	k2eAgMnSc2d3
Izraelce	Izraelec	k1gMnSc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Mount	Mounta	k1gFnPc2
Everestu	Everest	k1gInSc2
však	však	k9
obětoval	obětovat	k5eAaBmAgMnS
záchraně	záchrana	k1gFnSc3
tureckého	turecký	k2eAgMnSc2d1
horolezce	horolezec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestou	cestou	k7c2
dolů	dolů	k6eAd1
jim	on	k3xPp3gMnPc3
nikdo	nikdo	k3yNnSc1
nebyl	být	k5eNaImAgMnS
ochotný	ochotný	k2eAgMnSc1d1
pomoci	pomoct	k5eAaPmF
a	a	k8xC
Nadav	nadat	k5eAaPmDgInS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
oprávněně	oprávněně	k6eAd1
hrdinou	hrdina	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
učitel	učitel	k1gMnSc1
matematiky	matematika	k1gFnSc2
z	z	k7c2
britského	britský	k2eAgNnSc2d1
městečka	městečko	k1gNnSc2
Gainsborough	Gainsborough	k1gMnSc1
pokusil	pokusit	k5eAaPmAgMnS
vylézt	vylézt	k5eAaPmF
na	na	k7c4
Mount	Mount	k1gInSc4
Everestu	Everest	k1gInSc2
s	s	k7c7
cestovní	cestovní	k2eAgFnSc7d1
kanceláří	kancelář	k1gFnSc7
Asian	Asiany	k1gInPc2
Trekking	Trekking	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdolal	zdolat	k5eAaPmAgMnS
vrchol	vrchol	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
cestou	cesta	k1gFnSc7
dolů	dolů	k6eAd1
ho	on	k3xPp3gMnSc4
opustily	opustit	k5eAaPmAgFnP
síly	síla	k1gFnPc1
a	a	k8xC
ukryl	ukrýt	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
malé	malý	k2eAgFnSc6d1
jeskyni	jeskyně	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
několika	několik	k4yIc2
hodin	hodina	k1gFnPc2
ho	on	k3xPp3gNnSc2
míjelo	míjet	k5eAaImAgNnS
mnoho	mnoho	k4c1
dalších	další	k2eAgMnPc2d1
horolezců	horolezec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dalších	další	k2eAgNnPc6d1
deseti	deset	k4xCc6
hodinách	hodina	k1gFnPc6
přišla	přijít	k5eAaPmAgFnS
výprava	výprava	k1gFnSc1
natáčející	natáčející	k2eAgFnSc1d1
dokument	dokument	k1gInSc4
pro	pro	k7c4
Discovery	Discovera	k1gFnPc4
Channel	Channela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeptali	zeptat	k5eAaPmAgMnP
se	se	k3xPyFc4
Davida	David	k1gMnSc2
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
jsi	být	k5eAaImIp2nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
odpověděl	odpovědět	k5eAaPmAgMnS
"	"	kIx"
<g/>
Jmenuji	jmenovat	k5eAaImIp1nS,k5eAaBmIp1nS
se	se	k3xPyFc4
David	David	k1gMnSc1
Sharp	Sharp	kA
<g/>
,	,	kIx,
jsem	být	k5eAaImIp1nS
tu	tu	k6eAd1
s	s	k7c7
Asian	Asiany	k1gInPc2
Trekking	Trekking	k1gInSc1
a	a	k8xC
jen	jen	k9
se	se	k3xPyFc4
chci	chtít	k5eAaImIp1nS
trochu	trochu	k6eAd1
vyspat	vyspat	k5eAaPmF
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poskytli	poskytnout	k5eAaPmAgMnP
mu	on	k3xPp3gMnSc3
zásobu	zásoba	k1gFnSc4
kyslíku	kyslík	k1gInSc2
a	a	k8xC
nechali	nechat	k5eAaPmAgMnP
ho	on	k3xPp3gMnSc4
samotného	samotný	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
David	David	k1gMnSc1
Sharp	Sharp	kA
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celkem	celkem	k6eAd1
při	při	k7c6
výstupu	výstup	k1gInSc6
(	(	kIx(
<g/>
či	či	k8xC
sestupu	sestup	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c4
Mount	Mount	k1gInSc4
Everest	Everest	k1gInSc4
zahynulo	zahynout	k5eAaPmAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1922	#num#	k4
až	až	k9
2010	#num#	k4
219	#num#	k4
horolezců	horolezec	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
dvou	dva	k4xCgMnPc2
Čechů	Čech	k1gMnPc2
(	(	kIx(
<g/>
Libor	Libor	k1gMnSc1
Kozák	Kozák	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
a	a	k8xC
Věslav	Věslav	k1gMnSc1
Chrząszcz	Chrząszcz	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2015	#num#	k4
způsobilo	způsobit	k5eAaPmAgNnS
zemětřesení	zemětřesení	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Nepálu	Nepál	k1gInSc2
smrt	smrt	k1gFnSc4
přibližně	přibližně	k6eAd1
3	#num#	k4
700	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
na	na	k7c4
200	#num#	k4
lidí	člověk	k1gMnPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Mount	Mounta	k1gFnPc2
Everestu	Everest	k1gInSc2
pohřešovaných	pohřešovaná	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
závažným	závažný	k2eAgInSc7d1
problémem	problém	k1gInSc7
při	při	k7c6
výstupu	výstup	k1gInSc6
na	na	k7c4
Mount	Mount	k1gInSc4
Everest	Everest	k1gInSc1
je	být	k5eAaImIp3nS
množství	množství	k1gNnSc4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
o	o	k7c4
to	ten	k3xDgNnSc4
pokouší	pokoušet	k5eAaImIp3nS
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nP
leckdy	leckdy	k6eAd1
i	i	k9
hodinové	hodinový	k2eAgFnPc1d1
fronty	fronta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebezpečí	nebezpeč	k1gFnPc2wB
plyne	plynout	k5eAaImIp3nS
jak	jak	k6eAd1
z	z	k7c2
pobytu	pobyt	k1gInSc2
v	v	k7c6
zóně	zóna	k1gFnSc6
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
ze	z	k7c2
ztráty	ztráta	k1gFnSc2
tepla	teplo	k1gNnSc2
a	a	k8xC
z	z	k7c2
možnosti	možnost	k1gFnSc2
že	že	k8xS
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
vyčerpání	vyčerpání	k1gNnSc3
zásob	zásoba	k1gFnPc2
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
je	být	k5eAaImIp3nS
popularita	popularita	k1gFnSc1
výstupu	výstup	k1gInSc2
<g/>
,	,	kIx,
nezkušenost	nezkušenost	k1gFnSc1
lezců	lezec	k1gMnPc2
a	a	k8xC
omezený	omezený	k2eAgInSc4d1
počet	počet	k1gInSc4
tzv.	tzv.	kA
vrcholových	vrcholový	k2eAgInPc2d1
dní	den	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
</s>
<s>
Největší	veliký	k2eAgInSc1d3
počet	počet	k1gInSc1
výstupů	výstup	k1gInPc2
na	na	k7c4
vrchol	vrchol	k1gInSc4
zvládl	zvládnout	k5eAaPmAgInS
k	k	k7c3
roku	rok	k1gInSc3
2011	#num#	k4
Šerpa	šerpa	k1gFnSc1
Appa	Appa	k1gFnSc1
–	–	k?
celkem	celek	k1gInSc7
21	#num#	k4
výstupů	výstup	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Rekord	rekord	k1gInSc1
k	k	k7c3
21	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2019	#num#	k4
drží	držet	k5eAaImIp3nS
šerpa	šerpa	k1gFnSc1
Kami	Kam	k1gFnSc2
Rita	Rita	k1gFnSc1
s	s	k7c7
24	#num#	k4
výstupy	výstup	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Nejmladšímu	mladý	k2eAgMnSc3d3
lezci	lezec	k1gMnSc3
na	na	k7c6
Everestu	Everest	k1gInSc6
bylo	být	k5eAaImAgNnS
13	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
jím	on	k3xPp3gMnSc7
Američan	Američan	k1gMnSc1
Jordan	Jordan	k1gMnSc1
Romer	Romer	k1gMnSc1
<g/>
,	,	kIx,
nejstarší	starý	k2eAgMnSc1d3
byl	být	k5eAaImAgInS
Šerpa	šerpa	k1gFnSc1
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Horolezecké	horolezecký	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
</s>
<s>
Zleva	zleva	k6eAd1
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
<g/>
,	,	kIx,
Lhotse	Lhotse	k1gFnSc1
a	a	k8xC
Ama	Ama	k1gFnSc1
Dablam	Dablam	k1gInSc1
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
z	z	k7c2
Nepálu	Nepál	k1gInSc2
</s>
<s>
Everest	Everest	k1gInSc1
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
horolezecké	horolezecký	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
,	,	kIx,
jihovýchodní	jihovýchodní	k2eAgFnSc1d1
z	z	k7c2
Nepálu	Nepál	k1gInSc2
a	a	k8xC
severovýchodní	severovýchodní	k2eAgNnPc4d1
z	z	k7c2
Tibetu	Tibet	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
existují	existovat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc4d1
méně	málo	k6eAd2
časté	častý	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihovýchodní	jihovýchodní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
Nepálu	Nepál	k1gInSc2
je	být	k5eAaImIp3nS
technicky	technicky	k6eAd1
méně	málo	k6eAd2
obtížná	obtížný	k2eAgFnSc1d1
než	než	k8xS
severozápadní	severozápadní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
Tibetu	Tibet	k1gInSc2
<g/>
,	,	kIx,
také	také	k9
proto	proto	k8xC
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
frekventovanější	frekventovaný	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihovýchodní	jihovýchodní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
šli	jít	k5eAaImAgMnP
také	také	k9
Hillary	Hillara	k1gFnPc4
a	a	k8xC
Tenzing	Tenzing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výběr	výběr	k1gInSc1
prvovýstupu	prvovýstup	k1gInSc2
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
předurčen	předurčen	k2eAgInSc1d1
politickou	politický	k2eAgFnSc7d1
situací	situace	k1gFnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
Tibet	Tibet	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
pro	pro	k7c4
cizince	cizinec	k1gMnPc4
uzavřen	uzavřen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
pokusů	pokus	k1gInPc2
o	o	k7c4
výstup	výstup	k1gInSc4
je	být	k5eAaImIp3nS
během	během	k7c2
dubna	duben	k1gInSc2
a	a	k8xC
května	květen	k1gInSc2
před	před	k7c7
letní	letní	k2eAgFnSc7d1
monzunovou	monzunový	k2eAgFnSc7d1
sezónou	sezóna	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
také	také	k9
snižuje	snižovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
tzv.	tzv.	kA
tryskového	tryskový	k2eAgNnSc2d1
proudění	proudění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
k	k	k7c3
pokusům	pokus	k1gInPc3
o	o	k7c4
výstup	výstup	k1gInSc4
dochází	docházet	k5eAaImIp3nS
i	i	k9
po	po	k7c6
letní	letní	k2eAgFnSc6d1
monzunové	monzunový	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
v	v	k7c6
září	září	k1gNnSc6
a	a	k8xC
říjnu	říjen	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
výška	výška	k1gFnSc1
sněhové	sněhový	k2eAgFnSc2d1
pokrývky	pokrývka	k1gFnSc2
činí	činit	k5eAaImIp3nS
výstupy	výstup	k1gInPc4
mnohem	mnohem	k6eAd1
obtížnějšími	obtížný	k2eAgInPc7d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
výstup	výstup	k1gInSc4
po	po	k7c6
letní	letní	k2eAgFnSc6d1
monzunové	monzunový	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
se	se	k3xPyFc4
podařil	podařit	k5eAaPmAgInS
na	na	k7c4
podzim	podzim	k1gInSc4
1973	#num#	k4
japonské	japonský	k2eAgFnSc6d1
expedici	expedice	k1gFnSc6
jihovýchodním	jihovýchodní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1
kapitolou	kapitola	k1gFnSc7
jsou	být	k5eAaImIp3nP
zimní	zimní	k2eAgInPc1d1
výstupy	výstup	k1gInPc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
výstupy	výstup	k1gInPc1
v	v	k7c6
období	období	k1gNnSc6
zimního	zimní	k2eAgInSc2d1
monzunu	monzun	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
horolezci	horolezec	k1gMnPc1
musí	muset	k5eAaImIp3nP
čelit	čelit	k5eAaImF
polárním	polární	k2eAgFnPc3d1
teplotám	teplota	k1gFnPc3
a	a	k8xC
častým	častý	k2eAgFnPc3d1
vichřicím	vichřice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
v	v	k7c6
zimě	zima	k1gFnSc6
v	v	k7c6
Himálaji	Himálaj	k1gFnSc6
méně	málo	k6eAd2
sněží	sněžit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšných	úspěšný	k2eAgMnPc2d1
zimních	zimní	k2eAgMnPc2d1
výstupů	výstup	k1gInPc2
na	na	k7c4
Everest	Everest	k1gInSc4
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
než	než	k8xS
deset	deset	k4xCc4
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
horolezeckých	horolezecký	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
na	na	k7c4
Everest	Everest	k1gInSc4
</s>
<s>
První	první	k4xOgInPc1
pokusy	pokus	k1gInPc1
a	a	k8xC
výstupy	výstup	k1gInPc1
na	na	k7c6
Everestu	Everest	k1gInSc6
byly	být	k5eAaImAgInP
značně	značně	k6eAd1
ovlivněny	ovlivněn	k2eAgInPc1d1
politickou	politický	k2eAgFnSc7d1
situací	situace	k1gFnSc7
v	v	k7c6
obou	dva	k4xCgFnPc6
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
byl	být	k5eAaImAgInS
pro	pro	k7c4
cizince	cizinec	k1gMnPc4
uzavřen	uzavřen	k2eAgInSc4d1
Nepál	Nepál	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1950	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
Čína	Čína	k1gFnSc1
vojensky	vojensky	k6eAd1
obsadila	obsadit	k5eAaPmAgFnS
a	a	k8xC
znepřístupnila	znepřístupnit	k5eAaPmAgFnS
Tibet	Tibet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
tří	tři	k4xCgFnPc2
stěn	stěna	k1gFnPc2
Everestu	Everest	k1gInSc2
severní	severní	k2eAgFnSc1d1
a	a	k8xC
východní	východní	k2eAgMnSc1d1
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
Tibetu	Tibet	k1gInSc2
<g/>
,	,	kIx,
jihozápadní	jihozápadní	k2eAgFnSc6d1
do	do	k7c2
Nepálu	Nepál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
–	–	k?
klasická	klasický	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
přes	přes	k7c4
ledopád	ledopád	k1gInSc4
Khumbu	Khumb	k1gInSc2
<g/>
,	,	kIx,
Jižní	jižní	k2eAgNnSc1d1
sedlo	sedlo	k1gNnSc1
(	(	kIx(
<g/>
7986	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
JV	JV	kA
hřeben	hřeben	k1gInSc1
</s>
<s>
První	první	k4xOgInSc4
pokus	pokus	k1gInSc4
provedla	provést	k5eAaPmAgFnS
britsko-novozélandská	britsko-novozélandský	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jako	jako	k9
první	první	k4xOgFnSc1
prostoupila	prostoupit	k5eAaPmAgFnS
ledopád	ledopád	k1gInSc4
do	do	k7c2
výšky	výška	k1gFnSc2
6500	#num#	k4
m.	m.	k?
Následovaly	následovat	k5eAaImAgFnP
dvě	dva	k4xCgFnPc4
švýcarské	švýcarský	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
na	na	k7c6
jaře	jaro	k1gNnSc6
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
první	první	k4xOgFnSc2
z	z	k7c2
nich	on	k3xPp3gFnPc2
Raymond	Raymonda	k1gFnPc2
Lambert	Lambert	k1gInSc1
a	a	k8xC
Tenzing	Tenzing	k1gInSc1
Norkej	Norkej	k1gFnPc2
dosáhli	dosáhnout	k5eAaPmAgMnP
výšky	výška	k1gFnSc2
8595	#num#	k4
m.	m.	k?
Následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
jejich	jejich	k3xOp3gFnSc7
cestou	cesta	k1gFnSc7
dokončila	dokončit	k5eAaPmAgFnS
výstup	výstup	k1gInSc4
britská	britský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Johnem	John	k1gMnSc7
Huntem	hunt	k1gInSc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
ledovcem	ledovec	k1gInSc7
Khumbu	Khumb	k1gInSc2
a	a	k8xC
Jižním	jižní	k2eAgNnSc7d1
sedlem	sedlo	k1gNnSc7
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
variant	varianta	k1gFnPc2
výstupu	výstup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
trasa	trasa	k1gFnSc1
doslova	doslova	k6eAd1
obležena	oblehnout	k5eAaPmNgFnS
komerčními	komerční	k2eAgFnPc7d1
expedicemi	expedice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
sezóny	sezóna	k1gFnSc2
ji	on	k3xPp3gFnSc4
Šerpové	Šerpová	k1gFnSc6
vybaví	vybavit	k5eAaPmIp3nP
fixními	fixní	k2eAgNnPc7d1
lany	lano	k1gNnPc7
a	a	k8xC
upraví	upravit	k5eAaPmIp3nS
cestu	cesta	k1gFnSc4
ledopádem	ledopád	k1gInSc7
Khumbu	Khumb	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
použití	použití	k1gNnSc4
fixních	fixní	k2eAgNnPc2d1
lan	lano	k1gNnPc2
se	se	k3xPyFc4
platí	platit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
příznivých	příznivý	k2eAgInPc6d1
vrcholových	vrcholový	k2eAgInPc6d1
dnech	den	k1gInPc6
pak	pak	k6eAd1
vystupují	vystupovat	k5eAaImIp3nP
stovky	stovka	k1gFnPc4
zájemců	zájemce	k1gMnPc2
o	o	k7c4
vrchol	vrchol	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
drtivé	drtivý	k2eAgFnSc6d1
většině	většina	k1gFnSc6
s	s	k7c7
použitím	použití	k1gNnSc7
kyslíku	kyslík	k1gInSc2
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
Šerpů	Šerp	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
dopravit	dopravit	k5eAaPmF
platícího	platící	k2eAgMnSc4d1
klienta	klient	k1gMnSc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
a	a	k8xC
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Zvýšený	zvýšený	k2eAgInSc1d1
počet	počet	k1gInSc1
lidí	člověk	k1gMnPc2
pokoušejících	pokoušející	k2eAgMnPc2d1
se	se	k3xPyFc4
o	o	k7c6
dosažení	dosažení	k1gNnSc6
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
většina	většina	k1gFnSc1
jsou	být	k5eAaImIp3nP
více	hodně	k6eAd2
dobrodruzi	dobrodruh	k1gMnPc1
než	než	k8xS
horolezci	horolezec	k1gMnPc1
<g/>
,	,	kIx,
přináší	přinášet	k5eAaImIp3nS
s	s	k7c7
sebou	se	k3xPyFc7
negativní	negativní	k2eAgInPc1d1
jevy	jev	k1gInPc4
od	od	k7c2
znečišťování	znečišťování	k1gNnSc2
trasy	trasa	k1gFnSc2
odpadky	odpadek	k1gInPc4
až	až	k8xS
po	po	k7c4
krádeže	krádež	k1gFnPc4
materiálu	materiál	k1gInSc2
a	a	k8xC
vynesených	vynesený	k2eAgFnPc2d1
zásob	zásoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
–	–	k?
klasická	klasický	k2eAgFnSc1d1
severní	severní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
přes	přes	k7c4
Severní	severní	k2eAgNnSc4d1
sedlo	sedlo	k1gNnSc4
(	(	kIx(
<g/>
7000	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
severní	severní	k2eAgInSc4d1
a	a	k8xC
severovýchodní	severovýchodní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
</s>
<s>
Trasa	trasa	k1gFnSc1
prvních	první	k4xOgInPc2
pokusů	pokus	k1gInPc2
o	o	k7c4
výstup	výstup	k1gInSc4
na	na	k7c4
nejvyšší	vysoký	k2eAgFnSc4d3
horu	hora	k1gFnSc4
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1921	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
Britové	Brit	k1gMnPc1
zorganizovali	zorganizovat	k5eAaPmAgMnP
sedm	sedm	k4xCc4
expedic	expedice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejblíže	blízce	k6eAd3
úspěchu	úspěch	k1gInSc3
byly	být	k5eAaImAgFnP
třetí	třetí	k4xOgFnSc1
a	a	k8xC
čtvrtá	čtvrtý	k4xOgFnSc1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
1924	#num#	k4
a	a	k8xC
1933	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
sedm	sedm	k4xCc1
horolezců	horolezec	k1gMnPc2
(	(	kIx(
<g/>
tři	tři	k4xCgMnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
bez	bez	k7c2
kyslíkových	kyslíkový	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
výškový	výškový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
překonán	překonat	k5eAaPmNgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
<g/>
)	)	kIx)
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
výšky	výška	k1gFnSc2
8500	#num#	k4
<g/>
–	–	k?
<g/>
8600	#num#	k4
m.	m.	k?
Zastavily	zastavit	k5eAaPmAgFnP
je	být	k5eAaImIp3nS
fyzická	fyzický	k2eAgFnSc1d1
únava	únava	k1gFnSc1
a	a	k8xC
také	také	k9
technické	technický	k2eAgFnPc4d1
obtíže	obtíž	k1gFnPc4
tzv.	tzv.	kA
Druhého	druhý	k4xOgInSc2
výšvihu	výšvih	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstup	výstup	k1gInSc4
severní	severní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
dokončila	dokončit	k5eAaPmAgFnS
obrovská	obrovský	k2eAgFnSc1d1
čínská	čínský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
čítající	čítající	k2eAgFnSc1d1
přes	přes	k7c4
200	#num#	k4
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
vystoupili	vystoupit	k5eAaPmAgMnP
Číňané	Číňan	k1gMnPc1
Wang	Wanga	k1gFnPc2
Fu-čchou	Fu-čchý	k2eAgFnSc4d1
<g/>
,	,	kIx,
Čchu	Čcha	k1gFnSc4
Ťin-chua	Ťin-chu	k1gInSc2
a	a	k8xC
Tibeťan	Tibeťan	k1gMnSc1
Gonpa	Gonp	k1gMnSc2
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tato	tento	k3xDgFnSc1
trasa	trasa	k1gFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
v	v	k7c6
obležení	obležení	k1gNnSc6
komerčních	komerční	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
a	a	k8xC
zajištěná	zajištěný	k2eAgFnSc1d1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
fixními	fixní	k2eAgNnPc7d1
lany	lano	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
–	–	k?
americká	americký	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
přes	přes	k7c4
ledopád	ledopád	k1gInSc4
Khumbu	Khumb	k1gInSc2
<g/>
,	,	kIx,
západní	západní	k2eAgNnSc1d1
rameno	rameno	k1gNnSc1
(	(	kIx(
<g/>
7200	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Hornbeinův	Hornbeinův	k2eAgInSc1d1
kuloár	kuloár	k1gInSc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
stěně	stěna	k1gFnSc6
</s>
<s>
Trasu	trasa	k1gFnSc4
otevřela	otevřít	k5eAaPmAgFnS
americká	americký	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Normanem	Norman	k1gMnSc7
Dyhrenfurthem	Dyhrenfurth	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
současně	současně	k6eAd1
postupovala	postupovat	k5eAaImAgFnS
i	i	k9
klasickou	klasický	k2eAgFnSc7d1
jižní	jižní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obou	dva	k4xCgFnPc6
trasách	trasa	k1gFnPc6
byla	být	k5eAaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvovýstup	prvovýstup	k1gInSc1
přes	přes	k7c4
západní	západní	k2eAgNnSc4d1
rameno	rameno	k1gNnSc4
otevřeli	otevřít	k5eAaPmAgMnP
William	William	k1gInSc4
Unsoeld	Unsoelda	k1gFnPc2
a	a	k8xC
Tom	Tom	k1gMnSc1
Hornbein	Hornbein	k1gMnSc1
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestoupili	sestoupit	k5eAaPmAgMnP
jihovýchodním	jihovýchodní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
se	se	k3xPyFc4
svými	svůj	k3xOyFgMnPc7
kolegy	kolega	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
tudy	tudy	k6eAd1
ve	v	k7c4
stejný	stejný	k2eAgInSc4d1
den	den	k1gInSc4
stoupali	stoupat	k5eAaImAgMnP
a	a	k8xC
dosáhli	dosáhnout	k5eAaPmAgMnP
vrcholu	vrchol	k1gInSc2
jen	jen	k9
dvě	dva	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
před	před	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sestupu	sestup	k1gInSc6
museli	muset	k5eAaImAgMnP
bivakovat	bivakovat	k5eAaImF
v	v	k7c6
8400	#num#	k4
m	m	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
utrpěli	utrpět	k5eAaPmAgMnP
vážné	vážný	k2eAgFnPc4d1
omrzliny	omrzlina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
otevření	otevření	k1gNnSc2
nové	nový	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
se	se	k3xPyFc4
zdařil	zdařit	k5eAaPmAgInS
i	i	k9
první	první	k4xOgInSc1
přechod	přechod	k1gInSc1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
oblast	oblast	k1gFnSc4
Everestu	Everest	k1gInSc2
z	z	k7c2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
vesmírné	vesmírný	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
ISS	ISS	kA
</s>
<s>
1975	#num#	k4
–	–	k?
Boningtonova	Boningtonův	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
JZ	JZ	kA
stěnou	stěna	k1gFnSc7
</s>
<s>
O	o	k7c4
výstup	výstup	k1gInSc4
nejstrmější	strmý	k2eAgFnSc7d3
stěnou	stěna	k1gFnSc7
Everestu	Everest	k1gInSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
foto	foto	k1gNnSc4
v	v	k7c6
odstavci	odstavec	k1gInSc6
Historie	historie	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
pokoušelo	pokoušet	k5eAaImAgNnS
šest	šest	k4xCc4
výprav	výprava	k1gFnPc2
(	(	kIx(
<g/>
Japonci	Japonec	k1gMnPc1
<g/>
,	,	kIx,
Britové	Brit	k1gMnPc1
a	a	k8xC
dvě	dva	k4xCgFnPc4
mezinárodní	mezinárodní	k2eAgFnPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
zastavilo	zastavit	k5eAaPmAgNnS
počasí	počasí	k1gNnSc1
a	a	k8xC
technické	technický	k2eAgFnPc4d1
obtíže	obtíž	k1gFnPc4
skalní	skalní	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
ve	v	k7c6
výšce	výška	k1gFnSc6
8200	#num#	k4
<g/>
–	–	k?
<g/>
8500	#num#	k4
m	m	kA
nazývané	nazývaný	k2eAgFnSc2d1
Žlutý	žlutý	k2eAgInSc4d1
pás	pás	k1gInSc4
nebo	nebo	k8xC
Rockband	Rockband	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
zde	zde	k6eAd1
dvě	dva	k4xCgFnPc1
varianty	varianta	k1gFnPc1
<g/>
,	,	kIx,
prvovýstup	prvovýstup	k1gInSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
podařil	podařit	k5eAaPmAgInS
levou	levý	k2eAgFnSc7d1
variantou	varianta	k1gFnSc7
britské	britský	k2eAgFnSc3d1
expedici	expedice	k1gFnSc3
vedené	vedený	k2eAgFnSc3d1
Chrisem	Chris	k1gInSc7
Boningtonem	Bonington	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
hory	hora	k1gFnSc2
vystoupili	vystoupit	k5eAaPmAgMnP
Doug	Doug	k1gMnSc1
Scott	Scott	k1gMnSc1
a	a	k8xC
Dougal	Dougal	k1gMnSc1
Haston	Haston	k1gInSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
také	také	k9
Peter	Peter	k1gMnSc1
Boardman	Boardman	k1gMnSc1
s	s	k7c7
Šerpou	šerpa	k1gFnSc7
Pertembou	Pertemba	k1gFnSc7
a	a	k8xC
sólo	sólo	k2eAgMnSc1d1
Mick	Mick	k1gMnSc1
Burke	Burke	k1gNnSc2
(	(	kIx(
<g/>
ztratil	ztratit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
mlze	mlha	k1gFnSc6
při	při	k7c6
sestupu	sestup	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
cestu	cesta	k1gFnSc4
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
zopakovat	zopakovat	k5eAaPmF
třikrát	třikrát	k6eAd1
<g/>
:	:	kIx,
1988	#num#	k4
slovenský	slovenský	k2eAgInSc4d1
výstup	výstup	k1gInSc4
alpským	alpský	k2eAgInSc7d1
stylem	styl	k1gInSc7
<g/>
,	,	kIx,
1993	#num#	k4
Japonci	Japonec	k1gMnPc7
(	(	kIx(
<g/>
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
1995	#num#	k4
Korejci	Korejec	k1gMnPc7
expedičním	expediční	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
–	–	k?
jugoslávská	jugoslávský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
západním	západní	k2eAgNnSc6d1
hřebenem	hřeben	k1gInSc7
</s>
<s>
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
o	o	k7c4
něj	on	k3xPp3gNnSc4
pokusila	pokusit	k5eAaPmAgFnS
francouzská	francouzský	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
jejích	její	k3xOp3gMnPc2
členů	člen	k1gMnPc2
zahynulo	zahynout	k5eAaPmAgNnS
v	v	k7c6
lavině	lavina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvovýstup	prvovýstup	k1gInSc1
se	se	k3xPyFc4
zdařil	zdařit	k5eAaPmAgInS
slovinským	slovinský	k2eAgMnSc7d1
a	a	k8xC
chorvatským	chorvatský	k2eAgMnPc3d1
horolezcům	horolezec	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
otevřeli	otevřít	k5eAaPmAgMnP
technicky	technicky	k6eAd1
velmi	velmi	k6eAd1
obtížnou	obtížný	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
na	na	k7c6
většinou	většinou	k6eAd1
návětrné	návětrný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
vystoupili	vystoupit	k5eAaPmAgMnP
Andrej	Andrej	k1gMnSc1
Stremfelj	Stremfelj	k1gMnSc1
a	a	k8xC
Jernej	Jernej	k1gMnSc1
Zaplotnik	Zaplotnik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
družstvu	družstvo	k1gNnSc6
je	být	k5eAaImIp3nS
následovali	následovat	k5eAaImAgMnP
Stipe	Stip	k1gMnSc5
Božić	Božić	k1gMnSc5
<g/>
,	,	kIx,
Stane	stanout	k5eAaPmIp3nS
Belak	Belak	k1gMnSc1
a	a	k8xC
Šerpa	šerpa	k1gFnSc1
Ang	Ang	k1gMnSc2
Phu	Phu	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
zabil	zabít	k5eAaPmAgMnS
pádem	pád	k1gInSc7
při	při	k7c6
sestupu	sestup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
sestupu	sestup	k1gInSc3
obě	dva	k4xCgFnPc1
družstva	družstvo	k1gNnSc2
použila	použít	k5eAaPmAgFnS
snazší	snadný	k2eAgFnSc4d2
americkou	americký	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
Hornbeinovým	Hornbeinův	k2eAgInSc7d1
kuloárem	kuloár	k1gInSc7
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
jugoslávskou	jugoslávský	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
křižuje	křižovat	k5eAaImIp3nS
ve	v	k7c6
výšce	výška	k1gFnSc6
7300	#num#	k4
m.	m.	k?
Kompletní	kompletní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
západním	západní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
zopakovala	zopakovat	k5eAaPmAgFnS
bulharská	bulharský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
dalších	další	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
zvolila	zvolit	k5eAaPmAgFnS
kombinaci	kombinace	k1gFnSc4
jugoslávské	jugoslávský	k2eAgFnSc2d1
a	a	k8xC
americké	americký	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
a	a	k8xC
vyhnula	vyhnout	k5eAaPmAgFnS
se	se	k3xPyFc4
tím	ten	k3xDgInSc7
nejobtížnější	obtížný	k2eAgFnSc4d3
části	část	k1gFnPc4
hřebene	hřeben	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
–	–	k?
japonská	japonský	k2eAgFnSc1d1
diretissima	diretissima	k1gFnSc1
severní	severní	k2eAgFnSc1d1
stěnou	stěna	k1gFnSc7
</s>
<s>
Po	po	k7c6
otevření	otevření	k1gNnSc6
tibetských	tibetský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
vylezli	vylézt	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
velmi	velmi	k6eAd1
přímou	přímý	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
stěně	stěna	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
v	v	k7c6
8200	#num#	k4
m	m	kA
napojuje	napojovat	k5eAaImIp3nS
do	do	k7c2
americké	americký	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
v	v	k7c6
Hornbeinově	Hornbeinův	k2eAgInSc6d1
kuloáru	kuloár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
stáli	stát	k5eAaImAgMnP
Takaši	Takaš	k1gMnPc1
Ozaki	Ozaki	k1gNnPc2
a	a	k8xC
Tsuneo	Tsuneo	k6eAd1
Šigehiro	Šigehiro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trasu	tras	k1gInSc2
zopakovali	zopakovat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
Švýcaři	Švýcar	k1gMnPc1
Erhard	Erharda	k1gFnPc2
Loretan	Loretan	k1gInSc1
a	a	k8xC
Jean	Jean	k1gMnSc1
Troillet	Troillet	k1gInSc4
alpským	alpský	k2eAgInSc7d1
stylem	styl	k1gInSc7
za	za	k7c4
pouhých	pouhý	k2eAgFnPc2d1
36	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
později	pozdě	k6eAd2
několik	několik	k4yIc4
málo	málo	k6eAd1
dalších	další	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
–	–	k?
polská	polský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
jižním	jižní	k2eAgNnSc6d1
pilířem	pilíř	k1gInSc7
</s>
<s>
Cesta	cesta	k1gFnSc1
sleduje	sledovat	k5eAaImIp3nS
pravý	pravý	k2eAgInSc4d1
okraj	okraj	k1gInSc4
jihozápadní	jihozápadní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
8100	#num#	k4
m	m	kA
překonává	překonávat	k5eAaImIp3nS
obtížný	obtížný	k2eAgInSc4d1
skalní	skalní	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
expedice	expedice	k1gFnSc2
vedené	vedený	k2eAgFnSc2d1
Andrzejem	Andrzej	k1gInSc7
Zawadou	Zawada	k1gFnSc7
vystoupili	vystoupit	k5eAaPmAgMnP
na	na	k7c4
vrchol	vrchol	k1gInSc4
Andrzej	Andrzej	k1gFnSc2
Czok	Czoka	k1gFnPc2
a	a	k8xC
Jerzy	Jerza	k1gFnSc2
Kukuczka	Kukuczka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestu	cesta	k1gFnSc4
zopakovala	zopakovat	k5eAaPmAgFnS
československá	československý	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
a	a	k8xC
později	pozdě	k6eAd2
pár	pár	k4xCyI
dalších	další	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
–	–	k?
Messnerova	Messnerův	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
</s>
<s>
Reinhold	Reinhold	k1gMnSc1
Messner	Messner	k1gMnSc1
během	během	k7c2
svého	svůj	k3xOyFgInSc2
třídenního	třídenní	k2eAgInSc2d1
sólovýstupu	sólovýstup	k1gInSc2
opustil	opustit	k5eAaPmAgMnS
v	v	k7c6
7200	#num#	k4
m	m	kA
klasickou	klasický	k2eAgFnSc4d1
severní	severní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
a	a	k8xC
prostoupil	prostoupit	k5eAaPmAgMnS
severní	severní	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
a	a	k8xC
Nortonovým	Nortonový	k2eAgInSc7d1
kuloárem	kuloár	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trasa	trasa	k1gFnSc1
není	být	k5eNaImIp3nS
často	často	k6eAd1
lezena	lezen	k2eAgFnSc1d1
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
lezců	lezec	k1gMnPc2
sleduje	sledovat	k5eAaImIp3nS
raději	rád	k6eAd2
delší	dlouhý	k2eAgMnSc1d2
<g/>
,	,	kIx,
ale	ale	k8xC
snazší	snadný	k2eAgFnSc4d2
klasickou	klasický	k2eAgFnSc4d1
severní	severní	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
–	–	k?
sovětská	sovětský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
JZ	JZ	kA
pilířem	pilíř	k1gInSc7
</s>
<s>
Horolezci	horolezec	k1gMnPc1
z	z	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Ukrajiny	Ukrajina	k1gFnSc2
a	a	k8xC
Kazachstánu	Kazachstán	k1gInSc2
otevřeli	otevřít	k5eAaPmAgMnP
cestu	cesta	k1gFnSc4
vlevo	vlevo	k6eAd1
od	od	k7c2
Boningtonovy	Boningtonův	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
velmi	velmi	k6eAd1
obtížným	obtížný	k2eAgInSc7d1
skalním	skalní	k2eAgInSc7d1
pilířem	pilíř	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
8500	#num#	k4
m	m	kA
se	se	k3xPyFc4
výstup	výstup	k1gInSc1
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
západní	západní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
čtyřech	čtyři	k4xCgNnPc6
družstvech	družstvo	k1gNnPc6
vystoupilo	vystoupit	k5eAaPmAgNnS
na	na	k7c4
vrchol	vrchol	k1gInSc4
devět	devět	k4xCc1
horolezců	horolezec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výstup	výstup	k1gInSc1
nebyl	být	k5eNaImAgInS
nikdy	nikdy	k6eAd1
zopakován	zopakovat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
–	–	k?
americká	americký	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
V	v	k7c6
stěnou	stěna	k1gFnSc7
</s>
<s>
Prvnímu	první	k4xOgMnSc3
výstupu	výstup	k1gInSc2
východní	východní	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
předcházel	předcházet	k5eAaImAgInS
jeden	jeden	k4xCgInSc4
také	také	k6eAd1
americký	americký	k2eAgInSc4d1
pokus	pokus	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trasa	trasa	k1gFnSc1
překonává	překonávat	k5eAaImIp3nS
převýšení	převýšení	k1gNnSc4
3500	#num#	k4
m	m	kA
<g/>
,	,	kIx,
ve	v	k7c6
spodní	spodní	k2eAgFnSc6d1
části	část	k1gFnSc6
obtížné	obtížný	k2eAgFnSc2d1
skalní	skalní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
<g/>
,	,	kIx,
výše	vysoce	k6eAd2
lavinami	lavina	k1gFnPc7
ohrožovaný	ohrožovaný	k2eAgInSc4d1
pilíř	pilíř	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
vystoupily	vystoupit	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc4
trojice	trojice	k1gFnPc4
horolezců	horolezec	k1gMnPc2
–	–	k?
Kim	Kim	k1gFnSc2
Momb	Momb	k1gMnSc1
<g/>
,	,	kIx,
Lou	Lou	k1gMnSc1
Reichardt	Reichardt	k1gMnSc1
<g/>
,	,	kIx,
Carlos	Carlos	k1gMnSc1
Buhler	Buhler	k1gMnSc1
a	a	k8xC
George	Georg	k1gMnSc4
Lowe	Low	k1gMnSc4
<g/>
,	,	kIx,
Dan	Dan	k1gMnSc1
Reid	Reid	k1gMnSc1
<g/>
,	,	kIx,
Jay	Jay	k1gMnSc1
Cassell	Cassell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výstup	výstup	k1gInSc1
nebyl	být	k5eNaImAgInS
nikdy	nikdy	k6eAd1
zopakován	zopakovat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
–	–	k?
australská	australský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
Velkým	velký	k2eAgInSc7d1
(	(	kIx(
<g/>
Nortonovým	Nortonový	k2eAgInSc7d1
<g/>
)	)	kIx)
kuloárem	kuloár	k1gInSc7
v	v	k7c6
severní	severní	k2eAgFnSc6d1
stěně	stěna	k1gFnSc6
</s>
<s>
Trasu	trasa	k1gFnSc4
prostoupila	prostoupit	k5eAaPmAgFnS
malá	malý	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
několika	několik	k4yIc2
přátel	přítel	k1gMnPc2
bez	bez	k7c2
podpory	podpora	k1gFnSc2
šerpských	šerpský	k2eAgInPc2d1
nosičů	nosič	k1gInPc2
a	a	k8xC
bez	bez	k7c2
kyslíkových	kyslíkový	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
vystoupili	vystoupit	k5eAaPmAgMnP
Tim	Tim	k?
McCartney-Snape	McCartney-Snap	k1gInSc5
a	a	k8xC
Greg	Greg	k1gMnSc1
Mortimer	Mortimer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnSc7
spolulezec	spolulezec	k1gMnSc1
Henderson	Henderson	k1gMnSc1
musel	muset	k5eAaImAgMnS
vzdát	vzdát	k5eAaPmF
50	#num#	k4
m	m	kA
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
–	–	k?
pilíř	pilíř	k1gInSc1
spadající	spadající	k2eAgInSc1d1
ze	z	k7c2
západního	západní	k2eAgNnSc2d1
ramene	rameno	k1gNnSc2
Everestu	Everest	k1gInSc2
k	k	k7c3
severu	sever	k1gInSc3
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
zlezla	zlézt	k5eAaPmAgFnS
poprvé	poprvé	k6eAd1
pilíř	pilíř	k1gInSc4
spadající	spadající	k2eAgInPc4d1
ze	z	k7c2
západního	západní	k2eAgNnSc2d1
ramene	rameno	k1gNnSc2
Everestu	Everest	k1gInSc2
k	k	k7c3
severu	sever	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
7250	#num#	k4
m	m	kA
se	se	k3xPyFc4
trasa	trasa	k1gFnSc1
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
americkou	americký	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
hory	hora	k1gFnSc2
vystoupili	vystoupit	k5eAaPmAgMnP
Mike	Mike	k1gFnSc4
Congdon	Congdona	k1gFnPc2
a	a	k8xC
horolezkyně	horolezkyně	k1gFnSc1
Sharon	Sharon	k1gInSc1
Woodová	Woodová	k1gFnSc1
(	(	kIx(
<g/>
Wood	Wood	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
jediná	jediný	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zlezla	zlézt	k5eAaPmAgFnS
Everest	Everest	k1gInSc4
novou	nový	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
–	–	k?
východní	východní	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
vlevo	vlevo	k6eAd1
ve	v	k7c6
spádnici	spádnice	k1gFnSc6
Jižního	jižní	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
</s>
<s>
Australsko-britská	australsko-britský	k2eAgFnSc1d1
malá	malý	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
otevřela	otevřít	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
východní	východní	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
vlevo	vlevo	k6eAd1
ve	v	k7c6
spádnici	spádnice	k1gFnSc6
Jižního	jižní	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
trasa	trasa	k1gFnSc1
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
jihovýchodní	jihovýchodní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholu	vrchol	k1gInSc2
dosáhl	dosáhnout	k5eAaPmAgMnS
sólo	sólo	k2eAgMnSc1d1
Angličan	Angličan	k1gMnSc1
Stephen	Stephna	k1gFnPc2
Venables	Venables	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
cestu	cesta	k1gFnSc4
zopakovali	zopakovat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
horolezci	horolezec	k1gMnPc1
z	z	k7c2
Chile	Chile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
–	–	k?
severovýchodní	severovýchodní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
</s>
<s>
Trasu	trasa	k1gFnSc4
celým	celý	k2eAgInSc7d1
severovýchodním	severovýchodní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
přes	přes	k7c4
Tři	tři	k4xCgFnPc4
věže	věž	k1gFnPc4
poprvé	poprvé	k6eAd1
zkoušeli	zkoušet	k5eAaImAgMnP
Britové	Brit	k1gMnPc1
roku	rok	k1gInSc2
1982	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dosáhli	dosáhnout	k5eAaPmAgMnP
8200	#num#	k4
m.	m.	k?
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
dva	dva	k4xCgMnPc1
Britové	Brit	k1gMnPc1
prostoupili	prostoupit	k5eAaPmAgMnP
hřeben	hřeben	k1gInSc4
až	až	k9
do	do	k7c2
8450	#num#	k4
m	m	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
napojili	napojit	k5eAaPmAgMnP
do	do	k7c2
klasické	klasický	k2eAgFnSc2d1
severní	severní	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholu	vrchol	k1gInSc2
ale	ale	k8xC
nedosáhli	dosáhnout	k5eNaPmAgMnP
<g/>
,	,	kIx,
sestoupili	sestoupit	k5eAaPmAgMnP
do	do	k7c2
Severního	severní	k2eAgNnSc2d1
sedla	sedlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc4d1
kompletní	kompletní	k2eAgInSc4d1
výstup	výstup	k1gInSc4
severovýchodním	severovýchodní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
dokončila	dokončit	k5eAaPmAgFnS
velká	velký	k2eAgFnSc1d1
japonská	japonský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
hřeben	hřeben	k1gInSc4
zabezpečila	zabezpečit	k5eAaPmAgFnS
4	#num#	k4
km	km	kA
lan	lano	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgMnPc1
Japonci	Japonec	k1gMnPc1
a	a	k8xC
čtyři	čtyři	k4xCgMnPc1
Šerpové	Šerp	k1gMnPc1
vystoupili	vystoupit	k5eAaPmAgMnP
na	na	k7c4
vrchol	vrchol	k1gInSc4
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
–	–	k?
kazašská	kazašský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
severovýchodní	severovýchodní	k2eAgFnSc1d1
stěnou	stěna	k1gFnSc7
</s>
<s>
Kazaši	Kazach	k1gMnPc1
stoupali	stoupat	k5eAaImAgMnP
stěnou	stěna	k1gFnSc7
mezi	mezi	k7c7
severním	severní	k2eAgInSc7d1
a	a	k8xC
severovýchodním	severovýchodní	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
do	do	k7c2
zhruba	zhruba	k6eAd1
8000	#num#	k4
m	m	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
napojili	napojit	k5eAaPmAgMnP
na	na	k7c4
klasickou	klasický	k2eAgFnSc4d1
severní	severní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
–	–	k?
severní	severní	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
mezi	mezi	k7c7
Hornbeinovým	Hornbeinův	k2eAgInSc7d1
a	a	k8xC
velkým	velký	k2eAgInSc7d1
kuloárem	kuloár	k1gInSc7
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
otevřela	otevřít	k5eAaPmAgFnS
obtížnou	obtížný	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
severní	severní	k2eAgFnSc7d1
stěnou	stěna	k1gFnSc7
mezi	mezi	k7c7
Hornbeinovým	Hornbeinův	k2eAgInSc7d1
a	a	k8xC
velkým	velký	k2eAgInSc7d1
kuloárem	kuloár	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osm	osm	k4xCc1
horolezců	horolezec	k1gMnPc2
ve	v	k7c6
třech	tři	k4xCgNnPc6
družstvech	družstvo	k1gNnPc6
dosáhlo	dosáhnout	k5eAaPmAgNnS
vrcholu	vrchol	k1gInSc2
hory	hora	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výstup	výstup	k1gInSc1
překonává	překonávat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
náročné	náročný	k2eAgFnPc4d1
skalní	skalní	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
v	v	k7c4
7200	#num#	k4
a	a	k8xC
8500	#num#	k4
m.	m.	k?
</s>
<s>
2009	#num#	k4
–	–	k?
korejská	korejský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
jihozápadní	jihozápadní	k2eAgFnSc1d1
stěnou	stěna	k1gFnSc7
</s>
<s>
Jihokorejská	jihokorejský	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
dokončila	dokončit	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
v	v	k7c6
levé	levý	k2eAgFnSc6d1
části	část	k1gFnSc6
jihozápadní	jihozápadní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
výpravy	výprava	k1gFnSc2
Pak	pak	k6eAd1
Jong-sok	Jong-sok	k1gInSc4
s	s	k7c7
dalšími	další	k2eAgInPc7d1
třemi	tři	k4xCgInPc7
horolezci	horolezec	k1gMnPc7
vystoupil	vystoupit	k5eAaPmAgMnS
na	na	k7c4
vrchol	vrchol	k1gInSc4
hory	hora	k1gFnPc4
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
sestoupili	sestoupit	k5eAaPmAgMnP
klasickou	klasický	k2eAgFnSc7d1
jižní	jižní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstup	výstup	k1gInSc1
navazoval	navazovat	k5eAaImAgInS
na	na	k7c4
předchozí	předchozí	k2eAgInPc4d1
neúspěšné	úspěšný	k2eNgInPc4d1
pokusy	pokus	k1gInPc4
v	v	k7c6
letech	léto	k1gNnPc6
2007	#num#	k4
a	a	k8xC
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Celkem	celkem	k6eAd1
existuje	existovat	k5eAaImIp3nS
17	#num#	k4
cest	cesta	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
variant	varianta	k1gFnPc2
k	k	k7c3
vrcholu	vrchol	k1gInSc3
Everestu	Everest	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžně	běžně	k6eAd1
používané	používaný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
jen	jen	k9
dvě	dva	k4xCgFnPc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
šest	šest	k4xCc1
cest	cesta	k1gFnPc2
nebylo	být	k5eNaImAgNnS
od	od	k7c2
prvovýstupu	prvovýstup	k1gInSc2
nikdy	nikdy	k6eAd1
zopakováno	zopakován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
dva	dva	k4xCgInPc1
prvovýstupy	prvovýstup	k1gInPc1
byly	být	k5eAaImAgInP
podniknuty	podniknout	k5eAaPmNgInP
bez	bez	k7c2
použití	použití	k1gNnSc2
kyslíkových	kyslíkový	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
(	(	kIx(
<g/>
Velký	velký	k2eAgInSc1d1
kuloár	kuloár	k1gInSc1
1984	#num#	k4
a	a	k8xC
východní	východní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
Everestu	Everest	k1gInSc2
stálo	stát	k5eAaImAgNnS
už	už	k9
několik	několik	k4yIc1
tisíc	tisíc	k4xCgInSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
bez	bez	k7c2
kyslíkového	kyslíkový	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
zhruba	zhruba	k6eAd1
jen	jen	k9
100	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
výstupu	výstup	k1gInSc2
alpským	alpský	k2eAgInSc7d1
stylem	styl	k1gInSc7
(	(	kIx(
<g/>
bez	bez	k7c2
podpory	podpora	k1gFnSc2
Šerpů	Šerp	k1gInPc2
<g/>
,	,	kIx,
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
předchozího	předchozí	k2eAgNnSc2d1
zajištění	zajištění	k1gNnSc2
trasy	trasa	k1gFnSc2
lany	lano	k1gNnPc7
<g/>
)	)	kIx)
jen	jen	k9
tři	tři	k4xCgMnPc4
(	(	kIx(
<g/>
Messner	Messner	k1gInSc1
1980	#num#	k4
<g/>
,	,	kIx,
Loretan	Loretan	k1gInSc1
a	a	k8xC
Troillet	Troillet	k1gInSc1
1986	#num#	k4
<g/>
,	,	kIx,
Just	just	k6eAd1
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Možnosti	možnost	k1gFnPc1
nových	nový	k2eAgInPc2d1
prvovýstupů	prvovýstup	k1gInPc2
na	na	k7c4
Everest	Everest	k1gInSc4
jsou	být	k5eAaImIp3nP
omezené	omezený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Logickou	logický	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
Fantasy	fantas	k1gInPc7
Ridge	Ridge	k1gNnSc2
v	v	k7c6
pravé	pravý	k2eAgFnSc6d1
části	část	k1gFnSc6
východní	východní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
severovýchodní	severovýchodní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilíř	pilíř	k1gInSc1
je	být	k5eAaImIp3nS
ohrožován	ohrožovat	k5eAaImNgInS
lavinami	lavina	k1gFnPc7
a	a	k8xC
napojuje	napojovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
severovýchodní	severovýchodní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
teprve	teprve	k6eAd1
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
nejobtížnějším	obtížný	k2eAgNnSc7d3
místem	místo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
nejdelší	dlouhý	k2eAgFnSc4d3
výstupovou	výstupový	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
s	s	k7c7
největším	veliký	k2eAgNnSc7d3
převýšením	převýšení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
možnou	možný	k2eAgFnSc7d1
trasou	trasa	k1gFnSc7
je	být	k5eAaImIp3nS
pravá	pravý	k2eAgFnSc1d1
část	část	k1gFnSc1
jihozápadní	jihozápadní	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
vpravo	vpravo	k6eAd1
od	od	k7c2
Boningtonovy	Boningtonův	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
<g/>
,	,	kIx,
kudy	kudy	k6eAd1
se	se	k3xPyFc4
pokoušely	pokoušet	k5eAaImAgFnP
prostoupit	prostoupit	k5eAaPmF
tři	tři	k4xCgFnPc1
expedice	expedice	k1gFnPc1
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
a	a	k8xC
také	také	k9
československá	československý	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
dosažený	dosažený	k2eAgInSc1d1
bod	bod	k1gInSc1
na	na	k7c6
této	tento	k3xDgFnSc6
trase	trasa	k1gFnSc6
je	být	k5eAaImIp3nS
8350	#num#	k4
m	m	kA
(	(	kIx(
<g/>
Dougal	Dougal	k1gInSc1
Haston	Haston	k1gInSc1
a	a	k8xC
Don	Don	k1gMnSc1
Whillans	Whillans	k1gInSc4
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výstupy	výstup	k1gInPc1
českých	český	k2eAgMnPc2d1
horolezců	horolezec	k1gMnPc2
</s>
<s>
RokJménoCestaZajímavost	RokJménoCestaZajímavost	k1gFnSc1
<g/>
,	,	kIx,
poznámka	poznámka	k1gFnSc1
</s>
<s>
1991	#num#	k4
</s>
<s>
Leopold	Leopold	k1gMnSc1
Sulovský	Sulovský	k1gMnSc1
</s>
<s>
Nortonův	Nortonův	k2eAgInSc1d1
kuloár	kuloár	k1gInSc1
<g/>
,	,	kIx,
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
<g/>
,	,	kIx,
prvovýstup	prvovýstup	k1gInSc1
</s>
<s>
italsko-česká	italsko-český	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
čekání	čekání	k1gNnSc6
na	na	k7c4
Battistu	Battista	k1gMnSc4
Bonaliho	Bonali	k1gMnSc4
na	na	k7c6
konci	konec	k1gInSc6
kuloáru	kuloár	k1gInSc2
si	se	k3xPyFc3
zakouřil	zakouřit	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1996	#num#	k4
</s>
<s>
Josef	Josef	k1gMnSc1
Nežerka	Nežerka	k1gFnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
</s>
<s>
norsko-italsko-česká	norsko-italsko-český	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1998	#num#	k4
</s>
<s>
Radek	Radek	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Nosek	Nosek	k1gMnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
</s>
<s>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1999	#num#	k4
</s>
<s>
Renata	Renata	k1gFnSc1
Chlumská	chlumský	k2eAgFnSc1d1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
jihu	jih	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
</s>
<s>
Švédka	Švédka	k1gFnSc1
českého	český	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2002	#num#	k4
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Caban	Caban	k1gMnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
</s>
<s>
Druhý	druhý	k4xOgMnSc1
na	na	k7c6
světě	svět	k1gInSc6
zdolal	zdolat	k5eAaPmAgInS
projekt	projekt	k1gInSc1
Koruna	koruna	k1gFnSc1
planety	planeta	k1gFnSc2
bez	bez	k7c2
použití	použití	k1gNnSc2
kyslíkového	kyslíkový	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
„	„	k?
<g/>
podivně	podivně	k6eAd1
<g/>
“	“	k?
prezentovaný	prezentovaný	k2eAgInSc4d1
výstup	výstup	k1gInSc4
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
odborníky	odborník	k1gMnPc4
kritizováno	kritizován	k2eAgNnSc1d1
i	i	k8xC
přespání	přespání	k1gNnSc1
v	v	k7c6
cizím	cizí	k2eAgInSc6d1
stanu	stan	k1gInSc6
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2005	#num#	k4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Trčala	Trčal	k1gMnSc2
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
jihu	jih	k1gInSc2
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
výstupu	výstup	k1gInSc2
nejmladší	mladý	k2eAgMnSc1d3
Čech	Čech	k1gMnSc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sjezd	sjezd	k1gInSc1
z	z	k7c2
vrcholu	vrchol	k1gInSc2
částečně	částečně	k6eAd1
na	na	k7c6
lyžích	lyže	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstup	výstup	k1gInSc1
zpochybněn	zpochybněn	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
soudně	soudně	k6eAd1
uznán	uznat	k5eAaPmNgInS
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
</s>
<s>
David	David	k1gMnSc1
Fojtík	Fojtík	k1gMnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
</s>
<s>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2007	#num#	k4
</s>
<s>
Klára	Klára	k1gFnSc1
Poláčková	Poláčková	k1gFnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
</s>
<s>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2007	#num#	k4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Bém	Bém	k1gMnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
jihu	jih	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
</s>
<s>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
</s>
<s>
Karel	Karel	k1gMnSc1
Mašek	Mašek	k1gMnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
jihu	jih	k1gInSc2
</s>
<s>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2012	#num#	k4
</s>
<s>
Vít	Vít	k1gMnSc1
Morava	Morava	k1gFnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
jihu	jih	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2018	#num#	k4
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Hrad	hrad	k1gInSc4
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
jihu	jih	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
–	–	k?
přesně	přesně	k6eAd1
v	v	k7c4
den	den	k1gInSc4
svých	svůj	k3xOyFgMnPc2
40	#num#	k4
<g/>
.	.	kIx.
narozenin	narozeniny	k1gFnPc2
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2018	#num#	k4
</s>
<s>
Ivo	Ivo	k1gMnSc1
Grabműller	Grabműller	k1gMnSc1
</s>
<s>
normální	normální	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
jihu	jih	k1gInSc2
<g/>
;	;	kIx,
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
</s>
<s>
Projekt	projekt	k1gInSc1
Seven	Seven	k2eAgInSc4d1
Summits	Summits	k1gInSc4
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rekordy	rekord	k1gInPc4
a	a	k8xC
zajímavosti	zajímavost	k1gFnPc4
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
jednu	jeden	k4xCgFnSc4
ze	z	k7c2
14	#num#	k4
osmitisícovek	osmitisícovka	k1gFnPc2
a	a	k8xC
jako	jako	k9
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Asie	Asie	k1gFnSc2
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
hor	hora	k1gFnPc2
Koruny	koruna	k1gFnSc2
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
australský	australský	k2eAgMnSc1d1
horolezec	horolezec	k1gMnSc1
Tim	Tim	k?
McCartney-Snape	McCartney-Snap	k1gInSc5
dosáhl	dosáhnout	k5eAaPmAgMnS
jako	jako	k9
první	první	k4xOgMnSc1
člověk	člověk	k1gMnSc1
vrcholu	vrchol	k1gInSc2
zdoláním	zdolání	k1gNnSc7
celé	celý	k2eAgFnSc2d1
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
od	od	k7c2
0	#num#	k4
m	m	kA
až	až	k6eAd1
po	po	k7c4
samotný	samotný	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
Everestu	Everest	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestu	cesta	k1gFnSc4
absolvoval	absolvovat	k5eAaPmAgInS
pěšky	pěšky	k6eAd1
od	od	k7c2
moře	moře	k1gNnSc2
v	v	k7c6
Bengálském	bengálský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
Mount	Mounta	k1gFnPc2
Everestu	Everest	k1gInSc2
z	z	k7c2
Kalar	Kalara	k1gFnPc2
Patar	Patara	k1gFnPc2
</s>
<s>
Himálaj	Himálaj	k1gFnSc1
z	z	k7c2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
vesmírné	vesmírný	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
</s>
<s>
Masív	masív	k1gInSc1
Everestu	Everest	k1gInSc2
z	z	k7c2
letadla	letadlo	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
86	#num#	k4
centimetrů	centimetr	k1gInPc2
vyšší	vysoký	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
uvádělo	uvádět	k5eAaImAgNnS
<g/>
↑	↑	k?
Qomolangma	Qomolangma	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNESCO	Unesco	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BOHÁČ	Boháč	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc4
českých	český	k2eAgInPc2d1
exonym	exonym	k1gInSc4
<g/>
:	:	kIx,
standardizované	standardizovaný	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
,	,	kIx,
varianty	varianta	k1gFnSc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rozš	rozš	k5eAaPmIp2nS
<g/>
.	.	kIx.
a	a	k8xC
aktualiz	aktualiz	k1gInSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgMnSc1d1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
212	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Geografické	geografický	k2eAgInPc1d1
názvoslovné	názvoslovný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
OSN	OSN	kA
-	-	kIx~
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88197	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardizované	standardizovaný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
:	:	kIx,
Everest	Everest	k1gInSc1
<g/>
.	.	kIx.
↑	↑	k?
Tak	tak	k6eAd1
vyslovoval	vyslovovat	k5eAaImAgMnS
Everest	Everest	k1gInSc4
své	svůj	k3xOyFgMnPc4
vlastní	vlastnit	k5eAaImIp3nS
příjmení	příjmení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MESSNER	MESSNER	kA
<g/>
,	,	kIx,
Reinhold	Reinhold	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moje	můj	k3xOp1gFnPc1
osmitisícovky	osmitisícovka	k1gFnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Tatran	Tatran	k1gInSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
248	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
222	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
425	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
english	english	k1gInSc1
<g/>
.	.	kIx.
<g/>
people	people	k6eAd1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
cn	cn	k?
<g/>
↑	↑	k?
Nepal	pálit	k5eNaImRp2nS
and	and	k?
China	China	k1gFnSc1
agree	agreat	k5eAaPmIp3nS
on	on	k3xPp3gMnSc1
Mount	Mount	k1gMnSc1
Everest	Everest	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
height	heighta	k1gFnPc2
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
8	#num#	k4
April	April	k1gInSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
22	#num#	k4
August	August	k1gMnSc1
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
a	a	k8xC
Nepál	Nepál	k1gInSc1
se	se	k3xPyFc4
shodly	shodnout	k5eAaPmAgFnP,k5eAaBmAgFnP
na	na	k7c6
výšce	výška	k1gFnSc6
Mount	Mounta	k1gFnPc2
Everestu	Everest	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-04-08	2010-04-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
5003	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DANĚK	Daněk	k1gMnSc1
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemětřesení	zemětřesení	k1gNnSc1
v	v	k7c6
Nepálu	Nepál	k1gInSc6
nepřežilo	přežít	k5eNaPmAgNnS
nejméně	málo	k6eAd3
2500	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
zasáhla	zasáhnout	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
otřesů	otřes	k1gInPc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2015-04-26	2015-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čína	Čína	k1gFnSc1
a	a	k8xC
Nepál	Nepál	k1gInSc1
přeměřily	přeměřit	k5eAaPmAgInP
Mount	Mount	k1gMnSc1
Everest	Everest	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
tvrdilo	tvrdit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2020-12-08	2020-12-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
např.	např.	kA
Reinhold	Reinhold	k1gMnSc1
Messner	Messner	k1gMnSc1
<g/>
:	:	kIx,
Mount	Mount	k1gMnSc1
Everest	Everest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpravy	výprava	k1gFnPc4
na	na	k7c4
hranice	hranice	k1gFnPc4
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
;	;	kIx,
str	str	kA
<g/>
.	.	kIx.
47	#num#	k4
<g/>
↑	↑	k?
Bez	bez	k7c2
kyslíku	kyslík	k1gInSc2
na	na	k7c6
Everestu	Everest	k1gInSc6
<g/>
↑	↑	k?
Reinhold	Reinhold	k1gMnSc1
Messner	Messner	k1gMnSc1
<g/>
:	:	kIx,
Mount	Mount	k1gMnSc1
Everest	Everest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpravy	výprava	k1gFnPc4
na	na	k7c4
hranice	hranice	k1gFnPc4
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
;	;	kIx,
str	str	kA
<g/>
.	.	kIx.
180	#num#	k4
<g/>
–	–	k?
<g/>
195	#num#	k4
<g/>
;	;	kIx,
pozn	pozn	kA
<g/>
.	.	kIx.
Messner	Messner	k1gMnSc1
píše	psát	k5eAaImIp3nS
o	o	k7c4
5	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
nezabývá	zabývat	k5eNaImIp3nS
indickou	indický	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
postupující	postupující	k2eAgInSc4d1
jinudy	jinudy	k6eAd1
<g/>
1	#num#	k4
2	#num#	k4
Smrt	smrt	k1gFnSc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
–	–	k?
Everest	Everest	k1gInSc4
pokrývají	pokrývat	k5eAaImIp3nP
mrtvoly	mrtvola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bushcraft	Bushcraft	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12.12	12.12	k4
<g/>
.2013	.2013	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
8000	#num#	k4
<g/>
ers	ers	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
Fatalities	Fatalities	k1gMnSc1
–	–	k?
Everest	Everest	k1gInSc1
<g/>
↑	↑	k?
horydoly	horydola	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Na	na	k7c6
Everestu	Everest	k1gInSc6
zemřel	zemřít	k5eAaPmAgInS
Chrzaszcz	Chrzaszcz	k1gInSc1
<g/>
↑	↑	k?
ECHO	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
,	,	kIx,
JHR	JHR	kA
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
Everestu	Everest	k1gInSc2
je	být	k5eAaImIp3nS
zacpaný	zacpaný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
D	D	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fronty	fronta	k1gFnPc4
zavinily	zavinit	k5eAaPmAgInP
smrt	smrt	k1gFnSc4
horolezců	horolezec	k1gMnPc2
vyčerpáním	vyčerpání	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ECHO	echo	k1gNnSc4
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
A.S.	A.S.	k1gFnSc2
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
World	World	k1gMnSc1
Record	Record	k1gMnSc1
<g/>
:	:	kIx,
Apa	Apa	k1gMnSc1
Sherpa	Sherp	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Everest	Everest	k1gInSc1
summit	summit	k1gInSc4
no	no	k9
21	#num#	k4
<g/>
.	.	kIx.
explorersweb	explorerswba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ECHO	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepálský	nepálský	k2eAgMnSc1d1
horal	horal	k1gMnSc1
překonal	překonat	k5eAaPmAgMnS
vlastní	vlastní	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
pokořil	pokořit	k5eAaPmAgInS
už	už	k6eAd1
čtyřiadvacetkrát	čtyřiadvacetkrát	k6eAd1
<g/>
.	.	kIx.
echo	echo	k1gNnSc4
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ECHO	echo	k1gNnSc4
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
A.S.	A.S.	k1gFnSc2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Everest	Everest	k1gInSc1
<g/>
,	,	kIx,
the	the	k?
crowd	crowd	k1gInSc1
and	and	k?
the	the	k?
race	rac	k1gFnSc2
to	ten	k3xDgNnSc1
nowhere	nowhrat	k5eAaPmIp3nS
<g/>
↑	↑	k?
http://www.travelmag.cz/clanek.php?id=126%5B%5D1	http://www.travelmag.cz/clanek.php?id=126%5B%5D1	k4
2	#num#	k4
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.humpolak.cz	www.humpolak.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.k2news.com/mmani.m	http://www.k2news.com/mmani.m	k1gInSc1
<g/>
↑	↑	k?
http://www.casopisdobrodruh.cz/archiv/0498.pdf	http://www.casopisdobrodruh.cz/archiv/0498.pdf	k1gInSc1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.humpolak.cz	www.humpolak.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.national-geographic.cz	www.national-geographic.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
↑	↑	k?
http://martinminarik.cz/sest-himalajskych-sezon.html	http://martinminarik.cz/sest-himalajskych-sezon.html	k1gInSc1
<g/>
↑	↑	k?
Pavlovi	Pavel	k1gMnSc6
(	(	kIx(
<g/>
38	#num#	k4
<g/>
)	)	kIx)
nevěřili	věřit	k5eNaImAgMnP
výstup	výstup	k1gInSc4
na	na	k7c4
Everest	Everest	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Po	po	k7c6
11	#num#	k4
letech	léto	k1gNnPc6
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
potvrdil	potvrdit	k5eAaPmAgInS
soud	soud	k1gInSc1
|	|	kIx~
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.blesk.cz	www.blesk.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://sport.idnes.cz/fojtik-stanul-na-mount-everestu-dal-/sporty.asp?c=A060518_212406_sporty_no	http://sport.idnes.cz/fojtik-stanul-na-mount-everestu-dal-/sporty.asp?c=A060518_212406_sporty_no	k6eAd1
<g/>
↑	↑	k?
http://www.horyinfo.cz/view.php?nazevclanku=mount-everest-davida-fojtika&	http://www.horyinfo.cz/view.php?nazevclanku=mount-everest-davida-fojtika&	k?
<g/>
↑	↑	k?
http://www.ceskatelevize.cz/ivysilani/10111287948-mount-everest-branky-body-vteriny-8850/?streamtype=WM2	http://www.ceskatelevize.cz/ivysilani/10111287948-mount-everest-branky-body-vteriny-8850/?streamtype=WM2	k4
<g/>
↑	↑	k?
První	první	k4xOgFnSc1
Češka	Češka	k1gFnSc1
zdolala	zdolat	k5eAaPmAgFnS
Mount	Mount	k1gInSc4
Everest	Everest	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-05-16	2007-05-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://zpravy.idnes.cz/praha.asp?o=0&	http://zpravy.idnes.cz/praha.asp?o=0&	k?
<g/>
↑	↑	k?
http://www.mountainguides.com/everest-south10.shtml	http://www.mountainguides.com/everest-south10.shtml	k1gInSc1
<g/>
↑	↑	k?
Everest	Everest	k1gInSc4
Eco	Eco	k1gFnSc2
Expedition	Expedition	k1gInSc1
2012	#num#	k4
<g/>
↑	↑	k?
http://www.ckexpedice.cz/miroslav_hrad	http://www.ckexpedice.cz/miroslav_hrad	k1gInSc1
<g/>
↑	↑	k?
Ckexpedice	Ckexpedice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
fotky	fotka	k1gFnSc2
z	z	k7c2
výstupu	výstup	k1gInSc2
2018	#num#	k4
<g/>
↑	↑	k?
NYČ	NYČ	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
‚	‚	k?
<g/>
Nejtěžší	těžký	k2eAgFnSc1d3
<g/>
?	?	kIx.
</s>
<s desamb="1">
Určitě	určitě	k6eAd1
Everest	Everest	k1gInSc4
<g/>
.	.	kIx.
<g/>
‘	‘	k?
Ivo	Ivo	k1gMnSc1
Grabmüller	Grabmüller	k1gMnSc1
zdolal	zdolat	k5eAaPmAgMnS
Korunu	koruna	k1gFnSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
omrzliny	omrzlina	k1gFnPc1
z	z	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
hory	hora	k1gFnSc2
léčí	léčit	k5eAaImIp3nP
doteď	doteď	k?
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2018-06-04	2018-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
hor	hora	k1gFnPc2
světa	svět	k1gInSc2
</s>
<s>
Ojos	Ojos	k1gInSc1
del	del	k?
Salado	Salada	k1gFnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Mount	Mount	k1gInSc1
Everest	Everest	k1gInSc1
</s>
<s>
O	o	k7c4
Mount	Mount	k1gInSc4
Everest	Everest	k1gInSc4
(	(	kIx(
<g/>
Everestian	Everestian	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
tras	trasa	k1gFnPc2
na	na	k7c4
Everest	Everest	k1gInSc4
na	na	k7c4
www.explorersweb.com	www.explorersweb.com	k1gInSc4
</s>
<s>
Popis	popis	k1gInSc1
korejské	korejský	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
JZ	JZ	kA
stěnou	stěna	k1gFnSc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Osmitisícovky	Osmitisícovka	k1gFnPc1
</s>
<s>
Everest	Everest	k1gInSc1
(	(	kIx(
<g/>
8848	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
K2	K2	k1gFnSc1
(	(	kIx(
<g/>
8611	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Kančendženga	Kančendženga	k1gFnSc1
(	(	kIx(
<g/>
8586	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Lhoce	Lhoce	k1gFnSc1
(	(	kIx(
<g/>
8516	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Makalu	Makal	k1gInSc2
(	(	kIx(
<g/>
8463	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Čo	Čo	k1gMnSc1
Oju	Oju	k1gMnSc1
(	(	kIx(
<g/>
8201	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Dhaulágirí	Dhaulágirí	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
8167	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Manáslu	Manásl	k1gInSc2
(	(	kIx(
<g/>
8163	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Nanga	Nanga	k1gFnSc1
Parbat	Parbat	k1gFnSc1
(	(	kIx(
<g/>
8125	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Annapurna	Annapurna	k1gFnSc1
(	(	kIx(
<g/>
8091	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Gašerbrum	Gašerbrum	k1gInSc1
I	I	kA
(	(	kIx(
<g/>
8068	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Broad	Broad	k1gInSc1
Peak	Peak	k1gInSc1
(	(	kIx(
<g/>
8047	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Gašerbrum	Gašerbrum	k1gInSc1
II	II	kA
(	(	kIx(
<g/>
8035	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Šiša	Šiš	k2eAgFnSc1d1
Pangma	Pangma	k1gFnSc1
(	(	kIx(
<g/>
8027	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3
hory	hora	k1gFnPc1
Asie	Asie	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Sýrie	Sýrie	k1gFnSc2
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc4
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
•	•	k?
Vánoční	vánoční	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128521	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4040417-1	4040417-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85045978	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
247168735	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85045978	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Čína	Čína	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
