<p>
<s>
Jako	jako	k9	jako
opona	opona	k1gFnSc1	opona
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
zejména	zejména	k9	zejména
rozměrná	rozměrný	k2eAgFnSc1d1	rozměrná
malba	malba	k1gFnSc1	malba
malíře	malíř	k1gMnSc2	malíř
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Hynaise	Hynaise	k1gFnSc2	Hynaise
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
oponu	opona	k1gFnSc4	opona
Františka	František	k1gMnSc2	František
Ženíška	Ženíšek	k1gMnSc2	Ženíšek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
padla	padnout	k5eAaPmAgFnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
požáru	požár	k1gInSc2	požár
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původní	původní	k2eAgFnSc1d1	původní
Ženíškova	Ženíškův	k2eAgFnSc1d1	Ženíškova
opona	opona	k1gFnSc1	opona
===	===	k?	===
</s>
</p>
<p>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
oponu	opona	k1gFnSc4	opona
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
malíř	malíř	k1gMnSc1	malíř
František	František	k1gMnSc1	František
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
verzi	verze	k1gFnSc6	verze
byla	být	k5eAaImAgFnS	být
ústřední	ústřední	k2eAgFnSc1d1	ústřední
alegorie	alegorie	k1gFnSc1	alegorie
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
postava	postava	k1gFnSc1	postava
Génia	génius	k1gMnSc2	génius
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
obklopena	obklopit	k5eAaPmNgFnS	obklopit
alegoriemi	alegorie	k1gFnPc7	alegorie
Hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
Dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
dole	dole	k6eAd1	dole
hlavní	hlavní	k2eAgInSc4d1	hlavní
výjev	výjev	k1gInSc4	výjev
lemovaly	lemovat	k5eAaImAgInP	lemovat
další	další	k2eAgInPc1d1	další
motivy	motiv	k1gInPc1	motiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opona	opona	k1gFnSc1	opona
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
při	při	k7c6	při
velkém	velký	k2eAgInSc6d1	velký
požáru	požár	k1gInSc6	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
zkázu	zkáza	k1gFnSc4	zkáza
divadla	divadlo	k1gNnSc2	divadlo
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
výzdoba	výzdoba	k1gFnSc1	výzdoba
stropu	strop	k1gInSc2	strop
pocházející	pocházející	k2eAgFnSc1d1	pocházející
také	také	k6eAd1	také
z	z	k7c2	z
Ženíškovy	Ženíškův	k2eAgFnSc2d1	Ženíškova
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
alegorie	alegorie	k1gFnPc1	alegorie
druhů	druh	k1gInPc2	druh
umění	umění	k1gNnSc2	umění
umístěné	umístěný	k2eAgNnSc4d1	umístěné
kolem	kolem	k7c2	kolem
lustru	lustr	k1gInSc2	lustr
nad	nad	k7c7	nad
hledištěm	hlediště	k1gNnSc7	hlediště
<g/>
,	,	kIx,	,
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
zůstala	zůstat	k5eAaPmAgFnS	zůstat
<g/>
,	,	kIx,	,
oponu	opona	k1gFnSc4	opona
nahradila	nahradit	k5eAaPmAgFnS	nahradit
nová	nový	k2eAgFnSc1d1	nová
vybraná	vybraná	k1gFnSc1	vybraná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nové	nový	k2eAgFnSc2d1	nová
umělecké	umělecký	k2eAgFnSc2d1	umělecká
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
se	se	k3xPyFc4	se
už	už	k9	už
Ženíšek	Ženíšek	k1gMnSc1	Ženíšek
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
zaneprázdněnost	zaneprázdněnost	k1gFnSc4	zaneprázdněnost
nezapojil	zapojit	k5eNaPmAgMnS	zapojit
<g/>
,	,	kIx,	,
na	na	k7c6	na
vině	vina	k1gFnSc6	vina
ale	ale	k8xC	ale
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
i	i	k9	i
spory	spor	k1gInPc1	spor
o	o	k7c6	o
výši	výše	k1gFnSc6	výše
honoráře	honorář	k1gInSc2	honorář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zničené	zničený	k2eAgFnSc6d1	zničená
oponě	opona	k1gFnSc6	opona
zůstaly	zůstat	k5eAaPmAgInP	zůstat
jen	jen	k9	jen
ojedinělé	ojedinělý	k2eAgFnPc1d1	ojedinělá
fotografie	fotografia	k1gFnPc1	fotografia
a	a	k8xC	a
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
návrhu	návrh	k1gInSc6	návrh
a	a	k8xC	a
studii	studie	k1gFnSc6	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
uchované	uchovaný	k2eAgFnPc1d1	uchovaná
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Muzea	muzeum	k1gNnSc2	muzeum
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hynaisova	Hynaisův	k2eAgFnSc1d1	Hynaisova
opona	opona	k1gFnSc1	opona
===	===	k?	===
</s>
</p>
<p>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
nové	nový	k2eAgFnSc2d1	nová
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hynais	Hynais	k1gFnSc2	Hynais
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
překonání	překonání	k1gNnSc4	překonání
zdrcujícího	zdrcující	k2eAgInSc2d1	zdrcující
dopadu	dopad	k1gInSc2	dopad
požáru	požár	k1gInSc2	požár
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
a	a	k8xC	a
obnovu	obnova	k1gFnSc4	obnova
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
Hynais	Hynais	k1gFnSc7	Hynais
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
si	se	k3xPyFc3	se
Ženíškův	Ženíškův	k2eAgInSc4d1	Ženíškův
souhlas	souhlas	k1gInSc4	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
ještě	ještě	k9	ještě
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plátno	plátno	k1gNnSc1	plátno
samotné	samotný	k2eAgNnSc1d1	samotné
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
pomocníky	pomocník	k1gMnPc7	pomocník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
jeho	on	k3xPp3gInSc4	on
návrh	návrh	k1gInSc4	návrh
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k9	jako
vítězný	vítězný	k2eAgMnSc1d1	vítězný
<g/>
,	,	kIx,	,
s	s	k7c7	s
definitivní	definitivní	k2eAgFnSc7d1	definitivní
podobou	podoba	k1gFnSc7	podoba
malíř	malíř	k1gMnSc1	malíř
narazil	narazit	k5eAaPmAgInS	narazit
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
její	její	k3xOp3gFnSc4	její
mírnou	mírný	k2eAgFnSc4d1	mírná
šedavou	šedavý	k2eAgFnSc4d1	šedavá
barevnost	barevnost	k1gFnSc4	barevnost
<g/>
,	,	kIx,	,
neslovansky	slovansky	k6eNd1	slovansky
štíhlé	štíhlý	k2eAgFnPc4d1	štíhlá
postavy	postava	k1gFnPc4	postava
i	i	k9	i
jejich	jejich	k3xOp3gNnSc4	jejich
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Hynais	Hynais	k1gFnPc4	Hynais
dostal	dostat	k5eAaPmAgInS	dostat
úkol	úkol	k1gInSc1	úkol
oponu	opona	k1gFnSc4	opona
přepracovat	přepracovat	k5eAaPmF	přepracovat
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
už	už	k6eAd1	už
být	být	k5eAaImF	být
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
instalována	instalován	k2eAgFnSc1d1	instalována
–	–	k?	–
a	a	k8xC	a
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
sňata	sňat	k2eAgFnSc1d1	sňata
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
zadavateli	zadavatel	k1gMnPc7	zadavatel
schválena	schválen	k2eAgFnSc1d1	schválena
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgFnPc7	první
autoritami	autorita	k1gFnPc7	autorita
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Hynaisovu	Hynaisův	k2eAgFnSc4d1	Hynaisova
oponu	opona	k1gFnSc4	opona
přijaly	přijmout	k5eAaPmAgFnP	přijmout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
profesor	profesor	k1gMnSc1	profesor
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc1	umění
Václav	Václav	k1gMnSc1	Václav
Vilém	Vilém	k1gMnSc1	Vilém
Štech	Štech	k1gMnSc1	Štech
<g/>
.	.	kIx.	.
<g/>
Opona	opona	k1gFnSc1	opona
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
celkové	celkový	k2eAgFnSc2d1	celková
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
obnova	obnova	k1gFnSc1	obnova
opony	opona	k1gFnPc4	opona
v	v	k7c6	v
barrandovských	barrandovský	k2eAgFnPc6d1	Barrandovská
filmových	filmový	k2eAgFnPc6d1	filmová
studiích	studie	k1gFnPc6	studie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
péčí	péče	k1gFnSc7	péče
akademických	akademický	k2eAgMnPc2d1	akademický
malířů	malíř	k1gMnPc2	malíř
Jana	Jan	k1gMnSc2	Jan
Hály	Hála	k1gMnSc2	Hála
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Sedláka	Sedlák	k1gMnSc2	Sedlák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
znovu	znovu	k6eAd1	znovu
po	po	k7c6	po
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
nákladem	náklad	k1gInSc7	náklad
zhruba	zhruba	k6eAd1	zhruba
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
podnikatelka	podnikatelka	k1gFnSc1	podnikatelka
a	a	k8xC	a
mecenáška	mecenáška	k1gFnSc1	mecenáška
Dadja	Dadja	k1gFnSc1	Dadja
Altenburg-Kohlová	Altenburg-Kohlová	k1gFnSc1	Altenburg-Kohlová
<g/>
.	.	kIx.	.
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
opakovaně	opakovaně	k6eAd1	opakovaně
marně	marně	k6eAd1	marně
usilovalo	usilovat	k5eAaImAgNnS	usilovat
o	o	k7c4	o
zapsání	zapsání	k1gNnSc4	zapsání
opony	opona	k1gFnSc2	opona
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
architektura	architektura	k1gFnSc1	architektura
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opona	opona	k1gFnSc1	opona
samotná	samotný	k2eAgFnSc1d1	samotná
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
opony	opona	k1gFnSc2	opona
==	==	k?	==
</s>
</p>
<p>
<s>
Hynais	Hynais	k1gFnSc1	Hynais
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
obrazu	obraz	k1gInSc2	obraz
umístil	umístit	k5eAaPmAgMnS	umístit
ženské	ženský	k2eAgFnPc4d1	ženská
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterých	který	k3yRgFnPc2	který
rozmístil	rozmístit	k5eAaPmAgMnS	rozmístit
postavy	postava	k1gFnPc4	postava
dárců	dárce	k1gMnPc2	dárce
<g/>
,	,	kIx,	,
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
centrální	centrální	k2eAgFnSc2d1	centrální
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
symbol	symbol	k1gInSc4	symbol
všeho	všecek	k3xTgNnSc2	všecek
Slovanstva	Slovanstvo	k1gNnSc2	Slovanstvo
Slávii	Slávia	k1gFnSc4	Slávia
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
o	o	k7c4	o
alegorii	alegorie	k1gFnSc4	alegorie
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dramatických	dramatický	k2eAgInPc2d1	dramatický
žánrů	žánr	k1gInPc2	žánr
–	–	k?	–
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc2	komedie
a	a	k8xC	a
frašky	fraška	k1gFnSc2	fraška
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
model	model	k1gInSc1	model
pro	pro	k7c4	pro
postavy	postava	k1gFnPc4	postava
obrazu	obraz	k1gInSc2	obraz
sloužili	sloužit	k5eAaImAgMnP	sloužit
mnozí	mnohý	k2eAgMnPc1d1	mnohý
Hynaisovi	Hynaisův	k2eAgMnPc1d1	Hynaisův
známí	známý	k1gMnPc1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Vznášející	vznášející	k2eAgFnSc4d1	vznášející
se	se	k3xPyFc4	se
postavu	postava	k1gFnSc4	postava
Génia	génius	k1gMnSc2	génius
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Slávy	Sláva	k1gFnSc2	Sláva
Čechie	Čechie	k1gFnSc2	Čechie
<g/>
)	)	kIx)	)
maloval	malovat	k5eAaImAgMnS	malovat
Hynais	Hynais	k1gFnSc4	Hynais
podle	podle	k7c2	podle
malířky	malířka	k1gFnSc2	malířka
Marie	Maria	k1gFnSc2	Maria
"	"	kIx"	"
<g/>
Suzanne	Suzann	k1gMnSc5	Suzann
<g/>
"	"	kIx"	"
Valadonové	Valadonové	k2eAgFnSc7d1	Valadonové
<g/>
,	,	kIx,	,
postavu	postava	k1gFnSc4	postava
sedící	sedící	k2eAgFnSc2d1	sedící
Slávie	Slávia	k1gFnSc2	Slávia
podle	podle	k7c2	podle
manželky	manželka	k1gFnSc2	manželka
jeho	on	k3xPp3gMnSc2	on
přítele	přítel	k1gMnSc2	přítel
Oskara	Oskar	k1gMnSc2	Oskar
Rexe	Rex	k1gMnSc2	Rex
<g/>
.	.	kIx.	.
</s>
<s>
Plavovlasý	plavovlasý	k2eAgInSc1d1	plavovlasý
mladík	mladík	k1gInSc1	mladík
hledící	hledící	k2eAgInSc1d1	hledící
před	před	k7c7	před
levým	levý	k2eAgInSc7d1	levý
sloupem	sloup	k1gInSc7	sloup
vzhůru	vzhůru	k6eAd1	vzhůru
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
Hynaisova	Hynaisův	k2eAgMnSc2d1	Hynaisův
intimního	intimní	k2eAgMnSc2d1	intimní
přítele	přítel	k1gMnSc2	přítel
malíře	malíř	k1gMnSc2	malíř
Václava	Václav	k1gMnSc2	Václav
Sochora	Sochor	k1gMnSc2	Sochor
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
modely	model	k1gInPc7	model
a	a	k8xC	a
vzory	vzor	k1gInPc7	vzor
byli	být	k5eAaImAgMnP	být
též	též	k9	též
Hynaisovi	Hynaisův	k2eAgMnPc1d1	Hynaisův
přátelé	přítel	k1gMnPc1	přítel
Karbowsky	Karbowska	k1gFnSc2	Karbowska
<g/>
,	,	kIx,	,
Desriviè	Desriviè	k1gMnSc1	Desriviè
a	a	k8xC	a
doktor	doktor	k1gMnSc1	doktor
Rachet	Rachet	k1gMnSc1	Rachet
či	či	k8xC	či
manželka	manželka	k1gFnSc1	manželka
malíře	malíř	k1gMnSc2	malíř
Eugena	Eugen	k1gMnSc2	Eugen
Jettela	Jettel	k1gMnSc2	Jettel
<g/>
.	.	kIx.	.
<g/>
Opona	opona	k1gFnSc1	opona
má	mít	k5eAaImIp3nS	mít
plochu	plocha	k1gFnSc4	plocha
přibližně	přibližně	k6eAd1	přibližně
140	[number]	k4	140
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
180	[number]	k4	180
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Budí	budit	k5eAaImIp3nS	budit
dojem	dojem	k1gInSc4	dojem
gobelínu	gobelín	k1gInSc2	gobelín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
olejomalbu	olejomalba	k1gFnSc4	olejomalba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Protipožární	protipožární	k2eAgFnSc1d1	protipožární
a	a	k8xC	a
provozní	provozní	k2eAgFnSc1d1	provozní
opona	opona	k1gFnSc1	opona
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
ozdobné	ozdobný	k2eAgFnSc2d1	ozdobná
Hynaisovy	Hynaisův	k2eAgFnSc2d1	Hynaisova
opony	opona	k1gFnSc2	opona
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
budově	budova	k1gFnSc6	budova
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
jeviště	jeviště	k1gNnSc4	jeviště
od	od	k7c2	od
hlediště	hlediště	k1gNnSc2	hlediště
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
před	před	k7c7	před
představením	představení	k1gNnSc7	představení
je	být	k5eAaImIp3nS	být
spuštěná	spuštěný	k2eAgFnSc1d1	spuštěná
protipožární	protipožární	k2eAgFnSc1d1	protipožární
železná	železný	k2eAgFnSc1d1	železná
opona	opona	k1gFnSc1	opona
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
při	při	k7c6	při
představení	představení	k1gNnSc6	představení
slouží	sloužit	k5eAaImIp3nS	sloužit
ručně	ručně	k6eAd1	ručně
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
opona	opona	k1gFnSc1	opona
z	z	k7c2	z
tmavě	tmavě	k6eAd1	tmavě
červeného	červený	k2eAgInSc2d1	červený
sametu	samet	k1gInSc2	samet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Národ	národ	k1gInSc1	národ
sobě	se	k3xPyFc3	se
:	:	kIx,	:
národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
umělecké	umělecký	k2eAgInPc4d1	umělecký
poklady	poklad	k1gInPc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
170	[number]	k4	170
s.	s.	k?	s.
</s>
</p>
