<s>
Hérodotos	Hérodotos	k1gMnSc1
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
Narození	narození	k1gNnSc2
</s>
<s>
484	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Halikarnassos	Halikarnassos	k1gInSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
425	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
58	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Thúrie	Thúrie	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Řekové	Řek	k1gMnPc1
a	a	k8xC
Carians	Carians	k1gInSc4
Významná	významný	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
původní	původní	k2eAgInPc4d1
texty	text	k1gInPc4
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
z	z	k7c2
Halikarnássu	Halikarnáss	k1gInSc2
(	(	kIx(
<g/>
starořecky	starořecky	k6eAd1
Ἡ	Ἡ	k?
Ἁ	Ἁ	k?
<g/>
,	,	kIx,
asi	asi	k9
484	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
–	–	k?
asi	asi	k9
mezi	mezi	k7c7
lety	léto	k1gNnPc7
430	#num#	k4
a	a	k8xC
420	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
významným	významný	k2eAgMnSc7d1
antickým	antický	k2eAgMnSc7d1
historikem	historik	k1gMnSc7
<g/>
,	,	kIx,
proto	proto	k8xC
byl	být	k5eAaImAgMnS
Ciceronem	Cicero	k1gMnSc7
nazván	nazvat	k5eAaPmNgMnS,k5eAaBmNgMnS
otcem	otec	k1gMnSc7
dějepisu	dějepis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gInSc6
pojmenován	pojmenovat	k5eAaPmNgInS
měsíční	měsíční	k2eAgInSc1d1
kráter	kráter	k1gInSc1
Herodotus	Herodotus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnoho	mnoho	k6eAd1
cestoval	cestovat	k5eAaImAgMnS
(	(	kIx(
<g/>
do	do	k7c2
Itálie	Itálie	k1gFnSc2
na	na	k7c6
západě	západ	k1gInSc6
<g/>
,	,	kIx,
do	do	k7c2
Babylonie	Babylonie	k1gFnSc2
na	na	k7c6
východě	východ	k1gInSc6
<g/>
,	,	kIx,
do	do	k7c2
donských	donský	k2eAgFnPc2d1
stepí	step	k1gFnPc2
na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
k	k	k7c3
prvním	první	k4xOgInPc3
nilským	nilský	k2eAgInPc3d1
prahům	práh	k1gInPc3
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudil	soudil	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
historii	historie	k1gFnSc3
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
přistupovat	přistupovat	k5eAaImF
geograficky	geograficky	k6eAd1
a	a	k8xC
ke	k	k7c3
geografii	geografie	k1gFnSc3
historicky	historicky	k6eAd1
(	(	kIx(
<g/>
známý	známý	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
výrok	výrok	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
geografie	geografie	k1gFnSc1
je	být	k5eAaImIp3nS
reálná	reálný	k2eAgFnSc1d1
<g/>
,	,	kIx,
lidskou	lidský	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
vytvořená	vytvořený	k2eAgFnSc1d1
historie	historie	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
Dějinách	dějiny	k1gFnPc6
(	(	kIx(
<g/>
Histories	Histories	k1gInSc4
apodeixis	apodeixis	k1gFnSc2
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
„	„	k?
<g/>
Výkaz	výkaz	k1gInSc1
vyzkoumaného	vyzkoumaný	k2eAgNnSc2d1
bádání	bádání	k1gNnSc2
<g/>
“	“	k?
nebo	nebo	k8xC
také	také	k9
„	„	k?
<g/>
Výklad	výklad	k1gInSc1
zjištěných	zjištěný	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
vytvořil	vytvořit	k5eAaPmAgInS
v	v	k7c6
devíti	devět	k4xCc6
knihách	kniha	k1gFnPc6
nejúplnější	úplný	k2eAgInSc4d3
popis	popis	k1gInSc4
světa	svět	k1gInSc2
(	(	kIx(
<g/>
oikúmené	oikúmený	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známého	známý	k2eAgMnSc4d1
Řekům	Řek	k1gMnPc3
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Halikarnássu	Halikarnáss	k1gInSc6
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
i	i	k9
politicky	politicky	k6eAd1
angažoval	angažovat	k5eAaBmAgMnS
a	a	k8xC
zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
pokusu	pokus	k1gInSc2
svrhnout	svrhnout	k5eAaPmF
místního	místní	k2eAgMnSc4d1
tyrana	tyran	k1gMnSc4
Lygdamida	Lygdamid	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
celé	celý	k2eAgFnSc2d1
akce	akce	k1gFnSc2
musel	muset	k5eAaImAgMnS
odejít	odejít	k5eAaPmF
do	do	k7c2
exilu	exil	k1gInSc2
na	na	k7c4
ostrov	ostrov	k1gInSc4
Samos	Samos	k1gInSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
však	však	k9
vrátil	vrátit	k5eAaPmAgMnS
a	a	k8xC
opět	opět	k6eAd1
se	se	k3xPyFc4
zapletl	zaplést	k5eAaPmAgMnS
do	do	k7c2
komplotu	komplot	k1gInSc2
proti	proti	k7c3
Lygdamidovi	Lygdamid	k1gMnSc3
<g/>
;	;	kIx,
tentokrát	tentokrát	k6eAd1
se	s	k7c7
zdarem	zdar	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
krátce	krátce	k6eAd1
nato	nato	k6eAd1
rodné	rodný	k2eAgNnSc4d1
město	město	k1gNnSc4
navždy	navždy	k6eAd1
opustil	opustit	k5eAaPmAgMnS
a	a	k8xC
přesídlil	přesídlit	k5eAaPmAgMnS
zpátky	zpátky	k6eAd1
na	na	k7c4
Samos	Samos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
447	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
pobýval	pobývat	k5eAaImAgMnS
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
navázal	navázat	k5eAaPmAgMnS
kontakty	kontakt	k1gInPc4
s	s	k7c7
řadou	řada	k1gFnSc7
významných	významný	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
,	,	kIx,
mj.	mj.	kA
se	s	k7c7
Sofoklem	Sofokles	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
na	na	k7c4
Hérodota	Hérodot	k1gMnSc4
dokonce	dokonce	k9
odkazuje	odkazovat	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
Oidipus	Oidipus	k1gMnSc1
na	na	k7c6
Kolónu	kolón	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
Periklem	Perikl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Athénách	Athéna	k1gFnPc6
předčítal	předčítat	k5eAaImAgMnS
úryvky	úryvek	k1gInPc4
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
zač	zač	k6eAd1
dostával	dostávat	k5eAaImAgMnS
od	od	k7c2
města	město	k1gNnSc2
finanční	finanční	k2eAgFnSc4d1
odměnu	odměna	k1gFnSc4
<g/>
;	;	kIx,
později	pozdě	k6eAd2
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
nově	nově	k6eAd1
založené	založený	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
Thúrie	Thúrie	k1gFnSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
Dějiny	dějiny	k1gFnPc1
zveřejnil	zveřejnit	k5eAaPmAgInS
v	v	k7c6
úplné	úplný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
nejpozději	pozdě	k6eAd3
v	v	k7c6
roce	rok	k1gInSc6
424	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
brzy	brzy	k6eAd1
nato	nato	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřben	pohřben	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
Thúriích	Thúrie	k1gFnPc6
<g/>
.	.	kIx.
<g/>
Hérodotos	Hérodotos	k1gMnSc1
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
jako	jako	k8xS,k8xC
historik	historik	k1gMnSc1
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
podrobně	podrobně	k6eAd1
rozebral	rozebrat	k5eAaPmAgInS
vznik	vznik	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
největší	veliký	k2eAgFnSc2d3
mocnosti	mocnost	k1gFnSc2
soudobého	soudobý	k2eAgInSc2d1
světa	svět	k1gInSc2
–	–	k?
perské	perský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
–	–	k?
a	a	k8xC
její	její	k3xOp3gMnPc1
konflikt	konflikt	k1gInSc4
s	s	k7c7
evropskými	evropský	k2eAgMnPc7d1
Řeky	Řek	k1gMnPc7
v	v	k7c6
prvních	první	k4xOgNnPc6
desetiletích	desetiletí	k1gNnPc6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Řecko-perské	řecko-perský	k2eAgFnSc2d1
války	válka	k1gFnSc2
vyplňují	vyplňovat	k5eAaImIp3nP
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
jeho	jeho	k3xOp3gFnPc2
Dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
bez	bez	k7c2
jeho	jeho	k3xOp3gInPc2
údajů	údaj	k1gInPc2
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
jen	jen	k9
velmi	velmi	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
rekonstruovat	rekonstruovat	k5eAaBmF
složité	složitý	k2eAgNnSc4d1
předivo	předivo	k1gNnSc4
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
řeckými	řecký	k2eAgFnPc7d1
obcemi	obec	k1gFnPc7
<g/>
,	,	kIx,
Peršany	Peršan	k1gMnPc7
i	i	k8xC
dalšími	další	k2eAgMnPc7d1
aktéry	aktér	k1gMnPc7
zápasu	zápas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
výklad	výklad	k1gInSc1
je	být	k5eAaImIp3nS
podán	podat	k5eAaPmNgInS
plasticky	plasticky	k6eAd1
a	a	k8xC
relativně	relativně	k6eAd1
objektivně	objektivně	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
samozřejmě	samozřejmě	k6eAd1
nelze	lze	k6eNd1
přehlédnout	přehlédnout	k5eAaPmF
jeho	jeho	k3xOp3gNnSc4
prořecké	prořecký	k2eAgNnSc4d1
stranictví	stranictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
historických	historický	k2eAgInPc2d1
údajů	údaj	k1gInPc2
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
nemalou	malý	k2eNgFnSc4d1
pozornost	pozornost	k1gFnSc4
popisům	popis	k1gInPc3
jednotlivých	jednotlivý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
zvláštností	zvláštnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Hérodotův	Hérodotův	k2eAgInSc1d1
živý	živý	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
a	a	k8xC
snaha	snaha	k1gFnSc1
o	o	k7c4
objektivitu	objektivita	k1gFnSc4
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
vzorem	vzor	k1gInSc7
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
antickou	antický	k2eAgFnSc4d1
historiografii	historiografie	k1gFnSc4
a	a	k8xC
nejen	nejen	k6eAd1
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
první	první	k4xOgNnSc4
moderní	moderní	k2eAgNnSc4d1
pojednání	pojednání	k1gNnSc4
o	o	k7c6
vývoji	vývoj	k1gInSc6
vztahů	vztah	k1gInPc2
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
vedené	vedený	k2eAgFnSc2d1
snahou	snaha	k1gFnSc7
pochopit	pochopit	k5eAaPmF
i	i	k9
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
nejasné	jasný	k2eNgFnSc2d1
záležitosti	záležitost	k1gFnSc2
a	a	k8xC
jevy	jev	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
je	být	k5eAaImIp3nS
Hérodotos	Hérodotos	k1gMnSc1
průkopníkem	průkopník	k1gMnSc7
samostatného	samostatný	k2eAgNnSc2d1
uvažování	uvažování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hérodotův	Hérodotův	k2eAgInSc1d1
svět	svět	k1gInSc1
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
nám	my	k3xPp1nPc3
zanechal	zanechat	k5eAaPmAgMnS
detailní	detailní	k2eAgInSc4d1
obraz	obraz	k1gInSc4
světa	svět	k1gInSc2
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
údaje	údaj	k1gInPc1
mají	mít	k5eAaImIp3nP
i	i	k9
dnes	dnes	k6eAd1
(	(	kIx(
<g/>
navzdory	navzdory	k7c3
všem	všecek	k3xTgInPc3
archeologickým	archeologický	k2eAgInPc3d1
poznatkům	poznatek	k1gInPc3
<g/>
)	)	kIx)
zásadní	zásadní	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
v	v	k7c6
druhé	druhý	k4xOgFnSc6
knize	kniha	k1gFnSc6
se	se	k3xPyFc4
systematicky	systematicky	k6eAd1
zabýval	zabývat	k5eAaImAgMnS
Egyptem	Egypt	k1gInSc7
(	(	kIx(
<g/>
až	až	k6eAd1
do	do	k7c2
vzniku	vznik	k1gInSc2
egyptologie	egyptologie	k1gFnSc2
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gFnPc2
informací	informace	k1gFnPc2
jediným	jediný	k2eAgInSc7d1
pramenem	pramen	k1gInSc7
o	o	k7c6
dávných	dávný	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
této	tento	k3xDgFnSc2
země	zem	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čtvrtá	čtvrtý	k4xOgFnSc1
kniha	kniha	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
první	první	k4xOgFnPc4
věrohodné	věrohodný	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
o	o	k7c6
Skytii	Skytie	k1gFnSc6
–	–	k?
o	o	k7c6
severním	severní	k2eAgNnSc6d1
Černomoří	Černomoří	k1gNnSc6
a	a	k8xC
severu	sever	k1gInSc6
Evropy	Evropa	k1gFnSc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hérodotos	Hérodotos	k1gMnSc1
uvádí	uvádět	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
skytské	skytský	k2eAgFnPc4d1
řeky	řeka	k1gFnPc4
od	od	k7c2
Istru	Ister	k1gInSc2
(	(	kIx(
<g/>
tj.	tj.	kA
dolního	dolní	k2eAgInSc2d1
Dunaje	Dunaj	k1gInSc2
<g/>
)	)	kIx)
až	až	k9
po	po	k7c6
Tanais	Tanais	k1gFnSc6
(	(	kIx(
<g/>
Don	Don	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půda	půda	k1gFnSc1
ve	v	k7c6
Skytii	Skytie	k1gFnSc6
je	být	k5eAaImIp3nS
podle	podle	k7c2
něho	on	k3xPp3gMnSc2
„	„	k?
<g/>
rovná	rovnat	k5eAaImIp3nS
<g/>
,	,	kIx,
bohatě	bohatě	k6eAd1
porostlá	porostlý	k2eAgFnSc1d1
travou	trava	k1gFnSc7
<g/>
,	,	kIx,
hojně	hojně	k6eAd1
zavlažovaná	zavlažovaný	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
Skytie	Skytie	k1gFnSc2
prý	prý	k9
neustále	neustále	k6eAd1
padá	padat	k5eAaImIp3nS
sníh	sníh	k1gInSc1
<g/>
;	;	kIx,
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
tato	tento	k3xDgNnPc1
území	území	k1gNnPc1
neobyvatelná	obyvatelný	k2eNgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hérodotos	Hérodotos	k1gMnSc1
pochybuje	pochybovat	k5eAaImIp3nS
o	o	k7c4
existenci	existence	k1gFnSc4
mytických	mytický	k2eAgMnPc2d1
Hyperborejců	Hyperborejec	k1gMnPc2
<g/>
,	,	kIx,
„	„	k?
<g/>
národa	národ	k1gInSc2
nejzazšího	zadní	k2eAgInSc2d3
severu	sever	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
sídlícího	sídlící	k2eAgInSc2d1
(	(	kIx(
<g/>
podle	podle	k7c2
Homéra	Homér	k1gMnSc2
a	a	k8xC
Hésioda	Hésiod	k1gMnSc2
<g/>
)	)	kIx)
„	„	k?
<g/>
za	za	k7c7
severním	severní	k2eAgInSc7d1
větrem	vítr	k1gInSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
zvaným	zvaný	k2eAgInSc7d1
„	„	k?
<g/>
boreas	boreas	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
Hérodotova	Hérodotův	k2eAgInSc2d1
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
Indus	Indus	k1gInSc1
<g/>
;	;	kIx,
za	za	k7c7
ním	on	k3xPp3gMnSc7
se	se	k3xPyFc4
prý	prý	k9
rozkládá	rozkládat	k5eAaImIp3nS
poušť	poušť	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc2
vlastnosti	vlastnost	k1gFnSc2
nikdo	nikdo	k3yNnSc1
nezná	neznat	k5eAaImIp3nS,k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
Nilu	Nil	k1gInSc2
charakterizuje	charakterizovat	k5eAaBmIp3nS
zhruba	zhruba	k6eAd1
k	k	k7c3
obratníku	obratník	k1gInSc3
Raka	rak	k1gMnSc2
<g/>
;	;	kIx,
poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
zmiňuje	zmiňovat	k5eAaImIp3nS
o	o	k7c6
„	„	k?
<g/>
Meroé	Meroé	k1gNnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
starověkém	starověký	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
jižní	jižní	k2eAgFnSc2d1
Núbie	Núbie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
pobytu	pobyt	k1gInSc2
v	v	k7c6
Egyptě	Egypt	k1gInSc6
se	se	k3xPyFc4
prý	prý	k9
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
snažil	snažit	k5eAaImAgMnS
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
kde	kde	k6eAd1
leží	ležet	k5eAaImIp3nP
prameny	pramen	k1gInPc1
Nilu	Nil	k1gInSc2
<g/>
;	;	kIx,
nic	nic	k3yNnSc1
konkrétního	konkrétní	k2eAgNnSc2d1
se	se	k3xPyFc4
však	však	k9
nedověděl	dovědět	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
a	a	k8xC
Indický	indický	k2eAgInSc1d1
oceán	oceán	k1gInSc1
(	(	kIx(
<g/>
Eritrejské	eritrejský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
jediným	jediný	k2eAgNnSc7d1
spojeným	spojený	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libyi	Libye	k1gFnSc4
(	(	kIx(
<g/>
Afriku	Afrika	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
Asii	Asie	k1gFnSc4
obklopují	obklopovat	k5eAaImIp3nP
na	na	k7c6
jihu	jih	k1gInSc6
vodní	vodní	k2eAgFnSc2d1
prostory	prostora	k1gFnSc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
s	s	k7c7
určitostí	určitost	k1gFnSc7
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
na	na	k7c6
severu	sever	k1gInSc6
omývána	omýván	k2eAgFnSc1d1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
<g/>
“	“	k?
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hérodotos	Hérodotos	k1gMnSc1
se	se	k3xPyFc4
vysmívá	vysmívat	k5eAaImIp3nS
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Země	země	k1gFnSc1
je	být	k5eAaImIp3nS
kotoučem	kotouč	k1gInSc7
„	„	k?
<g/>
podle	podle	k7c2
kružítka	kružítko	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeku-oceán	Řeku-oceán	k1gMnSc1
si	se	k3xPyFc3
údajně	údajně	k6eAd1
vymyslel	vymyslet	k5eAaPmAgMnS
Homér	Homér	k1gMnSc1
či	či	k8xC
jiný	jiný	k2eAgMnSc1d1
z	z	k7c2
dávných	dávný	k2eAgMnPc2d1
básníků	básník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropa	Evropa	k1gFnSc1
je	být	k5eAaImIp3nS
prý	prý	k9
stejně	stejně	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Libye	Libye	k1gFnSc1
s	s	k7c7
Asií	Asie	k1gFnSc7
dohromady	dohromady	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
přitom	přitom	k6eAd1
mnohem	mnohem	k6eAd1
širší	široký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
jako	jako	k8xC,k8xS
pozorovatel	pozorovatel	k1gMnSc1
</s>
<s>
V	v	k7c6
Hérodotově	Hérodotův	k2eAgInSc6d1
regionálně	regionálně	k6eAd1
geografickém	geografický	k2eAgInSc6d1
popisu	popis	k1gInSc6
dominují	dominovat	k5eAaImIp3nP
čistě	čistě	k6eAd1
topografické	topografický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
(	(	kIx(
<g/>
výčty	výčet	k1gInPc1
řek	řeka	k1gFnPc2
<g/>
,	,	kIx,
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
měst	město	k1gNnPc2
<g/>
,	,	kIx,
pamětihodností	pamětihodnost	k1gFnPc2
<g/>
,	,	kIx,
chrámů	chrám	k1gInPc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jistá	jistý	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
je	být	k5eAaImIp3nS
věnována	věnovat	k5eAaImNgFnS,k5eAaPmNgFnS
charakteru	charakter	k1gInSc3
a	a	k8xC
zvyklostem	zvyklost	k1gFnPc3
různých	různý	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
a	a	k8xC
etnických	etnický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
přírodních	přírodní	k2eAgFnPc2d1
zvláštností	zvláštnost	k1gFnPc2
zemí	zem	k1gFnPc2
zaznamenává	zaznamenávat	k5eAaImIp3nS
hlavně	hlavně	k9
do	do	k7c2
očí	oko	k1gNnPc2
bijící	bijící	k2eAgInSc4d1
rysy	rys	k1gInPc4
podnebí	podnebí	k1gNnSc2
<g/>
,	,	kIx,
vzácnosti	vzácnost	k1gFnSc2
flóry	flóra	k1gFnSc2
a	a	k8xC
fauny	fauna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdařile	zdařile	k6eAd1
jsou	být	k5eAaImIp3nP
odpozorována	odpozorován	k2eAgFnSc1d1
některá	některý	k3yIgFnSc1
specifika	specifika	k1gFnSc1
–	–	k?
například	například	k6eAd1
že	že	k8xS
v	v	k7c6
Libyi	Libye	k1gFnSc6
téměř	téměř	k6eAd1
neprší	pršet	k5eNaImIp3nS
ani	ani	k8xC
nepadá	padat	k5eNaImIp3nS
sníh	sníh	k1gInSc1
<g/>
;	;	kIx,
že	že	k8xS
se	se	k3xPyFc4
tam	tam	k6eAd1
jestřábi	jestřáb	k1gMnPc1
stěhují	stěhovat	k5eAaImIp3nP
před	před	k7c7
skytskými	skytský	k2eAgInPc7d1
zimními	zimní	k2eAgInPc7d1
mrazy	mráz	k1gInPc7
<g/>
;	;	kIx,
že	že	k8xS
egyptské	egyptský	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
jsou	být	k5eAaImIp3nP
černé	černý	k2eAgInPc1d1
a	a	k8xC
zkypřené	zkypřený	k2eAgInPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
jílů	jíl	k1gInPc2
a	a	k8xC
nánosů	nános	k1gInPc2
<g/>
,	,	kIx,
přinesených	přinesený	k2eAgInPc2d1
řekou	řeka	k1gFnSc7
z	z	k7c2
Etiopie	Etiopie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nil	Nil	k1gInSc1
teče	téct	k5eAaImIp3nS
podle	podle	k7c2
Hérodota	Hérodot	k1gMnSc2
v	v	k7c6
podstatě	podstata	k1gFnSc6
ve	v	k7c6
směru	směr	k1gInSc6
poledníku	poledník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libye	Libye	k1gFnSc1
má	mít	k5eAaImIp3nS
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
půdy	půda	k1gFnPc1
načervenalé	načervenalý	k2eAgFnPc1d1
<g/>
,	,	kIx,
písčité	písčitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
Arábie	Arábie	k1gFnSc1
a	a	k8xC
Sýrie	Sýrie	k1gFnSc1
hlinité	hlinitý	k2eAgFnSc2d1
a	a	k8xC
kamenité	kamenitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
regionalizace	regionalizace	k1gFnSc1
Libye	Libye	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
prý	prý	k9
tvoří	tvořit	k5eAaImIp3nS
4	#num#	k4
rovnoběžková	rovnoběžkový	k2eAgFnSc1d1
pásma	pásmo	k1gNnSc2
(	(	kIx(
<g/>
obydlené	obydlený	k2eAgNnSc1d1
přímořské	přímořský	k2eAgNnSc1d1
<g/>
,	,	kIx,
zóna	zóna	k1gFnSc1
divoké	divoký	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
<g/>
,	,	kIx,
písečná	písečný	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
<g/>
,	,	kIx,
holá	holý	k2eAgFnSc1d1
poušť	poušť	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Slunce	slunce	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
zimě	zima	k1gFnSc6
odklání	odklánět	k5eAaImIp3nS
na	na	k7c4
jih	jih	k1gInSc4
vlivem	vlivem	k7c2
bouří	bouř	k1gFnPc2
a	a	k8xC
chladu	chlad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítr	vítr	k1gInSc4
prý	prý	k9
vždycky	vždycky	k6eAd1
vane	vanout	k5eAaImIp3nS
ze	z	k7c2
studených	studený	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asie	Asie	k1gFnSc1
leží	ležet	k5eAaImIp3nS
blíže	blízce	k6eAd2
Slunci	slunce	k1gNnSc3
než	než	k8xS
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
tamní	tamní	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
i	i	k8xC
plody	plod	k1gInPc4
lepší	dobrý	k2eAgFnSc1d2
a	a	k8xC
větší	veliký	k2eAgFnSc1d2
(	(	kIx(
<g/>
s	s	k7c7
podobným	podobný	k2eAgInSc7d1
názorem	názor	k1gInSc7
se	se	k3xPyFc4
rovněž	rovněž	k9
setkáváme	setkávat	k5eAaImIp1nP
u	u	k7c2
Hippokrata	Hippokrat	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Indii	Indie	k1gFnSc6
bývá	bývat	k5eAaImIp3nS
už	už	k9
v	v	k7c6
jitřních	jitřní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
teplo	teplo	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
prý	prý	k9
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
blíže	blízce	k6eAd2
k	k	k7c3
východu	východ	k1gInSc3
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
Hérodotův	Hérodotův	k2eAgInSc1d1
názor	názor	k1gInSc1
na	na	k7c4
původ	původ	k1gInSc4
delty	delta	k1gFnSc2
Nilu	Nil	k1gInSc2
–	–	k?
jde	jít	k5eAaImIp3nS
prý	prý	k9
o	o	k7c4
dávný	dávný	k2eAgInSc4d1
mořský	mořský	k2eAgInSc4d1
záliv	záliv	k1gInSc4
zaplněný	zaplněný	k2eAgInSc1d1
říčními	říční	k2eAgInPc7d1
nánosy	nános	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc1d1
osud	osud	k1gInSc1
by	by	kYmCp3nS
podle	podle	k7c2
něho	on	k3xPp3gInSc2
mohl	moct	k5eAaImAgInS
potkat	potkat	k5eAaPmF
i	i	k9
například	například	k6eAd1
Perský	perský	k2eAgInSc4d1
záliv	záliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thesálská	Thesálský	k2eAgFnSc1d1
nížina	nížina	k1gFnSc1
bývala	bývat	k5eAaImAgFnS
podle	podle	k7c2
Hérodota	Hérodot	k1gMnSc2
jezerem	jezero	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
měl	mít	k5eAaImAgMnS
značně	značně	k6eAd1
kritický	kritický	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
využívaným	využívaný	k2eAgFnPc3d1
informacím	informace	k1gFnPc3
a	a	k8xC
pramenům	pramen	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
množství	množství	k1gNnSc4
tradičních	tradiční	k2eAgInPc2d1
mýtů	mýtus	k1gInPc2
–	–	k?
o	o	k7c6
jednookých	jednooký	k2eAgInPc6d1
Arimaspech	Arimasp	k1gInPc6
<g/>
,	,	kIx,
o	o	k7c6
gryfech	gryf	k1gInPc6
(	(	kIx(
<g/>
mytických	mytický	k2eAgInPc6d1
lvech	lev	k1gInPc6
s	s	k7c7
orlími	orlí	k2eAgFnPc7d1
hlavami	hlava	k1gFnPc7
a	a	k8xC
křídly	křídlo	k1gNnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
střežících	střežící	k2eAgInPc2d1
zlaté	zlatý	k2eAgInPc4d1
poklady	poklad	k1gInPc4
<g/>
,	,	kIx,
o	o	k7c6
bojích	boj	k1gInPc6
gryfů	gryf	k1gInPc2
s	s	k7c7
Arimaspy	Arimasp	k1gInPc7
apod.	apod.	kA
To	ten	k3xDgNnSc4
však	však	k9
nijak	nijak	k6eAd1
nesnižuje	snižovat	k5eNaImIp3nS
jeho	jeho	k3xOp3gFnPc4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
kritického	kritický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Crater	Crater	k1gMnSc1
Herodotus	Herodotus	k1gMnSc1
on	on	k3xPp3gMnSc1
Moon	Moon	k1gMnSc1
Gazetteer	Gazetteer	k1gMnSc1
of	of	k?
Planetary	Planetara	k1gFnSc2
Nomenclature	Nomenclatur	k1gMnSc5
<g/>
,	,	kIx,
IAU	IAU	kA
<g/>
,	,	kIx,
USGS	USGS	kA
<g/>
,	,	kIx,
NASA	NASA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Antonín	Antonín	k1gMnSc1
Rükl	Rükl	k1gMnSc1
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
Aventinum	Aventinum	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
Aristarchus	Aristarchus	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
62	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
mapového	mapový	k2eAgInSc2d1
listu	list	k1gInSc2
18	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85277-10-7	80-85277-10-7	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
548	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-200-1192-7	80-200-1192-7	k4
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
<g/>
,	,	kIx,
Dejiny	Dejin	k1gInPc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Tatran	Tatran	k1gInSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
624	#num#	k4
s.	s.	k?
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
548	#num#	k4
s.	s.	k?
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
<g/>
,	,	kIx,
Z	z	k7c2
dějin	dějiny	k1gFnPc2
východních	východní	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
.	.	kIx.
178	#num#	k4
s.	s.	k?
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
<g/>
,	,	kIx,
Hérodotovy	Hérodotův	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
I.	I.	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
III	III	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
Grégr	Grégr	k1gMnSc1
<g/>
,	,	kIx,
1863	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hérodotos	Hérodotos	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Hérodotos	Hérodotos	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Autor	autor	k1gMnSc1
Hérodotos	Hérodotos	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Autor	autor	k1gMnSc1
Herodotus	Herodotus	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Hérodotos	Hérodotos	k1gMnSc1
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1
na	na	k7c6
Antice	antika	k1gFnSc6
</s>
<s>
Řecký	řecký	k2eAgInSc1d1
a	a	k8xC
anglický	anglický	k2eAgInSc1d1
text	text	k1gInSc1
Hérodotových	Hérodotův	k2eAgFnPc2d1
Dějin	dějiny	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19981001510	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118549855	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2353	#num#	k4
0125	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79086888	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500257721	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
100225976	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79086888	#num#	k4
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Antika	antika	k1gFnSc1
</s>
