<s>
Hérodotos	Hérodotos	k1gMnSc1
z	z	k7c2
Halikarnássu	Halikarnáss	k1gInSc2
(	(	kIx(
<g/>
starořecky	starořecky	k6eAd1
Ἡ	Ἡ	k?
Ἁ	Ἁ	k?
<g/>
,	,	kIx,
asi	asi	k9
484	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
–	–	k?
asi	asi	k9
mezi	mezi	k7c7
lety	léto	k1gNnPc7
430	#num#	k4
a	a	k8xC
420	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
významným	významný	k2eAgMnSc7d1
antickým	antický	k2eAgMnSc7d1
historikem	historik	k1gMnSc7
<g/>
,	,	kIx,
proto	proto	k8xC
byl	být	k5eAaImAgMnS
Ciceronem	Cicero	k1gMnSc7
nazván	nazvat	k5eAaPmNgMnS,k5eAaBmNgMnS
otcem	otec	k1gMnSc7
dějepisu	dějepis	k1gInSc2
<g/>
.	.	kIx.
</s>