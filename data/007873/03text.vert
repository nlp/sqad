<s>
Turecko	Turecko	k1gNnSc1	Turecko
(	(	kIx(	(
<g/>
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Türkiye	Türkiye	k1gInSc1	Türkiye
Cumhuriyeti	Cumhuriyet	k1gMnPc1	Cumhuriyet
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Turecká	turecký	k2eAgFnSc1d1	turecká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
části	část	k1gFnSc2	část
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Gruzií	Gruzie	k1gFnSc7	Gruzie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Arménií	Arménie	k1gFnSc7	Arménie
<g/>
,	,	kIx,	,
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
(	(	kIx(	(
<g/>
exklávou	exklávat	k5eAaPmIp3nP	exklávat
Nachičevan	Nachičevan	k1gMnSc1	Nachičevan
<g/>
)	)	kIx)	)
a	a	k8xC	a
Íránem	Írán	k1gInSc7	Írán
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Irákem	Irák	k1gInSc7	Irák
a	a	k8xC	a
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihu	jih	k1gInSc2	jih
omývá	omývat	k5eAaImIp3nS	omývat
břehy	břeh	k1gInPc4	břeh
Turecka	Turecko	k1gNnSc2	Turecko
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Egejské	egejský	k2eAgNnSc4d1	Egejské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Turecka	Turecko	k1gNnSc2	Turecko
probíhá	probíhat	k5eAaImIp3nS	probíhat
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInPc4d1	severní
břehy	břeh	k1gInPc4	břeh
Turecka	Turecko	k1gNnSc2	Turecko
omývá	omývat	k5eAaImIp3nS	omývat
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Turecké	turecký	k2eAgNnSc1d1	turecké
území	území	k1gNnSc1	území
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
trvale	trvale	k6eAd1	trvale
osídleným	osídlený	k2eAgFnPc3d1	osídlená
oblastem	oblast	k1gFnPc3	oblast
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
obydlí	obydlí	k1gNnPc1	obydlí
vznikají	vznikat	k5eAaImIp3nP	vznikat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
neolitu	neolit	k1gInSc6	neolit
(	(	kIx(	(
<g/>
Çatalhöyük	Çatalhöyük	k1gMnSc1	Çatalhöyük
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
existovala	existovat	k5eAaImAgNnP	existovat
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
byla	být	k5eAaImAgFnS	být
Chetitská	chetitský	k2eAgFnSc1d1	Chetitská
říše	říše	k1gFnSc1	říše
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
říše	říš	k1gFnSc2	říš
Chetitů	Chetit	k1gMnPc2	Chetit
se	se	k3xPyFc4	se
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
státem	stát	k1gInSc7	stát
stala	stát	k5eAaPmAgFnS	stát
Frýgie	Frýgie	k1gFnSc1	Frýgie
dokud	dokud	k6eAd1	dokud
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
Kr.	Kr.	k1gMnPc7	Kr.
nerozvrátili	rozvrátit	k5eNaPmAgMnP	rozvrátit
kmeny	kmen	k1gInPc4	kmen
Kimmeriů	Kimmeri	k1gInPc2	Kimmeri
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
nástupnické	nástupnický	k2eAgInPc4d1	nástupnický
státy	stát	k1gInPc4	stát
Frýgie	Frýgie	k1gFnSc2	Frýgie
patřily	patřit	k5eAaImAgFnP	patřit
Lýdie	Lýdia	k1gFnPc1	Lýdia
<g/>
,	,	kIx,	,
Kárie	Kárie	k1gFnPc1	Kárie
a	a	k8xC	a
Lýkie	Lýkie	k1gFnPc1	Lýkie
<g/>
.	.	kIx.	.
</s>
<s>
Lýďané	Lýďan	k1gMnPc1	Lýďan
a	a	k8xC	a
Lycané	Lycan	k1gMnPc1	Lycan
mluvili	mluvit	k5eAaImAgMnP	mluvit
indoevropským	indoevropský	k2eAgInSc7d1	indoevropský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc4	tento
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
mimoindoevropské	mimoindoevropský	k2eAgInPc4d1	mimoindoevropský
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
Chetity	Chetit	k1gMnPc4	Chetit
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
Kr.	Kr.	k1gFnPc7	Kr.
bylo	být	k5eAaImAgNnS	být
západní	západní	k2eAgNnSc1d1	západní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Anatolie	Anatolie	k1gFnSc2	Anatolie
kolonizováno	kolonizovat	k5eAaBmNgNnS	kolonizovat
aiolskými	aiolský	k2eAgMnPc7d1	aiolský
<g/>
,	,	kIx,	,
dórskými	dórský	k2eAgMnPc7d1	dórský
a	a	k8xC	a
iónskými	iónský	k2eAgMnPc7d1	iónský
Řeky	Řek	k1gMnPc7	Řek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
před	před	k7c4	před
Kr.	Kr.	k1gFnSc4	Kr.
Řekové	Řek	k1gMnPc1	Řek
zkolonizovali	zkolonizovat	k5eAaPmAgMnP	zkolonizovat
celé	celý	k2eAgNnSc4d1	celé
severní	severní	k2eAgInSc4d1	severní
(	(	kIx(	(
<g/>
Pontos	Pontos	k1gInSc4	Pontos
<g/>
)	)	kIx)	)
a	a	k8xC	a
jižní	jižní	k2eAgNnSc4d1	jižní
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
Kr.	Kr.	k1gFnSc7	Kr.
byla	být	k5eAaImAgFnS	být
Anatolie	Anatolie	k1gFnSc1	Anatolie
dobyta	dobýt	k5eAaPmNgFnS	dobýt
Peršany	Peršan	k1gMnPc7	Peršan
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
vedli	vést	k5eAaImAgMnP	vést
Řekové	Řek	k1gMnPc1	Řek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Anatolii	Anatolie	k1gFnSc4	Anatolie
dobyl	dobýt	k5eAaPmAgInS	dobýt
Alexandr	Alexandr	k1gMnSc1	Alexandr
Makedonský	makedonský	k2eAgInSc1d1	makedonský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
několik	několik	k4yIc4	několik
helenistických	helenistický	k2eAgInPc2d1	helenistický
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Bithýnie	Bithýnie	k1gFnSc1	Bithýnie
<g/>
,	,	kIx,	,
Kappadokie	Kappadokie	k1gFnSc1	Kappadokie
<g/>
,	,	kIx,	,
Pergamon	pergamon	k1gInSc1	pergamon
a	a	k8xC	a
Pontos	Pontos	k1gInSc1	Pontos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
řeckých	řecký	k2eAgMnPc2d1	řecký
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
usadilo	usadit	k5eAaPmAgNnS	usadit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
helenizaci	helenizace	k1gFnSc3	helenizace
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
neměly	mít	k5eNaImAgInP	mít
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
trvání	trvání	k1gNnSc4	trvání
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
před	před	k7c7	před
Kr.	Kr.	k1gFnSc7	Kr.
všechny	všechen	k3xTgMnPc4	všechen
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
rozdělení	rozdělení	k1gNnSc6	rozdělení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
395	[number]	k4	395
území	území	k1gNnPc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Byzantské	byzantský	k2eAgFnPc4d1	byzantská
říše	říš	k1gFnPc4	říš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pronikání	pronikání	k1gNnSc3	pronikání
tureckých	turecký	k2eAgMnPc2d1	turecký
seldžuků	seldžuk	k1gMnPc2	seldžuk
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1299	[number]	k4	1299
se	se	k3xPyFc4	se
Osman	Osman	k1gMnSc1	Osman
I.	I.	kA	I.
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
sultánem	sultán	k1gMnSc7	sultán
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jako	jako	k9	jako
muslimský	muslimský	k2eAgInSc4d1	muslimský
stát	stát	k1gInSc4	stát
započala	započnout	k5eAaPmAgFnS	započnout
s	s	k7c7	s
výboji	výboj	k1gInPc7	výboj
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
i	i	k8xC	i
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1453	[number]	k4	1453
padla	padnout	k5eAaImAgFnS	padnout
poslední	poslední	k2eAgFnSc1d1	poslední
bašta	bašta	k1gFnSc1	bašta
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
byla	být	k5eAaImAgFnS	být
Turky	Turek	k1gMnPc4	Turek
poprvé	poprvé	k6eAd1	poprvé
obležena	obležen	k2eAgFnSc1d1	obležen
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
jako	jako	k8xC	jako
hlava	hlava	k1gFnSc1	hlava
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
měl	mít	k5eAaImAgInS	mít
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
neomezenou	omezený	k2eNgFnSc4d1	neomezená
(	(	kIx(	(
<g/>
absolutní	absolutní	k2eAgFnSc4d1	absolutní
<g/>
)	)	kIx)	)
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
vytlačování	vytlačování	k1gNnSc1	vytlačování
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
završeno	završen	k2eAgNnSc1d1	završeno
během	během	k7c2	během
balkánských	balkánský	k2eAgFnPc2d1	balkánská
válek	válka	k1gFnPc2	válka
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgFnPc2	svůj
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
arabských	arabský	k2eAgFnPc2d1	arabská
držav	država	k1gFnPc2	država
přišla	přijít	k5eAaPmAgFnS	přijít
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Mustafa	Mustaf	k1gMnSc4	Mustaf
Kemal	Kemal	k1gMnSc1	Kemal
Atatürk	Atatürk	k1gInSc4	Atatürk
založil	založit	k5eAaPmAgMnS	založit
Tureckou	turecký	k2eAgFnSc4d1	turecká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Atatürk	Atatürk	k1gInSc1	Atatürk
sekularizoval	sekularizovat	k5eAaBmAgInS	sekularizovat
a	a	k8xC	a
zmodernizoval	zmodernizovat	k5eAaPmAgInS	zmodernizovat
turecký	turecký	k2eAgInSc1d1	turecký
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Přenesl	přenést	k5eAaPmAgMnS	přenést
také	také	k9	také
sídlo	sídlo	k1gNnSc4	sídlo
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
parlamentu	parlament	k1gInSc2	parlament
z	z	k7c2	z
Istanbulu	Istanbul	k1gInSc2	Istanbul
do	do	k7c2	do
Ankary	Ankara	k1gFnSc2	Ankara
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
malým	malý	k2eAgNnSc7d1	malé
městem	město	k1gNnSc7	město
uvnitř	uvnitř	k7c2	uvnitř
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
se	se	k3xPyFc4	se
i	i	k9	i
přesto	přesto	k8xC	přesto
nadále	nadále	k6eAd1	nadále
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
a	a	k8xC	a
politickými	politický	k2eAgInPc7d1	politický
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
žije	žít	k5eAaImIp3nS	žít
jedenáct	jedenáct	k4xCc4	jedenáct
až	až	k9	až
čtrnáct	čtrnáct	k4xCc4	čtrnáct
milionů	milion	k4xCgInPc2	milion
Kurdů	Kurd	k1gMnPc2	Kurd
a	a	k8xC	a
vztah	vztah	k1gInSc1	vztah
turecké	turecký	k2eAgFnSc2d1	turecká
vlády	vláda	k1gFnSc2	vláda
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
národnostní	národnostní	k2eAgFnSc3d1	národnostní
menšině	menšina	k1gFnSc3	menšina
dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
současného	současný	k2eAgNnSc2d1	současné
Turecka	Turecko	k1gNnSc2	Turecko
je	být	k5eAaImIp3nS	být
neschopnost	neschopnost	k1gFnSc1	neschopnost
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
se	se	k3xPyFc4	se
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
minulostí	minulost	k1gFnSc7	minulost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
turecká	turecký	k2eAgFnSc1d1	turecká
vláda	vláda	k1gFnSc1	vláda
donedávna	donedávna	k6eAd1	donedávna
odmítala	odmítat	k5eAaImAgFnS	odmítat
připustit	připustit	k5eAaPmF	připustit
turecký	turecký	k2eAgInSc4d1	turecký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
genocidě	genocida	k1gFnSc6	genocida
Arménů	Armén	k1gMnPc2	Armén
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
nesnášenlivostí	nesnášenlivost	k1gFnSc7	nesnášenlivost
(	(	kIx(	(
<g/>
namířenou	namířený	k2eAgFnSc4d1	namířená
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
křesťanům	křesťan	k1gMnPc3	křesťan
žijícím	žijící	k2eAgNnSc7d1	žijící
na	na	k7c4	na
území	území	k1gNnSc4	území
Turecka	Turecko	k1gNnSc2	Turecko
–	–	k?	–
Řekům	Řek	k1gMnPc3	Řek
<g/>
,	,	kIx,	,
Arménům	Armén	k1gMnPc3	Armén
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
nacionalismem	nacionalismus	k1gInSc7	nacionalismus
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
namířeným	namířený	k2eAgInSc7d1	namířený
proti	proti	k7c3	proti
netureckým	turecký	k2eNgFnPc3d1	turecký
národnostem	národnost	k1gFnPc3	národnost
žijícím	žijící	k2eAgFnPc3d1	žijící
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
–	–	k?	–
Kurdům	Kurd	k1gMnPc3	Kurd
a	a	k8xC	a
dalším	další	k2eAgMnPc3d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
nevyřešená	vyřešený	k2eNgFnSc1d1	nevyřešená
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
také	také	k9	také
otázka	otázka	k1gFnSc1	otázka
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Kypru	Kypr	k1gInSc2	Kypr
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Turecko	Turecko	k1gNnSc1	Turecko
de	de	k?	de
facto	facto	k1gNnSc1	facto
okupuje	okupovat	k5eAaBmIp3nS	okupovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
bojují	bojovat	k5eAaImIp3nP	bojovat
kurdští	kurdský	k2eAgMnPc1d1	kurdský
separatisté	separatista	k1gMnPc1	separatista
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
kurdský	kurdský	k2eAgInSc4d1	kurdský
stát	stát	k1gInSc4	stát
a	a	k8xC	a
turecko-kurdský	tureckourdský	k2eAgInSc4d1	turecko-kurdský
konflikt	konflikt	k1gInSc4	konflikt
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
na	na	k7c4	na
40	[number]	k4	40
000	[number]	k4	000
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
zabila	zabít	k5eAaPmAgFnS	zabít
turecká	turecký	k2eAgFnSc1d1	turecká
armáda	armáda	k1gFnSc1	armáda
přes	přes	k7c4	přes
2000	[number]	k4	2000
kurdských	kurdský	k2eAgMnPc2d1	kurdský
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
podporuje	podporovat	k5eAaImIp3nS	podporovat
Turecko	Turecko	k1gNnSc4	Turecko
protivládní	protivládní	k2eAgNnSc4d1	protivládní
povstalce	povstalec	k1gMnSc2	povstalec
včetně	včetně	k7c2	včetně
syrské	syrský	k2eAgFnSc2d1	Syrská
odnože	odnož	k1gFnSc2	odnož
al-Káidy	al-Káida	k1gFnSc2	al-Káida
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
turecké	turecký	k2eAgFnSc2d1	turecká
armády	armáda	k1gFnSc2	armáda
pokusila	pokusit	k5eAaPmAgNnP	pokusit
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
údajné	údajný	k2eAgNnSc4d1	údajné
potlačování	potlačování	k1gNnSc4	potlačování
principů	princip	k1gInPc2	princip
sekulárního	sekulární	k2eAgInSc2d1	sekulární
státu	stát	k1gInSc2	stát
režimem	režim	k1gInSc7	režim
prezidenta	prezident	k1gMnSc2	prezident
Recepa	Recep	k1gMnSc2	Recep
Tayyipa	Tayyip	k1gMnSc2	Tayyip
Erdoğ	Erdoğ	k1gMnSc2	Erdoğ
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
byl	být	k5eAaImAgInS	být
potlačen	potlačit	k5eAaPmNgInS	potlačit
již	již	k6eAd1	již
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
čistky	čistka	k1gFnPc1	čistka
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
policii	policie	k1gFnSc3	policie
<g/>
,	,	kIx,	,
justici	justice	k1gFnSc3	justice
<g/>
,	,	kIx,	,
médiích	médium	k1gNnPc6	médium
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
125	[number]	k4	125
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
propuštěno	propustit	k5eAaPmNgNnS	propustit
nebo	nebo	k8xC	nebo
postaveno	postavit	k5eAaPmNgNnS	postavit
mimo	mimo	k7c4	mimo
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
36	[number]	k4	36
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
<g/>
.	.	kIx.	.
170	[number]	k4	170
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
100	[number]	k4	100
novinářů	novinář	k1gMnPc2	novinář
bylo	být	k5eAaImAgNnS	být
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
opozičních	opoziční	k2eAgMnPc2d1	opoziční
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zbavena	zbaven	k2eAgFnSc1d1	zbavena
imunity	imunita	k1gFnPc1	imunita
<g/>
,	,	kIx,	,
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zatýkání	zatýkání	k1gNnSc3	zatýkání
představitelů	představitel	k1gMnPc2	představitel
opoziční	opoziční	k2eAgFnSc2d1	opoziční
prokurdské	prokurdský	k2eAgFnSc2d1	prokurdský
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
se	se	k3xPyFc4	se
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
konalo	konat	k5eAaImAgNnS	konat
ústavní	ústavní	k2eAgNnSc1d1	ústavní
referendum	referendum	k1gNnSc1	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Navrhované	navrhovaný	k2eAgFnPc1d1	navrhovaná
změny	změna	k1gFnPc1	změna
turecké	turecký	k2eAgFnSc2d1	turecká
ústavy	ústava	k1gFnSc2	ústava
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
zavedení	zavedení	k1gNnSc4	zavedení
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
nahradil	nahradit	k5eAaPmAgInS	nahradit
stávající	stávající	k2eAgInSc1d1	stávající
parlamentní	parlamentní	k2eAgInSc1d1	parlamentní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Referendum	referendum	k1gNnSc1	referendum
skončilo	skončit	k5eAaPmAgNnS	skončit
poměrem	poměr	k1gInSc7	poměr
51,3	[number]	k4	51,3
%	%	kIx~	%
ku	k	k7c3	k
48,7	[number]	k4	48,7
%	%	kIx~	%
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
tento	tento	k3xDgInSc1	tento
těsný	těsný	k2eAgInSc1d1	těsný
výsledek	výsledek	k1gInSc1	výsledek
ukázal	ukázat	k5eAaPmAgInS	ukázat
rozpolcenost	rozpolcenost	k1gFnSc4	rozpolcenost
turecké	turecký	k2eAgFnSc2d1	turecká
společnosti	společnost	k1gFnSc2	společnost
mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnSc4	její
městskou	městský	k2eAgFnSc4d1	městská
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
většinově	většinově	k6eAd1	většinově
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
proti	proti	k7c3	proti
záměru	záměr	k1gInSc3	záměr
prezidenta	prezident	k1gMnSc2	prezident
Erdoğ	Erdoğ	k1gMnSc2	Erdoğ
posílit	posílit	k5eAaPmF	posílit
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
a	a	k8xC	a
spíše	spíše	k9	spíše
venkovským	venkovský	k2eAgFnPc3d1	venkovská
oblastem	oblast	k1gFnPc3	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zřetelně	zřetelně	k6eAd1	zřetelně
vyslovily	vyslovit	k5eAaPmAgInP	vyslovit
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
návrh	návrh	k1gInSc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Asijská	asijský	k2eAgFnSc1d1	asijská
část	část	k1gFnSc1	část
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Malá	malý	k2eAgFnSc1d1	malá
Asie	Asie	k1gFnSc1	Asie
(	(	kIx(	(
<g/>
historicky	historicky	k6eAd1	historicky
známým	známý	k1gMnSc7	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
97	[number]	k4	97
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
evropské	evropský	k2eAgFnSc2d1	Evropská
(	(	kIx(	(
<g/>
Východní	východní	k2eAgFnSc2d1	východní
Thrákie	Thrákie	k1gFnSc2	Thrákie
<g/>
)	)	kIx)	)
oddělena	oddělit	k5eAaPmNgFnS	oddělit
Marmarským	Marmarský	k2eAgNnSc7d1	Marmarské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
průlivy	průliv	k1gInPc1	průliv
Bospor	Bospor	k1gInSc1	Bospor
a	a	k8xC	a
Dardanely	Dardanely	k1gFnPc1	Dardanely
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
dohromady	dohromady	k6eAd1	dohromady
spojují	spojovat	k5eAaImIp3nP	spojovat
Černé	Černé	k2eAgNnSc4d1	Černé
a	a	k8xC	a
Egejské	egejský	k2eAgNnSc4d1	Egejské
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
přes	přes	k7c4	přes
1600	[number]	k4	1600
km	km	kA	km
a	a	k8xC	a
šířkou	šířka	k1gFnSc7	šířka
pod	pod	k7c4	pod
800	[number]	k4	800
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
783	[number]	k4	783
562	[number]	k4	562
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Gruzií	Gruzie	k1gFnSc7	Gruzie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Arménií	Arménie	k1gFnSc7	Arménie
<g/>
,	,	kIx,	,
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc7	jeho
exklávou	exkláva	k1gFnSc7	exkláva
Nachičevan	Nachičevan	k1gMnSc1	Nachičevan
<g/>
)	)	kIx)	)
a	a	k8xC	a
Íránem	Írán	k1gInSc7	Írán
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Irákem	Irák	k1gInSc7	Irák
a	a	k8xC	a
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
stranách	strana	k1gFnPc6	strana
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
moři	moře	k1gNnSc3	moře
<g/>
:	:	kIx,	:
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Egejským	egejský	k2eAgNnSc7d1	Egejské
<g/>
,	,	kIx,	,
a	a	k8xC	a
severu	sever	k1gInSc2	sever
Černým	černé	k1gNnSc7	černé
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Středozemním	středozemní	k2eAgInSc6d1	středozemní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
hornatá	hornatý	k2eAgFnSc1d1	hornatá
země	země	k1gFnSc1	země
a	a	k8xC	a
úzké	úzký	k2eAgInPc1d1	úzký
pruhy	pruh	k1gInPc1	pruh
nížin	nížina	k1gFnPc2	nížina
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jen	jen	k9	jen
při	při	k7c6	při
pobřežích	pobřeží	k1gNnPc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
rozlohy	rozloha	k1gFnSc2	rozloha
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
nad	nad	k7c7	nad
1200	[number]	k4	1200
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
méně	málo	k6eAd2	málo
než	než	k8xS	než
40	[number]	k4	40
%	%	kIx~	%
leží	ležet	k5eAaImIp3nS	ležet
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
460	[number]	k4	460
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
hory	hora	k1gFnPc1	hora
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Turecka	Turecko	k1gNnSc2	Turecko
Ararat	Ararat	k1gInSc1	Ararat
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
5165	[number]	k4	5165
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
leží	ležet	k5eAaImIp3nP	ležet
Pontské	pontský	k2eAgFnPc1d1	pontský
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
pak	pak	k6eAd1	pak
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Taurus	Taurus	k1gInSc1	Taurus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středozápadní	středozápadní	k2eAgFnSc6d1	středozápadní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Oblastí	oblast	k1gFnSc7	oblast
Turecka	Turecko	k1gNnSc2	Turecko
probíhají	probíhat	k5eAaImIp3nP	probíhat
dva	dva	k4xCgInPc4	dva
významné	významný	k2eAgInPc4d1	významný
zlomy	zlom	k1gInPc4	zlom
<g/>
;	;	kIx,	;
Severoanatolský	Severoanatolský	k2eAgInSc4d1	Severoanatolský
zlom	zlom	k1gInSc4	zlom
a	a	k8xC	a
Východoanatolský	Východoanatolský	k2eAgInSc4d1	Východoanatolský
zlom	zlom	k1gInSc4	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Složitá	složitý	k2eAgFnSc1d1	složitá
geologická	geologický	k2eAgFnSc1d1	geologická
struktura	struktura	k1gFnSc1	struktura
Turecka	Turecko	k1gNnSc2	Turecko
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
občasného	občasný	k2eAgInSc2d1	občasný
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
častých	častý	k2eAgNnPc2d1	časté
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
pramení	pramenit	k5eAaImIp3nS	pramenit
velké	velký	k2eAgFnPc4d1	velká
řeky	řeka	k1gFnPc4	řeka
Eufrat	Eufrat	k1gInSc4	Eufrat
<g/>
,	,	kIx,	,
Tigris	Tigris	k1gInSc4	Tigris
a	a	k8xC	a
Araks	Araks	k1gInSc4	Araks
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnPc1d3	nejdelší
řeky	řeka	k1gFnPc1	řeka
na	na	k7c6	na
tureckém	turecký	k2eAgNnSc6d1	turecké
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
Kı	Kı	k1gMnSc1	Kı
<g/>
,	,	kIx,	,
Sakarya	Sakarya	k1gMnSc1	Sakarya
a	a	k8xC	a
Yeşilı	Yeşilı	k1gMnSc1	Yeşilı
<g/>
,	,	kIx,	,
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc2d3	nejdelší
řeky	řeka	k1gFnSc2	řeka
ústící	ústící	k2eAgFnSc6d1	ústící
do	do	k7c2	do
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
jsou	být	k5eAaImIp3nP	být
Velký	velký	k2eAgInSc4d1	velký
Menderes	Menderes	k1gInSc4	Menderes
a	a	k8xC	a
Gediz	Gediz	k1gInSc4	Gediz
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Marica	Marica	k1gFnSc1	Marica
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
jezer	jezero	k1gNnPc2	jezero
jsou	být	k5eAaImIp3nP	být
Vanské	Vanský	k2eAgNnSc4d1	Vanský
jezero	jezero	k1gNnSc4	jezero
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
3714	[number]	k4	3714
km2	km2	k4	km2
a	a	k8xC	a
Tuz	Tuz	k1gMnSc1	Tuz
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
zhruba	zhruba	k6eAd1	zhruba
1550	[number]	k4	1550
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
dvě	dva	k4xCgNnPc1	dva
tato	tento	k3xDgNnPc1	tento
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
slaná	slaný	k2eAgNnPc1d1	slané
<g/>
.	.	kIx.	.
</s>
<s>
Obdělává	obdělávat	k5eAaImIp3nS	obdělávat
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
%	%	kIx~	%
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
11	[number]	k4	11
%	%	kIx~	%
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
pastvě	pastva	k1gFnSc3	pastva
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
na	na	k7c4	na
11	[number]	k4	11
%	%	kIx~	%
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
jsou	být	k5eAaImIp3nP	být
zasazeny	zasazen	k2eAgFnPc1d1	zasazena
trvalé	trvalý	k2eAgFnPc1d1	trvalá
plodiny	plodina	k1gFnPc1	plodina
a	a	k8xC	a
18	[number]	k4	18
%	%	kIx~	%
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
zavlažováno	zavlažován	k2eAgNnSc1d1	zavlažováno
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ekologické	ekologický	k2eAgFnPc4d1	ekologická
hrozby	hrozba	k1gFnPc4	hrozba
patří	patřit	k5eAaImIp3nS	patřit
znečištění	znečištění	k1gNnSc1	znečištění
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
deforestace	deforestace	k1gFnSc1	deforestace
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
úniku	únik	k1gInSc2	únik
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
z	z	k7c2	z
asi	asi	k9	asi
5000	[number]	k4	5000
obchodních	obchodní	k2eAgFnPc2d1	obchodní
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
každoročně	každoročně	k6eAd1	každoročně
proplují	proplout	k5eAaPmIp3nP	proplout
Bosporem	Bospor	k1gInSc7	Bospor
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
typy	typ	k1gInPc7	typ
půdy	půda	k1gFnSc2	půda
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
jsou	být	k5eAaImIp3nP	být
travnaté	travnatý	k2eAgFnPc4d1	travnatá
stepi	step	k1gFnPc4	step
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Anatolii	Anatolie	k1gFnSc6	Anatolie
<g/>
,	,	kIx,	,
a	a	k8xC	a
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
zbytek	zbytek	k1gInSc4	zbytek
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozmanitější	rozmanitý	k2eAgInPc1d3	nejrozmanitější
lesy	les	k1gInPc1	les
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejčastějšími	častý	k2eAgInPc7d3	nejčastější
stromy	strom	k1gInPc7	strom
jsou	být	k5eAaImIp3nP	být
habr	habr	k1gInSc4	habr
<g/>
,	,	kIx,	,
kaštanovník	kaštanovník	k1gInSc4	kaštanovník
setý	setý	k2eAgInSc4d1	setý
<g/>
,	,	kIx,	,
smrk	smrk	k1gInSc4	smrk
východní	východní	k2eAgInSc4d1	východní
<g/>
,	,	kIx,	,
olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
buk	buk	k1gInSc1	buk
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
a	a	k8xC	a
jedle	jedle	k1gFnPc1	jedle
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
rostliny	rostlina	k1gFnPc1	rostlina
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
pěnišník	pěnišník	k1gInSc1	pěnišník
<g/>
,	,	kIx,	,
vavřín	vavřín	k1gInSc1	vavřín
<g/>
,	,	kIx,	,
cesmína	cesmína	k1gFnSc1	cesmína
<g/>
,	,	kIx,	,
myrta	myrta	k1gFnSc1	myrta
a	a	k8xC	a
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
na	na	k7c4	na
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
začínají	začínat	k5eAaImIp3nP	začínat
převažovat	převažovat	k5eAaImF	převažovat
jehličnany	jehličnan	k1gInPc1	jehličnan
a	a	k8xC	a
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
nad	nad	k7c7	nad
2000	[number]	k4	2000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
alpinské	alpinský	k2eAgFnPc4d1	alpinská
louky	louka	k1gFnPc4	louka
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
divoké	divoký	k2eAgMnPc4d1	divoký
živočichy	živočich	k1gMnPc4	živočich
a	a	k8xC	a
lovné	lovný	k2eAgInPc4d1	lovný
druhy	druh	k1gInPc4	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
lidskou	lidský	k2eAgFnSc7d1	lidská
aktivitou	aktivita	k1gFnSc7	aktivita
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
živočichové	živočich	k1gMnPc1	živočich
jako	jako	k8xC	jako
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
bobr	bobr	k1gMnSc1	bobr
<g/>
,	,	kIx,	,
kuna	kuna	k1gFnSc1	kuna
<g/>
,	,	kIx,	,
šakal	šakal	k1gMnSc1	šakal
<g/>
,	,	kIx,	,
hyena	hyena	k1gFnSc1	hyena
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
jelen	jelen	k1gMnSc1	jelen
<g/>
,	,	kIx,	,
gazela	gazela	k1gFnSc1	gazela
a	a	k8xC	a
kamzík	kamzík	k1gMnSc1	kamzík
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
běžná	běžný	k2eAgNnPc4d1	běžné
domestikovaná	domestikovaný	k2eAgNnPc4d1	domestikované
zvířata	zvíře	k1gNnPc4	zvíře
patří	patřit	k5eAaImIp3nP	patřit
buvol	buvol	k1gMnSc1	buvol
<g/>
,	,	kIx,	,
koza	koza	k1gFnSc1	koza
<g/>
,	,	kIx,	,
velbloud	velbloud	k1gMnSc1	velbloud
<g/>
,	,	kIx,	,
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
osel	osel	k1gMnSc1	osel
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
a	a	k8xC	a
tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
nejrozšířenější	rozšířený	k2eAgFnPc1d3	nejrozšířenější
koroptve	koroptev	k1gFnPc1	koroptev
<g/>
,	,	kIx,	,
husy	husa	k1gFnPc1	husa
<g/>
,	,	kIx,	,
křepelky	křepelka	k1gFnPc1	křepelka
a	a	k8xC	a
dropi	drop	k1gMnPc1	drop
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
suché	suchý	k2eAgInPc1d1	suchý
<g/>
,	,	kIx,	,
z	z	k7c2	z
části	část	k1gFnSc2	část
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
podnebí	podnebí	k1gNnSc1	podnebí
Turecka	Turecko	k1gNnSc2	Turecko
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
přítomností	přítomnost	k1gFnSc7	přítomnost
moří	moře	k1gNnPc2	moře
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
západě	západ	k1gInSc6	západ
a	a	k8xC	a
hor	hora	k1gFnPc2	hora
vyskytujících	vyskytující	k2eAgFnPc2d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
oblasti	oblast	k1gFnPc1	oblast
mají	mít	k5eAaImIp3nP	mít
podnebí	podnebí	k1gNnSc2	podnebí
podobné	podobný	k2eAgFnSc2d1	podobná
tomu	ten	k3xDgNnSc3	ten
středozemnímu	středozemní	k2eAgInSc3d1	středozemní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
chladnější	chladný	k2eAgFnPc1d2	chladnější
zimy	zima	k1gFnPc1	zima
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
větší	veliký	k2eAgInPc1d2	veliký
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
letními	letní	k2eAgFnPc7d1	letní
a	a	k8xC	a
zimními	zimní	k2eAgFnPc7d1	zimní
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
sahají	sahat	k5eAaImIp3nP	sahat
jarní	jarní	k2eAgFnPc1d1	jarní
teploty	teplota	k1gFnPc1	teplota
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
a	a	k8xC	a
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
teploty	teplota	k1gFnSc2	teplota
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
oblastech	oblast	k1gFnPc6	oblast
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
zhruba	zhruba	k6eAd1	zhruba
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
východě	východ	k1gInSc6	východ
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobřežích	pobřeží	k1gNnPc6	pobřeží
jarní	jarní	k2eAgFnSc2d1	jarní
teploty	teplota	k1gFnSc2	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
stoupají	stoupat	k5eAaImIp3nP	stoupat
až	až	k9	až
na	na	k7c4	na
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
305	[number]	k4	305
<g/>
–	–	k?	–
<g/>
406	[number]	k4	406
mm	mm	kA	mm
a	a	k8xC	a
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
výškách	výška	k1gFnPc6	výška
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
administrativně	administrativně	k6eAd1	administrativně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
81	[number]	k4	81
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
il	il	k?	il
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
iller	illra	k1gFnPc2	illra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
děleny	dělen	k2eAgInPc4d1	dělen
na	na	k7c4	na
okresy	okres	k1gInPc4	okres
a	a	k8xC	a
podokresy	podokresa	k1gFnPc4	podokresa
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnPc1	provincie
se	se	k3xPyFc4	se
seskupují	seskupovat	k5eAaImIp3nP	seskupovat
do	do	k7c2	do
7	[number]	k4	7
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
bölgesi	bölgese	k1gFnSc4	bölgese
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
bölgeleri	bölgeler	k1gFnSc2	bölgeler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
členění	členění	k1gNnSc1	členění
však	však	k9	však
slouží	sloužit	k5eAaImIp3nS	sloužit
statistickým	statistický	k2eAgInPc3d1	statistický
účelům	účel	k1gInPc3	účel
a	a	k8xC	a
netvoří	tvořit	k5eNaImIp3nP	tvořit
úroveň	úroveň	k1gFnSc4	úroveň
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Nejlidnatějšími	lidnatý	k2eAgNnPc7d3	nejlidnatější
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Istanbul	Istanbul	k1gInSc4	Istanbul
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
10	[number]	k4	10
378	[number]	k4	378
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Ankara	Ankara	k1gFnSc1	Ankara
(	(	kIx(	(
<g/>
3	[number]	k4	3
846	[number]	k4	846
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
İ	İ	k?	İ
(	(	kIx(	(
<g/>
2	[number]	k4	2
679	[number]	k4	679
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bursa	bursa	k1gFnSc1	bursa
(	(	kIx(	(
<g/>
1	[number]	k4	1
559	[number]	k4	559
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Adana	Adana	k1gFnSc1	Adana
(	(	kIx(	(
<g/>
1	[number]	k4	1
339	[number]	k4	339
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Regiony	region	k1gInPc1	region
a	a	k8xC	a
provincie	provincie	k1gFnPc1	provincie
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
Marmarský	Marmarský	k2eAgInSc1d1	Marmarský
region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
Marmara	Marmara	k1gFnSc1	Marmara
Bölgesi	Bölgese	k1gFnSc4	Bölgese
<g/>
)	)	kIx)	)
Balı	Balı	k1gMnSc1	Balı
<g/>
,	,	kIx,	,
Bilecik	Bilecik	k1gMnSc1	Bilecik
<g/>
,	,	kIx,	,
Bursa	bursa	k1gFnSc1	bursa
<g/>
,	,	kIx,	,
Çanakkale	Çanakkal	k1gMnSc5	Çanakkal
<g/>
,	,	kIx,	,
Edirne	Edirn	k1gInSc5	Edirn
<g/>
,	,	kIx,	,
İ	İ	k?	İ
<g/>
,	,	kIx,	,
Kı	Kı	k1gFnPc7	Kı
<g/>
,	,	kIx,	,
Kocaeli	Kocael	k1gInPc7	Kocael
<g/>
,	,	kIx,	,
Sakarya	Sakarya	k1gMnSc1	Sakarya
<g/>
,	,	kIx,	,
Tekirdağ	Tekirdağ	k1gMnSc1	Tekirdağ
<g/>
,	,	kIx,	,
Yalova	Yalov	k1gInSc2	Yalov
Egejský	egejský	k2eAgInSc1d1	egejský
region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Ege	Ege	k?	Ege
Bölgesi	Bölgese	k1gFnSc4	Bölgese
<g/>
)	)	kIx)	)
Afyonkarahisar	Afyonkarahisar	k1gMnSc1	Afyonkarahisar
<g/>
,	,	kIx,	,
Aydı	Aydı	k1gMnSc1	Aydı
<g/>
,	,	kIx,	,
Denizli	Denizli	k1gMnSc1	Denizli
<g/>
,	,	kIx,	,
İ	İ	k?	İ
<g/>
,	,	kIx,	,
Kütahya	Kütahyus	k1gMnSc4	Kütahyus
<g/>
,	,	kIx,	,
Manisa	Manis	k1gMnSc4	Manis
<g/>
,	,	kIx,	,
Muğ	Muğ	k1gMnSc4	Muğ
<g/>
,	,	kIx,	,
Uşak	Uşak	k1gMnSc1	Uşak
Středomořský	středomořský	k2eAgInSc4d1	středomořský
region	region	k1gInSc4	region
(	(	kIx(	(
<g/>
Akdeniz	Akdeniz	k1gInSc4	Akdeniz
Bölgesi	Bölgese	k1gFnSc4	Bölgese
<g/>
)	)	kIx)	)
Adana	Adana	k1gFnSc1	Adana
<g/>
,	,	kIx,	,
Antalya	Antalya	k1gMnSc1	Antalya
<g/>
,	,	kIx,	,
Burdur	Burdur	k1gMnSc1	Burdur
<g/>
,	,	kIx,	,
Hatay	Hatay	k1gInPc1	Hatay
<g/>
,	,	kIx,	,
Isparta	Isparta	k1gFnSc1	Isparta
<g/>
,	,	kIx,	,
Kahramanmaraş	Kahramanmaraş	k1gFnSc1	Kahramanmaraş
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Mersin	Mersin	k1gInSc1	Mersin
<g/>
,	,	kIx,	,
Osmaniye	Osmaniye	k1gFnSc1	Osmaniye
Střední	střední	k2eAgFnSc2d1	střední
Anatolie	Anatolie	k1gFnSc2	Anatolie
(	(	kIx(	(
<g/>
İ	İ	k?	İ
Anadolu	Anadola	k1gFnSc4	Anadola
Bölgesi	Bölgese	k1gFnSc4	Bölgese
<g/>
)	)	kIx)	)
Aksaray	Aksaray	k1gInPc1	Aksaray
<g/>
,	,	kIx,	,
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Çankı	Çankı	k1gMnSc1	Çankı
<g/>
,	,	kIx,	,
Eskişehir	Eskişehir	k1gMnSc1	Eskişehir
<g/>
,	,	kIx,	,
Karaman	Karaman	k1gMnSc1	Karaman
<g/>
,	,	kIx,	,
Kayseri	Kayser	k1gFnSc5	Kayser
<g/>
,	,	kIx,	,
Kı	Kı	k1gFnSc5	Kı
<g/>
,	,	kIx,	,
Kı	Kı	k1gFnSc5	Kı
<g/>
,	,	kIx,	,
Konya	Konya	k1gMnSc1	Konya
<g/>
,	,	kIx,	,
Nevşehir	Nevşehir	k1gMnSc1	Nevşehir
<g/>
,	,	kIx,	,
Niğ	Niğ	k1gMnSc1	Niğ
<g/>
,	,	kIx,	,
Sivas	Sivas	k1gMnSc1	Sivas
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Yozgat	Yozgat	k2eAgInSc4d1	Yozgat
Černomořský	černomořský	k2eAgInSc4d1	černomořský
region	region	k1gInSc4	region
(	(	kIx(	(
<g/>
Karadeniz	Karadeniz	k1gInSc4	Karadeniz
Bölgesi	Bölgese	k1gFnSc4	Bölgese
<g/>
)	)	kIx)	)
Amasya	Amasyum	k1gNnSc2	Amasyum
<g/>
,	,	kIx,	,
Artvin	Artvina	k1gFnPc2	Artvina
<g/>
,	,	kIx,	,
Bartı	Bartı	k1gFnPc2	Bartı
<g/>
,	,	kIx,	,
Bayburt	Bayburta	k1gFnPc2	Bayburta
<g/>
,	,	kIx,	,
Bolu	bol	k1gInSc2	bol
<g/>
,	,	kIx,	,
Çorum	Çorum	k1gInSc1	Çorum
<g/>
,	,	kIx,	,
Düzce	Düzce	k1gMnSc1	Düzce
<g/>
,	,	kIx,	,
Giresun	Giresun	k1gMnSc1	Giresun
<g/>
,	,	kIx,	,
Gümüşhane	Gümüşhan	k1gMnSc5	Gümüşhan
<g/>
,	,	kIx,	,
Karabük	Karabük	k1gInSc4	Karabük
<g/>
,	,	kIx,	,
Kastamonu	Kastamona	k1gFnSc4	Kastamona
<g/>
,	,	kIx,	,
Ordu	orda	k1gFnSc4	orda
<g/>
,	,	kIx,	,
Rize	Riga	k1gFnSc6	Riga
<g/>
,	,	kIx,	,
Samsun	Samsun	k1gMnSc1	Samsun
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Sinop	Sinop	k1gInSc4	Sinop
<g/>
,	,	kIx,	,
Tokat	tokat	k5eAaImF	tokat
<g/>
,	,	kIx,	,
Trabzon	Trabzon	k1gInSc1	Trabzon
<g/>
,	,	kIx,	,
Zonguldak	Zonguldak	k1gInSc1	Zonguldak
Východní	východní	k2eAgFnSc2d1	východní
Anatolie	Anatolie	k1gFnSc2	Anatolie
(	(	kIx(	(
<g/>
Doğ	Doğ	k1gMnSc1	Doğ
Anadolu	Anadola	k1gFnSc4	Anadola
Bölgesi	Bölgese	k1gFnSc4	Bölgese
<g/>
)	)	kIx)	)
Ağ	Ağ	k1gMnSc1	Ağ
<g/>
,	,	kIx,	,
Ardahan	Ardahan	k1gMnSc1	Ardahan
<g/>
,	,	kIx,	,
Bingöl	Bingöl	k1gMnSc1	Bingöl
<g/>
,	,	kIx,	,
Bitlis	Bitlis	k1gFnSc1	Bitlis
<g/>
,	,	kIx,	,
Elazı	Elazı	k1gFnSc1	Elazı
<g/>
,	,	kIx,	,
Erzincan	Erzincan	k1gInSc1	Erzincan
<g/>
,	,	kIx,	,
Erzurum	Erzurum	k1gNnSc1	Erzurum
<g/>
,	,	kIx,	,
Hakkâri	Hakkâri	k1gNnSc1	Hakkâri
<g/>
,	,	kIx,	,
Iğ	Iğ	k1gFnSc1	Iğ
<g/>
,	,	kIx,	,
Kars	Kars	k1gInSc1	Kars
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Malatya	Malatyus	k1gMnSc4	Malatyus
<g/>
,	,	kIx,	,
Muş	Muş	k1gMnSc4	Muş
<g/>
,	,	kIx,	,
Tunceli	Tunceli	k1gMnSc4	Tunceli
<g/>
,	,	kIx,	,
Van	van	k1gInSc4	van
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Anatolie	Anatolie	k1gFnSc2	Anatolie
(	(	kIx(	(
<g/>
Güneydoğ	Güneydoğ	k1gMnSc1	Güneydoğ
Anadolu	Anadola	k1gFnSc4	Anadola
Bölgesi	Bölgese	k1gFnSc4	Bölgese
<g/>
)	)	kIx)	)
Adı	Adı	k1gMnSc1	Adı
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
Diyarbakı	Diyarbakı	k1gMnSc1	Diyarbakı
<g/>
,	,	kIx,	,
Gaziantep	Gaziantep	k1gMnSc1	Gaziantep
<g/>
,	,	kIx,	,
Kilis	Kilis	k1gFnSc1	Kilis
<g/>
,	,	kIx,	,
Mardin	Mardina	k1gFnPc2	Mardina
<g/>
,	,	kIx,	,
Şanlı	Şanlı	k1gFnPc2	Şanlı
<g/>
,	,	kIx,	,
Siirt	Siirta	k1gFnPc2	Siirta
<g/>
,	,	kIx,	,
Şı	Şı	k1gFnPc4	Şı
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
formálně	formálně	k6eAd1	formálně
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
muslimskou	muslimský	k2eAgFnSc7d1	muslimská
většinou	většina	k1gFnSc7	většina
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
–	–	k?	–
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
již	již	k6eAd1	již
jen	jen	k6eAd1	jen
formálně	formálně	k6eAd1	formálně
–	–	k?	–
sekulárním	sekulární	k2eAgMnSc7d1	sekulární
politickým	politický	k2eAgInSc7d1	politický
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
větve	větev	k1gFnPc4	větev
–	–	k?	–
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
–	–	k?	–
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
na	na	k7c4	na
sobě	se	k3xPyFc3	se
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Recep	Recep	k1gMnSc1	Recep
Tayyip	Tayyip	k1gMnSc1	Tayyip
Erdoğ	Erdoğ	k1gMnSc1	Erdoğ
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
až	až	k9	až
2014	[number]	k4	2014
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
turecké	turecký	k2eAgFnSc2d1	turecká
ústavy	ústava	k1gFnSc2	ústava
má	mít	k5eAaImIp3nS	mít
prezident	prezident	k1gMnSc1	prezident
převážně	převážně	k6eAd1	převážně
ceremoniální	ceremoniální	k2eAgFnSc4d1	ceremoniální
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k9	již
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
zvolení	zvolení	k1gNnSc2	zvolení
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
Erdoğ	Erdoğ	k1gFnSc1	Erdoğ
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
de	de	k?	de
facto	facto	k1gNnSc1	facto
svrchovanou	svrchovaný	k2eAgFnSc4d1	svrchovaná
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
pravomoce	pravomoc	k1gFnSc2	pravomoc
náležely	náležet	k5eAaImAgInP	náležet
i	i	k9	i
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
tvoří	tvořit	k5eAaImIp3nS	tvořit
Rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
Binali	Binali	k1gMnSc1	Binali
Yı	Yı	k1gMnSc1	Yı
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
představuje	představovat	k5eAaImIp3nS	představovat
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
má	mít	k5eAaImIp3nS	mít
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
částečně	částečně	k6eAd1	částečně
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
civilní	civilní	k2eAgFnSc6d1	civilní
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
patří	patřit	k5eAaImIp3nS	patřit
Turecko	Turecko	k1gNnSc4	Turecko
k	k	k7c3	k
demokratičtějším	demokratický	k2eAgFnPc3d2	demokratičtější
zemím	zem	k1gFnPc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
jsou	být	k5eAaImIp3nP	být
svobodné	svobodný	k2eAgFnPc1d1	svobodná
a	a	k8xC	a
spravedlivé	spravedlivý	k2eAgFnPc1d1	spravedlivá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
mají	mít	k5eAaImIp3nP	mít
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
země	zem	k1gFnSc2	zem
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
určitým	určitý	k2eAgInSc7d1	určitý
etnickým	etnický	k2eAgInSc7d1	etnický
(	(	kIx(	(
<g/>
Kurdové	Kurd	k1gMnPc5	Kurd
<g/>
)	)	kIx)	)
a	a	k8xC	a
náboženským	náboženský	k2eAgFnPc3d1	náboženská
menšinám	menšina	k1gFnPc3	menšina
některá	některý	k3yIgNnPc1	některý
práva	právo	k1gNnPc1	právo
odepřena	odepřen	k2eAgFnSc1d1	odepřena
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
udržuje	udržovat	k5eAaImIp3nS	udržovat
pořádek	pořádek	k1gInSc4	pořádek
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působí	působit	k5eAaImIp3nP	působit
militantní	militantní	k2eAgFnPc4d1	militantní
kurdské	kurdský	k2eAgFnPc4d1	kurdská
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
chce	chtít	k5eAaImIp3nS	chtít
Turecko	Turecko	k1gNnSc1	Turecko
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zemi	zem	k1gFnSc4	zem
s	s	k7c7	s
transformací	transformace	k1gFnSc7	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nezaručená	zaručený	k2eNgFnSc1d1	nezaručená
svoboda	svoboda	k1gFnSc1	svoboda
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vláda	vláda	k1gFnSc1	vláda
postihuje	postihovat	k5eAaImIp3nS	postihovat
kritiku	kritika	k1gFnSc4	kritika
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
státních	státní	k2eAgFnPc2d1	státní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hranici	hranice	k1gFnSc4	hranice
10	[number]	k4	10
%	%	kIx~	%
volebních	volební	k2eAgInPc2d1	volební
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
hranice	hranice	k1gFnSc1	hranice
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
země	zem	k1gFnSc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
malé	malý	k2eAgFnPc1d1	malá
strany	strana	k1gFnPc1	strana
nechávají	nechávat	k5eAaImIp3nP	nechávat
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
vstoupit	vstoupit	k5eAaPmF	vstoupit
jako	jako	k9	jako
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
když	když	k8xS	když
se	se	k3xPyFc4	se
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
dostanou	dostat	k5eAaPmIp3nP	dostat
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
stranám	strana	k1gFnPc3	strana
znovu	znovu	k6eAd1	znovu
přihlásit	přihlásit	k5eAaPmF	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnějšími	silný	k2eAgFnPc7d3	nejsilnější
stranami	strana	k1gFnPc7	strana
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
jsou	být	k5eAaImIp3nP	být
středopravá	středopravý	k2eAgFnSc1d1	středopravá
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
Strana	strana	k1gFnSc1	strana
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
AKP	AKP	kA	AKP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
získala	získat	k5eAaPmAgFnS	získat
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
středolevá	středolevý	k2eAgFnSc1d1	středolevá
kemalistická	kemalistický	k2eAgFnSc1d1	kemalistická
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
lidová	lidový	k2eAgFnSc1d1	lidová
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
CHP	CHP	kA	CHP
<g/>
)	)	kIx)	)
s	s	k7c7	s
26	[number]	k4	26
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
ultrapravicová	ultrapravicový	k2eAgFnSc1d1	ultrapravicová
Strana	strana	k1gFnSc1	strana
národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
se	s	k7c7	s
13	[number]	k4	13
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
36	[number]	k4	36
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
550	[number]	k4	550
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
získali	získat	k5eAaPmAgMnP	získat
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
k	k	k7c3	k
prokurdským	prokurdský	k2eAgFnPc3d1	prokurdský
stranám	strana	k1gFnPc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgNnPc1d1	lidské
práva	právo	k1gNnPc1	právo
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
byla	být	k5eAaImAgFnS	být
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
diskusí	diskuse	k1gFnPc2	diskuse
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
odsouzení	odsouzení	k1gNnPc2	odsouzení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2008	[number]	k4	2008
Evropský	evropský	k2eAgInSc4d1	evropský
soud	soud	k1gInSc4	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
provedl	provést	k5eAaPmAgMnS	provést
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
600	[number]	k4	600
rozsudků	rozsudek	k1gInPc2	rozsudek
proti	proti	k7c3	proti
Turecku	Turecko	k1gNnSc3	Turecko
za	za	k7c4	za
porušování	porušování	k1gNnPc4	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
život	život	k1gInSc4	život
a	a	k8xC	a
zákaz	zákaz	k1gInSc4	zákaz
mučení	mučení	k1gNnPc2	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
otázky	otázka	k1gFnPc1	otázka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
práva	právo	k1gNnPc1	právo
Kurdů	Kurd	k1gMnPc2	Kurd
<g/>
,	,	kIx,	,
práva	právo	k1gNnSc2	právo
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
LGBT	LGBT	kA	LGBT
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
svoboda	svoboda	k1gFnSc1	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
také	také	k9	také
přitahovaly	přitahovat	k5eAaImAgFnP	přitahovat
diskuse	diskuse	k1gFnPc1	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
významnou	významný	k2eAgFnSc7d1	významná
překážkou	překážka	k1gFnSc7	překážka
v	v	k7c4	v
(	(	kIx(	(
<g/>
možná	možná	k6eAd1	možná
budoucímu	budoucí	k2eAgMnSc3d1	budoucí
<g/>
)	)	kIx)	)
členství	členství	k1gNnSc3	členství
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Výboru	výbor	k1gInSc2	výbor
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
novinářů	novinář	k1gMnPc2	novinář
(	(	kIx(	(
<g/>
VON	von	k1gInSc1	von
<g/>
)	)	kIx)	)
zavedla	zavést	k5eAaPmAgFnS	zavést
vláda	vláda	k1gFnSc1	vláda
AKP	AKP	kA	AKP
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
zásahů	zásah	k1gInPc2	zásah
proti	proti	k7c3	proti
svobodě	svoboda	k1gFnSc3	svoboda
médií	médium	k1gNnPc2	médium
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
novinářů	novinář	k1gMnPc2	novinář
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
kvůli	kvůli	k7c3	kvůli
obviněním	obvinění	k1gNnPc3	obvinění
z	z	k7c2	z
"	"	kIx"	"
<g/>
terorismu	terorismus	k1gInSc2	terorismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
protistátní	protistátní	k2eAgMnPc1d1	protistátní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
případy	případ	k1gInPc1	případ
Ergenekon	Ergenekona	k1gFnPc2	Ergenekona
a	a	k8xC	a
Balyoz	Balyoza	k1gFnPc2	Balyoza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tisíce	tisíc	k4xCgInPc1	tisíc
dalších	další	k2eAgMnPc2d1	další
byly	být	k5eAaImAgInP	být
vyšetřovaných	vyšetřovaný	k2eAgMnPc2d1	vyšetřovaný
kvůli	kvůli	k7c3	kvůli
"	"	kIx"	"
<g/>
pošpiňovaní	pošpiňovaný	k2eAgMnPc1d1	pošpiňovaný
tureckosti	tureckost	k1gFnPc4	tureckost
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
urážení	urážení	k1gNnSc1	urážení
islámu	islám	k1gInSc2	islám
<g/>
"	"	kIx"	"
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zasít	zasít	k5eAaPmF	zasít
autocenzuru	autocenzura	k1gFnSc4	autocenzura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
identifikoval	identifikovat	k5eAaBmAgInS	identifikovat
VON	von	k1gInSc4	von
76	[number]	k4	76
vězněných	vězněný	k2eAgMnPc2d1	vězněný
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
61	[number]	k4	61
přímo	přímo	k6eAd1	přímo
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Turecko	Turecko	k1gNnSc1	Turecko
předčilo	předčít	k5eAaPmAgNnS	předčít
i	i	k9	i
země	zem	k1gFnPc1	zem
jako	jako	k8xC	jako
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Eritrea	Eritrea	k1gFnSc1	Eritrea
či	či	k8xC	či
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
také	také	k9	také
9	[number]	k4	9
uvězněných	uvězněný	k2eAgMnPc2d1	uvězněný
hudebníků	hudebník	k1gMnPc2	hudebník
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
tvorbu	tvorba	k1gFnSc4	tvorba
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
Turecko	Turecko	k1gNnSc4	Turecko
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
mluvčí	mluvčí	k1gMnSc1	mluvčí
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
J.	J.	kA	J.
Crowley	Crowlea	k1gFnPc1	Crowlea
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
USA	USA	kA	USA
mají	mít	k5eAaImIp3nP	mít
"	"	kIx"	"
<g/>
široké	široký	k2eAgFnPc1d1	široká
obavy	obava	k1gFnPc1	obava
ohledně	ohledně	k7c2	ohledně
trendů	trend	k1gInPc2	trend
zahrnujících	zahrnující	k2eAgInPc2d1	zahrnující
zastrašování	zastrašování	k1gNnSc4	zastrašování
novinářů	novinář	k1gMnPc2	novinář
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
podle	podle	k7c2	podle
organizace	organizace	k1gFnSc2	organizace
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc4	house
doposud	doposud	k6eAd1	doposud
hodnocení	hodnocení	k1gNnSc4	hodnocení
'	'	kIx"	'
<g/>
částečně	částečně	k6eAd1	částečně
svobodná	svobodný	k2eAgFnSc1d1	svobodná
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
však	však	k9	však
Turecko	Turecko	k1gNnSc1	Turecko
zmítá	zmítat	k5eAaImIp3nS	zmítat
v	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
čistkách	čistka	k1gFnPc6	čistka
a	a	k8xC	a
zatýkání	zatýkání	k1gNnSc6	zatýkání
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
ve	v	k7c6	v
státním	státní	k2eAgInSc6d1	státní
a	a	k8xC	a
justičním	justiční	k2eAgInSc6d1	justiční
aparátě	aparát	k1gInSc6	aparát
<g/>
,	,	kIx,	,
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
i	i	k8xC	i
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
např.	např.	kA	např.
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
;	;	kIx,	;
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
;	;	kIx,	;
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
islámské	islámský	k2eAgFnSc2d1	islámská
spolupráce	spolupráce	k1gFnSc2	spolupráce
(	(	kIx(	(
<g/>
OIC	OIC	kA	OIC
<g/>
;	;	kIx,	;
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
OBSE	OBSE	kA	OBSE
<g/>
;	;	kIx,	;
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
(	(	kIx(	(
<g/>
ECO	ECO	kA	ECO
<g/>
;	;	kIx,	;
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
černomořské	černomořský	k2eAgFnSc2d1	černomořská
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
spolupráce	spolupráce	k1gFnSc2	spolupráce
(	(	kIx(	(
<g/>
BSEC	BSEC	kA	BSEC
<g/>
;	;	kIx,	;
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
D-8	D-8	k1gMnSc1	D-8
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
G20	G20	k1gMnSc1	G20
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
je	být	k5eAaImIp3nS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
<g/>
,	,	kIx,	,
ekonomicky	ekonomicky	k6eAd1	ekonomicky
a	a	k8xC	a
bezpečnostně	bezpečnostně	k6eAd1	bezpečnostně
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
vázat	vázat	k5eAaImF	vázat
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
západní	západní	k2eAgInPc4d1	západní
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgNnP	být
zahájena	zahájit	k5eAaPmNgNnP	zahájit
jednání	jednání	k1gNnPc1	jednání
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
zkomplikována	zkomplikovat	k5eAaPmNgFnS	zkomplikovat
zahraničními	zahraniční	k2eAgInPc7d1	zahraniční
vztahy	vztah	k1gInPc7	vztah
s	s	k7c7	s
Kyprem	Kypr	k1gInSc7	Kypr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Turecko	Turecko	k1gNnSc4	Turecko
okupuje	okupovat	k5eAaBmIp3nS	okupovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
hlavními	hlavní	k2eAgFnPc7d1	hlavní
snahami	snaha	k1gFnPc7	snaha
Turecka	Turecko	k1gNnSc2	Turecko
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
jsou	být	k5eAaImIp3nP	být
zajištění	zajištění	k1gNnSc4	zajištění
silné	silný	k2eAgFnSc2d1	silná
pozice	pozice	k1gFnSc2	pozice
mezi	mezi	k7c7	mezi
turkickými	turkický	k2eAgFnPc7d1	turkická
republikami	republika	k1gFnPc7	republika
a	a	k8xC	a
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
donedávna	donedávna	k6eAd1	donedávna
i	i	k9	i
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
turkických	turkický	k2eAgInPc2d1	turkický
států	stát	k1gInPc2	stát
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgFnPc7	který
Turecko	Turecko	k1gNnSc1	Turecko
sdílí	sdílet	k5eAaImIp3nS	sdílet
společné	společný	k2eAgNnSc4d1	společné
kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
jazykové	jazykový	k2eAgNnSc4d1	jazykové
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Turecku	Turecko	k1gNnSc6	Turecko
rozšířit	rozšířit	k5eAaPmF	rozšířit
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
vztahy	vztah	k1gInPc4	vztah
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
dokončení	dokončení	k1gNnSc3	dokončení
finančně	finančně	k6eAd1	finančně
náročného	náročný	k2eAgInSc2d1	náročný
projektu	projekt	k1gInSc2	projekt
ropovodu	ropovod	k1gInSc2	ropovod
a	a	k8xC	a
plynovodu	plynovod	k1gInSc2	plynovod
z	z	k7c2	z
ázerbájdžánského	ázerbájdžánský	k2eAgNnSc2d1	ázerbájdžánské
Baku	Baku	k1gNnSc2	Baku
do	do	k7c2	do
tureckého	turecký	k2eAgInSc2d1	turecký
přístavu	přístav	k1gInSc2	přístav
Ceyhan	Ceyhana	k1gFnPc2	Ceyhana
<g/>
.	.	kIx.	.
</s>
<s>
Ropovod	ropovod	k1gInSc1	ropovod
Baku-Tbilisi-Ceyhan	Baku-Tbilisi-Ceyhany	k1gInPc2	Baku-Tbilisi-Ceyhany
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
turecké	turecký	k2eAgFnSc2d1	turecká
zahraničněpolitické	zahraničněpolitický	k2eAgFnSc2d1	zahraničněpolitická
strategie	strategie	k1gFnSc2	strategie
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
energetickým	energetický	k2eAgInSc7d1	energetický
přivaděčem	přivaděč	k1gInSc7	přivaděč
pro	pro	k7c4	pro
Západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Arménií	Arménie	k1gFnSc7	Arménie
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
zavřená	zavřený	k2eAgFnSc1d1	zavřená
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
turecké	turecký	k2eAgFnSc3d1	turecká
podpoře	podpora	k1gFnSc3	podpora
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Náhorním	náhorní	k2eAgInSc6d1	náhorní
Karabachu	Karabach	k1gInSc6	Karabach
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
AKP	AKP	kA	AKP
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
vliv	vliv	k1gInSc4	vliv
země	zem	k1gFnSc2	zem
v	v	k7c6	v
prvně	prvně	k?	prvně
jmenovaných	jmenovaný	k2eAgFnPc6d1	jmenovaná
oblastech	oblast	k1gFnPc6	oblast
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Balkánu	Balkán	k1gInSc6	Balkán
na	na	k7c6	na
základě	základ	k1gInSc6	základ
doktríny	doktrína	k1gFnSc2	doktrína
"	"	kIx"	"
<g/>
strategické	strategický	k2eAgFnSc2d1	strategická
hloubky	hloubka	k1gFnSc2	hloubka
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
termín	termín	k1gInSc1	termín
zavedl	zavést	k5eAaPmAgInS	zavést
Ahmet	Ahmet	k1gInSc4	Ahmet
Davutoğ	Davutoğ	k1gFnSc2	Davutoğ
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
definici	definice	k1gFnSc4	definice
pro	pro	k7c4	pro
vzrůstající	vzrůstající	k2eAgNnSc4d1	vzrůstající
turecké	turecký	k2eAgNnSc4d1	turecké
angažování	angažování	k1gNnSc4	angažování
se	se	k3xPyFc4	se
v	v	k7c6	v
regionální	regionální	k2eAgFnSc6d1	regionální
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
také	také	k9	také
neo-osmanismus	neosmanismus	k1gInSc4	neo-osmanismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
arabském	arabský	k2eAgInSc6d1	arabský
jaru	jar	k1gInSc6	jar
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2010	[number]	k4	2010
a	a	k8xC	a
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
AKP	AKP	kA	AKP
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
jistých	jistý	k2eAgFnPc2d1	jistá
opozičních	opoziční	k2eAgFnPc2d1	opoziční
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
zasažených	zasažený	k2eAgFnPc6d1	zasažená
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
několika	několik	k4yIc7	několik
arabskými	arabský	k2eAgInPc7d1	arabský
státy	stát	k1gInPc7	stát
-	-	kIx~	-
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
se	s	k7c7	s
sousední	sousední	k2eAgFnSc7d1	sousední
Sýrií	Sýrie	k1gFnSc7	Sýrie
a	a	k8xC	a
po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
prezidenta	prezident	k1gMnSc2	prezident
Muhammada	Muhammada	k1gFnSc1	Muhammada
Mursího	Mursí	k1gMnSc2	Mursí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
Turecko	Turecko	k1gNnSc1	Turecko
nemělo	mít	k5eNaImAgNnS	mít
diplomatické	diplomatický	k2eAgNnSc1d1	diplomatické
zastoupení	zastoupení	k1gNnSc1	zastoupení
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
posledně	posledně	k6eAd1	posledně
jmenovanou	jmenovaný	k2eAgFnSc7d1	jmenovaná
zemí	zem	k1gFnSc7	zem
se	se	k3xPyFc4	se
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
po	po	k7c6	po
izraelském	izraelský	k2eAgNnSc6d1	izraelské
bombardování	bombardování	k1gNnSc6	bombardování
Pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
Turecku	Turecko	k1gNnSc3	Turecko
v	v	k7c6	v
Levantě	Levanta	k1gFnSc6	Levanta
(	(	kIx(	(
<g/>
s	s	k7c7	s
nedávnými	dávný	k2eNgInPc7d1	nedávný
bohatými	bohatý	k2eAgInPc7d1	bohatý
nálezy	nález	k1gInPc7	nález
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
jen	jen	k6eAd1	jen
málo	málo	k4c1	málo
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
původními	původní	k2eAgInPc7d1	původní
plány	plán	k1gInPc1	plán
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
nynějšího	nynější	k2eAgMnSc2d1	nynější
premiéra	premiér	k1gMnSc2	premiér
Davutoğ	Davutoğ	k1gMnSc2	Davutoğ
na	na	k7c4	na
zahraničně-politickou	zahraničněolitický	k2eAgFnSc4d1	zahraničně-politická
doktrínu	doktrína	k1gFnSc4	doktrína
"	"	kIx"	"
<g/>
žádných	žádný	k3yNgInPc2	žádný
problémů	problém	k1gInPc2	problém
se	s	k7c7	s
sousedy	soused	k1gMnPc7	soused
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
již	již	k6eAd1	již
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
udržuje	udržovat	k5eAaImIp3nS	udržovat
jednotky	jednotka	k1gFnPc4	jednotka
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
misích	mise	k1gFnPc6	mise
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
OSN	OSN	kA	OSN
a	a	k8xC	a
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
peacekeepingových	peacekeepingový	k2eAgFnPc2d1	peacekeepingová
misí	mise	k1gFnPc2	mise
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc3d1	bývalá
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
a	a	k8xC	a
podpory	podpora	k1gFnSc2	podpora
koaličních	koaliční	k2eAgNnPc2d1	koaliční
vojsk	vojsko	k1gNnPc2	vojsko
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
36	[number]	k4	36
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
Severním	severní	k2eAgInSc6d1	severní
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
,	,	kIx,	,
a	a	k8xC	a
asistovalo	asistovat	k5eAaImAgNnS	asistovat
Iráckému	irácký	k2eAgInSc3d1	irácký
Kurdistánu	Kurdistán	k1gInSc3	Kurdistán
s	s	k7c7	s
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
americké	americký	k2eAgFnSc2d1	americká
stabilizační	stabilizační	k2eAgFnSc2d1	stabilizační
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
a	a	k8xC	a
pod	pod	k7c7	pod
NATO	NATO	kA	NATO
spadajícího	spadající	k2eAgInSc2d1	spadající
ISAFu	ISAFus	k1gInSc2	ISAFus
vojáky	voják	k1gMnPc4	voják
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
dodává	dodávat	k5eAaImIp3nS	dodávat
Turecko	Turecko	k1gNnSc4	Turecko
vojenský	vojenský	k2eAgInSc1d1	vojenský
personál	personál	k1gInSc1	personál
pro	pro	k7c4	pro
Eurocorps	Eurocorps	k1gInSc4	Eurocorps
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
na	na	k7c6	na
iniciativě	iniciativa	k1gFnSc6	iniciativa
EU	EU	kA	EU
Battlegroup	Battlegroup	k1gInSc1	Battlegroup
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Československem	Československo	k1gNnSc7	Československo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
přátelské	přátelský	k2eAgInPc4d1	přátelský
<g/>
,	,	kIx,	,
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInPc1d1	obchodní
vztahy	vztah	k1gInPc1	vztah
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
Česko	Česko	k1gNnSc1	Česko
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
Dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
zamezení	zamezení	k1gNnSc1	zamezení
dvojího	dvojí	k4xRgNnSc2	dvojí
zdanění	zdanění	k1gNnSc2	zdanění
a	a	k8xC	a
Dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
podpoře	podpora	k1gFnSc6	podpora
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
obchodní	obchodní	k2eAgFnSc2d1	obchodní
výměny	výměna	k1gFnSc2	výměna
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
překročil	překročit	k5eAaPmAgMnS	překročit
hranici	hranice	k1gFnSc4	hranice
1	[number]	k4	1
miliardy	miliarda	k4xCgFnPc4	miliarda
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
hranici	hranice	k1gFnSc6	hranice
2	[number]	k4	2
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
vízová	vízový	k2eAgFnSc1d1	vízová
povinnost	povinnost	k1gFnSc1	povinnost
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
cestující	cestující	k1gMnPc1	cestující
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
za	za	k7c7	za
turistickým	turistický	k2eAgInSc7d1	turistický
účelem	účel	k1gInSc7	účel
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
–	–	k?	–
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
–	–	k?	–
počíná	počínat	k5eAaImIp3nS	počínat
již	již	k9	již
vznikem	vznik	k1gInSc7	vznik
Turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
od	od	k7c2	od
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Turecko	Turecko	k1gNnSc1	Turecko
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgInPc1d2	významnější
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
přiblížení	přiblížení	k1gNnSc6	přiblížení
se	se	k3xPyFc4	se
však	však	k9	však
datují	datovat	k5eAaImIp3nP	datovat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
1948	[number]	k4	1948
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
Turecko	Turecko	k1gNnSc4	Turecko
k	k	k7c3	k
zakládajícím	zakládající	k2eAgFnPc3d1	zakládající
členským	členský	k2eAgFnPc3d1	členská
zemím	zem	k1gFnPc3	zem
OECD	OECD	kA	OECD
1952	[number]	k4	1952
–	–	k?	–
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
Turecko	Turecko	k1gNnSc4	Turecko
k	k	k7c3	k
NATO	NATO	kA	NATO
1959	[number]	k4	1959
–	–	k?	–
podává	podávat	k5eAaImIp3nS	podávat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
EHS	EHS	kA	EHS
1964	[number]	k4	1964
–	–	k?	–
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
asociační	asociační	k2eAgFnSc1d1	asociační
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
EHS	EHS	kA	EHS
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
vstupu	vstup	k1gInSc2	vstup
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
EHS	EHS	kA	EHS
1987	[number]	k4	1987
–	–	k?	–
podává	podávat	k5eAaImIp3nS	podávat
Turecko	Turecko	k1gNnSc1	Turecko
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
plné	plný	k2eAgNnSc4d1	plné
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
ES	ES	kA	ES
1989	[number]	k4	1989
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
žádost	žádost	k1gFnSc4	žádost
je	být	k5eAaImIp3nS	být
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
(	(	kIx(	(
<g/>
nestabilní	stabilní	k2eNgInPc4d1	nestabilní
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
poměry	poměr	k1gInPc4	poměr
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
–	–	k?	–
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
s	s	k7c7	s
EU	EU	kA	EU
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
1997	[number]	k4	1997
–	–	k?	–
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
se	se	k3xPyFc4	se
EU	EU	kA	EU
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
pro	pro	k7c4	pro
možné	možný	k2eAgNnSc4d1	možné
členství	členství	k1gNnSc4	členství
Turecka	Turecko	k1gNnSc2	Turecko
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítá	odmítat	k5eAaImIp3nS	odmítat
udělit	udělit	k5eAaPmF	udělit
status	status	k1gInSc4	status
kandidátské	kandidátský	k2eAgFnSc2d1	kandidátská
země	zem	k1gFnSc2	zem
1999	[number]	k4	1999
–	–	k?	–
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
dostává	dostávat	k5eAaImIp3nS	dostávat
Turecko	Turecko	k1gNnSc1	Turecko
status	status	k1gInSc1	status
kandidáta	kandidát	k1gMnSc2	kandidát
na	na	k7c4	na
přístup	přístup	k1gInSc4	přístup
2002	[number]	k4	2002
–	–	k?	–
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
velké	velký	k2eAgFnSc2d1	velká
ústavní	ústavní	k2eAgFnSc2d1	ústavní
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
EU	EU	kA	EU
<g/>
,	,	kIx,	,
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
(	(	kIx(	(
<g/>
zrušení	zrušení	k1gNnSc4	zrušení
trestu	trest	k1gInSc2	trest
<g />
.	.	kIx.	.
</s>
<s>
smrti	smrt	k1gFnSc3	smrt
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
–	–	k?	–
nové	nový	k2eAgNnSc1d1	nové
trestní	trestní	k2eAgNnSc1d1	trestní
právo	právo	k1gNnSc1	právo
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
;	;	kIx,	;
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
jednání	jednání	k1gNnSc2	jednání
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
je	být	k5eAaImIp3nS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
termín	termín	k1gInSc1	termín
jednání	jednání	k1gNnSc2	jednání
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
s	s	k7c7	s
možným	možný	k2eAgInSc7d1	možný
vstupem	vstup	k1gInSc7	vstup
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
EU	EU	kA	EU
nejdříve	dříve	k6eAd3	dříve
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
2009	[number]	k4	2009
–	–	k?	–
Turecko	Turecko	k1gNnSc1	Turecko
přijalo	přijmout	k5eAaPmAgNnS	přijmout
zákon	zákon	k1gInSc4	zákon
<g />
.	.	kIx.	.
</s>
<s>
omezující	omezující	k2eAgFnSc4d1	omezující
pravomoc	pravomoc	k1gFnSc4	pravomoc
vojenských	vojenský	k2eAgInPc2d1	vojenský
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
splnilo	splnit	k5eAaPmAgNnS	splnit
další	další	k1gNnPc4	další
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
přístupových	přístupový	k2eAgFnPc2d1	přístupová
podmínek	podmínka	k1gFnPc2	podmínka
2013	[number]	k4	2013
–	–	k?	–
zpráva	zpráva	k1gFnSc1	zpráva
EU	EU	kA	EU
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významnému	významný	k2eAgInSc3d1	významný
pokroku	pokrok	k1gInSc3	pokrok
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ještě	ještě	k6eAd1	ještě
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
2015	[number]	k4	2015
–	–	k?	–
EU	EU	kA	EU
obnovila	obnovit	k5eAaPmAgFnS	obnovit
přístupové	přístupový	k2eAgInPc4d1	přístupový
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
,	,	kIx,	,
jednala	jednat	k5eAaImAgFnS	jednat
o	o	k7c6	o
bezvízovém	bezvízový	k2eAgInSc6d1	bezvízový
styku	styk	k1gInSc6	styk
a	a	k8xC	a
slíbila	slíbit	k5eAaPmAgFnS	slíbit
dát	dát	k5eAaPmF	dát
Turecku	Turecko	k1gNnSc3	Turecko
tři	tři	k4xCgFnPc4	tři
miliardy	miliarda	k4xCgFnSc2	miliarda
eur	euro	k1gNnPc2	euro
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
omezení	omezení	k1gNnSc4	omezení
migrační	migrační	k2eAgFnSc2d1	migrační
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přes	přes	k7c4	přes
Turecko	Turecko	k1gNnSc4	Turecko
míří	mířit	k5eAaImIp3nP	mířit
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
slíbila	slíbit	k5eAaPmAgFnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
Turecku	Turecko	k1gNnSc6	Turecko
800	[number]	k4	800
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Přístupové	přístupový	k2eAgInPc4d1	přístupový
rozhovory	rozhovor	k1gInPc4	rozhovor
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
zejména	zejména	k9	zejména
kurdská	kurdský	k2eAgFnSc1d1	kurdská
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc1	rozdělení
Kypru	Kypr	k1gInSc2	Kypr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
nezákonně	zákonně	k6eNd1	zákonně
okupována	okupovat	k5eAaBmNgFnS	okupovat
tureckou	turecký	k2eAgFnSc7d1	turecká
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
práv	právo	k1gNnPc2	právo
národnostních	národnostní	k2eAgFnPc2d1	národnostní
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
Turecka	Turecko	k1gNnSc2	Turecko
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Turecké	turecký	k2eAgNnSc1d1	turecké
četnictvo	četnictvo	k1gNnSc1	četnictvo
a	a	k8xC	a
Turecká	turecký	k2eAgFnSc1d1	turecká
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
stráž	stráž	k1gFnSc1	stráž
operují	operovat	k5eAaImIp3nP	operovat
pod	pod	k7c7	pod
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
podřízeny	podřídit	k5eAaPmNgFnP	podřídit
velitelství	velitelství	k1gNnSc3	velitelství
pozemních	pozemní	k2eAgNnPc2d1	pozemní
resp.	resp.	kA	resp.
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Náčelník	náčelník	k1gInSc1	náčelník
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
premiérovi	premiér	k1gMnSc3	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
parlamenu	parlamen	k1gInSc3	parlamen
za	za	k7c4	za
záležitosti	záležitost	k1gFnPc4	záležitost
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
adekvátní	adekvátní	k2eAgFnSc4d1	adekvátní
přípravu	příprava	k1gFnSc4	příprava
ozdbrojených	ozdbrojený	k2eAgFnPc2d1	ozdbrojený
sil	síla	k1gFnPc2	síla
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
nasadit	nasadit	k5eAaPmF	nasadit
turecká	turecký	k2eAgNnPc4d1	turecké
vojska	vojsko	k1gNnPc4	vojsko
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
či	či	k8xC	či
umožnit	umožnit	k5eAaPmF	umožnit
pobyt	pobyt	k1gInSc4	pobyt
cizích	cizí	k2eAgNnPc2d1	cizí
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
území	území	k1gNnSc6	území
Turecka	Turecko	k1gNnSc2	Turecko
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
armádě	armáda	k1gFnSc6	armáda
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
vojsko	vojsko	k1gNnSc4	vojsko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
NATO	NATO	kA	NATO
s	s	k7c7	s
odhadem	odhad	k1gInSc7	odhad
495	[number]	k4	495
000	[number]	k4	000
nasaditelných	nasaditelný	k2eAgMnPc2d1	nasaditelný
vojáků	voják	k1gMnPc2	voják
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
NATO	NATO	kA	NATO
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
států	stát	k1gInPc2	stát
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
politiky	politika	k1gFnSc2	politika
sdílení	sdílení	k1gNnSc2	sdílení
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letecké	letecký	k2eAgFnSc6d1	letecká
základně	základna	k1gFnSc6	základna
Incirlik	Incirlika	k1gFnPc2	Incirlika
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
celkem	celkem	k6eAd1	celkem
90	[number]	k4	90
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
B	B	kA	B
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
40	[number]	k4	40
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
tureckého	turecký	k2eAgNnSc2d1	turecké
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
však	však	k9	však
podléhá	podléhat	k5eAaImIp3nS	podléhat
souhlasu	souhlas	k1gInSc2	souhlas
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
zdravý	zdravý	k2eAgMnSc1d1	zdravý
mužský	mužský	k2eAgMnSc1d1	mužský
občan	občan	k1gMnSc1	občan
Turecka	Turecko	k1gNnSc2	Turecko
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
sloužit	sloužit	k5eAaImF	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
třemi	tři	k4xCgInPc7	tři
týdny	týden	k1gInPc7	týden
a	a	k8xC	a
rokem	rok	k1gInSc7	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vzdělání	vzdělání	k1gNnSc6	vzdělání
a	a	k8xC	a
lokalitě	lokalita	k1gFnSc6	lokalita
pracoviště	pracoviště	k1gNnSc2	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
neuznává	uznávat	k5eNaImIp3nS	uznávat
výjimky	výjimka	k1gFnPc4	výjimka
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svědomí	svědomí	k1gNnSc2	svědomí
a	a	k8xC	a
nenabízí	nabízet	k5eNaImIp3nS	nabízet
civilní	civilní	k2eAgFnSc4d1	civilní
alternativu	alternativa	k1gFnSc4	alternativa
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
země	země	k1gFnSc1	země
fungovala	fungovat	k5eAaImAgFnS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
smíšené	smíšený	k2eAgFnSc2d1	smíšená
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
sektorem	sektor	k1gInSc7	sektor
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
více	hodně	k6eAd2	hodně
prosazovat	prosazovat	k5eAaImF	prosazovat
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
Turecko	Turecko	k1gNnSc4	Turecko
relativně	relativně	k6eAd1	relativně
vyspělou	vyspělý	k2eAgFnSc4d1	vyspělá
liberální	liberální	k2eAgFnSc4d1	liberální
průmyslově-agrární	průmyslověgrární	k2eAgFnSc4d1	průmyslově-agrární
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
)	)	kIx)	)
a	a	k8xC	a
G20	G20	k1gFnSc1	G20
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
objemu	objem	k1gInSc2	objem
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
činil	činit	k5eAaImAgInS	činit
1,053	[number]	k4	1,053
bilionů	bilion	k4xCgInPc2	bilion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc7d3	veliký
ekonomikou	ekonomika	k1gFnSc7	ekonomika
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
prodělala	prodělat	k5eAaPmAgFnS	prodělat
země	země	k1gFnSc1	země
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
ale	ale	k8xC	ale
po	po	k7c6	po
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
ekonomických	ekonomický	k2eAgFnPc6d1	ekonomická
reformách	reforma	k1gFnPc6	reforma
zaznamenávala	zaznamenávat	k5eAaImAgFnS	zaznamenávat
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2002	[number]	k4	2002
a	a	k8xC	a
2008	[number]	k4	2008
HDP	HDP	kA	HDP
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
celkově	celkově	k6eAd1	celkově
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zemi	zem	k1gFnSc4	zem
postihl	postihnout	k5eAaPmAgInS	postihnout
pokles	pokles	k1gInSc1	pokles
exportu	export	k1gInSc2	export
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
světové	světový	k2eAgFnSc2d1	světová
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
HDP	HDP	kA	HDP
opět	opět	k6eAd1	opět
silně	silně	k6eAd1	silně
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
9,2	[number]	k4	9,2
%	%	kIx~	%
a	a	k8xC	a
oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
roku	rok	k1gInSc3	rok
tak	tak	k6eAd1	tak
klesla	klesnout	k5eAaPmAgFnS	klesnout
<g/>
.	.	kIx.	.
70	[number]	k4	70
%	%	kIx~	%
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
jsou	být	k5eAaImIp3nP	být
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
nemá	mít	k5eNaImIp3nS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
pracovalo	pracovat	k5eAaImAgNnS	pracovat
zhruba	zhruba	k6eAd1	zhruba
25,5	[number]	k4	25,5
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
26	[number]	k4	26
%	%	kIx~	%
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
48,5	[number]	k4	48,5
%	%	kIx~	%
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
oborům	obor	k1gInPc3	obor
služeb	služba	k1gFnPc2	služba
patří	patřit	k5eAaImIp3nS	patřit
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
související	související	k2eAgNnSc1d1	související
hotelnictví	hotelnictví	k1gNnSc1	hotelnictví
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
přístupových	přístupový	k2eAgInPc2d1	přístupový
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
EU	EU	kA	EU
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
zájem	zájem	k1gInSc4	zájem
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
investorů	investor	k1gMnPc2	investor
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc7d1	národní
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
turecká	turecký	k2eAgFnSc1d1	turecká
lira	lira	k1gFnSc1	lira
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
kurz	kurz	k1gInSc1	kurz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
1,67	[number]	k4	1,67
tureckých	turecký	k2eAgFnPc2d1	turecká
lir	lira	k1gFnPc2	lira
za	za	k7c4	za
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
nejvíce	hodně	k6eAd3	hodně
zboží	zboží	k1gNnSc2	zboží
vyváží	vyvážet	k5eAaImIp3nP	vyvážet
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
10,1	[number]	k4	10,1
%	%	kIx~	%
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
6,4	[number]	k4	6,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
5,7	[number]	k4	5,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
5,3	[number]	k4	5,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc2	Irák
(	(	kIx(	(
<g/>
5,3	[number]	k4	5,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
4,1	[number]	k4	4,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
vývozními	vývozní	k2eAgInPc7d1	vývozní
artikly	artikl	k1gInPc7	artikl
jsou	být	k5eAaImIp3nP	být
oděvy	oděv	k1gInPc1	oděv
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc1	potravina
<g/>
,	,	kIx,	,
textilie	textilie	k1gFnPc1	textilie
<g/>
,	,	kIx,	,
kovové	kovový	k2eAgInPc1d1	kovový
výrobky	výrobek	k1gInPc1	výrobek
a	a	k8xC	a
strojní	strojní	k2eAgFnSc1d1	strojní
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
importu	import	k1gInSc3	import
převažuje	převažovat	k5eAaImIp3nS	převažovat
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
11,6	[number]	k4	11,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
9,5	[number]	k4	9,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
9,3	[number]	k4	9,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
6,6	[number]	k4	6,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
4,4	[number]	k4	4,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
(	(	kIx(	(
<g/>
4,1	[number]	k4	4,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastěji	často	k6eAd3	často
dovážené	dovážený	k2eAgNnSc4d1	dovážené
zboží	zboží	k1gNnSc4	zboží
patří	patřit	k5eAaImIp3nS	patřit
strojní	strojní	k2eAgFnSc1d1	strojní
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
a	a	k8xC	a
pohonné	pohonný	k2eAgFnPc1d1	pohonná
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
bylo	být	k5eAaImAgNnS	být
Turecko	Turecko	k1gNnSc4	Turecko
silně	silně	k6eAd1	silně
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
pocházelo	pocházet	k5eAaImAgNnS	pocházet
35	[number]	k4	35
%	%	kIx~	%
HDP	HDP	kA	HDP
právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
sektoru	sektor	k1gInSc2	sektor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zhruba	zhruba	k6eAd1	zhruba
9	[number]	k4	9
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
turecké	turecký	k2eAgFnSc6d1	turecká
ekonomice	ekonomika	k1gFnSc6	ekonomika
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
je	být	k5eAaImIp3nS	být
zaměstnána	zaměstnán	k2eAgFnSc1d1	zaměstnána
stále	stále	k6eAd1	stále
asi	asi	k9	asi
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
tak	tak	k6eAd1	tak
silným	silný	k2eAgInSc7d1	silný
sektorem	sektor	k1gInSc7	sektor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zemědělským	zemědělský	k2eAgInPc3d1	zemědělský
účelům	účel	k1gInPc3	účel
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základních	základní	k2eAgInPc6d1	základní
zemědělských	zemědělský	k2eAgInPc6d1	zemědělský
produktech	produkt	k1gInPc6	produkt
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
soběstačná	soběstačný	k2eAgFnSc1d1	soběstačná
a	a	k8xC	a
část	část	k1gFnSc1	část
produkce	produkce	k1gFnSc2	produkce
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
plodinami	plodina	k1gFnPc7	plodina
jsou	být	k5eAaImIp3nP	být
obilniny	obilnina	k1gFnPc1	obilnina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vysazené	vysazený	k2eAgFnPc1d1	vysazená
asi	asi	k9	asi
na	na	k7c6	na
polovině	polovina	k1gFnSc6	polovina
obdělávaného	obdělávaný	k2eAgNnSc2d1	obdělávané
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
oves	oves	k1gInSc1	oves
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
a	a	k8xC	a
rýže	rýže	k1gFnSc1	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
plodiny	plodina	k1gFnPc1	plodina
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
a	a	k8xC	a
lilek	lilek	k1gInSc1	lilek
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
pokryta	pokryt	k2eAgFnSc1d1	pokryta
vinicemi	vinice	k1gFnPc7	vinice
<g/>
,	,	kIx,	,
ovocnými	ovocný	k2eAgInPc7d1	ovocný
sady	sad	k1gInPc7	sad
<g/>
,	,	kIx,	,
olivovými	olivový	k2eAgInPc7d1	olivový
háji	háj	k1gInPc7	háj
a	a	k8xC	a
zeleninovými	zeleninový	k2eAgFnPc7d1	zeleninová
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
Turecko	Turecko	k1gNnSc1	Turecko
světově	světově	k6eAd1	světově
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
lískových	lískový	k2eAgInPc2d1	lískový
ořechů	ořech	k1gInPc2	ořech
<g/>
,	,	kIx,	,
třešní	třešeň	k1gFnPc2	třešeň
<g/>
,	,	kIx,	,
fíků	fík	k1gInPc2	fík
<g/>
,	,	kIx,	,
meruněk	meruňka	k1gFnPc2	meruňka
a	a	k8xC	a
kdouloní	kdouloň	k1gFnPc2	kdouloň
<g/>
.	.	kIx.	.
</s>
<s>
Častou	častý	k2eAgFnSc7d1	častá
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
aktivitou	aktivita	k1gFnSc7	aktivita
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc1	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k6eAd1	hlavně
tura	tur	k1gMnSc2	tur
domácího	domácí	k1gMnSc2	domácí
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc2	ovce
domácí	domácí	k2eAgFnSc2d1	domácí
<g/>
,	,	kIx,	,
kozy	koza	k1gFnSc2	koza
domácí	domácí	k2eAgFnSc2d1	domácí
a	a	k8xC	a
buvola	buvol	k1gMnSc2	buvol
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
produkty	produkt	k1gInPc4	produkt
jako	jako	k8xS	jako
např.	např.	kA	např.
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
a	a	k8xC	a
oříšky	oříšek	k1gInPc1	oříšek
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
vývozu	vývoz	k1gInSc2	vývoz
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgInPc7d1	důležitý
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
obory	obor	k1gInPc7	obor
jsou	být	k5eAaImIp3nP	být
lesnictví	lesnictví	k1gNnSc1	lesnictví
a	a	k8xC	a
rybářství	rybářství	k1gNnSc1	rybářství
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
27	[number]	k4	27
%	%	kIx~	%
a	a	k8xC	a
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
plánuje	plánovat	k5eAaImIp3nS	plánovat
zalesňovat	zalesňovat	k5eAaImF	zalesňovat
další	další	k2eAgNnSc4d1	další
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
předešlo	předejít	k5eAaPmAgNnS	předejít
erozi	eroze	k1gFnSc4	eroze
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Rybářství	rybářství	k1gNnSc1	rybářství
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
činil	činit	k5eAaImAgInS	činit
objem	objem	k1gInSc1	objem
vylovených	vylovený	k2eAgFnPc2d1	vylovená
ryb	ryba	k1gFnPc2	ryba
656	[number]	k4	656
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
lovu	lov	k1gInSc2	lov
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
Černém	černý	k2eAgNnSc6d1	černé
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
odvětvím	odvětví	k1gNnSc7	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
automobilová	automobilový	k2eAgFnSc1d1	automobilová
produkce	produkce	k1gFnSc1	produkce
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
hranici	hranice	k1gFnSc4	hranice
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
propadu	propad	k1gInSc2	propad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
neustále	neustále	k6eAd1	neustále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
na	na	k7c6	na
tureckém	turecký	k2eAgInSc6d1	turecký
trhu	trh	k1gInSc6	trh
je	být	k5eAaImIp3nS	být
Oyak-Renault	Oyak-Renault	k1gInSc1	Oyak-Renault
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
52	[number]	k4	52
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
automobilů	automobil	k1gInPc2	automobil
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
turecký	turecký	k2eAgInSc4d1	turecký
průmysl	průmysl	k1gInSc4	průmysl
důležitá	důležitý	k2eAgFnSc1d1	důležitá
výroba	výroba	k1gFnSc1	výroba
oděvů	oděv	k1gInPc2	oděv
a	a	k8xC	a
textilní	textilní	k2eAgInPc4d1	textilní
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
příslušenství	příslušenství	k1gNnSc2	příslušenství
<g/>
,	,	kIx,	,
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgInPc1d1	elektrický
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
spotřební	spotřební	k2eAgFnSc1d1	spotřební
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
plasty	plast	k1gInPc1	plast
<g/>
,	,	kIx,	,
minerální	minerální	k2eAgNnPc1d1	minerální
paliva	palivo	k1gNnPc1	palivo
a	a	k8xC	a
oleje	olej	k1gInPc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc4	Turecko
má	mít	k5eAaImIp3nS	mít
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
zásob	zásoba	k1gFnPc2	zásoba
řady	řada	k1gFnSc2	řada
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
boru	bor	k1gInSc2	bor
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
bentonitu	bentonit	k1gInSc2	bentonit
<g/>
,	,	kIx,	,
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
živců	živec	k1gInPc2	živec
<g/>
,	,	kIx,	,
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
sádrovce	sádrovec	k1gInSc2	sádrovec
<g/>
,	,	kIx,	,
perlitu	perlit	k1gInSc2	perlit
<g/>
,	,	kIx,	,
stroncia	stroncium	k1gNnSc2	stroncium
a	a	k8xC	a
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zhruba	zhruba	k6eAd1	zhruba
3500	[number]	k4	3500
nalezišť	naleziště	k1gNnPc2	naleziště
kovových	kovový	k2eAgFnPc2d1	kovová
rud	ruda	k1gFnPc2	ruda
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
2000	[number]	k4	2000
nalezišť	naleziště	k1gNnPc2	naleziště
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
pracuje	pracovat	k5eAaImIp3nS	pracovat
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
sektor	sektor	k1gInSc4	sektor
se	se	k3xPyFc4	se
na	na	k7c6	na
HDP	HDP	kA	HDP
podílí	podílet	k5eAaImIp3nS	podílet
zhruba	zhruba	k6eAd1	zhruba
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Putuje	putovat	k5eAaImIp3nS	putovat
sem	sem	k6eAd1	sem
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
oborem	obor	k1gInSc7	obor
je	být	k5eAaImIp3nS	být
turismus	turismus	k1gInSc1	turismus
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
související	související	k2eAgFnPc4d1	související
hotelové	hotelový	k2eAgFnPc4d1	hotelová
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
gastronomie	gastronomie	k1gFnPc4	gastronomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
Turecko	Turecko	k1gNnSc1	Turecko
zhruba	zhruba	k6eAd1	zhruba
28,6	[number]	k4	28,6
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvíce	hodně	k6eAd3	hodně
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
významnými	významný	k2eAgInPc7d1	významný
obory	obor	k1gInPc7	obor
jsou	být	k5eAaImIp3nP	být
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
finanční	finanční	k2eAgFnPc4d1	finanční
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
telekomunikacemi	telekomunikace	k1gFnPc7	telekomunikace
a	a	k8xC	a
energetikou	energetika	k1gFnSc7	energetika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
měla	mít	k5eAaImAgFnS	mít
země	země	k1gFnSc1	země
2238	[number]	k4	2238
km	km	kA	km
moderních	moderní	k2eAgFnPc2d1	moderní
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
000	[number]	k4	000
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
zhruba	zhruba	k6eAd1	zhruba
64	[number]	k4	64
000	[number]	k4	000
km	km	kA	km
mělo	mít	k5eAaImAgNnS	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
15,6	[number]	k4	15,6
milionů	milion	k4xCgInPc2	milion
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
%	%	kIx~	%
tvořily	tvořit	k5eAaImAgInP	tvořit
osobní	osobní	k2eAgInPc1d1	osobní
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
16	[number]	k4	16
%	%	kIx~	%
malé	malý	k2eAgInPc4d1	malý
nákladní	nákladní	k2eAgInPc4d1	nákladní
automobily	automobil	k1gInPc4	automobil
a	a	k8xC	a
16	[number]	k4	16
%	%	kIx~	%
motocykly	motocykl	k1gInPc1	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
železnic	železnice	k1gFnPc2	železnice
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
8700	[number]	k4	8700
km	km	kA	km
a	a	k8xC	a
putují	putovat	k5eAaImIp3nP	putovat
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
významné	významný	k2eAgFnPc1d1	významná
investice	investice	k1gFnPc1	investice
<g/>
.	.	kIx.	.
</s>
<s>
Státním	státní	k2eAgMnSc7d1	státní
dopravcem	dopravce	k1gMnSc7	dopravce
je	být	k5eAaImIp3nS	být
Türkiye	Türkiye	k1gInSc1	Türkiye
Cumhuriyeti	Cumhuriye	k1gNnSc6	Cumhuriye
Devlet	Devlet	k1gInSc1	Devlet
Demiryolları	Demiryolları	k1gMnPc1	Demiryolları
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
vodní	vodní	k2eAgFnSc2d1	vodní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc1d1	obchodní
flotila	flotila	k1gFnSc1	flotila
Turecka	Turecko	k1gNnSc2	Turecko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
čítala	čítat	k5eAaImAgFnS	čítat
629	[number]	k4	629
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
přístavy	přístav	k1gInPc4	přístav
patří	patřit	k5eAaImIp3nS	patřit
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Mersin	Mersin	k1gInSc1	Mersin
<g/>
,	,	kIx,	,
Samsun	Samsun	k1gInSc1	Samsun
<g/>
,	,	kIx,	,
Trabzon	Trabzon	k1gInSc1	Trabzon
<g/>
,	,	kIx,	,
İ	İ	k?	İ
<g/>
,	,	kIx,	,
İ	İ	k?	İ
a	a	k8xC	a
İ	İ	k?	İ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
měla	mít	k5eAaImAgFnS	mít
země	země	k1gFnSc1	země
88	[number]	k4	88
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
13	[number]	k4	13
bylo	být	k5eAaImAgNnS	být
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
prochází	procházet	k5eAaImIp3nS	procházet
Turecko	Turecko	k1gNnSc1	Turecko
za	za	k7c2	za
Gülovy	Gülův	k2eAgFnSc2d1	Gülův
vlády	vláda	k1gFnSc2	vláda
rychlým	rychlý	k2eAgInSc7d1	rychlý
rozvojem	rozvoj	k1gInSc7	rozvoj
a	a	k8xC	a
privatizací	privatizace	k1gFnSc7	privatizace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zhruba	zhruba	k6eAd1	zhruba
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
zapojených	zapojený	k2eAgFnPc2d1	zapojená
pevných	pevný	k2eAgFnPc2d1	pevná
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
přes	přes	k7c4	přes
61	[number]	k4	61
milionů	milion	k4xCgInPc2	milion
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
uživateli	uživatel	k1gMnSc3	uživatel
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
zhruba	zhruba	k6eAd1	zhruba
43	[number]	k4	43
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
působí	působit	k5eAaImIp3nS	působit
několik	několik	k4yIc1	několik
celostátních	celostátní	k2eAgFnPc2d1	celostátní
a	a	k8xC	a
na	na	k7c4	na
300	[number]	k4	300
místních	místní	k2eAgFnPc2d1	místní
soukromých	soukromý	k2eAgFnPc2d1	soukromá
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
přes	přes	k7c4	přes
1000	[number]	k4	1000
soukromých	soukromý	k2eAgFnPc2d1	soukromá
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
zhruba	zhruba	k6eAd1	zhruba
ze	z	k7c2	z
70	[number]	k4	70
%	%	kIx~	%
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c6	na
dovozu	dovoz	k1gInSc6	dovoz
energie	energie	k1gFnSc2	energie
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
90	[number]	k4	90
%	%	kIx~	%
spotřebované	spotřebovaný	k2eAgFnSc2d1	spotřebovaná
surové	surový	k2eAgFnSc2d1	surová
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
dováží	dovážet	k5eAaImIp3nP	dovážet
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
)	)	kIx)	)
a	a	k8xC	a
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
importu	import	k1gInSc6	import
plynu	plyn	k1gInSc2	plyn
(	(	kIx(	(
<g/>
také	také	k9	také
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
potenciál	potenciál	k1gInSc4	potenciál
ve	v	k7c6	v
využití	využití	k1gNnSc6	využití
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
odpadu	odpad	k1gInSc2	odpad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
první	první	k4xOgFnSc2	první
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
na	na	k7c6	na
území	území	k1gNnSc6	území
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
Rosatom	Rosatom	k1gInSc1	Rosatom
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
2016	[number]	k4	2016
čítala	čítat	k5eAaImAgFnS	čítat
79	[number]	k4	79
463	[number]	k4	463
663	[number]	k4	663
obyvatel	obyvatel	k1gMnPc2	obyvatel
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
jenom	jenom	k9	jenom
nepatrně	patrně	k6eNd1	patrně
zaostává	zaostávat	k5eAaImIp3nS	zaostávat
za	za	k7c7	za
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
mělo	mít	k5eAaImAgNnS	mít
Turecko	Turecko	k1gNnSc1	Turecko
necelých	celý	k2eNgInPc2d1	necelý
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
přírůstek	přírůstek	k1gInSc1	přírůstek
činí	činit	k5eAaImIp3nS	činit
zhruba	zhruba	k6eAd1	zhruba
1,24	[number]	k4	1,24
%	%	kIx~	%
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
28,5	[number]	k4	28,5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
72,5	[number]	k4	72,5
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
70,6	[number]	k4	70,6
a	a	k8xC	a
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
74,5	[number]	k4	74,5
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
70	[number]	k4	70
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
míra	míra	k1gFnSc1	míra
urbanizace	urbanizace	k1gFnSc2	urbanizace
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1,7	[number]	k4	1,7
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
26,7	[number]	k4	26,7
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
mezi	mezi	k7c7	mezi
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
67	[number]	k4	67
%	%	kIx~	%
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
6,3	[number]	k4	6,3
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
starší	starší	k1gMnSc1	starší
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Turkové	Turek	k1gMnPc1	Turek
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
označováni	označován	k2eAgMnPc1d1	označován
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
tureckým	turecký	k2eAgNnSc7d1	turecké
občanstvím	občanství	k1gNnSc7	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Etnicky	etnicky	k6eAd1	etnicky
tvoří	tvořit	k5eAaImIp3nP	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
Turci	Turek	k1gMnPc1	Turek
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kurdové	Kurd	k1gMnPc1	Kurd
(	(	kIx(	(
<g/>
18	[number]	k4	18
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
menšiny	menšina	k1gFnPc1	menšina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
jsou	být	k5eAaImIp3nP	být
Řekové	Řek	k1gMnPc1	Řek
(	(	kIx(	(
<g/>
16	[number]	k4	16
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arméni	Armén	k1gMnPc1	Armén
(	(	kIx(	(
<g/>
59	[number]	k4	59
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
17	[number]	k4	17
600	[number]	k4	600
<g/>
)	)	kIx)	)
žijící	žijící	k2eAgMnSc1d1	žijící
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc2	Istanbul
a	a	k8xC	a
Adygejci	Adygejec	k1gMnSc3	Adygejec
(	(	kIx(	(
<g/>
2	[number]	k4	2
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gruzíni	Gruzín	k1gMnPc1	Gruzín
(	(	kIx(	(
<g/>
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lazové	Lazové	k2eAgFnSc1d1	Lazové
(	(	kIx(	(
<g/>
147	[number]	k4	147
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
žijící	žijící	k2eAgMnSc1d1	žijící
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
zbylých	zbylý	k2eAgInPc2d1	zbylý
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
používaná	používaný	k2eAgFnSc1d1	používaná
je	být	k5eAaImIp3nS	být
kurdština	kurdština	k1gFnSc1	kurdština
a	a	k8xC	a
menšiny	menšina	k1gFnPc1	menšina
hovoří	hovořit	k5eAaImIp3nP	hovořit
vlastními	vlastní	k2eAgInPc7d1	vlastní
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
Arméni	Armén	k1gMnPc1	Armén
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Lausannské	lausannský	k2eAgFnSc2d1	Lausannská
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
oficiálně	oficiálně	k6eAd1	oficiálně
uznanými	uznaný	k2eAgFnPc7d1	uznaná
menšinami	menšina	k1gFnPc7	menšina
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
jazyky	jazyk	k1gMnPc7	jazyk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyučovány	vyučovat	k5eAaImNgInP	vyučovat
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vysídlení	vysídlení	k1gNnSc3	vysídlení
1.3	[number]	k4	1.3
milionů	milion	k4xCgInPc2	milion
anatolských	anatolský	k2eAgMnPc2d1	anatolský
a	a	k8xC	a
pontských	pontský	k2eAgMnPc2d1	pontský
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
smělo	smět	k5eAaImAgNnS	smět
zůstat	zůstat	k5eAaPmF	zůstat
pouze	pouze	k6eAd1	pouze
200	[number]	k4	200
000	[number]	k4	000
Řeků	Řek	k1gMnPc2	Řek
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
sekulární	sekulární	k2eAgFnSc7d1	sekulární
zemí	zem	k1gFnSc7	zem
bez	bez	k7c2	bez
státního	státní	k2eAgNnSc2d1	státní
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
;	;	kIx,	;
turecká	turecký	k2eAgFnSc1d1	turecká
ústava	ústava	k1gFnSc1	ústava
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
svobodu	svoboda	k1gFnSc4	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
99	[number]	k4	99
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nP	tvořit
křesťané	křesťan	k1gMnPc1	křesťan
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
členové	člen	k1gMnPc1	člen
Arménské	arménský	k2eAgFnSc2d1	arménská
apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
církve	církev	k1gFnSc2	církev
<g/>
)	)	kIx)	)
a	a	k8xC	a
židé	žid	k1gMnPc1	žid
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
Sefardé	Sefardý	k2eAgNnSc1d1	Sefardý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
muslimů	muslim	k1gMnPc2	muslim
většina	většina	k1gFnSc1	většina
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
sunnitský	sunnitský	k2eAgInSc4d1	sunnitský
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
alevité	alevita	k1gMnPc1	alevita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
diskriminováni	diskriminován	k2eAgMnPc1d1	diskriminován
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Turecký	turecký	k2eAgInSc1d1	turecký
záchod	záchod	k1gInSc1	záchod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
středověkým	středověký	k2eAgMnPc3d1	středověký
osmanským	osmanský	k2eAgMnPc3d1	osmanský
spisovatelům	spisovatel	k1gMnPc3	spisovatel
patřil	patřit	k5eAaImAgInS	patřit
Muhammad	Muhammad	k1gInSc1	Muhammad
Füzuli	Füzule	k1gFnSc4	Füzule
či	či	k8xC	či
sufistický	sufistický	k2eAgMnSc1d1	sufistický
mystik	mystik	k1gMnSc1	mystik
Junus	Junus	k1gMnSc1	Junus
Emre	Emre	k1gFnSc1	Emre
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
autorům	autor	k1gMnPc3	autor
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
mladotureckého	mladoturecký	k2eAgNnSc2d1	mladoturecký
hnutí	hnutí	k1gNnSc2	hnutí
patřil	patřit	k5eAaImAgMnS	patřit
Namı	Namı	k1gMnSc1	Namı
Kemal	Kemal	k1gMnSc1	Kemal
<g/>
.	.	kIx.	.
</s>
<s>
Kemalistické	Kemalistický	k2eAgFnPc1d1	Kemalistický
a	a	k8xC	a
feministické	feministický	k2eAgFnPc1d1	feministická
pozice	pozice	k1gFnPc1	pozice
zastávala	zastávat	k5eAaImAgFnS	zastávat
Halide	Halid	k1gInSc5	Halid
Edib	Ediba	k1gFnPc2	Ediba
Adı	Adı	k1gMnPc4	Adı
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
se	se	k3xPyFc4	se
s	s	k7c7	s
Ataturkem	Ataturk	k1gInSc7	Ataturk
nakonec	nakonec	k6eAd1	nakonec
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
zemi	zem	k1gFnSc4	zem
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
symbolem	symbol	k1gInSc7	symbol
moderní	moderní	k2eAgFnPc1d1	moderní
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kemalovských	kemalovský	k2eAgFnPc2d1	kemalovský
pozic	pozice	k1gFnPc2	pozice
vycházel	vycházet	k5eAaImAgMnS	vycházet
i	i	k9	i
básník	básník	k1gMnSc1	básník
Mehmet	Mehmeta	k1gFnPc2	Mehmeta
Akif	Akif	k1gMnSc1	Akif
Ersoy	Ersoa	k1gFnSc2	Ersoa
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
turecké	turecký	k2eAgFnSc2d1	turecká
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Duši	duch	k1gMnPc1	duch
nového	nový	k2eAgNnSc2d1	nové
Turecka	Turecko	k1gNnSc2	Turecko
mapovali	mapovat	k5eAaImAgMnP	mapovat
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
prozaici	prozaik	k1gMnPc1	prozaik
Sait	Sait	k1gMnSc1	Sait
Faik	Faik	k1gMnSc1	Faik
Abası	Abası	k1gMnSc1	Abası
nebo	nebo	k8xC	nebo
Reşat	Reşat	k1gInSc1	Reşat
Nuri	Nur	k1gFnSc2	Nur
Güntekin	Güntekina	k1gFnPc2	Güntekina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
levicové	levicový	k2eAgFnSc2d1	levicová
a	a	k8xC	a
avantgardní	avantgardní	k2eAgFnSc2d1	avantgardní
pozice	pozice	k1gFnSc2	pozice
zastával	zastávat	k5eAaImAgMnS	zastávat
básník	básník	k1gMnSc1	básník
Nâzı	Nâzı	k1gMnSc1	Nâzı
Hikmet	Hikmet	k1gMnSc1	Hikmet
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
také	také	k9	také
nakonec	nakonec	k6eAd1	nakonec
zvolil	zvolit	k5eAaPmAgMnS	zvolit
exil	exil	k1gInSc4	exil
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
poválečného	poválečný	k2eAgInSc2d1	poválečný
ruralismu	ruralismus	k1gInSc2	ruralismus
byl	být	k5eAaImAgMnS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
kurdského	kurdský	k2eAgInSc2d1	kurdský
původu	původ	k1gInSc2	původ
Yaşar	Yaşar	k1gMnSc1	Yaşar
Kemal	Kemal	k1gMnSc1	Kemal
<g/>
.	.	kIx.	.
</s>
<s>
Aziz	Aziz	k1gMnSc1	Aziz
Nesin	Nesin	k1gMnSc1	Nesin
byl	být	k5eAaImAgMnS	být
známým	známý	k2eAgMnSc7d1	známý
humoristou	humorista	k1gMnSc7	humorista
a	a	k8xC	a
bojovníkem	bojovník	k1gMnSc7	bojovník
proti	proti	k7c3	proti
islámskému	islámský	k2eAgInSc3d1	islámský
fundamentalismu	fundamentalismus	k1gInSc3	fundamentalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
turecké	turecký	k2eAgFnSc3d1	turecká
literatuře	literatura	k1gFnSc3	literatura
kraluje	kralovat	k5eAaImIp3nS	kralovat
Orhan	Orhan	k1gMnSc1	Orhan
Pamuk	Pamuk	k1gMnSc1	Pamuk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
získal	získat	k5eAaPmAgInS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnSc3d1	velká
popularitě	popularita	k1gFnSc3	popularita
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
také	také	k9	také
prozaička	prozaička	k1gFnSc1	prozaička
Elif	Elif	k1gInSc4	Elif
Şafaková	Şafakový	k2eAgFnSc1d1	Şafakový
nebo	nebo	k8xC	nebo
prozaici	prozaik	k1gMnPc1	prozaik
Duran	Duran	k1gMnSc1	Duran
Çetin	Çetin	k1gMnSc1	Çetin
a	a	k8xC	a
Mustafa	Mustaf	k1gMnSc4	Mustaf
Balel	Balel	k1gInSc4	Balel
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
moderního	moderní	k2eAgNnSc2d1	moderní
malířství	malířství	k1gNnSc2	malířství
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Osman	Osman	k1gMnSc1	Osman
Hamdi	Hamd	k1gMnPc1	Hamd
Bey	Bey	k1gMnSc1	Bey
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
svými	svůj	k3xOyFgInPc7	svůj
nápady	nápad	k1gInPc7	nápad
budí	budit	k5eAaImIp3nS	budit
rozruch	rozruch	k1gInSc4	rozruch
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
umělec	umělec	k1gMnSc1	umělec
Genco	Genco	k1gMnSc1	Genco
Gulan	Gulan	k1gMnSc1	Gulan
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
nejpokročilejších	pokročilý	k2eAgFnPc2d3	nejpokročilejší
staveb	stavba	k1gFnPc2	stavba
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
byl	být	k5eAaImAgMnS	být
architekt	architekt	k1gMnSc1	architekt
Mimar	Mimar	k1gMnSc1	Mimar
Sinan	Sinan	k1gMnSc1	Sinan
<g/>
.	.	kIx.	.
</s>
<s>
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
palmu	palma	k1gFnSc4	palma
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
režisér	režisér	k1gMnSc1	režisér
Nuri	Nure	k1gFnSc4	Nure
Bilge	Bilge	k1gNnSc2	Bilge
Ceylan	Ceylana	k1gFnPc2	Ceylana
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
Kurdovi	Kurd	k1gMnSc3	Kurd
Yı	Yı	k1gMnSc1	Yı
Güneyovi	Güneya	k1gMnSc3	Güneya
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
italských	italský	k2eAgFnPc2d1	italská
filmových	filmový	k2eAgFnPc2d1	filmová
cen	cena	k1gFnPc2	cena
sesbíral	sesbírat	k5eAaPmAgInS	sesbírat
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
působící	působící	k2eAgMnSc1d1	působící
Ferzan	Ferzan	k1gMnSc1	Ferzan
Özpetek	Özpetka	k1gFnPc2	Özpetka
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
Ahmed	Ahmed	k1gMnSc1	Ahmed
Adnan	Adnan	k1gMnSc1	Adnan
Saygun	Saygun	k1gMnSc1	Saygun
<g/>
.	.	kIx.	.
</s>
<s>
Uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
klavíristou	klavírista	k1gMnSc7	klavírista
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
Fazı	Fazı	k1gMnSc1	Fazı
Say	Say	k1gMnSc1	Say
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
jaru	jar	k1gInSc6	jar
<g/>
.	.	kIx.	.
</s>
<s>
Turecký	turecký	k2eAgMnSc1d1	turecký
pop	pop	k1gMnSc1	pop
dravě	dravě	k6eAd1	dravě
proniká	pronikat	k5eAaImIp3nS	pronikat
na	na	k7c4	na
trhy	trh	k1gInPc4	trh
blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
daří	dařit	k5eAaImIp3nS	dařit
hůře	zle	k6eAd2	zle
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
Tarkan	Tarkan	k1gMnSc1	Tarkan
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Sertab	Sertaba	k1gFnPc2	Sertaba
Erener	Erener	k1gInSc4	Erener
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
představitelům	představitel	k1gMnPc3	představitel
turecké	turecký	k2eAgFnSc2d1	turecká
pop-music	popusic	k1gFnSc2	pop-music
patří	patřit	k5eAaImIp3nS	patřit
Hande	Hand	k1gInSc5	Hand
Yener	Yener	k1gMnSc1	Yener
<g/>
,	,	kIx,	,
Sezen	sezen	k2eAgMnSc1d1	sezen
Aksu	Aksus	k1gInSc2	Aksus
či	či	k8xC	či
Ajda	Ajda	k1gMnSc1	Ajda
Pekkan	Pekkan	k1gMnSc1	Pekkan
<g/>
.	.	kIx.	.
</s>
<s>
Barı	Barı	k?	Barı
Manço	Manço	k1gMnSc1	Manço
je	být	k5eAaImIp3nS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
rocku	rock	k1gInSc2	rock
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Turecká	turecký	k2eAgFnSc1d1	turecká
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
dědictvím	dědictví	k1gNnSc7	dědictví
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
splynutím	splynutí	k1gNnSc7	splynutí
a	a	k8xC	a
zdokonalením	zdokonalení	k1gNnSc7	zdokonalení
kuchyní	kuchyně	k1gFnSc7	kuchyně
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Středního	střední	k2eAgInSc2d1	střední
Východu	východ	k1gInSc2	východ
a	a	k8xC	a
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
turecká	turecký	k2eAgFnSc1d1	turecká
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
mísení	mísení	k1gNnSc2	mísení
originální	originální	k2eAgFnSc2d1	originální
kulinářské	kulinářský	k2eAgFnSc2d1	kulinářská
tradice	tradice	k1gFnSc2	tradice
kočovných	kočovný	k2eAgMnPc2d1	kočovný
tureckých	turecký	k2eAgMnPc2d1	turecký
kmenů	kmen	k1gInPc2	kmen
s	s	k7c7	s
indickou	indický	k2eAgFnSc7d1	indická
<g/>
,	,	kIx,	,
perskou	perský	k2eAgFnSc7d1	perská
<g/>
,	,	kIx,	,
kurdskou	kurdský	k2eAgFnSc7d1	kurdská
a	a	k8xC	a
arabskou	arabský	k2eAgFnSc7d1	arabská
kuchyní	kuchyně	k1gFnSc7	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
je	on	k3xPp3gInPc4	on
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
vlivů	vliv	k1gInPc2	vliv
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
staletími	staletí	k1gNnPc7	staletí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
až	až	k9	až
k	k	k7c3	k
dnešní	dnešní	k2eAgFnSc3d1	dnešní
typické	typický	k2eAgFnSc3d1	typická
turecké	turecký	k2eAgFnSc3d1	turecká
kuchyni	kuchyně	k1gFnSc3	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
kuchyně	kuchyně	k1gFnSc1	kuchyně
také	také	k9	také
ovlivňila	ovlivňit	k5eAaBmAgFnS	ovlivňit
řeckou	řecký	k2eAgFnSc4d1	řecká
a	a	k8xC	a
balkánskou	balkánský	k2eAgFnSc4d1	balkánská
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tzatziki	tzatziki	k6eAd1	tzatziki
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
tureckého	turecký	k2eAgNnSc2d1	turecké
slova	slovo	k1gNnSc2	slovo
cacı	cacı	k?	cacı
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
džažik	džažik	k1gInSc1	džažik
<g/>
)	)	kIx)	)
a	a	k8xC	a
čevabčiči	čevabčiči	k1gNnSc1	čevabčiči
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
kebab	kebab	k1gInSc4	kebab
şişi	şişe	k1gFnSc4	şişe
(	(	kIx(	(
<g/>
kebabový	kebabový	k2eAgInSc1d1	kebabový
špíz	špíz	k1gInSc1	špíz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jogurt	jogurt	k1gInSc1	jogurt
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tureckého	turecký	k2eAgNnSc2d1	turecké
slova	slovo	k1gNnSc2	slovo
yoğ	yoğ	k?	yoğ
<g/>
.	.	kIx.	.
</s>
<s>
Döner	Döner	k1gInSc1	Döner
kebab	kebab	k1gInSc1	kebab
je	být	k5eAaImIp3nS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
hovězího	hovězí	k1gNnSc2	hovězí
<g/>
,	,	kIx,	,
telecího	telecí	k2eAgNnSc2d1	telecí
nebo	nebo	k8xC	nebo
drůbežího	drůbeží	k2eAgNnSc2d1	drůbeží
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
(	(	kIx(	(
<g/>
evropských	evropský	k2eAgFnPc6d1	Evropská
<g/>
)	)	kIx)	)
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
döner	döner	k1gMnSc1	döner
kebab	kebab	k1gInSc4	kebab
servíruje	servírovat	k5eAaBmIp3nS	servírovat
na	na	k7c6	na
talíři	talíř	k1gInSc6	talíř
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nápojů	nápoj	k1gInPc2	nápoj
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
čaje	čaj	k1gInSc2	čaj
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc4d1	populární
ajran	ajran	k1gInSc4	ajran
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
zákusky	zákusek	k1gInPc4	zákusek
baklava	baklava	k1gFnSc1	baklava
a	a	k8xC	a
chalva	chalva	k1gFnSc1	chalva
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
přípravy	příprava	k1gFnSc2	příprava
kávy	káva	k1gFnSc2	káva
v	v	k7c6	v
džezvě	džezva	k1gFnSc6	džezva
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
kovové	kovový	k2eAgFnSc3d1	kovová
konvičce	konvička	k1gFnSc3	konvička
<g/>
,	,	kIx,	,
s	s	k7c7	s
kávou	káva	k1gFnSc7	káva
připravovanou	připravovaný	k2eAgFnSc7d1	připravovaná
v	v	k7c6	v
česku	česko	k1gNnSc6	česko
(	(	kIx(	(
<g/>
turek	turek	k1gMnSc1	turek
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
společný	společný	k2eAgInSc4d1	společný
jen	jen	k9	jen
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
domácí	domácí	k2eAgFnSc1d1	domácí
soutěž	soutěž	k1gFnSc1	soutěž
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k9	jako
Süper	Süpero	k1gNnPc2	Süpero
Lig	liga	k1gFnPc2	liga
hraje	hrát	k5eAaImIp3nS	hrát
18	[number]	k4	18
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
istanbulské	istanbulský	k2eAgInPc1d1	istanbulský
velkokluby	velkoklub	k1gInPc1	velkoklub
Beşiktaş	Beşiktaş	k1gFnSc2	Beşiktaş
<g/>
,	,	kIx,	,
Fenerbahçe	Fenerbahç	k1gFnSc2	Fenerbahç
a	a	k8xC	a
Galatasaray	Galatasaraa	k1gFnSc2	Galatasaraa
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
a	a	k8xC	a
stejné	stejný	k2eAgNnSc4d1	stejné
umístění	umístění	k1gNnSc4	umístění
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Atatürkově	Atatürkův	k2eAgInSc6d1	Atatürkův
olympijském	olympijský	k2eAgInSc6d1	olympijský
stadionu	stadion	k1gInSc6	stadion
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
finále	finále	k1gNnSc7	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
na	na	k7c6	na
Fenerbahçe	Fenerbahçe	k1gFnSc6	Fenerbahçe
Şükrü	Şükrü	k1gFnPc2	Şükrü
Saracoğ	Saracoğ	k1gMnSc2	Saracoğ
Stadyumu	Stadyum	k1gInSc2	Stadyum
finále	finále	k1gNnSc2	finále
poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Galatasaray	Galatasaraa	k1gFnPc1	Galatasaraa
<g/>
,	,	kIx,	,
Pohár	pohár	k1gInSc1	pohár
UEFA	UEFA	kA	UEFA
a	a	k8xC	a
Superpohár	superpohár	k1gInSc4	superpohár
UEFA	UEFA	kA	UEFA
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uspořádalo	uspořádat	k5eAaPmAgNnS	uspořádat
Turecko	Turecko	k1gNnSc1	Turecko
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
velmi	velmi	k6eAd1	velmi
sledovaným	sledovaný	k2eAgInSc7d1	sledovaný
sportem	sport	k1gInSc7	sport
je	být	k5eAaImIp3nS	být
basketbal	basketbal	k1gInSc4	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
hostila	hostit	k5eAaImAgFnS	hostit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
hostila	hostit	k5eAaImAgFnS	hostit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
pravidelně	pravidelně	k6eAd1	pravidelně
konaly	konat	k5eAaImAgInP	konat
závody	závod	k1gInPc1	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Biochemik	biochemik	k1gMnSc1	biochemik
Aziz	Aziz	k1gMnSc1	Aziz
Sancar	Sancar	k1gMnSc1	Sancar
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Proslulým	proslulý	k2eAgMnSc7d1	proslulý
středověkým	středověký	k2eAgMnSc7d1	středověký
kartografem	kartograf	k1gMnSc7	kartograf
byl	být	k5eAaImAgMnS	být
Piri	Piri	k1gNnSc4	Piri
Reis	Reis	k1gInSc1	Reis
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
známé	známý	k2eAgFnSc2d1	známá
mapy	mapa	k1gFnSc2	mapa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
objevení	objevení	k1gNnSc1	objevení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
senzaci	senzace	k1gFnSc4	senzace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
mimořádně	mimořádně	k6eAd1	mimořádně
přesně	přesně	k6eAd1	přesně
jihoamerický	jihoamerický	k2eAgInSc1d1	jihoamerický
kontinent	kontinent	k1gInSc1	kontinent
a	a	k8xC	a
také	také	k9	také
Antarktidu	Antarktida	k1gFnSc4	Antarktida
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
existenci	existence	k1gFnSc6	existence
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nevědělo	vědět	k5eNaImAgNnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Výborných	výborný	k2eAgInPc2d1	výborný
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
kartografii	kartografie	k1gFnSc6	kartografie
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
i	i	k8xC	i
Kâtip	Kâtip	k1gInSc4	Kâtip
Çelebi	Çeleb	k1gFnSc2	Çeleb
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vlivným	vlivný	k2eAgMnSc7d1	vlivný
ekonomem	ekonom	k1gMnSc7	ekonom
a	a	k8xC	a
profesorem	profesor	k1gMnSc7	profesor
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
Dani	daň	k1gFnSc3	daň
Rodrik	Rodrika	k1gFnPc2	Rodrika
<g/>
.	.	kIx.	.
</s>
<s>
Sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
politolog	politolog	k1gMnSc1	politolog
Ziya	Ziya	k1gMnSc1	Ziya
Gökalp	Gökalp	k1gMnSc1	Gökalp
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
moderní	moderní	k2eAgInSc4d1	moderní
turecký	turecký	k2eAgInSc4d1	turecký
nacionalismus	nacionalismus	k1gInSc4	nacionalismus
a	a	k8xC	a
kemalismus	kemalismus	k1gInSc4	kemalismus
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
otomanismu	otomanismus	k1gInSc3	otomanismus
a	a	k8xC	a
islamizaci	islamizace	k1gFnSc3	islamizace
a	a	k8xC	a
bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c4	za
modernizaci	modernizace	k1gFnSc4	modernizace
a	a	k8xC	a
evropeizaci	evropeizace	k1gFnSc4	evropeizace
tureckého	turecký	k2eAgInSc2d1	turecký
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Muzafer	Muzafer	k1gMnSc1	Muzafer
Sherif	Sherif	k1gMnSc1	Sherif
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
sociální	sociální	k2eAgFnSc2d1	sociální
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
odborníkem	odborník	k1gMnSc7	odborník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
osmanských	osmanský	k2eAgFnPc2d1	Osmanská
dějin	dějiny	k1gFnPc2	dějiny
byl	být	k5eAaImAgMnS	být
historik	historik	k1gMnSc1	historik
Halil	halit	k5eAaImAgMnS	halit
İ	İ	k?	İ
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
Turkey	Turkea	k1gFnSc2	Turkea
–	–	k?	–
The	The	k1gFnSc2	The
Quiet	Quieta	k1gFnPc2	Quieta
Revolution	Revolution	k1gInSc1	Revolution
at	at	k?	at
the	the	k?	the
Edge	Edge	k1gInSc1	Edge
of	of	k?	of
Europe	Europ	k1gInSc5	Europ
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Turecko	Turecko	k1gNnSc1	Turecko
–	–	k?	–
tichá	tichý	k2eAgFnSc1d1	tichá
revoluce	revoluce	k1gFnSc1	revoluce
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Morris	Morris	k1gFnSc1	Morris
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Granta	Granta	k1gFnSc1	Granta
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-86207-790-8	[number]	k4	1-86207-790-8
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
KREISER	KREISER	kA	KREISER
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
;	;	kIx,	;
NEUMANN	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
Christoph	Christoph	k1gMnSc1	Christoph
K.	K.	kA	K.
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
MUSIL	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Mosta	Mosta	k1gFnSc1	Mosta
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
:	:	kIx,	:
Nové	Nové	k2eAgNnSc1d1	Nové
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
262	[number]	k4	262
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
PIRICKÝ	PIRICKÝ	kA	PIRICKÝ
<g/>
,	,	kIx,	,
Gabriel	Gabriel	k1gMnSc1	Gabriel
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Turecký	turecký	k2eAgInSc1d1	turecký
Kurdistán	Kurdistán	k1gInSc1	Kurdistán
Severní	severní	k2eAgFnSc2d1	severní
Thrákie	Thrákie	k1gFnSc2	Thrákie
Západní	západní	k2eAgFnSc2d1	západní
Thrákie	Thrákie	k1gFnSc2	Thrákie
Thrákie	Thrákie	k1gFnSc2	Thrákie
Východní	východní	k2eAgFnSc2d1	východní
Thrákie	Thrákie	k1gFnSc2	Thrákie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Turecko	Turecko	k1gNnSc4	Turecko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Turecko	Turecko	k1gNnSc1	Turecko
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc1	heslo
Turecko	Turecko	k1gNnSc4	Turecko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Turecko	Turecko	k1gNnSc1	Turecko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Turecké	turecký	k2eAgNnSc1d1	turecké
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Turecká	turecký	k2eAgFnSc1d1	turecká
ekonomika	ekonomika	k1gFnSc1	ekonomika
a	a	k8xC	a
statistiky	statistika	k1gFnSc2	statistika
Turkey	Turke	k2eAgInPc1d1	Turke
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Turkey	Turkey	k1gInPc1	Turkey
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Turkey	Turkea	k1gMnSc2	Turkea
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k2eAgFnSc4d1	Burea
of	of	k?	of
European	Europeana	k1gFnPc2	Europeana
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gFnSc2	Not
<g/>
:	:	kIx,	:
Turkey	Turkea	k1gFnSc2	Turkea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Turkey	Turkea	k1gFnPc1	Turkea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Turkey	Turke	k2eAgMnPc4d1	Turke
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Ankaře	Ankara	k1gFnSc6	Ankara
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Turecko	Turecko	k1gNnSc1	Turecko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
DEWDNEY	DEWDNEY	kA	DEWDNEY
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
C	C	kA	C
<g/>
;	;	kIx,	;
YAPP	YAPP	kA	YAPP
<g/>
,	,	kIx,	,
Malcolm	Malcolm	k1gMnSc1	Malcolm
Edward	Edward	k1gMnSc1	Edward
<g/>
.	.	kIx.	.
</s>
<s>
Turkey	Turkea	k1gFnPc1	Turkea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Turecká	turecký	k2eAgFnSc1d1	turecká
kuchyňa	kuchyňa	k1gFnSc1	kuchyňa
|	|	kIx~	|
Chefparade	Chefparad	k1gInSc5	Chefparad
</s>
