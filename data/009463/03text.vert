<p>
<s>
Klasika	klasika	k1gFnSc1	klasika
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
jednodenního	jednodenní	k2eAgInSc2d1	jednodenní
závodu	závod	k1gInSc2	závod
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nP	jezdit
každoročně	každoročně	k6eAd1	každoročně
ve	v	k7c6	v
zhruba	zhruba	k6eAd1	zhruba
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
a	a	k8xC	a
po	po	k7c6	po
zhruba	zhruba	k6eAd1	zhruba
stejné	stejný	k2eAgFnSc6d1	stejná
trati	trať	k1gFnSc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Pětice	pětice	k1gFnSc1	pětice
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
klasik	klasika	k1gFnPc2	klasika
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Monumenty	monument	k1gInPc1	monument
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vesměs	vesměs	k6eAd1	vesměs
tradici	tradice	k1gFnSc4	tradice
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Monumenty	monument	k1gInPc4	monument
==	==	k?	==
</s>
</p>
<p>
<s>
Milán	Milán	k1gInSc1	Milán
-	-	kIx~	-
San	San	k1gMnSc1	San
Remo	Remo	k1gMnSc1	Remo
–	–	k?	–
zvaná	zvaný	k2eAgNnPc1d1	zvané
La	la	k1gNnSc3	la
Primavera	Primavera	k1gFnSc1	Primavera
(	(	kIx(	(
<g/>
Jaro	jaro	k1gNnSc1	jaro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
Flander	Flandry	k1gInPc2	Flandry
–	–	k?	–
zvaná	zvaný	k2eAgNnPc1d1	zvané
Vlaanderens	Vlaanderens	k1gInSc1	Vlaanderens
mooiste	mooist	k1gMnSc5	mooist
(	(	kIx(	(
<g/>
Flanderská	flanderský	k2eAgFnSc1d1	Flanderská
nejjemnější	jemný	k2eAgFnSc1d3	nejjemnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
</s>
</p>
<p>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
Roubaix	Roubaix	k1gInSc1	Roubaix
–	–	k?	–
zvaná	zvaný	k2eAgFnSc1d1	zvaná
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Enfer	Enfer	k1gMnSc1	Enfer
du	du	k?	du
Nord	Nord	k1gInSc1	Nord
(	(	kIx(	(
<g/>
Peklo	peklo	k1gNnSc1	peklo
Severu	sever	k1gInSc2	sever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
</s>
</p>
<p>
<s>
Lutych-Bastogne-Lutych	Lutych-Bastogne-Lutych	k1gInSc1	Lutych-Bastogne-Lutych
–	–	k?	–
zvaná	zvaný	k2eAgNnPc1d1	zvané
La	la	k0	la
Doyenne	Doyenn	k1gInSc5	Doyenn
(	(	kIx(	(
<g/>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
</s>
</p>
<p>
<s>
Giro	Giro	k1gMnSc1	Giro
di	di	k?	di
Lombardia	Lombardium	k1gNnSc2	Lombardium
–	–	k?	–
zvaná	zvaný	k2eAgFnSc1d1	zvaná
La	la	k1gNnSc7	la
classica	classic	k1gInSc2	classic
delle	dell	k1gMnSc2	dell
foglie	foglie	k1gFnSc2	foglie
morte	morte	k5eAaPmIp2nP	morte
(	(	kIx(	(
<g/>
Závod	závod	k1gInSc1	závod
padajícího	padající	k2eAgNnSc2d1	padající
listí	listí	k1gNnSc2	listí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
Další	další	k2eAgFnPc1d1	další
důležité	důležitý	k2eAgFnPc1d1	důležitá
klasiky	klasika	k1gFnPc1	klasika
jsou	být	k5eAaImIp3nP	být
Amstel	Amstel	k1gInSc4	Amstel
Gold	Gold	k1gInSc1	Gold
Race	Race	k1gInSc1	Race
<g/>
,	,	kIx,	,
Valonský	valonský	k2eAgInSc1d1	valonský
šíp	šíp	k1gInSc1	šíp
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
tvoří	tvořit	k5eAaImIp3nP	tvořit
společně	společně	k6eAd1	společně
se	s	k7c7	s
závodem	závod	k1gInSc7	závod
Lutych-Bastogne-Lutych	Lutych-Bastogne-Lutych	k1gMnSc1	Lutych-Bastogne-Lutych
trojici	trojice	k1gFnSc3	trojice
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
Ardenských	ardenský	k2eAgFnPc2d1	Ardenská
klasik	klasika	k1gFnPc2	klasika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ouverture	Ouvertur	k1gMnSc5	Ouvertur
La	la	k1gNnSc4	la
Marseillaise	Marseillaisa	k1gFnSc3	Marseillaisa
<g/>
,	,	kIx,	,
Omloop	Omloop	k1gInSc4	Omloop
Het	Het	k1gFnSc2	Het
Nieuwsblad	Nieuwsblad	k1gInSc1	Nieuwsblad
<g/>
,	,	kIx,	,
Paříž-Tours	Paříž-Tours	k1gInSc1	Paříž-Tours
<g/>
,	,	kIx,	,
Clásica	Clásica	k1gMnSc1	Clásica
de	de	k?	de
San	San	k1gMnSc1	San
Sebastián	Sebastián	k1gMnSc1	Sebastián
a	a	k8xC	a
Milán-Turín	Milán-Turín	k1gMnSc1	Milán-Turín
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
klasikou	klasika	k1gFnSc7	klasika
byl	být	k5eAaImAgInS	být
závod	závod	k1gInSc1	závod
Paříž-Bordeaux	Paříž-Bordeaux	k1gInSc4	Paříž-Bordeaux
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
okolo	okolo	k7c2	okolo
560	[number]	k4	560
km	km	kA	km
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
jezdil	jezdit	k5eAaImAgMnS	jezdit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasiky	klasika	k1gFnPc1	klasika
jsou	být	k5eAaImIp3nP	být
jezdecky	jezdecky	k6eAd1	jezdecky
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nedávají	dávat	k5eNaImIp3nP	dávat
šanci	šance	k1gFnSc4	šance
k	k	k7c3	k
taktizování	taktizování	k1gNnSc3	taktizování
a	a	k8xC	a
šetření	šetření	k1gNnSc3	šetření
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
starých	starý	k2eAgFnPc6d1	stará
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
ještě	ještě	k9	ještě
s	s	k7c7	s
kostkovým	kostkový	k2eAgInSc7d1	kostkový
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nebo	nebo	k8xC	nebo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
cyklistické	cyklistický	k2eAgFnSc2d1	cyklistická
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bývá	bývat	k5eAaImIp3nS	bývat
počasí	počasí	k1gNnSc1	počasí
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
závodníkům	závodník	k1gMnPc3	závodník
jízdu	jízda	k1gFnSc4	jízda
velmi	velmi	k6eAd1	velmi
znepříjemnit	znepříjemnit	k5eAaPmF	znepříjemnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
až	až	k9	až
2004	[number]	k4	2004
byly	být	k5eAaImAgFnP	být
klasiky	klasika	k1gFnPc1	klasika
součástí	součást	k1gFnPc2	součást
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://saltr.cz/2012/jarni-klasiky-jsou-tady/	[url]	k4	http://saltr.cz/2012/jarni-klasiky-jsou-tady/
</s>
</p>
<p>
<s>
http://sport.idnes.cz/kostky-bahno-bolest-jarni-klasiky-to-je-esence-cyklistiky-p3l-/cyklistika.aspx?c=A100420_213417_cyklistika_par	[url]	k1gMnSc1	http://sport.idnes.cz/kostky-bahno-bolest-jarni-klasiky-to-je-esence-cyklistiky-p3l-/cyklistika.aspx?c=A100420_213417_cyklistika_par
</s>
</p>
