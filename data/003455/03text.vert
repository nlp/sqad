<s>
William	William	k6eAd1	William
Clark	Clark	k1gInSc1	Clark
Styron	Styron	k1gInSc1	Styron
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc1	Jr
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
stajrn	stajrn	k1gInSc1	stajrn
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1925	[number]	k4	1925
Newport	Newport	k1gInSc1	Newport
News	News	k1gInSc4	News
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc1	Virginie
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
Martha	Martha	k1gMnSc1	Martha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Vineyard	Vineyard	k1gInSc1	Vineyard
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
tzv.	tzv.	kA	tzv.
jižanské	jižanský	k2eAgFnSc2d1	jižanská
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Proslul	proslout	k5eAaPmAgInS	proslout
především	především	k6eAd1	především
románem	román	k1gInSc7	román
Sophiina	Sophiin	k2eAgFnSc1d1	Sophiina
volba	volba	k1gFnSc1	volba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Dukeovu	Dukeův	k2eAgFnSc4d1	Dukeova
universitu	universita	k1gFnSc4	universita
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgMnS	působit
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
jako	jako	k8xC	jako
nakladatelský	nakladatelský	k2eAgMnSc1d1	nakladatelský
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
činnost	činnost	k1gFnSc1	činnost
ho	on	k3xPp3gMnSc4	on
neuspokojovala	uspokojovat	k5eNaImAgFnS	uspokojovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
vlastní	vlastní	k2eAgFnSc3d1	vlastní
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
Doznání	doznání	k1gNnSc2	doznání
Nata	Natus	k1gMnSc2	Natus
Turnera	turner	k1gMnSc2	turner
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
v	v	k7c4	v
Newport	Newport	k1gInSc4	Newport
News	Newsa	k1gFnPc2	Newsa
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
námořního	námořní	k2eAgMnSc2d1	námořní
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
,	,	kIx,	,
svérázného	svérázný	k2eAgMnSc2d1	svérázný
zásadového	zásadový	k2eAgMnSc2d1	zásadový
Jižana	Jižan	k1gMnSc2	Jižan
<g/>
,	,	kIx,	,
v	v	k7c6	v
podmanivé	podmanivý	k2eAgFnSc6d1	podmanivá
atmosféře	atmosféra	k1gFnSc6	atmosféra
kouzelného	kouzelný	k2eAgInSc2d1	kouzelný
Jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
však	však	k9	však
mladý	mladý	k2eAgInSc4d1	mladý
Styron	Styron	k1gInSc4	Styron
vycítil	vycítit	k5eAaPmAgMnS	vycítit
i	i	k9	i
ovzduší	ovzduší	k1gNnSc4	ovzduší
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ironicky	ironicky	k6eAd1	ironicky
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
arktický	arktický	k2eAgInSc1d1	arktický
kraj	kraj	k1gInSc1	kraj
"	"	kIx"	"
<g/>
Byrdland	Byrdland	k1gInSc1	Byrdland
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
absolutní	absolutní	k2eAgNnSc1d1	absolutní
srdce	srdce	k1gNnSc1	srdce
zmrtvělé	zmrtvělý	k2eAgFnSc2d1	zmrtvělá
kultury	kultura	k1gFnSc2	kultura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
duch	duch	k1gMnSc1	duch
ante	antat	k5eAaPmIp3nS	antat
bellum	bellum	k1gNnSc4	bellum
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
doby	doba	k1gFnPc4	doba
před	před	k7c7	před
vítězstvím	vítězství	k1gNnSc7	vítězství
Unie	unie	k1gFnSc2	unie
nad	nad	k7c7	nad
Konfederací	konfederace	k1gFnSc7	konfederace
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
realitou	realita	k1gFnSc7	realita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
presbyteriánskou	presbyteriánský	k2eAgFnSc4d1	presbyteriánská
Davidson	Davidson	k1gNnSc4	Davidson
College	College	k1gNnSc2	College
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Dukeovu	Dukeův	k2eAgFnSc4d1	Dukeova
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
;	;	kIx,	;
tam	tam	k6eAd1	tam
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
válečnému	válečný	k2eAgInSc3d1	válečný
stavu	stav	k1gInSc3	stav
zároveň	zároveň	k6eAd1	zároveň
prodělával	prodělávat	k5eAaImAgInS	prodělávat
výcvik	výcvik	k1gInSc1	výcvik
u	u	k7c2	u
jednotky	jednotka	k1gFnSc2	jednotka
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
příme	přít	k5eAaImIp1nP	přít
akce	akce	k1gFnPc1	akce
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
mladistvému	mladistvý	k2eAgInSc3d1	mladistvý
věku	věk	k1gInSc3	věk
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
na	na	k7c4	na
Dukeovu	Dukeův	k2eAgFnSc4d1	Dukeova
univerzitu	univerzita	k1gFnSc4	univerzita
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
prvotině	prvotina	k1gFnSc6	prvotina
Ulehni	ulehnout	k5eAaPmRp2nS	ulehnout
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
tu	tu	k6eAd1	tu
také	také	k9	také
seminář	seminář	k1gInSc1	seminář
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
tedy	tedy	k8xC	tedy
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
jeho	jeho	k3xOp3gInPc1	jeho
první	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
povídkovou	povídkový	k2eAgFnSc4d1	povídková
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgNnPc1d1	budoucí
velká	velký	k2eAgNnPc1d1	velké
témata	téma	k1gNnPc1	téma
signalizují	signalizovat	k5eAaImIp3nP	signalizovat
krátké	krátký	k2eAgFnPc4d1	krátká
prózy	próza	k1gFnPc4	próza
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
temná	temný	k2eAgFnSc1d1	temná
cesta	cesta	k1gFnSc1	cesta
a	a	k8xC	a
Chvíle	chvíle	k1gFnSc1	chvíle
v	v	k7c6	v
Terstu	Terst	k1gInSc6	Terst
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
prvního	první	k4xOgNnSc2	první
díla	dílo	k1gNnPc4	dílo
Ulehni	ulehnout	k5eAaPmRp2nS	ulehnout
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgMnS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c6	po
relativně	relativně	k6eAd1	relativně
krátkém	krátký	k2eAgNnSc6d1	krátké
redaktorském	redaktorský	k2eAgNnSc6d1	redaktorské
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
McGraw-Hill	McGraw-Hilla	k1gFnPc2	McGraw-Hilla
a	a	k8xC	a
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
slavné	slavný	k2eAgFnSc2d1	slavná
The	The	k1gFnSc2	The
Paris	Paris	k1gMnSc1	Paris
Review	Review	k1gMnSc1	Review
věnoval	věnovat	k5eAaImAgMnS	věnovat
cele	cele	k6eAd1	cele
a	a	k8xC	a
výhradně	výhradně	k6eAd1	výhradně
dráze	dráha	k1gFnSc3	dráha
spisovatelské	spisovatelský	k2eAgFnSc3d1	spisovatelská
<g/>
.	.	kIx.	.
</s>
<s>
Poválečný	poválečný	k2eAgInSc1d1	poválečný
pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
Styrona	Styron	k1gMnSc2	Styron
vyprovokoval	vyprovokovat	k5eAaPmAgMnS	vyprovokovat
k	k	k7c3	k
napsání	napsání	k1gNnSc3	napsání
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejpůsobivějších	působivý	k2eAgNnPc2d3	nejpůsobivější
děl	dělo	k1gNnPc2	dělo
trpce	trpce	k6eAd1	trpce
ironizujících	ironizující	k2eAgInPc2d1	ironizující
nevykořenitelný	vykořenitelný	k2eNgInSc4d1	nevykořenitelný
militarismus	militarismus	k1gInSc4	militarismus
<g/>
,	,	kIx,	,
útlé	útlý	k2eAgFnPc4d1	útlá
novelky	novelka	k1gFnPc4	novelka
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
pochod	pochod	k1gInSc1	pochod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
problému	problém	k1gInSc3	problém
amerického	americký	k2eAgNnSc2d1	americké
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
případu	případ	k1gInSc2	případ
jeho	jeho	k3xOp3gFnSc2	jeho
individuální	individuální	k2eAgFnSc2d1	individuální
anarchické	anarchický	k2eAgFnSc2d1	anarchická
likvidace	likvidace	k1gFnSc2	likvidace
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
románem	román	k1gInSc7	román
Zapal	zapálit	k5eAaPmRp2nS	zapálit
tento	tento	k3xDgInSc4	tento
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
román	román	k1gInSc1	román
Doznání	doznání	k1gNnSc2	doznání
Nata	Nat	k2eAgMnSc4d1	Nat
Turnera	turner	k1gMnSc4	turner
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
bouři	bouře	k1gFnSc4	bouře
nevole	nevole	k1gFnSc2	nevole
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
černošského	černošský	k2eAgNnSc2d1	černošské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
román	román	k1gInSc1	román
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
vzdělaného	vzdělaný	k2eAgMnSc2d1	vzdělaný
čenocha	čenoch	k1gMnSc2	čenoch
a	a	k8xC	a
vůdce	vůdce	k1gMnSc2	vůdce
černošské	černošský	k2eAgFnSc2d1	černošská
vzpoury	vzpoura	k1gFnSc2	vzpoura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
pro	pro	k7c4	pro
nepochopení	nepochopení	k1gNnSc4	nepochopení
autorova	autorův	k2eAgInSc2d1	autorův
záměru	záměr	k1gInSc2	záměr
<g/>
:	:	kIx,	:
Styron	Styron	k1gMnSc1	Styron
nezamýšlel	zamýšlet	k5eNaImAgMnS	zamýšlet
pouze	pouze	k6eAd1	pouze
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
dějiny	dějiny	k1gFnPc4	dějiny
a	a	k8xC	a
kopírovat	kopírovat	k5eAaImF	kopírovat
historickou	historický	k2eAgFnSc4d1	historická
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sestoupením	sestoupení	k1gNnSc7	sestoupení
do	do	k7c2	do
hrdinovy	hrdinův	k2eAgFnSc2d1	hrdinova
mysli	mysl	k1gFnSc2	mysl
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
dobrat	dobrat	k5eAaPmF	dobrat
pravdy	pravda	k1gFnPc4	pravda
o	o	k7c6	o
zasutých	zasutý	k2eAgFnPc6d1	zasutá
i	i	k8xC	i
otevřeně	otevřeně	k6eAd1	otevřeně
planoucích	planoucí	k2eAgFnPc6d1	planoucí
alergiích	alergie	k1gFnPc6	alergie
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
černých	černý	k2eAgMnPc2d1	černý
a	a	k8xC	a
bílých	bílý	k2eAgMnPc2d1	bílý
<g/>
,	,	kIx,	,
detailně	detailně	k6eAd1	detailně
evokovat	evokovat	k5eAaBmF	evokovat
myšlenkové	myšlenkový	k2eAgInPc4d1	myšlenkový
pochody	pochod	k1gInPc4	pochod
a	a	k8xC	a
zjitřené	zjitřený	k2eAgNnSc4d1	zjitřené
cítění	cítění	k1gNnSc4	cítění
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
a	a	k8xC	a
srdci	srdce	k1gNnSc6	srdce
černošského	černošský	k2eAgMnSc2d1	černošský
rebela	rebel	k1gMnSc2	rebel
<g/>
.	.	kIx.	.
</s>
<s>
Styron	Styron	k1gInSc1	Styron
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
autorem	autor	k1gMnSc7	autor
jediného	jediný	k2eAgNnSc2d1	jediné
dramatu	drama	k1gNnSc2	drama
Na	na	k7c6	na
triplárně	triplárna	k1gFnSc6	triplárna
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gFnSc1	drama
připomíná	připomínat	k5eAaImIp3nS	připomínat
zpodobením	zpodobení	k1gNnSc7	zpodobení
bludného	bludný	k2eAgInSc2d1	bludný
kruhu	kruh	k1gInSc2	kruh
vojenského	vojenský	k2eAgInSc2d1	vojenský
světa	svět	k1gInSc2	svět
novelku	novelka	k1gFnSc4	novelka
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
pochod	pochod	k1gInSc1	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
vyšel	vyjít	k5eAaPmAgInS	vyjít
jeho	jeho	k3xOp3gInSc1	jeho
nejznámější	známý	k2eAgInSc1d3	nejznámější
román	román	k1gInSc1	román
Sophiina	Sophiin	k2eAgFnSc1d1	Sophiina
volba	volba	k1gFnSc1	volba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
zabývá	zabývat	k5eAaImIp3nS	zabývat
otázkou	otázka	k1gFnSc7	otázka
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
viny	vina	k1gFnSc2	vina
a	a	k8xC	a
absolutního	absolutní	k2eAgNnSc2d1	absolutní
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
Sophiiny	Sophiin	k2eAgFnSc2d1	Sophiina
volby	volba	k1gFnSc2	volba
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Styron	Styron	k1gInSc4	Styron
pouze	pouze	k6eAd1	pouze
Tichý	tichý	k2eAgInSc4d1	tichý
prach	prach	k1gInSc4	prach
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
prozaik	prozaik	k1gMnSc1	prozaik
se	se	k3xPyFc4	se
odmlčel	odmlčet	k5eAaPmAgMnS	odmlčet
až	až	k6eAd1	až
do	do	k7c2	do
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
talent	talent	k1gInSc4	talent
opět	opět	k6eAd1	opět
upozornil	upozornit	k5eAaPmAgMnS	upozornit
sebezpytující	sebezpytující	k2eAgFnSc7d1	sebezpytující
výpovědí	výpověď	k1gFnSc7	výpověď
o	o	k7c6	o
boji	boj	k1gInSc6	boj
s	s	k7c7	s
depresí	deprese	k1gFnSc7	deprese
Viditelná	viditelný	k2eAgFnSc1d1	viditelná
temnota	temnota	k1gFnSc1	temnota
a	a	k8xC	a
sbírkou	sbírka	k1gFnSc7	sbírka
Ráno	ráno	k6eAd1	ráno
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
souborem	soubor	k1gInSc7	soubor
tří	tři	k4xCgFnPc2	tři
delších	dlouhý	k2eAgFnPc2d2	delší
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
poetickými	poetický	k2eAgFnPc7d1	poetická
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
mládí	mládí	k1gNnSc4	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
romány	román	k1gInPc1	román
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
otázkami	otázka	k1gFnPc7	otázka
trestu	trest	k1gInSc2	trest
a	a	k8xC	a
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
jako	jako	k9	jako
by	by	kYmCp3nS	by
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
F.	F.	kA	F.
M.	M.	kA	M.
Dostojevského	Dostojevský	k2eAgMnSc4d1	Dostojevský
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
jižanskému	jižanský	k2eAgNnSc3d1	jižanské
prostředí	prostředí	k1gNnSc3	prostředí
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
dílem	díl	k1gInSc7	díl
W.	W.	kA	W.
Faulknera	Faulkner	k1gMnSc2	Faulkner
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
stěžejním	stěžejní	k2eAgNnSc7d1	stěžejní
tématem	téma	k1gNnSc7	téma
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
rasismus	rasismus	k1gInSc1	rasismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
nečekaných	čekaný	k2eNgInPc6d1	nečekaný
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
i	i	k8xC	i
šokujících	šokující	k2eAgFnPc6d1	šokující
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Ulehni	ulehnout	k5eAaPmRp2nS	ulehnout
v	v	k7c6	v
temnotách	temnota	k1gFnPc6	temnota
(	(	kIx(	(
<g/>
Lie	Lie	k1gFnSc1	Lie
Down	Down	k1gInSc1	Down
in	in	k?	in
Darkness	Darkness	k1gInSc1	Darkness
<g/>
;	;	kIx,	;
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
rozpadem	rozpad	k1gInSc7	rozpad
rodiny	rodina	k1gFnSc2	rodina
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
pochod	pochod	k1gInSc1	pochod
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
March	March	k1gMnSc1	March
<g/>
;	;	kIx,	;
1952	[number]	k4	1952
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
jako	jako	k8xC	jako
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
Zapal	zapálit	k5eAaPmRp2nS	zapálit
ten	ten	k3xDgInSc1	ten
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Set	set	k1gInSc1	set
This	Thisa	k1gFnPc2	Thisa
House	house	k1gNnSc1	house
on	on	k3xPp3gMnSc1	on
Fire	Fire	k1gNnSc7	Fire
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
–	–	k?	–
příběh	příběh	k1gInSc1	příběh
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
snaží	snažit	k5eAaImIp3nP	snažit
utéci	utéct	k5eAaPmF	utéct
moci	moct	k5eAaImF	moct
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
korumpující	korumpující	k2eAgNnSc4d1	korumpující
Doznání	doznání	k1gNnSc4	doznání
Nata	Natus	k1gMnSc2	Natus
Turnera	turner	k1gMnSc2	turner
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Confessions	Confessionsa	k1gFnPc2	Confessionsa
of	of	k?	of
Nat	Nat	k1gMnSc1	Nat
Turner	turner	k1gMnSc1	turner
<g/>
;	;	kIx,	;
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
–	–	k?	–
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
nečernošský	černošský	k2eNgMnSc1d1	černošský
spisovatel	spisovatel	k1gMnSc1	spisovatel
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
popsat	popsat	k5eAaPmF	popsat
pohnutky	pohnutka	k1gFnPc4	pohnutka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
černošskému	černošský	k2eAgNnSc3d1	černošské
povstání	povstání	k1gNnSc3	povstání
Nata	Natus	k1gMnSc2	Natus
Turnera	turner	k1gMnSc2	turner
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
není	být	k5eNaImIp3nS	být
kronikou	kronika	k1gFnSc7	kronika
tohoto	tento	k3xDgNnSc2	tento
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
toto	tento	k3xDgNnSc4	tento
povstání	povstání	k1gNnSc4	povstání
neodsuzuje	odsuzovat	k5eNaImIp3nS	odsuzovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
považuje	považovat	k5eAaImIp3nS	považovat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
čin	čin	k1gInSc4	čin
zoufalých	zoufalý	k2eAgMnPc2d1	zoufalý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sophiina	Sophiin	k2eAgFnSc1d1	Sophiina
volba	volba	k1gFnSc1	volba
(	(	kIx(	(
<g/>
Sophie	Sophie	k1gFnSc1	Sophie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Choice	Choic	k1gMnSc2	Choic
<g/>
;	;	kIx,	;
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
–	–	k?	–
Styronovo	Styronův	k2eAgNnSc1d1	Styronův
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
psychologický	psychologický	k2eAgInSc1d1	psychologický
román	román	k1gInSc1	román
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
složitou	složitý	k2eAgFnSc4d1	složitá
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
jižanském	jižanský	k2eAgMnSc6d1	jižanský
mladíkovi	mladík	k1gMnSc6	mladík
Stingovi	Sting	k1gMnSc6	Sting
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
touží	toužit	k5eAaImIp3nS	toužit
stát	stát	k5eAaPmF	stát
spisovatelem	spisovatel	k1gMnSc7	spisovatel
a	a	k8xC	a
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
s	s	k7c7	s
krásnou	krásný	k2eAgFnSc7d1	krásná
Polkou	Polka	k1gFnSc7	Polka
Sophií	Sophie	k1gFnSc7	Sophie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
přežila	přežít	k5eAaPmAgFnS	přežít
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
milencem	milenec	k1gMnSc7	milenec
-	-	kIx~	-
uchvacujícím	uchvacující	k2eAgMnSc7d1	uchvacující
a	a	k8xC	a
nebezpečným	bezpečný	k2eNgMnSc7d1	nebezpečný
Nathanem	Nathan	k1gMnSc7	Nathan
<g/>
.	.	kIx.	.
</s>
<s>
This	This	k6eAd1	This
Quiet	Quiet	k1gMnSc1	Quiet
Dust	Dust	k1gMnSc1	Dust
<g/>
,	,	kIx,	,
and	and	k?	and
Other	Other	k1gInSc1	Other
Writings	Writings	k1gInSc1	Writings
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
rozšířeno	rozšířen	k2eAgNnSc4d1	rozšířeno
<g/>
)	)	kIx)	)
–	–	k?	–
eseje	esej	k1gFnSc2	esej
Viditelná	viditelný	k2eAgFnSc1d1	viditelná
temnota	temnota	k1gFnSc1	temnota
<g/>
:	:	kIx,	:
Memoáry	memoáry	k1gInPc1	memoáry
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Darkness	Darkness	k1gInSc1	Darkness
Visible	Visible	k1gFnSc2	Visible
<g/>
:	:	kIx,	:
A	a	k9	a
Memoir	Memoir	k1gInSc1	Memoir
of	of	k?	of
Madness	Madness	k1gInSc1	Madness
<g/>
;	;	kIx,	;
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
–	–	k?	–
autobiografické	autobiografický	k2eAgNnSc4d1	autobiografické
dílo	dílo	k1gNnSc4	dílo
o	o	k7c6	o
Styronových	Styronův	k2eAgInPc6d1	Styronův
problémech	problém	k1gInPc6	problém
s	s	k7c7	s
depresí	deprese	k1gFnSc7	deprese
<g />
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
(	(	kIx(	(
<g/>
A	a	k9	a
Tidewater	Tidewater	k1gInSc1	Tidewater
Morning	Morning	k1gInSc1	Morning
<g/>
:	:	kIx,	:
Three	Three	k1gFnSc1	Three
Tales	Tales	k1gMnSc1	Tales
from	from	k1gMnSc1	from
Youth	Youth	k1gMnSc1	Youth
<g/>
;	;	kIx,	;
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
–	–	k?	–
povídky	povídka	k1gFnSc2	povídka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc1	William
Styron	Styron	k1gInSc4	Styron
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
William	William	k1gInSc1	William
Styron	Styron	k1gInSc1	Styron
William	William	k1gInSc1	William
Styron	Styron	k1gInSc1	Styron
(	(	kIx(	(
<g/>
rozhovor	rozhovor	k1gInSc1	rozhovor
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
"	"	kIx"	"
<g/>
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
video	video	k1gNnSc1	video
on-line	onin	k1gInSc5	on-lin
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
ČT	ČT	kA	ČT
</s>
