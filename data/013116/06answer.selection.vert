<s>
Kautilja	Kautilja	k6eAd1	Kautilja
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
štěstí	štěstí	k1gNnSc1	štěstí
(	(	kIx(	(
<g/>
sukham	sukham	k1gInSc1	sukham
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
sebeovládání	sebeovládání	k1gNnSc2	sebeovládání
(	(	kIx(	(
<g/>
džitátmá	džitátmat	k5eAaPmIp3nS	džitátmat
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
touhami	touha	k1gFnPc7	touha
po	po	k7c6	po
hmotném	hmotný	k2eAgInSc6d1	hmotný
požitku	požitek	k1gInSc6	požitek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
