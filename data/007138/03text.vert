<s>
Akustika	akustika	k1gFnSc1	akustika
je	být	k5eAaImIp3nS	být
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
zvukového	zvukový	k2eAgNnSc2d1	zvukové
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
šířením	šíření	k1gNnSc7	šíření
<g/>
,	,	kIx,	,
vnímáním	vnímání	k1gNnSc7	vnímání
zvuku	zvuk	k1gInSc2	zvuk
sluchem	sluch	k1gInSc7	sluch
a	a	k8xC	a
přenosu	přenos	k1gInSc2	přenos
prostorem	prostor	k1gInSc7	prostor
až	až	k8xS	až
po	po	k7c6	po
vnímání	vnímání	k1gNnSc6	vnímání
lidskými	lidský	k2eAgFnPc7d1	lidská
smysly	smysl	k1gInPc1	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
poddisciplín	poddisciplína	k1gFnPc2	poddisciplína
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hudební	hudební	k2eAgFnSc1d1	hudební
akustika	akustika	k1gFnSc1	akustika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
fyzikální	fyzikální	k2eAgInPc4d1	fyzikální
základy	základ	k1gInPc4	základ
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgFnSc1d1	stavební
akustika	akustika	k1gFnSc1	akustika
zvukové	zvukový	k2eAgInPc4d1	zvukový
jevy	jev	k1gInPc4	jev
a	a	k8xC	a
souvislosti	souvislost	k1gFnPc4	souvislost
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgInSc6d1	uzavřený
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
budovách	budova	k1gFnPc6	budova
a	a	k8xC	a
stavbách	stavba	k1gFnPc6	stavba
<g/>
,	,	kIx,	,
prostorová	prostorový	k2eAgFnSc1d1	prostorová
akustika	akustika	k1gFnSc1	akustika
šíření	šíření	k1gNnSc2	šíření
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
akustika	akustika	k1gFnSc1	akustika
vznikem	vznik	k1gInSc7	vznik
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
hlasovém	hlasový	k2eAgInSc6d1	hlasový
orgánu	orgán	k1gInSc6	orgán
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
vnímáním	vnímání	k1gNnSc7	vnímání
v	v	k7c6	v
uchu	ucho	k1gNnSc6	ucho
<g/>
,	,	kIx,	,
psychoakustika	psychoakustika	k1gFnSc1	psychoakustika
vnímání	vnímání	k1gNnSc2	vnímání
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
atd.	atd.	kA	atd.
Akustika	akustika	k1gFnSc1	akustika
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInPc4d3	nejstarší
obory	obor	k1gInPc4	obor
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Akustika	akustika	k1gFnSc1	akustika
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
oblasti	oblast	k1gFnSc2	oblast
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
AKUSTIKA	akustika	k1gFnSc1	akustika
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řec.	řec.	k?	řec.
akoustikós	akoustikósa	k1gFnPc2	akoustikósa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
slyšení	slyšení	k1gNnSc4	slyšení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
od	od	k7c2	od
akoúo	akoúo	k6eAd1	akoúo
znamenající	znamenající	k2eAgFnSc2d1	znamenající
"	"	kIx"	"
<g/>
slyším	slyšet	k5eAaImIp1nS	slyšet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
akustika	akustika	k1gFnSc1	akustika
studuje	studovat	k5eAaImIp3nS	studovat
způsob	způsob	k1gInSc4	způsob
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
jeho	jeho	k3xOp3gInSc7	jeho
odrazem	odraz	k1gInSc7	odraz
a	a	k8xC	a
pohlcováním	pohlcování	k1gNnSc7	pohlcování
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
materiálech	materiál	k1gInPc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
akustika	akustika	k1gFnSc1	akustika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
zvuky	zvuk	k1gInPc4	zvuk
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
kombinace	kombinace	k1gFnPc4	kombinace
se	s	k7c7	s
zřetelem	zřetel	k1gInSc7	zřetel
na	na	k7c4	na
potřeby	potřeba	k1gFnPc4	potřeba
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
akustika	akustika	k1gFnSc1	akustika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vznikem	vznik	k1gInSc7	vznik
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
hlasivkách	hlasivka	k1gFnPc6	hlasivka
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
vnímáním	vnímání	k1gNnSc7	vnímání
v	v	k7c6	v
uchu	ucho	k1gNnSc6	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
se	se	k3xPyFc4	se
také	také	k9	také
podrobně	podrobně	k6eAd1	podrobně
zabývají	zabývat	k5eAaImIp3nP	zabývat
vlivem	vliv	k1gInSc7	vliv
hluku	hluk	k1gInSc2	hluk
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
fungování	fungování	k1gNnSc4	fungování
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1	stavební
akustika	akustika	k1gFnSc1	akustika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
dobré	dobrý	k2eAgNnSc4d1	dobré
a	a	k8xC	a
nerušené	rušený	k2eNgNnSc4d1	nerušené
podmínky	podmínka	k1gFnPc4	podmínka
poslouchatelnosti	poslouchatelnost	k1gFnSc2	poslouchatelnost
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
řeči	řeč	k1gFnSc2	řeč
v	v	k7c6	v
obytných	obytný	k2eAgFnPc6d1	obytná
místnostech	místnost	k1gFnPc6	místnost
a	a	k8xC	a
sálech	sál	k1gInPc6	sál
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
možnosti	možnost	k1gFnSc3	možnost
eliminace	eliminace	k1gFnSc2	eliminace
nežádoucího	žádoucí	k2eNgNnSc2d1	nežádoucí
šíření	šíření	k1gNnSc2	šíření
hluku	hluk	k1gInSc2	hluk
mimo	mimo	k7c4	mimo
místnosti	místnost	k1gFnPc4	místnost
či	či	k8xC	či
opačně	opačně	k6eAd1	opačně
z	z	k7c2	z
vnějšího	vnější	k2eAgInSc2d1	vnější
prostoru	prostor	k1gInSc2	prostor
do	do	k7c2	do
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Elektroakustika	elektroakustika	k1gFnSc1	elektroakustika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
záznamem	záznam	k1gInSc7	záznam
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
reprodukcí	reprodukce	k1gFnSc7	reprodukce
a	a	k8xC	a
šířením	šíření	k1gNnSc7	šíření
zvuku	zvuk	k1gInSc2	zvuk
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Infrazvuk	infrazvuk	k1gInSc1	infrazvuk
je	být	k5eAaImIp3nS	být
zvuk	zvuk	k1gInSc4	zvuk
o	o	k7c6	o
tak	tak	k6eAd1	tak
nízkém	nízký	k2eAgInSc6d1	nízký
kmitočtu	kmitočet	k1gInSc6	kmitočet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
lidské	lidský	k2eAgNnSc4d1	lidské
ucho	ucho	k1gNnSc4	ucho
není	být	k5eNaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
slyšitelným	slyšitelný	k2eAgInSc7d1	slyšitelný
zvukem	zvuk	k1gInSc7	zvuk
a	a	k8xC	a
infrazvukem	infrazvuk	k1gInSc7	infrazvuk
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
16	[number]	k4	16
a	a	k8xC	a
20	[number]	k4	20
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
mezi	mezi	k7c7	mezi
0,001	[number]	k4	0,001
a	a	k8xC	a
0,2	[number]	k4	0,2
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
sloni	slon	k1gMnPc1	slon
<g/>
,	,	kIx,	,
hroši	hroch	k1gMnPc1	hroch
<g/>
,	,	kIx,	,
nosorožci	nosorožec	k1gMnPc1	nosorožec
<g/>
,	,	kIx,	,
okapi	okapi	k1gFnPc1	okapi
a	a	k8xC	a
aligátoři	aligátor	k1gMnPc1	aligátor
používají	používat	k5eAaImIp3nP	používat
infrazvuk	infrazvuk	k1gInSc4	infrazvuk
k	k	k7c3	k
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
<g/>
.	.	kIx.	.
</s>
<s>
Ultrazvuk	ultrazvuk	k1gInSc1	ultrazvuk
je	být	k5eAaImIp3nS	být
zvuk	zvuk	k1gInSc4	zvuk
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
vyšší	vysoký	k2eAgFnSc7d2	vyšší
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Ultrazvuk	ultrazvuk	k1gInSc1	ultrazvuk
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
třeba	třeba	k6eAd1	třeba
při	při	k7c6	při
lékařském	lékařský	k2eAgNnSc6d1	lékařské
vyšetření	vyšetření	k1gNnSc6	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Ultrazvukové	ultrazvukový	k2eAgFnPc4d1	ultrazvuková
vlny	vlna	k1gFnPc4	vlna
procházejí	procházet	k5eAaImIp3nP	procházet
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
odrážejí	odrážet	k5eAaImIp3nP	odrážet
se	se	k3xPyFc4	se
od	od	k7c2	od
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
akustická	akustický	k2eAgFnSc1d1	akustická
impedance	impedance	k1gFnSc1	impedance
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odražené	odražený	k2eAgFnPc1d1	odražená
vlny	vlna	k1gFnPc1	vlna
lze	lze	k6eAd1	lze
převést	převést	k5eAaPmF	převést
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jasově	jasově	k6eAd1	jasově
modulovaného	modulovaný	k2eAgInSc2d1	modulovaný
obrazu	obraz	k1gInSc2	obraz
na	na	k7c4	na
monitor	monitor	k1gInSc4	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
akustika	akustika	k1gFnSc1	akustika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
akustika	akustika	k1gFnSc1	akustika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
