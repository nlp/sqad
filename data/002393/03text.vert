<s>
Temže	Temže	k1gFnSc1	Temže
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Thames	Thames	k1gInSc1	Thames
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
protékající	protékající	k2eAgFnSc1d1	protékající
jižní	jižní	k2eAgFnSc7d1	jižní
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Londýn	Londýn	k1gInSc4	Londýn
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
346	[number]	k4	346
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
12935	[number]	k4	12935
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
Kemble	Kemble	k1gFnSc2	Kemble
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
Cirencesteru	Cirencester	k1gInSc2	Cirencester
ve	v	k7c6	v
vysočině	vysočina	k1gFnSc6	vysočina
Cotswolds	Cotswoldsa	k1gFnPc2	Cotswoldsa
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
městy	město	k1gNnPc7	město
Lechlade	Lechlad	k1gInSc5	Lechlad
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
latinským	latinský	k2eAgNnSc7d1	latinské
jménem	jméno	k1gNnSc7	jméno
Isis	Isis	k1gFnSc1	Isis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Abingdon	Abingdon	k1gInSc1	Abingdon
<g/>
,	,	kIx,	,
Wallingford	Wallingford	k1gInSc1	Wallingford
<g/>
,	,	kIx,	,
Reading	Reading	k1gInSc1	Reading
<g/>
,	,	kIx,	,
Henley-on-Thames	Henleyn-Thames	k1gInSc1	Henley-on-Thames
<g/>
,	,	kIx,	,
Marlow	Marlow	k1gFnSc1	Marlow
<g/>
,	,	kIx,	,
Maidenhead	Maidenhead	k1gInSc1	Maidenhead
<g/>
,	,	kIx,	,
Windsor	Windsor	k1gInSc1	Windsor
<g/>
,	,	kIx,	,
Eton	Eton	k1gInSc1	Eton
<g/>
,	,	kIx,	,
Staines	Staines	k1gInSc1	Staines
<g/>
,	,	kIx,	,
Weybridge	Weybridge	k1gInSc1	Weybridge
a	a	k8xC	a
poté	poté	k6eAd1	poté
přitéká	přitékat	k5eAaImIp3nS	přitékat
k	k	k7c3	k
okraji	okraj	k1gInSc3	okraj
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hranice	hranice	k1gFnSc2	hranice
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
teče	téct	k5eAaImIp3nS	téct
podél	podél	k6eAd1	podél
Syon	Syon	k1gNnSc1	Syon
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
Hampton	Hampton	k1gInSc1	Hampton
Court	Courta	k1gFnPc2	Courta
<g/>
,	,	kIx,	,
Kingstonu	Kingston	k1gInSc2	Kingston
<g/>
,	,	kIx,	,
Richmondu	Richmond	k1gInSc2	Richmond
a	a	k8xC	a
Kew	Kew	k1gMnSc1	Kew
až	až	k9	až
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
protéká	protékat	k5eAaImIp3nS	protékat
Greenwichem	Greenwich	k1gInSc7	Greenwich
<g/>
,	,	kIx,	,
Dartfordem	Dartford	k1gInSc7	Dartford
a	a	k8xC	a
u	u	k7c2	u
Southend-on-Sea	Southendn-Se	k1gInSc2	Southend-on-Se
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
estuár	estuár	k1gInSc1	estuár
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
200	[number]	k4	200
do	do	k7c2	do
250	[number]	k4	250
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
estuáru	estuár	k1gInSc2	estuár
nedaleko	nedaleko	k7c2	nedaleko
okraje	okraj	k1gInSc2	okraj
města	město	k1gNnSc2	město
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
už	už	k9	už
650	[number]	k4	650
m	m	kA	m
a	a	k8xC	a
poblíž	poblíž	k6eAd1	poblíž
ústí	ústit	k5eAaImIp3nS	ústit
téměř	téměř	k6eAd1	téměř
16	[number]	k4	16
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
hrabství	hrabství	k1gNnSc2	hrabství
pramení	pramenit	k5eAaImIp3nS	pramenit
Temže	Temže	k1gFnSc1	Temže
v	v	k7c6	v
Gloucestershire	Gloucestershir	k1gMnSc5	Gloucestershir
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
hranice	hranice	k1gFnPc4	hranice
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
hrabství	hrabství	k1gNnPc2	hrabství
-	-	kIx~	-
Gloucestershire	Gloucestershir	k1gMnSc5	Gloucestershir
a	a	k8xC	a
Wiltshire	Wiltshir	k1gMnSc5	Wiltshir
<g/>
,	,	kIx,	,
Berkshire	Berkshir	k1gMnSc5	Berkshir
a	a	k8xC	a
Oxfordshire	Oxfordshir	k1gMnSc5	Oxfordshir
<g/>
,	,	kIx,	,
Berkshire	Berkshir	k1gMnSc5	Berkshir
a	a	k8xC	a
Buckinghamshire	Buckinghamshir	k1gMnSc5	Buckinghamshir
<g/>
,	,	kIx,	,
Berkshire	Berkshir	k1gMnSc5	Berkshir
a	a	k8xC	a
Surrey	Surre	k1gMnPc4	Surre
<g/>
,	,	kIx,	,
Surrey	Surre	k1gMnPc4	Surre
a	a	k8xC	a
Middlesex	Middlesex	k1gInSc4	Middlesex
a	a	k8xC	a
Essex	Essex	k1gInSc4	Essex
a	a	k8xC	a
Kent	Kent	k1gInSc4	Kent
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
nepřílivové	přílivový	k2eNgFnSc6d1	přílivový
a	a	k8xC	a
přílivové	přílivový	k2eAgFnSc6d1	přílivová
<g/>
.	.	kIx.	.
</s>
<s>
Nepřílivové	přílivový	k2eNgNnSc1d1	přílivový
povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
asi	asi	k9	asi
9	[number]	k4	9
948	[number]	k4	948
km2	km2	k4	km2
a	a	k8xC	a
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
do	do	k7c2	do
Teddingtonu	Teddington	k1gInSc2	Teddington
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
malých	malý	k2eAgFnPc2d1	malá
a	a	k8xC	a
38	[number]	k4	38
hlavních	hlavní	k2eAgInPc2d1	hlavní
přítoků	přítok	k1gInPc2	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
přítoky	přítok	k1gInPc1	přítok
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
Churn	Churn	k1gInSc4	Churn
<g/>
,	,	kIx,	,
Leach	Leach	k1gInSc4	Leach
<g/>
,	,	kIx,	,
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
Coln	Coln	k1gInSc1	Coln
<g/>
,	,	kIx,	,
Windrush	Windrush	k1gInSc1	Windrush
<g/>
,	,	kIx,	,
Evenlode	Evenlod	k1gMnSc5	Evenlod
<g/>
,	,	kIx,	,
Cherwell	Cherwell	k1gMnSc1	Cherwell
<g/>
,	,	kIx,	,
Ock	Ock	k1gMnSc1	Ock
<g/>
,	,	kIx,	,
Thame	Tham	k1gMnSc5	Tham
<g/>
,	,	kIx,	,
Pang	Pang	k1gMnSc1	Pang
<g/>
,	,	kIx,	,
Kennet	Kennet	k1gMnSc1	Kennet
<g/>
,	,	kIx,	,
Loddon	Loddon	k1gMnSc1	Loddon
<g/>
,	,	kIx,	,
Colne	Coln	k1gMnSc5	Coln
<g/>
,	,	kIx,	,
Wey	Wey	k1gMnSc5	Wey
<g/>
,	,	kIx,	,
Mole	mol	k1gMnSc5	mol
Přílivové	přílivový	k2eAgNnSc1d1	přílivové
povodí	povodí	k1gNnSc1	povodí
začíná	začínat	k5eAaImIp3nS	začínat
asi	asi	k9	asi
90	[number]	k4	90
km	km	kA	km
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
projevuje	projevovat	k5eAaImIp3nS	projevovat
vliv	vliv	k1gInSc4	vliv
přílivu	příliv	k1gInSc2	příliv
ze	z	k7c2	z
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
je	být	k5eAaImIp3nS	být
brakická	brakický	k2eAgFnSc1d1	brakická
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
slaná	slaný	k2eAgFnSc1d1	slaná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
přítoky	přítok	k1gInPc1	přítok
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
Brent	Brent	k?	Brent
<g/>
,	,	kIx,	,
Wandle	Wandle	k1gMnSc1	Wandle
<g/>
,	,	kIx,	,
Effra	Effra	k1gMnSc1	Effra
<g/>
,	,	kIx,	,
Westbourne	Westbourn	k1gInSc5	Westbourn
<g/>
,	,	kIx,	,
Fleet	Fleeta	k1gFnPc2	Fleeta
<g/>
,	,	kIx,	,
Ravensbourne	Ravensbourn	k1gInSc5	Ravensbourn
<g/>
,	,	kIx,	,
Lea	Lea	k1gFnSc1	Lea
<g/>
,	,	kIx,	,
Darent	Darent	k1gInSc1	Darent
<g/>
,	,	kIx,	,
Ingrebourne	Ingrebourn	k1gInSc5	Ingrebourn
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
Temže	Temže	k1gFnSc2	Temže
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
započítá	započítat	k5eAaPmIp3nS	započítat
i	i	k9	i
povodí	povodí	k1gNnSc1	povodí
řeky	řeka	k1gFnSc2	řeka
Medway	Medwaa	k1gFnSc2	Medwaa
<g/>
)	)	kIx)	)
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
12	[number]	k4	12
935	[number]	k4	935
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Řeku	řeka	k1gFnSc4	řeka
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
dešťový	dešťový	k2eAgInSc1d1	dešťový
(	(	kIx(	(
<g/>
pluviální	pluviální	k2eAgInSc1d1	pluviální
<g/>
)	)	kIx)	)
odtokový	odtokový	k2eAgInSc1d1	odtokový
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
až	až	k9	až
k	k	k7c3	k
66	[number]	k4	66
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
britské	britský	k2eAgFnPc1d1	britská
řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Severn	Severn	k1gMnSc1	Severn
a	a	k8xC	a
Tay	Tay	k1gMnSc1	Tay
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
vodnosti	vodnost	k1gFnPc4	vodnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
se	se	k3xPyFc4	se
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
výjimečně	výjimečně	k6eAd1	výjimečně
tuhých	tuhý	k2eAgFnPc6d1	tuhá
zimách	zima	k1gFnPc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
přílivu	příliv	k1gInSc2	příliv
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
6	[number]	k4	6
až	až	k9	až
6,5	[number]	k4	6,5
m	m	kA	m
a	a	k8xC	a
přílivová	přílivový	k2eAgFnSc1d1	přílivová
vlna	vlna	k1gFnSc1	vlna
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
k	k	k7c3	k
Teddingtonu	Teddington	k1gInSc3	Teddington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc1	koryto
přehrazeno	přehrazen	k2eAgNnSc1d1	přehrazeno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
okolí	okolí	k1gNnSc2	okolí
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
estuáru	estuár	k1gInSc2	estuár
od	od	k7c2	od
zatopení	zatopení	k1gNnSc2	zatopení
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
ochranné	ochranný	k2eAgFnPc1d1	ochranná
hráze	hráz	k1gFnPc1	hráz
a	a	k8xC	a
ve	v	k7c6	v
městech	město	k1gNnPc6	město
nábřeží	nábřeží	k1gNnSc2	nábřeží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Temži	Temže	k1gFnSc6	Temže
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
konají	konat	k5eAaImIp3nP	konat
dvě	dva	k4xCgFnPc1	dva
významné	významný	k2eAgFnPc1d1	významná
sportovní	sportovní	k2eAgFnPc1d1	sportovní
akce	akce	k1gFnPc1	akce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc1	závod
osmiveslic	osmiveslice	k1gFnPc2	osmiveslice
mezi	mezi	k7c7	mezi
družstvy	družstvo	k1gNnPc7	družstvo
Oxfordské	oxfordský	k2eAgMnPc4d1	oxfordský
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Cambridgeské	cambridgeský	k2eAgFnSc2d1	Cambridgeská
univerzity	univerzita	k1gFnSc2	univerzita
z	z	k7c2	z
Putney	Putnea	k1gFnSc2	Putnea
do	do	k7c2	do
Mortlake	Mortlak	k1gFnSc2	Mortlak
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
veslařským	veslařský	k2eAgInSc7d1	veslařský
závodem	závod	k1gInSc7	závod
je	být	k5eAaImIp3nS	být
Henleyova	Henleyův	k2eAgFnSc1d1	Henleyova
královská	královský	k2eAgFnSc1d1	královská
regata	regata	k1gFnSc1	regata
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
probíhá	probíhat	k5eAaImIp3nS	probíhat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
v	v	k7c4	v
Henley-on-Thames	Henleyn-Thames	k1gInSc4	Henley-on-Thames
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dostihy	dostih	k1gInPc7	dostih
v	v	k7c6	v
Ascotu	Ascot	k1gInSc6	Ascot
a	a	k8xC	a
tenisovým	tenisový	k2eAgInSc7d1	tenisový
turnajem	turnaj	k1gInSc7	turnaj
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
velkou	velký	k2eAgFnSc7d1	velká
společenskou	společenský	k2eAgFnSc7d1	společenská
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
čluny	člun	k1gInPc1	člun
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
až	až	k9	až
k	k	k7c3	k
Halfpenny	Halfpenna	k1gMnSc2	Halfpenna
Bridge	Bridge	k1gNnSc1	Bridge
u	u	k7c2	u
Lechlade	Lechlad	k1gInSc5	Lechlad
(	(	kIx(	(
<g/>
311	[number]	k4	311
km	km	kA	km
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
plout	plout	k5eAaImF	plout
lodě	loď	k1gFnPc4	loď
o	o	k7c6	o
výtlaku	výtlak	k1gInSc6	výtlak
až	až	k9	až
800	[number]	k4	800
t	t	k?	t
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
Tilbury	Tilbura	k1gFnSc2	Tilbura
námořní	námořní	k2eAgFnSc2d1	námořní
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ústím	ústí	k1gNnSc7	ústí
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
až	až	k9	až
po	po	k7c4	po
Teddington	Teddington	k1gInSc4	Teddington
Lock	Locka	k1gFnPc2	Locka
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
část	část	k1gFnSc1	část
Londýnského	londýnský	k2eAgInSc2d1	londýnský
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
starými	starý	k2eAgInPc7d1	starý
kanály	kanál	k1gInPc7	kanál
s	s	k7c7	s
Bristolským	bristolský	k2eAgInSc7d1	bristolský
zálivem	záliv	k1gInSc7	záliv
<g/>
,	,	kIx,	,
Irským	irský	k2eAgNnSc7d1	irské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
s	s	k7c7	s
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
oblastmi	oblast	k1gFnPc7	oblast
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
břehy	břeh	k1gInPc1	břeh
řeky	řeka	k1gFnSc2	řeka
spojuje	spojovat	k5eAaImIp3nS	spojovat
množství	množství	k1gNnSc1	množství
mostů	most	k1gInPc2	most
a	a	k8xC	a
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
Dartford	Dartford	k1gInSc4	Dartford
Crossing	Crossing	k1gInSc1	Crossing
<g/>
,	,	kIx,	,
Bariéry	bariéra	k1gFnPc1	bariéra
na	na	k7c4	na
Temži	Temže	k1gFnSc4	Temže
<g/>
,	,	kIx,	,
Blackwall	Blackwall	k1gMnSc1	Blackwall
Tunnel	Tunnel	k1gMnSc1	Tunnel
<g/>
,	,	kIx,	,
Rotherhithe	Rotherhithe	k1gFnSc1	Rotherhithe
Tunnel	Tunnel	k1gMnSc1	Tunnel
<g/>
,	,	kIx,	,
Thames	Thames	k1gMnSc1	Thames
Tunnel	Tunnel	k1gMnSc1	Tunnel
<g/>
,	,	kIx,	,
Tower	Tower	k1gMnSc1	Tower
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Bridge	Bridge	k1gFnSc1	Bridge
<g/>
,	,	kIx,	,
Millennium	millennium	k1gNnSc1	millennium
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
Hungerford	Hungerford	k1gInSc1	Hungerford
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
Westminster	Westminster	k1gInSc1	Westminster
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
,	,	kIx,	,
Maidenhead	Maidenhead	k1gInSc4	Maidenhead
Railway	Railwaa	k1gMnSc2	Railwaa
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
,	,	kIx,	,
Marlow	Marlow	k1gMnSc2	Marlow
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Temže	Temže	k1gFnSc1	Temže
hlavní	hlavní	k2eAgFnSc1d1	hlavní
dopravní	dopravní	k2eAgFnSc7d1	dopravní
tepnou	tepna	k1gFnSc7	tepna
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
zboží	zboží	k1gNnSc2	zboží
probíhala	probíhat	k5eAaImAgFnS	probíhat
především	především	k6eAd1	především
mezi	mezi	k7c7	mezi
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
City	city	k1gNnSc6	city
a	a	k8xC	a
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
nazývaném	nazývaný	k2eAgNnSc6d1	nazývané
malá	malý	k2eAgFnSc1d1	malá
doba	doba	k1gFnSc1	doba
ledová	ledový	k2eAgFnSc1d1	ledová
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
zamrzala	zamrzat	k5eAaImAgFnS	zamrzat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1607	[number]	k4	1607
byl	být	k5eAaImAgInS	být
pořádán	pořádat	k5eAaImNgInS	pořádat
první	první	k4xOgInSc1	první
Zimní	zimní	k2eAgInSc1d1	zimní
trh	trh	k1gInSc1	trh
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nechalo	nechat	k5eAaPmAgNnS	nechat
postavit	postavit	k5eAaPmF	postavit
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
stan	stan	k1gInSc4	stan
a	a	k8xC	a
konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
různé	různý	k2eAgFnPc1d1	různá
atrakce	atrakce	k1gFnPc1	atrakce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
kuželkách	kuželka	k1gFnPc6	kuželka
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
teplota	teplota	k1gFnSc1	teplota
začala	začít	k5eAaPmAgFnS	začít
stoupat	stoupat	k5eAaImF	stoupat
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
nikdy	nikdy	k6eAd1	nikdy
nezamrzla	zamrznout	k5eNaPmAgFnS	zamrznout
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k8xC	i
výstavba	výstavba	k1gFnSc1	výstavba
nového	nový	k2eAgInSc2d1	nový
London	London	k1gMnSc1	London
Bridge	Bridge	k1gFnSc4	Bridge
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
méně	málo	k6eAd2	málo
pilířů	pilíř	k1gInPc2	pilíř
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
řeka	řeka	k1gFnSc1	řeka
tekla	téct	k5eAaImAgFnS	téct
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Londýn	Londýn	k1gInSc1	Londýn
stal	stát	k5eAaPmAgInS	stát
centrem	centrum	k1gNnSc7	centrum
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Temže	Temže	k1gFnSc1	Temže
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejrušnějších	rušný	k2eAgFnPc2d3	nejrušnější
říčních	říční	k2eAgFnPc2d1	říční
dopravních	dopravní	k2eAgFnPc2d1	dopravní
tepen	tepna	k1gFnPc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
odehrála	odehrát	k5eAaPmAgFnS	odehrát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
tragédií	tragédie	k1gFnPc2	tragédie
<g/>
,	,	kIx,	,
když	když	k8xS	když
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1878	[number]	k4	1878
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
srážky	srážka	k1gFnSc2	srážka
vyhlídkové	vyhlídkový	k2eAgFnSc2d1	vyhlídková
lodi	loď	k1gFnSc2	loď
Princess	Princessa	k1gFnPc2	Princessa
Alice	Alice	k1gFnSc2	Alice
u	u	k7c2	u
Bywell	Bywella	k1gFnPc2	Bywella
Castle	Castle	k1gFnPc4	Castle
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
640	[number]	k4	640
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nastupující	nastupující	k2eAgFnSc1d1	nastupující
železniční	železniční	k2eAgFnSc1d1	železniční
a	a	k8xC	a
silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
poklesem	pokles	k1gInSc7	pokles
významu	význam	k1gInSc2	význam
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
v	v	k7c6	v
období	období	k1gNnSc6	období
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pokles	pokles	k1gInSc4	pokles
významu	význam	k1gInSc2	význam
Temže	Temže	k1gFnSc2	Temže
jako	jako	k8xS	jako
dopravního	dopravní	k2eAgNnSc2d1	dopravní
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Londýnský	londýnský	k2eAgInSc1d1	londýnský
přístav	přístav	k1gInSc1	přístav
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
níže	nízce	k6eAd2	nízce
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
do	do	k7c2	do
Tilbury	Tilbura	k1gFnSc2	Tilbura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
protipovodňové	protipovodňový	k2eAgNnSc1d1	protipovodňové
zařízení	zařízení	k1gNnSc1	zařízení
-	-	kIx~	-
Bariéry	bariéra	k1gFnPc1	bariéra
na	na	k7c6	na
Temži	Temže	k1gFnSc6	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ochránilo	ochránit	k5eAaPmAgNnS	ochránit
níže	nízce	k6eAd2	nízce
položené	položený	k2eAgFnPc4d1	položená
oblasti	oblast	k1gFnPc4	oblast
Londýna	Londýn	k1gInSc2	Londýn
před	před	k7c7	před
záplavami	záplava	k1gFnPc7	záplava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
vytvořen	vytvořen	k2eAgMnSc1d1	vytvořen
12	[number]	k4	12
<g/>
kilometrový	kilometrový	k2eAgInSc1d1	kilometrový
závlahový	závlahový	k2eAgInSc1d1	závlahový
kanál	kanál	k1gInSc1	kanál
Jubilee	Jubile	k1gFnSc2	Jubile
River	Rivra	k1gFnPc2	Rivra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Т	Т	k?	Т
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Temže	Temže	k1gFnSc2	Temže
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Temže	Temže	k1gFnSc2	Temže
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Temže	Temže	k1gFnSc1	Temže
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
nepřílivového	přílivový	k2eNgNnSc2d1	přílivový
povodí	povodí	k1gNnSc2	povodí
Temže	Temže	k1gFnSc2	Temže
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
článek	článek	k1gInSc4	článek
obsahující	obsahující	k2eAgFnSc4d1	obsahující
mapku	mapka	k1gFnSc4	mapka
povodí	povodí	k1gNnSc2	povodí
Temže	Temže	k1gFnSc2	Temže
</s>
