<s>
Auckland	Auckland	k1gInSc1	Auckland
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
oficiálně	oficiálně	k6eAd1	oficiálně
fungoval	fungovat	k5eAaImAgInS	fungovat
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tento	tento	k3xDgInSc4	tento
status	status	k1gInSc4	status
převzal	převzít	k5eAaPmAgMnS	převzít
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
a	a	k8xC	a
strategickému	strategický	k2eAgInSc3d1	strategický
přístavu	přístav	k1gInSc3	přístav
Wellington	Wellington	k1gInSc4	Wellington
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
aucklandském	aucklandský	k2eAgInSc6d1	aucklandský
přístavu	přístav	k1gInSc6	přístav
potopena	potopen	k2eAgFnSc1d1	potopena
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
loď	loď	k1gFnSc1	loď
organizace	organizace	k1gFnSc2	organizace
Greenpeace	Greenpeace	k1gFnSc1	Greenpeace
Rainbow	Rainbow	k1gMnSc1	Rainbow
Warrior	Warrior	k1gMnSc1	Warrior
<g/>
.	.	kIx.	.
</s>
