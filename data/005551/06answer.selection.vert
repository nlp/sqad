<s>
Opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
endotermická	endotermický	k2eAgFnSc1d1	endotermická
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
teplo	teplo	k6eAd1	teplo
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
reakce	reakce	k1gFnSc1	reakce
mohla	moct	k5eAaImAgFnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
<g/>
.	.	kIx.	.
</s>
