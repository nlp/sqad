<s>
Exotermní	exotermní	k2eAgFnSc1d1	exotermní
(	(	kIx(	(
<g/>
exotermická	exotermický	k2eAgFnSc1d1	exotermická
<g/>
)	)	kIx)	)
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Produkty	produkt	k1gInPc1	produkt
reakce	reakce	k1gFnSc2	reakce
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
chemickou	chemický	k2eAgFnSc4d1	chemická
energii	energie	k1gFnSc4	energie
než	než	k8xS	než
reaktanty	reaktant	k1gMnPc4	reaktant
(	(	kIx(	(
<g/>
látky	látka	k1gFnPc1	látka
do	do	k7c2	do
reakce	reakce	k1gFnSc2	reakce
vstupující	vstupující	k2eAgFnSc2d1	vstupující
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
endotermická	endotermický	k2eAgFnSc1d1	endotermická
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
teplo	teplo	k6eAd1	teplo
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
reakce	reakce	k1gFnSc1	reakce
mohla	moct	k5eAaImAgFnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
exotermická	exotermický	k2eAgFnSc1d1	exotermická
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
hoření	hoření	k1gNnSc4	hoření
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
ději	děj	k1gInSc6	děj
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
teplo	teplo	k1gNnSc1	teplo
a	a	k8xC	a
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
proces	proces	k1gInSc1	proces
(	(	kIx(	(
<g/>
příbuzný	příbuzný	k1gMnSc1	příbuzný
hoření	hoření	k2eAgMnSc1d1	hoření
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
exploze	exploze	k1gFnSc1	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
slučování	slučování	k1gNnSc1	slučování
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
:	:	kIx,	:
2H2	[number]	k4	2H2
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
→	→	k?	→
2H2O	[number]	k4	2H2O
Reagující	reagující	k2eAgFnPc1d1	reagující
látky	látka	k1gFnPc1	látka
předávají	předávat	k5eAaImIp3nP	předávat
teplo	teplo	k1gNnSc4	teplo
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
reakční	reakční	k2eAgNnSc1d1	reakční
teplo	teplo	k1gNnSc1	teplo
tedy	tedy	k9	tedy
má	mít	k5eAaImIp3nS	mít
zápornou	záporný	k2eAgFnSc4d1	záporná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
:	:	kIx,	:
Qr	Qr	k1gFnSc4	Qr
=	=	kIx~	=
-484	-484	k4	-484
kJ	kJ	k?	kJ
<g/>
.	.	kIx.	.
<g/>
mol-	mol-	k?	mol-
<g/>
1	[number]	k4	1
Endotermické	endotermický	k2eAgFnSc2d1	endotermická
reakce	reakce	k1gFnSc2	reakce
Chemická	chemický	k2eAgFnSc1d1	chemická
energie	energie	k1gFnSc1	energie
</s>
