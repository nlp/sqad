<s>
Jahoda	jahoda	k1gFnSc1	jahoda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
lehce	lehko	k6eAd1	lehko
stravitelnou	stravitelný	k2eAgFnSc4d1	stravitelná
vlákninu	vláknina	k1gFnSc4	vláknina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadné	snadný	k2eAgFnPc4d1	snadná
další	další	k2eAgFnPc4d1	další
tepelné	tepelný	k2eAgFnPc4d1	tepelná
úpravy	úprava	k1gFnPc4	úprava
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
marmelád	marmeláda	k1gFnPc2	marmeláda
<g/>
,	,	kIx,	,
či	či	k8xC	či
džemů	džem	k1gInPc2	džem
<g/>
.	.	kIx.	.
</s>
