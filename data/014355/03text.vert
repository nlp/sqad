<s>
Ronaldinho	Ronaldinze	k6eAd1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
nadbytečné	nadbytečný	k2eAgInPc4d1
vnitřní	vnitřní	k2eAgInPc4d1
odkazy	odkaz	k1gInPc4
<g/>
;	;	kIx,
Paříž	Paříž	k1gFnSc1
Saint-Germain	Saint-Germain	k1gInSc1
se	se	k3xPyFc4
nepoužívá	používat	k5eNaImIp3nS
<g/>
,	,	kIx,
česká	český	k2eAgNnPc1d1
média	médium	k1gNnPc1
používají	používat	k5eAaImIp3nP
název	název	k1gInSc4
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
Ronaldinho	Ronaldin	k1gMnSc4
Ronaldinho	Ronaldin	k1gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
Osobní	osobní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
de	de	k?
Assis	Assis	k1gFnSc1
Moreira	Moreir	k1gInSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1980	#num#	k4
(	(	kIx(
<g/>
41	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Porto	porto	k1gNnSc1
Alegre	Alegr	k1gInSc5
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
Výška	výška	k1gFnSc1
</s>
<s>
182	#num#	k4
cm	cm	kA
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
80	#num#	k4
kg	kg	kA
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
útočník	útočník	k1gMnSc1
Mládežnické	mládežnický	k2eAgInPc4d1
kluby	klub	k1gInPc4
</s>
<s>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
</s>
<s>
Grê	Grê	k6eAd1
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
<g/>
20012001	#num#	k4
<g/>
–	–	k?
<g/>
20032003	#num#	k4
<g/>
–	–	k?
<g/>
20082008	#num#	k4
<g/>
–	–	k?
<g/>
20112011	#num#	k4
<g/>
–	–	k?
<g/>
20122012	#num#	k4
<g/>
–	–	k?
<g/>
20142014	#num#	k4
<g/>
–	–	k?
<g/>
20152015	#num#	k4
Grê	Grê	k6eAd1
PSG	PSG	kA
Barcelona	Barcelona	k1gFnSc1
Milán	Milán	k1gInSc1
Flamengo	flamengo	k1gNnSc1
Mineiro	Mineira	k1gFnSc5
Querétaro	Querétara	k1gFnSc5
Fluminense	Fluminensa	k1gFnSc6
<g/>
0	#num#	k4
<g/>
520	#num#	k4
<g/>
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
550	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
(	(	kIx(
<g/>
17	#num#	k4
<g/>
)	)	kIx)
<g/>
1450	#num#	k4
<g/>
(	(	kIx(
<g/>
70	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
760	#num#	k4
<g/>
(	(	kIx(
<g/>
20	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
330	#num#	k4
<g/>
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
480	#num#	k4
<g/>
(	(	kIx(
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
2000	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
700	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
19971998	#num#	k4
<g/>
–	–	k?
<g/>
19991999	#num#	k4
<g/>
–	–	k?
<g/>
20081999	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
Brazílie	Brazílie	k1gFnSc1
U17	U17	k1gFnSc1
Brazílie	Brazílie	k1gFnSc1
U20	U20	k1gFnSc1
Brazílie	Brazílie	k1gFnSc1
(	(	kIx(
<g/>
Olympijská	olympijský	k2eAgFnSc1d1
<g/>
)	)	kIx)
Brazílie	Brazílie	k1gFnSc1
<g/>
130	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
170	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
270	#num#	k4
<g/>
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
<g/>
970	#num#	k4
<g/>
(	(	kIx(
<g/>
33	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
2002	#num#	k4
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Copa	Cop	k2eAgFnSc1d1
América	América	k1gFnSc1
</s>
<s>
1999	#num#	k4
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Fotbal	fotbal	k1gInSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
LOH	LOH	kA
2008	#num#	k4
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Konfederační	konfederační	k2eAgInSc1d1
pohár	pohár	k1gInSc1
FIFA	FIFA	kA
</s>
<s>
2005	#num#	k4
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
Coppa	Coppa	k1gFnSc1
Libertadores	Libertadoresa	k1gFnPc2
</s>
<s>
2013	#num#	k4
</s>
<s>
CA	ca	kA
Mineiro	Mineiro	k1gNnSc1
</s>
<s>
Recopa	Recopa	k1gFnSc1
Sudamericana	Sudamericana	k1gFnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
CA	ca	kA
Mineiro	Mineiro	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
italská	italský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Podpis	podpis	k1gInSc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
<g/>
*	*	kIx~
Starty	start	k1gInPc1
a	a	k8xC
góly	gól	k1gInPc1
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
lize	liga	k1gFnSc6
za	za	k7c4
klub	klub	k1gInSc4
aktuální	aktuální	k2eAgInSc4d1
k	k	k7c3
konec	konec	k1gInSc4
kariéry	kariéra	k1gFnSc2
<g/>
**	**	k?
Starty	start	k1gInPc1
a	a	k8xC
góly	gól	k1gInPc1
za	za	k7c4
reprezentaci	reprezentace	k1gFnSc4
aktuální	aktuální	k2eAgFnSc4d1
k	k	k7c3
konec	konec	k1gInSc4
kariéry	kariéra	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
de	de	k?
Assis	Assis	k1gFnSc1
Moreira	Moreir	k1gInSc2
zkráceně	zkráceně	k6eAd1
jen	jen	k9
Ronaldinho	Ronaldin	k1gMnSc4
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1980	#num#	k4
<g/>
,	,	kIx,
Porto	porto	k1gNnSc1
Alegre	Alegr	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
brazilský	brazilský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
národním	národní	k2eAgInSc7d1
týmem	tým	k1gInSc7
vyhrál	vyhrát	k5eAaPmAgMnS
MS	MS	kA
2002	#num#	k4
a	a	k8xC
CA	ca	kA
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
klubové	klubový	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
jednou	jednou	k9
LM	LM	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pohár	pohár	k1gInSc1
osvoboditelů	osvoboditel	k1gMnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Jihoamerický	jihoamerický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
slavil	slavit	k5eAaImAgMnS
tituly	titul	k1gInPc7
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
a	a	k8xC
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Itálii	Itálie	k1gFnSc6
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS
anketu	anketa	k1gFnSc4
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
(	(	kIx(
<g/>
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
2005	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dvakrát	dvakrát	k6eAd1
byl	být	k5eAaImAgMnS
vyhlášen	vyhlásit	k5eAaPmNgMnS
Fotbalistou	fotbalista	k1gMnSc7
roku	rok	k1gInSc2
FIFA	FIFA	kA
(	(	kIx(
<g/>
2004	#num#	k4
a	a	k8xC
2005	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jednou	jednou	k6eAd1
Fotbalistou	fotbalista	k1gMnSc7
roku	rok	k1gInSc2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
byl	být	k5eAaImAgInS
také	také	k9
jednou	jednou	k6eAd1
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
jihoamerickým	jihoamerický	k2eAgMnSc7d1
fotbalistou	fotbalista	k1gMnSc7
roku	rok	k1gInSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelé	Pelé	k1gNnSc1
ho	on	k3xPp3gInSc2
roku	rok	k1gInSc2
2004	#num#	k4
zařadil	zařadit	k5eAaPmAgInS
mezi	mezi	k7c4
125	#num#	k4
nejlepších	dobrý	k2eAgMnPc2d3
žijících	žijící	k2eAgMnPc2d1
fotbalistů	fotbalista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
,	,	kIx,
během	během	k7c2
výletu	výlet	k1gInSc2
v	v	k7c6
Paraguayi	Paraguay	k1gFnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zadržen	zadržet	k5eAaPmNgMnS
policií	policie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
u	u	k7c2
něj	on	k3xPp3gMnSc2
objevila	objevit	k5eAaPmAgFnS
falešný	falešný	k2eAgInSc4d1
pas	pas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vězení	vězení	k1gNnSc6
oslavil	oslavit	k5eAaPmAgMnS
i	i	k8xC
své	své	k1gNnSc4
40	#num#	k4
<g/>
.	.	kIx.
narozeniny	narozeniny	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klubová	klubový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Grê	Grê	k6eAd1
</s>
<s>
Ke	k	k7c3
zkrácenému	zkrácený	k2eAgNnSc3d1
jménu	jméno	k1gNnSc3
přišel	přijít	k5eAaPmAgMnS
v	v	k7c6
době	doba	k1gFnSc6
mládí	mládí	k1gNnSc2
když	když	k8xS
byl	být	k5eAaImAgInS
mezi	mezi	k7c7
spoluhráči	spoluhráč	k1gMnPc7
obvykle	obvykle	k6eAd1
nejmenší	malý	k2eAgMnPc1d3
<g/>
,	,	kIx,
také	také	k9
i	i	k9
kvůli	kvůli	k7c3
skutečností	skutečnost	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
debutoval	debutovat	k5eAaBmAgMnS
v	v	k7c6
národním	národní	k2eAgInSc6d1
týmu	tým	k1gInSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
přítomen	přítomen	k2eAgInSc4d1
Ronaldo	Ronaldo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
hráčů	hráč	k1gMnPc2
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
nosil	nosit	k5eAaImAgInS
na	na	k7c6
dresu	dres	k1gInSc6
své	svůj	k3xOyFgNnSc4
původní	původní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Ronaldo	Ronaldo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
byl	být	k5eAaImAgMnS
poprvé	poprvé	k6eAd1
povolán	povolat	k5eAaPmNgMnS
do	do	k7c2
mládežnických	mládežnický	k2eAgInPc2d1
výběrů	výběr	k1gInPc2
klubu	klub	k1gInSc2
Grê	Grê	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
1997	#num#	k4
podepsal	podepsat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
profesionální	profesionální	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc2
utkání	utkání	k1gNnPc2
odehrál	odehrát	k5eAaPmAgInS
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1998	#num#	k4
v	v	k7c6
Poháru	pohár	k1gInSc6
osvoboditelů	osvoboditel	k1gMnPc2
proti	proti	k7c3
CR	cr	k0
Vasco	Vasco	k6eAd1
da	da	k?
Gama	gama	k1gNnPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
za	za	k7c7
4	#num#	k4
roky	rok	k1gInPc7
odehrál	odehrát	k5eAaPmAgMnS
125	#num#	k4
utkání	utkání	k1gNnSc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
58	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc4d1
trofej	trofej	k1gFnSc4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
s	s	k7c7
klubem	klub	k1gInSc7
získal	získat	k5eAaPmAgMnS
bylo	být	k5eAaImAgNnS
vítězství	vítězství	k1gNnSc3
ligy	liga	k1gFnSc2
v	v	k7c6
regionu	region	k1gInSc6
Gaúcho	Gaúcha	k1gFnSc5
(	(	kIx(
<g/>
Campionato	Campionat	k2eAgNnSc1d1
Gaúcho	Gaúcha	k1gFnSc5
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
PSG	PSG	kA
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1
klub	klub	k1gInSc1
Paříž	Paříž	k1gFnSc1
Saint-Germain	Saint-Germain	k1gInSc1
FC	FC	kA
oznámil	oznámit	k5eAaPmAgInS
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2001	#num#	k4
jeho	jeho	k3xOp3gFnSc4
koupi	koupě	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vzbudila	vzbudit	k5eAaPmAgFnS
kontroverzi	kontroverze	k1gFnSc4
s	s	k7c7
mateřským	mateřský	k2eAgInSc7d1
klubem	klub	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
k	k	k7c3
převodu	převod	k1gInSc6
nedal	dát	k5eNaPmAgMnS
souhlas	souhlas	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
přestupu	přestup	k1gInSc6
rozhodovaly	rozhodovat	k5eAaImAgInP
jak	jak	k6eAd1
soudy	soud	k1gInPc1
tak	tak	k9
i	i	k9
FIFA	FIFA	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
uložila	uložit	k5eAaPmAgFnS
náhradu	náhrada	k1gFnSc4
brazilského	brazilský	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomuto	tento	k3xDgInSc3
sporu	spor	k1gInSc3
nemohl	moct	k5eNaImAgInS
hrát	hrát	k5eAaImF
<g/>
,	,	kIx,
sama	sám	k3xTgFnSc1
FIFA	FIFA	kA
schválila	schválit	k5eAaPmAgFnS
registraci	registrace	k1gFnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
srpnu	srpen	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Debut	debut	k1gInSc1
měl	mít	k5eAaImAgInS
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2001	#num#	k4
proti	proti	k7c3
Auxerre	Auxerr	k1gInSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc4
branku	branka	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
proti	proti	k7c3
Lyonu	Lyon	k1gInSc3
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Sezonu	sezona	k1gFnSc4
zakončil	zakončit	k5eAaPmAgMnS
devíti	devět	k4xCc7
brankami	branka	k1gFnPc7
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
místem	místo	k1gNnSc7
v	v	k7c6
tabulce	tabulka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
sezoně	sezona	k1gFnSc6
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgMnS
jako	jako	k9
nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
svého	své	k1gNnSc2
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nezabránil	zabránit	k5eNaPmAgMnS
11	#num#	k4
<g/>
.	.	kIx.
místu	místo	k1gNnSc3
v	v	k7c6
ligové	ligový	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dal	dát	k5eAaPmAgMnS
najevo	najevo	k6eAd1
odejít	odejít	k5eAaPmF
s	s	k7c7
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dvou	dva	k4xCgFnPc6
sezónách	sezóna	k1gFnPc6
za	za	k7c7
francouzský	francouzský	k2eAgMnSc1d1
odehrál	odehrát	k5eAaPmAgInS
celkem	celkem	k6eAd1
77	#num#	k4
utkání	utkání	k1gNnSc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
25	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
bylo	být	k5eAaImAgNnS
prohrané	prohraný	k2eAgNnSc1d1
finále	finále	k1gNnSc1
domácího	domácí	k2eAgInSc2d1
poháru	pohár	k1gInSc2
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2003	#num#	k4
ho	on	k3xPp3gInSc4
koupila	koupit	k5eAaPmAgFnS
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
a	a	k8xC
zaplatila	zaplatit	k5eAaPmAgFnS
za	za	k7c2
něj	on	k3xPp3gNnSc2
32	#num#	k4
milionů	milion	k4xCgInPc2
Euro	euro	k1gNnSc4
a	a	k8xC
přetáhla	přetáhnout	k5eAaPmAgFnS
Ronaldinha	Ronaldinha	k1gFnSc1
favorizovanému	favorizovaný	k2eAgInSc3d1
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
prvnímu	první	k4xOgNnSc3
utkání	utkání	k1gNnSc2
nastoupil	nastoupit	k5eAaPmAgInS
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2003	#num#	k4
proti	proti	k7c3
Bilbau	Bilbaum	k1gNnSc3
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
první	první	k4xOgFnSc4
branku	branka	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
proti	proti	k7c3
Seville	Sevilla	k1gFnSc3
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
sezonu	sezona	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
celkem	celkem	k6eAd1
22	#num#	k4
branek	branka	k1gFnPc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
sezoně	sezona	k1gFnSc6
získal	získat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
obhájil	obhájit	k5eAaPmAgInS
ligový	ligový	k2eAgInSc1d1
titul	titul	k1gInSc1
i	i	k9
díky	díky	k7c3
vítězství	vítězství	k1gNnSc3
nad	nad	k7c7
Realem	Real	k1gInSc7
kde	kde	k6eAd1
vstřelil	vstřelit	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
branky	branka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
branka	branka	k1gFnSc1
byla	být	k5eAaImAgFnS
dosažena	dosáhnout	k5eAaPmNgFnS
únikem	únik	k1gInSc7
s	s	k7c7
míčem	míč	k1gInSc7
<g/>
,	,	kIx,
obešel	obejít	k5eAaPmAgMnS
protihráče	protihráč	k1gMnPc4
a	a	k8xC
zasloužil	zasloužit	k5eAaPmAgMnS
si	se	k3xPyFc3
potlesk	potlesk	k1gInSc4
celého	celý	k2eAgInSc2d1
stadionu	stadion	k1gInSc2
Realu	Real	k1gInSc2
při	při	k7c6
opuštění	opuštění	k1gNnSc6
hřiště	hřiště	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Vyhrál	vyhrát	k5eAaPmAgMnS
anketu	anketa	k1gFnSc4
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Sezonu	sezona	k1gFnSc4
zakončil	zakončit	k5eAaPmAgInS
vítězstvím	vítězství	k1gNnSc7
v	v	k7c6
LM	LM	kA
když	když	k8xS
porazil	porazit	k5eAaPmAgInS
Arsenal	Arsenal	k1gFnSc4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
začal	začít	k5eAaPmAgInS
vítěznou	vítězný	k2eAgFnSc7d1
obhajobou	obhajoba	k1gFnSc7
španělským	španělský	k2eAgInSc7d1
superpohárem	superpohár	k1gInSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
prohrál	prohrát	k5eAaPmAgMnS
utkání	utkání	k1gNnSc4
o	o	k7c4
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
sezony	sezona	k1gFnSc2
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
prohrál	prohrát	k5eAaPmAgInS
ve	v	k7c6
finále	finále	k1gNnSc6
nad	nad	k7c7
SC	SC	kA
Internacional	Internacional	k1gFnSc7
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
lize	liga	k1gFnSc6
se	se	k3xPyFc4
obhajoba	obhajoba	k1gFnSc1
nepodařila	podařit	k5eNaPmAgFnS
kvůli	kvůli	k7c3
negativním	negativní	k2eAgFnPc3d1
skóre	skóre	k1gNnPc4
nad	nad	k7c7
Realem	Real	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
sezoně	sezona	k1gFnSc6
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
měl	mít	k5eAaImAgMnS
časté	častý	k2eAgFnPc4d1
zdravotní	zdravotní	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
a	a	k8xC
do	do	k7c2
základní	základní	k2eAgFnSc2d1
sestavy	sestava	k1gFnSc2
se	se	k3xPyFc4
dostával	dostávat	k5eAaImAgInS
sporadicky	sporadicky	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Přišel	přijít	k5eAaPmAgMnS
nový	nový	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Josep	Josep	k1gMnSc1
Guardiola	Guardiola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
trenér	trenér	k1gMnSc1
začal	začít	k5eAaPmAgMnS
s	s	k7c7
obměnou	obměna	k1gFnSc7
kádru	kádr	k1gInSc2
a	a	k8xC
oznámil	oznámit	k5eAaPmAgMnS
Ronaldinhovi	Ronaldinh	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
už	už	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
příští	příští	k2eAgInSc4d1
rok	rok	k1gInSc4
nepočítá	počítat	k5eNaImIp3nS
a	a	k8xC
může	moct	k5eAaImIp3nS
si	se	k3xPyFc3
začít	začít	k5eAaPmF
hledat	hledat	k5eAaImF
nové	nový	k2eAgNnSc4d1
angažmá	angažmá	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
zájem	zájem	k1gInSc1
o	o	k7c4
něj	on	k3xPp3gMnSc4
projevily	projevit	k5eAaPmAgInP
kluby	klub	k1gInPc1
Manchester	Manchester	k1gInSc1
City	city	k1gNnSc1
a	a	k8xC
AC	AC	kA
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celkem	celkem	k6eAd1
za	za	k7c4
Barcelonu	Barcelona	k1gFnSc4
odehrál	odehrát	k5eAaPmAgMnS
207	#num#	k4
utkání	utkání	k1gNnSc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
94	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgInS
s	s	k7c7
ní	on	k3xPp3gFnSc7
celkem	celkem	k6eAd1
5	#num#	k4
trofejí	trofej	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Milán	Milán	k1gInSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
nabídku	nabídka	k1gFnSc4
klubu	klub	k1gInSc2
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
a	a	k8xC
raději	rád	k6eAd2
podepsal	podepsat	k5eAaPmAgInS
italským	italský	k2eAgInSc7d1
klubem	klub	k1gInSc7
AC	AC	kA
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Přestupová	přestupový	k2eAgFnSc1d1
částka	částka	k1gFnSc1
byla	být	k5eAaImAgFnS
22	#num#	k4
milionu	milion	k4xCgInSc2
Euro	euro	k1gNnSc1
plus	plus	k1gNnSc2
bonus	bonus	k1gInSc4
1,05	1,05	k4
milionu	milion	k4xCgInSc2
Euro	euro	k1gNnSc4
za	za	k7c4
každou	každý	k3xTgFnSc4
odehranou	odehraný	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
prvnímu	první	k4xOgInSc3
utkání	utkání	k1gNnSc2
nastoupil	nastoupit	k5eAaPmAgInS
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
proti	proti	k7c3
Bologni	Bologna	k1gFnSc3
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
vítězném	vítězný	k2eAgNnSc6d1
derby	derby	k1gNnSc6
konané	konaný	k2eAgFnSc2d1
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
nad	nad	k7c7
Interem	Intero	k1gNnSc7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
vstřelil	vstřelit	k5eAaPmAgMnS
první	první	k4xOgFnSc4
branku	branka	k1gFnSc4
za	za	k7c4
Rossoneri	Rossonere	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Jenže	jenže	k8xC
během	během	k7c2
sezony	sezona	k1gFnSc2
se	se	k3xPyFc4
potýkal	potýkat	k5eAaImAgMnS
s	s	k7c7
špatnou	špatný	k2eAgFnSc7d1
kondicí	kondice	k1gFnSc7
a	a	k8xC
do	do	k7c2
zápasů	zápas	k1gInPc2
nastupoval	nastupovat	k5eAaImAgMnS
jako	jako	k9
střídající	střídající	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věci	věc	k1gFnSc2
došli	dojít	k5eAaPmAgMnP
tak	tak	k6eAd1
daleko	daleko	k6eAd1
že	že	k8xS
přemýšlel	přemýšlet	k5eAaImAgMnS
o	o	k7c6
přestupu	přestup	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Sezonu	sezona	k1gFnSc4
dokončil	dokončit	k5eAaPmAgMnS
s	s	k7c7
10	#num#	k4
brankami	branka	k1gFnPc7
a	a	k8xC
bez	bez	k7c2
trofeje	trofej	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
sezona	sezona	k1gFnSc1
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
byla	být	k5eAaImAgFnS
lepší	dobrý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
trenér	trenér	k1gMnSc1
Leonardo	Leonardo	k1gMnSc1
jej	on	k3xPp3gNnSc4
z	z	k7c2
útočícího	útočící	k2eAgMnSc2d1
záložníka	záložník	k1gMnSc2
dal	dát	k5eAaPmAgMnS
na	na	k7c4
levou	levý	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
v	v	k7c6
útočné	útočný	k2eAgFnSc6d1
formaci	formace	k1gFnSc6
4	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
dařilo	dařit	k5eAaImAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
byl	být	k5eAaImAgMnS
podle	podle	k7c2
magazínu	magazín	k1gInSc2
World	World	k1gMnSc1
Soccer	Soccer	k1gMnSc1
vyhlášen	vyhlásit	k5eAaPmNgMnS
fotbalistou	fotbalista	k1gMnSc7
desetiletí	desetiletí	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Sezonu	sezona	k1gFnSc4
v	v	k7c6
lize	liga	k1gFnSc6
dokončil	dokončit	k5eAaPmAgInS
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
tabulce	tabulka	k1gFnSc6
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
klubu	klub	k1gInSc2
s	s	k7c7
15	#num#	k4
brankami	branka	k1gFnPc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k8xC
když	když	k8xS
byli	být	k5eAaImAgMnP
informace	informace	k1gFnPc4
o	o	k7c6
odchodu	odchod	k1gInSc6
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
sezonu	sezona	k1gFnSc4
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
dresu	dres	k1gInSc6
Rossoneri	Rossoneri	k1gNnSc2
s	s	k7c7
novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Allegrim	Allegrima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
sezóny	sezóna	k1gFnSc2
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
útoku	útok	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
přibyli	přibýt	k5eAaPmAgMnP
dva	dva	k4xCgMnPc1
noví	nový	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
<g/>
:	:	kIx,
Ibrahimović	Ibrahimović	k1gMnSc1
a	a	k8xC
Robinho	Robin	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
postupem	postupem	k7c2
času	čas	k1gInSc2
už	už	k6eAd1
moc	moc	k6eAd1
nehrál	hrát	k5eNaImAgMnS
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
raději	rád	k6eAd2
v	v	k7c6
lednu	leden	k1gInSc6
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
brazilského	brazilský	k2eAgInSc2d1
klubu	klub	k1gInSc2
CR	cr	k0
Flamengo	flamengo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
I	i	k9
když	když	k8xS
opustil	opustit	k5eAaPmAgInS
klub	klub	k1gInSc1
v	v	k7c6
polovině	polovina	k1gFnSc6
sezóny	sezóna	k1gFnSc2
<g/>
,	,	kIx,
stále	stále	k6eAd1
měl	mít	k5eAaImAgInS
nárok	nárok	k1gInSc1
na	na	k7c4
medaili	medaile	k1gFnSc4
pro	pro	k7c4
vítěze	vítěz	k1gMnSc4
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
v	v	k7c6
sezoně	sezona	k1gFnSc6
AC	AC	kA
Milán	Milán	k1gInSc1
získal	získat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
za	za	k7c4
Rossoneri	Rossonere	k1gFnSc4
odehrál	odehrát	k5eAaPmAgMnS
95	#num#	k4
utkání	utkání	k1gNnSc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
26	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Flamengo	flamengo	k1gNnSc1
</s>
<s>
Do	do	k7c2
Flamenga	flamengo	k1gNnSc2
odešel	odejít	k5eAaPmAgInS
za	za	k7c4
3	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
<g/>
,	,	kIx,
podepsal	podepsat	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
do	do	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
a	a	k8xC
při	při	k7c6
přivítání	přivítání	k1gNnSc6
ho	on	k3xPp3gMnSc4
na	na	k7c6
stadionu	stadion	k1gInSc6
vítalo	vítat	k5eAaImAgNnS
více	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
000	#num#	k4
fanoušků	fanoušek	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgNnPc4
utkání	utkání	k1gNnPc4
odehrál	odehrát	k5eAaPmAgInS
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
proti	proti	k7c3
Nova	novum	k1gNnSc2
Iguaçu	Iguaçus	k1gInSc2
FC	FC	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
příštím	příští	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
vstřelil	vstřelit	k5eAaPmAgMnS
první	první	k4xOgFnSc4
branku	branka	k1gFnSc4
proti	proti	k7c3
Boavista	Boavist	k1gMnSc2
SC	SC	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
vše	všechen	k3xTgNnSc1
při	při	k7c6
šampionátu	šampionát	k1gInSc6
Carioca	carioca	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
i	i	k9
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
žaloval	žalovat	k5eAaImAgInS
klub	klub	k1gInSc1
za	za	k7c4
nezaplacení	nezaplacení	k1gNnSc4
výplaty	výplata	k1gFnSc2
po	po	k7c4
dobu	doba	k1gFnSc4
čtyř	čtyři	k4xCgInPc2
měsíců	měsíc	k1gInPc2
a	a	k8xC
zrušil	zrušit	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
odehrál	odehrát	k5eAaPmAgMnS
72	#num#	k4
utkání	utkání	k1gNnSc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
28	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mineiro	Mineiro	k6eAd1
</s>
<s>
Po	po	k7c6
výpovědi	výpověď	k1gFnSc6
již	již	k6eAd1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
podepsal	podepsat	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
klubem	klub	k1gInSc7
CA	ca	kA
Mineiro	Mineiro	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
utkání	utkání	k1gNnPc2
odehrává	odehrávat	k5eAaImIp3nS
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
proti	proti	k7c3
Palmeiras	Palmeiras	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
branku	branka	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
proti	proti	k7c3
Náuticu	Náuticum	k1gNnSc3
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezonu	sezona	k1gFnSc4
ukončil	ukončit	k5eAaPmAgMnS
2	#num#	k4
<g/>
.	.	kIx.
místem	místo	k1gNnSc7
v	v	k7c6
tabulce	tabulka	k1gFnSc6
a	a	k8xC
přímý	přímý	k2eAgInSc4d1
postup	postup	k1gInSc4
do	do	k7c2
Poháru	pohár	k1gInSc2
osvoboditelů	osvoboditel	k1gMnPc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
sezony	sezona	k1gFnSc2
vyhrál	vyhrát	k5eAaPmAgMnS
anketu	anketa	k1gFnSc4
o	o	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
klubem	klub	k1gInSc7
podepsal	podepsat	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
další	další	k2eAgInSc4d1
ročník	ročník	k1gInSc4
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrál	vyhrát	k5eAaPmAgMnS
šampionát	šampionát	k1gInSc4
Mineiro	Mineiro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
Pohár	pohár	k1gInSc4
osvoboditelů	osvoboditel	k1gMnPc2
díky	díky	k7c3
penaltám	penalta	k1gFnPc3
při	při	k7c6
nerozhodném	rozhodný	k2eNgInSc6d1
zápase	zápas	k1gInSc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
nad	nad	k7c7
paraguayským	paraguayský	k2eAgInSc7d1
klubem	klub	k1gInSc7
Club	club	k1gInSc1
Olimpia	Olimpia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
soutěže	soutěž	k1gFnSc2
vstřelil	vstřelit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
branky	branka	k1gFnPc4
a	a	k8xC
na	na	k7c4
osm	osm	k4xCc4
je	být	k5eAaImIp3nS
nahrál	nahrát	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mu	on	k3xPp3gMnSc3
vyneslo	vynést	k5eAaPmAgNnS
první	první	k4xOgNnSc1
místo	místo	k1gNnSc1
v	v	k7c6
žebříčku	žebříček	k1gInSc6
nahrávačů	nahrávač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
vítězstvím	vítězství	k1gNnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
kdy	kdy	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
během	během	k7c2
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
MS	MS	kA
<g/>
,	,	kIx,
Jihoamerický	jihoamerický	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
,	,	kIx,
LM	LM	kA
a	a	k8xC
Pohár	pohár	k1gInSc1
osvoboditelů	osvoboditel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
září	září	k1gNnSc2
2013	#num#	k4
se	se	k3xPyFc4
zranil	zranit	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
až	až	k9
na	na	k7c4
turnaj	turnaj	k1gInSc4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
oslavil	oslavit	k5eAaPmAgInS
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
vstřeleným	vstřelený	k2eAgFnPc3d1
brankám	branka	k1gFnPc3
na	na	k7c6
turnaji	turnaj	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
skóroval	skórovat	k5eAaBmAgMnS
se	s	k7c7
dvěma	dva	k4xCgInPc7
různými	různý	k2eAgInPc7d1
týmy	tým	k1gInPc7
(	(	kIx(
<g/>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
a	a	k8xC
CA	ca	kA
Mineiro	Mineiro	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
byl	být	k5eAaImAgMnS
vyhlášen	vyhlásit	k5eAaPmNgMnS
nejlepším	dobrý	k2eAgMnSc7d3
fotbalistou	fotbalista	k1gMnSc7
roku	rok	k1gInSc2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Opět	opět	k6eAd1
prodloužil	prodloužit	k5eAaPmAgMnS
o	o	k7c4
rok	rok	k1gInSc4
s	s	k7c7
klubem	klub	k1gInSc7
smlouvu	smlouva	k1gFnSc4
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
Jihoamerický	jihoamerický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
nad	nad	k7c7
klubem	klub	k1gInSc7
Lanús	Lanús	k1gInSc1
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
po	po	k7c6
prodl	prodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
poslední	poslední	k2eAgNnSc1d1
utkání	utkání	k1gNnSc1
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
odehrál	odehrát	k5eAaPmAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
pak	pak	k6eAd1
s	s	k7c7
klubem	klub	k1gInSc7
rozvázal	rozvázat	k5eAaPmAgInS
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
za	za	k7c4
Mineiro	Mineiro	k1gNnSc4
odehrál	odehrát	k5eAaPmAgMnS
85	#num#	k4
utkání	utkání	k1gNnSc2
ve	v	k7c6
kterých	který	k3yRgInPc2,k3yIgInPc2,k3yQgInPc2
vstřelil	vstřelit	k5eAaPmAgMnS
27	#num#	k4
branek	branka	k1gFnPc2
a	a	k8xC
získal	získat	k5eAaPmAgMnS
tři	tři	k4xCgFnPc4
trofeje	trofej	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Querétaro	Querétara	k1gFnSc5
</s>
<s>
Dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2014	#num#	k4
byl	být	k5eAaImAgInS
oznámen	oznámen	k2eAgInSc1d1
podpis	podpis	k1gInSc1
s	s	k7c7
mexickým	mexický	k2eAgInSc7d1
klubem	klub	k1gInSc7
Querétaro	Querétara	k1gFnSc5
FC	FC	kA
na	na	k7c4
dvě	dva	k4xCgFnPc4
sezony	sezona	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
svém	svůj	k3xOyFgNnSc6
prvním	první	k4xOgNnSc6
utkání	utkání	k1gNnSc6
neproměňuje	proměňovat	k5eNaImIp3nS
penaltu	penalta	k1gFnSc4
a	a	k8xC
prohrává	prohrávat	k5eAaImIp3nS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
branku	branka	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
sezony	sezona	k1gFnSc2
moc	moc	k6eAd1
nepřesvědčil	přesvědčit	k5eNaPmAgInS
svými	svůj	k3xOyFgInPc7
výkony	výkon	k1gInPc7
majitele	majitel	k1gMnSc4
klubu	klub	k1gInSc2
a	a	k8xC
zejména	zejména	k9
fanoušky	fanoušek	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
mu	on	k3xPp3gMnSc3
vyčítali	vyčítat	k5eAaImAgMnP
nedostatek	nedostatek	k1gInSc4
profesionálního	profesionální	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
(	(	kIx(
<g/>
často	často	k6eAd1
vynechával	vynechávat	k5eAaImAgMnS
tréninky	trénink	k1gInPc4
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
vydržel	vydržet	k5eAaPmAgMnS
jednu	jeden	k4xCgFnSc4
sezonu	sezona	k1gFnSc4
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
ukončení	ukončení	k1gNnSc1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fluminense	Fluminense	k6eAd1
</s>
<s>
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
oznámil	oznámit	k5eAaPmAgMnS
návrat	návrat	k1gInSc4
do	do	k7c2
Brazílie	Brazílie	k1gFnSc2
a	a	k8xC
podepsali	podepsat	k5eAaPmAgMnP
smlouvu	smlouva	k1gFnSc4
na	na	k7c4
18	#num#	k4
měsíců	měsíc	k1gInPc2
s	s	k7c7
klubem	klub	k1gInSc7
Fluminense	Fluminense	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
po	po	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
dohodě	dohoda	k1gFnSc6
ukončil	ukončit	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
svého	svůj	k3xOyFgNnSc2
dvouměsíčního	dvouměsíční	k2eAgNnSc2d1
působení	působení	k1gNnSc2
v	v	k7c6
klubu	klub	k1gInSc6
absolvoval	absolvovat	k5eAaPmAgMnS
devět	devět	k4xCc1
utkání	utkání	k1gNnPc2
<g/>
,	,	kIx,
nezapůsobil	zapůsobit	k5eNaPmAgMnS
a	a	k8xC
byl	být	k5eAaImAgMnS
fanoušky	fanoušek	k1gMnPc4
silně	silně	k6eAd1
kritizován	kritizovat	k5eAaImNgMnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
smlouvy	smlouva	k1gFnSc2
už	už	k6eAd1
hraje	hrát	k5eAaImIp3nS
jen	jen	k9
exhibiční	exhibiční	k2eAgNnSc1d1
utkání	utkání	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
konec	konec	k1gInSc1
kariéry	kariéra	k1gFnSc2
oznámil	oznámit	k5eAaPmAgInS
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
První	první	k4xOgFnPc1
zkušenosti	zkušenost	k1gFnPc1
z	z	k7c2
reprezentací	reprezentace	k1gFnPc2
měl	mít	k5eAaImAgInS
již	již	k6eAd1
na	na	k7c6
MS	MS	kA
U17	U17	k1gFnSc1
1997	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
slavil	slavit	k5eAaImAgInS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
a	a	k8xC
vešel	vejít	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
All	All	k1gMnPc2
stars	stars	k6eAd1
týmu	tým	k1gInSc2
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
MS	MS	kA
U20	U20	k1gFnSc1
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
z	z	k7c2
reprezentací	reprezentace	k1gFnPc2
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
utkání	utkání	k1gNnSc1
za	za	k7c4
seniorskou	seniorský	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
odehrál	odehrát	k5eAaPmAgMnS
proti	proti	k7c3
Lotyšsku	Lotyšsko	k1gNnSc3
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1999	#num#	k4
<g/>
,	,	kIx,
zaznamenal	zaznamenat	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
nahrávky	nahrávka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc4
branku	branka	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
proti	proti	k7c3
Venezuely	Venezuela	k1gFnSc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
turnaji	turnaj	k1gInSc6
vítězném	vítězný	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
Copa	Copa	k1gMnSc1
América	América	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
na	na	k7c4
Konfederační	konfederační	k2eAgInSc4d1
pohár	pohár	k1gInSc4
FIFA	FIFA	kA
1999	#num#	k4
<g/>
|	|	kIx~
<g/>
Konfederačním	konfederační	k2eAgInSc6d1
poháru	pohár	k1gInSc6
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
také	také	k9
vyhrál	vyhrát	k5eAaPmAgMnS
a	a	k8xC
navíc	navíc	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
i	i	k8xC
hráčem	hráč	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Následně	následně	k6eAd1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c6
vítězném	vítězný	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
na	na	k7c4
MS	MS	kA
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavně	hlavně	k6eAd1
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
proti	proti	k7c3
Anglii	Anglie	k1gFnSc3
byl	být	k5eAaImAgInS
fantastický	fantastický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstřelil	vstřelit	k5eAaPmAgMnS
krásnou	krásný	k2eAgFnSc4d1
branku	branka	k1gFnSc4
35	#num#	k4
metrové	metrový	k2eAgFnSc2d1
dálky	dálka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Krátce	krátce	k6eAd1
po	po	k7c6
vstřelené	vstřelený	k2eAgFnSc6d1
brance	branka	k1gFnSc6
dostal	dostat	k5eAaPmAgMnS
za	za	k7c4
faul	faul	k1gInSc4
červenou	červený	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
členem	člen	k1gMnSc7
All	All	k1gFnPc2
stars	stars	k1gInSc1
týmu	tým	k1gInSc2
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
dalšího	další	k2eAgMnSc2d1
MS	MS	kA
2006	#num#	k4
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgInS
ještě	ještě	k9
na	na	k7c6
dvou	dva	k4xCgInPc6
Konfederačních	konfederační	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
2003	#num#	k4
a	a	k8xC
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
MS	MS	kA
2006	#num#	k4
neobhájil	obhájit	k5eNaPmAgInS
a	a	k8xC
skončil	skončit	k5eAaPmAgInS
již	již	k6eAd1
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
velkým	velký	k2eAgInSc7d1
turnajem	turnaj	k1gInSc7
byli	být	k5eAaImAgMnP
bronzové	bronzový	k2eAgNnSc4d1
OH	OH	kA
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
odehrál	odehrát	k5eAaPmAgMnS
s	s	k7c7
kapitánskou	kapitánský	k2eAgFnSc7d1
páskou	páska	k1gFnSc7
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
proti	proti	k7c3
Chile	Chile	k1gNnSc3
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Odehrál	odehrát	k5eAaPmAgMnS
celkem	celek	k1gInSc7
97	#num#	k4
utkání	utkání	k1gNnSc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
33	#num#	k4
branek	branka	k1gFnPc2
za	za	k7c4
Brazílii	Brazílie	k1gFnSc4
a	a	k8xC
získal	získat	k5eAaPmAgMnS
s	s	k7c7
ní	on	k3xPp3gFnSc7
tři	tři	k4xCgFnPc4
velké	velká	k1gFnPc4
vítězství	vítězství	k1gNnPc2
(	(	kIx(
<g/>
Copa	Cop	k2eAgFnSc1d1
América	América	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2002	#num#	k4
a	a	k8xC
Konfederační	konfederační	k2eAgInSc4d1
pohár	pohár	k1gInSc4
FIFA	FIFA	kA
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přestupy	přestup	k1gInPc1
</s>
<s>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
z	z	k7c2
Grê	Grê	k1gNnSc4
FBPA	FBPA	kA
do	do	k7c2
Paříž	Paříž	k1gFnSc1
St.	st.	kA
<g/>
-Germain	-Germain	k1gInSc4
FC	FC	kA
za	za	k7c4
5	#num#	k4
000	#num#	k4
000	#num#	k4
Euro	euro	k1gNnSc4
</s>
<s>
z	z	k7c2
Paříž	Paříž	k1gFnSc1
St.	st.	kA
<g/>
-Germain	-Germain	k1gInSc4
FC	FC	kA
do	do	k7c2
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
za	za	k7c4
32	#num#	k4
250	#num#	k4
000	#num#	k4
Euro	euro	k1gNnSc4
</s>
<s>
z	z	k7c2
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
do	do	k7c2
AC	AC	kA
Milán	Milán	k1gInSc1
za	za	k7c4
24	#num#	k4
150	#num#	k4
000	#num#	k4
Euro	euro	k1gNnSc4
</s>
<s>
z	z	k7c2
AC	AC	kA
Milán	Milán	k1gInSc1
do	do	k7c2
CR	cr	k0
Flamengo	flamengo	k1gNnSc1
za	za	k7c4
3	#num#	k4
000	#num#	k4
000	#num#	k4
Euro	euro	k1gNnSc4
</s>
<s>
z	z	k7c2
CR	cr	k0
Flamengo	flamengo	k1gNnSc4
do	do	k7c2
CA	ca	kA
Mineiro	Mineiro	k1gNnSc1
zadarmo	zadarmo	k6eAd1
</s>
<s>
z	z	k7c2
CA	ca	kA
Mineiro	Mineiro	k1gNnSc1
do	do	k7c2
Querétaro	Querétara	k1gFnSc5
FC	FC	kA
zadarmo	zadarmo	k6eAd1
</s>
<s>
z	z	k7c2
Querétaro	Querétara	k1gFnSc5
FC	FC	kA
do	do	k7c2
Fluminense	Fluminense	k1gFnSc2
FC	FC	kA
zadarmo	zadarmo	k6eAd1
</s>
<s>
Hráčská	hráčský	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Ligové	ligový	k2eAgInPc1d1
poháry	pohár	k1gInPc1
</s>
<s>
Kontinentální	kontinentální	k2eAgInPc4d1
poháry	pohár	k1gInPc4
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
SoutěžZápasyGólySoutěžZápasyGólySoutěžZápasyGólyZápasyGóly	SoutěžZápasyGólySoutěžZápasyGólySoutěžZápasyGólyZápasyGóla	k1gFnPc1
</s>
<s>
1998	#num#	k4
</s>
<s>
Grê	Grê	k1gMnSc1
FBPA	FBPA	kA
</s>
<s>
CG	cg	kA
<g/>
+	+	kIx~
<g/>
Série	série	k1gFnSc1
A	a	k9
<g/>
7	#num#	k4
<g/>
+	+	kIx~
<g/>
142	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
BP	BP	kA
<g/>
20	#num#	k4
<g/>
PO	Po	kA
<g/>
+	+	kIx~
<g/>
JAP	JAP	kA
<g/>
10	#num#	k4
<g/>
+	+	kIx~
<g/>
51	#num#	k4
<g/>
+	+	kIx~
<g/>
2386	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
CG	cg	kA
<g/>
+	+	kIx~
<g/>
Série	série	k1gFnSc1
A	a	k9
<g/>
17	#num#	k4
<g/>
+	+	kIx~
<g/>
1715	#num#	k4
<g/>
+	+	kIx~
<g/>
4	#num#	k4
<g/>
BP	BP	kA
<g/>
30	#num#	k4
<g/>
JAP	JAP	kA
<g/>
424121	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
CG	cg	kA
<g/>
+	+	kIx~
<g/>
Série	série	k1gFnSc1
A	a	k9
<g/>
13	#num#	k4
<g/>
+	+	kIx~
<g/>
2111	#num#	k4
<g/>
+	+	kIx~
<g/>
14	#num#	k4
<g/>
BP	BP	kA
<g/>
33	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3728	#num#	k4
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Paříž	Paříž	k1gFnSc1
St.	st.	kA
<g/>
-Germain	-Germaina	k1gFnPc2
FC	FC	kA
</s>
<s>
División	División	k1gInSc1
1289	#num#	k4
<g/>
FP	FP	kA
<g/>
+	+	kIx~
<g/>
FLP	FLP	kA
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
40	#num#	k4
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
UEFA	UEFA	kA
<g/>
624013	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Ligue	Ligue	k1gFnSc1
1278	#num#	k4
<g/>
FP	FP	kA
<g/>
+	+	kIx~
<g/>
FLP	FLP	kA
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
13	#num#	k4
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
UEFA	UEFA	kA
<g/>
413712	#num#	k4
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
<g/>
3215	#num#	k4
<g/>
ŠP	ŠP	kA
<g/>
63	#num#	k4
<g/>
UEFA	UEFA	kA
<g/>
444522	#num#	k4
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
<g/>
359	#num#	k4
<g/>
ŠP	ŠP	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
LM	LM	kA
<g/>
744213	#num#	k4
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
<g/>
2917	#num#	k4
<g/>
ŠP	ŠP	kA
<g/>
+	+	kIx~
<g/>
ŠS	ŠS	kA
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
21	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
LM	LM	kA
<g/>
1274526	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
<g/>
3221	#num#	k4
<g/>
ŠP	ŠP	kA
<g/>
+	+	kIx~
<g/>
ŠS	ŠS	kA
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
20	#num#	k4
<g/>
LM	LM	kA
<g/>
+	+	kIx~
<g/>
ES	ES	kA
<g/>
+	+	kIx~
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
<g/>
8	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
22	#num#	k4
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
14924	#num#	k4
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
<g/>
178	#num#	k4
<g/>
ŠP	ŠP	kA
<g/>
10	#num#	k4
<g/>
LM	LM	kA
<g/>
81269	#num#	k4
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
AC	AC	kA
Milán	Milán	k1gInSc1
</s>
<s>
Serie	serie	k1gFnSc1
A298IP10UEFA623610	A298IP10UEFA623610	k1gFnSc2
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Serie	serie	k1gFnSc1
A3612IP00LM734315	A3612IP00LM734315	k1gFnSc2
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Serie	serie	k1gFnSc1
A110IP00LM51161	A110IP00LM51161	k1gFnSc2
</s>
<s>
2011	#num#	k4
</s>
<s>
CR	cr	k0
Flamengo	flamengo	k1gNnSc1
</s>
<s>
CC	CC	kA
<g/>
+	+	kIx~
<g/>
Série	série	k1gFnSc1
A	a	k9
<g/>
13	#num#	k4
<g/>
+	+	kIx~
<g/>
314	#num#	k4
<g/>
+	+	kIx~
<g/>
14	#num#	k4
<g/>
BP	BP	kA
<g/>
51	#num#	k4
<g/>
JAP	JAP	kA
<g/>
325221	#num#	k4
</s>
<s>
led	led	k1gInSc1
<g/>
.	.	kIx.
<g/>
-kvě	-kvě	k1gMnSc2
<g/>
.	.	kIx.
2012	#num#	k4
</s>
<s>
CC	CC	kA
<g/>
+	+	kIx~
<g/>
Série	série	k1gFnSc1
A	a	k9
<g/>
10	#num#	k4
<g/>
+	+	kIx~
<g/>
24	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
PO	Po	kA
<g/>
82207	#num#	k4
</s>
<s>
čer	čer	k?
<g/>
.	.	kIx.
<g/>
-pro	-pro	k1gMnSc1
<g/>
.	.	kIx.
2012	#num#	k4
</s>
<s>
CA	ca	kA
Mineiro	Mineiro	k1gNnSc1
</s>
<s>
Série	série	k1gFnSc1
A329-00-00329	A329-00-00329	k1gFnSc2
</s>
<s>
2013	#num#	k4
</s>
<s>
CM	cm	kA
<g/>
+	+	kIx~
<g/>
Série	série	k1gFnSc1
A	a	k9
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
144	#num#	k4
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
BP	BP	kA
<g/>
20	#num#	k4
<g/>
PO	Po	kA
<g/>
+	+	kIx~
<g/>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
<g/>
14	#num#	k4
<g/>
+	+	kIx~
<g/>
24	#num#	k4
<g/>
+	+	kIx~
<g/>
23817	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
CM	cm	kA
<g/>
+	+	kIx~
<g/>
Série	série	k1gFnSc1
A	a	k9
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
20	#num#	k4
<g/>
BP	BP	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
PO	Po	kA
<g/>
+	+	kIx~
<g/>
JAS	jas	k1gInSc1
<g/>
7	#num#	k4
<g/>
+	+	kIx~
<g/>
11	#num#	k4
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
151	#num#	k4
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Querétaro	Querétara	k1gFnSc5
FC	FC	kA
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
<g/>
258	#num#	k4
<g/>
MP	MP	kA
<g/>
40	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
298	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
Fluminense	Fluminense	k1gFnSc1
FC	FC	kA
</s>
<s>
CC	CC	kA
<g/>
+	+	kIx~
<g/>
Série	série	k1gFnSc1
A	a	k9
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
70	#num#	k4
<g/>
BP	BP	kA
<g/>
20	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
90	#num#	k4
</s>
<s>
Celkově	celkově	k6eAd1
</s>
<s>
511205-5114-12843690262	511205-5114-12843690262	k4
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
</s>
<s>
Statistika	statistika	k1gFnSc1
na	na	k7c4
velkým	velký	k2eAgNnPc3d1
turnajích	turnaj	k1gInPc6
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Fáze	fáze	k1gFnSc1
turnajeDatumSoupeřOdehraných	turnajeDatumSoupeřOdehraný	k2eAgFnPc2d1
minutVstřelené	minutVstřelený	k2eAgNnSc5d1
brankyVýsledek	brankyVýsledek	k1gInSc1
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
1999	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venezuela	Venezuela	k1gFnSc1
<g/>
2017	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
<g/>
1002	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chile	Chile	k1gNnSc1
<g/>
4501	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
<g/>
1502	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
</s>
<s>
1	#num#	k4
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovensko	Slovensko	k1gNnSc1
<g/>
7903	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
9001	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
<g/>
6501	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamerun	Kamerun	k1gInSc1
<g/>
11311	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
v	v	k7c4
(	(	kIx(
<g/>
prodl	prodnout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2002	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turecko	Turecko	k1gNnSc1
<g/>
6702	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
<g/>
4514	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgie	Belgie	k1gFnSc1
<g/>
8102	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglie	Anglie	k1gFnSc1
<g/>
5712	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
<g/>
8502	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
9001	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnSc1
<g/>
9002	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
<g/>
7104	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ghana	Ghana	k1gFnSc1
<g/>
9003	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
<g/>
9000	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2008	#num#	k4
</s>
<s>
1	#num#	k4
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgie	Belgie	k1gFnSc1
<g/>
9001	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
9025	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
zápas	zápas	k1gInSc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
<g/>
9003	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamerun	Kamerun	k1gInSc1
<g/>
9002	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
v	v	k7c4
(	(	kIx(
<g/>
prodl	prodnout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argentina	Argentina	k1gFnSc1
<g/>
9000	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Belgie	Belgie	k1gFnSc1
<g/>
9003	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Ronaldinho	Ronaldinze	k6eAd1
<g/>
,	,	kIx,
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
Klubové	klubový	k2eAgNnSc1d1
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
ligy	liga	k1gFnSc2
Gaúcho	Gaúcha	k1gFnSc5
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
ligy	liga	k1gFnSc2
Carioca	carioca	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
ligy	liga	k1gFnSc2
Mineiro	Mineiro	k1gNnSc4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
španělské	španělský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
italské	italský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
španělského	španělský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
Poháru	pohár	k1gInSc2
osvoboditelů	osvoboditel	k1gMnPc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
poháru	pohár	k1gInSc2
Intertoto	Intertota	k1gFnSc5
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
jihoamerického	jihoamerický	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reprezentační	reprezentační	k2eAgMnSc1d1
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
na	na	k7c6
MS	MS	kA
(	(	kIx(
<g/>
2002	#num#	k4
-	-	kIx~
zlato	zlato	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
na	na	k7c6
CA	ca	kA
(	(	kIx(
<g/>
1999	#num#	k4
-	-	kIx~
zlato	zlato	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
na	na	k7c4
Konfederačního	konfederační	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
1999	#num#	k4
-	-	kIx~
stříbro	stříbro	k1gNnSc4
<g/>
,2003	,2003	k4
<g/>
,	,	kIx,
2005	#num#	k4
-	-	kIx~
zlato	zlato	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
na	na	k7c6
OH	OH	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
-	-	kIx~
bronz	bronz	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
na	na	k7c6
MJA	MJA	kA
U20	U20	k1gFnSc6
(	(	kIx(
<g/>
1999	#num#	k4
-	-	kIx~
bronz	bronz	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
na	na	k7c6
MS	MS	kA
U20	U20	k1gFnSc6
(	(	kIx(
<g/>
1999	#num#	k4
-	-	kIx~
zlato	zlato	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
FIFA	FIFA	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
podle	podle	k7c2
časopisu	časopis	k1gInSc2
World	World	k1gMnSc1
Soccer	Soccer	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
na	na	k7c6
Konfederačním	konfederační	k2eAgInSc6d1
poháru	pohár	k1gInSc6
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
Nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
na	na	k7c6
Konfederačním	konfederační	k2eAgInSc6d1
poháru	pohár	k1gInSc6
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
Nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
Campeonato	Campeonat	k2eAgNnSc4d1
Gaúcho	Gaúcha	k1gFnSc5
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
Nejlepší	dobrý	k2eAgMnSc1d3
zahraniční	zahraniční	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
španělské	španělský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
x	x	k?
Nejlepší	dobrý	k2eAgMnSc1d3
fotbalista	fotbalista	k1gMnSc1
brazilské	brazilský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
Nejlepší	dobrý	k2eAgMnSc1d3
útočník	útočník	k1gMnSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
UEFA	UEFA	kA
Club	club	k1gInSc1
Footballer	Footballer	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
FIFPro	FIFPro	k1gNnSc4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
All	All	k?
Stars	Stars	k1gInSc1
Ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
All	All	k?
Stars	Stars	k1gInSc1
Team	team	k1gInSc1
UEFA	UEFA	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
All	All	k?
Stars	Stars	k1gInSc1
Team	team	k1gInSc1
ESM	ESM	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
All	All	k?
Stars	Stars	k1gInSc1
Team	team	k1gInSc1
FIFPro	FIFPro	k1gNnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
All	All	k?
Stars	Stars	k1gInSc1
Team	team	k1gInSc1
MS	MS	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
fotbalista	fotbalista	k1gMnSc1
desetiletí	desetiletí	k1gNnSc2
podle	podle	k7c2
časopisu	časopis	k1gInSc2
World	World	k1gMnSc1
Soccer	Soccer	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
člen	člen	k1gMnSc1
klubu	klub	k1gInSc2
Golden	Goldna	k1gFnPc2
Foot	Foot	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ronaldinho	Ronaldin	k1gMnSc2
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Pele	pel	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
list	list	k1gInSc1
of	of	k?
the	the	k?
greatest	greatest	k1gInSc1
<g/>
,	,	kIx,
BBC	BBC	kA
Sport	sport	k1gInSc1
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Ronaldinha	Ronaldinha	k1gFnSc1
odvedli	odvést	k5eAaPmAgMnP
v	v	k7c6
poutech	pouto	k1gNnPc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
Po	po	k7c6
šíleném	šílený	k2eAgInSc6d1
deliktu	delikt	k1gInSc6
mu	on	k3xPp3gMnSc3
hrozí	hrozit	k5eAaImIp3nS
vězení	vězení	k1gNnSc4
<g/>
.	.	kIx.
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
tn	tn	k?
<g/>
.	.	kIx.
<g/>
nova	nova	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-03-05	2020-03-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SEZNAM	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldinho	Ronaldin	k1gMnSc2
ve	v	k7c4
vězení	vězení	k1gNnSc4
<g/>
:	:	kIx,
oslavy	oslava	k1gFnPc1
<g/>
,	,	kIx,
specialita	specialita	k1gFnSc1
<g/>
,	,	kIx,
fotbálek	fotbálek	k1gInSc1
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/3296241	https://www.transfermarkt.de/spielbericht/index/spielbericht/3296241	k4
<g/>
↑	↑	k?
http://news.bbc.co.uk/sport2/hi/football/europe/1123095.stm	http://news.bbc.co.uk/sport2/hi/football/europe/1123095.stm	k1gInSc1
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/1002469	https://www.transfermarkt.de/spielbericht/index/spielbericht/1002469	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/1002547	https://www.transfermarkt.de/spielbericht/index/spielbericht/1002547	k4
<g/>
↑	↑	k?
https://www.repubblica.it/online/calciomercato2003/ronalbarca/rolabarca/rolabarca.html	https://www.repubblica.it/online/calciomercato2003/ronalbarca/rolabarca/rolabarca.html	k1gInSc1
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/492	https://www.transfermarkt.de/spielbericht/index/spielbericht/492	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/707	https://www.transfermarkt.de/spielbericht/index/spielbericht/707	k4
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/zahranici/kouzelnik-ronaldinho-skolil-real-madrid.A051119_222000_fot_zahranici_min	https://www.idnes.cz/fotbal/zahranici/kouzelnik-ronaldinho-skolil-real-madrid.A051119_222000_fot_zahranici_min	k1gInSc1
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/reprezentace/kralem-je-ronaldinho-ziskal-zlaty-mic.A051128_160247_fot_reprez_min	https://www.idnes.cz/fotbal/reprezentace/kralem-je-ronaldinho-ziskal-zlaty-mic.A051128_160247_fot_reprez_min	k1gInSc1
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/pohary/vitezem-ligy-mistru-se-stala-barcelona.A060517_223027_fot_pohary_no	https://www.idnes.cz/fotbal/pohary/vitezem-ligy-mistru-se-stala-barcelona.A060517_223027_fot_pohary_no	k6eAd1
<g/>
↑	↑	k?
https://www.livesport.cz/fotbal/evropa/superpohar-uefa-2006/	https://www.livesport.cz/fotbal/evropa/superpohar-uefa-2006/	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/991944	https://www.transfermarkt.de/spielbericht/index/spielbericht/991944	k4
<g/>
↑	↑	k?
https://sport.aktualne.cz/fotbal/ronaldinho-vzpomina-na-barcelonu-byla-to-nocni-mura/r~i:article:612747/	https://sport.aktualne.cz/fotbal/ronaldinho-vzpomina-na-barcelonu-byla-to-nocni-mura/r~i:article:612747/	k4
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/zahranici/o-osudu-ronaldinha-se-rozhodne-v-nejblizsich-72-hodinach.A080714_153902_fot_zahranici_pes	https://www.idnes.cz/fotbal/zahranici/o-osudu-ronaldinha-se-rozhodne-v-nejblizsich-72-hodinach.A080714_153902_fot_zahranici_pes	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
https://www.sport.cz/fotbal/serie-a/clanek/128132-ronaldinho-se-uz-dohodl-s-ac-milan.html	https://www.sport.cz/fotbal/serie-a/clanek/128132-ronaldinho-se-uz-dohodl-s-ac-milan.html	k1gInSc1
<g/>
↑	↑	k?
https://sport.aktualne.cz/fotbal/na-san-siru-propukl-karneval-dorazil-ronaldinho/r~i:article:611249/	https://sport.aktualne.cz/fotbal/na-san-siru-propukl-karneval-dorazil-ronaldinho/r~i:article:611249/	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/918601	https://www.transfermarkt.de/spielbericht/index/spielbericht/918601	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/926048	https://www.transfermarkt.de/spielbericht/index/spielbericht/926048	k4
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinho-by-mohl-zmenit-dres-do-brazilie-ale-nepujde-85562/	https://www.eurofotbal.cz/clanky/ronaldinho-by-mohl-zmenit-dres-do-brazilie-ale-nepujde-85562/	k4
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinhovi-se-vraci-usmev-na-tvar-99922/	https://www.eurofotbal.cz/clanky/ronaldinhovi-se-vraci-usmev-na-tvar-99922/	k4
<g/>
↑	↑	k?
http://www.fotbalportal.cz/ostatni/vse/19486-hracem-desetileti-je-podle-magazinu-world-soccer-ronaldinho/	http://www.fotbalportal.cz/ostatni/vse/19486-hracem-desetileti-je-podle-magazinu-world-soccer-ronaldinho/	k4
<g/>
↑	↑	k?
https://isport.blesk.cz/clanek/fotbal-zahranici/84794/hrac-desetileti-nedved-je-sedmy-vyhral-ronaldinho.html	https://isport.blesk.cz/clanek/fotbal-zahranici/84794/hrac-desetileti-nedved-je-sedmy-vyhral-ronaldinho.html	k1gInSc1
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinho-je-se-svymi-letosnimi-vykony-v-ac-spokojen-115984/	https://www.eurofotbal.cz/clanky/ronaldinho-je-se-svymi-letosnimi-vykony-v-ac-spokojen-115984/	k4
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/agent-vyloucil-letni-prestup-ronaldinha-do-flamenga-121603/	https://www.eurofotbal.cz/clanky/agent-vyloucil-letni-prestup-ronaldinha-do-flamenga-121603/	k4
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinho-je-v-milane-spokojeny-odchazet-nechce-132153/	https://www.eurofotbal.cz/clanky/ronaldinho-je-v-milane-spokojeny-odchazet-nechce-132153/	k4
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinho-se-stehuje-z-ac-milan-do-flamenga-136802/	https://www.eurofotbal.cz/clanky/ronaldinho-se-stehuje-z-ac-milan-do-flamenga-136802/	k4
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/zahranici/ronaldinha-vitalo-v-brazilskem-flamengu-dvacet-tisic-fanousku.A110113_120241_fot_zahranici_mn	https://www.idnes.cz/fotbal/zahranici/ronaldinha-vitalo-v-brazilskem-flamengu-dvacet-tisic-fanousku.A110113_120241_fot_zahranici_mn	k1gMnSc1
<g/>
↑	↑	k?
https://isport.blesk.cz/clanek/fotbal-zahranici/124198/ronaldinho-skoncil-ve-flamengu-a-chce-po-klubu-miliony-dolaru.html	https://isport.blesk.cz/clanek/fotbal-zahranici/124198/ronaldinho-skoncil-ve-flamengu-a-chce-po-klubu-miliony-dolaru.html	k1gInSc1
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinho-bude-pokracovat-v-mineiru-184696/	https://www.eurofotbal.cz/clanky/ronaldinho-bude-pokracovat-v-mineiru-184696/	k4
<g/>
↑	↑	k?
https://www.gazzetta.it/premium/plus/Calcio_Estero/25-07-2013/ronaldinho-atletico-mineiro-20846621738.shtml	https://www.gazzetta.it/premium/plus/Calcio_Estero/25-07-2013/ronaldinho-atletico-mineiro-20846621738.shtml	k1gMnSc1
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
https://www.sport.cz/fotbal/ostatni/clanek/512390-ronaldinho-celi-nejtezsimu-zraneni-v-kariere-pretrhl-si-sval.html	https://www.sport.cz/fotbal/ostatni/clanek/512390-ronaldinho-celi-nejtezsimu-zraneni-v-kariere-pretrhl-si-sval.html	k1gInSc1
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/zahranici/atletico-mineiro-do-finale-neproslo-senzace-na-ms-klubu.A131219_115935_fot_zahranici_min	https://www.idnes.cz/fotbal/zahranici/atletico-mineiro-do-finale-neproslo-senzace-na-ms-klubu.A131219_115935_fot_zahranici_min	k1gInSc1
<g/>
↑	↑	k?
https://www.eurofotbal.cz/souteze/fifa/mistrovstvi-sveta-klubu/2013/reportaz/guangzhou-fc-atletico-mineiro-364350/	https://www.eurofotbal.cz/souteze/fifa/mistrovstvi-sveta-klubu/2013/reportaz/guangzhou-fc-atletico-mineiro-364350/	k4
<g/>
↑	↑	k?
https://www.denik.cz/fotbal/nejlepsi-hrac-jizni-ameriky-messi-to-neni-poprve-vyhral-ronaldinho-20131231.html	https://www.denik.cz/fotbal/nejlepsi-hrac-jizni-ameriky-messi-to-neni-poprve-vyhral-ronaldinho-20131231.html	k1gInSc1
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2480758	https://www.transfermarkt.de/spielbericht/index/spielbericht/2480758	k4
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinho-rozvazal-smlouvu-s-atleticem-mineiro-263433/	https://www.eurofotbal.cz/clanky/ronaldinho-rozvazal-smlouvu-s-atleticem-mineiro-263433/	k4
<g/>
↑	↑	k?
https://www.theguardian.com/football/2015/jun/20/ronaldinho-queretaro-departure-one-year-contract	https://www.theguardian.com/football/2015/jun/20/ronaldinho-queretaro-departure-one-year-contract	k1gInSc1
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinho-v-queretaru-nechodi-na-treninky-hrozi-mu-vyhazov-278660/	https://www.eurofotbal.cz/clanky/ronaldinho-v-queretaru-nechodi-na-treninky-hrozi-mu-vyhazov-278660/	k4
<g/>
↑	↑	k?
http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/07/nao-falta-mais-nada-ronaldinho-assina-contrato-e-e-jogador-do-flu.html	http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/07/nao-falta-mais-nada-ronaldinho-assina-contrato-e-e-jogador-do-flu.html	k1gMnSc1
<g/>
↑	↑	k?
http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/09/ronaldinho-gaucho-nao-e-mais-jogador-do-fluminense.html	http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/09/ronaldinho-gaucho-nao-e-mais-jogador-do-fluminense.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.sport.cz/fotbal/ostatni/clanek/907368-gesto-par-excellence-ronaldinho-se-smiloval-nad-rozvernym-fanouskem.html	https://www.sport.cz/fotbal/ostatni/clanek/907368-gesto-par-excellence-ronaldinho-se-smiloval-nad-rozvernym-fanouskem.html	k1gInSc1
<g/>
↑	↑	k?
https://www.eurofotbal.cz/clanky/ronaldinho-definitivne-uzavrel-karieru-a-planuje-oficialni-rozluckove-turne-386964/	https://www.eurofotbal.cz/clanky/ronaldinho-definitivne-uzavrel-karieru-a-planuje-oficialni-rozluckove-turne-386964/	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2975817	https://www.transfermarkt.de/spielbericht/index/spielbericht/2975817	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/3041930	https://www.transfermarkt.de/spielbericht/index/spielbericht/3041930	k4
<g/>
↑	↑	k?
https://www.idnes.cz/fotbal/ms-2002/bitvu-gigantu-rozhodla-chyba-anglickeho-brankare.A020621_102852_ms2002_min	https://www.idnes.cz/fotbal/ms-2002/bitvu-gigantu-rozhodla-chyba-anglickeho-brankare.A020621_102852_ms2002_min	k1gInSc1
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2294295	https://www.transfermarkt.de/spielbericht/index/spielbericht/2294295	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/ronaldinho/profil/spieler/3373	https://www.transfermarkt.de/ronaldinho/profil/spieler/3373	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/3041930	https://www.transfermarkt.de/spielbericht/index/spielbericht/3041930	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/3042207	https://www.transfermarkt.de/spielbericht/index/spielbericht/3042207	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/3041934	https://www.transfermarkt.de/spielbericht/index/spielbericht/3041934	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/3041946	https://www.transfermarkt.de/spielbericht/index/spielbericht/3041946	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.eurofotbal.cz	www.eurofotbal.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.eurofotbal.cz	www.eurofotbal.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.eurofotbal.cz	www.eurofotbal.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://www.eurofotbal.cz/olympijske-hry-muzi-2000/reportaz/brazilie-kamerun-148981/	https://www.eurofotbal.cz/olympijske-hry-muzi-2000/reportaz/brazilie-kamerun-148981/	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/987519	https://www.transfermarkt.de/spielbericht/index/spielbericht/987519	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/987535	https://www.transfermarkt.de/spielbericht/index/spielbericht/987535	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/987568	https://www.transfermarkt.de/spielbericht/index/spielbericht/987568	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/987571	https://www.transfermarkt.de/spielbericht/index/spielbericht/987571	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/987578	https://www.transfermarkt.de/spielbericht/index/spielbericht/987578	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/46384	https://www.transfermarkt.de/spielbericht/index/spielbericht/46384	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/49324	https://www.transfermarkt.de/spielbericht/index/spielbericht/49324	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/49338	https://www.transfermarkt.de/spielbericht/index/spielbericht/49338	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/53480	https://www.transfermarkt.de/spielbericht/index/spielbericht/53480	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/53487	https://www.transfermarkt.de/spielbericht/index/spielbericht/53487	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2688716	https://www.transfermarkt.de/spielbericht/index/spielbericht/2688716	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2688718	https://www.transfermarkt.de/spielbericht/index/spielbericht/2688718	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2688720	https://www.transfermarkt.de/spielbericht/index/spielbericht/2688720	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2688728	https://www.transfermarkt.de/spielbericht/index/spielbericht/2688728	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2688733	https://www.transfermarkt.de/spielbericht/index/spielbericht/2688733	k4
<g/>
↑	↑	k?
https://www.transfermarkt.de/spielbericht/index/spielbericht/2688734	https://www.transfermarkt.de/spielbericht/index/spielbericht/2688734	k4
<g/>
↑	↑	k?
UEFA	UEFA	kA
Club	club	k1gInSc1
Footballer	Footballer	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
<g/>
,	,	kIx,
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
30	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ronaldinho	Ronaldin	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c4
Transfermarkt	Transfermarkt	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c4
National-football-teams	National-football-teams	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Photos	Photos	k1gMnSc1
and	and	k?
Videos	Videos	k1gMnSc1
Ronaldinho	Ronaldin	k1gMnSc2
</s>
<s>
Brazilské	brazilský	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
</s>
<s>
Ronaldinho	Ronaldinze	k6eAd1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
-	-	kIx~
fotbal	fotbal	k1gInSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2008	#num#	k4
-	-	kIx~
turnaj	turnaj	k1gInSc1
mužů	muž	k1gMnPc2
Argentina	Argentina	k1gFnSc1
</s>
<s>
Oscar	Oscar	k1gMnSc1
Ustari	Ustar	k1gFnSc2
•	•	k?
Ezequiel	Ezequiel	k1gMnSc1
Garay	Garaa	k1gFnSc2
•	•	k?
Luciano	Luciana	k1gFnSc5
Fabián	Fabián	k1gMnSc1
Monzón	Monzón	k1gMnSc1
•	•	k?
Pablo	Pablo	k1gNnSc4
Zabaleta	Zabaleta	k1gFnSc1
•	•	k?
Fernando	Fernanda	k1gFnSc5
Gago	Gago	k1gMnSc1
•	•	k?
Federico	Federico	k1gMnSc1
Fazio	Fazio	k1gMnSc1
•	•	k?
José	Josá	k1gFnSc2
Ernesto	Ernesta	k1gMnSc5
Sosa	Sosa	k1gMnSc1
•	•	k?
Éver	Éver	k1gMnSc1
Banega	Baneg	k1gMnSc2
•	•	k?
Ezequiel	Ezequiel	k1gMnSc1
Lavezzi	Lavezh	k1gMnPc1
•	•	k?
Juan	Juan	k1gMnSc1
Román	Román	k1gMnSc1
Riquelme	Riquelme	k1gMnSc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Nicolás	Nicolás	k1gInSc1
Pareja	Pareja	k1gFnSc1
•	•	k?
Lautaro	Lautara	k1gFnSc5
Acosta	Acosta	k1gMnSc1
•	•	k?
Javier	Javier	k1gMnSc1
Mascherano	Mascherana	k1gFnSc5
•	•	k?
Lionel	Lionel	k1gMnSc1
Messi	Mess	k1gMnSc3
•	•	k?
Sergio	Sergio	k1gMnSc1
Agüero	Agüero	k1gNnSc1
•	•	k?
Diego	Diega	k1gMnSc5
Buonanotte	Buonanott	k1gMnSc5
•	•	k?
Sergio	Sergio	k1gMnSc1
Germán	Germán	k1gMnSc1
Romero	Romero	k1gNnSc1
•	•	k?
Nicolás	Nicolása	k1gFnPc2
NavarroTrenér	NavarroTrenér	k1gMnSc1
<g/>
:	:	kIx,
Sergio	Sergio	k1gMnSc1
Batista	Batista	k1gMnSc1
Nigérie	Nigérie	k1gFnSc1
</s>
<s>
Ambruse	Ambruse	k6eAd1
Vanzekin	Vanzekin	k1gMnSc1
•	•	k?
Chibuzor	Chibuzor	k1gMnSc1
Okonkwo	Okonkwo	k1gMnSc1
•	•	k?
Onyekachi	Onyekachi	k1gNnPc2
Apam	Apamo	k1gNnPc2
•	•	k?
Dele	Delos	k1gInSc5
Adeleye	Adeley	k1gInPc4
•	•	k?
Monday	Mondaa	k1gFnSc2
James	James	k1gMnSc1
•	•	k?
Chinedu	Chined	k1gMnSc3
Obasi	Obas	k1gMnSc3
•	•	k?
Sani	saně	k1gFnSc3
Kaita	Kaita	k1gMnSc1
•	•	k?
Victor	Victor	k1gMnSc1
Nsofor	Nsofor	k1gMnSc1
Obinna	Obinna	k1gFnSc1
•	•	k?
Isaac	Isaac	k1gFnSc1
Promise	Promise	k1gFnSc2
•	•	k?
Solomon	Solomon	k1gMnSc1
Okoronkwo	Okoronkwo	k6eAd1
•	•	k?
Oluwafemi	Oluwafe	k1gFnPc7
Ajilore	Ajilor	k1gInSc5
•	•	k?
Olubayo	Olubayo	k6eAd1
Adefemi	Adefe	k1gFnPc7
•	•	k?
Peter	Peter	k1gMnSc1
Odemwingie	Odemwingie	k1gFnSc2
•	•	k?
Efe	Efe	k1gFnSc2
Ambrose	Ambrosa	k1gFnSc3
•	•	k?
Victor	Victor	k1gMnSc1
Anichebe	Anicheb	k1gInSc5
•	•	k?
Emmanuel	Emmanuel	k1gMnSc1
Ekpo	Ekpo	k1gMnSc1
•	•	k?
Ikechukwu	Ikechukwa	k1gMnSc4
Ezenwa	Ezenwus	k1gMnSc4
•	•	k?
Oladapo	Oladapa	k1gFnSc5
OlufemiTrenér	OlufemiTrenér	k1gMnSc1
<g/>
:	:	kIx,
Samson	Samson	k1gMnSc1
Siasia	Siasius	k1gMnSc2
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
Diego	Diego	k1gMnSc1
Alves	Alves	k1gMnSc1
•	•	k?
Renan	Renan	k1gMnSc1
•	•	k?
Rafinha	Rafinha	k1gMnSc1
•	•	k?
Alex	Alex	k1gMnSc1
Silva	Silva	k1gFnSc1
•	•	k?
Thiago	Thiago	k6eAd1
Silva	Silva	k1gFnSc1
•	•	k?
Marcelo	Marcela	k1gFnSc5
Vieira	Vieiro	k1gNnPc1
•	•	k?
Ilsinho	Ilsin	k1gMnSc2
•	•	k?
Breno	Breno	k6eAd1
•	•	k?
Hernanes	Hernanes	k1gMnSc1
•	•	k?
Anderson	Anderson	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gInSc1
•	•	k?
Ronaldinho	Ronaldin	k1gMnSc2
•	•	k?
Ramires	Ramires	k1gMnSc1
•	•	k?
Diego	Diego	k1gMnSc1
•	•	k?
Thiago	Thiago	k1gMnSc1
Neves	Neves	k1gMnSc1
•	•	k?
Alexandre	Alexandr	k1gInSc5
Pato	pata	k1gFnSc5
•	•	k?
Rafael	Rafael	k1gMnSc1
Sóbis	Sóbis	k1gFnSc2
•	•	k?
JôTrenér	JôTrenér	k1gInSc1
<g/>
:	:	kIx,
Dunga	Dunga	k1gFnSc1
</s>
<s>
Medailisté	medailista	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2002	#num#	k4
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
Dida	Dida	k1gMnSc1
•	•	k?
Marcos	Marcos	k1gMnSc1
•	•	k?
Rogério	Rogério	k1gMnSc1
Ceni	Cen	k1gFnSc2
•	•	k?
Ânderson	Ânderson	k1gInSc1
Polga	Polga	k1gFnSc1
•	•	k?
Juliano	Juliana	k1gFnSc5
Belletti	Belletti	k1gNnSc1
•	•	k?
Cafu	Cafus	k1gInSc2
–	–	k?
•	•	k?
Edmílson	Edmílson	k1gMnSc1
•	•	k?
Júnior	Júnior	k1gMnSc1
•	•	k?
Lúcio	Lúcio	k1gMnSc1
•	•	k?
Roberto	Roberta	k1gFnSc5
Carlos	Carlos	k1gMnSc1
•	•	k?
Roque	Roque	k1gInSc1
Júnior	Júnior	k1gInSc1
•	•	k?
Gilberto	Gilberta	k1gFnSc5
Silva	Silva	k1gFnSc1
•	•	k?
Juninho	Junin	k1gMnSc4
Paulista	Paulista	k1gMnSc1
•	•	k?
Kléberson	Kléberson	k1gInSc1
•	•	k?
Ricardinho	Ricardin	k1gMnSc2
•	•	k?
Ronaldinho	Ronaldin	k1gMnSc2
•	•	k?
Vampeta	Vampet	k1gMnSc2
•	•	k?
Denílson	Denílson	k1gMnSc1
•	•	k?
Edílson	Edílson	k1gMnSc1
•	•	k?
Kaká	kakat	k5eAaImIp3nS
•	•	k?
Luizã	Luizã	k6eAd1
•	•	k?
Rivaldo	Rivaldo	k1gNnSc1
•	•	k?
Ronaldo	Ronaldo	k1gNnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Luiz	Luiz	k1gMnSc1
Felipe	Felip	k1gInSc5
Scolari	Scolari	k1gNnSc4
Německo	Německo	k1gNnSc1
</s>
<s>
Hans-Jörg	Hans-Jörg	k1gMnSc1
Butt	Butt	k1gMnSc1
•	•	k?
Oliver	Oliver	k1gMnSc1
Kahn	Kahn	k1gMnSc1
–	–	k?
•	•	k?
Jens	Jens	k1gInSc1
Lehmann	Lehmann	k1gMnSc1
•	•	k?
Frank	Frank	k1gMnSc1
Baumann	Baumann	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Linke	Link	k1gFnSc2
•	•	k?
Christoph	Christoph	k1gMnSc1
Metzelder	Metzelder	k1gMnSc1
•	•	k?
Carsten	Carsten	k2eAgMnSc1d1
Ramelow	Ramelow	k1gMnSc1
•	•	k?
Marko	Marko	k1gMnSc1
Rehmer	Rehmer	k1gMnSc1
•	•	k?
Christian	Christian	k1gMnSc1
Ziege	Zieg	k1gFnSc2
•	•	k?
Michael	Michael	k1gMnSc1
Ballack	Ballack	k1gMnSc1
•	•	k?
Jörg	Jörg	k1gMnSc1
Böhme	Böhm	k1gInSc5
•	•	k?
Torsten	Torsten	k2eAgInSc1d1
Frings	Frings	k1gInSc1
•	•	k?
Dietmar	Dietmar	k1gInSc1
Hamann	Hamann	k1gInSc1
•	•	k?
Jens	Jens	k1gInSc1
Jeremies	Jeremies	k1gMnSc1
•	•	k?
Sebastian	Sebastian	k1gMnSc1
Kehl	Kehl	k1gMnSc1
•	•	k?
Lars	Lars	k1gInSc1
Ricken	Ricken	k1gInSc1
•	•	k?
Bernd	Bernd	k1gInSc1
Schneider	Schneider	k1gMnSc1
•	•	k?
Gerald	Gerald	k1gMnSc1
Asamoah	Asamoah	k1gMnSc1
•	•	k?
Oliver	Oliver	k1gMnSc1
Bierhoff	Bierhoff	k1gMnSc1
•	•	k?
Marco	Marco	k6eAd1
Bode	bůst	k5eAaImIp3nS
•	•	k?
Carsten	Carsten	k2eAgMnSc1d1
Jancker	Jancker	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Klose	Klose	k1gFnSc2
•	•	k?
Oliver	Oliver	k1gMnSc1
Neuville	Neuville	k1gFnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Rudi	ruď	k1gFnPc1
Völler	Völler	k1gInSc4
Turecko	Turecko	k1gNnSc1
</s>
<s>
Ömer	Ömer	k1gMnSc1
Çatkı	Çatkı	k1gMnSc1
•	•	k?
Zafer	Zafer	k1gMnSc1
Özgültekin	Özgültekin	k1gMnSc1
•	•	k?
Rüştü	Rüştü	k1gMnSc1
Reçber	Reçber	k1gMnSc1
•	•	k?
Fatih	Fatih	k1gMnSc1
Akyel	Akyel	k1gMnSc1
•	•	k?
Emre	Emre	k1gInSc1
Aşı	Aşı	k1gInSc1
•	•	k?
Bülent	Bülent	k1gInSc1
Korkmaz	Korkmaz	k1gInSc1
•	•	k?
Alpay	Alpaa	k1gFnSc2
Özalan	Özalan	k1gMnSc1
•	•	k?
Ümit	Ümit	k1gMnSc1
Özat	Özat	k1gMnSc1
•	•	k?
Hakan	Hakan	k1gMnSc1
Ünsal	Ünsal	k1gMnSc1
•	•	k?
Yı	Yı	k1gInPc1
Baştürk	Baştürk	k1gInSc1
•	•	k?
Emre	Emre	k1gFnSc1
Belözoğ	Belözoğ	k1gInSc2
•	•	k?
Okan	Okan	k1gMnSc1
Buruk	Buruk	k1gMnSc1
•	•	k?
Ümit	Ümit	k1gMnSc1
Davala	Daval	k1gMnSc2
•	•	k?
Abdullah	Abdullah	k1gMnSc1
Ercan	Ercan	k1gMnSc1
•	•	k?
Tayfur	Tayfur	k1gMnSc1
Havutçu	Havutçus	k1gInSc2
•	•	k?
Mustafa	Mustaf	k1gMnSc2
İ	İ	k5eAaPmF,k5eAaImF
•	•	k?
Tugay	Tugaa	k1gFnPc4
Kerimoğ	Kerimoğ	k1gInSc2
•	•	k?
Ergün	Ergün	k1gMnSc1
Penbe	Penb	k1gInSc5
•	•	k?
Arif	Arif	k1gInSc1
Erdem	Erdem	k1gInSc1
•	•	k?
Nihat	Nihat	k1gInSc4
Kahveci	Kahvece	k1gFnSc4
•	•	k?
İ	İ	k1gMnSc1
Mansı	Mansı	k1gMnSc1
•	•	k?
Hasan	Hasan	k1gMnSc1
Şaş	Şaş	k1gMnSc1
•	•	k?
Hakan	Hakan	k1gMnSc1
Şükür	Şükür	k1gMnSc1
–	–	k?
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Şenol	Şenol	k1gInSc1
Güneş	Güneş	k1gFnSc2
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Víctor	Víctor	k1gInSc1
Valdés	Valdésa	k1gFnPc2
Hráči	hráč	k1gMnPc1
</s>
<s>
Oleguer	Oleguer	k1gInSc1
•	•	k?
Juliano	Juliana	k1gFnSc5
Belletti	Belletti	k1gNnSc3
•	•	k?
Carles	Carles	k1gInSc1
Puyol	Puyol	k1gInSc1
–	–	k?
•	•	k?
Rafael	Rafael	k1gMnSc1
Márquez	Márquez	k1gMnSc1
•	•	k?
Giovanni	Giovaneň	k1gFnSc3
van	vana	k1gFnPc2
Bronckhorst	Bronckhorst	k1gMnSc1
•	•	k?
Deco	Deco	k1gMnSc1
•	•	k?
Edmílson	Edmílson	k1gMnSc1
•	•	k?
Andrés	Andrés	k1gInSc1
Iniesta	Iniesta	k1gMnSc1
•	•	k?
Mark	Mark	k1gMnSc1
van	vana	k1gFnPc2
Bommel	Bommel	k1gMnSc1
•	•	k?
Henrik	Henrik	k1gMnSc1
Larsson	Larsson	k1gNnSc1
•	•	k?
Ludovic	Ludovice	k1gFnPc2
Giuly	Giula	k1gFnSc2
•	•	k?
Samuel	Samuel	k1gMnSc1
Eto	Eto	k1gMnSc1
<g/>
'	'	kIx"
<g/>
o	o	k7c6
•	•	k?
Ronaldinho	Ronaldin	k1gMnSc2
•	•	k?
Albert	Albert	k1gMnSc1
Jorquera	Jorquer	k1gMnSc2
•	•	k?
Sylvinho	Sylvin	k1gMnSc2
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
Xavi	Xavi	k1gNnSc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Frank	Frank	k1gMnSc1
Rijkaard	Rijkaard	k1gMnSc1
</s>
<s>
Konfederační	konfederační	k2eAgInSc1d1
pohár	pohár	k1gInSc1
FIFA	FIFA	kA
2005	#num#	k4
–	–	k?
Brazílie	Brazílie	k1gFnSc2
Brankář	brankář	k1gMnSc1
</s>
<s>
Dida	Dida	k1gMnSc1
•	•	k?
Marcos	Marcos	k1gMnSc1
•	•	k?
Heurelho	Heurel	k1gMnSc4
Gomes	Gomes	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Maicon	Maicon	k1gMnSc1
•	•	k?
Lúcio	Lúcio	k1gMnSc1
•	•	k?
Roque	Roque	k1gInSc1
Júnior	Júnior	k1gMnSc1
•	•	k?
Emerson	Emerson	k1gMnSc1
•	•	k?
Gilberto	Gilberta	k1gFnSc5
Silva	Silva	k1gFnSc1
•	•	k?
Robinho	Robin	k1gMnSc2
•	•	k?
Kaká	kakat	k5eAaImIp3nS
•	•	k?
Adriano	Adriana	k1gFnSc5
•	•	k?
Ronaldinho	Ronaldin	k1gMnSc2
•	•	k?
Zé	Zé	k1gMnSc2
Roberto	Roberta	k1gFnSc5
•	•	k?
Cicinho	Cicin	k1gMnSc2
•	•	k?
Juan	Juan	k1gMnSc1
•	•	k?
Luisã	Luisã	k1gMnSc1
•	•	k?
Léo	Léo	k1gMnSc1
•	•	k?
Gilberto	Gilberta	k1gFnSc5
Silva	Silva	k1gFnSc1
•	•	k?
Juninho	Junin	k1gMnSc2
•	•	k?
Renato	Renata	k1gFnSc5
•	•	k?
Júlio	Júlio	k1gMnSc1
Baptista	baptista	k1gMnSc1
•	•	k?
Ricardo	Ricardo	k1gNnSc4
Oliveira	Oliveir	k1gInSc2
•	•	k?
Edu	Eda	k1gMnSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Carlos	Carlos	k1gMnSc1
Alberto	Alberta	k1gFnSc5
Parreira	Parreiro	k1gNnSc2
</s>
<s>
Copa	Cop	k2eAgFnSc1d1
América	América	k1gFnSc1
1999	#num#	k4
–	–	k?
Brazílie	Brazílie	k1gFnSc2
Brankář	brankář	k1gMnSc1
</s>
<s>
Dida	Dida	k1gMnSc1
•	•	k?
Marcos	Marcos	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Cafu	Cafu	k6eAd1
•	•	k?
Odvan	Odvan	k1gMnSc1
•	•	k?
Antônio	Antônio	k1gMnSc1
Carlos	Carlos	k1gMnSc1
•	•	k?
Emerson	Emerson	k1gMnSc1
•	•	k?
Roberto	Roberta	k1gFnSc5
Carlos	Carlos	k1gMnSc1
•	•	k?
Amoroso	amoroso	k6eAd1
•	•	k?
Vampeta	Vampeta	k1gFnSc1
•	•	k?
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Rivaldo	Rivaldo	k1gNnSc1
•	•	k?
Alex	Alex	k1gMnSc1
•	•	k?
Evanílson	Evanílson	k1gMnSc1
•	•	k?
César	César	k1gMnSc1
Belli	Bell	k1gMnPc1
•	•	k?
Joã	Joã	k6eAd1
Carlos	Carlos	k1gInSc1
•	•	k?
Serginho	Sergin	k1gMnSc2
•	•	k?
Marcos	Marcos	k1gMnSc1
Paulo	Paula	k1gFnSc5
•	•	k?
Flávio	Flávio	k1gMnSc1
Conceiçã	Conceiçã	k1gMnSc1
•	•	k?
Beto	Beto	k1gMnSc1
•	•	k?
Christian	Christian	k1gMnSc1
•	•	k?
Ronaldinho	Ronaldin	k1gMnSc2
•	•	k?
Zé	Zé	k1gMnSc2
Roberto	Roberta	k1gFnSc5
Hlavní	hlavní	k2eAgFnSc7d1
trenér	trenér	k1gMnSc1
</s>
<s>
Vanderlei	Vanderlei	k6eAd1
Luxemburgo	Luxemburgo	k6eAd1
</s>
<s>
Serie	serie	k1gFnSc1
A	a	k9
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
–	–	k?
AC	AC	kA
Milán	Milán	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Christian	Christian	k1gMnSc1
Abbiati	Abbiat	k1gMnPc1
•	•	k?
Marco	Marco	k6eAd1
Amelia	Amelium	k1gNnSc2
Hráči	hráč	k1gMnPc1
</s>
<s>
Thiago	Thiago	k6eAd1
Silva	Silva	k1gFnSc1
•	•	k?
Ignazio	Ignazia	k1gMnSc5
Abate	Abat	k1gMnSc5
•	•	k?
Alessandro	Alessandra	k1gFnSc5
Nesta	Nesto	k1gNnSc2
•	•	k?
Luca	Lucum	k1gNnSc2
Antonini	Antonin	k2eAgMnPc1d1
•	•	k?
Daniele	Daniela	k1gFnSc3
Bonera	Bonero	k1gNnSc2
•	•	k?
<g/>
Gianluca	Gianluca	k1gMnSc1
Zambrotta	Zambrotta	k1gMnSc1
•	•	k?
Mario	Mario	k1gMnSc1
Yepes	Yepes	k1gMnSc1
•	•	k?
Massimo	Massima	k1gFnSc5
Oddo	Oddo	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Jankulovski	Jankulovsk	k1gFnSc2
•	•	k?
Sokratis	Sokratis	k1gFnSc2
Papastathopulos	Papastathopulos	k1gMnSc1
•	•	k?
Gennaro	Gennara	k1gFnSc5
Gattuso	Gattusa	k1gFnSc5
•	•	k?
Clarence	Clarenec	k1gInPc1
Seedorf	Seedorf	k1gInSc1
•	•	k?
Kevin-Prince	Kevin-Prinec	k1gInSc2
Boateng	Boatenga	k1gFnPc2
•	•	k?
Mathieu	Mathieus	k1gInSc2
Flamini	Flamin	k2eAgMnPc1d1
•	•	k?
Massimo	Massima	k1gFnSc5
Ambrosini	Ambrosin	k2eAgMnPc1d1
•	•	k?
Andrea	Andrea	k1gFnSc1
Pirlo	Pirlo	k1gNnSc1
•	•	k?
Mark	Mark	k1gMnSc1
van	vana	k1gFnPc2
Bommel	Bommel	k1gMnSc1
•	•	k?
Urby	Urba	k1gFnSc2
Emanuelson	Emanuelsona	k1gFnPc2
•	•	k?
Alexander	Alexandra	k1gFnPc2
Merkel	Merkel	k1gMnSc1
•	•	k?
Rodney	Rodney	k1gInPc1
Strasser	Strasser	k1gInSc1
•	•	k?
Robinho	Robin	k1gMnSc2
•	•	k?
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc1
•	•	k?
Alexandre	Alexandr	k1gInSc5
Pato	pata	k1gFnSc5
•	•	k?
Antonio	Antonio	k1gMnSc1
Cassano	Cassana	k1gFnSc5
•	•	k?
Filippo	Filippa	k1gFnSc5
Inzaghi	Inzagh	k1gFnSc6
•	•	k?
Ronaldinho	Ronaldin	k1gMnSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Massimiliano	Massimiliana	k1gFnSc5
Allegri	Allegr	k1gFnSc5
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Víctor	Víctor	k1gMnSc1
Valdés	Valdésa	k1gFnPc2
•	•	k?
Albert	Albert	k1gMnSc1
Jorquera	Jorquer	k1gMnSc2
•	•	k?
Rubén	Rubén	k1gInSc1
Martínez	Martínez	k1gInSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Oleguer	Oleguer	k1gInSc1
•	•	k?
Carles	Carles	k1gInSc1
Puyol	Puyol	k1gInSc1
•	•	k?
Rafael	Rafael	k1gMnSc1
Márquez	Márquez	k1gMnSc1
•	•	k?
Juliano	Juliana	k1gFnSc5
Belletti	Belletti	k1gNnPc1
•	•	k?
Sylvinho	Sylvin	k1gMnSc2
•	•	k?
Damià	Damià	k1gMnSc1
•	•	k?
Edmílson	Edmílson	k1gMnSc1
•	•	k?
Fernando	Fernanda	k1gFnSc5
Navarro	Navarra	k1gFnSc5
•	•	k?
Rodri	Rodri	k1gNnSc6
•	•	k?
Andrés	Andrés	k1gInSc1
Iniesta	Iniesta	k1gMnSc1
•	•	k?
Xavi	Xav	k1gFnSc2
•	•	k?
Deco	Deco	k1gMnSc1
•	•	k?
Giovanni	Giovaneň	k1gFnSc3
van	vana	k1gFnPc2
Bronckhorst	Bronckhorst	k1gMnSc1
•	•	k?
Gerard	Gerard	k1gMnSc1
López	López	k1gMnSc1
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
Demetrio	Demetrio	k6eAd1
Albertini	Albertin	k2eAgMnPc1d1
•	•	k?
Gabri	Gabr	k1gFnPc4
•	•	k?
Samuel	Samuel	k1gMnSc1
Eto	Eto	k1gMnSc1
<g/>
'	'	kIx"
<g/>
o	o	k7c6
•	•	k?
Ronaldinho	Ronaldin	k1gMnSc2
•	•	k?
Ludovic	Ludovice	k1gFnPc2
Giuly	Giula	k1gFnSc2
•	•	k?
Henrik	Henrik	k1gMnSc1
Larsson	Larsson	k1gMnSc1
•	•	k?
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc3
•	•	k?
Maxi	maxi	k2eAgMnSc1d1
López	López	k1gMnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Frank	Frank	k1gMnSc1
Rijkaard	Rijkaard	k1gMnSc1
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
–	–	k?
FC	FC	kA
Barcelona	Barcelona	k1gFnSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Víctor	Víctor	k1gMnSc1
Valdés	Valdésa	k1gFnPc2
•	•	k?
Albert	Albert	k1gMnSc1
Jorquera	Jorquer	k1gMnSc2
Hráči	hráč	k1gMnPc1
</s>
<s>
Carles	Carles	k1gMnSc1
Puyol	Puyola	k1gFnPc2
•	•	k?
Oleguer	Oleguer	k1gMnSc1
•	•	k?
Edmílson	Edmílson	k1gMnSc1
•	•	k?
Juliano	Juliana	k1gFnSc5
Belletti	Belletti	k1gNnPc1
•	•	k?
Sylvinho	Sylvin	k1gMnSc2
•	•	k?
Rafael	Rafael	k1gMnSc1
Márquez	Márquez	k1gMnSc1
•	•	k?
Rodri	Rodr	k1gFnSc2
•	•	k?
Javi	Jav	k1gFnSc2
Martos	Martos	k1gMnSc1
•	•	k?
Jesús	Jesús	k1gInSc1
Olmo	Olmo	k1gMnSc1
•	•	k?
Andrés	Andrés	k1gInSc1
Iniesta	Iniesta	k1gFnSc1
•	•	k?
Ludovic	Ludovice	k1gFnPc2
Giuly	Giula	k1gFnSc2
•	•	k?
Deco	Deco	k1gMnSc1
•	•	k?
Mark	Mark	k1gMnSc1
van	vana	k1gFnPc2
Bommel	Bommel	k1gMnSc1
•	•	k?
Giovanni	Giovaneň	k1gFnSc3
van	vana	k1gFnPc2
Bronckhorst	Bronckhorst	k1gInSc4
•	•	k?
Xavi	Xav	k1gFnSc2
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
Gabri	Gabri	k1gNnSc2
•	•	k?
Ludovic	Ludovice	k1gFnPc2
Sylvestre	Sylvestr	k1gInSc5
•	•	k?
Andrea	Andrea	k1gFnSc1
Orlandi	Orland	k1gMnPc1
•	•	k?
Ramón	Ramón	k1gInSc1
Masó	Masó	k1gFnSc1
•	•	k?
Pitu	pit	k2eAgFnSc4d1
•	•	k?
Samuel	Samuel	k1gMnSc1
Eto	Eto	k1gMnSc1
<g/>
'	'	kIx"
<g/>
o	o	k7c6
•	•	k?
Ronaldinho	Ronaldin	k1gMnSc2
•	•	k?
Henrik	Henrik	k1gMnSc1
Larsson	Larsson	k1gMnSc1
•	•	k?
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc3
•	•	k?
Santiago	Santiago	k1gNnSc1
Ezquerro	Ezquerro	k1gNnSc1
•	•	k?
Maxi	maxi	k2eAgMnSc1d1
López	López	k1gMnSc1
•	•	k?
Francisco	Francisco	k6eAd1
Montañ	Montañ	k1gFnPc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Frank	Frank	k1gMnSc1
Rijkaard	Rijkaard	k1gMnSc1
</s>
<s>
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
FIFA	FIFA	kA
<g/>
)	)	kIx)
podle	podle	k7c2
roků	rok	k1gInPc2
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
FIFA	FIFA	kA
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
Lothar	Lothar	k1gMnSc1
Matthäus	Matthäus	k1gMnSc1
•	•	k?
1992	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1993	#num#	k4
Roberto	Roberta	k1gFnSc5
Baggio	Baggio	k1gNnSc4
•	•	k?
1994	#num#	k4
Romário	Romário	k1gNnSc4
•	•	k?
1995	#num#	k4
George	George	k1gInSc1
Weah	Weah	k1gInSc4
•	•	k?
1996	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1997	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1998	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
1999	#num#	k4
Rivaldo	Rivaldo	k1gNnSc4
•	•	k?
2000	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
2001	#num#	k4
Luís	Luísa	k1gFnPc2
Figo	Figo	k6eAd1
•	•	k?
2002	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2003	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
2004	#num#	k4
Ronaldinho	Ronaldin	k1gMnSc4
•	•	k?
2005	#num#	k4
Ronaldinho	Ronaldin	k1gMnSc4
•	•	k?
2006	#num#	k4
Fabio	Fabio	k6eAd1
Cannavaro	Cannavara	k1gFnSc5
•	•	k?
2007	#num#	k4
Kaká	kakat	k5eAaImIp3nS
•	•	k?
2008	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2009	#num#	k4
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
FIFA	FIFA	kA
</s>
<s>
2010	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
FIFA	FIFA	kA
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Luka	luka	k1gNnPc4
Modrić	Modrić	k1gFnSc2
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
•	•	k?
2020	#num#	k4
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Lewandowski	Lewandowsk	k1gFnSc2
</s>
<s>
Držitelé	držitel	k1gMnPc1
ocenění	ocenění	k1gNnSc2
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
</s>
<s>
1956	#num#	k4
Stanley	Stanlea	k1gFnSc2
Matthews	Matthewsa	k1gFnPc2
•	•	k?
1957	#num#	k4
Alfredo	Alfredo	k1gNnSc4
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
•	•	k?
1958	#num#	k4
Raymond	Raymonda	k1gFnPc2
Kopa	kopa	k1gFnSc1
•	•	k?
1959	#num#	k4
Alfredo	Alfredo	k1gNnSc4
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
•	•	k?
1960	#num#	k4
Luis	Luisa	k1gFnPc2
Suárez	Suáreza	k1gFnPc2
•	•	k?
1961	#num#	k4
Omar	Omar	k1gInSc1
Sívori	Sívor	k1gFnSc2
•	•	k?
1962	#num#	k4
Josef	Josef	k1gMnSc1
Masopust	Masopust	k1gMnSc1
•	•	k?
1963	#num#	k4
Lev	lev	k1gInSc1
Jašin	Jašin	k1gInSc4
•	•	k?
1964	#num#	k4
Denis	Denisa	k1gFnPc2
Law	Law	k1gFnPc2
•	•	k?
1965	#num#	k4
Eusébio	Eusébio	k1gNnSc4
•	•	k?
1966	#num#	k4
Bobby	Bobba	k1gFnSc2
Charlton	Charlton	k1gInSc1
•	•	k?
1967	#num#	k4
Flórián	Flórián	k1gMnSc1
Albert	Albert	k1gMnSc1
•	•	k?
1968	#num#	k4
George	Georg	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Best	Best	k1gInSc1
•	•	k?
1969	#num#	k4
Gianni	Gianeň	k1gFnSc6
Rivera	Rivera	k1gFnSc1
•	•	k?
1970	#num#	k4
Gerd	Gerd	k1gInSc1
Müller	Müller	k1gInSc4
•	•	k?
1971	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1972	#num#	k4
Franz	Franz	k1gInSc1
Beckenbauer	Beckenbauer	k1gInSc4
•	•	k?
1973	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1974	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1975	#num#	k4
Oleg	Oleg	k1gMnSc1
Blochin	Blochin	k1gMnSc1
•	•	k?
1976	#num#	k4
Franz	Franz	k1gInSc1
Beckenbauer	Beckenbauer	k1gInSc4
•	•	k?
1977	#num#	k4
Allan	Allan	k1gMnSc1
Simonsen	Simonsen	k1gInSc1
•	•	k?
1978	#num#	k4
Kevin	Kevin	k2eAgInSc4d1
Keegan	Keegan	k1gInSc4
•	•	k?
1979	#num#	k4
Kevin	Kevin	k2eAgInSc4d1
Keegan	Keegan	k1gInSc4
•	•	k?
1980	#num#	k4
Karl-Heinz	Karl-Heinz	k1gInSc1
Rummenigge	Rummenigg	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1981	#num#	k4
Karl-Heinz	Karl-Heinza	k1gFnPc2
Rummenigge	Rummenigge	k1gFnPc2
•	•	k?
1982	#num#	k4
Paolo	Paolo	k1gNnSc4
Rossi	Rosse	k1gFnSc3
•	•	k?
1983	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1984	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1985	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1986	#num#	k4
Ihor	Ihor	k1gInSc1
Belanov	Belanov	k1gInSc4
•	•	k?
1987	#num#	k4
Ruud	Ruud	k1gInSc1
Gullit	Gullit	k1gInSc4
•	•	k?
1988	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1989	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1990	#num#	k4
Lothar	Lothar	k1gMnSc1
Matthäus	Matthäus	k1gMnSc1
•	•	k?
1991	#num#	k4
Jean-Pierre	Jean-Pierr	k1gInSc5
Papin	Papin	k1gMnSc1
•	•	k?
1992	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1993	#num#	k4
Roberto	Roberta	k1gFnSc5
Baggio	Baggio	k1gNnSc4
•	•	k?
1994	#num#	k4
Christo	Christa	k1gMnSc5
Stoičkov	Stoičkov	k1gInSc4
•	•	k?
1995	#num#	k4
George	George	k1gInSc1
Weah	Weah	k1gInSc4
•	•	k?
1996	#num#	k4
Matthias	Matthias	k1gInSc1
Sammer	Sammer	k1gInSc4
•	•	k?
1997	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1998	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
1999	#num#	k4
Rivaldo	Rivaldo	k1gNnSc4
•	•	k?
2000	#num#	k4
Luís	Luísa	k1gFnPc2
Figo	Figo	k6eAd1
•	•	k?
2001	#num#	k4
Michael	Michael	k1gMnSc1
Owen	Owen	k1gMnSc1
•	•	k?
2002	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2003	#num#	k4
Pavel	Pavel	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
•	•	k?
2004	#num#	k4
Andrij	Andrij	k1gFnSc1
Ševčenko	Ševčenka	k1gFnSc5
•	•	k?
2005	#num#	k4
Ronaldinho	Ronaldin	k1gMnSc4
•	•	k?
2006	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Fabio	Fabio	k1gNnSc1
Cannavaro	Cannavara	k1gFnSc5
•	•	k?
2007	#num#	k4
Kaká	kakat	k5eAaImIp3nS
•	•	k?
2008	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2009	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2017	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2018	#num#	k4
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gFnSc2
•	•	k?
2019	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2020	#num#	k4
neuděleno	udělen	k2eNgNnSc1d1
V	v	k7c6
letech	let	k1gInPc6
2010	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
sloučeno	sloučit	k5eAaPmNgNnS
do	do	k7c2
ankety	anketa	k1gFnSc2
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
FIFAUmístění	FIFAUmístění	k1gNnSc2
českých	český	k2eAgMnPc2d1
fotbalistů	fotbalista	k1gMnPc2
v	v	k7c6
anketě	anketa	k1gFnSc6
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
51678	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
134061969	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1473	#num#	k4
6586	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2006053623	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
61865288	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2006053623	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
