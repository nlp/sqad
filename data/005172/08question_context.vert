<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
v	v	k7c6
současnosti	současnost	k1gFnSc6
nejvyšší	vysoký	k2eAgFnSc1d3
známá	známý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
-	-	kIx~
štítová	štítový	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
Olympus	Olympus	k1gMnSc1
Mons	Mons	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
výšky	výška	k1gFnPc4
přes	přes	k7c4
21	[number]	k4
km	km	kA
<g/>
.	.	kIx.
</s>