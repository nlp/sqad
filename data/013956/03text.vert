<s>
Jurij	Jurít	k5eAaPmRp2nS
Gagarin	Gagarin	k1gInSc4
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jiných	jiný	k2eAgInPc6d1
významech	význam	k1gInPc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Gagarin	Gagarin	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Alexejevič	Alexejevič	k1gMnSc1
Gagarin	Gagarin	k1gInSc1
Kosmonaut	kosmonaut	k1gMnSc1
CPK	CPK	kA
Státní	státní	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1934	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Klušino	Klušin	k2eAgNnSc1d1
<g/>
,	,	kIx,
Západní	západní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
<g/>
,	,	kIx,
SSSR	SSSR	kA
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
(	(	kIx(
<g/>
34	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Novoselovo	Novoselův	k2eAgNnSc1d1
<g/>
,	,	kIx,
Vladimirská	Vladimirský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
<g/>
,	,	kIx,
SSSR	SSSR	kA
Předchozízaměstnání	Předchozízaměstnání	k1gNnSc1
</s>
<s>
Letec	letec	k1gMnSc1
vojenského	vojenský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
Hodnost	hodnost	k1gFnSc1
</s>
<s>
Plukovník	plukovník	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1963	#num#	k4
<g/>
)	)	kIx)
Čas	čas	k1gInSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
1	#num#	k4
hodina	hodina	k1gFnSc1
a	a	k8xC
48	#num#	k4
minut	minuta	k1gFnPc2
Kosmonaut	kosmonaut	k1gMnSc1
od	od	k7c2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1960	#num#	k4
Mise	mise	k1gFnSc1
</s>
<s>
Vostok	Vostok	k1gInSc4
1	#num#	k4
Kosmonaut	kosmonaut	k1gMnSc1
do	do	k7c2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jurij	Jurij	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
Gagarin	Gagarin	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Ю	Ю	k?
А	А	k?
Г	Г	k?
<g/>
;	;	kIx,
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1934	#num#	k4
Klušino	Klušin	k2eAgNnSc4d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Západní	západní	k2eAgMnPc1d1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Smolenská	Smolenský	k2eAgFnSc1d1
<g/>
)	)	kIx)
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
<g/>
,	,	kIx,
Novoselovo	Novoselův	k2eAgNnSc1d1
<g/>
,	,	kIx,
Vladimirská	Vladimirský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
sovětský	sovětský	k2eAgMnSc1d1
kosmonaut	kosmonaut	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vzlétl	vzlétnout	k5eAaPmAgMnS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
svému	svůj	k3xOyFgInSc3
kosmickému	kosmický	k2eAgInSc3d1
letu	let	k1gInSc3
odstartoval	odstartovat	k5eAaPmAgInS
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
v	v	k7c6
lodi	loď	k1gFnSc6
Vostok	Vostok	k1gInSc1
1	#num#	k4
z	z	k7c2
kosmodromu	kosmodrom	k1gInSc2
Bajkonur	Bajkonura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obletěl	obletět	k5eAaPmAgMnS
Zemi	zem	k1gFnSc4
a	a	k8xC
po	po	k7c6
108	#num#	k4
či	či	k8xC
106	#num#	k4
minutách	minuta	k1gFnPc6
přistál	přistát	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
letu	let	k1gInSc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hrdinou	hrdina	k1gMnSc7
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
oslavovanou	oslavovaný	k2eAgFnSc7d1
světovou	světový	k2eAgFnSc7d1
celebritou	celebrita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
následujících	následující	k2eAgNnPc2d1
let	léto	k1gNnPc2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
života	život	k1gInSc2
zaujala	zaujmout	k5eAaPmAgFnS
setkání	setkání	k1gNnSc4
s	s	k7c7
lidmi	člověk	k1gMnPc7
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
i	i	k8xC
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
zátěž	zátěž	k1gFnSc4
svých	svůj	k3xOyFgFnPc2
veřejných	veřejný	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
od	od	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
Žukovského	Žukovského	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
<g/>
,	,	kIx,
současně	současně	k6eAd1
stál	stát	k5eAaImAgInS
v	v	k7c6
čele	čelo	k1gNnSc6
oddílu	oddíl	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalšímu	další	k2eAgInSc3d1
kosmickému	kosmický	k2eAgInSc3d1
letu	let	k1gInSc3
se	se	k3xPyFc4
nedostal	dostat	k5eNaPmAgMnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
pouze	pouze	k6eAd1
náhradníkem	náhradník	k1gMnSc7
Vladimira	Vladimiro	k1gNnSc2
Komarova	Komarův	k2eAgNnSc2d1
pro	pro	k7c4
let	let	k1gInSc4
Sojuzu	Sojuz	k1gInSc2
1	#num#	k4
v	v	k7c6
dubnu	duben	k1gInSc6
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1968	#num#	k4
dokončil	dokončit	k5eAaPmAgInS
studium	studium	k1gNnSc4
na	na	k7c6
akademii	akademie	k1gFnSc6
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
k	k	k7c3
létání	létání	k1gNnSc3
v	v	k7c6
letadlech	letadlo	k1gNnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
při	při	k7c6
cvičném	cvičný	k2eAgInSc6d1
letu	let	k1gInSc6
zahynul	zahynout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
2	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
1	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
3	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
2	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
4	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
3	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
5	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
4	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
6	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
5	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
7	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
6	#num#	k4
ul	ul	kA
<g/>
{	{	kIx(
<g/>
display	displaa	k1gMnSc2
<g/>
:	:	kIx,
<g/>
none	none	k6eAd1
<g/>
}	}	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Gagarinův	Gagarinův	k2eAgInSc1d1
rodný	rodný	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c4
Klušinu	Klušina	k1gFnSc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Gagarin	Gagarin	k1gInSc1
pocházel	pocházet	k5eAaImAgInS
z	z	k7c2
prosté	prostý	k2eAgFnSc2d1
venkovské	venkovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
žila	žít	k5eAaImAgFnS
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
Klušino	Klušin	k2eAgNnSc1d1
ve	v	k7c6
Smolenské	Smolenský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
rodiče	rodič	k1gMnPc1
pracovali	pracovat	k5eAaImAgMnP
v	v	k7c6
kolchozu	kolchoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
<g/>
,	,	kIx,
Alexej	Alexej	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Gagarin	Gagarin	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
tesař	tesař	k1gMnSc1
a	a	k8xC
truhlář	truhlář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
Timofejevna	Timofejevna	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
dojičkou	dojička	k1gFnSc7
<g/>
,	,	kIx,
později	pozdě	k6eAd2
povýšila	povýšit	k5eAaPmAgFnS
na	na	k7c4
vedoucí	vedoucí	k1gMnPc4
mléčné	mléčný	k2eAgFnSc2d1
farmy	farma	k1gFnSc2
kolchozu	kolchoz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Gagarinovi	Gagarin	k1gMnSc3
měli	mít	k5eAaImAgMnP
čtyři	čtyři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
nejstaršího	starý	k2eAgMnSc4d3
Valentina	Valentin	k1gMnSc4
<g/>
,	,	kIx,
jedinou	jediný	k2eAgFnSc4d1
dceru	dcera	k1gFnSc4
Zoju	Zoja	k1gFnSc4
<g/>
,	,	kIx,
Jurije	Jurije	k1gFnSc1
a	a	k8xC
nejmladšího	mladý	k2eAgMnSc2d3
Borise	Boris	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
jejich	jejich	k3xOp3gInSc2
života	život	k1gInSc2
silně	silně	k6eAd1
zasáhla	zasáhnout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
září	září	k1gNnSc6
1941	#num#	k4
přiblížila	přiblížit	k5eAaPmAgFnS
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
na	na	k7c6
poli	pole	k1gNnSc6
za	za	k7c7
vesnicí	vesnice	k1gFnSc7
poškozený	poškozený	k1gMnSc1
Jak-	Jak-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
vesnické	vesnický	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
poprvé	poprvé	k6eAd1
v	v	k7c6
životě	život	k1gInSc6
viděly	vidět	k5eAaImAgInP
letadlo	letadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malému	Malý	k1gMnSc3
Jurijovi	Jurij	k1gMnSc3
pilot	pilot	k1gInSc4
dovolil	dovolit	k5eAaPmAgInS
i	i	k8xC
sednout	sednout	k5eAaPmF
si	se	k3xPyFc3
do	do	k7c2
kabiny	kabina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
příchodu	příchod	k1gInSc6
Němců	Němec	k1gMnPc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1941	#num#	k4
byla	být	k5eAaImAgFnS
uzavřena	uzavřen	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
začal	začít	k5eAaPmAgMnS
sedmiletý	sedmiletý	k2eAgMnSc1d1
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc4
chodit	chodit	k5eAaImF
teprve	teprve	k6eAd1
před	před	k7c7
několika	několik	k4yIc7
týdny	týden	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
nikdo	nikdo	k3yNnSc1
z	z	k7c2
rodiny	rodina	k1gFnSc2
nezahynul	zahynout	k5eNaPmAgMnS
<g/>
,	,	kIx,
život	život	k1gInSc4
na	na	k7c6
okupovaném	okupovaný	k2eAgNnSc6d1
území	území	k1gNnSc6
byl	být	k5eAaImAgInS
nelehký	lehký	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnPc1
dům	dům	k1gInSc4
zabrali	zabrat	k5eAaPmAgMnP
němečtí	německý	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
a	a	k8xC
Gagarinovi	Gagarinův	k2eAgMnPc1d1
si	se	k3xPyFc3
pak	pak	k6eAd1
museli	muset	k5eAaImAgMnP
vykopat	vykopat	k5eAaPmF
zemljanku	zemljanka	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
měli	mít	k5eAaImAgMnP
kde	kde	k6eAd1
žít	žít	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
školních	školní	k2eAgFnPc2d1
lavic	lavice	k1gFnPc2
se	se	k3xPyFc4
Jurij	Jurij	k1gMnSc1
mohl	moct	k5eAaImAgMnS
vrátit	vrátit	k5eAaPmF
až	až	k9
po	po	k7c4
osvobození	osvobození	k1gNnSc4
v	v	k7c6
březnu	březen	k1gInSc6
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
rodina	rodina	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
<g/>
,	,	kIx,
ustupující	ustupující	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
s	s	k7c7
sebou	se	k3xPyFc7
odvlekli	odvléct	k5eAaPmAgMnP
jeho	jeho	k3xOp3gNnSc4
dva	dva	k4xCgInPc4
starší	starý	k2eAgMnSc1d2
sourozence	sourozenec	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
oba	dva	k4xCgMnPc1
ze	z	k7c2
zajetí	zajetí	k1gNnSc2
utekli	utéct	k5eAaPmAgMnP
a	a	k8xC
poté	poté	k6eAd1
bojovali	bojovat	k5eAaImAgMnP
v	v	k7c6
Rudé	rudý	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otce	otec	k1gMnSc2
zase	zase	k9
po	po	k7c6
osvobození	osvobození	k1gNnSc6
odvedli	odvést	k5eAaPmAgMnP
do	do	k7c2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
sloužil	sloužit	k5eAaImAgMnS
v	v	k7c6
nedalekém	daleký	k2eNgNnSc6d1
Gžatsku	Gžatsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
se	se	k3xPyFc4
shledala	shledat	k5eAaPmAgFnS
až	až	k9
po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
v	v	k7c6
létě	léto	k1gNnSc6
1945	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1945	#num#	k4
se	se	k3xPyFc4
Gagarinovi	Gagarinův	k2eAgMnPc1d1
přestěhovali	přestěhovat	k5eAaPmAgMnP
do	do	k7c2
Gžatska	Gžatsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1949	#num#	k4
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc4
dokončil	dokončit	k5eAaPmAgMnS
6	#num#	k4
<g/>
.	.	kIx.
třídu	třída	k1gFnSc4
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
vyučit	vyučit	k5eAaPmF
v	v	k7c6
nějakém	nějaký	k3yIgInSc6
řemeslu	řemeslo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijali	přijmout	k5eAaPmAgMnP
ho	on	k3xPp3gInSc4
do	do	k7c2
učiliště	učiliště	k1gNnSc2
při	při	k7c6
závodu	závod	k1gInSc6
zemědělských	zemědělský	k2eAgInPc2d1
strojů	stroj	k1gInPc2
v	v	k7c6
Ljubercích	Ljuberk	k1gInPc6
u	u	k7c2
Moskvy	Moskva	k1gFnSc2
na	na	k7c4
výuku	výuka	k1gFnSc4
v	v	k7c6
oboru	obor	k1gInSc6
slévač	slévač	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
ve	v	k7c6
večerní	večerní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
dělnické	dělnický	k2eAgFnSc2d1
mládeže	mládež	k1gFnSc2
chodil	chodit	k5eAaImAgMnS
do	do	k7c2
7	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
měl	mít	k5eAaImAgInS
úplné	úplný	k2eAgNnSc4d1
střední	střední	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
učiliště	učiliště	k1gNnSc2
roku	rok	k1gInSc2
1951	#num#	k4
byl	být	k5eAaImAgMnS
jako	jako	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
učňů	učeň	k1gMnPc2
vybrán	vybrán	k2eAgInSc1d1
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
studiu	studio	k1gNnSc3
na	na	k7c6
Saratovské	Saratovský	k2eAgFnSc6d1
průmyslové	průmyslový	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
absolvoval	absolvovat	k5eAaPmAgInS
<g />
.	.	kIx.
</s>
<s hack="1">
s	s	k7c7
výborným	výborný	k2eAgInSc7d1
prospěchem	prospěch	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Ljubercích	Ljuberk	k1gInPc6
i	i	k8xC
v	v	k7c6
Saratově	Saratův	k2eAgMnSc6d1
hodně	hodně	k6eAd1
času	čas	k1gInSc2
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
sportu	sport	k1gInSc2
<g/>
,	,	kIx,
vynikl	vyniknout	k5eAaPmAgInS
zejména	zejména	k9
v	v	k7c6
basketbalu	basketbal	k1gInSc6
<g/>
,	,	kIx,
navzdory	navzdory	k7c3
své	svůj	k3xOyFgFnSc3
výšce	výška	k1gFnSc3
–	–	k?
dorostl	dorůst	k5eAaPmAgInS
pouze	pouze	k6eAd1
165	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Saratově	Saratův	k2eAgFnSc6d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
i	i	k9
kapitánem	kapitán	k1gMnSc7
basketbalového	basketbalový	k2eAgNnSc2d1
mužstva	mužstvo	k1gNnSc2
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
také	také	k9
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
trubku	trubka	k1gFnSc4
v	v	k7c6
kapele	kapela	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Účastnil	účastnit	k5eAaImAgMnS
se	se	k3xPyFc4
práce	práce	k1gFnSc2
fyzikálního	fyzikální	k2eAgInSc2d1
kroužku	kroužek	k1gInSc2
<g/>
,	,	kIx,
zde	zde	k6eAd1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
dostal	dostat	k5eAaPmAgMnS
za	za	k7c4
úkol	úkol	k1gInSc4
připravit	připravit	k5eAaPmF
přednášku	přednáška	k1gFnSc4
o	o	k7c6
Konstantinu	Konstantin	k1gMnSc6
Ciolkovském	Ciolkovský	k2eAgMnSc6d1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc6
pracích	práce	k1gFnPc6
o	o	k7c6
raketových	raketový	k2eAgInPc6d1
motorech	motor	k1gInPc6
a	a	k8xC
kosmických	kosmický	k2eAgInPc6d1
letech	let	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ciolkovského	Ciolkovský	k2eAgInSc2d1
myšlenky	myšlenka	k1gFnPc4
zanechaly	zanechat	k5eAaPmAgFnP
v	v	k7c6
Gagarinovi	Gagarin	k1gMnSc6
hluboký	hluboký	k2eAgInSc1d1
dojem	dojem	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letec	letec	k1gMnSc1
</s>
<s>
MiG-	MiG-	k?
<g/>
15	#num#	k4
<g/>
bis	bis	k?
v	v	k7c6
Leteckém	letecký	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Moninu	Monin	k1gInSc6
<g/>
,	,	kIx,
typ	typ	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgMnSc6,k3yRgMnSc6,k3yQgMnSc6
létal	létat	k5eAaImAgMnS
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc4
v	v	k7c6
Luostari	Luostar	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
studií	studio	k1gNnPc2
v	v	k7c6
Saratově	Saratův	k2eAgNnSc6d1
se	s	k7c7
spolužáky	spolužák	k1gMnPc7
snil	snít	k5eAaImAgMnS
o	o	k7c4
létání	létání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
se	se	k3xPyFc4
přihlásili	přihlásit	k5eAaPmAgMnP
do	do	k7c2
pilotních	pilotní	k2eAgInPc2d1
kursů	kurs	k1gInPc2
DOSAAF	DOSAAF	kA
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
létání	létání	k1gNnSc3
se	se	k3xPyFc4
nedostali	dostat	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
nedaleko	daleko	k6eNd1
Saratova	Saratův	k2eAgFnSc1d1
vznikla	vzniknout	k5eAaPmAgFnS
škola	škola	k1gFnSc1
civilního	civilní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
zkusili	zkusit	k5eAaPmAgMnP
štěstí	štěstit	k5eAaImIp3nP
i	i	k9
tam	tam	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
nesplňovali	splňovat	k5eNaImAgMnP
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uspěli	uspět	k5eAaPmAgMnP
až	až	k9
napotřetí	napotřetí	k6eAd1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
říjnu	říjen	k1gInSc6
1954	#num#	k4
je	on	k3xPp3gInPc4
přijali	přijmout	k5eAaPmAgMnP
v	v	k7c6
saratovském	saratovský	k2eAgInSc6d1
aeroklubu	aeroklub	k1gInSc6
<g/>
,	,	kIx,
od	od	k7c2
jara	jaro	k1gNnSc2
1955	#num#	k4
už	už	k9
Gagarin	Gagarin	k1gInSc1
létal	létat	k5eAaImAgInS
na	na	k7c4
Jaku-	Jaku-	k1gFnSc4
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
dokončil	dokončit	k5eAaPmAgInS
studium	studium	k1gNnSc4
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
pro	pro	k7c4
dráhu	dráha	k1gFnSc4
stíhacího	stíhací	k2eAgMnSc2d1
letce	letec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
1955	#num#	k4
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
na	na	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Orenburgu	Orenburg	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
Studium	studium	k1gNnSc1
se	se	k3xPyFc4
neobešlo	obešnout	k5eNaImAgNnS,k5eNaPmAgNnS
bez	bez	k7c2
potíží	potíž	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
druhém	druhý	k4xOgInSc6
roce	rok	k1gInSc6
měl	mít	k5eAaImAgInS
Gagarin	Gagarin	k1gInSc1
problémy	problém	k1gInPc4
s	s	k7c7
přistáváním	přistávání	k1gNnSc7
s	s	k7c7
cvičným	cvičný	k2eAgMnSc7d1
MiGu-	MiGu-	k1gMnSc7
<g/>
15	#num#	k4
<g/>
UTI	UTI	kA
<g/>
,	,	kIx,
hrozilo	hrozit	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
dokonce	dokonce	k9
vyloučení	vyloučení	k1gNnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
ze	z	k7c2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Popularitu	popularita	k1gFnSc4
mezi	mezi	k7c7
spolustudenty	spolustudent	k1gMnPc7
mu	on	k3xPp3gMnSc3
nepřineslo	přinést	k5eNaPmAgNnS
jeho	jeho	k3xOp3gNnSc4
nekompromisní	kompromisní	k2eNgNnSc1d1
vyžadování	vyžadování	k1gNnSc1
disciplíny	disciplína	k1gFnSc2
z	z	k7c2
pozice	pozice	k1gFnSc2
pomocníka	pomocník	k1gMnSc2
velitele	velitel	k1gMnSc2
čety	četa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
dvou	dva	k4xCgInPc6
letech	let	k1gInPc6
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
]	]	kIx)
výuky	výuka	k1gFnSc2
leteckou	letecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
ukončil	ukončit	k5eAaPmAgInS
s	s	k7c7
vyznamenáním	vyznamenání	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k8xS,k8xC
absolvent	absolvent	k1gMnSc1
s	s	k7c7
vynikajícím	vynikající	k2eAgInSc7d1
prospěchem	prospěch	k1gInSc7
měl	mít	k5eAaImAgInS
právo	právo	k1gNnSc4
vybrat	vybrat	k5eAaPmF
si	se	k3xPyFc3
místo	místo	k7c2
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přáteli	přítel	k1gMnPc7
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
pro	pro	k7c4
život	život	k1gInSc4
na	na	k7c6
severu	sever	k1gInSc6
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Sloužil	sloužit	k5eAaImAgInS
u	u	k7c2
769	#num#	k4
<g/>
.	.	kIx.
stíhacího	stíhací	k2eAgInSc2d1
leteckého	letecký	k2eAgInSc2d1
pluku	pluk	k1gInSc2
122	#num#	k4
<g/>
.	.	kIx.
stíhací	stíhací	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
divize	divize	k1gFnSc2
(	(	kIx(
<g/>
769	#num#	k4
<g/>
-й	-й	k?
и	и	k?
а	а	k?
п	п	k?
(	(	kIx(
<g/>
И	И	k?
<g/>
)	)	kIx)
122	#num#	k4
<g/>
-й	-й	k?
и	и	k?
а	а	k?
д	д	k?
<g/>
)	)	kIx)
Severního	severní	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
na	na	k7c6
základně	základna	k1gFnSc6
Luostari-Novoje	Luostari-Novoj	k1gInSc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
]	]	kIx)
u	u	k7c2
Murmanska	Murmansk	k1gInSc2
za	za	k7c7
polárním	polární	k2eAgInSc7d1
kruhem	kruh	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
pluku	pluk	k1gInSc3
nováčci	nováček	k1gMnPc1
přijeli	přijet	k5eAaPmAgMnP
koncem	koncem	k7c2
listopadu	listopad	k1gInSc2
1957	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
důkladné	důkladný	k2eAgFnSc6d1
teoretické	teoretický	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
a	a	k8xC
složení	složení	k1gNnSc6
zkoušek	zkouška	k1gFnPc2
ze	z	k7c2
specifik	specifikon	k1gNnPc2
létání	létání	k1gNnSc2
na	na	k7c6
severu	sever	k1gInSc6
začali	začít	k5eAaPmAgMnP
od	od	k7c2
března	březen	k1gInSc2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
létat	létat	k5eAaImF
na	na	k7c6
strojích	stroj	k1gInPc6
MiG-	MiG-	k1gFnSc2
<g/>
15	#num#	k4
<g/>
bis	bis	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
útvaru	útvar	k1gInSc2
dosáhl	dosáhnout	k5eAaPmAgMnS
náletu	nálet	k1gInSc2
265	#num#	k4
hodin	hodina	k1gFnPc2
(	(	kIx(
<g/>
tedy	tedy	k9
v	v	k7c6
letadlech	letadlo	k1gNnPc6
nalétal	nalétat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
265	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
počítáno	počítán	k2eAgNnSc1d1
od	od	k7c2
počátků	počátek	k1gInPc2
jeho	jeho	k3xOp3gFnSc2
letecké	letecký	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
v	v	k7c6
aeroklubu	aeroklub	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
závěru	závěr	k1gInSc6
studií	studie	k1gFnPc2
v	v	k7c6
Orenburgu	Orenburg	k1gInSc6
se	se	k3xPyFc4
po	po	k7c6
jedenapůlroční	jedenapůlroční	k2eAgFnSc6d1
známosti	známost	k1gFnSc6
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1957	#num#	k4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
oženil	oženit	k5eAaPmAgMnS
se	s	k7c7
zdravotnicí	zdravotnice	k1gFnSc7
Valentinou	Valentina	k1gFnSc7
Ivanovnou	Ivanovna	k1gFnSc7
Gorjačevovou	Gorjačevová	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Seznámil	seznámit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
na	na	k7c6
tanečním	taneční	k2eAgInSc6d1
večeru	večer	k1gInSc6
v	v	k7c6
letecké	letecký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
8	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
rok	rok	k1gInSc4
a	a	k8xC
půl	půl	k1xP
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1959	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodila	narodit	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
Jelena	Jelena	k1gFnSc1
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
dcera	dcera	k1gFnSc1
Galja	Galja	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1961	#num#	k4
<g/>
,	,	kIx,
pouhých	pouhý	k2eAgInPc2d1
pět	pět	k4xCc4
týdnů	týden	k1gInPc2
před	před	k7c7
Gagarinovým	Gagarinův	k2eAgInSc7d1
kosmickým	kosmický	k2eAgInSc7d1
letem	let	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kosmonaut	kosmonaut	k1gMnSc1
<g/>
,	,	kIx,
výběr	výběr	k1gInSc1
a	a	k8xC
výcvik	výcvik	k1gInSc1
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1959	#num#	k4
rozhodla	rozhodnout	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
o	o	k7c6
výběru	výběr	k1gInSc6
budoucích	budoucí	k2eAgMnPc2d1
kosmonautů	kosmonaut	k1gMnPc2
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
pověřila	pověřit	k5eAaPmAgFnS
velitelství	velitelství	k1gNnSc4
letectva	letectvo	k1gNnSc2
najít	najít	k5eAaPmF
adepty	adept	k1gMnPc4
mezi	mezi	k7c7
řadovými	řadový	k2eAgInPc7d1
piloty	pilot	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Výběr	výběr	k1gInSc4
prováděla	provádět	k5eAaImAgFnS
speciální	speciální	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
v	v	k7c6
Institutu	institut	k1gInSc6
letecké	letecký	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
plukovníkem	plukovník	k1gMnSc7
zdravotní	zdravotní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
п	п	k?
м	м	k?
с	с	k?
<g/>
)	)	kIx)
Jevgenijem	Jevgenij	k1gMnSc7
Karpovem	Karpov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečný	konečný	k2eAgInSc1d1
verdikt	verdikt	k1gInSc1
v	v	k7c6
závěrečných	závěrečný	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
náboru	nábor	k1gInSc2
vynášela	vynášet	k5eAaImAgFnS
lékařská	lékařský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
generálem	generál	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
Babijčukem	Babijčuk	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
9	#num#	k4
<g/>
]	]	kIx)
Kritéria	kritérion	k1gNnPc1
byla	být	k5eAaImAgNnP
<g/>
:	:	kIx,
věk	věk	k1gInSc4
do	do	k7c2
35	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
do	do	k7c2
175	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
váha	váha	k1gFnSc1
do	do	k7c2
75	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
výborné	výborný	k2eAgNnSc1d1
zdraví	zdraví	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Vlastní	vlastní	k2eAgInSc1d1
výběr	výběr	k1gInSc1
začal	začít	k5eAaPmAgInS
v	v	k7c6
srpnu	srpen	k1gInSc6
1959	#num#	k4
prohlídkou	prohlídka	k1gFnSc7
lékařských	lékařský	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
3461	#num#	k4
letců	letec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
vybráno	vybrat	k5eAaPmNgNnS
206	#num#	k4
kandidátů	kandidát	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
v	v	k7c6
říjnu	říjen	k1gInSc6
až	až	k8xS
prosinci	prosinec	k1gInSc6
1959	#num#	k4
procházeli	procházet	k5eAaImAgMnP
prohlídkami	prohlídka	k1gFnPc7
v	v	k7c6
Ústřední	ústřední	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
vojenského	vojenský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Ještě	ještě	k6eAd1
před	před	k7c7
povoláním	povolání	k1gNnSc7
k	k	k7c3
prohlídce	prohlídka	k1gFnSc3
<g/>
,	,	kIx,
si	se	k3xPyFc3
pod	pod	k7c7
dojmem	dojem	k1gInSc7
úspěchů	úspěch	k1gInPc2
vesmírných	vesmírný	k2eAgInPc2d1
letů	let	k1gInPc2
Gagarin	Gagarin	k1gInSc1
podal	podat	k5eAaPmAgInS
žádost	žádost	k1gFnSc4
o	o	k7c4
začlenění	začlenění	k1gNnSc4
do	do	k7c2
oddílu	oddíl	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
takový	takový	k3xDgInSc4
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
10	#num#	k4
<g/>
]	]	kIx)
Absolvoval	absolvovat	k5eAaPmAgMnS
první	první	k4xOgMnSc1
i	i	k8xC
druhou	druhý	k4xOgFnSc7
(	(	kIx(
<g/>
v	v	k7c6
únoru	únor	k1gInSc6
1960	#num#	k4
<g/>
)	)	kIx)
vlnou	vlna	k1gFnSc7
prohlídek	prohlídka	k1gFnPc2
a	a	k8xC
pohovorů	pohovor	k1gInPc2
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
se	se	k3xPyFc4
mezi	mezi	k7c7
20	#num#	k4
budoucích	budoucí	k2eAgMnPc2d1
kosmonautů	kosmonaut	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
března	březen	k1gInSc2
1960	#num#	k4
do	do	k7c2
ledna	leden	k1gInSc2
1961	#num#	k4
procházel	procházet	k5eAaImAgInS
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
kolegy	kolega	k1gMnPc7
náročným	náročný	k2eAgInSc7d1
výcvikem	výcvik	k1gInSc7
v	v	k7c6
nově	nově	k6eAd1
zorganizovaném	zorganizovaný	k2eAgNnSc6d1
Středisku	středisko	k1gNnSc6
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
(	(	kIx(
<g/>
CPK	CPK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
čele	čelo	k1gNnSc6
stanul	stanout	k5eAaPmAgInS
Karpov	Karpov	k1gInSc1
<g/>
,	,	kIx,
celkově	celkově	k6eAd1
na	na	k7c4
přípravu	příprava	k1gFnSc4
kosmonautů	kosmonaut	k1gMnPc2
dohlížel	dohlížet	k5eAaImAgMnS
generál	generál	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Kamanin	Kamanin	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středisko	středisko	k1gNnSc1
bylo	být	k5eAaImAgNnS
zprvu	zprvu	k6eAd1
umístěno	umístit	k5eAaPmNgNnS
ve	v	k7c6
spartánských	spartánský	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
na	na	k7c6
moskevském	moskevský	k2eAgInSc6d1
Frunzeho	Frunzeha	k1gFnSc5
letišti	letiště	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
létě	léto	k1gNnSc6
1960	#num#	k4
přesídlilo	přesídlit	k5eAaPmAgNnS
do	do	k7c2
nově	nově	k6eAd1
budovaného	budovaný	k2eAgNnSc2d1
Hvězdného	hvězdný	k2eAgNnSc2d1
městečka	městečko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
výcviku	výcvik	k1gInSc2
Gagarin	Gagarin	k1gInSc1
vstoupil	vstoupit	k5eAaPmAgInS
do	do	k7c2
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gagarinovy	Gagarinův	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
při	při	k7c6
závěrečných	závěrečný	k2eAgFnPc6d1
zkouškách	zkouška	k1gFnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marxismus-leninismus	Marxismus-leninismus	k1gInSc1
–	–	k?
výborně	výborně	k6eAd1
</s>
<s>
Základy	základ	k1gInPc1
kosmické	kosmický	k2eAgFnSc2d1
a	a	k8xC
letecké	letecký	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
–	–	k?
započteno	započten	k2eAgNnSc1d1
</s>
<s>
Raketová	raketový	k2eAgFnSc1d1
a	a	k8xC
kosmická	kosmický	k2eAgFnSc1d1
technika	technika	k1gFnSc1
–	–	k?
výborně	výborně	k6eAd1
</s>
<s>
Konstrukce	konstrukce	k1gFnPc1
objektu	objekt	k1gInSc2
Vostok	Vostok	k1gInSc1
3A	3A	k4
–	–	k?
započteno	započten	k2eAgNnSc1d1
</s>
<s>
Speciální	speciální	k2eAgInSc1d1
kurs	kurs	k1gInSc1
astronomie	astronomie	k1gFnSc2
–	–	k?
výborně	výborně	k6eAd1
</s>
<s>
Speciální	speciální	k2eAgInSc1d1
kurs	kurs	k1gInSc1
geofyziky	geofyzika	k1gFnSc2
–	–	k?
dobře	dobře	k6eAd1
</s>
<s>
Speciální	speciální	k2eAgInSc1d1
kurs	kurs	k1gInSc1
filmování	filmování	k1gNnSc2
–	–	k?
výborně	výborně	k6eAd1
</s>
<s>
Praktické	praktický	k2eAgInPc1d1
kurzy	kurz	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Fyzická	fyzický	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
–	–	k?
dobře	dobře	k6eAd1
</s>
<s>
Výsadkářská	výsadkářský	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
–	–	k?
dobře	dobře	k6eAd1
</s>
<s>
Lety	let	k1gInPc1
v	v	k7c6
beztíži	beztíže	k1gFnSc6
–	–	k?
dobře	dobře	k6eAd1
</s>
<s>
Maketa	maketa	k1gFnSc1
objektu	objekt	k1gInSc2
č.	č.	k?
1	#num#	k4
–	–	k?
započteno	započten	k2eAgNnSc4d1
</s>
<s>
Maketa	maketa	k1gFnSc1
objektu	objekt	k1gInSc2
č.	č.	k?
2	#num#	k4
–	–	k?
započteno	započíst	k5eAaPmNgNnS
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
nedisponovalo	disponovat	k5eNaBmAgNnS
prostředky	prostředek	k1gInPc4
pro	pro	k7c4
výcvik	výcvik	k1gInSc4
všech	všecek	k3xTgFnPc2
dvaceti	dvacet	k4xCc2
kandidátů	kandidát	k1gMnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
byla	být	k5eAaImAgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
1960	#num#	k4
z	z	k7c2
oddílu	oddíl	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
vyčleněna	vyčlenit	k5eAaPmNgFnS
šestičlenná	šestičlenný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
první	první	k4xOgInPc4
lety	let	k1gInPc4
<g/>
,	,	kIx,
procházející	procházející	k2eAgFnSc7d1
výukou	výuka	k1gFnSc7
přednostně	přednostně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
11	#num#	k4
<g/>
]	]	kIx)
Program	program	k1gInSc4
přípravy	příprava	k1gFnSc2
sestavili	sestavit	k5eAaPmAgMnP
odborníci	odborník	k1gMnPc1
z	z	k7c2
Institutu	institut	k1gInSc2
letecké	letecký	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
vedením	vedení	k1gNnSc7
střediska	středisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládal	skládat	k5eAaImAgMnS
se	se	k3xPyFc4
z	z	k7c2
teoretické	teoretický	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnPc4d1
přednášky	přednáška	k1gFnPc4
o	o	k7c6
raketách	raketa	k1gFnPc6
<g/>
,	,	kIx,
o	o	k7c6
konstrukci	konstrukce	k1gFnSc6
kosmické	kosmický	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Vostok	Vostok	k1gInSc1
<g/>
,	,	kIx,
o	o	k7c6
letecké	letecký	k2eAgFnSc6d1
a	a	k8xC
kosmické	kosmický	k2eAgFnSc6d1
medicíně	medicína	k1gFnSc6
a	a	k8xC
zabezpečení	zabezpečení	k1gNnSc6
životních	životní	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
během	během	k7c2
kosmických	kosmický	k2eAgInPc2d1
letů	let	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c6
astronomii	astronomie	k1gFnSc6
<g/>
,	,	kIx,
geofyzice	geofyzika	k1gFnSc6
a	a	k8xC
filmování	filmování	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
kosmickou	kosmický	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
se	se	k3xPyFc4
kosmonauti	kosmonaut	k1gMnPc1
seznámili	seznámit	k5eAaPmAgMnP
ve	v	k7c6
výrobním	výrobní	k2eAgInSc6d1
závodu	závod	k1gInSc6
OKB-1	OKB-1	k1gFnSc2
i	i	k9
na	na	k7c6
kosmodromu	kosmodrom	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
praktická	praktický	k2eAgFnSc1d1
část	část	k1gFnSc1
obsahovala	obsahovat	k5eAaImAgFnS
<g/>
,	,	kIx,
kromě	kromě	k7c2
obecné	obecný	k2eAgFnSc2d1
fyzické	fyzický	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
<g/>
,	,	kIx,
náročné	náročný	k2eAgInPc1d1
testy	test	k1gInPc1
odolnosti	odolnost	k1gFnSc2
–	–	k?
v	v	k7c6
barokomoře	barokomora	k1gFnSc6
podstoupili	podstoupit	k5eAaPmAgMnP
simulaci	simulace	k1gFnSc4
pobytu	pobyt	k1gInSc2
ve	v	k7c6
výšce	výška	k1gFnSc6
5	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
a	a	k8xC
14	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
km	km	kA
<g/>
,	,	kIx,
na	na	k7c6
centrifuze	centrifuga	k1gFnSc6
zažívali	zažívat	k5eAaImAgMnP
přetížení	přetížený	k2eAgMnPc1d1
do	do	k7c2
12	#num#	k4
G	G	kA
<g/>
,	,	kIx,
v	v	k7c6
termokomoře	termokomora	k1gFnSc6
vydrželi	vydržet	k5eAaPmAgMnP
teplotu	teplota	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
70	#num#	k4
°	°	k?
<g/>
C	C	kA
až	až	k9
dvě	dva	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
<g/>
,	,	kIx,
třásli	třást	k5eAaImAgMnP
s	s	k7c7
nimi	on	k3xPp3gMnPc7
na	na	k7c6
vibračním	vibrační	k2eAgInSc6d1
stole	stol	k1gInSc6
<g/>
,	,	kIx,
strávili	strávit	k5eAaPmAgMnP
o	o	k7c6
samotě	samota	k1gFnSc6
v	v	k7c6
izolační	izolační	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
10	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Stav	stav	k1gInSc4
beztíže	beztíže	k1gFnSc2
si	se	k3xPyFc3
vyzkoušeli	vyzkoušet	k5eAaPmAgMnP
při	při	k7c6
parabolických	parabolický	k2eAgNnPc6d1
letech	léto	k1gNnPc6
na	na	k7c6
cvičném	cvičný	k2eAgInSc6d1
MiG-	MiG-	k1gMnSc2
<g/>
15	#num#	k4
<g/>
UTI	UTI	kA
<g/>
,	,	kIx,
absolvovali	absolvovat	k5eAaPmAgMnP
parašutistický	parašutistický	k2eAgInSc4d1
výcvik	výcvik	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
dnech	den	k1gInPc6
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
složila	složit	k5eAaPmAgFnS
vybraná	vybraný	k2eAgFnSc1d1
šestice	šestice	k1gFnSc1
závěrečné	závěrečný	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
získali	získat	k5eAaPmAgMnP
kvalifikaci	kvalifikace	k1gFnSc4
„	„	k?
<g/>
kosmonaut	kosmonaut	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zkušební	zkušební	k2eAgFnSc1d1
komise	komise	k1gFnSc1
doporučila	doporučit	k5eAaPmAgFnS
kandidáty	kandidát	k1gMnPc4
ke	k	k7c3
kosmickým	kosmický	k2eAgInPc3d1
letům	let	k1gInPc3
v	v	k7c6
pořadí	pořadí	k1gNnSc6
<g/>
:	:	kIx,
Jurij	Jurij	k1gFnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
,	,	kIx,
German	German	k1gMnSc1
Titov	Titov	k1gInSc1
<g/>
,	,	kIx,
Grigorij	Grigorij	k1gFnSc1
Něljubov	Něljubov	k1gInSc1
<g/>
,	,	kIx,
Andrijan	Andrijan	k1gMnSc1
Nikolajev	Nikolajev	k1gMnSc1
<g/>
,	,	kIx,
Valerij	Valerij	k1gMnSc1
Bykovskij	Bykovskij	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
Popovič	Popovič	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Ze	z	k7c2
šestice	šestice	k1gFnSc2
kandidátů	kandidát	k1gMnPc2
měli	mít	k5eAaImAgMnP
šanci	šance	k1gFnSc4
na	na	k7c4
první	první	k4xOgInSc4
let	let	k1gInSc4
Gagarin	Gagarin	k1gInSc4
<g/>
,	,	kIx,
Titov	Titov	k1gInSc4
a	a	k8xC
Něljubov	Něljubov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rovnocenné	rovnocenný	k2eAgFnSc6d1
připravenosti	připravenost	k1gFnSc6
kosmonautů	kosmonaut	k1gMnPc2
nakonec	nakonec	k6eAd1
byla	být	k5eAaImAgFnS
rozhodující	rozhodující	k2eAgFnSc1d1
Gagarinova	Gagarinův	k2eAgFnSc1d1
povaha	povaha	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc4
srdečnost	srdečnost	k1gFnSc4
a	a	k8xC
skromnost	skromnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Definitivně	definitivně	k6eAd1
vybrala	vybrat	k5eAaPmAgFnS
prvního	první	k4xOgMnSc4
kosmonauta	kosmonaut	k1gMnSc2
Státní	státní	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pod	pod	k7c7
předsednictvím	předsednictví	k1gNnSc7
Konstantina	Konstantin	k1gMnSc2
Rudněva	Rudněv	k1gMnSc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
12	#num#	k4
<g/>
]	]	kIx)
až	až	k6eAd1
na	na	k7c6
Bajkonuru	Bajkonur	k1gInSc6
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
<g/>
,	,	kIx,
Titov	Titov	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
náhradníkem	náhradník	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
Kamanin	Kamanin	k2eAgInSc4d1
s	s	k7c7
rozhodnutím	rozhodnutí	k1gNnSc7
komise	komise	k1gFnSc1
seznámil	seznámit	k5eAaPmAgMnS
kosmonauty	kosmonaut	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebylo	být	k5eNaImAgNnS
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
překvapivé	překvapivý	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
pouze	pouze	k6eAd1
potvrdilo	potvrdit	k5eAaPmAgNnS
pořadí	pořadí	k1gNnSc1
známé	známý	k2eAgNnSc1d1
už	už	k6eAd1
od	od	k7c2
ledna	leden	k1gInSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
přesto	přesto	k8xC
Gagarin	Gagarin	k1gInSc1
reagoval	reagovat	k5eAaBmAgInS
radostně	radostně	k6eAd1
a	a	k8xC
Titov	Titov	k1gInSc4
poněkud	poněkud	k6eAd1
rozmrzele	rozmrzele	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Večer	večer	k6eAd1
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
proběhlo	proběhnout	k5eAaPmAgNnS
slavnostní	slavnostní	k2eAgNnSc1d1
zasedání	zasedání	k1gNnSc1
komise	komise	k1gFnSc2
pro	pro	k7c4
kamery	kamera	k1gFnPc4
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
šestice	šestice	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
a	a	k8xC
mnoha	mnoho	k4c2
dalších	další	k2eAgMnPc2d1
činitelů	činitel	k1gMnPc2
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
přítomno	přítomno	k1gNnSc1
na	na	k7c4
70	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
něm	on	k3xPp3gMnSc6
komise	komise	k1gFnSc1
formálně	formálně	k6eAd1
odsouhlasila	odsouhlasit	k5eAaPmAgFnS
start	start	k1gInSc4
Vostoku	Vostok	k1gInSc2
a	a	k8xC
potvrdila	potvrdit	k5eAaPmAgFnS
výběr	výběr	k1gInSc4
Gagarina	Gagarino	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
Titovem	Titov	k1gInSc7
v	v	k7c6
záloze	záloha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
úsvitem	úsvit	k1gInSc7
technici	technik	k1gMnPc1
vyvezli	vyvézt	k5eAaPmAgMnP
raketu	raketa	k1gFnSc4
na	na	k7c4
start	start	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopoledne	dopoledne	k6eAd1
si	se	k3xPyFc3
kosmonauti	kosmonaut	k1gMnPc1
poslechli	poslechnout	k5eAaPmAgMnP
poslední	poslední	k2eAgFnPc4d1
instrukce	instrukce	k1gFnPc4
konstruktéra	konstruktér	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
Feoktistova	Feoktistův	k2eAgFnSc1d1
<g/>
,	,	kIx,
odpoledne	odpoledne	k6eAd1
se	se	k3xPyFc4
Gagarin	Gagarin	k1gInSc1
setkal	setkat	k5eAaPmAgInS
s	s	k7c7
vojáky	voják	k1gMnPc7
a	a	k8xC
důstojníky	důstojník	k1gMnPc7
sloužícími	sloužící	k2eAgMnPc7d1
na	na	k7c6
kosmodromu	kosmodrom	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Noc	noc	k1gFnSc1
Gagarin	Gagarin	k1gInSc1
s	s	k7c7
Titovem	Titov	k1gInSc7
strávili	strávit	k5eAaPmAgMnP
v	v	k7c6
dřevěném	dřevěný	k2eAgInSc6d1
domku	domek	k1gInSc6
na	na	k7c6
základně	základna	k1gFnSc6
<g/>
,	,	kIx,
usnout	usnout	k5eAaPmF
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Let	let	k1gInSc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Vostok	Vostok	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dráha	dráha	k1gFnSc1
Gagarinovy	Gagarinův	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Vostok	Vostok	k1gInSc1
1	#num#	k4
</s>
<s>
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
vstávali	vstávat	k5eAaImAgMnP
kosmonauti	kosmonaut	k1gMnPc1
v	v	k7c6
5	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
30	#num#	k4
minut	minuta	k1gFnPc2
moskevského	moskevský	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snídani	snídaně	k1gFnSc3
z	z	k7c2
tub	tuba	k1gFnPc2
dostali	dostat	k5eAaPmAgMnP
v	v	k7c6
montážní	montážní	k2eAgFnSc6d1
budově	budova	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
ní	on	k3xPp3gFnSc6
následovala	následovat	k5eAaImAgFnS
krátká	krátká	k1gFnSc1
zdravotní	zdravotní	k2eAgFnSc1d1
prohlídka	prohlídka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Deset	deset	k4xCc1
minut	minuta	k1gFnPc2
před	před	k7c7
sedmou	sedma	k1gFnSc7
přijeli	přijet	k5eAaPmAgMnP
Gagarin	Gagarin	k1gInSc4
a	a	k8xC
Titov	Titov	k1gInSc1
<g/>
,	,	kIx,
oba	dva	k4xCgInPc1
ve	v	k7c6
skafandrech	skafandr	k1gInPc6
<g/>
,	,	kIx,
na	na	k7c4
startovní	startovní	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
hodiny	hodina	k1gFnPc1
před	před	k7c7
startem	start	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
7	#num#	k4
hodin	hodina	k1gFnPc2
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
už	už	k6eAd1
byl	být	k5eAaImAgInS
Gagarin	Gagarin	k1gInSc1
usazen	usazen	k2eAgInSc1d1
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
lodi	loď	k1gFnSc6
a	a	k8xC
navázal	navázat	k5eAaPmAgMnS
spojení	spojení	k1gNnSc4
s	s	k7c7
řídícím	řídící	k2eAgInSc7d1
centrem	centr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používal	používat	k5eAaImAgInS
přidělený	přidělený	k2eAgInSc1d1
volací	volací	k2eAgInSc1d1
znak	znak	k1gInSc1
Kedr	Kedra	k1gFnPc2
(	(	kIx(
<g/>
К	К	k?
<g/>
,	,	kIx,
limba	limba	k1gFnSc1
nebo	nebo	k8xC
cedr	cedr	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
řídící	řídící	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
bylo	být	k5eAaImAgNnS
Zarja	Zarja	k1gFnSc1
(	(	kIx(
<g/>
З	З	k?
<g/>
,	,	kIx,
svítání	svítání	k1gNnSc2
či	či	k8xC
záře	zář	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
prověření	prověření	k1gNnSc6
spojení	spojení	k1gNnSc2
a	a	k8xC
přístrojů	přístroj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
trvalo	trvat	k5eAaImAgNnS
20	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
Gagarin	Gagarin	k1gInSc1
už	už	k6eAd1
jen	jen	k9
čekal	čekat	k5eAaImAgMnS
na	na	k7c4
start	start	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
udržování	udržování	k1gNnSc6
kontaktu	kontakt	k1gInSc2
s	s	k7c7
kosmonautem	kosmonaut	k1gMnSc7
se	se	k3xPyFc4
střídali	střídat	k5eAaImAgMnP
Kamanin	Kamanina	k1gFnPc2
<g/>
,	,	kIx,
Koroljov	Koroljov	k1gInSc1
a	a	k8xC
Popovič	Popovič	k1gInSc1
<g/>
,	,	kIx,
pro	pro	k7c4
ukrácení	ukrácení	k1gNnSc4
čekání	čekání	k1gNnSc6
mu	on	k3xPp3gMnSc3
pouštěli	pouštět	k5eAaImAgMnP
do	do	k7c2
sluchátek	sluchátko	k1gNnPc2
písničky	písnička	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
v	v	k7c4
9	#num#	k4
hodin	hodina	k1gFnPc2
7	#num#	k4
minut	minuta	k1gFnPc2
moskevského	moskevský	k2eAgInSc2d1
času	čas	k1gInSc2
(	(	kIx(
<g/>
6	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
7	#num#	k4
minut	minuta	k1gFnPc2
světového	světový	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
)	)	kIx)
odstartoval	odstartovat	k5eAaPmAgMnS
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc4
v	v	k7c6
kosmické	kosmický	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
Vostok	Vostok	k1gInSc1
1	#num#	k4
z	z	k7c2
kosmodromu	kosmodrom	k1gInSc2
Bajkonur	Bajkonura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
startu	start	k1gInSc6
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
27	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
П	П	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
–	–	k?
Jedeme	jet	k5eAaImIp1nP
<g/>
!	!	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Gagarin	Gagarin	k1gInSc1
při	při	k7c6
zážehu	zážeh	k1gInSc6
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přistávací	přistávací	k2eAgInSc4d1
modul	modul	k1gInSc4
lodi	loď	k1gFnSc2
Vostok	Vostok	k1gInSc1
1	#num#	k4
v	v	k7c6
muzeu	muzeum	k1gNnSc6
společnosti	společnost	k1gFnSc2
RKK	RKK	kA
Eněrgija	Eněrgija	k1gFnSc1
v	v	k7c4
Koroljovu	Koroljův	k2eAgFnSc4d1
</s>
<s>
Asi	asi	k9
minutu	minuta	k1gFnSc4
po	po	k7c6
startu	start	k1gInSc6
dosáhlo	dosáhnout	k5eAaPmAgNnS
přetížení	přetížení	k1gNnSc1
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
G	G	kA
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
snižovalo	snižovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
je	být	k5eAaImIp3nS
snášel	snášet	k5eAaImAgMnS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
puls	puls	k1gInSc4
mu	on	k3xPp3gMnSc3
vzrostl	vzrůst	k5eAaPmAgMnS
z	z	k7c2
klidných	klidný	k2eAgNnPc2d1
64	#num#	k4
na	na	k7c4
150	#num#	k4
úderů	úder	k1gInPc2
za	za	k7c4
minutu	minuta	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Motor	motor	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
rakety	raketa	k1gFnSc2
(	(	kIx(
<g/>
blok	blok	k1gInSc1
A	A	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
vypnul	vypnout	k5eAaPmAgInS
později	pozdě	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
plánováno	plánovat	k5eAaImNgNnS
<g/>
;	;	kIx,
Vostok	Vostok	k1gInSc1
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
proto	proto	k8xC
nabral	nabrat	k5eAaPmAgInS
vyšší	vysoký	k2eAgFnSc4d2
rychlost	rychlost	k1gFnSc4
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
místo	místo	k7c2
plánovaného	plánovaný	k2eAgInSc2d1
letu	let	k1gInSc2
ve	v	k7c6
výšce	výška	k1gFnSc6
180	#num#	k4
<g/>
–	–	k?
<g/>
230	#num#	k4
km	km	kA
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
nad	nad	k7c7
povrchem	povrch	k1gInSc7
Země	zem	k1gFnSc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
na	na	k7c4
dráhu	dráha	k1gFnSc4
s	s	k7c7
parametry	parametr	k1gInPc7
181	#num#	k4
<g/>
–	–	k?
<g/>
327	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
13	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
8	#num#	k4
minutách	minuta	k1gFnPc6
36	#num#	k4
sekundách	sekunda	k1gFnPc6
letu	let	k1gInSc2
dohořel	dohořet	k5eAaPmAgInS
motor	motor	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
se	se	k3xPyFc4
oddělil	oddělit	k5eAaPmAgMnS
3	#num#	k4
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
rakety	raketa	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
Gagarin	Gagarin	k1gInSc1
pocítil	pocítit	k5eAaPmAgInS
stav	stav	k1gInSc4
beztíže	beztíže	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Loď	loď	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
letěla	letět	k5eAaImAgFnS
automaticky	automaticky	k6eAd1
<g/>
,	,	kIx,
kosmonaut	kosmonaut	k1gMnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
letu	let	k1gInSc2
udržoval	udržovat	k5eAaImAgInS
spojení	spojení	k1gNnSc4
s	s	k7c7
řídícím	řídící	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
<g/>
,	,	kIx,
zapisoval	zapisovat	k5eAaImAgMnS
svá	svůj	k3xOyFgNnPc4
pozorování	pozorování	k1gNnPc4
a	a	k8xC
pocity	pocit	k1gInPc4
(	(	kIx(
<g/>
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
letu	let	k1gInSc2
se	se	k3xPyFc4
cítil	cítit	k5eAaImAgMnS
výborně	výborně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gNnSc3
uplavala	uplavat	k5eAaPmAgFnS
tužka	tužka	k1gFnSc1
<g/>
,	,	kIx,
diktoval	diktovat	k5eAaImAgMnS
na	na	k7c4
záznam	záznam	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
57	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
loď	loď	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
maximální	maximální	k2eAgFnPc4d1
výšky	výška	k1gFnPc4
327	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
trochu	trochu	k6eAd1
pojedl	pojíst	k5eAaPmAgInS
<g/>
,	,	kIx,
napil	napít	k5eAaPmAgMnS,k5eAaBmAgMnS
se	se	k3xPyFc4
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
chystat	chystat	k5eAaImF
na	na	k7c4
návrat	návrat	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sestup	sestup	k1gInSc1
byl	být	k5eAaImAgInS
komplikovaný	komplikovaný	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
10.25	10.25	k4
motor	motor	k1gInSc1
Vostoku	Vostok	k1gInSc2
zbrzdil	zbrzdit	k5eAaPmAgInS
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
nepracoval	pracovat	k5eNaImAgMnS
však	však	k9
plánovanou	plánovaný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
a	a	k8xC
řídící	řídící	k2eAgInSc4d1
systém	systém	k1gInSc4
lodi	loď	k1gFnSc2
proto	proto	k8xC
neoddělil	oddělit	k5eNaPmAgMnS
přístrojový	přístrojový	k2eAgInSc4d1
úsek	úsek	k1gInSc4
od	od	k7c2
kabiny	kabina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Loď	loď	k1gFnSc1
navíc	navíc	k6eAd1
začala	začít	k5eAaPmAgFnS
rotovat	rotovat	k5eAaImF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
Gagarin	Gagarin	k1gInSc4
oznámil	oznámit	k5eAaPmAgMnS
na	na	k7c4
Zem	zem	k1gFnSc4
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
mu	on	k3xPp3gMnSc3
však	však	k9
nemohl	moct	k5eNaImAgInS
pomoci	pomoct	k5eAaPmF
ani	ani	k9
radou	rada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
10	#num#	k4
minutách	minuta	k1gFnPc6
automatika	automatika	k1gFnSc1
přístrojový	přístrojový	k2eAgInSc4d1
úsek	úsek	k1gInSc4
konečně	konečně	k6eAd1
odstřelila	odstřelit	k5eAaPmAgFnS
<g/>
,	,	kIx,
loď	loď	k1gFnSc1
se	se	k3xPyFc4
stočila	stočit	k5eAaPmAgFnS
potřebným	potřebný	k2eAgInSc7d1
směrem	směr	k1gInSc7
a	a	k8xC
rotace	rotace	k1gFnSc1
ustala	ustat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Pak	pak	k6eAd1
sestup	sestup	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
normálně	normálně	k6eAd1
<g/>
,	,	kIx,
teplota	teplota	k1gFnSc1
ochranného	ochranný	k2eAgInSc2d1
štítu	štít	k1gInSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
2000	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
přetížení	přetížení	k1gNnSc2
10	#num#	k4
G.	G.	kA
Ve	v	k7c6
výšce	výška	k1gFnSc6
7	#num#	k4
km	km	kA
se	se	k3xPyFc4
odstřelil	odstřelit	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
poklop	poklop	k1gInSc1
průlezu	průlez	k1gInSc2
<g/>
,	,	kIx,
pilot	pilot	k1gInSc1
byl	být	k5eAaImAgInS
katapultován	katapultovat	k5eAaBmNgInS
a	a	k8xC
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
na	na	k7c6
padáku	padák	k1gInSc6
vedle	vedle	k7c2
kabiny	kabina	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
10.55	10.55	k4
nebo	nebo	k8xC
<g/>
,	,	kIx,
podle	podle	k7c2
jiných	jiný	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
10.53	10.53	k4
moskevského	moskevský	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
,	,	kIx,
mezitím	mezitím	k6eAd1
v	v	k7c6
10.48	10.48	k4
dopadla	dopadnout	k5eAaPmAgFnS
na	na	k7c4
zem	zem	k1gFnSc4
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
důsledku	důsledek	k1gInSc6
výše	výše	k1gFnSc2,k1gFnSc2wB
zmíněných	zmíněný	k2eAgFnPc2d1
odchylek	odchylka	k1gFnPc2
v	v	k7c6
práci	práce	k1gFnSc6
motorů	motor	k1gInPc2
loď	loď	k1gFnSc1
nedoletěla	doletět	k5eNaPmAgFnS
do	do	k7c2
plánované	plánovaný	k2eAgFnSc2d1
přistávací	přistávací	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
jižně	jižně	k6eAd1
od	od	k7c2
Kujbyševa	Kujbyšev	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
přistála	přistát	k5eAaImAgFnS,k5eAaPmAgFnS
u	u	k7c2
vesnice	vesnice	k1gFnSc2
Smelovka	Smelovka	k1gFnSc1
ležící	ležící	k2eAgFnSc1d1
nedaleko	nedaleko	k7c2
města	město	k1gNnSc2
Engels	Engels	k1gMnSc1
v	v	k7c6
Saratovské	Saratovský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
místo	místo	k1gNnSc1
přistání	přistání	k1gNnSc2
leží	ležet	k5eAaImIp3nS
na	na	k7c4
západ	západ	k1gInSc4
od	od	k7c2
kosmodromu	kosmodrom	k1gInSc2
Bajkonur	Bajkonura	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Vostok	Vostok	k1gInSc1
1	#num#	k4
vzlétl	vzlétnout	k5eAaPmAgInS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
,	,	kIx,
neuskutečnil	uskutečnit	k5eNaPmAgInS
Gagarin	Gagarin	k1gInSc1
kompletní	kompletní	k2eAgInSc4d1
oblet	oblet	k1gInSc4
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
kosmický	kosmický	k2eAgInSc1d1
let	let	k1gInSc1
trval	trvat	k5eAaImAgInS
108	#num#	k4
(	(	kIx(
<g/>
nebo	nebo	k8xC
106	#num#	k4
<g/>
)	)	kIx)
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
nejen	nejen	k6eAd1
první	první	k4xOgNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
(	(	kIx(
<g/>
ještě	ještě	k6eAd1
ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
)	)	kIx)
i	i	k8xC
nejkratší	krátký	k2eAgInSc1d3
orbitální	orbitální	k2eAgInSc1d1
kosmický	kosmický	k2eAgInSc1d1
let	let	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Když	když	k8xS
jsem	být	k5eAaImIp1nS
v	v	k7c6
kosmické	kosmický	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
obletěl	obletět	k5eAaPmAgMnS
Zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
viděl	vidět	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
naše	náš	k3xOp1gFnSc1
planeta	planeta	k1gFnSc1
krásná	krásný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
chraňme	chránit	k5eAaImRp1nP
a	a	k8xC
rozmnožujme	rozmnožovat	k5eAaImRp1nP
tuto	tento	k3xDgFnSc4
krásu	krása	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
neničme	ničit	k5eNaImRp1nP
ji	on	k3xPp3gFnSc4
<g/>
!	!	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Gagarinova	Gagarinův	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
po	po	k7c6
letu	let	k1gInSc6
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
letu	let	k1gInSc6
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Gagarin	Gagarin	k1gInSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
pozoruje	pozorovat	k5eAaImIp3nS
vojenskou	vojenský	k2eAgFnSc4d1
přehlídku	přehlídka	k1gFnSc4
v	v	k7c6
Egyptě	Egypt	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
</s>
<s>
Gagarin	Gagarin	k1gInSc1
při	při	k7c6
své	svůj	k3xOyFgFnSc6
návštěvě	návštěva	k1gFnSc6
ČSSR	ČSSR	kA
</s>
<s>
Po	po	k7c6
úspěšném	úspěšný	k2eAgInSc6d1
kosmickém	kosmický	k2eAgInSc6d1
letu	let	k1gInSc6
SSSR	SSSR	kA
požádal	požádat	k5eAaPmAgInS
Mezinárodní	mezinárodní	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
federaci	federace	k1gFnSc4
o	o	k7c4
uznání	uznání	k1gNnSc4
leteckých	letecký	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
<g/>
,	,	kIx,
délky	délka	k1gFnSc2
letu	let	k1gInSc2
40	#num#	k4
868,6	868,6	k4
km	km	kA
<g/>
,	,	kIx,
maximální	maximální	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
letu	let	k1gInSc2
28	#num#	k4
260	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
hod	hod	k1gInSc1
a	a	k8xC
výšky	výška	k1gFnSc2
letu	let	k1gInSc2
327	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
regule	regule	k1gFnSc1
uznání	uznání	k1gNnSc2
rekordu	rekord	k1gInSc2
vyžadovaly	vyžadovat	k5eAaImAgFnP
přistání	přistání	k1gNnSc4
pilota	pilot	k1gMnSc2
na	na	k7c4
Zemi	zem	k1gFnSc4
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
stroji	stroj	k1gInSc6
<g/>
,	,	kIx,
zatajili	zatajit	k5eAaPmAgMnP
Sověti	Sovět	k1gMnPc1
katapultáž	katapultáž	k1gFnSc1
pilota	pilota	k1gFnSc1
a	a	k8xC
přistání	přistání	k1gNnSc1
na	na	k7c6
padáku	padák	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
Gagarin	Gagarin	k1gInSc1
byl	být	k5eAaImAgInS
donucen	donutit	k5eAaPmNgInS
nadřízenými	nadřízený	k2eAgInPc7d1
orgány	orgán	k1gInPc7
lhát	lhát	k5eAaImF
na	na	k7c6
první	první	k4xOgFnSc6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
sále	sál	k1gInSc6
Domu	dům	k1gInSc2
vědců	vědec	k1gMnPc2
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vynechat	vynechat	k5eAaPmF
tuto	tento	k3xDgFnSc4
informaci	informace	k1gFnSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Cesta	cesta	k1gFnSc1
do	do	k7c2
kosmu	kosmos	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
FAI	FAI	kA
rekordy	rekord	k1gInPc4
v	v	k7c6
květnu	květen	k1gInSc6
1961	#num#	k4
oficiálně	oficiálně	k6eAd1
zaregistrovala	zaregistrovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uskutečnění	uskutečnění	k1gNnSc1
prvního	první	k4xOgNnSc2
kosmického	kosmický	k2eAgNnSc2d1
letu	let	k1gInSc3
člověka	člověk	k1gMnSc4
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
vyvolalo	vyvolat	k5eAaPmAgNnS
překvapení	překvapení	k1gNnSc1
nemenší	malý	k2eNgNnSc1d2
než	než	k8xS
při	při	k7c6
prvním	první	k4xOgInSc6
kosmickém	kosmický	k2eAgInSc6d1
letu	let	k1gInSc6
vůbec	vůbec	k9
<g/>
,	,	kIx,
také	také	k9
sovětském	sovětský	k2eAgInSc6d1
Sputniku	sputnik	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
John	John	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
Kennedy	Kenneda	k1gMnSc2
reagoval	reagovat	k5eAaBmAgInS
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1961	#num#	k4
vyhlášením	vyhlášení	k1gNnSc7
nového	nový	k2eAgInSc2d1
ambiciózního	ambiciózní	k2eAgInSc2d1
cíle	cíl	k1gInSc2
amerického	americký	k2eAgInSc2d1
kosmického	kosmický	k2eAgInSc2d1
programu	program	k1gInSc2
–	–	k?
letu	let	k1gInSc2
člověka	člověk	k1gMnSc2
na	na	k7c4
Měsíc	měsíc	k1gInSc4
do	do	k7c2
konce	konec	k1gInSc2
desetiletí	desetiletí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
neznámého	známý	k2eNgMnSc2d1
vojenského	vojenský	k2eAgMnSc2d1
letce	letec	k1gMnSc2
se	se	k3xPyFc4
během	během	k7c2
hodiny	hodina	k1gFnSc2
stal	stát	k5eAaPmAgMnS
nejpopulárnější	populární	k2eAgMnSc1d3
člověk	člověk	k1gMnSc1
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětská	sovětský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
toho	ten	k3xDgMnSc4
samozřejmě	samozřejmě	k6eAd1
náležitě	náležitě	k6eAd1
využila	využít	k5eAaPmAgFnS
k	k	k7c3
propagačním	propagační	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc4
byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
z	z	k7c2
nadporučíka	nadporučík	k1gMnSc2
(	(	kIx(
<g/>
с	с	k?
л	л	k?
<g/>
)	)	kIx)
na	na	k7c4
majora	major	k1gMnSc4
(	(	kIx(
<g/>
м	м	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vyznamenán	vyznamenán	k2eAgInSc1d1
titulem	titul	k1gInSc7
hrdina	hrdina	k1gMnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
Leninovým	Leninův	k2eAgInSc7d1
řádem	řád	k1gInSc7
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dostalo	dostat	k5eAaPmAgNnS
triumfálního	triumfální	k2eAgNnSc2d1
přijetí	přijetí	k1gNnSc3
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
a	a	k8xC
v	v	k7c6
následujících	následující	k2eAgInPc6d1
dnech	den	k1gInPc6
a	a	k8xC
měsících	měsíc	k1gInPc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
od	od	k7c2
Londýna	Londýn	k1gInSc2
až	až	k9
po	po	k7c6
Havanu	havano	k1gNnSc6
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
bez	bez	k7c2
zajímavosti	zajímavost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
Gagarinova	Gagarinův	k2eAgFnSc1d1
vůbec	vůbec	k9
první	první	k4xOgFnSc1
zahraniční	zahraniční	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
vedla	vést	k5eAaImAgFnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československo	Československo	k1gNnSc1
navštívil	navštívit	k5eAaPmAgMnS
pak	pak	k6eAd1
ještě	ještě	k6eAd1
roku	rok	k1gInSc2
1966	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Šarm	šarm	k1gInSc1
<g/>
,	,	kIx,
neustálý	neustálý	k2eAgInSc1d1
úsměv	úsměv	k1gInSc1
a	a	k8xC
přirozená	přirozený	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
mu	on	k3xPp3gMnSc3
na	na	k7c6
cestách	cesta	k1gFnPc6
získaly	získat	k5eAaPmAgFnP
sympatie	sympatie	k1gFnPc1
lidí	člověk	k1gMnPc2
a	a	k8xC
přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
popularitě	popularita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Gagarin	Gagarin	k1gInSc1
a	a	k8xC
Valentina	Valentina	k1gFnSc1
Těreškovová	Těreškovový	k2eAgFnSc1d1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Postavení	postavení	k1gNnSc1
celebrity	celebrita	k1gFnSc2
ovlivnilo	ovlivnit	k5eAaPmAgNnS
i	i	k9
Gagarinovo	Gagarinův	k2eAgNnSc1d1
chování	chování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nekonečná	konečný	k2eNgFnSc1d1
řada	řada	k1gFnSc1
setkání	setkání	k1gNnSc2
a	a	k8xC
banketů	banket	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
si	se	k3xPyFc3
kdekdo	kdekdo	k3yInSc1
chtěl	chtít	k5eAaImAgMnS
se	s	k7c7
slavným	slavný	k2eAgMnSc7d1
hostem	host	k1gMnSc7
připít	připít	k5eAaPmF
„	„	k?
<g/>
až	až	k9
do	do	k7c2
dna	dno	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
nezůstaly	zůstat	k5eNaPmAgInP
bez	bez	k7c2
následků	následek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
přestal	přestat	k5eAaPmAgInS
sportovat	sportovat	k5eAaImF
<g/>
,	,	kIx,
přibral	přibrat	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
namísto	namísto	k7c2
létání	létání	k1gNnSc2
se	se	k3xPyFc4
proháněl	prohánět	k5eAaImAgInS
autem	auto	k1gNnSc7
po	po	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Vážný	vážný	k2eAgInSc1d1
incident	incident	k1gInSc1
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
v	v	k7c6
září	září	k1gNnSc6
1961	#num#	k4
při	při	k7c6
dovolené	dovolená	k1gFnSc6
ve	v	k7c6
Forosu	Foros	k1gInSc6
na	na	k7c6
Krymu	Krym	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flirt	flirt	k1gInSc1
se	s	k7c7
zdravotní	zdravotní	k2eAgFnSc7d1
sestrou	sestra	k1gFnSc7
skončil	skončit	k5eAaPmAgMnS
skokem	skok	k1gInSc7
z	z	k7c2
balkónu	balkón	k1gInSc2
<g/>
,	,	kIx,
rozbitou	rozbitý	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
a	a	k8xC
měsíčním	měsíční	k2eAgInSc7d1
pobytem	pobyt	k1gInSc7
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Manželce	manželka	k1gFnSc6
poté	poté	k6eAd1
zůstal	zůstat	k5eAaPmAgMnS
věrný	věrný	k2eAgMnSc1d1
<g/>
,	,	kIx,
ostatně	ostatně	k6eAd1
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
ho	on	k3xPp3gMnSc4
pozorně	pozorně	k6eAd1
střežila	střežit	k5eAaImAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
problémy	problém	k1gInPc1
s	s	k7c7
rychlými	rychlý	k2eAgInPc7d1
auty	aut	k1gInPc7
a	a	k8xC
alkoholem	alkohol	k1gInSc7
(	(	kIx(
<g/>
ve	v	k7c6
volném	volný	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
ve	v	k7c6
službě	služba	k1gFnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
táhly	táhnout	k5eAaImAgInP
ještě	ještě	k6eAd1
několik	několik	k4yIc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k9
populární	populární	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
byl	být	k5eAaImAgMnS
v	v	k7c6
březnu	březen	k1gInSc6
1962	#num#	k4
za	za	k7c4
rodnou	rodný	k2eAgFnSc4d1
Smolenskou	Smolenský	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
zvolen	zvolit	k5eAaPmNgMnS
do	do	k7c2
Sovětu	sovět	k1gInSc2
svazu	svaz	k1gInSc2
Nejvyššího	vysoký	k2eAgInSc2d3
sovětu	sovět	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
po	po	k7c6
dalších	další	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
přešel	přejít	k5eAaPmAgMnS
do	do	k7c2
Sovětu	sovět	k1gInSc2
národností	národnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Současně	současně	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
sjezdu	sjezd	k1gInSc2
Komsomolu	Komsomol	k1gInSc2
(	(	kIx(
<g/>
1962	#num#	k4
a	a	k8xC
1966	#num#	k4
<g/>
)	)	kIx)
stal	stát	k5eAaPmAgInS
členem	člen	k1gInSc7
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
ústředního	ústřední	k2eAgInSc2d1
výboru	výbor	k1gInSc2
(	(	kIx(
<g/>
aktivním	aktivní	k2eAgInSc7d1
členem	člen	k1gInSc7
Komsomolu	Komsomol	k1gInSc2
byl	být	k5eAaImAgInS
už	už	k6eAd1
v	v	k7c6
saratovské	saratovský	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaujal	zaujmout	k5eAaPmAgMnS
také	také	k9
funkci	funkce	k1gFnSc4
předsedy	předseda	k1gMnSc2
Společnosti	společnost	k1gFnSc2
sovětsko-kubánského	sovětsko-kubánský	k2eAgNnSc2d1
přátelství	přátelství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přes	přes	k7c4
značnou	značný	k2eAgFnSc4d1
zátěž	zátěž	k1gFnSc4
veřejných	veřejný	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
zůstal	zůstat	k5eAaPmAgInS
Gagarin	Gagarin	k1gInSc1
kosmonautem	kosmonaut	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
vedoucí	vedoucí	k1gFnSc1
oddílu	oddíl	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
od	od	k7c2
ledna	leden	k1gInSc2
1963	#num#	k4
a	a	k8xC
zástupce	zástupce	k1gMnSc1
velitele	velitel	k1gMnSc2
CPK	CPK	kA
pro	pro	k7c4
leteckou	letecký	k2eAgFnSc4d1
a	a	k8xC
kosmickou	kosmický	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
od	od	k7c2
prosince	prosinec	k1gInSc2
1963	#num#	k4
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
výcviku	výcvik	k1gInSc6
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
velmi	velmi	k6eAd1
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
účast	účast	k1gFnSc4
na	na	k7c6
dalších	další	k2eAgNnPc6d1
kosmických	kosmický	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
pak	pak	k6eAd1
programu	program	k1gInSc2
letu	let	k1gInSc2
na	na	k7c4
Měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1965	#num#	k4
byl	být	k5eAaImAgMnS
zařazen	zařadit	k5eAaPmNgMnS
do	do	k7c2
skupiny	skupina	k1gFnSc2
určené	určený	k2eAgFnPc1d1
pro	pro	k7c4
nácvik	nácvik	k1gInSc4
spojení	spojení	k1gNnSc4
kosmických	kosmický	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Sojuz	Sojuz	k1gInSc1
jako	jako	k8xC,k8xS
náhradník	náhradník	k1gMnSc1
velitele	velitel	k1gMnSc2
aktivní	aktivní	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Vladimira	Vladimiro	k1gNnSc2
Komarova	Komarův	k2eAgFnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
perspektivou	perspektiva	k1gFnSc7
opětného	opětný	k2eAgInSc2d1
letu	let	k1gInSc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
opětném	opětný	k2eAgNnSc6d1
zařazení	zařazení	k1gNnSc6
do	do	k7c2
přípravy	příprava	k1gFnSc2
k	k	k7c3
letu	let	k1gInSc3
zlepšil	zlepšit	k5eAaPmAgMnS
i	i	k9
své	svůj	k3xOyFgNnSc4
chování	chování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
obvyklou	obvyklý	k2eAgFnSc7d1
důkladností	důkladnost	k1gFnSc7
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgInS
do	do	k7c2
výcviku	výcvik	k1gInSc2
<g/>
,	,	kIx,
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
ke	k	k7c3
sportu	sport	k1gInSc2
<g/>
,	,	kIx,
zhubl	zhubnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
rozdělení	rozdělení	k1gNnSc6
kosmonautů	kosmonaut	k1gMnPc2
do	do	k7c2
skupin	skupina	k1gFnPc2
podle	podle	k7c2
programů	program	k1gInPc2
v	v	k7c6
září	září	k1gNnSc6
1966	#num#	k4
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
skupiny	skupina	k1gFnSc2
připravující	připravující	k2eAgFnSc2d1
se	se	k3xPyFc4
na	na	k7c4
přistání	přistání	k1gNnSc4
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
Komarova	Komarův	k2eAgFnSc1d1
při	při	k7c6
neúspěšném	úspěšný	k2eNgInSc6d1
letu	let	k1gInSc6
Sojuzu	Sojuz	k1gInSc2
1	#num#	k4
v	v	k7c6
dubnu	duben	k1gInSc6
1967	#num#	k4
byl	být	k5eAaImAgMnS
z	z	k7c2
opatrnosti	opatrnost	k1gFnSc2
odstraněn	odstranit	k5eAaPmNgInS
z	z	k7c2
přípravy	příprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
září	září	k1gNnSc2
1961	#num#	k4
Gagarin	Gagarin	k1gInSc1
společně	společně	k6eAd1
s	s	k7c7
kolegy	kolega	k1gMnPc7
z	z	k7c2
oddílu	oddíl	k1gInSc2
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
Žukovského	Žukovského	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
inženýrské	inženýrský	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
vytvořili	vytvořit	k5eAaPmAgMnP
novu	nova	k1gFnSc4
specializaci	specializace	k1gFnSc3
pilot-inženýr-kosmonaut	pilot-inženýr-kosmonaut	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
1965	#num#	k4
kosmonauti	kosmonaut	k1gMnPc1
pracovali	pracovat	k5eAaImAgMnP
na	na	k7c6
komplexní	komplexní	k2eAgFnSc6d1
diplomové	diplomový	k2eAgFnSc6d1
práci	práce	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
zkoumali	zkoumat	k5eAaImAgMnP
možnost	možnost	k1gFnSc4
postavení	postavení	k1gNnSc2
opakovaně	opakovaně	k6eAd1
použitelné	použitelný	k2eAgFnSc3d1
okřídlené	okřídlený	k2eAgFnPc4d1
kosmické	kosmický	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
části	část	k1gFnPc4
problematiky	problematika	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Gagarin	Gagarin	k1gInSc1
hrál	hrát	k5eAaImAgInS
ve	v	k7c6
skupině	skupina	k1gFnSc6
roli	role	k1gFnSc4
„	„	k?
<g/>
hlavního	hlavní	k2eAgMnSc2d1
konstruktéra	konstruktér	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
když	když	k8xS
určoval	určovat	k5eAaImAgInS
metodu	metoda	k1gFnSc4
využití	využití	k1gNnSc2
a	a	k8xC
vybíral	vybírat	k5eAaImAgMnS
celkový	celkový	k2eAgInSc4d1
tvar	tvar	k1gInSc4
letounu	letoun	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
trenažéru	trenažér	k1gInSc6
zkoušel	zkoušet	k5eAaImAgMnS
způsoby	způsob	k1gInPc4
přistání	přistání	k1gNnSc2
uvažovaného	uvažovaný	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Diplomovou	diplomový	k2eAgFnSc4d1
práci	práce	k1gFnSc4
obhájil	obhájit	k5eAaPmAgMnS
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1968	#num#	k4
<g/>
,	,	kIx,
o	o	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
ukončil	ukončit	k5eAaPmAgMnS
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
Busta	busta	k1gFnSc1
v	v	k7c6
Erfurtu	Erfurt	k1gInSc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Smrt	smrt	k1gFnSc1
Jurije	Jurije	k1gFnSc1
Gagarina	Gagarina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
školy	škola	k1gFnSc2
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
po	po	k7c6
několikaměsíční	několikaměsíční	k2eAgFnSc6d1
přestávce	přestávka	k1gFnSc6
(	(	kIx(
<g/>
od	od	k7c2
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1967	#num#	k4
<g/>
)	)	kIx)
vrátit	vrátit	k5eAaPmF
k	k	k7c3
létání	létání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cvičné	cvičný	k2eAgInPc4d1
lety	let	k1gInPc4
opět	opět	k6eAd1
zahájil	zahájit	k5eAaPmAgInS
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
a	a	k8xC
do	do	k7c2
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
provedl	provést	k5eAaPmAgMnS
18	#num#	k4
letů	let	k1gInPc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
něco	něco	k6eAd1
přes	přes	k7c4
7	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Dopoledne	dopoledne	k1gNnSc1
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
měl	mít	k5eAaImAgMnS
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc4
provést	provést	k5eAaPmF
poslední	poslední	k2eAgInSc4d1
let	let	k1gInSc4
s	s	k7c7
instruktorem	instruktor	k1gMnSc7
na	na	k7c6
cvičném	cvičný	k2eAgInSc6d1
MiG-	MiG-	k1gMnSc2
<g/>
15	#num#	k4
<g/>
UTI	UTI	kA
<g/>
,	,	kIx,
vyrobeném	vyrobený	k2eAgNnSc6d1
v	v	k7c6
Aeru	aero	k1gNnSc6
Vodochody	Vodochod	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
kontrolní	kontrolní	k2eAgInSc4d1
let	let	k1gInSc4
techniky	technika	k1gFnSc2
pilotáže	pilotáž	k1gFnSc2
před	před	k7c7
samostatnými	samostatný	k2eAgNnPc7d1
lety	léto	k1gNnPc7
na	na	k7c6
už	už	k6eAd1
připraveném	připravený	k2eAgInSc6d1
MiGu-	MiGu-	k1gMnSc7
<g/>
17	#num#	k4
<g/>
,	,	kIx,
první	první	k4xOgInPc4
dva	dva	k4xCgInPc4
měly	mít	k5eAaImAgFnP
následovat	následovat	k5eAaImF
už	už	k6eAd1
odpoledne	odpoledne	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
instruktorem	instruktor	k1gMnSc7
<g/>
,	,	kIx,
Vladimirem	Vladimir	k1gMnSc7
Serjoginem	Serjogin	k1gMnSc7
<g/>
,	,	kIx,
hrdinou	hrdina	k1gMnSc7
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
velitelem	velitel	k1gMnSc7
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
letu	let	k1gInSc6
zahynul	zahynout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Gagarin	Gagarin	k1gInSc1
se	s	k7c7
Serjoginem	Serjogin	k1gInSc7
vzlétli	vzlétnout	k5eAaPmAgMnP
v	v	k7c6
10	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
10	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
Gagarin	Gagarin	k1gInSc1
zahájil	zahájit	k5eAaPmAgInS
v	v	k7c6
určené	určený	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
ve	v	k7c6
výšce	výška	k1gFnSc6
4200	#num#	k4
m	m	kA
trénink	trénink	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
10.30	10.30	k4
ohlásil	ohlásit	k5eAaPmAgMnS
splnění	splnění	k1gNnSc3
úkolu	úkol	k1gInSc2
a	a	k8xC
zažádal	zažádat	k5eAaPmAgMnS
o	o	k7c4
povolení	povolení	k1gNnSc4
k	k	k7c3
návratu	návrat	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Řídící	řídící	k2eAgFnSc1d1
létání	létání	k1gNnSc4
návrat	návrat	k1gInSc1
bez	bez	k7c2
otázek	otázka	k1gFnPc2
povolil	povolit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
cvičení	cvičení	k1gNnSc1
mělo	mít	k5eAaImAgNnS
trvat	trvat	k5eAaImF
nejméně	málo	k6eAd3
20	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
se	se	k3xPyFc4
Gagarin	Gagarin	k1gInSc1
ani	ani	k8xC
Serjogin	Serjogin	k1gMnSc1
už	už	k6eAd1
neozvali	ozvat	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trosky	troska	k1gFnPc4
letounu	letoun	k1gInSc2
byly	být	k5eAaImAgFnP
nalezeny	nalezen	k2eAgFnPc1d1
o	o	k7c4
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
později	pozdě	k6eAd2
65	#num#	k4
km	km	kA
od	od	k7c2
letiště	letiště	k1gNnSc2
<g/>
,	,	kIx,
u	u	k7c2
vesnice	vesnice	k1gFnSc2
Novoselovo	Novoselův	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1
Gagarinova	Gagarinův	k2eAgInSc2d1
posledního	poslední	k2eAgInSc2d1
letu	let	k1gInSc2
šetřila	šetřit	k5eAaImAgFnS
vládní	vládní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
do	do	k7c2
poloviny	polovina	k1gFnSc2
července	červenec	k1gInSc2
nakupila	nakupit	k5eAaPmAgFnS
29	#num#	k4
svazků	svazek	k1gInPc2
výpovědí	výpověď	k1gFnPc2
a	a	k8xC
rozborů	rozbor	k1gInPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jednoznačně	jednoznačně	k6eAd1
určit	určit	k5eAaPmF
příčinu	příčina	k1gFnSc4
pádu	pád	k1gInSc2
letounu	letoun	k1gInSc2
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
komise	komise	k1gFnSc2
prosadilo	prosadit	k5eAaPmAgNnS
závěr	závěr	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
pravděpodobnou	pravděpodobný	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
neštěstí	neštěstí	k1gNnSc2
byl	být	k5eAaImAgInS
prudký	prudký	k2eAgInSc1d1
úhyb	úhyb	k1gInSc1
pilota	pilot	k1gMnSc2
před	před	k7c7
mrakem	mrak	k1gInSc7
nebo	nebo	k8xC
meteorologickým	meteorologický	k2eAgInSc7d1
balónem	balón	k1gInSc7
<g/>
,	,	kIx,
následovaný	následovaný	k2eAgInSc1d1
pádem	pád	k1gInSc7
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
vybrat	vybrat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Proti	proti	k7c3
němu	on	k3xPp3gInSc3
však	však	k9
protestovali	protestovat	k5eAaBmAgMnP
mnozí	mnohý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
68	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
kosmonauti	kosmonaut	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
15	#num#	k4
<g/>
]	]	kIx)
Závěry	závěr	k1gInPc1
komise	komise	k1gFnSc2
byly	být	k5eAaImAgInP
odtajněny	odtajnit	k5eAaPmNgInP
až	až	k6eAd1
v	v	k7c6
dubnu	duben	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
průběhu	průběh	k1gInSc6
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
po	po	k7c6
uvolnění	uvolnění	k1gNnSc6
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
tiskem	tisk	k1gInSc7
v	v	k7c6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
několik	několik	k4yIc4
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
pravděpodobných	pravděpodobný	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
snažících	snažící	k2eAgFnPc2d1
se	se	k3xPyFc4
vysvětlit	vysvětlit	k5eAaPmF
neštěstí	neštěstí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Člen	člen	k1gMnSc1
původní	původní	k2eAgFnSc2d1
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
Žukovského	Žukovského	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
Sergej	Sergej	k1gMnSc1
Belocerkovskij	Belocerkovskij	k1gMnSc1
a	a	k8xC
kosmonaut	kosmonaut	k1gMnSc1
Alexej	Alexej	k1gMnSc1
Leonov	Leonov	k1gInSc4
publikovali	publikovat	k5eAaBmAgMnP
roku	rok	k1gInSc2
1987	#num#	k4
rozbor	rozbor	k1gInSc4
nehody	nehoda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Uvedli	uvést	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
vyšetřování	vyšetřování	k1gNnSc1
roku	rok	k1gInSc2
1968	#num#	k4
vyloučilo	vyloučit	k5eAaPmAgNnS
negativní	negativní	k2eAgNnSc1d1
ovlivnění	ovlivnění	k1gNnSc1
posádky	posádka	k1gFnSc2
alkoholem	alkohol	k1gInSc7
nebo	nebo	k8xC
jinými	jiný	k2eAgFnPc7d1
drogami	droga	k1gFnPc7
<g/>
,	,	kIx,
zavrhlo	zavrhnout	k5eAaPmAgNnS
i	i	k9
možnost	možnost	k1gFnSc4
atentátu	atentát	k1gInSc2
nebo	nebo	k8xC
spiknutí	spiknutí	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Dospělo	dochvít	k5eAaPmAgNnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
všechny	všechen	k3xTgInPc1
systémy	systém	k1gInPc1
letadla	letadlo	k1gNnSc2
fungovaly	fungovat	k5eAaImAgInP
normálně	normálně	k6eAd1
a	a	k8xC
že	že	k8xS
nebyly	být	k5eNaImAgFnP
nalezeny	nalézt	k5eAaBmNgFnP,k5eAaPmNgFnP
žádné	žádný	k3yNgFnPc1
stopy	stopa	k1gFnPc1
po	po	k7c6
srážce	srážka	k1gFnSc6
s	s	k7c7
cizím	cizí	k2eAgNnSc7d1
tělesem	těleso	k1gNnSc7
(	(	kIx(
<g/>
meteorologickým	meteorologický	k2eAgInSc7d1
balónem	balón	k1gInSc7
<g/>
,	,	kIx,
ptákem	pták	k1gMnSc7
<g/>
,	,	kIx,
jiným	jiný	k2eAgNnSc7d1
letadlem	letadlo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
jejich	jejich	k3xOp3gInSc2
názoru	názor	k1gInSc2
se	se	k3xPyFc4
během	během	k7c2
návratu	návrat	k1gInSc2
na	na	k7c4
letiště	letiště	k1gNnPc4
letoun	letoun	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
vývrtky	vývrtka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vylétnutí	vylétnutí	k1gNnSc6
ze	z	k7c2
spodní	spodní	k2eAgFnSc2d1
základny	základna	k1gFnSc2
oblačnosti	oblačnost	k1gFnSc2
(	(	kIx(
<g/>
spodní	spodní	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
mraků	mrak	k1gInPc2
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
ve	v	k7c6
výšce	výška	k1gFnSc6
od	od	k7c2
500	#num#	k4
<g/>
–	–	k?
<g/>
600	#num#	k4
do	do	k7c2
1500	#num#	k4
m	m	kA
<g/>
,	,	kIx,
horní	horní	k2eAgMnSc1d1
mezi	mezi	k7c7
4500	#num#	k4
a	a	k8xC
5500	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
piloti	pilot	k1gMnPc1
pravděpodobně	pravděpodobně	k6eAd1
pokoušeli	pokoušet	k5eAaImAgMnP
letadlo	letadlo	k1gNnSc4
srovnat	srovnat	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
malé	malý	k2eAgFnSc3d1
výšce	výška	k1gFnSc3
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
to	ten	k3xDgNnSc1
už	už	k6eAd1
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
10.31	10.31	k4
narazili	narazit	k5eAaPmAgMnP
do	do	k7c2
země	zem	k1gFnSc2
rychlostí	rychlost	k1gFnSc7
190	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
684	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
vývrtky	vývrtka	k1gFnSc2
podle	podle	k7c2
Belocerkovského	Belocerkovský	k2eAgInSc2d1
a	a	k8xC
Leonova	Leonův	k2eAgInSc2d1
letadlo	letadlo	k1gNnSc1
upadlo	upadnout	k5eAaPmAgNnS
pod	pod	k7c7
vlivem	vliv	k1gInSc7
vnější	vnější	k2eAgFnSc2d1
příčiny	příčina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snad	snad	k9
se	se	k3xPyFc4
piloti	pilot	k1gMnPc1
snažili	snažit	k5eAaImAgMnP
vyhnout	vyhnout	k5eAaPmF
hejnu	hejno	k1gNnSc3
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
mraku	mrak	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
hejno	hejno	k1gNnSc4
či	či	k8xC
za	za	k7c4
balón	balón	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
vlétli	vlétnout	k5eAaPmAgMnP
do	do	k7c2
úplavu	úplav	k1gInSc2
za	za	k7c7
letounem	letoun	k1gInSc7
Suchoj	Suchoj	k1gInSc1
Su-	Su-	k1gFnSc2
<g/>
15	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
letadlo	letadlo	k1gNnSc4
srazil	srazit	k5eAaPmAgInS
silný	silný	k2eAgInSc1d1
poryv	poryv	k1gInSc1
větru	vítr	k1gInSc2
objevivší	objevivší	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
přicházející	přicházející	k2eAgFnSc7d1
studenou	studený	k2eAgFnSc7d1
frontou	fronta	k1gFnSc7
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
prostoru	prostor	k1gInSc6
Gagarinova	Gagarinův	k2eAgInSc2d1
a	a	k8xC
Serjoginova	Serjoginův	k2eAgInSc2d1
letu	let	k1gInSc2
byl	být	k5eAaImAgInS
letoun	letoun	k1gInSc1
Suchoj	Suchoj	k1gInSc1
Su-	Su-	k1gFnSc1
<g/>
15	#num#	k4
<g/>
,	,	kIx,
podal	podat	k5eAaPmAgInS
Leonov	Leonov	k1gInSc1
svědectví	svědectví	k1gNnSc2
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
po	po	k7c6
45	#num#	k4
letech	léto	k1gNnPc6
od	od	k7c2
havárie	havárie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
hypotéz	hypotéza	k1gFnPc2
se	se	k3xPyFc4
točí	točit	k5eAaImIp3nS
okolo	okolo	k7c2
selhání	selhání	k1gNnSc2
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Kuzněcov	Kuzněcov	k1gInSc4
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Střediska	středisko	k1gNnSc2
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
<g/>
,	,	kIx,
vysvětloval	vysvětlovat	k5eAaImAgMnS
nehodu	nehoda	k1gFnSc4
infarktem	infarkt	k1gInSc7
Serjogina	Serjogino	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
tělo	tělo	k1gNnSc1
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
navalit	navalit	k5eAaPmF
na	na	k7c4
řízení	řízení	k1gNnSc4
a	a	k8xC
znemožnit	znemožnit	k5eAaPmF
Gagarinovi	Gagarin	k1gMnSc3
zachránit	zachránit	k5eAaPmF
situaci	situace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Kosmonaut	kosmonaut	k1gMnSc1
Vladimir	Vladimir	k1gMnSc1
Aksjonov	Aksjonov	k1gInSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
možnou	možný	k2eAgFnSc4d1
ztrátu	ztráta	k1gFnSc4
orientace	orientace	k1gFnSc2
posádky	posádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
návratu	návrat	k1gInSc6
na	na	k7c4
letiště	letiště	k1gNnSc4
se	se	k3xPyFc4
Serjogin	Serjogin	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
rázně	rázně	k6eAd1
snížit	snížit	k5eAaPmF
výšku	výška	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nezorientoval	zorientovat	k5eNaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
mracích	mrak	k1gInPc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
spodní	spodní	k2eAgInSc4d1
okraj	okraj	k1gInSc4
se	se	k3xPyFc4
navíc	navíc	k6eAd1
nalézal	nalézat	k5eAaImAgMnS
níže	nízce	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
byli	být	k5eAaImAgMnP
piloti	pilot	k1gMnPc1
před	před	k7c7
letem	letem	k6eAd1
informováni	informovat	k5eAaBmNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
možná	možná	k9
i	i	k9
Gagarinova	Gagarinův	k2eAgMnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
pilotní	pilotní	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
při	při	k7c6
provádění	provádění	k1gNnSc6
zatáčky	zatáčka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
následné	následný	k2eAgNnSc4d1
podlehnutí	podlehnutí	k1gNnSc4
iluzi	iluze	k1gFnSc4
<g/>
,	,	kIx,
spojené	spojený	k2eAgFnPc4d1
se	se	k3xPyFc4
ztrátou	ztráta	k1gFnSc7
prostorové	prostorový	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Nezanedbatelný	zanedbatelný	k2eNgInSc1d1
význam	význam	k1gInSc1
při	při	k7c6
havárii	havárie	k1gFnSc6
mělo	mít	k5eAaImAgNnS
i	i	k9
porušování	porušování	k1gNnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
na	na	k7c6
letišti	letiště	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
vzhledem	vzhledem	k7c3
ke	k	k7c3
ztíženým	ztížený	k2eAgFnPc3d1
povětrnostním	povětrnostní	k2eAgFnPc3d1
podmínkám	podmínka	k1gFnPc3
neměl	mít	k5eNaImAgInS
být	být	k5eAaImF
let	let	k1gInSc4
vůbec	vůbec	k9
povolen	povolit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
tým	tým	k1gInSc4
vědců	vědec	k1gMnPc2
a	a	k8xC
leteckých	letecký	k2eAgMnPc2d1
odborníků	odborník	k1gMnPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
leteckého	letecký	k2eAgMnSc2d1
inženýra	inženýr	k1gMnSc2
Igora	Igor	k1gMnSc2
Kuzněcova	Kuzněcův	k2eAgMnSc2d1
neúspěšně	úspěšně	k6eNd1
požádal	požádat	k5eAaPmAgMnS
ruskou	ruský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
o	o	k7c4
obnovení	obnovení	k1gNnSc4
vyšetřování	vyšetřování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kuzněcovův	Kuzněcovův	k2eAgInSc1d1
tým	tým	k1gInSc1
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
počátku	počátek	k1gInSc6
havárie	havárie	k1gFnSc2
stála	stát	k5eAaImAgFnS
technická	technický	k2eAgFnSc1d1
závada	závada	k1gFnSc1
–	–	k?
nedovřený	dovřený	k2eNgInSc4d1
vyrovnávací	vyrovnávací	k2eAgInSc4d1
ventil	ventil	k1gInSc4
v	v	k7c6
kokpitu	kokpit	k1gInSc6
letounu	letoun	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kabina	kabina	k1gFnSc1
tak	tak	k9
nebyla	být	k5eNaImAgFnS
hermeticky	hermeticky	k6eAd1
uzavřená	uzavřený	k2eAgFnSc1d1
a	a	k8xC
posádka	posádka	k1gFnSc1
se	se	k3xPyFc4
proto	proto	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
předpisy	předpis	k1gInPc7
<g/>
,	,	kIx,
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
předčasný	předčasný	k2eAgInSc4d1
návrat	návrat	k1gInSc4
a	a	k8xC
klesání	klesání	k1gNnSc4
do	do	k7c2
2000	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Serjogin	Serjogin	k2eAgInSc1d1
<g/>
,	,	kIx,
známý	známý	k2eAgInSc1d1
oblibou	obliba	k1gFnSc7
razantních	razantní	k2eAgInPc2d1
manévrů	manévr	k1gInPc2
<g/>
,	,	kIx,
zahájil	zahájit	k5eAaPmAgInS
prudký	prudký	k2eAgInSc1d1
sestup	sestup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počáteční	počáteční	k2eAgFnSc1d1
hypoxie	hypoxie	k1gFnSc1
<g/>
,	,	kIx,
následovaná	následovaný	k2eAgNnPc4d1
prudkým	prudký	k2eAgInSc7d1
nárůstem	nárůst	k1gInSc7
tlaku	tlak	k1gInSc2
v	v	k7c6
kabině	kabina	k1gFnSc6
a	a	k8xC
přetížením	přetížení	k1gNnSc7
vedla	vést	k5eAaImAgFnS
podle	podle	k7c2
Kuzněcova	Kuzněcův	k2eAgNnSc2d1
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
schopnosti	schopnost	k1gFnSc2
pilotů	pilot	k1gMnPc2
včas	včas	k6eAd1
reagovat	reagovat	k5eAaBmF
a	a	k8xC
vyrovnat	vyrovnat	k5eAaBmF,k5eAaPmF
letoun	letoun	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
teoriím	teorie	k1gFnPc3
bulvárního	bulvární	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
patří	patřit	k5eAaImIp3nS
v	v	k7c6
Rusku	Rusko	k1gNnSc6
populární	populární	k2eAgNnSc4d1
tvrzení	tvrzení	k1gNnSc4
o	o	k7c6
nehodě	nehoda	k1gFnSc6
pod	pod	k7c7
vlivem	vliv	k1gInSc7
vypité	vypitý	k2eAgFnSc2d1
vodky	vodka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
konspirační	konspirační	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
patří	patřit	k5eAaImIp3nP
tvrzení	tvrzení	k1gNnSc3
o	o	k7c6
smrti	smrt	k1gFnSc6
Gagarina	Gagarino	k1gNnSc2
a	a	k8xC
Serjogina	Serjogino	k1gNnSc2
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
při	při	k7c6
obletu	oblet	k1gInSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červenci	červenec	k1gInSc6
1990	#num#	k4
maďarský	maďarský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
István	István	k2eAgMnSc1d1
Nemere	Nemer	k1gInSc5
prezentoval	prezentovat	k5eAaBmAgInS
sérii	série	k1gFnSc4
nedoložených	doložený	k2eNgFnPc2d1
domněnek	domněnka	k1gFnPc2
z	z	k7c2
různých	různý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
o	o	k7c6
Gagarinovi	Gagarin	k1gMnSc6
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgInSc4
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpochybnil	zpochybnit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc4
prvenství	prvenství	k1gNnSc4
v	v	k7c6
kosmu	kosmos	k1gInSc6
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgMnS
po	po	k7c6
hádce	hádka	k1gFnSc6
s	s	k7c7
Leonidem	Leonid	k1gMnSc7
Břežněvem	Břežněv	k1gInSc7
zavřen	zavřen	k2eAgInSc1d1
do	do	k7c2
blázince	blázinec	k1gInSc2
a	a	k8xC
sprovozen	sprovodit	k5eAaPmNgMnS
ze	z	k7c2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohřeb	pohřeb	k1gInSc1
a	a	k8xC
pozůstalí	pozůstalý	k1gMnPc1
</s>
<s>
„	„	k?
<g/>
Kosmonaut	kosmonaut	k1gMnSc1
č.	č.	k?
1	#num#	k4
<g/>
“	“	k?
odpočívá	odpočívat	k5eAaImIp3nS
spolu	spolu	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
instruktorem	instruktor	k1gMnSc7
na	na	k7c6
čestném	čestný	k2eAgNnSc6d1
pohřebišti	pohřebiště	k1gNnSc6
u	u	k7c2
Kremelské	kremelský	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
mezi	mezi	k7c7
nejvýznamnějšími	významný	k2eAgMnPc7d3
představiteli	představitel	k1gMnPc7
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřeb	pohřeb	k1gInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
byla	být	k5eAaImAgFnS
urna	urna	k1gFnSc1
s	s	k7c7
popelem	popel	k1gInSc7
Gagarina	Gagarina	k1gFnSc1
a	a	k8xC
Serjogina	Serjogina	k1gFnSc1
vystavena	vystavit	k5eAaPmNgFnS
v	v	k7c6
moskevském	moskevský	k2eAgInSc6d1
Ústředním	ústřední	k2eAgInSc6d1
domu	dům	k1gInSc6wR
Sovětské	sovětský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
mrtvými	mrtvý	k2eAgMnPc7d1
se	se	k3xPyFc4
přišlo	přijít	k5eAaPmAgNnS
rozloučit	rozloučit	k5eAaPmF
na	na	k7c4
40	#num#	k4
tisíc	tisíc	k4xCgInSc4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
Gagarinově	Gagarinův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
vdova	vdova	k1gFnSc1
až	až	k8xS
do	do	k7c2
penze	penze	k1gFnSc2
pracovala	pracovat	k5eAaImAgFnS
ve	v	k7c6
Hvězdném	hvězdný	k2eAgNnSc6d1
městečku	městečko	k1gNnSc6
<g/>
,	,	kIx,
zůstala	zůstat	k5eAaPmAgFnS
zde	zde	k6eAd1
i	i	k9
na	na	k7c4
důchod	důchod	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
16	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
2011	#num#	k4
starší	starý	k2eAgFnSc1d2
dcera	dcera	k1gFnSc1
Jelena	Jelena	k1gFnSc1
Jurjevna	Jurjevna	k1gFnSc1
Gagarinová	Gagarinová	k1gFnSc1
<g/>
,	,	kIx,
historička	historička	k1gFnSc1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
působila	působit	k5eAaImAgFnS
desátým	desátý	k4xOgNnSc7
rokem	rok	k1gInSc7
ve	v	k7c6
funkci	funkce	k1gFnSc6
ředitelky	ředitelka	k1gFnSc2
muzea	muzeum	k1gNnSc2
„	„	k?
<g/>
Moskevský	moskevský	k2eAgInSc1d1
Kreml	Kreml	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
mladší	mladý	k2eAgFnSc1d2
dcera	dcera	k1gFnSc1
Galina	Galina	k1gFnSc1
Jurjevna	Jurjevna	k1gFnSc1
Gagarinová	Gagarinový	k2eAgFnSc1d1
roku	rok	k1gInSc2
2018	#num#	k4
již	již	k9
téměř	téměř	k6eAd1
tři	tři	k4xCgFnPc4
desetiletí	desetiletí	k1gNnPc2
přednášela	přednášet	k5eAaImAgFnS
na	na	k7c6
Plechanovově	Plechanovův	k2eAgFnSc6d1
ruské	ruský	k2eAgFnSc6d1
ekonomické	ekonomický	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Publikace	publikace	k1gFnSc1
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
Ю	Ю	k?
А	А	k?
<g/>
.	.	kIx.
Д	Д	k?
в	в	k?
к	к	k?
<g/>
.	.	kIx.
З	З	k?
л	л	k?
С	С	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Н	Н	k?
К	К	k?
<g/>
;	;	kIx,
literární	literární	k2eAgInSc1d1
zápis	zápis	k1gInSc1
Н	Н	k?
Д	Д	k?
<g/>
,	,	kIx,
С	С	k?
Б	Б	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
П	П	k?
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
GAGARIN	GAGARIN	kA
<g/>
,	,	kIx,
Jurij	Jurij	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moje	můj	k3xOp1gFnSc1
cesta	cesta	k1gFnSc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Věra	Věra	k1gFnSc1
Prokopová-Petříková	Prokopová-Petříková	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svět	svět	k1gInSc1
sovětů	sovět	k1gInPc2
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
.	.	kIx.
133	#num#	k4
s.	s.	k?
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
Ю	Ю	k?
А	А	k?
<g/>
.	.	kIx.
В	В	k?
З	З	k?
<g/>
...	...	k?
Д	Д	k?
р	р	k?
<g/>
..	..	k?
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Н	Н	k?
К	К	k?
<g/>
;	;	kIx,
redakce	redakce	k1gFnSc1
Ю	Ю	k?
Д	Д	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Д	Д	k?
л	л	k?
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
.	.	kIx.
64	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
Ю	Ю	k?
А	А	k?
<g/>
;	;	kIx,
Л	Л	k?
<g/>
,	,	kIx,
В	В	k?
И	И	k?
<g/>
.	.	kIx.
П	П	k?
и	и	k?
к	к	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
г	г	k?
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
djvu	djv	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
GAGARIN	GAGARIN	kA
<g/>
,	,	kIx,
Jurij	Jurij	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
<g/>
;	;	kIx,
LEBEDĚV	LEBEDĚV	kA
<g/>
,	,	kIx,
Vladimir	Vladimir	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
ke	k	k7c3
hvězdám	hvězda	k1gFnPc3
:	:	kIx,
Psychologie	psychologie	k1gFnSc1
a	a	k8xC
vesmír	vesmír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jaroslav	Jaroslav	k1gMnSc1
Jemelka	Jemelka	k1gMnSc1
<g/>
;	;	kIx,
Předmluva	předmluva	k1gFnSc1
:	:	kIx,
Milan	Milan	k1gMnSc1
Codr	Codr	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Pragopress	Pragopress	k1gInSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
.	.	kIx.
220	#num#	k4
s.	s.	k?
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
Ю	Ю	k?
А	А	k?
<g/>
.	.	kIx.
Е	Е	k?
п	п	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
С	С	k?
<g/>
.	.	kIx.
Р	Р	k?
<g/>
.	.	kIx.
П	П	k?
<g/>
.	.	kIx.
И	И	k?
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
doplněné	doplněný	k2eAgFnPc4d1
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
Г	Г	k?
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
.	.	kIx.
191	#num#	k4
s.	s.	k?
</s>
<s>
Hodnosti	hodnost	k1gFnPc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1957	#num#	k4
poručík	poručík	k1gMnSc1
(	(	kIx(
<g/>
л	л	k?
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1957	#num#	k4
nadporučík	nadporučík	k1gMnSc1
(	(	kIx(
<g/>
с	с	k?
л	л	k?
<g/>
)	)	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
major	major	k1gMnSc1
(	(	kIx(
<g/>
м	м	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
povýšen	povýšen	k2eAgMnSc1d1
mimořádně	mimořádně	k6eAd1
o	o	k7c4
dvě	dva	k4xCgFnPc4
hodnosti	hodnost	k1gFnPc4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1962	#num#	k4
podplukovník	podplukovník	k1gMnSc1
(	(	kIx(
<g/>
п	п	k?
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1963	#num#	k4
plukovník	plukovník	k1gMnSc1
(	(	kIx(
<g/>
п	п	k?
<g/>
)	)	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Po	po	k7c6
letu	let	k1gInSc6
Gagarin	Gagarin	k1gInSc1
obdržel	obdržet	k5eAaPmAgInS
řadu	řada	k1gFnSc4
řádů	řád	k1gInPc2
a	a	k8xC
titulů	titul	k1gInPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hrdina	Hrdina	k1gMnSc1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
Leninův	Leninův	k2eAgInSc1d1
řád	řád	k1gInSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hrdina	Hrdina	k1gMnSc1
socialistické	socialistický	k2eAgFnSc2d1
práce	práce	k1gFnSc2
BLR	BLR	kA
a	a	k8xC
řád	řád	k1gInSc1
Georgije	Georgije	k1gFnSc2
Dimitrova	Dimitrův	k2eAgInSc2d1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hrdina	Hrdina	k1gMnSc1
socialistické	socialistický	k2eAgFnSc2d1
práce	práce	k1gFnSc2
ČSSR	ČSSR	kA
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
Grunwaldského	Grunwaldský	k2eAgInSc2d1
kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
,	,	kIx,
polský	polský	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
Playa	Playa	k1gMnSc1
Girón	Girón	k1gMnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1961	#num#	k4
<g/>
,	,	kIx,
kubánský	kubánský	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
vzduchoplavby	vzduchoplavba	k1gFnSc2
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
,	,	kIx,
brazilský	brazilský	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
Státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
,	,	kIx,
maďarský	maďarský	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc4
Náhrdelník	náhrdelník	k1gInSc1
Nilu	Nil	k1gInSc2
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
,	,	kIx,
egyptský	egyptský	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
<g/>
,	,	kIx,
znak	znak	k1gInSc1
a	a	k8xC
velká	velký	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
africké	africký	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
k	k	k7c3
řádu	řád	k1gInSc3
„	„	k?
<g/>
Světlo	světlo	k1gNnSc1
v	v	k7c6
temnotě	temnota	k1gFnSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
,	,	kIx,
liberijský	liberijský	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Hrdina	Hrdina	k1gMnSc1
práce	práce	k1gFnSc2
Demokratické	demokratický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Vietnam	Vietnam	k1gInSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
,	,	kIx,
NDR	NDR	kA
<g/>
)	)	kIx)
</s>
<s>
A	a	k9
množství	množství	k1gNnSc1
dalších	další	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
a	a	k8xC
vyznamenání	vyznamenání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
čestným	čestný	k2eAgMnSc7d1
občanem	občan	k1gMnSc7
řady	řada	k1gFnSc2
sovětských	sovětský	k2eAgInPc2d1
(	(	kIx(
<g/>
Kaluga	Kaluga	k1gFnSc1
<g/>
,	,	kIx,
Komsomolsk	Komsomolsk	k1gInSc1
na	na	k7c6
Amuru	Amur	k1gInSc6
<g/>
,	,	kIx,
Ljuberci	Ljuberec	k1gInSc6
<g/>
,	,	kIx,
Novočerkassk	Novočerkassk	k1gInSc1
<g/>
,	,	kIx,
Saratov	Saratov	k1gInSc1
<g/>
,	,	kIx,
Sevastopol	Sevastopol	k1gInSc1
<g/>
,	,	kIx,
Smolensk	Smolensk	k1gInSc1
<g/>
,	,	kIx,
Sumgait	Sumgait	k1gInSc1
<g/>
,	,	kIx,
Ťumeň	Ťumeň	k1gFnSc1
<g/>
,	,	kIx,
Vinnica	Vinnica	k1gFnSc1
<g/>
...	...	k?
<g/>
)	)	kIx)
a	a	k8xC
zahraničních	zahraniční	k2eAgMnPc2d1
(	(	kIx(
<g/>
v	v	k7c6
Československu	Československo	k1gNnSc6
Trenčianských	trenčianský	k2eAgFnPc2d1
Teplic	Teplice	k1gFnPc2
<g/>
)	)	kIx)
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Památka	památka	k1gFnSc1
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Gagarin	Gagarin	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c2
života	život	k1gInSc2
symbol	symbol	k1gInSc1
úspěchů	úspěch	k1gInPc2
sovětské	sovětský	k2eAgFnSc2d1
kosmonautiky	kosmonautika	k1gFnSc2
a	a	k8xC
zdroj	zdroj	k1gInSc1
hrdosti	hrdost	k1gFnSc2
obyvatel	obyvatel	k1gMnPc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
jím	on	k3xPp3gNnSc7
zůstal	zůstat	k5eAaPmAgInS
i	i	k9
po	po	k7c6
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
uctění	uctění	k1gNnSc3
Gagarinovy	Gagarinův	k2eAgFnSc2d1
památky	památka	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgNnP
muzea	muzeum	k1gNnPc1
a	a	k8xC
památníky	památník	k1gInPc1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
po	po	k7c6
něm	on	k3xPp3gNnSc6
pojmenováno	pojmenován	k2eAgNnSc1d1
město	město	k1gNnSc1
Gžatsk	Gžatsk	k1gInSc1
<g/>
,	,	kIx,
školy	škola	k1gFnPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
17	#num#	k4
<g/>
]	]	kIx)
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
bulváry	bulvár	k1gInPc1
<g/>
,	,	kIx,
ulice	ulice	k1gFnPc1
a	a	k8xC
parky	park	k1gInPc1
v	v	k7c6
řadě	řada	k1gFnSc6
sovětských	sovětský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sovětská	sovětský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
představoval	představovat	k5eAaImAgMnS
ideálního	ideální	k2eAgMnSc4d1
hrdinu	hrdina	k1gMnSc4
<g/>
,	,	kIx,
udržovala	udržovat	k5eAaImAgFnS
povědomí	povědomí	k1gNnSc4
o	o	k7c6
Gagarinově	Gagarinův	k2eAgNnSc6d1
prvenství	prvenství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
vycházely	vycházet	k5eAaImAgFnP
vzpomínkové	vzpomínkový	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
a	a	k8xC
filmy	film	k1gInPc1
<g/>
,	,	kIx,
Gagarinův	Gagarinův	k2eAgInSc1d1
portrét	portrét	k1gInSc1
se	se	k3xPyFc4
objevoval	objevovat	k5eAaImAgInS
na	na	k7c6
poštovních	poštovní	k2eAgFnPc6d1
známkách	známka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
jméno	jméno	k1gNnSc1
Jurij	Jurij	k1gMnPc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
načas	načas	k6eAd1
populární	populární	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Sláva	Sláva	k1gFnSc1
prvního	první	k4xOgNnSc2
kosmonauta	kosmonaut	k1gMnSc2
získala	získat	k5eAaPmAgFnS
trvalý	trvalý	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
čtyřiatřiceti	čtyřiatřicet	k4xCc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
paměti	paměť	k1gFnSc6
lidí	člověk	k1gMnPc2
zůstal	zůstat	k5eAaPmAgInS
jako	jako	k9
věčně	věčně	k6eAd1
mladý	mladý	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
přátelský	přátelský	k2eAgInSc1d1
<g/>
,	,	kIx,
s	s	k7c7
typickým	typický	k2eAgInSc7d1
úsměvem	úsměv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyčejný	obyčejný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
vypracoval	vypracovat	k5eAaPmAgMnS
ze	z	k7c2
zapadlé	zapadlý	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
aby	aby	kYmCp3nS
doletěl	doletět	k5eAaPmAgInS
dále	daleko	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
kdokoliv	kdokoliv	k3yInSc1
před	před	k7c7
ním	on	k3xPp3gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
V	v	k7c6
dětství	dětství	k1gNnSc6
pro	pro	k7c4
mě	já	k3xPp1nSc4
byli	být	k5eAaImAgMnP
hrdinové	hrdina	k1gMnPc1
Gagarin	Gagarin	k1gInSc4
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
kosmonauti	kosmonaut	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Kosmonaut	kosmonaut	k1gMnSc1
Fjodor	Fjodor	k1gMnSc1
Jurčichin	Jurčichin	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
lidovým	lidový	k2eAgMnSc7d1
hrdinou	hrdina	k1gMnSc7
a	a	k8xC
<g/>
,	,	kIx,
v	v	k7c6
kontrastu	kontrast	k1gInSc6
k	k	k7c3
jiným	jiný	k2eAgMnPc3d1
sovětským	sovětský	k2eAgMnPc3d1
hrdinům	hrdina	k1gMnPc3
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnPc3
popularita	popularita	k1gFnSc1
nezeslábla	zeslábnout	k5eNaPmAgFnS
ani	ani	k8xC
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nynějším	nynější	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
je	být	k5eAaImIp3nS
populárnější	populární	k2eAgFnSc1d2
než	než	k8xS
kterýkoliv	kterýkoliv	k3yIgInSc1
jiný	jiný	k2eAgInSc1d1
idol	idol	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
průzkumu	průzkum	k1gInSc6
roku	rok	k1gInSc2
2010	#num#	k4
porazil	porazit	k5eAaPmAgInS
i	i	k9
Vladimira	Vladimir	k1gMnSc2
Vysockého	vysocký	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Respekt	respekt	k1gInSc1
ke	k	k7c3
Gagarinovi	Gagarin	k1gMnSc3
trvá	trvat	k5eAaImIp3nS
i	i	k9
mimo	mimo	k7c4
jeho	jeho	k3xOp3gFnSc4
vlast	vlast	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gNnSc6
pojmenované	pojmenovaný	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
v	v	k7c6
angolském	angolský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Namibe	Namib	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
prvního	první	k4xOgMnSc4
z	z	k7c2
kosmonautů	kosmonaut	k1gMnPc2
nezapomínají	zapomínat	k5eNaImIp3nP
ani	ani	k9
zahraniční	zahraniční	k2eAgMnPc1d1
kolegové	kolega	k1gMnPc1
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Gagarin	Gagarin	k1gInSc1
nás	my	k3xPp1nPc2
všechny	všechen	k3xTgInPc4
pozval	pozvat	k5eAaPmAgInS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Zápis	zápis	k1gInSc1
Neila	Neil	k1gMnSc2
Armstronga	Armstrong	k1gMnSc2
v	v	k7c6
pamětní	pamětní	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
Hvězdného	hvězdný	k2eAgNnSc2d1
městečka	městečko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
Gagarinově	Gagarinův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
bylo	být	k5eAaImAgNnS
po	po	k7c6
něm	on	k3xPp3gNnSc6
pojmenováno	pojmenován	k2eAgNnSc1d1
Středisko	středisko	k1gNnSc1
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
v	v	k7c6
Hvězdném	hvězdný	k2eAgNnSc6d1
městečku	městečko	k1gNnSc6
a	a	k8xC
Vojenská	vojenský	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
v	v	k7c6
Moninu	Monino	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
město	město	k1gNnSc1
Gžatsk	Gžatsk	k1gInSc1
poblíž	poblíž	k6eAd1
Gagarinovy	Gagarinův	k2eAgFnSc2d1
rodné	rodný	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
nese	nést	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
na	na	k7c6
jeho	jeho	k3xOp3gFnSc4
památku	památka	k1gFnSc4
jméno	jméno	k1gNnSc4
Gagarin	Gagarin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
bylo	být	k5eAaImAgNnS
náměstí	náměstí	k1gNnSc1
Kalužské	Kalužský	k2eAgFnSc2d1
stráže	stráž	k1gFnSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
Gagarinovo	Gagarinův	k2eAgNnSc4d1
a	a	k8xC
o	o	k7c4
dvanáct	dvanáct	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
zde	zde	k6eAd1
vztyčen	vztyčit	k5eAaPmNgInS
46	#num#	k4
m	m	kA
vysoký	vysoký	k2eAgInSc4d1
památník	památník	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
V	v	k7c4
muzeum	muzeum	k1gNnSc4
byl	být	k5eAaImAgInS
přeměněn	přeměněn	k2eAgInSc1d1
Gagarinův	Gagarinův	k2eAgInSc1d1
rodný	rodný	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c4
Klušinu	Klušina	k1gFnSc4
<g/>
,	,	kIx,
domy	dům	k1gInPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
žili	žít	k5eAaImAgMnP
Gagarinovi	Gagarinův	k2eAgMnPc1d1
v	v	k7c6
Gžatsku	Gžatsko	k1gNnSc6
i	i	k8xC
domy	dům	k1gInPc4
ve	v	k7c6
kterých	který	k3yQgFnPc6,k3yIgFnPc6,k3yRgFnPc6
bydlel	bydlet	k5eAaImAgMnS
během	během	k7c2
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
služby	služba	k1gFnSc2
na	na	k7c6
severu	sever	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Památník	památník	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
Hvězdném	hvězdný	k2eAgNnSc6d1
městečku	městečko	k1gNnSc6
i	i	k8xC
na	na	k7c6
místě	místo	k1gNnSc6
pádu	pád	k1gInSc2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
památníky	památník	k1gInPc1
a	a	k8xC
sochy	socha	k1gFnPc1
připomínající	připomínající	k2eAgFnPc1d1
Gagarina	Gagarino	k1gNnPc1
byly	být	k5eAaImAgInP
postaveny	postavit	k5eAaPmNgInP
v	v	k7c6
řadě	řada	k1gFnSc6
dalších	další	k2eAgNnPc2d1
měst	město	k1gNnPc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
i	i	k9
mimo	mimo	k7c4
něj	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
Ljubercích	Ljuberk	k1gInPc6
<g/>
,	,	kIx,
Kolomně	Kolomně	k1gFnPc6
<g/>
,	,	kIx,
Soči	Soči	k1gNnSc6
<g/>
,	,	kIx,
Erfurtu	Erfurt	k1gInSc6
<g/>
…	…	k?
Jediná	jediný	k2eAgFnSc1d1
socha	socha	k1gFnSc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byla	být	k5eAaImAgFnS
vztyčena	vztyčen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1975	#num#	k4
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
18	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
2012	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
Houstonu	Houston	k1gInSc6
odhalen	odhalit	k5eAaPmNgInS
první	první	k4xOgInSc1
památník	památník	k1gInSc1
Jurije	Jurije	k1gMnSc2
Gagarina	Gagarin	k1gMnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgMnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Gagarin	Gagarin	k1gInSc1
a	a	k8xC
výzkumná	výzkumný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
„	„	k?
<g/>
Kosmonavt	Kosmonavt	k1gInSc1
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
“	“	k?
na	na	k7c6
známce	známka	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1979	#num#	k4
</s>
<s>
Gagarinovo	Gagarinův	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
nosí	nosit	k5eAaImIp3nS
i	i	k9
saratovská	saratovský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
studoval	studovat	k5eAaImAgMnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
19	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1977	#num#	k4
letecký	letecký	k2eAgInSc1d1
závod	závod	k1gInSc1
v	v	k7c6
Komsomolsku	Komsomolsek	k1gInSc6
na	na	k7c6
Amuru	Amur	k1gInSc6
–	–	k?
Komsomolskoamurské	komsomolskoamurský	k2eAgNnSc1d1
letecké	letecký	k2eAgNnSc1d1
výrobní	výrobní	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
J.	J.	kA
A.	A.	kA
Gagarina	Gagarina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počínaje	počínaje	k7c7
ročníkem	ročník	k1gInSc7
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
vítěz	vítěz	k1gMnSc1
Kontinentální	kontinentální	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
dostává	dostávat	k5eAaImIp3nS
Gagarinův	Gagarinův	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
„	„	k?
<g/>
Kosmonavt	Kosmonavt	k2eAgMnSc1d1
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
“	“	k?
nesla	nést	k5eAaImAgFnS
sovětská	sovětský	k2eAgFnSc1d1
vědeckovýzkumná	vědeckovýzkumný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
především	především	k9
k	k	k7c3
udržování	udržování	k1gNnSc3
spojení	spojení	k1gNnSc2
s	s	k7c7
kosmickými	kosmický	k2eAgFnPc7d1
loděmi	loď	k1gFnPc7
a	a	k8xC
stanicemi	stanice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gInSc6
pojmenována	pojmenován	k2eAgFnSc1d1
planetka	planetka	k1gFnSc1
č.	č.	k?
(	(	kIx(
<g/>
1772	#num#	k4
<g/>
)	)	kIx)
Gagarin	Gagarin	k1gInSc1
a	a	k8xC
kráter	kráter	k1gInSc1
Gagarin	Gagarin	k1gInSc1
na	na	k7c6
odvrácené	odvrácený	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
A	a	k8xC
roku	rok	k1gInSc2
2011	#num#	k4
byla	být	k5eAaImAgFnS
k	k	k7c3
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
Gagarinova	Gagarinův	k2eAgInSc2d1
letu	let	k1gInSc2
kosmická	kosmický	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Sojuz	Sojuz	k1gInSc1
TMA-21	TMA-21	k1gFnSc4
pojmenována	pojmenován	k2eAgFnSc1d1
„	„	k?
<g/>
Jurij	Jurij	k1gFnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k6eAd1
jediným	jediný	k2eAgInSc7d1
Sojuzem	Sojuz	k1gInSc7
s	s	k7c7
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pravidelně	pravidelně	k6eAd1
se	se	k3xPyFc4
Gagarinův	Gagarinův	k2eAgInSc1d1
portrét	portrét	k1gInSc1
objevuje	objevovat	k5eAaImIp3nS
na	na	k7c6
známkách	známka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejen	nejen	k6eAd1
sovětská	sovětský	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
československá	československý	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
dokázaly	dokázat	k5eAaPmAgFnP
už	už	k6eAd1
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
vydat	vydat	k5eAaPmF
známky	známka	k1gFnPc4
k	k	k7c3
prvnímu	první	k4xOgInSc3
letu	let	k1gInSc3
člověka	člověk	k1gMnSc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Stovky	stovka	k1gFnPc1
známek	známka	k1gFnPc2
<g/>
,	,	kIx,
pamětních	pamětní	k2eAgNnPc2d1
razítek	razítko	k1gNnPc2
a	a	k8xC
obálek	obálka	k1gFnPc2
s	s	k7c7
gagarinovskými	gagarinovský	k2eAgInPc7d1
náměty	námět	k1gInPc7
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
tématem	téma	k1gNnSc7
několika	několik	k4yIc2
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Gagarin	Gagarin	k1gInSc4
byl	být	k5eAaImAgInS
a	a	k8xC
je	být	k5eAaImIp3nS
připomínán	připomínat	k5eAaImNgInS
v	v	k7c6
řadě	řada	k1gFnSc6
filmů	film	k1gInPc2
<g/>
,	,	kIx,
knih	kniha	k1gFnPc2
a	a	k8xC
písní	píseň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Československu	Československo	k1gNnSc6
vznikla	vzniknout	k5eAaPmAgFnS
oslavná	oslavný	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
už	už	k6eAd1
v	v	k7c4
den	den	k1gInSc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píseň	píseň	k1gFnSc1
„	„	k?
<g/>
Pozdrav	pozdrav	k1gInSc4
astronautovi	astronautův	k2eAgMnPc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
známější	známý	k2eAgFnSc4d2
pod	pod	k7c7
nepůvodním	původní	k2eNgInSc7d1
názvem	název	k1gInSc7
„	„	k?
<g/>
Dobrý	dobrý	k2eAgInSc1d1
den	den	k1gInSc1
<g/>
,	,	kIx,
majore	major	k1gMnSc5
Gagarine	Gagarin	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
nazpíval	nazpívat	k5eAaBmAgMnS,k5eAaPmAgMnS
brněnský	brněnský	k2eAgMnSc1d1
dirigent	dirigent	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Brom	brom	k1gInSc4
<g/>
,	,	kIx,
narychlo	narychlo	k6eAd1
ji	on	k3xPp3gFnSc4
k	k	k7c3
poctě	pocta	k1gFnSc3
prvního	první	k4xOgInSc2
kosmonauta	kosmonaut	k1gMnSc2
složili	složit	k5eAaPmAgMnP
Jaromír	Jaromír	k1gMnSc1
Hnilička	hnilička	k1gFnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
Pácl	Pácl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píseň	píseň	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
po	po	k7c4
léta	léto	k1gNnPc4
populární	populární	k2eAgFnSc2d1
jak	jak	k8xS,k8xC
melodií	melodie	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k8xC
slovy	slovo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obzvláště	obzvláště	k6eAd1
silná	silný	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
připomínání	připomínání	k1gNnSc4
Gagarina	Gagarin	k1gMnSc2
přišla	přijít	k5eAaPmAgFnS
s	s	k7c7
padesátým	padesátý	k4xOgNnSc7
výročím	výročí	k1gNnSc7
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
letu	let	k1gInSc2
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Rusku	Rusko	k1gNnSc6
vláda	vláda	k1gFnSc1
připravovala	připravovat	k5eAaImAgFnS
oslavy	oslava	k1gFnPc4
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2011	#num#	k4
proběhlo	proběhnout	k5eAaPmAgNnS
množství	množství	k1gNnSc1
oficiálních	oficiální	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
slavnostní	slavnostní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
politiků	politik	k1gMnPc2
a	a	k8xC
představitelů	představitel	k1gMnPc2
ruské	ruský	k2eAgFnSc2d1
kosmonautiky	kosmonautika	k1gFnSc2
a	a	k8xC
slavnostní	slavnostní	k2eAgInSc1d1
koncert	koncert	k1gInSc1
v	v	k7c6
Kremlu	Kreml	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
zřízení	zřízení	k1gNnSc4
Gagarinovy	Gagarinův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
renovace	renovace	k1gFnPc4
a	a	k8xC
rozšíření	rozšíření	k1gNnSc4
muzeí	muzeum	k1gNnPc2
<g/>
,	,	kIx,
rozhovory	rozhovor	k1gInPc4
a	a	k8xC
soutěže	soutěž	k1gFnPc4
na	na	k7c6
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
setkání	setkání	k1gNnSc6
a	a	k8xC
konference	konference	k1gFnSc2
věnované	věnovaný	k2eAgFnSc2d1
Gagarinovi	Gagarin	k1gMnSc3
a	a	k8xC
kosmonautice	kosmonautika	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Vyšlo	vyjít	k5eAaPmAgNnS
nové	nový	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
vzpomínek	vzpomínka	k1gFnPc2
jeho	jeho	k3xOp3gFnSc2
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
biografie	biografie	k1gFnPc1
z	z	k7c2
pera	pero	k1gNnSc2
Lva	Lev	k1gMnSc2
Danilkina	Danilkin	k1gMnSc2
<g/>
,	,	kIx,
podrobný	podrobný	k2eAgInSc4d1
rozbor	rozbor	k1gInSc4
letu	let	k1gInSc2
108	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
změnily	změnit	k5eAaPmAgFnP
svět	svět	k1gInSc4
Antona	Anton	k1gMnSc4
Pervušina	Pervušin	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
v	v	k7c6
Česku	Česko	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
dubnu	duben	k1gInSc6
2011	#num#	k4
k	k	k7c3
oživení	oživení	k1gNnSc3
zájmu	zájem	k1gInSc2
o	o	k7c4
Gagarina	Gagarino	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
vlny	vlna	k1gFnSc2
článků	článek	k1gInPc2
v	v	k7c6
tisku	tisk	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
dubnu	duben	k1gInSc6
2011	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
„	„	k?
<g/>
Noc	noc	k1gFnSc1
Jurije	Jurije	k1gFnSc1
Gagarina	Gagarina	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
Uskutečnila	uskutečnit	k5eAaPmAgFnS
se	se	k3xPyFc4
vzpomínková	vzpomínkový	k2eAgFnSc1d1
akce	akce	k1gFnSc1
u	u	k7c2
Gagarinova	Gagarinův	k2eAgInSc2d1
pomníku	pomník	k1gInSc2
na	na	k7c6
karlovarském	karlovarský	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Štefánikova	Štefánikův	k2eAgFnSc1d1
hvězdárna	hvězdárna	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
připravila	připravit	k5eAaPmAgFnS
výstavu	výstava	k1gFnSc4
s	s	k7c7
replikou	replika	k1gFnSc7
Vostoku	Vostok	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Po	po	k7c6
Gagarinově	Gagarinův	k2eAgInSc6d1
kosmickém	kosmický	k2eAgInSc6d1
letu	let	k1gInSc6
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
onen	onen	k3xDgMnSc1
pilot	pilot	k1gMnSc1
<g/>
,	,	kIx,
jistý	jistý	k2eAgMnSc1d1
Larcev	Larcev	k1gFnSc4
<g/>
,	,	kIx,
přežil	přežít	k5eAaPmAgMnS
válku	válka	k1gFnSc4
a	a	k8xC
začátkem	začátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
žil	žít	k5eAaImAgMnS
v	v	k7c6
Gorkém	Gorkij	k1gMnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Z	z	k7c2
32	#num#	k4
zkoušek	zkouška	k1gFnPc2
složených	složený	k2eAgFnPc2d1
za	za	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
studia	studio	k1gNnSc2
měl	mít	k5eAaImAgMnS
31	#num#	k4
„	„	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
pětek	pětka	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
z	z	k7c2
psychologie	psychologie	k1gFnSc2
„	„	k?
<g/>
čtyřku	čtyřka	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
V	v	k7c6
sovětském	sovětský	k2eAgNnSc6d1
i	i	k8xC
ruském	ruský	k2eAgNnSc6d1
známkování	známkování	k1gNnSc6
byla	být	k5eAaImAgFnS
a	a	k8xC
je	být	k5eAaImIp3nS
pětka	pětka	k1gFnSc1
nejlepší	dobrý	k2eAgFnSc1d3
a	a	k8xC
jednička	jednička	k1gFnSc1
nejhorší	zlý	k2eAgFnSc1d3
<g/>
.	.	kIx.
<g/>
)	)	kIx)
↑	↑	k?
Celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
První	první	k4xOgFnSc1
čkalovská	čkalovský	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pilotů	pilot	k1gMnPc2
K.	K.	kA
J.	J.	kA
Vorošilova	Vorošilův	k2eAgFnSc1d1
(	(	kIx(
<g/>
П	П	k?
Ч	Ч	k?
в	в	k?
а	а	k?
у	у	k?
л	л	k?
и	и	k?
<g/>
.	.	kIx.
К	К	k?
<g/>
В	В	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
byla	být	k5eAaImAgFnS
čkalovská	čkalovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
Orenburg	Orenburg	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
letech	let	k1gInPc6
1938	#num#	k4
<g/>
–	–	k?
<g/>
1957	#num#	k4
přejmenován	přejmenován	k2eAgInSc4d1
na	na	k7c4
Čkalov	Čkalov	k1gInSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tři	tři	k4xCgMnPc1
nespokojenci	nespokojenec	k1gMnPc1
ho	on	k3xPp3gInSc2
jedné	jeden	k4xCgFnSc6
noci	noc	k1gFnSc6
zbili	zbít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
si	se	k3xPyFc3
několik	několik	k4yIc4
dní	den	k1gInPc2
poležel	poležet	k5eAaPmAgMnS
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
<g/>
,	,	kIx,
viníky	viník	k1gMnPc4
soud	soud	k1gInSc4
poslal	poslat	k5eAaPmAgMnS
do	do	k7c2
nápravného	nápravný	k2eAgInSc2d1
tábora	tábor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Běžně	běžně	k6eAd1
bylo	být	k5eAaImAgNnS
studium	studium	k1gNnSc1
čtyřleté	čtyřletý	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
pilotů	pilot	k1gInPc2
mimořádně	mimořádně	k6eAd1
zkráceno	zkrátit	k5eAaPmNgNnS
na	na	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Dnes	dnes	k6eAd1
Korzunovo	Korzunův	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luostari-Novoje	Luostari-Novoj	k1gInSc2
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc2
1967	#num#	k4
s	s	k7c7
přispěním	přispění	k1gNnSc7
Gagarina	Gagarino	k1gNnSc2
pojmenováno	pojmenovat	k5eAaPmNgNnS
po	po	k7c6
generálu	generál	k1gMnSc6
Korzunovovi	Korzunova	k1gMnSc6
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1956	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
veliteli	velitel	k1gMnPc7
letectva	letectvo	k1gNnSc2
Severního	severní	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
V	v	k7c6
aeroklubu	aeroklub	k1gInSc6
nalétal	nalétat	k5eAaImAgMnS,k5eAaBmAgMnS,k5eAaPmAgMnS
42	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
23	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
ukončení	ukončení	k1gNnSc6
letecké	letecký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Orenburgu	Orenburg	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
měl	mít	k5eAaImAgInS
celkový	celkový	k2eAgInSc1d1
nálet	nálet	k1gInSc1
166	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
41	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
265	#num#	k4
hodin	hodina	k1gFnPc2
dosáhl	dosáhnout	k5eAaPmAgInS
při	při	k7c6
přijetí	přijetí	k1gNnSc6
do	do	k7c2
oddílu	oddíl	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
v	v	k7c6
březnu	březen	k1gInSc6
1960	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
k	k	k7c3
říjnu	říjen	k1gInSc3
1959	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
posledním	poslední	k2eAgInSc7d1
letem	let	k1gInSc7
měl	mít	k5eAaImAgInS
nálet	nálet	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
340	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
15	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Taneční	taneční	k2eAgInSc1d1
sál	sál	k1gInSc1
byl	být	k5eAaImAgInS
přímo	přímo	k6eAd1
v	v	k7c6
prostorách	prostora	k1gFnPc6
vojenské	vojenský	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
neobyčejně	obyčejně	k6eNd1
výhodně	výhodně	k6eAd1
pro	pro	k7c4
její	její	k3xOp3gMnPc4
studenty	student	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Generálmajor	generálmajor	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Babijčuk	Babijčuk	k1gMnSc1
stál	stát	k5eAaImAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
Lékařské	lékařský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
vojenského	vojenský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Taková	takový	k3xDgFnSc1
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Novinář	novinář	k1gMnSc1
Jaroslav	Jaroslava	k1gFnPc2
Golovanov	Golovanov	k1gInSc4
možnost	možnost	k1gFnSc4
žádosti	žádost	k1gFnSc2
popírá	popírat	k5eAaImIp3nS
a	a	k8xC
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
vymysleli	vymyslet	k5eAaPmAgMnP
N.	N.	kA
Děnisov	Děnisov	k1gInSc4
a	a	k8xC
S.	S.	kA
Borzenko	Borzenka	k1gFnSc5
<g/>
,	,	kIx,
když	když	k9
podle	podle	k7c2
Gagarinova	Gagarinův	k2eAgNnSc2d1
vyprávění	vyprávění	k1gNnSc2
sepisovali	sepisovat	k5eAaImAgMnP
knihu	kniha	k1gFnSc4
Moje	můj	k3xOp1gFnSc1
cesta	cesta	k1gFnSc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
↑	↑	k?
Vybranou	vybraný	k2eAgFnSc4d1
šestici	šestice	k1gFnSc4
tvořili	tvořit	k5eAaImAgMnP
zprvu	zprvu	k6eAd1
Valentin	Valentin	k1gMnSc1
Varlamov	Varlamov	k1gInSc1
<g/>
,	,	kIx,
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
,	,	kIx,
Anatolij	Anatolij	k1gMnSc1
Kartašov	Kartašov	k1gInSc1
<g/>
,	,	kIx,
Andrijan	Andrijan	k1gMnSc1
Nikolajev	Nikolajev	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Popovič	Popovič	k1gMnSc1
a	a	k8xC
German	German	k1gMnSc1
Titov	Titov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kartašov	Kartašov	k1gInSc4
a	a	k8xC
Varlamov	Varlamov	k1gInSc4
záhy	záhy	k6eAd1
vypadli	vypadnout	k5eAaPmAgMnP
ze	z	k7c2
zdravotních	zdravotní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
nahradili	nahradit	k5eAaPmAgMnP
je	on	k3xPp3gFnPc4
Grigorij	Grigorij	k1gFnPc4
Něljubov	Něljubovo	k1gNnPc2
a	a	k8xC
Valerij	Valerij	k1gMnSc1
Bykovskij	Bykovskij	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Konstantin	Konstantin	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Rudněv	Rudněv	k1gMnSc1
byl	být	k5eAaImAgMnS
ministr	ministr	k1gMnSc1
–	–	k?
předseda	předseda	k1gMnSc1
Státního	státní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
obrannou	obranný	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dráhu	dráha	k1gFnSc4
181	#num#	k4
<g/>
–	–	k?
<g/>
327	#num#	k4
uvádějí	uvádět	k5eAaImIp3nP
<g />
.	.	kIx.
</s>
<s hack="1">
ruské	ruský	k2eAgInPc1d1
prameny	pramen	k1gInPc1
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
Sergej	Sergej	k1gMnSc1
Koroljov	Koroljov	k1gInSc1
ve	v	k7c6
zprávě	zpráva	k1gFnSc6
o	o	k7c6
letu	let	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Antonín	Antonín	k1gMnSc1
Vítek	Vítek	k1gMnSc1
v	v	k7c6
encyklopedii	encyklopedie	k1gFnSc6
Space	Spaec	k1gInSc2
40	#num#	k4
má	mít	k5eAaImIp3nS
168	#num#	k4
<g/>
–	–	k?
<g/>
314	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
NASA	NASA	kA
uvádí	uvádět	k5eAaImIp3nS
169	#num#	k4
<g/>
–	–	k?
<g/>
315	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Zprvu	zprvu	k6eAd1
byla	být	k5eAaImAgFnS
doba	doba	k1gFnSc1
přistání	přistání	k1gNnSc2
kosmonauta	kosmonaut	k1gMnSc2
určena	určit	k5eAaPmNgFnS
na	na	k7c6
10.55	10.55	k4
a	a	k8xC
takto	takto	k6eAd1
byla	být	k5eAaImAgFnS
zveřejněna	zveřejnit	k5eAaPmNgFnS
a	a	k8xC
všude	všude	k6eAd1
uváděna	uvádět	k5eAaImNgFnS
<g/>
,	,	kIx,
ještě	ještě	k9
roku	rok	k1gInSc2
2021	#num#	k4
i	i	k8xC
v	v	k7c6
ruských	ruský	k2eAgInPc6d1
oficiálních	oficiální	k2eAgInPc6d1
zdrojích	zdroj	k1gInPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
na	na	k7c6
webových	webový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
Roskosmosu	Roskosmos	k1gInSc2
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Střediska	středisko	k1gNnSc2
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
věnovaných	věnovaný	k2eAgInPc2d1
Gagarinovu	Gagarinův	k2eAgInSc3d1
letu	let	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čas	čas	k1gInSc1
10.55	10.55	k4
byl	být	k5eAaImAgInS
zpochybněn	zpochybnit	k5eAaPmNgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
po	po	k7c6
zveřejnění	zveřejnění	k1gNnSc6
dvoudílného	dvoudílný	k2eAgInSc2d1
souboru	soubor	k1gInSc2
dokumentů	dokument	k1gInPc2
týkajících	týkající	k2eAgInPc2d1
se	se	k3xPyFc4
Gagarinova	Gagarinův	k2eAgInSc2d1
letu	let	k1gInSc2
a	a	k8xC
v	v	k7c6
něm	on	k3xPp3gNnSc6
i	i	k9
zprávy	zpráva	k1gFnPc1
konstrukční	konstrukční	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
OKB-1	OKB-1	k1gFnSc2
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1961	#num#	k4
o	o	k7c6
průběhu	průběh	k1gInSc6
a	a	k8xC
výsledcích	výsledek	k1gInPc6
letu	let	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
Koroljov	Koroljov	k1gInSc4
a	a	k8xC
dva	dva	k4xCgInPc4
jeho	jeho	k3xOp3gInPc4
podřízení	podřízený	k1gMnPc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Gagarin	Gagarin	k1gInSc1
se	se	k3xPyFc4
dotkl	dotknout	k5eAaPmAgInS
země	zem	k1gFnSc2
v	v	k7c6
10.53	10.53	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Kosmonauti	kosmonaut	k1gMnPc1
Titov	Titovo	k1gNnPc2
<g/>
,	,	kIx,
Nikolajev	Nikolajev	k1gMnSc1
<g/>
,	,	kIx,
Popovič	Popovič	k1gMnSc1
<g/>
,	,	kIx,
Bykovskij	Bykovskij	k1gMnSc1
a	a	k8xC
Beljajev	Beljajev	k1gMnSc1
poslali	poslat	k5eAaPmAgMnP
protestní	protestní	k2eAgInSc4d1
dopis	dopis	k1gInSc4
předsedovi	předseda	k1gMnSc3
komise	komise	k1gFnSc2
a	a	k8xC
tajemníkovi	tajemník	k1gMnSc3
ÚV	ÚV	kA
Ustinovovi	Ustinova	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
klíčové	klíčový	k2eAgInPc4d1
při	při	k7c6
vysvětlení	vysvětlení	k1gNnSc6
příčin	příčina	k1gFnPc2
nehody	nehoda	k1gFnSc2
považovali	považovat	k5eAaImAgMnP
náznaky	náznak	k1gInPc4
svědčící	svědčící	k2eAgInPc4d1
o	o	k7c4
dehermetizaci	dehermetizace	k1gFnSc4
kabiny	kabina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
stále	stále	k6eAd1
zůstávala	zůstávat	k5eAaImAgFnS
ve	v	k7c6
bytě	byt	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
Gagarinovi	Gagarinův	k2eAgMnPc1d1
dostali	dostat	k5eAaPmAgMnP
začátkem	začátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
↑	↑	k?
Někde	někde	k6eAd1
se	se	k3xPyFc4
vztah	vztah	k1gInSc1
ke	k	k7c3
Gagarinovi	Gagarin	k1gMnSc3
neomezil	omezit	k5eNaPmAgMnS
jen	jen	k6eAd1
na	na	k7c4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
škole	škola	k1gFnSc6
č.	č.	k?
62	#num#	k4
v	v	k7c6
Iževsku	Iževsko	k1gNnSc6
už	už	k6eAd1
po	po	k7c6
jejím	její	k3xOp3gNnSc6
založení	založení	k1gNnSc6
roku	rok	k1gInSc2
1960	#num#	k4
žáci	žák	k1gMnPc1
s	s	k7c7
učiteli	učitel	k1gMnPc7
sestavovali	sestavovat	k5eAaImAgMnP
a	a	k8xC
odpalovali	odpalovat	k5eAaImAgMnP
rakety	raketa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Gagarinově	Gagarinův	k2eAgInSc6d1
letu	let	k1gInSc6
byla	být	k5eAaImAgFnS
škola	škola	k1gFnSc1
po	po	k7c6
něm	on	k3xPp3gInSc6
pojmenována	pojmenován	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
jejích	její	k3xOp3gFnPc6
prostorách	prostora	k1gFnPc6
vzniklo	vzniknout	k5eAaPmAgNnS
Muzeum	muzeum	k1gNnSc1
historie	historie	k1gFnSc2
letectví	letectví	k1gNnSc2
a	a	k8xC
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vyniklo	vyniknout	k5eAaPmAgNnS
mezi	mezi	k7c7
ostatními	ostatní	k2eAgFnPc7d1
podobnými	podobný	k2eAgFnPc7d1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žáci	Žák	k1gMnPc1
–	–	k?
gagarinci	gagarinec	k1gInSc6
–	–	k?
se	se	k3xPyFc4
v	v	k7c6
„	„	k?
<g/>
posádkách	posádka	k1gFnPc6
<g/>
“	“	k?
(	(	kIx(
<g/>
třídách	třída	k1gFnPc6
<g/>
)	)	kIx)
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
na	na	k7c6
expedice	expedice	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
poznávají	poznávat	k5eAaImIp3nP
historii	historie	k1gFnSc4
letectví	letectví	k1gNnSc2
a	a	k8xC
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průmyslovém	průmyslový	k2eAgNnSc6d1
Iževsku	Iževsko	k1gNnSc6
mohou	moct	k5eAaImIp3nP
takto	takto	k6eAd1
získaný	získaný	k2eAgInSc4d1
vztah	vztah	k1gInSc4
k	k	k7c3
technice	technika	k1gFnSc3
zúročit	zúročit	k5eAaPmF
v	v	k7c6
místních	místní	k2eAgInPc6d1
podnicích	podnik	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Roku	rok	k1gInSc2
1992	#num#	k4
byla	být	k5eAaImAgFnS
místními	místní	k2eAgFnPc7d1
politiky	politika	k1gFnSc2
vyvezena	vyvézt	k5eAaPmNgFnS
na	na	k7c4
smetiště	smetiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
podařilo	podařit	k5eAaPmAgNnS
instalovat	instalovat	k5eAaBmF
na	na	k7c6
karlovarském	karlovarský	k2eAgNnSc6d1
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Saratovská	Saratovský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
profesionálně-pedagogická	profesionálně-pedagogický	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
J.	J.	kA
A.	A.	kA
Gagarina	Gagarina	k1gFnSc1
(	(	kIx(
<g/>
С	С	k?
г	г	k?
п	п	k?
к	к	k?
и	и	k?
Ю	Ю	k?
А	А	k?
Г	Г	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
;	;	kIx,
LÁLA	Lála	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kosmonauti-piloti	Kosmonauti-pilot	k1gMnPc1
SSSR	SSSR	kA
<g/>
,	,	kIx,
s.	s.	k?
353	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
GAGARIN	GAGARIN	kA
<g/>
,	,	kIx,
Jurij	Jurij	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
<g/>
.	.	kIx.
Д	Д	k?
в	в	k?
к	к	k?
<g/>
.	.	kIx.
З	З	k?
л	л	k?
С	С	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
N.	N.	kA
Kamanin	Kamanin	k2eAgMnSc1d1
<g/>
;	;	kIx,
literární	literární	k2eAgInSc1d1
zápis	zápis	k1gInSc1
N.	N.	kA
Děnisov	Děnisov	k1gInSc1
a	a	k8xC
S.	S.	kA
Borzenko	Borzenka	k1gFnSc5
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
П	П	k?
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
С	С	k?
—	—	k?
м	м	k?
р	р	k?
к	к	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Gagarin	Gagarin	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
П	П	k?
к	к	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdné	hvězdný	k2eAgNnSc1d1
městečko	městečko	k1gNnSc1
<g/>
:	:	kIx,
Středisko	středisko	k1gNnSc1
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
J.	J.	kA
A.	A.	kA
Gagarina	Gagarina	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Ж	Ж	k?
п	п	k?
<g/>
.	.	kIx.
Г	Г	k?
у	у	k?
Ю	Ю	k?
Г	Г	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
П	П	k?
к	к	k?
п	п	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GAGARIN	GAGARIN	kA
<g/>
,	,	kIx,
Valentin	Valentin	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
<g/>
.	.	kIx.
М	М	k?
б	б	k?
Ю	Ю	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Valentin	Valentin	k1gMnSc1
Safonov	Safonov	k1gInSc1
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
р	р	k?
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
288	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Л	Л	k?
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
–	–	k?
<g/>
72	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Jurij	Jurij	k1gFnSc1
Alexejevič	Alexejevič	k1gInSc4
Gagarin	Gagarin	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ю	Ю	k?
Г	Г	k?
н	н	k?
с	с	k?
з	з	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
С	С	k?
<g/>
:	:	kIx,
А	А	k?
м	м	k?
о	о	k?
«	«	k?
<g/>
Г	Г	k?
С	С	k?
<g/>
»	»	k?
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
У	У	k?
Т	Т	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
POROCHŇA	POROCHŇA	kA
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
Sidorovič	Sidorovič	k1gMnSc1
<g/>
.	.	kIx.
Х	Х	k?
Ю	Ю	k?
к	к	k?
в	в	k?
С	С	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
Roskosmos	Roskosmos	k1gMnSc1
<g/>
,	,	kIx,
2010-05-21	2010-05-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
А	А	k?
о	о	k?
ч	ч	k?
м	м	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Д	Д	k?
<g/>
,	,	kIx,
Л	Л	k?
А	А	k?
«	«	k?
<g/>
Ю	Ю	k?
Г	Г	k?
<g/>
»	»	k?
<g/>
.	.	kIx.
Г	Г	k?
и	и	k?
к	к	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
Г	Г	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Г	Г	k?
5	#num#	k4
<g/>
.	.	kIx.
Г	Г	k?
и	и	k?
Н	Н	k?
Л	Л	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázky	ukázka	k1gFnSc2
z	z	k7c2
knihy	kniha	k1gFnSc2
„	„	k?
<g/>
Jurij	Jurij	k1gFnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
“	“	k?
Lva	Lev	k1gMnSc2
Danilkina	Danilkin	k1gMnSc2
na	na	k7c6
webu	web	k1gInSc6
nakladatelství	nakladatelství	k1gNnSc2
Molodaja	Molodajus	k1gMnSc2
Gvardija	Gvardijus	k1gMnSc2
<g/>
..	..	k?
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.131	.131	k4
2	#num#	k4
П	П	k?
к	к	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Ж	Ж	k?
п	п	k?
<g/>
.	.	kIx.
С	С	k?
в	в	k?
В	В	k?
С	С	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
С	С	k?
<g/>
,	,	kIx,
С	С	k?
<g/>
.	.	kIx.
С	С	k?
п	п	k?
к	к	k?
н	н	k?
в	в	k?
г	г	k?
<g/>
.	.	kIx.
Е	Е	k?
2000	#num#	k4
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2011	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
14	#num#	k4
(	(	kIx(
<g/>
553	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
IVANOV	IVANOV	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
К	К	k?
э	э	k?
ASTROnote	ASTROnot	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2008-10-21	2008-10-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Ю	Ю	k?
А	А	k?
Г	Г	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Ivanov	Ivanov	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
12	#num#	k4
а	а	k?
2011	#num#	k4
г	г	k?
–	–	k?
50	#num#	k4
<g/>
-л	-л	k?
п	п	k?
п	п	k?
ч	ч	k?
в	в	k?
к	к	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInSc1d1
vědecko-výzkumný	vědecko-výzkumný	k2eAgInSc1d1
institut	institut	k1gInSc1
strojírenství	strojírenství	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Б	Б	k?
п	п	k?
к	к	k?
З	З	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
CNIIMaš	CNIIMaš	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Б	Б	k?
<g/>
,	,	kIx,
С	С	k?
<g/>
;	;	kIx,
Л	Л	k?
<g/>
,	,	kIx,
А	А	k?
<g/>
.	.	kIx.
П	П	k?
п	п	k?
<g/>
.	.	kIx.
П	П	k?
№	№	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Září	zářit	k5eAaImIp3nS
–	–	k?
prosinec	prosinec	k1gInSc1
2010	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydal	vydat	k5eAaPmAgMnS
О	О	k?
п	п	k?
п	п	k?
и	и	k?
п	п	k?
п	п	k?
в	в	k?
2011	#num#	k4
г	г	k?
50	#num#	k4
<g/>
-л	-л	k?
п	п	k?
в	в	k?
к	к	k?
Ю	Ю	k?
Г	Г	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Belocerkovskij	Belocerkovskij	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
19	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
П	П	k?
к	к	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Ж	Ж	k?
п	п	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
13	#num#	k4
<g/>
↑	↑	k?
Gagarin	Gagarin	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
П	П	k?
н	н	k?
в	в	k?
Р	Р	k?
<g/>
.	.	kIx.
↑	↑	k?
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půlstoletí	půlstoletí	k1gNnSc3
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paráda	paráda	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87027	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
První	první	k4xOgInSc1
byl	být	k5eAaImAgInS
Gagarin	Gagarin	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
42	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
Vítek	Vítek	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ivanov	Ivanovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2008-09-15	2008-09-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
П	П	k?
н	н	k?
в	в	k?
о	о	k?
к	к	k?
(	(	kIx(
<g/>
1960	#num#	k4
г	г	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolumbové	Kolumbus	k1gMnPc1
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
přepracované	přepracovaný	k2eAgInPc1d1
a	a	k8xC
doplněné	doplněný	k2eAgInPc1d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souboj	souboj	k1gInSc1
o	o	k7c4
Měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Ladislav	Ladislav	k1gMnSc1
Horáček	Horáček	k1gMnSc1
–	–	k?
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
651	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
150	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Pacner	Pacner	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Г	Г	k?
н	н	k?
о	о	k?
<g/>
.	.	kIx.
Г	Г	k?
<g/>
,	,	kIx,
Я	Я	k?
К	К	k?
<g/>
.	.	kIx.
К	К	k?
<g/>
:	:	kIx,
Ф	Ф	k?
и	и	k?
м	м	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Н	Н	k?
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
822	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
602	#num#	k4
<g/>
–	–	k?
<g/>
603	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Э	Э	k?
с	с	k?
Ц	Ц	k?
и	и	k?
<g/>
.	.	kIx.
Ю	Ю	k?
Г	Г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
Ф	Ф	k?
"	"	kIx"
<g/>
Н	Н	k?
Ц	Ц	k?
и	и	k?
Ю	Ю	k?
А	А	k?
Г	Г	k?
<g/>
"	"	kIx"
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1960-1969	1960-1969	k4
г	г	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.141	.141	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
П	П	k?
к	к	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
П	П	k?
к	к	k?
п	п	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
157	#num#	k4
<g/>
–	–	k?
<g/>
158	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GALLAJ	GALLAJ	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
Я	Я	k?
д	д	k?
<g/>
:	:	kIx,
э	э	k?
д	д	k?
з	з	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
И	И	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
testpilot	testpilot	k1gInSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KAMANIN	KAMANIN	kA
<g/>
,	,	kIx,
Nikolaj	Nikolaj	k1gMnSc1
Petrovič	Petrovič	k1gMnSc1
<g/>
.	.	kIx.
С	С	k?
к	к	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
К	К	k?
п	п	k?
<g/>
.	.	kIx.
1960-1963	1960-1963	k4
г	г	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
И	И	k?
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
А	А	k?
<g/>
.	.	kIx.
8	#num#	k4
а	а	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Kamanin	Kamanin	k2eAgMnSc1d1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Kamanin	Kamanina	k1gFnPc2
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
А	А	k?
<g/>
.	.	kIx.
10	#num#	k4
а	а	k?
<g/>
↑	↑	k?
Kamanin	Kamanina	k1gFnPc2
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
А	А	k?
<g/>
.	.	kIx.
11	#num#	k4
а	а	k?
<g/>
↑	↑	k?
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
Vítek	Vítek	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
50	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
Vítek	Vítek	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
51	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
131	#num#	k4
2	#num#	k4
3	#num#	k4
П	П	k?
к	к	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
С	С	k?
о	о	k?
п	п	k?
<g/>
:	:	kIx,
Х	Х	k?
п	п	k?
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KOROLJOV	KOROLJOV	kA
<g/>
,	,	kIx,
Sergej	Sergej	k1gMnSc1
<g/>
;	;	kIx,
GAGARIN	GAGARIN	kA
<g/>
,	,	kIx,
Jurij	Jurij	k1gMnSc1
<g/>
.	.	kIx.
О	О	k?
о	о	k?
п	п	k?
к	к	k?
п	п	k?
г	г	k?
С	С	k?
Ю	Ю	k?
А	А	k?
Г	Г	k?
н	н	k?
к	к	k?
к	к	k?
«	«	k?
<g/>
В	В	k?
<g/>
»	»	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-05-15	2006-05-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ч	Ч	k?
<g/>
,	,	kIx,
Б	Б	k?
Е	Е	k?
<g/>
.	.	kIx.
Р	Р	k?
и	и	k?
л	л	k?
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
К	К	k?
II	II	kA
Ф	Ф	k?
—	—	k?
П	П	k?
—	—	k?
Т	Т	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
448	#num#	k4
s.	s.	k?
S.	S.	kA
437	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Čertok	Čertok	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Čertok	Čertok	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
432	#num#	k4
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Spaec	k1gInSc2
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
družic	družice	k1gFnPc2
a	a	k8xC
kosmických	kosmický	k2eAgFnPc2d1
sond	sonda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-05-30	2009-05-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1961-012A	1961-012A	k4
(	(	kIx(
<g/>
μ	μ	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
Vostok	Vostok	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Space	Space	k1gFnSc1
Science	Science	k1gFnSc2
Data	datum	k1gNnSc2
Center	centrum	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2010-10-08	2010-10-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Vostok	Vostok	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
176	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
178	#num#	k4
<g/>
–	–	k?
<g/>
179.1	179.1	k4
2	#num#	k4
Ф	Ф	k?
<g/>
,	,	kIx,
Г	Г	k?
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
П	П	k?
о	о	k?
в	в	k?
Ю	Ю	k?
Г	Г	k?
<g/>
.	.	kIx.
Н	Н	k?
к	к	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Únor	únor	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1561	#num#	k4
<g/>
-	-	kIx~
<g/>
1078	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
Vítek	Vítek	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
54	#num#	k4
<g/>
.	.	kIx.
Р	Р	k?
<g/>
.	.	kIx.
К	К	k?
э	э	k?
б	б	k?
в	в	k?
1961	#num#	k4
<g/>
-	-	kIx~
<g/>
м	м	k?
<g/>
..	..	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Р	Р	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2021	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Н	Н	k?
Ц	Ц	k?
и	и	k?
Ю	Ю	k?
<g/>
Г	Г	k?
<g/>
.	.	kIx.
Х	Х	k?
п	п	k?
<g/>
.	.	kIx.
К	К	k?
п	п	k?
п	п	k?
д	д	k?
к	к	k?
э	э	k?
ч	ч	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Н	Н	k?
Ц	Ц	k?
и	и	k?
Ю	Ю	k?
<g/>
Г	Г	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
К	К	k?
<g/>
;	;	kIx,
Б	Б	k?
<g/>
;	;	kIx,
Т	Т	k?
<g/>
.	.	kIx.
О	О	k?
О	О	k?
п	п	k?
р	р	k?
з	з	k?
к	к	k?
с	с	k?
п	п	k?
Ю	Ю	k?
А	А	k?
Г	Г	k?
н	н	k?
б	б	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Д	Д	k?
<g/>
,	,	kIx,
В	В	k?
П	П	k?
п	п	k?
п	п	k?
<g/>
.	.	kIx.
Р	Р	k?
к	к	k?
в	в	k?
а	а	k?
д	д	k?
<g/>
.	.	kIx.
К	К	k?
2	#num#	k4
<g/>
..	..	k?
М	М	k?
<g/>
:	:	kIx,
Р	Р	k?
М	М	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
905350	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
905350	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
42	#num#	k4
<g/>
–	–	k?
<g/>
46	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
s.	s.	k?
42	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Ч	Ч	k?
<g/>
,	,	kIx,
Б	Б	k?
Е	Е	k?
<g/>
.	.	kIx.
Ч	Ч	k?
Б	Б	k?
Р	Р	k?
и	и	k?
л	л	k?
<g/>
.	.	kIx.
Г	Г	k?
д	д	k?
"	"	kIx"
<g/>
х	х	k?
в	в	k?
<g/>
"	"	kIx"
Г	Г	k?
3	#num#	k4
<g/>
.	.	kIx.
"	"	kIx"
<g/>
П	П	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
RKK	RKK	kA
Eněrgija	Eněrgija	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
П	П	k?
<g/>
,	,	kIx,
В	В	k?
<g/>
.	.	kIx.
О	О	k?
б	б	k?
с	с	k?
б	б	k?
<g/>
.	.	kIx.
Р	Р	k?
к	к	k?
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
46	#num#	k4
<g/>
–	–	k?
<g/>
53	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
5	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
С	С	k?
<g/>
,	,	kIx,
А	А	k?
<g/>
.	.	kIx.
Ч	Ч	k?
<g/>
,	,	kIx,
к	к	k?
п	п	k?
с	с	k?
у	у	k?
Г	Г	k?
<g/>
.	.	kIx.
Р	Р	k?
г	г	k?
-	-	kIx~
Н	Н	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-04-01	2005-04-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3734	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOLUB	Holub	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MEK	mek	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Žebříček	žebříček	k1gInSc1
hodin	hodina	k1gFnPc2
nalétaných	nalétaný	k2eAgFnPc2d1
v	v	k7c6
kosmu	kosmos	k1gInSc6
k	k	k7c3
31.12	31.12	k4
<g/>
.2011	.2011	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Р	Р	k?
ф	ф	k?
Ю	Ю	k?
Г	Г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
У	У	k?
<g/>
,	,	kIx,
2009-02-17	2009-02-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
CNIIMaš	CNIIMaš	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Р	Р	k?
<g/>
,	,	kIx,
у	у	k?
в	в	k?
п	п	k?
<g/>
.	.	kIx.
↑	↑	k?
TOUFAR	Toufar	k1gMnSc1
<g/>
;	;	kIx,
PAVEL	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmické	kosmický	k2eAgInPc1d1
skandály	skandál	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Regia	Regia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902484	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Krize	krize	k1gFnPc4
a	a	k8xC
odchody	odchod	k1gInPc4
<g/>
,	,	kIx,
s.	s.	k?
532	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Gagarin	Gagarin	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
С	С	k?
<g/>
,	,	kIx,
12	#num#	k4
а	а	k?
<g/>
.	.	kIx.
↑	↑	k?
Čertok	Čertok	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
440	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠVANDRLÍK	ŠVANDRLÍK	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jurij	Jurij	k1gMnSc2
Gagarin	Gagarin	k1gInSc4
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
kosmonaut	kosmonaut	k1gMnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
Mariánských	mariánský	k2eAgFnPc6d1
Lázních	lázeň	k1gFnPc6
v	v	k7c6
létě	léto	k1gNnSc6
1966	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamelika	Hamelika	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Kamanin	Kamanina	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
К	К	k?
т	т	k?
<g/>
.	.	kIx.
1967-1968	1967-1968	k4
г	г	k?
<g/>
.	.	kIx.
А	А	k?
<g/>
.	.	kIx.
11	#num#	k4
а	а	k?
<g/>
..	..	k?
1	#num#	k4
2	#num#	k4
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odvrácená	odvrácený	k2eAgFnSc1d1
tvář	tvář	k1gFnSc1
slávy	sláva	k1gFnSc2
Jurije	Jurije	k1gFnSc1
Gagarina	Gagarina	k1gFnSc1
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
Technet	Technet	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-12	2011-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HIRSCHKOWITZ	HIRSCHKOWITZ	kA
<g/>
,	,	kIx,
Nafthali	Nafthali	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
С	С	k?
п	п	k?
и	и	k?
К	К	k?
п	п	k?
и	и	k?
С	С	k?
С	С	k?
1898	#num#	k4
-	-	kIx~
1991	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
knowbysight	knowbysight	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Д	Д	k?
В	В	k?
С	С	k?
С	С	k?
VI-г	VI-г	k1gFnSc2
с	с	k?
1962	#num#	k4
-	-	kIx~
1966	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Hirschkowitz	Hirschkowitz	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hirschkowitz	Hirschkowitz	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Д	Д	k?
В	В	k?
С	С	k?
С	С	k?
VII-г	VII-г	k1gFnSc2
с	с	k?
1966	#num#	k4
-	-	kIx~
1970	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Belocerkovskij	Belocerkovskij	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
38	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
187	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kamanin	Kamanina	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
К	К	k?
т	т	k?
<g/>
.	.	kIx.
1967-1968	1967-1968	k4
г	г	k?
<g/>
.	.	kIx.
А	А	k?
<g/>
.	.	kIx.
3	#num#	k4
а	а	k?
<g/>
..	..	k?
1	#num#	k4
2	#num#	k4
Belocerkovskij	Belocerkovskij	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
41.1	41.1	k4
2	#num#	k4
3	#num#	k4
PAVLÍK	Pavlík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarinův	Gagarinův	k2eAgInSc1d1
případ	případ	k1gInSc1
stále	stále	k6eAd1
aktuální	aktuální	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc4
<g/>
,	,	kIx,
září	zářit	k5eAaImIp3nS
2005	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
П	П	k?
<g/>
,	,	kIx,
И	И	k?
<g/>
.	.	kIx.
13	#num#	k4
п	п	k?
г	г	k?
Г	Г	k?
<g/>
.	.	kIx.
Н	Н	k?
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-11-10	2006-11-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Eduardem	Eduard	k1gMnSc7
Šeršerem	Šeršer	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Д	Д	k?
<g/>
,	,	kIx,
В	В	k?
<g/>
;	;	kIx,
Е	Е	k?
<g/>
,	,	kIx,
А	А	k?
<g/>
.	.	kIx.
П	П	k?
м	м	k?
Ю	Ю	k?
Г	Г	k?
<g/>
.	.	kIx.
Р	Р	k?
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2004-03-30	2004-03-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3441	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ш	Ш	k?
<g/>
,	,	kIx,
С	С	k?
П	П	k?
Ю	Ю	k?
Г	Г	k?
и	и	k?
В	В	k?
С	С	k?
<g/>
.	.	kIx.
Н	Н	k?
к	к	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1561	#num#	k4
<g/>
-	-	kIx~
<g/>
1078	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamanin	Kamanina	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1968	#num#	k4
г	г	k?
30	#num#	k4
и	и	k?
<g/>
.	.	kIx.
2	#num#	k4
а	а	k?
<g/>
..	..	k?
↑	↑	k?
Gagarin	Gagarin	k1gInSc1
zahynul	zahynout	k5eAaPmAgInS
asi	asi	k9
kvůli	kvůli	k7c3
hrozící	hrozící	k2eAgFnSc3d1
srážce	srážka	k1gFnSc3
s	s	k7c7
balonovou	balonový	k2eAgFnSc7d1
sondou	sonda	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-08	2011-04-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Belocerkovskij	Belocerkovskij	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
36	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Belocerkovskij	Belocerkovskij	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
44.1	44.1	k4
2	#num#	k4
Belocerkovskij	Belocerkovskij	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
45	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Belocerkovskij	Belocerkovskij	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
46	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarinova	Gagarinův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
nejasná	jasný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1
vyšetřování	vyšetřování	k1gNnSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příloha	příloha	k1gFnSc1
Technet	Techneta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-08	2011-04-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
↑	↑	k?
aš	aš	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
se	se	k3xPyFc4
nedopustil	dopustit	k5eNaPmAgInS
pilotní	pilotní	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
<g/>
,	,	kIx,
smetla	smetnout	k5eAaPmAgFnS
ho	on	k3xPp3gInSc4
stíhačka	stíhačka	k1gFnSc1
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
odtajněná	odtajněný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borgis	borgis	k1gInSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
2013-6-27	2013-6-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
AKSJONOV	AKSJONOV	kA
<g/>
,	,	kIx,
Vladimir	Vladimir	k1gMnSc1
<g/>
.	.	kIx.
С	С	k?
о	о	k?
н	н	k?
в	в	k?
г	г	k?
Г	Г	k?
<g/>
.	.	kIx.
Э	Э	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-07-01	2010-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gagarin	Gagarin	k1gInSc1
prý	prý	k9
zahynul	zahynout	k5eAaPmAgInS
kvůli	kvůli	k7c3
vadné	vadný	k2eAgFnSc3d1
hermetičnosti	hermetičnost	k1gFnSc3
kabiny	kabina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-01-09	2010-01-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
М	М	k?
<g/>
,	,	kIx,
А	А	k?
<g/>
.	.	kIx.
Г	Г	k?
п	п	k?
<g/>
,	,	kIx,
п	п	k?
ч	ч	k?
ч	ч	k?
с	с	k?
и	и	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
Ч	Ч	k?
2	#num#	k4
<g/>
.	.	kIx.
К	К	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-03-27	2008-03-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
К	К	k?
<g/>
,	,	kIx,
И	И	k?
<g/>
.	.	kIx.
К	К	k?
о	о	k?
п	п	k?
б	б	k?
<g/>
.	.	kIx.
С	С	k?
н	н	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2004-03-10	2004-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ZHELEZNYAKOV	ZHELEZNYAKOV	kA
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
was	was	k?
still	still	k1gInSc1
the	the	k?
first	first	k1gInSc1
:	:	kIx,
Part	part	k1gInSc1
two	two	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2002-10-16	2002-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Publikováno	publikován	k2eAgNnSc1d1
v	v	k7c6
"	"	kIx"
<g/>
ORBIT	orbita	k1gFnPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Journal	Journal	k1gMnSc1
of	of	k?
the	the	k?
Astro	astra	k1gFnSc5
Space	Space	k1gFnPc1
Stamp	Stamp	k1gInSc4
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
Issue	Issu	k1gInSc2
№	№	k?
54	#num#	k4
<g/>
,	,	kIx,
June	jun	k1gMnSc5
2002	#num#	k4
<g/>
;	;	kIx,
"	"	kIx"
<g/>
Spaceflight	Spaceflight	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Vol	vol	k6eAd1
44	#num#	k4
<g/>
,	,	kIx,
№	№	k?
11	#num#	k4
<g/>
,	,	kIx,
November	November	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
471	#num#	k4
<g/>
-	-	kIx~
<g/>
475	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TOUFAR	Toufar	k1gMnSc1
<g/>
;	;	kIx,
PAVEL	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmické	kosmický	k2eAgInPc1d1
skandály	skandál	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Regia	Regia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902484	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Krize	krize	k1gFnPc4
a	a	k8xC
odchody	odchod	k1gInPc4
<g/>
,	,	kIx,
s.	s.	k?
536	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kamanin	Kamanina	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
К	К	k?
т	т	k?
<g/>
.	.	kIx.
1967-1968	1967-1968	k4
г	г	k?
<g/>
.	.	kIx.
1968	#num#	k4
г	г	k?
<g/>
.	.	kIx.
11	#num#	k4
а	а	k?
<g/>
..	..	k?
↑	↑	k?
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
195	#num#	k4
<g/>
.	.	kIx.
Б	Б	k?
<g/>
,	,	kIx,
О	О	k?
<g/>
.	.	kIx.
Г	Г	k?
<g/>
:	:	kIx,
с	с	k?
з	з	k?
с	с	k?
<g/>
.	.	kIx.
Г	Г	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-04-12	2010-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Д	Д	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
«	«	k?
<g/>
Г	Г	k?
и	и	k?
м	м	k?
„	„	k?
<g/>
М	М	k?
К	К	k?
<g/>
“	“	k?
<g/>
»	»	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Г	Г	k?
Г	Г	k?
Ю	Ю	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Р	Р	k?
э	э	k?
у	у	k?
и	и	k?
<g/>
.	.	kIx.
Г	Г	k?
П	П	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CNIIMaš	CNIIMaš	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Н	Н	k?
Ю	Ю	k?
Г	Г	k?
<g/>
.	.	kIx.
↑	↑	k?
П	П	k?
<g/>
,	,	kIx,
Ю	Ю	k?
<g/>
.	.	kIx.
Г	Г	k?
д	д	k?
д	д	k?
к	к	k?
<g/>
.	.	kIx.
В	В	k?
п	п	k?
<g/>
.	.	kIx.
И	И	k?
р	р	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-09	2011-04-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Т	Т	k?
<g/>
,	,	kIx,
В	В	k?
<g/>
.	.	kIx.
Г	Г	k?
н	н	k?
в	в	k?
<g/>
.	.	kIx.
З	З	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-15	2011-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
52	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
В	В	k?
ш	ш	k?
№	№	k?
62	#num#	k4
п	п	k?
п	п	k?
в	в	k?
Г	Г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
П	П	k?
У	У	k?
К	К	k?
<g/>
,	,	kIx,
2009-04-13	2009-04-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
И	И	k?
в	в	k?
р	р	k?
у	у	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
С	С	k?
<g/>
:	:	kIx,
М	М	k?
о	о	k?
п	п	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
И	И	k?
и	и	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
П	П	k?
Р	Р	k?
<g/>
.	.	kIx.
Ф	Ф	k?
Ю	Ю	k?
<g/>
:	:	kIx,
«	«	k?
<g/>
Д	Д	k?
м	м	k?
в	в	k?
д	д	k?
г	г	k?
б	б	k?
Г	Г	k?
<g/>
»	»	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Р	Р	k?
<g/>
,	,	kIx,
2011-05-21	2011-05-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Г	Г	k?
к	к	k?
р	р	k?
о	о	k?
Ю	Ю	k?
Г	Г	k?
и	и	k?
В	В	k?
В	В	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Р	Р	k?
Н	Н	k?
<g/>
,	,	kIx,
2010-01-20	2010-01-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Namibe	Namib	k1gMnSc5
Yuri	Yur	k1gMnSc5
Gagarin	Gagarin	k1gInSc4
Airport	Airport	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centre	centr	k1gInSc5
for	forum	k1gNnPc2
Asia	Asia	k1gMnSc1
Pacific	Pacific	k1gMnSc1
Aviation	Aviation	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
190	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
П	П	k?
Ю	Ю	k?
А	А	k?
Г	Г	k?
в	в	k?
Г	Г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
MOSGID	MOSGID	kA
<g/>
.	.	kIx.
<g/>
RU	RU	kA
<g/>
,	,	kIx,
П	П	k?
п	п	k?
М	М	k?
и	и	k?
М	М	k?
о	о	k?
<g/>
,	,	kIx,
2010-11-09	2010-11-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
ČECHOVÁ	Čechová	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chudák	chudák	k1gMnSc1
Gagarin	Gagarin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Varech	Vary	k1gInPc6
skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
skládce	skládka	k1gFnSc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
na	na	k7c6
letišti	letiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentní	parlamentní	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-14	2011-04-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
BAROCH	BAROCH	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kolonády	kolonáda	k1gFnSc2
na	na	k7c4
smetiště	smetiště	k1gNnSc4
-	-	kIx~
a	a	k8xC
na	na	k7c4
letiště	letiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-04-12	2006-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Б	Б	k?
<g/>
,	,	kIx,
О	О	k?
<g/>
.	.	kIx.
П	П	k?
Ю	Ю	k?
Г	Г	k?
о	о	k?
в	в	k?
С	С	k?
<g/>
.	.	kIx.
К	К	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-10-16	2012-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
С	С	k?
г	г	k?
п	п	k?
к	к	k?
и	и	k?
Ю	Ю	k?
А	А	k?
Г	Г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
С	С	k?
<g/>
:	:	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Antonín	Antonín	k1gMnSc1
Rükl	Rükl	k1gMnSc1
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
Aventinum	Aventinum	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
Mapa	mapa	k1gFnSc1
celého	celý	k2eAgInSc2d1
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
191	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
↑	↑	k?
Crater	Crater	k1gMnSc1
Gagarin	Gagarin	k1gInSc4
on	on	k3xPp3gMnSc1
Moon	Moon	k1gMnSc1
Gazetteer	Gazetteer	k1gMnSc1
of	of	k?
Planetary	Planetara	k1gFnSc2
Nomenclature	Nomenclatur	k1gMnSc5
<g/>
,	,	kIx,
IAU	IAU	kA
<g/>
,	,	kIx,
USGS	USGS	kA
<g/>
,	,	kIx,
NASA	NASA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
-	-	kIx~
И	И	k?
В	В	k?
в	в	k?
с	с	k?
к	к	k?
«	«	k?
<g/>
Ю	Ю	k?
<g/>
Г	Г	k?
н	н	k?
п	п	k?
м	м	k?
<g/>
»	»	k?
<g/>
.	.	kIx.
Н	Н	k?
к	к	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-02-11	2005-02-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1561	#num#	k4
<g/>
-	-	kIx~
<g/>
1078	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
М	М	k?
<g/>
,	,	kIx,
А	А	k?
З	З	k?
<g/>
,	,	kIx,
к	к	k?
о	о	k?
п	п	k?
б	б	k?
<g/>
…	…	k?
П	П	k?
м	м	k?
<g/>
,	,	kIx,
к	к	k?
<g/>
,	,	kIx,
с	с	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Р	Р	k?
и	и	k?
с	с	k?
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
128	#num#	k4
s.	s.	k?
Předmluva	předmluva	k1gFnSc1
G.	G.	kA
Titova	Titovo	k1gNnSc2
<g/>
.	.	kIx.
↑	↑	k?
К	К	k?
<g/>
,	,	kIx,
Ю	Ю	k?
Ю	Ю	k?
<g/>
Г	Г	k?
н	н	k?
п	п	k?
м	м	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
П	П	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příloha	příloha	k1gFnSc1
časopisu	časopis	k1gInSc2
М	М	k?
м	м	k?
(	(	kIx(
<g/>
únor	únor	k1gInSc1
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Г	Г	k?
в	в	k?
п	п	k?
<g/>
,	,	kIx,
п	п	k?
<g/>
,	,	kIx,
к	к	k?
<g/>
...	...	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
г	г	k?
Г	Г	k?
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HONUS	HONUS	kA
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
ódy	óda	k1gFnPc4
na	na	k7c4
Gagarina	Gagarino	k1gNnPc4
přiznal	přiznat	k5eAaPmAgMnS
Právu	právo	k1gNnSc6
<g/>
:	:	kIx,
psal	psát	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
to	ten	k3xDgNnSc4
dvě	dva	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-12	2011-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
П	П	k?
о	о	k?
м	м	k?
п	п	k?
п	п	k?
50	#num#	k4
<g/>
-л	-л	k?
п	п	k?
в	в	k?
к	к	k?
Ю	Ю	k?
<g/>
Г	Г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Р	Р	k?
<g/>
,	,	kIx,
2010-03	2010-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
В	В	k?
у	у	k?
п	п	k?
и	и	k?
Г	Г	k?
з	з	k?
в	в	k?
в	в	k?
р	р	k?
к	к	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Р	Р	k?
г	г	k?
<g/>
,	,	kIx,
2011-04-07	2011-04-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MICHAL	Michal	k1gMnSc1
<g/>
,	,	kIx,
Polák	Polák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
úterý	úterý	k1gNnSc6
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
se	se	k3xPyFc4
uskuteční	uskutečnit	k5eAaPmIp3nS
Noc	noc	k1gFnSc1
Jurije	Jurije	k1gFnSc1
Gagarina	Gagarina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
2011-04-11	2011-04-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HAVELKA	Havelka	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
se	se	k3xPyFc4
vzpomínalo	vzpomínat	k5eAaImAgNnS
na	na	k7c4
Gagarina	Gagarino	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denik	Denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-12	2011-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ROZEHNAL	Rozehnal	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seděli	sedět	k5eAaImAgMnP
jste	být	k5eAaImIp2nP
někdy	někdy	k6eAd1
v	v	k7c6
raketě	raketa	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Štefánikova	Štefánikův	k2eAgFnSc1d1
hvězdárna	hvězdárna	k1gFnSc1
<g/>
,	,	kIx,
2011-04-13	2011-04-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Česky	česky	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolumbové	Kolumbus	k1gMnPc1
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
přepracované	přepracovaný	k2eAgInPc1d1
a	a	k8xC
doplněné	doplněný	k2eAgInPc1d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souboj	souboj	k1gInSc1
o	o	k7c4
Měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Ladislav	Ladislav	k1gMnSc1
Horáček	Horáček	k1gMnSc1
–	–	k?
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
651	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Cesta	cesta	k1gFnSc1
k	k	k7c3
nesmrtelnosti	nesmrtelnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
prvního	první	k4xOgMnSc2
kosmonauta	kosmonaut	k1gMnSc2
Jurije	Jurije	k1gFnSc2
Gagarina	Gagarin	k1gMnSc2
<g/>
,	,	kIx,
s.	s.	k?
142	#num#	k4
<g/>
–	–	k?
<g/>
195	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půlstoletí	půlstoletí	k1gNnSc3
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
472	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87027	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOUFAR	Toufar	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzestup	vzestup	k1gInSc4
a	a	k8xC
pád	pád	k1gInSc4
Jurije	Jurije	k1gMnSc2
Gagarina	Gagarin	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Regia	Regia	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
239	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86367	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOUFAR	Toufar	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
<g/>
:	:	kIx,
byl	být	k5eAaImAgInS
první	první	k4xOgMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
280	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7376	#num#	k4
<g/>
-	-	kIx~
<g/>
277	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rusky	Ruska	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Б	Б	k?
<g/>
,	,	kIx,
С	С	k?
М	М	k?
<g/>
.	.	kIx.
Д	Д	k?
Г	Г	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
Г	Г	k?
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
176	#num#	k4
s.	s.	k?
</s>
<s>
Б	Б	k?
<g/>
,	,	kIx,
С	С	k?
М	М	k?
<g/>
.	.	kIx.
П	П	k?
В	В	k?
<g/>
:	:	kIx,
З	З	k?
[	[	kIx(
<g/>
О	О	k?
Ю	Ю	k?
А	А	k?
Г	Г	k?
и	и	k?
п	п	k?
о	о	k?
к	к	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
303	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
217	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2865	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Д	Д	k?
<g/>
,	,	kIx,
Л	Л	k?
А	А	k?
<g/>
.	.	kIx.
Ю	Ю	k?
Г	Г	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
г	г	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Ж	Ж	k?
з	з	k?
л	л	k?
<g/>
.	.	kIx.
С	С	k?
б	б	k?
<g/>
;	;	kIx,
sv.	sv.	kA
1500	#num#	k4
(	(	kIx(
<g/>
1300	#num#	k4
<g/>
))	))	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
235	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3440	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
В	В	k?
А	А	k?
<g/>
.	.	kIx.
М	М	k?
б	б	k?
Ю	Ю	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgInSc1d1
zápis	zápis	k1gInSc1
В	В	k?
С	С	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
р	р	k?
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
288	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
А	А	k?
Т	Т	k?
<g/>
;	;	kIx,
К	К	k?
<g/>
,	,	kIx,
Т	Т	k?
А	А	k?
<g/>
.	.	kIx.
С	С	k?
о	о	k?
с	с	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
Г	Г	k?
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
А	А	k?
Т	Т	k?
<g/>
;	;	kIx,
К	К	k?
<g/>
,	,	kIx,
Т	Т	k?
А	А	k?
<g/>
.	.	kIx.
Ю	Ю	k?
Г	Г	k?
<g/>
:	:	kIx,
г	г	k?
м	м	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
К	К	k?
Р	Р	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
368	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6090	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
Я	Я	k?
К	К	k?
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
Н	Н	k?
Г	Г	k?
<g/>
:	:	kIx,
к	к	k?
о	о	k?
п	п	k?
к	к	k?
и	и	k?
з	з	k?
<g/>
,	,	kIx,
н	н	k?
к	к	k?
о	о	k?
р	р	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
П	П	k?
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
330	#num#	k4
s.	s.	k?
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
Я	Я	k?
К	К	k?
К	К	k?
№	№	k?
1	#num#	k4
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
И	И	k?
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
80	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
К	К	k?
<g/>
,	,	kIx,
С	С	k?
Д	Д	k?
<g/>
.	.	kIx.
Ю	Ю	k?
Г	Г	k?
<g/>
:	:	kIx,
п	п	k?
б	б	k?
р	р	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
С	С	k?
<g/>
.	.	kIx.
<g/>
Р	Р	k?
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
155	#num#	k4
s.	s.	k?
</s>
<s>
П	П	k?
<g/>
,	,	kIx,
А	А	k?
И	И	k?
<g/>
.	.	kIx.
108	#num#	k4
м	м	k?
<g/>
,	,	kIx,
и	и	k?
м	м	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Э	Э	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
528	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
699	#num#	k4
<g/>
-	-	kIx~
<g/>
48001	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
П	П	k?
<g/>
,	,	kIx,
В	В	k?
С	С	k?
<g/>
.	.	kIx.
Д	Д	k?
н	н	k?
Б	Б	k?
<g/>
.	.	kIx.
Р	Р	k?
о	о	k?
Ю	Ю	k?
Г	Г	k?
<g/>
.	.	kIx.
А	А	k?
<g/>
:	:	kIx,
К	К	k?
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
104	#num#	k4
s.	s.	k?
</s>
<s>
У	У	k?
<g/>
,	,	kIx,
Ю	Ю	k?
С	С	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
Б	Б	k?
Г	Г	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Г	Г	k?
О	О	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
624	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
89823	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
36	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
O	o	k7c6
Gagarinově	Gagarinův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
Б	Б	k?
<g/>
,	,	kIx,
С	С	k?
М	М	k?
<g/>
.	.	kIx.
Г	Г	k?
Г	Г	k?
<g/>
:	:	kIx,
Ф	Ф	k?
и	и	k?
д	д	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
160	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
217	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1901	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Б	Б	k?
<g/>
,	,	kIx,
С	С	k?
<g/>
;	;	kIx,
Л	Л	k?
<g/>
,	,	kIx,
А	А	k?
<g/>
.	.	kIx.
П	П	k?
п	п	k?
<g/>
.	.	kIx.
П	П	k?
№	№	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Září	zářit	k5eAaImIp3nS
–	–	k?
prosinec	prosinec	k1gInSc1
2010	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
36	#num#	k4
<g/>
–	–	k?
<g/>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydal	vydat	k5eAaPmAgMnS
О	О	k?
п	п	k?
п	п	k?
и	и	k?
п	п	k?
п	п	k?
в	в	k?
2011	#num#	k4
г	г	k?
50	#num#	k4
<g/>
-л	-л	k?
п	п	k?
в	в	k?
к	к	k?
Ю	Ю	k?
Г	Г	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
19	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
HANOUSEK	Hanousek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
s	s	k7c7
Gagarinem	Gagarin	k1gInSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leden	leden	k1gInSc1
2004	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PAVLÍK	Pavlík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarinův	Gagarinův	k2eAgInSc1d1
případ	případ	k1gInSc1
stále	stále	k6eAd1
aktuální	aktuální	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc4
<g/>
,	,	kIx,
září	zářit	k5eAaImIp3nS
2005	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jurij	Jurij	k1gFnSc2
Alexejevič	Alexejevič	k1gInSc1
Gagarin	Gagarin	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jurij	Jurij	k1gMnSc1
Gagarin	Gagarin	k1gInSc1
</s>
<s>
Česky	česky	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1961	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
41	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
102	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc1
z	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1961	#num#	k4
s	s	k7c7
řadou	řada	k1gFnSc7
článků	článek	k1gInPc2
věnovaných	věnovaný	k2eAgInPc2d1
Gagarinově	Gagarinův	k2eAgInSc6d1
letu	let	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Archiv	archiv	k1gInSc1
rozhlasu	rozhlas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlasy	hlas	k1gInPc1
slavných	slavný	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
Jurij	Jurij	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvukový	zvukový	k2eAgInSc1d1
záznam	záznam	k1gInSc1
setkání	setkání	k1gNnSc2
Jurije	Jurije	k1gMnSc2
Gagarina	Gagarin	k1gMnSc2
s	s	k7c7
dětmi	dítě	k1gFnPc7
Dětského	dětský	k2eAgInSc2d1
pěveckého	pěvecký	k2eAgInSc2d1
sboru	sbor	k1gInSc2
Československého	československý	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
čtyřiceti	čtyřicet	k4xCc7
roky	rok	k1gInPc7
zahynul	zahynout	k5eAaPmAgMnS
první	první	k4xOgMnSc1
kosmonaut	kosmonaut	k1gMnSc1
světa	svět	k1gInSc2
Jurij	Jurij	k1gFnSc2
Gagarin	Gagarin	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
2008-03-26	2008-03-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tiskové	tiskový	k2eAgNnSc1d1
prohlášení	prohlášení	k1gNnSc1
České	český	k2eAgFnSc2d1
astronomické	astronomický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
Astronomického	astronomický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
AV	AV	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
KONDZIOLKA	KONDZIOLKA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instantní	instantní	k2eAgInSc4d1
pamětník	pamětník	k1gInSc4
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instantní	instantní	k2eAgFnPc1d1
astronomické	astronomický	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-04-09	2007-04-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografie	fotografia	k1gFnSc2
a	a	k8xC
videa	video	k1gNnSc2
o	o	k7c6
Gagarinovi	Gagarin	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
6691	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KONDZIOLKA	KONDZIOLKA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
kdy	kdy	k6eAd1
svět	svět	k1gInSc1
slyšel	slyšet	k5eAaImAgInS
zprávu	zpráva	k1gFnSc4
TASSu	Tass	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instantní	instantní	k2eAgFnPc1d1
astronomické	astronomický	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-04-16	2006-04-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotogalerie	Fotogalerie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
6691	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Host	host	k1gMnSc1
Radiožurnálu	radiožurnál	k1gInSc2
(	(	kIx(
<g/>
12.04	12.04	k4
<g/>
.2011	.2011	k4
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiožurnál	radiožurnál	k1gInSc1
<g/>
,	,	kIx,
2011-04-12	2011-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Antonínem	Antonín	k1gMnSc7
Vítkem	Vítek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VOLDÁN	Voldán	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc1
–	–	k?
synonymum	synonymum	k1gNnSc4
světové	světový	k2eAgFnSc2d1
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haló	haló	k0
noviny	novina	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-12	2011-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Vladimírem	Vladimír	k1gMnSc7
Remkem	Remek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
ZUNA	zuna	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
–	–	k?
Den	den	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
první	první	k4xOgMnSc1
člověk	člověk	k1gMnSc1
vyletěl	vyletět	k5eAaPmAgMnS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
stream	stream	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Video	video	k1gNnSc1
o	o	k7c6
Gagarinově	Gagarinův	k2eAgInSc6d1
letu	let	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Rusky	Ruska	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
12	#num#	k4
а	а	k?
1961	#num#	k4
г	г	k?
-	-	kIx~
п	п	k?
п	п	k?
ч	ч	k?
в	в	k?
к	к	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
Roskosmos	Roskosmos	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
let	léto	k1gNnPc2
na	na	k7c6
webu	web	k1gInSc6
Roskosmosu	Roskosmos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
П	П	k?
к	к	k?
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdné	hvězdný	k2eAgNnSc1d1
městečko	městečko	k1gNnSc1
<g/>
:	:	kIx,
Středisko	středisko	k1gNnSc1
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
J.	J.	kA
A.	A.	kA
Gagarina	Gagarina	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
let	léto	k1gNnPc2
na	na	k7c6
webu	web	k1gInSc6
Střediska	středisko	k1gNnSc2
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
J.	J.	kA
A.	A.	kA
Gagarina	Gagarina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
а	а	k?
2011	#num#	k4
г	г	k?
–	–	k?
50	#num#	k4
<g/>
-л	-л	k?
п	п	k?
п	п	k?
ч	ч	k?
в	в	k?
к	к	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInSc1d1
vědecko-výzkumný	vědecko-výzkumný	k2eAgInSc1d1
institut	institut	k1gInSc1
strojírenství	strojírenství	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
let	léto	k1gNnPc2
na	na	k7c6
webu	web	k1gInSc6
Ústředního	ústřední	k2eAgInSc2d1
vědecko-výzkumného	vědecko-výzkumný	k2eAgInSc2d1
institutu	institut	k1gInSc2
strojírenství	strojírenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
12	#num#	k4
а	а	k?
1961	#num#	k4
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koroljov	Koroljov	k1gInSc1
<g/>
:	:	kIx,
RKK	RKK	kA
Eněrgija	Eněrgija	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
let	léto	k1gNnPc2
na	na	k7c6
webu	web	k1gInSc6
RKK	RKK	kA
Eněrgija	Eněrgijum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Ж	Ж	k?
<g/>
,	,	kIx,
А	А	k?
Б	Б	k?
<g/>
.	.	kIx.
Э	Э	k?
"	"	kIx"
<g/>
К	К	k?
<g/>
"	"	kIx"
<g/>
.	.	kIx.
П	П	k?
п	п	k?
ч	ч	k?
в	в	k?
к	к	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2009-12-13	2009-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gagarin	Gagarin	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnPc2
let	léto	k1gNnPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Encyklopedie	encyklopedie	k1gFnSc2
kosmonautiky	kosmonautika	k1gFnSc2
A.	A.	kA
Železňakova	Železňakův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700578	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118537105	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8387	#num#	k4
5419	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50015443	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500342574	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
61673822	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50015443	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
