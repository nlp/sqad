<s>
Jurij	Jurij	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
Gagarin	Gagarin	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Ю	Ю	k?
А	А	k?
Г	Г	k?
<g/>
;	;	kIx,
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1934	#num#	k4
Klušino	Klušin	k2eAgNnSc4d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Západní	západní	k2eAgMnPc1d1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Smolenská	Smolenský	k2eAgFnSc1d1
<g/>
)	)	kIx)
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
<g/>
,	,	kIx,
Novoselovo	Novoselův	k2eAgNnSc1d1
<g/>
,	,	kIx,
Vladimirská	Vladimirský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
sovětský	sovětský	k2eAgMnSc1d1
kosmonaut	kosmonaut	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vzlétl	vzlétnout	k5eAaPmAgMnS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>