<s>
jošitoši	jošitoš	k1gMnPc1	jošitoš
ABe	ABe	k1gFnSc2	ABe
(	(	kIx(	(
<g/>
安	安	k?	安
吉	吉	k?	吉
<g/>
;	;	kIx,	;
místo	místo	k7c2	místo
znaku	znak	k1gInSc2	znak
吉	吉	k?	吉
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
znak	znak	k1gInSc1	znak
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahoře	nahoře	k6eAd1	nahoře
místo	místo	k7c2	místo
士	士	k?	士
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
土	土	k?	土
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
japonský	japonský	k2eAgMnSc1d1	japonský
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tvorby	tvorba	k1gFnSc2	tvorba
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
