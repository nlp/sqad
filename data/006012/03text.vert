<s>
jošitoši	jošitoš	k1gMnPc1	jošitoš
ABe	ABe	k1gFnSc2	ABe
(	(	kIx(	(
<g/>
安	安	k?	安
吉	吉	k?	吉
<g/>
;	;	kIx,	;
místo	místo	k7c2	místo
znaku	znak	k1gInSc2	znak
吉	吉	k?	吉
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
znak	znak	k1gInSc1	znak
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahoře	nahoře	k6eAd1	nahoře
místo	místo	k7c2	místo
士	士	k?	士
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
土	土	k?	土
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
japonský	japonský	k2eAgMnSc1d1	japonský
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tvorby	tvorba	k1gFnSc2	tvorba
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c6	na
Tokijské	tokijský	k2eAgFnSc6d1	Tokijská
národní	národní	k2eAgFnSc6d1	národní
univerzitě	univerzita	k1gFnSc6	univerzita
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Známost	známost	k1gFnSc4	známost
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
práci	práce	k1gFnSc3	práce
na	na	k7c4	na
anime	animat	k5eAaPmIp3nS	animat
Serial	Serial	k1gInSc1	Serial
Experiments	Experimentsa	k1gFnPc2	Experimentsa
Lain	Laina	k1gFnPc2	Laina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
původního	původní	k2eAgInSc2d1	původní
konceptu	koncept	k1gInSc2	koncept
a	a	k8xC	a
návrhu	návrh	k1gInSc3	návrh
postav	postava	k1gFnPc2	postava
série	série	k1gFnSc2	série
NieA	NieA	k1gMnSc1	NieA
under	under	k1gMnSc1	under
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
dódžinši	dódžinše	k1gFnSc4	dódžinše
Haibane	Haiban	k1gMnSc5	Haiban
renmei	renmei	k1gNnPc3	renmei
a	a	k8xC	a
scénář	scénář	k1gInSc1	scénář
anime	animat	k5eAaPmIp3nS	animat
seriálu	seriál	k1gInSc2	seriál
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
manze	manza	k1gFnSc6	manza
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
také	také	k9	také
na	na	k7c4	na
sérii	série	k1gFnSc4	série
Texhnolyze	Texhnolyze	k1gFnSc2	Texhnolyze
a	a	k8xC	a
NHK	NHK	kA	NHK
ni	on	k3xPp3gFnSc4	on
jókoso	jókoso	k6eAd1	jókoso
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
místo	místo	k1gNnSc1	místo
kandži	kandzat	k5eAaPmIp1nS	kandzat
přepis	přepis	k1gInSc1	přepis
rómadži	rómadzat	k5eAaPmIp1nS	rómadzat
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
do	do	k7c2	do
latinky	latinka	k1gFnSc2	latinka
(	(	kIx(	(
<g/>
jošitoši	jošitoš	k1gMnPc1	jošitoš
ABe	ABe	k1gFnSc2	ABe
<g/>
)	)	kIx)	)
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
příjmení	příjmení	k1gNnSc6	příjmení
a	a	k8xC	a
malým	malý	k2eAgInSc7d1	malý
"	"	kIx"	"
<g/>
j	j	k?	j
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
přepisu	přepis	k1gInSc6	přepis
"	"	kIx"	"
<g/>
y	y	k?	y
<g/>
"	"	kIx"	"
-	-	kIx~	-
yoshitoshi	yoshitoshi	k6eAd1	yoshitoshi
<g/>
)	)	kIx)	)
ve	v	k7c6	v
jméně	jméno	k1gNnSc6	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jošitoši	Jošitoš	k1gMnPc1	Jošitoš
ABe	ABe	k1gFnPc4	ABe
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
ABeho	ABe	k1gMnSc2	ABe
domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
ABeho	ABe	k1gMnSc2	ABe
blog	blog	k1gMnSc1	blog
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
Jošitoši	Jošitoš	k1gMnPc1	Jošitoš
ABe	ABe	k1gFnPc2	ABe
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Anime	Anim	k1gInSc5	Anim
News	Newsa	k1gFnPc2	Newsa
Network	network	k1gInSc1	network
</s>
