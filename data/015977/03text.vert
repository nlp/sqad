<s>
Triclosan	Triclosan	k1gMnSc1
</s>
<s>
Triclosan	Triclosan	k1gMnSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
-chlor-	-chlor-	k?
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
2,4	2,4	k4
<g/>
-dichlorfenoxy	-dichlorfenox	k1gInPc4
<g/>
)	)	kIx)
<g/>
fenol	fenol	k1gInSc4
</s>
<s>
Triviální	triviální	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
triclosan	triclosan	k1gMnSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
triklosan	triklosan	k1gMnSc1
<g/>
,	,	kIx,
2,4	2,4	k4
<g/>
,4	,4	k4
<g/>
'	'	kIx"
<g/>
-trichlor-	-trichlor-	k?
<g/>
2	#num#	k4
<g/>
'	'	kIx"
<g/>
-hydroxydifenylether	-hydroxydifenylethra	k1gFnPc2
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Triclosan	Triclosan	k1gMnSc1
</s>
<s>
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Triclosan	Triclosan	k1gMnSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
C	C	kA
<g/>
12	#num#	k4
<g/>
H	H	kA
<g/>
7	#num#	k4
<g/>
Cl	Cl	k1gFnSc2
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
2	#num#	k4
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
bílý	bílý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
3380-34-5	3380-34-5	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
5564	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
C	C	kA
<g/>
1	#num#	k4
<g/>
=	=	kIx~
<g/>
CC	CC	kA
<g/>
(	(	kIx(
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
1	#num#	k4
<g/>
Cl	Cl	k1gFnPc1
<g/>
)	)	kIx)
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
OC	OC	kA
<g/>
2	#num#	k4
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Cl	Cl	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Cl	Cl	k1gFnSc1
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
289,54	289,54	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
55-57	55-57	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
120	#num#	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Dráždivý	dráždivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Xi	Xi	k1gMnSc1
<g/>
)	)	kIx)
Nebezpečný	bezpečný	k2eNgMnSc1d1
pro	pro	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
(	(	kIx(
<g/>
N	N	kA
<g/>
)	)	kIx)
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
R	R	kA
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
R	R	kA
<g/>
50	#num#	k4
<g/>
/	/	kIx~
<g/>
53	#num#	k4
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S26	S26	k4
S39	S39	k1gMnSc1
S46	S46	k1gMnSc1
S60	S60	k1gMnSc1
S61	S61	k1gMnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Triclosan	Triclosan	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
triklosan	triklosan	k1gInSc1
<g/>
,	,	kIx,
systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
5	#num#	k4
<g/>
-chlor-	-chlor-	k?
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
2,4	2,4	k4
<g/>
-dichlorfenoxy	-dichlorfenox	k1gInPc4
<g/>
)	)	kIx)
<g/>
fenol	fenol	k1gInSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
silné	silný	k2eAgNnSc4d1
protibakteriální	protibakteriální	k2eAgNnSc4d1
a	a	k8xC
protihoubové	protihoubový	k2eAgNnSc4d1
činidlo	činidlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
mýdlo	mýdlo	k1gNnSc4
ani	ani	k8xC
jiné	jiný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
s	s	k7c7
triclosanem	triclosan	k1gInSc7
nemají	mít	k5eNaImIp3nP
proti	proti	k7c3
bakteriím	bakterium	k1gNnPc3
lepší	lepšit	k5eAaImIp3nS
účinnost	účinnost	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
naopak	naopak	k6eAd1
při	při	k7c6
výskytu	výskyt	k1gInSc6
Triclosanu	Triclosan	k1gInSc2
v	v	k7c6
těle	tělo	k1gNnSc6
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
odolnost	odolnost	k1gFnSc1
(	(	kIx(
<g/>
rezistence	rezistence	k1gFnSc1
<g/>
)	)	kIx)
bakterií	bakterie	k1gFnPc2
proti	proti	k7c3
léčbě	léčba	k1gFnSc3
antibiotiky	antibiotikum	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
a	a	k8xC
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tato	tento	k3xDgFnSc1
organická	organický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
je	být	k5eAaImIp3nS
bílý	bílý	k2eAgInSc4d1
prášek	prášek	k1gInSc4
slabého	slabý	k2eAgInSc2d1
aromatického	aromatický	k2eAgInSc2d1
<g/>
/	/	kIx~
<g/>
fenolového	fenolový	k2eAgInSc2d1
pachu	pach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
chlorovaná	chlorovaný	k2eAgFnSc1d1
aromatická	aromatický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgInPc1d1
jak	jak	k8xS,k8xC
etherové	etherový	k2eAgInPc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
fenolové	fenolový	k2eAgFnPc1d1
funkční	funkční	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fenoly	fenol	k1gInPc7
mají	mít	k5eAaImIp3nP
často	často	k6eAd1
antibakteriální	antibakteriální	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triclosan	Triclosan	k1gInSc1
je	být	k5eAaImIp3nS
jen	jen	k9
mírně	mírně	k6eAd1
rozpustný	rozpustný	k2eAgMnSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
dobře	dobře	k6eAd1
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
v	v	k7c6
ethanolu	ethanol	k1gInSc6
<g/>
,	,	kIx,
diethyletheru	diethylether	k1gInSc6
a	a	k8xC
silných	silný	k2eAgFnPc6d1
zásadách	zásada	k1gFnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
1M	1M	k4
hydroxidu	hydroxid	k1gInSc6
sodném	sodný	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triclosan	Triclosany	k1gInPc2
lze	lze	k6eAd1
získat	získat	k5eAaPmF
částečnou	částečný	k2eAgFnSc7d1
oxidací	oxidace	k1gFnSc7
benzenu	benzen	k1gInSc2
nebo	nebo	k8xC
kyseliny	kyselina	k1gFnSc2
benzoové	benzoový	k2eAgFnSc2d1
<g/>
,	,	kIx,
kumenovým	kumenův	k2eAgInSc7d1
procesem	proces	k1gInSc7
nebo	nebo	k8xC
Raschigovým	Raschigův	k2eAgInSc7d1
procesem	proces	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
jako	jako	k9
produkt	produkt	k1gInSc4
oxidace	oxidace	k1gFnSc2
uhlí	uhlí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Triclosan	Triclosan	k1gInSc1
se	se	k3xPyFc4
přidává	přidávat	k5eAaImIp3nS
do	do	k7c2
mýdel	mýdlo	k1gNnPc2
(	(	kIx(
<g/>
0,15	0,15	k4
-	-	kIx~
0,30	0,30	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
deodorantů	deodorant	k1gInPc2
<g/>
,	,	kIx,
past	past	k1gFnSc1
na	na	k7c4
zuby	zub	k1gInPc4
<g/>
,	,	kIx,
holicích	holicí	k2eAgMnPc2d1
krémů	krém	k1gInPc2
<g/>
,	,	kIx,
ústních	ústní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
a	a	k8xC
čisticích	čisticí	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
též	též	k9
napouštěn	napouštěn	k2eAgMnSc1d1
do	do	k7c2
čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
dál	daleko	k6eAd2
většího	veliký	k2eAgInSc2d2
počtu	počet	k1gInSc2
spotřebitelských	spotřebitelský	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
kuchyňských	kuchyňský	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
hraček	hračka	k1gFnPc2
<g/>
,	,	kIx,
ložního	ložní	k2eAgNnSc2d1
prádla	prádlo	k1gNnSc2
<g/>
,	,	kIx,
ponožek	ponožka	k1gFnPc2
a	a	k8xC
odpadkových	odpadkový	k2eAgInPc2d1
pytlů	pytel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triclosan	Triclosan	k1gMnSc1
se	se	k3xPyFc4
jevil	jevit	k5eAaImAgMnS
účinným	účinný	k2eAgMnSc7d1
ve	v	k7c6
snižování	snižování	k1gNnSc3
bakteriální	bakteriální	k2eAgFnSc2d1
kontaminace	kontaminace	k1gFnSc2
rukou	ruka	k1gFnPc2
a	a	k8xC
ošetřených	ošetřený	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sprchování	sprchování	k1gNnSc1
nebo	nebo	k8xC
koupání	koupání	k1gNnSc1
ve	v	k7c6
2	#num#	k4
<g/>
%	%	kIx~
triclosanu	triclosanout	k5eAaPmIp1nS
se	se	k3xPyFc4
nedávno	nedávno	k6eAd1
stalo	stát	k5eAaPmAgNnS
doporučeným	doporučený	k2eAgInSc7d1
režimem	režim	k1gInSc7
pro	pro	k7c4
dekolonizaci	dekolonizace	k1gFnSc4
pacientů	pacient	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
kůže	kůže	k1gFnSc1
je	být	k5eAaImIp3nS
osídlena	osídlit	k5eAaPmNgFnS
bakterií	bakterie	k1gFnSc7
Staphylococcus	Staphylococcus	k1gMnSc1
aureus	aureus	k1gMnSc1
rezistentní	rezistentní	k2eAgMnSc1d1
na	na	k7c4
meticilin	meticilin	k1gInSc4
(	(	kIx(
<g/>
Meticilin-rezistentní	Meticilin-rezistentní	k2eAgInSc4d1
zlatý	zlatý	k2eAgInSc4d1
stafylokok	stafylokok	k1gInSc4
<g/>
,	,	kIx,
MRSA	MRSA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
zvládnutí	zvládnutí	k1gNnSc6
bujení	bujení	k1gNnPc2
MRSA	MRSA	kA
v	v	k7c6
několika	několik	k4yIc2
zdravotnických	zdravotnický	k2eAgNnPc6d1
zařízeních	zařízení	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Triclosan	Triclosan	k1gInSc1
je	být	k5eAaImIp3nS
regulován	regulovat	k5eAaImNgInS
americkými	americký	k2eAgInPc7d1
úřady	úřad	k1gInPc7
FDA	FDA	kA
a	a	k8xC
EPA	EPA	kA
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
Evropskou	evropský	k2eAgFnSc7d1
unií	unie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zpracování	zpracování	k1gNnSc6
odpadních	odpadní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
část	část	k1gFnSc1
triclosanu	triclosana	k1gFnSc4
degraduje	degradovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
je	být	k5eAaImIp3nS
pohlcen	pohltit	k5eAaPmNgInS
do	do	k7c2
kalu	kal	k1gInSc2
nebo	nebo	k8xC
odtéká	odtékat	k5eAaImIp3nS
s	s	k7c7
pročištěnou	pročištěný	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rozkládán	rozkládán	k2eAgInSc4d1
mikroorganismy	mikroorganismus	k1gInPc4
nebo	nebo	k8xC
reagovat	reagovat	k5eAaBmF
vlivem	vliv	k1gInSc7
slunečního	sluneční	k2eAgNnSc2d1
záření	záření	k1gNnSc2
za	za	k7c2
vzniku	vznik	k1gInSc2
různých	různý	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
včetně	včetně	k7c2
chlorfenolů	chlorfenol	k1gInPc2
a	a	k8xC
dioxinů	dioxin	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
navázat	navázat	k5eAaPmF
na	na	k7c4
částice	částice	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
usazují	usazovat	k5eAaImIp3nP
na	na	k7c6
dně	dno	k1gNnSc6
vodního	vodní	k2eAgInSc2d1
sloupce	sloupec	k1gInSc2
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
sediment	sediment	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Triclosan	Triclosan	k1gInSc1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
2002	#num#	k4
nalezen	naleznout	k5eAaPmNgMnS,k5eAaBmNgMnS
v	v	k7c6
sedimentu	sediment	k1gInSc6
švýcarského	švýcarský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
Greifensee	Greifense	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
let	léto	k1gNnPc2
starý	starý	k2eAgMnSc1d1
-	-	kIx~
to	ten	k3xDgNnSc1
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
triclosan	triclosan	k1gInSc1
rozkládá	rozkládat	k5eAaImIp3nS
a	a	k8xC
mizí	mizet	k5eAaImIp3nS
ze	z	k7c2
sedimentu	sediment	k1gInSc2
velmi	velmi	k6eAd1
pomalu	pomalu	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Patent	patent	k1gInSc1
na	na	k7c4
triclosan	triclosan	k1gInSc4
získala	získat	k5eAaPmAgFnS
roku	rok	k1gInSc2
1964	#num#	k4
švýcarská	švýcarský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Ciba-Geigy	Ciba-Geiga	k1gFnSc2
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Novartis	Novartis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1
účinku	účinek	k1gInSc2
</s>
<s>
Při	při	k7c6
používaných	používaný	k2eAgFnPc6d1
koncentracích	koncentrace	k1gFnPc6
funguje	fungovat	k5eAaImIp3nS
triclosan	triclosan	k1gInSc1
jako	jako	k8xC,k8xS
biocid	biocid	k1gInSc1
s	s	k7c7
více	hodně	k6eAd2
cytoplazmatickými	cytoplazmatický	k2eAgInPc7d1
a	a	k8xC
membránovými	membránový	k2eAgInPc7d1
cíli	cíl	k1gInPc7
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nižších	nízký	k2eAgFnPc6d2
koncentracích	koncentrace	k1gFnPc6
je	být	k5eAaImIp3nS
však	však	k9
triclosan	triclosan	k1gInSc1
jen	jen	k6eAd1
bakteriostatický	bakteriostatický	k2eAgInSc1d1
a	a	k8xC
působí	působit	k5eAaImIp3nS
na	na	k7c4
bakterie	bakterie	k1gFnPc4
hlavně	hlavně	k9
inhibicí	inhibice	k1gFnSc7
syntézy	syntéza	k1gFnSc2
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triclosan	Triclosan	k1gInSc1
se	se	k3xPyFc4
váže	vázat	k5eAaImIp3nS
na	na	k7c4
bakteriální	bakteriální	k2eAgFnSc4d1
reduktázu	reduktáza	k1gFnSc4
nosiče	nosič	k1gInSc2
enoyl-acyl	enoyl-acyl	k1gInSc1
bílkoviny	bílkovina	k1gFnSc2
(	(	kIx(
<g/>
ENR	ENR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
kódována	kódovat	k5eAaBmNgFnS
v	v	k7c6
genu	gen	k1gInSc6
FabI	FabI	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vazba	vazba	k1gFnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
afinitu	afinita	k1gFnSc4
enzymu	enzym	k1gInSc2
k	k	k7c3
nikotinamidadenindinukleotidu	nikotinamidadenindinukleotid	k1gInSc3
(	(	kIx(
<g/>
NAD	NAD	kA
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
stabilního	stabilní	k2eAgInSc2d1
ternárního	ternární	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
ENR-NAD	ENR-NAD	k1gFnSc2
<g/>
+	+	kIx~
<g/>
-triclosan	-triclosan	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
nemůže	moct	k5eNaImIp3nS
účastnit	účastnit	k5eAaImF
v	v	k7c6
syntéze	syntéza	k1gFnSc6
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mastné	mastný	k2eAgFnPc4d1
kyseliny	kyselina	k1gFnPc4
jsou	být	k5eAaImIp3nP
nezbytné	nezbytný	k2eAgInPc1d1,k2eNgInPc1d1
pro	pro	k7c4
reprodukci	reprodukce	k1gFnSc4
a	a	k8xC
stavbu	stavba	k1gFnSc4
buněčných	buněčný	k2eAgFnPc2d1
membrán	membrána	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
nemá	mít	k5eNaImIp3nS
enzym	enzym	k1gInSc4
ENR	ENR	kA
<g/>
,	,	kIx,
proto	proto	k8xC
není	být	k5eNaImIp3nS
triclosanem	triclosan	k1gMnSc7
takto	takto	k6eAd1
ovlivněn	ovlivněn	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
druhy	druh	k1gInPc4
bakterií	bakterie	k1gFnPc2
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
vyvinout	vyvinout	k5eAaPmF
nízkoúrovňovou	nízkoúrovňový	k2eAgFnSc4d1
rezistenci	rezistence	k1gFnSc4
na	na	k7c4
triclosan	triclosan	k1gInSc4
mutací	mutace	k1gFnPc2
genu	gen	k1gInSc2
FabI	FabI	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
snižuje	snižovat	k5eAaImIp3nS
účinek	účinek	k1gInSc4
triclosanu	triclosan	k1gInSc2
na	na	k7c4
vazbu	vazba	k1gFnSc4
ENR-NAD	ENR-NAD	k1gFnSc2
<g/>
+	+	kIx~
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
u	u	k7c2
bakterií	bakterium	k1gNnPc2
Escherichia	Escherichius	k1gMnSc2
coli	col	k1gFnSc2
a	a	k8xC
Staphylococcus	Staphylococcus	k1gMnSc1
aureus	aureus	k1gMnSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiným	jiný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
mohou	moct	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
bakterie	bakterie	k1gFnPc4
získat	získat	k5eAaPmF
nízkoúrovňovou	nízkoúrovňový	k2eAgFnSc4d1
rezistenci	rezistence	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zvýšená	zvýšený	k2eAgFnSc1d1
exprese	exprese	k1gFnSc1
FabI	FabI	k1gFnSc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
bakterie	bakterie	k1gFnSc2
mají	mít	k5eAaImIp3nP
vrozenou	vrozený	k2eAgFnSc4d1
rezistenci	rezistence	k1gFnSc4
na	na	k7c4
triclosan	triclosan	k1gInSc4
<g/>
,	,	kIx,
například	například	k6eAd1
Pseudomonas	Pseudomonas	k1gInSc1
aeruginosa	aeruginosa	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
vícelátkové	vícelátkový	k2eAgFnPc4d1
odtokové	odtokový	k2eAgFnPc4d1
pumpy	pumpa	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
"	"	kIx"
<g/>
vyčerpávají	vyčerpávat	k5eAaImIp3nP
<g/>
"	"	kIx"
triclosan	triclosan	k1gInSc1
ven	ven	k6eAd1
z	z	k7c2
buňky	buňka	k1gFnSc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgFnPc4d1
bakterie	bakterie	k1gFnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
z	z	k7c2
rodu	rod	k1gInSc2
Bacillus	Bacillus	k1gInSc4
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
alternativní	alternativní	k2eAgFnSc2d1
FabI	FabI	k1gFnSc2
geny	gen	k1gInPc1
(	(	kIx(
<g/>
FabK	FabK	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
triclosan	triclosan	k1gInSc1
neváže	vázat	k5eNaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc1
bakterie	bakterie	k1gFnPc1
méně	málo	k6eAd2
citlivé	citlivý	k2eAgFnPc1d1
na	na	k7c4
triclosan	triclosan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Tvorba	tvorba	k1gFnSc1
dioxinů	dioxin	k1gInPc2
v	v	k7c6
povrchových	povrchový	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
</s>
<s>
Používáním	používání	k1gNnSc7
triclosanu	triclosan	k1gInSc2
v	v	k7c6
antibakteriálních	antibakteriální	k2eAgInPc6d1
výrobcích	výrobek	k1gInPc6
pro	pro	k7c4
domácnost	domácnost	k1gFnSc4
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
chemikálie	chemikálie	k1gFnSc1
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
povrchových	povrchový	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
může	moct	k5eAaImIp3nS
tvořit	tvořit	k5eAaImF
dioxiny	dioxin	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dioxinové	dioxinový	k2eAgFnPc4d1
sloučeniny	sloučenina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
vznikají	vznikat	k5eAaImIp3nP
rozkladem	rozklad	k1gInSc7
triclosanu	triclosan	k1gInSc2
slunečním	sluneční	k2eAgNnPc3d1
zářením	záření	k1gNnPc3
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
ukázány	ukázat	k5eAaPmNgInP
ve	v	k7c6
studii	studie	k1gFnSc6
vědců	vědec	k1gMnPc2
z	z	k7c2
Virginia	Virginium	k1gNnSc2
Tech	Tech	k?
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
důvod	důvod	k1gInSc4
k	k	k7c3
obavám	obava	k1gFnPc3
o	o	k7c4
veřejné	veřejný	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dioxin	dioxin	k1gInSc1
není	být	k5eNaImIp3nS
jedna	jeden	k4xCgFnSc1
sloučenina	sloučenina	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
rodina	rodina	k1gFnSc1
sloučenin	sloučenina	k1gFnPc2
široké	široký	k2eAgFnSc2d1
škály	škála	k1gFnSc2
toxicity	toxicita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
210	#num#	k4
členů	člen	k1gInPc2
rodiny	rodina	k1gFnSc2
dioxinů	dioxin	k1gInPc2
a	a	k8xC
furanů	furan	k1gInPc2
jen	jen	k9
17	#num#	k4
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
nebezpečné	bezpečný	k2eNgInPc4d1
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Problematika	problematika	k1gFnSc1
rezistence	rezistence	k1gFnSc2
</s>
<s>
Článek	článek	k1gInSc1
v	v	k7c6
časopisu	časopis	k1gInSc6
Nature	Natur	k1gMnSc5
z	z	k7c2
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1998	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
spoluautorem	spoluautor	k1gMnSc7
byl	být	k5eAaImAgInS
Dr	dr	kA
<g/>
.	.	kIx.
Stuart	Stuarta	k1gFnPc2
Levy	Levy	k?
varoval	varovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nadužívání	nadužívání	k1gNnSc1
triclosanu	triclosan	k1gInSc2
může	moct	k5eAaImIp3nS
u	u	k7c2
bakterií	bakterie	k1gFnPc2
způsobit	způsobit	k5eAaPmF
rozvoj	rozvoj	k1gInSc4
antibiotické	antibiotický	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
zásadě	zásada	k1gFnSc6
stejnou	stejný	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
jako	jako	k8xC,k8xS
u	u	k7c2
antibiotik	antibiotikum	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
základě	základ	k1gInSc6
spekulace	spekulace	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
triclosan	triclosan	k1gInSc1
chová	chovat	k5eAaImIp3nS
jako	jako	k9
antibiotikum	antibiotikum	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
základě	základ	k1gInSc6
této	tento	k3xDgFnSc2
spekulace	spekulace	k1gFnSc2
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
Sunday	Sundaa	k1gFnSc2
Herald	Heralda	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
některé	některý	k3yIgInPc1
britské	britský	k2eAgInPc1d1
supermarkety	supermarket	k1gInPc1
a	a	k8xC
jiní	jiný	k2eAgMnPc1d1
prodejci	prodejce	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
ukončit	ukončit	k5eAaPmF
prodej	prodej	k1gInSc4
výrobků	výrobek	k1gInPc2
obsahujících	obsahující	k2eAgFnPc2d1
triclosan	triclosana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byl	být	k5eAaImAgInS
však	však	k9
potvrzen	potvrzen	k2eAgInSc1d1
možný	možný	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
rezistence	rezistence	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Triclosan	Triclosan	k1gInSc1
je	být	k5eAaImIp3nS
neúčinný	účinný	k2eNgInSc1d1
a	a	k8xC
bakterie	bakterie	k1gFnPc1
navíc	navíc	k6eAd1
získávají	získávat	k5eAaImIp3nP
rezistenci	rezistence	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
však	však	k9
také	také	k9
<g/>
,	,	kIx,
že	že	k8xS
laboratorní	laboratorní	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
použitá	použitý	k2eAgFnSc1d1
Dr	dr	kA
<g/>
.	.	kIx.
Levym	Levym	k?
nebyla	být	k5eNaImAgFnS
efektivní	efektivní	k2eAgFnSc1d1
v	v	k7c6
predikci	predikce	k1gFnSc6
bakteriální	bakteriální	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
na	na	k7c4
biocidy	biocida	k1gFnPc4
jako	jako	k8xS,k8xC
triclosan	triclosan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
informace	informace	k1gFnPc4
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
na	na	k7c4
práci	práce	k1gFnSc4
Dr	dr	kA
<g/>
.	.	kIx.
Petera	Peter	k1gMnSc2
Gilberta	Gilbert	k1gMnSc2
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
výzkum	výzkum	k1gInSc1
byl	být	k5eAaImAgInS
ale	ale	k8xC
podporován	podporovat	k5eAaImNgInS
firmou	firma	k1gFnSc7
Procter	Proctra	k1gFnPc2
&	&	k?
Gamble	Gamble	k1gMnSc1
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
nejméně	málo	k6eAd3
sedmi	sedm	k4xCc6
recenzovaných	recenzovaný	k2eAgFnPc2d1
a	a	k8xC
publikovaných	publikovaný	k2eAgFnPc2d1
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
jedné	jeden	k4xCgFnSc2
studie	studie	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
i	i	k9
Dr	dr	kA
<g/>
.	.	kIx.
Levy	Levy	k?
a	a	k8xC
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
publikována	publikovat	k5eAaBmNgFnS
v	v	k7c6
srpnu	srpen	k1gInSc6
2004	#num#	k4
v	v	k7c4
Antimicrobial	Antimicrobial	k1gInSc4
Agents	Agents	k1gInSc1
and	and	k?
Chemotherapy	Chemotherapa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
bylo	být	k5eAaImAgNnS
ukázáno	ukázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
triclosan	triclosan	k1gInSc1
není	být	k5eNaImIp3nS
významně	významně	k6eAd1
spojen	spojen	k2eAgInSc1d1
s	s	k7c7
bakteriální	bakteriální	k2eAgFnSc7d1
rezistencí	rezistence	k1gFnSc7
jako	jako	k8xC,k8xS
takovou	takový	k3xDgFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Určitá	určitý	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
rezistence	rezistence	k1gFnSc2
na	na	k7c4
triclosan	triclosan	k1gInSc4
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
u	u	k7c2
některých	některý	k3yIgInPc2
mikroorganismů	mikroorganismus	k1gInPc2
objevit	objevit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
větší	veliký	k2eAgFnPc1d2
obavy	obava	k1gFnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
potenciální	potenciální	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
nebo	nebo	k8xC
korezistence	korezistence	k1gFnSc2
na	na	k7c4
jiné	jiný	k2eAgFnPc4d1
antimikrobiální	antimikrobiální	k2eAgFnPc4d1
látky	látka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
zkoumající	zkoumající	k2eAgFnSc1d1
tuto	tento	k3xDgFnSc4
možnost	možnost	k1gFnSc4
proběhly	proběhnout	k5eAaPmAgFnP
zatím	zatím	k6eAd1
jen	jen	k9
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Získání	získání	k1gNnSc1
křížové	křížový	k2eAgFnSc2d1
rezistence	rezistence	k1gFnSc2
je	být	k5eAaImIp3nS
však	však	k9
také	také	k9
možné	možný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vliv	vliv	k1gInSc1
na	na	k7c4
zdraví	zdraví	k1gNnSc4
</s>
<s>
Některé	některý	k3yIgFnPc1
zprávy	zpráva	k1gFnPc1
naznačily	naznačit	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
triclosan	triclosan	k1gInSc1
může	moct	k5eAaImIp3nS
ve	v	k7c6
vodovodní	vodovodní	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
reagovat	reagovat	k5eAaBmF
s	s	k7c7
chlorem	chlor	k1gInSc7
za	za	k7c2
vzniku	vznik	k1gInSc2
chloroformu	chloroform	k1gInSc2
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
klasifikován	klasifikován	k2eAgInSc1d1
EPA	EPA	kA
jako	jako	k8xC,k8xS
pravděpodobný	pravděpodobný	k2eAgInSc1d1
karcinogen	karcinogen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
triclosan	triclosan	k1gMnSc1
stal	stát	k5eAaPmAgMnS
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
cílem	cíl	k1gInSc7
varování	varování	k1gNnSc2
ohledně	ohledně	k7c2
rakoviny	rakovina	k1gFnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
studie	studie	k1gFnPc1
ukázaly	ukázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
množství	množství	k1gNnSc1
vznikajícího	vznikající	k2eAgInSc2d1
chloroformu	chloroform	k1gInSc2
je	být	k5eAaImIp3nS
menší	malý	k2eAgNnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
množství	množství	k1gNnSc1
běžně	běžně	k6eAd1
přítomné	přítomný	k2eAgNnSc1d1
v	v	k7c6
chlorovaných	chlorovaný	k2eAgFnPc6d1
pitných	pitný	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Triclosan	Triclosan	k1gInSc1
reaguje	reagovat	k5eAaBmIp3nS
s	s	k7c7
volným	volný	k2eAgInSc7d1
chlorem	chlor	k1gInSc7
ve	v	k7c6
vodě	voda	k1gFnSc6
také	také	k9
za	za	k7c2
vzniku	vznik	k1gInSc2
menších	malý	k2eAgNnPc2d2
množství	množství	k1gNnPc2
dalších	další	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
2,4	2,4	k4
<g/>
-dichlorfenolu	-dichlorfenol	k1gInSc2
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
látek	látka	k1gFnPc2
přechází	přecházet	k5eAaImIp3nS
při	při	k7c6
expozici	expozice	k1gFnSc6
UV	UV	kA
záření	záření	k1gNnSc2
(	(	kIx(
<g/>
ze	z	k7c2
Slunce	slunce	k1gNnSc2
nebo	nebo	k8xC
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
)	)	kIx)
v	v	k7c4
dioxiny	dioxin	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
vznikají	vznikat	k5eAaImIp3nP
jen	jen	k9
malá	malý	k2eAgNnPc1d1
množství	množství	k1gNnPc1
dioxinů	dioxin	k1gInPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
ohledně	ohledně	k7c2
toho	ten	k3xDgNnSc2
velké	velký	k2eAgFnPc4d1
obavy	obava	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
dioxiny	dioxin	k1gInPc1
jsou	být	k5eAaImIp3nP
extrémně	extrémně	k6eAd1
jedovaté	jedovatý	k2eAgFnPc1d1
a	a	k8xC
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
velmi	velmi	k6eAd1
silné	silný	k2eAgInPc1d1
endokrinní	endokrinní	k2eAgInPc1d1
disruptory	disruptor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
také	také	k9
chemicky	chemicky	k6eAd1
velmi	velmi	k6eAd1
stabilní	stabilní	k2eAgFnSc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
z	z	k7c2
těla	tělo	k1gNnSc2
eliminují	eliminovat	k5eAaBmIp3nP
velmi	velmi	k6eAd1
pomalu	pomalu	k6eAd1
(	(	kIx(
<g/>
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
akumulovat	akumulovat	k5eAaBmF
na	na	k7c4
nebezpečné	bezpečný	k2eNgFnPc4d1
úrovně	úroveň	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
přetrvávají	přetrvávat	k5eAaImIp3nP
v	v	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
velmi	velmi	k6eAd1
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dioxin	dioxin	k1gInSc4
však	však	k9
není	být	k5eNaImIp3nS
jediná	jediný	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
skupina	skupina	k1gFnSc1
sloučenin	sloučenina	k1gFnPc2
s	s	k7c7
širokým	široký	k2eAgInSc7d1
rozsahem	rozsah	k1gInSc7
toxicity	toxicita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dioxiny	dioxin	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
vznikají	vznikat	k5eAaImIp3nP
při	při	k7c6
rozkladu	rozklad	k1gInSc6
triclosanu	triclosanout	k5eAaPmIp1nS
slunečním	sluneční	k2eAgNnSc7d1
zářením	záření	k1gNnSc7
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
zdraví	zdraví	k1gNnSc4
nebezpečné	bezpečný	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Triclosan	Triclosan	k1gInSc1
je	být	k5eAaImIp3nS
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
chemicky	chemicky	k6eAd1
podobný	podobný	k2eAgMnSc1d1
k	k	k7c3
dioxinové	dioxinový	k2eAgFnSc3d1
třídě	třída	k1gFnSc3
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
výroba	výroba	k1gFnSc1
vede	vést	k5eAaImIp3nS
ke	k	k7c3
vzniku	vznik	k1gInSc3
malých	malý	k2eAgInPc2d1
množství	množství	k1gNnSc1
zbytkových	zbytkový	k2eAgInPc2d1
polychlorovaných	polychlorovaný	k2eAgInPc2d1
dioxinů	dioxin	k1gInPc2
a	a	k8xC
polychlorovaných	polychlorovaný	k2eAgInPc2d1
furanů	furan	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
v	v	k7c6
malém	malé	k1gNnSc6
množství	množství	k1gNnSc2
obsaženy	obsažen	k2eAgFnPc1d1
ve	v	k7c6
výrobcích	výrobek	k1gInPc6
s	s	k7c7
triclosanem	triclosan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Studie	studie	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
malé	malý	k2eAgFnPc1d1
dávky	dávka	k1gFnPc1
triclosanu	triclosan	k1gInSc2
působí	působit	k5eAaImIp3nP
u	u	k7c2
severoamerického	severoamerický	k2eAgMnSc2d1
skokana	skokan	k1gMnSc2
volského	volský	k2eAgMnSc2d1
jako	jako	k8xC,k8xS
endokrinní	endokrinní	k2eAgInSc1d1
disruptor	disruptor	k1gInSc1
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hypotéza	hypotéza	k1gFnSc1
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
triclosan	triclosan	k1gInSc1
blokuje	blokovat	k5eAaImIp3nS
metabolismus	metabolismus	k1gInSc4
hormonu	hormon	k1gInSc2
štítné	štítný	k2eAgFnSc2d1
žlázy	žláza	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
chemicky	chemicky	k6eAd1
podobá	podobat	k5eAaImIp3nS
tomuto	tento	k3xDgInSc3
hormonu	hormon	k1gInSc3
<g/>
,	,	kIx,
váže	vázat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
hormonální	hormonální	k2eAgInPc4d1
receptory	receptor	k1gInPc4
a	a	k8xC
blokuje	blokovat	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
normální	normální	k2eAgInPc1d1
hormony	hormon	k1gInPc1
neúčinkují	účinkovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triclosan	Triclosan	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k9
nalezen	naleznout	k5eAaPmNgInS,k5eAaBmNgInS
jak	jak	k6eAd1
ve	v	k7c6
žluči	žluč	k1gFnSc6
ryb	ryba	k1gFnPc2
žijících	žijící	k2eAgFnPc2d1
ve	v	k7c6
vodních	vodní	k2eAgInPc6d1
tocích	tok	k1gInPc6
pod	pod	k7c7
čistírnami	čistírna	k1gFnPc7
odpadních	odpadní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
lidském	lidský	k2eAgNnSc6d1
mateřském	mateřský	k2eAgNnSc6d1
mléce	mléko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Negativní	negativní	k2eAgInPc4d1
účinky	účinek	k1gInPc4
triclosanu	triclosanout	k5eAaPmIp1nS
na	na	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
sporné	sporný	k2eAgInPc1d1
přínosy	přínos	k1gInPc1
v	v	k7c6
pastách	pasta	k1gFnPc6
na	na	k7c4
zuby	zub	k1gInPc4
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
vedly	vést	k5eAaImAgFnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Švédská	švédský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
Naturskyddsföreningen	Naturskyddsföreningen	k1gInSc1
<g/>
)	)	kIx)
nedoporučuje	doporučovat	k5eNaImIp3nS
triclosan	triclosan	k1gMnSc1
v	v	k7c6
zubních	zubní	k2eAgFnPc6d1
pastách	pasta	k1gFnPc6
používat	používat	k5eAaImF
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Triclosan	Triclosan	k1gMnSc1
se	se	k3xPyFc4
běžně	běžně	k6eAd1
používá	používat	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
výrobcích	výrobek	k1gInPc6
pro	pro	k7c4
domácnost	domácnost	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
Clearasil	Clearasil	k1gFnSc1
Daily	Daila	k1gFnSc2
Face	Face	k1gFnPc2
Wash	Wash	k1gInSc1
<g/>
,	,	kIx,
ústní	ústní	k2eAgFnSc1d1
voda	voda	k1gFnSc1
Dentyl	Dentyl	k1gInSc1
<g/>
,	,	kIx,
Dawn	Dawn	k1gInSc1
<g/>
,	,	kIx,
výrobky	výrobek	k1gInPc1
řady	řada	k1gFnSc2
Colgate	Colgat	k1gInSc5
Total	totat	k5eAaImAgMnS
<g/>
,	,	kIx,
Crest	Crest	k1gMnSc1
Cavity	Cavita	k1gFnSc2
Protection	Protection	k1gInSc1
<g/>
,	,	kIx,
Softsoap	Softsoap	k1gInSc1
<g/>
,	,	kIx,
Dial	Dial	k1gInSc1
<g/>
,	,	kIx,
deodorant	deodorant	k1gInSc1
Right	Right	k2eAgInSc4d1
Guard	Guard	k1gInSc4
<g/>
,	,	kIx,
Sensodyne	Sensodyn	k1gInSc5
Total	totat	k5eAaImAgInS
Care	car	k1gMnSc5
<g/>
,	,	kIx,
Old	Olda	k1gFnPc2
Spice	Spiec	k1gInSc2
nebo	nebo	k8xC
Mentadent	Mentadent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
musejí	muset	k5eAaImIp3nP
výrobci	výrobce	k1gMnPc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
uvádět	uvádět	k5eAaImF
na	na	k7c6
výrobcích	výrobek	k1gInPc6
<g/>
,	,	kIx,
že	že	k8xS
obsahují	obsahovat	k5eAaImIp3nP
triclosan	triclosan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
ADA	Ada	kA
(	(	kIx(
<g/>
American	American	k1gMnSc1
Dental	Dental	k1gMnSc1
Association	Association	k1gInSc1
<g/>
)	)	kIx)
publikovala	publikovat	k5eAaBmAgFnS
reakci	reakce	k1gFnSc4
na	na	k7c4
obavy	obava	k1gFnPc4
vyvolané	vyvolaný	k2eAgFnSc2d1
studií	studie	k1gFnSc7
Virginia	Virginium	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
triclosan	triclosan	k1gInSc4
obsažený	obsažený	k2eAgInSc4d1
v	v	k7c6
pastě	pasta	k1gFnSc6
na	na	k7c4
zuby	zub	k1gInPc4
není	být	k5eNaImIp3nS
významný	významný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
jedné	jeden	k4xCgFnSc6
studii	studie	k1gFnSc6
nedávno	nedávno	k6eAd1
přijaté	přijatý	k2eAgFnSc2d1
k	k	k7c3
publikaci	publikace	k1gFnSc3
v	v	k7c6
časopisu	časopis	k1gInSc6
Environmental	Environmental	k1gMnSc1
Health	Health	k1gMnSc1
Perspectives	Perspectives	k1gMnSc1
a	a	k8xC
zveřejněné	zveřejněný	k2eAgInPc4d1
na	na	k7c6
webu	web	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
Isaac	Isaac	k1gFnSc1
Pessah	Pessah	k1gInSc1
<g/>
,	,	kIx,
PhD	PhD	k1gMnSc1
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
U.C.	U.C.	k1gFnSc2
Davis	Davis	k1gFnSc2
Children	Childrna	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Environmental	Environmental	k1gMnSc1
Health	Health	k1gMnSc1
<g/>
,	,	kIx,
podíval	podívat	k5eAaPmAgMnS,k5eAaImAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
může	moct	k5eAaImIp3nS
triclosan	triclosan	k1gMnSc1
ovlivňovat	ovlivňovat	k5eAaImF
mozek	mozek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
chemikálie	chemikálie	k1gFnSc1
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
speciální	speciální	k2eAgInSc4d1
"	"	kIx"
<g/>
receptorové	receptorové	k?
<g/>
"	"	kIx"
molekuly	molekula	k1gFnPc1
na	na	k7c6
povrchu	povrch	k1gInSc6
buněk	buňka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
hladinu	hladina	k1gFnSc4
vápníku	vápník	k1gInSc2
v	v	k7c6
buňce	buňka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buňky	buňka	k1gFnPc1
s	s	k7c7
příliš	příliš	k6eAd1
vysokou	vysoký	k2eAgFnSc7d1
hladinou	hladina	k1gFnSc7
vápníku	vápník	k1gInSc2
jsou	být	k5eAaImIp3nP
přebuzené	přebuzený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mozku	mozek	k1gInSc6
toto	tento	k3xDgNnSc4
přebuzení	přebuzení	k1gNnSc4
(	(	kIx(
<g/>
nadměrná	nadměrný	k2eAgFnSc1d1
excitace	excitace	k1gFnSc1
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
nerovnováze	nerovnováha	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
dopady	dopad	k1gInPc4
na	na	k7c4
duševní	duševní	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
zmutované	zmutovaný	k2eAgInPc1d1
geny	gen	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
usnadňují	usnadňovat	k5eAaImIp3nP
vazbu	vazba	k1gFnSc4
triclosanu	triclosana	k1gFnSc4
na	na	k7c4
jejich	jejich	k3xOp3gFnPc4
buňky	buňka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
citlivější	citlivý	k2eAgFnPc1d2
na	na	k7c4
tyto	tento	k3xDgInPc4
účinky	účinek	k1gInPc4
triclosanu	triclosan	k1gInSc2
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
studie	studie	k1gFnSc2
prováděné	prováděný	k2eAgInPc1d1
v	v	k7c6
USA	USA	kA
v	v	k7c6
letech	léto	k1gNnPc6
2003-2006	2003-2006	k4
a	a	k8xC
publikované	publikovaný	k2eAgInPc4d1
v	v	k7c6
listopadu	listopad	k1gInSc6
2010	#num#	k4
nasvědčují	nasvědčovat	k5eAaImIp3nP
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
triclosan	triclosan	k1gInSc1
(	(	kIx(
<g/>
a	a	k8xC
podobně	podobně	k6eAd1
také	také	k9
bisfenol	bisfenol	k1gInSc1
A	a	k9
<g/>
)	)	kIx)
negativní	negativní	k2eAgInPc1d1
účinky	účinek	k1gInPc1
na	na	k7c4
fungování	fungování	k1gNnSc4
imunitního	imunitní	k2eAgInSc2d1
systému	systém	k1gInSc2
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
závěrů	závěr	k1gInPc2
této	tento	k3xDgFnSc2
studie	studie	k1gFnSc2
bude	být	k5eAaImBp3nS
žádoucí	žádoucí	k2eAgFnSc1d1
prozkoumat	prozkoumat	k5eAaPmF
mechanismy	mechanismus	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
těchto	tento	k3xDgInPc6
účincích	účinek	k1gInPc6
podílejí	podílet	k5eAaImIp3nP
a	a	k8xC
také	také	k9
jaké	jaký	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
expozice	expozice	k1gFnPc4
(	(	kIx(
<g/>
z	z	k7c2
hlediska	hledisko	k1gNnSc2
množství	množství	k1gNnSc2
a	a	k8xC
času	čas	k1gInSc2
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
významný	významný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
imunitní	imunitní	k2eAgInSc4d1
systém	systém	k1gInSc4
a	a	k8xC
na	na	k7c4
náchylnost	náchylnost	k1gFnSc4
k	k	k7c3
nemocem	nemoc	k1gFnPc3
(	(	kIx(
<g/>
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
vážnost	vážnost	k1gFnSc4
<g/>
)	)	kIx)
v	v	k7c6
pozdějších	pozdní	k2eAgNnPc6d2
obdobích	období	k1gNnPc6
lidského	lidský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
přinesla	přinést	k5eAaPmAgFnS
The	The	k1gFnSc1
Lip	lípa	k1gFnPc2
TV	TV	kA
zprávu	zpráva	k1gFnSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
triclosan	triclosan	k1gInSc1
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zubní	zubní	k2eAgFnSc7d1
pastou	pasta	k1gFnSc7
Colgate	Colgat	k1gInSc5
Total	totat	k5eAaImAgMnS
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
podle	podle	k7c2
nálezů	nález	k1gInPc2
americké	americký	k2eAgFnSc2d1
FDA	FDA	kA
spojován	spojovat	k5eAaImNgInS
s	s	k7c7
rakovinou	rakovina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
hoax	hoax	k1gInSc1
se	se	k3xPyFc4
však	však	k9
vyskytl	vyskytnout	k5eAaPmAgMnS
již	již	k6eAd1
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alternativy	alternativa	k1gFnPc1
</s>
<s>
Důkladná	důkladný	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
University	universita	k1gFnSc2
of	of	k?
Oregon	Oregon	k1gInSc1
School	School	k1gInSc1
of	of	k?
Public	publicum	k1gNnPc2
Health	Health	k1gInSc1
ukázala	ukázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
obyčejná	obyčejný	k2eAgNnPc1d1
mýdla	mýdlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
stejně	stejně	k6eAd1
účinná	účinný	k2eAgFnSc1d1
při	při	k7c6
odstraňování	odstraňování	k1gNnSc6
bakterií	bakterie	k1gFnPc2
z	z	k7c2
rukou	ruka	k1gFnPc2
a	a	k8xC
prevenci	prevence	k1gFnSc4
onemocnění	onemocnění	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
antibakteriální	antibakteriální	k2eAgNnPc1d1
mýdla	mýdlo	k1gNnPc1
s	s	k7c7
triclosanem	triclosan	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přidávání	přidávání	k1gNnSc1
triclosanu	triclosanout	k5eAaPmIp1nS
do	do	k7c2
mýdla	mýdlo	k1gNnSc2
na	na	k7c6
ruce	ruka	k1gFnSc6
bylo	být	k5eAaImAgNnS
dříve	dříve	k6eAd2
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
výhodu	výhoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozrušení	rozrušení	k1gNnSc1
vosků	vosk	k1gInPc2
a	a	k8xC
olejů	olej	k1gInPc2
čistým	čistý	k2eAgInSc7d1
mýdlem	mýdlo	k1gNnSc7
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
trvá	trvat	k5eAaImIp3nS
<g/>
,	,	kIx,
proto	proto	k8xC
velmi	velmi	k6eAd1
rychlá	rychlý	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
a	a	k8xC
okamžité	okamžitý	k2eAgNnSc1d1
opláchnutí	opláchnutí	k1gNnSc1
mýdla	mýdlo	k1gNnSc2
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
dostatečné	dostatečný	k2eAgNnSc1d1
k	k	k7c3
odstranění	odstranění	k1gNnSc3
bakterií	bakterie	k1gFnPc2
chráněných	chráněný	k2eAgFnPc2d1
tlustou	tlustý	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
vosku	vosk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triclosan	Triclosan	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
užitečný	užitečný	k2eAgInSc1d1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
zůstává	zůstávat	k5eAaImIp3nS
na	na	k7c6
rukou	ruka	k1gFnPc6
po	po	k7c6
opláchnutí	opláchnutí	k1gNnSc6
jako	jako	k8xC,k8xS
zbytkový	zbytkový	k2eAgInSc4d1
film	film	k1gInSc4
a	a	k8xC
měl	mít	k5eAaImAgMnS
pokračovat	pokračovat	k5eAaImF
v	v	k7c4
ničení	ničení	k1gNnSc4
bakterií	bakterie	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
zkouškami	zkouška	k1gFnPc7
bylo	být	k5eAaImAgNnS
prokázáno	prokázán	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
ruce	ruka	k1gFnPc4
v	v	k7c6
roztoku	roztok	k1gInSc6
mnoho	mnoho	k4c4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
významněji	významně	k6eAd2
ovlivnilo	ovlivnit	k5eAaPmAgNnS
množství	množství	k1gNnSc1
bakterií	bakterie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roztok	roztok	k1gInSc1
přibližně	přibližně	k6eAd1
70	#num#	k4
<g/>
%	%	kIx~
ethanolu	ethanol	k1gInSc2
je	být	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
účinný	účinný	k2eAgInSc1d1
v	v	k7c4
ničení	ničení	k1gNnSc4
bakterií	bakterie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
roztoky	roztok	k1gInPc1
jsou	být	k5eAaImIp3nP
nyní	nyní	k6eAd1
dostupné	dostupný	k2eAgFnPc1d1
v	v	k7c6
mnoha	mnoho	k4c6
čističích	čistič	k1gInPc6
na	na	k7c6
ruce	ruka	k1gFnSc6
a	a	k8xC
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
prodávají	prodávat	k5eAaImIp3nP
jako	jako	k9
"	"	kIx"
<g/>
sanitizér	sanitizér	k1gInSc1
rukou	ruka	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Anorganická	anorganický	k2eAgNnPc1d1
antibiotika	antibiotikum	k1gNnPc1
a	a	k8xC
biocidy	biocida	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
stříbrné	stříbrný	k2eAgInPc4d1
nebo	nebo	k8xC
měďnaté	měďnatý	k2eAgInPc4d1
ionty	ion	k1gInPc4
nebo	nebo	k8xC
nanočástice	nanočástika	k1gFnSc3
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
účinnými	účinný	k2eAgFnPc7d1
alternativami	alternativa	k1gFnPc7
triclosanu	triclosanout	k5eAaPmIp1nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Chlor	chlor	k1gInSc1
</s>
<s>
Chloroform	chloroform	k1gInSc1
</s>
<s>
Dioxin	dioxin	k1gInSc1
</s>
<s>
Furan	furan	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Triclosan	Triclosana	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://21stoleti.cz/2015/09/16/antibakterialni-mydlo-bakterie-nezabiji/	http://21stoleti.cz/2015/09/16/antibakterialni-mydlo-bakterie-nezabiji/	k4
-	-	kIx~
Antibakteriální	antibakteriální	k2eAgNnSc1d1
mýdlo	mýdlo	k1gNnSc1
bakterie	bakterie	k1gFnSc2
nezabíjí	zabíjet	k5eNaImIp3nS
<g/>
↑	↑	k?
http://www.osel.cz/10391-triclosan-do-tretice-a-znovu-palec-dolu_1.html	http://www.osel.cz/10391-triclosan-do-tretice-a-znovu-palec-dolu_1.html	k1gInSc1
-	-	kIx~
Triclosan	Triclosan	k1gInSc1
do	do	k7c2
třetice	třetice	k1gFnSc2
=	=	kIx~
Znovu	znovu	k6eAd1
palec	palec	k1gInSc1
dolů	dol	k1gInPc2
<g/>
↑	↑	k?
Coia	Coia	k1gFnSc1
JE	být	k5eAaImIp3nS
<g/>
,	,	kIx,
Duckworth	Duckworth	k1gInSc1
GJ	GJ	kA
<g/>
,	,	kIx,
Edwards	Edwards	k1gInSc1
DI	DI	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guidelines	Guidelines	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
control	control	k1gInSc1
and	and	k?
prevention	prevention	k1gInSc1
of	of	k?
meticillin-resistant	meticillin-resistant	k1gMnSc1
Staphylococcus	Staphylococcus	k1gMnSc1
aureus	aureus	k1gMnSc1
(	(	kIx(
<g/>
MRSA	MRSA	kA
<g/>
)	)	kIx)
in	in	k?
healthcare	healthcar	k1gMnSc5
facilities	facilitiesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Hosp	Hosp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infect	Infect	k1gInSc1
<g/>
..	..	k?
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
63	#num#	k4
Suppl	Suppl	k1gFnPc2
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
S	s	k7c7
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
jhin	jhin	k1gInSc1
<g/>
.2006	.2006	k4
<g/>
.01	.01	k4
<g/>
.001	.001	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16581155	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Brady	brada	k1gFnSc2
LM	LM	kA
<g/>
,	,	kIx,
Thomson	Thomson	k1gNnSc1
M	M	kA
<g/>
,	,	kIx,
Palmer	Palmer	k1gMnSc1
MA	MA	kA
<g/>
,	,	kIx,
Harkness	Harkness	k1gInSc1
JL	JL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Successful	Successful	k1gInSc1
control	control	k1gInSc4
of	of	k?
endemic	endemice	k1gFnPc2
MRSA	MRSA	kA
in	in	k?
a	a	k8xC
cardiothoracic	cardiothoracic	k1gMnSc1
surgical	surgicat	k5eAaPmAgMnS
unit	unit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Med	med	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Aust	Austa	k1gFnPc2
<g/>
..	..	k?
1990	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
152	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
240	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
2255283	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zafar	Zafar	k1gInSc1
AB	AB	kA
<g/>
,	,	kIx,
Butler	Butler	k1gInSc1
RC	RC	kA
<g/>
,	,	kIx,
Reese	Reese	k1gFnSc1
DJ	DJ	kA
<g/>
,	,	kIx,
Gaydos	Gaydos	k1gMnSc1
LA	la	k1gNnSc2
<g/>
,	,	kIx,
Mennonna	Mennonno	k1gNnSc2
PA	Pa	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Use	usus	k1gInSc5
of	of	k?
0.3	0.3	k4
<g/>
%	%	kIx~
triclosan	triclosan	k1gInSc1
(	(	kIx(
<g/>
Bacti-Stat	Bacti-Stat	k1gFnPc1
<g/>
)	)	kIx)
to	ten	k3xDgNnSc1
eradicate	eradicat	k1gInSc5
an	an	k?
outbreak	outbreak	k1gMnSc1
of	of	k?
methicillin-resistant	methicillin-resistant	k1gMnSc1
Staphylococcus	Staphylococcus	k1gMnSc1
aureus	aureus	k1gMnSc1
in	in	k?
a	a	k8xC
neonatal	neonatal	k1gMnSc1
nursery	nursera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gInSc1
journal	journat	k5eAaPmAgInS,k5eAaImAgInS
of	of	k?
infection	infection	k1gInSc1
control	control	k1gInSc1
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
23	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
200	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
196	#num#	k4
<g/>
-	-	kIx~
<g/>
6553	#num#	k4
<g/>
(	(	kIx(
<g/>
95	#num#	k4
<g/>
)	)	kIx)
<g/>
90042	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
PMID	PMID	kA
7677266	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Singer	Singra	k1gFnPc2
H	H	kA
<g/>
,	,	kIx,
Muller	Muller	k1gInSc1
S	s	k7c7
<g/>
,	,	kIx,
Tixier	Tixier	k1gInSc4
C	C	kA
<g/>
,	,	kIx,
Pillonel	Pillonel	k1gMnSc1
L.	L.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Triclosan	Triclosan	k1gInSc1
<g/>
:	:	kIx,
occurrence	occurrence	k1gFnSc1
and	and	k?
fate	fatat	k5eAaPmIp3nS
of	of	k?
a	a	k8xC
widely	widela	k1gFnSc2
used	used	k6eAd1
biocide	biocid	k1gInSc5
in	in	k?
the	the	k?
aquatic	aquatice	k1gFnPc2
environment	environment	k1gMnSc1
<g/>
:	:	kIx,
field	field	k1gMnSc1
measurements	measurements	k6eAd1
in	in	k?
wastewater	wastewater	k1gInSc1
treatment	treatment	k1gMnSc1
plants	plants	k1gInSc1
<g/>
,	,	kIx,
surface	surface	k1gFnSc1
waters	waters	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
lake	lake	k1gInSc1
sediments	sediments	k1gInSc1
<g/>
..	..	k?
Environ	Environ	k1gInSc1
Sci	Sci	k1gFnSc2
Technol	Technola	k1gFnPc2
<g/>
..	..	k?
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
36	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
23	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4998	#num#	k4
<g/>
–	–	k?
<g/>
5004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12523412	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Heidler	Heidler	k1gInSc1
J	J	kA
<g/>
,	,	kIx,
Halden	Haldna	k1gFnPc2
RU	RU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mass	Mass	k1gInSc1
balance	balanc	k1gFnSc2
assessment	assessment	k1gMnSc1
of	of	k?
triclosan	triclosan	k1gMnSc1
removal	removat	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
during	during	k1gInSc4
conventional	conventionat	k5eAaPmAgInS,k5eAaImAgInS
sewage	sewage	k1gInSc1
treatment	treatment	k1gInSc4
<g/>
..	..	k?
Chemosphere	Chemospher	k1gMnSc5
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
66	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
362	#num#	k4
<g/>
–	–	k?
<g/>
369	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
chemosphere	chemosphrat	k5eAaPmIp3nS
<g/>
.2006	.2006	k4
<g/>
.04	.04	k4
<g/>
.066	.066	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16766013	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Latch	Latch	k1gInSc1
DE	DE	k?
<g/>
,	,	kIx,
Packer	Packer	k1gInSc1
JL	JL	kA
<g/>
,	,	kIx,
Stender	Stender	k1gInSc1
BL	BL	kA
<g/>
,	,	kIx,
VanOverbeke	VanOverbeke	k1gNnSc1
J	J	kA
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
WA	WA	kA
<g/>
,	,	kIx,
McNeill	McNeill	k1gMnSc1
K.	K.	kA
Aqueous	Aqueous	k1gMnSc1
photochemistry	photochemistr	k1gMnPc4
of	of	k?
triclosan	triclosan	k1gInSc1
<g/>
:	:	kIx,
formation	formation	k1gInSc1
of	of	k?
2,4	2,4	k4
<g/>
-dichlorophenol	-dichlorophenola	k1gFnPc2
<g/>
,	,	kIx,
2,8	2,8	k4
<g/>
-dichlorodibenzo-p-dioxin	-dichlorodibenzo-p-dioxina	k1gFnPc2
<g/>
,	,	kIx,
and	and	k?
oligomerization	oligomerization	k1gInSc1
products	products	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Environ	Environ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toxicol	Toxicola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chem.	Chem.	k1gFnSc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
24	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
517	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.189	10.189	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
243	#num#	k4
<g/>
R	R	kA
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15779749	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Russell	Russell	k1gInSc1
AD	ad	k7c4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Whither	Whithra	k1gFnPc2
triclosan	triclosana	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Antimicrob	Antimicroba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemother	Chemothra	k1gFnPc2
<g/>
..	..	k?
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
53	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
693	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
jac	jac	k?
<g/>
/	/	kIx~
<g/>
dkh	dkh	k?
<g/>
171	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15073159	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Heath	Heath	k1gInSc1
RJ	RJ	kA
<g/>
,	,	kIx,
Rubin	Rubin	k2eAgMnSc1d1
JR	JR	kA
<g/>
,	,	kIx,
Holland	Holland	k1gInSc1
DR	dr	kA
<g/>
,	,	kIx,
Zhang	Zhang	k1gInSc1
E	E	kA
<g/>
,	,	kIx,
Snow	Snow	k1gMnSc1
ME	ME	kA
<g/>
,	,	kIx,
Rock	rock	k1gInSc1
CO	co	k3yRnSc1,k3yInSc1,k3yQnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechanism	Mechanism	k1gInSc1
of	of	k?
triclosan	triclosan	k1gInSc1
inhibition	inhibition	k1gInSc1
of	of	k?
bacterial	bacterial	k1gInSc1
fatty	fatta	k1gFnSc2
acid	acida	k1gFnPc2
synthesis	synthesis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Biol.	Biol.	k1gMnSc1
Chem.	Chem.	k1gMnSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
274	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
16	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
11110	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
jbc	jbc	k?
<g/>
.274	.274	k4
<g/>
.16	.16	k4
<g/>
.11110	.11110	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
10196195	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Fan	Fana	k1gFnPc2
F	F	kA
<g/>
,	,	kIx,
Yan	Yan	k1gFnSc1
K	K	kA
<g/>
,	,	kIx,
Wallis	Wallis	k1gFnSc1
NG	NG	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Defining	Defining	k1gInSc1
and	and	k?
combating	combating	k1gInSc1
the	the	k?
mechanisms	mechanisms	k1gInSc1
of	of	k?
triclosan	triclosan	k1gInSc1
resistance	resistanec	k1gMnSc2
in	in	k?
clinical	clinicat	k5eAaPmAgMnS
isolates	isolates	k1gMnSc1
of	of	k?
Staphylococcus	Staphylococcus	k1gMnSc1
aureus	aureus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antimicrob	Antimicroba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agents	Agentsa	k1gFnPc2
Chemother	Chemothra	k1gFnPc2
<g/>
..	..	k?
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
46	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3343	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
AAC	AAC	kA
<g/>
.46	.46	k4
<g/>
.11	.11	k4
<g/>
.3343	.3343	k4
<g/>
-	-	kIx~
<g/>
3347.2002	3347.2002	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12384334	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Slater-Radosti	Slater-Radost	k1gFnSc2
C	C	kA
<g/>
,	,	kIx,
Van	van	k1gInSc1
Aller	Aller	k1gInSc1
G	G	kA
<g/>
,	,	kIx,
Greenwood	Greenwood	k1gInSc1
R	R	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biochemical	Biochemical	k1gFnSc1
and	and	k?
genetic	genetice	k1gFnPc2
characterization	characterization	k1gInSc4
of	of	k?
the	the	k?
action	action	k1gInSc1
of	of	k?
triclosan	triclosan	k1gInSc4
on	on	k3xPp3gMnSc1
Staphylococcus	Staphylococcus	k1gMnSc1
aureus	aureus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Antimicrob	Antimicroba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemother	Chemothra	k1gFnPc2
<g/>
..	..	k?
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
jac	jac	k?
<g/>
/	/	kIx~
<g/>
48.1	48.1	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
11418506	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Chuanchuen	Chuanchuen	k1gInSc1
R	R	kA
<g/>
,	,	kIx,
Karkhoff-Schweizer	Karkhoff-Schweizer	k1gInSc1
RR	RR	kA
<g/>
,	,	kIx,
Schweizer	Schweizer	k1gMnSc1
HP	HP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
High-level	High-level	k1gMnSc1
triclosan	triclosan	k1gMnSc1
resistance	resistance	k1gFnSc2
in	in	k?
Pseudomonas	Pseudomonas	k1gInSc1
aeruginosa	aeruginosa	k1gFnSc1
is	is	k?
solely	solela	k1gFnSc2
a	a	k8xC
result	result	k2eAgInSc1d1
of	of	k?
efflux	efflux	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gInSc1
journal	journat	k5eAaImAgInS,k5eAaPmAgInS
of	of	k?
infection	infection	k1gInSc1
control	control	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
31	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
124	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.106	10.106	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
mic	mic	k?
<g/>
.2003	.2003	k4
<g/>
.11	.11	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12665747	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.dioxinfacts.org/dioxin_health/dioxin_rumors/triclosan.html	http://www.dioxinfacts.org/dioxin_health/dioxin_rumors/triclosan.html	k1gInSc1
<g/>
↑	↑	k?
McMurry	McMurra	k1gFnSc2
LM	LM	kA
<g/>
,	,	kIx,
Oethinger	Oethinger	k1gInSc1
M	M	kA
<g/>
,	,	kIx,
Levy	Levy	k?
SB	sb	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triclosan	Triclosan	k1gInSc1
targets	targets	k6eAd1
lipid	lipid	k1gInSc4
synthesis	synthesis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
394	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6693	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
531	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
28970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
9707111	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://medicalxpress.com/news/2017-07-links-antibiotic-resistance-common-household.html	https://medicalxpress.com/news/2017-07-links-antibiotic-resistance-common-household.html	k1gMnSc1
-	-	kIx~
New	New	k1gMnSc1
study	stud	k1gInPc4
links	links	k6eAd1
antibiotic	antibiotice	k1gFnPc2
resistance	resistanka	k1gFnSc3
to	ten	k3xDgNnSc4
common	common	k1gNnSc4
household	household	k1gMnSc1
disinfectant	disinfectant	k1gMnSc1
triclosan	triclosan	k1gMnSc1
<g/>
↑	↑	k?
http://www.osel.cz/10391-triclosan-do-tretice-a-znovu-palec-dolu_1.html	http://www.osel.cz/10391-triclosan-do-tretice-a-znovu-palec-dolu_1.html	k1gMnSc1
-	-	kIx~
Triclosan	Triclosan	k1gMnSc1
do	do	k7c2
třetice	třetice	k1gFnSc2
a	a	k8xC
znovu	znovu	k6eAd1
palec	palec	k1gInSc1
dolů	dol	k1gInPc2
<g/>
↑	↑	k?
McBain	McBaina	k1gFnPc2
AJ	aj	kA
<g/>
,	,	kIx,
Bartolo	Bartola	k1gFnSc5
RG	RG	kA
<g/>
,	,	kIx,
Catrenich	Catrenich	k1gInSc1
CE	CE	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exposure	Exposur	k1gMnSc5
of	of	k?
sink	sink	k1gInSc4
drain	draina	k1gFnPc2
microcosms	microcosms	k6eAd1
to	ten	k3xDgNnSc4
triclosan	triclosan	k1gMnSc1
<g/>
:	:	kIx,
population	population	k1gInSc1
dynamics	dynamics	k1gInSc1
and	and	k?
antimicrobial	antimicrobial	k1gInSc4
susceptibility	susceptibilita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Appl	Appla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Environ	Environ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microbiol	Microbiol	k1gInSc1
<g/>
..	..	k?
2003	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
69	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5433	#num#	k4
<g/>
–	–	k?
<g/>
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
AEM	AEM	kA
<g/>
.69	.69	k4
<g/>
.9	.9	k4
<g/>
.5433	.5433	k4
<g/>
-	-	kIx~
<g/>
5442.2003	5442.2003	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
12957932	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Aiello	Aiello	k1gNnSc1
AE	AE	kA
<g/>
,	,	kIx,
Marshall	Marshall	k1gInSc1
B	B	kA
<g/>
,	,	kIx,
Levy	Levy	k?
SB	sb	kA
<g/>
,	,	kIx,
Della-Latta	Della-Latta	k1gFnSc1
P	P	kA
<g/>
,	,	kIx,
Larson	Larson	k1gMnSc1
E.	E.	kA
Relationship	Relationship	k1gMnSc1
between	between	k2eAgMnSc1d1
triclosan	triclosan	k1gMnSc1
and	and	k?
susceptibilities	susceptibilities	k1gMnSc1
of	of	k?
bacteria	bacterium	k1gNnSc2
isolated	isolated	k1gMnSc1
from	from	k1gMnSc1
hands	hands	k6eAd1
in	in	k?
the	the	k?
community	communita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antimicrob	Antimicroba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agents	Agentsa	k1gFnPc2
Chemother	Chemothra	k1gFnPc2
<g/>
..	..	k?
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2973	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
AAC	AAC	kA
<g/>
.48	.48	k4
<g/>
.8	.8	k4
<g/>
.2973	.2973	k4
<g/>
-	-	kIx~
<g/>
2979.2004	2979.2004	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15273108	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Yazdankhah	Yazdankhah	k1gInSc1
SP	SP	kA
<g/>
,	,	kIx,
Scheie	Scheie	k1gFnSc1
AA	AA	kA
<g/>
,	,	kIx,
Hø	Hø	k1gInPc7
EA	EA	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Triclosan	Triclosan	k1gMnSc1
and	and	k?
antimicrobial	antimicrobial	k1gMnSc1
resistance	resistanec	k1gInSc2
in	in	k?
bacteria	bacterium	k1gNnSc2
<g/>
:	:	kIx,
an	an	k?
overview	overview	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microb	Microba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drug	Drug	k1gMnSc1
Resist	Resist	k1gMnSc1
<g/>
..	..	k?
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
83	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
mdr	mdr	k?
<g/>
.2006	.2006	k4
<g/>
.12	.12	k4
<g/>
.83	.83	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16922622	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://ec.europa.eu/health/scientific_committees/opinions_layman/triclosan/en/l-2/5-risk-resistance.htm	http://ec.europa.eu/health/scientific_committees/opinions_layman/triclosan/en/l-2/5-risk-resistance.htm	k1gInSc1
-	-	kIx~
Triclosan	Triclosan	k1gInSc1
and	and	k?
Antibiotics	Antibiotics	k1gInSc1
resistance	resistance	k1gFnSc1
<g/>
1	#num#	k4
2	#num#	k4
Rule	rula	k1gFnSc3
KL	kl	kA
<g/>
,	,	kIx,
Ebbett	Ebbett	k2eAgMnSc1d1
VR	vr	k0
<g/>
,	,	kIx,
Vikesland	Vikesland	k1gInSc4
PJ	PJ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formation	Formation	k1gInSc1
of	of	k?
chloroform	chloroform	k1gInSc1
and	and	k?
chlorinated	chlorinated	k1gInSc1
organics	organics	k6eAd1
by	by	kYmCp3nS
free-chlorine-mediated	free-chlorine-mediated	k1gInSc1
oxidation	oxidation	k1gInSc4
of	of	k?
triclosan	triclosan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Environ	Environ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sci	Sci	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technol	Technol	k1gInSc1
<g/>
..	..	k?
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
39	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3176	#num#	k4
<g/>
–	–	k?
<g/>
85	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
15926568	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Nik	nika	k1gFnPc2
Veldhoen	Veldhoen	k1gInSc1
<g/>
,	,	kIx,
Rachel	Rachel	k1gInSc1
C.	C.	kA
Skirrow	Skirrow	k1gMnPc2
<g/>
,	,	kIx,
Heather	Heathra	k1gFnPc2
Osachoff	Osachoff	k1gInSc1
<g/>
,	,	kIx,
Heidi	Heid	k1gMnPc1
Wigmore	Wigmor	k1gInSc5
<g/>
,	,	kIx,
David	David	k1gMnSc1
J.	J.	kA
Clapson	Clapson	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
P.	P.	kA
Gunderson	Gunderson	k1gMnSc1
<g/>
,	,	kIx,
Graham	Graham	k1gMnSc1
Van	vana	k1gFnPc2
Aggelen	Aggelna	k1gFnPc2
and	and	k?
Caren	Carna	k1gFnPc2
C.	C.	kA
Helbing	Helbing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
bactericidal	bactericidat	k5eAaPmAgMnS,k5eAaImAgMnS
agent	agent	k1gMnSc1
triclosan	triclosan	k1gMnSc1
modulates	modulatesa	k1gFnPc2
thyroid	thyroid	k1gInSc1
hormone-associated	hormone-associated	k1gMnSc1
gene	gen	k1gInSc5
expression	expression	k1gInSc4
and	and	k?
disrupts	disrupts	k1gInSc1
postembryonic	postembryonice	k1gFnPc2
anuran	anurana	k1gFnPc2
development	development	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aquatic	Aquatice	k1gFnPc2
Toxicology	Toxicolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
80	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
217	#num#	k4
<g/>
–	–	k?
<g/>
227	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
aquatox	aquatox	k1gInSc1
<g/>
.2006	.2006	k4
<g/>
.08	.08	k4
<g/>
.010	.010	k4
<g/>
.	.	kIx.
↑	↑	k?
Adolfsson-Erici	Adolfsson-Erice	k1gFnSc4
M	M	kA
<g/>
,	,	kIx,
Pettersson	Pettersson	k1gNnSc1
M	M	kA
<g/>
,	,	kIx,
Parkkonen	Parkkonen	k2eAgMnSc1d1
J	J	kA
<g/>
,	,	kIx,
Sturve	Sturev	k1gFnSc2
J.	J.	kA
Triclosan	Triclosan	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
commonly	commonl	k1gInPc1
used	used	k6eAd1
bactericide	bactericid	k1gInSc5
found	found	k1gMnSc1
in	in	k?
human	human	k1gMnSc1
milk	milk	k1gMnSc1
and	and	k?
in	in	k?
the	the	k?
aquatic	aquatice	k1gFnPc2
environment	environment	k1gMnSc1
in	in	k?
Sweden	Swedno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemosphere	Chemospher	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
March	March	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
46	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1485	#num#	k4
<g/>
–	–	k?
<g/>
1489	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
45	#num#	k4
<g/>
-	-	kIx~
<g/>
6535	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
255	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Edvardsson	Edvardsson	k1gInSc1
S	s	k7c7
<g/>
,	,	kIx,
Burman	Burman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
G	G	kA
<g/>
,	,	kIx,
AdolfssonErici	AdolfssonErice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
M	M	kA
<g/>
,	,	kIx,
Bäckman	Bäckman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
N.	N.	kA
Risker	Riskra	k1gFnPc2
och	och	k0
nytta	nytt	k1gInSc2
med	med	k1gInSc4
triklosan	triklosany	k1gInPc2
i	i	k8xC
tandkräm	tandkräma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tandläkartidningen	Tandläkartidningen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
97	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
–	–	k?
<g/>
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Start	start	k1gInSc1
~	~	kIx~
Naturskyddsföreningen	Naturskyddsföreningen	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Safety	Safeta	k1gFnSc2
of	of	k?
Antibacterial	Antibacterial	k1gMnSc1
Soap	Soap	k1gMnSc1
Debated	Debated	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
Impact	Impact	k1gMnSc1
of	of	k?
Bisphenol	Bisphenol	k1gInSc1
A	a	k8xC
and	and	k?
Triclosan	Triclosan	k1gInSc1
on	on	k3xPp3gMnSc1
Immune	Immun	k1gInSc5
Parameters	Parametersa	k1gFnPc2
in	in	k?
the	the	k?
US	US	kA
Population	Population	k1gInSc1
<g/>
,	,	kIx,
NHANES	NHANES	kA
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Colgate	Colgat	k1gMnSc5
Toothpaste	Toothpast	k1gMnSc5
Chemical	Chemical	k1gMnSc5
Linked	Linked	k1gInSc1
to	ten	k3xDgNnSc4
Cancer	Cancer	k1gMnSc1
<g/>
↑	↑	k?
http://www.hoax.cz/hoax/zkontrolujte-si-svuj-sampon/	http://www.hoax.cz/hoax/zkontrolujte-si-svuj-sampon/	k?
-	-	kIx~
Rakovinotvorný	rakovinotvorný	k2eAgMnSc1d1
SLS	SLS	kA
ve	v	k7c6
většině	většina	k1gFnSc6
šamponů	šampon	k1gInPc2
a	a	k8xC
v	v	k7c6
zubních	zubní	k2eAgFnPc6d1
pastách	pasta	k1gFnPc6
Colgate	Colgat	k1gInSc5
<g/>
↑	↑	k?
Plain	Plain	k2eAgInSc4d1
soap	soap	k1gInSc4
as	as	k9
effective	effectiv	k1gInSc5
as	as	k1gNnSc2
antibacterial	antibacterial	k1gMnSc1
but	but	k?
without	without	k1gMnSc1
the	the	k?
risk	risk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
State	status	k1gInSc5
News	News	k1gInSc1
-	-	kIx~
www.statenews.com	www.statenews.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Antibiotic-Resistant	Antibiotic-Resistant	k1gMnSc1
"	"	kIx"
<g/>
Staph	Staph	k1gMnSc1
<g/>
"	"	kIx"
Infection	Infection	k1gInSc1
in	in	k?
Our	Our	k1gMnSc1
Communities	Communities	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.osel.cz/8431-antibakterialni-mydla-nejsou-pro-choroboplodne-zarodky-zadnou-hrozbou.html	http://www.osel.cz/8431-antibakterialni-mydla-nejsou-pro-choroboplodne-zarodky-zadnou-hrozbou.html	k1gInSc1
-	-	kIx~
Antibakteriální	antibakteriální	k2eAgNnPc1d1
mýdla	mýdlo	k1gNnPc1
nejsou	být	k5eNaImIp3nP
pro	pro	k7c4
choroboplodné	choroboplodný	k2eAgInPc4d1
zárodky	zárodek	k1gInPc4
žádnou	žádný	k3yNgFnSc7
hrozbou	hrozba	k1gFnSc7
<g/>
↑	↑	k?
Kim	Kim	k1gFnSc2
<g/>
,	,	kIx,
J.	J.	kA
<g/>
S.	S.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antimicrobial	Antimicrobial	k1gInSc1
effects	effects	k6eAd1
of	of	k?
silver	silver	k1gMnSc1
nanoparticles	nanoparticles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nanomedicine	Nanomedicin	k1gInSc5
<g/>
:	:	kIx,
Nanotechnology	Nanotechnolog	k1gMnPc4
<g/>
,	,	kIx,
Biology	biolog	k1gMnPc4
&	&	k?
Medicine	Medicin	k1gInSc5
<g/>
.	.	kIx.
3	#num#	k4
<g/>
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
95	#num#	k4
<g/>
-	-	kIx~
<g/>
101	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Triclosan	Triclosan	k1gInSc1
Information	Information	k1gInSc1
from	from	k1gInSc1
Ciba	Ciba	k1gFnSc1
–	–	k?
manufacturer	manufacturer	k1gInSc1
information	information	k1gInSc1
pages	pages	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Triclosan	Triclosan	k1gMnSc1
and	and	k?
Its	Its	k1gMnSc1
Impurities	Impurities	k1gMnSc1
</s>
<s>
The	The	k?
Ubiquitous	Ubiquitous	k1gMnSc1
Triclosan	Triclosan	k1gMnSc1
<g/>
:	:	kIx,
An	An	k1gMnSc1
Antibacterial	Antibacterial	k1gMnSc1
Agent	agent	k1gMnSc1
Exposed	Exposed	k1gMnSc1
by	by	kYmCp3nS
Aviva	Avivo	k1gNnPc1
Glaser	Glaser	k1gMnSc1
<g/>
,	,	kIx,
Beyond	Beyond	k1gMnSc1
Pesticides	Pesticides	k1gMnSc1
</s>
<s>
Antibacterials	Antibacterials	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Here	Here	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
the	the	k?
Rub	rub	k1gInSc1
–	–	k?
campaign	campaign	k1gInSc1
site	sitat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Toothpaste	Toothpasit	k5eAaPmRp2nP,k5eAaImRp2nP
cancer	cancer	k1gInSc4
alert	alert	k1gInSc1
–	–	k?
Newspaper	Newspaper	k1gInSc4
story	story	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
When	When	k1gMnSc1
chlorine	chlorin	k1gInSc5
+	+	kIx~
antimicrobials	antimicrobialsit	k5eAaPmRp2nS
=	=	kIx~
unintended	unintended	k1gMnSc1
consequences	consequences	k1gMnSc1
–	–	k?
summary	summara	k1gFnSc2
of	of	k?
research	research	k1gMnSc1
paper	paper	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
CBC	CBC	kA
Marketplace	Marketplace	k1gFnSc1
investigation	investigation	k1gInSc1
into	into	k1gMnSc1
Triclosan	Triclosan	k1gMnSc1
–	–	k?
Investigative	Investigativ	k1gInSc5
News	Newsa	k1gFnPc2
story	story	k1gFnPc4
<g/>
.	.	kIx.
</s>
