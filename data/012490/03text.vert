<p>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
(	(	kIx(	(
<g/>
z	z	k7c2	z
ital	itala	k1gFnPc2	itala
<g/>
.	.	kIx.	.
rilievo	rilievo	k1gNnSc1	rilievo
<g/>
,	,	kIx,	,
vystouplý	vystouplý	k2eAgMnSc1d1	vystouplý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plastické	plastický	k2eAgNnSc1d1	plastické
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
plochy	plocha	k1gFnSc2	plocha
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ornamentální	ornamentální	k2eAgFnSc1d1	ornamentální
nebo	nebo	k8xC	nebo
figurální	figurální	k2eAgFnSc1d1	figurální
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
výzdobě	výzdoba	k1gFnSc3	výzdoba
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
pomníků	pomník	k1gInPc2	pomník
<g/>
,	,	kIx,	,
náhrobků	náhrobek	k1gInPc2	náhrobek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Reliéfy	reliéf	k1gInPc1	reliéf
se	se	k3xPyFc4	se
dělají	dělat	k5eAaImIp3nP	dělat
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
štuku	štuk	k1gInSc2	štuk
<g/>
,	,	kIx,	,
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
nebo	nebo	k8xC	nebo
slonoviny	slonovina	k1gFnSc2	slonovina
i	i	k9	i
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dalece	dalece	k?	dalece
je	být	k5eAaImIp3nS	být
reliéf	reliéf	k1gInSc1	reliéf
prostorový	prostorový	k2eAgInSc1d1	prostorový
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
</s>
</p>
<p>
<s>
Bas-reliéf	Baseliéf	k1gInSc1	Bas-reliéf
(	(	kIx(	(
<g/>
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
plochý	plochý	k2eAgInSc1d1	plochý
reliéf	reliéf	k1gInSc1	reliéf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
motiv	motiv	k1gInSc4	motiv
od	od	k7c2	od
pozadí	pozadí	k1gNnSc2	pozadí
neodděluje	oddělovat	k5eNaImIp3nS	oddělovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
druh	druh	k1gInSc1	druh
reliéfu	reliéf	k1gInSc2	reliéf
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
fasádách	fasáda	k1gFnPc6	fasáda
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
na	na	k7c6	na
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
stěnách	stěna	k1gFnPc6	stěna
a	a	k8xC	a
stropech	strop	k1gInPc6	strop
<g/>
,	,	kIx,	,
na	na	k7c6	na
náhrobních	náhrobní	k2eAgInPc6d1	náhrobní
kamenech	kámen	k1gInPc6	kámen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
plaketách	plaketa	k1gFnPc6	plaketa
<g/>
,	,	kIx,	,
mincích	mince	k1gFnPc6	mince
a	a	k8xC	a
medailích	medaile	k1gFnPc6	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haut-reliéf	Hauteliéf	k1gMnSc1	Haut-reliéf
[	[	kIx(	[
<g/>
ótreliéf	ótreliéf	k1gMnSc1	ótreliéf
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
vysoký	vysoký	k2eAgInSc1d1	vysoký
reliéf	reliéf	k1gInSc1	reliéf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
například	například	k6eAd1	například
ruka	ruka	k1gFnSc1	ruka
nebo	nebo	k8xC	nebo
hlava	hlava	k1gFnSc1	hlava
postavy	postava	k1gFnSc2	postava
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
jako	jako	k9	jako
socha	socha	k1gFnSc1	socha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahloubený	zahloubený	k2eAgInSc4d1	zahloubený
reliéf	reliéf	k1gInSc4	reliéf
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
pro	pro	k7c4	pro
staré	staré	k1gNnSc4	staré
egyptské	egyptský	k2eAgNnSc1d1	egyptské
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
:	:	kIx,	:
plastický	plastický	k2eAgInSc1d1	plastický
motiv	motiv	k1gInSc1	motiv
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ostře	ostro	k6eAd1	ostro
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
zahloubením	zahloubení	k1gNnSc7	zahloubení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
nad	nad	k7c4	nad
rovinu	rovina	k1gFnSc4	rovina
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kontra-reliéf	Kontraeliéf	k1gInSc1	Kontra-reliéf
či	či	k8xC	či
intaglie	intaglie	k1gFnSc1	intaglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
figura	figura	k1gFnSc1	figura
naopak	naopak	k6eAd1	naopak
vyhloubena	vyhloubit	k5eAaPmNgFnS	vyhloubit
v	v	k7c6	v
negativu	negativ	k1gInSc6	negativ
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
broušeného	broušený	k2eAgNnSc2d1	broušené
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
drahokamů	drahokam	k1gInPc2	drahokam
(	(	kIx(	(
<g/>
gemma	gemmum	k1gNnSc2	gemmum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
formám	forma	k1gFnPc3	forma
sochařství	sochařství	k1gNnSc2	sochařství
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vyspělejších	vyspělý	k2eAgFnPc6d2	vyspělejší
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgInPc1d1	starověký
reliéfy	reliéf	k1gInPc1	reliéf
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
kostěné	kostěný	k2eAgFnPc1d1	kostěná
<g/>
,	,	kIx,	,
štukové	štukový	k2eAgFnPc1d1	štuková
nebo	nebo	k8xC	nebo
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
starověkého	starověký	k2eAgNnSc2d1	starověké
reliéfního	reliéfní	k2eAgNnSc2d1	reliéfní
sochařství	sochařství	k1gNnSc2	sochařství
patří	patřit	k5eAaImIp3nS	patřit
egyptské	egyptský	k2eAgInPc4d1	egyptský
reliéfy	reliéf	k1gInPc4	reliéf
a	a	k8xC	a
reliéfní	reliéfní	k2eAgInPc4d1	reliéfní
nápisy	nápis	k1gInPc4	nápis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zdobí	zdobit	k5eAaImIp3nP	zdobit
stěny	stěna	k1gFnPc4	stěna
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
ploché	plochý	k2eAgFnPc1d1	plochá
(	(	kIx(	(
<g/>
bas-reliéf	baseliéf	k1gInSc1	bas-reliéf
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zahloubené	zahloubený	k2eAgFnPc1d1	zahloubená
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc4d1	jiná
povahu	povaha	k1gFnSc4	povaha
mají	mít	k5eAaImIp3nP	mít
realističtější	realistický	k2eAgInPc4d2	realističtější
řecké	řecký	k2eAgInPc4d1	řecký
reliéfy	reliéf	k1gInPc4	reliéf
<g/>
,	,	kIx,	,
ploché	plochý	k2eAgInPc4d1	plochý
i	i	k8xC	i
vysoké	vysoký	k2eAgInPc4d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Feidiovy	Feidiův	k2eAgInPc1d1	Feidiův
římsy	říms	k1gInPc1	říms
a	a	k8xC	a
metopy	metopa	k1gFnPc1	metopa
z	z	k7c2	z
Parthenonu	Parthenon	k1gInSc2	Parthenon
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
řeckého	řecký	k2eAgInSc2d1	řecký
reliéfu	reliéf	k1gInSc2	reliéf
pak	pak	k6eAd1	pak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
na	na	k7c6	na
náhrobcích	náhrobek	k1gInPc6	náhrobek
<g/>
,	,	kIx,	,
vítězných	vítězný	k2eAgInPc6d1	vítězný
sloupech	sloup	k1gInPc6	sloup
a	a	k8xC	a
bránách	brána	k1gFnPc6	brána
i	i	k8xC	i
v	v	k7c6	v
raně	raně	k6eAd1	raně
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
byzantské	byzantský	k2eAgFnSc6d1	byzantská
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
drobné	drobný	k2eAgInPc1d1	drobný
reliéfy	reliéf	k1gInPc1	reliéf
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
ceněné	ceněný	k2eAgFnPc1d1	ceněná
až	až	k9	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
středověkého	středověký	k2eAgNnSc2d1	středověké
umění	umění	k1gNnSc2	umění
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
kamenný	kamenný	k2eAgInSc1d1	kamenný
reliéf	reliéf	k1gInSc1	reliéf
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
gotice	gotika	k1gFnSc6	gotika
<g/>
,	,	kIx,	,
při	při	k7c6	při
výzdobě	výzdoba	k1gFnSc6	výzdoba
francouzských	francouzský	k2eAgFnPc2d1	francouzská
katedrál	katedrála	k1gFnPc2	katedrála
(	(	kIx(	(
<g/>
Chartres	Chartres	k1gInSc1	Chartres
<g/>
,	,	kIx,	,
Amiens	Amiens	k1gInSc1	Amiens
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
a	a	k8xC	a
výjevy	výjev	k1gInPc1	výjev
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
stylizované	stylizovaný	k2eAgInPc1d1	stylizovaný
a	a	k8xC	a
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
se	se	k3xPyFc4	se
tvarům	tvar	k1gInPc3	tvar
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
portálů	portál	k1gInPc2	portál
<g/>
,	,	kIx,	,
náhrobků	náhrobek	k1gInPc2	náhrobek
a	a	k8xC	a
medailonů	medailon	k1gInPc2	medailon
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
rozkvět	rozkvět	k1gInSc1	rozkvět
reliefního	reliefní	k2eAgNnSc2d1	reliefní
umění	umění	k1gNnSc2	umění
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
renesanci	renesance	k1gFnSc6	renesance
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
i	i	k9	i
monumentální	monumentální	k2eAgFnPc1d1	monumentální
stavby	stavba	k1gFnPc1	stavba
zpravidla	zpravidla	k6eAd1	zpravidla
omítají	omítat	k5eAaImIp3nP	omítat
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
kamenných	kamenný	k2eAgInPc2d1	kamenný
a	a	k8xC	a
mramorových	mramorový	k2eAgInPc2d1	mramorový
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
reliéfy	reliéf	k1gInPc1	reliéf
štukové	štukový	k2eAgInPc1d1	štukový
a	a	k8xC	a
keramické	keramický	k2eAgInPc1d1	keramický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barokním	barokní	k2eAgNnSc6d1	barokní
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
reliéf	reliéf	k1gInSc1	reliéf
stává	stávat	k5eAaImIp3nS	stávat
služebnou	služebný	k2eAgFnSc7d1	služebná
částí	část	k1gFnSc7	část
chrámové	chrámový	k2eAgFnSc2d1	chrámová
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
v	v	k7c6	v
palácích	palác	k1gInPc6	palác
a	a	k8xC	a
zámcích	zámek	k1gInPc6	zámek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
na	na	k7c6	na
fasádách	fasáda	k1gFnPc6	fasáda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
stěnách	stěna	k1gFnPc6	stěna
a	a	k8xC	a
stropech	strop	k1gInPc6	strop
<g/>
,	,	kIx,	,
klenutých	klenutý	k2eAgFnPc6d1	klenutá
i	i	k8xC	i
plochých	plochý	k2eAgFnPc6d1	plochá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
klasicismu	klasicismus	k1gInSc2	klasicismus
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
spíše	spíše	k9	spíše
strohý	strohý	k2eAgInSc4d1	strohý
bas-reliéf	baseliéf	k1gInSc4	bas-reliéf
od	od	k7c2	od
volně	volně	k6eAd1	volně
stojící	stojící	k2eAgFnSc2d1	stojící
sochy	socha	k1gFnSc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historizující	historizující	k2eAgFnSc6d1	historizující
městské	městský	k2eAgFnSc6d1	městská
zástavbě	zástavba	k1gFnSc6	zástavba
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
plastická	plastický	k2eAgFnSc1d1	plastická
výzdoba	výzdoba	k1gFnSc1	výzdoba
fasád	fasáda	k1gFnPc2	fasáda
stává	stávat	k5eAaImIp3nS	stávat
masovou	masový	k2eAgFnSc7d1	masová
módou	móda	k1gFnSc7	móda
a	a	k8xC	a
prefabrikované	prefabrikovaný	k2eAgInPc4d1	prefabrikovaný
sádrové	sádrový	k2eAgInPc4d1	sádrový
a	a	k8xC	a
štukové	štukový	k2eAgInPc4d1	štukový
odlitky	odlitek	k1gInPc4	odlitek
se	se	k3xPyFc4	se
na	na	k7c4	na
fasády	fasáda	k1gFnPc4	fasáda
prostě	prostě	k6eAd1	prostě
přilepují	přilepovat	k5eAaImIp3nP	přilepovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
secesním	secesní	k2eAgNnSc6d1	secesní
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
plochý	plochý	k2eAgInSc1d1	plochý
figurální	figurální	k2eAgInSc1d1	figurální
reliéf	reliéf	k1gInSc1	reliéf
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
starověkými	starověký	k2eAgInPc7d1	starověký
vzory	vzor	k1gInPc7	vzor
a	a	k8xC	a
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
architektura	architektura	k1gFnSc1	architektura
hladkých	hladký	k2eAgInPc2d1	hladký
geometrických	geometrický	k2eAgInPc2d1	geometrický
tvarů	tvar	k1gInPc2	tvar
bez	bez	k7c2	bez
plastické	plastický	k2eAgFnSc2d1	plastická
výzdoby	výzdoba	k1gFnSc2	výzdoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
J.	J.	kA	J.
S.	S.	kA	S.
Curl	Curl	k1gInSc1	Curl
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
dictionary	dictionara	k1gFnSc2	dictionara
of	of	k?	of
architectrue	architectru	k1gFnSc2	architectru
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
OUP	OUP	kA	OUP
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Debicki	Debick	k1gFnPc1	Debick
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k6eAd1	Argo
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Herout	Herout	k1gMnSc1	Herout
<g/>
,	,	kIx,	,
Slabikář	slabikář	k1gInSc1	slabikář
návštěvníků	návštěvník	k1gMnPc2	návštěvník
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPPOP	SPPOP	kA	SPPOP
1978	[number]	k4	1978
</s>
</p>
<p>
<s>
F.	F.	kA	F.
Kadeřávek	Kadeřávek	k1gMnSc1	Kadeřávek
<g/>
,	,	kIx,	,
Relief	Relief	k1gMnSc1	Relief
<g/>
.	.	kIx.	.
</s>
<s>
Příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
sochaře	sochař	k1gMnPc4	sochař
a	a	k8xC	a
architekty	architekt	k1gMnPc4	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Štenc	Štenc	k1gFnSc1	Štenc
1925	[number]	k4	1925
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Relief	Relief	k1gInSc1	Relief
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
21	[number]	k4	21
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
531	[number]	k4	531
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
nové	nový	k2eAgNnSc4d1	nové
doby	doba	k1gFnPc4	doba
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Relief	Relief	k1gInSc1	Relief
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
9	[number]	k4	9
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
533	[number]	k4	533
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
</s>
</p>
<p>
<s>
Medaile	medaile	k1gFnSc1	medaile
</s>
</p>
<p>
<s>
Ozdobný	ozdobný	k2eAgInSc1d1	ozdobný
architektonický	architektonický	k2eAgInSc1d1	architektonický
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
<s>
Sochařství	sochařství	k1gNnSc1	sochařství
</s>
</p>
<p>
<s>
Kamej	kamej	k1gFnSc1	kamej
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
reliéf	reliéf	k1gInSc1	reliéf
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Reliéf	reliéf	k1gInSc1	reliéf
(	(	kIx(	(
<g/>
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Katalog	katalog	k1gInSc1	katalog
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
</s>
</p>
