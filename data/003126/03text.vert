<s>
Alice	Alice	k1gFnSc1	Alice
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgMnSc1d1	narozen
jako	jako	k8xS	jako
Vincent	Vincent	k1gMnSc1	Vincent
Damon	Damon	k1gMnSc1	Damon
Furnier	Furnier	k1gMnSc1	Furnier
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
Detroit	Detroit	k1gInSc1	Detroit
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kariéra	kariéra	k1gFnSc1	kariéra
překlenuje	překlenovat	k5eAaImIp3nS	překlenovat
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jevištní	jevištní	k2eAgFnSc7d1	jevištní
show	show	k1gFnSc7	show
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
gilotiny	gilotina	k1gFnPc4	gilotina
<g/>
,	,	kIx,	,
elektrická	elektrický	k2eAgNnPc4d1	elektrické
křesla	křeslo	k1gNnPc4	křeslo
<g/>
,	,	kIx,	,
falešnou	falešný	k2eAgFnSc4d1	falešná
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
hada	had	k1gMnSc2	had
škrtiče	škrtič	k1gMnSc2	škrtič
hroznýše	hroznýš	k1gMnSc2	hroznýš
královského	královský	k2eAgInSc2d1	královský
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
směsicí	směsice	k1gFnSc7	směsice
scén	scéna	k1gFnPc2	scéna
z	z	k7c2	z
horrorových	horrorův	k2eAgInPc2d1	horrorův
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
kabaretu	kabaret	k1gInSc2	kabaret
<g/>
,	,	kIx,	,
heavy	heava	k1gFnSc2	heava
metalové	metalový	k2eAgFnSc2d1	metalová
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
garage	garag	k1gFnSc2	garag
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Alice	Alice	k1gFnPc4	Alice
Cooper	Coopero	k1gNnPc2	Coopero
divadelní	divadelní	k2eAgFnSc4d1	divadelní
podobu	podoba	k1gFnSc4	podoba
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k9	jako
shock	shock	k6eAd1	shock
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Alice	Alice	k1gFnSc1	Alice
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
jméno	jméno	k1gNnSc1	jméno
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
vedoucí	vedoucí	k1gMnSc1	vedoucí
osobností	osobnost	k1gFnPc2	osobnost
byl	být	k5eAaImAgMnS	být
Vincent	Vincent	k1gMnSc1	Vincent
Furnier	Furnier	k1gMnSc1	Furnier
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
si	se	k3xPyFc3	se
Furnier	Furnier	k1gMnSc1	Furnier
změnil	změnit	k5eAaPmAgMnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Alice	Alice	k1gFnPc4	Alice
Cooper	Coopra	k1gFnPc2	Coopra
a	a	k8xC	a
spustil	spustit	k5eAaPmAgInS	spustit
svou	svůj	k3xOyFgFnSc4	svůj
sólovou	sólový	k2eAgFnSc4d1	sólová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
vydali	vydat	k5eAaPmAgMnP	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Spiders	Spidersa	k1gFnPc2	Spidersa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
Alice	Alice	k1gFnSc2	Alice
Cooper	Coopra	k1gFnPc2	Coopra
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
hudebního	hudební	k2eAgInSc2d1	hudební
proudu	proud	k1gInSc2	proud
hitem	hit	k1gInSc7	hit
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Eighteen	Eightena	k1gFnPc2	Eightena
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Love	lov	k1gInSc5	lov
It	It	k1gMnSc1	It
to	ten	k3xDgNnSc1	ten
Death	Death	k1gInSc1	Death
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgInS	následovat
hit	hit	k1gInSc1	hit
"	"	kIx"	"
<g/>
School	School	k1gInSc1	School
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Out	Out	k1gFnSc7	Out
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
a	a	k8xC	a
svého	své	k1gNnSc2	své
uměleckého	umělecký	k2eAgInSc2d1	umělecký
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
albem	album	k1gNnSc7	album
Billion	Billion	k1gInSc1	Billion
Dollar	dollar	k1gInSc1	dollar
Babies	Babies	k1gInSc1	Babies
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cooperova	Cooperův	k2eAgFnSc1d1	Cooperova
sólová	sólový	k2eAgFnSc1d1	sólová
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
koncepčním	koncepční	k2eAgNnSc7d1	koncepční
albem	album	k1gNnSc7	album
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
My	my	k3xPp1nPc1	my
Nightmare	Nightmar	k1gMnSc5	Nightmar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Alice	Alice	k1gFnSc2	Alice
Coopera	Cooper	k1gMnSc2	Cooper
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
sedm	sedm	k4xCc4	sedm
alb	album	k1gNnPc2	album
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Alice	Alice	k1gFnSc2	Alice
Cooper	Coopero	k1gNnPc2	Coopero
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc1d1	další
alba	album	k1gNnPc1	album
jsou	být	k5eAaImIp3nP	být
sólová	sólový	k2eAgNnPc1d1	sólové
<g/>
.	.	kIx.	.
</s>
<s>
Pretties	Pretties	k1gInSc1	Pretties
for	forum	k1gNnPc2	forum
You	You	k1gFnSc2	You
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Easy	Easa	k1gFnSc2	Easa
Action	Action	k1gInSc1	Action
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Love	lov	k1gInSc5	lov
It	It	k1gMnSc1	It
to	ten	k3xDgNnSc4	ten
Death	Death	k1gMnSc1	Death
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Killer	Killer	k1gInSc1	Killer
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
School	School	k1gInSc1	School
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Out	Out	k1gFnSc7	Out
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Billion	Billion	k1gInSc1	Billion
Dollar	dollar	k1gInSc1	dollar
Babies	Babies	k1gInSc1	Babies
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Muscle	Muscle	k1gFnSc1	Muscle
<g />
.	.	kIx.	.
</s>
<s>
of	of	k?	of
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
My	my	k3xPp1nPc1	my
Nightmare	Nightmar	k1gMnSc5	Nightmar
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Alice	Alice	k1gFnSc1	Alice
Cooper	Cooper	k1gMnSc1	Cooper
Goes	Goes	k1gInSc1	Goes
to	ten	k3xDgNnSc4	ten
Hell	Hell	k1gMnSc1	Hell
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Lace	laka	k1gFnSc3	laka
and	and	k?	and
Whiskey	Whiske	k1gMnPc7	Whiske
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
From	From	k1gInSc1	From
the	the	k?	the
Inside	Insid	k1gInSc5	Insid
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Flush	flush	k1gInSc1	flush
the	the	k?	the
Fashion	Fashion	k1gInSc1	Fashion
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Special	Special	k1gMnSc1	Special
Forces	Forces	k1gMnSc1	Forces
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Zipper	Zipper	k1gMnSc1	Zipper
Catches	Catches	k1gMnSc1	Catches
Skin	skin	k1gMnSc1	skin
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
DaDa	dada	k1gNnSc6	dada
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Constrictor	Constrictor	k1gInSc1	Constrictor
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Raise	Raisa	k1gFnSc6	Raisa
Your	Youra	k1gFnPc2	Youra
Fist	Fist	k2eAgInSc1d1	Fist
and	and	k?	and
Yell	Yell	k1gInSc1	Yell
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Trash	Trasha	k1gFnPc2	Trasha
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Hey	Hey	k1gFnSc1	Hey
Stoopid	Stoopida	k1gFnPc2	Stoopida
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Last	Last	k1gInSc1	Last
Temptation	Temptation	k1gInSc1	Temptation
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Brutal	Brutal	k1gMnSc1	Brutal
Planet	planeta	k1gFnPc2	planeta
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Dragontown	Dragontowna	k1gFnPc2	Dragontowna
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Eyes	Eyes	k1gInSc1	Eyes
of	of	k?	of
Alice	Alice	k1gFnSc1	Alice
Cooper	Cooper	k1gMnSc1	Cooper
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Dirty	Dirta	k1gFnSc2	Dirta
Diamonds	Diamonds	k1gInSc1	Diamonds
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Along	Along	k1gInSc1	Along
Came	Came	k1gFnPc2	Came
a	a	k8xC	a
Spider	Spidra	k1gFnPc2	Spidra
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Welcome	Welcom	k1gInSc5	Welcom
2	[number]	k4	2
My	my	k3xPp1nPc1	my
Nightmare	Nightmar	k1gMnSc5	Nightmar
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Viz	vidět	k5eAaImRp2nS	vidět
Detailní	detailní	k2eAgInPc4d1	detailní
seznam	seznam	k1gInSc4	seznam
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
</s>
