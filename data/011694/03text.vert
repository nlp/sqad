<p>
<s>
Komando	komando	k1gNnSc1	komando
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc4d1	americký
akční	akční	k2eAgInSc4d1	akční
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
hrají	hrát	k5eAaImIp3nP	hrát
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
<g/>
,	,	kIx,	,
Rae	Rae	k1gMnSc1	Rae
Dawn	Dawn	k1gMnSc1	Dawn
Chong	Chong	k1gMnSc1	Chong
a	a	k8xC	a
Alyssa	Alyssa	k1gFnSc1	Alyssa
Milano	Milana	k1gFnSc5	Milana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
velitel	velitel	k1gMnSc1	velitel
elitní	elitní	k2eAgFnSc2d1	elitní
bojové	bojový	k2eAgFnSc2d1	bojová
jednotky	jednotka	k1gFnSc2	jednotka
plukovník	plukovník	k1gMnSc1	plukovník
John	John	k1gMnSc1	John
Matrix	Matrix	k1gInSc1	Matrix
(	(	kIx(	(
<g/>
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
svoji	svůj	k3xOyFgFnSc4	svůj
dcerou	dcera	k1gFnSc7	dcera
Jenny	Jenna	k1gMnSc2	Jenna
(	(	kIx(	(
<g/>
Alyssa	Alyss	k1gMnSc2	Alyss
Milano	Milana	k1gFnSc5	Milana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
objeven	objevit	k5eAaPmNgInS	objevit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
horském	horský	k2eAgInSc6d1	horský
domě	dům	k1gInSc6	dům
a	a	k8xC	a
napaden	napadnout	k5eAaPmNgMnS	napadnout
žoldáky	žoldák	k1gMnPc4	žoldák
exdiktátora	exdiktátor	k1gMnSc2	exdiktátor
Ariuse	Ariuse	k1gFnSc2	Ariuse
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
Matrix	Matrix	k1gInSc1	Matrix
svrhl	svrhnout	k5eAaPmAgInS	svrhnout
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
následně	následně	k6eAd1	následně
zahájí	zahájit	k5eAaPmIp3nP	zahájit
útok	útok	k1gInSc4	útok
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
dům	dům	k1gInSc4	dům
a	a	k8xC	a
když	když	k8xS	když
si	se	k3xPyFc3	se
jde	jít	k5eAaImIp3nS	jít
Matrix	Matrix	k1gInSc4	Matrix
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
zbraně	zbraň	k1gFnPc4	zbraň
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
milovanou	milovaný	k2eAgFnSc4d1	milovaná
dceru	dcera	k1gFnSc4	dcera
Jenny	Jenna	k1gFnSc2	Jenna
unesou	unést	k5eAaPmIp3nP	unést
a	a	k8xC	a
žádají	žádat	k5eAaImIp3nP	žádat
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
na	na	k7c6	na
zavraždění	zavraždění	k1gNnSc6	zavraždění
prezidenta	prezident	k1gMnSc2	prezident
Velasqueze	Velasqueze	k1gFnSc2	Velasqueze
z	z	k7c2	z
jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
státu	stát	k1gInSc2	stát
Val	val	k1gInSc4	val
Verde	Verd	k1gInSc5	Verd
a	a	k8xC	a
svržení	svržení	k1gNnSc3	svržení
tamní	tamní	k2eAgFnSc2d1	tamní
demokratické	demokratický	k2eAgFnSc2d1	demokratická
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Matrix	Matrix	k1gInSc1	Matrix
však	však	k9	však
vyskočí	vyskočit	k5eAaPmIp3nS	vyskočit
z	z	k7c2	z
rolujícího	rolující	k2eAgNnSc2d1	rolující
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jej	on	k3xPp3gMnSc4	on
mělo	mít	k5eAaImAgNnS	mít
dovézt	dovézt	k5eAaPmF	dovézt
do	do	k7c2	do
Val	val	k1gInSc4	val
Verde	Verd	k1gInSc5	Verd
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
výskokem	výskok	k1gInSc7	výskok
z	z	k7c2	z
letadla	letadlo	k1gNnSc2	letadlo
však	však	k9	však
ještě	ještě	k6eAd1	ještě
zabije	zabít	k5eAaPmIp3nS	zabít
černocha	černoch	k1gMnSc4	černoch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
únosců	únosce	k1gMnPc2	únosce
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
Sullyho	Sully	k1gMnSc4	Sully
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
něj	on	k3xPp3gInSc2	on
chce	chtít	k5eAaImIp3nS	chtít
zjistit	zjistit	k5eAaPmF	zjistit
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
drží	držet	k5eAaImIp3nP	držet
Jenny	Jenen	k2eAgFnPc1d1	Jenna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
připlete	připlést	k5eAaPmIp3nS	připlést
mladá	mladý	k2eAgFnSc1d1	mladá
letuška	letuška	k1gFnSc1	letuška
(	(	kIx(	(
<g/>
Rae	Rae	k1gMnSc1	Rae
Dawn	Dawn	k1gMnSc1	Dawn
Chong	Chong	k1gMnSc1	Chong
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matrix	Matrix	k1gInSc1	Matrix
hledá	hledat	k5eAaImIp3nS	hledat
dceru	dcera	k1gFnSc4	dcera
společně	společně	k6eAd1	společně
s	s	k7c7	s
letuškou	letuška	k1gFnSc7	letuška
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
najít	najít	k5eAaPmF	najít
do	do	k7c2	do
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
–	–	k?	–
než	než	k8xS	než
přistane	přistat	k5eAaPmIp3nS	přistat
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
měl	mít	k5eAaImAgMnS	mít
letět	letět	k5eAaImF	letět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
byla	být	k5eAaImAgFnS	být
použita	použit	k2eAgFnSc1d1	použita
replika	replika	k1gFnSc1	replika
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
vrátím	vrátit	k5eAaPmIp1nS	vrátit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
be	be	k?	be
back	back	k1gInSc1	back
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
jihoamerický	jihoamerický	k2eAgInSc1d1	jihoamerický
stát	stát	k1gInSc1	stát
Val	val	k1gInSc4	val
Verde	Verd	k1gInSc5	Verd
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgMnSc1d1	fiktivní
a	a	k8xC	a
objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgInPc6d1	další
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Komando	komando	k1gNnSc1	komando
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Komando	komando	k1gNnSc1	komando
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
