<s>
Do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
byla	být	k5eAaImAgFnS	být
RegioJet	RegioJet	k1gInSc4	RegioJet
a.s.	a.s.	k?	a.s.
zapsána	zapsán	k2eAgFnSc1d1	zapsána
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
s	s	k7c7	s
předmětem	předmět	k1gInSc7	předmět
podnikání	podnikání	k1gNnSc2	podnikání
"	"	kIx"	"
<g/>
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
neuvedené	uvedený	k2eNgFnPc1d1	neuvedená
v	v	k7c6	v
přílohách	příloha	k1gFnPc6	příloha
1	[number]	k4	1
až	až	k8xS	až
3	[number]	k4	3
živnostenského	živnostenský	k2eAgInSc2d1	živnostenský
zákona	zákon	k1gInSc2	zákon
<g/>
"	"	kIx"	"
a	a	k8xC	a
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
předmět	předmět	k1gInSc1	předmět
činnosti	činnost	k1gFnSc2	činnost
"	"	kIx"	"
<g/>
provozování	provozování	k1gNnSc3	provozování
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
