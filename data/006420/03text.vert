<s>
Švestka	švestka	k1gFnSc1	švestka
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
s	s	k7c7	s
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Smoljakem	Smoljak	k1gMnSc7	Smoljak
a	a	k8xC	a
jako	jako	k8xS	jako
spoluautor	spoluautor	k1gMnSc1	spoluautor
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
uváděn	uváděn	k2eAgMnSc1d1	uváděn
fiktivní	fiktivní	k2eAgMnSc1d1	fiktivní
český	český	k2eAgMnSc1d1	český
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
v	v	k7c6	v
Žižkovském	žižkovský	k2eAgNnSc6d1	Žižkovské
divadle	divadlo	k1gNnSc6	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgNnPc2d1	ostatní
představení	představení	k1gNnPc2	představení
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
jevištní	jevištní	k2eAgMnSc1d1	jevištní
sklerotikon	sklerotikon	k1gMnSc1	sklerotikon
Švestka	Švestka	k1gMnSc1	Švestka
složen	složen	k2eAgMnSc1d1	složen
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
–	–	k?	–
série	série	k1gFnSc2	série
odborných	odborný	k2eAgInPc2d1	odborný
referátů	referát	k1gInPc2	referát
<g/>
,	,	kIx,	,
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
života	život	k1gInSc2	život
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
v	v	k7c6	v
části	část	k1gFnSc6	část
první	první	k4xOgFnSc6	první
a	a	k8xC	a
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
pak	pak	k6eAd1	pak
ucelenějšího	ucelený	k2eAgNnSc2d2	ucelenější
zpracování	zpracování	k1gNnSc2	zpracování
Cimrmanova	Cimrmanův	k2eAgNnSc2d1	Cimrmanovo
díla	dílo	k1gNnSc2	dílo
–	–	k?	–
činohry	činohra	k1gFnSc2	činohra
Švestka	Švestka	k1gMnSc1	Švestka
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
uvozena	uvodit	k5eAaPmNgFnS	uvodit
následujícími	následující	k2eAgInPc7d1	následující
referáty	referát	k1gInPc7	referát
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
zpravidla	zpravidla	k6eAd1	zpravidla
příslušný	příslušný	k2eAgMnSc1d1	příslušný
přednášející	přednášející	k1gMnSc1	přednášející
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Cimrmanův	Cimrmanův	k2eAgInSc1d1	Cimrmanův
krokový	krokový	k2eAgInSc1d1	krokový
defekt	defekt	k1gInSc1	defekt
(	(	kIx(	(
<g/>
Eliška	Eliška	k1gFnSc1	Eliška
Najbrtová	Najbrtová	k1gFnSc1	Najbrtová
<g/>
,	,	kIx,	,
Jenny	Jenna	k1gFnPc1	Jenna
Suk	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
Anička	Anička	k1gFnSc1	Anička
Šafářová	Šafářová	k1gFnSc1	Šafářová
<g/>
)	)	kIx)	)
Písně	píseň	k1gFnPc1	píseň
s	s	k7c7	s
dentální	dentální	k2eAgFnSc7d1	dentální
tematikou	tematika	k1gFnSc7	tematika
(	(	kIx(	(
<g/>
Sváťa	Sváťa	k1gMnSc1	Sváťa
Pulec	pulec	k1gMnSc1	pulec
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Železniční	železniční	k2eAgMnSc1d1	železniční
stomatolog	stomatolog	k1gMnSc1	stomatolog
(	(	kIx(	(
<g/>
Blažej	Blažej	k1gMnSc1	Blažej
Motyčka	Motyčka	k1gMnSc1	Motyčka
<g/>
)	)	kIx)	)
Japonská	japonský	k2eAgFnSc1d1	japonská
inspirace	inspirace	k1gFnSc1	inspirace
(	(	kIx(	(
<g/>
Eda	Eda	k1gMnSc1	Eda
Wasserfall	Wasserfall	k1gMnSc1	Wasserfall
<g/>
)	)	kIx)	)
Dva	dva	k4xCgInPc1	dva
stařecké	stařecký	k2eAgInPc1d1	stařecký
neduhy	neduh	k1gInPc1	neduh
(	(	kIx(	(
<g/>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
)	)	kIx)	)
Hudební	hudební	k2eAgFnSc1d1	hudební
tečka	tečka	k1gFnSc1	tečka
–	–	k?	–
píseň	píseň	k1gFnSc1	píseň
Šel	jít	k5eAaImAgMnS	jít
nádražák	nádražák	k?	nádražák
na	na	k7c6	na
mlíčí	mlíčí	k1gNnSc6	mlíčí
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
účinkující	účinkující	k1gMnPc1	účinkující
<g/>
)	)	kIx)	)
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
staničce	stanička	k1gFnSc6	stanička
Středoplky	Středoplka	k1gFnSc2	Středoplka
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vechtr	vechtr	k1gMnSc1	vechtr
Přemysl	Přemysl	k1gMnSc1	Přemysl
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
patří	patřit	k5eAaImIp3nS	patřit
švestka	švestka	k1gFnSc1	švestka
v	v	k7c6	v
zastávce	zastávka	k1gFnSc6	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
povolení	povolení	k1gNnSc4	povolení
ji	on	k3xPp3gFnSc4	on
očesat	očesat	k5eAaPmF	očesat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
to	ten	k3xDgNnSc4	ten
stihnout	stihnout	k5eAaPmF	stihnout
do	do	k7c2	do
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
mu	on	k3xPp3gMnSc3	on
mladý	mladý	k2eAgMnSc1d1	mladý
vechtr	vechtr	k1gMnSc1	vechtr
Kamil	Kamil	k1gMnSc1	Kamil
Patka	patka	k1gFnSc1	patka
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
půjčit	půjčit	k5eAaPmF	půjčit
žebřík	žebřík	k1gInSc4	žebřík
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
Přemysl	Přemysl	k1gMnSc1	Přemysl
využít	využít	k5eAaPmF	využít
zkušenosti	zkušenost	k1gFnPc4	zkušenost
svého	svůj	k3xOyFgMnSc2	svůj
kamaráda	kamarád	k1gMnSc2	kamarád
<g/>
,	,	kIx,	,
horolezce	horolezec	k1gMnSc2	horolezec
Sváti	Sváťa	k1gMnSc2	Sváťa
Pulce	pulec	k1gMnSc2	pulec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Přemyslův	Přemyslův	k2eAgMnSc1d1	Přemyslův
bratranec	bratranec	k1gMnSc1	bratranec
Blažej	Blažej	k1gMnSc1	Blažej
Motyčka	Motyčka	k1gMnSc1	Motyčka
je	být	k5eAaImIp3nS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohl	pomoct	k5eAaPmAgInS	pomoct
očesat	očesat	k5eAaPmF	očesat
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
třetinu	třetina	k1gFnSc4	třetina
úrody	úroda	k1gFnSc2	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
lehký	lehký	k2eAgInSc1d1	lehký
úkol	úkol	k1gInSc1	úkol
je	být	k5eAaImIp3nS	být
komplikován	komplikovat	k5eAaBmNgInS	komplikovat
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Přemysl	Přemysl	k1gMnSc1	Přemysl
Hájek	Hájek	k1gMnSc1	Hájek
trpí	trpět	k5eAaImIp3nS	trpět
stařeckým	stařecký	k2eAgInSc7d1	stařecký
neduhem	neduh	k1gInSc7	neduh
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
neschopností	neschopnost	k1gFnSc7	neschopnost
udržet	udržet	k5eAaPmF	udržet
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
stále	stále	k6eAd1	stále
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
chce	chtít	k5eAaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Blažej	Blažej	k1gMnSc1	Blažej
Motyčka	Motyčka	k1gMnSc1	Motyčka
je	být	k5eAaImIp3nS	být
neschopen	schopen	k2eNgMnSc1d1	neschopen
myšlenku	myšlenka	k1gFnSc4	myšlenka
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
ostatní	ostatní	k2eAgMnSc1d1	ostatní
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
vyprávěním	vyprávění	k1gNnSc7	vyprávění
postřehů	postřeh	k1gInPc2	postřeh
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
probráno	probrán	k2eAgNnSc1d1	probráno
a	a	k8xC	a
již	již	k6eAd1	již
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
nemluví	mluvit	k5eNaImIp3nP	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
zdržení	zdržení	k1gNnSc1	zdržení
přivodí	přivodit	k5eAaBmIp3nS	přivodit
postupně	postupně	k6eAd1	postupně
tři	tři	k4xCgFnPc1	tři
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
stanicí	stanice	k1gFnSc7	stanice
projdou	projít	k5eAaPmIp3nP	projít
–	–	k?	–
Emilka	Emilka	k1gFnSc1	Emilka
Najbrtová	Najbrtová	k1gFnSc1	Najbrtová
<g/>
,	,	kIx,	,
Jenny	Jenno	k1gNnPc7	Jenno
Suk	suk	k1gInSc1	suk
a	a	k8xC	a
Andulka	Andulka	k1gFnSc1	Andulka
Šafářová	Šafářová	k1gFnSc1	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
se	se	k3xPyFc4	se
mladý	mladý	k2eAgMnSc1d1	mladý
Kamil	Kamil	k1gMnSc1	Kamil
Patka	patka	k1gFnSc1	patka
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
získat	získat	k5eAaPmF	získat
přízeň	přízeň	k1gFnSc4	přízeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
provdat	provdat	k5eAaPmF	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Náhodně	náhodně	k6eAd1	náhodně
příchozí	příchozí	k1gMnSc1	příchozí
Eda	Eda	k1gMnSc1	Eda
Wasserfall	Wasserfall	k1gMnSc1	Wasserfall
z	z	k7c2	z
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
vytvářející	vytvářející	k2eAgFnSc1d1	vytvářející
turistické	turistický	k2eAgNnSc4d1	turistické
značení	značení	k1gNnSc4	značení
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
na	na	k7c4	na
švestku	švestka	k1gFnSc4	švestka
natřít	natřít	k5eAaPmF	natřít
značku	značka	k1gFnSc4	značka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Sváťa	Sváťa	k1gFnSc1	Sváťa
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
odradit	odradit	k5eAaPmF	odradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
švestku	švestka	k1gFnSc4	švestka
očesat	očesat	k5eAaPmF	očesat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Přemyslovi	Přemyslův	k2eAgMnPc1d1	Přemyslův
jeho	on	k3xPp3gInSc4	on
záměr	záměr	k1gInSc4	záměr
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
hry	hra	k1gFnSc2	hra
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
drezínou	drezína	k1gFnSc7	drezína
železniční	železniční	k2eAgMnSc1d1	železniční
úředník	úředník	k1gMnSc1	úředník
Kryštof	Kryštof	k1gMnSc1	Kryštof
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
úroda	úroda	k1gFnSc1	úroda
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Přemysl	Přemysl	k1gMnSc1	Přemysl
nestihl	stihnout	k5eNaPmAgMnS	stihnout
švestku	švestka	k1gFnSc4	švestka
očesat	očesat	k5eAaPmF	očesat
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Hájek	Hájek	k1gMnSc1	Hájek
–	–	k?	–
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
†	†	k?	†
<g/>
)	)	kIx)	)
Kamil	Kamil	k1gMnSc1	Kamil
Patka	patka	k1gFnSc1	patka
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Reidinger	Reidinger	k1gMnSc1	Reidinger
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Šimon	Šimon	k1gMnSc1	Šimon
Blažej	Blažej	k1gMnSc1	Blažej
Motyčka	Motyčka	k1gMnSc1	Motyčka
–	–	k?	–
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Penc	Penc	k1gFnSc4	Penc
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Weigel	Weigel	k1gMnSc1	Weigel
Sváťa	Sváťa	k1gMnSc1	Sváťa
Pulec	pulec	k1gMnSc1	pulec
–	–	k?	–
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
†	†	k?	†
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hraběta	Hraběto	k1gNnSc2	Hraběto
Eda	Eda	k1gMnSc1	Eda
Wasserfall	Wasserfall	k1gMnSc1	Wasserfall
–	–	k?	–
Genadij	Genadij	k1gMnSc1	Genadij
<g />
.	.	kIx.	.
</s>
<s>
Rumlena	Rumlena	k1gFnSc1	Rumlena
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Vondruška	Vondruška	k1gMnSc1	Vondruška
†	†	k?	†
<g/>
)	)	kIx)	)
Emilka	Emilek	k1gMnSc2	Emilek
Najbrtová	Najbrtový	k2eAgFnSc1d1	Najbrtový
–	–	k?	–
Miloň	Miloň	k1gFnSc1	Miloň
Čepelka	čepelka	k1gFnSc1	čepelka
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Brukner	Brukner	k1gMnSc1	Brukner
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Kotek	Kotek	k1gMnSc1	Kotek
Jenny	Jenna	k1gFnSc2	Jenna
Suk	Suk	k1gMnSc1	Suk
–	–	k?	–
Miloň	Miloň	k1gFnSc1	Miloň
Čepelka	čepelka	k1gFnSc1	čepelka
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Brukner	Brukner	k1gMnSc1	Brukner
Andulka	Andulka	k1gFnSc1	Andulka
Šafářová	Šafářová	k1gFnSc1	Šafářová
–	–	k?	–
Miloň	Miloň	k1gFnSc1	Miloň
Čepelka	čepelka	k1gFnSc1	čepelka
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Brukner	Brukner	k1gMnSc1	Brukner
Kryštof	Kryštof	k1gMnSc1	Kryštof
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Kotek	Kotek	k1gMnSc1	Kotek
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
Divadlo	divadlo	k1gNnSc4	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
</s>
