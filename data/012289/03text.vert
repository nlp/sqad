<p>
<s>
Jodidy	jodid	k1gInPc1	jodid
jsou	být	k5eAaImIp3nP	být
soli	sůl	k1gFnPc4	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
jodovodíkové	jodovodíkový	k2eAgFnSc2d1	jodovodíková
(	(	kIx(	(
<g/>
HI	hi	k0	hi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
jodid	jodid	k1gInSc1	jodid
draselný	draselný	k2eAgInSc1d1	draselný
(	(	kIx(	(
<g/>
KI	KI	kA	KI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
indikacích	indikace	k1gFnPc6	indikace
<g/>
,	,	kIx,	,
k	k	k7c3	k
"	"	kIx"	"
<g/>
jodidování	jodidování	k1gNnSc3	jodidování
<g/>
"	"	kIx"	"
soli	sůl	k1gFnPc4	sůl
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Jod	jod	k1gInSc4	jod
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
podobu	podoba	k1gFnSc4	podoba
iontu	ion	k1gInSc2	ion
I-	I-	k1gFnSc2	I-
<g/>
.	.	kIx.	.
</s>
<s>
Jodidy	jodid	k1gInPc1	jodid
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
tmavnout	tmavnout	k5eAaImF	tmavnout
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
měnit	měnit	k5eAaImF	měnit
odstín	odstín	k1gInSc4	odstín
více	hodně	k6eAd2	hodně
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
až	až	k9	až
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tendencí	tendence	k1gFnPc2	tendence
tvořit	tvořit	k5eAaImF	tvořit
trijodidy	trijodid	k1gInPc4	trijodid
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
3	[number]	k4	3
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
reakcí	reakce	k1gFnSc7	reakce
jodidu	jodid	k1gInSc2	jodid
(	(	kIx(	(
<g/>
I-	I-	k1gFnSc1	I-
<g/>
)	)	kIx)	)
s	s	k7c7	s
jodem	jod	k1gInSc7	jod
(	(	kIx(	(
<g/>
I	i	k9	i
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
jodidech	jodid	k1gInPc6	jodid
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxidací	oxidace	k1gFnSc7	oxidace
jodidů	jodid	k1gInPc2	jodid
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jodidů	jodid	k1gInPc2	jodid
této	tento	k3xDgFnSc6	tento
oxidaci	oxidace	k1gFnSc6	oxidace
snadno	snadno	k6eAd1	snadno
podléhá	podléhat	k5eAaImIp3nS	podléhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc1d1	významný
jodidy	jodid	k1gInPc1	jodid
==	==	k?	==
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
není	být	k5eNaImIp3nS	být
úplnýS	úplnýS	k?	úplnýS
prvky	prvek	k1gInPc1	prvek
v	v	k7c6	v
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
I.	I.	kA	I.
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
"	"	kIx"	"
<g/>
I.	I.	kA	I.
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
I.	I.	kA	I.
===	===	k?	===
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
(	(	kIx(	(
<g/>
AgI	AgI	k1gFnSc1	AgI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
NaI	NaI	k1gFnSc1	NaI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
draselný	draselný	k2eAgInSc1d1	draselný
(	(	kIx(	(
<g/>
KI	KI	kA	KI
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jodovodík	jodovodík	k1gInSc1	jodovodík
(	(	kIx(	(
<g/>
HI	hi	k0	hi
<g/>
)	)	kIx)	)
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Jodid	jodid	k1gInSc1	jodid
amonný	amonný	k2eAgInSc1d1	amonný
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
I	I	kA	I
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jodkyan	Jodkyan	k1gMnSc1	Jodkyan
(	(	kIx(	(
<g/>
ICN	ICN	kA	ICN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
II	II	kA	II
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
barnatý	barnatý	k2eAgInSc1d1	barnatý
(	(	kIx(	(
<g/>
BaI	BaI	k1gFnSc1	BaI
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
(	(	kIx(	(
<g/>
CaI	CaI	k1gFnSc1	CaI
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
kademnatý	kademnatý	k2eAgInSc1d1	kademnatý
(	(	kIx(	(
<g/>
CdI	CdI	k1gFnSc1	CdI
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
rtuťnatý	rtuťnatý	k2eAgInSc1d1	rtuťnatý
(	(	kIx(	(
<g/>
HgI	HgI	k1gFnSc1	HgI
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
(	(	kIx(	(
<g/>
PbI	PbI	k1gFnSc1	PbI
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
(	(	kIx(	(
<g/>
ZnI	znít	k5eAaImRp2nS	znít
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
III	III	kA	III
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
bismutitý	bismutitý	k2eAgInSc1d1	bismutitý
(	(	kIx(	(
<g/>
BiI	BiI	k?	BiI
<g/>
3	[number]	k4	3
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
Dragendorffova	Dragendorffův	k2eAgNnSc2d1	Dragendorffův
činidla	činidlo	k1gNnSc2	činidlo
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
fosforitý	fosforitý	k2eAgInSc1d1	fosforitý
(	(	kIx(	(
<g/>
PI	pi	k0	pi
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
IV	IV	kA	IV
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
neboli	neboli	k8xC	neboli
Tetrajodmethan	Tetrajodmethan	k1gInSc1	Tetrajodmethan
(	(	kIx(	(
<g/>
CI	ci	k0	ci
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
činidlo	činidlo	k1gNnSc1	činidlo
pro	pro	k7c4	pro
jodidace	jodidace	k1gFnPc4	jodidace
v	v	k7c6	v
organických	organický	k2eAgFnPc6d1	organická
syntézách	syntéza	k1gFnPc6	syntéza
</s>
</p>
<p>
<s>
===	===	k?	===
V.	V.	kA	V.
===	===	k?	===
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
fosforečný	fosforečný	k2eAgInSc1d1	fosforečný
(	(	kIx(	(
<g/>
PI	pi	k0	pi
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
Jodidy	jodid	k1gInPc1	jodid
nejsou	být	k5eNaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
ve	v	k7c6	v
vyšším	vysoký	k2eAgNnSc6d2	vyšší
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
než	než	k8xS	než
V.	V.	kA	V.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Fluoridy	fluorid	k1gInPc1	fluorid
</s>
</p>
<p>
<s>
Chloridy	chlorid	k1gInPc1	chlorid
</s>
</p>
<p>
<s>
Bromidy	bromid	k1gInPc1	bromid
</s>
</p>
