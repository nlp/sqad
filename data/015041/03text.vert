<s>
Grigorij	Grigorít	k5eAaPmRp2nS
Perelman	Perelman	k1gMnSc1
</s>
<s>
Grigorij	Grigorít	k5eAaPmRp2nS
Jakovlevič	Jakovlevič	k1gMnSc1
Perelman	Perelman	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
(	(	kIx(
<g/>
54	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Leningrad	Leningrad	k1gInSc1
<g/>
,	,	kIx,
SSSR	SSSR	kA
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
Státní	státní	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
ruská	ruský	k2eAgFnSc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
St.	st.	kA
Petersburg	Petersburg	k1gInSc1
Department	department	k1gInSc1
of	of	k?
Steklov	Steklov	k1gInSc1
Institute	institut	k1gInSc5
of	of	k?
Mathematics	Mathematics	k1gInSc1
of	of	k?
Russian	Russian	k1gInSc1
Academy	Academa	k1gFnSc2
of	of	k?
Sciences	Sciences	k1gInSc1
(	(	kIx(
<g/>
do	do	k7c2
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
Petrohradské	petrohradský	k2eAgNnSc1d1
fyzikálně-matematické	fyzikálně-matematický	k2eAgNnSc1d1
lyceum	lyceum	k1gNnSc1
№	№	k?
239	#num#	k4
<g/>
Fakulta	fakulta	k1gFnSc1
matematiky	matematika	k1gFnSc2
a	a	k8xC
mechaniky	mechanika	k1gFnSc2
Petrohradské	petrohradský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
univerzityPetrohradská	univerzityPetrohradský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Pracoviště	pracoviště	k1gNnSc2
</s>
<s>
Petrohradská	petrohradský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Obor	obor	k1gInSc1
</s>
<s>
matematika	matematik	k1gMnSc2
Známý	známý	k2eAgMnSc1d1
díky	díky	k7c3
</s>
<s>
Dokázání	dokázání	k1gNnSc1
Poincarého	Poincarý	k2eAgInSc2d1
domněnky	domněnka	k1gFnPc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Fieldsova	Fieldsův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prémie	prémie	k1gFnSc1
1	#num#	k4
milion	milion	k4xCgInSc4
USD	USD	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
obě	dva	k4xCgFnPc1
odmítl	odmítnout	k5eAaPmAgMnS
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Grigorij	Grigorít	k5eAaPmRp2nS
Jakovlevič	Jakovlevič	k1gMnSc1
Perelman	Perelman	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Г	Г	k?
Я	Я	k?
П	П	k?
<g/>
;	;	kIx,
narozen	narozen	k2eAgInSc1d1
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
v	v	k7c6
Leningradu	Leningrad	k1gInSc3
<g/>
,	,	kIx,
SSSR	SSSR	kA
–	–	k?
nyní	nyní	k6eAd1
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Griša	Griša	k1gMnSc1
Perelman	Perelman	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ruský	ruský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
řešením	řešení	k1gNnSc7
matematických	matematický	k2eAgInPc2d1
problémů	problém	k1gInPc2
nejvyšší	vysoký	k2eAgFnSc2d3
úrovně	úroveň	k1gFnSc2
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
oboru	obor	k1gInSc6
dosáhl	dosáhnout	k5eAaPmAgMnS
velkého	velký	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
vyřešil	vyřešit	k5eAaPmAgInS
jednak	jednak	k8xC
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejobtížnějších	obtížný	k2eAgInPc2d3
a	a	k8xC
jednak	jednak	k8xC
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejslavnějších	slavný	k2eAgInPc2d3
matematických	matematický	k2eAgInPc2d1
problémů	problém	k1gInPc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
tzv.	tzv.	kA
Poincarého	Poincarý	k2eAgNnSc2d1
hypotézu	hypotéza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byl	být	k5eAaImAgInS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ani	ani	k8xC
po	po	k7c6
čtyřech	čtyři	k4xCgNnPc6
letech	léto	k1gNnPc6
nedokázal	dokázat	k5eNaPmAgMnS
nikdo	nikdo	k3yNnSc1
zpochybnit	zpochybnit	k5eAaPmF
<g/>
,	,	kIx,
odměněn	odměněn	k2eAgInSc1d1
prestižní	prestižní	k2eAgFnSc7d1
Fieldsovou	Fieldsův	k2eAgFnSc7d1
medailí	medaile	k1gFnSc7
<g/>
,	,	kIx,
matematickou	matematický	k2eAgFnSc7d1
obdobou	obdoba	k1gFnSc7
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědec	vědec	k1gMnSc1
však	však	k9
cenu	cena	k1gFnSc4
odmítl	odmítnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
odmítl	odmítnout	k5eAaPmAgMnS
rovněž	rovněž	k9
převzetí	převzetí	k1gNnSc4
finanční	finanční	k2eAgFnSc2d1
prémie	prémie	k1gFnSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
1	#num#	k4
000	#num#	k4
000	#num#	k4
USD	USD	kA
za	za	k7c2
vyřešení	vyřešení	k1gNnSc2
jednoho	jeden	k4xCgMnSc2
ze	z	k7c2
sedmi	sedm	k4xCc2
matematických	matematický	k2eAgInPc2d1
problémů	problém	k1gInPc2
tisíciletí	tisíciletí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
důvod	důvod	k1gInSc1
uvedl	uvést	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
nesouhlas	nesouhlas	k1gInSc4
s	s	k7c7
názory	názor	k1gInPc7
organizované	organizovaný	k2eAgFnSc2d1
matematické	matematický	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vede	vést	k5eAaImIp3nS
odloučený	odloučený	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
ignoruje	ignorovat	k5eAaImIp3nS
novináře	novinář	k1gMnPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
jeho	jeho	k3xOp3gInPc7
asketickými	asketický	k2eAgInPc7d1
názory	názor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Vím	vědět	k5eAaImIp1nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
ovládat	ovládat	k5eAaImF
vesmír	vesmír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
mi	já	k3xPp1nSc3
řekněte	říct	k5eAaPmRp2nP
<g/>
,	,	kIx,
proč	proč	k6eAd1
bych	by	kYmCp1nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
hnát	hnát	k1gInSc1
za	za	k7c7
milionem	milion	k4xCgInSc7
<g/>
?!	?!	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Raná	raný	k2eAgNnPc1d1
léta	léto	k1gNnPc1
</s>
<s>
Grigorij	Grigorít	k5eAaPmRp2nS
Perelman	Perelman	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1966	#num#	k4
v	v	k7c6
Leningradu	Leningrad	k1gInSc2
do	do	k7c2
židovské	židovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Jacob	Jacoba	k1gFnPc2
byl	být	k5eAaImAgMnS
elektrotechnik	elektrotechnik	k1gMnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
emigroval	emigrovat	k5eAaBmAgMnS
do	do	k7c2
Izraele	Izrael	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
Lyubov	Lyubov	k1gInSc4
Leybovna	Leybovna	k1gFnSc1
Steingolts	Steingolts	k1gInSc1
zůstala	zůstat	k5eAaPmAgFnS
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
<g/>
,	,	kIx,
pracovala	pracovat	k5eAaImAgFnS
jako	jako	k9
učitelka	učitelka	k1gFnSc1
matematiky	matematika	k1gFnSc2
na	na	k7c6
odborné	odborný	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
má	mít	k5eAaImIp3nS
mladší	mladý	k2eAgFnSc4d2
sestru	sestra	k1gFnSc4
Elenu	Elena	k1gFnSc4
(	(	kIx(
<g/>
nar	nar	kA
<g/>
.	.	kIx.
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
také	také	k9
matematičku	matematička	k1gFnSc4
<g/>
,	,	kIx,
absolventku	absolventka	k1gFnSc4
Petrohradské	petrohradský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
obhájila	obhájit	k5eAaPmAgFnS
doktorskou	doktorský	k2eAgFnSc4d1
práci	práce	k1gFnSc4
na	na	k7c6
Weizmannově	Weizmannův	k2eAgInSc6d1
institutu	institut	k1gInSc6
v	v	k7c6
Rehovotu	Rehovot	k1gInSc6
(	(	kIx(
<g/>
Izrael	Izrael	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
pracuje	pracovat	k5eAaImIp3nS
jako	jako	k9
programátorka	programátorka	k1gFnSc1
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Až	až	k9
do	do	k7c2
devaté	devatý	k2eAgFnSc2d1
třidy	třida	k1gFnSc2
Perelman	Perelman	k1gMnSc1
navštěvoval	navštěvovat	k5eAaImAgMnS
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
na	na	k7c6
okraji	okraj	k1gInSc6
Leningradu	Leningrad	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
pak	pak	k6eAd1
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
239	#num#	k4
<g/>
.	.	kIx.
matematické	matematický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrál	hrát	k5eAaImAgInS
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
,	,	kIx,
navštěvoval	navštěvovat	k5eAaImAgInS
hudební	hudební	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
nedostal	dostat	k5eNaPmAgMnS
jen	jen	k9
kvůli	kvůli	k7c3
tělesné	tělesný	k2eAgFnSc3d1
výchově	výchova	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
neprošel	projít	k5eNaPmAgMnS
standardy	standard	k1gInPc7
GTO	GTO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
střední	střední	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Grigorij	Grigorij	k1gMnSc1
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
Matematickém	matematický	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
pod	pod	k7c7
vedením	vedení	k1gNnSc7
docenta	docent	k1gMnSc2
ruské	ruský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
pedagogické	pedagogický	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
Sergeje	Sergej	k1gMnSc2
Rukšina	Rukšin	k2eAgMnSc2d1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
studenti	student	k1gMnPc1
získali	získat	k5eAaPmAgMnP
řadu	řada	k1gFnSc4
ocenění	ocenění	k1gNnSc2
na	na	k7c6
matematických	matematický	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
člen	člen	k1gMnSc1
týmu	tým	k1gInSc2
sovětských	sovětský	k2eAgMnPc2d1
školáků	školák	k1gMnPc2
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgInS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
matematické	matematický	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
plné	plný	k2eAgNnSc4d1
skóre	skóre	k1gNnSc4
za	za	k7c4
dokonalé	dokonalý	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
všech	všecek	k3xTgFnPc2
úloh	úloha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
a	a	k8xC
univerzita	univerzita	k1gFnSc1
</s>
<s>
Byl	být	k5eAaImAgInS
zapsán	zapsat	k5eAaPmNgInS
na	na	k7c4
Matematicko-mechanickou	matematicko-mechanický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Leningradské	leningradský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
bez	bez	k7c2
zkoušek	zkouška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
celého	celý	k2eAgNnSc2d1
studia	studio	k1gNnSc2
měl	mít	k5eAaImAgInS
pouze	pouze	k6eAd1
„	„	k?
<g/>
výborné	výborná	k1gFnSc2
<g/>
“	“	k?
ze	z	k7c2
všech	všecek	k3xTgInPc2
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
úspěchu	úspěch	k1gInSc3
ve	v	k7c6
škole	škola	k1gFnSc6
získal	získat	k5eAaPmAgMnS
Leninovo	Leninův	k2eAgNnSc4d1
stipendium	stipendium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
univerzity	univerzita	k1gFnSc2
vstoupil	vstoupit	k5eAaPmAgInS
do	do	k7c2
aspirantury	aspirantura	k1gFnSc2
(	(	kIx(
<g/>
školitel	školitel	k1gMnSc1
–	–	k?
A.	A.	kA
D.	D.	kA
Alexandrov	Alexandrov	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
Leningradské	leningradský	k2eAgFnSc6d1
pobočce	pobočka	k1gFnSc6
Matematického	matematický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
V.	V.	kA
A.	A.	kA
Steklové	Steklová	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
,	,	kIx,
obhájil	obhájit	k5eAaPmAgInS
diplomovou	diplomový	k2eAgFnSc4d1
práci	práce	k1gFnSc4
na	na	k7c4
téma	téma	k1gNnSc4
„	„	k?
<g/>
Sedlové	sedlový	k2eAgInPc4d1
povrchy	povrch	k1gInPc4
v	v	k7c6
euklidovských	euklidovský	k2eAgInPc6d1
prostorech	prostor	k1gInPc6
<g/>
“	“	k?
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
ústavu	ústav	k1gInSc6
jako	jako	k8xS,k8xC
vedoucí	vedoucí	k2eAgMnSc1d1
vědecký	vědecký	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
létech	léto	k1gNnPc6
Perelman	Perelman	k1gMnSc1
přišel	přijít	k5eAaPmAgInS
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k8xS,k8xC
vědecký	vědecký	k2eAgMnSc1d1
asistent	asistent	k1gMnSc1
u	u	k7c2
různých	různý	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překvapil	překvapit	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
kolegy	kolega	k1gMnPc4
asketicismem	asketicismus	k1gInSc7
života	život	k1gInSc2
(	(	kIx(
<g/>
nejoblíbenějším	oblíbený	k2eAgNnSc7d3
jídlem	jídlo	k1gNnSc7
Perelmana	Perelman	k1gMnSc2
bylo	být	k5eAaImAgNnS
mléko	mléko	k1gNnSc1
<g/>
,	,	kIx,
chléb	chléb	k1gInSc1
a	a	k8xC
sýr	sýr	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
práci	práce	k1gFnSc6
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
sám	sám	k3xTgMnSc1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
důkazu	důkaz	k1gInSc6
Poincarého	Poincarý	k2eAgInSc2d1
hypotézy	hypotéza	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1
práce	práce	k1gFnSc1
a	a	k8xC
doktorát	doktorát	k1gInSc1
</s>
<s>
V	v	k7c6
létech	léto	k1gNnPc6
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
tři	tři	k4xCgInPc4
články	článek	k1gInPc4
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
Vzorec	vzorec	k1gInSc1
entropie	entropie	k1gFnSc2
pro	pro	k7c4
Ricciho	Ricci	k1gMnSc4
tok	tok	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnPc4
geometrické	geometrický	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
The	The	k1gFnSc1
entropy	entropa	k1gFnSc2
formula	formulum	k1gNnSc2
for	forum	k1gNnPc2
the	the	k?
Ricci	Ricec	k1gInSc6
flow	flow	k?
and	and	k?
its	its	k?
geometric	geometrice	k1gFnPc2
applications	applications	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Ricciho	Riccize	k6eAd1
tok	tok	k1gInSc1
s	s	k7c7
operací	operace	k1gFnSc7
na	na	k7c6
trojrozměrných	trojrozměrný	k2eAgFnPc6d1
varietách	varieta	k1gFnPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Ricci	Ricce	k1gFnSc4
flow	flow	k?
with	with	k1gMnSc1
surgery	surgera	k1gFnSc2
on	on	k3xPp3gInSc1
three-manifolds	three-manifolds	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
zániku	zánik	k1gInSc2
řešení	řešení	k1gNnSc2
Ricciho	Ricci	k1gMnSc2
toku	tok	k1gInSc2
na	na	k7c6
některých	některý	k3yIgFnPc6
trojrozměrných	trojrozměrný	k2eAgFnPc6d1
varietách	varieta	k1gFnPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Finite	Finit	k1gInSc5
extinction	extinction	k1gInSc4
time	time	k1gFnSc1
for	forum	k1gNnPc2
the	the	k?
solutions	solutions	k6eAd1
to	ten	k3xDgNnSc1
the	the	k?
Ricci	Ricce	k1gMnSc3
flow	flow	k?
on	on	k3xPp3gMnSc1
certain	certain	k1gMnSc1
three-manifolds	three-manifoldsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
nich	on	k3xPp3gMnPc6
Perelman	Perelman	k1gMnSc1
stručně	stručně	k6eAd1
popsal	popsat	k5eAaPmAgMnS
původní	původní	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
dokazování	dokazování	k1gNnSc2
Poincarého	Poincarý	k2eAgInSc2d1
hypotézy	hypotéza	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
dostal	dostat	k5eAaPmAgMnS
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
mezinárodní	mezinárodní	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
–	–	k?
Fieldsovu	Fieldsův	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
–	–	k?
za	za	k7c4
řešení	řešení	k1gNnSc4
Poincarého	Poincarý	k2eAgInSc2d1
hypotézy	hypotéza	k1gFnPc1
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgFnSc1d1
formulace	formulace	k1gFnSc1
ceny	cena	k1gFnSc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Za	za	k7c4
přínos	přínos	k1gInSc4
pro	pro	k7c4
geometrii	geometrie	k1gFnSc4
a	a	k8xC
revoluční	revoluční	k2eAgFnPc1d1
myšlenky	myšlenka	k1gFnPc1
ve	v	k7c6
studiu	studio	k1gNnSc6
geometrické	geometrický	k2eAgFnSc2d1
a	a	k8xC
analytické	analytický	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
toku	tok	k1gInSc2
Ricci	Ricce	k1gFnSc3
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2010	#num#	k4
mu	on	k3xPp3gNnSc3
Clayův	Clayův	k2eAgInSc4d1
matematický	matematický	k2eAgInSc4d1
ústav	ústav	k1gInSc4
udělil	udělit	k5eAaPmAgInS
cenu	cena	k1gFnSc4
a	a	k8xC
odměnu	odměna	k1gFnSc4
jeden	jeden	k4xCgInSc1
milión	milión	k4xCgInSc1
amerických	americký	k2eAgMnPc2d1
dolarů	dolar	k1gInPc2
za	za	k7c4
prokázání	prokázání	k1gNnSc4
Poincarého	Poincarý	k2eAgInSc2d1
hypotezy	hypotez	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perelman	Perelman	k1gMnSc1
ale	ale	k9
ignoroval	ignorovat	k5eAaImAgMnS
matematickou	matematický	k2eAgFnSc4d1
konferenci	konference	k1gFnSc4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
cena	cena	k1gFnSc1
tisíciletí	tisíciletí	k1gNnSc2
udělena	udělen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
veřejně	veřejně	k6eAd1
oznámil	oznámit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
odmítnutí	odmítnutí	k1gNnSc4
a	a	k8xC
vysvětlil	vysvětlit	k5eAaPmAgInS
to	ten	k3xDgNnSc1
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Odmítl	odmítnout	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
mnoho	mnoho	k4c4
důvodů	důvod	k1gInPc2
jak	jak	k8xC,k8xS
pro	pro	k7c4
<g/>
,	,	kIx,
tak	tak	k6eAd1
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
tak	tak	k9
dlouho	dlouho	k6eAd1
rozhodoval	rozhodovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
nesouhlas	nesouhlas	k1gInSc4
s	s	k7c7
organizovanou	organizovaný	k2eAgFnSc7d1
matematickou	matematický	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesouhlasím	souhlasit	k5eNaImIp1nS
s	s	k7c7
jejich	jejich	k3xOp3gNnSc7
rozhodnutím	rozhodnutí	k1gNnSc7
<g/>
,	,	kIx,
považuji	považovat	k5eAaImIp1nS
je	být	k5eAaImIp3nS
za	za	k7c4
nespravedlivé	spravedlivý	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věřím	věřit	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
příspěvek	příspěvek	k1gInSc4
amerického	americký	k2eAgMnSc2d1
matematika	matematik	k1gMnSc2
Hamiltona	Hamilton	k1gMnSc2
k	k	k7c3
řešení	řešení	k1gNnSc3
tohoto	tento	k3xDgInSc2
problému	problém	k1gInSc2
není	být	k5eNaImIp3nS
o	o	k7c4
nic	nic	k3yNnSc4
menší	malý	k2eAgInSc1d2
než	než	k8xS
můj	můj	k3xOp1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
Perelmana	Perelman	k1gMnSc2
byl	být	k5eAaImAgInS
Hamiltonův	Hamiltonův	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
výrazně	výrazně	k6eAd1
zpomalen	zpomalen	k2eAgInSc4d1
kvůli	kvůli	k7c3
nepřekonatelným	překonatelný	k2eNgFnPc3d1
technickým	technický	k2eAgFnPc3d1
obtížím	obtíž	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2011	#num#	k4
zřídil	zřídit	k5eAaPmAgInS
Clayův	Clayův	k2eAgInSc1d1
matematický	matematický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
společně	společně	k6eAd1
s	s	k7c7
Institutem	institut	k1gInSc7
Henriho	Henri	k1gMnSc2
Poincarého	Poincarý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
pozici	pozice	k1gFnSc4
pro	pro	k7c4
mladé	mladý	k2eAgMnPc4d1
matematiky	matematik	k1gMnPc4
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
bude	být	k5eAaImBp3nS
vyplácena	vyplácet	k5eAaImNgFnS
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
nepřijal	přijmout	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
získali	získat	k5eAaPmAgMnP
Richard	Richard	k1gMnSc1
Hamilton	Hamilton	k1gInSc1
a	a	k8xC
Demetrios	Demetrios	k1gMnSc1
Christodoulus	Christodoulus	k1gMnSc1
tzv.	tzv.	kA
Shao	Shao	k6eAd1
matematickou	matematický	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
$	$	kIx~
<g/>
1	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
Hamilton	Hamilton	k1gInSc1
byl	být	k5eAaImAgInS
vyznamenán	vyznamenat	k5eAaPmNgInS
za	za	k7c4
vytvoření	vytvoření	k1gNnSc4
matematické	matematický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
pak	pak	k6eAd1
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
použil	použít	k5eAaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
práci	práce	k1gFnSc6
na	na	k7c6
důkazu	důkaz	k1gInSc6
Poincarého	Poincarý	k2eAgInSc2d1
hypotézy	hypotéza	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Hamilton	Hamilton	k1gInSc1
tuto	tento	k3xDgFnSc4
cenu	cena	k1gFnSc4
přijal	přijmout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2005	#num#	k4
odstoupil	odstoupit	k5eAaPmAgMnS
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
postu	post	k1gInSc2
vedoucího	vedoucí	k2eAgMnSc2d1
výzkumného	výzkumný	k2eAgMnSc2d1
pracovníka	pracovník	k1gMnSc2
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
matematické	matematický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
,	,	kIx,
přestal	přestat	k5eAaPmAgMnS
pracovat	pracovat	k5eAaImF
na	na	k7c4
POMI	POMI	kA
(	(	kIx(
<g/>
Matematický	matematický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
A.	A.	kA
Steklovа	Steklovа	k1gMnSc1
POMI	POMI	kA
RAS	ras	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
téměř	téměř	k6eAd1
úplně	úplně	k6eAd1
přerušil	přerušit	k5eAaPmAgMnS
kontakty	kontakt	k1gInPc4
s	s	k7c7
kolegy	kolega	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2011	#num#	k4
matematik	matematik	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
přijmout	přijmout	k5eAaPmF
nabídku	nabídka	k1gFnSc4
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
členem	člen	k1gInSc7
Ruské	ruský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vede	vést	k5eAaImIp3nS
osamocený	osamocený	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
ignoruje	ignorovat	k5eAaImIp3nS
tisk	tisk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
v	v	k7c6
Kupchinu	Kupchin	k1gInSc6
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
matkou	matka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tisku	tisk	k1gInSc6
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
zprávy	zpráva	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
žije	žít	k5eAaImIp3nS
Grigorij	Grigorij	k1gFnSc1
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
tam	tam	k6eAd1
jen	jen	k9
občas	občas	k6eAd1
jezdí	jezdit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
И	И	k?
с	с	k?
м	м	k?
Г	Г	k?
П	П	k?
<g/>
:	:	kIx,
З	З	k?
м	м	k?
м	м	k?
д	д	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
Я	Я	k?
м	м	k?
у	у	k?
В	В	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Komsomolskaja	Komsomolskaj	k2eAgFnSc1d1
Pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
↑	↑	k?
Poslednee	Poslednee	k1gInSc1
„	„	k?
<g/>
nět	nět	k?
<g/>
“	“	k?
doktora	doktor	k1gMnSc2
Perelmana	Perelman	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interfax	Interfax	k1gInSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Poincarého	Poincarý	k2eAgInSc2d1
domněnka	domněnka	k1gFnSc1
</s>
<s>
Problémy	problém	k1gInPc1
tisíciletí	tisíciletí	k1gNnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Držitelé	držitel	k1gMnPc1
Fieldsovy	Fieldsův	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
</s>
<s>
Lars	Lars	k6eAd1
Ahlfors	Ahlfors	k1gInSc1
<g/>
,	,	kIx,
Jesse	Jesse	k1gFnSc1
Douglas	Douglas	k1gMnSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laurent	Laurent	k1gMnSc1
Schwartz	Schwartz	k1gMnSc1
<g/>
,	,	kIx,
Atle	Atle	k1gFnSc1
Selberg	Selberg	k1gMnSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kunihiko	Kunihika	k1gFnSc5
Kodaira	Kodair	k1gMnSc2
<g/>
,	,	kIx,
Jean-Pierre	Jean-Pierr	k1gMnSc5
Serre	Serr	k1gMnSc5
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Klaus	Klaus	k1gMnSc1
Roth	Roth	k1gMnSc1
<g/>
,	,	kIx,
René	René	k1gMnSc1
Thom	Thom	k1gMnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lars	Lars	k1gInSc1
Hörmander	Hörmander	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
John	John	k1gMnSc1
Milnor	Milnor	k1gMnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Atiyah	Atiyah	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
Cohen	Cohna	k1gFnPc2
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
Grothendieck	Grothendiecka	k1gFnPc2
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
Smale	Smale	k1gInSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alan	Alan	k1gMnSc1
Baker	Baker	k1gMnSc1
<g/>
,	,	kIx,
Heisuke	Heisuk	k1gMnPc4
Hironaka	Hironak	k1gMnSc4
<g/>
,	,	kIx,
Sergej	Sergej	k1gMnSc1
Petrovič	Petrovič	k1gMnSc1
Novikov	Novikov	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Griggs	Griggsa	k1gFnPc2
Thompson	Thompson	k1gMnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Enrico	Enrico	k1gMnSc1
Bombieri	Bombier	k1gMnPc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Mumford	Mumford	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pierre	Pierr	k1gMnSc5
Deligne	Delign	k1gMnSc5
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Fefferman	Fefferman	k1gMnSc1
<g/>
,	,	kIx,
Grigorij	Grigorij	k1gMnSc1
Margulis	Margulis	k1gFnSc2
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Quillen	Quillen	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alain	Alain	k1gMnSc1
Connes	Connes	k1gMnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Thurston	Thurston	k1gInSc4
<g/>
,	,	kIx,
Čchiou	Čchiá	k1gFnSc4
Čcheng-tung	Čcheng-tunga	k1gFnPc2
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Simon	Simon	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Donaldson	Donaldson	k1gMnSc1
<g/>
,	,	kIx,
Gerd	Gerd	k1gMnSc1
Faltings	Faltings	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Freedman	Freedman	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladimir	Vladimir	k1gMnSc1
Drinfeld	Drinfeld	k1gMnSc1
<g/>
,	,	kIx,
Vaughan	Vaughan	k1gMnSc1
Jones	Jones	k1gMnSc1
<g/>
,	,	kIx,
Šigefumi	Šigefu	k1gFnPc7
Mori	Mori	k1gNnSc2
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Witten	Witten	k2eAgMnSc1d1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jefim	Jefim	k1gInSc1
Zelmanov	Zelmanov	k1gInSc1
<g/>
,	,	kIx,
Pierre-Louis	Pierre-Louis	k1gInSc1
Lions	Lions	k1gInSc1
<g/>
,	,	kIx,
Jean	Jean	k1gMnSc1
Bourgain	Bourgain	k1gMnSc1
<g/>
,	,	kIx,
Jean-Christophe	Jean-Christophe	k1gFnSc1
Yoccoz	Yoccoz	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Richard	Richard	k1gMnSc1
Borcherds	Borcherds	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Timothy	Timotha	k1gFnSc2
Gowers	Gowersa	k1gFnPc2
<g/>
,	,	kIx,
Maxim	Maxim	k1gMnSc1
Lvovič	Lvovič	k1gMnSc1
Koncevič	Koncevič	k1gMnSc1
<g/>
,	,	kIx,
Curtis	Curtis	k1gInSc1
T.	T.	kA
McMullen	McMullen	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laurent	Laurent	k1gInSc1
Lafforgue	Lafforgue	k1gFnSc1
<g/>
,	,	kIx,
Vladimir	Vladimir	k1gMnSc1
Voevodskij	Voevodskij	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Andrej	Andrej	k1gMnSc1
Okounkov	Okounkov	k1gInSc1
<g/>
,	,	kIx,
Grigorij	Grigorij	k1gMnSc1
Perelman	Perelman	k1gMnSc1
<g/>
,	,	kIx,
Terence	Terence	k1gFnSc1
Tao	Tao	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Wendelin	Wendelin	k2eAgMnSc1d1
Werner	Werner	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elon	Elon	k1gInSc1
Lindenstrauss	Lindenstrauss	k1gInSc1
<g/>
,	,	kIx,
Ngo	Ngo	k1gFnSc1
Bao	Bao	k1gFnSc2
Chau	Chaus	k1gInSc2
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
Smirnov	Smirnovo	k1gNnPc2
<g/>
,	,	kIx,
Cédric	Cédrice	k1gFnPc2
Villani	Villan	k1gMnPc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marjam	Marjam	k1gInSc1
Mírzácháníová	Mírzácháníová	k1gFnSc1
<g/>
,	,	kIx,
Artur	Artur	k1gMnSc1
Avila	Avila	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Hairer	Hairer	k1gMnSc1
<g/>
,	,	kIx,
Manjul	Manjul	k1gInSc1
Bhargava	Bhargava	k1gFnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Caucher	Cauchra	k1gFnPc2
Birkar	Birkar	k1gMnSc1
<g/>
,	,	kIx,
Alessio	Alessio	k6eAd1
Figalli	Figall	k1gMnPc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Scholze	Scholze	k1gFnSc2
<g/>
,	,	kIx,
Akshay	Akshaa	k1gFnSc2
Venkatesh	Venkatesha	k1gFnPc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
136216218	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5559	#num#	k4
2454	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2008107620	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
68746706	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2008107620	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
