<s>
Grzegorz	Grzegorz	k1gInSc1
Schetyna	Schetyn	k1gInSc2
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1
Schetyna	Schetyn	k1gInSc2
</s>
<s>
Maršálek	Maršálek	k1gMnSc1
Sejmu	sejmout	k5eAaPmIp1nS
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
Prezident	prezident	k1gMnSc1
</s>
<s>
Bronisław	Bronisław	k?
KomorowskiAndrzej	KomorowskiAndrzej	k1gMnSc1
Duda	Duda	k1gMnSc1
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Donald	Donald	k1gMnSc1
Tusk	Tusk	k1gMnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Bronisław	Bronisław	k?
Komorowski	Komorowsk	k1gMnSc5
Nástupce	nástupce	k1gMnSc5
</s>
<s>
Ewa	Ewa	k?
Kopaczová	Kopaczová	k1gFnSc1
</s>
<s>
Prezident	prezident	k1gMnSc1
Polska	Polsko	k1gNnSc2
(	(	kIx(
<g/>
úřadující	úřadující	k2eAgFnSc7d1
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Donald	Donald	k1gMnSc1
Tusk	Tusk	k1gMnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Bogdan	Bogdan	k1gMnSc1
Borusewicz	Borusewicz	k1gMnSc1
(	(	kIx(
<g/>
úřadující	úřadující	k2eAgMnSc1d1
<g/>
)	)	kIx)
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Bronisław	Bronisław	k?
Komorowski	Komorowski	k1gNnSc1
</s>
<s>
Místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
Polské	polský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
Prezident	prezident	k1gMnSc1
</s>
<s>
Lech	Lech	k1gMnSc1
Kaczyński	Kaczyński	k1gNnSc2
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Donald	Donald	k1gMnSc1
Tusk	Tusk	k1gMnSc1
</s>
<s>
Ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
a	a	k8xC
administrativy	administrativa	k1gFnSc2
Polské	polský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
Prezident	prezident	k1gMnSc1
</s>
<s>
Lech	Lech	k1gMnSc1
Kaczyński	Kaczyński	k1gNnSc2
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Donald	Donald	k1gMnSc1
Tusk	Tusk	k1gMnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Władysław	Władysław	k?
Stasiak	Stasiak	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Jerzy	Jerza	k1gFnPc1
Miller	Miller	k1gMnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1963	#num#	k4
Opole	Opole	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
polská	polský	k2eAgFnSc1d1
Profese	profese	k1gFnSc1
</s>
<s>
politik	politik	k1gMnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
římskokatolické	římskokatolický	k2eAgInPc1d1
Commons	Commons	k1gInSc1
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1
Schetyna	Schetyn	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1
Juliusz	Juliusz	k1gInSc1
Schetyna	Schetyna	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
18	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1963	#num#	k4
<g/>
,	,	kIx,
Opole	Opole	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
polský	polský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
Občanská	občanský	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
vstupem	vstup	k1gInSc7
do	do	k7c2
politiky	politika	k1gFnSc2
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1
Schetyna	Schetyn	k1gInSc2
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
studoval	studovat	k5eAaImAgMnS
historii	historie	k1gFnSc4
na	na	k7c6
Vratislavské	vratislavský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
aktivním	aktivní	k2eAgMnSc7d1
členem	člen	k1gMnSc7
protikomunistického	protikomunistický	k2eAgNnSc2d1
studentského	studentský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc4
historie	historie	k1gFnSc2
ukončil	ukončit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
V	v	k7c6
Sejmu	Sejm	k1gInSc6
byl	být	k5eAaImAgInS
poslancem	poslanec	k1gMnSc7
v	v	k7c6
letech	léto	k1gNnPc6
1997	#num#	k4
-	-	kIx~
2001	#num#	k4
a	a	k8xC
2001-	2001-	k4
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
do	do	k7c2
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
byl	být	k5eAaImAgMnS
ministrem	ministr	k1gMnSc7
vnitra	vnitro	k1gNnSc2
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
zastával	zastávat	k5eAaImAgMnS
funkci	funkce	k1gFnSc4
maršálka	maršálka	k1gFnSc1
Sejmu	Sejm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
titulu	titul	k1gInSc2
této	tento	k3xDgFnSc2
funkce	funkce	k1gFnSc2
byl	být	k5eAaImAgMnS
zároveň	zároveň	k6eAd1
úřadujícím	úřadující	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Funkci	funkce	k1gFnSc4
úřadujícího	úřadující	k2eAgMnSc4d1
prezidenta	prezident	k1gMnSc4
vykonával	vykonávat	k5eAaImAgMnS
do	do	k7c2
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
složil	složit	k5eAaPmAgMnS
přísahu	přísaha	k1gFnSc4
nově	nově	k6eAd1
zvolený	zvolený	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Bronisław	Bronisław	k1gFnSc2
Komorowski	Komorowsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
do	do	k7c2
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
byl	být	k5eAaImAgMnS
ministrem	ministr	k1gMnSc7
zahraničí	zahraničí	k1gNnSc2
ve	v	k7c6
vládě	vláda	k1gFnSc6
Ewy	Ewy	k1gFnSc2
Kopaczové	Kopaczová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
vystřídal	vystřídat	k5eAaPmAgMnS
Radosława	Radosława	k1gMnSc1
Sikorského	Sikorský	k2eAgNnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
předsedou	předseda	k1gMnSc7
Občanské	občanský	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1
Schetyna	Schetyno	k1gNnSc2
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Kalina	kalina	k1gFnSc1
rozena	rozen	k2eAgFnSc1d1
Rowińska	Rowińska	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
dceru	dcera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bydlí	bydlet	k5eAaImIp3nS
ve	v	k7c6
Vratislavi	Vratislav	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Články	článek	k1gInPc1
</s>
<s>
EHL	EHL	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
Kaczyńskému	Kaczyńské	k1gNnSc3
vyráží	vyrážet	k5eAaImIp3nS
tvrdý	tvrdý	k2eAgMnSc1d1
soupeř	soupeř	k1gMnSc1
<g/>
:	:	kIx,
Grzegorz	Grzegorz	k1gInSc1
Schetyna	Schetyn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27.1	27.1	k4
<g/>
.2016	.2016	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Polsko	Polsko	k1gNnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
dni	den	k1gInSc6
tři	tři	k4xCgMnPc1
zastupující	zastupující	k2eAgMnPc1d1
prezidenty	prezident	k1gMnPc4
-	-	kIx~
Článek	článek	k1gInSc1
na	na	k7c4
Novinky	novinka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Sikorski	Sikorski	k1gNnSc1
už	už	k9
nebude	být	k5eNaImBp3nS
šéfem	šéf	k1gMnSc7
polské	polský	k2eAgFnSc2d1
diplomacie	diplomacie	k1gFnSc2
http://zpravy.aktualne.cz/zahranici/sikorski-uz-nebude-sefem-polske-diplomacie/r~6878b54a3fe511e480c30025900fea04/,	http://zpravy.aktualne.cz/zahranici/sikorski-uz-nebude-sefem-polske-diplomacie/r~6878b54a3fe511e480c30025900fea04/,	k4
19	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
politika	politika	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Prezident	prezident	k1gMnSc1
Polska	Polsko	k1gNnSc2
Druhá	druhý	k4xOgFnSc1
Polská	polský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gabriel	Gabriel	k1gMnSc1
Narutowicz	Narutowicz	k1gMnSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stanisław	Stanisław	k1gFnSc1
Wojciechowski	Wojciechowsk	k1gFnSc2
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ignacy	Ignac	k2eAgFnPc4d1
Mościcki	Mościck	k1gFnPc4
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
Polská	polský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bolesław	Bolesław	k?
Bierut	Bierut	k1gInSc1
Polská	polský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
předseda	předseda	k1gMnSc1
státní	státní	k2eAgMnSc1d1
radyPrvní	radyPrvnit	k5eAaPmIp3nS
tajemník	tajemník	k1gMnSc1
Polské	polský	k2eAgFnSc2d1
sjednocené	sjednocený	k2eAgFnSc2d1
dělnické	dělnický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
Aleksander	Aleksander	k1gMnSc1
Zawadzki	Zawadzk	k1gFnSc2
•	•	k?
Edward	Edward	k1gMnSc1
Ochab	ochabit	k5eAaPmRp2nS
•	•	k?
Marian	Marian	k1gMnSc1
Spychalski	Spychalsk	k1gFnSc2
•	•	k?
Józef	Józef	k1gMnSc1
Cyrankiewicz	Cyrankiewicz	k1gMnSc1
•	•	k?
Henryk	Henryk	k1gMnSc1
Jabłoński	Jabłońsk	k1gFnSc2
•	•	k?
Wojciech	Wojciech	k1gInSc1
JaruzelskiBolesław	JaruzelskiBolesław	k1gMnSc1
Bierut	Bierut	k1gMnSc1
•	•	k?
Edward	Edward	k1gMnSc1
Ochab	ochabit	k5eAaPmRp2nS
•	•	k?
Władysław	Władysław	k1gMnSc1
Gomułka	Gomułka	k1gMnSc1
•	•	k?
Edward	Edward	k1gMnSc1
Gierek	Gierek	k1gMnSc1
•	•	k?
Stanisław	Stanisław	k1gMnSc1
Kania	Kanium	k1gNnSc2
•	•	k?
Wojciech	Wojciech	k1gInSc4
Jaruzelski	Jaruzelsk	k1gFnSc2
Třetí	třetí	k4xOgFnSc1
Polská	polský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wojciech	Wojciech	k1gInSc1
Jaruzelski	Jaruzelsk	k1gFnSc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lech	Lech	k1gMnSc1
Wałęsa	Wałęs	k1gMnSc2
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aleksander	Aleksander	k1gInSc1
Kwaśniewski	Kwaśniewsk	k1gFnSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lech	Lech	k1gMnSc1
Kaczyński	Kaczyńsk	k1gFnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bronisław	Bronisław	k1gMnSc5
Komorowski	Komorowsk	k1gMnSc5
(	(	kIx(
<g/>
úřadující	úřadující	k2eAgFnSc7d1
<g/>
)	)	kIx)
•	•	k?
Bogdan	Bogdan	k1gMnSc1
Borusewicz	Borusewicz	k1gMnSc1
úřadující	úřadující	k2eAgMnSc1d1
•	•	k?
Grzegorz	Grzegorz	k1gMnSc1
Schetyna	Schetyn	k1gMnSc2
úřadující	úřadující	k2eAgFnSc2d1
•	•	k?
Bronisław	Bronisław	k1gFnSc2
Komorowski	Komorowsk	k1gFnSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Andrzej	Andrzej	k1gMnSc1
Duda	Duda	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ministři	ministr	k1gMnPc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
Polska	Polsko	k1gNnSc2
Druhá	druhý	k4xOgFnSc1
Polská	polský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Leon	Leona	k1gFnPc2
Wasilewski	Wasilewski	k1gNnSc2
</s>
<s>
Ignacy	Ignaca	k1gMnSc2
Jan	Jan	k1gMnSc1
Paderewski	Paderewsk	k1gFnPc4
</s>
<s>
Władysław	Władysław	k?
Wróblewski	Wróblewski	k1gNnSc1
</s>
<s>
Stanisław	Stanisław	k?
Patek	patka	k1gFnPc2
</s>
<s>
Eustachy	Eustach	k1gMnPc4
Sapieha	Sapieha	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Dąbski	Dąbsk	k1gFnSc2
</s>
<s>
Konstanty	konstanta	k1gFnPc1
Skirmunt	Skirmunta	k1gFnPc2
</s>
<s>
Gabriel	Gabriel	k1gMnSc1
Narutowicz	Narutowicz	k1gMnSc1
</s>
<s>
Aleksander	Aleksander	k1gInSc1
Skrzyński	Skrzyńsk	k1gFnSc2
</s>
<s>
Marian	Marian	k1gMnSc1
Seyda	Seyda	k1gMnSc1
</s>
<s>
Roman	Roman	k1gMnSc1
Dmowski	Dmowsk	k1gFnSc2
</s>
<s>
Karol	Karol	k1gInSc4
Bertoni	Berton	k1gMnPc1
</s>
<s>
Maurycy	Mauryc	k2eAgFnPc1d1
Zamoyski	Zamoysk	k1gFnPc1
</s>
<s>
Aleksander	Aleksander	k1gInSc1
Skrzyński	Skrzyńsk	k1gFnSc2
</s>
<s>
Kajetan	Kajetan	k1gInSc1
Dzierżykraj-Morawski	Dzierżykraj-Morawsk	k1gFnSc2
</s>
<s>
August	August	k1gMnSc1
Zaleski	Zalesk	k1gFnSc2
</s>
<s>
August	August	k1gMnSc1
Zaleski	Zalesk	k1gFnSc2
</s>
<s>
Józef	Józef	k1gInSc1
Beck	Beck	k1gInSc1
Exilová	exilový	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
August	August	k1gMnSc1
Zaleski	Zalesk	k1gFnSc2
</s>
<s>
Edward	Edward	k1gMnSc1
Raczyński	Raczyńsk	k1gFnSc2
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Romer	Romer	k1gMnSc1
</s>
<s>
Adam	Adam	k1gMnSc1
Tarnowski	Tarnowsk	k1gFnSc2
Polská	polský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Edward	Edward	k1gMnSc1
Osóbka-Morawski	Osóbka-Morawsk	k1gFnSc2
</s>
<s>
Wincenty	Wincenta	k1gFnPc1
Rzymowski	Rzymowsk	k1gFnSc2
</s>
<s>
Zygmunt	Zygmunt	k1gInSc1
Modzelewski	Modzelewsk	k1gFnSc2
</s>
<s>
Stanisław	Stanisław	k?
Skrzeszewski	Skrzeszewski	k1gNnSc1
</s>
<s>
Adam	Adam	k1gMnSc1
Rapacki	Rapack	k1gFnSc2
</s>
<s>
Stefan	Stefan	k1gMnSc1
Jędrychowski	Jędrychowsk	k1gFnSc2
</s>
<s>
Stefan	Stefan	k1gMnSc1
Olszowski	Olszowsk	k1gFnSc2
</s>
<s>
Emil	Emil	k1gMnSc1
Wojtaszek	Wojtaszka	k1gFnPc2
</s>
<s>
Józef	Józef	k1gMnSc1
Czyrek	Czyrek	k1gMnSc1
</s>
<s>
Stefan	Stefan	k1gMnSc1
Olszowski	Olszowsk	k1gFnSc2
</s>
<s>
Marian	Marian	k1gMnSc1
Orzechowski	Orzechowsk	k1gFnSc2
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Olechowski	Olechowsk	k1gFnSc2
Třetí	třetí	k4xOgFnSc1
Polská	polský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Krzysztof	Krzysztof	k1gInSc1
Skubiszewski	Skubiszewsk	k1gFnSc2
</s>
<s>
Andrzej	Andrzat	k5eAaPmRp2nS,k5eAaImRp2nS
Olechowski	Olechowsk	k1gMnSc5
</s>
<s>
Władysław	Władysław	k?
Bartoszewski	Bartoszewski	k1gNnSc1
</s>
<s>
Dariusz	Dariusz	k1gInSc4
Rosati	Rosat	k1gMnPc1
</s>
<s>
Bronisław	Bronisław	k?
Geremek	Geremek	k1gInSc1
</s>
<s>
Władysław	Władysław	k?
Bartoszewski	Bartoszewski	k1gNnSc1
</s>
<s>
Włodzimierz	Włodzimierz	k1gMnSc1
Cimoszewicz	Cimoszewicz	k1gMnSc1
</s>
<s>
Adam	Adam	k1gMnSc1
Daniel	Daniel	k1gMnSc1
Rotfeld	Rotfeld	k1gMnSc1
</s>
<s>
Stefan	Stefan	k1gMnSc1
Meller	Meller	k1gMnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Fotyga	Fotyga	k1gFnSc1
</s>
<s>
Radosław	Radosław	k?
Sikorski	Sikorski	k1gNnSc1
</s>
<s>
Grzegorz	Grzegorz	k1gInSc1
Schetyna	Schetyn	k1gInSc2
</s>
<s>
Witold	Witold	k1gInSc1
Waszczykowski	Waszczykowsk	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1198127104	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2019114500	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
307174896	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2019114500	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Polsko	Polsko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
