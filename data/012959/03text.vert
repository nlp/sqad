<p>
<s>
Anakonda	anakonda	k1gFnSc1	anakonda
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gInSc1	Eunectes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
velkých	velká	k1gFnPc2	velká
<g/>
,	,	kIx,	,
nejedovatých	jedovatý	k2eNgMnPc2d1	nejedovatý
hadů	had	k1gMnPc2	had
vyskytujících	vyskytující	k2eAgMnPc2d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
skupinu	skupina	k1gFnSc4	skupina
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
označuje	označovat	k5eAaImIp3nS	označovat
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
anakonda	anakonda	k1gFnSc1	anakonda
velká	velký	k2eAgFnSc1d1	velká
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gMnSc1	Eunectes
murinus	murinus	k1gMnSc1	murinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
had	had	k1gMnSc1	had
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Anakonda	anakonda	k1gFnSc1	anakonda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
od	od	k7c2	od
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
a	a	k8xC	a
Venezuely	Venezuela	k1gFnSc2	Venezuela
až	až	k9	až
po	po	k7c4	po
Argentinu	Argentina	k1gFnSc4	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
anakonda	anakonda	k1gFnSc1	anakonda
velká	velký	k2eAgFnSc1d1	velká
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gMnSc1	Eunectes
murinus	murinus	k1gMnSc1	murinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
areálem	areál	k1gInSc7	areál
svého	své	k1gNnSc2	své
výskytu	výskyt	k1gInSc2	výskyt
zahrnujícím	zahrnující	k2eAgMnSc7d1	zahrnující
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
sever	sever	k1gInSc4	sever
jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Anakonda	anakonda	k1gFnSc1	anakonda
velká	velký	k2eAgFnSc1d1	velká
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gMnSc1	Eunectes
murinus	murinus	k1gMnSc1	murinus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anakonda	anakonda	k1gFnSc1	anakonda
žlutá	žlutý	k2eAgFnSc1d1	žlutá
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gMnSc1	Eunectes
notaeus	notaeus	k1gMnSc1	notaeus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anakonda	anakonda	k1gFnSc1	anakonda
temnoskrvnná	temnoskrvnný	k2eAgFnSc1d1	temnoskrvnný
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gMnSc1	Eunectes
deschauenseei	deschauensee	k1gFnSc2	deschauensee
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anakonda	anakonda	k1gFnSc1	anakonda
bolivijská	bolivijský	k2eAgFnSc1d1	bolivijská
(	(	kIx(	(
<g/>
Eunectes	Eunectes	k1gMnSc1	Eunectes
beniensis	beniensis	k1gFnSc2	beniensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhynulé	vyhynulý	k2eAgInPc1d1	vyhynulý
druhy	druh	k1gInPc1	druh
===	===	k?	===
</s>
</p>
<p>
<s>
Eunectes	Eunectes	k1gInSc4	Eunectes
stirtoni	stirton	k1gMnPc1	stirton
-	-	kIx~	-
popsána	popsat	k5eAaPmNgFnS	popsat
Hofstetterem	Hofstetter	k1gInSc7	Hofstetter
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
období	období	k1gNnSc6	období
Miocénu	miocén	k1gInSc2	miocén
</s>
</p>
<p>
<s>
===	===	k?	===
Mýty	mýtus	k1gInPc1	mýtus
===	===	k?	===
</s>
</p>
<p>
<s>
Obří	obří	k2eAgFnSc1d1	obří
anakonda	anakonda	k1gFnSc1	anakonda
je	být	k5eAaImIp3nS	být
mýtický	mýtický	k2eAgMnSc1d1	mýtický
had	had	k1gMnSc1	had
obrovských	obrovský	k2eAgInPc2d1	obrovský
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
nalezen	nalézt	k5eAaBmNgInS	nalézt
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Anaconda	Anacond	k1gMnSc2	Anacond
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eunectes	Eunectesa	k1gFnPc2	Eunectesa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
