<s>
Kdy	kdy	k6eAd1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
Herbert	Herbert	k1gMnSc1
Simon	Simon	k1gMnSc1
<g/>
,	,	kIx,
americký	americký	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
počítačovou	počítačový	k2eAgFnSc7d1
vědou	věda	k1gFnSc7
<g/>
,	,	kIx,
kognitivní	kognitivní	k2eAgFnSc7d1
psychologií	psychologie	k1gFnSc7
<g/>
,	,	kIx,
ekonomikou	ekonomika	k1gFnSc7
a	a	k8xC
filozofií	filozofie	k1gFnSc7
<g/>
?	?	kIx.
</s>