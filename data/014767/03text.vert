<s>
Herbert	Herbert	k1gMnSc1
A.	A.	kA
Simon	Simon	k1gMnSc1
</s>
<s>
Herbert	Herbert	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Simon	Simona	k1gFnPc2
Narození	narození	k1gNnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1916	#num#	k4
Milwaukee	Milwaukee	k1gFnPc2
<g/>
,	,	kIx,
Wisconsin	Wisconsina	k1gFnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2001	#num#	k4
Pittsburgh	Pittsburgh	k1gInSc1
<g/>
,	,	kIx,
Pennsylvania	Pennsylvanium	k1gNnPc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
chirurgické	chirurgický	k2eAgFnPc4d1
komplikace	komplikace	k1gFnPc4
Povolání	povolání	k1gNnSc2
</s>
<s>
ekonom	ekonom	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
sociolog	sociolog	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnPc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Chicagská	chicagský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Administrative	Administrativ	k1gInSc5
Behavior	Behaviora	k1gFnPc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Americké	americký	k2eAgFnSc2d1
psychologické	psychologický	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
za	za	k7c4
význačný	význačný	k2eAgInSc4d1
vědecký	vědecký	k2eAgInSc4d1
přínos	přínos	k1gInSc4
psychologii	psychologie	k1gFnSc4
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
<g/>
Turingova	Turingův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1
pamětní	pamětní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
William	William	k1gInSc1
Procter	Proctrum	k1gNnPc2
Prize	Prize	k1gFnSc2
for	forum	k1gNnPc2
Scientific	Scientific	k1gMnSc1
Achievement	Achievement	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
Josiah	Josiah	k1gInSc1
Willard	Willard	k1gMnSc1
Gibbs	Gibbsa	k1gFnPc2
Lectureship	Lectureship	k1gMnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Dorothea	Dorothea	k1gFnSc1
Isabel	Isabela	k1gFnPc2
Pye	Pye	k1gFnSc2
Děti	dítě	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Herbert	Herbert	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Simon	Simon	k1gMnSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1916	#num#	k4
Milwaukee	Milwaukee	k1gFnPc2
<g/>
,	,	kIx,
Wisconsin	Wisconsina	k1gFnPc2
<g/>
,	,	kIx,
USA	USA	kA
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
Pittsburgh	Pittsburgh	k1gInSc1
<g/>
,	,	kIx,
Pensylvánie	Pensylvánie	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
počítačovou	počítačový	k2eAgFnSc7d1
vědou	věda	k1gFnSc7
<g/>
,	,	kIx,
kognitivní	kognitivní	k2eAgFnSc7d1
psychologií	psychologie	k1gFnSc7
<g/>
,	,	kIx,
ekonomikou	ekonomika	k1gFnSc7
a	a	k8xC
filozofií	filozofie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Položil	položit	k5eAaPmAgInS
základy	základ	k1gInPc4
rozhodovacího	rozhodovací	k2eAgInSc2d1
přístupu	přístup	k1gInSc2
k	k	k7c3
managementu	management	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
považuje	považovat	k5eAaImIp3nS
rozhodování	rozhodování	k1gNnSc4
za	za	k7c4
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
řízení	řízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
za	za	k7c4
průkopnický	průkopnický	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
rozhodovacích	rozhodovací	k2eAgInPc2d1
procesů	proces	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
organizace	organizace	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
Turingovu	Turingův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
Richardem	Richard	k1gMnSc7
Cyertem	Cyert	k1gMnSc7
a	a	k8xC
James	James	k1gInSc4
G.	G.	kA
Marchem	March	k1gMnSc7
přišli	přijít	k5eAaPmAgMnP
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
až	až	k6eAd1
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
kritikou	kritika	k1gFnSc7
teorie	teorie	k1gFnSc2
racionálního	racionální	k2eAgNnSc2d1
rozhodování	rozhodování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
Simon	Simon	k1gMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
publikacích	publikace	k1gFnPc6
<g/>
,	,	kIx,
objektivně	objektivně	k6eAd1
racionální	racionální	k2eAgNnSc4d1
rozhodování	rozhodování	k1gNnSc4
je	být	k5eAaImIp3nS
nereálné	reálný	k2eNgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
klade	klást	k5eAaImIp3nS
přehnané	přehnaný	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
na	na	k7c4
kognitivní	kognitivní	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
rozhodovatele	rozhodovatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodování	rozhodování	k1gNnSc1
je	být	k5eAaImIp3nS
determinované	determinovaný	k2eAgInPc4d1
předpoklady	předpoklad	k1gInPc4
subjektu	subjekt	k1gInSc2
rozhodování	rozhodování	k1gNnSc4
–	–	k?
schopnosti	schopnost	k1gFnSc2
<g/>
,	,	kIx,
vědomosti	vědomost	k1gFnSc2
<g/>
,	,	kIx,
osobními	osobní	k2eAgInPc7d1
cíli	cíl	k1gInPc7
a	a	k8xC
zájmy	zájem	k1gInPc7
<g/>
,	,	kIx,
okamžitým	okamžitý	k2eAgInSc7d1
stavem	stav	k1gInSc7
–	–	k?
psychologickým	psychologický	k2eAgNnSc7d1
rozpoložením	rozpoložení	k1gNnSc7
<g/>
,	,	kIx,
náladou	nálada	k1gFnSc7
a	a	k8xC
objektivními	objektivní	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
materiální	materiální	k2eAgFnSc2d1
a	a	k8xC
nemateriální	materiální	k2eNgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oboru	obor	k1gInSc6
psychologie	psychologie	k1gFnSc2
byl	být	k5eAaImAgInS
37	#num#	k4
<g/>
.	.	kIx.
nejcitovanějším	citovaný	k2eAgMnSc7d3
autorem	autor	k1gMnSc7
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Herbert	Herbert	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Simon	Simon	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
ve	v	k7c6
městě	město	k1gNnSc6
Milwaukee	Milwauke	k1gFnSc2
ve	v	k7c6
státě	stát	k1gInSc6
Wisconsin	Wisconsina	k1gFnPc2
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1916	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
Simon	Simon	k1gMnSc1
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
-	-	kIx~
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
židovský	židovský	k2eAgMnSc1d1
elektrotechnik	elektrotechnik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
přišel	přijít	k5eAaPmAgMnS
z	z	k7c2
Německa	Německo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1903	#num#	k4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
získal	získat	k5eAaPmAgMnS
inženýrský	inženýrský	k2eAgInSc4d1
titul	titul	k1gInSc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
Technische	Technisch	k1gFnSc2
Hochschule	Hochschule	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
matka	matka	k1gFnSc1
<g/>
,	,	kIx,
Edna	Edna	k1gFnSc1
Marguerite	Marguerit	k1gInSc5
Merkelová	Merkelová	k1gFnSc1
(	(	kIx(
<g/>
1888	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
uznávanou	uznávaný	k2eAgFnSc7d1
pianistkou	pianistka	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
předkové	předek	k1gMnPc1
pocházeli	pocházet	k5eAaImAgMnP
z	z	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
Kolína	Kolín	k1gInSc2
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
i	i	k9
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
rodiny	rodina	k1gFnSc2
židovského	židovský	k2eAgInSc2d1
<g/>
,	,	kIx,
luteránského	luteránský	k2eAgInSc2d1
a	a	k8xC
katolického	katolický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Simon	Simon	k1gMnSc1
navštěvoval	navštěvovat	k5eAaImAgMnS
veřejné	veřejný	k2eAgFnPc4d1
školy	škola	k1gFnPc4
v	v	k7c6
Milwaukee	Milwaukee	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
zajímat	zajímat	k5eAaImF
o	o	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
prosadil	prosadit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
ateista	ateista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
dětí	dítě	k1gFnPc2
ho	on	k3xPp3gMnSc4
jeho	jeho	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
seznámila	seznámit	k5eAaPmAgFnS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
lidské	lidský	k2eAgNnSc4d1
chování	chování	k1gNnSc4
lze	lze	k6eAd1
studovat	studovat	k5eAaImF
vědecky	vědecky	k6eAd1
<g/>
;	;	kIx,
jedním	jeden	k4xCgInSc7
z	z	k7c2
jeho	jeho	k3xOp3gInPc2
prvních	první	k4xOgInPc2
vlivů	vliv	k1gInPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
jeho	jeho	k3xOp3gFnSc2
matky	matka	k1gFnSc2
Harold	Harold	k1gMnSc1
Merkel	Merkel	k1gMnSc1
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
-	-	kIx~
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
studoval	studovat	k5eAaImAgMnS
ekonomii	ekonomie	k1gFnSc4
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Madison	Madisona	k1gFnPc2
ve	v	k7c4
Wisconsinu	Wisconsina	k1gFnSc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Johna	John	k1gMnSc2
R.	R.	kA
Commonse	Commons	k1gMnSc2
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
Haroldových	Haroldův	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
o	o	k7c4
ekonomii	ekonomie	k1gFnSc4
a	a	k8xC
psychologii	psychologie	k1gFnSc4
objevil	objevit	k5eAaPmAgInS
sociální	sociální	k2eAgFnSc4d1
vědy	věda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gInPc4
nejranější	raný	k2eAgInPc4d3
vlivy	vliv	k1gInPc4
Simon	Simon	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
Normana	Norman	k1gMnSc4
Angella	Angell	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
knihu	kniha	k1gFnSc4
Velká	velký	k2eAgFnSc1d1
iluze	iluze	k1gFnSc1
a	a	k8xC
Henryho	Henry	k1gMnSc2
George	Georg	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
knihu	kniha	k1gFnSc4
Pokrok	pokrok	k1gInSc1
a	a	k8xC
chudoba	chudoba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
Simon	Simon	k1gMnSc1
vstoupil	vstoupit	k5eAaPmAgMnS
na	na	k7c4
univerzitu	univerzita	k1gFnSc4
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
studovat	studovat	k5eAaImF
společenské	společenský	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
a	a	k8xC
matematiku	matematika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simon	Simon	k1gMnSc1
se	se	k3xPyFc4
zajímal	zajímat	k5eAaImAgMnS
o	o	k7c4
studium	studium	k1gNnSc4
biologie	biologie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
„	„	k?
<g/>
barvosleposti	barvoslepost	k1gFnSc3
a	a	k8xC
nešikovnosti	nešikovnost	k1gFnSc3
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
<g/>
“	“	k?
se	se	k3xPyFc4
tomuto	tento	k3xDgInSc3
oboru	obor	k1gInSc3
nechtěl	chtít	k5eNaImAgMnS
věnovat	věnovat	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
raném	raný	k2eAgInSc6d1
věku	věk	k1gInSc6
se	se	k3xPyFc4
Simon	Simon	k1gMnSc1
dozvěděl	dozvědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
barvoslepý	barvoslepý	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
objevil	objevit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vnější	vnější	k2eAgInSc1d1
svět	svět	k1gInSc1
není	být	k5eNaImIp3nS
stejný	stejný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
svět	svět	k1gInSc1
vnímaný	vnímaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
studia	studio	k1gNnSc2
se	se	k3xPyFc4
Simon	Simon	k1gMnSc1
soustředil	soustředit	k5eAaPmAgMnS
na	na	k7c4
politologii	politologie	k1gFnSc4
a	a	k8xC
ekonomii	ekonomie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simonovým	Simonův	k2eAgInSc7d1
nejdůležitějším	důležitý	k2eAgInSc7d3
mentorem	mentor	k1gInSc7
byl	být	k5eAaImAgMnS
matematický	matematický	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
Henry	Henry	k1gMnSc1
Schultz	Schultz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simon	Simon	k1gMnSc1
získal	získat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
titul	titul	k1gInSc4
Bc.	Bc.	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
a	a	k8xC
titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
v	v	k7c6
roce	rok	k1gInSc6
1943	#num#	k4
v	v	k7c6
oboru	obor	k1gInSc6
politologie	politologie	k1gFnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
studoval	studovat	k5eAaImAgMnS
u	u	k7c2
Harolda	Harold	k1gMnSc2
Lasswella	Lasswell	k1gMnSc2
<g/>
,	,	kIx,
Nicolase	Nicolasa	k1gFnSc3
Rashevského	Rashevský	k2eAgMnSc2d1
<g/>
,	,	kIx,
Rudolfa	Rudolf	k1gMnSc2
Carnapa	Carnap	k1gMnSc2
<g/>
,	,	kIx,
Henryho	Henry	k1gMnSc2
Schultze	Schultze	k1gFnSc2
a	a	k8xC
Charlese	Charles	k1gMnSc2
Edwarda	Edward	k1gMnSc2
Merriama	Merriam	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
se	se	k3xPyFc4
Simon	Simon	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Dorotheu	Dorotheus	k1gInSc2
Pye	Pye	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
manželství	manželství	k1gNnSc1
trvalo	trvat	k5eAaImAgNnS
63	#num#	k4
let	léto	k1gNnPc2
až	až	k9
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2001	#num#	k4
podstoupil	podstoupit	k5eAaPmAgMnS
Simon	Simon	k1gMnSc1
operaci	operace	k1gFnSc4
v	v	k7c6
UPMC	UPMC	kA
Presbyterian	Presbyterian	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mu	on	k3xPp3gMnSc3
odstranili	odstranit	k5eAaPmAgMnP
rakovinový	rakovinový	k2eAgInSc4d1
nádor	nádor	k1gInSc4
v	v	k7c6
břiše	břicho	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
operace	operace	k1gFnSc1
byla	být	k5eAaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
později	pozdě	k6eAd2
podlehl	podlehnout	k5eAaPmAgMnS
komplikacím	komplikace	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
následovaly	následovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
Katherine	Katherin	k1gInSc5
<g/>
,	,	kIx,
Petera	Peter	k1gMnSc2
a	a	k8xC
Barbaru	Barbara	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
měl	mít	k5eAaImAgMnS
velký	velký	k2eAgInSc4d1
zájem	zájem	k1gInSc4
o	o	k7c4
umění	umění	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
byl	být	k5eAaImAgMnS
klavíristou	klavírista	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
přítelem	přítel	k1gMnSc7
Roberta	Robert	k1gMnSc2
Leppera	Lepper	k1gMnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Richarda	Richard	k1gMnSc2
Rappaporta	Rappaport	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
horlivým	horlivý	k2eAgMnSc7d1
horolezcem	horolezec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
a	a	k8xC
výzkum	výzkum	k1gInSc1
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1942	#num#	k4
až	až	k9
1949	#num#	k4
působil	působit	k5eAaImAgInS
jako	jako	k9
profesor	profesor	k1gMnSc1
politologie	politologie	k1gFnSc2
a	a	k8xC
pracoval	pracovat	k5eAaImAgInS
také	také	k9
jako	jako	k9
předseda	předseda	k1gMnSc1
katedry	katedra	k1gFnSc2
na	na	k7c6
Illinois	Illinois	k1gFnSc6
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgMnS
účastnit	účastnit	k5eAaImF
seminářů	seminář	k1gInPc2
pořádaných	pořádaný	k2eAgInPc2d1
pracovníky	pracovník	k1gMnPc7
Cowlesovy	Cowlesův	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byli	být	k5eAaImAgMnP
Trygve	Trygev	k1gFnPc4
Haavelmo	Haavelma	k1gFnSc5
<g/>
,	,	kIx,
Jacob	Jacoba	k1gFnPc2
Marschak	Marschak	k1gInSc1
a	a	k8xC
Tjalling	Tjalling	k1gInSc1
Koopmans	Koopmansa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
tak	tak	k9
hloubkové	hloubkový	k2eAgNnSc4d1
studium	studium	k1gNnSc4
ekonomie	ekonomie	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
institucionalizmu	institucionalizmus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
působil	působit	k5eAaImAgMnS
Simon	Simon	k1gMnSc1
na	na	k7c6
fakultě	fakulta	k1gFnSc6
Carnegie	Carnegie	k1gFnSc2
Mellon	Mellon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
se	se	k3xPyFc4
Simon	Simon	k1gMnSc1
stal	stát	k5eAaPmAgMnS
předsedou	předseda	k1gMnSc7
katedry	katedra	k1gFnSc2
průmyslového	průmyslový	k2eAgInSc2d1
managementu	management	k1gInSc2
na	na	k7c4
Carnegie	Carnegie	k1gFnPc4
Tech	Tech	k?
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Carnegie	Carnegie	k1gFnSc1
Mellon	Mellon	k1gInSc4
University	universita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
také	také	k9
učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
psychologii	psychologie	k1gFnSc4
a	a	k8xC
informatiku	informatika	k1gFnSc4
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Rozhodování	rozhodování	k1gNnSc1
(	(	kIx(
<g/>
desicion	desicion	k1gInSc1
-	-	kIx~
making	making	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kniha	kniha	k1gFnSc1
Administrative	Administrativ	k1gInSc5
Behavior	Behavior	k1gInSc4
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
publikována	publikován	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
a	a	k8xC
aktualizovaná	aktualizovaný	k2eAgFnSc1d1
v	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
Simonově	Simonův	k2eAgFnSc6d1
disertační	disertační	k2eAgFnSc6d1
práci	práce	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
základ	základ	k1gInSc4
pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
celoživotní	celoživotní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středobodem	středobod	k1gInSc7
této	tento	k3xDgFnSc2
knihy	kniha	k1gFnSc2
jsou	být	k5eAaImIp3nP
behaviorální	behaviorální	k2eAgInPc1d1
a	a	k8xC
kognitivní	kognitivní	k2eAgInPc1d1
procesy	proces	k1gInPc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
činí	činit	k5eAaImIp3nP
racionální	racionální	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jeho	jeho	k3xOp3gFnSc2
definice	definice	k1gFnSc2
by	by	kYmCp3nS
operativní	operativní	k2eAgNnSc1d1
rozhodnutí	rozhodnutí	k1gNnSc1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
správné	správný	k2eAgNnSc1d1
a	a	k8xC
účinné	účinný	k2eAgNnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
provádění	provádění	k1gNnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
praktické	praktický	k2eAgInPc4d1
pomocí	pomocí	k7c2
souboru	soubor	k1gInSc2
koordinovaných	koordinovaný	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
uznal	uznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
teorie	teorie	k1gFnSc1
správy	správa	k1gFnSc2
je	být	k5eAaImIp3nS
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
teorií	teorie	k1gFnPc2
rozhodování	rozhodování	k1gNnSc2
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
jako	jako	k9
taková	takový	k3xDgFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
založena	založit	k5eAaPmNgFnS
jak	jak	k6eAd1
na	na	k7c4
ekonomii	ekonomie	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
na	na	k7c6
psychologii	psychologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
[	[	kIx(
<g/>
Kdyby	kdyby	k9
<g/>
]	]	kIx)
neexistovaly	existovat	k5eNaImAgFnP
žádné	žádný	k3yNgFnPc1
limity	limita	k1gFnPc1
lidské	lidský	k2eAgFnSc2d1
racionality	racionalita	k1gFnSc2
<g/>
,	,	kIx,
administrativní	administrativní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
neúrodná	úrodný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládala	skládat	k5eAaImAgNnP
by	by	kYmCp3nP
se	se	k3xPyFc4
z	z	k7c2
jediného	jediný	k2eAgNnSc2d1
pravidla	pravidlo	k1gNnSc2
<g/>
:	:	kIx,
Vždy	vždy	k6eAd1
si	se	k3xPyFc3
vyberte	vybrat	k5eAaPmRp2nP
tu	tu	k6eAd1
alternativu	alternativa	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
nejúplnějšímu	úplný	k2eAgNnSc3d3
dosažení	dosažení	k1gNnSc3
vašich	váš	k3xOp2gInPc2
cílů	cíl	k1gInPc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Simon	Simon	k1gMnSc1
definoval	definovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
úkolem	úkol	k1gInSc7
racionálního	racionální	k2eAgNnSc2d1
rozhodování	rozhodování	k1gNnSc2
je	být	k5eAaImIp3nS
vybrat	vybrat	k5eAaPmF
alternativu	alternativa	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
preferovanějšímu	preferovaný	k2eAgInSc3d2
souboru	soubor	k1gInSc3
všech	všecek	k3xTgInPc2
možných	možný	k2eAgInPc2d1
důsledků	důsledek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správnost	správnost	k1gFnSc1
správních	správní	k2eAgNnPc2d1
rozhodnutí	rozhodnutí	k1gNnPc2
byla	být	k5eAaImAgFnS
tedy	tedy	k9
měřena	měřit	k5eAaImNgFnS
pomocí	pomoc	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
Přiměřenost	přiměřenost	k1gFnSc1
dosažení	dosažení	k1gNnSc2
požadovaného	požadovaný	k2eAgInSc2d1
cíle	cíl	k1gInSc2
</s>
<s>
Účinnost	účinnost	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
byl	být	k5eAaImAgMnS
získán	získán	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
</s>
<s>
Úkol	úkol	k1gInSc1
volby	volba	k1gFnSc2
byl	být	k5eAaImAgInS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
tří	tři	k4xCgInPc2
požadovaných	požadovaný	k2eAgInPc2d1
kroků	krok	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Identifikace	identifikace	k1gFnSc1
a	a	k8xC
seznam	seznam	k1gInSc1
všech	všecek	k3xTgFnPc2
alternativ	alternativa	k1gFnPc2
</s>
<s>
Stanovení	stanovení	k1gNnSc1
všech	všecek	k3xTgInPc2
důsledků	důsledek	k1gInPc2
vyplývajících	vyplývající	k2eAgInPc2d1
z	z	k7c2
každé	každý	k3xTgFnSc2
z	z	k7c2
alternativ	alternativa	k1gFnPc2
<g/>
;	;	kIx,
</s>
<s>
Porovnání	porovnání	k1gNnSc1
přesnosti	přesnost	k1gFnSc2
a	a	k8xC
účinnosti	účinnost	k1gFnSc2
každé	každý	k3xTgFnSc6
z	z	k7c2
těchto	tento	k3xDgFnPc2
sad	sada	k1gFnPc2
důsledků	důsledek	k1gInPc2
</s>
<s>
Kterýkoli	kterýkoli	k3yIgMnSc1
jednotlivec	jednotlivec	k1gMnSc1
nebo	nebo	k8xC
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
implementovat	implementovat	k5eAaImF
tento	tento	k3xDgInSc4
model	model	k1gInSc4
v	v	k7c6
reálné	reálný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
by	by	kYmCp3nP
nemohli	moct	k5eNaImAgMnP
splnit	splnit	k5eAaPmF
tyto	tento	k3xDgInPc4
tři	tři	k4xCgInPc4
požadavky	požadavek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simon	Simon	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
znalost	znalost	k1gFnSc1
všech	všecek	k3xTgFnPc2
alternativ	alternativa	k1gFnPc2
nebo	nebo	k8xC
všech	všecek	k3xTgInPc2
důsledků	důsledek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
z	z	k7c2
každé	každý	k3xTgFnSc2
alternativy	alternativa	k1gFnSc2
vyplývají	vyplývat	k5eAaImIp3nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
realistických	realistický	k2eAgFnPc6d1
případech	případ	k1gInPc6
nemožná	možný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
následoval	následovat	k5eAaImAgMnS
Chestera	Chester	k1gMnSc4
Barnarda	Barnard	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zdůraznil	zdůraznit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
rozhodnutí	rozhodnutí	k1gNnSc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
jednotlivec	jednotlivec	k1gMnSc1
učiní	učinit	k5eAaPmIp3nS,k5eAaImIp3nS
jako	jako	k9
člen	člen	k1gMnSc1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
zcela	zcela	k6eAd1
odlišná	odlišný	k2eAgFnSc1d1
od	od	k7c2
jeho	jeho	k3xOp3gNnPc2
osobních	osobní	k2eAgNnPc2d1
rozhodnutí	rozhodnutí	k1gNnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
určit	určit	k5eAaPmF
osobní	osobní	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
jednotlivec	jednotlivec	k1gMnSc1
připojí	připojit	k5eAaPmIp3nS
ke	k	k7c3
konkrétní	konkrétní	k2eAgFnSc3d1
organizaci	organizace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
člen	člen	k1gMnSc1
organizace	organizace	k1gFnSc2
však	však	k9
tento	tento	k3xDgMnSc1
jedinec	jedinec	k1gMnSc1
rozhoduje	rozhodovat	k5eAaImIp3nS
nikoli	nikoli	k9
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
osobním	osobní	k2eAgFnPc3d1
potřebám	potřeba	k1gFnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
neosobním	osobní	k2eNgInSc6d1
smyslu	smysl	k1gInSc6
jako	jako	k9
součást	součást	k1gFnSc4
organizačního	organizační	k2eAgInSc2d1
záměru	záměr	k1gInSc2
<g/>
,	,	kIx,
účelu	účel	k1gInSc2
a	a	k8xC
účinku	účinek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizační	organizační	k2eAgFnPc4d1
pobídky	pobídka	k1gFnPc4
<g/>
,	,	kIx,
odměny	odměna	k1gFnPc4
a	a	k8xC
sankce	sankce	k1gFnPc4
jsou	být	k5eAaImIp3nP
navrženy	navrhnout	k5eAaPmNgFnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tuto	tento	k3xDgFnSc4
identifikaci	identifikace	k1gFnSc4
formovaly	formovat	k5eAaImAgInP
<g/>
,	,	kIx,
posilovaly	posilovat	k5eAaImAgInP
a	a	k8xC
udržovaly	udržovat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
viděl	vidět	k5eAaImAgMnS
dva	dva	k4xCgInPc4
univerzální	univerzální	k2eAgInPc4d1
prvky	prvek	k1gInPc4
lidského	lidský	k2eAgNnSc2d1
sociálního	sociální	k2eAgNnSc2d1
chování	chování	k1gNnSc2
jako	jako	k8xC,k8xS
klíčové	klíčový	k2eAgFnSc2d1
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
možnosti	možnost	k1gFnSc2
organizačního	organizační	k2eAgNnSc2d1
chování	chování	k1gNnSc2
u	u	k7c2
lidských	lidský	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
<g/>
:	:	kIx,
autorita	autorita	k1gFnSc1
(	(	kIx(
<g/>
řešena	řešen	k2eAgFnSc1d1
v	v	k7c6
kapitole	kapitola	k1gFnSc6
VII	VII	kA
-	-	kIx~
Role	role	k1gFnSc1
autority	autorita	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
loajalita	loajalita	k1gFnSc1
a	a	k8xC
identifikace	identifikace	k1gFnSc1
(	(	kIx(
<g/>
řešeno	řešit	k5eAaImNgNnS
v	v	k7c6
kapitole	kapitola	k1gFnSc6
X	X	kA
<g/>
:	:	kIx,
loajalita	loajalita	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
organizační	organizační	k2eAgFnSc1d1
identifikace	identifikace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Autorita	autorita	k1gFnSc1
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
prostudovaný	prostudovaný	k2eAgInSc1d1
primární	primární	k2eAgInSc1d1
znak	znak	k1gInSc1
organizačního	organizační	k2eAgNnSc2d1
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
organizačním	organizační	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
jasně	jasně	k6eAd1
definován	definovat	k5eAaBmNgInS
jako	jako	k8xC,k8xS
schopnost	schopnost	k1gFnSc1
a	a	k8xC
právo	právo	k1gNnSc1
jedince	jedinec	k1gMnSc2
vyšší	vysoký	k2eAgFnSc2d2
hodnosti	hodnost	k1gFnSc2
řídit	řídit	k5eAaImF
rozhodnutí	rozhodnutí	k1gNnPc4
jedince	jedinko	k6eAd1
nižší	nízký	k2eAgFnPc4d2
hodnosti	hodnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Loajalitu	loajalita	k1gFnSc4
definoval	definovat	k5eAaBmAgInS
Simon	Simon	k1gMnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
proces	proces	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
jednotlivec	jednotlivec	k1gMnSc1
nahrazuje	nahrazovat	k5eAaImIp3nS
organizační	organizační	k2eAgInPc4d1
cíle	cíl	k1gInPc4
svými	svůj	k3xOyFgInPc7
vlastními	vlastní	k2eAgInPc7d1
cíli	cíl	k1gInPc7
jako	jako	k8xS,k8xC
hodnotové	hodnotový	k2eAgInPc4d1
indexy	index	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
určují	určovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gNnPc4
organizační	organizační	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
vyhodnocení	vyhodnocení	k1gNnSc4
alternativních	alternativní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
z	z	k7c2
hlediska	hledisko	k1gNnSc2
jejich	jejich	k3xOp3gInPc2
důsledků	důsledek	k1gInPc2
pro	pro	k7c4
skupinu	skupina	k1gFnSc4
<g/>
,	,	kIx,
nikoli	nikoli	k9
pouze	pouze	k6eAd1
pro	pro	k7c4
sebe	sebe	k3xPyFc4
nebo	nebo	k8xC
rodinu	rodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Rozhodování	rozhodování	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
složitou	složitý	k2eAgFnSc7d1
příměsí	příměs	k1gFnSc7
faktů	fakt	k1gInPc2
a	a	k8xC
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnSc1
o	o	k7c6
faktech	fakt	k1gInPc6
<g/>
,	,	kIx,
zejména	zejména	k9
empiricky	empiricky	k6eAd1
prokázaná	prokázaný	k2eAgNnPc1d1
fakta	faktum	k1gNnPc1
nebo	nebo	k8xC
fakta	faktum	k1gNnPc1
odvozená	odvozený	k2eAgNnPc1d1
ze	z	k7c2
specializovaných	specializovaný	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
při	při	k7c6
výkonu	výkon	k1gInSc6
autority	autorita	k1gFnSc2
přenášejí	přenášet	k5eAaImIp3nP
snadněji	snadno	k6eAd2
než	než	k8xS
vyjádření	vyjádření	k1gNnSc4
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simon	Simon	k1gMnSc1
se	se	k3xPyFc4
primárně	primárně	k6eAd1
zajímá	zajímat	k5eAaImIp3nS
o	o	k7c4
identifikaci	identifikace	k1gFnSc4
jednotlivých	jednotlivý	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
s	s	k7c7
organizačními	organizační	k2eAgInPc7d1
cíli	cíl	k1gInPc7
a	a	k8xC
hodnotami	hodnota	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
kritizoval	kritizovat	k5eAaImAgMnS
elementární	elementární	k2eAgNnSc4d1
chápání	chápání	k1gNnSc4
rozhodování	rozhodování	k1gNnSc2
tradiční	tradiční	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
a	a	k8xC
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
rychlé	rychlý	k2eAgInPc4d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vytvořil	vytvořit	k5eAaPmAgInS
idealistick	idealistick	k1gInSc1
<g/>
,	,	kIx,
nerealistický	realistický	k2eNgInSc1d1
obraz	obraz	k1gInSc1
rozhodovacího	rozhodovací	k2eAgInSc2d1
procesu	proces	k1gInSc2
a	a	k8xC
poté	poté	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
tohoto	tento	k3xDgInSc2
nerealistického	realistický	k2eNgInSc2d1
obrazu	obraz	k1gInSc2
předepisoval	předepisovat	k5eAaImAgMnS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Sociologie	sociologie	k1gFnSc1
a	a	k8xC
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Simonovi	Simon	k1gMnSc3
byla	být	k5eAaImAgFnS
připsána	připsat	k5eAaPmNgFnS
revoluční	revoluční	k2eAgFnSc1d1
změna	změna	k1gFnSc1
v	v	k7c6
mikroekonomii	mikroekonomie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
odpovědný	odpovědný	k2eAgMnSc1d1
za	za	k7c4
koncept	koncept	k1gInSc4
organizačního	organizační	k2eAgNnSc2d1
rozhodování	rozhodování	k1gNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
znám	znám	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k6eAd1
prvním	první	k4xOgInPc3
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
diskutoval	diskutovat	k5eAaImAgMnS
o	o	k7c6
tomto	tento	k3xDgInSc6
konceptu	koncept	k1gInSc6
z	z	k7c2
hlediska	hledisko	k1gNnSc2
nejistoty	nejistota	k1gFnSc2
<g/>
;	;	kIx,
tj.	tj.	kA
je	být	k5eAaImIp3nS
nemožné	možný	k2eNgNnSc1d1
mít	mít	k5eAaImF
v	v	k7c6
daném	daný	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
dokonalé	dokonalý	k2eAgFnSc2d1
a	a	k8xC
úplné	úplný	k2eAgFnSc2d1
informace	informace	k1gFnSc2
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
tato	tento	k3xDgFnSc1
představa	představa	k1gFnSc1
nebyla	být	k5eNaImAgFnS
úplně	úplně	k6eAd1
nová	nový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
původce	původce	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgNnP
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
udělena	udělen	k2eAgFnSc1d1
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
dále	daleko	k6eAd2
zdůraznil	zdůraznit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
psychologové	psycholog	k1gMnPc1
se	se	k3xPyFc4
dovolávají	dovolávat	k5eAaImIp3nP
„	„	k?
<g/>
procedurální	procedurální	k2eAgInSc1d1
<g/>
“	“	k?
definice	definice	k1gFnSc1
racionality	racionalita	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ekonomové	ekonom	k1gMnPc1
používají	používat	k5eAaImIp3nP
„	„	k?
<g/>
věcnou	věcný	k2eAgFnSc4d1
<g/>
“	“	k?
definici	definice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gustavos	Gustavos	k1gMnSc1
Barros	Barrosa	k1gFnPc2
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
koncept	koncept	k1gInSc1
procedurální	procedurální	k2eAgFnSc2d1
racionality	racionalita	k1gFnSc2
nemá	mít	k5eNaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
ekonomiky	ekonomika	k1gFnSc2
významnou	významný	k2eAgFnSc4d1
přítomnost	přítomnost	k1gFnSc4
a	a	k8xC
nikdy	nikdy	k6eAd1
neměl	mít	k5eNaImAgMnS
takovou	takový	k3xDgFnSc4
váhu	váha	k1gFnSc4
jako	jako	k8xS,k8xC
koncept	koncept	k1gInSc4
omezené	omezený	k2eAgFnSc2d1
racionality	racionalita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
v	v	k7c6
dřívějším	dřívější	k2eAgInSc6d1
článku	článek	k1gInSc6
Manjul	Manjul	k1gInSc1
Bhargava	Bhargava	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
poukázal	poukázat	k5eAaPmAgMnS
na	na	k7c4
důležitost	důležitost	k1gFnSc4
Simonových	Simonových	k2eAgInPc2d1
argumentů	argument	k1gInPc2
a	a	k8xC
zdůraznil	zdůraznit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
ekonometrických	ekonometrický	k2eAgFnPc6d1
analýzách	analýza	k1gFnPc6
údajů	údaj	k1gInPc2
o	o	k7c6
zdraví	zdraví	k1gNnSc6
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
aplikací	aplikace	k1gFnPc2
„	„	k?
<g/>
procedurální	procedurální	k2eAgFnSc2d1
<g/>
“	“	k?
definice	definice	k1gFnSc2
racionality	racionalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomové	ekonom	k1gMnPc1
by	by	kYmCp3nP
měli	mít	k5eAaImAgMnP
zejména	zejména	k9
využívat	využívat	k5eAaPmF,k5eAaImF
„	„	k?
<g/>
pomocné	pomocný	k2eAgInPc1d1
předpoklady	předpoklad	k1gInPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
odrážejí	odrážet	k5eAaImIp3nP
znalosti	znalost	k1gFnPc4
v	v	k7c6
příslušných	příslušný	k2eAgInPc6d1
biomedicínských	biomedicínský	k2eAgInPc6d1
oborech	obor	k1gInPc6
<g/>
,	,	kIx,
a	a	k8xC
řídit	řídit	k5eAaImF
se	s	k7c7
specifikací	specifikace	k1gFnSc7
ekonometrických	ekonometrický	k2eAgInPc2d1
modelů	model	k1gInPc2
výsledků	výsledek	k1gInPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
zdraví	zdraví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
byl	být	k5eAaImAgMnS
také	také	k9
známý	známý	k2eAgInSc1d1
svým	svůj	k3xOyFgInSc7
výzkumem	výzkum	k1gInSc7
průmyslové	průmyslový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vnitřní	vnitřní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
firem	firma	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
externí	externí	k2eAgNnPc4d1
obchodní	obchodní	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
neodpovídají	odpovídat	k5eNaImIp3nP
neoklasickým	neoklasický	k2eAgFnPc3d1
teoriím	teorie	k1gFnPc3
„	„	k?
<g/>
racionálního	racionální	k2eAgNnSc2d1
<g/>
“	“	k?
rozhodování	rozhodování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simon	Simon	k1gMnSc1
během	během	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
na	na	k7c4
toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
mnoho	mnoho	k4c1
článků	článek	k1gInPc2
<g/>
,	,	kIx,
zaměřil	zaměřit	k5eAaPmAgInS
se	se	k3xPyFc4
hlavně	hlavně	k9
na	na	k7c4
otázku	otázka	k1gFnSc4
rozhodování	rozhodování	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
chování	chování	k1gNnSc2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
nazval	nazvat	k5eAaBmAgInS,k5eAaPmAgInS
„	„	k?
omezenou	omezený	k2eAgFnSc7d1
racionalitou	racionalita	k1gFnSc7
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nejlepším	dobrý	k2eAgInSc7d3
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
studovat	studovat	k5eAaImF
tyto	tento	k3xDgFnPc4
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
počítačové	počítačový	k2eAgFnPc4d1
simulace	simulace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
takový	takový	k3xDgMnSc1
<g/>
,	,	kIx,
on	on	k3xPp3gMnSc1
vyvinul	vyvinout	k5eAaPmAgInS
zájem	zájem	k1gInSc4
o	o	k7c4
informatiku	informatika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simonovy	Simonův	k2eAgInPc4d1
hlavní	hlavní	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
v	v	k7c6
počítačové	počítačový	k2eAgFnSc6d1
vědě	věda	k1gFnSc6
byly	být	k5eAaImAgInP
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
<g/>
,	,	kIx,
interakce	interakce	k1gFnPc1
člověka	člověk	k1gMnSc2
s	s	k7c7
počítačem	počítač	k1gInSc7
<g/>
,	,	kIx,
principy	princip	k1gInPc4
organizace	organizace	k1gFnSc2
lidí	člověk	k1gMnPc2
a	a	k8xC
strojů	stroj	k1gInPc2
jako	jako	k8xS,k8xC
systémů	systém	k1gInPc2
zpracování	zpracování	k1gNnSc2
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
využití	využití	k1gNnSc4
počítačů	počítač	k1gMnPc2
ke	k	k7c3
studiu	studio	k1gNnSc3
(	(	kIx(
<g/>
modelování	modelování	k1gNnSc1
<g/>
)	)	kIx)
filozofických	filozofický	k2eAgInPc2d1
problémů	problém	k1gInPc2
povahy	povaha	k1gFnSc2
inteligence	inteligence	k1gFnSc2
a	a	k8xC
epistemologie	epistemologie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
sociální	sociální	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
počítačové	počítačový	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
mládí	mládí	k1gNnSc6
se	se	k3xPyFc4
Simon	Simon	k1gMnSc1
začal	začít	k5eAaPmAgMnS
zajímat	zajímat	k5eAaImF
o	o	k7c4
ekonomiku	ekonomika	k1gFnSc4
půdy	půda	k1gFnSc2
a	a	k8xC
georgismus	georgismus	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
známý	známý	k2eAgInSc4d1
nápad	nápad	k1gInSc4
jako	jako	k9
„	„	k?
<g/>
jednotná	jednotný	k2eAgFnSc1d1
daň	daň	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účelem	účel	k1gInSc7
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
přerozdělit	přerozdělit	k5eAaPmF
nezasloužené	zasloužený	k2eNgNnSc4d1
ekonomické	ekonomický	k2eAgNnSc4d1
nájemné	nájemné	k1gNnSc4
veřejnosti	veřejnost	k1gFnSc2
a	a	k8xC
zlepšit	zlepšit	k5eAaPmF
využívání	využívání	k1gNnSc4
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
Simon	Simon	k1gMnSc1
tyto	tento	k3xDgFnPc4
myšlenky	myšlenka	k1gFnPc4
stále	stále	k6eAd1
udržoval	udržovat	k5eAaImAgMnS
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
daň	daň	k1gFnSc4
z	z	k7c2
nemovitých	movitý	k2eNgFnPc2d1
věcí	věc	k1gFnPc2
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
nahradit	nahradit	k5eAaPmF
daně	daň	k1gFnPc4
ze	z	k7c2
mzdy	mzda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Část	část	k1gFnSc1
Simonova	Simonův	k2eAgInSc2d1
ekonomického	ekonomický	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
směřovala	směřovat	k5eAaImAgFnS
k	k	k7c3
porozumění	porozumění	k1gNnSc3
technologickým	technologický	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
obecně	obecně	k6eAd1
a	a	k8xC
zejména	zejména	k9
revoluci	revoluce	k1gFnSc4
zpracování	zpracování	k1gNnSc2
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
</s>
<s>
Simon	Simon	k1gMnSc1
byl	být	k5eAaImAgMnS
průkopníkem	průkopník	k1gMnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Allenem	Allen	k1gMnSc7
Newellem	Newell	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
počítačový	počítačový	k2eAgInSc1d1
program	program	k1gInSc1
Logic	Logice	k1gFnPc2
theory	theora	k1gFnSc2
machine	machinout	k5eAaPmIp3nS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
program	program	k1gInSc1
GPS	GPS	kA
(	(	kIx(
<g/>
General	General	k1gMnSc7
problem	probl	k1gMnSc7
solver	solver	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
programy	program	k1gInPc1
byly	být	k5eAaImAgInP
vyvinuty	vyvinout	k5eAaPmNgInP
pomocí	pomocí	k7c2
jazyka	jazyk	k1gInSc2
Information	Information	k1gInSc4
Processing	Processing	k1gInSc1
Language	language	k1gFnSc1
(	(	kIx(
<g/>
IPL	IPL	kA
<g/>
)	)	kIx)
vyvinutého	vyvinutý	k2eAgInSc2d1
Newellem	Newell	k1gMnSc7
<g/>
,	,	kIx,
Cliffem	Cliff	k1gMnSc7
Shawem	Shaw	k1gMnSc7
a	a	k8xC
Simonem	Simon	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
psycholog	psycholog	k1gMnSc1
Ulric	Ulric	k1gMnSc1
Neisser	Neisser	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zatímco	zatímco	k8xS
stroje	stroj	k1gInPc1
jsou	být	k5eAaImIp3nP
schopné	schopný	k2eAgFnPc1d1
replikovat	replikovat	k5eAaImF
chování	chování	k1gNnSc4
„	„	k?
<g/>
cold	cold	k1gInSc1
cognition	cognition	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
uvažování	uvažování	k1gNnSc1
<g/>
,	,	kIx,
plánování	plánování	k1gNnSc1
<g/>
,	,	kIx,
vnímání	vnímání	k1gNnSc1
a	a	k8xC
rozhodování	rozhodování	k1gNnSc1
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
nebudou	být	k5eNaImBp3nP
schopny	schopen	k2eAgFnPc1d1
replikovat	replikovat	k5eAaImF
chování	chování	k1gNnSc4
„	„	k?
<g/>
hot	hot	k0
cognition	cognition	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
bolest	bolest	k1gFnSc4
<g/>
,	,	kIx,
potěšení	potěšení	k1gNnSc4
<g/>
,	,	kIx,
touha	touha	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
emoce	emoce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simon	Simon	k1gMnSc1
reagoval	reagovat	k5eAaBmAgMnS
na	na	k7c4
Neisserovy	Neisserův	k2eAgInPc4d1
názory	názor	k1gInPc4
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
napsáním	napsání	k1gNnSc7
článku	článek	k1gInSc2
o	o	k7c6
emočním	emoční	k2eAgNnSc6d1
poznání	poznání	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
aktualizoval	aktualizovat	k5eAaBmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
a	a	k8xC
publikoval	publikovat	k5eAaBmAgMnS
v	v	k7c6
Psychological	Psychological	k1gFnSc6
Review	Review	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simonova	Simonův	k2eAgFnSc1d1
práce	práce	k1gFnSc1
na	na	k7c6
emočním	emoční	k2eAgNnSc6d1
poznávání	poznávání	k1gNnSc6
byla	být	k5eAaImAgFnS
výzkumnou	výzkumný	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
pro	pro	k7c4
umělou	umělý	k2eAgFnSc4d1
inteligenci	inteligence	k1gFnSc4
několik	několik	k4yIc4
let	léto	k1gNnPc2
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
ignorována	ignorovat	k5eAaImNgNnP
<g/>
,	,	kIx,
ale	ale	k8xC
následná	následný	k2eAgFnSc1d1
práce	práce	k1gFnSc1
na	na	k7c6
emocích	emoce	k1gFnPc6
Aarona	Aaron	k1gMnSc2
Slomana	Sloman	k1gMnSc2
a	a	k8xC
Rosalind	Rosalinda	k1gFnPc2
Picardové	Picardový	k2eAgFnSc2d1
pomohla	pomoct	k5eAaPmAgFnS
znovu	znovu	k6eAd1
zaměřit	zaměřit	k5eAaPmF
pozornost	pozornost	k1gFnSc4
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nakonec	nakonec	k6eAd1
velmi	velmi	k6eAd1
ovlivnila	ovlivnit	k5eAaPmAgFnS
toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
Allenem	Allen	k1gMnSc7
Newellem	Newell	k1gMnSc7
vyvinul	vyvinout	k5eAaPmAgMnS
Simon	Simon	k1gMnSc1
teorii	teorie	k1gFnSc4
pro	pro	k7c4
simulaci	simulace	k1gFnSc4
chování	chování	k1gNnSc2
při	při	k7c6
řešení	řešení	k1gNnSc6
lidských	lidský	k2eAgInPc2d1
problémů	problém	k1gInPc2
pomocí	pomocí	k7c2
produkčních	produkční	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
řešení	řešení	k1gNnSc2
lidských	lidský	k2eAgInPc2d1
problémů	problém	k1gInPc2
vyžadovala	vyžadovat	k5eAaImAgFnS
nové	nový	k2eAgInPc1d1
druhy	druh	k1gInPc1
lidských	lidský	k2eAgNnPc2d1
měření	měření	k1gNnPc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
Andersem	Anders	k1gMnSc7
Ericssonem	Ericsson	k1gMnSc7
vyvinul	vyvinout	k5eAaPmAgMnS
Simon	Simon	k1gMnSc1
experimentální	experimentální	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
analýzy	analýza	k1gFnSc2
verbálního	verbální	k2eAgInSc2d1
protokolu	protokol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simon	Simon	k1gMnSc1
se	se	k3xPyFc4
zajímal	zajímat	k5eAaImAgMnS
o	o	k7c4
roli	role	k1gFnSc4
znalostí	znalost	k1gFnPc2
v	v	k7c6
odborných	odborný	k2eAgFnPc6d1
znalostech	znalost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
odborníkem	odborník	k1gMnSc7
na	na	k7c4
dané	daný	k2eAgNnSc4d1
téma	téma	k1gNnSc4
vyžadovalo	vyžadovat	k5eAaImAgNnS
přibližně	přibližně	k6eAd1
desetileté	desetiletý	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
a	a	k8xC
on	on	k3xPp3gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
kolegové	kolega	k1gMnPc1
odhadli	odhadnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
odborné	odborný	k2eAgFnPc1d1
znalosti	znalost	k1gFnPc1
byly	být	k5eAaImAgFnP
výsledkem	výsledek	k1gInSc7
učení	učení	k1gNnSc2
zhruba	zhruba	k6eAd1
50	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Spolu	spolu	k6eAd1
s	s	k7c7
Allenem	Allen	k1gMnSc7
Newellem	Newell	k1gMnSc7
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgNnP
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
udělena	udělen	k2eAgFnSc1d1
Turingova	Turingův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
.	.	kIx.
„	„	k?
<g/>
Ve	v	k7c6
společném	společný	k2eAgNnSc6d1
vědeckém	vědecký	k2eAgNnSc6d1
úsilí	úsilí	k1gNnSc6
trvajícím	trvající	k2eAgMnSc6d1
déle	dlouho	k6eAd2
než	než	k8xS
dvacet	dvacet	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Cliffem	Cliff	k1gMnSc7
Shawem	Shaw	k1gMnSc7
ve	v	k7c6
společnosti	společnost	k1gFnSc6
RAND	rand	k1gInSc1
Corporation	Corporation	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
následně	následně	k6eAd1
s	s	k7c7
mnoha	mnoho	k4c7
zaměstnanci	zaměstnanec	k1gMnPc7
a	a	k8xC
studenty	student	k1gMnPc7
z	z	k7c2
Carnegie	Carnegie	k1gFnSc2
Mellon	Mellon	k1gInSc4
University	universita	k1gFnSc2
<g/>
,	,	kIx,
zásadním	zásadní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
přispěli	přispět	k5eAaPmAgMnP
k	k	k7c3
umělé	umělý	k2eAgFnSc3d1
inteligenci	inteligence	k1gFnSc3
a	a	k8xC
psychologii	psychologie	k1gFnSc3
lidského	lidský	k2eAgNnSc2d1
poznání	poznání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Psychologie	psychologie	k1gFnSc1
</s>
<s>
Simon	Simon	k1gMnSc1
se	se	k3xPyFc4
zajímal	zajímat	k5eAaImAgMnS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
učí	učit	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
Edwardem	Edward	k1gMnSc7
Feigenbaumem	Feigenbaum	k1gInSc7
vyvinul	vyvinout	k5eAaPmAgInS
teorii	teorie	k1gFnSc4
EPAM	EPAM	kA
(	(	kIx(
<g/>
Elementary	Elementara	k1gFnSc2
Perceiver	Perceiver	k1gMnSc1
and	and	k?
Memorizer	Memorizer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
teorií	teorie	k1gFnPc2
učení	učení	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
implementována	implementován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
počítačový	počítačový	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
EPAM	EPAM	kA
dokázal	dokázat	k5eAaPmAgMnS
vysvětlit	vysvětlit	k5eAaPmF
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
jevů	jev	k1gInPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
verbálního	verbální	k2eAgNnSc2d1
učení	učení	k1gNnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdější	pozdní	k2eAgFnSc1d2
verze	verze	k1gFnSc1
modelu	model	k1gInSc2
byly	být	k5eAaImAgInP
použity	použít	k5eAaPmNgInP
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
konceptů	koncept	k1gInPc2
a	a	k8xC
získávání	získávání	k1gNnSc6
odborných	odborný	k2eAgFnPc2d1
znalostí	znalost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Fernandem	Fernando	k1gNnSc7
Gobetem	Gobe	k1gNnSc7
rozšířil	rozšířit	k5eAaPmAgInS
teorii	teorie	k1gFnSc4
EPAM	EPAM	kA
do	do	k7c2
výpočetního	výpočetní	k2eAgInSc2d1
modelu	model	k1gInSc2
CHREST	CHREST	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Teorie	teorie	k1gFnSc1
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
jednoduché	jednoduchý	k2eAgInPc1d1
kousky	kousek	k1gInPc1
informací	informace	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nP
stavební	stavební	k2eAgInPc1d1
bloky	blok	k1gInPc1
schémat	schéma	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jsou	být	k5eAaImIp3nP
složitější	složitý	k2eAgFnPc1d2
struktury	struktura	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Během	během	k7c2
života	život	k1gInSc2
získal	získat	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
významných	významný	k2eAgNnPc2d1
ocenění	ocenění	k1gNnPc2
a	a	k8xC
vyznamenání	vyznamenání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
Americké	americký	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
umění	umění	k1gNnSc2
a	a	k8xC
věd	věda	k1gFnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
členem	člen	k1gMnSc7
ACM	ACM	kA
(	(	kIx(
<g/>
Associations	Associationsa	k1gFnPc2
for	forum	k1gNnPc2
Computing	Computing	k1gInSc1
Machinery	Machiner	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Významná	významný	k2eAgNnPc4d1
ocenění	ocenění	k1gNnPc4
</s>
<s>
1969	#num#	k4
-	-	kIx~
cena	cena	k1gFnSc1
APA	APA	kA
za	za	k7c4
významné	významný	k2eAgInPc4d1
vědecké	vědecký	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
v	v	k7c6
psychologii	psychologie	k1gFnSc6
</s>
<s>
1975	#num#	k4
-	-	kIx~
Turingova	Turingův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
významné	významný	k2eAgInPc4d1
příspěvky	příspěvek	k1gInPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
</s>
<s>
1978	#num#	k4
-	-	kIx~
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
</s>
<s>
1986	#num#	k4
-	-	kIx~
Národní	národní	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
za	za	k7c4
vědu	věda	k1gFnSc4
</s>
<s>
1993	#num#	k4
-	-	kIx~
cena	cena	k1gFnSc1
APA	APA	kA
za	za	k7c4
významné	významný	k2eAgInPc4d1
celoživotní	celoživotní	k2eAgInPc4d1
příspěvky	příspěvek	k1gInPc4
v	v	k7c6
psychologiii	psychologiie	k1gFnSc6
</s>
<s>
Vybrané	vybraný	k2eAgFnPc1d1
publikace	publikace	k1gFnPc1
</s>
<s>
Simon	Simon	k1gMnSc1
byl	být	k5eAaImAgMnS
autorem	autor	k1gMnSc7
27	#num#	k4
knih	kniha	k1gFnPc2
a	a	k8xC
stovek	stovka	k1gFnPc2
článků	článek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
byl	být	k5eAaImAgMnS
Simon	Simon	k1gMnSc1
nejvíce	nejvíce	k6eAd1,k6eAd3
citovanou	citovaný	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
a	a	k8xC
kognitivní	kognitivní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
ve	v	k7c6
službě	služba	k1gFnSc6
Google	Google	k1gNnSc2
Scholar	Scholara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
téměř	téměř	k6eAd1
tisíci	tisíc	k4xCgInPc7
vysoce	vysoce	k6eAd1
citovanými	citovaný	k2eAgFnPc7d1
publikacemi	publikace	k1gFnPc7
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejvlivnějších	vlivný	k2eAgMnPc2d3
sociálních	sociální	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Knihy	kniha	k1gFnPc1
</s>
<s>
1947	#num#	k4
-	-	kIx~
Administrative	Administrativ	k1gInSc5
Behavior	Behaviora	k1gFnPc2
<g/>
:	:	kIx,
A	a	k8xC
Study	stud	k1gInPc1
of	of	k?
Decision-Making	Decision-Making	k1gInSc1
Processes	Processes	k1gInSc1
in	in	k?
Administrative	Administrativ	k1gInSc5
Organization	Organization	k1gInSc4
–	–	k?
4	#num#	k4
<g/>
th	th	k?
ed	ed	k?
<g/>
.	.	kIx.
in	in	k?
1997	#num#	k4
<g/>
,	,	kIx,
The	The	k1gFnSc1
Free	Fre	k1gFnSc2
Press	Pressa	k1gFnPc2
</s>
<s>
1969	#num#	k4
-	-	kIx~
The	The	k1gMnSc1
Scinces	Scinces	k1gMnSc1
of	of	k?
the	the	k?
Artificial	Artificial	k1gMnSc1
-	-	kIx~
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Mass	Mass	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
st	st	kA
edition	edition	k1gInSc1
</s>
<s>
1972	#num#	k4
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
Allenem	Allen	k1gMnSc7
Newellem	Newell	k1gMnSc7
<g/>
)	)	kIx)
-	-	kIx~
Human	Human	k1gInSc1
Problem	Probl	k1gInSc7
Solving	Solving	k1gInSc1
-	-	kIx~
Prentice	Prentice	k1gFnSc1
Hall	Hall	k1gInSc1
<g/>
,	,	kIx,
Englewood	Englewood	k1gInSc1
Cliffs	Cliffs	k1gInSc1
<g/>
,	,	kIx,
NJ	NJ	kA
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
-	-	kIx~
Models	Models	k1gInSc1
of	of	k?
My	my	k3xPp1nPc1
Life	Lif	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basic	Basic	kA
Books	Books	k1gInSc1
<g/>
,	,	kIx,
Sloan	Sloan	k1gInSc1
Foundation	Foundation	k1gInSc1
Series	Series	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
autobiografie	autobiografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
-	-	kIx~
Economics	Economics	k1gInSc1
<g/>
,	,	kIx,
Bounded	Bounded	k1gMnSc1
Rationality	Rationalita	k1gFnSc2
and	and	k?
the	the	k?
Cognitive	Cognitiv	k1gInSc5
Revolution	Revolution	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edward	Edward	k1gMnSc1
Elgar	Elgar	k1gMnSc1
Publishing	Publishing	k1gInSc4
<g/>
,	,	kIx,
ISBN	ISBN	kA
1847208967	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Články	článek	k1gInPc1
</s>
<s>
1955	#num#	k4
-	-	kIx~
"	"	kIx"
<g/>
A	a	k9
Behavioral	Behavioral	k1gMnSc1
Model	modla	k1gFnPc2
of	of	k?
Rational	Rational	k1gMnSc2
Choice	Choic	k1gMnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Quarterly	Quarterl	k1gInPc1
Journal	Journal	k1gFnPc2
of	of	k?
Economics	Economicsa	k1gFnPc2
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
69	#num#	k4
<g/>
,	,	kIx,
99	#num#	k4
<g/>
–	–	k?
<g/>
118	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
-	-	kIx~
"	"	kIx"
<g/>
Motivational	Motivational	k1gMnSc1
and	and	k?
emotional	emotionat	k5eAaPmAgMnS,k5eAaImAgMnS
controls	controls	k6eAd1
of	of	k?
cognition	cognition	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Psychological	Psychological	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
74	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
<g/>
,	,	kIx,
reprinted	reprinted	k1gInSc1
in	in	k?
Models	Models	k1gInSc1
of	of	k?
Thought	Thought	k1gInSc1
Vol	vol	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Theories	Theories	k1gInSc1
of	of	k?
Bounded	Bounded	k1gInSc1
Rationality	Rationalita	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Chapter	Chapter	k1gInSc1
8	#num#	k4
in	in	k?
C.	C.	kA
B.	B.	kA
McGuire	McGuir	k1gInSc5
and	and	k?
R.	R.	kA
Radner	Radner	k1gInSc1
<g/>
,	,	kIx,
eds	eds	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Decision	Decision	k1gInSc1
and	and	k?
Organization	Organization	k1gInSc1
<g/>
,	,	kIx,
Amsterdam	Amsterdam	k1gInSc1
<g/>
:	:	kIx,
North-Holland	North-Holland	k1gInSc1
Publishing	Publishing	k1gInSc1
Company	Compana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
-	-	kIx~
"	"	kIx"
<g/>
Human	Human	k1gInSc1
Nature	Natur	k1gMnSc5
in	in	k?
Politics	Politics	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Dialogue	Dialogu	k1gFnSc2
of	of	k?
Psychology	psycholog	k1gMnPc4
with	with	k1gMnSc1
Political	Political	k1gMnSc1
Science	Science	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
The	The	k1gFnSc1
American	Americany	k1gInPc2
Political	Political	k1gFnSc1
Science	Science	k1gFnSc1
Review	Review	k1gFnSc1
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
79	#num#	k4
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
2	#num#	k4
(	(	kIx(
<g/>
Jun	jun	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
293	#num#	k4
<g/>
–	–	k?
<g/>
304	#num#	k4
</s>
<s>
1992	#num#	k4
-	-	kIx~
"	"	kIx"
<g/>
What	What	k1gInSc1
is	is	k?
an	an	k?
"	"	kIx"
<g/>
Explanation	Explanation	k1gInSc1
<g/>
"	"	kIx"
of	of	k?
Behavior	Behavior	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
Psychological	Psychological	k1gFnSc1
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
150-161	150-161	k4
</s>
<s>
1995	#num#	k4
(	(	kIx(
<g/>
with	with	k1gMnSc1
Peter	Peter	k1gMnSc1
C.	C.	kA
<g/>
-	-	kIx~
<g/>
H.	H.	kA
Cheng	Cheng	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Scientific	Scientific	k1gMnSc1
discovery	discovera	k1gFnSc2
and	and	k?
creative	creativ	k1gInSc5
reasoning	reasoning	k1gInSc1
with	with	k1gMnSc1
diagrams	diagrams	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
in	in	k?
S.	S.	kA
M.	M.	kA
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
T.	T.	kA
B.	B.	kA
Ward	Ward	k1gMnSc1
&	&	k?
R.	R.	kA
A.	A.	kA
Finke	Finke	k1gFnSc1
(	(	kIx(
<g/>
Eds	Eds	k1gFnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
The	The	k1gMnSc1
Creative	Creativ	k1gInSc5
Cognition	Cognition	k1gInSc4
Approach	Approach	k1gInSc4
(	(	kIx(
<g/>
pp	pp	k?
<g/>
.	.	kIx.
205	#num#	k4
<g/>
–	–	k?
<g/>
228	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
MA	MA	kA
<g/>
:	:	kIx,
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Haggbloom	Haggbloom	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
<g/>
J.	J.	kA
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
100	#num#	k4
Most	most	k1gInSc1
Eminent	Eminent	k1gMnSc1
Psychologists	Psychologistsa	k1gFnPc2
of	of	k?
the	the	k?
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Review	Review	k1gMnSc1
of	of	k?
General	General	k1gMnSc1
Psychology	psycholog	k1gMnPc4
6	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
139	#num#	k4
<g/>
–	–	k?
<g/>
152	#num#	k4
<g/>
↑	↑	k?
RAWSON	RAWSON	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
Wilson	Wilson	k1gMnSc1
Estate	Estat	k1gInSc5
<g/>
,	,	kIx,
Pittsburgh	Pittsburgh	k1gInSc1
Post-Gazette	Post-Gazett	k1gMnSc5
<g/>
,	,	kIx,
and	and	k?
August	August	k1gMnSc1
Wilson	Wilson	k1gMnSc1
House	house	k1gNnSc1
Establish	Establish	k1gMnSc1
August	August	k1gMnSc1
Wilson	Wilson	k1gMnSc1
Century	Centura	k1gFnSc2
Cycle	Cycle	k1gFnSc2
Awards	Awardsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
Wilson	Wilson	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2577	#num#	k4
<g/>
-	-	kIx~
<g/>
7432	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.519	10.519	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
awj	awj	k?
<g/>
.2019	.2019	k4
<g/>
.36	.36	k4
<g/>
.	.	kIx.
↑	↑	k?
Original	Original	k1gFnPc4
PDF	PDF	kA
<g/>
.	.	kIx.
dx	dx	k?
<g/>
.	.	kIx.
<g/>
doi	doi	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Division	Division	k1gInSc1
Commander	Commander	k1gInSc1
<g/>
,	,	kIx,
May	May	k1gMnSc1
<g/>
–	–	k?
<g/>
October	October	k1gMnSc1
1864	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
North	Northa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Press	Press	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.514	10.514	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
9780807895702	#num#	k4
<g/>
_kundahl	_kundahnout	k5eAaPmAgInS
<g/>
.12	.12	k4
<g/>
.	.	kIx.
↑	↑	k?
BARROS	BARROS	kA
<g/>
,	,	kIx,
Gustavo	Gustava	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herbert	Herbert	k1gMnSc1
A.	A.	kA
Simon	Simon	k1gMnSc1
and	and	k?
the	the	k?
concept	concept	k1gInSc1
of	of	k?
rationality	rationalita	k1gFnSc2
<g/>
:	:	kIx,
boundaries	boundaries	k1gMnSc1
and	and	k?
procedures	procedures	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brazilian	Brazilian	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Political	Political	k1gFnSc1
Economy	Econom	k1gInPc4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
30	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
455	#num#	k4
<g/>
–	–	k?
<g/>
472	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1809	#num#	k4
<g/>
-	-	kIx~
<g/>
4538	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.159	10.159	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
101	#num#	k4
<g/>
-	-	kIx~
<g/>
31572010000300006	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ANDERSON	Anderson	k1gMnSc1
<g/>
,	,	kIx,
Marc	Marc	k1gInSc1
H.	H.	kA
<g/>
;	;	kIx,
LEMKEN	LEMKEN	kA
<g/>
,	,	kIx,
Russell	Russell	k1gMnSc1
K.	K.	kA
An	An	k1gMnSc1
Empirical	Empirical	k1gMnSc1
Assessment	Assessment	k1gMnSc1
of	of	k?
the	the	k?
Influence	influence	k1gFnSc2
of	of	k?
March	March	k1gMnSc1
and	and	k?
Simon	Simon	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Organizations	Organizationsa	k1gFnPc2
:	:	kIx,
The	The	k1gFnSc1
Realized	Realized	k1gMnSc1
Contribution	Contribution	k1gInSc1
and	and	k?
Unfulfilled	Unfulfilled	k1gInSc1
Promise	Promise	k1gFnSc1
of	of	k?
a	a	k8xC
Masterpiece	Masterpiece	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Management	management	k1gInSc1
Studies	Studies	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
56	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1537	#num#	k4
<g/>
–	–	k?
<g/>
1569	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
2380	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
joms	joms	k6eAd1
<g/>
.12527	.12527	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NEWELL	NEWELL	kA
<g/>
,	,	kIx,
Allen	Allen	k1gMnSc1
<g/>
;	;	kIx,
SIMON	Simon	k1gMnSc1
<g/>
,	,	kIx,
Herbert	Herbert	k1gMnSc1
A.	A.	kA
Computer	computer	k1gInSc1
science	scienec	k1gInSc2
as	as	k9
empirical	empiricat	k5eAaPmAgMnS
inquiry	inquir	k1gInPc4
<g/>
:	:	kIx,
symbols	symbols	k6eAd1
and	and	k?
search	search	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Association	Association	k1gInSc1
of	of	k?
Computing	Computing	k1gInSc1
Machinery	Machiner	k1gMnPc4
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4503	#num#	k4
<g/>
-	-	kIx~
<g/>
1049	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1975	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FEIGENBAUM	FEIGENBAUM	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
A.	A.	kA
<g/>
;	;	kIx,
SIMON	Simon	k1gMnSc1
<g/>
,	,	kIx,
Herbert	Herbert	k1gMnSc1
A.	A.	kA
EPAM-like	EPAM-like	k1gInSc1
Models	Models	k1gInSc1
of	of	k?
Recognition	Recognition	k1gInSc1
and	and	k?
Learning	Learning	k1gInSc1
<g/>
*	*	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
305	#num#	k4
<g/>
–	–	k?
<g/>
336	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.120	10.120	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
15516709	#num#	k4
<g/>
cog	cog	k?
<g/>
0	#num#	k4
<g/>
804	#num#	k4
<g/>
_	_	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GOBET	GOBET	kA
<g/>
,	,	kIx,
Fernand	Fernand	k1gInSc1
<g/>
;	;	kIx,
SIMON	Simon	k1gMnSc1
<g/>
,	,	kIx,
Herbert	Herbert	k1gMnSc1
A.	A.	kA
Five	Five	k1gInSc1
Seconds	Seconds	k1gInSc1
or	or	k?
Sixty	Sixta	k1gMnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Presentation	Presentation	k1gInSc1
Time	Tim	k1gFnSc2
in	in	k?
Expert	expert	k1gMnSc1
Memory	Memora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
24	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
651	#num#	k4
<g/>
–	–	k?
<g/>
682	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
364	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
213	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.120	10.120	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
15516709	#num#	k4
<g/>
cog	cog	k?
<g/>
2404	#num#	k4
<g/>
_	_	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Herbert	Herbert	k1gMnSc1
A.	A.	kA
Simon	Simon	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Herbert	Herbert	k1gMnSc1
A.	A.	kA
Simon	Simon	k1gMnSc1
6	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nositelé	nositel	k1gMnPc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
</s>
<s>
Milton	Milton	k1gInSc1
Friedman	Friedman	k1gMnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Bertil	Bertit	k5eAaImAgMnS,k5eAaPmAgMnS
Ohlin	Ohlin	k1gMnSc1
/	/	kIx~
James	James	k1gMnSc1
Meade	Mead	k1gInSc5
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Herbert	Herbert	k1gMnSc1
A.	A.	kA
Simon	Simon	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Theodore	Theodor	k1gMnSc5
Schultz	Schultz	k1gMnSc1
/	/	kIx~
Arthur	Arthur	k1gMnSc1
Lewis	Lewis	k1gFnSc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lawrence	Lawrence	k1gFnSc1
Klein	Klein	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
James	James	k1gMnSc1
Tobin	Tobin	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
George	Georg	k1gMnSc4
Stigler	Stigler	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Gérard	Gérard	k1gInSc1
Debreu	Debreus	k1gInSc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Richard	Richard	k1gMnSc1
Stone	ston	k1gInSc5
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Franco	Franco	k6eAd1
Modigliani	Modiglian	k1gMnPc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
James	James	k1gMnSc1
M.	M.	kA
Buchanan	Buchanan	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Robert	Robert	k1gMnSc1
Solow	Solow	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Maurice	Maurika	k1gFnSc3
Allais	Allais	k1gInSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Trygve	Trygvat	k5eAaPmIp3nS
Haavelmo	Haavelma	k1gFnSc5
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Harry	Harr	k1gInPc1
Markowitz	Markowitza	k1gFnPc2
/	/	kIx~
Merton	Merton	k1gInSc1
Miller	Miller	k1gMnSc1
/	/	kIx~
William	William	k1gInSc1
Forsyth	Forsytha	k1gFnPc2
Sharpe	sharp	k1gInSc5
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ronald	Ronald	k1gMnSc1
H.	H.	kA
Coase	Coase	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Gary	Gar	k2eAgInPc1d1
Stanley	Stanley	k1gInPc1
Becker	Beckra	k1gFnPc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Robert	Robert	k1gMnSc1
Fogel	Fogel	k1gMnSc1
/	/	kIx~
Douglass	Douglass	k1gInSc1
North	North	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
John	John	k1gMnSc1
Harsanyi	Harsany	k1gFnSc2
/	/	kIx~
John	John	k1gMnSc1
Forbes	forbes	k1gInSc4
Nash	Nash	k1gMnSc1
/	/	kIx~
Reinhard	Reinhard	k1gMnSc1
Selten	Selten	k2eAgMnSc1d1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Robert	Robert	k1gMnSc1
Lucas	Lucas	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
James	James	k1gMnSc1
Mirrlees	Mirrlees	k1gMnSc1
/	/	kIx~
William	William	k1gInSc1
Vickrey	Vickrea	k1gFnSc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Robert	Robert	k1gMnSc1
C.	C.	kA
Merton	Merton	k1gInSc1
/	/	kIx~
Myron	Myron	k1gMnSc1
Scholes	Scholes	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Amartya	Amartya	k1gFnSc1
Sen	sena	k1gFnPc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Robert	Robert	k1gMnSc1
Mundell	Mundell	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
James	James	k1gMnSc1
Heckman	Heckman	k1gMnSc1
/	/	kIx~
Daniel	Daniel	k1gMnSc1
McFadden	McFaddna	k1gFnPc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
•	•	k?
</s>
<s>
1976	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
•	•	k?
</s>
<s>
2001	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990007976	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118936271	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2095	#num#	k4
8302	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79021485	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
29540765	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79021485	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
</s>
