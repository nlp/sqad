<s>
Katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Blue	Blue	k1gFnSc1	Blue
tongue	tonguat	k5eAaPmIp3nS	tonguat
disease	disease	k6eAd1	disease
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nekontagiózní	kontagiózní	k2eNgNnSc1d1	kontagiózní
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
domácích	domácí	k1gMnPc2	domácí
a	a	k8xC	a
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
virem	vir	k1gInSc7	vir
Bluetongue	Bluetongue	k1gFnPc2	Bluetongue
virus	virus	k1gInSc4	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Reoviridae	Reovirida	k1gFnSc2	Reovirida
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přenášeno	přenášet	k5eAaImNgNnS	přenášet
krevsajícím	krevsající	k2eAgInSc7d1	krevsající
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
tiplíky	tiplík	k1gMnPc7	tiplík
(	(	kIx(	(
<g/>
Culicoides	Culicoidesa	k1gFnPc2	Culicoidesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nákaza	nákaza	k1gFnSc1	nákaza
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
záněty	zánět	k1gInPc7	zánět
a	a	k8xC	a
otoky	otok	k1gInPc1	otok
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
víček	víčko	k1gNnPc2	víčko
<g/>
,	,	kIx,	,
uší	ucho	k1gNnPc2	ucho
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgInPc1d1	ústní
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
krváceniny	krvácenina	k1gFnPc1	krvácenina
a	a	k8xC	a
vředy	vřed	k1gInPc1	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
onemocnění	onemocnění	k1gNnSc3	onemocnění
vnímavý	vnímavý	k2eAgInSc4d1	vnímavý
<g/>
.	.	kIx.	.
</s>
<s>
Katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
popsána	popsat	k5eAaPmNgFnS	popsat
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
po	po	k7c6	po
importu	import	k1gInSc6	import
jemnovlnných	jemnovlnný	k2eAgNnPc2d1	jemnovlnný
plemen	plemeno	k1gNnPc2	plemeno
ovcí	ovce	k1gFnPc2	ovce
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
bylo	být	k5eAaImAgNnS	být
nazýváno	nazývat	k5eAaImNgNnS	nazývat
mj.	mj.	kA	mj.
"	"	kIx"	"
<g/>
pseudo-slintavka	pseudolintavka	k1gFnSc1	pseudo-slintavka
a	a	k8xC	a
kulhavka	kulhavka	k1gFnSc1	kulhavka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
bolavá	bolavý	k2eAgFnSc1d1	bolavá
tlama	tlama	k1gFnSc1	tlama
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
bluetongue	bluetongue	k1gInSc1	bluetongue
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
cyanózy	cyanóza	k1gFnSc2	cyanóza
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
vážněji	vážně	k6eAd2	vážně
postižených	postižený	k2eAgFnPc2d1	postižená
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
mimo	mimo	k7c4	mimo
africký	africký	k2eAgInSc4d1	africký
kontinent	kontinent	k1gInSc4	kontinent
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
výskyt	výskyt	k1gInSc1	výskyt
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
byl	být	k5eAaImAgInS	být
virus	virus	k1gInSc1	virus
izolován	izolovat	k5eAaBmNgInS	izolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byl	být	k5eAaImAgInS	být
virus	virus	k1gInSc1	virus
zjištěn	zjistit	k5eAaPmNgInS	zjistit
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
svoje	svůj	k3xOyFgNnSc4	svůj
celosvětové	celosvětový	k2eAgNnSc4d1	celosvětové
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
,	,	kIx,	,
omezená	omezený	k2eAgFnSc1d1	omezená
výskytem	výskyt	k1gInSc7	výskyt
vektora	vektor	k1gMnSc2	vektor
na	na	k7c4	na
zeměpisné	zeměpisný	k2eAgFnPc4d1	zeměpisná
šířky	šířka	k1gFnPc4	šířka
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
40	[number]	k4	40
<g/>
°	°	k?	°
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
do	do	k7c2	do
35	[number]	k4	35
<g/>
°	°	k?	°
j.š.	j.š.	k?	j.š.
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
jen	jen	k9	jen
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
tradiční	tradiční	k2eAgFnSc1d1	tradiční
hranice	hranice	k1gFnSc1	hranice
výskytu	výskyt	k1gInSc2	výskyt
posouvat	posouvat	k5eAaImF	posouvat
severněji	severně	k6eAd2	severně
a	a	k8xC	a
bluetongue	bluetongue	k6eAd1	bluetongue
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
objevovala	objevovat	k5eAaImAgFnS	objevovat
ve	v	k7c6	v
Středozemí	středozemí	k1gNnSc6	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
zcela	zcela	k6eAd1	zcela
nečekaně	nečekaně	k6eAd1	nečekaně
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Epizoocie	Epizoocie	k1gFnPc1	Epizoocie
probíhající	probíhající	k2eAgFnPc1d1	probíhající
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
nebývalého	bývalý	k2eNgInSc2d1	bývalý
rozsahu	rozsah	k1gInSc2	rozsah
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
v	v	k7c6	v
populacích	populace	k1gFnPc6	populace
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
dosud	dosud	k6eAd1	dosud
probíhala	probíhat	k5eAaImAgFnS	probíhat
většinou	většina	k1gFnSc7	většina
inaparentně	inaparentně	k6eAd1	inaparentně
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
katarální	katarální	k2eAgFnSc2d1	katarální
horečky	horečka	k1gFnSc2	horečka
ovcí	ovce	k1gFnPc2	ovce
je	být	k5eAaImIp3nS	být
bluetongue	bluetongue	k1gInSc1	bluetongue
virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
BTV	BTV	kA	BTV
<g/>
)	)	kIx)	)
-	-	kIx~	-
RNA	RNA	kA	RNA
virus	virus	k1gInSc1	virus
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Orbivirus	Orbivirus	k1gInSc4	Orbivirus
<g/>
,	,	kIx,	,
čeledi	čeleď	k1gFnPc4	čeleď
Reoviridae	Reovirida	k1gFnSc2	Reovirida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
neobalený	obalený	k2eNgInSc1d1	neobalený
virus	virus	k1gInSc1	virus
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
cca	cca	kA	cca
90	[number]	k4	90
nm	nm	k?	nm
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
genom	genom	k1gInSc1	genom
tvoří	tvořit	k5eAaImIp3nS	tvořit
10	[number]	k4	10
segmentů	segment	k1gInPc2	segment
dvojvláknité	dvojvláknitý	k2eAgFnSc2d1	dvojvláknitý
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Virová	virový	k2eAgFnSc1d1	virová
partikule	partikule	k1gFnSc1	partikule
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
sedmi	sedm	k4xCc7	sedm
strukturálními	strukturální	k2eAgInPc7d1	strukturální
proteiny	protein	k1gInPc7	protein
(	(	kIx(	(
<g/>
VP	VP	kA	VP
<g/>
1	[number]	k4	1
-	-	kIx~	-
VP	VP	kA	VP
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
infikované	infikovaný	k2eAgFnSc6d1	infikovaná
buňce	buňka	k1gFnSc6	buňka
jsou	být	k5eAaImIp3nP	být
syntetizovány	syntetizovat	k5eAaImNgInP	syntetizovat
čtyři	čtyři	k4xCgInPc1	čtyři
nestrukturální	strukturální	k2eNgInPc1d1	strukturální
proteiny	protein	k1gInPc1	protein
(	(	kIx(	(
<g/>
NS	NS	kA	NS
<g/>
1	[number]	k4	1
-	-	kIx~	-
NS	NS	kA	NS
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
NS	NS	kA	NS
<g/>
3	[number]	k4	3
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
uznáváno	uznávat	k5eAaImNgNnS	uznávat
celkem	celek	k1gInSc7	celek
24	[number]	k4	24
sérotypů	sérotyp	k1gInPc2	sérotyp
BTV	BTV	kA	BTV
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
objevil	objevit	k5eAaPmAgInS	objevit
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
bluetongue	bluetongue	k1gInSc1	bluetongue
viru	vir	k1gInSc2	vir
podobný	podobný	k2eAgInSc1d1	podobný
virus	virus	k1gInSc1	virus
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Toggenburg	Toggenburg	k1gMnSc1	Toggenburg
orbivirus	orbivirus	k1gMnSc1	orbivirus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
možný	možný	k2eAgInSc1d1	možný
25	[number]	k4	25
<g/>
.	.	kIx.	.
sérotyp	sérotyp	k1gInSc1	sérotyp
BTV	BTV	kA	BTV
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
26	[number]	k4	26
<g/>
.	.	kIx.	.
sérotyp	sérotyp	k1gMnSc1	sérotyp
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Bluetongue	Bluetongue	k6eAd1	Bluetongue
virus	virus	k1gInSc1	virus
je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k1gMnSc1	příbuzný
viru	vir	k1gInSc2	vir
epizootického	epizootický	k2eAgNnSc2d1	epizootické
hemoragického	hemoragický	k2eAgNnSc2d1	hemoragické
onemocnění	onemocnění	k1gNnSc2	onemocnění
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
,	,	kIx,	,
viru	vira	k1gFnSc4	vira
moru	mor	k1gInSc2	mor
koní	kůň	k1gMnPc2	kůň
či	či	k8xC	či
viru	vir	k1gInSc2	vir
koňské	koňský	k2eAgFnSc2d1	koňská
encefalózy	encefalóza	k1gFnSc2	encefalóza
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
jsou	být	k5eAaImIp3nP	být
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
všichni	všechen	k3xTgMnPc1	všechen
domácí	domácí	k1gMnPc1	domácí
i	i	k9	i
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
přežvýkavci	přežvýkavec	k1gMnPc1	přežvýkavec
a	a	k8xC	a
velbloudovití	velbloudovitý	k2eAgMnPc1d1	velbloudovitý
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
u	u	k7c2	u
jemnovlnných	jemnovlnný	k2eAgNnPc2d1	jemnovlnný
plemen	plemeno	k1gNnPc2	plemeno
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
jelence	jelenka	k1gFnSc6	jelenka
běloocasého	běloocasý	k2eAgMnSc2d1	běloocasý
(	(	kIx(	(
<g/>
Odocoileus	Odocoileus	k1gInSc1	Odocoileus
virginianus	virginianus	k1gInSc1	virginianus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kozy	koza	k1gFnPc1	koza
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
skotu	skot	k1gInSc2	skot
probíhala	probíhat	k5eAaImAgFnS	probíhat
tato	tento	k3xDgFnSc1	tento
nákaza	nákaza	k1gFnSc1	nákaza
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
obvykle	obvykle	k6eAd1	obvykle
asymptomaticky	asymptomaticky	k6eAd1	asymptomaticky
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
epizoocie	epizoocie	k1gFnSc1	epizoocie
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
sérotypem	sérotyp	k1gInSc7	sérotyp
BTV-	BTV-	k1gFnSc2	BTV-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
klinických	klinický	k2eAgInPc2d1	klinický
projevů	projev	k1gInPc2	projev
i	i	k9	i
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Skot	Skot	k1gMnSc1	Skot
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
epizootologického	epizootologický	k2eAgNnSc2d1	epizootologické
hlediska	hledisko	k1gNnSc2	hledisko
významný	významný	k2eAgInSc1d1	významný
také	také	k6eAd1	také
dlouhotrvající	dlouhotrvající	k2eAgFnSc7d1	dlouhotrvající
virémií	virémie	k1gFnSc7	virémie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
60	[number]	k4	60
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
100	[number]	k4	100
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
i	i	k8xC	i
případy	případ	k1gInPc1	případ
katarální	katarální	k2eAgFnSc2d1	katarální
horečky	horečka	k1gFnSc2	horečka
ovcí	ovce	k1gFnPc2	ovce
u	u	k7c2	u
masožravců	masožravec	k1gMnPc2	masožravec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
sérologickou	sérologický	k2eAgFnSc4d1	sérologická
pozitivitu	pozitivita	k1gFnSc4	pozitivita
(	(	kIx(	(
<g/>
především	především	k9	především
u	u	k7c2	u
afrických	africký	k2eAgFnPc2d1	africká
šelem	šelma	k1gFnPc2	šelma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dvou	dva	k4xCgInPc2	dva
rysů	rys	k1gInPc2	rys
v	v	k7c6	v
belgické	belgický	k2eAgFnSc6d1	belgická
zoologické	zoologický	k2eAgFnSc6d1	zoologická
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
krmených	krmený	k2eAgFnPc2d1	krmená
zmetky	zmetek	k1gMnPc7	zmetek
a	a	k8xC	a
mrtvě	mrtvě	k6eAd1	mrtvě
narozenými	narozený	k2eAgNnPc7d1	narozené
telaty	tele	k1gNnPc7	tele
z	z	k7c2	z
blízkých	blízký	k2eAgFnPc2d1	blízká
farem	farma	k1gFnPc2	farma
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
klinických	klinický	k2eAgInPc2d1	klinický
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
úhynu	úhyn	k1gInSc2	úhyn
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
katarální	katarální	k2eAgFnSc2d1	katarální
horečky	horečka	k1gFnSc2	horečka
ovcí	ovce	k1gFnPc2	ovce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
zprostředkován	zprostředkován	k2eAgInSc4d1	zprostředkován
vektory	vektor	k1gInPc4	vektor
-	-	kIx~	-
tiplíky	tiplík	k1gMnPc4	tiplík
rodu	rod	k1gInSc2	rod
Culicoides	Culicoides	k1gMnSc1	Culicoides
<g/>
.	.	kIx.	.
</s>
<s>
Tiplíci	tiplík	k1gMnPc1	tiplík
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
několik	několik	k4yIc1	několik
milimetrů	milimetr	k1gInPc2	milimetr
a	a	k8xC	a
v	v	k7c6	v
přenosu	přenos	k1gInSc6	přenos
bluetongue	bluetongu	k1gFnSc2	bluetongu
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
tiplíků	tiplík	k1gMnPc2	tiplík
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
před	před	k7c7	před
nakladením	nakladení	k1gNnSc7	nakladení
vajíček	vajíčko	k1gNnPc2	vajíčko
nasát	nasát	k5eAaPmF	nasát
krve	krev	k1gFnPc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
tiplíků	tiplík	k1gMnPc2	tiplík
trvá	trvat	k5eAaImIp3nS	trvat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
dva	dva	k4xCgInPc1	dva
až	až	k6eAd1	až
šest	šest	k4xCc1	šest
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímý	přímý	k2eAgInSc1d1	přímý
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
přes	přes	k7c4	přes
stádia	stádium	k1gNnPc4	stádium
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
,	,	kIx,	,
třech	tři	k4xCgInPc2	tři
larválních	larvální	k2eAgInPc2d1	larvální
instarů	instar	k1gInPc2	instar
<g/>
,	,	kIx,	,
kukly	kukla	k1gFnSc2	kukla
a	a	k8xC	a
dospělého	dospělý	k2eAgMnSc2d1	dospělý
tiplíka	tiplík	k1gMnSc2	tiplík
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
aktivně	aktivně	k6eAd1	aktivně
létají	létat	k5eAaImIp3nP	létat
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
1-2	[number]	k4	1-2
km	km	kA	km
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
unášeni	unášen	k2eAgMnPc1d1	unášen
větrem	vítr	k1gInSc7	vítr
až	až	k9	až
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
přenašečem	přenašeč	k1gInSc7	přenašeč
katarální	katarální	k2eAgFnSc2d1	katarální
horečky	horečka	k1gFnSc2	horečka
ovcí	ovce	k1gFnPc2	ovce
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
teplomilný	teplomilný	k2eAgInSc1d1	teplomilný
druh	druh	k1gInSc1	druh
Culicoides	Culicoides	k1gMnSc1	Culicoides
imicola	imicola	k1gFnSc1	imicola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
choroba	choroba	k1gFnSc1	choroba
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přenášena	přenášet	k5eAaImNgFnS	přenášet
druhy	druh	k1gInPc7	druh
patřícími	patřící	k2eAgInPc7d1	patřící
do	do	k7c2	do
komplexů	komplex	k1gInPc2	komplex
Obsoletus	Obsoletus	k1gInSc1	Obsoletus
a	a	k8xC	a
Pulicaris	Pulicaris	k1gInSc1	Pulicaris
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
komplexy	komplex	k1gInPc1	komplex
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
morfologicky	morfologicky	k6eAd1	morfologicky
těžko	těžko	k6eAd1	těžko
odlišitelné	odlišitelný	k2eAgMnPc4d1	odlišitelný
tiplíky	tiplík	k1gMnPc4	tiplík
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
samičky	samička	k1gFnPc1	samička
tiplíků	tiplík	k1gMnPc2	tiplík
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
podle	podle	k7c2	podle
druhů	druh	k1gInPc2	druh
C.	C.	kA	C.
obsoletus	obsoletus	k1gInSc1	obsoletus
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
C.	C.	kA	C.
pulicaris	pulicaris	k1gFnSc1	pulicaris
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
přenašečem	přenašeč	k1gInSc7	přenašeč
druh	druh	k1gInSc1	druh
C.	C.	kA	C.
sonorensis	sonorensis	k1gInSc1	sonorensis
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
pak	pak	k6eAd1	pak
C.	C.	kA	C.
insignis	insignis	k1gFnSc1	insignis
a	a	k8xC	a
C.	C.	kA	C.
pusillus	pusillus	k1gInSc1	pusillus
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
pak	pak	k6eAd1	pak
druhy	druh	k1gInPc4	druh
C.	C.	kA	C.
brevitarsis	brevitarsis	k1gInSc4	brevitarsis
<g/>
,	,	kIx,	,
C.	C.	kA	C.
fulvus	fulvus	k1gInSc1	fulvus
<g/>
,	,	kIx,	,
C.	C.	kA	C.
wadai	wada	k1gFnSc2	wada
a	a	k8xC	a
C.	C.	kA	C.
actoni	acton	k1gMnPc1	acton
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tiplíků	tiplík	k1gMnPc2	tiplík
byl	být	k5eAaImAgMnS	být
BTV	BTV	kA	BTV
izolován	izolován	k2eAgMnSc1d1	izolován
i	i	k8xC	i
z	z	k7c2	z
jiných	jiný	k2eAgMnPc2d1	jiný
členovců	členovec	k1gMnPc2	členovec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kloše	kloš	k1gMnSc2	kloš
ovčího	ovčí	k2eAgMnSc2d1	ovčí
(	(	kIx(	(
<g/>
Melophagus	Melophagus	k1gInSc1	Melophagus
ovinus	ovinus	k1gInSc1	ovinus
<g/>
)	)	kIx)	)
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
klíšťat	klíště	k1gNnPc2	klíště
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc2	on
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tiplíků	tiplík	k1gMnPc2	tiplík
nereplikuje	replikovat	k5eNaImIp3nS	replikovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
mechanické	mechanický	k2eAgInPc4d1	mechanický
přenašeče	přenašeč	k1gInPc4	přenašeč
<g/>
.	.	kIx.	.
</s>
<s>
BTV	BTV	kA	BTV
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
virémie	virémie	k1gFnSc2	virémie
může	moct	k5eAaImIp3nS	moct
vylučovat	vylučovat	k5eAaImF	vylučovat
semenem	semeno	k1gNnSc7	semeno
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
i	i	k9	i
transplacentární	transplacentární	k2eAgFnSc7d1	transplacentární
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
přenosu	přenos	k1gInSc2	přenos
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
přenos	přenos	k1gInSc1	přenos
BTV	BTV	kA	BTV
mlezivem	mlezivo	k1gNnSc7	mlezivo
<g/>
.	.	kIx.	.
</s>
<s>
Orální	orální	k2eAgInSc1d1	orální
přenos	přenos	k1gInSc1	přenos
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
u	u	k7c2	u
šelem	šelma	k1gFnPc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
katarální	katarální	k2eAgFnSc2d1	katarální
horečky	horečka	k1gFnSc2	horečka
ovcí	ovce	k1gFnPc2	ovce
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
přežít	přežít	k5eAaPmF	přežít
období	období	k1gNnSc4	období
bez	bez	k7c2	bez
výskytu	výskyt	k1gInSc2	výskyt
dospělých	dospělý	k2eAgMnPc2d1	dospělý
tiplíků	tiplík	k1gMnPc2	tiplík
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
overwintering	overwintering	k1gInSc4	overwintering
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
přezimování	přezimování	k1gNnSc1	přezimování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
BTV	BTV	kA	BTV
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
období	období	k1gNnSc1	období
přežívá	přežívat	k5eAaImIp3nS	přežívat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
přežití	přežití	k1gNnSc4	přežití
dospělých	dospělý	k2eAgMnPc2d1	dospělý
tiplíků	tiplík	k1gMnPc2	tiplík
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mírné	mírný	k2eAgFnSc2d1	mírná
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
tiplíci	tiplík	k1gMnPc1	tiplík
sice	sice	k8xC	sice
žijí	žít	k5eAaImIp3nP	žít
obvykle	obvykle	k6eAd1	obvykle
10	[number]	k4	10
-	-	kIx~	-
20	[number]	k4	20
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k8xS	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
přežití	přežití	k1gNnSc4	přežití
viru	vir	k1gInSc2	vir
ve	v	k7c6	v
vývojových	vývojový	k2eAgNnPc6d1	vývojové
stádiích	stádium	k1gNnPc6	stádium
tiplíků	tiplík	k1gMnPc2	tiplík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
u	u	k7c2	u
tiplíků	tiplík	k1gMnPc2	tiplík
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
transovariálnímu	transovariální	k2eAgInSc3d1	transovariální
přenosu	přenos	k1gInSc3	přenos
BTV	BTV	kA	BTV
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nepotrvzenou	potrvzený	k2eNgFnSc7d1	potrvzený
teorií	teorie	k1gFnSc7	teorie
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
navázání	navázání	k1gNnSc2	navázání
BTV	BTV	kA	BTV
na	na	k7c4	na
γ	γ	k?	γ
T-lymfocyty	Tymfocyt	k1gInPc4	T-lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
přežití	přežití	k1gNnSc4	přežití
viru	vir	k1gInSc2	vir
díky	díky	k7c3	díky
transplacentárnímu	transplacentární	k2eAgInSc3d1	transplacentární
přenosu	přenos	k1gInSc3	přenos
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dlouhotrvající	dlouhotrvající	k2eAgFnSc4d1	dlouhotrvající
virémii	virémie	k1gFnSc4	virémie
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
bodnutí	bodnutí	k1gNnSc4	bodnutí
infikovaným	infikovaný	k2eAgMnSc7d1	infikovaný
tiplíkem	tiplík	k1gMnSc7	tiplík
je	být	k5eAaImIp3nS	být
BTV	BTV	kA	BTV
dendritickými	dendritický	k2eAgFnPc7d1	dendritická
buňkami	buňka	k1gFnPc7	buňka
transportován	transportován	k2eAgInSc1d1	transportován
do	do	k7c2	do
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
virus	virus	k1gInSc1	virus
pomnoží	pomnožit	k5eAaPmIp3nS	pomnožit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
primární	primární	k2eAgFnSc1d1	primární
virémie	virémie	k1gFnSc1	virémie
<g/>
.	.	kIx.	.
</s>
<s>
Krví	krev	k1gFnSc7	krev
je	být	k5eAaImIp3nS	být
BTV	BTV	kA	BTV
zanesen	zanesen	k2eAgMnSc1d1	zanesen
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
,	,	kIx,	,
sleziny	slezina	k1gFnSc2	slezina
a	a	k8xC	a
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nS	množit
v	v	k7c6	v
endoteliálních	endoteliální	k2eAgFnPc6d1	endoteliální
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
makrofázích	makrofág	k1gInPc6	makrofág
a	a	k8xC	a
lymfocytech	lymfocyt	k1gInPc6	lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
navázán	navázán	k2eAgInSc1d1	navázán
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
krevní	krevní	k2eAgInPc4d1	krevní
elementy	element	k1gInPc4	element
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bývá	bývat	k5eAaImIp3nS	bývat
nacházen	nacházen	k2eAgInSc1d1	nacházen
jen	jen	k9	jen
na	na	k7c6	na
erytrocytech	erytrocyt	k1gInPc6	erytrocyt
<g/>
,	,	kIx,	,
v	v	k7c6	v
záhybech	záhyb	k1gInPc6	záhyb
jejich	jejich	k3xOp3gFnSc2	jejich
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
účinkem	účinek	k1gInSc7	účinek
neutralizačních	neutralizační	k2eAgFnPc2d1	neutralizační
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
BTV	BTV	kA	BTV
replikuje	replikovat	k5eAaImIp3nS	replikovat
<g/>
,	,	kIx,	,
zanikají	zanikat	k5eAaImIp3nP	zanikat
nekrózou	nekróza	k1gFnSc7	nekróza
či	či	k8xC	či
apoptózou	apoptóza	k1gFnSc7	apoptóza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
poškození	poškození	k1gNnSc2	poškození
endotelu	endotel	k1gInSc2	endotel
malých	malý	k2eAgFnPc2d1	malá
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
cílových	cílový	k2eAgInPc2d1	cílový
orgánů	orgán	k1gInPc2	orgán
podílejí	podílet	k5eAaImIp3nP	podílet
i	i	k9	i
obranné	obranný	k2eAgInPc4d1	obranný
mechanismy	mechanismus	k1gInPc4	mechanismus
infikovaného	infikovaný	k2eAgNnSc2d1	infikované
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
produkce	produkce	k1gFnSc1	produkce
prozánětlivých	prozánětlivý	k2eAgInPc2d1	prozánětlivý
cytokinů	cytokin	k1gInPc2	cytokin
<g/>
,	,	kIx,	,
cyklooxygenázy	cyklooxygenáza	k1gFnSc2	cyklooxygenáza
2	[number]	k4	2
<g/>
,	,	kIx,	,
prostacyklinu	prostacyklin	k1gInSc2	prostacyklin
<g/>
,	,	kIx,	,
tromboxanu	tromboxan	k1gInSc2	tromboxan
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
probíhá	probíhat	k5eAaImIp3nS	probíhat
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
akutně	akutně	k6eAd1	akutně
<g/>
,	,	kIx,	,
subakutně	subakutně	k6eAd1	subakutně
či	či	k8xC	či
chronicky	chronicky	k6eAd1	chronicky
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
rozvinou	rozvinout	k5eAaPmIp3nP	rozvinout
u	u	k7c2	u
jemnovlnných	jemnovlnný	k2eAgNnPc2d1	jemnovlnný
plemen	plemeno	k1gNnPc2	plemeno
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
inkubační	inkubační	k2eAgFnSc6d1	inkubační
době	doba	k1gFnSc6	doba
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
čtyři	čtyři	k4xCgNnPc4	čtyři
až	až	k9	až
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
apatie	apatie	k1gFnSc1	apatie
<g/>
,	,	kIx,	,
otok	otok	k1gInSc1	otok
a	a	k8xC	a
překrvení	překrvení	k1gNnSc1	překrvení
sliznice	sliznice	k1gFnSc2	sliznice
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
a	a	k8xC	a
nozder	nozdra	k1gFnPc2	nozdra
<g/>
,	,	kIx,	,
postižená	postižený	k2eAgNnPc1d1	postižené
zvířata	zvíře	k1gNnPc1	zvíře
nadměrně	nadměrně	k6eAd1	nadměrně
sliní	slinit	k5eAaImIp3nP	slinit
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
výtok	výtok	k1gInSc1	výtok
z	z	k7c2	z
nozder	nozdra	k1gFnPc2	nozdra
-	-	kIx~	-
zpočátku	zpočátku	k6eAd1	zpočátku
vodnatý	vodnatý	k2eAgInSc4d1	vodnatý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
hlenohnisavý	hlenohnisavý	k2eAgMnSc1d1	hlenohnisavý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Výtok	výtok	k1gInSc1	výtok
na	na	k7c6	na
nozdrách	nozdra	k1gFnPc6	nozdra
zasychá	zasychat	k5eAaImIp3nS	zasychat
v	v	k7c4	v
krusty	krusta	k1gFnPc4	krusta
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nP	rozvíjet
otoky	otok	k1gInPc1	otok
pysků	pysk	k1gInPc2	pysk
<g/>
,	,	kIx,	,
mezisaničí	mezisaničí	k2eAgFnSc2d1	mezisaničí
či	či	k8xC	či
uší	ucho	k1gNnPc2	ucho
<g/>
,	,	kIx,	,
na	na	k7c6	na
sliznicích	sliznice	k1gFnPc6	sliznice
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
tečkovité	tečkovitý	k2eAgFnPc1d1	tečkovitá
krváceniny	krvácenina	k1gFnPc1	krvácenina
<g/>
,	,	kIx,	,
sliznice	sliznice	k1gFnSc1	sliznice
odumírá	odumírat	k5eAaImIp3nS	odumírat
a	a	k8xC	a
odlupuje	odlupovat	k5eAaImIp3nS	odlupovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
vředy	vřed	k1gInPc1	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Cyanóza	cyanóza	k1gFnSc1	cyanóza
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
horečnaté	horečnatý	k2eAgFnSc2d1	horečnatá
fáze	fáze	k1gFnSc2	fáze
onemocnění	onemocnění	k1gNnSc2	onemocnění
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
otok	otok	k1gInSc1	otok
a	a	k8xC	a
zčervenání	zčervenání	k1gNnSc1	zčervenání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přechodu	přechod	k1gInSc2	přechod
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
rohoviny	rohovina	k1gFnSc2	rohovina
pazhehtů	pazhehta	k1gMnPc2	pazhehta
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
koronitida	koronitida	k1gFnSc1	koronitida
<g/>
)	)	kIx)	)
laminitida	laminitida	k1gFnSc1	laminitida
a	a	k8xC	a
svalové	svalový	k2eAgFnPc1d1	svalová
nekrózy	nekróza	k1gFnPc1	nekróza
<g/>
,	,	kIx,	,
torticolis	torticolis	k1gFnPc1	torticolis
<g/>
,	,	kIx,	,
záněty	zánět	k1gInPc1	zánět
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
zhoršení	zhoršení	k1gNnSc2	zhoršení
kvality	kvalita	k1gFnSc2	kvalita
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k8xC	i
průjem	průjem	k1gInSc1	průjem
a	a	k8xC	a
zvracení	zvracení	k1gNnSc1	zvracení
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
aspirační	aspirační	k2eAgFnSc2d1	aspirační
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
Březí	březí	k2eAgFnPc1d1	březí
ovce	ovce	k1gFnPc1	ovce
mohou	moct	k5eAaImIp3nP	moct
zmetat	zmetat	k5eAaImF	zmetat
nebo	nebo	k8xC	nebo
rodit	rodit	k5eAaImF	rodit
malformovaná	malformovaný	k2eAgNnPc4d1	malformovaný
jehňata	jehně	k1gNnPc4	jehně
(	(	kIx(	(
<g/>
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
hydrocefalus	hydrocefalus	k1gInSc1	hydrocefalus
<g/>
,	,	kIx,	,
mozkové	mozkový	k2eAgFnSc2d1	mozková
cysty	cysta	k1gFnSc2	cysta
<g/>
,	,	kIx,	,
retinální	retinální	k2eAgFnSc2d1	retinální
dysplazie	dysplazie	k1gFnSc2	dysplazie
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
komplikacích	komplikace	k1gFnPc6	komplikace
způsobených	způsobený	k2eAgInPc2d1	způsobený
sekundární	sekundární	k2eAgFnSc7d1	sekundární
bakteriální	bakteriální	k2eAgFnSc7d1	bakteriální
infekcí	infekce	k1gFnSc7	infekce
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozvinout	rozvinout	k5eAaPmF	rozvinout
edém	edém	k1gInSc4	edém
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
nemocné	nemocný	k2eAgFnSc2d1	nemocná
ovce	ovce	k1gFnSc2	ovce
mohou	moct	k5eAaImIp3nP	moct
uhynout	uhynout	k5eAaPmF	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
klinických	klinický	k2eAgInPc2d1	klinický
projevů	projev	k1gInPc2	projev
u	u	k7c2	u
koz	koza	k1gFnPc2	koza
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
klinickým	klinický	k2eAgInPc3d1	klinický
příznakům	příznak	k1gInPc3	příznak
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bývají	bývat	k5eAaImIp3nP	bývat
mírnější	mírný	k2eAgFnPc1d2	mírnější
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
skotu	skot	k1gInSc2	skot
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
dříve	dříve	k6eAd2	dříve
probíhala	probíhat	k5eAaImAgFnS	probíhat
obvykle	obvykle	k6eAd1	obvykle
subklinicky	subklinicky	k6eAd1	subklinicky
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
infekce	infekce	k1gFnSc1	infekce
sérotypem	sérotyp	k1gInSc7	sérotyp
BTV-8	BTV-8	k1gFnSc2	BTV-8
se	se	k3xPyFc4	se
často	často	k6eAd1	často
projevovala	projevovat	k5eAaImAgFnS	projevovat
zjevným	zjevný	k2eAgNnSc7d1	zjevné
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Nemocná	nemocný	k2eAgNnPc1d1	nemocný
zvířata	zvíře	k1gNnPc1	zvíře
mají	mít	k5eAaImIp3nP	mít
horečku	horečka	k1gFnSc4	horečka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
apatická	apatický	k2eAgNnPc1d1	apatické
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
eroze	eroze	k1gFnPc1	eroze
a	a	k8xC	a
nekrózy	nekróza	k1gFnPc1	nekróza
na	na	k7c6	na
sliznicích	sliznice	k1gFnPc6	sliznice
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
a	a	k8xC	a
nosní	nosní	k2eAgFnSc2d1	nosní
<g/>
,	,	kIx,	,
výtok	výtok	k1gInSc1	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
salivace	salivace	k1gFnSc1	salivace
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc1	zánět
spojivek	spojivka	k1gFnPc2	spojivka
<g/>
,	,	kIx,	,
laminitida	laminitida	k1gFnSc1	laminitida
<g/>
,	,	kIx,	,
otoky	otok	k1gInPc1	otok
a	a	k8xC	a
zčervenání	zčervenání	k1gNnSc1	zčervenání
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
strucích	struk	k1gInPc6	struk
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
strupy	strup	k1gMnPc4	strup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
infekce	infekce	k1gFnSc2	infekce
březích	březí	k2eAgFnPc2d1	březí
krav	kráva	k1gFnPc2	kráva
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
odúmrti	odúmrť	k1gFnSc3	odúmrť
a	a	k8xC	a
resorpci	resorpce	k1gFnSc3	resorpce
embryí	embryo	k1gNnPc2	embryo
<g/>
,	,	kIx,	,
abortům	abort	k1gInPc3	abort
či	či	k8xC	či
porodům	porod	k1gInPc3	porod
slabých	slabý	k2eAgNnPc2d1	slabé
telat	tele	k1gNnPc2	tele
nebo	nebo	k8xC	nebo
telat	tele	k1gNnPc2	tele
s	s	k7c7	s
vrozenou	vrozený	k2eAgFnSc7d1	vrozená
vodnatelností	vodnatelnost	k1gFnSc7	vodnatelnost
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jelence	jelenka	k1gFnSc3	jelenka
běloocasého	běloocasý	k2eAgInSc2d1	běloocasý
má	mít	k5eAaImIp3nS	mít
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
obvykle	obvykle	k6eAd1	obvykle
závažný	závažný	k2eAgInSc4d1	závažný
průběh	průběh	k1gInSc4	průběh
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
letalitou	letalita	k1gFnSc7	letalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
infekce	infekce	k1gFnSc2	infekce
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
hemoragická	hemoragický	k2eAgFnSc1d1	hemoragická
diatéza	diatéza	k1gFnSc1	diatéza
-	-	kIx~	-
krváceniny	krvácenina	k1gFnPc1	krvácenina
v	v	k7c6	v
orgánech	orgán	k1gInPc6	orgán
<g/>
,	,	kIx,	,
otoky	otok	k1gInPc1	otok
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
slinění	slinění	k1gNnSc4	slinění
<g/>
,	,	kIx,	,
krvavý	krvavý	k2eAgInSc4d1	krvavý
výtok	výtok	k1gInSc4	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
krvavý	krvavý	k2eAgInSc4d1	krvavý
průjem	průjem	k1gInSc4	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
příznakům	příznak	k1gInPc3	příznak
epizootického	epizootický	k2eAgNnSc2d1	epizootické
hemoragického	hemoragický	k2eAgNnSc2d1	hemoragické
onemocnění	onemocnění	k1gNnSc2	onemocnění
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
nemocných	nemocný	k1gMnPc2	nemocný
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
podkožní	podkožní	k2eAgInPc1d1	podkožní
otoky	otok	k1gInPc1	otok
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
hyperemie	hyperemie	k1gFnSc1	hyperemie
<g/>
,	,	kIx,	,
cyanóza	cyanóza	k1gFnSc1	cyanóza
<g/>
,	,	kIx,	,
tečkovité	tečkovitý	k2eAgFnSc2d1	tečkovitá
i	i	k8xC	i
větší	veliký	k2eAgFnSc2d2	veliký
krváceniny	krvácenina	k1gFnSc2	krvácenina
na	na	k7c6	na
sliznici	sliznice	k1gFnSc6	sliznice
dutiny	dutina	k1gFnPc4	dutina
ústní	ústní	k2eAgFnPc4d1	ústní
<g/>
,	,	kIx,	,
patognomické	patognomický	k2eAgFnPc4d1	patognomická
jsou	být	k5eAaImIp3nP	být
krváceniny	krvácenina	k1gFnPc1	krvácenina
v	v	k7c4	v
tunica	tunic	k2eAgNnPc4d1	tunic
media	medium	k1gNnPc4	medium
plicní	plicní	k2eAgFnSc2d1	plicní
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
nekrotické	nekrotický	k2eAgFnPc1d1	nekrotická
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
eroze	eroze	k1gFnSc1	eroze
na	na	k7c6	na
pyscích	pysk	k1gInPc6	pysk
<g/>
,	,	kIx,	,
jazyku	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
dásních	dáseň	k1gFnPc6	dáseň
<g/>
,	,	kIx,	,
hyperemie	hyperemie	k1gFnPc1	hyperemie
bachorových	bachorův	k2eAgInPc2d1	bachorův
pilířů	pilíř	k1gInPc2	pilíř
a	a	k8xC	a
sliznice	sliznice	k1gFnSc2	sliznice
čepce	čepec	k1gInSc2	čepec
<g/>
.	.	kIx.	.
</s>
<s>
Slezina	slezina	k1gFnSc1	slezina
<g/>
,	,	kIx,	,
mízní	mízní	k2eAgFnPc1d1	mízní
uzliny	uzlina	k1gFnPc1	uzlina
a	a	k8xC	a
mandle	mandle	k1gFnPc1	mandle
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zvětšené	zvětšený	k2eAgInPc1d1	zvětšený
a	a	k8xC	a
překrvené	překrvený	k2eAgInPc1d1	překrvený
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
tečkovitými	tečkovitý	k2eAgFnPc7d1	tečkovitá
krváceninami	krvácenina	k1gFnPc7	krvácenina
<g/>
.	.	kIx.	.
</s>
<s>
Krváceniny	Krvácenina	k1gFnPc1	Krvácenina
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
na	na	k7c6	na
kořeni	kořen	k1gInSc6	kořen
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
na	na	k7c6	na
perikardu	perikard	k1gInSc6	perikard
<g/>
,	,	kIx,	,
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
,	,	kIx,	,
střevě	střevo	k1gNnSc6	střevo
a	a	k8xC	a
v	v	k7c6	v
podkoží	podkoží	k1gNnSc6	podkoží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kosterní	kosterní	k2eAgFnSc6d1	kosterní
svalovině	svalovina	k1gFnSc6	svalovina
a	a	k8xC	a
myokardu	myokard	k1gInSc6	myokard
bývají	bývat	k5eAaImIp3nP	bývat
světlé	světlý	k2eAgFnPc4d1	světlá
nekrotické	nekrotický	k2eAgFnPc4d1	nekrotická
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
zánět	zánět	k1gInSc1	zánět
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
,	,	kIx,	,
edém	edém	k1gInSc4	edém
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
pleuritida	pleuritida	k1gFnSc1	pleuritida
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc1	zánět
osrdečníku	osrdečník	k1gInSc2	osrdečník
a	a	k8xC	a
zánět	zánět	k1gInSc4	zánět
střev	střevo	k1gNnPc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Histologicky	histologicky	k6eAd1	histologicky
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
hypertrofie	hypertrofie	k1gFnSc1	hypertrofie
kapilárního	kapilární	k2eAgInSc2d1	kapilární
endotelu	endotel	k1gInSc2	endotel
<g/>
,	,	kIx,	,
perivaskulární	perivaskulární	k2eAgInSc1d1	perivaskulární
edém	edém	k1gInSc1	edém
<g/>
,	,	kIx,	,
žilní	žilní	k2eAgFnPc1d1	žilní
kongesce	kongesce	k1gFnPc1	kongesce
<g/>
,	,	kIx,	,
tkáňové	tkáňový	k2eAgInPc1d1	tkáňový
infarkty	infarkt	k1gInPc1	infarkt
vedoucí	vedoucí	k2eAgInPc1d1	vedoucí
k	k	k7c3	k
hypoxii	hypoxie	k1gFnSc3	hypoxie
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
odlupování	odlupování	k1gNnSc3	odlupování
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kosterní	kosterní	k2eAgFnSc6d1	kosterní
svalovině	svalovina	k1gFnSc6	svalovina
a	a	k8xC	a
myokardu	myokard	k1gInSc6	myokard
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
krváceniny	krvácenina	k1gFnPc4	krvácenina
<g/>
,	,	kIx,	,
nekrózy	nekróza	k1gFnPc4	nekróza
a	a	k8xC	a
okrsky	okrsek	k1gInPc4	okrsek
prostoupené	prostoupený	k2eAgInPc4d1	prostoupený
mononukleárními	mononukleární	k2eAgFnPc7d1	mononukleární
bílými	bílý	k2eAgFnPc7d1	bílá
krvinkami	krvinka	k1gFnPc7	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Předběžná	předběžný	k2eAgFnSc1d1	předběžná
diagnóza	diagnóza	k1gFnSc1	diagnóza
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
epizootologické	epizootologický	k2eAgFnSc2d1	epizootologická
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
patologicko-anatomického	patologickonatomický	k2eAgInSc2d1	patologicko-anatomický
nálezu	nález	k1gInSc2	nález
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
laboratorním	laboratorní	k2eAgNnSc7d1	laboratorní
vyšetřením	vyšetření	k1gNnSc7	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
se	se	k3xPyFc4	se
odebírají	odebírat	k5eAaImIp3nP	odebírat
vzorky	vzorek	k1gInPc1	vzorek
nesrážlivé	srážlivý	k2eNgFnSc2d1	nesrážlivá
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
použití	použití	k1gNnSc3	použití
EDTA	EDTA	kA	EDTA
či	či	k8xC	či
heparinu	heparin	k1gInSc2	heparin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
krevního	krevní	k2eAgNnSc2d1	krevní
séra	sérum	k1gNnSc2	sérum
od	od	k7c2	od
živých	živý	k2eAgNnPc2d1	živé
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
slezina	slezina	k1gFnSc1	slezina
<g/>
,	,	kIx,	,
mízní	mízní	k2eAgFnPc1d1	mízní
uzliny	uzlina	k1gFnPc1	uzlina
<g/>
,	,	kIx,	,
plíce	plíce	k1gFnPc1	plíce
<g/>
,	,	kIx,	,
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
Vzorky	vzorek	k1gInPc1	vzorek
krevního	krevní	k2eAgNnSc2d1	krevní
séra	sérum	k1gNnSc2	sérum
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
zamrazit	zamrazit	k5eAaPmF	zamrazit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
okamžitě	okamžitě	k6eAd1	okamžitě
vyšetřeny	vyšetřen	k2eAgFnPc1d1	vyšetřena
<g/>
.	.	kIx.	.
</s>
<s>
Plnou	plný	k2eAgFnSc4d1	plná
krev	krev	k1gFnSc4	krev
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
uchovávat	uchovávat	k5eAaImF	uchovávat
při	při	k7c6	při
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
orgány	orgán	k1gInPc1	orgán
se	se	k3xPyFc4	se
do	do	k7c2	do
laboratoře	laboratoř	k1gFnSc2	laboratoř
transportují	transportovat	k5eAaBmIp3nP	transportovat
na	na	k7c6	na
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnějším	vhodný	k2eAgNnSc7d3	nejvhodnější
médiem	médium	k1gNnSc7	médium
k	k	k7c3	k
izolaci	izolace	k1gFnSc3	izolace
viru	vir	k1gInSc2	vir
jsou	být	k5eAaImIp3nP	být
kuřecí	kuřecí	k2eAgNnPc1d1	kuřecí
embrya	embryo	k1gNnPc1	embryo
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
devět	devět	k4xCc1	devět
až	až	k8xS	až
dvanáct	dvanáct	k4xCc1	dvanáct
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřovaný	vyšetřovaný	k2eAgInSc1d1	vyšetřovaný
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
inokuluje	inokulovat	k5eAaImIp3nS	inokulovat
intravenózně	intravenózně	k6eAd1	intravenózně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
to	ten	k3xDgNnSc1	ten
značné	značný	k2eAgFnPc1d1	značná
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
katarální	katarální	k2eAgFnSc2d1	katarální
horečky	horečka	k1gFnSc2	horečka
ovcí	ovce	k1gFnPc2	ovce
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
možné	možný	k2eAgNnSc1d1	možné
izolovat	izolovat	k5eAaBmF	izolovat
na	na	k7c6	na
buněčných	buněčný	k2eAgFnPc6d1	buněčná
kulturách	kultura	k1gFnPc6	kultura
(	(	kIx(	(
<g/>
např.	např.	kA	např.
BHK-	BHK-	k1gFnSc1	BHK-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
Vero	Vero	k1gNnSc1	Vero
<g/>
,	,	kIx,	,
hmyzí	hmyzí	k2eAgFnPc1d1	hmyzí
linie	linie	k1gFnPc1	linie
KC	KC	kA	KC
připravené	připravený	k2eAgFnPc1d1	připravená
z	z	k7c2	z
tiplíka	tiplík	k1gMnSc2	tiplík
C.	C.	kA	C.
sonorensis	sonorensis	k1gInSc1	sonorensis
nebo	nebo	k8xC	nebo
linie	linie	k1gFnSc1	linie
AA	AA	kA	AA
připravená	připravený	k2eAgFnSc1d1	připravená
z	z	k7c2	z
komára	komár	k1gMnSc2	komár
Aedes	Aedesa	k1gFnPc2	Aedesa
albopictus	albopictus	k1gMnSc1	albopictus
<g/>
)	)	kIx)	)
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
po	po	k7c6	po
pasáži	pasáž	k1gFnSc6	pasáž
viru	vir	k1gInSc2	vir
na	na	k7c6	na
kuřecích	kuřecí	k2eAgNnPc6d1	kuřecí
embryích	embryo	k1gNnPc6	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
savčích	savčí	k2eAgFnPc6d1	savčí
kulturách	kultura	k1gFnPc6	kultura
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
BTV	BTV	kA	BTV
cytopatický	cytopatický	k2eAgInSc4d1	cytopatický
efekt	efekt	k1gInSc4	efekt
tři	tři	k4xCgNnPc4	tři
až	až	k9	až
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
po	po	k7c6	po
inokulaci	inokulace	k1gFnSc6	inokulace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
izolaci	izolace	k1gFnSc3	izolace
viru	vir	k1gInSc2	vir
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
možné	možný	k2eAgInPc1d1	možný
použít	použít	k5eAaPmF	použít
i	i	k9	i
pokusné	pokusný	k2eAgFnPc4d1	pokusná
ovce	ovce	k1gFnPc4	ovce
<g/>
,	,	kIx,	,
především	především	k9	především
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
virus	virus	k1gInSc4	virus
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
jen	jen	k6eAd1	jen
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgFnSc7d3	nejvhodnější
metodou	metoda	k1gFnSc7	metoda
k	k	k7c3	k
průkazu	průkaz	k1gInSc3	průkaz
virového	virový	k2eAgInSc2d1	virový
antigenu	antigen	k1gInSc2	antigen
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
je	být	k5eAaImIp3nS	být
reverzně-transkripční	reverzněranskripční	k2eAgFnSc1d1	reverzně-transkripční
polymerázová	polymerázový	k2eAgFnSc1d1	polymerázová
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
(	(	kIx(	(
<g/>
RT-PCR	RT-PCR	k1gFnSc1	RT-PCR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
průkaz	průkaz	k1gInSc1	průkaz
virové	virový	k2eAgFnSc2d1	virová
RNA	RNA	kA	RNA
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
předchozí	předchozí	k2eAgFnSc2d1	předchozí
izolace	izolace	k1gFnSc2	izolace
viru	vir	k1gInSc2	vir
a	a	k8xC	a
také	také	k9	také
rozlišení	rozlišení	k1gNnSc3	rozlišení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
sérotypů	sérotyp	k1gInPc2	sérotyp
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
RNA	RNA	kA	RNA
BTV	BTV	kA	BTV
až	až	k9	až
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
viru	vir	k1gInSc2	vir
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stanovit	stanovit	k5eAaPmF	stanovit
kvantitativní	kvantitativní	k2eAgFnSc4d1	kvantitativní
real-time	realimat	k5eAaPmIp3nS	real-timat
RT-PCR	RT-PCR	k1gFnSc4	RT-PCR
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
sérotypově	sérotypově	k6eAd1	sérotypově
specifickým	specifický	k2eAgInSc7d1	specifický
testem	test	k1gInSc7	test
k	k	k7c3	k
průkazu	průkaz	k1gInSc3	průkaz
BTV	BTV	kA	BTV
je	být	k5eAaImIp3nS	být
neutralizační	neutralizační	k2eAgInSc4d1	neutralizační
test	test	k1gInSc4	test
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
skupinově	skupinově	k6eAd1	skupinově
specifické	specifický	k2eAgInPc4d1	specifický
testy	test	k1gInPc4	test
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
antigen-capture	antigenaptur	k1gMnSc5	antigen-captur
ELISA	ELISA	kA	ELISA
<g/>
,	,	kIx,	,
imunospot	imunospot	k1gMnSc1	imunospot
test	test	k1gInSc1	test
či	či	k8xC	či
imunofluorescenční	imunofluorescenční	k2eAgInSc1d1	imunofluorescenční
test	test	k1gInSc1	test
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
využívá	využívat	k5eAaPmIp3nS	využívat
RT-PCR	RT-PCR	k1gFnSc1	RT-PCR
<g/>
.	.	kIx.	.
</s>
<s>
Specifické	specifický	k2eAgFnPc4d1	specifická
protilátky	protilátka	k1gFnPc4	protilátka
proti	proti	k7c3	proti
BTV	BTV	kA	BTV
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
prokázat	prokázat	k5eAaPmF	prokázat
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
séru	sérum	k1gNnSc6	sérum
<g/>
,	,	kIx,	,
plazmě	plazma	k1gFnSc6	plazma
či	či	k8xC	či
mléku	mléko	k1gNnSc6	mléko
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
ELISA	ELISA	kA	ELISA
testů	test	k1gInPc2	test
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
protilátky	protilátka	k1gFnPc4	protilátka
proti	proti	k7c3	proti
virovému	virový	k2eAgInSc3d1	virový
proteinu	protein	k1gInSc3	protein
VP	VP	kA	VP
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
skupinově	skupinově	k6eAd1	skupinově
specifické	specifický	k2eAgFnPc1d1	specifická
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
další	další	k2eAgFnPc1d1	další
metody	metoda	k1gFnPc1	metoda
-	-	kIx~	-
imunodifúzní	imunodifúzní	k2eAgInSc4d1	imunodifúzní
test	test	k1gInSc4	test
nebo	nebo	k8xC	nebo
komplement	komplement	k1gInSc4	komplement
fixační	fixační	k2eAgInSc1d1	fixační
test	test	k1gInSc1	test
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
sérotypy	sérotyp	k1gInPc4	sérotyp
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odlišit	odlišit	k5eAaPmF	odlišit
neutralizačním	neutralizační	k2eAgInSc7d1	neutralizační
testem	test	k1gInSc7	test
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
projevujících	projevující	k2eAgNnPc2d1	projevující
onemocnění	onemocnění	k1gNnPc2	onemocnění
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
odlišit	odlišit	k5eAaPmF	odlišit
příměť	příměť	k1gFnSc4	příměť
pyskovou	pyskový	k2eAgFnSc4d1	pyskový
<g/>
,	,	kIx,	,
slintavku	slintavka	k1gFnSc4	slintavka
a	a	k8xC	a
kulhavku	kulhavka	k1gFnSc4	kulhavka
<g/>
,	,	kIx,	,
vezikulární	vezikulární	k2eAgFnSc4d1	vezikulární
stomatitidu	stomatitida	k1gFnSc4	stomatitida
<g/>
,	,	kIx,	,
akutní	akutní	k2eAgFnSc4d1	akutní
fotosenzitivitu	fotosenzitivita	k1gFnSc4	fotosenzitivita
<g/>
,	,	kIx,	,
akutní	akutní	k2eAgFnSc4d1	akutní
hemonchózu	hemonchóza	k1gFnSc4	hemonchóza
<g/>
,	,	kIx,	,
ekzémy	ekzém	k1gInPc4	ekzém
<g/>
,	,	kIx,	,
napadení	napadení	k1gNnPc4	napadení
střečkem	střeček	k1gMnSc7	střeček
ovčím	ovčí	k2eAgFnPc3d1	ovčí
Oestrus	Oestrus	k1gInSc1	Oestrus
ovis	ovis	k1gInSc4	ovis
<g/>
,	,	kIx,	,
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
otravy	otrava	k1gFnPc1	otrava
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
salmonelózu	salmonelóza	k1gFnSc4	salmonelóza
<g/>
,	,	kIx,	,
neštovice	neštovice	k1gFnPc4	neštovice
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
mor	mor	k1gInSc1	mor
malých	malý	k2eAgMnPc2d1	malý
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
,	,	kIx,	,
mor	mor	k1gInSc4	mor
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
maligní	maligní	k2eAgFnSc4d1	maligní
katarální	katarální	k2eAgFnSc4d1	katarální
horečku	horečka	k1gFnSc4	horečka
<g/>
,	,	kIx,	,
pododermatitidy	pododermatitis	k1gFnPc4	pododermatitis
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgFnSc4d1	infekční
bovinní	bovinní	k2eAgFnSc4d1	bovinní
rhinotracheitidu	rhinotracheitida	k1gFnSc4	rhinotracheitida
<g/>
,	,	kIx,	,
bovinní	bovinní	k2eAgFnSc4d1	bovinní
virovou	virový	k2eAgFnSc4d1	virová
diarrheu	diarrhea	k1gFnSc4	diarrhea
<g/>
,	,	kIx,	,
papulární	papulární	k2eAgFnSc4d1	papulární
stomatitidu	stomatitida	k1gFnSc4	stomatitida
<g/>
,	,	kIx,	,
bovinní	bovinní	k2eAgFnSc4d1	bovinní
herpetickou	herpetický	k2eAgFnSc4d1	herpetická
mamilitidu	mamilitida	k1gFnSc4	mamilitida
a	a	k8xC	a
epizootické	epizootický	k2eAgNnSc4d1	epizootické
hemoragické	hemoragický	k2eAgNnSc4d1	hemoragické
onemocnění	onemocnění	k1gNnSc4	onemocnění
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
.	.	kIx.	.
</s>
<s>
Katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
u	u	k7c2	u
býka	býk	k1gMnSc2	býk
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Cheb	Cheb	k1gInSc1	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008-2009	[number]	k4	2008-2009
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
dalších	další	k2eAgNnPc2d1	další
13	[number]	k4	13
ohnisek	ohnisko	k1gNnPc2	ohnisko
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
nebyla	být	k5eNaImAgFnS	být
KHO	KHO	kA	KHO
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
směrnicí	směrnice	k1gFnSc7	směrnice
Rady	rada	k1gFnSc2	rada
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
75	[number]	k4	75
<g/>
/	/	kIx~	/
<g/>
ES	ES	kA	ES
bylo	být	k5eAaImAgNnS	být
okolo	okolo	k7c2	okolo
každého	každý	k3xTgNnSc2	každý
postiženého	postižený	k2eAgNnSc2d1	postižené
hospodářství	hospodářství	k1gNnSc2	hospodářství
vyhlášeno	vyhlášen	k2eAgNnSc4d1	vyhlášeno
ohnisko	ohnisko	k1gNnSc4	ohnisko
nákazy	nákaza	k1gFnSc2	nákaza
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
20	[number]	k4	20
km	km	kA	km
a	a	k8xC	a
vymezeno	vymezen	k2eAgNnSc1d1	vymezeno
ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
a	a	k8xC	a
pásmo	pásmo	k1gNnSc1	pásmo
dozoru	dozor	k1gInSc2	dozor
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
poloměru	poloměr	k1gInSc6	poloměr
150	[number]	k4	150
km	km	kA	km
(	(	kIx(	(
<g/>
souhrnně	souhrnně	k6eAd1	souhrnně
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
uzavřených	uzavřený	k2eAgNnPc6d1	uzavřené
pásmech	pásmo	k1gNnPc6	pásmo
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
ochranná	ochranný	k2eAgFnSc1d1	ochranná
protinákazová	protinákazový	k2eAgFnSc1d1	protinákazový
opatření	opatření	k1gNnSc1	opatření
stanovená	stanovený	k2eAgFnSc1d1	stanovená
nařízením	nařízení	k1gNnSc7	nařízení
Komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
1266	[number]	k4	1266
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spočívala	spočívat	k5eAaImAgFnS	spočívat
především	především	k6eAd1	především
v	v	k7c6	v
omezení	omezení	k1gNnSc6	omezení
přesunů	přesun	k1gInPc2	přesun
vnímavých	vnímavý	k2eAgNnPc2d1	vnímavé
zvířat	zvíře	k1gNnPc2	zvíře
z	z	k7c2	z
a	a	k8xC	a
do	do	k7c2	do
uzavřených	uzavřený	k2eAgNnPc2d1	uzavřené
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
tlumení	tlumení	k1gNnSc2	tlumení
<g/>
,	,	kIx,	,
sledování	sledování	k1gNnSc2	sledování
a	a	k8xC	a
dozoru	dozor	k1gInSc2	dozor
nad	nad	k7c7	nad
KHO	KHO	kA	KHO
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétněji	konkrétně	k6eAd2	konkrétně
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
r.	r.	kA	r.
2007	[number]	k4	2007
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
probíhalo	probíhat	k5eAaImAgNnS	probíhat
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
krevních	krevní	k2eAgNnPc2d1	krevní
sér	sérum	k1gNnPc2	sérum
vnímavých	vnímavý	k2eAgInPc2d1	vnímavý
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
reprezentativní	reprezentativní	k2eAgInSc1d1	reprezentativní
vzorek	vzorek	k1gInSc1	vzorek
populace	populace	k1gFnSc2	populace
domácích	domácí	k2eAgMnPc2d1	domácí
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
vyšetřoval	vyšetřovat	k5eAaImAgMnS	vyšetřovat
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
BTV	BTV	kA	BTV
(	(	kIx(	(
<g/>
národní	národní	k2eAgFnSc7d1	národní
referenční	referenční	k2eAgFnSc7d1	referenční
laboratoří	laboratoř	k1gFnSc7	laboratoř
pro	pro	k7c4	pro
KHO	KHO	kA	KHO
je	být	k5eAaImIp3nS	být
Státní	státní	k2eAgInSc1d1	státní
veterinární	veterinární	k2eAgInSc1d1	veterinární
ústav	ústav	k1gInSc1	ústav
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2008	[number]	k4	2008
začalo	začít	k5eAaPmAgNnS	začít
entomologické	entomologický	k2eAgNnSc1d1	entomologické
sledování	sledování	k1gNnSc1	sledování
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
byli	být	k5eAaImAgMnP	být
odchytáváni	odchytáván	k2eAgMnPc1d1	odchytáván
tiplíci	tiplík	k1gMnPc1	tiplík
<g/>
,	,	kIx,	,
přenašeči	přenašeč	k1gMnPc1	přenašeč
KHO	KHO	kA	KHO
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
poté	poté	k6eAd1	poté
identifikováni	identifikován	k2eAgMnPc1d1	identifikován
specialisty	specialista	k1gMnPc4	specialista
katedry	katedra	k1gFnSc2	katedra
parazitologie	parazitologie	k1gFnSc2	parazitologie
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zavedena	zaveden	k2eAgFnSc1d1	zavedena
povinná	povinný	k2eAgFnSc1d1	povinná
plošná	plošný	k2eAgFnSc1d1	plošná
nouzová	nouzový	k2eAgFnSc1d1	nouzová
vakcinace	vakcinace	k1gFnSc1	vakcinace
všech	všecek	k3xTgMnPc2	všecek
domácích	domácí	k2eAgMnPc2d1	domácí
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
utlumit	utlumit	k5eAaPmF	utlumit
výskyt	výskyt	k1gInSc4	výskyt
KHO	KHO	kA	KHO
a	a	k8xC	a
usnadnit	usnadnit	k5eAaPmF	usnadnit
chovatelům	chovatel	k1gMnPc3	chovatel
obchodování	obchodování	k1gNnSc4	obchodování
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
prostými	prostý	k2eAgFnPc7d1	prostá
této	tento	k3xDgFnSc2	tento
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Vakcinace	vakcinace	k1gFnSc1	vakcinace
byla	být	k5eAaImAgFnS	být
hrazena	hradit	k5eAaImNgFnS	hradit
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
EU	EU	kA	EU
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
také	také	k9	také
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
probíhalo	probíhat	k5eAaImAgNnS	probíhat
sérologické	sérologický	k2eAgNnSc1d1	sérologické
sledování	sledování	k1gNnSc1	sledování
<g/>
,	,	kIx,	,
vyšetřovala	vyšetřovat	k5eAaImAgFnS	vyšetřovat
se	se	k3xPyFc4	se
však	však	k9	však
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
ověřovací	ověřovací	k2eAgFnSc1d1	ověřovací
(	(	kIx(	(
<g/>
sentinelová	sentinelový	k2eAgFnSc1d1	sentinelová
<g/>
)	)	kIx)	)
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
záměrně	záměrně	k6eAd1	záměrně
vakcinována	vakcinován	k2eAgFnSc1d1	vakcinována
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
2010	[number]	k4	2010
se	se	k3xPyFc4	se
již	již	k6eAd1	již
provádělo	provádět	k5eAaImAgNnS	provádět
pouze	pouze	k6eAd1	pouze
sledování	sledování	k1gNnSc1	sledování
virologické	virologický	k2eAgNnSc1d1	virologické
(	(	kIx(	(
<g/>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
vybraných	vybraný	k2eAgNnPc2d1	vybrané
vnímavých	vnímavý	k2eAgNnPc2d1	vnímavé
zvířat	zvíře	k1gNnPc2	zvíře
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
BTV	BTV	kA	BTV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
příznivé	příznivý	k2eAgFnSc3d1	příznivá
nákazové	nákazový	k2eAgFnSc3d1	nákazová
situaci	situace	k1gFnSc3	situace
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
r.	r.	kA	r.
2011	[number]	k4	2011
povinná	povinný	k2eAgFnSc1d1	povinná
vakcinace	vakcinace	k1gFnSc1	vakcinace
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
byla	být	k5eAaImAgFnS	být
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
<g/>
.37	.37	k4	.37
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
Státní	státní	k2eAgFnSc2d1	státní
veterinární	veterinární	k2eAgFnSc2d1	veterinární
správy	správa	k1gFnSc2	správa
ČR	ČR	kA	ČR
bylo	být	k5eAaImAgNnS	být
dne	den	k1gInSc2	den
25.11	[number]	k4	25.11
<g/>
.2011	.2011	k4	.2011
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
a	a	k8xC	a
od	od	k7c2	od
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
data	datum	k1gNnSc2	datum
je	být	k5eAaImIp3nS	být
ČR	ČR	kA	ČR
zemí	zem	k1gFnPc2	zem
bez	bez	k7c2	bez
výskytu	výskyt	k1gInSc2	výskyt
KHO	KHO	kA	KHO
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
jako	jako	k9	jako
území	území	k1gNnSc4	území
prosté	prostý	k2eAgFnSc2d1	prostá
KHO	KHO	kA	KHO
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
:	:	kIx,	:
žádný	žádný	k3yNgInSc4	žádný
případ	případ	k1gInSc4	případ
nákazy	nákaza	k1gFnSc2	nákaza
a	a	k8xC	a
žádné	žádný	k3yNgNnSc1	žádný
očkování	očkování	k1gNnSc1	očkování
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
24	[number]	k4	24
měsících	měsíc	k1gInPc6	měsíc
s	s	k7c7	s
vhodným	vhodný	k2eAgNnSc7d1	vhodné
tlumením	tlumení	k1gNnSc7	tlumení
populace	populace	k1gFnSc2	populace
Culicoides	Culicoidesa	k1gFnPc2	Culicoidesa
(	(	kIx(	(
<g/>
prováděcí	prováděcí	k2eAgNnSc1d1	prováděcí
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
Komise	komise	k1gFnSc2	komise
č.	č.	k?	č.
253	[number]	k4	253
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
EU	EU	kA	EU
ze	z	k7c2	z
dne	den	k1gInSc2	den
10.5	[number]	k4	10.5
<g/>
.2012	.2012	k4	.2012
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
příloha	příloha	k1gFnSc1	příloha
II	II	kA	II
směrnice	směrnice	k1gFnSc1	směrnice
Rady	rada	k1gFnSc2	rada
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
ES	es	k1gNnPc2	es
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
základní	základní	k2eAgNnPc4d1	základní
obecná	obecný	k2eAgNnPc4d1	obecné
kritéria	kritérion	k1gNnPc4	kritérion
pro	pro	k7c4	pro
území	území	k1gNnSc4	území
považována	považován	k2eAgNnPc4d1	považováno
za	za	k7c7	za
prostá	prostý	k2eAgFnSc1d1	prostá
KHO	KHO	kA	KHO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
případ	případ	k1gInSc1	případ
KHO	KHO	kA	KHO
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
a	a	k8xC	a
povinné	povinný	k2eAgNnSc4d1	povinné
očkování	očkování	k1gNnSc4	očkování
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
dnem	den	k1gInSc7	den
28.4	[number]	k4	28.4
<g/>
.2011	.2011	k4	.2011
<g/>
.	.	kIx.	.
</s>
<s>
Územím	území	k1gNnSc7	území
prostým	prostý	k2eAgNnSc7d1	prosté
bude	být	k5eAaImBp3nS	být
ČR	ČR	kA	ČR
nejdříve	dříve	k6eAd3	dříve
29.4	[number]	k4	29.4
<g/>
.2013	.2013	k4	.2013
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Šperlová	Šperlová	k1gFnSc1	Šperlová
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Zendulková	Zendulková	k1gFnSc1	Zendulková
D.	D.	kA	D.
<g/>
,	,	kIx,	,
Lamka	Lamka	k1gMnSc1	Lamka
J.	J.	kA	J.
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Musíme	muset	k5eAaImIp1nP	muset
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
obávat	obávat	k5eAaImF	obávat
katarální	katarální	k2eAgFnPc4d1	katarální
horečky	horečka	k1gFnPc4	horečka
ovcí	ovce	k1gFnPc2	ovce
<g/>
?	?	kIx.	?
</s>
<s>
Svět	svět	k1gInSc1	svět
myslivosti	myslivost	k1gFnSc2	myslivost
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
s.	s.	k?	s.
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
SVS	SVS	kA	SVS
ČR	ČR	kA	ČR
Poučení	poučení	k1gNnSc4	poučení
o	o	k7c6	o
nákaze	nákaza	k1gFnSc6	nákaza
–	–	k?	–
Katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
(	(	kIx(	(
<g/>
Bluetongue	Bluetongue	k1gFnSc1	Bluetongue
<g/>
)	)	kIx)	)
Katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
-	-	kIx~	-
info	info	k6eAd1	info
SVS	SVS	kA	SVS
12.9	[number]	k4	12.9
<g/>
.2007	.2007	k4	.2007
KVS	KVS	kA	KVS
pro	pro	k7c4	pro
Ústecký	ústecký	k2eAgInSc4d1	ústecký
kraj	kraj	k1gInSc4	kraj
<g/>
:	:	kIx,	:
Informace	informace	k1gFnPc1	informace
–	–	k?	–
Bluetongue	Bluetongu	k1gInSc2	Bluetongu
–	–	k?	–
katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
</s>
