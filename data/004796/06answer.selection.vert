<s>
Katarální	katarální	k2eAgFnSc1d1	katarální
horečka	horečka	k1gFnSc1	horečka
ovcí	ovce	k1gFnPc2	ovce
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Blue	Blue	k1gFnSc1	Blue
tongue	tonguat	k5eAaPmIp3nS	tonguat
disease	disease	k6eAd1	disease
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nekontagiózní	kontagiózní	k2eNgNnSc1d1	kontagiózní
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
domácích	domácí	k1gMnPc2	domácí
a	a	k8xC	a
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
.	.	kIx.	.
</s>
