<s>
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
největší	veliký	k2eAgFnSc7d3	veliký
rybník	rybník	k1gInSc4	rybník
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
určité	určitý	k2eAgFnSc2d1	určitá
definice	definice	k1gFnSc2	definice
rybníku	rybník	k1gInSc3	rybník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
