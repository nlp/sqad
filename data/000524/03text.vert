<s>
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
největší	veliký	k2eAgFnSc7d3	veliký
rybník	rybník	k1gInSc4	rybník
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
určité	určitý	k2eAgFnSc2d1	určitá
definice	definice	k1gFnSc2	definice
rybníku	rybník	k1gInSc3	rybník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
přibližně	přibližně	k6eAd1	přibližně
6	[number]	k4	6
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Třeboně	Třeboň	k1gFnSc2	Třeboň
(	(	kIx(	(
<g/>
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
územím	území	k1gNnSc7	území
Třeboň	Třeboň	k1gFnSc4	Třeboň
II	II	kA	II
<g/>
,	,	kIx,	,
katastru	katastr	k1gInSc2	katastr
města	město	k1gNnSc2	město
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
hráze	hráz	k1gFnSc2	hráz
je	být	k5eAaImIp3nS	být
2430	[number]	k4	2430
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
55	[number]	k4	55
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
13,5	[number]	k4	13,5
m	m	kA	m
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
11,5	[number]	k4	11,5
m.	m.	k?	m.
Rozloha	rozloha	k1gFnSc1	rozloha
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
činí	činit	k5eAaImIp3nP	činit
489	[number]	k4	489
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
výměra	výměra	k1gFnSc1	výměra
činí	činit	k5eAaImIp3nS	činit
677	[number]	k4	677
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
zadržené	zadržený	k2eAgFnSc2d1	zadržená
vody	voda	k1gFnSc2	voda
6,2	[number]	k4	6,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
10	[number]	k4	10
m.	m.	k?	m.
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
426	[number]	k4	426
m.	m.	k?	m.
Lužnice	Lužnice	k1gFnSc2	Lužnice
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
vtéká	vtékat	k5eAaImIp3nS	vtékat
u	u	k7c2	u
Staré	Staré	k2eAgFnSc2d1	Staré
Hlíny	hlína	k1gFnSc2	hlína
a	a	k8xC	a
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
z	z	k7c2	z
rybníka	rybník	k1gInSc2	rybník
vytéká	vytékat	k5eAaImIp3nS	vytékat
ještě	ještě	k9	ještě
kanál	kanál	k1gInSc4	kanál
Potěšilka	Potěšilka	k1gFnSc1	Potěšilka
výpustí	výpust	k1gFnPc2	výpust
Adolfka	Adolfka	k1gFnSc1	Adolfka
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
prudký	prudký	k2eAgInSc1d1	prudký
proud	proud	k1gInSc1	proud
řeky	řeka	k1gFnSc2	řeka
nepoškodil	poškodit	k5eNaPmAgInS	poškodit
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
Nová	nový	k2eAgFnSc1d1	nová
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odvádí	odvádět	k5eAaImIp3nS	odvádět
vody	voda	k1gFnPc4	voda
Lužnice	Lužnice	k1gFnSc2	Lužnice
nad	nad	k7c7	nad
Rožmberkem	Rožmberk	k1gInSc7	Rožmberk
do	do	k7c2	do
Nežárky	Nežárka	k1gFnSc2	Nežárka
<g/>
.	.	kIx.	.
</s>
<s>
Lužnice	Lužnice	k1gFnSc1	Lužnice
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
zdrojnicí	zdrojnice	k1gFnSc7	zdrojnice
tohoto	tento	k3xDgInSc2	tento
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
do	do	k7c2	do
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
vtéká	vtékat	k5eAaImIp3nS	vtékat
potok	potok	k1gInSc1	potok
od	od	k7c2	od
rybníka	rybník	k1gInSc2	rybník
Hodějovského	Hodějovský	k2eAgInSc2d1	Hodějovský
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
u	u	k7c2	u
Smitky	Smitka	k1gFnSc2	Smitka
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dva	dva	k4xCgInPc1	dva
přítoky	přítok	k1gInPc1	přítok
z	z	k7c2	z
rybníku	rybník	k1gInSc3	rybník
rybníku	rybník	k1gInSc6	rybník
Vítek	Vítek	k1gMnSc1	Vítek
<g/>
,	,	kIx,	,
Prostřední	prostřednět	k5eAaImIp3nS	prostřednět
stoka	stoka	k1gFnSc1	stoka
přitékající	přitékající	k2eAgFnSc1d1	přitékající
z	z	k7c2	z
Mokrých	Mokrých	k2eAgFnPc2d1	Mokrých
luk	louka	k1gFnPc2	louka
u	u	k7c2	u
Třeboně	Třeboň	k1gFnSc2	Třeboň
a	a	k8xC	a
přívod	přívod	k1gInSc4	přívod
ze	z	k7c2	z
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgInSc1d1	hlavní
význam	význam	k1gInSc1	význam
při	při	k7c6	při
výlovech	výlov	k1gInPc6	výlov
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
výtoku	výtok	k1gInSc6	výtok
řeky	řeka	k1gFnSc2	řeka
Lužnice	Lužnice	k1gFnSc2	Lužnice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
Francisovými	Francisový	k2eAgFnPc7d1	Francisová
turbínami	turbína	k1gFnPc7	turbína
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
260	[number]	k4	260
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
výpusti	výpust	k1gFnSc6	výpust
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
štola	štola	k1gFnSc1	štola
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
260	[number]	k4	260
×	×	k?	×
260	[number]	k4	260
cm	cm	kA	cm
hrazená	hrazený	k2eAgFnSc1d1	hrazená
dvěma	dva	k4xCgFnPc7	dva
dřevěnými	dřevěný	k2eAgFnPc7d1	dřevěná
tabulemi	tabule	k1gFnPc7	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
obetonované	obetonovaný	k2eAgNnSc4d1	obetonované
potrubí	potrubí	k1gNnSc4	potrubí
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2	[number]	k4	2
m	m	kA	m
<g/>
,	,	kIx,	,
opatřené	opatřený	k2eAgNnSc1d1	opatřené
česlemi	česle	k1gFnPc7	česle
proti	proti	k7c3	proti
úniku	únik	k1gInSc3	únik
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Potrubí	potrubí	k1gNnSc1	potrubí
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zahradit	zahradit	k5eAaPmF	zahradit
i	i	k9	i
pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
v	v	k7c6	v
šachtě	šachta	k1gFnSc6	šachta
další	další	k2eAgFnSc7d1	další
ocelovou	ocelový	k2eAgFnSc7d1	ocelová
tabulí	tabule	k1gFnSc7	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárna	elektrárna	k1gFnSc1	elektrárna
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgMnSc1d1	unikátní
svojí	svůj	k3xOyFgFnSc7	svůj
polohou	poloha	k1gFnSc7	poloha
u	u	k7c2	u
nádrže	nádrž	k1gFnSc2	nádrž
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
navržen	navrhnout	k5eAaPmNgMnS	navrhnout
rožmberským	rožmberský	k2eAgMnSc7d1	rožmberský
regentem	regens	k1gMnSc7	regens
Jakubem	Jakub	k1gMnSc7	Jakub
Krčínem	Krčín	k1gMnSc7	Krčín
z	z	k7c2	z
Jelčan	Jelčan	k1gMnSc1	Jelčan
a	a	k8xC	a
Sedlčan	Sedlčany	k1gInPc2	Sedlčany
a	a	k8xC	a
postaven	postavit	k5eAaPmNgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1584	[number]	k4	1584
až	až	k9	až
1590	[number]	k4	1590
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
provádělo	provádět	k5eAaImAgNnS	provádět
800	[number]	k4	800
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přemístili	přemístit	k5eAaPmAgMnP	přemístit
750	[number]	k4	750
000	[number]	k4	000
m3	m3	k4	m3
zeminy	zemina	k1gFnPc1	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Nutnost	nutnost	k1gFnSc1	nutnost
stavby	stavba	k1gFnSc2	stavba
tohoto	tento	k3xDgNnSc2	tento
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
také	také	k9	také
podpořila	podpořit	k5eAaPmAgFnS	podpořit
velká	velký	k2eAgFnSc1d1	velká
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
20	[number]	k4	20
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1544	[number]	k4	1544
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Čech	Čechy	k1gFnPc2	Čechy
dorazila	dorazit	k5eAaPmAgFnS	dorazit
až	až	k6eAd1	až
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
rybníka	rybník	k1gInSc2	rybník
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
již	již	k9	již
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
Netolický	netolický	k2eAgMnSc1d1	netolický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
velkým	velký	k2eAgInPc3d1	velký
nákladům	náklad	k1gInPc3	náklad
a	a	k8xC	a
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
dílo	dílo	k1gNnSc1	dílo
nemohl	moct	k5eNaImAgInS	moct
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
hráz	hráz	k1gFnSc1	hráz
by	by	kYmCp3nS	by
neodolala	odolat	k5eNaPmAgFnS	odolat
velké	velká	k1gFnSc3	velká
povodni	povodeň	k1gFnSc3	povodeň
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Lužnice	Lužnice	k1gFnSc2	Lužnice
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
to	ten	k3xDgNnSc4	ten
i	i	k9	i
Krčín	Krčín	k1gMnSc1	Krčín
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
současně	současně	k6eAd1	současně
stavěl	stavět	k5eAaImAgMnS	stavět
Novou	nový	k2eAgFnSc4d1	nová
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odváděla	odvádět	k5eAaImAgFnS	odvádět
velkou	velký	k2eAgFnSc4d1	velká
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
povodí	povodí	k1gNnSc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
chtěl	chtít	k5eAaImAgMnS	chtít
Karel	Karel	k1gMnSc1	Karel
Bonaventura	Bonaventura	k1gFnSc1	Bonaventura
Buquoy	Buquoa	k1gFnSc2	Buquoa
<g/>
,	,	kIx,	,
císařský	císařský	k2eAgMnSc1d1	císařský
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
hráz	hráz	k1gFnSc4	hráz
prokopat	prokopat	k5eAaPmF	prokopat
a	a	k8xC	a
zaplavit	zaplavit	k5eAaPmF	zaplavit
stavovské	stavovský	k2eAgNnSc4d1	Stavovské
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
leželo	ležet	k5eAaImAgNnS	ležet
u	u	k7c2	u
Soběslavi	Soběslav	k1gFnSc2	Soběslav
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zde	zde	k6eAd1	zde
držel	držet	k5eAaImAgMnS	držet
majitel	majitel	k1gMnSc1	majitel
třeboňského	třeboňský	k2eAgNnSc2d1	Třeboňské
panství	panství	k1gNnSc2	panství
Petr	Petr	k1gMnSc1	Petr
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
<g/>
,	,	kIx,	,
zabránila	zabránit	k5eAaPmAgFnS	zabránit
katastrofě	katastrofa	k1gFnSc3	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
během	během	k7c2	během
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
války	válka	k1gFnSc2	válka
rybník	rybník	k1gInSc1	rybník
zpustl	zpustnout	k5eAaPmAgInS	zpustnout
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
až	až	k9	až
za	za	k7c4	za
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Hráz	hráz	k1gFnSc1	hráz
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
svými	svůj	k3xOyFgMnPc7	svůj
rozměry	rozměr	k1gInPc4	rozměr
hráze	hráz	k1gFnSc2	hráz
jiných	jiný	k2eAgInPc2d1	jiný
velkých	velký	k2eAgInPc2d1	velký
rybníků	rybník	k1gInPc2	rybník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
2	[number]	k4	2
355	[number]	k4	355
m.	m.	k?	m.
V	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
patě	pata	k1gFnSc6	pata
hráze	hráz	k1gFnSc2	hráz
šíři	šíř	k1gFnSc3	šíř
55	[number]	k4	55
až	až	k9	až
60	[number]	k4	60
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
hráze	hráz	k1gFnSc2	hráz
až	až	k9	až
12	[number]	k4	12
m.	m.	k?	m.
Výška	výška	k1gFnSc1	výška
hráze	hráz	k1gFnSc2	hráz
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
m	m	kA	m
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
hráze	hráz	k1gFnSc2	hráz
průměrně	průměrně	k6eAd1	průměrně
6,5	[number]	k4	6,5
m.	m.	k?	m.
Niveleta	niveleta	k1gFnSc1	niveleta
hráze	hráz	k1gFnSc2	hráz
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
429,4	[number]	k4	429,4
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
po	po	k7c4	po
432,1	[number]	k4	432,1
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
výpust	výpust	k1gFnSc1	výpust
rybníka	rybník	k1gInSc2	rybník
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
inženýrem	inženýr	k1gMnSc7	inženýr
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Šimanem	Šiman	k1gMnSc7	Šiman
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1916	[number]	k4	1916
-	-	kIx~	-
1918	[number]	k4	1918
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
Krčínovy	Krčínův	k2eAgFnSc2d1	Krčínova
výpusti	výpust	k1gFnSc2	výpust
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
Hluboká	hluboký	k2eAgNnPc4d1	hluboké
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
betonový	betonový	k2eAgInSc4d1	betonový
monolit	monolit	k1gInSc4	monolit
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
podkovy	podkova	k1gFnSc2	podkova
obložený	obložený	k2eAgInSc1d1	obložený
lícovanými	lícovaný	k2eAgInPc7d1	lícovaný
kamennými	kamenný	k2eAgInPc7d1	kamenný
kvádry	kvádr	k1gInPc7	kvádr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vyzděny	vyzdít	k5eAaPmNgFnP	vyzdít
dvě	dva	k4xCgFnPc1	dva
odtokové	odtokový	k2eAgFnPc1d1	odtoková
štoly	štola	k1gFnPc1	štola
(	(	kIx(	(
<g/>
160	[number]	k4	160
x	x	k?	x
220	[number]	k4	220
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
hrazeny	hradit	k5eAaImNgInP	hradit
litinovými	litinový	k2eAgFnPc7d1	litinová
lopatami	lopata	k1gFnPc7	lopata
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
pomocí	pomoc	k1gFnPc2	pomoc
elektromotorem	elektromotor	k1gInSc7	elektromotor
nebo	nebo	k8xC	nebo
mechanicky	mechanicky	k6eAd1	mechanicky
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc4	dva
česlové	česlový	k2eAgFnPc4d1	česlový
stěny	stěna	k1gFnPc4	stěna
bránící	bránící	k2eAgFnPc4d1	bránící
úniku	únik	k1gInSc3	únik
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
výpust	výpust	k1gFnSc1	výpust
Adolfka	Adolfka	k1gFnSc1	Adolfka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
poslední	poslední	k2eAgFnSc2d1	poslední
třetiny	třetina	k1gFnSc2	třetina
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
nahradila	nahradit	k5eAaPmAgFnS	nahradit
původní	původní	k2eAgFnSc4d1	původní
výpust	výpust	k1gFnSc4	výpust
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Potěšilka	Potěšilka	k1gFnSc1	Potěšilka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
stoky	stoka	k1gFnSc2	stoka
Potěšilka	Potěšilka	k1gFnSc1	Potěšilka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vychází	vycházet	k5eAaImIp3nS	vycházet
a	a	k8xC	a
napájí	napájet	k5eAaImIp3nS	napájet
řadu	řada	k1gFnSc4	řada
rybníků	rybník	k1gInPc2	rybník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zůstalo	zůstat	k5eAaPmAgNnS	zůstat
zde	zde	k6eAd1	zde
původní	původní	k2eAgMnSc1d1	původní
dřevěného	dřevěný	k2eAgMnSc2d1	dřevěný
potrubí	potrubí	k1gNnSc4	potrubí
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
trouby	trouba	k1gFnPc1	trouba
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
30	[number]	k4	30
×	×	k?	×
60	[number]	k4	60
cm	cm	kA	cm
<g/>
)	)	kIx)	)
opatřené	opatřený	k2eAgNnSc1d1	opatřené
betonovým	betonový	k2eAgNnSc7d1	betonové
zhlavím	zhlaví	k1gNnSc7	zhlaví
kryté	krytý	k2eAgInPc1d1	krytý
dřevěnými	dřevěný	k2eAgFnPc7d1	dřevěná
lopatami	lopata	k1gFnPc7	lopata
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
oprava	oprava	k1gFnSc1	oprava
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
opravě	oprava	k1gFnSc6	oprava
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Krčínovo	Krčínův	k2eAgNnSc1d1	Krčínovo
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
potrubí	potrubí	k1gNnSc1	potrubí
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
400	[number]	k4	400
letech	léto	k1gNnPc6	léto
stále	stále	k6eAd1	stále
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
přeliv	přeliv	k1gInSc1	přeliv
rybníka	rybník	k1gInSc2	rybník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
nejvýchodnější	východní	k2eAgFnSc7d3	nejvýchodnější
částí	část	k1gFnSc7	část
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Přeliv	přeliv	k1gInSc1	přeliv
rozptyluje	rozptylovat	k5eAaImIp3nS	rozptylovat
a	a	k8xC	a
odvádí	odvádět	k5eAaImIp3nS	odvádět
povodeň	povodeň	k1gFnSc4	povodeň
pod	pod	k7c4	pod
hráz	hráz	k1gFnSc4	hráz
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Lužnice	Lužnice	k1gFnSc2	Lužnice
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
vlastního	vlastní	k2eAgInSc2d1	vlastní
přelivu	přeliv	k1gInSc2	přeliv
je	být	k5eAaImIp3nS	být
56,5	[number]	k4	56,5
m.	m.	k?	m.
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hrázi	hráz	k1gFnSc6	hráz
uložené	uložený	k2eAgNnSc4d1	uložené
potrubí	potrubí	k1gNnSc4	potrubí
pro	pro	k7c4	pro
zásobování	zásobování	k1gNnSc4	zásobování
sádek	sádka	k1gFnPc2	sádka
u	u	k7c2	u
Rožmberské	rožmberský	k2eAgFnSc2d1	Rožmberská
bašty	bašta	k1gFnSc2	bašta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odebírat	odebírat	k5eAaImF	odebírat
vodu	voda	k1gFnSc4	voda
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc1	opevnění
hráze	hráz	k1gFnSc2	hráz
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
tvořené	tvořený	k2eAgFnSc6d1	tvořená
dubovými	dubový	k2eAgInPc7d1	dubový
piloty	pilot	k1gInPc7	pilot
<g/>
,	,	kIx,	,
kulatinou	kulatina	k1gFnSc7	kulatina
a	a	k8xC	a
haluzemi	haluz	k1gFnPc7	haluz
jedlí	jedle	k1gFnPc2	jedle
a	a	k8xC	a
smrků	smrk	k1gInPc2	smrk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
neustále	neustále	k6eAd1	neustále
opravováno	opravován	k2eAgNnSc1d1	opravováno
a	a	k8xC	a
vyměňováno	vyměňován	k2eAgNnSc1d1	vyměňováno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Schwarzenbergů	Schwarzenberg	k1gMnPc2	Schwarzenberg
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
tarasem	taras	k1gInSc7	taras
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
taras	taras	k1gInSc1	taras
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
rybníka	rybník	k1gInSc2	rybník
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
způsobila	způsobit	k5eAaPmAgFnS	způsobit
nejen	nejen	k6eAd1	nejen
zaplavení	zaplavení	k1gNnSc4	zaplavení
bývalé	bývalý	k2eAgFnSc2d1	bývalá
tvrze	tvrz	k1gFnSc2	tvrz
(	(	kIx(	(
<g/>
nacházela	nacházet	k5eAaImAgFnS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
dnešní	dnešní	k2eAgFnPc1d1	dnešní
hráze	hráz	k1gFnPc1	hráz
rybníka	rybník	k1gInSc2	rybník
Vítek	Vítek	k1gMnSc1	Vítek
<g/>
)	)	kIx)	)
u	u	k7c2	u
Staré	Staré	k2eAgFnSc2d1	Staré
Hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Moravské	moravský	k2eAgFnSc2d1	Moravská
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
Třeboně	Třeboň	k1gFnSc2	Třeboň
do	do	k7c2	do
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
povozy	povoz	k1gInPc1	povoz
nemusely	muset	k5eNaImAgInP	muset
objíždět	objíždět	k5eAaImF	objíždět
Rožmberk	Rožmberk	k1gInSc4	Rožmberk
přes	přes	k7c4	přes
Lomnici	Lomnice	k1gFnSc4	Lomnice
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
Krčín	Krčín	k1gMnSc1	Krčín
postavit	postavit	k5eAaPmF	postavit
roku	rok	k1gInSc2	rok
1594	[number]	k4	1594
most	most	k1gInSc4	most
z	z	k7c2	z
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
kulatiny	kulatina	k1gFnSc2	kulatina
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
902	[number]	k4	902
m	m	kA	m
přes	přes	k7c4	přes
zátoku	zátoka	k1gFnSc4	zátoka
mezi	mezi	k7c7	mezi
Starou	starý	k2eAgFnSc7d1	stará
a	a	k8xC	a
Novou	nový	k2eAgFnSc7d1	nová
Hlínou	hlína	k1gFnSc7	hlína
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1604	[number]	k4	1604
<g/>
)	)	kIx)	)
ještě	ještě	k6eAd1	ještě
jeden	jeden	k4xCgMnSc1	jeden
jižněji	jižně	k6eAd2	jižně
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Lužnice	Lužnice	k1gFnSc2	Lužnice
do	do	k7c2	do
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1659	[number]	k4	1659
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
most	most	k1gInSc1	most
nahrazen	nahradit	k5eAaPmNgInS	nahradit
pevnějším	pevný	k2eAgNnPc3d2	pevnější
na	na	k7c6	na
dubových	dubový	k2eAgFnPc6d1	dubová
pilotách	pilota	k1gFnPc6	pilota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
povodni	povodeň	k1gFnSc6	povodeň
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
se	se	k3xPyFc4	se
most	most	k1gInSc1	most
doslova	doslova	k6eAd1	doslova
položil	položit	k5eAaPmAgInS	položit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Mosty	most	k1gInPc1	most
byly	být	k5eAaImAgInP	být
poškozovány	poškozovat	k5eAaImNgInP	poškozovat
každou	každý	k3xTgFnSc7	každý
větší	veliký	k2eAgFnSc7d2	veliký
povodní	povodeň	k1gFnSc7	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
za	za	k7c2	za
První	první	k4xOgFnSc2	první
slezské	slezský	k2eAgFnSc2d1	Slezská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
k	k	k7c3	k
Třeboni	Třeboň	k1gFnSc3	Třeboň
obsazené	obsazený	k2eAgFnSc2d1	obsazená
francouzským	francouzský	k2eAgNnSc7d1	francouzské
vojskem	vojsko	k1gNnSc7	vojsko
blížili	blížit	k5eAaImAgMnP	blížit
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
velitel	velitel	k1gMnSc1	velitel
přikázal	přikázat	k5eAaPmAgMnS	přikázat
oba	dva	k4xCgInPc4	dva
mosty	most	k1gInPc4	most
strhnout	strhnout	k5eAaPmF	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dali	dát	k5eAaPmAgMnP	dát
Schwarzenberkové	Schwarzenberek	k1gMnPc1	Schwarzenberek
postavit	postavit	k5eAaPmF	postavit
dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgInPc1d1	nový
kamenné	kamenný	k2eAgInPc1d1	kamenný
mosty	most	k1gInPc1	most
o	o	k7c4	o
5	[number]	k4	5
a	a	k8xC	a
12	[number]	k4	12
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
přetrasována	přetrasován	k2eAgFnSc1d1	přetrasován
silnice	silnice	k1gFnSc1	silnice
E	E	kA	E
551	[number]	k4	551
<g/>
,	,	kIx,	,
sloužily	sloužit	k5eAaImAgInP	sloužit
oba	dva	k4xCgInPc1	dva
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
silniční	silniční	k2eAgFnSc6d1	silniční
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgInSc1d2	kratší
most	most	k1gInSc1	most
přes	přes	k7c4	přes
rybník	rybník	k1gInSc4	rybník
Vítek	Vítek	k1gMnSc1	Vítek
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
oblouků	oblouk	k1gInPc2	oblouk
z	z	k7c2	z
kamenných	kamenný	k2eAgInPc2d1	kamenný
kvádrů	kvádr	k1gInPc2	kvádr
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
má	mít	k5eAaImIp3nS	mít
dvanáct	dvanáct	k4xCc1	dvanáct
oblouků	oblouk	k1gInPc2	oblouk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
letopočtem	letopočet	k1gInSc7	letopočet
1799	[number]	k4	1799
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
mosty	most	k1gInPc1	most
nejspíše	nejspíše	k9	nejspíše
projektoval	projektovat	k5eAaBmAgMnS	projektovat
inženýr	inženýr	k1gMnSc1	inženýr
Rosenauer	Rosenauer	k1gMnSc1	Rosenauer
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Schwarzenbergů	Schwarzenberg	k1gMnPc2	Schwarzenberg
a	a	k8xC	a
vyprojektoval	vyprojektovat	k5eAaPmAgMnS	vyprojektovat
i	i	k9	i
Schwarzenberský	schwarzenberský	k2eAgInSc4d1	schwarzenberský
kanál	kanál	k1gInSc4	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Pětiobloukový	pětiobloukový	k2eAgInSc1d1	pětiobloukový
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
poškozen	poškodit	k5eAaPmNgInS	poškodit
za	za	k7c7	za
povodní	povodeň	k1gFnSc7	povodeň
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
opravit	opravit	k5eAaPmF	opravit
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
most	most	k1gInSc4	most
si	se	k3xPyFc3	se
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
filmaři	filmař	k1gMnPc1	filmař
<g/>
.	.	kIx.	.
</s>
<s>
Natáčely	natáčet	k5eAaImAgFnP	natáčet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohádky	pohádka	k1gFnSc2	pohádka
Byl	být	k5eAaImAgInS	být
jednou	jednou	k6eAd1	jednou
jeden	jeden	k4xCgMnSc1	jeden
král	král	k1gMnSc1	král
a	a	k8xC	a
Z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
odolala	odolat	k5eAaPmAgFnS	odolat
hráz	hráz	k1gFnSc4	hráz
několika	několik	k4yIc3	několik
obrovským	obrovský	k2eAgFnPc3d1	obrovská
povodním	povodeň	k1gFnPc3	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
povodni	povodeň	k1gFnSc6	povodeň
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
zadržoval	zadržovat	k5eAaImAgInS	zadržovat
Rožmberk	Rožmberk	k1gInSc4	Rožmberk
asi	asi	k9	asi
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
m3	m3	k4	m3
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
záplava	záplava	k1gFnSc1	záplava
nastala	nastat	k5eAaPmAgFnS	nastat
během	během	k7c2	během
povodně	povodeň	k1gFnSc2	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
směřoval	směřovat	k5eAaImAgInS	směřovat
kvůli	kvůli	k7c3	kvůli
protržení	protržení	k1gNnSc3	protržení
Novořecké	novořecký	k2eAgFnSc2d1	novořecká
hráze	hráz	k1gFnSc2	hráz
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
odtok	odtok	k1gInSc4	odtok
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
horní	horní	k2eAgFnSc2d1	horní
Lužnice	Lužnice	k1gFnSc2	Lužnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
retenčním	retenční	k2eAgInSc6d1	retenční
prostoru	prostor	k1gInSc6	prostor
bylo	být	k5eAaImAgNnS	být
zadrženo	zadržet	k5eAaPmNgNnS	zadržet
až	až	k9	až
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
m3	m3	k4	m3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
odhadovaný	odhadovaný	k2eAgInSc4d1	odhadovaný
kulminační	kulminační	k2eAgInSc4d1	kulminační
přítok	přítok	k1gInSc4	přítok
700	[number]	k4	700
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
s-	s-	k?	s-
<g/>
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
rybníkem	rybník	k1gInSc7	rybník
transformován	transformovat	k5eAaBmNgInS	transformovat
na	na	k7c4	na
maximální	maximální	k2eAgInSc4d1	maximální
odtok	odtok	k1gInSc4	odtok
270	[number]	k4	270
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
s-	s-	k?	s-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
přepadu	přepad	k1gInSc2	přepad
na	na	k7c6	na
bezpečnostním	bezpečnostní	k2eAgInSc6d1	bezpečnostní
přelivu	přeliv	k1gInSc6	přeliv
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
téměř	téměř	k6eAd1	téměř
tří	tři	k4xCgInPc2	tři
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zatopená	zatopený	k2eAgFnSc1d1	zatopená
plocha	plocha	k1gFnSc1	plocha
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
2300	[number]	k4	2300
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
hladina	hladina	k1gFnSc1	hladina
dotýkala	dotýkat	k5eAaImAgFnS	dotýkat
rybníku	rybník	k1gInSc6	rybník
Ženich	ženich	k1gMnSc1	ženich
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
propojila	propojit	k5eAaPmAgFnS	propojit
s	s	k7c7	s
Kaňovem	Kaňov	k1gInSc7	Kaňov
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
voda	voda	k1gFnSc1	voda
sahala	sahat	k5eAaImAgFnS	sahat
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
od	od	k7c2	od
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
pod	pod	k7c7	pod
Opatovickým	opatovický	k2eAgInSc7d1	opatovický
rybníkem	rybník	k1gInSc7	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
přitom	přitom	k6eAd1	přitom
část	část	k1gFnSc1	část
Třeboně	Třeboň	k1gFnSc2	Třeboň
mezi	mezi	k7c7	mezi
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
čtvrtí	čtvrtit	k5eAaImIp3nS	čtvrtit
Kopeček	kopeček	k1gInSc1	kopeček
pod	pod	k7c7	pod
Kotěrovou	Kotěrový	k2eAgFnSc7d1	Kotěrová
vodárnou	vodárna	k1gFnSc7	vodárna
kolem	kolem	k7c2	kolem
železniční	železniční	k2eAgFnSc2d1	železniční
zastávky	zastávka	k1gFnSc2	zastávka
Třeboň-lázně	Třeboňázeň	k1gFnSc2	Třeboň-lázeň
a	a	k8xC	a
hasičské	hasičský	k2eAgFnSc2d1	hasičská
zbrojnice	zbrojnice	k1gFnSc2	zbrojnice
<g/>
.	.	kIx.	.
</s>
<s>
Kompletně	kompletně	k6eAd1	kompletně
byla	být	k5eAaImAgFnS	být
zatopena	zatopen	k2eAgFnSc1d1	zatopena
i	i	k8xC	i
třeboňská	třeboňský	k2eAgNnPc1d1	Třeboňské
Mokrá	mokrý	k2eAgNnPc1d1	mokré
luka	luka	k1gNnPc1	luka
s	s	k7c7	s
historickými	historický	k2eAgInPc7d1	historický
seníky	seník	k1gInPc7	seník
<g/>
,	,	kIx,	,
známými	známá	k1gFnPc7	známá
i	i	k8xC	i
z	z	k7c2	z
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
povodeň	povodeň	k1gFnSc1	povodeň
následovala	následovat	k5eAaImAgFnS	následovat
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Vody	voda	k1gFnPc1	voda
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
na	na	k7c6	na
Lužnici	Lužnice	k1gFnSc6	Lužnice
napáchala	napáchat	k5eAaBmAgFnS	napáchat
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Rožmberku	Rožmberk	k1gInSc6	Rožmberk
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
hrázi	hráz	k1gFnSc6	hráz
i	i	k8xC	i
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
opraveny	opravit	k5eAaPmNgFnP	opravit
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
mezi	mezi	k7c7	mezi
Rožmberkem	Rožmberk	k1gInSc7	Rožmberk
a	a	k8xC	a
velkou	velký	k2eAgFnSc7d1	velká
vodou	voda	k1gFnSc7	voda
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Povodeň	povodeň	k1gFnSc1	povodeň
šla	jít	k5eAaImAgFnS	jít
Nežárkou	Nežárka	k1gFnSc7	Nežárka
na	na	k7c6	na
Veselí	veselí	k1gNnSc6	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
.	.	kIx.	.
</s>
<s>
Rybáři	rybář	k1gMnPc1	rybář
snížili	snížit	k5eAaPmAgMnP	snížit
odtok	odtok	k1gInSc4	odtok
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
na	na	k7c4	na
minimum	minimum	k1gNnSc4	minimum
<g/>
,	,	kIx,	,
hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
Lužnici	Lužnice	k1gFnSc6	Lužnice
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
evakuaci	evakuace	k1gFnSc3	evakuace
<g/>
.	.	kIx.	.
</s>
<s>
Rožmberská	rožmberský	k2eAgFnSc1d1	Rožmberská
bašta	bašta	k1gFnSc1	bašta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
od	od	k7c2	od
rybníka	rybník	k1gInSc2	rybník
vedle	vedle	k7c2	vedle
sádek	sádka	k1gFnPc2	sádka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
patrovou	patrový	k2eAgFnSc4d1	patrová
zděnou	zděný	k2eAgFnSc4d1	zděná
stavbu	stavba	k1gFnSc4	stavba
se	s	k7c7	s
zachovanou	zachovaný	k2eAgFnSc7d1	zachovaná
renesanční	renesanční	k2eAgFnSc7d1	renesanční
sgrafitovou	sgrafitový	k2eAgFnSc7d1	sgrafitová
omítkou	omítka	k1gFnSc7	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
renesanční	renesanční	k2eAgFnSc7d1	renesanční
stavbou	stavba	k1gFnSc7	stavba
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Výpust	výpust	k1gFnSc1	výpust
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
Samice	samice	k1gFnSc1	samice
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
používala	používat	k5eAaImAgFnS	používat
při	při	k7c6	při
výlovu	výlov	k1gInSc6	výlov
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
patrný	patrný	k2eAgInSc1d1	patrný
obrys	obrys	k1gInSc1	obrys
potrubí	potrubí	k1gNnSc2	potrubí
a	a	k8xC	a
zbytky	zbytek	k1gInPc7	zbytek
kamenných	kamenný	k2eAgInPc2d1	kamenný
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
tímto	tento	k3xDgNnSc7	tento
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hrázi	hráz	k1gFnSc6	hráz
kamenný	kamenný	k2eAgInSc4d1	kamenný
pomníček	pomníček	k1gInSc4	pomníček
s	s	k7c7	s
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
jsou	být	k5eAaImIp3nP	být
vyryti	vyryt	k2eAgMnPc1d1	vyryt
tři	tři	k4xCgMnPc1	tři
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
zakleslí	zakleslý	k2eAgMnPc1d1	zakleslý
kapři	kapr	k1gMnPc1	kapr
a	a	k8xC	a
nápis	nápis	k1gInSc1	nápis
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
ubit	ubit	k2eAgMnSc1d1	ubit
pytlák	pytlák	k1gMnSc1	pytlák
pro	pro	k7c4	pro
tři	tři	k4xCgMnPc4	tři
kapry	kapr	k1gMnPc4	kapr
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dávnou	dávný	k2eAgFnSc4d1	dávná
událost	událost	k1gFnSc4	událost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
střetli	střetnout	k5eAaPmAgMnP	střetnout
rybáři	rybář	k1gMnPc1	rybář
s	s	k7c7	s
pytláky	pytlák	k1gMnPc7	pytlák
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
pytlák	pytlák	k1gMnSc1	pytlák
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
selhání	selhání	k1gNnSc6	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
ale	ale	k8xC	ale
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
vinili	vinit	k5eAaImAgMnP	vinit
rybáře	rybář	k1gMnSc4	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
měl	mít	k5eAaImAgInS	mít
kdysi	kdysi	k6eAd1	kdysi
větší	veliký	k2eAgFnSc4d2	veliký
rozlohu	rozloha	k1gFnSc4	rozloha
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
že	že	k9	že
1000	[number]	k4	1000
<g/>
-	-	kIx~	-
<g/>
1060	[number]	k4	1060
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
končila	končit	k5eAaImAgFnS	končit
přibližně	přibližně	k6eAd1	přibližně
pod	pod	k7c7	pod
Starou	starý	k2eAgFnSc7d1	stará
Hlínou	hlína	k1gFnSc7	hlína
severně	severně	k6eAd1	severně
od	od	k7c2	od
dnešní	dnešní	k2eAgFnSc2d1	dnešní
silnice	silnice	k1gFnSc2	silnice
E	E	kA	E
551	[number]	k4	551
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
rybníka	rybník	k1gInSc2	rybník
sahala	sahat	k5eAaImAgFnS	sahat
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Mokrých	Mokrých	k2eAgFnPc6d1	Mokrých
loukách	louka	k1gFnPc6	louka
zálivem	záliv	k1gInSc7	záliv
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
obchvatu	obchvat	k1gInSc2	obchvat
Třeboně	Třeboň	k1gFnSc2	Třeboň
(	(	kIx(	(
<g/>
třeboňské	třeboňský	k2eAgFnSc2d1	Třeboňská
dálničky	dálnička	k1gFnSc2	dálnička
na	na	k7c4	na
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
Třeboňským	třeboňský	k2eAgInPc3d1	třeboňský
historickým	historický	k2eAgInPc3d1	historický
roubeným	roubený	k2eAgInPc3d1	roubený
seníkům	seník	k1gInPc3	seník
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
hladina	hladina	k1gFnSc1	hladina
kvůli	kvůli	k7c3	kvůli
chovu	chov	k1gInSc3	chov
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
možným	možný	k2eAgFnPc3d1	možná
záplavám	záplava	k1gFnPc3	záplava
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rybníku	rybník	k1gInSc6	rybník
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
zakřivení	zakřivení	k1gNnSc4	zakřivení
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
napříč	napříč	k6eAd1	napříč
přes	přes	k7c4	přes
rybník	rybník	k1gInSc4	rybník
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2,5	[number]	k4	2,5
km	km	kA	km
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
svislici	svislice	k1gFnSc6	svislice
37,6	[number]	k4	37,6
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
necelých	celý	k2eNgFnPc2d1	necelá
40	[number]	k4	40
cm	cm	kA	cm
zmizí	zmizet	k5eAaPmIp3nS	zmizet
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
rybníka	rybník	k1gInSc2	rybník
za	za	k7c7	za
obzorem	obzor	k1gInSc7	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Rybník	rybník	k1gInSc1	rybník
se	se	k3xPyFc4	se
loví	lovit	k5eAaImIp3nS	lovit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
kvůli	kvůli	k7c3	kvůli
Lužnici	Lužnice	k1gFnSc3	Lužnice
hnojit	hnojit	k5eAaImF	hnojit
a	a	k8xC	a
přikrmovat	přikrmovat	k5eAaImF	přikrmovat
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
úlovek	úlovek	k1gInSc1	úlovek
jen	jen	k9	jen
asi	asi	k9	asi
150	[number]	k4	150
tun	tuna	k1gFnPc2	tuna
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
kapra	kapr	k1gMnSc2	kapr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tradiční	tradiční	k2eAgInSc4d1	tradiční
výlov	výlov	k1gInSc4	výlov
vždy	vždy	k6eAd1	vždy
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
připravený	připravený	k2eAgInSc1d1	připravený
bohatý	bohatý	k2eAgInSc1d1	bohatý
kulturní	kulturní	k2eAgInSc1d1	kulturní
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výlov	výlov	k1gInSc4	výlov
tohoto	tento	k3xDgMnSc2	tento
velikána	velikán	k1gMnSc2	velikán
se	se	k3xPyFc4	se
dostavují	dostavovat	k5eAaImIp3nP	dostavovat
i	i	k9	i
turisté	turist	k1gMnPc1	turist
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
výlovu	výlov	k1gInSc6	výlov
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Antonínem	Antonín	k1gMnSc7	Antonín
Švehlou	Švehla	k1gMnSc7	Švehla
<g/>
.	.	kIx.	.
</s>
