<p>
<s>
Hulk	Hulk	k1gInSc1	Hulk
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
akční	akční	k2eAgInSc1d1	akční
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
natočil	natočit	k5eAaBmAgMnS	natočit
Ang	Ang	k1gFnSc4	Ang
Lee	Lea	k1gFnSc3	Lea
podle	podle	k7c2	podle
komiksových	komiksový	k2eAgInPc2d1	komiksový
příběhů	příběh	k1gInPc2	příběh
o	o	k7c6	o
Hulkovi	Hulek	k1gMnSc6	Hulek
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc4d1	titulní
postavu	postava	k1gFnSc4	postava
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
Eric	Eric	k1gFnSc4	Eric
Bana	Ban	k1gInSc2	Ban
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
amerických	americký	k2eAgNnPc2d1	americké
kin	kino	k1gNnPc2	kino
byl	být	k5eAaImAgMnS	být
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rozpočet	rozpočet	k1gInSc1	rozpočet
činil	činit	k5eAaImAgInS	činit
137	[number]	k4	137
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
uveden	uveden	k2eAgInSc1d1	uveden
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
celosvětově	celosvětově	k6eAd1	celosvětově
utržil	utržit	k5eAaPmAgInS	utržit
245	[number]	k4	245
360	[number]	k4	360
480	[number]	k4	480
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
reboot	reboot	k1gInSc1	reboot
Neuvěřitelný	uvěřitelný	k2eNgInSc1d1	neuvěřitelný
Hulk	Hulk	k1gInSc1	Hulk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
filmové	filmový	k2eAgFnSc2d1	filmová
série	série	k1gFnSc2	série
Marvel	Marvela	k1gFnPc2	Marvela
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Genetik	genetik	k1gMnSc1	genetik
Bruce	Bruce	k1gMnSc1	Bruce
Krenzler	Krenzler	k1gMnSc1	Krenzler
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kolegyní	kolegyně	k1gFnSc7	kolegyně
a	a	k8xC	a
bývalou	bývalý	k2eAgFnSc7d1	bývalá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Betty	Betty	k1gFnSc7	Betty
Rossovou	Rossová	k1gFnSc7	Rossová
na	na	k7c6	na
výzkumu	výzkum	k1gInSc6	výzkum
nanomedů	nanomed	k1gMnPc2	nanomed
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
nízké	nízký	k2eAgFnSc6d1	nízká
dávce	dávka	k1gFnSc6	dávka
záření	záření	k1gNnSc2	záření
gama	gama	k1gNnSc2	gama
opravovat	opravovat	k5eAaImF	opravovat
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
však	však	k9	však
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
nehodě	nehoda	k1gFnSc3	nehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
Bruce	Bruce	k1gMnSc1	Bruce
vystaven	vystavit	k5eAaPmNgMnS	vystavit
značnému	značný	k2eAgNnSc3d1	značné
množství	množství	k1gNnSc3	množství
gama	gama	k1gNnSc2	gama
radiace	radiace	k1gFnSc2	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
incidentu	incident	k1gInSc3	incident
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
ve	v	k7c4	v
velké	velký	k2eAgNnSc4d1	velké
zelené	zelený	k2eAgNnSc4d1	zelené
monstrum	monstrum	k1gNnSc4	monstrum
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
se	se	k3xPyFc4	se
rozzuří	rozzuřit	k5eAaPmIp3nS	rozzuřit
<g/>
.	.	kIx.	.
</s>
<s>
Setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
biologickým	biologický	k2eAgMnSc7d1	biologický
otcem	otec	k1gMnSc7	otec
Davidem	David	k1gMnSc7	David
Bannerem	banner	k1gInSc7	banner
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
prováděl	provádět	k5eAaImAgInS	provádět
na	na	k7c4	na
sobě	se	k3xPyFc3	se
tajné	tajný	k2eAgInPc4d1	tajný
vojenské	vojenský	k2eAgInPc4d1	vojenský
pokusy	pokus	k1gInPc4	pokus
a	a	k8xC	a
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
armádou	armáda	k1gFnSc7	armáda
zadržován	zadržovat	k5eAaImNgMnS	zadržovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bruceovi	Bruceus	k1gMnSc6	Bruceus
však	však	k9	však
jde	jít	k5eAaImIp3nS	jít
i	i	k9	i
generál	generál	k1gMnSc1	generál
Thaddeus	Thaddeus	k1gMnSc1	Thaddeus
Ross	Ross	k1gInSc1	Ross
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Betty	Betty	k1gFnSc2	Betty
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kdysi	kdysi	k6eAd1	kdysi
vedl	vést	k5eAaImAgMnS	vést
Davidův	Davidův	k2eAgInSc4d1	Davidův
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Eric	Eric	k6eAd1	Eric
Bana	Bana	k1gMnSc1	Bana
jako	jako	k8xC	jako
Bruce	Bruce	k1gMnSc1	Bruce
Krenzler	Krenzler	k1gMnSc1	Krenzler
/	/	kIx~	/
Bruce	Bruce	k1gFnSc1	Bruce
Banner	banner	k1gInSc1	banner
/	/	kIx~	/
Hulk	Hulk	k1gInSc1	Hulk
</s>
</p>
<p>
<s>
Jennifer	Jennifer	k1gInSc1	Jennifer
Connelly	Connella	k1gFnSc2	Connella
jako	jako	k8xS	jako
Betty	Betty	k1gFnSc1	Betty
Rossová	Rossová	k1gFnSc1	Rossová
</s>
</p>
<p>
<s>
Sam	Sam	k1gMnSc1	Sam
Elliott	Elliott	k1gMnSc1	Elliott
jako	jako	k8xC	jako
generál	generál	k1gMnSc1	generál
Thunderbolt	Thunderbolt	k2eAgInSc4d1	Thunderbolt
Ross	Ross	k1gInSc4	Ross
</s>
</p>
<p>
<s>
Josh	Josh	k1gMnSc1	Josh
Lucas	Lucas	k1gMnSc1	Lucas
jako	jako	k8xC	jako
major	major	k1gMnSc1	major
Glenn	Glenn	k1gMnSc1	Glenn
Talbot	Talbot	k1gMnSc1	Talbot
</s>
</p>
<p>
<s>
Nick	Nick	k6eAd1	Nick
Nolte	Nolt	k1gInSc5	Nolt
jako	jako	k8xS	jako
David	David	k1gMnSc1	David
Banner	banner	k1gInSc4	banner
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Hulk	Hulk	k6eAd1	Hulk
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hulk	Hulk	k6eAd1	Hulk
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
</s>
</p>
