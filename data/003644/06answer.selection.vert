<s>
Šachovou	šachový	k2eAgFnSc4d1	šachová
soupravu	souprava	k1gFnSc4	souprava
tvoří	tvořit	k5eAaImIp3nP	tvořit
šachovnice	šachovnice	k1gFnPc1	šachovnice
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
sady	sada	k1gFnPc1	sada
kamenů	kámen	k1gInPc2	kámen
–	–	k?	–
světlých	světlý	k2eAgInPc2d1	světlý
(	(	kIx(	(
<g/>
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
"	"	kIx"	"
<g/>
bílé	bílé	k1gNnSc1	bílé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
tmavých	tmavý	k2eAgMnPc2d1	tmavý
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
černé	černý	k2eAgFnPc1d1	černá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
