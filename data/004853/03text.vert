<s>
Isao	Isao	k1gMnSc1	Isao
Takahata	Takahat	k1gMnSc2	Takahat
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
高	高	k?	高
<g/>
,	,	kIx,	,
Takahata	Takahata	k1gFnSc1	Takahata
Isao	Isao	k1gMnSc1	Isao
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1935	[number]	k4	1935
v	v	k7c6	v
Udžijamadě	Udžijamada	k1gFnSc6	Udžijamada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
a	a	k8xC	a
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
tvůrců	tvůrce	k1gMnPc2	tvůrce
japonských	japonský	k2eAgInPc2d1	japonský
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
–	–	k?	–
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
Spoluzaložil	spoluzaložit	k5eAaPmAgMnS	spoluzaložit
s	s	k7c7	s
Hajao	Hajao	k1gMnSc1	Hajao
Mijazakim	Mijazakim	k1gMnSc1	Mijazakim
animátorské	animátorský	k2eAgNnSc4d1	animátorské
studio	studio	k1gNnSc4	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
také	také	k9	také
produkovalo	produkovat	k5eAaImAgNnS	produkovat
jeho	jeho	k3xOp3gInPc4	jeho
nejznámější	známý	k2eAgInPc4d3	nejznámější
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
animované	animovaný	k2eAgInPc1d1	animovaný
filmy	film	k1gInPc1	film
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
neobyčejně	obyčejně	k6eNd1	obyčejně
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
témat	téma	k1gNnPc2	téma
-	-	kIx~	-
jenom	jenom	k6eAd1	jenom
pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
studia	studio	k1gNnSc2	studio
Ghibli	Ghibli	k1gMnPc2	Ghibli
vytvoři	vytvoř	k1gFnSc3	vytvoř
válečný	válečný	k2eAgInSc4d1	válečný
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
Hrob	hrob	k1gInSc1	hrob
světlušek	světluška	k1gFnPc2	světluška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psychologické	psychologický	k2eAgNnSc4d1	psychologické
romantické	romantický	k2eAgNnSc4d1	romantické
drama	drama	k1gNnSc4	drama
(	(	kIx(	(
<g/>
Střípky	střípek	k1gInPc1	střípek
minulosti	minulost	k1gFnSc2	minulost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
komedii	komedie	k1gFnSc4	komedie
(	(	kIx(	(
<g/>
Mí	můj	k3xOp1gMnPc1	můj
sousedé	soused	k1gMnPc1	soused
Jamadovi	Jamadův	k2eAgMnPc1d1	Jamadův
<g/>
)	)	kIx)	)
a	a	k8xC	a
ekologický	ekologický	k2eAgInSc1d1	ekologický
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
Pom	Pom	k1gMnSc1	Pom
poko	poko	k1gMnSc1	poko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
Hrob	hrob	k1gInSc1	hrob
světlušek	světluška	k1gFnPc2	světluška
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
popularity	popularita	k1gFnSc2	popularita
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
kladných	kladný	k2eAgMnPc2d1	kladný
ohlasů	ohlas	k1gInPc2	ohlas
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
jej	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Isao	Isao	k6eAd1	Isao
Takahata	Takahe	k1gNnPc4	Takahe
graduoval	graduovat	k5eAaBmAgMnS	graduovat
na	na	k7c6	na
Tokijské	tokijský	k2eAgFnSc6d1	Tokijská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
nově	nově	k6eAd1	nově
vytvořeného	vytvořený	k2eAgNnSc2d1	vytvořené
animátorského	animátorský	k2eAgNnSc2d1	animátorské
studia	studio	k1gNnSc2	studio
Tóei	Tóe	k1gFnSc2	Tóe
dóga	dóg	k1gInSc2	dóg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
dlouhodobým	dlouhodobý	k2eAgMnSc7d1	dlouhodobý
kolegou	kolega	k1gMnSc7	kolega
Mijazakim	Mijazaki	k1gNnSc7	Mijazaki
a	a	k8xC	a
kde	kde	k6eAd1	kde
také	také	k9	také
režíroval	režírovat	k5eAaImAgMnS	režírovat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
Taijó	Taijó	k1gFnSc2	Taijó
no	no	k9	no
ódži	ódži	k6eAd1	ódži
<g/>
:	:	kIx,	:
Horus	Horus	k1gInSc1	Horus
no	no	k9	no
daibóken	daibóken	k1gInSc1	daibóken
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Horus	Horus	k1gMnSc1	Horus
<g/>
:	:	kIx,	:
Princ	princ	k1gMnSc1	princ
slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
umělecky	umělecky	k6eAd1	umělecky
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
finančně	finančně	k6eAd1	finančně
selhal	selhat	k5eAaPmAgInS	selhat
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mijazakim	Mijazaki	k1gNnSc7	Mijazaki
studio	studio	k1gNnSc4	studio
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
společných	společný	k2eAgInPc6d1	společný
projektech	projekt	k1gInPc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
režisérů	režisér	k1gMnPc2	režisér
anime	animat	k5eAaPmIp3nS	animat
Takahata	Takahat	k2eAgFnSc1d1	Takahat
nekreslí	kreslit	k5eNaImIp3nS	kreslit
a	a	k8xC	a
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
režisérem	režisér	k1gMnSc7	režisér
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nepracoval	pracovat	k5eNaImAgMnS	pracovat
jako	jako	k9	jako
animátor	animátor	k1gMnSc1	animátor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
není	být	k5eNaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
rozšíříte	rozšířit	k5eAaPmIp2nP	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Taijó	Taijó	k?	Taijó
no	no	k9	no
ódži	ódži	k1gNnSc1	ódži
<g/>
:	:	kIx,	:
Horus	Horus	k1gInSc1	Horus
no	no	k9	no
daibóken	daibóken	k1gInSc1	daibóken
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Panda	panda	k1gFnSc1	panda
kopanda	kopanda	k1gFnSc1	kopanda
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Panda	panda	k1gFnSc1	panda
kopanda	kopando	k1gNnSc2	kopando
<g/>
:	:	kIx,	:
Amefuri	Amefuri	k1gNnSc2	Amefuri
Circus	Circus	k1gInSc4	Circus
no	no	k9	no
maki	maki	k6eAd1	maki
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Džarinko	Džarinka	k1gFnSc5	Džarinka
Čie	Čie	k1gMnSc6	Čie
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Sero-hiki	Seroiki	k1gNnPc7	Sero-hiki
no	no	k9	no
Gauche	Gauche	k1gNnSc1	Gauche
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Janagawa	Janagaw	k1gInSc2	Janagaw
horiwari	horiwar	k1gFnSc2	horiwar
<g />
.	.	kIx.	.
</s>
<s>
monogatari	monogatari	k1gNnSc1	monogatari
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
–	–	k?	–
hraný	hraný	k2eAgInSc4d1	hraný
dokument	dokument	k1gInSc4	dokument
Hrob	hrob	k1gInSc1	hrob
světlušek	světluška	k1gFnPc2	světluška
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Střípky	střípek	k1gInPc1	střípek
minulosti	minulost	k1gFnSc2	minulost
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Pom	Pom	k1gMnSc1	Pom
poko	poko	k1gMnSc1	poko
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Mí	můj	k3xOp1gMnPc1	můj
sousedé	soused	k1gMnPc1	soused
Jamadovi	Jamadův	k2eAgMnPc1d1	Jamadův
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Fuju	Fujus	k1gInSc2	Fujus
no	no	k9	no
hi	hi	k0	hi
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
35	[number]	k4	35
<g />
.	.	kIx.	.
</s>
<s>
režisérů	režisér	k1gMnPc2	režisér
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
Kaguja-hime	Kagujaim	k1gInSc5	Kaguja-him
no	no	k9	no
monogatari	monogatar	k1gMnSc3	monogatar
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Naušika	Naušika	k1gFnSc1	Naušika
z	z	k7c2	z
Větrného	větrný	k2eAgNnSc2d1	větrné
údolí	údolí	k1gNnSc2	údolí
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Laputa	Laput	k2eAgFnSc1d1	Laputa
<g/>
:	:	kIx,	:
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Umi	Umi	k1gFnSc3	Umi
ga	ga	k?	ga
kikoeru	kikoer	k1gInSc2	kikoer
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Isao	Isao	k6eAd1	Isao
Takahata	Takahata	k1gFnSc1	Takahata
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Isao	Isao	k6eAd1	Isao
Takahata	Takahata	k1gFnSc1	Takahata
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Isao	Isao	k6eAd1	Isao
Takahata	Takahat	k2eAgFnSc1d1	Takahat
na	na	k7c4	na
Nausicaa	Nausicaus	k1gMnSc4	Nausicaus
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
-	-	kIx~	-
recenze	recenze	k1gFnSc1	recenze
<g/>
,	,	kIx,	,
filmografie	filmografie	k1gFnSc1	filmografie
Isao	Isao	k6eAd1	Isao
Takahata	Takahat	k2eAgFnSc1d1	Takahat
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
