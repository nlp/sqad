<p>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
týden	týden	k1gInSc1	týden
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
označuje	označovat	k5eAaImIp3nS	označovat
zhuštění	zhuštění	k1gNnSc1	zhuštění
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
týmy	tým	k1gInPc1	tým
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
ke	k	k7c3	k
dvěma	dva	k4xCgFnPc7	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
zápasům	zápas	k1gInPc3	zápas
během	během	k7c2	během
jediného	jediné	k1gNnSc2	jediné
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
je	být	k5eAaImIp3nS	být
viděn	vidět	k5eAaImNgInS	vidět
často	často	k6eAd1	často
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
celé	celý	k2eAgFnSc2d1	celá
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
použit	použít	k5eAaPmNgInS	použít
v	v	k7c4	v
anglické	anglický	k2eAgInPc4d1	anglický
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gFnSc2	Leagu
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
náročného	náročný	k2eAgInSc2d1	náročný
programu	program	k1gInSc2	program
anglických	anglický	k2eAgMnPc2d1	anglický
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgInPc1d1	anglický
týdny	týden	k1gInPc1	týden
musí	muset	k5eAaImIp3nP	muset
kluby	klub	k1gInPc4	klub
podstupovat	podstupovat	k5eAaImF	podstupovat
pro	pro	k7c4	pro
urychlení	urychlení	k1gNnSc4	urychlení
průběhu	průběh	k1gInSc2	průběh
soutěže	soutěž	k1gFnSc2	soutěž
Premier	Premira	k1gFnPc2	Premira
League	Leagu	k1gFnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgInPc1d1	anglický
týdny	týden	k1gInPc1	týden
však	však	k9	však
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
evropských	evropský	k2eAgFnPc6d1	Evropská
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
soutěže	soutěž	k1gFnPc4	soutěž
více	hodně	k6eAd2	hodně
než	než	k8xS	než
18	[number]	k4	18
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgNnSc1d1	nutné
zahrávat	zahrávat	k5eAaImF	zahrávat
více	hodně	k6eAd2	hodně
utkání	utkání	k1gNnPc4	utkání
v	v	k7c6	v
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
