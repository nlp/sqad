<s>
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1810	[number]	k4	1810
Praha-Malá	Praha-Malý	k2eAgFnSc1d1	Praha-Malá
Strana	strana	k1gFnSc1	strana
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1836	[number]	k4	1836
Litoměřice	Litoměřice	k1gInPc4	Litoměřice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
českého	český	k2eAgInSc2d1	český
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
české	český	k2eAgFnSc2d1	Česká
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
