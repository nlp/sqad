<p>
<s>
Televize	televize	k1gFnSc1	televize
Seznam	seznam	k1gInSc1	seznam
(	(	kIx(	(
<g/>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
TV	TV	kA	TV
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
komerční	komerční	k2eAgFnSc1d1	komerční
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
nabízející	nabízející	k2eAgFnSc1d1	nabízející
celoplošné	celoplošný	k2eAgNnSc4d1	celoplošné
vysílání	vysílání	k1gNnSc4	vysílání
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
Jakub	Jakub	k1gMnSc1	Jakub
Unger	Unger	k1gMnSc1	Unger
<g/>
,	,	kIx,	,
provozovatelem	provozovatel	k1gMnSc7	provozovatel
firma	firma	k1gFnSc1	firma
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Iva	Ivo	k1gMnSc4	Ivo
Lukačoviče	Lukačovič	k1gMnSc4	Lukačovič
<g/>
.	.	kIx.	.
</s>
<s>
Vysílat	vysílat	k5eAaImF	vysílat
začala	začít	k5eAaPmAgFnS	začít
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
v	v	k7c4	v
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
a	a	k8xC	a
publicistiku	publicistika	k1gFnSc4	publicistika
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
ale	ale	k8xC	ale
i	i	k8xC	i
pořady	pořad	k1gInPc1	pořad
vlastní	vlastní	k2eAgFnSc2d1	vlastní
tvorby	tvorba	k1gFnSc2	tvorba
vysílané	vysílaný	k2eAgFnPc1d1	vysílaná
původně	původně	k6eAd1	původně
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
Zprávy	zpráva	k1gFnSc2	zpráva
nebo	nebo	k8xC	nebo
na	na	k7c6	na
internetové	internetový	k2eAgFnSc6d1	internetová
televizi	televize	k1gFnSc6	televize
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Program	program	k1gInSc1	program
==	==	k?	==
</s>
</p>
<p>
<s>
Televize	televize	k1gFnSc1	televize
Seznam	seznam	k1gInSc1	seznam
je	být	k5eAaImIp3nS	být
především	především	k9	především
zpravodajskou	zpravodajský	k2eAgFnSc7d1	zpravodajská
stanicí	stanice	k1gFnSc7	stanice
nabízející	nabízející	k2eAgFnPc1d1	nabízející
zprávy	zpráva	k1gFnPc1	zpráva
z	z	k7c2	z
domova	domov	k1gInSc2	domov
i	i	k8xC	i
zahraničí	zahraničí	k1gNnSc2	zahraničí
jak	jak	k8xS	jak
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
tak	tak	k6eAd1	tak
na	na	k7c6	na
webu	web	k1gInSc6	web
Seznam	seznam	k1gInSc1	seznam
Zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
štáby	štáb	k1gInPc1	štáb
natáčely	natáčet	k5eAaImAgInP	natáčet
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nabízí	nabízet	k5eAaImIp3nS	nabízet
publicistické	publicistický	k2eAgInPc4d1	publicistický
pořady	pořad	k1gInPc4	pořad
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
představuje	představovat	k5eAaImIp3nS	představovat
silné	silný	k2eAgFnSc2d1	silná
osobnosti	osobnost	k1gFnSc2	osobnost
společenského	společenský	k2eAgInSc2d1	společenský
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
i	i	k8xC	i
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zábavnou	zábavný	k2eAgFnSc4d1	zábavná
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
zábavných	zábavný	k2eAgInPc2d1	zábavný
formátů	formát	k1gInPc2	formát
je	být	k5eAaImIp3nS	být
satirický	satirický	k2eAgInSc1d1	satirický
souhrn	souhrn	k1gInSc1	souhrn
Šťastné	Šťastné	k2eAgNnSc2d1	Šťastné
pondělí	pondělí	k1gNnSc2	pondělí
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Šídla	Šídlo	k1gMnSc4	Šídlo
<g/>
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
televize	televize	k1gFnSc1	televize
vysílala	vysílat	k5eAaImAgFnS	vysílat
finále	finále	k1gNnSc4	finále
Česká	český	k2eAgFnSc1d1	Česká
Miss	miss	k1gFnSc1	miss
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
seriály	seriál	k1gInPc1	seriál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
politicko-satirický	politickoatirický	k2eAgInSc4d1	politicko-satirický
seriál	seriál	k1gInSc4	seriál
Kancelář	kancelář	k1gFnSc1	kancelář
Blaník	Blaník	k1gInSc1	Blaník
nebo	nebo	k8xC	nebo
průvodce	průvodce	k1gMnSc1	průvodce
Prahou	Praha	k1gFnSc7	Praha
Honest	Honest	k1gMnSc1	Honest
Guide	Guid	k1gInSc5	Guid
Janka	Janka	k1gFnSc1	Janka
Rubeše	Rubeš	k1gMnSc2	Rubeš
a	a	k8xC	a
Honzy	Honza	k1gMnSc2	Honza
Mikulky	Mikulka	k1gMnSc2	Mikulka
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
na	na	k7c6	na
internetové	internetový	k2eAgFnSc6d1	internetová
televizi	televize	k1gFnSc6	televize
Stream	Stream	k1gInSc4	Stream
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
televize	televize	k1gFnSc2	televize
představila	představit	k5eAaPmAgFnS	představit
nové	nový	k2eAgNnSc4d1	nové
vysílací	vysílací	k2eAgNnSc4d1	vysílací
schéma	schéma	k1gNnSc4	schéma
pro	pro	k7c4	pro
podzim	podzim	k1gInSc4	podzim
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Vysílání	vysílání	k1gNnSc1	vysílání
doplní	doplnit	k5eAaPmIp3nS	doplnit
nový	nový	k2eAgInSc4d1	nový
formát	formát	k1gInSc4	formát
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
byznysu	byznys	k1gInSc2	byznys
Studio	studio	k1gNnSc1	studio
Byznys	byznys	k1gInSc1	byznys
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc1d1	sportovní
show	show	k1gFnSc1	show
trojice	trojice	k1gFnSc2	trojice
moderátorů	moderátor	k1gMnPc2	moderátor
Luďka	Luďka	k1gFnSc1	Luďka
Mádla	mádlo	k1gNnPc1	mádlo
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc4	Jiří
Hoška	Hošek	k1gMnSc4	Hošek
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Šídla	Šídlo	k1gMnSc4	Šídlo
<g/>
.	.	kIx.	.
</s>
<s>
Víkendové	víkendový	k2eAgNnSc1d1	víkendové
vysílání	vysílání	k1gNnSc1	vysílání
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
páteční	páteční	k2eAgInSc4d1	páteční
blok	blok	k1gInSc4	blok
nezávislých	závislý	k2eNgInPc2d1	nezávislý
filmů	film	k1gInPc2	film
a	a	k8xC	a
nedělní	nedělní	k2eAgInSc4d1	nedělní
blok	blok	k1gInSc4	blok
seriálů	seriál	k1gInPc2	seriál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vysílané	vysílaný	k2eAgInPc1d1	vysílaný
pořady	pořad	k1gInPc1	pořad
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Dostupnost	dostupnost	k1gFnSc4	dostupnost
==	==	k?	==
</s>
</p>
<p>
<s>
Vysílání	vysílání	k1gNnSc4	vysílání
Televize	televize	k1gFnSc2	televize
Seznam	seznam	k1gInSc4	seznam
lze	lze	k6eAd1	lze
naladit	naladit	k5eAaPmF	naladit
v	v	k7c6	v
multiplexu	multiplex	k1gInSc6	multiplex
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
95,7	[number]	k4	95,7
%	%	kIx~	%
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Přechodové	přechodový	k2eAgFnSc6d1	přechodová
síti	síť	k1gFnSc6	síť
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
nabídky	nabídka	k1gFnSc2	nabídka
většiny	většina	k1gFnSc2	většina
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
satelitního	satelitní	k2eAgMnSc2d1	satelitní
a	a	k8xC	a
kabelového	kabelový	k2eAgInSc2d1	kabelový
televizního	televizní	k2eAgInSc2d1	televizní
signálu	signál	k1gInSc2	signál
Digi	Dig	k1gMnPc1	Dig
TV	TV	kA	TV
<g/>
,	,	kIx,	,
Skylink	Skylink	k1gInSc1	Skylink
a	a	k8xC	a
UPC	UPC	kA	UPC
<g/>
.	.	kIx.	.
</s>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
IPTV	IPTV	kA	IPTV
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
O2	O2	k1gMnPc1	O2
TV	TV	kA	TV
<g/>
,	,	kIx,	,
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
TV	TV	kA	TV
nebo	nebo	k8xC	nebo
Kuki	Kuk	k1gMnPc1	Kuk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
šíří	šířit	k5eAaImIp3nP	šířit
televizní	televizní	k2eAgInSc4d1	televizní
signál	signál	k1gInSc4	signál
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Televize	televize	k1gFnSc1	televize
Seznam	seznam	k1gInSc1	seznam
</s>
</p>
<p>
<s>
Televize	televize	k1gFnSc1	televize
Seznam	seznam	k1gInSc1	seznam
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Televize	televize	k1gFnSc1	televize
Seznam	seznam	k1gInSc1	seznam
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
</s>
</p>
