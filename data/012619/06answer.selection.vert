<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
převážně	převážně	k6eAd1	převážně
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnPc1d1	modrá
nebo	nebo	k8xC	nebo
žluté	žlutý	k2eAgFnPc1d1	žlutá
stěny	stěna	k1gFnPc1	stěna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ty	ten	k3xDgMnPc4	ten
navozují	navozovat	k5eAaImIp3nP	navozovat
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
pocity	pocit	k1gInPc4	pocit
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
