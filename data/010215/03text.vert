<p>
<s>
James	James	k1gMnSc1	James
Mallahan	Mallahan	k1gMnSc1	Mallahan
Cain	Cain	k1gMnSc1	Cain
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1892	[number]	k4	1892
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Cain	Cain	k1gMnSc1	Cain
striktně	striktně	k6eAd1	striktně
odmítal	odmítat	k5eAaImAgMnS	odmítat
"	"	kIx"	"
<g/>
škatulkování	škatulkování	k?	škatulkování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojován	spojován	k2eAgInSc1d1	spojován
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
drsnou	drsný	k2eAgFnSc7d1	drsná
školou	škola	k1gFnSc7	škola
amerického	americký	k2eAgInSc2d1	americký
detektivního	detektivní	k2eAgInSc2d1	detektivní
románu	román	k1gInSc2	román
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
hardboiled	hardboiled	k1gInSc1	hardboiled
school	school	k1gInSc1	school
of	of	k?	of
American	American	k1gInSc1	American
crime	crimat	k5eAaPmIp3nS	crimat
fiction	fiction	k1gInSc1	fiction
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
tvůrců	tvůrce	k1gMnPc2	tvůrce
románu	román	k1gInSc2	román
noir	noir	k1gMnSc1	noir
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
jeho	jeho	k3xOp3gInPc2	jeho
detektivních	detektivní	k2eAgInPc2d1	detektivní
románů	román	k1gInPc2	román
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
předlohou	předloha	k1gFnSc7	předloha
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
hollywoodských	hollywoodský	k2eAgInPc2d1	hollywoodský
filmů	film	k1gInPc2	film
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pošťák	pošťák	k1gMnSc1	pošťák
vždy	vždy	k6eAd1	vždy
zvoní	zvonit	k5eAaImIp3nS	zvonit
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
Mildred	Mildred	k1gInSc1	Mildred
Pierceová	Pierceová	k1gFnSc1	Pierceová
nebo	nebo	k8xC	nebo
Pojistka	pojistka	k1gFnSc1	pojistka
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Our	Our	k?	Our
Government	Government	k1gInSc1	Government
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Postman	Postman	k1gMnSc1	Postman
Always	Alwaysa	k1gFnPc2	Alwaysa
Rings	Rings	k1gInSc1	Rings
Twice	Twice	k1gMnSc1	Twice
(	(	kIx(	(
<g/>
Pošťák	pošťák	k1gMnSc1	pošťák
vždy	vždy	k6eAd1	vždy
zvoní	zvonit	k5eAaImIp3nS	zvonit
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Serenade	Serenást	k5eAaPmIp3nS	Serenást
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mildred	Mildred	k1gMnSc1	Mildred
Pierce	Pierce	k1gMnSc1	Pierce
(	(	kIx(	(
<g/>
Mildred	Mildred	k1gMnSc1	Mildred
Pierceová	Pierceová	k1gFnSc1	Pierceová
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Love	lov	k1gInSc5	lov
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lovely	Lovel	k1gMnPc7	Lovel
Counterfeit	Counterfeita	k1gFnPc2	Counterfeita
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Career	Career	k1gInSc1	Career
in	in	k?	in
C	C	kA	C
Major	major	k1gMnSc1	major
and	and	k?	and
Other	Other	k1gMnSc1	Other
Stories	Stories	k1gMnSc1	Stories
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Double	double	k2eAgFnPc1d1	double
Indemnity	Indemnita	k1gFnPc1	Indemnita
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Pojistka	pojistka	k1gFnSc1	pojistka
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
publikováno	publikovat	k5eAaBmNgNnS	publikovat
v	v	k7c4	v
Liberty	Libert	k1gMnPc4	Libert
Magazine	Magazin	k1gInSc5	Magazin
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Embezzler	Embezzler	k1gInSc1	Embezzler
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
publikováno	publikovat	k5eAaBmNgNnS	publikovat
jako	jako	k8xS	jako
Money	Monea	k1gFnPc4	Monea
and	and	k?	and
the	the	k?	the
Woman	Woman	k1gInSc1	Woman
v	v	k7c4	v
Liberty	Libert	k1gMnPc4	Libert
Magazine	Magazin	k1gInSc5	Magazin
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Past	past	k1gFnSc1	past
All	All	k1gMnSc1	All
Dishonor	Dishonor	k1gMnSc1	Dishonor
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Butterfly	butterfly	k1gInSc1	butterfly
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Moth	Moth	k1gInSc1	Moth
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sinful	Sinful	k1gInSc1	Sinful
Woman	Woman	k1gInSc1	Woman
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jealous	Jealous	k1gMnSc1	Jealous
Woman	Woman	k1gMnSc1	Woman
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Root	Root	k1gInSc1	Root
of	of	k?	of
His	his	k1gNnSc1	his
Evil	Evil	k1gMnSc1	Evil
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
publikováno	publikovat	k5eAaBmNgNnS	publikovat
také	také	k9	také
jako	jako	k9	jako
Shameless	Shameless	k1gInSc1	Shameless
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galatea	Galatea	k1gFnSc1	Galatea
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mignon	Mignon	k1gNnSc1	Mignon
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Magician	Magician	k1gInSc1	Magician
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wife	Wife	k1gFnSc7	Wife
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rainbow	Rainbow	k?	Rainbow
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
End	End	k1gFnSc7	End
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Institute	institut	k1gInSc5	institut
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Baby	baby	k1gNnSc1	baby
in	in	k?	in
the	the	k?	the
Icebox	Icebox	k1gInSc1	Icebox
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
krátké	krátký	k2eAgFnSc2d1	krátká
povídky	povídka	k1gFnSc2	povídka
</s>
</p>
<p>
<s>
Cloud	Cloud	k1gInSc1	Cloud
Nine	Nin	k1gFnSc2	Nin
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Enchanted	Enchanted	k1gInSc1	Enchanted
Isle	Isle	k1gFnSc1	Isle
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Cocktail	cocktail	k1gInSc1	cocktail
Waitress	Waitress	k1gInSc1	Waitress
(	(	kIx(	(
<g/>
editace	editace	k1gFnSc1	editace
<g/>
:	:	kIx,	:
Charles	Charles	k1gMnSc1	Charles
Ardai	Arda	k1gFnSc2	Arda
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Raymond	Raymond	k1gMnSc1	Raymond
Chandler	Chandler	k1gMnSc1	Chandler
</s>
</p>
<p>
<s>
Dashiell	Dashiell	k1gMnSc1	Dashiell
Hammett	Hammett	k1gMnSc1	Hammett
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
noir	noira	k1gFnPc2	noira
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
James	Jamesa	k1gFnPc2	Jamesa
M.	M.	kA	M.
Cain	Cain	k1gInSc1	Cain
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
