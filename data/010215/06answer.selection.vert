<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Cain	Cain	k1gMnSc1	Cain
striktně	striktně	k6eAd1	striktně
odmítal	odmítat	k5eAaImAgMnS	odmítat
"	"	kIx"	"
<g/>
škatulkování	škatulkování	k?	škatulkování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojován	spojován	k2eAgInSc1d1	spojován
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
drsnou	drsný	k2eAgFnSc7d1	drsná
školou	škola	k1gFnSc7	škola
amerického	americký	k2eAgInSc2d1	americký
detektivního	detektivní	k2eAgInSc2d1	detektivní
románu	román	k1gInSc2	román
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
hardboiled	hardboiled	k1gInSc1	hardboiled
school	school	k1gInSc1	school
of	of	k?	of
American	American	k1gInSc1	American
crime	crimat	k5eAaPmIp3nS	crimat
fiction	fiction	k1gInSc1	fiction
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
tvůrců	tvůrce	k1gMnPc2	tvůrce
románu	román	k1gInSc2	román
noir	noir	k1gMnSc1	noir
<g/>
.	.	kIx.	.
</s>
