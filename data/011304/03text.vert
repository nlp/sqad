<p>
<s>
Černé	Černé	k2eAgNnSc1d1	Černé
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schwarzer	Schwarzer	k1gMnSc1	Schwarzer
See	See	k1gMnSc1	See
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepočítáme	počítat	k5eNaImIp1nP	počítat
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
18,47	[number]	k4	18,47
hektaru	hektar	k1gInSc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1008	[number]	k4	1008
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
39,8	[number]	k4	39,8
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnPc1d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
šest	šest	k4xCc1	šest
kilometrů	kilometr	k1gInPc2	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Železné	železný	k2eAgFnSc2d1	železná
Rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
kilometr	kilometr	k1gInSc4	kilometr
od	od	k7c2	od
česko-německé	českoěmecký	k2eAgFnSc2d1	česko-německá
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
severním	severní	k2eAgInSc7d1	severní
svahem	svah	k1gInSc7	svah
Jezerní	jezerní	k2eAgFnSc2d1	jezerní
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
zvedá	zvedat	k5eAaImIp3nS	zvedat
320	[number]	k4	320
metrů	metr	k1gInPc2	metr
vysokou	vysoký	k2eAgFnSc7d1	vysoká
Jezerní	jezerní	k2eAgFnSc7d1	jezerní
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
karová	karový	k2eAgNnPc4d1	karové
jezera	jezero	k1gNnPc4	jezero
vyhloubená	vyhloubený	k2eAgNnPc4d1	vyhloubené
ledovcem	ledovec	k1gInSc7	ledovec
v	v	k7c6	v
období	období	k1gNnSc6	období
würmského	würmský	k2eAgNnSc2d1	würmský
zalednění	zalednění	k1gNnSc2	zalednění
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
odrazem	odraz	k1gInSc7	odraz
tmavých	tmavý	k2eAgInPc2d1	tmavý
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
ho	on	k3xPp3gNnSc4	on
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
jezera	jezero	k1gNnSc2	jezero
tvoří	tvořit	k5eAaImIp3nS	tvořit
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
asi	asi	k9	asi
devět	devět	k4xCc4	devět
metrů	metr	k1gInPc2	metr
tlustá	tlustý	k2eAgFnSc1d1	tlustá
vrstva	vrstva	k1gFnSc1	vrstva
kalu	kal	k1gInSc2	kal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
pyl	pyl	k1gInSc1	pyl
z	z	k7c2	z
okolních	okolní	k2eAgInPc2d1	okolní
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
ukládající	ukládající	k2eAgNnPc1d1	ukládající
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
po	po	k7c4	po
tisíce	tisíc	k4xCgInPc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
jihu	jih	k1gInSc2	jih
se	se	k3xPyFc4	se
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
vlévají	vlévat	k5eAaImIp3nP	vlévat
dva	dva	k4xCgInPc4	dva
přítoky	přítok	k1gInPc4	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
odtéká	odtékat	k5eAaImIp3nS	odtékat
Černým	černý	k2eAgInSc7d1	černý
potokem	potok	k1gInSc7	potok
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Úhlavy	Úhlava	k1gFnSc2	Úhlava
<g/>
.	.	kIx.	.
</s>
<s>
Průhlednost	průhlednost	k1gFnSc1	průhlednost
vody	voda	k1gFnSc2	voda
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubky	hloubka	k1gFnSc2	hloubka
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
obvykle	obvykle	k6eAd1	obvykle
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
až	až	k8xS	až
května	květen	k1gInSc2	květen
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
ledu	led	k1gInSc2	led
bývá	bývat	k5eAaImIp3nS	bývat
až	až	k9	až
75	[number]	k4	75
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
Černého	Černého	k2eAgNnSc2d1	Černého
i	i	k8xC	i
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
Čertova	čertův	k2eAgNnSc2d1	Čertovo
jezera	jezero	k1gNnSc2	jezero
začala	začít	k5eAaPmAgFnS	začít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
součástí	součást	k1gFnPc2	součást
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Černé	Černá	k1gFnSc2	Černá
a	a	k8xC	a
Čertovo	čertův	k2eAgNnSc1d1	Čertovo
jezero	jezero	k1gNnSc1	jezero
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
208,46	[number]	k4	208,46
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
roste	růst	k5eAaImIp3nS	růst
šídlatka	šídlatka	k1gFnSc1	šídlatka
jezerní	jezerní	k2eAgFnSc1d1	jezerní
a	a	k8xC	a
rašeliník	rašeliník	k1gInSc1	rašeliník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fytoplanktonu	fytoplankton	k1gInSc6	fytoplankton
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgFnPc1d1	zastoupena
obrněnky	obrněnka	k1gFnPc1	obrněnka
Peridinium	Peridinium	k1gNnSc1	Peridinium
umbonatum	umbonatum	k1gNnSc1	umbonatum
a	a	k8xC	a
Gymnodinium	Gymnodinium	k1gNnSc1	Gymnodinium
uberrimum	uberrimum	k1gNnSc1	uberrimum
a	a	k8xC	a
zlativka	zlativka	k1gFnSc1	zlativka
Dinobryon	Dinobryona	k1gFnPc2	Dinobryona
pediforme	pediform	k1gInSc5	pediform
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zooplanktonu	zooplankton	k1gInSc2	zooplankton
pak	pak	k6eAd1	pak
perloočka	perloočka	k1gFnSc1	perloočka
Ceriodaphnia	Ceriodaphnium	k1gNnSc2	Ceriodaphnium
quadrangula	quadrangulum	k1gNnSc2	quadrangulum
a	a	k8xC	a
larvy	larva	k1gFnPc1	larva
chrostíka	chrostík	k1gMnSc2	chrostík
Molanna	Molann	k1gMnSc2	Molann
nigra	nigr	k1gMnSc2	nigr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tajemné	tajemný	k2eAgNnSc1d1	tajemné
místo	místo	k1gNnSc1	místo
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
české	český	k2eAgMnPc4d1	český
umělce	umělec	k1gMnPc4	umělec
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
−	−	k?	−
Romance	romance	k1gFnSc2	romance
o	o	k7c6	o
Černém	černý	k2eAgNnSc6d1	černé
jezeře	jezero	k1gNnSc6	jezero
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Heyduk	Heyduk	k1gMnSc1	Heyduk
−	−	k?	−
U	u	k7c2	u
horského	horský	k2eAgNnSc2d1	horské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
−	−	k?	−
Černé	Černé	k2eAgNnSc1d1	Černé
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
−	−	k?	−
cyklus	cyklus	k1gInSc1	cyklus
Ze	z	k7c2	z
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
také	také	k9	také
opředeno	opředen	k2eAgNnSc1d1	opředeno
mnoha	mnoho	k4c7	mnoho
pověstmi	pověst	k1gFnPc7	pověst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tady	tady	k6eAd1	tady
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
zinscenovala	zinscenovat	k5eAaPmAgFnS	zinscenovat
aféru	aféra	k1gFnSc4	aféra
s	s	k7c7	s
nálezem	nález	k1gInSc7	nález
tajných	tajný	k2eAgInPc2d1	tajný
dokumentů	dokument	k1gInPc2	dokument
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1964	[number]	k4	1964
ve	v	k7c4	v
3.10	[number]	k4	3.10
hodin	hodina	k1gFnPc2	hodina
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
akce	akce	k1gFnSc2	akce
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Neptun	Neptun	k1gInSc4	Neptun
uložila	uložit	k5eAaPmAgFnS	uložit
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
bedny	bedna	k1gFnSc2	bedna
s	s	k7c7	s
dokumenty	dokument	k1gInPc7	dokument
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1964	[number]	k4	1964
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Lubomír	Lubomír	k1gMnSc1	Lubomír
Štrougal	Štrougal	k1gMnSc1	Štrougal
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
o	o	k7c4	o
akci	akce	k1gFnSc4	akce
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
nález	nález	k1gInSc4	nález
na	na	k7c6	na
mimořádné	mimořádný	k2eAgFnSc6d1	mimořádná
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Nalezené	nalezený	k2eAgFnPc1d1	nalezená
listiny	listina	k1gFnPc1	listina
měly	mít	k5eAaImAgFnP	mít
dokazovat	dokazovat	k5eAaImF	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
jsou	být	k5eAaImIp3nP	být
prorostlé	prorostlý	k2eAgInPc1d1	prorostlý
sítí	síť	k1gFnPc2	síť
přisluhovačů	přisluhovač	k1gMnPc2	přisluhovač
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Uloženy	uložen	k2eAgInPc1d1	uložen
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
byly	být	k5eAaImAgInP	být
pravé	pravý	k2eAgInPc1d1	pravý
dokumenty	dokument	k1gInPc1	dokument
z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
archivů	archiv	k1gInPc2	archiv
a	a	k8xC	a
také	také	k9	také
prázdné	prázdný	k2eAgInPc4d1	prázdný
papíry	papír	k1gInPc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
KGB	KGB	kA	KGB
slíbila	slíbit	k5eAaPmAgFnS	slíbit
dodat	dodat	k5eAaPmF	dodat
ty	ten	k3xDgInPc4	ten
nejzásadnější	zásadní	k2eAgInPc4d3	nejzásadnější
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
zpozdila	zpozdit	k5eAaPmAgFnS	zpozdit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
akce	akce	k1gFnSc1	akce
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
bez	bez	k7c2	bez
nich	on	k3xPp3gNnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ukládání	ukládání	k1gNnSc6	ukládání
pokladu	poklad	k1gInSc2	poklad
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
i	i	k9	i
"	"	kIx"	"
<g/>
soudruh	soudruh	k1gMnSc1	soudruh
Brychta	Brychta	k1gMnSc1	Brychta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Ladislav	Ladislav	k1gMnSc1	Ladislav
Bittman	Bittman	k1gMnSc1	Bittman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
infiltrován	infiltrovat	k5eAaBmNgMnS	infiltrovat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
sportovních	sportovní	k2eAgMnPc2d1	sportovní
potápěčů	potápěč	k1gMnPc2	potápěč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
poklad	poklad	k1gInSc1	poklad
nalezla	nalézt	k5eAaBmAgFnS	nalézt
<g/>
;	;	kIx,	;
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
bedny	bedna	k1gFnPc1	bedna
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
byla	být	k5eAaImAgFnS	být
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
nálezem	nález	k1gInSc7	nález
desítek	desítka	k1gFnPc2	desítka
beden	bedna	k1gFnPc2	bedna
s	s	k7c7	s
falešnými	falešný	k2eAgFnPc7d1	falešná
anglickými	anglický	k2eAgFnPc7d1	anglická
librami	libra	k1gFnPc7	libra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
vyzdviženy	vyzdvihnout	k5eAaPmNgFnP	vyzdvihnout
z	z	k7c2	z
rakouského	rakouský	k2eAgNnSc2d1	rakouské
jezera	jezero	k1gNnSc2	jezero
Toplitzsee	Toplitzsee	k1gNnSc2	Toplitzsee
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Úhlavy	Úhlava	k1gFnSc2	Úhlava
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
až	až	k9	až
1930	[number]	k4	1930
postavena	postavit	k5eAaPmNgFnS	postavit
naše	náš	k3xOp1gFnSc1	náš
první	první	k4xOgFnSc1	první
přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
Peltonovy	Peltonův	k2eAgFnPc4d1	Peltonova
turbíny	turbína	k1gFnPc4	turbína
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
kW	kW	kA	kW
a	a	k8xC	a
370	[number]	k4	370
kW	kW	kA	kW
<g/>
)	)	kIx)	)
a	a	k8xC	a
horizontální	horizontální	k2eAgFnSc1d1	horizontální
průtočná	průtočný	k2eAgFnSc1d1	průtočná
Kaplanova	Kaplanův	k2eAgFnSc1d1	Kaplanova
turbína	turbína	k1gFnSc1	turbína
(	(	kIx(	(
<g/>
40	[number]	k4	40
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
bylo	být	k5eAaImAgNnS	být
jezero	jezero	k1gNnSc1	jezero
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
do	do	k7c2	do
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
pásma	pásmo	k1gNnSc2	pásmo
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnSc2	veřejnost
nepřístupné	přístupný	k2eNgFnSc2d1	nepřístupná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
stráže	stráž	k1gFnSc2	stráž
opět	opět	k6eAd1	opět
zpřístupněno	zpřístupnit	k5eAaPmNgNnS	zpřístupnit
<g/>
.	.	kIx.	.
</s>
<s>
Sezónně	sezónně	k6eAd1	sezónně
zde	zde	k6eAd1	zde
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
na	na	k7c4	na
počátku	počátek	k1gInSc2	počátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc6	století
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
i	i	k8xC	i
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
ČSAD	ČSAD	kA	ČSAD
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgInS	být
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
využíván	využívat	k5eAaPmNgInS	využívat
například	například	k6eAd1	například
autobus	autobus	k1gInSc1	autobus
z	z	k7c2	z
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
autobus	autobus	k1gInSc1	autobus
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
RTO	RTO	kA	RTO
městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
Železné	železný	k2eAgFnSc6d1	železná
Rudě	ruda	k1gFnSc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Provozování	provozování	k1gNnSc1	provozování
linky	linka	k1gFnSc2	linka
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zvýšením	zvýšení	k1gNnSc7	zvýšení
režimu	režim	k1gInSc2	režim
ochrany	ochrana	k1gFnSc2	ochrana
zdejší	zdejší	k2eAgFnSc2d1	zdejší
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
linku	linka	k1gFnSc4	linka
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
439040	[number]	k4	439040
od	od	k7c2	od
parkoviště	parkoviště	k1gNnSc2	parkoviště
ve	v	k7c6	v
špičáckém	špičácký	k2eAgNnSc6d1	špičácký
sedle	sedlo	k1gNnSc6	sedlo
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
páry	pár	k1gInPc1	pár
spojů	spoj	k1gInPc2	spoj
denně	denně	k6eAd1	denně
prováděnými	prováděný	k2eAgFnPc7d1	prováděná
plynovým	plynový	k2eAgInSc7d1	plynový
autobusem	autobus	k1gInSc7	autobus
SOR	SOR	kA	SOR
CN	CN	kA	CN
12	[number]	k4	12
EKOBUS	EKOBUS	kA	EKOBUS
obnovila	obnovit	k5eAaPmAgFnS	obnovit
ČSAD	ČSAD	kA	ČSAD
autobusy	autobus	k1gInPc4	autobus
Plzeň	Plzeň	k1gFnSc1	Plzeň
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
města	město	k1gNnSc2	město
Železná	železný	k2eAgFnSc1d1	železná
Ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
tarif	tarif	k1gInSc1	tarif
a	a	k8xC	a
přepravní	přepravní	k2eAgFnPc1d1	přepravní
podmínky	podmínka	k1gFnPc1	podmínka
vyhlášené	vyhlášený	k2eAgFnPc1d1	vyhlášená
městem	město	k1gNnSc7	město
Železná	železný	k2eAgFnSc1d1	železná
Ruda	ruda	k1gFnSc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc4	provoz
linky	linka	k1gFnSc2	linka
umožnila	umožnit	k5eAaPmAgFnS	umožnit
výjimka	výjimka	k1gFnSc1	výjimka
a	a	k8xC	a
podmínky	podmínka	k1gFnSc2	podmínka
stanovené	stanovený	k2eAgFnSc2d1	stanovená
Správou	správa	k1gFnSc7	správa
CHKO	CHKO	kA	CHKO
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
platná	platný	k2eAgFnSc1d1	platná
po	po	k7c4	po
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
na	na	k7c4	na
období	období	k1gNnSc4	období
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
jednalo	jednat	k5eAaImAgNnS	jednat
s	s	k7c7	s
CHKO	CHKO	kA	CHKO
o	o	k7c6	o
zavedení	zavedení	k1gNnSc6	zavedení
dopravy	doprava	k1gFnSc2	doprava
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
jednalo	jednat	k5eAaImAgNnS	jednat
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
dopravcem	dopravce	k1gMnSc7	dopravce
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
turistického	turistický	k2eAgInSc2d1	turistický
vláčku	vláček	k1gInSc2	vláček
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
záměr	záměr	k1gInSc1	záměr
však	však	k9	však
zatím	zatím	k6eAd1	zatím
odsunulo	odsunout	k5eAaPmAgNnS	odsunout
na	na	k7c4	na
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
kolem	kolem	k7c2	kolem
jezera	jezero	k1gNnSc2	jezero
vede	vést	k5eAaImIp3nS	vést
pěší	pěší	k2eAgFnSc1d1	pěší
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
hřebenová	hřebenový	k2eAgFnSc1d1	hřebenová
trasa	trasa	k1gFnSc1	trasa
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
od	od	k7c2	od
Malého	malé	k1gNnSc2	malé
Špičáku	špičák	k1gInSc2	špičák
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
žlutě	žlutě	k6eAd1	žlutě
značená	značený	k2eAgFnSc1d1	značená
pěší	pěší	k2eAgFnSc1d1	pěší
trasa	trasa	k1gFnSc1	trasa
a	a	k8xC	a
cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
hřebenová	hřebenový	k2eAgFnSc1d1	hřebenová
trasa	trasa	k1gFnSc1	trasa
2055	[number]	k4	2055
ze	z	k7c2	z
Špičáckého	Špičácký	k2eAgNnSc2d1	Špičácké
sedla	sedlo	k1gNnSc2	sedlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ottova	Ottův	k2eAgFnSc1d1	Ottova
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
456	[number]	k4	456
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DAVID	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
SOUKUP	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
999	[number]	k4	999
turistických	turistický	k2eAgFnPc2d1	turistická
zajímavostí	zajímavost	k1gFnPc2	zajímavost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kartografie	kartografie	k1gFnSc1	kartografie
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
392	[number]	k4	392
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7011	[number]	k4	7011
<g/>
-	-	kIx~	-
<g/>
656	[number]	k4	656
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Černé	Černé	k2eAgFnSc2d1	Černé
jezero	jezero	k1gNnSc4	jezero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Černé	Černá	k1gFnSc2	Černá
a	a	k8xC	a
Čertovo	čertův	k2eAgNnSc1d1	Čertovo
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
