<s>
Černé	Černé	k2eAgNnSc1d1	Černé
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schwarzer	Schwarzer	k1gMnSc1	Schwarzer
See	See	k1gMnSc1	See
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepočítáme	počítat	k5eNaImIp1nP	počítat
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
