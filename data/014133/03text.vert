<s>
Stol	stol	k1gInSc1
(	(	kIx(
<g/>
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
StolHochstuhl	StolHochstuhnout	k5eAaPmAgInS
Stol	stol	k1gInSc1
od	od	k7c2
severovýchodu	severovýchod	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
2	#num#	k4
236	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
1	#num#	k4
022	#num#	k4
m	m	kA
↓	↓	k?
Seebergsattel	Seebergsattel	k1gInSc1
Izolace	izolace	k1gFnSc1
</s>
<s>
22,1	22,1	k4
km	km	kA
→	→	k?
Rjavina	Rjavin	k2eAgFnSc1d1
Poznámka	poznámka	k1gFnSc1
</s>
<s>
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Karavanek	Karavanky	k1gFnPc2
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
SlovinskoRakousko	SlovinskoRakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Karavanky	Karavanky	k1gFnPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
2	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
26	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Stol	stol	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stol	stol	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
Veliki	Velik	k2eAgFnSc1d1
Stol	stol	k1gInSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Hochstuhl	Hochstuhl	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
236	#num#	k4
m	m	kA
n.	n.	kA
m.	m.	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Karavanek	Karavanky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
přímo	přímo	k6eAd1
na	na	k7c4
státní	státní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
Slovinskem	Slovinsko	k1gNnSc7
a	a	k8xC
Rakouskem	Rakousko	k1gNnSc7
(	(	kIx(
<g/>
Korutany	Korutany	k1gInPc7
<g/>
)	)	kIx)
asi	asi	k9
9	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Radovljice	Radovljice	k1gFnSc2
a	a	k8xC
14	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Ferlachu	Ferlach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrchol	vrchol	k1gInSc4
lze	lze	k6eAd1
vystoupit	vystoupit	k5eAaPmF
buď	buď	k8xC
z	z	k7c2
rakouské	rakouský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
z	z	k7c2
obce	obec	k1gFnSc2
Feistritz	Feistritz	k1gMnSc1
im	im	k?
Rosental	Rosental	k1gMnSc1
či	či	k8xC
od	od	k7c2
chaty	chata	k1gFnSc2
Klagenfurter	Klagenfurtra	k1gFnPc2
Hütte	Hütt	k1gInSc5
(	(	kIx(
<g/>
1664	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
ze	z	k7c2
slovinské	slovinský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
z	z	k7c2
obce	obec	k1gFnSc2
Žirovnica	Žirovnicum	k1gNnSc2
či	či	k8xC
od	od	k7c2
chaty	chata	k1gFnSc2
Prešernova	Prešernov	k1gInSc2
koča	koča	k?
(	(	kIx(
<g/>
2174	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Stol	stol	k1gInSc1
(	(	kIx(
<g/>
mountain	mountain	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stol	stol	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stol	stol	k1gInSc1
na	na	k7c4
SummitPost	SummitPost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Stol	stol	k1gInSc1
na	na	k7c6
Hribi	Hrib	k1gFnSc6
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovinsko	Slovinsko	k1gNnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
