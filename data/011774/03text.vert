<p>
<s>
79	[number]	k4	79
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelskou	pořadatelský	k2eAgFnSc7d1	pořadatelská
zemí	zem	k1gFnSc7	zem
bylo	být	k5eAaImAgNnS	být
Česko	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
kongres	kongres	k1gInSc1	kongres
IIHF	IIHF	kA	IIHF
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgInS	odehrávat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
MS	MS	kA	MS
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pořádalo	pořádat	k5eAaImAgNnS	pořádat
už	už	k6eAd1	už
po	po	k7c6	po
desáté	desátá	k1gFnSc6	desátá
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
potřetí	potřetí	k4xO	potřetí
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
14	[number]	k4	14
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
z	z	k7c2	z
minulého	minulý	k2eAgNnSc2d1	Minulé
mistrovství	mistrovství	k1gNnSc2	mistrovství
a	a	k8xC	a
2	[number]	k4	2
postupující	postupující	k2eAgFnSc7d1	postupující
z	z	k7c2	z
minulého	minulý	k2eAgInSc2d1	minulý
ročníku	ročník	k1gInSc2	ročník
skupiny	skupina	k1gFnSc2	skupina
A	a	k8xC	a
divize	divize	k1gFnSc1	divize
1	[number]	k4	1
-	-	kIx~	-
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obhájcem	obhájce	k1gMnSc7	obhájce
mistrovské	mistrovský	k2eAgFnSc2d1	mistrovská
trofeje	trofej	k1gFnSc2	trofej
byl	být	k5eAaImAgInS	být
výběr	výběr	k1gInSc1	výběr
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
tým	tým	k1gInSc4	tým
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
poměrem	poměr	k1gInSc7	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběr	výběr	k1gInSc1	výběr
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
pořádání	pořádání	k1gNnSc6	pořádání
79	[number]	k4	79
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
právě	právě	k6eAd1	právě
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hlasování	hlasování	k1gNnSc2	hlasování
kongresu	kongres	k1gInSc2	kongres
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
protikandidátem	protikandidát	k1gMnSc7	protikandidát
Česka	Česko	k1gNnSc2	Česko
v	v	k7c6	v
kandidatuře	kandidatura	k1gFnSc6	kandidatura
byla	být	k5eAaImAgFnS	být
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ovšem	ovšem	k9	ovšem
prohrála	prohrát	k5eAaPmAgFnS	prohrát
výrazným	výrazný	k2eAgInSc7d1	výrazný
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
84	[number]	k4	84
ku	k	k7c3	k
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g/>
Kandidující	kandidující	k2eAgInPc1d1	kandidující
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Stadiony	stadion	k1gInPc4	stadion
==	==	k?	==
</s>
</p>
<p>
<s>
Pořadatelé	pořadatel	k1gMnPc1	pořadatel
šampionátu	šampionát	k1gInSc2	šampionát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vybrali	vybrat	k5eAaPmAgMnP	vybrat
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
pražskou	pražský	k2eAgFnSc4d1	Pražská
O2	O2	k1gFnSc4	O2
arenu	aren	k1gInSc2	aren
a	a	k8xC	a
ČEZ	ČEZ	kA	ČEZ
Arénu	aréna	k1gFnSc4	aréna
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
při	při	k7c6	při
pořádání	pořádání	k1gNnSc6	pořádání
MS	MS	kA	MS
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Maskoti	Maskot	k1gMnPc1	Maskot
šampionátu	šampionát	k1gInSc2	šampionát
==	==	k?	==
</s>
</p>
<p>
<s>
Maskoty	maskot	k1gInPc1	maskot
turnaje	turnaj	k1gInSc2	turnaj
byli	být	k5eAaImAgMnP	být
králíci	králík	k1gMnPc1	králík
z	z	k7c2	z
klobouku	klobouk	k1gInSc2	klobouk
Bob	Bob	k1gMnSc1	Bob
a	a	k8xC	a
Bobek	Bobek	k1gMnSc1	Bobek
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgFnSc4d1	populární
večerníčkovou	večerníčkový	k2eAgFnSc4d1	večerníčková
dvojici	dvojice	k1gFnSc4	dvojice
poprvé	poprvé	k6eAd1	poprvé
představili	představit	k5eAaPmAgMnP	představit
zástupci	zástupce	k1gMnPc1	zástupce
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2014	[number]	k4	2014
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Clarion	Clarion	k1gInSc1	Clarion
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
Tomáš	Tomáš	k1gMnSc1	Tomáš
Král	Král	k1gMnSc1	Král
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
výběr	výběr	k1gInSc4	výběr
maskotů	maskot	k1gInPc2	maskot
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Maskot	maskot	k1gInSc1	maskot
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vyznění	vyznění	k1gNnSc4	vyznění
akce	akce	k1gFnSc2	akce
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
stěžejních	stěžejní	k2eAgFnPc2d1	stěžejní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
intenzivně	intenzivně	k6eAd1	intenzivně
věnovali	věnovat	k5eAaPmAgMnP	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
něco	něco	k3yInSc4	něco
vypovídal	vypovídat	k5eAaPmAgMnS	vypovídat
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
zároveň	zároveň	k6eAd1	zároveň
něco	něco	k6eAd1	něco
říkal	říkat	k5eAaImAgMnS	říkat
našim	náš	k3xOp1gMnPc3	náš
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
však	však	k9	však
chtělo	chtít	k5eAaImAgNnS	chtít
vedení	vedení	k1gNnSc1	vedení
ČSLH	ČSLH	kA	ČSLH
za	za	k7c4	za
maskota	maskot	k1gMnSc4	maskot
šampionátu	šampionát	k1gInSc2	šampionát
světoznámého	světoznámý	k2eAgMnSc4d1	světoznámý
Krtečka	krteček	k1gMnSc4	krteček
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
tvůrcem	tvůrce	k1gMnSc7	tvůrce
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
autor	autor	k1gMnSc1	autor
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Miler	Miler	k1gMnSc1	Miler
<g/>
.	.	kIx.	.
</s>
<s>
Tenhle	tenhle	k3xDgInSc1	tenhle
návrh	návrh	k1gInSc1	návrh
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
kvůli	kvůli	k7c3	kvůli
složitým	složitý	k2eAgNnPc3d1	složité
jednáním	jednání	k1gNnPc3	jednání
ohledně	ohledně	k7c2	ohledně
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
s	s	k7c7	s
vnučkou	vnučka	k1gFnSc7	vnučka
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Milera	Miler	k1gMnSc2	Miler
neprošel	projít	k5eNaPmAgMnS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
svazu	svaz	k1gInSc2	svaz
Tomáš	Tomáš	k1gMnSc1	Tomáš
Král	Král	k1gMnSc1	Král
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
tak	tak	k6eAd1	tak
složitá	složitý	k2eAgFnSc1d1	složitá
situace	situace	k1gFnSc1	situace
s	s	k7c7	s
majiteli	majitel	k1gMnPc7	majitel
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
padlo	padnout	k5eAaPmAgNnS	padnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
schvalování	schvalování	k1gNnSc2	schvalování
oficiálního	oficiální	k2eAgMnSc2d1	oficiální
maskota	maskot	k1gMnSc2	maskot
zabral	zabrat	k5eAaPmAgMnS	zabrat
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
schválit	schválit	k5eAaPmF	schválit
výběr	výběr	k1gInSc4	výběr
totiž	totiž	k9	totiž
vzápětí	vzápětí	k6eAd1	vzápětí
musela	muset	k5eAaImAgFnS	muset
ještě	ještě	k9	ještě
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
a	a	k8xC	a
sportovní	sportovní	k2eAgFnSc1d1	sportovní
marketingová	marketingový	k2eAgFnSc1d1	marketingová
agentura	agentura	k1gFnSc1	agentura
Infront	Infronta	k1gFnPc2	Infronta
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc1	právo
na	na	k7c6	na
použití	použití	k1gNnSc6	použití
Boba	Bob	k1gMnSc4	Bob
a	a	k8xC	a
Bobka	Bobek	k1gMnSc4	Bobek
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
zástupci	zástupce	k1gMnPc7	zástupce
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc2	vedení
ČSLH	ČSLH	kA	ČSLH
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
domluvilo	domluvit	k5eAaPmAgNnS	domluvit
<g/>
.	.	kIx.	.
</s>
<s>
Představitelé	představitel	k1gMnPc1	představitel
svazu	svaz	k1gInSc2	svaz
mohli	moct	k5eAaImAgMnP	moct
vybírat	vybírat	k5eAaImF	vybírat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
variant	varianta	k1gFnPc2	varianta
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
maskotem	maskot	k1gInSc7	maskot
stane	stanout	k5eAaPmIp3nS	stanout
tradičně	tradičně	k6eAd1	tradičně
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
už	už	k9	už
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
šampionátech	šampionát	k1gInPc6	šampionát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
i	i	k8xC	i
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
jsme	být	k5eAaImIp1nP	být
přemýšleli	přemýšlet	k5eAaImAgMnP	přemýšlet
o	o	k7c6	o
lvu	lev	k1gInSc6	lev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
hokejem	hokej	k1gInSc7	hokej
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
spojen	spojit	k5eAaPmNgInS	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
však	však	k9	však
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ten	ten	k3xDgInSc1	ten
maskot	maskot	k1gInSc1	maskot
byl	být	k5eAaImAgInS	být
příjemný	příjemný	k2eAgMnSc1d1	příjemný
a	a	k8xC	a
milý	milý	k2eAgMnSc1d1	milý
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lidi	člověk	k1gMnPc4	člověk
bavili	bavit	k5eAaImAgMnP	bavit
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
maskot	maskot	k1gInSc1	maskot
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
dodal	dodat	k5eAaPmAgMnS	dodat
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
Král	Král	k1gMnSc1	Král
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
hokejový	hokejový	k2eAgInSc4d1	hokejový
vzhled	vzhled	k1gInSc4	vzhled
Boba	Bob	k1gMnSc2	Bob
a	a	k8xC	a
Bobka	Bobek	k1gMnSc2	Bobek
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
výtvarník	výtvarník	k1gMnSc1	výtvarník
a	a	k8xC	a
scénograf	scénograf	k1gMnSc1	scénograf
Alex	Alex	k1gMnSc1	Alex
Dowis	Dowis	k1gInSc4	Dowis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
originálního	originální	k2eAgNnSc2d1	originální
provedení	provedení	k1gNnSc2	provedení
od	od	k7c2	od
kreslíře	kreslíř	k1gMnSc2	kreslíř
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Jiránka	Jiránek	k1gMnSc2	Jiránek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prodej	prodej	k1gInSc1	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
oficiálním	oficiální	k2eAgInSc7d1	oficiální
začátkem	začátek	k1gInSc7	začátek
prodeje	prodej	k1gInSc2	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
pořadatelé	pořadatel	k1gMnPc1	pořadatel
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodej	prodej	k1gInSc1	prodej
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
fázích	fáze	k1gFnPc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
si	se	k3xPyFc3	se
fanoušci	fanoušek	k1gMnPc1	fanoušek
mohli	moct	k5eAaImAgMnP	moct
koupit	koupit	k5eAaPmF	koupit
jenom	jenom	k9	jenom
denní	denní	k2eAgInPc4d1	denní
balíčky	balíček	k1gInPc4	balíček
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
respektive	respektive	k9	respektive
tři	tři	k4xCgInPc4	tři
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
dějišti	dějiště	k1gNnSc6	dějiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
od	od	k7c2	od
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
mohli	moct	k5eAaImAgMnP	moct
koupit	koupit	k5eAaPmF	koupit
vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
utkání	utkání	k1gNnPc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgMnSc7d1	oficiální
prodejcem	prodejce	k1gMnSc7	prodejce
vstupenek	vstupenka	k1gFnPc2	vstupenka
je	být	k5eAaImIp3nS	být
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Sazka	Sazka	k1gFnSc1	Sazka
Ticket	Ticket	k1gInSc4	Ticket
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ceny	cena	k1gFnPc1	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
Organizační	organizační	k2eAgInSc1d1	organizační
výbor	výbor	k1gInSc1	výbor
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
cenové	cenový	k2eAgFnSc2d1	cenová
kategorie	kategorie	k1gFnSc2	kategorie
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc4	způsob
a	a	k8xC	a
časový	časový	k2eAgInSc4d1	časový
harmonogram	harmonogram	k1gInSc4	harmonogram
jejich	jejich	k3xOp3gInSc2	jejich
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
od	od	k7c2	od
190	[number]	k4	190
Kč	Kč	kA	Kč
na	na	k7c4	na
méně	málo	k6eAd2	málo
atraktivní	atraktivní	k2eAgInPc4d1	atraktivní
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
zápasy	zápas	k1gInPc4	zápas
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
690	[number]	k4	690
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinálové	čtvrtfinálový	k2eAgInPc4d1	čtvrtfinálový
zápasy	zápas	k1gInPc4	zápas
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zakoupit	zakoupit	k5eAaPmF	zakoupit
v	v	k7c6	v
cenových	cenový	k2eAgFnPc6d1	cenová
relacích	relace	k1gFnPc6	relace
2	[number]	k4	2
390	[number]	k4	390
až	až	k9	až
2	[number]	k4	2
990	[number]	k4	990
korun	koruna	k1gFnPc2	koruna
českých	český	k2eAgFnPc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c6	na
semifinále	semifinále	k1gNnSc6	semifinále
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
4	[number]	k4	4
690	[number]	k4	690
Kč	Kč	kA	Kč
a	a	k8xC	a
5	[number]	k4	5
790	[number]	k4	790
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
ceny	cena	k1gFnSc2	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k6eAd1	místo
byly	být	k5eAaImAgInP	být
4	[number]	k4	4
290	[number]	k4	290
Kč	Kč	kA	Kč
a	a	k8xC	a
5	[number]	k4	5
390	[number]	k4	390
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenka	vstupenka	k1gFnSc1	vstupenka
na	na	k7c4	na
finálový	finálový	k2eAgInSc4d1	finálový
zápas	zápas	k1gInSc4	zápas
pak	pak	k6eAd1	pak
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
7	[number]	k4	7
190	[number]	k4	190
Kč	Kč	kA	Kč
resp.	resp.	kA	resp.
8	[number]	k4	8
990	[number]	k4	990
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
ČSLH	ČSLH	kA	ČSLH
JUDr.	JUDr.	kA	JUDr.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Král	Král	k1gMnSc1	Král
k	k	k7c3	k
ceně	cena	k1gFnSc3	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Snahou	snaha	k1gFnSc7	snaha
organizátorů	organizátor	k1gMnPc2	organizátor
bylo	být	k5eAaImAgNnS	být
zohlednit	zohlednit	k5eAaPmF	zohlednit
v	v	k7c6	v
cenách	cena	k1gFnPc6	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
program	program	k1gInSc4	program
a	a	k8xC	a
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
rozlosování	rozlosování	k1gNnSc4	rozlosování
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
výnos	výnos	k1gInSc1	výnos
ze	z	k7c2	z
vstupenek	vstupenka	k1gFnPc2	vstupenka
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
do	do	k7c2	do
rozpočtu	rozpočet	k1gInSc2	rozpočet
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
musí	muset	k5eAaImIp3nS	muset
pokrýt	pokrýt	k5eAaPmF	pokrýt
veškeré	veškerý	k3xTgInPc4	veškerý
výdaje	výdaj	k1gInPc4	výdaj
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
organizací	organizace	k1gFnSc7	organizace
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
zejména	zejména	k9	zejména
zápasy	zápas	k1gInPc1	zápas
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
místním	místní	k2eAgMnPc3d1	místní
fanouškům	fanoušek	k1gMnPc3	fanoušek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
ceny	cena	k1gFnPc1	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
jsou	být	k5eAaImIp3nP	být
předražené	předražený	k2eAgInPc1d1	předražený
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
trenér	trenér	k1gMnSc1	trenér
české	český	k2eAgFnSc2d1	Česká
hokejová	hokejový	k2eAgFnSc1d1	hokejová
reprezentace	reprezentace	k1gFnSc1	reprezentace
Vladimír	Vladimír	k1gMnSc1	Vladimír
Růžička	Růžička	k1gMnSc1	Růžička
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
české	český	k2eAgInPc4d1	český
poměry	poměr	k1gInPc4	poměr
jsou	být	k5eAaImIp3nP	být
vstupenky	vstupenka	k1gFnPc1	vstupenka
dražší	drahý	k2eAgFnPc1d2	dražší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
vstupenek	vstupenka	k1gFnPc2	vstupenka
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
prodeje	prodej	k1gInSc2	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
začala	začít	k5eAaPmAgFnS	začít
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
od	od	k7c2	od
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
SEČ	SEČ	kA	SEČ
na	na	k7c6	na
terminálech	terminál	k1gInPc6	terminál
společnosti	společnost	k1gFnSc2	společnost
Sazka	Sazka	k1gFnSc1	Sazka
<g/>
,	,	kIx,	,
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
Sazky	Sazka	k1gFnSc2	Sazka
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
pokladnách	pokladna	k1gFnPc6	pokladna
obou	dva	k4xCgInPc2	dva
dějišť	dějiště	k1gNnPc2	dějiště
<g/>
,	,	kIx,	,
O2	O2	k1gMnSc1	O2
areny	arena	k1gFnSc2	arena
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
ČEZ	ČEZ	kA	ČEZ
Arény	aréna	k1gFnSc2	aréna
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
oběma	dva	k4xCgFnPc7	dva
halami	hala	k1gFnPc7	hala
se	se	k3xPyFc4	se
první	první	k4xOgMnPc1	první
zájemci	zájemce	k1gMnPc1	zájemce
o	o	k7c4	o
lístky	lístek	k1gInPc4	lístek
postavili	postavit	k5eAaPmAgMnP	postavit
do	do	k7c2	do
fronty	fronta	k1gFnSc2	fronta
už	už	k6eAd1	už
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
den	den	k1gInSc4	den
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
nápor	nápor	k1gInSc1	nápor
lidí	člověk	k1gMnPc2	člověk
však	však	k9	však
přišel	přijít	k5eAaPmAgMnS	přijít
brzo	brzo	k6eAd1	brzo
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
začalo	začít	k5eAaPmAgNnS	začít
jezdit	jezdit	k5eAaImF	jezdit
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Fronty	front	k1gInPc1	front
se	se	k3xPyFc4	se
tvořily	tvořit	k5eAaImAgInP	tvořit
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
samotný	samotný	k2eAgInSc1d1	samotný
prodej	prodej	k1gInSc1	prodej
šel	jít	k5eAaImAgInS	jít
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
kupovala	kupovat	k5eAaImAgFnS	kupovat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
za	za	k7c4	za
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
prodeje	prodej	k1gInSc2	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
organizátorů	organizátor	k1gMnPc2	organizátor
v	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
88	[number]	k4	88
860	[number]	k4	860
balíčků	balíček	k1gInPc2	balíček
vstupenek	vstupenka	k1gFnPc2	vstupenka
a	a	k8xC	a
zaplaceno	zaplatit	k5eAaPmNgNnS	zaplatit
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
22	[number]	k4	22
957	[number]	k4	957
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
vstupenky	vstupenka	k1gFnPc1	vstupenka
na	na	k7c6	na
utkání	utkání	k1gNnSc6	utkání
domácího	domácí	k2eAgNnSc2d1	domácí
mužstva	mužstvo	k1gNnSc2	mužstvo
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
před	před	k7c7	před
15	[number]	k4	15
<g/>
.	.	kIx.	.
hodinou	hodina	k1gFnSc7	hodina
rezervovány	rezervován	k2eAgFnPc1d1	rezervována
<g/>
.	.	kIx.	.
</s>
<s>
Šéfka	šéfka	k1gFnSc1	šéfka
ticketingu	ticketing	k1gInSc2	ticketing
Radka	Radka	k1gFnSc1	Radka
Bařtipánová	Bařtipánová	k1gFnSc1	Bařtipánová
však	však	k9	však
dodala	dodat	k5eAaPmAgFnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
fanoušci	fanoušek	k1gMnPc1	fanoušek
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
nedostalo	dostat	k5eNaPmAgNnS	dostat
nemusí	muset	k5eNaImIp3nS	muset
zoufat	zoufat	k5eAaPmF	zoufat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
vstupenky	vstupenka	k1gFnSc2	vstupenka
z	z	k7c2	z
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nebudou	být	k5eNaImBp3nP	být
uhrazeny	uhradit	k5eAaPmNgFnP	uhradit
ve	v	k7c6	v
stanoveném	stanovený	k2eAgInSc6d1	stanovený
časovém	časový	k2eAgInSc6d1	časový
limitu	limit	k1gInSc6	limit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
vlně	vlna	k1gFnSc6	vlna
prodeje	prodej	k1gInSc2	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začne	začít	k5eAaPmIp3nS	začít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
reálné	reálný	k2eAgNnSc1d1	reálné
uvolnění	uvolnění	k1gNnSc1	uvolnění
vstupenek	vstupenka	k1gFnPc2	vstupenka
z	z	k7c2	z
blokací	blokace	k1gFnPc2	blokace
pro	pro	k7c4	pro
IIHF	IIHF	kA	IIHF
<g/>
,	,	kIx,	,
marketingovou	marketingový	k2eAgFnSc4d1	marketingová
společnost	společnost	k1gFnSc4	společnost
Infront	Infronta	k1gFnPc2	Infronta
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
partnery	partner	k1gMnPc4	partner
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
technických	technický	k2eAgFnPc2d1	technická
blokací	blokace	k1gFnPc2	blokace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
pak	pak	k6eAd1	pak
organizátoři	organizátor	k1gMnPc1	organizátor
šampionátu	šampionát	k1gInSc2	šampionát
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
šampionát	šampionát	k1gInSc4	šampionát
prozatím	prozatím	k6eAd1	prozatím
prodáno	prodat	k5eAaPmNgNnS	prodat
43	[number]	k4	43
%	%	kIx~	%
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mírně	mírně	k6eAd1	mírně
převyšovalo	převyšovat	k5eAaImAgNnS	převyšovat
jejich	jejich	k3xOp3gInPc4	jejich
odhady	odhad	k1gInPc4	odhad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Generální	generální	k2eAgMnSc1d1	generální
sekretář	sekretář	k1gMnSc1	sekretář
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
Martin	Martin	k1gMnSc1	Martin
Urban	Urban	k1gMnSc1	Urban
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
podrobnosti	podrobnost	k1gFnPc4	podrobnost
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Prodejní	prodejní	k2eAgFnSc1d1	prodejní
kapacita	kapacita	k1gFnSc1	kapacita
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
723	[number]	k4	723
678	[number]	k4	678
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
včerejšku	včerejšek	k1gInSc3	včerejšek
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
311	[number]	k4	311
718	[number]	k4	718
a	a	k8xC	a
těchto	tento	k3xDgInPc2	tento
43	[number]	k4	43
procent	procento	k1gNnPc2	procento
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
natvrdo	natvrdo	k6eAd1	natvrdo
prodaných	prodaný	k2eAgFnPc2d1	prodaná
a	a	k8xC	a
zaplacených	zaplacený	k2eAgFnPc2d1	zaplacená
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
první	první	k4xOgFnSc6	první
vlně	vlna	k1gFnSc6	vlna
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
prodáno	prodat	k5eAaPmNgNnS	prodat
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
%	%	kIx~	%
lístků	lístek	k1gInPc2	lístek
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
kapacity	kapacita	k1gFnSc2	kapacita
723	[number]	k4	723
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
vstupenek	vstupenka	k1gFnPc2	vstupenka
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
prodeje	prodej	k1gInSc2	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
začala	začít	k5eAaPmAgFnS	začít
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
od	od	k7c2	od
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
SEČ	seč	k6eAd1	seč
a	a	k8xC	a
probíhala	probíhat	k5eAaImAgFnS	probíhat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
vlna	vlna	k1gFnSc1	vlna
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
i	i	k9	i
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
200	[number]	k4	200
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
vstupenek	vstupenka	k1gFnPc2	vstupenka
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
českého	český	k2eAgNnSc2d1	české
mužstva	mužstvo	k1gNnSc2	mužstvo
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
i	i	k8xC	i
v	v	k7c6	v
případném	případný	k2eAgNnSc6d1	případné
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
lístky	lístek	k1gInPc4	lístek
z	z	k7c2	z
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
technických	technický	k2eAgFnPc2d1	technická
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Lístky	lístek	k1gInPc4	lístek
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgFnP	být
vyprodané	vyprodaný	k2eAgInPc1d1	vyprodaný
po	po	k7c4	po
50	[number]	k4	50
minutách	minuta	k1gFnPc6	minuta
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
sekretář	sekretář	k1gMnSc1	sekretář
ČSLH	ČSLH	kA	ČSLH
Martin	Martin	k1gMnSc1	Martin
Urban	Urban	k1gMnSc1	Urban
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
první	první	k4xOgFnSc6	první
vlně	vlna	k1gFnSc6	vlna
prodeje	prodej	k1gInSc2	prodej
byl	být	k5eAaImAgInS	být
největší	veliký	k2eAgInSc1d3	veliký
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zmizely	zmizet	k5eAaPmAgFnP	zmizet
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
po	po	k7c6	po
devíti	devět	k4xCc6	devět
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc4	minuta
déle	dlouho	k6eAd2	dlouho
vydržely	vydržet	k5eAaPmAgInP	vydržet
v	v	k7c6	v
systému	systém	k1gInSc6	systém
vstupenky	vstupenka	k1gFnSc2	vstupenka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
českého	český	k2eAgInSc2d1	český
týmu	tým	k1gInSc2	tým
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
šampionátu	šampionát	k1gInSc2	šampionát
oznámili	oznámit	k5eAaPmAgMnP	oznámit
organizátoři	organizátor	k1gMnPc1	organizátor
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
495	[number]	k4	495
tisíc	tisíc	k4xCgInPc2	tisíc
vstupenek	vstupenka	k1gFnPc2	vstupenka
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
68	[number]	k4	68
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
kapacity	kapacita	k1gFnSc2	kapacita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organizátoři	organizátor	k1gMnPc1	organizátor
navíc	navíc	k6eAd1	navíc
vyčlenili	vyčlenit	k5eAaPmAgMnP	vyčlenit
35	[number]	k4	35
tisíc	tisíc	k4xCgInSc4	tisíc
vstupenek	vstupenka	k1gFnPc2	vstupenka
pro	pro	k7c4	pro
školáky	školák	k1gMnPc4	školák
a	a	k8xC	a
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
lístků	lístek	k1gInPc2	lístek
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
hokejisty	hokejista	k1gMnPc4	hokejista
do	do	k7c2	do
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návštěvnický	návštěvnický	k2eAgInSc4d1	návštěvnický
rekord	rekord	k1gInSc4	rekord
===	===	k?	===
</s>
</p>
<p>
<s>
MS	MS	kA	MS
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Ostravě	Ostrava	k1gFnSc6	Ostrava
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
na	na	k7c6	na
64	[number]	k4	64
zápasech	zápas	k1gInPc6	zápas
741	[number]	k4	741
690	[number]	k4	690
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
největší	veliký	k2eAgFnSc1d3	veliký
návštěva	návštěva	k1gFnSc1	návštěva
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
mistrovství	mistrovství	k1gNnSc1	mistrovství
také	také	k9	také
překonalo	překonat	k5eAaPmAgNnS	překonat
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
návštěvnosti	návštěvnost	k1gFnSc6	návštěvnost
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
činí	činit	k5eAaImIp3nS	činit
11	[number]	k4	11
589	[number]	k4	589
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc1	přehled
skupin	skupina	k1gFnPc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Nasazení	nasazení	k1gNnSc1	nasazení
týmů	tým	k1gInPc2	tým
(	(	kIx(	(
<g/>
čísla	číslo	k1gNnSc2	číslo
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
podle	podle	k7c2	podle
žebříčku	žebříček	k1gInSc2	žebříček
IIHF	IIHF	kA	IIHF
<g/>
,	,	kIx,	,
vydaného	vydaný	k2eAgInSc2d1	vydaný
po	po	k7c6	po
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelé	pořadatel	k1gMnPc1	pořadatel
šampionátu	šampionát	k1gInSc2	šampionát
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
využít	využít	k5eAaPmF	využít
přesunutí	přesunutí	k1gNnSc3	přesunutí
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
týmů	tým	k1gInPc2	tým
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
původní	původní	k2eAgFnSc2d1	původní
přidělené	přidělený	k2eAgFnSc2d1	přidělená
skupiny	skupina	k1gFnSc2	skupina
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
využili	využít	k5eAaPmAgMnP	využít
hned	hned	k6eAd1	hned
po	po	k7c4	po
oznámení	oznámení	k1gNnSc4	oznámení
složení	složení	k1gNnSc2	složení
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
když	když	k8xS	když
záměrně	záměrně	k6eAd1	záměrně
zařadili	zařadit	k5eAaPmAgMnP	zařadit
Slovensko	Slovensko	k1gNnSc4	Slovensko
do	do	k7c2	do
ostravské	ostravský	k2eAgFnSc2d1	Ostravská
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
to	ten	k3xDgNnSc1	ten
slovenští	slovenský	k2eAgMnPc1d1	slovenský
fanoušci	fanoušek	k1gMnPc1	fanoušek
mají	mít	k5eAaImIp3nP	mít
daleko	daleko	k6eAd1	daleko
blíže	blízce	k6eAd2	blízce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
nich	on	k3xPp3gInPc2	on
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
Švýcaři	Švýcar	k1gMnPc1	Švýcar
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
totiž	totiž	k9	totiž
původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pořadatelé	pořadatel	k1gMnPc1	pořadatel
nechtěli	chtít	k5eNaImAgMnP	chtít
připustit	připustit	k5eAaPmF	připustit
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
atraktivitu	atraktivita	k1gFnSc4	atraktivita
celého	celý	k2eAgInSc2d1	celý
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soupisky	soupiska	k1gFnSc2	soupiska
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
družstvo	družstvo	k1gNnSc1	družstvo
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
17	[number]	k4	17
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
15	[number]	k4	15
bruslařů	bruslař	k1gMnPc2	bruslař
a	a	k8xC	a
2	[number]	k4	2
brankářů	brankář	k1gMnPc2	brankář
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
25	[number]	k4	25
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
22	[number]	k4	22
bruslařů	bruslař	k1gMnPc2	bruslař
a	a	k8xC	a
3	[number]	k4	3
brankářů	brankář	k1gMnPc2	brankář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
zúčastněná	zúčastněný	k2eAgNnPc1d1	zúčastněné
mužstva	mužstvo	k1gNnPc1	mužstvo
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
skrze	skrze	k?	skrze
potvrzení	potvrzení	k1gNnSc4	potvrzení
svých	svůj	k3xOyFgFnPc2	svůj
příslušných	příslušný	k2eAgFnPc2d1	příslušná
národních	národní	k2eAgFnPc2d1	národní
asociací	asociace	k1gFnPc2	asociace
<g/>
,	,	kIx,	,
předložit	předložit	k5eAaPmF	předložit
potvrzenou	potvrzený	k2eAgFnSc4d1	potvrzená
soupisku	soupiska	k1gFnSc4	soupiska
prvně	prvně	k?	prvně
ředitelství	ředitelství	k1gNnPc2	ředitelství
IIHF	IIHF	kA	IIHF
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
následně	následně	k6eAd1	následně
schválí	schválit	k5eAaPmIp3nS	schválit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
nominovala	nominovat	k5eAaBmAgFnS	nominovat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
16	[number]	k4	16
hlavních	hlavní	k2eAgMnPc2d1	hlavní
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
a	a	k8xC	a
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
čárových	čárový	k2eAgFnPc2d1	čárová
sudích	sudí	k1gFnPc2	sudí
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zastoupení	zastoupení	k1gNnSc1	zastoupení
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
na	na	k7c6	na
šampionátu	šampionát	k1gInSc6	šampionát
mělo	mít	k5eAaImAgNnS	mít
Česko	Česko	k1gNnSc1	Česko
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
rekordní	rekordní	k2eAgInSc1d1	rekordní
počet	počet	k1gInSc1	počet
hned	hned	k9	hned
pěti	pět	k4xCc2	pět
českých	český	k2eAgMnPc2d1	český
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
,	,	kIx,	,
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
a	a	k8xC	a
třech	tři	k4xCgInPc2	tři
čárových	čárový	k2eAgInPc2d1	čárový
<g/>
.	.	kIx.	.
</s>
<s>
Švédové	Švéd	k1gMnPc1	Švéd
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
tři	tři	k4xCgMnPc1	tři
hlavní	hlavní	k2eAgMnPc1d1	hlavní
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
čárové	čárový	k2eAgFnPc1d1	čárová
sudí	sudí	k1gFnSc1	sudí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
mělo	mít	k5eAaImAgNnS	mít
Finsko	Finsko	k1gNnSc1	Finsko
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
šestnácti	šestnáct	k4xCc2	šestnáct
hlavních	hlavní	k2eAgMnPc2d1	hlavní
sudí	sudí	k1gFnSc1	sudí
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
pětice	pětice	k1gFnPc4	pětice
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Švýcaři	Švýcar	k1gMnPc1	Švýcar
Daniel	Daniel	k1gMnSc1	Daniel
Stricker	Stricker	k1gMnSc1	Stricker
a	a	k8xC	a
Tobias	Tobias	k1gMnSc1	Tobias
Wehrli	Wehrle	k1gFnSc4	Wehrle
již	již	k9	již
na	na	k7c4	na
MS	MS	kA	MS
působili	působit	k5eAaImAgMnP	působit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
jako	jako	k9	jako
čároví	čárový	k2eAgMnPc1d1	čárový
sudí	sudí	k1gMnPc1	sudí
<g/>
.	.	kIx.	.
</s>
<s>
Nejzkušenějším	zkušený	k2eAgMnSc7d3	nejzkušenější
hlavním	hlavní	k2eAgMnSc7d1	hlavní
rozhodčím	rozhodčí	k1gMnSc7	rozhodčí
byl	být	k5eAaImAgMnS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šindler	Šindler	k1gMnSc1	Šindler
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pískal	pískat	k5eAaImAgMnS	pískat
už	už	k6eAd1	už
na	na	k7c6	na
jedenácti	jedenáct	k4xCc6	jedenáct
světových	světový	k2eAgInPc6d1	světový
šampionátech	šampionát	k1gInPc6	šampionát
a	a	k8xC	a
dvou	dva	k4xCgFnPc6	dva
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
sedm	sedm	k4xCc1	sedm
čárových	čárový	k2eAgMnPc2d1	čárový
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
působilo	působit	k5eAaImAgNnS	působit
na	na	k7c6	na
seniorském	seniorský	k2eAgInSc6d1	seniorský
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
úplně	úplně	k6eAd1	úplně
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Nejzkušenějším	zkušený	k2eAgMnSc7d3	nejzkušenější
čárovým	čárový	k2eAgMnSc7d1	čárový
sudím	sudí	k1gMnSc7	sudí
byl	být	k5eAaImAgMnS	být
Estonec	Estonec	k1gMnSc1	Estonec
Anton	Anton	k1gMnSc1	Anton
Semjonov	Semjonov	k1gInSc4	Semjonov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
už	už	k6eAd1	už
osmi	osm	k4xCc2	osm
světových	světový	k2eAgInPc2d1	světový
šampionátů	šampionát	k1gInPc2	šampionát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgInSc1d1	herní
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Šestnáct	šestnáct	k4xCc1	šestnáct
účastníků	účastník	k1gMnPc2	účastník
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
8	[number]	k4	8
týmech	tým	k1gInPc6	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
utkal	utkat	k5eAaPmAgMnS	utkat
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
udělovaly	udělovat	k5eAaImAgFnP	udělovat
3	[number]	k4	3
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
či	či	k8xC	či
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
2	[number]	k4	2
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
či	či	k8xC	či
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
1	[number]	k4	1
bod	bod	k1gInSc4	bod
a	a	k8xC	a
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
0	[number]	k4	0
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgNnSc1d1	případné
prodloužení	prodloužení	k1gNnSc1	prodloužení
následovalo	následovat	k5eAaImAgNnS	následovat
po	po	k7c6	po
krátké	krátká	k1gFnSc6	krátká
3	[number]	k4	3
minutové	minutový	k2eAgFnSc6d1	minutová
přestávce	přestávka	k1gFnSc6	přestávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
části	část	k1gFnSc6	část
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
skupiny	skupina	k1gFnSc2	skupina
postoupila	postoupit	k5eAaPmAgFnS	postoupit
čtveřice	čtveřice	k1gFnSc1	čtveřice
týmů	tým	k1gInPc2	tým
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
playoff	playoff	k1gInSc1	playoff
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
týmy	tým	k1gInPc4	tým
na	na	k7c6	na
pátých	pátá	k1gFnPc6	pátá
až	až	k9	až
sedmých	sedmý	k4xOgNnPc6	sedmý
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
turnaj	turnaj	k1gInSc1	turnaj
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
osmých	osmý	k4xOgNnPc2	osmý
míst	místo	k1gNnPc2	místo
automaticky	automaticky	k6eAd1	automaticky
sestoupily	sestoupit	k5eAaPmAgFnP	sestoupit
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
neplatilo	platit	k5eNaImAgNnS	platit
pro	pro	k7c4	pro
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
pořadatele	pořadatel	k1gMnPc4	pořadatel
MS	MS	kA	MS
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritéria	kritérion	k1gNnPc4	kritérion
při	při	k7c6	při
rovnosti	rovnost	k1gFnSc6	rovnost
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
skupinách	skupina	k1gFnPc6	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
měly	mít	k5eAaImAgInP	mít
po	po	k7c6	po
konci	konec	k1gInSc6	konec
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
dva	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc4	tým
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
o	o	k7c6	o
postupujícím	postupující	k2eAgNnSc6d1	postupující
nebo	nebo	k8xC	nebo
o	o	k7c6	o
lépe	dobře	k6eAd2	dobře
nasazeném	nasazený	k2eAgInSc6d1	nasazený
týmu	tým	k1gInSc6	tým
pro	pro	k7c4	pro
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
výsledek	výsledek	k1gInSc1	výsledek
jejich	jejich	k3xOp3gInSc2	jejich
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
rovnost	rovnost	k1gFnSc1	rovnost
nastala	nastat	k5eAaPmAgFnS	nastat
mezi	mezi	k7c7	mezi
třemi	tři	k4xCgInPc7	tři
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
týmy	tým	k1gInPc4	tým
<g/>
,	,	kIx,	,
postupovalo	postupovat	k5eAaImAgNnS	postupovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
následujících	následující	k2eAgNnPc2d1	následující
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nezbyly	zbýt	k5eNaPmAgInP	zbýt
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
výsledek	výsledek	k1gInSc4	výsledek
ze	z	k7c2	z
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
zápasu	zápas	k1gInSc2	zápas
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Body	bod	k1gInPc4	bod
z	z	k7c2	z
minitabulky	minitabulka	k1gFnSc2	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
z	z	k7c2	z
minitabulky	minitabulka	k1gFnSc2	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
proti	proti	k7c3	proti
nejbližšímu	blízký	k2eAgInSc3d3	Nejbližší
nejvýše	nejvýše	k6eAd1	nejvýše
umístěnému	umístěný	k2eAgInSc3d1	umístěný
týmu	tým	k1gInSc3	tým
mimo	mimo	k7c4	mimo
týmy	tým	k1gInPc4	tým
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
více	hodně	k6eAd2	hodně
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
<g/>
)	)	kIx)	)
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
družstvu	družstvo	k1gNnSc3	družstvo
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
proti	proti	k7c3	proti
nejbližšímu	blízký	k2eAgInSc3d3	Nejbližší
druhému	druhý	k4xOgInSc3	druhý
nejvýše	vysoce	k6eAd3	vysoce
umístěnému	umístěný	k2eAgInSc3d1	umístěný
týmu	tým	k1gInSc3	tým
mimo	mimo	k7c4	mimo
týmů	tým	k1gInPc2	tým
v	v	k7c6	v
minitabulce	minitabulka	k1gFnSc6	minitabulka
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
více	hodně	k6eAd2	hodně
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
<g/>
)	)	kIx)	)
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
družstvu	družstvo	k1gNnSc3	družstvo
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíV	mistrovstvíV	k?	mistrovstvíV
průběhu	průběh	k1gInSc2	průběh
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgInP	být
sehrány	sehrát	k5eAaPmNgInP	sehrát
všechny	všechen	k3xTgInPc1	všechen
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bodové	bodový	k2eAgFnSc2d1	bodová
rovnosti	rovnost	k1gFnSc2	rovnost
tato	tento	k3xDgNnPc1	tento
kritéria	kritérion	k1gNnPc1	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nižší	nízký	k2eAgInSc1d2	nižší
počet	počet	k1gInSc1	počet
odehraných	odehraný	k2eAgNnPc2d1	odehrané
utkání	utkání	k1gNnPc2	utkání
</s>
</p>
<p>
<s>
Brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíČtvrtfinále	mistrovstvíČtvrtfinále	k1gNnSc2	mistrovstvíČtvrtfinále
bylo	být	k5eAaImAgNnS	být
sehráno	sehrát	k5eAaPmNgNnS	sehrát
křížovým	křížový	k2eAgInSc7d1	křížový
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
1	[number]	k4	1
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
proti	proti	k7c3	proti
4	[number]	k4	4
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
proti	proti	k7c3	proti
3	[number]	k4	3
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
proti	proti	k7c3	proti
4	[number]	k4	4
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
tým	tým	k1gInSc1	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
proti	proti	k7c3	proti
3	[number]	k4	3
<g/>
.	.	kIx.	.
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
poražené	poražený	k1gMnPc4	poražený
čtvrtfinalisty	čtvrtfinalista	k1gMnPc4	čtvrtfinalista
turnaj	turnaj	k1gInSc4	turnaj
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
se	se	k3xPyFc4	se
dvojice	dvojice	k1gFnSc1	dvojice
utkaly	utkat	k5eAaPmAgInP	utkat
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
vzorci	vzorec	k1gInSc6	vzorec
<g/>
:	:	kIx,	:
vítěz	vítěz	k1gMnSc1	vítěz
utkání	utkání	k1gNnSc4	utkání
mezi	mezi	k7c4	mezi
1A	[number]	k4	1A
-	-	kIx~	-
4B	[number]	k4	4B
vs	vs	k?	vs
vítěz	vítěz	k1gMnSc1	vítěz
2B	[number]	k4	2B
-	-	kIx~	-
3	[number]	k4	3
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
1B	[number]	k4	1B
-	-	kIx~	-
4A	[number]	k4	4A
vs	vs	k?	vs
vítěz	vítěz	k1gMnSc1	vítěz
2A	[number]	k4	2A
-	-	kIx~	-
3	[number]	k4	3
<g/>
B.	B.	kA	B.
Oba	dva	k4xCgInPc1	dva
semifinálové	semifinálový	k2eAgInPc1d1	semifinálový
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
O2	O2	k1gFnSc6	O2
areně	areeň	k1gFnSc2	areeň
<g/>
.	.	kIx.	.
</s>
<s>
Vítězní	vítězný	k2eAgMnPc1d1	vítězný
semifinalisté	semifinalista	k1gMnPc1	semifinalista
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c4	o
držitelích	držitel	k1gMnPc6	držitel
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
poražení	poražený	k2eAgMnPc1d1	poražený
semifinalisté	semifinalista	k1gMnPc1	semifinalista
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Systém	systém	k1gInSc1	systém
prodloužení	prodloužení	k1gNnSc2	prodloužení
v	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vyrovnaného	vyrovnaný	k2eAgInSc2d1	vyrovnaný
stavu	stav	k1gInSc2	stav
i	i	k9	i
po	po	k7c6	po
šedesáti	šedesát	k4xCc6	šedesát
minutách	minuta	k1gFnPc6	minuta
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
semifinále	semifinále	k1gNnSc2	semifinále
nebo	nebo	k8xC	nebo
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
zápas	zápas	k1gInSc4	zápas
prodlužoval	prodlužovat	k5eAaImAgInS	prodlužovat
o	o	k7c4	o
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
tříminutové	tříminutový	k2eAgFnSc6d1	tříminutová
přestávce	přestávka	k1gFnSc6	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
by	by	kYmCp3nS	by
následovalo	následovat	k5eAaImAgNnS	následovat
dvacetiminutové	dvacetiminutový	k2eAgNnSc1d1	dvacetiminutové
prodloužení	prodloužení	k1gNnSc1	prodloužení
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterým	který	k3yIgNnSc7	který
by	by	kYmCp3nS	by
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
patnáctiminutová	patnáctiminutový	k2eAgFnSc1d1	patnáctiminutová
přestávka	přestávka	k1gFnSc1	přestávka
s	s	k7c7	s
úpravou	úprava	k1gFnSc7	úprava
ledové	ledový	k2eAgFnSc2d1	ledová
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nP	by
během	během	k7c2	během
prodloužení	prodloužení	k1gNnSc2	prodloužení
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
týmů	tým	k1gInPc2	tým
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
branky	branka	k1gFnPc4	branka
<g/>
,	,	kIx,	,
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
samostatné	samostatný	k2eAgInPc1d1	samostatný
nájezdy	nájezd	k1gInPc1	nájezd
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
určily	určit	k5eAaPmAgFnP	určit
vítěze	vítěz	k1gMnPc4	vítěz
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritéria	kritérion	k1gNnPc4	kritérion
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
konečného	konečný	k2eAgNnSc2d1	konečné
pořadí	pořadí	k1gNnSc2	pořadí
týmů	tým	k1gInPc2	tým
===	===	k?	===
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
čtyřech	čtyři	k4xCgNnPc6	čtyři
místech	místo	k1gNnPc6	místo
určil	určit	k5eAaPmAgInS	určit
výsledek	výsledek	k1gInSc1	výsledek
finálového	finálový	k2eAgInSc2d1	finálový
zápasu	zápas	k1gInSc2	zápas
a	a	k8xC	a
utkání	utkání	k1gNnSc2	utkání
o	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
konečném	konečný	k2eAgNnSc6d1	konečné
umístění	umístění	k1gNnSc6	umístění
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
tato	tento	k3xDgNnPc4	tento
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
postavení	postavení	k1gNnSc1	postavení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
</s>
</p>
<p>
<s>
Lepší	dobrý	k2eAgInSc1d2	lepší
brankový	brankový	k2eAgInSc1d1	brankový
rozdíl	rozdíl	k1gInSc1	rozdíl
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
IIHF	IIHF	kA	IIHF
před	před	k7c7	před
startem	start	k1gInSc7	start
mistrovstvíPoznámka	mistrovstvíPoznámka	k1gFnSc1	mistrovstvíPoznámka
<g/>
:	:	kIx,	:
Poražení	poražený	k2eAgMnPc1d1	poražený
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1	čtvrtfinalista
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
automaticky	automaticky	k6eAd1	automaticky
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
seřazeni	seřadit	k5eAaPmNgMnP	seřadit
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
skupinách	skupina	k1gFnPc6	skupina
dle	dle	k7c2	dle
kritérií	kritérion	k1gNnPc2	kritérion
uvedených	uvedený	k2eAgFnPc2d1	uvedená
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legenda	legenda	k1gFnSc1	legenda
==	==	k?	==
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
vysvětlivek	vysvětlivka	k1gFnPc2	vysvětlivka
použitých	použitý	k2eAgInPc2d1	použitý
v	v	k7c6	v
souhrnech	souhrn	k1gInPc6	souhrn
odehraných	odehraný	k2eAgInPc2d1	odehraný
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
A	a	k8xC	a
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
O	o	k7c6	o
sestupu	sestup	k1gInSc6	sestup
Rakouska	Rakousko	k1gNnSc2	Rakousko
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
minitabulka	minitabulka	k1gFnSc1	minitabulka
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
měla	mít	k5eAaImAgFnS	mít
Francie	Francie	k1gFnSc1	Francie
5	[number]	k4	5
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
3	[number]	k4	3
body	bod	k1gInPc1	bod
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
1	[number]	k4	1
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
časy	čas	k1gInPc1	čas
zápasů	zápas	k1gInPc2	zápas
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
ve	v	k7c6	v
středoevropském	středoevropský	k2eAgInSc6d1	středoevropský
letním	letní	k2eAgInSc6d1	letní
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
+2	+2	k4	+2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
časy	čas	k1gInPc1	čas
zápasů	zápas	k1gInPc2	zápas
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
ve	v	k7c6	v
středoevropském	středoevropský	k2eAgInSc6d1	středoevropský
letním	letní	k2eAgInSc6d1	letní
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
+2	+2	k4	+2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Play	play	k0	play
off	off	k?	off
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pavouk	pavouk	k1gMnSc1	pavouk
===	===	k?	===
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
časy	čas	k1gInPc1	čas
zápasů	zápas	k1gInPc2	zápas
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
ve	v	k7c6	v
středoevropském	středoevropský	k2eAgInSc6d1	středoevropský
letním	letní	k2eAgInSc6d1	letní
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
+2	+2	k4	+2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Semifinále	semifinále	k1gNnPc3	semifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčské	hráčský	k2eAgFnPc1d1	hráčská
statistiky	statistika	k1gFnPc1	statistika
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc1	hodnocení
hráčů	hráč	k1gMnPc2	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
podle	podle	k7c2	podle
direktoriátu	direktoriát	k1gInSc2	direktoriát
IIHF	IIHF	kA	IIHF
===	===	k?	===
</s>
</p>
<p>
<s>
Reference	reference	k1gFnSc1	reference
<g/>
:	:	kIx,	:
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
===	===	k?	===
All	All	k1gFnPc2	All
Stars	Stars	k1gInSc1	Stars
===	===	k?	===
</s>
</p>
<p>
<s>
Reference	reference	k1gFnSc1	reference
<g/>
:	:	kIx,	:
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
===	===	k?	===
Kanadské	kanadský	k2eAgNnSc4d1	kanadské
bodování	bodování	k1gNnSc4	bodování
===	===	k?	===
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
pořadí	pořadí	k1gNnSc1	pořadí
hráčů	hráč	k1gMnPc2	hráč
podle	podle	k7c2	podle
dosažených	dosažený	k2eAgInPc2d1	dosažený
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
vstřelený	vstřelený	k2eAgInSc4d1	vstřelený
gól	gól	k1gInSc4	gól
nebo	nebo	k8xC	nebo
přihrávku	přihrávka	k1gFnSc4	přihrávka
na	na	k7c4	na
gól	gól	k1gInSc4	gól
hráč	hráč	k1gMnSc1	hráč
získal	získat	k5eAaPmAgMnS	získat
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záp	Záp	k?	Záp
<g/>
.	.	kIx.	.
=	=	kIx~	=
Odehrané	odehraný	k2eAgInPc1d1	odehraný
zápasy	zápas	k1gInPc1	zápas
<g/>
;	;	kIx,	;
G	G	kA	G
=	=	kIx~	=
Góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
A	a	k8xC	a
=	=	kIx~	=
Přihrávky	přihrávka	k1gFnSc2	přihrávka
na	na	k7c4	na
gól	gól	k1gInSc4	gól
<g/>
;	;	kIx,	;
Body	bod	k1gInPc4	bod
=	=	kIx~	=
Body	bod	k1gInPc4	bod
<g/>
;	;	kIx,	;
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
−	−	k?	−
=	=	kIx~	=
Plus	plus	k1gInSc1	plus
<g/>
/	/	kIx~	/
<g/>
Minus	minus	k1gInSc1	minus
<g/>
;	;	kIx,	;
PTM	PTM	kA	PTM
=	=	kIx~	=
Počet	počet	k1gInSc1	počet
trestných	trestný	k2eAgFnPc2d1	trestná
minut	minuta	k1gFnPc2	minuta
<g/>
;	;	kIx,	;
Poz	Poz	k1gFnSc1	Poz
<g/>
.	.	kIx.	.
=	=	kIx~	=
Pozice	pozice	k1gFnSc1	pozice
</s>
</p>
<p>
<s>
===	===	k?	===
Hodnocení	hodnocení	k1gNnSc1	hodnocení
brankářů	brankář	k1gMnPc2	brankář
===	===	k?	===
</s>
</p>
<p>
<s>
Pořadí	pořadí	k1gNnSc4	pořadí
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
pěti	pět	k4xCc2	pět
brankářů	brankář	k1gMnPc2	brankář
podle	podle	k7c2	podle
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
zásahů	zásah	k1gInPc2	zásah
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
.	.	kIx.	.
</s>
<s>
Brankář	brankář	k1gMnSc1	brankář
musel	muset	k5eAaImAgMnS	muset
mít	mít	k5eAaImF	mít
odehráno	odehrát	k5eAaPmNgNnS	odehrát
minimálně	minimálně	k6eAd1	minimálně
40	[number]	k4	40
<g/>
%	%	kIx~	%
hrací	hrací	k2eAgFnSc2d1	hrací
doby	doba	k1gFnSc2	doba
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záp	Záp	k?	Záp
<g/>
.	.	kIx.	.
=	=	kIx~	=
Odehrané	odehraný	k2eAgInPc1d1	odehraný
zápasy	zápas	k1gInPc1	zápas
<g/>
;	;	kIx,	;
Čas	čas	k1gInSc1	čas
=	=	kIx~	=
Čas	čas	k1gInSc1	čas
na	na	k7c6	na
ledě	led	k1gInSc6	led
(	(	kIx(	(
<g/>
minuty	minuta	k1gFnSc2	minuta
<g/>
/	/	kIx~	/
<g/>
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
OG	OG	kA	OG
=	=	kIx~	=
Obdržené	obdržený	k2eAgInPc1d1	obdržený
góly	gól	k1gInPc1	gól
<g/>
;	;	kIx,	;
PZ	PZ	kA	PZ
=	=	kIx~	=
Počet	počet	k1gInSc1	počet
zásahů	zásah	k1gInPc2	zásah
<g/>
;	;	kIx,	;
ČK	ČK	kA	ČK
=	=	kIx~	=
Čistá	čistá	k1gFnSc1	čistá
konta	konto	k1gNnSc2	konto
<g/>
;	;	kIx,	;
Úsp	Úsp	k1gFnSc1	Úsp
<g/>
%	%	kIx~	%
=	=	kIx~	=
Procento	procento	k1gNnSc1	procento
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
zásahů	zásah	k1gInPc2	zásah
<g/>
;	;	kIx,	;
POG	POG	kA	POG
=	=	kIx~	=
Průměr	průměr	k1gInSc1	průměr
obdržených	obdržený	k2eAgInPc2d1	obdržený
gólů	gól	k1gInPc2	gól
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
Náhledy	náhled	k1gInPc1	náhled
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Česko	Česko	k1gNnSc1	Česko
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Domácí	domácí	k2eAgInSc1d1	domácí
výběr	výběr	k1gInSc1	výběr
se	se	k3xPyFc4	se
na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
světového	světový	k2eAgInSc2d1	světový
šampionátu	šampionát	k1gInSc2	šampionát
střetl	střetnout	k5eAaPmAgInS	střetnout
s	s	k7c7	s
hokejovou	hokejový	k2eAgFnSc7d1	hokejová
reprezentací	reprezentace	k1gFnSc7	reprezentace
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Dramatickou	dramatický	k2eAgFnSc4d1	dramatická
bitvu	bitva	k1gFnSc4	bitva
před	před	k7c7	před
vyprodaným	vyprodaný	k2eAgInSc7d1	vyprodaný
stadionem	stadion	k1gInSc7	stadion
český	český	k2eAgInSc1d1	český
hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
prohrál	prohrát	k5eAaPmAgInS	prohrát
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
po	po	k7c6	po
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
domácí	domácí	k2eAgFnSc1d1	domácí
reprezentace	reprezentace	k1gFnSc1	reprezentace
utkala	utkat	k5eAaPmAgFnS	utkat
s	s	k7c7	s
Lotyši	Lotyš	k1gMnPc7	Lotyš
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
Češi	Čech	k1gMnPc1	Čech
udolali	udolat	k5eAaPmAgMnP	udolat
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
zápas	zápas	k1gInSc1	zápas
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
výběr	výběr	k1gInSc1	výběr
prohrál	prohrát	k5eAaPmAgInS	prohrát
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
testem	test	k1gInSc7	test
prošel	projít	k5eAaPmAgInS	projít
domácí	domácí	k2eAgInSc1d1	domácí
výběr	výběr	k1gInSc1	výběr
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Česko	Česko	k1gNnSc1	Česko
nakonec	nakonec	k6eAd1	nakonec
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
Češi	Čech	k1gMnPc1	Čech
porazili	porazit	k5eAaPmAgMnP	porazit
reprezentaci	reprezentace	k1gFnSc4	reprezentace
Rakouska	Rakousko	k1gNnSc2	Rakousko
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předposledním	předposlední	k2eAgInSc6d1	předposlední
zápase	zápas	k1gInSc6	zápas
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
si	se	k3xPyFc3	se
Češi	česat	k5eAaImIp1nS	česat
výhrou	výhra	k1gFnSc7	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
nad	nad	k7c7	nad
Německem	Německo	k1gNnSc7	Německo
zajistili	zajistit	k5eAaPmAgMnP	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
utkání	utkání	k1gNnSc1	utkání
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
česká	český	k2eAgFnSc1d1	Česká
sestava	sestava	k1gFnSc1	sestava
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
po	po	k7c6	po
samostatných	samostatný	k2eAgInPc6d1	samostatný
nájezdech	nájezd	k1gInPc6	nájezd
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinálový	čtvrtfinálový	k2eAgInSc1d1	čtvrtfinálový
zápas	zápas	k1gInSc1	zápas
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
Česko	Česko	k1gNnSc1	Česko
ve	v	k7c6	v
vypjaté	vypjatý	k2eAgFnSc6d1	vypjatá
atmosféře	atmosféra	k1gFnSc6	atmosféra
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Češi	Čech	k1gMnPc1	Čech
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
favorizovanému	favorizovaný	k2eAgInSc3d1	favorizovaný
výběru	výběr	k1gInSc3	výběr
Kanady	Kanada	k1gFnSc2	Kanada
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zápase	zápas	k1gInSc6	zápas
ovšem	ovšem	k9	ovšem
reprezentanti	reprezentant	k1gMnPc1	reprezentant
Česka	Česko	k1gNnSc2	Česko
gól	gól	k1gInSc4	gól
dali	dát	k5eAaPmAgMnP	dát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
Bulanov	Bulanov	k1gInSc1	Bulanov
gól	gól	k1gInSc4	gól
neuznal	uznat	k5eNaPmAgInS	uznat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
sice	sice	k8xC	sice
Češi	Čech	k1gMnPc1	Čech
přestříleli	přestřílet	k5eAaPmAgMnP	přestřílet
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
39	[number]	k4	39
ku	k	k7c3	k
16	[number]	k4	16
ale	ale	k8xC	ale
přesto	přesto	k6eAd1	přesto
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
výběru	výběr	k1gInSc2	výběr
USA	USA	kA	USA
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
tak	tak	k6eAd1	tak
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
I	I	kA	I
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
II	II	kA	II
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc2	divize
III	III	kA	III
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2015	[number]	k4	2015
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
MS	MS	kA	MS
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
2015	[number]	k4	2015
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
www.hokejportal.cz	www.hokejportal.cz	k1gMnSc1	www.hokejportal.cz
–	–	k?	–
výsledky	výsledek	k1gInPc1	výsledek
<g/>
,	,	kIx,	,
novinky	novinka	k1gFnPc1	novinka
a	a	k8xC	a
kompletní	kompletní	k2eAgNnPc1d1	kompletní
zpravodajství	zpravodajství	k1gNnPc1	zpravodajství
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
www.hokej.cz	www.hokej.cz	k1gMnSc1	www.hokej.cz
–	–	k?	–
výsledky	výsledek	k1gInPc1	výsledek
<g/>
,	,	kIx,	,
novinky	novinka	k1gFnPc1	novinka
a	a	k8xC	a
kompletní	kompletní	k2eAgNnPc1d1	kompletní
zpravodajství	zpravodajství	k1gNnPc1	zpravodajství
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
