<s>
Národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
stranaNationalsozialistische	stranaNationalsozialistische	k1gFnSc1
Deutsche	Deutsch	k1gInSc2
Arbeiterpartei	Arbeiterparte	k1gFnSc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
NSDAP	NSDAP	kA
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1920	#num#	k4
Datum	datum	k1gNnSc4
rozpuštění	rozpuštění	k1gNnSc2
</s>
<s>
1945	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Anton	Anton	k1gMnSc1
Drexler	Drexler	k1gMnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
<g/>
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
Martin	Martin	k1gMnSc1
Bormann	Bormann	k1gMnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Anton	Anton	k1gMnSc1
Drexler	Drexler	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Německá	německý	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Ideologie	ideologie	k1gFnSc2
</s>
<s>
NacismusFašismusPangermanismus	NacismusFašismusPangermanismus	k1gInSc1
Antisemitismus	antisemitismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
Antikomunismus	antikomunismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
Krajní	krajní	k2eAgFnSc1d1
pravice	pravice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Studentská	studentská	k1gFnSc1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Nationalsozialistischer	Nationalsozialistischra	k1gFnPc2
Deutscher	Deutschra	k1gFnPc2
Studentenbund	Studentenbunda	k1gFnPc2
Mládežnická	mládežnický	k2eAgFnSc1d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Hitlerjugend	Hitlerjugend	k1gMnSc1
</s>
<s>
Deutsches	Deutsches	k1gMnSc1
Jungvolk	Jungvolk	k1gMnSc1
</s>
<s>
Svaz	svaz	k1gInSc1
německých	německý	k2eAgFnPc2d1
dívek	dívka	k1gFnPc2
Polovojenská	polovojenský	k2eAgFnSc1d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Sturmabteilung	Sturmabteilung	k1gMnSc1
(	(	kIx(
<g/>
SA	SA	kA
<g/>
)	)	kIx)
<g/>
Schutzstaffel	Schutzstaffel	k1gMnSc1
(	(	kIx(
<g/>
SS	SS	kA
<g/>
)	)	kIx)
Stranické	stranický	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
</s>
<s>
Völkischer	Völkischra	k1gFnPc2
Beobachter	Beobachtra	k1gFnPc2
Počet	počet	k1gInSc1
členů	člen	k1gMnPc2
</s>
<s>
Méně	málo	k6eAd2
než	než	k8xS
60	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
<g/>
)	)	kIx)
8,5	8,5	k4
milionů	milion	k4xCgInPc2
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
)	)	kIx)
Slogan	slogan	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Ein	Ein	k1gMnSc1
Volk	Volk	k1gMnSc1
<g/>
,	,	kIx,
Ein	Ein	k1gMnSc1
Reich	Reich	k?
<g/>
,	,	kIx,
Ein	Ein	k1gMnSc1
Führer	Führer	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
Jeden	jeden	k4xCgInSc1
lid	lid	k1gInSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
vůdce	vůdce	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
Barvy	barva	k1gFnSc2
</s>
<s>
Červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
a	a	k8xC
černá	černý	k2eAgFnSc1d1
<g/>
,	,	kIx,
též	též	k9
hnědá	hnědý	k2eAgFnSc1d1
(	(	kIx(
<g/>
SA	SA	kA
<g/>
)	)	kIx)
Volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
</s>
<s>
43,9	43,9	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
)	)	kIx)
Zisk	zisk	k1gInSc1
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
491	#num#	k4
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
107	#num#	k4
<g/>
/	/	kIx~
<g/>
577	#num#	k4
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
(	(	kIx(
<g/>
červenec	červenec	k1gInSc1
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
230	#num#	k4
<g/>
/	/	kIx~
<g/>
608	#num#	k4
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
196	#num#	k4
<g/>
/	/	kIx~
<g/>
584	#num#	k4
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
(	(	kIx(
<g/>
březen	březen	k1gInSc1
1933	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
288	#num#	k4
<g/>
/	/	kIx~
<g/>
647	#num#	k4
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
1933	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
661	#num#	k4
<g/>
/	/	kIx~
<g/>
661	#num#	k4
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
741	#num#	k4
<g/>
/	/	kIx~
<g/>
741	#num#	k4
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
813	#num#	k4
<g/>
/	/	kIx~
<g/>
813	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
strany	strana	k1gFnSc2
</s>
<s>
Národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
:	:	kIx,
Nationalsozialistische	Nationalsozialistische	k2eAgFnSc1d1
Deutsche	Deutsch	k2eAgFnSc1d1
Arbeiterpartei	Arbeiterparte	k1gFnSc1
<g/>
,	,	kIx,
NSDAP	NSDAP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
nacistická	nacistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
německá	německý	k2eAgFnSc1d1
krajně	krajně	k6eAd1
pravicová	pravicový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1920	#num#	k4
přejmenováním	přejmenování	k1gNnSc7
Německé	německý	k2eAgFnSc2d1
dělnické	dělnický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
již	již	k6eAd1
založili	založit	k5eAaPmAgMnP
Anton	Anton	k1gMnSc1
Drexler	Drexler	k1gMnSc1
a	a	k8xC
Karl	Karl	k1gMnSc1
Harrer	Harrer	k1gMnSc1
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1919	#num#	k4
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnPc1
členové	člen	k1gMnPc1
se	se	k3xPyFc4
nazývali	nazývat	k5eAaImAgMnP
nacisté	nacista	k1gMnPc1
(	(	kIx(
<g/>
spojení	spojení	k1gNnSc1
<g/>
/	/	kIx~
<g/>
zkratka	zkratka	k1gFnSc1
slov	slovo	k1gNnPc2
nacionální	nacionální	k2eAgInSc1d1
a	a	k8xC
socialisté	socialist	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
hlavním	hlavní	k2eAgMnPc3d1
znakům	znak	k1gInPc3
nacismu	nacismus	k1gInSc2
patřil	patřit	k5eAaImAgInS
krajní	krajní	k2eAgInSc1d1
šovinismus	šovinismus	k1gInSc1
<g/>
,	,	kIx,
revanšismus	revanšismus	k1gInSc1
<g/>
,	,	kIx,
rasismus	rasismus	k1gInSc1
<g/>
,	,	kIx,
antisemitismus	antisemitismus	k1gInSc1
a	a	k8xC
pangermanismus	pangermanismus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
používala	používat	k5eAaImAgFnS
sociální	sociální	k2eAgFnSc3d1
a	a	k8xC
nacionální	nacionální	k2eAgFnSc3d1
demagogii	demagogie	k1gFnSc3
a	a	k8xC
teror	teror	k1gInSc1
proti	proti	k7c3
svým	svůj	k3xOyFgMnPc3
skutečným	skutečný	k2eAgMnPc3d1
či	či	k8xC
domnělým	domnělý	k2eAgMnPc3d1
oponentům	oponent	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propagandisticky	propagandisticky	k6eAd1
těžila	těžit	k5eAaImAgFnS
jednak	jednak	k8xC
z	z	k7c2
odmítání	odmítání	k1gNnPc2
mírových	mírový	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
ukončujících	ukončující	k2eAgFnPc2d1
první	první	k4xOgFnSc4
světovou	světový	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc4
vítězové	vítěz	k1gMnPc1
po	po	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
státu	stát	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
válku	válka	k1gFnSc4
rozpoutal	rozpoutat	k5eAaPmAgMnS
<g/>
,	,	kIx,
požadovali	požadovat	k5eAaImAgMnP
vysoké	vysoký	k2eAgFnPc4d1
válečné	válečný	k2eAgFnPc4d1
reparace	reparace	k1gFnPc4
na	na	k7c4
úhradu	úhrada	k1gFnSc4
způsobených	způsobený	k2eAgFnPc2d1
škod	škoda	k1gFnPc2
a	a	k8xC
další	další	k2eAgNnSc4d1
omezení	omezení	k1gNnSc4
<g/>
,	,	kIx,
jednak	jednak	k8xC
z	z	k7c2
antisemitismu	antisemitismus	k1gInSc2
a	a	k8xC
šovinismu	šovinismus	k1gInSc2
části	část	k1gFnSc2
Němců	Němec	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
konečně	konečně	k6eAd1
z	z	k7c2
ekonomických	ekonomický	k2eAgInPc2d1
dopadů	dopad	k1gInPc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
hyperinflace	hyperinflace	k1gFnSc2
a	a	k8xC
později	pozdě	k6eAd2
také	také	k9
Velké	velká	k1gFnSc2
hospodářská	hospodářský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
na	na	k7c4
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
čele	čelo	k1gNnSc6
NSDAP	NSDAP	kA
stál	stát	k5eAaImAgInS
po	po	k7c4
většinu	většina	k1gFnSc4
její	její	k3xOp3gFnSc2
existence	existence	k1gFnSc2
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
zavedl	zavést	k5eAaPmAgMnS
v	v	k7c6
Německu	Německo	k1gNnSc6
nacistickou	nacistický	k2eAgFnSc4d1
totalitu	totalita	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
u	u	k7c2
moci	moc	k1gFnSc2
držela	držet	k5eAaImAgFnS
12	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
Německo	Německo	k1gNnSc1
poraženo	porazit	k5eAaPmNgNnS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
byla	být	k5eAaImAgFnS
nacistická	nacistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Mezinárodním	mezinárodnět	k5eAaPmIp1nS
vojenským	vojenský	k2eAgInSc7d1
tribunálem	tribunál	k1gInSc7
během	během	k7c2
Norimberského	norimberský	k2eAgInSc2d1
procesu	proces	k1gInSc2
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c4
zločineckou	zločinecký	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Delegace	delegace	k1gFnSc1
NSDAP	NSDAP	kA
v	v	k7c6
Coburgu	Coburg	k1gInSc6
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
1922	#num#	k4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1919	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Německu	Německo	k1gNnSc6
přijata	přijat	k2eAgFnSc1d1
Výmarská	výmarský	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
federativní	federativní	k2eAgFnSc1d1
parlamentní	parlamentní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Výmarská	výmarský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonodárnou	zákonodárný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
tvořil	tvořit	k5eAaImAgInS
Reichstag	Reichstag	k1gInSc1
(	(	kIx(
<g/>
dolní	dolní	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
nějž	jenž	k3xRgNnSc2
se	se	k3xPyFc4
volilo	volit	k5eAaImAgNnS
poměrným	poměrný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
a	a	k8xC
každý	každý	k3xTgMnSc1
poslanec	poslanec	k1gMnSc1
představoval	představovat	k5eAaImAgMnS
60	#num#	k4
000	#num#	k4
voličů	volič	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
horní	horní	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
scházeli	scházet	k5eAaImAgMnP
zástupci	zástupce	k1gMnPc1
17	#num#	k4
spolkových	spolkový	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkonnou	výkonný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
představoval	představovat	k5eAaImAgInS
kabinet	kabinet	k1gInSc1
s	s	k7c7
kancléřem	kancléř	k1gMnSc7
v	v	k7c6
čele	čelo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
<g/>
,	,	kIx,
hlava	hlava	k1gFnSc1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
volen	volit	k5eAaImNgInS
na	na	k7c4
7	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
jmenoval	jmenovat	k5eAaImAgInS,k5eAaBmAgInS
a	a	k8xC
odvolával	odvolávat	k5eAaImAgInS
kancléře	kancléř	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
volit	volit	k5eAaImF
měli	mít	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
němečtí	německý	k2eAgMnPc1d1
občané	občan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
mužského	mužský	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
a	a	k8xC
starší	starý	k2eAgMnPc1d2
20	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
po	po	k7c6
skončení	skončení	k1gNnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
Německo	Německo	k1gNnSc1
stálo	stát	k5eAaImAgNnS
na	na	k7c6
straně	strana	k1gFnSc6
poražených	poražený	k1gMnPc2
<g/>
,	,	kIx,
založili	založit	k5eAaPmAgMnP
Anton	Anton	k1gMnSc1
Drexler	Drexler	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
Harrer	Harrer	k1gMnSc1
a	a	k8xC
Gottfried	Gottfried	k1gMnSc1
Feder	Feder	k1gMnSc1
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
Německou	německý	k2eAgFnSc4d1
dělnickou	dělnický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
(	(	kIx(
<g/>
Deutsche	Deutsche	k1gFnSc4
Arbeiterpartei	Arbeiterparte	k1gFnSc2
<g/>
,	,	kIx,
DAP	DAP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
německý	německý	k2eAgInSc4d1
stát	stát	k1gInSc4
to	ten	k3xDgNnSc1
nebyla	být	k5eNaImAgFnS
ideální	ideální	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemi	zem	k1gFnSc6
vyčerpala	vyčerpat	k5eAaPmAgFnS
válka	válka	k1gFnSc1
a	a	k8xC
chaos	chaos	k1gInSc1
vzniklý	vzniklý	k2eAgInSc1d1
v	v	k7c6
jejím	její	k3xOp3gInSc6
důsledku	důsledek	k1gInSc6
se	se	k3xPyFc4
těžko	těžko	k6eAd1
dává	dávat	k5eAaImIp3nS
do	do	k7c2
pořádku	pořádek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
byla	být	k5eAaImAgNnP
na	na	k7c4
spadnutí	spadnutí	k1gNnSc4
ratifikace	ratifikace	k1gFnSc2
Versailleské	versailleský	k2eAgFnSc2d1
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
posunula	posunout	k5eAaPmAgFnS
hranice	hranice	k1gFnPc4
v	v	k7c4
neprospěch	neprospěch	k1gInSc4
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
uložila	uložit	k5eAaPmAgFnS
mu	on	k3xPp3gInSc3
platit	platit	k5eAaImF
velké	velký	k2eAgFnPc1d1
válečné	válečný	k2eAgFnPc1d1
reparace	reparace	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
pomlalu	pomlal	k1gMnSc3
přiváděly	přivádět	k5eAaImAgInP
stát	stát	k5eAaPmF,k5eAaImF
do	do	k7c2
kolapsu	kolaps	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
redukovat	redukovat	k5eAaBmF
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
působil	působit	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
jako	jako	k8xS,k8xC
agent	agent	k1gMnSc1
reichswehru	reichswehra	k1gFnSc4
(	(	kIx(
<g/>
německé	německý	k2eAgFnPc1d1
armády	armáda	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
měl	mít	k5eAaImAgMnS
za	za	k7c4
úkol	úkol	k1gInSc4
sledovat	sledovat	k5eAaImF
mnichovské	mnichovský	k2eAgFnPc4d1
extremistické	extremistický	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Jednou	jednou	k6eAd1
z	z	k7c2
těchto	tento	k3xDgFnPc2
sledovaných	sledovaný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
byla	být	k5eAaImAgFnS
i	i	k9
nacionalistická	nacionalistický	k2eAgNnPc4d1
DAP	DAP	kA
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1919	#num#	k4
Hitler	Hitler	k1gMnSc1
vstoupil	vstoupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
měla	mít	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
mezi	mezi	k7c7
20	#num#	k4
až	až	k9
40	#num#	k4
členy	člen	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Nacionalistické	nacionalistický	k2eAgInPc4d1
a	a	k8xC
antisemitské	antisemitský	k2eAgInPc4d1
názory	názor	k1gInPc4
panující	panující	k2eAgInPc4d1
ve	v	k7c6
vedení	vedení	k1gNnSc6
strany	strana	k1gFnSc2
lahodily	lahodit	k5eAaImAgFnP
uchu	ucha	k1gFnSc4
nově	nova	k1gFnSc3
příchozího	příchozí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
však	však	k9
zvedl	zvednout	k5eAaPmAgMnS
jakýsi	jakýsi	k3yIgMnSc1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
jméno	jméno	k1gNnSc4
už	už	k6eAd1
neznáme	neznat	k5eAaImIp1nP,k5eNaImIp1nP
<g/>
,	,	kIx,
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
špatnou	špatný	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
Německa	Německo	k1gNnSc2
je	být	k5eAaImIp3nS
zodpovědná	zodpovědný	k2eAgFnSc1d1
politika	politika	k1gFnSc1
Pruska	Prusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
Bavorsko	Bavorsko	k1gNnSc1
od	od	k7c2
Pruska	Prusko	k1gNnSc2
oddělit	oddělit	k5eAaPmF
a	a	k8xC
spojit	spojit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
Rakušáky	Rakušáky	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc6
Adolf	Adolf	k1gMnSc1
vstal	vstát	k5eAaPmAgMnS
a	a	k8xC
onoho	onen	k3xDgMnSc2
člověka	člověk	k1gMnSc2
pořádně	pořádně	k6eAd1
splísnil	splísnit	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
viník	viník	k1gMnSc1
není	být	k5eNaImIp3nS
na	na	k7c6
straně	strana	k1gFnSc6
Pruska	Prusko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
straně	strana	k1gFnSc6
Židů	Žid	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
odcházel	odcházet	k5eAaImAgMnS
naštvaný	naštvaný	k2eAgMnSc1d1
ze	z	k7c2
schůze	schůze	k1gFnSc2
DAP	DAP	kA
<g/>
,	,	kIx,
zastavil	zastavit	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
Anton	Anton	k1gMnSc1
Drexler	Drexler	k1gMnSc1
a	a	k8xC
dal	dát	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
brožuru	brožura	k1gFnSc4
s	s	k7c7
nápisem	nápis	k1gInSc7
„	„	k?
<g/>
Mé	můj	k3xOp1gNnSc1
politické	politický	k2eAgNnSc1d1
probuzení	probuzení	k1gNnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitler	Hitler	k1gMnSc1
se	se	k3xPyFc4
na	na	k7c4
další	další	k2eAgFnSc4d1
schůzi	schůze	k1gFnSc4
DAP	DAP	kA
dostavil	dostavit	k5eAaPmAgInS
a	a	k8xC
se	s	k7c7
stranou	strana	k1gFnSc7
se	se	k3xPyFc4
postupně	postupně	k6eAd1
sžil	sžít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Strana	strana	k1gFnSc1
DAP	DAP	kA
se	se	k3xPyFc4
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c4
NSDAP	NSDAP	kA
(	(	kIx(
<g/>
původně	původně	k6eAd1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c6
Sociální	sociální	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1920	#num#	k4
byl	být	k5eAaImAgInS
zveřejněn	zveřejnit	k5eAaPmNgInS
její	její	k3xOp3gInSc1
pětadvacetibodový	pětadvacetibodový	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
na	na	k7c6
šovinismu	šovinismus	k1gInSc6
a	a	k8xC
požaduje	požadovat	k5eAaImIp3nS
například	například	k6eAd1
odmítnutí	odmítnutí	k1gNnSc4
Versailleské	versailleský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
vytvoření	vytvoření	k1gNnSc1
Velkého	velký	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
či	či	k8xC
zestátnění	zestátnění	k1gNnSc2
podniků	podnik	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
tímto	tento	k3xDgInSc7
programem	program	k1gInSc7
byl	být	k5eAaImAgInS
podepsán	podepsat	k5eAaPmNgInS
Drexler	Drexler	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
program	program	k1gInSc1
už	už	k6eAd1
byl	být	k5eAaImAgInS
částečně	částečně	k6eAd1
ovlivněn	ovlivnit	k5eAaPmNgInS
i	i	k9
názory	názor	k1gInPc1
Adolfa	Adolf	k1gMnSc2
Hitlera	Hitler	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
čela	čelo	k1gNnSc2
strany	strana	k1gFnSc2
se	s	k7c7
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
postavil	postavit	k5eAaPmAgMnS
Hitler	Hitler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
měsíc	měsíc	k1gInSc4
od	od	k7c2
svého	svůj	k3xOyFgNnSc2
zvolení	zvolení	k1gNnSc2
založil	založit	k5eAaPmAgInS
Tělovýchovný	tělovýchovný	k2eAgInSc1d1
a	a	k8xC
sportovní	sportovní	k2eAgInSc1d1
odbor	odbor	k1gInSc1
–	–	k?
předchůdce	předchůdce	k1gMnSc2
nechvalně	chvalně	k6eNd1
známé	známý	k2eAgInPc1d1
SA	SA	kA
(	(	kIx(
<g/>
Sturmabteilung	Sturmabteilung	k1gMnSc1
–	–	k?
úderné	úderný	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
tak	tak	k9
v	v	k7c6
rukou	ruka	k1gFnPc6
polovojenskou	polovojenský	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
proti	proti	k7c3
demokratickým	demokratický	k2eAgInPc3d1
a	a	k8xC
socialistickým	socialistický	k2eAgInPc3d1
vlivům	vliv	k1gInPc3
a	a	k8xC
armáda	armáda	k1gFnSc1
i	i	k8xC
policie	policie	k1gFnSc2
byly	být	k5eAaImAgInP
vůči	vůči	k7c3
pravicovým	pravicový	k2eAgFnPc3d1
násilnostem	násilnost	k1gFnPc3
shovívavé	shovívavý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
se	se	k3xPyFc4
projevilo	projevit	k5eAaPmAgNnS
Hitlerovo	Hitlerův	k2eAgNnSc4d1
politické	politický	k2eAgNnSc4d1
nadání	nadání	k1gNnSc4
<g/>
,	,	kIx,
nejenže	nejenže	k6eAd1
schopně	schopně	k6eAd1
dosazoval	dosazovat	k5eAaImAgMnS
ideologicky	ideologicky	k6eAd1
blízké	blízký	k2eAgMnPc4d1
jedince	jedinec	k1gMnPc4
do	do	k7c2
důležitých	důležitý	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
umně	umně	k6eAd1
nakládal	nakládat	k5eAaImAgMnS
s	s	k7c7
propagandou	propaganda	k1gFnSc7
a	a	k8xC
veřejnými	veřejný	k2eAgInPc7d1
projevy	projev	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sporu	spor	k1gInSc6
s	s	k7c7
Drexlerem	Drexler	k1gInSc7
složil	složit	k5eAaPmAgMnS
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
11	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1921	#num#	k4
svou	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
ve	v	k7c6
vedení	vedení	k1gNnSc6
strany	strana	k1gFnSc2
a	a	k8xC
vymínil	vymínit	k5eAaPmAgInS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
vrátit	vrátit	k5eAaPmF
zpět	zpět	k6eAd1
do	do	k7c2
čela	čelo	k1gNnSc2
NSDAP	NSDAP	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
mu	on	k3xPp3gMnSc3
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
přiznány	přiznán	k2eAgFnPc4d1
neomezené	omezený	k2eNgFnPc4d1
kompetence	kompetence	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitler	Hitler	k1gMnSc1
tak	tak	k9
dosáhl	dosáhnout	k5eAaPmAgMnS
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
v	v	k7c6
červenci	červenec	k1gInSc6
1921	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
předsedou	předseda	k1gMnSc7
strany	strana	k1gFnSc2
s	s	k7c7
přiznanými	přiznaný	k2eAgFnPc7d1
diktátorskými	diktátorský	k2eAgFnPc7d1
pravomocemi	pravomoc	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Anton	Anton	k1gMnSc1
Drexler	Drexler	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
čestným	čestný	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
a	a	k8xC
ztratil	ztratit	k5eAaPmAgMnS
ve	v	k7c6
straně	strana	k1gFnSc6
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
v	v	k7c6
listopadu	listopad	k1gInSc6
čítalo	čítat	k5eAaImAgNnS
kolem	kolem	k7c2
3	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
nyní	nyní	k6eAd1
Hitler	Hitler	k1gMnSc1
poznal	poznat	k5eAaPmAgMnS
význam	význam	k1gInSc4
právě	právě	k9
symbolů	symbol	k1gInPc2
<g/>
,	,	kIx,
mýtů	mýtus	k1gInPc2
a	a	k8xC
rituálů	rituál	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
prohloubily	prohloubit	k5eAaPmAgFnP
víru	víra	k1gFnSc4
ve	v	k7c4
stranu	strana	k1gFnSc4
a	a	k8xC
zakoření	zakořenit	k5eAaPmIp3nS
v	v	k7c6
lidech	člověk	k1gMnPc6
nacismus	nacismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokazují	dokazovat	k5eAaImIp3nP
to	ten	k3xDgNnSc1
uniformy	uniforma	k1gFnPc1
s	s	k7c7
insigniemi	insignie	k1gFnPc7
<g/>
,	,	kIx,
stranická	stranický	k2eAgNnPc4d1
shromáždění	shromáždění	k1gNnPc4
i	i	k9
pozdrav	pozdrav	k1gInSc1
–	–	k?
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
zakázaný	zakázaný	k2eAgInSc4d1
–	–	k?
„	„	k?
<g/>
Heil	Heil	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
NSDAP	NSDAP	kA
vydávala	vydávat	k5eAaPmAgFnS,k5eAaImAgFnS
i	i	k9
noviny	novina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
nazývaly	nazývat	k5eAaImAgFnP
Völkischer	Völkischra	k1gFnPc2
Beobachter	Beobachtrum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
jeho	jeho	k3xOp3gFnSc1
výřečnost	výřečnost	k1gFnSc1
získávala	získávat	k5eAaImAgFnS
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
dál	daleko	k6eAd2
více	hodně	k6eAd2
stoupenců	stoupenec	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
pořád	pořád	k6eAd1
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
extrémistická	extrémistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
držela	držet	k5eAaImAgFnS
na	na	k7c6
okraji	okraj	k1gInSc6
volebního	volební	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přebal	přebal	k1gInSc1
knihy	kniha	k1gFnSc2
Mein	Mein	k1gMnSc1
Kampf	Kampf	k1gMnSc1
</s>
<s>
Roku	rok	k1gInSc2
1923	#num#	k4
za	za	k7c2
nepokojů	nepokoj	k1gInPc2
(	(	kIx(
<g/>
z	z	k7c2
důvodů	důvod	k1gInPc2
inflace	inflace	k1gFnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
Gustav	Gustav	k1gMnSc1
Stresemann	Stresemann	k1gMnSc1
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
kancléřem	kancléř	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
bavorská	bavorský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
mu	on	k3xPp3gMnSc3
odmítla	odmítnout	k5eAaPmAgFnS
poslušnost	poslušnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
hrozilo	hrozit	k5eAaImAgNnS
rozdělení	rozdělení	k1gNnSc1
Německa	Německo	k1gNnSc2
i	i	k9
kvůli	kvůli	k7c3
rozpolcenosti	rozpolcenost	k1gFnSc3
ostatních	ostatní	k2eAgInPc2d1
regionů	region	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využil	využít	k5eAaPmAgMnS
toho	ten	k3xDgMnSc4
Hitler	Hitler	k1gMnSc1
za	za	k7c2
podpory	podpora	k1gFnSc2
Ericha	Erich	k1gMnSc4
Ludendorffa	Ludendorff	k1gMnSc4
a	a	k8xC
přepadl	přepadnout	k5eAaPmAgMnS
s	s	k7c7
oddílem	oddíl	k1gInSc7
SA-manů	SA-man	k1gInPc2
zasedání	zasedání	k1gNnSc2
bavorské	bavorský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
v	v	k7c6
Bierhalle	Bierhall	k1gInSc6
(	(	kIx(
<g/>
proto	proto	k8xC
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
akce	akce	k1gFnSc1
nazývá	nazývat	k5eAaImIp3nS
Mnichovský	mnichovský	k2eAgInSc4d1
pivní	pivní	k2eAgInSc4d1
puč	puč	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
tak	tak	k6eAd1
jeho	jeho	k3xOp3gInSc1
první	první	k4xOgInSc1
pokus	pokus	k1gInSc1
získat	získat	k5eAaPmF
úřad	úřad	k1gInSc4
říšského	říšský	k2eAgMnSc2d1
kancléře	kancléř	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozehnání	rozehnání	k1gNnSc6
„	„	k?
<g/>
pochodu	pochod	k1gInSc2
<g/>
“	“	k?
na	na	k7c4
Mnichov	Mnichov	k1gInSc4
(	(	kIx(
<g/>
zorganizovaného	zorganizovaný	k2eAgNnSc2d1
po	po	k7c6
vzoru	vzor	k1gInSc6
Mussoliniho	Mussolini	k1gMnSc2
<g/>
)	)	kIx)
říšskou	říšský	k2eAgFnSc7d1
obranou	obrana	k1gFnSc7
byl	být	k5eAaImAgMnS
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
na	na	k7c4
5	#num#	k4
let	léto	k1gNnPc2
do	do	k7c2
vězení	vězení	k1gNnSc2
v	v	k7c6
Landsbergu	Landsberg	k1gInSc6
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1923	#num#	k4
byla	být	k5eAaImAgFnS
NSDAP	NSDAP	kA
rozpuštěna	rozpustit	k5eAaPmNgFnS
a	a	k8xC
zakázána	zakázat	k5eAaPmNgFnS
na	na	k7c6
celém	celý	k2eAgNnSc6d1
území	území	k1gNnSc6
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
byl	být	k5eAaImAgInS
trest	trest	k1gInSc1
zkrácen	zkrátit	k5eAaPmNgInS
na	na	k7c4
9	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
za	za	k7c7
mřížemi	mříž	k1gFnPc7
nenudil	nudit	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadiktoval	nadiktovat	k5eAaPmAgMnS
Rudolfu	Rudolf	k1gMnSc3
Hessovi	Hess	k1gMnSc3
knihu	kniha	k1gFnSc4
Mein	Mein	k1gMnSc1
Kampf	Kampf	k1gMnSc1
–	–	k?
v	v	k7c6
podstatě	podstata	k1gFnSc6
ideový	ideový	k2eAgInSc4d1
program	program	k1gInSc4
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
svém	svůj	k3xOyFgNnSc6
propuštění	propuštění	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
obnovil	obnovit	k5eAaPmAgMnS
NSDAP	NSDAP	kA
a	a	k8xC
v	v	k7c6
tomtéž	týž	k3xTgInSc6
roce	rok	k1gInSc6
vyšel	vyjít	k5eAaPmAgMnS
i	i	k9
Mein	Mein	k1gMnSc1
Kampf	Kampf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
4	#num#	k4
000	#num#	k4
stoupenci	stoupenec	k1gMnPc7
pronesl	pronést	k5eAaPmAgMnS
Hitler	Hitler	k1gMnSc1
svůj	svůj	k3xOyFgInSc4
projev	projev	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
označil	označit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
cíl	cíl	k1gInSc4
–	–	k?
získat	získat	k5eAaPmF
moc	moc	k6eAd1
legálními	legální	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
poloviny	polovina	k1gFnSc2
dvacátých	dvacátý	k4xOgNnPc2
let	léto	k1gNnPc2
útočil	útočit	k5eAaImAgMnS
Hitler	Hitler	k1gMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
projevech	projev	k1gInPc6
stále	stále	k6eAd1
častěji	často	k6eAd2
na	na	k7c4
marxismus	marxismus	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
pokládal	pokládat	k5eAaImAgMnS
za	za	k7c4
politický	politický	k2eAgInSc4d1
a	a	k8xC
ideologický	ideologický	k2eAgInSc4d1
produkt	produkt	k1gInSc4
židovství	židovství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Židé	Žid	k1gMnPc1
se	se	k3xPyFc4
tak	tak	k9
pro	pro	k7c4
Hitlera	Hitler	k1gMnSc4
stali	stát	k5eAaPmAgMnP
synonymem	synonymum	k1gNnSc7
bolševismu	bolševismus	k1gInSc2
a	a	k8xC
marxismus	marxismus	k1gInSc1
jím	on	k3xPp3gNnSc7
byl	být	k5eAaImAgInS
chápán	chápat	k5eAaImNgInS
jako	jako	k8xS,k8xC
politická	politický	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
judaismu	judaismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
1925	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
se	se	k3xPyFc4
vyhraněné	vyhraněný	k2eAgFnSc3d1
straně	strana	k1gFnSc3
nedařilo	dařit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
hospodářsky	hospodářsky	k6eAd1
rostlo	růst	k5eAaImAgNnS
a	a	k8xC
spělo	spět	k5eAaImAgNnS
k	k	k7c3
demokracii	demokracie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
radikální	radikální	k2eAgMnPc1d1
členové	člen	k1gMnPc1
strany	strana	k1gFnSc2
pokoušeli	pokoušet	k5eAaImAgMnP
o	o	k7c4
rozvrat	rozvrat	k1gInSc4
Výmarské	výmarský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
neznamenali	znamenat	k5eNaImAgMnP
větší	veliký	k2eAgInSc4d2
problém	problém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1928	#num#	k4
získala	získat	k5eAaPmAgFnS
strana	strana	k1gFnSc1
jen	jen	k9
12	#num#	k4
(	(	kIx(
<g/>
1,9	1,9	k4
%	%	kIx~
<g/>
)	)	kIx)
poslaneckých	poslanecký	k2eAgInPc2d1
mandátů	mandát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Stranický	stranický	k2eAgInSc1d1
odznak	odznak	k1gInSc1
</s>
<s>
Prapor	prapor	k1gInSc1
NSDAP	NSDAP	kA
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Hitler	Hitler	k1gMnSc1
s	s	k7c7
členy	člen	k1gInPc7
NSDAP	NSDAP	kA
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
</s>
<s>
S	s	k7c7
hospodářskou	hospodářský	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
se	se	k3xPyFc4
situace	situace	k1gFnSc1
změnila	změnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
NSDAP	NSDAP	kA
zažila	zažít	k5eAaPmAgFnS
volební	volební	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
ovlivnit	ovlivnit	k5eAaPmF
celý	celý	k2eAgInSc4d1
národ	národ	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
řečeno	říct	k5eAaPmNgNnS
i	i	k9
v	v	k7c6
díle	dílo	k1gNnSc6
Mein	Mein	k1gMnSc1
Kampf	Kampf	k1gMnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Nové	Nová	k1gFnSc2
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
si	se	k3xPyFc3
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
cíl	cíl	k1gInSc4
vytyčilo	vytyčit	k5eAaPmAgNnS
opětovné	opětovný	k2eAgNnSc1d1
vybudování	vybudování	k1gNnSc1
německého	německý	k2eAgInSc2d1
státu	stát	k1gInSc2
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
suverenitou	suverenita	k1gFnSc7
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
boj	boj	k1gInSc4
bezezbytku	bezezbytku	k6eAd1
zaměřit	zaměřit	k5eAaPmF
na	na	k7c4
získání	získání	k1gNnSc4
širokých	široký	k2eAgFnPc2d1
mas	masa	k1gFnPc2
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
NSDAP	NSDAP	kA
se	se	k3xPyFc4
pokusila	pokusit	k5eAaPmAgFnS
zachytit	zachytit	k5eAaPmF
co	co	k9
nejvíce	nejvíce	k6eAd1,k6eAd3
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střední	střední	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
a	a	k8xC
rolníky	rolník	k1gMnPc7
lákal	lákat	k5eAaImAgInS
slibovaný	slibovaný	k2eAgInSc1d1
boj	boj	k1gInSc1
s	s	k7c7
nezaměstnaností	nezaměstnanost	k1gFnSc7
(	(	kIx(
<g/>
začátkem	začátkem	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byly	být	k5eAaImAgFnP
v	v	k7c6
říši	říš	k1gFnSc6
3	#num#	k4
miliony	milion	k4xCgInPc1
lidí	člověk	k1gMnPc2
bez	bez	k7c2
práce	práce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
průmyslníky	průmyslník	k1gMnPc4
zase	zase	k9
zajímalo	zajímat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
NSDAP	NSDAP	kA
nechce	chtít	k5eNaImIp3nS
mít	mít	k5eAaImF
nic	nic	k3yNnSc1
společného	společný	k2eAgNnSc2d1
s	s	k7c7
komunisty	komunista	k1gMnPc7
a	a	k8xC
neslibuje	slibovat	k5eNaImIp3nS
podporu	podpora	k1gFnSc4
dělnictvu	dělnictvo	k1gNnSc3
<g/>
,	,	kIx,
i	i	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
do	do	k7c2
nacistických	nacistický	k2eAgFnPc2d1
pokladen	pokladna	k1gFnPc2
hrnuly	hrnout	k5eAaImAgInP
sponzorské	sponzorský	k2eAgInPc1d1
dary	dar	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demagogickou	demagogický	k2eAgFnSc7d1
agitací	agitace	k1gFnSc7
postupně	postupně	k6eAd1
získala	získat	k5eAaPmAgFnS
členy	člen	k1gMnPc4
takřka	takřka	k6eAd1
všech	všecek	k3xTgFnPc2
sociálních	sociální	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
projevy	projev	k1gInPc1
Hitlera	Hitler	k1gMnSc2
nebyly	být	k5eNaImAgInP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
přineslo	přinést	k5eAaPmAgNnS
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Imponovala	imponovat	k5eAaImAgFnS
i	i	k9
mladé	mladý	k2eAgFnSc6d1
generaci	generace	k1gFnSc6
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
odhodlanosti	odhodlanost	k1gFnSc3
a	a	k8xC
též	též	k9
svým	svůj	k3xOyFgMnSc7
charismatickým	charismatický	k2eAgMnSc7d1
vůdcem	vůdce	k1gMnSc7
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1930	#num#	k4
se	se	k3xPyFc4
durynským	durynský	k2eAgMnSc7d1
ministrem	ministr	k1gMnSc7
vnitra	vnitro	k1gNnSc2
stal	stát	k5eAaPmAgMnS
Wilhelm	Wilhelm	k1gMnSc1
Frick	Frick	k1gMnSc1
–	–	k?
první	první	k4xOgMnSc1
nacista	nacista	k1gMnSc1
ve	v	k7c6
státní	státní	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1930	#num#	k4
rezignoval	rezignovat	k5eAaBmAgMnS
kancléř	kancléř	k1gMnSc1
Brüning	Brüning	k1gInSc1
a	a	k8xC
vypsal	vypsat	k5eAaPmAgMnS
všeobecné	všeobecný	k2eAgFnPc4d1
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
úkor	úkor	k1gInSc4
většinou	většinou	k6eAd1
umírněných	umírněný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
dostali	dostat	k5eAaPmAgMnP
nacisté	nacista	k1gMnPc1
v	v	k7c6
říšském	říšský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
107	#num#	k4
(	(	kIx(
<g/>
17,5	17,5	k4
%	%	kIx~
<g/>
)	)	kIx)
poslaneckých	poslanecký	k2eAgInPc2d1
mandátů	mandát	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
6	#num#	k4
400	#num#	k4
000	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stali	stát	k5eAaPmAgMnP
se	se	k3xPyFc4
tak	tak	k8xS,k8xC
druhou	druhý	k4xOgFnSc7
nejsilnější	silný	k2eAgFnSc7d3
stranou	strana	k1gFnSc7
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vítali	vítat	k5eAaImAgMnP
i	i	k8xC
lidé	člověk	k1gMnPc1
z	z	k7c2
různých	různý	k2eAgFnPc2d1
končin	končina	k1gFnPc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
přáli	přát	k5eAaImAgMnP
jakkoli	jakkoli	k6eAd1
zabránit	zabránit	k5eAaPmF
bolševismu	bolševismus	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Hitler	Hitler	k1gMnSc1
nadále	nadále	k6eAd1
řečnil	řečnit	k5eAaImAgMnS
o	o	k7c6
silném	silný	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
a	a	k8xC
znovupřipojení	znovupřipojení	k1gNnSc6
ztracených	ztracený	k2eAgFnPc2d1
území	území	k1gNnSc6
kvůli	kvůli	k7c3
Versailleské	versailleský	k2eAgFnSc3d1
smlouvě	smlouva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
stahovaly	stahovat	k5eAaImAgFnP
spojenecké	spojenecký	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
z	z	k7c2
bývalých	bývalý	k2eAgFnPc2d1
částí	část	k1gFnPc2
říše	říš	k1gFnSc2
–	–	k?
ze	z	k7c2
Sárska	Sársko	k1gNnSc2
a	a	k8xC
z	z	k7c2
Porýní	Porýní	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1931	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyhlásila	vyhlásit	k5eAaPmAgFnS
Danatbank	Danatbank	k1gInSc4
bankrot	bankrot	k1gInSc1
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
odstartoval	odstartovat	k5eAaPmAgInS
i	i	k9
postupný	postupný	k2eAgInSc1d1
finanční	finanční	k2eAgInSc1d1
krach	krach	k1gInSc1
všech	všecek	k3xTgFnPc2
bank	banka	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
ocitlo	ocitnout	k5eAaPmAgNnS
téměř	téměř	k6eAd1
6	#num#	k4
milionů	milion	k4xCgInPc2
Němců	Němec	k1gMnPc2
bez	bez	k7c2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nahrávalo	nahrávat	k5eAaImAgNnS
komunistům	komunista	k1gMnPc3
a	a	k8xC
nacionálnímu	nacionální	k2eAgInSc3d1
socialismu	socialismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
se	se	k3xPyFc4
spolčil	spolčit	k5eAaPmAgMnS
s	s	k7c7
tiskovým	tiskový	k2eAgMnSc7d1
magnátem	magnát	k1gMnSc7
Alfredem	Alfred	k1gMnSc7
Hugenbergem	Hugenberg	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitlerovy	Hitlerův	k2eAgInPc1d1
proslovy	proslov	k1gInPc1
tak	tak	k6eAd1
získaly	získat	k5eAaPmAgInP
velkou	velký	k2eAgFnSc4d1
publicitu	publicita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emil	Emil	k1gMnSc1
Kirdorf	Kirdorf	k1gMnSc1
a	a	k8xC
Fritz	Fritz	k1gMnSc1
Thyssen	Thyssna	k1gFnPc2
<g/>
,	,	kIx,
porúrští	porúrský	k2eAgMnPc1d1
průmyslníci	průmyslník	k1gMnPc1
<g/>
,	,	kIx,
taktéž	taktéž	k?
podporovali	podporovat	k5eAaImAgMnP
NSDAP	NSDAP	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
cca	cca	kA
800	#num#	k4
000	#num#	k4
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prezidentských	prezidentský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1932	#num#	k4
se	se	k3xPyFc4
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
umístil	umístit	k5eAaPmAgMnS
až	až	k9
druhý	druhý	k4xOgMnSc1
s	s	k7c7
11	#num#	k4
miliony	milion	k4xCgInPc7
hlasy	hlas	k1gInPc7
<g/>
,	,	kIx,
hned	hned	k6eAd1
za	za	k7c7
maršálem	maršál	k1gMnSc7
Paulem	Paul	k1gMnSc7
von	von	k1gInSc4
Hindenburg	Hindenburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
obdrželi	obdržet	k5eAaPmAgMnP
nacionální	nacionální	k2eAgMnPc1d1
socialisté	socialist	k1gMnPc1
230	#num#	k4
mandátů	mandát	k1gInPc2
<g/>
,	,	kIx,
sociální	sociální	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
133	#num#	k4
<g/>
,	,	kIx,
střed	střed	k1gInSc1
97	#num#	k4
a	a	k8xC
komunisté	komunista	k1gMnPc1
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
byli	být	k5eAaImAgMnP
nacisté	nacista	k1gMnPc1
nejsilnější	silný	k2eAgFnSc4d3
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
neměli	mít	k5eNaImAgMnP
většinu	většina	k1gFnSc4
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
vlády	vláda	k1gFnSc2
a	a	k8xC
odmítali	odmítat	k5eAaImAgMnP
spolu	spolu	k6eAd1
s	s	k7c7
komunisty	komunista	k1gMnPc7
vytvoření	vytvoření	k1gNnSc2
vlády	vláda	k1gFnSc2
koaliční	koaliční	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitler	Hitler	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
i	i	k9
místo	místo	k7c2
vicekancléře	vicekancléř	k1gMnSc2
pod	pod	k7c7
Papenem	Papen	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyslovení	vyslovení	k1gNnSc6
nedůvěry	nedůvěra	k1gFnPc4
Papenově	Papenův	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
a	a	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
odstoupení	odstoupení	k1gNnSc6
se	se	k3xPyFc4
situace	situace	k1gFnSc1
nezlepšila	zlepšit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunisté	komunista	k1gMnPc1
získali	získat	k5eAaPmAgMnP
několik	několik	k4yIc4
hlasů	hlas	k1gInPc2
na	na	k7c4
úkor	úkor	k1gInSc4
socialistů	socialist	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
setrvalo	setrvat	k5eAaPmAgNnS
se	se	k3xPyFc4
na	na	k7c6
mrtvém	mrtvý	k2eAgInSc6d1
bodě	bod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
von	von	k1gInSc4
Hindenburg	Hindenburg	k1gMnSc1
podal	podat	k5eAaPmAgMnS
Hitlerovi	Hitler	k1gMnSc3
nabídku	nabídka	k1gFnSc4
na	na	k7c4
kancléřství	kancléřství	k1gNnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
Hitler	Hitler	k1gMnSc1
nepřijal	přijmout	k5eNaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Buď	buď	k8xC
všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
nic	nic	k3yNnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1933	#num#	k4
ho	on	k3xPp3gInSc4
tak	tak	k9
říšský	říšský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
říšským	říšský	k2eAgMnSc7d1
kancléřem	kancléř	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
tvrdé	tvrdý	k2eAgFnSc2d1
<g/>
,	,	kIx,
neúprosné	úprosný	k2eNgFnSc2d1
politické	politický	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
zachvátil	zachvátit	k5eAaPmAgInS
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1933	#num#	k4
budovu	budova	k1gFnSc4
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
požár	požár	k1gInSc1
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgNnSc2
Hitler	Hitler	k1gMnSc1
obvinil	obvinit	k5eAaPmAgMnS
komunistická	komunistický	k2eAgNnPc4d1
hnutí	hnutí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Von	von	k1gInSc1
Hindenburg	Hindenburg	k1gMnSc1
to	ten	k3xDgNnSc4
použil	použít	k5eAaPmAgMnS
jako	jako	k9
důvod	důvod	k1gInSc4
pro	pro	k7c4
vyhlášení	vyhlášení	k1gNnSc4
dekretu	dekret	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
omezil	omezit	k5eAaPmAgInS
ústavní	ústavní	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
slova	slovo	k1gNnSc2
<g/>
,	,	kIx,
tisku	tisk	k1gInSc2
atd.	atd.	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožnilo	umožnit	k5eAaPmAgNnS
v	v	k7c6
podstatě	podstata	k1gFnSc6
neomezené	omezený	k2eNgNnSc1d1
řádění	řádění	k1gNnSc1
nacistických	nacistický	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnových	březnový	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
získala	získat	k5eAaPmAgFnS
NSDAP	NSDAP	kA
44	#num#	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
v	v	k7c6
koalici	koalice	k1gFnSc6
s	s	k7c7
pravicovou	pravicový	k2eAgFnSc7d1
německou	německý	k2eAgFnSc7d1
nacionální	nacionální	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
to	ten	k3xDgNnSc1
činilo	činit	k5eAaImAgNnS
53	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
to	ten	k3xDgNnSc1
nebyla	být	k5eNaImAgFnS
převážná	převážný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
dezorganizovanosti	dezorganizovanost	k1gFnSc3
opozičních	opoziční	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
uchopila	uchopit	k5eAaPmAgFnS
NSDAP	NSDAP	kA
moc	moc	k6eAd1
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitler	Hitler	k1gMnSc1
tak	tak	k9
směl	smět	k5eAaImAgMnS
po	po	k7c4
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
vládnout	vládnout	k5eAaImF
pomocí	pomocí	k7c2
dekretů	dekret	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgFnPc7
diktátorskými	diktátorský	k2eAgFnPc7d1
pravomocemi	pravomoc	k1gFnPc7
upevnil	upevnit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znamenalo	znamenat	k5eAaImAgNnS
to	ten	k3xDgNnSc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
konec	konec	k1gInSc4
Výmarské	výmarský	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgInS
proti	proti	k7c3
nezávislým	závislý	k2eNgInPc3d1
odborům	odbor	k1gInPc3
<g/>
,	,	kIx,
Židům	Žid	k1gMnPc3
a	a	k8xC
jiným	jiný	k2eAgFnPc3d1
politickým	politický	k2eAgFnPc3d1
stranám	strana	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židé	Žid	k1gMnPc1
byli	být	k5eAaImAgMnP
hromadně	hromadně	k6eAd1
propouštěni	propouštět	k5eAaImNgMnP
ze	z	k7c2
státních	státní	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
jim	on	k3xPp3gMnPc3
bojkotovány	bojkotovat	k5eAaImNgInP
obchody	obchod	k1gInPc1
<g/>
,	,	kIx,
podniky	podnik	k1gInPc1
a	a	k8xC
i	i	k9
lékařské	lékařský	k2eAgFnSc2d1
či	či	k8xC
advokátské	advokátský	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
odborů	odbor	k1gInPc2
bylo	být	k5eAaImAgNnS
pozatýkáno	pozatýkán	k2eAgNnSc1d1
a	a	k8xC
místo	místo	k7c2
něj	on	k3xPp3gMnSc4
byli	být	k5eAaImAgMnP
dosazeni	dosazen	k2eAgMnPc1d1
nacisté	nacista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitler	Hitler	k1gMnSc1
si	se	k3xPyFc3
mistrnými	mistrný	k2eAgInPc7d1
tahy	tah	k1gInPc7
získal	získat	k5eAaPmAgMnS
i	i	k9
přízeň	přízeň	k1gFnSc4
dělníků	dělník	k1gMnPc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
zakládal	zakládat	k5eAaImAgInS
pracovní	pracovní	k2eAgInPc4d1
tábory	tábor	k1gInPc4
pro	pro	k7c4
mladé	mladý	k2eAgFnPc4d1
a	a	k8xC
nové	nový	k2eAgFnPc4d1
práce	práce	k1gFnPc4
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
i	i	k9
díky	díky	k7c3
snaze	snaha	k1gFnSc3
o	o	k7c6
znovuvyzbrojení	znovuvyzbrojení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
se	se	k3xPyFc4
maximálně	maximálně	k6eAd1
vzmohl	vzmoct	k5eAaPmAgInS
na	na	k7c4
plané	planý	k2eAgFnPc4d1
výhrůžky	výhrůžka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
patronací	patronace	k1gFnSc7
fanatického	fanatický	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
propagandy	propaganda	k1gFnSc2
Josepha	Joseph	k1gMnSc2
Goebbelse	Goebbels	k1gMnSc2
se	se	k3xPyFc4
veřejně	veřejně	k6eAd1
pálily	pálit	k5eAaImAgFnP
knihy	kniha	k1gFnPc4
mnohých	mnohý	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
–	–	k?
Einsteinem	Einstein	k1gMnSc7
počínaje	počínaje	k7c7
a	a	k8xC
konče	konče	k7c7
Freudem	Freud	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
mlčel	mlčet	k5eAaImAgInS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
NSDAP	NSDAP	kA
14	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1933	#num#	k4
ustanovila	ustanovit	k5eAaPmAgFnS
jedinou	jediný	k2eAgFnSc7d1
legální	legální	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
v	v	k7c6
Německu	Německo	k1gNnSc6
–	–	k?
ostatní	ostatní	k2eAgFnPc1d1
strany	strana	k1gFnPc1
byly	být	k5eAaImAgFnP
zakázány	zakázat	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
byl	být	k5eAaImAgInS
podpořen	podpořit	k5eAaPmNgInS
92	#num#	k4
%	%	kIx~
voličů	volič	k1gMnPc2
–	–	k?
zbytek	zbytek	k1gInSc1
<g/>
,	,	kIx,
odpůrci	odpůrce	k1gMnPc1
nacismu	nacismus	k1gInSc2
<g/>
,	,	kIx,
odevzdali	odevzdat	k5eAaPmAgMnP
prázdný	prázdný	k2eAgInSc4d1
volební	volební	k2eAgInSc4d1
lístek	lístek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říši	říš	k1gFnSc6
bylo	být	k5eAaImAgNnS
schváleno	schválit	k5eAaPmNgNnS
i	i	k9
vystoupení	vystoupení	k1gNnSc1
Německa	Německo	k1gNnSc2
ze	z	k7c2
Společnosti	společnost	k1gFnSc2
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nacistický	nacistický	k2eAgInSc4d1
rozhlasový	rozhlasový	k2eAgInSc4d1
vůz	vůz	k1gInSc4
během	během	k7c2
volební	volební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
k	k	k7c3
prezidentským	prezidentský	k2eAgFnPc3d1
volbám	volba	k1gFnPc3
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
v	v	k7c6
Berlínském	berlínský	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
Pankow	Pankow	k1gFnSc2
</s>
<s>
Hitler	Hitler	k1gMnSc1
začal	začít	k5eAaPmAgMnS
zakládat	zakládat	k5eAaImF
lidové	lidový	k2eAgInPc4d1
soudy	soud	k1gInPc4
a	a	k8xC
zrušil	zrušit	k5eAaPmAgInS
i	i	k9
běžnou	běžný	k2eAgFnSc4d1
právní	právní	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
často	často	k6eAd1
vykonstruovaných	vykonstruovaný	k2eAgInPc2d1
procesů	proces	k1gInPc2
byli	být	k5eAaImAgMnP
odsouzení	odsouzený	k1gMnPc1
posíláni	posílat	k5eAaImNgMnP
do	do	k7c2
koncentračních	koncentrační	k2eAgMnPc2d1
táborů	tábor	k1gMnPc2
zřízených	zřízený	k2eAgMnPc2d1
taktéž	taktéž	k?
již	již	k6eAd1
roku	rok	k1gInSc2
1933	#num#	k4
(	(	kIx(
<g/>
například	například	k6eAd1
v	v	k7c6
Dachau	Dachaus	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
si	se	k3xPyFc3
takříkajíc	takříkajíc	k6eAd1
sám	sám	k3xTgMnSc1
zametl	zamést	k5eAaPmAgInS
před	před	k7c7
prahem	práh	k1gInSc7
díky	díky	k7c3
čistkám	čistka	k1gFnPc3
trvajícím	trvající	k2eAgFnPc3d1
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
masakr	masakr	k1gInSc4
vedení	vedení	k1gNnSc2
SA	SA	kA
<g/>
,	,	kIx,
Hitlerových	Hitlerových	k2eAgMnPc2d1
odpůrců	odpůrce	k1gMnPc2
a	a	k8xC
sociálních	sociální	k2eAgMnPc2d1
živlů	živel	k1gInPc2
NSDAP	NSDAP	kA
se	se	k3xPyFc4
vžil	vžít	k5eAaPmAgInS
název	název	k1gInSc1
Noc	noc	k1gFnSc4
dlouhých	dlouhý	k2eAgInPc2d1
nožů	nůž	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitler	Hitler	k1gMnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
zbavil	zbavit	k5eAaPmAgInS
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
oslabit	oslabit	k5eAaPmF
jeho	jeho	k3xOp3gFnSc4
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
SA	SA	kA
<g/>
,	,	kIx,
mající	mající	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
3	#num#	k4
miliony	milion	k4xCgInPc1
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
mohla	moct	k5eAaImAgFnS
směle	směle	k6eAd1
konkurovat	konkurovat	k5eAaImF
armádě	armáda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
cca	cca	kA
1	#num#	k4
000	#num#	k4
zavražděnými	zavražděný	k2eAgFnPc7d1
osobami	osoba	k1gFnPc7
nechyběl	chybět	k5eNaImAgMnS
ani	ani	k9
velitel	velitel	k1gMnSc1
SA	SA	kA
Ernst	Ernst	k1gMnSc1
Röhm	Röhm	k1gMnSc1
<g/>
,	,	kIx,
spoluzakladatel	spoluzakladatel	k1gMnSc1
strany	strana	k1gFnSc2
Gregor	Gregor	k1gMnSc1
Strasser	Strasser	k1gMnSc1
či	či	k8xC
exkancléř	exkancléř	k1gMnSc1
Schleicher	Schleichra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akci	akce	k1gFnSc4
provedly	provést	k5eAaPmAgFnP
polovojenské	polovojenský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
SS	SS	kA
(	(	kIx(
<g/>
Schutzstaffeln	Schutzstaffeln	k1gMnSc1
der	drát	k5eAaImRp2nS
NSDAP	NSDAP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
roku	rok	k1gInSc3
1925	#num#	k4
k	k	k7c3
ochraně	ochrana	k1gFnSc3
funkcionářů	funkcionář	k1gMnPc2
NSDAP	NSDAP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
SS	SS	kA
vedené	vedený	k2eAgFnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
Heinrichem	Heinrich	k1gMnSc7
Himmlerem	Himmler	k1gMnSc7
se	se	k3xPyFc4
posléze	posléze	k6eAd1
prosadily	prosadit	k5eAaPmAgFnP
jako	jako	k9
samostatná	samostatný	k2eAgFnSc1d1
složka	složka	k1gFnSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
obávaný	obávaný	k2eAgInSc1d1
bezpečnostní	bezpečnostní	k2eAgInSc1d1
aparát	aparát	k1gInSc1
v	v	k7c6
Říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nacistický	nacistický	k2eAgInSc1d1
režim	režim	k1gInSc1
<g/>
,	,	kIx,
už	už	k6eAd1
dost	dost	k6eAd1
silný	silný	k2eAgInSc1d1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
získal	získat	k5eAaPmAgMnS
i	i	k9
úřad	úřad	k1gInSc4
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
se	se	k3xPyFc4
po	po	k7c6
smrti	smrt	k1gFnSc6
87	#num#	k4
<g/>
letého	letý	k2eAgInSc2d1
Paula	Paula	k1gFnSc1
von	von	k1gInSc1
Hindenburg	Hindenburg	k1gInSc1
stal	stát	k5eAaPmAgMnS
Hitler	Hitler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
spojil	spojit	k5eAaPmAgMnS
prezidentskou	prezidentský	k2eAgFnSc4d1
a	a	k8xC
kancléřskou	kancléřský	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
–	–	k?
Hitler	Hitler	k1gMnSc1
i	i	k9
tak	tak	k6eAd1
stejně	stejně	k6eAd1
upřednostňoval	upřednostňovat	k5eAaImAgMnS
oslovení	oslovení	k1gNnSc4
„	„	k?
<g/>
der	drát	k5eAaImRp2nS
Führer	Führer	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
vůdce	vůdce	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Plakát	plakát	k1gInSc1
NSDAP	NSDAP	kA
-	-	kIx~
Reichstagswahl	Reichstagswahl	k1gMnSc1
</s>
<s>
Leták	leták	k1gInSc1
-	-	kIx~
pálení	pálení	k1gNnSc1
knih	kniha	k1gFnPc2
</s>
<s>
Volební	volební	k2eAgFnSc1d1
propaganda	propaganda	k1gFnSc1
</s>
<s>
Reichsautobahn	Reichsautobahn	k1gMnSc1
-	-	kIx~
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
</s>
<s>
Nürnberg	Nürnberg	k1gMnSc1
-	-	kIx~
Reichsparteitag	Reichsparteitag	k1gMnSc1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
štábem	štáb	k1gInSc7
</s>
<s>
Stranický	stranický	k2eAgInSc1d1
průkaz	průkaz	k1gInSc1
NSDAP	NSDAP	kA
</s>
<s>
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
se	se	k3xPyFc4
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
představovaná	představovaný	k2eAgFnSc1d1
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
Hitlerem	Hitler	k1gMnSc7
<g/>
,	,	kIx,
chovala	chovat	k5eAaImAgFnS
taktéž	taktéž	k?
nevybíravě	vybíravě	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokusila	pokusit	k5eAaPmAgFnS
se	se	k3xPyFc4
o	o	k7c4
vpád	vpád	k1gInSc4
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
zabránil	zabránit	k5eAaPmAgInS
promyšlený	promyšlený	k2eAgInSc4d1
Mussoliniho	Mussolini	k1gMnSc2
tah	tah	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
oproti	oproti	k7c3
Versailleské	versailleský	k2eAgFnSc3d1
mírové	mírový	k2eAgFnSc3d1
smlouvě	smlouva	k1gFnSc3
neustále	neustále	k6eAd1
zvyšovala	zvyšovat	k5eAaImAgFnS
stavy	stav	k1gInPc4
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavedena	zaveden	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
všeobecná	všeobecný	k2eAgFnSc1d1
branná	branný	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
a	a	k8xC
na	na	k7c4
veřejnost	veřejnost	k1gFnSc4
prosakovaly	prosakovat	k5eAaImAgFnP
zprávy	zpráva	k1gFnPc1
i	i	k9
o	o	k7c6
založení	založení	k1gNnSc6
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
argument	argument	k1gInSc1
pro	pro	k7c4
znovuvyzbrojení	znovuvyzbrojení	k1gNnSc4
předložil	předložit	k5eAaPmAgMnS
Hitler	Hitler	k1gMnSc1
taktéž	taktéž	k?
nepovolený	povolený	k2eNgInSc1d1
růst	růst	k1gInSc1
armády	armáda	k1gFnSc2
SSSR	SSSR	kA
a	a	k8xC
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastrašovací	zastrašovací	k2eAgFnPc4d1
diplomacie	diplomacie	k1gFnPc4
se	se	k3xPyFc4
NSDAP	NSDAP	kA
dařila	dařit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
Německo	Německo	k1gNnSc1
obsadilo	obsadit	k5eAaPmAgNnS
Porýní	Porýní	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc4d1
roky	rok	k1gInPc4
postupovalo	postupovat	k5eAaImAgNnS
podle	podle	k7c2
plánu	plán	k1gInSc2
<g/>
,	,	kIx,
vytyčeného	vytyčený	k2eAgInSc2d1
generálním	generální	k2eAgInSc7d1
štábem	štáb	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c4
získání	získání	k1gNnSc4
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oblíbenost	oblíbenost	k1gFnSc1
nacistů	nacista	k1gMnPc2
v	v	k7c6
Třetí	třetí	k4xOgFnSc6
říši	říš	k1gFnSc6
stoupala	stoupat	k5eAaImAgFnS
díky	díky	k7c3
národnímu	národní	k2eAgInSc3d1
hospodářskému	hospodářský	k2eAgInSc3d1
růstu	růst	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbrojení	zbrojení	k1gNnSc1
dalo	dát	k5eAaPmAgNnS
práci	práce	k1gFnSc4
nezaměstnaným	nezaměstnaný	k1gMnPc3
<g/>
,	,	kIx,
stavěly	stavět	k5eAaImAgInP
se	se	k3xPyFc4
budovy	budova	k1gFnPc4
<g/>
,	,	kIx,
byty	byt	k1gInPc4
a	a	k8xC
dálnice	dálnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
pořádání	pořádání	k1gNnSc4
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Berlíně	Berlín	k1gInSc6
1936	#num#	k4
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgMnS
Hitler	Hitler	k1gMnSc1
jako	jako	k8xS,k8xC
propagandistický	propagandistický	k2eAgInSc1d1
tah	tah	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbenost	oblíbenost	k1gFnSc1
„	„	k?
<g/>
vůdce	vůdce	k1gMnSc1
<g/>
“	“	k?
se	se	k3xPyFc4
zvyšovala	zvyšovat	k5eAaImAgFnS
a	a	k8xC
občané	občan	k1gMnPc1
byli	být	k5eAaImAgMnP
hrdi	hrd	k2eAgMnPc1d1
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc4d1
smutnou	smutný	k2eAgFnSc4d1
kapitolu	kapitola	k1gFnSc4
pro	pro	k7c4
židovskou	židovský	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
zahájilo	zahájit	k5eAaPmAgNnS
přijetí	přijetí	k1gNnSc4
norimberských	norimberský	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
15	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
tvrdé	tvrdý	k2eAgInPc1d1
rasové	rasový	k2eAgInPc1d1
zákony	zákon	k1gInPc1
měly	mít	k5eAaImAgInP
ospravedlnit	ospravedlnit	k5eAaPmF
kruté	krutý	k2eAgNnSc4d1
zacházení	zacházení	k1gNnSc4
s	s	k7c7
Židy	Žid	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
zbaveni	zbaven	k2eAgMnPc1d1
občanských	občanský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
omezeno	omezen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
i	i	k9
jejich	jejich	k3xOp3gNnSc1
vlastnictví	vlastnictví	k1gNnSc1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nešlo	jít	k5eNaImAgNnS
o	o	k7c4
poslední	poslední	k2eAgInPc4d1
antisemitské	antisemitský	k2eAgInPc4d1
zákony	zákon	k1gInPc4
schválené	schválený	k2eAgFnSc2d1
NSDAP	NSDAP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
platnost	platnost	k1gFnSc4
se	se	k3xPyFc4
posléze	posléze	k6eAd1
rozšiřovala	rozšiřovat	k5eAaImAgFnS
i	i	k9
na	na	k7c4
dobyté	dobytý	k2eAgFnPc4d1
země	zem	k1gFnPc4
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
etapou	etapa	k1gFnSc7
holokaustu	holokaust	k1gInSc2
–	–	k?
vyvrcholením	vyvrcholení	k1gNnSc7
represí	represe	k1gFnPc2
proti	proti	k7c3
židovskému	židovský	k2eAgInSc3d1
národu	národ	k1gInSc3
–	–	k?
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
pogrom	pogrom	k1gInSc1
v	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
10	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1938	#num#	k4
(	(	kIx(
<g/>
tzv.	tzv.	kA
křišťálová	křišťálový	k2eAgFnSc1d1
noc	noc	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SA	SA	kA
a	a	k8xC
NSDAP	NSDAP	kA
se	se	k3xPyFc4
mstila	mstít	k5eAaImAgFnS
pod	pod	k7c7
záminkou	záminka	k1gFnSc7
vraždy	vražda	k1gFnSc2
německého	německý	k2eAgMnSc2d1
úředníka	úředník	k1gMnSc2
na	na	k7c6
velvyslanectví	velvyslanectví	k1gNnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
židovským	židovský	k2eAgInSc7d1
mladíkem	mladík	k1gInSc7
<g/>
.	.	kIx.
91	#num#	k4
Židů	Žid	k1gMnPc2
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
život	život	k1gInSc4
<g/>
,	,	kIx,
7	#num#	k4
000	#num#	k4
židovských	židovský	k2eAgInPc2d1
obchodů	obchod	k1gInPc2
bylo	být	k5eAaImAgNnS
spolu	spolu	k6eAd1
s	s	k7c7
židovskými	židovský	k2eAgFnPc7d1
školami	škola	k1gFnPc7
a	a	k8xC
synagogami	synagoga	k1gFnPc7
vypleněno	vypleněn	k2eAgNnSc1d1
a	a	k8xC
zapáleno	zapálen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejenže	nejenže	k6eAd1
v	v	k7c6
následných	následný	k2eAgInPc6d1
procesech	proces	k1gInPc6
bylo	být	k5eAaImAgNnS
posláno	poslán	k2eAgNnSc4d1
30	#num#	k4
000	#num#	k4
Židů	Žid	k1gMnPc2
do	do	k7c2
koncentračních	koncentrační	k2eAgInPc2d1
táborů	tábor	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
židovská	židovský	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
musela	muset	k5eAaImAgFnS
ještě	ještě	k6eAd1
zaplatit	zaplatit	k5eAaPmF
odškodné	odškodné	k1gNnSc4
1	#num#	k4
miliardu	miliarda	k4xCgFnSc4
marek	marka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
další	další	k2eAgFnSc2d1
postupné	postupný	k2eAgFnSc2d1
arizace	arizace	k1gFnSc2
se	se	k3xPyFc4
nehledělo	hledět	k5eNaImAgNnS
na	na	k7c4
hanebnost	hanebnost	k1gFnSc4
diskriminačních	diskriminační	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
horšila	horšit	k5eAaImAgFnS
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
rasy	ras	k1gMnPc4
neárijského	árijský	k2eNgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ozbrojené	ozbrojený	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
podléhající	podléhající	k2eAgNnSc1d1
velení	velení	k1gNnSc1
NSDAP	NSDAP	kA
terorizovaly	terorizovat	k5eAaImAgInP
národy	národ	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
uchvacoval	uchvacovat	k5eAaImAgMnS
svými	svůj	k3xOyFgFnPc7
fanatickými	fanatický	k2eAgFnPc7d1
idejemi	idea	k1gFnPc7
miliony	milion	k4xCgInPc4
následovníků	následovník	k1gMnPc2
a	a	k8xC
své	svůj	k3xOyFgMnPc4
odpůrce	odpůrce	k1gMnPc4
tvrdě	tvrdě	k6eAd1
potíral	potírat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
stoupenci	stoupenec	k1gMnPc7
po	po	k7c6
boku	bok	k1gInSc6
–	–	k?
ministrem	ministr	k1gMnSc7
letectví	letectví	k1gNnSc2
Hermannem	Hermann	k1gMnSc7
Göringem	Göring	k1gInSc7
<g/>
,	,	kIx,
ministrem	ministr	k1gMnSc7
propagandy	propaganda	k1gFnSc2
Josephem	Joseph	k1gInSc7
Goebbelsem	Goebbels	k1gInSc7
a	a	k8xC
Heinrichem	Heinrich	k1gMnSc7
Himmlerem	Himmler	k1gMnSc7
<g/>
,	,	kIx,
vůdcem	vůdce	k1gMnSc7
SS	SS	kA
a	a	k8xC
velitelem	velitel	k1gMnSc7
gestapa	gestapo	k1gNnSc2
–	–	k?
se	se	k3xPyFc4
odvážil	odvážit	k5eAaPmAgMnS
vkročit	vkročit	k5eAaPmF
do	do	k7c2
dějin	dějiny	k1gFnPc2
bezohledným	bezohledný	k2eAgInSc7d1
rozpoutáním	rozpoutání	k1gNnSc7
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
NSDAP	NSDAP	kA
ve	v	k7c6
společnosti	společnost	k1gFnSc6
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
moci	moc	k1gFnSc2
v	v	k7c6
NSDAP	NSDAP	kA
ujal	ujmout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
s	s	k7c7
konečnou	konečný	k2eAgFnSc7d1
platností	platnost	k1gFnSc7
Hitler	Hitler	k1gMnSc1
<g/>
,	,	kIx,
získávala	získávat	k5eAaImAgFnS
strana	strana	k1gFnSc1
pozvolna	pozvolna	k6eAd1
stále	stále	k6eAd1
více	hodně	k6eAd2
příznivců	příznivec	k1gMnPc2
ve	v	k7c6
všech	všecek	k3xTgFnPc6
společenských	společenský	k2eAgFnPc6d1
třídách	třída	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Predominance	Predominance	k1gFnSc1
NSDAP	NSDAP	kA
byla	být	k5eAaImAgFnS
mezi	mezi	k7c7
nižšími	nízký	k2eAgFnPc7d2
středními	střední	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
<g/>
,	,	kIx,
protestanty	protestant	k1gMnPc7
<g/>
,	,	kIx,
vesnickými	vesnický	k2eAgMnPc7d1
a	a	k8xC
maloměstskými	maloměstský	k2eAgMnPc7d1
voliči	volič	k1gMnPc7
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
severním	severní	k2eAgNnSc6d1
a	a	k8xC
východním	východní	k2eAgNnSc6d1
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
schopna	schopen	k2eAgFnSc1d1
získat	získat	k5eAaPmF
podporu	podpora	k1gFnSc4
mezi	mezi	k7c7
vzdělanými	vzdělaný	k2eAgFnPc7d1
<g/>
,	,	kIx,
horními	horní	k2eAgFnPc7d1
středními	střední	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
a	a	k8xC
profesionálními	profesionální	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
více	hodně	k6eAd2
limitovaný	limitovaný	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
podpory	podpora	k1gFnSc2
mezi	mezi	k7c7
některými	některý	k3yIgFnPc7
sekcemi	sekce	k1gFnPc7
méně	málo	k6eAd2
organizovaného	organizovaný	k2eAgNnSc2d1
dělnictva	dělnictvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podpora	podpora	k1gFnSc1
průmyslníků	průmyslník	k1gMnPc2
</s>
<s>
Po	po	k7c6
svém	svůj	k3xOyFgNnSc6
znovuzvolení	znovuzvolení	k1gNnSc6
do	do	k7c2
čela	čelo	k1gNnSc2
strany	strana	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
<g/>
,	,	kIx,
Hitler	Hitler	k1gMnSc1
zavrhl	zavrhnout	k5eAaPmAgMnS
původní	původní	k2eAgInPc4d1
socialistické	socialistický	k2eAgInPc4d1
ideály	ideál	k1gInPc4
své	svůj	k3xOyFgFnSc2
strany	strana	k1gFnSc2
a	a	k8xC
získal	získat	k5eAaPmAgMnS
si	se	k3xPyFc3
tím	ten	k3xDgNnSc7
přízeň	přízeň	k1gFnSc4
velkých	velký	k2eAgMnPc2d1
průmyslníků	průmyslník	k1gMnPc2
a	a	k8xC
středostavovských	středostavovský	k2eAgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Začal	začít	k5eAaPmAgMnS
být	být	k5eAaImF
stále	stále	k6eAd1
častěji	často	k6eAd2
zván	zvát	k5eAaImNgInS
do	do	k7c2
prominentních	prominentní	k2eAgInPc2d1
salonů	salon	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
důležitých	důležitý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
z	z	k7c2
řad	řada	k1gFnPc2
armády	armáda	k1gFnSc2
a	a	k8xC
z	z	k7c2
podnikatelských	podnikatelský	k2eAgInPc2d1
kruhů	kruh	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
mu	on	k3xPp3gNnSc3
později	pozdě	k6eAd2
pomohli	pomoct	k5eAaPmAgMnP
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
moci	moc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacisté	nacista	k1gMnPc1
potřebovali	potřebovat	k5eAaImAgMnP
stále	stále	k6eAd1
větší	veliký	k2eAgFnPc4d2
sumy	suma	k1gFnPc4
peněz	peníze	k1gInPc2
na	na	k7c4
volební	volební	k2eAgFnPc4d1
kampaně	kampaň	k1gFnPc4
a	a	k8xC
na	na	k7c4
financování	financování	k1gNnSc4
provozu	provoz	k1gInSc2
stranického	stranický	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
<g/>
,	,	kIx,
SA	SA	kA
a	a	k8xC
SS	SS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Straně	strana	k1gFnSc6
tak	tak	k8xS,k8xC
finančně	finančně	k6eAd1
pomáhali	pomáhat	k5eAaImAgMnP
němečtí	německý	k2eAgMnPc1d1
průmyslníci	průmyslník	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
uhlobaron	uhlobaron	k1gMnSc1
Emil	Emil	k1gMnSc1
Kirdorf	Kirdorf	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
ovládal	ovládat	k5eAaImAgMnS
černé	černý	k2eAgInPc4d1
politické	politický	k2eAgInPc4d1
fondy	fond	k1gInPc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
porúrskou	porúrský	k2eAgFnSc4d1
pokladnu	pokladna	k1gFnSc4
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
přispívali	přispívat	k5eAaImAgMnP
majitelé	majitel	k1gMnPc1
dolů	dolů	k6eAd1
v	v	k7c6
Porúří	Porúří	k1gNnSc6
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
spoléhali	spoléhat	k5eAaImAgMnP
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nacisté	nacista	k1gMnPc1
tvrdě	tvrdě	k6eAd1
vypořádají	vypořádat	k5eAaPmIp3nP
s	s	k7c7
odboráři	odborář	k1gMnPc7
a	a	k8xC
komunisty	komunista	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finanční	finanční	k2eAgFnPc4d1
pomoci	pomoc	k1gFnPc4
se	se	k3xPyFc4
strana	strana	k1gFnSc1
dočkala	dočkat	k5eAaPmAgFnS
také	také	k9
od	od	k7c2
ocelářských	ocelářský	k2eAgMnPc2d1
magnátů	magnát	k1gMnPc2
Fritze	Fritze	k1gFnSc1
Thyssena	Thyssen	k2eAgFnSc1d1
a	a	k8xC
Alberta	Alberta	k1gFnSc1
Voeglera	Voegler	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tichá	Tichá	k1gFnSc1
podpora	podpora	k1gFnSc1
plynula	plynout	k5eAaImAgFnS
i	i	k9
od	od	k7c2
dalších	další	k2eAgMnPc2d1
průmyslníků	průmyslník	k1gMnPc2
jako	jako	k8xS,k8xC
byl	být	k5eAaImAgMnS
např.	např.	kA
Georg	Georg	k1gMnSc1
von	von	k1gInSc4
Schnitzler	Schnitzler	k1gMnSc1
z	z	k7c2
chemického	chemický	k2eAgInSc2d1
podniku	podnik	k1gInSc2
I.	I.	kA
G.	G.	kA
Farben	Farbna	k1gFnPc2
<g/>
,	,	kIx,
Wilhelm	Wilhelm	k1gMnSc1
Cuno	Cuno	k1gMnSc1
z	z	k7c2
Hamburg-Amerika-Line	Hamburg-Amerika-Lin	k1gInSc5
<g/>
,	,	kIx,
kolínský	kolínský	k2eAgMnSc1d1
průmyslník	průmyslník	k1gMnSc1
Otto	Otto	k1gMnSc1
Wolf	Wolf	k1gMnSc1
nebo	nebo	k8xC
kolínský	kolínský	k2eAgMnSc1d1
bankéř	bankéř	k1gMnSc1
Kurt	Kurt	k1gMnSc1
von	von	k1gInSc4
Schroeder	Schroedra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispěla	přispět	k5eAaPmAgFnS
také	také	k9
řada	řada	k1gFnSc1
bank	banka	k1gFnPc2
a	a	k8xC
finančních	finanční	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
–	–	k?
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
Deutsche	Deutsche	k1gNnSc4
Bank	bank	k1gInSc1
<g/>
,	,	kIx,
Dresdner	Dresdner	k1gInSc1
Bank	bank	k1gInSc1
nebo	nebo	k8xC
pojišťovna	pojišťovna	k1gFnSc1
Allianz	Allianza	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ačkoliv	ačkoliv	k8xS
byla	být	k5eAaImAgFnS
finanční	finanční	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
podnikatelských	podnikatelský	k2eAgInPc2d1
kruhů	kruh	k1gInPc2
zanedbatelná	zanedbatelný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
faktor	faktor	k1gInSc1
v	v	k7c6
úspěchu	úspěch	k1gInSc6
NSDAP	NSDAP	kA
před	před	k7c7
rokem	rok	k1gInSc7
1930	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
nebyla	být	k5eNaImAgFnS
ani	ani	k8xC
velmi	velmi	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
potom	potom	k6eAd1
<g/>
,	,	kIx,
průmyslníci	průmyslník	k1gMnPc1
si	se	k3xPyFc3
začali	začít	k5eAaPmAgMnP
všímat	všímat	k5eAaImF
důležitosti	důležitost	k1gFnPc4
NSDAP	NSDAP	kA
v	v	k7c4
destrukci	destrukce	k1gFnSc4
parlamentního	parlamentní	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
stávali	stávat	k5eAaImAgMnP
se	s	k7c7
vlivnými	vlivný	k2eAgMnPc7d1
zejména	zejména	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1932	#num#	k4
až	až	k9
1933	#num#	k4
v	v	k7c6
přesvědčování	přesvědčování	k1gNnSc6
Hindenburga	Hindenburg	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
Hitler	Hitler	k1gMnSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
nějak	nějak	k6eAd1
užitečný	užitečný	k2eAgInSc1d1
v	v	k7c6
tomto	tento	k3xDgInSc6
projektu	projekt	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podpora	podpora	k1gFnSc1
armády	armáda	k1gFnSc2
</s>
<s>
Většina	většina	k1gFnSc1
vyšších	vysoký	k2eAgMnPc2d2
německých	německý	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nesmířila	smířit	k5eNaPmAgNnP
s	s	k7c7
porážkou	porážka	k1gFnSc7
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
s	s	k7c7
nástupem	nástup	k1gInSc7
republikánského	republikánský	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Armádě	armáda	k1gFnSc6
imponovalo	imponovat	k5eAaImAgNnS
<g/>
,	,	kIx,
jaký	jaký	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
význam	význam	k1gInSc4
přikládají	přikládat	k5eAaImIp3nP
nacisté	nacista	k1gMnPc1
vojenským	vojenský	k2eAgFnPc3d1
záležitostem	záležitost	k1gFnPc3
a	a	k8xC
v	v	k7c4
Hitlerovi	Hitler	k1gMnSc3
spatřovala	spatřovat	k5eAaImAgFnS
většina	většina	k1gFnSc1
důstojníků	důstojník	k1gMnPc2
obhájce	obhájce	k1gMnSc4
jejich	jejich	k3xOp3gInPc2
zájmů	zájem	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
opětovně	opětovně	k6eAd1
vyzbrojí	vyzbrojit	k5eAaPmIp3nS
Německo	Německo	k1gNnSc1
a	a	k8xC
zasadí	zasadit	k5eAaPmIp3nP
se	se	k3xPyFc4
o	o	k7c4
revizi	revize	k1gFnSc4
Versailleské	versailleský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
tak	tak	k9
nakonec	nakonec	k6eAd1
mlčky	mlčky	k6eAd1
přihlížela	přihlížet	k5eAaImAgFnS
postupné	postupný	k2eAgFnSc3d1
likvidaci	likvidace	k1gFnSc3
parlamentní	parlamentní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
nastolení	nastolení	k1gNnSc6
vlády	vláda	k1gFnSc2
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
–	–	k?
NSDAP	NSDAP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
hlasy	hlas	k1gInPc1
odporu	odpor	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
armádě	armáda	k1gFnSc6
objevily	objevit	k5eAaPmAgFnP
až	až	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1937	#num#	k4
a	a	k8xC
1938	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
první	první	k4xOgFnPc1
skupinky	skupinka	k1gFnPc1
důstojníků	důstojník	k1gMnPc2
došly	dojít	k5eAaPmAgFnP
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
Hitlera	Hitler	k1gMnSc2
zastavit	zastavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znepokojení	znepokojení	k1gNnSc1
v	v	k7c6
armádě	armáda	k1gFnSc6
nastalo	nastat	k5eAaPmAgNnS
především	především	k9
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Hitler	Hitler	k1gMnSc1
5	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1937	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
záměr	záměr	k1gInSc4
napadnout	napadnout	k5eAaPmF
Československo	Československo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podpora	podpora	k1gFnSc1
konzervativních	konzervativní	k2eAgMnPc2d1
politických	politický	k2eAgMnPc2d1
kruhů	kruh	k1gInPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
NSDAP	NSDAP	kA
tajné	tajný	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
i	i	k9
od	od	k7c2
konzervativních	konzervativní	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
špiček	špička	k1gFnPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1933	#num#	k4
se	se	k3xPyFc4
Hitler	Hitler	k1gMnSc1
sešel	sejít	k5eAaPmAgMnS
s	s	k7c7
významným	významný	k2eAgMnSc7d1
pravicovým	pravicový	k2eAgMnSc7d1
politikem	politik	k1gMnSc7
Franzem	Franz	k1gMnSc7
von	von	k1gInSc4
Papen	Papen	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
spojit	spojit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
Hitlerem	Hitler	k1gMnSc7
proti	proti	k7c3
úřadujícímu	úřadující	k2eAgMnSc3d1
kancléři	kancléř	k1gMnSc3
Kurtu	Kurt	k1gMnSc3
von	von	k1gInSc4
Schleicher	Schleichra	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
vládě	vláda	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Schleicherova	Schleicherův	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
byla	být	k5eAaImAgFnS
otřesená	otřesený	k2eAgFnSc1d1
a	a	k8xC
prezident	prezident	k1gMnSc1
Hindenburg	Hindenburg	k1gMnSc1
začal	začít	k5eAaPmAgMnS
vyjednávat	vyjednávat	k5eAaImF
s	s	k7c7
nacisty	nacista	k1gMnPc7
a	a	k8xC
Franzem	Franz	k1gInSc7
von	von	k1gInSc1
Papen	Papna	k1gFnPc2
o	o	k7c6
nové	nový	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
vedl	vést	k5eAaImAgMnS
Hitler	Hitler	k1gMnSc1
tajné	tajný	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
o	o	k7c6
budoucí	budoucí	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
s	s	k7c7
prezidentovým	prezidentův	k2eAgMnSc7d1
synem	syn	k1gMnSc7
<g/>
,	,	kIx,
plukovníkem	plukovník	k1gMnSc7
Oskarem	Oskar	k1gMnSc7
von	von	k1gInSc4
Hindenburg	Hindenburg	k1gInSc4
(	(	kIx(
<g/>
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
po	po	k7c6
nástupu	nástup	k1gInSc6
nacistů	nacista	k1gMnPc2
k	k	k7c3
moci	moc	k1gFnSc3
získal	získat	k5eAaPmAgInS
hodnost	hodnost	k1gFnSc4
generála	generál	k1gMnSc2
a	a	k8xC
značný	značný	k2eAgInSc4d1
majetek	majetek	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
30	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1933	#num#	k4
dosáhli	dosáhnout	k5eAaPmAgMnP
nacisté	nacista	k1gMnPc1
a	a	k8xC
Franz	Franz	k1gMnSc1
von	von	k1gInSc4
Papen	Papen	k2eAgInSc4d1
svého	svůj	k3xOyFgInSc2
–	–	k?
byla	být	k5eAaImAgFnS
jmenována	jmenovat	k5eAaBmNgFnS,k5eAaImNgFnS
nová	nový	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
a	a	k8xC
Hitler	Hitler	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
kancléřem	kancléř	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
</s>
<s>
U	u	k7c2
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
se	se	k3xPyFc4
NSDAP	NSDAP	kA
těšila	těšit	k5eAaImAgFnS
rozsáhlé	rozsáhlý	k2eAgFnSc3d1
podpoře	podpora	k1gFnSc3
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
nacisté	nacista	k1gMnPc1
přiživovali	přiživovat	k5eAaImAgMnP
nenávist	nenávist	k1gFnSc4
ke	k	k7c3
státům	stát	k1gInPc3
Dohody	dohoda	k1gFnSc2
a	a	k8xC
usilovali	usilovat	k5eAaImAgMnP
o	o	k7c4
revizi	revize	k1gFnSc4
Versaillské	Versaillský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacisté	nacista	k1gMnPc1
také	také	k9
projevovali	projevovat	k5eAaImAgMnP
nesmiřitelný	smiřitelný	k2eNgInSc4d1
odpor	odpor	k1gInSc4
k	k	k7c3
marxismu	marxismus	k1gInSc3
a	a	k8xC
bolševismu	bolševismus	k1gInSc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
k	k	k7c3
ideologiím	ideologie	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
střední	střední	k2eAgFnSc1d1
třída	třída	k1gFnSc1
považovala	považovat	k5eAaImAgFnS
za	za	k7c4
přímé	přímý	k2eAgNnSc4d1
ohrožení	ohrožení	k1gNnSc4
svých	svůj	k3xOyFgFnPc2
tradičních	tradiční	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
justice	justice	k1gFnSc2
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
konzervativní	konzervativní	k2eAgMnPc1d1
právníci	právník	k1gMnPc1
a	a	k8xC
soudci	soudce	k1gMnPc1
nepatřili	patřit	k5eNaImAgMnP
k	k	k7c3
přesvědčeným	přesvědčený	k2eAgMnPc3d1
nacistům	nacista	k1gMnPc3
<g/>
,	,	kIx,
přesto	přesto	k8xC
přivítali	přivítat	k5eAaPmAgMnP
nárůst	nárůst	k1gInSc4
moci	moc	k1gFnSc2
NSDAP	NSDAP	kA
a	a	k8xC
následně	následně	k6eAd1
i	i	k9
Hitlerův	Hitlerův	k2eAgInSc4d1
režim	režim	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
nástupu	nástup	k1gInSc6
tohoto	tento	k3xDgInSc2
režimu	režim	k1gInSc2
spatřovala	spatřovat	k5eAaImAgFnS
justice	justice	k1gFnSc1
návrat	návrat	k1gInSc4
k	k	k7c3
právu	právo	k1gNnSc3
a	a	k8xC
pořádku	pořádek	k1gInSc6
v	v	k7c6
autoritativním	autoritativní	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
jako	jako	k9
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
za	za	k7c2
dob	doba	k1gFnPc2
císařství	císařství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tohoto	tento	k3xDgNnSc2
znovunastolení	znovunastolení	k1gNnSc2
práva	právo	k1gNnSc2
a	a	k8xC
pořádku	pořádek	k1gInSc6
si	se	k3xPyFc3
justice	justice	k1gFnSc1
slibovala	slibovat	k5eAaImAgFnS
nárůst	nárůst	k1gInSc4
upadající	upadající	k2eAgFnSc2d1
prestiže	prestiž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Justice	justice	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k9
postupně	postupně	k6eAd1
zcela	zcela	k6eAd1
podřídila	podřídit	k5eAaPmAgFnS
Hitlerově	Hitlerův	k2eAgFnSc3d1
vůli	vůle	k1gFnSc3
a	a	k8xC
přestala	přestat	k5eAaPmAgFnS
hrát	hrát	k5eAaImF
roli	role	k1gFnSc4
jednoho	jeden	k4xCgInSc2
ze	z	k7c2
tří	tři	k4xCgInPc2
základních	základní	k2eAgInPc2d1
nezávislých	závislý	k2eNgInPc2d1
pilířů	pilíř	k1gInPc2
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
Počty	počet	k1gInPc1
členů	člen	k1gMnPc2
</s>
<s>
1938	#num#	k4
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
vývoj	vývoj	k1gInSc4
členství	členství	k1gNnSc2
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
Členové	člen	k1gMnPc1
</s>
<s>
konec	konec	k1gInSc1
191964	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
19203	#num#	k4
000	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
19216	#num#	k4
000	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
192355	#num#	k4
787	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
192527	#num#	k4
117	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
192649	#num#	k4
523	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
192772	#num#	k4
590	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
1928108	#num#	k4
717	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
1929176	#num#	k4
426	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
1930389	#num#	k4
000	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
1931806	#num#	k4
294	#num#	k4
</s>
<s>
duben	duben	k1gInSc1
19321	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
19321	#num#	k4
200	#num#	k4
000	#num#	k4
</s>
<s>
konec	konec	k1gInSc1
19333	#num#	k4
900	#num#	k4
000	#num#	k4
</s>
<s>
Volební	volební	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Reichstagu	Reichstag	k1gInSc2
z	z	k7c2
března	březen	k1gInSc2
1933	#num#	k4
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
%	%	kIx~
</s>
<s>
mandáty	mandát	k1gInPc4
</s>
<s>
květen	květen	k1gInSc4
1924	#num#	k4
</s>
<s>
1	#num#	k4
918	#num#	k4
300	#num#	k4
</s>
<s>
6,5	6,5	k4
</s>
<s>
32	#num#	k4
</s>
<s>
prosinec	prosinec	k1gInSc1
1924	#num#	k4
</s>
<s>
907	#num#	k4
300	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
květen	květen	k1gInSc4
1928	#num#	k4
</s>
<s>
810	#num#	k4
100	#num#	k4
</s>
<s>
2,6	2,6	k4
</s>
<s>
12	#num#	k4
</s>
<s>
září	zářit	k5eAaImIp3nS
1930	#num#	k4
</s>
<s>
6	#num#	k4
409	#num#	k4
600	#num#	k4
</s>
<s>
18,3	18,3	k4
</s>
<s>
107	#num#	k4
</s>
<s>
červenec	červenec	k1gInSc1
1932	#num#	k4
</s>
<s>
13	#num#	k4
745	#num#	k4
800	#num#	k4
</s>
<s>
37,4	37,4	k4
</s>
<s>
230	#num#	k4
</s>
<s>
listopad	listopad	k1gInSc1
1932	#num#	k4
</s>
<s>
11	#num#	k4
737	#num#	k4
000	#num#	k4
</s>
<s>
33,1	33,1	k4
</s>
<s>
196	#num#	k4
</s>
<s>
březen	březen	k1gInSc1
1933	#num#	k4
</s>
<s>
17	#num#	k4
277	#num#	k4
000	#num#	k4
</s>
<s>
43,9	43,9	k4
</s>
<s>
288	#num#	k4
</s>
<s>
Symboly	symbol	k1gInPc1
strany	strana	k1gFnSc2
</s>
<s>
Parteiadler	Parteiadler	k1gInSc1
reprezentující	reprezentující	k2eAgFnSc2d1
NSDAP	NSDAP	kA
</s>
<s>
Reichsadler	Reichsadler	k1gInSc1
(	(	kIx(
<g/>
říšská	říšský	k2eAgFnSc1d1
orlice	orlice	k1gFnSc1
<g/>
)	)	kIx)
během	během	k7c2
nacistické	nacistický	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
reprezentující	reprezentující	k2eAgNnSc1d1
nacistické	nacistický	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
jako	jako	k8xC,k8xS
národní	národní	k2eAgInSc1d1
symbol	symbol	k1gInSc1
(	(	kIx(
<g/>
Hoheitszeichen	Hoheitszeichen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
říšských	říšský	k2eAgFnPc2d1
marek	marka	k1gFnPc2
před	před	k7c7
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
přidána	přidat	k5eAaPmNgFnS
svastika	svastika	k1gFnSc1
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
odznak	odznak	k1gInSc1
NSDAP	NSDAP	kA
</s>
<s>
Organizace	organizace	k1gFnSc1
NSDAP	NSDAP	kA
</s>
<s>
Územní	územní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
NSDAP	NSDAP	kA
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sturmabteilung	Sturmabteilung	k1gMnSc1
(	(	kIx(
<g/>
SA	SA	kA
<g/>
)	)	kIx)
</s>
<s>
Schutzstaffel	Schutzstaffel	k1gInSc4
(	(	kIx(
<g/>
Allgemeine	Allgemein	k1gMnSc5
SS	SS	kA
a	a	k8xC
Waffen-SS	Waffen-SS	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Fliegerkorps	Fliegerkorps	k1gInSc1
(	(	kIx(
<g/>
NSFK	NSFK	kA
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Kraftfahrkorps	Kraftfahrkorps	k1gInSc1
(	(	kIx(
<g/>
NSKK	NSKK	kA
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Deutscher	Deutschra	k1gFnPc2
Dozentenbund	Dozentenbund	k1gInSc1
(	(	kIx(
<g/>
NSDDB	NSDDB	kA
<g/>
)	)	kIx)
–	–	k?
(	(	kIx(
<g/>
od	od	k7c2
června	červen	k1gInSc2
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hitler	Hitler	k1gMnSc1
-	-	kIx~
Jugend	Jugend	k1gMnSc1
(	(	kIx(
<g/>
HJ	HJ	kA
<g/>
)	)	kIx)
a	a	k8xC
Bund	bund	k1gInSc4
Deutscher	Deutschra	k1gFnPc2
Mädel	Mädel	k1gMnSc1
(	(	kIx(
<g/>
BDM	BDM	kA
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Frauenschaft	Frauenschaft	k1gMnSc1
(	(	kIx(
<g/>
NSF	NSF	kA
<g/>
)	)	kIx)
</s>
<s>
NSDAP	NSDAP	kA
<g/>
/	/	kIx~
<g/>
AO	AO	kA
–	–	k?
zahraniční	zahraniční	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
</s>
<s>
NSDAP	NSDAP	kA
přidružené	přidružený	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
</s>
<s>
Deutsche	Deutsche	k1gFnSc1
Arbeitsfront	Arbeitsfront	k1gMnSc1
(	(	kIx(
<g/>
DAF	DAF	kA
<g/>
)	)	kIx)
</s>
<s>
Reichsarbeitsdienst	Reichsarbeitsdienst	k1gFnSc1
(	(	kIx(
<g/>
RAD	rad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Volkswohlfahrt	Volkswohlfahrt	k1gInSc1
(	(	kIx(
<g/>
NSV	NSV	kA
<g/>
)	)	kIx)
</s>
<s>
Reichsbund	Reichsbund	k1gMnSc1
der	drát	k5eAaImRp2nS
Deutschen	Deutschen	k1gInSc1
Beamten	Beamtno	k1gNnPc2
(	(	kIx(
<g/>
RDB	RDB	kA
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Lehrerbund	Lehrerbund	k1gMnSc1
(	(	kIx(
<g/>
NSLB	NSLB	kA
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Rechtswahrerbund	Rechtswahrerbund	k1gMnSc1
(	(	kIx(
<g/>
NSRB	NSRB	kA
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Bund	bund	k1gInSc1
Deutscher	Deutschra	k1gFnPc2
Technik	technik	k1gMnSc1
(	(	kIx(
<g/>
NSBDT	NSBDT	kA
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Kriegsopferversorgung	Kriegsopferversorgung	k1gMnSc1
(	(	kIx(
<g/>
NSKOV	NSKOV	kA
<g/>
)	)	kIx)
</s>
<s>
NS	NS	kA
-	-	kIx~
Deutscher	Deutschra	k1gFnPc2
Ärztebund	Ärztebund	k1gInSc1
(	(	kIx(
<g/>
NSDÄB	NSDÄB	kA
<g/>
)	)	kIx)
</s>
<s>
NSDAP	NSDAP	kA
kontrolované	kontrolovaný	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
</s>
<s>
NS	NS	kA
-	-	kIx~
Reichskriegerbund	Reichskriegerbund	k1gMnSc1
</s>
<s>
Technische	Technischus	k1gMnSc5
Nothilfe	Nothilf	k1gMnSc5
</s>
<s>
Reichskolonialbund	Reichskolonialbund	k1gMnSc1
</s>
<s>
Deutsches	Deutsches	k1gMnSc1
Rotes	Rotes	k1gMnSc1
Kreuz	Kreuz	k1gMnSc1
(	(	kIx(
<g/>
DRK	DRK	kA
<g/>
)	)	kIx)
</s>
<s>
Reichsluftschutzbund	Reichsluftschutzbund	k1gMnSc1
</s>
<s>
Nationalsozialistischer	Nationalsozialistischra	k1gFnPc2
Reichsbund	Reichsbund	k1gMnSc1
fűr	fűr	k?
Leibesűbungen	Leibesűbungen	k1gInSc1
(	(	kIx(
<g/>
NSRL	NSRL	kA
<g/>
)	)	kIx)
</s>
<s>
Předsedové	předseda	k1gMnPc1
</s>
<s>
Anton	Anton	k1gMnSc1
Drexler	Drexler	k1gMnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Miloš	Miloš	k1gMnSc1
Calda	Calda	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Znečištěný	znečištěný	k2eAgInSc1d1
pramen	pramen	k1gInSc1
od	od	k7c2
Johna	John	k1gMnSc2
Laughlanda	Laughlanda	k1gFnSc1
<g/>
:	:	kIx,
nacismus	nacismus	k1gInSc1
a	a	k8xC
EU	EU	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
část	část	k1gFnSc1
citující	citující	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
Johna	John	k1gMnSc4
Laughlanda	Laughlando	k1gNnSc2
Znečištěný	znečištěný	k2eAgInSc4d1
pramen	pramen	k1gInSc4
<g/>
:	:	kIx,
nedemokratické	demokratický	k2eNgInPc4d1
počátky	počátek	k1gInPc4
evropské	evropský	k2eAgFnSc2d1
ideje	idea	k1gFnSc2
(	(	kIx(
<g/>
The	The	k1gMnSc1
Tainted	Tainted	k1gMnSc1
Source	Source	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Undemocratic	Undemocratice	k1gFnPc2
Origins	Origins	k1gInSc4
of	of	k?
the	the	k?
European	European	k1gInSc1
Idea	idea	k1gFnSc1
<g/>
,	,	kIx,
Little	Little	k1gFnSc1
<g/>
,	,	kIx,
Brown	Brown	k1gMnSc1
and	and	k?
Co	co	k9
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
Warner	Warner	k1gInSc1
Books	Books	k1gInSc1
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
..	..	k?
<g/>
„	„	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Nacismus	nacismus	k1gInSc4
a	a	k8xC
fašismus	fašismus	k1gInSc4
byla	být	k5eAaImAgFnS
zjevně	zjevně	k6eAd1
od	od	k7c2
základů	základ	k1gInPc2
antiindividualistická	antiindividualistický	k2eAgFnSc1d1
<g/>
,	,	kIx,
antiliberální	antiliberální	k2eAgFnSc1d1
<g/>
,	,	kIx,
antiparlamentární	antiparlamentární	k2eAgFnSc1d1
a	a	k8xC
antikapitalistická	antikapitalistický	k2eAgNnPc1d1
hnutí	hnutí	k1gNnPc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
3.10	3.10	k4
<g/>
.2006	.2006	k4
<g/>
,	,	kIx,
euportal	euportat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.druhasvetovavalka.nazory.cz	www.druhasvetovavalka.nazory.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Nazi	Nazi	k1gNnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
Oxford	Oxford	k1gInSc1
American	American	k1gInSc1
Dictionary	Dictionara	k1gFnSc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
nd	nd	k?
ed	ed	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MCGOWAN	MCGOWAN	kA
<g/>
,	,	kIx,
Lee	Lea	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
radical	radicat	k5eAaPmAgMnS
right	right	k5eAaPmF
in	in	k?
Germany	German	k1gInPc4
<g/>
:	:	kIx,
1870	#num#	k4
to	ten	k3xDgNnSc1
the	the	k?
present	present	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harlow	Harlow	k1gMnSc1
<g/>
:	:	kIx,
Longman	Longman	k1gMnSc1
Publishing	Publishing	k1gInSc4
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
582	#num#	k4
<g/>
-	-	kIx~
<g/>
29193	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
57	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
David	David	k1gMnSc1
Welch	Welch	k1gMnSc1
<g/>
,	,	kIx,
Němci	Němec	k1gMnSc3
proti	proti	k7c3
Hitlerovi	Hitler	k1gMnSc3
<g/>
,	,	kIx,
Opozice	opozice	k1gFnSc1
v	v	k7c6
třetí	třetí	k4xOgFnSc6
říši	říš	k1gFnSc6
1933	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gMnSc1
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
1379	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.6	.6	k4
-	-	kIx~
31	#num#	k4
<g/>
↑	↑	k?
http://www.osel.cz/10044-jaky-byl-skutecny-vliv-hitlerovych-plamennych-projevu-na-uspech-nsdap.html	http://www.osel.cz/10044-jaky-byl-skutecny-vliv-hitlerovych-plamennych-projevu-na-uspech-nsdap.htmla	k1gFnPc2
-	-	kIx~
Jaký	jaký	k9
byl	být	k5eAaImAgInS
skutečný	skutečný	k2eAgInSc1d1
vliv	vliv	k1gInSc1
Hitlerových	Hitlerová	k1gFnPc2
plamenných	plamenný	k2eAgInPc2d1
projevů	projev	k1gInPc2
na	na	k7c4
úspěch	úspěch	k1gInSc4
NSDAP	NSDAP	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
FULLBROOK	FULLBROOK	kA
<g/>
,	,	kIx,
Mary	Mary	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Concise	Concise	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
Germany	German	k1gMnPc7
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780521540711	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
174	#num#	k4
<g/>
↑	↑	k?
FULLBROOK	FULLBROOK	kA
<g/>
,	,	kIx,
Mary	Mary	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Concise	Concise	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
Germany	German	k1gMnPc7
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780521540711	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
174	#num#	k4
<g/>
,	,	kIx,
177	#num#	k4
<g/>
↑	↑	k?
David	David	k1gMnSc1
Welch	Welch	k1gMnSc1
<g/>
,	,	kIx,
Němci	Němec	k1gMnSc3
proti	proti	k7c3
Hitlerovi	Hitler	k1gMnSc3
<g/>
,	,	kIx,
Opozice	opozice	k1gFnSc1
v	v	k7c6
třetí	třetí	k4xOgFnSc6
říši	říš	k1gFnSc6
1933	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gMnSc1
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
1379	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.145	.145	k4
<g/>
↑	↑	k?
David	David	k1gMnSc1
Welch	Welch	k1gMnSc1
<g/>
,	,	kIx,
Němci	Němec	k1gMnSc3
proti	proti	k7c3
Hitlerovi	Hitler	k1gMnSc3
<g/>
,	,	kIx,
Opozice	opozice	k1gFnSc1
v	v	k7c6
třetí	třetí	k4xOgFnSc6
říši	říš	k1gFnSc6
1933	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gMnSc1
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
1379	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.1471	.1471	k4
2	#num#	k4
David	David	k1gMnSc1
Welch	Welch	k1gMnSc1
<g/>
,	,	kIx,
Němci	Němec	k1gMnSc3
proti	proti	k7c3
Hitlerovi	Hitler	k1gMnSc3
<g/>
,	,	kIx,
Opozice	opozice	k1gFnSc1
v	v	k7c6
třetí	třetí	k4xOgFnSc6
říši	říš	k1gFnSc6
1933	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gMnSc1
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
1379	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.76	.76	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Schlag	Schlag	k1gInSc1
nach	nach	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
;	;	kIx,
Bibliographisches	Bibliographisches	k1gInSc1
Institut	institut	k1gInSc1
Leipzig	Leipziga	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auflage	Auflage	k1gNnSc1
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Krieger	Krieger	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
Oxfordský	oxfordský	k2eAgInSc1d1
slovník	slovník	k1gInSc1
světové	světový	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2000	#num#	k4
</s>
<s>
Pečenka	pečenka	k1gFnSc1
M.	M.	kA
<g/>
,	,	kIx,
Luňák	Luňák	k1gMnSc1
P.	P.	kA
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
moderní	moderní	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelství	nakladatelství	k1gNnSc1
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1999	#num#	k4
</s>
<s>
kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
osobností	osobnost	k1gFnPc2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelský	nakladatelský	k2eAgInSc4d1
dům	dům	k1gInSc4
OP	op	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
1993	#num#	k4
</s>
<s>
Kuklík	kuklík	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Kuklík	Kuklík	k1gInSc1
J.	J.	kA
Dějepis	dějepis	k1gInSc4
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SPN	SPN	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2005	#num#	k4
</s>
<s>
sestavil	sestavit	k5eAaPmAgMnS
Bowman	Bowman	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
<g/>
,	,	kIx,
<g/>
B.	B.	kA
Kronika	kronika	k1gFnSc1
dějin	dějiny	k1gFnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Columbus	Columbus	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
<g/>
1993	#num#	k4
</s>
<s>
Hitler	Hitler	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
Můj	můj	k3xOp1gInSc4
boj	boj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OtakarII	OtakarII	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Pohořelice	Pohořelice	k1gFnPc1
<g/>
:	:	kIx,
<g/>
2000	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Program	program	k1gInSc1
NSDAP	NSDAP	kA
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Sieg	Sieg	k1gMnSc1
Heil	Heil	k1gMnSc1
</s>
<s>
Heil	Heil	k1gMnSc1
Hitler	Hitler	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Program	program	k1gInSc1
NSDAP	NSDAP	kA
holocaust	holocaust	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nacismus	nacismus	k1gInSc1
Organizace	organizace	k1gFnSc2
</s>
<s>
Národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
NSDAP	NSDAP	kA
<g/>
)	)	kIx)
•	•	k?
Sturmabteilung	Sturmabteilung	k1gMnSc1
(	(	kIx(
<g/>
SA	SA	kA
<g/>
)	)	kIx)
•	•	k?
Schutzstaffel	Schutzstaffel	k1gMnSc1
(	(	kIx(
<g/>
SS	SS	kA
<g/>
)	)	kIx)
•	•	k?
Gestapo	gestapo	k1gNnSc4
•	•	k?
Hitlerjugend	Hitlerjugend	k1gInSc1
•	•	k?
Svaz	svaz	k1gInSc1
německých	německý	k2eAgFnPc2d1
dívek	dívka	k1gFnPc2
•	•	k?
Reichsarbeitsdienst	Reichsarbeitsdienst	k1gMnSc1
•	•	k?
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
•	•	k?
RuSHA	RuSHA	k1gMnSc1
•	•	k?
VoMi	VoM	k1gFnSc2
•	•	k?
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1
Historie	historie	k1gFnPc4
</s>
<s>
Pivní	pivní	k2eAgInSc1d1
puč	puč	k1gInSc1
•	•	k?
Nacistické	nacistický	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
•	•	k?
Noc	noc	k1gFnSc1
dlouhých	dlouhý	k2eAgInPc2d1
nožů	nůž	k1gInPc2
•	•	k?
Požár	požár	k1gInSc1
Říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
•	•	k?
Pakt	pakt	k1gInSc4
proti	proti	k7c3
Kominterně	Kominterna	k1gFnSc3
•	•	k?
Křišťálová	křišťálový	k2eAgFnSc1d1
noc	noc	k1gFnSc1
•	•	k?
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Pakt	pakt	k1gInSc1
tří	tři	k4xCgInPc2
•	•	k?
Holokaust	holokaust	k1gInSc1
•	•	k?
Norimberský	norimberský	k2eAgInSc1d1
proces	proces	k1gInSc1
•	•	k?
Denacifikace	denacifikace	k1gFnSc2
•	•	k?
Neonacismus	neonacismus	k1gInSc1
Ideologie	ideologie	k1gFnSc1
a	a	k8xC
rasismus	rasismus	k1gInSc1
</s>
<s>
Ezoterický	Ezoterický	k2eAgInSc1d1
nacismus	nacismus	k1gInSc1
•	•	k?
Nacistická	nacistický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
•	•	k?
Program	program	k1gInSc1
NSDAP	NSDAP	kA
•	•	k?
Mein	Mein	k1gMnSc1
Kampf	Kampf	k1gMnSc1
•	•	k?
Antidemokracie	Antidemokracie	k1gFnSc2
•	•	k?
Blut	Blut	k1gInSc1
und	und	k?
Boden	bůst	k5eAaImNgInS,k5eAaPmNgInS
•	•	k?
Heim	Heim	k1gInSc1
ins	ins	k?
Reich	Reich	k?
•	•	k?
Panská	panský	k2eAgFnSc1d1
rasa	rasa	k1gFnSc1
•	•	k?
Lebensraum	Lebensraum	k1gInSc1
•	•	k?
Velkoněmecká	velkoněmecký	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
•	•	k?
Norimberské	norimberský	k2eAgInPc1d1
zákony	zákon	k1gInPc1
•	•	k?
Lebensborn	Lebensborn	k1gInSc1
•	•	k?
Akce	akce	k1gFnSc1
T4	T4	k1gFnSc2
•	•	k?
Konečné	Konečné	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
židovské	židovský	k2eAgFnSc2d1
otázky	otázka	k1gFnSc2
•	•	k?
Nacistické	nacistický	k2eAgNnSc1d1
experimentování	experimentování	k1gNnSc1
na	na	k7c6
lidech	lid	k1gInPc6
•	•	k?
Gleichschaltung	Gleichschaltung	k1gInSc4
Mimo	mimo	k7c4
Německo	Německo	k1gNnSc4
</s>
<s>
Strana	strana	k1gFnSc1
Šípových	Šípových	k2eAgInPc2d1
křížů	kříž	k1gInPc2
•	•	k?
Nationaal-Socialistische	Nationaal-Socialistische	k1gInSc1
Beweging	Beweging	k1gInSc1
•	•	k?
Nasjonal	Nasjonal	k1gFnSc1
Samling	Samling	k1gInSc1
•	•	k?
German-American	German-American	k1gInSc1
Bund	bund	k1gInSc1
•	•	k?
Zlatý	zlatý	k2eAgInSc1d1
úsvit	úsvit	k1gInSc1
Související	související	k2eAgFnSc2d1
</s>
<s>
Heil	Heil	k1gMnSc1
Hitler	Hitler	k1gMnSc1
•	•	k?
Sieg	Sieg	k1gMnSc1
Heil	Heil	k1gMnSc1
•	•	k?
Triumf	triumf	k1gInSc1
vůle	vůle	k1gFnSc1
•	•	k?
Kinderlandverschickung	Kinderlandverschickunga	k1gFnPc2
•	•	k?
Krajní	krajní	k2eAgFnSc2d1
pravice	pravice	k1gFnSc2
•	•	k?
Německý	německý	k2eAgInSc4d1
odboj	odboj	k1gInSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711296	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1012979-0	1012979-0	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2193	#num#	k4
6797	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79053942	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
146810847	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79053942	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Historie	historie	k1gFnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
