<s>
Národně	národně	k6eAd1
socialistická	socialistický	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
dělnická	dělnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
:	:	kIx,
Nationalsozialistische	Nationalsozialistische	k2eAgFnSc1d1
Deutsche	Deutsch	k2eAgFnSc1d1
Arbeiterpartei	Arbeiterparte	k1gFnSc1
<g/>
,	,	kIx,
NSDAP	NSDAP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
nacistická	nacistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
německá	německý	k2eAgFnSc1d1
krajně	krajně	k6eAd1
pravicová	pravicový	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1920	#num#	k4
přejmenováním	přejmenování	k1gNnSc7
Německé	německý	k2eAgFnSc2d1
dělnické	dělnický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
již	již	k6eAd1
založili	založit	k5eAaPmAgMnP
Anton	Anton	k1gMnSc1
Drexler	Drexler	k1gMnSc1
a	a	k8xC
Karl	Karl	k1gMnSc1
Harrer	Harrer	k1gMnSc1
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1919	#num#	k4
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>