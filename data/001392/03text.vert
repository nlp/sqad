<s>
Rhodium	rhodium	k1gNnSc1	rhodium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Rh	Rh	k1gFnSc1	Rh
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Rhodium	rhodium	k1gNnSc1	rhodium
je	být	k5eAaImIp3nS	být
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
stálé	stálý	k2eAgNnSc1d1	stálé
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
vysokým	vysoký	k2eAgInSc7d1	vysoký
bodem	bod	k1gInSc7	bod
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
s	s	k7c7	s
platinou	platina	k1gFnSc7	platina
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
termočlánků	termočlánek	k1gInPc2	termočlánek
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
a	a	k8xC	a
teplotně	teplotně	k6eAd1	teplotně
odolných	odolný	k2eAgFnPc2d1	odolná
součástí	součást	k1gFnPc2	součást
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
výrobních	výrobní	k2eAgNnPc2d1	výrobní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Rhodium	rhodium	k1gNnSc1	rhodium
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
chemicky	chemicky	k6eAd1	chemicky
odolné	odolný	k2eAgFnPc1d1	odolná
a	a	k8xC	a
neochotně	ochotně	k6eNd1	ochotně
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
lučavce	lučavka	k1gFnSc6	lučavka
královské	královský	k2eAgFnSc6d1	královská
nebo	nebo	k8xC	nebo
za	za	k7c2	za
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
chloristanu	chloristan	k1gInSc2	chloristan
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
rutheniem	ruthenium	k1gNnSc7	ruthenium
a	a	k8xC	a
palladiem	palladion	k1gNnSc7	palladion
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
triády	triáda	k1gFnSc2	triáda
lehkých	lehký	k2eAgInPc2d1	lehký
platinových	platinový	k2eAgInPc2d1	platinový
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Rhodium	rhodium	k1gNnSc1	rhodium
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
anglický	anglický	k2eAgMnSc1d1	anglický
chemik	chemik	k1gMnSc1	chemik
William	William	k1gInSc4	William
Hyde	Hyd	k1gInSc2	Hyd
Wollaston	Wollaston	k1gInSc4	Wollaston
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
objevil	objevit	k5eAaPmAgMnS	objevit
palladium	palladium	k1gNnSc4	palladium
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc4	název
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
podle	podle	k7c2	podle
růžové	růžový	k2eAgFnSc2d1	růžová
(	(	kIx(	(
<g/>
růže	růž	k1gFnSc2	růž
je	být	k5eAaImIp3nS	být
řecky	řecky	k6eAd1	řecky
rhodon	rhodon	k1gNnSc1	rhodon
<g/>
)	)	kIx)	)
barvy	barva	k1gFnPc1	barva
rhodiových	rhodiový	k2eAgFnPc2d1	rhodiová
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ušlechtilý	ušlechtilý	k2eAgInSc4d1	ušlechtilý
<g/>
,	,	kIx,	,
odolný	odolný	k2eAgInSc4d1	odolný
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
i	i	k9	i
tepelně	tepelně	k6eAd1	tepelně
středně	středně	k6eAd1	středně
dobře	dobře	k6eAd1	dobře
vodivý	vodivý	k2eAgInSc1d1	vodivý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
ryzí	ryzí	k2eAgMnPc4d1	ryzí
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
společně	společně	k6eAd1	společně
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
drahými	drahý	k2eAgInPc7d1	drahý
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
světovými	světový	k2eAgFnPc7d1	světová
nalezišti	naleziště	k1gNnSc3	naleziště
jsou	být	k5eAaImIp3nP	být
platinové	platinový	k2eAgInPc1d1	platinový
doly	dol	k1gInPc1	dol
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Ural	Ural	k1gInSc4	Ural
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
rhodium	rhodium	k1gNnSc1	rhodium
společně	společně	k6eAd1	společně
se	s	k7c7	s
zlatonosnými	zlatonosný	k2eAgFnPc7d1	zlatonosná
rudami	ruda	k1gFnPc7	ruda
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
rud	ruda	k1gFnPc2	ruda
niklových	niklový	k2eAgFnPc2d1	niklová
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
své	svůj	k3xOyFgFnSc2	svůj
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
chemické	chemický	k2eAgFnSc2d1	chemická
odolnosti	odolnost	k1gFnSc2	odolnost
se	se	k3xPyFc4	se
slitiny	slitina	k1gFnSc2	slitina
rhodia	rhodium	k1gNnSc2	rhodium
s	s	k7c7	s
platinou	platina	k1gFnSc7	platina
a	a	k8xC	a
iridiem	iridium	k1gNnSc7	iridium
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
odolného	odolný	k2eAgNnSc2d1	odolné
chemického	chemický	k2eAgNnSc2d1	chemické
nádobí	nádobí	k1gNnSc2	nádobí
pro	pro	k7c4	pro
rozklady	rozklad	k1gInPc4	rozklad
vzorků	vzorek	k1gInPc2	vzorek
tavením	tavení	k1gNnSc7	tavení
nebo	nebo	k8xC	nebo
spalováním	spalování	k1gNnSc7	spalování
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
slouží	sloužit	k5eAaImIp3nP	sloužit
jeho	jeho	k3xOp3gFnPc1	jeho
slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
platinou	platina	k1gFnSc7	platina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zařízení	zařízení	k1gNnSc2	zařízení
na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
rhodium	rhodium	k1gNnSc1	rhodium
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgInPc2	některý
speciálních	speciální	k2eAgInPc2d1	speciální
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
platinou	platina	k1gFnSc7	platina
a	a	k8xC	a
palladiem	palladion	k1gNnSc7	palladion
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
autokatalyzátorů	autokatalyzátor	k1gInPc2	autokatalyzátor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
výfukových	výfukový	k2eAgInPc2d1	výfukový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
velkých	velký	k2eAgInPc2d1	velký
objemů	objem	k1gInPc2	objem
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výroba	výroba	k1gFnSc1	výroba
termočlánků	termočlánek	k1gInPc2	termočlánek
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
na	na	k7c4	na
bázi	báze	k1gFnSc4	báze
slitin	slitina	k1gFnPc2	slitina
rhodia	rhodium	k1gNnSc2	rhodium
s	s	k7c7	s
platinou	platina	k1gFnSc7	platina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
rhodium	rhodium	k1gNnSc1	rhodium
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
k	k	k7c3	k
pokovování	pokovování	k1gNnSc3	pokovování
méně	málo	k6eAd2	málo
ušlechtilých	ušlechtilý	k2eAgMnPc2d1	ušlechtilý
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
rhodiují	rhodiovat	k5eAaImIp3nP	rhodiovat
stříbrné	stříbrný	k2eAgInPc4d1	stříbrný
šperky	šperk	k1gInPc4	šperk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
chráněny	chránit	k5eAaImNgFnP	chránit
před	před	k7c7	před
korozivním	korozivní	k2eAgNnSc7d1	korozivní
černáním	černání	k1gNnSc7	černání
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
lesk	lesk	k1gInSc1	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rhodium	rhodium	k1gNnSc4	rhodium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rhodium	rhodium	k1gNnSc4	rhodium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
</s>
