<s>
Counter-Strike	Counter-Strike	k6eAd1
<g/>
:	:	kIx,
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Counter-Strike	Counter-Strike	k1gInSc1
<g/>
:	:	kIx,
Global	globat	k5eAaImAgInS
OffensiveZákladní	OffensiveZákladný	k2eAgMnPc1d1
informaceVývojářiValveHidden	informaceVývojářiValveHiddno	k1gNnPc2
Path	Path	k1gMnSc1
EntertainmentVydavatelValveSkladatelMike	EntertainmentVydavatelValveSkladatelMik	k1gInSc2
MoraskyHerní	MoraskyHerní	k2eAgInSc4d1
sérieCounter-StrikeEngineSourcePlatformyMicrosoft	sérieCounter-StrikeEngineSourcePlatformyMicrosoft	k2eAgInSc4d1
WindowsmacOSLinuxPlayStation	WindowsmacOSLinuxPlayStation	k1gInSc4
3	#num#	k4
<g/>
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
Datum	datum	k1gNnSc4
vydání	vydání	k1gNnSc2
<g/>
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2012	#num#	k4
<g/>
Žánrfirst-person	Žánrfirst-person	k1gMnSc1
shooterHerní	shooterHerní	k2eAgFnSc2d1
módmultiplayerKlasifikacePEGI	módmultiplayerKlasifikacePEGI	k?
ESRB	ESRB	kA
Posloupnost	posloupnost	k1gFnSc1
</s>
<s>
Counter-Strike	Counter-Strike	k1gFnSc1
<g/>
:	:	kIx,
Source	Source	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Counter-Strike	Counter-Strike	k6eAd1
<g/>
:	:	kIx,
Global	global	k5eAaImAgMnS
Offensive	offensive	k1gFnPc4
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
počítačová	počítačový	k2eAgFnSc1d1
online	onlinout	k5eAaPmIp3nS
FPS	FPS	kA
(	(	kIx(
<g/>
First	First	k1gMnSc1
Person	persona	k1gFnPc2
Shooter	Shooter	k1gMnSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
střílecí	střílecí	k2eAgFnSc1d1
hra	hra	k1gFnSc1
z	z	k7c2
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hru	hra	k1gFnSc4
vyvinulo	vyvinout	k5eAaPmAgNnS
Valve	Valev	k1gFnSc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Hidden	Hiddna	k1gFnPc2
Path	Path	k1gMnSc1
Entertainment	Entertainment	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
předchozí	předchozí	k2eAgFnPc4d1
verze	verze	k1gFnPc4
hry	hra	k1gFnSc2
Counter-Strike	Counter-Strik	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
v	v	k7c6
pořadí	pořadí	k1gNnSc6
již	již	k6eAd1
čtvrtá	čtvrtý	k4xOgFnSc1
hra	hra	k1gFnSc1
ze	z	k7c2
série	série	k1gFnSc2
Counter-Strike	Counter-Strik	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2012	#num#	k4
k	k	k7c3
dispozici	dispozice	k1gFnSc3
na	na	k7c6
Windows	Windows	kA
<g/>
,	,	kIx,
OS	OS	kA
X	X	kA
a	a	k8xC
na	na	k7c6
platformách	platforma	k1gFnPc6
Steam	Steam	k1gInSc1
<g/>
,	,	kIx,
Xbox	Xbox	k1gInSc1
Live	Liv	k1gFnSc2
Arcade	Arcad	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
pro	pro	k7c4
Linux	linux	k1gInSc4
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
září	září	k1gNnSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
například	například	k6eAd1
předělané	předělaný	k2eAgFnPc4d1
verze	verze	k1gFnPc4
klasických	klasický	k2eAgFnPc2d1
map	mapa	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
úplně	úplně	k6eAd1
nové	nový	k2eAgFnPc4d1
mapy	mapa	k1gFnPc4
<g/>
,	,	kIx,
postavy	postava	k1gFnPc4
a	a	k8xC
herní	herní	k2eAgFnPc4d1
módy	móda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Multiplatformní	multiplatformní	k2eAgInSc1d1
multiplayer	multiplayer	k1gInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
plánován	plánovat	k5eAaImNgInS
pro	pro	k7c4
Windows	Windows	kA
<g/>
,	,	kIx,
OS	OS	kA
X	X	kA
a	a	k8xC
PSN	PSN	kA
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgInS
umožněn	umožnit	k5eAaPmNgInS
pouze	pouze	k6eAd1
pro	pro	k7c4
Windows	Windows	kA
a	a	k8xC
OS	OS	kA
X.	X.	kA
PSN	PSN	kA
verze	verze	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
metody	metoda	k1gFnPc4
ovládání	ovládání	k1gNnSc2
<g/>
:	:	kIx,
ovladač	ovladač	k1gInSc1
DualShock	DualShock	k1gInSc1
3	#num#	k4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
Move	Mov	k1gFnSc2
nebo	nebo	k8xC
USB	USB	kA
klávesnici	klávesnice	k1gFnSc6
<g/>
/	/	kIx~
<g/>
myš	myš	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualizace	aktualizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
umožnila	umožnit	k5eAaPmAgFnS
hru	hra	k1gFnSc4
hrát	hrát	k5eAaImF
jako	jako	k8xS,k8xC
free	freat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
play	play	k0
zdarma	zdarma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelé	uživatel	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
hru	hra	k1gFnSc4
zakoupili	zakoupit	k5eAaPmAgMnP
před	před	k7c7
tímto	tento	k3xDgNnSc7
datem	datum	k1gNnSc7
<g/>
,	,	kIx,
dostali	dostat	k5eAaPmAgMnP
zvláštní	zvláštní	k2eAgInSc4d1
status	status	k1gInSc4
"	"	kIx"
<g/>
Prime	prim	k1gInSc5
<g/>
"	"	kIx"
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
jim	on	k3xPp3gMnPc3
umožňuje	umožňovat	k5eAaImIp3nS
přístup	přístup	k1gInSc1
k	k	k7c3
lepšímu	dobrý	k2eAgNnSc3d2
vybavení	vybavení	k1gNnSc3
a	a	k8xC
herním	herní	k2eAgInPc3d1
režimům	režim	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
nová	nový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
představila	představit	k5eAaPmAgFnS
režim	režim	k1gInSc4
tzv.	tzv.	kA
battle	battlat	k5eAaPmIp3nS
royale	royale	k6eAd1
nazvaný	nazvaný	k2eAgInSc1d1
"	"	kIx"
<g/>
Danger	Danger	k1gInSc1
Zone	Zon	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Hratelnost	hratelnost	k1gFnSc1
</s>
<s>
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnPc1
předchůdci	předchůdce	k1gMnPc1
v	v	k7c6
sérii	série	k1gFnSc6
Counter-Strike	Counter-Strik	k1gFnSc2
<g/>
,	,	kIx,
střílečkou	střílečka	k1gFnSc7
z	z	k7c2
pohledu	pohled	k1gInSc2
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
pro	pro	k7c4
více	hodně	k6eAd2
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
hráči	hráč	k1gMnPc1
plní	plnit	k5eAaImIp3nP
předem	předem	k6eAd1
určené	určený	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
herních	herní	k2eAgInPc6d1
módech	mód	k1gInPc6
proti	proti	k7c3
sobě	se	k3xPyFc3
soutěží	soutěžit	k5eAaImIp3nP
dva	dva	k4xCgInPc1
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
Teroristé	terorista	k1gMnPc1
a	a	k8xC
Counter-Teroristé	Counter-Terorista	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
plní	plnit	k5eAaImIp3nP
úkoly	úkol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
jimi	on	k3xPp3gMnPc7
například	například	k6eAd1
položení	položení	k1gNnSc1
nebo	nebo	k8xC
zneškodnění	zneškodnění	k1gNnSc1
bobmy	bobma	k1gFnSc2
a	a	k8xC
záchrana	záchrana	k1gFnSc1
nebo	nebo	k8xC
únos	únos	k1gInSc1
rukojmích	rukojmí	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hráči	hráč	k1gMnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
konci	konec	k1gInSc6
každého	každý	k3xTgInSc2
z	z	k7c2
kol	kolo	k1gNnPc2
odměněni	odměnit	k5eAaPmNgMnP
dle	dle	k7c2
jejich	jejich	k3xOp3gFnPc2
individuálních	individuální	k2eAgFnPc2d1
a	a	k8xC
týmových	týmový	k2eAgMnPc2d1
výkonů	výkon	k1gInPc2
herní	herní	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
nakupovat	nakupovat	k5eAaBmF
potřebné	potřebný	k2eAgFnPc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
zbraně	zbraň	k1gFnPc1
či	či	k8xC
vybavení	vybavení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Hráči	hráč	k1gMnPc1
většinou	většina	k1gFnSc7
získávají	získávat	k5eAaImIp3nP
za	za	k7c4
vyhrané	vyhraný	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
větší	veliký	k2eAgNnSc4d2
množství	množství	k1gNnSc4
peněz	peníze	k1gInPc2
než	než	k8xS
za	za	k7c4
prohrané	prohraná	k1gFnPc4
a	a	k8xC
za	za	k7c4
plnění	plnění	k1gNnSc4
úkolů	úkol	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
zabíjení	zabíjení	k1gNnSc2
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
dostávají	dostávat	k5eAaImIp3nP
další	další	k2eAgInPc1d1
bonusy	bonus	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Herní	herní	k2eAgFnPc1d1
módy	móda	k1gFnPc1
</s>
<s>
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
momentálně	momentálně	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
osm	osm	k4xCc1
herních	herní	k2eAgInPc2d1
módů	mód	k1gInPc2
ke	k	k7c3
hraní	hraní	k1gNnSc3
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
</s>
<s>
Casual	Casual	k1gInSc1
(	(	kIx(
<g/>
Nenáročný	náročný	k2eNgMnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
Competitive	Competitiv	k1gInSc5
(	(	kIx(
<g/>
Kompetitivní	kompetitivní	k2eAgMnPc4d1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Nejznámější	známý	k2eAgFnSc2d3
herní	herní	k2eAgFnSc2d1
módy	móda	k1gFnSc2
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
Mise	mise	k1gFnPc4
s	s	k7c7
bombou	bomba	k1gFnSc7
a	a	k8xC
Mise	mise	k1gFnSc1
s	s	k7c7
rukojmími	rukojmí	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nenáročný	náročný	k2eNgInSc1d1
mód	mód	k1gInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
především	především	k9
pro	pro	k7c4
občasné	občasný	k2eAgMnPc4d1
nebo	nebo	k8xC
začínající	začínající	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
<g/>
,	,	kIx,
na	na	k7c6
serveru	server	k1gInSc6
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
hráčů	hráč	k1gMnPc2
a	a	k8xC
mód	móda	k1gFnPc2
je	být	k5eAaImIp3nS
celkově	celkově	k6eAd1
zrychlen	zrychlen	k2eAgMnSc1d1
a	a	k8xC
ulehčen	ulehčen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nenáročném	náročný	k2eNgInSc6d1
módu	mód	k1gInSc6
je	být	k5eAaImIp3nS
vypnuta	vypnut	k2eAgFnSc1d1
palba	palba	k1gFnSc1
do	do	k7c2
vlastních	vlastní	k2eAgFnPc2d1
řad	řada	k1gFnPc2
<g/>
,	,	kIx,
hráči	hráč	k1gMnPc1
nemohou	moct	k5eNaImIp3nP
zranit	zranit	k5eAaPmF
své	svůj	k3xOyFgMnPc4
spoluhráče	spoluhráč	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompetitivní	kompetitivní	k2eAgInSc1d1
mód	mód	k1gInSc1
je	být	k5eAaImIp3nS
zaměřen	zaměřit	k5eAaPmNgInS
na	na	k7c4
soutěživé	soutěživý	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
vyhledá	vyhledat	k5eAaPmIp3nS
server	server	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
hraje	hrát	k5eAaImIp3nS
5	#num#	k4
proti	proti	k7c3
5	#num#	k4
hráčům	hráč	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrává	vyhrávat	k5eAaImIp3nS
tým	tým	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
jako	jako	k8xS,k8xC
první	první	k4xOgInSc1
dosáhne	dosáhnout	k5eAaPmIp3nS
16	#num#	k4
vyhraných	vyhraný	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápas	zápas	k1gInSc1
může	moct	k5eAaImIp3nS
skončit	skončit	k5eAaPmF
remízou	remíza	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
úspěchů	úspěch	k1gInPc2
a	a	k8xC
individuálních	individuální	k2eAgFnPc2d1
dovedností	dovednost	k1gFnPc2
hráče	hráč	k1gMnPc4
je	být	k5eAaImIp3nS
zařazen	zařadit	k5eAaPmNgInS
do	do	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
18	#num#	k4
dovednostních	dovednostní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
které	který	k3yIgFnSc3,k3yRgFnSc3,k3yQgFnSc3
jsou	být	k5eAaImIp3nP
kompetitivní	kompetitivní	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
vyrovnané	vyrovnaný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovednostní	dovednostní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
se	se	k3xPyFc4
časem	časem	k6eAd1
mění	měnit	k5eAaImIp3nS
podle	podle	k7c2
výkonů	výkon	k1gInPc2
hráče	hráč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
každého	každý	k3xTgNnSc2
kola	kolo	k1gNnSc2
si	se	k3xPyFc3
hráči	hráč	k1gMnPc1
mohou	moct	k5eAaImIp3nP
koupit	koupit	k5eAaPmF
zbraně	zbraň	k1gFnPc4
a	a	k8xC
vybavení	vybavení	k1gNnSc4
za	za	k7c4
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
v	v	k7c6
minulých	minulý	k2eAgNnPc6d1
kolech	kolo	k1gNnPc6
získali	získat	k5eAaPmAgMnP
například	například	k6eAd1
za	za	k7c4
zabití	zabití	k1gNnSc4
či	či	k8xC
asistenci	asistence	k1gFnSc4
na	na	k7c4
zabití	zabití	k1gNnSc4
nepřítele	nepřítel	k1gMnSc2
nebo	nebo	k8xC
plnění	plnění	k1gNnSc6
různých	různý	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
mapy	mapa	k1gFnSc2
(	(	kIx(
<g/>
položení	položení	k1gNnSc1
bomby	bomba	k1gFnSc2
<g/>
,	,	kIx,
zneškodnění	zneškodnění	k1gNnSc2
bomby	bomba	k1gFnSc2
<g/>
,	,	kIx,
záchrana	záchrana	k1gFnSc1
rukojmích	rukojmí	k1gMnPc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nenáročném	náročný	k2eNgInSc6d1
módu	mód	k1gInSc6
si	se	k3xPyFc3
hráči	hráč	k1gMnPc1
nemusí	muset	k5eNaImIp3nP
kupovat	kupovat	k5eAaImF
vestu	vesta	k1gFnSc4
a	a	k8xC
helmu	helma	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
dostanou	dostat	k5eAaPmIp3nP
každé	každý	k3xTgNnSc4
kolo	kolo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
typ	typ	k1gInSc4
Mise	mise	k1gFnSc2
končí	končit	k5eAaImIp3nS
každé	každý	k3xTgNnSc1
kolo	kolo	k1gNnSc1
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
jeden	jeden	k4xCgInSc1
z	z	k7c2
týmů	tým	k1gInPc2
buď	buď	k8xC
dokončí	dokončit	k5eAaPmIp3nP
svůj	svůj	k3xOyFgInSc4
úkol	úkol	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
zabije	zabít	k5eAaPmIp3nS
celý	celý	k2eAgInSc4d1
nepřátelský	přátelský	k2eNgInSc4d1
tým	tým	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
nebo	nebo	k8xC
nechá	nechat	k5eAaPmIp3nS
vypršet	vypršet	k5eAaPmF
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Mise	mise	k1gFnSc1
s	s	k7c7
bombou	bomba	k1gFnSc7
<g/>
:	:	kIx,
Teroristé	terorista	k1gMnPc1
mají	mít	k5eAaImIp3nP
za	za	k7c4
úkol	úkol	k1gInSc4
položit	položit	k5eAaPmF
bombu	bomba	k1gFnSc4
na	na	k7c4
jedno	jeden	k4xCgNnSc4
ze	z	k7c2
dvou	dva	k4xCgNnPc2
určených	určený	k2eAgNnPc2d1
položišť	položiště	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
označena	označit	k5eAaPmNgNnP
písmeny	písmeno	k1gNnPc7
A	A	kA
a	a	k8xC
B	B	kA
<g/>
;	;	kIx,
Counter-Teroristé	Counter-Terorista	k1gMnPc1
mají	mít	k5eAaImIp3nP
za	za	k7c4
úkol	úkol	k1gInSc4
předejít	předejít	k5eAaPmF
výbuchu	výbuch	k1gInSc3
bomby	bomba	k1gFnSc2
buďto	buďto	k8xC
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
nenechají	nechat	k5eNaPmIp3nP
Teroristy	terorista	k1gMnPc7
bombu	bomba	k1gFnSc4
vůbec	vůbec	k9
položit	položit	k5eAaPmF
<g/>
,	,	kIx,
anebo	anebo	k8xC
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
již	již	k9
položená	položená	k1gFnSc1
<g/>
,	,	kIx,
jejím	její	k3xOp3gNnSc7
zneškodněním	zneškodnění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnPc4
ke	k	k7c3
hraní	hraní	k1gNnSc3
tohoto	tento	k3xDgInSc2
módu	mód	k1gInSc2
<g/>
:	:	kIx,
Dust	Dust	k2eAgMnSc1d1
II	II	kA
<g/>
,	,	kIx,
Mirage	Mirage	k1gFnSc1
<g/>
,	,	kIx,
Train	Train	k1gInSc1
<g/>
,	,	kIx,
Overpass	Overpass	k1gInSc1
<g/>
,	,	kIx,
Nuke	Nuke	k1gNnSc1
<g/>
,	,	kIx,
Inferno	inferno	k1gNnSc1
<g/>
,	,	kIx,
Cobblestone	Cobbleston	k1gInSc5
<g/>
,	,	kIx,
Cache	Cache	k1gFnSc5
<g/>
,	,	kIx,
Vertigo	Vertiga	k1gFnSc5
</s>
<s>
Mise	mise	k1gFnSc1
s	s	k7c7
rukojmími	rukojmí	k1gNnPc7
<g/>
:	:	kIx,
Counter-Teroristé	Counter-Terorista	k1gMnPc1
mají	mít	k5eAaImIp3nP
za	za	k7c4
úkol	úkol	k1gInSc4
osvobodit	osvobodit	k5eAaPmF
rukojmí	rukojmí	k1gMnPc4
držené	držený	k2eAgMnPc4d1
Teroristy	terorista	k1gMnPc4
a	a	k8xC
dostat	dostat	k5eAaPmF
je	on	k3xPp3gMnPc4
na	na	k7c4
místo	místo	k1gNnSc4
vyzvednutí	vyzvednutí	k1gNnSc1
<g/>
;	;	kIx,
Teroristé	terorista	k1gMnPc1
mají	mít	k5eAaImIp3nP
za	za	k7c4
úkol	úkol	k1gInSc4
předejít	předejít	k5eAaPmF
osvobození	osvobození	k1gNnSc4
rukojmích	rukojmí	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
Terorista	terorista	k1gMnSc1
nebo	nebo	k8xC
Counter-Terorista	Counter-Terorista	k1gMnSc1
pokusí	pokusit	k5eAaPmIp3nS
zabít	zabít	k5eAaPmF
rukojmího	rukojmí	k1gMnSc4
<g/>
,	,	kIx,
utrpí	utrpět	k5eAaPmIp3nS
těžkou	těžký	k2eAgFnSc4d1
peněžní	peněžní	k2eAgFnSc4d1
pokutu	pokuta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Update	update	k1gInSc1
později	pozdě	k6eAd2
změnil	změnit	k5eAaPmAgInS
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
jakým	jaký	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
Counter-Teroristé	Counter-Terorista	k1gMnPc1
zachraňují	zachraňovat	k5eAaImIp3nP
rukojmí	rukojmí	k1gMnPc1
<g/>
:	:	kIx,
namísto	namísto	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
rukojmí	rukojmí	k1gMnSc1
na	na	k7c4
místo	místo	k1gNnSc4
vyzvednutí	vyzvednutí	k1gNnSc2
dovedli	dovést	k5eAaPmAgMnP
<g/>
,	,	kIx,
tam	tam	k6eAd1
musí	muset	k5eAaImIp3nS
nyní	nyní	k6eAd1
jednoho	jeden	k4xCgMnSc4
donést	donést	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalo	dát	k5eAaPmAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zde	zde	k6eAd1
prohodily	prohodit	k5eAaPmAgFnP
role	role	k1gFnPc1
–	–	k?
CT	CT	kA
útočí	útočit	k5eAaImIp3nS
a	a	k8xC
naopak	naopak	k6eAd1
T	T	kA
brání	bránit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnPc4
ke	k	k7c3
hraní	hraní	k1gNnSc3
tohoto	tento	k3xDgInSc2
módu	mód	k1gInSc2
<g/>
:	:	kIx,
Assault	Assault	k1gInSc1
<g/>
,	,	kIx,
Italy	Ital	k1gMnPc4
<g/>
,	,	kIx,
Agency	Agency	k1gInPc1
<g/>
,	,	kIx,
Office	Office	kA
<g/>
.	.	kIx.
</s>
<s>
Arms	Arms	k1gInSc1
Race	Race	k1gInSc1
(	(	kIx(
<g/>
Zbrojící	zbrojící	k2eAgInSc1d1
závod	závod	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Každý	každý	k3xTgMnSc1
hráč	hráč	k1gMnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
zabít	zabít	k5eAaPmF
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
protihráčů	protihráč	k1gMnPc2
s	s	k7c7
cílem	cíl	k1gInSc7
získat	získat	k5eAaPmF
16	#num#	k4
lvl	lvl	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
každým	každý	k3xTgInSc7
levlem	levl	k1gInSc7
se	s	k7c7
hráči	hráč	k1gMnPc7
zlepšuje	zlepšovat	k5eAaImIp3nS
zbraň	zbraň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
levlem	levl	k1gInSc7
je	být	k5eAaImIp3nS
zlatý	zlatý	k2eAgInSc4d1
nůž	nůž	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
hráč	hráč	k1gMnSc1
zabije	zabít	k5eAaPmIp3nS
zlatým	zlatý	k2eAgInSc7d1
nožem	nůž	k1gInSc7
protivníka	protivník	k1gMnSc2
<g/>
,	,	kIx,
celý	celý	k2eAgInSc1d1
jeho	jeho	k3xOp3gInSc1
tým	tým	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Demolition	Demolition	k1gInSc1
<g/>
:	:	kIx,
Na	na	k7c6
začátku	začátek	k1gInSc6
hry	hra	k1gFnSc2
týmy	tým	k1gInPc1
dostanou	dostat	k5eAaPmIp3nP
zbraň	zbraň	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
typická	typický	k2eAgFnSc1d1
pro	pro	k7c4
jejich	jejich	k3xOp3gInSc4
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
dostanou	dostat	k5eAaPmIp3nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zabijí	zabít	k5eAaPmIp3nP
protivníka	protivník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mód	mód	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
každý	každý	k3xTgMnSc1
zastřílí	zastřílet	k5eAaPmIp3nS
s	s	k7c7
jinou	jiný	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Deathmatch	Deathmatch	k1gMnSc1
<g/>
:	:	kIx,
Každý	každý	k3xTgMnSc1
hráč	hráč	k1gMnSc1
si	se	k3xPyFc3
zvolí	zvolit	k5eAaPmIp3nS
libovolnou	libovolný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
zabít	zabít	k5eAaPmF
co	co	k9
nejvíce	nejvíce	k6eAd1,k6eAd3
protihráčů	protihráč	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mu	on	k3xPp3gMnSc3
udělí	udělit	k5eAaPmIp3nS
určitý	určitý	k2eAgInSc1d1
počet	počet	k1gInSc1
bodů	bod	k1gInPc2
a	a	k8xC
snaží	snažit	k5eAaImIp3nP
se	se	k3xPyFc4
vyhrát	vyhrát	k5eAaPmF
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
počtem	počet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mód	mód	k1gInSc1
je	být	k5eAaImIp3nS
doplněn	doplnit	k5eAaPmNgInS
respawnem	respawn	k1gInSc7
(	(	kIx(
<g/>
oživením	oživení	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
znovu	znovu	k6eAd1
objeví	objevit	k5eAaPmIp3nS
na	na	k7c6
jiném	jiný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
dané	daný	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Wingman	Wingman	k1gMnSc1
(	(	kIx(
<g/>
Bratr	bratr	k1gMnSc1
ve	v	k7c6
zbrani	zbraň	k1gFnSc6
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Verze	verze	k1gFnSc1
kompetitivního	kompetitivní	k2eAgInSc2d1
módu	mód	k1gInSc2
pro	pro	k7c4
čtyři	čtyři	k4xCgMnPc4
hráče	hráč	k1gMnPc4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
vs	vs	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
pravidel	pravidlo	k1gNnPc2
dovednostních	dovednostní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
je	být	k5eAaImIp3nS
obdobný	obdobný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
u	u	k7c2
kompetitivního	kompetitivní	k2eAgInSc2d1
módu	mód	k1gInSc2
<g/>
,	,	kIx,
hraje	hrát	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
pouze	pouze	k6eAd1
16	#num#	k4
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souboj	souboj	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
klasických	klasický	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
pouze	pouze	k6eAd1
pro	pro	k7c4
mise	mise	k1gFnPc4
s	s	k7c7
bombou	bomba	k1gFnSc7
<g/>
,	,	kIx,
dostupné	dostupný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
však	však	k9
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc1
pokladiště	pokladiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Flying	Flying	k1gInSc1
Scoutsman	Scoutsman	k1gMnSc1
:	:	kIx,
Hráči	hráč	k1gMnPc1
dostanou	dostat	k5eAaPmIp3nP
každé	každý	k3xTgNnSc4
kolo	kolo	k1gNnSc4
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc4
zbraně	zbraň	k1gFnPc4
<g/>
,	,	kIx,
odstřelovací	odstřelovací	k2eAgFnSc4d1
pušku	puška	k1gFnSc4
SSG	SSG	kA
08	#num#	k4
a	a	k8xC
nůž	nůž	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
módu	mód	k1gInSc6
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
snížena	snížen	k2eAgFnSc1d1
gravitace	gravitace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Komunitní	komunitní	k2eAgInPc1d1
servery	server	k1gInPc1
<g/>
:	:	kIx,
PC	PC	kA
verze	verze	k1gFnSc1
hry	hra	k1gFnSc2
umožňuje	umožňovat	k5eAaImIp3nS
hráčům	hráč	k1gMnPc3
zprovoznit	zprovoznit	k5eAaPmF
si	se	k3xPyFc3
vlastní	vlastní	k2eAgInSc4d1
server	server	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
některých	některý	k3yIgInPc6
serverech	server	k1gInPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
klasické	klasický	k2eAgFnPc4d1
herní	herní	k2eAgFnPc4d1
módy	móda	k1gFnPc4
<g/>
,	,	kIx,
především	především	k6eAd1
kompetitivní	kompetitivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
mnoha	mnoho	k4c6
z	z	k7c2
nich	on	k3xPp3gFnPc2
však	však	k9
nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
mnoho	mnoho	k4c4
módů	mód	k1gInPc2
vytvořených	vytvořený	k2eAgInPc2d1
hráči	hráč	k1gMnPc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
nejznámější	známý	k2eAgFnSc7d3
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
JailBreak	JailBreak	k1gMnSc1
<g/>
,	,	kIx,
Trouble	Trouble	k1gMnSc1
in	in	k?
Terrorist	Terrorist	k1gMnSc1
Town	Town	k1gMnSc1
nebo	nebo	k8xC
Surf	surf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Danger	Danger	k1gInSc1
Zone	Zone	k1gInSc1
(	(	kIx(
<g/>
Zóna	zóna	k1gFnSc1
smrti	smrt	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Mód	mód	k1gInSc1
je	být	k5eAaImIp3nS
vytvořen	vytvořit	k5eAaPmNgInS
ve	v	k7c6
stylu	styl	k1gInSc6
Battle	Battle	k1gFnSc2
Royale	Royala	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
hrát	hrát	k5eAaImF
samostatně	samostatně	k6eAd1
nebo	nebo	k8xC
v	v	k7c6
týmu	tým	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
zvolí	zvolit	k5eAaPmIp3nP
svou	svůj	k3xOyFgFnSc4
lokaci	lokace	k1gFnSc4
dopadu	dopad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dopadu	dopad	k1gInSc6
padákem	padák	k1gInSc7
má	mít	k5eAaImIp3nS
tým	tým	k1gInSc1
za	za	k7c4
úkol	úkol	k1gInSc4
zůstat	zůstat	k5eAaPmF
jako	jako	k8xC,k8xS
poslední	poslední	k2eAgInSc4d1
přeživší	přeživší	k2eAgInSc4d1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
využít	využít	k5eAaPmF
jak	jak	k8xC,k8xS
zbraně	zbraň	k1gFnPc1
umístěné	umístěný	k2eAgFnPc1d1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
tablet	tablet	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
všem	všecek	k3xTgMnPc3
hráčům	hráč	k1gMnPc3
umožňuje	umožňovat	k5eAaImIp3nS
nakoupit	nakoupit	k5eAaPmF
zbraně	zbraň	k1gFnPc4
a	a	k8xC
další	další	k2eAgFnPc4d1
utility	utilita	k1gFnPc4
za	za	k7c4
určitý	určitý	k2eAgInSc4d1
obnos	obnos	k1gInSc4
peněz	peníze	k1gInPc2
získaný	získaný	k2eAgInSc4d1
přežíváním	přežívání	k1gNnSc7
<g/>
,	,	kIx,
prohledáváním	prohledávání	k1gNnSc7
mapy	mapa	k1gFnSc2
nebo	nebo	k8xC
zabíjením	zabíjení	k1gNnSc7
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnPc4
určené	určený	k2eAgFnPc1d1
výhradně	výhradně	k6eAd1
pro	pro	k7c4
mód	mód	k1gInSc4
Danger	Danger	k1gInSc1
Zone	Zone	k1gFnSc1
<g/>
:	:	kIx,
Blacksite	Blacksit	k1gInSc5
<g/>
,	,	kIx,
Sirocco	Sirocca	k1gMnSc5
<g/>
,	,	kIx,
Frostbite	Frostbit	k1gMnSc5
(	(	kIx(
<g/>
nejnovější	nový	k2eAgFnSc1d3
mapa	mapa	k1gFnSc1
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s>
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
také	také	k9
nabízí	nabízet	k5eAaImIp3nS
offline	offlin	k1gInSc5
módy	móda	k1gFnSc2
<g/>
:	:	kIx,
Offline	Offlin	k1gInSc5
s	s	k7c7
Boty	bota	k1gFnPc1
a	a	k8xC
Trénink	trénink	k1gInSc1
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
uživatelům	uživatel	k1gMnPc3
vyzkoušet	vyzkoušet	k5eAaPmF
také	také	k9
mapy	mapa	k1gFnPc1
ze	z	k7c2
Steam	Steam	k1gInSc4
Workshopu	workshop	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
může	moct	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
publikovat	publikovat	k5eAaBmF
svůj	svůj	k3xOyFgInSc4
výtvor	výtvor	k1gInSc4
a	a	k8xC
sdílet	sdílet	k5eAaImF
ho	on	k3xPp3gMnSc4
s	s	k7c7
komunitou	komunita	k1gFnSc7
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Skiny	skin	k1gMnPc4
<g/>
,	,	kIx,
Mapy	mapa	k1gFnSc2
a	a	k8xC
pod	pod	k7c7
<g/>
.	.	kIx.
)	)	kIx)
</s>
<s>
Operace	operace	k1gFnSc1
</s>
<s>
Operace	operace	k1gFnPc1
jsou	být	k5eAaImIp3nP
tří	tři	k4xCgFnPc2
až	až	k9
pětiměsíční	pětiměsíční	k2eAgFnPc4d1
akce	akce	k1gFnPc4
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
odemčení	odemčení	k1gNnSc4
všech	všecek	k3xTgFnPc2
částí	část	k1gFnPc2
operace	operace	k1gFnSc2
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
zakoupit	zakoupit	k5eAaPmF
odznak	odznak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakoupením	zakoupení	k1gNnSc7
odznaku	odznak	k1gInSc2
hráči	hráč	k1gMnPc1
dostávají	dostávat	k5eAaImIp3nP
nejen	nejen	k6eAd1
možnost	možnost	k1gFnSc4
zahrát	zahrát	k5eAaPmF
si	se	k3xPyFc3
exkluzivní	exkluzivní	k2eAgFnPc4d1
mapy	mapa	k1gFnPc4
v	v	k7c6
kompetitivním	kompetitivní	k2eAgInSc6d1
módu	mód	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
šanci	šance	k1gFnSc4
na	na	k7c4
získání	získání	k1gNnSc4
dražších	drahý	k2eAgMnPc2d2
skinů	skin	k1gMnPc2
po	po	k7c6
splnění	splnění	k1gNnSc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
misí	mise	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Operace	operace	k1gFnSc1
v	v	k7c6
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
</s>
<s>
Název	název	k1gInSc1
operace	operace	k1gFnSc2
</s>
<s>
Datum	datum	k1gNnSc1
začátku	začátek	k1gInSc2
</s>
<s>
Datum	datum	k1gNnSc1
ukončení	ukončení	k1gNnSc2
<g/>
[	[	kIx(
<g/>
*	*	kIx~
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
*	*	kIx~
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
*	*	kIx~
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
*	*	kIx~
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Exkluzivní	exkluzivní	k2eAgFnPc1d1
mapy	mapa	k1gFnPc1
<g/>
[	[	kIx(
<g/>
*	*	kIx~
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Payback	Payback	k1gInSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2013	#num#	k4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
</s>
<s>
50	#num#	k4
odehraných	odehraný	k2eAgMnPc2d1
hodin	hodina	k1gFnPc2
</s>
<s>
10	#num#	k4
odehraných	odehraný	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
v	v	k7c6
operaci	operace	k1gFnSc6
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
5,99	5,99	k4
€	€	k?
</s>
<s>
Favela	Favela	k1gFnSc1
<g/>
,	,	kIx,
Library	Librara	k1gFnPc1
<g/>
,	,	kIx,
Seaside	Seasid	k1gMnSc5
<g/>
,	,	kIx,
Downtown	Downtown	k1gInSc1
<g/>
,	,	kIx,
Motel	motel	k1gInSc1
<g/>
,	,	kIx,
Museum	museum	k1gNnSc1
<g/>
,	,	kIx,
Thunder	Thunder	k1gInSc1
</s>
<s>
Bravo	bravo	k1gMnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
</s>
<s>
30	#num#	k4
odehraných	odehraný	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
a	a	k8xC
15	#num#	k4
vítězství	vítězství	k1gNnPc2
</s>
<s>
10	#num#	k4
odehraných	odehraný	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
a	a	k8xC
5	#num#	k4
vítězství	vítězství	k1gNnPc2
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
5,99	5,99	k4
€	€	k?
</s>
<s>
Ali	Ali	k?
<g/>
,	,	kIx,
Cache	Cache	k1gInSc1
<g/>
,	,	kIx,
Chinatown	Chinatown	k1gInSc1
<g/>
,	,	kIx,
Cobblestone	Cobbleston	k1gInSc5
<g/>
,	,	kIx,
Gwalior	Gwalior	k1gInSc1
<g/>
,	,	kIx,
Overpass	Overpass	k1gInSc1
<g/>
,	,	kIx,
Ruins	Ruins	k1gInSc1
<g/>
,	,	kIx,
Agency	Agency	k1gInPc1
<g/>
,	,	kIx,
Siege	Sieg	k1gInPc1
</s>
<s>
Phoenix	Phoenix	k1gInSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
</s>
<s>
30	#num#	k4
odehraných	odehraný	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
a	a	k8xC
15	#num#	k4
vítězství	vítězství	k1gNnPc2
</s>
<s>
10	#num#	k4
odehraných	odehraný	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
a	a	k8xC
5	#num#	k4
vítězství	vítězství	k1gNnPc2
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
2,99	2,99	k4
€	€	k?
</s>
<s>
Ali	Ali	k?
<g/>
,	,	kIx,
Cache	Cach	k1gMnSc4
<g/>
,	,	kIx,
Favela	Favel	k1gMnSc4
<g/>
,	,	kIx,
Seaside	Seasid	k1gMnSc5
<g/>
,	,	kIx,
Agency	Agency	k1gInPc1
<g/>
,	,	kIx,
Downtown	Downtown	k1gNnSc1
<g/>
,	,	kIx,
Motel	motel	k1gInSc1
<g/>
,	,	kIx,
Thunder	Thunder	k1gInSc1
</s>
<s>
Breakout	Breakout	k1gMnSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
</s>
<s>
15	#num#	k4
dokončených	dokončený	k2eAgMnPc2d1
misí	mise	k1gFnPc2
</s>
<s>
5	#num#	k4
dokončených	dokončený	k2eAgMnPc2d1
misí	mise	k1gFnPc2
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
4,99	4,99	k4
€	€	k?
</s>
<s>
Black	Black	k1gMnSc1
Gold	Gold	k1gMnSc1
<g/>
,	,	kIx,
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
Mist	Mist	k1gInSc1
<g/>
,	,	kIx,
Overgrown	Overgrown	k1gInSc1
<g/>
,	,	kIx,
Insertion	Insertion	k1gInSc1
<g/>
,	,	kIx,
Rush	Rush	k1gInSc1
</s>
<s>
Vanguard	Vanguard	k1gInSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
</s>
<s>
Získání	získání	k1gNnSc1
4	#num#	k4
hvězd	hvězda	k1gFnPc2
</s>
<s>
Získání	získání	k1gNnSc1
3	#num#	k4
hvězd	hvězda	k1gFnPc2
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
2,59	2,59	k4
–	–	k?
5,19	5,19	k4
€	€	k?
</s>
<s>
Bazaar	Bazaar	k1gMnSc1
<g/>
,	,	kIx,
Facade	Facad	k1gInSc5
<g/>
,	,	kIx,
Marquis	Marquis	k1gFnPc2
<g/>
,	,	kIx,
Season	Seasona	k1gFnPc2
<g/>
,	,	kIx,
Train	Traina	k1gFnPc2
<g/>
,	,	kIx,
Backalley	Backallea	k1gFnSc2
<g/>
,	,	kIx,
Workout	Workout	k1gMnSc1
</s>
<s>
Bloodhound	Bloodhound	k1gInSc1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
</s>
<s>
Získání	získání	k1gNnSc1
14	#num#	k4
hvězd	hvězda	k1gFnPc2
</s>
<s>
Získání	získání	k1gNnSc1
9	#num#	k4
hvězd	hvězda	k1gFnPc2
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
5,49	5,49	k4
€	€	k?
</s>
<s>
Crashsite	Crashsit	k1gInSc5
(	(	kIx(
<g/>
2	#num#	k4
verze	verze	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Log	log	k1gInSc1
<g/>
,	,	kIx,
Rails	Rails	k1gInSc1
<g/>
,	,	kIx,
Resort	resort	k1gInSc1
<g/>
,	,	kIx,
Season	Season	k1gInSc1
<g/>
,	,	kIx,
ZOO	zoo	k1gNnPc1
<g/>
,	,	kIx,
Agency	Agency	k1gInPc1
</s>
<s>
Wildfire	Wildfir	k1gMnSc5
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
</s>
<s>
Získání	získání	k1gNnSc1
14	#num#	k4
hvězd	hvězda	k1gFnPc2
</s>
<s>
Získání	získání	k1gNnSc1
9	#num#	k4
hvězd	hvězda	k1gFnPc2
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
5,29	5,29	k4
€	€	k?
</s>
<s>
Coast	Coast	k1gFnSc1
<g/>
,	,	kIx,
Empire	empir	k1gInSc5
<g/>
,	,	kIx,
Mikla	miknout	k5eAaPmAgFnS
<g/>
,	,	kIx,
Nuke	Nuke	k1gFnSc1
<g/>
,	,	kIx,
Royal	Royal	k1gMnSc1
<g/>
,	,	kIx,
Santorini	Santorin	k1gMnPc1
<g/>
,	,	kIx,
Tulip	Tulip	k1gMnSc1
<g/>
,	,	kIx,
Cruise	Cruise	k1gFnSc1
<g/>
,	,	kIx,
Phoenix	Phoenix	k1gInSc1
Compound	Compounda	k1gFnPc2
</s>
<s>
Hydra	hydra	k1gFnSc1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
</s>
<s>
Získání	získání	k1gNnSc1
18	#num#	k4
<g/>
/	/	kIx~
<g/>
25	#num#	k4
hvězd	hvězda	k1gFnPc2
<g/>
[	[	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Získání	získání	k1gNnSc1
5	#num#	k4
hvězd	hvězda	k1gFnPc2
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
5,29	5,29	k4
€	€	k?
</s>
<s>
Austria	Austrium	k1gNnPc1
<g/>
,	,	kIx,
Black	Black	k1gMnSc1
Gold	Gold	k1gMnSc1
<g/>
,	,	kIx,
Lite	lit	k1gInSc5
<g/>
,	,	kIx,
Shipped	Shipped	k1gInSc1
<g/>
,	,	kIx,
Thrill	Thrill	k1gInSc1
<g/>
,	,	kIx,
Agency	Agency	k1gInPc1
<g/>
,	,	kIx,
Insertion	Insertion	k1gInSc1
<g/>
,	,	kIx,
Dizzy	Dizz	k1gInPc1
<g/>
,	,	kIx,
Rialto	Rialto	k1gNnSc1
</s>
<s>
Shatteredweb	Shatteredwba	k1gFnPc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2019	#num#	k4
</s>
<s>
Březen	březen	k1gInSc1
2020	#num#	k4
</s>
<s>
66	#num#	k4
dokončených	dokončený	k2eAgMnPc2d1
misí	mise	k1gFnPc2
</s>
<s>
33	#num#	k4
dokončených	dokončený	k2eAgMnPc2d1
misí	mise	k1gFnPc2
</s>
<s>
Zakoupení	zakoupení	k1gNnSc1
odznaku	odznak	k1gInSc2
za	za	k7c4
13,25	13,25	k4
€	€	k?
</s>
<s>
Studio	studio	k1gNnSc1
<g/>
,	,	kIx,
Lunacy	Lunacy	k1gInPc1
<g/>
,	,	kIx,
Jungle	Jungle	k1gFnPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
↑	↑	k?
V	v	k7c6
tabulce	tabulka	k1gFnSc6
jsou	být	k5eAaImIp3nP
uvedena	uveden	k2eAgNnPc1d1
data	datum	k1gNnPc1
skutečného	skutečný	k2eAgNnSc2d1
ukončení	ukončení	k1gNnSc2
operace	operace	k1gFnSc2
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
byly	být	k5eAaImAgFnP
pro	pro	k7c4
velký	velký	k2eAgInSc4d1
zájem	zájem	k1gInSc4
prodlouženy	prodloužit	k5eAaPmNgFnP
<g/>
.1	.1	k4
2	#num#	k4
Odehrané	odehraný	k2eAgFnSc2d1
hodiny	hodina	k1gFnSc2
se	se	k3xPyFc4
počítají	počítat	k5eAaImIp3nP
pouze	pouze	k6eAd1
na	na	k7c6
mapách	mapa	k1gFnPc6
operace	operace	k1gFnSc2
<g/>
,	,	kIx,
vítězství	vítězství	k1gNnSc1
pouze	pouze	k6eAd1
v	v	k7c6
kompetitivním	kompetitivní	k2eAgInSc6d1
módu	mód	k1gInSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
V	v	k7c6
tabulce	tabulka	k1gFnSc6
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgFnP
pouze	pouze	k6eAd1
orientační	orientační	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgInPc6d1
regionech	region	k1gInPc6
a	a	k8xC
datech	datum	k1gNnPc6
mohly	moct	k5eAaImAgFnP
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mapy	mapa	k1gFnSc2
označené	označený	k2eAgFnSc2d1
kurzívou	kurzíva	k1gFnSc7
jsou	být	k5eAaImIp3nP
určeny	určit	k5eAaPmNgInP
pro	pro	k7c4
mód	mód	k1gInSc4
hry	hra	k1gFnSc2
s	s	k7c7
rukojmími	rukojmí	k1gMnPc7
<g/>
,	,	kIx,
mapy	mapa	k1gFnPc4
vyznačené	vyznačený	k2eAgFnPc4d1
tučně	tučně	k6eAd1
si	se	k3xPyFc3
hráči	hráč	k1gMnPc1
mohou	moct	k5eAaImIp3nP
zahrát	zahrát	k5eAaPmF
na	na	k7c6
oficiálních	oficiální	k2eAgInPc6d1
serverech	server	k1gInPc6
do	do	k7c2
dnešního	dnešní	k2eAgInSc2d1
dne	den	k1gInSc2
(	(	kIx(
<g/>
mapy	mapa	k1gFnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
aktivní	aktivní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
nebo	nebo	k8xC
jiné	jiný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
podtržené	podtržený	k2eAgFnPc1d1
mapy	mapa	k1gFnPc1
jsou	být	k5eAaImIp3nP
určeny	určit	k5eAaPmNgFnP
pro	pro	k7c4
speciální	speciální	k2eAgInSc4d1
herní	herní	k2eAgInSc4d1
mód	mód	k1gInSc4
(	(	kIx(
<g/>
Rialto	Rialto	k1gNnSc1
pro	pro	k7c4
mód	mód	k1gInSc4
Bratr	bratr	k1gMnSc1
ve	v	k7c6
Zbrani	zbraň	k1gFnSc6
<g/>
,	,	kIx,
Dizzy	Dizz	k1gInPc4
pro	pro	k7c4
mód	mód	k1gInSc4
Flying	Flying	k1gInSc4
Scoutsman	Scoutsman	k1gMnSc1
a	a	k8xC
Phoenix	Phoenix	k1gInSc1
Compound	Compounda	k1gFnPc2
pro	pro	k7c4
ko-operační	ko-operační	k2eAgFnPc4d1
mise	mise	k1gFnPc4
v	v	k7c6
operaci	operace	k1gFnSc6
Wildfire	Wildfir	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnSc2
bez	bez	k7c2
formátování	formátování	k1gNnSc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
vyznačené	vyznačený	k2eAgFnPc1d1
tučně	tučně	k6eAd1
<g/>
)	)	kIx)
používají	používat	k5eAaImIp3nP
mód	mód	k1gInSc4
hry	hra	k1gFnSc2
s	s	k7c7
bombou	bomba	k1gFnSc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
V	v	k7c6
Operaci	operace	k1gFnSc6
Hydra	hydra	k1gFnSc1
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
jedinečnou	jedinečný	k2eAgFnSc4d1
<g/>
,	,	kIx,
diamantovou	diamantový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
odznaku	odznak	k1gInSc2
operace	operace	k1gFnSc2
po	po	k7c6
získání	získání	k1gNnSc6
25	#num#	k4
hvězd	hvězda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Progaming	Progaming	k1gInSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
esport	esport	k1gInSc1
</s>
<s>
Profesionální	profesionální	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
soupeří	soupeřit	k5eAaImIp3nP
v	v	k7c6
týmech	tým	k1gInPc6
o	o	k7c4
5	#num#	k4
hráčích	hráč	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejlepší	dobrý	k2eAgMnPc4d3
hráče	hráč	k1gMnPc4
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
Christopher	Christophra	k1gFnPc2
"	"	kIx"
<g/>
GeT_RiGhT	GeT_RiGhT	k1gMnSc1
<g/>
"	"	kIx"
Alesund	Alesund	k1gMnSc1
<g/>
,	,	kIx,
Gabriel	Gabriel	k1gMnSc1
"	"	kIx"
<g/>
FalleN	FalleN	k1gFnSc1
<g/>
"	"	kIx"
Toledo	Toledo	k1gNnSc1
<g/>
,	,	kIx,
Nikola	Nikola	k1gFnSc1
"	"	kIx"
<g/>
NiKo	nika	k1gFnSc5
<g/>
"	"	kIx"
Kovač	Kovač	k1gInSc1
,	,	kIx,
Aleksandr	Aleksandr	k1gInSc1
"	"	kIx"
<g/>
s	s	k7c7
<g/>
1	#num#	k4
<g/>
mple	mple	k1gNnPc7
Kostyliev	Kostyliva	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgMnPc1d3
českoslovenští	československý	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
jsou	být	k5eAaImIp3nP
Tomáš	Tomáš	k1gMnSc1
"	"	kIx"
<g/>
oskar	oskar	k1gMnSc1
<g/>
"	"	kIx"
Šťastný	Šťastný	k1gMnSc1
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
"	"	kIx"
<g/>
AFU	AFU	kA
<g/>
"	"	kIx"
Kory	Kora	k1gFnSc2
,	,	kIx,
David	David	k1gMnSc1
"	"	kIx"
<g/>
frozen	frozen	k2eAgMnSc1d1
<g/>
"	"	kIx"
Čerňanský	Čerňanský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
"	"	kIx"
<g/>
GuardiaN	GuardiaN	k1gMnSc1
<g/>
"	"	kIx"
Kovács	Kovács	k1gInSc1
<g/>
,	,	kIx,
<g/>
Nikolas	Nikolas	k1gMnSc1
"	"	kIx"
<g/>
TECHNO	TECHNO	kA
<g/>
"	"	kIx"
Kešner	Kešner	k1gMnSc1
</s>
<s>
Významné	významný	k2eAgInPc1d1
týmy	tým	k1gInPc1
</s>
<s>
světová	světový	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
<g/>
:	:	kIx,
fnatic	fnatice	k1gFnPc2
<g/>
,	,	kIx,
FaZe	FaZe	k1gFnSc1
Clan	Clan	k1gMnSc1
<g/>
,	,	kIx,
Ninjas	Ninjas	k1gMnSc1
in	in	k?
Pyjamas	Pyjamas	k1gMnSc1
<g/>
,	,	kIx,
G2	G2	k1gMnSc1
eSports	eSports	k1gInSc1
<g/>
,	,	kIx,
Team	team	k1gInSc1
EnVyUs	EnVyUs	k1gInSc1
<g/>
,	,	kIx,
Natus	Natus	k1gInSc1
Vincere	Vincer	k1gInSc5
<g/>
,	,	kIx,
Made	Mad	k1gFnPc1
in	in	k?
Brazil	Brazil	k1gFnSc1
<g/>
,	,	kIx,
Cloud	Cloud	k1gInSc1
<g/>
9	#num#	k4
<g/>
,	,	kIx,
Luminosity	Luminosita	k1gFnPc1
Gaming	Gaming	k1gInSc1
<g/>
,	,	kIx,
Astralis	Astralis	k1gInSc1
<g/>
,	,	kIx,
North	North	k1gInSc1
<g/>
,	,	kIx,
<g/>
Gambit	gambit	k1gInSc1
Gaming	Gaming	k1gInSc1
<g/>
,	,	kIx,
Team	team	k1gInSc1
Kinguin	Kinguin	k1gInSc1
<g/>
,	,	kIx,
Virtus	Virtus	k1gInSc1
<g/>
.	.	kIx.
<g/>
pro	pro	k7c4
<g/>
,	,	kIx,
mousesports	mousesports	k6eAd1
<g/>
,	,	kIx,
Hellraisers	Hellraisers	k1gInSc1
<g/>
,	,	kIx,
Team	team	k1gInSc1
Liquid	Liquid	k1gInSc1
<g/>
,	,	kIx,
Sprout	Sprout	k1gMnSc1
<g/>
,	,	kIx,
Vega	Vega	k1gMnSc1
Squadron	Squadron	k1gMnSc1
<g/>
,	,	kIx,
Winstrike	Winstrike	k1gFnSc1
<g/>
,	,	kIx,
Avangar	Avangar	k1gMnSc1
<g/>
,	,	kIx,
Tyloo	Tyloo	k1gMnSc1
<g/>
,	,	kIx,
ENCE	ENCE	kA
</s>
<s>
československá	československý	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
<g/>
:	:	kIx,
eSuba	eSuba	k1gFnSc1
<g/>
,	,	kIx,
Neophyte	Neophyt	k1gInSc5
<g/>
,	,	kIx,
Majestic	Majestice	k1gFnPc2
Lions	Lionsa	k1gFnPc2
<g/>
,	,	kIx,
eXtatus	eXtatus	k1gMnSc1
<g/>
,	,	kIx,
Entropiq	Entropiq	k1gMnSc1
</s>
<s>
Významné	významný	k2eAgInPc1d1
turnaje	turnaj	k1gInPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Premier	Premira	k1gFnPc2
</s>
<s>
ELEAGUE	ELEAGUE	kA
–	–	k?
S1	S1	k1gMnSc1
•	•	k?
S2	S2	k1gMnSc1
•	•	k?
Major	major	k1gMnSc1
2017	#num#	k4
•	•	k?
Major	major	k1gMnSc1
2018	#num#	k4
</s>
<s>
ESL	ESL	kA
One	One	k1gFnSc1
</s>
<s>
Cologne	Colognout	k5eAaImIp3nS,k5eAaPmIp3nS
(	(	kIx(
<g/>
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
New	New	k?
York	York	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Katowice	Katowice	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ESL	ESL	kA
Pro	pro	k7c4
League	Leagu	k1gFnPc4
–	–	k?
S1	S1	k1gMnSc1
•	•	k?
S2	S2	k1gMnSc1
•	•	k?
S3	S3	k1gMnSc1
•	•	k?
S4	S4	k1gMnSc1
•	•	k?
S5	S5	k1gMnSc1
</s>
<s>
Swiss	Swiss	k6eAd1
System	Syst	k1gInSc7
</s>
<s>
Švýcarský	švýcarský	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
uváděno	uvádět	k5eAaImNgNnS
v	v	k7c6
anglické	anglický	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
jako	jako	k8xC,k8xS
Swiss	Swiss	k1gInSc4
System	Syst	k1gInSc7
je	být	k5eAaImIp3nS
systém	systém	k1gInSc1
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
využívaný	využívaný	k2eAgInSc4d1
na	na	k7c6
turnajích	turnaj	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
verze	verze	k1gFnSc1
pro	pro	k7c4
16	#num#	k4
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
týmů	tým	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Šestnáct	šestnáct	k4xCc1
týmů	tým	k1gInPc2
je	být	k5eAaImIp3nS
rozlosováno	rozlosovat	k5eAaPmNgNnS
do	do	k7c2
8	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
zápasů	zápas	k1gInPc2
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
vyšší	vysoký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
</s>
<s>
Poražení	poražený	k1gMnPc1
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
nižší	nízký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
skupina	skupina	k1gFnSc1
</s>
<s>
Osm	osm	k4xCc1
vítězů	vítěz	k1gMnPc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
je	být	k5eAaImIp3nS
rozlosováno	rozlosovat	k5eAaPmNgNnS
do	do	k7c2
4	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
vyšší	vysoký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poražení	poražení	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
střední	střední	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
skupina	skupina	k1gFnSc1
</s>
<s>
Osm	osm	k4xCc1
poražených	poražený	k1gMnPc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
je	být	k5eAaImIp3nS
rozlosováno	rozlosovat	k5eAaPmNgNnS
do	do	k7c2
4	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
střední	střední	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poražení	poražení	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
nižší	nízký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
skupina	skupina	k1gFnSc1
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1
vítězové	vítěz	k1gMnPc1
z	z	k7c2
vyšší	vysoký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
se	se	k3xPyFc4
rozlosují	rozlosovat	k5eAaPmIp3nP
do	do	k7c2
2	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
play-off	play-off	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Poražení	poražení	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
vyšší	vysoký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Střední	střední	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1
poražení	poražený	k1gMnPc1
z	z	k7c2
vyšší	vysoký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
a	a	k8xC
čtyři	čtyři	k4xCgMnPc1
vítězové	vítěz	k1gMnPc1
z	z	k7c2
nižší	nízký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
se	se	k3xPyFc4
rozlosují	rozlosovat	k5eAaPmIp3nP
do	do	k7c2
4	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
vyšší	vysoký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poražení	poražení	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
nižší	nízký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
skupina	skupina	k1gFnSc1
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1
poražení	poražený	k1gMnPc1
z	z	k7c2
nižší	nízký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
jsou	být	k5eAaImIp3nP
rozlosováni	rozlosován	k2eAgMnPc1d1
do	do	k7c2
2	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
nižší	nízký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poražení	poražení	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vyřazeni	vyřadit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
skupina	skupina	k1gFnSc1
</s>
<s>
Dva	dva	k4xCgMnPc1
poražení	poražený	k1gMnPc1
z	z	k7c2
vyšší	vysoký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
a	a	k8xC
čtyři	čtyři	k4xCgMnPc1
vítězové	vítěz	k1gMnPc1
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
jsou	být	k5eAaImIp3nP
rozlosováni	rozlosován	k2eAgMnPc1d1
do	do	k7c2
6	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
play-off	play-off	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Poražení	poražení	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
5	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
skupina	skupina	k1gFnSc1
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1
poražení	poražený	k1gMnPc1
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
a	a	k8xC
dva	dva	k4xCgMnPc1
vítězové	vítěz	k1gMnPc1
z	z	k7c2
nižší	nízký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
jsou	být	k5eAaImIp3nP
rozlosováni	rozlosován	k2eAgMnPc1d1
do	do	k7c2
6	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
5	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Poražení	poražení	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vyřazeni	vyřadit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
Tři	tři	k4xCgMnPc1
poražení	poražený	k1gMnPc1
z	z	k7c2
vyšší	vysoký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
a	a	k8xC
tři	tři	k4xCgMnPc1
vítězové	vítěz	k1gMnPc1
z	z	k7c2
nižší	nízký	k2eAgFnSc2d2
skupiny	skupina	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
jsou	být	k5eAaImIp3nP
rozlosování	rozlosování	k1gNnSc4
do	do	k7c2
3	#num#	k4
zápasů	zápas	k1gInPc2
</s>
<s>
Vítězové	vítěz	k1gMnPc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
play-off	play-off	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Poražení	poražení	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
celkovým	celkový	k2eAgNnSc7d1
skóre	skóre	k1gNnSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vyřazeni	vyřadit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
Major	major	k1gMnSc1
</s>
<s>
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
Major	major	k1gMnSc1
Championship	Championship	k1gMnSc1
(	(	kIx(
<g/>
fanoušky	fanoušek	k1gMnPc7
zkracováno	zkracován	k2eAgNnSc1d1
na	na	k7c4
"	"	kIx"
<g/>
Majors	Majors	k1gInSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
turnaje	turnaj	k1gInPc4
sponzorované	sponzorovaný	k2eAgFnSc2d1
VALVE	VALVE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
Majorem	major	k1gMnSc7
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
DreamHack	DreamHack	k1gMnSc1
Winter	Winter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Turnaje	turnaj	k1gInPc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Místo	místo	k7c2
konání	konání	k1gNnSc2
</s>
<s>
Od	od	k7c2
</s>
<s>
Do	do	k7c2
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
DreamHack	DreamHack	k1gMnSc1
Winter	Winter	k1gMnSc1
2013	#num#	k4
Jönköping	Jönköping	k1gInSc1
<g/>
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
201330	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
<g/>
FnaticNinjas	FnaticNinjas	k1gMnSc1
in	in	k?
PyjamascompLexityVeryGames	PyjamascompLexityVeryGames	k1gMnSc1
</s>
<s>
EMS	EMS	kA
One	One	k1gFnSc1
<g/>
:	:	kIx,
Katowice	Katowice	k1gFnSc1
2014	#num#	k4
Katovice	Katovice	k1gFnPc4
<g/>
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
201416	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
<g/>
Virtus	Virtus	k1gInSc1
<g/>
.	.	kIx.
<g/>
proNinjas	proNinjas	k1gInSc1
in	in	k?
PyjamasTeam	PyjamasTeam	k1gInSc1
DignitasLGB	DignitasLGB	k1gFnPc2
eSports	eSports	k6eAd1
</s>
<s>
ESL	ESL	kA
One	One	k1gFnSc1
<g/>
:	:	kIx,
Cologne	Cologn	k1gInSc5
2014	#num#	k4
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
14	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
201417	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
<g/>
Ninjas	Ninjas	k1gMnSc1
in	in	k?
PyjamasFnaticTeam	PyjamasFnaticTeam	k1gInSc1
DignitasTeam	DignitasTeam	k1gInSc1
LDLC	LDLC	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
DreamHack	DreamHack	k1gMnSc1
Winter	Winter	k1gMnSc1
2014	#num#	k4
Jönköping	Jönköping	k1gInSc1
<g/>
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
201429	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
<g/>
Team	team	k1gInSc1
LDLC	LDLC	kA
<g/>
.	.	kIx.
<g/>
comNinjas	comNinjas	k1gMnSc1
in	in	k?
PyjamasVirtus	PyjamasVirtus	k1gMnSc1
<g/>
.	.	kIx.
<g/>
proNatus	proNatus	k1gMnSc1
Vincere	Vincer	k1gInSc5
</s>
<s>
ESL	ESL	kA
One	One	k1gFnSc1
<g/>
:	:	kIx,
Katowice	Katowice	k1gFnSc1
2015	#num#	k4
Katovice	Katovice	k1gFnPc4
<g/>
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
201515	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
<g/>
FnaticNinjas	FnaticNinjas	k1gMnSc1
in	in	k?
PyjamasVirtus	PyjamasVirtus	k1gMnSc1
<g/>
.	.	kIx.
<g/>
proTeam	proTeam	k1gInSc1
EnVyUs	EnVyUsa	k1gFnPc2
</s>
<s>
ESL	ESL	kA
One	One	k1gFnSc1
<g/>
:	:	kIx,
Cologne	Cologn	k1gInSc5
2015	#num#	k4
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
201523	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2015	#num#	k4
<g/>
FnaticTeam	FnaticTeam	k1gInSc1
EnVyUsTeam	EnVyUsTeam	k1gInSc4
SoloMidVirtus	SoloMidVirtus	k1gInSc4
<g/>
.	.	kIx.
<g/>
pro	pro	k7c4
</s>
<s>
DreamHack	DreamHack	k1gInSc1
Open	Open	k1gInSc1
Cluj-Napoca	Cluj-Napoca	k1gFnSc1
2015	#num#	k4
Kluž	Kluž	k1gFnSc1
<g/>
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
20151	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
<g/>
Team	team	k1gInSc1
EnVyUsNatus	EnVyUsNatus	k1gInSc4
VincereG	VincereG	k1gFnSc2
<g/>
2	#num#	k4
EsportsNinjas	EsportsNinjas	k1gMnSc1
in	in	k?
Pyjamas	Pyjamas	k1gMnSc1
</s>
<s>
MLG	MLG	kA
Columbus	Columbus	k1gMnSc1
2016	#num#	k4
Columbus	Columbus	k1gInSc1
<g/>
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
20163	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
LuminosityNatus	LuminosityNatus	k1gInSc4
VincereAstralisTeam	VincereAstralisTeam	k1gInSc4
Liquid	Liquida	k1gFnPc2
</s>
<s>
ESL	ESL	kA
One	One	k1gFnSc1
<g/>
:	:	kIx,
Cologne	Cologn	k1gInSc5
2016	#num#	k4
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
201610	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016SK	2016SK	k4
GamingTeam	GamingTeam	k1gInSc1
LiquidVirtus	LiquidVirtus	k1gInSc4
<g/>
.	.	kIx.
<g/>
proFnatic	proFnatice	k1gFnPc2
</s>
<s>
ELEAGUE	ELEAGUE	kA
Major	major	k1gMnSc1
2017	#num#	k4
Atlanta	Atlanta	k1gFnSc1
<g/>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
201729	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
AstralisVirtus	AstralisVirtus	k1gInSc1
<g/>
.	.	kIx.
<g/>
proFnaticSK	proFnaticSK	k?
Gaming	Gaming	k1gInSc1
</s>
<s>
PGL	PGL	kA
Major	major	k1gMnSc1
Kraków	Kraków	k1gMnSc1
2017	#num#	k4
Krakov	Krakov	k1gInSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
201723	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
<g/>
Gambit	gambit	k1gInSc1
EsportsImmortalsAstralisVirtus	EsportsImmortalsAstralisVirtus	k1gInSc4
<g/>
.	.	kIx.
<g/>
pro	pro	k7c4
</s>
<s>
ELEAGUE	ELEAGUE	kA
Major	major	k1gMnSc1
2018	#num#	k4
Boston	Boston	k1gInSc1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
201828	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
Cloud	Cloud	k1gInSc1
<g/>
9	#num#	k4
<g/>
Faze	Faz	k1gInSc2
ClanNatus	ClanNatus	k1gMnSc1
VincereSK	VincereSK	k1gMnSc1
Gaming	Gaming	k1gInSc4
</s>
<s>
FACEIT	FACEIT	kA
Major	major	k1gMnSc1
2018	#num#	k4
Londýn	Londýn	k1gInSc1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
201823	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2018	#num#	k4
<g/>
AstralisNatus	AstralisNatus	k1gInSc1
VincereTeam	VincereTeam	k1gInSc4
LiquidMIBR	LiquidMIBR	k1gFnSc2
</s>
<s>
IEM	IEM	kA
Katowice	Katowice	k1gFnSc1
2019	#num#	k4
Katovice	Katovice	k1gFnPc4
<g/>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
20193	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
<g/>
AstralisENCENatus	AstralisENCENatus	k1gMnSc1
VincereMIBR	VincereMIBR	k1gMnSc1
</s>
<s>
StarLadder	StarLadder	k1gMnSc1
Berlin	berlina	k1gFnPc2
Major	major	k1gMnSc1
2019	#num#	k4
</s>
<s>
Berlín	Berlín	k1gInSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
</s>
<s>
Astralis	Astralis	k1gFnSc1
</s>
<s>
AVANGAR	AVANGAR	kA
</s>
<s>
Renegades	Renegades	k1gMnSc1
</s>
<s>
NRG	NRG	kA
</s>
<s>
ESL	ESL	kA
One	One	k1gMnSc1
Rio	Rio	k1gMnSc1
Major	major	k1gMnSc1
2020	#num#	k4
</s>
<s>
Rio	Rio	k?
de	de	k?
Janeiro	Janeiro	k1gNnSc4
</s>
<s>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
PGL	PGL	kA
Major	major	k1gMnSc1
Stockholm	Stockholm	k1gInSc4
2021	#num#	k4
</s>
<s>
Stockholm	Stockholm	k1gInSc1
</s>
<s>
plánováno	plánován	k2eAgNnSc4d1
říjen	říjen	k2eAgInSc4d1
<g/>
/	/	kIx~
<g/>
listopad	listopad	k1gInSc4
2021	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
Týmy	tým	k1gInPc1
</s>
<s>
Legendy	legenda	k1gFnPc4
–	–	k?
8	#num#	k4
nejlepších	dobrý	k2eAgInPc2d3
týmů	tým	k1gInPc2
posledního	poslední	k2eAgNnSc2d1
Majoru	major	k1gMnSc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
automaticky	automaticky	k6eAd1
kvalifikovány	kvalifikovat	k5eAaBmNgFnP
na	na	k7c4
další	další	k2eAgFnPc4d1
Major	major	k1gMnSc1
</s>
<s>
Vyzyvatelé	Vyzyvatelé	k?
–	–	k?
8	#num#	k4
nejlepších	dobrý	k2eAgInPc2d3
týmů	tým	k1gInPc2
v	v	k7c6
kvalifikaci	kvalifikace	k1gFnSc6
na	na	k7c4
Major	major	k1gMnSc1
</s>
<s>
Samolepky	samolepka	k1gFnPc1
a	a	k8xC
"	"	kIx"
<g/>
Pick	Pick	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
<g/>
"	"	kIx"
</s>
<s>
Samolepky	samolepka	k1gFnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
novějších	nový	k2eAgInPc2d2
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
Majorů	major	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
samolepky	samolepka	k1gFnPc4
s	s	k7c7
logy	log	k1gInPc7
týmů	tým	k1gInPc2
a	a	k8xC
podpisy	podpis	k1gInPc1
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
samolepky	samolepka	k1gFnPc4
lze	lze	k6eAd1
lepit	lepit	k5eAaImF
na	na	k7c4
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samolepky	samolepka	k1gFnPc1
mají	mít	k5eAaImIp3nP
4	#num#	k4
typy	typ	k1gInPc7
–	–	k?
základní	základní	k2eAgInPc1d1
(	(	kIx(
<g/>
cena	cena	k1gFnSc1
na	na	k7c6
Steamu	Steam	k1gInSc6
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
řádově	řádově	k6eAd1
kolem	kolem	k7c2
desítek	desítka	k1gFnPc2
centů	cent	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
holografické	holografický	k2eAgInPc1d1
<g/>
,	,	kIx,
speciální	speciální	k2eAgInPc1d1
a	a	k8xC
pozlacené	pozlacený	k2eAgInPc1d1
(	(	kIx(
<g/>
cena	cena	k1gFnSc1
na	na	k7c6
Steamu	Steam	k1gInSc6
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
řádově	řádově	k6eAd1
kolem	kolem	k7c2
několika	několik	k4yIc2
eur	euro	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
peněz	peníze	k1gInPc2
z	z	k7c2
prodeje	prodej	k1gInSc2
jde	jít	k5eAaImIp3nS
hráčům	hráč	k1gMnPc3
a	a	k8xC
týmům	tým	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Pick	Pick	k1gInSc1
<g/>
'	'	kIx"
<g/>
em	em	k?
je	být	k5eAaImIp3nS
minihra	minihra	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
nakoupili	nakoupit	k5eAaPmAgMnP
samolepky	samolepka	k1gFnPc4
svých	svůj	k3xOyFgMnPc2
favoritů	favorit	k1gMnPc2
<g/>
,	,	kIx,
sází	sázet	k5eAaImIp3nP
tyto	tento	k3xDgInPc4
nálepky	nálepka	k1gFnPc1
na	na	k7c4
výsledky	výsledek	k1gInPc4
týmů	tým	k1gInPc2
v	v	k7c6
tabulce	tabulka	k1gFnSc6
a	a	k8xC
play-off	play-off	k1gInSc4
části	část	k1gFnSc2
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgMnPc6
majorech	major	k1gMnPc6
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
tipovat	tipovat	k5eAaImF
i	i	k9
nejlepší	dobrý	k2eAgMnPc4d3
hráče	hráč	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
za	za	k7c4
odměnu	odměna	k1gFnSc4
dostanou	dostat	k5eAaPmIp3nP
zlatý	zlatý	k2eAgInSc4d1
<g/>
,	,	kIx,
stříbrný	stříbrný	k2eAgInSc4d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
bronzový	bronzový	k2eAgInSc4d1
odznak	odznak	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
jinak	jinak	k6eAd1
než	než	k8xS
účastí	účast	k1gFnSc7
v	v	k7c6
této	tento	k3xDgFnSc6
minihře	minihra	k1gFnSc6
získat	získat	k5eAaPmF
nelze	lze	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samolepky	samolepka	k1gFnPc1
jsou	být	k5eAaImIp3nP
po	po	k7c4
dobu	doba	k1gFnSc4
turnaje	turnaj	k1gInSc2
uzamčeny	uzamknout	k5eAaPmNgInP
a	a	k8xC
nelze	lze	k6eNd1
je	být	k5eAaImIp3nS
použít	použít	k5eAaPmF
<g/>
,	,	kIx,
vyměnovat	vyměnovat	k5eAaBmF,k5eAaPmF,k5eAaImF
ani	ani	k8xC
s	s	k7c7
nimi	on	k3xPp3gFnPc7
obchodovat	obchodovat	k5eAaImF
<g/>
,	,	kIx,
po	po	k7c6
skončení	skončení	k1gNnSc6
turnaje	turnaj	k1gInSc2
se	se	k3xPyFc4
však	však	k9
odemknou	odemknout	k5eAaPmIp3nP
a	a	k8xC
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
použít	použít	k5eAaPmF
<g/>
,	,	kIx,
vyměnit	vyměnit	k5eAaPmF
<g/>
,	,	kIx,
nebo	nebo	k8xC
prodat	prodat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Major	major	k1gMnSc1
IEM	IEM	kA
Katowice	Katowice	k1gFnSc1
2019	#num#	k4
byl	být	k5eAaImAgInS
prvním	první	k4xOgInSc7
turnajem	turnaj	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
hráči	hráč	k1gMnPc1
mohli	moct	k5eAaImAgMnP
zakoupit	zakoupit	k5eAaPmF
tzv.	tzv.	kA
viewer	viewer	k1gInSc4
pass	passa	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
po	po	k7c6
zakoupení	zakoupení	k1gNnSc6
umožňuje	umožňovat	k5eAaImIp3nS
sázení	sázení	k1gNnSc1
na	na	k7c4
týmy	tým	k1gInPc4
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
nakoupené	nakoupený	k2eAgFnPc4d1
samolepky	samolepka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
viewer	viewra	k1gFnPc2
passu	pass	k1gInSc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
je	být	k5eAaImIp3nS
8,75	8,75	k4
€	€	k?
<g/>
.	.	kIx.
</s>
<s>
Sběratelské	sběratelský	k2eAgFnPc1d1
bedny	bedna	k1gFnPc1
</s>
<s>
Na	na	k7c6
Majorech	major	k1gMnPc6
VALVe	VALV	k1gInSc2
nabízí	nabízet	k5eAaImIp3nS
exkluzivní	exkluzivní	k2eAgFnPc4d1
bedny	bedna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
bedny	bedna	k1gFnPc1
dávají	dávat	k5eAaImIp3nP
náhodně	náhodně	k6eAd1
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
Major	major	k1gMnSc1
sledují	sledovat	k5eAaImIp3nP
na	na	k7c6
Twitchi	Twitch	k1gFnSc6
<g/>
,	,	kIx,
steam	steam	k6eAd1
<g/>
.	.	kIx.
<g/>
tv	tv	k?
nebo	nebo	k8xC
přímo	přímo	k6eAd1
ve	v	k7c6
hře	hra	k1gFnSc6
přes	přes	k7c4
GOTV	GOTV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
MLG	MLG	kA
v	v	k7c6
Columbusu	Columbus	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
šlo	jít	k5eAaImAgNnS
získat	získat	k5eAaPmF
bedny	bedna	k1gFnPc4
i	i	k9
při	při	k7c6
sledování	sledování	k1gNnSc6
mlg	mlg	k?
<g/>
.	.	kIx.
<g/>
tv	tv	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těchto	tento	k3xDgFnPc2
beden	bedna	k1gFnPc2
padají	padat	k5eAaImIp3nP
vzhledy	vzhled	k1gInPc1
na	na	k7c4
zbraně	zbraň	k1gFnPc4
z	z	k7c2
kolekce	kolekce	k1gFnSc2
mapy	mapa	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
hrála	hrát	k5eAaImAgFnS
<g/>
,	,	kIx,
když	když	k8xS
hráčovi	hráč	k1gMnSc3
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
bedna	bedna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
vzhledy	vzhled	k1gInPc1
mají	mít	k5eAaImIp3nP
označení	označení	k1gNnSc4
suvenýr	suvenýr	k1gInSc1
a	a	k8xC
jsou	být	k5eAaImIp3nP
cennější	cenný	k2eAgInPc1d2
než	než	k8xS
vzhledy	vzhled	k1gInPc1
bez	bez	k7c2
označení	označení	k1gNnSc2
suvenýr	suvenýr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
účast	účast	k1gFnSc1
na	na	k7c4
Majorech	major	k1gMnPc6
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Major	major	k1gMnSc1
</s>
<s>
Status	status	k1gInSc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
"	"	kIx"
<g/>
oskar	oskar	k1gMnSc1
<g/>
"	"	kIx"
ŠťastnýmousesportsPGL	ŠťastnýmousesportsPGL	k1gMnSc1
Major	major	k1gMnSc1
Kraków	Kraków	k1gMnSc1
2017	#num#	k4
<g/>
Vyzyvatel	Vyzyvatel	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
ELEAGUE	ELEAGUE	kA
Major	major	k1gMnSc1
2018	#num#	k4
<g/>
Vyzyvatel	Vyzyvatel	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
FACEIT	FACEIT	kA
Major	major	k1gMnSc1
2018	#num#	k4
<g/>
Legenda	legenda	k1gFnSc1
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Counter-Strike	Counter-Strik	k1gFnSc2
<g/>
:	:	kIx,
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Counter-Strike	Counter-Strike	k1gFnSc1
<g/>
:	:	kIx,
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
soe	soe	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
PINSOF	PINSOF	kA
<g/>
,	,	kIx,
Allistair	Allistair	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Review	Review	k1gFnSc1
<g/>
:	:	kIx,
Counter-Strike	Counter-Strike	k1gFnSc1
<g/>
:	:	kIx,
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Destructoid	Destructoid	k1gInSc1
<g/>
,	,	kIx,
2012-08-24	2012-08-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
OWEN	OWEN	kA
<g/>
,	,	kIx,
Phil	Phil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GAME	game	k1gInSc1
BYTES	BYTES	kA
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Counter-Strike	Counter-Strike	k1gInSc1
<g/>
'	'	kIx"
Lackluster	Lackluster	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnPc3
Media	medium	k1gNnSc2
Investment	Investment	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2012-08-31	2012-08-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HEATH	HEATH	kA
<g/>
,	,	kIx,
Jerome	Jerom	k1gInSc5
<g/>
;	;	kIx,
VILLANUEVA	VILLANUEVA	kA
<g/>
,	,	kIx,
Jamie	Jamie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
Economy	Econom	k1gInPc1
Guide	Guid	k1gInSc5
<g/>
:	:	kIx,
How	How	k1gMnSc3
it	it	k?
Works	Works	kA
<g/>
,	,	kIx,
Bonuses	Bonuses	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
Meta	meta	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
More	mor	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dot	Dot	k1gFnSc1
Esports	Esportsa	k1gFnPc2
<g/>
,	,	kIx,
2020-05-04	2020-05-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Money	Monea	k1gFnSc2
system	syst	k1gInSc7
in	in	k?
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
explained	explained	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natus	Natus	k1gInSc1
Vincere	Vincer	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Payback	Payback	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Bravo	bravo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Phoenix	Phoenix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Breakout	Breakout	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Vanguard	Vanguard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Bloodhound	Bloodhound	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Wildfire	Wildfir	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Hydra	hydra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Operation	Operation	k1gInSc1
Shattered	Shattered	k1gMnSc1
Web	web	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Counter-Strike	Counter-Strike	k1gNnSc7
Wiki	Wiki	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Counter-Strike	Counter-Strik	k1gInSc2
<g/>
:	:	kIx,
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Global	globat	k5eAaImAgMnS
Offensive	Offensiev	k1gFnPc4
na	na	k7c4
ValveSoftware	ValveSoftwar	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
CS	CS	kA
<g/>
:	:	kIx,
<g/>
GO	GO	kA
Major	major	k1gMnSc1
2013	#num#	k4
</s>
<s>
DreamHack	DreamHack	k1gMnSc1
Winter	Winter	k1gMnSc1
2013	#num#	k4
2014	#num#	k4
</s>
<s>
EMS	EMS	kA
One	One	k1gFnSc1
<g/>
:	:	kIx,
Katowice	Katowice	k1gFnSc1
2014	#num#	k4
•	•	k?
ESL	ESL	kA
One	One	k1gFnSc2
<g/>
:	:	kIx,
Cologne	Cologn	k1gInSc5
2014	#num#	k4
•	•	k?
DreamHack	DreamHack	k1gMnSc1
Winter	Winter	k1gMnSc1
2014	#num#	k4
2015	#num#	k4
</s>
<s>
ESL	ESL	kA
One	One	k1gFnSc1
<g/>
:	:	kIx,
Katowice	Katowice	k1gFnSc1
2015	#num#	k4
•	•	k?
ESL	ESL	kA
One	One	k1gFnSc2
<g/>
:	:	kIx,
Cologne	Cologn	k1gInSc5
2015	#num#	k4
•	•	k?
DreamHack	DreamHack	k1gMnSc1
Open	Open	k1gMnSc1
Cluj-Napoca	Cluj-Napoca	k1gMnSc1
2015	#num#	k4
2016	#num#	k4
</s>
<s>
MLG	MLG	kA
Columbus	Columbus	k1gMnSc1
2016	#num#	k4
•	•	k?
ESL	ESL	kA
One	One	k1gFnSc2
<g/>
:	:	kIx,
Cologne	Cologn	k1gInSc5
2016	#num#	k4
2017	#num#	k4
</s>
<s>
ELEAGUE	ELEAGUE	kA
Major	major	k1gMnSc1
2017	#num#	k4
•	•	k?
PGL	PGL	kA
Major	major	k1gMnSc1
Kraków	Kraków	k1gMnSc1
2017	#num#	k4
2018	#num#	k4
</s>
<s>
ELEAGUE	ELEAGUE	kA
Major	major	k1gMnSc1
2018	#num#	k4
•	•	k?
FACEIT	FACEIT	kA
Major	major	k1gMnSc1
2018	#num#	k4
2019	#num#	k4
</s>
<s>
IEM	IEM	kA
Katowice	Katowice	k1gFnSc1
2019	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
