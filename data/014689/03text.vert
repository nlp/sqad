<s>
Láska	láska	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
</s>
<s>
Láska	láska	k1gFnSc1
se	se	k3xPyFc4
vracíInterpretSvětlana	vracíInterpretSvětlana	k1gFnSc1
NálepkováDruh	NálepkováDruha	k1gFnPc2
albaStudiové	albaStudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
2008	#num#	k4
<g/>
NahránoStudio	NahránoStudio	k1gNnSc4
TOFAŽánršansonPélka	TOFAŽánršansonPélko	k1gNnSc2
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
JazykčeštinaVydavatelstvíSupraphonSvětlana	JazykčeštinaVydavatelstvíSupraphonSvětlana	k1gFnSc1
Nálepková	nálepkový	k2eAgFnSc1d1
chronologicky	chronologicky	k6eAd1
</s>
<s>
Nelituj	litovat	k5eNaImRp2nS
<g/>
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Láska	láska	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
<g/>
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mantra	mantra	k1gFnSc1
9	#num#	k4
<g/>
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Láska	láska	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
je	on	k5eAaImIp3nS
páté	pátá	k1gNnSc1
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
Světlany	Světlana	k1gFnSc2
Nálepkové	Nálepková	k1gFnSc2xF
nahrané	nahraný	k2eAgNnSc1d1
v	v	k7c6
Studio	studio	k1gNnSc6
TOFA	TOFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
vyšlo	vyjít	k5eAaPmAgNnS
v	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
složené	složený	k2eAgNnSc1d1
z	z	k7c2
několika	několik	k4yIc2
českých	český	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
textů	text	k1gInPc2
francouzských	francouzský	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
ale	ale	k8xC
také	také	k9
písní	píseň	k1gFnPc2
s	s	k7c7
hudbou	hudba	k1gFnSc7
Lumíra	Lumír	k1gMnSc2
Olšovského	Olšovský	k1gMnSc2
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
Toufara	Toufar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudební	hudební	k2eAgInSc4d1
aranžmá	aranžmá	k1gNnSc1
vytvořil	vytvořit	k5eAaPmAgInS
Blue	Blue	k1gInSc4
Angel	Angela	k1gFnPc2
Memory	Memora	k1gFnSc2
Band	banda	k1gFnPc2
Jiřího	Jiří	k1gMnSc2
Toufara	Toufar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Nechci	chtít	k5eNaImIp1nS
ti	ty	k3xPp2nSc3
vzít	vzít	k5eAaPmF
tvůj	tvůj	k3xOp2gInSc1
plán	plán	k1gInSc1
(	(	kIx(
<g/>
J	J	kA
<g/>
'	'	kIx"
<g/>
ai	ai	k?
tout	tout	k1gMnSc1
quitté	quitta	k1gMnPc1
pour	pour	k1gMnSc1
toi	toi	k?
<g/>
)	)	kIx)
český	český	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
;	;	kIx,
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
</s>
<s>
Náruč	náruč	k1gFnSc1
(	(	kIx(
<g/>
Je	být	k5eAaImIp3nS
voudrais	voudrais	k1gFnSc1
la	la	k1gNnSc2
connaître	connaîtr	k1gInSc5
<g/>
)	)	kIx)
český	český	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
;	;	kIx,
Gabriela	Gabriela	k1gFnSc1
Tomášovová	Tomášovová	k1gFnSc1
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Nevěrná	věrný	k2eNgFnSc1d1
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Toufar	Toufar	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
<g/>
)	)	kIx)
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
Sama	sám	k3xTgFnSc1
(	(	kIx(
<g/>
Lumír	Lumír	k1gMnSc1
Olšovský	Olšovský	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Lumír	Lumír	k1gMnSc1
Olšovský	Olšovský	k1gMnSc1
<g/>
)	)	kIx)
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
</s>
<s>
Tvůj	tvůj	k1gMnSc1
<g/>
,	,	kIx,
tvůj	tvůj	k3xOp2gMnSc1
(	(	kIx(
<g/>
Tout	Tout	k2eAgMnSc1d1
<g/>
)	)	kIx)
český	český	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
;	;	kIx,
Gabriela	Gabriela	k1gFnSc1
Tomášovová	Tomášovová	k1gFnSc1
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
</s>
<s>
Už	už	k6eAd1
je	být	k5eAaImIp3nS
po	po	k7c6
všem	všecek	k3xTgNnSc6
<g/>
,	,	kIx,
lásko	láska	k1gFnSc5
(	(	kIx(
<g/>
Si	se	k3xPyFc3
tu	tu	k6eAd1
m	m	kA
<g/>
'	'	kIx"
<g/>
aimes	aimes	k1gMnSc1
<g/>
)	)	kIx)
český	český	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
;	;	kIx,
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
</s>
<s>
I	i	k9
když	když	k8xS
mám	mít	k5eAaImIp1nS
světlo	světlo	k1gNnSc4
ve	v	k7c6
jméně	jméno	k1gNnSc6
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc2
Toufar	Toufar	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Pavel	Pavel	k1gMnSc1
Žižka	Žižka	k1gMnSc1
<g/>
)	)	kIx)
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
</s>
<s>
Tam	tam	k6eAd1
na	na	k7c6
předměstí	předměstí	k1gNnSc6
(	(	kIx(
<g/>
Complainte	Complaint	k1gInSc5
du	du	k?
la	la	k1gNnPc1
butte	butte	k5eAaPmIp2nP
<g/>
)	)	kIx)
český	český	k2eAgInSc4d1
text	text	k1gInSc4
<g/>
;	;	kIx,
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
</s>
<s>
Náš	náš	k3xOp1gInSc1
první	první	k4xOgInSc1
byt	byt	k1gInSc1
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Toufar	Toufar	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
<g/>
)	)	kIx)
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
</s>
<s>
Černý	Černý	k1gMnSc1
orel	orel	k1gMnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
aigle	aigle	k1gInSc1
noir	noir	k1gInSc1
<g/>
)	)	kIx)
český	český	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
;	;	kIx,
Jiří	Jiří	k1gMnSc1
Dědeček	dědeček	k1gMnSc1
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Tvůj	tvůj	k3xOp2gInSc1
kříž	kříž	k1gInSc1
(	(	kIx(
<g/>
Chanson	Chanson	k1gInSc1
simple	simple	k6eAd1
<g/>
)	)	kIx)
český	český	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
;	;	kIx,
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
</s>
<s>
Tvá	tvůj	k3xOp2gFnSc1
svatá	svatý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Toufar	Toufar	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
<g/>
)	)	kIx)
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
</s>
<s>
Píseň	píseň	k1gFnSc1
starých	starý	k2eAgMnPc2d1
milenců	milenec	k1gMnPc2
(	(	kIx(
<g/>
La	la	k1gNnSc1
Chanson	Chansona	k1gFnPc2
des	des	k1gNnSc2
vieux	vieux	k1gInSc4
amants	amants	k6eAd1
<g/>
)	)	kIx)
český	český	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
;	;	kIx,
Jiří	Jiří	k1gMnSc1
Dědeček	dědeček	k1gMnSc1
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
22	#num#	k4
</s>
<s>
Víc	hodně	k6eAd2
se	se	k3xPyFc4
snaž	snažit	k5eAaImRp2nS
(	(	kIx(
<g/>
Quand	Quand	k1gInSc1
j	j	k?
<g/>
'	'	kIx"
<g/>
ai	ai	k?
peur	peur	k1gInSc1
de	de	k?
tout	tout	k1gInSc1
<g/>
)	)	kIx)
český	český	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
;	;	kIx,
Václav	Václav	k1gMnSc1
Kopta	Kopt	k1gMnSc2
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
</s>
<s>
Černý	Černý	k1gMnSc1
orel	orel	k1gMnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
aigle	aigl	k1gMnSc2
noir	noir	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
duet	duet	k1gInSc1
s	s	k7c7
Bohušem	Bohuš	k1gMnSc7
Matušem	Matuš	k1gMnSc7
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Světlana	Světlana	k1gFnSc1
Nálepková	nálepkový	k2eAgFnSc1d1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Supraphon	supraphon	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
