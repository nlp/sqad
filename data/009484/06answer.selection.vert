<s>
Vata	vata	k1gFnSc1	vata
je	být	k5eAaImIp3nS	být
slisované	slisovaný	k2eAgNnSc4d1	slisované
celulózové	celulózový	k2eAgNnSc4d1	celulózové
vlákenné	vlákenný	k2eAgNnSc4d1	vlákenný
rouno	rouno	k1gNnSc4	rouno
o	o	k7c6	o
hustotě	hustota	k1gFnSc6	hustota
až	až	k9	až
80	[number]	k4	80
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
vlákna	vlákno	k1gNnPc1	vlákno
drží	držet	k5eAaImIp3nP	držet
vlastní	vlastní	k2eAgFnSc7d1	vlastní
silou	síla	k1gFnSc7	síla
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
