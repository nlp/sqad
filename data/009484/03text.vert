<p>
<s>
Vata	vata	k1gFnSc1	vata
je	být	k5eAaImIp3nS	být
slisované	slisovaný	k2eAgNnSc4d1	slisované
celulózové	celulózový	k2eAgNnSc4d1	celulózové
vlákenné	vlákenný	k2eAgNnSc4d1	vlákenný
rouno	rouno	k1gNnSc4	rouno
o	o	k7c6	o
hustotě	hustota	k1gFnSc6	hustota
až	až	k9	až
80	[number]	k4	80
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnPc1	jehož
vlákna	vlákno	k1gNnPc1	vlákno
drží	držet	k5eAaImIp3nP	držet
vlastní	vlastní	k2eAgFnSc7d1	vlastní
silou	síla	k1gFnSc7	síla
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
krátké	krátký	k2eAgFnSc2d1	krátká
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
bavlněného	bavlněný	k2eAgInSc2d1	bavlněný
odpadu	odpad	k1gInSc2	odpad
nebo	nebo	k8xC	nebo
z	z	k7c2	z
viskózové	viskózový	k2eAgFnSc2d1	viskózová
stříže	stříž	k1gFnSc2	stříž
ke	k	k7c3	k
zdravotnickým	zdravotnický	k2eAgFnPc3d1	zdravotnická
<g/>
,	,	kIx,	,
hygienickým	hygienický	k2eAgFnPc3d1	hygienická
a	a	k8xC	a
kosmetickým	kosmetický	k2eAgInPc3d1	kosmetický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
prvním	první	k4xOgInSc7	první
výrobkem	výrobek	k1gInSc7	výrobek
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
Victor	Victor	k1gMnSc1	Victor
von	von	k1gInSc1	von
Bruns	Bruns	k1gInSc1	Bruns
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
chirurgie	chirurgie	k1gFnSc2	chirurgie
v	v	k7c6	v
Tübingenu	Tübingen	k1gInSc6	Tübingen
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
začal	začít	k5eAaPmAgInS	začít
vatu	vata	k1gFnSc4	vata
vyrábět	vyrábět	k5eAaImF	vyrábět
Švýcar	Švýcar	k1gMnSc1	Švýcar
Heinrich	Heinrich	k1gMnSc1	Heinrich
Theophil	Theophil	k1gMnSc1	Theophil
Baeschlin	Baeschlina	k1gFnPc2	Baeschlina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
pro	pro	k7c4	pro
medicínské	medicínský	k2eAgInPc4d1	medicínský
obvazové	obvazový	k2eAgInPc4d1	obvazový
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
podniku	podnik	k1gInSc3	podnik
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Hartmann	Hartmann	k1gMnSc1	Hartmann
AG	AG	kA	AG
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
ČR	ČR	kA	ČR
i	i	k8xC	i
bývalá	bývalý	k2eAgFnSc1d1	bývalá
firma	firma	k1gFnSc1	firma
RICO	RICO	kA	RICO
ve	v	k7c6	v
Veverské	Veverský	k2eAgFnSc6d1	Veverská
Bítýšce	Bítýška	k1gFnSc6	Bítýška
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podobný	podobný	k2eAgInSc1d1	podobný
sortiment	sortiment	k1gInSc1	sortiment
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
bavlnářským	bavlnářský	k2eAgInSc7d1	bavlnářský
způsobem	způsob	k1gInSc7	způsob
==	==	k?	==
</s>
</p>
<p>
<s>
Surovina	surovina	k1gFnSc1	surovina
se	se	k3xPyFc4	se
rozvolní	rozvolnit	k5eAaPmIp3nS	rozvolnit
<g/>
,	,	kIx,	,
načechrá	načechrat	k5eAaPmIp3nS	načechrat
a	a	k8xC	a
eventuálně	eventuálně	k6eAd1	eventuálně
mísí	mísit	k5eAaImIp3nP	mísit
(	(	kIx(	(
<g/>
bavlna	bavlna	k1gFnSc1	bavlna
s	s	k7c7	s
viskózou	viskóza	k1gFnSc7	viskóza
<g/>
)	)	kIx)	)
na	na	k7c6	na
čistírenské	čistírenský	k2eAgFnSc6d1	čistírenská
soupravě	souprava	k1gFnSc6	souprava
a	a	k8xC	a
rozvlákňuje	rozvlákňovat	k5eAaImIp3nS	rozvlákňovat
na	na	k7c6	na
mykacím	mykací	k2eAgInSc6d1	mykací
stroji	stroj	k1gInSc6	stroj
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
vychází	vycházet	k5eAaImIp3nS	vycházet
nejčastěji	často	k6eAd3	často
jako	jako	k8xS	jako
pramen	pramen	k1gInSc4	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Mykaný	mykaný	k2eAgInSc1d1	mykaný
pramen	pramen	k1gInSc1	pramen
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
bělí	bělet	k5eAaImIp3nP	bělet
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
účely	účel	k1gInPc4	účel
i	i	k9	i
barví	barvit	k5eAaImIp3nP	barvit
<g/>
.	.	kIx.	.
</s>
<s>
Obvazová	obvazový	k2eAgFnSc1d1	obvazová
vata	vata	k1gFnSc1	vata
se	se	k3xPyFc4	se
sterilizuje	sterilizovat	k5eAaImIp3nS	sterilizovat
zářením	záření	k1gNnSc7	záření
γ	γ	k?	γ
dávkou	dávka	k1gFnSc7	dávka
25	[number]	k4	25
kGy	kGy	k?	kGy
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
(	(	kIx(	(
<g/>
záruční	záruční	k2eAgFnSc1d1	záruční
doba	doba	k1gFnSc1	doba
3-4	[number]	k4	3-4
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Buničitá	buničitý	k2eAgFnSc1d1	buničitá
vata	vata	k1gFnSc1	vata
==	==	k?	==
</s>
</p>
<p>
<s>
Buničitá	buničitý	k2eAgFnSc1d1	buničitá
vata	vata	k1gFnSc1	vata
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
listu	list	k1gInSc2	list
z	z	k7c2	z
bělené	bělený	k2eAgFnSc2d1	bělená
sulfitové	sulfitový	k2eAgFnSc2d1	sulfitová
nebo	nebo	k8xC	nebo
sulfátové	sulfátový	k2eAgFnSc2d1	sulfátová
buničiny	buničina	k1gFnSc2	buničina
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
přimíchávat	přimíchávat	k5eAaImF	přimíchávat
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
dřevní	dřevní	k2eAgFnSc2d1	dřevní
buničiny	buničina	k1gFnSc2	buničina
(	(	kIx(	(
<g/>
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konečný	konečný	k2eAgInSc1d1	konečný
výrobek	výrobek	k1gInSc1	výrobek
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
listu	list	k1gInSc2	list
nebo	nebo	k8xC	nebo
z	z	k7c2	z
několika	několik	k4yIc2	několik
vrstev	vrstva	k1gFnPc2	vrstva
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zdravotnické	zdravotnický	k2eAgInPc4d1	zdravotnický
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
vata	vata	k1gFnSc1	vata
dodává	dodávat	k5eAaImIp3nS	dodávat
adjustovaná	adjustovaný	k2eAgFnSc1d1	adjustovaná
nebo	nebo	k8xC	nebo
dělená	dělený	k2eAgFnSc1d1	dělená
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
a	a	k8xC	a
velikostech	velikost	k1gFnPc6	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Vata	vata	k1gFnSc1	vata
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vata	vata	k1gFnSc1	vata
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Simon	Simon	k1gMnSc1	Simon
<g/>
/	/	kIx~	/
<g/>
Horáček	Horáček	k1gMnSc1	Horáček
<g/>
:	:	kIx,	:
Technologie	technologie	k1gFnSc1	technologie
přádelnictví	přádelnictví	k1gNnSc2	přádelnictví
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
Kießling	Kießling	k1gInSc1	Kießling
<g/>
/	/	kIx~	/
<g/>
Matthes	Matthes	k1gInSc1	Matthes
<g/>
:	:	kIx,	:
Textil-Fachwörterbuch	Textil-Fachwörterbuch	k1gInSc1	Textil-Fachwörterbuch
<g/>
,	,	kIx,	,
Schiele	Schiel	k1gInPc1	Schiel
&	&	k?	&
Schön	Schön	k1gInSc4	Schön
Berlin	berlina	k1gFnPc2	berlina
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-7949-0546-6	[number]	k4	3-7949-0546-6
</s>
</p>
