<s>
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodné	rodný	k2eAgFnPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Donald	Donald	k1gMnSc1
John	John	k1gMnSc1
Trump	Trump	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
43	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Manhattan	Manhattan	k1gInSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
New	New	k?
York	York	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Wharton	Wharton	k1gInSc1
SchoolThe	SchoolThe	k1gFnPc2
Hill	Hill	k1gMnSc1
SchoolBuckley	SchoolBucklea	k1gFnSc2
School	Schoola	k1gFnPc2
Povolání	povolání	k1gNnSc2
</s>
<s>
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
obchodník	obchodník	k1gMnSc1
a	a	k8xC
televizní	televizní	k2eAgMnSc1d1
hlasatel	hlasatel	k1gMnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
The	The	k?
Trump	Trump	k1gInSc1
Organization	Organization	k1gInSc1
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Vanessa	Vanessa	k1gFnSc1
Trump	Trump	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Kai	Kai	k?
Madison	Madison	k1gInSc1
TrumpDonald	TrumpDonald	k1gInSc1
Trump	Trump	k1gInSc1
IIITristan	IIITristan	k1gInSc1
Milos	Milos	k1gInSc1
TrumpChloe	TrumpChlo	k1gMnSc2
Sophia	Sophius	k1gMnSc2
TrumpSpencer	TrumpSpencer	k1gMnSc1
Frederick	Frederick	k1gMnSc1
Trump	Trump	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
a	a	k8xC
Ivana	Ivana	k1gFnSc1
Trumpová	Trumpová	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Eric	Eric	k1gInSc1
Trump	Trump	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Tiffany	Tiffana	k1gFnSc2
Trumpová	Trumpový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Ivanka	Ivanka	k1gFnSc1
Trumpová	Trumpová	k1gFnSc1
a	a	k8xC
Barron	Barron	k1gInSc1
William	William	k1gInSc1
Trump	Trump	k1gInSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.trump.com	www.trump.com	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Donald	Donald	k1gMnSc1
John	John	k1gMnSc1
„	„	k?
<g/>
Don	Don	k1gMnSc1
<g/>
“	“	k?
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
*	*	kIx~
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1977	#num#	k4
New	New	k1gFnPc2
York	York	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
prvorozeným	prvorozený	k2eAgMnSc7d1
potomkem	potomek	k1gMnSc7
45	#num#	k4
<g/>
.	.	kIx.
prezidenta	prezident	k1gMnSc2
USA	USA	kA
Donalda	Donald	k1gMnSc2
Trumpa	Trump	k1gMnSc2
a	a	k8xC
české	český	k2eAgFnSc2d1
modelky	modelka	k1gFnSc2
Ivany	Ivana	k1gFnSc2
Trumpové	Trumpová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivně	aktivně	k6eAd1
podporoval	podporovat	k5eAaImAgInS
kandidaturu	kandidatura	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
na	na	k7c4
funkci	funkce	k1gFnSc4
prezidenta	prezident	k1gMnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
před	před	k7c7
volbami	volba	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
pracuje	pracovat	k5eAaImIp3nS
spolu	spolu	k6eAd1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
sestrou	sestra	k1gFnSc7
Ivankou	Ivanka	k1gFnSc7
Trumpovou	Trumpová	k1gFnSc7
a	a	k8xC
bratrem	bratr	k1gMnSc7
Ericem	Erice	k1gMnSc7
Trumpem	Trump	k1gMnSc7
na	na	k7c6
pozici	pozice	k1gFnSc6
výkonného	výkonný	k2eAgMnSc2d1
viceprezidenta	viceprezident	k1gMnSc2
společnosti	společnost	k1gFnSc2
The	The	k1gMnSc1
Trump	Trump	k1gMnSc1
Organization	Organization	k1gInSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
vybudoval	vybudovat	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Dětství	dětství	k1gNnSc4
a	a	k8xC
mládí	mládí	k1gNnSc4
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1977	#num#	k4
na	na	k7c6
Manhattanu	Manhattan	k1gInSc6
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dva	dva	k4xCgMnPc4
mladší	mladý	k2eAgMnPc4d2
sourozence	sourozenec	k1gMnPc4
<g/>
,	,	kIx,
Ivanku	Ivanka	k1gFnSc4
a	a	k8xC
Erica	Erica	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
dva	dva	k4xCgMnPc1
nevlastní	vlastnit	k5eNaImIp3nP
sourozence	sourozenec	k1gMnPc4
<g/>
,	,	kIx,
Tiffany	Tiffana	k1gFnPc4
z	z	k7c2
otcova	otcův	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
s	s	k7c7
Marlou	Marla	k1gFnSc7
Maples	Maplesa	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
Barrona	Barron	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
současného	současný	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
otce	otec	k1gMnSc4
s	s	k7c7
Melanií	Melanie	k1gFnSc7
Trumpovou	Trumpová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
vlastních	vlastní	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
mu	on	k3xPp3gNnSc3
byli	být	k5eAaImAgMnP
zvláště	zvláště	k9
blízcí	blízký	k2eAgMnPc1d1
babička	babička	k1gFnSc1
a	a	k8xC
dědeček	dědeček	k1gMnSc1
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
hovoří	hovořit	k5eAaImIp3nS
proto	proto	k8xC
plynule	plynule	k6eAd1
česky	česky	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studoval	studovat	k5eAaImAgInS
na	na	k7c6
The	The	k1gFnSc6
Hill	Hill	k1gMnSc1
School	Schoola	k1gFnPc2
<g/>
,	,	kIx,
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
přípravné	přípravný	k2eAgFnSc3d1
internátní	internátní	k2eAgFnSc3d1
škole	škola	k1gFnSc3
v	v	k7c6
Pottstownu	Pottstown	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Pensylvánii	Pensylvánie	k1gFnSc6
<g/>
,	,	kIx,
následně	následně	k6eAd1
na	na	k7c4
Wharton	Wharton	k1gInSc4
School	School	k1gInSc1
of	of	k?
the	the	k?
University	universita	k1gFnSc2
of	of	k?
Pennsylvania	Pennsylvanium	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
B.	B.	kA
S.	S.	kA
v	v	k7c6
oboru	obor	k1gInSc6
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1
působení	působení	k1gNnSc1
</s>
<s>
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
hovoří	hovořit	k5eAaImIp3nS
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
na	na	k7c6
předvolebním	předvolební	k2eAgInSc6d1
mítinku	mítink	k1gInSc6
ve	v	k7c6
státě	stát	k1gInSc6
Iowa	Iow	k1gInSc2
o	o	k7c6
programu	program	k1gInSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
</s>
<s>
Objevil	objevit	k5eAaPmAgInS
se	se	k3xPyFc4
jako	jako	k9
host	host	k1gMnSc1
v	v	k7c6
několika	několik	k4yIc6
epizodách	epizoda	k1gFnPc6
televizní	televizní	k2eAgFnSc2d1
reality	realita	k1gFnSc2
show	show	k1gFnSc1
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
The	The	k1gMnSc2
Apprentice	Apprentice	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
od	od	k7c2
5	#num#	k4
<g/>
.	.	kIx.
řady	řada	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
až	až	k9
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
14	#num#	k4
<g/>
.	.	kIx.
řady	řada	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Působil	působit	k5eAaImAgMnS
při	při	k7c6
organizaci	organizace	k1gFnSc6
otcovy	otcův	k2eAgFnSc2d1
volební	volební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
jeho	jeho	k3xOp3gFnSc2
kandidatury	kandidatura	k1gFnSc2
ve	v	k7c6
volbách	volba	k1gFnPc6
prezidenta	prezident	k1gMnSc2
USA	USA	kA
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
promluvil	promluvit	k5eAaPmAgMnS
na	na	k7c6
republikánském	republikánský	k2eAgNnSc6d1
národním	národní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
delegátem	delegát	k1gMnSc7
byl	být	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
Trumpově	Trumpův	k2eAgNnSc6d1
zvolení	zvolení	k1gNnSc6
byl	být	k5eAaImAgMnS
členem	člen	k1gInSc7
16	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
členného	členný	k2eAgInSc2d1
přípravného	přípravný	k2eAgInSc2d1
týmu	tým	k1gInSc2
pro	pro	k7c4
novou	nový	k2eAgFnSc4d1
administrativu	administrativa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
aférou	aféra	k1gFnSc7
kolem	kolem	k7c2
ovlivňování	ovlivňování	k1gNnSc2
voleb	volba	k1gFnPc2
Ruskem	Rusko	k1gNnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
především	především	k6eAd1
prezidenta	prezident	k1gMnSc2
Trumpa	Trump	k1gMnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
v	v	k7c6
červenci	červenec	k1gInSc6
2017	#num#	k4
intenzívně	intenzívně	k6eAd1
diskutováno	diskutován	k2eAgNnSc1d1
setkání	setkání	k1gNnSc1
Donalda	Donald	k1gMnSc2
Trumpa	Trump	k1gMnSc2
juniora	junior	k1gMnSc2
s	s	k7c7
ruskou	ruský	k2eAgFnSc7d1
občankou	občanka	k1gFnSc7
<g/>
,	,	kIx,
právničkou	právnička	k1gFnSc7
Nataljou	Nataljý	k2eAgFnSc7d1
Veselnickou	Veselnický	k2eAgFnSc7d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
blízká	blízký	k2eAgFnSc1d1
administrativě	administrativa	k1gFnSc3
prezidenta	prezident	k1gMnSc2
Putina	putin	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2005	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
rodinném	rodinný	k2eAgNnSc6d1
sídle	sídlo	k1gNnSc6
Mar-a-Lago	Mar-a-Lago	k1gNnSc1
v	v	k7c4
Palm	Palm	k1gInSc4
Beach	Beacha	k1gFnPc2
ve	v	k7c6
státě	stát	k1gInSc6
Florida	Florida	k1gFnSc1
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
modelkou	modelka	k1gFnSc7
Vanessou	Vanessa	k1gFnSc7
Haydonovou	Haydonová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Servis	servis	k1gInSc1
při	při	k7c6
svatbě	svatba	k1gFnSc6
zajistila	zajistit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
teta	teta	k1gFnSc1
Maryanne	Maryann	k1gInSc5
Trump	Trump	k1gInSc4
Barryová	Barryový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haydonová	Haydonová	k1gFnSc1
je	být	k5eAaImIp3nS
dcerou	dcera	k1gFnSc7
Bonnie	Bonnie	k1gFnSc2
a	a	k8xC
Charlese	Charles	k1gMnSc2
Haydona	Haydon	k1gMnSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
židovského	židovský	k2eAgInSc2d1
a	a	k8xC
dánského	dánský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Absolvovala	absolvovat	k5eAaPmAgFnS
Dwight	Dwight	k2eAgInSc4d1
School	School	k1gInSc4
a	a	k8xC
poté	poté	k6eAd1
studovala	studovat	k5eAaImAgFnS
psychologii	psychologie	k1gFnSc4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
na	na	k7c4
Marymount	Marymount	k1gInSc4
Manhattan	Manhattan	k1gInSc1
College	College	k1gFnSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Trumpovi	Trumpův	k2eAgMnPc1d1
mají	mít	k5eAaImIp3nP
pět	pět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
dcery	dcera	k1gFnSc2
Kai	Kai	k1gMnSc1
Madison	Madison	k1gMnSc1
(	(	kIx(
<g/>
narozena	narozen	k2eAgFnSc1d1
2007	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Chloe	Chloe	k1gFnSc1
Sophii	Sophium	k1gNnPc7
(	(	kIx(
<g/>
*	*	kIx~
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
syny	syn	k1gMnPc7
Donalda	Donald	k1gMnSc2
Johna	John	k1gMnSc2
III	III	kA
(	(	kIx(
<g/>
narozen	narozen	k2eAgInSc1d1
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tristana	Tristan	k1gMnSc4
Miloše	Miloš	k1gMnSc4
(	(	kIx(
<g/>
narozen	narozen	k2eAgInSc1d1
2011	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Spencera	Spencera	k1gFnSc1
Fredericka	Fredericka	k1gFnSc1
(	(	kIx(
<g/>
narozen	narozen	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
on	on	k3xPp3gMnSc1
Twitter	Twitter	k1gMnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
I	i	k9
speak	speak	k6eAd1
Czech	Czech	k1gInSc4
fluently	fluentnout	k5eAaPmAgInP
<g/>
"	"	kIx"
@	@	kIx~
<g/>
sofia	sofia	k1gFnSc1
<g/>
5013	#num#	k4
<g/>
:	:	kIx,
Jr	Jr	k1gFnSc6
great	great	k2eAgMnSc1d1
work	work	k1gMnSc1
with	with	k1gMnSc1
@	@	kIx~
<g/>
operationsmile	operationsmile	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Industry	Industr	k1gInPc4
Insiders	Insiders	k1gInSc1
<g/>
:	:	kIx,
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Trumped	Trumped	k1gInSc1
Again	Again	k1gInSc1
<g/>
"	"	kIx"
Archivováno	archivovat	k5eAaBmNgNnS
23	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.1	.1	k4
2	#num#	k4
Erste	Erst	k1gInSc5
große	große	k1gNnSc3
Personalentscheidung	Personalentscheidung	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trump	Trump	k1gInSc1
besetzt	besetzt	k2eAgInSc4d1
sein	sein	k1gInSc4
Team	team	k1gInSc1
um	uma	k1gFnPc2
(	(	kIx(
<g/>
První	první	k4xOgFnSc2
velké	velká	k1gFnSc2
personální	personální	k2eAgNnPc1d1
rozhodnutí	rozhodnutí	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trump	Trump	k1gInSc1
přeorganizoval	přeorganizovat	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
tým	tým	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
http://www.t-online.de/nachrichten/ausland/usa/id_79524864/donald-trump-besetzt-sein-uebergangsteam-um.html.	http://www.t-online.de/nachrichten/ausland/usa/id_79524864/donald-trump-besetzt-sein-uebergangsteam-um.html.	k4
T-Online	T-Onlin	k1gInSc5
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LA	la	k1gNnSc2
Times	Timesa	k1gFnPc2
<g/>
,	,	kIx,
CLEVELAND	CLEVELAND	kA
<g/>
,	,	kIx,
Lisa	Lisa	k1gFnSc1
Mascaro	Mascara	k1gFnSc5
<g/>
,	,	kIx,
July	Jula	k1gFnSc2
19	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
announced	announced	k1gMnSc1
the	the	k?
votes	votes	k1gMnSc1
to	ten	k3xDgNnSc4
send	send	k6eAd1
his	his	k1gNnSc1
dad	dad	k?
over	over	k1gMnSc1
the	the	k?
top	topit	k5eAaImRp2nS
<g/>
↑	↑	k?
Kompro	Kompro	k1gNnSc1
na	na	k7c4
Clintonovou	Clintonová	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jsem	být	k5eAaImIp1nS
nadšen	nadchnout	k5eAaPmNgMnS
<g/>
!	!	kIx.
</s>
<s desamb="1">
reagoval	reagovat	k5eAaBmAgMnS
Trumpův	Trumpův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
na	na	k7c4
údajnou	údajný	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
z	z	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-07-11	2017-07-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Weddings	Weddings	k1gInSc1
<g/>
:	:	kIx,
Vanessa	Vanessa	k1gFnSc1
Haydon	Haydon	k1gMnSc1
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
"	"	kIx"
<g/>
↑	↑	k?
http://nymag.com/nymetro/health/features/3422/	http://nymag.com/nymetro/health/features/3422/	k4
<g/>
↑	↑	k?
https://www.jewishtampa.com/jews-in-the-news/jews-in-the-news-andy-samberg-liza-weil-ivanka-trump	https://www.jewishtampa.com/jews-in-the-news/jews-in-the-news-andy-samberg-liza-weil-ivanka-trump	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1212147197	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2019063346	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
9573150325558710090003	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2019063346	#num#	k4
</s>
