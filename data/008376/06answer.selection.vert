<s>
Johannes	Johannes	k1gMnSc1	Johannes
Gensfleisch	Gensfleisch	k1gMnSc1	Gensfleisch
<g/>
,	,	kIx,	,
řečený	řečený	k2eAgMnSc1d1	řečený
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Johannes	Johannes	k1gMnSc1	Johannes
Gensfleisch	Gensfleisch	k1gMnSc1	Gensfleisch
zur	zur	k?	zur
Laden	ladno	k1gNnPc2	ladno
zum	zum	k?	zum
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
(	(	kIx(	(
<g/>
1397	[number]	k4	1397
<g/>
/	/	kIx~	/
<g/>
1400	[number]	k4	1400
<g/>
,	,	kIx,	,
Mohuč	Mohuč	k1gFnSc1	Mohuč
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1468	[number]	k4	1468
<g/>
,	,	kIx,	,
Mohuč	Mohuč	k1gFnSc1	Mohuč
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vynálezce	vynálezce	k1gMnSc1	vynálezce
technologie	technologie	k1gFnSc2	technologie
mechanického	mechanický	k2eAgInSc2d1	mechanický
knihtisku	knihtisk	k1gInSc2	knihtisk
pomocí	pomocí	k7c2	pomocí
sestavovatelných	sestavovatelný	k2eAgFnPc2d1	sestavovatelný
liter	litera	k1gFnPc2	litera
<g/>
.	.	kIx.	.
</s>
