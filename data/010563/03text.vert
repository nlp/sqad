<p>
<s>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
<g/>
;	;	kIx,	;
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
transkripcí	transkripce	k1gFnSc7	transkripce
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
al-Kajdá	al-Kajdý	k2eAgFnSc1d1	al-Kajdá
<g/>
,	,	kIx,	,
al-Kajda	al-Kajda	k1gMnSc1	al-Kajda
<g/>
,	,	kIx,	,
al-Kájda	al-Kájda	k1gMnSc1	al-Kájda
<g/>
,	,	kIx,	,
Al-kajda	Alajda	k1gMnSc1	Al-kajda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
obvykle	obvykle	k6eAd1	obvykle
překládáno	překládat	k5eAaImNgNnS	překládat
jako	jako	k9	jako
základna	základna	k1gFnSc1	základna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
militantní	militantní	k2eAgFnSc7d1	militantní
islamistickou	islamistický	k2eAgFnSc7d1	islamistická
teroristickou	teroristický	k2eAgFnSc7d1	teroristická
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
řadou	řada	k1gFnSc7	řada
velkých	velký	k2eAgInPc2d1	velký
atentátů	atentát	k1gInPc2	atentát
a	a	k8xC	a
únosů	únos	k1gInPc2	únos
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
včetně	včetně	k7c2	včetně
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2001	[number]	k4	2001
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
stál	stát	k5eAaImAgInS	stát
již	již	k6eAd1	již
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
založení	založení	k1gNnSc2	založení
saúdskoarabský	saúdskoarabský	k2eAgMnSc1d1	saúdskoarabský
multimilionář	multimilionář	k1gMnSc1	multimilionář
<g/>
,	,	kIx,	,
terorista	terorista	k1gMnSc1	terorista
Usáma	Usámum	k1gNnSc2	Usámum
bin	bin	k?	bin
Ládin	Ládin	k2eAgInSc1d1	Ládin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
měrou	míra	k1gFnSc7wR	míra
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
financování	financování	k1gNnSc6	financování
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
při	při	k7c6	při
cíleném	cílený	k2eAgInSc6d1	cílený
vojenském	vojenský	k2eAgInSc6d1	vojenský
útoku	útok	k1gInSc6	útok
amerických	americký	k2eAgFnPc2d1	americká
speciálních	speciální	k2eAgFnPc2d1	speciální
sil	síla	k1gFnPc2	síla
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
2	[number]	k4	2
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
sídle	sídlo	k1gNnSc6	sídlo
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
podporuje	podporovat	k5eAaImIp3nS	podporovat
sunnitskou	sunnitský	k2eAgFnSc4d1	sunnitská
větvi	větvit	k5eAaImRp2nS	větvit
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cíle	Cíla	k1gFnSc6	Cíla
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
prohlášení	prohlášení	k1gNnPc2	prohlášení
pokládá	pokládat	k5eAaImIp3nS	pokládat
al-Káida	al-Káida	k1gFnSc1	al-Káida
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
prioritní	prioritní	k2eAgInSc4d1	prioritní
cíl	cíl	k1gInSc4	cíl
nastolení	nastolení	k1gNnSc2	nastolení
ryze	ryze	k6eAd1	ryze
muslimských	muslimský	k2eAgInPc2d1	muslimský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chalífátů	chalífát	k1gInPc2	chalífát
<g/>
)	)	kIx)	)
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
šíření	šíření	k1gNnSc6	šíření
islámské	islámský	k2eAgFnSc2d1	islámská
víry	víra	k1gFnSc2	víra
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
hlavního	hlavní	k2eAgMnSc4d1	hlavní
nepřítele	nepřítel	k1gMnSc4	nepřítel
přitom	přitom	k6eAd1	přitom
označuje	označovat	k5eAaImIp3nS	označovat
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
viní	vinit	k5eAaImIp3nP	vinit
z	z	k7c2	z
drancování	drancování	k1gNnSc2	drancování
arabského	arabský	k2eAgNnSc2d1	arabské
ropného	ropný	k2eAgNnSc2d1	ropné
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
ze	z	k7c2	z
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Osočují	osočovat	k5eAaImIp3nP	osočovat
však	však	k9	však
i	i	k9	i
řadu	řada	k1gFnSc4	řada
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
z	z	k7c2	z
výrazné	výrazný	k2eAgFnSc2d1	výrazná
podpory	podpora	k1gFnSc2	podpora
amerických	americký	k2eAgFnPc2d1	americká
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
údajně	údajně	k6eAd1	údajně
nebudou	být	k5eNaImBp3nP	být
váhat	váhat	k5eAaImF	váhat
teroristicky	teroristicky	k6eAd1	teroristicky
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
evropské	evropský	k2eAgInPc4d1	evropský
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgNnSc2	svůj
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c4	o
prosazení	prosazení	k1gNnSc4	prosazení
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
šarí	šarit	k5eAaPmIp3nP	šarit
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
si	se	k3xPyFc3	se
ideologicky	ideologicky	k6eAd1	ideologicky
ospravedlňují	ospravedlňovat	k5eAaImIp3nP	ospravedlňovat
zabíjení	zabíjení	k1gNnSc4	zabíjení
bezvěrců	bezvěrec	k1gMnPc2	bezvěrec
i	i	k8xC	i
muslimů	muslim	k1gMnPc2	muslim
vlažných	vlažný	k2eAgMnPc2d1	vlažný
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uváděn	k2eAgInSc4d1	uváděn
rok	rok	k1gInSc4	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
však	však	k9	však
sahají	sahat	k5eAaImIp3nP	sahat
již	již	k6eAd1	již
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
zahájení	zahájení	k1gNnSc2	zahájení
okupace	okupace	k1gFnSc2	okupace
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
vojsky	vojsko	k1gNnPc7	vojsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gInPc2	jeho
členů	člen	k1gInPc2	člen
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
rekrutovala	rekrutovat	k5eAaImAgFnS	rekrutovat
z	z	k7c2	z
arabských	arabský	k2eAgInPc2d1	arabský
mudžáhidů	mudžáhid	k1gMnPc2	mudžáhid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
okupačním	okupační	k2eAgNnPc3d1	okupační
sovětským	sovětský	k2eAgNnPc3d1	sovětské
vojskům	vojsko	k1gNnPc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
al-Káidy	al-Káida	k1gFnSc2	al-Káida
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
Usáma	Usáma	k1gNnSc1	Usáma
bin	bin	k?	bin
Ládin	Ládin	k2eAgMnSc1d1	Ládin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zasílal	zasílat	k5eAaImAgMnS	zasílat
pomoc	pomoc	k1gFnSc4	pomoc
ze	z	k7c2	z
sousedního	sousední	k2eAgInSc2d1	sousední
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
mudžáhidům	mudžáhid	k1gMnPc3	mudžáhid
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
během	během	k7c2	během
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
sovětské	sovětský	k2eAgFnSc3d1	sovětská
okupaci	okupace	k1gFnSc3	okupace
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
tak	tak	k6eAd1	tak
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
velké	velký	k2eAgFnPc1d1	velká
skupiny	skupina	k1gFnPc1	skupina
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vyzbrojeny	vyzbrojen	k2eAgFnPc1d1	vyzbrojena
a	a	k8xC	a
kvalitně	kvalitně	k6eAd1	kvalitně
vycvičeny	vycvičen	k2eAgFnPc1d1	vycvičena
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
řadou	řada	k1gFnSc7	řada
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
mudžáhidů	mudžáhid	k1gMnPc2	mudžáhid
navázal	navázat	k5eAaPmAgInS	navázat
Usáma	Usáma	k1gNnSc4	Usáma
bin	bin	k?	bin
Ládin	Ládin	k2eAgInSc4d1	Ládin
kontakt	kontakt	k1gInSc4	kontakt
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
jim	on	k3xPp3gMnPc3	on
místo	místo	k7c2	místo
v	v	k7c6	v
nově	nova	k1gFnSc6	nova
vznikající	vznikající	k2eAgFnSc6d1	vznikající
ryze	ryze	k6eAd1	ryze
islámské	islámský	k2eAgFnSc6d1	islámská
organizaci	organizace	k1gFnSc6	organizace
al-Káida	al-Káid	k1gMnSc2	al-Káid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizační	organizační	k2eAgInSc4d1	organizační
systém	systém	k1gInSc4	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Organizační	organizační	k2eAgNnSc1d1	organizační
schéma	schéma	k1gNnSc1	schéma
al-Káidy	al-Káida	k1gFnSc2	al-Káida
je	být	k5eAaImIp3nS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
velmi	velmi	k6eAd1	velmi
propracovaně	propracovaně	k6eAd1	propracovaně
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tříčlenných	tříčlenný	k2eAgFnPc2d1	tříčlenná
skupinek	skupinka	k1gFnPc2	skupinka
(	(	kIx(	(
<g/>
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
znají	znát	k5eAaImIp3nP	znát
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Velitelé	velitel	k1gMnPc1	velitel
nad	nad	k7c7	nad
nimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
třemi	tři	k4xCgFnPc7	tři
buňkami	buňka	k1gFnPc7	buňka
a	a	k8xC	a
tak	tak	k6eAd1	tak
až	až	k9	až
prakticky	prakticky	k6eAd1	prakticky
k	k	k7c3	k
vrcholnému	vrcholný	k2eAgNnSc3d1	vrcholné
velení	velení	k1gNnSc3	velení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
této	tento	k3xDgFnSc2	tento
struktury	struktura	k1gFnSc2	struktura
je	být	k5eAaImIp3nS	být
především	především	k9	především
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
utajení	utajení	k1gNnSc1	utajení
a	a	k8xC	a
jen	jen	k6eAd1	jen
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgFnSc6d1	obtížná
infiltraci	infiltrace	k1gFnSc6	infiltrace
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
prozrazení	prozrazení	k1gNnSc3	prozrazení
jedné	jeden	k4xCgFnSc2	jeden
tříčlenné	tříčlenný	k2eAgFnSc2d1	tříčlenná
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
pozatýkáni	pozatýkán	k2eAgMnPc1d1	pozatýkán
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
prakticky	prakticky	k6eAd1	prakticky
prozradit	prozradit	k5eAaPmF	prozradit
žádného	žádný	k3yNgMnSc4	žádný
dalšího	další	k2eAgMnSc4d1	další
člena	člen	k1gMnSc4	člen
kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgMnSc2	svůj
přímého	přímý	k2eAgMnSc2d1	přímý
velitele	velitel	k1gMnSc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jemu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
stopa	stopa	k1gFnSc1	stopa
zde	zde	k6eAd1	zde
definitivně	definitivně	k6eAd1	definitivně
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
systému	systém	k1gInSc2	systém
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
i	i	k9	i
současná	současný	k2eAgFnSc1d1	současná
praxe	praxe	k1gFnSc1	praxe
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přes	přes	k7c4	přes
intenzivní	intenzivní	k2eAgNnSc4d1	intenzivní
pátrání	pátrání	k1gNnSc4	pátrání
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
tajných	tajný	k2eAgFnPc2d1	tajná
služeb	služba	k1gFnPc2	služba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zatčení	zatčení	k1gNnSc3	zatčení
špičkově	špičkově	k6eAd1	špičkově
postavených	postavený	k2eAgMnPc2d1	postavený
mužů	muž	k1gMnPc2	muž
al-Káidy	al-Káida	k1gFnSc2	al-Káida
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
členové	člen	k1gMnPc1	člen
uvedených	uvedený	k2eAgFnPc2d1	uvedená
buněk	buňka	k1gFnPc2	buňka
pracují	pracovat	k5eAaImIp3nP	pracovat
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
spící	spící	k2eAgMnPc1d1	spící
agenti	agent	k1gMnPc1	agent
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevyvíjejí	vyvíjet	k5eNaImIp3nP	vyvíjet
žádnou	žádný	k3yNgFnSc4	žádný
vlastní	vlastní	k2eAgFnSc4d1	vlastní
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
naprosto	naprosto	k6eAd1	naprosto
skrytě	skrytě	k6eAd1	skrytě
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
terorista	terorista	k1gMnSc1	terorista
aktivován	aktivován	k2eAgMnSc1d1	aktivován
a	a	k8xC	a
provede	provést	k5eAaPmIp3nS	provést
předem	předem	k6eAd1	předem
dohodnutou	dohodnutý	k2eAgFnSc4d1	dohodnutá
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
komunikaci	komunikace	k1gFnSc3	komunikace
používají	používat	k5eAaImIp3nP	používat
členové	člen	k1gMnPc1	člen
al-Káidy	al-Káida	k1gFnSc2	al-Káida
i	i	k9	i
velmi	velmi	k6eAd1	velmi
moderní	moderní	k2eAgFnPc1d1	moderní
metody	metoda	k1gFnPc1	metoda
včetně	včetně	k7c2	včetně
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
šifrovaných	šifrovaný	k2eAgFnPc2d1	šifrovaná
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jdou	jít	k5eAaImIp3nP	jít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
stěží	stěží	k6eAd1	stěží
rozluštit	rozluštit	k5eAaPmF	rozluštit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odnože	odnož	k1gFnPc4	odnož
a	a	k8xC	a
příbuzné	příbuzný	k1gMnPc4	příbuzný
organizace	organizace	k1gFnSc2	organizace
Al-Káidy	Al-Káida	k1gFnSc2	Al-Káida
==	==	k?	==
</s>
</p>
<p>
<s>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
v	v	k7c6	v
islámském	islámský	k2eAgInSc6d1	islámský
Maghrebu	Maghreb	k1gInSc6	Maghreb
–	–	k?	–
podružná	podružný	k2eAgFnSc1d1	podružná
organizace	organizace	k1gFnSc1	organizace
aktivní	aktivní	k2eAgFnSc1d1	aktivní
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
–	–	k?	–
podružná	podružný	k2eAgFnSc1d1	podružná
organizace	organizace	k1gFnSc1	organizace
aktivní	aktivní	k2eAgFnSc1d1	aktivní
na	na	k7c6	na
území	území	k1gNnSc6	území
Jemenu	Jemen	k1gInSc2	Jemen
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
na	na	k7c6	na
Indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
(	(	kIx(	(
<g/>
též	též	k9	též
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Islámský	islámský	k2eAgInSc1d1	islámský
džihád	džihád	k1gInSc1	džihád
Jemenu	Jemen	k1gInSc2	Jemen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
teroristická	teroristický	k2eAgFnSc1d1	teroristická
organizace	organizace	k1gFnSc1	organizace
volně	volně	k6eAd1	volně
přidružená	přidružený	k2eAgFnSc1d1	přidružená
k	k	k7c3	k
al-Káidě	al-Káida	k1gFnSc3	al-Káida
<g/>
,	,	kIx,	,
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
bombový	bombový	k2eAgInSc4d1	bombový
útok	útok	k1gInSc4	útok
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
ambasádu	ambasáda	k1gFnSc4	ambasáda
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Aš-Šabáb	Aš-Šabáb	k1gInSc1	Aš-Šabáb
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
islámských	islámský	k2eAgMnPc2d1	islámský
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
loajální	loajální	k2eAgMnSc1d1	loajální
k	k	k7c3	k
al-Káidě	al-Káida	k1gFnSc3	al-Káida
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ansar	Ansar	k1gInSc1	Ansar
al-Islám	al-Islat	k5eAaPmIp1nS	al-Islat
–	–	k?	–
aktivní	aktivní	k2eAgFnSc4d1	aktivní
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
v	v	k7c6	v
Iráckém	irácký	k2eAgInSc6d1	irácký
Kurdistánu	Kurdistán	k1gInSc6	Kurdistán
<g/>
,	,	kIx,	,
působící	působící	k2eAgFnSc3d1	působící
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
podružná	podružný	k2eAgFnSc1d1	podružná
organizace	organizace	k1gFnSc1	organizace
al-Káidy	al-Káida	k1gFnSc2	al-Káida
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Fronta	fronta	k1gFnSc1	fronta
an-Nusrá	an-Nusrý	k2eAgFnSc1d1	an-Nusrý
–	–	k?	–
syrská	syrský	k2eAgFnSc1d1	Syrská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
organizace	organizace	k1gFnSc1	organizace
a	a	k8xC	a
syrská	syrský	k2eAgFnSc1d1	Syrská
odnož	odnož	k1gFnSc1	odnož
al-Káidy	al-Káida	k1gFnSc2	al-Káida
účastnící	účastnící	k2eAgFnSc2d1	účastnící
se	se	k3xPyFc4	se
Občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
Chorásán	Chorásán	k1gInSc1	Chorásán
</s>
</p>
<p>
<s>
Islámský	islámský	k2eAgInSc1d1	islámský
džihád	džihád	k1gInSc1	džihád
–	–	k?	–
egyptské	egyptský	k2eAgNnSc1d1	egyptské
islámské	islámský	k2eAgNnSc1d1	islámské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vznik	vznik	k1gInSc1	vznik
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
al-Káidě	al-Káida	k1gFnSc3	al-Káida
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
vůdce	vůdce	k1gMnSc1	vůdce
al-Káidy	al-Káida	k1gFnSc2	al-Káida
Ajmán	Ajmán	k1gMnSc1	Ajmán
Zavahrí	Zavahrí	k1gMnSc1	Zavahrí
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Fatah	Fatah	k1gInSc1	Fatah
al-Islám	al-Islat	k5eAaImIp1nS	al-Islat
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
aktivní	aktivní	k2eAgFnSc1d1	aktivní
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
účastnící	účastnící	k2eAgFnSc7d1	účastnící
se	se	k3xPyFc4	se
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
napojená	napojený	k2eAgNnPc1d1	napojené
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
al-Káidu	al-Káid	k1gInSc2	al-Káid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Al-Káida	Al-Káid	k1gMnSc2	Al-Káid
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
===	===	k?	===
</s>
</p>
<p>
<s>
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
–	–	k?	–
organizace	organizace	k1gFnSc1	organizace
původně	původně	k6eAd1	původně
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
"	"	kIx"	"
<g/>
Islámský	islámský	k2eAgInSc4d1	islámský
stát	stát	k1gInSc4	stát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
"	"	kIx"	"
<g/>
Islámský	islámský	k2eAgInSc4d1	islámský
stát	stát	k1gInSc4	stát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Levantě	Levanta	k1gFnSc6	Levanta
<g/>
"	"	kIx"	"
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
působící	působící	k2eAgMnSc1d1	působící
jako	jako	k8xC	jako
odnož	odnož	k1gInSc1	odnož
al-Káidy	al-Káida	k1gFnSc2	al-Káida
na	na	k7c6	na
území	území	k1gNnSc6	území
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgFnSc1d1	aktivní
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
vůdcem	vůdce	k1gMnSc7	vůdce
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
až	až	k9	až
2006	[number]	k4	2006
Abú	abú	k1gMnPc2	abú
Músá	Músá	k1gFnSc1	Músá
Zarkáví	Zarkáví	k1gNnSc3	Zarkáví
již	již	k9	již
jako	jako	k9	jako
od	od	k7c2	od
al-Káidy	al-Káida	k1gFnSc2	al-Káida
odštěpené	odštěpený	k2eAgFnSc2d1	odštěpená
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
<g/>
,	,	kIx,	,
extrémní	extrémní	k2eAgInSc1d1	extrémní
radikalismus	radikalismus	k1gInSc1	radikalismus
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
protichůdné	protichůdný	k2eAgInPc1d1	protichůdný
zájmy	zájem	k1gInPc1	zájem
ji	on	k3xPp3gFnSc4	on
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
postavily	postavit	k5eAaPmAgFnP	postavit
proti	proti	k7c3	proti
původní	původní	k2eAgFnSc3d1	původní
"	"	kIx"	"
<g/>
umírněnější	umírněný	k2eAgFnSc3d2	umírněnější
<g/>
"	"	kIx"	"
al-Káidě	al-Káida	k1gFnSc3	al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
vysoké	vysoký	k2eAgFnSc2d1	vysoká
ambice	ambice	k1gFnSc2	ambice
ovládnout	ovládnout	k5eAaPmF	ovládnout
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Levanty	Levanta	k1gFnSc2	Levanta
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
svůj	svůj	k3xOyFgInSc4	svůj
chalífát	chalífát	k1gInSc4	chalífát
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
postavily	postavit	k5eAaPmAgFnP	postavit
i	i	k9	i
proti	proti	k7c3	proti
An-Nusře	An-Nusra	k1gFnSc3	An-Nusra
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc3d1	další
organizaci	organizace	k1gFnSc3	organizace
al-Káidy	al-Káida	k1gFnSc2	al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
IS	IS	kA	IS
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
organizace	organizace	k1gFnSc1	organizace
navzdory	navzdory	k7c3	navzdory
nepřátelství	nepřátelství	k1gNnSc3	nepřátelství
s	s	k7c7	s
al-Káidou	al-Káida	k1gFnSc7	al-Káida
nadále	nadále	k6eAd1	nadále
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
jejími	její	k3xOp3gFnPc7	její
odnožemi	odnož	k1gFnPc7	odnož
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fronta	fronta	k1gFnSc1	fronta
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
===	===	k?	===
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
a	a	k8xC	a
Katar	katar	k1gInSc1	katar
podporují	podporovat	k5eAaImIp3nP	podporovat
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc3	Sýrie
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
penězi	peníze	k1gInPc7	peníze
a	a	k8xC	a
volnou	volný	k2eAgFnSc7d1	volná
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
turecké	turecký	k2eAgNnSc4d1	turecké
území	území	k1gNnSc4	území
protivládní	protivládní	k2eAgMnPc4d1	protivládní
islamistické	islamistický	k2eAgMnPc4d1	islamistický
povstalce	povstalec	k1gMnPc4	povstalec
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
al-Káidu	al-Káida	k1gFnSc4	al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Islamisté	islamista	k1gMnPc1	islamista
z	z	k7c2	z
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
masakrují	masakrovat	k5eAaBmIp3nP	masakrovat
příslušníky	příslušník	k1gMnPc7	příslušník
syrských	syrský	k2eAgFnPc2d1	Syrská
menšin	menšina	k1gFnPc2	menšina
a	a	k8xC	a
nutí	nutit	k5eAaImIp3nS	nutit
je	on	k3xPp3gNnSc4	on
konvertovat	konvertovat	k5eAaBmF	konvertovat
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
podezřelá	podezřelý	k2eAgFnSc1d1	podezřelá
z	z	k7c2	z
chemických	chemický	k2eAgInPc2d1	chemický
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Al-Káida	Al-Káid	k1gMnSc2	Al-Káid
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
===	===	k?	===
</s>
</p>
<p>
<s>
Odnož	odnož	k1gInSc1	odnož
al-Kajdy	al-Kajda	k1gFnSc2	al-Kajda
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
osm	osm	k4xCc1	osm
tisíc	tisíc	k4xCgInSc1	tisíc
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
ovládá	ovládat	k5eAaImIp3nS	ovládat
část	část	k1gFnSc4	část
jemenského	jemenský	k2eAgNnSc2d1	jemenské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
koalice	koalice	k1gFnSc2	koalice
válčící	válčící	k2eAgFnSc2d1	válčící
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
,	,	kIx,	,
dohody	dohoda	k1gFnPc4	dohoda
s	s	k7c7	s
bojovníky	bojovník	k1gMnPc7	bojovník
al-Kajdy	al-Kajda	k1gFnSc2	al-Kajda
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
koalice	koalice	k1gFnSc2	koalice
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
hútíjským	hútíjský	k2eAgMnPc3d1	hútíjský
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
dohody	dohoda	k1gFnPc1	dohoda
umožnily	umožnit	k5eAaPmAgFnP	umožnit
bojovníkům	bojovník	k1gMnPc3	bojovník
al-Kajdy	al-Kajdy	k6eAd1	al-Kajdy
opustit	opustit	k5eAaPmF	opustit
území	území	k1gNnSc4	území
i	i	k9	i
s	s	k7c7	s
výzbrojí	výzbroj	k1gFnSc7	výzbroj
a	a	k8xC	a
bojovou	bojový	k2eAgFnSc7d1	bojová
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
AP	ap	kA	ap
analytika	analytika	k1gFnSc1	analytika
Michaela	Michaela	k1gFnSc1	Michaela
Hortona	Hortona	k1gFnSc1	Hortona
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
dobře	dobře	k6eAd1	dobře
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporou	podpora	k1gFnSc7	podpora
koalice	koalice	k1gFnSc2	koalice
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
i	i	k9	i
tamní	tamní	k2eAgFnSc3d1	tamní
odnoži	odnož	k1gFnSc3	odnož
al-Kajdy	al-Kajda	k1gFnSc2	al-Kajda
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
podpora	podpora	k1gFnSc1	podpora
SAE	SAE	kA	SAE
a	a	k8xC	a
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
USA	USA	kA	USA
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
íránský	íránský	k2eAgInSc1d1	íránský
expanzionismus	expanzionismus	k1gInSc1	expanzionismus
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
al-Kajdě	al-Kajda	k1gFnSc3	al-Kajda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Teroristické	teroristický	k2eAgFnSc2d1	teroristická
akce	akce	k1gFnSc2	akce
al-Káidy	al-Káida	k1gFnSc2	al-Káida
==	==	k?	==
</s>
</p>
<p>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
arabští	arabský	k2eAgMnPc1d1	arabský
teroristé	terorista	k1gMnPc1	terorista
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
organizační	organizační	k2eAgFnSc2d1	organizační
struktury	struktura	k1gFnSc2	struktura
al-Káidy	al-Káida	k1gFnSc2	al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
spolupráci	spolupráce	k1gFnSc3	spolupráce
těchto	tento	k3xDgFnPc2	tento
extrémistických	extrémistický	k2eAgFnPc2d1	extrémistická
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
i	i	k9	i
obtížné	obtížný	k2eAgNnSc1d1	obtížné
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc4	jaký
vedení	vedení	k1gNnSc4	vedení
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
určitým	určitý	k2eAgInSc7d1	určitý
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
atentátem	atentát	k1gInSc7	atentát
nebo	nebo	k8xC	nebo
bombovým	bombový	k2eAgInSc7d1	bombový
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
akce	akce	k1gFnPc4	akce
připisované	připisovaný	k2eAgFnSc3d1	připisovaná
al-Káidě	al-Káida	k1gFnSc3	al-Káida
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Za	za	k7c4	za
suverénně	suverénně	k6eAd1	suverénně
největší	veliký	k2eAgFnSc4d3	veliký
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
akci	akce	k1gFnSc4	akce
soudobých	soudobý	k2eAgFnPc2d1	soudobá
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
al-Káidě	al-Káida	k1gFnSc3	al-Káida
přičítána	přičítán	k2eAgFnSc1d1	přičítána
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
označit	označit	k5eAaPmF	označit
letecké	letecký	k2eAgInPc4d1	letecký
útoky	útok	k1gInPc4	útok
na	na	k7c6	na
USA	USA	kA	USA
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
19	[number]	k4	19
únosců	únosce	k1gMnPc2	únosce
<g/>
,	,	kIx,	,
napojených	napojený	k2eAgFnPc2d1	napojená
na	na	k7c6	na
al-Káidu	al-Káid	k1gInSc6	al-Káid
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
časovém	časový	k2eAgInSc6d1	časový
sledu	sled	k1gInSc6	sled
zmocnilo	zmocnit	k5eAaPmAgNnS	zmocnit
4	[number]	k4	4
velkých	velký	k2eAgNnPc2d1	velké
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
letadel	letadlo	k1gNnPc2	letadlo
byla	být	k5eAaImAgFnS	být
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
budovy	budova	k1gFnPc4	budova
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
(	(	kIx(	(
<g/>
WTC	WTC	kA	WTC
<g/>
)	)	kIx)	)
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
zasažené	zasažený	k2eAgFnPc1d1	zasažená
budovy	budova	k1gFnPc1	budova
se	se	k3xPyFc4	se
po	po	k7c6	po
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
minut	minuta	k1gFnPc2	minuta
zřítily	zřítit	k5eAaPmAgFnP	zřítit
<g/>
.	.	kIx.	.
</s>
<s>
Zřítila	zřítit	k5eAaPmAgFnS	zřítit
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
budova	budova	k1gFnSc1	budova
World	World	k1gInSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
7	[number]	k4	7
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
budovy	budova	k1gFnPc1	budova
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
poškozena	poškodit	k5eAaPmNgFnS	poškodit
pádem	pád	k1gInSc7	pád
"	"	kIx"	"
<g/>
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
letadlo	letadlo	k1gNnSc1	letadlo
bylo	být	k5eAaImAgNnS	být
navedeno	navést	k5eAaPmNgNnS	navést
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
Pentagonu	Pentagon	k1gInSc2	Pentagon
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
se	se	k3xPyFc4	se
zřítilo	zřítit	k5eAaPmAgNnS	zřítit
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
díky	díky	k7c3	díky
aktivnímu	aktivní	k2eAgInSc3d1	aktivní
odporu	odpor	k1gInSc3	odpor
cestujících	cestující	k1gMnPc2	cestující
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
akcích	akce	k1gFnPc6	akce
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přibližně	přibližně	k6eAd1	přibližně
3500	[number]	k4	3500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
se	se	k3xPyFc4	se
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
opakovaně	opakovaně	k6eAd1	opakovaně
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
<g/>
.	.	kIx.	.
<g/>
Příslušníci	příslušník	k1gMnPc1	příslušník
hnutí	hnutí	k1gNnSc2	hnutí
al-Káida	al-Káida	k1gFnSc1	al-Káida
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
teroristickými	teroristický	k2eAgInPc7d1	teroristický
bombovými	bombový	k2eAgInPc7d1	bombový
útoky	útok	k1gInPc7	útok
na	na	k7c4	na
americké	americký	k2eAgFnPc4d1	americká
ambasády	ambasáda	k1gFnPc4	ambasáda
v	v	k7c6	v
Nairobi	Nairobi	k1gNnSc6	Nairobi
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
a	a	k8xC	a
Dar	dar	k1gInSc4	dar
es-Salaamu	es-Salaam	k1gInSc2	es-Salaam
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
útocích	útok	k1gInPc6	útok
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
5	[number]	k4	5
000	[number]	k4	000
bylo	být	k5eAaImAgNnS	být
zraněno	zraněn	k2eAgNnSc1d1	zraněno
<g/>
.	.	kIx.	.
<g/>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
bombovými	bombový	k2eAgInPc7d1	bombový
útoky	útok	k1gInPc7	útok
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
a	a	k8xC	a
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
americké	americký	k2eAgInPc4d1	americký
vojenské	vojenský	k2eAgInPc4d1	vojenský
objekty	objekt	k1gInPc4	objekt
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
tehdy	tehdy	k6eAd1	tehdy
kolem	kolem	k7c2	kolem
10	[number]	k4	10
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
některých	některý	k3yIgMnPc2	některý
novinářů	novinář	k1gMnPc2	novinář
však	však	k9	však
za	za	k7c7	za
těmito	tento	k3xDgInPc7	tento
útoky	útok	k1gInPc7	útok
stálo	stát	k5eAaImAgNnS	stát
hnutí	hnutí	k1gNnSc1	hnutí
Hizballáh	Hizballáh	k1gInSc1	Hizballáh
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
sebevražedném	sebevražedný	k2eAgInSc6d1	sebevražedný
pumovém	pumový	k2eAgInSc6d1	pumový
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
torpédoborec	torpédoborec	k1gInSc4	torpédoborec
USS	USS	kA	USS
Cole	cola	k1gFnSc3	cola
v	v	k7c6	v
jemenském	jemenský	k2eAgInSc6d1	jemenský
přístavu	přístav	k1gInSc6	přístav
Aden	Aden	k1gInSc1	Aden
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2000	[number]	k4	2000
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
celkem	celkem	k6eAd1	celkem
17	[number]	k4	17
amerických	americký	k2eAgMnPc2d1	americký
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
39	[number]	k4	39
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
oba	dva	k4xCgMnPc1	dva
atentátníci	atentátník	k1gMnPc1	atentátník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
napadli	napadnout	k5eAaPmAgMnP	napadnout
americkou	americký	k2eAgFnSc4d1	americká
loď	loď	k1gFnSc4	loď
ve	v	k7c6	v
člunu	člun	k1gInSc6	člun
naloženém	naložený	k2eAgInSc6d1	naložený
výbušninami	výbušnina	k1gFnPc7	výbušnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
Usáma	Usáma	k1gNnSc1	Usáma
bin	bin	k?	bin
Ládin	Ládin	k2eAgMnSc1d1	Ládin
</s>
</p>
<p>
<s>
Ajmán	Ajmán	k1gMnSc1	Ajmán
Zavahrí	Zavahrí	k1gMnSc1	Zavahrí
</s>
</p>
<p>
<s>
Abú	abú	k1gMnSc1	abú
Músá	Músá	k1gFnSc1	Músá
Zarkáví	Zarkáví	k1gNnSc6	Zarkáví
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŘEHKA	ŘEHKA	kA	ŘEHKA
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgFnSc1d1	informační
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-2770-2	[number]	k4	978-80-200-2770-2
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Al-Káida	Al-Káid	k1gMnSc2	Al-Káid
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Audiovizuální	audiovizuální	k2eAgInPc1d1	audiovizuální
dokumenty	dokument	k1gInPc1	dokument
===	===	k?	===
</s>
</p>
<p>
<s>
Fabled	Fabled	k1gMnSc1	Fabled
Enemies	Enemies	k1gMnSc1	Enemies
<g/>
,	,	kIx,	,
102	[number]	k4	102
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
znění	znění	k1gNnSc6	znění
</s>
</p>
