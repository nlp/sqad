<s>
Jako	jako	k8xS	jako
pogromy	pogrom	k1gInPc1	pogrom
(	(	kIx(	(
<g/>
z	z	k7c2	z
ruského	ruský	k2eAgMnSc2d1	ruský
"	"	kIx"	"
<g/>
gromiť	gromiť	k1gFnSc1	gromiť
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
г	г	k?	г
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
pustošit	pustošit	k5eAaImF	pustošit
<g/>
,	,	kIx,	,
drtit	drtit	k5eAaImF	drtit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
významu	význam	k1gInSc6	význam
označovány	označován	k2eAgInPc4d1	označován
rasové	rasový	k2eAgInPc4d1	rasový
nepokoje	nepokoj	k1gInPc4	nepokoj
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
proti	proti	k7c3	proti
židům	žid	k1gMnPc3	žid
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
docházelo	docházet	k5eAaImAgNnS	docházet
často	často	k6eAd1	často
s	s	k7c7	s
vládní	vládní	k2eAgFnSc7d1	vládní
podporou	podpora	k1gFnSc7	podpora
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
může	moct	k5eAaImIp3nS	moct
pojem	pojem	k1gInSc1	pojem
označovat	označovat	k5eAaImF	označovat
obecně	obecně	k6eAd1	obecně
krvavé	krvavý	k2eAgNnSc1d1	krvavé
masové	masový	k2eAgNnSc1d1	masové
pronásledování	pronásledování	k1gNnSc1	pronásledování
<g/>
,	,	kIx,	,
vraždění	vraždění	k1gNnSc1	vraždění
skupiny	skupina	k1gFnSc2	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
jiného	jiný	k2eAgNnSc2d1	jiné
náboženského	náboženský	k2eAgNnSc2d1	náboženské
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
nebo	nebo	k8xC	nebo
příslušníků	příslušník	k1gMnPc2	příslušník
jiné	jiný	k2eAgFnSc2d1	jiná
etnické	etnický	k2eAgFnSc2d1	etnická
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
nazývat	nazývat	k5eAaImF	nazývat
tzv.	tzv.	kA	tzv.
náhony	náhon	k1gInPc4	náhon
(	(	kIx(	(
<g/>
nadběhnutí	nadběhnutí	k1gNnSc1	nadběhnutí
více	hodně	k6eAd2	hodně
pogromských	pogromský	k2eAgMnPc2d1	pogromský
přívrženců	přívrženec	k1gMnPc2	přívrženec
proti	proti	k7c3	proti
židům	žid	k1gMnPc3	žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pogromy	pogrom	k1gInPc1	pogrom
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
během	během	k7c2	během
masové	masový	k2eAgFnSc2d1	masová
vlny	vlna	k1gFnSc2	vlna
protižidovských	protižidovský	k2eAgFnPc2d1	protižidovská
bouří	bouř	k1gFnPc2	bouř
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
přehnaly	přehnat	k5eAaPmAgFnP	přehnat
západními	západní	k2eAgFnPc7d1	západní
oblastmi	oblast	k1gFnPc7	oblast
Ruského	ruský	k2eAgInSc2d1	ruský
impéria	impérium	k1gNnSc2	impérium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byli	být	k5eAaImAgMnP	být
židé	žid	k1gMnPc1	žid
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
obviněni	obvinit	k5eAaPmNgMnP	obvinit
ze	z	k7c2	z
zavraždění	zavraždění	k1gNnSc2	zavraždění
cara	car	k1gMnSc2	car
Alexandra	Alexandr	k1gMnSc2	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
propuknutí	propuknutí	k1gNnSc6	propuknutí
pogromů	pogrom	k1gInPc2	pogrom
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
166	[number]	k4	166
městech	město	k1gNnPc6	město
zničeny	zničit	k5eAaPmNgFnP	zničit
tisíce	tisíc	k4xCgInPc1	tisíc
židovských	židovský	k2eAgMnPc2d1	židovský
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
celé	celý	k2eAgFnPc1d1	celá
rodiny	rodina	k1gFnPc1	rodina
byly	být	k5eAaImAgFnP	být
uvrženy	uvrhnout	k5eAaPmNgFnP	uvrhnout
do	do	k7c2	do
krajní	krajní	k2eAgFnSc2d1	krajní
bídy	bída	k1gFnSc2	bída
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
znásilňování	znásilňování	k1gNnSc3	znásilňování
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
i	i	k8xC	i
dětí	dítě	k1gFnPc2	dítě
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
nebo	nebo	k8xC	nebo
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejhoršímu	zlý	k2eAgInSc3d3	Nejhorší
pogromu	pogrom	k1gInSc3	pogrom
došlo	dojít	k5eAaPmAgNnS	dojít
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1881	[number]	k4	1881
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
,	,	kIx,	,
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
velkým	velký	k2eAgInPc3d1	velký
pogromům	pogrom	k1gInPc3	pogrom
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
a	a	k8xC	a
v	v	k7c6	v
Oděse	Oděsa	k1gFnSc6	Oděsa
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
nepokojů	nepokoj	k1gInPc2	nepokoj
obvinil	obvinit	k5eAaPmAgMnS	obvinit
židy	žid	k1gMnPc4	žid
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
omezení	omezení	k1gNnSc3	omezení
soubor	soubor	k1gInSc4	soubor
tvrdých	tvrdý	k2eAgNnPc2d1	tvrdé
nařízení	nařízení	k1gNnPc2	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
Pogromy	pogrom	k1gInPc1	pogrom
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
za	za	k7c4	za
mlčenlivé	mlčenlivý	k2eAgFnPc4d1	mlčenlivá
nečinnosti	nečinnost	k1gFnPc4	nečinnost
úřadů	úřad	k1gInPc2	úřad
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
krvavější	krvavý	k2eAgFnSc1d2	krvavější
vlna	vlna	k1gFnSc1	vlna
pogromů	pogrom	k1gInPc2	pogrom
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
za	za	k7c4	za
sebou	se	k3xPyFc7	se
nechala	nechat	k5eAaPmAgFnS	nechat
přibližně	přibližně	k6eAd1	přibližně
2000	[number]	k4	2000
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
zraněných	zraněný	k2eAgMnPc2d1	zraněný
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
k	k	k7c3	k
887	[number]	k4	887
pogromům	pogrom	k1gInPc3	pogrom
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
židovských	židovský	k2eAgFnPc2d1	židovská
obětí	oběť	k1gFnPc2	oběť
mezi	mezi	k7c7	mezi
70-250	[number]	k4	70-250
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
ozbrojenci	ozbrojenec	k1gMnPc1	ozbrojenec
bojovníka	bojovník	k1gMnSc2	bojovník
za	za	k7c4	za
Ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
národní	národní	k2eAgFnSc4d1	národní
republiku	republika	k1gFnSc4	republika
Simona	Simon	k1gMnSc2	Simon
Petljury	Petljura	k1gFnSc2	Petljura
<g/>
,	,	kIx,	,
25	[number]	k4	25
%	%	kIx~	%
Zelená	zelený	k2eAgFnSc1d1	zelená
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
nacionalistické	nacionalistický	k2eAgInPc1d1	nacionalistický
a	a	k8xC	a
anarchistické	anarchistický	k2eAgInPc1d1	anarchistický
bandy	band	k1gInPc1	band
<g/>
,	,	kIx,	,
17	[number]	k4	17
%	%	kIx~	%
bělogvardějci	bělogvardějec	k1gMnPc1	bělogvardějec
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
oddíly	oddíl	k1gInPc4	oddíl
Antona	Anton	k1gMnSc2	Anton
Děnikina	Děnikin	k1gMnSc2	Děnikin
<g/>
)	)	kIx)	)
a	a	k8xC	a
8,5	[number]	k4	8,5
%	%	kIx~	%
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pogromům	pogrom	k1gInPc3	pogrom
také	také	k9	také
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Argentině	Argentina	k1gFnSc6	Argentina
a	a	k8xC	a
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Krajně	krajně	k6eAd1	krajně
ničivé	ničivý	k2eAgInPc1d1	ničivý
pogromy	pogrom	k1gInPc1	pogrom
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
rumunskému	rumunský	k2eAgInSc3d1	rumunský
pogromu	pogrom	k1gInSc3	pogrom
v	v	k7c6	v
Iaşi	Iaş	k1gFnSc6	Iaş
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
14	[number]	k4	14
000	[number]	k4	000
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
pogromu	pogrom	k1gInSc3	pogrom
v	v	k7c6	v
Jedwabnem	Jedwabn	k1gInSc7	Jedwabn
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
s	s	k7c7	s
380	[number]	k4	380
-	-	kIx~	-
1600	[number]	k4	1600
oběťmi	oběť	k1gFnPc7	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
velký	velký	k2eAgInSc1d1	velký
pogrom	pogrom	k1gInSc1	pogrom
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
poválečný	poválečný	k2eAgInSc1d1	poválečný
pogrom	pogrom	k1gInSc1	pogrom
v	v	k7c6	v
Kielcích	Kielek	k1gInPc6	Kielek
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
