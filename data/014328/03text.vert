<s>
Nivské	Nivský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaNivské	památkaNivský	k2eAgFnPc4d1
loukyIUCN	loukyIUCN	k?
kategorie	kategorie	k1gFnPc4
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
Horní	horní	k2eAgFnSc1d1
rybníkZákladní	rybníkZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1989	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgInS
</s>
<s>
Krajský	krajský	k2eAgInSc4d1
úřad	úřad	k1gInSc4
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
580	#num#	k4
-	-	kIx~
580	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
4,59	4,59	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Prostějov	Prostějov	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Niva	niva	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
46,55	46,55	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
32,52	32,52	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Nivské	Nivský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
1214	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Nivské	Nivský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
ev.	ev.	k?
č.	č.	k?
1214	#num#	k4
poblíž	poblíž	k7c2
obce	obec	k1gFnSc2
Niva	niva	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Prostějov	Prostějov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
spravuje	spravovat	k5eAaImIp3nS
Krajský	krajský	k2eAgInSc4d1
úřad	úřad	k1gInSc4
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Předmět	předmět	k1gInSc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
mokřad	mokřad	k1gInSc1
s	s	k7c7
vlhkomilnou	vlhkomilný	k2eAgFnSc7d1
flórou	flóra	k1gFnSc7
a	a	k8xC
faunou	fauna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Územím	území	k1gNnSc7
protéká	protékat	k5eAaImIp3nS
potok	potok	k1gInSc1
Bílá	bílý	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
napájí	napájet	k5eAaImIp3nS
uvnitř	uvnitř	k7c2
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
ležící	ležící	k2eAgInSc1d1
Horní	horní	k2eAgInSc1d1
rybník	rybník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
