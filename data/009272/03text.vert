<p>
<s>
Libia	Libium	k1gNnPc4	Libium
byl	být	k5eAaImAgInS	být
chráněný	chráněný	k2eAgInSc1d1	chráněný
křižník	křižník	k1gInSc1	křižník
italského	italský	k2eAgNnSc2d1	italské
královského	královský	k2eAgNnSc2d1	královské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
rozestavěn	rozestavět	k5eAaPmNgInS	rozestavět
pro	pro	k7c4	pro
osmanské	osmanský	k2eAgNnSc4d1	osmanské
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
italsko-turecké	italskourecký	k2eAgFnSc2d1	italsko-turecký
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
zabaven	zabavit	k5eAaPmNgInS	zabavit
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
byl	být	k5eAaImAgMnS	být
zasazen	zasazen	k2eAgMnSc1d1	zasazen
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Osmanské	osmanský	k2eAgNnSc1d1	osmanské
mámořnictvo	mámořnictvo	k1gNnSc1	mámořnictvo
objednalo	objednat	k5eAaPmAgNnS	objednat
stavbu	stavba	k1gFnSc4	stavba
křižníku	křižník	k1gInSc2	křižník
Drama	dramo	k1gNnSc2	dramo
u	u	k7c2	u
loděnice	loděnice	k1gFnSc2	loděnice
Ansaldo	Ansaldo	k1gNnSc1	Ansaldo
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
<g/>
.	.	kIx.	.
</s>
<s>
Stavělo	stavět	k5eAaImAgNnS	stavět
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
britského	britský	k2eAgInSc2d1	britský
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
poněkud	poněkud	k6eAd1	poněkud
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
<g/>
.	.	kIx.	.
</s>
<s>
Plavidlo	plavidlo	k1gNnSc1	plavidlo
mělo	mít	k5eAaImAgNnS	mít
konstrukčně	konstrukčně	k6eAd1	konstrukčně
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
předcházejícím	předcházející	k2eAgInPc3d1	předcházející
křižníkům	křižník	k1gInPc3	křižník
Mecidiye	Mecidiy	k1gFnSc2	Mecidiy
a	a	k8xC	a
Hamidiye	Hamidiy	k1gFnSc2	Hamidiy
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
italsko-turecké	italskourecký	k2eAgFnSc2d1	italsko-turecký
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
rozestavěný	rozestavěný	k2eAgInSc4d1	rozestavěný
křižník	křižník	k1gInSc4	křižník
zabaven	zabaven	k2eAgInSc4d1	zabaven
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1912	[number]	k4	1912
a	a	k8xC	a
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
přijat	přijat	k2eAgInSc4d1	přijat
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1913	[number]	k4	1913
pod	pod	k7c7	pod
novým	nový	k2eAgNnSc7d1	nové
jménem	jméno	k1gNnSc7	jméno
Libia	Libium	k1gNnSc2	Libium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Křižník	křižník	k1gInSc1	křižník
byl	být	k5eAaImAgInS	být
vyzbrojen	vyzbrojit	k5eAaPmNgInS	vyzbrojit
dvěma	dva	k4xCgInPc7	dva
152	[number]	k4	152
<g/>
mm	mm	kA	mm
kanóny	kanón	k1gInPc4	kanón
(	(	kIx(	(
<g/>
odstraněny	odstranit	k5eAaPmNgInP	odstranit
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osmi	osm	k4xCc2	osm
120	[number]	k4	120
<g/>
mm	mm	kA	mm
kanóny	kanón	k1gInPc1	kanón
<g/>
,	,	kIx,	,
osmi	osm	k4xCc2	osm
47	[number]	k4	47
<g/>
mm	mm	kA	mm
kanóny	kanón	k1gInPc1	kanón
<g/>
,	,	kIx,	,
šesti	šest	k4xCc6	šest
37	[number]	k4	37
<g/>
mm	mm	kA	mm
kanóny	kanón	k1gInPc4	kanón
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
450	[number]	k4	450
<g/>
mm	mm	kA	mm
torpédomety	torpédomet	k1gInPc1	torpédomet
<g/>
.	.	kIx.	.
</s>
<s>
Pohonný	pohonný	k2eAgInSc1d1	pohonný
systém	systém	k1gInSc1	systém
měl	mít	k5eAaImAgInS	mít
výkon	výkon	k1gInSc4	výkon
11	[number]	k4	11
530	[number]	k4	530
hp	hp	k?	hp
<g/>
.	.	kIx.	.
</s>
<s>
Tvořily	tvořit	k5eAaImAgFnP	tvořit
ho	on	k3xPp3gMnSc4	on
čtyři	čtyři	k4xCgInPc4	čtyři
parní	parní	k2eAgInPc4d1	parní
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
16	[number]	k4	16
kotlů	kotel	k1gInPc2	kotel
<g/>
,	,	kIx,	,
pohánějící	pohánějící	k2eAgInPc4d1	pohánějící
dva	dva	k4xCgInPc4	dva
lodní	lodní	k2eAgInPc4d1	lodní
šrouby	šroub	k1gInPc4	šroub
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rychlost	rychlost	k1gFnSc1	rychlost
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
23	[number]	k4	23
uzlů	uzel	k1gInPc2	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Dosah	dosah	k1gInSc1	dosah
byl	být	k5eAaImAgInS	být
3150	[number]	k4	3150
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
při	při	k7c6	při
rychlosti	rychlost	k1gFnSc6	rychlost
10	[number]	k4	10
uzlů	uzel	k1gInPc2	uzel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Služba	služba	k1gFnSc1	služba
==	==	k?	==
</s>
</p>
<p>
<s>
Křižník	křižník	k1gInSc1	křižník
byl	být	k5eAaImAgInS	být
nasazen	nasadit	k5eAaPmNgInS	nasadit
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazen	vyřadit	k5eAaPmNgInS	vyřadit
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Libia	Libium	k1gNnSc2	Libium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
