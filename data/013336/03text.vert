<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1584	[number]	k4	1584
<g/>
,	,	kIx,	,
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1611	[number]	k4	1611
San	San	k1gFnPc2	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
de	de	k?	de
El	Ela	k1gFnPc2	Ela
Escorial	Escorial	k1gMnSc1	Escorial
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
manželka	manželka	k1gFnSc1	manželka
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Španělského	španělský	k2eAgInSc2d1	španělský
španělská	španělský	k2eAgNnPc1d1	španělské
<g/>
,	,	kIx,	,
portugalská	portugalský	k2eAgNnPc1d1	portugalské
<g/>
,	,	kIx,	,
neapolská	neapolský	k2eAgNnPc1d1	Neapolské
a	a	k8xC	a
sicilská	sicilský	k2eAgFnSc1d1	sicilská
královna	královna	k1gFnSc1	královna
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
habsburské	habsburský	k2eAgFnSc2d1	habsburská
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
dcera	dcera	k1gFnSc1	dcera
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štýrského	štýrský	k2eAgMnSc2d1	štýrský
a	a	k8xC	a
Marie	Maria	k1gFnPc4	Maria
Anny	Anna	k1gFnSc2	Anna
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
Markéty	Markéta	k1gFnSc2	Markéta
a	a	k8xC	a
mladého	mladý	k2eAgMnSc2d1	mladý
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
r.	r.	kA	r.
1599	[number]	k4	1599
a	a	k8xC	a
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
spokojené	spokojený	k2eAgNnSc1d1	spokojené
<g/>
.	.	kIx.	.
</s>
<s>
Protivníkem	protivník	k1gMnSc7	protivník
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
zajímající	zajímající	k2eAgFnSc4d1	zajímající
se	se	k3xPyFc4	se
o	o	k7c4	o
vládnutí	vládnutí	k1gNnSc4	vládnutí
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
králův	králův	k2eAgMnSc1d1	králův
favorit	favorit	k1gMnSc1	favorit
Francisco	Francisco	k1gMnSc1	Francisco
Goméz	Goméza	k1gFnPc2	Goméza
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
<g/>
.	.	kIx.	.
</s>
<s>
Markétě	Markéta	k1gFnSc3	Markéta
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Lermy	Lermy	k?	Lermy
díky	díky	k7c3	díky
intrikám	intrika	k1gFnPc3	intrika
vzdálit	vzdálit	k5eAaPmF	vzdálit
od	od	k7c2	od
královského	královský	k2eAgMnSc2d1	královský
chotě	choť	k1gMnSc2	choť
a	a	k8xC	a
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc4	její
snahu	snaha	k1gFnSc4	snaha
překazila	překazit	k5eAaPmAgFnS	překazit
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
svého	své	k1gNnSc2	své
osmého	osmý	k4xOgNnSc2	osmý
dítěte	dítě	k1gNnSc2	dítě
(	(	kIx(	(
<g/>
Alfons	Alfons	k1gMnSc1	Alfons
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
se	se	k3xPyFc4	se
už	už	k6eAd1	už
znovu	znovu	k6eAd1	znovu
neoženil	oženit	k5eNaPmAgMnS	oženit
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
Markéty	Markéta	k1gFnSc2	Markéta
a	a	k8xC	a
Filipa	Filip	k1gMnSc4	Filip
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
osm	osm	k4xCc1	osm
potomků	potomek	k1gMnPc2	potomek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
–	–	k?	–
<g/>
1666	[number]	k4	1666
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1615	[number]	k4	1615
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
–	–	k?	–
<g/>
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
<g/>
–	–	k?	–
<g/>
1665	[number]	k4	1665
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
<g/>
∞	∞	k?	∞
1	[number]	k4	1
<g/>
/	/	kIx~	/
1615	[number]	k4	1615
princezna	princezna	k1gFnSc1	princezna
Izabela	Izabela	k1gFnSc1	Izabela
Bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
(	(	kIx(	(
<g/>
1602	[number]	k4	1602
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
∞	∞	k?	∞
2	[number]	k4	2
<g/>
/	/	kIx~	/
1649	[number]	k4	1649
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
(	(	kIx(	(
<g/>
1634	[number]	k4	1634
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
<g/>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
1606	[number]	k4	1606
<g/>
–	–	k?	–
<g/>
1646	[number]	k4	1646
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1631	[number]	k4	1631
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
<g/>
–	–	k?	–
<g/>
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1607	[number]	k4	1607
<g/>
–	–	k?	–
<g/>
1632	[number]	k4	1632
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
–	–	k?	–
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nozozemský	nozozemský	k2eAgMnSc1d1	nozozemský
místodržitel	místodržitel	k1gMnSc1	místodržitel
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
</s>
</p>
<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Františka	František	k1gMnSc2	František
(	(	kIx(	(
<g/>
1610	[number]	k4	1610
<g/>
–	–	k?	–
<g/>
1617	[number]	k4	1617
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mořic	Mořic	k1gMnSc1	Mořic
(	(	kIx(	(
<g/>
1611	[number]	k4	1611
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc4	vývod
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Markéta	Markéta	k1gFnSc1	Markéta
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
