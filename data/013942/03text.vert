<s>
Humason	Humason	k1gInSc1
(	(	kIx(
<g/>
kráter	kráter	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kráter	kráter	k1gInSc1
HumasonSouřadnice	HumasonSouřadnice	k1gFnSc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
Selenografická	Selenografický	k2eAgFnSc1d1
šířka	šířka	k1gFnSc1
</s>
<s>
30,7	30,7	k4
<g/>
°	°	k?
S	s	k7c7
Selenografická	Selenografický	k2eAgNnPc4d1
délka	délka	k1gFnSc1
</s>
<s>
56,6	56,6	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
Humason	Humason	k1gMnSc1
</s>
<s>
Humason	Humason	k1gInSc1
<g/>
,	,	kIx,
Měsíc	měsíc	k1gInSc1
Další	další	k2eAgInSc1d1
údaje	údaj	k1gInSc2
Průměr	průměr	k1gInSc1
kráteru	kráter	k1gInSc2
</s>
<s>
4	#num#	k4
km	km	kA
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hloubka	hloubka	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
Colongitudo	Colongitudo	k1gNnSc1
</s>
<s>
57	#num#	k4
<g/>
°	°	k?
Eponym	eponym	k1gMnSc1
</s>
<s>
Milton	Milton	k1gInSc1
L.	L.	kA
Humason	Humason	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
kráteru	kráter	k1gInSc2
Humason	Humasona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Humason	Humason	k1gInSc1
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
impaktní	impaktní	k2eAgInSc1d1
kráter	kráter	k1gInSc1
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
Oceánu	oceán	k1gInSc6
bouří	bouř	k1gFnPc2
(	(	kIx(
<g/>
Oceanus	Oceanus	k1gInSc1
Procellarum	Procellarum	k1gNnSc1
<g/>
)	)	kIx)
na	na	k7c6
přivrácené	přivrácený	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
průměr	průměr	k1gInSc4
pouhé	pouhý	k2eAgInPc4d1
4	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
jej	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
Mezinárodní	mezinárodní	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
pojmenovala	pojmenovat	k5eAaPmAgFnS
na	na	k7c4
současný	současný	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
,	,	kIx,
nesl	nést	k5eAaImAgMnS
označení	označení	k1gNnSc4
Lichtenberg	Lichtenberg	k1gMnSc1
G.	G.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Západně	západně	k6eAd1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
směrem	směr	k1gInSc7
k	k	k7c3
jihu	jih	k1gInSc3
hřeben	hřeben	k1gInSc4
Dorsa	Dors	k1gMnSc2
Whiston	Whiston	k1gInSc4
<g/>
,	,	kIx,
jihovýchodně	jihovýchodně	k6eAd1
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
dlouhé	dlouhý	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
Montes	Montesa	k1gFnPc2
Agricola	Agricola	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
podle	podle	k7c2
amerického	americký	k2eAgMnSc2d1
astronoma	astronom	k1gMnSc2
Miltona	Milton	k1gMnSc2
L.	L.	kA
Humasona	Humason	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Antonín	Antonín	k1gMnSc1
Rükl	Rükl	k1gMnSc1
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
Aventinum	Aventinum	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kapitola	kapitola	k1gFnSc1
Rümker	Rümker	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
42	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
mapového	mapový	k2eAgInSc2d1
listu	list	k1gInSc2
8	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
↑	↑	k?
Crater	Crater	k1gMnSc1
Humason	Humason	k1gMnSc1
on	on	k3xPp3gMnSc1
Moon	Moon	k1gMnSc1
Gazetteer	Gazetteer	k1gMnSc1
of	of	k?
Planetary	Planetara	k1gFnSc2
Nomenclature	Nomenclatur	k1gMnSc5
<g/>
,	,	kIx,
IAU	IAU	kA
<g/>
,	,	kIx,
USGS	USGS	kA
<g/>
,	,	kIx,
NASA	NASA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
RÜKL	RÜKL	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Kráter	kráter	k1gInSc1
Humason	Humason	k1gInSc1
<g/>
,	,	kIx,
Wikispaces	Wikispaces	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LAC	LAC	kA
38	#num#	k4
<g/>
,	,	kIx,
mapa	mapa	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
000	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
Lambertova	Lambertův	k2eAgFnSc1d1
projekce	projekce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Planetární	planetární	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
</s>
