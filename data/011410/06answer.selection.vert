<s>
Společenský	společenský	k2eAgInSc1d1	společenský
oblek	oblek	k1gInSc1	oblek
<g/>
,	,	kIx,	,
také	také	k9	také
nazýván	nazývat	k5eAaImNgInS	nazývat
formální	formální	k2eAgInSc1d1	formální
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
šedou	šedý	k2eAgFnSc4d1	šedá
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
modrou	modrý	k2eAgFnSc7d1	modrá
pro	pro	k7c4	pro
nošení	nošení	k1gNnSc4	nošení
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
hnědou	hnědý	k2eAgFnSc4d1	hnědá
nebo	nebo	k8xC	nebo
olivovou	olivový	k2eAgFnSc4d1	olivová
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
</s>
