<p>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
Brněnský	brněnský	k2eAgInSc1d1	brněnský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
územně	územně	k6eAd1	územně
samosprávný	samosprávný	k2eAgInSc4d1	samosprávný
celek	celek	k1gInSc4	celek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
7195	[number]	k4	7195
km2	km2	k4	km2
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
sedmi	sedm	k4xCc7	sedm
okresy	okres	k1gInPc7	okres
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
územního	územní	k2eAgInSc2d1	územní
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
okresy	okres	k1gInPc1	okres
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
Brno-město	Brnoěsta	k1gFnSc5	Brno-města
<g/>
,	,	kIx,	,
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
,	,	kIx,	,
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
Vyškov	Vyškov	k1gInSc1	Vyškov
a	a	k8xC	a
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Samosprávný	samosprávný	k2eAgInSc1d1	samosprávný
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Jihočeským	jihočeský	k2eAgInSc7d1	jihočeský
krajem	kraj	k1gInSc7	kraj
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Krajem	kraj	k1gInSc7	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Pardubickým	pardubický	k2eAgInSc7d1	pardubický
krajem	kraj	k1gInSc7	kraj
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Olomouckým	olomoucký	k2eAgInSc7d1	olomoucký
krajem	kraj	k1gInSc7	kraj
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	s	k7c7	s
Zlínským	zlínský	k2eAgInSc7d1	zlínský
krajem	kraj	k1gInSc7	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
krajská	krajský	k2eAgFnSc1d1	krajská
hranice	hranice	k1gFnSc1	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
zároveň	zároveň	k6eAd1	zároveň
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
(	(	kIx(	(
<g/>
země	zem	k1gFnSc2	zem
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovenskem	Slovensko	k1gNnSc7	Slovensko
(	(	kIx(	(
<g/>
Trenčínský	trenčínský	k2eAgInSc4d1	trenčínský
a	a	k8xC	a
Trnavský	trnavský	k2eAgInSc4d1	trnavský
kraj	kraj	k1gInSc4	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
1,188	[number]	k4	1,188
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
672	[number]	k4	672
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
50	[number]	k4	50
měst	město	k1gNnPc2	město
a	a	k8xC	a
39	[number]	k4	39
městysů	městys	k1gInPc2	městys
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
vojenský	vojenský	k2eAgInSc1d1	vojenský
újezd	újezd	k1gInSc1	újezd
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
historickém	historický	k2eAgNnSc6d1	historické
území	území	k1gNnSc6	území
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
osada	osada	k1gFnSc1	osada
Jobova	Jobův	k2eAgFnSc1d1	Jobova
Lhota	Lhota	k1gFnSc1	Lhota
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
historickou	historický	k2eAgFnSc4d1	historická
součást	součást	k1gFnSc4	součást
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
a	a	k8xC	a
území	území	k1gNnSc4	území
Valticka	Valticko	k1gNnSc2	Valticko
a	a	k8xC	a
Dyjského	dyjský	k2eAgInSc2d1	dyjský
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
součástí	součást	k1gFnPc2	součást
Dolních	dolní	k2eAgInPc2d1	dolní
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
západ	západ	k1gInSc4	západ
a	a	k8xC	a
severozápad	severozápad	k1gInSc4	severozápad
kraje	kraj	k1gInSc2	kraj
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
výběžky	výběžek	k1gInPc1	výběžek
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Drahanská	Drahanský	k2eAgFnSc1d1	Drahanská
vrchovina	vrchovina	k1gFnSc1	vrchovina
s	s	k7c7	s
Moravským	moravský	k2eAgInSc7d1	moravský
krasem	kras	k1gInSc7	kras
<g/>
,	,	kIx,	,
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
už	už	k6eAd1	už
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
západních	západní	k2eAgFnPc2d1	západní
vrchovin	vrchovina	k1gFnPc2	vrchovina
odděleny	oddělen	k2eAgMnPc4d1	oddělen
Dolnomoravským	dolnomoravský	k2eAgInSc7d1	dolnomoravský
úvalem	úval	k1gInSc7	úval
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kraj	kraj	k1gInSc1	kraj
dělí	dělit	k5eAaImIp3nS	dělit
se	s	k7c7	s
Zlínským	zlínský	k2eAgInSc7d1	zlínský
krajem	kraj	k1gInSc7	kraj
i	i	k8xC	i
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Trojmezí	trojmezí	k1gNnSc4	trojmezí
těchto	tento	k3xDgInPc2	tento
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
838	[number]	k4	838
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
nejvýše	vysoce	k6eAd3	vysoce
položeným	položený	k2eAgInSc7d1	položený
bodem	bod	k1gInSc7	bod
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nedaleko	nedaleko	k7c2	nedaleko
vrcholu	vrchol	k1gInSc2	vrchol
Durda	Durdo	k1gNnSc2	Durdo
(	(	kIx(	(
<g/>
842	[number]	k4	842
m	m	kA	m
<g/>
)	)	kIx)	)
ležícího	ležící	k2eAgMnSc2d1	ležící
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
vrcholu	vrchol	k1gInSc2	vrchol
geomorfologického	geomorfologický	k2eAgInSc2d1	geomorfologický
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
Javořina	Javořina	k1gFnSc1	Javořina
/	/	kIx~	/
Veľká	Veľký	k2eAgFnSc1d1	Veľká
Javorina	Javorina	k1gFnSc1	Javorina
(	(	kIx(	(
<g/>
970	[number]	k4	970
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
Čupec	čupec	k1gInSc1	čupec
(	(	kIx(	(
<g/>
819	[number]	k4	819
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc2d1	ležící
taktéž	taktéž	k?	taktéž
u	u	k7c2	u
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Durdy	Durda	k1gFnSc2	Durda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
kraj	kraj	k1gInSc1	kraj
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
k	k	k7c3	k
povodí	povodí	k1gNnSc3	povodí
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
vody	voda	k1gFnPc1	voda
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
odvádí	odvádět	k5eAaImIp3nS	odvádět
řeka	řeka	k1gFnSc1	řeka
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nížině	nížina	k1gFnSc6	nížina
u	u	k7c2	u
česko-slovensko-rakouského	českolovenskoakouský	k2eAgNnSc2d1	česko-slovensko-rakouský
trojmezí	trojmezí	k1gNnSc2	trojmezí
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přidávají	přidávat	k5eAaImIp3nP	přidávat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
důležité	důležitý	k2eAgFnPc1d1	důležitá
řeky	řeka	k1gFnPc1	řeka
regionu	region	k1gInSc2	region
<g/>
:	:	kIx,	:
Dyje	Dyje	k1gFnSc1	Dyje
<g/>
,	,	kIx,	,
Svratka	Svratka	k1gFnSc1	Svratka
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
přítok	přítok	k1gInSc4	přítok
Svitava	Svitava	k1gFnSc1	Svitava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Podyjí	Podyjí	k1gNnSc2	Podyjí
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
tři	tři	k4xCgFnPc1	tři
chráněné	chráněný	k2eAgFnPc1d1	chráněná
krajinné	krajinný	k2eAgFnPc1d1	krajinná
oblasti	oblast	k1gFnPc1	oblast
<g/>
:	:	kIx,	:
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
území	území	k1gNnSc6	území
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
a	a	k8xC	a
Pálava	Pálava	k1gFnSc1	Pálava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
vymezeno	vymezit	k5eAaPmNgNnS	vymezit
územím	území	k1gNnSc7	území
sedmi	sedm	k4xCc2	sedm
okresů	okres	k1gInPc2	okres
<g/>
:	:	kIx,	:
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
Brno-město	Brnoěsta	k1gFnSc5	Brno-města
<g/>
,	,	kIx,	,
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
,	,	kIx,	,
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
Vyškov	Vyškov	k1gInSc1	Vyškov
a	a	k8xC	a
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2003	[number]	k4	2003
zanikly	zaniknout	k5eAaPmAgInP	zaniknout
okresní	okresní	k2eAgInPc1d1	okresní
úřady	úřad	k1gInPc1	úřad
a	a	k8xC	a
samosprávné	samosprávný	k2eAgInPc1d1	samosprávný
kraje	kraj	k1gInPc1	kraj
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
správní	správní	k2eAgInPc4d1	správní
obvody	obvod	k1gInPc4	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
okresních	okresní	k2eAgNnPc2d1	okresní
měst	město	k1gNnPc2	město
Blanska	Blansko	k1gNnSc2	Blansko
<g/>
,	,	kIx,	,
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Břeclavi	Břeclav	k1gFnSc2	Břeclav
<g/>
,	,	kIx,	,
Hodonína	Hodonín	k1gInSc2	Hodonín
<g/>
,	,	kIx,	,
Vyškova	Vyškov	k1gInSc2	Vyškov
a	a	k8xC	a
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gMnPc7	on
také	také	k9	také
Boskovice	Boskovice	k1gInPc1	Boskovice
<g/>
,	,	kIx,	,
Bučovice	Bučovice	k1gFnPc1	Bučovice
<g/>
,	,	kIx,	,
Hustopeče	Hustopeč	k1gFnPc1	Hustopeč
<g/>
,	,	kIx,	,
Ivančice	Ivančice	k1gFnPc1	Ivančice
<g/>
,	,	kIx,	,
Kuřim	Kuřim	k1gInSc1	Kuřim
<g/>
,	,	kIx,	,
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
,	,	kIx,	,
Mikulov	Mikulov	k1gInSc1	Mikulov
<g/>
,	,	kIx,	,
Moravský	moravský	k2eAgInSc1d1	moravský
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
Pohořelice	Pohořelice	k1gFnPc1	Pohořelice
<g/>
,	,	kIx,	,
Rosice	Rosice	k1gFnPc1	Rosice
<g/>
,	,	kIx,	,
Slavkov	Slavkov	k1gInSc1	Slavkov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Šlapanice	Šlapanice	k1gFnSc1	Šlapanice
<g/>
,	,	kIx,	,
Tišnov	Tišnov	k1gInSc1	Tišnov
<g/>
,	,	kIx,	,
Veselí	veselí	k1gNnSc1	veselí
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
<g/>
,	,	kIx,	,
Židlochovice	Židlochovice	k1gFnPc1	Židlochovice
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
21	[number]	k4	21
správních	správní	k2eAgInPc2d1	správní
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
malých	malý	k2eAgInPc2d1	malý
okresů	okres	k1gInPc2	okres
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
správní	správní	k2eAgInPc4d1	správní
obvody	obvod	k1gInPc4	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
celkem	celkem	k6eAd1	celkem
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
je	být	k5eAaImIp3nS	být
672	[number]	k4	672
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
39	[number]	k4	39
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
městyse	městys	k1gInSc2	městys
a	a	k8xC	a
50	[number]	k4	50
obcí	obec	k1gFnPc2	obec
status	status	k1gInSc4	status
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
kraje	kraj	k1gInSc2	kraj
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
jeden	jeden	k4xCgInSc1	jeden
vojenský	vojenský	k2eAgInSc1d1	vojenský
újezd	újezd	k1gInSc1	újezd
–	–	k?	–
Březina	Březina	k1gMnSc1	Březina
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
městského	městský	k2eAgNnSc2d1	Městské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
63,5	[number]	k4	63,5
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
kraje	kraj	k1gInSc2	kraj
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
význam	význam	k1gInSc4	význam
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
hranice	hranice	k1gFnSc1	hranice
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
sídlem	sídlo	k1gNnSc7	sídlo
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
justičních	justiční	k2eAgFnPc2d1	justiční
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
velkých	velký	k2eAgFnPc2d1	velká
událostí	událost	k1gFnPc2	událost
evropského	evropský	k2eAgInSc2d1	evropský
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
7	[number]	k4	7
195	[number]	k4	195
km2	km2	k4	km2
(	(	kIx(	(
<g/>
9	[number]	k4	9
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
počtem	počet	k1gInSc7	počet
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
11	[number]	k4	11
%	%	kIx~	%
z	z	k7c2	z
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistické	statistický	k2eAgInPc1d1	statistický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnPc4	město
nad	nad	k7c4	nad
10	[number]	k4	10
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
kraje	kraj	k1gInSc2	kraj
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2005	[number]	k4	2005
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
okresů	okres	k1gInPc2	okres
kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
okresu	okres	k1gInSc2	okres
Brno-venkov	Brnoenkov	k1gInSc4	Brno-venkov
pod	pod	k7c4	pod
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
řada	řada	k1gFnSc1	řada
obcí	obec	k1gFnPc2	obec
západně	západně	k6eAd1	západně
od	od	k7c2	od
Tišnova	Tišnov	k1gInSc2	Tišnov
(	(	kIx(	(
<g/>
Nedvědice	Nedvědice	k1gFnSc1	Nedvědice
s	s	k7c7	s
částí	část	k1gFnPc2	část
Pernštejn	Pernštejn	k1gInSc1	Pernštejn
<g/>
;	;	kIx,	;
Černvír	Černvír	k1gInSc1	Černvír
<g/>
;	;	kIx,	;
Doubravník	Doubravník	k1gInSc1	Doubravník
s	s	k7c7	s
částí	část	k1gFnSc7	část
Křížovice	Křížovice	k1gFnSc1	Křížovice
<g/>
;	;	kIx,	;
Borač	Borač	k1gInSc1	Borač
s	s	k7c7	s
částí	část	k1gFnSc7	část
Podolí	Podolí	k1gNnSc2	Podolí
<g/>
;	;	kIx,	;
Pernštejnské	pernštejnský	k2eAgNnSc1d1	Pernštejnské
Jestřabí	Jestřabí	k1gNnSc1	Jestřabí
s	s	k7c7	s
částmi	část	k1gFnPc7	část
Maňová	Maňová	k1gFnSc1	Maňová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Husle	husle	k1gFnPc4	husle
a	a	k8xC	a
Jilmoví	jilmoví	k1gNnSc4	jilmoví
<g/>
;	;	kIx,	;
Olší	olše	k1gFnSc7	olše
s	s	k7c7	s
částmi	část	k1gFnPc7	část
Litava	Litava	k1gFnSc1	Litava
<g/>
,	,	kIx,	,
Klokočí	klokočí	k2eAgFnPc1d1	klokočí
a	a	k8xC	a
Rakové	rakový	k2eAgFnPc1d1	Raková
<g/>
;	;	kIx,	;
Drahonín	Drahonín	k1gMnSc1	Drahonín
<g/>
;	;	kIx,	;
Žďárec	Žďárec	k1gMnSc1	Žďárec
s	s	k7c7	s
částí	část	k1gFnSc7	část
Víckov	Víckov	k1gInSc1	Víckov
<g/>
;	;	kIx,	;
Vratislávka	Vratislávka	k1gFnSc1	Vratislávka
<g/>
;	;	kIx,	;
Tišnovská	tišnovský	k2eAgFnSc1d1	Tišnovská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
<g/>
;	;	kIx,	;
Skryje	skrýt	k5eAaPmIp3nS	skrýt
<g/>
;	;	kIx,	;
Kaly	kal	k1gInPc1	kal
s	s	k7c7	s
částí	část	k1gFnSc7	část
Zahrada	zahrada	k1gFnSc1	zahrada
<g/>
;	;	kIx,	;
Dolní	dolní	k2eAgFnPc1d1	dolní
Loučky	loučka	k1gFnPc1	loučka
s	s	k7c7	s
částí	část	k1gFnSc7	část
Střemchoví	střemchoví	k1gNnSc2	střemchoví
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
Horní	horní	k2eAgFnSc2d1	horní
Loučky	loučka	k1gFnSc2	loučka
<g/>
;	;	kIx,	;
Újezd	Újezd	k1gInSc1	Újezd
u	u	k7c2	u
Tišnova	Tišnov	k1gInSc2	Tišnov
<g/>
;	;	kIx,	;
Řikonín	Řikonín	k1gInSc1	Řikonín
<g/>
;	;	kIx,	;
Kuřimské	kuřimský	k2eAgNnSc1d1	Kuřimské
Jestřabí	Jestřabí	k1gNnSc1	Jestřabí
s	s	k7c7	s
částí	část	k1gFnSc7	část
Blahoňov	Blahoňovo	k1gNnPc2	Blahoňovo
<g/>
;	;	kIx,	;
Kuřimská	kuřimský	k2eAgFnSc1d1	Kuřimská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
s	s	k7c7	s
částí	část	k1gFnSc7	část
Prosatín	Prosatína	k1gFnPc2	Prosatína
<g/>
;	;	kIx,	;
Lubné	Lubné	k2eAgNnPc2d1	Lubné
<g/>
;	;	kIx,	;
Níhov	Níhovo	k1gNnPc2	Níhovo
<g/>
;	;	kIx,	;
Rojetín	Rojetín	k1gInSc1	Rojetín
<g/>
;	;	kIx,	;
Borovník	borovník	k1gInSc1	borovník
<g/>
;	;	kIx,	;
Katov	Katov	k1gInSc1	Katov
<g/>
;	;	kIx,	;
Křižínkov	Křižínkov	k1gInSc1	Křižínkov
<g/>
)	)	kIx)	)
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
Senorady	Senorada	k1gFnSc2	Senorada
západně	západně	k6eAd1	západně
od	od	k7c2	od
Oslavan	Oslavany	k1gInPc2	Oslavany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
kraj	kraj	k1gInSc1	kraj
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
samosprávnými	samosprávný	k2eAgInPc7d1	samosprávný
kraji	kraj	k1gInPc7	kraj
na	na	k7c6	na
základě	základ	k1gInSc6	základ
článku	článek	k1gInSc2	článek
99	[number]	k4	99
a	a	k8xC	a
následujících	následující	k2eAgInPc2d1	následující
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
347	[number]	k4	347
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
vyšších	vysoký	k2eAgInPc2d2	vyšší
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
názvy	název	k1gInPc4	název
krajů	kraj	k1gInPc2	kraj
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vymezení	vymezení	k1gNnSc4	vymezení
výčtem	výčet	k1gInSc7	výčet
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
území	území	k1gNnSc1	území
okresů	okres	k1gInPc2	okres
definuje	definovat	k5eAaBmIp3nS	definovat
vyhláška	vyhláška	k1gFnSc1	vyhláška
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
č.	č.	k?	č.
564	[number]	k4	564
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgInPc4d2	vyšší
územní	územní	k2eAgInPc4d1	územní
samosprávné	samosprávný	k2eAgInPc4d1	samosprávný
celky	celek	k1gInPc4	celek
stanoví	stanovit	k5eAaPmIp3nS	stanovit
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
kraje	kraj	k1gInPc1	kraj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kraje	kraj	k1gInPc1	kraj
definitivně	definitivně	k6eAd1	definitivně
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
kompetence	kompetence	k1gFnSc2	kompetence
získaly	získat	k5eAaPmAgFnP	získat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
129	[number]	k4	129
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
krajích	kraj	k1gInPc6	kraj
(	(	kIx(	(
<g/>
krajské	krajský	k2eAgNnSc4d1	krajské
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
první	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
jejich	jejich	k3xOp3gNnPc2	jejich
nově	nově	k6eAd1	nově
zřízených	zřízený	k2eAgNnPc2d1	zřízené
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
krajské	krajský	k2eAgNnSc1d1	krajské
členění	členění	k1gNnSc1	členění
je	být	k5eAaImIp3nS	být
obdobné	obdobný	k2eAgNnSc1d1	obdobné
krajům	kraj	k1gInPc3	kraj
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
zřízených	zřízený	k2eAgFnPc2d1	zřízená
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
280	[number]	k4	280
<g/>
/	/	kIx~	/
<g/>
1948	[number]	k4	1948
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc3	květen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
dnem	den	k1gInSc7	den
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
novelizačního	novelizační	k2eAgInSc2d1	novelizační
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
176	[number]	k4	176
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přejmenován	přejmenován	k2eAgInSc1d1	přejmenován
Brněnský	brněnský	k2eAgInSc1d1	brněnský
kraj	kraj	k1gInSc1	kraj
na	na	k7c4	na
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
byly	být	k5eAaImAgInP	být
přejmenovány	přejmenovat	k5eAaPmNgInP	přejmenovat
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
tři	tři	k4xCgInPc1	tři
kraje	kraj	k1gInPc1	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
387	[number]	k4	387
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
25	[number]	k4	25
obcí	obec	k1gFnPc2	obec
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
do	do	k7c2	do
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
existoval	existovat	k5eAaImAgInS	existovat
už	už	k6eAd1	už
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
současný	současný	k2eAgInSc1d1	současný
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
i	i	k9	i
části	část	k1gFnSc6	část
dnešních	dnešní	k2eAgInPc2d1	dnešní
krajů	kraj	k1gInPc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
Olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
a	a	k8xC	a
Zlínského	zlínský	k2eAgNnSc2d1	zlínské
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
tento	tento	k3xDgInSc4	tento
kraj	kraj	k1gInSc1	kraj
sice	sice	k8xC	sice
ještě	ještě	k6eAd1	ještě
formálně	formálně	k6eAd1	formálně
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
nemá	mít	k5eNaImIp3nS	mít
správní	správní	k2eAgInPc1d1	správní
orgány	orgán	k1gInPc1	orgán
s	s	k7c7	s
obecnou	obecný	k2eAgFnSc7d1	obecná
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
obvodem	obvod	k1gInSc7	obvod
Krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
orgánů	orgán	k1gInPc2	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
prehistorické	prehistorický	k2eAgFnPc1d1	prehistorická
doby	doba	k1gFnPc1	doba
proslavily	proslavit	k5eAaPmAgFnP	proslavit
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jiné	jiný	k2eAgFnPc1d1	jiná
části	část	k1gFnPc1	část
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
známé	známý	k2eAgFnPc1d1	známá
památky	památka	k1gFnPc1	památka
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
lovců	lovec	k1gMnPc2	lovec
mamutů	mamut	k1gMnPc2	mamut
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
věstonickou	věstonický	k2eAgFnSc7d1	Věstonická
Venuší	Venuše	k1gFnSc7	Venuše
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
právě	právě	k9	právě
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Moravy	Morava	k1gFnSc2	Morava
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
Oldřichem	Oldřich	k1gMnSc7	Oldřich
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
jih	jih	k1gInSc1	jih
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c4	na
úděl	úděl	k1gInSc4	úděl
brněnský	brněnský	k2eAgInSc4d1	brněnský
a	a	k8xC	a
znojemský	znojemský	k2eAgInSc4d1	znojemský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
najdeme	najít	k5eAaPmIp1nP	najít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
rotund	rotunda	k1gFnPc2	rotunda
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc1d1	dnešní
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
už	už	k6eAd1	už
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
sídlem	sídlo	k1gNnSc7	sídlo
moravských	moravský	k2eAgNnPc2d1	Moravské
markrabat	markrabě	k1gNnPc2	markrabě
a	a	k8xC	a
zasedal	zasedat	k5eAaImAgInS	zasedat
zde	zde	k6eAd1	zde
Moravský	moravský	k2eAgInSc1d1	moravský
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
střídavě	střídavě	k6eAd1	střídavě
zasedal	zasedat	k5eAaImAgInS	zasedat
také	také	k9	také
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
i	i	k9	i
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
světově	světově	k6eAd1	světově
nejproslulejších	proslulý	k2eAgFnPc2d3	nejproslulejší
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
udály	udát	k5eAaPmAgFnP	udát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
bitva	bitva	k1gFnSc1	bitva
tří	tři	k4xCgMnPc2	tři
císařů	císař	k1gMnPc2	císař
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1805	[number]	k4	1805
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc7	jenž
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
porazil	porazit	k5eAaPmAgMnS	porazit
spojená	spojený	k2eAgNnPc4d1	spojené
vojska	vojsko	k1gNnSc2	vojsko
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
a	a	k8xC	a
ruského	ruský	k2eAgMnSc2d1	ruský
cara	car	k1gMnSc2	car
Alexandra	Alexandr	k1gMnSc2	Alexandr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
roli	role	k1gFnSc4	role
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
má	mít	k5eAaImIp3nS	mít
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
První	první	k4xOgFnSc1	první
brněnská	brněnský	k2eAgFnSc1d1	brněnská
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
,	,	kIx,	,
Siemens	siemens	k1gInSc1	siemens
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
turbín	turbína	k1gFnPc2	turbína
<g/>
,	,	kIx,	,
traktorů	traktor	k1gInPc2	traktor
Zetor	zetor	k1gInSc1	zetor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
oblastí	oblast	k1gFnSc7	oblast
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
Blansko	Blansko	k1gNnSc1	Blansko
(	(	kIx(	(
<g/>
ČKD	ČKD	kA	ČKD
Blansko	Blansko	k1gNnSc1	Blansko
Holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
Metra	metro	k1gNnPc1	metro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kuřim	Kuřim	k1gMnSc1	Kuřim
(	(	kIx(	(
<g/>
TOS	TOS	kA	TOS
Kuřim	Kuřim	k1gInSc1	Kuřim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Boskovice	Boskovice	k1gInPc1	Boskovice
(	(	kIx(	(
<g/>
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
,	,	kIx,	,
Novibra	Novibra	k1gFnSc1	Novibra
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Břeclav	Břeclav	k1gFnSc1	Břeclav
(	(	kIx(	(
<g/>
OTIS	OTIS	kA	OTIS
Escalators	Escalators	k1gInSc1	Escalators
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
stoletou	stoletý	k2eAgFnSc4d1	stoletá
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
i	i	k9	i
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
průmysl	průmysl	k1gInSc1	průmysl
(	(	kIx(	(
<g/>
Siemens	siemens	k1gInSc1	siemens
Drásov	Drásov	k1gInSc1	Drásov
<g/>
,	,	kIx,	,
VUES	VUES	kA	VUES
<g/>
,	,	kIx,	,
ZPA	ZPA	kA	ZPA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
rozmístěn	rozmístit	k5eAaPmNgInS	rozmístit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
východě	východ	k1gInSc6	východ
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Mikulov	Mikulov	k1gInSc1	Mikulov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
zpracování	zpracování	k1gNnSc4	zpracování
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
Tišnov	Tišnov	k1gInSc1	Tišnov
–	–	k?	–
Steinhauser	Steinhauser	k1gInSc1	Steinhauser
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc4	zpracování
sterilované	sterilovaný	k2eAgFnSc2d1	sterilovaná
zeleniny	zelenina	k1gFnSc2	zelenina
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
Bzenci	Bzenec	k1gInSc6	Bzenec
<g/>
)	)	kIx)	)
a	a	k8xC	a
cukrovary	cukrovar	k1gInPc1	cukrovar
(	(	kIx(	(
<g/>
Hrušovany	Hrušovany	k1gInPc1	Hrušovany
nad	nad	k7c7	nad
Jevišovkou	Jevišovka	k1gFnSc7	Jevišovka
–	–	k?	–
společnost	společnost	k1gFnSc4	společnost
Moravskoslezské	moravskoslezský	k2eAgInPc1d1	moravskoslezský
cukrovary	cukrovar	k1gInPc1	cukrovar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
čtyři	čtyři	k4xCgInPc1	čtyři
velké	velký	k2eAgInPc1d1	velký
pivovary	pivovar	k1gInPc1	pivovar
(	(	kIx(	(
<g/>
Starobrno	Starobrno	k1gNnSc1	Starobrno
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Vyškov	Vyškov	k1gInSc1	Vyškov
a	a	k8xC	a
Znojemský	znojemský	k2eAgInSc1d1	znojemský
městský	městský	k2eAgInSc1d1	městský
pivovar	pivovar	k1gInSc1	pivovar
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
velkovýrobců	velkovýrobce	k1gMnPc2	velkovýrobce
vína	víno	k1gNnSc2	víno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Znovín	Znovín	k1gInSc1	Znovín
Znojmo	Znojmo	k1gNnSc1	Znojmo
nebo	nebo	k8xC	nebo
Vinium	Vinium	k1gNnSc1	Vinium
ve	v	k7c6	v
Velkých	velký	k2eAgFnPc6d1	velká
Pavlovicích	Pavlovice	k1gFnPc6	Pavlovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
a	a	k8xC	a
farmaceutický	farmaceutický	k2eAgInSc1d1	farmaceutický
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
soustředěn	soustředěn	k2eAgMnSc1d1	soustředěn
především	především	k9	především
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Lachema	lachema	k1gFnSc1	lachema
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ivanovicích	Ivanovice	k1gFnPc6	Ivanovice
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
(	(	kIx(	(
<g/>
Bioveta	Bioveta	k1gFnSc1	Bioveta
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Veverské	Veverský	k2eAgFnSc6d1	Veverská
Bítýšce	Bítýška	k1gFnSc6	Bítýška
(	(	kIx(	(
<g/>
Hartmann	Hartmann	k1gMnSc1	Hartmann
Rico	Rico	k1gMnSc1	Rico
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejteplejším	teplý	k2eAgNnPc3d3	nejteplejší
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
rozvinuto	rozvinout	k5eAaPmNgNnS	rozvinout
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
význam	význam	k1gInSc1	význam
v	v	k7c6	v
pěstování	pěstování	k1gNnSc6	pěstování
obilnin	obilnina	k1gFnPc2	obilnina
<g/>
:	:	kIx,	:
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Zelenina	zelenina	k1gFnSc1	zelenina
<g/>
:	:	kIx,	:
okurky	okurek	k1gInPc4	okurek
<g/>
,	,	kIx,	,
papriky	paprika	k1gFnPc4	paprika
a	a	k8xC	a
rajčata	rajče	k1gNnPc4	rajče
<g/>
.	.	kIx.	.
</s>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
<g/>
:	:	kIx,	:
broskve	broskev	k1gFnPc1	broskev
<g/>
,	,	kIx,	,
meruňky	meruňka	k1gFnPc1	meruňka
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
nejvíce	hodně	k6eAd3	hodně
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
%	%	kIx~	%
plochy	plocha	k1gFnPc1	plocha
kraje	kraj	k1gInPc1	kraj
tvoří	tvořit	k5eAaImIp3nS	tvořit
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	nízce	k6eAd2	nízce
83	[number]	k4	83
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
96	[number]	k4	96
%	%	kIx~	%
vinic	vinice	k1gFnPc2	vinice
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
leží	ležet	k5eAaImIp3nS	ležet
právě	právě	k9	právě
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
chov	chov	k1gInSc1	chov
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
domácího	domácí	k1gMnSc2	domácí
kuru	kura	k1gFnSc4	kura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
železničním	železniční	k2eAgInSc7d1	železniční
i	i	k8xC	i
silničním	silniční	k2eAgInSc7d1	silniční
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Dálnicemi	dálnice	k1gFnPc7	dálnice
je	být	k5eAaImIp3nS	být
napojeno	napojen	k2eAgNnSc1d1	napojeno
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
i	i	k8xC	i
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
Bratislavu	Bratislava	k1gFnSc4	Bratislava
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgNnSc1d1	dobré
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
i	i	k9	i
do	do	k7c2	do
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
rakouské	rakouský	k2eAgFnSc2d1	rakouská
Vídně	Vídeň	k1gFnSc2	Vídeň
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
52	[number]	k4	52
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
severojižní	severojižní	k2eAgFnSc7d1	severojižní
spojnicí	spojnice	k1gFnSc7	spojnice
je	být	k5eAaImIp3nS	být
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
43	[number]	k4	43
na	na	k7c4	na
Svitavy	Svitava	k1gFnPc4	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
Železnicí	železnice	k1gFnSc7	železnice
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
Vídní	Vídeň	k1gFnSc7	Vídeň
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
železniční	železniční	k2eAgFnSc7d1	železniční
spojnicí	spojnice	k1gFnSc7	spojnice
mezi	mezi	k7c7	mezi
Vídní	Vídeň	k1gFnSc7	Vídeň
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Bratislavou	Bratislava	k1gFnSc7	Bratislava
<g/>
)	)	kIx)	)
a	a	k8xC	a
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jihomoravským	jihomoravský	k2eAgInSc7d1	jihomoravský
krajem	kraj	k1gInSc7	kraj
prochází	procházet	k5eAaImIp3nS	procházet
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
tranzitní	tranzitní	k2eAgInSc4d1	tranzitní
železniční	železniční	k2eAgInSc4d1	železniční
koridor	koridor	k1gInSc4	koridor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Brno-Tuřany	Brno-Tuřan	k1gMnPc7	Brno-Tuřan
(	(	kIx(	(
<g/>
ICAO	ICAO	kA	ICAO
LKTB	LKTB	kA	LKTB
<g/>
)	)	kIx)	)
s	s	k7c7	s
pravidelnými	pravidelný	k2eAgInPc7d1	pravidelný
lety	let	k1gInPc7	let
např.	např.	kA	např.
na	na	k7c4	na
Letiště	letiště	k1gNnPc4	letiště
London	London	k1gMnSc1	London
Stansted	Stansted	k1gMnSc1	Stansted
(	(	kIx(	(
<g/>
48	[number]	k4	48
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Londýna	Londýn	k1gInSc2	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Milána	Milán	k1gInSc2	Milán
či	či	k8xC	či
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
obslužnost	obslužnost	k1gFnSc1	obslužnost
větších	veliký	k2eAgNnPc2d2	veliký
sídel	sídlo	k1gNnPc2	sídlo
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
větších	veliký	k2eAgNnPc2d2	veliký
sídel	sídlo	k1gNnPc2	sídlo
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
přímo	přímo	k6eAd1	přímo
napojena	napojen	k2eAgNnPc4d1	napojeno
města	město	k1gNnPc4	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Břeclav	Břeclav	k1gFnSc1	Břeclav
a	a	k8xC	a
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
.	.	kIx.	.
</s>
<s>
Městy	město	k1gNnPc7	město
Hodonín	Hodonín	k1gInSc1	Hodonín
a	a	k8xC	a
Znojmo	Znojmo	k1gNnSc1	Znojmo
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnPc4	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
městem	město	k1gNnSc7	město
Blansko	Blansko	k1gNnSc1	Blansko
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
<g/>
Dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
trať	trať	k1gFnSc1	trať
a	a	k8xC	a
I.	I.	kA	I.
tranzitní	tranzitní	k2eAgInSc1d1	tranzitní
železniční	železniční	k2eAgInSc1d1	železniční
koridor	koridor	k1gInSc1	koridor
spojuje	spojovat	k5eAaImIp3nS	spojovat
města	město	k1gNnPc1	město
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
trať	trať	k1gFnSc1	trať
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
tranzitní	tranzitní	k2eAgInSc1d1	tranzitní
koridor	koridor	k1gInSc1	koridor
spojuje	spojovat	k5eAaImIp3nS	spojovat
města	město	k1gNnSc2	město
Břeclav	Břeclav	k1gFnSc1	Břeclav
a	a	k8xC	a
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnPc1	město
Znojmo	Znojmo	k1gNnSc1	Znojmo
a	a	k8xC	a
Vyškov	Vyškov	k1gInSc1	Vyškov
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
železniční	železniční	k2eAgFnSc4d1	železniční
síť	síť	k1gFnSc4	síť
napojena	napojen	k2eAgFnSc1d1	napojena
jednokolejnými	jednokolejný	k2eAgFnPc7d1	jednokolejná
tratěmi	trať	k1gFnPc7	trať
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgInPc4d1	hlavní
tranzitní	tranzitní	k2eAgInPc4d1	tranzitní
tahy	tah	k1gInPc4	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
veřejných	veřejný	k2eAgFnPc2d1	veřejná
a	a	k8xC	a
státních	státní	k2eAgFnPc2d1	státní
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
–	–	k?	–
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
akademie	akademie	k1gFnSc1	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
a	a	k8xC	a
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
univerzita	univerzita	k1gFnSc1	univerzita
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
<g/>
.	.	kIx.	.
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
dal	dát	k5eAaPmAgInS	dát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
na	na	k7c4	na
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
3,8	[number]	k4	3,8
%	%	kIx~	%
svého	svůj	k1gMnSc4	svůj
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gMnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
českými	český	k2eAgInPc7d1	český
kraji	kraj	k1gInPc7	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
vyniká	vynikat	k5eAaImIp3nS	vynikat
podporou	podpora	k1gFnSc7	podpora
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
IT	IT	kA	IT
<g/>
,	,	kIx,	,
až	až	k9	až
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
začalo	začít	k5eAaPmAgNnS	začít
přezdívat	přezdívat	k5eAaImF	přezdívat
"	"	kIx"	"
<g/>
české	český	k2eAgInPc4d1	český
Silicon	Silicon	kA	Silicon
Valley	Valley	k1gInPc4	Valley
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hejtmani	hejtman	k1gMnPc1	hejtman
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
==	==	k?	==
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
Stanislav	Stanislav	k1gMnSc1	Stanislav
Juránek	Juránek	k1gMnSc1	Juránek
(	(	kIx(	(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
Bohumil	Bohumil	k1gMnSc1	Bohumil
Šimek	Šimek	k1gMnSc1	Šimek
(	(	kIx(	(
<g/>
nestr	nestr	k1gMnSc1	nestr
<g/>
.	.	kIx.	.
za	za	k7c4	za
ANO	ano	k9	ano
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mikroregiony	mikroregion	k1gInPc1	mikroregion
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
Symboly	symbol	k1gInPc1	symbol
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hradů	hrad	k1gInPc2	hrad
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
rozhleden	rozhledna	k1gFnPc2	rozhledna
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zámků	zámek	k1gInPc2	zámek
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
tvrzí	tvrz	k1gFnPc2	tvrz
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
turistický	turistický	k2eAgInSc1d1	turistický
portál	portál	k1gInSc1	portál
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
o	o	k7c6	o
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
Adresy	adresa	k1gFnPc1	adresa
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
Adresář	adresář	k1gInSc1	adresář
úřadů	úřad	k1gInPc2	úřad
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
E-PUSA	E-PUSA	k1gFnSc2	E-PUSA
</s>
</p>
<p>
<s>
Tematický	tematický	k2eAgInSc4d1	tematický
atlas	atlas	k1gInSc4	atlas
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
</s>
</p>
