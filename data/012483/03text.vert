<p>
<s>
Lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
je	být	k5eAaImIp3nS	být
čirý	čirý	k2eAgInSc1d1	čirý
žlutý	žlutý	k2eAgInSc1d1	žlutý
olej	olej	k1gInSc1	olej
získávaný	získávaný	k2eAgInSc1d1	získávaný
ze	z	k7c2	z
sušených	sušený	k2eAgNnPc2d1	sušené
zralých	zralý	k2eAgNnPc2d1	zralé
semen	semeno	k1gNnPc2	semeno
lnu	len	k1gInSc2	len
setého	setý	k2eAgInSc2d1	setý
(	(	kIx(	(
<g/>
Linum	Linum	k1gInSc1	Linum
usitatissimum	usitatissimum	k1gInSc1	usitatissimum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
lisováním	lisování	k1gNnSc7	lisování
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
následnou	následný	k2eAgFnSc7d1	následná
extrakcí	extrakce	k1gFnSc7	extrakce
rozpouštědlem	rozpouštědlo	k1gNnSc7	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lněný	lněný	k2eAgInSc4d1	lněný
olej	olej	k1gInSc4	olej
lze	lze	k6eAd1	lze
polymerizovat	polymerizovat	k5eAaBmF	polymerizovat
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
exotermická	exotermický	k2eAgFnSc1d1	exotermická
<g/>
,	,	kIx,	,
hadr	hadr	k1gInSc1	hadr
namočený	namočený	k2eAgInSc1d1	namočený
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oleji	olej	k1gInSc6	olej
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
sám	sám	k3xTgMnSc1	sám
vznítit	vznítit	k5eAaPmF	vznítit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
polymerizačním	polymerizační	k2eAgFnPc3d1	polymerizační
vlastnostem	vlastnost	k1gFnPc3	vlastnost
se	se	k3xPyFc4	se
lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
používá	používat	k5eAaImIp3nS	používat
samotný	samotný	k2eAgMnSc1d1	samotný
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
oleji	olej	k1gInPc7	olej
<g/>
,	,	kIx,	,
pryskyřicemi	pryskyřice	k1gFnPc7	pryskyřice
a	a	k8xC	a
rozpouštědly	rozpouštědlo	k1gNnPc7	rozpouštědlo
jako	jako	k8xC	jako
impregnační	impregnační	k2eAgFnSc1d1	impregnační
látka	látka	k1gFnSc1	látka
a	a	k8xC	a
lak	lak	k1gInSc1	lak
při	při	k7c6	při
úpravě	úprava	k1gFnSc6	úprava
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pojivo	pojivo	k1gNnSc1	pojivo
pigmentů	pigment	k1gInPc2	pigment
v	v	k7c6	v
olejových	olejový	k2eAgFnPc6d1	olejová
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
plastifikátor	plastifikátor	k1gInSc1	plastifikátor
a	a	k8xC	a
tvrdidlo	tvrdidlo	k1gNnSc1	tvrdidlo
ve	v	k7c6	v
tmelech	tmel	k1gInPc6	tmel
a	a	k8xC	a
také	také	k9	také
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
linolea	linoleum	k1gNnSc2	linoleum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedlý	jedlý	k2eAgInSc4d1	jedlý
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kvůli	kvůli	k7c3	kvůli
výrazné	výrazný	k2eAgFnSc3d1	výrazná
chuti	chuť	k1gFnSc3	chuť
a	a	k8xC	a
pachu	pach	k1gInSc2	pach
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
lidské	lidský	k2eAgFnSc6d1	lidská
výživě	výživa	k1gFnSc6	výživa
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
jako	jako	k9	jako
potravní	potravní	k2eAgInSc1d1	potravní
doplněk	doplněk	k1gInSc1	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
pro	pro	k7c4	pro
nátěrové	nátěrový	k2eAgFnPc4d1	nátěrová
hmoty	hmota	k1gFnPc4	hmota
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
alkydových	alkydův	k2eAgFnPc2d1	alkydův
pryskyřic	pryskyřice	k1gFnPc2	pryskyřice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
syntetické	syntetický	k2eAgInPc1d1	syntetický
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
lépe	dobře	k6eAd2	dobře
odolávají	odolávat	k5eAaImIp3nP	odolávat
žloutnutí	žloutnutí	k1gNnSc4	žloutnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
zasychající	zasychající	k2eAgInPc4d1	zasychající
oleje	olej	k1gInPc4	olej
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
expozici	expozice	k1gFnSc6	expozice
vzduchu	vzduch	k1gInSc2	vzduch
tvrdne	tvrdnout	k5eAaImIp3nS	tvrdnout
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
různých	různý	k2eAgInPc2d1	různý
triglyceridů	triglycerid	k1gInPc2	triglycerid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svými	svůj	k3xOyFgFnPc7	svůj
mastnými	mastný	k2eAgFnPc7d1	mastná
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Triglyceridy	triglycerid	k1gInPc1	triglycerid
ve	v	k7c6	v
lněném	lněný	k2eAgInSc6d1	lněný
oleji	olej	k1gInSc6	olej
jsou	být	k5eAaImIp3nP	být
odvozeny	odvozen	k2eAgFnPc1d1	odvozena
převážně	převážně	k6eAd1	převážně
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nasycené	nasycený	k2eAgFnPc1d1	nasycená
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
:	:	kIx,	:
kyselina	kyselina	k1gFnSc1	kyselina
palmitová	palmitový	k2eAgFnSc1d1	palmitová
(	(	kIx(	(
<g/>
cca	cca	kA	cca
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
stearová	stearový	k2eAgFnSc1d1	stearová
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
<g/>
–	–	k?	–
<g/>
4,6	[number]	k4	4,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
mononenasycená	mononenasycený	k2eAgFnSc1d1	mononenasycená
kyselina	kyselina	k1gFnSc1	kyselina
olejová	olejový	k2eAgFnSc1d1	olejová
(	(	kIx(	(
<g/>
18,5	[number]	k4	18,5
<g/>
–	–	k?	–
<g/>
22,6	[number]	k4	22,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
dvojitě	dvojitě	k6eAd1	dvojitě
nenasycená	nasycený	k2eNgFnSc1d1	nenasycená
kyselina	kyselina	k1gFnSc1	kyselina
linolová	linolový	k2eAgFnSc1d1	linolová
(	(	kIx(	(
<g/>
14,2	[number]	k4	14,2
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
trojitě	trojitě	k6eAd1	trojitě
nenasycená	nasycený	k2eNgFnSc1d1	nenasycená
(	(	kIx(	(
<g/>
omega-	omega-	k?	omega-
<g/>
3	[number]	k4	3
nenasycená	nasycený	k2eNgFnSc1d1	nenasycená
mastná	mastný	k2eAgFnSc1d1	mastná
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
)	)	kIx)	)
kyselina	kyselina	k1gFnSc1	kyselina
α	α	k?	α
(	(	kIx(	(
<g/>
51,9	[number]	k4	51,9
<g/>
–	–	k?	–
<g/>
55,2	[number]	k4	55,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
nenasycených	nasycený	k2eNgInPc2d1	nenasycený
esterů	ester	k1gInPc2	ester
je	být	k5eAaImIp3nS	být
lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
zvláštně	zvláštně	k6eAd1	zvláštně
náchylný	náchylný	k2eAgInSc1d1	náchylný
na	na	k7c4	na
polymerizační	polymerizační	k2eAgFnPc4d1	polymerizační
reakce	reakce	k1gFnPc4	reakce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vystaven	vystavit	k5eAaPmNgInS	vystavit
kyslíku	kyslík	k1gInSc2	kyslík
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
polymerizace	polymerizace	k1gFnSc1	polymerizace
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zasychání	zasychání	k1gNnSc1	zasychání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
použití	použití	k1gNnSc2	použití
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
zasychacích	zasychací	k2eAgFnPc2d1	zasychací
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
výchozím	výchozí	k2eAgInSc7d1	výchozí
materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
kapalina	kapalina	k1gFnSc1	kapalina
nebo	nebo	k8xC	nebo
přinejmenším	přinejmenším	k6eAd1	přinejmenším
tvárná	tvárný	k2eAgFnSc1d1	tvárná
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
vyzrálý	vyzrálý	k2eAgInSc1d1	vyzrálý
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
tuhý	tuhý	k2eAgInSc1d1	tuhý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
hydrofobní	hydrofobní	k2eAgFnSc1d1	hydrofobní
povaha	povaha	k1gFnSc1	povaha
tohoto	tento	k3xDgInSc2	tento
uhlovodíkového	uhlovodíkový	k2eAgInSc2d1	uhlovodíkový
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pojivo	pojivo	k1gNnSc1	pojivo
v	v	k7c6	v
nátěrových	nátěrový	k2eAgFnPc6d1	nátěrová
hmotách	hmota	k1gFnPc6	hmota
===	===	k?	===
</s>
</p>
<p>
<s>
Lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
nosičem	nosič	k1gInSc7	nosič
používaným	používaný	k2eAgInSc7d1	používaný
v	v	k7c6	v
olejových	olejový	k2eAgFnPc6d1	olejová
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
použít	použít	k5eAaPmF	použít
i	i	k9	i
jako	jako	k9	jako
přísadu	přísada	k1gFnSc4	přísada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
činí	činit	k5eAaImIp3nS	činit
barvy	barva	k1gFnPc1	barva
tekutější	tekutý	k2eAgFnPc1d2	tekutější
<g/>
,	,	kIx,	,
průhledné	průhledný	k2eAgFnPc1d1	průhledná
a	a	k8xC	a
lesklé	lesklý	k2eAgFnPc1d1	lesklá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
cca	cca	kA	cca
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
do	do	k7c2	do
tiskových	tiskový	k2eAgFnPc2d1	tisková
barev	barva	k1gFnPc2	barva
pro	pro	k7c4	pro
úpravu	úprava	k1gFnSc4	úprava
viskozity	viskozita	k1gFnSc2	viskozita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
variantách	varianta	k1gFnPc6	varianta
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lisovaný	lisovaný	k2eAgMnSc1d1	lisovaný
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
<g/>
,	,	kIx,	,
alkalicky	alkalicky	k6eAd1	alkalicky
rafinovaný	rafinovaný	k2eAgMnSc1d1	rafinovaný
<g/>
,	,	kIx,	,
bělený	bělený	k2eAgMnSc1d1	bělený
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
zahušťovaný	zahušťovaný	k2eAgMnSc1d1	zahušťovaný
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
nebo	nebo	k8xC	nebo
polymerizovaný	polymerizovaný	k2eAgMnSc1d1	polymerizovaný
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
stand	stand	k?	stand
oil	oil	k?	oil
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
bylo	být	k5eAaImAgNnS	být
významným	významný	k2eAgInSc7d1	významný
krokem	krok	k1gInSc7	krok
v	v	k7c6	v
technologii	technologie	k1gFnSc6	technologie
olejových	olejový	k2eAgFnPc2d1	olejová
nátěrových	nátěrový	k2eAgFnPc2d1	nátěrová
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tmely	tmel	k1gInPc4	tmel
===	===	k?	===
</s>
</p>
<p>
<s>
Sklenářský	sklenářský	k2eAgInSc1d1	sklenářský
tmel	tmel	k1gInSc1	tmel
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
křídového	křídový	k2eAgInSc2d1	křídový
prášku	prášek	k1gInSc2	prášek
a	a	k8xC	a
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
zasklívání	zasklívání	k1gNnSc4	zasklívání
oken	okno	k1gNnPc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdne	tvrdnout	k5eAaImIp3nS	tvrdnout
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
aplikaci	aplikace	k1gFnSc6	aplikace
a	a	k8xC	a
potom	potom	k6eAd1	potom
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
natírat	natírat	k5eAaImF	natírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
úprava	úprava	k1gFnSc1	úprava
dřeva	dřevo	k1gNnSc2	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
Použije	použít	k5eAaPmIp3nS	použít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
při	při	k7c6	při
povrchové	povrchový	k2eAgFnSc6d1	povrchová
úpravě	úprava	k1gFnSc6	úprava
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
zvolna	zvolna	k6eAd1	zvolna
zasychá	zasychat	k5eAaImIp3nS	zasychat
a	a	k8xC	a
při	při	k7c6	při
tvrdnutí	tvrdnutí	k1gNnSc6	tvrdnutí
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
smršťuje	smršťovat	k5eAaImIp3nS	smršťovat
<g/>
.	.	kIx.	.
</s>
<s>
Nepokrývá	pokrývat	k5eNaImIp3nS	pokrývat
povrch	povrch	k1gInSc4	povrch
jako	jako	k8xC	jako
laky	lak	k1gInPc4	lak
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
vsákne	vsáknout	k5eAaPmIp3nS	vsáknout
do	do	k7c2	do
(	(	kIx(	(
<g/>
viditelných	viditelný	k2eAgInPc2d1	viditelný
i	i	k8xC	i
mikroskopických	mikroskopický	k2eAgInPc2d1	mikroskopický
<g/>
)	)	kIx)	)
pórů	pór	k1gInPc2	pór
a	a	k8xC	a
zanechá	zanechat	k5eAaPmIp3nS	zanechat
jasný	jasný	k2eAgMnSc1d1	jasný
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
lesklý	lesklý	k2eAgInSc4d1	lesklý
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
texturu	textura	k1gFnSc4	textura
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
ošetřené	ošetřený	k2eAgNnSc1d1	ošetřené
lněným	lněný	k2eAgInSc7d1	lněný
olejem	olej	k1gInSc7	olej
je	být	k5eAaImIp3nS	být
odolné	odolný	k2eAgNnSc1d1	odolné
proti	proti	k7c3	proti
promačkávání	promačkávání	k1gNnSc3	promačkávání
a	a	k8xC	a
poškrábání	poškrábání	k1gNnSc3	poškrábání
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
snadno	snadno	k6eAd1	snadno
opravit	opravit	k5eAaPmF	opravit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
povrch	povrch	k1gInSc1	povrch
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
jako	jako	k8xS	jako
u	u	k7c2	u
moderních	moderní	k2eAgInPc2d1	moderní
laků	lak	k1gInPc2	lak
a	a	k8xC	a
dřevo	dřevo	k1gNnSc1	dřevo
zvolna	zvolna	k6eAd1	zvolna
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ponecháno	ponechat	k5eAaPmNgNnS	ponechat
mokré	mokrý	k2eAgNnSc1d1	mokré
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgNnSc1d1	měkké
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
proti	proti	k7c3	proti
promačkávání	promačkávání	k1gNnSc3	promačkávání
<g/>
,	,	kIx,	,
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
však	však	k9	však
několikrát	několikrát	k6eAd1	několikrát
opakovanou	opakovaný	k2eAgFnSc4d1	opakovaná
aplikaci	aplikace	k1gFnSc4	aplikace
a	a	k8xC	a
také	také	k9	také
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgInSc4d2	delší
zasychací	zasychací	k2eAgInSc4d1	zasychací
čas	čas	k1gInSc4	čas
oproti	oproti	k7c3	oproti
dřevu	dřevo	k1gNnSc3	dřevo
tvrdému	tvrdé	k1gNnSc3	tvrdé
<g/>
.	.	kIx.	.
</s>
<s>
Zahradní	zahradní	k2eAgInSc1d1	zahradní
nábytek	nábytek	k1gInSc1	nábytek
ošetřený	ošetřený	k2eAgInSc1d1	ošetřený
lněným	lněný	k2eAgInSc7d1	lněný
olejem	olej	k1gInSc7	olej
může	moct	k5eAaImIp3nS	moct
napadnout	napadnout	k5eAaPmF	napadnout
houba	houba	k1gFnSc1	houba
<g/>
.	.	kIx.	.
</s>
<s>
Naolejované	naolejovaný	k2eAgNnSc1d1	naolejované
dřevo	dřevo	k1gNnSc1	dřevo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nažloutlé	nažloutlý	k2eAgNnSc1d1	nažloutlé
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
časem	časem	k6eAd1	časem
tmavnout	tmavnout	k5eAaImF	tmavnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
úpravu	úprava	k1gFnSc4	úprava
pažeb	pažba	k1gFnPc2	pažba
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
získání	získání	k1gNnSc1	získání
velmi	velmi	k6eAd1	velmi
jemného	jemný	k2eAgInSc2d1	jemný
povrchu	povrch	k1gInSc2	povrch
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
nátěrů	nátěr	k1gInPc2	nátěr
lněným	lněný	k2eAgInSc7d1	lněný
olejem	olej	k1gInSc7	olej
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc7d1	tradiční
ochranou	ochrana	k1gFnSc7	ochrana
pro	pro	k7c4	pro
vrbové	vrbový	k2eAgNnSc4d1	vrbové
dřevo	dřevo	k1gNnSc4	dřevo
kriketových	kriketový	k2eAgFnPc2d1	kriketová
pálek	pálka	k1gFnPc2	pálka
<g/>
.	.	kIx.	.
</s>
<s>
Lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
kulečníková	kulečníkový	k2eAgNnPc4d1	kulečníkové
tága	tágo	k1gNnPc4	tágo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
mazivo	mazivo	k1gNnSc1	mazivo
<g/>
/	/	kIx~	/
<g/>
ochrana	ochrana	k1gFnSc1	ochrana
pro	pro	k7c4	pro
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
zobcové	zobcový	k2eAgFnPc4d1	zobcová
flétny	flétna	k1gFnPc4	flétna
a	a	k8xC	a
fujary	fujara	k1gFnPc4	fujara
a	a	k8xC	a
namísto	namísto	k7c2	namísto
epoxidových	epoxidový	k2eAgFnPc2d1	epoxidová
pryskyřic	pryskyřice	k1gFnPc2	pryskyřice
pro	pro	k7c4	pro
moderní	moderní	k2eAgFnSc4d1	moderní
dřevěná	dřevěný	k2eAgNnPc1d1	dřevěné
surfová	surfový	k2eAgNnPc1d1	surfové
prkna	prkno	k1gNnPc1	prkno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Linoleum	linoleum	k1gNnSc1	linoleum
===	===	k?	===
</s>
</p>
<p>
<s>
Lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
spojování	spojování	k1gNnPc4	spojování
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
korkových	korkový	k2eAgFnPc2d1	korková
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
materiálů	materiál	k1gInPc2	materiál
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
linolea	linoleum	k1gNnSc2	linoleum
pro	pro	k7c4	pro
podlahové	podlahový	k2eAgFnPc4d1	podlahová
krytiny	krytina	k1gFnPc4	krytina
(	(	kIx(	(
<g/>
linoleum	linoleum	k1gNnSc4	linoleum
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
Frederick	Frederick	k1gMnSc1	Frederick
Walton	Walton	k1gInSc4	Walton
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linoleum	linoleum	k1gNnSc1	linoleum
<g/>
,	,	kIx,	,
či	či	k8xC	či
zkráceně	zkráceně	k6eAd1	zkráceně
"	"	kIx"	"
<g/>
lino	lino	k6eAd1	lino
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
běžnou	běžný	k2eAgFnSc7d1	běžná
podlahovou	podlahový	k2eAgFnSc7d1	podlahová
krytinou	krytina	k1gFnSc7	krytina
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
ho	on	k3xPp3gMnSc4	on
nahradily	nahradit	k5eAaPmAgFnP	nahradit
krytiny	krytina	k1gFnPc1	krytina
z	z	k7c2	z
PVC	PVC	kA	PVC
<g/>
.	.	kIx.	.
</s>
<s>
Linoleum	linoleum	k1gNnSc1	linoleum
dalo	dát	k5eAaPmAgNnS	dát
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
tiskařské	tiskařský	k2eAgFnSc3d1	tiskařská
technice	technika	k1gFnSc3	technika
linorytu	linoryt	k1gInSc2	linoryt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
reliéfní	reliéfní	k2eAgInSc1d1	reliéfní
design	design	k1gInSc1	design
ryje	rýt	k5eAaImIp3nS	rýt
do	do	k7c2	do
hladkého	hladký	k2eAgInSc2d1	hladký
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nanese	nanést	k5eAaBmIp3nS	nanést
barva	barva	k1gFnSc1	barva
a	a	k8xC	a
použije	použít	k5eAaPmIp3nS	použít
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
tisku	tisk	k1gInSc3	tisk
dřevorytu	dřevoryt	k1gInSc2	dřevoryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potravinový	potravinový	k2eAgInSc1d1	potravinový
doplněk	doplněk	k1gInSc1	doplněk
===	===	k?	===
</s>
</p>
<p>
<s>
Potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
olej	olej	k1gInSc1	olej
ze	z	k7c2	z
lněných	lněný	k2eAgNnPc2d1	lněné
semen	semeno	k1gNnPc2	semeno
se	se	k3xPyFc4	se
lisuje	lisovat	k5eAaImIp3nS	lisovat
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
extrakce	extrakce	k1gFnSc2	extrakce
rozpouštědlem	rozpouštědlo	k1gNnSc7	rozpouštědlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
jako	jako	k9	jako
jedlý	jedlý	k2eAgInSc1d1	jedlý
lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
<g/>
,	,	kIx,	,
chlazený	chlazený	k2eAgInSc1d1	chlazený
a	a	k8xC	a
nezpracovaný	zpracovaný	k2eNgInSc1d1	nezpracovaný
lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
potravní	potravní	k2eAgInSc1d1	potravní
doplněk	doplněk	k1gInSc1	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velká	velká	k1gFnSc1	velká
množství	množství	k1gNnSc1	množství
omega-	omega-	k?	omega-
<g/>
3	[number]	k4	3
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kyseliny	kyselina	k1gFnSc2	kyselina
α	α	k?	α
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přínosná	přínosný	k2eAgNnPc1d1	přínosné
pro	pro	k7c4	pro
omezení	omezení	k1gNnSc4	omezení
zánětů	zánět	k1gInPc2	zánět
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
k	k	k7c3	k
ateroskleróze	ateroskleróza	k1gFnSc3	ateroskleróza
<g/>
,	,	kIx,	,
prevenci	prevence	k1gFnSc3	prevence
srdečních	srdeční	k2eAgFnPc2d1	srdeční
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
arytmie	arytmie	k1gFnSc2	arytmie
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
vývoj	vývoj	k1gInSc4	vývoj
dětí	dítě	k1gFnPc2	dítě
<g/>
..	..	k?	..
Ovšem	ovšem	k9	ovšem
nedávné	dávný	k2eNgNnSc4d1	nedávné
kontrolované	kontrolovaný	k2eAgNnSc4d1	kontrolované
placebo	placebo	k1gNnSc4	placebo
studie	studie	k1gFnSc2	studie
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
běžná	běžný	k2eAgFnSc1d1	běžná
konzumace	konzumace	k1gFnSc1	konzumace
oleje	olej	k1gInSc2	olej
ze	z	k7c2	z
lněných	lněný	k2eAgNnPc2d1	lněné
semen	semeno	k1gNnPc2	semeno
nesnižuje	snižovat	k5eNaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
,	,	kIx,	,
srdečních	srdeční	k2eAgNnPc2d1	srdeční
onemocnění	onemocnění	k1gNnPc2	onemocnění
nebo	nebo	k8xC	nebo
rakoviny	rakovina	k1gFnSc2	rakovina
jakkoli	jakkoli	k6eAd1	jakkoli
více	hodně	k6eAd2	hodně
než	než	k8xS	než
placebo	placebo	k1gNnSc4	placebo
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
52	[number]	k4	52
až	až	k9	až
63	[number]	k4	63
%	%	kIx~	%
kyseliny	kyselina	k1gFnSc2	kyselina
alfa-linolenové	alfainolenová	k1gFnSc2	alfa-linolenová
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
n-	n-	k?	n-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pěstitelé	pěstitel	k1gMnPc1	pěstitel
vyšlechtili	vyšlechtit	k5eAaPmAgMnP	vyšlechtit
i	i	k9	i
len	len	k1gInSc4	len
jak	jak	k6eAd1	jak
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
obsahem	obsah	k1gInSc7	obsah
kyseliny	kyselina	k1gFnSc2	kyselina
alfa-linolenové	alfainolenová	k1gFnSc2	alfa-linolenová
(	(	kIx(	(
<g/>
70	[number]	k4	70
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
kyseliny	kyselina	k1gFnSc2	kyselina
alfa-linolenové	alfainolenová	k1gFnSc2	alfa-linolenová
(	(	kIx(	(
<g/>
<	<	kIx(	<
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
FDA	FDA	kA	FDA
schválila	schválit	k5eAaPmAgFnS	schválit
pro	pro	k7c4	pro
lněný	lněný	k2eAgInSc4d1	lněný
olej	olej	k1gInSc4	olej
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
alfa-linolenové	alfainolenové	k2eAgInSc4d1	alfa-linolenové
status	status	k1gInSc4	status
"	"	kIx"	"
<g/>
všeobecně	všeobecně	k6eAd1	všeobecně
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
semena	semeno	k1gNnPc4	semeno
lnu	lnout	k5eAaImIp1nS	lnout
jako	jako	k9	jako
taková	takový	k3xDgFnSc1	takový
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
lignany	lignan	k1gInPc4	lignan
<g/>
,	,	kIx,	,
třídu	třída	k1gFnSc4	třída
fytoestrogenů	fytoestrogen	k1gInPc2	fytoestrogen
považovaných	považovaný	k2eAgInPc2d1	považovaný
za	za	k7c2	za
látky	látka	k1gFnSc2	látka
s	s	k7c7	s
antioxidačními	antioxidační	k2eAgFnPc7d1	antioxidační
a	a	k8xC	a
protirakovinnými	protirakovinný	k2eAgFnPc7d1	protirakovinná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
extrahovaný	extrahovaný	k2eAgInSc1d1	extrahovaný
lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
lignany	lignana	k1gFnSc2	lignana
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
a	a	k8xC	a
proto	proto	k8xC	proto
nemá	mít	k5eNaImIp3nS	mít
takové	takový	k3xDgFnPc4	takový
antioxidační	antioxidační	k2eAgFnPc4d1	antioxidační
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
lze	lze	k6eAd1	lze
lněný	lněný	k2eAgInSc4d1	lněný
olej	olej	k1gInSc4	olej
snadno	snadno	k6eAd1	snadno
oxidovat	oxidovat	k5eAaBmF	oxidovat
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
proto	proto	k8xC	proto
žlukne	žluknout	k5eAaBmIp3nS	žluknout
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
nepříjemný	příjemný	k2eNgInSc4d1	nepříjemný
zápach	zápach	k1gInSc4	zápach
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
uchováván	uchováván	k2eAgMnSc1d1	uchováván
chlazený	chlazený	k2eAgMnSc1d1	chlazený
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
chladu	chlad	k1gInSc6	chlad
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
jen	jen	k9	jen
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Olej	olej	k1gInSc1	olej
s	s	k7c7	s
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
nebo	nebo	k8xC	nebo
žluklým	žluklý	k2eAgInSc7d1	žluklý
zápachem	zápach	k1gInSc7	zápach
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zlikvidován	zlikvidovat	k5eAaPmNgInS	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Žluklé	žluklý	k2eAgInPc1d1	žluklý
oleje	olej	k1gInPc1	olej
přispívají	přispívat	k5eAaImIp3nP	přispívat
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
volných	volný	k2eAgMnPc2d1	volný
radikálů	radikál	k1gMnPc2	radikál
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
karcinogenní	karcinogenní	k2eAgNnPc1d1	karcinogenní
<g/>
.	.	kIx.	.
</s>
<s>
Oxidace	oxidace	k1gFnSc1	oxidace
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
komerčním	komerční	k2eAgInSc7d1	komerční
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
žluknutí	žluknutí	k1gNnSc3	žluknutí
přidávat	přidávat	k5eAaImF	přidávat
antioxidanty	antioxidant	k1gInPc1	antioxidant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
====	====	k?	====
</s>
</p>
<p>
<s>
Výživové	výživový	k2eAgFnPc1d1	výživová
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Flax	Flax	k1gInSc1	Flax
Council	Council	k1gMnSc1	Council
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
polévková	polévkový	k2eAgFnSc1d1	polévková
lžíce	lžíce	k1gFnSc1	lžíce
(	(	kIx(	(
<g/>
14	[number]	k4	14
g	g	kA	g
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
energie	energie	k1gFnSc1	energie
<g/>
:	:	kIx,	:
520	[number]	k4	520
kJ	kJ	k?	kJ
</s>
</p>
<p>
<s>
celkový	celkový	k2eAgInSc1d1	celkový
tuk	tuk	k1gInSc1	tuk
<g/>
:	:	kIx,	:
14	[number]	k4	14
g	g	kA	g
</s>
</p>
<p>
<s>
Omega-	Omega-	k?	Omega-
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
8	[number]	k4	8
g	g	kA	g
</s>
</p>
<p>
<s>
Omega-	Omega-	k?	Omega-
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
2	[number]	k4	2
g	g	kA	g
</s>
</p>
<p>
<s>
Omega-	Omega-	k?	Omega-
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
3	[number]	k4	3
gOlej	gOlej	k1gInSc4	gOlej
ze	z	k7c2	z
lněných	lněný	k2eAgNnPc2d1	lněné
semen	semeno	k1gNnPc2	semeno
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
významná	významný	k2eAgNnPc4d1	významné
množství	množství	k1gNnPc4	množství
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
sacharidů	sacharid	k1gInPc2	sacharid
ani	ani	k8xC	ani
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lněná	lněný	k2eAgFnSc1d1	lněná
fermež	fermež	k1gFnSc1	fermež
==	==	k?	==
</s>
</p>
<p>
<s>
Lněná	lněný	k2eAgFnSc1d1	lněná
fermež	fermež	k1gFnSc1	fermež
(	(	kIx(	(
<g/>
též	též	k9	též
jen	jen	k9	jen
fermež	fermež	k1gFnSc1	fermež
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
úpravou	úprava	k1gFnSc7	úprava
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
pojivo	pojivo	k1gNnSc1	pojivo
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
nebo	nebo	k8xC	nebo
i	i	k9	i
samotná	samotný	k2eAgFnSc1d1	samotná
pro	pro	k7c4	pro
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
úpravu	úprava	k1gFnSc4	úprava
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fermež	fermež	k1gFnSc1	fermež
se	se	k3xPyFc4	se
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
lázni	lázeň	k1gFnSc6	lázeň
a	a	k8xC	a
nanáší	nanášet	k5eAaImIp3nS	nanášet
se	se	k3xPyFc4	se
horká	horký	k2eAgFnSc1d1	horká
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
dřevo	dřevo	k1gNnSc1	dřevo
pije	pít	k5eAaImIp3nS	pít
<g/>
,	,	kIx,	,
v	v	k7c6	v
několika	několik	k4yIc6	několik
vrstvách	vrstva	k1gFnPc6	vrstva
s	s	k7c7	s
delším	dlouhý	k2eAgInSc7d2	delší
rozestupem	rozestup	k1gInSc7	rozestup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
loužičky	loužička	k1gFnPc1	loužička
přebytečné	přebytečný	k2eAgFnSc2d1	přebytečná
fermeže	fermež	k1gFnSc2	fermež
setřou	setřít	k5eAaPmIp3nP	setřít
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
hadříkem	hadřík	k1gInSc7	hadřík
<g/>
.	.	kIx.	.
</s>
<s>
Schne	schnout	k5eAaImIp3nS	schnout
i	i	k9	i
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
až	až	k8xS	až
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
úpravu	úprava	k1gFnSc4	úprava
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
včelí	včelí	k2eAgInSc4d1	včelí
vosk	vosk	k1gInSc4	vosk
zatepla	zatepla	k6eAd1	zatepla
rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
v	v	k7c6	v
terpentýnu	terpentýn	k1gInSc6	terpentýn
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
fermež	fermež	k1gFnSc1	fermež
neuzavírá	uzavírat	k5eNaImIp3nS	uzavírat
dřevo	dřevo	k1gNnSc4	dřevo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dále	daleko	k6eAd2	daleko
přirozeně	přirozeně	k6eAd1	přirozeně
dýchá	dýchat	k5eAaImIp3nS	dýchat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Hadry	hadr	k1gInPc4	hadr
nasáklé	nasáklý	k2eAgInPc4d1	nasáklý
lněným	lněný	k2eAgInSc7d1	lněný
olejem	olej	k1gInSc7	olej
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
požární	požární	k2eAgNnSc4d1	požární
riziko	riziko	k1gNnSc4	riziko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
velký	velký	k2eAgInSc4d1	velký
povrch	povrch	k1gInSc4	povrch
pro	pro	k7c4	pro
oxidaci	oxidace	k1gFnSc4	oxidace
oleje	olej	k1gInSc2	olej
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
může	moct	k5eAaImIp3nS	moct
oxidovat	oxidovat	k5eAaBmF	oxidovat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Oxidace	oxidace	k1gFnSc1	oxidace
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
je	být	k5eAaImIp3nS	být
exotermická	exotermický	k2eAgFnSc1d1	exotermická
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
hadry	hadr	k1gInPc1	hadr
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
míra	míra	k1gFnSc1	míra
akumulace	akumulace	k1gFnSc2	akumulace
tepla	teplo	k1gNnSc2	teplo
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
míru	míra	k1gFnSc4	míra
jeho	on	k3xPp3gNnSc2	on
rozptylování	rozptylování	k1gNnSc2	rozptylování
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
reakce	reakce	k1gFnSc1	reakce
může	moct	k5eAaImIp3nS	moct
nakonec	nakonec	k6eAd1	nakonec
uvolnit	uvolnit	k5eAaPmF	uvolnit
dostatek	dostatek	k1gInSc1	dostatek
tepla	teplo	k1gNnSc2	teplo
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
samovznícení	samovznícení	k1gNnSc3	samovznícení
hadru	hadr	k1gInSc2	hadr
<g/>
.	.	kIx.	.
</s>
<s>
Hadry	hadr	k1gInPc4	hadr
nasáklé	nasáklý	k2eAgInPc4d1	nasáklý
lněným	lněný	k2eAgInSc7d1	lněný
olejem	olej	k1gInSc7	olej
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neměly	mít	k5eNaImAgFnP	mít
uchovávat	uchovávat	k5eAaImF	uchovávat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ne	ne	k9	ne
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
nádobě	nádoba	k1gFnSc6	nádoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
akumulaci	akumulace	k1gFnSc4	akumulace
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
vyprat	vyprat	k5eAaPmF	vyprat
<g/>
,	,	kIx,	,
nasáknout	nasáknout	k5eAaPmF	nasáknout
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
spálit	spálit	k5eAaPmF	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
zničil	zničit	k5eAaPmAgMnS	zničit
El	Ela	k1gFnPc2	Ela
Rey	Rea	k1gFnSc2	Rea
Theater-Golden	Theater-Goldna	k1gFnPc2	Theater-Goldna
West	West	k2eAgInSc4d1	West
Saloon	saloon	k1gInSc4	saloon
v	v	k7c6	v
Albuquerque	Albuquerque	k1gFnSc6	Albuquerque
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
samovznícením	samovznícení	k1gNnSc7	samovznícení
hadrů	hadr	k1gInPc2	hadr
nasáklých	nasáklý	k2eAgInPc2d1	nasáklý
lněným	lněný	k2eAgInSc7d1	lněný
olejem	olej	k1gInSc7	olej
a	a	k8xC	a
zanechaných	zanechaný	k2eAgInPc2d1	zanechaný
v	v	k7c6	v
plastovém	plastový	k2eAgInSc6d1	plastový
kontejneru	kontejner	k1gInSc6	kontejner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
lněného	lněný	k2eAgInSc2d1	lněný
oleje	olej	k1gInSc2	olej
nebo	nebo	k8xC	nebo
fermeže	fermež	k1gFnSc2	fermež
nad	nad	k7c7	nad
190	[number]	k4	190
°	°	k?	°
<g/>
C	C	kA	C
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
na	na	k7c4	na
nižší	nízký	k2eAgNnSc4d2	nižší
mastné	mastné	k1gNnSc4	mastné
kyseliny	kyselina	k1gFnSc2	kyselina
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
toxický	toxický	k2eAgInSc1d1	toxický
aldehyd	aldehyd	k1gInSc1	aldehyd
akrolein	akrolein	k2eAgInSc1d1	akrolein
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
plynným	plynný	k2eAgInSc7d1	plynný
chlorem	chlor	k1gInSc7	chlor
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Linseed	Linseed	k1gInSc1	Linseed
oil	oil	k?	oil
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lněný	lněný	k2eAgInSc4d1	lněný
olej	olej	k1gInSc4	olej
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
lněný	lněný	k2eAgMnSc1d1	lněný
olej	olej	k1gInSc4	olej
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
