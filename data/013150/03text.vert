<p>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
poslanec	poslanec	k1gMnSc1	poslanec
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
za	za	k7c4	za
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
český	český	k2eAgMnSc1d1	český
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
ministr	ministr	k1gMnSc1	ministr
bez	bez	k7c2	bez
portfeje	portfej	k1gInSc2	portfej
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
soudce	soudce	k1gMnSc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
soudce	soudce	k1gMnSc1	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
složil	složit	k5eAaPmAgMnS	složit
až	až	k9	až
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
advokátní	advokátní	k2eAgMnSc1d1	advokátní
koncipient	koncipient	k1gMnSc1	koncipient
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
coby	coby	k?	coby
advokát	advokát	k1gMnSc1	advokát
v	v	k7c6	v
Chrudimi	Chrudim	k1gFnSc6	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
zakládal	zakládat	k5eAaImAgInS	zakládat
Občanské	občanský	k2eAgNnSc4d1	občanské
fórum	fórum	k1gNnSc4	fórum
v	v	k7c6	v
bydlišti	bydliště	k1gNnSc6	bydliště
a	a	k8xC	a
na	na	k7c6	na
okrese	okres	k1gInSc6	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
za	za	k7c7	za
KDU	KDU	kA	KDU
zvolen	zvolen	k2eAgMnSc1d1	zvolen
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
za	za	k7c4	za
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
obhájil	obhájit	k5eAaPmAgMnS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Východočeský	východočeský	k2eAgInSc1d1	východočeský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
ČNR	ČNR	kA	ČNR
transformována	transformovat	k5eAaBmNgFnS	transformovat
na	na	k7c4	na
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
ústavněprávního	ústavněprávní	k2eAgInSc2d1	ústavněprávní
výboru	výbor	k1gInSc2	výbor
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
navíc	navíc	k6eAd1	navíc
členem	člen	k1gMnSc7	člen
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
zastával	zastávat	k5eAaImAgMnS	zastávat
i	i	k9	i
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
předsedou	předseda	k1gMnSc7	předseda
mandátového	mandátový	k2eAgInSc2d1	mandátový
a	a	k8xC	a
imunitního	imunitní	k2eAgInSc2d1	imunitní
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
členem	člen	k1gInSc7	člen
ústavněprávního	ústavněprávní	k2eAgInSc2d1	ústavněprávní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
opětovně	opětovně	k6eAd1	opětovně
předsedou	předseda	k1gMnSc7	předseda
ústavněprávního	ústavněprávní	k2eAgInSc2d1	ústavněprávní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
jako	jako	k8xC	jako
místopředseda	místopředseda	k1gMnSc1	místopředseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
<g/>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
stranické	stranický	k2eAgFnSc6d1	stranická
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
ho	on	k3xPp3gMnSc4	on
zvolil	zvolit	k5eAaPmAgMnS	zvolit
místopředsedou	místopředseda	k1gMnSc7	místopředseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Opětovně	opětovně	k6eAd1	opětovně
se	se	k3xPyFc4	se
místopředsedou	místopředseda	k1gMnSc7	místopředseda
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
strany	strana	k1gFnSc2	strana
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgInS	zastávat
i	i	k9	i
vládní	vládní	k2eAgInPc4d1	vládní
posty	post	k1gInPc4	post
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1996	[number]	k4	1996
–	–	k?	–
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
byl	být	k5eAaImAgMnS	být
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
vládě	vláda	k1gFnSc6	vláda
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
června	červen	k1gInSc2	červen
1998	[number]	k4	1998
ministrem	ministr	k1gMnSc7	ministr
bez	bez	k7c2	bez
portfeje	portfej	k1gInSc2	portfej
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
ministr	ministr	k1gMnSc1	ministr
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Legislativní	legislativní	k2eAgFnSc2d1	legislativní
rady	rada	k1gFnSc2	rada
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Josefa	Josef	k1gMnSc2	Josef
Tošovského	Tošovský	k1gMnSc2	Tošovský
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
i	i	k9	i
v	v	k7c6	v
komunální	komunální	k2eAgFnSc6d1	komunální
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
1998	[number]	k4	1998
–	–	k?	–
2003	[number]	k4	2003
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Heřmanův	Heřmanův	k2eAgInSc4d1	Heřmanův
Městec	Městec	k1gInSc4	Městec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tamního	tamní	k2eAgNnSc2d1	tamní
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
a	a	k8xC	a
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
KDU-ČSL	KDU-ČSL	k1gFnSc7	KDU-ČSL
<g/>
.3	.3	k4	.3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
soudcem	soudce	k1gMnSc7	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
ČR	ČR	kA	ČR
a	a	k8xC	a
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
jej	on	k3xPp3gMnSc4	on
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
u	u	k7c2	u
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
ale	ale	k9	ale
nedal	dát	k5eNaPmAgInS	dát
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
znovujmenování	znovujmenování	k1gNnSc3	znovujmenování
potřebný	potřebný	k2eAgInSc4d1	potřebný
souhlas	souhlas	k1gInSc4	souhlas
<g/>
,	,	kIx,	,
Výborný	Výborný	k1gMnSc1	Výborný
získal	získat	k5eAaPmAgMnS	získat
jen	jen	k9	jen
36	[number]	k4	36
ze	z	k7c2	z
73	[number]	k4	73
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
jmenování	jmenování	k1gNnSc4	jmenování
mu	on	k3xPp3gMnSc3	on
uniklo	uniknout	k5eAaPmAgNnS	uniknout
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
kandidátem	kandidát	k1gMnSc7	kandidát
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
Senát	senát	k1gInSc1	senát
Zemanovi	Zeman	k1gMnSc3	Zeman
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
soudcem	soudce	k1gMnSc7	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Miloslav	Miloslav	k1gMnSc1	Miloslav
Výborný	Výborný	k1gMnSc1	Výborný
</s>
</p>
<p>
<s>
Medailonek	medailonek	k1gInSc1	medailonek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
</s>
</p>
