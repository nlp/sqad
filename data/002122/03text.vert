<s>
Dakar	Dakar	k1gInSc1	Dakar
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Senegalu	Senegal	k1gInSc2	Senegal
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Zeleném	zelený	k2eAgInSc6d1	zelený
mysu	mys	k1gInSc6	mys
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
Dakar	Dakar	k1gInSc1	Dakar
je	být	k5eAaImIp3nS	být
nejzápadnějším	západní	k2eAgNnSc7d3	nejzápadnější
městem	město	k1gNnSc7	město
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výhodným	výhodný	k2eAgInSc7d1	výhodný
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
trans-atlantický	transtlantický	k2eAgInSc4d1	trans-atlantický
a	a	k8xC	a
evropský	evropský	k2eAgInSc4d1	evropský
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
jistě	jistě	k9	jistě
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dakar	Dakar	k1gInSc1	Dakar
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
přístavem	přístav	k1gInSc7	přístav
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c6	na
2,4	[number]	k4	2,4
miliónu	milión	k4xCgInSc2	milión
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
městě	město	k1gNnSc6	město
žije	žít	k5eAaImIp3nS	žít
1,031	[number]	k4	1,031
miliónu	milión	k4xCgInSc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dakar	Dakar	k1gInSc1	Dakar
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
administrativním	administrativní	k2eAgNnSc7d1	administrativní
centrem	centrum	k1gNnSc7	centrum
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Senegalského	senegalský	k2eAgNnSc2d1	senegalské
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
Senegalského	senegalský	k2eAgInSc2d1	senegalský
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
polopouštní	polopouštní	k2eAgNnSc1d1	polopouštní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přímořské	přímořský	k2eAgFnSc3d1	přímořská
poloze	poloha	k1gFnSc3	poloha
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
mírné	mírný	k2eAgNnSc1d1	mírné
a	a	k8xC	a
ani	ani	k8xC	ani
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
nejsou	být	k5eNaImIp3nP	být
teploty	teplota	k1gFnPc1	teplota
nijak	nijak	k6eAd1	nijak
vysoké	vysoký	k2eAgInPc1d1	vysoký
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rallye	rallye	k1gFnSc1	rallye
Dakar	Dakar	k1gInSc4	Dakar
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Dakar	Dakar	k1gInSc1	Dakar
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dakar	Dakar	k1gInSc4	Dakar
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
</s>
