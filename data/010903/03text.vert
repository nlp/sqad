<p>
<s>
Orloj	orloj	k1gInSc1	orloj
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
horologium	horologium	k1gNnSc1	horologium
<g/>
,	,	kIx,	,
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
ital	ital	k1gMnSc1	ital
<g/>
.	.	kIx.	.
orologio	orologio	k1gMnSc1	orologio
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
středověké	středověký	k2eAgFnPc1d1	středověká
mechanické	mechanický	k2eAgFnPc1d1	mechanická
věžní	věžní	k2eAgFnPc1d1	věžní
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
doplňované	doplňovaný	k2eAgInPc4d1	doplňovaný
dalšími	další	k2eAgInPc7d1	další
zařízeními	zařízení	k1gNnPc7	zařízení
ukazujícími	ukazující	k2eAgFnPc7d1	ukazující
různé	různý	k2eAgInPc4d1	různý
časové	časový	k2eAgInPc4d1	časový
údaje	údaj	k1gInPc4	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Orloje	orloj	k1gInPc1	orloj
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
vybaveny	vybavit	k5eAaPmNgInP	vybavit
též	též	k9	též
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
i	i	k9	i
polohu	poloha	k1gFnSc4	poloha
některých	některý	k3yIgNnPc2	některý
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
lze	lze	k6eAd1	lze
orloj	orloj	k1gInSc1	orloj
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
planetária	planetárium	k1gNnSc2	planetárium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
konstrukce	konstrukce	k1gFnSc1	konstrukce
s	s	k7c7	s
ozubenými	ozubený	k2eAgNnPc7d1	ozubené
koly	kolo	k1gNnPc7	kolo
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
už	už	k6eAd1	už
z	z	k7c2	z
antiky	antika	k1gFnSc2	antika
(	(	kIx(	(
<g/>
Mechanismus	mechanismus	k1gInSc1	mechanismus
z	z	k7c2	z
Antikythéry	Antikythéra	k1gFnSc2	Antikythéra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
vlastního	vlastní	k2eAgInSc2d1	vlastní
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vynálezu	vynález	k1gInSc6	vynález
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
koncem	koncem	k7c2	koncem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
první	první	k4xOgInSc4	první
orloj	orloj	k1gInSc4	orloj
(	(	kIx(	(
<g/>
astrarium	astrarium	k1gNnSc4	astrarium
<g/>
)	)	kIx)	)
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
de	de	k?	de
Dondi	Dond	k1gMnPc1	Dond
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
v	v	k7c6	v
letech	let	k1gInPc6	let
1348	[number]	k4	1348
<g/>
–	–	k?	–
<g/>
1364	[number]	k4	1364
<g/>
.	.	kIx.	.
</s>
<s>
Orloje	orloj	k1gInPc1	orloj
patřily	patřit	k5eAaImAgInP	patřit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
novověku	novověk	k1gInSc2	novověk
k	k	k7c3	k
vrcholným	vrcholný	k2eAgInPc3d1	vrcholný
produktům	produkt	k1gInPc3	produkt
soudobé	soudobý	k2eAgFnSc2d1	soudobá
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
jen	jen	k9	jen
polohy	poloha	k1gFnSc2	poloha
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Dondiho	Dondi	k1gMnSc4	Dondi
Astrarium	Astrarium	k1gNnSc1	Astrarium
prý	prý	k9	prý
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
i	i	k9	i
další	další	k2eAgFnSc2d1	další
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Středověké	středověký	k2eAgInPc1d1	středověký
orloje	orloj	k1gInPc1	orloj
mají	mít	k5eAaImIp3nP	mít
běžný	běžný	k2eAgInSc4d1	běžný
hodinový	hodinový	k2eAgInSc4d1	hodinový
stroj	stroj	k1gInSc4	stroj
s	s	k7c7	s
lihýřem	lihýř	k1gInSc7	lihýř
a	a	k8xC	a
pohonem	pohon	k1gInSc7	pohon
závažím	závaží	k1gNnSc7	závaží
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
hodinového	hodinový	k2eAgInSc2d1	hodinový
hřídele	hřídel	k1gInSc2	hřídel
se	se	k3xPyFc4	se
ozubeným	ozubený	k2eAgInSc7d1	ozubený
převodem	převod	k1gInSc7	převod
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
další	další	k2eAgInPc1d1	další
(	(	kIx(	(
<g/>
pomalejší	pomalý	k2eAgInPc1d2	pomalejší
<g/>
)	)	kIx)	)
pohyby	pohyb	k1gInPc1	pohyb
zvířetníku	zvířetník	k1gInSc2	zvířetník
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
orloje	orloj	k1gInPc1	orloj
(	(	kIx(	(
<g/>
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
,	,	kIx,	,
Brescia	Brescium	k1gNnPc1	Brescium
<g/>
,	,	kIx,	,
Wells	Wells	k1gInSc1	Wells
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgFnP	mít
24	[number]	k4	24
<g/>
hodinový	hodinový	k2eAgInSc1d1	hodinový
ciferník	ciferník	k1gInSc1	ciferník
s	s	k7c7	s
hodinovou	hodinový	k2eAgFnSc7d1	hodinová
ručkou	ručka	k1gFnSc7	ručka
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
Měsíce	měsíc	k1gInSc2	měsíc
znázorňovala	znázorňovat	k5eAaImAgNnP	znázorňovat
buď	buď	k8xC	buď
další	další	k2eAgFnSc1d1	další
ručka	ručka	k1gFnSc1	ručka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
fáze	fáze	k1gFnSc1	fáze
Měsíce	měsíc	k1gInSc2	měsíc
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
mimo	mimo	k7c4	mimo
ciferník	ciferník	k1gInSc4	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
Zvířetník	zvířetník	k1gInSc1	zvířetník
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
pohyblivém	pohyblivý	k2eAgNnSc6d1	pohyblivé
mezikruží	mezikruží	k1gNnSc6	mezikruží
ciferníku	ciferník	k1gInSc2	ciferník
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
orloje	orloj	k1gInPc1	orloj
(	(	kIx(	(
<g/>
Bern	Bern	k1gInSc1	Bern
<g/>
,	,	kIx,	,
Lund	Lund	k1gInSc1	Lund
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Rostock	Rostock	k1gInSc1	Rostock
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
užívaly	užívat	k5eAaImAgFnP	užívat
konstrukci	konstrukce	k1gFnSc4	konstrukce
astrolábu	astroláb	k1gInSc2	astroláb
s	s	k7c7	s
excentrickým	excentrický	k2eAgInSc7d1	excentrický
zvířetníkem	zvířetník	k1gInSc7	zvířetník
a	a	k8xC	a
symboly	symbol	k1gInPc7	symbol
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
příslušných	příslušný	k2eAgFnPc6d1	příslušná
ručkách	ručka	k1gFnPc6	ručka
posouvají	posouvat	k5eAaImIp3nP	posouvat
<g/>
.	.	kIx.	.
</s>
<s>
Orloj	orloj	k1gInSc1	orloj
tak	tak	k6eAd1	tak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
i	i	k9	i
jejich	jejich	k3xOp3gFnSc4	jejich
výšku	výška	k1gFnSc4	výška
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tzv.	tzv.	kA	tzv.
babylonský	babylonský	k2eAgInSc4d1	babylonský
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Orloje	orloj	k1gInSc2	orloj
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
zachovaných	zachovaný	k2eAgMnPc2d1	zachovaný
je	být	k5eAaImIp3nS	být
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
umístěný	umístěný	k2eAgInSc1d1	umístěný
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
Staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zkonstruován	zkonstruovat	k5eAaPmNgInS	zkonstruovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1410	[number]	k4	1410
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
upravován	upravovat	k5eAaImNgInS	upravovat
a	a	k8xC	a
rozšiřován	rozšiřován	k2eAgMnSc1d1	rozšiřován
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
jeho	jeho	k3xOp3gMnSc7	jeho
domnělým	domnělý	k2eAgMnSc7d1	domnělý
tvůrcem	tvůrce	k1gMnSc7	tvůrce
<g/>
,	,	kIx,	,
orlojníkem	orlojník	k1gMnSc7	orlojník
mistrem	mistr	k1gMnSc7	mistr
Hanušem	Hanuš	k1gMnSc7	Hanuš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skutečného	skutečný	k2eAgMnSc4d1	skutečný
autora	autor	k1gMnSc4	autor
orloje	orloj	k1gInSc2	orloj
<g/>
,	,	kIx,	,
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
z	z	k7c2	z
Kadaně	Kadaň	k1gFnSc2	Kadaň
<g/>
,	,	kIx,	,
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
rodišti	rodiště	k1gNnSc6	rodiště
nefunkční	funkční	k2eNgFnSc1d1	nefunkční
replika	replika	k1gFnSc1	replika
astronomické	astronomický	k2eAgFnSc2d1	astronomická
části	část	k1gFnSc2	část
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
heliocentrickým	heliocentrický	k2eAgInSc7d1	heliocentrický
orlojem	orloj	k1gInSc7	orloj
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
orloj	orloj	k1gInSc1	orloj
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
kompletně	kompletně	k6eAd1	kompletně
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Menší	malý	k2eAgInPc1d2	menší
novodobé	novodobý	k2eAgInPc1d1	novodobý
orloje	orloj	k1gInPc1	orloj
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
Nové	Nové	k2eAgFnSc6d1	Nové
radnici	radnice	k1gFnSc6	radnice
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
a	a	k8xC	a
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
orloj	orloj	k1gInSc4	orloj
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
také	také	k9	také
hodiny	hodina	k1gFnPc4	hodina
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Brodě	Brod	k1gInSc6	Brod
<g/>
,	,	kIx,	,
ukazující	ukazující	k2eAgFnPc4d1	ukazující
fáze	fáze	k1gFnPc4	fáze
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
obdobný	obdobný	k2eAgInSc1d1	obdobný
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
s	s	k7c7	s
poskakujícími	poskakující	k2eAgInPc7d1	poskakující
kozlíky	kozlík	k1gInPc7	kozlík
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Pelhřimově	Pelhřimov	k1gInSc6	Pelhřimov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orloj	orloj	k1gInSc1	orloj
v	v	k7c6	v
Kryštofově	Kryštofův	k2eAgNnSc6d1	Kryštofovo
Údolí	údolí	k1gNnSc6	údolí
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
pražským	pražský	k2eAgInSc7d1	pražský
orlojem	orloj	k1gInSc7	orloj
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nebyl	být	k5eNaImAgMnS	být
orlojem	orloj	k1gInSc7	orloj
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
o	o	k7c4	o
zobrazení	zobrazení	k1gNnSc4	zobrazení
fází	fáze	k1gFnPc2	fáze
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
znamení	znamení	k1gNnSc2	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
orloj	orloj	k1gInSc4	orloj
je	být	k5eAaImIp3nS	být
také	také	k9	také
nepřesně	přesně	k6eNd1	přesně
označován	označovat	k5eAaImNgInS	označovat
brněnský	brněnský	k2eAgInSc1d1	brněnský
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
sestrojený	sestrojený	k2eAgInSc1d1	sestrojený
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
skromný	skromný	k2eAgInSc4d1	skromný
orloj	orloj	k1gInSc4	orloj
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
také	také	k9	také
daly	dát	k5eAaPmAgFnP	dát
považovat	považovat	k5eAaImF	považovat
Astronomické	astronomický	k2eAgFnPc4d1	astronomická
hodiny	hodina	k1gFnPc4	hodina
u	u	k7c2	u
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Orloje	orloj	k1gInPc1	orloj
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
významné	významný	k2eAgInPc1d1	významný
orloje	orloj	k1gInPc1	orloj
byly	být	k5eAaImAgInP	být
především	především	k9	především
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rouen	Rouen	k1gInSc1	Rouen
<g/>
,	,	kIx,	,
Besançon	Besançon	k1gInSc1	Besançon
<g/>
,	,	kIx,	,
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
,	,	kIx,	,
Bern	Bern	k1gInSc1	Bern
<g/>
,	,	kIx,	,
Heilbronn	Heilbronn	k1gInSc1	Heilbronn
<g/>
,	,	kIx,	,
Rostock	Rostock	k1gInSc1	Rostock
<g/>
,	,	kIx,	,
Lübeck	Lübeck	k1gInSc1	Lübeck
<g/>
,	,	kIx,	,
Münster	Münster	k1gInSc1	Münster
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Norimberk	Norimberk	k1gInSc1	Norimberk
<g/>
,	,	kIx,	,
Wells	Wells	k1gInSc1	Wells
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
orloj	orloj	k1gInSc1	orloj
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
katedrála	katedrála	k1gFnSc1	katedrála
ve	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
Lundu	Lunda	k1gMnSc4	Lunda
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
též	též	k9	též
Kremelský	kremelský	k2eAgInSc1d1	kremelský
orloj	orloj	k1gInSc1	orloj
v	v	k7c6	v
moskevském	moskevský	k2eAgInSc6d1	moskevský
Kremlu	Kreml	k1gInSc6	Kreml
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dal	dát	k5eAaPmAgInS	dát
jméno	jméno	k1gNnSc4	jméno
i	i	k8xC	i
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Repliky	replika	k1gFnPc1	replika
Staroměstského	staroměstský	k2eAgInSc2d1	staroměstský
orloje	orloj	k1gInSc2	orloj
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
(	(	kIx(	(
<g/>
park	park	k1gInSc1	park
u	u	k7c2	u
Nishi-Shinjuku	Nishi-Shinjuk	k1gInSc2	Nishi-Shinjuk
Tower	Towra	k1gFnPc2	Towra
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
(	(	kIx(	(
<g/>
Castle	Castle	k1gFnSc1	Castle
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Astroláb	Astroláb	k1gInSc1	Astroláb
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
je	být	k5eAaImIp3nS	být
však	však	k9	však
okopírován	okopírovat	k5eAaPmNgInS	okopírovat
z	z	k7c2	z
pražského	pražský	k2eAgInSc2d1	pražský
orloje	orloj	k1gInSc2	orloj
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jejich	jejich	k3xOp3gInPc1	jejich
astronomické	astronomický	k2eAgInPc1d1	astronomický
údaje	údaj	k1gInPc1	údaj
nejsou	být	k5eNaImIp3nP	být
platné	platný	k2eAgInPc1d1	platný
pro	pro	k7c4	pro
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
polohu	poloha	k1gFnSc4	poloha
těchto	tento	k3xDgNnPc2	tento
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgMnPc2d3	nejmladší
orlojů	orloj	k1gInPc2	orloj
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
slovenské	slovenský	k2eAgFnSc6d1	slovenská
obci	obec	k1gFnSc6	obec
Stará	starý	k2eAgFnSc1d1	stará
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
;	;	kIx,	;
dokončen	dokončen	k2eAgMnSc1d1	dokončen
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Astroláb	Astroláb	k1gMnSc1	Astroláb
</s>
</p>
<p>
<s>
Planetárium	planetárium	k1gNnSc1	planetárium
</s>
</p>
<p>
<s>
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
</s>
</p>
<p>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
orloj	orloj	k1gInSc1	orloj
</s>
</p>
<p>
<s>
Orloj	orloj	k1gInSc1	orloj
v	v	k7c6	v
Kryštofově	Kryštofův	k2eAgNnSc6d1	Kryštofovo
Údolí	údolí	k1gNnSc6	údolí
</s>
</p>
<p>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
orloj	orloj	k1gInSc1	orloj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pokojový	pokojový	k2eAgInSc1d1	pokojový
orloj	orloj	k1gInSc1	orloj
Jana	Jan	k1gMnSc2	Jan
Maška	Mašek	k1gMnSc2	Mašek
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
orloj	orloj	k1gInSc1	orloj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
orloj	orloj	k1gInSc1	orloj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
orloj	orloj	k1gInSc1	orloj
-	-	kIx~	-
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
simulátory	simulátor	k1gInPc1	simulátor
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
orloje	orloj	k1gInPc1	orloj
</s>
</p>
<p>
<s>
Webkamera	Webkamera	k1gFnSc1	Webkamera
</s>
</p>
<p>
<s>
WINTER	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Práce	práce	k1gFnSc2	práce
umělecké	umělecký	k2eAgInPc4d1	umělecký
:	:	kIx,	:
Orloje	orloj	k1gInPc4	orloj
<g/>
,	,	kIx,	,
s.	s.	k?	s.
824	[number]	k4	824
<g/>
-	-	kIx~	-
<g/>
825	[number]	k4	825
<g/>
.	.	kIx.	.
</s>
</p>
