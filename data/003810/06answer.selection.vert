<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
Příbor	Příbor	k1gInSc1	Příbor
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Sigismund	Sigismunda	k1gFnPc2	Sigismunda
Šlomo	Šloma	k1gFnSc5	Šloma
Freud	Freud	k1gInSc4	Freud
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
lékař-neurolog	lékařeurolog	k1gMnSc1	lékař-neurolog
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
