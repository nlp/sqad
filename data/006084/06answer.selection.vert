<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
nula	nula	k1gFnSc1	nula
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
navržena	navržen	k2eAgFnSc1d1	navržena
Guillaumem	Guillaum	k1gInSc7	Guillaum
Amontonsem	Amontons	k1gInSc7	Amontons
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
<g/>
.	.	kIx.	.
</s>
