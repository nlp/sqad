<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
nula	nula	k1gFnSc1	nula
je	být	k5eAaImIp3nS	být
hypotetický	hypotetický	k2eAgInSc4d1	hypotetický
stav	stav	k1gInSc4	stav
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
veškerý	veškerý	k3xTgInSc4	veškerý
tepelný	tepelný	k2eAgInSc4d1	tepelný
pohyb	pohyb	k1gInSc4	pohyb
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
nula	nula	k1gFnSc1	nula
je	být	k5eAaImIp3nS	být
počátek	počátek	k1gInSc4	počátek
stupnice	stupnice	k1gFnSc2	stupnice
absolutní	absolutní	k2eAgFnSc2d1	absolutní
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
pro	pro	k7c4	pro
termodynamickou	termodynamický	k2eAgFnSc4d1	termodynamická
teplotu	teplota	k1gFnSc4	teplota
T	T	kA	T
=	=	kIx~	=
0	[number]	k4	0
K	K	kA	K
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
–	–	k?	–
273,149	[number]	k4	273,149
<g/>
806372	[number]	k4	806372
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
nula	nula	k1gFnSc1	nula
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
navržena	navržen	k2eAgFnSc1d1	navržena
Guillaumem	Guillaum	k1gInSc7	Guillaum
Amontonsem	Amontons	k1gInSc7	Amontons
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Chyběly	chybět	k5eAaImAgInP	chybět
mu	on	k3xPp3gNnSc3	on
dostatečně	dostatečně	k6eAd1	dostatečně
přesné	přesný	k2eAgInPc1d1	přesný
teploměry	teploměr	k1gInPc1	teploměr
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
výsledky	výsledek	k1gInPc1	výsledek
velmi	velmi	k6eAd1	velmi
nepřesné	přesný	k2eNgInPc1d1	nepřesný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tlak	tlak	k1gInSc1	tlak
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
asi	asi	k9	asi
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
chladnou	chladný	k2eAgFnSc7d1	chladná
<g/>
"	"	kIx"	"
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
bodem	bod	k1gInSc7	bod
varu	var	k1gInSc2	var
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
jej	on	k3xPp3gMnSc4	on
dovedla	dovést	k5eAaPmAgFnS	dovést
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
snížení	snížení	k1gNnSc1	snížení
teploty	teplota	k1gFnSc2	teplota
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
ovšem	ovšem	k9	ovšem
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
reálné	reálný	k2eAgInPc1d1	reálný
plyny	plyn	k1gInPc1	plyn
zkapalní	zkapalnit	k5eAaPmIp3nP	zkapalnit
během	během	k7c2	během
zchlazování	zchlazování	k1gNnSc2	zchlazování
k	k	k7c3	k
absolutní	absolutní	k2eAgFnSc3d1	absolutní
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
William	William	k1gInSc1	William
Thomson	Thomson	k1gInSc4	Thomson
(	(	kIx(	(
<g/>
lord	lord	k1gMnSc1	lord
Kelvin	kelvin	k1gInSc1	kelvin
of	of	k?	of
Largs	Largs	k1gInSc1	Largs
<g/>
)	)	kIx)	)
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
termodynamickou	termodynamický	k2eAgFnSc4d1	termodynamická
teplotní	teplotní	k2eAgFnSc4d1	teplotní
stupnici	stupnice	k1gFnSc4	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
se	se	k3xPyFc4	se
vymanilo	vymanit	k5eAaPmAgNnS	vymanit
z	z	k7c2	z
omezení	omezení	k1gNnSc2	omezení
plynných	plynný	k2eAgFnPc2d1	plynná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
definovalo	definovat	k5eAaBmAgNnS	definovat
absolutní	absolutní	k2eAgFnSc4d1	absolutní
nulu	nula	k1gFnSc4	nula
jako	jako	k8xC	jako
takovou	takový	k3xDgFnSc4	takový
teplotu	teplota	k1gFnSc4	teplota
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
již	již	k6eAd1	již
nelze	lze	k6eNd1	lze
odebírat	odebírat	k5eAaImF	odebírat
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
dosud	dosud	k6eAd1	dosud
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgInPc2d1	pouhý
100	[number]	k4	100
pK	pK	k?	pK
=	=	kIx~	=
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
10	[number]	k4	10
K	K	kA	K
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
systému	systém	k1gInSc6	systém
jaderných	jaderný	k2eAgInPc2d1	jaderný
spinů	spin	k1gInPc2	spin
v	v	k7c6	v
kovovém	kovový	k2eAgNnSc6d1	kovové
rhodiu	rhodium	k1gNnSc6	rhodium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
kolektiv	kolektivum	k1gNnPc2	kolektivum
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
Massachusettského	massachusettský	k2eAgInSc2d1	massachusettský
technologického	technologický	k2eAgInSc2d1	technologický
institutu	institut	k1gInSc2	institut
v	v	k7c6	v
Cambridge	Cambridge	k1gFnSc1	Cambridge
(	(	kIx(	(
<g/>
A.E.	A.E.	k1gMnSc1	A.E.
Leanhardt	Leanhardt	k1gMnSc1	Leanhardt
<g/>
,	,	kIx,	,
T.A.	T.A.	k1gMnSc1	T.A.
Pasquini	Pasquin	k2eAgMnPc1d1	Pasquin
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Saba	Sab	k1gInSc2	Sab
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Schirotzek	Schirotzek	k1gInSc1	Schirotzek
<g/>
,	,	kIx,	,
Y.	Y.	kA	Y.
Shin	Shin	k1gInSc1	Shin
<g/>
,	,	kIx,	,
D.	D.	kA	D.
Kielpinski	Kielpinski	k1gNnSc1	Kielpinski
<g/>
,	,	kIx,	,
D.E.	D.E.	k1gFnSc1	D.E.
Pritchard	Pritchard	k1gMnSc1	Pritchard
a	a	k8xC	a
W.	W.	kA	W.
Ketterle	Ketterle	k1gInSc1	Ketterle
<g/>
)	)	kIx)	)
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
mechanicky	mechanicky	k6eAd1	mechanicky
dosažené	dosažený	k2eAgFnSc2d1	dosažená
teploty	teplota	k1gFnSc2	teplota
450	[number]	k4	450
pikokelvinů	pikokelvin	k1gInPc2	pikokelvin
=	=	kIx~	=
0,000	[number]	k4	0,000
000	[number]	k4	000
000	[number]	k4	000
45	[number]	k4	45
K	K	kA	K
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Boseho-Einsteinově	Boseho-Einsteinův	k2eAgInSc6d1	Boseho-Einsteinův
kondenzátu	kondenzát	k1gInSc6	kondenzát
spinově	spinově	k6eAd1	spinově
polarizovaných	polarizovaný	k2eAgInPc2d1	polarizovaný
atomů	atom	k1gInPc2	atom
sodíku	sodík	k1gInSc2	sodík
zachycených	zachycený	k2eAgInPc2d1	zachycený
v	v	k7c6	v
gravitomagnetické	gravitomagnetický	k2eAgFnSc6d1	gravitomagnetický
pasti	past	k1gFnSc6	past
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
věta	věta	k1gFnSc1	věta
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
nelze	lze	k6eNd1	lze
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
absolutní	absolutní	k2eAgFnSc1d1	absolutní
nula	nula	k1gFnSc1	nula
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
teoretická	teoretický	k2eAgFnSc1d1	teoretická
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
ovšem	ovšem	k9	ovšem
limitně	limitně	k6eAd1	limitně
přiblížit	přiblížit	k5eAaPmF	přiblížit
velice	velice	k6eAd1	velice
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
statistické	statistický	k2eAgFnSc2d1	statistická
fyziky	fyzika	k1gFnSc2	fyzika
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgFnSc1d1	absolutní
nula	nula	k1gFnSc1	nula
stav	stav	k1gInSc1	stav
tělesa	těleso	k1gNnSc2	těleso
s	s	k7c7	s
nejmenší	malý	k2eAgFnSc7d3	nejmenší
možnou	možný	k2eAgFnSc7d1	možná
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
nutně	nutně	k6eAd1	nutně
nulovou	nulový	k2eAgFnSc4d1	nulová
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
systém	systém	k1gInSc1	systém
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
kvantově	kvantově	k6eAd1	kvantově
mechanických	mechanický	k2eAgInPc2d1	mechanický
harmonických	harmonický	k2eAgInPc2d1	harmonický
oscilátorů	oscilátor	k1gInPc2	oscilátor
<g/>
)	)	kIx)	)
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
zcela	zcela	k6eAd1	zcela
mění	měnit	k5eAaImIp3nS	měnit
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Bose-Einsteinův	Bose-Einsteinův	k2eAgInSc1d1	Bose-Einsteinův
kondenzát	kondenzát	k1gInSc1	kondenzát
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spojeny	spojen	k2eAgInPc4d1	spojen
jevy	jev	k1gInPc4	jev
supravodivosti	supravodivost	k1gFnSc2	supravodivost
a	a	k8xC	a
supratekutosti	supratekutost	k1gFnSc2	supratekutost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
pojem	pojem	k1gInSc1	pojem
záporné	záporný	k2eAgFnSc2d1	záporná
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
systému	systém	k1gInSc2	systém
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
energií	energie	k1gFnSc7	energie
shora	shora	k6eAd1	shora
i	i	k9	i
zdola	zdola	k6eAd1	zdola
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
s	s	k7c7	s
omezeným	omezený	k2eAgNnSc7d1	omezené
energetickým	energetický	k2eAgNnSc7d1	energetické
spektrem	spektrum	k1gNnSc7	spektrum
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečným	konečný	k2eAgInSc7d1	konečný
počtem	počet	k1gInSc7	počet
energetických	energetický	k2eAgInPc2d1	energetický
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
s	s	k7c7	s
inverzním	inverzní	k2eAgNnSc7d1	inverzní
obsazením	obsazení	k1gNnSc7	obsazení
stavů	stav	k1gInPc2	stav
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
obsazené	obsazený	k2eAgInPc4d1	obsazený
stavy	stav	k1gInPc4	stav
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
energií	energie	k1gFnSc7	energie
než	než	k8xS	než
nízkou	nízký	k2eAgFnSc7d1	nízká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
však	však	k9	však
neporušuje	porušovat	k5eNaImIp3nS	porušovat
platnost	platnost	k1gFnSc4	platnost
třetího	třetí	k4xOgInSc2	třetí
termodynamického	termodynamický	k2eAgInSc2d1	termodynamický
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
takový	takový	k3xDgInSc1	takový
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
teplejší	teplý	k2eAgInSc1d2	teplejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
systém	systém	k1gInSc1	systém
s	s	k7c7	s
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
posloupnost	posloupnost	k1gFnSc4	posloupnost
stoupajících	stoupající	k2eAgFnPc2d1	stoupající
teplot	teplota	k1gFnPc2	teplota
lze	lze	k6eAd1	lze
formálně	formálně	k6eAd1	formálně
zapsat	zapsat	k5eAaPmF	zapsat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
+	+	kIx~	+
ε	ε	k?	ε
<	<	kIx(	<
∞	∞	k?	∞
<	<	kIx(	<
−	−	k?	−
∞	∞	k?	∞
<	<	kIx(	<
0	[number]	k4	0
−	−	k?	−
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnSc1	epsilon
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
<	<	kIx(	<
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnSc1	epsilon
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnPc3	epsilon
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
označuje	označovat	k5eAaImIp3nS	označovat
infinitezimální	infinitezimální	k2eAgInSc4d1	infinitezimální
přírůstek	přírůstek	k1gInSc4	přírůstek
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc1	příklad
praktického	praktický	k2eAgNnSc2d1	praktické
použití	použití	k1gNnSc2	použití
inverzního	inverzní	k2eAgNnSc2d1	inverzní
obsazení	obsazení	k1gNnSc2	obsazení
stavů	stav	k1gInPc2	stav
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
laser	laser	k1gInSc4	laser
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
Teplotní	teplotní	k2eAgFnSc2d1	teplotní
stupnice	stupnice	k1gFnSc2	stupnice
Teplo	teplo	k6eAd1	teplo
Bose-Einsteinův	Bose-Einsteinův	k2eAgInSc1d1	Bose-Einsteinův
kondenzát	kondenzát	k1gInSc1	kondenzát
ITS-90	ITS-90	k1gFnSc2	ITS-90
</s>
