<s>
Paraskevopoulos	Paraskevopoulos	k1gInSc1
(	(	kIx(
<g/>
kráter	kráter	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kráter	kráter	k1gInSc1
Paraskevopoulos	Paraskevopoulosa	k1gFnPc2
</s>
<s>
Souřadnice	souřadnice	k1gFnSc1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
</s>
<s>
Selenografická	Selenografický	k2eAgFnSc1d1
šířka	šířka	k1gFnSc1
</s>
<s>
50,1	50,1	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
Selenografická	Selenografický	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
150,3	150,3	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
Paraskevopoulos	Paraskevopoulos	k1gMnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
kráteru	kráter	k1gInSc2
</s>
<s>
Průměr	průměr	k1gInSc1
kráteru	kráter	k1gInSc2
</s>
<s>
99	#num#	k4
km	km	kA
</s>
<s>
Hloubka	hloubka	k1gFnSc1
</s>
<s>
</s>
<s>
Colongitudo	Colongitudo	k1gNnSc1
</s>
<s>
152	#num#	k4
<g/>
°	°	k?
</s>
<s>
Eponym	eponym	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
S.	S.	kA
Paraskevopoulos	Paraskevopoulos	k1gMnSc1
</s>
<s>
Paraskevopoulos	Paraskevopoulos	k1gInSc1
je	být	k5eAaImIp3nS
starý	starý	k2eAgInSc4d1
kráter	kráter	k1gInSc1
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Měsíce	měsíc	k1gInSc2
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
odvrácené	odvrácený	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
ze	z	k7c2
Země	Země	k1gFnSc2
jej	on	k3xPp3gMnSc4
tedy	tedy	k9
nelze	lze	k6eNd1
pozorovat	pozorovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
průměr	průměr	k1gInSc4
99	#num#	k4
km	km	kA
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
dně	dno	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
řada	řada	k1gFnSc1
dalších	další	k2eAgInPc2d1
kráterů	kráter	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
největší	veliký	k2eAgInPc4d3
je	být	k5eAaImIp3nS
Paraskevopoulos	Paraskevopoulos	k1gInSc4
E.	E.	kA
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
okrajového	okrajový	k2eAgInSc2d1
valu	val	k1gInSc2
je	být	k5eAaImIp3nS
narušena	narušit	k5eAaPmNgFnS
satelitním	satelitní	k2eAgInSc7d1
Paraskevopoulos	Paraskevopoulos	k1gInSc1
H.	H.	kA
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1
leží	ležet	k5eAaImIp3nS
kráter	kráter	k1gInSc4
Carnot	Carnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pojmenován	pojmenován	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
na	na	k7c4
počest	počest	k1gFnSc4
řeckého	řecký	k2eAgMnSc2d1
astronoma	astronom	k1gMnSc2
Johna	John	k1gMnSc2
S.	S.	kA
Paraskevopoulose	Paraskevopoulosa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Satelitní	satelitní	k2eAgInPc1d1
krátery	kráter	k1gInPc1
</s>
<s>
V	v	k7c6
okolí	okolí	k1gNnSc6
kráteru	kráter	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
několik	několik	k4yIc1
dalších	další	k2eAgInPc2d1
kráterů	kráter	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
byly	být	k5eAaImAgInP
označeny	označit	k5eAaPmNgInP
podle	podle	k7c2
zavedených	zavedený	k2eAgFnPc2d1
zvyklostí	zvyklost	k1gFnPc2
jménem	jméno	k1gNnSc7
hlavního	hlavní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
a	a	k8xC
velkým	velký	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
abecedy	abeceda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Paraskevopoulos	Paraskevopoulos	k1gMnSc1
</s>
<s>
Sel	sít	k5eAaImAgMnS
<g/>
.	.	kIx.
šířka	šířka	k1gFnSc1
</s>
<s>
Sel	sít	k5eAaImAgMnS
<g/>
.	.	kIx.
délka	délka	k1gFnSc1
</s>
<s>
Průměr	průměr	k1gInSc1
</s>
<s>
E	E	kA
</s>
<s>
50.6	50.6	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
149.4	149.4	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
24	#num#	k4
km	km	kA
</s>
<s>
H	H	kA
</s>
<s>
49.7	49.7	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
147.2	147.2	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
48	#num#	k4
km	km	kA
</s>
<s>
N	N	kA
</s>
<s>
47.2	47.2	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
150.8	150.8	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
26	#num#	k4
km	km	kA
</s>
<s>
Q	Q	kA
</s>
<s>
48.6	48.6	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
152.3	152.3	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
35	#num#	k4
km	km	kA
</s>
<s>
R	R	kA
</s>
<s>
48.6	48.6	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
154.7	154.7	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
23	#num#	k4
km	km	kA
</s>
<s>
S	s	k7c7
</s>
<s>
49.1	49.1	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
154.9	154.9	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
67	#num#	k4
km	km	kA
</s>
<s>
U	u	k7c2
</s>
<s>
50.4	50.4	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
154.7	154.7	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
30	#num#	k4
km	km	kA
</s>
<s>
X	X	kA
</s>
<s>
53.6	53.6	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
152.2	152.2	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
26	#num#	k4
km	km	kA
</s>
<s>
Y	Y	kA
</s>
<s>
53.1	53.1	k4
<g/>
°	°	k?
S	s	k7c7
</s>
<s>
150.4	150.4	k4
<g/>
°	°	k?
Z	z	k7c2
</s>
<s>
46	#num#	k4
km	km	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Crater	Cratra	k1gFnPc2
Paraskevopoulos	Paraskevopoulos	k1gInSc4
on	on	k3xPp3gMnSc1
Moon	Moon	k1gMnSc1
Gazetteer	Gazetteer	k1gMnSc1
of	of	k?
Planetary	Planetara	k1gFnSc2
Nomenclature	Nomenclatur	k1gMnSc5
<g/>
,	,	kIx,
IAU	IAU	kA
<g/>
,	,	kIx,
USGS	USGS	kA
<g/>
,	,	kIx,
NASA	NASA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
RÜKL	RÜKL	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Kráter	kráter	k1gInSc1
Paraskevopoulos	Paraskevopoulos	k1gInSc1
<g/>
,	,	kIx,
Moon-Wikispaces	Moon-Wikispaces	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LAC	LAC	kA
19	#num#	k4
<g/>
,	,	kIx,
mapa	mapa	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
000	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
Lambertova	Lambertův	k2eAgFnSc1d1
projekce	projekce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Planetární	planetární	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
</s>
