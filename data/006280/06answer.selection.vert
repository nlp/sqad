<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Johna	John	k1gMnSc4	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
.	.	kIx.	.
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
v	v	k7c6	v
texaském	texaský	k2eAgNnSc6d1	texaské
městě	město	k1gNnSc6	město
Dallas	Dallas	k1gInSc1	Dallas
ve	v	k7c6	v
12.30	[number]	k4	12.30
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
18.30	[number]	k4	18.30
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
