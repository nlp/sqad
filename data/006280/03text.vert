<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Johna	John	k1gMnSc4	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
.	.	kIx.	.
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
v	v	k7c6	v
texaském	texaský	k2eAgNnSc6d1	texaské
městě	město	k1gNnSc6	město
Dallas	Dallas	k1gInSc1	Dallas
ve	v	k7c6	v
12.30	[number]	k4	12.30
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
18.30	[number]	k4	18.30
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenned	k1gMnPc4	Kenned
byl	být	k5eAaImAgMnS	být
smrtelně	smrtelně	k6eAd1	smrtelně
raněn	ranit	k5eAaPmNgMnS	ranit
střelou	střela	k1gFnSc7	střela
z	z	k7c2	z
pušky	puška	k1gFnSc2	puška
Carcano	Carcana	k1gFnSc5	Carcana
M	M	kA	M
<g/>
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kampaně	kampaň	k1gFnSc2	kampaň
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
1964	[number]	k4	1964
projížděl	projíždět	k5eAaImAgInS	projíždět
ulicemi	ulice	k1gFnPc7	ulice
Dallasu	Dallas	k1gInSc6	Dallas
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
voze	vůz	k1gInSc6	vůz
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
manželkou	manželka	k1gFnSc7	manželka
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
<g/>
,	,	kIx,	,
texaským	texaský	k2eAgMnSc7d1	texaský
guvernérem	guvernér	k1gMnSc7	guvernér
Johnem	John	k1gMnSc7	John
Connallym	Connallym	k1gInSc4	Connallym
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
Nellie	Nellie	k1gFnSc2	Nellie
<g/>
.	.	kIx.	.
</s>
<s>
Vážně	vážně	k6eAd1	vážně
zraněn	zraněn	k2eAgMnSc1d1	zraněn
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
guvernér	guvernér	k1gMnSc1	guvernér
Connally	Connalla	k1gFnSc2	Connalla
<g/>
,	,	kIx,	,
atentát	atentát	k1gInSc4	atentát
však	však	k9	však
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vyšetření	vyšetření	k1gNnSc4	vyšetření
atentátu	atentát	k1gInSc2	atentát
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
zkr.	zkr.	kA	zkr.
HSCA	HSCA	kA	HSCA
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
vládních	vládní	k2eAgFnPc2d1	vládní
agentur	agentura	k1gFnPc2	agentura
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
atentát	atentát	k1gInSc1	atentát
byl	být	k5eAaImAgInS	být
spáchán	spáchán	k2eAgInSc1d1	spáchán
Lee	Lea	k1gFnSc3	Lea
Harvey	Harvea	k1gFnSc2	Harvea
Oswaldem	Oswald	k1gMnSc7	Oswald
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
příslušníkem	příslušník	k1gMnSc7	příslušník
Námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovní	sněmovní	k2eAgFnSc1d1	sněmovní
komise	komise	k1gFnSc1	komise
navíc	navíc	k6eAd1	navíc
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vražda	vražda	k1gFnSc1	vražda
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
"	"	kIx"	"
<g/>
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Povaha	povaha	k1gFnSc1	povaha
tohoto	tento	k3xDgNnSc2	tento
spiknutí	spiknutí	k1gNnSc2	spiknutí
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
vyjasněna	vyjasněn	k2eAgFnSc1d1	vyjasněna
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
řada	řada	k1gFnSc1	řada
podezření	podezření	k1gNnSc2	podezření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
atentátu	atentát	k1gInSc3	atentát
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
například	například	k6eAd1	například
kubánský	kubánský	k2eAgInSc4d1	kubánský
režim	režim	k1gInSc4	režim
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
kubánské	kubánský	k2eAgFnSc2d1	kubánská
komunity	komunita	k1gFnSc2	komunita
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
,	,	kIx,	,
mafie	mafie	k1gFnSc1	mafie
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc1	podezření
se	se	k3xPyFc4	se
nevyhnulo	vyhnout	k5eNaPmAgNnS	vyhnout
ani	ani	k8xC	ani
složkám	složka	k1gFnPc3	složka
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
jako	jako	k8xS	jako
FBI	FBI	kA	FBI
<g/>
,	,	kIx,	,
CIA	CIA	kA	CIA
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
viceprezidentovi	viceprezident	k1gMnSc3	viceprezident
Lyndonu	Lyndon	k1gMnSc3	Lyndon
B.	B.	kA	B.
Johnsonovi	Johnson	k1gMnSc3	Johnson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
po	po	k7c4	po
Kennedyho	Kennedy	k1gMnSc4	Kennedy
smrti	smrt	k1gFnSc2	smrt
převzal	převzít	k5eAaPmAgInS	převzít
úřad	úřad	k1gInSc1	úřad
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobný	pravděpodobný	k2eAgMnSc1d1	pravděpodobný
střelec	střelec	k1gMnSc1	střelec
Lee	Lea	k1gFnSc6	Lea
Harvey	Harvea	k1gFnPc1	Harvea
Oswald	Oswalda	k1gFnPc2	Oswalda
se	se	k3xPyFc4	se
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
nepřiznal	přiznat	k5eNaPmAgMnS	přiznat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
zadržení	zadržení	k1gNnSc6	zadržení
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
mimořádně	mimořádně	k6eAd1	mimořádně
populárního	populární	k2eAgMnSc2d1	populární
prezidenta	prezident	k1gMnSc2	prezident
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
ohlas	ohlas	k1gInSc1	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
a	a	k8xC	a
účastnilo	účastnit	k5eAaImAgNnS	účastnit
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc4	on
220	[number]	k4	220
zástupců	zástupce	k1gMnPc2	zástupce
zemí	zem	k1gFnPc2	zem
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nevyjímaje	nevyjímaje	k7c4	nevyjímaje
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
málokterá	málokterý	k3yIgFnSc1	málokterý
jiná	jiný	k2eAgFnSc1d1	jiná
politická	politický	k2eAgFnSc1d1	politická
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
zavraždění	zavraždění	k1gNnSc4	zavraždění
Kennedyho	Kennedy	k1gMnSc2	Kennedy
silně	silně	k6eAd1	silně
zapsalo	zapsat	k5eAaPmAgNnS	zapsat
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
veřejnosti	veřejnost	k1gFnSc2	veřejnost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
většina	většina	k1gFnSc1	většina
pamětníků	pamětník	k1gMnPc2	pamětník
dodnes	dodnes	k6eAd1	dodnes
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dělali	dělat	k5eAaImAgMnP	dělat
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
o	o	k7c4	o
poslední	poslední	k2eAgInSc4d1	poslední
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Kennedyho	Kennedyze	k6eAd1	Kennedyze
politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
letech	let	k1gInPc6	let
1947	[number]	k4	1947
<g/>
−	−	k?	−
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
stát	stát	k5eAaImF	stát
Massachusetts	Massachusetts	k1gNnSc4	Massachusetts
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgNnPc2d1	americké
jakožto	jakožto	k8xS	jakožto
demokrat	demokrat	k1gMnSc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1953	[number]	k4	1953
<g/>
−	−	k?	−
<g/>
1960	[number]	k4	1960
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
tentýž	týž	k3xTgInSc1	týž
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
demokratických	demokratický	k2eAgFnPc6d1	demokratická
primárních	primární	k2eAgFnPc6d1	primární
volbách	volba	k1gFnPc6	volba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
primárkách	primárky	k1gFnPc6	primárky
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
stanul	stanout	k5eAaPmAgMnS	stanout
proti	proti	k7c3	proti
senátorovi	senátor	k1gMnSc3	senátor
Hubertovi	Hubert	k1gMnSc6	Hubert
Humphreymu	Humphreym	k1gInSc2	Humphreym
ze	z	k7c2	z
státu	stát	k1gInSc2	stát
Minnesota	Minnesota	k1gFnSc1	Minnesota
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
proti	proti	k7c3	proti
Waynovi	Wayn	k1gMnSc3	Wayn
Morsemu	Morse	k1gMnSc3	Morse
z	z	k7c2	z
Oregonu	Oregon	k1gInSc2	Oregon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
oběma	dva	k4xCgInPc7	dva
protikandidáty	protikandidát	k1gMnPc7	protikandidát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
oponentem	oponent	k1gMnSc7	oponent
v	v	k7c6	v
demokratické	demokratický	k2eAgFnSc6d1	demokratická
straně	strana	k1gFnSc6	strana
stal	stát	k5eAaPmAgMnS	stát
texaský	texaský	k2eAgMnSc1d1	texaský
senátor	senátor	k1gMnSc1	senátor
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
ze	z	k7c2	z
shromáždění	shromáždění	k1gNnPc2	shromáždění
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
Kennedy	Kenneda	k1gMnSc2	Kenneda
přednesl	přednést	k5eAaPmAgMnS	přednést
svůj	svůj	k3xOyFgInSc4	svůj
slavný	slavný	k2eAgInSc4d1	slavný
projev	projev	k1gInSc4	projev
o	o	k7c6	o
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgFnSc6d1	Nové
hranici	hranice	k1gFnSc6	hranice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
New	New	k1gMnSc1	New
frontier	frontier	k1gMnSc1	frontier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ohlašující	ohlašující	k2eAgFnPc1d1	ohlašující
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
projít	projít	k5eAaPmF	projít
Amerika	Amerika	k1gFnSc1	Amerika
i	i	k8xC	i
zbytek	zbytek	k1gInSc1	zbytek
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1960	[number]	k4	1960
se	se	k3xPyFc4	se
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nominovat	nominovat	k5eAaBmF	nominovat
Kennedyho	Kennedy	k1gMnSc4	Kennedy
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc4	svůj
kandidáta	kandidát	k1gMnSc4	kandidát
do	do	k7c2	do
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
většinově	většinově	k6eAd1	většinově
protestantská	protestantský	k2eAgFnSc1d1	protestantská
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
měla	mít	k5eAaImAgFnS	mít
jisté	jistý	k2eAgFnPc4d1	jistá
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
římskokatolického	římskokatolický	k2eAgNnSc2d1	římskokatolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
Kennedy	Kenneda	k1gMnSc2	Kenneda
pronést	pronést	k5eAaPmF	pronést
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1960	[number]	k4	1960
projev	projev	k1gInSc1	projev
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
zazněla	zaznít	k5eAaPmAgFnS	zaznít
slavná	slavný	k2eAgFnSc1d1	slavná
věta	věta	k1gFnSc1	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemluvím	mluvit	k5eNaImIp1nS	mluvit
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
církev	církev	k1gFnSc4	církev
do	do	k7c2	do
věcí	věc	k1gFnPc2	věc
veřejných	veřejný	k2eAgFnPc2d1	veřejná
a	a	k8xC	a
má	můj	k3xOp1gFnSc1	můj
církev	církev	k1gFnSc1	církev
nemluví	mluvit	k5eNaImIp3nS	mluvit
za	za	k7c4	za
mě	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
1960	[number]	k4	1960
byly	být	k5eAaImAgInP	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
televizí	televize	k1gFnSc7	televize
přenášeny	přenášen	k2eAgFnPc1d1	přenášena
debaty	debata	k1gFnPc1	debata
mezi	mezi	k7c7	mezi
prezidentskými	prezidentský	k2eAgMnPc7d1	prezidentský
kandidáty	kandidát	k1gMnPc7	kandidát
Kennedym	Kennedymum	k1gNnPc2	Kennedymum
a	a	k8xC	a
republikánem	republikán	k1gMnSc7	republikán
Richardem	Richard	k1gMnSc7	Richard
Nixonem	Nixon	k1gMnSc7	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začala	začít	k5eAaPmAgFnS	začít
televize	televize	k1gFnSc1	televize
hrát	hrát	k5eAaImF	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
Kennedy	Kenneda	k1gMnSc2	Kenneda
svého	svůj	k1gMnSc2	svůj
protikandidáta	protikandidát	k1gMnSc2	protikandidát
Nixona	Nixon	k1gMnSc2	Nixon
porazil	porazit	k5eAaPmAgMnS	porazit
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejtěsnějších	těsný	k2eAgInPc2d3	nejtěsnější
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
katolickým	katolický	k2eAgInSc7d1	katolický
a	a	k8xC	a
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
43	[number]	k4	43
let	léto	k1gNnPc2	léto
druhým	druhý	k4xOgNnSc7	druhý
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
USA	USA	kA	USA
(	(	kIx(	(
<g/>
po	po	k7c6	po
Theodoru	Theodor	k1gMnSc6	Theodor
Rooseveltovi	Roosevelt	k1gMnSc6	Roosevelt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
prezident	prezident	k1gMnSc1	prezident
stal	stát	k5eAaPmAgMnS	stát
držitelem	držitel	k1gMnSc7	držitel
Pulitzerovy	Pulitzerův	k2eAgFnSc2d1	Pulitzerova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
vnitrostátní	vnitrostátní	k2eAgInSc1d1	vnitrostátní
program	program	k1gInSc1	program
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k8xC	jako
Nová	nový	k2eAgFnSc1d1	nová
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zlepšení	zlepšení	k1gNnSc1	zlepšení
financování	financování	k1gNnSc2	financování
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
jenž	jenž	k3xRgInSc1	jenž
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
ekonomická	ekonomický	k2eAgNnPc4d1	ekonomické
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
recese	recese	k1gFnSc2	recese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
daňovou	daňový	k2eAgFnSc4d1	daňová
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
i	i	k9	i
daňové	daňový	k2eAgInPc4d1	daňový
škrty	škrt	k1gInPc4	škrt
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc4	návrh
však	však	k8xC	však
prošel	projít	k5eAaPmAgMnS	projít
kongresem	kongres	k1gInSc7	kongres
až	až	k8xS	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenneda	k1gMnSc2	Kenneda
se	se	k3xPyFc4	se
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
o	o	k7c4	o
integraci	integrace	k1gFnSc4	integrace
černošské	černošský	k2eAgFnSc2d1	černošská
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
diskriminaci	diskriminace	k1gFnSc3	diskriminace
menšin	menšina	k1gFnPc2	menšina
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
také	také	k9	také
zlepšit	zlepšit	k5eAaPmF	zlepšit
postavení	postavení	k1gNnSc4	postavení
amerických	americký	k2eAgFnPc2d1	americká
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Kennedyho	Kennedy	k1gMnSc2	Kennedy
také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
imigrační	imigrační	k2eAgFnSc6d1	imigrační
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vydal	vydat	k5eAaPmAgInS	vydat
zákon	zákon	k1gInSc1	zákon
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgInSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Immigration	Immigration	k1gInSc1	Immigration
and	and	k?	and
Nationality	Nationalita	k1gFnSc2	Nationalita
Act	Act	k1gFnSc2	Act
of	of	k?	of
1965	[number]	k4	1965
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
zformuloval	zformulovat	k5eAaPmAgInS	zformulovat
záměr	záměr	k1gInSc4	záměr
vyslat	vyslat	k5eAaPmF	vyslat
pilotovanou	pilotovaný	k2eAgFnSc4d1	pilotovaná
misi	mise	k1gFnSc4	mise
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
letět	letět	k5eAaImF	letět
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
desetiletí	desetiletí	k1gNnSc6	desetiletí
letět	letět	k5eAaImF	letět
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
dokázat	dokázat	k5eAaPmF	dokázat
ještě	ještě	k6eAd1	ještě
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kennedy	Kenneda	k1gMnSc2	Kenneda
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
netajil	tajit	k5eNaImAgInS	tajit
negativním	negativní	k2eAgInSc7d1	negativní
postojem	postoj	k1gInSc7	postoj
vůči	vůči	k7c3	vůči
komunismu	komunismus	k1gInSc3	komunismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
svým	svůj	k3xOyFgInSc7	svůj
výrokem	výrok	k1gInSc7	výrok
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
města	město	k1gNnSc2	město
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nechápou	chápat	k5eNaImIp3nP	chápat
nebo	nebo	k8xC	nebo
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechápou	chápat	k5eNaImIp3nP	chápat
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yRnSc6	co
spočívá	spočívat	k5eAaImIp3nS	spočívat
rozpor	rozpor	k1gInSc4	rozpor
mezi	mezi	k7c7	mezi
svobodným	svobodný	k2eAgInSc7d1	svobodný
světem	svět	k1gInSc7	svět
a	a	k8xC	a
komunistickým	komunistický	k2eAgInSc7d1	komunistický
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
přijdou	přijít	k5eAaPmIp3nP	přijít
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
!	!	kIx.	!
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunismus	komunismus	k1gInSc1	komunismus
je	být	k5eAaImIp3nS	být
vlna	vlna	k1gFnSc1	vlna
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
přijdou	přijít	k5eAaPmIp3nP	přijít
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k9	i
jinde	jinde	k6eAd1	jinde
můžeme	moct	k5eAaImIp1nP	moct
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
přijdou	přijít	k5eAaPmIp3nP	přijít
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
!	!	kIx.	!
</s>
<s>
A	a	k9	a
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunismus	komunismus	k1gInSc1	komunismus
je	být	k5eAaImIp3nS	být
zlý	zlý	k2eAgInSc1d1	zlý
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nám	my	k3xPp1nPc3	my
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
vzestup	vzestup	k1gInSc1	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Lass	Lass	k1gInSc1	Lass
<g/>
'	'	kIx"	'
sie	sie	k?	sie
nach	nach	k1gInSc1	nach
Berlin	berlina	k1gFnPc2	berlina
kommen	kommen	k1gInSc4	kommen
−	−	k?	−
ať	ať	k8xS	ať
přijdou	přijít	k5eAaPmIp3nP	přijít
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Atentát	atentát	k1gInSc1	atentát
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
ve	v	k7c6	v
složitém	složitý	k2eAgNnSc6d1	složité
období	období	k1gNnSc6	období
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
k	k	k7c3	k
neúspěšnému	úspěšný	k2eNgNnSc3d1	neúspěšné
vylodění	vylodění	k1gNnSc3	vylodění
v	v	k7c6	v
Zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
<g/>
,	,	kIx,	,
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
svržení	svržení	k1gNnSc4	svržení
režimu	režim	k1gInSc2	režim
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgNnSc6	který
svět	svět	k1gInSc1	svět
vzápětí	vzápětí	k6eAd1	vzápětí
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
jaderného	jaderný	k2eAgInSc2d1	jaderný
konfliktu	konflikt	k1gInSc2	konflikt
kvůli	kvůli	k7c3	kvůli
Karibské	karibský	k2eAgFnSc3d1	karibská
krizi	krize	k1gFnSc3	krize
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
před	před	k7c7	před
dilematem	dilema	k1gNnSc7	dilema
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
USA	USA	kA	USA
vojensky	vojensky	k6eAd1	vojensky
zasáhnou	zasáhnout	k5eAaPmIp3nP	zasáhnout
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
by	by	kYmCp3nS	by
situace	situace	k1gFnSc1	situace
vést	vést	k5eAaImF	vést
k	k	k7c3	k
jadernému	jaderný	k2eAgInSc3d1	jaderný
konfliktu	konflikt	k1gInSc3	konflikt
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
však	však	k9	však
situaci	situace	k1gFnSc4	situace
nevyřeší	vyřešit	k5eNaPmIp3nS	vyřešit
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
by	by	kYmCp3nP	by
USA	USA	kA	USA
ohroženy	ohrožen	k2eAgInPc1d1	ohrožen
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
jadernými	jaderný	k2eAgFnPc7d1	jaderná
zbraněmi	zbraň	k1gFnPc7	zbraň
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
začal	začít	k5eAaPmAgInS	začít
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
vůdcem	vůdce	k1gMnSc7	vůdce
Nikitou	Nikita	k1gMnSc7	Nikita
Chruščovem	Chruščov	k1gInSc7	Chruščov
dospěli	dochvít	k5eAaPmAgMnP	dochvít
po	po	k7c6	po
týdenním	týdenní	k2eAgNnSc6d1	týdenní
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uchránila	uchránit	k5eAaPmAgFnS	uchránit
svět	svět	k1gInSc4	svět
od	od	k7c2	od
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vybudována	vybudován	k2eAgFnSc1d1	vybudována
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
a	a	k8xC	a
začaly	začít	k5eAaPmAgInP	začít
první	první	k4xOgInPc1	první
střety	střet	k1gInPc1	střet
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naplno	naplno	k6eAd1	naplno
propukla	propuknout	k5eAaPmAgFnS	propuknout
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Kennedyho	Kennedy	k1gMnSc2	Kennedy
nástupce	nástupce	k1gMnSc2	nástupce
Lyndona	Lyndon	k1gMnSc2	Lyndon
Johnsona	Johnson	k1gMnSc2	Johnson
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Kennedyho	Kennedy	k1gMnSc2	Kennedy
smrti	smrt	k1gFnSc6	smrt
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
závod	závod	k1gInSc1	závod
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
korunovaný	korunovaný	k2eAgInSc1d1	korunovaný
úspěchem	úspěch	k1gInSc7	úspěch
americké	americký	k2eAgFnSc2d1	americká
mise	mise	k1gFnSc2	mise
Apollo	Apollo	k1gMnSc1	Apollo
11	[number]	k4	11
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
lidská	lidský	k2eAgFnSc1d1	lidská
noha	noha	k1gFnSc1	noha
poprvé	poprvé	k6eAd1	poprvé
stanula	stanout	k5eAaPmAgFnS	stanout
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
si	se	k3xPyFc3	se
Dallas	Dallas	k1gInSc4	Dallas
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
návštěvu	návštěva	k1gFnSc4	návštěva
vybral	vybrat	k5eAaPmAgMnS	vybrat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
aby	aby	kYmCp3nS	aby
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
volební	volební	k2eAgFnSc7d1	volební
kampaní	kampaň	k1gFnSc7	kampaň
jako	jako	k8xS	jako
takovou	takový	k3xDgFnSc4	takový
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zajištění	zajištění	k1gNnSc3	zajištění
dalších	další	k2eAgFnPc2d1	další
financí	finance	k1gFnPc2	finance
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
předvolební	předvolební	k2eAgFnSc4d1	předvolební
kampaň	kampaň	k1gFnSc4	kampaň
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
chtěl	chtít	k5eAaImAgMnS	chtít
svou	svůj	k3xOyFgFnSc4	svůj
kampaň	kampaň	k1gFnSc4	kampaň
zahájit	zahájit	k5eAaPmF	zahájit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
získal	získat	k5eAaPmAgInS	získat
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
listopadové	listopadový	k2eAgFnSc6d1	listopadová
návštěvě	návštěva	k1gFnSc6	návštěva
Dallasu	Dallas	k1gInSc2	Dallas
padlo	padnout	k5eAaPmAgNnS	padnout
během	během	k7c2	během
setkání	setkání	k1gNnSc2	setkání
Kennedyho	Kennedy	k1gMnSc2	Kennedy
s	s	k7c7	s
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
Johnsonem	Johnson	k1gMnSc7	Johnson
a	a	k8xC	a
texaským	texaský	k2eAgMnSc7d1	texaský
guvernérem	guvernér	k1gMnSc7	guvernér
Johnem	John	k1gMnSc7	John
Connallym	Connallym	k1gInSc4	Connallym
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Dallasu	Dallas	k1gInSc2	Dallas
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
obavy	obava	k1gFnPc1	obava
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
především	především	k9	především
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
davem	dav	k1gInSc7	dav
demonstrujících	demonstrující	k2eAgMnPc2d1	demonstrující
pravicových	pravicový	k2eAgMnPc2d1	pravicový
radikálů	radikál	k1gMnPc2	radikál
během	během	k7c2	během
oslav	oslava	k1gFnPc2	oslava
Dne	den	k1gInSc2	den
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
konaných	konaný	k2eAgNnPc2d1	konané
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
fyzicky	fyzicky	k6eAd1	fyzicky
i	i	k9	i
slovně	slovně	k6eAd1	slovně
napaden	napaden	k2eAgInSc1d1	napaden
Adlai	Adla	k1gFnSc3	Adla
Stevenson	Stevenson	k1gMnSc1	Stevenson
<g/>
,	,	kIx,	,
vyslanec	vyslanec	k1gMnSc1	vyslanec
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
při	při	k7c6	při
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Znepokojení	znepokojení	k1gNnSc4	znepokojení
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
také	také	k9	také
možnost	možnost	k1gFnSc1	možnost
útoku	útok	k1gInSc2	útok
skrytého	skrytý	k2eAgMnSc2d1	skrytý
odstřelovače	odstřelovač	k1gMnSc2	odstřelovač
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Kennedy	Kenned	k1gMnPc4	Kenned
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kolony	kolona	k1gFnSc2	kolona
Dallasem	Dallas	k1gInSc7	Dallas
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
již	již	k6eAd1	již
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
oběma	dva	k4xCgFnPc7	dva
dallaskými	dallaský	k2eAgFnPc7d1	dallaská
novinami	novina	k1gFnPc7	novina
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejněn	k2eAgInSc1d1	zveřejněn
plánek	plánek	k1gInSc1	plánek
trasy	trasa	k1gFnSc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kolona	kolona	k1gFnSc1	kolona
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
začít	začít	k5eAaPmF	začít
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Love	lov	k1gInSc5	lov
Field	Field	k1gInSc4	Field
<g/>
,	,	kIx,	,
pokračovat	pokračovat	k5eAaImF	pokračovat
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
Dallasu	Dallas	k1gInSc2	Dallas
<g/>
,	,	kIx,	,
projet	projet	k5eAaPmF	projet
parkem	park	k1gInSc7	park
Dealey	Dealea	k1gFnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
měl	mít	k5eAaImAgMnS	mít
přednést	přednést	k5eAaPmF	přednést
projev	projev	k1gInSc4	projev
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Dallas	Dallas	k1gInSc1	Dallas
Trade	Trad	k1gInSc5	Trad
Mart	Marta	k1gFnPc2	Marta
<g/>
.	.	kIx.	.
</s>
<s>
Automobil	automobil	k1gInSc1	automobil
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenned	k1gMnPc4	Kenned
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
speciálně	speciálně	k6eAd1	speciálně
upravená	upravený	k2eAgFnSc1d1	upravená
limuzína	limuzína	k1gFnSc1	limuzína
Lincoln	Lincoln	k1gMnSc1	Lincoln
Continental	Continental	k1gMnSc1	Continental
ročník	ročník	k1gInSc4	ročník
1961	[number]	k4	1961
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
střechou	střecha	k1gFnSc7	střecha
a	a	k8xC	a
kódovým	kódový	k2eAgNnSc7d1	kódové
označením	označení	k1gNnSc7	označení
SS-	SS-	k1gFnSc1	SS-
<g/>
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
limuzína	limuzína	k1gFnSc1	limuzína
jela	jet	k5eAaImAgFnS	jet
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
kolony	kolona	k1gFnSc2	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
ji	on	k3xPp3gFnSc4	on
měli	mít	k5eAaImAgMnP	mít
poklusem	poklus	k1gInSc7	poklus
doprovázet	doprovázet	k5eAaImF	doprovázet
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
prezidentovy	prezidentův	k2eAgFnSc2d1	prezidentova
tělesné	tělesný	k2eAgFnSc2d1	tělesná
stráže	stráž	k1gFnSc2	stráž
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
ovšem	ovšem	k9	ovšem
byli	být	k5eAaImAgMnP	být
během	během	k7c2	během
výjezdu	výjezd	k1gInSc2	výjezd
kolony	kolona	k1gFnSc2	kolona
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
odvoláni	odvolat	k5eAaPmNgMnP	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
Dallasu	Dallas	k1gInSc2	Dallas
nechal	nechat	k5eAaPmAgMnS	nechat
prezident	prezident	k1gMnSc1	prezident
kolonu	kolona	k1gFnSc4	kolona
dvakrát	dvakrát	k6eAd1	dvakrát
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spatřil	spatřit	k5eAaPmAgInS	spatřit
houf	houf	k1gInSc1	houf
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
společně	společně	k6eAd1	společně
nad	nad	k7c7	nad
hlavami	hlava	k1gFnPc7	hlava
držely	držet	k5eAaImAgFnP	držet
transparent	transparent	k1gInSc4	transparent
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
prezidente	prezident	k1gMnSc5	prezident
<g/>
,	,	kIx,	,
prosíme	prosit	k5eAaImIp1nP	prosit
<g/>
,	,	kIx,	,
zastavte	zastavit	k5eAaPmRp2nP	zastavit
a	a	k8xC	a
potřeste	potřást	k5eAaPmRp2nP	potřást
si	se	k3xPyFc3	se
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podruhé	podruhé	k6eAd1	podruhé
zastavil	zastavit	k5eAaPmAgInS	zastavit
u	u	k7c2	u
skupinky	skupinka	k1gFnSc2	skupinka
řádových	řádový	k2eAgFnPc2d1	řádová
sester	sestra	k1gFnPc2	sestra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
pozdravil	pozdravit	k5eAaPmAgMnS	pozdravit
a	a	k8xC	a
prohodil	prohodit	k5eAaPmAgMnS	prohodit
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
pár	pár	k4xCyI	pár
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Reaganovy	Reaganův	k2eAgFnSc2d1	Reaganova
ulice	ulice	k1gFnSc2	ulice
zahlédl	zahlédnout	k5eAaPmAgInS	zahlédnout
Kennedy	Kenned	k1gMnPc4	Kenned
v	v	k7c6	v
davu	dav	k1gInSc6	dav
lidí	člověk	k1gMnPc2	člověk
kněze	kněz	k1gMnPc4	kněz
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
kolárkem	kolárek	k1gInSc7	kolárek
<g/>
,	,	kIx,	,
zvedl	zvednout	k5eAaPmAgMnS	zvednout
se	se	k3xPyFc4	se
ze	z	k7c2	z
sedadla	sedadlo	k1gNnSc2	sedadlo
a	a	k8xC	a
široce	široko	k6eAd1	široko
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
usmál	usmát	k5eAaPmAgMnS	usmát
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgMnSc7	tento
knězem	kněz	k1gMnSc7	kněz
byl	být	k5eAaImAgMnS	být
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
Oscar	Oscar	k1gMnSc1	Oscar
Hubert	Hubert	k1gMnSc1	Hubert
z	z	k7c2	z
dallaského	dallaský	k2eAgInSc2d1	dallaský
Kostela	kostel	k1gInSc2	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
o	o	k7c4	o
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
Kennedymu	Kennedym	k1gInSc2	Kennedym
udělil	udělit	k5eAaPmAgMnS	udělit
poslední	poslední	k2eAgNnSc4d1	poslední
pomazání	pomazání	k1gNnSc4	pomazání
<g/>
.	.	kIx.	.
</s>
<s>
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
je	být	k5eAaImIp3nS	být
náměstí	náměstí	k1gNnSc4	náměstí
parkového	parkový	k2eAgInSc2d1	parkový
charakteru	charakter	k1gInSc2	charakter
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
podle	podle	k7c2	podle
vydavatele	vydavatel	k1gMnSc2	vydavatel
prvních	první	k4xOgInPc2	první
dallaských	dallaský	k2eAgFnPc2d1	dallaská
novin	novina	k1gFnPc2	novina
The	The	k1gFnSc1	The
Dallas	Dallas	k1gInSc1	Dallas
Morning	Morning	k1gInSc1	Morning
News	News	k1gInSc1	News
George	Georg	k1gMnSc2	Georg
Bannermana	Bannerman	k1gMnSc2	Bannerman
Dealeyho	Dealey	k1gMnSc2	Dealey
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
centra	centrum	k1gNnSc2	centrum
Dallasu	Dallas	k1gInSc2	Dallas
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
východu	východ	k1gInSc2	východ
a	a	k8xC	a
severu	sever	k1gInSc2	sever
je	být	k5eAaImIp3nS	být
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třicet	třicet	k4xCc1	třicet
metrů	metr	k1gMnPc2	metr
vysokými	vysoký	k2eAgFnPc7d1	vysoká
budovami	budova	k1gFnPc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
budovy	budova	k1gFnPc4	budova
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Texas	Texas	k1gInSc1	Texas
School	Schoola	k1gFnPc2	Schoola
Book	Book	k1gInSc1	Book
Depository	Depositor	k1gInPc1	Depositor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnPc1	společnost
Dal-Tex	Dal-Tex	k1gInSc1	Dal-Tex
<g/>
,	,	kIx,	,
okresního	okresní	k2eAgInSc2d1	okresní
archivu	archiv	k1gInSc2	archiv
<g/>
,	,	kIx,	,
Dallaského	Dallaský	k2eAgInSc2d1	Dallaský
trestního	trestní	k2eAgInSc2d1	trestní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
starého	starý	k2eAgInSc2d1	starý
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
rodného	rodný	k2eAgInSc2d1	rodný
domu	dům	k1gInSc2	dům
zakladatele	zakladatel	k1gMnSc2	zakladatel
Dallasu	Dallas	k1gInSc2	Dallas
Johna	John	k1gMnSc2	John
Neely	Neela	k1gMnSc2	Neela
Bryana	Bryan	k1gMnSc2	Bryan
a	a	k8xC	a
o	o	k7c4	o
budovu	budova	k1gFnSc4	budova
Poštovního	poštovní	k2eAgInSc2d1	poštovní
úřadu	úřad	k1gInSc2	úřad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
travnatý	travnatý	k2eAgInSc1d1	travnatý
pahorek	pahorek	k1gInSc1	pahorek
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
grassy	grass	k1gInPc4	grass
knoll	knollum	k1gNnPc2	knollum
<g/>
)	)	kIx)	)
lemovaný	lemovaný	k2eAgInSc1d1	lemovaný
betonovou	betonový	k2eAgFnSc7d1	betonová
pergolou	pergola	k1gFnSc7	pergola
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
též	též	k6eAd1	též
dřevěným	dřevěný	k2eAgInSc7d1	dřevěný
plotem	plot	k1gInSc7	plot
<g/>
.	.	kIx.	.
</s>
<s>
Pomyslnou	pomyslný	k2eAgFnSc4d1	pomyslná
jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
náměstí	náměstí	k1gNnSc2	náměstí
tvoří	tvořit	k5eAaImIp3nP	tvořit
trojitý	trojitý	k2eAgInSc4d1	trojitý
železniční	železniční	k2eAgInSc4d1	železniční
viadukt	viadukt	k1gInSc4	viadukt
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
ulice	ulice	k1gFnSc1	ulice
Elm	Elm	k1gFnSc1	Elm
<g/>
,	,	kIx,	,
Main	Main	k1gNnSc1	Main
a	a	k8xC	a
Commerce	Commerka	k1gFnSc6	Commerka
street	streeta	k1gFnPc2	streeta
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
velký	velký	k2eAgInSc4d1	velký
silniční	silniční	k2eAgInSc4d1	silniční
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Dealey	Deale	k1gMnPc4	Deale
Plaza	plaz	k1gMnSc4	plaz
obvykle	obvykle	k6eAd1	obvykle
zaplněno	zaplněn	k2eAgNnSc1d1	zaplněno
množstvím	množství	k1gNnSc7	množství
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
i	i	k9	i
místní	místní	k2eAgNnSc4d1	místní
Muzeum	muzeum	k1gNnSc4	muzeum
Sixth	Sixth	k1gMnSc1	Sixth
Floor	Floor	k1gMnSc1	Floor
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
nejvyšších	vysoký	k2eAgNnPc6d3	nejvyšší
patrech	patro	k1gNnPc6	patro
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
navštíveno	navštívit	k5eAaPmNgNnS	navštívit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šesti	šest	k4xCc7	šest
miliony	milion	k4xCgInPc7	milion
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
expozice	expozice	k1gFnPc4	expozice
věnující	věnující	k2eAgFnPc4d1	věnující
se	se	k3xPyFc4	se
okolnostem	okolnost	k1gFnPc3	okolnost
atentátu	atentát	k1gInSc2	atentát
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zde	zde	k6eAd1	zde
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
množství	množství	k1gNnSc4	množství
autentického	autentický	k2eAgInSc2d1	autentický
fotografického	fotografický	k2eAgInSc2d1	fotografický
či	či	k8xC	či
audiovizuálního	audiovizuální	k2eAgInSc2d1	audiovizuální
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
podkapitola	podkapitola	k1gFnSc1	podkapitola
popisuje	popisovat	k5eAaImIp3nS	popisovat
průběh	průběh	k1gInSc4	průběh
atentátu	atentát	k1gInSc2	atentát
dle	dle	k7c2	dle
závěrů	závěr	k1gInPc2	závěr
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
komise	komise	k1gFnSc1	komise
HSCA	HSCA	kA	HSCA
Chvíli	chvíle	k1gFnSc4	chvíle
před	před	k7c4	před
12.30	[number]	k4	12.30
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
vjela	vjet	k5eAaPmAgFnS	vjet
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
limuzína	limuzína	k1gFnSc1	limuzína
na	na	k7c4	na
lidmi	člověk	k1gMnPc7	člověk
zcela	zcela	k6eAd1	zcela
zaplněné	zaplněný	k2eAgNnSc4d1	zaplněné
náměstí	náměstí	k1gNnSc4	náměstí
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
blížila	blížit	k5eAaImAgFnS	blížit
k	k	k7c3	k
Texaskému	texaský	k2eAgInSc3d1	texaský
knižnímu	knižní	k2eAgInSc3d1	knižní
velkoskladu	velkosklad	k1gInSc3	velkosklad
<g/>
.	.	kIx.	.
</s>
<s>
Řidičem	řidič	k1gMnSc7	řidič
limuzíny	limuzína	k1gFnSc2	limuzína
byl	být	k5eAaImAgMnS	být
agent	agent	k1gMnSc1	agent
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
Bill	Bill	k1gMnSc1	Bill
Greer	Greer	k1gMnSc1	Greer
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
pro	pro	k7c4	pro
spolujezdce	spolujezdec	k1gMnSc4	spolujezdec
seděl	sedět	k5eAaImAgMnS	sedět
agent	agent	k1gMnSc1	agent
Roy	Roy	k1gMnSc1	Roy
Kellerman	Kellerman	k1gMnSc1	Kellerman
<g/>
.	.	kIx.	.
</s>
<s>
Nellie	Nellie	k1gFnSc1	Nellie
Connallyová	Connallyová	k1gFnSc1	Connallyová
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
guvernéra	guvernér	k1gMnSc2	guvernér
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
otočila	otočit	k5eAaPmAgFnS	otočit
ke	k	k7c3	k
Kennedymu	Kennedym	k1gInSc3	Kennedym
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
seděl	sedět	k5eAaImAgMnS	sedět
na	na	k7c6	na
zadním	zadní	k2eAgNnSc6d1	zadní
sedadle	sedadlo	k1gNnSc6	sedadlo
a	a	k8xC	a
řekla	říct	k5eAaPmAgFnS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
prezidente	prezident	k1gMnSc5	prezident
<g/>
,	,	kIx,	,
rozhodně	rozhodně	k6eAd1	rozhodně
nemůžete	moct	k5eNaImIp2nP	moct
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vás	vy	k3xPp2nPc4	vy
Dallas	Dallas	k1gMnSc1	Dallas
nemiluje	milovat	k5eNaImIp3nS	milovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
prezident	prezident	k1gMnSc1	prezident
s	s	k7c7	s
potěšením	potěšení	k1gNnSc7	potěšení
uznal	uznat	k5eAaPmAgInS	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
limuzína	limuzína	k1gFnSc1	limuzína
projela	projet	k5eAaPmAgFnS	projet
kolem	kolem	k7c2	kolem
budovy	budova	k1gFnSc2	budova
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
vjíždět	vjíždět	k5eAaImF	vjíždět
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
Elm	Elm	k1gMnSc1	Elm
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
kdosi	kdosi	k3yInSc1	kdosi
zahájil	zahájit	k5eAaPmAgMnS	zahájit
palbu	palba	k1gFnSc4	palba
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
;	;	kIx,	;
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
svědků	svědek	k1gMnPc2	svědek
vyslechnutých	vyslechnutý	k2eAgMnPc2d1	vyslechnutý
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
uváděla	uvádět	k5eAaImAgFnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
padly	padnout	k5eAaImAgInP	padnout
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc1	tři
výstřely	výstřel	k1gInPc1	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
první	první	k4xOgFnSc2	první
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Kennedyho	Kennedy	k1gMnSc2	Kennedy
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
výpovědí	výpověď	k1gFnPc2	výpověď
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
davu	dav	k1gInSc2	dav
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
určit	určit	k5eAaPmF	určit
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
byla	být	k5eAaImAgFnS	být
vypálena	vypálen	k2eAgFnSc1d1	vypálena
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
svědků	svědek	k1gMnPc2	svědek
později	pozdě	k6eAd2	pozdě
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc4	první
výstřel	výstřel	k1gInSc4	výstřel
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
ohňostroj	ohňostroj	k1gInSc4	ohňostroj
nebo	nebo	k8xC	nebo
za	za	k7c4	za
ránu	rána	k1gFnSc4	rána
z	z	k7c2	z
výfuku	výfuk	k1gInSc2	výfuk
staršího	starý	k2eAgInSc2d2	starší
automobilu	automobil	k1gInSc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
F.	F.	kA	F.
Kennedy	Kenned	k1gMnPc7	Kenned
a	a	k8xC	a
John	John	k1gMnSc1	John
Connally	Connalla	k1gFnSc2	Connalla
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
seděli	sedět	k5eAaImAgMnP	sedět
po	po	k7c6	po
boku	bok	k1gInSc6	bok
svých	svůj	k3xOyFgFnPc2	svůj
manželek	manželka	k1gFnPc2	manželka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
po	po	k7c6	po
první	první	k4xOgFnSc6	první
střele	střela	k1gFnSc6	střela
začali	začít	k5eAaPmAgMnP	začít
zmateně	zmateně	k6eAd1	zmateně
ohlížet	ohlížet	k5eAaImF	ohlížet
kolem	kolem	k7c2	kolem
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Connally	Connall	k1gMnPc4	Connall
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zvuk	zvuk	k1gInSc4	zvuk
pušky	puška	k1gFnSc2	puška
velmi	velmi	k6eAd1	velmi
velké	velký	k2eAgFnSc2d1	velká
ráže	ráže	k1gFnSc2	ráže
a	a	k8xC	a
vykřikl	vykřiknout	k5eAaPmAgMnS	vykřiknout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ó	ó	k0	ó
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Otočil	otočit	k5eAaPmAgMnS	otočit
se	se	k3xPyFc4	se
doprava	doprava	k6eAd1	doprava
a	a	k8xC	a
poté	poté	k6eAd1	poté
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podíval	podívat	k5eAaPmAgMnS	podívat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
sedícího	sedící	k2eAgMnSc4d1	sedící
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zpráv	zpráva	k1gFnPc2	zpráva
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
HSCA	HSCA	kA	HSCA
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prezident	prezident	k1gMnSc1	prezident
mával	mávat	k5eAaImAgMnS	mávat
davům	dav	k1gInPc3	dav
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
pravici	pravice	k1gFnSc6	pravice
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
vnikla	vniknout	k5eAaPmAgFnS	vniknout
kulka	kulka	k1gFnSc1	kulka
do	do	k7c2	do
vrchní	vrchní	k2eAgFnSc2d1	vrchní
části	část	k1gFnSc2	část
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
prošla	projít	k5eAaPmAgFnS	projít
jeho	jeho	k3xOp3gInSc7	jeho
krkem	krk	k1gInSc7	krk
a	a	k8xC	a
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ven	ven	k6eAd1	ven
jícnem	jícen	k1gInSc7	jícen
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zvedl	zvednout	k5eAaPmAgMnS	zvednout
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
chytil	chytit	k5eAaPmAgMnS	chytit
si	se	k3xPyFc3	se
krk	krk	k1gInSc4	krk
a	a	k8xC	a
zavrávoral	zavrávorat	k5eAaPmAgMnS	zavrávorat
nejprve	nejprve	k6eAd1	nejprve
směrem	směr	k1gInSc7	směr
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
následně	následně	k6eAd1	následně
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ho	on	k3xPp3gInSc4	on
jeho	on	k3xPp3gInSc4	on
manželka	manželka	k1gFnSc1	manželka
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
v	v	k7c6	v
šoku	šok	k1gInSc6	šok
objímala	objímat	k5eAaImAgFnS	objímat
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
ne	ne	k9	ne
<g/>
,	,	kIx,	,
můj	můj	k1gMnSc5	můj
bože	bůh	k1gMnSc5	bůh
<g/>
,	,	kIx,	,
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
mi	já	k3xPp1nSc3	já
manžela	manžel	k1gMnSc2	manžel
<g/>
...	...	k?	...
miluji	milovat	k5eAaImIp1nS	milovat
tě	ty	k3xPp2nSc4	ty
Jacku	Jack	k1gInSc6	Jack
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
Kellerman	Kellerman	k1gMnSc1	Kellerman
prý	prý	k9	prý
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
zaslechl	zaslechnout	k5eAaPmAgMnS	zaslechnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
zachroptět	zachroptět	k5eAaPmF	zachroptět
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Můj	můj	k1gMnSc5	můj
Bože	bůh	k1gMnSc5	bůh
<g/>
,	,	kIx,	,
trefili	trefit	k5eAaPmAgMnP	trefit
mě	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Guvernéra	guvernér	k1gMnSc2	guvernér
Connallyho	Connally	k1gMnSc2	Connally
podle	podle	k7c2	podle
"	"	kIx"	"
<g/>
teorie	teorie	k1gFnSc2	teorie
jedné	jeden	k4xCgFnSc2	jeden
kulky	kulka	k1gFnSc2	kulka
<g/>
"	"	kIx"	"
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
tatáž	týž	k3xTgFnSc1	týž
kulka	kulka	k1gFnSc1	kulka
jako	jako	k9	jako
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
projela	projet	k5eAaPmAgFnS	projet
mu	on	k3xPp3gMnSc3	on
zády	záda	k1gNnPc7	záda
<g/>
,	,	kIx,	,
hrudníkem	hrudník	k1gInSc7	hrudník
<g/>
,	,	kIx,	,
pravým	pravý	k2eAgNnSc7d1	pravé
zápěstím	zápěstí	k1gNnSc7	zápěstí
a	a	k8xC	a
levým	levý	k2eAgNnSc7d1	levé
stehnem	stehno	k1gNnSc7	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
vykřikl	vykřiknout	k5eAaPmAgMnS	vykřiknout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Můj	můj	k1gMnSc5	můj
Bože	bůh	k1gMnSc5	bůh
<g/>
,	,	kIx,	,
oni	onen	k3xDgMnPc1	onen
nás	my	k3xPp1nPc4	my
všechny	všechen	k3xTgFnPc4	všechen
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Poslední	poslední	k2eAgFnSc1d1	poslední
střela	střela	k1gFnSc1	střela
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
limuzína	limuzína	k1gFnSc1	limuzína
míjela	míjet	k5eAaImAgFnS	míjet
betonový	betonový	k2eAgInSc4d1	betonový
plot	plot	k1gInSc4	plot
zahrady	zahrada	k1gFnSc2	zahrada
Johna	John	k1gMnSc2	John
Neely	Neela	k1gMnSc2	Neela
Bryana	Bryan	k1gMnSc2	Bryan
<g/>
.	.	kIx.	.
</s>
<s>
Rozbila	rozbít	k5eAaPmAgFnS	rozbít
pravou	pravý	k2eAgFnSc4d1	pravá
část	část	k1gFnSc4	část
Kennedyho	Kennedy	k1gMnSc2	Kennedy
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
vytrhla	vytrhnout	k5eAaPmAgFnS	vytrhnout
kus	kus	k1gInSc4	kus
tkáně	tkáň	k1gFnSc2	tkáň
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
lidské	lidský	k2eAgFnSc2d1	lidská
pěsti	pěst	k1gFnSc2	pěst
<g/>
.	.	kIx.	.
</s>
<s>
Kapota	kapota	k1gFnSc1	kapota
kufru	kufr	k1gInSc2	kufr
limuzíny	limuzína	k1gFnSc2	limuzína
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
policistů	policista	k1gMnPc2	policista
na	na	k7c6	na
motocyklu	motocykl	k1gInSc6	motocykl
jedoucích	jedoucí	k2eAgInPc2d1	jedoucí
jako	jako	k8xC	jako
doprovod	doprovod	k1gInSc1	doprovod
za	za	k7c7	za
autem	auto	k1gNnSc7	auto
byli	být	k5eAaImAgMnP	být
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
potřísněni	potřísněn	k2eAgMnPc1d1	potřísněn
krví	krev	k1gFnSc7	krev
a	a	k8xC	a
částmi	část	k1gFnPc7	část
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
limuzínu	limuzína	k1gFnSc4	limuzína
následovalo	následovat	k5eAaImAgNnS	následovat
doprovodné	doprovodný	k2eAgNnSc1d1	doprovodné
auto	auto	k1gNnSc1	auto
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
levé	levý	k2eAgFnSc6d1	levá
boční	boční	k2eAgFnSc6d1	boční
stojací	stojací	k2eAgFnSc6d1	stojací
plošině	plošina	k1gFnSc6	plošina
stál	stát	k5eAaImAgMnS	stát
tajný	tajný	k2eAgMnSc1d1	tajný
agent	agent	k1gMnSc1	agent
Clint	Clint	k1gMnSc1	Clint
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Okamžik	okamžik	k1gInSc1	okamžik
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
první	první	k4xOgFnSc7	první
střelou	střela	k1gFnSc7	střela
<g/>
,	,	kIx,	,
seskočil	seskočit	k5eAaPmAgMnS	seskočit
Hill	Hill	k1gMnSc1	Hill
z	z	k7c2	z
auta	auto	k1gNnSc2	auto
a	a	k8xC	a
rozběhl	rozběhnout	k5eAaPmAgMnS	rozběhnout
se	se	k3xPyFc4	se
k	k	k7c3	k
limuzíně	limuzína	k1gFnSc3	limuzína
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
řidič	řidič	k1gMnSc1	řidič
Bill	Bill	k1gMnSc1	Bill
Greer	Greer	k1gMnSc1	Greer
instinktivně	instinktivně	k6eAd1	instinktivně
zpomalil	zpomalit	k5eAaPmAgMnS	zpomalit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
byl	být	k5eAaImAgMnS	být
agent	agent	k1gMnSc1	agent
Hill	Hill	k1gMnSc1	Hill
schopen	schopen	k2eAgMnSc1d1	schopen
doběhnout	doběhnout	k5eAaPmF	doběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
fatálním	fatální	k2eAgInSc6d1	fatální
zásahu	zásah	k1gInSc6	zásah
Kennedyho	Kennedy	k1gMnSc2	Kennedy
hlavy	hlava	k1gFnSc2	hlava
vylezla	vylézt	k5eAaPmAgFnS	vylézt
paní	paní	k1gFnSc1	paní
Kennedyová	Kennedyová	k1gFnSc1	Kennedyová
v	v	k7c6	v
šoku	šok	k1gInSc6	šok
na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
kapotu	kapota	k1gFnSc4	kapota
limuzíny	limuzína	k1gFnSc2	limuzína
<g/>
,	,	kIx,	,
během	během	k7c2	během
výpovědi	výpověď	k1gFnSc2	výpověď
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgNnSc2	tento
svého	své	k1gNnSc2	své
jednání	jednání	k1gNnSc2	jednání
nebyla	být	k5eNaImAgFnS	být
zcela	zcela	k6eAd1	zcela
jista	jist	k2eAgFnSc1d1	jista
<g/>
.	.	kIx.	.
</s>
<s>
Hill	Hill	k1gMnSc1	Hill
se	se	k3xPyFc4	se
však	však	k9	však
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
sebrat	sebrat	k5eAaPmF	sebrat
kus	kus	k1gInSc4	kus
odstřelené	odstřelený	k2eAgFnSc2d1	odstřelená
části	část	k1gFnSc2	část
Kennedyho	Kennedy	k1gMnSc2	Kennedy
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
naskakoval	naskakovat	k5eAaImAgMnS	naskakovat
na	na	k7c4	na
kufr	kufr	k1gInSc4	kufr
limuzíny	limuzína	k1gFnSc2	limuzína
<g/>
,	,	kIx,	,
v	v	k7c6	v
naprostém	naprostý	k2eAgInSc6d1	naprostý
šoku	šok	k1gInSc6	šok
křičela	křičet	k5eAaImAgFnS	křičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zabili	zabít	k5eAaPmAgMnP	zabít
mi	já	k3xPp1nSc3	já
manžela	manžel	k1gMnSc4	manžel
<g/>
!	!	kIx.	!
</s>
<s>
Ustřelili	ustřelit	k5eAaPmAgMnP	ustřelit
mu	on	k3xPp3gMnSc3	on
hlavu	hlava	k1gFnSc4	hlava
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
paní	paní	k1gFnSc1	paní
Kennedyová	Kennedyová	k1gFnSc1	Kennedyová
rozevřela	rozevřít	k5eAaPmAgFnS	rozevřít
svou	svůj	k3xOyFgFnSc4	svůj
ruku	ruka	k1gFnSc4	ruka
oděnou	oděný	k2eAgFnSc4d1	oděná
do	do	k7c2	do
bílé	bílý	k2eAgFnSc2d1	bílá
rukavičky	rukavička	k1gFnSc2	rukavička
a	a	k8xC	a
vykřikla	vykřiknout	k5eAaPmAgFnS	vykřiknout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
mám	mít	k5eAaImIp1nS	mít
jeho	jeho	k3xOp3gInSc1	jeho
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hillovi	Hill	k1gMnSc3	Hill
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
konečně	konečně	k6eAd1	konečně
podařilo	podařit	k5eAaPmAgNnS	podařit
naskočit	naskočit	k5eAaPmF	naskočit
na	na	k7c4	na
kufr	kufr	k1gInSc4	kufr
limuzíny	limuzína	k1gFnSc2	limuzína
<g/>
,	,	kIx,	,
strčil	strčit	k5eAaPmAgMnS	strčit
paní	paní	k1gFnSc3	paní
Kennedyovou	Kennedyová	k1gFnSc4	Kennedyová
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
sedadlo	sedadlo	k1gNnSc4	sedadlo
a	a	k8xC	a
přitiskl	přitisknout	k5eAaPmAgMnS	přitisknout
se	se	k3xPyFc4	se
na	na	k7c4	na
kapotu	kapota	k1gFnSc4	kapota
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
ujíždět	ujíždět	k5eAaImF	ujíždět
z	z	k7c2	z
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
nemocnici	nemocnice	k1gFnSc3	nemocnice
Parkland	Parklanda	k1gFnPc2	Parklanda
Memorial	Memorial	k1gMnSc1	Memorial
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
Clinton	Clinton	k1gMnSc1	Clinton
Hill	Hill	k1gMnSc1	Hill
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
The	The	k1gMnSc2	The
Kennedy	Kenneda	k1gMnSc2	Kenneda
Detail	detail	k1gInSc1	detail
od	od	k7c2	od
Geralda	Gerald	k1gMnSc2	Gerald
Blainea	Blaineus	k1gMnSc2	Blaineus
o	o	k7c6	o
strašlivé	strašlivý	k2eAgFnSc6d1	strašlivá
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
naskočil	naskočit	k5eAaPmAgMnS	naskočit
na	na	k7c4	na
limuzínu	limuzína	k1gFnSc4	limuzína
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Šedé	Šedé	k2eAgInPc1d1	Šedé
kusy	kus	k1gInPc1	kus
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
úlomky	úlomek	k1gInPc1	úlomek
lebky	lebka	k1gFnSc2	lebka
byly	být	k5eAaImAgInP	být
po	po	k7c6	po
limuzíně	limuzína	k1gFnSc6	limuzína
rozesety	rozesít	k5eAaPmNgFnP	rozesít
jako	jako	k9	jako
nějaké	nějaký	k3yIgFnPc1	nějaký
děsivé	děsivý	k2eAgFnPc1d1	děsivá
konfety	konfeta	k1gFnPc1	konfeta
<g/>
.	.	kIx.	.
</s>
<s>
Podlahu	podlaha	k1gFnSc4	podlaha
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
kaluž	kaluž	k1gFnSc1	kaluž
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
ležel	ležet	k5eAaImAgInS	ležet
na	na	k7c6	na
sedadle	sedadlo	k1gNnSc6	sedadlo
zcela	zcela	k6eAd1	zcela
nehybný	hybný	k2eNgInSc4d1	nehybný
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
hlavě	hlava	k1gFnSc6	hlava
byla	být	k5eAaImAgFnS	být
zřetelně	zřetelně	k6eAd1	zřetelně
vidět	vidět	k5eAaImF	vidět
díra	díra	k1gFnSc1	díra
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
lidské	lidský	k2eAgFnSc2d1	lidská
pěsti	pěst	k1gFnSc2	pěst
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Guvernér	guvernér	k1gMnSc1	guvernér
Connally	Connalla	k1gMnSc2	Connalla
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jel	jet	k5eAaImAgMnS	jet
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
též	též	k9	též
vážně	vážně	k6eAd1	vážně
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
později	pozdě	k6eAd2	pozdě
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gNnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gNnSc4	on
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
strhla	strhnout	k5eAaPmAgFnS	strhnout
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
klína	klín	k1gInSc2	klín
<g/>
,	,	kIx,	,
nevědomky	nevědomky	k6eAd1	nevědomky
pomohla	pomoct	k5eAaPmAgFnS	pomoct
zatlačit	zatlačit	k5eAaPmF	zatlačit
zranění	zranění	k1gNnSc4	zranění
na	na	k7c6	na
hrudníku	hrudník	k1gInSc6	hrudník
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
mohl	moct	k5eAaImAgInS	moct
vzduch	vzduch	k1gInSc1	vzduch
proudit	proudit	k5eAaPmF	proudit
do	do	k7c2	do
funkční	funkční	k2eAgFnSc2d1	funkční
poloviny	polovina	k1gFnSc2	polovina
plic	plíce	k1gFnPc2	plíce
–	–	k?	–
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
plic	plíce	k1gFnPc2	plíce
totiž	totiž	k9	totiž
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
zraněným	zraněný	k1gMnSc7	zraněný
byl	být	k5eAaImAgMnS	být
svědek	svědek	k1gMnSc1	svědek
atentátu	atentát	k1gInSc2	atentát
James	James	k1gMnSc1	James
Tague	Tague	k1gNnPc2	Tague
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
82	[number]	k4	82
m	m	kA	m
směrem	směr	k1gInSc7	směr
vpřed	vpřed	k6eAd1	vpřed
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
lehce	lehko	k6eAd1	lehko
zraněn	zranit	k5eAaPmNgMnS	zranit
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
tváři	tvář	k1gFnSc6	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Zranění	zranění	k1gNnSc1	zranění
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
odraženou	odražený	k2eAgFnSc7d1	odražená
kulkou	kulka	k1gFnSc7	kulka
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc7	její
částí	část	k1gFnSc7	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
obrubník	obrubník	k1gInSc4	obrubník
chodníku	chodník	k1gInSc2	chodník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
níže	nízce	k6eAd2	nízce
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
Vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
Jima	Jim	k2eAgFnSc1d1	Jima
Garrisona	Garrisona	k1gFnSc1	Garrisona
Alternativní	alternativní	k2eAgFnSc4d1	alternativní
verzi	verze	k1gFnSc4	verze
celé	celý	k2eAgFnSc2d1	celá
události	událost	k1gFnSc2	událost
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
někdejší	někdejší	k2eAgMnSc1d1	někdejší
státní	státní	k2eAgMnSc1d1	státní
návladní	návladní	k2eAgMnSc1d1	návladní
z	z	k7c2	z
New	New	k1gMnPc2	New
Orleans	Orleans	k1gInSc4	Orleans
Jim	on	k3xPp3gMnPc3	on
Garrison	Garrison	k1gMnSc1	Garrison
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stal	stát	k5eAaPmAgInS	stát
značně	značně	k6eAd1	značně
skeptickým	skeptický	k2eAgNnSc7d1	skeptické
vůči	vůči	k7c3	vůči
závěrům	závěr	k1gInPc3	závěr
oficiální	oficiální	k2eAgFnSc2d1	oficiální
zprávy	zpráva	k1gFnSc2	zpráva
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Trail	Trail	k1gInSc1	Trail
of	of	k?	of
the	the	k?	the
Assassins	Assassins	k1gInSc1	Assassins
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Na	na	k7c6	na
stopě	stopa	k1gFnSc6	stopa
vrahům	vrah	k1gMnPc3	vrah
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
na	na	k7c4	na
atentát	atentát	k1gInSc4	atentát
začal	začít	k5eAaPmAgMnS	začít
silně	silně	k6eAd1	silně
měnit	měnit	k5eAaImF	měnit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vlivným	vlivný	k2eAgMnSc7d1	vlivný
senátorem	senátor	k1gMnSc7	senátor
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Louisiana	Louisian	k1gMnSc2	Louisian
<g/>
,	,	kIx,	,
Russelem	Russel	k1gMnSc7	Russel
Longem	Long	k1gMnSc7	Long
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgInS	mít
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
sdělit	sdělit	k5eAaPmF	sdělit
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ti	ten	k3xDgMnPc1	ten
chlapi	chlap	k1gMnPc1	chlap
z	z	k7c2	z
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
jsou	být	k5eAaImIp3nP	být
úplně	úplně	k6eAd1	úplně
vedle	vedle	k7c2	vedle
<g/>
.	.	kIx.	.
</s>
<s>
Nikde	nikde	k6eAd1	nikde
na	na	k7c6	na
světě	svět	k1gInSc6	svět
byste	by	kYmCp2nP	by
nenašel	najít	k5eNaPmAgInS	najít
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
takto	takto	k6eAd1	takto
schopen	schopen	k2eAgInSc1d1	schopen
docela	docela	k6eAd1	docela
sám	sám	k3xTgInSc4	sám
zastřelit	zastřelit	k5eAaPmF	zastřelit
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
alternativní	alternativní	k2eAgFnSc2d1	alternativní
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgInSc4d1	popisující
průběh	průběh	k1gInSc4	průběh
samotného	samotný	k2eAgInSc2d1	samotný
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
střel	střela	k1gFnPc2	střela
dvěma	dva	k4xCgFnPc7	dva
nebo	nebo	k8xC	nebo
i	i	k9	i
třemi	tři	k4xCgMnPc7	tři
různými	různý	k2eAgMnPc7d1	různý
střelci	střelec	k1gMnPc7	střelec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
střelce	střelec	k1gMnSc2	střelec
ukrytého	ukrytý	k2eAgMnSc2d1	ukrytý
v	v	k7c6	v
Texaském	texaský	k2eAgInSc6d1	texaský
knižním	knižní	k2eAgInSc6d1	knižní
velkoskladu	velkosklad	k1gInSc6	velkosklad
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Garrison	Garrison	k1gNnSc1	Garrison
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Oswald	Oswald	k1gInSc1	Oswald
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
vůbec	vůbec	k9	vůbec
kdy	kdy	k6eAd1	kdy
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
–	–	k?	–
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
On	on	k3xPp3gInSc1	on
the	the	k?	the
Trail	Trail	k1gInSc1	Trail
of	of	k?	of
the	the	k?	the
Assassins	Assassins	k1gInSc1	Assassins
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
hypotézu	hypotéza	k1gFnSc4	hypotéza
(	(	kIx(	(
<g/>
potažmo	potažmo	k6eAd1	potažmo
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
JFK	JFK	kA	JFK
od	od	k7c2	od
režiséra	režisér	k1gMnSc4	režisér
Olivera	Oliver	k1gMnSc2	Oliver
Stonea	Stoneus	k1gMnSc2	Stoneus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
Oswald	Oswald	k1gMnSc1	Oswald
jednoduše	jednoduše	k6eAd1	jednoduše
nacházel	nacházet	k5eAaImAgMnS	nacházet
ve	v	k7c4	v
špatnou	špatný	k2eAgFnSc4d1	špatná
dobu	doba	k1gFnSc4	doba
na	na	k7c6	na
špatném	špatný	k2eAgNnSc6d1	špatné
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
skutečně	skutečně	k6eAd1	skutečně
jen	jen	k9	jen
obětním	obětní	k2eAgInSc7d1	obětní
beránkem	beránek	k1gInSc7	beránek
<g/>
.	.	kIx.	.
</s>
<s>
Zkráceně	zkráceně	k6eAd1	zkráceně
řečeno	říct	k5eAaPmNgNnS	říct
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
Garrison	Garrison	k1gMnSc1	Garrison
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
střílela	střílet	k5eAaImAgFnS	střílet
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
zcela	zcela	k6eAd1	zcela
jiná	jiný	k2eAgFnSc1d1	jiná
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
střelcem	střelec	k1gMnSc7	střelec
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
neznámý	známý	k2eNgMnSc1d1	neznámý
muž	muž	k1gMnSc1	muž
ukrytý	ukrytý	k2eAgMnSc1d1	ukrytý
za	za	k7c7	za
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
palisádou	palisáda	k1gFnSc7	palisáda
lemující	lemující	k2eAgInSc1d1	lemující
travnatý	travnatý	k2eAgInSc1d1	travnatý
pahorek	pahorek	k1gInSc1	pahorek
a	a	k8xC	a
hypoteticky	hypoteticky	k6eAd1	hypoteticky
i	i	k9	i
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
střílející	střílející	k2eAgFnSc1d1	střílející
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
Dal-Tex	Dal-Tex	k1gInSc1	Dal-Tex
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
několika	několik	k4yIc2	několik
svědků	svědek	k1gMnPc2	svědek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
cítit	cítit	k5eAaImF	cítit
zápach	zápach	k1gInSc4	zápach
spáleného	spálený	k2eAgInSc2d1	spálený
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
podle	podle	k7c2	podle
bývalého	bývalý	k2eAgMnSc2d1	bývalý
armádního	armádní	k2eAgMnSc2d1	armádní
kapitána	kapitán	k1gMnSc2	kapitán
Ralpha	Ralph	k1gMnSc2	Ralph
Yarborougha	Yarborough	k1gMnSc2	Yarborough
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
přicházet	přicházet	k5eAaImF	přicházet
ze	z	k7c2	z
šestého	šestý	k4xOgNnSc2	šestý
patra	patro	k1gNnSc2	patro
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
.	.	kIx.	.
</s>
<s>
Přítomný	přítomný	k2eAgMnSc1d1	přítomný
fotoreportér	fotoreportér	k1gMnSc1	fotoreportér
Thomas	Thomas	k1gMnSc1	Thomas
Atkins	Atkins	k1gInSc4	Atkins
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kennedyho	Kennedy	k1gMnSc2	Kennedy
vůz	vůz	k1gInSc1	vůz
zrovna	zrovna	k6eAd1	zrovna
zabočil	zabočit	k5eAaPmAgInS	zabočit
doleva	doleva	k6eAd1	doleva
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
železničnímu	železniční	k2eAgInSc3d1	železniční
podjezdu	podjezd	k1gInSc3	podjezd
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
nedíval	dívat	k5eNaImAgMnS	dívat
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
nad	nad	k7c7	nad
námi	my	k3xPp1nPc7	my
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
jsem	být	k5eAaImIp1nS	být
všechno	všechen	k3xTgNnSc4	všechen
slyšel	slyšet	k5eAaImAgMnS	slyšet
<g/>
...	...	k?	...
<g/>
střely	střel	k1gInPc1	střel
přicházely	přicházet	k5eAaImAgInP	přicházet
zespoda	zespoda	k7c2	zespoda
a	a	k8xC	a
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
od	od	k7c2	od
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
<g/>
...	...	k?	...
nikdy	nikdy	k6eAd1	nikdy
bych	by	kYmCp1nS	by
netvrdil	tvrdit	k5eNaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přicházely	přicházet	k5eAaImAgFnP	přicházet
pouze	pouze	k6eAd1	pouze
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
nenaznačoval	naznačovat	k5eNaImAgMnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
vypáleny	vypálit	k5eAaPmNgInP	vypálit
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
výše	výše	k1gFnSc2	výše
položeného	položený	k2eAgMnSc2d1	položený
než	než	k8xS	než
ulice	ulice	k1gFnPc4	ulice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Garrison	Garrison	k1gMnSc1	Garrison
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
uvádí	uvádět	k5eAaImIp3nS	uvádět
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenned	k1gMnPc7	Kenned
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c7	pod
tzv.	tzv.	kA	tzv.
křížovou	křížový	k2eAgFnSc4d1	křížová
palbu	palba	k1gFnSc4	palba
(	(	kIx(	(
<g/>
triangulační	triangulační	k2eAgFnSc4d1	triangulační
palbu	palba	k1gFnSc4	palba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
útoku	útok	k1gInSc6	útok
tří	tři	k4xCgInPc2	tři
sniperů	sniper	k1gInPc2	sniper
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
cíl	cíl	k1gInSc4	cíl
páleno	pálit	k5eAaImNgNnS	pálit
z	z	k7c2	z
více	hodně	k6eAd2	hodně
úhlů	úhel	k1gInPc2	úhel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
zásahu	zásah	k1gInSc2	zásah
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zkomplikovalo	zkomplikovat	k5eAaPmAgNnS	zkomplikovat
nalezení	nalezení	k1gNnSc1	nalezení
jejich	jejich	k3xOp3gInSc2	jejich
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
střelby	střelba	k1gFnSc2	střelba
podle	podle	k7c2	podle
verze	verze	k1gFnSc2	verze
o	o	k7c4	o
více	hodně	k6eAd2	hodně
střelcích	střelec	k1gMnPc6	střelec
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
střela	střela	k1gFnSc1	střela
je	být	k5eAaImIp3nS	být
posádkou	posádka	k1gFnSc7	posádka
vozu	vůz	k1gInSc2	vůz
zaregistrována	zaregistrován	k2eAgFnSc1d1	zaregistrována
<g/>
,	,	kIx,	,
nikoho	nikdo	k3yNnSc4	nikdo
však	však	k9	však
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
a	a	k8xC	a
trefila	trefit	k5eAaPmAgFnS	trefit
pouze	pouze	k6eAd1	pouze
chodník	chodník	k1gInSc4	chodník
<g/>
;	;	kIx,	;
vypálena	vypálit	k5eAaPmNgFnS	vypálit
byla	být	k5eAaImAgFnS	být
buďto	buďto	k8xC	buďto
z	z	k7c2	z
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
nebo	nebo	k8xC	nebo
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
Dal-Tex	Dal-Tex	k1gInSc1	Dal-Tex
<g/>
,	,	kIx,	,
za	za	k7c7	za
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
vnějším	vnější	k2eAgNnSc7d1	vnější
požárním	požární	k2eAgNnSc7d1	požární
schodištěm	schodiště	k1gNnSc7	schodiště
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
<g />
.	.	kIx.	.
</s>
<s>
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnSc2	patro
ukryt	ukryt	k2eAgInSc4d1	ukryt
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
sniperů	sniper	k1gInPc2	sniper
2	[number]	k4	2
<g/>
.	.	kIx.	.
střela	střela	k1gFnSc1	střela
byla	být	k5eAaImAgFnS	být
vypálena	vypálit	k5eAaPmNgFnS	vypálit
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
sekundy	sekunda	k1gFnPc4	sekunda
později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
palisády	palisáda	k1gFnSc2	palisáda
a	a	k8xC	a
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
do	do	k7c2	do
krku	krk	k1gInSc2	krk
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
okénko	okénko	k1gNnSc4	okénko
188	[number]	k4	188
<g/>
,	,	kIx,	,
Zapruderův	Zapruderův	k2eAgInSc1d1	Zapruderův
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Kennedy	Kenneda	k1gMnSc2	Kenneda
se	se	k3xPyFc4	se
chytá	chytat	k5eAaImIp3nS	chytat
oběma	dva	k4xCgMnPc7	dva
rukama	ruka	k1gFnPc7	ruka
za	za	k7c4	za
prostřelený	prostřelený	k2eAgInSc4d1	prostřelený
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
střela	střela	k1gFnSc1	střela
silného	silný	k2eAgInSc2d1	silný
kalibru	kalibr	k1gInSc2	kalibr
<g/>
,	,	kIx,	,
vypálená	vypálený	k2eAgFnSc1d1	vypálená
ze	z	k7c2	z
šestého	šestý	k4xOgNnSc2	šestý
patra	patro	k1gNnSc2	patro
skladu	sklad	k1gInSc2	sklad
knih	kniha	k1gFnPc2	kniha
těsně	těsně	k6eAd1	těsně
míjí	míjet	k5eAaImIp3nS	míjet
prezidenta	prezident	k1gMnSc4	prezident
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
guvernéra	guvernér	k1gMnSc4	guvernér
Connallyho	Connally	k1gMnSc4	Connally
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
proniká	pronikat	k5eAaImIp3nS	pronikat
hrudníkem	hrudník	k1gInSc7	hrudník
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
ven	ven	k6eAd1	ven
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
jeho	jeho	k3xOp3gFnSc2	jeho
pravé	pravý	k2eAgFnSc2d1	pravá
prsní	prsní	k2eAgFnSc2d1	prsní
bradavky	bradavka	k1gFnSc2	bradavka
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
střela	střela	k1gFnSc1	střela
<g/>
,	,	kIx,	,
kolem	kolem	k6eAd1	kolem
které	který	k3yIgNnSc1	který
panuje	panovat	k5eAaImIp3nS	panovat
nejvíce	nejvíce	k6eAd1	nejvíce
dohadů	dohad	k1gInPc2	dohad
a	a	k8xC	a
nejasností	nejasnost	k1gFnPc2	nejasnost
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
opět	opět	k6eAd1	opět
vypálena	vypálit	k5eAaPmNgNnP	vypálit
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
Dal-Tex	Dal-Tex	k1gInSc4	Dal-Tex
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
naproti	naproti	k7c3	naproti
skladu	sklad	k1gInSc3	sklad
učebnic	učebnice	k1gFnPc2	učebnice
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
zranění	zranění	k1gNnSc4	zranění
na	na	k7c6	na
prezidentových	prezidentův	k2eAgNnPc6d1	prezidentovo
zádech	záda	k1gNnPc6	záda
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
střela	střela	k1gFnSc1	střela
byla	být	k5eAaImAgFnS	být
vypálena	vypálit	k5eAaPmNgFnS	vypálit
od	od	k7c2	od
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
<g/>
,	,	kIx,	,
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
do	do	k7c2	do
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
fatální	fatální	k2eAgFnSc4d1	fatální
frakturu	fraktura	k1gFnSc4	fraktura
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vytrhla	vytrhnout	k5eAaPmAgFnS	vytrhnout
velký	velký	k2eAgInSc4d1	velký
kus	kus	k1gInSc4	kus
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
její	její	k3xOp3gInSc4	její
náraz	náraz	k1gInSc4	náraz
prudce	prudko	k6eAd1	prudko
pohnul	pohnout	k5eAaPmAgMnS	pohnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
tělem	tělo	k1gNnSc7	tělo
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
poté	poté	k6eAd1	poté
doleva	doleva	k6eAd1	doleva
<g/>
;	;	kIx,	;
mnoho	mnoho	k4c1	mnoho
svědků	svědek	k1gMnPc2	svědek
přítomných	přítomný	k2eAgMnPc2d1	přítomný
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
činu	čin	k1gInSc2	čin
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
střela	střela	k1gFnSc1	střela
byla	být	k5eAaImAgFnS	být
vypálena	vypálit	k5eAaPmNgFnS	vypálit
zpoza	zpoza	k7c2	zpoza
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
palisády	palisáda	k1gFnSc2	palisáda
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
střela	střela	k1gFnSc1	střela
byla	být	k5eAaImAgFnS	být
vypálena	vypálit	k5eAaPmNgFnS	vypálit
opět	opět	k6eAd1	opět
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
,	,	kIx,	,
roztříštila	roztříštit	k5eAaPmAgFnS	roztříštit
zápěstí	zápěstí	k1gNnSc4	zápěstí
guvernéra	guvernér	k1gMnSc2	guvernér
Connallyho	Connally	k1gMnSc2	Connally
<g/>
,	,	kIx,	,
pronikla	proniknout	k5eAaPmAgFnS	proniknout
jeho	jeho	k3xOp3gNnSc7	jeho
levým	levý	k2eAgNnSc7d1	levé
stehnem	stehno	k1gNnSc7	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
střelbě	střelba	k1gFnSc6	střelba
zpoza	zpoza	k7c2	zpoza
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
svědectví	svědectví	k1gNnSc4	svědectví
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
Julie	Julie	k1gFnSc2	Julie
Ann	Ann	k1gFnSc2	Ann
Mercerové	Mercerová	k1gFnSc2	Mercerová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chvíli	chvíle	k1gFnSc4	chvíle
před	před	k7c7	před
spácháním	spáchání	k1gNnSc7	spáchání
atentátu	atentát	k1gInSc2	atentát
zastavila	zastavit	k5eAaPmAgFnS	zastavit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vozem	vůz	k1gInSc7	vůz
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
vidět	vidět	k5eAaImF	vidět
muže	muž	k1gMnSc4	muž
vystupujícího	vystupující	k2eAgMnSc4d1	vystupující
z	z	k7c2	z
dodávky	dodávka	k1gFnSc2	dodávka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nesl	nést	k5eAaImAgInS	nést
pušku	puška	k1gFnSc4	puška
zabalenou	zabalený	k2eAgFnSc4d1	zabalená
ve	v	k7c6	v
speciálním	speciální	k2eAgNnSc6d1	speciální
pouzdře	pouzdro	k1gNnSc6	pouzdro
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
dřevěné	dřevěný	k2eAgFnSc3d1	dřevěná
palisádě	palisáda	k1gFnSc3	palisáda
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
výpověď	výpověď	k1gFnSc4	výpověď
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
svědectví	svědectví	k1gNnSc4	svědectví
Jean	Jean	k1gMnSc1	Jean
Hillové	Hillová	k1gFnSc2	Hillová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podobě	podoba	k1gFnSc6	podoba
jako	jako	k9	jako
později	pozdě	k6eAd2	pozdě
zmiňovaný	zmiňovaný	k2eAgMnSc1d1	zmiňovaný
J.	J.	kA	J.
C.	C.	kA	C.
Price	Price	k1gMnSc1	Price
<g/>
,	,	kIx,	,
viděla	vidět	k5eAaImAgFnS	vidět
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
střelbě	střelba	k1gFnSc6	střelba
rychle	rychle	k6eAd1	rychle
běžícího	běžící	k2eAgMnSc4d1	běžící
muže	muž	k1gMnSc4	muž
oděného	oděný	k2eAgMnSc4d1	oděný
v	v	k7c6	v
hnědém	hnědý	k2eAgInSc6d1	hnědý
kabátu	kabát	k1gInSc6	kabát
a	a	k8xC	a
klobouku	klobouk	k1gInSc2	klobouk
<g/>
,	,	kIx,	,
směřujícího	směřující	k2eAgInSc2d1	směřující
ke	k	k7c3	k
kolejím	kolej	k1gFnPc3	kolej
u	u	k7c2	u
železničního	železniční	k2eAgInSc2d1	železniční
viaduktu	viadukt	k1gInSc2	viadukt
<g/>
.	.	kIx.	.
</s>
<s>
Hillová	Hillová	k1gFnSc1	Hillová
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Mary	Mary	k1gFnSc7	Mary
Moormanovou	Moormanová	k1gFnSc7	Moormanová
na	na	k7c4	na
Dealey	Dealea	k1gFnPc4	Dealea
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
jen	jen	k9	jen
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
smrtelně	smrtelně	k6eAd1	smrtelně
raněn	raněn	k2eAgInSc1d1	raněn
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
svědkyně	svědkyně	k1gFnPc1	svědkyně
lze	lze	k6eAd1	lze
společně	společně	k6eAd1	společně
vidět	vidět	k5eAaImF	vidět
například	například	k6eAd1	například
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
č.	č.	k?	č.
301	[number]	k4	301
ze	z	k7c2	z
Zapruderova	Zapruderův	k2eAgInSc2d1	Zapruderův
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
muže	muž	k1gMnSc2	muž
Hillová	Hillová	k1gFnSc1	Hillová
identifikovala	identifikovat	k5eAaBmAgFnS	identifikovat
jako	jako	k9	jako
Jacka	Jacek	k1gMnSc4	Jacek
Rubyho	Ruby	k1gMnSc4	Ruby
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
na	na	k7c4	na
Dealey	Dealey	k1gInPc4	Dealey
Plaza	plaz	k1gMnSc2	plaz
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
nelze	lze	k6eNd1	lze
zcela	zcela	k6eAd1	zcela
bezpečně	bezpečně	k6eAd1	bezpečně
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
fatálního	fatální	k2eAgInSc2d1	fatální
zásahu	zásah	k1gInSc2	zásah
projížděla	projíždět	k5eAaImAgFnS	projíždět
limuzína	limuzína	k1gFnSc1	limuzína
kolem	kolem	k7c2	kolem
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
grassy	grass	k1gInPc4	grass
knoll	knoll	k1gInSc1	knoll
<g/>
)	)	kIx)	)
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Elm	Elm	k1gMnSc1	Elm
street	street	k1gMnSc1	street
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
kolona	kolona	k1gFnSc1	kolona
opustila	opustit	k5eAaPmAgFnS	opustit
Dealey	Dealea	k1gFnPc4	Dealea
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
rozběhli	rozběhnout	k5eAaPmAgMnP	rozběhnout
se	se	k3xPyFc4	se
policisté	policista	k1gMnPc1	policista
a	a	k8xC	a
svědci	svědek	k1gMnPc1	svědek
z	z	k7c2	z
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
a	a	k8xC	a
nedalekého	daleký	k2eNgInSc2d1	nedaleký
železničního	železniční	k2eAgInSc2d1	železniční
viaduktu	viadukt	k1gInSc2	viadukt
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
1,5	[number]	k4	1,5
m	m	kA	m
vysokému	vysoký	k2eAgInSc3d1	vysoký
plotu	plot	k1gInSc3	plot
oddělujícímu	oddělující	k2eAgInSc3d1	oddělující
pahorek	pahorek	k1gInSc1	pahorek
od	od	k7c2	od
přilehlého	přilehlý	k2eAgNnSc2d1	přilehlé
parkoviště	parkoviště	k1gNnSc2	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgMnSc1d1	soukromý
výzkumník	výzkumník	k1gMnSc1	výzkumník
John	John	k1gMnSc1	John
Hankey	Hankey	k1gInPc4	Hankey
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
filmu	film	k1gInSc6	film
JFKII	JFKII	kA	JFKII
-	-	kIx~	-
The	The	k1gFnSc1	The
Bush	Bush	k1gMnSc1	Bush
Connection	Connection	k1gInSc1	Connection
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
spontánní	spontánní	k2eAgFnSc1d1	spontánní
reakce	reakce	k1gFnSc1	reakce
svědků	svědek	k1gMnPc2	svědek
nepřímo	přímo	k6eNd1	přímo
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
míst	místo	k1gNnPc2	místo
musel	muset	k5eAaImAgInS	muset
skutečně	skutečně	k6eAd1	skutečně
zaznít	zaznít	k5eAaPmF	zaznít
minimálně	minimálně	k6eAd1	minimálně
jeden	jeden	k4xCgInSc4	jeden
výstřel	výstřel	k1gInSc4	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
chování	chování	k1gNnSc1	chování
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
podle	podle	k7c2	podle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
nelze	lze	k6eNd1	lze
jinak	jinak	k6eAd1	jinak
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
sniper	sniper	k1gInSc1	sniper
však	však	k9	však
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
přímo	přímo	k6eAd1	přímo
zpozorován	zpozorován	k2eAgMnSc1d1	zpozorován
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Svědek	svědek	k1gMnSc1	svědek
S.	S.	kA	S.
M.	M.	kA	M.
Holland	Holland	k1gInSc1	Holland
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sledoval	sledovat	k5eAaImAgInS	sledovat
průjezd	průjezd	k1gInSc4	průjezd
kolony	kolona	k1gFnSc2	kolona
z	z	k7c2	z
železničního	železniční	k2eAgInSc2d1	železniční
viaduktu	viadukt	k1gInSc2	viadukt
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
zaznění	zaznění	k1gNnSc6	zaznění
tří	tři	k4xCgFnPc2	tři
ran	rána	k1gFnPc2	rána
rozběhl	rozběhnout	k5eAaPmAgInS	rozběhnout
kolem	kolem	k7c2	kolem
rohu	roh	k1gInSc2	roh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
viadukt	viadukt	k1gInSc1	viadukt
napojoval	napojovat	k5eAaImAgInS	napojovat
na	na	k7c4	na
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
plot	plot	k1gInSc4	plot
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neviděl	vidět	k5eNaImAgMnS	vidět
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
z	z	k7c2	z
daného	daný	k2eAgNnSc2d1	dané
místa	místo	k1gNnSc2	místo
prchal	prchat	k5eAaImAgMnS	prchat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zvuku	zvuk	k1gInSc2	zvuk
střelby	střelba	k1gFnSc2	střelba
ale	ale	k8xC	ale
soudil	soudit	k5eAaImAgMnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rány	rána	k1gFnPc1	rána
přicházely	přicházet	k5eAaImAgFnP	přicházet
ze	z	k7c2	z
směru	směr	k1gInSc2	směr
od	od	k7c2	od
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
navíc	navíc	k6eAd1	navíc
spatřil	spatřit	k5eAaPmAgMnS	spatřit
dým	dým	k1gInSc4	dým
vycházející	vycházející	k2eAgInSc4d1	vycházející
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
střelby	střelba	k1gFnSc2	střelba
z	z	k7c2	z
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
svědecká	svědecký	k2eAgFnSc1d1	svědecká
výpověď	výpověď	k1gFnSc1	výpověď
J.	J.	kA	J.
C.	C.	kA	C.
Price	Price	k1gMnSc1	Price
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stál	stát	k5eAaImAgMnS	stát
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
střelby	střelba	k1gFnSc2	střelba
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
budovy	budova	k1gFnSc2	budova
Poštovního	poštovní	k2eAgInSc2d1	poštovní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Price	Priko	k6eAd1	Priko
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
železničního	železniční	k2eAgInSc2d1	železniční
viaduktu	viadukt	k1gInSc2	viadukt
jsem	být	k5eAaImIp1nS	být
zaslech	zasl	k1gInPc6	zasl
několik	několik	k4yIc1	několik
výstřelů	výstřel	k1gInPc2	výstřel
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gNnPc2	on
asi	asi	k9	asi
pět	pět	k4xCc1	pět
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jsem	být	k5eAaImIp1nS	být
tam	tam	k6eAd1	tam
spatřil	spatřit	k5eAaPmAgMnS	spatřit
rychle	rychle	k6eAd1	rychle
běžícího	běžící	k2eAgMnSc4d1	běžící
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
směřoval	směřovat	k5eAaImAgMnS	směřovat
k	k	k7c3	k
vozům	vůz	k1gInPc3	vůz
zaparkovaným	zaparkovaný	k2eAgInPc3d1	zaparkovaný
nedaleko	nedaleko	k7c2	nedaleko
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
bílou	bílý	k2eAgFnSc4d1	bílá
košili	košile	k1gFnSc4	košile
bez	bez	k7c2	bez
kravaty	kravata	k1gFnSc2	kravata
a	a	k8xC	a
kalhoty	kalhoty	k1gFnPc4	kalhoty
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
khaki	khaki	k1gNnSc2	khaki
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
delší	dlouhý	k2eAgInPc4d2	delší
tmavé	tmavý	k2eAgInPc4d1	tmavý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rychle	rychle	k6eAd1	rychle
běžel	běžet	k5eAaImAgMnS	běžet
<g/>
,	,	kIx,	,
bych	by	kYmCp1nS	by
jeho	jeho	k3xOp3gInSc4	jeho
věk	věk	k1gInSc4	věk
odhadoval	odhadovat	k5eAaImAgMnS	odhadovat
tak	tak	k9	tak
na	na	k7c4	na
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
připomínalo	připomínat	k5eAaImAgNnS	připomínat
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
vysílačku	vysílačka	k1gFnSc4	vysílačka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jistý	jistý	k2eAgMnSc1d1	jistý
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
nejsem	být	k5eNaImIp1nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lee	Lea	k1gFnSc3	Lea
Bowers	Bowersa	k1gFnPc2	Bowersa
<g/>
,	,	kIx,	,
posunovač	posunovač	k1gInSc1	posunovač
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
seděl	sedět	k5eAaImAgMnS	sedět
ve	v	k7c6	v
dvoupatrové	dvoupatrový	k2eAgFnSc6d1	dvoupatrová
kontrolní	kontrolní	k2eAgFnSc6d1	kontrolní
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
dobrý	dobrý	k2eAgInSc1d1	dobrý
výhled	výhled	k1gInSc1	výhled
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
za	za	k7c7	za
plotem	plot	k1gInSc7	plot
obklopujícím	obklopující	k2eAgInSc7d1	obklopující
travnatý	travnatý	k2eAgInSc1d1	travnatý
pahorek	pahorek	k1gInSc1	pahorek
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgMnPc4	čtyři
muže	muž	k1gMnPc4	muž
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
mezi	mezi	k7c7	mezi
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
Elm	Elm	k1gFnSc7	Elm
street	street	k5eAaPmF	street
<g/>
:	:	kIx,	:
muže	muž	k1gMnSc4	muž
středního	střední	k2eAgInSc2d1	střední
věku	věk	k1gInSc2	věk
s	s	k7c7	s
mladším	mladý	k2eAgMnSc7d2	mladší
mužem	muž	k1gMnSc7	muž
stojící	stojící	k2eAgFnSc2d1	stojící
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
m	m	kA	m
od	od	k7c2	od
viaduktu	viadukt	k1gInSc2	viadukt
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
jevili	jevit	k5eAaImAgMnP	jevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
neznají	neznat	k5eAaImIp3nP	neznat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
či	či	k8xC	či
dva	dva	k4xCgMnPc4	dva
muže	muž	k1gMnPc4	muž
z	z	k7c2	z
obsluhy	obsluha	k1gFnSc2	obsluha
parkoviště	parkoviště	k1gNnSc2	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
střelby	střelba	k1gFnSc2	střelba
prý	prý	k9	prý
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
"	"	kIx"	"
<g/>
něco	něco	k3yInSc1	něco
neobvyklého	obvyklý	k2eNgNnSc2d1	neobvyklé
<g/>
,	,	kIx,	,
záblesk	záblesk	k1gInSc4	záblesk
a	a	k8xC	a
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
dost	dost	k6eAd1	dost
podivně	podivně	k6eAd1	podivně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
to	ten	k3xDgNnSc4	ten
popsat	popsat	k5eAaPmF	popsat
přesněji	přesně	k6eAd2	přesně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
však	však	k9	však
žádného	žádný	k3yNgMnSc4	žádný
střelce	střelec	k1gMnSc4	střelec
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Bowers	Bowers	k6eAd1	Bowers
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
z	z	k7c2	z
obsluhy	obsluha	k1gFnSc2	obsluha
parkoviště	parkoviště	k1gNnSc2	parkoviště
stáli	stát	k5eAaImAgMnP	stát
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
místech	místo	k1gNnPc6	místo
i	i	k8xC	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
policista	policista	k1gMnSc1	policista
Clyde	Clyd	k1gInSc5	Clyd
Haygood	Haygooda	k1gFnPc2	Haygooda
přelezl	přelézt	k5eAaPmAgMnS	přelézt
plot	plot	k1gInSc4	plot
u	u	k7c2	u
pahorku	pahorek	k1gInSc2	pahorek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interview	interview	k1gNnSc6	interview
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Bowers	Bowers	k1gInSc1	Bowers
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgMnPc1	dva
zmínění	zmíněný	k2eAgMnPc1d1	zmíněný
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
plotu	plot	k1gInSc2	plot
(	(	kIx(	(
<g/>
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
pohledu	pohled	k1gInSc2	pohled
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nestál	stát	k5eNaImAgMnS	stát
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
plotu	plot	k1gInSc2	plot
během	během	k7c2	během
střelby	střelba	k1gFnSc2	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Howard	Howard	k1gMnSc1	Howard
Brennan	Brennan	k1gMnSc1	Brennan
<g/>
,	,	kIx,	,
instalatér	instalatér	k1gMnSc1	instalatér
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
seděl	sedět	k5eAaImAgMnS	sedět
naproti	naproti	k6eAd1	naproti
budovy	budova	k1gFnSc2	budova
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
,	,	kIx,	,
sdělil	sdělit	k5eAaPmAgMnS	sdělit
policii	policie	k1gFnSc4	policie
<g/>
,	,	kIx,	,
že	že	k8xS	že
sledoval	sledovat	k5eAaImAgMnS	sledovat
projíždějící	projíždějící	k2eAgFnSc4d1	projíždějící
kolonu	kolona	k1gFnSc4	kolona
a	a	k8xC	a
najednou	najednou	k6eAd1	najednou
uslyšel	uslyšet	k5eAaPmAgInS	uslyšet
ránu	rána	k1gFnSc4	rána
přicházející	přicházející	k2eAgFnSc4d1	přicházející
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
<s>
Podíval	podívat	k5eAaImAgInS	podívat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
rána	rána	k1gFnSc1	rána
vyšla	vyjít	k5eAaPmAgFnS	vyjít
a	a	k8xC	a
uviděl	uvidět	k5eAaPmAgMnS	uvidět
muže	muž	k1gMnSc4	muž
s	s	k7c7	s
puškou	puška	k1gFnSc7	puška
vystřelujícího	vystřelující	k2eAgMnSc2d1	vystřelující
další	další	k2eAgInSc4d1	další
náboj	náboj	k1gInSc4	náboj
z	z	k7c2	z
rohového	rohový	k2eAgNnSc2d1	rohové
okna	okno	k1gNnSc2	okno
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgMnSc4	týž
muže	muž	k1gMnSc4	muž
prý	prý	k9	prý
viděl	vidět	k5eAaImAgInS	vidět
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
ze	z	k7c2	z
stejného	stejný	k2eAgNnSc2d1	stejné
okna	okno	k1gNnSc2	okno
rozhlíží	rozhlížet	k5eAaImIp3nP	rozhlížet
<g/>
.	.	kIx.	.
</s>
<s>
Brennan	Brennan	k1gMnSc1	Brennan
předal	předat	k5eAaPmAgMnS	předat
policii	policie	k1gFnSc3	policie
popis	popis	k1gInSc4	popis
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c4	v
12.45	[number]	k4	12.45
<g/>
,	,	kIx,	,
12.48	[number]	k4	12.48
a	a	k8xC	a
12.55	[number]	k4	12.55
rádiově	rádiově	k6eAd1	rádiově
odvysílán	odvysílán	k2eAgInSc4d1	odvysílán
všem	všecek	k3xTgFnPc3	všecek
dallaským	dallaský	k2eAgFnPc3d1	dallaská
policejním	policejní	k2eAgFnPc3d1	policejní
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Brennan	Brennan	k1gMnSc1	Brennan
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
mluvil	mluvit	k5eAaImAgMnS	mluvit
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vyšetřujících	vyšetřující	k2eAgMnPc2d1	vyšetřující
policistů	policista	k1gMnPc2	policista
<g/>
,	,	kIx,	,
připojili	připojit	k5eAaPmAgMnP	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
Harold	Harold	k1gInSc1	Harold
Norman	Norman	k1gMnSc1	Norman
a	a	k8xC	a
James	James	k1gMnSc1	James
Jarman	Jarman	k1gMnSc1	Jarman
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sledovali	sledovat	k5eAaImAgMnP	sledovat
průjezd	průjezd	k1gInSc4	průjezd
kolony	kolona	k1gFnSc2	kolona
z	z	k7c2	z
jihovýchodního	jihovýchodní	k2eAgNnSc2d1	jihovýchodní
okna	okno	k1gNnSc2	okno
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
podlaží	podlaží	k1gNnSc2	podlaží
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
společně	společně	k6eAd1	společně
s	s	k7c7	s
kolegou	kolega	k1gMnSc7	kolega
Bonnie	Bonnie	k1gFnSc2	Bonnie
Rayem	Ray	k1gMnSc7	Ray
Williamsem	Williams	k1gMnSc7	Williams
uslyšeli	uslyšet	k5eAaPmAgMnP	uslyšet
tři	tři	k4xCgInPc4	tři
výstřely	výstřel	k1gInPc4	výstřel
přicházející	přicházející	k2eAgNnSc1d1	přicházející
ze	z	k7c2	z
směru	směr	k1gInSc2	směr
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
jejich	jejich	k3xOp3gFnPc7	jejich
hlavami	hlava	k1gFnPc7	hlava
a	a	k8xC	a
ze	z	k7c2	z
stropu	strop	k1gInSc2	strop
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
se	se	k3xPyFc4	se
odloupla	odloupnout	k5eAaPmAgFnS	odloupnout
část	část	k1gFnSc1	část
omítky	omítka	k1gFnSc2	omítka
<g/>
.	.	kIx.	.
</s>
<s>
Norman	Norman	k1gMnSc1	Norman
též	též	k9	též
slyšel	slyšet	k5eAaImAgMnS	slyšet
zvuky	zvuk	k1gInPc4	zvuk
nabíjení	nabíjení	k1gNnSc2	nabíjení
pušky	puška	k1gFnSc2	puška
a	a	k8xC	a
pád	pád	k1gInSc1	pád
vystřelených	vystřelený	k2eAgFnPc2d1	vystřelená
patron	patrona	k1gFnPc2	patrona
na	na	k7c4	na
podlahu	podlaha	k1gFnSc4	podlaha
nad	nad	k7c7	nad
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dallaská	dallaský	k2eAgFnSc1d1	dallaská
policie	policie	k1gFnSc1	policie
zablokovala	zablokovat	k5eAaPmAgFnS	zablokovat
vchody	vchod	k1gInPc4	vchod
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
12.33	[number]	k4	12.33
do	do	k7c2	do
12.50	[number]	k4	12.50
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
zmíněnými	zmíněný	k2eAgInPc7d1	zmíněný
časy	čas	k1gInPc7	čas
byl	být	k5eAaImAgInS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
otevřen	otevřít	k5eAaPmNgInS	otevřít
zadní	zadní	k2eAgInSc1d1	zadní
únikový	únikový	k2eAgInSc1d1	únikový
východ	východ	k1gInSc1	východ
<g/>
,	,	kIx,	,
a	a	k8xC	a
budovu	budova	k1gFnSc4	budova
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgMnS	moct
bez	bez	k7c2	bez
kontroly	kontrola	k1gFnSc2	kontrola
opustit	opustit	k5eAaPmF	opustit
naprosto	naprosto	k6eAd1	naprosto
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
104	[number]	k4	104
svědků	svědek	k1gMnPc2	svědek
stojících	stojící	k2eAgFnPc2d1	stojící
na	na	k7c4	na
Dealey	Dealey	k1gInPc4	Dealey
Plaza	plaz	k1gMnSc4	plaz
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
slyšeli	slyšet	k5eAaImAgMnP	slyšet
výstřely	výstřel	k1gInPc4	výstřel
<g/>
,	,	kIx,	,
56	[number]	k4	56
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
výstřely	výstřel	k1gInPc1	výstřel
přicházely	přicházet	k5eAaImAgFnP	přicházet
od	od	k7c2	od
budovy	budova	k1gFnSc2	budova
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
,	,	kIx,	,
35	[number]	k4	35
se	se	k3xPyFc4	se
domnívalo	domnívat	k5eAaImAgNnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvuk	zvuk	k1gInSc1	zvuk
přicházel	přicházet	k5eAaImAgInS	přicházet
od	od	k7c2	od
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
nebo	nebo	k8xC	nebo
železničního	železniční	k2eAgInSc2d1	železniční
viaduktu	viadukt	k1gInSc2	viadukt
<g/>
,	,	kIx,	,
8	[number]	k4	8
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvuk	zvuk	k1gInSc1	zvuk
vycházel	vycházet	k5eAaImAgInS	vycházet
buďto	buďto	k8xC	buďto
od	od	k7c2	od
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
nebo	nebo	k8xC	nebo
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
a	a	k8xC	a
5	[number]	k4	5
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
směr	směr	k1gInSc4	směr
přesně	přesně	k6eAd1	přesně
specifikovat	specifikovat	k5eAaBmF	specifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lee	Lea	k1gFnSc6	Lea
Harvey	Harvea	k1gFnPc1	Harvea
Oswald	Oswalda	k1gFnPc2	Oswalda
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
Harvey	Harvea	k1gFnSc2	Harvea
Oswald	Oswalda	k1gFnPc2	Oswalda
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
podezřelý	podezřelý	k2eAgMnSc1d1	podezřelý
ze	z	k7c2	z
spáchání	spáchání	k1gNnSc2	spáchání
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Texaském	texaský	k2eAgInSc6d1	texaský
knižním	knižní	k2eAgInSc6d1	knižní
velkoskladu	velkosklad	k1gInSc6	velkosklad
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1963	[number]	k4	1963
jakožto	jakožto	k8xS	jakožto
posilový	posilový	k2eAgMnSc1d1	posilový
brigádník	brigádník	k1gMnSc1	brigádník
na	na	k7c4	na
předvánoční	předvánoční	k2eAgNnSc4d1	předvánoční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
devadesát	devadesát	k4xCc1	devadesát
sekund	sekunda	k1gFnPc2	sekunda
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
byl	být	k5eAaImAgInS	být
viděn	vidět	k5eAaImNgInS	vidět
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
jídelně	jídelna	k1gFnSc6	jídelna
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
popíjí	popíjet	k5eAaImIp3nS	popíjet
Coca-Colu	cocaola	k1gFnSc4	coca-cola
a	a	k8xC	a
pojídá	pojídat	k5eAaImIp3nS	pojídat
sýrový	sýrový	k2eAgInSc4d1	sýrový
sendvič	sendvič	k1gInSc4	sendvič
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k1gMnSc1	Oswald
tedy	tedy	k9	tedy
musel	muset	k5eAaImAgMnS	muset
dle	dle	k7c2	dle
závěrů	závěr	k1gInPc2	závěr
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
minuty	minuta	k1gFnSc2	minuta
a	a	k8xC	a
půl	půl	k6eAd1	půl
stihnout	stihnout	k5eAaPmF	stihnout
vypálit	vypálit	k5eAaPmF	vypálit
tři	tři	k4xCgFnPc4	tři
střely	střela	k1gFnPc4	střela
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
ukrýt	ukrýt	k5eAaPmF	ukrýt
pušku	puška	k1gFnSc4	puška
mezi	mezi	k7c7	mezi
krabicemi	krabice	k1gFnPc7	krabice
<g/>
,	,	kIx,	,
seběhnout	seběhnout	k5eAaPmF	seběhnout
čtyři	čtyři	k4xCgNnPc1	čtyři
patra	patro	k1gNnPc1	patro
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
koupit	koupit	k5eAaPmF	koupit
si	se	k3xPyFc3	se
Coca-Colu	cocaola	k1gFnSc4	coca-cola
v	v	k7c6	v
automatu	automat	k1gInSc6	automat
a	a	k8xC	a
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
v	v	k7c6	v
jídelně	jídelna	k1gFnSc6	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
policistou	policista	k1gMnSc7	policista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Marion	Marion	k1gInSc4	Marion
Baker	Baker	k1gMnSc1	Baker
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
vešel	vejít	k5eAaPmAgInS	vejít
ve	v	k7c6	v
12.32	[number]	k4	12.32
a	a	k8xC	a
ve	v	k7c6	v
vstupní	vstupní	k2eAgFnSc6d1	vstupní
hale	hala	k1gFnSc6	hala
potkal	potkat	k5eAaPmAgInS	potkat
ředitele	ředitel	k1gMnSc4	ředitel
Roye	Roy	k1gMnSc4	Roy
Trulyho	Truly	k1gMnSc4	Truly
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
zeptal	zeptat	k5eAaPmAgMnS	zeptat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
schody	schod	k1gInPc4	schod
nebo	nebo	k8xC	nebo
výtah	výtah	k1gInSc4	výtah
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Truly	trul	k1gInPc4	trul
mu	on	k3xPp3gMnSc3	on
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
tu	tu	k6eAd1	tu
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Pojďte	jít	k5eAaImRp2nP	jít
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
strážníku	strážník	k1gMnSc5	strážník
<g/>
,	,	kIx,	,
ukážu	ukázat	k5eAaPmIp1nS	ukázat
vám	vy	k3xPp2nPc3	vy
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Oba	dva	k4xCgMnPc1	dva
poté	poté	k6eAd1	poté
společně	společně	k6eAd1	společně
vyběhli	vyběhnout	k5eAaPmAgMnP	vyběhnout
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnSc2	patro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spatřili	spatřit	k5eAaPmAgMnP	spatřit
muže	muž	k1gMnSc4	muž
(	(	kIx(	(
<g/>
Oswalda	Oswalda	k1gMnSc1	Oswalda
<g/>
)	)	kIx)	)
vycházejícího	vycházející	k2eAgMnSc2d1	vycházející
z	z	k7c2	z
kantýny	kantýna	k1gFnSc2	kantýna
<g/>
.	.	kIx.	.
</s>
<s>
Baker	Baker	k1gMnSc1	Baker
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
ihned	ihned	k6eAd1	ihned
zavolal	zavolat	k5eAaPmAgMnS	zavolat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pojďte	jít	k5eAaImRp2nP	jít
sem	sem	k6eAd1	sem
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Oswald	Oswaldo	k1gNnPc2	Oswaldo
se	se	k3xPyFc4	se
otočil	otočit	k5eAaPmAgInS	otočit
a	a	k8xC	a
šel	jít	k5eAaImAgInS	jít
rovnou	rovnou	k6eAd1	rovnou
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Baker	Baker	k1gMnSc1	Baker
se	se	k3xPyFc4	se
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
zeptal	zeptat	k5eAaPmAgMnS	zeptat
ředitele	ředitel	k1gMnSc4	ředitel
Trulyho	Truly	k1gMnSc4	Truly
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Znáte	znát	k5eAaImIp2nP	znát
toho	ten	k3xDgMnSc4	ten
muže	muž	k1gMnSc4	muž
<g/>
?	?	kIx.	?
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
tady	tady	k6eAd1	tady
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Truly	trula	k1gFnSc2	trula
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Oswald	Oswald	k1gMnSc1	Oswald
skutečně	skutečně	k6eAd1	skutečně
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
velkoskladu	velkosklad	k1gInSc2	velkosklad
a	a	k8xC	a
Baker	Baker	k1gMnSc1	Baker
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
již	již	k9	již
dále	daleko	k6eAd2	daleko
nezajímal	zajímat	k5eNaImAgMnS	zajímat
a	a	k8xC	a
běžel	běžet	k5eAaImAgMnS	běžet
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
až	až	k9	až
do	do	k7c2	do
šestého	šestý	k4xOgNnSc2	šestý
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Policista	policista	k1gMnSc1	policista
Baker	Baker	k1gMnSc1	Baker
Warrenově	Warrenův	k2eAgFnSc3d1	Warrenova
komisi	komise	k1gFnSc3	komise
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Oswald	Oswald	k1gInSc1	Oswald
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
zcela	zcela	k6eAd1	zcela
klidný	klidný	k2eAgMnSc1d1	klidný
a	a	k8xC	a
nejevil	jevit	k5eNaImAgMnS	jevit
žádné	žádný	k3yNgFnPc4	žádný
známky	známka	k1gFnPc4	známka
nervozity	nervozita	k1gFnSc2	nervozita
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
namířil	namířit	k5eAaPmAgInS	namířit
svou	svůj	k3xOyFgFnSc4	svůj
služební	služební	k2eAgFnSc4d1	služební
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k1gMnSc1	Oswald
opustil	opustit	k5eAaPmAgMnS	opustit
budovu	budova	k1gFnSc4	budova
Knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
ještě	ještě	k9	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
policií	policie	k1gFnSc7	policie
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
budovu	budova	k1gFnSc4	budova
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nadřízený	nadřízený	k2eAgMnSc1d1	nadřízený
Roy	Roy	k1gMnSc1	Roy
Truly	trula	k1gFnSc2	trula
tedy	tedy	k9	tedy
nahlásil	nahlásit	k5eAaPmAgInS	nahlásit
pohřešování	pohřešování	k1gNnSc4	pohřešování
přítomným	přítomný	k2eAgMnPc3d1	přítomný
příslušníkům	příslušník	k1gMnPc3	příslušník
dallaské	dallaský	k2eAgFnSc2d1	dallaská
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12.40	[number]	k4	12.40
Oswald	Oswald	k1gInSc1	Oswald
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
linkového	linkový	k2eAgInSc2d1	linkový
autobusu	autobus	k1gInSc2	autobus
č.	č.	k?	č.
1213	[number]	k4	1213
na	na	k7c6	na
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
zastávce	zastávka	k1gFnSc6	zastávka
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
autobusu	autobus	k1gInSc6	autobus
jej	on	k3xPp3gMnSc4	on
spatřila	spatřit	k5eAaPmAgFnS	spatřit
jeho	jeho	k3xOp3gFnSc1	jeho
někdejší	někdejší	k2eAgFnSc1d1	někdejší
bytná	bytný	k2eAgFnSc1d1	bytná
Mary	Mary	k1gFnSc1	Mary
E.	E.	kA	E.
Bledsoeová	Bledsoeová	k1gFnSc1	Bledsoeová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vypověděla	vypovědět	k5eAaPmAgFnS	vypovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
Oswald	Oswald	k1gMnSc1	Oswald
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
<g/>
.	.	kIx.	.
</s>
<s>
Vypadal	vypadat	k5eAaPmAgMnS	vypadat
jako	jako	k9	jako
blázen	blázen	k1gMnSc1	blázen
<g/>
.	.	kIx.	.
</s>
<s>
Rukávy	rukáv	k1gInPc1	rukáv
mu	on	k3xPp3gMnSc3	on
takto	takto	k6eAd1	takto
plandaly	plandat	k5eAaImAgInP	plandat
[	[	kIx(	[
<g/>
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Košili	košile	k1gFnSc3	košile
měl	mít	k5eAaImAgMnS	mít
nevyspravenou	vyspravený	k2eNgFnSc7d1	vyspravený
<g/>
...	...	k?	...
s	s	k7c7	s
dírou	díra	k1gFnSc7	díra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
špinavý	špinavý	k2eAgInSc1d1	špinavý
<g/>
,	,	kIx,	,
dívala	dívat	k5eAaImAgFnS	dívat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
radši	rád	k6eAd2	rád
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěla	chtít	k5eNaImAgFnS	chtít
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
spatřila	spatřit	k5eAaPmAgFnS	spatřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
koukala	koukat	k5eAaImAgFnS	koukat
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
okamžiku	okamžik	k1gInSc6	okamžik
řidič	řidič	k1gMnSc1	řidič
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
prezidenta	prezident	k1gMnSc4	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
silného	silný	k2eAgInSc2d1	silný
provozu	provoz	k1gInSc2	provoz
však	však	k9	však
jel	jet	k5eAaImAgInS	jet
autobus	autobus	k1gInSc1	autobus
velice	velice	k6eAd1	velice
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
Oswald	Oswald	k1gInSc4	Oswald
tedy	tedy	k9	tedy
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
bloky	blok	k1gInPc4	blok
dále	daleko	k6eAd2	daleko
požádal	požádat	k5eAaPmAgInS	požádat
řidiče	řidič	k1gMnSc4	řidič
o	o	k7c4	o
otevření	otevření	k1gNnSc4	otevření
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vystoupit	vystoupit	k5eAaPmF	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
šel	jít	k5eAaImAgMnS	jít
pěšky	pěšky	k6eAd1	pěšky
na	na	k7c4	na
blízkou	blízký	k2eAgFnSc4d1	blízká
zastávku	zastávka	k1gFnSc4	zastávka
společnosti	společnost	k1gFnSc2	společnost
Greyhound	Greyhound	k1gInSc1	Greyhound
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
přivolal	přivolat	k5eAaPmAgInS	přivolat
taxi	taxe	k1gFnSc4	taxe
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
svého	svůj	k3xOyFgNnSc2	svůj
bydliště	bydliště	k1gNnSc2	bydliště
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
1026	[number]	k4	1026
North	Northa	k1gFnPc2	Northa
Beckley	Becklea	k1gFnSc2	Becklea
Avenue	avenue	k1gFnSc2	avenue
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
bytu	byt	k1gInSc2	byt
přišel	přijít	k5eAaPmAgInS	přijít
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
13.00	[number]	k4	13.00
a	a	k8xC	a
dle	dle	k7c2	dle
výpovědi	výpověď	k1gFnSc2	výpověď
domovnice	domovnice	k1gFnSc2	domovnice
Earlene	Earlen	k1gInSc5	Earlen
Robertsové	Robertsová	k1gFnSc6	Robertsová
si	se	k3xPyFc3	se
oblékl	obléct	k5eAaPmAgMnS	obléct
kabát	kabát	k1gInSc4	kabát
a	a	k8xC	a
"	"	kIx"	"
<g/>
šel	jít	k5eAaImAgMnS	jít
velice	velice	k6eAd1	velice
rychlou	rychlý	k2eAgFnSc4d1	rychlá
chůzí	chůze	k1gFnSc7	chůze
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
běžel	běžet	k5eAaImAgMnS	běžet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k6eAd1	Oswald
opustil	opustit	k5eAaPmAgInS	opustit
dům	dům	k1gInSc1	dům
a	a	k8xC	a
zmíněnou	zmíněný	k2eAgFnSc7d1	zmíněná
svědkyní	svědkyně	k1gFnSc7	svědkyně
Robertsovou	Robertsová	k1gFnSc7	Robertsová
byl	být	k5eAaImAgMnS	být
naposledy	naposledy	k6eAd1	naposledy
viděn	vidět	k5eAaImNgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c6	na
blízké	blízký	k2eAgFnSc6d1	blízká
autobusové	autobusový	k2eAgFnSc6d1	autobusová
zastávce	zastávka	k1gFnSc6	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k1gInSc1	Oswald
byl	být	k5eAaImAgInS	být
zanedlouho	zanedlouho	k6eAd1	zanedlouho
poté	poté	k6eAd1	poté
spatřen	spatřit	k5eAaPmNgInS	spatřit
v	v	k7c6	v
rezidenční	rezidenční	k2eAgFnSc6d1	rezidenční
čtvrti	čtvrt	k1gFnSc6	čtvrt
Oak	Oak	k1gMnSc1	Oak
Cliff	Cliff	k1gMnSc1	Cliff
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Avenue	avenue	k1gFnPc1	avenue
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
cca	cca	kA	cca
1,3	[number]	k4	1,3
km	km	kA	km
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
bydliště	bydliště	k1gNnSc2	bydliště
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
zprávy	zpráva	k1gFnSc2	zpráva
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
jej	on	k3xPp3gMnSc4	on
zde	zde	k6eAd1	zde
zpozoroval	zpozorovat	k5eAaPmAgMnS	zpozorovat
strážník	strážník	k1gMnSc1	strážník
Tippit	Tippit	k1gMnSc1	Tippit
a	a	k8xC	a
promluvil	promluvit	k5eAaPmAgMnS	promluvit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
skrze	skrze	k?	skrze
okénko	okénko	k1gNnSc1	okénko
svého	svůj	k3xOyFgInSc2	svůj
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
Tippit	Tippit	k1gFnSc1	Tippit
z	z	k7c2	z
vozu	vůz	k1gInSc2	vůz
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
promluvit	promluvit	k5eAaPmF	promluvit
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Oswald	Oswald	k1gMnSc1	Oswald
dle	dle	k7c2	dle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
verze	verze	k1gFnSc2	verze
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
revolver	revolver	k1gInSc4	revolver
ráže	ráže	k1gFnSc2	ráže
.38	.38	k4	.38
a	a	k8xC	a
chladnokrevně	chladnokrevně	k6eAd1	chladnokrevně
jej	on	k3xPp3gMnSc4	on
čtyřmi	čtyři	k4xCgNnPc7	čtyři
ranami	rána	k1gFnPc7	rána
zastřelit	zastřelit	k5eAaPmF	zastřelit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
dvou	dva	k4xCgMnPc2	dva
svědků	svědek	k1gMnPc2	svědek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
důvodné	důvodný	k2eAgFnPc4d1	důvodná
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
Oswald	Oswald	k1gInSc1	Oswald
skutečně	skutečně	k6eAd1	skutečně
tou	ten	k3xDgFnSc7	ten
samou	samý	k3xTgFnSc7	samý
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Tippita	Tippita	k1gFnSc1	Tippita
zastřelila	zastřelit	k5eAaPmAgFnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Svědkyně	svědkyně	k1gFnSc1	svědkyně
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
Aquilla	Aquilla	k1gFnSc1	Aquilla
Clemmonsová	Clemmonsová	k1gFnSc1	Clemmonsová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Avenue	avenue	k1gFnSc1	avenue
a	a	k8xC	a
Patton	Patton	k1gInSc1	Patton
Avenue	avenue	k1gFnPc2	avenue
vypověděla	vypovědět	k5eAaPmAgNnP	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
muž	muž	k1gMnSc1	muž
střílející	střílející	k2eAgMnSc1d1	střílející
na	na	k7c4	na
policistu	policista	k1gMnSc4	policista
Tippita	Tippit	k1gMnSc4	Tippit
měl	mít	k5eAaImAgMnS	mít
krátce	krátce	k6eAd1	krátce
střižené	střižený	k2eAgInPc4d1	střižený
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vysoké	vysoký	k2eAgInPc4d1	vysoký
statné	statný	k2eAgInPc4d1	statný
postavy	postav	k1gInPc4	postav
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
o	o	k7c6	o
Oswaldovi	Oswald	k1gMnSc6	Oswald
říci	říct	k5eAaPmF	říct
nedá	dát	k5eNaPmIp3nS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
pochybnost	pochybnost	k1gFnSc1	pochybnost
vnáší	vnášet	k5eAaImIp3nS	vnášet
do	do	k7c2	do
případu	případ	k1gInSc2	případ
výpověď	výpověď	k1gFnSc4	výpověď
policisty	policista	k1gMnSc2	policista
Geralda	Gerald	k1gMnSc2	Gerald
Hilla	Hill	k1gMnSc2	Hill
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Warrenově	Warrenův	k2eAgFnSc3d1	Warrenova
komisi	komise	k1gFnSc3	komise
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
revolver	revolver	k1gInSc4	revolver
zajištěný	zajištěný	k2eAgInSc4d1	zajištěný
u	u	k7c2	u
Oswalda	Oswald	k1gMnSc2	Oswald
měl	mít	k5eAaImAgMnS	mít
plnou	plný	k2eAgFnSc4d1	plná
nábojovou	nábojový	k2eAgFnSc4d1	nábojová
komoru	komora	k1gFnSc4	komora
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
činu	čin	k1gInSc2	čin
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
vystřelené	vystřelený	k2eAgFnPc1d1	vystřelená
patrony	patrona	k1gFnPc1	patrona
místo	místo	k7c2	místo
"	"	kIx"	"
<g/>
oficiálních	oficiální	k2eAgInPc2d1	oficiální
<g/>
"	"	kIx"	"
čtyř	čtyři	k4xCgFnPc2	čtyři
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ráže	ráže	k1gFnSc1	ráže
těchto	tento	k3xDgFnPc2	tento
patron	patrona	k1gFnPc2	patrona
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
lišila	lišit	k5eAaImAgFnS	lišit
od	od	k7c2	od
ráže	ráže	k1gFnSc2	ráže
Oswaldova	Oswaldův	k2eAgInSc2d1	Oswaldův
revolveru	revolver	k1gInSc2	revolver
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zastřelení	zastřelení	k1gNnSc6	zastřelení
policisty	policista	k1gMnSc2	policista
Tippita	Tippit	k1gMnSc2	Tippit
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
Oswald	Oswald	k1gMnSc1	Oswald
schovat	schovat	k5eAaPmF	schovat
před	před	k7c7	před
přijíždějícími	přijíždějící	k2eAgInPc7d1	přijíždějící
policejními	policejní	k2eAgInPc7d1	policejní
vozy	vůz	k1gInPc7	vůz
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
obuví	obuv	k1gFnSc7	obuv
Hardy	Harda	k1gMnSc2	Harda
Shoe	Sho	k1gMnSc2	Sho
Store	Stor	k1gInSc5	Stor
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Jefferson	Jefferson	k1gInSc4	Jefferson
Ave	ave	k1gNnSc2	ave
a	a	k8xC	a
chvíli	chvíle	k1gFnSc4	chvíle
poté	poté	k6eAd1	poté
bez	bez	k7c2	bez
placení	placení	k1gNnSc2	placení
vniknul	vniknout	k5eAaPmAgMnS	vniknout
do	do	k7c2	do
místního	místní	k2eAgNnSc2d1	místní
kina	kino	k1gNnSc2	kino
"	"	kIx"	"
<g/>
Texas	Texas	k1gInSc1	Texas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dvaadvacetiletý	dvaadvacetiletý	k2eAgMnSc1d1	dvaadvacetiletý
prodavač	prodavač	k1gMnSc1	prodavač
z	z	k7c2	z
obuvnického	obuvnický	k2eAgInSc2d1	obuvnický
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
Johnny	Johnna	k1gFnSc2	Johnna
Calvin	Calvina	k1gFnPc2	Calvina
Brewer	Brewer	k1gInSc1	Brewer
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
zprávy	zpráva	k1gFnPc4	zpráva
v	v	k7c6	v
rádiu	rádio	k1gNnSc6	rádio
a	a	k8xC	a
Oswald	Oswald	k1gInSc1	Oswald
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdál	zdát	k5eAaImAgInS	zdát
podezřelý	podezřelý	k2eAgInSc4d1	podezřelý
<g/>
.	.	kIx.	.
</s>
<s>
Sledoval	sledovat	k5eAaImAgMnS	sledovat
jej	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
do	do	k7c2	do
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
kina	kino	k1gNnSc2	kino
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
podezření	podezření	k1gNnSc6	podezření
informoval	informovat	k5eAaBmAgMnS	informovat
prodavače	prodavač	k1gMnPc4	prodavač
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
posléze	posléze	k6eAd1	posléze
přivolal	přivolat	k5eAaPmAgInS	přivolat
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
cca	cca	kA	cca
ve	v	k7c6	v
13.45	[number]	k4	13.45
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnikla	vniknout	k5eAaPmAgFnS	vniknout
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
sálu	sál	k1gInSc2	sál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mělo	mít	k5eAaImAgNnS	mít
zanedlouho	zanedlouho	k6eAd1	zanedlouho
začít	začít	k5eAaPmF	začít
promítání	promítání	k1gNnSc4	promítání
filmu	film	k1gInSc2	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
War	War	k1gMnSc1	War
Is	Is	k1gMnSc1	Is
Hell	Hell	k1gMnSc1	Hell
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
peklo	peklo	k1gNnSc4	peklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zasahující	zasahující	k2eAgMnSc1d1	zasahující
policista	policista	k1gMnSc1	policista
Maurice	Maurika	k1gFnSc3	Maurika
N.	N.	kA	N.
McDonald	McDonald	k1gMnSc1	McDonald
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
k	k	k7c3	k
Oswaldovi	Oswald	k1gMnSc3	Oswald
sedícímu	sedící	k2eAgInSc3d1	sedící
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k1gInSc1	Oswald
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
No	no	k9	no
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
konec	konec	k1gInSc1	konec
<g/>
"	"	kIx"	"
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvedá	zvedat	k5eAaImIp3nS	zvedat
ruce	ruka	k1gFnPc4	ruka
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
kapitulace	kapitulace	k1gFnSc2	kapitulace
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
policistu	policista	k1gMnSc4	policista
McDonalda	McDonald	k1gMnSc4	McDonald
odstrčil	odstrčit	k5eAaPmAgMnS	odstrčit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
svůj	svůj	k3xOyFgInSc4	svůj
revolver	revolver	k1gInSc4	revolver
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
bránit	bránit	k5eAaImF	bránit
střelbou	střelba	k1gFnSc7	střelba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
McDonald	McDonald	k1gInSc1	McDonald
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
kohout	kohout	k1gInSc4	kohout
a	a	k8xC	a
nábojový	nábojový	k2eAgInSc4d1	nábojový
válec	válec	k1gInSc4	válec
revolveru	revolver	k1gInSc2	revolver
rychle	rychle	k6eAd1	rychle
vrazil	vrazit	k5eAaPmAgMnS	vrazit
tenkou	tenký	k2eAgFnSc4d1	tenká
blanku	blanka	k1gFnSc4	blanka
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
lidské	lidský	k2eAgFnSc6d1	lidská
ruce	ruka	k1gFnSc6	ruka
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
palcem	palec	k1gInSc7	palec
a	a	k8xC	a
ukazováčkem	ukazováček	k1gInSc7	ukazováček
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Oswald	Oswald	k1gInSc1	Oswald
vyváděn	vyvádět	k5eAaImNgInS	vyvádět
ven	ven	k6eAd1	ven
z	z	k7c2	z
kina	kino	k1gNnSc2	kino
kolem	kolem	k7c2	kolem
skupinky	skupinka	k1gFnSc2	skupinka
přihlížejících	přihlížející	k2eAgInPc2d1	přihlížející
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
vykřikoval	vykřikovat	k5eAaImAgMnS	vykřikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
"	"	kIx"	"
<g/>
obětí	oběť	k1gFnSc7	oběť
policejní	policejní	k2eAgFnSc2d1	policejní
brutality	brutalita	k1gFnSc2	brutalita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následující	následující	k2eAgFnSc2d1	následující
noci	noc	k1gFnSc2	noc
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
sejmuty	sejmout	k5eAaPmNgInP	sejmout
otisky	otisk	k1gInPc1	otisk
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
následně	následně	k6eAd1	následně
sděleno	sdělen	k2eAgNnSc4d1	sděleno
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
policisty	policista	k1gMnSc2	policista
Tippita	Tippit	k1gMnSc2	Tippit
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k1gInSc1	Oswald
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
svou	svůj	k3xOyFgFnSc4	svůj
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
oběma	dva	k4xCgFnPc7	dva
vraždami	vražda	k1gFnPc7	vražda
a	a	k8xC	a
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
sdělil	sdělit	k5eAaPmAgMnS	sdělit
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
reportérů	reportér	k1gMnPc2	reportér
<g/>
,	,	kIx,	,
přítomných	přítomný	k2eAgMnPc2d1	přítomný
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Dallaského	Dallaský	k2eAgNnSc2d1	Dallaský
policejního	policejní	k2eAgNnSc2d1	policejní
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
obětním	obětní	k2eAgInSc7d1	obětní
beránkem	beránek	k1gInSc7	beránek
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
reportéra	reportér	k1gMnSc2	reportér
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
jste	být	k5eAaImIp2nP	být
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Odpověď	odpověď	k1gFnSc1	odpověď
Oswalda	Oswalda	k1gFnSc1	Oswalda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechápu	chápat	k5eNaImIp1nS	chápat
o	o	k7c6	o
čem	co	k3yInSc6	co
tohle	tenhle	k3xDgNnSc1	tenhle
celé	celý	k2eAgNnSc1d1	celé
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Sebrali	sebrat	k5eAaPmAgMnP	sebrat
mě	já	k3xPp1nSc4	já
jenom	jenom	k6eAd1	jenom
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
...	...	k?	...
jsem	být	k5eAaImIp1nS	být
jen	jen	k9	jen
obětní	obětní	k2eAgInSc1d1	obětní
beránek	beránek	k1gInSc1	beránek
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Případ	případ	k1gInSc1	případ
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Oswald	Oswald	k1gInSc1	Oswald
byl	být	k5eAaImAgInS	být
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
televizních	televizní	k2eAgInPc2d1	televizní
štábů	štáb	k1gInPc2	štáb
tehdy	tehdy	k6eAd1	tehdy
dvaapadesátiletým	dvaapadesátiletý	k2eAgMnSc7d1	dvaapadesátiletý
Jacobem	Jacob	k1gMnSc7	Jacob
Rubinsteinem	Rubinstein	k1gInSc7	Rubinstein
<g/>
,	,	kIx,	,
zvaným	zvaný	k2eAgInSc7d1	zvaný
"	"	kIx"	"
<g/>
Jack	Jack	k1gInSc1	Jack
Ruby	rub	k1gInPc1	rub
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
majitelem	majitel	k1gMnSc7	majitel
místního	místní	k2eAgInSc2d1	místní
nočního	noční	k2eAgInSc2d1	noční
klubu	klub	k1gInSc2	klub
"	"	kIx"	"
<g/>
Carousel	Carousel	k1gInSc1	Carousel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
útoku	útok	k1gInSc3	útok
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
garáži	garáž	k1gFnSc6	garáž
Dallaské	Dallaský	k2eAgFnSc2d1	Dallaská
vazební	vazební	k2eAgFnSc2d1	vazební
věznice	věznice	k1gFnSc2	věznice
v	v	k7c6	v
11.20	[number]	k4	11.20
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Oswalda	Oswalda	k1gMnSc1	Oswalda
vyhlížel	vyhlížet	k5eAaImAgMnS	vyhlížet
dav	dav	k1gInSc4	dav
novinářů	novinář	k1gMnPc2	novinář
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
přistavena	přistaven	k2eAgFnSc1d1	přistavena
vězeňská	vězeňský	k2eAgFnSc1d1	vězeňská
dodávka	dodávka	k1gFnSc1	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gNnSc4	on
měla	mít	k5eAaImAgFnS	mít
neprodleně	prodleně	k6eNd1	prodleně
transportovat	transportovat	k5eAaBmF	transportovat
do	do	k7c2	do
Dallaské	Dallaský	k2eAgFnSc2d1	Dallaská
okresní	okresní	k2eAgFnSc2d1	okresní
věznice	věznice	k1gFnSc2	věznice
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
k	k	k7c3	k
cele	cela	k1gFnSc3	cela
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
ostrahou	ostraha	k1gFnSc7	ostraha
<g/>
.	.	kIx.	.
</s>
<s>
Rubinstein	Rubinstein	k1gInSc4	Rubinstein
<g/>
,	,	kIx,	,
ukrytý	ukrytý	k2eAgInSc4d1	ukrytý
mezi	mezi	k7c7	mezi
novináři	novinář	k1gMnPc7	novinář
<g/>
,	,	kIx,	,
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
průchodu	průchod	k1gInSc2	průchod
Oswalda	Oswalda	k1gMnSc1	Oswalda
vyskočil	vyskočit	k5eAaPmAgMnS	vyskočit
z	z	k7c2	z
davu	dav	k1gInSc2	dav
<g/>
,	,	kIx,	,
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
zpod	zpod	k7c2	zpod
kabátu	kabát	k1gInSc2	kabát
svůj	svůj	k3xOyFgInSc4	svůj
revolver	revolver	k1gInSc4	revolver
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
výstřelem	výstřel	k1gInSc7	výstřel
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
ještě	ještě	k6eAd1	ještě
zakřičel	zakřičet	k5eAaPmAgMnS	zakřičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zabil	zabít	k5eAaPmAgMnS	zabít
jsi	být	k5eAaImIp2nS	být
mého	můj	k3xOp1gMnSc4	můj
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc4	ten
kryso	krysa	k1gFnSc5	krysa
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
policistů	policista	k1gMnPc2	policista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vedli	vést	k5eAaImAgMnP	vést
spoutaného	spoutaný	k2eAgMnSc4d1	spoutaný
Oswalda	Oswald	k1gMnSc4	Oswald
<g/>
,	,	kIx,	,
stihl	stihnout	k5eAaPmAgMnS	stihnout
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
údivu	údiv	k1gInSc6	údiv
vykřiknout	vykřiknout	k5eAaPmF	vykřiknout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jacku	Jacka	k1gFnSc4	Jacka
<g/>
,	,	kIx,	,
ty	ten	k3xDgMnPc4	ten
zkurvysynu	zkurvysynu	k?	zkurvysynu
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Oswald	Oswald	k1gMnSc1	Oswald
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
průstřel	průstřel	k1gInSc4	průstřel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dutiny	dutina	k1gFnSc2	dutina
břišní	břišní	k2eAgFnSc1d1	břišní
s	s	k7c7	s
následným	následný	k2eAgNnSc7d1	následné
masivním	masivní	k2eAgNnSc7d1	masivní
krvácením	krvácení	k1gNnSc7	krvácení
z	z	k7c2	z
poškozené	poškozený	k2eAgFnSc2d1	poškozená
mezenterické	mezenterický	k2eAgFnSc2d1	mezenterický
tepny	tepna	k1gFnSc2	tepna
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
Parkland	Parklanda	k1gFnPc2	Parklanda
Memorial	Memorial	k1gMnSc1	Memorial
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
dříve	dříve	k6eAd2	dříve
zemřel	zemřít	k5eAaPmAgMnS	zemřít
prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
.	.	kIx.	.
</s>
<s>
Oswaldovo	Oswaldův	k2eAgNnSc1d1	Oswaldovo
zranění	zranění	k1gNnSc1	zranění
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
natolik	natolik	k6eAd1	natolik
vážné	vážný	k2eAgNnSc1d1	vážné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ve	v	k7c6	v
13.07	[number]	k4	13.07
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Ruby	rub	k1gInPc4	rub
odůvodnil	odůvodnit	k5eAaPmAgInS	odůvodnit
svůj	svůj	k3xOyFgInSc4	svůj
čin	čin	k1gInSc4	čin
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
chtěl	chtít	k5eAaImAgMnS	chtít
ušetřit	ušetřit	k5eAaPmF	ušetřit
vdovu	vdova	k1gFnSc4	vdova
po	po	k7c6	po
zesnulém	zesnulý	k1gMnSc6	zesnulý
prezidentovi	prezident	k1gMnSc6	prezident
od	od	k7c2	od
útrap	útrapa	k1gFnPc2	útrapa
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
projít	projít	k5eAaPmF	projít
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
s	s	k7c7	s
vrahem	vrah	k1gMnSc7	vrah
svého	svůj	k1gMnSc2	svůj
manžela	manžel	k1gMnSc2	manžel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
komentátorů	komentátor	k1gMnPc2	komentátor
se	se	k3xPyFc4	se
však	však	k9	však
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rubyho	Ruby	k1gMnSc4	Ruby
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
odstranit	odstranit	k5eAaPmF	odstranit
Oswalda	Oswald	k1gMnSc4	Oswald
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pravda	pravda	k1gFnSc1	pravda
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
Ruby	rub	k1gInPc4	rub
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
čin	čin	k1gInSc4	čin
odsouzen	odsouzen	k2eAgInSc1d1	odsouzen
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1964	[number]	k4	1964
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
křesle	křeslo	k1gNnSc6	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Rubymu	Rubym	k1gInSc3	Rubym
nepomohla	pomoct	k5eNaPmAgFnS	pomoct
ani	ani	k8xC	ani
snaha	snaha	k1gFnSc1	snaha
obhájců	obhájce	k1gMnPc2	obhájce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
poukázat	poukázat	k5eAaPmF	poukázat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
sníženou	snížený	k2eAgFnSc4d1	snížená
příčetnost	příčetnost	k1gFnSc4	příčetnost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
a	a	k8xC	a
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
rozsudku	rozsudek	k1gInSc6	rozsudek
bylo	být	k5eAaImAgNnS	být
vyřčeno	vyřknout	k5eAaPmNgNnS	vyřknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ruby	rub	k1gInPc7	rub
dopustil	dopustit	k5eAaPmAgMnS	dopustit
chladnokrevné	chladnokrevný	k2eAgFnSc2d1	chladnokrevná
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
odsouzení	odsouzení	k1gNnSc6	odsouzení
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
úspěšně	úspěšně	k6eAd1	úspěšně
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
než	než	k8xS	než
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
nový	nový	k2eAgInSc1d1	nový
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgInS	zemřít
Ruby	rub	k1gInPc4	rub
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Ruby	rub	k1gInPc1	rub
se	se	k3xPyFc4	se
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
věznění	věznění	k1gNnSc2	věznění
označoval	označovat	k5eAaImAgInS	označovat
též	též	k9	též
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
a	a	k8xC	a
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
televizníćh	televizníćh	k1gMnSc1	televizníćh
interview	interview	k1gNnSc2	interview
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Svět	svět	k1gInSc1	svět
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nedozví	dozvědět	k5eNaPmIp3nS	dozvědět
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
moje	můj	k3xOp1gInPc4	můj
pravdivé	pravdivý	k2eAgInPc4d1	pravdivý
motivy	motiv	k1gInPc4	motiv
<g/>
...	...	k?	...
naneštěstí	naneštěstí	k9	naneštěstí
ti	ten	k3xDgMnPc1	ten
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tím	ten	k3xDgMnSc7	ten
tolik	tolik	k6eAd1	tolik
získali	získat	k5eAaPmAgMnP	získat
a	a	k8xC	a
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
silný	silný	k2eAgInSc4d1	silný
motiv	motiv	k1gInSc4	motiv
mne	já	k3xPp1nSc4	já
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
skutečná	skutečný	k2eAgFnSc1d1	skutečná
pravda	pravda	k1gFnSc1	pravda
spatřila	spatřit	k5eAaPmAgFnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Carcano	Carcana	k1gFnSc5	Carcana
(	(	kIx(	(
<g/>
puška	puška	k1gFnSc1	puška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
použitou	použitý	k2eAgFnSc4d1	použitá
ke	k	k7c3	k
spáchání	spáchání	k1gNnSc3	spáchání
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
puška	puška	k1gFnSc1	puška
(	(	kIx(	(
<g/>
karabina	karabina	k1gFnSc1	karabina
<g/>
)	)	kIx)	)
italské	italský	k2eAgFnSc2d1	italská
výroby	výroba	k1gFnSc2	výroba
–	–	k?	–
Carcano	Carcana	k1gFnSc5	Carcana
M	M	kA	M
<g/>
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
ráže	ráže	k1gFnSc1	ráže
6,52	[number]	k4	6,52
x	x	k?	x
52	[number]	k4	52
mm	mm	kA	mm
<g/>
,	,	kIx,	,
se	s	k7c7	s
sériovým	sériový	k2eAgNnSc7d1	sériové
číslem	číslo	k1gNnSc7	číslo
C	C	kA	C
<g/>
2766	[number]	k4	2766
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
současnými	současný	k2eAgFnPc7d1	současná
puškami	puška	k1gFnPc7	puška
a	a	k8xC	a
karabinami	karabina	k1gFnPc7	karabina
nedá	dát	k5eNaPmIp3nS	dát
hovořit	hovořit	k5eAaImF	hovořit
jako	jako	k9	jako
o	o	k7c4	o
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
výkonné	výkonný	k2eAgInPc1d1	výkonný
či	či	k8xC	či
přesné	přesný	k2eAgInPc1d1	přesný
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
vezl	vézt	k5eAaImAgMnS	vézt
Oswalda	Oswalda	k1gMnSc1	Oswalda
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
jeho	jeho	k3xOp3gMnSc1	jeho
kolega	kolega	k1gMnSc1	kolega
Wesley	Weslea	k1gFnSc2	Weslea
Frazier	Frazier	k1gMnSc1	Frazier
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nese	nést	k5eAaImIp3nS	nést
nějaký	nějaký	k3yIgInSc4	nějaký
delší	dlouhý	k2eAgInSc4d2	delší
předmět	předmět	k1gInSc4	předmět
zabalený	zabalený	k2eAgInSc4d1	zabalený
v	v	k7c6	v
papíru	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Frazier	Frazier	k1gMnSc1	Frazier
dotázal	dotázat	k5eAaPmAgMnS	dotázat
o	o	k7c4	o
co	co	k3yQnSc4	co
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
mu	on	k3xPp3gMnSc3	on
Oswald	Oswald	k1gInSc4	Oswald
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
tyč	tyč	k1gFnSc1	tyč
na	na	k7c4	na
záclony	záclona	k1gFnPc4	záclona
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
později	pozdě	k6eAd2	pozdě
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
papírovém	papírový	k2eAgNnSc6d1	papírové
pouzdře	pouzdro	k1gNnSc6	pouzdro
zabalenu	zabalen	k2eAgFnSc4d1	zabalena
právě	právě	k9	právě
onu	onen	k3xDgFnSc4	onen
vražednou	vražedný	k2eAgFnSc4d1	vražedná
pušku	puška	k1gFnSc4	puška
Carcano	Carcana	k1gFnSc5	Carcana
<g/>
.	.	kIx.	.
</s>
<s>
Puška	puška	k1gFnSc1	puška
Carcano	Carcana	k1gFnSc5	Carcana
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
funkční	funkční	k2eAgFnSc6d1	funkční
podobě	podoba	k1gFnSc6	podoba
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
přibližně	přibližně	k6eAd1	přibližně
97	[number]	k4	97
cm	cm	kA	cm
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozmontování	rozmontování	k1gNnSc6	rozmontování
nejméně	málo	k6eAd3	málo
88	[number]	k4	88
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědeckých	svědecký	k2eAgFnPc2d1	svědecká
výpovědí	výpověď	k1gFnPc2	výpověď
L.	L.	kA	L.
M.	M.	kA	M.
Randlové	Randlový	k2eAgFnPc1d1	Randlová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Oswalda	Oswalda	k1gFnSc1	Oswalda
viděla	vidět	k5eAaImAgFnS	vidět
ráno	ráno	k6eAd1	ráno
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
domem	dům	k1gInSc7	dům
a	a	k8xC	a
dle	dle	k7c2	dle
výpovědi	výpověď	k1gFnSc2	výpověď
již	již	k6eAd1	již
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
kolegy	kolega	k1gMnSc2	kolega
Fraziera	Frazier	k1gMnSc2	Frazier
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
Oswald	Oswald	k1gInSc1	Oswald
nést	nést	k5eAaImF	nést
papírový	papírový	k2eAgInSc4d1	papírový
balík	balík	k1gInSc4	balík
o	o	k7c6	o
přibližné	přibližný	k2eAgFnSc6d1	přibližná
délce	délka	k1gFnSc6	délka
68	[number]	k4	68
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výpověď	výpověď	k1gFnSc1	výpověď
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
policie	policie	k1gFnSc1	policie
přeměřila	přeměřit	k5eAaPmAgFnS	přeměřit
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Frazierově	Frazierův	k2eAgNnSc6d1	Frazierův
autě	auto	k1gNnSc6	auto
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
dotyčný	dotyčný	k2eAgInSc4d1	dotyčný
balík	balík	k1gInSc4	balík
ležel	ležet	k5eAaImAgMnS	ležet
<g/>
:	:	kIx,	:
mezi	mezi	k7c7	mezi
sedadlem	sedadlo	k1gNnSc7	sedadlo
a	a	k8xC	a
dveřmi	dveře	k1gFnPc7	dveře
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgInPc4	jenž
byl	být	k5eAaImAgInS	být
balík	balík	k1gInSc1	balík
opřen	opřít	k5eAaPmNgInS	opřít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
27	[number]	k4	27
palců	palec	k1gInPc2	palec
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
68	[number]	k4	68
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
nesrovnalostí	nesrovnalost	k1gFnSc7	nesrovnalost
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
nikterak	nikterak	k6eAd1	nikterak
nezabývala	zabývat	k5eNaImAgFnS	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Frazier	Frazier	k1gMnSc1	Frazier
Oswalda	Oswalda	k1gMnSc1	Oswalda
to	ten	k3xDgNnSc4	ten
ráno	ráno	k6eAd1	ráno
vysadil	vysadit	k5eAaPmAgMnS	vysadit
na	na	k7c6	na
parkovišti	parkoviště	k1gNnSc6	parkoviště
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
dva	dva	k4xCgInPc4	dva
bloky	blok	k1gInPc4	blok
od	od	k7c2	od
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
,	,	kIx,	,
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
jej	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
přímo	přímo	k6eAd1	přímo
nedoprovázel	doprovázet	k5eNaImAgMnS	doprovázet
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
zdržel	zdržet	k5eAaPmAgInS	zdržet
pozorováním	pozorování	k1gNnSc7	pozorování
posouvaných	posouvaný	k2eAgInPc2d1	posouvaný
vagónů	vagón	k1gInPc2	vagón
na	na	k7c6	na
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
vlakovém	vlakový	k2eAgNnSc6d1	vlakové
nádraží	nádraží	k1gNnSc6	nádraží
a	a	k8xC	a
nemohl	moct	k5eNaImAgMnS	moct
tak	tak	k6eAd1	tak
dosvědčit	dosvědčit	k5eAaPmF	dosvědčit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Oswald	Oswald	k1gInSc1	Oswald
se	s	k7c7	s
zmíněným	zmíněný	k2eAgInSc7d1	zmíněný
balíkem	balík	k1gInSc7	balík
skutečně	skutečně	k6eAd1	skutečně
vešel	vejít	k5eAaPmAgInS	vejít
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
velkoskladu	velkosklad	k1gInSc2	velkosklad
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
svědek	svědek	k1gMnSc1	svědek
sice	sice	k8xC	sice
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Oswalda	Oswalda	k1gMnSc1	Oswalda
to	ten	k3xDgNnSc4	ten
ráno	ráno	k6eAd1	ráno
viděl	vidět	k5eAaImAgMnS	vidět
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
nepamatoval	pamatovat	k5eNaImAgMnS	pamatovat
si	se	k3xPyFc3	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
dovnitř	dovnitř	k6eAd1	dovnitř
cokoliv	cokoliv	k3yInSc4	cokoliv
vnášel	vnášet	k5eAaImAgInS	vnášet
<g/>
.	.	kIx.	.
</s>
<s>
Dallaská	Dallaský	k2eAgFnSc1d1	Dallaská
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
ani	ani	k8xC	ani
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
tedy	tedy	k9	tedy
nebyly	být	k5eNaImAgInP	být
schopny	schopen	k2eAgInPc1d1	schopen
zajistit	zajistit	k5eAaPmF	zajistit
jediného	jediný	k2eAgMnSc4d1	jediný
svědka	svědek	k1gMnSc4	svědek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
dosvědčit	dosvědčit	k5eAaPmF	dosvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Oswald	Oswald	k1gInSc1	Oswald
v	v	k7c4	v
den	den	k1gInSc4	den
atentátu	atentát	k1gInSc2	atentát
vnesl	vnést	k5eAaPmAgMnS	vnést
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
velkoskladu	velkosklad	k1gInSc2	velkosklad
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
třebas	třebas	k8wRxS	třebas
jen	jen	k9	jen
připomínající	připomínající	k2eAgFnSc4d1	připomínající
pušku	puška	k1gFnSc4	puška
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Oswaldova	Oswaldův	k2eAgFnSc1d1	Oswaldova
<g/>
"	"	kIx"	"
zbraň	zbraň	k1gFnSc1	zbraň
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
zaměřovacím	zaměřovací	k2eAgInSc7d1	zaměřovací
dalekohledem	dalekohled	k1gInSc7	dalekohled
japonské	japonský	k2eAgFnSc2d1	japonská
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
zpětným	zpětný	k2eAgInSc7d1	zpětný
rázem	ráz	k1gInSc7	ráz
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
tato	tento	k3xDgFnSc1	tento
karabina	karabina	k1gFnSc1	karabina
trpěla	trpět	k5eAaImAgFnS	trpět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
zbraň	zbraň	k1gFnSc4	zbraň
schopnou	schopný	k2eAgFnSc7d1	schopná
jen	jen	k9	jen
pomalé	pomalý	k2eAgFnPc1d1	pomalá
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
také	také	k9	také
nepřesné	přesný	k2eNgFnSc2d1	nepřesná
střelby	střelba	k1gFnSc2	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgFnPc4d1	značná
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
mohl	moct	k5eAaImAgMnS	moct
Oswald	Oswald	k1gMnSc1	Oswald
touto	tento	k3xDgFnSc7	tento
zbraní	zbraň	k1gFnSc7	zbraň
třikrát	třikrát	k6eAd1	třikrát
takto	takto	k6eAd1	takto
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
vypálit	vypálit	k5eAaPmF	vypálit
<g/>
.	.	kIx.	.
</s>
<s>
Zbraň	zbraň	k1gFnSc1	zbraň
byla	být	k5eAaImAgFnS	být
nalezena	nalézt	k5eAaBmNgFnS	nalézt
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
policisty	policista	k1gMnPc7	policista
Seymourem	Seymour	k1gMnSc7	Seymour
Weitzmanem	Weitzman	k1gMnSc7	Weitzman
a	a	k8xC	a
Eugenem	Eugen	k1gMnSc7	Eugen
Boonem	Boon	k1gMnSc7	Boon
zanedlouho	zanedlouho	k6eAd1	zanedlouho
po	po	k7c4	po
spáchání	spáchání	k1gNnSc4	spáchání
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
pod	pod	k7c7	pod
oknem	okno	k1gNnSc7	okno
byly	být	k5eAaImAgFnP	být
též	též	k9	též
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
tři	tři	k4xCgFnPc1	tři
vypálené	vypálený	k2eAgFnPc1d1	vypálená
nábojnice	nábojnice	k1gFnPc1	nábojnice
ze	z	k7c2	z
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
pušky	puška	k1gFnSc2	puška
<g/>
.	.	kIx.	.
</s>
<s>
Policista	policista	k1gMnSc1	policista
J.	J.	kA	J.
W.	W.	kA	W.
Fritz	Fritz	k1gMnSc1	Fritz
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
místo	místo	k6eAd1	místo
přivolán	přivolán	k2eAgInSc4d1	přivolán
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nebylo	být	k5eNaImAgNnS	být
s	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
nalezeným	nalezený	k2eAgInSc7d1	nalezený
důkazem	důkaz	k1gInSc7	důkaz
manipulováno	manipulován	k2eAgNnSc1d1	manipulováno
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
vše	všechen	k3xTgNnSc1	všechen
fotograficky	fotograficky	k6eAd1	fotograficky
zdokumentuje	zdokumentovat	k5eAaPmIp3nS	zdokumentovat
poručík	poručík	k1gMnSc1	poručík
Day	Day	k1gMnSc1	Day
z	z	k7c2	z
dallaského	dallaský	k2eAgNnSc2d1	dallaský
policejního	policejní	k2eAgNnSc2d1	policejní
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
zbraně	zbraň	k1gFnSc2	zbraň
posléze	posléze	k6eAd1	posléze
nafilmoval	nafilmovat	k5eAaPmAgMnS	nafilmovat
Tom	Tom	k1gMnSc1	Tom
Alyea	Alyea	k1gMnSc1	Alyea
<g/>
,	,	kIx,	,
reportér	reportér	k1gMnSc1	reportér
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
WFAA	WFAA	kA	WFAA
<g/>
−	−	k?	−
<g/>
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
Analýzy	analýza	k1gFnPc1	analýza
provedené	provedený	k2eAgFnSc2d1	provedená
HSCA	HSCA	kA	HSCA
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
k	k	k7c3	k
atentátu	atentát	k1gInSc3	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
také	také	k9	také
kulka	kulka	k1gFnSc1	kulka
vytažená	vytažený	k2eAgFnSc1d1	vytažená
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
guvernéra	guvernér	k1gMnSc2	guvernér
Connallyho	Connally	k1gMnSc2	Connally
byla	být	k5eAaImAgNnP	být
vystřelena	vystřelit	k5eAaPmNgNnP	vystřelit
z	z	k7c2	z
nalezené	nalezený	k2eAgFnSc2d1	nalezená
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Zbraň	zbraň	k1gFnSc1	zbraň
byla	být	k5eAaImAgFnS	být
zakoupena	zakoupit	k5eAaPmNgFnS	zakoupit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
Lee	Lea	k1gFnSc3	Lea
Harvey	Harve	k2eAgInPc1d1	Harve
Oswaldem	Oswaldo	k1gNnSc7	Oswaldo
pod	pod	k7c7	pod
falešným	falešný	k2eAgNnSc7d1	falešné
jménem	jméno	k1gNnSc7	jméno
Alek	alka	k1gFnPc2	alka
J.	J.	kA	J.
Hidell	Hidell	k1gInSc1	Hidell
a	a	k8xC	a
doručena	doručit	k5eAaPmNgFnS	doručit
do	do	k7c2	do
P.	P.	kA	P.
<g/>
O.	O.	kA	O.
Boxu	box	k1gInSc2	box
č.	č.	k?	č.
2915	[number]	k4	2915
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
pažbě	pažba	k1gFnSc6	pažba
zbraně	zbraň	k1gFnSc2	zbraň
nalezen	nalezen	k2eAgInSc1d1	nalezen
částečný	částečný	k2eAgInSc1d1	částečný
otisk	otisk	k1gInSc1	otisk
dlaně	dlaň	k1gFnSc2	dlaň
Lee	Lea	k1gFnSc3	Lea
Harveyho	Harvey	k1gMnSc2	Harvey
Oswalda	Oswald	k1gMnSc2	Oswald
<g/>
.	.	kIx.	.
</s>
<s>
Experti	expert	k1gMnPc1	expert
z	z	k7c2	z
FBI	FBI	kA	FBI
ovšem	ovšem	k9	ovšem
byli	být	k5eAaImAgMnP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
otisk	otisk	k1gInSc1	otisk
sňat	snít	k5eAaPmNgInS	snít
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Oswald	Oswald	k1gInSc1	Oswald
mohl	moct	k5eAaImAgInS	moct
dotknout	dotknout	k5eAaPmF	dotknout
jen	jen	k9	jen
při	při	k7c6	při
demontáži	demontáž	k1gFnSc6	demontáž
pušky	puška	k1gFnSc2	puška
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
pouze	pouze	k6eAd1	pouze
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Oswald	Oswald	k1gInSc1	Oswald
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
puškou	puška	k1gFnSc7	puška
manipuloval	manipulovat	k5eAaImAgMnS	manipulovat
a	a	k8xC	a
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
nespecifikované	specifikovaný	k2eNgFnSc6d1	nespecifikovaná
době	doba	k1gFnSc6	doba
ji	on	k3xPp3gFnSc4	on
demontoval	demontovat	k5eAaBmAgInS	demontovat
<g/>
.	.	kIx.	.
</s>
<s>
Neříká	říkat	k5eNaImIp3nS	říkat
však	však	k9	však
zcela	zcela	k6eAd1	zcela
nic	nic	k3yNnSc1	nic
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
z	z	k7c2	z
nalezené	nalezený	k2eAgFnSc2d1	nalezená
pušky	puška	k1gFnSc2	puška
střílel	střílet	k5eAaImAgMnS	střílet
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
provázely	provázet	k5eAaImAgInP	provázet
identifikaci	identifikace	k1gFnSc4	identifikace
zbraně	zbraň	k1gFnSc2	zbraň
četné	četný	k2eAgFnSc2d1	četná
nejasnosti	nejasnost	k1gFnSc2	nejasnost
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
policista	policista	k1gMnSc1	policista
Seymour	Seymour	k1gMnSc1	Seymour
Weizman	Weizman	k1gMnSc1	Weizman
totiž	totiž	k9	totiž
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
prvotním	prvotní	k2eAgNnSc6d1	prvotní
hlášení	hlášení	k1gNnSc6	hlášení
identifikoval	identifikovat	k5eAaBmAgInS	identifikovat
nalezenou	nalezený	k2eAgFnSc4d1	nalezená
zbraň	zbraň	k1gFnSc4	zbraň
jako	jako	k8xC	jako
německou	německý	k2eAgFnSc4d1	německá
pušku	puška	k1gFnSc4	puška
Mauser	Mauser	k1gInSc1	Mauser
M	M	kA	M
98	[number]	k4	98
<g/>
,	,	kIx,	,
ráže	ráže	k1gFnSc1	ráže
7,92	[number]	k4	7,92
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc4d1	československý
deník	deník	k1gInSc4	deník
Rudé	rudý	k2eAgNnSc1d1	Rudé
Právo	právo	k1gNnSc1	právo
pak	pak	k6eAd1	pak
ovšem	ovšem	k9	ovšem
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
uvedl	uvést	k5eAaPmAgMnS	uvést
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
NBC	NBC	kA	NBC
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c4	po
Kennedyho	Kennedy	k1gMnSc4	Kennedy
vrahovi	vrah	k1gMnSc3	vrah
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rohovém	rohový	k2eAgNnSc6d1	rohové
okně	okno	k1gNnSc6	okno
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byly	být	k5eAaImAgFnP	být
vypáleny	vypálen	k2eAgFnPc1d1	vypálena
smrtící	smrtící	k2eAgFnPc1d1	smrtící
střely	střela	k1gFnPc1	střela
<g/>
,	,	kIx,	,
nalezena	nalezen	k2eAgFnSc1d1	nalezena
lovecká	lovecký	k2eAgFnSc1d1	lovecká
puška	puška	k1gFnSc1	puška
britské	britský	k2eAgFnSc2d1	britská
výroby	výroba	k1gFnSc2	výroba
s	s	k7c7	s
dalekohledem	dalekohled	k1gInSc7	dalekohled
a	a	k8xC	a
na	na	k7c6	na
podlaze	podlaha	k1gFnSc6	podlaha
místnosti	místnost	k1gFnSc6	místnost
tři	tři	k4xCgFnPc4	tři
prázdné	prázdný	k2eAgFnPc4d1	prázdná
nábojnice	nábojnice	k1gFnPc4	nábojnice
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
však	však	k9	však
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
tajná	tajný	k2eAgFnSc1d1	tajná
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vrahova	vrahův	k2eAgFnSc1d1	vrahova
zbraň	zbraň	k1gFnSc1	zbraň
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
průbojná	průbojný	k2eAgFnSc1d1	průbojná
armádní	armádní	k2eAgFnSc1d1	armádní
nebo	nebo	k8xC	nebo
japonská	japonský	k2eAgFnSc1d1	japonská
kulovnice	kulovnice	k1gFnSc1	kulovnice
ráže	ráže	k1gFnSc2	ráže
asi	asi	k9	asi
8	[number]	k4	8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
V	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
vyšetřovacích	vyšetřovací	k2eAgInPc6d1	vyšetřovací
spisech	spis	k1gInPc6	spis
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
hovoří	hovořit	k5eAaImIp3nS	hovořit
výhradně	výhradně	k6eAd1	výhradně
o	o	k7c6	o
pušce	puška	k1gFnSc6	puška
Carcano	Carcana	k1gFnSc5	Carcana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
12.36	[number]	k4	12.36
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pouhých	pouhý	k2eAgFnPc2d1	pouhá
šest	šest	k4xCc1	šest
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
<g/>
,	,	kIx,	,
dorazily	dorazit	k5eAaPmAgFnP	dorazit
k	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
vchodu	vchod	k1gInSc3	vchod
nemocnice	nemocnice	k1gFnSc2	nemocnice
Parkland	Parklanda	k1gFnPc2	Parklanda
Memorial	Memorial	k1gMnSc1	Memorial
Hospital	Hospital	k1gMnSc1	Hospital
postupně	postupně	k6eAd1	postupně
vedoucí	vedoucí	k1gMnPc4	vedoucí
automobil	automobil	k1gInSc1	automobil
s	s	k7c7	s
policejním	policejní	k2eAgMnSc7d1	policejní
šéfem	šéf	k1gMnSc7	šéf
Currym	Currym	k1gInSc4	Currym
<g/>
,	,	kIx,	,
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
limuzína	limuzína	k1gFnSc1	limuzína
s	s	k7c7	s
těžce	těžce	k6eAd1	těžce
raněným	raněný	k2eAgInSc7d1	raněný
Kennedym	Kennedymum	k1gNnPc2	Kennedymum
(	(	kIx(	(
<g/>
a	a	k8xC	a
guvernérem	guvernér	k1gMnSc7	guvernér
Connallym	Connallymum	k1gNnPc2	Connallymum
<g/>
)	)	kIx)	)
a	a	k8xC	a
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
vůz	vůz	k1gInSc1	vůz
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
personálu	personál	k1gInSc2	personál
zde	zde	k6eAd1	zde
však	však	k9	však
nečekal	čekat	k5eNaImAgMnS	čekat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
nemocnice	nemocnice	k1gFnSc1	nemocnice
požádána	požádat	k5eAaPmNgFnS	požádat
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
střelbě	střelba	k1gFnSc6	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
onu	onen	k3xDgFnSc4	onen
skutečnost	skutečnost	k1gFnSc4	skutečnost
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
agent	agent	k1gMnSc1	agent
Kellerman	Kellerman	k1gMnSc1	Kellerman
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zastavení	zastavení	k1gNnSc6	zastavení
doslova	doslova	k6eAd1	doslova
vyskočil	vyskočit	k5eAaPmAgInS	vyskočit
z	z	k7c2	z
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
limuzíny	limuzína	k1gFnSc2	limuzína
a	a	k8xC	a
zakřičel	zakřičet	k5eAaPmAgMnS	zakřičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ať	ať	k8xS	ať
nám	my	k3xPp1nPc3	my
někdo	někdo	k3yInSc1	někdo
okamžitě	okamžitě	k6eAd1	okamžitě
sežene	sehnat	k5eAaPmIp3nS	sehnat
dva	dva	k4xCgInPc4	dva
sanitární	sanitární	k2eAgInPc4d1	sanitární
vozíky	vozík	k1gInPc4	vozík
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Agent	agent	k1gMnSc1	agent
Win	Win	k1gMnSc1	Win
Lawson	Lawson	k1gMnSc1	Lawson
z	z	k7c2	z
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
vozu	vůz	k1gInSc2	vůz
vběhl	vběhnout	k5eAaPmAgMnS	vběhnout
do	do	k7c2	do
vstupní	vstupní	k2eAgFnSc2d1	vstupní
haly	hala	k1gFnSc2	hala
<g/>
,	,	kIx,	,
popadl	popadnout	k5eAaPmAgMnS	popadnout
dva	dva	k4xCgInPc4	dva
vozíky	vozík	k1gInPc4	vozík
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
vytlačit	vytlačit	k5eAaPmF	vytlačit
před	před	k7c4	před
vchod	vchod	k1gInSc4	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jej	on	k3xPp3gNnSc2	on
zpozorovalo	zpozorovat	k5eAaPmAgNnS	zpozorovat
několik	několik	k4yIc1	několik
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mu	on	k3xPp3gNnSc3	on
nakonec	nakonec	k6eAd1	nakonec
pomohli	pomoct	k5eAaPmAgMnP	pomoct
vyvézt	vyvézt	k5eAaPmF	vyvézt
vozíky	vozík	k1gInPc4	vozík
ven	ven	k6eAd1	ven
k	k	k7c3	k
limuzíně	limuzína	k1gFnSc3	limuzína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
stále	stále	k6eAd1	stále
seděla	sedět	k5eAaImAgFnS	sedět
šokovaná	šokovaný	k2eAgFnSc1d1	šokovaná
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Kennedyová	Kennedyová	k1gFnSc1	Kennedyová
svírajíc	svírat	k5eAaImSgFnS	svírat
smrtelně	smrtelně	k6eAd1	smrtelně
raněného	raněný	k2eAgMnSc4d1	raněný
manžela	manžel	k1gMnSc4	manžel
v	v	k7c6	v
klíně	klín	k1gInSc6	klín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sedadle	sedadlo	k1gNnSc6	sedadlo
vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
ležela	ležet	k5eAaImAgFnS	ležet
část	část	k1gFnSc1	část
Kennedyho	Kennedy	k1gMnSc2	Kennedy
lebky	lebka	k1gFnSc2	lebka
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
vlasy	vlas	k1gInPc7	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
Emory	Emora	k1gFnSc2	Emora
Roberts	Roberts	k1gInSc1	Roberts
musel	muset	k5eAaImAgInS	muset
paní	paní	k1gFnSc3	paní
Kennedyovou	Kennedyová	k1gFnSc4	Kennedyová
požádat	požádat	k5eAaPmF	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přestala	přestat	k5eAaPmAgFnS	přestat
tisknout	tisknout	k5eAaImF	tisknout
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jej	on	k3xPp3gMnSc4	on
hodlal	hodlat	k5eAaImAgMnS	hodlat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
agentů	agent	k1gMnPc2	agent
Hilla	Hillo	k1gNnSc2	Hillo
a	a	k8xC	a
Kellermana	Kellerman	k1gMnSc4	Kellerman
naložit	naložit	k5eAaPmF	naložit
na	na	k7c4	na
již	již	k6eAd1	již
přistavený	přistavený	k2eAgInSc4d1	přistavený
vozík	vozík	k1gInSc4	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jej	on	k3xPp3gNnSc4	on
poté	poté	k6eAd1	poté
neprodleně	prodleně	k6eNd1	prodleně
zavezli	zavézt	k5eAaPmAgMnP	zavézt
na	na	k7c4	na
operační	operační	k2eAgInSc4d1	operační
sál	sál	k1gInSc4	sál
Trauma	trauma	k1gNnSc4	trauma
Room	Room	k1gInSc1	Room
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Personál	personál	k1gInSc1	personál
traumatologického	traumatologický	k2eAgNnSc2d1	traumatologické
oddělení	oddělení	k1gNnSc2	oddělení
(	(	kIx(	(
<g/>
Trauma	trauma	k1gNnSc4	trauma
Room	Rooma	k1gFnPc2	Rooma
1	[number]	k4	1
<g/>
)	)	kIx)	)
ovšem	ovšem	k9	ovšem
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Kennedyho	Kennedy	k1gMnSc2	Kennedy
stav	stav	k1gInSc1	stav
neslučitelný	slučitelný	k2eNgInSc1d1	neslučitelný
se	s	k7c7	s
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
v	v	k7c6	v
případě	případ	k1gInSc6	případ
okamžitého	okamžitý	k2eAgInSc2d1	okamžitý
zásahu	zásah	k1gInSc2	zásah
lékaře	lékař	k1gMnSc2	lékař
hned	hned	k6eAd1	hned
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
atentátu	atentát	k1gInSc2	atentát
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
naději	naděje	k1gFnSc4	naděje
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
měl	mít	k5eAaImAgMnS	mít
nehmatný	hmatný	k2eNgInSc4d1	nehmatný
puls	puls	k1gInSc4	puls
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
nulový	nulový	k2eAgInSc4d1	nulový
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
poslechem	poslech	k1gInSc7	poslech
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
velice	velice	k6eAd1	velice
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
tlukot	tlukot	k1gInSc1	tlukot
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
resuscitaci	resuscitace	k1gFnSc4	resuscitace
vešel	vejít	k5eAaPmAgInS	vejít
na	na	k7c4	na
sál	sál	k1gInSc4	sál
mladý	mladý	k2eAgMnSc1d1	mladý
neurochirurg	neurochirurg	k1gMnSc1	neurochirurg
Kemp	kemp	k1gInSc1	kemp
Clark	Clark	k1gInSc4	Clark
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
přítomným	přítomný	k2eAgMnSc7d1	přítomný
doktorem	doktor	k1gMnSc7	doktor
Baxterem	Baxter	k1gMnSc7	Baxter
okamžitě	okamžitě	k6eAd1	okamžitě
dotázán	dotázán	k2eAgMnSc1d1	dotázán
na	na	k7c4	na
stav	stav	k1gInSc4	stav
prezidenta	prezident	k1gMnSc2	prezident
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kempe	kemp	k1gInSc5	kemp
<g/>
,	,	kIx,	,
řekni	říct	k5eAaPmRp2nS	říct
nám	my	k3xPp1nPc3	my
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zlé	zlý	k2eAgNnSc1d1	zlé
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zranění	zranění	k1gNnSc1	zranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
ztrácíme	ztrácet	k5eAaImIp1nP	ztrácet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Clark	Clark	k1gInSc1	Clark
tedy	tedy	k9	tedy
odhrnul	odhrnout	k5eAaPmAgInS	odhrnout
část	část	k1gFnSc4	část
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
provedl	provést	k5eAaPmAgInS	provést
vyšetření	vyšetření	k1gNnSc4	vyšetření
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bože	bůh	k1gMnSc5	bůh
můj	můj	k1gMnSc5	můj
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
má	mít	k5eAaImIp3nS	mít
odstřelenou	odstřelený	k2eAgFnSc4d1	odstřelená
celou	celý	k2eAgFnSc4d1	celá
pravou	pravý	k2eAgFnSc4d1	pravá
polovinu	polovina	k1gFnSc4	polovina
hlavy	hlava	k1gFnSc2	hlava
<g/>
...	...	k?	...
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
nemůžeme	moct	k5eNaImIp1nP	moct
nic	nic	k3yNnSc4	nic
udělat	udělat	k5eAaPmF	udělat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
přítomných	přítomný	k2eAgMnPc2d1	přítomný
doktorů	doktor	k1gMnPc2	doktor
ještě	ještě	k9	ještě
provedl	provést	k5eAaPmAgMnS	provést
otevření	otevření	k1gNnSc2	otevření
hrudníku	hrudník	k1gInSc2	hrudník
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
přímou	přímý	k2eAgFnSc4d1	přímá
masáž	masáž	k1gFnSc4	masáž
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Baxter	Baxter	k1gMnSc1	Baxter
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
když	když	k8xS	když
víme	vědět	k5eAaImIp1nP	vědět
v	v	k7c6	v
jakém	jaký	k3yQgInSc6	jaký
stavu	stav	k1gInSc6	stav
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nemáme	mít	k5eNaImIp1nP	mít
důvod	důvod	k1gInSc4	důvod
nadále	nadále	k6eAd1	nadále
resuscitovat	resuscitovat	k5eAaImF	resuscitovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
George	Georg	k1gMnPc4	Georg
Gregory	Gregor	k1gMnPc4	Gregor
Burkley	Burklea	k1gFnSc2	Burklea
<g/>
,	,	kIx,	,
prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
bylo	být	k5eAaImAgNnS	být
devastující	devastující	k2eAgNnSc1d1	devastující
zranění	zranění	k1gNnSc1	zranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
také	také	k9	také
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
úmrtním	úmrtní	k2eAgInSc6d1	úmrtní
listu	list	k1gInSc6	list
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
hodin	hodina	k1gFnPc2	hodina
odpoledne	odpoledne	k6eAd1	odpoledne
(	(	kIx(	(
<g/>
19.00	[number]	k4	19.00
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
konstatována	konstatován	k2eAgFnSc1d1	konstatována
úplná	úplný	k2eAgFnSc1d1	úplná
zástava	zástava	k1gFnSc1	zástava
srdeční	srdeční	k2eAgFnSc2d1	srdeční
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
Oscar	Oscar	k1gMnSc1	Oscar
Hubert	Hubert	k1gMnSc1	Hubert
udělil	udělit	k5eAaPmAgMnS	udělit
Kennedymu	Kennedym	k1gInSc3	Kennedym
poslední	poslední	k2eAgNnSc1d1	poslední
pomazání	pomazání	k1gNnSc1	pomazání
a	a	k8xC	a
pacient	pacient	k1gMnSc1	pacient
#	#	kIx~	#
<g/>
24740	[number]	k4	24740
<g/>
,	,	kIx,	,
Kennedy	Kenned	k1gMnPc4	Kenned
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
F.	F.	kA	F.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zasahujících	zasahující	k2eAgMnPc2d1	zasahující
doktorů	doktor	k1gMnPc2	doktor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
jsme	být	k5eAaImIp1nP	být
neměli	mít	k5eNaImAgMnP	mít
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kněz	kněz	k1gMnSc1	kněz
Hubert	Hubert	k1gMnSc1	Hubert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
udělil	udělit	k5eAaPmAgMnS	udělit
poslední	poslední	k2eAgNnSc4d1	poslední
pomazání	pomazání	k1gNnSc4	pomazání
<g/>
,	,	kIx,	,
sdělil	sdělit	k5eAaPmAgMnS	sdělit
deníku	deník	k1gInSc2	deník
The	The	k1gMnSc2	The
New	New	k1gMnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gInSc4	Times
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
de	de	k?	de
facto	facto	k1gNnSc4	facto
mrtev	mrtev	k2eAgMnSc1d1	mrtev
již	již	k6eAd1	již
při	při	k7c6	při
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
a	a	k8xC	a
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
musel	muset	k5eAaImAgMnS	muset
sundat	sundat	k5eAaPmF	sundat
z	z	k7c2	z
tváře	tvář	k1gFnSc2	tvář
pokrývku	pokrývka	k1gFnSc4	pokrývka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vykonat	vykonat	k5eAaPmF	vykonat
obřad	obřad	k1gInSc4	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
prohlášení	prohlášení	k1gNnSc1	prohlášení
o	o	k7c4	o
Kennedyho	Kennedy	k1gMnSc4	Kennedy
smrti	smrt	k1gFnSc2	smrt
vydal	vydat	k5eAaPmAgMnS	vydat
zastupující	zastupující	k2eAgMnSc1d1	zastupující
tiskový	tiskový	k2eAgMnSc1d1	tiskový
mluvčí	mluvčí	k1gMnSc1	mluvčí
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
Malcolme	Malcolme	k1gMnSc1	Malcolme
Kilduff	Kilduff	k1gMnSc1	Kilduff
ve	v	k7c6	v
13.33	[number]	k4	13.33
odpoledne	odpoledne	k1gNnSc2	odpoledne
(	(	kIx(	(
<g/>
19.33	[number]	k4	19.33
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
minut	minuta	k1gFnPc2	minuta
po	po	k7c4	po
14.00	[number]	k4	14.00
(	(	kIx(	(
<g/>
20.00	[number]	k4	20.00
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
dallaské	dallaský	k2eAgFnSc2d1	dallaská
policie	policie	k1gFnSc2	policie
s	s	k7c7	s
agenty	agent	k1gMnPc7	agent
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Kennedyho	Kennedy	k1gMnSc4	Kennedy
tělo	tělo	k1gNnSc1	tělo
umístěno	umístit	k5eAaPmNgNnS	umístit
do	do	k7c2	do
rakve	rakev	k1gFnSc2	rakev
a	a	k8xC	a
převezeno	převézt	k5eAaPmNgNnS	převézt
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
One	One	k1gFnSc2	One
<g/>
.	.	kIx.	.
</s>
<s>
Rakev	rakev	k1gFnSc1	rakev
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
letadla	letadlo	k1gNnSc2	letadlo
naložena	naložen	k2eAgFnSc1d1	naložena
zadními	zadní	k2eAgFnPc7d1	zadní
dveřmi	dveře	k1gFnPc7	dveře
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
do	do	k7c2	do
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
oddělení	oddělení	k1gNnSc2	oddělení
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
uvolněno	uvolnit	k5eAaPmNgNnS	uvolnit
odmontováním	odmontování	k1gNnSc7	odmontování
několika	několik	k4yIc2	několik
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Viceprezident	viceprezident	k1gMnSc1	viceprezident
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jel	jet	k5eAaImAgMnS	jet
v	v	k7c6	v
koloně	kolona	k1gFnSc6	kolona
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
autě	auto	k1gNnSc6	auto
za	za	k7c7	za
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
limuzínou	limuzína	k1gFnSc7	limuzína
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
Kennedyho	Kennedy	k1gMnSc2	Kennedy
smrti	smrt	k1gFnSc6	smrt
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
;	;	kIx,	;
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
přísahu	přísaha	k1gFnSc4	přísaha
složil	složit	k5eAaPmAgMnS	složit
ve	v	k7c6	v
14.38	[number]	k4	14.38
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
One	One	k1gFnSc2	One
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
národní	národní	k2eAgInSc1d1	národní
archiv	archiv	k1gInSc1	archiv
(	(	kIx(	(
<g/>
zkr.	zkr.	kA	zkr.
NARA	NARA	kA	NARA
<g/>
)	)	kIx)	)
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
magnetofonové	magnetofonový	k2eAgFnSc2d1	magnetofonová
pásky	páska	k1gFnSc2	páska
obsahující	obsahující	k2eAgFnSc2d1	obsahující
odchozí	odchozí	k2eAgFnSc2d1	odchozí
a	a	k8xC	a
příchozí	příchozí	k1gFnSc2	příchozí
radiovou	radiový	k2eAgFnSc4d1	radiová
komunikaci	komunikace	k1gFnSc4	komunikace
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
letounu	letoun	k1gInSc2	letoun
Air	Air	k1gFnSc2	Air
Force	force	k1gFnPc2	force
One	One	k1gMnSc1	One
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
směřoval	směřovat	k5eAaImAgInS	směřovat
z	z	k7c2	z
Dallasu	Dallas	k1gInSc2	Dallas
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Andrewsovu	Andrewsův	k2eAgFnSc4d1	Andrewsova
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
u	u	k7c2	u
Washingtonu	Washington	k1gInSc2	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gMnSc1	D.C.
<g/>
,	,	kIx,	,
převážeje	převážet	k5eAaImSgMnS	převážet
ostatky	ostatek	k1gInPc1	ostatek
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pásky	pásek	k1gInPc1	pásek
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
desítky	desítka	k1gFnPc1	desítka
minut	minuta	k1gFnPc2	minuta
hlasového	hlasový	k2eAgInSc2d1	hlasový
záznamu	záznam	k1gInSc2	záznam
<g/>
,	,	kIx,	,
daroval	darovat	k5eAaPmAgMnS	darovat
archivu	archiv	k1gInSc2	archiv
soukromý	soukromý	k2eAgMnSc1d1	soukromý
sběratel	sběratel	k1gMnSc1	sběratel
Nathan	Nathan	k1gMnSc1	Nathan
Raab	Raab	k1gMnSc1	Raab
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
jednoho	jeden	k4xCgNnSc2	jeden
z	z	k7c2	z
Kennedyho	Kennedy	k1gMnSc2	Kennedy
osobních	osobní	k2eAgMnPc2d1	osobní
poradců	poradce	k1gMnPc2	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
záznamu	záznam	k1gInSc2	záznam
tvoří	tvořit	k5eAaImIp3nS	tvořit
rutinní	rutinní	k2eAgFnSc1d1	rutinní
komunikace	komunikace	k1gFnSc1	komunikace
mezi	mezi	k7c7	mezi
piloty	pilot	k1gMnPc7	pilot
letounu	letoun	k1gInSc2	letoun
AFO	AFO	kA	AFO
a	a	k8xC	a
řízením	řízení	k1gNnSc7	řízení
letového	letový	k2eAgInSc2d1	letový
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
například	například	k6eAd1	například
i	i	k9	i
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dále	daleko	k6eAd2	daleko
manipulovat	manipulovat	k5eAaImF	manipulovat
s	s	k7c7	s
prezidentovými	prezidentův	k2eAgInPc7d1	prezidentův
ostatky	ostatek	k1gInPc7	ostatek
nebo	nebo	k8xC	nebo
také	také	k9	také
telefonickou	telefonický	k2eAgFnSc4d1	telefonická
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
Lyndonem	Lyndon	k1gInSc7	Lyndon
Johnsonem	Johnson	k1gInSc7	Johnson
a	a	k8xC	a
Rose	Rose	k1gMnSc1	Rose
Kennedyovou	Kennedyová	k1gFnSc7	Kennedyová
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
zesnulého	zesnulý	k2eAgInSc2d1	zesnulý
JFK	JFK	kA	JFK
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
Rose	Rose	k1gMnSc1	Rose
Kennedyové	Kennedyová	k1gFnSc2	Kennedyová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
utěšit	utěšit	k5eAaPmF	utěšit
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tak	tak	k9	tak
bych	by	kYmCp1nS	by
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
bože	bože	k0	bože
<g/>
,	,	kIx,	,
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bych	by	kYmCp1nS	by
mohl	moct	k5eAaImAgMnS	moct
<g/>
...	...	k?	...
Chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
vám	vy	k3xPp2nPc3	vy
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
truchlíme	truchlet	k5eAaImIp1nP	truchlet
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Velmi	velmi	k6eAd1	velmi
vám	vy	k3xPp2nPc3	vy
děkuji	děkovat	k5eAaImIp1nS	děkovat
<g/>
.	.	kIx.	.
</s>
<s>
Děkuji	děkovat	k5eAaImIp1nS	děkovat
vám	vy	k3xPp2nPc3	vy
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
Jacka	Jacek	k1gMnSc4	Jacek
miloval	milovat	k5eAaImAgMnS	milovat
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
miloval	milovat	k5eAaImAgMnS	milovat
vás	vy	k3xPp2nPc4	vy
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
Air	Air	k1gFnSc1	Air
Force	force	k1gFnSc1	force
One	One	k1gFnSc1	One
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
Andrewsově	Andrewsův	k2eAgFnSc6d1	Andrewsova
základně	základna	k1gFnSc6	základna
nedaleko	nedaleko	k7c2	nedaleko
Washingtonu	Washington	k1gInSc2	Washington
D.C.	D.C.	k1gFnSc2	D.C.
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
tělo	tělo	k1gNnSc1	tělo
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
převezeno	převézt	k5eAaPmNgNnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
Bethesda	Bethesdo	k1gNnSc2	Bethesdo
Naval	navalit	k5eAaPmRp2nS	navalit
Hospital	Hospital	k1gMnSc1	Hospital
k	k	k7c3	k
okamžité	okamžitý	k2eAgFnSc3d1	okamžitá
pitvě	pitva	k1gFnSc3	pitva
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
trvala	trvat	k5eAaImAgFnS	trvat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
20.00	[number]	k4	20.00
do	do	k7c2	do
23.00	[number]	k4	23.00
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
cca	cca	kA	cca
23.00	[number]	k4	23.00
až	až	k9	až
0	[number]	k4	0
<g/>
4.00	[number]	k4	4.00
tělo	tělo	k1gNnSc4	tělo
nabalzamováno	nabalzamován	k2eAgNnSc4d1	nabalzamován
a	a	k8xC	a
kosmeticky	kosmeticky	k6eAd1	kosmeticky
upraveno	upravit	k5eAaPmNgNnS	upravit
na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Pitva	pitva	k1gFnSc1	pitva
prezidentova	prezidentův	k2eAgNnSc2d1	prezidentovo
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
provedená	provedený	k2eAgFnSc1d1	provedená
třemi	tři	k4xCgMnPc7	tři
patology	patolog	k1gMnPc7	patolog
<g/>
,	,	kIx,	,
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zranění	zranění	k1gNnSc1	zranění
prezidentovy	prezidentův	k2eAgFnSc2d1	prezidentova
hlavy	hlava	k1gFnSc2	hlava
bylo	být	k5eAaImAgNnS	být
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
a	a	k8xC	a
kulka	kulka	k1gFnSc1	kulka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
způsobila	způsobit	k5eAaPmAgFnS	způsobit
odtržením	odtržení	k1gNnSc7	odtržení
pravé	pravý	k2eAgFnSc2d1	pravá
části	část	k1gFnSc2	část
Kennedyho	Kennedy	k1gMnSc2	Kennedy
hlavy	hlava	k1gFnSc2	hlava
naprosto	naprosto	k6eAd1	naprosto
devastující	devastující	k2eAgNnSc4d1	devastující
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nebylo	být	k5eNaImAgNnS	být
slučitelné	slučitelný	k2eAgNnSc1d1	slučitelné
se	s	k7c7	s
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zranění	zranění	k1gNnSc1	zranění
na	na	k7c6	na
krku	krk	k1gInSc6	krk
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
pitevní	pitevní	k2eAgFnSc3d1	pitevní
zprávě	zpráva	k1gFnSc3	zpráva
přisouzeno	přisoudit	k5eAaPmNgNnS	přisoudit
druhé	druhý	k4xOgFnSc3	druhý
střele	střela	k1gFnSc3	střela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
"	"	kIx"	"
<g/>
zasáhla	zasáhnout	k5eAaPmAgNnP	zasáhnout
prezidenta	prezident	k1gMnSc2	prezident
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
prošla	projít	k5eAaPmAgFnS	projít
svaly	sval	k1gInPc4	sval
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
poškodila	poškodit	k5eAaPmAgFnS	poškodit
vrchní	vrchní	k2eAgInSc4d1	vrchní
cíp	cíp	k1gInSc4	cíp
pravé	pravý	k2eAgFnSc2d1	pravá
plíce	plíce	k1gFnSc2	plíce
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
jí	on	k3xPp3gFnSc7	on
prošla	projít	k5eAaPmAgFnS	projít
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vyletěla	vyletět	k5eAaPmAgFnS	vyletět
přední	přední	k2eAgFnSc7d1	přední
částí	část	k1gFnSc7	část
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
pseudotracheotomické	pseudotracheotomický	k2eAgFnSc2d1	pseudotracheotomický
jizvy	jizva	k1gFnSc2	jizva
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zjištění	zjištění	k1gNnSc1	zjištění
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
prezidentovým	prezidentův	k2eAgMnSc7d1	prezidentův
osobním	osobní	k2eAgMnSc7d1	osobní
lékařem	lékař	k1gMnSc7	lékař
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Burkleym	Burkleym	k1gInSc1	Burkleym
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
do	do	k7c2	do
úmrtního	úmrtní	k2eAgInSc2d1	úmrtní
listu	list	k1gInSc2	list
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
kulka	kulka	k1gFnSc1	kulka
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
3	[number]	k4	3
<g/>
.	.	kIx.	.
hrudního	hrudní	k2eAgInSc2d1	hrudní
obratle	obratel	k1gInSc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Kennedyho	Kennedy	k1gMnSc4	Kennedy
košili	košile	k1gFnSc4	košile
a	a	k8xC	a
v	v	k7c6	v
kabátě	kabát	k1gInSc6	kabát
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
12,5	[number]	k4	12,5
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
cm	cm	kA	cm
pod	pod	k7c7	pod
límcem	límec	k1gInSc7	límec
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
díry	díra	k1gFnPc1	díra
po	po	k7c6	po
kulkách	kulka	k1gFnPc6	kulka
<g/>
.	.	kIx.	.
</s>
<s>
Fotografická	fotografický	k2eAgFnSc1d1	fotografická
analýza	analýza	k1gFnSc1	analýza
záznamu	záznam	k1gInSc2	záznam
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kolony	kolona	k1gFnSc2	kolona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
i	i	k9	i
nově	nově	k6eAd1	nově
zveřejněný	zveřejněný	k2eAgMnSc1d1	zveřejněný
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
film	film	k1gInSc4	film
zachycující	zachycující	k2eAgFnSc4d1	zachycující
dobu	doba	k1gFnSc4	doba
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
<g/>
,	,	kIx,	,
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezidentův	prezidentův	k2eAgInSc1d1	prezidentův
kabát	kabát	k1gInSc1	kabát
byl	být	k5eAaImAgInS	být
zmačkán	zmačkat	k5eAaPmNgInS	zmačkat
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
krku	krk	k1gInSc2	krk
a	a	k8xC	a
nevisel	viset	k5eNaImAgInS	viset
tedy	tedy	k9	tedy
volně	volně	k6eAd1	volně
podél	podél	k7c2	podél
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tehdejší	tehdejší	k2eAgInPc1d1	tehdejší
průzkumy	průzkum	k1gInPc1	průzkum
oblečení	oblečení	k1gNnSc2	oblečení
byly	být	k5eAaImAgInP	být
označeny	označen	k2eAgInPc1d1	označen
za	za	k7c4	za
nevěrohodné	věrohodný	k2eNgNnSc4d1	nevěrohodné
co	co	k9	co
se	se	k3xPyFc4	se
objasnění	objasnění	k1gNnSc1	objasnění
přesného	přesný	k2eAgNnSc2d1	přesné
místa	místo	k1gNnSc2	místo
zranění	zranění	k1gNnSc2	zranění
prezidentových	prezidentův	k2eAgNnPc2d1	prezidentovo
zad	záda	k1gNnPc2	záda
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Diagram	diagram	k1gInSc1	diagram
z	z	k7c2	z
pitevní	pitevní	k2eAgFnSc2d1	pitevní
zprávy	zpráva	k1gFnSc2	zpráva
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Thorntona	Thornton	k1gMnSc2	Thornton
Boswella	Boswell	k1gMnSc2	Boswell
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
důkaz	důkaz	k1gInSc1	důkaz
zranění	zranění	k1gNnPc2	zranění
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
části	část	k1gFnSc6	část
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
ovšem	ovšem	k9	ovšem
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Boswell	Boswell	k1gMnSc1	Boswell
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
diagram	diagram	k1gInSc1	diagram
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
zamýšlen	zamýšlen	k2eAgInSc1d1	zamýšlen
jako	jako	k8xS	jako
zcela	zcela	k6eAd1	zcela
přesné	přesný	k2eAgNnSc4d1	přesné
měřítko	měřítko	k1gNnSc4	měřítko
a	a	k8xC	a
pro	pro	k7c4	pro
deník	deník	k1gInSc4	deník
The	The	k1gFnSc1	The
Baltimore	Baltimore	k1gInSc1	Baltimore
Sun	Sun	kA	Sun
ze	z	k7c2	z
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
jej	on	k3xPp3gMnSc4	on
překreslil	překreslit	k5eAaPmAgInS	překreslit
a	a	k8xC	a
zakřížkoval	zakřížkovat	k5eAaPmAgInS	zakřížkovat
bod	bod	k1gInSc1	bod
výše	výše	k1gFnSc2	výše
na	na	k7c6	na
diagramu	diagram	k1gInSc6	diagram
<g/>
.	.	kIx.	.
</s>
<s>
Boswell	Boswell	k1gMnSc1	Boswell
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnPc1	jeho
měření	měření	k1gNnPc1	měření
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
lokalizovala	lokalizovat	k5eAaBmAgFnS	lokalizovat
zranění	zranění	k1gNnSc4	zranění
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
14	[number]	k4	14
cm	cm	kA	cm
od	od	k7c2	od
krku	krk	k1gInSc2	krk
a	a	k8xC	a
ucha	ucho	k1gNnSc2	ucho
<g/>
,	,	kIx,	,
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
nálezem	nález	k1gInSc7	nález
tvrdícím	tvrdící	k2eAgInSc7d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
poškozen	poškodit	k5eAaPmNgInS	poškodit
3	[number]	k4	3
<g/>
.	.	kIx.	.
hrudní	hrudní	k2eAgInSc4d1	hrudní
obratel	obratel	k1gInSc4	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
členové	člen	k1gMnPc1	člen
pitevního	pitevní	k2eAgInSc2d1	pitevní
týmu	tým	k1gInSc2	tým
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
Bethesda	Bethesdo	k1gNnSc2	Bethesdo
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
fotografie	fotografie	k1gFnSc1	fotografie
z	z	k7c2	z
pitvy	pitva	k1gFnSc2	pitva
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
zranění	zranění	k1gNnSc4	zranění
po	po	k7c6	po
vniku	vnik	k1gInSc6	vnik
kulky	kulka	k1gFnSc2	kulka
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
6	[number]	k4	6
<g/>
.	.	kIx.	.
krčního	krční	k2eAgInSc2d1	krční
obratle	obratel	k1gInSc2	obratel
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
spodním	spodní	k2eAgInSc6d1	spodní
okraji	okraj	k1gInSc6	okraj
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
úrovní	úroveň	k1gFnSc7	úroveň
určenou	určený	k2eAgFnSc7d1	určená
během	během	k7c2	během
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
HSCA	HSCA	kA	HSCA
na	na	k7c6	na
základě	základ	k1gInSc6	základ
fotografické	fotografický	k2eAgFnSc2d1	fotografická
a	a	k8xC	a
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
dokumentace	dokumentace	k1gFnSc2	dokumentace
z	z	k7c2	z
pitvy	pitva	k1gFnSc2	pitva
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
pitva	pitva	k1gFnSc1	pitva
kritizována	kritizován	k2eAgFnSc1d1	kritizována
v	v	k7c6	v
několika	několik	k4yIc6	několik
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
spáleny	spálen	k2eAgInPc1d1	spálen
originální	originální	k2eAgInPc1d1	originální
náčrty	náčrt	k1gInPc1	náčrt
a	a	k8xC	a
poznámky	poznámka	k1gFnPc1	poznámka
Jamese	Jamese	k1gFnSc2	Jamese
Humese	Humese	k1gFnSc2	Humese
z	z	k7c2	z
pitvy	pitva	k1gFnSc2	pitva
a	a	k8xC	a
také	také	k9	také
neschopnost	neschopnost	k1gFnSc1	neschopnost
zajistit	zajistit	k5eAaPmF	zajistit
řádným	řádný	k2eAgInSc7d1	řádný
způsobem	způsob	k1gInSc7	způsob
veškeré	veškerý	k3xTgNnSc4	veškerý
nálezy	nález	k1gInPc1	nález
a	a	k8xC	a
důkazy	důkaz	k1gInPc1	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
pitevního	pitevní	k2eAgInSc2d1	pitevní
nálezu	nález	k1gInSc2	nález
později	pozdě	k6eAd2	pozdě
doplnil	doplnit	k5eAaPmAgMnS	doplnit
Robert	Robert	k1gMnSc1	Robert
B.	B.	kA	B.
Livingston	Livingston	k1gInSc1	Livingston
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
světových	světový	k2eAgMnPc2d1	světový
specialistů	specialista	k1gMnPc2	specialista
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
lidského	lidský	k2eAgInSc2d1	lidský
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Assassination	Assassination	k1gInSc4	Assassination
Science	Scienec	k1gMnSc2	Scienec
<g/>
:	:	kIx,	:
Experts	Experts	k1gInSc1	Experts
Speak	Speak	k1gMnSc1	Speak
Out	Out	k1gMnSc1	Out
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Death	Death	k1gInSc1	Death
of	of	k?	of
JFK	JFK	kA	JFK
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Vražedná	vražedný	k2eAgFnSc1d1	vražedná
věda	věda	k1gFnSc1	věda
<g/>
:	:	kIx,	:
Experti	expert	k1gMnPc1	expert
se	se	k3xPyFc4	se
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g />
.	.	kIx.	.
</s>
<s>
JFK	JFK	kA	JFK
<g/>
)	)	kIx)	)
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
něco	něco	k3yInSc1	něco
muselo	muset	k5eAaImAgNnS	muset
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
silou	síla	k1gFnSc7	síla
explodovat	explodovat	k5eAaBmF	explodovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
velkého	velký	k2eAgInSc2d1	velký
týlního	týlní	k2eAgInSc2d1	týlní
otvoru	otvor	k1gInSc2	otvor
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odtržení	odtržení	k1gNnSc3	odtržení
mozečku	mozeček	k1gInSc2	mozeček
<g/>
,	,	kIx,	,
částí	část	k1gFnSc7	část
lebeční	lebeční	k2eAgFnSc2d1	lebeční
kosti	kost	k1gFnSc2	kost
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
masívního	masívní	k2eAgInSc2d1	masívní
otvoru	otvor	k1gInSc2	otvor
v	v	k7c6	v
tvrdé	tvrdý	k2eAgFnSc6d1	tvrdá
pleně	plena	k1gFnSc6	plena
mozkové	mozkový	k2eAgFnSc2d1	mozková
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
mozeček	mozeček	k1gInSc4	mozeček
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc4d1	hlavní
dutinu	dutina	k1gFnSc4	dutina
lebeční	lebeční	k2eAgFnSc4d1	lebeční
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Charakter	charakter	k1gInSc1	charakter
zranění	zranění	k1gNnSc1	zranění
podporuje	podporovat	k5eAaImIp3nS	podporovat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
fatální	fatální	k2eAgNnSc4d1	fatální
zranění	zranění	k1gNnSc4	zranění
Kennedyho	Kennedy	k1gMnSc2	Kennedy
hlavy	hlava	k1gFnSc2	hlava
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tříštivou	tříštivý	k2eAgFnSc7d1	tříštivá
střelou	střela	k1gFnSc7	střela
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
dum-dum	dumum	k1gInSc1	dum-dum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vypálenou	vypálený	k2eAgFnSc4d1	vypálená
zepředu	zepředu	k6eAd1	zepředu
od	od	k7c2	od
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
užití	užití	k1gNnSc2	užití
střely	střela	k1gFnSc2	střela
dum-dum	dumum	k1gInSc1	dum-dum
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
i	i	k9	i
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
Spartanburg	Spartanburg	k1gMnSc1	Spartanburg
Herald-Journal	Herald-Journal	k1gMnSc1	Herald-Journal
ze	z	k7c2	z
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
hovoří	hovořit	k5eAaImIp3nS	hovořit
patolog	patolog	k1gMnSc1	patolog
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
James	James	k1gMnSc1	James
Beyer	Beyer	k1gMnSc1	Beyer
z	z	k7c2	z
Arlingtonské	Arlingtonský	k2eAgFnSc2d1	Arlingtonský
nemocnice	nemocnice	k1gFnSc2	nemocnice
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
minimálně	minimálně	k6eAd1	minimálně
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
několika	několik	k4yIc3	několik
zranění	zranění	k1gNnPc2	zranění
prezidenta	prezident	k1gMnSc2	prezident
by	by	kYmCp3nP	by
tomuto	tento	k3xDgNnSc3	tento
podezření	podezření	k1gNnSc3	podezření
mohlo	moct	k5eAaImAgNnS	moct
odpovídat	odpovídat	k5eAaImF	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
pitvě	pitva	k1gFnSc6	pitva
převezeno	převézt	k5eAaPmNgNnS	převézt
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
místností	místnost	k1gFnPc2	místnost
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
rakvi	rakev	k1gFnSc6	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Privátně	privátně	k6eAd1	privátně
je	být	k5eAaImIp3nS	být
však	však	k9	však
mohla	moct	k5eAaImAgFnS	moct
vidět	vidět	k5eAaImF	vidět
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
následující	následující	k2eAgFnSc6d1	následující
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
byla	být	k5eAaImAgFnS	být
rakev	rakev	k1gFnSc1	rakev
přikrytá	přikrytý	k2eAgFnSc1d1	přikrytá
americkou	americký	k2eAgFnSc7d1	americká
vlajkou	vlajka	k1gFnSc7	vlajka
převezena	převézt	k5eAaPmNgFnS	převézt
do	do	k7c2	do
Kapitolu	Kapitol	k1gInSc2	Kapitol
<g/>
.	.	kIx.	.
</s>
<s>
Tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
stály	stát	k5eAaImAgFnP	stát
takřka	takřka	k6eAd1	takřka
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
fronty	fronta	k1gFnPc1	fronta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
rakev	rakev	k1gFnSc4	rakev
hlídanou	hlídaný	k2eAgFnSc7d1	hlídaná
čestnou	čestný	k2eAgFnSc7d1	čestná
stráží	stráž	k1gFnSc7	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
prezidenta	prezident	k1gMnSc2	prezident
Johna	John	k1gMnSc2	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
na	na	k7c4	na
220	[number]	k4	220
státníků	státník	k1gMnPc2	státník
ze	z	k7c2	z
102	[number]	k4	102
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgMnSc7d1	oficiální
československým	československý	k2eAgMnSc7d1	československý
zástupcem	zástupce	k1gMnSc7	zástupce
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
vyslanec	vyslanec	k1gMnSc1	vyslanec
ČSSR	ČSSR	kA	ČSSR
při	při	k7c6	při
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
za	za	k7c4	za
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
se	se	k3xPyFc4	se
pohřbu	pohřeb	k1gInSc6	pohřeb
oficiálně	oficiálně	k6eAd1	oficiálně
účastnil	účastnit	k5eAaImAgMnS	účastnit
první	první	k4xOgMnSc1	první
náměstek	náměstek	k1gMnSc1	náměstek
předsedy	předseda	k1gMnSc2	předseda
Rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
SSSR	SSSR	kA	SSSR
Anastáz	Anastáza	k1gFnPc2	Anastáza
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Mikojan	Mikojan	k1gMnSc1	Mikojan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smutečním	smuteční	k2eAgInSc6d1	smuteční
obřadu	obřad	k1gInSc6	obřad
byla	být	k5eAaImAgFnS	být
rakev	rakev	k1gFnSc1	rakev
před	před	k7c7	před
zraky	zrak	k1gInPc7	zrak
800	[number]	k4	800
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
převezena	převézt	k5eAaPmNgFnS	převézt
na	na	k7c6	na
zvláštním	zvláštní	k2eAgInSc6d1	zvláštní
voze	vůz	k1gInSc6	vůz
na	na	k7c4	na
Arlingtonský	Arlingtonský	k2eAgInSc4d1	Arlingtonský
národní	národní	k2eAgInSc4d1	národní
hřbitov	hřbitov	k1gInSc4	hřbitov
k	k	k7c3	k
pohřbu	pohřeb	k1gInSc3	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
televizní	televizní	k2eAgFnSc1d1	televizní
ani	ani	k8xC	ani
radiová	radiový	k2eAgFnSc1d1	radiová
stanice	stanice	k1gFnSc1	stanice
nezachytila	zachytit	k5eNaPmAgFnS	zachytit
atentát	atentát	k1gInSc4	atentát
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
udál	udát	k5eAaPmAgInS	udát
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
novinářů	novinář	k1gMnPc2	novinář
ani	ani	k9	ani
nedoprovázela	doprovázet	k5eNaImAgFnS	doprovázet
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kolonu	kolona	k1gFnSc4	kolona
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
čekala	čekat	k5eAaImAgFnS	čekat
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
příjezd	příjezd	k1gInSc4	příjezd
u	u	k7c2	u
Dallas	Dallas	k1gInSc1	Dallas
Trade	Trad	k1gInSc5	Trad
Mart	Marta	k1gFnPc2	Marta
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
novináři	novinář	k1gMnPc1	novinář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
kolonu	kolona	k1gFnSc4	kolona
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
nejzadnější	zadní	k2eAgFnSc6d3	nejzazší
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Dallaská	Dallaský	k2eAgFnSc1d1	Dallaská
policie	policie	k1gFnSc1	policie
zaznamenávala	zaznamenávat	k5eAaImAgFnS	zaznamenávat
svůj	svůj	k3xOyFgInSc4	svůj
radiový	radiový	k2eAgInSc4d1	radiový
provoz	provoz	k1gInSc4	provoz
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
kanálech	kanál	k1gInPc6	kanál
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
komunikační	komunikační	k2eAgInSc1d1	komunikační
kanál	kanál	k1gInSc1	kanál
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
rutinní	rutinní	k2eAgFnSc4d1	rutinní
policejní	policejní	k2eAgFnSc4d1	policejní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
kanál	kanál	k1gInSc1	kanál
byl	být	k5eAaImAgInS	být
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kolony	kolona	k1gFnSc2	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
atentátu	atentát	k1gInSc2	atentát
sestávala	sestávat	k5eAaImAgFnS	sestávat
komunikace	komunikace	k1gFnSc1	komunikace
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
kanálu	kanál	k1gInSc6	kanál
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
hlášení	hlášení	k1gNnSc2	hlášení
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
času	čas	k1gInSc2	čas
12.30	[number]	k4	12.30
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
záznamu	záznam	k1gInSc6	záznam
z	z	k7c2	z
kanálu	kanál	k1gInSc2	kanál
číslo	číslo	k1gNnSc4	číslo
1	[number]	k4	1
slyšet	slyšet	k5eAaImF	slyšet
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
3	[number]	k4	3
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
11	[number]	k4	11
sekund	sekunda	k1gFnPc2	sekunda
nesrozumitelný	srozumitelný	k2eNgInSc1d1	nesrozumitelný
hluk	hluk	k1gInSc1	hluk
a	a	k8xC	a
šum	šum	k1gInSc1	šum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
lze	lze	k6eAd1	lze
slyšet	slyšet	k5eAaImF	slyšet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dallas	Dallas	k1gInSc1	Dallas
1	[number]	k4	1
<g/>
:	:	kIx,	:
Jedeme	jet	k5eAaImIp1nP	jet
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
–	–	k?	–
do	do	k7c2	do
Parkland	Parklanda	k1gFnPc2	Parklanda
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
;	;	kIx,	;
upozorněte	upozornit	k5eAaPmRp2nP	upozornit
personál	personál	k1gInSc4	personál
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připravil	připravit	k5eAaPmAgInS	připravit
<g/>
;	;	kIx,	;
pošlete	poslat	k5eAaPmIp2nP	poslat
někoho	někdo	k3yInSc4	někdo
na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
železniční	železniční	k2eAgInSc4d1	železniční
viadukt	viadukt	k1gInSc4	viadukt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
stalo	stát	k5eAaPmAgNnS	stát
<g/>
;	;	kIx,	;
Dallas	Dallas	k1gInSc1	Dallas
1	[number]	k4	1
<g/>
:	:	kIx,	:
Vypadá	vypadat	k5eAaImIp3nS	vypadat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
střelili	střelit	k5eAaPmAgMnP	střelit
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
upozorněte	upozornit	k5eAaPmRp2nP	upozornit
personál	personál	k1gInSc4	personál
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
;	;	kIx,	;
Dispečer	dispečer	k1gMnSc1	dispečer
<g/>
:	:	kIx,	:
10-4	[number]	k4	10-4
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
upozorněni	upozornit	k5eAaPmNgMnP	upozornit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
sekundy	sekunda	k1gFnPc1	sekunda
života	život	k1gInSc2	život
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
byly	být	k5eAaImAgFnP	být
natočeny	natočen	k2eAgFnPc1d1	natočena
na	na	k7c4	na
němý	němý	k2eAgInSc4d1	němý
8	[number]	k4	8
mm	mm	kA	mm
film	film	k1gInSc1	film
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
26,6	[number]	k4	26,6
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
chvíle	chvíle	k1gFnPc4	chvíle
těsně	těsně	k6eAd1	těsně
před	před	k7c4	před
<g/>
,	,	kIx,	,
během	běh	k1gInSc7	běh
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
pořízen	pořídit	k5eAaPmNgMnS	pořídit
amatérským	amatérský	k2eAgMnSc7d1	amatérský
kameramanem	kameraman	k1gMnSc7	kameraman
Abrahamem	Abraham	k1gMnSc7	Abraham
Zapruderem	Zapruder	k1gMnSc7	Zapruder
a	a	k8xC	a
vešel	vejít	k5eAaPmAgInS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xS	jako
Zapruderův	Zapruderův	k2eAgInSc4d1	Zapruderův
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Zvětšené	zvětšený	k2eAgInPc1d1	zvětšený
výřezy	výřez	k1gInPc1	výřez
ze	z	k7c2	z
Zapruderova	Zapruderův	k2eAgInSc2d1	Zapruderův
filmu	film	k1gInSc2	film
byly	být	k5eAaImAgInP	být
zanedlouho	zanedlouho	k6eAd1	zanedlouho
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Life	Lif	k1gFnSc2	Lif
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
záběry	záběr	k1gInPc1	záběr
opakovaně	opakovaně	k6eAd1	opakovaně
vysílány	vysílat	k5eAaImNgInP	vysílat
televizí	televize	k1gFnSc7	televize
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mnohdy	mnohdy	k6eAd1	mnohdy
vynechaly	vynechat	k5eAaPmAgFnP	vynechat
záběr	záběr	k1gInSc4	záběr
fatálního	fatální	k2eAgInSc2d1	fatální
zásahu	zásah	k1gInSc2	zásah
a	a	k8xC	a
roztříštění	roztříštění	k1gNnSc2	roztříštění
části	část	k1gFnSc2	část
Kennedyho	Kennedy	k1gMnSc2	Kennedy
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Zapruder	Zapruder	k1gMnSc1	Zapruder
ovšem	ovšem	k9	ovšem
nebyl	být	k5eNaImAgMnS	být
jediným	jediné	k1gNnSc7	jediné
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
pořídil	pořídit	k5eAaPmAgInS	pořídit
záběry	záběr	k1gInPc4	záběr
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Dealey	Dealea	k1gFnPc4	Dealea
Plaza	plaz	k1gMnSc2	plaz
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
celkem	celkem	k6eAd1	celkem
32	[number]	k4	32
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Amatérské	amatérský	k2eAgInPc1d1	amatérský
záběry	záběr	k1gInPc1	záběr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
fatální	fatální	k2eAgInSc4d1	fatální
zásah	zásah	k1gInSc4	zásah
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
také	také	k6eAd1	také
Orvillem	Orvillo	k1gNnSc7	Orvillo
Nixem	Nixum	k1gNnSc7	Nixum
<g/>
,	,	kIx,	,
Marií	Maria	k1gFnSc7	Maria
Muchmoreovou	Muchmoreův	k2eAgFnSc7d1	Muchmoreův
a	a	k8xC	a
Charlesem	Charles	k1gMnSc7	Charles
Bronsonem	Bronson	k1gMnSc7	Bronson
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc2d2	veliký
dálky	dálka	k1gFnSc2	dálka
než	než	k8xS	než
stál	stát	k5eAaImAgInS	stát
Zapruder	Zapruder	k1gInSc1	Zapruder
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
filmy	film	k1gInPc1	film
zachycující	zachycující	k2eAgFnSc2d1	zachycující
Dealey	Dealea	k1gFnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
okolo	okolo	k7c2	okolo
doby	doba	k1gFnSc2	doba
atentátu	atentát	k1gInSc2	atentát
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
Robertem	Robert	k1gMnSc7	Robert
Hughesem	Hughes	k1gMnSc7	Hughes
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Markem	Marek	k1gMnSc7	Marek
Bellem	bell	k1gInSc7	bell
<g/>
,	,	kIx,	,
Elsie	Elsie	k1gFnSc1	Elsie
Dormanovou	Dormanová	k1gFnSc7	Dormanová
<g/>
,	,	kIx,	,
Johnem	John	k1gMnSc7	John
Martinem	Martin	k1gMnSc7	Martin
<g/>
,	,	kIx,	,
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Patsy	Pats	k1gInPc1	Pats
Paschallovou	Paschallová	k1gFnSc7	Paschallová
<g/>
,	,	kIx,	,
Tinou	Tina	k1gFnSc7	Tina
Townerovou	Townerová	k1gFnSc7	Townerová
<g/>
,	,	kIx,	,
Jamesem	James	k1gMnSc7	James
Underwoodem	underwood	k1gInSc7	underwood
<g/>
,	,	kIx,	,
Davem	Dav	k1gInSc7	Dav
Wiegmanem	Wiegman	k1gMnSc7	Wiegman
<g/>
,	,	kIx,	,
Malem	Mal	k1gMnSc7	Mal
Couchem	Couch	k1gMnSc7	Couch
<g/>
,	,	kIx,	,
Thomasem	Thomas	k1gMnSc7	Thomas
Atkinsem	Atkins	k1gMnSc7	Atkins
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Danielem	Daniel	k1gMnSc7	Daniel
a	a	k8xC	a
neznámou	známý	k2eNgFnSc7d1	neznámá
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
modrých	modrý	k2eAgInPc6d1	modrý
šatech	šat	k1gInPc6	šat
stojící	stojící	k2eAgMnSc1d1	stojící
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
Elm	Elm	k1gMnSc1	Elm
street	street	k1gMnSc1	street
<g/>
.	.	kIx.	.
</s>
<s>
Statické	statický	k2eAgFnSc2d1	statická
fotografie	fotografia	k1gFnSc2	fotografia
pořídili	pořídit	k5eAaPmAgMnP	pořídit
Phillip	Phillip	k1gInSc4	Phillip
Willis	Willis	k1gFnSc2	Willis
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
Moorman	Moorman	k1gMnSc1	Moorman
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gMnSc1	Hugh
W.	W.	kA	W.
Betzner	Betzner	k1gMnSc1	Betzner
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Wilma	Wilma	k1gFnSc1	Wilma
Bond	bond	k1gInSc1	bond
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Croft	Croft	k1gMnSc1	Croft
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
fotografem	fotograf	k1gMnSc7	fotograf
přítomným	přítomný	k1gMnPc3	přítomný
na	na	k7c4	na
Dealey	Dealea	k1gFnPc4	Dealea
Plaza	plaz	k1gMnSc2	plaz
byl	být	k5eAaImAgMnS	být
Ike	Ike	k1gMnSc1	Ike
Altgens	Altgensa	k1gFnPc2	Altgensa
z	z	k7c2	z
dallaského	dallaské	k1gNnSc2	dallaské
Associated	Associated	k1gMnSc1	Associated
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
důležité	důležitý	k2eAgInPc1d1	důležitý
snímky	snímek	k1gInPc1	snímek
atentátu	atentát	k1gInSc2	atentát
mohla	moct	k5eAaImAgFnS	moct
pořídit	pořídit	k5eAaPmF	pořídit
i	i	k9	i
dodnes	dodnes	k6eAd1	dodnes
neidentifikovaná	identifikovaný	k2eNgFnSc1d1	neidentifikovaná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
výzkumníky	výzkumník	k1gMnPc4	výzkumník
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
jako	jako	k8xC	jako
Babushka	Babushka	k1gFnSc1	Babushka
Lady	Lada	k1gFnSc2	Lada
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
Bábuška	bábuška	k1gFnSc1	bábuška
lejdy	lejda	k1gFnSc2	lejda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
blízkosti	blízkost	k1gFnSc2	blízkost
filmovala	filmovat	k5eAaImAgFnS	filmovat
průjezd	průjezd	k1gInSc4	průjezd
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kolony	kolona	k1gFnSc2	kolona
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
záběry	záběr	k1gInPc1	záběr
jiných	jiný	k2eAgMnPc2d1	jiný
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
Zapruderově	Zapruderův	k2eAgInSc6d1	Zapruderův
filmu	film	k1gInSc6	film
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
snímků	snímek	k1gInPc2	snímek
283	[number]	k4	283
<g/>
–	–	k?	–
<g/>
292	[number]	k4	292
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
mužem	muž	k1gMnSc7	muž
ve	v	k7c6	v
světlém	světlý	k2eAgInSc6d1	světlý
svetru	svetr	k1gInSc6	svetr
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
filmuje	filmovat	k5eAaImIp3nS	filmovat
průjezd	průjezd	k1gInSc4	průjezd
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
limuzíny	limuzína	k1gFnSc2	limuzína
<g/>
.	.	kIx.	.
</s>
<s>
Vidět	vidět	k5eAaImF	vidět
je	být	k5eAaImIp3nS	být
též	též	k9	též
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
Marie	Maria	k1gFnSc2	Maria
Muchmoorové	Muchmoorová	k1gFnSc2	Muchmoorová
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
42	[number]	k4	42
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
filmu	film	k1gInSc6	film
Orvilla	Orvill	k1gMnSc2	Orvill
Nixe	Nix	k1gMnSc2	Nix
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
byly	být	k5eAaImAgFnP	být
dallaským	dallaský	k2eAgNnSc7d1	dallaský
Muzeem	muzeum	k1gNnSc7	muzeum
Sixth	Sixtha	k1gFnPc2	Sixtha
Floor	Floora	k1gFnPc2	Floora
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
dlouho	dlouho	k6eAd1	dlouho
neznámé	známý	k2eNgInPc1d1	neznámý
barevné	barevný	k2eAgInPc1d1	barevný
snímky	snímek	k1gInPc1	snímek
<g/>
,	,	kIx,	,
pořízené	pořízený	k2eAgNnSc1d1	pořízené
Georgem	Georg	k1gMnSc7	Georg
Jefferiesem	Jefferies	k1gMnSc7	Jefferies
v	v	k7c4	v
den	den	k1gInSc4	den
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
záběrech	záběr	k1gInPc6	záběr
není	být	k5eNaImIp3nS	být
zachycena	zachycen	k2eAgFnSc1d1	zachycena
doba	doba	k1gFnSc1	doba
střelby	střelba	k1gFnSc2	střelba
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byly	být	k5eAaImAgFnP	být
pořízeny	pořídit	k5eAaPmNgFnP	pořídit
zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
sekund	sekunda	k1gFnPc2	sekunda
před	před	k7c7	před
prvním	první	k4xOgInSc7	první
výstřelem	výstřel	k1gInSc7	výstřel
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
několik	několik	k4yIc4	několik
bloků	blok	k1gInPc2	blok
od	od	k7c2	od
Dealey	Dealea	k1gFnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
detail	detail	k1gInSc1	detail
relevantní	relevantní	k2eAgInSc1d1	relevantní
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
je	být	k5eAaImIp3nS	být
zřetelný	zřetelný	k2eAgInSc4d1	zřetelný
záběr	záběr	k1gInSc4	záběr
Kennedyho	Kennedy	k1gMnSc2	Kennedy
obleku	oblek	k1gInSc2	oblek
pomačkaného	pomačkaný	k2eAgInSc2d1	pomačkaný
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
hned	hned	k6eAd1	hned
pod	pod	k7c7	pod
límcem	límec	k1gInSc7	límec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
odlišným	odlišný	k2eAgFnPc3d1	odlišná
kalkulacím	kalkulace	k1gFnPc3	kalkulace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nízko	nízko	k6eAd1	nízko
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
byl	být	k5eAaImAgInS	být
Kennedy	Kenned	k1gMnPc4	Kenned
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
první	první	k4xOgFnSc7	první
střelou	střela	k1gFnSc7	střela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
Muzeu	muzeum	k1gNnSc3	muzeum
Sixth	Sixth	k1gMnSc1	Sixth
Floor	Floor	k1gInSc4	Floor
věnován	věnován	k2eAgInSc4d1	věnován
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
neznámý	známý	k2eNgInSc4d1	neznámý
barevný	barevný	k2eAgInSc4d1	barevný
film	film	k1gInSc4	film
pořízený	pořízený	k2eAgInSc4d1	pořízený
Williamem	William	k1gInSc7	William
W.	W.	kA	W.
Warrenem	Warren	k1gMnSc7	Warren
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
den	den	k1gInSc4	den
atentátu	atentát	k1gInSc2	atentát
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
osudného	osudný	k2eAgMnSc2d1	osudný
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
nezachycuje	zachycovat	k5eNaImIp3nS	zachycovat
atentát	atentát	k1gInSc1	atentát
samotný	samotný	k2eAgInSc1d1	samotný
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
přílet	přílet	k1gInSc1	přílet
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
One	One	k1gFnSc2	One
na	na	k7c4	na
dallaské	dallaský	k2eAgNnSc4d1	dallaský
letiště	letiště	k1gNnSc4	letiště
Love	lov	k1gInSc5	lov
Field	Field	k1gInSc4	Field
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
přivítání	přivítání	k1gNnSc4	přivítání
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
páru	pár	k1gInSc2	pár
představiteli	představitel	k1gMnPc7	představitel
Dallasu	Dallas	k1gInSc2	Dallas
a	a	k8xC	a
Texasu	Texas	k1gInSc2	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
studenti	student	k1gMnPc1	student
z	z	k7c2	z
dallaských	dallaský	k2eAgFnPc2d1	dallaská
škol	škola	k1gFnPc2	škola
byli	být	k5eAaImAgMnP	být
toho	ten	k3xDgNnSc2	ten
dne	den	k1gInSc2	den
uvolněni	uvolnit	k5eAaPmNgMnP	uvolnit
dříve	dříve	k6eAd2	dříve
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
a	a	k8xC	a
sledovat	sledovat	k5eAaImF	sledovat
přílet	přílet	k1gInSc4	přílet
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
choti	choť	k1gFnSc2	choť
<g/>
.	.	kIx.	.
</s>
<s>
Ward	Ward	k1gMnSc1	Ward
tedy	tedy	k9	tedy
spěchal	spěchat	k5eAaImAgMnS	spěchat
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
8	[number]	k4	8
mm	mm	kA	mm
barevnou	barevný	k2eAgFnSc4d1	barevná
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
záběry	záběr	k1gInPc4	záběr
pořídil	pořídit	k5eAaPmAgMnS	pořídit
<g/>
.	.	kIx.	.
</s>
<s>
Gary	Gara	k1gFnPc1	Gara
Mack	Macka	k1gFnPc2	Macka
<g/>
,	,	kIx,	,
kurátor	kurátor	k1gMnSc1	kurátor
sbírek	sbírka	k1gFnPc2	sbírka
Muzea	muzeum	k1gNnSc2	muzeum
Sixth	Sixth	k1gMnSc1	Sixth
Floor	Floor	k1gMnSc1	Floor
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
tyto	tento	k3xDgInPc4	tento
snímky	snímek	k1gInPc4	snímek
"	"	kIx"	"
<g/>
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
amatérský	amatérský	k2eAgInSc4d1	amatérský
záznam	záznam	k1gInSc4	záznam
příletu	přílet	k1gInSc2	přílet
Kennedyho	Kennedy	k1gMnSc2	Kennedy
do	do	k7c2	do
Dallasu	Dallas	k1gInSc2	Dallas
ze	z	k7c2	z
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
zatčení	zatčení	k1gNnSc6	zatčení
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
a	a	k8xC	a
zajištění	zajištění	k1gNnSc2	zajištění
důkazů	důkaz	k1gInPc2	důkaz
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
činu	čin	k1gInSc2	čin
držela	držet	k5eAaImAgFnS	držet
dallaská	dallaský	k2eAgFnSc1d1	dallaská
policie	policie	k1gFnSc1	policie
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
na	na	k7c6	na
policejní	policejní	k2eAgFnSc6d1	policejní
centrále	centrála	k1gFnSc6	centrála
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
provedeny	proveden	k2eAgInPc4d1	proveden
výslechy	výslech	k1gInPc4	výslech
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k1gInSc1	Oswald
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c4	mezi
22	[number]	k4	22
<g/>
.	.	kIx.	.
až	až	k9	až
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
vyslýchán	vyslýchat	k5eAaImNgInS	vyslýchat
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
otázky	otázka	k1gFnPc1	otázka
směřovaly	směřovat	k5eAaImAgFnP	směřovat
především	především	k9	především
k	k	k7c3	k
objasnění	objasnění	k1gNnSc3	objasnění
vraždy	vražda	k1gFnSc2	vražda
policisty	policista	k1gMnSc2	policista
Tippita	Tippit	k1gMnSc2	Tippit
a	a	k8xC	a
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Johna	John	k1gMnSc4	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k6eAd1	Oswald
však	však	k9	však
během	během	k7c2	během
výslechů	výslech	k1gInPc2	výslech
neustále	neustále	k6eAd1	neustále
odmítal	odmítat	k5eAaImAgMnS	odmítat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
vraždami	vražda	k1gFnPc7	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
otázek	otázka	k1gFnPc2	otázka
kladl	klást	k5eAaImAgMnS	klást
kapitán	kapitán	k1gMnSc1	kapitán
Fritz	Fritz	k1gMnSc1	Fritz
<g/>
,	,	kIx,	,
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
oddělení	oddělení	k1gNnSc2	oddělení
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
z	z	k7c2	z
výslechu	výslech	k1gInSc2	výslech
pořizoval	pořizovat	k5eAaImAgInS	pořizovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
stručné	stručný	k2eAgFnPc4d1	stručná
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Fritz	Fritz	k1gInSc1	Fritz
tedy	tedy	k9	tedy
napsal	napsat	k5eAaBmAgInS	napsat
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
výsleších	výslech	k1gInPc6	výslech
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
"	"	kIx"	"
<g/>
zpaměti	zpaměti	k6eAd1	zpaměti
<g/>
"	"	kIx"	"
o	o	k7c4	o
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výslechů	výslech	k1gInPc2	výslech
navíc	navíc	k6eAd1	navíc
nebyly	být	k5eNaImAgFnP	být
pořizování	pořizování	k1gNnSc4	pořizování
žádné	žádný	k3yNgInPc4	žádný
zvukové	zvukový	k2eAgInPc4d1	zvukový
ani	ani	k8xC	ani
těsnopisné	těsnopisný	k2eAgInPc4d1	těsnopisný
záznamy	záznam	k1gInPc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Výslechům	výslech	k1gInPc3	výslech
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
i	i	k9	i
příslušníci	příslušník	k1gMnPc1	příslušník
FBI	FBI	kA	FBI
a	a	k8xC	a
amerických	americký	k2eAgFnPc2d1	americká
tajných	tajný	k2eAgFnPc2d1	tajná
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
též	též	k9	též
příležitostně	příležitostně	k6eAd1	příležitostně
kladli	klást	k5eAaImAgMnP	klást
otázky	otázka	k1gFnPc4	otázka
a	a	k8xC	a
zapisovali	zapisovat	k5eAaImAgMnP	zapisovat
si	se	k3xPyFc3	se
průběžné	průběžný	k2eAgFnPc4d1	průběžná
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byly	být	k5eAaImAgFnP	být
Oswaldovi	Oswaldův	k2eAgMnPc1d1	Oswaldův
provedeny	provést	k5eAaPmNgInP	provést
parafínové	parafínový	k2eAgInPc1d1	parafínový
testy	test	k1gInPc1	test
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
pravé	pravý	k2eAgFnSc2d1	pravá
líce	líc	k1gFnSc2	líc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
skutečně	skutečně	k6eAd1	skutečně
střílel	střílet	k5eAaImAgMnS	střílet
z	z	k7c2	z
nalezené	nalezený	k2eAgFnSc2d1	nalezená
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rukou	ruka	k1gFnPc2	ruka
byly	být	k5eAaImAgInP	být
výsledky	výsledek	k1gInPc1	výsledek
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pravé	pravý	k2eAgFnSc2d1	pravá
líce	líc	k1gFnSc2	líc
však	však	k9	však
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
značné	značný	k2eAgFnSc2d1	značná
nespolehlivosti	nespolehlivost	k1gFnSc2	nespolehlivost
těchto	tento	k3xDgInPc2	tento
testů	test	k1gInPc2	test
však	však	k9	však
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
šetření	šetření	k1gNnSc6	šetření
nebrala	brát	k5eNaImAgFnS	brát
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
výsledky	výsledek	k1gInPc1	výsledek
ohled	ohled	k1gInSc1	ohled
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k1gMnSc1	Oswald
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
během	během	k7c2	během
výslechu	výslech	k1gInSc2	výslech
na	na	k7c4	na
policii	policie	k1gFnSc4	policie
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
byl	být	k5eAaImAgInS	být
konfrontován	konfrontován	k2eAgInSc1d1	konfrontován
s	s	k7c7	s
důkazy	důkaz	k1gInPc7	důkaz
a	a	k8xC	a
zjištěními	zjištění	k1gNnPc7	zjištění
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nijak	nijak	k6eAd1	nijak
neobjasnil	objasnit	k5eNaPmAgMnS	objasnit
a	a	k8xC	a
uchyloval	uchylovat	k5eAaImAgMnS	uchylovat
se	se	k3xPyFc4	se
k	k	k7c3	k
tvrzením	tvrzení	k1gNnPc3	tvrzení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nepravdivá	pravdivý	k2eNgFnSc1d1	nepravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
výslechy	výslech	k1gInPc4	výslech
již	již	k9	již
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
provést	provést	k5eAaPmF	provést
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Oswald	Oswald	k1gInSc1	Oswald
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Federální	federální	k2eAgInSc1d1	federální
úřad	úřad	k1gInSc1	úřad
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
první	první	k4xOgInSc1	první
pověřen	pověřen	k2eAgInSc1d1	pověřen
úplným	úplný	k2eAgNnSc7d1	úplné
vyšetřením	vyšetření	k1gNnSc7	vyšetření
okolností	okolnost	k1gFnPc2	okolnost
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
<g/>
,	,	kIx,	,
sdělil	sdělit	k5eAaPmAgMnS	sdělit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
John	John	k1gMnSc1	John
Edgar	Edgar	k1gMnSc1	Edgar
Hoover	Hoover	k1gMnSc1	Hoover
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
chceme	chtít	k5eAaImIp1nP	chtít
získat	získat	k5eAaPmF	získat
evidentní	evidentní	k2eAgInSc4d1	evidentní
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
veřejnost	veřejnost	k1gFnSc4	veřejnost
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
o	o	k7c6	o
vině	vina	k1gFnSc6	vina
L.	L.	kA	L.
H.	H.	kA	H.
Oswalda	Oswalda	k1gMnSc1	Oswalda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
vydal	vydat	k5eAaPmAgInS	vydat
FBI	FBI	kA	FBI
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pak	pak	k6eAd1	pak
předal	předat	k5eAaPmAgMnS	předat
Warrenově	Warrenův	k2eAgFnSc3d1	Warrenova
komisi	komise	k1gFnSc3	komise
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
vypáleny	vypálit	k5eAaPmNgFnP	vypálit
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
střely	střel	k1gInPc4	střel
<g/>
;	;	kIx,	;
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
střel	střel	k1gInSc1	střel
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
,	,	kIx,	,
neshoda	neshoda	k1gFnSc1	neshoda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ze	z	k7c2	z
střel	střela	k1gFnPc2	střela
zasáhla	zasáhnout	k5eAaPmAgNnP	zasáhnout
Kennedyho	Kennedy	k1gMnSc2	Kennedy
a	a	k8xC	a
která	který	k3yQgFnSc1	který
guvernéra	guvernér	k1gMnSc4	guvernér
Connallyho	Connally	k1gMnSc4	Connally
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
FBI	FBI	kA	FBI
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
ze	z	k7c2	z
střel	střela	k1gFnPc2	střela
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
guvernéra	guvernér	k1gMnSc4	guvernér
Connallyho	Connally	k1gMnSc4	Connally
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
smrtící	smrtící	k2eAgMnSc1d1	smrtící
opět	opět	k6eAd1	opět
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
názorem	názor	k1gInSc7	názor
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
první	první	k4xOgFnSc1	první
střela	střela	k1gFnSc1	střela
cíl	cíl	k1gInSc1	cíl
minula	minout	k5eAaImAgFnS	minout
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
a	a	k8xC	a
zranila	zranit	k5eAaPmAgFnS	zranit
i	i	k9	i
Connallyho	Connally	k1gMnSc4	Connally
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
opět	opět	k6eAd1	opět
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
přičemž	přičemž	k6eAd1	přičemž
způsobila	způsobit	k5eAaPmAgFnS	způsobit
devastující	devastující	k2eAgNnSc4d1	devastující
zranění	zranění	k1gNnSc4	zranění
Kennedyho	Kennedy	k1gMnSc2	Kennedy
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
Federálního	federální	k2eAgInSc2d1	federální
úřadu	úřad	k1gInSc2	úřad
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
přezkoumáno	přezkoumat	k5eAaPmNgNnS	přezkoumat
výborem	výbor	k1gInSc7	výbor
HSCA	HSCA	kA	HSCA
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
FBI	FBI	kA	FBI
dostatečně	dostatečně	k6eAd1	dostatečně
prošetřil	prošetřit	k5eAaPmAgMnS	prošetřit
Oswaldovy	Oswaldův	k2eAgInPc4d1	Oswaldův
motivy	motiv	k1gInPc4	motiv
spáchání	spáchání	k1gNnSc3	spáchání
atentátu	atentát	k1gInSc3	atentát
a	a	k8xC	a
správně	správně	k6eAd1	správně
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
jeho	jeho	k3xOp3gFnPc4	jeho
pohnutky	pohnutka	k1gFnPc4	pohnutka
záměrně	záměrně	k6eAd1	záměrně
ohrozit	ohrozit	k5eAaPmF	ohrozit
národní	národní	k2eAgFnSc4d1	národní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
vedl	vést	k5eAaImAgMnS	vést
řádné	řádný	k2eAgNnSc4d1	řádné
a	a	k8xC	a
profesionální	profesionální	k2eAgNnSc4d1	profesionální
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
spojitosti	spojitost	k1gFnSc3	spojitost
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
s	s	k7c7	s
atentátem	atentát	k1gInSc7	atentát
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
selhal	selhat	k5eAaPmAgInS	selhat
v	v	k7c6	v
prošetření	prošetření	k1gNnSc6	prošetření
možnosti	možnost	k1gFnSc2	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
za	za	k7c7	za
atentátem	atentát	k1gInSc7	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
spiknutí	spiknutí	k1gNnSc1	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
dostatečně	dostatečně	k6eAd1	dostatečně
nesdílel	sdílet	k5eNaImAgMnS	sdílet
svá	svůj	k3xOyFgNnPc4	svůj
zjištění	zjištění	k1gNnPc4	zjištění
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
vyšetřujícími	vyšetřující	k2eAgInPc7d1	vyšetřující
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
podroben	podrobit	k5eAaPmNgInS	podrobit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
možného	možný	k2eAgNnSc2d1	možné
spojení	spojení	k1gNnSc2	spojení
agenta	agent	k1gMnSc2	agent
Jamese	Jamese	k1gFnSc2	Jamese
Hostyho	Hosty	k1gMnSc2	Hosty
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
adresáři	adresář	k1gInSc6	adresář
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
Hostyho	Hosty	k1gMnSc2	Hosty
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Warrenově	Warrenův	k2eAgFnSc3d1	Warrenova
komisi	komise	k1gFnSc3	komise
nejprve	nejprve	k6eAd1	nejprve
přepis	přepis	k1gInSc4	přepis
ručně	ručně	k6eAd1	ručně
psaného	psaný	k2eAgInSc2d1	psaný
Oswaldova	Oswaldův	k2eAgInSc2d1	Oswaldův
adresáře	adresář	k1gInSc2	adresář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
Hostyho	Hosty	k1gMnSc4	Hosty
jméno	jméno	k1gNnSc4	jméno
vymazáno	vymazán	k2eAgNnSc4d1	vymazáno
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
Oswald	Oswald	k1gInSc1	Oswald
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
Hostym	Hostym	k1gInSc4	Hostym
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kanceláři	kancelář	k1gFnSc6	kancelář
a	a	k8xC	a
když	když	k8xS	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hosty	host	k1gMnPc4	host
není	být	k5eNaImIp3nS	být
přítomen	přítomno	k1gNnPc2	přítomno
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
mu	on	k3xPp3gMnSc3	on
zde	zde	k6eAd1	zde
zanechat	zanechat	k5eAaPmF	zanechat
obálku	obálka	k1gFnSc4	obálka
s	s	k7c7	s
dopisem	dopis	k1gInSc7	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgMnS	být
Oswald	Oswald	k1gMnSc1	Oswald
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
Jackem	Jacek	k1gMnSc7	Jacek
Rubym	Rubym	k1gInSc4	Rubym
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Hostyho	Hosty	k1gMnSc2	Hosty
nadřízený	nadřízený	k1gMnSc1	nadřízený
pověřen	pověřit	k5eAaPmNgMnS	pověřit
zničením	zničení	k1gNnSc7	zničení
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
dopisu	dopis	k1gInSc2	dopis
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
učinil	učinit	k5eAaPmAgMnS	učinit
roztrháním	roztrhání	k1gNnSc7	roztrhání
a	a	k8xC	a
spláchnutím	spláchnutí	k1gNnSc7	spláchnutí
do	do	k7c2	do
toalety	toaleta	k1gFnSc2	toaleta
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hosty	host	k1gMnPc7	host
vypovídal	vypovídat	k5eAaPmAgMnS	vypovídat
před	před	k7c7	před
Warrenovou	Warrenový	k2eAgFnSc7d1	Warrenový
komisí	komise	k1gFnSc7	komise
<g/>
,	,	kIx,	,
nepřiznal	přiznat	k5eNaPmAgInS	přiznat
žádnou	žádný	k3yNgFnSc4	žádný
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
Oswaldem	Oswald	k1gInSc7	Oswald
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
možné	možný	k2eAgFnSc6d1	možná
spojitosti	spojitost	k1gFnSc6	spojitost
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
až	až	k9	až
později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
HSCA	HSCA	kA	HSCA
<g/>
.	.	kIx.	.
</s>
<s>
Seržant	seržant	k1gMnSc1	seržant
Davis	Davis	k1gFnSc2	Davis
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
vražd	vražda	k1gFnPc2	vražda
dallaské	dallaský	k2eAgFnSc2d1	dallaská
policie	policie	k1gFnSc2	policie
se	se	k3xPyFc4	se
před	před	k7c7	před
spácháním	spáchání	k1gNnSc7	spáchání
atentátu	atentát	k1gInSc2	atentát
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zajistil	zajistit	k5eAaPmAgMnS	zajistit
dostatečně	dostatečně	k6eAd1	dostatečně
přísná	přísný	k2eAgNnPc4d1	přísné
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kolony	kolona	k1gFnSc2	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
Winston	Winston	k1gInSc1	Winston
Lawson	Lawson	k1gInSc1	Lawson
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
plánování	plánování	k1gNnSc4	plánování
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
dallaskou	dallaský	k2eAgFnSc4d1	dallaská
policii	policie	k1gFnSc4	policie
navíc	navíc	k6eAd1	navíc
ujišťoval	ujišťovat	k5eAaImAgMnS	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bezprostředně	bezprostředně	k6eAd1	bezprostředně
za	za	k7c7	za
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
limuzínou	limuzína	k1gFnSc7	limuzína
jel	jet	k5eAaImAgInS	jet
doprovod	doprovod	k1gInSc1	doprovod
zkušených	zkušený	k2eAgMnPc2d1	zkušený
policistů	policista	k1gMnPc2	policista
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
přítomnost	přítomnost	k1gFnSc1	přítomnost
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
návštěvách	návštěva	k1gFnPc6	návštěva
důležitých	důležitý	k2eAgFnPc2d1	důležitá
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
politiků	politik	k1gMnPc2	politik
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgMnSc1d1	policejní
velitel	velitel	k1gMnSc1	velitel
Jesse	Jess	k1gMnSc2	Jess
Curry	Curra	k1gMnSc2	Curra
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
své	svůj	k3xOyFgMnPc4	svůj
muže	muž	k1gMnPc4	muž
vybavené	vybavený	k2eAgInPc1d1	vybavený
samopaly	samopal	k1gInPc1	samopal
a	a	k8xC	a
puškami	puška	k1gFnPc7	puška
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
atentátu	atentát	k1gInSc2	atentát
zabráněno	zabráněn	k2eAgNnSc1d1	zabráněno
nebo	nebo	k8xC	nebo
by	by	kYmCp3nP	by
policisté	policista	k1gMnPc1	policista
přinejmenším	přinejmenším	k6eAd1	přinejmenším
mohli	moct	k5eAaImAgMnP	moct
zabránit	zabránit	k5eAaPmF	zabránit
útěku	útěk	k1gInSc3	útěk
vraha	vrah	k1gMnSc2	vrah
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
mezeru	mezera	k1gFnSc4	mezera
představovala	představovat	k5eAaImAgFnS	představovat
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dallaská	dallaský	k2eAgFnSc1d1	dallaská
policie	policie	k1gFnSc1	policie
neměla	mít	k5eNaImAgFnS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
jednotku	jednotka	k1gFnSc4	jednotka
odstřelovačů	odstřelovač	k1gMnPc2	odstřelovač
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
ochrany	ochrana	k1gFnSc2	ochrana
prezidenta	prezident	k1gMnSc2	prezident
nasazena	nasadit	k5eAaPmNgNnP	nasadit
podél	podél	k7c2	podél
trasy	trasa	k1gFnSc2	trasa
průjezdu	průjezd	k1gInSc2	průjezd
kolony	kolona	k1gFnSc2	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kolona	kolona	k1gFnSc1	kolona
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
opustila	opustit	k5eAaPmAgFnS	opustit
ulici	ulice	k1gFnSc4	ulice
Main	Main	k1gNnSc4	Main
Street	Streeta	k1gFnPc2	Streeta
musela	muset	k5eAaImAgFnS	muset
hned	hned	k6eAd1	hned
při	při	k7c6	při
vjezdu	vjezd	k1gInSc6	vjezd
na	na	k7c4	na
Dealey	Dealea	k1gFnPc4	Dealea
Plaza	plaz	k1gMnSc2	plaz
zabočit	zabočit	k5eAaPmF	zabočit
nejprve	nejprve	k6eAd1	nejprve
doprava	doprava	k1gFnSc1	doprava
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
90	[number]	k4	90
<g/>
°	°	k?	°
a	a	k8xC	a
po	po	k7c6	po
přibližně	přibližně	k6eAd1	přibližně
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
metrech	metr	k1gInPc6	metr
musela	muset	k5eAaImAgFnS	muset
u	u	k7c2	u
budovy	budova	k1gFnSc2	budova
Knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
zatočit	zatočit	k5eAaPmF	zatočit
prudce	prudko	k6eAd1	prudko
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
nestandardností	nestandardnost	k1gFnPc2	nestandardnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
sice	sice	k8xC	sice
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolona	kolona	k1gFnSc1	kolona
provedla	provést	k5eAaPmAgFnS	provést
zabočení	zabočení	k1gNnSc4	zabočení
doleva	doleva	k6eAd1	doleva
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
120	[number]	k4	120
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
návštěv	návštěva	k1gFnPc2	návštěva
vysokých	vysoký	k2eAgInPc2d1	vysoký
státních	státní	k2eAgInPc2d1	státní
činitelů	činitel	k1gInPc2	činitel
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
pouze	pouze	k6eAd1	pouze
taková	takový	k3xDgFnSc1	takový
zatáčka	zatáčka	k1gFnSc1	zatáčka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
úhel	úhel	k1gInSc1	úhel
nepřesahoval	přesahovat	k5eNaImAgInS	přesahovat
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nutnosti	nutnost	k1gFnSc2	nutnost
vybrat	vybrat	k5eAaPmF	vybrat
takto	takto	k6eAd1	takto
ostrou	ostrý	k2eAgFnSc4d1	ostrá
zatáčku	zatáčka	k1gFnSc4	zatáčka
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
provést	provést	k5eAaPmF	provést
výrazné	výrazný	k2eAgNnSc4d1	výrazné
zpomalení	zpomalení	k1gNnSc4	zpomalení
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
limuzíny	limuzína	k1gFnSc2	limuzína
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Trasa	trasa	k1gFnSc1	trasa
průjezdu	průjezd	k1gInSc2	průjezd
byla	být	k5eAaImAgFnS	být
těmito	tento	k3xDgFnPc7	tento
"	"	kIx"	"
<g/>
manévry	manévr	k1gInPc4	manévr
<g/>
"	"	kIx"	"
značně	značně	k6eAd1	značně
zkomplikována	zkomplikován	k2eAgFnSc1d1	zkomplikována
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Kolona	kolona	k1gFnSc1	kolona
mohla	moct	k5eAaImAgFnS	moct
při	při	k7c6	při
vjezdu	vjezd	k1gInSc6	vjezd
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
pokračovat	pokračovat	k5eAaImF	pokračovat
dále	daleko	k6eAd2	daleko
po	po	k7c4	po
prostřední	prostřední	k2eAgFnSc4d1	prostřední
komunikaci	komunikace	k1gFnSc4	komunikace
ulice	ulice	k1gFnSc2	ulice
Main	Main	k1gMnSc1	Main
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
přímo	přímo	k6eAd1	přímo
skrze	skrze	k?	skrze
park	park	k1gInSc1	park
a	a	k8xC	a
nevyžadovala	vyžadovat	k5eNaImAgFnS	vyžadovat
tak	tak	k6eAd1	tak
jeho	jeho	k3xOp3gNnSc4	jeho
zdlouhavé	zdlouhavý	k2eAgNnSc4d1	zdlouhavé
objíždění	objíždění	k1gNnSc4	objíždění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ulice	ulice	k1gFnSc2	ulice
Houston	Houston	k1gInSc1	Houston
Street	Street	k1gMnSc1	Street
a	a	k8xC	a
Elm	Elm	k1gMnSc1	Elm
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
podle	podle	k7c2	podle
trasy	trasa	k1gFnSc2	trasa
<g/>
,	,	kIx,	,
zveřejněné	zveřejněný	k2eAgInPc1d1	zveřejněný
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
denících	deník	k1gInPc6	deník
Dallas	Dallas	k1gInSc4	Dallas
Times	Timesa	k1gFnPc2	Timesa
Herald	Heralda	k1gFnPc2	Heralda
a	a	k8xC	a
Dallas	Dallas	k1gInSc1	Dallas
Morning	Morning	k1gInSc1	Morning
News	News	k1gInSc4	News
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kolona	kolona	k1gFnSc1	kolona
skutečně	skutečně	k6eAd1	skutečně
projet	projet	k5eAaPmF	projet
komunikací	komunikace	k1gFnSc7	komunikace
přímo	přímo	k6eAd1	přímo
skrze	skrze	k?	skrze
střed	střed	k1gInSc1	střed
náměstí	náměstí	k1gNnSc1	náměstí
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
dále	daleko	k6eAd2	daleko
až	až	k9	až
za	za	k7c4	za
železniční	železniční	k2eAgInSc4d1	železniční
viadukt	viadukt	k1gInSc4	viadukt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
ulice	ulice	k1gFnPc1	ulice
(	(	kIx(	(
<g/>
Elm	Elm	k1gFnPc1	Elm
<g/>
,	,	kIx,	,
Main	Main	k1gInSc1	Main
a	a	k8xC	a
Commerce	Commerka	k1gFnSc3	Commerka
Street	Streeta	k1gFnPc2	Streeta
<g/>
)	)	kIx)	)
stékaly	stékat	k5eAaImAgFnP	stékat
v	v	k7c6	v
jeden	jeden	k4xCgInSc4	jeden
velký	velký	k2eAgInSc4d1	velký
devítiproudý	devítiproudý	k2eAgInSc4d1	devítiproudý
silniční	silniční	k2eAgInSc4d1	silniční
tok	tok	k1gInSc4	tok
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
Dallas	Dallas	k1gInSc1	Dallas
Trade	Trad	k1gInSc5	Trad
Mart	Marta	k1gFnPc2	Marta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
byla	být	k5eAaImAgFnS	být
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
prezidentem	prezident	k1gMnSc7	prezident
Johnsonem	Johnson	k1gMnSc7	Johnson
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
týden	týden	k1gInSc4	týden
po	po	k7c4	po
spáchání	spáchání	k1gNnSc4	spáchání
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgMnS	stanout
Earl	earl	k1gMnSc1	earl
Warren	Warrna	k1gFnPc2	Warrna
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
soudce	soudce	k1gMnSc1	soudce
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
komise	komise	k1gFnSc1	komise
neoficiálně	neoficiálně	k6eAd1	neoficiálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
desetiměsíčním	desetiměsíční	k2eAgNnSc6d1	desetiměsíční
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	září	k1gNnSc2	září
1964	[number]	k4	1964
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
zpráva	zpráva	k1gFnSc1	zpráva
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezjistila	zjistit	k5eNaPmAgFnS	zjistit
žádnou	žádný	k3yNgFnSc4	žádný
spojitost	spojitost	k1gFnSc1	spojitost
atentátu	atentát	k1gInSc2	atentát
s	s	k7c7	s
domácím	domácí	k2eAgNnSc7d1	domácí
či	či	k8xC	či
zahraničním	zahraniční	k2eAgNnSc7d1	zahraniční
spiknutím	spiknutí	k1gNnSc7	spiknutí
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
zapojení	zapojení	k1gNnSc1	zapojení
žádných	žádný	k3yNgFnPc2	žádný
jiných	jiný	k2eAgFnPc2d1	jiná
osob	osoba	k1gFnPc2	osoba
či	či	k8xC	či
skupin	skupina	k1gFnPc2	skupina
do	do	k7c2	do
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Oswald	Oswald	k1gInSc4	Oswald
jednal	jednat	k5eAaImAgMnS	jednat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
Jack	Jack	k1gMnSc1	Jack
Ruby	rub	k1gInPc4	rub
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
zabil	zabít	k5eAaPmAgMnS	zabít
též	též	k9	též
"	"	kIx"	"
<g/>
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
střely	střela	k1gFnPc1	střela
byly	být	k5eAaImAgFnP	být
vypáleny	vypálit	k5eAaPmNgFnP	vypálit
Lee	Lea	k1gFnSc3	Lea
Harvey	Harvea	k1gMnSc2	Harvea
Oswaldem	Oswald	k1gMnSc7	Oswald
budovy	budova	k1gFnSc2	budova
z	z	k7c2	z
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
zádech	záda	k1gNnPc6	záda
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kolony	kolona	k1gFnSc2	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
též	též	k6eAd1	též
stanovila	stanovit	k5eAaPmAgFnS	stanovit
časové	časový	k2eAgNnSc4d1	časové
rozmezí	rozmezí	k1gNnSc4	rozmezí
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
byly	být	k5eAaImAgFnP	být
vypáleny	vypálen	k2eAgFnPc1d1	vypálena
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
střely	střela	k1gFnPc4	střela
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozmezí	rozmezí	k1gNnSc1	rozmezí
mělo	mít	k5eAaImAgNnS	mít
činit	činit	k5eAaImF	činit
4,8	[number]	k4	4,8
až	až	k9	až
7	[number]	k4	7
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
dále	daleko	k6eAd2	daleko
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
střel	střela	k1gFnPc2	střela
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
minula	minout	k5eAaImAgFnS	minout
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
"	"	kIx"	"
<g/>
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
"	"	kIx"	"
střela	střela	k1gFnSc1	střela
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
rozhraní	rozhraní	k1gNnSc2	rozhraní
mezi	mezi	k7c7	mezi
krkem	krk	k1gInSc7	krk
a	a	k8xC	a
zády	záda	k1gNnPc7	záda
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ven	ven	k6eAd1	ven
přední	přední	k2eAgFnSc7d1	přední
částí	část	k1gFnSc7	část
krku	krk	k1gInSc2	krk
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zranila	zranit	k5eAaPmAgFnS	zranit
guvernéra	guvernér	k1gMnSc4	guvernér
Connallyho	Connally	k1gMnSc4	Connally
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
ze	z	k7c2	z
střel	střela	k1gFnPc2	střela
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
mu	on	k3xPp3gMnSc3	on
způsobila	způsobit	k5eAaPmAgFnS	způsobit
fatální	fatální	k2eAgFnSc7d1	fatální
zranění	zranění	k1gNnSc4	zranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Texaského	texaský	k2eAgInSc2d1	texaský
knižního	knižní	k2eAgInSc2d1	knižní
velkoskladu	velkosklad	k1gInSc2	velkosklad
nalezeny	naleznout	k5eAaPmNgInP	naleznout
tři	tři	k4xCgInPc1	tři
prázdné	prázdný	k2eAgInPc1d1	prázdný
patrony	patron	k1gInPc1	patron
a	a	k8xC	a
použitá	použitý	k2eAgFnSc1d1	použitá
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
6,5	[number]	k4	6,5
<g/>
×	×	k?	×
<g/>
52	[number]	k4	52
mm	mm	kA	mm
Model	model	k1gInSc4	model
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
Carcano	Carcana	k1gFnSc5	Carcana
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nalezena	nalézt	k5eAaBmNgFnS	nalézt
ukrytá	ukrytý	k2eAgFnSc1d1	ukrytá
poblíž	poblíž	k7c2	poblíž
místa	místo	k1gNnSc2	místo
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
zranění	zranění	k1gNnSc4	zranění
guvernéra	guvernér	k1gMnSc2	guvernér
Connallyho	Connally	k1gMnSc2	Connally
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejná	stejný	k2eAgFnSc1d1	stejná
kulka	kulka	k1gFnSc1	kulka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Kennedyho	Kennedy	k1gMnSc4	Kennedy
poranila	poranit	k5eAaPmAgFnS	poranit
na	na	k7c6	na
krku	krk	k1gInSc6	krk
způsobila	způsobit	k5eAaPmAgFnS	způsobit
i	i	k9	i
všechna	všechen	k3xTgNnPc1	všechen
zranění	zranění	k1gNnPc1	zranění
Connallyho	Connally	k1gMnSc2	Connally
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
vešla	vejít	k5eAaPmAgFnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k9	jako
teorie	teorie	k1gFnSc1	teorie
jedné	jeden	k4xCgFnSc2	jeden
kulky	kulka	k1gFnSc2	kulka
<g/>
.	.	kIx.	.
</s>
<s>
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
také	také	k9	také
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
ostatní	ostatní	k2eAgFnPc4d1	ostatní
okolnosti	okolnost	k1gFnPc4	okolnost
atentátu	atentát	k1gInSc2	atentát
a	a	k8xC	a
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
kritiku	kritika	k1gFnSc4	kritika
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
zajištění	zajištění	k1gNnSc2	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgInPc1d1	chráněný
lhůtou	lhůta	k1gFnSc7	lhůta
pro	pro	k7c4	pro
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
práv	právo	k1gNnPc2	právo
nevinných	vinný	k2eNgFnPc2d1	nevinná
osob	osoba	k1gFnPc2	osoba
zapojených	zapojený	k2eAgFnPc2d1	zapojená
do	do	k7c2	do
případu	případ	k1gInSc2	případ
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
v	v	k7c6	v
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
generálním	generální	k2eAgMnSc7d1	generální
prokurátorem	prokurátor	k1gMnSc7	prokurátor
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Ramsey	Ramsea	k1gFnSc2	Ramsea
Clarkem	Clarko	k1gNnSc7	Clarko
<g/>
,	,	kIx,	,
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
skupina	skupina	k1gFnSc1	skupina
čtyř	čtyři	k4xCgInPc2	čtyři
lékařských	lékařský	k2eAgMnPc2d1	lékařský
expertů	expert	k1gMnPc2	expert
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
panel	panel	k1gInSc1	panel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc6	D.C.
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
prozkoumání	prozkoumání	k1gNnSc1	prozkoumání
veškerých	veškerý	k3xTgInPc2	veškerý
záznamů	záznam	k1gInPc2	záznam
souvisejících	související	k2eAgInPc2d1	související
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
(	(	kIx(	(
<g/>
rentgenové	rentgenový	k2eAgFnPc1d1	rentgenová
snímky	snímka	k1gFnPc1	snímka
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
dokumenty	dokument	k1gInPc1	dokument
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ramseyho	Ramseyze	k6eAd1	Ramseyze
panel	panel	k1gInSc1	panel
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
byl	být	k5eAaImAgMnS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
dvěma	dva	k4xCgFnPc7	dva
střelami	střela	k1gFnPc7	střela
vypálenými	vypálený	k2eAgFnPc7d1	vypálená
zezadu	zezadu	k6eAd1	zezadu
a	a	k8xC	a
shora	shora	k6eAd1	shora
(	(	kIx(	(
<g/>
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgFnSc1	první
střela	střela	k1gFnSc1	střela
prošla	projít	k5eAaPmAgFnS	projít
jeho	jeho	k3xOp3gNnSc1	jeho
krkem	krk	k1gInSc7	krk
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
měla	mít	k5eAaImAgNnP	mít
Kennedyho	Kennedy	k1gMnSc2	Kennedy
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
do	do	k7c2	do
lebky	lebka	k1gFnSc2	lebka
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
již	již	k6eAd1	již
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
fatální	fatální	k2eAgFnSc4d1	fatální
frakturu	fraktura	k1gFnSc4	fraktura
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pravého	pravý	k2eAgInSc2d1	pravý
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Geralda	Gerald	k1gMnSc2	Gerald
Forda	ford	k1gMnSc2	ford
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Komise	komise	k1gFnSc1	komise
prezidenta	prezident	k1gMnSc2	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
pro	pro	k7c4	pro
aktivity	aktivita	k1gFnPc4	aktivita
CIA	CIA	kA	CIA
na	na	k7c4	na
území	území	k1gNnSc4	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
President	president	k1gMnSc1	president
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Commission	Commission	k1gInSc1	Commission
on	on	k3xPp3gMnSc1	on
CIA	CIA	kA	CIA
activities	activities	k1gMnSc1	activities
within	within	k1gMnSc1	within
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
)	)	kIx)	)
s	s	k7c7	s
úkolem	úkol	k1gInSc7	úkol
prošetřit	prošetřit	k5eAaPmF	prošetřit
aktivity	aktivita	k1gFnPc4	aktivita
CIA	CIA	kA	CIA
prováděné	prováděný	k2eAgNnSc1d1	prováděné
na	na	k7c4	na
území	území	k1gNnSc4	území
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
USA	USA	kA	USA
Nelsonem	Nelson	k1gMnSc7	Nelson
Rockefellerem	Rockefeller	k1gMnSc7	Rockefeller
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
komise	komise	k1gFnSc2	komise
byla	být	k5eAaImAgFnS	být
určena	určen	k2eAgFnSc1d1	určena
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
prošetření	prošetření	k1gNnSc3	prošetření
okolností	okolnost	k1gFnPc2	okolnost
atentátu	atentát	k1gInSc2	atentát
na	na	k7c6	na
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
především	především	k9	především
na	na	k7c6	na
analýze	analýza	k1gFnSc6	analýza
Zapruderova	Zapruderův	k2eAgInSc2d1	Zapruderův
filmu	film	k1gInSc2	film
a	a	k8xC	a
zjišťovala	zjišťovat	k5eAaImAgFnS	zjišťovat
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
možnou	možný	k2eAgFnSc4d1	možná
přítomnost	přítomnost	k1gFnSc4	přítomnost
agentů	agens	k1gInPc2	agens
CIA	CIA	kA	CIA
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Howarda	Howard	k1gMnSc2	Howard
Hunta	Hunt	k1gMnSc2	Hunt
a	a	k8xC	a
Franka	Frank	k1gMnSc2	Frank
Sturgise	Sturgise	k1gFnSc2	Sturgise
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
spáchání	spáchání	k1gNnSc2	spáchání
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
Hunt	hunt	k1gInSc4	hunt
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
podezření	podezření	k1gNnSc4	podezření
kvůli	kvůli	k7c3	kvůli
dopisu	dopis	k1gInSc3	dopis
objevenému	objevený	k2eAgInSc3d1	objevený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
obdržet	obdržet	k5eAaPmF	obdržet
od	od	k7c2	od
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
Huntův	Huntův	k2eAgInSc1d1	Huntův
záměr	záměr	k1gInSc1	záměr
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Oswaldem	Oswald	k1gInSc7	Oswald
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
přípravách	příprava	k1gFnPc6	příprava
na	na	k7c4	na
atentát	atentát	k1gInSc4	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
však	však	k9	však
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
agentů	agent	k1gMnPc2	agent
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dopis	dopis	k1gInSc1	dopis
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
je	být	k5eAaImIp3nS	být
padělek	padělek	k1gInSc1	padělek
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
KGB	KGB	kA	KGB
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tak	tak	k6eAd1	tak
chtěla	chtít	k5eAaImAgFnS	chtít
zdiskreditovat	zdiskreditovat	k5eAaPmF	zdiskreditovat
americké	americký	k2eAgFnPc4d1	americká
tajné	tajný	k2eAgFnPc4d1	tajná
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
také	také	k9	také
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zásah	zásah	k1gInSc1	zásah
prezidentovy	prezidentův	k2eAgFnSc2d1	prezidentova
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
Zapruderově	Zapruderův	k2eAgInSc6d1	Zapruderův
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
skutečně	skutečně	k6eAd1	skutečně
způsoben	způsobit	k5eAaPmNgInS	způsobit
střelou	střela	k1gFnSc7	střela
vypálenou	vypálený	k2eAgFnSc7d1	vypálená
zezadu	zezadu	k6eAd1	zezadu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
nicméně	nicméně	k8xC	nicméně
synem	syn	k1gMnSc7	syn
E.	E.	kA	E.
Hunta	Hunt	k1gInSc2	Hunt
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
zvuková	zvukový	k2eAgFnSc1d1	zvuková
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
Hunt	hunt	k1gInSc1	hunt
na	na	k7c6	na
smrtelném	smrtelný	k2eAgNnSc6d1	smrtelné
loži	lože	k1gNnSc6	lože
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
do	do	k7c2	do
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
atentát	atentát	k1gInSc1	atentát
spáchán	spáchat	k5eAaPmNgInS	spáchat
na	na	k7c4	na
přímou	přímý	k2eAgFnSc4d1	přímá
objednávku	objednávka	k1gFnSc4	objednávka
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Johnsona	Johnson	k1gMnSc2	Johnson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
navíc	navíc	k6eAd1	navíc
postarat	postarat	k5eAaPmF	postarat
o	o	k7c6	o
zametení	zametení	k1gNnSc6	zametení
stop	stopa	k1gFnPc2	stopa
po	po	k7c6	po
skutečných	skutečný	k2eAgInPc6d1	skutečný
iniciátorech	iniciátor	k1gInPc6	iniciátor
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Šestnáct	šestnáct	k4xCc1	šestnáct
let	léto	k1gNnPc2	léto
po	po	k7c6	po
spáchání	spáchání	k1gNnSc6	spáchání
atentátu	atentát	k1gInSc3	atentát
zrevidoval	zrevidovat	k5eAaPmAgInS	zrevidovat
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
vyšetření	vyšetření	k1gNnSc4	vyšetření
atentátu	atentát	k1gInSc2	atentát
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
HSCA	HSCA	kA	HSCA
<g/>
,	,	kIx,	,
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
zprávu	zpráva	k1gFnSc4	zpráva
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
zprávu	zpráva	k1gFnSc4	zpráva
FBI	FBI	kA	FBI
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
zpráva	zpráva	k1gFnSc1	zpráva
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vycházela	vycházet	k5eAaImAgFnS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
HSCA	HSCA	kA	HSCA
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
kritiku	kritika	k1gFnSc4	kritika
obou	dva	k4xCgFnPc2	dva
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
prý	prý	k9	prý
dostatečně	dostatečně	k6eAd1	dostatečně
neprověřily	prověřit	k5eNaPmAgFnP	prověřit
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Oswald	Oswald	k1gMnSc1	Oswald
článkem	článek	k1gInSc7	článek
spiknutí	spiknutí	k1gNnSc2	spiknutí
proti	proti	k7c3	proti
prezidentovi	prezident	k1gMnSc3	prezident
Kennedymu	Kennedym	k1gInSc2	Kennedym
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
komise	komise	k1gFnSc1	komise
dále	daleko	k6eAd2	daleko
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
FBI	FBI	kA	FBI
silně	silně	k6eAd1	silně
podcenila	podcenit	k5eAaPmAgFnS	podcenit
možnost	možnost	k1gFnSc4	možnost
spiknutí	spiknutí	k1gNnSc2	spiknutí
proti	proti	k7c3	proti
Kennedymu	Kennedymum	k1gNnSc3	Kennedymum
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
možnosti	možnost	k1gFnSc2	možnost
spiknutí	spiknutí	k1gNnSc1	spiknutí
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgInPc1d1	omezený
a	a	k8xC	a
dostupné	dostupný	k2eAgInPc1d1	dostupný
důkazy	důkaz	k1gInPc1	důkaz
a	a	k8xC	a
zdroje	zdroj	k1gInPc1	zdroj
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
nedostatečně	dostatečně	k6eNd1	dostatečně
prověřeny	prověřen	k2eAgFnPc4d1	prověřena
<g/>
.	.	kIx.	.
</s>
<s>
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
též	též	k6eAd1	též
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Subjekt	subjekt	k1gInSc1	subjekt
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
věnována	věnovat	k5eAaImNgFnS	věnovat
největší	veliký	k2eAgFnSc1d3	veliký
pozornost	pozornost	k1gFnSc1	pozornost
během	během	k7c2	během
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
,	,	kIx,	,
Oswaldovi	Oswald	k1gMnSc3	Oswald
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
úloze	úloha	k1gFnSc3	úloha
v	v	k7c6	v
případu	případ	k1gInSc6	případ
a	a	k8xC	a
možnému	možný	k2eAgNnSc3d1	možné
zapojení	zapojení	k1gNnSc3	zapojení
do	do	k7c2	do
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
dostatečně	dostatečně	k6eAd1	dostatečně
prověřen	prověřit	k5eAaPmNgInS	prověřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dle	dle	k7c2	dle
HSCA	HSCA	kA	HSCA
též	též	k6eAd1	též
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
dostatečně	dostatečně	k6eAd1	dostatečně
neprošetřila	prošetřit	k5eNaPmAgFnS	prošetřit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Oswald	Oswald	k1gInSc4	Oswald
jednal	jednat	k5eAaImAgMnS	jednat
sám	sám	k3xTgMnSc1	sám
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nějaké	nějaký	k3yIgFnSc2	nějaký
skupiny	skupina	k1gFnSc2	skupina
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
nebylo	být	k5eNaImAgNnS	být
Oswaldovo	Oswaldův	k2eAgNnSc1d1	Oswaldovo
jednání	jednání	k1gNnSc1	jednání
vyústěním	vyústění	k1gNnSc7	vyústění
aktivit	aktivita	k1gFnPc2	aktivita
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
HSCA	HSCA	kA	HSCA
též	též	k9	též
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lee	Lea	k1gFnSc3	Lea
Harvey	Harvea	k1gFnSc2	Harvea
Oswald	Oswalda	k1gFnPc2	Oswalda
skutečně	skutečně	k6eAd1	skutečně
vypálil	vypálit	k5eAaPmAgMnS	vypálit
tři	tři	k4xCgFnPc4	tři
střely	střela	k1gFnPc4	střela
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
komise	komise	k1gFnSc2	komise
zasáhly	zasáhnout	k5eAaPmAgInP	zasáhnout
prezidenta	prezident	k1gMnSc2	prezident
střely	střela	k1gFnSc2	střela
č.	č.	k?	č.
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
HSCA	HSCA	kA	HSCA
uvedla	uvést	k5eAaPmAgFnS	uvést
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
atentátu	atentát	k1gInSc2	atentát
stálo	stát	k5eAaImAgNnS	stát
spiknutí	spiknutí	k1gNnSc1	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
HSCA	HSCA	kA	HSCA
padly	padnout	k5eAaImAgInP	padnout
během	běh	k1gInSc7	běh
atentátu	atentát	k1gInSc2	atentát
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgInPc1	čtyři
výstřely	výstřel	k1gInPc1	výstřel
<g/>
,	,	kIx,	,
Oswald	Oswald	k1gInSc1	Oswald
měl	mít	k5eAaImAgInS	mít
vystřelit	vystřelit	k5eAaPmF	vystřelit
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
(	(	kIx(	(
<g/>
smrtelná	smrtelný	k2eAgNnPc4d1	smrtelné
<g/>
)	)	kIx)	)
a	a	k8xC	a
neznámá	známý	k2eNgFnSc1d1	neznámá
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
vystřelit	vystřelit	k5eAaPmF	vystřelit
třetí	třetí	k4xOgFnSc4	třetí
střelu	střela	k1gFnSc4	střela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ovšem	ovšem	k9	ovšem
Kennedyho	Kennedy	k1gMnSc2	Kennedy
minula	minulo	k1gNnSc2	minulo
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
HSCA	HSCA	kA	HSCA
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
G.	G.	kA	G.
Robert	Robert	k1gMnSc1	Robert
Blakey	Blakey	k1gInPc4	Blakey
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgInPc4	svůj
závěry	závěr	k1gInPc4	závěr
postavila	postavit	k5eAaPmAgFnS	postavit
především	především	k9	především
na	na	k7c6	na
průzkumu	průzkum	k1gInSc6	průzkum
magnetofonových	magnetofonový	k2eAgFnPc2d1	magnetofonová
pásek	páska	k1gFnPc2	páska
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byla	být	k5eAaImAgFnS	být
nahrán	nahrát	k5eAaPmNgInS	nahrát
radiový	radiový	k2eAgInSc1d1	radiový
provoz	provoz	k1gInSc1	provoz
dallaské	dallaský	k2eAgFnSc2d1	dallaská
policie	policie	k1gFnSc2	policie
z	z	k7c2	z
inkriminovaného	inkriminovaný	k2eAgInSc2d1	inkriminovaný
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Představitel	představitel	k1gMnSc1	představitel
komise	komise	k1gFnSc2	komise
HSCA	HSCA	kA	HSCA
Robert	Robert	k1gMnSc1	Robert
Blakey	Blakea	k1gFnSc2	Blakea
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
zpráva	zpráva	k1gFnSc1	zpráva
vycházela	vycházet	k5eAaImAgFnS	vycházet
jednak	jednak	k8xC	jednak
ze	z	k7c2	z
svědeckých	svědecký	k2eAgFnPc2d1	svědecká
výpovědí	výpověď	k1gFnPc2	výpověď
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
také	také	k9	také
z	z	k7c2	z
radiových	radiový	k2eAgInPc2d1	radiový
záznamů	záznam	k1gInPc2	záznam
dallaské	dallaský	k2eAgFnSc2d1	dallaská
policie	policie	k1gFnSc2	policie
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
střelci	střelec	k1gMnSc6	střelec
ukrytém	ukrytý	k2eAgMnSc6d1	ukrytý
na	na	k7c6	na
travnatém	travnatý	k2eAgInSc6d1	travnatý
pahorku	pahorek	k1gInSc6	pahorek
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
grassy	grass	k1gInPc4	grass
knoll	knoll	k1gInSc1	knoll
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podpořena	podpořit	k5eAaPmNgFnS	podpořit
záznamem	záznam	k1gInSc7	záznam
zvuku	zvuk	k1gInSc2	zvuk
z	z	k7c2	z
vysílačky	vysílačka	k1gFnSc2	vysílačka
policejního	policejní	k2eAgInSc2d1	policejní
motocyklu	motocykl	k1gInSc2	motocykl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c4	v
inkriminovanou	inkriminovaný	k2eAgFnSc4d1	inkriminovaná
dobu	doba	k1gFnSc4	doba
nacházel	nacházet	k5eAaImAgMnS	nacházet
na	na	k7c4	na
Dealey	Dealey	k1gInPc4	Dealey
Plaza	plaz	k1gMnSc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
podepřena	podepřít	k5eAaPmNgFnS	podepřít
i	i	k9	i
četnými	četný	k2eAgFnPc7d1	četná
výpověďmi	výpověď	k1gFnPc7	výpověď
svědků	svědek	k1gMnPc2	svědek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
poblíž	poblíž	k7c2	poblíž
místa	místo	k1gNnSc2	místo
činu	čin	k1gInSc2	čin
nacházeli	nacházet	k5eAaImAgMnP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
přinejmenším	přinejmenším	k6eAd1	přinejmenším
o	o	k7c6	o
dvaceti	dvacet	k4xCc6	dvacet
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
střelbu	střelba	k1gFnSc4	střelba
slyšeli	slyšet	k5eAaImAgMnP	slyšet
přicházet	přicházet	k5eAaImF	přicházet
právě	právě	k9	právě
od	od	k7c2	od
travnatého	travnatý	k2eAgInSc2d1	travnatý
pahorku	pahorek	k1gInSc2	pahorek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jediným	jediný	k2eAgInSc7d1	jediný
věrohodným	věrohodný	k2eAgInSc7d1	věrohodný
záznamem	záznam	k1gInSc7	záznam
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
HSCA	HSCA	kA	HSCA
podpořila	podpořit	k5eAaPmAgFnS	podpořit
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
druhém	druhý	k4xOgInSc6	druhý
střelci	střelec	k1gMnPc7	střelec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
záznam	záznam	k1gInSc4	záznam
radiového	radiový	k2eAgNnSc2d1	radiové
vysílání	vysílání	k1gNnSc2	vysílání
policejního	policejní	k2eAgInSc2d1	policejní
motocyklu	motocykl	k1gInSc2	motocykl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
nacházel	nacházet	k5eAaImAgMnS	nacházet
na	na	k7c4	na
Dealey	Dealey	k1gInPc4	Dealey
Plaza	plaz	k1gMnSc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Zvukový	zvukový	k2eAgInSc1d1	zvukový
záznam	záznam	k1gInSc1	záznam
byl	být	k5eAaImAgInS	být
veřejnosti	veřejnost	k1gFnSc3	veřejnost
představen	představit	k5eAaPmNgInS	představit
experty	expert	k1gMnPc4	expert
pro	pro	k7c4	pro
akustické	akustický	k2eAgInPc4d1	akustický
jevy	jev	k1gInPc4	jev
Markem	Marek	k1gMnSc7	Marek
R.	R.	kA	R.
Weissem	Weiss	k1gMnSc7	Weiss
a	a	k8xC	a
Ernestem	Ernest	k1gMnSc7	Ernest
Aschkenasym	Aschkenasymum	k1gNnPc2	Aschkenasymum
z	z	k7c2	z
Queens	Queensa	k1gFnPc2	Queensa
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
City	city	k1gNnSc1	city
University	universita	k1gFnSc2	universita
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
známých	známý	k2eAgMnPc2d1	známý
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
svou	svůj	k3xOyFgFnSc7	svůj
teorií	teorie	k1gFnSc7	teorie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
18,5	[number]	k4	18,5
minut	minuta	k1gFnPc2	minuta
trvající	trvající	k2eAgNnSc4d1	trvající
hluché	hluchý	k2eAgNnSc4d1	hluché
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
důkazních	důkazní	k2eAgFnPc6d1	důkazní
páskách	páska	k1gFnPc6	páska
z	z	k7c2	z
Aféry	aféra	k1gFnSc2	aféra
Watergate	Watergat	k1gInSc5	Watergat
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
cíleným	cílený	k2eAgNnSc7d1	cílené
vymazáním	vymazání	k1gNnSc7	vymazání
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
HSCA	HSCA	kA	HSCA
dokončila	dokončit	k5eAaPmAgFnS	dokončit
svou	svůj	k3xOyFgFnSc4	svůj
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
amatérských	amatérský	k2eAgMnPc2d1	amatérský
výzkumníků	výzkumník	k1gMnPc2	výzkumník
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvuk	zvuk	k1gInSc1	zvuk
přisuzovaný	přisuzovaný	k2eAgInSc1d1	přisuzovaný
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
střele	střela	k1gFnSc6	střela
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
zkřížením	zkřížení	k1gNnSc7	zkřížení
signálů	signál	k1gInPc2	signál
z	z	k7c2	z
více	hodně	k6eAd2	hodně
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
odborný	odborný	k2eAgInSc1d1	odborný
panel	panel	k1gInSc1	panel
Národní	národní	k2eAgFnSc2d1	národní
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
NAS	NAS	kA	NAS
<g/>
)	)	kIx)	)
věrohodnost	věrohodnost	k1gFnSc4	věrohodnost
záznamu	záznam	k1gInSc2	záznam
předpokládané	předpokládaný	k2eAgFnSc3d1	předpokládaná
čtvrté	čtvrtá	k1gFnSc3	čtvrtá
střely	střela	k1gFnSc2	střela
neboť	neboť	k8xC	neboť
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prostě	prostě	k9	prostě
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nějaký	nějaký	k3yIgInSc4	nějaký
náhodný	náhodný	k2eAgInSc4d1	náhodný
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
možná	možná	k9	možná
způsobený	způsobený	k2eAgInSc1d1	způsobený
statickou	statický	k2eAgFnSc7d1	statická
elektřinou	elektřina	k1gFnSc7	elektřina
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
překřížením	překřížení	k1gNnSc7	překřížení
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
policejním	policejní	k2eAgInSc7d1	policejní
kanálem	kanál	k1gInSc7	kanál
až	až	k6eAd1	až
minutu	minuta	k1gFnSc4	minuta
po	po	k7c6	po
střelbě	střelba	k1gFnSc6	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Závěry	závěra	k1gFnPc1	závěra
tohoto	tento	k3xDgInSc2	tento
panelu	panel	k1gInSc2	panel
však	však	k9	však
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc3	rok
2001	[number]	k4	2001
odmítnuty	odmítnut	k2eAgInPc4d1	odmítnut
D.	D.	kA	D.
B.	B.	kA	B.
Thomasem	Thomas	k1gMnSc7	Thomas
<g/>
,	,	kIx,	,
vládním	vládní	k2eAgMnSc7d1	vládní
expertem	expert	k1gMnSc7	expert
a	a	k8xC	a
soukromým	soukromý	k2eAgMnSc7d1	soukromý
výzkumníkem	výzkumník	k1gMnSc7	výzkumník
okolností	okolnost	k1gFnPc2	okolnost
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
v	v	k7c6	v
odborném	odborný	k2eAgInSc6d1	odborný
časopise	časopis	k1gInSc6	časopis
Science	Science	k1gFnSc2	Science
&	&	k?	&
Justice	justice	k1gFnSc2	justice
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
jeho	jeho	k3xOp3gInSc1	jeho
článek	článek	k1gInSc1	článek
"	"	kIx"	"
<g/>
Echo	echo	k1gNnSc1	echo
correlation	correlation	k1gInSc4	correlation
analysis	analysis	k1gFnSc2	analysis
and	and	k?	and
the	the	k?	the
acoustic	acoustice	k1gFnPc2	acoustice
evidence	evidence	k1gFnSc1	evidence
in	in	k?	in
the	the	k?	the
Kennedy	Kenneda	k1gMnSc2	Kenneda
assassination	assassination	k1gInSc4	assassination
revisited	revisited	k1gInSc4	revisited
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Analýza	analýza	k1gFnSc1	analýza
zvukových	zvukový	k2eAgFnPc2d1	zvuková
ozvěn	ozvěna	k1gFnPc2	ozvěna
a	a	k8xC	a
akustických	akustický	k2eAgInPc2d1	akustický
důkazů	důkaz	k1gInPc2	důkaz
v	v	k7c6	v
případu	případ	k1gInSc6	případ
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
zrevidována	zrevidován	k2eAgFnSc1d1	zrevidována
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
shledal	shledat	k5eAaPmAgMnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
závěr	závěr	k1gInSc4	závěr
HSCA	HSCA	kA	HSCA
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
druhého	druhý	k4xOgMnSc4	druhý
střelce	střelec	k1gMnPc4	střelec
byl	být	k5eAaImAgMnS	být
správný	správný	k2eAgMnSc1d1	správný
a	a	k8xC	a
zpráva	zpráva	k1gFnSc1	zpráva
NAS	NAS	kA	NAS
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
chybná	chybný	k2eAgFnSc1d1	chybná
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zmíněné	zmíněný	k2eAgFnSc3d1	zmíněná
minutové	minutový	k2eAgFnSc3d1	minutová
"	"	kIx"	"
<g/>
zpoždění	zpoždění	k1gNnSc1	zpoždění
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
přeskočením	přeskočení	k1gNnSc7	přeskočení
záznamové	záznamový	k2eAgFnSc2d1	záznamová
hlavy	hlava	k1gFnSc2	hlava
na	na	k7c4	na
kanál	kanál	k1gInSc4	kanál
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
následné	následný	k2eAgFnSc3d1	následná
mylné	mylný	k2eAgFnSc3d1	mylná
interpretaci	interpretace	k1gFnSc3	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odpovědi	odpověď	k1gFnSc6	odpověď
na	na	k7c4	na
Thomasovy	Thomasův	k2eAgInPc4d1	Thomasův
závěry	závěr	k1gInPc4	závěr
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jiným	jiný	k2eAgMnSc7d1	jiný
výzkumníkem	výzkumník	k1gMnSc7	výzkumník
<g/>
,	,	kIx,	,
Michaelem	Michael	k1gMnSc7	Michael
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Dellem	Dell	k1gInSc7	Dell
<g/>
,	,	kIx,	,
vyslovena	vysloven	k2eAgFnSc1d1	vyslovena
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohý	k2eAgFnPc1d1	mnohá
zprávy	zpráva	k1gFnPc1	zpráva
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
chybné	chybný	k2eAgFnSc2d1	chybná
časové	časový	k2eAgFnSc2d1	časová
linie	linie	k1gFnSc2	linie
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
nelze	lze	k6eNd1	lze
tedy	tedy	k9	tedy
pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gInPc2	on
zcela	zcela	k6eAd1	zcela
prokázat	prokázat	k5eAaPmF	prokázat
existenci	existence	k1gFnSc4	existence
výstřelů	výstřel	k1gInPc2	výstřel
na	na	k7c6	na
zmíněných	zmíněný	k2eAgFnPc6d1	zmíněná
páskách	páska	k1gFnPc6	páska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
americká	americký	k2eAgFnSc1d1	americká
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
ABC	ABC	kA	ABC
News	Newsa	k1gFnPc2	Newsa
svůj	svůj	k3xOyFgInSc4	svůj
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
pořad	pořad	k1gInSc4	pořad
s	s	k7c7	s
názvem	název	k1gInSc7	název
Peter	Petra	k1gFnPc2	Petra
Jennings	Jenningsa	k1gFnPc2	Jenningsa
Reporting	Reporting	k1gInSc1	Reporting
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Kennedy	Kenneda	k1gMnSc2	Kenneda
Assassination	Assassination	k1gInSc1	Assassination
–	–	k?	–
Beyond	Beyond	k1gInSc1	Beyond
Conspiracy	Conspiraca	k1gMnSc2	Conspiraca
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Reportáž	reportáž	k1gFnSc1	reportáž
Petera	Peter	k1gMnSc2	Peter
Jenningse	Jennings	k1gMnSc2	Jennings
<g/>
:	:	kIx,	:
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
–	–	k?	–
pozadí	pozadí	k1gNnSc4	pozadí
spiknutí	spiknutí	k1gNnPc2	spiknutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
představila	představit	k5eAaPmAgFnS	představit
výsledky	výsledek	k1gInPc7	výsledek
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
počítačových	počítačový	k2eAgInPc2d1	počítačový
diagramů	diagram	k1gInPc2	diagram
a	a	k8xC	a
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
Petera	Peter	k1gMnSc2	Peter
K.	K.	kA	K.
Myerse	Myerse	k1gFnPc4	Myerse
došla	dojít	k5eAaPmAgFnS	dojít
ABC	ABC	kA	ABC
News	News	k1gInSc4	News
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvuk	zvuk	k1gInSc1	zvuk
na	na	k7c6	na
pásce	páska	k1gFnSc6	páska
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
výstřel	výstřel	k1gInSc4	výstřel
skrytého	skrytý	k2eAgMnSc2d1	skrytý
střelce	střelec	k1gMnSc2	střelec
byl	být	k5eAaImAgInS	být
chybně	chybně	k6eAd1	chybně
interpretován	interpretovat	k5eAaBmNgInS	interpretovat
a	a	k8xC	a
nemohl	moct	k5eNaImAgInS	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Policista	policista	k1gMnSc1	policista
H.B.	H.B.	k1gMnSc1	H.B.
McLain	McLain	k1gMnSc1	McLain
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnSc2	jehož
vysílačky	vysílačka	k1gFnSc2	vysílačka
pochází	pocházet	k5eAaImIp3nS	pocházet
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
pasáž	pasáž	k1gFnSc1	pasáž
navíc	navíc	k6eAd1	navíc
sám	sám	k3xTgInSc4	sám
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Dealey	Dealey	k1gInPc4	Dealey
Plaza	plaz	k1gMnSc2	plaz
nenacházel	nacházet	k5eNaImAgMnS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
reportéra	reportér	k1gMnSc2	reportér
Petera	Peter	k1gMnSc2	Peter
Jenningse	Jennings	k1gMnSc2	Jennings
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
bodech	bod	k1gInPc6	bod
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
filmového	filmový	k2eAgInSc2d1	filmový
dokumentu	dokument	k1gInSc2	dokument
JFKII	JFKII	kA	JFKII
–	–	k?	–
The	The	k1gMnSc1	The
Bush	Bush	k1gMnSc1	Bush
Connection	Connection	k1gInSc1	Connection
od	od	k7c2	od
producenta	producent	k1gMnSc2	producent
Johna	John	k1gMnSc2	John
Hankeyho	Hankey	k1gMnSc2	Hankey
<g/>
.	.	kIx.	.
</s>
<s>
Hankey	Hanke	k1gMnPc4	Hanke
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Peter	Peter	k1gMnSc1	Peter
Jennings	Jennings	k1gInSc4	Jennings
zaujatý	zaujatý	k2eAgInSc4d1	zaujatý
a	a	k8xC	a
že	že	k8xS	že
záměrně	záměrně	k6eAd1	záměrně
opomíjí	opomíjet	k5eAaImIp3nP	opomíjet
některá	některý	k3yIgNnPc1	některý
podstatná	podstatný	k2eAgNnPc1d1	podstatné
fakta	faktum	k1gNnPc1	faktum
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
filmový	filmový	k2eAgInSc1d1	filmový
záznam	záznam	k1gInSc1	záznam
tiskového	tiskový	k2eAgNnSc2d1	tiskové
prohlášení	prohlášení	k1gNnSc2	prohlášení
Malcolma	Malcolm	k1gMnSc2	Malcolm
Kilduffa	Kilduff	k1gMnSc2	Kilduff
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Kilduff	Kilduff	k1gMnSc1	Kilduff
zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
ukázal	ukázat	k5eAaPmAgInS	ukázat
prstem	prst	k1gInSc7	prst
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
čelo	čelo	k1gNnSc4	čelo
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
rána	rána	k1gFnSc1	rána
Kennedyho	Kennedy	k1gMnSc2	Kennedy
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
zepředu	zepředu	k6eAd1	zepředu
<g/>
.	.	kIx.	.
</s>
<s>
Vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
kapitoly	kapitola	k1gFnSc2	kapitola
Garrisonova	Garrisonův	k2eAgFnSc1d1	Garrisonův
alternativní	alternativní	k2eAgFnSc1d1	alternativní
verze	verze	k1gFnSc1	verze
o	o	k7c4	o
více	hodně	k6eAd2	hodně
střelcích	střelec	k1gMnPc6	střelec
Do	do	k7c2	do
hledáčku	hledáček	k1gInSc2	hledáček
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
neworleanského	worleanský	k2eNgInSc2d1	neworleanský
státního	státní	k2eAgInSc2d1	státní
návladního	návladní	k2eAgInSc2d1	návladní
Jima	Jim	k1gInSc2	Jim
Garrisona	Garrison	k1gMnSc2	Garrison
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
544	[number]	k4	544
Camp	camp	k1gInSc1	camp
Street	Street	k1gInSc4	Street
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleansu	Orleans	k1gInSc2	Orleans
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
sloužil	sloužit	k5eAaImAgMnS	sloužit
ke	k	k7c3	k
schůzkám	schůzka	k1gFnPc3	schůzka
kubánských	kubánský	k2eAgMnPc2d1	kubánský
<g/>
,	,	kIx,	,
proticastrovsky	proticastrovsky	k6eAd1	proticastrovsky
zaměřených	zaměřený	k2eAgMnPc2d1	zaměřený
emigrantů	emigrant	k1gMnPc2	emigrant
a	a	k8xC	a
agentů	agent	k1gMnPc2	agent
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
<g/>
;	;	kIx,	;
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
adrese	adresa	k1gFnSc6	adresa
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
vídán	vídán	k2eAgInSc1d1	vídán
i	i	k8xC	i
Lee	Lea	k1gFnSc6	Lea
H.	H.	kA	H.
Oswald	Oswald	k1gInSc1	Oswald
<g/>
.	.	kIx.	.
</s>
<s>
Oswald	Oswald	k6eAd1	Oswald
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgMnS	mít
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
stýkat	stýkat	k5eAaImF	stýkat
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
velitelem	velitel	k1gMnSc7	velitel
civilní	civilní	k2eAgFnSc2d1	civilní
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
hlídky	hlídka	k1gFnSc2	hlídka
Davidem	David	k1gMnSc7	David
W.	W.	kA	W.
Ferriem	Ferrium	k1gNnSc7	Ferrium
<g/>
.	.	kIx.	.
</s>
<s>
Ferrie	Ferrie	k1gFnSc1	Ferrie
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
plánování	plánování	k1gNnSc4	plánování
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Zátoky	zátoka	k1gFnSc2	zátoka
sviní	svině	k1gFnPc2	svině
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
úzce	úzko	k6eAd1	úzko
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
protikastrovským	protikastrovský	k2eAgNnSc7d1	protikastrovský
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
často	často	k6eAd1	často
vídán	vídat	k5eAaImNgInS	vídat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Sergia	Sergius	k1gMnSc2	Sergius
Archaca-Smithe	Archaca-Smith	k1gMnSc2	Archaca-Smith
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
vedení	vedení	k1gNnSc2	vedení
extremistické	extremistický	k2eAgFnSc2d1	extremistická
organizace	organizace	k1gFnSc2	organizace
kubánských	kubánský	k2eAgMnPc2d1	kubánský
emigrantů	emigrant	k1gMnPc2	emigrant
jménem	jméno	k1gNnSc7	jméno
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
revoluční	revoluční	k2eAgFnSc1d1	revoluční
fronta	fronta	k1gFnSc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
kontakty	kontakt	k1gInPc1	kontakt
Oswalda	Oswald	k1gMnSc2	Oswald
a	a	k8xC	a
Ferrieho	Ferrie	k1gMnSc2	Ferrie
vedly	vést	k5eAaImAgFnP	vést
Garrisona	Garrisona	k1gFnSc1	Garrisona
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
vlivného	vlivný	k2eAgMnSc2d1	vlivný
neworleanského	worleanský	k2eNgMnSc2d1	neworleanský
obchodníka	obchodník	k1gMnSc2	obchodník
Claye	Claye	k1gNnSc2	Claye
L.	L.	kA	L.
Shawa	Shawa	k1gMnSc1	Shawa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
vídán	vídat	k5eAaImNgInS	vídat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Davida	David	k1gMnSc2	David
Ferrieho	Ferrie	k1gMnSc2	Ferrie
a	a	k8xC	a
kterého	který	k3yIgMnSc4	který
podezříval	podezřívat	k5eAaImAgMnS	podezřívat
jako	jako	k8xC	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
možných	možný	k2eAgMnPc2d1	možný
spiklenců	spiklenec	k1gMnPc2	spiklenec
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Zapojení	zapojení	k1gNnSc1	zapojení
samotného	samotný	k2eAgMnSc2d1	samotný
Ferrieho	Ferrie	k1gMnSc2	Ferrie
do	do	k7c2	do
spiknutí	spiknutí	k1gNnSc2	spiknutí
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
přímo	přímo	k6eAd1	přímo
prokázat	prokázat	k5eAaPmF	prokázat
nepodařilo	podařit	k5eNaPmAgNnS	podařit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
Garrison	Garrison	k1gMnSc1	Garrison
začal	začít	k5eAaPmAgMnS	začít
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
již	již	k9	již
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Ferrieho	Ferrie	k1gMnSc2	Ferrie
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
požádal	požádat	k5eAaPmAgInS	požádat
o	o	k7c6	o
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
proč	proč	k6eAd1	proč
jel	jet	k5eAaImAgMnS	jet
v	v	k7c4	v
den	den	k1gInSc4	den
atentátu	atentát	k1gInSc2	atentát
do	do	k7c2	do
Houstounu	Houstoun	k1gInSc2	Houstoun
během	během	k7c2	během
silné	silný	k2eAgFnSc2d1	silná
bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
chtěl	chtít	k5eAaImAgMnS	chtít
jen	jen	k9	jen
zabruslit	zabruslit	k5eAaPmF	zabruslit
na	na	k7c6	na
místním	místní	k2eAgInSc6d1	místní
umělém	umělý	k2eAgInSc6d1	umělý
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Garrison	Garrison	k1gNnSc4	Garrison
jej	on	k3xPp3gInSc2	on
sice	sice	k8xC	sice
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
podezíral	podezírat	k5eAaImAgMnS	podezírat
ze	z	k7c2	z
zapojení	zapojení	k1gNnSc2	zapojení
do	do	k7c2	do
spiknutí	spiknutí	k1gNnSc2	spiknutí
proti	proti	k7c3	proti
Kennedymu	Kennedymum	k1gNnSc3	Kennedymum
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
si	se	k3xPyFc3	se
však	však	k9	však
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosavadní	dosavadní	k2eAgNnPc1d1	dosavadní
zjištění	zjištění	k1gNnPc1	zjištění
a	a	k8xC	a
důkazy	důkaz	k1gInPc1	důkaz
neprokazují	prokazovat	k5eNaImIp3nP	prokazovat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
Ferrieho	Ferrie	k1gMnSc2	Ferrie
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
výslechy	výslech	k1gInPc4	výslech
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
provést	provést	k5eAaPmF	provést
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
mrtev	mrtev	k2eAgInSc1d1	mrtev
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
apartmánu	apartmán	k1gInSc6	apartmán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
prázdnými	prázdný	k2eAgFnPc7d1	prázdná
lahvičkami	lahvička	k1gFnPc7	lahvička
od	od	k7c2	od
léků	lék	k1gInPc2	lék
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
látky	látka	k1gFnSc2	látka
Proloid	Proloid	k1gInSc1	Proloid
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
droga	droga	k1gFnSc1	droga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pozření	pozření	k1gNnSc2	pozření
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
krvácení	krvácení	k1gNnSc4	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
vše	všechen	k3xTgNnSc1	všechen
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
jako	jako	k9	jako
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
,	,	kIx,	,
uvedla	uvést	k5eAaPmAgFnS	uvést
posléze	posléze	k6eAd1	posléze
pitevní	pitevní	k2eAgFnSc1d1	pitevní
zpráva	zpráva	k1gFnSc1	zpráva
nález	nález	k1gInSc4	nález
pohmožděniny	pohmožděnina	k1gFnSc2	pohmožděnina
na	na	k7c4	na
Ferrieho	Ferrie	k1gMnSc4	Ferrie
spodním	spodní	k2eAgInPc3d1	spodní
rtu	ret	k1gInSc2	ret
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
naznačovala	naznačovat	k5eAaImAgFnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
spolknutí	spolknutí	k1gNnSc3	spolknutí
léků	lék	k1gInPc2	lék
donucen	donutit	k5eAaPmNgInS	donutit
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Garrison	Garrison	k1gMnSc1	Garrison
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
během	během	k7c2	během
šetření	šetření	k1gNnSc2	šetření
Davida	David	k1gMnSc4	David
Ferrieho	Ferrie	k1gMnSc4	Ferrie
střetával	střetávat	k5eAaImAgMnS	střetávat
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Clay	Claa	k1gFnSc2	Claa
Bertrand	Bertrand	k1gInSc1	Bertrand
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
jistý	jistý	k2eAgMnSc1d1	jistý
Ferrieho	Ferrie	k1gMnSc4	Ferrie
obchodní	obchodní	k2eAgMnSc1d1	obchodní
partner	partner	k1gMnSc1	partner
<g/>
.	.	kIx.	.
</s>
<s>
Totožnost	totožnost	k1gFnSc4	totožnost
tohoto	tento	k3xDgMnSc2	tento
muže	muž	k1gMnSc2	muž
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
neznámá	známý	k2eNgFnSc1d1	neznámá
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Garrison	Garrison	k1gMnSc1	Garrison
dověděl	dovědět	k5eAaPmAgMnS	dovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
právě	právě	k9	právě
o	o	k7c4	o
pseudonym	pseudonym	k1gInSc4	pseudonym
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
neworleanského	worleanský	k2eNgMnSc2d1	neworleanský
obchodníka	obchodník	k1gMnSc2	obchodník
Claye	Clay	k1gMnSc2	Clay
L.	L.	kA	L.
Shawa	Shawus	k1gMnSc2	Shawus
<g/>
.	.	kIx.	.
</s>
<s>
Clay	Cla	k2eAgFnPc4d1	Cla
Shaw	Shaw	k1gFnPc4	Shaw
působil	působit	k5eAaImAgMnS	působit
během	během	k7c2	během
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
čelních	čelní	k2eAgMnPc2d1	čelní
pracovníků	pracovník	k1gMnPc2	pracovník
Western	Western	kA	Western
Union	union	k1gInSc4	union
Telegraph	Telegrapha	k1gFnPc2	Telegrapha
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
styčný	styčný	k2eAgMnSc1d1	styčný
důstojník	důstojník	k1gMnSc1	důstojník
ve	v	k7c6	v
zpravodajské	zpravodajský	k2eAgFnSc6d1	zpravodajská
službě	služba	k1gFnSc6	služba
OSS	OSS	kA	OSS
(	(	kIx(	(
<g/>
Office	Office	kA	Office
of	of	k?	of
Strategic	Strategic	k1gMnSc1	Strategic
Services	Services	k1gMnSc1	Services
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
štábu	štáb	k1gInSc6	štáb
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
britského	britský	k2eAgMnSc2d1	britský
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
Winstona	Winston	k1gMnSc2	Winston
Churchilla	Churchill	k1gMnSc2	Churchill
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vlivným	vlivný	k2eAgMnSc7d1	vlivný
obchodníkem	obchodník	k1gMnSc7	obchodník
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
realit	realita	k1gFnPc2	realita
a	a	k8xC	a
podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
Garrisona	Garrison	k1gMnSc2	Garrison
dále	daleko	k6eAd2	daleko
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
italské	italský	k2eAgFnSc2d1	italská
finanční	finanční	k2eAgFnSc2d1	finanční
společnosti	společnost	k1gFnSc2	společnost
Centro	Centro	k1gNnSc1	Centro
Mondiale	Mondiala	k1gFnSc3	Mondiala
Commerciale	Commerciala	k1gFnSc3	Commerciala
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
údajně	údajně	k6eAd1	údajně
sloužila	sloužit	k5eAaImAgFnS	sloužit
CIA	CIA	kA	CIA
jako	jako	k8xS	jako
úložiště	úložiště	k1gNnSc2	úložiště
peněz	peníze	k1gInPc2	peníze
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
financování	financování	k1gNnSc3	financování
politicko-špionážních	politicko-špionážní	k2eAgFnPc2d1	politicko-špionážní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
později	pozdě	k6eAd2	pozdě
fúzovala	fúzovat	k5eAaBmAgFnS	fúzovat
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Schlumberger	Schlumbergra	k1gFnPc2	Schlumbergra
Tools	Toolsa	k1gFnPc2	Toolsa
Company	Compana	k1gFnSc2	Compana
z	z	k7c2	z
Louisiany	Louisiana	k1gFnSc2	Louisiana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dodávala	dodávat	k5eAaImAgFnS	dodávat
protikastrovským	protikastrovský	k2eAgFnPc3d1	protikastrovský
exilovým	exilový	k2eAgFnPc3d1	exilová
skupinám	skupina	k1gFnPc3	skupina
zbraně	zbraň	k1gFnSc2	zbraň
a	a	k8xC	a
výstroj	výstroj	k1gFnSc4	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
"	"	kIx"	"
<g/>
Clay	Clay	k1gInPc1	Clay
Bertrand	Bertranda	k1gFnPc2	Bertranda
<g/>
"	"	kIx"	"
najmul	najmout	k5eAaPmAgMnS	najmout
Shaw	Shaw	k1gMnSc1	Shaw
neworleanského	worleanský	k2eNgMnSc2d1	neworleanský
advokáta	advokát	k1gMnSc2	advokát
Deana	Dean	k1gMnSc4	Dean
Andrewse	Andrews	k1gMnSc4	Andrews
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
obhajovat	obhajovat	k5eAaImF	obhajovat
L.	L.	kA	L.
H.	H.	kA	H.
Oswalda	Oswalda	k1gMnSc1	Oswalda
u	u	k7c2	u
případného	případný	k2eAgInSc2d1	případný
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Garrison	Garrison	k1gInSc1	Garrison
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
nebyl	být	k5eNaImAgInS	být
zcela	zcela	k6eAd1	zcela
jistý	jistý	k2eAgInSc1d1	jistý
Shawovým	Shawův	k2eAgNnSc7d1	Shawův
zapojením	zapojení	k1gNnSc7	zapojení
do	do	k7c2	do
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
věděl	vědět	k5eAaImAgMnS	vědět
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
že	že	k8xS	že
zcela	zcela	k6eAd1	zcela
evidentně	evidentně	k6eAd1	evidentně
lhal	lhát	k5eAaImAgMnS	lhát
svým	svůj	k3xOyFgNnPc3	svůj
tvrzením	tvrzení	k1gNnPc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Oswalda	Oswald	k1gMnSc2	Oswald
ani	ani	k8xC	ani
Ferrieho	Ferrie	k1gMnSc2	Ferrie
nezná	neznat	k5eAaImIp3nS	neznat
a	a	k8xC	a
v	v	k7c6	v
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Existovalo	existovat	k5eAaImAgNnS	existovat
totiž	totiž	k9	totiž
mnoho	mnoho	k4c1	mnoho
svědků	svědek	k1gMnPc2	svědek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jej	on	k3xPp3gMnSc4	on
opakovaně	opakovaně	k6eAd1	opakovaně
viděli	vidět	k5eAaImAgMnP	vidět
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
obou	dva	k4xCgMnPc2	dva
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
Garrison	Garrison	k1gMnSc1	Garrison
předvolal	předvolat	k5eAaPmAgMnS	předvolat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
kanceláře	kancelář	k1gFnSc2	kancelář
Claye	Clay	k1gFnSc2	Clay
Shawa	Shaw	k1gInSc2	Shaw
k	k	k7c3	k
výslechu	výslech	k1gInSc2	výslech
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
den	den	k1gInSc4	den
Božího	boží	k2eAgInSc2d1	boží
hodu	hod	k1gInSc2	hod
velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něj	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
mezi	mezi	k7c7	mezi
čtyřma	čtyři	k4xCgNnPc7	čtyři
očima	oko	k1gNnPc7	oko
<g/>
"	"	kIx"	"
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Shawa	Shawa	k1gMnSc1	Shawa
ze	z	k7c2	z
zapojení	zapojení	k1gNnSc2	zapojení
do	do	k7c2	do
spiknutí	spiknutí	k1gNnSc2	spiknutí
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Kennedyho	Kennedy	k1gMnSc4	Kennedy
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jej	on	k3xPp3gMnSc4	on
obvinil	obvinit	k5eAaPmAgMnS	obvinit
ze	z	k7c2	z
zneužívání	zneužívání	k1gNnSc2	zneužívání
svých	svůj	k3xOyFgFnPc2	svůj
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
konexí	konexe	k1gFnPc2	konexe
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zásobování	zásobování	k1gNnSc2	zásobování
protikastrovských	protikastrovský	k2eAgFnPc2d1	protikastrovský
exilových	exilový	k2eAgFnPc2d1	exilová
komunit	komunita	k1gFnPc2	komunita
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
samotnému	samotný	k2eAgNnSc3d1	samotné
soudnímu	soudní	k2eAgNnSc3d1	soudní
líčení	líčení	k1gNnSc3	líčení
nicméně	nicméně	k8xC	nicméně
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Shaw	Shaw	k1gMnSc1	Shaw
předvolán	předvolat	k5eAaPmNgMnS	předvolat
před	před	k7c4	před
Neworleanský	Neworleanský	k2eAgInSc4d1	Neworleanský
trestní	trestní	k2eAgInSc4d1	trestní
soud	soud	k1gInSc4	soud
a	a	k8xC	a
zde	zde	k6eAd1	zde
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
sděleno	sdělit	k5eAaPmNgNnS	sdělit
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
porota	porota	k1gFnSc1	porota
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
shledala	shledat	k5eAaPmAgFnS	shledat
Claye	Claye	k1gNnSc4	Claye
Shawa	Shawum	k1gNnSc2	Shawum
vinným	vinný	k2eAgInSc7d1	vinný
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
křivé	křivý	k2eAgFnSc2d1	křivá
výpovědi	výpověď	k1gFnSc2	výpověď
a	a	k8xC	a
naznala	naznat	k5eAaPmAgFnS	naznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Garrison	Garrison	k1gInSc1	Garrison
dostatečně	dostatečně	k6eAd1	dostatečně
prokázal	prokázat	k5eAaPmAgInS	prokázat
možnou	možný	k2eAgFnSc4d1	možná
existenci	existence	k1gFnSc4	existence
spiknutí	spiknutí	k1gNnSc2	spiknutí
na	na	k7c4	na
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
,	,	kIx,	,
důkazy	důkaz	k1gInPc7	důkaz
zapojení	zapojení	k1gNnSc2	zapojení
samotného	samotný	k2eAgNnSc2d1	samotné
Shawa	Shawum	k1gNnSc2	Shawum
do	do	k7c2	do
spiknutí	spiknutí	k1gNnSc2	spiknutí
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
ovšem	ovšem	k9	ovšem
dostatečné	dostatečná	k1gFnPc1	dostatečná
nebyly	být	k5eNaImAgFnP	být
a	a	k8xC	a
zprostila	zprostit	k5eAaPmAgFnS	zprostit
jej	on	k3xPp3gNnSc4	on
tedy	tedy	k9	tedy
obvinění	obviněný	k1gMnPc1	obviněný
<g/>
.	.	kIx.	.
</s>
<s>
Garrison	Garrison	k1gMnSc1	Garrison
dále	daleko	k6eAd2	daleko
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
Oswald	Oswald	k1gInSc1	Oswald
v	v	k7c6	v
New	New	k1gFnPc6	New
Orleansu	Orleans	k1gInSc2	Orleans
setkat	setkat	k5eAaPmF	setkat
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
dnů	den	k1gInPc2	den
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
budoucím	budoucí	k2eAgMnSc7d1	budoucí
vrahem	vrah	k1gMnSc7	vrah
<g/>
,	,	kIx,	,
Jackem	Jacek	k1gInSc7	Jacek
Rubym	Rubym	k1gInSc1	Rubym
a	a	k8xC	a
že	že	k8xS	že
si	se	k3xPyFc3	se
tito	tento	k3xDgMnPc1	tento
muži	muž	k1gMnPc1	muž
tudíž	tudíž	k8xC	tudíž
nemohli	moct	k5eNaImAgMnP	moct
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
cizí	cizí	k2eAgFnPc1d1	cizí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Garrisona	Garrison	k1gMnSc2	Garrison
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
schůzce	schůzka	k1gFnSc6	schůzka
naplánován	naplánován	k2eAgInSc4d1	naplánován
Oswaldův	Oswaldův	k2eAgInSc4d1	Oswaldův
útěk	útěk	k1gInSc4	útěk
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
provedení	provedení	k1gNnSc6	provedení
atentátu	atentát	k1gInSc2	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Rubyho	Rubyze	k6eAd1	Rubyze
právní	právní	k2eAgMnSc1d1	právní
zástupce	zástupce	k1gMnSc1	zástupce
Phil	Phil	k1gMnSc1	Phil
Burleson	Burleson	k1gMnSc1	Burleson
však	však	k9	však
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
teorií	teorie	k1gFnSc7	teorie
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
silné	silný	k2eAgFnSc3d1	silná
pochybnosti	pochybnost	k1gFnSc3	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Osudného	osudný	k2eAgInSc2d1	osudný
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
neworleanských	worleanský	k2eNgInPc2d1	neworleanský
barů	bar	k1gInPc2	bar
sejít	sejít	k5eAaPmF	sejít
nad	nad	k7c7	nad
sklenkou	sklenka	k1gFnSc7	sklenka
alkoholu	alkohol	k1gInSc6	alkohol
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
zapojení	zapojený	k2eAgMnPc1d1	zapojený
do	do	k7c2	do
atentátu	atentát	k1gInSc2	atentát
–	–	k?	–
Guy	Guy	k1gMnSc1	Guy
Banister	Banister	k1gMnSc1	Banister
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
příslušník	příslušník	k1gMnSc1	příslušník
FBI	FBI	kA	FBI
a	a	k8xC	a
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
soukromé	soukromý	k2eAgFnSc2d1	soukromá
detektivní	detektivní	k2eAgFnSc2d1	detektivní
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Jack	Jack	k1gMnSc1	Jack
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
zaměstnaný	zaměstnaný	k1gMnSc1	zaměstnaný
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
večera	večer	k1gInSc2	večer
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
silně	silně	k6eAd1	silně
podnapilí	podnapilý	k2eAgMnPc1d1	podnapilý
muži	muž	k1gMnPc1	muž
vydali	vydat	k5eAaPmAgMnP	vydat
z	z	k7c2	z
baru	bar	k1gInSc2	bar
do	do	k7c2	do
Banisterovy	Banisterův	k2eAgFnSc2d1	Banisterův
kanceláře	kancelář	k1gFnSc2	kancelář
a	a	k8xC	a
zde	zde	k6eAd1	zde
mělo	mít	k5eAaImAgNnS	mít
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
muži	muž	k1gMnPc7	muž
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
slovní	slovní	k2eAgFnSc3d1	slovní
a	a	k8xC	a
fyzické	fyzický	k2eAgFnSc3d1	fyzická
potyčce	potyčka	k1gFnSc3	potyčka
<g/>
,	,	kIx,	,
když	když	k8xS	když
Banister	Banister	k1gInSc1	Banister
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Martina	Martin	k1gMnSc2	Martin
z	z	k7c2	z
odcizení	odcizení	k1gNnSc2	odcizení
některých	některý	k3yIgInPc2	některý
důležitých	důležitý	k2eAgInPc2d1	důležitý
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
měl	mít	k5eAaImAgMnS	mít
naopak	naopak	k6eAd1	naopak
Banisterovi	Banister	k1gMnSc3	Banister
připomenout	připomenout	k5eAaPmF	připomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
svědkem	svědek	k1gMnSc7	svědek
návštěv	návštěva	k1gFnPc2	návštěva
prapodivných	prapodivný	k2eAgFnPc2d1	prapodivná
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kanceláři	kancelář	k1gFnSc6	kancelář
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
1963	[number]	k4	1963
<g/>
;	;	kIx,	;
Banister	Banister	k1gMnSc1	Banister
měl	mít	k5eAaImAgMnS	mít
poté	poté	k6eAd1	poté
v	v	k7c6	v
afektu	afekt	k1gInSc6	afekt
Martina	Martina	k1gFnSc1	Martina
praštit	praštit	k5eAaPmF	praštit
rukojetí	rukojeť	k1gFnSc7	rukojeť
svého	svůj	k3xOyFgInSc2	svůj
revolveru	revolver	k1gInSc2	revolver
Magnum	Magnum	k1gInSc1	Magnum
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
podle	podle	k7c2	podle
policejního	policejní	k2eAgInSc2d1	policejní
protokolu	protokol	k1gInSc2	protokol
Martin	Martin	k1gMnSc1	Martin
vykřiknul	vykřiknout	k5eAaPmAgMnS	vykřiknout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Banisterovi	Banisterův	k2eAgMnPc1d1	Banisterův
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chceš	chtít	k5eAaImIp2nS	chtít
mě	já	k3xPp1nSc4	já
zabít	zabít	k5eAaPmF	zabít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsi	být	k5eAaImIp2nS	být
zabil	zabít	k5eAaPmAgMnS	zabít
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
propojení	propojení	k1gNnSc6	propojení
Oswalda	Oswald	k1gMnSc2	Oswald
s	s	k7c7	s
Rubym	Rubym	k1gInSc1	Rubym
ovšem	ovšem	k9	ovšem
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
svědectví	svědectví	k1gNnSc4	svědectví
Beverly	Beverla	k1gFnSc2	Beverla
Oliverové	Oliverová	k1gFnSc2	Oliverová
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
zaměstnankyně	zaměstnankyně	k1gFnSc2	zaměstnankyně
nočního	noční	k2eAgInSc2d1	noční
klubu	klub	k1gInSc2	klub
"	"	kIx"	"
<g/>
Colony	colon	k1gNnPc7	colon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
vypověděla	vypovědět	k5eAaPmAgNnP	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
klubu	klub	k1gInSc6	klub
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
Jackem	Jacek	k1gMnSc7	Jacek
Rubym	Rubym	k1gInSc4	Rubym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jí	jíst	k5eAaImIp3nS	jíst
zde	zde	k6eAd1	zde
představil	představit	k5eAaPmAgMnS	představit
Lee	Lea	k1gFnSc3	Lea
Oswalda	Oswalda	k1gFnSc1	Oswalda
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
Leon	Leona	k1gFnPc2	Leona
Oswald	Oswald	k1gInSc4	Oswald
ze	z	k7c2	z
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jim	on	k3xPp3gMnPc3	on
Garrison	Garrisona	k1gFnPc2	Garrisona
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
pozastavoval	pozastavovat	k5eAaImAgMnS	pozastavovat
nad	nad	k7c7	nad
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
opomíjela	opomíjet	k5eAaImAgFnS	opomíjet
výpovědi	výpověď	k1gFnPc4	výpověď
některých	některý	k3yIgMnPc2	některý
svědků	svědek	k1gMnPc2	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
nebyly	být	k5eNaImAgInP	být
dostatečně	dostatečně	k6eAd1	dostatečně
vzaty	vzat	k2eAgInPc1d1	vzat
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
zcela	zcela	k6eAd1	zcela
ignorovány	ignorován	k2eAgFnPc1d1	ignorována
výpovědi	výpověď	k1gFnPc1	výpověď
následujících	následující	k2eAgMnPc2d1	následující
svědků	svědek	k1gMnPc2	svědek
<g/>
:	:	kIx,	:
Julie	Julie	k1gFnSc1	Julie
Ann	Ann	k1gFnSc2	Ann
Mercerové	Mercerová	k1gFnSc2	Mercerová
<g/>
,	,	kIx,	,
Lee	Lea	k1gFnSc3	Lea
Bowerse	Bowerse	k1gFnSc2	Bowerse
<g/>
,	,	kIx,	,
J.	J.	kA	J.
C.	C.	kA	C.
Price	Price	k1gMnSc1	Price
<g/>
,	,	kIx,	,
S.	S.	kA	S.
M.	M.	kA	M.
Hollanda	Hollanda	k1gFnSc1	Hollanda
<g/>
,	,	kIx,	,
O.	O.	kA	O.
V.	V.	kA	V.
Campbella	Campbella	k1gMnSc1	Campbella
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnSc1	Jamese
Taquea	Taqueus	k1gMnSc2	Taqueus
<g/>
,	,	kIx,	,
Billyho	Billy	k1gMnSc2	Billy
Loveladyho	Lovelady	k1gMnSc2	Lovelady
<g/>
,	,	kIx,	,
Abrahama	Abraham	k1gMnSc2	Abraham
Zaprudera	Zapruder	k1gMnSc2	Zapruder
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
L.	L.	kA	L.
C.	C.	kA	C.
Smithe	Smithe	k1gNnPc2	Smithe
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
dallaského	dallaský	k1gMnSc4	dallaský
šerifa	šerif	k1gMnSc4	šerif
a	a	k8xC	a
Williama	William	k1gMnSc4	William
Newmana	Newman	k1gMnSc4	Newman
<g/>
,	,	kIx,	,
technika	technik	k1gMnSc4	technik
z	z	k7c2	z
Dallasu	Dallas	k1gInSc2	Dallas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zrovna	zrovna	k6eAd1	zrovna
jsme	být	k5eAaImIp1nP	být
stáli	stát	k5eAaImAgMnP	stát
na	na	k7c6	na
obrubníku	obrubník	k1gInSc6	obrubník
a	a	k8xC	a
dívali	dívat	k5eAaImAgMnP	dívat
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgNnSc4	první
přijíždějící	přijíždějící	k2eAgNnSc4d1	přijíždějící
auto	auto	k1gNnSc4	auto
a	a	k8xC	a
náhle	náhle	k6eAd1	náhle
se	se	k3xPyFc4	se
něco	něco	k3yInSc1	něco
ozvalo	ozvat	k5eAaPmAgNnS	ozvat
<g/>
,	,	kIx,	,
asi	asi	k9	asi
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
výstřel	výstřel	k1gInSc1	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
sedící	sedící	k2eAgFnSc1d1	sedící
na	na	k7c6	na
sedadle	sedadlo	k1gNnSc6	sedadlo
najednou	najednou	k6eAd1	najednou
jako	jako	k9	jako
kdyby	kdyby	kYmCp3nS	kdyby
povyskočil	povyskočit	k5eAaPmAgMnS	povyskočit
<g/>
,	,	kIx,	,
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
nějaká	nějaký	k3yIgFnSc1	nějaký
oslavná	oslavný	k2eAgFnSc1d1	oslavná
petarda	petarda	k1gFnSc1	petarda
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
limuzína	limuzína	k1gFnSc1	limuzína
před	před	k7c7	před
námi	my	k3xPp1nPc7	my
a	a	k8xC	a
já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
díval	dívat	k5eAaImAgMnS	dívat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gNnSc3	on
najednou	najednou	k6eAd1	najednou
odstřelili	odstřelit	k5eAaPmAgMnP	odstřelit
celou	celý	k2eAgFnSc4d1	celá
půlku	půlka	k1gFnSc4	půlka
hlavy	hlava	k1gFnSc2	hlava
i	i	k9	i
s	s	k7c7	s
uchem	ucho	k1gNnSc7	ucho
<g/>
...	...	k?	...
pak	pak	k6eAd1	pak
jsme	být	k5eAaImIp1nP	být
všichni	všechen	k3xTgMnPc1	všechen
padli	padnout	k5eAaImAgMnP	padnout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
dráze	dráha	k1gFnSc6	dráha
střelby	střelba	k1gFnSc2	střelba
<g/>
...	...	k?	...
<g/>
<g />
.	.	kIx.	.
</s>
<s>
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
střela	střela	k1gFnSc1	střela
byla	být	k5eAaImAgFnS	být
vypálena	vypálit	k5eAaPmNgFnS	vypálit
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
za	za	k7c7	za
mnou	já	k3xPp1nSc7	já
<g/>
,	,	kIx,	,
nevzpomínám	vzpomínat	k5eNaImIp1nS	vzpomínat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mne	já	k3xPp1nSc4	já
nějak	nějak	k6eAd1	nějak
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
sklad	sklad	k1gInSc4	sklad
učebnic	učebnice	k1gFnPc2	učebnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
úleku	úlek	k1gInSc6	úlek
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
díval	dívat	k5eAaImAgMnS	dívat
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
na	na	k7c4	na
travnatý	travnatý	k2eAgInSc4d1	travnatý
pahorek	pahorek	k1gInSc4	pahorek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Assassination	Assassination	k1gInSc1	Assassination
Records	Recordsa	k1gFnPc2	Recordsa
Review	Review	k1gMnSc1	Review
Board	Board	k1gMnSc1	Board
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
záznamů	záznam	k1gInPc2	záznam
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
atentátu	atentát	k1gInSc3	atentát
<g/>
)	)	kIx)	)
neměl	mít	k5eNaImAgInS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
přezkoumávat	přezkoumávat	k5eAaImF	přezkoumávat
fakta	faktum	k1gNnPc4	faktum
nebo	nebo	k8xC	nebo
vytvářet	vytvářet	k5eAaImF	vytvářet
jakékoliv	jakýkoliv	k3yIgInPc4	jakýkoliv
závěry	závěr	k1gInPc4	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zajistit	zajistit	k5eAaPmF	zajistit
postupné	postupný	k2eAgNnSc4d1	postupné
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
všech	všecek	k3xTgInPc2	všecek
dokumentů	dokument	k1gInPc2	dokument
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
veřejnost	veřejnost	k1gFnSc1	veřejnost
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
mohla	moct	k5eAaImAgNnP	moct
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
vytvořit	vytvořit	k5eAaPmF	vytvořit
závěry	závěr	k1gInPc4	závěr
vlastní	vlastní	k2eAgInPc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
ARRB	ARRB	kA	ARRB
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1992	[number]	k4	1992
až	až	k9	až
1998	[number]	k4	1998
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
000	[number]	k4	000
různých	různý	k2eAgInPc2d1	různý
dokumentů	dokument	k1gInPc2	dokument
o	o	k7c6	o
souhrnném	souhrnný	k2eAgInSc6d1	souhrnný
rozsahu	rozsah	k1gInSc6	rozsah
cca	cca	kA	cca
4	[number]	k4	4
milionů	milion	k4xCgInPc2	milion
stran	strana	k1gFnPc2	strana
A	a	k8xC	a
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgNnSc1d1	chráněné
lhůtou	lhůta	k1gFnSc7	lhůta
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
Warrenovy	Warrenův	k2eAgFnSc2d1	Warrenova
komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vyšetření	vyšetření	k1gNnSc4	vyšetření
atentátu	atentát	k1gInSc2	atentát
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
vládních	vládní	k2eAgFnPc2d1	vládní
agentur	agentura	k1gFnPc2	agentura
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
atentát	atentát	k1gInSc1	atentát
byl	být	k5eAaImAgInS	být
spáchán	spáchán	k2eAgInSc1d1	spáchán
Lee	Lea	k1gFnSc3	Lea
Harvey	Harvea	k1gFnSc2	Harvea
Oswaldem	Oswald	k1gMnSc7	Oswald
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
příslušníkem	příslušník	k1gMnSc7	příslušník
Námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
služby	služba	k1gFnSc2	služba
se	se	k3xPyFc4	se
Oswald	Oswald	k1gMnSc1	Oswald
<g/>
,	,	kIx,	,
inklinující	inklinující	k2eAgMnSc1d1	inklinující
ke	k	k7c3	k
komunismu	komunismus	k1gInSc3	komunismus
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
podezřelý	podezřelý	k2eAgMnSc1d1	podezřelý
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nestanul	stanout	k5eNaPmAgMnS	stanout
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
zatčení	zatčení	k1gNnSc6	zatčení
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
Jackem	Jacek	k1gInSc7	Jacek
Leonem	Leo	k1gMnSc7	Leo
Rubinsteinem	Rubinstein	k1gMnSc7	Rubinstein
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
tak	tak	k9	tak
zapříčinil	zapříčinit	k5eAaPmAgInS	zapříčinit
vznik	vznik	k1gInSc1	vznik
mnoha	mnoho	k4c2	mnoho
podezření	podezření	k1gNnPc2	podezření
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
konspiračních	konspirační	k2eAgFnPc2d1	konspirační
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
motivů	motiv	k1gInPc2	motiv
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
značné	značný	k2eAgNnSc1d1	značné
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c4	mezi
Kennedym	Kennedym	k1gInSc4	Kennedym
a	a	k8xC	a
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgInS	stát
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1961	[number]	k4	1961
Allen	Allen	k1gMnSc1	Allen
Dulles	Dulles	k1gMnSc1	Dulles
<g/>
,	,	kIx,	,
datované	datovaný	k2eAgFnSc6d1	datovaná
od	od	k7c2	od
neúspěšného	úspěšný	k2eNgNnSc2d1	neúspěšné
vylodění	vylodění	k1gNnSc2	vylodění
v	v	k7c6	v
Zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
negativní	negativní	k2eAgInSc4d1	negativní
postoj	postoj	k1gInSc4	postoj
vůči	vůči	k7c3	vůči
CIA	CIA	kA	CIA
Kennedy	Kenned	k1gMnPc4	Kenned
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
výrokem	výrok	k1gInSc7	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejraději	rád	k6eAd3	rád
bych	by	kYmCp1nS	by
CIA	CIA	kA	CIA
rozbil	rozbít	k5eAaPmAgInS	rozbít
na	na	k7c4	na
tisíc	tisíc	k4xCgInSc4	tisíc
kousků	kousek	k1gInPc2	kousek
a	a	k8xC	a
rozfoukal	rozfoukat	k5eAaPmAgMnS	rozfoukat
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
větru	vítr	k1gInSc6	vítr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Od	od	k7c2	od
neúspěchu	neúspěch	k1gInSc2	neúspěch
v	v	k7c6	v
Zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
i	i	k9	i
další	další	k2eAgInSc4d1	další
možný	možný	k2eAgInSc4d1	možný
motiv	motiv	k1gInSc4	motiv
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
odpor	odpor	k1gInSc1	odpor
vůči	vůči	k7c3	vůči
Kennedymu	Kennedymum	k1gNnSc3	Kennedymum
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
proticastrovských	proticastrovský	k2eAgFnPc2d1	proticastrovská
kubánských	kubánský	k2eAgFnPc2d1	kubánská
exilových	exilový	k2eAgFnPc2d1	exilová
komunit	komunita	k1gFnPc2	komunita
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgMnPc2	který
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c6	v
nemilost	nemilost	k1gFnSc4	nemilost
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
nepodnikat	podnikat	k5eNaImF	podnikat
žádné	žádný	k3yNgFnPc4	žádný
další	další	k2eAgFnPc4d1	další
vojenské	vojenský	k2eAgFnPc4d1	vojenská
akce	akce	k1gFnPc4	akce
vůči	vůči	k7c3	vůči
Castrovu	Castrův	k2eAgInSc3d1	Castrův
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
navíc	navíc	k6eAd1	navíc
nechal	nechat	k5eAaPmAgInS	nechat
ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
uzavřít	uzavřít	k5eAaPmF	uzavřít
několik	několik	k4yIc4	několik
jejich	jejich	k3xOp3gNnPc2	jejich
výcvikových	výcvikový	k2eAgNnPc2d1	výcvikové
středisek	středisko	k1gNnPc2	středisko
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleansu	Orleans	k1gInSc2	Orleans
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
místních	místní	k2eAgFnPc2d1	místní
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
složek	složka	k1gFnPc2	složka
a	a	k8xC	a
FBI	FBI	kA	FBI
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgNnP	být
tato	tento	k3xDgNnPc1	tento
výcviková	výcvikový	k2eAgNnPc1d1	výcvikové
střediska	středisko	k1gNnPc1	středisko
financována	financován	k2eAgNnPc1d1	financováno
a	a	k8xC	a
organizována	organizován	k2eAgFnSc1d1	organizována
již	již	k6eAd1	již
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
ovšem	ovšem	k9	ovšem
nezůstala	zůstat	k5eNaPmAgFnS	zůstat
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
ve	v	k7c4	v
12.32	[number]	k4	12.32
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pouhé	pouhý	k2eAgFnPc4d1	pouhá
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
totálnímu	totální	k2eAgInSc3d1	totální
výpadku	výpadek	k1gInSc3	výpadek
telefonních	telefonní	k2eAgFnPc2d1	telefonní
linek	linka	k1gFnPc2	linka
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc6	D.C.
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
celou	celý	k2eAgFnSc4d1	celá
následující	následující	k2eAgFnSc4d1	následující
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
tvrzení	tvrzení	k1gNnPc2	tvrzení
autorů	autor	k1gMnPc2	autor
knihy	kniha	k1gFnSc2	kniha
JFK	JFK	kA	JFK
<g/>
:	:	kIx,	:
conspiracy	conspiraca	k1gMnSc2	conspiraca
of	of	k?	of
silence	silenka	k1gFnSc6	silenka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
však	však	k9	však
telefonní	telefonní	k2eAgFnSc1d1	telefonní
linka	linka	k1gFnSc1	linka
ředitele	ředitel	k1gMnSc2	ředitel
FBI	FBI	kA	FBI
Edgara	Edgar	k1gMnSc2	Edgar
Hoovera	Hoover	k1gMnSc2	Hoover
zůstala	zůstat	k5eAaPmAgFnS	zůstat
"	"	kIx"	"
<g/>
záhadně	záhadně	k6eAd1	záhadně
<g/>
"	"	kIx"	"
funkční	funkční	k2eAgFnPc4d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
si	se	k3xPyFc3	se
CIA	CIA	kA	CIA
velmi	velmi	k6eAd1	velmi
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
i	i	k9	i
svým	svůj	k3xOyFgInSc7	svůj
záměrem	záměr	k1gInSc7	záměr
reorganizovat	reorganizovat	k5eAaBmF	reorganizovat
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
přesunout	přesunout	k5eAaPmF	přesunout
pod	pod	k7c4	pod
přímou	přímý	k2eAgFnSc4d1	přímá
kontrolu	kontrola	k1gFnSc4	kontrola
Armády	armáda	k1gFnSc2	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
CIA	CIA	kA	CIA
spatřoval	spatřovat	k5eAaImAgInS	spatřovat
jakýsi	jakýsi	k3yIgInSc4	jakýsi
"	"	kIx"	"
<g/>
stát	stát	k1gInSc4	stát
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nikým	nikdo	k3yNnSc7	nikdo
kontrolován	kontrolovat	k5eAaImNgInS	kontrolovat
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
si	se	k3xPyFc3	se
"	"	kIx"	"
<g/>
dělat	dělat	k5eAaImF	dělat
co	co	k3yQnSc1	co
chce	chtít	k5eAaImIp3nS	chtít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
spekulaci	spekulace	k1gFnSc4	spekulace
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
motivu	motiv	k1gInSc6	motiv
atentátu	atentát	k1gInSc2	atentát
přináší	přinášet	k5eAaImIp3nS	přinášet
nově	nově	k6eAd1	nově
odhalený	odhalený	k2eAgInSc1d1	odhalený
dopis	dopis	k1gInSc1	dopis
ze	z	k7c2	z
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Kennedy	Kenned	k1gMnPc4	Kenned
zaslal	zaslat	k5eAaPmAgMnS	zaslat
nástupci	nástupce	k1gMnPc1	nástupce
Allena	Allen	k1gMnSc2	Allen
Dullese	Dullese	k1gFnSc2	Dullese
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ředitele	ředitel	k1gMnSc2	ředitel
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
Johnovi	John	k1gMnSc3	John
A.	A.	kA	A.
McConemu	McConem	k1gInSc6	McConem
<g/>
.	.	kIx.	.
</s>
<s>
Pouhých	pouhý	k2eAgInPc2d1	pouhý
deset	deset	k4xCc4	deset
dnů	den	k1gInPc2	den
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
Kennedy	Kenned	k1gMnPc4	Kenned
od	od	k7c2	od
CIA	CIA	kA	CIA
žádá	žádat	k5eAaImIp3nS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
veškeré	veškerý	k3xTgInPc4	veškerý
dokumenty	dokument	k1gInPc4	dokument
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
pozorování	pozorování	k1gNnSc2	pozorování
neidentifikovatelných	identifikovatelný	k2eNgInPc2d1	neidentifikovatelný
létajících	létající	k2eAgInPc2d1	létající
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
UFO	UFO	kA	UFO
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
sovětské	sovětský	k2eAgInPc1d1	sovětský
úřady	úřad	k1gInPc1	úřad
mohly	moct	k5eAaImAgInP	moct
různá	různý	k2eAgNnPc1d1	různé
tajemná	tajemný	k2eAgNnPc1d1	tajemné
světla	světlo	k1gNnPc1	světlo
a	a	k8xC	a
objekty	objekt	k1gInPc1	objekt
nad	nad	k7c7	nad
svým	svůj	k3xOyFgNnSc7	svůj
územím	území	k1gNnSc7	území
mylně	mylně	k6eAd1	mylně
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
americké	americký	k2eAgInPc4d1	americký
špionážní	špionážní	k2eAgInPc4d1	špionážní
letouny	letoun	k1gInPc4	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Kennedy	Kenned	k1gMnPc4	Kenned
chtěl	chtít	k5eAaImAgMnS	chtít
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
poskytnout	poskytnout	k5eAaPmF	poskytnout
sovětům	sovět	k1gInPc3	sovět
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
problematice	problematika	k1gFnSc6	problematika
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ovšem	ovšem	k9	ovšem
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
interního	interní	k2eAgInSc2d1	interní
oběžníku	oběžník	k1gInSc2	oběžník
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
vydaného	vydaný	k2eAgInSc2d1	vydaný
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
požadavku	požadavek	k1gInSc2	požadavek
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
Kennedyho	Kennedy	k1gMnSc2	Kennedy
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
těchto	tento	k3xDgInPc2	tento
tajných	tajný	k2eAgInPc2d1	tajný
dokumentů	dokument	k1gInPc2	dokument
zcela	zcela	k6eAd1	zcela
mizivé	mizivý	k2eAgFnPc1d1	mizivá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oběžníku	oběžník	k1gInSc6	oběžník
totiž	totiž	k9	totiž
McCone	McCon	k1gInSc5	McCon
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
znepokojení	znepokojení	k1gNnSc4	znepokojení
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
Lancer	Lancer	k1gInSc1	Lancer
(	(	kIx(	(
<g/>
krycí	krycí	k2eAgInSc1d1	krycí
název	název	k1gInSc1	název
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
)	)	kIx)	)
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
naše	náš	k3xOp1gFnPc4	náš
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nelze	lze	k6eNd1	lze
dopustit	dopustit	k5eAaPmF	dopustit
<g/>
"	"	kIx"	"
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
žádá	žádat	k5eAaImIp3nS	žádat
o	o	k7c6	o
zaslání	zaslání	k1gNnSc6	zaslání
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
tuto	tento	k3xDgFnSc4	tento
pro	pro	k7c4	pro
CIA	CIA	kA	CIA
nepříjemnou	příjemný	k2eNgFnSc4d1	nepříjemná
situaci	situace	k1gFnSc4	situace
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
již	již	k6eAd1	již
částečně	částečně	k6eAd1	částečně
ohořelou	ohořelý	k2eAgFnSc4d1	ohořelá
část	část	k1gFnSc4	část
oběžníku	oběžník	k1gInSc2	oběžník
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
:	:	kIx,	:
Burned	Burned	k1gMnSc1	Burned
Memo	Memo	k1gMnSc1	Memo
<g/>
)	)	kIx)	)
obdržel	obdržet	k5eAaPmAgInS	obdržet
poštou	pošta	k1gFnSc7	pošta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
americký	americký	k2eAgInSc1d1	americký
ufolog	ufolog	k1gInSc1	ufolog
Timothy	Timotha	k1gFnSc2	Timotha
Cooper	Coopra	k1gFnPc2	Coopra
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
odesílatelem	odesílatel	k1gMnSc7	odesílatel
byl	být	k5eAaImAgMnS	být
anonymní	anonymní	k2eAgMnSc1d1	anonymní
agent	agent	k1gMnSc1	agent
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pro	pro	k7c4	pro
CIA	CIA	kA	CIA
pracoval	pracovat	k5eAaImAgMnS	pracovat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
agent	agent	k1gMnSc1	agent
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
navíc	navíc	k6eAd1	navíc
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
listinu	listina	k1gFnSc4	listina
podařilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
během	během	k7c2	během
pokusu	pokus	k1gInSc2	pokus
CIA	CIA	kA	CIA
o	o	k7c6	o
spálení	spálení	k1gNnSc6	spálení
"	"	kIx"	"
<g/>
některých	některý	k3yIgInPc2	některý
citlivých	citlivý	k2eAgInPc2d1	citlivý
dokumentů	dokument	k1gInPc2	dokument
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
americkým	americký	k2eAgMnSc7d1	americký
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Williamem	William	k1gInSc7	William
Lesterem	Lester	k1gMnSc7	Lester
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
Celebration	Celebration	k1gInSc1	Celebration
of	of	k?	of
Freedom	Freedom	k1gInSc1	Freedom
<g/>
:	:	kIx,	:
JFK	JFK	kA	JFK
and	and	k?	and
New	New	k1gMnSc1	New
Frontier	Frontier	k1gMnSc1	Frontier
(	(	kIx(	(
<g/>
Oslava	oslava	k1gFnSc1	oslava
svobody	svoboda	k1gFnSc2	svoboda
<g/>
:	:	kIx,	:
JFK	JFK	kA	JFK
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
hranice	hranice	k1gFnSc2	hranice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
výše	vysoce	k6eAd2	vysoce
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
tedy	tedy	k8xC	tedy
vyvstaly	vyvstat	k5eAaPmAgFnP	vyvstat
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
odstranění	odstranění	k1gNnSc4	odstranění
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
citlivým	citlivý	k2eAgFnPc3d1	citlivá
informacím	informace	k1gFnPc3	informace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
navíc	navíc	k6eAd1	navíc
sdílet	sdílet	k5eAaImF	sdílet
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
možné	možný	k2eAgNnSc1d1	možné
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
motivu	motiv	k1gInSc2	motiv
atentátu	atentát	k1gInSc2	atentát
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
sovětské	sovětský	k2eAgFnSc2d1	sovětská
rozvědky	rozvědka	k1gFnSc2	rozvědka
Ion	ion	k1gInSc4	ion
Mihai	Miha	k1gFnSc2	Miha
Pacepa	Pacepa	k1gFnSc1	Pacepa
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijící	žijící	k2eAgMnSc1d1	žijící
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podal	podat	k5eAaPmAgInS	podat
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Nicolaem	Nicola	k1gInSc7	Nicola
Ceauşescu	Ceauşescus	k1gInSc2	Ceauşescus
<g/>
.	.	kIx.	.
</s>
<s>
Ceauşescu	Ceauşescu	k6eAd1	Ceauşescu
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
zmínit	zmínit	k5eAaPmF	zmínit
o	o	k7c6	o
seznamu	seznam	k1gInSc6	seznam
pro	pro	k7c4	pro
Kreml	Kreml	k1gInSc4	Kreml
nepohodlných	pohodlný	k2eNgMnPc2d1	nepohodlný
světových	světový	k2eAgMnPc2d1	světový
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patřili	patřit	k5eAaImAgMnP	patřit
např.	např.	kA	např.
Imre	Imr	k1gInPc1	Imr
Nagy	Naga	k1gFnSc2	Naga
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Masaryk	Masaryk	k1gMnSc1	Masaryk
nebo	nebo	k8xC	nebo
také	také	k9	také
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výpovědi	výpověď	k1gFnSc2	výpověď
Pacepa	Pacep	k1gMnSc2	Pacep
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
tyto	tento	k3xDgFnPc1	tento
osoby	osoba	k1gFnPc1	osoba
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
KGB	KGB	kA	KGB
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezřeň	k1gFnSc7	podezřeň
se	se	k3xPyFc4	se
nevyhnuli	vyhnout	k5eNaPmAgMnP	vyhnout
ani	ani	k8xC	ani
američtí	americký	k2eAgMnPc1d1	americký
ropní	ropný	k2eAgMnPc1d1	ropný
magnáti	magnát	k1gMnPc1	magnát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
systematicky	systematicky	k6eAd1	systematicky
dopouštět	dopouštět	k5eAaImF	dopouštět
krácení	krácení	k1gNnSc3	krácení
státu	stát	k1gInSc2	stát
na	na	k7c6	na
daních	daň	k1gFnPc6	daň
a	a	k8xC	a
Kennedy	Kenned	k1gMnPc4	Kenned
připravoval	připravovat	k5eAaImAgMnS	připravovat
mimo	mimo	k7c4	mimo
jiné	jiná	k1gFnPc4	jiná
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
na	na	k7c6	na
potlačení	potlačení	k1gNnSc6	potlačení
takového	takový	k3xDgNnSc2	takový
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
subjektům	subjekt	k1gInPc3	subjekt
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
byl	být	k5eAaImAgInS	být
Kennedy	Kenned	k1gMnPc4	Kenned
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
nepohodlný	pohodlný	k2eNgMnSc1d1	nepohodlný
<g/>
,	,	kIx,	,
patřily	patřit	k5eAaImAgFnP	patřit
americké	americký	k2eAgFnPc1d1	americká
zbrojařské	zbrojařský	k2eAgFnPc1d1	zbrojařská
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgInS	líbit
Kennedyho	Kennedy	k1gMnSc2	Kennedy
plán	plán	k1gInSc4	plán
na	na	k7c4	na
postupné	postupný	k2eAgNnSc4d1	postupné
stažení	stažení	k1gNnSc4	stažení
osmnácti	osmnáct	k4xCc2	osmnáct
tisíc	tisíc	k4xCgInPc2	tisíc
amerických	americký	k2eAgMnPc2d1	americký
vojenských	vojenský	k2eAgMnPc2d1	vojenský
"	"	kIx"	"
<g/>
poradců	poradce	k1gMnPc2	poradce
<g/>
"	"	kIx"	"
z	z	k7c2	z
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
Kennedyho	Kennedy	k1gMnSc2	Kennedy
nařízení	nařízení	k1gNnSc2	nařízení
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
National	National	k1gFnSc2	National
Security	Securita	k1gFnSc2	Securita
Action	Action	k1gInSc1	Action
Memorandum	memorandum	k1gNnSc1	memorandum
263	[number]	k4	263
a	a	k8xC	a
lze	lze	k6eAd1	lze
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
najít	najít	k5eAaPmF	najít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
výslovný	výslovný	k2eAgInSc4d1	výslovný
příkaz	příkaz	k1gInSc4	příkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
první	první	k4xOgFnSc1	první
tisícovka	tisícovka	k1gFnSc1	tisícovka
armádních	armádní	k2eAgMnPc2d1	armádní
specialistů	specialista	k1gMnPc2	specialista
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
by	by	kYmCp3nP	by
zbrojařské	zbrojařský	k2eAgFnPc1d1	zbrojařská
firmy	firma	k1gFnPc1	firma
přišly	přijít	k5eAaPmAgFnP	přijít
o	o	k7c4	o
obrovské	obrovský	k2eAgInPc4d1	obrovský
zisky	zisk	k1gInPc4	zisk
z	z	k7c2	z
dodávek	dodávka	k1gFnPc2	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
vojenského	vojenský	k2eAgInSc2d1	vojenský
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návazném	návazný	k2eAgInSc6d1	návazný
dokumentu	dokument	k1gInSc6	dokument
National	National	k1gFnSc2	National
Security	Securita	k1gFnSc2	Securita
Action	Action	k1gInSc1	Action
Memorandum	memorandum	k1gNnSc1	memorandum
273	[number]	k4	273
ze	z	k7c2	z
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
po	po	k7c6	po
Kennedyho	Kennedy	k1gMnSc2	Kennedy
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
Lyndonem	Lyndon	k1gMnSc7	Lyndon
Johnsonem	Johnson	k1gMnSc7	Johnson
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
Kennedyho	Kennedy	k1gMnSc2	Kennedy
plán	plán	k1gInSc1	plán
zcela	zcela	k6eAd1	zcela
přehodnocen	přehodnocen	k2eAgInSc1d1	přehodnocen
a	a	k8xC	a
de	de	k?	de
facto	facto	k1gNnSc1	facto
i	i	k9	i
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1982	[number]	k4	1982
přišel	přijít	k5eAaPmAgInS	přijít
deník	deník	k1gInSc1	deník
Dallas	Dallas	k1gInSc1	Dallas
Morning	Morning	k1gInSc1	Morning
News	News	k1gInSc1	News
s	s	k7c7	s
informací	informace	k1gFnSc7	informace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
měl	mít	k5eAaImAgInS	mít
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
blíže	blízce	k6eAd2	blízce
nespecifikovanému	specifikovaný	k2eNgMnSc3d1	nespecifikovaný
agentovi	agent	k1gMnSc3	agent
FBI	FBI	kA	FBI
sdělit	sdělit	k5eAaPmF	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
politické	politický	k2eAgMnPc4d1	politický
představitele	představitel	k1gMnPc4	představitel
Kuby	Kuba	k1gFnSc2	Kuba
obrátil	obrátit	k5eAaPmAgInS	obrátit
Lee	Lea	k1gFnSc6	Lea
H.	H.	kA	H.
Oswald	Oswald	k1gInSc1	Oswald
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
zavraždit	zavraždit	k5eAaPmF	zavraždit
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
;	;	kIx,	;
ke	k	k7c3	k
kontaktu	kontakt	k1gInSc3	kontakt
mělo	mít	k5eAaImAgNnS	mít
dle	dle	k7c2	dle
Castra	Castrum	k1gNnSc2	Castrum
dojít	dojít	k5eAaPmF	dojít
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
ABC	ABC	kA	ABC
News	Newsa	k1gFnPc2	Newsa
informaci	informace	k1gFnSc4	informace
zanedlouho	zanedlouho	k6eAd1	zanedlouho
doplnila	doplnit	k5eAaPmAgFnS	doplnit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
někdejší	někdejší	k2eAgMnSc1d1	někdejší
ředitel	ředitel	k1gMnSc1	ředitel
FBI	FBI	kA	FBI
J.	J.	kA	J.
Edgar	Edgar	k1gMnSc1	Edgar
Hoover	Hoover	k1gMnSc1	Hoover
vyslat	vyslat	k5eAaPmF	vyslat
člena	člen	k1gMnSc4	člen
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
CPUSA	CPUSA	kA	CPUSA
<g/>
)	)	kIx)	)
Morrise	Morrise	k1gFnSc1	Morrise
Childse	Childse	k1gFnSc2	Childse
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
od	od	k7c2	od
Castra	Castr	k1gMnSc2	Castr
získal	získat	k5eAaPmAgMnS	získat
bližší	blízký	k2eAgFnPc4d2	bližší
informace	informace	k1gFnPc4	informace
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
Oswaldovy	Oswaldův	k2eAgFnSc2d1	Oswaldova
nabídky	nabídka	k1gFnSc2	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
dallaské	dallaský	k2eAgFnSc2d1	dallaská
pobočky	pobočka	k1gFnSc2	pobočka
FBI	FBI	kA	FBI
James	James	k1gMnSc1	James
Hosty	host	k1gMnPc4	host
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
s	s	k7c7	s
Childsem	Childs	k1gInSc7	Childs
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Childs	Childs	k1gInSc1	Childs
pod	pod	k7c7	pod
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Solo	Solo	k6eAd1	Solo
<g/>
"	"	kIx"	"
získal	získat	k5eAaPmAgMnS	získat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
Oswaldově	Oswaldův	k2eAgNnSc6d1	Oswaldovo
napojení	napojení	k1gNnSc6	napojení
na	na	k7c4	na
kubánskou	kubánský	k2eAgFnSc4d1	kubánská
ambasádu	ambasáda	k1gFnSc4	ambasáda
v	v	k7c4	v
Mexico	Mexico	k1gNnSc4	Mexico
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Hosty	host	k1gMnPc4	host
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
takzvaný	takzvaný	k2eAgMnSc1d1	takzvaný
Solo	Solo	k1gMnSc1	Solo
podnikl	podniknout	k5eAaPmAgMnS	podniknout
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
a	a	k8xC	a
hovořil	hovořit	k5eAaImAgMnS	hovořit
zde	zde	k6eAd1	zde
s	s	k7c7	s
Castrem	Castr	k1gMnSc7	Castr
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
měl	mít	k5eAaImAgInS	mít
Solovi	Sola	k1gMnSc3	Sola
sdělit	sdělit	k5eAaPmF	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
Oswald	Oswald	k1gMnSc1	Oswald
navštívil	navštívit	k5eAaPmAgMnS	navštívit
kubánskou	kubánský	k2eAgFnSc4d1	kubánská
ambasádu	ambasáda	k1gFnSc4	ambasáda
v	v	k7c4	v
Mexico	Mexico	k1gNnSc4	Mexico
City	City	k1gFnSc2	City
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
Kubu	Kuba	k1gFnSc4	Kuba
zabije	zabít	k5eAaPmIp3nS	zabít
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Své	svůj	k3xOyFgInPc4	svůj
důvody	důvod	k1gInPc4	důvod
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
Kennedyových	Kennedyových	k2eAgFnSc1d1	Kennedyových
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
již	již	k6eAd1	již
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
mafie	mafie	k1gFnSc1	mafie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
nim	on	k3xPp3gMnPc3	on
se	se	k3xPyFc4	se
datoval	datovat	k5eAaImAgInS	datovat
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gInSc7	člen
senátního	senátní	k2eAgInSc2d1	senátní
výboru	výbor	k1gInSc2	výbor
vedeného	vedený	k2eAgInSc2d1	vedený
senátorem	senátor	k1gMnSc7	senátor
MacClellanem	MacClellan	k1gInSc7	MacClellan
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
Robert	Robert	k1gMnSc1	Robert
Kennedy	Kenneda	k1gMnSc2	Kenneda
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
právní	právní	k2eAgMnSc1d1	právní
poradce	poradce	k1gMnSc1	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
výboru	výbor	k1gInSc2	výbor
bylo	být	k5eAaImAgNnS	být
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
nezákonných	zákonný	k2eNgFnPc2d1	nezákonná
praktik	praktika	k1gFnPc2	praktika
v	v	k7c6	v
amerických	americký	k2eAgInPc6d1	americký
odborových	odborový	k2eAgInPc6d1	odborový
svazech	svaz	k1gInPc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Kennedy	Kenneda	k1gMnSc2	Kenneda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mj.	mj.	kA	mj.
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
odborové	odborový	k2eAgFnSc2d1	odborová
organizace	organizace	k1gFnSc2	organizace
řidičů	řidič	k1gMnPc2	řidič
a	a	k8xC	a
</s>
<s>
závozníků	závozník	k1gMnPc2	závozník
Teamsters	Teamsters	k1gInSc1	Teamsters
Union	union	k1gInSc1	union
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
jejího	její	k3xOp3gMnSc4	její
bosse	boss	k1gMnSc4	boss
Jimmyho	Jimmy	k1gMnSc4	Jimmy
Hoffa	Hoff	k1gMnSc4	Hoff
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
nejprve	nejprve	k6eAd1	nejprve
zproštěn	zprostit	k5eAaPmNgMnS	zprostit
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
tlaku	tlak	k1gInSc6	tlak
Roberta	Robert	k1gMnSc2	Robert
Kennedyho	Kennedy	k1gMnSc2	Kennedy
na	na	k7c4	na
orgány	orgán	k1gInPc4	orgán
činné	činný	k2eAgInPc4d1	činný
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
zkorumpovanými	zkorumpovaný	k2eAgInPc7d1	zkorumpovaný
odborovými	odborový	k2eAgInPc7d1	odborový
předáky	předák	k1gInPc7	předák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bratry	bratr	k1gMnPc4	bratr
Kennedyovi	Kennedyův	k2eAgMnPc1d1	Kennedyův
měli	mít	k5eAaImAgMnP	mít
též	též	k9	též
spadeno	spaden	k2eAgNnSc4d1	spadeno
mafiánští	mafiánský	k2eAgMnPc1d1	mafiánský
bossové	boss	k1gMnPc1	boss
Carlos	Carlos	k1gMnSc1	Carlos
Marcello	Marcello	k1gNnSc1	Marcello
<g/>
,	,	kIx,	,
Santos	Santos	k1gInSc1	Santos
Trafficante	Trafficant	k1gMnSc5	Trafficant
nebo	nebo	k8xC	nebo
také	také	k9	také
Johnny	Johnna	k1gFnPc1	Johnna
Roselli	Rosell	k1gMnPc1	Rosell
<g/>
.	.	kIx.	.
</s>
<s>
Marcello	Marcello	k1gNnSc1	Marcello
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stal	stát	k5eAaPmAgMnS	stát
mafiánským	mafiánský	k2eAgMnSc7d1	mafiánský
kmotrem	kmotr	k1gMnSc7	kmotr
celého	celý	k2eAgInSc2d1	celý
amerického	americký	k2eAgInSc2d1	americký
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Roberta	Robert	k1gMnSc2	Robert
Kennedyho	Kennedy	k1gMnSc2	Kennedy
několikrát	několikrát	k6eAd1	několikrát
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
z	z	k7c2	z
území	území	k1gNnSc2	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Carlos	Carlos	k1gMnSc1	Carlos
Marcello	Marcello	k1gNnSc4	Marcello
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
Kennedyových	Kennedyová	k1gFnPc2	Kennedyová
pronesl	pronést	k5eAaPmAgMnS	pronést
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Robert	Robert	k1gMnSc1	Robert
je	být	k5eAaImIp3nS	být
ocasem	ocas	k1gInSc7	ocas
psa	pes	k1gMnSc2	pes
(	(	kIx(	(
<g/>
Johna	John	k1gMnSc2	John
<g/>
)	)	kIx)	)
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zamezit	zamezit	k5eAaPmF	zamezit
jeho	jeho	k3xOp3gNnSc4	jeho
vrtění	vrtění	k1gNnSc4	vrtění
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
useknout	useknout	k5eAaPmF	useknout
psovi	pes	k1gMnSc3	pes
hlavu	hlava	k1gFnSc4	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Santos	Santos	k1gMnSc1	Santos
Trafficante	Trafficant	k1gMnSc5	Trafficant
<g/>
,	,	kIx,	,
boss	boss	k1gMnSc1	boss
floridské	floridský	k2eAgFnSc2d1	floridská
mafie	mafie	k1gFnSc2	mafie
<g/>
,	,	kIx,	,
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
Johna	John	k1gMnSc2	John
Kennedyho	Kennedy	k1gMnSc2	Kennedy
pronesl	pronést	k5eAaPmAgMnS	pronést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
...	...	k?	...
se	se	k3xPyFc4	se
nedožije	dožít	k5eNaPmIp3nS	dožít
příštích	příští	k2eAgFnPc2d1	příští
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
schytá	schytat	k5eAaPmIp3nS	schytat
ještě	ještě	k9	ještě
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
konáním	konání	k1gNnSc7	konání
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tuto	tento	k3xDgFnSc4	tento
větu	věta	k1gFnSc4	věta
měl	mít	k5eAaImAgMnS	mít
říci	říct	k5eAaPmF	říct
Joseovi	Joseus	k1gMnSc3	Joseus
Alemanovi	Aleman	k1gMnSc3	Aleman
<g/>
,	,	kIx,	,
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
činitelů	činitel	k1gMnPc2	činitel
kubánské	kubánský	k2eAgFnSc2d1	kubánská
exilové	exilový	k2eAgFnSc2d1	exilová
komunity	komunita	k1gFnSc2	komunita
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
nejasnosti	nejasnost	k1gFnPc1	nejasnost
do	do	k7c2	do
případu	případ	k1gInSc2	případ
vneslo	vnést	k5eAaPmAgNnS	vnést
i	i	k9	i
svědectví	svědectví	k1gNnSc4	svědectví
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
byl	být	k5eAaImAgMnS	být
policista	policista	k1gMnSc1	policista
Tippit	Tippit	k1gMnSc1	Tippit
viděn	viděn	k2eAgInSc4d1	viděn
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
atentátem	atentát	k1gInSc7	atentát
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Lee	Lea	k1gFnSc6	Lea
Oswalda	Oswalda	k1gFnSc1	Oswalda
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
dallaských	dallaský	k2eAgFnPc2d1	dallaská
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
policistovi	policista	k1gMnSc6	policista
Tippitovi	Tippita	k1gMnSc6	Tippita
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
udržoval	udržovat	k5eAaImAgInS	udržovat
styky	styk	k1gInPc4	styk
s	s	k7c7	s
mafií	mafie	k1gFnSc7	mafie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
Jacka	Jacek	k1gMnSc2	Jacek
Rubyho	Ruby	k1gMnSc2	Ruby
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
znal	znát	k5eAaImAgMnS	znát
i	i	k9	i
s	s	k7c7	s
Oswaldem	Oswaldo	k1gNnSc7	Oswaldo
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1963	[number]	k4	1963
vydal	vydat	k5eAaPmAgInS	vydat
Kennedy	Kenneda	k1gMnSc2	Kenneda
Prezidentské	prezidentský	k2eAgNnSc1d1	prezidentské
nařízení	nařízení	k1gNnSc1	nařízení
č.	č.	k?	č.
11	[number]	k4	11
110	[number]	k4	110
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ministru	ministr	k1gMnSc6	ministr
financí	finance	k1gFnSc7	finance
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
:	:	kIx,	:
Secretary	Secretara	k1gFnSc2	Secretara
of	of	k?	of
the	the	k?	the
Treasury	Treasura	k1gFnSc2	Treasura
<g/>
)	)	kIx)	)
nově	nově	k6eAd1	nově
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
emisi	emise	k1gFnSc4	emise
certifikátů	certifikát	k1gInPc2	certifikát
stříbra	stříbro	k1gNnSc2	stříbro
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
stříbro	stříbro	k1gNnSc4	stříbro
držené	držený	k2eAgNnSc1d1	držené
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zcela	zcela	k6eAd1	zcela
proti	proti	k7c3	proti
monetární	monetární	k2eAgFnSc3d1	monetární
politice	politika	k1gFnSc3	politika
ražené	ražený	k2eAgNnSc1d1	ražené
Federálním	federální	k2eAgInSc7d1	federální
rezervním	rezervní	k2eAgInSc7d1	rezervní
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
výrazné	výrazný	k2eAgNnSc4d1	výrazné
omezení	omezení	k1gNnSc4	omezení
fyzického	fyzický	k2eAgNnSc2d1	fyzické
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
běžných	běžný	k2eAgMnPc2d1	běžný
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
navíc	navíc	k6eAd1	navíc
o	o	k7c4	o
pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
zrušil	zrušit	k5eAaPmAgInS	zrušit
stříbrné	stříbrný	k2eAgInPc4d1	stříbrný
certifikáty	certifikát	k1gInPc4	certifikát
jako	jako	k8xC	jako
takové	takový	k3xDgInPc1	takový
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
vydávány	vydávat	k5eAaImNgFnP	vydávat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunitě	komunita	k1gFnSc6	komunita
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zajímajících	zajímající	k2eAgFnPc2d1	zajímající
se	se	k3xPyFc4	se
o	o	k7c4	o
atentát	atentát	k1gInSc4	atentát
na	na	k7c6	na
JFK	JFK	kA	JFK
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c6	o
souvislosti	souvislost	k1gFnSc6	souvislost
mezi	mezi	k7c4	mezi
Kennedyho	Kennedy	k1gMnSc4	Kennedy
odporem	odpor	k1gInSc7	odpor
vůči	vůči	k7c3	vůči
FEDu	FEDum	k1gNnSc3	FEDum
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
záležitosti	záležitost	k1gFnSc6	záležitost
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
následnou	následný	k2eAgFnSc7d1	následná
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
napsal	napsat	k5eAaBmAgMnS	napsat
Jim	on	k3xPp3gMnPc3	on
Marrs	Marrs	k1gInSc1	Marrs
knihu	kniha	k1gFnSc4	kniha
Crossfire	Crossfir	k1gInSc5	Crossfir
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Plot	plot	k1gInSc4	plot
that	that	k2eAgInSc1d1	that
Killed	Killed	k1gInSc1	Killed
Kennedy	Kenneda	k1gMnSc2	Kenneda
(	(	kIx(	(
<g/>
Křížová	křížový	k2eAgFnSc1d1	křížová
palba	palba	k1gFnSc1	palba
<g/>
:	:	kIx,	:
Komplot	komplot	k1gInSc1	komplot
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zabil	zabít	k5eAaPmAgMnS	zabít
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
esej	esej	k1gFnSc1	esej
Debunking	Debunking	k1gInSc1	Debunking
the	the	k?	the
Federal	Federal	k1gFnSc2	Federal
Reserve	Reserev	k1gFnSc2	Reserev
Conspiracy	Conspiraca	k1gFnSc2	Conspiraca
Theories	Theories	k1gInSc1	Theories
(	(	kIx(	(
<g/>
Odhalení	odhalení	k1gNnSc1	odhalení
konspiračních	konspirační	k2eAgFnPc2d1	konspirační
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
Federálním	federální	k2eAgInSc6d1	federální
rezervním	rezervní	k2eAgInSc6d1	rezervní
systému	systém	k1gInSc6	systém
<g/>
)	)	kIx)	)
profesora	profesor	k1gMnSc2	profesor
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Edwarda	Edward	k1gMnSc2	Edward
Flahertyho	Flaherty	k1gMnSc2	Flaherty
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
naopak	naopak	k6eAd1	naopak
toto	tento	k3xDgNnSc4	tento
spojení	spojení	k1gNnSc4	spojení
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
chybné	chybný	k2eAgNnSc4d1	chybné
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
rovněž	rovněž	k9	rovněž
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
Jerryho	Jerry	k1gMnSc4	Jerry
T.	T.	kA	T.
Belknapa	Belknap	k1gMnSc4	Belknap
<g/>
,	,	kIx,	,
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
novin	novina	k1gFnPc2	novina
Dallas	Dallas	k1gInSc1	Dallas
Morning	Morning	k1gInSc1	Morning
News	News	k1gInSc4	News
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
příjezdem	příjezd	k1gInSc7	příjezd
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kolony	kolona	k1gFnSc2	kolona
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
12.15	[number]	k4	12.15
<g/>
)	)	kIx)	)
postihl	postihnout	k5eAaPmAgInS	postihnout
epileptický	epileptický	k2eAgInSc1d1	epileptický
záchvat	záchvat	k1gInSc1	záchvat
nedaleko	nedaleko	k7c2	nedaleko
rohu	roh	k1gInSc2	roh
ulic	ulice	k1gFnPc2	ulice
Houston	Houston	k1gInSc1	Houston
a	a	k8xC	a
Elm	Elm	k1gMnSc1	Elm
street	street	k1gMnSc1	street
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
spekulací	spekulace	k1gFnPc2	spekulace
měl	mít	k5eAaImAgMnS	mít
Belknap	Belknap	k1gMnSc1	Belknap
záchvat	záchvat	k1gInSc4	záchvat
předstírat	předstírat	k5eAaImF	předstírat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
odvedení	odvedení	k1gNnSc1	odvedení
pozornosti	pozornost	k1gFnSc2	pozornost
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
aby	aby	k9	aby
by	by	kYmCp3nP	by
tak	tak	k9	tak
pachatelé	pachatel	k1gMnPc1	pachatel
atentátu	atentát	k1gInSc2	atentát
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
zaujmout	zaujmout	k5eAaPmF	zaujmout
střelecké	střelecký	k2eAgFnPc4d1	střelecká
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Belknap	Belknap	k1gInSc1	Belknap
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
záchvatu	záchvat	k1gInSc6	záchvat
odvezen	odvézt	k5eAaPmNgMnS	odvézt
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
nemocnice	nemocnice	k1gFnSc2	nemocnice
jako	jako	k8xC	jako
později	pozdě	k6eAd2	pozdě
John	John	k1gMnSc1	John
Kennedy	Kenneda	k1gMnSc2	Kenneda
–	–	k?	–
tedy	tedy	k9	tedy
do	do	k7c2	do
Parkland	Parklanda	k1gFnPc2	Parklanda
Memorial	Memorial	k1gMnSc1	Memorial
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
však	však	k9	však
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc1	žádný
lékařský	lékařský	k2eAgInSc1d1	lékařský
záznam	záznam	k1gInSc1	záznam
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
ošetření	ošetření	k1gNnSc4	ošetření
<g/>
.	.	kIx.	.
</s>
<s>
Jerry	Jerra	k1gMnSc2	Jerra
Belknap	Belknap	k1gMnSc1	Belknap
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
později	pozdě	k6eAd2	pozdě
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
čekal	čekat	k5eAaImAgInS	čekat
na	na	k7c4	na
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
prohlídku	prohlídka	k1gFnSc4	prohlídka
<g/>
,	,	kIx,	,
dorazil	dorazit	k5eAaPmAgMnS	dorazit
vůz	vůz	k1gInSc4	vůz
s	s	k7c7	s
těžce	těžce	k6eAd1	těžce
raněným	raněný	k2eAgInSc7d1	raněný
Kennedym	Kennedym	k1gInSc4	Kennedym
a	a	k8xC	a
v	v	k7c6	v
nastalém	nastalý	k2eAgInSc6d1	nastalý
zmatku	zmatek	k1gInSc6	zmatek
odešel	odejít	k5eAaPmAgMnS	odejít
šokovaný	šokovaný	k2eAgMnSc1d1	šokovaný
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
muže	muž	k1gMnSc4	muž
s	s	k7c7	s
deštníkem	deštník	k1gInSc7	deštník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Umbrella	Umbrella	k1gMnSc1	Umbrella
Man	Man	k1gMnSc1	Man
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zřetelně	zřetelně	k6eAd1	zřetelně
vidět	vidět	k5eAaImF	vidět
mj.	mj.	kA	mj.
na	na	k7c6	na
Zapruderově	Zapruderův	k2eAgInSc6d1	Zapruderův
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
tajemstvím	tajemství	k1gNnSc7	tajemství
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyřicet	čtyřicet	k4xCc4	čtyřicet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
stovek	stovka	k1gFnPc2	stovka
svědků	svědek	k1gMnPc2	svědek
stál	stát	k5eAaImAgMnS	stát
v	v	k7c4	v
osudnou	osudný	k2eAgFnSc4d1	osudná
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Elm	Elm	k1gMnSc1	Elm
street	street	k1gMnSc1	street
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
kroků	krok	k1gInPc2	krok
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
poslední	poslední	k2eAgFnSc7d1	poslední
smrtící	smrtící	k2eAgFnSc7d1	smrtící
střelou	střela	k1gFnSc7	střela
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
panovalo	panovat	k5eAaImAgNnS	panovat
slunečné	slunečný	k2eAgNnSc1d1	slunečné
a	a	k8xC	a
teplé	teplý	k2eAgNnSc1d1	teplé
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
držel	držet	k5eAaImAgMnS	držet
tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
velký	velký	k2eAgInSc1d1	velký
černý	černý	k2eAgInSc1d1	černý
deštník	deštník	k1gInSc1	deštník
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jej	on	k3xPp3gNnSc4	on
během	během	k7c2	během
samotného	samotný	k2eAgInSc2d1	samotný
průjezdu	průjezd	k1gInSc2	průjezd
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
limuzíny	limuzína	k1gFnSc2	limuzína
opakovaně	opakovaně	k6eAd1	opakovaně
otevíral	otevírat	k5eAaImAgMnS	otevírat
a	a	k8xC	a
zavíral	zavírat	k5eAaImAgMnS	zavírat
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Groden	Grodna	k1gFnPc2	Grodna
<g/>
,	,	kIx,	,
soukromý	soukromý	k2eAgMnSc1d1	soukromý
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gFnSc2	The
Killing	Killing	k1gInSc1	Killing
of	of	k?	of
a	a	k8xC	a
President	president	k1gMnSc1	president
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
otázky	otázka	k1gFnPc4	otázka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgMnS	mít
snad	snad	k9	snad
být	být	k5eAaImF	být
tento	tento	k3xDgInSc4	tento
deštník	deštník	k1gInSc4	deštník
nějakou	nějaký	k3yIgFnSc4	nějaký
formou	forma	k1gFnSc7	forma
vizuálního	vizuální	k2eAgInSc2d1	vizuální
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
by	by	kYmCp3nP	by
případnému	případný	k2eAgNnSc3d1	případné
střelci	střelec	k1gMnSc3	střelec
či	či	k8xC	či
střelcům	střelec	k1gMnPc3	střelec
dával	dávat	k5eAaImAgMnS	dávat
pokyn	pokyn	k1gInSc4	pokyn
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
palby	palba	k1gFnSc2	palba
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Nebo	nebo	k8xC	nebo
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
deštník	deštník	k1gInSc4	deštník
nějakou	nějaký	k3yIgFnSc4	nějaký
důmyslnou	důmyslný	k2eAgFnSc4d1	důmyslná
tajnou	tajný	k2eAgFnSc4d1	tajná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Je	být	k5eAaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
muži	muž	k1gMnPc1	muž
před	před	k7c7	před
ostrým	ostrý	k2eAgNnSc7d1	ostré
sluncem	slunce	k1gNnSc7	slunce
chránili	chránit	k5eAaImAgMnP	chránit
slunečníkem	slunečník	k1gInSc7	slunečník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
střelbě	střelba	k1gFnSc6	střelba
složil	složit	k5eAaPmAgMnS	složit
svůj	svůj	k3xOyFgInSc4	svůj
deštník	deštník	k1gInSc4	deštník
a	a	k8xC	a
sedl	sednout	k5eAaPmAgMnS	sednout
si	se	k3xPyFc3	se
na	na	k7c4	na
obrubník	obrubník	k1gInSc4	obrubník
ke	k	k7c3	k
dvěma	dva	k4xCgFnPc3	dva
dalším	další	k2eAgFnPc3d1	další
osobám	osoba	k1gFnPc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Chvíli	chvíle	k1gFnSc4	chvíle
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
odešel	odejít	k5eAaPmAgMnS	odejít
sám	sám	k3xTgMnSc1	sám
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
deštníkem	deštník	k1gInSc7	deštník
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
patnáctiletém	patnáctiletý	k2eAgNnSc6d1	patnáctileté
pátrání	pátrání	k1gNnSc6	pátrání
identifikován	identifikován	k2eAgInSc4d1	identifikován
komisí	komise	k1gFnSc7	komise
HSCA	HSCA	kA	HSCA
jako	jako	k8xC	jako
Louis	Louis	k1gMnSc1	Louis
Steven	Stevna	k1gFnPc2	Stevna
Witt	Witt	k1gMnSc1	Witt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výslechu	výslech	k1gInSc2	výslech
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
rodinu	rodina	k1gFnSc4	rodina
Kennedyových	Kennedyová	k1gFnPc2	Kennedyová
nesnášel	snášet	k5eNaImAgMnS	snášet
a	a	k8xC	a
otevřeným	otevřený	k2eAgInSc7d1	otevřený
deštníkem	deštník	k1gInSc7	deštník
chtěl	chtít	k5eAaImAgMnS	chtít
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
své	svůj	k3xOyFgNnSc4	svůj
pohrdání	pohrdání	k1gNnSc4	pohrdání
touto	tento	k3xDgFnSc7	tento
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Deštníkem	deštník	k1gInSc7	deštník
prý	prý	k9	prý
chtěl	chtít	k5eAaImAgMnS	chtít
Kennedymu	Kennedym	k1gInSc3	Kennedym
připomenout	připomenout	k5eAaPmF	připomenout
jeho	on	k3xPp3gMnSc4	on
otce	otec	k1gMnSc4	otec
Josepha	Joseph	k1gMnSc4	Joseph
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
<g/>
,	,	kIx,	,
k	k	k7c3	k
Wittově	Wittův	k2eAgFnSc3d1	Wittova
nelibosti	nelibost	k1gFnSc3	nelibost
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgMnS	podporovat
politiku	politika	k1gFnSc4	politika
appeasementu	appeasement	k1gInSc2	appeasement
vůči	vůči	k7c3	vůči
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
britského	britský	k2eAgMnSc2d1	britský
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
Nevilla	Nevill	k1gMnSc2	Nevill
Chamberlaina	Chamberlain	k1gMnSc2	Chamberlain
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
černý	černý	k2eAgInSc4d1	černý
deštník	deštník	k1gInSc4	deštník
byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
nepsaným	nepsaný	k2eAgInSc7d1	nepsaný
Chamberlainovým	Chamberlainův	k2eAgInSc7d1	Chamberlainův
symbolem	symbol	k1gInSc7	symbol
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
doutník	doutník	k1gInSc4	doutník
symbolem	symbol	k1gInSc7	symbol
Winstona	Winston	k1gMnSc2	Winston
Churchilla	Churchill	k1gMnSc2	Churchill
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
policie	policie	k1gFnSc1	policie
následně	následně	k6eAd1	následně
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
prohledávala	prohledávat	k5eAaImAgFnS	prohledávat
náměstí	náměstí	k1gNnSc2	náměstí
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
a	a	k8xC	a
bezprostřední	bezprostřední	k2eAgNnSc4d1	bezprostřední
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
všimnul	všimnout	k5eAaPmAgMnS	všimnout
si	se	k3xPyFc3	se
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
zmiňovaný	zmiňovaný	k2eAgMnSc1d1	zmiňovaný
pracovník	pracovník	k1gMnSc1	pracovník
železnice	železnice	k1gFnSc2	železnice
Lee	Lea	k1gFnSc6	Lea
Bowers	Bowersa	k1gFnPc2	Bowersa
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozjíždět	rozjíždět	k5eAaImF	rozjíždět
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
železničnímu	železniční	k2eAgInSc3d1	železniční
viaduktu	viadukt	k1gInSc3	viadukt
<g/>
.	.	kIx.	.
</s>
<s>
Bowers	Bowers	k6eAd1	Bowers
si	se	k3xPyFc3	se
však	však	k9	však
byl	být	k5eAaImAgMnS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
tomuto	tento	k3xDgInSc3	tento
vlaku	vlak	k1gInSc3	vlak
neudělil	udělit	k5eNaPmAgMnS	udělit
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
odjezdu	odjezd	k1gInSc3	odjezd
a	a	k8xC	a
neprodleně	prodleně	k6eNd1	prodleně
jej	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
zastavil	zastavit	k5eAaPmAgInS	zastavit
a	a	k8xC	a
přivolal	přivolat	k5eAaPmAgInS	přivolat
policii	policie	k1gFnSc3	policie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
vagónů	vagón	k1gInPc2	vagón
byli	být	k5eAaImAgMnP	být
objeveni	objevit	k5eAaPmNgMnP	objevit
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
tři	tři	k4xCgMnPc1	tři
trampové	tramp	k1gMnPc1	tramp
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
okamžitě	okamžitě	k6eAd1	okamžitě
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
záznamy	záznam	k1gInPc1	záznam
dallaské	dallaský	k2eAgFnSc2d1	dallaská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
snímky	snímek	k1gInPc4	snímek
a	a	k8xC	a
otisky	otisk	k1gInPc4	otisk
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
ztratily	ztratit	k5eAaPmAgFnP	ztratit
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
důkazem	důkaz	k1gInSc7	důkaz
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
zatčení	zatčení	k1gNnSc4	zatčení
zůstala	zůstat	k5eAaPmAgFnS	zůstat
série	série	k1gFnSc1	série
několika	několik	k4yIc2	několik
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
pořízených	pořízený	k2eAgFnPc2d1	pořízená
na	na	k7c4	na
Dealey	Dealey	k1gInPc4	Dealey
Plaza	plaz	k1gMnSc2	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
nezůstal	zůstat	k5eNaPmAgInS	zůstat
ani	ani	k8xC	ani
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
těmto	tento	k3xDgMnPc3	tento
mužům	muž	k1gMnPc3	muž
přezdíváno	přezdíván	k2eAgNnSc4d1	přezdíváno
"	"	kIx"	"
<g/>
tři	tři	k4xCgMnPc1	tři
trampové	tramp	k1gMnPc1	tramp
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
tři	tři	k4xCgMnPc1	tři
bezdomovci	bezdomovec	k1gMnPc1	bezdomovec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
dle	dle	k7c2	dle
svědeckých	svědecký	k2eAgFnPc2d1	svědecká
výpovědí	výpověď	k1gFnPc2	výpověď
pečlivě	pečlivě	k6eAd1	pečlivě
oholeni	oholen	k2eAgMnPc1d1	oholen
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
oblečení	oblečení	k1gNnPc1	oblečení
a	a	k8xC	a
boty	bota	k1gFnPc1	bota
byly	být	k5eAaImAgFnP	být
ve	v	k7c4	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
několik	několik	k4yIc1	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
identifikaci	identifikace	k1gFnSc4	identifikace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgInPc2d1	oficiální
záznamů	záznam	k1gInPc2	záznam
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
muže	muž	k1gMnPc4	muž
jménem	jméno	k1gNnSc7	jméno
Gus	Gus	k1gFnSc2	Gus
Abrams	Abramsa	k1gFnPc2	Abramsa
<g/>
,	,	kIx,	,
Harold	Harold	k1gMnSc1	Harold
Doyle	Doyle	k1gFnSc2	Doyle
a	a	k8xC	a
John	John	k1gMnSc1	John
Gedney	Gednea	k1gFnSc2	Gednea
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumníka	výzkumník	k1gMnSc2	výzkumník
Roberta	Robert	k1gMnSc2	Robert
Grodena	Groden	k1gMnSc2	Groden
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
na	na	k7c4	na
fotografii	fotografia	k1gFnSc4	fotografia
prostřední	prostřední	k2eAgFnSc4d1	prostřední
z	z	k7c2	z
trampů	tramp	k1gInPc2	tramp
<g/>
)	)	kIx)	)
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
odsouzeného	odsouzený	k2eAgMnSc4d1	odsouzený
vraha	vrah	k1gMnSc4	vrah
Charlese	Charles	k1gMnSc2	Charles
Harrelsona	Harrelson	k1gMnSc2	Harrelson
či	či	k8xC	či
bývalého	bývalý	k2eAgMnSc2d1	bývalý
agenta	agent	k1gMnSc2	agent
CIA	CIA	kA	CIA
Franka	Frank	k1gMnSc2	Frank
Sturgise	Sturgise	k1gFnSc2	Sturgise
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
trampů	tramp	k1gMnPc2	tramp
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
drobnější	drobný	k2eAgFnSc2d2	drobnější
postavy	postava	k1gFnSc2	postava
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
na	na	k7c6	na
fotografii	fotografia	k1gFnSc6	fotografia
zcela	zcela	k6eAd1	zcela
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
Howard	Howard	k1gMnSc1	Howard
Hunt	hunt	k1gInSc4	hunt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
aféry	aféra	k1gFnSc2	aféra
Watergate	Watergat	k1gInSc5	Watergat
vyšetřován	vyšetřován	k2eAgInSc4d1	vyšetřován
kvůli	kvůli	k7c3	kvůli
zcizení	zcizení	k1gNnSc3	zcizení
důležitých	důležitý	k2eAgInPc2d1	důležitý
dokumentů	dokument	k1gInPc2	dokument
ze	z	k7c2	z
sídla	sídlo	k1gNnSc2	sídlo
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
robustní	robustní	k2eAgFnSc2d1	robustní
postavy	postava	k1gFnSc2	postava
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc7	první
napravo	napravo	k6eAd1	napravo
od	od	k7c2	od
policisty	policista	k1gMnSc2	policista
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
tvrzení	tvrzení	k1gNnSc2	tvrzení
deníku	deník	k1gInSc2	deník
Dallas	Dallas	k1gMnSc1	Dallas
Times	Times	k1gMnSc1	Times
Herald	Herald	k1gMnSc1	Herald
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
podobá	podobat	k5eAaImIp3nS	podobat
popisu	popis	k1gInSc2	popis
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
spáchal	spáchat	k5eAaPmAgInS	spáchat
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Martina	Martin	k1gMnSc4	Martin
Luthera	Luther	k1gMnSc4	Luther
Kinga	King	k1gMnSc4	King
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnějším	vnější	k2eAgNnSc6d1	vnější
požárním	požární	k2eAgNnSc6d1	požární
schodišti	schodiště	k1gNnSc6	schodiště
budovy	budova	k1gFnSc2	budova
Dal-Tex	Dal-Tex	k1gInSc1	Dal-Tex
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hned	hned	k6eAd1	hned
naproti	naproti	k7c3	naproti
Texaskému	texaský	k2eAgInSc3d1	texaský
knižnímu	knižní	k2eAgInSc3d1	knižní
velkoskladu	velkosklad	k1gInSc3	velkosklad
<g/>
,	,	kIx,	,
seděl	sedět	k5eAaImAgInS	sedět
v	v	k7c6	v
době	doba	k1gFnSc6	doba
atentátu	atentát	k1gInSc2	atentát
neznámý	známý	k2eNgMnSc1d1	neznámý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
zřetelně	zřetelně	k6eAd1	zřetelně
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
fotografa	fotograf	k1gMnSc2	fotograf
Ika	Ika	k1gMnSc2	Ika
Altgense	Altgens	k1gMnSc2	Altgens
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
sekund	sekunda	k1gFnPc2	sekunda
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgInS	být
Kennedy	Kenned	k1gMnPc4	Kenned
zasažen	zasažen	k2eAgInSc4d1	zasažen
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
a	a	k8xC	a
do	do	k7c2	do
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
ztratil	ztratit	k5eAaPmAgMnS	ztratit
tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
evidentně	evidentně	k6eAd1	evidentně
úlekem	úlek	k1gInSc7	úlek
<g/>
,	,	kIx,	,
rovnováhu	rovnováha	k1gFnSc4	rovnováha
a	a	k8xC	a
spadl	spadnout	k5eAaPmAgInS	spadnout
o	o	k7c4	o
několik	několik	k4yIc4	několik
schodů	schod	k1gInPc2	schod
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
znovu	znovu	k6eAd1	znovu
vstát	vstát	k5eAaPmF	vstát
<g/>
.	.	kIx.	.
</s>
<s>
Bližší	blízký	k2eAgInSc1d2	bližší
průzkum	průzkum	k1gInSc1	průzkum
Altgensovy	Altgensův	k2eAgFnSc2d1	Altgensův
fotografie	fotografia	k1gFnSc2	fotografia
posléze	posléze	k6eAd1	posléze
odhalil	odhalit	k5eAaPmAgInS	odhalit
postavu	postava	k1gFnSc4	postava
dalšího	další	k2eAgMnSc2d1	další
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
střelby	střelba	k1gFnSc2	střelba
vykláněl	vyklánět	k5eAaImAgMnS	vyklánět
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
budovy	budova	k1gFnSc2	budova
Dal-Tex	Dal-Tex	k1gInSc4	Dal-Tex
<g/>
,	,	kIx,	,
nacházejícího	nacházející	k2eAgMnSc2d1	nacházející
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
zmíněným	zmíněný	k2eAgNnSc7d1	zmíněné
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
měl	mít	k5eAaImAgMnS	mít
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
položenou	položený	k2eAgFnSc4d1	položená
na	na	k7c6	na
okenním	okenní	k2eAgInSc6d1	okenní
parapetu	parapet	k1gInSc6	parapet
<g/>
,	,	kIx,	,
pravá	pravý	k2eAgFnSc1d1	pravá
ruka	ruka	k1gFnSc1	ruka
byla	být	k5eAaImAgFnS	být
schována	schovat	k5eAaPmNgFnS	schovat
v	v	k7c6	v
šeru	šer	k1gInSc6wB	šer
za	za	k7c7	za
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumník	výzkumník	k1gMnSc1	výzkumník
Groden	Grodna	k1gFnPc2	Grodna
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
okno	okno	k1gNnSc1	okno
skýtalo	skýtat	k5eAaImAgNnS	skýtat
perfektní	perfektní	k2eAgFnSc4d1	perfektní
palebnou	palebný	k2eAgFnSc4d1	palebná
pozici	pozice	k1gFnSc4	pozice
pro	pro	k7c4	pro
minimálně	minimálně	k6eAd1	minimálně
dva	dva	k4xCgInPc4	dva
výstřely	výstřel	k1gInPc4	výstřel
a	a	k8xC	a
dle	dle	k7c2	dle
Garrisonova	Garrisonův	k2eAgNnSc2d1	Garrisonův
schématu	schéma	k1gNnSc2	schéma
střelby	střelba	k1gFnSc2	střelba
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
odsud	odsud	k6eAd1	odsud
vypálena	vypálen	k2eAgFnSc1d1	vypálena
druhá	druhý	k4xOgFnSc1	druhý
kulka	kulka	k1gFnSc1	kulka
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Kennedyho	Kennedy	k1gMnSc2	Kennedy
minula	minout	k5eAaImAgFnS	minout
a	a	k8xC	a
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
chodník	chodník	k1gInSc4	chodník
a	a	k8xC	a
poté	poté	k6eAd1	poté
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Kennedyho	Kennedy	k1gMnSc4	Kennedy
</s>
<s>
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
byly	být	k5eAaImAgInP	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
spáchání	spáchání	k1gNnSc6	spáchání
odvysílány	odvysílat	k5eAaPmNgInP	odvysílat
živě	živě	k6eAd1	živě
na	na	k7c4	na
dallaské	dallaské	k2eAgFnSc4d1	dallaské
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
WFAA-TV	WFAA-TV	k1gFnSc1	WFAA-TV
8	[number]	k4	8
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
bylo	být	k5eAaImAgNnS	být
přerušeno	přerušen	k2eAgNnSc1d1	přerušeno
vysílání	vysílání	k1gNnSc1	vysílání
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
programu	program	k1gInSc2	program
a	a	k8xC	a
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
improvizovaným	improvizovaný	k2eAgInSc7d1	improvizovaný
zpravodajským	zpravodajský	k2eAgInSc7d1	zpravodajský
blokem	blok	k1gInSc7	blok
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
12.37	[number]	k4	12.37
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
televizním	televizní	k2eAgInSc6d1	televizní
kanálu	kanál	k1gInSc6	kanál
zcela	zcela	k6eAd1	zcela
náhle	náhle	k6eAd1	náhle
přerušen	přerušit	k5eAaPmNgInS	přerušit
pořad	pořad	k1gInSc1	pořad
pro	pro	k7c4	pro
dámy	dáma	k1gFnPc4	dáma
zaobírající	zaobírající	k2eAgFnPc4d1	zaobírající
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
módou	móda	k1gFnSc7	móda
a	a	k8xC	a
módními	módní	k2eAgInPc7d1	módní
doplňky	doplněk	k1gInPc7	doplněk
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
objevil	objevit	k5eAaPmAgMnS	objevit
rozrušený	rozrušený	k2eAgMnSc1d1	rozrušený
reportér	reportér	k1gMnSc1	reportér
Jay	Jay	k1gMnSc1	Jay
Watson	Watson	k1gMnSc1	Watson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
divákům	divák	k1gMnPc3	divák
sdělil	sdělit	k5eAaPmAgMnS	sdělit
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dobré	dobrý	k2eAgNnSc1d1	dobré
odpoledne	odpoledne	k1gNnSc1	odpoledne
dámy	dáma	k1gFnSc2	dáma
a	a	k8xC	a
pánové	pán	k1gMnPc1	pán
<g/>
,	,	kIx,	,
omluvte	omluvit	k5eAaPmRp2nP	omluvit
<g />
.	.	kIx.	.
</s>
<s>
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sotva	sotva	k6eAd1	sotva
popadám	popadat	k5eAaImIp1nS	popadat
dech	dech	k1gInSc4	dech
<g/>
,	,	kIx,	,
před	před	k7c7	před
zhruba	zhruba	k6eAd1	zhruba
deseti	deset	k4xCc7	deset
patnácti	patnáct	k4xCc7	patnáct
minutami	minuta	k1gFnPc7	minuta
došlo	dojít	k5eAaPmAgNnS	dojít
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
k	k	k7c3	k
tragické	tragický	k2eAgFnSc3d1	tragická
události	událost	k1gFnSc3	událost
v	v	k7c6	v
texaském	texaský	k2eAgInSc6d1	texaský
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
...	...	k?	...
dovolte	dovolit	k5eAaPmRp2nP	dovolit
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
vám	vy	k3xPp2nPc3	vy
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
pověděl	povědět	k5eAaPmAgMnS	povědět
<g/>
...	...	k?	...
a	a	k8xC	a
opravdu	opravdu	k6eAd1	opravdu
mne	já	k3xPp1nSc4	já
omluvte	omluvit	k5eAaPmRp2nP	omluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sotva	sotva	k6eAd1	sotva
popadám	popadat	k5eAaImIp1nS	popadat
dech	dech	k1gInSc1	dech
<g/>
...	...	k?	...
Bulletin	bulletin	k1gInSc1	bulletin
od	od	k7c2	od
dallaské	dallaský	k2eAgFnSc2d1	dallaská
pobočky	pobočka	k1gFnSc2	pobočka
United	United	k1gMnSc1	United
Press	Press	k1gInSc1	Press
<g/>
:	:	kIx,	:
Prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
a	a	k8xC	a
guvernér	guvernér	k1gMnSc1	guvernér
John	John	k1gMnSc1	John
Connally	Connalla	k1gFnSc2	Connalla
byli	být	k5eAaImAgMnP	být
zasaženi	zasáhnout	k5eAaPmNgMnP	zasáhnout
střelbou	střelba	k1gFnSc7	střelba
atentátníka	atentátník	k1gMnSc2	atentátník
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Downtown	Downtown	k1gMnSc1	Downtown
Dallas	Dallas	k1gMnSc1	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Jeli	jet	k5eAaImAgMnP	jet
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
automobilu	automobil	k1gInSc6	automobil
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
najednou	najednou	k6eAd1	najednou
ozvala	ozvat	k5eAaPmAgFnS	ozvat
střelba	střelba	k1gFnSc1	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgMnS	zhroutit
do	do	k7c2	do
náruče	náruč	k1gFnSc2	náruč
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
neprodleně	prodleně	k6eNd1	prodleně
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
Parkland	Parkland	k1gInSc1	Parkland
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
rychle	rychle	k6eAd1	rychle
šířit	šířit	k5eAaImF	šířit
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
velkých	velký	k2eAgFnPc2d1	velká
amerických	americký	k2eAgFnPc2d1	americká
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12.40	[number]	k4	12.40
přerušila	přerušit	k5eAaPmAgFnS	přerušit
vysílání	vysílání	k1gNnSc4	vysílání
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
stanice	stanice	k1gFnSc1	stanice
CBS	CBS	kA	CBS
News	News	k1gInSc4	News
a	a	k8xC	a
divákům	divák	k1gMnPc3	divák
se	se	k3xPyFc4	se
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
na	na	k7c6	na
obrazovkách	obrazovka	k1gFnPc6	obrazovka
objevil	objevit	k5eAaPmAgInS	objevit
textový	textový	k2eAgInSc1d1	textový
proužek	proužek	k1gInSc1	proužek
CBS	CBS	kA	CBS
News	News	k1gInSc1	News
Bulletin	bulletin	k1gInSc1	bulletin
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
foto	foto	k1gNnSc4	foto
vlevo	vlevo	k6eAd1	vlevo
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několikavteřinové	několikavteřinový	k2eAgFnSc6d1	několikavteřinová
prodlevě	prodleva	k1gFnSc6	prodleva
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
obrázek	obrázek	k1gInSc1	obrázek
doplněn	doplnit	k5eAaPmNgInS	doplnit
hlasovou	hlasový	k2eAgFnSc7d1	hlasová
zprávou	zpráva	k1gFnSc7	zpráva
známého	známý	k2eAgMnSc2d1	známý
amerického	americký	k2eAgMnSc2d1	americký
televizního	televizní	k2eAgMnSc2d1	televizní
hlasatele	hlasatel	k1gMnSc2	hlasatel
Waltera	Walter	k1gMnSc2	Walter
Cronkita	Cronkit	k1gMnSc2	Cronkit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
bulletin	bulletin	k1gInSc1	bulletin
od	od	k7c2	od
CBS	CBS	kA	CBS
News	Newsa	k1gFnPc2	Newsa
<g/>
:	:	kIx,	:
V	v	k7c6	v
texaském	texaský	k2eAgInSc6d1	texaský
Dallasu	Dallas	k1gInSc6	Dallas
byly	být	k5eAaImAgInP	být
vypáleny	vypálen	k2eAgInPc1d1	vypálen
tři	tři	k4xCgInPc1	tři
střely	střel	k1gInPc1	střel
na	na	k7c4	na
kolonu	kolona	k1gFnSc4	kolona
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Downtown	Downtown	k1gMnSc1	Downtown
Dallas	Dallas	k1gMnSc1	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prvních	první	k4xOgFnPc2	první
zpráv	zpráva	k1gFnPc2	zpráva
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
střelbě	střelba	k1gFnSc6	střelba
vážně	vážně	k6eAd1	vážně
raněn	raněn	k2eAgMnSc1d1	raněn
<g/>
...	...	k?	...
Právě	právě	k6eAd1	právě
dorazily	dorazit	k5eAaPmAgFnP	dorazit
další	další	k2eAgFnPc4d1	další
podrobnosti	podrobnost	k1gFnPc4	podrobnost
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
předchozí	předchozí	k2eAgFnSc2d1	předchozí
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
byl	být	k5eAaImAgInS	být
dnes	dnes	k6eAd1	dnes
postřelen	postřelen	k2eAgMnSc1d1	postřelen
<g/>
,	,	kIx,	,
když	když	k8xS	když
projížděl	projíždět	k5eAaImAgMnS	projíždět
čtvrtí	čtvrtit	k5eAaImIp3nS	čtvrtit
Downtown	Downtown	k1gNnSc4	Downtown
Dallas	Dallas	k1gInSc1	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Kennedyová	Kennedyová	k1gFnSc1	Kennedyová
vyskočila	vyskočit	k5eAaPmAgFnS	vyskočit
a	a	k8xC	a
přitiskla	přitisknout	k5eAaPmAgFnS	přitisknout
k	k	k7c3	k
sobě	se	k3xPyFc3	se
pana	pan	k1gMnSc2	pan
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
vykřila	vykřit	k5eAaPmAgFnS	vykřit
"	"	kIx"	"
<g/>
Ó	ó	k0	ó
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
kolona	kolona	k1gFnSc1	kolona
zrychlila	zrychlit	k5eAaPmAgFnS	zrychlit
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gMnSc1	United
Press	Pressa	k1gFnPc2	Pressa
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
zranění	zranění	k1gNnSc3	zranění
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
fatální	fatální	k2eAgMnSc1d1	fatální
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
minut	minuta	k1gFnPc2	minuta
nejistoty	nejistota	k1gFnSc2	nejistota
a	a	k8xC	a
dohadů	dohad	k1gInPc2	dohad
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Malcolm	Malcolm	k1gMnSc1	Malcolm
Kilduff	Kilduff	k1gMnSc1	Kilduff
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
tiskového	tiskový	k2eAgMnSc2d1	tiskový
mluvčího	mluvčí	k1gMnSc2	mluvčí
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
úmrtí	úmrtí	k1gNnSc4	úmrtí
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13.38	[number]	k4	13.38
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
informovala	informovat	k5eAaBmAgFnS	informovat
veřejnost	veřejnost	k1gFnSc1	veřejnost
již	již	k6eAd1	již
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
stanice	stanice	k1gFnSc1	stanice
CBS	CBS	kA	CBS
News	News	k1gInSc1	News
ústy	ústa	k1gNnPc7	ústa
emočně	emočně	k6eAd1	emočně
pohnutého	pohnutý	k2eAgMnSc2d1	pohnutý
Waltera	Walter	k1gMnSc2	Walter
Cronkita	Cronkit	k1gMnSc2	Cronkit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Z	z	k7c2	z
texaského	texaský	k2eAgInSc2d1	texaský
Dallasu	Dallas	k1gInSc2	Dallas
<g/>
,	,	kIx,	,
blesková	bleskový	k2eAgFnSc1d1	blesková
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
oficiální	oficiální	k2eAgFnSc1d1	oficiální
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
zemřel	zemřít	k5eAaPmAgInS	zemřít
ve	v	k7c6	v
13	[number]	k4	13
hodin	hodina	k1gFnPc2	hodina
Central	Central	k1gFnPc2	Central
Standard	standard	k1gInSc1	standard
Time	Tim	k1gInSc2	Tim
<g/>
,	,	kIx,	,
ve	v	k7c6	v
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
Eastern	Easterna	k1gFnPc2	Easterna
Standard	standard	k1gInSc1	standard
Time	Tim	k1gInSc2	Tim
<g/>
...	...	k?	...
před	před	k7c7	před
nějakými	nějaký	k3yIgFnPc7	nějaký
osmatřiceti	osmatřicet	k4xCc7	osmatřicet
minutami	minuta	k1gFnPc7	minuta
<g/>
...	...	k?	...
viceprezident	viceprezident	k1gMnSc1	viceprezident
Johnson	Johnson	k1gMnSc1	Johnson
opustil	opustit	k5eAaPmAgMnS	opustit
nemocnici	nemocnice	k1gFnSc4	nemocnice
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
,	,	kIx,	,
nevíme	vědět	k5eNaImIp1nP	vědět
však	však	k9	však
kam	kam	k6eAd1	kam
poté	poté	k6eAd1	poté
odjel	odjet	k5eAaPmAgMnS	odjet
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
zakrátko	zakrátko	k6eAd1	zakrátko
převezme	převzít	k5eAaPmIp3nS	převzít
otěže	otěž	k1gFnPc4	otěž
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	s	k7c7	s
36	[number]	k4	36
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Školy	škola	k1gFnPc1	škola
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
propustily	propustit	k5eAaPmAgFnP	propustit
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
dříve	dříve	k6eAd2	dříve
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
a	a	k8xC	a
54	[number]	k4	54
%	%	kIx~	%
Američanů	Američan	k1gMnPc2	Američan
zanechalo	zanechat	k5eAaPmAgNnS	zanechat
svých	svůj	k3xOyFgFnPc2	svůj
běžných	běžný	k2eAgFnPc2d1	běžná
denních	denní	k2eAgFnPc2d1	denní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
silný	silný	k2eAgInSc4d1	silný
smutek	smutek	k1gInSc4	smutek
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
nervozitu	nervozita	k1gFnSc4	nervozita
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
spánkem	spánek	k1gInSc7	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
každý	každý	k3xTgMnSc1	každý
americký	americký	k2eAgMnSc1d1	americký
pamětník	pamětník	k1gMnSc1	pamětník
události	událost	k1gFnSc2	událost
vzpomene	vzpomenout	k5eAaPmIp3nS	vzpomenout
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
právě	právě	k9	právě
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
zaslechl	zaslechnout	k5eAaPmAgMnS	zaslechnout
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přicházející	přicházející	k2eAgFnPc4d1	přicházející
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
i	i	k9	i
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
burza	burza	k1gFnSc1	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
(	(	kIx(	(
<g/>
zkr.	zkr.	kA	zkr.
NYSE	NYSE	kA	NYSE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
většina	většina	k1gFnSc1	většina
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
obchodovaných	obchodovaný	k2eAgFnPc2d1	obchodovaná
akcií	akcie	k1gFnPc2	akcie
skokově	skokově	k6eAd1	skokově
oslabila	oslabit	k5eAaPmAgFnS	oslabit
souhrnně	souhrnně	k6eAd1	souhrnně
o	o	k7c4	o
10,6	[number]	k4	10,6
miliardy	miliarda	k4xCgFnSc2	miliarda
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obav	obava	k1gFnPc2	obava
před	před	k7c7	před
ještě	ještě	k6eAd1	ještě
větším	veliký	k2eAgInSc7d2	veliký
propadem	propad	k1gInSc7	propad
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
počínající	počínající	k2eAgFnSc7d1	počínající
panikou	panika	k1gFnSc7	panika
investorů	investor	k1gMnPc2	investor
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
páteční	páteční	k2eAgNnSc1d1	páteční
obchodování	obchodování	k1gNnSc1	obchodování
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
ve	v	k7c6	v
14.07	[number]	k4	14.07
newyorského	newyorský	k2eAgInSc2d1	newyorský
času	čas	k1gInSc2	čas
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
burzy	burza	k1gFnSc2	burza
následně	následně	k6eAd1	následně
ohlásilo	ohlásit	k5eAaPmAgNnS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
zrušeny	zrušit	k5eAaPmNgInP	zrušit
veškeré	veškerý	k3xTgInPc1	veškerý
příkazy	příkaz	k1gInPc1	příkaz
investorů	investor	k1gMnPc2	investor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
uzavření	uzavření	k1gNnSc2	uzavření
burzy	burza	k1gFnSc2	burza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
zastavení	zastavení	k1gNnSc4	zastavení
obchodování	obchodování	k1gNnSc2	obchodování
na	na	k7c6	na
neworské	worský	k2eNgFnSc6d1	worský
burze	burza	k1gFnSc6	burza
ohlásily	ohlásit	k5eAaPmAgInP	ohlásit
konec	konec	k1gInSc4	konec
obchodování	obchodování	k1gNnSc2	obchodování
i	i	k8xC	i
trhy	trh	k1gInPc4	trh
s	s	k7c7	s
bavlnou	bavlna	k1gFnSc7	bavlna
a	a	k8xC	a
ovčí	ovčí	k2eAgFnSc7d1	ovčí
vlnou	vlna	k1gFnSc7	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Obchodování	obchodování	k1gNnSc4	obchodování
posléze	posléze	k6eAd1	posléze
ukončila	ukončit	k5eAaPmAgFnS	ukončit
i	i	k9	i
Středozápadní	Středozápadní	k2eAgFnSc1d1	Středozápadní
burza	burza	k1gFnSc1	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
(	(	kIx(	(
<g/>
Midwest	Midwest	k1gFnSc1	Midwest
Stock	Stock	k1gMnSc1	Stock
Exchange	Exchange	k1gInSc1	Exchange
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
i	i	k9	i
obchodování	obchodování	k1gNnSc1	obchodování
se	s	k7c7	s
zemědělskými	zemědělský	k2eAgFnPc7d1	zemědělská
komoditami	komodita	k1gFnPc7	komodita
jako	jako	k8xS	jako
např.	např.	kA	např.
kakaem	kakao	k1gNnSc7	kakao
<g/>
,	,	kIx,	,
kávou	káva	k1gFnSc7	káva
a	a	k8xC	a
cukrem	cukr	k1gInSc7	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hodnota	hodnota	k1gFnSc1	hodnota
Indexu	index	k1gInSc2	index
Dow-Jones	Dow-Jonesa	k1gFnPc2	Dow-Jonesa
činila	činit	k5eAaImAgFnS	činit
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
ve	v	k7c4	v
13.00	[number]	k4	13.00
735,81	[number]	k4	735,81
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
14.07	[number]	k4	14.07
již	již	k9	již
činil	činit	k5eAaImAgInS	činit
pouze	pouze	k6eAd1	pouze
711,49	[number]	k4	711,49
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
byl	být	k5eAaImAgInS	být
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
světa	svět	k1gInSc2	svět
velkým	velký	k2eAgInSc7d1	velký
šokem	šok	k1gInSc7	šok
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
ohlas	ohlas	k1gInSc1	ohlas
potvrzený	potvrzený	k2eAgInSc1d1	potvrzený
kondolencemi	kondolence	k1gFnPc7	kondolence
mnoha	mnoho	k4c2	mnoho
státníků	státník	k1gMnPc2	státník
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
soustrast	soustrast	k1gFnSc4	soustrast
novému	nový	k2eAgMnSc3d1	nový
prezidentovi	prezident	k1gMnSc3	prezident
Lyndonu	Lyndon	k1gMnSc3	Lyndon
B.	B.	kA	B.
Johnsonovi	Johnson	k1gMnSc3	Johnson
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
Kennedy	Kenned	k1gMnPc7	Kenned
stal	stát	k5eAaPmAgMnS	stát
nejspíše	nejspíše	k9	nejspíše
obětí	oběť	k1gFnSc7	oběť
kriminálního	kriminální	k2eAgNnSc2d1	kriminální
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
titulní	titulní	k2eAgFnSc4d1	titulní
stranu	strana	k1gFnSc4	strana
československého	československý	k2eAgInSc2d1	československý
deníku	deník	k1gInSc2	deník
Rudé	rudý	k2eAgNnSc1d1	Rudé
právo	právo	k1gNnSc1	právo
otištěn	otištěn	k2eAgMnSc1d1	otištěn
článek	článek	k1gInSc4	článek
následujícího	následující	k2eAgNnSc2d1	následující
znění	znění	k1gNnSc2	znění
(	(	kIx(	(
<g/>
titulek	titulek	k1gInSc4	titulek
a	a	k8xC	a
úvod	úvod	k1gInSc4	úvod
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
President	president	k1gMnSc1	president
Kennedy	Kenneda	k1gMnSc2	Kenneda
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
při	při	k7c6	při
atentátu	atentát	k1gInSc6	atentát
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
O	o	k7c6	o
pachatelích	pachatel	k1gMnPc6	pachatel
dosud	dosud	k6eAd1	dosud
nejasno	jasno	k6eNd1	jasno
–	–	k?	–
Presidentský	presidentský	k2eAgInSc1d1	presidentský
úřad	úřad	k1gInSc1	úřad
přejímá	přejímat	k5eAaImIp3nS	přejímat
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
vicepresident	vicepresident	k1gMnSc1	vicepresident
L.	L.	kA	L.
Johnson	Johnson	k1gMnSc1	Johnson
–	–	k?	–
Krajní	krajní	k2eAgFnSc2d1	krajní
pravice	pravice	k1gFnSc2	pravice
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
zločinu	zločin	k1gInSc2	zločin
<g/>
?	?	kIx.	?
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
ve	v	k7c4	v
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
středoevropského	středoevropský	k2eAgInSc2d1	středoevropský
času	čas	k1gInSc2	čas
zemřel	zemřít	k5eAaPmAgMnS	zemřít
president	president	k1gMnSc1	president
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
John	John	k1gMnSc1	John
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
Kennedy	Kenneda	k1gMnSc2	Kenneda
na	na	k7c4	na
následky	následek	k1gInPc4	následek
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
spáchán	spáchán	k2eAgInSc1d1	spáchán
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pozdních	pozdní	k2eAgFnPc2d1	pozdní
nočních	noční	k2eAgFnPc2d1	noční
hodin	hodina	k1gFnPc2	hodina
našeho	náš	k3xOp1gInSc2	náš
času	čas	k1gInSc2	čas
nedošly	dojít	k5eNaPmAgFnP	dojít
přesnější	přesný	k2eAgFnPc1d2	přesnější
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
pachateli	pachatel	k1gMnSc6	pachatel
nebo	nebo	k8xC	nebo
pachatelích	pachatel	k1gMnPc6	pachatel
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
několik	několik	k4yIc1	několik
podezřelých	podezřelý	k2eAgFnPc2d1	podezřelá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
atentátu	atentát	k1gInSc3	atentát
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
texaského	texaský	k2eAgNnSc2d1	texaské
města	město	k1gNnSc2	město
Dallasu	Dallas	k1gInSc2	Dallas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
minulý	minulý	k2eAgInSc4d1	minulý
měsíc	měsíc	k1gInSc4	měsíc
napadli	napadnout	k5eAaPmAgMnP	napadnout
rasisté	rasista	k1gMnPc1	rasista
a	a	k8xC	a
ultrapravicové	ultrapravicový	k2eAgInPc1d1	ultrapravicový
kruhy	kruh	k1gInPc1	kruh
Adlaie	Adlaie	k1gFnSc2	Adlaie
Stevensona	Stevenson	k1gMnSc2	Stevenson
<g/>
,	,	kIx,	,
představitele	představitel	k1gMnSc2	představitel
liberálnějšího	liberální	k2eAgNnSc2d2	liberálnější
křídla	křídlo	k1gNnSc2	křídlo
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
pak	pak	k6eAd1	pak
Rudé	rudý	k2eAgNnSc1d1	Rudé
Právo	právo	k1gNnSc1	právo
otisklo	otisknout	k5eAaPmAgNnS	otisknout
článek	článek	k1gInSc4	článek
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
pobočku	pobočka	k1gFnSc4	pobočka
ČTK	ČTK	kA	ČTK
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kolují	kolovat	k5eAaImIp3nP	kolovat
různé	různý	k2eAgFnPc1d1	různá
protichůdné	protichůdný	k2eAgFnPc1d1	protichůdná
zvěsti	zvěst	k1gFnPc1	zvěst
o	o	k7c6	o
silách	síla	k1gFnPc6	síla
stojících	stojící	k2eAgMnPc2d1	stojící
za	za	k7c7	za
atentátem	atentát	k1gInSc7	atentát
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
dohady	dohad	k1gInPc4	dohad
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
tím	ten	k3xDgNnSc7	ten
stojí	stát	k5eAaImIp3nP	stát
FBI	FBI	kA	FBI
a	a	k8xC	a
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
nezůstala	zůstat	k5eNaPmAgFnS	zůstat
ani	ani	k8xC	ani
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Boris	Boris	k1gMnSc1	Boris
Ivanov	Ivanov	k1gInSc1	Ivanov
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
náčelník	náčelník	k1gMnSc1	náčelník
rezidentury	rezidentura	k1gFnSc2	rezidentura
KGB	KGB	kA	KGB
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
zmínit	zmínit	k5eAaPmF	zmínit
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
čin	čin	k1gInSc4	čin
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
skupiny	skupina	k1gFnSc2	skupina
než	než	k8xS	než
o	o	k7c4	o
čin	čin	k1gInSc4	čin
osamocené	osamocený	k2eAgFnSc2d1	osamocená
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
popřela	popřít	k5eAaPmAgFnS	popřít
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
napojení	napojení	k1gNnSc4	napojení
Lee	Lea	k1gFnSc3	Lea
Oswalda	Oswaldo	k1gNnSc2	Oswaldo
na	na	k7c4	na
organizace	organizace	k1gFnPc4	organizace
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
popsala	popsat	k5eAaPmAgFnS	popsat
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
podivínského	podivínský	k2eAgMnSc4d1	podivínský
neurotika	neurotik	k1gMnSc4	neurotik
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
byly	být	k5eAaImAgFnP	být
odvysílány	odvysílán	k2eAgMnPc4d1	odvysílán
Domácím	domácí	k2eAgInSc7d1	domácí
Rádiem	rádius	k1gInSc7	rádius
22	[number]	k4	22
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
v	v	k7c6	v
19.28	[number]	k4	19.28
UT	UT	kA	UT
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
Nědělja	Nědělja	k1gFnSc1	Nědělja
<g/>
,	,	kIx,	,
týdenní	týdenní	k2eAgFnSc1d1	týdenní
příloha	příloha	k1gFnSc1	příloha
deníku	deník	k1gInSc2	deník
Izvěstija	Izvěstijum	k1gNnSc2	Izvěstijum
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
fotografii	fotografia	k1gFnSc4	fotografia
prezidenta	prezident	k1gMnSc2	prezident
Kennedyho	Kennedy	k1gMnSc2	Kennedy
s	s	k7c7	s
titulkem	titulek	k1gInSc7	titulek
informujícím	informující	k2eAgInSc7d1	informující
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
tragédie	tragédie	k1gFnSc2	tragédie
byly	být	k5eAaImAgInP	být
také	také	k9	také
přenášeny	přenášet	k5eAaImNgInP	přenášet
z	z	k7c2	z
Washingtonu	Washington	k1gInSc2	Washington
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
pomocí	pomocí	k7c2	pomocí
první	první	k4xOgFnSc2	první
telekomunikační	telekomunikační	k2eAgFnSc2d1	telekomunikační
družice	družice	k1gFnSc2	družice
Telstar	Telstara	k1gFnPc2	Telstara
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
SSSR	SSSR	kA	SSSR
Nikita	Nikita	k1gFnSc1	Nikita
Chruščov	Chruščov	k1gInSc1	Chruščov
se	se	k3xPyFc4	se
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
vrátil	vrátit	k5eAaPmAgInS	vrátit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
služební	služební	k2eAgFnSc2d1	služební
cesty	cesta	k1gFnSc2	cesta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
speciální	speciální	k2eAgFnSc2d1	speciální
telefonní	telefonní	k2eAgFnSc2d1	telefonní
linky	linka	k1gFnSc2	linka
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
zdejšího	zdejší	k2eAgMnSc4d1	zdejší
amerického	americký	k2eAgMnSc4d1	americký
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
s	s	k7c7	s
vyjádřením	vyjádření	k1gNnSc7	vyjádření
nejhlubší	hluboký	k2eAgFnSc2d3	nejhlubší
soustrasti	soustrast	k1gFnSc2	soustrast
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
označil	označit	k5eAaPmAgInS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ohavný	ohavný	k2eAgInSc4d1	ohavný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
ranou	rána	k1gFnSc7	rána
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
světový	světový	k2eAgInSc4d1	světový
mír	mír	k1gInSc4	mír
a	a	k8xC	a
Americko-Sovětskou	americkoovětský	k2eAgFnSc4d1	americko-sovětská
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
odeslal	odeslat	k5eAaPmAgMnS	odeslat
kondolenční	kondolenční	k2eAgInPc4d1	kondolenční
dopisy	dopis	k1gInPc4	dopis
Kennedyho	Kennedy	k1gMnSc2	Kennedy
manželce	manželka	k1gFnSc3	manželka
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
a	a	k8xC	a
viceprezidentovi	viceprezident	k1gMnSc3	viceprezident
Lyndonu	Lyndon	k1gMnSc3	Lyndon
Johnsonovi	Johnson	k1gMnSc3	Johnson
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
dostavil	dostavit	k5eAaPmAgMnS	dostavit
do	do	k7c2	do
rezidence	rezidence	k1gFnSc2	rezidence
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
amerického	americký	k2eAgMnSc2d1	americký
ambassadora	ambassador	k1gMnSc2	ambassador
Foye	Foy	k1gMnSc2	Foy
D.	D.	kA	D.
Kohlera	Kohler	k1gMnSc2	Kohler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
podepsal	podepsat	k5eAaPmAgMnS	podepsat
kondolenční	kondolenční	k2eAgFnSc4d1	kondolenční
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Jacquline	Jacqulin	k1gInSc5	Jacqulin
Kennedyové	Kennedyové	k2eAgFnSc1d1	Kennedyové
Chruščov	Chruščov	k1gInSc4	Chruščov
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ho	on	k3xPp3gMnSc4	on
znali	znát	k5eAaImAgMnP	znát
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
velice	velice	k6eAd1	velice
respektovali	respektovat	k5eAaImAgMnP	respektovat
a	a	k8xC	a
já	já	k3xPp1nSc1	já
osobně	osobně	k6eAd1	osobně
budu	být	k5eAaImBp1nS	být
vždy	vždy	k6eAd1	vždy
vzpomínat	vzpomínat	k5eAaImF	vzpomínat
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
setkání	setkání	k1gNnPc4	setkání
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Těmi	ten	k3xDgInPc7	ten
setkáními	setkání	k1gNnPc7	setkání
měl	mít	k5eAaImAgInS	mít
Chruščov	Chruščov	k1gInSc4	Chruščov
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
Kennedym	Kennedymum	k1gNnPc2	Kennedymum
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
eskalace	eskalace	k1gFnSc2	eskalace
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
