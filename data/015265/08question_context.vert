<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
CHKO	CHKO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
systému	systém	k1gInSc6
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
dříve	dříve	k6eAd2
Československa	Československo	k1gNnPc1
od	od	k7c2
poloviny	polovina	k1gFnSc2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
od	od	k7c2
vyhlášení	vyhlášení	k1gNnSc2
prvních	první	k4xOgInPc2
zákonů	zákon	k1gInPc2
o	o	k7c6
státní	státní	k2eAgFnSc6d1
ochraně	ochrana	k1gFnSc6
přírody	příroda	k1gFnSc2
označení	označení	k1gNnSc2
pro	pro	k7c4
velkoplošné	velkoplošný	k2eAgNnSc4d1
chráněné	chráněný	k2eAgNnSc4d1
území	území	k1gNnSc4
nižšího	nízký	k2eAgInSc2d2
stupně	stupeň	k1gInSc2
ochrany	ochrana	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
jaký	jaký	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
platí	platit	k5eAaImIp3nS
pro	pro	k7c4
národní	národní	k2eAgInPc4d1
parky	park	k1gInPc4
<g/>
.	.	kIx.
</s>