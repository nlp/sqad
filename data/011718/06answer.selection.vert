<s>
Germáni	Germán	k1gMnPc1	Germán
jsou	být	k5eAaImIp3nP	být
indoevropská	indoevropský	k2eAgFnSc1d1	indoevropská
etnolingvistická	etnolingvistický	k2eAgFnSc1d1	etnolingvistický
skupina	skupina	k1gFnSc1	skupina
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
podle	podle	k7c2	podle
užívání	užívání	k1gNnSc2	užívání
germánské	germánský	k2eAgInPc4d1	germánský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
starogermánštiny	starogermánština	k1gFnSc2	starogermánština
rozvětvily	rozvětvit	k5eAaPmAgInP	rozvětvit
v	v	k7c6	v
předřímské	předřímský	k2eAgFnSc6d1	předřímský
době	doba	k1gFnSc6	doba
železné	železný	k2eAgFnPc1d1	železná
<g/>
.	.	kIx.	.
</s>
