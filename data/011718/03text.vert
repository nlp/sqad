<p>
<s>
Germáni	Germán	k1gMnPc1	Germán
jsou	být	k5eAaImIp3nP	být
indoevropská	indoevropský	k2eAgFnSc1d1	indoevropská
etnolingvistická	etnolingvistický	k2eAgFnSc1d1	etnolingvistický
skupina	skupina	k1gFnSc1	skupina
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
podle	podle	k7c2	podle
užívání	užívání	k1gNnSc2	užívání
germánské	germánský	k2eAgInPc4d1	germánský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
starogermánštiny	starogermánština	k1gFnSc2	starogermánština
rozvětvily	rozvětvit	k5eAaPmAgInP	rozvětvit
v	v	k7c6	v
předřímské	předřímský	k2eAgFnSc6d1	předřímský
době	doba	k1gFnSc6	doba
železné	železný	k2eAgFnPc1d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
odkazu	odkaz	k1gInSc3	odkaz
na	na	k7c4	na
etnické	etnický	k2eAgFnPc4d1	etnická
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mluví	mluvit	k5eAaImIp3nP	mluvit
germánským	germánský	k2eAgInSc7d1	germánský
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
rodový	rodový	k2eAgInSc4d1	rodový
původ	původ	k1gInSc4	původ
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
ze	z	k7c2	z
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
moderní	moderní	k2eAgInPc4d1	moderní
germánské	germánský	k2eAgInPc4d1	germánský
národy	národ	k1gInPc4	národ
patří	patřit	k5eAaImIp3nP	patřit
Norové	Nor	k1gMnPc1	Nor
<g/>
,	,	kIx,	,
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
,	,	kIx,	,
Dánové	Dán	k1gMnPc1	Dán
<g/>
,	,	kIx,	,
Islanďané	Islanďan	k1gMnPc1	Islanďan
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
,	,	kIx,	,
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
Holanďané	Holanďan	k1gMnPc1	Holanďan
<g/>
,	,	kIx,	,
Afrikánci	Afrikánec	k1gMnPc1	Afrikánec
<g/>
,	,	kIx,	,
Vlámové	Vlám	k1gMnPc1	Vlám
<g/>
,	,	kIx,	,
Frísové	Frís	k1gMnPc1	Frís
<g/>
,	,	kIx,	,
Skotové	Skot	k1gMnPc1	Skot
z	z	k7c2	z
Lowlands	Lowlandsa	k1gFnPc2	Lowlandsa
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
diaspor	diaspora	k1gFnPc2	diaspora
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgMnPc2d1	evropský
Američanů	Američan	k1gMnPc2	Američan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
se	se	k3xPyFc4	se
o	o	k7c6	o
Germánech	Germán	k1gMnPc6	Germán
coby	coby	k?	coby
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
etnické	etnický	k2eAgFnSc3d1	etnická
skupině	skupina	k1gFnSc3	skupina
zmínil	zmínit	k5eAaPmAgMnS	zmínit
antický	antický	k2eAgMnSc1d1	antický
zeměpisec	zeměpisec	k1gMnSc1	zeměpisec
Strabón	Strabón	k1gMnSc1	Strabón
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
barbarskou	barbarský	k2eAgFnSc4d1	barbarská
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
podobnou	podobný	k2eAgFnSc4d1	podobná
Keltům	Kelt	k1gMnPc3	Kelt
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nikoli	nikoli	k9	nikoli
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
shodnou	shodnout	k5eAaBmIp3nP	shodnout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Germánech	Germán	k1gMnPc6	Germán
vůbec	vůbec	k9	vůbec
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Posidonia	Posidonium	k1gNnSc2	Posidonium
z	z	k7c2	z
roku	rok	k1gInSc2	rok
100	[number]	k4	100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
;	;	kIx,	;
hned	hned	k9	hned
druhý	druhý	k4xOgMnSc1	druhý
byl	být	k5eAaImAgMnS	být
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
spisu	spis	k1gInSc6	spis
"	"	kIx"	"
<g/>
Zápisky	zápiska	k1gFnSc2	zápiska
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
galské	galský	k2eAgNnSc1d1	galské
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Commentarii	Commentarie	k1gFnSc3	Commentarie
de	de	k?	de
bello	bello	k1gNnSc1	bello
Gallico	Gallico	k1gMnSc1	Gallico
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
Germánů	Germán	k1gMnPc2	Germán
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zkomolenina	zkomolenina	k1gFnSc1	zkomolenina
raného	raný	k2eAgNnSc2d1	rané
germánského	germánský	k2eAgNnSc2d1	germánské
slova	slovo	k1gNnSc2	slovo
Gaizamannoz	Gaizamannoz	k1gInSc1	Gaizamannoz
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
s	s	k7c7	s
oštěpy	oštěp	k1gInPc7	oštěp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c7	za
Germány	Germán	k1gMnPc7	Germán
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Germani	German	k1gMnPc1	German
<g/>
)	)	kIx)	)
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
řadu	řad	k1gInSc2	řad
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
chyběla	chybět	k5eAaImAgFnS	chybět
taková	takový	k3xDgFnSc1	takový
politická	politický	k2eAgFnSc1d1	politická
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
Římané	Říman	k1gMnPc1	Říman
vnutili	vnutit	k5eAaPmAgMnP	vnutit
národům	národ	k1gInPc3	národ
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kmeny	kmen	k1gInPc1	kmen
zůstávaly	zůstávat	k5eAaImAgInP	zůstávat
svobodné	svobodný	k2eAgInPc1d1	svobodný
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
svými	svůj	k3xOyFgMnPc7	svůj
vlastními	vlastní	k2eAgMnPc7d1	vlastní
dědičnými	dědičný	k2eAgMnPc7d1	dědičný
nebo	nebo	k8xC	nebo
volenými	volený	k2eAgMnPc7d1	volený
vůdci	vůdce	k1gMnPc7	vůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
hovořily	hovořit	k5eAaImAgFnP	hovořit
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelnými	srozumitelný	k2eAgInPc7d1	srozumitelný
dialekty	dialekt	k1gInPc7	dialekt
a	a	k8xC	a
sdílely	sdílet	k5eAaImAgInP	sdílet
stejnou	stejný	k2eAgFnSc4d1	stejná
mytologii	mytologie	k1gFnSc4	mytologie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
germánská	germánský	k2eAgFnSc1d1	germánská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vycházejí	vycházet	k5eAaImIp3nP	vycházet
i	i	k9	i
starobylé	starobylý	k2eAgFnPc4d1	starobylá
germánské	germánský	k2eAgFnPc4d1	germánská
literární	literární	k2eAgFnPc4d1	literární
památky	památka	k1gFnPc4	památka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Nibelunzích	Nibelung	k1gMnPc6	Nibelung
či	či	k8xC	či
Sága	ságo	k1gNnPc4	ságo
o	o	k7c4	o
Vø	Vø	k1gFnSc4	Vø
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Germáni	Germán	k1gMnPc1	Germán
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
společnou	společný	k2eAgFnSc4d1	společná
identitu	identita	k1gFnSc4	identita
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
měli	mít	k5eAaImAgMnP	mít
pojmenování	pojmenování	k1gNnSc4	pojmenování
pro	pro	k7c4	pro
negermánské	germánský	k2eNgInPc4d1	germánský
národy	národ	k1gInPc4	národ
–	–	k?	–
Walha	Walha	k1gFnSc1	Walha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
byly	být	k5eAaImAgFnP	být
odvozeny	odvozen	k2eAgInPc4d1	odvozen
pozdější	pozdní	k2eAgInPc4d2	pozdější
zeměpisné	zeměpisný	k2eAgInPc4d1	zeměpisný
názvy	název	k1gInPc4	název
Wales	Wales	k1gInSc1	Wales
(	(	kIx(	(
<g/>
keltské	keltský	k2eAgNnSc1d1	keltské
území	území	k1gNnSc1	území
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wallis	Wallis	k1gFnSc1	Wallis
(	(	kIx(	(
<g/>
kanton	kanton	k1gInSc1	kanton
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Valonsko	Valonsko	k1gNnSc1	Valonsko
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Valašsko	Valašsko	k1gNnSc1	Valašsko
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
později	pozdě	k6eAd2	pozdě
přeneseně	přeneseně	k6eAd1	přeneseně
i	i	k8xC	i
další	další	k2eAgNnPc1d1	další
území	území	k1gNnPc1	území
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Jazykovědné	jazykovědný	k2eAgInPc1d1	jazykovědný
i	i	k8xC	i
archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
napovídají	napovídat	k5eAaBmIp3nP	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lid	lid	k1gInSc4	lid
sdílející	sdílející	k2eAgInSc4d1	sdílející
podobnou	podobný	k2eAgFnSc4d1	podobná
kulturu	kultura	k1gFnSc4	kultura
obýval	obývat	k5eAaImAgMnS	obývat
severozápadní	severozápadní	k2eAgNnSc4d1	severozápadní
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
během	během	k7c2	během
pozdní	pozdní	k2eAgFnSc2d1	pozdní
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
až	až	k9	až
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
přítomnost	přítomnost	k1gFnSc4	přítomnost
germánských	germánský	k2eAgInPc2d1	germánský
kmenů	kmen	k1gInPc2	kmen
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
(	(	kIx(	(
<g/>
protoindoevropský	protoindoevropský	k2eAgInSc1d1	protoindoevropský
jazyk	jazyk	k1gInSc1	jazyk
sem	sem	k6eAd1	sem
dorazil	dorazit	k5eAaPmAgInS	dorazit
zřejmě	zřejmě	k6eAd1	zřejmě
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
neexistence	neexistence	k1gFnPc4	neexistence
předgermánských	předgermánský	k2eAgNnPc2d1	předgermánský
místních	místní	k2eAgNnPc2d1	místní
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířící	šířící	k2eAgFnSc1d1	šířící
kultura	kultura	k1gFnSc1	kultura
se	se	k3xPyFc4	se
lišila	lišit	k5eAaImAgFnS	lišit
od	od	k7c2	od
kultury	kultura	k1gFnSc2	kultura
Keltů	Kelt	k1gMnPc2	Kelt
obývajících	obývající	k2eAgMnPc2d1	obývající
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jižnější	jižní	k2eAgNnSc4d2	jižnější
území	území	k1gNnSc4	území
podél	podél	k7c2	podél
Dunaje	Dunaj	k1gInSc2	Dunaj
a	a	k8xC	a
pod	pod	k7c7	pod
Alpami	Alpy	k1gFnPc7	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Germáni	Germán	k1gMnPc1	Germán
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
nezávislých	závislý	k2eNgFnPc6d1	nezávislá
osadách	osada	k1gFnPc6	osada
a	a	k8xC	a
živili	živit	k5eAaImAgMnP	živit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
chovem	chov	k1gInSc7	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazykovědci	jazykovědec	k1gMnPc1	jazykovědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
hovořila	hovořit	k5eAaImAgFnS	hovořit
pragermánštinou	pragermánština	k1gFnSc7	pragermánština
<g/>
,	,	kIx,	,
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
větví	větev	k1gFnSc7	větev
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
částečně	částečně	k6eAd1	částečně
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
srovnáváním	srovnávání	k1gNnSc7	srovnávání
historicky	historicky	k6eAd1	historicky
známých	známý	k2eAgInPc2d1	známý
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc4	počátek
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
stěhování	stěhování	k1gNnSc4	stěhování
Germánů	Germán	k1gMnPc2	Germán
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zahaleny	zahalit	k5eAaPmNgInP	zahalit
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
koncem	koncem	k7c2	koncem
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
lze	lze	k6eAd1	lze
vyčíst	vyčíst	k5eAaPmF	vyčíst
u	u	k7c2	u
římských	římský	k2eAgMnPc2d1	římský
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
že	že	k8xS	že
migrující	migrující	k2eAgInPc1d1	migrující
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
napadly	napadnout	k5eAaPmAgInP	napadnout
Galii	Galie	k1gFnSc4	Galie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vojenskému	vojenský	k2eAgInSc3d1	vojenský
střetu	střet	k1gInSc3	střet
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
Římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šedesát	šedesát	k4xCc4	šedesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
germánské	germánský	k2eAgNnSc1d1	germánské
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pro	pro	k7c4	pro
Julia	Julius	k1gMnSc4	Julius
Caesara	Caesar	k1gMnSc4	Caesar
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
záminek	záminka	k1gFnPc2	záminka
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
Galie	Galie	k1gFnSc2	Galie
k	k	k7c3	k
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
,	,	kIx,	,
Tacita	Tacitum	k1gNnSc2	Tacitum
a	a	k8xC	a
Plinia	Plinium	k1gNnSc2	Plinium
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
germánské	germánský	k2eAgFnPc4d1	germánská
pověsti	pověst	k1gFnPc4	pověst
<g/>
,	,	kIx,	,
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
jaké	jaký	k3yRgFnPc4	jaký
skupiny	skupina	k1gFnPc4	skupina
se	se	k3xPyFc4	se
dělili	dělit	k5eAaImAgMnP	dělit
Západní	západní	k2eAgMnPc1d1	západní
Germáni	Germán	k1gMnPc1	Germán
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ingveoni	Ingveon	k1gMnPc1	Ingveon
(	(	kIx(	(
<g/>
Ingvaeones	Ingvaeones	k1gMnSc1	Ingvaeones
<g/>
)	)	kIx)	)
–	–	k?	–
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
Istveoni	Istveon	k1gMnPc1	Istveon
(	(	kIx(	(
<g/>
Istvaeones	Istvaeones	k1gMnSc1	Istvaeones
<g/>
)	)	kIx)	)
–	–	k?	–
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Rýnu	Rýn	k1gInSc6	Rýn
a	a	k8xC	a
okolo	okolo	k7c2	okolo
Vezery	Vezera	k1gFnSc2	Vezera
</s>
</p>
<p>
<s>
Hermi	Hermi	k1gFnSc1	Hermi
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
oni	onen	k3xDgMnPc1	onen
(	(	kIx(	(
<g/>
Irminones	Irminonesa	k1gFnPc2	Irminonesa
<g/>
)	)	kIx)	)
–	–	k?	–
u	u	k7c2	u
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
JutskuKromě	JutskuKromě	k1gMnPc1	JutskuKromě
toho	ten	k3xDgMnSc4	ten
existovali	existovat	k5eAaImAgMnP	existovat
ještě	ještě	k6eAd1	ještě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Severní	severní	k2eAgMnPc1d1	severní
Germáni	Germán	k1gMnPc1	Germán
–	–	k?	–
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
</s>
</p>
<p>
<s>
Východní	východní	k2eAgMnPc1d1	východní
Germáni	Germán	k1gMnPc1	Germán
–	–	k?	–
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Odry	Odra	k1gFnSc2	Odra
a	a	k8xC	a
horního	horní	k2eAgInSc2d1	horní
LabeVšechny	LabeVšechen	k2eAgFnPc1d1	LabeVšechen
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
si	se	k3xPyFc3	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
vlastní	vlastní	k2eAgInSc4d1	vlastní
dialekty	dialekt	k1gInPc4	dialekt
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
dnešní	dnešní	k2eAgInPc1d1	dnešní
germánské	germánský	k2eAgInPc1d1	germánský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Řím	Řím	k1gInSc1	Řím
posouval	posouvat	k5eAaImAgInS	posouvat
své	svůj	k3xOyFgFnPc4	svůj
hranice	hranice	k1gFnPc4	hranice
k	k	k7c3	k
Rýnu	Rýn	k1gInSc3	Rýn
a	a	k8xC	a
Dunaji	Dunaj	k1gInSc3	Dunaj
a	a	k8xC	a
připojoval	připojovat	k5eAaImAgInS	připojovat
mnohá	mnohý	k2eAgNnPc4d1	mnohé
keltská	keltský	k2eAgNnPc4d1	keltské
společenství	společenství	k1gNnPc4	společenství
k	k	k7c3	k
impériu	impérium	k1gNnSc3	impérium
<g/>
,	,	kIx,	,
kmenová	kmenový	k2eAgNnPc4d1	kmenové
území	území	k1gNnPc4	území
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
na	na	k7c4	na
východ	východ	k1gInSc4	východ
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
v	v	k7c6	v
záznamech	záznam	k1gInPc6	záznam
pod	pod	k7c7	pod
společným	společný	k2eAgInSc7d1	společný
názvem	název	k1gInSc7	název
Germania	germanium	k1gNnSc2	germanium
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
občas	občas	k6eAd1	občas
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
impériem	impérium	k1gNnSc7	impérium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
provozovali	provozovat	k5eAaImAgMnP	provozovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
jižním	jižní	k2eAgMnSc7d1	jižní
sousedem	soused	k1gMnSc7	soused
čilý	čilý	k2eAgInSc4d1	čilý
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
kulturní	kulturní	k2eAgFnSc3d1	kulturní
výměně	výměna	k1gFnSc3	výměna
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
k	k	k7c3	k
vojenským	vojenský	k2eAgNnPc3d1	vojenské
spojenectvím	spojenectví	k1gNnPc3	spojenectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doba	doba	k1gFnSc1	doba
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
se	se	k3xPyFc4	se
blížil	blížit	k5eAaImAgInS	blížit
konec	konec	k1gInSc1	konec
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgInP	začít
se	se	k3xPyFc4	se
četné	četný	k2eAgInPc1d1	četný
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
hromadně	hromadně	k6eAd1	hromadně
stěhovat	stěhovat	k5eAaImF	stěhovat
různými	různý	k2eAgInPc7d1	různý
směry	směr	k1gInPc7	směr
a	a	k8xC	a
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Takzvané	takzvaný	k2eAgNnSc1d1	takzvané
stěhování	stěhování	k1gNnSc1	stěhování
národů	národ	k1gInPc2	národ
je	on	k3xPp3gMnPc4	on
přivedlo	přivést	k5eAaPmAgNnS	přivést
až	až	k9	až
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
do	do	k7c2	do
Středomoří	středomoří	k1gNnSc2	středomoří
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
až	až	k9	až
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Místy	místy	k6eAd1	místy
pochopitelně	pochopitelně	k6eAd1	pochopitelně
narážely	narážet	k5eAaImAgFnP	narážet
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
kmeny	kmen	k1gInPc4	kmen
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
válkám	válka	k1gFnPc3	válka
a	a	k8xC	a
poražené	poražený	k2eAgInPc1d1	poražený
kmeny	kmen	k1gInPc1	kmen
časem	časem	k6eAd1	časem
splynuly	splynout	k5eAaPmAgInP	splynout
s	s	k7c7	s
vítězi	vítěz	k1gMnPc7	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
se	se	k3xPyFc4	se
Jutové	jutový	k2eAgFnPc1d1	jutová
sloučili	sloučit	k5eAaPmAgMnP	sloučit
s	s	k7c7	s
Dány	Dán	k1gMnPc7	Dán
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
se	se	k3xPyFc4	se
Géaté	Géatý	k2eAgInPc1d1	Géatý
(	(	kIx(	(
<g/>
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
Góty	Gót	k1gMnPc7	Gót
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
spojili	spojit	k5eAaPmAgMnP	spojit
se	s	k7c7	s
Švédy	Švéd	k1gMnPc7	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
pomíchali	pomíchat	k5eAaPmAgMnP	pomíchat
Keltové	Kelt	k1gMnPc1	Kelt
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
částí	část	k1gFnSc7	část
Sasů	Sas	k1gMnPc2	Sas
a	a	k8xC	a
Anglů	Angl	k1gMnPc2	Angl
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přeplavila	přeplavit	k5eAaPmAgFnS	přeplavit
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
o	o	k7c6	o
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
hovořící	hovořící	k2eAgNnSc1d1	hovořící
anglicky	anglicky	k6eAd1	anglicky
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c4	o
anglosaské	anglosaský	k2eAgFnPc4d1	anglosaská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
===	===	k?	===
</s>
</p>
<p>
<s>
Alamani	Alaman	k1gMnPc1	Alaman
<g/>
,	,	kIx,	,
Ambronové	Ambron	k1gMnPc5	Ambron
<g/>
,	,	kIx,	,
Ampsivarové	Ampsivar	k1gMnPc5	Ampsivar
<g/>
,	,	kIx,	,
Anglové	Angl	k1gMnPc5	Angl
<g/>
,	,	kIx,	,
Angrivarové	Angrivar	k1gMnPc5	Angrivar
</s>
</p>
<p>
<s>
Batavové	Batavový	k2eAgFnPc1d1	Batavový
<g/>
,	,	kIx,	,
Bavoři	Bavor	k1gMnPc1	Bavor
<g/>
,	,	kIx,	,
Brukteři	Brukter	k1gMnPc1	Brukter
<g/>
,	,	kIx,	,
Burgundi	Burgund	k1gMnPc1	Burgund
</s>
</p>
<p>
<s>
Canninefatové	Canninefatový	k2eAgFnPc1d1	Canninefatový
<g/>
,	,	kIx,	,
Chamavové	Chamavový	k2eAgFnPc1d1	Chamavový
<g/>
,	,	kIx,	,
Chasuarové	Chasuar	k1gMnPc5	Chasuar
<g/>
,	,	kIx,	,
Chaukové	Chauk	k1gMnPc5	Chauk
<g/>
,	,	kIx,	,
Cheruskové	Cherusek	k1gMnPc5	Cherusek
<g/>
,	,	kIx,	,
Chattové	Chatt	k1gMnPc5	Chatt
<g/>
,	,	kIx,	,
Cimbrové	Cimbr	k1gMnPc5	Cimbr
</s>
</p>
<p>
<s>
Dánové	Dán	k1gMnPc1	Dán
<g/>
,	,	kIx,	,
Dulgubnové	Dulgubn	k1gMnPc1	Dulgubn
<g/>
,	,	kIx,	,
Durynkové	Durynk	k1gMnPc1	Durynk
</s>
</p>
<p>
<s>
Fennové	Fenn	k1gMnPc1	Fenn
<g/>
,	,	kIx,	,
Fosové	Fosus	k1gMnPc1	Fosus
<g/>
,	,	kIx,	,
Frankové	Frank	k1gMnPc1	Frank
<g/>
,	,	kIx,	,
Frísové	Frís	k1gMnPc1	Frís
</s>
</p>
<p>
<s>
Gétové	Gétus	k1gMnPc1	Gétus
<g/>
,	,	kIx,	,
Gepidové	Gepid	k1gMnPc1	Gepid
<g/>
,	,	kIx,	,
Gótové	Gót	k1gMnPc1	Gót
(	(	kIx(	(
<g/>
Ostrogóti	Ostrogót	k1gMnPc1	Ostrogót
<g/>
,	,	kIx,	,
Vizigóti	Vizigót	k1gMnPc1	Vizigót
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hárové	Hárová	k1gFnPc1	Hárová
<g/>
,	,	kIx,	,
Helisové	Helisový	k2eAgFnPc1d1	Helisová
<g/>
,	,	kIx,	,
Hermunduři	Hermundur	k1gMnPc1	Hermundur
<g/>
,	,	kIx,	,
Herulové	Herul	k1gMnPc1	Herul
</s>
</p>
<p>
<s>
Ingvaeones	Ingvaeones	k1gInSc1	Ingvaeones
(	(	kIx(	(
<g/>
Severomořští	severomořský	k2eAgMnPc1d1	severomořský
Germáni	Germán	k1gMnPc1	Germán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Irminones	Irminones	k1gInSc1	Irminones
(	(	kIx(	(
<g/>
Polabští	polabský	k2eAgMnPc1d1	polabský
Germáni	Germán	k1gMnPc1	Germán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Istvaeones	Istvaeones	k1gInSc1	Istvaeones
(	(	kIx(	(
<g/>
Rýnsko-veserští	Rýnskoeserský	k2eAgMnPc1d1	Rýnsko-veserský
Germáni	Germán	k1gMnPc1	Germán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Juthungové	Juthungový	k2eAgFnPc1d1	Juthungový
<g/>
,	,	kIx,	,
Jutové	jutový	k2eAgFnPc1d1	jutová
</s>
</p>
<p>
<s>
Kvádové	Kvád	k1gMnPc1	Kvád
</s>
</p>
<p>
<s>
Langobardi	Langobard	k1gMnPc1	Langobard
<g/>
,	,	kIx,	,
Lemové	lemový	k2eAgFnPc1d1	lemová
<g/>
,	,	kIx,	,
Lugové	Lugové	k2eAgFnPc1d1	Lugové
</s>
</p>
<p>
<s>
Manimové	Manim	k1gMnPc1	Manim
<g/>
,	,	kIx,	,
Markomani	Markoman	k1gMnPc1	Markoman
<g/>
,	,	kIx,	,
Marobudové	Marobuda	k1gMnPc1	Marobuda
<g/>
,	,	kIx,	,
Mattiacové	Mattiace	k1gMnPc1	Mattiace
</s>
</p>
<p>
<s>
Naharvalové	Naharvalová	k1gFnPc1	Naharvalová
<g/>
,	,	kIx,	,
Nemetové	Nemetový	k2eAgFnPc1d1	Nemetový
<g/>
,	,	kIx,	,
Nervové	nervový	k2eAgFnPc1d1	nervová
<g/>
,	,	kIx,	,
Njarové	Njarový	k2eAgFnPc1d1	Njarový
</s>
</p>
<p>
<s>
Rugiové	Rugius	k1gMnPc1	Rugius
</s>
</p>
<p>
<s>
Sasové	Sas	k1gMnPc1	Sas
<g/>
,	,	kIx,	,
Skirové	Skir	k1gMnPc5	Skir
<g/>
,	,	kIx,	,
Semoni	Semon	k1gMnPc5	Semon
<g/>
,	,	kIx,	,
Sitoni	Siton	k1gMnPc5	Siton
<g/>
,	,	kIx,	,
Suioni	Suion	k1gMnPc5	Suion
<g/>
,	,	kIx,	,
Sugambrové	Sugambr	k1gMnPc5	Sugambr
<g/>
,	,	kIx,	,
Svébové	Svéb	k1gMnPc5	Svéb
(	(	kIx(	(
<g/>
Svévové	Svév	k1gMnPc5	Svév
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tencterové	Tencter	k1gMnPc1	Tencter
<g/>
,	,	kIx,	,
Teutoni	Teuton	k1gMnPc5	Teuton
<g/>
,	,	kIx,	,
Trevové	Treva	k1gMnPc5	Treva
<g/>
,	,	kIx,	,
Triboci	Triboce	k1gMnPc5	Triboce
<g/>
,	,	kIx,	,
Tudrové	Tudr	k1gMnPc5	Tudr
</s>
</p>
<p>
<s>
Ubiové	Ubius	k1gMnPc1	Ubius
<g/>
,	,	kIx,	,
Usipetové	Usipet	k1gMnPc1	Usipet
</s>
</p>
<p>
<s>
Vikingové	Viking	k1gMnPc1	Viking
<g/>
,	,	kIx,	,
Vandalové	Vandal	k1gMnPc1	Vandal
<g/>
,	,	kIx,	,
Vangioni	Vangion	k1gMnPc1	Vangion
</s>
</p>
<p>
<s>
==	==	k?	==
Germáni	Germán	k1gMnPc1	Germán
a	a	k8xC	a
pád	pád	k1gInSc1	pád
Říma	Řím	k1gInSc2	Řím
==	==	k?	==
</s>
</p>
<p>
<s>
Některým	některý	k3yIgInPc3	některý
germánským	germánský	k2eAgInPc3d1	germánský
kmenům	kmen	k1gInPc3	kmen
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
dáván	dávat	k5eAaImNgInS	dávat
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
"	"	kIx"	"
<g/>
pád	pád	k1gInSc1	pád
<g/>
"	"	kIx"	"
Římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
koncem	koncem	k7c2	koncem
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
a	a	k8xC	a
archeologové	archeolog	k1gMnPc1	archeolog
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
posouvají	posouvat	k5eAaImIp3nP	posouvat
své	svůj	k3xOyFgInPc4	svůj
výklady	výklad	k1gInPc4	výklad
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Germáni	Germán	k1gMnPc1	Germán
nebyli	být	k5eNaImAgMnP	být
jen	jen	k6eAd1	jen
útočníky	útočník	k1gMnPc7	útočník
na	na	k7c6	na
upadající	upadající	k2eAgFnSc6d1	upadající
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
zapojili	zapojit	k5eAaPmAgMnP	zapojit
do	do	k7c2	do
obrany	obrana	k1gFnSc2	obrana
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
už	už	k6eAd1	už
centrální	centrální	k2eAgFnSc1d1	centrální
vláda	vláda	k1gFnSc1	vláda
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
spravovat	spravovat	k5eAaImF	spravovat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
i	i	k8xC	i
malé	malý	k2eAgFnPc1d1	malá
skupiny	skupina	k1gFnPc1	skupina
Germánů	Germán	k1gMnPc2	Germán
se	se	k3xPyFc4	se
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
rekrutovaly	rekrutovat	k5eAaImAgInP	rekrutovat
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
limes	limesa	k1gFnPc2	limesa
(	(	kIx(	(
<g/>
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
oblastí	oblast	k1gFnPc2	oblast
<g/>
)	)	kIx)	)
římského	římský	k2eAgInSc2d1	římský
světa	svět	k1gInSc2	svět
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
vystoupali	vystoupat	k5eAaPmAgMnP	vystoupat
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
armádní	armádní	k2eAgFnSc6d1	armádní
hierarchii	hierarchie	k1gFnSc6	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Odoaker	Odoaker	k1gMnSc1	Odoaker
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
sesadil	sesadit	k5eAaPmAgMnS	sesadit
Romula	Romulus	k1gMnSc4	Romulus
Augusta	August	k1gMnSc4	August
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
císařové	císař	k1gMnPc1	císař
časem	časem	k6eAd1	časem
najímali	najímat	k5eAaImAgMnP	najímat
celé	celý	k2eAgFnPc4d1	celá
kmenové	kmenový	k2eAgFnPc4d1	kmenová
skupiny	skupina	k1gFnPc4	skupina
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
jejich	jejich	k3xOp3gMnPc2	jejich
vlastních	vlastní	k2eAgMnPc2d1	vlastní
vůdců	vůdce	k1gMnPc2	vůdce
jako	jako	k8xS	jako
římských	římský	k2eAgMnPc2d1	římský
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Pomoc	pomoc	k1gFnSc1	pomoc
s	s	k7c7	s
obranou	obrana	k1gFnSc7	obrana
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
změnila	změnit	k5eAaPmAgFnS	změnit
ve	v	k7c4	v
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sice	sice	k8xC	sice
fungovala	fungovat	k5eAaImAgFnS	fungovat
podle	podle	k7c2	podle
římských	římský	k2eAgFnPc2d1	římská
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
germánských	germánský	k2eAgMnPc2d1	germánský
kmenových	kmenový	k2eAgMnPc2d1	kmenový
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
kontrolovaných	kontrolovaný	k2eAgInPc2d1	kontrolovaný
šlechtou	šlechta	k1gFnSc7	šlechta
některého	některý	k3yIgInSc2	některý
germánského	germánský	k2eAgInSc2d1	germánský
kmene	kmen	k1gInSc2	kmen
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
někdejšímu	někdejší	k2eAgNnSc3d1	někdejší
srdci	srdce	k1gNnSc3	srdce
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
Odoakerovi	Odoaker	k1gMnSc6	Odoaker
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
Theodorich	Theodorich	k1gInSc1	Theodorich
Veliký	veliký	k2eAgInSc1d1	veliký
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnPc4	vůdce
Ostrogótů	Ostrogót	k1gMnPc2	Ostrogót
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
římští	římský	k2eAgMnPc1d1	římský
občané	občan	k1gMnPc1	občan
i	i	k8xC	i
gótští	gótský	k2eAgMnPc1d1	gótský
osadníci	osadník	k1gMnPc1	osadník
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
legitimního	legitimní	k2eAgMnSc4d1	legitimní
dědice	dědic	k1gMnSc4	dědic
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
Římem	Řím	k1gInSc7	Řím
i	i	k8xC	i
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přijetí	přijetí	k1gNnSc1	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrogóti	Ostrogót	k1gMnPc1	Ostrogót
<g/>
,	,	kIx,	,
Vizigóti	Vizigót	k1gMnPc1	Vizigót
a	a	k8xC	a
Vandalové	Vandal	k1gMnPc1	Vandal
byli	být	k5eAaImAgMnP	být
christianizováni	christianizovat	k5eAaImNgMnP	christianizovat
ještě	ještě	k6eAd1	ještě
<g/>
,	,	kIx,	,
když	když	k8xS	když
žili	žít	k5eAaImAgMnP	žít
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
impéria	impérium	k1gNnSc2	impérium
<g/>
;	;	kIx,	;
přijali	přijmout	k5eAaPmAgMnP	přijmout
arianismus	arianismus	k1gInSc4	arianismus
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
katolické	katolický	k2eAgNnSc1d1	katolické
náboženství	náboženství	k1gNnSc1	náboženství
a	a	k8xC	a
římsko-byzantskými	římskoyzantský	k2eAgMnPc7d1	římsko-byzantský
křesťany	křesťan	k1gMnPc7	křesťan
byli	být	k5eAaImAgMnP	být
brzy	brzy	k6eAd1	brzy
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
kacíře	kacíř	k1gMnPc4	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Gótský	gótský	k2eAgInSc1d1	gótský
překlad	překlad	k1gInSc1	překlad
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
pořízený	pořízený	k2eAgInSc4d1	pořízený
Ulfilem	Ulfil	k1gMnSc7	Ulfil
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obrátil	obrátit	k5eAaPmAgMnS	obrátit
Góty	Gót	k1gMnPc4	Gót
na	na	k7c4	na
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
cenným	cenný	k2eAgInSc7d1	cenný
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
tohoto	tento	k3xDgInSc2	tento
vymřelého	vymřelý	k2eAgInSc2d1	vymřelý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Langobardi	Langobard	k1gMnPc1	Langobard
byli	být	k5eAaImAgMnP	být
obráceni	obrátit	k5eAaPmNgMnP	obrátit
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přejali	přejmout	k5eAaPmAgMnP	přejmout
víru	víra	k1gFnSc4	víra
ariánských	ariánský	k2eAgFnPc2d1	ariánská
germánských	germánský	k2eAgFnPc2d1	germánská
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Frankové	Frank	k1gMnPc1	Frank
byli	být	k5eAaImAgMnP	být
obráceni	obrátit	k5eAaPmNgMnP	obrátit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
paganismu	paganismus	k1gInSc2	paganismus
(	(	kIx(	(
<g/>
pohanství	pohanství	k1gNnSc2	pohanství
<g/>
)	)	kIx)	)
na	na	k7c4	na
katolicismus	katolicismus	k1gInSc4	katolicismus
bez	bez	k7c2	bez
ariánského	ariánský	k2eAgNnSc2d1	ariánské
mezidobí	mezidobí	k1gNnSc2	mezidobí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
později	pozdě	k6eAd2	pozdě
zajistili	zajistit	k5eAaPmAgMnP	zajistit
franští	franský	k2eAgMnPc1d1	franský
misionáři	misionář	k1gMnPc1	misionář
a	a	k8xC	a
bojovníci	bojovník	k1gMnPc1	bojovník
silou	síla	k1gFnSc7	síla
pokřtění	pokřtění	k1gNnSc4	pokřtění
svých	svůj	k3xOyFgMnPc2	svůj
severních	severní	k2eAgMnPc2d1	severní
saských	saský	k2eAgMnPc2d1	saský
sousedů	soused	k1gMnPc2	soused
v	v	k7c6	v
sérii	série	k1gFnSc6	série
nájezdů	nájezd	k1gInPc2	nájezd
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
současně	současně	k6eAd1	současně
připojovali	připojovat	k5eAaImAgMnP	připojovat
saská	saský	k2eAgNnPc4d1	Saské
území	území	k1gNnPc4	území
k	k	k7c3	k
Francké	francký	k2eAgFnSc3d1	Francká
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Cejnková	Cejnková	k1gFnSc1	Cejnková
–	–	k?	–
Komoróczy	Komorócza	k1gFnSc2	Komorócza
-	-	kIx~	-
Tejral	Tejral	k1gMnSc1	Tejral
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
Germáni	Germán	k1gMnPc1	Germán
:	:	kIx,	:
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
,	,	kIx,	,
rivalové	rival	k1gMnPc1	rival
<g/>
,	,	kIx,	,
sousedé	soused	k1gMnPc1	soused
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Collins	Collins	k1gInSc1	Collins
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc1	Roger
<g/>
:	:	kIx,	:
Evropa	Evropa	k1gFnSc1	Evropa
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
479	[number]	k4	479
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
660	[number]	k4	660
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Češka	Češka	k1gFnSc1	Češka
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Zánik	zánik	k1gInSc1	zánik
antického	antický	k2eAgInSc2d1	antický
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
:	:	kIx,	:
Interakce	interakce	k1gFnSc1	interakce
Gótů	Gót	k1gMnPc2	Gót
a	a	k8xC	a
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Schlette	Schletit	k5eAaImRp2nP	Schletit
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
,	,	kIx,	,
Germáni	Germán	k1gMnPc1	Germán
mezi	mezi	k7c7	mezi
Thorsbergem	Thorsberg	k1gInSc7	Thorsberg
a	a	k8xC	a
Ravennou	Ravenný	k2eAgFnSc7d1	Ravenný
:	:	kIx,	:
kulturní	kulturní	k2eAgFnPc4d1	kulturní
dějiny	dějiny	k1gFnPc4	dějiny
Germánů	Germán	k1gMnPc2	Germán
do	do	k7c2	do
konce	konec	k1gInSc2	konec
doby	doba	k1gFnSc2	doba
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Germanie	Germanie	k1gFnSc1	Germanie
Klaudia	Klaudium	k1gNnSc2	Klaudium
Ptolemaia	Ptolemaios	k1gMnSc2	Ptolemaios
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
1930-1953	[number]	k4	1930-1953
</s>
</p>
<p>
<s>
Tacitus	Tacitus	k1gMnSc1	Tacitus
<g/>
,	,	kIx,	,
C.	C.	kA	C.
<g/>
,	,	kIx,	,
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
císařského	císařský	k2eAgInSc2d1	císařský
Říma	Řím	k1gInSc2	Řím
<g/>
:	:	kIx,	:
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
Iulia	Iulius	k1gMnSc2	Iulius
Agricoly	Agricola	k1gFnSc2	Agricola
<g/>
,	,	kIx,	,
Germánie	Germánie	k1gFnSc1	Germánie
<g/>
,	,	kIx,	,
rozprava	rozprava	k1gFnSc1	rozprava
o	o	k7c6	o
řečnících	řečnící	k2eAgFnPc6d1	řečnící
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Todd	Todd	k1gMnSc1	Todd
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Germáni	Germán	k1gMnPc1	Germán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Wolters	Wolters	k1gInSc1	Wolters
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
v	v	k7c4	v
Germánii	Germánie	k1gFnSc4	Germánie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Malcolm	Malcolm	k1gMnSc1	Malcolm
Todd	Todd	k1gMnSc1	Todd
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Early	earl	k1gMnPc7	earl
Germans	Germansa	k1gFnPc2	Germansa
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Blackwell	Blackwell	k1gInSc1	Blackwell
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
631	[number]	k4	631
<g/>
-	-	kIx~	-
<g/>
19904	[number]	k4	19904
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Germáni	Germán	k1gMnPc1	Germán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Germán	Germán	k1gMnSc1	Germán
ve	v	k7c6	v
WikislovníkuKategorie	WikislovníkuKategorie	k1gFnSc1	WikislovníkuKategorie
<g/>
:	:	kIx,	:
<g/>
Pravěk	pravěk	k1gInSc1	pravěk
a	a	k8xC	a
starověk	starověk	k1gInSc1	starověk
na	na	k7c4	na
území	území	k1gNnSc4	území
Německa	Německo	k1gNnSc2	Německo
</s>
</p>
