<s>
Výstaviště	výstaviště	k1gNnSc1
</s>
<s>
Slovem	slovem	k6eAd1
výstaviště	výstaviště	k1gNnSc1
obvykle	obvykle	k6eAd1
označujeme	označovat	k5eAaImIp1nP
zvláštní	zvláštní	k2eAgNnSc4d1
společenské	společenský	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
primárně	primárně	k6eAd1
určené	určený	k2eAgFnSc2d1
k	k	k7c3
pravidelnému	pravidelný	k2eAgNnSc3d1
pořádání	pořádání	k1gNnSc3
veletrhů	veletrh	k1gInPc2
a	a	k8xC
výstav	výstava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
běžné	běžný	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
soubor	soubor	k1gInSc4
pozemků	pozemek	k1gInPc2
<g/>
,	,	kIx,
výstavních	výstavní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
technických	technický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
a	a	k8xC
jiných	jiný	k2eAgInPc2d1
doplňkových	doplňkový	k2eAgInPc2d1
organizačních	organizační	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
umožňují	umožňovat	k5eAaImIp3nP
výstavní	výstavní	k2eAgInSc4d1
a	a	k8xC
veletržní	veletržní	k2eAgInSc4d1
provoz	provoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
obvykle	obvykle	k6eAd1
bývá	bývat	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
do	do	k7c2
nějakého	nějaký	k3yIgInSc2
uzavřeného	uzavřený	k2eAgInSc2d1
výstavního	výstavní	k2eAgInSc2d1
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastním	vlastnit	k5eAaImIp1nS
větším	veliký	k2eAgNnSc7d2
či	či	k8xC
menším	malý	k2eAgNnSc7d2
výstavištěm	výstaviště	k1gNnSc7
či	či	k8xC
výstavním	výstavní	k2eAgInSc7d1
areálem	areál	k1gInSc7
disponuje	disponovat	k5eAaBmIp3nS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
více	hodně	k6eAd2
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
je	on	k3xPp3gFnPc4
i	i	k9
některá	některý	k3yIgNnPc1
menší	malý	k2eAgNnPc1d2
města	město	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstaviště	výstaviště	k1gNnSc1
obvykle	obvykle	k6eAd1
plní	plnit	k5eAaImIp3nS
kromě	kromě	k7c2
své	svůj	k3xOyFgFnSc2
primární	primární	k2eAgFnSc2d1
výstavní	výstavní	k2eAgFnSc2d1
a	a	k8xC
obchodní	obchodní	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
také	také	k9
nezanedbatelnou	zanedbatelný	k2eNgFnSc4d1
funkci	funkce	k1gFnSc4
kulturní	kulturní	k2eAgFnSc4d1
a	a	k8xC
společenskou	společenský	k2eAgFnSc4d1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
jedno	jeden	k4xCgNnSc4
z	z	k7c2
místních	místní	k2eAgNnPc2d1
kulturních	kulturní	k2eAgNnPc2d1
a	a	k8xC
společenských	společenský	k2eAgNnPc2d1
center	centrum	k1gNnPc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3
česká	český	k2eAgFnSc1d1
výstaviště	výstaviště	k1gNnSc1
</s>
<s>
Brněnské	brněnský	k2eAgNnSc1d1
výstaviště	výstaviště	k1gNnSc1
(	(	kIx(
<g/>
nejznámější	známý	k2eAgFnSc1d3
výstavní	výstavní	k2eAgFnSc1d1
akce	akce	k1gFnSc1
<g/>
:	:	kIx,
Mezinárodní	mezinárodní	k2eAgInSc1d1
strojírenský	strojírenský	k2eAgInSc1d1
veletrh	veletrh	k1gInSc1
<g/>
,	,	kIx,
Invex	Invex	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Výstaviště	výstaviště	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
(	(	kIx(
<g/>
nejznámější	známý	k2eAgFnSc1d3
akce	akce	k1gFnSc1
<g/>
:	:	kIx,
Země	země	k1gFnSc1
živitelka	živitelka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Výstaviště	výstaviště	k1gNnSc1
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
nejznámější	známý	k2eAgFnSc1d3
akce	akce	k1gFnSc1
<g/>
:	:	kIx,
v	v	k7c6
minulosti	minulost	k1gFnSc6
např.	např.	kA
Liberecké	liberecký	k2eAgInPc4d1
výstavní	výstavní	k2eAgInPc4d1
trhy	trh	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Litoměřické	litoměřický	k2eAgNnSc1d1
výstaviště	výstaviště	k1gNnSc1
(	(	kIx(
<g/>
nejznámější	známý	k2eAgFnSc1d3
akce	akce	k1gFnSc1
<g/>
:	:	kIx,
Zahrada	zahrada	k1gFnSc1
Čech	Čechy	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Výstaviště	výstaviště	k1gNnSc1
Louny	Louny	k1gInPc1
</s>
<s>
Výstaviště	výstaviště	k1gNnSc1
Lysá	lysat	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
nejznámější	známý	k2eAgFnSc2d3
výstavní	výstavní	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
:	:	kIx,
Zemědělec	zemědělec	k1gMnSc1
<g/>
,	,	kIx,
Natura	Natura	k1gFnSc1
Viva	Viva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Olomoucké	olomoucký	k2eAgNnSc1d1
výstaviště	výstaviště	k1gNnSc1
(	(	kIx(
<g/>
nejznámější	známý	k2eAgFnSc1d3
akce	akce	k1gFnSc1
<g/>
:	:	kIx,
Flora	Flora	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ostravské	ostravský	k2eAgNnSc1d1
výstaviště	výstaviště	k1gNnSc1
Černá	Černá	k1gFnSc1
louka	louka	k1gFnSc1
</s>
<s>
Výstaviště	výstaviště	k1gNnSc1
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Plzeňské	plzeňský	k2eAgNnSc1d1
výstaviště	výstaviště	k1gNnSc1
</s>
<s>
Výstaviště	výstaviště	k1gNnSc1
Praha-Holešovice	Praha-Holešovice	k1gFnSc2
(	(	kIx(
<g/>
nejvýznamnější	významný	k2eAgFnSc2d3
výstavní	výstavní	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
:	:	kIx,
Jubilejní	jubilejní	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
výstavy	výstava	k1gFnSc2
1791,1891	1791,1891	k4
<g/>
,1991	,1991	k4
<g/>
)	)	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Brněnské	brněnský	k2eAgNnSc1d1
výstaviště	výstaviště	k1gNnSc1
</s>
<s>
Výstaviště	výstaviště	k1gNnSc1
Lysá	lysat	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
