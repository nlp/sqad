<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
orgánem	orgán	k1gInSc7	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
justiční	justiční	k2eAgFnSc6d1	justiční
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
ústředních	ústřední	k2eAgInPc2d1	ústřední
orgánů	orgán	k1gInPc2	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kompetenční	kompetenční	k2eAgInSc1d1	kompetenční
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
)	)	kIx)	)
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
základní	základní	k2eAgFnSc4d1	základní
působnost	působnost	k1gFnSc4	působnost
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
kompetence	kompetence	k1gFnPc1	kompetence
jsou	být	k5eAaImIp3nP	být
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
zákonech	zákon	k1gInPc6	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oblasti	oblast	k1gFnPc1	oblast
působnosti	působnost	k1gFnSc2	působnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
orgánem	orgán	k1gInSc7	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
pro	pro	k7c4	pro
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
státní	státní	k2eAgNnSc4d1	státní
zastupitelství	zastupitelství	k1gNnSc4	zastupitelství
<g/>
,	,	kIx,	,
probaci	probace	k1gFnSc4	probace
a	a	k8xC	a
mediaci	mediace	k1gFnSc4	mediace
a	a	k8xC	a
vězeňství	vězeňství	k1gNnSc4	vězeňství
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
nadřízeno	nadřízen	k2eAgNnSc1d1	nadřízeno
Vězeňské	vězeňský	k2eAgNnSc1d1	vězeňské
službě	služba	k1gFnSc3	služba
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
její	její	k3xOp3gFnSc4	její
telekomunikační	telekomunikační	k2eAgFnSc4d1	telekomunikační
síť	síť	k1gFnSc4	síť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
právní	právní	k2eAgInPc4d1	právní
posudky	posudek	k1gInPc4	posudek
k	k	k7c3	k
úvěrovým	úvěrový	k2eAgFnPc3d1	úvěrová
a	a	k8xC	a
garančním	garanční	k2eAgFnPc3d1	garanční
dohodám	dohoda	k1gFnPc3	dohoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
smluvní	smluvní	k2eAgFnSc1d1	smluvní
stranou	stranou	k6eAd1	stranou
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
u	u	k7c2	u
Evropského	evropský	k2eAgInSc2d1	evropský
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
při	při	k7c6	při
vyřizování	vyřizování	k1gNnSc6	vyřizování
stížností	stížnost	k1gFnPc2	stížnost
na	na	k7c4	na
porušení	porušení	k1gNnSc4	porušení
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
Protokolů	protokol	k1gInPc2	protokol
a	a	k8xC	a
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
paktu	pakt	k1gInSc2	pakt
o	o	k7c6	o
občanských	občanský	k2eAgInPc6d1	občanský
a	a	k8xC	a
politických	politický	k2eAgInPc6d1	politický
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
provádění	provádění	k1gNnSc4	provádění
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
příslušných	příslušný	k2eAgInPc2d1	příslušný
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
orgánů	orgán	k1gInPc2	orgán
</s>
</p>
<p>
<s>
řídí	řídit	k5eAaImIp3nS	řídit
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
kriminologii	kriminologie	k1gFnSc4	kriminologie
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
prevenci	prevence	k1gFnSc4	prevence
<g/>
,	,	kIx,	,
Rejstřík	rejstřík	k1gInSc4	rejstřík
trestů	trest	k1gInPc2	trest
a	a	k8xC	a
Justiční	justiční	k2eAgFnSc4d1	justiční
akademii	akademie	k1gFnSc4	akademie
</s>
</p>
<p>
<s>
vede	vést	k5eAaImIp3nS	vést
seznam	seznam	k1gInSc1	seznam
rozhodců	rozhodce	k1gMnPc2	rozhodce
pro	pro	k7c4	pro
spotřebitelské	spotřebitelský	k2eAgInPc4d1	spotřebitelský
spory	spor	k1gInPc4	spor
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
sídlilo	sídlit	k5eAaImAgNnS	sídlit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
správním	správní	k2eAgInSc7d1	správní
soudem	soud	k1gInSc7	soud
(	(	kIx(	(
<g/>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
kadetky	kadetka	k1gFnSc2	kadetka
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Mariánské	mariánský	k2eAgFnSc2d1	Mariánská
hradby	hradba	k1gFnSc2	hradba
2	[number]	k4	2
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vykonávalo	vykonávat	k5eAaImAgNnS	vykonávat
také	také	k9	také
správu	správa	k1gFnSc4	správa
státního	státní	k2eAgNnSc2d1	státní
notářství	notářství	k1gNnSc2	notářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ministrů	ministr	k1gMnPc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ministrů	ministr	k1gMnPc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
instrukcí	instrukce	k1gFnPc2	instrukce
a	a	k8xC	a
sdělení	sdělení	k1gNnSc2	sdělení
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
