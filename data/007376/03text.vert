<s>
Dub	dub	k1gInSc1	dub
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bukovitých	bukovití	k1gMnPc2	bukovití
<g/>
.	.	kIx.	.
</s>
<s>
Duby	dub	k1gInPc1	dub
jsou	být	k5eAaImIp3nP	být
stálezelené	stálezelený	k2eAgFnPc4d1	stálezelená
nebo	nebo	k8xC	nebo
opadavé	opadavý	k2eAgFnPc4d1	opadavá
dřeviny	dřevina	k1gFnPc4	dřevina
se	s	k7c7	s
střídavými	střídavý	k2eAgInPc7d1	střídavý
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
listy	list	k1gInPc7	list
a	a	k8xC	a
nenápadnými	nápadný	k2eNgInPc7d1	nenápadný
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
rod	rod	k1gInSc1	rod
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
300	[number]	k4	300
až	až	k9	až
600	[number]	k4	600
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Duby	dub	k1gInPc1	dub
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
především	především	k9	především
v	v	k7c6	v
mírných	mírný	k2eAgFnPc6d1	mírná
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc6d1	subtropická
oblastech	oblast	k1gFnPc6	oblast
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
,	,	kIx,	,
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
však	však	k9	však
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
i	i	k9	i
do	do	k7c2	do
tropů	trop	k1gInPc2	trop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
původních	původní	k2eAgInPc2d1	původní
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
dubů	dub	k1gInPc2	dub
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgMnSc1d3	Nejběžnější
jsou	být	k5eAaImIp3nP	být
dub	dub	k1gInSc1	dub
letní	letní	k2eAgInSc1d1	letní
a	a	k8xC	a
dub	dub	k1gInSc1	dub
zimní	zimní	k2eAgInSc1d1	zimní
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
cizokrajné	cizokrajný	k2eAgInPc1d1	cizokrajný
druhy	druh	k1gInPc1	druh
dubů	dub	k1gInPc2	dub
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
jako	jako	k9	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
dřeviny	dřevina	k1gFnPc1	dřevina
<g/>
,	,	kIx,	,
severoamerický	severoamerický	k2eAgInSc1d1	severoamerický
dub	dub	k1gInSc1	dub
červený	červený	k2eAgInSc1d1	červený
se	se	k3xPyFc4	se
vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
i	i	k9	i
do	do	k7c2	do
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgInSc1d1	lesní
porost	porost	k1gInSc1	porost
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
převažuje	převažovat	k5eAaImIp3nS	převažovat
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
doubrava	doubrava	k1gFnSc1	doubrava
nebo	nebo	k8xC	nebo
dubina	dubina	k1gFnSc1	dubina
<g/>
.	.	kIx.	.
</s>
<s>
Duby	dub	k1gInPc1	dub
jsou	být	k5eAaImIp3nP	být
dlouhověké	dlouhověký	k2eAgInPc1d1	dlouhověký
<g/>
,	,	kIx,	,
pomalu	pomalu	k6eAd1	pomalu
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
opadavé	opadavý	k2eAgInPc4d1	opadavý
nebo	nebo	k8xC	nebo
stálezelené	stálezelený	k2eAgInPc4d1	stálezelený
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
i	i	k9	i
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
s	s	k7c7	s
tvrdým	tvrdý	k2eAgNnSc7d1	tvrdé
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Borka	borka	k1gFnSc1	borka
bývá	bývat	k5eAaImIp3nS	bývat
hluboce	hluboko	k6eAd1	hluboko
brázditá	brázditý	k2eAgFnSc1d1	brázditá
nebo	nebo	k8xC	nebo
podélně	podélně	k6eAd1	podélně
odlupčivá	odlupčivý	k2eAgFnSc1d1	odlupčivá
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgInPc1d1	zimní
pupeny	pupen	k1gInPc1	pupen
jsou	být	k5eAaImIp3nP	být
vejcovité	vejcovitý	k2eAgInPc1d1	vejcovitý
<g/>
,	,	kIx,	,
s	s	k7c7	s
okrouhlým	okrouhlý	k2eAgInSc7d1	okrouhlý
či	či	k8xC	či
hranatým	hranatý	k2eAgInSc7d1	hranatý
průřezem	průřez	k1gInSc7	průřez
<g/>
,	,	kIx,	,
nahloučené	nahloučený	k2eAgInPc1d1	nahloučený
při	při	k7c6	při
koncích	konec	k1gInPc6	konec
větévek	větévka	k1gFnPc2	větévka
a	a	k8xC	a
kryté	krytý	k2eAgInPc4d1	krytý
několika	několik	k4yIc2	několik
až	až	k9	až
mnoha	mnoho	k4c2	mnoho
střechovitě	střechovitě	k6eAd1	střechovitě
se	s	k7c7	s
překrývajícími	překrývající	k2eAgFnPc7d1	překrývající
šupinami	šupina	k1gFnPc7	šupina
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc1	lista
jsou	být	k5eAaImIp3nP	být
střídavé	střídavý	k2eAgFnPc1d1	střídavá
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
s	s	k7c7	s
opadavými	opadavý	k2eAgInPc7d1	opadavý
nenápadnými	nápadný	k2eNgInPc7d1	nenápadný
palisty	palist	k1gInPc7	palist
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gInSc1	čepel
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
laločnatá	laločnatý	k2eAgFnSc1d1	laločnatá
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
celistvá	celistvý	k2eAgFnSc1d1	celistvá
se	se	k3xPyFc4	se
zubatým	zubatý	k2eAgInSc7d1	zubatý
okrajem	okraj	k1gInSc7	okraj
nebo	nebo	k8xC	nebo
celokrajná	celokrajný	k2eAgNnPc1d1	celokrajné
<g/>
,	,	kIx,	,
měkká	měkký	k2eAgNnPc1d1	měkké
nebo	nebo	k8xC	nebo
tuhá	tuhý	k2eAgNnPc1d1	tuhé
<g/>
.	.	kIx.	.
</s>
<s>
Žilnatina	žilnatina	k1gFnSc1	žilnatina
je	být	k5eAaImIp3nS	být
zpeřená	zpeřený	k2eAgFnSc1d1	zpeřená
<g/>
,	,	kIx,	,
s	s	k7c7	s
postranními	postranní	k2eAgFnPc7d1	postranní
žilkami	žilka	k1gFnPc7	žilka
buď	buď	k8xC	buď
paralelními	paralelní	k2eAgFnPc7d1	paralelní
<g/>
,	,	kIx,	,
nevětvenými	větvený	k2eNgInPc7d1	nevětvený
a	a	k8xC	a
běžícími	běžící	k2eAgInPc7d1	běžící
až	až	k9	až
k	k	k7c3	k
listovému	listový	k2eAgInSc3d1	listový
okraji	okraj	k1gInSc3	okraj
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
větvenými	větvený	k2eAgFnPc7d1	větvená
a	a	k8xC	a
spojujícími	spojující	k2eAgFnPc7d1	spojující
se	se	k3xPyFc4	se
před	před	k7c7	před
dosažením	dosažení	k1gNnSc7	dosažení
listového	listový	k2eAgInSc2d1	listový
okraje	okraj	k1gInSc2	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgInPc1d1	drobný
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednopohlavních	jednopohlavní	k2eAgNnPc6d1	jednopohlavní
květenstvích	květenství	k1gNnPc6	květenství
nahloučených	nahloučený	k2eAgFnPc2d1	nahloučená
nejčastěji	často	k6eAd3	často
při	při	k7c6	při
bázi	báze	k1gFnSc6	báze
ročních	roční	k2eAgInPc2d1	roční
výmladků	výmladek	k1gInPc2	výmladek
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgNnPc1d1	samčí
květenství	květenství	k1gNnPc1	květenství
jsou	být	k5eAaImIp3nP	být
nící	nící	k2eAgFnPc4d1	nící
jehnědy	jehněda	k1gFnPc4	jehněda
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgInPc1d1	samčí
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
květenství	květenství	k1gNnSc2	květenství
<g/>
,	,	kIx,	,
s	s	k7c7	s
kalichovitým	kalichovitý	k2eAgInSc7d1	kalichovitý
<g/>
,	,	kIx,	,
čtyř	čtyři	k4xCgNnPc2	čtyři
až	až	k8xS	až
sedmilaločným	sedmilaločný	k2eAgNnSc7d1	sedmilaločný
okvětím	okvětí	k1gNnSc7	okvětí
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
se	s	k7c7	s
šesti	šest	k4xCc7	šest
(	(	kIx(	(
<g/>
2	[number]	k4	2
až	až	k9	až
12	[number]	k4	12
<g/>
)	)	kIx)	)
tyčinkami	tyčinka	k1gFnPc7	tyčinka
s	s	k7c7	s
tenkými	tenký	k2eAgFnPc7d1	tenká
nitkami	nitka	k1gFnPc7	nitka
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgNnPc1d1	samičí
květenství	květenství	k1gNnPc1	květenství
jsou	být	k5eAaImIp3nP	být
chudokvěté	chudokvětý	k2eAgFnPc4d1	chudokvětá
jehnědy	jehněda	k1gFnPc4	jehněda
nebo	nebo	k8xC	nebo
strboulky	strboulek	k1gInPc4	strboulek
<g/>
,	,	kIx,	,
vyvíjející	vyvíjející	k2eAgInSc4d1	vyvíjející
se	se	k3xPyFc4	se
v	v	k7c6	v
paždí	paždí	k1gNnSc6	paždí
listů	list	k1gInPc2	list
blíže	blízce	k6eAd2	blízce
konci	konec	k1gInSc3	konec
větévky	větévka	k1gFnSc2	větévka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
samičího	samičí	k2eAgInSc2d1	samičí
květu	květ	k1gInSc2	květ
je	být	k5eAaImIp3nS	být
miskovitá	miskovitý	k2eAgFnSc1d1	miskovitá
číška	číška	k1gFnSc1	číška
<g/>
.	.	kIx.	.
</s>
<s>
Okvětí	okvětí	k1gNnSc1	okvětí
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
šestilaločné	šestilaločný	k2eAgNnSc1d1	šestilaločný
<g/>
,	,	kIx,	,
plodolisty	plodolist	k1gInPc1	plodolist
a	a	k8xC	a
čnělky	čnělka	k1gFnPc1	čnělka
bývají	bývat	k5eAaImIp3nP	bývat
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
šest	šest	k4xCc1	šest
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
nažka	nažka	k1gFnSc1	nažka
(	(	kIx(	(
<g/>
označovaná	označovaný	k2eAgFnSc1d1	označovaná
též	též	k9	též
jako	jako	k8xS	jako
oříšek	oříšek	k1gInSc1	oříšek
<g/>
)	)	kIx)	)
podepřená	podepřený	k2eAgFnSc1d1	podepřená
zveličelou	zveličelý	k2eAgFnSc7d1	zveličelý
dřevnatou	dřevnatý	k2eAgFnSc7d1	dřevnatá
číškou	číška	k1gFnSc7	číška
a	a	k8xC	a
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
žalud	žalud	k1gInSc1	žalud
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
v	v	k7c6	v
prvním	první	k4xOgMnSc6	první
nebo	nebo	k8xC	nebo
až	až	k9	až
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
rodu	rod	k1gInSc2	rod
dub	dub	k1gInSc1	dub
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mírné	mírný	k2eAgNnSc1d1	mírné
až	až	k8xS	až
subtropické	subtropický	k2eAgNnSc1d1	subtropické
pásmo	pásmo	k1gNnSc1	pásmo
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
s	s	k7c7	s
nehojnými	hojný	k2eNgInPc7d1	nehojný
přesahy	přesah	k1gInPc7	přesah
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
tropického	tropický	k2eAgNnSc2d1	tropické
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
duby	dub	k1gInPc1	dub
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
i	i	k9	i
do	do	k7c2	do
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
humboldtii	humboldtie	k1gFnSc4	humboldtie
<g/>
)	)	kIx)	)
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
kolumbijských	kolumbijský	k2eAgFnPc6d1	kolumbijská
Andách	Anda	k1gFnPc6	Anda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
duby	dub	k1gInPc1	dub
i	i	k9	i
do	do	k7c2	do
tropické	tropický	k2eAgFnSc2d1	tropická
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
primárních	primární	k2eAgInPc6d1	primární
lesích	les	k1gInPc6	les
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
0	[number]	k4	0
až	až	k6eAd1	až
3350	[number]	k4	3350
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
druhů	druh	k1gInPc2	druh
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
rostou	růst	k5eAaImIp3nP	růst
také	také	k9	také
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Česka	Česko	k1gNnSc2	Česko
rostou	růst	k5eAaImIp3nP	růst
dub	dub	k1gInSc1	dub
zimní	zimní	k2eAgInSc1d1	zimní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
petraea	petrae	k1gInSc2	petrae
<g/>
)	)	kIx)	)
a	a	k8xC	a
dub	dub	k1gInSc1	dub
letní	letní	k2eAgInSc1d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
robur	robura	k1gFnPc2	robura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejteplejších	teplý	k2eAgFnPc6d3	nejteplejší
oblastech	oblast	k1gFnPc6	oblast
Česka	Česko	k1gNnSc2	Česko
roste	růst	k5eAaImIp3nS	růst
dub	dub	k1gInSc1	dub
pýřitý	pýřitý	k2eAgInSc1d1	pýřitý
čili	čili	k8xC	čili
šipák	šipák	k1gInSc1	šipák
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
pubescens	pubescens	k1gInSc1	pubescens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
svým	svůj	k3xOyFgInSc7	svůj
areálem	areál	k1gInSc7	areál
dub	dub	k1gInSc1	dub
cer	cer	k1gInSc1	cer
neboli	neboli	k8xC	neboli
dub	dub	k1gInSc1	dub
slovenský	slovenský	k2eAgInSc1d1	slovenský
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
cerris	cerris	k1gFnSc2	cerris
<g/>
)	)	kIx)	)
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
dub	dub	k1gInSc1	dub
balkánský	balkánský	k2eAgInSc1d1	balkánský
neboli	neboli	k8xC	neboli
uherský	uherský	k2eAgInSc1d1	uherský
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
frainetto	frainetto	k1gNnSc1	frainetto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
rozlišovány	rozlišován	k2eAgInPc1d1	rozlišován
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
dubu	dub	k1gInSc3	dub
zimnímu	zimní	k2eAgInSc3d1	zimní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dub	dub	k1gInSc1	dub
žlutavý	žlutavý	k2eAgInSc1d1	žlutavý
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
dalechampii	dalechampie	k1gFnSc4	dalechampie
<g/>
)	)	kIx)	)
a	a	k8xC	a
dub	dub	k1gInSc1	dub
mnohoplodý	mnohoplodý	k2eAgInSc1d1	mnohoplodý
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
polycarpa	polycarp	k1gMnSc2	polycarp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odlišit	odlišit	k5eAaPmF	odlišit
je	on	k3xPp3gFnPc4	on
však	však	k9	však
dokáže	dokázat	k5eAaPmIp3nS	dokázat
jen	jen	k9	jen
specialista	specialista	k1gMnSc1	specialista
a	a	k8xC	a
některými	některý	k3yIgMnPc7	některý
autory	autor	k1gMnPc7	autor
je	být	k5eAaImIp3nS	být
taxonomický	taxonomický	k2eAgInSc1d1	taxonomický
význam	význam	k1gInSc1	význam
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
zpochybňován	zpochybňován	k2eAgInSc1d1	zpochybňován
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
dubu	dub	k1gInSc2	dub
jadranského	jadranský	k2eAgInSc2d1	jadranský
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gMnSc1	Quercus
virgiliana	virgilian	k1gMnSc2	virgilian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k1gMnSc1	příbuzný
dubu	dub	k1gInSc2	dub
pýřitému	pýřitý	k2eAgInSc3d1	pýřitý
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
dub	dub	k1gInSc1	dub
sivozelený	sivozelený	k2eAgInSc1d1	sivozelený
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
pedunculiflora	pedunculiflora	k1gFnSc1	pedunculiflora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k1gMnSc1	příbuzný
dubu	dub	k1gInSc2	dub
letnímu	letní	k2eAgNnSc3d1	letní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc7	jeho
výskyt	výskyt	k1gInSc1	výskyt
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
i	i	k8xC	i
Slovenska	Slovensko	k1gNnSc2	Slovensko
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
pochybný	pochybný	k2eAgMnSc1d1	pochybný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
původních	původní	k2eAgInPc2d1	původní
okolo	okolo	k7c2	okolo
23	[number]	k4	23
druhů	druh	k1gInPc2	druh
dubů	dub	k1gInPc2	dub
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celém	celý	k2eAgNnSc6d1	celé
Středomoří	středomoří	k1gNnSc6	středomoří
je	být	k5eAaImIp3nS	být
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
dub	dub	k1gInSc1	dub
kermesový	kermesový	k2eAgInSc1d1	kermesový
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
coccifera	coccifera	k1gFnSc1	coccifera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
křovinatých	křovinatý	k2eAgInPc6d1	křovinatý
porostech	porost	k1gInPc6	porost
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgMnPc2d1	nazývaný
makchie	makchie	k1gFnSc2	makchie
a	a	k8xC	a
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původních	původní	k2eAgInPc2d1	původní
středomořských	středomořský	k2eAgInPc2d1	středomořský
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
původní	původní	k2eAgInSc1d1	původní
dub	dub	k1gInSc1	dub
cesmínovitý	cesmínovitý	k2eAgInSc1d1	cesmínovitý
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
ilex	ilex	k1gInSc1	ilex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
již	již	k6eAd1	již
omezenější	omezený	k2eAgInPc1d2	omezenější
areály	areál	k1gInPc1	areál
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Středomoří	středomoří	k1gNnSc6	středomoří
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dub	dub	k1gInSc1	dub
korkový	korkový	k2eAgInSc1d1	korkový
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
suber	suber	k1gInSc1	suber
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
dub	dub	k1gInSc1	dub
alžírský	alžírský	k2eAgInSc1d1	alžírský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
canariensis	canariensis	k1gFnSc2	canariensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
portugalský	portugalský	k2eAgInSc1d1	portugalský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
faginea	faginea	k1gFnSc1	faginea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
pyrenaica	pyrenaica	k1gFnSc1	pyrenaica
<g/>
)	)	kIx)	)
a	a	k8xC	a
duby	dub	k1gInPc1	dub
Q.	Q.	kA	Q.
lusitanica	lusitanica	k6eAd1	lusitanica
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Q.	Q.	kA	Q.
cerrioides	cerrioides	k1gInSc1	cerrioides
a	a	k8xC	a
Q.	Q.	kA	Q.
mas	masa	k1gFnPc2	masa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
dub	dub	k1gInSc1	dub
balkánský	balkánský	k2eAgMnSc1d1	balkánský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
frainetto	frainetto	k1gNnSc4	frainetto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
trojský	trojský	k2eAgInSc1d1	trojský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
trojana	trojana	k1gFnSc1	trojana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
sivozelený	sivozelený	k2eAgInSc1d1	sivozelený
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
pedunculiflora	pedunculiflora	k1gFnSc1	pedunculiflora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
šupinatý	šupinatý	k2eAgInSc1d1	šupinatý
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
macrolepis	macrolepis	k1gFnSc2	macrolepis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
Hartwissův	Hartwissův	k2eAgInSc1d1	Hartwissův
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
hartwisiana	hartwisiana	k1gFnSc1	hartwisiana
<g/>
)	)	kIx)	)
a	a	k8xC	a
Q.	Q.	kA	Q.
haas	haas	k1gInSc1	haas
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
na	na	k7c6	na
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
dub	dub	k1gInSc1	dub
Q.	Q.	kA	Q.
congesta	congesta	k1gFnSc1	congesta
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
Q.	Q.	kA	Q.
sicula	sicul	k1gMnSc2	sicul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
IUCN	IUCN	kA	IUCN
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
jako	jako	k9	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
druhy	druh	k1gInPc1	druh
dubů	dub	k1gInPc2	dub
<g/>
:	:	kIx,	:
Quercus	Quercus	k1gInSc1	Quercus
graciliformis	graciliformis	k1gFnSc2	graciliformis
<g/>
,	,	kIx,	,
Quercus	Quercus	k1gMnSc1	Quercus
hinckleyi	hinckley	k1gFnSc2	hinckley
a	a	k8xC	a
Quercus	Quercus	k1gInSc1	Quercus
tardifolia	tardifolium	k1gNnSc2	tardifolium
z	z	k7c2	z
Texasu	Texas	k1gInSc2	Texas
a	a	k8xC	a
Quercus	Quercus	k1gInSc1	Quercus
hintonii	hintonie	k1gFnSc4	hintonie
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
je	být	k5eAaImIp3nS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
8	[number]	k4	8
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k9	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
dub	dub	k1gInSc1	dub
cer	cer	k1gInSc1	cer
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
cerris	cerris	k1gFnSc2	cerris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobné	k1gInPc1	drobné
<g/>
,	,	kIx,	,
nenápadné	nápadný	k2eNgInPc1d1	nenápadný
květy	květ	k1gInPc1	květ
dubů	dub	k1gInPc2	dub
jsou	být	k5eAaImIp3nP	být
opylovány	opylovat	k5eAaImNgFnP	opylovat
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Žaludy	žalud	k1gInPc1	žalud
našich	náš	k3xOp1gInPc2	náš
dubů	dub	k1gInPc2	dub
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
<g/>
,	,	kIx,	,
plši	plch	k1gMnPc1	plch
a	a	k8xC	a
veverky	veverka	k1gFnPc1	veverka
<g/>
.	.	kIx.	.
</s>
<s>
Duby	dub	k1gInPc1	dub
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
potravu	potrava	k1gFnSc4	potrava
celé	celý	k2eAgFnSc3d1	celá
škále	škála	k1gFnSc3	škála
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Listím	listí	k1gNnSc7	listí
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
housenky	housenka	k1gFnSc2	housenka
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
denních	denní	k2eAgMnPc2d1	denní
i	i	k8xC	i
nočních	noční	k2eAgMnPc2d1	noční
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
obaleč	obaleč	k1gMnSc1	obaleč
dubový	dubový	k2eAgMnSc1d1	dubový
(	(	kIx(	(
<g/>
Tortrix	Tortrix	k1gInSc1	Tortrix
viridana	viridan	k1gMnSc2	viridan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bourovec	bourovec	k1gMnSc1	bourovec
dubový	dubový	k2eAgMnSc1d1	dubový
(	(	kIx(	(
<g/>
Lasiocampa	Lasiocampa	k1gFnSc1	Lasiocampa
quercus	quercus	k1gMnSc1	quercus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostruháček	ostruháček	k1gMnSc1	ostruháček
česvinový	česvinový	k2eAgMnSc1d1	česvinový
(	(	kIx(	(
<g/>
Satyrium	Satyrium	k1gNnSc1	Satyrium
ilicis	ilicis	k1gFnPc2	ilicis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostruháček	ostruháček	k1gMnSc1	ostruháček
dubový	dubový	k2eAgMnSc1d1	dubový
(	(	kIx(	(
<g/>
Neozephyrus	Neozephyrus	k1gMnSc1	Neozephyrus
quercus	quercus	k1gMnSc1	quercus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bourovčík	bourovčík	k1gMnSc1	bourovčík
toulavý	toulavý	k2eAgMnSc1d1	toulavý
(	(	kIx(	(
<g/>
Thaumetopoea	Thaumetopoea	k1gMnSc1	Thaumetopoea
processionea	processionea	k1gMnSc1	processionea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hřbetozubec	hřbetozubec	k1gInSc4	hřbetozubec
Milhauserův	Milhauserův	k2eAgInSc4d1	Milhauserův
(	(	kIx(	(
<g/>
Harpyia	Harpyia	k1gFnSc1	Harpyia
milhauseri	milhauser	k1gFnSc2	milhauser
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stužkonoska	stužkonoska	k1gFnSc1	stužkonoska
úskopásná	úskopásný	k2eAgFnSc1d1	úskopásný
(	(	kIx(	(
<g/>
Catocala	Catocala	k1gFnSc1	Catocala
promissa	promissa	k1gFnSc1	promissa
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Hálky	hálka	k1gFnPc1	hálka
na	na	k7c6	na
listech	list	k1gInPc6	list
či	či	k8xC	či
větévkách	větévka	k1gFnPc6	větévka
tvoří	tvořit	k5eAaImIp3nS	tvořit
různé	různý	k2eAgFnPc4d1	různá
žlabatky	žlabatka	k1gFnPc4	žlabatka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
žlabatka	žlabatka	k1gFnSc1	žlabatka
duběnková	duběnkový	k2eAgFnSc1d1	duběnkový
(	(	kIx(	(
<g/>
Andricus	Andricus	k1gMnSc1	Andricus
kollari	kollar	k1gFnSc2	kollar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlabatka	žlabatka	k1gFnSc1	žlabatka
dubová	dubový	k2eAgFnSc1d1	dubová
(	(	kIx(	(
<g/>
Cynips	Cynips	k1gInSc1	Cynips
quercus-folii	quercusolie	k1gFnSc4	quercus-folie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlabatka	žlabatka	k1gFnSc1	žlabatka
penízková	penízkový	k2eAgFnSc1d1	penízková
(	(	kIx(	(
<g/>
Neuroterus	Neuroterus	k1gMnSc1	Neuroterus
numismalis	numismalis	k1gFnSc2	numismalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlabatka	žlabatka	k1gFnSc1	žlabatka
hrášková	hráškový	k2eAgFnSc1d1	hrášková
(	(	kIx(	(
<g/>
N.	N.	kA	N.
quercusbaccarum	quercusbaccarum	k1gNnSc4	quercusbaccarum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlabatka	žlabatka	k1gFnSc1	žlabatka
šišticová	šišticový	k2eAgFnSc1d1	šišticový
(	(	kIx(	(
<g/>
Andricus	Andricus	k1gInSc1	Andricus
foecundatrix	foecundatrix	k1gInSc1	foecundatrix
<g/>
)	)	kIx)	)
či	či	k8xC	či
žlabatka	žlabatka	k1gFnSc1	žlabatka
kalichová	kalichový	k2eAgFnSc1d1	Kalichová
(	(	kIx(	(
<g/>
Andricus	Andricus	k1gMnSc1	Andricus
quercuscalicis	quercuscalicis	k1gFnSc2	quercuscalicis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živém	živý	k2eAgNnSc6d1	živé
dřevě	dřevo	k1gNnSc6	dřevo
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
larvy	larva	k1gFnPc1	larva
tesaříka	tesařík	k1gMnSc2	tesařík
obrovského	obrovský	k2eAgMnSc2d1	obrovský
(	(	kIx(	(
<g/>
Cerambyx	Cerambyx	k1gInSc1	Cerambyx
cerdo	cerdo	k1gNnSc1	cerdo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
odumřelých	odumřelý	k2eAgFnPc6d1	odumřelá
částech	část	k1gFnPc6	část
žijí	žít	k5eAaImIp3nP	žít
larvy	larva	k1gFnPc1	larva
tesaříka	tesařík	k1gMnSc2	tesařík
dubového	dubový	k2eAgMnSc2d1	dubový
(	(	kIx(	(
<g/>
Plagionotus	Plagionotus	k1gInSc1	Plagionotus
arcuatus	arcuatus	k1gInSc1	arcuatus
<g/>
)	)	kIx)	)
či	či	k8xC	či
tesaříka	tesařík	k1gMnSc2	tesařík
Pyrrhinium	Pyrrhinium	k1gNnSc4	Pyrrhinium
sanguineum	sanguineum	k1gInSc4	sanguineum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trouchu	trouch	k1gInSc6	trouch
dubových	dubový	k2eAgInPc2d1	dubový
stromů	strom	k1gInPc2	strom
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
larvy	larva	k1gFnPc1	larva
našeho	náš	k3xOp1gMnSc2	náš
největšího	veliký	k2eAgMnSc2d3	veliký
brouka	brouk	k1gMnSc2	brouk
<g/>
,	,	kIx,	,
roháče	roháč	k1gMnSc2	roháč
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Lucanus	Lucanus	k1gInSc1	Lucanus
cervus	cervus	k1gInSc1	cervus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
dub	dub	k1gInSc1	dub
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
taxonomii	taxonomie	k1gFnSc6	taxonomie
členěn	členit	k5eAaImNgInS	členit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
podrodů	podrod	k1gInPc2	podrod
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
šesti	šest	k4xCc2	šest
sekcí	sekce	k1gFnPc2	sekce
<g/>
:	:	kIx,	:
Podrod	podrod	k1gInSc1	podrod
Cyclobalanopsis	Cyclobalanopsis	k1gFnSc2	Cyclobalanopsis
–	–	k?	–
stálezelené	stálezelený	k2eAgInPc4d1	stálezelený
duby	dub	k1gInPc4	dub
s	s	k7c7	s
celokrajnými	celokrajný	k2eAgInPc7d1	celokrajný
nebo	nebo	k8xC	nebo
zubatými	zubatý	k2eAgInPc7d1	zubatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nelaločnatými	laločnatý	k2eNgInPc7d1	laločnatý
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
Podrod	podrod	k1gInSc4	podrod
Quercus	Quercus	k1gMnSc1	Quercus
sekce	sekce	k1gFnSc2	sekce
Quercus	Quercus	k1gMnSc1	Quercus
–	–	k?	–
opadavé	opadavý	k2eAgInPc1d1	opadavý
i	i	k8xC	i
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
duby	dub	k1gInPc1	dub
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
v	v	k7c6	v
Aurasii	Aurasie	k1gFnSc6	Aurasie
i	i	k8xC	i
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
sem	sem	k6eAd1	sem
většina	většina	k1gFnSc1	většina
našich	náš	k3xOp1gInPc2	náš
dubů	dub	k1gInPc2	dub
<g/>
.	.	kIx.	.
sekce	sekce	k1gFnSc1	sekce
Mesobalanus	Mesobalanus	k1gMnSc1	Mesobalanus
–	–	k?	–
žaludy	žalud	k1gInPc1	žalud
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
hořké	hořký	k2eAgFnPc1d1	hořká
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
našich	náš	k3xOp1gInPc2	náš
dubů	dub	k1gInPc2	dub
sem	sem	k6eAd1	sem
náleží	náležet	k5eAaImIp3nS	náležet
dub	dub	k1gInSc1	dub
balkánský	balkánský	k2eAgInSc1d1	balkánský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
frainetto	frainetto	k1gNnSc4	frainetto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
dub	dub	k1gInSc1	dub
velkokvětý	velkokvětý	k2eAgInSc1d1	velkokvětý
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
macranthera	macranthera	k1gFnSc1	macranthera
<g/>
)	)	kIx)	)
a	a	k8xC	a
dub	dub	k1gInSc1	dub
zubatý	zubatý	k2eAgInSc1d1	zubatý
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
dentata	dentata	k1gFnSc1	dentata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
sekce	sekce	k1gFnSc1	sekce
Cerris	Cerris	k1gFnSc2	Cerris
–	–	k?	–
opadavé	opadavý	k2eAgInPc1d1	opadavý
i	i	k8xC	i
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
duby	dub	k1gInPc1	dub
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
sem	sem	k6eAd1	sem
náš	náš	k3xOp1gInSc1	náš
dub	dub	k1gInSc1	dub
cer	cer	k1gInSc4	cer
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
cerris	cerris	k1gFnSc2	cerris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
zejména	zejména	k9	zejména
dub	dub	k1gInSc1	dub
libanonský	libanonský	k2eAgInSc1d1	libanonský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
libani	libaň	k1gFnSc6	libaň
<g/>
)	)	kIx)	)
a	a	k8xC	a
dub	dub	k1gInSc1	dub
špičatolistý	špičatolistý	k2eAgInSc1d1	špičatolistý
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
acutissima	acutissima	k1gNnSc4	acutissima
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
středomořských	středomořský	k2eAgInPc2d1	středomořský
druhů	druh	k1gInPc2	druh
např.	např.	kA	např.
dub	dub	k1gInSc1	dub
korkový	korkový	k2eAgInSc1d1	korkový
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
suber	suber	k1gInSc1	suber
<g/>
)	)	kIx)	)
a	a	k8xC	a
dub	dub	k1gInSc1	dub
kermesový	kermesový	k2eAgInSc1d1	kermesový
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
coccifera	coccifera	k1gFnSc1	coccifera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
sekce	sekce	k1gFnSc1	sekce
Protobalanus	Protobalanus	k1gInSc4	Protobalanus
–	–	k?	–
stálezelené	stálezelený	k2eAgInPc4d1	stálezelený
duby	dub	k1gInPc4	dub
s	s	k7c7	s
celokrajnými	celokrajný	k2eAgInPc7d1	celokrajný
nebo	nebo	k8xC	nebo
zubatými	zubatý	k2eAgInPc7d1	zubatý
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
v	v	k7c6	v
jz	jz	k?	jz
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
a	a	k8xC	a
sz.	sz.	k?	sz.
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
sekce	sekce	k1gFnSc1	sekce
Lobatae	Lobata	k1gFnSc2	Lobata
–	–	k?	–
tzv.	tzv.	kA	tzv.
červené	červený	k2eAgInPc4d1	červený
duby	dub	k1gInPc4	dub
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
sporadicky	sporadicky	k6eAd1	sporadicky
i	i	k8xC	i
Střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Opadavé	opadavý	k2eAgFnPc1d1	opadavá
i	i	k8xC	i
stálezelené	stálezelený	k2eAgFnPc1d1	stálezelená
<g/>
,	,	kIx,	,
s	s	k7c7	s
listy	list	k1gInPc7	list
laločnatými	laločnatý	k2eAgInPc7d1	laločnatý
nebo	nebo	k8xC	nebo
celistvými	celistvý	k2eAgInPc7d1	celistvý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
běžněji	běžně	k6eAd2	běžně
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
dubů	dub	k1gInPc2	dub
sem	sem	k6eAd1	sem
náleží	náležet	k5eAaImIp3nS	náležet
např.	např.	kA	např.
dub	dub	k1gInSc1	dub
červený	červený	k2eAgInSc1d1	červený
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
rubra	rubrum	k1gNnSc2	rubrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
bahenní	bahenní	k2eAgInSc1d1	bahenní
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
a	a	k8xC	a
dub	dub	k1gInSc1	dub
šarlatový	šarlatový	k2eAgInSc1d1	šarlatový
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
coccinea	coccinea	k1gFnSc1	coccinea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrod	podrod	k1gInSc1	podrod
Cyclobalanopsis	Cyclobalanopsis	k1gFnSc2	Cyclobalanopsis
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
pojímán	pojímán	k2eAgInSc1d1	pojímán
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
rod	rod	k1gInSc1	rod
<g/>
.	.	kIx.	.
</s>
<s>
Duby	dub	k1gInPc1	dub
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zdroje	zdroj	k1gInSc2	zdroj
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
dřeva	dřevo	k1gNnSc2	dřevo
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
stromů	strom	k1gInPc2	strom
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Dubové	Dubové	k2eAgNnSc1d1	Dubové
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
trvanlivé	trvanlivý	k2eAgInPc1d1	trvanlivý
a	a	k8xC	a
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
trámů	trám	k1gInPc2	trám
<g/>
,	,	kIx,	,
podlah	podlaha	k1gFnPc2	podlaha
<g/>
,	,	kIx,	,
nábytku	nábytek	k1gInSc2	nábytek
i	i	k8xC	i
sudů	sud	k1gInPc2	sud
<g/>
.	.	kIx.	.
</s>
<s>
Alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
<g/>
,	,	kIx,	,
zrající	zrající	k2eAgInPc1d1	zrající
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
z	z	k7c2	z
dubového	dubový	k2eAgNnSc2d1	dubové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
získávají	získávat	k5eAaImIp3nP	získávat
nezaměnitelnou	zaměnitelný	k2eNgFnSc4d1	nezaměnitelná
příchuť	příchuť	k1gFnSc4	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Žaludy	žalud	k1gInPc1	žalud
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
oblastech	oblast	k1gFnPc6	oblast
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
potravy	potrava	k1gFnSc2	potrava
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jedlé	jedlý	k2eAgInPc1d1	jedlý
žaludy	žalud	k1gInPc1	žalud
mají	mít	k5eAaImIp3nP	mít
např.	např.	kA	např.
středomořský	středomořský	k2eAgInSc4d1	středomořský
dub	dub	k1gInSc4	dub
cesmínovitý	cesmínovitý	k2eAgInSc4d1	cesmínovitý
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
ilex	ilex	k1gInSc1	ilex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
indický	indický	k2eAgMnSc1d1	indický
Q.	Q.	kA	Q.
glauca	glauca	k1gMnSc1	glauca
<g/>
,	,	kIx,	,
íránský	íránský	k2eAgMnSc1d1	íránský
Q.	Q.	kA	Q.
persica	persicum	k1gNnSc2	persicum
a	a	k8xC	a
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
Q.	Q.	kA	Q.
emoryii	emoryie	k1gFnSc4	emoryie
a	a	k8xC	a
Q.	Q.	kA	Q.
obtusifolia	obtusifolia	k1gFnSc1	obtusifolia
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
pražené	pražený	k2eAgFnSc3d1	pražená
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
melou	mlít	k5eAaImIp3nP	mlít
jako	jako	k8xS	jako
přísada	přísada	k1gFnSc1	přísada
do	do	k7c2	do
mouky	mouka	k1gFnSc2	mouka
<g/>
.	.	kIx.	.
</s>
<s>
Žaludy	žalud	k1gInPc1	žalud
slouží	sloužit	k5eAaImIp3nP	sloužit
také	také	k9	také
jako	jako	k9	jako
krmivo	krmivo	k1gNnSc1	krmivo
pro	pro	k7c4	pro
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dubů	dub	k1gInPc2	dub
jsou	být	k5eAaImIp3nP	být
získávány	získáván	k2eAgFnPc1d1	získávána
třísloviny	tříslovina	k1gFnPc1	tříslovina
a	a	k8xC	a
kůra	kůra	k1gFnSc1	kůra
a	a	k8xC	a
listy	lista	k1gFnPc1	lista
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
k	k	k7c3	k
vyčiňování	vyčiňování	k1gNnSc3	vyčiňování
kůží	kůže	k1gFnPc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
tříslovin	tříslovina	k1gFnPc2	tříslovina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
duběnky	duběnka	k1gFnPc1	duběnka
<g/>
,	,	kIx,	,
hálky	hálka	k1gFnPc1	hálka
vznikající	vznikající	k2eAgFnPc1d1	vznikající
na	na	k7c6	na
listech	list	k1gInPc6	list
dubů	dub	k1gInPc2	dub
po	po	k7c6	po
nakladení	nakladení	k1gNnSc6	nakladení
vajíček	vajíčko	k1gNnPc2	vajíčko
do	do	k7c2	do
pupenu	pupen	k1gInSc2	pupen
dubu	dub	k1gInSc2	dub
žlabatkou	žlabatka	k1gFnSc7	žlabatka
<g/>
.	.	kIx.	.
</s>
<s>
Hálky	hálka	k1gFnPc1	hálka
na	na	k7c6	na
listech	list	k1gInPc6	list
dubu	dub	k1gInSc2	dub
hálkového	hálkový	k2eAgInSc2d1	hálkový
z	z	k7c2	z
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
tříslovin	tříslovina	k1gFnPc2	tříslovina
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
prodávány	prodávat	k5eAaImNgInP	prodávat
jako	jako	k9	jako
halepské	halepský	k2eAgFnPc1d1	halepská
duběnky	duběnka	k1gFnPc1	duběnka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dubu	dub	k1gInSc2	dub
korkového	korkový	k2eAgInSc2d1	korkový
<g/>
,	,	kIx,	,
pocházejícího	pocházející	k2eAgInSc2d1	pocházející
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
získáván	získáván	k2eAgInSc4d1	získáván
korek	korek	k1gInSc4	korek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
tento	tento	k3xDgInSc1	tento
dub	dub	k1gInSc1	dub
v	v	k7c6	v
klimaticky	klimaticky	k6eAd1	klimaticky
příhodných	příhodný	k2eAgFnPc6d1	příhodná
oblastech	oblast	k1gFnPc6	oblast
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bylinném	bylinný	k2eAgNnSc6d1	bylinné
léčení	léčení	k1gNnSc6	léčení
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
zejména	zejména	k9	zejména
mladá	mladý	k2eAgFnSc1d1	mladá
kůra	kůra	k1gFnSc1	kůra
dubu	dub	k1gInSc2	dub
letního	letní	k2eAgInSc2d1	letní
<g/>
,	,	kIx,	,
získávaná	získávaný	k2eAgNnPc1d1	získávané
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
tenkých	tenký	k2eAgInPc2d1	tenký
dubových	dubový	k2eAgInPc2d1	dubový
kmínků	kmínek	k1gInPc2	kmínek
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
tříslovin	tříslovina	k1gFnPc2	tříslovina
<g/>
,	,	kIx,	,
katechiny	katechina	k1gFnSc2	katechina
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
ellagovou	ellagový	k2eAgFnSc4d1	ellagová
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
svíravé	svíravý	k2eAgNnSc1d1	svíravé
působení	působení	k1gNnSc1	působení
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
při	při	k7c6	při
průjmech	průjem	k1gInPc6	průjem
a	a	k8xC	a
zánětech	zánět	k1gInPc6	zánět
trávicího	trávicí	k2eAgNnSc2d1	trávicí
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
využívána	využívat	k5eAaPmNgFnS	využívat
i	i	k9	i
zevně	zevně	k6eAd1	zevně
<g/>
.	.	kIx.	.
</s>
<s>
Dub	dub	k1gInSc1	dub
je	být	k5eAaImIp3nS	být
také	také	k9	také
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
v	v	k7c6	v
Bachově	Bachův	k2eAgFnSc6d1	Bachova
květové	květový	k2eAgFnSc6d1	květová
terapii	terapie	k1gFnSc6	terapie
jako	jako	k8xS	jako
esence	esence	k1gFnSc1	esence
č.	č.	k?	č.
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Duby	dub	k1gInPc1	dub
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc4d1	významná
parkové	parkový	k2eAgFnPc4d1	parková
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
našich	náš	k3xOp1gInPc2	náš
domácích	domácí	k2eAgInPc2d1	domácí
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
okrasných	okrasný	k2eAgInPc2d1	okrasný
kultivarů	kultivar	k1gInPc2	kultivar
je	být	k5eAaImIp3nS	být
pěstován	pěstovat	k5eAaImNgInS	pěstovat
zejména	zejména	k9	zejména
severoamerický	severoamerický	k2eAgInSc1d1	severoamerický
dub	dub	k1gInSc1	dub
červený	červený	k2eAgInSc1d1	červený
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
rubra	rubrum	k1gNnSc2	rubrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
místy	místy	k6eAd1	místy
vysazován	vysazován	k2eAgInSc1d1	vysazován
i	i	k9	i
do	do	k7c2	do
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
již	již	k6eAd1	již
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
např.	např.	kA	např.
dub	dub	k1gInSc1	dub
balkánský	balkánský	k2eAgMnSc1d1	balkánský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
frainetto	frainetto	k1gNnSc4	frainetto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
bahenní	bahenní	k2eAgInSc1d1	bahenní
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
palustris	palustris	k1gFnSc1	palustris
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
V	v	k7c6	v
parcích	park	k1gInPc6	park
a	a	k8xC	a
arboretech	arboretum	k1gNnPc6	arboretum
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
nápadnými	nápadný	k2eAgInPc7d1	nápadný
cizokrajnými	cizokrajný	k2eAgInPc7d1	cizokrajný
duby	dub	k1gInPc7	dub
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
dub	dub	k1gInSc1	dub
celokrajný	celokrajný	k2eAgInSc1d1	celokrajný
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
imbricaria	imbricarium	k1gNnSc2	imbricarium
<g/>
)	)	kIx)	)
s	s	k7c7	s
celokrajnými	celokrajný	k2eAgInPc7d1	celokrajný
listy	list	k1gInPc7	list
bez	bez	k7c2	bez
laloků	lalok	k1gInPc2	lalok
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
libanonský	libanonský	k2eAgInSc1d1	libanonský
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
libani	libaň	k1gFnSc6	libaň
<g/>
)	)	kIx)	)
s	s	k7c7	s
kopinatými	kopinatý	k2eAgInPc7d1	kopinatý
zubatými	zubatý	k2eAgInPc7d1	zubatý
listy	list	k1gInPc7	list
nebo	nebo	k8xC	nebo
dub	dub	k1gInSc1	dub
dvoubarevný	dvoubarevný	k2eAgInSc1d1	dvoubarevný
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
bicolor	bicolor	k1gInSc1	bicolor
<g/>
)	)	kIx)	)
s	s	k7c7	s
listy	list	k1gInPc7	list
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
bělavými	bělavý	k2eAgInPc7d1	bělavý
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
pěstované	pěstovaný	k2eAgInPc1d1	pěstovaný
duby	dub	k1gInPc1	dub
jsou	být	k5eAaImIp3nP	být
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dub	dub	k1gInSc1	dub
Turnerův	turnerův	k2eAgInSc1d1	turnerův
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
x	x	k?	x
turneri	turner	k1gFnSc2	turner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Listí	listí	k1gNnSc1	listí
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
dub	dub	k1gInSc1	dub
šarlatový	šarlatový	k2eAgInSc1d1	šarlatový
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
coccinea	coccinea	k1gFnSc1	coccinea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
zbarvuje	zbarvovat	k5eAaImIp3nS	zbarvovat
do	do	k7c2	do
nápadných	nápadný	k2eAgInPc2d1	nápadný
jasně	jasně	k6eAd1	jasně
červených	červený	k2eAgInPc2d1	červený
odstínů	odstín	k1gInPc2	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
dřevu	dřevo	k1gNnSc3	dřevo
a	a	k8xC	a
dlouhověkosti	dlouhověkost	k1gFnSc3	dlouhověkost
je	být	k5eAaImIp3nS	být
dub	dub	k1gInSc1	dub
symbolem	symbol	k1gInSc7	symbol
síly	síla	k1gFnSc2	síla
<g/>
;	;	kIx,	;
dubové	dubový	k2eAgInPc1d1	dubový
háje	háj	k1gInPc1	háj
byly	být	k5eAaImAgInP	být
posvátnými	posvátný	k2eAgNnPc7d1	posvátné
místy	místo	k1gNnPc7	místo
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
,	,	kIx,	,
dub	dub	k1gInSc1	dub
je	být	k5eAaImIp3nS	být
národním	národní	k2eAgInSc7d1	národní
stromem	strom	k1gInSc7	strom
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
Litvy	Litva	k1gFnSc2	Litva
a	a	k8xC	a
neoficiálně	oficiálně	k6eNd1	oficiálně
též	též	k9	též
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Dub	dub	k1gInSc1	dub
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Indoevropanů	Indoevropan	k1gMnPc2	Indoevropan
stromem	strom	k1gInSc7	strom
tradičně	tradičně	k6eAd1	tradičně
zasvěceným	zasvěcený	k2eAgMnPc3d1	zasvěcený
hromovládcům	hromovládce	k1gMnPc3	hromovládce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
slovanský	slovanský	k2eAgInSc4d1	slovanský
Perun	perun	k1gInSc4	perun
<g/>
,	,	kIx,	,
baltský	baltský	k2eAgInSc4d1	baltský
Perkunas	Perkunas	k1gInSc4	Perkunas
<g/>
,	,	kIx,	,
severský	severský	k2eAgInSc4d1	severský
Thór	Thór	k1gInSc4	Thór
nebo	nebo	k8xC	nebo
řecký	řecký	k2eAgInSc4d1	řecký
Zeus	Zeus	k1gInSc4	Zeus
<g/>
.	.	kIx.	.
</s>
<s>
Slované	Slovan	k1gMnPc1	Slovan
tradičně	tradičně	k6eAd1	tradičně
ctili	ctít	k5eAaImAgMnP	ctít
posvátné	posvátný	k2eAgInPc4d1	posvátný
duby	dub	k1gInPc4	dub
zasvěcené	zasvěcený	k2eAgInPc4d1	zasvěcený
Perunovi	Perun	k1gMnSc3	Perun
<g/>
.	.	kIx.	.
</s>
<s>
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
úcta	úcta	k1gFnSc1	úcta
k	k	k7c3	k
dubu	dub	k1gInSc3	dub
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
až	až	k9	až
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
novověku	novověk	k1gInSc2	novověk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Feofan	Feofan	k1gMnSc1	Feofan
Prokopovič	Prokopovič	k1gMnSc1	Prokopovič
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
zakazovat	zakazovat	k5eAaImF	zakazovat
křesťanům	křesťan	k1gMnPc3	křesťan
zpívat	zpívat	k5eAaImF	zpívat
pod	pod	k7c4	pod
duby	dub	k1gInPc4	dub
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
se	se	k3xPyFc4	se
rolníci	rolník	k1gMnPc1	rolník
modlili	modlit	k5eAaImAgMnP	modlit
pod	pod	k7c7	pod
dubem	dub	k1gInSc7	dub
za	za	k7c4	za
déšť	déšť	k1gInSc4	déšť
<g/>
.	.	kIx.	.
</s>
<s>
Jihoslované	Jihoslovan	k1gMnPc1	Jihoslovan
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
bez	bez	k7c2	bez
kostela	kostel	k1gInSc2	kostel
konali	konat	k5eAaImAgMnP	konat
pod	pod	k7c7	pod
dubem	dub	k1gInSc7	dub
církevní	církevní	k2eAgInPc4d1	církevní
obřady	obřad	k1gInPc4	obřad
a	a	k8xC	a
přinášeli	přinášet	k5eAaImAgMnP	přinášet
jim	on	k3xPp3gInPc3	on
zvířecí	zvířecí	k2eAgFnPc1d1	zvířecí
oběti	oběť	k1gFnPc1	oběť
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
voly	vůl	k1gMnPc4	vůl
a	a	k8xC	a
berany	beran	k1gMnPc4	beran
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jasně	jasně	k6eAd1	jasně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
pohanský	pohanský	k2eAgInSc4d1	pohanský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
dub	dub	k1gInSc1	dub
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Chortice	Chortice	k1gFnSc2	Chortice
na	na	k7c6	na
Dněpru	Dněpr	k1gInSc6	Dněpr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Konstantin	Konstantin	k1gMnSc1	Konstantin
Porfyrogennétos	Porfyrogennétos	k1gMnSc1	Porfyrogennétos
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
mu	on	k3xPp3gMnSc3	on
obětováni	obětován	k2eAgMnPc1d1	obětován
kohouti	kohout	k1gMnPc1	kohout
<g/>
,	,	kIx,	,
chléb	chléb	k1gInSc1	chléb
a	a	k8xC	a
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
okolo	okolo	k7c2	okolo
něj	on	k3xPp3gInSc2	on
či	či	k8xC	či
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
kmene	kmen	k1gInSc2	kmen
zaráženy	zarážen	k2eAgFnPc4d1	zarážen
střely	střela	k1gFnPc4	střela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
byl	být	k5eAaImAgInS	být
ctěn	ctěn	k2eAgInSc1d1	ctěn
Święty	Święt	k1gInPc7	Święt
Dąb	Dąb	k1gMnSc2	Dąb
u	u	k7c2	u
Čenstochové	Čenstochová	k1gFnSc2	Čenstochová
a	a	k8xC	a
dubový	dubový	k2eAgInSc4d1	dubový
háj	háj	k1gInSc4	háj
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
bohu	bůh	k1gMnSc6	bůh
Provenovi	Proven	k1gMnSc6	Proven
ve	v	k7c6	v
Starigradu	Starigrad	k1gInSc6	Starigrad
<g/>
,	,	kIx,	,
dnešním	dnešní	k2eAgNnSc6d1	dnešní
městě	město	k1gNnSc6	město
Oldenburg	Oldenburg	k1gInSc1	Oldenburg
in	in	k?	in
Holstein	Holstein	k1gInSc1	Holstein
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
také	také	k9	také
nalezli	nalézt	k5eAaBmAgMnP	nalézt
nedaleko	nedaleko	k7c2	nedaleko
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
ústí	ústit	k5eAaImIp3nS	ústit
Desny	Desn	k1gMnPc4	Desn
do	do	k7c2	do
Dněpru	Dněpr	k1gInSc2	Dněpr
téměř	téměř	k6eAd1	téměř
deset	deset	k4xCc4	deset
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
bylo	být	k5eAaImAgNnS	být
zaraženo	zarazit	k5eAaPmNgNnS	zarazit
devět	devět	k4xCc1	devět
kančích	kančí	k2eAgFnPc2d1	kančí
čelistí	čelist	k1gFnPc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Kanec	kanec	k1gMnSc1	kanec
nejspíše	nejspíše	k9	nejspíše
symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
