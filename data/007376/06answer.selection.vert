<s>
Duby	dub	k1gInPc1	dub
jsou	být	k5eAaImIp3nP	být
dlouhověké	dlouhověký	k2eAgInPc1d1	dlouhověký
<g/>
,	,	kIx,	,
pomalu	pomalu	k6eAd1	pomalu
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
opadavé	opadavý	k2eAgInPc4d1	opadavý
nebo	nebo	k8xC	nebo
stálezelené	stálezelený	k2eAgInPc4d1	stálezelený
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
i	i	k9	i
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
s	s	k7c7	s
tvrdým	tvrdý	k2eAgNnSc7d1	tvrdé
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
