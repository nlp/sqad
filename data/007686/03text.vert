<s>
Ghost	Ghost	k1gFnSc1	Ghost
Rider	Ridra	k1gFnPc2	Ridra
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
akční	akční	k2eAgInSc1d1	akční
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
Mark	Mark	k1gMnSc1	Mark
Steven	Stevna	k1gFnPc2	Stevna
Johnson	Johnson	k1gInSc4	Johnson
podle	podle	k7c2	podle
komiksových	komiksový	k2eAgInPc2d1	komiksový
příběhů	příběh	k1gInPc2	příběh
o	o	k7c4	o
Ghost	Ghost	k1gFnSc4	Ghost
Riderovi	Rider	k1gMnSc3	Rider
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
amerických	americký	k2eAgNnPc2d1	americké
kin	kino	k1gNnPc2	kino
byl	být	k5eAaImAgMnS	být
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rozpočet	rozpočet	k1gInSc1	rozpočet
činil	činit	k5eAaImAgInS	činit
110	[number]	k4	110
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
uveden	uveden	k2eAgInSc1d1	uveden
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
celosvětově	celosvětově	k6eAd1	celosvětově
utržil	utržit	k5eAaPmAgInS	utržit
228	[number]	k4	228
738	[number]	k4	738
393	[number]	k4	393
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
komerčnímu	komerční	k2eAgInSc3d1	komerční
úspěchu	úspěch	k1gInSc3	úspěch
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
natočen	natočen	k2eAgInSc4d1	natočen
navazující	navazující	k2eAgInSc4d1	navazující
snímek	snímek	k1gInSc4	snímek
Ghost	Ghost	k1gFnSc1	Ghost
Rider	Rider	k1gInSc1	Rider
2	[number]	k4	2
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
si	se	k3xPyFc3	se
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
Nicolas	Nicolas	k1gMnSc1	Nicolas
Cage	Cag	k1gInSc2	Cag
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
slouží	sloužit	k5eAaImIp3nS	sloužit
ďáblovi	ďábel	k1gMnSc3	ďábel
<g/>
,	,	kIx,	,
Mefistofelovi	Mefistofeles	k1gMnSc3	Mefistofeles
<g/>
,	,	kIx,	,
Ghost	Ghost	k1gInSc1	Ghost
Rideři	Rider	k1gMnPc1	Rider
<g/>
,	,	kIx,	,
lidští	lidský	k2eAgMnPc1d1	lidský
lovci	lovec	k1gMnPc1	lovec
sbírající	sbírající	k2eAgFnSc2d1	sbírající
duše	duše	k1gFnSc2	duše
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
ďáblovi	ďábel	k1gMnSc3	ďábel
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tajnou	tajný	k2eAgFnSc4d1	tajná
smlouvu	smlouva	k1gFnSc4	smlouva
ze	z	k7c2	z
San	San	k1gFnSc2	San
Venganza	Venganza	k1gFnSc1	Venganza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
Mefistofelovi	Mefistofeles	k1gMnSc3	Mefistofeles
dodat	dodat	k5eAaPmF	dodat
tisíce	tisíc	k4xCgInPc1	tisíc
duší	duše	k1gFnPc2	duše
zkažených	zkažený	k2eAgMnPc2d1	zkažený
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
síly	síla	k1gFnPc4	síla
přivést	přivést	k5eAaPmF	přivést
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
peklo	peklo	k1gNnSc1	peklo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
je	být	k5eAaImIp3nS	být
mladý	mladý	k1gMnSc1	mladý
Johnny	Johnn	k1gInPc4	Johnn
Blaze	blaze	k6eAd1	blaze
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
proslulým	proslulý	k2eAgMnSc7d1	proslulý
motocyklovým	motocyklový	k2eAgMnSc7d1	motocyklový
kaskadérem	kaskadér	k1gMnSc7	kaskadér
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
otec	otec	k1gMnSc1	otec
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
jej	on	k3xPp3gMnSc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Mefistofeles	Mefistofeles	k1gMnSc1	Mefistofeles
a	a	k8xC	a
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
Johnnyho	Johnny	k1gMnSc4	Johnny
duši	duše	k1gFnSc4	duše
otce	otec	k1gMnSc2	otec
vyléčí	vyléčit	k5eAaPmIp3nS	vyléčit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
zabije	zabít	k5eAaPmIp3nS	zabít
při	při	k7c6	při
skoku	skok	k1gInSc6	skok
na	na	k7c6	na
motorce	motorka	k1gFnSc6	motorka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
21	[number]	k4	21
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
Johnny	Johnna	k1gFnSc2	Johnna
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kaskadérem	kaskadér	k1gMnSc7	kaskadér
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přežije	přežít	k5eAaPmIp3nS	přežít
i	i	k9	i
ty	ten	k3xDgInPc1	ten
nejtěžší	těžký	k2eAgInPc1d3	nejtěžší
pády	pád	k1gInPc1	pád
<g/>
.	.	kIx.	.
</s>
<s>
Setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
dětskou	dětský	k2eAgFnSc7d1	dětská
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
Roxanne	Roxann	k1gInSc5	Roxann
Simpsononovou	Simpsononový	k2eAgFnSc4d1	Simpsononový
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
televizní	televizní	k2eAgFnSc7d1	televizní
reportérkou	reportérka	k1gFnSc7	reportérka
a	a	k8xC	a
domluví	domluvit	k5eAaPmIp3nP	domluvit
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
rande	rande	k1gNnSc1	rande
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
ďábel	ďábel	k1gMnSc1	ďábel
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
protestům	protest	k1gInPc3	protest
promění	proměnit	k5eAaPmIp3nS	proměnit
Johnnyho	Johnny	k1gMnSc4	Johnny
na	na	k7c4	na
Ghost	Ghost	k1gFnSc4	Ghost
Ridera	Ridero	k1gNnSc2	Ridero
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dostane	dostat	k5eAaPmIp3nS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zabít	zabít	k5eAaPmF	zabít
Mefistofelova	Mefistofelův	k2eAgMnSc4d1	Mefistofelův
syna	syn	k1gMnSc4	syn
Blackhearta	Blackheart	k1gMnSc4	Blackheart
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
schovanou	schovaný	k2eAgFnSc4d1	schovaná
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Roxanne	Roxann	k1gInSc5	Roxann
čeká	čekat	k5eAaImIp3nS	čekat
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
,	,	kIx,	,
Ghost	Ghost	k1gMnSc1	Ghost
Rider	Rider	k1gMnSc1	Rider
musí	muset	k5eAaImIp3nS	muset
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
Blackhearta	Blackheart	k1gMnSc4	Blackheart
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
unikne	uniknout	k5eAaPmIp3nS	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Johnnymu	Johnnym	k1gInSc2	Johnnym
pomůže	pomoct	k5eAaPmIp3nS	pomoct
hrobař	hrobař	k1gMnSc1	hrobař
z	z	k7c2	z
místního	místní	k2eAgInSc2d1	místní
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
noc	noc	k1gFnSc1	noc
je	být	k5eAaImIp3nS	být
Johnny	Johnna	k1gFnSc2	Johnna
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uteče	utéct	k5eAaPmIp3nS	utéct
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
Blackhearta	Blackhearta	k1gFnSc1	Blackhearta
<g/>
.	.	kIx.	.
</s>
<s>
Roxanne	Roxannout	k5eAaImIp3nS	Roxannout
Johnnyho	Johnny	k1gMnSc4	Johnny
pozná	poznat	k5eAaPmIp3nS	poznat
i	i	k9	i
v	v	k7c4	v
Ghost	Ghost	k1gFnSc4	Ghost
Riderově	Riderův	k2eAgFnSc3d1	Riderův
podobě	podoba	k1gFnSc3	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
unesena	unést	k5eAaPmNgFnS	unést
Blackheartemm	Blackheartem	k1gNnSc7	Blackheartem
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
ji	on	k3xPp3gFnSc4	on
chce	chtít	k5eAaImIp3nS	chtít
vyměnit	vyměnit	k5eAaPmF	vyměnit
za	za	k7c4	za
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Hrobař	hrobař	k1gMnSc1	hrobař
Johnnymu	Johnnym	k1gInSc2	Johnnym
smlouvu	smlouva	k1gFnSc4	smlouva
vydá	vydat	k5eAaPmIp3nS	vydat
a	a	k8xC	a
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
oním	onen	k3xDgInSc7	onen
posledním	poslední	k2eAgInSc7d1	poslední
Ghost	Ghost	k1gInSc1	Ghost
Riderem	Rider	k1gInSc7	Rider
<g/>
.	.	kIx.	.
</s>
<s>
Johnny	Johnn	k1gMnPc4	Johnn
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
Blackheartem	Blackheart	k1gInSc7	Blackheart
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
ten	ten	k3xDgMnSc1	ten
nemá	mít	k5eNaImIp3nS	mít
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
zvítězit	zvítězit	k5eAaPmF	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Blackheart	Blackheart	k1gInSc1	Blackheart
přečte	přečíst	k5eAaPmIp3nS	přečíst
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
převezme	převzít	k5eAaPmIp3nS	převzít
duše	duše	k1gFnSc1	duše
tisíce	tisíc	k4xCgInSc2	tisíc
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
Johnny	Johnen	k2eAgMnPc4d1	Johnen
to	ten	k3xDgNnSc1	ten
využije	využít	k5eAaPmIp3nS	využít
a	a	k8xC	a
ďáblova	ďáblův	k2eAgMnSc4d1	ďáblův
syna	syn	k1gMnSc4	syn
definitivně	definitivně	k6eAd1	definitivně
porazí	porazit	k5eAaPmIp3nS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Dorazí	dorazit	k5eAaPmIp3nS	dorazit
Mefistofeles	Mefistofeles	k1gMnSc1	Mefistofeles
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
chce	chtít	k5eAaImIp3nS	chtít
navrátit	navrátit	k5eAaPmF	navrátit
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
Johnnymu	Johnnym	k1gInSc6	Johnnym
dal	dát	k5eAaPmAgMnS	dát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
povinnostmi	povinnost	k1gFnPc7	povinnost
Ghost	Ghost	k1gFnSc4	Ghost
Ridera	Ridero	k1gNnSc2	Ridero
trpěli	trpět	k5eAaImAgMnP	trpět
další	další	k2eAgMnPc1d1	další
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nicolas	Nicolas	k1gInSc1	Nicolas
Cage	Cag	k1gFnSc2	Cag
jako	jako	k8xC	jako
Johnny	Johnna	k1gFnSc2	Johnna
Blaze	blaze	k6eAd1	blaze
/	/	kIx~	/
Ghost	Ghost	k1gFnSc1	Ghost
Rider	Rider	k1gInSc1	Rider
Eva	Eva	k1gFnSc1	Eva
Mendes	Mendes	k1gMnSc1	Mendes
jako	jako	k8xC	jako
Roxanne	Roxann	k1gInSc5	Roxann
Simpsonová	Simpsonový	k2eAgFnSc1d1	Simpsonová
Wes	Wes	k1gMnSc3	Wes
Bentley	Bentlea	k1gFnSc2	Bentlea
jako	jako	k8xS	jako
Blackheart	Blackhearta	k1gFnPc2	Blackhearta
Sam	Sam	k1gMnSc1	Sam
Elliott	Elliott	k1gMnSc1	Elliott
jako	jako	k8xC	jako
Carter	Carter	k1gMnSc1	Carter
Slade	slad	k1gInSc5	slad
Donal	Donal	k1gInSc1	Donal
Logue	Logu	k1gFnPc1	Logu
jako	jako	k8xC	jako
Mack	Mack	k1gMnSc1	Mack
Matt	Matt	k1gMnSc1	Matt
Long	Long	k1gMnSc1	Long
jako	jako	k8xS	jako
mladý	mladý	k1gMnSc1	mladý
Johnny	Johnna	k1gMnSc2	Johnna
Blaze	blaze	k6eAd1	blaze
Peter	Peter	k1gMnSc1	Peter
Fonda	Fonda	k1gMnSc1	Fonda
jako	jako	k8xS	jako
Mefistofeles	Mefistofeles	k1gMnSc1	Mefistofeles
Brett	Brett	k2eAgInSc4d1	Brett
Cullen	Cullen	k1gInSc4	Cullen
jako	jako	k8xC	jako
Barton	Barton	k1gInSc4	Barton
Blaze	blaze	k6eAd1	blaze
Raquel	Raquel	k1gMnSc1	Raquel
Alessi	Alesse	k1gFnSc4	Alesse
jako	jako	k9	jako
mladá	mladý	k2eAgFnSc1d1	mladá
Roxanne	Roxann	k1gInSc5	Roxann
Simpsonová	Simpsonový	k2eAgFnSc1d1	Simpsonová
</s>
