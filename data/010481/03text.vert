<p>
<s>
JPEG-LS	JPEG-LS	k?	JPEG-LS
je	být	k5eAaImIp3nS	být
bezeztrátový	bezeztrátový	k2eAgInSc1d1	bezeztrátový
formát	formát	k1gInSc1	formát
obrazu	obraz	k1gInSc2	obraz
vytvořený	vytvořený	k2eAgMnSc1d1	vytvořený
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
bezeztrátový	bezeztrátový	k2eAgInSc4d1	bezeztrátový
režim	režim	k1gInSc4	režim
formátu	formát	k1gInSc2	formát
JPEG	JPEG	kA	JPEG
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
neefektivní	efektivní	k2eNgFnPc4d1	neefektivní
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
neimplementovaný	implementovaný	k2eNgInSc1d1	implementovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
modifikaci	modifikace	k1gFnSc4	modifikace
formátu	formát	k1gInSc2	formát
JPEG	JPEG	kA	JPEG
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formálně	formálně	k6eAd1	formálně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
standardizován	standardizován	k2eAgMnSc1d1	standardizován
v	v	k7c6	v
ITU-T	ITU-T	k1gFnSc6	ITU-T
T	T	kA	T
<g/>
.87	.87	k4	.87
a	a	k8xC	a
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
14495-1	[number]	k4	14495-1
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formát	formát	k1gInSc1	formát
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
komprimovat	komprimovat	k5eAaImF	komprimovat
obraz	obraz	k1gInSc4	obraz
bezeztrátově	bezeztrátově	k6eAd1	bezeztrátově
nebo	nebo	k8xC	nebo
téměř	téměř	k6eAd1	téměř
bezeztrátově	bezeztrátově	k6eAd1	bezeztrátově
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JPEG-LS	JPEG-LS	k?	JPEG-LS
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
algoritmu	algoritmus	k1gInSc6	algoritmus
LOCO-I	LOCO-I	k1gFnSc1	LOCO-I
(	(	kIx(	(
<g/>
LOw	LOw	k1gFnSc1	LOw
COmplexity	COmplexita	k1gFnSc2	COmplexita
LOssless	LOsslessa	k1gFnPc2	LOsslessa
COmpression	COmpression	k1gInSc1	COmpression
for	forum	k1gNnPc2	forum
Images	Images	k1gInSc1	Images
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
kompresní	kompresní	k2eAgFnSc1d1	kompresní
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
prediktoru	prediktor	k1gInSc2	prediktor
a	a	k8xC	a
následného	následný	k2eAgInSc2d1	následný
kontextového	kontextový	k2eAgInSc2d1	kontextový
entropického	entropický	k2eAgInSc2d1	entropický
kodéru	kodér	k1gInSc2	kodér
(	(	kIx(	(
<g/>
Golombův-Riceův	Golombův-Riceův	k2eAgInSc1d1	Golombův-Riceův
kód	kód	k1gInSc1	kód
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
tento	tento	k3xDgInSc4	tento
řetězec	řetězec	k1gInSc4	řetězec
může	moct	k5eAaImIp3nS	moct
kodér	kodér	k1gInSc1	kodér
zakódovat	zakódovat	k5eAaPmF	zakódovat
sled	sled	k1gInSc4	sled
stejných	stejný	k2eAgInPc2d1	stejný
pixelů	pixel	k1gInPc2	pixel
variantou	varianta	k1gFnSc7	varianta
metody	metoda	k1gFnSc2	metoda
RLE	RLE	kA	RLE
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komprese	komprese	k1gFnSc1	komprese
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
řádcích	řádek	k1gInPc6	řádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vícekanálové	vícekanálový	k2eAgInPc4d1	vícekanálový
obrazy	obraz	k1gInPc4	obraz
(	(	kIx(	(
<g/>
např.	např.	kA	např.
RGB	RGB	kA	RGB
<g/>
)	)	kIx)	)
podporuje	podporovat	k5eAaImIp3nS	podporovat
formát	formát	k1gInSc4	formát
několik	několik	k4yIc4	několik
režimů	režim	k1gInPc2	režim
prokládání	prokládání	k1gNnSc2	prokládání
–	–	k?	–
neprokládaný	prokládaný	k2eNgInSc4d1	neprokládaný
režim	režim	k1gInSc4	režim
(	(	kIx(	(
<g/>
jediný	jediný	k2eAgInSc4d1	jediný
kanál	kanál	k1gInSc4	kanál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prokládání	prokládání	k1gNnSc2	prokládání
několika	několik	k4yIc2	několik
řádků	řádek	k1gInPc2	řádek
<g/>
,	,	kIx,	,
prokládání	prokládání	k1gNnSc2	prokládání
vzorků	vzorek	k1gInPc2	vzorek
(	(	kIx(	(
<g/>
složek	složka	k1gFnPc2	složka
pixelů	pixel	k1gInPc2	pixel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
paletové	paletový	k2eAgInPc4d1	paletový
obrázky	obrázek	k1gInPc4	obrázek
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
stejný	stejný	k2eAgInSc1d1	stejný
postup	postup	k1gInSc1	postup
jako	jako	k9	jako
pro	pro	k7c4	pro
obrázky	obrázek	k1gInPc4	obrázek
jednokanálové	jednokanálový	k2eAgInPc4d1	jednokanálový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
JPEG	JPEG	kA	JPEG
–	–	k?	–
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
ztrátový	ztrátový	k2eAgInSc1d1	ztrátový
kompresní	kompresní	k2eAgInSc1d1	kompresní
formát	formát	k1gInSc1	formát
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
https://jpeg.org/jpegls/	[url]	k?	https://jpeg.org/jpegls/
–	–	k?	–
stránka	stránka	k1gFnSc1	stránka
skupiny	skupina	k1gFnSc2	skupina
JPEG	JPEG	kA	JPEG
(	(	kIx(	(
<g/>
Joint	Joint	k1gMnSc1	Joint
Photographic	Photographic	k1gMnSc1	Photographic
Experts	Expertsa	k1gFnPc2	Expertsa
Group	Group	k1gMnSc1	Group
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
http://www.hpl.hp.com/research/info_theory/loco/	[url]	k?	http://www.hpl.hp.com/research/info_theory/loco/
–	–	k?	–
stránka	stránka	k1gFnSc1	stránka
HP	HP	kA	HP
Labs	Labsa	k1gFnPc2	Labsa
</s>
</p>
