<s>
JPEG-LS	JPEG-LS	k?	JPEG-LS
je	být	k5eAaImIp3nS	být
bezeztrátový	bezeztrátový	k2eAgInSc1d1	bezeztrátový
formát	formát	k1gInSc1	formát
obrazu	obraz	k1gInSc2	obraz
vytvořený	vytvořený	k2eAgMnSc1d1	vytvořený
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
bezeztrátový	bezeztrátový	k2eAgInSc4d1	bezeztrátový
režim	režim	k1gInSc4	režim
formátu	formát	k1gInSc2	formát
JPEG	JPEG	kA	JPEG
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
neefektivní	efektivní	k2eNgFnPc4d1	neefektivní
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
neimplementovaný	implementovaný	k2eNgInSc1d1	implementovaný
<g/>
.	.	kIx.	.
</s>
