<s>
Drslav	Drslat	k5eAaPmDgInS	Drslat
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
byl	být	k5eAaImAgMnS	být
moravský	moravský	k2eAgMnSc1d1	moravský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
příslušníkem	příslušník	k1gMnSc7	příslušník
rodu	rod	k1gInSc2	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Vok	Vok	k1gMnSc1	Vok
I.	I.	kA	I.
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Drslav	Drslat	k5eAaPmDgInS	Drslat
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
Vokova	Vokův	k2eAgNnSc2d1	Vokův
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Ješkem	Ješek	k1gMnSc7	Ješek
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
z	z	k7c2	z
téhož	týž	k3xTgNnSc2	týž
manželství	manželství	k1gNnSc2	manželství
kráčeli	kráčet	k5eAaImAgMnP	kráčet
společnou	společný	k2eAgFnSc7d1	společná
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jičínsko-fulnecká	jičínskoulnecký	k2eAgFnSc1d1	jičínsko-fulnecký
větev	větev	k1gFnSc1	větev
Kravařů	kravař	k1gMnPc2	kravař
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Drslavovi	Drslava	k1gMnSc6	Drslava
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1329	[number]	k4	1329
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
uvádí	uvádět	k5eAaImIp3nS	uvádět
na	na	k7c6	na
listině	listina	k1gFnSc6	listina
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Ješkem	Ješek	k1gMnSc7	Ješek
<g/>
,	,	kIx,	,
když	když	k8xS	když
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
spásu	spása	k1gFnSc4	spása
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
odpuštění	odpuštění	k1gNnSc2	odpuštění
svých	svůj	k3xOyFgInPc2	svůj
hříchů	hřích	k1gInPc2	hřích
<g/>
"	"	kIx"	"
darovali	darovat	k5eAaPmAgMnP	darovat
fulneckému	fulnecký	k2eAgInSc3d1	fulnecký
kostelu	kostel	k1gInSc3	kostel
plat	plat	k1gInSc4	plat
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
lánu	lán	k1gInSc2	lán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
si	se	k3xPyFc3	se
bratři	bratr	k1gMnPc1	bratr
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
Drslav	Drslav	k1gMnSc1	Drslav
získal	získat	k5eAaPmAgMnS	získat
panství	panství	k1gNnSc4	panství
Fulnek	Fulnek	k1gInSc1	Fulnek
a	a	k8xC	a
Bílovec	Bílovec	k1gInSc1	Bílovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
Drslavovi	Drslava	k1gMnSc6	Drslava
málo	málo	k6eAd1	málo
písemních	písemní	k2eAgFnPc2d1	písemní
zmínek	zmínka	k1gFnPc2	zmínka
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1340	[number]	k4	1340
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
o	o	k7c6	o
něj	on	k3xPp3gMnSc2	on
zmínky	zmínka	k1gFnSc2	zmínka
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
panského	panské	k1gNnSc2	panské
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1355	[number]	k4	1355
si	se	k3xPyFc3	se
Drslav	Drslav	k1gMnSc1	Drslav
spolčil	spolčit	k5eAaPmAgMnS	spolčit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Ješkem	Ješek	k1gMnSc7	Ješek
navzájem	navzájem	k6eAd1	navzájem
statky	statek	k1gInPc7	statek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
případě	případ	k1gInSc6	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zůstal	zůstat	k5eAaPmAgInS	zůstat
majetek	majetek	k1gInSc4	majetek
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
Drslav	Drslav	k1gFnSc1	Drslav
na	na	k7c6	na
listinách	listina	k1gFnPc6	listina
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
pronikl	proniknout	k5eAaPmAgInS	proniknout
i	i	k9	i
do	do	k7c2	do
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
kruhů	kruh	k1gInPc2	kruh
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Drslav	Drslav	k1gMnSc1	Drslav
titulu	titul	k1gInSc2	titul
královského	královský	k2eAgMnSc2d1	královský
hejtmana	hejtman	k1gMnSc2	hejtman
ve	v	k7c6	v
Frankensteinu	Frankenstein	k1gInSc6	Frankenstein
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Ząbkowice	Ząbkowice	k1gFnSc1	Ząbkowice
Śląskie	Śląskie	k1gFnSc2	Śląskie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1360	[number]	k4	1360
se	se	k3xPyFc4	se
Drslav	Drslav	k1gMnSc1	Drslav
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
tažení	tažení	k1gNnSc4	tažení
vojska	vojsko	k1gNnSc2	vojsko
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
proti	proti	k7c3	proti
württemberským	württemberský	k2eAgNnPc3d1	württemberský
hrabatům	hrabě	k1gNnPc3	hrabě
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
králův	králův	k2eAgMnSc1d1	králův
důvěrník	důvěrník	k1gMnSc1	důvěrník
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
králových	králův	k2eAgFnPc2d1	králova
vojenských	vojenský	k2eAgFnPc2d1	vojenská
akcí	akce	k1gFnPc2	akce
i	i	k9	i
jednání	jednání	k1gNnSc1	jednání
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ho	on	k3xPp3gNnSc2	on
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
listině	listina	k1gFnSc6	listina
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
soukromého	soukromý	k2eAgMnSc4d1	soukromý
rádce	rádce	k1gMnSc4	rádce
<g/>
.	.	kIx.	.
</s>
<s>
Drslav	Drslat	k5eAaPmDgInS	Drslat
zemřel	zemřít	k5eAaPmAgMnS	zemřít
předčasně	předčasně	k6eAd1	předčasně
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
již	již	k6eAd1	již
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
jeho	on	k3xPp3gInSc2	on
majetku	majetek	k1gInSc2	majetek
mezi	mezi	k7c7	mezi
potomky	potomek	k1gMnPc7	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Vok	Vok	k?	Vok
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
Jičína	Jičín	k1gInSc2	Jičín
(	(	kIx(	(
<g/>
1340	[number]	k4	1340
<g/>
-	-	kIx~	-
<g/>
1386	[number]	k4	1386
<g/>
)	)	kIx)	)
Beneš	Beneš	k1gMnSc1	Beneš
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
Krumlova	Krumlov	k1gInSc2	Krumlov
(	(	kIx(	(
<g/>
1347	[number]	k4	1347
<g/>
-	-	kIx~	-
<g/>
1398	[number]	k4	1398
<g/>
)	)	kIx)	)
Drslav	Drslav	k1gFnSc2	Drslav
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
Fulneka	Fulneko	k1gNnSc2	Fulneko
(	(	kIx(	(
<g/>
1347	[number]	k4	1347
<g/>
-	-	kIx~	-
<g/>
1380	[number]	k4	1380
<g/>
)	)	kIx)	)
Lacek	Lacek	k6eAd1	Lacek
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
Helfštejna	Helfštejno	k1gNnSc2	Helfštejno
(	(	kIx(	(
<g/>
1348	[number]	k4	1348
<g/>
-	-	kIx~	-
<g/>
1416	[number]	k4	1416
<g/>
)	)	kIx)	)
Ofka	Ofk	k1gInSc2	Ofk
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1404	[number]	k4	1404
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Lipé	Lipá	k1gFnSc2	Lipá
Dorota	Dorota	k1gFnSc1	Dorota
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
(	(	kIx(	(
<g/>
1369	[number]	k4	1369
<g/>
-	-	kIx~	-
<g/>
1371	[number]	k4	1371
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Vildenberka	Vildenberka	k1gFnSc1	Vildenberka
BALETKA	baletka	k1gFnSc1	baletka
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
až	až	k9	až
na	na	k7c4	na
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
488	[number]	k4	488
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
682	[number]	k4	682
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
