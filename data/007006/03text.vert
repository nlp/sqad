<s>
Prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1903	[number]	k4	1903
Boskovice	Boskovice	k1gInPc1	Boskovice
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1968	[number]	k4	1968
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
paleontolog	paleontolog	k1gMnSc1	paleontolog
a	a	k8xC	a
také	také	k9	také
autor	autor	k1gMnSc1	autor
populárně	populárně	k6eAd1	populárně
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
i	i	k9	i
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
jihomoravském	jihomoravský	k2eAgNnSc6d1	Jihomoravské
městě	město	k1gNnSc6	město
Boskovice	Boskovice	k1gInPc1	Boskovice
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
berního	berní	k1gMnSc2	berní
úředníka	úředník	k1gMnSc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
klasického	klasický	k2eAgNnSc2d1	klasické
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
až	až	k9	až
1925	[number]	k4	1925
přírodní	přírodní	k2eAgFnSc2d1	přírodní
vědy	věda	k1gFnSc2	věda
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
studium	studium	k1gNnSc1	studium
zakončil	zakončit	k5eAaPmAgMnS	zakončit
doktorátem	doktorát	k1gInSc7	doktorát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
setrval	setrvat	k5eAaPmAgInS	setrvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
v	v	k7c6	v
roli	role	k1gFnSc6	role
asistenta	asistent	k1gMnSc2	asistent
geologického	geologický	k2eAgInSc2d1	geologický
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
nejprve	nejprve	k6eAd1	nejprve
docentem	docent	k1gMnSc7	docent
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
profesorem	profesor	k1gMnSc7	profesor
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
děkanem	děkan	k1gMnSc7	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vědecké	vědecký	k2eAgFnSc6d1	vědecká
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
zprvu	zprvu	k6eAd1	zprvu
zabýval	zabývat	k5eAaImAgMnS	zabývat
permskou	permský	k2eAgFnSc7d1	Permská
flórou	flóra	k1gFnSc7	flóra
a	a	k8xC	a
studiem	studio	k1gNnSc7	studio
prvohorních	prvohorní	k2eAgMnPc2d1	prvohorní
krytolebců	krytolebec	k1gMnPc2	krytolebec
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgInS	stát
významným	významný	k2eAgMnSc7d1	významný
popularizátorem	popularizátor	k1gMnSc7	popularizátor
zejména	zejména	k9	zejména
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
paleontologie	paleontologie	k1gFnSc2	paleontologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
stratigrafie	stratigrafie	k1gFnSc2	stratigrafie
a	a	k8xC	a
geologie	geologie	k1gFnSc2	geologie
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
věhlasu	věhlas	k1gInSc2	věhlas
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
zejména	zejména	k9	zejména
rekonstrukcemi	rekonstrukce	k1gFnPc7	rekonstrukce
pravěké	pravěký	k2eAgFnSc2d1	pravěká
flóry	flóra	k1gFnSc2	flóra
a	a	k8xC	a
fauny	fauna	k1gFnSc2	fauna
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
akademickým	akademický	k2eAgMnSc7d1	akademický
malířem	malíř	k1gMnSc7	malíř
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Burianem	Burian	k1gMnSc7	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
vědecké	vědecký	k2eAgNnSc4d1	vědecké
dílo	dílo	k1gNnSc4	dílo
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
četných	četný	k2eAgInPc6d1	četný
našich	náš	k3xOp1gFnPc6	náš
i	i	k8xC	i
zahraničních	zahraniční	k2eAgInPc6d1	zahraniční
odborných	odborný	k2eAgInPc6d1	odborný
časopisech	časopis	k1gInPc6	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
sto	sto	k4xCgNnSc4	sto
dvaceti	dvacet	k4xCc2	dvacet
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
publikací	publikace	k1gFnPc2	publikace
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
populárně	populárně	k6eAd1	populárně
naučných	naučný	k2eAgFnPc2d1	naučná
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
i	i	k9	i
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
vědecký	vědecký	k2eAgMnSc1d1	vědecký
poradce	poradce	k1gMnSc1	poradce
na	na	k7c6	na
slavném	slavný	k2eAgInSc6d1	slavný
filmu	film	k1gInSc6	film
režiséra	režisér	k1gMnSc2	režisér
Karla	Karel	k1gMnSc2	Karel
Zemana	Zeman	k1gMnSc2	Zeman
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
pravěku	pravěk	k1gInSc2	pravěk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Krytolebci	krytolebec	k1gMnPc1	krytolebec
ze	z	k7c2	z
spodního	spodní	k2eAgInSc2d1	spodní
permu	perm	k1gInSc2	perm
Boskovické	boskovický	k2eAgFnSc2d1	boskovická
brázdy	brázda	k1gFnSc2	brázda
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
publikace	publikace	k1gFnSc1	publikace
<g/>
,	,	kIx,	,
O	o	k7c6	o
glaukonitické	glaukonitický	k2eAgFnSc6d1	glaukonitický
vrstvě	vrstva	k1gFnSc6	vrstva
s	s	k7c7	s
rybími	rybí	k2eAgInPc7d1	rybí
zbytky	zbytek	k1gInPc7	zbytek
z	z	k7c2	z
křídy	křída	k1gFnSc2	křída
plaňanské	plaňanský	k2eAgInPc1d1	plaňanský
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
publikace	publikace	k1gFnSc1	publikace
<g/>
,	,	kIx,	,
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
paleontologie	paleontologie	k1gFnSc2	paleontologie
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
publikace	publikace	k1gFnSc1	publikace
<g/>
,	,	kIx,	,
O	o	k7c6	o
larválních	larvální	k2eAgFnPc6d1	larvální
<g />
.	.	kIx.	.
</s>
<s>
stadiích	stadion	k1gNnPc6	stadion
některých	některý	k3yIgMnPc2	některý
stegocephalů	stegocephal	k1gInPc2	stegocephal
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
permu	perm	k1gInSc2	perm
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
publikace	publikace	k1gFnSc1	publikace
<g/>
,	,	kIx,	,
Zavátý	zavátý	k2eAgInSc1d1	zavátý
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
populárně	populárně	k6eAd1	populárně
naučná	naučný	k2eAgFnSc1d1	naučná
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
společná	společný	k2eAgFnSc1d1	společná
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Burianem	Burian	k1gMnSc7	Burian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
devět	devět	k4xCc1	devět
povídek	povídka	k1gFnPc2	povídka
ze	z	k7c2	z
života	život	k1gInSc2	život
pravěkého	pravěký	k2eAgNnSc2d1	pravěké
tvorstva	tvorstvo	k1gNnSc2	tvorstvo
<g/>
,	,	kIx,	,
Divy	diva	k1gFnSc2	diva
prasvěta	prasvět	k1gInSc2	prasvět
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
populárně	populárně	k6eAd1	populárně
naučná	naučný	k2eAgFnSc1d1	naučná
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
kronika	kronika	k1gFnSc1	kronika
pravěké	pravěký	k2eAgFnSc2d1	pravěká
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
tvorstva	tvorstvo	k1gNnSc2	tvorstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karpatský	karpatský	k2eAgMnSc1d1	karpatský
jelen	jelen	k1gMnSc1	jelen
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
publikace	publikace	k1gFnSc1	publikace
<g/>
,	,	kIx,	,
Draci	drak	k1gMnPc1	drak
a	a	k8xC	a
obři	obr	k1gMnPc1	obr
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
populárně	populárně	k6eAd1	populárně
naučná	naučný	k2eAgFnSc1d1	naučná
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
tvorstvo	tvorstvo	k1gNnSc1	tvorstvo
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
starých	starý	k2eAgInPc2d1	starý
přírodopisů	přírodopis	k1gInPc2	přírodopis
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Lovci	lovec	k1gMnPc7	lovec
jeskynních	jeskynní	k2eAgMnPc2d1	jeskynní
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
populárně	populárně	k6eAd1	populárně
naučná	naučný	k2eAgFnSc1d1	naučná
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
formou	forma	k1gFnSc7	forma
povídek	povídka	k1gFnPc2	povídka
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
mladému	mladý	k1gMnSc3	mladý
čtenáři	čtenář	k1gMnSc3	čtenář
život	život	k1gInSc4	život
neandertálského	neandertálský	k2eAgMnSc2d1	neandertálský
pračlověka	pračlověk	k1gMnSc2	pračlověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Choroby	choroba	k1gFnSc2	choroba
a	a	k8xC	a
zranění	zranění	k1gNnSc2	zranění
pravěkých	pravěký	k2eAgNnPc2d1	pravěké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
publikace	publikace	k1gFnPc4	publikace
Ztracený	ztracený	k2eAgInSc1d1	ztracený
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
populárně	populárně	k6eAd1	populárně
naučná	naučný	k2eAgFnSc1d1	naučná
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
beletristickou	beletristický	k2eAgFnSc7d1	beletristická
formou	forma	k1gFnSc7	forma
zpřístupněné	zpřístupněný	k2eAgInPc1d1	zpřístupněný
paleontologické	paleontologický	k2eAgInPc1d1	paleontologický
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c6	o
životě	život	k1gInSc6	život
pravěkých	pravěký	k2eAgNnPc2d1	pravěké
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
o	o	k7c6	o
geologických	geologický	k2eAgFnPc6d1	geologická
epochách	epocha	k1gFnPc6	epocha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgMnPc6	který
žila	žít	k5eAaImAgFnS	žít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pravěké	pravěký	k2eAgNnSc1d1	pravěké
ptactvo	ptactvo	k1gNnSc1	ptactvo
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
populárně	populárně	k6eAd1	populárně
naučná	naučný	k2eAgFnSc1d1	naučná
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
předcích	předek	k1gInPc6	předek
našich	náš	k3xOp1gMnPc2	náš
ptáků	pták	k1gMnPc2	pták
i	i	k9	i
o	o	k7c6	o
ptácích	pták	k1gMnPc6	pták
vymřelých	vymřelý	k2eAgInPc2d1	vymřelý
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
chronologicky	chronologicky	k6eAd1	chronologicky
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s>
systematicky	systematicky	k6eAd1	systematicky
podle	podle	k7c2	podle
rodů	rod	k1gInPc2	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
pravěku	pravěk	k1gInSc2	pravěk
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
beletristicky	beletristicky	k6eAd1	beletristicky
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
popularizují	popularizovat	k5eAaImIp3nP	popularizovat
paleontologické	paleontologický	k2eAgInPc4d1	paleontologický
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
pravěkých	pravěký	k2eAgFnPc6d1	pravěká
<g/>
,	,	kIx,	,
dávno	dávno	k6eAd1	dávno
vymřelých	vymřelý	k2eAgNnPc6d1	vymřelé
zvířatech	zvíře	k1gNnPc6	zvíře
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
boji	boj	k1gInSc6	boj
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
barvitě	barvitě	k6eAd1	barvitě
popisují	popisovat	k5eAaImIp3nP	popisovat
přírodu	příroda	k1gFnSc4	příroda
různých	různý	k2eAgFnPc2d1	různá
geologických	geologický	k2eAgFnPc2d1	geologická
epoch	epocha	k1gFnPc2	epocha
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
kapitoly	kapitola	k1gFnPc1	kapitola
objasňují	objasňovat	k5eAaImIp3nP	objasňovat
významné	významný	k2eAgInPc4d1	významný
nálezy	nález	k1gInPc4	nález
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mamuta	mamut	k1gMnSc2	mamut
<g/>
,	,	kIx,	,
zamrzlého	zamrzlý	k2eAgMnSc2d1	zamrzlý
v	v	k7c6	v
ledu	led	k1gInSc6	led
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
praptáka	prapták	k1gMnSc2	prapták
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neandrtálci	neandrtálec	k1gMnPc1	neandrtálec
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
kapitola	kapitola	k1gFnSc1	kapitola
z	z	k7c2	z
vývojových	vývojový	k2eAgFnPc2d1	vývojová
dějin	dějiny	k1gFnPc2	dějiny
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
učební	učební	k2eAgFnSc1d1	učební
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Z	z	k7c2	z
pradějin	pradějiny	k1gFnPc2	pradějiny
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
<g />
.	.	kIx.	.
</s>
<s>
nejnovější	nový	k2eAgInPc1d3	nejnovější
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
lidského	lidský	k2eAgNnSc2d1	lidské
plemene	plemeno	k1gNnSc2	plemeno
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nálezů	nález	k1gInPc2	nález
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
lidí	člověk	k1gMnPc2	člověk
poutavě	poutavě	k6eAd1	poutavě
líčí	líčit	k5eAaImIp3nS	líčit
jejich	jejich	k3xOp3gInSc1	jejich
lovecký	lovecký	k2eAgInSc1d1	lovecký
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
postupné	postupný	k2eAgNnSc4d1	postupné
zdokonalování	zdokonalování	k1gNnSc4	zdokonalování
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
primitivní	primitivní	k2eAgInPc1d1	primitivní
náboženské	náboženský	k2eAgInPc1d1	náboženský
kulty	kult	k1gInPc1	kult
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vyplývající	vyplývající	k2eAgInSc4d1	vyplývající
způsoby	způsob	k1gInPc4	způsob
pohřbívání	pohřbívání	k1gNnSc2	pohřbívání
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
<g/>
,	,	kIx,	,
Z	z	k7c2	z
pradějin	pradějiny	k1gFnPc2	pradějiny
tvorstva	tvorstvo	k1gNnSc2	tvorstvo
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
života	život	k1gInSc2	život
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
postupně	postupně	k6eAd1	postupně
čtenáře	čtenář	k1gMnSc2	čtenář
se	s	k7c7	s
živočichy	živočich	k1gMnPc7	živočich
pravěkých	pravěký	k2eAgNnPc2d1	pravěké
moří	moře	k1gNnPc2	moře
<g/>
,	,	kIx,	,
s	s	k7c7	s
prvotní	prvotní	k2eAgFnSc7d1	prvotní
suchozemskou	suchozemský	k2eAgFnSc7d1	suchozemská
květenou	květena	k1gFnSc7	květena
<g/>
,	,	kIx,	,
s	s	k7c7	s
praobojživelníky	praobojživelník	k1gMnPc7	praobojživelník
<g/>
,	,	kIx,	,
s	s	k7c7	s
plazími	plazí	k2eAgFnPc7d1	plazí
obludami	obluda	k1gFnPc7	obluda
doby	doba	k1gFnSc2	doba
druhohorní	druhohorní	k2eAgFnSc7d1	druhohorní
a	a	k8xC	a
s	s	k7c7	s
praptáky	prapták	k1gMnPc7	prapták
(	(	kIx(	(
<g/>
závěrem	závěr	k1gInSc7	závěr
je	být	k5eAaImIp3nS	být
pojednáno	pojednán	k2eAgNnSc1d1	pojednáno
o	o	k7c6	o
Darwinově	Darwinův	k2eAgFnSc6d1	Darwinova
teorii	teorie	k1gFnSc6	teorie
a	a	k8xC	a
jejích	její	k3xOp3gFnPc6	její
předchůdcích	předchůdce	k1gMnPc6	předchůdce
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Lovci	lovec	k1gMnPc1	lovec
mamutů	mamut	k1gMnPc2	mamut
a	a	k8xC	a
sobů	sob	k1gMnPc2	sob
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kapitola	kapitola	k1gFnSc1	kapitola
z	z	k7c2	z
vývojových	vývojový	k2eAgFnPc2d1	vývojová
dějin	dějiny	k1gFnPc2	dějiny
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
učební	učební	k2eAgFnSc1d1	učební
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pradějiny	pradějiny	k1gFnPc1	pradějiny
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kapitola	kapitola	k1gFnSc1	kapitola
z	z	k7c2	z
vývojových	vývojový	k2eAgFnPc2d1	vývojová
dějin	dějiny	k1gFnPc2	dějiny
živočišstva	živočišstvo	k1gNnSc2	živočišstvo
(	(	kIx(	(
<g/>
učební	učební	k2eAgFnSc1d1	učební
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hlubinami	hlubina	k1gFnPc7	hlubina
pravěku	pravěk	k1gInSc2	pravěk
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
populárně	populárně	k6eAd1	populárně
naučná	naučný	k2eAgFnSc1d1	naučná
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
přehledem	přehled	k1gInSc7	přehled
vývoje	vývoj	k1gInSc2	vývoj
rostlinstva	rostlinstvo	k1gNnSc2	rostlinstvo
a	a	k8xC	a
živočišstva	živočišstvo	k1gNnSc2	živočišstvo
pro	pro	k7c4	pro
široké	široký	k2eAgInPc4d1	široký
kruhy	kruh	k1gInPc4	kruh
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prehistoric	Prehistoric	k1gMnSc1	Prehistoric
Animals	Animals	k1gInSc1	Animals
(	(	kIx(	(
<g/>
Pravěká	pravěký	k2eAgNnPc1d1	pravěké
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
Z	z	k7c2	z
vývojových	vývojový	k2eAgFnPc2d1	vývojová
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
pradějin	pradějiny	k1gFnPc2	pradějiny
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
antropologická	antropologický	k2eAgFnSc1d1	antropologická
<g />
.	.	kIx.	.
</s>
<s>
publikace	publikace	k1gFnPc1	publikace
líčící	líčící	k2eAgFnPc1d1	líčící
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
rysech	rys	k1gInPc6	rys
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
ubíral	ubírat	k5eAaImAgMnS	ubírat
vývoj	vývoj	k1gInSc4	vývoj
člověka	člověk	k1gMnSc2	člověk
od	od	k7c2	od
dávných	dávný	k2eAgMnPc2d1	dávný
prapředků	prapředek	k1gMnPc2	prapředek
až	až	k6eAd1	až
k	k	k7c3	k
dnešku	dnešek	k1gInSc3	dnešek
<g/>
,	,	kIx,	,
Zrození	zrození	k1gNnSc3	zrození
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
povídky	povídka	k1gFnPc1	povídka
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
děl	dělo	k1gNnPc2	dělo
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
Prehistoric	Prehistoric	k1gMnSc1	Prehistoric
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
Pravěký	pravěký	k2eAgMnSc1d1	pravěký
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
Opolidé	opočlověk	k1gMnPc1	opočlověk
a	a	k8xC	a
předlidé	předlidý	k2eAgInPc1d1	předlidý
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
archeologické	archeologický	k2eAgInPc4d1	archeologický
nálezy	nález	k1gInPc4	nález
<g/>
,	,	kIx,	,
dokládající	dokládající	k2eAgInSc4d1	dokládající
vývoj	vývoj	k1gInSc4	vývoj
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
portrétuje	portrétovat	k5eAaImIp3nS	portrétovat
i	i	k9	i
vynikající	vynikající	k2eAgMnPc4d1	vynikající
antropology	antropolog	k1gMnPc4	antropolog
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
historii	historie	k1gFnSc4	historie
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
vývojových	vývojový	k2eAgFnPc2d1	vývojová
teorií	teorie	k1gFnPc2	teorie
Prehistoric	Prehistoric	k1gMnSc1	Prehistoric
Reptiles	Reptiles	k1gMnSc1	Reptiles
and	and	k?	and
Birds	Birds	k1gInSc1	Birds
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
anglické	anglický	k2eAgFnSc2d1	anglická
vydáni	vydat	k5eAaPmNgMnP	vydat
knihy	kniha	k1gFnPc1	kniha
Pravěké	pravěký	k2eAgNnSc1d1	pravěké
ptactvo	ptactvo	k1gNnSc1	ptactvo
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Das	Das	k1gFnSc1	Das
Buch	buch	k1gInSc1	buch
von	von	k1gInSc1	von
den	den	k1gInSc1	den
Mammuten	Mammuten	k2eAgInSc1d1	Mammuten
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
mamutů	mamut	k1gMnPc2	mamut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
Prehistoric	Prehistoric	k1gMnSc1	Prehistoric
Sea	Sea	k1gFnSc2	Sea
Monsters	Monstersa	k1gFnPc2	Monstersa
(	(	kIx(	(
<g/>
Prehistorické	prehistorický	k2eAgFnSc2d1	prehistorická
vodní	vodní	k2eAgFnSc2d1	vodní
příšery	příšera	k1gFnSc2	příšera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Age	Age	k1gFnSc2	Age
Of	Of	k1gMnSc1	Of
Monsters	Monsters	k1gInSc1	Monsters
(	(	kIx(	(
<g/>
Věk	věk	k1gInSc1	věk
obrů	obr	k1gMnPc2	obr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
U	u	k7c2	u
pravěkých	pravěký	k2eAgMnPc2d1	pravěký
lovců	lovec	k1gMnPc2	lovec
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
mladšího	mladý	k2eAgNnSc2d2	mladší
období	období	k1gNnSc2	období
starší	starý	k2eAgFnSc2d2	starší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
poplatné	poplatný	k2eAgFnPc1d1	poplatná
své	svůj	k3xOyFgFnSc3	svůj
době	doba	k1gFnSc3	doba
a	a	k8xC	a
reflektují	reflektovat	k5eAaImIp3nP	reflektovat
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
pravěkém	pravěký	k2eAgInSc6d1	pravěký
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
patrné	patrný	k2eAgNnSc1d1	patrné
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
druhohorních	druhohorní	k2eAgMnPc2d1	druhohorní
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
jako	jako	k8xC	jako
studenokrevné	studenokrevný	k2eAgMnPc4d1	studenokrevný
a	a	k8xC	a
pomalé	pomalý	k2eAgMnPc4d1	pomalý
plazy	plaz	k1gMnPc4	plaz
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dnes	dnes	k6eAd1	dnes
převažuje	převažovat	k5eAaImIp3nS	převažovat
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
o	o	k7c6	o
hbitých	hbitý	k2eAgFnPc6d1	hbitá
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
i	i	k8xC	i
neobvykle	obvykle	k6eNd1	obvykle
bystrých	bystrý	k2eAgMnPc2d1	bystrý
tvorů	tvor	k1gMnPc2	tvor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
příbuzensky	příbuzensky	k6eAd1	příbuzensky
bližší	blízký	k2eAgMnPc1d2	bližší
ptákům	pták	k1gMnPc3	pták
než	než	k8xS	než
plazům	plaz	k1gMnPc3	plaz
<g/>
.	.	kIx.	.
</s>
