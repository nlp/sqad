<s>
Prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1903	[number]	k4	1903
Boskovice	Boskovice	k1gInPc1	Boskovice
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1968	[number]	k4	1968
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
paleontolog	paleontolog	k1gMnSc1	paleontolog
a	a	k8xC	a
také	také	k9	také
autor	autor	k1gMnSc1	autor
populárně	populárně	k6eAd1	populárně
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
i	i	k9	i
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
.	.	kIx.	.
</s>
