<s>
Báječná	báječný	k2eAgNnPc1d1	báječné
léta	léto	k1gNnPc1	léto
pod	pod	k7c4	pod
psa	pes	k1gMnSc4	pes
je	být	k5eAaImIp3nS	být
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
román	román	k1gInSc1	román
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Michala	Michal	k1gMnSc2	Michal
Viewegha	Viewegh	k1gMnSc2	Viewegh
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
natočil	natočit	k5eAaBmAgInS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
režisér	režisér	k1gMnSc1	režisér
Petr	Petr	k1gMnSc1	Petr
Nikolaev	Nikolaev	k1gFnSc1	Nikolaev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hlavní	hlavní	k2eAgMnPc1d1	hlavní
postavy	postava	k1gFnSc2	postava
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Ondřej	Ondřej	k1gMnSc1	Ondřej
Vetchý	vetchý	k2eAgMnSc1d1	vetchý
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
Šafránková	Šafránková	k1gFnSc1	Šafránková
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zahálka	Zahálka	k1gMnSc1	Zahálka
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Javorský	Javorský	k1gMnSc1	Javorský
<g/>
,	,	kIx,	,
Vilma	Vilma	k1gFnSc1	Vilma
Cibulková	Cibulková	k1gFnSc1	Cibulková
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Odmaturuj	odmaturovat	k5eAaPmRp2nS	odmaturovat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
-	-	kIx~	-
Báječná	báječný	k2eAgNnPc4d1	báječné
léta	léto	k1gNnPc4	léto
pod	pod	k7c4	pod
psa	pes	k1gMnSc4	pes
Český-jazyk	Českýazyk	k1gMnSc1	Český-jazyk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Viewegh	Viewegh	k1gMnSc1	Viewegh
Michal	Michal	k1gMnSc1	Michal
<g/>
:	:	kIx,	:
Báječná	báječný	k2eAgNnPc4d1	báječné
léta	léto	k1gNnPc4	léto
pod	pod	k7c4	pod
psa	pes	k1gMnSc4	pes
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
–	–	k?	–
Čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
deník	deník	k1gInSc1	deník
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
<g/>
:	:	kIx,	:
Báječná	báječný	k2eAgNnPc4d1	báječné
léta	léto	k1gNnPc4	léto
pod	pod	k7c7	pod
psa	pes	k1gMnSc4	pes
</s>
