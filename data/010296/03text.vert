<p>
<s>
Tenorit	tenorit	k1gInSc1	tenorit
je	být	k5eAaImIp3nS	být
minerál	minerál	k1gInSc4	minerál
krystalující	krystalující	k2eAgInSc4d1	krystalující
v	v	k7c6	v
monoklinické	monoklinický	k2eAgFnSc6d1	monoklinická
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
oxid	oxid	k1gInSc1	oxid
měďnatý	měďnatý	k2eAgInSc1d1	měďnatý
-	-	kIx~	-
CuO	CuO	k1gFnSc1	CuO
<g/>
.	.	kIx.	.
</s>
<s>
Objeven	objeven	k2eAgMnSc1d1	objeven
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
a	a	k8xC	a
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
italského	italský	k2eAgMnSc2d1	italský
botanika	botanik	k1gMnSc2	botanik
M.	M.	kA	M.
Tenora	tenor	k1gMnSc2	tenor
<g/>
.	.	kIx.	.
</s>
<s>
Systematické	systematický	k2eAgNnSc1d1	systematické
zařazení	zařazení	k1gNnSc1	zařazení
podle	podle	k7c2	podle
Strunze	strunga	k1gFnSc6	strunga
je	být	k5eAaImIp3nS	být
IV	IV	kA	IV
<g/>
/	/	kIx~	/
<g/>
A	A	kA	A
<g/>
.05	.05	k4	.05
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
ložiscích	ložisko	k1gNnPc6	ložisko
minerálů	minerál	k1gInPc2	minerál
mědi	měď	k1gFnSc2	měď
jako	jako	k8xC	jako
sekundární	sekundární	k2eAgInSc4d1	sekundární
minerál	minerál	k1gInSc4	minerál
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
chryzokolem	chryzokol	k1gInSc7	chryzokol
<g/>
,	,	kIx,	,
malachitem	malachit	k1gInSc7	malachit
<g/>
,	,	kIx,	,
olivenitem	olivenit	k1gInSc7	olivenit
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Morfologie	morfologie	k1gFnSc2	morfologie
==	==	k?	==
</s>
</p>
<p>
<s>
Tenorit	tenorit	k1gInSc1	tenorit
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
práškovitý	práškovitý	k2eAgMnSc1d1	práškovitý
<g/>
,	,	kIx,	,
zemitý	zemitý	k2eAgMnSc1d1	zemitý
<g/>
,	,	kIx,	,
kusový	kusový	k2eAgMnSc1d1	kusový
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
výplně	výplň	k1gFnPc1	výplň
puklin	puklina	k1gFnPc2	puklina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
černý	černý	k2eAgInSc1d1	černý
nebo	nebo	k8xC	nebo
šedočerný	šedočerný	k2eAgInSc1d1	šedočerný
minerál	minerál	k1gInSc1	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Vryp	vryp	k1gInSc1	vryp
tenoritu	tenorit	k1gInSc2	tenorit
je	být	k5eAaImIp3nS	být
také	také	k9	také
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
neprůhledný	průhledný	k2eNgInSc1d1	neprůhledný
minerál	minerál	k1gInSc1	minerál
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
leskem	lesk	k1gInSc7	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nP	vytvářet
srůsty	srůst	k1gInPc1	srůst
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Tvrdost	tvrdost	k1gFnSc1	tvrdost
podle	podle	k7c2	podle
Mohsovy	Mohsův	k2eAgFnSc2d1	Mohsova
stupnice	stupnice	k1gFnSc2	stupnice
3,5	[number]	k4	3,5
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
6,5	[number]	k4	6,5
<g/>
.	.	kIx.	.
</s>
<s>
Lom	lom	k1gInSc1	lom
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Optické	optický	k2eAgFnPc4d1	optická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
černé	černý	k2eAgFnPc4d1	černá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
šedočerných	šedočerný	k2eAgInPc2d1	šedočerný
(	(	kIx(	(
<g/>
ocelově	ocelově	k6eAd1	ocelově
šedých	šedá	k1gFnPc2	šedá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
je	být	k5eAaImIp3nS	být
matný	matný	k2eAgInSc1d1	matný
<g/>
.	.	kIx.	.
</s>
<s>
Průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
opakní	opaknit	k5eAaPmIp3nS	opaknit
<g/>
.	.	kIx.	.
</s>
<s>
Vryp	vryp	k1gInSc1	vryp
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Procentuální	procentuální	k2eAgNnSc1d1	procentuální
zastoupení	zastoupení	k1gNnSc1	zastoupení
prvků	prvek	k1gInPc2	prvek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Cu	Cu	k?	Cu
79,89	[number]	k4	79,89
%	%	kIx~	%
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
O	o	k7c6	o
20,11	[number]	k4	20,11
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Rokytnice	Rokytnice	k1gFnSc1	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
</s>
</p>
<p>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
Špania	Španium	k1gNnPc1	Španium
Dolina	dolina	k1gFnSc1	dolina
</s>
</p>
<p>
<s>
Piesky	Piesky	k6eAd1	Piesky
</s>
</p>
<p>
<s>
Novoveská	Novoveský	k2eAgFnSc1d1	Novoveská
Huta	Huta	k1gFnSc1	Huta
</s>
</p>
<p>
<s>
Mednorudjansk	Mednorudjansk	k1gInSc1	Mednorudjansk
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Copper	Copper	k1gMnSc1	Copper
Harbor	Harbor	k1gMnSc1	Harbor
(	(	kIx(	(
<g/>
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bisbee	Bisbee	k1gFnSc1	Bisbee
(	(	kIx(	(
<g/>
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
Dodoma	Dodoma	k1gFnSc1	Dodoma
(	(	kIx(	(
<g/>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tenorit	tenorit	k1gInSc1	tenorit
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tenorit	tenorit	k1gInSc1	tenorit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Tenorit	tenorit	k1gInSc1	tenorit
v	v	k7c6	v
Atlase	atlas	k1gInSc6	atlas
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
sci	sci	k?	sci
<g/>
.	.	kIx.	.
<g/>
muni	muni	k?	muni
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tenorit	tenorit	k1gInSc1	tenorit
na	na	k7c4	na
Mineraly	Mineral	k1gMnPc4	Mineral
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
<p>
<s>
Tenorit	tenorit	k1gInSc1	tenorit
na	na	k7c6	na
Webmineral	Webmineral	k1gFnSc6	Webmineral
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tenorit	tenorit	k1gInSc1	tenorit
na	na	k7c6	na
Mindat	Mindat	k1gFnSc6	Mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
