<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
Entente	entente	k1gFnSc2	entente
Florale	Floral	k1gMnSc5	Floral
CZ	CZ	kA	CZ
–	–	k?	–
Souznění	souznění	k1gNnSc1	souznění
<g/>
,	,	kIx,	,
z.	z.	k?	z.
<g/>
s.	s.	k?	s.
je	být	k5eAaImIp3nS	být
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
aktivity	aktivita	k1gFnPc4	aktivita
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
soutěži	soutěž	k1gFnSc6	soutěž
Entente	entente	k1gFnSc2	entente
Florale	Floral	k1gMnSc5	Floral
Europe	Europ	k1gInSc5	Europ
v	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
shodný	shodný	k2eAgInSc1d1	shodný
cíl	cíl	k1gInSc1	cíl
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
společně	společně	k6eAd1	společně
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
kvalitu	kvalita	k1gFnSc4	kvalita
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
života	život	k1gInSc2	život
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
obcích	obec	k1gFnPc6	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
pořádá	pořádat	k5eAaImIp3nS	pořádat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
partnery	partner	k1gMnPc4	partner
následující	následující	k2eAgInPc1d1	následující
projekty	projekt	k1gInPc1	projekt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc4d1	národní
putovní	putovní	k2eAgFnSc4d1	putovní
výstavu	výstava	k1gFnSc4	výstava
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
cestami	cesta	k1gFnPc7	cesta
proměn	proměna	k1gFnPc2	proměna
</s>
</p>
<p>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
o	o	k7c4	o
titul	titul	k1gInSc4	titul
Nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
nádraží	nádraží	k1gNnSc4	nádraží
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Podvečery	podvečer	k1gInPc1	podvečer
s	s	k7c7	s
osobnostmi	osobnost	k1gFnPc7	osobnost
zahradní	zahradní	k2eAgFnSc2d1	zahradní
a	a	k8xC	a
krajinářské	krajinářský	k2eAgFnSc2d1	krajinářská
tvorby	tvorba	k1gFnSc2	tvorba
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
Asociace	asociace	k1gFnSc2	asociace
Entente	entente	k1gFnSc2	entente
Florale	Floral	k1gMnSc5	Floral
CZ	CZ	kA	CZ
–	–	k?	–
Souznění	souznění	k1gNnSc6	souznění
je	být	k5eAaImIp3nS	být
Ing.	ing.	kA	ing.
Karel	Karel	k1gMnSc1	Karel
Drhovský	Drhovský	k1gMnSc1	Drhovský
<g/>
,	,	kIx,	,
viceprezidenty	viceprezident	k1gMnPc4	viceprezident
Mgr.	Mgr.	kA	Mgr.
Anna	Anna	k1gFnSc1	Anna
Pavlíková	Pavlíková	k1gFnSc1	Pavlíková
Kutějová	Kutějový	k2eAgFnSc1d1	Kutějová
<g/>
,	,	kIx,	,
PhDr.	PhDr.	kA	PhDr.
Pavel	Pavel	k1gMnSc1	Pavel
Bureš	Bureš	k1gMnSc1	Bureš
a	a	k8xC	a
JUDr.	JUDr.	kA	JUDr.
Jiří	Jiří	k1gMnSc1	Jiří
Štancl	Štancl	k1gMnSc1	Štancl
<g/>
,	,	kIx,	,
ředitelkou	ředitelka	k1gFnSc7	ředitelka
je	být	k5eAaImIp3nS	být
Ing.	ing.	kA	ing.
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Kolmanová	Kolmanová	k1gFnSc1	Kolmanová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
http://www.aefcz.eu/aef-cz	[url]	k1gMnSc1	http://www.aefcz.eu/aef-cz
</s>
</p>
<p>
<s>
http://www.propamatky.info/cs/katalog-sluzeb/hlavni-mesto-praha/neziskovy-sektor/asociace-entente-florale-cz-souzneni-z-s-/1102/	[url]	k4	http://www.propamatky.info/cs/katalog-sluzeb/hlavni-mesto-praha/neziskovy-sektor/asociace-entente-florale-cz-souzneni-z-s-/1102/
</s>
</p>
<p>
<s>
https://www.youtube.com/channel/UC8acuc-spU8KqyS6Bv0wAmg	[url]	k1gMnSc1	https://www.youtube.com/channel/UC8acuc-spU8KqyS6Bv0wAmg
</s>
</p>
<p>
<s>
https://www.novinky.cz/bydleni/tipy-a-trendy/420557-nejkrasnejsim-nadrazim-letosniho-roku-je-architektonicky-cenna-stanice-v-mimoni.html	[url]	k1gMnSc1	https://www.novinky.cz/bydleni/tipy-a-trendy/420557-nejkrasnejsim-nadrazim-letosniho-roku-je-architektonicky-cenna-stanice-v-mimoni.html
</s>
</p>
<p>
<s>
http://www.ceskatelevize.cz/ct24/regiony/1954649-nejhezci-nadrazi-maji-v-mimoni-cihlova-kraska-zaujala-historii-i-hospodou-s-pivnim	[url]	k6eAd1	http://www.ceskatelevize.cz/ct24/regiony/1954649-nejhezci-nadrazi-maji-v-mimoni-cihlova-kraska-zaujala-historii-i-hospodou-s-pivnim
</s>
</p>
<p>
<s>
https://zpravy.aktualne.cz/ekonomika/projdete-si-nejkrasnejsi-nadrazi-v-cesku-anketu-vyhrali-novy/r~4dc0d17a885e11e5a80c0025900fea04/?redirected=1494960201	[url]	k4	https://zpravy.aktualne.cz/ekonomika/projdete-si-nejkrasnejsi-nadrazi-v-cesku-anketu-vyhrali-novy/r~4dc0d17a885e11e5a80c0025900fea04/?redirected=1494960201
</s>
</p>
<p>
<s>
http://zlin.cz/528099n-promeny-obci-zlinskeho-kraje-se-predstavi-na-narodni-vystave	[url]	k5eAaPmIp3nS	http://zlin.cz/528099n-promeny-obci-zlinskeho-kraje-se-predstavi-na-narodni-vystave
</s>
</p>
