<s>
Prezidentem	prezident	k1gMnSc7	prezident
Asociace	asociace	k1gFnSc2	asociace
Entente	entente	k1gFnSc2	entente
Florale	Floral	k1gMnSc5	Floral
CZ	CZ	kA	CZ
–	–	k?	–
Souznění	souznění	k1gNnSc6	souznění
je	být	k5eAaImIp3nS	být
Ing.	ing.	kA	ing.
Karel	Karel	k1gMnSc1	Karel
Drhovský	Drhovský	k1gMnSc1	Drhovský
<g/>
,	,	kIx,	,
viceprezidenty	viceprezident	k1gMnPc4	viceprezident
Mgr.	Mgr.	kA	Mgr.
Anna	Anna	k1gFnSc1	Anna
Pavlíková	Pavlíková	k1gFnSc1	Pavlíková
Kutějová	Kutějový	k2eAgFnSc1d1	Kutějová
<g/>
,	,	kIx,	,
PhDr.	PhDr.	kA	PhDr.
Pavel	Pavel	k1gMnSc1	Pavel
Bureš	Bureš	k1gMnSc1	Bureš
a	a	k8xC	a
JUDr.	JUDr.	kA	JUDr.
Jiří	Jiří	k1gMnSc1	Jiří
Štancl	Štancl	k1gMnSc1	Štancl
<g/>
,	,	kIx,	,
ředitelkou	ředitelka	k1gFnSc7	ředitelka
je	být	k5eAaImIp3nS	být
Ing.	ing.	kA	ing.
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Kolmanová	Kolmanová	k1gFnSc1	Kolmanová
<g/>
.	.	kIx.	.
</s>
