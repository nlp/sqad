<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
Platné	platný	k2eAgFnSc2d1
ukrajinské	ukrajinský	k2eAgFnSc2d1
minceZemě	minceZemě	k6eAd1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
UAH	UAH	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
12,8	12,8	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2017	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
₴	₴	k?
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
kopějka	kopějka	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
kopějek	kopějka	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
a	a	k8xC
10	#num#	k4
hřiven	hřivna	k1gFnPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
hřiven	hřivna	k1gFnPc2
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
г	г	k?
<g/>
,	,	kIx,
hryvňa	hryvňa	k1gFnSc1
<g/>
;	;	kIx,
[	[	kIx(
<g/>
ˈ	ˈ	k5eAaPmIp1nS
<g/>
̯	̯	k?
<g/>
nʲ	nʲ	k?
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
,	,	kIx,
₴	₴	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
východoevropského	východoevropský	k2eAgInSc2d1
státu	stát	k1gInSc2
Ukrajina	Ukrajina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc1
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
je	být	k5eAaImIp3nS
UAH	UAH	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
hřivna	hřivna	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
100	#num#	k4
kopijkami	kopijka	k1gFnPc7
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
к	к	k?
/	/	kIx~
kopijka	kopijka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc4
hřivna	hřivna	k1gFnSc1
měla	mít	k5eAaImAgFnS
už	už	k9
měna	měna	k1gFnSc1
Kyjevské	kyjevský	k2eAgFnSc2d1
Rusi	Rus	k1gFnSc2
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Měnový	měnový	k2eAgInSc1d1
kurz	kurz	k1gInSc1
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
se	se	k3xPyFc4
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
držela	držet	k5eAaImAgFnS
okolo	okolo	k7c2
1,50	1,50	k4
Kč	Kč	kA
za	za	k7c4
1	#num#	k4
UAH	UAH	kA
<g/>
,	,	kIx,
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
zaznamenala	zaznamenat	k5eAaPmAgFnS
rychlý	rychlý	k2eAgInSc4d1
propad	propad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
dne	den	k1gInSc2
na	na	k7c4
den	den	k1gInSc4
ztratila	ztratit	k5eAaPmAgFnS
vůči	vůči	k7c3
koruně	koruna	k1gFnSc3
32,23	32,23	k4
%	%	kIx~
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
úroveň	úroveň	k1gFnSc4
0,98	0,98	k4
Kč	Kč	kA
za	za	k7c4
1	#num#	k4
UAH	UAH	kA
a	a	k8xC
má	mít	k5eAaImIp3nS
stále	stále	k6eAd1
klesající	klesající	k2eAgFnSc4d1
tendenci	tendence	k1gFnSc4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
úrovni	úroveň	k1gFnSc6
0,84	0,84	k4
Kč	Kč	kA
za	za	k7c4
1	#num#	k4
UAH	UAH	kA
a	a	k8xC
ekonomové	ekonom	k1gMnPc1
očekávají	očekávat	k5eAaImIp3nP
že	že	k8xS
bude	být	k5eAaImBp3nS
i	i	k9
nadále	nadále	k6eAd1
klesat	klesat	k5eAaImF
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Dokladem	doklad	k1gInSc7
křehkosti	křehkost	k1gFnSc2
měny	měna	k1gFnSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
ukrajinská	ukrajinský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
neudává	udávat	k5eNaImIp3nS
svoje	svůj	k3xOyFgInPc4
mezinárodní	mezinárodní	k2eAgInPc4d1
tarify	tarif	k1gInPc4
v	v	k7c6
hřivnách	hřivna	k1gFnPc6
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
v	v	k7c6
amerických	americký	k2eAgInPc6d1
dolarech	dolar	k1gInPc6
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
cena	cena	k1gFnSc1
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
u	u	k7c2
přepážek	přepážka	k1gFnPc2
přepočítávána	přepočítáván	k2eAgFnSc1d1
na	na	k7c4
hřivny	hřivna	k1gFnPc4
podle	podle	k7c2
aktuálního	aktuální	k2eAgInSc2d1
kurzu	kurz	k1gInSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
se	se	k3xPyFc4
do	do	k7c2
oběhu	oběh	k1gInSc2
dostala	dostat	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1996	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nahradila	nahradit	k5eAaPmAgFnS
předešlou	předešlý	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
–	–	k?
ukrajinský	ukrajinský	k2eAgInSc1d1
karbovanec	karbovanec	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
kupon	kupon	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
dočasnou	dočasný	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
v	v	k7c6
období	období	k1gNnSc6
přechodu	přechod	k1gInSc2
ze	z	k7c2
sovětského	sovětský	k2eAgInSc2d1
rublu	rubl	k1gInSc2
na	na	k7c4
ukrajinskou	ukrajinský	k2eAgFnSc4d1
hřivnu	hřivna	k1gFnSc4
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřivna	hřivna	k1gFnSc1
vycházela	vycházet	k5eAaImAgFnS
z	z	k7c2
karbovance	karbovanec	k1gInSc2
v	v	k7c6
poměru	poměr	k1gInSc6
100	#num#	k4
000	#num#	k4
karbovanců	karbovanec	k1gInPc2
=	=	kIx~
1	#num#	k4
hřivna	hřivna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karbovanec	karbovanec	k1gInSc1
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
překlenul	překlenout	k5eAaPmAgInS
období	období	k1gNnSc4
hyperinflace	hyperinflace	k1gFnSc2
a	a	k8xC
aby	aby	kYmCp3nS
nová	nový	k2eAgFnSc1d1
ukrajinská	ukrajinský	k2eAgFnSc1d1
měna	měna	k1gFnSc1
tento	tento	k3xDgInSc4
problém	problém	k1gInSc4
už	už	k6eAd1
neměla	mít	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
začaly	začít	k5eAaPmAgFnP
přípravy	příprava	k1gFnPc1
na	na	k7c4
zavedení	zavedení	k1gNnSc4
hřivny	hřivna	k1gFnSc2
–	–	k?
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
byly	být	k5eAaImAgFnP
vytištěny	vytisknout	k5eAaPmNgFnP
první	první	k4xOgFnPc1
bankovky	bankovka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
čekaly	čekat	k5eAaImAgFnP
na	na	k7c4
své	své	k1gNnSc4
použití	použití	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgMnPc1d1
prezident	prezident	k1gMnSc1
Ukrajiny	Ukrajina	k1gFnSc2
podepsal	podepsat	k5eAaPmAgMnS
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1996	#num#	k4
zákon	zákon	k1gInSc1
o	o	k7c4
zavedení	zavedení	k1gNnSc4
hřivny	hřivna	k1gFnSc2
do	do	k7c2
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karbovanec	karbovanec	k1gInSc4
a	a	k8xC
hřivna	hřivna	k1gFnSc1
souběžně	souběžně	k6eAd1
platily	platit	k5eAaImAgInP
mezi	mezi	k7c4
2	#num#	k4
<g/>
.	.	kIx.
zářím	zářit	k5eAaImIp1nS
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
zářím	zářit	k5eAaImIp1nS
<g/>
,	,	kIx,
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
byla	být	k5eAaImAgFnS
jedinou	jediný	k2eAgFnSc7d1
platnou	platný	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
hřivna	hřivna	k1gFnSc1
a	a	k8xC
karbovanec	karbovanec	k1gInSc1
přestal	přestat	k5eAaPmAgInS
existovat	existovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
Mince	mince	k1gFnSc1
ukrajinské	ukrajinský	k2eAgFnSc2d1
hřivny	hřivna	k1gFnSc2
(	(	kIx(
<g/>
hryvni	hryvnit	k5eAaPmRp2nS
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
kopijok	kopijok	k1gInSc4
a	a	k8xC
1	#num#	k4
hřivna	hřivna	k1gFnSc1
(	(	kIx(
<g/>
hryvňa	hryvňa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
ukrajinských	ukrajinský	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
</s>
<s>
1	#num#	k4
kopijka	kopijka	k1gFnSc1
</s>
<s>
2	#num#	k4
kopijky	kopijka	k1gFnPc4
</s>
<s>
5	#num#	k4
kopijok	kopijok	k1gInSc1
</s>
<s>
10	#num#	k4
kopijok	kopijok	k1gInSc1
</s>
<s>
25	#num#	k4
kopijok	kopijok	k1gInSc1
</s>
<s>
50	#num#	k4
kopijok	kopijok	k1gInSc1
</s>
<s>
1	#num#	k4
hřivna	hřivna	k1gFnSc1
</s>
<s>
1	#num#	k4
hřivna	hřivna	k1gFnSc1
</s>
<s>
Bankovky	bankovka	k1gFnPc1
</s>
<s>
Bankovky	bankovka	k1gFnPc1
v	v	k7c6
oběhu	oběh	k1gInSc6
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
a	a	k8xC
1000	#num#	k4
hřiven	hřivna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
série	série	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vnikla	vniknout	k5eAaPmAgFnS
do	do	k7c2
oběhu	oběh	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
,	,	kIx,
zahrnovala	zahrnovat	k5eAaImAgFnS
bankovky	bankovka	k1gFnPc4
o	o	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
a	a	k8xC
100	#num#	k4
hřiven	hřivna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
série	série	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
)	)	kIx)
obsahovala	obsahovat	k5eAaImAgFnS
i	i	k9
dvousethřivnovou	dvousethřivnový	k2eAgFnSc4d1
bankovku	bankovka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
série	série	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
)	)	kIx)
zahrnuje	zahrnovat	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
předešlé	předešlý	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
a	a	k8xC
nově	nově	k6eAd1
i	i	k8xC
bankovku	bankovka	k1gFnSc4
500	#num#	k4
hřiven	hřivna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukrajinská	ukrajinský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
stahuje	stahovat	k5eAaImIp3nS
z	z	k7c2
oběhu	oběh	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
sérii	série	k1gFnSc4
bankovek	bankovka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
už	už	k6eAd1
jen	jen	k9
zřídka	zřídka	k6eAd1
používané	používaný	k2eAgInPc1d1
(	(	kIx(
<g/>
ale	ale	k8xC
i	i	k9
nadále	nadále	k6eAd1
jsou	být	k5eAaImIp3nP
platné	platný	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
série	série	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
)	)	kIx)
zahrnuje	zahrnovat	k5eAaImIp3nS
bankovky	bankovka	k1gFnPc4
o	o	k7c6
hodnotách	hodnota	k1gFnPc6
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
hřiven	hřivna	k1gFnPc2
a	a	k8xC
nově	nově	k6eAd1
také	také	k9
bankovku	bankovka	k1gFnSc4
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
1000	#num#	k4
hřiven	hřivna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovky	bankovka	k1gFnSc2
o	o	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
a	a	k8xC
10	#num#	k4
hřiven	hřivna	k1gFnPc2
zůstavají	zůstavat	k5eAaPmIp3nP,k5eAaImIp3nP
nadále	nadále	k6eAd1
platné	platný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
serie	serie	k1gFnSc1
</s>
<s>
VyobrazeníHodnotaRozměryBarvaMotiv	VyobrazeníHodnotaRozměryBarvaMotit	k5eAaPmDgInS
na	na	k7c4
aversní	aversní	k2eAgNnSc4d1
straněUvedení	straněUvedení	k1gNnSc4
do	do	k7c2
oběhu	oběh	k1gInSc2
</s>
<s>
AversRevers	AversRevers	k6eAd1
</s>
<s>
1	#num#	k4
Hřivna	hřivna	k1gFnSc1
</s>
<s>
118	#num#	k4
×	×	k?
63	#num#	k4
mm	mm	kA
</s>
<s>
šedozelená	šedozelený	k2eAgFnSc1d1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2004	#num#	k4
</s>
<s>
1	#num#	k4
Hřivna	hřivna	k1gFnSc1
</s>
<s>
118	#num#	k4
×	×	k?
63	#num#	k4
mm	mm	kA
</s>
<s>
žlutá	žlutý	k2eAgFnSc1d1
a	a	k8xC
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
</s>
<s>
2	#num#	k4
Hřivny	hřivna	k1gFnPc1
</s>
<s>
118	#num#	k4
×	×	k?
63	#num#	k4
mm	mm	kA
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Moudrý	moudrý	k2eAgMnSc1d1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2004	#num#	k4
</s>
<s>
5	#num#	k4
hřiven	hřivna	k1gFnPc2
</s>
<s>
118	#num#	k4
×	×	k?
63	#num#	k4
mm	mm	kA
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Bohdan	Bohdan	k1gMnSc1
Chmelnický	Chmelnický	k2eAgMnSc1d1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2004	#num#	k4
</s>
<s>
10	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
124	#num#	k4
×	×	k?
66	#num#	k4
mm	mm	kA
</s>
<s>
červená	červený	k2eAgFnSc1d1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Mazepa	Mazep	k1gMnSc2
</s>
<s>
srpen	srpen	k1gInSc1
2006	#num#	k4
</s>
<s>
20	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
130	#num#	k4
×	×	k?
69	#num#	k4
mm	mm	kA
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Franko	franko	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
</s>
<s>
50	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
136	#num#	k4
x	x	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
fialová	fialový	k2eAgFnSc1d1
</s>
<s>
Mychajlo	Mychajlo	k1gNnSc1
Hruševskyj	Hruševskyj	k1gMnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2004	#num#	k4
</s>
<s>
100	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
142	#num#	k4
x	x	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
žluto-zelená	žluto-zelený	k2eAgFnSc1d1
</s>
<s>
Taras	taras	k1gInSc1
Ševčenko	Ševčenka	k1gFnSc5
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2006	#num#	k4
</s>
<s>
200	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
148	#num#	k4
×	×	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
růžová	růžový	k2eAgFnSc1d1
</s>
<s>
Lesja	Lesj	k2eAgFnSc1d1
Ukrajinka	Ukrajinka	k1gFnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
</s>
<s>
500	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
154	#num#	k4
×	×	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
oranžová	oranžový	k2eAgFnSc1d1
</s>
<s>
Hryhorij	Hryhorít	k5eAaPmRp2nS
Skovoroda	Skovoroda	k1gFnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2006	#num#	k4
</s>
<s>
Bankovky	bankovka	k1gFnPc1
4	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
</s>
<s>
VyobrazeníHodnotaRozměryBarvaMotiv	VyobrazeníHodnotaRozměryBarvaMotit	k5eAaPmDgInS
na	na	k7c4
aversní	aversní	k2eAgNnSc4d1
straněUvedení	straněUvedení	k1gNnSc4
do	do	k7c2
oběhu	oběh	k1gInSc2
</s>
<s>
AversRevers	AversRevers	k6eAd1
</s>
<s>
20	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
130	#num#	k4
×	×	k?
69	#num#	k4
mm	mm	kA
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Franko	franko	k6eAd1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2018	#num#	k4
</s>
<s>
50	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
136	#num#	k4
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
fialová	fialový	k2eAgFnSc1d1
</s>
<s>
Mychajlo	Mychajlo	k1gNnSc1
Hruševskyj	Hruševskyj	k1gMnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
</s>
<s>
100	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
142	#num#	k4
×	×	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
žluto-zelená	žluto-zelený	k2eAgFnSc1d1
</s>
<s>
Taras	taras	k1gInSc1
Ševčenko	Ševčenka	k1gFnSc5
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
</s>
<s>
200	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
154	#num#	k4
×	×	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
růžová	růžový	k2eAgFnSc1d1
</s>
<s>
Lesja	Lesj	k2eAgFnSc1d1
Ukrajinka	Ukrajinka	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
</s>
<s>
500	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
154	#num#	k4
×	×	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
bežová	bežovat	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Hryhorij	Hryhorít	k5eAaPmRp2nS
Skovoroda	Skovoroda	k1gFnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
</s>
<s>
1000	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
160	#num#	k4
x	x	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Volodymyr	Volodymyr	k1gMnSc1
Vernadskyj	Vernadskyj	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
</s>
<s>
Speciální	speciální	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
</s>
<s>
Bankovky	bankovka	k1gFnPc1
</s>
<s>
VyobrazeníHodnotaRozměryBarvaMotiv	VyobrazeníHodnotaRozměryBarvaMotit	k5eAaPmDgInS
na	na	k7c4
aversní	aversní	k2eAgNnSc4d1
straněUvedení	straněUvedení	k1gNnSc4
do	do	k7c2
oběhu	oběh	k1gInSc2
</s>
<s>
AversRevers	AversRevers	k6eAd1
</s>
<s>
20	#num#	k4
Hřiven	hřivna	k1gFnPc2
(	(	kIx(
<g/>
pamětní	pamětní	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
130	#num#	k4
×	×	k?
69	#num#	k4
mm	mm	kA
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Franko	franko	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2016	#num#	k4
</s>
<s>
50	#num#	k4
Hřiven	hřivna	k1gFnPc2
(	(	kIx(
<g/>
pamětní	pamětní	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
136	#num#	k4
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
fialová	fialový	k2eAgFnSc1d1
</s>
<s>
Mychajlo	Mychajlo	k1gNnSc1
Hruševskyj	Hruševskyj	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2011	#num#	k4
</s>
<s>
500	#num#	k4
Hřiven	hřivna	k1gFnPc2
</s>
<s>
154	#num#	k4
×	×	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Hryhorij	Hryhorít	k5eAaPmRp2nS
Skovoroda	Skovoroda	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
čerence	čerenec	k1gInSc2
2015	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
bankovky	bankovka	k1gFnSc2
10	#num#	k4
hřiven	hřivna	k1gFnPc2
</s>
<s>
rok	rok	k1gInSc1
1996	#num#	k4
</s>
<s>
rok	rok	k1gInSc1
1997	#num#	k4
</s>
<s>
rok	rok	k1gInSc1
2004	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
</s>
<s>
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
–	–	k?
tarify	tarifa	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
18	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
ukrposhta	ukrposhta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
ua	ua	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Ukrajinské	ukrajinský	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
,	,	kIx,
bank	banka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
ua	ua	k?
</s>
<s>
Ukrajinské	ukrajinský	k2eAgFnPc4d1
mince	mince	k1gFnPc4
na	na	k7c6
stránkách	stránka	k1gFnPc6
Národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
,	,	kIx,
bank	banka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
ua	ua	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4618908-7	4618908-7	k4
</s>
