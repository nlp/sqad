<s>
Coby	Coby	k?	Coby
historická	historický	k2eAgFnSc1d1	historická
metropole	metropole	k1gFnSc1	metropole
Čech	Čechy	k1gFnPc2	Čechy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
římsko-německých	římskoěmecký	k2eAgMnPc2d1	římsko-německý
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
