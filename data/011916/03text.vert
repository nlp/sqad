<p>
<s>
Kopernicium	Kopernicium	k1gNnSc1	Kopernicium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cn	Cn	k1gFnSc2	Cn
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Copernicium	Copernicium	k1gNnSc4	Copernicium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
<g/>
.	.	kIx.	.
transuranem	transuran	k1gInSc7	transuran
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc4d1	připravovaný
uměle	uměle	k6eAd1	uměle
v	v	k7c6	v
cyklotronu	cyklotron	k1gInSc6	cyklotron
nebo	nebo	k8xC	nebo
urychlovači	urychlovač	k1gInSc6	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
objevitelé	objevitel	k1gMnPc1	objevitel
<g/>
,	,	kIx,	,
oznámila	oznámit	k5eAaPmAgFnS	oznámit
IUPAC	IUPAC	kA	IUPAC
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
právě	právě	k9	právě
v	v	k7c4	v
den	den	k1gInSc4	den
narozenin	narozeniny	k1gFnPc2	narozeniny
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Koperníka	Koperník	k1gMnSc2	Koperník
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgNnSc6	který
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc1	prvek
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
<g/>
.	.	kIx.	.
<g/>
Kopernicium	Kopernicium	k1gNnSc1	Kopernicium
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
doposud	doposud	k6eAd1	doposud
nebyl	být	k5eNaImAgInS	být
izolován	izolovat	k5eAaBmNgInS	izolovat
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
určit	určit	k5eAaPmF	určit
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
konstanty	konstanta	k1gFnPc4	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
poloze	poloha	k1gFnSc6	poloha
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
prvků	prvek	k1gInPc2	prvek
by	by	k9	by
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
mělo	mít	k5eAaImAgNnS	mít
připomínat	připomínat	k5eAaImF	připomínat
rtuť	rtuť	k1gFnSc4	rtuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
přípravu	příprava	k1gFnSc4	příprava
prvku	prvek	k1gInSc2	prvek
s	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
112	[number]	k4	112
(	(	kIx(	(
<g/>
s	s	k7c7	s
dočasným	dočasný	k2eAgInSc7d1	dočasný
názvem	název	k1gInSc7	název
ununbium	ununbium	k1gNnSc4	ununbium
<g/>
)	)	kIx)	)
oznámili	oznámit	k5eAaPmAgMnP	oznámit
němečtí	německý	k2eAgMnPc1d1	německý
fyzici	fyzik	k1gMnPc1	fyzik
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
z	z	k7c2	z
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
těžkých	těžký	k2eAgInPc2d1	těžký
iontů	ion	k1gInPc2	ion
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Darmstadtu	Darmstadt	k1gInSc6	Darmstadt
<g/>
.	.	kIx.	.
</s>
<s>
Bombardováním	bombardování	k1gNnSc7	bombardování
izotopu	izotop	k1gInSc2	izotop
olova	olovo	k1gNnSc2	olovo
jádry	jádro	k1gNnPc7	jádro
atomu	atom	k1gInSc6	atom
zinku	zinek	k1gInSc2	zinek
získali	získat	k5eAaPmAgMnP	získat
izotop	izotop	k1gInSc4	izotop
277	[number]	k4	277
<g/>
Cn	Cn	k1gFnPc2	Cn
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
asi	asi	k9	asi
0,2	[number]	k4	0,2
milisekundy	milisekunda	k1gFnPc4	milisekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20882	[number]	k4	20882
Pb	Pb	k1gFnSc1	Pb
+	+	kIx~	+
7030	[number]	k4	7030
Zn	zn	kA	zn
→	→	k?	→
278112	[number]	k4	278112
Cn	Cn	k1gFnSc1	Cn
→	→	k?	→
277112	[number]	k4	277112
Cn	Cn	k1gFnPc2	Cn
+	+	kIx~	+
10	[number]	k4	10
nV	nV	k?	nV
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
detekoval	detekovat	k5eAaImAgInS	detekovat
tým	tým	k1gInSc1	tým
výzkumníků	výzkumník	k1gMnPc2	výzkumník
v	v	k7c6	v
Dubně	Dubna	k1gFnSc6	Dubna
kopernicium	kopernicium	k1gNnSc1	kopernicium
282	[number]	k4	282
<g/>
Cn	Cn	k1gFnPc2	Cn
jako	jako	k8xC	jako
produkt	produkt	k1gInSc1	produkt
alfa	alfa	k1gNnSc2	alfa
rozpadu	rozpad	k1gInSc2	rozpad
oganessonu	oganesson	k1gInSc2	oganesson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
294118	[number]	k4	294118
Uuo	Uuo	k1gFnSc1	Uuo
→	→	k?	→
290116	[number]	k4	290116
Lv	Lv	k1gFnSc1	Lv
→	→	k?	→
286114	[number]	k4	286114
Fl	Fl	k1gFnSc1	Fl
→	→	k?	→
282112	[number]	k4	282112
CnNejstálejší	CnNejstálý	k2eAgInSc1d2	CnNejstálý
známý	známý	k2eAgInSc1d1	známý
izotop	izotop	k1gInSc1	izotop
kopernicia	kopernicium	k1gNnSc2	kopernicium
je	být	k5eAaImIp3nS	být
285	[number]	k4	285
<g/>
Cn	Cn	k1gFnPc2	Cn
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
29	[number]	k4	29
s.	s.	k?	s.
Další	další	k2eAgInSc1d1	další
izotop	izotop	k1gInSc1	izotop
<g/>
,	,	kIx,	,
283	[number]	k4	283
<g/>
Cn	Cn	k1gMnPc2	Cn
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
na	na	k7c4	na
darmstadtium	darmstadtium	k1gNnSc4	darmstadtium
při	při	k7c6	při
vyzáření	vyzáření	k1gNnSc6	vyzáření
částice	částice	k1gFnSc2	částice
alfa	alfa	k1gNnSc2	alfa
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
oficiálně	oficiálně	k6eAd1	oficiálně
existující	existující	k2eAgInSc4d1	existující
prvek	prvek	k1gInSc4	prvek
však	však	k8xC	však
kopernicium	kopernicium	k1gNnSc4	kopernicium
uznala	uznat	k5eAaPmAgFnS	uznat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
unie	unie	k1gFnSc1	unie
pro	pro	k7c4	pro
čistou	čistý	k2eAgFnSc4d1	čistá
a	a	k8xC	a
užitou	užitý	k2eAgFnSc4d1	užitá
chemii	chemie	k1gFnSc4	chemie
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Darmstadtský	Darmstadtský	k2eAgInSc1d1	Darmstadtský
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
tým	tým	k1gInSc1	tým
vedený	vedený	k2eAgInSc1d1	vedený
prof.	prof.	kA	prof.
Sigurdem	Sigurd	k1gInSc7	Sigurd
Hofmannem	Hofmanno	k1gNnSc7	Hofmanno
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
Koperníka	Koperník	k1gMnSc4	Koperník
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
jméno	jméno	k1gNnSc1	jméno
copernicium	copernicium	k1gNnSc1	copernicium
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
kopernicium	kopernicium	k1gNnSc1	kopernicium
<g/>
)	)	kIx)	)
a	a	k8xC	a
značku	značka	k1gFnSc4	značka
Cp	Cp	k1gFnSc2	Cp
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c6	na
konečné	konečný	k2eAgFnSc6d1	konečná
Cn	Cn	k1gFnSc6	Cn
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
Cp	Cp	k1gFnSc2	Cp
byl	být	k5eAaImAgInS	být
totiž	totiž	k8xC	totiž
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
latinským	latinský	k2eAgNnSc7d1	latinské
jménem	jméno	k1gNnSc7	jméno
cassiopeium	cassiopeium	k1gNnSc1	cassiopeium
<g/>
,	,	kIx,	,
navrženým	navržený	k2eAgMnSc7d1	navržený
pro	pro	k7c4	pro
prvek	prvek	k1gInSc4	prvek
dnes	dnes	k6eAd1	dnes
zvaný	zvaný	k2eAgInSc4d1	zvaný
lutecium	lutecium	k1gNnSc4	lutecium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Izotopy	izotop	k1gInPc4	izotop
==	==	k?	==
</s>
</p>
<p>
<s>
Doposud	doposud	k6eAd1	doposud
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
10	[number]	k4	10
následujících	následující	k2eAgInPc2d1	následující
izotopů	izotop	k1gInPc2	izotop
kopernicia	kopernicium	k1gNnSc2	kopernicium
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
izotopy	izotop	k1gInPc1	izotop
s	s	k7c7	s
nukleonovým	nukleonův	k2eAgNnSc7d1	nukleonův
číslem	číslo	k1gNnSc7	číslo
283	[number]	k4	283
a	a	k8xC	a
285	[number]	k4	285
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
2	[number]	k4	2
izomerních	izomerní	k2eAgFnPc6d1	izomerní
modifikacích	modifikace	k1gFnPc6	modifikace
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
poločasem	poločas	k1gInSc7	poločas
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
skupina	skupina	k1gFnSc1	skupina
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fyzika	fyzika	k1gFnSc1	fyzika
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kopernicium	kopernicium	k1gNnSc4	kopernicium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kopernicium	kopernicium	k1gNnSc4	kopernicium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
