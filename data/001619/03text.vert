<s>
Walther	Walthra	k1gFnPc2	Walthra
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Georg	Georg	k1gMnSc1	Georg
Bothe	Bothe	k1gFnSc1	Bothe
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Oranienburg	Oranienburg	k1gInSc1	Oranienburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
představovaly	představovat	k5eAaImAgFnP	představovat
významný	významný	k2eAgInSc4d1	významný
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
moderní	moderní	k2eAgFnSc2d1	moderní
nukleární	nukleární	k2eAgFnSc2d1	nukleární
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
Říšském	říšský	k2eAgInSc6d1	říšský
fyzikálně-technickém	fyzikálněechnický	k2eAgInSc6d1	fyzikálně-technický
ústavu	ústav	k1gInSc6	ústav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
po	po	k7c6	po
předchozím	předchozí	k2eAgInSc6d1	předchozí
jmenování	jmenování	k1gNnSc2	jmenování
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Gießenu	Gießeno	k1gNnSc6	Gießeno
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
rentgenovým	rentgenový	k2eAgNnSc7d1	rentgenové
zářením	záření	k1gNnSc7	záření
<g/>
,	,	kIx,	,
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
i	i	k8xC	i
umělou	umělý	k2eAgFnSc7d1	umělá
radioaktivitou	radioaktivita	k1gFnSc7	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
H.	H.	kA	H.
Becrerem	Becrer	k1gMnSc7	Becrer
objevil	objevit	k5eAaPmAgInS	objevit
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
identifikovat	identifikovat	k5eAaBmF	identifikovat
až	až	k9	až
J.	J.	kA	J.
Chadwickovi	Chadwicek	k1gMnSc6	Chadwicek
jako	jako	k8xS	jako
proud	proud	k1gInSc4	proud
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
Botheův	Botheův	k2eAgInSc4d1	Botheův
objev	objev	k1gInSc4	objev
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
koincidenční	koincidenční	k2eAgFnSc1d1	koincidenční
detekční	detekční	k2eAgFnSc1d1	detekční
metoda	metoda	k1gFnSc1	metoda
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
stanovit	stanovit	k5eAaPmF	stanovit
směr	směr	k1gInSc4	směr
jejich	jejich	k3xOp3gInSc2	jejich
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
odhalit	odhalit	k5eAaPmF	odhalit
jejich	jejich	k3xOp3gInSc4	jejich
případný	případný	k2eAgInSc4d1	případný
současný	současný	k2eAgInSc4d1	současný
vznik	vznik	k1gInSc4	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaImNgNnS	využívat
zejména	zejména	k9	zejména
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
M.	M.	kA	M.
Bornem	Borno	k1gNnSc7	Borno
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
