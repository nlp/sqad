<s>
Albert	Albert	k1gMnSc1	Albert
(	(	kIx(	(
<g/>
za	za	k7c4	za
alternativu	alternativa	k1gFnSc4	alternativa
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
považuje	považovat	k5eAaImIp3nS	považovat
Albrecht	Albrecht	k1gMnSc1	Albrecht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
Adalberaht	Adalberaht	k1gInSc1	Adalberaht
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vykládá	vykládat	k5eAaImIp3nS	vykládat
se	se	k3xPyFc4	se
jako	jako	k9	jako
adal	adanout	k5eAaPmAgInS	adanout
-	-	kIx~	-
vznešený	vznešený	k2eAgInSc1d1	vznešený
<g/>
,	,	kIx,	,
beraht	beraht	k2eAgInSc1d1	beraht
-	-	kIx~	-
zářící	zářící	k2eAgNnSc1d1	zářící
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
občanského	občanský	k2eAgInSc2d1	občanský
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
jmeniny	jmeniny	k1gFnPc4	jmeniny
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
Albi	Albi	k1gNnSc1	Albi
<g/>
,	,	kIx,	,
Albík	Albík	k1gInSc1	Albík
<g/>
,	,	kIx,	,
Albertík	Albertík	k1gInSc1	Albertík
<g/>
,	,	kIx,	,
Bert	Berta	k1gFnPc2	Berta
<g/>
,	,	kIx,	,
Bertík	Bertík	k1gInSc1	Bertík
<g/>
,	,	kIx,	,
Berťa	Berť	k2eAgFnSc1d1	Berť
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
-	-	kIx~	-
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
<g />
.	.	kIx.	.
</s>
<s>
trend	trend	k1gInSc1	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+3,7	+3,7	k4	+3,7
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
nárůstu	nárůst	k1gInSc6	nárůst
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Albert	Albert	k1gMnSc1	Albert
nebo	nebo	k8xC	nebo
Adalbert	Adalbert	k1gMnSc1	Adalbert
(	(	kIx(	(
<g/>
Adalbert	Adalbert	k1gMnSc1	Adalbert
je	být	k5eAaImIp3nS	být
protějšek	protějšek	k1gInSc4	protějšek
jména	jméno	k1gNnSc2	jméno
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
)	)	kIx)	)
Italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
<g/>
španělsky	španělsky	k6eAd1	španělsky
alberto	alberta	k1gFnSc5	alberta
Svatý	svatý	k2eAgMnSc1d1	svatý
Albert	Albert	k1gMnSc1	Albert
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gMnSc1	Camus
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Albert	Albert	k1gMnSc1	Albert
Arnold	Arnold	k1gMnSc1	Arnold
Gore	Gore	k1gFnSc1	Gore
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
Albert	Albert	k1gMnSc1	Albert
Hofmann	Hofmann	k1gMnSc1	Hofmann
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
chemik	chemik	k1gMnSc1	chemik
Albert	Albert	k1gMnSc1	Albert
Kesselring	Kesselring	k1gInSc4	Kesselring
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
maršál	maršál	k1gMnSc1	maršál
Albert	Albert	k1gMnSc1	Albert
Maysles	Maysles	k1gMnSc1	Maysles
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
direct	direct	k1gMnSc1	direct
cinema	cinema	k1gFnSc1	cinema
Alberto	Alberta	k1gFnSc5	Alberta
Moravia	Moravius	k1gMnSc4	Moravius
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Albert	Albert	k1gMnSc1	Albert
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1	Sasko-Kobursko-Gothajský
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
Albert	Albert	k1gMnSc1	Albert
Schweitzer	Schweitzer	k1gMnSc1	Schweitzer
<g/>
,	,	kIx,	,
německo-francouzský	německorancouzský	k2eAgMnSc1d1	německo-francouzský
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Albert	Albert	k1gMnSc1	Albert
Speer	Speer	k1gMnSc1	Speer
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
architekt	architekt	k1gMnSc1	architekt
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nacistické	nacistický	k2eAgFnSc2d1	nacistická
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ministr	ministr	k1gMnSc1	ministr
zbrojního	zbrojní	k2eAgInSc2d1	zbrojní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
Albert	Alberta	k1gFnPc2	Alberta
(	(	kIx(	(
<g/>
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
