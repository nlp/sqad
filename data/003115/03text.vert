<s>
Cumulus	Cumulus	k1gMnSc1	Cumulus
congestus	congestus	k1gMnSc1	congestus
je	být	k5eAaImIp3nS	být
oblak	oblak	k1gInSc1	oblak
vznikající	vznikající	k2eAgInSc1d1	vznikající
především	především	k6eAd1	především
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
při	při	k7c6	při
vzestupných	vzestupný	k2eAgInPc6d1	vzestupný
proudech	proud	k1gInPc6	proud
a	a	k8xC	a
vysokému	vysoký	k2eAgInSc3d1	vysoký
inventivnímu	inventivní	k2eAgInSc3d1	inventivní
charakteru	charakter	k1gInSc3	charakter
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
dalším	další	k2eAgNnSc7d1	další
stadiem	stadion	k1gNnSc7	stadion
je	být	k5eAaImIp3nS	být
cumulonimbus	cumulonimbus	k1gInSc1	cumulonimbus
(	(	kIx(	(
<g/>
bouřkový	bouřkový	k2eAgInSc1d1	bouřkový
oblak	oblak	k1gInSc1	oblak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
oblaky	oblak	k1gInPc1	oblak
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
dusném	dusný	k2eAgNnSc6d1	dusné
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cumulu	cumul	k1gInSc2	cumul
congestu	congest	k1gInSc2	congest
převážně	převážně	k6eAd1	převážně
nevypadávají	vypadávat	k5eNaImIp3nP	vypadávat
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
rysy	rys	k1gInPc1	rys
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
tmavá	tmavý	k2eAgFnSc1d1	tmavá
základna	základna	k1gFnSc1	základna
oblaku	oblak	k1gInSc2	oblak
a	a	k8xC	a
květákový	květákový	k2eAgInSc4d1	květákový
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Cumuly	Cumula	k1gFnPc1	Cumula
se	se	k3xPyFc4	se
rozpínají	rozpínat	k5eAaImIp3nP	rozpínat
a	a	k8xC	a
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
atmosférickém	atmosférický	k2eAgNnSc6d1	atmosférické
proudění	proudění	k1gNnSc6	proudění
a	a	k8xC	a
taky	taky	k6eAd1	taky
na	na	k7c4	na
invenci	invence	k1gFnSc4	invence
můžou	můžou	k?	můžou
a	a	k8xC	a
taky	taky	k6eAd1	taky
zpravidla	zpravidla	k6eAd1	zpravidla
přejdou	přejít	k5eAaPmIp3nP	přejít
do	do	k7c2	do
stádia	stádium	k1gNnSc2	stádium
cumulonimbus	cumulonimbus	k1gInSc1	cumulonimbus
<g/>
.	.	kIx.	.
</s>
