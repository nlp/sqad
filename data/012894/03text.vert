<p>
<s>
Myslbekova	Myslbekův	k2eAgFnSc1d1	Myslbekova
ulice	ulice	k1gFnSc1	ulice
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
a	a	k8xC	a
Střešovicích	Střešovice	k1gFnPc6	Střešovice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
spojuje	spojovat	k5eAaImIp3nS	spojovat
křižovatku	křižovatka	k1gFnSc4	křižovatka
ulic	ulice	k1gFnPc2	ulice
Bělohorská	bělohorský	k2eAgFnSc1d1	Bělohorská
a	a	k8xC	a
Dlabačov	Dlabačov	k1gInSc1	Dlabačov
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Patočkova	Patočkův	k2eAgNnSc2d1	Patočkovo
<g/>
.	.	kIx.	.
</s>
<s>
Nazvána	nazvat	k5eAaPmNgFnS	nazvat
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zakladatele	zakladatel	k1gMnSc2	zakladatel
novodobého	novodobý	k2eAgNnSc2d1	novodobé
českého	český	k2eAgNnSc2d1	české
sochařství	sochařství	k1gNnSc2	sochařství
Josefa	Josef	k1gMnSc2	Josef
Václava	Václav	k1gMnSc2	Václav
Myslbeka	Myslbeek	k1gMnSc2	Myslbeek
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnSc1	ulice
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
Hradčany	Hradčany	k1gInPc1	Hradčany
a	a	k8xC	a
Střešovice	Střešovice	k1gFnPc1	Střešovice
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
Strossmayerovo	Strossmayerův	k2eAgNnSc1d1	Strossmayerovo
náměstí	náměstí	k1gNnSc1	náměstí
–	–	k?	–
Hradčanská	hradčanský	k2eAgFnSc1d1	Hradčanská
–	–	k?	–
Malovanka	Malovanka	k1gFnSc1	Malovanka
<g/>
,	,	kIx,	,
nedávno	nedávno	k6eAd1	nedávno
byla	být	k5eAaImAgFnS	být
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
<g/>
.	.	kIx.	.
</s>
<s>
Ulice	ulice	k1gFnSc1	ulice
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
Pražského	pražský	k2eAgInSc2d1	pražský
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Myslbekova	Myslbekov	k1gInSc2	Myslbekov
-	-	kIx~	-
Prašný	prašný	k2eAgInSc1d1	prašný
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
stavba	stavba	k1gFnSc1	stavba
9515	[number]	k4	9515
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
firmy	firma	k1gFnPc1	firma
a	a	k8xC	a
instituce	instituce	k1gFnPc1	instituce
==	==	k?	==
</s>
</p>
<p>
<s>
potraviny	potravina	k1gFnPc4	potravina
Žabka	žabka	k1gFnSc1	žabka
-	-	kIx~	-
Myslbekova	Myslbekov	k1gInSc2	Myslbekov
1	[number]	k4	1
</s>
</p>
<p>
<s>
tlumočení	tlumočení	k1gNnSc1	tlumočení
a	a	k8xC	a
průvodci	průvodce	k1gMnPc1	průvodce
Viandanti	Viandant	k1gMnPc1	Viandant
Praghesi	Praghese	k1gFnSc4	Praghese
-	-	kIx~	-
Myslbekova	Myslbekov	k1gInSc2	Myslbekov
5	[number]	k4	5
</s>
</p>
<p>
<s>
Pavros	Pavrosa	k1gFnPc2	Pavrosa
<g/>
,	,	kIx,	,
NDT	NDT	kA	NDT
měření	měření	k1gNnSc4	měření
-	-	kIx~	-
Myslbekova	Myslbekov	k1gInSc2	Myslbekov
9	[number]	k4	9
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Myslbekova	Myslbekov	k1gInSc2	Myslbekov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
