<s>
Kaple	kaple	k1gFnSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
(	(	kIx(
<g/>
Svor	svor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kaple	kaple	k1gFnSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojiceve	Trojiceev	k1gFnSc2
Svoru	svor	k1gInSc2
Kaple	kaple	k1gFnSc2
ve	v	k7c6
SvoruMísto	SvoruMísta	k1gMnSc5
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Svor	svor	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
47	#num#	k4
<g/>
′	′	k?
<g/>
33,57	33,57	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
31,4	31,4	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
litoměřická	litoměřický	k2eAgFnSc1d1
Vikariát	vikariát	k1gInSc1
</s>
<s>
českolipský	českolipský	k2eAgInSc1d1
Farnost	farnost	k1gFnSc4
</s>
<s>
Cvikov	Cvikov	k1gInSc1
Status	status	k1gInSc1
</s>
<s>
kaple	kaple	k1gFnSc1
Užívání	užívání	k1gNnSc2
</s>
<s>
bližší	blízký	k2eAgFnPc4d2
informace	informace	k1gFnPc4
o	o	k7c6
bohoslužbách	bohoslužba	k1gFnPc6
Architektonický	architektonický	k2eAgInSc4d1
popis	popis	k1gInSc4
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc1
</s>
<s>
baroko	baroko	k1gNnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1788	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Adresa	adresa	k1gFnSc1
</s>
<s>
Svor	svor	k1gInSc1
Kód	kód	k1gInSc1
památky	památka	k1gFnSc2
</s>
<s>
26393	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
3328	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kaple	kaple	k1gFnSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
obci	obec	k1gFnSc6
Svor	svora	k1gFnPc2
nedaleko	nedaleko	k7c2
Nového	Nového	k2eAgInSc2d1
Boru	bor	k1gInSc2
v	v	k7c6
okrese	okres	k1gInSc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaple	kaple	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1788	#num#	k4
je	být	k5eAaImIp3nS
chráněna	chráněn	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Původně	původně	k6eAd1
srubová	srubový	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1745	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1788	#num#	k4
přestavěna	přestavět	k5eAaPmNgFnS
jako	jako	k9
zděná	zděný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
v	v	k7c6
pozdně	pozdně	k6eAd1
barokním	barokní	k2eAgInSc6d1
slohu	sloh	k1gInSc6
s	s	k7c7
věžičkou	věžička	k1gFnSc7
a	a	k8xC
mansardovou	mansardový	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Kaple	kaple	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
evidenci	evidence	k1gFnSc6
Římskokatolické	římskokatolický	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
v	v	k7c6
Cvikově	Cvikov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Bohoslužby	bohoslužba	k1gFnPc1
jsou	být	k5eAaImIp3nP
slouženy	sloužen	k2eAgFnPc1d1
zpravidla	zpravidla	k6eAd1
dvakrát	dvakrát	k6eAd1
za	za	k7c4
měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
řadě	řada	k1gFnSc6
turistických	turistický	k2eAgInPc2d1
průvodců	průvodce	k1gMnPc2
je	být	k5eAaImIp3nS
kaple	kaple	k1gFnSc1
uváděna	uvádět	k5eAaImNgFnS
jako	jako	k8xS,k8xC
kostel	kostel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
137566	#num#	k4
:	:	kIx,
Kaple	kaple	k1gFnPc4
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
↑	↑	k?
ŘEHÁČEK	Řeháček	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lužické	lužický	k2eAgFnPc4d1
a	a	k8xC
Žitavské	žitavský	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
:	:	kIx,
Kalendář	kalendář	k1gInSc1
Liberecka	Liberecko	k1gNnSc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Svor	svora	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
223	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Katalog	katalog	k1gInSc4
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
AD	ad	k7c4
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litoměřice	Litoměřice	k1gInPc4
<g/>
:	:	kIx,
Biskupství	biskupství	k1gNnSc3
litoměřické	litoměřický	k2eAgInPc1d1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1045	#num#	k4
Cvikov	Cvikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kaple	kaple	k1gFnSc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pořad	pořad	k1gInSc1
bohoslužeb	bohoslužba	k1gFnPc2
v	v	k7c6
kapli	kaple	k1gFnSc6
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
<g/>
,	,	kIx,
Svor	svor	k1gInSc1
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
biskupství	biskupství	k1gNnSc2
litoměřického	litoměřický	k2eAgNnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Web	web	k1gInSc1
Místopisy	místopis	k1gInPc1
</s>
<s>
Web	web	k1gInSc1
Severní	severní	k2eAgFnSc2d1
Čechy	Čech	k1gMnPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
