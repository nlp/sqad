<s>
Kaple	kaple	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
evidenci	evidence	k1gFnSc6
Římskokatolické	římskokatolický	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
v	v	k7c6
Cvikově	Cvikov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Bohoslužby	bohoslužba	k1gFnPc1
jsou	být	k5eAaImIp3nP
slouženy	sloužen	k2eAgFnPc1d1
zpravidla	zpravidla	k6eAd1
dvakrát	dvakrát	k6eAd1
za	za	k7c4
měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
</s>