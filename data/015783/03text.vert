<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Guam	Guam	k1gInSc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Bitva	bitva	k1gFnSc1
o	o	k7c4
Guam	Guam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
o	o	k7c4
Guam	Guam	k1gInSc4
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Vylodění	vylodění	k1gNnSc1
Japonců	Japonec	k1gMnPc2
na	na	k7c6
Guamu	Guam	k1gInSc6
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
až	až	k6eAd1
10	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1941	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Guam	Guam	k1gInSc1
<g/>
,	,	kIx,
Mariany	Mariana	k1gFnPc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
USA	USA	kA
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
George	George	k1gInSc1
J.	J.	kA
McMillin	McMillin	k1gInSc1
</s>
<s>
Tomitaro	Tomitara	k1gFnSc5
Horii	Horius	k1gMnPc7
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
Pozemní	pozemní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
:	:	kIx,
547	#num#	k4
Námořní	námořní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
:	:	kIx,
1	#num#	k4
minolovka	minolovka	k1gFnSc1
2	#num#	k4
hlídkové	hlídkový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
1	#num#	k4
nákladní	nákladní	k2eAgFnSc4d1
loď	loď	k1gFnSc4
</s>
<s>
Pozemní	pozemní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
:	:	kIx,
5	#num#	k4
900	#num#	k4
Námořní	námořní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
:	:	kIx,
4	#num#	k4
těžké	těžký	k2eAgInPc1d1
křížníky	křížník	k1gInPc1
4	#num#	k4
torpédoborce	torpédoborec	k1gInSc2
3	#num#	k4
dělové	dělový	k2eAgInPc4d1
čluny	člun	k1gInPc4
6	#num#	k4
stíhačů	stíhač	k1gMnPc2
ponorek	ponorka	k1gFnPc2
2	#num#	k4
minolovky	minolovka	k1gFnSc2
1	#num#	k4
minonoska	minonoska	k1gFnSc1
1	#num#	k4
nosič	nosič	k1gInSc4
hydroplánů	hydroplán	k1gInPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
17	#num#	k4
zabito	zabít	k5eAaPmNgNnS
<g/>
,	,	kIx,
</s>
<s>
35	#num#	k4
zraněných	zraněný	k2eAgMnPc2d1
<g/>
,	,	kIx,
</s>
<s>
406	#num#	k4
zajatých	zajatá	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
1	#num#	k4
minolovka	minolovka	k1gFnSc1
potopena	potopit	k5eAaPmNgFnS
<g/>
,	,	kIx,
</s>
<s>
1	#num#	k4
hlídkový	hlídkový	k2eAgInSc1d1
člun	člun	k1gInSc1
potopen	potopen	k2eAgInSc1d1
<g/>
,	,	kIx,
</s>
<s>
1	#num#	k4
hlídkový	hlídkový	k2eAgInSc1d1
člun	člun	k1gInSc1
zajat	zajat	k2eAgInSc1d1
<g/>
,	,	kIx,
</s>
<s>
1	#num#	k4
nákladní	nákladní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
poškozena	poškodit	k5eAaPmNgFnS
</s>
<s>
1	#num#	k4
zabit	zabit	k2eAgInSc1d1
<g/>
,	,	kIx,
</s>
<s>
6	#num#	k4
zraněno	zranit	k5eAaPmNgNnS
<g/>
,	,	kIx,
</s>
<s>
1	#num#	k4
letadlo	letadlo	k1gNnSc4
zničeno	zničen	k2eAgNnSc4d1
</s>
<s>
První	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
o	o	k7c4
Guam	Guam	k1gInSc4
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
bitev	bitva	k1gFnPc2
za	za	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
mezi	mezi	k7c7
Američany	Američan	k1gMnPc7
a	a	k8xC
Japonci	Japonec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odehrála	odehrát	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c6
Marianském	Marianský	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
Guam	Guam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začala	začít	k5eAaPmAgFnS
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
vyloděním	vylodění	k1gNnSc7
japonské	japonský	k2eAgFnSc2d1
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
a	a	k8xC
skončila	skončit	k5eAaPmAgFnS
o	o	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
dobytím	dobytí	k1gNnSc7
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgNnSc4
americké	americký	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
za	za	k7c2
války	válka	k1gFnSc2
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
do	do	k7c2
japonských	japonský	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
japonské	japonský	k2eAgFnSc6d1
okupaci	okupace	k1gFnSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
ostrov	ostrov	k1gInSc1
znovudobyt	znovudobyt	k1gInSc1
Američany	Američan	k1gMnPc4
během	během	k7c2
operace	operace	k1gFnSc2
Forager	Foragra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Battle	Battle	k1gFnSc2
of	of	k?
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
グ	グ	k?
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
年	年	k?
<g/>
)	)	kIx)
na	na	k7c6
japonské	japonský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bitva	bitva	k1gFnSc1
o	o	k7c4
Guam	Guam	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Malajsii	Malajsie	k1gFnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
•	•	k?
Boje	boj	k1gInPc4
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
Guadalcanal	Guadalcanal	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrova	ostrov	k1gInSc2
Savo	Savo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Šalomounů	Šalomoun	k1gMnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
mysu	mys	k1gInSc2
Esperance	Esperance	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrovů	ostrov	k1gInPc2
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Guadalcanalu	Guadalcanal	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tassafarongy	Tassafarong	k1gInPc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Rennellova	Rennellův	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Bismarckově	Bismarckův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Komandorských	Komandorský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Tarawu	Tarawus	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Operace	operace	k1gFnSc1
Forager	Forager	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Saipan	Saipan	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Peleliu	Pelelium	k1gNnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Leyte	Leyt	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Manilu	Manila	k1gFnSc4
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Iwodžimu	Iwodžimo	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Okinawu	Okinawa	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
