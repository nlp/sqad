<p>
<s>
Base	basa	k1gFnSc6	basa
<g/>
64	[number]	k4	64
je	být	k5eAaImIp3nS	být
kódování	kódování	k1gNnSc2	kódování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
binární	binární	k2eAgNnPc4d1	binární
data	datum	k1gNnPc4	datum
na	na	k7c6	na
posloupnosti	posloupnost	k1gFnSc6	posloupnost
tisknutelných	tisknutelný	k2eAgInPc2d1	tisknutelný
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přenos	přenos	k1gInSc4	přenos
binárních	binární	k2eAgNnPc2d1	binární
dat	datum	k1gNnPc2	datum
kanály	kanál	k1gInPc7	kanál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
pouze	pouze	k6eAd1	pouze
přenos	přenos	k1gInSc4	přenos
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
rozšíření	rozšíření	k1gNnSc6	rozšíření
MIME	mim	k1gMnSc5	mim
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
e-mailů	eail	k1gInPc2	e-mail
původně	původně	k6eAd1	původně
7	[number]	k4	7
<g/>
bitovým	bitový	k2eAgInSc7d1	bitový
poštovním	poštovní	k2eAgInSc7d1	poštovní
systémem	systém	k1gInSc7	systém
v	v	k7c6	v
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
v	v	k7c6	v
RFC	RFC	kA	RFC
4648	[number]	k4	4648
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Base	basa	k1gFnSc6	basa
<g/>
64	[number]	k4	64
kóduje	kódovat	k5eAaBmIp3nS	kódovat
vždy	vždy	k6eAd1	vždy
tři	tři	k4xCgInPc1	tři
oktety	oktet	k1gInPc1	oktet
binárních	binární	k2eAgNnPc2d1	binární
dat	datum	k1gNnPc2	datum
pomocí	pomocí	k7c2	pomocí
čtyř	čtyři	k4xCgMnPc2	čtyři
ASCII	ascii	kA	ascii
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kódování	kódování	k1gNnSc3	kódování
používá	používat	k5eAaImIp3nS	používat
64	[number]	k4	64
<g/>
prvkovou	prvkový	k2eAgFnSc4d1	prvková
sadu	sada	k1gFnSc4	sada
znaků	znak	k1gInPc2	znak
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
velkými	velká	k1gFnPc7	velká
i	i	k8xC	i
malými	malý	k2eAgNnPc7d1	malé
písmeny	písmeno	k1gNnPc7	písmeno
anglické	anglický	k2eAgFnSc2d1	anglická
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
číslicemi	číslice	k1gFnPc7	číslice
a	a	k8xC	a
znaky	znak	k1gInPc7	znak
plus	plus	k6eAd1	plus
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
+	+	kIx~	+
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
a	a	k8xC	a
lomítko	lomítko	k1gNnSc4	lomítko
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
/	/	kIx~	/
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
počet	počet	k1gInSc1	počet
oktetů	oktet	k1gInPc2	oktet
původních	původní	k2eAgNnPc2d1	původní
dat	datum	k1gNnPc2	datum
není	být	k5eNaImIp3nS	být
dělitelný	dělitelný	k2eAgInSc1d1	dělitelný
třemi	tři	k4xCgNnPc7	tři
<g/>
,	,	kIx,	,
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
zakódovaného	zakódovaný	k2eAgInSc2d1	zakódovaný
textu	text	k1gInSc2	text
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgNnPc1	dva
rovnítka	rovnítko	k1gNnPc1	rovnítko
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Leviathan	Leviathan	k1gMnSc1	Leviathan
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
Hobbes	Hobbes	k1gMnSc1	Hobbes
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Man	Man	k1gMnSc1	Man
is	is	k?	is
distinguished	distinguished	k1gInSc1	distinguished
<g/>
,	,	kIx,	,
not	nota	k1gFnPc2	nota
only	onla	k1gFnSc2	onla
by	by	k9	by
his	his	k1gNnSc4	his
reason	reason	k1gMnSc1	reason
<g/>
,	,	kIx,	,
but	but	k?	but
by	by	kYmCp3nS	by
this	this	k6eAd1	this
singular	singular	k1gInSc1	singular
passion	passion	k1gInSc1	passion
from	froma	k1gFnPc2	froma
other	othra	k1gFnPc2	othra
animals	animals	k1gInSc1	animals
<g/>
,	,	kIx,	,
which	which	k1gInSc1	which
is	is	k?	is
a	a	k8xC	a
lust	lust	k1gMnSc1	lust
of	of	k?	of
the	the	k?	the
mind	mind	k1gInSc4	mind
<g/>
,	,	kIx,	,
that	that	k1gInSc4	that
by	by	kYmCp3nS	by
a	a	k8xC	a
perseverance	perseverance	k1gFnSc1	perseverance
of	of	k?	of
delight	delight	k1gInSc1	delight
in	in	k?	in
the	the	k?	the
continued	continued	k1gInSc1	continued
and	and	k?	and
indefatigable	indefatigable	k6eAd1	indefatigable
generation	generation	k1gInSc1	generation
of	of	k?	of
knowledge	knowledge	k1gInSc1	knowledge
<g/>
,	,	kIx,	,
exceeds	exceeds	k1gInSc1	exceeds
the	the	k?	the
short	short	k1gInSc1	short
vehemence	vehemence	k1gFnSc1	vehemence
of	of	k?	of
any	any	k?	any
carnal	carnat	k5eAaPmAgMnS	carnat
pleasure	pleasur	k1gMnSc5	pleasur
<g/>
.	.	kIx.	.
<g/>
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
MIME	mim	k1gMnSc5	mim
Base	bas	k1gInSc6	bas
<g/>
64	[number]	k4	64
zakódován	zakódovat	k5eAaPmNgInS	zakódovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
TWFuIGlzIGRpc	TWFuIGlzIGRpc	k6eAd1	TWFuIGlzIGRpc
<g/>
3	[number]	k4	3
<g/>
Rpbmd	Rpbmda	k1gFnPc2	Rpbmda
<g/>
1	[number]	k4	1
<g/>
aXNoZWQsIG	aXNoZWQsIG	k?	aXNoZWQsIG
<g/>
5	[number]	k4	5
<g/>
vdCBvbmx	vdCBvbmx	k1gInSc1	vdCBvbmx
<g/>
5	[number]	k4	5
<g/>
IGJ	IGJ	kA	IGJ
<g/>
5	[number]	k4	5
<g/>
IGhpcyByZWFzb	IGhpcyByZWFzb	k1gInSc1	IGhpcyByZWFzb
<g/>
24	[number]	k4	24
<g/>
sIGJ	sIGJ	k?	sIGJ
<g/>
1	[number]	k4	1
<g/>
dCBieSB	dCBieSB	k?	dCBieSB
<g/>
0	[number]	k4	0
<g/>
aGlz	aGlza	k1gFnPc2	aGlza
</s>
</p>
<p>
<s>
IHNpbmd	IHNpbmd	k6eAd1	IHNpbmd
<g/>
1	[number]	k4	1
<g/>
bGFyIHBhc	bGFyIHBhc	k6eAd1	bGFyIHBhc
<g/>
3	[number]	k4	3
<g/>
Npb	Npb	k1gFnPc2	Npb
<g/>
24	[number]	k4	24
<g/>
gZnJvbSBvdGhlciBhbmltYWxzLCB	gZnJvbSBvdGhlciBhbmltYWxzLCB	k?	gZnJvbSBvdGhlciBhbmltYWxzLCB
<g/>
3	[number]	k4	3
<g/>
aGljaCBpcyBhIGx	aGljaCBpcyBhIGx	k1gInSc1	aGljaCBpcyBhIGx
<g/>
1	[number]	k4	1
<g/>
c	c	k0	c
<g/>
3	[number]	k4	3
<g/>
Qgb	Qgb	k1gFnSc6	Qgb
<g/>
2	[number]	k4	2
<g/>
Yg	Yg	k1gFnPc2	Yg
</s>
</p>
<p>
<s>
dGhlIG	dGhlIG	k?	dGhlIG
<g/>
1	[number]	k4	1
<g/>
pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY	pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY	k?	pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY
<g/>
2	[number]	k4	2
<g/>
Ugb	Ugb	k1gFnSc1	Ugb
<g/>
2	[number]	k4	2
<g/>
YgZGVsaWdodCBpbiB	YgZGVsaWdodCBpbiB	k1gFnSc1	YgZGVsaWdodCBpbiB
<g/>
0	[number]	k4	0
<g/>
aGUgY	aGUgY	k?	aGUgY
<g/>
29	[number]	k4	29
<g/>
udGlu	udGlat	k5eAaPmIp1nS	udGlat
</s>
</p>
<p>
<s>
dWVkIGFuZCBpbmRlZmF	dWVkIGFuZCBpbmRlZmF	k?	dWVkIGFuZCBpbmRlZmF
<g/>
0	[number]	k4	0
<g/>
aWdhYmxlIGdlbmVyYXRpb	aWdhYmxlIGdlbmVyYXRpb	k1gInSc1	aWdhYmxlIGdlbmVyYXRpb
<g/>
24	[number]	k4	24
<g/>
gb	gb	k?	gb
<g/>
2	[number]	k4	2
<g/>
Yga	Yga	k1gFnSc1	Yga
<g/>
25	[number]	k4	25
<g/>
vd	vd	k?	vd
<g/>
2	[number]	k4	2
<g/>
xlZGdlLCBleGNlZWRzIHRo	xlZGdlLCBleGNlZWRzIHRo	k6eAd1	xlZGdlLCBleGNlZWRzIHRo
</s>
</p>
<p>
<s>
ZSBzaG	ZSBzaG	k?	ZSBzaG
<g/>
9	[number]	k4	9
<g/>
ydCB	ydCB	k?	ydCB
<g/>
2	[number]	k4	2
<g/>
ZWhlbWVuY	ZWhlbWVuY	k1gFnSc1	ZWhlbWVuY
<g/>
2	[number]	k4	2
<g/>
Ugb	Ugb	k1gMnPc2	Ugb
<g/>
2	[number]	k4	2
<g/>
YgYW	YgYW	k1gMnPc2	YgYW
<g/>
55	[number]	k4	55
<g/>
IGNhcm	IGNhcmo	k1gNnPc2	IGNhcmo
<g/>
5	[number]	k4	5
<g/>
hbCBwbGVhc	hbCBwbGVhc	k6eAd1	hbCBwbGVhc
<g/>
3	[number]	k4	3
<g/>
VyZS	VyZS	k1gFnPc2	VyZS
<g/>
4	[number]	k4	4
<g/>
=	=	kIx~	=
</s>
</p>
<p>
<s>
==	==	k?	==
Algoritmus	algoritmus	k1gInSc1	algoritmus
base	bas	k1gInSc6	bas
<g/>
64	[number]	k4	64
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
příkladu	příklad	k1gInSc2	příklad
je	být	k5eAaImIp3nS	být
řetězec	řetězec	k1gInSc1	řetězec
Man	Man	k1gMnSc1	Man
zakódován	zakódovat	k5eAaPmNgMnS	zakódovat
do	do	k7c2	do
řetězce	řetězec	k1gInSc2	řetězec
TWFu	TWFus	k1gInSc2	TWFus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M	M	kA	M
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
,	,	kIx,	,
n	n	k0	n
jsou	být	k5eAaImIp3nP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
v	v	k7c6	v
ASCII	ascii	kA	ascii
jako	jako	k8xS	jako
čísla	číslo	k1gNnSc2	číslo
77	[number]	k4	77
<g/>
,	,	kIx,	,
97	[number]	k4	97
<g/>
,	,	kIx,	,
110	[number]	k4	110
dekadicky	dekadicky	k6eAd1	dekadicky
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
dvojkové	dvojkový	k2eAgFnSc6d1	dvojková
soustavě	soustava	k1gFnSc6	soustava
0	[number]	k4	0
<g/>
1001101	[number]	k4	1001101
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
1100001	[number]	k4	1100001
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
1101110	[number]	k4	1101110
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
3	[number]	k4	3
bajty	bajt	k1gInPc1	bajt
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
do	do	k7c2	do
24	[number]	k4	24
bitů	bit	k1gInPc2	bit
0	[number]	k4	0
<g/>
10011010110000101101110	[number]	k4	10011010110000101101110
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
6	[number]	k4	6
bitech	bit	k1gInPc6	bit
(	(	kIx(	(
<g/>
6	[number]	k4	6
bitů	bit	k1gInPc2	bit
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
64	[number]	k4	64
možných	možný	k2eAgInPc2d1	možný
stavů	stav	k1gInPc2	stav
<g/>
)	)	kIx)	)
a	a	k8xC	a
převedeny	převést	k5eAaPmNgInP	převést
do	do	k7c2	do
4	[number]	k4	4
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
24	[number]	k4	24
=	=	kIx~	=
6	[number]	k4	6
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
použita	použít	k5eAaPmNgNnP	použít
jako	jako	k8xC	jako
index	index	k1gInSc1	index
do	do	k7c2	do
tabulky	tabulka	k1gFnSc2	tabulka
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz	ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz	k1gInSc1	ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
<g/>
0	[number]	k4	0
<g/>
123456789	[number]	k4	123456789
<g/>
+	+	kIx~	+
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
znaku	znak	k1gInSc3	znak
"	"	kIx"	"
<g/>
A	a	k9	a
<g/>
"	"	kIx"	"
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
index	index	k1gInSc4	index
0	[number]	k4	0
a	a	k8xC	a
znaku	znak	k1gInSc3	znak
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
index	index	k1gInSc1	index
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
příkladu	příklad	k1gInSc6	příklad
s	s	k7c7	s
textovými	textový	k2eAgNnPc7d1	textové
daty	datum	k1gNnPc7	datum
<g/>
,	,	kIx,	,
kódování	kódování	k1gNnSc1	kódování
base	basa	k1gFnSc6	basa
<g/>
64	[number]	k4	64
převede	převést	k5eAaPmIp3nS	převést
každé	každý	k3xTgNnSc4	každý
3	[number]	k4	3
původní	původní	k2eAgInPc4d1	původní
bajty	bajt	k1gInPc4	bajt
(	(	kIx(	(
<g/>
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
případě	případ	k1gInSc6	případ
3	[number]	k4	3
znaky	znak	k1gInPc1	znak
ASCII	ascii	kA	ascii
<g/>
)	)	kIx)	)
na	na	k7c4	na
4	[number]	k4	4
kódované	kódovaný	k2eAgInPc4d1	kódovaný
znaky	znak	k1gInPc4	znak
ASCII	ascii	kA	ascii
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
počet	počet	k1gInSc1	počet
nevychází	vycházet	k5eNaImIp3nS	vycházet
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
trojice	trojice	k1gFnPc4	trojice
<g/>
,	,	kIx,	,
zakóduje	zakódovat	k5eAaPmIp3nS	zakódovat
se	se	k3xPyFc4	se
poslední	poslední	k2eAgFnSc1d1	poslední
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
přidají	přidat	k5eAaPmIp3nP	přidat
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jedno	jeden	k4xCgNnSc4	jeden
rovnítko	rovnítko	k1gNnSc4	rovnítko
<g/>
.	.	kIx.	.
</s>
<s>
Base	bas	k1gInSc6	bas
<g/>
64	[number]	k4	64
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
míře	míra	k1gFnSc6	míra
k	k	k7c3	k
zakódování	zakódování	k1gNnSc3	zakódování
binárních	binární	k2eAgNnPc2d1	binární
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
multimédií	multimédium	k1gNnPc2	multimédium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
base	basa	k1gFnSc6	basa
<g/>
64	[number]	k4	64
==	==	k?	==
</s>
</p>
<p>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
řetězec	řetězec	k1gInSc1	řetězec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tisknutelných	tisknutelný	k2eAgInPc2d1	tisknutelný
znaků	znak	k1gInPc2	znak
ASCII	ascii	kA	ascii
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
velikost	velikost	k1gFnSc1	velikost
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nemá	mít	k5eNaImIp3nS	mít
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kódování	kódování	k1gNnSc1	kódování
probíhá	probíhat	k5eAaImIp3nS	probíhat
binárně	binárně	k6eAd1	binárně
(	(	kIx(	(
<g/>
neřeší	řešit	k5eNaImIp3nS	řešit
se	se	k3xPyFc4	se
znakové	znakový	k2eAgNnSc1d1	znakové
kódování	kódování	k1gNnSc1	kódování
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Base	basa	k1gFnSc6	basa
<g/>
64	[number]	k4	64
je	být	k5eAaImIp3nS	být
binárně	binárně	k6eAd1	binárně
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
výsledného	výsledný	k2eAgInSc2d1	výsledný
řetězce	řetězec	k1gInSc2	řetězec
se	se	k3xPyFc4	se
obyčejně	obyčejně	k6eAd1	obyčejně
navýší	navýšit	k5eAaPmIp3nS	navýšit
o	o	k7c4	o
33	[number]	k4	33
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nástroje	nástroj	k1gInPc1	nástroj
On-line	Onin	k1gInSc5	On-lin
==	==	k?	==
</s>
</p>
<p>
<s>
Decode	Decod	k1gMnSc5	Decod
Base	basa	k1gFnSc3	basa
<g/>
64	[number]	k4	64
encoded	encoded	k1gInSc1	encoded
text	text	k1gInSc4	text
</s>
</p>
<p>
<s>
Base	basa	k1gFnSc3	basa
<g/>
64	[number]	k4	64
Decoder	Decodero	k1gNnPc2	Decodero
</s>
</p>
<p>
<s>
On-line	Onin	k1gInSc5	On-lin
Base	bas	k1gInSc6	bas
<g/>
64	[number]	k4	64
encoder	encoder	k1gMnSc1	encoder
and	and	k?	and
decoder	decoder	k1gMnSc1	decoder
</s>
</p>
<p>
<s>
On-line	Onin	k1gInSc5	On-lin
decoding	decoding	k1gInSc4	decoding
and	and	k?	and
encoding	encoding	k1gInSc1	encoding
text	text	k1gInSc1	text
and	and	k?	and
files	files	k1gInSc1	files
</s>
</p>
<p>
<s>
On-line	Onin	k1gInSc5	On-lin
Base	bas	k1gInSc5	bas
<g/>
64	[number]	k4	64
encode	encod	k1gInSc5	encod
/	/	kIx~	/
decode	decod	k1gInSc5	decod
</s>
</p>
<p>
<s>
Downloadable	Downloadable	k6eAd1	Downloadable
open	open	k1gMnSc1	open
source	source	k1gMnSc1	source
tool	toonout	k5eAaPmAgMnS	toonout
to	ten	k3xDgNnSc4	ten
encode	encod	k1gInSc5	encod
or	or	k?	or
decode	decod	k1gInSc5	decod
Base	bas	k1gInSc5	bas
<g/>
64	[number]	k4	64
on	on	k3xPp3gMnSc1	on
Unix	Unix	k1gInSc1	Unix
or	or	k?	or
Win	Win	k1gFnSc2	Win
<g/>
32	[number]	k4	32
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
