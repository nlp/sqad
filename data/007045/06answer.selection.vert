<s>
Voda	voda	k1gFnSc1	voda
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
ve	v	k7c6	v
vodních	vodní	k2eAgInPc6d1	vodní
tocích	tok	k1gInPc6	tok
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tajícího	tající	k2eAgInSc2d1	tající
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vsákla	vsáknout	k5eAaPmAgFnS	vsáknout
do	do	k7c2	do
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
znovu	znovu	k6eAd1	znovu
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nazývaném	nazývaný	k2eAgNnSc6d1	nazývané
pramen	pramen	k1gInSc1	pramen
<g/>
.	.	kIx.	.
</s>
