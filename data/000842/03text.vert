<s>
Derby	derby	k1gNnSc1	derby
je	být	k5eAaImIp3nS	být
klasických	klasický	k2eAgInPc2d1	klasický
rovinový	rovinový	k2eAgInSc4d1	rovinový
dostih	dostih	k1gInSc4	dostih
určený	určený	k2eAgInSc4d1	určený
tříletým	tříletý	k2eAgMnSc7d1	tříletý
koním	kůň	k1gMnPc3	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
testujících	testující	k2eAgFnPc2d1	testující
výkonnost	výkonnost	k1gFnSc4	výkonnost
anglických	anglický	k2eAgMnPc2d1	anglický
plnokrevníků	plnokrevník	k1gMnPc2	plnokrevník
v	v	k7c6	v
klasických	klasický	k2eAgInPc6d1	klasický
dostizích	dostih	k1gInPc6	dostih
je	být	k5eAaImIp3nS	být
nejprestižnějším	prestižní	k2eAgInSc7d3	nejprestižnější
rovinovým	rovinový	k2eAgInSc7d1	rovinový
dostihem	dostih	k1gInSc7	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
dostihu	dostih	k1gInSc2	dostih
je	být	k5eAaImIp3nS	být
1,5	[number]	k4	1,5
míle	míle	k1gFnSc2	míle
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2400	[number]	k4	2400
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostih	dostih	k1gInSc1	dostih
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
tříletým	tříletý	k2eAgInSc7d1	tříletý
hřebcům	hřebec	k1gMnPc3	hřebec
a	a	k8xC	a
klisnám	klisna	k1gFnPc3	klisna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
běžel	běžet	k5eAaImAgMnS	běžet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1780	[number]	k4	1780
na	na	k7c6	na
anglickém	anglický	k2eAgNnSc6d1	anglické
dostihovém	dostihový	k2eAgNnSc6d1	dostihové
závodišti	závodiště	k1gNnSc6	závodiště
v	v	k7c6	v
Epsomu	Epsom	k1gInSc6	Epsom
nedaleko	nedaleko	k7c2	nedaleko
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
dostihu	dostih	k1gInSc2	dostih
stáli	stát	k5eAaImAgMnP	stát
Edward	Edward	k1gMnSc1	Edward
Smith	Smith	k1gMnSc1	Smith
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
earl	earl	k1gMnSc1	earl
z	z	k7c2	z
Derby	derby	k1gNnSc2	derby
(	(	kIx(	(
<g/>
1752	[number]	k4	1752
-	-	kIx~	-
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
a	a	k8xC	a
sir	sir	k1gMnSc1	sir
Thomas	Thomas	k1gMnSc1	Thomas
Charles	Charles	k1gMnSc1	Charles
Bunbury	Bunbura	k1gFnSc2	Bunbura
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
baronet	baroneta	k1gFnPc2	baroneta
Bunbury	Bunbura	k1gFnSc2	Bunbura
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
-	-	kIx~	-
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Bunbury	Bunbura	k1gFnSc2	Bunbura
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
předsedou	předseda	k1gMnSc7	předseda
anglického	anglický	k2eAgNnSc2d1	anglické
Jockey	jockey	k1gMnSc1	jockey
Clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
byl	být	k5eAaImAgMnS	být
kůň	kůň	k1gMnSc1	kůň
Diomed	Diomed	k1gMnSc1	Diomed
(	(	kIx(	(
<g/>
majitel	majitel	k1gMnSc1	majitel
i	i	k8xC	i
chovatel	chovatel	k1gMnSc1	chovatel
sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Bunbury	Bunbura	k1gFnSc2	Bunbura
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
jezdcem	jezdec	k1gMnSc7	jezdec
Samem	Sam	k1gMnSc7	Sam
Arnullem	Arnull	k1gMnSc7	Arnull
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
každého	každý	k3xTgMnSc4	každý
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
50	[number]	k4	50
guineí	guineum	k1gNnPc2	guineum
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
výhra	výhra	k1gFnSc1	výhra
pak	pak	k6eAd1	pak
činila	činit	k5eAaImAgFnS	činit
1125	[number]	k4	1125
liber	libra	k1gFnPc2	libra
šterlinků	šterlink	k1gInPc2	šterlink
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
se	se	k3xPyFc4	se
běhalo	běhat	k5eAaImAgNnS	běhat
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
1	[number]	k4	1
míle	míle	k1gFnSc1	míle
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
na	na	k7c4	na
1,5	[number]	k4	1,5
míle	míle	k1gFnSc2	míle
(	(	kIx(	(
<g/>
něco	něco	k3yInSc1	něco
přes	přes	k7c4	přes
2400	[number]	k4	2400
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostih	dostih	k1gInSc1	dostih
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
britské	britský	k2eAgFnSc2d1	britská
klasické	klasický	k2eAgFnSc2d1	klasická
trojkoruny	trojkoruna	k1gFnSc2	trojkoruna
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
patří	patřit	k5eAaImIp3nS	patřit
ještě	ještě	k9	ještě
2000	[number]	k4	2000
guineí	guineum	k1gNnPc2	guineum
a	a	k8xC	a
St.	st.	kA	st.
Leger	Leger	k1gInSc1	Leger
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
epsomského	epsomský	k2eAgNnSc2d1	epsomský
derby	derby	k1gNnSc2	derby
je	být	k5eAaImIp3nS	být
The	The	k1gFnSc1	The
Derby	derby	k1gNnSc2	derby
Stakes	Stakesa	k1gFnPc2	Stakesa
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
veřejnosti	veřejnost	k1gFnSc3	veřejnost
je	být	k5eAaImIp3nS	být
dostih	dostih	k1gInSc1	dostih
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
Epsom	Epsom	k1gInSc1	Epsom
Derby	derby	k1gNnSc2	derby
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
názvu	název	k1gInSc6	název
dostihu	dostih	k1gInSc2	dostih
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
jako	jako	k8xS	jako
odborný	odborný	k2eAgInSc4d1	odborný
termín	termín	k1gInSc4	termín
převzaly	převzít	k5eAaPmAgInP	převzít
také	také	k9	také
další	další	k2eAgInPc1d1	další
sporty	sport	k1gInPc1	sport
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
hod	hod	k1gInSc1	hod
mincí	mince	k1gFnPc2	mince
-	-	kIx~	-
stačilo	stačit	k5eAaBmAgNnS	stačit
málo	málo	k6eAd1	málo
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
jsme	být	k5eAaImIp1nP	být
chodit	chodit	k5eAaImF	chodit
třeba	třeba	k6eAd1	třeba
na	na	k7c4	na
"	"	kIx"	"
<g/>
bunbury	bunbur	k1gInPc4	bunbur
Sparta	Sparta	k1gFnSc1	Sparta
-	-	kIx~	-
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
derby	derby	k1gNnSc2	derby
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tento	tento	k3xDgInSc1	tento
dostih	dostih	k1gInSc1	dostih
pořádá	pořádat	k5eAaImIp3nS	pořádat
kolem	kolem	k7c2	kolem
200	[number]	k4	200
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
začaly	začít	k5eAaPmAgInP	začít
derby	derby	k1gNnSc4	derby
pořádat	pořádat	k5eAaImF	pořádat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
následovalo	následovat	k5eAaImAgNnS	následovat
derby	derby	k1gNnSc1	derby
na	na	k7c6	na
území	území	k1gNnSc6	území
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americké	americký	k2eAgInPc1d1	americký
Kentucky	Kentuck	k1gInPc1	Kentuck
Derby	derby	k1gNnSc1	derby
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
běhá	běhat	k5eAaImIp3nS	běhat
několik	několik	k4yIc1	několik
dostihů	dostih	k1gInPc2	dostih
derby	derby	k1gNnPc2	derby
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konalo	konat	k5eAaImAgNnS	konat
derby	derby	k1gNnSc1	derby
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
na	na	k7c6	na
závodišti	závodiště	k1gNnSc6	závodiště
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Chuchli	Chuchle	k1gFnSc6	Chuchle
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
nyní	nyní	k6eAd1	nyní
název	název	k1gInSc4	název
České	český	k2eAgNnSc1d1	české
derby	derby	k1gNnSc1	derby
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
derby	derby	k1gNnSc4	derby
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
klusáci	klusák	k1gMnPc1	klusák
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
čtyřleté	čtyřletý	k2eAgMnPc4d1	čtyřletý
koně	kůň	k1gMnPc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Dostih	dostih	k1gInSc1	dostih
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
na	na	k7c6	na
závodišti	závodiště	k1gNnSc6	závodiště
v	v	k7c6	v
Epsomu	Epsom	k1gInSc6	Epsom
(	(	kIx(	(
<g/>
Epsom	Epsom	k1gInSc1	Epsom
Downs	Downsa	k1gFnPc2	Downsa
Racecourse	Racecourse	k1gFnSc2	Racecourse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1915	[number]	k4	1915
-	-	kIx~	-
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
první	první	k4xOgFnSc3	první
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
do	do	k7c2	do
Newmarketu	Newmarket	k1gInSc2	Newmarket
<g/>
.	.	kIx.	.
</s>
<s>
Dostih	dostih	k1gInSc1	dostih
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
vždy	vždy	k6eAd1	vždy
první	první	k4xOgFnSc4	první
červnovou	červnový	k2eAgFnSc4d1	červnová
středu	středa	k1gFnSc4	středa
<g/>
,	,	kIx,	,
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vždy	vždy	k6eAd1	vždy
první	první	k4xOgFnSc4	první
červnovou	červnový	k2eAgFnSc4d1	červnová
sobotu	sobota	k1gFnSc4	sobota
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
francouzský	francouzský	k2eAgInSc1d1	francouzský
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
porazil	porazit	k5eAaPmAgMnS	porazit
koně	kůň	k1gMnSc4	kůň
britské	britský	k2eAgNnSc1d1	Britské
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Gladiateur	Gladiateur	k1gMnSc1	Gladiateur
s	s	k7c7	s
Harrym	Harrym	k1gInSc1	Harrym
Grimshawem	Grimshaw	k1gInSc7	Grimshaw
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
této	tento	k3xDgFnSc2	tento
"	"	kIx"	"
<g/>
pomsty	pomsta	k1gFnSc2	pomsta
za	za	k7c2	za
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
"	"	kIx"	"
získal	získat	k5eAaPmAgMnS	získat
Gladiateur	Gladiateur	k1gMnSc1	Gladiateur
i	i	k8xC	i
ostatní	ostatní	k2eAgFnPc1d1	ostatní
části	část	k1gFnPc1	část
anglické	anglický	k2eAgFnSc2d1	anglická
Trojkoruny	Trojkoruna	k1gFnSc2	Trojkoruna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
první	první	k4xOgMnSc1	první
americký	americký	k2eAgMnSc1d1	americký
kůň	kůň	k1gMnSc1	kůň
-	-	kIx~	-
Iroquis	Iroquis	k1gInSc1	Iroquis
s	s	k7c7	s
Fredem	Fred	k1gMnSc7	Fred
Archerem	Archer	k1gMnSc7	Archer
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
derby	derby	k1gNnSc6	derby
získal	získat	k5eAaPmAgMnS	získat
Lester	Lester	k1gMnSc1	Lester
Piggott	Piggott	k1gMnSc1	Piggott
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
-	-	kIx~	-
1983	[number]	k4	1983
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
celkem	celkem	k6eAd1	celkem
devětkrát	devětkrát	k6eAd1	devětkrát
<g/>
.	.	kIx.	.
</s>
<s>
Epsom	Epsom	k1gInSc1	Epsom
Derby	derby	k1gNnSc1	derby
-	-	kIx~	-
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Kentucky	Kentucka	k1gFnSc2	Kentucka
Derby	derby	k1gNnSc1	derby
-	-	kIx~	-
USA	USA	kA	USA
Australian	Australian	k1gInSc1	Australian
Derby	derby	k1gNnSc1	derby
-	-	kIx~	-
Austrálie	Austrálie	k1gFnSc1	Austrálie
Irish	Irish	k1gInSc1	Irish
Derby	derby	k1gNnSc1	derby
-	-	kIx~	-
Irsko	Irsko	k1gNnSc1	Irsko
Prix	Prix	k1gInSc1	Prix
du	du	k?	du
Jockey	jockey	k1gMnSc1	jockey
Club	club	k1gInSc1	club
-	-	kIx~	-
Francie	Francie	k1gFnSc1	Francie
Derby	derby	k1gNnSc1	derby
Italiano	Italiana	k1gFnSc5	Italiana
-	-	kIx~	-
Itálie	Itálie	k1gFnSc1	Itálie
Feldstein	Feldstein	k1gMnSc1	Feldstein
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Derbyová	Derbyová	k1gFnSc1	Derbyová
jubilea	jubileum	k1gNnSc2	jubileum
v	v	k7c6	v
Epsomu	Epsom	k1gInSc6	Epsom
i	i	k8xC	i
v	v	k7c6	v
Chuchli	Chuchle	k1gFnSc6	Chuchle
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Paddock	Paddock	k1gInSc1	Paddock
Revue	revue	k1gFnSc2	revue
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
s.	s.	k?	s.
29	[number]	k4	29
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
Horáček	Horáček	k1gMnSc1	Horáček
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
:	:	kIx,	:
Království	království	k1gNnSc1	království
za	za	k7c4	za
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
derby	derby	k1gNnSc2	derby
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
