<s>
Kombajn	kombajn	k1gInSc1	kombajn
označuje	označovat	k5eAaImIp3nS	označovat
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
plnící	plnící	k2eAgInSc1d1	plnící
účel	účel	k1gInSc4	účel
kompletní	kompletní	k2eAgInSc4d1	kompletní
výrobní	výrobní	k2eAgFnSc4d1	výrobní
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
určený	určený	k2eAgMnSc1d1	určený
ke	k	k7c3	k
sklizni	sklizeň	k1gFnSc3	sklizeň
či	či	k8xC	či
těžbě	těžba	k1gFnSc3	těžba
<g/>
.	.	kIx.	.
</s>
