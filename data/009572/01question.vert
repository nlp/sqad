<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
kombinovanému	kombinovaný	k2eAgInSc3d1	kombinovaný
stroji	stroj	k1gInSc3	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
plní	plnit	k5eAaImIp3nS	plnit
účel	účel	k1gInSc4	účel
kompletní	kompletní	k2eAgInSc4d1	kompletní
výrobní	výrobní	k2eAgFnSc4d1	výrobní
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
určený	určený	k2eAgMnSc1d1	určený
ke	k	k7c3	k
sklizni	sklizeň	k1gFnSc3	sklizeň
či	či	k8xC	či
těžbě	těžba	k1gFnSc3	těžba
<g/>
?	?	kIx.	?
</s>
