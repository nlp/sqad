<p>
<s>
Kombajn	kombajn	k1gInSc1	kombajn
označuje	označovat	k5eAaImIp3nS	označovat
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
plnící	plnící	k2eAgInSc1d1	plnící
účel	účel	k1gInSc4	účel
kompletní	kompletní	k2eAgInSc4d1	kompletní
výrobní	výrobní	k2eAgFnSc4d1	výrobní
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
určený	určený	k2eAgMnSc1d1	určený
ke	k	k7c3	k
sklizni	sklizeň	k1gFnSc3	sklizeň
či	či	k8xC	či
těžbě	těžba	k1gFnSc3	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
významech	význam	k1gInPc6	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nejčastěji	často	k6eAd3	často
</s>
</p>
<p>
<s>
Obilní	obilní	k2eAgInSc1d1	obilní
kombajn	kombajn	k1gInSc1	kombajn
(	(	kIx(	(
<g/>
sklízecí	sklízecí	k2eAgFnSc1d1	sklízecí
mlátička	mlátička	k1gFnSc1	mlátička
<g/>
)	)	kIx)	)
<g/>
dále	daleko	k6eAd2	daleko
také	také	k9	také
</s>
</p>
<p>
<s>
Bramborový	bramborový	k2eAgInSc1d1	bramborový
kombajn	kombajn	k1gInSc1	kombajn
</s>
</p>
<p>
<s>
Chmelový	chmelový	k2eAgInSc1d1	chmelový
kombajn	kombajn	k1gInSc1	kombajn
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
statická	statický	k2eAgFnSc1d1	statická
česačka	česačka	k1gFnSc1	česačka
chmele	chmel	k1gInSc2	chmel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Řepný	řepný	k2eAgInSc1d1	řepný
kombajn	kombajn	k1gInSc1	kombajn
</s>
</p>
<p>
<s>
Podvodní	podvodní	k2eAgInSc1d1	podvodní
kombajn	kombajn	k1gInSc1	kombajn
</s>
</p>
<p>
<s>
Důlní	důlní	k2eAgInSc1d1	důlní
kombajn	kombajn	k1gInSc1	kombajn
</s>
</p>
<p>
<s>
Kombajn	kombajn	k1gInSc1	kombajn
na	na	k7c4	na
borůvky	borůvka	k1gFnPc4	borůvka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kombajn	kombajn	k1gInSc1	kombajn
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
