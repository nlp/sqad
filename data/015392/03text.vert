<s>
HIV	HIV	kA
</s>
<s>
HIV	HIV	kA
HIV	HIV	kA
pod	pod	k7c7
elektronovým	elektronový	k2eAgInSc7d1
mikroskopem	mikroskop	k1gInSc7
Stylizovaná	stylizovaný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
HIV	HIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baltimorova	Baltimorův	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
virů	vir	k1gInPc2
Skupina	skupina	k1gFnSc1
</s>
<s>
VI	VI	kA
(	(	kIx(
<g/>
ssRNA	ssRNA	k?
viry	vir	k1gInPc7
s	s	k7c7
reverzní	reverzní	k2eAgFnSc7d1
transkriptázou	transkriptáza	k1gFnSc7
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Čeleď	čeleď	k1gFnSc1
</s>
<s>
retroviry	retrovir	k1gInPc1
(	(	kIx(
<g/>
Retroviridae	Retroviridae	k1gFnSc1
<g/>
)	)	kIx)
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
Orthoretrovirinae	Orthoretrovirinae	k6eAd1
Rod	rod	k1gInSc1
</s>
<s>
Lentivirus	Lentivirus	k1gMnSc1
Druh	druh	k1gMnSc1
</s>
<s>
HIV-	HIV-	k?
<g/>
1	#num#	k4
<g/>
;	;	kIx,
HIV-2	HIV-2	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
HIV	HIV	kA
(	(	kIx(
<g/>
z	z	k7c2
angl.	angl.	k?
Human	Human	k1gMnSc1
Immunodeficiency	Immunodeficienca	k1gFnSc2
Virus	virus	k1gInSc1
<g/>
,	,	kIx,
virus	virus	k1gInSc1
lidské	lidský	k2eAgFnSc2d1
imunitní	imunitní	k2eAgFnSc2d1
nedostatečnosti	nedostatečnost	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obalený	obalený	k2eAgInSc1d1
RNA	RNA	kA
virus	virus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
náleží	náležet	k5eAaImIp3nS
mezi	mezi	k7c7
retroviry	retrovir	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retroviry	Retrovir	k1gMnPc7
je	být	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
virů	vir	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
schopnost	schopnost	k1gFnSc4
vytvořit	vytvořit	k5eAaPmF
podle	podle	k7c2
své	svůj	k3xOyFgFnSc2
RNA	RNA	kA
molekulu	molekula	k1gFnSc4
DNA	DNA	kA
a	a	k8xC
tu	tu	k6eAd1
vložit	vložit	k5eAaPmF
do	do	k7c2
genomu	genom	k1gInSc2
hostitelské	hostitelský	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
způsobovat	způsobovat	k5eAaImF
nemoc	nemoc	k1gFnSc4
AIDS	AIDS	kA
(	(	kIx(
<g/>
z	z	k7c2
angl.	angl.	k?
Acquired	Acquired	k1gInSc1
Immune	Immun	k1gInSc5
Deficiency	Deficiency	k1gInPc1
Syndrome	syndrom	k1gInSc5
<g/>
,	,	kIx,
též	též	k9
Acquired	Acquired	k1gInSc1
Immunodeficiency	Immunodeficienca	k1gMnSc2
Syndrome	syndrom	k1gInSc5
<g/>
)	)	kIx)
čili	čili	k8xC
syndrom	syndrom	k1gInSc1
získané	získaný	k2eAgFnSc2d1
imunitní	imunitní	k2eAgFnSc2d1
nedostatečnosti	nedostatečnost	k1gFnSc2
<g/>
,	,	kIx,
též	též	k9
syndrom	syndrom	k1gInSc1
získaného	získaný	k2eAgNnSc2d1
selhání	selhání	k1gNnSc2
imunity	imunita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
viru	vir	k1gInSc2
</s>
<s>
Částice	částice	k1gFnPc1
HIV	HIV	kA
sestávají	sestávat	k5eAaImIp3nP
ze	z	k7c2
dvou	dva	k4xCgFnPc2
molekul	molekula	k1gFnPc2
pozitivní	pozitivní	k2eAgFnSc2d1
jednořetězcové	jednořetězcový	k2eAgFnSc2d1
RNA	RNA	kA
(	(	kIx(
<g/>
+	+	kIx~
<g/>
ssRNA	ssRNA	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
enzymu	enzym	k1gInSc2
reverzní	reverzní	k2eAgFnSc2d1
transkriptázy	transkriptáza	k1gFnSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
zodpovědný	zodpovědný	k2eAgMnSc1d1
za	za	k7c4
přepis	přepis	k1gInSc4
RNA	RNA	kA
do	do	k7c2
DNA	DNA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
enzymu	enzym	k1gInSc3
integrázy	integráza	k1gFnSc2
(	(	kIx(
<g/>
zodpovídá	zodpovídat	k5eAaPmIp3nS,k5eAaImIp3nS
za	za	k7c4
začlenění	začlenění	k1gNnSc4
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgNnSc4d1
DNA	dna	k1gFnSc1
do	do	k7c2
DNA	dno	k1gNnSc2
buňky	buňka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kapsidy	kapsida	k1gFnPc4
a	a	k8xC
membrány	membrána	k1gFnPc4
z	z	k7c2
dříve	dříve	k6eAd2
infikované	infikovaný	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
obohacené	obohacený	k2eAgFnPc1d1
o	o	k7c4
glykoproteiny	glykoproteina	k1gFnPc4
gp	gp	k?
<g/>
120	#num#	k4
a	a	k8xC
gp	gp	k?
<g/>
41	#num#	k4
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
identifikaci	identifikace	k1gFnSc3
vhodné	vhodný	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
a	a	k8xC
usnadnění	usnadnění	k1gNnSc4
průniku	průnik	k1gInSc2
do	do	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Způsoby	způsob	k1gInPc1
přenosu	přenos	k1gInSc2
a	a	k8xC
prevence	prevence	k1gFnSc2
</s>
<s>
HIV	HIV	kA
se	se	k3xPyFc4
přenáší	přenášet	k5eAaImIp3nS
především	především	k6eAd1
krví	krev	k1gFnPc2
<g/>
,	,	kIx,
některými	některý	k3yIgFnPc7
sexuálními	sexuální	k2eAgFnPc7d1
aktivitami	aktivita	k1gFnPc7
(	(	kIx(
<g/>
zejména	zejména	k9
nechráněným	chráněný	k2eNgInSc7d1
pohlavním	pohlavní	k2eAgInSc7d1
stykem	styk	k1gInSc7
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
matky	matka	k1gFnSc2
na	na	k7c4
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžný	běžný	k2eAgInSc1d1
kontakt	kontakt	k1gInSc1
s	s	k7c7
nakaženou	nakažený	k2eAgFnSc7d1
osobou	osoba	k1gFnSc7
nepředstavuje	představovat	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc1
riziko	riziko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Virus	virus	k1gInSc1
je	být	k5eAaImIp3nS
obsažen	obsáhnout	k5eAaPmNgInS
v	v	k7c6
tělních	tělní	k2eAgFnPc6d1
tekutinách	tekutina	k1gFnPc6
nakažené	nakažený	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontaktem	kontakt	k1gInSc7
tělní	tělní	k2eAgFnSc2d1
tekutiny	tekutina	k1gFnSc2
obsahující	obsahující	k2eAgInSc1d1
virus	virus	k1gInSc1
se	s	k7c7
sliznicí	sliznice	k1gFnSc7
nebo	nebo	k8xC
otevřenou	otevřený	k2eAgFnSc7d1
ranou	rána	k1gFnSc7
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
přenosu	přenos	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejrizikovější	rizikový	k2eAgFnPc4d3
tělní	tělní	k2eAgFnPc4d1
tekutiny	tekutina	k1gFnPc4
patří	patřit	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
krev	krev	k1gFnSc1
včetně	včetně	k7c2
krve	krev	k1gFnSc2
menstruační	menstruační	k2eAgFnSc2d1
a	a	k8xC
některých	některý	k3yIgInPc2
krevních	krevní	k2eAgInPc2d1
derivátů	derivát	k1gInPc2
(	(	kIx(
<g/>
představuje	představovat	k5eAaImIp3nS
největší	veliký	k2eAgNnSc4d3
riziko	riziko	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
sperma	sperma	k1gNnSc1
</s>
<s>
poševní	poševní	k2eAgInSc1d1
sekret	sekret	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Virus	virus	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
nebezpečných	bezpečný	k2eNgFnPc6d1
koncentracích	koncentrace	k1gFnPc6
nachází	nacházet	k5eAaImIp3nS
i	i	k9
v	v	k7c6
mozkomíšní	mozkomíšní	k2eAgFnSc6d1
tekutině	tekutina	k1gFnSc6
a	a	k8xC
mateřském	mateřský	k2eAgNnSc6d1
mléku	mléko	k1gNnSc6
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
vyskytovat	vyskytovat	k5eAaImF
v	v	k7c6
preejakulační	preejakulační	k2eAgFnSc6d1
tekutině	tekutina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nízkých	nízký	k2eAgFnPc6d1
koncentracích	koncentrace	k1gFnPc6
byl	být	k5eAaImAgInS
nalezen	nalézt	k5eAaBmNgInS,k5eAaPmNgInS
i	i	k9
ve	v	k7c6
slinách	slina	k1gFnPc6
<g/>
,	,	kIx,
slzách	slza	k1gFnPc6
a	a	k8xC
moči	moč	k1gFnSc6
–	–	k?
koncentrace	koncentrace	k1gFnSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
však	však	k9
natolik	natolik	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nepředstavuje	představovat	k5eNaImIp3nS
reálné	reálný	k2eAgNnSc1d1
riziko	riziko	k1gNnSc1
nákazy	nákaza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Riziko	riziko	k1gNnSc1
však	však	k9
významně	významně	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc1
tekutiny	tekutina	k1gFnPc1
smíšeny	smíšen	k2eAgFnPc1d1
kvůli	kvůli	k7c3
zranění	zranění	k1gNnSc3
s	s	k7c7
krví	krev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
potu	pot	k1gInSc6
virus	virus	k1gInSc1
nalezen	nalezen	k2eAgMnSc1d1
nebyl	být	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Vědomý	vědomý	k2eAgInSc1d1
přenos	přenos	k1gInSc1
viru	vir	k1gInSc2
na	na	k7c4
druhé	druhý	k4xOgFnPc4
osoby	osoba	k1gFnPc4
bez	bez	k7c2
jejich	jejich	k3xOp3gNnSc2
vědomí	vědomí	k1gNnSc2
je	být	k5eAaImIp3nS
ve	v	k7c6
většině	většina	k1gFnSc6
zemí	zem	k1gFnPc2
trestným	trestný	k2eAgInSc7d1
činem	čin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
postihován	postihován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
těžké	těžký	k2eAgNnSc1d1
ublížení	ublížení	k1gNnSc1
na	na	k7c4
zdraví	zdraví	k1gNnSc4
a	a	k8xC
šíření	šíření	k1gNnSc4
nakažlivé	nakažlivý	k2eAgFnSc2d1
lidské	lidský	k2eAgFnSc2d1
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přenos	přenos	k1gInSc1
krví	krvit	k5eAaImIp3nS
</s>
<s>
Krev	krev	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
přenosu	přenos	k1gInSc2
nejnebezpečnější	bezpečný	k2eNgFnSc7d3
tekutinou	tekutina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rizikové	rizikový	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
použité	použitý	k2eAgFnPc1d1
injekční	injekční	k2eAgFnPc1d1
jehly	jehla	k1gFnPc1
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
sdílené	sdílený	k2eAgFnPc1d1
mezi	mezi	k7c4
narkomany	narkoman	k1gMnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
opakovaně	opakovaně	k6eAd1
používané	používaný	k2eAgNnSc1d1
v	v	k7c6
zemích	zem	k1gFnPc6
se	s	k7c7
zaostalým	zaostalý	k2eAgNnSc7d1
zdravotnictvím	zdravotnictví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledné	důsledný	k2eAgNnSc1d1
používání	používání	k1gNnSc4
jednorázových	jednorázový	k2eAgFnPc2d1
jehel	jehla	k1gFnPc2
je	být	k5eAaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
prevence	prevence	k1gFnSc2
nutností	nutnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenos	přenos	k1gInSc4
krevní	krevní	k2eAgFnSc7d1
transfúzí	transfúze	k1gFnSc7
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
díky	díky	k7c3
testování	testování	k1gNnSc3
krve	krev	k1gFnSc2
vzácný	vzácný	k2eAgInSc4d1
<g/>
,	,	kIx,
stále	stále	k6eAd1
k	k	k7c3
němu	on	k3xPp3gNnSc3
však	však	k9
občas	občas	k6eAd1
dochází	docházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Riziko	riziko	k1gNnSc1
představují	představovat	k5eAaImIp3nP
i	i	k9
orgánové	orgánový	k2eAgFnPc4d1
transplantace	transplantace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkému	velký	k2eAgNnSc3d1
riziku	riziko	k1gNnSc3
je	být	k5eAaImIp3nS
vystaven	vystavit	k5eAaPmNgInS
zdravotnický	zdravotnický	k2eAgInSc1d1
personál	personál	k1gInSc1
manipulující	manipulující	k2eAgInSc1d1
s	s	k7c7
krví	krev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Hmyzí	hmyzí	k2eAgNnSc4d1
kousnutí	kousnutí	k1gNnSc4
riziko	riziko	k1gNnSc1
přenosu	přenos	k1gInSc2
nepředstavuje	představovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Sexuální	sexuální	k2eAgInSc1d1
přenos	přenos	k1gInSc1
</s>
<s>
Sexuální	sexuální	k2eAgInSc1d1
přenos	přenos	k1gInSc1
byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
z	z	k7c2
ženy	žena	k1gFnSc2
na	na	k7c4
muže	muž	k1gMnSc4
<g/>
,	,	kIx,
muže	muž	k1gMnSc2
na	na	k7c4
ženu	žena	k1gFnSc4
<g/>
,	,	kIx,
muže	muž	k1gMnSc2
na	na	k7c4
muže	muž	k1gMnPc4
i	i	k8xC
ženy	žena	k1gFnPc4
na	na	k7c4
ženu	žena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
přenosu	přenos	k1gInSc3
může	moct	k5eAaImIp3nS
docházet	docházet	k5eAaImF
při	při	k7c6
análním	anální	k2eAgMnSc6d1
<g/>
,	,	kIx,
vaginálním	vaginální	k2eAgMnSc6d1
i	i	k8xC
orálním	orální	k2eAgInSc6d1
styku	styk	k1gInSc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
jakékoliv	jakýkoliv	k3yIgFnSc3
jiné	jiný	k2eAgFnSc3d1
praktice	praktika	k1gFnSc3
<g/>
,	,	kIx,
během	během	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
styku	styk	k1gInSc3
sliznic	sliznice	k1gFnPc2
se	s	k7c7
sexuálními	sexuální	k2eAgInPc7d1
sekrety	sekret	k1gInPc7
či	či	k8xC
krví	krev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anální	anální	k2eAgInSc4d1
styk	styk	k1gInSc4
je	být	k5eAaImIp3nS
významně	významně	k6eAd1
nebezpečnější	bezpečný	k2eNgInSc4d2
než	než	k8xS
vaginální	vaginální	k2eAgInSc4d1
<g/>
,	,	kIx,
orální	orální	k2eAgInSc4d1
styk	styk	k1gInSc4
je	být	k5eAaImIp3nS
nebezpečný	bezpečný	k2eNgMnSc1d1
nejméně	málo	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vaginální	vaginální	k2eAgInSc1d1
styk	styk	k1gInSc1
představuje	představovat	k5eAaImIp3nS
vyšší	vysoký	k2eAgNnSc4d2
riziko	riziko	k1gNnSc4
přenosu	přenos	k1gInSc2
z	z	k7c2
muže	muž	k1gMnSc2
na	na	k7c4
ženu	žena	k1gFnSc4
než	než	k8xS
naopak	naopak	k6eAd1
(	(	kIx(
<g/>
žena	žena	k1gFnSc1
je	být	k5eAaImIp3nS
vystavena	vystavit	k5eAaPmNgFnS
většímu	veliký	k2eAgNnSc3d2
množství	množství	k1gNnSc3
tekutin	tekutina	k1gFnPc2
partnera	partner	k1gMnSc4
než	než	k8xS
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
sperma	sperma	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
koncentrace	koncentrace	k1gFnSc1
viru	vir	k1gInSc2
než	než	k8xS
vaginální	vaginální	k2eAgInSc1d1
sekret	sekret	k1gInSc1
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
je	být	k5eAaImIp3nS
tekutinám	tekutina	k1gFnPc3
vystavena	vystavit	k5eAaPmNgFnS
déle	dlouho	k6eAd2
a	a	k8xC
větší	veliký	k2eAgFnSc7d2
plochou	plocha	k1gFnSc7
sliznice	sliznice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
I	i	k9
v	v	k7c6
případě	případ	k1gInSc6
análního	anální	k2eAgInSc2d1
styku	styk	k1gInSc2
je	být	k5eAaImIp3nS
většímu	veliký	k2eAgNnSc3d2
riziku	riziko	k1gNnSc3
vystaven	vystavit	k5eAaPmNgInS
příjemce	příjemka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1
přenosu	přenos	k1gInSc2
HIV	HIV	kA
infekce	infekce	k1gFnSc2
při	při	k7c6
jednotlivém	jednotlivý	k2eAgInSc6d1
nechráněném	chráněný	k2eNgInSc6d1
sexuálním	sexuální	k2eAgInSc6d1
styku	styk	k1gInSc6
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
řadě	řada	k1gFnSc6
faktorů	faktor	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
styku	styk	k1gInSc2
<g/>
,	,	kIx,
aktuální	aktuální	k2eAgNnSc1d1
množství	množství	k1gNnSc1
viru	vir	k1gInSc2
v	v	k7c6
nakaženém	nakažený	k2eAgMnSc6d1
partnerovi	partner	k1gMnSc6
<g/>
,	,	kIx,
současná	současný	k2eAgFnSc1d1
nákaza	nákaza	k1gFnSc1
další	další	k2eAgFnSc1d1
pohlavní	pohlavní	k2eAgFnSc7d1
chorobou	choroba	k1gFnSc7
(	(	kIx(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS
riziko	riziko	k1gNnSc1
přenosu	přenos	k1gInSc2
<g/>
)	)	kIx)
atd.	atd.	kA
Pohlavní	pohlavní	k2eAgInSc1d1
styk	styk	k1gInSc4
během	během	k7c2
menstruace	menstruace	k1gFnSc2
je	být	k5eAaImIp3nS
vzhledem	vzhledem	k7c3
k	k	k7c3
přítomnosti	přítomnost	k1gFnSc3
krve	krev	k1gFnSc2
rizikovější	rizikový	k2eAgFnSc2d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
při	při	k7c6
vaginálním	vaginální	k2eAgInSc6d1
styku	styk	k1gInSc6
je	být	k5eAaImIp3nS
pravděpodobnost	pravděpodobnost	k1gFnSc1
přenosu	přenos	k1gInSc2
z	z	k7c2
muže	muž	k1gMnSc2
na	na	k7c4
ženu	žena	k1gFnSc4
jedna	jeden	k4xCgFnSc1
ku	k	k7c3
několika	několik	k4yIc3
stům	sto	k4xCgNnPc3
až	až	k9
tisícům	tisíc	k4xCgInPc3
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
análního	anální	k2eAgInSc2d1
styku	styk	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
až	až	k9
jedna	jeden	k4xCgFnSc1
ku	k	k7c3
několika	několik	k4yIc3
málo	málo	k4c1
desítkám	desítka	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Líbání	líbání	k1gNnSc1
rizikové	rizikový	k2eAgNnSc1d1
není	být	k5eNaImIp3nS
za	za	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
ústa	ústa	k1gNnPc1
nejsou	být	k5eNaImIp3nP
poraněná	poraněný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Riziko	riziko	k1gNnSc1
nákazy	nákaza	k1gFnSc2
zvyšuje	zvyšovat	k5eAaImIp3nS
časté	častý	k2eAgNnSc1d1
střídání	střídání	k1gNnSc1
partnerů	partner	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnost	možnost	k1gFnSc1
nakažení	nakažení	k1gNnSc2
významně	významně	k6eAd1
snižuje	snižovat	k5eAaImIp3nS
užívání	užívání	k1gNnSc1
kondomu	kondom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Waller	Waller	k1gInSc4
a	a	k8xC
Davis	Davis	k1gInSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
metaanalýze	metaanalýza	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
ukázali	ukázat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
podle	podle	k7c2
dostupných	dostupný	k2eAgInPc2d1
údajů	údaj	k1gInPc2
užívání	užívání	k1gNnSc2
kondomu	kondom	k1gInSc2
snižuje	snižovat	k5eAaImIp3nS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
nepoužíváním	nepoužívání	k1gNnSc7
kondomu	kondom	k1gInSc2
dlouhodobé	dlouhodobý	k2eAgNnSc1d1
riziko	riziko	k1gNnSc1
sérokonverze	sérokonverze	k1gFnSc2
o	o	k7c4
80	#num#	k4
%	%	kIx~
<g/>
;	;	kIx,
autoři	autor	k1gMnPc1
dodávají	dodávat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
použité	použitý	k2eAgInPc1d1
prameny	pramen	k1gInPc1
nehodnotily	hodnotit	k5eNaImAgInP
správnost	správnost	k1gFnSc4
použití	použití	k1gNnSc2
kondomů	kondom	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgFnPc4d1
formy	forma	k1gFnPc4
antikoncepce	antikoncepce	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
hormonální	hormonální	k2eAgInPc1d1
<g/>
)	)	kIx)
při	při	k7c6
styku	styk	k1gInSc6
ochranu	ochrana	k1gFnSc4
před	před	k7c7
nákazou	nákaza	k1gFnSc7
neposkytují	poskytovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nechráněném	chráněný	k2eNgInSc6d1
styku	styk	k1gInSc6
s	s	k7c7
neznámým	známý	k2eNgMnSc7d1
partnerem	partner	k1gMnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
dostatečném	dostatečný	k2eAgInSc6d1
časovém	časový	k2eAgInSc6d1
odstupu	odstup	k1gInSc6
nutném	nutný	k2eAgInSc6d1
pro	pro	k7c4
správnou	správný	k2eAgFnSc4d1
diagnózu	diagnóza	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
u	u	k7c2
lékaře	lékař	k1gMnSc2
podstoupit	podstoupit	k5eAaPmF
krevní	krevní	k2eAgInPc4d1
testy	test	k1gInPc4
na	na	k7c4
přítomnost	přítomnost	k1gFnSc4
protilátek	protilátka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nákaze	nákaz	k1gInSc6
tak	tak	k8xC,k8xS
již	již	k6eAd1
samozřejmě	samozřejmě	k6eAd1
nelze	lze	k6eNd1
zabránit	zabránit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
lze	lze	k6eAd1
zabránit	zabránit	k5eAaPmF
nákaze	nákaza	k1gFnSc3
dalších	další	k2eAgMnPc2d1
partnerů	partner	k1gMnPc2
a	a	k8xC
včasnou	včasný	k2eAgFnSc7d1
léčbou	léčba	k1gFnSc7
zlepšit	zlepšit	k5eAaPmF
prognózu	prognóza	k1gFnSc4
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přenos	přenos	k1gInSc1
z	z	k7c2
matky	matka	k1gFnSc2
na	na	k7c4
dítě	dítě	k1gNnSc4
</s>
<s>
K	k	k7c3
přenosu	přenos	k1gInSc3
HIV	HIV	kA
z	z	k7c2
matky	matka	k1gFnSc2
na	na	k7c4
dítě	dítě	k1gNnSc4
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
jak	jak	k6eAd1
během	během	k7c2
vlastního	vlastní	k2eAgNnSc2d1
těhotenství	těhotenství	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
během	během	k7c2
porodu	porod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
přenosu	přenos	k1gInSc3
tak	tak	k6eAd1
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
15	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
%	%	kIx~
případů	případ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhodnou	vhodný	k2eAgFnSc7d1
antivirovou	antivirový	k2eAgFnSc7d1
léčbou	léčba	k1gFnSc7
během	během	k7c2
těhotenství	těhotenství	k1gNnSc2
a	a	k8xC
porodem	porod	k1gInSc7
císařským	císařský	k2eAgInSc7d1
řezem	řez	k1gInSc7
lze	lze	k6eAd1
riziko	riziko	k1gNnSc4
významně	významně	k6eAd1
snížit	snížit	k5eAaPmF
(	(	kIx(
<g/>
1	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
riziko	riziko	k1gNnSc4
přenosu	přenos	k1gInSc2
představuje	představovat	k5eAaImIp3nS
kojení	kojení	k1gNnSc4
<g/>
,	,	kIx,
kterého	který	k3yRgInSc2,k3yIgInSc2,k3yQgInSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
HIV	HIV	kA
pozitivní	pozitivní	k2eAgFnPc4d1
matky	matka	k1gFnPc4
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
<g/>
-li	-li	k?
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
měly	mít	k5eAaImAgFnP
vyvarovat	vyvarovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Statistika	statistika	k1gFnSc1
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
nákazy	nákaza	k1gFnSc2
HIV	HIV	kA
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
ČR	ČR	kA
tvoří	tvořit	k5eAaImIp3nS
nakažené	nakažený	k2eAgNnSc4d1
HIV	HIV	kA
z	z	k7c2
67	#num#	k4
%	%	kIx~
lidé	člověk	k1gMnPc1
české	český	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
a	a	k8xC
33	#num#	k4
%	%	kIx~
cizinci	cizinec	k1gMnPc1
(	(	kIx(
<g/>
krátkodobý	krátkodobý	k2eAgInSc4d1
pobyt	pobyt	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
rezidenti	rezident	k1gMnPc1
(	(	kIx(
<g/>
dlouhodobý	dlouhodobý	k2eAgInSc4d1
pobyt	pobyt	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
nakaženými	nakažený	k2eAgMnPc7d1
převažují	převažovat	k5eAaImIp3nP
muži	muž	k1gMnPc7
(	(	kIx(
<g/>
přes	přes	k7c4
82	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
převažuje	převažovat	k5eAaImIp3nS
homosexuální	homosexuální	k2eAgInSc1d1
<g/>
/	/	kIx~
<g/>
bisexuální	bisexuální	k2eAgInSc1d1
přenos	přenos	k1gInSc1
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
70	#num#	k4
%	%	kIx~
případů	případ	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SZÚ	SZÚ	kA
evidoval	evidovat	k5eAaImAgInS
k	k	k7c3
listopadu	listopad	k1gInSc3
2012	#num#	k4
1875	#num#	k4
nakažených	nakažený	k2eAgMnPc2d1
virem	vir	k1gInSc7
HIV	HIV	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
364	#num#	k4
onemocnění	onemocnění	k1gNnPc2
AIDS	AIDS	kA
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
186	#num#	k4
zemřelých	zemřelá	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
uveřejnila	uveřejnit	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
referenční	referenční	k2eAgFnSc1d1
laboratoř	laboratoř	k1gFnSc1
pro	pro	k7c4
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
Státního	státní	k2eAgInSc2d1
zdravotního	zdravotní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
statistiky	statistika	k1gFnSc2
za	za	k7c4
1	#num#	k4
<g/>
.	.	kIx.
čtvrtletí	čtvrtletí	k1gNnSc6
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přibylo	přibýt	k5eAaPmAgNnS
95	#num#	k4
pacientů	pacient	k1gMnPc2
s	s	k7c7
HIV	HIV	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
90	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
5	#num#	k4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
měli	mít	k5eAaImAgMnP
sex	sex	k1gInSc4
s	s	k7c7
muži	muž	k1gMnPc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
nakazilo	nakazit	k5eAaPmAgNnS
72	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
heterosexuální	heterosexuální	k2eAgInSc1d1
přenos	přenos	k1gInSc1
byl	být	k5eAaImAgInS
u	u	k7c2
14	#num#	k4
%	%	kIx~
případů	případ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
tak	tak	k6eAd1
evidujeme	evidovat	k5eAaImIp1nP
2715	#num#	k4
nakažených	nakažený	k2eAgMnPc2d1
virem	vir	k1gInSc7
HIV	HIV	kA
<g/>
,	,	kIx,
467	#num#	k4
s	s	k7c7
rozvinutou	rozvinutý	k2eAgFnSc7d1
chorobou	choroba	k1gFnSc7
AIDS	AIDS	kA
a	a	k8xC
241	#num#	k4
zemřelých	zemřelý	k2eAgMnPc2d1
pacientů	pacient	k1gMnPc2
na	na	k7c6
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
podle	podle	k7c2
laboratoře	laboratoř	k1gFnSc2
přibylo	přibýt	k5eAaPmAgNnS
262	#num#	k4
nově	nově	k6eAd1
diagnostikovaných	diagnostikovaný	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
a	a	k8xC
24	#num#	k4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
přenosu	přenos	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
sexuální	sexuální	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
z	z	k7c2
92,3	92,3	k4
%	%	kIx~
případů	případ	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
asi	asi	k9
75	#num#	k4
%	%	kIx~
nových	nový	k2eAgFnPc2d1
nákaz	nákaza	k1gFnPc2
se	se	k3xPyFc4
týkalo	týkat	k5eAaImAgNnS
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
mají	mít	k5eAaImIp3nP
sex	sex	k1gInSc4
s	s	k7c7
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
v	v	k7c6
Česku	Česko	k1gNnSc6
2906	#num#	k4
případů	případ	k1gInPc2
HIV	HIV	kA
<g/>
,	,	kIx,
2484	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
422	#num#	k4
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
500	#num#	k4
se	se	k3xPyFc4
rozvinulo	rozvinout	k5eAaPmAgNnS
AIDS	aids	k1gInSc4
a	a	k8xC
361	#num#	k4
jich	on	k3xPp3gMnPc2
zemřelo	zemřít	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chování	chování	k1gNnSc1
viru	vir	k1gInSc2
v	v	k7c6
organismu	organismus	k1gInSc6
</s>
<s>
Virus	virus	k1gInSc1
v	v	k7c6
organismu	organismus	k1gInSc6
napadá	napadat	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
primárně	primárně	k6eAd1
CD	CD	kA
<g/>
4	#num#	k4
<g/>
+	+	kIx~
buňky	buňka	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
typ	typ	k1gInSc1
T-lymfocytů	T-lymfocyt	k1gInPc2
odpovídajících	odpovídající	k2eAgInPc2d1
za	za	k7c4
řízení	řízení	k1gNnSc4
imunitní	imunitní	k2eAgFnSc2d1
odpovědi	odpověď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inkorporuje	inkorporovat	k5eAaBmIp3nS
se	se	k3xPyFc4
do	do	k7c2
jejich	jejich	k3xOp3gNnSc2
jádra	jádro	k1gNnSc2
a	a	k8xC
čas	čas	k1gInSc1
od	od	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
namnoží	namnožit	k5eAaPmIp3nS
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
organismus	organismus	k1gInSc1
nějak	nějak	k6eAd1
oslaben	oslabit	k5eAaPmNgInS
<g/>
,	,	kIx,
a	a	k8xC
infikuje	infikovat	k5eAaBmIp3nS
další	další	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imunitní	imunitní	k2eAgInSc1d1
systém	systém	k1gInSc1
napadeného	napadený	k2eAgNnSc2d1
zpočátku	zpočátku	k6eAd1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
každou	každý	k3xTgFnSc4
větší	veliký	k2eAgFnSc4d2
produkci	produkce	k1gFnSc4
částic	částice	k1gFnPc2
tvorbou	tvorba	k1gFnSc7
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
protilátek	protilátka	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
infekce	infekce	k1gFnSc1
zpočátku	zpočátku	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
zákopovou	zákopový	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
virus	virus	k1gInSc1
občas	občas	k6eAd1
udělá	udělat	k5eAaPmIp3nS
krůček	krůček	k1gInSc4
vpřed	vpřed	k6eAd1
a	a	k8xC
zase	zase	k9
se	s	k7c7
chvíli	chvíle	k1gFnSc4
nic	nic	k3yNnSc1
neděje	dít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
další	další	k2eAgInSc4d1
krok	krok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
postup	postup	k1gInSc1
je	být	k5eAaImIp3nS
pomalý	pomalý	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nezadržitelný	zadržitelný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Posléze	posléze	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
odpověď	odpověď	k1gFnSc1
imunitního	imunitní	k2eAgInSc2d1
systému	systém	k1gInSc2
na	na	k7c4
podnět	podnět	k1gInSc4
(	(	kIx(
<g/>
infekci	infekce	k1gFnSc4
<g/>
,	,	kIx,
zhoubné	zhoubný	k2eAgNnSc4d1
bujení	bujení	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vzhledem	vzhled	k1gInSc7
ke	k	k7c3
klesajícímu	klesající	k2eAgInSc3d1
počtu	počet	k1gInSc3
CD	CD	kA
<g/>
4	#num#	k4
<g/>
+	+	kIx~
buněk	buňka	k1gFnPc2
stále	stále	k6eAd1
váhavější	váhavý	k2eAgInSc1d2
–	–	k?
přichází	přicházet	k5eAaImIp3nS
opožděně	opožděně	k6eAd1
(	(	kIx(
<g/>
infekce	infekce	k1gFnSc1
má	mít	k5eAaImIp3nS
čas	čas	k1gInSc1
se	se	k3xPyFc4
rozvinout	rozvinout	k5eAaPmF
<g/>
,	,	kIx,
nádorek	nádorek	k1gInSc1
rozrůst	rozrůst	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
trvá	trvat	k5eAaImIp3nS
zbytečně	zbytečně	k6eAd1
dlouho	dlouho	k6eAd1
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
opožděně	opožděně	k6eAd1
vypínána	vypínat	k5eAaImNgFnS
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
značně	značně	k6eAd1
vyčerpává	vyčerpávat	k5eAaImIp3nS
organismus	organismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
se	se	k3xPyFc4
potíže	potíž	k1gFnPc1
akumulují	akumulovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
špatně	špatně	k6eAd1
fungující	fungující	k2eAgInSc1d1
systém	systém	k1gInSc1
se	se	k3xPyFc4
nejdříve	dříve	k6eAd3
přetíží	přetížit	k5eAaPmIp3nS
a	a	k8xC
nakonec	nakonec	k6eAd1
zhroutí	zhroutit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
podpořen	podpořen	k2eAgInSc1d1
extrémním	extrémní	k2eAgNnSc7d1
vyčerpáním	vyčerpání	k1gNnSc7
organismu	organismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
musí	muset	k5eAaImIp3nS
vyvinout	vyvinout	k5eAaPmF
nezměrné	nezměrný	k2eAgNnSc4d1,k2eNgNnSc4d1
úsilí	úsilí	k1gNnSc4
na	na	k7c4
zvládnutí	zvládnutí	k1gNnSc4
i	i	k8xC
té	ten	k3xDgFnSc2
sebemenší	sebemenší	k2eAgFnSc2d1
infekce	infekce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
nemocného	nemocný	k1gMnSc2
propuká	propukat	k5eAaImIp3nS
AIDS	aids	k1gInSc1
a	a	k8xC
virus	virus	k1gInSc1
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
masivně	masivně	k6eAd1
šířit	šířit	k5eAaImF
organismem	organismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
již	již	k6eAd1
napadá	napadat	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
další	další	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
–	–	k?
makrofágy	makrofág	k1gInPc1
<g/>
,	,	kIx,
nervové	nervový	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Odběr	odběr	k1gInSc1
krve	krev	k1gFnSc2
na	na	k7c4
test	test	k1gInSc4
HIV	HIV	kA
</s>
<s>
Testy	test	k1gInPc1
HIV	HIV	kA
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Testy	testa	k1gFnSc2
HIV	HIV	kA
<g/>
.	.	kIx.
</s>
<s>
Testování	testování	k1gNnPc1
přítomnosti	přítomnost	k1gFnSc2
HIV	HIV	kA
je	být	k5eAaImIp3nS
laboratorní	laboratorní	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
je	být	k5eAaImIp3nS
analyzována	analyzován	k2eAgFnSc1d1
krev	krev	k1gFnSc1
jedince	jedinec	k1gMnSc2
na	na	k7c4
protilátky	protilátka	k1gFnPc4
proti	proti	k7c3
HIV	HIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
složen	složit	k5eAaPmNgInS
z	z	k7c2
odběru	odběr	k1gInSc2
krve	krev	k1gFnSc2
pomocí	pomocí	k7c2
injekční	injekční	k2eAgFnSc2d1
stříkačky	stříkačka	k1gFnSc2
a	a	k8xC
laboratorního	laboratorní	k2eAgInSc2d1
rozboru	rozbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
testují	testovat	k5eAaImIp3nP
i	i	k9
metody	metoda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
dokázaly	dokázat	k5eAaPmAgFnP
virus	virus	k1gInSc4
lokalizovat	lokalizovat	k5eAaBmF
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc3
odběru	odběr	k1gInSc2
lidské	lidský	k2eAgFnSc2d1
krve	krev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
viru	vir	k1gInSc2
</s>
<s>
Stáří	stáří	k1gNnSc1
nejstarších	starý	k2eAgMnPc2d3
lentivirů	lentivir	k1gMnPc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
60	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Viry	vira	k1gFnSc2
HIV-1	HIV-1	k1gFnSc2
a	a	k8xC
HIV-2	HIV-2	k1gFnSc2
se	se	k3xPyFc4
vyvinuly	vyvinout	k5eAaPmAgFnP
z	z	k7c2
lentivirů	lentivir	k1gInPc2
napadajících	napadající	k2eAgInPc2d1
různé	různý	k2eAgInPc4d1
primáty	primát	k1gInPc4
v	v	k7c6
západní	západní	k2eAgFnSc6d1
a	a	k8xC
centrální	centrální	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
na	na	k7c4
člověka	člověk	k1gMnSc4
se	se	k3xPyFc4
přenesly	přenést	k5eAaPmAgFnP
někdy	někdy	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k8xS,k8xC
klasické	klasický	k2eAgFnSc2d1
zoonózy	zoonóza	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zleva	zleva	k6eAd1
<g/>
:	:	kIx,
kočkodan	kočkodan	k1gMnSc1
zelený	zelený	k2eAgMnSc1d1
(	(	kIx(
<g/>
zdroj	zdroj	k1gInSc1
SIV	sivo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mangabej	mangabej	k1gMnSc1
kouřový	kouřový	k2eAgMnSc1d1
(	(	kIx(
<g/>
zdroj	zdroj	k1gInSc1
HIV-	HIV-	k1gFnSc2
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
šimpanz	šimpanz	k1gMnSc1
(	(	kIx(
<g/>
zdroj	zdroj	k1gInSc1
HIV-	HIV-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
HIV-1	HIV-1	k4
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
vyvinul	vyvinout	k5eAaPmAgInS
v	v	k7c6
jižním	jižní	k2eAgInSc6d1
Kamerunu	Kamerun	k1gInSc6
evolucí	evoluce	k1gFnSc7
viru	vir	k1gInSc2
opičí	opičí	k2eAgFnSc2d1
imunitní	imunitní	k2eAgFnSc2d1
nedostatečnosti	nedostatečnost	k1gFnSc2
–	–	k?
Simian	Simian	k1gMnSc1
Immunodeficiency	Immunodeficienca	k1gFnSc2
Virus	virus	k1gInSc1
<g/>
,	,	kIx,
SIV	sivo	k1gNnPc2
(	(	kIx(
<g/>
cpz	cpz	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
napadá	napadat	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
šimpanze	šimpanz	k1gMnPc4
čego	čego	k6eAd1
(	(	kIx(
<g/>
Pan	Pan	k1gMnSc1
troglodytes	troglodytes	k1gMnSc1
troglodytes	troglodytes	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
HIV-2	HIV-2	k1gMnSc1
má	mít	k5eAaImIp3nS
mírně	mírně	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
původ	původ	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc3
nejbližším	blízký	k2eAgMnPc3d3
příbuzným	příbuzný	k1gMnPc3
je	být	k5eAaImIp3nS
SIV	sivo	k1gNnPc2
(	(	kIx(
<g/>
smm	smm	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
hostitelem	hostitel	k1gMnSc7
je	být	k5eAaImIp3nS
mangabej	mangabej	k1gMnSc1
kouřový	kouřový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Cercocebus	Cercocebus	k1gMnSc1
atys	atys	k6eAd1
atys	atys	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jiná	jiný	k2eAgFnSc1d1
úzkonosá	úzkonosý	k2eAgFnSc1d1
opice	opice	k1gFnSc1
vyskytující	vyskytující	k2eAgFnSc1d1
se	se	k3xPyFc4
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
západní	západní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
jižního	jižní	k2eAgInSc2d1
Senegalu	Senegal	k1gInSc2
po	po	k7c4
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
Pobřeží	pobřeží	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
slonoviny	slonovina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
ploskonosé	ploskonosý	k2eAgFnPc1d1
opice	opice	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
mirikina	mirikina	k1gFnSc1
(	(	kIx(
<g/>
Aotus	Aotus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vůči	vůči	k7c3
HIV	HIV	kA
pravděpodobně	pravděpodobně	k6eAd1
imunní	imunní	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
HIV-1	HIV-1	k1gFnSc2
přeskočil	přeskočit	k5eAaPmAgInS
druhovou	druhový	k2eAgFnSc4d1
bariéru	bariéra	k1gFnSc4
z	z	k7c2
šimpanze	šimpanz	k1gMnSc2
na	na	k7c4
člověka	člověk	k1gMnSc4
asi	asi	k9
(	(	kIx(
<g/>
minimálně	minimálně	k6eAd1
<g/>
)	)	kIx)
třikrát	třikrát	k6eAd1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
také	také	k6eAd1
vznikly	vzniknout	k5eAaPmAgInP
tři	tři	k4xCgFnPc4
virové	virový	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
HIV-	HIV-	k1gFnSc4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
M	M	kA
<g/>
,	,	kIx,
N	N	kA
a	a	k8xC
O.	O.	kA
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Afričtí	africký	k2eAgMnPc1d1
lovci	lovec	k1gMnPc1
a	a	k8xC
prodejci	prodejce	k1gMnPc1
zvěřiny	zvěřina	k1gFnSc2
v	v	k7c6
Africe	Afrika	k1gFnSc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
snadno	snadno	k6eAd1
nakazit	nakazit	k5eAaPmF
opičím	opičit	k5eAaImIp1nS
SIV	sivo	k1gNnPc2
a	a	k8xC
často	často	k6eAd1
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
pravděpodobně	pravděpodobně	k6eAd1
i	i	k9
stává	stávat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
SIV	sivo	k1gNnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
lidském	lidský	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
slabý	slabý	k2eAgMnSc1d1
a	a	k8xC
imunitní	imunitní	k2eAgInSc1d1
systém	systém	k1gInSc1
ho	on	k3xPp3gMnSc4
během	během	k7c2
několika	několik	k4yIc2
týdnů	týden	k1gInPc2
zcela	zcela	k6eAd1
potlačí	potlačit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyvinul	vyvinout	k5eAaPmAgInS
do	do	k7c2
HIV	HIV	kA
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
muselo	muset	k5eAaImAgNnS
proběhnout	proběhnout	k5eAaPmF
několik	několik	k4yIc1
přenosů	přenos	k1gInPc2
mezi	mezi	k7c7
jedinci	jedinec	k1gMnPc7
v	v	k7c6
rychlém	rychlý	k2eAgInSc6d1
sledu	sled	k1gInSc6
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Přitom	přitom	k6eAd1
přenos	přenos	k1gInSc1
tohoto	tento	k3xDgInSc2
viru	vir	k1gInSc2
z	z	k7c2
člověka	člověk	k1gMnSc2
na	na	k7c4
člověka	člověk	k1gMnSc4
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
neefektivní	efektivní	k2eNgMnSc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
šířit	šířit	k5eAaImF
jen	jen	k9
v	v	k7c6
populacích	populace	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
rozšířeny	rozšířit	k5eAaPmNgFnP
vysokorizikové	vysokorizikový	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
přenosu	přenos	k1gInSc2
(	(	kIx(
<g/>
jež	jenž	k3xRgFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
Africe	Afrika	k1gFnSc6
před	před	k7c7
začátkem	začátek	k1gInSc7
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
spíše	spíše	k9
vzácné	vzácný	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
konkrétní	konkrétní	k2eAgFnSc1d1
vysokoriziková	vysokorizikový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
přenosu	přenos	k1gInSc2
viru	vir	k1gInSc2
hrála	hrát	k5eAaImAgFnS
roli	role	k1gFnSc4
ve	v	k7c6
vzniku	vznik	k1gInSc6
HIV-1	HIV-1	k1gFnSc2
a	a	k8xC
rozšíření	rozšíření	k1gNnSc2
do	do	k7c2
lidské	lidský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
období	období	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vlastně	vlastně	k9
k	k	k7c3
prvotnímu	prvotní	k2eAgInSc3d1
přenosu	přenos	k1gInSc3
mezi	mezi	k7c7
zvířetem	zvíře	k1gNnSc7
a	a	k8xC
člověkem	člověk	k1gMnSc7
došlo	dojít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genetické	genetický	k2eAgFnPc4d1
studie	studie	k1gFnPc4
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
HIV-1	HIV-1	k1gMnSc1
skupiny	skupina	k1gFnSc2
M	M	kA
se	se	k3xPyFc4
datuje	datovat	k5eAaImIp3nS
někdy	někdy	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1910	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
epidemie	epidemie	k1gFnSc1
HIV	HIV	kA
souviset	souviset	k5eAaImF
s	s	k7c7
rozvojem	rozvoj	k1gInSc7
kolonialismu	kolonialismus	k1gInSc2
a	a	k8xC
vznikem	vznik	k1gInSc7
velkých	velký	k2eAgNnPc2d1
afrických	africký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
šířila	šířit	k5eAaImAgFnS
promiskuita	promiskuita	k1gFnSc1
<g/>
,	,	kIx,
prostituce	prostituce	k1gFnSc1
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgInSc7
související	související	k2eAgFnPc4d1
vředovitá	vředovitý	k2eAgNnPc4d1
onemocnění	onemocnění	k1gNnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
syfilis	syfilis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Riziko	riziko	k1gNnSc1
přenosu	přenos	k1gInSc2
HIV	HIV	kA
vaginálním	vaginální	k2eAgInSc7d1
sexuálním	sexuální	k2eAgInSc7d1
stykem	styk	k1gInSc7
totiž	totiž	k9
prudce	prudko	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
partnerů	partner	k1gMnPc2
trpí	trpět	k5eAaImIp3nS
vředovitým	vředovitý	k2eAgNnSc7d1
onemocněním	onemocnění	k1gNnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
pohlavních	pohlavní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
se	se	k3xPyFc4
udává	udávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
např.	např.	kA
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Leopoldville	Leopoldvillo	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
bylo	být	k5eAaImAgNnS
45	#num#	k4
%	%	kIx~
ženských	ženský	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
prostitutkami	prostitutka	k1gFnPc7
a	a	k8xC
15	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
trpělo	trpět	k5eAaImAgNnS
jednou	jednou	k6eAd1
z	z	k7c2
forem	forma	k1gFnPc2
syfilis	syfilis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
odlišné	odlišný	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
souvisí	souviset	k5eAaImIp3nS
rozšíření	rozšíření	k1gNnSc3
HIV	HIV	kA
s	s	k7c7
lékařskými	lékařský	k2eAgFnPc7d1
iniciativami	iniciativa	k1gFnPc7
v	v	k7c6
Africe	Afrika	k1gFnSc6
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
<g />
.	.	kIx.
</s>
<s hack="1">
válce	válec	k1gInSc2
(	(	kIx(
<g/>
očkovací	očkovací	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
<g/>
,	,	kIx,
podávání	podávání	k1gNnSc1
antibiotik	antibiotikum	k1gNnPc2
a	a	k8xC
protimalarická	protimalarický	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgInPc6
byly	být	k5eAaImAgFnP
opakovaně	opakovaně	k6eAd1
používány	používat	k5eAaImNgFnP
nesterilní	sterilní	k2eNgFnPc1d1
injekční	injekční	k2eAgFnPc1d1
stříkačky	stříkačka	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
mohly	moct	k5eAaImAgInP
tedy	tedy	k9
umožnit	umožnit	k5eAaPmF
šíření	šíření	k1gNnSc4
viru	vir	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3
dokumentovaný	dokumentovaný	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
HIV	HIV	kA
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Belgického	belgický	k2eAgNnSc2d1
Konga	Kongo	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Virus	virus	k1gInSc1
byl	být	k5eAaImAgInS
možná	možná	k9
přítomen	přítomno	k1gNnPc2
např.	např.	kA
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
už	už	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1
pacient	pacient	k1gMnSc1
se	s	k7c7
symptomy	symptom	k1gInPc7
nemoci	nemoc	k1gFnSc2
byl	být	k5eAaImAgInS
diagnostikován	diagnostikovat	k5eAaBmNgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
a	a	k8xC
podlehl	podlehnout	k5eAaPmAgMnS
jí	on	k3xPp3gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
nebyl	být	k5eNaImAgInS
prokazatelně	prokazatelně	k6eAd1
zjištěn	zjistit	k5eAaPmNgInS
původ	původ	k1gInSc1
viru	vir	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konspirační	konspirační	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
</s>
<s>
Jelikož	jelikož	k8xS
onemocnění	onemocnění	k1gNnSc1
HIV	HIV	kA
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
a	a	k8xC
znenadání	znenadání	k6eAd1
<g/>
,	,	kIx,
objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
spekulací	spekulace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
rychlý	rychlý	k2eAgInSc4d1
nástup	nástup	k1gInSc4
nemoci	nemoc	k1gFnSc2
vysvětlit	vysvětlit	k5eAaPmF
lidskou	lidský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
respektive	respektive	k9
jako	jako	k8xC,k8xS
výsledek	výsledek	k1gInSc1
armádních	armádní	k2eAgMnPc2d1
pokusů	pokus	k1gInPc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
i	i	k9
nástrojem	nástroj	k1gInSc7
souboje	souboj	k1gInSc2
hlavních	hlavní	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Pavel	Pavel	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
Jefremov	Jefremov	k1gInSc4
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
v	v	k7c6
moskevském	moskevský	k2eAgNnSc6d1
ústředí	ústředí	k1gNnSc6
KGB	KGB	kA
podílel	podílet	k5eAaImAgMnS
na	na	k7c4
vymýšlení	vymýšlení	k1gNnSc4
různých	různý	k2eAgFnPc2d1
dezinformací	dezinformace	k1gFnPc2
–	–	k?
vytvořil	vytvořit	k5eAaPmAgMnS
např.	např.	kA
fámu	fáma	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
virus	virus	k1gInSc1
HIV	HIV	kA
byl	být	k5eAaImAgInS
uměle	uměle	k6eAd1
vytvořen	vytvořit	k5eAaPmNgInS
v	v	k7c6
americké	americký	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
teoriích	teorie	k1gFnPc6
umělého	umělý	k2eAgInSc2d1
původu	původ	k1gInSc2
HIV	HIV	kA
mluví	mluvit	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
nositelka	nositelka	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Nobelovy	Nobelův	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
míru	míra	k1gFnSc4
Wangari	Wangare	k1gFnSc4
Maathai	Maathae	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
konspirační	konspirační	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
o	o	k7c6
umělém	umělý	k2eAgInSc6d1
původu	původ	k1gInSc6
viru	vir	k1gInSc2
většinou	většina	k1gFnSc7
vědecké	vědecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
odmítány	odmítat	k5eAaImNgInP
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
v	v	k7c6
době	doba	k1gFnSc6
prvních	první	k4xOgInPc2
zdokumentovaných	zdokumentovaný	k2eAgInPc2d1
případů	případ	k1gInPc2
výskytu	výskyt	k1gInSc2
HIV	HIV	kA
nebyla	být	k5eNaImAgFnS
technologická	technologický	k2eAgFnSc1d1
vyspělost	vyspělost	k1gFnSc1
lidstva	lidstvo	k1gNnSc2
na	na	k7c6
takové	takový	k3xDgFnSc6
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
umožňovala	umožňovat	k5eAaImAgFnS
složitou	složitý	k2eAgFnSc4d1
genovou	genový	k2eAgFnSc4d1
manipulaci	manipulace	k1gFnSc4
s	s	k7c7
virem	vir	k1gInSc7
a	a	k8xC
výrobu	výroba	k1gFnSc4
nového	nový	k2eAgInSc2d1
viru	vir	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
HIV	HIV	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Hygienici	hygienik	k1gMnPc1
podali	podat	k5eAaPmAgMnP
trestní	trestní	k2eAgNnSc4d1
oznámení	oznámení	k1gNnSc4
na	na	k7c4
30	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
podezírají	podezírat	k5eAaImIp3nP
je	on	k3xPp3gNnSc4
z	z	k7c2
šíření	šíření	k1gNnSc2
HIV	HIV	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01-26	2016-01-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Weller	Weller	k1gInSc1
S	s	k7c7
<g/>
,	,	kIx,
Davis	Davis	k1gFnPc7
<g/>
,	,	kIx,
K.	K.	kA
<g/>
,	,	kIx,
Condom	Condom	k1gInSc1
effectiveness	effectiveness	k1gInSc1
in	in	k?
reducing	reducing	k1gInSc1
heterosexual	heterosexual	k1gInSc1
HIV	HIV	kA
transmission	transmission	k1gInSc1
(	(	kIx(
<g/>
Cochrane	Cochran	k1gInSc5
Review	Review	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Cochrane	Cochran	k1gInSc5
Library	Librara	k1gFnPc1
<g/>
,	,	kIx,
Issue	Issue	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chichester	Chichester	k1gInSc1
<g/>
,	,	kIx,
UK	UK	kA
<g/>
:	:	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
&	&	k?
Sons	Sons	k1gInSc1
<g/>
,	,	kIx,
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.szu.cz/uploads/documents/CeM/HIV_AIDS/rocni_zpravy/2012/trendy_HIVAIDS_CR.pdf	http://www.szu.cz/uploads/documents/CeM/HIV_AIDS/rocni_zpravy/2012/trendy_HIVAIDS_CR.pdf	k1gInSc1
-	-	kIx~
Dlouhodobé	dlouhodobý	k2eAgInPc1d1
trendy	trend	k1gInPc1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
epidemiologické	epidemiologický	k2eAgFnSc2d1
situace	situace	k1gFnSc2
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
v	v	k7c6
ČR	ČR	kA
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
,	,	kIx,
V	v	k7c6
ČR	ČR	kA
letos	letos	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
listopadu	listopad	k1gInSc2
přibylo	přibýt	k5eAaPmAgNnS
200	#num#	k4
pacientů	pacient	k1gMnPc2
s	s	k7c7
virem	vir	k1gInSc7
HIV	HIV	kA
<g/>
,	,	kIx,
22.12	22.12	k4
<g/>
.2012	.2012	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
,	,	kIx,
V	v	k7c6
ČR	ČR	kA
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
čtvrtletí	čtvrtletí	k1gNnSc6
rekordně	rekordně	k6eAd1
přibylo	přibýt	k5eAaPmAgNnS
pacientů	pacient	k1gMnPc2
s	s	k7c7
HIV	HIV	kA
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
jich	on	k3xPp3gMnPc2
95	#num#	k4
<g/>
,	,	kIx,
28.04	28.04	k4
<g/>
.2016	.2016	k4
<g/>
↑	↑	k?
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Lidí	člověk	k1gMnPc2
s	s	k7c7
HIV	HIV	kA
přibývá	přibývat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
u	u	k7c2
mužů	muž	k1gMnPc2
než	než	k8xS
u	u	k7c2
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
28.1	28.1	k4
<g/>
.2017	.2017	k4
<g/>
↑	↑	k?
MUDR.	MUDR.	k1gMnSc1
SMETANA	Smetana	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epidemiologie	epidemiologie	k1gFnSc1
HIV	HIV	kA
<g/>
/	/	kIx~
<g/>
AIDS	AIDS	kA
<g/>
.	.	kIx.
aktuální	aktuální	k2eAgFnSc2d1
situace	situace	k1gFnSc2
v	v	k7c6
ČR	ČR	kA
a	a	k8xC
ve	v	k7c6
světě	svět	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
kar	kar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Virus	virus	k1gInSc1
HIV	HIV	kA
je	být	k5eAaImIp3nS
starší	starý	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
jsme	být	k5eAaImIp1nP
mysleli	myslet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
ho	on	k3xPp3gNnSc4
vystopovali	vystopovat	k5eAaPmAgMnP
do	do	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
60	#num#	k4
miliony	milion	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-10-07	2016-10-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SHARP	sharp	k1gInSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
HAHN	HAHN	kA
<g/>
,	,	kIx,
Beatrice	Beatrice	k1gFnSc2
H.	H.	kA
Origins	Origins	k1gInSc1
of	of	k?
HIV	HIV	kA
and	and	k?
the	the	k?
AIDS	AIDS	kA
Pandemic	Pandemic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cold	Colda	k1gFnPc2
Spring	Spring	k1gInSc1
Harbor	Harbor	k1gMnSc1
Perspectives	Perspectives	k1gMnSc1
in	in	k?
Medicine	Medicin	k1gInSc5
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
a	a	k8xC
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6841	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
<g/>
:	:	kIx,
22229120	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2157	#num#	k4
<g/>
-	-	kIx~
<g/>
1422	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
cshperspect	cshperspect	k1gInSc1
<g/>
.	.	kIx.
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6841	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FARIA	FARIA	kA
<g/>
,	,	kIx,
Nuno	Nuno	k1gMnSc1
R.	R.	kA
<g/>
;	;	kIx,
RAMBAUT	RAMBAUT	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
<g/>
;	;	kIx,
SUCHARD	SUCHARD	kA
<g/>
,	,	kIx,
Marc	Marc	k1gFnSc1
A.	A.	kA
The	The	k1gFnSc1
early	earl	k1gMnPc4
spread	spread	k6eAd1
and	and	k?
epidemic	epidemic	k1gMnSc1
ignition	ignition	k1gInSc1
of	of	k?
HIV-1	HIV-1	k1gFnSc2
in	in	k?
human	human	k1gInSc1
populations	populations	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
346	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6205	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
56	#num#	k4
<g/>
–	–	k?
<g/>
61	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
<g/>
:	:	kIx,
25278604	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
36	#num#	k4
<g/>
-	-	kIx~
<g/>
8075	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1256739	.1256739	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GAO	GAO	kA
<g/>
,	,	kIx,
Feng	Feng	k1gMnSc1
<g/>
;	;	kIx,
BAILES	BAILES	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
;	;	kIx,
ROBERTSON	ROBERTSON	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
L.	L.	kA
Origin	Origin	k1gMnSc1
of	of	k?
HIV-1	HIV-1	k1gMnSc1
in	in	k?
the	the	k?
chimpanzee	chimpanze	k1gFnSc2
Pan	Pan	k1gMnSc1
troglodytes	troglodytes	k1gMnSc1
troglodytes	troglodytes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
397	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6718	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
436	#num#	k4
<g/>
–	–	k?
<g/>
441	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
17130	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KEELE	KEELE	kA
<g/>
,	,	kIx,
Brandon	Brandon	k1gMnSc1
F.	F.	kA
<g/>
;	;	kIx,
HEUVERSWYN	HEUVERSWYN	kA
<g/>
,	,	kIx,
Fran	Fran	k1gNnSc1
Van	vana	k1gFnPc2
<g/>
;	;	kIx,
LI	li	k9
<g/>
,	,	kIx,
Yingying	Yingying	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chimpanzee	Chimpanzee	k1gNnSc1
Reservoirs	Reservoirsa	k1gFnPc2
of	of	k?
Pandemic	Pandemic	k1gMnSc1
and	and	k?
Nonpandemic	Nonpandemic	k1gMnSc1
HIV-	HIV-	k1gMnSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
313	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5786	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
523	#num#	k4
<g/>
–	–	k?
<g/>
526	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
<g/>
:	:	kIx,
16728595	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
36	#num#	k4
<g/>
-	-	kIx~
<g/>
8075	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1126531	.1126531	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
REEVES	REEVES	kA
<g/>
,	,	kIx,
Jacqueline	Jacquelin	k1gInSc5
D.	D.	kA
<g/>
;	;	kIx,
DOMS	DOMS	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
W.	W.	kA
Human	Human	k1gMnSc1
immunodeficiency	immunodeficienca	k1gFnSc2
virus	virus	k1gInSc1
type	typ	k1gInSc5
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
General	General	k1gMnSc1
Virology	virolog	k1gMnPc7
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
83	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1253	#num#	k4
<g/>
–	–	k?
<g/>
1265	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
1317	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
1253	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GOODIER	GOODIER	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
KAZAZIAN	KAZAZIAN	kA
<g/>
,	,	kIx,
Haig	Haig	k1gInSc1
H.	H.	kA
Retrotransposons	Retrotransposons	k1gInSc1
Revisited	Revisited	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Restraint	Restraint	k1gMnSc1
and	and	k?
Rehabilitation	Rehabilitation	k1gInSc1
of	of	k?
Parasites	Parasites	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cell	cello	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
135	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
23	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cell	cello	k1gNnPc2
<g/>
.2008	.2008	k4
<g/>
.09	.09	k4
<g/>
.022	.022	k4
<g/>
.	.	kIx.
↑	↑	k?
SHARP	sharp	k1gInSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
BAILES	BAILES	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
;	;	kIx,
CHAUDHURI	CHAUDHURI	kA
<g/>
,	,	kIx,
Roy	Roy	k1gMnSc1
R.	R.	kA
The	The	k1gMnSc1
origins	origins	k6eAd1
of	of	k?
acquired	acquired	k1gInSc1
immune	immun	k1gInSc5
deficiency	deficiency	k1gInPc1
syndrome	syndrom	k1gInSc5
viruses	viruses	k1gInSc4
<g/>
:	:	kIx,
where	wher	k1gMnSc5
and	and	k?
when	whena	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philosophical	Philosophical	k1gFnSc1
Transactions	Transactionsa	k1gFnPc2
of	of	k?
the	the	k?
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
London	London	k1gMnSc1
B	B	kA
<g/>
:	:	kIx,
Biological	Biological	k1gMnSc1
Sciences	Sciences	k1gMnSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
356	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1410	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
867	#num#	k4
<g/>
–	–	k?
<g/>
876	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
<g/>
:	:	kIx,
11405934	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
962	#num#	k4
<g/>
-	-	kIx~
<g/>
8436	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rstb	rstb	k1gInSc1
<g/>
.2001	.2001	k4
<g/>
.0863	.0863	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KALISH	KALISH	kA
<g/>
,	,	kIx,
Marcia	Marcia	k1gFnSc1
L.	L.	kA
<g/>
;	;	kIx,
WOLFE	Wolf	k1gMnSc5
<g/>
,	,	kIx,
Nathan	Nathan	k1gMnSc1
D.	D.	kA
<g/>
;	;	kIx,
NDONGMO	NDONGMO	kA
<g/>
,	,	kIx,
Clement	Clement	k1gMnSc1
B.	B.	kA
Central	Central	k1gMnSc1
African	African	k1gMnSc1
Hunters	Huntersa	k1gFnPc2
Exposed	Exposed	k1gMnSc1
to	ten	k3xDgNnSc4
Simian	Simian	k1gMnSc1
Immunodeficiency	Immunodeficienca	k1gFnSc2
Virus	virus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emerging	Emerging	k1gInSc1
Infectious	Infectious	k1gMnSc1
Diseases	Diseases	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1928	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.320	10.320	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
eid	eid	k?
<g/>
1112.050394	1112.050394	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
MARX	Marx	k1gMnSc1
<g/>
,	,	kIx,
Preston	Preston	k1gInSc1
A.	A.	kA
<g/>
;	;	kIx,
ALCABES	ALCABES	kA
<g/>
,	,	kIx,
Phillip	Phillip	k1gMnSc1
G.	G.	kA
<g/>
;	;	kIx,
DRUCKER	DRUCKER	kA
<g/>
,	,	kIx,
Ernest	Ernest	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Serial	Serial	k1gMnSc1
human	human	k1gMnSc1
passage	passage	k6eAd1
of	of	k?
simian	simian	k1gMnSc1
immunodeficiency	immunodeficienca	k1gFnSc2
virus	virus	k1gInSc1
by	by	kYmCp3nP
unsterile	unsterile	k6eAd1
injections	injections	k1gInSc1
and	and	k?
the	the	k?
emergence	emergence	k1gFnSc2
of	of	k?
epidemic	epidemic	k1gMnSc1
human	human	k1gMnSc1
immunodeficiency	immunodeficienca	k1gFnSc2
virus	virus	k1gInSc1
in	in	k?
Africa	Africa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philosophical	Philosophical	k1gFnSc1
Transactions	Transactionsa	k1gFnPc2
of	of	k?
the	the	k?
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
London	London	k1gMnSc1
B	B	kA
<g/>
:	:	kIx,
Biological	Biological	k1gMnSc1
Sciences	Sciences	k1gMnSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
356	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1410	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
911	#num#	k4
<g/>
–	–	k?
<g/>
920	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
<g/>
:	:	kIx,
11405938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
962	#num#	k4
<g/>
-	-	kIx~
<g/>
8436	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rstb	rstb	k1gInSc1
<g/>
.2001	.2001	k4
<g/>
.0867	.0867	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WOROBEY	WOROBEY	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
;	;	kIx,
GEMMEL	GEMMEL	kA
<g/>
,	,	kIx,
Marlea	Marlea	k1gFnSc1
<g/>
;	;	kIx,
TEUWEN	TEUWEN	kA
<g/>
,	,	kIx,
Dirk	Dirk	k1gMnSc1
E.	E.	kA
Direct	Direct	k1gMnSc1
evidence	evidence	k1gFnSc2
of	of	k?
extensive	extensivat	k5eAaPmIp3nS
diversity	diversit	k1gInPc4
of	of	k?
HIV-1	HIV-1	k1gFnSc2
in	in	k?
Kinshasa	Kinshasa	k1gFnSc1
by	by	k9
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
455	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7213	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
661	#num#	k4
<g/>
–	–	k?
<g/>
664	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nature	natur	k1gMnSc5
<g/>
0	#num#	k4
<g/>
7390	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
SOUSA	SOUSA	kA
<g/>
,	,	kIx,
Joã	Joã	k1gMnSc1
Dinis	Dinis	k1gFnSc2
de	de	k?
<g/>
;	;	kIx,
MÜLLER	MÜLLER	kA
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
<g/>
;	;	kIx,
LEMEY	LEMEY	kA
<g/>
,	,	kIx,
Philippe	Philipp	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
High	High	k1gInSc1
GUD	GUD	kA
Incidence	incidence	k1gFnSc2
in	in	k?
the	the	k?
Early	earl	k1gMnPc7
20	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
Created	Created	k1gMnSc1
a	a	k8xC
Particularly	Particularla	k1gMnSc2
Permissive	Permissiev	k1gFnSc2
Time	Time	k1gNnSc1
Window	Window	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
Origin	Origin	k1gMnSc1
and	and	k?
Initial	Initial	k1gInSc1
Spread	Spread	k1gInSc1
of	of	k?
Epidemic	Epidemice	k1gFnPc2
HIV	HIV	kA
Strains	Strains	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PLOS	plos	k1gMnSc1
ONE	ONE	kA
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
e	e	k0
<g/>
9936	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
6203	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.137	10.137	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
journal	journat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
pone	pone	k1gInSc1
<g/>
.0009936	.0009936	k4
<g/>
.	.	kIx.
↑	↑	k?
CHITNIS	CHITNIS	kA
<g/>
,	,	kIx,
Amit	Amit	k1gMnSc1
<g/>
;	;	kIx,
RAWLS	RAWLS	kA
<g/>
,	,	kIx,
Diana	Diana	k1gFnSc1
<g/>
;	;	kIx,
MOORE	MOORE	kA
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Origin	Origin	k1gInSc1
of	of	k?
HIV	HIV	kA
Type	typ	k1gInSc5
1	#num#	k4
in	in	k?
Colonial	Colonial	k1gMnSc1
French	French	k1gMnSc1
Equatorial	Equatorial	k1gMnSc1
Africa	Africa	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
AIDS	aids	k1gInSc1
Research	Research	k1gInSc4
and	and	k?
Human	Human	k1gInSc1
Retroviruses	Retroviruses	k1gInSc1
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
16	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
889	#num#	k4
<g/>
-	-	kIx~
<g/>
2229	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
88922200309548	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JR	JR	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
G.	G.	kA
Mcneil	Mcneil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Precursor	Precursor	k1gInSc1
to	ten	k3xDgNnSc4
H.I.V.	H.I.V.	k1gFnSc7
Was	Was	k1gMnSc1
in	in	k?
Monkeys	Monkeys	k1gInSc1
for	forum	k1gNnPc2
Millenniums	Millenniumsa	k1gFnPc2
<g/>
,	,	kIx,
Study	stud	k1gInPc1
Says	Saysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HO	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
David	David	k1gMnSc1
D.	D.	kA
<g/>
;	;	kIx,
ZHU	ZHU	kA
<g/>
,	,	kIx,
Tuofu	Tuof	k1gInSc2
<g/>
;	;	kIx,
KORBER	KORBER	kA
<g/>
,	,	kIx,
Bette	Bett	k1gInSc5
T.	T.	kA
An	An	k1gMnSc1
African	African	k1gMnSc1
HIV-1	HIV-1	k1gMnSc1
sequence	sequenka	k1gFnSc3
from	from	k6eAd1
1959	#num#	k4
and	and	k?
implications	implicationsa	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
origin	origin	k1gMnSc1
of	of	k?
the	the	k?
epidemic	epidemic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
391	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6667	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
594	#num#	k4
<g/>
–	–	k?
<g/>
597	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
35400	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KOLATA	KOLATA	kA
<g/>
,	,	kIx,
Gina	Ginum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BOY	boy	k1gMnSc1
<g/>
'	'	kIx"
<g/>
S	s	k7c7
1969	#num#	k4
DEATH	DEATH	kA
SUGGESTS	SUGGESTS	kA
AIDS	AIDS	kA
INVADED	INVADED	kA
U.	U.	kA
<g/>
S.	S.	kA
SEVERAL	SEVERAL	kA
TIMES	TIMES	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Womack	Womack	k1gInSc1
<g/>
,	,	kIx,
H.	H.	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Špioni	špion	k1gMnPc1
KGB	KGB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utajené	utajený	k2eAgInPc1d1
životy	život	k1gInPc1
agentů	agens	k1gInPc2
sovětské	sovětský	k2eAgFnSc2d1
tajné	tajný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
408	#num#	k4
s.	s.	k?
<g/>
↑	↑	k?
Nobel	Nobel	k1gMnSc1
peace	peace	k1gMnSc1
laureate	laureat	k1gInSc5
claims	claims	k6eAd1
HIV	HIV	kA
deliberately	deliberatet	k5eAaBmAgFnP,k5eAaPmAgFnP,k5eAaImAgFnP
created	created	k1gInSc4
<g/>
.	.	kIx.
www.abc.net.au	www.abc.net.au	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Původ	původ	k1gInSc1
HIV	HIV	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AIDS	aids	k1gInSc1
pomoc	pomoc	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
AIDS	AIDS	kA
</s>
<s>
Test	test	k1gInSc1
HIV	HIV	kA
</s>
<s>
Imunitní	imunitní	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Virus	virus	k1gInSc1
</s>
<s>
Bugchasing	Bugchasing	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
HIV	HIV	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
HIV	HIV	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Národní	národní	k2eAgInSc1d1
program	program	k1gInSc1
boje	boj	k1gInSc2
proti	proti	k7c3
AIDS	AIDS	kA
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
AIDS	aids	k1gInSc4
a	a	k8xC
virus	virus	k1gInSc4
HIV	HIV	kA
Archivováno	archivován	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
na	na	k7c6
starých	starý	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
programu	program	k1gInSc2
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1
neručí	ručit	k5eNaImIp3nS
za	za	k7c4
správnost	správnost	k1gFnSc4
lékařských	lékařský	k2eAgFnPc2d1
informací	informace	k1gFnPc2
v	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
vyhledejte	vyhledat	k5eAaPmRp2nP
lékaře	lékař	k1gMnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
Přečtěte	přečíst	k5eAaPmRp2nP
si	se	k3xPyFc3
prosím	prosit	k5eAaImIp1nS
pokyny	pokyn	k1gInPc4
pro	pro	k7c4
využití	využití	k1gNnSc4
článků	článek	k1gInPc2
o	o	k7c4
zdravotnictví	zdravotnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Sexuálně	sexuálně	k6eAd1
přenosné	přenosný	k2eAgFnSc2d1
nemoci	nemoc	k1gFnSc2
a	a	k8xC
infekce	infekce	k1gFnSc2
(	(	kIx(
<g/>
SPN	SPN	kA
<g/>
/	/	kIx~
<g/>
SPI	spát	k5eAaImRp2nS
<g/>
)	)	kIx)
(	(	kIx(
<g/>
primárně	primárně	k6eAd1
A	a	k8xC
<g/>
50	#num#	k4
<g/>
-A	-A	k?
<g/>
64	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
90	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
99	#num#	k4
<g/>
)	)	kIx)
virové	virový	k2eAgInPc1d1
</s>
<s>
AIDS	AIDS	kA
(	(	kIx(
<g/>
HIV-	HIV-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
HIV-	HIV-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
karcinom	karcinom	k1gInSc1
děložního	děložní	k2eAgNnSc2d1
hrdla	hrdlo	k1gNnSc2
&	&	k?
genitální	genitální	k2eAgFnSc1d1
bradavice	bradavice	k1gFnSc1
(	(	kIx(
<g/>
lidský	lidský	k2eAgInSc1d1
papilomavirus	papilomavirus	k1gInSc1
(	(	kIx(
<g/>
HPV	HPV	kA
<g/>
))	))	k?
•	•	k?
hepatitida	hepatitida	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
HBV	HBV	kA
<g/>
)	)	kIx)
•	•	k?
herpes	herpes	k1gInSc1
simplex	simplex	k1gInSc1
(	(	kIx(
<g/>
HSV	HSV	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
HSV	HSV	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
molluscum	molluscum	k1gInSc1
contagiosum	contagiosum	k1gInSc1
(	(	kIx(
<g/>
MCV	MCV	kA
<g/>
)	)	kIx)
bakteriální	bakteriální	k2eAgFnSc4d1
</s>
<s>
měkký	měkký	k2eAgInSc1d1
vřed	vřed	k1gInSc1
(	(	kIx(
<g/>
Haemophilus	Haemophilus	k1gInSc1
ducreyi	ducrey	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
chlamydióza	chlamydióza	k1gFnSc1
/	/	kIx~
lymfogranuloma	lymfogranuloma	k1gFnSc1
venereum	venereum	k1gInSc1
(	(	kIx(
<g/>
Chlamydia	Chlamydium	k1gNnSc2
trachomatis	trachomatis	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
granuloma	granuloma	k1gFnSc1
inguinale	inguinale	k6eAd1
(	(	kIx(
<g/>
Klebsiella	Klebsiella	k1gFnSc1
granulomatis	granulomatis	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
kapavka	kapavka	k1gFnSc1
(	(	kIx(
<g/>
Neisseria	Neisserium	k1gNnPc1
gonorrhoeae	gonorrhoeae	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
syfilis	syfilis	k1gFnSc1
(	(	kIx(
<g/>
Treponema	Treponema	k1gFnSc1
pallidum	pallidum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
ureaplasmatická	ureaplasmatický	k2eAgFnSc1d1
infekce	infekce	k1gFnSc1
(	(	kIx(
<g/>
Ureaplasma	Ureaplasma	k1gFnSc1
urealyticum	urealyticum	k1gInSc1
<g/>
)	)	kIx)
protozoální	protozoálnit	k5eAaPmIp3nS
</s>
<s>
trichomoniáza	trichomoniáza	k1gFnSc1
(	(	kIx(
<g/>
Trichomonas	Trichomonas	k1gInSc1
vaginalis	vaginalis	k1gFnSc2
<g/>
)	)	kIx)
parazitické	parazitický	k2eAgFnSc2d1
</s>
<s>
veš	veš	k1gFnSc1
muňka	muňka	k?
•	•	k?
svrab	svrab	k1gInSc1
zánět	zánět	k1gInSc1
</s>
<s>
ženy	žena	k1gFnPc1
</s>
<s>
cervicitida	cervicitida	k1gFnSc1
•	•	k?
hluboký	hluboký	k2eAgInSc1d1
pánevní	pánevní	k2eAgInSc1d1
zánět	zánět	k1gInSc1
(	(	kIx(
<g/>
PID	PID	kA
<g/>
)	)	kIx)
muži	muž	k1gMnSc3
</s>
<s>
epididymitida	epididymitida	k1gFnSc1
•	•	k?
prostatitida	prostatitida	k1gFnSc1
</s>
<s>
proktitida	proktitida	k1gFnSc1
•	•	k?
uretritida	uretritida	k1gFnSc1
(	(	kIx(
<g/>
nekapavčitá	kapavčitý	k2eNgFnSc1d1
<g/>
,	,	kIx,
NGU	NGU	kA
<g/>
)	)	kIx)
důsledky	důsledek	k1gInPc1
</s>
<s>
mimoděložní	mimoděložní	k2eAgNnSc1d1
těhotenství	těhotenství	k1gNnSc1
•	•	k?
předčasný	předčasný	k2eAgInSc1d1
porod	porod	k1gInSc1
•	•	k?
neplodnost	neplodnost	k1gFnSc1
•	•	k?
reakční	reakční	k2eAgFnPc4d1
artropatie	artropatie	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4200792-6	4200792-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
86004391	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
86004391	#num#	k4
</s>
