<p>
<s>
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
裕	裕	k?	裕
<g/>
;	;	kIx,	;
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1901	[number]	k4	1901
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
124	[number]	k4	124
<g/>
.	.	kIx.	.
japonský	japonský	k2eAgMnSc1d1	japonský
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926	[number]	k4	1926
až	až	k9	až
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
císař	císař	k1gMnSc1	císař
Šówa	Šówa	k1gMnSc1	Šówa
(	(	kIx(	(
<g/>
昭	昭	k?	昭
天	天	k?	天
<g/>
,	,	kIx,	,
Šówa	Šówa	k1gMnSc1	Šówa
tennó	tennó	k1gMnSc1	tennó
<g/>
;	;	kIx,	;
Šówa	Šówa	k1gMnSc1	Šówa
–	–	k?	–
"	"	kIx"	"
<g/>
zářící	zářící	k2eAgInSc1d1	zářící
mír	mír	k1gInSc1	mír
<g/>
"	"	kIx"	"
–	–	k?	–
byl	být	k5eAaImAgMnS	být
název	název	k1gInSc4	název
jeho	jeho	k3xOp3gFnSc2	jeho
panovnické	panovnický	k2eAgFnSc2d1	panovnická
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
volí	volit	k5eAaImIp3nS	volit
každý	každý	k3xTgMnSc1	každý
císař	císař	k1gMnSc1	císař
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Japonsko	Japonsko	k1gNnSc4	Japonsko
bývá	bývat	k5eAaImIp3nS	bývat
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Hirohito	Hirohit	k2eAgNnSc4d1	Hirohito
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dětské	dětský	k2eAgNnSc1d1	dětské
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Miči	Miči	k1gNnSc4	Miči
no	no	k9	no
mija	mija	k6eAd1	mija
(	(	kIx(	(
<g/>
迪	迪	k?	迪
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
Miči	Mič	k1gMnPc1	Mič
no	no	k9	no
mija	mija	k6eAd1	mija
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
Šinnó	Šinnó	k1gFnSc4	Šinnó
(	(	kIx(	(
<g/>
迪	迪	k?	迪
裕	裕	k?	裕
親	親	k?	親
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
nejdéle	dlouho	k6eAd3	dlouho
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
japonských	japonský	k2eAgMnPc2d1	japonský
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
přinesla	přinést	k5eAaPmAgFnS	přinést
japonské	japonský	k2eAgFnPc4d1	japonská
společnosti	společnost	k1gFnPc4	společnost
obrovské	obrovský	k2eAgFnPc4d1	obrovská
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Aojama	Aojamum	k1gNnSc2	Aojamum
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
syn	syn	k1gMnSc1	syn
císaře	císař	k1gMnSc2	císař
Taišó	Taišó	k1gFnSc2	Taišó
Jošihity	Jošihita	k1gFnSc2	Jošihita
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1916	[number]	k4	1916
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
korunním	korunní	k2eAgMnSc7d1	korunní
princem	princ	k1gMnSc7	princ
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1921	[number]	k4	1921
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
regentem	regens	k1gMnSc7	regens
a	a	k8xC	a
ujal	ujmout	k5eAaPmAgInS	ujmout
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
místo	místo	k1gNnSc1	místo
svého	svůj	k3xOyFgMnSc4	svůj
těžce	těžce	k6eAd1	těžce
nemocného	mocný	k2eNgMnSc4d1	nemocný
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
podnikl	podniknout	k5eAaPmAgMnS	podniknout
šestiměsíční	šestiměsíční	k2eAgFnSc4d1	šestiměsíční
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc7	první
japonským	japonský	k2eAgMnSc7d1	japonský
vládcem	vládce	k1gMnSc7	vládce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kdy	kdy	k6eAd1	kdy
vycestoval	vycestovat	k5eAaPmAgMnS	vycestovat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Korunovace	korunovace	k1gFnSc2	korunovace
==	==	k?	==
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1926	[number]	k4	1926
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Korunován	korunován	k2eAgInSc1d1	korunován
císařem	císař	k1gMnSc7	císař
byl	být	k5eAaImAgInS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Role	role	k1gFnSc1	role
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Počátek	počátek	k1gInSc1	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1928	[number]	k4	1928
a	a	k8xC	a
1945	[number]	k4	1945
je	on	k3xPp3gInPc4	on
poznamenám	poznamenat	k5eAaPmIp1nS	poznamenat
silným	silný	k2eAgNnSc7d1	silné
postavením	postavení	k1gNnSc7	postavení
japonských	japonský	k2eAgInPc2d1	japonský
armádních	armádní	k2eAgInPc2d1	armádní
kruhů	kruh	k1gInPc2	kruh
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
faktické	faktický	k2eAgFnSc3d1	faktická
ztrátě	ztráta	k1gFnSc3	ztráta
civilní	civilní	k2eAgFnSc2d1	civilní
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
téměř	téměř	k6eAd1	téměř
veškerou	veškerý	k3xTgFnSc4	veškerý
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
agresivní	agresivní	k2eAgFnSc4d1	agresivní
válku	válka	k1gFnSc4	válka
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
zavedla	zavést	k5eAaPmAgFnS	zavést
Japonsko	Japonsko	k1gNnSc4	Japonsko
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
výzkumů	výzkum	k1gInPc2	výzkum
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
středem	středem	k7c2	středem
faktického	faktický	k2eAgNnSc2d1	faktické
politického	politický	k2eAgNnSc2d1	politické
dění	dění	k1gNnSc2	dění
a	a	k8xC	a
události	událost	k1gFnSc2	událost
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
k	k	k7c3	k
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
následné	následný	k2eAgInPc1d1	následný
masakry	masakr	k1gInPc1	masakr
a	a	k8xC	a
vstup	vstup	k1gInSc1	vstup
Japonska	Japonsko	k1gNnSc2	Japonsko
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
děly	dít	k5eAaImAgFnP	dít
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
přímou	přímý	k2eAgFnSc7d1	přímá
kontrolou	kontrola	k1gFnSc7	kontrola
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
souhlasem	souhlas	k1gInSc7	souhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
Šigeko	Šigeko	k1gNnSc4	Šigeko
(	(	kIx(	(
<g/>
Teru	Terus	k1gInSc2	Terus
no	no	k9	no
mija	mija	k6eAd1	mija
Šigeko	Šigeko	k1gNnSc1	Šigeko
Naišinnó	Naišinnó	k1gFnSc2	Naišinnó
照	照	k?	照
成	成	k?	成
内	内	k?	内
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
Sačiko	Sačika	k1gFnSc5	Sačika
(	(	kIx(	(
<g/>
Hisa	Hisus	k1gMnSc2	Hisus
no	no	k9	no
mija	mija	k6eAd1	mija
Sačiko	Sačika	k1gFnSc5	Sačika
Naišinnó	Naišinnó	k1gMnPc3	Naišinnó
久	久	k?	久
祐	祐	k?	祐
内	内	k?	内
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
Kazuko	Kazuko	k1gNnSc4	Kazuko
(	(	kIx(	(
<g/>
Taka	Taka	k1gFnSc1	Taka
no	no	k9	no
mija	mija	k6eAd1	mija
Kazuko	Kazuko	k1gNnSc1	Kazuko
Naišinnó	Naišinnó	k1gFnSc2	Naišinnó
孝	孝	k?	孝
和	和	k?	和
内	内	k?	内
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
Acuko	Acuko	k1gNnSc4	Acuko
(	(	kIx(	(
<g/>
Jori	Jori	k1gNnSc4	Jori
no	no	k9	no
mija	mija	k6eAd1	mija
Acuko	Acuko	k1gNnSc1	Acuko
Naišinnó	Naišinnó	k1gFnSc2	Naišinnó
順	順	k?	順
厚	厚	k?	厚
内	内	k?	内
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Akihito	Akihit	k2eAgNnSc4d1	Akihito
(	(	kIx(	(
<g/>
Cugu	Cuga	k1gFnSc4	Cuga
no	no	k9	no
mija	mija	k6eAd1	mija
Akihito	Akihit	k2eAgNnSc1d1	Akihito
Šinnó	Šinnó	k1gFnSc4	Šinnó
継	継	k?	継
明	明	k?	明
親	親	k?	親
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Princ	princ	k1gMnSc1	princ
Masahito	Masahit	k2eAgNnSc1d1	Masahit
(	(	kIx(	(
<g/>
Hitači	Hitač	k1gMnSc3	Hitač
no	no	k9	no
mija	mija	k6eAd1	mija
Masahito	Masahit	k2eAgNnSc1d1	Masahit
Šinnó	Šinnó	k1gFnSc4	Šinnó
常	常	k?	常
正	正	k?	正
親	親	k?	親
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
Takako	Takako	k1gNnSc4	Takako
(	(	kIx(	(
<g/>
Suga	Suga	k1gFnSc1	Suga
no	no	k9	no
mija	mija	k6eAd1	mija
Takako	Takako	k1gNnSc1	Takako
Naišinnó	Naišinnó	k1gFnSc2	Naišinnó
清	清	k?	清
貴	貴	k?	貴
内	内	k?	内
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BIX	BIX	kA	BIX
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
P.	P.	kA	P.
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
moderního	moderní	k2eAgNnSc2d1	moderní
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7257	[number]	k4	7257
<g/>
-	-	kIx~	-
<g/>
712	[number]	k4	712
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
599	[number]	k4	599
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HATA	HATA	kA	HATA
<g/>
,	,	kIx,	,
Ikuhiko	Ikuhika	k1gFnSc5	Ikuhika
<g/>
;	;	kIx,	;
JANSEN	JANSEN	kA	JANSEN
<g/>
,	,	kIx,	,
Marius	Marius	k1gMnSc1	Marius
B.	B.	kA	B.
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Shō	Shō	k1gMnSc1	Shō
Emperor	Emperor	k1gMnSc1	Emperor
in	in	k?	in
War	War	k1gMnSc1	War
and	and	k?	and
Peace	Peace	k1gMnSc1	Peace
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Global	globat	k5eAaImAgMnS	globat
Oriental	Oriental	k1gMnSc1	Oriental
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
</s>
</p>
<p>
<s>
DOSTÁLOVÁ	Dostálová	k1gFnSc1	Dostálová
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Hirohito	Hirohit	k2eAgNnSc4d1	Hirohito
-	-	kIx~	-
zhroucení	zhroucení	k1gNnSc4	zhroucení
božského	božský	k2eAgInSc2d1	božský
mýtu	mýtus	k1gInSc2	mýtus
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Kunaicho	Kunaicho	k6eAd1	Kunaicho
|	|	kIx~	|
Emperor	Emperor	k1gInSc1	Emperor
Shō	Shō	k1gFnSc2	Shō
and	and	k?	and
Empress	Empress	k1gInSc1	Empress
Kō	Kō	k1gFnSc2	Kō
</s>
</p>
<p>
<s>
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
biography	biograph	k1gInPc1	biograph
and	and	k?	and
timeline	timelin	k1gInSc5	timelin
v	v	k7c4	v
Rotten	Rotten	k2eAgInSc4d1	Rotten
Library	Librar	k1gInPc4	Librar
</s>
</p>
<p>
<s>
Reflections	Reflections	k6eAd1	Reflections
on	on	k3xPp3gMnSc1	on
Emperor	Emperor	k1gMnSc1	Emperor
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
death	death	k1gInSc1	death
</s>
</p>
<p>
<s>
Booknotes	Booknotes	k1gMnSc1	Booknotes
interview	interview	k1gNnSc2	interview
with	with	k1gMnSc1	with
Herbert	Herbert	k1gMnSc1	Herbert
Bix	Bix	k1gMnSc1	Bix
on	on	k3xPp3gMnSc1	on
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
and	and	k?	and
the	the	k?	the
Making	Making	k1gInSc1	Making
of	of	k?	of
Modern	Modern	k1gInSc1	Modern
Japan	japan	k1gInSc1	japan
<g/>
,	,	kIx,	,
September	September	k1gInSc1	September
2	[number]	k4	2
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Booknotes	Booknotes	k1gMnSc1	Booknotes
interview	interview	k1gNnSc2	interview
with	with	k1gMnSc1	with
John	John	k1gMnSc1	John
Dower	Dower	k1gMnSc1	Dower
on	on	k3xPp3gMnSc1	on
Embracing	Embracing	k1gInSc1	Embracing
Defeat	Defeat	k1gInSc1	Defeat
<g/>
,	,	kIx,	,
March	March	k1gInSc1	March
26	[number]	k4	26
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
</s>
</p>
<p>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
cesta	cesta	k1gFnSc1	cesta
1945-1970	[number]	k4	1945-1970
-	-	kIx~	-
proč	proč	k6eAd1	proč
císař	císař	k1gMnSc1	císař
Hirohito	Hirohita	k1gMnSc5	Hirohita
unikl	uniknout	k5eAaPmAgMnS	uniknout
válečnému	válečný	k2eAgInSc3d1	válečný
soudu	soud	k1gInSc3	soud
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
<g/>
.	.	kIx.	.
</s>
<s>
Bál	bál	k1gInSc1	bál
se	se	k3xPyFc4	se
amerických	americký	k2eAgFnPc2d1	americká
atomových	atomový	k2eAgFnPc2d1	atomová
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Sovětů	Sovět	k1gMnPc2	Sovět
<g/>
?	?	kIx.	?
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
detailní	detailní	k2eAgNnSc4d1	detailní
životopisné	životopisný	k2eAgFnPc4d1	životopisná
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
podrobnosti	podrobnost	k1gFnPc4	podrobnost
o	o	k7c6	o
císařově	císařův	k2eAgFnSc6d1	císařova
postojích	postoj	k1gInPc6	postoj
během	během	k7c2	během
války	válka	k1gFnSc2	válka
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
</p>
