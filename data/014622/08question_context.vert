<s>
Led	led	k1gInSc1
vzniklý	vzniklý	k2eAgInSc1d1
přirozeně	přirozeně	k6eAd1
</s>
<s>
Některé	některý	k3yIgFnPc1
amorfní	amorfní	k2eAgFnPc1d1
látky	látka	k1gFnPc1
(	(	kIx(
<g/>
např.	např.	kA
opál	opál	k1gInSc1
<g/>
,	,	kIx,
georgeit	georgeit	k1gMnSc1
<g/>
,	,	kIx,
kalciouranoit	kalciouranoit	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Látky	látka	k1gFnPc1
pocházející	pocházející	k2eAgFnPc1d1
z	z	k7c2
jiných	jiný	k2eAgNnPc2d1
kosmických	kosmický	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
(	(	kIx(
<g/>
Měsíc	měsíc	k1gInSc1
<g/>
,	,	kIx,
Mars	Mars	k1gInSc1
<g/>
,	,	kIx,
meteority	meteorit	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Homogenní	homogenní	k2eAgFnPc1d1
biogenní	biogenní	k2eAgFnPc1d1
látky	látka	k1gFnPc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
formování	formování	k1gNnSc6
podílely	podílet	k5eAaImAgInP
geologické	geologický	k2eAgInPc1d1
procesy	proces	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
minerály	minerál	k1gInPc4
guana	guana	k1gFnSc1
<g/>
)	)	kIx)
</s>