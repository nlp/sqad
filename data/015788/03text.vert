<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Cesta	cesta	k1gFnSc1
k	k	k7c3
jednání	jednání	k1gNnSc3
o	o	k7c4
kapitulaci	kapitulace	k1gFnSc4
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Singapuru	Singapur	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
<g/>
Zleva	zleva	k6eAd1
doprava	doprava	k1gFnSc1
<g/>
:	:	kIx,
Major	major	k1gMnSc1
Cyril	Cyril	k1gMnSc1
Wild	Wild	k1gMnSc1
(	(	kIx(
<g/>
s	s	k7c7
bílou	bílý	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Newbigging	Newbigging	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
vlajkou	vlajka	k1gFnSc7
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podplukovník	podplukovník	k1gMnSc1
Ichiji	Ichiji	k1gMnSc1
Sugita	Sugita	k1gMnSc1
<g/>
,	,	kIx,
brigádní	brigádní	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Torrance	Torrance	k1gFnSc2
a	a	k8xC
generál	generál	k1gMnSc1
Arthur	Arthur	k1gMnSc1
Percival	Percival	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
kapitulaci	kapitulace	k1gFnSc4
sil	síla	k1gFnPc2
vedených	vedený	k2eAgFnPc2d1
Brity	Brit	k1gMnPc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Singapur	Singapur	k1gInSc1
<g/>
,	,	kIx,
Průlivové	Průlivový	k2eAgFnSc2d1
osady	osada	k1gFnSc2
</s>
<s>
zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
103	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
a	a	k8xC
okupace	okupace	k1gFnSc1
Singapuru	Singapur	k1gInSc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
IndieAustrálie	IndieAustrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
Spojené	spojený	k2eAgNnSc4d1
království	království	k1gNnSc4
Britské	britský	k2eAgNnSc1d1
Malajsko	Malajsko	k1gNnSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Arthur	Arthur	k1gMnSc1
E.	E.	kA
Percival	Percival	k1gMnSc1
H.	H.	kA
Gordon	Gordon	k1gMnSc1
Bennett	Bennett	k1gMnSc1
Lewis	Lewis	k1gFnSc2
M.	M.	kA
HeathMerton	HeathMerton	k1gInSc1
Beckwith-Smith	Beckwith-Smith	k1gInSc1
</s>
<s>
Tomojuki	Tomojuki	k6eAd1
Jamašita	Jamašit	k2eAgFnSc1d1
Takuma	Takuma	k1gFnSc1
Nišimura	Nišimura	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Takuro	Takura	k1gFnSc5
Macui	Macui	k1gNnPc4
Renja	Renj	k1gInSc2
Mutaguči	Mutaguč	k1gFnSc3
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
85	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
400	#num#	k4
děl	dělo	k1gNnPc2
</s>
<s>
36	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
200	#num#	k4
děl	dělo	k1gNnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
134	#num#	k4
5009	#num#	k4
500	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
<g/>
5	#num#	k4
000	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
<g/>
120	#num#	k4
000	#num#	k4
zajatých	zajatá	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
4851	#num#	k4
713	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
<g/>
2	#num#	k4
772	#num#	k4
zraněných	zraněný	k2eAgMnPc2d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
je	být	k5eAaImIp3nS
pojmenováním	pojmenování	k1gNnSc7
válečných	válečný	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgFnP
na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1941	#num#	k4
a	a	k8xC
1942	#num#	k4
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
bojů	boj	k1gInPc2
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
Singapur	Singapur	k1gInSc1
hlavním	hlavní	k2eAgMnSc7d1
britským	britský	k2eAgInSc7d1
opěrným	opěrný	k2eAgInSc7d1
bodem	bod	k1gInSc7
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
strategická	strategický	k2eAgFnSc1d1
důležitost	důležitost	k1gFnSc1
tohoto	tento	k3xDgNnSc2
místa	místo	k1gNnSc2
byla	být	k5eAaImAgFnS
zřejmá	zřejmý	k2eAgFnSc1d1
nejen	nejen	k6eAd1
Spojencům	spojenec	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
představitelům	představitel	k1gMnPc3
Osy	osa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgFnPc4d1
snahy	snaha	k1gFnPc4
o	o	k7c6
získáni	získat	k5eAaPmNgMnP
tohoto	tento	k3xDgNnSc2
města	město	k1gNnSc2
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
již	již	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
Singapur	Singapur	k1gInSc1
poprvé	poprvé	k6eAd1
bombardován	bombardován	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
byly	být	k5eAaImAgFnP
završeny	završit	k5eAaPmNgFnP
mezi	mezi	k7c4
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
únorem	únor	k1gInSc7
1942	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Singapur	Singapur	k1gInSc1
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
do	do	k7c2
japonských	japonský	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vylodění	vylodění	k1gNnSc6
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
se	se	k3xPyFc4
početně	početně	k6eAd1
slabší	slabý	k2eAgNnSc1d2
japonské	japonský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
vydalo	vydat	k5eAaPmAgNnS
na	na	k7c4
rychlý	rychlý	k2eAgInSc4d1
postup	postup	k1gInSc4
až	až	k9
k	k	k7c3
Singapuru	Singapur	k1gInSc3
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
dosáhli	dosáhnout	k5eAaPmAgMnP
počátkem	počátkem	k7c2
února	únor	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
na	na	k7c4
město	město	k1gNnSc4
samotné	samotný	k2eAgFnSc2d1
začal	začít	k5eAaPmAgInS
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
večer	večer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britští	britský	k2eAgMnPc1d1
obránci	obránce	k1gMnPc1
byli	být	k5eAaImAgMnP
takovýmto	takovýto	k3xDgInSc7
vývojem	vývoj	k1gInSc7
zaskočeni	zaskočit	k5eAaPmNgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
nepočítali	počítat	k5eNaImAgMnP
s	s	k7c7
možností	možnost	k1gFnSc7
útoku	útok	k1gInSc2
z	z	k7c2
týlu	týl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
dnech	den	k1gInPc6
bojů	boj	k1gInPc2
<g/>
,	,	kIx,
Singapur	Singapur	k1gInSc1
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
kapituloval	kapitulovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
zajetí	zajetí	k1gNnSc2
putovalo	putovat	k5eAaImAgNnS
na	na	k7c4
80	#num#	k4
000	#num#	k4
australských	australský	k2eAgMnPc2d1
<g/>
,	,	kIx,
britských	britský	k2eAgMnPc2d1
a	a	k8xC
indických	indický	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
přidali	přidat	k5eAaPmAgMnP
k	k	k7c3
dalším	další	k2eAgFnPc3d1
50	#num#	k4
000	#num#	k4
svých	svůj	k3xOyFgMnPc2
kolegů	kolega	k1gMnPc2
zajatých	zajatý	k1gMnPc2
Japonci	Japonec	k1gMnPc1
během	během	k7c2
jejich	jejich	k3xOp3gNnSc2
malajského	malajský	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Winston	Winston	k1gInSc1
Churchill	Churchill	k1gInSc4
<g/>
,	,	kIx,
britský	britský	k2eAgMnSc1d1
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
<g/>
,	,	kIx,
označil	označit	k5eAaPmAgMnS
pád	pád	k1gInSc4
Singapuru	Singapur	k1gInSc2
za	za	k7c4
„	„	k?
<g/>
největší	veliký	k2eAgFnSc4d3
katastrofu	katastrofa	k1gFnSc4
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
největší	veliký	k2eAgFnSc4d3
kapitulaci	kapitulace	k1gFnSc4
<g/>
“	“	k?
v	v	k7c6
dějinách	dějiny	k1gFnPc6
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavostí	zajímavost	k1gFnPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
obrany	obrana	k1gFnSc2
Singapuru	Singapur	k1gInSc2
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
i	i	k9
menší	malý	k2eAgNnSc1d2
množství	množství	k1gNnSc1
československých	československý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důležitost	důležitost	k1gFnSc1
Singapuru	Singapur	k1gInSc2
</s>
<s>
Singapur	Singapur	k1gInSc4
patřil	patřit	k5eAaImAgMnS
mezi	mezi	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejvýznamnějších	významný	k2eAgFnPc2d3
britských	britský	k2eAgFnPc2d1
základen	základna	k1gFnPc2
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
klenot	klenot	k1gInSc4
v	v	k7c6
koruně	koruna	k1gFnSc6
Impéria	impérium	k1gNnSc2
umožňoval	umožňovat	k5eAaImAgInS
Britům	Brit	k1gMnPc3
ovládat	ovládat	k5eAaImF
důležité	důležitý	k2eAgFnPc4d1
lodní	lodní	k2eAgFnPc4d1
linky	linka	k1gFnPc4
vedoucí	vedoucí	k1gMnPc1
podél	podél	k7c2
jižního	jižní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Malajsie	Malajsie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
jelikož	jelikož	k8xS
právě	právě	k9
touto	tento	k3xDgFnSc7
oblastí	oblast	k1gFnSc7
vedly	vést	k5eAaImAgInP
lodní	lodní	k2eAgInPc1d1
trasy	tras	k1gInPc1
zajišťující	zajišťující	k2eAgInSc1d1
obchod	obchod	k1gInSc4
na	na	k7c6
obrovských	obrovský	k2eAgInPc6d1
trzích	trh	k1gInPc6
jihozápadní	jihozápadní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
japonským	japonský	k2eAgMnPc3d1
představitelům	představitel	k1gMnPc3
jasné	jasný	k2eAgNnSc4d1
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
chtějí	chtít	k5eAaImIp3nP
oslabit	oslabit	k5eAaPmF
britský	britský	k2eAgInSc4d1
vliv	vliv	k1gInSc4
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
se	se	k3xPyFc4
Singapuru	Singapur	k1gInSc6
zmocnit	zmocnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Přípravy	příprava	k1gFnPc1
k	k	k7c3
útoku	útok	k1gInSc3
</s>
<s>
Japonská	japonský	k2eAgNnPc1d1
vylodění	vylodění	k1gNnPc1
</s>
<s>
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Malajsie	Malajsie	k1gFnSc2
byla	být	k5eAaImAgFnS
situace	situace	k1gFnSc1
v	v	k7c6
Singapuru	Singapur	k1gInSc6
pro	pro	k7c4
Brity	Brit	k1gMnPc4
kritická	kritický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostrově	ostrov	k1gInSc6
bylo	být	k5eAaImAgNnS
sice	sice	k8xC
dislokováno	dislokován	k2eAgNnSc1d1
okolo	okolo	k7c2
100	#num#	k4
000	#num#	k4
britských	britský	k2eAgMnPc2d1
<g/>
,	,	kIx,
australských	australský	k2eAgInPc2d1
<g/>
,	,	kIx,
holandských	holandský	k2eAgInPc2d1
<g/>
,	,	kIx,
indických	indický	k2eAgInPc2d1
a	a	k8xC
malajských	malajský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
měli	mít	k5eAaImAgMnP
dostatek	dostatek	k1gInSc4
munice	munice	k1gFnSc2
<g/>
,	,	kIx,
paliva	palivo	k1gNnSc2
a	a	k8xC
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
prospěch	prospěch	k1gInSc4
Japonců	Japonec	k1gMnPc2
však	však	k9
hrála	hrát	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
vzdušná	vzdušný	k2eAgFnSc1d1
převaha	převaha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
vyčleněné	vyčleněný	k2eAgFnSc2d1
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
operaci	operace	k1gFnSc4
čítalo	čítat	k5eAaImAgNnS
na	na	k7c4
500	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
mohla	moct	k5eAaImAgFnS
útočit	útočit	k5eAaImF
z	z	k7c2
letišť	letiště	k1gNnPc2
vzdálených	vzdálený	k2eAgInPc2d1
pouhých	pouhý	k2eAgInPc2d1
20	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
Singapuru	Singapur	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgMnSc3
Britové	Brit	k1gMnPc1
disponovali	disponovat	k5eAaBmAgMnP
pouhými	pouhý	k2eAgNnPc7d1
50	#num#	k4
letadly	letadlo	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
navíc	navíc	k6eAd1
byla	být	k5eAaImAgFnS
zastaralejší	zastaralý	k2eAgInPc4d2
než	než	k8xS
stroje	stroj	k1gInPc1
dobře	dobře	k6eAd1
vycvičených	vycvičený	k2eAgInPc2d1
japonských	japonský	k2eAgInPc2d1
pilotů	pilot	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
obrany	obrana	k1gFnSc2
Singapuru	Singapur	k1gInSc2
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
Percival	Percival	k1gMnSc1
<g/>
,	,	kIx,
sice	sice	k8xC
letectvo	letectvo	k1gNnSc4
žádal	žádat	k5eAaImAgMnS
o	o	k7c4
větší	veliký	k2eAgFnSc4d2
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnPc1
prosby	prosba	k1gFnPc1
nebyly	být	k5eNaImAgFnP
vyslyšeny	vyslyšet	k5eAaPmNgFnP
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
přednost	přednost	k1gFnSc4
dostala	dostat	k5eAaPmAgFnS
dodávka	dodávka	k1gFnSc1
600	#num#	k4
letadel	letadlo	k1gNnPc2
sovětské	sovětský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Japonci	Japonec	k1gMnPc1
svojí	svojit	k5eAaImIp3nP
leteckou	letecký	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
skvěle	skvěle	k6eAd1
uplatnili	uplatnit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neustále	neustále	k6eAd1
byly	být	k5eAaImAgInP
vysílány	vysílat	k5eAaImNgInP
bombardéry	bombardér	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
zasypávaly	zasypávat	k5eAaImAgInP
britské	britský	k2eAgFnSc3d1
pozice	pozice	k1gFnSc1
pumami	puma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bombardování	bombardování	k1gNnPc1
spojeneckých	spojenecký	k2eAgFnPc2d1
obranných	obranný	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
ničilo	ničit	k5eAaImAgNnS
spojenecké	spojenecký	k2eAgFnPc4d1
obranné	obranný	k2eAgFnPc4d1
linie	linie	k1gFnPc4
a	a	k8xC
výrazně	výrazně	k6eAd1
narušilo	narušit	k5eAaPmAgNnS
morálku	morálka	k1gFnSc4
spojeneckých	spojenecký	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
účelem	účel	k1gInSc7
prohloubení	prohloubení	k1gNnSc2
devastujících	devastující	k2eAgInPc2d1
účinků	účinek	k1gInPc2
byl	být	k5eAaImAgInS
Singapur	Singapur	k1gInSc1
před	před	k7c7
samotným	samotný	k2eAgInSc7d1
útokem	útok	k1gInSc7
vystaven	vystavit	k5eAaPmNgInS
rovněž	rovněž	k9
těžké	těžký	k2eAgFnSc6d1
dělostřelecké	dělostřelecký	k2eAgFnSc6d1
palbě	palba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
město	město	k1gNnSc4
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
nálet	nálet	k1gInSc1
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
</s>
<s>
Velitel	velitel	k1gMnSc1
japonského	japonský	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
,	,	kIx,
Tomojuki	Tomojuk	k1gFnSc2
Jamašita	Jamašit	k2eAgFnSc1d1
<g/>
,	,	kIx,
naplánoval	naplánovat	k5eAaBmAgMnS
lest	lest	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
útočícím	útočící	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
dobytí	dobytí	k1gNnSc4
Singapuru	Singapur	k1gInSc2
usnadnit	usnadnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schválně	schválně	k6eAd1
dal	dát	k5eAaPmAgInS
bombardovat	bombardovat	k5eAaImF
východní	východní	k2eAgFnSc4d1
část	část	k1gFnSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
Percival	Percival	k1gMnSc1
myslel	myslet	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Japonci	Japonec	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
útočit	útočit	k5eAaImF
od	od	k7c2
východu	východ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
tuto	tento	k3xDgFnSc4
lest	lest	k1gFnSc4
neodhalili	odhalit	k5eNaPmAgMnP
a	a	k8xC
přesunem	přesun	k1gInSc7
vojsk	vojsko	k1gNnPc2
na	na	k7c4
východ	východ	k1gInSc4
značně	značně	k6eAd1
oslabili	oslabit	k5eAaPmAgMnP
obranu	obrana	k1gFnSc4
v	v	k7c6
ostatních	ostatní	k2eAgFnPc6d1
částech	část	k1gFnPc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc1
omyl	omyl	k1gInSc4
si	se	k3xPyFc3
uvědomili	uvědomit	k5eAaPmAgMnP
až	až	k9
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
mohutném	mohutný	k2eAgNnSc6d1
bombardování	bombardování	k1gNnSc6
<g/>
,	,	kIx,
vylodilo	vylodit	k5eAaPmAgNnS
16	#num#	k4
japonských	japonský	k2eAgInPc2d1
praporů	prapor	k1gInPc2
na	na	k7c4
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
ostrova	ostrov	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
bránily	bránit	k5eAaImAgFnP
jen	jen	k6eAd1
3	#num#	k4
australské	australský	k2eAgInPc1d1
prapory	prapor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
za	za	k7c2
rozbřesku	rozbřesk	k1gInSc2
drželi	držet	k5eAaImAgMnP
Jamašitovi	Jamašitův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
předmostí	předmostí	k1gNnSc2
o	o	k7c6
síle	síla	k1gFnSc6
tisíců	tisíc	k4xCgInPc2
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
odpoledne	odpoledne	k1gNnSc2
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
Singapuru	Singapur	k1gInSc6
nacházelo	nacházet	k5eAaImAgNnS
na	na	k7c4
30	#num#	k4
000	#num#	k4
japonských	japonský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
a	a	k8xC
několik	několik	k4yIc4
tanků	tank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrana	obrana	k1gFnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
sektoru	sektor	k1gInSc6
byla	být	k5eAaImAgFnS
svěřena	svěřen	k2eAgFnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
australské	australský	k2eAgFnSc3d1
brigádě	brigáda	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
Japonci	Japonec	k1gMnPc1
brzy	brzy	k6eAd1
rozprášena	rozprášen	k2eAgFnSc1d1
a	a	k8xC
městu	město	k1gNnSc3
začalo	začít	k5eAaPmAgNnS
hrozit	hrozit	k5eAaImF
obklíčení	obklíčení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonský	japonský	k2eAgInSc1d1
nápor	nápor	k1gInSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
zaměřil	zaměřit	k5eAaPmAgMnS
na	na	k7c4
výšiny	výšina	k1gFnPc4
Bukit	Bukit	k2eAgInSc1d1
Timah	Timah	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nalézaly	nalézat	k5eAaImAgFnP
vodní	vodní	k2eAgFnPc1d1
nádrže	nádrž	k1gFnPc1
a	a	k8xC
sklady	sklad	k1gInPc1
munice	munice	k1gFnSc2
a	a	k8xC
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpoutala	rozpoutat	k5eAaPmAgFnS
se	se	k3xPyFc4
divoká	divoký	k2eAgFnSc1d1
a	a	k8xC
krvavá	krvavý	k2eAgFnSc1d1
řež	řež	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
protáhla	protáhnout	k5eAaPmAgFnS
na	na	k7c4
několik	několik	k4yIc4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
využili	využít	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
letecké	letecký	k2eAgFnPc4d1
převahy	převaha	k1gFnPc4
a	a	k8xC
ostrov	ostrov	k1gInSc4
úporně	úporně	k6eAd1
bombardovali	bombardovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bombardováním	bombardování	k1gNnSc7
si	se	k3xPyFc3
však	však	k9
sami	sám	k3xTgMnPc1
způsobili	způsobit	k5eAaPmAgMnP
velké	velký	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
protože	protože	k8xS
bomby	bomba	k1gFnSc2
zapálily	zapálit	k5eAaPmAgFnP
nádrže	nádrž	k1gFnPc1
s	s	k7c7
naftou	nafta	k1gFnSc7
a	a	k8xC
hořící	hořící	k2eAgFnSc1d1
nafta	nafta	k1gFnSc1
začala	začít	k5eAaPmAgFnS
stékat	stékat	k5eAaImF
k	k	k7c3
moři	moře	k1gNnSc3
a	a	k8xC
cestou	cestou	k7c2
spálila	spálit	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
japonské	japonský	k2eAgFnPc4d1
vojáky	voják	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
v	v	k7c6
daném	daný	k2eAgInSc6d1
úseku	úsek	k1gInSc6
nacházeli	nacházet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
obdržela	obdržet	k5eAaPmAgFnS
od	od	k7c2
Jamašity	Jamašit	k2eAgInPc4d1
slib	slib	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
Singapur	Singapur	k1gInSc1
bude	být	k5eAaImBp3nS
dobyt	dobýt	k5eAaPmNgInS
už	už	k9
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c4
Den	den	k1gInSc4
založení	založení	k1gNnSc2
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
tohoto	tento	k3xDgInSc2
dne	den	k1gInSc2
se	se	k3xPyFc4
Japoncům	Japonec	k1gMnPc3
teprve	teprve	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
dobýt	dobýt	k5eAaPmF
výšiny	výšin	k1gInPc4
Bukit	Bukita	k1gFnPc2
Timah	Timaha	k1gFnPc2
a	a	k8xC
postoupit	postoupit	k5eAaPmF
k	k	k7c3
Singapuru	Singapur	k1gInSc3
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnPc1
postupovaly	postupovat	k5eAaImAgFnP
k	k	k7c3
městu	město	k1gNnSc3
od	od	k7c2
Bukit	Bukit	k1gInSc4
Timahu	Timaha	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
gardová	gardový	k2eAgFnSc1d1
divize	divize	k1gFnSc1
postupovala	postupovat	k5eAaImAgFnS
východněji	východně	k6eAd2
od	od	k7c2
města	město	k1gNnSc2
Nee	Nee	k1gMnSc1
Soon	Soon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obklíčení	obklíčení	k1gNnSc1
Singapuru	Singapur	k1gInSc2
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
bez	bez	k7c2
větších	veliký	k2eAgFnPc2d2
potíží	potíž	k1gFnPc2
povedlo	povést	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Kapitulace	kapitulace	k1gFnSc1
</s>
<s>
Lt	Lt	k?
Gen.	gen.	kA
Jamašita	Jamašit	k2eAgFnSc1d1
(	(	kIx(
<g/>
sedí	sedit	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
<g/>
)	)	kIx)
se	s	k7c7
sevřenými	sevřený	k2eAgFnPc7d1
pěstmi	pěst	k1gFnPc7
u	u	k7c2
stolu	stol	k1gInSc2
vyžaduje	vyžadovat	k5eAaImIp3nS
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1
kapitulaci	kapitulace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lt	Lt	k1gFnSc1
Gen	gen	kA
Percival	Percival	k1gFnPc1
sedí	sedit	k5eAaImIp3nP
mezi	mezi	k7c7
svými	svůj	k3xOyFgMnPc7
důstojníky	důstojník	k1gMnPc7
s	s	k7c7
rukou	ruka	k1gFnSc7
sevřenou	sevřený	k2eAgFnSc7d1
u	u	k7c2
úst	ústa	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Ráno	ráno	k6eAd1
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
poslal	poslat	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Jamašita	Jamašit	k2eAgFnSc1d1
generálu	generál	k1gMnSc3
Percivalovi	Percival	k1gMnSc3
výzvu	výzva	k1gFnSc4
ke	k	k7c3
kapitulaci	kapitulace	k1gFnSc3
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
však	však	k9
zatím	zatím	k6eAd1
přijata	přijat	k2eAgFnSc1d1
nebyla	být	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
masakru	masakr	k1gInSc3
v	v	k7c6
Britské	britský	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Alexandřině	Alexandřin	k2eAgFnSc6d1
<g/>
)	)	kIx)
nemocnici	nemocnice	k1gFnSc6
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
zemřelo	zemřít	k5eAaPmAgNnS
200	#num#	k4
pacientů	pacient	k1gMnPc2
a	a	k8xC
členů	člen	k1gMnPc2
ošetřujícího	ošetřující	k2eAgInSc2d1
personálu	personál	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
převděpodobně	převděpodobně	k6eAd1
československý	československý	k2eAgMnSc1d1
dobrovolník	dobrovolník	k1gMnSc1
<g/>
,	,	kIx,
baťovec	baťovec	k1gMnSc1
Silvestr	Silvestr	k1gMnSc1
Němec	Němec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tradovaným	tradovaný	k2eAgInSc7d1
mýtem	mýtus	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
akci	akce	k1gFnSc4
na	na	k7c4
rozkaz	rozkaz	k1gInSc4
Jamašity	Jamašit	k2eAgFnPc1d1
-	-	kIx~
uchýlit	uchýlit	k5eAaPmF
ke	k	k7c3
zvěrstvům	zvěrstvo	k1gNnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Britům	Brit	k1gMnPc3
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
má	mít	k5eAaImIp3nS
v	v	k7c6
plánu	plán	k1gInSc6
s	s	k7c7
obránci	obránce	k1gMnPc7
pevnosti	pevnost	k1gFnSc2
naložit	naložit	k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
nevzdají	vzdát	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
o	o	k7c6
něm	on	k3xPp3gInSc6
údajně	údajně	k6eAd1
nevěděl	vědět	k5eNaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
ani	ani	k8xC
velitel	velitel	k1gMnSc1
divize	divize	k1gFnSc2
Mutaguči	Mutaguč	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masakr	masakr	k1gInSc4
-	-	kIx~
aslespoň	aslesponit	k5eAaPmRp2nS
jeho	jeho	k3xOp3gFnSc1
první	první	k4xOgFnSc1
část	část	k1gFnSc1
-	-	kIx~
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
v	v	k7c6
zápalu	zápal	k1gInSc6
boje	boj	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
z	z	k7c2
budovy	budova	k1gFnSc2
nemocnice	nemocnice	k1gFnSc2
po	po	k7c6
Japoncích	Japonec	k1gMnPc6
stříleli	střílet	k5eAaImAgMnP
ustupující	ustupující	k2eAgMnPc1d1
indičtí	indický	k2eAgMnPc1d1
sapéři	sapéři	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
nebyl	být	k5eNaImAgInS
po	po	k7c6
poválečném	poválečný	k2eAgNnSc6d1
vyšetřování	vyšetřování	k1gNnSc6
za	za	k7c4
zločin	zločin	k1gInSc4
nikdo	nikdo	k3yNnSc1
potrestán	potrestat	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Percival	Percivat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
však	však	k9
kapitulovat	kapitulovat	k5eAaBmF
nehodlal	hodlat	k5eNaImAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
stále	stále	k6eAd1
věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Japonce	Japonec	k1gMnPc4
dokáže	dokázat	k5eAaPmIp3nS
porazit	porazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
tak	tak	k9
sebevědomý	sebevědomý	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
dokonce	dokonce	k9
zapsal	zapsat	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
naše	náš	k3xOp1gInPc1
oddíly	oddíl	k1gInPc1
na	na	k7c6
Singapurském	singapurský	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
mají	mít	k5eAaImIp3nP
velkou	velký	k2eAgFnSc4d1
početní	početní	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
nad	nad	k7c7
všemi	všecek	k3xTgMnPc7
Japonci	Japonec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
překročili	překročit	k5eAaPmAgMnP
úžinu	úžina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sázce	sázka	k1gFnSc6
je	být	k5eAaImIp3nS
veškerá	veškerý	k3xTgFnSc1
naše	náš	k3xOp1gFnSc1
bojová	bojový	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
a	a	k8xC
čest	čest	k1gFnSc1
Britské	britský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Další	další	k2eAgInPc1d1
boje	boj	k1gInPc1
však	však	k9
byly	být	k5eAaImAgInP
beznadějné	beznadějný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britští	britský	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
poznali	poznat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
schopni	schopen	k2eAgMnPc1d1
porazit	porazit	k5eAaPmF
skvěle	skvěle	k6eAd1
vycvičené	vycvičený	k2eAgMnPc4d1
japonské	japonský	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
navíc	navíc	k6eAd1
měli	mít	k5eAaImAgMnP
válečné	válečný	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
z	z	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disciplína	disciplína	k1gFnSc1
<g/>
,	,	kIx,
spolupráce	spolupráce	k1gFnSc1
<g/>
,	,	kIx,
rozhodnost	rozhodnost	k1gFnSc1
v	v	k7c6
boji	boj	k1gInSc6
a	a	k8xC
vytrvalost	vytrvalost	k1gFnSc1
zajistila	zajistit	k5eAaPmAgFnS
japonským	japonský	k2eAgMnPc3d1
oddílům	oddíl	k1gInPc3
převahu	převah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Britům	Brit	k1gMnPc3
začalo	začít	k5eAaPmAgNnS
docházet	docházet	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gInSc1
boj	boj	k1gInSc1
je	být	k5eAaImIp3nS
marný	marný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrana	obrana	k1gFnSc1
ostrova	ostrov	k1gInSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
rozpadla	rozpadnout	k5eAaPmAgFnS
a	a	k8xC
Brity	Brit	k1gMnPc4
začal	začít	k5eAaPmAgMnS
tížit	tížit	k5eAaImF
nedostatek	nedostatek	k1gInSc4
vody	voda	k1gFnSc2
a	a	k8xC
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
na	na	k7c6
úpatí	úpatí	k1gNnSc6
výšiny	výšina	k1gFnSc2
Bukit	Bukit	k1gMnSc1
Timah	Timah	k1gMnSc1
vztyčil	vztyčit	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
generál	generál	k1gMnSc1
Percival	Percival	k1gMnSc1
bílý	bílý	k2eAgInSc4d1
prapor	prapor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osud	osud	k1gInSc1
Singapuru	Singapur	k1gInSc2
byl	být	k5eAaImAgInS
zpečetěn	zpečetěn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kapitulace	kapitulace	k1gFnSc1
britských	britský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
</s>
<s>
Závěr	závěr	k1gInSc1
</s>
<s>
Důsledky	důsledek	k1gInPc1
japonských	japonský	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
v	v	k7c6
Malajsku	Malajsko	k1gNnSc6
a	a	k8xC
v	v	k7c6
Singapuru	Singapur	k1gInSc6
byly	být	k5eAaImAgFnP
pro	pro	k7c4
Spojence	spojenec	k1gMnPc4
katastrofální	katastrofální	k2eAgMnPc1d1
<g/>
.	.	kIx.
130	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
nebo	nebo	k8xC
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
do	do	k7c2
zajetí	zajetí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgMnSc3
Japonci	Japonec	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
„	„	k?
<g/>
jen	jen	k6eAd1
<g/>
“	“	k?
o	o	k7c4
9	#num#	k4
000	#num#	k4
padlých	padlý	k1gMnPc2
a	a	k8xC
raněných	raněný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
navíc	navíc	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
velmi	velmi	k6eAd1
důležitou	důležitý	k2eAgFnSc4d1
asijskou	asijský	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
prestiž	prestiž	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
v	v	k7c6
Asii	Asie	k1gFnSc6
byla	být	k5eAaImAgFnS
značně	značně	k6eAd1
otřesena	otřesen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
historikové	historik	k1gMnPc1
zastávají	zastávat	k5eAaImIp3nP
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
katastrofu	katastrofa	k1gFnSc4
srovnatelnou	srovnatelný	k2eAgFnSc4d1
s	s	k7c7
pádem	pád	k1gInSc7
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
příčina	příčina	k1gFnSc1
britské	britský	k2eAgFnSc2d1
porážky	porážka	k1gFnSc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
podcenění	podcenění	k1gNnSc6
japonských	japonský	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
a	a	k8xC
nepřipravenosti	nepřipravenost	k1gFnSc2
na	na	k7c4
možnost	možnost	k1gFnSc4
útoku	útok	k1gInSc2
z	z	k7c2
pevniny	pevnina	k1gFnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
džungle	džungle	k1gFnSc1
na	na	k7c6
Malajském	malajský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
byla	být	k5eAaImAgFnS
britským	britský	k2eAgNnSc7d1
velením	velení	k1gNnSc7
považována	považován	k2eAgFnSc1d1
za	za	k7c4
neprostupnou	prostupný	k2eNgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
vítězství	vítězství	k1gNnSc3
se	se	k3xPyFc4
Japonsku	Japonsko	k1gNnSc6
otevřela	otevřít	k5eAaPmAgFnS
cesta	cesta	k1gFnSc1
k	k	k7c3
získání	získání	k1gNnSc3
Nizozemské	nizozemský	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Singapur	Singapur	k1gInSc1
byl	být	k5eAaImAgInS
Japonci	Japonec	k1gMnPc1
přejmenován	přejmenován	k2eAgMnSc1d1
na	na	k7c4
Šónan	Šónan	k1gInSc4
(	(	kIx(
<g/>
照	照	k?
~	~	kIx~
Světlo	světlo	k1gNnSc1
jihu	jih	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
prestiž	prestiž	k1gFnSc1
generála	generál	k1gMnSc2
Jamašity	Jamašit	k2eAgFnPc1d1
stoupla	stoupnout	k5eAaPmAgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
vysloužil	vysloužit	k5eAaPmAgMnS
přezdívku	přezdívka	k1gFnSc4
„	„	k?
<g/>
Malajský	malajský	k2eAgMnSc1d1
tygr	tygr	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Battle	Battle	k1gFnSc2
of	of	k?
Singapore	Singapor	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
L	L	kA
<g/>
,	,	kIx,
Klemen	Klemen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rear-Admiral	Rear-Admiral	k1gMnSc1
Shoji	Shoje	k1gFnSc4
Nishimura	Nishimura	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Celkem	celkem	k6eAd1
spojenci	spojenec	k1gMnPc1
<g/>
:	:	kIx,
7	#num#	k4
500	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
<g/>
,	,	kIx,
10	#num#	k4
000	#num#	k4
zraněných	zraněný	k1gMnPc2
a	a	k8xC
kolem	kolem	k7c2
120	#num#	k4
000	#num#	k4
zajatých	zajatá	k1gFnPc2
po	po	k7c4
celou	celý	k2eAgFnSc4d1
malajskou	malajský	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
↑	↑	k?
SMITH	SMITH	kA
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Singapore	Singapor	k1gInSc5
Burning	Burning	k1gInSc1
<g/>
:	:	kIx,
Heroism	Heroism	k1gMnSc1
and	and	k?
Surrender	Surrender	k1gMnSc1
in	in	k?
World	World	k1gMnSc1
War	War	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
141	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1036	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Podle	podle	k7c2
anglické	anglický	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
↑	↑	k?
Čechoslováci	Čechoslovák	k1gMnPc1
v	v	k7c6
bojích	boj	k1gInPc6
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Asii	Asie	k1gFnSc6
-	-	kIx~
referát	referát	k1gInSc1
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Beránek	Beránek	k1gMnSc1
-	-	kIx~
Pátrání	pátrání	k1gNnSc1
po	po	k7c6
Silvestrovi	Silvestr	k1gMnSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2020	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GRYNER	GRYNER	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
H.	H.	kA
Pád	Pád	k1gInSc1
nedobytné	dobytný	k2eNgFnSc2d1
pevnosti	pevnost	k1gFnSc2
Singapur	Singapur	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Magnet-Press	Magnet-Press	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SHORES	SHORES	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
CULL	CULL	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
;	;	kIx,
IZAVA	IZAVA	kA
<g/>
,	,	kIx,
Jasuho	Jasuha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I.	I.	kA
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85831	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SHORES	SHORES	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
CULL	CULL	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
;	;	kIx,
IZAVA	IZAVA	kA
<g/>
,	,	kIx,
Jasuho	Jasuha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85831	#num#	k4
<g/>
-	-	kIx~
<g/>
73	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BURTON	BURTON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortnight	Fortnight	k1gInSc4
of	of	k?
infamy	infam	k1gInPc4
<g/>
:	:	kIx,
the	the	k?
collapse	collapse	k1gFnSc2
of	of	k?
Allied	Allied	k1gMnSc1
airpower	airpower	k1gMnSc1
west	west	k1gMnSc1
of	of	k?
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781591140962	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HUBÁČEK	Hubáček	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pacifik	Pacifik	k1gInSc1
v	v	k7c6
plamenech	plamen	k1gInPc6
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
436	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WILLMOTT	WILLMOTT	kA
<g/>
,	,	kIx,
H.	H.	kA
P.	P.	kA
Empires	Empires	k1gMnSc1
in	in	k?
the	the	k?
Balance	balance	k1gFnSc1
<g/>
:	:	kIx,
Japanese	Japanese	k1gFnSc1
and	and	k?
Allied	Allied	k1gMnSc1
Pacific	Pacific	k1gMnSc1
Strategies	Strategies	k1gMnSc1
To	to	k9
April	April	k1gInSc4
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
948	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
</s>
<s>
Rising	Rising	k1gInSc1
Sun	Sun	kA
<g/>
:	:	kIx,
The	The	k1gMnSc1
Japanese	Japanese	k1gFnSc2
Conquests	Conquests	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HyperWar	HyperWar	k1gInSc1
Foundation	Foundation	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
–	–	k?
linky	linka	k1gFnPc1
na	na	k7c4
další	další	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
a	a	k8xC
odtajněné	odtajněný	k2eAgInPc4d1
materiály	materiál	k1gInPc4
(	(	kIx(
<g/>
primární	primární	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
HEADRICK	HEADRICK	kA
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
C.	C.	kA
Bicycle	Bicycle	k1gFnSc1
Blitzkrieg	Blitzkrieg	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Malayan	Malayan	k1gMnSc1
Campaign	Campaign	k1gMnSc1
and	and	k?
the	the	k?
Fall	Fall	k1gInSc1
of	of	k?
Singapore	Singapor	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HyperWar	HyperWar	k1gInSc1
Foundation	Foundation	k1gInSc4
<g/>
,	,	kIx,
1994	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Malajsii	Malajsie	k1gFnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
•	•	k?
Boje	boj	k1gInPc4
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
Guadalcanal	Guadalcanal	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrova	ostrov	k1gInSc2
Savo	Savo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Šalomounů	Šalomoun	k1gMnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
mysu	mys	k1gInSc2
Esperance	Esperance	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrovů	ostrov	k1gInPc2
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Guadalcanalu	Guadalcanal	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tassafarongy	Tassafarong	k1gInPc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Rennellova	Rennellův	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Bismarckově	Bismarckův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Komandorských	Komandorský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Tarawu	Tarawus	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Operace	operace	k1gFnSc1
Forager	Forager	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Saipan	Saipan	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Peleliu	Pelelium	k1gNnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Leyte	Leyt	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Manilu	Manila	k1gFnSc4
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Iwodžimu	Iwodžimo	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Okinawu	Okinawa	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
541857	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
95002267	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
95002267	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
