<s desamb="1">
Japonské	japonský	k2eAgFnPc4d1
snahy	snaha	k1gFnPc4
o	o	k7c6
získáni	získat	k5eAaPmNgMnP
tohoto	tento	k3xDgNnSc2
města	město	k1gNnSc2
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
již	již	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
Singapur	Singapur	k1gInSc1
poprvé	poprvé	k6eAd1
bombardován	bombardován	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
byly	být	k5eAaImAgFnP
završeny	završit	k5eAaPmNgFnP
mezi	mezi	k7c4
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
únorem	únor	k1gInSc7
1942	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Singapur	Singapur	k1gInSc1
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
do	do	k7c2
japonských	japonský	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>