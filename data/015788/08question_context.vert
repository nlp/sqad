<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
je	být	k5eAaImIp3nS
pojmenováním	pojmenování	k1gNnSc7
válečných	válečný	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgFnP
na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1941	#num#	k4
a	a	k8xC
1942	#num#	k4
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
bojů	boj	k1gInPc2
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>