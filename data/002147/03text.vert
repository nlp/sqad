<s>
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
komiksových	komiksový	k2eAgInPc2d1	komiksový
příběhů	příběh	k1gInPc2	příběh
např.	např.	kA	např.
o	o	k7c6	o
Avengerech	Avenger	k1gInPc6	Avenger
vydávaných	vydávaný	k2eAgFnPc2d1	vydávaná
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Marvel	Marvel	k1gInSc1	Marvel
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
komiksové	komiksový	k2eAgFnSc6d1	komiksová
knize	kniha	k1gFnSc6	kniha
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Suspense	suspense	k1gFnSc1	suspense
#	#	kIx~	#
<g/>
39	[number]	k4	39
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Postavu	postava	k1gFnSc4	postava
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Stan	stan	k1gInSc4	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
ji	on	k3xPp3gFnSc4	on
rozpracoval	rozpracovat	k5eAaPmAgMnS	rozpracovat
Larry	Larr	k1gInPc4	Larr
Lieber	Liebra	k1gFnPc2	Liebra
a	a	k8xC	a
výtvarně	výtvarně	k6eAd1	výtvarně
ji	on	k3xPp3gFnSc4	on
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
Don	Don	k1gMnSc1	Don
Heck	Heck	k1gMnSc1	Heck
a	a	k8xC	a
Jack	Jack	k1gMnSc1	Jack
Kirby	Kirba	k1gFnSc2	Kirba
<g/>
.	.	kIx.	.
</s>
<s>
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc6	Lea
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
nového	nový	k2eAgMnSc2d1	nový
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
typický	typický	k2eAgMnSc1d1	typický
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
kapitalista	kapitalista	k1gMnSc1	kapitalista
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
konceptem	koncept	k1gInSc7	koncept
přišel	přijít	k5eAaPmAgInS	přijít
úmyslně	úmyslně	k6eAd1	úmyslně
v	v	k7c6	v
období	období	k1gNnSc6	období
probíhající	probíhající	k2eAgFnSc2d1	probíhající
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
hrdina	hrdina	k1gMnSc1	hrdina
měl	mít	k5eAaImAgMnS	mít
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
americký	americký	k2eAgInSc4d1	americký
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
příběh	příběh	k1gInSc1	příběh
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
-	-	kIx~	-
amerického	americký	k2eAgMnSc4d1	americký
průmyslníka	průmyslník	k1gMnSc4	průmyslník
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
prodejce	prodejce	k1gMnSc2	prodejce
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgNnSc7	tento
pojetím	pojetí	k1gNnSc7	pojetí
šel	jít	k5eAaImAgMnS	jít
proti	proti	k7c3	proti
zajetým	zajetý	k2eAgFnPc3d1	zajetá
kolejím	kolej	k1gFnPc3	kolej
komiksových	komiksový	k2eAgMnPc2d1	komiksový
hrdinů	hrdina	k1gMnPc2	hrdina
a	a	k8xC	a
že	že	k8xS	že
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
ujme	ujmout	k5eAaPmIp3nS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
inspirace	inspirace	k1gFnSc1	inspirace
pro	pro	k7c4	pro
charakter	charakter	k1gInSc4	charakter
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
posloužil	posloužit	k5eAaPmAgMnS	posloužit
Howard	Howard	k1gMnSc1	Howard
Hughes	Hughes	k1gMnSc1	Hughes
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
oblek	oblek	k1gInSc1	oblek
Iron	iron	k1gInSc1	iron
Mana	Man	k1gMnSc2	Man
měl	mít	k5eAaImAgInS	mít
ocelově	ocelově	k6eAd1	ocelově
šedou	šedý	k2eAgFnSc4d1	šedá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
čísle	číslo	k1gNnSc6	číslo
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Suspense	suspense	k1gFnSc1	suspense
#	#	kIx~	#
<g/>
40	[number]	k4	40
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
zlatý	zlatý	k2eAgInSc4d1	zlatý
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
48	[number]	k4	48
<g/>
.	.	kIx.	.
čísle	číslo	k1gNnSc6	číslo
na	na	k7c4	na
zlato-červený	zlato-červený	k2eAgInSc4d1	zlato-červený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
byl	být	k5eAaImAgMnS	být
Iron-Man	Iron-Man	k1gInSc4	Iron-Man
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
proti	proti	k7c3	proti
komunistům	komunista	k1gMnPc3	komunista
a	a	k8xC	a
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
vietnamským	vietnamský	k2eAgMnPc3d1	vietnamský
špionům	špion	k1gMnPc3	špion
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
koncept	koncept	k1gInSc1	koncept
komiksu	komiks	k1gInSc2	komiks
Tales	Talesa	k1gFnPc2	Talesa
of	of	k?	of
Suspense	suspens	k1gFnSc2	suspens
přeměněn	přeměněn	k2eAgInSc4d1	přeměněn
a	a	k8xC	a
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
objevovaly	objevovat	k5eAaImAgInP	objevovat
pouze	pouze	k6eAd1	pouze
příběhy	příběh	k1gInPc1	příběh
Iron	iron	k1gInSc4	iron
Mana	Man	k1gMnSc2	Man
a	a	k8xC	a
"	"	kIx"	"
<g/>
Kapitána	kapitán	k1gMnSc2	kapitán
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1968	[number]	k4	1968
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vlastním	vlastní	k2eAgNnSc6d1	vlastní
prvním	první	k4xOgMnSc6	první
komiksu	komiks	k1gInSc2	komiks
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
and	and	k?	and
Sub-Mariner	Sub-Mariner	k1gMnSc1	Sub-Mariner
a	a	k8xC	a
od	od	k7c2	od
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
sérii	série	k1gFnSc6	série
The	The	k1gFnSc4	The
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
vydávána	vydávat	k5eAaPmNgFnS	vydávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
do	do	k7c2	do
1996	[number]	k4	1996
a	a	k8xC	a
čítala	čítat	k5eAaImAgFnS	čítat
332	[number]	k4	332
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
The	The	k1gFnSc2	The
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
byla	být	k5eAaImAgFnS	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1996	[number]	k4	1996
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
hlavními	hlavní	k2eAgMnPc7d1	hlavní
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
Scott	Scott	k1gMnSc1	Scott
Lobdell	Lobdell	k1gMnSc1	Lobdell
<g/>
,	,	kIx,	,
Jeph	Jeph	k1gMnSc1	Jeph
Loeb	Loeb	k1gMnSc1	Loeb
a	a	k8xC	a
výtvarníci	výtvarník	k1gMnPc1	výtvarník
Whilce	Whilec	k1gMnSc2	Whilec
Portacio	Portacio	k1gNnSc1	Portacio
a	a	k8xC	a
Ryan	Ryan	k1gMnSc1	Ryan
Benjamin	Benjamin	k1gMnSc1	Benjamin
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
série	série	k1gFnSc1	série
The	The	k1gFnSc2	The
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
byla	být	k5eAaImAgFnS	být
vydávána	vydávat	k5eAaPmNgFnS	vydávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
do	do	k7c2	do
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
čítala	čítat	k5eAaImAgFnS	čítat
89	[number]	k4	89
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
Kurt	Kurt	k1gMnSc1	Kurt
Busiek	Busiek	k1gMnSc1	Busiek
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Stern	sternum	k1gNnPc2	sternum
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
série	série	k1gFnSc1	série
The	The	k1gFnSc2	The
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
byla	být	k5eAaImAgFnS	být
vydávána	vydávat	k5eAaPmNgFnS	vydávat
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2005	[number]	k4	2005
až	až	k6eAd1	až
2009	[number]	k4	2009
a	a	k8xC	a
čítala	čítat	k5eAaImAgFnS	čítat
16	[number]	k4	16
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
vydávání	vydávání	k1gNnSc2	vydávání
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
série	série	k1gFnSc2	série
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
vydávána	vydávat	k5eAaImNgFnS	vydávat
i	i	k9	i
série	série	k1gFnSc1	série
pátá	pátý	k4xOgFnSc1	pátý
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
čítala	čítat	k5eAaImAgFnS	čítat
33	[number]	k4	33
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
autory	autor	k1gMnPc7	autor
byli	být	k5eAaImAgMnP	být
Matt	Matt	k2eAgInSc4d1	Matt
Fraction	Fraction	k1gInSc4	Fraction
a	a	k8xC	a
Salvador	Salvador	k1gInSc4	Salvador
Larroca	Larrocum	k1gNnSc2	Larrocum
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
již	již	k9	již
přes	přes	k7c4	přes
500	[number]	k4	500
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Fraction	Fraction	k1gInSc1	Fraction
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
29	[number]	k4	29
čísly	číslo	k1gNnPc7	číslo
počítaným	počítaný	k2eAgNnSc7d1	počítané
k	k	k7c3	k
první	první	k4xOgFnSc3	první
sérii	série	k1gFnSc3	série
(	(	kIx(	(
<g/>
#	#	kIx~	#
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
527	[number]	k4	527
a	a	k8xC	a
#	#	kIx~	#
<g/>
500.1	[number]	k4	500.1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
řazení	řazení	k1gNnSc6	řazení
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
tato	tento	k3xDgFnSc1	tento
pátá	pátá	k1gFnSc1	pátá
série	série	k1gFnSc2	série
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
The	The	k1gFnSc2	The
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc4d1	ostatní
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Marvel	Marvela	k1gFnPc2	Marvela
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
vydávána	vydáván	k2eAgFnSc1d1	vydávána
šestá	šestý	k4xOgFnSc1	šestý
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Kieron	Kieron	k1gMnSc1	Kieron
Gillen	Gillna	k1gFnPc2	Gillna
a	a	k8xC	a
čítala	čítat	k5eAaImAgFnS	čítat
31	[number]	k4	31
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
série	série	k1gFnSc1	série
The	The	k1gMnSc1	The
Superior	superior	k1gMnSc1	superior
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Tom	Tom	k1gMnSc1	Tom
Taylor	Taylor	k1gMnSc1	Taylor
a	a	k8xC	a
kreslíři	kreslíř	k1gMnPc1	kreslíř
Yildiray	Yildiraa	k1gFnSc2	Yildiraa
Çinar	Çinar	k1gInSc1	Çinar
<g/>
,	,	kIx,	,
Laura	Laura	k1gFnSc1	Laura
Braga	Braga	k1gFnSc1	Braga
a	a	k8xC	a
Felipe	Felip	k1gMnSc5	Felip
Watanabe	Watanab	k1gMnSc5	Watanab
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přechodu	přechod	k1gInSc6	přechod
k	k	k7c3	k
All-New	All-New	k1gFnSc3	All-New
<g/>
,	,	kIx,	,
All-Different	All-Different	k1gInSc4	All-Different
Marvel	Marvela	k1gFnPc2	Marvela
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
sérií	série	k1gFnSc7	série
Invincible	Invincible	k1gFnSc2	Invincible
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
psal	psát	k5eAaImAgMnS	psát
Brian	Brian	k1gMnSc1	Brian
Michael	Michael	k1gMnSc1	Michael
Bendis	Bendis	k1gFnSc2	Bendis
a	a	k8xC	a
kreslili	kreslit	k5eAaImAgMnP	kreslit
David	David	k1gMnSc1	David
Marquez	Marquez	k1gMnSc1	Marquez
a	a	k8xC	a
Mike	Mike	k1gNnSc1	Mike
Deodato	Deodat	k2eAgNnSc1d1	Deodato
Jr	Jr	k1gFnSc7	Jr
<g/>
..	..	k?	..
Série	série	k1gFnSc1	série
čítala	čítat	k5eAaImAgFnS	čítat
14	[number]	k4	14
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
navazující	navazující	k2eAgFnSc7d1	navazující
sérií	série	k1gFnSc7	série
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
i	i	k9	i
nadále	nadále	k6eAd1	nadále
psal	psát	k5eAaImAgMnS	psát
Brian	Brian	k1gMnSc1	Brian
Michael	Michael	k1gMnSc1	Michael
Bendis	Bendis	k1gFnSc2	Bendis
a	a	k8xC	a
nově	nově	k6eAd1	nově
kreslil	kreslit	k5eAaImAgMnS	kreslit
Stefano	Stefana	k1gFnSc5	Stefana
Caselli	Caselle	k1gFnSc3	Caselle
<g/>
.	.	kIx.	.
</s>
<s>
Bendis	Bendis	k1gInSc1	Bendis
využil	využít	k5eAaPmAgInS	využít
předchozí	předchozí	k2eAgFnSc4d1	předchozí
sérii	série	k1gFnSc4	série
pro	pro	k7c4	pro
představení	představení	k1gNnSc4	představení
nové	nový	k2eAgFnSc2d1	nová
superhrdinky	superhrdinka	k1gFnSc2	superhrdinka
Riri	Rir	k1gFnSc2	Rir
Williamsové	Williamsová	k1gFnSc2	Williamsová
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
představena	představit	k5eAaPmNgNnP	představit
v	v	k7c6	v
Invincible	Invincible	k1gFnSc6	Invincible
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
sestrojila	sestrojit	k5eAaPmAgFnS	sestrojit
vlastní	vlastní	k2eAgInSc4d1	vlastní
oblek	oblek	k1gInSc4	oblek
a	a	k8xC	a
v	v	k7c4	v
sérii	série	k1gFnSc4	série
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
protagonistkou	protagonistka	k1gFnSc7	protagonistka
série	série	k1gFnSc2	série
pod	pod	k7c7	pod
superhrdinským	superhrdinský	k2eAgNnSc7d1	superhrdinské
jménem	jméno	k1gNnSc7	jméno
Ironheart	Ironhearta	k1gFnPc2	Ironhearta
<g/>
.	.	kIx.	.
</s>
<s>
Bendis	Bendis	k1gFnSc1	Bendis
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
představil	představit	k5eAaPmAgInS	představit
další	další	k2eAgFnSc4d1	další
sérii	série	k1gFnSc4	série
o	o	k7c4	o
Iron	iron	k1gInSc4	iron
Manovi	Manův	k2eAgMnPc1d1	Manův
s	s	k7c7	s
názvem	název	k1gInSc7	název
International	International	k1gMnPc2	International
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
kreslil	kreslit	k5eAaImAgMnS	kreslit
Alex	Alex	k1gMnSc1	Alex
Maleev	Maleva	k1gFnPc2	Maleva
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
navazující	navazující	k2eAgFnSc7d1	navazující
sérií	série	k1gFnSc7	série
Infamous	Infamous	k1gInSc1	Infamous
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
stejné	stejný	k2eAgNnSc1d1	stejné
tvůrčí	tvůrčí	k2eAgNnSc1d1	tvůrčí
duo	duo	k1gNnSc1	duo
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
Iron	iron	k1gInSc1	iron
Mana	Man	k1gMnSc4	Man
zde	zde	k6eAd1	zde
převzal	převzít	k5eAaPmAgMnS	převzít
Victor	Victor	k1gMnSc1	Victor
von	von	k1gInSc4	von
Doom	Dooma	k1gFnPc2	Dooma
<g/>
.	.	kIx.	.
</s>
<s>
Tales	Tales	k1gInSc1	Tales
of	of	k?	of
Suspense	suspens	k1gFnSc2	suspens
#	#	kIx~	#
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1963	[number]	k4	1963
-	-	kIx~	-
březen	březen	k1gInSc1	březen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
332	[number]	k4	332
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
1968	[number]	k4	1968
-	-	kIx~	-
září	září	k1gNnSc2	září
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
13	[number]	k4	13
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1996	[number]	k4	1996
-	-	kIx~	-
listopad	listopad	k1gInSc1	listopad
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
1998	[number]	k4	1998
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
2005	[number]	k4	2005
-	-	kIx~	-
leden	leden	k1gInSc1	leden
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Invincible	Invincible	k1gFnPc2	Invincible
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
,	,	kIx,	,
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
527	[number]	k4	527
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
2008	[number]	k4	2008
-	-	kIx~	-
únor	únor	k1gInSc1	únor
2011	[number]	k4	2011
<g/>
;	;	kIx,	;
březen	březen	k1gInSc1	březen
2011	[number]	k4	2011
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
2012	[number]	k4	2012
-	-	kIx~	-
červen	červen	k1gInSc1	červen
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Superior	superior	k1gMnSc1	superior
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
2014	[number]	k4	2014
-	-	kIx~	-
červen	červen	k1gInSc1	červen
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
2015	[number]	k4	2015
-	-	kIx~	-
říjen	říjen	k1gInSc1	říjen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Invincible	Invincible	k1gMnPc2	Invincible
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
2016	[number]	k4	2016
-	-	kIx~	-
...	...	k?	...
<g/>
)	)	kIx)	)
Anthony	Anthona	k1gFnSc2	Anthona
Edward	Edward	k1gMnSc1	Edward
Stark	Stark	k1gInSc1	Stark
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
na	na	k7c6	na
newyorském	newyorský	k2eAgInSc6d1	newyorský
ostrově	ostrov	k1gInSc6	ostrov
Long	Long	k1gMnSc1	Long
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
slavný	slavný	k2eAgMnSc1d1	slavný
vývojář	vývojář	k1gMnSc1	vývojář
a	a	k8xC	a
obchodník	obchodník	k1gMnSc1	obchodník
Howard	Howarda	k1gFnPc2	Howarda
Stark	Stark	k1gInSc1	Stark
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c4	na
Long	Long	k1gInSc4	Long
Islandu	Island	k1gInSc2	Island
založil	založit	k5eAaPmAgInS	založit
společnost	společnost	k1gFnSc4	společnost
Stark	Stark	k1gInSc1	Stark
Industries	Industriesa	k1gFnPc2	Industriesa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
zbraně	zbraň	k1gFnPc4	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
<g/>
.	.	kIx.	.
</s>
<s>
Tony	Tony	k1gMnSc1	Tony
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
génius	génius	k1gMnSc1	génius
<g/>
,	,	kIx,	,
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
přijat	přijat	k2eAgInSc1d1	přijat
na	na	k7c4	na
Massachusettský	massachusettský	k2eAgInSc4d1	massachusettský
technologický	technologický	k2eAgInSc4d1	technologický
institut	institut	k1gInSc4	institut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
elektroinženýrství	elektroinženýrství	k1gNnSc4	elektroinženýrství
a	a	k8xC	a
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
převzal	převzít	k5eAaPmAgInS	převzít
vedení	vedení	k1gNnSc4	vedení
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
při	při	k7c6	při
autenohodě	autenohod	k1gInSc6	autenohod
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
zbraně	zbraň	k1gFnPc4	zbraň
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
prezentaci	prezentace	k1gFnSc6	prezentace
svých	svůj	k3xOyFgFnPc2	svůj
zbraní	zbraň	k1gFnPc2	zbraň
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
během	během	k7c2	během
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
šrapnely	šrapnel	k1gInPc7	šrapnel
a	a	k8xC	a
zajat	zajat	k2eAgInSc4d1	zajat
Wong-Chuem	Wong-Chuem	k1gInSc4	Wong-Chuem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
operaci	operace	k1gFnSc4	operace
nutil	nutit	k5eAaImAgMnS	nutit
sestrojit	sestrojit	k5eAaPmF	sestrojit
nejmodernější	moderní	k2eAgFnPc4d3	nejmodernější
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
operaci	operace	k1gFnSc4	operace
provedl	provést	k5eAaPmAgMnS	provést
Ho	on	k3xPp3gNnSc4	on
Yinsen	Yinsen	k2eAgMnSc1d1	Yinsen
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
vězněm	vězeň	k1gMnSc7	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
šrapnely	šrapnel	k1gInPc1	šrapnel
mířily	mířit	k5eAaImAgFnP	mířit
do	do	k7c2	do
Starkova	Starkův	k2eAgNnSc2d1	Starkovo
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Yinsen	Yinsen	k1gInSc4	Yinsen
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
magnetické	magnetický	k2eAgNnSc1d1	magnetické
zařízení	zařízení	k1gNnSc1	zařízení
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
zadržení	zadržení	k1gNnSc6	zadržení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
společně	společně	k6eAd1	společně
s	s	k7c7	s
Yinsenem	Yinseno	k1gNnSc7	Yinseno
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
ocelový	ocelový	k2eAgInSc4d1	ocelový
oblek	oblek	k1gInSc4	oblek
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yQgMnSc7	který
později	pozdě	k6eAd2	pozdě
utekl	utéct	k5eAaPmAgMnS	utéct
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
autoři	autor	k1gMnPc1	autor
měnili	měnit	k5eAaImAgMnP	měnit
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Tony	Tony	k1gMnSc1	Tony
Stark	Stark	k1gInSc4	Stark
zraněn	zraněn	k2eAgInSc4d1	zraněn
a	a	k8xC	a
vězněn	vězněn	k2eAgInSc4d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
až	až	k9	až
po	po	k7c4	po
Afghánistán	Afghánistán	k1gInSc4	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
obleku	oblek	k1gInSc3	oblek
utekl	utéct	k5eAaPmAgMnS	utéct
a	a	k8xC	a
vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
americkou	americký	k2eAgFnSc4d1	americká
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
potkal	potkat	k5eAaPmAgInS	potkat
zraněného	zraněný	k2eAgMnSc4d1	zraněný
vojáka	voják	k1gMnSc4	voják
Jamese	Jamese	k1gFnSc2	Jamese
Ruperta	Rupert	k1gMnSc4	Rupert
Rhodese	Rhodese	k1gFnSc2	Rhodese
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Zpátky	zpátky	k6eAd1	zpátky
doma	doma	k6eAd1	doma
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
magnetického	magnetický	k2eAgNnSc2d1	magnetické
zařízení	zařízení	k1gNnSc2	zařízení
by	by	kYmCp3nS	by
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
musel	muset	k5eAaImAgMnS	muset
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
dobíjet	dobíjet	k5eAaImF	dobíjet
v	v	k7c6	v
obleku	oblek	k1gInSc6	oblek
Iron	iron	k1gInSc1	iron
Mana	mana	k1gFnSc1	mana
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
krytí	krytí	k1gNnSc4	krytí
sloužila	sloužit	k5eAaImAgFnS	sloužit
historka	historka	k1gFnSc1	historka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
je	být	k5eAaImIp3nS	být
Tonyho	Tony	k1gMnSc2	Tony
bodyguard	bodyguard	k1gMnSc1	bodyguard
a	a	k8xC	a
maskot	maskot	k1gInSc1	maskot
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Pravdu	pravda	k1gFnSc4	pravda
sdělil	sdělit	k5eAaPmAgMnS	sdělit
jen	jen	k9	jen
své	svůj	k3xOyFgFnSc3	svůj
sekretářce	sekretářka	k1gFnSc3	sekretářka
Virginii	Virginie	k1gFnSc3	Virginie
"	"	kIx"	"
<g/>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
"	"	kIx"	"
Potts	Potts	k1gInSc1	Potts
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původních	původní	k2eAgInPc6d1	původní
příbězích	příběh	k1gInPc6	příběh
byl	být	k5eAaImAgInS	být
silným	silný	k2eAgMnSc7d1	silný
antikomunistou	antikomunista	k1gMnSc7	antikomunista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc4	jeho
charakter	charakter	k1gInSc4	charakter
přerodil	přerodit	k5eAaPmAgMnS	přerodit
a	a	k8xC	a
přestal	přestat	k5eAaPmAgMnS	přestat
dodávat	dodávat	k5eAaImF	dodávat
zbraně	zbraň	k1gFnPc4	zbraň
americké	americký	k2eAgFnSc3d1	americká
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
vylepšení	vylepšení	k1gNnSc4	vylepšení
pro	pro	k7c4	pro
S.	S.	kA	S.
<g/>
H.I.E.L.D.	H.I.E.L.D.	k1gFnSc2	H.I.E.L.D.
<g/>
,	,	kIx,	,
Avengers	Avengersa	k1gFnPc2	Avengersa
a	a	k8xC	a
X-Meny	X-Mena	k1gFnSc2	X-Mena
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
Stark	Stark	k1gInSc4	Stark
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
lepší	dobrý	k2eAgMnSc1d2	lepší
a	a	k8xC	a
modernější	moderní	k2eAgNnPc4d2	modernější
napájecí	napájecí	k2eAgNnPc4d1	napájecí
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
svůj	svůj	k3xOyFgInSc4	svůj
oblek	oblek	k1gInSc4	oblek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mohl	moct	k5eAaImAgInS	moct
létat	létat	k5eAaImF	létat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgMnSc2	který
učinil	učinit	k5eAaImAgMnS	učinit
silnou	silný	k2eAgFnSc4d1	silná
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečném	skutečný	k2eAgInSc6d1	skutečný
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
si	se	k3xPyFc3	se
prošel	projít	k5eAaPmAgInS	projít
těžkou	těžký	k2eAgFnSc7d1	těžká
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
(	(	kIx(	(
<g/>
v	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
Demon	Demona	k1gFnPc2	Demona
in	in	k?	in
a	a	k8xC	a
Bottle	Bottle	k1gFnSc2	Bottle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gMnSc1	jeho
rival	rival	k1gMnSc1	rival
Obadiah	Obadiah	k1gMnSc1	Obadiah
Stane	stanout	k5eAaPmIp3nS	stanout
ho	on	k3xPp3gInSc4	on
manipuloval	manipulovat	k5eAaImAgMnS	manipulovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
vyvolal	vyvolat	k5eAaPmAgMnS	vyvolat
emoční	emoční	k2eAgInPc4d1	emoční
záchvaty	záchvat	k1gInPc4	záchvat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ho	on	k3xPp3gMnSc4	on
připravil	připravit	k5eAaPmAgInS	připravit
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Stark	Stark	k1gInSc1	Stark
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
společnost	společnost	k1gFnSc4	společnost
i	i	k8xC	i
identitu	identita	k1gFnSc4	identita
Iron	iron	k1gInSc1	iron
Mana	mana	k1gFnSc1	mana
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
převzal	převzít	k5eAaPmAgMnS	převzít
James	James	k1gMnSc1	James
Rupert	Rupert	k1gMnSc1	Rupert
Rhodes	Rhodes	k1gMnSc1	Rhodes
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
vzpamatoval	vzpamatovat	k5eAaPmAgInS	vzpamatovat
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
obleku	oblek	k1gInSc2	oblek
Iron	iron	k1gInSc1	iron
Mana	mana	k1gFnSc1	mana
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Rhodes	Rhodes	k1gMnSc1	Rhodes
získal	získat	k5eAaPmAgMnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
porazili	porazit	k5eAaPmAgMnP	porazit
Stanea	Stanea	k1gMnSc1	Stanea
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nakonec	nakonec	k6eAd1	nakonec
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
týmů	tým	k1gInPc2	tým
Marvel	Marvel	k1gInSc1	Marvel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Avengers	Avengers	k1gInSc1	Avengers
nebo	nebo	k8xC	nebo
Ultimates	Ultimates	k1gInSc1	Ultimates
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
relaunchi	relaunch	k1gInSc6	relaunch
Marvel	Marvela	k1gFnPc2	Marvela
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
Marvel	Marvel	k1gInSc1	Marvel
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
Tony	Tony	k1gMnSc1	Tony
Stark	Stark	k1gInSc4	Stark
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
novou	nový	k2eAgFnSc4d1	nová
umělou	umělý	k2eAgFnSc4d1	umělá
inteligenci	inteligence	k1gFnSc4	inteligence
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
P.	P.	kA	P.
<g/>
E.	E.	kA	E.
<g/>
P.	P.	kA	P.
<g/>
P.	P.	kA	P.
<g/>
E.R	E.R	k1gMnSc1	E.R
a	a	k8xC	a
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
Strážcům	strážce	k1gMnPc3	strážce
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
eventu	event	k1gInSc2	event
AXIS	AXIS	kA	AXIS
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
osobnost	osobnost	k1gFnSc1	osobnost
pozměněna	pozměněn	k2eAgFnSc1d1	pozměněna
a	a	k8xC	a
Stark	Stark	k1gInSc1	Stark
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
znovu	znovu	k6eAd1	znovu
více	hodně	k6eAd2	hodně
sobeckým	sobecký	k2eAgMnPc3d1	sobecký
<g/>
,	,	kIx,	,
zištným	zištný	k2eAgMnPc3d1	zištný
a	a	k8xC	a
závislým	závislý	k2eAgMnSc7d1	závislý
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
Superior	superior	k1gMnSc1	superior
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
nový	nový	k2eAgInSc1d1	nový
techno-virus	technoirus	k1gInSc1	techno-virus
Extremis	Extremis	k1gFnSc1	Extremis
3.0	[number]	k4	3.0
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
lidem	člověk	k1gMnPc3	člověk
mohl	moct	k5eAaImAgMnS	moct
přinést	přinést	k5eAaPmF	přinést
krásu	krása	k1gFnSc4	krása
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
nesmrtelnost	nesmrtelnost	k1gFnSc4	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
si	se	k3xPyFc3	se
postavil	postavit	k5eAaPmAgMnS	postavit
nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
netradičně	tradičně	k6eNd1	tradičně
bílý	bílý	k2eAgInSc1d1	bílý
oblek	oblek	k1gInSc1	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
lidi	člověk	k1gMnPc4	člověk
nalákal	nalákat	k5eAaPmAgMnS	nalákat
na	na	k7c4	na
free	fre	k1gMnSc4	fre
verzi	verze	k1gFnSc4	verze
Extremis	Extremis	k1gFnSc1	Extremis
3.0	[number]	k4	3.0
<g/>
,	,	kIx,	,
jim	on	k3xPp3gFnPc3	on
začal	začít	k5eAaPmAgInS	začít
účtovat	účtovat	k5eAaImF	účtovat
neúnosný	únosný	k2eNgInSc1d1	neúnosný
denní	denní	k2eAgInSc1d1	denní
poplatek	poplatek	k1gInSc1	poplatek
100	[number]	k4	100
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
eventu	event	k1gInSc2	event
Secret	Secreta	k1gFnPc2	Secreta
Wars	Warsa	k1gFnPc2	Warsa
a	a	k8xC	a
změnách	změna	k1gFnPc6	změna
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
All-New	All-New	k1gFnSc7	All-New
<g/>
,	,	kIx,	,
All-Different	All-Different	k1gMnSc1	All-Different
Marvel	Marvel	k1gMnSc1	Marvel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Starkova	Starkův	k2eAgFnSc1d1	Starkova
osobnost	osobnost	k1gFnSc1	osobnost
vrátila	vrátit	k5eAaPmAgFnS	vrátit
před	před	k7c4	před
změny	změna	k1gFnPc4	změna
z	z	k7c2	z
Marvel	Marvela	k1gFnPc2	Marvela
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Postavil	postavit	k5eAaPmAgMnS	postavit
si	se	k3xPyFc3	se
nový	nový	k2eAgInSc4d1	nový
oblek	oblek	k1gInSc4	oblek
(	(	kIx(	(
<g/>
netradičího	tradičí	k2eNgInSc2d1	tradičí
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
novou	nový	k2eAgFnSc4d1	nová
umělou	umělý	k2eAgFnSc4d1	umělá
inteligenci	inteligence	k1gFnSc4	inteligence
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Friday	Fridaa	k1gFnSc2	Fridaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
minisérii	minisérie	k1gFnSc4	minisérie
International	International	k1gMnPc2	International
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalit	k5eAaPmNgNnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tony	Tony	k1gMnSc1	Tony
je	být	k5eAaImIp3nS	být
adoptivní	adoptivní	k2eAgMnSc1d1	adoptivní
syn	syn	k1gMnSc1	syn
Starků	Starek	k1gMnPc2	Starek
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc7	jeho
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
matkou	matka	k1gFnSc7	matka
je	být	k5eAaImIp3nS	být
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
tajná	tajný	k2eAgFnSc1d1	tajná
agentka	agentka	k1gFnSc1	agentka
S.	S.	kA	S.
<g/>
H.I.E.L.D.u	H.I.E.L.D.a	k1gMnSc4	H.I.E.L.D.a
Amanda	Amand	k1gMnSc4	Amand
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
zřekl	zřeknout	k5eAaPmAgInS	zřeknout
role	role	k1gFnSc2	role
Iron	iron	k1gInSc1	iron
Mana	Man	k1gMnSc2	Man
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
převzala	převzít	k5eAaPmAgFnS	převzít
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
studentka	studentka	k1gFnSc1	studentka
MIT	MIT	kA	MIT
Riri	Rire	k1gFnSc4	Rire
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pod	pod	k7c7	pod
superhrdinským	superhrdinský	k2eAgNnSc7d1	superhrdinské
jménem	jméno	k1gNnSc7	jméno
Ironheart	Ironhearta	k1gFnPc2	Ironhearta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vydává	vydávat	k5eAaPmIp3nS	vydávat
komiksové	komiksový	k2eAgFnPc4d1	komiksová
knihy	kniha	k1gFnPc4	kniha
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Crew	Crew	k1gMnSc1	Crew
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
-	-	kIx~	-
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
-	-	kIx~	-
Extremis	Extremis	k1gFnSc1	Extremis
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Warren	Warrna	k1gFnPc2	Warrna
Ellis	Ellis	k1gInSc1	Ellis
a	a	k8xC	a
Adi	Adi	k1gFnSc1	Adi
Granov	Granov	k1gInSc1	Granov
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
-	-	kIx~	-
Ultimátní	Ultimátní	k2eAgInSc1d1	Ultimátní
komiksový	komiksový	k2eAgInSc1d1	komiksový
komplet	komplet	k1gInSc1	komplet
#	#	kIx~	#
<g/>
43	[number]	k4	43
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
-	-	kIx~	-
Extremis	Extremis	k1gFnSc1	Extremis
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Warren	Warrna	k1gFnPc2	Warrna
Ellis	Ellis	k1gInSc1	Ellis
a	a	k8xC	a
Adi	Adi	k1gFnSc1	Adi
Granov	Granov	k1gInSc1	Granov
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
-	-	kIx~	-
Ultimátní	Ultimátní	k2eAgInSc1d1	Ultimátní
komiksový	komiksový	k2eAgInSc1d1	komiksový
komplet	komplet	k1gInSc1	komplet
#	#	kIx~	#
<g/>
58	[number]	k4	58
<g/>
:	:	kIx,	:
The	The	k1gFnSc6	The
Invincible	Invincible	k1gFnPc2	Invincible
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
-	-	kIx~	-
Pět	pět	k4xCc4	pět
nočních	noční	k2eAgFnPc2d1	noční
můr	můra	k1gFnPc2	můra
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Matt	Matt	k2eAgInSc4d1	Matt
Fraction	Fraction	k1gInSc4	Fraction
a	a	k8xC	a
Salvador	Salvador	k1gMnSc1	Salvador
Larroca	Larroca	k1gMnSc1	Larroca
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Invincible	Invincible	k1gFnSc2	Invincible
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
-	-	kIx~	-
Ultimátní	Ultimátní	k2eAgInSc1d1	Ultimátní
komiksový	komiksový	k2eAgInSc1d1	komiksový
komplet	komplet	k1gInSc1	komplet
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
-	-	kIx~	-
Démon	démon	k1gMnSc1	démon
v	v	k7c6	v
lahvi	lahev	k1gFnSc6	lahev
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Michelinie	Michelinie	k1gFnSc2	Michelinie
a	a	k8xC	a
John	John	k1gMnSc1	John
Romita	Romita	k1gMnSc1	Romita
jr	jr	k?	jr
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
#	#	kIx~	#
<g/>
120	[number]	k4	120
<g/>
-	-	kIx~	-
<g/>
128	[number]	k4	128
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
-	-	kIx~	-
Ultimátní	Ultimátní	k2eAgInSc1d1	Ultimátní
komiksový	komiksový	k2eAgInSc1d1	komiksový
komplet	komplet	k1gInSc1	komplet
#	#	kIx~	#
<g/>
85	[number]	k4	85
<g/>
:	:	kIx,	:
Marvel	Marvel	k1gInSc1	Marvel
<g/>
:	:	kIx,	:
Počátky	počátek	k1gInPc1	počátek
-	-	kIx~	-
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Larry	Larr	k1gInPc1	Larr
Lieber	Lieber	k1gInSc1	Lieber
a	a	k8xC	a
Don	Don	k1gMnSc1	Don
Heck	Heck	k1gMnSc1	Heck
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Tales	Tales	k1gInSc1	Tales
of	of	k?	of
Suspense	suspens	k1gFnSc2	suspens
#	#	kIx~	#
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
-	-	kIx~	-
Ultimátní	Ultimátní	k2eAgInSc1d1	Ultimátní
komiksový	komiksový	k2eAgInSc1d1	komiksový
komplet	komplet	k1gInSc1	komplet
#	#	kIx~	#
<g/>
91	[number]	k4	91
<g/>
:	:	kIx,	:
Invincible	Invincible	k1gFnPc2	Invincible
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
-	-	kIx~	-
Tragédie	tragédie	k1gFnSc1	tragédie
a	a	k8xC	a
triumf	triumf	k1gInSc1	triumf
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Archie	Archie	k1gFnPc1	Archie
Goodwin	Goodwin	k1gInSc1	Goodwin
<g/>
,	,	kIx,	,
Gene	gen	k1gInSc5	gen
Colan	Colan	k1gInSc4	Colan
a	a	k8xC	a
Johny	John	k1gMnPc4	John
Craig	Craiga	k1gFnPc2	Craiga
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Tales	Tales	k1gInSc1	Tales
of	of	k?	of
Suspense	suspens	k1gFnSc2	suspens
#	#	kIx~	#
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
and	and	k?	and
Sub-Mariner	Sub-Mariner	k1gMnSc1	Sub-Mariner
#	#	kIx~	#
<g/>
1	[number]	k4	1
a	a	k8xC	a
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
-	-	kIx~	-
Nejmocnější	mocný	k2eAgMnPc1d3	nejmocnější
hrdinové	hrdina	k1gMnPc1	hrdina
<g />
.	.	kIx.	.
</s>
<s>
Marvelu	Marvel	k1gMnSc3	Marvel
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Kurt	Kurt	k1gMnSc1	Kurt
Busiek	Busiek	k1gMnSc1	Busiek
a	a	k8xC	a
Sean	Sean	k1gMnSc1	Sean
Chen	Chen	k1gMnSc1	Chen
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
#	#	kIx~	#
<g/>
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
;	;	kIx,	;
Joe	Joe	k1gFnSc1	Joe
Quesada	Quesada	k1gFnSc1	Quesada
a	a	k8xC	a
Sean	Sean	k1gMnSc1	Sean
Chen	Chen	k1gMnSc1	Chen
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
Vol	vol	k6eAd1	vol
<g />
.	.	kIx.	.
</s>
<s>
<g/>
3	[number]	k4	3
#	#	kIx~	#
<g/>
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
;	;	kIx,	;
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Larry	Larr	k1gInPc1	Larr
Lieber	Lieber	k1gInSc1	Lieber
a	a	k8xC	a
Don	Don	k1gMnSc1	Don
Heck	Heck	k1gMnSc1	Heck
<g/>
:	:	kIx,	:
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Suspense	suspense	k1gFnSc1	suspense
#	#	kIx~	#
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
-	-	kIx~	-
Ultimátní	Ultimátní	k2eAgInSc1d1	Ultimátní
komiksový	komiksový	k2eAgInSc1d1	komiksový
komplet	komplet	k1gInSc1	komplet
#	#	kIx~	#
<g/>
101	[number]	k4	101
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Invincible	Invincible	k1gMnSc1	Invincible
Iron	iron	k1gInSc1	iron
<g />
.	.	kIx.	.
</s>
<s>
Man	Man	k1gMnSc1	Man
-	-	kIx~	-
Začátek	začátek	k1gInSc1	začátek
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc5	autor
<g/>
:	:	kIx,	:
Archie	Archie	k1gFnSc1	Archie
Goodwin	Goodwina	k1gFnPc2	Goodwina
a	a	k8xC	a
George	Georg	k1gMnSc2	Georg
Tuska	Tusek	k1gMnSc2	Tusek
<g/>
:	:	kIx,	:
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
#	#	kIx~	#
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Jon	Jon	k1gFnSc2	Jon
Favreau	Favreaus	k1gInSc2	Favreaus
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
-	-	kIx~	-
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
2	[number]	k4	2
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Jon	Jon	k1gFnSc2	Jon
Favreau	Favreaus	k1gInSc2	Favreaus
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
-	-	kIx~	-
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
3	[number]	k4	3
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Shane	Shan	k1gInSc5	Shan
Black	Black	k1gInSc4	Black
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
-	-	kIx~	-
The	The	k1gMnSc1	The
Marvel	Marvel	k1gMnSc1	Marvel
<g />
.	.	kIx.	.
</s>
<s>
Super	super	k2eAgInSc1d1	super
Heroes	Heroes	k1gInSc1	Heroes
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
-	-	kIx~	-
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
26	[number]	k4	26
dílů	díl	k1gInPc2	díl
2007	[number]	k4	2007
-	-	kIx~	-
The	The	k1gFnSc6	The
Invincible	Invincible	k1gFnPc2	Invincible
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
-	-	kIx~	-
film	film	k1gInSc1	film
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
-	-	kIx~	-
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
<g/>
:	:	kIx,	:
Armored	Armored	k1gMnSc1	Armored
Adventures	Adventures	k1gMnSc1	Adventures
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
52	[number]	k4	52
dílů	díl	k1gInPc2	díl
2013	[number]	k4	2013
-	-	kIx~	-
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Rise	Rise	k1gFnSc1	Rise
of	of	k?	of
Technovore	Technovor	k1gInSc5	Technovor
-	-	kIx~	-
film	film	k1gInSc1	film
2013	[number]	k4	2013
-	-	kIx~	-
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
and	and	k?	and
Hulk	Hulk	k1gMnSc1	Hulk
<g/>
:	:	kIx,	:
Heroes	Heroes	k1gMnSc1	Heroes
United	United	k1gMnSc1	United
-	-	kIx~	-
film	film	k1gInSc1	film
2014	[number]	k4	2014
-	-	kIx~	-
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
and	and	k?	and
Captain	Captain	k1gMnSc1	Captain
America	America	k1gMnSc1	America
<g/>
:	:	kIx,	:
Heroes	Heroes	k1gMnSc1	Heroes
United	United	k1gMnSc1	United
-	-	kIx~	-
film	film	k1gInSc1	film
Mark	Mark	k1gMnSc1	Mark
I	I	kA	I
-	-	kIx~	-
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
Iron	iron	k1gInSc4	iron
Manovo	Manův	k2eAgNnSc1d1	Manovo
brnění	brnění	k1gNnSc1	brnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
během	během	k7c2	během
zajetí	zajetí	k1gNnSc2	zajetí
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ho	on	k3xPp3gNnSc4	on
ze	z	k7c2	z
součástek	součástka	k1gFnPc2	součástka
na	na	k7c4	na
rakety	raketa	k1gFnPc4	raketa
Jericho	Jericho	k1gNnSc4	Jericho
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
tohoto	tento	k3xDgNnSc2	tento
brnění	brnění	k1gNnSc2	brnění
unikl	uniknout	k5eAaPmAgMnS	uniknout
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
brnění	brnění	k1gNnPc1	brnění
se	se	k3xPyFc4	se
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
kdesi	kdesi	k6eAd1	kdesi
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
našli	najít	k5eAaPmAgMnP	najít
vědci	vědec	k1gMnPc1	vědec
ze	z	k7c2	z
Stark	Stark	k1gInSc4	Stark
Industries	Industriesa	k1gFnPc2	Industriesa
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
II	II	kA	II
-	-	kIx~	-
Brnění	brnění	k1gNnSc1	brnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Tony	Tony	k1gMnSc1	Tony
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
silné	silný	k2eAgNnSc1d1	silné
a	a	k8xC	a
odolné	odolný	k2eAgNnSc1d1	odolné
jenže	jenže	k8xC	jenže
bylo	být	k5eAaImAgNnS	být
nedotažené	dotažený	k2eNgNnSc1d1	nedotažené
a	a	k8xC	a
časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
chyby	chyba	k1gFnPc1	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
III	III	kA	III
-	-	kIx~	-
Nové	Nové	k2eAgNnSc1d1	Nové
brnění	brnění	k1gNnSc1	brnění
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
Tony	Tony	k1gMnSc1	Tony
snažil	snažit	k5eAaImAgMnS	snažit
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
všech	všecek	k3xTgFnPc2	všecek
možných	možný	k2eAgFnPc2d1	možná
nedokonalostí	nedokonalost	k1gFnPc2	nedokonalost
jenž	jenž	k3xRgMnSc1	jenž
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
Mark	Mark	k1gMnSc1	Mark
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
oblek	oblek	k1gInSc1	oblek
měl	mít	k5eAaImAgInS	mít
barvu	barva	k1gFnSc4	barva
červenozlatou	červenozlatý	k2eAgFnSc4d1	červenozlatá
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
95	[number]	k4	95
procent	procento	k1gNnPc2	procento
z	z	k7c2	z
Titanu	titan	k1gInSc2	titan
a	a	k8xC	a
5	[number]	k4	5
procent	procento	k1gNnPc2	procento
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Vevnitř	vevnitř	k6eAd1	vevnitř
je	být	k5eAaImIp3nS	být
potažen	potáhnout	k5eAaPmNgMnS	potáhnout
kevlarem	kevlar	k1gInSc7	kevlar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
brnění	brnění	k1gNnSc2	brnění
vychází	vycházet	k5eAaImIp3nS	vycházet
všechny	všechen	k3xTgFnPc4	všechen
pozdější	pozdní	k2eAgFnPc4d2	pozdější
Tonyho	Tony	k1gMnSc4	Tony
brnění	brnění	k1gNnSc2	brnění
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
IV	IV	kA	IV
-	-	kIx~	-
Mark	Mark	k1gMnSc1	Mark
IV	IV	kA	IV
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
Mark	Mark	k1gMnSc1	Mark
III	III	kA	III
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Mongerem	Monger	k1gInSc7	Monger
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
byly	být	k5eAaImAgInP	být
výrazně	výrazně	k6eAd1	výrazně
vylepšeny	vylepšen	k2eAgInPc1d1	vylepšen
pouze	pouze	k6eAd1	pouze
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
V	V	kA	V
-	-	kIx~	-
Mark	Mark	k1gMnSc1	Mark
V	v	k7c6	v
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejlehčí	lehký	k2eAgNnSc1d3	nejlehčí
brnění	brnění	k1gNnSc1	brnění
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
<g/>
,	,	kIx,	,
celé	celá	k1gFnPc1	celá
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
z	z	k7c2	z
Titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
proto	proto	k8xC	proto
pouhých	pouhý	k2eAgInPc2d1	pouhý
30	[number]	k4	30
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
přenášet	přenášet	k5eAaImF	přenášet
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
balíčku	balíček	k1gInSc6	balíček
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
,	,	kIx,	,
skenuje	skenovat	k5eAaImIp3nS	skenovat
svého	svůj	k3xOyFgMnSc4	svůj
majitele	majitel	k1gMnSc4	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
identity	identita	k1gFnPc1	identita
nesedí	sedit	k5eNaImIp3nP	sedit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
brnění	brnění	k1gNnSc1	brnění
zloděje	zloděj	k1gMnSc2	zloděj
spoutá	spoutat	k5eAaPmIp3nS	spoutat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
při	při	k7c6	při
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Whiplashem	Whiplash	k1gInSc7	Whiplash
<g/>
.	.	kIx.	.
</s>
<s>
Mk	Mk	k?	Mk
VI	VI	kA	VI
-	-	kIx~	-
Mk	Mk	k1gMnSc4	Mk
VI	VI	kA	VI
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
a	a	k8xC	a
nejvýkonnější	výkonný	k2eAgNnSc1d3	nejvýkonnější
brnění	brnění	k1gNnSc1	brnění
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyměněna	vyměnit	k5eAaPmNgFnS	vyměnit
hrudní	hrudní	k2eAgFnSc1d1	hrudní
část	část	k1gFnSc1	část
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
hodila	hodit	k5eAaPmAgFnS	hodit
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
zdroje	zdroj	k1gInSc2	zdroj
v	v	k7c6	v
Tonyho	Tony	k1gMnSc2	Tony
hrudi	hruď	k1gFnSc6	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
brnění	brnění	k1gNnSc1	brnění
je	být	k5eAaImIp3nS	být
vyzbrojeno	vyzbrojit	k5eAaPmNgNnS	vyzbrojit
extrémně	extrémně	k6eAd1	extrémně
výkonnými	výkonný	k2eAgInPc7d1	výkonný
lasery	laser	k1gInPc7	laser
a	a	k8xC	a
různými	různý	k2eAgFnPc7d1	různá
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
brnění	brnění	k1gNnSc1	brnění
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
Tony	Tony	k1gMnSc1	Tony
sundal	sundat	k5eAaPmAgMnS	sundat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
i	i	k9	i
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
7	[number]	k4	7
-	-	kIx~	-
Mark	Mark	k1gMnSc1	Mark
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
nejvyzbrojenější	vyzbrojený	k2eAgFnSc1d3	vyzbrojený
a	a	k8xC	a
nejmodernější	moderní	k2eAgFnSc1d3	nejmodernější
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
Tonyho	Tony	k1gMnSc4	Tony
brnění	brnění	k1gNnSc2	brnění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
Mark	Mark	k1gMnSc1	Mark
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyzbrojené	vyzbrojený	k2eAgNnSc1d1	vyzbrojené
mnoha	mnoho	k4c7	mnoho
zbraněmi	zbraň	k1gFnPc7	zbraň
včetně	včetně	k7c2	včetně
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
laserů	laser	k1gInPc2	laser
<g/>
.	.	kIx.	.
</s>
<s>
Motory	motor	k1gInPc1	motor
na	na	k7c4	na
létání	létání	k1gNnSc4	létání
má	mít	k5eAaImIp3nS	mít
umístěné	umístěný	k2eAgNnSc1d1	umístěné
na	na	k7c4	na
více	hodně	k6eAd2	hodně
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
brnění	brnění	k1gNnSc1	brnění
bylo	být	k5eAaImAgNnS	být
navrženo	navržen	k2eAgNnSc1d1	navrženo
aby	aby	kYmCp3nS	aby
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
dostat	dostat	k5eAaPmF	dostat
Tonyho	Tony	k1gMnSc4	Tony
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
jakýsi	jakýsi	k3yIgInSc4	jakýsi
lepší	dobrý	k2eAgMnSc1d2	lepší
Mark	Mark	k1gMnSc1	Mark
V.	V.	kA	V.
War	War	k1gMnSc1	War
Machine	Machin	k1gInSc5	Machin
-	-	kIx~	-
War	War	k1gMnSc5	War
Machine	Machin	k1gMnSc5	Machin
je	být	k5eAaImIp3nS	být
brnění	brnění	k1gNnSc4	brnění
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
speciálně	speciálně	k6eAd1	speciálně
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
pilotem	pilot	k1gMnSc7	pilot
je	být	k5eAaImIp3nS	být
James	James	k1gMnSc1	James
Rhodes	Rhodes	k1gMnSc1	Rhodes
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
brnění	brnění	k1gNnSc1	brnění
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
Mark	Mark	k1gMnSc1	Mark
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tonym	Tony	k1gMnSc7	Tony
porazí	porazit	k5eAaPmIp3nS	porazit
James	James	k1gMnSc1	James
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obleku	oblek	k1gInSc6	oblek
Whiplashe	Whiplash	k1gFnSc2	Whiplash
<g/>
.	.	kIx.	.
</s>
<s>
Iron	iron	k1gInSc1	iron
Patriot	patriot	k1gMnSc1	patriot
-	-	kIx~	-
Toto	tento	k3xDgNnSc1	tento
brnění	brnění	k1gNnSc1	brnění
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
War	War	k1gFnSc2	War
Machine	Machin	k1gInSc5	Machin
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
barvy	barva	k1gFnPc4	barva
americké	americký	k2eAgFnSc2d1	americká
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
v	v	k7c4	v
Iron	iron	k1gInSc4	iron
Man	Man	k1gMnSc1	Man
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Monger	Monger	k1gInSc1	Monger
-	-	kIx~	-
Alias	alias	k9	alias
Iron	iron	k1gInSc1	iron
Monger	Monger	k1gMnSc1	Monger
je	být	k5eAaImIp3nS	být
brnění	brnění	k1gNnSc4	brnění
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
z	z	k7c2	z
Mark	Mark	k1gMnSc1	Mark
I	I	kA	I
(	(	kIx(	(
<g/>
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
poznámek	poznámka	k1gFnPc2	poznámka
Tonyho	Tony	k1gMnSc2	Tony
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
pilotem	pilot	k1gMnSc7	pilot
je	být	k5eAaImIp3nS	být
Obidiah	Obidiah	k1gInSc1	Obidiah
Stane	stanout	k5eAaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
o	o	k7c4	o
Omnium	omnium	k1gNnSc4	omnium
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
nezničitelnou	zničitelný	k2eNgFnSc4d1	nezničitelná
slitinu	slitina	k1gFnSc4	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
asi	asi	k9	asi
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
brnění	brnění	k1gNnSc4	brnění
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Uzvedne	uzvednout	k5eAaPmIp3nS	uzvednout
až	až	k9	až
50	[number]	k4	50
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
neměli	mít	k5eNaImAgMnP	mít
Mark	Mark	k1gMnSc1	Mark
I	i	k8xC	i
a	a	k8xC	a
Mark	Mark	k1gMnSc1	Mark
II	II	kA	II
vyřešené	vyřešený	k2eAgInPc4d1	vyřešený
ony	onen	k3xDgInPc4	onen
nedodělky	nedodělek	k1gInPc4	nedodělek
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yQgFnPc3	který
museli	muset	k5eAaImAgMnP	muset
být	být	k5eAaImF	být
odstraněni	odstraněn	k2eAgMnPc1d1	odstraněn
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
III	III	kA	III
proto	proto	k8xC	proto
chladem	chlad	k1gInSc7	chlad
dokázal	dokázat	k5eAaPmAgMnS	dokázat
ochromit	ochromit	k5eAaPmF	ochromit
Mongera	Monger	k1gMnSc4	Monger
<g/>
.	.	kIx.	.
</s>
<s>
Whiplash	Whiplash	k1gInSc1	Whiplash
-	-	kIx~	-
Toto	tento	k3xDgNnSc4	tento
brnění	brnění	k1gNnSc4	brnění
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc1	dva
úrovně	úroveň	k1gFnPc1	úroveň
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chráničů	chránič	k1gInPc2	chránič
a	a	k8xC	a
s	s	k7c7	s
chrániči	chránič	k1gInPc7	chránič
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
úrovně	úroveň	k1gFnPc1	úroveň
jsou	být	k5eAaImIp3nP	být
vyzbrojeny	vyzbrojen	k2eAgInPc4d1	vyzbrojen
plazmovými	plazmový	k2eAgInPc7d1	plazmový
řetězy	řetěz	k1gInPc7	řetěz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
cokoliv	cokoliv	k3yInSc4	cokoliv
rozříznout	rozříznout	k5eAaPmF	rozříznout
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
vynálezcem	vynálezce	k1gMnSc7	vynálezce
je	být	k5eAaImIp3nS	být
Ivan	Ivan	k1gMnSc1	Ivan
Vanko	Vanko	k1gNnSc4	Vanko
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
pomstít	pomstít	k5eAaPmF	pomstít
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
Antona	Anton	k1gMnSc4	Anton
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
s	s	k7c7	s
chrániči	chránič	k1gInSc6	chránič
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pod	pod	k7c4	pod
HammerTech	HammerTech	k1gInSc4	HammerTech
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
odolná	odolný	k2eAgFnSc1d1	odolná
vůči	vůči	k7c3	vůči
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
všem	všecek	k3xTgFnPc3	všecek
možným	možný	k2eAgFnPc3d1	možná
zbraním	zbraň	k1gFnPc3	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Mark	Mark	k1gMnSc1	Mark
42	[number]	k4	42
-	-	kIx~	-
Mark	Mark	k1gMnSc1	Mark
42	[number]	k4	42
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Iron	iron	k1gInSc1	iron
man	man	k1gMnSc1	man
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybavené	vybavený	k2eAgNnSc1d1	vybavené
samonavigačním	samonavigační	k2eAgNnSc7d1	samonavigační
skládáním	skládání	k1gNnSc7	skládání
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozloží	rozložit	k5eAaPmIp3nS	rozložit
<g/>
,	,	kIx,	,
po	po	k7c6	po
částech	část	k1gFnPc6	část
přiletí	přiletět	k5eAaPmIp3nS	přiletět
a	a	k8xC	a
složí	složit	k5eAaPmIp3nS	složit
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
majiteli	majitel	k1gMnSc6	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Hulk	Hulk	k1gInSc1	Hulk
Buster	Buster	k1gInSc1	Buster
-	-	kIx~	-
Toto	tento	k3xDgNnSc4	tento
brnění	brnění	k1gNnSc4	brnění
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
Tony	Tony	k1gFnSc4	Tony
Stark	Stark	k1gInSc1	Stark
pro	pro	k7c4	pro
situace	situace	k1gFnPc4	situace
kdy	kdy	k6eAd1	kdy
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zkrotit	zkrotit	k5eAaPmF	zkrotit
Hulka	Hulka	k1gFnSc1	Hulka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
brnění	brnění	k1gNnSc2	brnění
Tony	Tony	k1gFnSc4	Tony
vlétává	vlétávat	k5eAaImIp3nS	vlétávat
již	již	k6eAd1	již
oděn	odět	k5eAaPmNgInS	odět
v	v	k7c4	v
některýh	některýh	k1gInSc4	některýh
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
klasických	klasický	k2eAgNnPc2d1	klasické
brnění	brnění	k1gNnPc2	brnění
<g/>
.	.	kIx.	.
</s>
<s>
Hulk	Hulk	k1gMnSc1	Hulk
Buster	Buster	k1gMnSc1	Buster
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c4	v
Avengers	Avengers	k1gInSc4	Avengers
<g/>
:	:	kIx,	:
Age	Age	k1gMnSc1	Age
of	of	k?	of
Ultron	Ultron	k1gMnSc1	Ultron
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
-	-	kIx~	-
fiktivní	fiktivní	k2eAgFnPc1d1	fiktivní
biografie	biografie	k1gFnPc1	biografie
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
komiksů	komiks	k1gInPc2	komiks
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
Marvel	Marvel	k1gMnSc1	Marvel
<g/>
.	.	kIx.	.
<g/>
com_Universe_Iron_Man	com_Universe_Iron_Man	k1gMnSc1	com_Universe_Iron_Man
-	-	kIx~	-
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
biografie	biografie	k1gFnSc1	biografie
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
