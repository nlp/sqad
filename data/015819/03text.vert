<s>
Lagoa	Lagoa	k1gFnSc1
dos	dos	k?
Patos	patos	k1gInSc1
</s>
<s>
Lagoa	Lagoa	k1gFnSc1
dos	dos	k?
Patos	patos	k1gInSc1
satelitní	satelitní	k2eAgInSc1d1
snímek	snímek	k1gInSc1
Lagoa	Lagous	k1gMnSc2
dos	dos	k?
PatosPoloha	PatosPoloh	k1gMnSc4
Světadíl	světadíl	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
Brazílie	Brazílie	k1gFnSc2
Stát	stát	k1gInSc1
</s>
<s>
Rio	Rio	k?
Grande	grand	k1gMnSc5
do	do	k7c2
Sul	sout	k5eAaImAgMnS
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
31	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
51	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Rozměry	rozměra	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
</s>
<s>
10	#num#	k4
144	#num#	k4
km²	km²	k?
Délka	délka	k1gFnSc1
</s>
<s>
265	#num#	k4
km	km	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
60	#num#	k4
km	km	kA
Objem	objem	k1gInSc1
</s>
<s>
30	#num#	k4
km³	km³	k?
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
hloubka	hloubka	k1gFnSc1
</s>
<s>
7	#num#	k4
m	m	kA
Prům	Průma	k1gFnPc2
<g/>
.	.	kIx.
hloubka	hloubka	k1gFnSc1
</s>
<s>
3	#num#	k4
m	m	kA
Ostatní	ostatní	k2eAgInSc1d1
Typ	typ	k1gInSc1
</s>
<s>
laguna	laguna	k1gFnSc1
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
5	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Přítok	přítok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
Rio	Rio	k?
Jacuí	Jacuí	k1gFnSc1
ou	ou	k0
Lago	Lago	k1gNnSc4
Guaíba	Guaíba	k1gMnSc1
<g/>
,	,	kIx,
Rio	Rio	k1gMnSc1
Camaquã	Camaquã	k1gMnSc1
<g/>
,	,	kIx,
Sã	Sã	k1gMnSc1
Gonçalo	Gonçala	k1gFnSc5
Odtok	odtok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
průtok	průtok	k1gInSc1
do	do	k7c2
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
Ostrovy	ostrov	k1gInPc1
</s>
<s>
5	#num#	k4
Sídla	sídlo	k1gNnPc1
</s>
<s>
Porto	porto	k1gNnSc1
Alegre	Alegr	k1gInSc5
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lagoa	Lagoa	k1gFnSc1
dos	dos	k?
Patos	patos	k1gInSc4
je	být	k5eAaImIp3nS
mělké	mělký	k2eAgNnSc1d1
lagunové	lagunový	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
ve	v	k7c6
státě	stát	k1gInSc6
Rio	Rio	k1gFnSc2
Grande	grand	k1gMnSc5
do	do	k7c2
Sul	sout	k5eAaImAgInS
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Brazílie	Brazílie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
10	#num#	k4
144	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
265	#num#	k4
km	km	kA
dlouhé	dlouhý	k2eAgNnSc1d1
a	a	k8xC
maximálně	maximálně	k6eAd1
60	#num#	k4
km	km	kA
široké	široký	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
5	#num#	k4
m	m	kA
a	a	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
maximální	maximální	k2eAgFnSc2d1
hloubky	hloubka	k1gFnSc2
7	#num#	k4
m.	m.	k?
Při	při	k7c6
průměrné	průměrný	k2eAgFnSc6d1
hloubce	hloubka	k1gFnSc6
3	#num#	k4
m	m	kA
má	mít	k5eAaImIp3nS
celkový	celkový	k2eAgInSc1d1
objem	objem	k1gInSc1
30	#num#	k4
km³	km³	k?
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Lagunu	laguna	k1gFnSc4
napájejí	napájet	k5eAaImIp3nP
řeky	řeka	k1gFnPc1
Rio	Rio	k1gFnSc2
Jacuí	Jacuí	k1gFnSc2
ou	ou	k0
Lago	Lago	k1gNnSc4
Guaíba	Guaíba	k1gMnSc1
<g/>
,	,	kIx,
Rio	Rio	k1gMnSc1
Camaquã	Camaquã	k1gMnSc1
a	a	k8xC
Sã	Sã	k1gMnSc1
Gonçalo	Gonçala	k1gFnSc5
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
přitéká	přitékat	k5eAaImIp3nS
z	z	k7c2
jezera	jezero	k1gNnSc2
Lagoa	Lagoa	k1gMnSc1
Mirim	Mirim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
odtéká	odtékat	k5eAaImIp3nS
úzkým	úzký	k2eAgInSc7d1
průtokem	průtok	k1gInSc7
do	do	k7c2
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
</s>
<s>
Fauna	fauna	k1gFnSc1
a	a	k8xC
flóra	flóra	k1gFnSc1
</s>
<s>
Na	na	k7c6
laguně	laguna	k1gFnSc6
je	být	k5eAaImIp3nS
rozvinutý	rozvinutý	k2eAgInSc1d1
rybolov	rybolov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Osídlení	osídlení	k1gNnSc1
pobřeží	pobřeží	k1gNnSc2
</s>
<s>
V	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
leží	ležet	k5eAaImIp3nS
město	město	k1gNnSc1
Porto	porto	k1gNnSc4
Alegre	Alegr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Lagoa	Lago	k1gInSc2
dos	dos	k?
Patos	patos	k1gInSc1
na	na	k7c6
portugalské	portugalský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
П	П	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lagoa	Lago	k1gInSc2
dos	dos	k?
Patos	patos	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
239218136	#num#	k4
</s>
