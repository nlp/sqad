<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Trošky	troška	k1gFnSc2	troška
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pokračování	pokračování	k1gNnSc4	pokračování
pohádky	pohádka	k1gFnSc2	pohádka
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Noční	noční	k2eAgFnPc1d1	noční
scény	scéna	k1gFnPc1	scéna
při	při	k7c6	při
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Turkem	turek	k1gInSc7	turek
se	se	k3xPyFc4	se
natáčely	natáčet	k5eAaImAgFnP	natáčet
způsobem	způsob	k1gInSc7	způsob
Americké	americký	k2eAgFnPc1d1	americká
noci	noc	k1gFnPc1	noc
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
kaolínových	kaolínový	k2eAgInPc2d1	kaolínový
dolů	dol	k1gInPc2	dol
u	u	k7c2	u
Kaznějova	Kaznějův	k2eAgInSc2d1	Kaznějův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
2	[number]	k4	2
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
