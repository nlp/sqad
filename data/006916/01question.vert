<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
psal	psát	k5eAaImAgMnS	psát
rusky	rusky	k6eAd1	rusky
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Aleksandr	Aleksandr	k1gInSc1	Aleksandr
Lomm	Lomma	k1gFnPc2	Lomma
<g/>
?	?	kIx.	?
</s>
