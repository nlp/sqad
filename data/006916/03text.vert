<s>
Václav	Václav	k1gMnSc1	Václav
Klička	klička	k1gFnSc1	klička
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Čeremchovo	Čeremchův	k2eAgNnSc1d1	Čeremchův
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
-	-	kIx~	-
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
publikoval	publikovat	k5eAaBmAgMnS	publikovat
jen	jen	k9	jen
rusky	rusky	k6eAd1	rusky
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Alexandr	Alexandr	k1gMnSc1	Alexandr
Lomm	Lomm	k1gMnSc1	Lomm
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klička	klička	k1gFnSc1	klička
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
českému	český	k2eAgMnSc3d1	český
otci	otec	k1gMnSc3	otec
Josefu	Josef	k1gMnSc3	Josef
Kličkovi	Kliček	k1gMnSc3	Kliček
(	(	kIx(	(
<g/>
bývalému	bývalý	k2eAgMnSc3d1	bývalý
legionáři	legionář	k1gMnSc3	legionář
<g/>
)	)	kIx)	)
a	a	k8xC	a
ruské	ruský	k2eAgFnSc3d1	ruská
matce	matka	k1gFnSc3	matka
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
v	v	k7c6	v
Irkutské	irkutský	k2eAgFnSc6d1	Irkutská
oblasti	oblast	k1gFnSc6	oblast
poblíž	poblíž	k7c2	poblíž
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Josef	Josef	k1gMnSc1	Josef
Klička	klička	k1gFnSc1	klička
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
úřady	úřad	k1gInPc1	úřad
zatčen	zatknout	k5eAaPmNgMnS	zatknout
pro	pro	k7c4	pro
podezření	podezření	k1gNnSc4	podezření
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Klička	klička	k1gFnSc1	klička
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
sourozenci	sourozenec	k1gMnPc1	sourozenec
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Ruské	ruský	k2eAgNnSc4d1	ruské
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dospěl	dochvít	k5eAaPmAgMnS	dochvít
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
politickému	politický	k2eAgMnSc3d1	politický
"	"	kIx"	"
<g/>
škraloupu	škraloup	k1gInSc3	škraloup
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
znemožnily	znemožnit	k5eAaPmAgFnP	znemožnit
uplatnit	uplatnit	k5eAaPmF	uplatnit
se	se	k3xPyFc4	se
v	v	k7c6	v
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jeho	jeho	k3xOp3gNnSc1	jeho
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
schopnostem	schopnost	k1gFnPc3	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
<g/>
,	,	kIx,	,
než	než	k8xS	než
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pracovním	pracovní	k2eAgInSc6d1	pracovní
úrazu	úraz	k1gInSc6	úraz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
invalidním	invalidní	k2eAgMnSc7d1	invalidní
důchodcem	důchodce	k1gMnSc7	důchodce
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
živit	živit	k5eAaImF	živit
jako	jako	k9	jako
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
i	i	k9	i
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
psal	psát	k5eAaImAgInS	psát
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgInS	publikovat
převážně	převážně	k6eAd1	převážně
díla	dílo	k1gNnSc2	dílo
vědeckofantastické	vědeckofantastický	k2eAgFnSc2d1	vědeckofantastická
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
jazyce	jazyk	k1gInSc6	jazyk
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Alexandr	Alexandr	k1gMnSc1	Alexandr
Lomm	Lomm	k1gMnSc1	Lomm
(	(	kIx(	(
<g/>
А	А	k?	А
Л	Л	k?	Л
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
některá	některý	k3yIgNnPc4	některý
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
С	С	k?	С
с	с	k?	с
з	з	k?	з
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Případ	případ	k1gInSc1	případ
se	s	k7c7	s
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
П	П	k?	П
<g/>
"	"	kIx"	"
д	д	k?	д
Э	Э	k?	Э
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Zločin	zločin	k1gInSc1	zločin
<g />
.	.	kIx.	.
</s>
<s>
doktora	doktor	k1gMnSc4	doktor
Elliota	Elliot	k1gMnSc4	Elliot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc1	sci-fi
novela	novela	k1gFnSc1	novela
odehrávající	odehrávající	k2eAgFnSc1d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
mezi	mezi	k7c4	mezi
kapitalisty	kapitalista	k1gMnPc4	kapitalista
a	a	k8xC	a
gangstery	gangster	k1gMnPc4	gangster
<g/>
.	.	kIx.	.
В	В	k?	В
т	т	k?	т
г	г	k?	г
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
V	v	k7c6	v
temném	temný	k2eAgNnSc6d1	temné
městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
odboje	odboj	k1gInSc2	odboj
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
М	М	k?	М
ц	ц	k?	ц
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Mravenčí	mravenčí	k2eAgInSc1d1	mravenčí
<g />
.	.	kIx.	.
</s>
<s>
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc1	sci-fi
novela	novela	k1gFnSc1	novela
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
robota	robot	k1gMnSc2	robot
velikosti	velikost	k1gFnSc2	velikost
mravence	mravenec	k1gMnSc2	mravenec
<g/>
.	.	kIx.	.
Н	Н	k?	Н
О	О	k?	О
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Noční	noční	k2eAgMnSc1d1	noční
orel	orel	k1gMnSc1	orel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc1	sci-fi
román	román	k1gInSc1	román
odehrávající	odehrávající	k2eAgFnSc2d1	odehrávající
se	se	k3xPyFc4	se
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
výsadkář	výsadkář	k1gMnSc1	výsadkář
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
při	při	k7c6	při
seskoku	seskok	k1gInSc6	seskok
<g />
.	.	kIx.	.
</s>
<s>
porouchá	porouchat	k5eAaPmIp3nS	porouchat
padák	padák	k1gInSc1	padák
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
létat	létat	k5eAaImF	létat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pak	pak	k6eAd1	pak
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
partyzánském	partyzánský	k2eAgInSc6d1	partyzánský
boji	boj	k1gInSc6	boj
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
С	С	k?	С
А	А	k?	А
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Ahasvérův	Ahasvérův	k2eAgInSc1d1	Ahasvérův
skafandr	skafandr	k1gInSc1	skafandr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc1	sci-fi
povídka	povídka	k1gFnSc1	povídka
<g/>
.	.	kIx.	.
И	И	k?	И
н	н	k?	н
б	б	k?	б
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Obr	obr	k1gMnSc1	obr
nad	nad	k7c7	nad
propastí	propast	k1gFnSc7	propast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc1	sci-fi
román	román	k1gInSc1	román
<g/>
.	.	kIx.	.
Н	Н	k?	Н
О	О	k?	О
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
Noční	noční	k2eAgMnSc1d1	noční
orel	orel	k1gMnSc1	orel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
sci-fi	scii	k1gNnSc2	sci-fi
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
román	román	k1gInSc4	román
Н	Н	k?	Н
О	О	k?	О
(	(	kIx(	(
<g/>
Noční	noční	k2eAgMnSc1d1	noční
orel	orel	k1gMnSc1	orel
<g/>
)	)	kIx)	)
a	a	k8xC	a
povídky	povídka	k1gFnSc2	povídka
В	В	k?	В
т	т	k?	т
г	г	k?	г
(	(	kIx(	(
<g/>
V	v	k7c6	v
temném	temný	k2eAgNnSc6d1	temné
městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
У	У	k?	У
п	п	k?	п
Д	Д	k?	Д
М	М	k?	М
(	(	kIx(	(
<g/>
Pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
přeměna	přeměna	k1gFnSc1	přeměna
Dicka	Dicek	k1gMnSc2	Dicek
Murrayho	Murray	k1gMnSc2	Murray
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ч	Ч	k?	Ч
в	в	k?	в
к	к	k?	к
(	(	kIx(	(
<g/>
Čerti	čert	k1gMnPc1	čert
v	v	k7c6	v
kovárně	kovárna	k1gFnSc6	kovárna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
П	П	k?	П
у	у	k?	у
с	с	k?	с
<g/>
((	((	k?	((
(	(	kIx(	(
<g/>
Poslední	poslední	k2eAgMnSc1d1	poslední
umírá	umírat	k5eAaImIp3nS	umírat
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
К	К	k?	К
к	к	k?	к
к	к	k?	к
(	(	kIx(	(
<g/>
Konec	konec	k1gInSc1	konec
krále	král	k1gMnSc2	král
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
)	)	kIx)	)
a	a	k8xC	a
С	С	k?	С
А	А	k?	А
(	(	kIx(	(
<g/>
Ahasvérův	Ahasvérův	k2eAgInSc1d1	Ahasvérův
skafandr	skafandr	k1gInSc1	skafandr
<g/>
)	)	kIx)	)
Д	Д	k?	Д
п	п	k?	п
З	З	k?	З
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Drion	Drion	k1gInSc1	Drion
opouští	opouštět	k5eAaImIp3nS	opouštět
zemi	zem	k1gFnSc4	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc4	sci-fi
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgNnSc4d1	poslední
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
autor	autor	k1gMnSc1	autor
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
napsal	napsat	k5eAaBmAgMnS	napsat
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
přistávají	přistávat	k5eAaImIp3nP	přistávat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
mimozemšťané	mimozemšťan	k1gMnPc1	mimozemšťan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
pátrají	pátrat	k5eAaImIp3nP	pátrat
po	po	k7c6	po
planetách	planeta	k1gFnPc6	planeta
postižených	postižený	k2eAgFnPc2d1	postižená
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
chorobou	choroba	k1gFnSc7	choroba
guolou	guola	k1gFnSc7	guola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
civilizace	civilizace	k1gFnSc1	civilizace
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
je	být	k5eAaImIp3nS	být
nucena	nucen	k2eAgFnSc1d1	nucena
vést	vést	k5eAaImF	vést
války	válka	k1gFnPc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zrozený	zrozený	k2eAgMnSc1d1	zrozený
bez	bez	k7c2	bez
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
noviny	novina	k1gFnPc1	novina
Práce	práce	k1gFnSc1	práce
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Zkrocení	zkrocení	k1gNnSc1	zkrocení
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
noviny	novina	k1gFnPc1	novina
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Mikojové	Mikojový	k2eAgFnPc1d1	Mikojový
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
noviny	novina	k1gFnPc1	novina
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgMnSc1d1	noční
orel	orel	k1gMnSc1	orel
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Olga	Olga	k1gFnSc1	Olga
Ptáčková-Macháčková	Ptáčková-Macháčková	k1gFnSc1	Ptáčková-Macháčková
<g/>
.	.	kIx.	.
</s>
<s>
Drion	Drion	k1gInSc1	Drion
opouští	opouštět	k5eAaImIp3nS	opouštět
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Metanoona	Metanoona	k1gFnSc1	Metanoona
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc1	sci-fi
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
kybernetický	kybernetický	k2eAgMnSc1d1	kybernetický
mravenec	mravenec	k1gMnSc1	mravenec
vymkne	vymknout	k5eAaPmIp3nS	vymknout
svým	svůj	k3xOyFgMnPc3	svůj
tvůrcům	tvůrce	k1gMnPc3	tvůrce
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
ujme	ujmout	k5eAaPmIp3nS	ujmout
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
všemi	všecek	k3xTgMnPc7	všecek
mravenci	mravenec	k1gMnPc7	mravenec
světa	svět	k1gInSc2	svět
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
donutí	donutit	k5eAaPmIp3nP	donutit
lidstvo	lidstvo	k1gNnSc4	lidstvo
k	k	k7c3	k
odzbrojení	odzbrojení	k1gNnSc3	odzbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Keep	Keep	k1gInSc1	Keep
Smiling	Smiling	k1gInSc1	Smiling
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
fanzin	fanzin	k2eAgInSc1d1	fanzin
Trifid	Trifid	k1gInSc1	Trifid
1987	[number]	k4	1987
<g/>
/	/	kIx~	/
<g/>
Epsilon	epsilon	k1gNnSc6	epsilon
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
číhá	číhat	k5eAaImIp3nS	číhat
u	u	k7c2	u
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
detektivní	detektivní	k2eAgInSc1d1	detektivní
román	román	k1gInSc1	román
s	s	k7c7	s
fantastickým	fantastický	k2eAgInSc7d1	fantastický
námětem	námět	k1gInSc7	námět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
policie	policie	k1gFnSc1	policie
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
záhadné	záhadný	k2eAgFnPc4d1	záhadná
vraždy	vražda	k1gFnPc4	vražda
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
skladatel	skladatel	k1gMnSc1	skladatel
Leoš	Leoš	k1gMnSc1	Leoš
Lorn	Lorn	k1gMnSc1	Lorn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hudba	hudba	k1gFnSc1	hudba
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
nutkání	nutkání	k1gNnSc4	nutkání
k	k	k7c3	k
vraždě	vražda	k1gFnSc3	vražda
<g/>
.	.	kIx.	.
</s>
