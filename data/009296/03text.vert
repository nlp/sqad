<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Mexika	Mexiko	k1gNnSc2	Mexiko
je	být	k5eAaImIp3nS	být
zeleno-bílo-červená	zelenoílo-červený	k2eAgFnSc1d1	zeleno-bílo-červený
trikolóra	trikolóra	k1gFnSc1	trikolóra
se	s	k7c7	s
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
vlajky	vlajka	k1gFnSc2	vlajka
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc1	poměr
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
naposledy	naposledy	k6eAd1	naposledy
změněný	změněný	k2eAgInSc1d1	změněný
roku	rok	k1gInSc3	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
<g/>
Znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
hnědým	hnědý	k2eAgMnSc7d1	hnědý
orlem	orel	k1gMnSc7	orel
stojícím	stojící	k2eAgMnSc7d1	stojící
na	na	k7c6	na
zeleném	zelený	k2eAgInSc6d1	zelený
nopálovém	nopálový	k2eAgInSc6d1	nopálový
kaktusu	kaktus	k1gInSc6	kaktus
<g/>
,	,	kIx,	,
vyrůstajícím	vyrůstající	k2eAgMnSc7d1	vyrůstající
ze	z	k7c2	z
světle	světle	k6eAd1	světle
hnědé	hnědý	k2eAgFnSc2d1	hnědá
skály	skála	k1gFnSc2	skála
v	v	k7c6	v
modrých	modrý	k2eAgFnPc6d1	modrá
jezerních	jezerní	k2eAgFnPc6d1	jezerní
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
zobrazený	zobrazený	k2eAgMnSc1d1	zobrazený
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
a	a	k8xC	a
v	v	k7c6	v
bojovém	bojový	k2eAgNnSc6d1	bojové
postavení	postavení	k1gNnSc6	postavení
hledí	hledět	k5eAaImIp3nS	hledět
heraldicky	heraldicky	k6eAd1	heraldicky
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
a	a	k8xC	a
pravém	pravý	k2eAgInSc6d1	pravý
pařátu	pařát	k1gInSc6	pařát
drží	držet	k5eAaImIp3nS	držet
zeleného	zelený	k2eAgMnSc4d1	zelený
hada	had	k1gMnSc4	had
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
půlkruhu	půlkruh	k1gInSc2	půlkruh
prohnuté	prohnutý	k2eAgFnSc2d1	prohnutá
ratolesti	ratolest	k1gFnSc2	ratolest
dubu	dub	k1gInSc2	dub
a	a	k8xC	a
vavřínu	vavřín	k1gInSc2	vavřín
<g/>
,	,	kIx,	,
svázané	svázaný	k2eAgNnSc1d1	svázané
zeleno-bílo-červenou	zelenoílo-červený	k2eAgFnSc7d1	zeleno-bílo-červený
stužkou	stužka	k1gFnSc7	stužka
v	v	k7c6	v
národních	národní	k2eAgFnPc6d1	národní
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rubové	rubový	k2eAgFnSc6d1	rubová
straně	strana	k1gFnSc6	strana
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
znak	znak	k1gInSc1	znak
převrácen	převrácen	k2eAgInSc1d1	převrácen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
orlí	orlí	k2eAgFnSc1d1	orlí
hlava	hlava	k1gFnSc1	hlava
směřovala	směřovat	k5eAaImAgFnS	směřovat
také	také	k9	také
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
<g/>
.	.	kIx.	.
<g/>
Znak	znak	k1gInSc1	znak
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
starou	starý	k2eAgFnSc4d1	stará
aztéckou	aztécký	k2eAgFnSc4d1	aztécká
legendu	legenda	k1gFnSc4	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
měli	mít	k5eAaImAgMnP	mít
(	(	kIx(	(
<g/>
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
boha	bůh	k1gMnSc2	bůh
slunce	slunce	k1gNnSc2	slunce
Huitzilopochtla	Huitzilopochtla	k1gFnSc2	Huitzilopochtla
<g/>
)	)	kIx)	)
postavit	postavit	k5eAaPmF	postavit
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvidí	uvidět	k5eAaPmIp3nS	uvidět
<g />
.	.	kIx.	.
</s>
<s>
tuto	tento	k3xDgFnSc4	tento
scénu	scéna	k1gFnSc4	scéna
<g/>
:	:	kIx,	:
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Texcoco	Texcoco	k6eAd1	Texcoco
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
založený	založený	k2eAgInSc1d1	založený
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1	Tenochtitlán
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
.	.	kIx.	.
<g/>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
představuje	představovat	k5eAaImIp3nS	představovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
čistotu	čistota	k1gFnSc4	čistota
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
jednotu	jednota	k1gFnSc4	jednota
spojení	spojení	k1gNnSc2	spojení
španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
,	,	kIx,	,
indiánské	indiánský	k2eAgFnSc2d1	indiánská
i	i	k8xC	i
míšenecké	míšenecký	k2eAgFnSc2d1	míšenecký
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mexika	Mexiko	k1gNnSc2	Mexiko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
předkolumbovské	předkolumbovský	k2eAgFnSc6d1	předkolumbovská
éře	éra	k1gFnSc6	éra
obýváno	obývat	k5eAaImNgNnS	obývat
mnoha	mnoho	k4c2	mnoho
indiánskými	indiánský	k2eAgFnPc7d1	indiánská
kulturami	kultura	k1gFnPc7	kultura
a	a	k8xC	a
existovaly	existovat	k5eAaImAgFnP	existovat
zde	zde	k6eAd1	zde
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
říše	říš	k1gFnPc1	říš
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc4	ten
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
Zapotékové	Zapotéková	k1gFnSc2	Zapotéková
<g/>
,	,	kIx,	,
Toltékové	Toltéková	k1gFnSc2	Toltéková
<g/>
,	,	kIx,	,
Mayové	Mayové	k2eAgFnSc1d1	Mayové
(	(	kIx(	(
<g/>
Mayská	mayský	k2eAgFnSc1d1	mayská
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Aztékové	Azték	k1gMnPc1	Azték
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1325	[number]	k4	1325
založili	založit	k5eAaPmAgMnP	založit
Aztékové	Azték	k1gMnPc1	Azték
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Tenochtitlán	Tenochtitlán	k2eAgMnSc1d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1428	[number]	k4	1428
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
městskému	městský	k2eAgInSc3d1	městský
státu	stát	k1gInSc3	stát
Azcapotzalco	Azcapotzalco	k1gNnSc1	Azcapotzalco
konfederace	konfederace	k1gFnSc2	konfederace
tří	tři	k4xCgInPc2	tři
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
,	,	kIx,	,
Texcoca	Texcoc	k2eAgMnSc4d1	Texcoc
a	a	k8xC	a
Tlacopánu	Tlacopán	k2eAgFnSc4d1	Tlacopán
a	a	k8xC	a
Aztécká	aztécký	k2eAgFnSc1d1	aztécká
říše	říše	k1gFnSc1	říše
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dominantní	dominantní	k2eAgFnSc7d1	dominantní
silou	síla	k1gFnSc7	síla
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Hernána	Hernán	k2eAgFnSc1d1	Hernána
Cortése	Cortése	k1gFnSc1	Cortése
toto	tento	k3xDgNnSc4	tento
impérium	impérium	k1gNnSc4	impérium
v	v	k7c6	v
letech	let	k1gInPc6	let
1519	[number]	k4	1519
<g/>
–	–	k?	–
<g/>
1521	[number]	k4	1521
zcela	zcela	k6eAd1	zcela
vyvrátili	vyvrátit	k5eAaPmAgMnP	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
na	na	k7c6	na
dobytém	dobytý	k2eAgNnSc6d1	dobyté
území	území	k1gNnSc6	území
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
Místokrálovství	Místokrálovství	k1gNnPc2	Místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
Mexiko	Mexiko	k1gNnSc4	Mexiko
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnPc4d1	přilehlá
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
státními	státní	k2eAgMnPc7d1	státní
(	(	kIx(	(
<g/>
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
<g/>
)	)	kIx)	)
vlajkami	vlajka	k1gFnPc7	vlajka
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mexika	Mexiko	k1gNnSc2	Mexiko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
vlajky	vlajka	k1gFnSc2	vlajka
Španělské	španělský	k2eAgNnSc1d1	španělské
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vzrůstalo	vzrůstat	k5eAaImAgNnS	vzrůstat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1810	[number]	k4	1810
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Miguel	Miguel	k1gMnSc1	Miguel
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
y	y	k?	y
Costilla	Costilla	k1gMnSc1	Costilla
<g/>
,	,	kIx,	,
farář	farář	k1gMnSc1	farář
z	z	k7c2	z
Atotonilco	Atotonilco	k1gMnSc1	Atotonilco
El	Ela	k1gFnPc2	Ela
Grande	grand	k1gMnSc5	grand
<g/>
,	,	kIx,	,
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
proti	proti	k7c3	proti
španělské	španělský	k2eAgFnSc3d1	španělská
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
neměl	mít	k5eNaImAgMnS	mít
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgInS	použít
prapor	prapor	k1gInSc4	prapor
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
procesí	procesí	k1gNnSc3	procesí
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Guadalupské	Guadalupský	k2eAgFnSc2d1	Guadalupská
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
praporem	prapor	k1gInSc7	prapor
bojovali	bojovat	k5eAaImAgMnP	bojovat
asi	asi	k9	asi
do	do	k7c2	do
března	březen	k1gInSc2	březen
1811	[number]	k4	1811
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
zajat	zajat	k2eAgMnSc1d1	zajat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
.6	.6	k4	.6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1813	[number]	k4	1813
přijal	přijmout	k5eAaPmAgInS	přijmout
národní	národní	k2eAgInSc1d1	národní
kongres	kongres	k1gInSc1	kongres
Deklaraci	deklarace	k1gFnSc4	deklarace
o	o	k7c6	o
suverenitě	suverenita	k1gFnSc6	suverenita
a	a	k8xC	a
nezávislosti	nezávislost	k1gFnSc6	nezávislost
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1815	[number]	k4	1815
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
vlajka	vlajka	k1gFnSc1	vlajka
Mexické	mexický	k2eAgFnSc2d1	mexická
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
modro-bílou	modroílý	k2eAgFnSc7d1	modro-bílá
šachovnicí	šachovnice	k1gFnSc7	šachovnice
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
lemem	lem	k1gInSc7	lem
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
obdélníku	obdélník	k1gInSc2	obdélník
uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
nový	nový	k2eAgInSc1d1	nový
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazená	zobrazený	k2eAgFnSc1d1	zobrazená
vlajka	vlajka	k1gFnSc1	vlajka
postrádá	postrádat	k5eAaImIp3nS	postrádat
oproti	oproti	k7c3	oproti
zdroji	zdroj	k1gInSc3	zdroj
bílý	bílý	k2eAgInSc1d1	bílý
obdélník	obdélník	k1gInSc1	obdélník
a	a	k8xC	a
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
není	být	k5eNaImIp3nS	být
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
ale	ale	k8xC	ale
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
některými	některý	k3yIgMnPc7	některý
vzbouřenci	vzbouřenec	k1gMnPc7	vzbouřenec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Bandera	Bandera	k1gFnSc1	Bandera
de	de	k?	de
los	los	k1gInSc1	los
Insurgentes	Insurgentes	k1gInSc1	Insurgentes
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Vlajka	vlajka	k1gFnSc1	vlajka
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
jednání	jednání	k1gNnSc6	jednání
španělského	španělský	k2eAgMnSc2d1	španělský
generála	generál	k1gMnSc2	generál
Agustína	Agustín	k1gMnSc2	Agustín
de	de	k?	de
Iturbide	Iturbid	k1gInSc5	Iturbid
s	s	k7c7	s
vůdcem	vůdce	k1gMnSc7	vůdce
protišpanělského	protišpanělský	k2eAgInSc2d1	protišpanělský
odboje	odboj	k1gInSc2	odboj
Vicentem	Vicent	k1gMnSc7	Vicent
Guerrerem	Guerrer	k1gMnSc7	Guerrer
v	v	k7c6	v
Iguale	Iguala	k1gFnSc6	Iguala
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1921	[number]	k4	1921
nechal	nechat	k5eAaPmAgMnS	nechat
Iturbide	Iturbid	k1gInSc5	Iturbid
vyrobit	vyrobit	k5eAaPmF	vyrobit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
prapory	prapor	k1gInPc7	prapor
<g/>
,	,	kIx,	,
rozdělené	rozdělený	k2eAgNnSc1d1	rozdělené
širokým	široký	k2eAgInSc7d1	široký
šikmým	šikmý	k2eAgInSc7d1	šikmý
zeleným	zelený	k2eAgInSc7d1	zelený
pruhem	pruh	k1gInSc7	pruh
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
z	z	k7c2	z
horního	horní	k2eAgInSc2d1	horní
rohu	roh	k1gInSc2	roh
do	do	k7c2	do
dolního	dolní	k2eAgInSc2d1	dolní
cípu	cíp	k1gInSc2	cíp
<g/>
)	)	kIx)	)
rozdělující	rozdělující	k2eAgFnSc4d1	rozdělující
vlajku	vlajka	k1gFnSc4	vlajka
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
(	(	kIx(	(
<g/>
vlající	vlající	k2eAgFnSc1d1	vlající
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červené	červený	k2eAgFnSc6d1	červená
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
(	(	kIx(	(
<g/>
žerďová	žerďová	k1gFnSc1	žerďová
<g/>
)	)	kIx)	)
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
rohu	roh	k1gInSc6	roh
byla	být	k5eAaImAgFnS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
zelená	zelený	k2eAgFnSc1d1	zelená
šesticípá	šesticípý	k2eAgFnSc1d1	šesticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
cípu	cíp	k1gInSc6	cíp
bílá	bílý	k2eAgFnSc1d1	bílá
šesticípá	šesticípý	k2eAgFnSc1d1	šesticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zeleném	zelený	k2eAgInSc6d1	zelený
pruhu	pruh	k1gInSc6	pruh
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
černý	černý	k2eAgInSc1d1	černý
nápis	nápis	k1gInSc1	nápis
UNYON	UNYON	kA	UNYON
RELYGION	RELYGION	kA	RELYGION
YNDEPENDENCIA	YNDEPENDENCIA	kA	YNDEPENDENCIA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
se	se	k3xPyFc4	se
inspirací	inspirace	k1gFnPc2	inspirace
pro	pro	k7c4	pro
barvy	barva	k1gFnPc4	barva
stal	stát	k5eAaPmAgMnS	stát
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mačetou	mačeta	k1gFnSc7	mačeta
rozpůlil	rozpůlit	k5eAaPmAgInS	rozpůlit
meloun	meloun	k1gInSc1	meloun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1821	[number]	k4	1821
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
Místokrálovství	Místokrálovství	k1gNnSc2	Místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Mexické	mexický	k2eAgNnSc1d1	mexické
císařství	císařství	k1gNnSc1	císařství
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
přijala	přijmout	k5eAaPmAgFnS	přijmout
provizorní	provizorní	k2eAgFnSc1d1	provizorní
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
junta	junta	k1gFnSc1	junta
dekret	dekret	k1gInSc1	dekret
s	s	k7c7	s
popisem	popis	k1gInSc7	popis
první	první	k4xOgFnSc2	první
statní	statnět	k5eAaImIp3nS	statnět
vlajky	vlajka	k1gFnPc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
tvořila	tvořit	k5eAaImAgFnS	tvořit
trikolóra	trikolóra	k1gFnSc1	trikolóra
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
zeleným	zelený	k2eAgInSc7d1	zelený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
vlajky	vlajka	k1gFnSc2	vlajka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
pruhu	pruh	k1gInSc2	pruh
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
korunovaný	korunovaný	k2eAgMnSc1d1	korunovaný
orel	orel	k1gMnSc1	orel
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1822	[number]	k4	1822
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
dalším	další	k2eAgInSc7d1	další
dekretem	dekret	k1gInSc7	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
seděl	sedět	k5eAaImAgMnS	sedět
orel	orel	k1gMnSc1	orel
na	na	k7c6	na
nopálovém	nopálový	k2eAgInSc6d1	nopálový
kaktusu	kaktus	k1gInSc6	kaktus
<g/>
,	,	kIx,	,
vyrůstajícím	vyrůstající	k2eAgFnPc3d1	vyrůstající
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
štítem	štít	k1gInSc7	štít
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1822	[number]	k4	1822
se	se	k3xPyFc4	se
Iturbide	Iturbid	k1gInSc5	Iturbid
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
prvním	první	k4xOgMnSc7	první
mexickým	mexický	k2eAgMnSc7d1	mexický
císařem	císař	k1gMnSc7	císař
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Augustín	Augustína	k1gFnPc2	Augustína
I.	I.	kA	I.
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
připojil	připojit	k5eAaPmAgInS	připojit
celou	celý	k2eAgFnSc4d1	celá
Střední	střední	k2eAgFnSc4d1	střední
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
i	i	k9	i
poté	poté	k6eAd1	poté
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nezměněna	změněn	k2eNgFnSc1d1	nezměněna
<g/>
.29	.29	k4	.29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1823	[number]	k4	1823
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
svržen	svržen	k2eAgMnSc1d1	svržen
a	a	k8xC	a
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
emigraci	emigrace	k1gFnSc6	emigrace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1823	[number]	k4	1823
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Mexiko	Mexiko	k1gNnSc4	Mexiko
a	a	k8xC	a
mexická	mexický	k2eAgFnSc1d1	mexická
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
mírně	mírně	k6eAd1	mírně
upravena	upravit	k5eAaPmNgFnS	upravit
(	(	kIx(	(
<g/>
stále	stále	k6eAd1	stále
však	však	k9	však
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
přibližně	přibližně	k6eAd1	přibližně
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
z	z	k7c2	z
orlí	orlí	k2eAgFnSc2d1	orlí
hlavy	hlava	k1gFnSc2	hlava
byla	být	k5eAaImAgFnS	být
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
orel	orel	k1gMnSc1	orel
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
hada	had	k1gMnSc2	had
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
emblému	emblém	k1gInSc2	emblém
byly	být	k5eAaImAgFnP	být
nově	nově	k6eAd1	nově
zkřížené	zkřížený	k2eAgFnPc1d1	zkřížená
ratolesti	ratolest	k1gFnPc1	ratolest
dubu	dub	k1gInSc2	dub
a	a	k8xC	a
vavřínu	vavřín	k1gInSc2	vavřín
převázané	převázaný	k2eAgInPc1d1	převázaný
červenou	červený	k2eAgFnSc7d1	červená
stuhou	stuha	k1gFnSc7	stuha
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1824	[number]	k4	1824
se	se	k3xPyFc4	se
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
stala	stát	k5eAaPmAgFnS	stát
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlajka	vlajka	k1gFnSc1	vlajka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
i	i	k9	i
po	po	k7c6	po
několika	několik	k4yIc6	několik
změnách	změna	k1gFnPc6	změna
názvu	název	k1gInSc2	název
státu	stát	k1gInSc2	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mexické	mexický	k2eAgFnSc2d1	mexická
(	(	kIx(	(
<g/>
4.10	[number]	k4	4.10
<g/>
.1824	.1824	k4	.1824
–	–	k?	–
1.1	[number]	k4	1.1
<g/>
.1837	.1837	k4	.1837
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
1.1	[number]	k4	1.1
<g/>
.1837	.1837	k4	.1837
–	–	k?	–
22.8	[number]	k4	22.8
<g/>
.1846	.1846	k4	.1846
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mexické	mexický	k2eAgFnSc2d1	mexická
(	(	kIx(	(
<g/>
22.8	[number]	k4	22.8
<g/>
.1846	.1846	k4	.1846
–	–	k?	–
20.4	[number]	k4	20.4
<g/>
.1853	.1853	k4	.1853
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
20.4	[number]	k4	20.4
<g/>
.1853	.1853	k4	.1853
–	–	k?	–
16.9	[number]	k4	16.9
<g/>
.1857	.1857	k4	.1857
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mexické	mexický	k2eAgFnSc2d1	mexická
(	(	kIx(	(
<g/>
16.9	[number]	k4	16.9
<g/>
.1857	.1857	k4	.1857
–	–	k?	–
21.4	[number]	k4	21.4
<g/>
.1862	.1862	k4	.1862
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
(	(	kIx(	(
<g/>
21.4	[number]	k4	21.4
<g/>
.1862	.1862	k4	.1862
–	–	k?	–
10.4	[number]	k4	10.4
<g/>
.1864	.1864	k4	.1864
<g/>
)	)	kIx)	)
<g/>
Po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
tzv.	tzv.	kA	tzv.
Druhé	druhý	k4xOgNnSc1	druhý
Mexické	mexický	k2eAgNnSc1d1	mexické
císařství	císařství	k1gNnSc1	císařství
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
Habsburským	habsburský	k2eAgInSc7d1	habsburský
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1864	[number]	k4	1864
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
tvořila	tvořit	k5eAaImAgFnS	tvořit
znovu	znovu	k6eAd1	znovu
trikolóra	trikolóra	k1gFnSc1	trikolóra
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
zeleným	zelený	k2eAgInSc7d1	zelený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
pruhu	pruh	k1gInSc2	pruh
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozích	roh	k1gInPc6	roh
listu	list	k1gInSc2	list
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
malí	malý	k2eAgMnPc1d1	malý
<g/>
,	,	kIx,	,
korunovaní	korunovaný	k2eAgMnPc1d1	korunovaný
orli	orel	k1gMnPc1	orel
s	s	k7c7	s
hadem	had	k1gMnSc7	had
v	v	k7c6	v
zobáku	zobák	k1gInSc6	zobák
<g/>
,	,	kIx,	,
sedící	sedící	k2eAgFnSc4d1	sedící
na	na	k7c6	na
kaktusu	kaktus	k1gInSc6	kaktus
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc1	všechen
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
ve	v	k7c6	v
žluté	žlutý	k2eAgFnSc6d1	žlutá
barvě	barva	k1gFnSc6	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orli	orel	k1gMnPc1	orel
leželi	ležet	k5eAaImAgMnP	ležet
na	na	k7c6	na
pomyslných	pomyslný	k2eAgFnPc6d1	pomyslná
úhlopříčkách	úhlopříčka	k1gFnPc6	úhlopříčka
s	s	k7c7	s
korunovanou	korunovaný	k2eAgFnSc7d1	korunovaná
hlavou	hlava	k1gFnSc7	hlava
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
znaku	znak	k1gInSc3	znak
(	(	kIx(	(
<g/>
orli	orel	k1gMnPc1	orel
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
znaku	znak	k1gInSc2	znak
(	(	kIx(	(
<g/>
orli	orel	k1gMnPc1	orel
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
<g/>
)	)	kIx)	)
<g/>
.15	.15	k4	.15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1867	[number]	k4	1867
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
názvu	název	k1gInSc2	název
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mexické	mexický	k2eAgInPc1d1	mexický
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
symbolů	symbol	k1gInPc2	symbol
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1823	[number]	k4	1823
<g/>
–	–	k?	–
<g/>
1864.30	[number]	k4	1864.30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1880	[number]	k4	1880
byly	být	k5eAaImAgInP	být
mexické	mexický	k2eAgInPc1d1	mexický
státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
novými	nový	k2eAgMnPc7d1	nový
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
prezidenta	prezident	k1gMnSc2	prezident
Porfiria	Porfirium	k1gNnSc2	Porfirium
Díaze	Díaze	k1gFnSc2	Díaze
malíř	malíř	k1gMnSc1	malíř
Tomás	Tomása	k1gFnPc2	Tomása
de	de	k?	de
la	la	k1gNnSc4	la
Peñ	Peñ	k1gFnSc2	Peñ
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
trikolórou	trikolóra	k1gFnSc7	trikolóra
jako	jako	k8xS	jako
na	na	k7c6	na
předchozí	předchozí	k2eAgFnSc6d1	předchozí
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
uprostřed	uprostřed	k6eAd1	uprostřed
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
upravený	upravený	k2eAgInSc4d1	upravený
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
měl	mít	k5eAaImAgMnS	mít
méně	málo	k6eAd2	málo
roztažená	roztažený	k2eAgNnPc4d1	roztažené
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
ratolesti	ratolest	k1gFnPc4	ratolest
dubu	dub	k1gInSc2	dub
a	a	k8xC	a
vavřínu	vavřín	k1gInSc2	vavřín
si	se	k3xPyFc3	se
vyměnily	vyměnit	k5eAaPmAgFnP	vyměnit
pozice	pozice	k1gFnPc1	pozice
a	a	k8xC	a
převázány	převázán	k2eAgFnPc1d1	převázána
byly	být	k5eAaImAgFnP	být
bílou	bílý	k2eAgFnSc7d1	bílá
stuhou	stuha	k1gFnSc7	stuha
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1899	[number]	k4	1899
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
drobné	drobný	k2eAgFnSc3d1	drobná
úpravě	úprava	k1gFnSc3	úprava
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc4	znak
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sochař	sochař	k1gMnSc1	sochař
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
Dios	Dios	k1gInSc1	Dios
Fernández	Fernández	k1gInSc1	Fernández
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
měla	mít	k5eAaImAgFnS	mít
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Orel	Orel	k1gMnSc1	Orel
měl	mít	k5eAaImAgMnS	mít
plnější	plný	k2eAgInPc4d2	plnější
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
světlejší	světlý	k2eAgInSc1d2	světlejší
(	(	kIx(	(
<g/>
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
)	)	kIx)	)
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
ratolesti	ratolest	k1gFnPc1	ratolest
byly	být	k5eAaImAgFnP	být
zmenšeny	zmenšen	k2eAgFnPc1d1	zmenšena
a	a	k8xC	a
ohraničovaly	ohraničovat	k5eAaImAgFnP	ohraničovat
modré	modrý	k2eAgFnPc1d1	modrá
vody	voda	k1gFnPc1	voda
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1916	[number]	k4	1916
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
změněn	změnit	k5eAaPmNgInS	změnit
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k8xC	i
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
stylizace	stylizace	k1gFnSc2	stylizace
znaku	znak	k1gInSc2	znak
byl	být	k5eAaImAgMnS	být
malíř	malíř	k1gMnSc1	malíř
mexického	mexický	k2eAgNnSc2d1	mexické
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
pro	pro	k7c4	pro
archeologii	archeologie	k1gFnSc4	archeologie
<g/>
,	,	kIx,	,
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
etnografii	etnografie	k1gFnSc4	etnografie
Antonio	Antonio	k1gMnSc1	Antonio
Goméz	Goméza	k1gFnPc2	Goméza
R.	R.	kA	R.
Orel	Orel	k1gMnSc1	Orel
na	na	k7c6	na
znaku	znak	k1gInSc6	znak
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
byl	být	k5eAaImAgInS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
<g/>
,	,	kIx,	,
se	s	k7c7	s
skloněnou	skloněný	k2eAgFnSc7d1	skloněná
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
heraldicky	heraldicky	k6eAd1	heraldicky
hledící	hledící	k2eAgFnSc1d1	hledící
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
orlem	orel	k1gMnSc7	orel
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
do	do	k7c2	do
půlkruhu	půlkruh	k1gInSc2	půlkruh
prohnuté	prohnutý	k2eAgFnPc4d1	prohnutá
dubové	dubový	k2eAgFnPc4d1	dubová
a	a	k8xC	a
vavřínové	vavřínový	k2eAgFnPc4d1	vavřínová
ratolesti	ratolest	k1gFnPc4	ratolest
svázané	svázaný	k2eAgFnPc4d1	svázaná
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
bílou	bílý	k2eAgFnSc7d1	bílá
stuhou	stuha	k1gFnSc7	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Nopálový	nopálový	k2eAgInSc1d1	nopálový
kaktus	kaktus	k1gInSc1	kaktus
měl	mít	k5eAaImAgInS	mít
osm	osm	k4xCc4	osm
výhonků	výhonek	k1gInPc2	výhonek
s	s	k7c7	s
červenými	červený	k2eAgInPc7d1	červený
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
vyvěšena	vyvěsit	k5eAaPmNgFnS	vyvěsit
až	až	k6eAd1	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1917.5	[number]	k4	1917.5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1934	[number]	k4	1934
byly	být	k5eAaImAgFnP	být
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
<g/>
)	)	kIx)	)
změněny	změněn	k2eAgFnPc1d1	změněna
státní	státní	k2eAgFnPc1d1	státní
znak	znak	k1gInSc4	znak
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarný	výtvarný	k2eAgInSc1d1	výtvarný
návrh	návrh	k1gInSc1	návrh
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Jorge	Jorge	k1gFnSc4	Jorge
Enciso	Encisa	k1gFnSc5	Encisa
<g/>
.	.	kIx.	.
</s>
<s>
Ratolesti	ratolest	k1gFnPc1	ratolest
(	(	kIx(	(
<g/>
dubové	dubový	k2eAgFnPc1d1	dubová
a	a	k8xC	a
vavřínové	vavřínový	k2eAgFnPc1d1	vavřínová
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
svázány	svázán	k2eAgFnPc1d1	svázána
byly	být	k5eAaImAgFnP	být
zeleno-bílo-červenou	zelenoílo-červený	k2eAgFnSc7d1	zeleno-bílo-červený
stuhou	stuha	k1gFnSc7	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Výhonky	výhonek	k1gInPc1	výhonek
kaktusu	kaktus	k1gInSc2	kaktus
jsou	být	k5eAaImIp3nP	být
zredukovány	zredukovat	k5eAaPmNgFnP	zredukovat
na	na	k7c4	na
počet	počet	k1gInSc4	počet
rovný	rovný	k2eAgInSc4d1	rovný
pěti	pět	k4xCc3	pět
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c4	na
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Zobrazené	zobrazený	k2eAgFnPc1d1	zobrazená
vlajky	vlajka	k1gFnPc1	vlajka
z	z	k7c2	z
commons	commonsa	k1gFnPc2	commonsa
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
vlajek	vlajka	k1gFnPc2	vlajka
z	z	k7c2	z
uvedeného	uvedený	k2eAgInSc2d1	uvedený
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
byly	být	k5eAaImAgInP	být
naposledy	naposledy	k6eAd1	naposledy
změněny	změněn	k2eAgInPc4d1	změněn
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
vlakového	vlakový	k2eAgInSc2d1	vlakový
listu	list	k1gInSc2	list
na	na	k7c4	na
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
dalšími	další	k2eAgInPc7d1	další
zákony	zákon	k1gInPc7	zákon
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1995	[number]	k4	1995
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Den	den	k1gInSc4	den
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
slaví	slavit	k5eAaImIp3nS	slavit
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Den	den	k1gInSc1	den
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
připomínají	připomínat	k5eAaImIp3nP	připomínat
události	událost	k1gFnPc1	událost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
mexických	mexický	k2eAgInPc2d1	mexický
států	stát	k1gInPc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgInSc1d1	federativní
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
31	[number]	k4	31
států	stát	k1gInPc2	stát
a	a	k8xC	a
1	[number]	k4	1
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k1gNnSc1	México
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Mexika	Mexiko	k1gNnSc2	Mexiko
</s>
</p>
<p>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Mexika	Mexiko	k1gNnSc2	Mexiko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mexická	mexický	k2eAgFnSc1d1	mexická
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
