<s>
Německo	Německo	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2008	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2008	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
výpravyIOC	výpravyIOC	k?
kód	kód	k1gInSc4
</s>
<s>
GER	Gera	k1gFnPc2
</s>
<s>
ZlatáStříbrnáBronzováCelkem	ZlatáStříbrnáBronzováCelek	k1gInSc7
</s>
<s>
16	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
NOV	nov	k1gInSc1
</s>
<s>
Deutscher	Deutschra	k1gFnPc2
Olympischer	Olympischra	k1gFnPc2
Sportbund	Sportbund	k1gInSc1
((	((	k?
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
link	link	k1gMnSc1
<g/>
)	)	kIx)
Vlajkonoš	vlajkonoš	k1gMnSc1
</s>
<s>
Dirk	Dirk	k1gInSc1
Nowitzki	Nowitzk	k1gFnSc2
</s>
<s>
Německo	Německo	k1gNnSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
reprezentovala	reprezentovat	k5eAaImAgFnS
výprava	výprava	k1gFnSc1
420	#num#	k4
sportovců	sportovec	k1gMnPc2
(	(	kIx(
<g/>
237	#num#	k4
mužů	muž	k1gMnPc2
and	and	k?
183	#num#	k4
žen	žena	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
30	#num#	k4
sportech	sport	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Medailisté	medailista	k1gMnPc1
</s>
<s>
Medaile	medaile	k1gFnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
ZlatoAlexander	ZlatoAlexander	k1gMnSc1
Grimm	Grimm	k1gMnSc1
KanoistikaK	KanoistikaK	k1gMnSc1
<g/>
1	#num#	k4
slalom	slalom	k1gInSc1
muži	muž	k1gMnSc6
</s>
<s>
ZlatoOle	ZlatoOle	k6eAd1
Bischof	Bischof	k1gInSc1
JudoMuži	JudoMuž	k1gFnSc6
81	#num#	k4
kg	kg	kA
</s>
<s>
ZlatoPeter	ZlatoPeter	k1gMnSc1
ThomsenFrank	ThomsenFrank	k1gMnSc1
OstholtHinrich	OstholtHinrich	k1gMnSc1
RomeikeIngrid	RomeikeIngrida	k1gFnPc2
KlimkeAndreas	KlimkeAndreas	k1gMnSc1
Dibowski	Dibowsk	k1gFnSc2
JezdectvíJezdecká	JezdectvíJezdecký	k2eAgFnSc1d1
všestrannost	všestrannost	k1gFnSc1
-	-	kIx~
družstvo	družstvo	k1gNnSc1
</s>
<s>
ZlatoHinrich	ZlatoHinrich	k1gMnSc1
Romeike	Romeik	k1gInSc2
JezdectvíJezdecká	JezdectvíJezdecký	k2eAgFnSc1d1
všestrannost	všestrannost	k1gFnSc1
-	-	kIx~
jednotlivci	jednotlivec	k1gMnPc1
</s>
<s>
ZlatoBenjamin	ZlatoBenjamin	k2eAgInSc4d1
Kleibrink	Kleibrink	k1gInSc4
ŠermMuži	ŠermMuž	k1gFnSc3
fleret	fleret	k1gInSc1
</s>
<s>
ZlatoBritta	ZlatoBritta	k1gFnSc1
Heidemannová	Heidemannová	k1gFnSc1
ŠermŽeny	ŠermŽena	k1gFnSc2
kord	kord	k1gInSc1
</s>
<s>
ZlatoHeike	ZlatoHeike	k6eAd1
Kemmerová	Kemmerový	k2eAgFnSc1d1
Nadine	Nadin	k1gInSc5
CapellmannováIsabell	CapellmannováIsabell	k1gInSc1
Werthová	Werthový	k2eAgFnSc1d1
JezdectvíDrezura	JezdectvíDrezura	k1gFnSc1
-	-	kIx~
družstvo	družstvo	k1gNnSc1
</s>
<s>
ZlatoBritta	ZlatoBritta	k1gFnSc1
Steffenová	Steffenová	k1gFnSc1
PlaváníŽeny	PlaváníŽena	k1gFnSc2
100	#num#	k4
m	m	kA
volný	volný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
</s>
<s>
ZlatoBritta	ZlatoBritta	k1gFnSc1
Steffenová	Steffenová	k1gFnSc1
PlaváníŽeny	PlaváníŽena	k1gFnSc2
50	#num#	k4
m	m	kA
volný	volný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
</s>
<s>
ZlatoJan	ZlatoJan	k1gMnSc1
Frodeno	Froden	k2eAgNnSc4d1
TriatlonMuži	TriatlonMuž	k1gFnSc6
</s>
<s>
ZlatoMatthias	ZlatoMatthias	k1gMnSc1
Steiner	Steiner	k1gMnSc1
VzpíráníMuži	VzpíráníMuž	k1gFnSc3
nad	nad	k7c7
105	#num#	k4
kg	kg	kA
</s>
<s>
ZlatoMartin	ZlatoMartin	k2eAgInSc1d1
HollsteinAndreas	HollsteinAndreas	k1gInSc1
Ihle	Ihle	k1gNnSc2
KanoistikaK	KanoistikaK	k1gFnSc2
<g/>
2	#num#	k4
1000	#num#	k4
metrů	metr	k1gInPc2
muži	muž	k1gMnSc6
</s>
<s>
ZlatoFanny	ZlatoFanen	k2eAgFnPc1d1
FischerováNicole	FischerováNicole	k1gFnPc1
ReinhardtováKatrin	ReinhardtováKatrin	k1gInSc4
AugustinováConny	AugustinováConna	k1gFnSc2
Waßmuthová	Waßmuthová	k1gFnSc1
KanoistikaK	KanoistikaK	k1gFnSc1
<g/>
4	#num#	k4
500	#num#	k4
metrů	metr	k1gInPc2
ženy	žena	k1gFnSc2
</s>
<s>
ZlatoLena	ZlatoLena	k1gFnSc1
Schöneborn	Schöneborna	k1gFnPc2
Moderní	moderní	k2eAgFnSc2d1
pětibojŽeny	pětibojŽena	k1gFnSc2
</s>
<s>
ZlatoSabine	ZlatoSabinout	k5eAaPmIp3nS
Spitzová	Spitzová	k1gFnSc1
CyklistikaŽeny	CyklistikaŽena	k1gFnSc2
cross-country	cross-countr	k1gInPc1
</s>
<s>
ZlatoSebastian	ZlatoSebastian	k1gMnSc1
BiederlackMoritz	BiederlackMoritz	k1gMnSc1
FuersteTobias	FuersteTobias	k1gMnSc1
HaukeFlorian	HaukeFlorian	k1gMnSc1
KellerOliver	KellerOliver	k1gMnSc1
KornNiklas	KornNiklas	k1gMnSc1
MeinertMaximillian	MeinertMaximillian	k1gMnSc1
MuellerCarlos	MuellerCarlos	k1gMnSc1
NevadoMax	NevadoMax	k1gInSc4
WeinholdTimo	WeinholdTima	k1gFnSc5
WessBenjamin	WessBenjamin	k1gInSc1
WessTibor	WessTibor	k1gMnSc1
WeißenbornPhilip	WeißenbornPhilip	k1gMnSc1
WitteMatthias	WitteMatthias	k1gMnSc1
WitthausChristopher	WitthausChristophra	k1gFnPc2
ZellerPhilipp	ZellerPhilipp	k1gMnSc1
Zeller	Zeller	k1gMnSc1
Pozemní	pozemní	k2eAgFnSc4d1
hokejTurnaj	hokejTurnaj	k1gFnSc4
mužů	muž	k1gMnPc2
</s>
<s>
StříbroPatrick	StříbroPatrick	k1gMnSc1
HausdingSascha	HausdingSascha	k1gMnSc1
Klein	Klein	k1gMnSc1
Skoky	skok	k1gInPc7
do	do	k7c2
vodyMuži	vodyMuzat	k5eAaPmIp1nS
synchronizované	synchronizovaný	k2eAgInPc4d1
skoky	skok	k1gInPc4
z	z	k7c2
věže	věž	k1gFnSc2
10	#num#	k4
m	m	kA
</s>
<s>
StříbroMirko	StříbroMirko	k1gNnSc1
Englich	Englich	k1gInSc1
Zápasmuži	Zápasmuž	k1gFnSc3
<g/>
,	,	kIx,
řecko-římský	řecko-římský	k2eAgMnSc1d1
do	do	k7c2
96	#num#	k4
kg	kg	kA
</s>
<s>
StříbroRalf	StříbroRalf	k1gMnSc1
Schumann	Schumann	k1gMnSc1
Sportovní	sportovní	k2eAgFnSc3d1
střelbaMuži	střelbaMuž	k1gFnSc3
25	#num#	k4
metrů	metr	k1gInPc2
rychlopalná	rychlopalný	k2eAgFnSc1d1
pistole	pistole	k1gFnSc1
</s>
<s>
StříbroAnnekatrin	StříbroAnnekatrin	k1gInSc1
ThieleováChristiane	ThieleováChristian	k1gMnSc5
Huthová	Huthová	k1gFnSc1
VeslováníŽeny	VeslováníŽena	k1gFnSc2
dvojskif	dvojskif	k1gInSc1
</s>
<s>
StříbroRoger	StříbroRoger	k1gInSc1
Kluge	Klug	k1gFnSc2
CyklistikaMuži	CyklistikaMuž	k1gFnSc3
bodovací	bodovací	k2eAgInSc4d1
závod	závod	k1gInSc4
</s>
<s>
StříbroOksana	StříbroOksana	k1gFnSc1
Čusovitinová	Čusovitinový	k2eAgFnSc1d1
Sportovní	sportovní	k2eAgFnPc4d1
gymnastikaŽeny	gymnastikaŽena	k1gFnPc4
přeskok	přeskok	k1gInSc4
</s>
<s>
StříbroTimo	StříbroTimo	k6eAd1
BollDimitrij	BollDimitrij	k1gMnSc1
OvtcharovChristian	OvtcharovChristian	k1gMnSc1
Süß	Süß	k1gMnSc1
Stolní	stolní	k2eAgNnSc4d1
tenisDružstvo	tenisDružstvo	k1gNnSc4
mužů	muž	k1gMnPc2
</s>
<s>
StříbroIsabell	StříbroIsabell	k1gInSc1
Werth	Werth	k1gInSc1
JezdectvíDrezura	JezdectvíDrezura	k1gFnSc1
-	-	kIx~
jednotlivci	jednotlivec	k1gMnPc1
</s>
<s>
StříbroChristian	StříbroChristian	k1gMnSc1
GilleTomasz	GilleTomasza	k1gFnPc2
Wylenzek	Wylenzek	k1gInSc4
KanoistikaC	KanoistikaC	k1gFnSc4
<g/>
2	#num#	k4
1000	#num#	k4
metrů	metr	k1gInPc2
muži	muž	k1gMnSc6
</s>
<s>
StříbroRonald	StříbroRonald	k1gMnSc1
Rauhe	Rauh	k1gFnSc2
Tim	Tim	k?
Wieskötter	Wieskötter	k1gInSc1
KanoistikaK	KanoistikaK	k1gFnSc1
<g/>
2	#num#	k4
500	#num#	k4
metrů	metr	k1gInPc2
muži	muž	k1gMnSc6
</s>
<s>
BronzMunkhbayar	BronzMunkhbayar	k1gInSc1
Dorjsuren	Dorjsurna	k1gFnPc2
Sportovní	sportovní	k2eAgFnSc2d1
střelbaŽeny	střelbaŽena	k1gFnSc2
25	#num#	k4
metrů	metr	k1gInPc2
sportovní	sportovní	k2eAgFnSc2d1
pistole	pistol	k1gFnSc2
</s>
<s>
BronzChristine	BronzChristin	k1gMnSc5
Brinker	Brinker	k1gMnSc1
Sportovní	sportovní	k2eAgFnSc2d1
střelbaŽeny	střelbaŽena	k1gFnSc2
sket	sketa	k1gFnPc2
</s>
<s>
BronzRené	BronzRená	k1gFnPc1
EndersMaximilian	EndersMaximiliana	k1gFnPc2
LevyStefan	LevyStefany	k1gInPc2
Nimke	Nimk	k1gFnSc2
CyklistikaDružstvo	CyklistikaDružstvo	k1gNnSc1
mužů	muž	k1gMnPc2
sprint	sprint	k1gInSc4
</s>
<s>
BronzChristian	BronzChristiana	k1gFnPc2
Reitz	Reitza	k1gFnPc2
Sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
<g/>
25	#num#	k4
metrů	metr	k1gInPc2
rychlopalná	rychlopalný	k2eAgFnSc1d1
pistole	pistole	k1gFnSc1
</s>
<s>
BronzBritta	BronzBritta	k1gFnSc1
OppeltováManuela	OppeltováManuela	k1gFnSc1
LutzeováKathrin	LutzeováKathrin	k1gInSc4
BoronováStephanie	BoronováStephanie	k1gFnSc2
Schillerová	Schillerová	k1gFnSc1
VeslováníŽeny	VeslováníŽena	k1gFnSc2
párová	párový	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
</s>
<s>
BronzJan	BronzJan	k1gMnSc1
Peter	Peter	k1gMnSc1
PeckoltHannes	PeckoltHannes	k1gMnSc1
Peckolt	Peckolt	k1gMnSc1
JachtingTřída	JachtingTřída	k1gMnSc1
49	#num#	k4
<g/>
er	er	k?
</s>
<s>
BronzDitte	BronzDitte	k5eAaPmIp2nP
KotzianováHeike	KotzianováHeike	k1gFnSc1
Fischerová	Fischerová	k1gFnSc1
Skoky	skok	k1gInPc1
do	do	k7c2
vodyŽeny	vodyŽena	k1gFnSc2
synchronizované	synchronizovaný	k2eAgInPc4d1
skoky	skok	k1gInPc4
z	z	k7c2
prkna	prkno	k1gNnSc2
3	#num#	k4
m	m	kA
</s>
<s>
BronzFabian	BronzFabian	k1gInSc1
Hambüchen	Hambüchen	k2eAgInSc1d1
Sportovní	sportovní	k2eAgFnSc3d1
gymnastikaMuži	gymnastikaMuž	k1gFnSc3
hrazda	hrazda	k1gFnSc1
</s>
<s>
BronzHeike	BronzHeike	k6eAd1
Kemmerová	Kemmerový	k2eAgFnSc1d1
JezdectvíDrezura	JezdectvíDrezura	k1gFnSc1
-	-	kIx~
jednotlivci	jednotlivec	k1gMnPc1
</s>
<s>
BronzThomas	BronzThomas	k1gMnSc1
Lurz	Lurz	k1gMnSc1
PlaváníMuži	PlaváníMuž	k1gFnSc3
10	#num#	k4
km	km	kA
marathon	marathona	k1gFnPc2
</s>
<s>
BronzNadine	BronzNadin	k1gMnSc5
AngererFatmire	AngererFatmir	k1gMnSc5
BajramajSaskia	BajramajSaskius	k1gMnSc4
BartusiakMelanie	BartusiakMelanie	k1gFnSc2
BehringerLinda	BehringerLind	k1gMnSc4
BresonikKerstin	BresonikKerstin	k1gMnSc1
GarefrekesAriane	GarefrekesArian	k1gMnSc5
HingstUrsula	HingstUrsula	k1gFnSc1
HollAnnike	HollAnnikus	k1gMnSc5
KrahnSimone	KrahnSimon	k1gMnSc5
LaudehrRenate	LaudehrRenat	k1gMnSc5
LingorAnja	LingorAnj	k2eAgFnSc1d1
MittagCélia	MittagCélia	k1gFnSc1
Okoyino	Okoyino	k1gNnSc1
da	da	k?
MbabiBabett	MbabiBabett	k1gMnSc1
PeterConny	PeterConna	k1gMnSc2
PohlersBirgit	PohlersBirgit	k1gMnSc1
PrinzSandra	PrinzSandr	k1gMnSc2
SmisekKerstin	SmisekKerstin	k1gMnSc1
Stegemann	Stegemann	k1gMnSc1
FotbalTurnaj	FotbalTurnaj	k1gMnSc1
žen	žena	k1gFnPc2
</s>
<s>
BronzChristina	BronzChristin	k2eAgFnSc1d1
Obergföllová	Obergföllová	k1gFnSc1
AtletikaŽeny	AtletikaŽena	k1gFnSc2
hod	hod	k1gInSc1
oštěpem	oštěp	k1gInSc7
</s>
<s>
BronzLutz	BronzLutz	k1gMnSc1
AltepostNorman	AltepostNorman	k1gMnSc1
BröcklTorsten	BröcklTorsten	k2eAgInSc4d1
EckbrettBjörn	EckbrettBjörn	k1gInSc4
Goldschmidt	Goldschmidt	k2eAgInSc4d1
KanoistikaK	KanoistikaK	k1gFnSc7
<g/>
4	#num#	k4
1000	#num#	k4
metrů	metr	k1gInPc2
muži	muž	k1gMnSc6
</s>
<s>
BronzChristian	BronzChristian	k1gMnSc1
GilleTomasz	GilleTomasza	k1gFnPc2
Wylenzek	Wylenzek	k1gInSc4
KanoistikaC	KanoistikaC	k1gFnSc4
<g/>
2	#num#	k4
500	#num#	k4
metrů	metr	k1gInPc2
muži	muž	k1gMnSc6
</s>
<s>
BronzKatrin	BronzKatrin	k1gInSc1
Augustinová	Augustinová	k1gFnSc1
KanoistikaK	KanoistikaK	k1gFnSc1
<g/>
1	#num#	k4
500	#num#	k4
metrů	metr	k1gInPc2
ženy	žena	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Německo	Německo	k1gNnSc1
na	na	k7c4
LOH	LOH	kA
2008	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Země	zem	k1gFnPc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2008	#num#	k4
v	v	k7c6
Pekingu	Peking	k1gInSc6
Afrika	Afrika	k1gFnSc1
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
•	•	k?
</s>
<s>
Angola	Angola	k1gFnSc1
•	•	k?
</s>
<s>
Benin	Benin	k1gInSc1
•	•	k?
</s>
<s>
Botswana	Botswana	k1gFnSc1
•	•	k?
</s>
<s>
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
</s>
<s>
Burundi	Burundi	k1gNnSc1
•	•	k?
</s>
<s>
Čad	Čad	k1gInSc1
•	•	k?
</s>
<s>
Džibutsko	Džibutsko	k6eAd1
•	•	k?
</s>
<s>
Egypt	Egypt	k1gInSc1
•	•	k?
</s>
<s>
Eritrea	Eritrea	k1gFnSc1
•	•	k?
</s>
<s>
Etiopie	Etiopie	k1gFnSc1
•	•	k?
</s>
<s>
Gabon	Gabon	k1gInSc1
•	•	k?
</s>
<s>
Gambie	Gambie	k1gFnSc1
•	•	k?
</s>
<s>
Ghana	Ghana	k1gFnSc1
•	•	k?
</s>
<s>
Guinea	Guinea	k1gFnSc1
•	•	k?
</s>
<s>
Guinea	Guinea	k1gFnSc1
<g/>
‑	‑	k?
<g/>
Bissau	Bissaus	k1gInSc2
•	•	k?
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
</s>
<s>
Kamerun	Kamerun	k1gInSc1
•	•	k?
</s>
<s>
Kapverdy	Kapverdy	k6eAd1
•	•	k?
</s>
<s>
Keňa	Keňa	k1gFnSc1
•	•	k?
</s>
<s>
Komory	komora	k1gFnPc1
•	•	k?
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Kongo	Kongo	k1gNnSc1
•	•	k?
</s>
<s>
Konžská	konžský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
</s>
<s>
Lesotho	Lesotze	k6eAd1
•	•	k?
</s>
<s>
Libérie	Libérie	k1gFnSc1
•	•	k?
</s>
<s>
Libye	Libye	k1gFnSc1
•	•	k?
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1
•	•	k?
</s>
<s>
Malawi	Malawi	k1gNnSc1
•	•	k?
</s>
<s>
Mali	Mali	k1gNnSc1
•	•	k?
</s>
<s>
Mauritánie	Mauritánie	k1gFnSc1
•	•	k?
</s>
<s>
Mauricius	Mauricius	k1gInSc1
•	•	k?
</s>
<s>
Maroko	Maroko	k1gNnSc1
•	•	k?
</s>
<s>
Mosambik	Mosambik	k1gInSc1
•	•	k?
</s>
<s>
Namibie	Namibie	k1gFnSc1
•	•	k?
</s>
<s>
Niger	Niger	k1gInSc1
•	•	k?
</s>
<s>
Nigérie	Nigérie	k1gFnSc1
•	•	k?
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
•	•	k?
</s>
<s>
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
</s>
<s>
Rwanda	Rwanda	k1gFnSc1
•	•	k?
</s>
<s>
Senegal	Senegal	k1gInSc1
•	•	k?
</s>
<s>
Seychely	Seychely	k1gFnPc1
•	•	k?
</s>
<s>
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
</s>
<s>
Somálsko	Somálsko	k1gNnSc1
•	•	k?
</s>
<s>
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
</s>
<s>
Súdán	Súdán	k1gInSc1
•	•	k?
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
Princův	princův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
</s>
<s>
Svazijsko	Svazijsko	k1gNnSc1
•	•	k?
</s>
<s>
Tanzanie	Tanzanie	k1gFnSc1
•	•	k?
</s>
<s>
Togo	Togo	k1gNnSc1
•	•	k?
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
•	•	k?
</s>
<s>
Uganda	Uganda	k1gFnSc1
•	•	k?
</s>
<s>
Zambie	Zambie	k1gFnSc1
•	•	k?
</s>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1
</s>
<s>
Amerika	Amerika	k1gFnSc1
</s>
<s>
Americké	americký	k2eAgInPc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
•	•	k?
</s>
<s>
Argentina	Argentina	k1gFnSc1
•	•	k?
</s>
<s>
Aruba	Aruba	k1gFnSc1
•	•	k?
</s>
<s>
Bahamy	Bahamy	k1gFnPc1
•	•	k?
</s>
<s>
Barbados	Barbados	k1gInSc1
•	•	k?
</s>
<s>
Belize	Belize	k1gFnSc1
•	•	k?
</s>
<s>
Bermudy	Bermudy	k1gFnPc1
•	•	k?
</s>
<s>
Bolívie	Bolívie	k1gFnSc1
•	•	k?
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
•	•	k?
</s>
<s>
Britské	britský	k2eAgInPc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Dominika	Dominik	k1gMnSc4
•	•	k?
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1
•	•	k?
</s>
<s>
Grenada	Grenada	k1gFnSc1
•	•	k?
</s>
<s>
Guatemala	Guatemala	k1gFnSc1
•	•	k?
</s>
<s>
Guyana	Guyana	k1gFnSc1
•	•	k?
</s>
<s>
Haiti	Haiti	k1gNnSc4
•	•	k?
</s>
<s>
Honduras	Honduras	k1gInSc1
•	•	k?
</s>
<s>
Chile	Chile	k1gNnSc1
•	•	k?
</s>
<s>
Jamajka	Jamajka	k1gFnSc1
•	•	k?
</s>
<s>
Kanada	Kanada	k1gFnSc1
•	•	k?
</s>
<s>
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
•	•	k?
</s>
<s>
Kostarika	Kostarika	k1gFnSc1
•	•	k?
</s>
<s>
Kuba	Kuba	k1gFnSc1
•	•	k?
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
•	•	k?
</s>
<s>
Nizozemské	nizozemský	k2eAgFnPc1d1
Antily	Antily	k1gFnPc1
•	•	k?
</s>
<s>
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
</s>
<s>
Panama	Panama	k1gFnSc1
•	•	k?
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
•	•	k?
</s>
<s>
Peru	Peru	k1gNnSc1
•	•	k?
</s>
<s>
Portoriko	Portoriko	k1gNnSc1
•	•	k?
</s>
<s>
Salvador	Salvador	k1gInSc1
•	•	k?
</s>
<s>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
</s>
<s>
Surinam	Surinam	k1gInSc1
•	•	k?
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
</s>
<s>
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
•	•	k?
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
•	•	k?
</s>
<s>
Venezuela	Venezuela	k1gFnSc1
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
</s>
<s>
Arménie	Arménie	k1gFnSc1
•	•	k?
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
</s>
<s>
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
</s>
<s>
Bhútán	Bhútán	k1gInSc1
•	•	k?
</s>
<s>
Čína	Čína	k1gFnSc1
•	•	k?
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
•	•	k?
</s>
<s>
Hongkong	Hongkong	k1gInSc1
•	•	k?
</s>
<s>
Indie	Indie	k1gFnSc1
•	•	k?
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
•	•	k?
</s>
<s>
Írán	Írán	k1gInSc1
•	•	k?
</s>
<s>
Irák	Irák	k1gInSc1
•	•	k?
</s>
<s>
Izrael	Izrael	k1gInSc1
•	•	k?
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
•	•	k?
</s>
<s>
Jemen	Jemen	k1gInSc1
•	•	k?
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
</s>
<s>
Katar	katar	k1gInSc1
•	•	k?
</s>
<s>
Kambodža	Kambodža	k1gFnSc1
•	•	k?
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
</s>
<s>
Laos	Laos	k1gInSc1
•	•	k?
</s>
<s>
Libanon	Libanon	k1gInSc1
•	•	k?
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
•	•	k?
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
•	•	k?
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
</s>
<s>
Myanmar	Myanmar	k1gInSc1
•	•	k?
</s>
<s>
Nepál	Nepál	k1gInSc1
•	•	k?
</s>
<s>
Omán	Omán	k1gInSc1
•	•	k?
</s>
<s>
Pákistán	Pákistán	k1gInSc1
•	•	k?
</s>
<s>
Palestina	Palestina	k1gFnSc1
•	•	k?
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
</s>
<s>
Singapur	Singapur	k1gInSc1
•	•	k?
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
•	•	k?
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
•	•	k?
</s>
<s>
Šrí	Šrí	k?
Lanka	lanko	k1gNnSc2
•	•	k?
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
•	•	k?
</s>
<s>
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
</s>
<s>
Vietnam	Vietnam	k1gInSc1
•	•	k?
</s>
<s>
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
</s>
<s>
Andorra	Andorra	k1gFnSc1
•	•	k?
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
</s>
<s>
Belgie	Belgie	k1gFnSc1
•	•	k?
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
•	•	k?
</s>
<s>
Česko	Česko	k1gNnSc1
•	•	k?
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
•	•	k?
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
•	•	k?
</s>
<s>
Finsko	Finsko	k1gNnSc1
•	•	k?
</s>
<s>
Francie	Francie	k1gFnSc1
•	•	k?
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
•	•	k?
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
</s>
<s>
Island	Island	k1gInSc1
•	•	k?
</s>
<s>
Irsko	Irsko	k1gNnSc1
•	•	k?
</s>
<s>
Itálie	Itálie	k1gFnSc1
•	•	k?
</s>
<s>
Kypr	Kypr	k1gInSc1
•	•	k?
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
</s>
<s>
Litva	Litva	k1gFnSc1
•	•	k?
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
</s>
<s>
Makedonie	Makedonie	k1gFnSc1
•	•	k?
</s>
<s>
Malta	Malta	k1gFnSc1
•	•	k?
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
</s>
<s>
Monako	Monako	k1gNnSc1
•	•	k?
</s>
<s>
Německo	Německo	k1gNnSc1
•	•	k?
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
</s>
<s>
Norsko	Norsko	k1gNnSc1
•	•	k?
</s>
<s>
Polsko	Polsko	k1gNnSc1
•	•	k?
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
•	•	k?
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
</s>
<s>
Rusko	Rusko	k1gNnSc1
•	•	k?
</s>
<s>
Řecko	Řecko	k1gNnSc1
•	•	k?
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
•	•	k?
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
•	•	k?
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
•	•	k?
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
•	•	k?
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
•	•	k?
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
</s>
<s>
Turecko	Turecko	k1gNnSc1
•	•	k?
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
•	•	k?
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
</s>
<s>
Cookovy	Cookův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Fidži	Fidž	k1gFnSc3
•	•	k?
</s>
<s>
Guam	Guam	k1gInSc1
•	•	k?
</s>
<s>
Kiribati	Kiribat	k5eAaImF,k5eAaPmF
•	•	k?
</s>
<s>
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Mikronésie	Mikronésie	k1gFnSc1
•	•	k?
</s>
<s>
Nauru	Naura	k1gFnSc4
•	•	k?
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
</s>
<s>
Palau	Palau	k6eAd1
•	•	k?
</s>
<s>
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
</s>
<s>
Samoa	Samoa	k1gFnSc1
•	•	k?
</s>
<s>
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
</s>
<s>
Tonga	Tonga	k1gFnSc1
•	•	k?
</s>
<s>
Tuvalu	Tuvala	k1gFnSc4
•	•	k?
</s>
<s>
Vanuatu	Vanuata	k1gFnSc4
</s>
<s>
Německo	Německo	k1gNnSc1
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
Německo	Německo	k1gNnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
1896	#num#	k4
•	•	k?
1900	#num#	k4
•	•	k?
1904	#num#	k4
•	•	k?
1908	#num#	k4
•	•	k?
1912	#num#	k4
•	•	k?
1920	#num#	k4
•	•	k?
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2020	#num#	k4
Německo	Německo	k1gNnSc4
na	na	k7c6
ZOH	ZOH	kA
</s>
<s>
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2022	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
