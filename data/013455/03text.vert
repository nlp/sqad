<p>
<s>
Adventure	Adventur	k1gMnSc5	Adventur
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
americké	americký	k2eAgFnSc2d1	americká
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Television	Television	k1gInSc4	Television
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1978	[number]	k4	1978
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Elektra	Elektrum	k1gNnSc2	Elektrum
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Nahráno	nahrát	k5eAaBmNgNnS	nahrát
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1977	[number]	k4	1977
v	v	k7c6	v
newyorských	newyorský	k2eAgFnPc6d1	newyorská
studiích	studie	k1gFnPc6	studie
Soundmixers	Soundmixersa	k1gFnPc2	Soundmixersa
a	a	k8xC	a
Record	Recorda	k1gFnPc2	Recorda
Plant	planta	k1gFnPc2	planta
Studios	Studios	k?	Studios
a	a	k8xC	a
o	o	k7c6	o
produkci	produkce	k1gFnSc6	produkce
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgMnS	starat
John	John	k1gMnSc1	John
Jansen	Jansen	k2eAgMnSc1d1	Jansen
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tomem	Tom	k1gMnSc7	Tom
Verlainem	Verlain	k1gMnSc7	Verlain
<g/>
,	,	kIx,	,
kytaristou	kytarista	k1gMnSc7	kytarista
a	a	k8xC	a
zpěvákem	zpěvák	k1gMnSc7	zpěvák
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Verlaine	Verlain	k1gInSc5	Verlain
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Lloyd	Lloyd	k1gMnSc1	Lloyd
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Fred	Fred	k1gMnSc1	Fred
Smith	Smith	k1gMnSc1	Smith
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Billy	Bill	k1gMnPc7	Bill
Ficca	Ficc	k1gInSc2	Ficc
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
