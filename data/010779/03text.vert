<p>
<s>
Olej	olej	k1gInSc1	olej
je	být	k5eAaImIp3nS	být
kapalina	kapalina	k1gFnSc1	kapalina
tvořená	tvořený	k2eAgFnSc1d1	tvořená
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hydrofobní	hydrofobní	k2eAgInPc4d1	hydrofobní
uhlovodíkové	uhlovodíkový	k2eAgInPc4d1	uhlovodíkový
řetězce	řetězec	k1gInPc4	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
oleje	olej	k1gInPc1	olej
nerozpouštějí	rozpouštět	k5eNaImIp3nP	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
menší	malý	k2eAgFnSc4d2	menší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
olejů	olej	k1gInPc2	olej
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Potravinářské	potravinářský	k2eAgMnPc4d1	potravinářský
===	===	k?	===
</s>
</p>
<p>
<s>
Potravinářské	potravinářský	k2eAgInPc4d1	potravinářský
<g/>
,	,	kIx,	,
jedlé	jedlý	k2eAgInPc4d1	jedlý
oleje	olej	k1gInPc4	olej
jsou	být	k5eAaImIp3nP	být
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
kapalné	kapalný	k2eAgInPc4d1	kapalný
triacylglyceroly	triacylglycerol	k1gInPc4	triacylglycerol
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
dvojných	dvojný	k2eAgFnPc2d1	dvojná
vazeb	vazba	k1gFnPc2	vazba
v	v	k7c6	v
řetězci	řetězec	k1gInSc6	řetězec
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
olej	olej	k1gInSc4	olej
tekutější	tekutý	k2eAgInSc4d2	tekutější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rostlinných	rostlinný	k2eAgFnPc6d1	rostlinná
tkáních	tkáň	k1gFnPc6	tkáň
slouží	sloužit	k5eAaImIp3nP	sloužit
oleje	olej	k1gInPc1	olej
jako	jako	k8xC	jako
zásobárna	zásobárna	k1gFnSc1	zásobárna
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jako	jako	k8xC	jako
pohotový	pohotový	k2eAgInSc1d1	pohotový
energetický	energetický	k2eAgInSc1d1	energetický
zdroj	zdroj	k1gInSc1	zdroj
pro	pro	k7c4	pro
klíčící	klíčící	k2eAgNnPc4d1	klíčící
semena	semeno	k1gNnPc4	semeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Druhy	druh	k1gInPc1	druh
potravinářských	potravinářský	k2eAgInPc2d1	potravinářský
olejů	olej	k1gInPc2	olej
====	====	k?	====
</s>
</p>
<p>
<s>
slunečnicový	slunečnicový	k2eAgInSc1d1	slunečnicový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
dýňový	dýňový	k2eAgInSc1d1	dýňový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
kokosový	kokosový	k2eAgInSc1d1	kokosový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
konopný	konopný	k2eAgInSc1d1	konopný
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
makový	makový	k2eAgInSc1d1	makový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
mandlový	mandlový	k2eAgInSc1d1	mandlový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
olivový	olivový	k2eAgInSc4d1	olivový
olej	olej	k1gInSc4	olej
</s>
</p>
<p>
<s>
palmový	palmový	k2eAgInSc1d1	palmový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
řepkový	řepkový	k2eAgInSc1d1	řepkový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
sezamový	sezamový	k2eAgInSc1d1	sezamový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
sójový	sójový	k2eAgInSc1d1	sójový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
arganový	arganový	k2eAgInSc1d1	arganový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
spermový	spermový	k2eAgInSc1d1	spermový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
bodlákový	bodlákový	k2eAgInSc1d1	bodlákový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
===	===	k?	===
Technické	technický	k2eAgFnSc2d1	technická
===	===	k?	===
</s>
</p>
<p>
<s>
Technické	technický	k2eAgInPc1d1	technický
oleje	olej	k1gInPc1	olej
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
</s>
</p>
<p>
<s>
minerálních	minerální	k2eAgInPc2d1	minerální
olejů	olej	k1gInPc2	olej
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
směsí	směs	k1gFnSc7	směs
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
získaných	získaný	k2eAgInPc2d1	získaný
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
silikonových	silikonový	k2eAgInPc2d1	silikonový
olejů	olej	k1gInPc2	olej
<g/>
,	,	kIx,	,
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
synteticky	synteticky	k6eAd1	synteticky
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
místo	místo	k6eAd1	místo
uhlíkových	uhlíkový	k2eAgInPc2d1	uhlíkový
řetězců	řetězec	k1gInPc2	řetězec
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
řetězce	řetězec	k1gInPc1	řetězec
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
křemíku	křemík	k1gInSc2	křemík
<g/>
.	.	kIx.	.
<g/>
Technické	technický	k2eAgInPc1d1	technický
oleje	olej	k1gInPc1	olej
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
především	především	k9	především
jako	jako	k9	jako
maziva	mazivo	k1gNnPc1	mazivo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jako	jako	k8xS	jako
náplně	náplň	k1gFnPc1	náplň
hydraulických	hydraulický	k2eAgInPc2d1	hydraulický
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInPc1d1	speciální
silikonové	silikonový	k2eAgInPc1d1	silikonový
oleje	olej	k1gInPc1	olej
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
chladivo	chladivo	k1gNnSc4	chladivo
a	a	k8xC	a
hasivo	hasivo	k1gNnSc4	hasivo
v	v	k7c6	v
elektrických	elektrický	k2eAgInPc6d1	elektrický
transformátorech	transformátor	k1gInPc6	transformátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ricinový	ricinový	k2eAgInSc1d1	ricinový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
===	===	k?	===
Esenciální	esenciální	k2eAgMnPc4d1	esenciální
===	===	k?	===
</s>
</p>
<p>
<s>
Esenciální	esenciální	k2eAgInPc4d1	esenciální
oleje	olej	k1gInPc4	olej
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
silice	silice	k1gFnPc4	silice
<g/>
,	,	kIx,	,
tekuté	tekutý	k2eAgFnPc4d1	tekutá
izoprenoidy	izoprenoida	k1gFnPc4	izoprenoida
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vonné	vonný	k2eAgFnPc4d1	vonná
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
použití	použití	k1gNnSc4	použití
nalézají	nalézat	k5eAaImIp3nP	nalézat
např.	např.	kA	např.
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kosmetiky	kosmetika	k1gFnSc2	kosmetika
<g/>
,	,	kIx,	,
v	v	k7c6	v
aromaterapii	aromaterapie	k1gFnSc6	aromaterapie
nebo	nebo	k8xC	nebo
při	při	k7c6	při
aromaterapeutických	aromaterapeutický	k2eAgFnPc6d1	Aromaterapeutická
masážích	masáž	k1gFnPc6	masáž
-	-	kIx~	-
například	například	k6eAd1	například
růžový	růžový	k2eAgInSc1d1	růžový
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
olejů	olej	k1gInPc2	olej
==	==	k?	==
</s>
</p>
<p>
<s>
Tuky	tuk	k1gInPc1	tuk
jako	jako	k8xS	jako
zásobní	zásobní	k2eAgFnPc1d1	zásobní
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
v	v	k7c6	v
semenech	semeno	k1gNnPc6	semeno
nebo	nebo	k8xC	nebo
plodech	plod	k1gInPc6	plod
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
nasbírání	nasbírání	k1gNnSc6	nasbírání
lisují	lisovat	k5eAaImIp3nP	lisovat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
<g/>
)	)	kIx)	)
získává	získávat	k5eAaImIp3nS	získávat
olej	olej	k1gInSc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
lisovány	lisovat	k5eAaImNgFnP	lisovat
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
olej	olej	k1gInSc1	olej
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
příkladně	příkladně	k6eAd1	příkladně
na	na	k7c6	na
konzumaci	konzumace	k1gFnSc6	konzumace
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
terapeutický	terapeutický	k2eAgInSc4d1	terapeutický
olej	olej	k1gInSc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Lisování	lisování	k1gNnSc1	lisování
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
získáme	získat	k5eAaPmIp1nP	získat
méně	málo	k6eAd2	málo
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
vhodný	vhodný	k2eAgInSc4d1	vhodný
pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Potravinářství	potravinářství	k1gNnSc1	potravinářství
</s>
</p>
<p>
<s>
Anne	Anne	k1gInSc1	Anne
Iburg	Iburg	k1gInSc1	Iburg
<g/>
:	:	kIx,	:
Lexikon	lexikon	k1gNnSc1	lexikon
octů	ocet	k1gInPc2	ocet
a	a	k8xC	a
olejů	olej	k1gInPc2	olej
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
REBO	REBO	kA	REBO
Productions	Productions	k1gInSc4	Productions
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7234	[number]	k4	7234
<g/>
-	-	kIx~	-
<g/>
382	[number]	k4	382
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
EAN	EAN	kA	EAN
9788072343829	[number]	k4	9788072343829
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jill	Jill	k1gInSc1	Jill
Normanová	Normanová	k1gFnSc1	Normanová
<g/>
:	:	kIx,	:
Oleje	olej	k1gInPc1	olej
<g/>
,	,	kIx,	,
octy	ocet	k1gInPc1	ocet
a	a	k8xC	a
iné	iné	k?	iné
prísady	prísada	k1gFnSc2	prísada
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Slovart	Slovart	k1gInSc1	Slovart
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7145	[number]	k4	7145
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Johanna	Johann	k1gMnSc4	Johann
Budwig	Budwig	k1gMnSc1	Budwig
<g/>
:	:	kIx,	:
Lněný	lněný	k2eAgInSc1d1	lněný
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
Účinná	účinný	k2eAgFnSc1d1	účinná
pomoc	pomoc	k1gFnSc1	pomoc
proti	proti	k7c3	proti
arterioskleróze	arterioskleróza	k1gFnSc3	arterioskleróza
<g/>
,	,	kIx,	,
infarktu	infarkt	k1gInSc3	infarkt
a	a	k8xC	a
rakovině	rakovina	k1gFnSc3	rakovina
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Pragma	Pragmum	k1gNnSc2	Pragmum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7205	[number]	k4	7205
<g/>
-	-	kIx~	-
<g/>
105	[number]	k4	105
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
EAN	EAN	kA	EAN
9788072051052	[number]	k4	9788072051052
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Esenciální	esenciální	k2eAgInPc1d1	esenciální
oleje	olej	k1gInPc1	olej
</s>
</p>
<p>
<s>
Colleen	Colleen	k1gInSc4	Colleen
K.	K.	kA	K.
Dodt	Dodt	k2eAgMnSc1d1	Dodt
<g/>
:	:	kIx,	:
Éterické	éterický	k2eAgInPc4d1	éterický
a	a	k8xC	a
esenciální	esenciální	k2eAgInPc4d1	esenciální
oleje	olej	k1gInPc4	olej
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Pragma	Pragmum	k1gNnSc2	Pragmum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7349	[number]	k4	7349
<g/>
-	-	kIx~	-
<g/>
115	[number]	k4	115
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
EAN	EAN	kA	EAN
9788073491154	[number]	k4	9788073491154
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Motorový	motorový	k2eAgInSc1d1	motorový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
Olejnička	olejnička	k1gFnSc1	olejnička
</s>
</p>
<p>
<s>
Maznice	maznice	k1gFnSc1	maznice
</s>
</p>
<p>
<s>
Masáž	masáž	k1gFnSc1	masáž
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
olej	olej	k1gInSc1	olej
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
olej	olej	k1gInSc1	olej
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Potravinárske	Potravinárske	k1gFnSc1	Potravinárske
oleje	olej	k1gInSc2	olej
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Zlato	zlato	k1gNnSc1	zlato
po	po	k7c6	po
kvapkách	kvapkách	k?	kvapkách
-	-	kIx~	-
zena	zena	k1gMnSc1	zena
<g/>
.	.	kIx.	.
<g/>
pluska	pluska	k1gFnSc1	pluska
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
<p>
<s>
Esenciálne	Esenciálnout	k5eAaPmIp3nS	Esenciálnout
oleje	olej	k1gInSc2	olej
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Rastlinné	Rastlinný	k2eAgInPc1d1	Rastlinný
oleje	olej	k1gInPc1	olej
-	-	kIx~	-
prehľad	prehľad	k1gInSc1	prehľad
-	-	kIx~	-
www.nanicmama.sk	www.nanicmama.sk	k1gInSc1	www.nanicmama.sk
</s>
</p>
