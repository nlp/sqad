<s>
Esenciální	esenciální	k2eAgInPc4d1	esenciální
oleje	olej	k1gInPc4	olej
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
silice	silice	k1gFnPc4	silice
<g/>
,	,	kIx,	,
tekuté	tekutý	k2eAgFnPc4d1	tekutá
izoprenoidy	izoprenoida	k1gFnPc4	izoprenoida
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vonné	vonný	k2eAgFnPc4d1	vonná
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
použití	použití	k1gNnSc4	použití
nalézají	nalézat	k5eAaImIp3nP	nalézat
např.	např.	kA	např.
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kosmetiky	kosmetika	k1gFnSc2	kosmetika
<g/>
,	,	kIx,	,
v	v	k7c6	v
aromaterapii	aromaterapie	k1gFnSc6	aromaterapie
nebo	nebo	k8xC	nebo
při	při	k7c6	při
aromaterapeutických	aromaterapeutický	k2eAgFnPc6d1	Aromaterapeutická
masážích	masáž	k1gFnPc6	masáž
-	-	kIx~	-
například	například	k6eAd1	například
růžový	růžový	k2eAgInSc1d1	růžový
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
</s>

