<s>
Bostonské	bostonský	k2eAgNnSc1d1	Bostonské
pití	pití	k1gNnSc1	pití
čaje	čaj	k1gInSc2	čaj
neboli	neboli	k8xC	neboli
Bostonský	bostonský	k2eAgInSc4d1	bostonský
čajový	čajový	k2eAgInSc4d1	čajový
dýchánek	dýchánek	k1gInSc4	dýchánek
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Boston	Boston	k1gInSc1	Boston
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnSc1	party
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
protest	protest	k1gInSc4	protest
amerických	americký	k2eAgMnPc2d1	americký
kolonistů	kolonista	k1gMnPc2	kolonista
proti	proti	k7c3	proti
britskému	britský	k2eAgNnSc3d1	Britské
impériu	impérium	k1gNnSc3	impérium
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
bostonském	bostonský	k2eAgInSc6d1	bostonský
přístavu	přístav	k1gInSc6	přístav
zničeno	zničit	k5eAaPmNgNnS	zničit
mnoho	mnoho	k6eAd1	mnoho
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
342	[number]	k4	342
<g/>
)	)	kIx)	)
beden	bedna	k1gFnPc2	bedna
lisovaného	lisovaný	k2eAgInSc2d1	lisovaný
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Odehrálo	odehrát	k5eAaPmAgNnS	odehrát
se	se	k3xPyFc4	se
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1773	[number]	k4	1773
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
konfliktů	konflikt	k1gInPc2	konflikt
směřujících	směřující	k2eAgFnPc2d1	směřující
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Kolkový	kolkový	k2eAgInSc1d1	kolkový
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
(	(	kIx(	(
<g/>
Stamp	Stamp	k1gInSc1	Stamp
Act	Act	k1gFnSc2	Act
of	of	k?	of
1765	[number]	k4	1765
<g/>
,	,	kIx,	,
povinnost	povinnost	k1gFnSc4	povinnost
lepit	lepit	k5eAaImF	lepit
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
kolky	kolek	k1gInPc4	kolek
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
zboží	zboží	k1gNnSc2	zboží
<g/>
)	)	kIx)	)
a	a	k8xC	a
Townshendovy	Townshendův	k2eAgInPc1d1	Townshendův
zákony	zákon	k1gInPc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
(	(	kIx(	(
<g/>
Townshend	Townshenda	k1gFnPc2	Townshenda
Acts	Acts	k1gInSc1	Acts
<g/>
,	,	kIx,	,
uvalení	uvalení	k1gNnSc1	uvalení
cel	clo	k1gNnPc2	clo
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
mnohého	mnohé	k1gNnSc2	mnohé
běžně	běžně	k6eAd1	běžně
dováženého	dovážený	k2eAgNnSc2d1	dovážené
zboží	zboží	k1gNnSc2	zboží
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
rozhněval	rozhněvat	k5eAaPmAgMnS	rozhněvat
americké	americký	k2eAgMnPc4d1	americký
kolonisty	kolonista	k1gMnPc4	kolonista
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výše	výše	k1gFnSc1	výše
nových	nový	k2eAgFnPc2d1	nová
daní	daň	k1gFnPc2	daň
neodpovídala	odpovídat	k5eNaImAgFnS	odpovídat
míře	míra	k1gFnSc3	míra
jejich	jejich	k3xOp3gNnSc4	jejich
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
Parlamentu	parlament	k1gInSc6	parlament
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
protestujících	protestující	k2eAgMnPc2d1	protestující
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Hancock	Hancock	k1gMnSc1	Hancock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1768	[number]	k4	1768
byla	být	k5eAaImAgFnS	být
Hancockova	Hancockův	k2eAgFnSc1d1	Hancockova
loď	loď	k1gFnSc1	loď
Liberty	Libert	k1gInPc1	Libert
obsazena	obsadit	k5eAaPmNgFnS	obsadit
celníky	celník	k1gMnPc7	celník
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
pašování	pašování	k1gNnSc2	pašování
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
ho	on	k3xPp3gMnSc4	on
sice	sice	k8xC	sice
úspěšně	úspěšně	k6eAd1	úspěšně
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
však	však	k9	však
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
čelil	čelit	k5eAaImAgInS	čelit
stovkám	stovka	k1gFnPc3	stovka
podobných	podobný	k2eAgFnPc2d1	podobná
obvinění	obvinění	k1gNnPc2	obvinění
<g/>
.	.	kIx.	.
</s>
<s>
Hancock	Hancock	k1gMnSc1	Hancock
organizoval	organizovat	k5eAaBmAgMnS	organizovat
bojkot	bojkot	k1gInSc4	bojkot
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
dovážela	dovážet	k5eAaImAgFnS	dovážet
britská	britský	k2eAgFnSc1d1	britská
Východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
British	British	k1gInSc1	British
East	Easta	k1gFnPc2	Easta
India	indium	k1gNnSc2	indium
Company	Compana	k1gFnSc2	Compana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
prodeje	prodej	k1gFnPc1	prodej
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
rychle	rychle	k6eAd1	rychle
klesly	klesnout	k5eAaPmAgInP	klesnout
z	z	k7c2	z
145	[number]	k4	145
000	[number]	k4	000
kg	kg	kA	kg
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
240	[number]	k4	240
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
se	se	k3xPyFc4	se
již	již	k6eAd1	již
společnost	společnost	k1gFnSc1	společnost
topila	topit	k5eAaImAgFnS	topit
v	v	k7c6	v
dluzích	dluh	k1gInPc6	dluh
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
obrovské	obrovský	k2eAgFnPc4d1	obrovská
zásoby	zásoba	k1gFnPc4	zásoba
čaje	čaj	k1gInSc2	čaj
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
žádné	žádný	k3yNgFnPc4	žádný
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
prodeje	prodej	k1gInSc2	prodej
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pašeráci	pašerák	k1gMnPc1	pašerák
jako	jako	k8xS	jako
Hancock	Hancock	k1gInSc4	Hancock
dováželi	dovážet	k5eAaImAgMnP	dovážet
čaj	čaj	k1gInSc4	čaj
bez	bez	k7c2	bez
placení	placení	k1gNnSc2	placení
cel	cela	k1gFnPc2	cela
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
proto	proto	k8xC	proto
přijala	přijmout	k5eAaPmAgFnS	přijmout
Čajový	čajový	k2eAgInSc4d1	čajový
zákon	zákon	k1gInSc4	zákon
(	(	kIx(	(
<g/>
Tea	Tea	k1gFnSc1	Tea
Act	Act	k1gMnSc1	Act
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dovoloval	dovolovat	k5eAaImAgInS	dovolovat
společnosti	společnost	k1gFnSc3	společnost
prodávat	prodávat	k5eAaImF	prodávat
čaj	čaj	k1gInSc4	čaj
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
nezdaněný	zdaněný	k2eNgInSc4d1	nezdaněný
za	za	k7c4	za
ceny	cena	k1gFnPc4	cena
nižší	nízký	k2eAgFnPc4d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
za	za	k7c4	za
jaké	jaký	k3yQgNnSc4	jaký
jej	on	k3xPp3gNnSc4	on
nabízeli	nabízet	k5eAaImAgMnP	nabízet
pašeráci	pašerák	k1gMnPc1	pašerák
<g/>
.	.	kIx.	.
</s>
<s>
Protesty	protest	k1gInPc1	protest
probíhaly	probíhat	k5eAaImAgInP	probíhat
jak	jak	k6eAd1	jak
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgInS	zapsat
Boston	Boston	k1gInSc1	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Bostoňané	Bostoňan	k1gMnPc1	Bostoňan
pokládali	pokládat	k5eAaImAgMnP	pokládat
nový	nový	k2eAgInSc4d1	nový
zákon	zákon	k1gInSc4	zákon
za	za	k7c4	za
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
opětovné	opětovný	k2eAgNnSc4d1	opětovné
potlačení	potlačení	k1gNnSc4	potlačení
práv	právo	k1gNnPc2	právo
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Adams	Adams	k1gInSc1	Adams
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
požadovali	požadovat	k5eAaImAgMnP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
jednatelé	jednatel	k1gMnPc1	jednatel
Východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
příjemci	příjemce	k1gMnSc3	příjemce
jí	jíst	k5eAaImIp3nS	jíst
dováženého	dovážený	k2eAgInSc2d1	dovážený
čaje	čaj	k1gInSc2	čaj
vzdali	vzdát	k5eAaPmAgMnP	vzdát
svých	svůj	k3xOyFgNnPc2	svůj
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sklady	sklad	k1gInPc4	sklad
a	a	k8xC	a
domovy	domov	k1gInPc4	domov
těch	ten	k3xDgMnPc2	ten
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zdráhali	zdráhat	k5eAaImAgMnP	zdráhat
<g/>
,	,	kIx,	,
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
lodí	loď	k1gFnPc2	loď
přivážejících	přivážející	k2eAgMnPc2d1	přivážející
čaj	čaj	k1gInSc4	čaj
Východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
připlula	připlout	k5eAaPmAgFnS	připlout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
1773	[number]	k4	1773
HMS	HMS	kA	HMS
Dartmouth	Dartmoutha	k1gFnPc2	Dartmoutha
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuela	k1gFnPc2	Samuela
Adams	Adamsa	k1gFnPc2	Adamsa
pobízel	pobízet	k5eAaImAgInS	pobízet
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
dav	dav	k1gInSc1	dav
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
sérii	série	k1gFnSc4	série
protestních	protestní	k2eAgNnPc2d1	protestní
shromáždění	shromáždění	k1gNnPc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
mítink	mítink	k1gInSc4	mítink
přicházelo	přicházet	k5eAaImAgNnS	přicházet
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Davy	Dav	k1gInPc1	Dav
provolávaly	provolávat	k5eAaImAgFnP	provolávat
odpor	odpor	k1gInSc4	odpor
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
Britskému	britský	k2eAgInSc3d1	britský
parlamentu	parlament	k1gInSc3	parlament
<g/>
,	,	kIx,	,
Východoindické	východoindický	k2eAgFnSc6d1	Východoindická
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
lodi	loď	k1gFnSc6	loď
HMS	HMS	kA	HMS
Dartmouth	Dartmouth	k1gInSc1	Dartmouth
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ke	k	k7c3	k
guvernéru	guvernér	k1gMnSc3	guvernér
Thomasu	Thomasu	k?	Thomasu
Hutchinsonovi	Hutchinson	k1gMnSc6	Hutchinson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stále	stále	k6eAd1	stále
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
vyložení	vyložení	k1gNnSc4	vyložení
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
protestní	protestní	k2eAgNnSc4d1	protestní
shromáždění	shromáždění	k1gNnSc4	shromáždění
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
jižním	jižní	k2eAgInSc6d1	jižní
kostele	kostel	k1gInSc6	kostel
(	(	kIx(	(
<g/>
Old	Olda	k1gFnPc2	Olda
South	South	k1gInSc1	South
Church	Church	k1gMnSc1	Church
<g/>
)	)	kIx)	)
zatím	zatím	k6eAd1	zatím
nejvíce	nejvíce	k6eAd1	nejvíce
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
8000	[number]	k4	8000
<g/>
.	.	kIx.	.
</s>
<s>
Pití	pití	k1gNnSc1	pití
čaje	čaj	k1gInSc2	čaj
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
událostí	událost	k1gFnPc2	událost
Americké	americký	k2eAgFnSc2d1	americká
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
svobodní	svobodný	k2eAgMnPc1d1	svobodný
zednáři	zednář	k1gMnPc1	zednář
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
v	v	k7c6	v
Taverně	taverna	k1gFnSc6	taverna
Zeleného	Zelený	k1gMnSc2	Zelený
draka	drak	k1gMnSc2	drak
(	(	kIx(	(
<g/>
Green	Green	k2eAgMnSc1d1	Green
Dragon	Dragon	k1gMnSc1	Dragon
Tavern	Tavern	k1gMnSc1	Tavern
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
připravoval	připravovat	k5eAaImAgMnS	připravovat
plán	plán	k1gInSc4	plán
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
lóže	lóže	k1gFnSc2	lóže
byli	být	k5eAaImAgMnP	být
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
Paul	Paula	k1gFnPc2	Paula
Revere	Rever	k1gInSc5	Rever
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Hancock	Hancock	k1gMnSc1	Hancock
a	a	k8xC	a
Joseph	Joseph	k1gMnSc1	Joseph
Warren	Warrna	k1gFnPc2	Warrna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pití	pití	k1gNnSc1	pití
čaje	čaj	k1gInSc2	čaj
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
schůzka	schůzka	k1gFnSc1	schůzka
lóže	lóže	k1gFnSc2	lóže
neuskutečnila	uskutečnit	k5eNaPmAgFnS	uskutečnit
a	a	k8xC	a
v	v	k7c6	v
zápisu	zápis	k1gInSc6	zápis
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
jen	jen	k9	jen
stručné	stručný	k2eAgNnSc1d1	stručné
"	"	kIx"	"
<g/>
Zabývali	zabývat	k5eAaImAgMnP	zabývat
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
čajem	čaj	k1gInSc7	čaj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
připlula	připlout	k5eAaPmAgFnS	připlout
do	do	k7c2	do
bostonského	bostonský	k2eAgInSc2d1	bostonský
přístavu	přístav	k1gInSc2	přístav
loď	loď	k1gFnSc4	loď
Dartmouth	Dartmoutha	k1gFnPc2	Dartmoutha
s	s	k7c7	s
nákladem	náklad	k1gInSc7	náklad
čaje	čaj	k1gInSc2	čaj
Darjeeling	Darjeeling	k1gInSc1	Darjeeling
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Adams	Adams	k1gInSc1	Adams
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
radikálové	radikál	k1gMnPc1	radikál
byli	být	k5eAaImAgMnP	být
odhodláni	odhodlán	k2eAgMnPc1d1	odhodlán
zabránit	zabránit	k5eAaPmF	zabránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
náklad	náklad	k1gInSc1	náklad
čaje	čaj	k1gInSc2	čaj
vyložen	vyložit	k5eAaPmNgInS	vyložit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
koloniích	kolonie	k1gFnPc6	kolonie
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
adresáti	adresát	k1gMnPc1	adresát
zásilek	zásilka	k1gFnPc2	zásilka
protestujícím	protestující	k2eAgMnPc3d1	protestující
ustoupili	ustoupit	k5eAaPmAgMnP	ustoupit
a	a	k8xC	a
lodě	loď	k1gFnPc1	loď
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
čajem	čaj	k1gInSc7	čaj
vrátily	vrátit	k5eAaPmAgFnP	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Bostonský	bostonský	k2eAgMnSc1d1	bostonský
guvernér	guvernér	k1gMnSc1	guvernér
Hutchinson	Hutchinson	k1gMnSc1	Hutchinson
byl	být	k5eAaImAgMnS	být
však	však	k9	však
odhodlán	odhodlán	k2eAgMnSc1d1	odhodlán
vytrvat	vytrvat	k5eAaPmF	vytrvat
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
příjemce	příjemce	k1gMnSc1	příjemce
zásilek	zásilka	k1gFnPc2	zásilka
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
dva	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gNnSc4	jeho
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neustoupili	ustoupit	k5eNaPmAgMnP	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
Čajový	čajový	k2eAgInSc1d1	čajový
zákon	zákon	k1gInSc1	zákon
požadoval	požadovat	k5eAaImAgInS	požadovat
vyložení	vyložení	k1gNnSc4	vyložení
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
zaplacení	zaplacení	k1gNnSc4	zaplacení
daně	daň	k1gFnSc2	daň
do	do	k7c2	do
20	[number]	k4	20
dnů	den	k1gInPc2	den
od	od	k7c2	od
přistání	přistání	k1gNnSc2	přistání
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
mohli	moct	k5eAaImAgMnP	moct
celní	celní	k2eAgMnPc1d1	celní
úředníci	úředník	k1gMnPc1	úředník
zboží	zboží	k1gNnSc2	zboží
zabavit	zabavit	k5eAaPmF	zabavit
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Adams	Adams	k1gInSc4	Adams
svolal	svolat	k5eAaPmAgMnS	svolat
na	na	k7c4	na
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1773	[number]	k4	1773
veřejné	veřejný	k2eAgNnSc1d1	veřejné
shromáždění	shromáždění	k1gNnSc1	shromáždění
do	do	k7c2	do
Faneuil	Faneuila	k1gFnPc2	Faneuila
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
dostavily	dostavit	k5eAaPmAgInP	dostavit
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
shromáždění	shromáždění	k1gNnSc1	shromáždění
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
do	do	k7c2	do
většího	veliký	k2eAgInSc2d2	veliký
Old	Olda	k1gFnPc2	Olda
South	South	k1gInSc4	South
Meeting	meeting	k1gInSc1	meeting
House	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
přijalo	přijmout	k5eAaPmAgNnS	přijmout
usnesení	usnesení	k1gNnSc4	usnesení
navržené	navržený	k2eAgFnSc2d1	navržená
Adamsem	Adams	k1gInSc7	Adams
a	a	k8xC	a
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
podobných	podobný	k2eAgFnPc6d1	podobná
vyhláškách	vyhláška	k1gFnPc6	vyhláška
přijatých	přijatý	k2eAgFnPc6d1	přijatá
dříve	dříve	k6eAd2	dříve
ve	v	k7c6	v
Philadephii	Philadephie	k1gFnSc6	Philadephie
<g/>
.	.	kIx.	.
</s>
<s>
Požadovalo	požadovat	k5eAaImAgNnS	požadovat
odplutí	odplutí	k1gNnSc1	odplutí
lodě	loď	k1gFnSc2	loď
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
bez	bez	k1gInSc1	bez
zaplacní	zaplacnět	k5eAaPmIp3nS	zaplacnět
dovozního	dovozní	k2eAgNnSc2d1	dovozní
cla	clo	k1gNnSc2	clo
<g/>
.	.	kIx.	.
</s>
<s>
Schůze	schůze	k1gFnSc1	schůze
také	také	k9	také
vyslala	vyslat	k5eAaPmAgFnS	vyslat
dvacet	dvacet	k4xCc4	dvacet
pět	pět	k4xCc4	pět
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hlídkovali	hlídkovat	k5eAaImAgMnP	hlídkovat
u	u	k7c2	u
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
zabránili	zabránit	k5eAaPmAgMnP	zabránit
vyložení	vyložení	k1gNnSc4	vyložení
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
Hutchinson	Hutchinson	k1gMnSc1	Hutchinson
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
udělit	udělit	k5eAaPmF	udělit
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
odplutí	odplutí	k1gNnSc3	odplutí
lodě	loď	k1gFnSc2	loď
bez	bez	k7c2	bez
zaplacení	zaplacení	k1gNnSc2	zaplacení
cla	clo	k1gNnSc2	clo
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
připluly	připlout	k5eAaPmAgFnP	připlout
do	do	k7c2	do
bostonského	bostonský	k2eAgInSc2d1	bostonský
přístavu	přístav	k1gInSc2	přístav
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
s	s	k7c7	s
čajem	čaj	k1gInSc7	čaj
<g/>
,	,	kIx,	,
Beaver	Beaver	k1gInSc1	Beaver
a	a	k8xC	a
Eleanor	Eleanor	k1gInSc1	Eleanor
<g/>
;	;	kIx,	;
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
loď	loď	k1gFnSc1	loď
William	William	k1gInSc1	William
nedorazila	dorazit	k5eNaPmAgFnS	dorazit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
obětí	oběť	k1gFnSc7	oběť
bouře	bouře	k1gFnSc1	bouře
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
-	-	kIx~	-
posledního	poslední	k2eAgInSc2d1	poslední
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohl	moct	k5eAaImAgInS	moct
Dartmouth	Dartmouth	k1gInSc1	Dartmouth
zaplatit	zaplatit	k5eAaPmF	zaplatit
clo	clo	k1gNnSc4	clo
-	-	kIx~	-
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
Old	Olda	k1gFnPc2	Olda
South	Southa	k1gFnPc2	Southa
Meeting	meeting	k1gInSc4	meeting
House	house	k1gNnSc1	house
shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
asi	asi	k9	asi
sedm	sedm	k4xCc1	sedm
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
odrazila	odrazit	k5eAaPmAgFnS	odrazit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
guvernér	guvernér	k1gMnSc1	guvernér
Hutchinson	Hutchinson	k1gMnSc1	Hutchinson
opět	opět	k6eAd1	opět
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
povolit	povolit	k5eAaPmF	povolit
lodím	loď	k1gFnPc3	loď
odjezd	odjezd	k1gInSc4	odjezd
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Adams	Adams	k1gInSc4	Adams
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
tato	tento	k3xDgFnSc1	tento
schůze	schůze	k1gFnSc1	schůze
nemůže	moct	k5eNaImIp3nS	moct
již	již	k6eAd1	již
nic	nic	k3yNnSc1	nic
učinit	učinit	k5eAaPmF	učinit
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
vlasti	vlast	k1gFnSc2	vlast
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
populární	populární	k2eAgFnSc2d1	populární
verze	verze	k1gFnSc2	verze
událostí	událost	k1gFnPc2	událost
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
formulace	formulace	k1gFnSc1	formulace
smluveným	smluvený	k2eAgInSc7d1	smluvený
signálem	signál	k1gInSc7	signál
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
"	"	kIx"	"
<g/>
čajového	čajový	k2eAgInSc2d1	čajový
dýchánku	dýchánek	k1gInSc2	dýchánek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
objevilo	objevit	k5eAaPmAgNnS	objevit
až	až	k6eAd1	až
téměř	téměř	k6eAd1	téměř
po	po	k7c4	po
sto	sto	k4xCgNnSc4	sto
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
samotné	samotný	k2eAgFnSc2d1	samotná
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Adamsově	Adamsův	k2eAgInSc6d1	Adamsův
životopise	životopis	k1gInSc6	životopis
sepsaném	sepsaný	k2eAgInSc6d1	sepsaný
jeho	jeho	k3xOp3gMnSc7	jeho
pravnukem	pravnuk	k1gMnSc7	pravnuk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
zřejmě	zřejmě	k6eAd1	zřejmě
chybně	chybně	k6eAd1	chybně
vyložil	vyložit	k5eAaPmAgMnS	vyložit
dostupné	dostupný	k2eAgInPc4d1	dostupný
záznamy	záznam	k1gInPc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
líčení	líčení	k1gNnPc2	líčení
svědků	svědek	k1gMnPc2	svědek
lidé	člověk	k1gMnPc1	člověk
schůzi	schůze	k1gFnSc4	schůze
opustili	opustit	k5eAaPmAgMnP	opustit
až	až	k6eAd1	až
deset	deset	k4xCc4	deset
či	či	k8xC	či
patnáct	patnáct	k4xCc4	patnáct
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
údajném	údajný	k2eAgInSc6d1	údajný
Adamsově	Adamsův	k2eAgInSc6d1	Adamsův
"	"	kIx"	"
<g/>
signálu	signál	k1gInSc6	signál
<g/>
"	"	kIx"	"
a	a	k8xC	a
Adams	Adams	k1gInSc1	Adams
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
schůze	schůze	k1gFnSc1	schůze
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
u	u	k7c2	u
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
Samuel	Samuel	k1gMnSc1	Samuel
Adams	Adamsa	k1gFnPc2	Adamsa
snažil	snažit	k5eAaImAgMnS	snažit
opět	opět	k6eAd1	opět
ovládnout	ovládnout	k5eAaPmF	ovládnout
schůzi	schůze	k1gFnSc4	schůze
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnPc1	její
účastníci	účastník	k1gMnPc1	účastník
už	už	k6eAd1	už
ji	on	k3xPp3gFnSc4	on
opouštěli	opouštět	k5eAaImAgMnP	opouštět
a	a	k8xC	a
připravovali	připravovat	k5eAaImAgMnP	připravovat
se	se	k3xPyFc4	se
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
si	se	k3xPyFc3	se
jen	jen	k9	jen
zahalili	zahalit	k5eAaPmAgMnP	zahalit
tvář	tvář	k1gFnSc4	tvář
-	-	kIx~	-
protože	protože	k8xS	protože
protestní	protestní	k2eAgFnSc1d1	protestní
akce	akce	k1gFnSc1	akce
byla	být	k5eAaImAgFnS	být
nezákonná	zákonný	k2eNgFnSc1d1	nezákonná
-	-	kIx~	-
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
se	se	k3xPyFc4	se
oblékli	obléct	k5eAaPmAgMnP	obléct
do	do	k7c2	do
předem	předem	k6eAd1	předem
připravených	připravený	k2eAgInPc2d1	připravený
kostýmů	kostým	k1gInPc2	kostým
příslušníků	příslušník	k1gMnPc2	příslušník
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Mohawků	Mohawk	k1gInPc2	Mohawk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
výkladů	výklad	k1gInPc2	výklad
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
<g/>
:	:	kIx,	:
aktéři	aktér	k1gMnPc1	aktér
vzpoury	vzpoura	k1gFnSc2	vzpoura
tak	tak	k6eAd1	tak
upřednostňovali	upřednostňovat	k5eAaImAgMnP	upřednostňovat
identifikaci	identifikace	k1gFnSc4	identifikace
s	s	k7c7	s
Amerikou	Amerika	k1gFnSc7	Amerika
před	před	k7c7	před
svým	svůj	k3xOyFgMnSc7	svůj
oficiálním	oficiální	k2eAgInSc7d1	oficiální
statusem	status	k1gInSc7	status
poddaných	poddaná	k1gFnPc2	poddaná
britské	britský	k2eAgFnSc2d1	britská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
30	[number]	k4	30
až	až	k9	až
150	[number]	k4	150
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
indiánském	indiánský	k2eAgNnSc6d1	indiánské
přestrojení	přestrojení	k1gNnSc6	přestrojení
<g/>
,	,	kIx,	,
vnikla	vniknout	k5eAaPmAgFnS	vniknout
téhož	týž	k3xTgInSc2	týž
večera	večer	k1gInSc2	večer
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
lodí	loď	k1gFnPc2	loď
zakotvených	zakotvený	k2eAgFnPc2d1	zakotvená
u	u	k7c2	u
Griffinského	Griffinský	k2eAgNnSc2d1	Griffinský
přístaviště	přístaviště	k1gNnSc2	přístaviště
a	a	k8xC	a
během	během	k7c2	během
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
vyhodila	vyhodit	k5eAaPmAgFnS	vyhodit
všech	všecek	k3xTgFnPc2	všecek
342	[number]	k4	342
beden	bedna	k1gFnPc2	bedna
s	s	k7c7	s
čajem	čaj	k1gInSc7	čaj
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přístavní	přístavní	k2eAgFnSc2d1	přístavní
hráze	hráz	k1gFnSc2	hráz
celému	celý	k2eAgInSc3d1	celý
dění	dění	k1gNnSc2	dění
přihlížel	přihlížet	k5eAaImAgInS	přihlížet
tichý	tichý	k2eAgInSc1d1	tichý
a	a	k8xC	a
ukázněný	ukázněný	k2eAgInSc1d1	ukázněný
zástup	zástup	k1gInSc1	zástup
<g/>
.	.	kIx.	.
</s>
<s>
Guvernér	guvernér	k1gMnSc1	guvernér
Thomas	Thomas	k1gMnSc1	Thomas
Hutchinson	Hutchinson	k1gMnSc1	Hutchinson
byl	být	k5eAaImAgMnS	být
událostí	událost	k1gFnSc7	událost
znechucen	znechutit	k5eAaPmNgMnS	znechutit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nejdrzejší	drzý	k2eAgInSc1d3	nejdrzejší
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
odehrál	odehrát	k5eAaPmAgMnS	odehrát
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
mínil	mínit	k5eAaImAgMnS	mínit
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
události	událost	k1gFnPc4	událost
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
tolik	tolik	k4xDc1	tolik
řádných	řádný	k2eAgMnPc2d1	řádný
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bostonu	Boston	k1gInSc2	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
samotným	samotný	k2eAgNnSc7d1	samotné
zničením	zničení	k1gNnSc7	zničení
čaje	čaj	k1gInSc2	čaj
však	však	k9	však
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
ani	ani	k8xC	ani
mnozí	mnohý	k2eAgMnPc1d1	mnohý
američtí	americký	k2eAgMnPc1d1	americký
představitelé	představitel	k1gMnPc1	představitel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklina	k1gFnPc2	Franklina
i	i	k8xC	i
George	Georg	k1gInSc2	Georg
Washington	Washington	k1gInSc4	Washington
zastávali	zastávat	k5eAaImAgMnP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
zničený	zničený	k2eAgInSc1d1	zničený
čaj	čaj	k1gInSc1	čaj
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
společnosti	společnost	k1gFnSc2	společnost
East	Easta	k1gFnPc2	Easta
India	indium	k1gNnSc2	indium
Company	Compana	k1gFnSc2	Compana
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
částku	částka	k1gFnSc4	částka
<g/>
:	:	kIx,	:
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zhruba	zhruba	k6eAd1	zhruba
9000	[number]	k4	9000
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
asi	asi	k9	asi
1,02	[number]	k4	1,02
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
GBP	GBP	kA	GBP
<g/>
.	.	kIx.	.
</s>
<s>
Newyorský	newyorský	k2eAgMnSc1d1	newyorský
obchodník	obchodník	k1gMnSc1	obchodník
Robert	Robert	k1gMnSc1	Robert
Murray	Murraa	k1gFnSc2	Murraa
dokonce	dokonce	k9	dokonce
oslovil	oslovit	k5eAaPmAgInS	oslovit
lorda	lord	k1gMnSc4	lord
Northa	North	k1gMnSc4	North
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
dalšími	další	k2eAgMnPc7d1	další
obchodníky	obchodník	k1gMnPc7	obchodník
ztráty	ztráta	k1gFnSc2	ztráta
uhradí	uhradit	k5eAaPmIp3nP	uhradit
<g/>
,	,	kIx,	,
nabídka	nabídka	k1gFnSc1	nabídka
však	však	k9	však
byla	být	k5eAaImAgFnS	být
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
Samuel	Samuel	k1gMnSc1	Samuel
Adams	Adamsa	k1gFnPc2	Adamsa
účastnil	účastnit	k5eAaImAgMnS	účastnit
plánování	plánování	k1gNnSc3	plánování
protestu	protest	k1gInSc2	protest
<g/>
,	,	kIx,	,
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
však	však	k9	však
ihned	ihned	k6eAd1	ihned
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
dýchánek	dýchánek	k1gInSc1	dýchánek
nebyl	být	k5eNaImAgInS	být
činem	čin	k1gInSc7	čin
chátry	chátra	k1gFnSc2	chátra
nerespektující	respektující	k2eNgInSc1d1	nerespektující
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zásadním	zásadní	k2eAgInSc7d1	zásadní
protestem	protest	k1gInSc7	protest
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
zbývajícím	zbývající	k2eAgInSc7d1	zbývající
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohl	moct	k5eAaImAgInS	moct
lid	lid	k1gInSc4	lid
hájit	hájit	k5eAaImF	hájit
svá	svůj	k3xOyFgNnPc4	svůj
ústavní	ústavní	k2eAgNnPc4d1	ústavní
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
ústavní	ústavní	k2eAgInSc1d1	ústavní
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
odvolával	odvolávat	k5eAaImAgMnS	odvolávat
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
má	mít	k5eAaImIp3nS	mít
psanou	psaný	k2eAgFnSc4d1	psaná
formu	forma	k1gFnSc4	forma
či	či	k8xC	či
ne	ne	k9	ne
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
že	že	k8xS	že
ústavu	ústava	k1gFnSc4	ústava
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
lze	lze	k6eAd1	lze
vyložit	vyložit	k5eAaPmF	vyložit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zapovídá	zapovídat	k5eAaImIp3nS	zapovídat
ukládat	ukládat	k5eAaImF	ukládat
někomu	někdo	k3yInSc3	někdo
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
zastoupení	zastoupení	k1gNnSc2	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1689	[number]	k4	1689
ustanovovala	ustanovovat	k5eAaImAgFnS	ustanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trvalé	trvalý	k2eAgFnPc1d1	trvalá
daně	daň	k1gFnPc1	daň
nelze	lze	k6eNd1	lze
uvalit	uvalit	k5eAaPmF	uvalit
bez	bez	k7c2	bez
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
precedenční	precedenční	k2eAgNnPc4d1	precedenční
ustanovení	ustanovení	k1gNnPc4	ustanovení
zase	zase	k9	zase
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
parlament	parlament	k1gInSc1	parlament
legitimní	legitimní	k2eAgInSc1d1	legitimní
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
skutečně	skutečně	k6eAd1	skutečně
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
vládne	vládnout	k5eAaImIp3nS	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
argumentace	argumentace	k1gFnSc1	argumentace
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bostonský	bostonský	k2eAgInSc1d1	bostonský
čajový	čajový	k2eAgInSc1d1	čajový
dýchánek	dýchánek	k1gInSc1	dýchánek
nebyl	být	k5eNaImAgInS	být
protestem	protest	k1gInSc7	protest
proti	proti	k7c3	proti
výši	výše	k1gFnSc3	výše
daně	daň	k1gFnSc2	daň
-	-	kIx~	-
tu	tu	k6eAd1	tu
Čajový	čajový	k2eAgInSc1d1	čajový
zákon	zákon	k1gInSc1	zákon
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nezvýšil	zvýšit	k5eNaPmAgMnS	zvýšit
-	-	kIx~	-
ale	ale	k8xC	ale
proti	proti	k7c3	proti
způsobu	způsob	k1gInSc3	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
Británie	Británie	k1gFnSc1	Británie
o	o	k7c4	o
obhodu	obhoda	k1gFnSc4	obhoda
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
koloniích	kolonie	k1gFnPc6	kolonie
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
byli	být	k5eAaImAgMnP	být
událostí	událost	k1gFnSc7	událost
zděšeni	zděšen	k2eAgMnPc1d1	zděšen
i	i	k8xC	i
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
dosud	dosud	k6eAd1	dosud
americkým	americký	k2eAgFnPc3d1	americká
koloniím	kolonie	k1gFnPc3	kolonie
nakloněni	naklonit	k5eAaPmNgMnP	naklonit
<g/>
,	,	kIx,	,
a	a	k8xC	a
akce	akce	k1gFnSc1	akce
tak	tak	k6eAd1	tak
proti	proti	k7c3	proti
koloniím	kolonie	k1gFnPc3	kolonie
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
premiéra	premiér	k1gMnSc2	premiér
lorda	lord	k1gMnSc2	lord
Northa	North	k1gMnSc2	North
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
uzavřením	uzavření	k1gNnSc7	uzavření
bostonského	bostonský	k2eAgInSc2d1	bostonský
přístavu	přístav	k1gInSc2	přístav
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc4	jeho
funkci	funkce	k1gFnSc4	funkce
převzal	převzít	k5eAaPmAgInS	převzít
nedaleký	daleký	k2eNgInSc1d1	nedaleký
Salem	Salem	k1gInSc1	Salem
-	-	kIx~	-
a	a	k8xC	a
přijetím	přijetí	k1gNnSc7	přijetí
dalších	další	k2eAgInPc2d1	další
zákonů	zákon	k1gInPc2	zákon
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
souhrnně	souhrnně	k6eAd1	souhrnně
Donucovací	donucovací	k2eAgInPc4d1	donucovací
zákony	zákon	k1gInPc4	zákon
(	(	kIx(	(
<g/>
Coercive	Coerciev	k1gFnPc4	Coerciev
Acts	Acts	k1gInSc1	Acts
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
koloniích	kolonie	k1gFnPc6	kolonie
označovány	označovat	k5eAaImNgInP	označovat
dnes	dnes	k6eAd1	dnes
známějším	známý	k2eAgInSc7d2	známější
názvem	název	k1gInSc7	název
Nepřijatelné	přijatelný	k2eNgInPc4d1	nepřijatelný
zákony	zákon	k1gInPc4	zákon
(	(	kIx(	(
<g/>
Intolerable	Intolerable	k1gFnSc1	Intolerable
Acts	Actsa	k1gFnPc2	Actsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
uzavření	uzavření	k1gNnSc2	uzavření
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgFnP	týkat
úpravy	úprava	k1gFnPc1	úprava
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnSc6	možnost
přesunutí	přesunutí	k1gNnSc2	přesunutí
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
také	také	k9	také
pravidel	pravidlo	k1gNnPc2	pravidlo
ubytovávání	ubytovávání	k1gNnSc4	ubytovávání
britského	britský	k2eAgNnSc2d1	Britské
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
kolonií	kolonie	k1gFnPc2	kolonie
se	se	k3xPyFc4	se
na	na	k7c6	na
několika	několik	k4yIc6	několik
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
bostonskou	bostonský	k2eAgFnSc7d1	Bostonská
událostí	událost	k1gFnSc7	událost
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
k	k	k7c3	k
podobným	podobný	k2eAgInPc3d1	podobný
prostestům	prostest	k1gInPc3	prostest
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1774	[number]	k4	1774
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
v	v	k7c6	v
Annapolisu	Annapolis	k1gInSc6	Annapolis
zapálena	zapálit	k5eAaPmNgFnS	zapálit
a	a	k8xC	a
zničena	zničit	k5eAaPmNgFnS	zničit
loď	loď	k1gFnSc1	loď
Peggy	Pegga	k1gFnSc2	Pegga
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obchodovala	obchodovat	k5eAaImAgFnS	obchodovat
s	s	k7c7	s
čajem	čaj	k1gInSc7	čaj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
kolonisté	kolonista	k1gMnPc1	kolonista
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
po	po	k7c6	po
britské	britský	k2eAgFnSc6d1	britská
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
Bostonský	bostonský	k2eAgInSc4d1	bostonský
čajový	čajový	k2eAgInSc4d1	čajový
dýchánek	dýchánek	k1gInSc4	dýchánek
bojkotovat	bojkotovat	k5eAaImF	bojkotovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Bostonu	Boston	k1gInSc6	Boston
se	se	k3xPyFc4	se
podobná	podobný	k2eAgFnSc1d1	podobná
událost	událost	k1gFnSc1	událost
s	s	k7c7	s
ničením	ničení	k1gNnSc7	ničení
čaje	čaj	k1gInSc2	čaj
opakovala	opakovat	k5eAaImAgFnS	opakovat
již	již	k6eAd1	již
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1774	[number]	k4	1774
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vyhozeno	vyhozen	k2eAgNnSc1d1	vyhozeno
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
15	[number]	k4	15
beden	bedna	k1gFnPc2	bedna
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Bostonská	bostonský	k2eAgFnSc1d1	Bostonská
událost	událost	k1gFnSc1	událost
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
zostření	zostření	k1gNnSc3	zostření
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vláda	vláda	k1gFnSc1	vláda
snažila	snažit	k5eAaImAgFnS	snažit
zmírnit	zmírnit	k5eAaPmF	zmírnit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1775	[number]	k4	1775
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
přijato	přijmout	k5eAaPmNgNnS	přijmout
usnesení	usnesení	k1gNnSc1	usnesení
směřující	směřující	k2eAgNnSc1d1	směřující
k	k	k7c3	k
usmíření	usmíření	k1gNnSc3	usmíření
s	s	k7c7	s
koloniemi	kolonie	k1gFnPc7	kolonie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Conciliatory	Conciliator	k1gInPc1	Conciliator
Resolution	Resolution	k1gInSc1	Resolution
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
danění	daněný	k2eAgMnPc1d1	daněný
takových	takový	k3xDgFnPc2	takový
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
míře	míra	k1gFnSc6	míra
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
britských	britský	k2eAgMnPc2d1	britský
státních	státní	k2eAgMnPc2d1	státní
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Daň	daň	k1gFnSc1	daň
z	z	k7c2	z
čaje	čaj	k1gInSc2	čaj
přestala	přestat	k5eAaPmAgFnS	přestat
platit	platit	k5eAaImF	platit
s	s	k7c7	s
přijetím	přijetí	k1gNnSc7	přijetí
nového	nový	k2eAgInSc2d1	nový
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
zdanění	zdanění	k1gNnSc6	zdanění
kolonií	kolonie	k1gFnPc2	kolonie
(	(	kIx(	(
<g/>
Taxation	Taxation	k1gInSc1	Taxation
of	of	k?	of
Colonies	Colonies	k1gInSc1	Colonies
Act	Act	k1gFnSc1	Act
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1778	[number]	k4	1778
<g/>
.	.	kIx.	.
</s>
<s>
Bostonské	bostonský	k2eAgNnSc1d1	Bostonské
pití	pití	k1gNnSc1	pití
čaje	čaj	k1gInSc2	čaj
však	však	k9	však
přesto	přesto	k8xC	přesto
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
zaujetí	zaujetí	k1gNnSc3	zaujetí
vyhraněnějších	vyhraněný	k2eAgInPc2d2	vyhraněnější
postojů	postoj	k1gInPc2	postoj
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vlastenců	vlastenec	k1gMnPc2	vlastenec
i	i	k8xC	i
loyalistů	loyalista	k1gMnPc2	loyalista
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
předznamenaly	předznamenat	k5eAaPmAgFnP	předznamenat
Americkou	americký	k2eAgFnSc4d1	americká
revoluci	revoluce	k1gFnSc4	revoluce
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
