<s>
Hugenotské	hugenotský	k2eAgFnPc1d1	hugenotská
války	válka	k1gFnPc1	válka
(	(	kIx(	(
<g/>
též	též	k9	též
náboženské	náboženský	k2eAgFnPc1d1	náboženská
války	válka	k1gFnPc1	válka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
konflikt	konflikt	k1gInSc4	konflikt
probíhající	probíhající	k2eAgInSc4d1	probíhající
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
mezi	mezi	k7c7	mezi
hugenoty	hugenot	k1gMnPc7	hugenot
a	a	k8xC	a
katolíky	katolík	k1gMnPc7	katolík
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1562	[number]	k4	1562
<g/>
-	-	kIx~	-
<g/>
1598	[number]	k4	1598
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
většinou	většinou	k6eAd1	většinou
uvádějí	uvádět	k5eAaImIp3nP	uvádět
těchto	tento	k3xDgFnPc2	tento
válek	válka	k1gFnPc2	válka
osm	osm	k4xCc1	osm
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
pronikat	pronikat	k5eAaImF	pronikat
luteránství	luteránství	k1gNnSc1	luteránství
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
královské	královský	k2eAgFnSc3d1	královská
vládě	vláda	k1gFnSc3	vláda
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
podařilo	podařit	k5eAaPmAgNnS	podařit
jej	on	k3xPp3gInSc4	on
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
značnou	značný	k2eAgFnSc4d1	značná
odezvu	odezva	k1gFnSc4	odezva
ovšem	ovšem	k9	ovšem
získal	získat	k5eAaPmAgInS	získat
kalvinismus	kalvinismus	k1gInSc1	kalvinismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgInS	šířit
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Přijímali	přijímat	k5eAaImAgMnP	přijímat
jej	on	k3xPp3gNnSc4	on
bohatí	bohatý	k2eAgMnPc1d1	bohatý
i	i	k8xC	i
drobní	drobný	k2eAgMnPc1d1	drobný
měšťané	měšťan	k1gMnPc1	měšťan
a	a	k8xC	a
vítala	vítat	k5eAaImAgFnS	vítat
jej	on	k3xPp3gMnSc4	on
i	i	k9	i
šlechta	šlechta	k1gFnSc1	šlechta
a	a	k8xC	a
část	část	k1gFnSc1	část
poddaných	poddaný	k1gMnPc2	poddaný
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
kalvinismu	kalvinismus	k1gInSc6	kalvinismus
viděli	vidět	k5eAaImAgMnP	vidět
prostředek	prostředek	k1gInSc4	prostředek
očisty	očista	k1gFnSc2	očista
z	z	k7c2	z
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
nedobrých	dobrý	k2eNgInPc2d1	nedobrý
poměrů	poměr	k1gInPc2	poměr
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
církevních	církevní	k2eAgFnPc2d1	církevní
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
nereformovaná	reformovaný	k2eNgFnSc1d1	nereformovaná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zpustlá	zpustlý	k2eAgFnSc1d1	zpustlá
a	a	k8xC	a
bohatá	bohatý	k2eAgFnSc1d1	bohatá
církev	církev	k1gFnSc1	církev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
společenských	společenský	k2eAgInPc2d1	společenský
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
měšťany	měšťan	k1gMnPc7	měšťan
a	a	k8xC	a
poddanými	poddaný	k1gMnPc7	poddaný
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
a	a	k8xC	a
privilegovanými	privilegovaný	k2eAgInPc7d1	privilegovaný
stavy	stav	k1gInPc7	stav
-	-	kIx~	-
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
šlechtou	šlechta	k1gFnSc7	šlechta
-	-	kIx~	-
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
dvorskou	dvorský	k2eAgFnSc7d1	dvorská
a	a	k8xC	a
venkovskou	venkovský	k2eAgFnSc7d1	venkovská
aristokracií	aristokracie	k1gFnSc7	aristokracie
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
posilování	posilování	k1gNnSc3	posilování
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
i	i	k9	i
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
(	(	kIx(	(
<g/>
inflace	inflace	k1gFnSc1	inflace
způsobená	způsobený	k2eAgFnSc1d1	způsobená
přílivem	příliv	k1gInSc7	příliv
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
ze	z	k7c2	z
španělských	španělský	k2eAgFnPc2d1	španělská
amerických	americký	k2eAgFnPc2d1	americká
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
vyčerpávajícími	vyčerpávající	k2eAgFnPc7d1	vyčerpávající
"	"	kIx"	"
<g/>
italskými	italský	k2eAgFnPc7d1	italská
válkami	válka	k1gFnPc7	válka
<g/>
"	"	kIx"	"
-	-	kIx~	-
šesti	šest	k4xCc6	šest
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1521	[number]	k4	1521
a	a	k8xC	a
1559	[number]	k4	1559
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vedoucích	vedoucí	k2eAgFnPc6d1	vedoucí
pozicích	pozice	k1gFnPc6	pozice
mezi	mezi	k7c7	mezi
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
kalvinisty	kalvinista	k1gMnPc7	kalvinista
<g/>
,	,	kIx,	,
zvanými	zvaný	k2eAgMnPc7d1	zvaný
hugenoti	hugenot	k1gMnPc1	hugenot
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Bourboni	Bourbon	k1gMnPc1	Bourbon
<g/>
,	,	kIx,	,
blízcí	blízký	k2eAgMnPc1d1	blízký
příbuzní	příbuzný	k1gMnPc1	příbuzný
(	(	kIx(	(
<g/>
následující	následující	k2eAgMnSc1d1	následující
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nástupnictví	nástupnictví	k1gNnSc6	nástupnictví
<g/>
)	)	kIx)	)
královské	královský	k2eAgFnSc2d1	královská
dynastie	dynastie	k1gFnSc2	dynastie
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
panovali	panovat	k5eAaImAgMnP	panovat
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
Navarrském	navarrský	k2eAgNnSc6d1	Navarrské
království	království	k1gNnSc6	království
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Pyrenejích	Pyreneje	k1gFnPc6	Pyreneje
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
jim	on	k3xPp3gMnPc3	on
vzal	vzít	k5eAaPmAgInS	vzít
aragonský	aragonský	k2eAgMnSc1d1	aragonský
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
hugenotští	hugenotský	k2eAgMnPc1d1	hugenotský
Bourboni	Bourbon	k1gMnPc1	Bourbon
měli	mít	k5eAaImAgMnP	mít
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
královskou	královský	k2eAgFnSc4d1	královská
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
vévodové	vévoda	k1gMnPc1	vévoda
de	de	k?	de
Guise	Guise	k1gFnSc1	Guise
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
Guisové	Guisové	k2eAgFnSc1d1	Guisové
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
urvat	urvat	k5eAaPmF	urvat
nekompromisním	kompromisní	k2eNgNnSc7d1	nekompromisní
katolictvím	katolictví	k1gNnSc7	katolictví
královskou	královský	k2eAgFnSc4d1	královská
moc	moc	k6eAd1	moc
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
pro	pro	k7c4	pro
katolickou	katolický	k2eAgFnSc4d1	katolická
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
širším	široký	k2eAgInSc6d2	širší
pohledu	pohled	k1gInSc6	pohled
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Hugenotské	hugenotský	k2eAgInPc1d1	hugenotský
války	válek	k1gInPc1	válek
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgInP	být
jen	jen	k6eAd1	jen
částí	část	k1gFnSc7	část
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
Habsburky	Habsburk	k1gMnPc7	Habsburk
<g/>
,	,	kIx,	,
snažícími	snažící	k2eAgMnPc7d1	snažící
se	se	k3xPyFc4	se
ovládnout	ovládnout	k5eAaPmF	ovládnout
celou	celá	k1gFnSc4	celá
jižní	jižní	k2eAgFnSc4d1	jižní
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
silami	síla	k1gFnPc7	síla
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
bránícími	bránící	k2eAgInPc7d1	bránící
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
liga	liga	k1gFnSc1	liga
a	a	k8xC	a
Guisové	Guis	k1gMnPc1	Guis
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
"	"	kIx"	"
<g/>
jen	jen	k6eAd1	jen
<g/>
"	"	kIx"	"
agenty	agens	k1gInPc7	agens
(	(	kIx(	(
<g/>
španělských	španělský	k2eAgInPc2d1	španělský
<g/>
)	)	kIx)	)
Habsburků	Habsburk	k1gInPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
války	válka	k1gFnPc1	válka
a	a	k8xC	a
boje	boj	k1gInPc1	boj
trvaly	trvat	k5eAaImAgInP	trvat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Francouzsko-Španělské	francouzsko-španělský	k2eAgFnSc2d1	francouzsko-španělská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
Pyrenejským	pyrenejský	k2eAgInSc7d1	pyrenejský
mírem	mír	k1gInSc7	mír
<g/>
)	)	kIx)	)
1659	[number]	k4	1659
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc4	Španělsko
byla	být	k5eAaImAgFnS	být
pokračováním	pokračování	k1gNnSc7	pokračování
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1562	[number]	k4	1562
masakrem	masakr	k1gInSc7	masakr
hugenotů	hugenot	k1gMnPc2	hugenot
ve	v	k7c4	v
Wassy	Wass	k1gInPc4	Wass
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
války	válek	k1gInPc1	válek
trvaly	trvat	k5eAaImAgInP	trvat
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
jednáními	jednání	k1gNnPc7	jednání
<g/>
,	,	kIx,	,
míry	míra	k1gFnSc2	míra
(	(	kIx(	(
<g/>
edikt	edikt	k1gInSc1	edikt
amboiský	amboiský	k2eAgInSc1d1	amboiský
a	a	k8xC	a
míry	míra	k1gFnPc1	míra
z	z	k7c2	z
Longjumeau	Longjumeaus	k1gInSc2	Longjumeaus
a	a	k8xC	a
ze	z	k7c2	z
Saint-Germain-en-Laye	Saint-Germainn-Lay	k1gFnSc2	Saint-Germain-en-Lay
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
porušováním	porušování	k1gNnSc7	porušování
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Guisy	Guis	k1gMnPc7	Guis
a	a	k8xC	a
hugenoty	hugenot	k1gMnPc7	hugenot
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
také	také	k9	také
mezi	mezi	k7c7	mezi
královskou	královský	k2eAgFnSc7d1	královská
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
královna	královna	k1gFnSc1	královna
matka	matka	k1gFnSc1	matka
Kateřina	Kateřina	k1gFnSc1	Kateřina
Medicejská	Medicejský	k2eAgFnSc1d1	Medicejská
a	a	k8xC	a
která	který	k3yQgFnSc1	který
sňatkem	sňatek	k1gInSc7	sňatek
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
princezny	princezna	k1gFnSc2	princezna
Markéty	Markéta	k1gFnSc2	Markéta
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Navarrským	navarrský	k2eAgMnSc7d1	navarrský
chtěla	chtít	k5eAaImAgFnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
usmíření	usmíření	k1gNnSc3	usmíření
<g/>
,	,	kIx,	,
a	a	k8xC	a
Guisy	Guisa	k1gFnSc2	Guisa
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
ve	v	k7c6	v
vraždění	vraždění	k1gNnSc6	vraždění
Bartolomějské	bartolomějský	k2eAgFnSc2d1	Bartolomějská
noci	noc	k1gFnSc2	noc
(	(	kIx(	(
<g/>
noci	noc	k1gFnSc2	noc
ze	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1572	[number]	k4	1572
<g/>
)	)	kIx)	)
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Vražděni	vražděn	k2eAgMnPc1d1	vražděn
byli	být	k5eAaImAgMnP	být
předáci	předák	k1gMnPc1	předák
hugenotů	hugenot	k1gMnPc2	hugenot
a	a	k8xC	a
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
právě	právě	k6eAd1	právě
přítomní	přítomný	k2eAgMnPc1d1	přítomný
hugenoté	hugenota	k1gMnPc1	hugenota
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přijeli	přijet	k5eAaPmAgMnP	přijet
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
do	do	k7c2	do
silně	silně	k6eAd1	silně
katolické	katolický	k2eAgFnSc2d1	katolická
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
až	až	k6eAd1	až
provokativně	provokativně	k6eAd1	provokativně
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
masakru	masakr	k1gInSc2	masakr
jen	jen	k9	jen
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
2	[number]	k4	2
700	[number]	k4	700
hugenotů	hugenot	k1gMnPc2	hugenot
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Paříží	Paříž	k1gFnSc7	Paříž
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
"	"	kIx"	"
<g/>
omylem	omylem	k6eAd1	omylem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
ženich	ženich	k1gMnSc1	ženich
<g/>
,	,	kIx,	,
králův	králův	k2eAgMnSc1d1	králův
švagr	švagr	k1gMnSc1	švagr
nebyl	být	k5eNaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
přestoupit	přestoupit	k5eAaPmF	přestoupit
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
Slavného	slavný	k2eAgMnSc2d1	slavný
chirurga	chirurg	k1gMnSc2	chirurg
Ambroise	Ambroise	k1gFnSc2	Ambroise
Parého	Parý	k1gMnSc2	Parý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
také	také	k9	také
hugenot	hugenot	k1gMnSc1	hugenot
<g/>
,	,	kIx,	,
zachránil	zachránit	k5eAaPmAgMnS	zachránit
samotný	samotný	k2eAgMnSc1d1	samotný
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
IX	IX	kA	IX
<g/>
.	.	kIx.	.
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
schoval	schovat	k5eAaPmAgMnS	schovat
do	do	k7c2	do
šatníku	šatník	k1gInSc2	šatník
<g/>
.	.	kIx.	.
</s>
<s>
Bartolomějskou	bartolomějský	k2eAgFnSc7d1	Bartolomějská
nocí	noc	k1gFnSc7	noc
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
vystupňovala	vystupňovat	k5eAaPmAgFnS	vystupňovat
<g/>
.	.	kIx.	.
</s>
<s>
Války	válka	k1gFnPc1	válka
trvaly	trvat	k5eAaImAgFnP	trvat
opět	opět	k6eAd1	opět
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
jednáními	jednání	k1gNnPc7	jednání
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
(	(	kIx(	(
<g/>
Edikt	edikt	k1gInSc1	edikt
z	z	k7c2	z
Beaulieu	Beaulieus	k1gInSc2	Beaulieus
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
mír	mír	k1gInSc1	mír
Pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Joinville	Joinville	k1gNnSc2	Joinville
<g/>
,	,	kIx,	,
Nemours	Nemoursa	k1gFnPc2	Nemoursa
<g/>
,	,	kIx,	,
míry	míra	k1gFnSc2	míra
z	z	k7c2	z
Longjumeau	Longjumeaus	k1gInSc2	Longjumeaus
a	a	k8xC	a
opět	opět	k6eAd1	opět
ze	z	k7c2	z
Saint-Germain-en-Laye	Saint-Germainn-Lay	k1gFnSc2	Saint-Germain-en-Lay
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
porušováním	porušování	k1gNnSc7	porušování
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
předcházejím	předcházej	k1gFnPc3	předcházej
náboženským	náboženský	k2eAgFnPc3d1	náboženská
válkám	válka	k1gFnPc3	válka
sílilo	sílit	k5eAaImAgNnS	sílit
i	i	k9	i
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Guisy	Guis	k1gMnPc7	Guis
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
katolickou	katolický	k2eAgFnSc7d1	katolická
ligou	liga	k1gFnSc7	liga
a	a	k8xC	a
královskou	královský	k2eAgFnSc7d1	královská
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jimi	on	k3xPp3gInPc7	on
byla	být	k5eAaImAgNnP	být
vmanévrována	vmanévrován	k2eAgFnSc1d1	vmanévrována
i	i	k8xC	i
do	do	k7c2	do
vraždění	vraždění	k1gNnSc2	vraždění
Bartolomějské	bartolomějský	k2eAgFnSc2d1	Bartolomějská
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
zavražděním	zavraždění	k1gNnSc7	zavraždění
vévody	vévoda	k1gMnSc2	vévoda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
I.	I.	kA	I.
de	de	k?	de
Guise	Guis	k1gMnSc2	Guis
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
kardinála	kardinál	k1gMnSc2	kardinál
de	de	k?	de
Guise	Guis	k1gMnSc2	Guis
královými	králův	k2eAgMnPc7d1	králův
ozbrojenci	ozbrojenec	k1gMnPc7	ozbrojenec
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c4	v
Blois	Blois	k1gFnSc4	Blois
a	a	k8xC	a
krále	král	k1gMnSc4	král
Jindřicha	Jindřich	k1gMnSc4	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
dominikánským	dominikánský	k2eAgMnSc7d1	dominikánský
mnichem	mnich	k1gMnSc7	mnich
Jacquesem	Jacques	k1gMnSc7	Jacques
Clémentem	Clément	k1gMnSc7	Clément
v	v	k7c6	v
Saint-Cloudu	Saint-Cloud	k1gInSc6	Saint-Cloud
<g/>
.	.	kIx.	.
</s>
<s>
Trvaly	trvat	k5eAaImAgInP	trvat
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
menšími	malý	k2eAgMnPc7d2	menší
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1562	[number]	k4	1562
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
<g/>
.	.	kIx.	.
</s>
<s>
Hugenotská	hugenotský	k2eAgFnSc1d1	hugenotská
šlechta	šlechta	k1gFnSc1	šlechta
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
německými	německý	k2eAgMnPc7d1	německý
protestantskými	protestantský	k2eAgMnPc7d1	protestantský
knížaty	kníže	k1gMnPc7wR	kníže
měla	mít	k5eAaImAgFnS	mít
silné	silný	k2eAgFnPc4d1	silná
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
a	a	k8xC	a
jihozápadních	jihozápadní	k2eAgInPc6d1	jihozápadní
krajích	kraj	k1gInPc6	kraj
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Francii	Francie	k1gFnSc6	Francie
dominovala	dominovat	k5eAaImAgFnS	dominovat
Katolická	katolický	k2eAgFnSc1d1	katolická
liga	liga	k1gFnSc1	liga
vedená	vedený	k2eAgFnSc1d1	vedená
rodem	rod	k1gInSc7	rod
Guisů	Guis	k1gMnPc2	Guis
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Habsburků	Habsburk	k1gInPc2	Habsburk
hlavně	hlavně	k9	hlavně
španělských	španělský	k2eAgInPc2d1	španělský
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
a	a	k8xC	a
říšských	říšský	k2eAgFnPc2d1	říšská
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jedné	jeden	k4xCgFnSc3	jeden
straně	strana	k1gFnSc3	strana
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
trvaleji	trvale	k6eAd2	trvale
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jednoznačné	jednoznačný	k2eAgInPc4d1	jednoznačný
vojenské	vojenský	k2eAgInPc4d1	vojenský
převahy	převah	k1gInPc4	převah
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
únavě	únava	k1gFnSc3	únava
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
snaha	snaha	k1gFnSc1	snaha
umírněných	umírněný	k2eAgMnPc2d1	umírněný
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgFnPc2d1	zvaná
"	"	kIx"	"
<g/>
politici	politik	k1gMnPc1	politik
<g/>
"	"	kIx"	"
a	a	k8xC	a
hugenotů	hugenot	k1gMnPc2	hugenot
po	po	k7c6	po
dorozumění	dorozumění	k1gNnSc6	dorozumění
a	a	k8xC	a
míru	mír	k1gInSc6	mír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
posledního	poslední	k2eAgMnSc2d1	poslední
krále	král	k1gMnSc2	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Valois	Valois	k1gFnSc2	Valois
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
jeho	jeho	k3xOp3gMnSc1	jeho
švagr	švagr	k1gMnSc1	švagr
vůdce	vůdce	k1gMnSc1	vůdce
hugenotů	hugenot	k1gMnPc2	hugenot
Bourbon	bourbon	k1gInSc1	bourbon
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
si	se	k3xPyFc3	se
dosud	dosud	k6eAd1	dosud
váhající	váhající	k2eAgMnPc4d1	váhající
katolíky	katolík	k1gMnPc4	katolík
i	i	k8xC	i
odbojnou	odbojný	k2eAgFnSc4d1	odbojná
Paříž	Paříž	k1gFnSc4	Paříž
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Paris	Paris	k1gMnSc1	Paris
vaut	vaut	k1gMnSc1	vaut
bien	biena	k1gFnPc2	biena
une	une	k?	une
messe	mess	k1gMnSc2	mess
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
Paříž	Paříž	k1gFnSc1	Paříž
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
mši	mše	k1gFnSc4	mše
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
1593	[number]	k4	1593
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1598	[number]	k4	1598
vydal	vydat	k5eAaPmAgInS	vydat
Edikt	edikt	k1gInSc4	edikt
Nantský	nantský	k2eAgInSc4d1	nantský
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nantes	Nantesa	k1gFnPc2	Nantesa
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
hugenoty	hugenot	k1gMnPc7	hugenot
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
listina	listina	k1gFnSc1	listina
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
svobodu	svoboda	k1gFnSc4	svoboda
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc2d1	trvající
hugenotské	hugenotský	k2eAgFnSc2d1	hugenotská
války	válka	k1gFnSc2	válka
Francii	Francie	k1gFnSc6	Francie
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
jak	jak	k8xC	jak
hospodářsky	hospodářsky	k6eAd1	hospodářsky
tak	tak	k9	tak
i	i	k9	i
politicky	politicky	k6eAd1	politicky
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nepochybně	pochybně	k6eNd1	pochybně
projevil	projevit	k5eAaPmAgInS	projevit
jako	jako	k9	jako
výborný	výborný	k2eAgMnSc1d1	výborný
panovník	panovník	k1gMnSc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Uklidnil	uklidnit	k5eAaPmAgInS	uklidnit
znesvářené	znesvářený	k2eAgInPc4d1	znesvářený
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
rody	rod	k1gInPc4	rod
<g/>
,	,	kIx,	,
Rolníkům	rolník	k1gMnPc3	rolník
snížil	snížit	k5eAaPmAgInS	snížit
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
účinně	účinně	k6eAd1	účinně
bránit	bránit	k5eAaImF	bránit
lichvě	lichva	k1gFnSc3	lichva
a	a	k8xC	a
zpronevěrám	zpronevěra	k1gFnPc3	zpronevěra
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
opravit	opravit	k5eAaPmF	opravit
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
splavňovat	splavňovat	k5eAaImF	splavňovat
řeky	řeka	k1gFnPc4	řeka
<g/>
,	,	kIx,	,
vyhloubil	vyhloubit	k5eAaPmAgMnS	vyhloubit
kanál	kanál	k1gInSc4	kanál
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Seinou	Seina	k1gFnSc7	Seina
a	a	k8xC	a
Loirou	Loira	k1gFnSc7	Loira
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
stavět	stavět	k5eAaImF	stavět
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Východoindická	východoindický	k2eAgFnSc1d1	Východoindická
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
podporována	podporován	k2eAgFnSc1d1	podporována
kolonizace	kolonizace	k1gFnSc1	kolonizace
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
politicky	politicky	k6eAd1	politicky
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
a	a	k8xC	a
zbavena	zbaven	k2eAgFnSc1d1	zbavena
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
následků	následek	k1gInPc2	následek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
skvělá	skvělý	k2eAgFnSc1d1	skvělá
vláda	vláda	k1gFnSc1	vláda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
ukončena	ukončit	k5eAaPmNgFnS	ukončit
jeho	jeho	k3xOp3gNnSc1	jeho
zavražděním	zavraždění	k1gNnSc7	zavraždění
rukou	ruka	k1gFnPc2	ruka
katolického	katolický	k2eAgMnSc2d1	katolický
fanatika	fanatik	k1gMnSc2	fanatik
Françoise	Françoise	k1gFnSc1	Françoise
Ravaillaca	Ravaillaca	k1gFnSc1	Ravaillaca
<g/>
.	.	kIx.	.
</s>
