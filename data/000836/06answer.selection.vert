<s>
Hugenotské	hugenotský	k2eAgFnPc1d1	hugenotská
války	válka	k1gFnPc1	válka
(	(	kIx(	(
<g/>
též	též	k9	též
náboženské	náboženský	k2eAgFnPc1d1	náboženská
války	válka	k1gFnPc1	válka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
konflikt	konflikt	k1gInSc4	konflikt
probíhající	probíhající	k2eAgInSc4d1	probíhající
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
mezi	mezi	k7c7	mezi
hugenoty	hugenot	k1gMnPc7	hugenot
a	a	k8xC	a
katolíky	katolík	k1gMnPc7	katolík
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1562	[number]	k4	1562
<g/>
-	-	kIx~	-
<g/>
1598	[number]	k4	1598
<g/>
.	.	kIx.	.
</s>
