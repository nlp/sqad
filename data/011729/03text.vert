<p>
<s>
Malika	Malika	k1gFnSc1	Malika
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
podoba	podoba	k1gFnSc1	podoba
mužského	mužský	k2eAgNnSc2d1	mužské
jména	jméno	k1gNnSc2	jméno
Malik	Malika	k1gFnPc2	Malika
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
a	a	k8xC	a
svahilském	svahilský	k2eAgInSc6d1	svahilský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
Kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
indických	indický	k2eAgInPc6d1	indický
jazycích	jazyk	k1gInPc6	jazyk
znamená	znamenat	k5eAaImIp3nS	znamenat
květina	květina	k1gFnSc1	květina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známé	známý	k2eAgFnPc1d1	známá
nositelky	nositelka	k1gFnPc1	nositelka
==	==	k?	==
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Haqq	Haqq	k1gFnSc1	Haqq
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
al-Fassi	al-Fasse	k1gFnSc4	al-Fasse
<g/>
,	,	kIx,	,
marocká	marocký	k2eAgFnSc1d1	marocká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Amar	Amar	k1gMnSc1	Amar
Sheikh	Sheikh	k1gMnSc1	Sheikh
<g/>
,	,	kIx,	,
Marathi-indická	Marathindický	k2eAgFnSc1d1	Marathi-indický
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Ayane	Ayan	k1gInSc5	Ayan
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Benarab-Attou	Benarab-Atta	k1gFnSc7	Benarab-Atta
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
de	de	k?	de
la	la	k1gNnSc2	la
Flor	Flora	k1gFnPc2	Flora
<g/>
,	,	kIx,	,
Peruanská	Peruanský	k2eAgFnSc1d1	Peruanský
návrhářka	návrhářka	k1gFnSc1	návrhářka
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
El	Ela	k1gFnPc2	Ela
Aroud	Arouda	k1gFnPc2	Arouda
<g/>
,	,	kIx,	,
marocká	marocký	k2eAgFnSc1d1	marocká
internetová	internetový	k2eAgFnSc1d1	internetová
islamistka	islamistka	k1gFnSc1	islamistka
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Kalontarova	Kalontarův	k2eAgFnSc1d1	Kalontarův
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
tanečnice	tanečnice	k1gFnSc1	tanečnice
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Ménard	Ménard	k1gInSc1	Ménard
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
Francie	Francie	k1gFnSc1	Francie
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Mokkeddem	Mokkedd	k1gInSc7	Mokkedd
<g/>
,	,	kIx,	,
alžírská	alžírský	k2eAgFnSc1d1	alžírská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Oufkir	Oufkira	k1gFnPc2	Oufkira
<g/>
,	,	kIx,	,
marocká	marocký	k2eAgFnSc1d1	marocká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Pukhraj	Pukhraj	k1gFnSc1	Pukhraj
<g/>
,	,	kIx,	,
pákistánská	pákistánský	k2eAgFnSc1d1	pákistánská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Souiri	Souir	k1gFnSc2	Souir
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Sama	Sam	k1gMnSc2	Sam
Kinisona	Kinison	k1gMnSc2	Kinison
</s>
</p>
<p>
<s>
Malika	Malika	k1gFnSc1	Malika
Tahir	Tahira	k1gFnPc2	Tahira
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
krsobruslařka	krsobruslařka	k1gFnSc1	krsobruslařka
</s>
</p>
<p>
<s>
Malika-e-Tarranum	Malika-Tarranum	k1gNnSc1	Malika-e-Tarranum
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
z	z	k7c2	z
Britské	britský	k2eAgFnSc2d1	britská
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
Pákistánu	Pákistán	k1gInSc2	Pákistán
</s>
</p>
<p>
<s>
==	==	k?	==
Mallika	Mallika	k1gFnSc1	Mallika
==	==	k?	==
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
(	(	kIx(	(
<g/>
herečka	herečka	k1gFnSc1	herečka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tamilská	tamilský	k2eAgFnSc1d1	tamilská
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Chopra	Chopra	k1gFnSc1	Chopra
<g/>
,	,	kIx,	,
indo-americká	indomerický	k2eAgFnSc1d1	indo-americký
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
obchodnice	obchodnice	k1gFnSc1	obchodnice
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Dutt	Dutta	k1gFnPc2	Dutta
<g/>
,	,	kIx,	,
indo-americká	indomerický	k2eAgFnSc1d1	indo-americký
aktivistka	aktivistka	k1gFnSc1	aktivistka
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Kapoor	Kapoora	k1gFnPc2	Kapoora
<g/>
,	,	kIx,	,
indická	indický	k2eAgFnSc1d1	indická
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Kapur	Kapura	k1gFnPc2	Kapura
<g/>
,	,	kIx,	,
ï	ï	k?	ï
novinářka	novinářka	k1gFnSc1	novinářka
pracující	pracující	k2eAgFnSc1d1	pracující
pro	pro	k7c4	pro
CNN	CNN	kA	CNN
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Prasad	Prasad	k1gInSc1	Prasad
<g/>
,	,	kIx,	,
indická	indický	k2eAgFnSc1d1	indická
politička	politička	k1gFnSc1	politička
z	z	k7c2	z
Karnataky	Karnatak	k1gInPc5	Karnatak
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Sarabhai	Sarabha	k1gFnSc2	Sarabha
<g/>
,	,	kIx,	,
gujarati-indická	gujaratindický	k2eAgFnSc1d1	gujarati-indický
tanečnice	tanečnice	k1gFnSc1	tanečnice
a	a	k8xC	a
aktivistka	aktivistka	k1gFnSc1	aktivistka
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Sengupta	Sengupta	k1gFnSc1	Sengupta
<g/>
,	,	kIx,	,
bengalsko-indická	bengalskondický	k2eAgFnSc1d1	bengalsko-indický
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
feministka	feministka	k1gFnSc1	feministka
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Sherawat	Sherawat	k1gInSc1	Sherawat
<g/>
,	,	kIx,	,
indická	indický	k2eAgFnSc1d1	indická
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Mallika	Mallika	k1gFnSc1	Mallika
Sukumaran	Sukumarana	k1gFnPc2	Sukumarana
<g/>
,	,	kIx,	,
indická	indický	k2eAgFnSc1d1	indická
televizní	televizní	k2eAgFnSc1d1	televizní
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Světová	světový	k2eAgNnPc4d1	světové
jména	jméno	k1gNnPc4	jméno
miminek	miminko	k1gNnPc2	miminko
</s>
</p>
<p>
<s>
Babynames	Babynames	k1gMnSc1	Babynames
</s>
</p>
<p>
<s>
Baby	baby	k1gNnSc1	baby
Vornamen	Vornamen	k1gInSc1	Vornamen
</s>
</p>
