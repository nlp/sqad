<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gMnSc1	Spheniscus
mendiculus	mendiculus	k1gMnSc1	mendiculus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
tučňáka	tučňák	k1gMnSc2	tučňák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Galapážských	galapážský	k2eAgInPc6d1	galapážský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
druhem	druh	k1gInSc7	druh
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
snáší	snášet	k5eAaImIp3nS	snášet
vejce	vejce	k1gNnSc4	vejce
a	a	k8xC	a
vyvádí	vyvádět	k5eAaImIp3nS	vyvádět
své	svůj	k3xOyFgNnSc4	svůj
potomstvo	potomstvo	k1gNnSc4	potomstvo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
zmínil	zmínit	k5eAaPmAgMnS	zmínit
ornitolog	ornitolog	k1gMnSc1	ornitolog
Carl	Carl	k1gMnSc1	Carl
Jakob	Jakob	k1gMnSc1	Jakob
Sundevall	Sundevall	k1gMnSc1	Sundevall
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
jméno	jméno	k1gNnSc1	jméno
mendiculus	mendiculus	k1gInSc1	mendiculus
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
malý	malý	k2eAgMnSc1d1	malý
žebrák	žebrák	k1gMnSc1	žebrák
<g/>
"	"	kIx"	"
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
malou	malý	k2eAgFnSc4d1	malá
dosahující	dosahující	k2eAgFnSc4d1	dosahující
výšku	výška	k1gFnSc4	výška
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
50	[number]	k4	50
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
výhradně	výhradně	k6eAd1	výhradně
rybí	rybí	k2eAgFnSc7d1	rybí
stravou	strava	k1gFnSc7	strava
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
pak	pak	k6eAd1	pak
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
sardinek	sardinka	k1gFnPc2	sardinka
a	a	k8xC	a
ančoviček	ančovička	k1gFnPc2	ančovička
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jeho	on	k3xPp3gInSc2	on
jídelníčku	jídelníček	k1gInSc2	jídelníček
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
i	i	k9	i
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
,	,	kIx,	,
měkkýšů	měkkýš	k1gMnPc2	měkkýš
nebo	nebo	k8xC	nebo
korýšů	korýš	k1gMnPc2	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
neobvykle	obvykle	k6eNd1	obvykle
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
dostaví	dostavit	k5eAaPmIp3nP	dostavit
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
obsadí	obsadit	k5eAaPmIp3nP	obsadit
zpravidla	zpravidla	k6eAd1	zpravidla
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgNnSc4	samý
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
na	na	k7c6	na
jakém	jaký	k3yRgInSc6	jaký
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
zpravidla	zpravidla	k6eAd1	zpravidla
monogamní	monogamní	k2eAgInPc1d1	monogamní
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
a	a	k8xC	a
před	před	k7c7	před
pářením	páření	k1gNnSc7	páření
tak	tak	k9	tak
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
krátkodobým	krátkodobý	k2eAgFnPc3d1	krátkodobá
námluvám	námluva	k1gFnPc3	námluva
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
snese	snést	k5eAaPmIp3nS	snést
samice	samice	k1gFnSc1	samice
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgNnPc1	dva
vejce	vejce	k1gNnPc1	vejce
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
květnem	květen	k1gInSc7	květen
a	a	k8xC	a
červencem	červenec	k1gInSc7	červenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
38	[number]	k4	38
až	až	k9	až
42	[number]	k4	42
dnech	den	k1gInPc6	den
inkubace	inkubace	k1gFnSc2	inkubace
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
ochmýřená	ochmýřený	k2eAgNnPc4d1	ochmýřené
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
o	o	k7c4	o
která	který	k3yIgNnPc4	který
pečuje	pečovat	k5eAaImIp3nS	pečovat
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
60	[number]	k4	60
dnech	den	k1gInPc6	den
života	život	k1gInSc2	život
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
opeřena	opeřen	k2eAgFnSc1d1	opeřen
a	a	k8xC	a
kolonii	kolonie	k1gFnSc3	kolonie
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
galápážský	galápážský	k2eAgMnSc1d1	galápážský
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
vzácný	vzácný	k2eAgMnSc1d1	vzácný
pták	pták	k1gMnSc1	pták
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc6d1	nízká
populaci	populace	k1gFnSc6	populace
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc1d1	obývající
pouze	pouze	k6eAd1	pouze
nevelkou	velký	k2eNgFnSc4d1	nevelká
rozlohu	rozloha	k1gFnSc4	rozloha
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
ochraně	ochrana	k1gFnSc6	ochrana
tohoto	tento	k3xDgInSc2	tento
ohroženého	ohrožený	k2eAgInSc2d1	ohrožený
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
nanejvýš	nanejvýš	k6eAd1	nanejvýš
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
je	být	k5eAaImIp3nS	být
endemitem	endemit	k1gInSc7	endemit
Galapážkých	Galapážká	k1gFnPc2	Galapážká
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
hnízdící	hnízdící	k2eAgFnSc1d1	hnízdící
kolonie	kolonie	k1gFnSc1	kolonie
(	(	kIx(	(
<g/>
95	[number]	k4	95
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Isabela	Isabela	k1gFnSc1	Isabela
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Albemarle	Albemarle	k1gInSc1	Albemarle
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fernandina	Fernandin	k2eAgMnSc2d1	Fernandin
(	(	kIx(	(
<g/>
Narborough	Narborough	k1gInSc1	Narborough
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
drobné	drobná	k1gFnPc4	drobná
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Floreana	Florean	k1gMnSc2	Florean
(	(	kIx(	(
<g/>
Charles	Charles	k1gMnSc1	Charles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Salvador	Salvador	k1gMnSc1	Salvador
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
(	(	kIx(	(
<g/>
Indefatigable	Indefatigable	k1gMnSc1	Indefatigable
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bartolomé	Bartolomý	k2eAgNnSc1d1	Bartolomé
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
zatoulat	zatoulat	k5eAaPmF	zatoulat
jako	jako	k8xS	jako
vůbec	vůbec	k9	vůbec
jediný	jediný	k2eAgInSc4d1	jediný
druh	druh	k1gInSc4	druh
až	až	k9	až
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
krátký	krátký	k2eAgInSc4d1	krátký
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
oceáně	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
nelze	lze	k6eNd1	lze
z	z	k7c2	z
praktického	praktický	k2eAgNnSc2d1	praktické
hlediska	hledisko	k1gNnSc2	hledisko
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tučňáka	tučňák	k1gMnSc4	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
jakousi	jakýsi	k3yIgFnSc7	jakýsi
výjimkou	výjimka	k1gFnSc7	výjimka
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
druhy	druh	k1gInPc7	druh
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
Sphenisciformes	Sphenisciformes	k1gInSc1	Sphenisciformes
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
vymezen	vymezit	k5eAaPmNgInS	vymezit
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
mohou	moct	k5eAaImIp3nP	moct
tučňáci	tučňák	k1gMnPc1	tučňák
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
ostrova	ostrov	k1gInSc2	ostrov
Isabela	Isabela	k1gFnSc1	Isabela
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
rovníkem	rovník	k1gInSc7	rovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přizpůsobení	přizpůsobení	k1gNnSc1	přizpůsobení
podmínkám	podmínka	k1gFnPc3	podmínka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
vybrali	vybrat	k5eAaPmAgMnP	vybrat
za	za	k7c4	za
pravidelná	pravidelný	k2eAgNnPc4d1	pravidelné
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
od	od	k7c2	od
25	[number]	k4	25
do	do	k7c2	do
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
28	[number]	k4	28
°	°	k?	°
<g/>
C.	C.	kA	C.
Vůbec	vůbec	k9	vůbec
přežit	přežít	k5eAaPmNgInS	přežít
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
podmínkách	podmínka	k1gFnPc6	podmínka
tučňákům	tučňák	k1gMnPc3	tučňák
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
studené	studený	k2eAgInPc1d1	studený
proudy	proud	k1gInPc1	proud
Humboldtův	Humboldtův	k2eAgInSc1d1	Humboldtův
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
Cromwellův	Cromwellův	k2eAgInSc1d1	Cromwellův
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
Galapág	Galapágy	k1gFnPc2	Galapágy
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
teplým	teplý	k2eAgInSc7d1	teplý
proudem	proud	k1gInSc7	proud
Panamským	panamský	k2eAgInSc7d1	panamský
<g/>
.	.	kIx.	.
</s>
<s>
Studené	Studené	k2eAgInPc1d1	Studené
proudy	proud	k1gInPc1	proud
ochlazují	ochlazovat	k5eAaImIp3nP	ochlazovat
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c4	na
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
<g/>
Vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
se	se	k3xPyFc4	se
tučňáci	tučňák	k1gMnPc1	tučňák
galapážští	galapážský	k2eAgMnPc1d1	galapážský
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
;	;	kIx,	;
Ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
rozsahu	rozsah	k1gInSc6	rozsah
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
kolísá	kolísat	k5eAaImIp3nS	kolísat
jeho	jeho	k3xOp3gNnSc1	jeho
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
38	[number]	k4	38
do	do	k7c2	do
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
od	od	k7c2	od
37	[number]	k4	37
do	do	k7c2	do
39	[number]	k4	39
°	°	k?	°
<g/>
C.	C.	kA	C.
Tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
snižují	snižovat	k5eAaImIp3nP	snižovat
převážně	převážně	k6eAd1	převážně
vyzařováním	vyzařování	k1gNnSc7	vyzařování
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
o	o	k7c4	o
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
chladnější	chladný	k2eAgMnSc1d2	chladnější
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
drží	držet	k5eAaImIp3nP	držet
tučňáci	tučňák	k1gMnPc1	tučňák
vždy	vždy	k6eAd1	vždy
křídla	křídlo	k1gNnPc4	křídlo
odtažena	odtažen	k2eAgNnPc4d1	odtaženo
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ochlazují	ochlazovat	k5eAaImIp3nP	ochlazovat
zrychleným	zrychlený	k2eAgInSc7d1	zrychlený
dýcháním	dýchání	k1gNnSc7	dýchání
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
75	[number]	k4	75
vdechů	vdech	k1gInPc2	vdech
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
toho	ten	k3xDgMnSc4	ten
nebylo	být	k5eNaImAgNnS	být
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
po	po	k7c6	po
okrajích	okraj	k1gInPc6	okraj
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
zobáku	zobák	k1gInSc2	zobák
mají	mít	k5eAaImIp3nP	mít
neopeřenou	opeřený	k2eNgFnSc4d1	neopeřená
a	a	k8xC	a
narůžovělou	narůžovělý	k2eAgFnSc4d1	narůžovělá
kožní	kožní	k2eAgFnSc4d1	kožní
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
prokrvená	prokrvený	k2eAgFnSc1d1	prokrvená
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
termoregulaci	termoregulace	k1gFnSc3	termoregulace
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
tělní	tělní	k2eAgFnSc1d1	tělní
teplota	teplota	k1gFnSc1	teplota
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
žlázách	žláza	k1gFnPc6	žláza
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tak	tak	k9	tak
ochlazena	ochladit	k5eAaPmNgFnS	ochladit
okolním	okolní	k2eAgInSc7d1	okolní
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
růžově	růžově	k6eAd1	růžově
zabarvení	zabarvení	k1gNnSc1	zabarvení
kůže	kůže	k1gFnSc2	kůže
patrnější	patrný	k2eAgFnSc2d2	patrnější
(	(	kIx(	(
<g/>
tmavší	tmavý	k2eAgFnSc2d2	tmavší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Tučňáci	tučňák	k1gMnPc1	tučňák
galapážští	galapážský	k2eAgMnPc1d1	galapážský
tráví	trávit	k5eAaImIp3nP	trávit
přes	přes	k7c4	přes
den	den	k1gInSc4	den
více	hodně	k6eAd2	hodně
než	než	k8xS	než
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
vylézají	vylézat	k5eAaImIp3nP	vylézat
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
až	až	k6eAd1	až
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
přepeřování	přepeřování	k1gNnSc2	přepeřování
se	se	k3xPyFc4	se
před	před	k7c7	před
sluncem	slunce	k1gNnSc7	slunce
skrývají	skrývat	k5eAaImIp3nP	skrývat
do	do	k7c2	do
stínů	stín	k1gInPc2	stín
skal	skála	k1gFnPc2	skála
nebo	nebo	k8xC	nebo
zalézají	zalézat	k5eAaImIp3nP	zalézat
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
druhům	druh	k1gInPc3	druh
rodu	rod	k1gInSc2	rod
Spheniscus	Spheniscus	k1gInSc4	Spheniscus
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
kratší	krátký	k2eAgNnSc1d2	kratší
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
také	také	k9	také
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
vyzařování	vyzařování	k1gNnSc1	vyzařování
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
tučňáci	tučňák	k1gMnPc1	tučňák
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Spheniscus	Spheniscus	k1gInSc1	Spheniscus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
tmavé	tmavý	k2eAgFnPc1d1	tmavá
tváře	tvář	k1gFnPc1	tvář
a	a	k8xC	a
jen	jen	k9	jen
úzký	úzký	k2eAgInSc1d1	úzký
bílý	bílý	k2eAgInSc1d1	bílý
pásek	pásek	k1gInSc1	pásek
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
oka	oko	k1gNnSc2	oko
k	k	k7c3	k
hrdlu	hrdlo	k1gNnSc3	hrdlo
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
napojující	napojující	k2eAgFnSc1d1	napojující
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
úzkému	úzký	k2eAgInSc3d1	úzký
pásku	pásek	k1gInSc6	pásek
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
zad	záda	k1gNnPc2	záda
má	mít	k5eAaImIp3nS	mít
tmavě	tmavě	k6eAd1	tmavě
šedou	šedá	k1gFnSc4	šedá
až	až	k9	až
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
(	(	kIx(	(
<g/>
pocitově	pocitově	k6eAd1	pocitově
černou	černý	k2eAgFnSc7d1	černá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
bíle	bíle	k6eAd1	bíle
s	s	k7c7	s
případnými	případný	k2eAgInPc7d1	případný
černými	černý	k2eAgInPc7d1	černý
fleky	flek	k1gInPc7	flek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nikdy	nikdy	k6eAd1	nikdy
nejsou	být	k5eNaImIp3nP	být
totožné	totožný	k2eAgInPc1d1	totožný
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
jedincem	jedinec	k1gMnSc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
kontrastní	kontrastní	k2eAgNnSc1d1	kontrastní
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
dělené	dělený	k2eAgNnSc1d1	dělené
nepravidelnou	pravidelný	k2eNgFnSc7d1	nepravidelná
linií	linie	k1gFnSc7	linie
připomínající	připomínající	k2eAgFnSc4d1	připomínající
podkovu	podkova	k1gFnSc4	podkova
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
mírně	mírně	k6eAd1	mírně
oddělený	oddělený	k2eAgMnSc1d1	oddělený
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozostřený	rozostřený	k2eAgInSc1d1	rozostřený
pruh	pruh	k1gInSc1	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
je	být	k5eAaImIp3nS	být
černě	černě	k6eAd1	černě
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
s	s	k7c7	s
různorodými	různorodý	k2eAgFnPc7d1	různorodá
skvrnkami	skvrnka	k1gFnPc7	skvrnka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
vyniká	vynikat	k5eAaImIp3nS	vynikat
delším	dlouhý	k2eAgInSc7d2	delší
bílým	bílý	k2eAgInSc7d1	bílý
až	až	k8xS	až
růžovým	růžový	k2eAgInSc7d1	růžový
páskem	pásek	k1gInSc7	pásek
<g/>
,	,	kIx,	,
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
zobáku	zobák	k1gInSc2	zobák
pak	pak	k6eAd1	pak
nažloutlým	nažloutlý	k2eAgMnPc3d1	nažloutlý
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
modro-šedě	modro-šeda	k1gFnSc3	modro-šeda
s	s	k7c7	s
nedefinovaným	definovaný	k2eNgInSc7d1	nedefinovaný
vzorem	vzor	k1gInSc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Odrostlejší	odrostlý	k2eAgMnPc1d2	odrostlejší
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
šedobílý	šedobílý	k2eAgInSc4d1	šedobílý
<g/>
,	,	kIx,	,
ošaceni	ošatit	k5eAaPmNgMnP	ošatit
již	již	k6eAd1	již
juvenilním	juvenilní	k2eAgInSc6d1	juvenilní
(	(	kIx(	(
<g/>
nepromokavým	promokavý	k2eNgMnSc7d1	nepromokavý
<g/>
)	)	kIx)	)
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
plného	plný	k2eAgNnSc2d1	plné
opeření	opeření	k1gNnSc2	opeření
dospělých	dospělí	k1gMnPc2	dospělí
se	se	k3xPyFc4	se
vybarví	vybarvit	k5eAaPmIp3nS	vybarvit
až	až	k6eAd1	až
ve	v	k7c6	v
věku	věk	k1gInSc2	věk
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
je	být	k5eAaImIp3nS	být
nejmenším	malý	k2eAgInSc7d3	nejmenší
druhem	druh	k1gInSc7	druh
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
;	;	kIx,	;
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
těla	tělo	k1gNnSc2	tělo
48	[number]	k4	48
až	až	k9	až
53	[number]	k4	53
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnPc1	hmotnost
2	[number]	k4	2
až	až	k9	až
2,5	[number]	k4	2,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nepatrně	patrně	k6eNd1	patrně
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
než	než	k8xS	než
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
Bergmannovu	Bergmannův	k2eAgNnSc3d1	Bergmannovo
pravidlu	pravidlo	k1gNnSc3	pravidlo
<g/>
;	;	kIx,	;
velikost	velikost	k1gFnSc1	velikost
zvířat	zvíře	k1gNnPc2	zvíře
téhož	týž	k3xTgInSc2	týž
druhu	druh	k1gInSc2	druh
nebo	nebo	k8xC	nebo
úzce	úzko	k6eAd1	úzko
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
se	s	k7c7	s
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
rovníků	rovník	k1gInPc2	rovník
k	k	k7c3	k
oběma	dva	k4xCgInPc3	dva
pólům	pól	k1gInPc3	pól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
výhradně	výhradně	k6eAd1	výhradně
rybí	rybí	k2eAgFnSc7d1	rybí
stravou	strava	k1gFnSc7	strava
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
pak	pak	k6eAd1	pak
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
sardinek	sardinka	k1gFnPc2	sardinka
a	a	k8xC	a
ančoviček	ančovička	k1gFnPc2	ančovička
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
10	[number]	k4	10
až	až	k9	až
180	[number]	k4	180
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gInSc2	jeho
jídelníčku	jídelníček	k1gInSc2	jídelníček
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
,	,	kIx,	,
měkkýšů	měkkýš	k1gMnPc2	měkkýš
nebo	nebo	k8xC	nebo
korýšů	korýš	k1gMnPc2	korýš
<g/>
.	.	kIx.	.
<g/>
Podniká	podnikat	k5eAaImIp3nS	podnikat
asi	asi	k9	asi
15	[number]	k4	15
až	až	k9	až
30	[number]	k4	30
metrové	metrový	k2eAgInPc1d1	metrový
ponory	ponor	k1gInPc1	ponor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
sekund	sekunda	k1gFnPc2	sekunda
(	(	kIx(	(
<g/>
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
však	však	k9	však
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
tří	tři	k4xCgInPc2	tři
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
začíná	začínat	k5eAaImIp3nS	začínat
lovit	lovit	k5eAaImF	lovit
už	už	k6eAd1	už
za	za	k7c2	za
rozbřesku	rozbřesk	k1gInSc2	rozbřesk
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Pakliže	pakliže	k8xS	pakliže
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc1	podmínka
zhoršeny	zhoršen	k2eAgFnPc1d1	zhoršena
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vlivem	vlivem	k7c2	vlivem
El	Ela	k1gFnPc2	Ela
Niñ	Niñ	k1gFnSc2	Niñ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
nakrmit	nakrmit	k5eAaPmF	nakrmit
svá	svůj	k3xOyFgNnPc4	svůj
mláďat	mládě	k1gNnPc2	mládě
patrně	patrně	k6eAd1	patrně
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
<g/>
Loví	lovit	k5eAaImIp3nS	lovit
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
o	o	k7c4	o
30	[number]	k4	30
až	až	k9	až
80	[number]	k4	80
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
tým	tým	k1gInSc4	tým
spolu	spolu	k6eAd1	spolu
nahánějí	nahánět	k5eAaImIp3nP	nahánět
hejnovité	hejnovitý	k2eAgInPc1d1	hejnovitý
druhy	druh	k1gInPc1	druh
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
zátokách	zátoka	k1gFnPc6	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Soustavně	soustavně	k6eAd1	soustavně
obeplouvají	obeplouvat	k5eAaImIp3nP	obeplouvat
vyhlédnuté	vyhlédnutý	k2eAgNnSc4d1	vyhlédnuté
hejno	hejno	k1gNnSc4	hejno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
smrskává	smrskávat	k5eAaImIp3nS	smrskávat
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
fázi	fáze	k1gFnSc6	fáze
vzniká	vznikat	k5eAaImIp3nS	vznikat
jakýsi	jakýsi	k3yIgInSc1	jakýsi
sloup	sloup	k1gInSc1	sloup
zmatených	zmatený	k2eAgFnPc2d1	zmatená
ryb	ryba	k1gFnPc2	ryba
krouživých	krouživý	k2eAgFnPc2d1	krouživá
kolem	kolem	k6eAd1	kolem
do	do	k7c2	do
kola	kolo	k1gNnSc2	kolo
<g/>
;	;	kIx,	;
shora	shora	k6eAd1	shora
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
tlačí	tlačit	k5eAaImIp3nS	tlačit
vodní	vodní	k2eAgFnSc1d1	vodní
hladina	hladina	k1gFnSc1	hladina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zdola	zdola	k6eAd1	zdola
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
číhá	číhat	k5eAaImIp3nS	číhat
několik	několik	k4yIc1	několik
hladových	hladový	k2eAgInPc2d1	hladový
zobáků	zobák	k1gInPc2	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
pak	pak	k6eAd1	pak
do	do	k7c2	do
středu	střed	k1gInSc2	střed
hejna	hejno	k1gNnSc2	hejno
vplouvají	vplouvat	k5eAaImIp3nP	vplouvat
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
snadnou	snadný	k2eAgFnSc4d1	snadná
kořist	kořist	k1gFnSc4	kořist
chytají	chytat	k5eAaImIp3nP	chytat
a	a	k8xC	a
polykají	polykat	k5eAaImIp3nP	polykat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vodního	vodní	k2eAgNnSc2d1	vodní
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
rozvíří	rozvířit	k5eAaPmIp3nS	rozvířit
navíc	navíc	k6eAd1	navíc
písek	písek	k1gInSc4	písek
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
kořist	kořist	k1gFnSc4	kořist
dezorientují	dezorientovat	k5eAaBmIp3nP	dezorientovat
úplně	úplně	k6eAd1	úplně
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
účelně	účelně	k6eAd1	účelně
tak	tak	k6eAd1	tak
činí	činit	k5eAaImIp3nS	činit
třeba	třeba	k6eAd1	třeba
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
delfínů	delfín	k1gMnPc2	delfín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přirození	přirozený	k2eAgMnPc1d1	přirozený
predátoři	predátor	k1gMnPc1	predátor
==	==	k?	==
</s>
</p>
<p>
<s>
Galapážské	galapážský	k2eAgInPc1d1	galapážský
ostrovy	ostrov	k1gInPc1	ostrov
skýtají	skýtat	k5eAaImIp3nP	skýtat
bohatou	bohatý	k2eAgFnSc4d1	bohatá
faunu	fauna	k1gFnSc4	fauna
a	a	k8xC	a
tučňáci	tučňák	k1gMnPc1	tučňák
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k6eAd1	mnoho
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Hadi	had	k1gMnPc1	had
a	a	k8xC	a
krabi	krab	k1gMnPc1	krab
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Graphus	Graphus	k1gInSc1	Graphus
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
vejci	vejce	k1gNnPc7	vejce
z	z	k7c2	z
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
již	již	k6eAd1	již
vylíhlá	vylíhlý	k2eAgNnPc4d1	vylíhlé
mláďata	mládě	k1gNnPc4	mládě
představují	představovat	k5eAaImIp3nP	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
racci	racek	k1gMnPc1	racek
nebo	nebo	k8xC	nebo
sovy	sova	k1gFnPc1	sova
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
invazivní	invazivní	k2eAgInPc1d1	invazivní
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
zatoulaní	zatoulaný	k2eAgMnPc1d1	zatoulaný
psi	pes	k1gMnPc1	pes
a	a	k8xC	a
kočky	kočka	k1gFnPc1	kočka
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
mláďata	mládě	k1gNnPc1	mládě
nebo	nebo	k8xC	nebo
dospělé	dospělý	k2eAgFnPc1d1	dospělá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
na	na	k7c4	na
snižování	snižování	k1gNnSc4	snižování
jejich	jejich	k3xOp3gInSc2	jejich
počtu	počet	k1gInSc2	počet
podílejí	podílet	k5eAaImIp3nP	podílet
především	především	k9	především
žraloci	žralok	k1gMnPc1	žralok
<g/>
,	,	kIx,	,
kosatky	kosatka	k1gFnPc1	kosatka
či	či	k8xC	či
lachtani	lachtan	k1gMnPc1	lachtan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Rozmnožovací	rozmnožovací	k2eAgFnSc2d1	rozmnožovací
aktivity	aktivita	k1gFnSc2	aktivita
tučňáka	tučňák	k1gMnSc2	tučňák
galapážského	galapážský	k2eAgMnSc2d1	galapážský
nejsou	být	k5eNaImIp3nP	být
fixně	fixně	k6eAd1	fixně
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
limitován	limitovat	k5eAaBmNgInS	limitovat
proměnlivou	proměnlivý	k2eAgFnSc7d1	proměnlivá
hojností	hojnost	k1gFnSc7	hojnost
potravy	potrava	k1gFnSc2	potrava
ani	ani	k8xC	ani
patrnější	patrný	k2eAgFnSc7d2	patrnější
změnou	změna	k1gFnSc7	změna
svého	svůj	k3xOyFgNnSc2	svůj
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
častokrát	častokrát	k6eAd1	častokrát
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
maximálně	maximálně	k6eAd1	maximálně
teplota	teplota	k1gFnSc1	teplota
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
vodách	voda	k1gFnPc6	voda
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
nachází	nacházet	k5eAaImIp3nS	nacházet
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
tyto	tento	k3xDgFnPc1	tento
skromné	skromný	k2eAgFnPc1d1	skromná
podmínky	podmínka	k1gFnPc1	podmínka
splněny	splnit	k5eAaPmNgFnP	splnit
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
a	a	k8xC	a
zahnízdí	zahnízdit	k5eAaPmIp3nS	zahnízdit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
dostaví	dostavit	k5eAaPmIp3nP	dostavit
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
obsadí	obsadit	k5eAaPmIp3nP	obsadit
zpravidla	zpravidla	k6eAd1	zpravidla
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgNnSc4	samý
hnízdo	hnízdo	k1gNnSc4	hnízdo
<g/>
,	,	kIx,	,
na	na	k7c6	na
jakém	jaký	k3yIgInSc6	jaký
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
připravují	připravovat	k5eAaImIp3nP	připravovat
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
skal	skála	k1gFnPc2	skála
nebo	nebo	k8xC	nebo
v	v	k7c6	v
lávových	lávový	k2eAgInPc6d1	lávový
tunelech	tunel	k1gInPc6	tunel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vystýlají	vystýlat	k5eAaImIp3nP	vystýlat
měkkým	měkký	k2eAgInSc7d1	měkký
podkladem	podklad	k1gInSc7	podklad
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
trávou	tráva	k1gFnSc7	tráva
<g/>
,	,	kIx,	,
větvemi	větev	k1gFnPc7	větev
nebo	nebo	k8xC	nebo
listím	listí	k1gNnSc7	listí
<g/>
.	.	kIx.	.
<g/>
Taktéž	Taktéž	k?	Taktéž
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
hnízda	hnízdo	k1gNnSc2	hnízdo
ze	z	k7c2	z
základu	základ	k1gInSc2	základ
guána	guáno	k1gNnSc2	guáno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ukrytou	ukrytý	k2eAgFnSc4d1	ukrytá
prohlubeň	prohlubeň	k1gFnSc4	prohlubeň
v	v	k7c6	v
zemině	zemina	k1gFnSc6	zemina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
o	o	k7c4	o
vyhloubenou	vyhloubený	k2eAgFnSc4d1	vyhloubená
noru	nora	k1gFnSc4	nora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
<g/>
Tučňáci	tučňák	k1gMnPc1	tučňák
galapážští	galapážský	k2eAgMnPc1d1	galapážský
tvoří	tvořit	k5eAaImIp3nP	tvořit
zpravidla	zpravidla	k6eAd1	zpravidla
monogamní	monogamní	k2eAgInPc1d1	monogamní
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
pářením	páření	k1gNnSc7	páření
tak	tak	k9	tak
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
krátkodobým	krátkodobý	k2eAgFnPc3d1	krátkodobá
námluvám	námluva	k1gFnPc3	námluva
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
o	o	k7c6	o
sebe	sebe	k3xPyFc4	sebe
partneři	partner	k1gMnPc1	partner
otírají	otírat	k5eAaImIp3nP	otírat
<g/>
,	,	kIx,	,
čistí	čistit	k5eAaImIp3nS	čistit
se	se	k3xPyFc4	se
a	a	k8xC	a
v	v	k7c6	v
konečné	konečný	k2eAgFnSc6d1	konečná
fázi	fáze	k1gFnSc6	fáze
společně	společně	k6eAd1	společně
troubí	troubit	k5eAaImIp3nP	troubit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
svazku	svazek	k1gInSc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
páření	páření	k1gNnSc6	páření
leží	ležet	k5eAaImIp3nS	ležet
samice	samice	k1gFnSc1	samice
na	na	k7c6	na
břichu	břich	k1gInSc6	břich
<g/>
,	,	kIx,	,
samec	samec	k1gInSc1	samec
ji	on	k3xPp3gFnSc4	on
vyskočí	vyskočit	k5eAaPmIp3nS	vyskočit
na	na	k7c4	na
záda	záda	k1gNnPc4	záda
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
přitisknout	přitisknout	k5eAaPmF	přitisknout
svou	svůj	k3xOyFgFnSc4	svůj
kloaku	kloaka	k1gFnSc4	kloaka
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
snese	snést	k5eAaPmIp3nS	snést
samice	samice	k1gFnSc1	samice
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgNnPc1	dva
vejce	vejce	k1gNnPc1	vejce
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
květnem	květen	k1gInSc7	květen
a	a	k8xC	a
červencem	červenec	k1gInSc7	červenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
38	[number]	k4	38
až	až	k9	až
42	[number]	k4	42
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
ochmýřená	ochmýřený	k2eAgNnPc1d1	ochmýřené
mláďata	mládě	k1gNnPc1	mládě
<g/>
,	,	kIx,	,
o	o	k7c6	o
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
60	[number]	k4	60
dnech	den	k1gInPc6	den
mají	mít	k5eAaImIp3nP	mít
mladiství	mladiství	k1gNnSc3	mladiství
juvenilní	juvenilní	k2eAgInSc4d1	juvenilní
šat	šat	k1gInSc4	šat
a	a	k8xC	a
kolonii	kolonie	k1gFnSc4	kolonie
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Schopni	schopen	k2eAgMnPc1d1	schopen
reprodukce	reprodukce	k1gFnSc2	reprodukce
budou	být	k5eAaImBp3nP	být
za	za	k7c4	za
poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
4	[number]	k4	4
až	až	k9	až
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
jsou	být	k5eAaImIp3nP	být
snižovány	snižovat	k5eAaImNgInP	snižovat
zapříčiněním	zapříčinění	k1gNnSc7	zapříčinění
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
především	především	k6eAd1	především
komerční	komerční	k2eAgInSc1d1	komerční
rybolov	rybolov	k1gInSc1	rybolov
<g/>
)	)	kIx)	)
a	a	k8xC	a
přirozených	přirozený	k2eAgMnPc2d1	přirozený
či	či	k8xC	či
invazivních	invazivní	k2eAgMnPc2d1	invazivní
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
následkem	následkem	k7c2	následkem
složitého	složitý	k2eAgInSc2d1	složitý
klimatického	klimatický	k2eAgInSc2d1	klimatický
jevu	jev	k1gInSc2	jev
El	Ela	k1gFnPc2	Ela
Niñ	Niñ	k1gFnSc2	Niñ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
studený	studený	k2eAgInSc1d1	studený
Humboldtův	Humboldtův	k2eAgInSc1d1	Humboldtův
proud	proud	k1gInSc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řádkách	řádka	k1gFnPc6	řádka
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
naruší	narušit	k5eAaPmIp3nS	narušit
ekologická	ekologický	k2eAgFnSc1d1	ekologická
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
,	,	kIx,	,
a	a	k8xC	a
tučňáci	tučňák	k1gMnPc1	tučňák
trpí	trpět	k5eAaImIp3nP	trpět
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
<g/>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
populace	populace	k1gFnSc1	populace
tučňáků	tučňák	k1gMnPc2	tučňák
galapážských	galapážský	k2eAgMnPc2d1	galapážský
podle	podle	k7c2	podle
veškerých	veškerý	k3xTgInPc2	veškerý
předpokladů	předpoklad	k1gInPc2	předpoklad
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
neexistují	existovat	k5eNaImIp3nP	existovat
záznamy	záznam	k1gInPc1	záznam
s	s	k7c7	s
přesnějšími	přesný	k2eAgNnPc7d2	přesnější
čísly	číslo	k1gNnPc7	číslo
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
odhadovalo	odhadovat	k5eAaImAgNnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
asi	asi	k9	asi
700	[number]	k4	700
až	až	k9	až
10	[number]	k4	10
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
1800	[number]	k4	1800
až	až	k9	až
5000	[number]	k4	5000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
Galapážských	galapážský	k2eAgInPc6d1	galapážský
ostrovech	ostrov	k1gInPc6	ostrov
zjevně	zjevně	k6eAd1	zjevně
obdobný	obdobný	k2eAgInSc1d1	obdobný
počet	počet	k1gInSc1	počet
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1000	[number]	k4	1000
až	až	k9	až
2000	[number]	k4	2000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VESELOVSKÝ	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
–	–	k?	–
Tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
10	[number]	k4	10
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Spheniscus	Spheniscus	k1gInSc1	Spheniscus
mendiculus	mendiculus	k1gInSc1	mendiculus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
www.birdlife.org	www.birdlife.org	k1gMnSc1	www.birdlife.org
</s>
</p>
<p>
<s>
www.birds.cornell.edu	www.birds.cornell.edu	k6eAd1	www.birds.cornell.edu
</s>
</p>
<p>
<s>
www.biolib.cz	www.biolib.cz	k1gMnSc1	www.biolib.cz
</s>
</p>
