<p>
<s>
V	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
městě	město	k1gNnSc6	město
Murmansk	Murmansk	k1gInSc4	Murmansk
je	být	k5eAaImIp3nS	být
provozována	provozován	k2eAgFnSc1d1	provozována
síť	síť	k1gFnSc1	síť
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejseverněji	severně	k6eAd3	severně
položenou	položený	k2eAgFnSc4d1	položená
trolejbusovou	trolejbusový	k2eAgFnSc4d1	trolejbusová
síť	síť	k1gFnSc4	síť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
společnost	společnost	k1gFnSc4	společnost
MP	MP	kA	MP
Elektrotransport	Elektrotransport	k1gInSc1	Elektrotransport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Murmanské	murmanský	k2eAgInPc1d1	murmanský
trolejbusy	trolejbus	k1gInPc1	trolejbus
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
projektové	projektový	k2eAgFnSc2d1	projektová
přípravy	příprava	k1gFnSc2	příprava
však	však	k9	však
začaly	začít	k5eAaPmAgFnP	začít
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
linek	linka	k1gFnPc2	linka
postupně	postupně	k6eAd1	postupně
stoupal	stoupat	k5eAaImAgInS	stoupat
až	až	k9	až
na	na	k7c4	na
8	[number]	k4	8
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
některé	některý	k3yIgFnPc1	některý
linky	linka	k1gFnPc1	linka
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
sloučeny	sloučen	k2eAgFnPc4d1	sloučena
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
na	na	k7c6	na
současných	současný	k2eAgInPc6d1	současný
pěti	pět	k4xCc6	pět
(	(	kIx(	(
<g/>
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
a	a	k8xC	a
10	[number]	k4	10
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
též	též	k9	též
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
a	a	k8xC	a
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusy	trolejbus	k1gInPc1	trolejbus
obsluhují	obsluhovat	k5eAaImIp3nP	obsluhovat
především	především	k6eAd1	především
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
velká	velký	k2eAgNnPc4d1	velké
panelová	panelový	k2eAgNnPc4d1	panelové
sídliště	sídliště	k1gNnPc4	sídliště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
105	[number]	k4	105
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
především	především	k9	především
typu	typ	k1gInSc2	typ
ZiU	ZiU	k1gFnSc2	ZiU
<g/>
.	.	kIx.	.
</s>
<s>
Servis	servis	k1gInSc1	servis
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
dvě	dva	k4xCgFnPc4	dva
vozovny	vozovna	k1gFnPc4	vozovna
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Karla	Karel	k1gMnSc2	Karel
Libknechta	Libknecht	k1gMnSc2	Libknecht
(	(	kIx(	(
<g/>
Vozovna	vozovna	k1gFnSc1	vozovna
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sverdlova	Sverdlův	k2eAgFnSc1d1	Sverdlova
(	(	kIx(	(
<g/>
Vozovna	vozovna	k1gFnSc1	vozovna
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
jednorázové	jednorázový	k2eAgFnSc2d1	jednorázová
jízdenky	jízdenka	k1gFnSc2	jízdenka
činí	činit	k5eAaImIp3nS	činit
9	[number]	k4	9
rublů	rubl	k1gInPc2	rubl
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
