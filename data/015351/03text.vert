<s>
Liekė	Liekė	k?
</s>
<s>
Liekė	Liekė	k?
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
17,5	17,5	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
182,4	182,4	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
0,30	0,30	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Hydrologické	hydrologický	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc4
</s>
<s>
10011760	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pramen	pramen	k1gInSc1
</s>
<s>
v	v	k7c6
lese	les	k1gInSc6
Gerdžių	Gerdžių	k1gMnSc1
miškas	miškas	k1gMnSc1
<g/>
,	,	kIx,
JZ	JZ	kA
od	od	k7c2
vsi	ves	k1gFnSc2
Jančiai	Jančia	k1gFnSc2
54	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
26,42	26,42	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
35,41	35,41	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Němen	Němen	k1gInSc1
2,5	2,5	k4
km	km	kA
na	na	k7c4
jihovýchod	jihovýchod	k1gInSc4
od	od	k7c2
vsi	ves	k1gFnSc2
Šė	Šė	k1gFnSc2
<g/>
,	,	kIx,
6	#num#	k4
km	km	kA
Z	Z	kA
od	od	k7c2
města	město	k1gNnSc2
Lekė	Lekė	k1gFnSc2
55	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
30,74	30,74	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
9,55	9,55	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Litva	Litva	k1gFnSc1
Litva	Litva	k1gFnSc1
(	(	kIx(
<g/>
Marijampolský	Marijampolský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Šakiai	Šakia	k1gFnSc2
a	a	k8xC
Kaunaský	Kaunaský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Kaunas	Kaunas	k1gInSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Baltské	baltský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Němen	Němen	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Liekė	Liekė	k?
je	být	k5eAaImIp3nS
říčka	říčka	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
řádu	řád	k1gInSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Litvě	Litva	k1gFnSc6
v	v	k7c6
okresech	okres	k1gInPc6
Šakiai	Šakiai	k1gNnSc2
a	a	k8xC
Kaunas	Kaunasa	k1gFnPc2
<g/>
,	,	kIx,
levý	levý	k2eAgInSc4d1
přítok	přítok	k1gInSc4
Němenu	Němen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
toku	tok	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Šakiai	Šakia	k1gFnSc2
<g/>
,	,	kIx,
jen	jen	k9
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
dolního	dolní	k2eAgInSc2d1
toku	tok	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
těmito	tento	k3xDgInPc7
okresy	okres	k1gInPc7
a	a	k8xC
ještě	ještě	k6eAd1
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
okresu	okres	k1gInSc2
Kaunas	Kaunasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
lese	les	k1gInSc6
Gerdžių	Gerdžių	k1gMnSc1
miškas	miškas	k1gMnSc1
<g/>
,	,	kIx,
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
vsi	ves	k1gFnSc2
Jančiai	Jančia	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klikatí	klikatit	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
celkovém	celkový	k2eAgInSc6d1
směru	směr	k1gInSc6
východoseverovýchodním	východoseverovýchodní	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vsi	ves	k1gFnSc2
Zizai	Ziza	k1gFnSc2
protéká	protékat	k5eAaImIp3nS
rybníkem	rybník	k1gInSc7
Zizų	Zizų	k1gFnSc2
tvenkinys	tvenkinys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
soutokem	soutok	k1gInSc7
s	s	k7c7
potokem	potok	k1gInSc7
Liekaitis	Liekaitis	k1gFnSc1
protéká	protékat	k5eAaImIp3nS
dvěma	dva	k4xCgInPc7
malými	malý	k2eAgInPc7d1
rybníčky	rybníček	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
řeky	řeka	k1gFnSc2
Němen	Němna	k1gFnPc2
se	se	k3xPyFc4
vlévá	vlévat	k5eAaImIp3nS
182,4	182,4	k4
km	km	kA
od	od	k7c2
jeho	jeho	k3xOp3gFnPc2
ústí	ústit	k5eAaImIp3nS
do	do	k7c2
Kuršského	Kuršský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
blízkosti	blízkost	k1gFnSc6
soutoku	soutok	k1gInSc2
s	s	k7c7
Němenem	Němen	k1gInSc7
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
hradiště	hradiště	k1gNnPc1
<g/>
:	:	kIx,
Šė	Šė	k1gFnSc1
piliakalnis	piliakalnis	k1gFnSc1
a	a	k8xC
Jadagonių	Jadagonių	k1gFnSc1
piliakalnis	piliakalnis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
spád	spád	k1gInSc1
je	být	k5eAaImIp3nS
2,67	2,67	k4
cm	cm	kA
<g/>
/	/	kIx~
<g/>
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šířka	šířka	k1gFnSc1
koryta	koryto	k1gNnSc2
je	být	k5eAaImIp3nS
3	#num#	k4
-	-	kIx~
6	#num#	k4
m.	m.	k?
Úsek	úsek	k1gInSc1
erozního	erozní	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
dolního	dolní	k2eAgInSc2d1
toku	tok	k1gInSc2
od	od	k7c2
Zizů	Ziz	k1gInPc2
až	až	k9
do	do	k7c2
ústí	ústí	k1gNnSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
geomorfologické	geomorfologický	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Liekė	Liekė	k1gFnPc2
geomorfologinis	geomorfologinis	k1gFnSc1
draustinis	draustinis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přítoky	přítok	k1gInPc1
</s>
<s>
Levé	levá	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
HydrologicképořadíŘekaDélka	HydrologicképořadíŘekaDélka	k1gMnSc1
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
<g/>
Povodí	povodí	k1gNnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
<g/>
Vzdálenostod	Vzdálenostod	k1gInSc1
ústí	ústí	k1gNnSc1
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
<g/>
Ústí	ústí	k1gNnSc1
kdeKoordináty	kdeKoordinát	k1gInPc7
ústí	ústí	k1gNnSc6
</s>
<s>
Ruzgys	Ruzgys	k1gInSc1
Lekė	Lekė	k1gNnSc1
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Buidupis	Buidupis	k1gFnSc1
Zizai	Ziza	k1gFnSc2
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Užliekis	Užliekis	k1gFnSc1
Jagadoniai	Jagadonia	k1gFnSc2
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Šė	Šė	k?
upelis	upelis	k1gInSc1
Jagadoniai	Jagadoniai	k1gNnSc1
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
57	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Pravé	pravá	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
HydrologicképořadíŘekaDélka	HydrologicképořadíŘekaDélka	k1gMnSc1
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
<g/>
Povodí	povodí	k1gNnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
Vzdálenostod	Vzdálenostod	k1gInSc1
ústí	ústí	k1gNnSc1
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
<g/>
Ústí	ústí	k1gNnSc1
kdeKoordináty	kdeKoordinát	k1gInPc7
ústí	ústí	k1gNnSc6
</s>
<s>
10011761	#num#	k4
<g/>
Liekaitis	Liekaitis	k1gFnSc1
<g/>
4,09	4,09	k4
<g/>
,39	,39	k4
<g/>
,7	,7	k4
<g/>
Lekė	Lekė	k1gNnPc2
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
22	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Vincentupis	Vincentupis	k1gFnSc1
Lekė	Lekė	k1gFnSc2
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
10011762	#num#	k4
<g/>
Molupis	Molupis	k1gFnSc1
<g/>
2,42	2,42	k4
<g/>
,06	,06	k4
<g/>
,2	,2	k4
<g/>
Lekė	Lekė	k1gNnPc2
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
10011763	#num#	k4
<g/>
Rū	Rū	k1gInSc4
neboli	neboli	k8xC
Rū	Rū	k1gFnSc4
<g/>
2,22	2,22	k4
<g/>
,65	,65	k4
<g/>
,4	,4	k4
<g/>
Zizai	Zizai	k1gNnPc2
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Aisučio	Aisučio	k1gNnSc1
upelis	upelis	k1gFnSc1
Zizai	Zizai	k1gNnSc1
<g/>
54	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
10011764	#num#	k4
<g/>
Palendris	Palendris	k1gFnSc1
<g/>
2,62	2,62	k4
<g/>
,10	,10	k4
<g/>
,8	,8	k4
<g/>
Jagadoniai	Jagadoniai	k1gNnPc2
<g/>
55	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Liekė	Liekė	k1gFnSc2
na	na	k7c6
litevské	litevský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Oficiální	oficiální	k2eAgMnSc1d1
klasifikátor	klasifikátor	k1gMnSc1
litevských	litevský	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
<g/>
↑	↑	k?
Algirdas	Algirdas	k1gInSc1
Rainys	Rainys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liekė	Liekė	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Visuotinė	Visuotinė	k1gFnSc1
lietuvių	lietuvių	k?
enciklopedija	enciklopedija	k1gFnSc1
(	(	kIx(
<g/>
Všeobecná	všeobecný	k2eAgFnSc1d1
litevská	litevský	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Díl	díl	k1gInSc1
XIII	XIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Leo-Magazyn	Leo-Magazyn	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
<g/>
:	:	kIx,
Mokslo	Mokslo	k1gMnSc1
ir	ir	k?
enciklopedijų	enciklopedijų	k?
leidybos	leidybos	k1gMnSc1
institutas	institutas	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
134	#num#	k4
(	(	kIx(
<g/>
litevsky	litevsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Lietuvos	Lietuvos	k1gInSc1
upė	upė	k1gInSc1
<g/>
:	:	kIx,
B.	B.	kA
Gailiušis	Gailiušis	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Jablonskis	Jablonskis	k1gInSc1
<g/>
,	,	kIx,
M.	M.	kA
Kovalenkovienė	Kovalenkovienė	k1gMnSc1
<g/>
,	,	kIx,
Kaunas	Kaunas	k1gMnSc1
<g/>
,	,	kIx,
Lietuvos	Lietuvos	k1gMnSc1
energetikos	energetikos	k1gMnSc1
institutas	institutas	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
419	#num#	k4
(	(	kIx(
<g/>
litevsky	litevsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Lietuvos	Lietuvos	k1gInSc1
autokelių	autokelių	k?
atlasas	atlasas	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
200	#num#	k4
000	#num#	k4
:	:	kIx,
Jā	Jā	k2eAgFnSc1d1
Sē	Sē	k1gFnSc1
Map	mapa	k1gFnPc2
publishers	publishersa	k1gFnPc2
Ltd	ltd	kA
<g/>
.	.	kIx.
:	:	kIx,
Rī	Rī	k1gFnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
-	-	kIx~
222	#num#	k4
p.	p.	k?
ISBN	ISBN	kA
978-9984-07-475-7	978-9984-07-475-7	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
