<p>
<s>
Řasenka	řasenka	k1gFnSc1	řasenka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
kosmetického	kosmetický	k2eAgInSc2d1	kosmetický
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
nanášení	nanášení	k1gNnSc4	nanášení
barvy	barva	k1gFnSc2	barva
na	na	k7c4	na
oční	oční	k2eAgFnPc4d1	oční
řasy	řasa	k1gFnPc4	řasa
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
jejich	jejich	k3xOp3gNnSc2	jejich
zvětšení	zvětšení	k1gNnSc2	zvětšení
a	a	k8xC	a
zviditelnění	zviditelnění	k1gNnSc2	zviditelnění
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
malý	malý	k2eAgInSc4d1	malý
kartáček	kartáček	k1gInSc4	kartáček
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
tyčce	tyčka	k1gFnSc6	tyčka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zasunuje	zasunovat	k5eAaImIp3nS	zasunovat
do	do	k7c2	do
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vytažení	vytažení	k1gNnSc6	vytažení
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
tato	tento	k3xDgFnSc1	tento
barva	barva	k1gFnSc1	barva
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
pohybem	pohyb	k1gInSc7	pohyb
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
rotace	rotace	k1gFnSc2	rotace
nanáší	nanášet	k5eAaImIp3nP	nanášet
na	na	k7c4	na
oční	oční	k2eAgFnPc4d1	oční
řasy	řasa	k1gFnPc4	řasa
–	–	k?	–
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
i	i	k8xC	i
dolní	dolní	k2eAgFnSc6d1	dolní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
optické	optický	k2eAgNnSc1d1	optické
prodloužení	prodloužení	k1gNnSc1	prodloužení
řas	řasa	k1gFnPc2	řasa
<g/>
,	,	kIx,	,
či	či	k8xC	či
jejich	jejich	k3xOp3gNnSc4	jejich
ztmavení	ztmavení	k1gNnSc4	ztmavení
a	a	k8xC	a
tedy	tedy	k9	tedy
zviditelnění	zviditelnění	k1gNnSc1	zviditelnění
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pozornost	pozornost	k1gFnSc4	pozornost
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
na	na	k7c4	na
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
řasenky	řasenka	k1gFnPc1	řasenka
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
barevné	barevný	k2eAgFnSc6d1	barevná
škále	škála	k1gFnSc6	škála
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
přírodní	přírodní	k2eAgFnSc3d1	přírodní
barvě	barva	k1gFnSc3	barva
řas	řasa	k1gFnPc2	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
nanášecích	nanášecí	k2eAgInPc2d1	nanášecí
kartáčků	kartáček	k1gInPc2	kartáček
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
větší	veliký	k2eAgInSc4d2	veliký
objem	objem	k1gInSc4	objem
nebo	nebo	k8xC	nebo
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Řasenka	řasenka	k1gFnSc1	řasenka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
doplněk	doplněk	k1gInSc1	doplněk
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
proceduru	procedura	k1gFnSc4	procedura
nalíčení	nalíčení	k1gNnSc2	nalíčení
očí	oko	k1gNnPc2	oko
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
očních	oční	k2eAgInPc2d1	oční
stínů	stín	k1gInPc2	stín
a	a	k8xC	a
celkové	celkový	k2eAgFnSc2d1	celková
úpravy	úprava	k1gFnSc2	úprava
make-upu	makep	k1gInSc2	make-up
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
skupiny	skupina	k1gFnPc1	skupina
řasenek	řasenka	k1gFnPc2	řasenka
–	–	k?	–
vodě	voda	k1gFnSc6	voda
odolné	odolný	k2eAgFnSc6d1	odolná
a	a	k8xC	a
vodě	voda	k1gFnSc6	voda
neodolné	odolný	k2eNgNnSc1d1	neodolné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
moderní	moderní	k2eAgFnSc1d1	moderní
řasenka	řasenka	k1gFnSc1	řasenka
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
Eugenem	Eugen	k1gMnSc7	Eugen
Rimmelem	Rimmel	k1gMnSc7	Rimmel
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
vžilo	vžít	k5eAaPmAgNnS	vžít
slovo	slovo	k1gNnSc1	slovo
rimmel	rimmela	k1gFnPc2	rimmela
pro	pro	k7c4	pro
název	název	k1gInSc4	název
řasenky	řasenka	k1gFnSc2	řasenka
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
portugalština	portugalština	k1gFnSc1	portugalština
<g/>
,	,	kIx,	,
turečtina	turečtina	k1gFnSc1	turečtina
<g/>
,	,	kIx,	,
rumunština	rumunština	k1gFnSc1	rumunština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Řasenka	řasenka	k1gFnSc1	řasenka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
