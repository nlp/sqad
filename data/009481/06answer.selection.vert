<s>
Řasenka	řasenka	k1gFnSc1	řasenka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
kosmetického	kosmetický	k2eAgInSc2d1	kosmetický
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
nanášení	nanášení	k1gNnSc4	nanášení
barvy	barva	k1gFnSc2	barva
na	na	k7c4	na
oční	oční	k2eAgFnPc4d1	oční
řasy	řasa	k1gFnPc4	řasa
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
jejich	jejich	k3xOp3gNnSc2	jejich
zvětšení	zvětšení	k1gNnSc2	zvětšení
a	a	k8xC	a
zviditelnění	zviditelnění	k1gNnSc2	zviditelnění
<g/>
.	.	kIx.	.
</s>
