<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
kosmetickému	kosmetický	k2eAgInSc3d1	kosmetický
výrobku	výrobek	k1gInSc3	výrobek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
nanášení	nanášení	k1gNnSc4	nanášení
barvy	barva	k1gFnSc2	barva
na	na	k7c4	na
oční	oční	k2eAgFnPc4d1	oční
řasy	řasa	k1gFnPc4	řasa
<g/>
?	?	kIx.	?
</s>
