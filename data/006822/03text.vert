<s>
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
Jevany	Jevana	k1gFnSc2	Jevana
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
skladatel	skladatel	k1gMnSc1	skladatel
filmové	filmový	k2eAgFnSc2d1	filmová
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc2d1	televizní
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc2d1	populární
a	a	k8xC	a
muzikálové	muzikálový	k2eAgFnSc2d1	muzikálová
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Gottem	Gott	k1gMnSc7	Gott
a	a	k8xC	a
s	s	k7c7	s
textařem	textař	k1gMnSc7	textař
Jiřím	Jiří	k1gMnSc7	Jiří
Štaidlem	Štaidlo	k1gNnSc7	Štaidlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgInS	učit
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
studium	studium	k1gNnSc4	studium
stomatologie	stomatologie	k1gFnSc2	stomatologie
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
lékařství	lékařství	k1gNnSc2	lékařství
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
založil	založit	k5eAaPmAgMnS	založit
bigbeatovou	bigbeatový	k2eAgFnSc4d1	bigbeatová
skupinu	skupina	k1gFnSc4	skupina
Mefisto	Mefisto	k1gMnSc1	Mefisto
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
Laterně	laterna	k1gFnSc6	laterna
magice	magika	k1gFnSc6	magika
a	a	k8xC	a
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
měla	mít	k5eAaImAgFnS	mít
stálé	stálý	k2eAgNnSc4d1	stálé
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Rokoko	rokoko	k1gNnSc4	rokoko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
dostal	dostat	k5eAaPmAgMnS	dostat
příležitost	příležitost	k1gFnSc4	příležitost
skládat	skládat	k5eAaImF	skládat
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
generaci	generace	k1gFnSc4	generace
zpěváků	zpěvák	k1gMnPc2	zpěvák
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Neckářem	Neckář	k1gMnSc7	Neckář
<g/>
,	,	kIx,	,
Martou	Marta	k1gFnSc7	Marta
Kubišovou	Kubišová	k1gFnSc7	Kubišová
nebo	nebo	k8xC	nebo
Helenou	Helena	k1gFnSc7	Helena
Vondráčkovou	Vondráčková	k1gFnSc7	Vondráčková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Hanou	Hana	k1gFnSc7	Hana
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Vendulou	Vendula	k1gFnSc7	Vendula
Svobodovou	Svobodová	k1gFnSc7	Svobodová
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
často	často	k6eAd1	často
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Jiřím	Jiří	k1gMnSc7	Jiří
Svobodou	Svoboda	k1gMnSc7	Svoboda
<g/>
,	,	kIx,	,
též	též	k9	též
skladatelem	skladatel	k1gMnSc7	skladatel
především	především	k9	především
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
filmům	film	k1gInPc3	film
a	a	k8xC	a
televizním	televizní	k2eAgFnPc3d1	televizní
inscenacím	inscenace	k1gFnPc3	inscenace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Karlu	Karel	k1gMnSc6	Karel
Svobodovi	Svoboda	k1gMnSc6	Svoboda
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
dokument	dokument	k1gInSc4	dokument
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
cyklu	cyklus	k1gInSc2	cyklus
GEN	gen	kA	gen
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
pohledem	pohled	k1gInSc7	pohled
Roberta	Robert	k1gMnSc2	Robert
Sedláčka	Sedláček	k1gMnSc2	Sedláček
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
zastřelený	zastřelený	k2eAgInSc1d1	zastřelený
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
své	svůj	k3xOyFgFnSc2	svůj
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
Jevanech	Jevan	k1gInPc6	Jevan
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc2	policie
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
v	v	k7c6	v
Aldašíně	Aldašína	k1gFnSc6	Aldašína
východně	východně	k6eAd1	východně
od	od	k7c2	od
Jevan	Jevany	k1gInPc2	Jevany
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
Hana	Hana	k1gFnSc1	Hana
Svobodová	Svobodová	k1gFnSc1	Svobodová
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Bohatová	Bohatová	k1gFnSc1	Bohatová
<g/>
)	)	kIx)	)
–	–	k?	–
manželé	manžel	k1gMnPc1	manžel
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
nádorové	nádorový	k2eAgNnSc4d1	nádorové
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
)	)	kIx)	)
Jana	Jana	k1gFnSc1	Jana
Wallace-Svobodová	Wallace-Svobodový	k2eAgFnSc1d1	Wallace-Svobodový
–	–	k?	–
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Clark	Clark	k1gInSc4	Clark
Wallace	Wallace	k1gFnSc2	Wallace
Michelle	Michell	k1gMnSc2	Michell
Christophe	Christoph	k1gMnSc2	Christoph
Petr	Petr	k1gMnSc1	Petr
Svoboda	Svoboda	k1gMnSc1	Svoboda
–	–	k?	–
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Lucia	Lucia	k1gFnSc1	Lucia
Gažiová	Gažiový	k2eAgFnSc1d1	Gažiová
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Vendula	Vendula	k1gFnSc1	Vendula
Pizingerová	Pizingerová	k1gFnSc1	Pizingerová
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Svobodová	Svobodová	k1gFnSc1	Svobodová
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Horová	Horová	k1gFnSc1	Horová
<g/>
)	)	kIx)	)
–	–	k?	–
manželé	manžel	k1gMnPc1	manžel
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
Klára	Klára	k1gFnSc1	Klára
Svobodová	Svobodová	k1gFnSc1	Svobodová
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1996	[number]	k4	1996
–	–	k?	–
†	†	k?	†
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
vyšla	vyjít	k5eAaPmAgFnS	vyjít
první	první	k4xOgFnSc1	první
gramofonová	gramofonový	k2eAgFnSc1d1	gramofonová
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
skladbou	skladba	k1gFnSc7	skladba
Slunce	slunce	k1gNnSc2	slunce
za	za	k7c7	za
oknem	okno	k1gNnSc7	okno
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
Jaromír	Jaromír	k1gMnSc1	Jaromír
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
rozjela	rozjet	k5eAaPmAgFnS	rozjet
i	i	k9	i
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
instrumentálními	instrumentální	k2eAgFnPc7d1	instrumentální
skladbami	skladba	k1gFnPc7	skladba
Návrat	návrat	k1gInSc1	návrat
Gemini	Gemin	k1gMnPc1	Gemin
a	a	k8xC	a
Start	start	k1gInSc1	start
Gemini	Gemin	k1gMnPc1	Gemin
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
píseň	píseň	k1gFnSc4	píseň
Vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
soutěžila	soutěžit	k5eAaImAgFnS	soutěžit
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
brazilském	brazilský	k2eAgMnSc6d1	brazilský
Rio	Rio	k1gMnSc6	Rio
de	de	k?	de
Janeiru	Janeir	k1gMnSc3	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
navázal	navázat	k5eAaPmAgMnS	navázat
Svoboda	Svoboda	k1gMnSc1	Svoboda
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Gottem	Gott	k1gMnSc7	Gott
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
repertoáru	repertoár	k1gInSc6	repertoár
asi	asi	k9	asi
80	[number]	k4	80
jeho	jeho	k3xOp3gFnPc2	jeho
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
byli	být	k5eAaImAgMnP	být
interprety	interpret	k1gMnPc4	interpret
jeho	jeho	k3xOp3gFnPc2	jeho
písní	píseň	k1gFnPc2	píseň
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Pilarová	Pilarová	k1gFnSc1	Pilarová
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
,	,	kIx,	,
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
,	,	kIx,	,
Lešek	Lešek	k1gMnSc1	Lešek
Semelka	Semelka	k1gMnSc1	Semelka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Schelinger	Schelinger	k1gMnSc1	Schelinger
nebo	nebo	k8xC	nebo
Jana	Jana	k1gFnSc1	Jana
Kratochvílová	Kratochvílová	k1gFnSc1	Kratochvílová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
pak	pak	k6eAd1	pak
přibyli	přibýt	k5eAaPmAgMnP	přibýt
Michal	Michal	k1gMnSc1	Michal
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
Iveta	Iveta	k1gFnSc1	Iveta
Bartošová	Bartošová	k1gFnSc1	Bartošová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Sepéši	Sepéše	k1gFnSc4	Sepéše
<g/>
,	,	kIx,	,
Marcela	Marcela	k1gFnSc1	Marcela
Březinová	Březinová	k1gFnSc1	Březinová
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Anna	Anna	k1gFnSc1	Anna
Rusticano	Rusticana	k1gFnSc5	Rusticana
<g/>
,	,	kIx,	,
Marcela	Marcela	k1gFnSc1	Marcela
Holanová	Holanová	k1gFnSc1	Holanová
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Janů	Janů	k1gMnSc1	Janů
a	a	k8xC	a
Dalibor	Dalibor	k1gMnSc1	Dalibor
Janda	Janda	k1gMnSc1	Janda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgMnS	udržet
v	v	k7c6	v
branži	branže	k1gFnSc6	branže
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
produkováním	produkování	k1gNnSc7	produkování
obchodně	obchodně	k6eAd1	obchodně
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
CD	CD	kA	CD
Daniela	Daniel	k1gMnSc2	Daniel
Hůlky	Hůlka	k1gMnSc2	Hůlka
<g/>
,	,	kIx,	,
Leony	Leona	k1gFnSc2	Leona
Machálkové	Machálková	k1gFnSc2	Machálková
nebo	nebo	k8xC	nebo
Petra	Petr	k1gMnSc2	Petr
Koláře	Kolář	k1gMnSc2	Kolář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
muzikál	muzikál	k1gInSc4	muzikál
Dracula	Draculum	k1gNnSc2	Draculum
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
ve	v	k7c6	v
veliké	veliký	k2eAgFnSc6d1	veliká
intenzitě	intenzita	k1gFnSc6	intenzita
filmové	filmový	k2eAgFnSc3d1	filmová
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
dvorním	dvorní	k2eAgMnSc7d1	dvorní
skladatelem	skladatel	k1gMnSc7	skladatel
režiséra	režisér	k1gMnSc2	režisér
Václava	Václav	k1gMnSc2	Václav
Vorlíčka	Vorlíček	k1gMnSc2	Vorlíček
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
filmem	film	k1gInSc7	film
Smrt	smrt	k1gFnSc1	smrt
si	se	k3xPyFc3	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
přes	přes	k7c4	přes
proslulou	proslulý	k2eAgFnSc4d1	proslulá
filmovou	filmový	k2eAgFnSc4d1	filmová
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
pohádkovému	pohádkový	k2eAgInSc3d1	pohádkový
filmu	film	k1gInSc3	film
Tři	tři	k4xCgMnPc4	tři
oříšky	oříšek	k1gMnPc4	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
až	až	k9	až
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
Létající	létající	k2eAgMnSc1d1	létající
Čestmír	Čestmír	k1gMnSc1	Čestmír
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
napsal	napsat	k5eAaBmAgMnS	napsat
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
seriálu	seriál	k1gInSc6	seriál
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
i	i	k8xC	i
znělku	znělka	k1gFnSc4	znělka
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
popularitu	popularita	k1gFnSc4	popularita
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
skladatelů	skladatel	k1gMnPc2	skladatel
pracoval	pracovat	k5eAaImAgMnS	pracovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
tzv.	tzv.	kA	tzv.
Železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
seriálech	seriál	k1gInPc6	seriál
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
televizí	televize	k1gFnSc7	televize
ZDF	ZDF	kA	ZDF
<g/>
.	.	kIx.	.
</s>
<s>
Složil	Složil	k1gMnSc1	Složil
mnoho	mnoho	k4c4	mnoho
písňových	písňový	k2eAgInPc2d1	písňový
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiná	k1gFnPc7	jiná
Lady	lady	k1gFnSc1	lady
Karneval	karneval	k1gInSc1	karneval
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
zřejmě	zřejmě	k6eAd1	zřejmě
nejproslulejší	proslulý	k2eAgFnSc4d3	nejproslulejší
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
později	pozdě	k6eAd2	pozdě
převzali	převzít	k5eAaPmAgMnP	převzít
interpreti	interpret	k1gMnPc1	interpret
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
nebo	nebo	k8xC	nebo
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stín	stín	k1gInSc1	stín
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
,	,	kIx,	,
Čau	čau	k0	čau
lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
Ještě	ještě	k9	ještě
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tě	ty	k3xPp2nSc4	ty
lásko	láska	k1gFnSc5	láska
mám	mít	k5eAaImIp1nS	mít
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
podkreslovala	podkreslovat	k5eAaImAgFnS	podkreslovat
i	i	k8xC	i
veškeré	veškerý	k3xTgFnPc1	veškerý
znělky	znělka	k1gFnPc1	znělka
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
Smrt	smrt	k1gFnSc1	smrt
si	se	k3xPyFc3	se
vybírá	vybírat	k5eAaImIp3nS	vybírat
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
Tři	tři	k4xCgMnPc4	tři
oříšky	oříšek	k1gMnPc4	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
Noc	noc	k1gFnSc1	noc
klavíristy	klavírista	k1gMnSc2	klavírista
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
Pinocchio	Pinocchio	k6eAd1	Pinocchio
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
budí	budit	k5eAaImIp3nS	budit
princezny	princezna	k1gFnSc2	princezna
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
Což	což	k9	což
takhle	takhle	k6eAd1	takhle
dát	dát	k5eAaPmF	dát
si	se	k3xPyFc3	se
<g />
.	.	kIx.	.
</s>
<s>
špenát	špenát	k1gInSc4	špenát
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
Zítra	zítra	k6eAd1	zítra
vstanu	vstát	k5eAaPmIp1nS	vstát
a	a	k8xC	a
opařím	opařit	k5eAaPmIp1nS	opařit
se	se	k3xPyFc4	se
čajem	čaj	k1gInSc7	čaj
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
Smrt	smrt	k1gFnSc4	smrt
stopařek	stopařka	k1gFnPc2	stopařka
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
Nils	Nilsa	k1gFnPc2	Nilsa
Holgersson	Holgerssona	k1gFnPc2	Holgerssona
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
Kopretiny	kopretina	k1gFnPc4	kopretina
pro	pro	k7c4	pro
zámeckou	zámecký	k2eAgFnSc4d1	zámecká
paní	paní	k1gFnSc4	paní
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Spievaj	Spievaj	k1gFnSc4	Spievaj
kovboj	kovboj	k1gMnSc1	kovboj
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Sůl	sůl	k1gFnSc4	sůl
nad	nad	k7c4	nad
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Tao	Tao	k1gFnPc2	Tao
Tao	Tao	k1gFnPc2	Tao
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g />
.	.	kIx.	.
</s>
<s>
Létající	létající	k2eAgMnSc1d1	létající
Čestmír	Čestmír	k1gMnSc1	Čestmír
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Big	Big	k1gMnSc1	Big
Man	Man	k1gMnSc1	Man
<g/>
/	/	kIx~	/
<g/>
Jack	Jack	k1gMnSc1	Jack
Clementi	Clement	k1gMnPc1	Clement
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
/	/	kIx~	/
<g/>
1988	[number]	k4	1988
Cirkus	cirkus	k1gInSc1	cirkus
Humberto	Humberta	k1gFnSc5	Humberta
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Druhý	druhý	k4xOgInSc1	druhý
dech	dech	k1gInSc1	dech
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
kriminalistiky	kriminalistika	k1gFnSc2	kriminalistika
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Kačenka	Kačenka	k1gFnSc1	Kačenka
a	a	k8xC	a
zase	zase	k9	zase
ta	ten	k3xDgNnPc4	ten
strašidla	strašidlo	k1gNnPc4	strašidlo
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Flash	Flasha	k1gFnPc2	Flasha
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Und	Und	k1gFnSc1	Und
keiner	keinra	k1gFnPc2	keinra
weint	weinta	k1gFnPc2	weinta
mir	mir	k1gInSc1	mir
<g />
.	.	kIx.	.
</s>
<s>
nach	nach	k1gInSc1	nach
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Milenci	milenec	k1gMnPc7	milenec
a	a	k8xC	a
vrazi	vrah	k1gMnPc1	vrah
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Rodinná	rodinný	k2eAgNnPc4d1	rodinné
pouta	pouto	k1gNnPc4	pouto
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Dracula	Draculum	k1gNnSc2	Draculum
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Borovec	Borovec	k1gMnSc1	Borovec
<g/>
)	)	kIx)	)
Monte	Mont	k1gMnSc5	Mont
Cristo	Crista	k1gMnSc5	Crista
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Borovec	Borovec	k1gMnSc1	Borovec
<g/>
)	)	kIx)	)
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
Karlín	Karlín	k1gInSc1	Karlín
<g/>
)	)	kIx)	)
Golem	Golem	k1gMnSc1	Golem
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Karla	Karel	k1gMnSc2	Karel
Svobody	Svoboda	k1gMnSc2	Svoboda
Osoba	osoba	k1gFnSc1	osoba
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
Karel-Svoboda	Karel-Svoboda	k1gMnSc1	Karel-Svoboda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
prezentace	prezentace	k1gFnSc2	prezentace
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
-	-	kIx~	-
články	článek	k1gInPc1	článek
–	–	k?	–
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
o	o	k7c6	o
Karlovi	Karel	k1gMnSc3	Karel
Svobodovi	Svoboda	k1gMnSc3	Svoboda
napsalo	napsat	k5eAaBmAgNnS	napsat
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
Talent	talent	k1gInSc1	talent
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Mefista	Mefisto	k1gMnSc2	Mefisto
–	–	k?	–
nekrolog	nekrolog	k1gInSc1	nekrolog
J.	J.	kA	J.
Rejžka	Rejžka	k1gFnSc1	Rejžka
na	na	k7c4	na
Lidovky	Lidovky	k1gFnPc4	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
