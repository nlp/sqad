<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1890	[number]	k4	1890
Praha-Staré	Praha-Starý	k2eAgNnSc4d1	Praha-Staré
Město	město	k1gNnSc4	město
–	–	k?	–
27.	[number]	k4	27.
března	březen	k1gInSc2	březen
1967	[number]	k4	1967
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
fyzikální	fyzikální	k2eAgMnSc1d1	fyzikální
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
polarografie	polarografie	k1gFnSc2	polarografie
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
.	.	kIx.
</s>
