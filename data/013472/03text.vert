<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
Logo	logo	k1gNnSc4
Hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
JAMU	JAMU	k1gFnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
JAMU	JAMU	k1gFnSc1
Rok	rok	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
1947	#num#	k4
Typ	typ	k1gInSc1
školy	škola	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vedení	vedení	k1gNnSc1
Rektor	rektor	k1gMnSc1
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Oslzlý	oslzlý	k2eAgMnSc1d1
Kvestor	kvestor	k1gMnSc1
</s>
<s>
JUDr.	JUDr.	kA
Lenka	Lenka	k1gFnSc1
Valová	Valová	k1gFnSc1
Prorektor	prorektor	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
vnější	vnější	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
</s>
<s>
doc.	doc.	kA
MgA.	MgA.	k1gMnSc1
Vít	Vít	k1gMnSc1
Spilka	Spilka	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
strategii	strategie	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
MgA.	MgA.	k1gMnSc1
Ivo	Ivo	k1gMnSc1
Medek	Medek	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
tvůrčí	tvůrčí	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
</s>
<s>
doc.	doc.	kA
MgA.	MgA.	k1gMnSc1
Marek	Marek	k1gMnSc1
Hlavica	Hlavica	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
a	a	k8xC
kvalitu	kvalita	k1gFnSc4
</s>
<s>
prof.	prof.	kA
PhDr.	PhDr.	kA
Silva	Silva	k1gFnSc1
Macková	Macková	k1gFnSc1
Předseda	předseda	k1gMnSc1
akademického	akademický	k2eAgInSc2d1
senátu	senát	k1gInSc2
</s>
<s>
doc.	doc.	kA
Mgr.	Mgr.	kA
Roman	Roman	k1gMnSc1
Novozámský	Novozámský	k2eAgMnSc1d1
Počty	počet	k1gInPc1
akademiků	akademik	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
bakalářských	bakalářský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
410	#num#	k4
Počet	počet	k1gInSc4
magisterských	magisterský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
249	#num#	k4
Počet	počet	k1gInSc4
doktorandů	doktorand	k1gMnPc2
</s>
<s>
61	#num#	k4
Počet	počet	k1gInSc4
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
</s>
<s>
211	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Počet	počet	k1gInSc1
fakult	fakulta	k1gFnPc2
</s>
<s>
2	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Brno	Brno	k1gNnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Beethovenova	Beethovenův	k2eAgFnSc1d1
650	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
602	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
37,14	37,14	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
http://www.jamu.cz	http://www.jamu.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
(	(	kIx(
<g/>
JAMU	JAMU	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
veřejná	veřejný	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
s	s	k7c7
uměleckým	umělecký	k2eAgNnSc7d1
zaměřením	zaměření	k1gNnSc7
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
rektorát	rektorát	k1gInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
Beethovenově	Beethovenův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
650	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
a	a	k8xC
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
dvou	dva	k4xCgFnPc2
fakult	fakulta	k1gFnPc2
<g/>
:	:	kIx,
Hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
sídlí	sídlet	k5eAaImIp3nS
na	na	k7c6
Komenského	Komenského	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
609	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
pak	pak	k6eAd1
v	v	k7c6
Mozartově	Mozartův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
647	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
JAMU	jam	k1gInSc3
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1947	#num#	k4
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
její	její	k3xOp3gInSc1
vznik	vznik	k1gInSc1
byl	být	k5eAaImAgInS
inspirován	inspirovat	k5eAaBmNgInS
postavou	postava	k1gFnSc7
všestranného	všestranný	k2eAgMnSc2d1
umělce	umělec	k1gMnSc2
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
již	již	k6eAd1
za	za	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c6
zřízení	zřízení	k1gNnSc6
hudební	hudební	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
však	však	k9
spolu	spolu	k6eAd1
s	s	k7c7
Vilémem	Vilém	k1gMnSc7
Kurzem	Kurz	k1gMnSc7
profesorem	profesor	k1gMnSc7
mistrovské	mistrovský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Pražské	pražský	k2eAgFnSc2d1
konzervatoře	konzervatoř	k1gFnSc2
s	s	k7c7
přidělením	přidělení	k1gNnSc7
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tuto	tento	k3xDgFnSc4
školu	škola	k1gFnSc4
JAMU	jam	k1gInSc2
navázala	navázat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
hudebně	hudebně	k6eAd1
i	i	k8xC
divadelně	divadelně	k6eAd1
orientovaných	orientovaný	k2eAgInPc6d1
oborech	obor	k1gInPc6
na	na	k7c4
svoji	svůj	k3xOyFgFnSc4
uměleckou	umělecký	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
připravuje	připravovat	k5eAaImIp3nS
na	na	k7c4
500	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
Hudební	hudební	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
JAMU	jam	k1gInSc2
na	na	k7c6
Komenského	Komenského	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
je	být	k5eAaImIp3nS
dílem	dílo	k1gNnSc7
autorů	autor	k1gMnPc2
vídeňské	vídeňský	k2eAgFnSc2d1
opery	opera	k1gFnSc2
Eduarda	Eduard	k1gMnSc2
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Nülla	Nüll	k1gMnSc2
a	a	k8xC
Augusta	August	k1gMnSc2
Siccardsburga	Siccardsburg	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
její	její	k3xOp3gInSc4
návrh	návrh	k1gInSc4
byla	být	k5eAaImAgFnS
uspořádána	uspořádat	k5eAaPmNgFnS
pravděpodobně	pravděpodobně	k6eAd1
první	první	k4xOgFnSc1
architektonická	architektonický	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
1860	#num#	k4
jako	jako	k8xC,k8xS
na	na	k7c4
budovu	budova	k1gFnSc4
německého	německý	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
čtyř	čtyři	k4xCgFnPc2
karyatid	karyatida	k1gFnPc2
u	u	k7c2
vstupu	vstup	k1gInSc2
do	do	k7c2
budovy	budova	k1gFnSc2
je	být	k5eAaImIp3nS
sochař	sochař	k1gMnSc1
Josef	Josef	k1gMnSc1
Břenek	Břenek	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
studentům	student	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
tehdy	tehdy	k6eAd1
navštěvovali	navštěvovat	k5eAaImAgMnP
tuto	tento	k3xDgFnSc4
budovu	budova	k1gFnSc4
gymnázia	gymnázium	k1gNnSc2
patřili	patřit	k5eAaImAgMnP
T.	T.	kA
G.	G.	kA
Masaryk	Masaryk	k1gMnSc1
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
Loos	Loos	k1gInSc1
a	a	k8xC
Alfons	Alfons	k1gMnSc1
Mucha	Mucha	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
JAMU	jam	k1gInSc3
tvoří	tvořit	k5eAaImIp3nS
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
a	a	k8xC
Hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
má	mít	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
činoherní	činoherní	k2eAgNnSc1d1
Studio	studio	k1gNnSc1
Marta	Mars	k1gMnSc2
a	a	k8xC
hudebně-dramatickou	hudebně-dramatický	k2eAgFnSc4d1
laboratoř	laboratoř	k1gFnSc4
Divadlo	divadlo	k1gNnSc1
na	na	k7c4
Orlí	orlí	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
školu	škola	k1gFnSc4
funguje	fungovat	k5eAaImIp3nS
informační	informační	k2eAgNnSc1d1
<g/>
,	,	kIx,
výukové	výukový	k2eAgNnSc1d1
a	a	k8xC
ubytovací	ubytovací	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Astorka	Astorka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Aktivity	aktivita	k1gFnPc1
školy	škola	k1gFnSc2
</s>
<s>
Škola	škola	k1gFnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
pořádá	pořádat	k5eAaImIp3nS
významné	významný	k2eAgFnPc4d1
kulturní	kulturní	k2eAgFnPc4d1
akce	akce	k1gFnPc4
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInPc1d1
mistrovské	mistrovský	k2eAgInPc1d1
interpretační	interpretační	k2eAgInPc1d1
kurzy	kurz	k1gInPc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1
setkání	setkání	k1gNnSc1
kontrabasistů	kontrabasista	k1gMnPc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
festival	festival	k1gInSc1
divadelních	divadelní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
SETKÁNÍ	setkání	k1gNnSc2
<g/>
/	/	kIx~
<g/>
ENCOUNTER	ENCOUNTER	kA
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Festival	festival	k1gInSc1
dramatické	dramatický	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
Sítko	sítko	k1gNnSc1
</s>
<s>
The	The	k?
International	International	k1gMnSc1
Seminar	Seminar	k1gMnSc1
of	of	k?
Doctoral	Doctoral	k1gMnSc1
Studies	Studies	k1gMnSc1
of	of	k?
Theatre	Theatr	k1gInSc5
Schools	Schoolsa	k1gFnPc2
</s>
<s>
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Studijní	studijní	k2eAgInPc1d1
obory	obor	k1gInPc1
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Studijní	studijní	k2eAgInSc4d1
program	program	k1gInSc4
dramatická	dramatický	k2eAgNnPc4d1
umění	umění	k1gNnSc4
nabízí	nabízet	k5eAaImIp3nS
obory	obora	k1gFnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
audiovizuální	audiovizuální	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
a	a	k8xC
divadlo	divadlo	k1gNnSc1
</s>
<s>
výchovná	výchovný	k2eAgFnSc1d1
dramatika	dramatika	k1gFnSc1
pro	pro	k7c4
Neslyšící	slyšící	k2eNgInSc4d1
</s>
<s>
divadelní	divadelní	k2eAgNnSc1d1
manažerství	manažerství	k1gNnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
jevištní	jevištní	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
činoherní	činoherní	k2eAgFnSc1d1
režie	režie	k1gFnSc1
</s>
<s>
divadelní	divadelní	k2eAgFnSc1d1
dramaturgie	dramaturgie	k1gFnSc1
</s>
<s>
dramatická	dramatický	k2eAgFnSc1d1
výchova	výchova	k1gFnSc1
</s>
<s>
rozhlasová	rozhlasový	k2eAgFnSc1d1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
dramaturgie	dramaturgie	k1gFnSc1
a	a	k8xC
scenáristika	scenáristika	k1gFnSc1
</s>
<s>
fyzické	fyzický	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
činoherní	činoherní	k2eAgNnSc1d1
herectví	herectví	k1gNnSc1
</s>
<s>
muzikálové	muzikálový	k2eAgNnSc1d1
herectví	herectví	k1gNnSc1
</s>
<s>
scénografie	scénografie	k1gFnSc1
</s>
<s>
V	v	k7c6
programu	program	k1gInSc6
taneční	taneční	k2eAgNnSc1d1
umění	umění	k1gNnSc1
obor	obor	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
taneční	taneční	k2eAgFnSc1d1
pedagogika	pedagogika	k1gFnSc1
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
V	v	k7c6
programu	program	k1gInSc6
hudební	hudební	k2eAgNnSc4d1
umění	umění	k1gNnSc4
nabízí	nabízet	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
studijní	studijní	k2eAgFnSc2d1
obory	obora	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
duchovní	duchovní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
</s>
<s>
dirigování	dirigování	k1gNnSc1
orchestru	orchestr	k1gInSc2
</s>
<s>
dirigování	dirigování	k1gNnSc1
sboru	sbor	k1gInSc2
</s>
<s>
kompozice	kompozice	k1gFnSc1
</s>
<s>
operní	operní	k2eAgFnSc1d1
režie	režie	k1gFnSc1
</s>
<s>
zpěv	zpěv	k1gInSc1
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
klavír	klavír	k1gInSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
varhany	varhany	k1gFnPc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
cembalo	cembalo	k1gNnSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
housle	housle	k1gFnPc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
violu	viola	k1gFnSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
violoncello	violoncello	k1gNnSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
kontrabas	kontrabas	k1gInSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
flétnu	flétna	k1gFnSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
hoboj	hoboj	k1gFnSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
klarinet	klarinet	k1gInSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
fagot	fagot	k1gInSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
lesní	lesní	k2eAgInSc4d1
roh	roh	k1gInSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
trubku	trubka	k1gFnSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
pozoun	pozoun	k1gInSc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
bicí	bicí	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
</s>
<s>
hra	hra	k1gFnSc1
na	na	k7c4
kytaru	kytara	k1gFnSc4
</s>
<s>
hudební	hudební	k2eAgNnSc4d1
manažerství	manažerství	k1gNnSc4
</s>
<s>
jazzová	jazzový	k2eAgFnSc1d1
interpretace	interpretace	k1gFnSc1
</s>
<s>
Studio	studio	k1gNnSc1
MARTA	Mars	k1gMnSc2
</s>
<s>
Studio	studio	k1gNnSc1
Marta	Mars	k1gMnSc2
je	být	k5eAaImIp3nS
scéna	scéna	k1gFnSc1
Divadelní	divadelní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
JAMU	jam	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
působí	působit	k5eAaImIp3nP
studenti	student	k1gMnPc1
ateliérů	ateliér	k1gInPc2
herectví	herectví	k1gNnPc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
<g/>
,	,	kIx,
dramaturgie	dramaturgie	k1gFnSc1
<g/>
,	,	kIx,
scénografie	scénografie	k1gFnSc1
<g/>
,	,	kIx,
jevištní	jevištní	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
atd.	atd.	kA
Ročně	ročně	k6eAd1
na	na	k7c6
svých	svůj	k3xOyFgNnPc6
prknech	prkno	k1gNnPc6
uvede	uvést	k5eAaPmIp3nS
12	#num#	k4
premiérových	premiérový	k2eAgMnPc2d1
titulů	titul	k1gInPc2
a	a	k8xC
na	na	k7c4
125	#num#	k4
představení	představení	k1gNnPc2
a	a	k8xC
řadí	řadit	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
nejnavštěvovanějším	navštěvovaný	k2eAgFnPc3d3
divadelním	divadelní	k2eAgFnPc3d1
scénám	scéna	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
koncepce	koncepce	k1gFnSc2
variabilní	variabilní	k2eAgFnSc2d1
divadelní	divadelní	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
s	s	k7c7
měnitelným	měnitelný	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Významní	významný	k2eAgMnPc1d1
absolventi	absolvent	k1gMnPc1
</s>
<s>
Na	na	k7c6
škole	škola	k1gFnSc6
studovaly	studovat	k5eAaImAgFnP
a	a	k8xC
ve	v	k7c6
studiu	studio	k1gNnSc6
Marta	Mars	k1gMnSc2
působily	působit	k5eAaImAgFnP
mnohé	mnohý	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
hudby	hudba	k1gFnSc2
<g/>
,	,	kIx,
divadla	divadlo	k1gNnSc2
<g/>
,	,	kIx,
rozhlasu	rozhlas	k1gInSc2
<g/>
,	,	kIx,
televize	televize	k1gFnSc2
i	i	k8xC
filmu	film	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Bořivoj	Bořivoj	k1gMnSc1
Srba	Srba	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Menšík	Menšík	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Somr	Somr	k1gMnSc1
</s>
<s>
Květa	Květa	k1gFnSc1
Fialová	Fialová	k1gFnSc1
</s>
<s>
Věra	Věra	k1gFnSc1
Galatíková	Galatíková	k1gFnSc1
</s>
<s>
Blanka	Blanka	k1gFnSc1
Bohdanová	Bohdanová	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Tálská	Tálská	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Gregor	Gregor	k1gMnSc1
Emmert	Emmert	k1gMnSc1
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Ištvan	Ištvan	k1gMnSc1
</s>
<s>
Alois	Alois	k1gMnSc1
Piňos	Piňos	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Chmelo	Chmela	k1gFnSc5
</s>
<s>
Zdena	Zdena	k1gFnSc1
Herfortová	Herfortová	k1gFnSc1
</s>
<s>
Hana	Hana	k1gFnSc1
Zagorová	Zagorová	k1gFnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Frej	frej	k1gInSc4
</s>
<s>
Karel	Karel	k1gMnSc1
Heřmánek	Heřmánek	k1gMnSc1
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1
Skopal	Skopal	k1gMnSc1
</s>
<s>
Eliška	Eliška	k1gFnSc1
Balzerová	Balzerová	k1gFnSc1
</s>
<s>
Dagmar	Dagmar	k1gFnSc1
Havlová	Havlová	k1gFnSc1
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
Polívka	Polívka	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Bartoška	Bartoška	k1gMnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Donutil	donutit	k5eAaPmAgMnS
</s>
<s>
Radúz	Radúz	k1gMnSc1
Mácha	Mácha	k1gMnSc1
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Moša	Moša	k1gMnSc1
</s>
<s>
Ivo	Ivo	k1gMnSc1
Krobot	Krobot	k?
</s>
<s>
Pavel	Pavel	k1gMnSc1
Zedníček	Zedníček	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Trávníček	Trávníček	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Tomek	Tomek	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Pecha	Pecha	k1gMnSc1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
Goldflam	Goldflam	k1gInSc4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Dufek	Dufek	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Morávek	Morávek	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Trtílek	Trtílek	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Brikcius	Brikcius	k1gMnSc1
</s>
<s>
Roman	Roman	k1gMnSc1
Sikora	Sikor	k1gMnSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Budař	Budař	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Matonoha	Matonoha	k?
</s>
<s>
Josef	Josef	k1gMnSc1
Polášek	Polášek	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Liška	Liška	k1gMnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Daniel	Daniel	k1gMnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Plodková	Plodkový	k2eAgFnSc1d1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Burdych	Burdych	k1gMnSc1
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Berešová	Berešová	k1gFnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Scherhaufer	Scherhaufer	k1gMnSc1
</s>
<s>
Alois	Alois	k1gMnSc1
Hajda	Hajda	k1gMnSc1
</s>
<s>
Rocc	Rocc	k6eAd1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Studený	Studený	k1gMnSc1
</s>
<s>
Radomil	Radomil	k1gMnSc1
Eliška	Eliška	k1gFnSc1
</s>
<s>
Dušan	Dušan	k1gMnSc1
Urban	Urban	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
příloha	příloha	k1gFnSc1
č.	č.	k?
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Brně	Brno	k1gNnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
72	#num#	k4
<g/>
,	,	kIx,
78	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7460	#num#	k4
<g/>
-	-	kIx~
<g/>
117	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SEYFERT	SEYFERT	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sochař	sochař	k1gMnSc1
Josef	Josef	k1gMnSc1
Břenek	Břenek	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tišnovské	tišnovský	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Tišnov	Tišnov	k1gInSc1
<g/>
,	,	kIx,
2020-03-05	2020-03-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zatloukal	Zatloukal	k1gMnSc1
Pavel	Pavel	k1gMnSc1
<g/>
:	:	kIx,
Brněnská	brněnský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
1815-1915	1815-1915	k4
-	-	kIx~
Průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Obecní	obecní	k2eAgInSc1d1
dům	dům	k1gInSc1
+	+	kIx~
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
47	#num#	k4
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
7745	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
↑	↑	k?
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
Setkání	setkání	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Encounter	Encounter	k1gInSc1
<g/>
↑	↑	k?
Akreditované	akreditovaný	k2eAgInPc4d1
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
–	–	k?
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Spolek	spolek	k1gInSc1
Management	management	k1gInSc1
a	a	k8xC
kultura	kultura	k1gFnSc1
<g/>
.	.	kIx.
culture-management	culture-management	k1gInSc1
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ateliér	ateliér	k1gInSc1
divadelního	divadelní	k2eAgNnSc2d1
manažerství	manažerství	k1gNnSc2
a	a	k8xC
jevištní	jevištní	k2eAgFnSc2d1
technologie	technologie	k1gFnSc2
<g/>
.	.	kIx.
difa	dif	k1gInSc2
<g/>
.	.	kIx.
<g/>
jamu	jam	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ateliér	ateliér	k1gInSc1
<g/>
.	.	kIx.
difa	difa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
jamu	jam	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Akreditované	akreditovaný	k2eAgInPc4d1
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
–	–	k?
Hudební	hudební	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
www.jamu.cz	www.jamu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Studio	studio	k1gNnSc1
Marta	Marta	k1gFnSc1
<g/>
.	.	kIx.
studiomarta	studiomarta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
jamu	jam	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.jamu.cz	www.jamu.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
•	•	k?
Hudební	hudební	k2eAgFnSc1d1
Ostatní	ostatní	k2eAgFnSc1d1
</s>
<s>
Studio	studio	k1gNnSc1
Marta	Mars	k1gMnSc2
•	•	k?
Divadlo	divadlo	k1gNnSc1
na	na	k7c6
Orlí	orlí	k2eAgFnSc6d1
•	•	k?
Centrum	centrum	k1gNnSc1
Astorka	Astorka	k1gFnSc1
•	•	k?
seznam	seznam	k1gInSc4
rektorů	rektor	k1gMnPc2
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Akademie	akademie	k1gFnSc2
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Česká	český	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
České	český	k2eAgFnSc6d1
vysoké	vysoká	k1gFnSc6
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
chemicko-technologická	chemicko-technologický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Liberec	Liberec	k1gInSc1
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
polytechnická	polytechnický	k2eAgFnSc1d1
Jihlava	Jihlava	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
•	•	k?
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Mendelova	Mendelův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
•	•	k?
Veterinární	veterinární	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Ostravská	ostravský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
báňská	báňský	k2eAgFnSc1d1
–	–	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
Opava	Opava	k1gFnSc1
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
Zlín	Zlín	k1gInSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
</s>
<s>
Leoš	Leoš	k1gMnSc1
Janáček	Janáček	k1gMnSc1
Dílo	dílo	k1gNnSc4
</s>
<s>
Operní	operní	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
</s>
<s>
Šárka	Šárka	k1gFnSc1
</s>
<s>
Počátek	počátek	k1gInSc1
románu	román	k1gInSc2
</s>
<s>
Její	její	k3xOp3gFnSc1
pastorkyňa	pastorkyňa	k1gFnSc1
(	(	kIx(
<g/>
Jenůfa	Jenůfa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Osud	osud	k1gInSc1
</s>
<s>
Výlety	výlet	k1gInPc1
páně	páně	k2eAgFnSc2d1
Broučkovy	Broučkův	k2eAgFnSc2d1
</s>
<s>
Káťa	Káťa	k1gFnSc1
Kabanová	Kabanová	k1gFnSc1
</s>
<s>
Příhody	příhoda	k1gFnPc1
lišky	liška	k1gFnSc2
Bystroušky	Bystrouška	k1gFnSc2
</s>
<s>
Věc	věc	k1gFnSc1
Makropulos	Makropulosa	k1gFnPc2
</s>
<s>
Z	z	k7c2
mrtvého	mrtvý	k2eAgInSc2d1
domu	dům	k1gInSc2
Skladby	skladba	k1gFnSc2
pro	pro	k7c4
orchestr	orchestr	k1gInSc4
</s>
<s>
Taras	taras	k1gInSc1
Bulba	Bulb	k1gMnSc2
</s>
<s>
Balada	balada	k1gFnSc1
blanická	blanický	k2eAgFnSc1d1
</s>
<s>
Lašské	lašský	k2eAgInPc4d1
tance	tanec	k1gInPc4
</s>
<s>
Sinfonietta	Sinfonietta	k1gFnSc1
Koncertantní	koncertantní	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
</s>
<s>
Concertino	concertino	k1gNnSc1
</s>
<s>
Capriccio	capriccio	k1gNnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Vzdor	vzdor	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Putování	putování	k1gNnSc1
dušičky	dušička	k1gFnSc2
Komorní	komorní	k2eAgFnSc2d1
tvorba	tvorba	k1gFnSc1
</s>
<s>
Pohádka	pohádka	k1gFnSc1
</s>
<s>
Houslová	houslový	k2eAgFnSc1d1
sonáta	sonáta	k1gFnSc1
</s>
<s>
Smyčcový	smyčcový	k2eAgInSc1d1
kvartet	kvartet	k1gInSc1
č.	č.	k?
1	#num#	k4
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Smyčcový	smyčcový	k2eAgInSc1d1
kvartet	kvartet	k1gInSc1
č.	č.	k?
2	#num#	k4
Klavírní	klavírní	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
</s>
<s>
Po	po	k7c6
zarostlém	zarostlý	k2eAgInSc6d1
chodníčku	chodníček	k1gInSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
X.	X.	kA
1905	#num#	k4
(	(	kIx(
<g/>
klavírní	klavírní	k2eAgFnSc1d1
sonáta	sonáta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
mlhách	mlha	k1gFnPc6
</s>
<s>
Intimní	intimní	k2eAgFnPc1d1
skicy	skic	k2eAgFnPc1d1
Skladby	skladba	k1gFnPc1
pro	pro	k7c4
sbor	sbor	k1gInSc4
</s>
<s>
Amarus	Amarus	k1gMnSc1
</s>
<s>
Elegie	elegie	k1gFnSc1
na	na	k7c4
smrt	smrt	k1gFnSc4
dcery	dcera	k1gFnSc2
Olgy	Olga	k1gFnSc2
</s>
<s>
Zápisník	zápisník	k1gInSc1
zmizelého	zmizelý	k2eAgMnSc2d1
</s>
<s>
Potulný	potulný	k2eAgMnSc1d1
šílenec	šílenec	k1gMnSc1
</s>
<s>
Glagolská	glagolský	k2eAgFnSc1d1
mše	mše	k1gFnSc1
</s>
<s>
Pojmenováno	pojmenován	k2eAgNnSc1d1
po	po	k7c6
Janáčkovi	Janáček	k1gMnSc3
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
(	(	kIx(
<g/>
JAMU	jam	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
filharmonie	filharmonie	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Janáčkovo	Janáčkův	k2eAgNnSc1d1
kvarteto	kvarteto	k1gNnSc1
</s>
<s>
Varhanická	varhanický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Letiště	letiště	k1gNnSc1
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Janáček	Janáček	k1gMnSc1
(	(	kIx(
<g/>
kráter	kráter	k1gInSc1
<g/>
)	)	kIx)
Související	související	k2eAgNnPc1d1
témata	téma	k1gNnPc1
</s>
<s>
Seznam	seznam	k1gInSc4
děl	dělo	k1gNnPc2
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
</s>
<s>
Hledání	hledání	k1gNnSc1
Janáčka	Janáček	k1gMnSc2
</s>
<s>
Můj	můj	k3xOp1gMnSc1
Janáček	Janáček	k1gMnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Stösslová	Stösslový	k2eAgFnSc1d1
</s>
<s>
Literární	literární	k2eAgFnSc1d1
opera	opera	k1gFnSc1
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
Portál	portál	k1gInSc1
<g/>
:	:	kIx,
<g/>
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711193	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8703	#num#	k4
3286	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81066645	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
126153359	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81066645	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
|	|	kIx~
Hudba	hudba	k1gFnSc1
</s>
