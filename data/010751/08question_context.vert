<s>
Pavel	Pavel	k1gMnSc1	Pavel
Brümer	Brümer	k1gMnSc1	Brümer
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1946	[number]	k4	1946
Šluknov	Šluknov	k1gInSc1	Šluknov
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
banjo	banjo	k1gNnSc4	banjo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Poláčkem	Poláček	k1gMnSc7	Poláček
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Turkem	Turek	k1gMnSc7	Turek
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
skupinu	skupina	k1gFnSc4	skupina
Greenes	Greenesa	k1gFnPc2	Greenesa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
později	pozdě	k6eAd2	pozdě
změnila	změnit	k5eAaPmAgFnS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
Bluegrass	bluegrass	k1gInSc4	bluegrass
Hoppers	Hoppersa	k1gFnPc2	Hoppersa
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
na	na	k7c4	na
Fešáci	fešák	k1gMnPc1	fešák
<g/>
.	.	kIx.	.
</s>
