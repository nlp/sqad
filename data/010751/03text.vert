<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Brümer	Brümer	k1gMnSc1	Brümer
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1946	[number]	k4	1946
Šluknov	Šluknov	k1gInSc1	Šluknov
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
banjo	banjo	k1gNnSc4	banjo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Poláčkem	Poláček	k1gMnSc7	Poláček
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Turkem	Turek	k1gMnSc7	Turek
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
skupinu	skupina	k1gFnSc4	skupina
Greenes	Greenesa	k1gFnPc2	Greenesa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
později	pozdě	k6eAd2	pozdě
změnila	změnit	k5eAaPmAgFnS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
Bluegrass	bluegrass	k1gInSc4	bluegrass
Hoppers	Hoppersa	k1gFnPc2	Hoppersa
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
na	na	k7c4	na
Fešáci	fešák	k1gMnPc1	fešák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Fešáků	fešák	k1gMnPc2	fešák
<g/>
,	,	kIx,	,
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
nucen	nutit	k5eAaImNgMnS	nutit
skupinu	skupina	k1gFnSc4	skupina
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
založil	založit	k5eAaPmAgInS	založit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
z	z	k7c2	z
Fešaků	Fešak	k1gInPc2	Fešak
skupinu	skupina	k1gFnSc4	skupina
Nervové	nervový	k2eAgNnSc1d1	nervové
sanatorium	sanatorium	k1gNnSc1	sanatorium
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Cadillac	cadillac	k1gInSc4	cadillac
podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
desky	deska	k1gFnSc2	deska
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
a	a	k8xC	a
1997	[number]	k4	1997
vydal	vydat	k5eAaPmAgInS	vydat
dvě	dva	k4xCgNnPc1	dva
společná	společný	k2eAgNnPc1d1	společné
alba	album	k1gNnPc1	album
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Linkou	linka	k1gFnSc7	linka
Poslední	poslední	k2eAgInSc1d1	poslední
soud	soud	k1gInSc1	soud
a	a	k8xC	a
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
jízda	jízda	k1gFnSc1	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
solovou	solový	k2eAgFnSc4d1	solová
desku	deska	k1gFnSc4	deska
Není	být	k5eNaImIp3nS	být
nejhůř	zle	k6eAd3	zle
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
nemoci	nemoc	k1gFnSc6	nemoc
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejznámější	známý	k2eAgInPc4d3	nejznámější
hity	hit	k1gInPc4	hit
==	==	k?	==
</s>
</p>
<p>
<s>
Matterhorn	Matterhorn	k1gMnSc1	Matterhorn
</s>
</p>
<p>
<s>
Dárek	dárek	k1gInSc1	dárek
vánoční	vánoční	k2eAgFnSc2d1	vánoční
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
honorář	honorář	k1gInSc1	honorář
</s>
</p>
<p>
<s>
Strašidelný	strašidelný	k2eAgInSc1d1	strašidelný
vlak	vlak	k1gInSc1	vlak
</s>
</p>
<p>
<s>
Country	country	k2eAgInSc1d1	country
bál	bál	k1gInSc1	bál
</s>
</p>
<p>
<s>
Amanda	Amanda	k1gFnSc1	Amanda
</s>
</p>
<p>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
Countryon	Countryon	k1gMnSc1	Countryon
</s>
</p>
<p>
<s>
Komedianti	komediant	k1gMnPc1	komediant
nestárnou	stárnout	k5eNaImIp3nP	stárnout
</s>
</p>
<p>
<s>
Cadillac	cadillac	k1gInSc1	cadillac
</s>
</p>
<p>
<s>
Kamráde	Kamrád	k1gMnSc5	Kamrád
<g/>
,	,	kIx,	,
pusť	pustit	k5eAaPmRp2nS	pustit
ten	ten	k3xDgMnSc1	ten
kolotoč	kolotoč	k1gInSc1	kolotoč
</s>
</p>
<p>
<s>
Ranní	ranní	k2eAgNnSc1d1	ranní
ticho	ticho	k1gNnSc1	ticho
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
vlaků	vlak	k1gInPc2	vlak
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
</s>
</p>
<p>
<s>
Paměti	paměť	k1gFnPc1	paměť
</s>
</p>
<p>
<s>
Neboj	bát	k5eNaImRp2nS	bát
<g/>
,	,	kIx,	,
lásko	láska	k1gFnSc5	láska
</s>
</p>
<p>
<s>
Od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
</s>
</p>
<p>
<s>
Zvony	zvon	k1gInPc1	zvon
</s>
</p>
<p>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Brümer	Brümer	k1gMnSc1	Brümer
-	-	kIx~	-
Cadillac	cadillac	k1gInSc1	cadillac
1992	[number]	k4	1992
Multisonic	Multisonice	k1gFnPc2	Multisonice
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Brümer	Brümer	k1gMnSc1	Brümer
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Linka	linka	k1gFnSc1	linka
-	-	kIx~	-
Poslední	poslední	k2eAgInSc1d1	poslední
soud	soud	k1gInSc1	soud
1995	[number]	k4	1995
PRESSTON	PRESSTON	kA	PRESSTON
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Brümer	Brümer	k1gMnSc1	Brümer
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Linka	linka	k1gFnSc1	linka
-	-	kIx~	-
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
jízda	jízda	k1gFnSc1	jízda
1997	[number]	k4	1997
AB	AB	kA	AB
STUDIO	studio	k1gNnSc1	studio
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Brümer	Brümer	k1gMnSc1	Brümer
-	-	kIx~	-
Není	být	k5eNaImIp3nS	být
nejhůř	zle	k6eAd3	zle
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
1999	[number]	k4	1999
AB	AB	kA	AB
STUDIO	studio	k1gNnSc1	studio
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Pavel	Pavel	k1gMnSc1	Pavel
Brümer	Brümer	k1gMnSc1	Brümer
</s>
</p>
