<s>
Led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šesterečný	šesterečný	k2eAgInSc4d1	šesterečný
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
specifické	specifický	k2eAgNnSc4d1	specifické
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
minerály	minerál	k1gInPc7	minerál
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
mineralogických	mineralogický	k2eAgInPc6d1	mineralogický
systémech	systém	k1gInPc6	systém
neuvádí	uvádět	k5eNaImIp3nS	uvádět
vůbec	vůbec	k9	vůbec
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
okrajově	okrajově	k6eAd1	okrajově
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
běžném	běžný	k2eAgInSc6d1	běžný
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
tlaku	tlak	k1gInSc6	tlak
tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
v	v	k7c4	v
led	led	k1gInSc4	led
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
273,15	[number]	k4	273,15
K	K	kA	K
<g/>
,	,	kIx,	,
32	[number]	k4	32
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpuštěny	rozpustit	k5eAaPmNgFnP	rozpustit
další	další	k2eAgFnPc1d1	další
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sůl	sůl	k1gFnSc1	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
voda	voda	k1gFnSc1	voda
zůstat	zůstat	k5eAaPmF	zůstat
tekutá	tekutý	k2eAgFnSc1d1	tekutá
i	i	k9	i
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
krystaly	krystal	k1gInPc1	krystal
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
celistvé	celistvý	k2eAgInPc4d1	celistvý
<g/>
,	,	kIx,	,
rozpadavé	rozpadavý	k2eAgInPc4d1	rozpadavý
<g/>
,	,	kIx,	,
zrnité	zrnitý	k2eAgInPc4d1	zrnitý
či	či	k8xC	či
sypké	sypký	k2eAgInPc4d1	sypký
agregáty	agregát	k1gInPc4	agregát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
sněhových	sněhový	k2eAgFnPc2d1	sněhová
vloček	vločka	k1gFnPc2	vločka
–	–	k?	–
kostrovitých	kostrovitý	k2eAgInPc2d1	kostrovitý
krystalů	krystal	k1gInPc2	krystal
(	(	kIx(	(
<g/>
složitě	složitě	k6eAd1	složitě
členěných	členěný	k2eAgFnPc2d1	členěná
šestiramenných	šestiramenný	k2eAgFnPc2d1	šestiramenná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
zploštělé	zploštělý	k2eAgInPc1d1	zploštělý
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
ledové	ledový	k2eAgInPc1d1	ledový
krystaly	krystal	k1gInPc1	krystal
kroupy	kroupa	k1gFnSc2	kroupa
jinovatka	jinovatka	k1gFnSc1	jinovatka
<g/>
,	,	kIx,	,
námraza	námraza	k1gFnSc1	námraza
kra	kra	k1gFnSc1	kra
rampouch	rampouch	k1gInSc4	rampouch
Světová	světový	k2eAgFnSc1d1	světová
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
organizace	organizace	k1gFnSc1	organizace
definuje	definovat	k5eAaBmIp3nS	definovat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
ledu	led	k1gInSc2	led
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
původu	původ	k1gInSc6	původ
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc6	tvar
<g/>
,	,	kIx,	,
váze	váha	k1gFnSc6	váha
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
Lze	lze	k6eAd1	lze
rýpat	rýpat	k5eAaImF	rýpat
nehtem	nehet	k1gInSc7	nehet
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
tvrdost	tvrdost	k1gFnSc1	tvrdost
1,5	[number]	k4	1,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
0,917	[number]	k4	0,917
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
neštěpný	štěpný	k2eNgInSc1d1	neštěpný
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
je	být	k5eAaImIp3nS	být
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
působení	působení	k1gNnSc6	působení
tlaku	tlak	k1gInSc2	tlak
plastický	plastický	k2eAgMnSc1d1	plastický
<g/>
,	,	kIx,	,
tepelně	tepelně	k6eAd1	tepelně
nestálý	stálý	k2eNgInSc1d1	nestálý
–	–	k?	–
taje	tát	k5eAaImIp3nS	tát
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
-80	-80	k4	-80
°	°	k?	°
<g/>
C	C	kA	C
krystaluje	krystalovat	k5eAaImIp3nS	krystalovat
v	v	k7c6	v
krychlové	krychlový	k2eAgFnSc6d1	krychlová
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
ε	ε	k?	ε
je	být	k5eAaImIp3nS	být
3,1	[number]	k4	3,1
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
čirý	čirý	k2eAgInSc1d1	čirý
až	až	k8xS	až
mléčně	mléčně	k6eAd1	mléčně
zakalený	zakalený	k2eAgInSc1d1	zakalený
<g/>
,	,	kIx,	,
namodralá	namodralý	k2eAgFnSc1d1	namodralá
<g/>
,	,	kIx,	,
modrozelená	modrozelený	k2eAgFnSc1d1	modrozelená
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Průhledný	průhledný	k2eAgInSc1d1	průhledný
až	až	k8xS	až
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
H	H	kA	H
11,19	[number]	k4	11,19
%	%	kIx~	%
<g/>
,	,	kIx,	,
O	o	k7c6	o
88,81	[number]	k4	88,81
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgMnSc1d1	běžný
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
oblastech	oblast	k1gFnPc6	oblast
sezónní	sezónní	k2eAgInSc4d1	sezónní
výskyt	výskyt	k1gInSc4	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
půd	půda	k1gFnPc2	půda
a	a	k8xC	a
sedimentů	sediment	k1gInPc2	sediment
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
a	a	k8xC	a
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
těžen	těžen	k2eAgInSc1d1	těžen
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
tzv.	tzv.	kA	tzv.
ledaři	ledař	k1gMnPc1	ledař
<g/>
,	,	kIx,	,
uskladněn	uskladněn	k2eAgInSc1d1	uskladněn
v	v	k7c6	v
ledárnách	ledárna	k1gFnPc6	ledárna
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgMnS	používat
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
chlazení	chlazení	k1gNnSc3	chlazení
potravin	potravina	k1gFnPc2	potravina
v	v	k7c6	v
teplejších	teplý	k2eAgNnPc6d2	teplejší
obdobích	období	k1gNnPc6	období
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
led	led	k1gInSc1	led
k	k	k7c3	k
témuž	týž	k3xTgInSc3	týž
účelu	účel	k1gInSc3	účel
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
průmyslově	průmyslově	k6eAd1	průmyslově
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
<g/>
:	:	kIx,	:
skladování	skladování	k1gNnSc1	skladování
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
zboží	zboží	k1gNnSc2	zboží
podléhající	podléhající	k2eAgFnSc1d1	podléhající
rychlé	rychlý	k2eAgFnSc3d1	rychlá
zkáze	zkáza	k1gFnSc3	zkáza
<g/>
,	,	kIx,	,
provozování	provozování	k1gNnSc3	provozování
ledových	ledový	k2eAgNnPc2d1	ledové
kluzišť	kluziště	k1gNnPc2	kluziště
a	a	k8xC	a
zimních	zimní	k2eAgInPc2d1	zimní
stadionů	stadion	k1gInPc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Kostky	kostka	k1gFnPc1	kostka
ledu	led	k1gInSc2	led
najdou	najít	k5eAaPmIp3nP	najít
využití	využití	k1gNnSc4	využití
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
chlazených	chlazený	k2eAgInPc2d1	chlazený
nápojů	nápoj	k1gInPc2	nápoj
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
po	po	k7c6	po
rozmixování	rozmixování	k1gNnSc6	rozmixování
jako	jako	k8xC	jako
ledové	ledový	k2eAgFnPc4d1	ledová
tříště	tříšť	k1gFnPc4	tříšť
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebo	nebo	k8xC	nebo
při	při	k7c6	při
ošetřování	ošetřování	k1gNnSc6	ošetřování
poranění	poranění	k1gNnSc2	poranění
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
vymknutí	vymknutí	k1gNnSc4	vymknutí
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
otoky	otok	k1gInPc1	otok
či	či	k8xC	či
po	po	k7c6	po
kousnutí	kousnutí	k1gNnSc6	kousnutí
<g/>
,	,	kIx,	,
bodnutí	bodnutí	k1gNnSc1	bodnutí
<g/>
,	,	kIx,	,
uštknutí	uštknutí	k1gNnSc1	uštknutí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
raritní	raritní	k2eAgNnSc4d1	raritní
využití	využití	k1gNnSc4	využití
patří	patřit	k5eAaImIp3nS	patřit
výroba	výroba	k1gFnSc1	výroba
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
skulptur	skulptura	k1gFnPc2	skulptura
z	z	k7c2	z
ledu	led	k1gInSc2	led
jako	jako	k8xC	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
na	na	k7c4	na
party	party	k1gFnSc4	party
nebo	nebo	k8xC	nebo
ledový	ledový	k2eAgInSc4d1	ledový
hotel	hotel	k1gInSc4	hotel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jukkasjärvi	Jukkasjärev	k1gFnSc6	Jukkasjärev
Ice	Ice	k1gFnPc2	Ice
Hotel	hotel	k1gInSc1	hotel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
odstavce	odstavec	k1gInPc1	odstavec
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
nejběžnější	běžný	k2eAgFnSc6d3	nejběžnější
pevné	pevný	k2eAgFnSc6d1	pevná
fázi	fáze	k1gFnSc6	fáze
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
led	led	k1gInSc1	led
Ih	Ih	k1gFnSc2	Ih
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	s	k7c7	s
šesterečnou	šesterečný	k2eAgFnSc7d1	šesterečná
(	(	kIx(	(
<g/>
hexagonální	hexagonální	k2eAgFnSc7d1	hexagonální
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
od	od	k7c2	od
bodu	bod	k1gInSc2	bod
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
až	až	k9	až
k	k	k7c3	k
73	[number]	k4	73
K	k	k7c3	k
a	a	k8xC	a
tlacích	tlak	k1gInPc6	tlak
do	do	k7c2	do
200	[number]	k4	200
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
Ic	Ic	k1gFnSc2	Ic
je	být	k5eAaImIp3nS	být
metastabilní	metastabilní	k2eAgFnSc7d1	metastabilní
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
krychlovou	krychlový	k2eAgFnSc7d1	krychlová
(	(	kIx(	(
<g/>
kubickou	kubický	k2eAgFnSc7d1	kubická
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
podobnou	podobný	k2eAgFnSc4d1	podobná
diamantu	diamant	k1gInSc2	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
130-150	[number]	k4	130-150
K	k	k7c3	k
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stabilní	stabilní	k2eAgFnSc1d1	stabilní
až	až	k9	až
do	do	k7c2	do
200	[number]	k4	200
K	K	kA	K
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přechází	přecházet	k5eAaImIp3nS	přecházet
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
Ih	Ih	k1gFnSc2	Ih
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ojediněle	ojediněle	k6eAd1	ojediněle
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k1gNnSc1	ostatní
pevné	pevný	k2eAgFnSc2d1	pevná
krystalické	krystalický	k2eAgFnSc2d1	krystalická
fáze	fáze	k1gFnSc2	fáze
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
ledu	led	k1gInSc2	led
XI	XI	kA	XI
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
umělé	umělý	k2eAgFnPc1d1	umělá
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
na	na	k7c6	na
ledových	ledový	k2eAgFnPc6d1	ledová
planetách	planeta	k1gFnPc6	planeta
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
Jupiterově	Jupiterův	k2eAgMnSc6d1	Jupiterův
Ganymedu	Ganymed	k1gMnSc6	Ganymed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stručné	stručný	k2eAgFnSc6d1	stručná
charakteristice	charakteristika	k1gFnSc6	charakteristika
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
typické	typický	k2eAgFnPc1d1	typická
podmínky	podmínka	k1gFnPc1	podmínka
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
přesný	přesný	k2eAgInSc1d1	přesný
fázový	fázový	k2eAgInSc1d1	fázový
diagram	diagram	k1gInSc1	diagram
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
II	II	kA	II
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
klencovou	klencový	k2eAgFnSc7d1	klencová
(	(	kIx(	(
<g/>
trigonální	trigonální	k2eAgFnSc7d1	trigonální
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
ledu	led	k1gInSc2	led
Ih	Ih	k1gFnSc2	Ih
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
198	[number]	k4	198
K	K	kA	K
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
300	[number]	k4	300
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
III	III	kA	III
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
čtverečnou	čtverečný	k2eAgFnSc7d1	čtverečná
(	(	kIx(	(
<g/>
tetragonální	tetragonální	k2eAgFnSc7d1	tetragonální
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tuhnutím	tuhnutí	k1gNnSc7	tuhnutí
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
250	[number]	k4	250
K	K	kA	K
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
300	[number]	k4	300
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
IV	IV	kA	IV
je	být	k5eAaImIp3nS	být
metastabilní	metastabilní	k2eAgFnSc7d1	metastabilní
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
klencovou	klencový	k2eAgFnSc7d1	klencová
(	(	kIx(	(
<g/>
trigonální	trigonální	k2eAgFnSc7d1	trigonální
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
tlaku	tlak	k1gInSc2	tlak
810	[number]	k4	810
MPa	MPa	k1gFnSc4	MPa
pomalým	pomalý	k2eAgNnSc7d1	pomalé
ohřátím	ohřátí	k1gNnSc7	ohřátí
amorfního	amorfní	k2eAgInSc2d1	amorfní
ledu	led	k1gInSc2	led
HDA	HDA	kA	HDA
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
V	V	kA	V
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
jednoklonnou	jednoklonný	k2eAgFnSc7d1	jednoklonná
(	(	kIx(	(
<g/>
monoklinickou	monoklinický	k2eAgFnSc7d1	monoklinická
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tuhnutím	tuhnutí	k1gNnSc7	tuhnutí
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
253	[number]	k4	253
K	K	kA	K
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
500	[number]	k4	500
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
VI	VI	kA	VI
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
čtverečnou	čtverečný	k2eAgFnSc7d1	čtverečná
(	(	kIx(	(
<g/>
tetragonální	tetragonální	k2eAgFnSc7d1	tetragonální
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tuhnutím	tuhnutí	k1gNnSc7	tuhnutí
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
270	[number]	k4	270
K	K	kA	K
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
1100	[number]	k4	1100
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
VII	VII	kA	VII
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
krychlovou	krychlový	k2eAgFnSc7d1	krychlová
(	(	kIx(	(
<g/>
kubickou	kubický	k2eAgFnSc7d1	kubická
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tuhnutím	tuhnutí	k1gNnSc7	tuhnutí
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
nad	nad	k7c4	nad
2200	[number]	k4	2200
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
VIII	VIII	kA	VIII
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
čtverečnou	čtverečný	k2eAgFnSc7d1	čtverečná
(	(	kIx(	(
<g/>
tetragonální	tetragonální	k2eAgFnSc7d1	tetragonální
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
ledu	led	k1gInSc2	led
VII	VII	kA	VII
ochlazením	ochlazení	k1gNnSc7	ochlazení
pod	pod	k7c4	pod
278	[number]	k4	278
K.	K.	kA	K.
Led	led	k1gInSc1	led
IX	IX	kA	IX
je	být	k5eAaImIp3nS	být
metastabilní	metastabilní	k2eAgFnSc7d1	metastabilní
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
čtverečnou	čtverečný	k2eAgFnSc7d1	čtverečná
(	(	kIx(	(
<g/>
tetragonální	tetragonální	k2eAgFnSc7d1	tetragonální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
ledu	led	k1gInSc2	led
III	III	kA	III
prudkým	prudký	k2eAgNnSc7d1	prudké
ochlazením	ochlazení	k1gNnSc7	ochlazení
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
165	[number]	k4	165
K.	K.	kA	K.
Je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c4	pod
140	[number]	k4	140
K	k	k7c3	k
a	a	k8xC	a
tlacích	tlak	k1gInPc6	tlak
200-400	[number]	k4	200-400
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
X	X	kA	X
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
krychlovou	krychlový	k2eAgFnSc7d1	krychlová
(	(	kIx(	(
<g/>
kubickou	kubický	k2eAgFnSc7d1	kubická
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tuhnutím	tuhnutí	k1gNnSc7	tuhnutí
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
nad	nad	k7c4	nad
70000	[number]	k4	70000
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
XI	XI	kA	XI
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
kosočtverečnou	kosočtverečný	k2eAgFnSc7d1	kosočtverečná
(	(	kIx(	(
<g/>
ortorombickou	ortorombický	k2eAgFnSc7d1	ortorombický
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
ledu	led	k1gInSc2	led
Ih	Ih	k1gFnSc2	Ih
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejstabilnější	stabilní	k2eAgFnSc7d3	nejstabilnější
pevnou	pevný	k2eAgFnSc7d1	pevná
fází	fáze	k1gFnSc7	fáze
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
antarktickém	antarktický	k2eAgInSc6d1	antarktický
ledu	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
XII	XII	kA	XII
je	být	k5eAaImIp3nS	být
metastabilní	metastabilní	k2eAgFnSc7d1	metastabilní
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
čtverečnou	čtverečný	k2eAgFnSc7d1	čtverečná
(	(	kIx(	(
<g/>
tetragonální	tetragonální	k2eAgFnSc7d1	tetragonální
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tuhnutím	tuhnutí	k1gNnSc7	tuhnutí
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
260	[number]	k4	260
K	K	kA	K
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
550	[number]	k4	550
MPa	MPa	k1gFnPc2	MPa
(	(	kIx(	(
<g/>
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
stability	stabilita	k1gFnSc2	stabilita
ledu	led	k1gInSc2	led
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
ledu	led	k1gInSc2	led
Ih	Ih	k1gFnSc4	Ih
prudkým	prudký	k2eAgNnSc7d1	prudké
stlačením	stlačení	k1gNnSc7	stlačení
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1000	[number]	k4	1000
MPa	MPa	k1gFnPc2	MPa
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ohřátím	ohřátit	k5eAaPmIp1nS	ohřátit
amorfního	amorfní	k2eAgInSc2d1	amorfní
ledu	led	k1gInSc2	led
HDA	HDA	kA	HDA
při	při	k7c6	při
tlacích	tlak	k1gInPc6	tlak
800-1600	[number]	k4	800-1600
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
XIII	XIII	kA	XIII
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
jednoklonnou	jednoklonný	k2eAgFnSc7d1	jednoklonná
(	(	kIx(	(
<g/>
monoklinickou	monoklinický	k2eAgFnSc7d1	monoklinická
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tuhnutím	tuhnutí	k1gNnSc7	tuhnutí
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
pod	pod	k7c4	pod
130	[number]	k4	130
K	K	kA	K
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
500	[number]	k4	500
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
XIV	XIV	kA	XIV
je	být	k5eAaImIp3nS	být
fází	fáze	k1gFnSc7	fáze
s	s	k7c7	s
kosočtverečnou	kosočtverečný	k2eAgFnSc7d1	kosočtverečná
(	(	kIx(	(
<g/>
ortorombickou	ortorombický	k2eAgFnSc7d1	ortorombický
<g/>
)	)	kIx)	)
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tuhnutím	tuhnutí	k1gNnSc7	tuhnutí
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
118	[number]	k4	118
K	K	kA	K
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
1200	[number]	k4	1200
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
XV	XV	kA	XV
je	být	k5eAaImIp3nS	být
předpovězenou	předpovězená	k1gFnSc4	předpovězená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
experimentálně	experimentálně	k6eAd1	experimentálně
neprokázanou	prokázaný	k2eNgFnSc7d1	neprokázaná
krystalickou	krystalický	k2eAgFnSc7d1	krystalická
fází	fáze	k1gFnSc7	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
vznikat	vznikat	k5eAaImF	vznikat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
80-108	[number]	k4	80-108
K	K	kA	K
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
1100	[number]	k4	1100
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
XVI	XVI	kA	XVI
je	být	k5eAaImIp3nS	být
krystalickou	krystalický	k2eAgFnSc7d1	krystalická
fází	fáze	k1gFnSc7	fáze
uměle	uměle	k6eAd1	uměle
připravenou	připravený	k2eAgFnSc7d1	připravená
v	v	k7c6	v
r.	r.	kA	r.
2014	[number]	k4	2014
vyprázdněním	vyprázdnění	k1gNnSc7	vyprázdnění
klathrátu	klathrát	k1gInSc2	klathrát
původně	původně	k6eAd1	původně
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
neonové	neonový	k2eAgFnSc2d1	neonová
molekuly	molekula	k1gFnSc2	molekula
obestavěné	obestavěný	k2eAgFnSc2d1	obestavěná
strukturou	struktura	k1gFnSc7	struktura
vázaných	vázaný	k2eAgFnPc2d1	vázaná
vodních	vodní	k2eAgFnPc2d1	vodní
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hustotu	hustota	k1gFnSc4	hustota
ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
krystalických	krystalický	k2eAgFnPc2d1	krystalická
forem	forma	k1gFnPc2	forma
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
byly	být	k5eAaImAgFnP	být
předpovězeny	předpovězen	k2eAgFnPc1d1	předpovězena
další	další	k2eAgFnPc1d1	další
krystalické	krystalický	k2eAgFnPc1d1	krystalická
fáze	fáze	k1gFnPc1	fáze
ledu	led	k1gInSc2	led
pro	pro	k7c4	pro
podmínky	podmínka	k1gFnPc4	podmínka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
dosud	dosud	k6eAd1	dosud
nedosažitelné	dosažitelný	k2eNgFnSc6d1	nedosažitelná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
tlaky	tlak	k1gInPc4	tlak
řádu	řád	k1gInSc2	řád
TPa	TPa	k1gFnSc2	TPa
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgNnSc1d1	odlišné
chování	chování	k1gNnSc1	chování
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
krystalický	krystalický	k2eAgInSc1d1	krystalický
led	led	k1gInSc1	led
tvořený	tvořený	k2eAgInSc1d1	tvořený
polotěžkou	polotěžký	k2eAgFnSc7d1	polotěžká
či	či	k8xC	či
těžkou	těžký	k2eAgFnSc7d1	těžká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
struktura	struktura	k1gFnSc1	struktura
ledu	led	k1gInSc2	led
VII	VII	kA	VII
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nS	lišit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
kyslíkovou	kyslíkový	k2eAgFnSc7d1	kyslíková
mřížkou	mřížka	k1gFnSc7	mřížka
s	s	k7c7	s
deuterony	deuteron	k1gInPc7	deuteron
v	v	k7c6	v
intersticiálních	intersticiální	k2eAgFnPc6d1	intersticiální
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
pevné	pevný	k2eAgFnPc1d1	pevná
fáze	fáze	k1gFnPc1	fáze
jsou	být	k5eAaImIp3nP	být
amorfní	amorfní	k2eAgFnPc1d1	amorfní
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
tři	tři	k4xCgInPc4	tři
<g/>
:	:	kIx,	:
Amorfní	amorfní	k2eAgInSc1d1	amorfní
led	led	k1gInSc1	led
LDA	LDA	kA	LDA
–	–	k?	–
"	"	kIx"	"
<g/>
low	low	k?	low
density	densit	k1gInPc1	densit
amorphous	amorphous	k1gInSc1	amorphous
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
též	též	k9	též
ASW	ASW	kA	ASW
–	–	k?	–
"	"	kIx"	"
<g/>
amorphous	amorphous	k1gInSc1	amorphous
solid	solid	k1gInSc1	solid
water	water	k1gInSc1	water
<g/>
"	"	kIx"	"
či	či	k8xC	či
HGW	HGW	kA	HGW
–	–	k?	–
"	"	kIx"	"
<g/>
hyperquenched	hyperquenched	k1gMnSc1	hyperquenched
glassy	glassa	k1gFnSc2	glassa
water	water	k1gMnSc1	water
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
skelnou	skelný	k2eAgFnSc4d1	skelná
amorfní	amorfní	k2eAgFnSc4d1	amorfní
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
např.	např.	kA	např.
pomalým	pomalý	k2eAgInSc7d1	pomalý
napařováním	napařování	k1gNnSc7	napařování
na	na	k7c4	na
malý	malý	k2eAgInSc4d1	malý
kovový	kovový	k2eAgInSc4d1	kovový
krystalový	krystalový	k2eAgInSc4d1	krystalový
povrch	povrch	k1gInSc4	povrch
při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc1d1	běžný
v	v	k7c6	v
podpovrchových	podpovrchový	k2eAgFnPc6d1	podpovrchová
vrstvách	vrstva	k1gFnPc6	vrstva
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Amorfní	amorfní	k2eAgInSc1d1	amorfní
led	led	k1gInSc1	led
HDA	HDA	kA	HDA
–	–	k?	–
"	"	kIx"	"
<g/>
high	high	k1gMnSc1	high
density	densit	k1gInPc4	densit
amorphous	amorphous	k1gInSc1	amorphous
<g/>
"	"	kIx"	"
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
stlačením	stlačení	k1gNnSc7	stlačení
ledu	led	k1gInSc2	led
Ih	Ih	k1gMnPc2	Ih
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c4	pod
140	[number]	k4	140
K	k	k7c3	k
tlakem	tlak	k1gInSc7	tlak
kolem	kolem	k7c2	kolem
1600	[number]	k4	1600
MPa	MPa	k1gFnPc2	MPa
nebo	nebo	k8xC	nebo
stlačením	stlačení	k1gNnSc7	stlačení
LDA	LDA	kA	LDA
tlakem	tlak	k1gInSc7	tlak
cca	cca	kA	cca
500	[number]	k4	500
MPa	MPa	k1gFnSc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Amorfní	amorfní	k2eAgInSc1d1	amorfní
led	led	k1gInSc1	led
VHDA	VHDA	kA	VHDA
–	–	k?	–
"	"	kIx"	"
<g/>
very	vera	k1gMnSc2	vera
high	high	k1gMnSc1	high
density	densit	k1gInPc4	densit
amorphous	amorphous	k1gInSc1	amorphous
<g/>
"	"	kIx"	"
vzniká	vznikat	k5eAaImIp3nS	vznikat
ohřátím	ohřátí	k1gNnSc7	ohřátí
HDA	HDA	kA	HDA
při	při	k7c6	při
tlacích	tlak	k1gInPc6	tlak
1000-2000	[number]	k4	1000-2000
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
druhem	druh	k1gInSc7	druh
fáze	fáze	k1gFnSc2	fáze
na	na	k7c6	na
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
kapalným	kapalný	k2eAgNnSc7d1	kapalné
a	a	k8xC	a
pevným	pevný	k2eAgNnSc7d1	pevné
skupenstvím	skupenství	k1gNnSc7	skupenství
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
amorfních	amorfní	k2eAgFnPc2d1	amorfní
fází	fáze	k1gFnPc2	fáze
ledu	led	k1gInSc2	led
tzv.	tzv.	kA	tzv.
superionický	superionický	k2eAgInSc4d1	superionický
led	led	k1gInSc4	led
(	(	kIx(	(
<g/>
SI	si	k1gNnSc2	si
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
jiným	jiný	k2eAgInSc7d1	jiný
názvem	název	k1gInSc7	název
superionická	superionický	k2eAgFnSc1d1	superionický
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
její	její	k3xOp3gFnSc6	její
kompaktní	kompaktní	k2eAgFnSc6d1	kompaktní
krystalové	krystalový	k2eAgFnSc6d1	krystalová
mřížce	mřížka	k1gFnSc6	mřížka
mají	mít	k5eAaImIp3nP	mít
pevnou	pevný	k2eAgFnSc4d1	pevná
polohu	poloha	k1gFnSc4	poloha
pouze	pouze	k6eAd1	pouze
atomy	atom	k1gInPc1	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
atomy	atom	k1gInPc1	atom
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc4d1	známa
3	[number]	k4	3
fáze	fáze	k1gFnPc4	fáze
superionického	superionický	k2eAgInSc2d1	superionický
ledu	led	k1gInSc2	led
<g/>
:	:	kIx,	:
Tzv.	tzv.	kA	tzv.
BCC-SI	BCC-SI	k1gFnSc1	BCC-SI
s	s	k7c7	s
kubickou	kubický	k2eAgFnSc7d1	kubická
prostorově	prostorově	k6eAd1	prostorově
centrovanou	centrovaný	k2eAgFnSc7d1	centrovaná
mřížkou	mřížka	k1gFnSc7	mřížka
(	(	kIx(	(
<g/>
bcc	bcc	k?	bcc
fáze	fáze	k1gFnSc1	fáze
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
anglického	anglický	k2eAgInSc2d1	anglický
body	bod	k1gInPc4	bod
centered	centered	k1gMnSc1	centered
cubic	cubic	k1gMnSc1	cubic
lattice	lattice	k1gFnSc1	lattice
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
r.	r.	kA	r.
1999	[number]	k4	1999
pomocí	pomoc	k1gFnSc7	pomoc
počítačové	počítačový	k2eAgFnSc2d1	počítačová
simulace	simulace	k1gFnSc2	simulace
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
vědců	vědec	k1gMnPc2	vědec
vedeném	vedený	k2eAgInSc6d1	vedený
Carlem	Carl	k1gMnSc7	Carl
Cavazzonim	Cavazzonima	k1gFnPc2	Cavazzonima
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
při	při	k7c6	při
tlacích	tlak	k1gInPc6	tlak
přesahujících	přesahující	k2eAgInPc6d1	přesahující
50	[number]	k4	50
000	[number]	k4	000
MPa	MPa	k1gFnPc6	MPa
a	a	k8xC	a
teplotách	teplota	k1gFnPc6	teplota
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
i	i	k8xC	i
příznaky	příznak	k1gInPc1	příznak
její	její	k3xOp3gFnSc2	její
skutečné	skutečný	k2eAgFnSc2d1	skutečná
existence	existence	k1gFnSc2	existence
v	v	k7c6	v
kosmu	kosmos	k1gInSc6	kosmos
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
CP-SI	CP-SI	k1gFnSc1	CP-SI
s	s	k7c7	s
nejtěsnějším	těsný	k2eAgNnSc7d3	nejtěsnější
uspořádáním	uspořádání	k1gNnSc7	uspořádání
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
anglického	anglický	k2eAgMnSc2d1	anglický
close-packed	closeacked	k1gInSc1	close-packed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
kubickou	kubický	k2eAgFnSc7d1	kubická
plošně	plošně	k6eAd1	plošně
centrovanou	centrovaný	k2eAgFnSc7d1	centrovaná
mřížkou	mřížka	k1gFnSc7	mřížka
(	(	kIx(	(
<g/>
fcc	fcc	k?	fcc
fáze	fáze	k1gFnSc1	fáze
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
anglického	anglický	k2eAgNnSc2d1	anglické
face	face	k1gNnSc2	face
centered	centered	k1gMnSc1	centered
cubic	cubic	k1gMnSc1	cubic
lattice	lattice	k1gFnSc1	lattice
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
r.	r.	kA	r.
2013	[number]	k4	2013
týmem	tým	k1gInSc7	tým
vedeným	vedený	k2eAgInSc7d1	vedený
Hugh	Hugh	k1gInSc4	Hugh
F.	F.	kA	F.
Wilsonem	Wilson	k1gInSc7	Wilson
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
fázového	fázový	k2eAgInSc2d1	fázový
diagramu	diagram	k1gInSc2	diagram
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
stabilnější	stabilní	k2eAgFnSc1d2	stabilnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
superionická	superionický	k2eAgFnSc1d1	superionický
bcc	bcc	k?	bcc
fáze	fáze	k1gFnSc1	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
tlak	tlak	k1gInSc1	tlak
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
je	být	k5eAaImIp3nS	být
nad	nad	k7c4	nad
100	[number]	k4	100
000	[number]	k4	000
MPa	MPa	k1gFnPc2	MPa
<g/>
;	;	kIx,	;
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
hodnotě	hodnota	k1gFnSc6	hodnota
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
fázovému	fázový	k2eAgInSc3d1	fázový
přechodu	přechod	k1gInSc3	přechod
mezi	mezi	k7c7	mezi
bcc	bcc	k?	bcc
a	a	k8xC	a
fcc	fcc	k?	fcc
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Superionická	Superionický	k2eAgFnSc1d1	Superionický
fcc	fcc	k?	fcc
fáze	fáze	k1gFnSc1	fáze
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nižší	nízký	k2eAgFnSc1d2	nižší
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
projevovat	projevovat	k5eAaImF	projevovat
i	i	k9	i
nižší	nízký	k2eAgFnSc7d2	nižší
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
a	a	k8xC	a
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
vodivostí	vodivost	k1gFnSc7	vodivost
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
bcc	bcc	k?	bcc
fáze	fáze	k1gFnSc1	fáze
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
Neptunu	Neptun	k1gInSc2	Neptun
a	a	k8xC	a
v	v	k7c6	v
exoplanetách	exoplaneta	k1gFnPc6	exoplaneta
s	s	k7c7	s
podobnými	podobný	k2eAgFnPc7d1	podobná
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
P	P	kA	P
<g/>
21	[number]	k4	21
<g/>
/	/	kIx~	/
<g/>
c-SI	c-SI	k?	c-SI
<g/>
,	,	kIx,	,
fáze	fáze	k1gFnSc1	fáze
se	s	k7c7	s
složitější	složitý	k2eAgFnSc7d2	složitější
symetrií	symetrie	k1gFnSc7	symetrie
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
mříže	mříž	k1gFnSc2	mříž
<g/>
,	,	kIx,	,
předpovězená	předpovězený	k2eAgFnSc1d1	předpovězená
týmem	tým	k1gInSc7	tým
vědců	vědec	k1gMnPc2	vědec
Princetonské	Princetonský	k2eAgFnSc2d1	Princetonská
university	universita	k1gFnSc2	universita
v	v	k7c6	v
r.	r.	kA	r.
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
fázovém	fázový	k2eAgInSc6d1	fázový
diagramu	diagram	k1gInSc6	diagram
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ještě	ještě	k6eAd1	ještě
vyšších	vysoký	k2eAgInPc2d2	vyšší
tlaků	tlak	k1gInPc2	tlak
než	než	k8xS	než
CP-SI	CP-SI	k1gFnPc2	CP-SI
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
výskyt	výskyt	k1gInSc1	výskyt
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
u	u	k7c2	u
předchozích	předchozí	k2eAgFnPc2d1	předchozí
fází	fáze	k1gFnPc2	fáze
<g/>
)	)	kIx)	)
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
velkých	velký	k2eAgFnPc2d1	velká
plynových	plynový	k2eAgFnPc2d1	plynová
planet	planeta	k1gFnPc2	planeta
–	–	k?	–
existenci	existence	k1gFnSc6	existence
superionického	superionický	k2eAgInSc2d1	superionický
ledu	led	k1gInSc2	led
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
složitá	složitý	k2eAgFnSc1d1	složitá
struktura	struktura	k1gFnSc1	struktura
jejich	jejich	k3xOp3gNnPc2	jejich
lokálních	lokální	k2eAgNnPc2d1	lokální
magnetických	magnetický	k2eAgNnPc2d1	magnetické
polí	pole	k1gNnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
BERNARD	Bernard	k1gMnSc1	Bernard
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
;	;	kIx,	;
ROST	rost	k1gInSc1	rost
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedický	encyklopedický	k2eAgInSc1d1	encyklopedický
přehled	přehled	k1gInSc1	přehled
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
701	[number]	k4	701
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
360	[number]	k4	360
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
324	[number]	k4	324
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
minerálů	minerál	k1gInPc2	minerál
voda	voda	k1gFnSc1	voda
–	–	k?	–
výskyt	výskyt	k1gInSc4	výskyt
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
ledu	led	k1gInSc2	led
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
přechlazená	přechlazený	k2eAgFnSc1d1	přechlazená
voda	voda	k1gFnSc1	voda
permafrost	permafrost	k1gFnSc1	permafrost
–	–	k?	–
věčně	věčně	k6eAd1	věčně
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
půda	půda	k1gFnSc1	půda
ledovec	ledovec	k1gInSc4	ledovec
zrnitý	zrnitý	k2eAgInSc1d1	zrnitý
led	led	k1gInSc1	led
ledoborec	ledoborec	k1gInSc1	ledoborec
Sněhovo-ledový	Sněhovoedový	k2eAgInSc1d1	Sněhovo-ledový
splaz	splaz	k1gInSc4	splaz
Ledovcový	ledovcový	k2eAgInSc4d1	ledovcový
splaz	splaz	k1gInSc4	splaz
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
led	led	k1gInSc1	led
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
led	led	k1gInSc1	led
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Led	led	k1gInSc1	led
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Led	led	k1gInSc1	led
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Led	led	k1gInSc1	led
v	v	k7c6	v
Atlasu	Atlas	k1gInSc6	Atlas
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
