<s>
Při	při	k7c6	při
běžném	běžný	k2eAgInSc6d1	běžný
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
tlaku	tlak	k1gInSc6	tlak
tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
v	v	k7c4	v
led	led	k1gInSc4	led
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
273,15	[number]	k4	273,15
K	K	kA	K
<g/>
,	,	kIx,	,
32	[number]	k4	32
°	°	k?	°
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
