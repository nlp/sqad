<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
a	a	k8xC	a
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Ivem	Ivo	k1gMnSc7	Ivo
Lukačovičem	Lukačovič	k1gMnSc7	Lukačovič
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
českých	český	k2eAgInPc2d1	český
internetových	internetový	k2eAgInPc2d1	internetový
katalogů	katalog	k1gInPc2	katalog
a	a	k8xC	a
vyhledávačů	vyhledávač	k1gMnPc2	vyhledávač
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
a	a	k8xC	a
katalog	katalog	k1gInSc1	katalog
firem	firma	k1gFnPc2	firma
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
doplňovaly	doplňovat	k5eAaImAgFnP	doplňovat
další	další	k2eAgFnPc1d1	další
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
firma	firma	k1gFnSc1	firma
provozovala	provozovat	k5eAaImAgFnS	provozovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
různých	různý	k2eAgFnPc2d1	různá
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
přidružených	přidružený	k2eAgFnPc2d1	přidružená
značek	značka	k1gFnPc2	značka
jako	jako	k8xC	jako
Email	email	k1gInSc1	email
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Mapy	mapa	k1gFnPc1	mapa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Sklik	Sklik	k1gInSc1	Sklik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Sreality	Srealit	k1gInPc1	Srealit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Sauto	Sauto	k1gNnSc1	Sauto
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
služeb	služba	k1gFnPc2	služba
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
internetu	internet	k1gInSc6	internet
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
6,75	[number]	k4	6,75
milionu	milion	k4xCgInSc2	milion
unikátních	unikátní	k2eAgMnPc2d1	unikátní
návštěvníků	návštěvník	k1gMnPc2	návštěvník
měsíčně	měsíčně	k6eAd1	měsíčně
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
z	z	k7c2	z
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejnavštěvovanější	navštěvovaný	k2eAgFnPc4d3	nejnavštěvovanější
služby	služba	k1gFnPc4	služba
patří	patřit	k5eAaImIp3nP	patřit
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
NetMonitoru	NetMonitor	k1gInSc2	NetMonitor
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Homepage	Homepage	k1gInSc1	Homepage
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc1	email
<g/>
.	.	kIx.	.
</s>
<s>
Ivo	Ivo	k1gMnSc1	Ivo
Lukačovič	Lukačovič	k1gMnSc1	Lukačovič
založil	založit	k5eAaPmAgMnS	založit
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
jako	jako	k8xC	jako
první	první	k4xOgInSc1	první
katalogový	katalogový	k2eAgInSc1d1	katalogový
server	server	k1gInSc1	server
a	a	k8xC	a
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
internetu	internet	k1gInSc6	internet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
spuštění	spuštění	k1gNnSc2	spuštění
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Ivo	Ivo	k1gMnSc1	Ivo
údajně	údajně	k6eAd1	údajně
investoval	investovat	k5eAaBmAgMnS	investovat
50	[number]	k4	50
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
měl	mít	k5eAaImAgMnS	mít
našetřené	našetřený	k2eAgNnSc4d1	našetřené
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
přibyla	přibýt	k5eAaPmAgFnS	přibýt
freemailová	freemailový	k2eAgFnSc1d1	freemailová
služba	služba	k1gFnSc1	služba
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc1	email
<g/>
,	,	kIx,	,
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
v	v	k7c6	v
doméně	doména	k1gFnSc6	doména
seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
přibyly	přibýt	k5eAaPmAgFnP	přibýt
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Seznam	seznam	k1gInSc1	seznam
disponoval	disponovat	k5eAaBmAgInS	disponovat
ještě	ještě	k6eAd1	ještě
zpravodajstvím	zpravodajství	k1gNnSc7	zpravodajství
<g/>
,	,	kIx,	,
TV	TV	kA	TV
přehledem	přehled	k1gInSc7	přehled
<g/>
,	,	kIx,	,
předpovědí	předpověď	k1gFnSc7	předpověď
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
kulturními	kulturní	k2eAgInPc7d1	kulturní
a	a	k8xC	a
finančními	finanční	k2eAgInPc7d1	finanční
přehledy	přehled	k1gInPc7	přehled
<g/>
,	,	kIx,	,
mapou	mapa	k1gFnSc7	mapa
ČR	ČR	kA	ČR
a	a	k8xC	a
komunitní	komunitní	k2eAgFnSc7d1	komunitní
službou	služba	k1gFnSc7	služba
Lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
investovala	investovat	k5eAaBmAgFnS	investovat
do	do	k7c2	do
rozvoje	rozvoj	k1gInSc2	rozvoj
Seznamu	seznam	k1gInSc2	seznam
švédská	švédský	k2eAgFnSc1d1	švédská
investiční	investiční	k2eAgFnSc1d1	investiční
společnost	společnost	k1gFnSc1	společnost
Spray	Spraa	k1gFnSc2	Spraa
Ventures	Venturesa	k1gFnPc2	Venturesa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
provozoval	provozovat	k5eAaImAgInS	provozovat
Lukačovič	Lukačovič	k1gInSc1	Lukačovič
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
jako	jako	k8xS	jako
živnostník	živnostník	k1gMnSc1	živnostník
<g/>
.	.	kIx.	.
</s>
<s>
Spray	Spra	k1gMnPc4	Spra
získal	získat	k5eAaPmAgInS	získat
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
zřejmě	zřejmě	k6eAd1	zřejmě
až	až	k9	až
30	[number]	k4	30
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc1d1	přesná
informace	informace	k1gFnPc1	informace
nelze	lze	k6eNd1	lze
do	do	k7c2	do
dnešního	dnešní	k2eAgNnSc2d1	dnešní
data	datum	k1gNnSc2	datum
hodnověrně	hodnověrně	k6eAd1	hodnověrně
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ivo	Ivo	k1gMnSc1	Ivo
Lukačovič	Lukačovič	k1gMnSc1	Lukačovič
i	i	k8xC	i
zástupci	zástupce	k1gMnPc1	zástupce
Seznamu	seznam	k1gInSc2	seznam
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
vždy	vždy	k6eAd1	vždy
odmítali	odmítat	k5eAaImAgMnP	odmítat
podat	podat	k5eAaPmF	podat
přesnější	přesný	k2eAgFnPc4d2	přesnější
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ivo	Ivo	k1gMnSc1	Ivo
Lukačovič	Lukačovič	k1gMnSc1	Lukačovič
měl	mít	k5eAaImAgMnS	mít
70	[number]	k4	70
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
Seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
akcie	akcie	k1gFnPc1	akcie
na	na	k7c4	na
majitele	majitel	k1gMnSc4	majitel
držel	držet	k5eAaImAgInS	držet
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Spray	Spray	k1gInPc1	Spray
vstupoval	vstupovat	k5eAaImAgMnS	vstupovat
do	do	k7c2	do
Seznamu	seznam	k1gInSc2	seznam
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
provést	provést	k5eAaPmF	provést
IPO	IPO	kA	IPO
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
splasknutí	splasknutí	k1gNnSc6	splasknutí
velké	velký	k2eAgFnSc2d1	velká
internetové	internetový	k2eAgFnSc2d1	internetová
bubliny	bublina	k1gFnSc2	bublina
však	však	k8xC	však
Spray	Spraa	k1gFnSc2	Spraa
snahu	snaha	k1gFnSc4	snaha
o	o	k7c6	o
IPO	IPO	kA	IPO
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
neprovedením	neprovedení	k1gNnSc7	neprovedení
IPO	IPO	kA	IPO
nedostál	dostát	k5eNaPmAgMnS	dostát
Spray	Spraa	k1gFnPc4	Spraa
svým	svůj	k3xOyFgInSc7	svůj
závazkům	závazek	k1gInPc3	závazek
ze	z	k7c2	z
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
Lukačovič	Lukačovič	k1gInSc1	Lukačovič
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nikdo	nikdo	k3yNnSc1	nikdo
ze	z	k7c2	z
Seznamu	seznam	k1gInSc2	seznam
nikdy	nikdy	k6eAd1	nikdy
neupřesnili	upřesnit	k5eNaPmAgMnP	upřesnit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
veřejných	veřejný	k2eAgFnPc2d1	veřejná
listin	listina	k1gFnPc2	listina
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Lukačovič	Lukačovič	k1gInSc1	Lukačovič
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
navýšit	navýšit	k5eAaPmF	navýšit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
70	[number]	k4	70
%	%	kIx~	%
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
základní	základní	k2eAgNnSc4d1	základní
jmění	jmění	k1gNnSc4	jmění
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
Spraye	Spray	k1gFnSc2	Spray
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
a	a	k8xC	a
navýšil	navýšit	k5eAaPmAgMnS	navýšit
tak	tak	k6eAd1	tak
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
v	v	k7c4	v
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Spray	Spraa	k1gFnPc1	Spraa
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
sloučil	sloučit	k5eAaPmAgInS	sloučit
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Lycos	Lycosa	k1gFnPc2	Lycosa
Europe	Europ	k1gMnSc5	Europ
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
od	od	k7c2	od
Iva	Ivo	k1gMnSc2	Ivo
Lukačoviče	Lukačovič	k1gMnSc2	Lukačovič
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnPc2	jeho
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Lukačovič	Lukačovič	k1gMnSc1	Lukačovič
se	se	k3xPyFc4	se
tak	tak	k9	tak
znovu	znovu	k6eAd1	znovu
stal	stát	k5eAaPmAgInS	stát
70	[number]	k4	70
<g/>
%	%	kIx~	%
vlastníkem	vlastník	k1gMnSc7	vlastník
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
také	také	k9	také
Lukačovič	Lukačovič	k1gInSc1	Lukačovič
představil	představit	k5eAaPmAgInS	představit
nového	nový	k2eAgMnSc4d1	nový
maskota	maskot	k1gMnSc4	maskot
společnosti	společnost	k1gFnSc2	společnost
psa	pes	k1gMnSc4	pes
Krastyho	Krasty	k1gMnSc4	Krasty
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
domovské	domovský	k2eAgFnSc6d1	domovská
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
nahradil	nahradit	k5eAaPmAgMnS	nahradit
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
fulltextovou	fulltextový	k2eAgFnSc4d1	fulltextová
technologii	technologie	k1gFnSc4	technologie
od	od	k7c2	od
Jyxa	Jyx	k1gInSc2	Jyx
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dále	daleko	k6eAd2	daleko
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
koupil	koupit	k5eAaPmAgInS	koupit
Seznam	seznam	k1gInSc1	seznam
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
společnost	společnost	k1gFnSc4	společnost
ATC	ATC	kA	ATC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
freemail	freemail	k1gInSc4	freemail
Email	email	k1gInSc4	email
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
službu	služba	k1gFnSc4	služba
následně	následně	k6eAd1	následně
včlenil	včlenit	k5eAaPmAgMnS	včlenit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
brandu	brand	k1gInSc2	brand
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
domény	doména	k1gFnPc4	doména
seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
i	i	k8xC	i
email	email	k1gInSc1	email
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
pod	pod	k7c7	pod
svou	svůj	k3xOyFgFnSc7	svůj
freemailovou	freemailův	k2eAgFnSc7d1	freemailův
službou	služba	k1gFnSc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgNnSc1d1	hlavní
sídlo	sídlo	k1gNnSc1	sídlo
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
z	z	k7c2	z
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
kanceláří	kancelář	k1gFnPc2	kancelář
v	v	k7c6	v
Naskové	Nasková	k1gFnSc6	Nasková
ul	ul	kA	ul
<g/>
.	.	kIx.	.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Košířích	Košíře	k1gInPc6	Košíře
do	do	k7c2	do
kanceláří	kancelář	k1gFnPc2	kancelář
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Radlická	radlický	k2eAgFnSc1d1	Radlická
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Ivo	Ivo	k1gMnSc1	Ivo
Lukačovič	Lukačovič	k1gMnSc1	Lukačovič
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
rovněž	rovněž	k6eAd1	rovněž
vzdal	vzdát	k5eAaPmAgInS	vzdát
přímého	přímý	k2eAgNnSc2d1	přímé
řízení	řízení	k1gNnSc2	řízení
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
exekutivu	exekutiva	k1gFnSc4	exekutiva
Pavlovi	Pavel	k1gMnSc3	Pavel
Zimovi	Zima	k1gMnSc3	Zima
<g/>
,	,	kIx,	,
dosavadnímu	dosavadní	k2eAgMnSc3d1	dosavadní
technickému	technický	k2eAgMnSc3d1	technický
řediteli	ředitel	k1gMnSc3	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Zima	Zima	k1gMnSc1	Zima
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
CEO	CEO	kA	CEO
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místo	místo	k1gNnSc4	místo
technického	technický	k2eAgMnSc2d1	technický
ředitele	ředitel	k1gMnSc2	ředitel
se	se	k3xPyFc4	se
posunul	posunout	k5eAaPmAgMnS	posunout
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Pečínka	pečínka	k1gFnSc1	pečínka
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
šéf	šéf	k1gMnSc1	šéf
vývojového	vývojový	k2eAgInSc2d1	vývojový
týmu	tým	k1gInSc2	tým
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
programátor	programátor	k1gMnSc1	programátor
služby	služba	k1gFnSc2	služba
Email	email	k1gInSc1	email
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
společnosti	společnost	k1gFnSc6	společnost
ATC	ATC	kA	ATC
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
rovněž	rovněž	k9	rovněž
převzal	převzít	k5eAaPmAgMnS	převzít
od	od	k7c2	od
Czech	Czech	k1gMnSc1	Czech
On	on	k3xPp3gMnSc1	on
Line	linout	k5eAaImIp3nS	linout
doménu	doména	k1gFnSc4	doména
a	a	k8xC	a
uživatele	uživatel	k1gMnPc4	uživatel
freemailu	freemail	k1gInSc2	freemail
Post	posta	k1gFnPc2	posta
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
většinou	většina	k1gFnSc7	většina
českých	český	k2eAgFnPc2d1	Česká
médii	médium	k1gNnPc7	médium
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
údajném	údajný	k2eAgNnSc6d1	údajné
odcizení	odcizení	k1gNnSc6	odcizení
databáze	databáze	k1gFnSc2	databáze
hesel	heslo	k1gNnPc2	heslo
uživatelů	uživatel	k1gMnPc2	uživatel
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Emailu	email	k1gInSc2	email
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
vypustila	vypustit	k5eAaPmAgFnS	vypustit
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hlavní	hlavní	k2eAgFnSc6d1	hlavní
zpravodajské	zpravodajský	k2eAgFnSc6d1	zpravodajská
relaci	relace	k1gFnSc6	relace
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
jednomu	jeden	k4xCgNnSc3	jeden
studentovi	student	k1gMnSc3	student
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
podařilo	podařit	k5eAaPmAgNnS	podařit
na	na	k7c6	na
školní	školní	k2eAgFnSc6d1	školní
počítačové	počítačový	k2eAgFnSc6d1	počítačová
síti	síť	k1gFnSc6	síť
sniffovat	sniffovat	k5eAaImF	sniffovat
hesla	heslo	k1gNnPc4	heslo
osmi	osm	k4xCc2	osm
jeho	jeho	k3xOp3gMnPc2	jeho
spolužáků	spolužák	k1gMnPc2	spolužák
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
následně	následně	k6eAd1	následně
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
osobní	osobní	k2eAgFnPc4d1	osobní
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
screenshot	screenshot	k1gMnSc1	screenshot
dané	daný	k2eAgFnSc2d1	daná
stránky	stránka	k1gFnSc2	stránka
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
kauza	kauza	k1gFnSc1	kauza
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
společným	společný	k2eAgNnSc7d1	společné
tiskovým	tiskový	k2eAgNnSc7d1	tiskové
prohlášením	prohlášení	k1gNnSc7	prohlášení
zástupců	zástupce	k1gMnPc2	zástupce
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
napadení	napadení	k1gNnSc6	napadení
databáze	databáze	k1gFnSc2	databáze
Seznamu	seznam	k1gInSc2	seznam
dementovaly	dementovat	k5eAaBmAgFnP	dementovat
<g/>
.	.	kIx.	.
</s>
<s>
Dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
student	student	k1gMnSc1	student
Modrák	modrák	k1gMnSc1	modrák
se	se	k3xPyFc4	se
v	v	k7c6	v
oné	onen	k3xDgFnSc6	onen
době	doba	k1gFnSc6	doba
těšil	těšit	k5eAaImAgMnS	těšit
značné	značný	k2eAgFnSc3d1	značná
mediální	mediální	k2eAgFnSc3d1	mediální
pozornosti	pozornost	k1gFnSc3	pozornost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
chlubil	chlubit	k5eAaImAgMnS	chlubit
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gFnSc4	jeho
osobní	osobní	k2eAgFnPc1d1	osobní
stránky	stránka	k1gFnPc1	stránka
už	už	k6eAd1	už
nefunkční	funkční	k2eNgFnPc1d1	nefunkční
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
koupil	koupit	k5eAaPmAgMnS	koupit
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
ovládací	ovládací	k2eAgInSc4d1	ovládací
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Global	globat	k5eAaImAgInS	globat
Inspiration	Inspiration	k1gInSc1	Inspiration
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
video	video	k1gNnSc4	video
portál	portál	k1gInSc1	portál
Stream	Stream	k1gInSc4	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
prodal	prodat	k5eAaPmAgInS	prodat
Lycos	Lycos	k1gInSc1	Lycos
Europe	Europ	k1gInSc5	Europ
svůj	svůj	k3xOyFgMnSc1	svůj
30	[number]	k4	30
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
v	v	k7c4	v
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
za	za	k7c2	za
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
1,7	[number]	k4	1,7
mld	mld	k?	mld
Kč	Kč	kA	Kč
společnostem	společnost	k1gFnPc3	společnost
Tiger	Tigra	k1gFnPc2	Tigra
Holding	holding	k1gInSc1	holding
Four	Four	k1gMnSc1	Four
a	a	k8xC	a
Miura	Miura	k1gMnSc1	Miura
International	International	k1gMnSc1	International
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
přinesly	přinést	k5eAaPmAgFnP	přinést
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
údajných	údajný	k2eAgNnPc6d1	údajné
jednáních	jednání	k1gNnPc6	jednání
zástupců	zástupce	k1gMnPc2	zástupce
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
s	s	k7c7	s
investiční	investiční	k2eAgFnSc7d1	investiční
bankou	banka	k1gFnSc7	banka
Goldman	Goldman	k1gMnSc1	Goldman
Sachs	Sachs	k1gInSc1	Sachs
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
kupce	kupec	k1gMnSc4	kupec
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
dodnes	dodnes	k6eAd1	dodnes
popírají	popírat	k5eAaImIp3nP	popírat
jak	jak	k6eAd1	jak
Ivo	Ivo	k1gMnSc1	Ivo
Lukačovič	Lukačovič	k1gMnSc1	Lukačovič
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zástupci	zástupce	k1gMnPc1	zástupce
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
chystaném	chystaný	k2eAgInSc6d1	chystaný
prodeji	prodej	k1gInSc6	prodej
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
nelze	lze	k6eNd1	lze
dodnes	dodnes	k6eAd1	dodnes
potvrdit	potvrdit	k5eAaPmF	potvrdit
z	z	k7c2	z
žádných	žádný	k3yNgInPc2	žádný
ověřitelných	ověřitelný	k2eAgInPc2d1	ověřitelný
a	a	k8xC	a
neanonymních	anonymní	k2eNgInPc2d1	neanonymní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Spekulace	spekulace	k1gFnPc1	spekulace
utichly	utichnout	k5eAaPmAgFnP	utichnout
v	v	k7c4	v
období	období	k1gNnSc4	období
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
světové	světový	k2eAgFnSc2d1	světová
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
například	například	k6eAd1	například
právě	právě	k9	právě
banka	banka	k1gFnSc1	banka
Goldman	Goldman	k1gMnSc1	Goldman
Sachs	Sachs	k1gInSc1	Sachs
ztratila	ztratit	k5eAaPmAgFnS	ztratit
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
přes	přes	k7c4	přes
70	[number]	k4	70
%	%	kIx~	%
hodnoty	hodnota	k1gFnPc1	hodnota
svých	svůj	k3xOyFgFnPc2	svůj
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
poprvé	poprvé	k6eAd1	poprvé
společnost	společnost	k1gFnSc1	společnost
nedominovala	dominovat	k5eNaImAgFnS	dominovat
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
Lupa	lupa	k1gFnSc1	lupa
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
předběhnuta	předběhnout	k5eAaPmNgFnS	předběhnout
společností	společnost	k1gFnSc7	společnost
Google	Google	k1gFnSc2	Google
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
Lupa	lupa	k1gFnSc1	lupa
i	i	k9	i
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
vyhledávače	vyhledávač	k1gMnSc2	vyhledávač
a	a	k8xC	a
databáze	databáze	k1gFnSc2	databáze
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
ocenění	ocenění	k1gNnSc4	ocenění
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
server	server	k1gInSc4	server
Mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
komunikační	komunikační	k2eAgFnSc2d1	komunikační
služby	služba	k1gFnSc2	služba
získala	získat	k5eAaPmAgFnS	získat
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
za	za	k7c4	za
emailovou	emailový	k2eAgFnSc4d1	emailová
službu	služba	k1gFnSc4	služba
jež	jenž	k3xRgNnSc1	jenž
nabízí	nabízet	k5eAaImIp3nS	nabízet
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Seznam	seznam	k1gInSc1	seznam
Email	email	k1gInSc4	email
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc4d1	další
ocenění	ocenění	k1gNnSc4	ocenění
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
sociální	sociální	k2eAgFnSc4d1	sociální
službu	služba	k1gFnSc4	služba
Lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
mobilní	mobilní	k2eAgNnSc4d1	mobilní
<g />
.	.	kIx.	.
</s>
<s>
přístup	přístup	k1gInSc1	přístup
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
mobilní	mobilní	k2eAgFnSc1d1	mobilní
služba	služba	k1gFnSc1	služba
<g/>
)	)	kIx)	)
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
majoritní	majoritní	k2eAgInSc1d1	majoritní
podíl	podíl	k1gInSc1	podíl
Iva	Ivo	k1gMnSc2	Ivo
Lukačoviče	Lukačovič	k1gMnSc2	Lukačovič
převeden	převést	k5eAaPmNgInS	převést
do	do	k7c2	do
kyperské	kyperský	k2eAgFnSc2d1	Kyperská
společnosti	společnost	k1gFnSc2	společnost
HELIFREAK	HELIFREAK	kA	HELIFREAK
LIMITED	limited	k2eAgFnPc1d1	limited
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
podle	podle	k7c2	podle
magazínu	magazín	k1gInSc2	magazín
Lupa	lupa	k1gFnSc1	lupa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
šéfa	šéf	k1gMnSc2	šéf
společnosti	společnost	k1gFnSc2	společnost
Pavla	Pavel	k1gMnSc2	Pavel
Zimy	Zima	k1gMnSc2	Zima
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vlastněna	vlastněn	k2eAgFnSc1d1	vlastněna
Ivem	Ivo	k1gMnSc7	Ivo
Lukačovičem	Lukačovič	k1gMnSc7	Lukačovič
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
majitelem	majitel	k1gMnSc7	majitel
vrtulníku	vrtulník	k1gInSc2	vrtulník
Eurocopter	Eurocopter	k1gInSc1	Eurocopter
EC	EC	kA	EC
<g/>
135	[number]	k4	135
<g/>
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
koupil	koupit	k5eAaPmAgInS	koupit
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
od	od	k7c2	od
Miloše	Miloš	k1gMnSc2	Miloš
Petany	Petana	k1gFnSc2	Petana
zbývající	zbývající	k2eAgFnSc2d1	zbývající
50	[number]	k4	50
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Global	globat	k5eAaImAgInS	globat
Inspiration	Inspiration	k1gInSc1	Inspiration
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
100	[number]	k4	100
<g/>
%	%	kIx~	%
vlastníkem	vlastník	k1gMnSc7	vlastník
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
videoportál	videoportál	k1gInSc4	videoportál
Stream	Stream	k1gInSc4	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
slevových	slevův	k2eAgInPc2d1	slevův
portálů	portál	k1gInPc2	portál
a	a	k8xC	a
spustil	spustit	k5eAaPmAgInS	spustit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
verzi	verze	k1gFnSc4	verze
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Seznam	seznam	k1gInSc1	seznam
TIP	tip	k1gInSc1	tip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
spouští	spoušť	k1gFnPc2	spoušť
podle	podle	k7c2	podle
odborných	odborný	k2eAgMnPc2d1	odborný
kritiků	kritik	k1gMnPc2	kritik
odvážnou	odvážný	k2eAgFnSc4d1	odvážná
podobu	podoba	k1gFnSc4	podoba
bulvárního	bulvární	k2eAgInSc2d1	bulvární
serveru	server	k1gInSc2	server
Super	super	k2eAgNnSc1d1	super
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
radikálně	radikálně	k6eAd1	radikálně
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
řazení	řazení	k1gNnSc1	řazení
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
diskuze	diskuze	k1gFnSc2	diskuze
a	a	k8xC	a
nechává	nechávat	k5eAaImIp3nS	nechávat
pouze	pouze	k6eAd1	pouze
sekvenčně	sekvenčně	k6eAd1	sekvenčně
řazené	řazený	k2eAgInPc4d1	řazený
články	článek	k1gInPc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
také	také	k9	také
mapová	mapový	k2eAgFnSc1d1	mapová
služba	služba	k1gFnSc1	služba
Mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
o	o	k7c4	o
mobilní	mobilní	k2eAgFnSc4d1	mobilní
aplikaci	aplikace	k1gFnSc4	aplikace
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
Android	android	k1gInSc4	android
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
provedl	provést	k5eAaPmAgInS	provést
fúzi	fúze	k1gFnSc4	fúze
s	s	k7c7	s
dříve	dříve	k6eAd2	dříve
koupenou	koupený	k2eAgFnSc7d1	koupená
společností	společnost	k1gFnSc7	společnost
Global	globat	k5eAaImAgInS	globat
Inspiration	Inspiration	k1gInSc1	Inspiration
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
videoserver	videoserver	k1gInSc4	videoserver
Stream	Stream	k1gInSc4	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
půlce	půlka	k1gFnSc6	půlka
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
zakládá	zakládat	k5eAaImIp3nS	zakládat
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
Mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
do	do	k7c2	do
které	který	k3yRgFnSc2	který
kupuje	kupovat	k5eAaImIp3nS	kupovat
digitální	digitální	k2eAgFnSc4d1	digitální
část	část	k1gFnSc4	část
podniku	podnik	k1gInSc2	podnik
svého	svůj	k3xOyFgMnSc2	svůj
mapového	mapový	k2eAgMnSc2d1	mapový
dodavatele	dodavatel	k1gMnSc2	dodavatel
-	-	kIx~	-
firmy	firma	k1gFnPc1	firma
PLANstudio	PLANstudio	k6eAd1	PLANstudio
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
změnou	změna	k1gFnSc7	změna
v	v	k7c6	v
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
vedení	vedení	k1gNnSc6	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Zima	Zima	k1gMnSc1	Zima
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
CEO	CEO	kA	CEO
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
Michal	Michal	k1gMnSc1	Michal
Feix	Feix	k1gInSc1	Feix
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zastával	zastávat	k5eAaImAgInS	zastávat
pozici	pozice	k1gFnSc4	pozice
provozního	provozní	k2eAgMnSc2d1	provozní
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
zájem	zájem	k1gInSc4	zájem
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
produktový	produktový	k2eAgInSc4d1	produktový
rozvoj	rozvoj	k1gInSc4	rozvoj
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
pár	pár	k4xCyI	pár
metrů	metr	k1gInPc2	metr
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
nově	nově	k6eAd1	nově
vybudovaných	vybudovaný	k2eAgFnPc2d1	vybudovaná
kanceláří	kancelář	k1gFnPc2	kancelář
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Andělu	Anděl	k1gMnSc3	Anděl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
konkurenčního	konkurenční	k2eAgInSc2d1	konkurenční
Googleplexu	Googleplex	k1gInSc2	Googleplex
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
zaměstnance	zaměstnanec	k1gMnSc4	zaměstnanec
horolezeckou	horolezecký	k2eAgFnSc4d1	horolezecká
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
velké	velká	k1gFnPc4	velká
odpočinkové	odpočinkový	k2eAgFnSc2d1	odpočinková
terasy	terasa	k1gFnSc2	terasa
nebo	nebo	k8xC	nebo
posilovnu	posilovna	k1gFnSc4	posilovna
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
představuje	představovat	k5eAaImIp3nS	představovat
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
vlastního	vlastní	k2eAgNnSc2d1	vlastní
datového	datový	k2eAgNnSc2d1	datové
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
doposud	doposud	k6eAd1	doposud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
koupil	koupit	k5eAaPmAgMnS	koupit
za	za	k7c4	za
85	[number]	k4	85
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
třetinový	třetinový	k2eAgInSc4d1	třetinový
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Borgis	borgis	k1gInSc4	borgis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
obou	dva	k4xCgMnPc2	dva
minoritních	minoritní	k2eAgMnPc2d1	minoritní
akcionářů	akcionář	k1gMnPc2	akcionář
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
30	[number]	k4	30
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
transakci	transakce	k1gFnSc4	transakce
nebyla	být	k5eNaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
získala	získat	k5eAaPmAgFnS	získat
celkově	celkově	k6eAd1	celkově
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
zakázku	zakázka	k1gFnSc4	zakázka
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
505	[number]	k4	505
000	[number]	k4	000
Kč	Kč	kA	Kč
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ze	z	k7c2	z
strukturálních	strukturální	k2eAgInPc2d1	strukturální
fondů	fond	k1gInPc2	fond
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
-	-	kIx~	-
2015	[number]	k4	2015
37	[number]	k4	37
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Portálu	portál	k1gInSc2	portál
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
konkurovaly	konkurovat	k5eAaImAgInP	konkurovat
portály	portál	k1gInPc1	portál
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ale	ale	k8xC	ale
ani	ani	k8xC	ani
po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
nemají	mít	k5eNaImIp3nP	mít
podle	podle	k7c2	podle
NetMonitoru	NetMonitor	k1gInSc2	NetMonitor
takovou	takový	k3xDgFnSc4	takový
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
a	a	k8xC	a
trend	trend	k1gInSc4	trend
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
překonaly	překonat	k5eAaPmAgFnP	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
konkurentem	konkurent	k1gMnSc7	konkurent
portálu	portál	k1gInSc2	portál
je	být	k5eAaImIp3nS	být
Google	Google	k1gFnSc1	Google
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fulltextového	fulltextový	k2eAgNnSc2d1	Fulltextové
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
(	(	kIx(	(
<g/>
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
začal	začít	k5eAaPmAgInS	začít
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
používat	používat	k5eAaImF	používat
nové	nový	k2eAgNnSc4d1	nové
fulltextové	fulltextový	k2eAgNnSc4d1	Fulltextové
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
Navrcholu	Navrchol	k1gInSc2	Navrchol
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
nejpoužívanějším	používaný	k2eAgMnSc7d3	nejpoužívanější
internetovým	internetový	k2eAgMnSc7d1	internetový
vyhledávačem	vyhledávač	k1gMnSc7	vyhledávač
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
53,91	[number]	k4	53,91
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
32,85	[number]	k4	32,85
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
čtyřmi	čtyři	k4xCgInPc7	čtyři
společnostmi	společnost	k1gFnPc7	společnost
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
-	-	kIx~	-
Baidu	Baido	k1gNnSc6	Baido
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Naver	Naver	k1gInSc1	Naver
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Yahoo	Yahoo	k1gMnSc1	Yahoo
Japan	japan	k1gInSc1	japan
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Yandex	Yandex	k1gInSc1	Yandex
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
-	-	kIx~	-
představuje	představovat	k5eAaImIp3nS	představovat
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
hrstku	hrstka	k1gFnSc4	hrstka
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
vzdorují	vzdorovat	k5eAaImIp3nP	vzdorovat
globálnímu	globální	k2eAgNnSc3d1	globální
hráči	hráč	k1gMnSc6	hráč
Google	Google	k1gFnSc1	Google
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
on-line	onin	k1gInSc5	on-lin
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
počátku	počátek	k1gInSc2	počátek
společností	společnost	k1gFnPc2	společnost
využívající	využívající	k2eAgFnSc4d1	využívající
především	především	k9	především
opensource	opensourka	k1gFnSc3	opensourka
technologie	technologie	k1gFnSc2	technologie
a	a	k8xC	a
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
většina	většina	k1gFnSc1	většina
serverů	server	k1gInPc2	server
běží	běžet	k5eAaImIp3nS	běžet
s	s	k7c7	s
linuxovým	linuxový	k2eAgInSc7d1	linuxový
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Debian	Debiana	k1gFnPc2	Debiana
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
webové	webový	k2eAgInPc4d1	webový
servery	server	k1gInPc4	server
používá	používat	k5eAaImIp3nS	používat
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
podle	podle	k7c2	podle
letmého	letmý	k2eAgInSc2d1	letmý
průzkumu	průzkum	k1gInSc2	průzkum
telnetem	telnet	k1gInSc7	telnet
nejčastěji	často	k6eAd3	často
NGINX	NGINX	kA	NGINX
<g/>
,	,	kIx,	,
Apache	Apache	k1gNnSc1	Apache
webserver	webservra	k1gFnPc2	webservra
nebo	nebo	k8xC	nebo
Lighttpd	Lighttpda	k1gFnPc2	Lighttpda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
aplikací	aplikace	k1gFnPc2	aplikace
je	být	k5eAaImIp3nS	být
vyvíjena	vyvíjet	k5eAaImNgFnS	vyvíjet
v	v	k7c6	v
Pythonu	Python	k1gMnSc6	Python
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
databázové	databázový	k2eAgInPc4d1	databázový
servery	server	k1gInPc4	server
používá	používat	k5eAaImIp3nS	používat
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
MySQL	MySQL	k1gMnSc1	MySQL
<g/>
,	,	kIx,	,
PSQL	PSQL	kA	PSQL
<g/>
,	,	kIx,	,
HADOOP	HADOOP	kA	HADOOP
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
serverů	server	k1gInPc2	server
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přes	přes	k7c4	přes
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
spotřeba	spotřeba	k1gFnSc1	spotřeba
IT	IT	kA	IT
vybavení	vybavení	k1gNnSc1	vybavení
Seznamu	seznam	k1gInSc2	seznam
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
200	[number]	k4	200
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
začátcích	začátek	k1gInPc6	začátek
hostován	hostován	k2eAgInSc4d1	hostován
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
CESNETu	CESNETus	k1gInSc2	CESNETus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
přešel	přejít	k5eAaPmAgInS	přejít
pod	pod	k7c4	pod
poskytovatele	poskytovatel	k1gMnSc4	poskytovatel
Global	globat	k5eAaImAgMnS	globat
One	One	k1gMnSc1	One
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
servery	server	k1gInPc4	server
a	a	k8xC	a
technologie	technologie	k1gFnPc4	technologie
pak	pak	k6eAd1	pak
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
do	do	k7c2	do
telehousu	telehous	k1gInSc2	telehous
TTC	TTC	kA	TTC
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
Malešicích	Malešice	k1gFnPc6	Malešice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
některých	některý	k3yIgInPc6	některý
problémech	problém	k1gInPc6	problém
služby	služba	k1gFnSc2	služba
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc4	email
začalo	začít	k5eAaPmAgNnS	začít
budování	budování	k1gNnSc1	budování
druhé	druhý	k4xOgFnSc2	druhý
záložní	záložní	k2eAgFnSc2d1	záložní
serverovny	serverovna	k1gFnSc2	serverovna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Seznam	seznam	k1gInSc1	seznam
zajistil	zajistit	k5eAaPmAgInS	zajistit
maximální	maximální	k2eAgFnSc4d1	maximální
dostupnost	dostupnost	k1gFnSc4	dostupnost
nejen	nejen	k6eAd1	nejen
freemailu	freemaila	k1gFnSc4	freemaila
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
serverovnu	serverovna	k1gFnSc4	serverovna
v	v	k7c6	v
telehousu	telehous	k1gInSc6	telehous
O2	O2	k1gFnSc2	O2
Nagano	Nagano	k1gNnSc1	Nagano
v	v	k7c6	v
Pražském	pražský	k2eAgInSc6d1	pražský
Karlíně	Karlín	k1gInSc6	Karlín
zprovoznil	zprovoznit	k5eAaPmAgInS	zprovoznit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
ostrého	ostrý	k2eAgInSc2d1	ostrý
provozu	provoz	k1gInSc2	provoz
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
datové	datový	k2eAgNnSc1d1	datové
centrum	centrum	k1gNnSc1	centrum
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kokura	Kokur	k1gMnSc2	Kokur
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přemístil	přemístit	k5eAaPmAgInS	přemístit
své	svůj	k3xOyFgInPc4	svůj
servery	server	k1gInPc4	server
z	z	k7c2	z
TTC	TTC	kA	TTC
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
pyšní	pyšnit	k5eAaImIp3nS	pyšnit
duálním	duální	k2eAgInSc7d1	duální
provozem	provoz	k1gInSc7	provoz
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
provozuje	provozovat	k5eAaImIp3nS	provozovat
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
serverovnách	serverovna	k1gFnPc6	serverovna
paralelně	paralelně	k6eAd1	paralelně
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
připojen	připojit	k5eAaPmNgInS	připojit
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
peerovacího	peerovací	k2eAgInSc2d1	peerovací
uzlu	uzel	k1gInSc2	uzel
NIX	NIX	kA	NIX
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
sdružení	sdružení	k1gNnSc2	sdružení
RIPE	RIPE	kA	RIPE
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc1d1	vlastní
autonomní	autonomní	k2eAgInSc1d1	autonomní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
IPv	IPv	k1gFnSc1	IPv
<g/>
4	[number]	k4	4
adresní	adresní	k2eAgInSc4d1	adresní
rozsah	rozsah	k1gInSc4	rozsah
77.75.72.0	[number]	k4	77.75.72.0
<g/>
/	/	kIx~	/
<g/>
21	[number]	k4	21
a	a	k8xC	a
IPv	IPv	k1gMnSc1	IPv
<g/>
6	[number]	k4	6
rozsah	rozsah	k1gInSc1	rozsah
2	[number]	k4	2
<g/>
a	a	k8xC	a
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
598	[number]	k4	598
<g/>
::	::	k?	::
<g/>
/	/	kIx~	/
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
veřejných	veřejný	k2eAgInPc2d1	veřejný
zdrojů	zdroj	k1gInPc2	zdroj
používá	používat	k5eAaImIp3nS	používat
Seznam	seznam	k1gInSc1	seznam
tranzitní	tranzitní	k2eAgFnSc4d1	tranzitní
konektivitu	konektivita	k1gFnSc4	konektivita
od	od	k7c2	od
nejméně	málo	k6eAd3	málo
dvou	dva	k4xCgMnPc2	dva
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
-	-	kIx~	-
Telefóniky	Telefónik	k1gInPc1	Telefónik
a	a	k8xC	a
Dial	Dial	k1gInSc1	Dial
Telecomu	Telecom	k1gInSc2	Telecom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
začal	začít	k5eAaPmAgInS	začít
Seznam	seznam	k1gInSc4	seznam
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
Horních	horní	k2eAgFnPc6d1	horní
Počernicích	Počernice	k1gFnPc6	Počernice
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
vlastního	vlastní	k2eAgNnSc2d1	vlastní
datacentra	datacentrum	k1gNnSc2	datacentrum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
později	pozdě	k6eAd2	pozdě
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
růstu	růst	k1gInSc6	růst
služeb	služba	k1gFnPc2	služba
Seznamu	seznam	k1gInSc2	seznam
i	i	k8xC	i
vlastní	vlastní	k2eAgFnSc2d1	vlastní
technické	technický	k2eAgFnSc2d1	technická
soběstačnosti	soběstačnost	k1gFnSc2	soběstačnost
<g/>
.	.	kIx.	.
</s>
<s>
Datacentrum	Datacentrum	k1gNnSc1	Datacentrum
Seznamu	seznam	k1gInSc2	seznam
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
vyjde	vyjít	k5eAaPmIp3nS	vyjít
na	na	k7c4	na
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
datacentra	datacentr	k1gMnSc2	datacentr
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
kladen	klást	k5eAaImNgInS	klást
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
energetickou	energetický	k2eAgFnSc4d1	energetická
efektivitu	efektivita	k1gFnSc4	efektivita
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
návratnost	návratnost	k1gFnSc1	návratnost
by	by	kYmCp3nS	by
tak	tak	k9	tak
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
Seznamu	seznam	k1gInSc6	seznam
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
do	do	k7c2	do
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
od	od	k7c2	od
dokončení	dokončení	k1gNnSc2	dokončení
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
plného	plný	k2eAgNnSc2d1	plné
spuštění	spuštění	k1gNnSc2	spuštění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
březnu	březen	k1gInSc3	březen
2013	[number]	k4	2013
provozuje	provozovat	k5eAaImIp3nS	provozovat
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
necelých	celý	k2eNgInPc2d1	necelý
30	[number]	k4	30
služeb	služba	k1gFnPc2	služba
<g/>
/	/	kIx~	/
<g/>
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vyvíjených	vyvíjený	k2eAgInPc2d1	vyvíjený
a	a	k8xC	a
spravovaných	spravovaný	k2eAgInPc2d1	spravovaný
in-house	inouse	k6eAd1	in-house
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
portfolia	portfolio	k1gNnSc2	portfolio
patří	patřit	k5eAaImIp3nP	patřit
Firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Horoskopy	horoskop	k1gInPc1	horoskop
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Spolužáci	spolužák	k1gMnPc1	spolužák
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Mapy	mapa	k1gFnPc1	mapa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Super	super	k1gInSc1	super
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Proženy	Prožen	k2eAgFnPc1d1	Prožen
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc1	email
<g/>
,	,	kIx,	,
Sdovolena	Sdovolen	k2eAgFnSc1d1	Sdovolena
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Sfinance	Sfinance	k1gFnSc1	Sfinance
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Porovnej	porovnat	k5eAaPmRp2nS	porovnat
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spráce	Spráce	k1gFnSc1	Spráce
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Sbazar	Sbazar	k1gInSc1	Sbazar
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
TIP	tip	k1gInSc1	tip
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
Homepage	Homepag	k1gFnSc2	Homepag
<g/>
,	,	kIx,	,
Sreality	Srealita	k1gFnSc2	Srealita
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Zboží	zboží	k1gNnSc1	zboží
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Sauto	Sauto	k1gNnSc1	Sauto
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Sklik	Sklik	k1gInSc1	Sklik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
fulltext	fulltext	k1gInSc1	fulltext
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
Slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
nebo	nebo	k8xC	nebo
TV	TV	kA	TV
Program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
službám	služba	k1gFnPc3	služba
patřila	patřit	k5eAaImAgFnS	patřit
také	také	k9	také
stránka	stránka	k1gFnSc1	stránka
Mixér	mixér	k1gMnSc1	mixér
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ovšem	ovšem	k9	ovšem
Seznam	seznam	k1gInSc4	seznam
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
služba	služba	k1gFnSc1	služba
z	z	k7c2	z
portfólia	portfólium	k1gNnSc2	portfólium
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gInSc3	jeho
vzniku	vznik	k1gInSc3	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
počátku	počátek	k1gInSc6	počátek
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
pouze	pouze	k6eAd1	pouze
rozcestník	rozcestník	k1gInSc4	rozcestník
pro	pro	k7c4	pro
katalog	katalog	k1gInSc4	katalog
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
odkazů	odkaz	k1gInPc2	odkaz
s	s	k7c7	s
bannerovou	bannerův	k2eAgFnSc7d1	bannerův
reklamou	reklama	k1gFnSc7	reklama
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvořila	tvořit	k5eAaImAgFnS	tvořit
část	část	k1gFnSc4	část
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
financován	financovat	k5eAaBmNgMnS	financovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
na	na	k7c6	na
homepage	homepage	k1gFnSc6	homepage
přidaly	přidat	k5eAaPmAgFnP	přidat
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
z	z	k7c2	z
portfólia	portfólium	k1gNnSc2	portfólium
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
vznikaly	vznikat	k5eAaImAgInP	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
první	první	k4xOgFnSc6	první
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
služba	služba	k1gFnSc1	služba
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc1	email
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
homepage	homepage	k6eAd1	homepage
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
personifikovanou	personifikovaný	k2eAgFnSc4d1	personifikovaná
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
uživatel	uživatel	k1gMnSc1	uživatel
po	po	k7c4	po
přihlášení	přihlášení	k1gNnSc4	přihlášení
může	moct	k5eAaImIp3nS	moct
nastavit	nastavit	k5eAaPmF	nastavit
vzhled	vzhled	k1gInSc4	vzhled
své	svůj	k3xOyFgFnSc2	svůj
homepage	homepag	k1gFnSc2	homepag
a	a	k8xC	a
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
odstranit	odstranit	k5eAaPmF	odstranit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
homepage	homepag	k1gFnSc2	homepag
veškerou	veškerý	k3xTgFnSc4	veškerý
reklamu	reklama	k1gFnSc4	reklama
a	a	k8xC	a
provozovat	provozovat	k5eAaImF	provozovat
ji	on	k3xPp3gFnSc4	on
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
rozcestník	rozcestník	k1gInSc4	rozcestník
nad	nad	k7c4	nad
další	další	k2eAgFnPc4d1	další
seznamácké	seznamácký	k2eAgFnPc4d1	seznamácká
služby	služba	k1gFnPc4	služba
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
personifikace	personifikace	k1gFnSc2	personifikace
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Homepage	Homepage	k1gInSc1	Homepage
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
NetMonitoru	NetMonitor	k1gInSc2	NetMonitor
s	s	k7c7	s
6,0	[number]	k4	6,0
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
reálnými	reálný	k2eAgMnPc7d1	reálný
uživateli	uživatel	k1gMnPc7	uživatel
měsíčně	měsíčně	k6eAd1	měsíčně
nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3	nejnavštěvovanější
stránkou	stránka	k1gFnSc7	stránka
Českého	český	k2eAgInSc2d1	český
internetu	internet	k1gInSc2	internet
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
k	k	k7c3	k
prosinci	prosinec	k1gInSc3	prosinec
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
seznam	seznam	k1gInSc1	seznam
spustil	spustit	k5eAaPmAgInS	spustit
novou	nový	k2eAgFnSc4d1	nová
podobu	podoba	k1gFnSc4	podoba
homepage	homepag	k1gFnSc2	homepag
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
ladí	ladit	k5eAaImIp3nP	ladit
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc1	email
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Homepage	Homepage	k1gInSc4	Homepage
nejviditelnější	viditelný	k2eAgFnSc7d3	nejviditelnější
službou	služba	k1gFnSc7	služba
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
obhospodařovala	obhospodařovat	k5eAaImAgFnS	obhospodařovat
služba	služba	k1gFnSc1	služba
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc1	email
přes	přes	k7c4	přes
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
aktivních	aktivní	k2eAgFnPc2d1	aktivní
emailových	emailový	k2eAgFnPc2d1	emailová
schránek	schránka	k1gFnPc2	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc1	email
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
freemailovou	freemailův	k2eAgFnSc7d1	freemailův
službou	služba	k1gFnSc7	služba
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
všech	všecek	k3xTgFnPc2	všecek
schránek	schránka	k1gFnPc2	schránka
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
maximální	maximální	k2eAgFnSc1d1	maximální
možná	možný	k2eAgFnSc1d1	možná
velikost	velikost	k1gFnSc1	velikost
e-mailové	eailové	k2eAgFnSc2d1	e-mailové
schránky	schránka	k1gFnSc2	schránka
2	[number]	k4	2
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Vánoc	Vánoce	k1gFnPc2	Vánoce
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
limit	limit	k1gInSc1	limit
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
velikost	velikost	k1gFnSc1	velikost
schránky	schránka	k1gFnSc2	schránka
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
<s>
Navýšení	navýšení	k1gNnSc1	navýšení
na	na	k7c4	na
neomezenou	omezený	k2eNgFnSc4d1	neomezená
kapacitu	kapacita	k1gFnSc4	kapacita
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zasláním	zaslání	k1gNnSc7	zaslání
speciální	speciální	k2eAgFnSc2d1	speciální
aktivační	aktivační	k2eAgFnSc2d1	aktivační
SMS	SMS	kA	SMS
<g/>
.	.	kIx.	.
</s>
<s>
Omezením	omezení	k1gNnSc7	omezení
"	"	kIx"	"
<g/>
neomezené	omezený	k2eNgFnSc2d1	neomezená
<g/>
"	"	kIx"	"
schránky	schránka	k1gFnSc2	schránka
je	být	k5eAaImIp3nS	být
limit	limit	k1gInSc1	limit
30	[number]	k4	30
000	[number]	k4	000
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
služba	služba	k1gFnSc1	služba
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Email	email	k1gInSc1	email
redesignováno	redesignován	k2eAgNnSc1d1	redesignován
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
podoby	podoba	k1gFnSc2	podoba
přestěhováno	přestěhován	k2eAgNnSc4d1	přestěhováno
cca	cca	kA	cca
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
schránek	schránka	k1gFnPc2	schránka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
počátku	počátek	k1gInSc6	počátek
měl	mít	k5eAaImAgInS	mít
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
technologii	technologie	k1gFnSc4	technologie
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Kompas	kompas	k1gInSc1	kompas
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
pro	pro	k7c4	pro
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
naprogramoval	naprogramovat	k5eAaPmAgMnS	naprogramovat
Ivův	Ivův	k2eAgMnSc1d1	Ivův
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
spolužák	spolužák	k1gMnSc1	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Kompas	kompas	k1gInSc4	kompas
následně	následně	k6eAd1	následně
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
outsourcovaná	outsourcovaný	k2eAgFnSc1d1	outsourcovaná
služba	služba	k1gFnSc1	služba
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Empyreum	empyreum	k1gNnSc1	empyreum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
společnost	společnost	k1gFnSc4	společnost
Empyreum	empyreum	k1gNnSc1	empyreum
větší	veliký	k2eAgFnSc2d2	veliký
globální	globální	k2eAgFnSc2d1	globální
partner	partner	k1gMnSc1	partner
Google	Google	k1gNnSc4	Google
a	a	k8xC	a
Jyxo	Jyxo	k1gNnSc4	Jyxo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
provozuje	provozovat	k5eAaImIp3nS	provozovat
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
fulltextový	fulltextový	k2eAgMnSc1d1	fulltextový
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
<g/>
,	,	kIx,	,
orientovaný	orientovaný	k2eAgMnSc1d1	orientovaný
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
a	a	k8xC	a
slovenský	slovenský	k2eAgInSc4d1	slovenský
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základní	základní	k2eAgFnSc2d1	základní
funkčnosti	funkčnost	k1gFnSc2	funkčnost
podporuje	podporovat	k5eAaImIp3nS	podporovat
tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
(	(	kIx(	(
<g/>
skloňování	skloňování	k1gNnSc1	skloňování
a	a	k8xC	a
časování	časování	k1gNnSc1	časování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
technologii	technologie	k1gFnSc4	technologie
sitemap	sitemap	k1gMnSc1	sitemap
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
HTML	HTML	kA	HTML
formátu	formát	k1gInSc2	formát
indexuje	indexovat	k5eAaImIp3nS	indexovat
také	také	k9	také
XML	XML	kA	XML
<g/>
,	,	kIx,	,
PDF	PDF	kA	PDF
a	a	k8xC	a
DOC.	doc.	kA	doc.
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
výdejové	výdejový	k2eAgFnSc6d1	výdejová
databázi	databáze	k1gFnSc6	databáze
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
750	[number]	k4	750
milionů	milion	k4xCgInPc2	milion
dokumentů	dokument	k1gInPc2	dokument
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
slovenském	slovenský	k2eAgInSc6d1	slovenský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
v	v	k7c6	v
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
internetu	internet	k1gInSc6	internet
outsourcoval	outsourcovat	k5eAaBmAgInS	outsourcovat
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Google	Google	k1gInSc1	Google
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
nahradil	nahradit	k5eAaPmAgMnS	nahradit
technologii	technologie	k1gFnSc4	technologie
Googlu	Googl	k1gInSc2	Googl
produkt	produkt	k1gInSc1	produkt
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
,	,	kIx,	,
Bing	bingo	k1gNnPc2	bingo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
spustil	spustit	k5eAaPmAgInS	spustit
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
vlastní	vlastní	k2eAgFnSc4d1	vlastní
indexaci	indexace	k1gFnSc4	indexace
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
webů	web	k1gInPc2	web
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
představil	představit	k5eAaPmAgMnS	představit
vlastní	vlastní	k2eAgMnSc1d1	vlastní
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
pro	pro	k7c4	pro
platformy	platforma	k1gFnPc4	platforma
Windows	Windows	kA	Windows
a	a	k8xC	a
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
doplnil	doplnit	k5eAaPmAgMnS	doplnit
již	již	k6eAd1	již
existující	existující	k2eAgMnSc1d1	existující
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
aplikačním	aplikační	k2eAgNnSc6d1	aplikační
rozhraní	rozhraní	k1gNnSc6	rozhraní
node-webkit	nodeebkit	k5eAaBmF	node-webkit
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
je	být	k5eAaImIp3nS	být
Blink	blink	k0	blink
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
konkurenčního	konkurenční	k2eAgNnSc2d1	konkurenční
Google	Google	k1gNnSc2	Google
Chrome	chromat	k5eAaImIp3nS	chromat
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
v	v	k7c6	v
novém	nový	k2eAgMnSc6d1	nový
prohlížeči	prohlížeč	k1gMnSc6	prohlížeč
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mnohem	mnohem	k6eAd1	mnohem
snadnější	snadný	k2eAgInSc1d2	snadnější
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
službám	služba	k1gFnPc3	služba
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
nenáročné	náročný	k2eNgMnPc4d1	nenáročný
uživatele	uživatel	k1gMnPc4	uživatel
a	a	k8xC	a
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
oproti	oproti	k7c3	oproti
jiným	jiný	k1gMnPc3	jiný
skoupější	skoupý	k2eAgNnSc4d2	skoupější
na	na	k7c4	na
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
instalace	instalace	k1gFnSc2	instalace
doplňků	doplněk	k1gInPc2	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
spustila	spustit	k5eAaPmAgFnS	spustit
společnost	společnost	k1gFnSc1	společnost
službu	služba	k1gFnSc4	služba
Seznam	seznam	k1gInSc4	seznam
Zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
servis	servis	k1gInSc4	servis
začala	začít	k5eAaPmAgFnS	začít
poskytovat	poskytovat	k5eAaImF	poskytovat
třicetičlenná	třicetičlenný	k2eAgFnSc1d1	třicetičlenná
redakce	redakce	k1gFnSc1	redakce
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
přešli	přejít	k5eAaPmAgMnP	přejít
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
Novy	nova	k1gFnSc2	nova
<g/>
,	,	kIx,	,
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Economia	Economium	k1gNnSc2	Economium
či	či	k8xC	či
MAFRA	MAFRA	kA	MAFRA
<g/>
.	.	kIx.	.
</s>
<s>
Jejího	její	k3xOp3gNnSc2	její
vedení	vedení	k1gNnSc2	vedení
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
někdejší	někdejší	k2eAgMnSc1d1	někdejší
zakladatel	zakladatel	k1gMnSc1	zakladatel
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Jakub	Jakub	k1gMnSc1	Jakub
Unger	Unger	k1gMnSc1	Unger
a	a	k8xC	a
zástupcem	zástupce	k1gMnSc7	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
Jiří	Jiří	k1gMnSc1	Jiří
Kubík	Kubík	k1gMnSc1	Kubík
<g/>
.	.	kIx.	.
</s>
<s>
Třemi	tři	k4xCgInPc7	tři
základními	základní	k2eAgInPc7d1	základní
prvky	prvek	k1gInPc7	prvek
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
stát	stát	k5eAaImF	stát
<g/>
:	:	kIx,	:
proud	proud	k1gInSc1	proud
krátkých	krátká	k1gFnPc2	krátká
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
či	či	k8xC	či
rychlých	rychlý	k2eAgNnPc2d1	rychlé
videí	video	k1gNnPc2	video
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
relace	relace	k1gFnSc2	relace
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
komentovanými	komentovaný	k2eAgFnPc7d1	komentovaná
večerními	večerní	k2eAgFnPc7d1	večerní
Zprávami	zpráva	k1gFnPc7	zpráva
na	na	k7c6	na
Seznamu	seznam	k1gInSc6	seznam
a	a	k8xC	a
autorské	autorský	k2eAgInPc4d1	autorský
pořady	pořad	k1gInPc4	pořad
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Šťastné	Šťastné	k2eAgNnSc1d1	Šťastné
pondělí	pondělí	k1gNnSc1	pondělí
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Šídla	Šídlo	k1gMnSc2	Šídlo
či	či	k8xC	či
Interview	interview	k1gNnSc1	interview
Zuzany	Zuzana	k1gFnSc2	Zuzana
Hodkové	Hodková	k1gFnSc2	Hodková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Tiscali	Tiscali	k1gFnSc1	Tiscali
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Portál	portál	k1gInSc4	portál
Seznam	seznam	k1gInSc4	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
