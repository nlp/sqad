<s>
Velbloud	velbloud	k1gMnSc1	velbloud
jednohrbý	jednohrbý	k2eAgMnSc1d1	jednohrbý
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
často	často	k6eAd1	často
jako	jako	k8xC	jako
dromedár	dromedár	k1gMnSc1	dromedár
nebo	nebo	k8xC	nebo
dromedář	dromedář	k1gMnSc1	dromedář
(	(	kIx(	(
<g/>
Camelus	Camelus	k1gMnSc1	Camelus
dromedarius	dromedarius	k1gMnSc1	dromedarius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
savec	savec	k1gMnSc1	savec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
velbloudovití	velbloudovitý	k2eAgMnPc1d1	velbloudovitý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
už	už	k6eAd1	už
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
ve	v	k7c6	v
zdomácnělé	zdomácnělý	k2eAgFnSc6d1	zdomácnělá
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přenášení	přenášení	k1gNnSc3	přenášení
těžkých	těžký	k2eAgInPc2d1	těžký
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zdomácňováním	zdomácňování	k1gNnSc7	zdomácňování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
už	už	k6eAd1	už
asi	asi	k9	asi
4000	[number]	k4	4000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
dromedár	dromedár	k1gMnSc1	dromedár
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
dromedár	dromedár	k1gMnSc1	dromedár
významné	významný	k2eAgNnSc1d1	významné
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
zvíře	zvíře	k1gNnSc1	zvíře
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
území	území	k1gNnSc6	území
výskytu	výskyt	k1gInSc2	výskyt
<g/>
,	,	kIx,	,
v	v	k7c6	v
Přední	přední	k2eAgFnSc6d1	přední
a	a	k8xC	a
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
až	až	k9	až
po	po	k7c6	po
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Divoce	divoce	k6eAd1	divoce
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
dromedáři	dromedár	k1gMnPc1	dromedár
uměle	uměle	k6eAd1	uměle
vysazeni	vysazen	k2eAgMnPc1d1	vysazen
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
přirozeným	přirozený	k2eAgInSc7d1	přirozený
biotopem	biotop	k1gInSc7	biotop
byly	být	k5eAaImAgFnP	být
polopouště	polopoušť	k1gFnPc4	polopoušť
a	a	k8xC	a
suché	suchý	k2eAgFnPc4d1	suchá
stepi	step	k1gFnPc4	step
<g/>
,	,	kIx,	,
pouště	poušť	k1gFnPc4	poušť
a	a	k8xC	a
planiny	planina	k1gFnPc4	planina
<g/>
.	.	kIx.	.
</s>
<s>
Velbloud	velbloud	k1gMnSc1	velbloud
jednohrbý	jednohrbý	k2eAgMnSc1d1	jednohrbý
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc1d1	velké
zvíře	zvíře	k1gNnSc1	zvíře
s	s	k7c7	s
dlouhýma	dlouhý	k2eAgFnPc7d1	dlouhá
nohama	noha	k1gFnPc7	noha
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
2,2	[number]	k4	2,2
až	až	k9	až
3,4	[number]	k4	3,4
m	m	kA	m
a	a	k8xC	a
vážící	vážící	k2eAgInSc1d1	vážící
450-550	[number]	k4	450-550
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
krémově	krémově	k6eAd1	krémově
hnědou	hnědý	k2eAgFnSc4d1	hnědá
hrubou	hrubý	k2eAgFnSc4d1	hrubá
srst	srst	k1gFnSc4	srst
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
na	na	k7c6	na
temeni	temeno	k1gNnSc6	temeno
<g/>
,	,	kIx,	,
krku	krk	k1gInSc6	krk
<g/>
,	,	kIx,	,
hrdle	hrdla	k1gFnSc6	hrdla
a	a	k8xC	a
hrbu	hrb	k1gInSc6	hrb
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
měřící	měřící	k2eAgInSc1d1	měřící
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
očích	oko	k1gNnPc6	oko
má	mít	k5eAaImIp3nS	mít
dvouvrstvé	dvouvrstvý	k2eAgFnPc4d1	dvouvrstvá
řasy	řasa	k1gFnPc4	řasa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ho	on	k3xPp3gMnSc4	on
ochraňují	ochraňovat	k5eAaImIp3nP	ochraňovat
před	před	k7c4	před
vniknutí	vniknutí	k1gNnSc4	vniknutí
písku	písek	k1gInSc2	písek
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
dromedára	dromedár	k1gMnSc2	dromedár
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
hrb	hrb	k1gInSc4	hrb
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
tak	tak	k9	tak
zvíře	zvíře	k1gNnSc1	zvíře
před	před	k7c7	před
sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zásobní	zásobní	k2eAgInSc1d1	zásobní
tuk	tuk	k1gInSc1	tuk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
odbouráváním	odbourávání	k1gNnSc7	odbourávání
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
životu	život	k1gInSc3	život
v	v	k7c6	v
horkém	horký	k2eAgNnSc6d1	horké
<g/>
,	,	kIx,	,
suchém	suchý	k2eAgNnSc6d1	suché
podnebí	podnebí	k1gNnSc6	podnebí
je	být	k5eAaImIp3nS	být
velbloud	velbloud	k1gMnSc1	velbloud
vybaven	vybavit	k5eAaPmNgInS	vybavit
různými	různý	k2eAgFnPc7d1	různá
adaptacemi	adaptace	k1gFnPc7	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
Nejnápadnější	nápadní	k2eAgMnSc1d3	nápadní
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
přečkat	přečkat	k5eAaPmF	přečkat
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
bez	bez	k7c2	bez
pití	pití	k1gNnSc2	pití
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
vázat	vázat	k5eAaImF	vázat
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velbloud	velbloud	k1gMnSc1	velbloud
ovšem	ovšem	k9	ovšem
nezadržuje	zadržovat	k5eNaImIp3nS	zadržovat
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
hrbu	hrb	k1gInSc6	hrb
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
dříve	dříve	k6eAd2	dříve
lidé	člověk	k1gMnPc1	člověk
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
výstelce	výstelka	k1gFnSc6	výstelka
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Ledviny	ledvina	k1gFnPc1	ledvina
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
koncentrovat	koncentrovat	k5eAaBmF	koncentrovat
moč	moč	k1gFnSc4	moč
a	a	k8xC	a
snižovat	snižovat	k5eAaImF	snižovat
tak	tak	k6eAd1	tak
ztrátu	ztráta	k1gFnSc4	ztráta
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
vlhkost	vlhkost	k1gFnSc1	vlhkost
je	být	k5eAaImIp3nS	být
absorbována	absorbovat	k5eAaBmNgFnS	absorbovat
z	z	k7c2	z
trusu	trus	k1gInSc2	trus
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
klesá	klesat	k5eAaImIp3nS	klesat
velbloudům	velbloud	k1gMnPc3	velbloud
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
během	během	k7c2	během
dne	den	k1gInSc2	den
zvolna	zvolna	k6eAd1	zvolna
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zvířata	zvíře	k1gNnPc1	zvíře
nemusejí	muset	k5eNaImIp3nP	muset
potit	potit	k5eAaImF	potit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ochladila	ochladit	k5eAaPmAgFnS	ochladit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
období	období	k1gNnSc2	období
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
může	moct	k5eAaImIp3nS	moct
velbloud	velbloud	k1gMnSc1	velbloud
jednohrbý	jednohrbý	k2eAgMnSc1d1	jednohrbý
ztratit	ztratit	k5eAaPmF	ztratit
až	až	k9	až
27	[number]	k4	27
%	%	kIx~	%
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
bez	bez	k7c2	bez
vážných	vážný	k2eAgInPc2d1	vážný
následků	následek	k1gInPc2	následek
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
ztrátu	ztráta	k1gFnSc4	ztráta
dokáže	dokázat	k5eAaPmIp3nS	dokázat
rychle	rychle	k6eAd1	rychle
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
během	během	k7c2	během
asi	asi	k9	asi
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypije	vypít	k5eAaPmIp3nS	vypít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudi	velbloud	k1gMnPc1	velbloud
jednohrbí	jednohrbý	k2eAgMnPc1d1	jednohrbý
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
trávou	tráva	k1gFnSc7	tráva
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
dostupnými	dostupný	k2eAgFnPc7d1	dostupná
rostlinami	rostlina	k1gFnPc7	rostlina
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
i	i	k9	i
trnitými	trnitý	k2eAgFnPc7d1	trnitá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ožírají	ožírat	k5eAaImIp3nP	ožírat
kosti	kost	k1gFnPc4	kost
a	a	k8xC	a
vyschlé	vyschlý	k2eAgFnPc4d1	vyschlá
zdechliny	zdechlina	k1gFnPc4	zdechlina
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
žít	žít	k5eAaImF	žít
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
řídkým	řídký	k2eAgInSc7d1	řídký
<g/>
,	,	kIx,	,
tuhým	tuhý	k2eAgInSc7d1	tuhý
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
stádech	stádo	k1gNnPc6	stádo
tvořených	tvořený	k2eAgNnPc6d1	tvořené
z	z	k7c2	z
několika	několik	k4yIc2	několik
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
samce	samec	k1gMnSc2	samec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stádo	stádo	k1gNnSc4	stádo
ochraňuje	ochraňovat	k5eAaImIp3nS	ochraňovat
pliváním	plivání	k1gNnSc7	plivání
a	a	k8xC	a
kousáním	kousání	k1gNnSc7	kousání
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
dromedárů	dromedár	k1gMnPc2	dromedár
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
-	-	kIx~	-
v	v	k7c6	v
období	období	k1gNnSc6	období
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
samci	samec	k1gMnPc1	samec
dosti	dosti	k6eAd1	dosti
agresivní	agresivní	k2eAgMnSc1d1	agresivní
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
velkou	velký	k2eAgFnSc7d1	velká
výjimkou	výjimka	k1gFnSc7	výjimka
úmrtí	úmrť	k1gFnPc2	úmrť
protivníka	protivník	k1gMnSc2	protivník
na	na	k7c4	na
četná	četný	k2eAgNnPc4d1	četné
pokousání	pokousání	k1gNnPc4	pokousání
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudovití	velbloudovitý	k2eAgMnPc1d1	velbloudovitý
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nP	pářit
vleže	vleže	k6eAd1	vleže
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
samec	samec	k1gInSc1	samec
vystačí	vystačit	k5eAaBmIp3nS	vystačit
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
samic	samice	k1gFnPc2	samice
a	a	k8xC	a
páření	páření	k1gNnSc2	páření
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
vydává	vydávat	k5eAaPmIp3nS	vydávat
samec	samec	k1gInSc4	samec
velice	velice	k6eAd1	velice
hlasitý	hlasitý	k2eAgInSc4d1	hlasitý
řev	řev	k1gInSc4	řev
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
365	[number]	k4	365
až	až	k9	až
440	[number]	k4	440
dnech	den	k1gInPc6	den
březosti	březost	k1gFnSc2	březost
se	se	k3xPyFc4	se
samice	samice	k1gFnPc1	samice
oddělí	oddělit	k5eAaPmIp3nP	oddělit
od	od	k7c2	od
stáda	stádo	k1gNnSc2	stádo
a	a	k8xC	a
rodí	rodit	k5eAaImIp3nS	rodit
vstoje	vstoje	k6eAd1	vstoje
jediné	jediný	k2eAgNnSc1d1	jediné
mládě	mládě	k1gNnSc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
pohybu	pohyb	k1gInSc3	pohyb
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
připojí	připojit	k5eAaPmIp3nS	připojit
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
ke	k	k7c3	k
stádu	stádo	k1gNnSc3	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
mládě	mládě	k1gNnSc1	mládě
saje	sát	k5eAaImIp3nS	sát
téměř	téměř	k6eAd1	téměř
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
oždibuje	oždibovat	k5eAaImIp3nS	oždibovat
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
ve	v	k7c6	v
2	[number]	k4	2
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
pravidelně	pravidelně	k6eAd1	pravidelně
přikrmuje	přikrmovat	k5eAaImIp3nS	přikrmovat
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
mládětem	mládě	k1gNnSc7	mládě
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
pomocí	pomocí	k7c2	pomocí
hlasových	hlasový	k2eAgInPc2d1	hlasový
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
ztracené	ztracený	k2eAgNnSc1d1	ztracené
mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
naříkavým	naříkavý	k2eAgNnSc7d1	naříkavé
voláním	volání	k1gNnSc7	volání
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
již	již	k6eAd1	již
ve	v	k7c6	v
3	[number]	k4	3
až	až	k8xS	až
4	[number]	k4	4
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
