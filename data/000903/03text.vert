<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
(	(	kIx(	(
<g/>
lat	lat	k1gInSc1	lat
<g/>
.	.	kIx.	.
</s>
<s>
Bulla	bulla	k1gFnSc1	bulla
Aurea	Aurea	k1gFnSc1	Aurea
Siciliæ	Siciliæ	k1gFnSc1	Siciliæ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
tří	tři	k4xCgFnPc2	tři
navzájem	navzájem	k6eAd1	navzájem
provázaných	provázaný	k2eAgFnPc2d1	provázaná
listin	listina	k1gFnPc2	listina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc7	jejichž
význam	význam	k1gInSc1	význam
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
procházel	procházet	k5eAaImAgMnS	procházet
významnými	významný	k2eAgFnPc7d1	významná
proměnami	proměna	k1gFnPc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslu	Přemysl	k1gMnSc3	Přemysl
Otakarovi	Otakar	k1gMnSc3	Otakar
I.	I.	kA	I.
ji	on	k3xPp3gFnSc4	on
26.	[number]	k4	26.
září	září	k1gNnSc6	září
1212	[number]	k4	1212
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
budoucí	budoucí	k2eAgMnSc1d1	budoucí
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
mu	on	k3xPp3gMnSc3	on
Přemysl	Přemysl	k1gMnSc1	Přemysl
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
říšskou	říšský	k2eAgFnSc4d1	říšská
královskou	královský	k2eAgFnSc4d1	královská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
listiny	listina	k1gFnSc2	listina
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
listin	listina	k1gFnPc2	listina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
pečeti	pečeť	k1gFnSc2	pečeť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dokumentu	dokument	k1gInSc3	dokument
přivěšena	přivěšen	k2eAgFnSc1d1	přivěšena
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
král	král	k1gMnSc1	král
Sicílie	Sicílie	k1gFnSc2	Sicílie
disponoval	disponovat	k5eAaBmAgMnS	disponovat
tehdy	tehdy	k6eAd1	tehdy
pouze	pouze	k6eAd1	pouze
pečetí	pečetit	k5eAaImIp3nS	pečetit
tohoto	tento	k3xDgNnSc2	tento
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Říšskými	říšský	k2eAgFnPc7d1	říšská
insigniemi	insignie	k1gFnPc7	insignie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
říšským	říšský	k2eAgInSc7d1	říšský
typářem	typář	k1gInSc7	typář
<g/>
,	,	kIx,	,
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
vlastně	vlastně	k9	vlastně
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
ještě	ještě	k6eAd1	ještě
nedisponoval	disponovat	k5eNaBmAgMnS	disponovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
prvního	první	k4xOgMnSc2	první
privilegia	privilegium	k1gNnPc4	privilegium
==	==	k?	==
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
intitulace	intitulace	k1gFnSc1	intitulace
basilejského	basilejský	k2eAgNnSc2d1	Basilejské
privilegia	privilegium	k1gNnSc2	privilegium
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
interpretační	interpretační	k2eAgInSc1d1	interpretační
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
představuje	představovat	k5eAaImIp3nS	představovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
jako	jako	k9	jako
"	"	kIx"	"
<g/>
imperator	imperator	k1gMnSc1	imperator
electus	electus	k1gMnSc1	electus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
překlad	překlad	k1gInSc1	překlad
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
zvolený	zvolený	k2eAgMnSc1d1	zvolený
císař	císař	k1gMnSc1	císař
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nesprávný	správný	k2eNgMnSc1d1	nesprávný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
nebyl	být	k5eNaImAgMnS	být
ještě	ještě	k6eAd1	ještě
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
využívala	využívat	k5eAaImAgFnS	využívat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
římská	římský	k2eAgFnSc1d1	římská
kurie	kurie	k1gFnSc1	kurie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tím	ten	k3xDgNnSc7	ten
designovala	designovat	k5eAaPmAgFnS	designovat
svého	svůj	k3xOyFgMnSc4	svůj
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
říšský	říšský	k2eAgInSc4d1	říšský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
spíše	spíše	k9	spíše
překládat	překládat	k5eAaImF	překládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vyvolený	vyvolený	k2eAgMnSc1d1	vyvolený
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
budoucí	budoucí	k2eAgMnSc1d1	budoucí
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Sicilský	sicilský	k2eAgMnSc1d1	sicilský
král	král	k1gMnSc1	král
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Přemyslovci	Přemyslovec	k1gMnSc3	Přemyslovec
Přemyslu	Přemysl	k1gMnSc3	Přemysl
Otakarovi	Otakar	k1gMnSc3	Otakar
I.	I.	kA	I.
královskou	královský	k2eAgFnSc4d1	královská
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
vděčnost	vděčnost	k1gFnSc1	vděčnost
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
Fridrichovi	Fridrichův	k2eAgMnPc1d1	Fridrichův
II	II	kA	II
<g/>
.	.	kIx.	.
při	při	k7c6	při
zvolení	zvolení	k1gNnSc4	zvolení
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Privilegium	privilegium	k1gNnSc1	privilegium
zřejmě	zřejmě	k6eAd1	zřejmě
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
starší	starý	k2eAgFnPc4d2	starší
výsady	výsada	k1gFnPc4	výsada
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
Filipa	Filip	k1gMnSc2	Filip
Švábského	švábský	k2eAgMnSc2d1	švábský
a	a	k8xC	a
Oty	Ota	k1gMnSc2	Ota
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Brunšvického	brunšvický	k2eAgMnSc4d1	brunšvický
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
je	být	k5eAaImIp3nS	být
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
jen	jen	k9	jen
první	první	k4xOgMnSc1	první
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pochopitelné	pochopitelný	k2eAgNnSc1d1	pochopitelné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1212	[number]	k4	1212
bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
připomínat	připomínat	k5eAaImF	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Přemyslovec	Přemyslovec	k1gMnSc1	Přemyslovec
kdysi	kdysi	k6eAd1	kdysi
podporoval	podporovat	k5eAaImAgMnS	podporovat
welfskou	welfský	k2eAgFnSc4d1	welfský
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
soupeřící	soupeřící	k2eAgFnSc4d1	soupeřící
se	s	k7c7	s
Štaufy	Štauf	k1gInPc7	Štauf
o	o	k7c4	o
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedinečnost	jedinečnost	k1gFnSc1	jedinečnost
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnSc2d1	sicilská
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
spatřována	spatřován	k2eAgFnSc1d1	spatřována
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
českému	český	k2eAgMnSc3d1	český
panovníkovi	panovník	k1gMnSc3	panovník
byla	být	k5eAaImAgFnS	být
královská	královský	k2eAgFnSc1d1	královská
hodnost	hodnost	k1gFnSc1	hodnost
přiznána	přiznán	k2eAgFnSc1d1	přiznána
poprvé	poprvé	k6eAd1	poprvé
dědičně	dědičně	k6eAd1	dědičně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
privilegium	privilegium	k1gNnSc1	privilegium
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1158	[number]	k4	1158
pro	pro	k7c4	pro
knížete	kníže	k1gNnSc4wR	kníže
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pasáž	pasáž	k1gFnSc1	pasáž
o	o	k7c4	o
dědicích	dědic	k1gMnPc6	dědic
královské	královský	k2eAgFnSc6d1	královská
hodnosti	hodnost	k1gFnSc6	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
musel	muset	k5eAaImAgMnS	muset
o	o	k7c4	o
dědičnost	dědičnost	k1gFnSc4	dědičnost
svého	svůj	k3xOyFgNnSc2	svůj
království	království	k1gNnSc2	království
ještě	ještě	k6eAd1	ještě
svést	svést	k5eAaPmF	svést
tuhý	tuhý	k2eAgInSc4d1	tuhý
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
dále	daleko	k6eAd2	daleko
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
udělí	udělit	k5eAaPmIp3nP	udělit
odznaky	odznak	k1gInPc4	odznak
královské	královský	k2eAgInPc4d1	královský
moci	moct	k5eAaImF	moct
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
bude	být	k5eAaImBp3nS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
právo	právo	k1gNnSc1	právo
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
na	na	k7c4	na
volbu	volba	k1gFnSc4	volba
svého	svůj	k3xOyFgMnSc2	svůj
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
tento	tento	k3xDgInSc4	tento
nárok	nárok	k1gInSc4	nárok
šlechty	šlechta	k1gFnSc2	šlechta
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k6eAd1	rovněž
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
právo	právo	k1gNnSc1	právo
investitury	investitura	k1gFnSc2	investitura
pražských	pražský	k2eAgMnPc2d1	pražský
a	a	k8xC	a
olomouckých	olomoucký	k2eAgMnPc2d1	olomoucký
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
však	však	k9	však
všechny	všechen	k3xTgFnPc4	všechen
svobody	svoboda	k1gFnPc4	svoboda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
biskupové	biskup	k1gMnPc1	biskup
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
od	od	k7c2	od
předchozích	předchozí	k2eAgMnPc2d1	předchozí
římských	římský	k2eAgMnPc2d1	římský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
zachovány	zachovat	k5eAaPmNgInP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
privilegium	privilegium	k1gNnSc4	privilegium
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
Fridrich	Fridrich	k1gMnSc1	Fridrich
I.	I.	kA	I.
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
uznal	uznat	k5eAaPmAgMnS	uznat
pražského	pražský	k2eAgInSc2d1	pražský
biskupa	biskup	k1gInSc2	biskup
za	za	k7c2	za
říšského	říšský	k2eAgMnSc2d1	říšský
knížete	kníže	k1gMnSc2	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
panovník	panovník	k1gMnSc1	panovník
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
napříště	napříště	k6eAd1	napříště
osvobozen	osvobozen	k2eAgMnSc1d1	osvobozen
od	od	k7c2	od
všech	všecek	k3xTgFnPc2	všecek
povinností	povinnost	k1gFnPc2	povinnost
vůči	vůči	k7c3	vůči
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
říšských	říšský	k2eAgInPc6d1	říšský
sněmech	sněm	k1gInPc6	sněm
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
Merseburku	Merseburk	k1gInSc6	Merseburk
<g/>
,	,	kIx,	,
Norimberku	Norimberk	k1gInSc6	Norimberk
nebo	nebo	k8xC	nebo
Bamberku	Bamberk	k1gInSc6	Bamberk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nedaleko	nedaleko	k7c2	nedaleko
území	území	k1gNnSc2	území
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
doprovodu	doprovod	k1gInSc2	doprovod
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
korunovační	korunovační	k2eAgFnSc6d1	korunovační
jízdě	jízda	k1gFnSc6	jízda
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
povinen	povinen	k2eAgMnSc1d1	povinen
vypravit	vypravit	k5eAaPmF	vypravit
300	[number]	k4	300
jezdců	jezdec	k1gMnPc2	jezdec
nebo	nebo	k8xC	nebo
zaplatit	zaplatit	k5eAaPmF	zaplatit
300	[number]	k4	300
hřiven	hřivna	k1gFnPc2	hřivna
stříbra	stříbro	k1gNnSc2	stříbro
(	(	kIx(	(
<g/>
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
vypravení	vypravení	k1gNnSc4	vypravení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
větě	věta	k1gFnSc3	věta
o	o	k7c6	o
návštěvě	návštěva	k1gFnSc6	návštěva
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
Merseburku	Merseburk	k1gInSc6	Merseburk
jsou	být	k5eAaImIp3nP	být
připojena	připojen	k2eAgNnPc1d1	připojeno
nejasná	jasný	k2eNgNnPc1d1	nejasné
slova	slovo	k1gNnPc1	slovo
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
kníže	kníže	k1gMnSc1	kníže
polský	polský	k2eAgMnSc1d1	polský
přijal	přijmout	k5eAaPmAgInS	přijmout
pozvání	pozvání	k1gNnSc4	pozvání
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
mu	on	k3xPp3gInSc3	on
dát	dát	k5eAaPmF	dát
průvod	průvod	k1gInSc4	průvod
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
někdy	někdy	k6eAd1	někdy
jejich	jejich	k3xOp3gMnPc1	jejich
předchůdcové	předchůdce	k1gMnPc1	předchůdce
<g/>
,	,	kIx,	,
králové	král	k1gMnPc1	král
čeští	český	k2eAgMnPc1d1	český
činívali	činívat	k5eAaImAgMnP	činívat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
věta	věta	k1gFnSc1	věta
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
celého	celý	k2eAgNnSc2d1	celé
privilegia	privilegium	k1gNnSc2	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgFnPc1d1	polská
záležitosti	záležitost	k1gFnPc1	záležitost
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
vždy	vždy	k6eAd1	vždy
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
prolínaly	prolínat	k5eAaImAgFnP	prolínat
s	s	k7c7	s
osudy	osud	k1gInPc4	osud
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Vratislav	Vratislav	k1gMnSc1	Vratislav
byl	být	k5eAaImAgMnS	být
korunován	korunovat	k5eAaBmNgMnS	korunovat
i	i	k9	i
na	na	k7c4	na
krále	král	k1gMnSc4	král
polského	polský	k2eAgMnSc4d1	polský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korunovačním	korunovační	k2eAgNnSc6d1	korunovační
privilegiu	privilegium	k1gNnSc6	privilegium
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1158	[number]	k4	1158
se	se	k3xPyFc4	se
dočteme	dočíst	k5eAaPmIp1nP	dočíst
o	o	k7c6	o
polském	polský	k2eAgInSc6d1	polský
censu	census	k1gInSc6	census
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vyplácen	vyplácet	k5eAaImNgInS	vyplácet
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tato	tento	k3xDgFnSc1	tento
věta	věta	k1gFnSc1	věta
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
zmateným	zmatený	k2eAgInSc7d1	zmatený
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
historické	historický	k2eAgFnSc2d1	historická
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
či	či	k8xC	či
zda	zda	k8xS	zda
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
chtěl	chtít	k5eAaImAgMnS	chtít
roli	role	k1gFnSc3	role
přímluvce	přímluvce	k1gMnSc2	přímluvce
polského	polský	k2eAgMnSc2d1	polský
knížete	kníže	k1gMnSc2	kníže
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
dvoře	dvůr	k1gInSc6	dvůr
konkrétně	konkrétně	k6eAd1	konkrétně
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
garantovala	garantovat	k5eAaBmAgFnS	garantovat
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
nezávislost	nezávislost	k1gFnSc1	nezávislost
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gNnSc7	jeho
formálním	formální	k2eAgNnSc7d1	formální
začleněním	začlenění	k1gNnSc7	začlenění
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
nabízela	nabízet	k5eAaImAgFnS	nabízet
českým	český	k2eAgMnPc3d1	český
králům	král	k1gMnPc3	král
jako	jako	k8xS	jako
říšským	říšský	k2eAgMnPc3d1	říšský
knížatům	kníže	k1gMnPc3wR	kníže
nové	nový	k2eAgFnSc2d1	nová
možnosti	možnost	k1gFnSc2	možnost
aktivní	aktivní	k2eAgFnSc2d1	aktivní
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Přispěla	přispět	k5eAaPmAgFnS	přispět
tak	tak	k9	tak
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
prestiže	prestiž	k1gFnSc2	prestiž
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k9	i
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
pozic	pozice	k1gFnPc2	pozice
jeho	jeho	k3xOp3gMnPc2	jeho
panovníků	panovník	k1gMnPc2	panovník
jak	jak	k8xS	jak
mezi	mezi	k7c7	mezi
evropskými	evropský	k2eAgMnPc7d1	evropský
vládci	vládce	k1gMnPc7	vládce
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c6	na
domácím	domácí	k2eAgNnSc6d1	domácí
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
současní	současný	k2eAgMnPc1d1	současný
badatelé	badatel	k1gMnPc1	badatel
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
kloní	klonit	k5eAaImIp3nS	klonit
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
význam	význam	k1gInSc1	význam
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnSc2d1	sicilská
byl	být	k5eAaImAgInS	být
nadhodnocen	nadhodnotit	k5eAaPmNgInS	nadhodnotit
v	v	k7c6	v
19.	[number]	k4	19.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k8xC	jako
právní	právní	k2eAgInSc1d1	právní
doklad	doklad	k1gInSc1	doklad
o	o	k7c6	o
historickém	historický	k2eAgInSc6d1	historický
nároku	nárok	k1gInSc6	nárok
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
státnost	státnost	k1gFnSc4	státnost
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
interpretace	interpretace	k1gFnSc1	interpretace
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
učebnic	učebnice	k1gFnPc2	učebnice
a	a	k8xC	a
do	do	k7c2	do
veřejného	veřejný	k2eAgNnSc2d1	veřejné
povědomí	povědomí	k1gNnSc2	povědomí
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
stejně	stejně	k6eAd1	stejně
důležitými	důležitý	k2eAgFnPc7d1	důležitá
listinami	listina	k1gFnPc7	listina
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
potvrzení	potvrzení	k1gNnSc4	potvrzení
darování	darování	k1gNnSc4	darování
královské	královský	k2eAgFnSc2d1	královská
hodnosti	hodnost	k1gFnSc2	hodnost
Filipem	Filip	k1gMnSc7	Filip
Švábským	švábský	k2eAgMnSc7d1	švábský
a	a	k8xC	a
Otou	Ota	k1gMnSc7	Ota
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Brunšvickým	brunšvický	k2eAgInSc7d1	brunšvický
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
budování	budování	k1gNnSc4	budování
důležitosti	důležitost	k1gFnSc2	důležitost
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnPc1d1	sicilská
byly	být	k5eAaImAgFnP	být
údajně	údajně	k6eAd1	údajně
její	její	k3xOp3gInSc4	její
majestátní	majestátní	k2eAgInSc4d1	majestátní
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
důležité	důležitý	k2eAgNnSc4d1	důležité
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
panovnické	panovnický	k2eAgFnSc6d1	panovnická
koncepci	koncepce	k1gFnSc6	koncepce
určil	určit	k5eAaPmAgMnS	určit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
snažil	snažit	k5eAaImAgMnS	snažit
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
starší	starý	k2eAgFnSc4d2	starší
domácí	domácí	k2eAgFnSc4d1	domácí
přemyslovskou	přemyslovský	k2eAgFnSc4d1	Přemyslovská
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právní	právní	k2eAgInSc1d1	právní
význam	význam	k1gInSc1	význam
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnSc2d1	sicilská
klesl	klesnout	k5eAaPmAgMnS	klesnout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1216	[number]	k4	1216
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
tzv.	tzv.	kA	tzv.
Zlatou	Zlata	k1gFnSc7	Zlata
bulou	bula	k1gFnSc7	bula
ulmskou	ulmska	k1gFnSc7	ulmska
a	a	k8xC	a
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
ji	on	k3xPp3gFnSc4	on
ani	ani	k9	ani
tzv.	tzv.	kA	tzv.
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
melfská	melfský	k2eAgFnSc1d1	melfský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1231	[number]	k4	1231
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
intronizaci	intronizace	k1gFnSc4	intronizace
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Originál	originál	k1gInSc1	originál
listiny	listina	k1gFnSc2	listina
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
ve	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
archivu	archiv	k1gInSc6	archiv
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
fond	fond	k1gInSc1	fond
"	"	kIx"	"
<g/>
Archiv	archiv	k1gInSc1	archiv
České	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
inv	inv	k?	inv
<g/>
.	.	kIx.	.
č	č	k0	č
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
text	text	k1gInSc1	text
vydal	vydat	k5eAaPmAgMnS	vydat
G.	G.	kA	G.
Friedrich	Friedrich	k1gMnSc1	Friedrich
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
<g/>
:	:	kIx,	:
Codex	Codex	k1gInSc1	Codex
diplomaticus	diplomaticus	k1gInSc1	diplomaticus
et	et	k?	et
epistolaris	epistolaris	k1gInSc4	epistolaris
regni	regeň	k1gFnSc3	regeň
Bohemiae	Bohemiae	k1gInSc1	Bohemiae
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
.	.	kIx.	.
96	[number]	k4	96
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
93-94	[number]	k4	93-94
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
dokumenty	dokument	k1gInPc1	dokument
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojené	spojený	k2eAgInPc1d1	spojený
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc4	tři
listiny	listina	k1gFnPc4	listina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
pro	pro	k7c4	pro
(	(	kIx(	(
<g/>
budoucího	budoucí	k2eAgMnSc4d1	budoucí
císaře	císař	k1gMnSc4	císař
<g/>
)	)	kIx)	)
Fridricha	Fridrich	k1gMnSc4	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
sepsal	sepsat	k5eAaPmAgMnS	sepsat
notář	notář	k1gMnSc1	notář
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Pairis	Pairis	k1gFnSc2	Pairis
a	a	k8xC	a
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
logický	logický	k2eAgInSc4d1	logický
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Listina	listina	k1gFnSc1	listina
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
adresována	adresován	k2eAgFnSc1d1	adresována
Přemyslu	Přemysl	k1gMnSc3	Přemysl
Otakaru	Otakar	k1gMnSc3	Otakar
I.	I.	kA	I.
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
jeho	jeho	k3xOp3gMnSc3	jeho
bratrovi	bratr	k1gMnSc3	bratr
Vladislavu	Vladislav	k1gMnSc3	Vladislav
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
<g/>
,	,	kIx,	,
moravskému	moravský	k2eAgMnSc3d1	moravský
markraběti	markrabě	k1gMnSc3	markrabě
<g/>
.	.	kIx.	.
</s>
<s>
Obsahem	obsah	k1gInSc7	obsah
obou	dva	k4xCgFnPc6	dva
je	být	k5eAaImIp3nS	být
věnování	věnování	k1gNnSc1	věnování
statků	statek	k1gInPc2	statek
a	a	k8xC	a
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Nejasná	jasný	k2eNgFnSc1d1	nejasná
formulace	formulace	k1gFnSc1	formulace
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
listině	listina	k1gFnSc6	listina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
věnováno	věnovat	k5eAaPmNgNnS	věnovat
a	a	k8xC	a
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
zboží	zboží	k1gNnSc1	zboží
"	"	kIx"	"
<g/>
Mocran	Mocran	k1gInSc1	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
<g/>
"	"	kIx"	"
se	s	k7c7	s
všemi	všecek	k3xTgNnPc7	všecek
právy	právo	k1gNnPc7	právo
a	a	k8xC	a
příslušenstvím	příslušenství	k1gNnSc7	příslušenství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
záhadou	záhada	k1gFnSc7	záhada
českého	český	k2eAgInSc2d1	český
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
badatelů	badatel	k1gMnPc2	badatel
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
Mocran	Mocran	k1gInSc4	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
jako	jako	k8xS	jako
zahraniční	zahraniční	k2eAgNnSc1d1	zahraniční
léno	léno	k1gNnSc1	léno
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
slovní	slovní	k2eAgNnSc1d1	slovní
spojení	spojení	k1gNnPc1	spojení
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
jako	jako	k9	jako
chybu	chyba	k1gFnSc4	chyba
notáře	notář	k1gMnSc2	notář
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
neznalého	znalý	k2eNgNnSc2d1	neznalé
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
vidí	vidět	k5eAaImIp3nS	vidět
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
markrabství	markrabství	k1gNnSc1	markrabství
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
příp.	příp.	kA	příp.
"	"	kIx"	"
<g/>
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
totiž	totiž	k9	totiž
Přemysl	Přemysl	k1gMnSc1	Přemysl
spravuje	spravovat	k5eAaImIp3nS	spravovat
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
Vladislav	Vladislav	k1gMnSc1	Vladislav
moravskou	moravský	k2eAgFnSc4d1	Moravská
část	část	k1gFnSc4	část
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
oficiálním	oficiální	k2eAgInSc7d1	oficiální
centrem	centr	k1gInSc7	centr
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
otazníky	otazník	k1gInPc1	otazník
ohledně	ohledně	k7c2	ohledně
souboru	soubor	k1gInSc2	soubor
tří	tři	k4xCgFnPc2	tři
listin	listina	k1gFnPc2	listina
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
možná	možná	k9	možná
řešení	řešení	k1gNnSc3	řešení
nastínila	nastínit	k5eAaPmAgFnS	nastínit
kniha	kniha	k1gFnSc1	kniha
Martina	Martin	k1gMnSc2	Martin
Wihody	Wihoda	k1gFnSc2	Wihoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozproudila	rozproudit	k5eAaPmAgFnS	rozproudit
živou	živý	k2eAgFnSc4d1	živá
výměnu	výměna	k1gFnSc4	výměna
názorů	názor	k1gInPc2	názor
mezi	mezi	k7c7	mezi
českými	český	k2eAgInPc7d1	český
a	a	k8xC	a
moravskými	moravský	k2eAgInPc7d1	moravský
medievisty	medievist	k1gInPc7	medievist
<g/>
.	.	kIx.	.
</s>
<s>
Diskuze	diskuze	k1gFnSc1	diskuze
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21.	[number]	k4	21.
století	století	k1gNnSc2	století
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
a	a	k8xC	a
nezdá	zdát	k5eNaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zatím	zatím	k6eAd1	zatím
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
nějakým	nějaký	k3yIgInPc3	nějaký
konkrétnějším	konkrétní	k2eAgInPc3d2	konkrétnější
závěrům	závěr	k1gInPc3	závěr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BRETHOLZ	BRETHOLZ	kA	BRETHOLZ
<g/>
,	,	kIx,	,
Berthold	Berthold	k1gMnSc1	Berthold
<g/>
.	.	kIx.	.
</s>
<s>
Mocran	Mocran	k1gInSc1	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
<g/>
.	.	kIx.	.
</s>
<s>
Zur	Zur	k?	Zur
Kritik	kritik	k1gMnSc1	kritik
der	drát	k5eAaImRp2nS	drát
goldenen	goldenen	k1gInSc4	goldenen
Bulle	bulla	k1gFnSc3	bulla
König	König	k1gInSc1	König
Friedrichs	Friedrichs	k1gInSc4	Friedrichs
II	II	kA	II
<g/>
.	.	kIx.	.
für	für	k?	für
Mähren	Mährno	k1gNnPc2	Mährno
vom	vom	k?	vom
Jahre	Jahr	k1gInSc5	Jahr
1212	[number]	k4	1212
<g/>
.	.	kIx.	.
</s>
<s>
Zeitschrift	Zeitschrift	k1gMnSc1	Zeitschrift
des	des	k1gNnSc2	des
Vereines	Vereines	k1gMnSc1	Vereines
für	für	k?	für
die	die	k?	die
Geschichte	Geschicht	k1gInSc5	Geschicht
Mährens	Mährens	k1gInSc4	Mährens
und	und	k?	und
Schlesiens	Schlesiens	k1gInSc1	Schlesiens
<g/>
.	.	kIx.	.
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
305-320	[number]	k4	305-320
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
bule	bula	k1gFnSc6	bula
sicilské	sicilský	k2eAgFnSc6d1	sicilská
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
1-3	[number]	k4	1-3
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0418-5129	[number]	k4	0418-5129
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Goll	Goll	k1gMnSc1	Goll
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
:	:	kIx,	:
K	k	k7c3	k
výkladu	výklad	k1gInSc3	výklad
privilegia	privilegium	k1gNnSc2	privilegium
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
království	království	k1gNnSc4	království
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
Věstník	věstník	k1gMnSc1	věstník
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
třída	třída	k1gFnSc1	třída
filosoficko-historická	filosofickoistorický	k2eAgFnSc1d1	filosoficko-historický
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
1-9	[number]	k4	1-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HLAVÁČEK	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
–	–	k?	–
a	a	k8xC	a
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
možná	možná	k9	možná
ne	ne	k9	ne
naposledy	naposledy	k6eAd1	naposledy
–	–	k?	–
Mocran	Mocran	k1gInSc1	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
POLÍVKA	Polívka	k1gMnSc1	Polívka
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
;	;	kIx,	;
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
memoriam	memoriam	k1gInSc1	memoriam
Josefa	Josef	k1gMnSc2	Josef
Macka	Macek	k1gMnSc2	Macek
(	(	kIx(	(
<g/>
1922-1991	[number]	k4	1922-1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85268-64-7	[number]	k4	80-85268-64-7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
45-50	[number]	k4	45-50
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HLAVÁČEK	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Mocran	Mocran	k1gInSc1	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
<g/>
.	.	kIx.	.
</s>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
diskusní	diskusní	k2eAgInSc1d1	diskusní
epilog	epilog	k1gInSc1	epilog
Ivana	Ivan	k1gMnSc2	Ivan
Hlaváčka	Hlaváček	k1gMnSc2	Hlaváček
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Matice	matice	k1gFnSc2	matice
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
126	[number]	k4	126
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
369-370	[number]	k4	369-370
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0323-052X	[number]	k4	0323-052X
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HRŮZA	Hrůza	k1gMnSc1	Hrůza
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hruza	Hruza	k1gFnSc1	Hruza
neví	vědět	k5eNaImIp3nS	vědět
či	či	k8xC	či
nechce	chtít	k5eNaImIp3nS	chtít
vědět	vědět	k5eAaImF	vědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Několik	několik	k4yIc4	několik
poznámek	poznámka	k1gFnPc2	poznámka
k	k	k7c3	k
diskusi	diskuse	k1gFnSc3	diskuse
nad	nad	k7c7	nad
souborem	soubor	k1gInSc7	soubor
basilejských	basilejský	k2eAgFnPc2d1	Basilejská
listin	listina	k1gFnPc2	listina
z	z	k7c2	z
26.	[number]	k4	26.
září	září	k1gNnSc2	září
1212	[number]	k4	1212
a	a	k8xC	a
charakterem	charakter	k1gInSc7	charakter
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Matice	matice	k1gFnSc2	matice
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
126	[number]	k4	126
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
371-376	[number]	k4	371-376
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0323-052X	[number]	k4	0323-052X
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSS	KOSS	kA	KOSS
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Mocran	Mocran	k1gInSc1	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
českého	český	k2eAgInSc2d1	český
zemského	zemský	k2eAgInSc2d1	zemský
archivu	archiv	k1gInSc2	archiv
<g/>
.	.	kIx.	.
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1803-2532	[number]	k4	1803-2532
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PALACKÝ	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
národu	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
v	v	k7c6	v
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Od	od	k7c2	od
prvověkosti	prvověkost	k1gFnSc2	prvověkost
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1253.	[number]	k4	1253.
4.	[number]	k4	4.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Bursík	Bursík	k1gMnSc1	Bursík
&	&	k?	&
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
1921.	[number]	k4	1921.
554	[number]	k4	554
s	s	k7c7	s
<g/>
.	.	kIx.	.
S.	S.	kA	S.
285.	[number]	k4	285.
</s>
</p>
<p>
<s>
ŠUSTA	Šusta	k1gMnSc1	Šusta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
koruna	koruna	k1gFnSc1	koruna
polská	polský	k2eAgFnSc1d1	polská
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
1915	[number]	k4	1915
ročník	ročník	k1gInSc1	ročník
=	=	kIx~	=
21	[number]	k4	21
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
313-346	[number]	k4	313-346
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0862-6111	[number]	k4	0862-6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
dornige	dornige	k6eAd1	dornige
Weg	Weg	k1gMnSc2	Weg
zur	zur	k?	zur
"	"	kIx"	"
<g/>
Goldenen	Goldenen	k1gInSc1	Goldenen
Bulle	bulla	k1gFnSc3	bulla
<g/>
"	"	kIx"	"
von	von	k1gInSc1	von
1212	[number]	k4	1212
für	für	k?	für
Markgraf	Markgraf	k1gMnSc1	Markgraf
Vladislav	Vladislav	k1gMnSc1	Vladislav
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Mähren	Mährna	k1gFnPc2	Mährna
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
HRUZA	HRUZA	kA	HRUZA
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
HEROLD	herold	k1gMnSc1	herold
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
A.	A.	kA	A.
Wege	Weg	k1gFnPc1	Weg
zur	zur	k?	zur
Urkunde	Urkund	k1gMnSc5	Urkund
<g/>
,	,	kIx,	,
Wege	Wegus	k1gMnSc5	Wegus
der	drát	k5eAaImRp2nS	drát
Urkunde	Urkund	k1gMnSc5	Urkund
<g/>
,	,	kIx,	,
Wege	Wegus	k1gMnSc5	Wegus
der	drát	k5eAaImRp2nS	drát
Forschung	Forschung	k1gInSc1	Forschung
:	:	kIx,	:
Beiträge	Beiträge	k1gNnSc1	Beiträge
zur	zur	k?	zur
europäischen	europäischen	k2eAgMnSc1d1	europäischen
Diplomatik	diplomatik	k1gMnSc1	diplomatik
des	des	k1gNnSc2	des
Mittelalters	Mittelaltersa	k1gFnPc2	Mittelaltersa
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gNnSc1	Wien
<g/>
:	:	kIx,	:
Böhlau	Böhlaus	k1gInSc2	Böhlaus
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-205-77271-7	[number]	k4	3-205-77271-7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
65-79	[number]	k4	65-79
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Imperator	Imperator	k1gMnSc1	Imperator
electus	electus	k1gMnSc1	electus
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
NODL	NODL	kA	NODL
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Verba	verbum	k1gNnSc2	verbum
in	in	k?	in
imaginibus	imaginibus	k1gMnSc1	imaginibus
<g/>
.	.	kIx.	.
</s>
<s>
Františku	František	k1gMnSc3	František
Šmahelovi	Šmahel	k1gMnSc3	Šmahel
k	k	k7c3	k
70.	[number]	k4	70.
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7203-605	[number]	k4	80-7203-605
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
299	[number]	k4	299
<g/>
–	–	k?	–
<g/>
307	[number]	k4	307
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Mocran	Mocran	k1gInSc1	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
92	[number]	k4	92
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
213-226	[number]	k4	213-226
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0862-6111	[number]	k4	0862-6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Mocran	Mocran	k1gInSc1	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
jako	jako	k8xS	jako
prostor	prostor	k1gInSc1	prostor
k	k	k7c3	k
zamýšlení	zamýšlení	k1gNnSc3	zamýšlení
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Matice	matice	k1gFnSc2	matice
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
126	[number]	k4	126
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
377-410	[number]	k4	377-410
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0323-052X	[number]	k4	0323-052X
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
...	...	k?	...
nec	nec	k?	nec
petiuimus	petiuimus	k1gMnSc1	petiuimus
nec	nec	k?	nec
habemus	habemus	k1gMnSc1	habemus
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
volbě	volba	k1gFnSc6	volba
roku	rok	k1gInSc2	rok
1306	[number]	k4	1306
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
BOROVSKÝ	Borovský	k1gMnSc1	Borovský
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
;	;	kIx,	;
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
;	;	kIx,	;
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Ad	ad	k7c4	ad
vitam	vitam	k6eAd1	vitam
et	et	k?	et
honorem	honor	k1gInSc7	honor
:	:	kIx,	:
profesoru	profesor	k1gMnSc3	profesor
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
Mezníkovi	Mezníkův	k2eAgMnPc1d1	Mezníkův
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
žáci	žák	k1gMnPc1	žák
k	k	k7c3	k
pětasedmdesátým	pětasedmdesátý	k4xOgFnPc3	pětasedmdesátý
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86488-13-6	[number]	k4	80-86488-13-6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
261-272	[number]	k4	261-272
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
koruna	koruna	k1gFnSc1	koruna
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
102	[number]	k4	102
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
721-744	[number]	k4	721-744
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0862-6111	[number]	k4	0862-6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
:	:	kIx,	:
podivuhodný	podivuhodný	k2eAgInSc1d1	podivuhodný
příběh	příběh	k1gInSc1	příběh
ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2005.	[number]	k4	2005.
316	[number]	k4	316
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7203-682-3	[number]	k4	80-7203-682-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Mocran	Mocran	k1gInSc1	Mocran
et	et	k?	et
Mocran	Mocran	k1gInSc1	Mocran
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
basilejská	basilejský	k2eAgFnSc1d1	Basilejská
listina	listina	k1gFnSc1	listina
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnSc2d1	sicilská
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
104	[number]	k4	104
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
733-781	[number]	k4	733-781
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0862-6111	[number]	k4	0862-6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Mocren	Mocrna	k1gFnPc2	Mocrna
<g/>
,	,	kIx,	,
Mogkran	Mogkrana	k1gFnPc2	Mogkrana
<g/>
,	,	kIx,	,
Muckern	Muckerna	k1gFnPc2	Muckerna
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
hledat	hledat	k5eAaImF	hledat
říšský	říšský	k2eAgInSc4d1	říšský
majetek	majetek	k1gInSc4	majetek
Mocran	Mocrana	k1gFnPc2	Mocrana
et	et	k?	et
Mocran	Mocrana	k1gFnPc2	Mocrana
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
105	[number]	k4	105
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
305-350	[number]	k4	305-350
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0862-6111	[number]	k4	0862-6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Österreich	Österreich	k1gInSc1	Österreich
und	und	k?	und
Böhmen	Böhmen	k1gInSc1	Böhmen
1156-1212	[number]	k4	1156-1212
:	:	kIx,	:
Versuch	Versuch	k1gMnSc1	Versuch
eines	eines	k1gMnSc1	eines
historischen	historischen	k2eAgInSc4d1	historischen
Vergleich	Vergleich	k1gInSc4	Vergleich
des	des	k1gNnSc2	des
Privilegium	privilegium	k1gNnSc1	privilegium
minus	minus	k1gInSc1	minus
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
Goldenen	Goldenen	k1gInSc4	Goldenen
Bulle	bulla	k1gFnSc3	bulla
von	von	k1gInSc1	von
Sizilien	Sizilien	k2eAgMnSc1d1	Sizilien
<g/>
.	.	kIx.	.
</s>
<s>
Historica	Historica	k1gMnSc1	Historica
:	:	kIx,	:
historical	historicat	k5eAaPmAgMnS	historicat
sciences	sciences	k1gMnSc1	sciences
in	in	k?	in
the	the	k?	the
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
<g/>
.	.	kIx.	.
</s>
<s>
Series	Series	k1gInSc1	Series
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
47-74	[number]	k4	47-74
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210-8499	[number]	k4	1210-8499
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
Čech	Čechy	k1gFnPc2	Čechy
královských	královský	k2eAgInPc2d1	královský
1198-1253	[number]	k4	1198-1253
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002.	[number]	k4	2002.
964	[number]	k4	964
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7106-140-9	[number]	k4	80-7106-140-9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
109-110	[number]	k4	109-110
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1987.	[number]	k4	1987.
40	[number]	k4	40
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
-	-	kIx~	-
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
pojmu	pojem	k1gInSc2	pojem
</s>
</p>
<p>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Milána	Milán	k1gInSc2	Milán
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
ulmská	ulmskat	k5eAaPmIp3nS	ulmskat
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
melfská	melfský	k2eAgFnSc1d1	melfský
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Zlatá	zlatá	k1gFnSc1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
v	v	k7c6	v
digitálním	digitální	k2eAgInSc6d1	digitální
archivu	archiv	k1gInSc6	archiv
Monasterium	Monasterium	k1gNnSc1	Monasterium
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
</s>
</p>
<p>
<s>
Kritická	kritický	k2eAgFnSc1d1	kritická
edice	edice	k1gFnSc1	edice
privilegia	privilegium	k1gNnPc4	privilegium
v	v	k7c6	v
CDB	CDB	kA	CDB
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Digitální	digitální	k2eAgFnSc1d1	digitální
kopie	kopie	k1gFnSc1	kopie
prvního	první	k4xOgMnSc2	první
basilejského	basilejský	k2eAgMnSc2d1	basilejský
privilegia	privilegium	k1gNnPc4	privilegium
</s>
</p>
<p>
<s>
Digitální	digitální	k2eAgFnSc1d1	digitální
kopie	kopie	k1gFnSc1	kopie
druhého	druhý	k4xOgMnSc2	druhý
basilejského	basilejský	k2eAgMnSc2d1	basilejský
privilegia	privilegium	k1gNnPc4	privilegium
</s>
</p>
<p>
<s>
Digitální	digitální	k2eAgFnSc1d1	digitální
kopie	kopie	k1gFnSc1	kopie
třetího	třetí	k4xOgMnSc2	třetí
basilejského	basilejský	k2eAgMnSc2d1	basilejský
privilegia	privilegium	k1gNnPc4	privilegium
</s>
</p>
<p>
<s>
Znění	znění	k1gNnSc1	znění
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnSc2d1	sicilská
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
buly	bula	k1gFnSc2	bula
sicilské	sicilský	k2eAgFnSc2d1	sicilská
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Rakouše	Rakouš	k1gMnSc2	Rakouš
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
sicilská	sicilský	k2eAgFnSc1d1	sicilská
-	-	kIx~	-
listina	listina	k1gFnSc1	listina
z	z	k7c2	z
"	"	kIx"	"
<g/>
nejvzácnějších	vzácný	k2eAgMnPc2d3	nejvzácnější
<g/>
"	"	kIx"	"
</s>
</p>
