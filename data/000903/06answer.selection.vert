<s>
Přemyslu	Přemysl	k1gMnSc3	Přemysl
Otakarovi	Otakar	k1gMnSc3	Otakar
I.	I.	kA	I.
ji	on	k3xPp3gFnSc4	on
26.	[number]	k4	26.
září	září	k1gNnSc6	září
1212	[number]	k4	1212
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
budoucí	budoucí	k2eAgMnSc1d1	budoucí
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
mu	on	k3xPp3gMnSc3	on
Přemysl	Přemysl	k1gMnSc1	Přemysl
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
říšskou	říšský	k2eAgFnSc4d1	říšská
královskou	královský	k2eAgFnSc4d1	královská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
