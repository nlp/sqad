<p>
<s>
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
2	[number]	k4	2
ostrovy	ostrov	k1gInPc4	ostrov
při	při	k7c6	při
východoafrickém	východoafrický	k2eAgNnSc6d1	Východoafrické
pobřeží	pobřeží	k1gNnSc6	pobřeží
–	–	k?	–
jižní	jižní	k2eAgInSc1d1	jižní
ostrov	ostrov	k1gInSc1	ostrov
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
(	(	kIx(	(
<g/>
svahilsky	svahilsky	k6eAd1	svahilsky
Unguja	Unguj	k2eAgFnSc1d1	Unguj
<g/>
)	)	kIx)	)
a	a	k8xC	a
severní	severní	k2eAgInSc4d1	severní
ostrov	ostrov	k1gInSc4	ostrov
Pemba	Pemb	k1gMnSc2	Pemb
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
může	moct	k5eAaImIp3nS	moct
vztahovat	vztahovat	k5eAaImF	vztahovat
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Unguja	Unguj	k1gInSc2	Unguj
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Unguja	Unguj	k1gInSc2	Unguj
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
1	[number]	k4	1
658	[number]	k4	658
km2	km2	k4	km2
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Pemba	Pemba	k1gFnSc1	Pemba
984	[number]	k4	984
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
981	[number]	k4	981
754	[number]	k4	754
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
centrum	centrum	k1gNnSc4	centrum
je	být	k5eAaImIp3nS	být
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
autonomní	autonomní	k2eAgInSc1d1	autonomní
stát	stát	k5eAaPmF	stát
tvořící	tvořící	k2eAgFnSc4d1	tvořící
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tanganikou	Tanganika	k1gFnSc7	Tanganika
jednotný	jednotný	k2eAgInSc1d1	jednotný
stát	stát	k1gInSc1	stát
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
je	být	k5eAaImIp3nS	být
Jamhuri	Jamhuri	k1gNnSc1	Jamhuri
ya	ya	k?	ya
Muungano	Muungana	k1gFnSc5	Muungana
wa	wa	k?	wa
Tanzania-	Tanzania-	k1gFnSc2	Tanzania-
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
republika	republika	k1gFnSc1	republika
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
republika	republika	k1gFnSc1	republika
Tanganiky	Tanganika	k1gFnSc2	Tanganika
a	a	k8xC	a
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
je	být	k5eAaImIp3nS	být
složeninou	složenina	k1gFnSc7	složenina
slov	slovo	k1gNnPc2	slovo
Tanganika	Tanganika	k1gFnSc1	Tanganika
a	a	k8xC	a
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
aktivita	aktivita	k1gFnSc1	aktivita
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc4	pěstování
palmy	palma	k1gFnSc2	palma
Raffia	Raffium	k1gNnSc2	Raffium
a	a	k8xC	a
koření	koření	k1gNnSc2	koření
(	(	kIx(	(
<g/>
hřebíček	hřebíček	k1gInSc1	hřebíček
<g/>
,	,	kIx,	,
skořice	skořice	k1gFnSc1	skořice
<g/>
,	,	kIx,	,
muškátový	muškátový	k2eAgInSc1d1	muškátový
oříšek	oříšek	k1gInSc1	oříšek
a	a	k8xC	a
pepř	pepř	k1gInSc1	pepř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
turismus	turismus	k1gInSc1	turismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Příchod	příchod	k1gInSc1	příchod
Islámu	islám	k1gInSc2	islám
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k9	už
od	od	k7c2	od
přinejmenším	přinejmenším	k6eAd1	přinejmenším
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prozkoumávali	prozkoumávat	k5eAaImAgMnP	prozkoumávat
ostrov	ostrov	k1gInSc4	ostrov
arabští	arabský	k2eAgMnPc1d1	arabský
a	a	k8xC	a
perští	perský	k2eAgMnPc1d1	perský
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
pobřeží	pobřeží	k1gNnSc4	pobřeží
zandž	zandž	k6eAd1	zandž
a	a	k8xC	a
barr	barr	k1gInSc4	barr
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
Pobřeží	pobřeží	k1gNnSc1	pobřeží
černochů	černoch	k1gMnPc2	černoch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
převažujícím	převažující	k2eAgNnSc7d1	převažující
náboženstvím	náboženství	k1gNnSc7	náboženství
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
domluvy	domluva	k1gFnSc2	domluva
mezi	mezi	k7c7	mezi
arabskými	arabský	k2eAgMnPc7d1	arabský
obchodníky	obchodník	k1gMnPc7	obchodník
a	a	k8xC	a
domorodým	domorodý	k2eAgNnSc7d1	domorodé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
samovolnému	samovolný	k2eAgInSc3d1	samovolný
vzniku	vznik	k1gInSc3	vznik
nového	nový	k2eAgInSc2d1	nový
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgInPc4d1	silný
prvky	prvek	k1gInPc4	prvek
arabštiny	arabština	k1gFnSc2	arabština
<g/>
,	,	kIx,	,
perštiny	perština	k1gFnSc2	perština
či	či	k8xC	či
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
má	mít	k5eAaImIp3nS	mít
značně	značně	k6eAd1	značně
zjednodušenou	zjednodušený	k2eAgFnSc4d1	zjednodušená
strukturu	struktura	k1gFnSc4	struktura
afrických	africký	k2eAgInPc2d1	africký
bantuských	bantuský	k2eAgInPc2d1	bantuský
jazyků	jazyk	k1gInPc2	jazyk
–	–	k?	–
svahilštiny	svahilština	k1gFnPc4	svahilština
<g/>
.	.	kIx.	.
</s>
<s>
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
další	další	k2eAgFnSc7d1	další
zemí	zem	k1gFnSc7	zem
kromě	kromě	k7c2	kromě
Ománu	Omán	k1gInSc2	Omán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
majoritní	majoritní	k2eAgFnSc4d1	majoritní
větev	větev	k1gFnSc4	větev
islámu	islám	k1gInSc2	islám
zvaná	zvaný	k2eAgNnPc1d1	zvané
Ibádíja	Ibádíjum	k1gNnPc1	Ibádíjum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozkvět	rozkvět	k1gInSc1	rozkvět
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
a	a	k8xC	a
portugalská	portugalský	k2eAgFnSc1d1	portugalská
kolonizace	kolonizace	k1gFnSc1	kolonizace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založili	založit	k5eAaPmAgMnP	založit
Arabové	Arab	k1gMnPc1	Arab
svoji	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
"	"	kIx"	"
v	v	k7c6	v
africkém	africký	k2eAgInSc6d1	africký
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
časem	čas	k1gInSc7	čas
ve	v	k7c4	v
vzkvétající	vzkvétající	k2eAgFnSc4d1	vzkvétající
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1499	[number]	k4	1499
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Vasco	Vasco	k1gMnSc1	Vasco
da	da	k?	da
Gama	gama	k1gNnPc2	gama
<g/>
,	,	kIx,	,
nalezl	naleznout	k5eAaPmAgMnS	naleznout
zde	zde	k6eAd1	zde
krásná	krásný	k2eAgNnPc4d1	krásné
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
pulsoval	pulsovat	k5eAaImAgMnS	pulsovat
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1503	[number]	k4	1503
založili	založit	k5eAaPmAgMnP	založit
Portugalci	Portugalec	k1gMnPc1	Portugalec
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
obchodní	obchodní	k2eAgFnSc4d1	obchodní
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
poznali	poznat	k5eAaPmAgMnP	poznat
portugalské	portugalský	k2eAgInPc4d1	portugalský
zájmy	zájem	k1gInPc4	zájem
o	o	k7c4	o
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
tomuto	tento	k3xDgInSc3	tento
nátlaku	nátlak	k1gInSc3	nátlak
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jejich	jejich	k3xOp3gNnSc1	jejich
obchodování	obchodování	k1gNnSc1	obchodování
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
muslimské	muslimský	k2eAgNnSc1d1	muslimské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
majoritou	majorita	k1gFnSc7	majorita
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
svoje	své	k1gNnSc1	své
državy	država	k1gFnSc2	država
severně	severně	k6eAd1	severně
od	od	k7c2	od
Mosambiku	Mosambik	k1gInSc2	Mosambik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1698	[number]	k4	1698
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patřil	patřit	k5eAaImAgInS	patřit
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
nominálně	nominálně	k6eAd1	nominálně
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
ománského	ománský	k2eAgMnSc2d1	ománský
sultána	sultán	k1gMnSc2	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
centrem	centr	k1gMnSc7	centr
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
východoafrické	východoafrický	k2eAgNnSc4d1	Východoafrické
pobřeží	pobřeží	k1gNnSc4	pobřeží
(	(	kIx(	(
<g/>
ročně	ročně	k6eAd1	ročně
prošlo	projít	k5eAaPmAgNnS	projít
přes	přes	k7c4	přes
ostrov	ostrov	k1gInSc4	ostrov
až	až	k9	až
10	[number]	k4	10
000	[number]	k4	000
otroků	otrok	k1gMnPc2	otrok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Madagaskarem	Madagaskar	k1gInSc7	Madagaskar
byl	být	k5eAaImAgInS	být
významnou	významný	k2eAgFnSc7d1	významná
obchodní	obchodní	k2eAgFnSc7d1	obchodní
metropolí	metropol	k1gFnSc7	metropol
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Obchodovalo	obchodovat	k5eAaImAgNnS	obchodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
slonovinou	slonovina	k1gFnSc7	slonovina
a	a	k8xC	a
kořením	koření	k1gNnSc7	koření
<g/>
,	,	kIx,	,
především	především	k9	především
hřebíčkem	hřebíček	k1gInSc7	hřebíček
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
koření	koření	k1gNnSc1	koření
stálo	stát	k5eAaImAgNnS	stát
za	za	k7c7	za
světovou	světový	k2eAgFnSc7d1	světová
proslulostí	proslulost	k1gFnSc7	proslulost
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Éra	éra	k1gFnSc1	éra
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
===	===	k?	===
</s>
</p>
<p>
<s>
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
přinutili	přinutit	k5eAaPmAgMnP	přinutit
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
sultána	sultán	k1gMnSc2	sultán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zakázal	zakázat	k5eAaPmAgInS	zakázat
a	a	k8xC	a
přestal	přestat	k5eAaPmAgInS	přestat
provozovat	provozovat	k5eAaImF	provozovat
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
formálně	formálně	k6eAd1	formálně
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nezměnilo	změnit	k5eNaPmAgNnS	změnit
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zanzibarský	zanzibarský	k2eAgInSc1d1	zanzibarský
sultanát	sultanát	k1gInSc1	sultanát
měl	mít	k5eAaImAgInS	mít
svoje	svůj	k3xOyFgFnPc4	svůj
državy	država	k1gFnPc4	država
i	i	k9	i
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
plynuly	plynout	k5eAaImAgInP	plynout
mocenské	mocenský	k2eAgInPc1d1	mocenský
konflikty	konflikt	k1gInPc1	konflikt
s	s	k7c7	s
Německou	německý	k2eAgFnSc7d1	německá
východoafrickou	východoafrický	k2eAgFnSc7d1	východoafrická
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
vytyčila	vytyčit	k5eAaPmAgFnS	vytyčit
německo-britská	německoritský	k2eAgFnSc1d1	německo-britská
komise	komise	k1gFnSc1	komise
hranice	hranice	k1gFnSc1	hranice
zanzibarského	zanzibarský	k2eAgNnSc2d1	zanzibarský
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
tvořilo	tvořit	k5eAaImAgNnS	tvořit
pobřežní	pobřežní	k2eAgInSc1d1	pobřežní
pás	pás	k1gInSc1	pás
široký	široký	k2eAgInSc1d1	široký
10	[number]	k4	10
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
19	[number]	k4	19
km	km	kA	km
<g/>
)	)	kIx)	)
od	od	k7c2	od
města	město	k1gNnSc2	město
Kap	kapa	k1gFnPc2	kapa
Delgado	Delgada	k1gFnSc5	Delgada
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Mosambik	Mosambik	k1gInSc1	Mosambik
<g/>
)	)	kIx)	)
po	po	k7c4	po
Kipiny	Kipin	k1gInPc4	Kipin
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
byla	být	k5eAaImAgFnS	být
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
pobřeží	pobřeží	k1gNnSc2	pobřeží
pronajata	pronajat	k2eAgFnSc1d1	pronajata
Zanzibarem	Zanzibar	k1gInSc7	Zanzibar
britské	britský	k2eAgFnSc3d1	britská
Východoafrické	východoafrický	k2eAgFnSc3d1	východoafrická
imperiální	imperiální	k2eAgFnSc3d1	imperiální
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
spravovala	spravovat	k5eAaImAgFnS	spravovat
až	až	k9	až
do	do	k7c2	do
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Keni	Keňa	k1gFnSc2	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
pobřeží	pobřeží	k1gNnSc2	pobřeží
si	se	k3xPyFc3	se
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
Německá	německý	k2eAgFnSc1d1	německá
východoafrická	východoafrický	k2eAgFnSc1d1	východoafrická
společnost	společnost	k1gFnSc1	společnost
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
přímo	přímo	k6eAd1	přímo
koupila	koupit	k5eAaPmAgFnS	koupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
zanzibarský	zanzibarský	k2eAgInSc1d1	zanzibarský
sultanát	sultanát	k1gInSc1	sultanát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
už	už	k6eAd1	už
jen	jen	k9	jen
ostrovy	ostrov	k1gInPc1	ostrov
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
a	a	k8xC	a
Pemba	Pemba	k1gFnSc1	Pemba
<g/>
,	,	kIx,	,
britským	britský	k2eAgInSc7d1	britský
protektorátem	protektorát	k1gInSc7	protektorát
a	a	k8xC	a
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
britské	britský	k2eAgFnSc2d1	britská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Báječný	báječný	k2eAgInSc1d1	báječný
hřebíčkový	hřebíčkový	k2eAgInSc1d1	hřebíčkový
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
totiž	totiž	k9	totiž
předmětem	předmět	k1gInSc7	předmět
obchodu	obchod	k1gInSc2	obchod
mezi	mezi	k7c7	mezi
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sultanát	sultanát	k1gInSc1	sultanát
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
sice	sice	k8xC	sice
stále	stále	k6eAd1	stále
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Němci	Němec	k1gMnPc1	Němec
zde	zde	k6eAd1	zde
měli	mít	k5eAaImAgMnP	mít
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
dalo	dát	k5eAaPmAgNnS	dát
se	se	k3xPyFc4	se
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ostrovy	ostrov	k1gInPc1	ostrov
stanou	stanout	k5eAaPmIp3nP	stanout
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
afrických	africký	k2eAgFnPc2d1	africká
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
naopak	naopak	k6eAd1	naopak
ostrov	ostrov	k1gInSc4	ostrov
Helgoland	Helgoland	k1gInSc4	Helgoland
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
budovali	budovat	k5eAaImAgMnP	budovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
byli	být	k5eAaImAgMnP	být
znepokojeni	znepokojen	k2eAgMnPc1d1	znepokojen
tak	tak	k9	tak
blízkou	blízký	k2eAgFnSc7d1	blízká
přítomností	přítomnost	k1gFnSc7	přítomnost
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
pevninského	pevninský	k2eAgNnSc2d1	pevninské
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
s	s	k7c7	s
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
Zanzibar	Zanzibar	k1gInSc4	Zanzibar
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
britského	britský	k2eAgInSc2d1	britský
protektorátu	protektorát	k1gInSc2	protektorát
měl	mít	k5eAaImAgInS	mít
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
svého	svůj	k3xOyFgMnSc4	svůj
sultána	sultán	k1gMnSc4	sultán
z	z	k7c2	z
dědičné	dědičný	k2eAgFnSc2d1	dědičná
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
mocenského	mocenský	k2eAgMnSc4d1	mocenský
zmocněnce	zmocněnec	k1gMnSc4	zmocněnec
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1890	[number]	k4	1890
a	a	k8xC	a
1913	[number]	k4	1913
se	se	k3xPyFc4	se
tomuto	tento	k3xDgMnSc3	tento
britskému	britský	k2eAgMnSc3d1	britský
správci	správce	k1gMnSc3	správce
říkalo	říkat	k5eAaImAgNnS	říkat
vezír	vezír	k1gMnSc1	vezír
<g/>
,	,	kIx,	,
od	od	k7c2	od
1913	[number]	k4	1913
až	až	k9	až
do	do	k7c2	do
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
místodržící	místodržící	k1gMnSc1	místodržící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
===	===	k?	===
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
444	[number]	k4	444
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
na	na	k7c6	na
Pembě	Pemba	k1gFnSc6	Pemba
314	[number]	k4	314
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
získal	získat	k5eAaPmAgInS	získat
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
opět	opět	k6eAd1	opět
sultán	sultán	k1gMnSc1	sultán
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
vládce	vládce	k1gMnSc1	vládce
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vládl	vládnout	k5eAaImAgInS	vládnout
jen	jen	k9	jen
krátce-	krátce-	k?	krátce-
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgInS	svrhnout
a	a	k8xC	a
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
lidovou	lidový	k2eAgFnSc4d1	lidová
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
převratu	převrat	k1gInSc6	převrat
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masakru	masakr	k1gInSc3	masakr
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
místních	místní	k2eAgMnPc2d1	místní
Indů	Ind	k1gMnPc2	Ind
a	a	k8xC	a
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
se	s	k7c7	s
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1964	[number]	k4	1964
spojila	spojit	k5eAaPmAgNnP	spojit
s	s	k7c7	s
Tanganikou	Tanganika	k1gFnSc7	Tanganika
v	v	k7c4	v
nový	nový	k2eAgInSc4d1	nový
stát	stát	k1gInSc4	stát
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
má	mít	k5eAaImIp3nS	mít
vlastního	vlastní	k2eAgMnSc4d1	vlastní
prezidenta	prezident	k1gMnSc4	prezident
a	a	k8xC	a
sněmovnu	sněmovna	k1gFnSc4	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
50	[number]	k4	50
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
krajů	kraj	k1gInPc2	kraj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
střed	střed	k1gInSc1	střed
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
</s>
</p>
<p>
<s>
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
sever	sever	k1gInSc1	sever
</s>
</p>
<p>
<s>
Zanzibar	Zanzibar	k1gInSc4	Zanzibar
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
západ	západ	k1gInSc1	západ
</s>
</p>
<p>
<s>
Pemba	Pemba	k1gMnSc1	Pemba
sever	sever	k1gInSc4	sever
</s>
</p>
<p>
<s>
Pemba	Pemba	k1gFnSc1	Pemba
jih	jih	k1gInSc1	jih
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Anglo-zanzibarská	Angloanzibarský	k2eAgFnSc1d1	Anglo-zanzibarský
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
české	český	k2eAgFnPc1d1	Česká
stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
</s>
</p>
