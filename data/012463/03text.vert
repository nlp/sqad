<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Koníček	Koníček	k1gMnSc1	Koníček
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1964	[number]	k4	1964
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
až	až	k9	až
2018	[number]	k4	2018
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
člen	člen	k1gInSc1	člen
kolegia	kolegium	k1gNnSc2	kolegium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
profese	profese	k1gFnSc1	profese
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
UJEP	UJEP	kA	UJEP
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
obor	obor	k1gInSc1	obor
fyzika	fyzik	k1gMnSc2	fyzik
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
promoci	promoce	k1gFnSc6	promoce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
podniku	podnik	k1gInSc2	podnik
Mesit	Mesita	k1gFnPc2	Mesita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
učit	učit	k5eAaImF	učit
na	na	k7c6	na
Střední	střední	k2eAgFnSc6d1	střední
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
a	a	k8xC	a
hotelové	hotelový	k2eAgFnSc6d1	hotelová
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgNnSc3	tento
povolání	povolání	k1gNnSc4	povolání
si	se	k3xPyFc3	se
doplnil	doplnit	k5eAaPmAgMnS	doplnit
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
učitelství	učitelství	k1gNnSc6	učitelství
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Alenou	Alena	k1gFnSc7	Alena
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Vladimíra	Vladimír	k1gMnSc2	Vladimír
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
Terezu	Tereza	k1gFnSc4	Tereza
a	a	k8xC	a
Alžbětu	Alžběta	k1gFnSc4	Alžběta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
KSČ	KSČ	kA	KSČ
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
a	a	k8xC	a
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
za	za	k7c4	za
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Profesně	profesně	k6eAd1	profesně
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
a	a	k8xC	a
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
za	za	k7c7	za
KSČM	KSČM	kA	KSČM
zvolen	zvolen	k2eAgMnSc1d1	zvolen
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
obce	obec	k1gFnSc2	obec
Boršice	Boršice	k1gFnSc2	Boršice
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
obhájil	obhájit	k5eAaPmAgMnS	obhájit
post	post	k1gInSc4	post
zastupitele	zastupitel	k1gMnSc2	zastupitel
obce	obec	k1gFnSc2	obec
Boršice	Boršic	k1gMnSc2	Boršic
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
preferenčním	preferenční	k2eAgInPc3d1	preferenční
hlasům	hlas	k1gInPc3	hlas
(	(	kIx(	(
<g/>
posunul	posunout	k5eAaPmAgMnS	posunout
se	se	k3xPyFc4	se
z	z	k7c2	z
původního	původní	k2eAgMnSc2d1	původní
8	[number]	k4	8
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
konečné	konečná	k1gFnSc6	konečná
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
;	;	kIx,	;
KSČM	KSČM	kA	KSČM
přitom	přitom	k6eAd1	přitom
získala	získat	k5eAaPmAgFnS	získat
dva	dva	k4xCgInPc4	dva
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
uspěl	uspět	k5eAaPmAgMnS	uspět
vlivem	vlivem	k7c2	vlivem
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
za	za	k7c4	za
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Zlínský	zlínský	k2eAgInSc1d1	zlínský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
mládež	mládež	k1gFnSc4	mládež
a	a	k8xC	a
tělovýchovu	tělovýchova	k1gFnSc4	tělovýchova
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
sněmovního	sněmovní	k2eAgInSc2d1	sněmovní
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Opětovně	opětovně	k6eAd1	opětovně
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
zvolen	zvolit	k5eAaPmNgInS	zvolit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
místopředsedou	místopředseda	k1gMnSc7	místopředseda
petičního	petiční	k2eAgInSc2d1	petiční
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
volebního	volební	k2eAgInSc2d1	volební
výboru	výbor	k1gInSc2	výbor
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
stínové	stínový	k2eAgFnSc6d1	stínová
vládě	vláda	k1gFnSc6	vláda
KSČM	KSČM	kA	KSČM
spravuje	spravovat	k5eAaImIp3nS	spravovat
průřezový	průřezový	k2eAgInSc1d1	průřezový
resort	resort	k1gInSc1	resort
informatiky	informatika	k1gFnSc2	informatika
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
jako	jako	k9	jako
lídr	lídr	k1gMnSc1	lídr
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
lídrem	lídr	k1gMnSc7	lídr
KSČM	KSČM	kA	KSČM
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
1	[number]	k4	1
475	[number]	k4	475
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
obhájil	obhájit	k5eAaPmAgMnS	obhájit
tak	tak	k9	tak
mandát	mandát	k1gInSc4	mandát
poslance	poslanec	k1gMnSc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
PČR	PČR	kA	PČR
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
kolegia	kolegium	k1gNnSc2	kolegium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
mu	on	k3xPp3gMnSc3	on
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
funkce	funkce	k1gFnSc1	funkce
poslance	poslanec	k1gMnSc2	poslanec
a	a	k8xC	a
člena	člen	k1gMnSc2	člen
kolegia	kolegium	k1gNnSc2	kolegium
NKÚ	NKÚ	kA	NKÚ
nejsou	být	k5eNaImIp3nP	být
slučitelné	slučitelný	k2eAgFnPc1d1	slučitelná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
jej	on	k3xPp3gInSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
stranická	stranický	k2eAgFnSc1d1	stranická
kolegyně	kolegyně	k1gFnSc1	kolegyně
Marie	Marie	k1gFnSc1	Marie
Pěnčíková	Pěnčíková	k1gFnSc1	Pěnčíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Koníček	Koníček	k1gMnSc1	Koníček
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
