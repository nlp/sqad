<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Koníček	Koníček	k1gMnSc1	Koníček
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1964	[number]	k4	1964
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
až	až	k9	až
2018	[number]	k4	2018
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
člen	člen	k1gInSc1	člen
kolegia	kolegium	k1gNnSc2	kolegium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
