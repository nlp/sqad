<p>
<s>
Velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
kraslice	kraslice	k1gFnSc1	kraslice
je	být	k5eAaImIp3nS	být
natvrdo	natvrdo	k6eAd1	natvrdo
uvařené	uvařený	k2eAgNnSc1d1	uvařené
nebo	nebo	k8xC	nebo
vyfouknuté	vyfouknutý	k2eAgNnSc1d1	vyfouknuté
prázdné	prázdný	k2eAgNnSc1d1	prázdné
vejce	vejce	k1gNnSc1	vejce
<g/>
,	,	kIx,	,
ozdobené	ozdobený	k2eAgNnSc1d1	ozdobené
různými	různý	k2eAgFnPc7d1	různá
výtvarnými	výtvarný	k2eAgFnPc7d1	výtvarná
technikami	technika	k1gFnPc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
raného	raný	k2eAgNnSc2d1	rané
křesťanství	křesťanství	k1gNnSc2	křesťanství
jsou	být	k5eAaImIp3nP	být
kraslice	kraslice	k1gFnPc1	kraslice
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
tradici	tradice	k1gFnSc6	tradice
nedílně	nedílně	k6eAd1	nedílně
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
plodnost	plodnost	k1gFnSc4	plodnost
a	a	k8xC	a
znovuzrození	znovuzrození	k1gNnSc4	znovuzrození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
pradávných	pradávný	k2eAgFnPc2d1	pradávná
dob	doba	k1gFnPc2	doba
bylo	být	k5eAaImAgNnS	být
vejce	vejce	k1gNnSc4	vejce
symbolem	symbol	k1gInSc7	symbol
zrození	zrození	k1gNnSc2	zrození
a	a	k8xC	a
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zdobení	zdobení	k1gNnSc1	zdobení
skořápek	skořápka	k1gFnPc2	skořápka
vajec	vejce	k1gNnPc2	vejce
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
až	až	k9	až
do	do	k7c2	do
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
či	či	k8xC	či
Persii	Persie	k1gFnSc6	Persie
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
svátky	svátek	k1gInPc4	svátek
jara	jaro	k1gNnSc2	jaro
barvila	barvit	k5eAaImAgFnS	barvit
červeně	červeně	k6eAd1	červeně
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
vejce	vejce	k1gNnSc2	vejce
vkládala	vkládat	k5eAaImAgFnS	vkládat
do	do	k7c2	do
hrobů	hrob	k1gInPc2	hrob
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
Zlatě	zlatě	k6eAd1	zlatě
obarvené	obarvený	k2eAgNnSc1d1	obarvené
vejce	vejce	k1gNnSc1	vejce
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
hrobce	hrobka	k1gFnSc6	hrobka
v	v	k7c6	v
Sumeru	Sumer	k1gInSc6	Sumer
<g/>
.	.	kIx.	.
</s>
<s>
Obyčej	obyčej	k1gInSc4	obyčej
malování	malování	k1gNnSc2	malování
vajec	vejce	k1gNnPc2	vejce
znali	znát	k5eAaImAgMnP	znát
antičtí	antický	k2eAgMnPc1d1	antický
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
době	doba	k1gFnSc6	doba
císařské	císařský	k2eAgFnSc6d1	císařská
<g/>
,	,	kIx,	,
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
Ovidius	Ovidius	k1gMnSc1	Ovidius
<g/>
,	,	kIx,	,
Plinius	Plinius	k1gMnSc1	Plinius
mladší	mladý	k2eAgMnSc1d2	mladší
i	i	k8xC	i
Juvenalis	Juvenalis	k1gInSc1	Juvenalis
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
nalezly	nalézt	k5eAaBmAgInP	nalézt
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
polských	polský	k2eAgInPc6d1	polský
archeologických	archeologický	k2eAgInPc6d1	archeologický
výzkumech	výzkum	k1gInPc6	výzkum
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
přijalo	přijmout	k5eAaPmAgNnS	přijmout
symboliku	symbolika	k1gFnSc4	symbolika
červené	červená	k1gFnSc2	červená
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
krve	krev	k1gFnSc2	krev
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgFnSc2d1	Česká
pověry	pověra	k1gFnSc2	pověra
a	a	k8xC	a
zvyky	zvyk	k1gInPc4	zvyk
==	==	k?	==
</s>
</p>
<p>
<s>
Kraslice	Kraslice	k1gFnPc1	Kraslice
slouží	sloužit	k5eAaImIp3nP	sloužit
ženám	žena	k1gFnPc3	žena
a	a	k8xC	a
dívkám	dívka	k1gFnPc3	dívka
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
jako	jako	k8xC	jako
odměna	odměna	k1gFnSc1	odměna
pro	pro	k7c4	pro
koledníka	koledník	k1gMnSc4	koledník
za	za	k7c7	za
tzv.	tzv.	kA	tzv.
pomlazení	pomlazení	k1gNnSc1	pomlazení
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
vyšlehání	vyšlehání	k1gNnSc1	vyšlehání
pomlázkou	pomlázka	k1gFnSc7	pomlázka
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgInP	být
takové	takový	k3xDgInPc1	takový
dárky	dárek	k1gInPc1	dárek
vykoupením	vykoupení	k1gNnSc7	vykoupení
<g/>
,	,	kIx,	,
dárkem	dárek	k1gInSc7	dárek
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
koledy	koleda	k1gFnSc2	koleda
a	a	k8xC	a
pomlázka	pomlázka	k1gFnSc1	pomlázka
či	či	k8xC	či
oblévačka	oblévačka	k1gFnSc1	oblévačka
byly	být	k5eAaImAgInP	být
zábavným	zábavný	k2eAgInSc7d1	zábavný
trestem	trest	k1gInSc7	trest
pro	pro	k7c4	pro
málo	málo	k6eAd1	málo
štědré	štědrý	k2eAgFnPc4d1	štědrá
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
v	v	k7c4	v
magickou	magický	k2eAgFnSc4d1	magická
moc	moc	k1gFnSc4	moc
vajec	vejce	k1gNnPc2	vejce
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
posílení	posílení	k1gNnSc3	posílení
se	se	k3xPyFc4	se
pomalovávala	pomalovávat	k5eAaImAgFnS	pomalovávat
různými	různý	k2eAgInPc7d1	různý
magickými	magický	k2eAgInPc7d1	magický
ornamenty	ornament	k1gInPc7	ornament
<g/>
.	.	kIx.	.
</s>
<s>
Magie	magie	k1gFnSc1	magie
postupně	postupně	k6eAd1	postupně
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgInP	zůstat
jen	jen	k9	jen
ornamenty	ornament	k1gInPc1	ornament
převážně	převážně	k6eAd1	převážně
abstraktní	abstraktní	k2eAgInPc1d1	abstraktní
nebo	nebo	k8xC	nebo
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
a	a	k8xC	a
zvířecí	zvířecí	k2eAgInPc4d1	zvířecí
motivy	motiv	k1gInPc4	motiv
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
přírodu	příroda	k1gFnSc4	příroda
probouzející	probouzející	k2eAgFnSc4d1	probouzející
se	se	k3xPyFc4	se
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
symbolice	symbolika	k1gFnSc6	symbolika
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
nejčastěji	často	k6eAd3	často
odvozován	odvozován	k2eAgInSc4d1	odvozován
původ	původ	k1gInSc4	původ
slova	slovo	k1gNnSc2	slovo
kraslice	kraslice	k1gFnSc2	kraslice
(	(	kIx(	(
<g/>
ze	z	k7c2	z
staroslovanského	staroslovanský	k2eAgMnSc2d1	staroslovanský
krasnyj	krasnyj	k1gMnSc1	krasnyj
<g/>
,	,	kIx,	,
krasniti	krasnit	k5eAaImF	krasnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červenou	červený	k2eAgFnSc4d1	červená
kraslici	kraslice	k1gFnSc4	kraslice
si	se	k3xPyFc3	se
dívka	dívka	k1gFnSc1	dívka
nechávala	nechávat	k5eAaImAgFnS	nechávat
pro	pro	k7c4	pro
hocha	hoch	k1gMnSc4	hoch
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterého	který	k3yQgNnSc2	který
si	se	k3xPyFc3	se
tajně	tajně	k6eAd1	tajně
myslela	myslet	k5eAaImAgFnS	myslet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vejce	vejce	k1gNnSc1	vejce
darované	darovaný	k2eAgNnSc1d1	darované
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
nejen	nejen	k6eAd1	nejen
červené	červený	k2eAgNnSc1d1	červené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
plné	plný	k2eAgInPc1d1	plný
<g/>
.	.	kIx.	.
</s>
<s>
Prázdné	prázdný	k2eAgFnPc1d1	prázdná
bílé	bílý	k2eAgFnPc1d1	bílá
skořápky	skořápka	k1gFnPc1	skořápka
byly	být	k5eAaImAgFnP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prázdnými	prázdný	k2eAgFnPc7d1	prázdná
ulitami	ulita	k1gFnPc7	ulita
šneků	šnek	k1gMnPc2	šnek
symboly	symbol	k1gInPc7	symbol
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgFnP	sloužit
ke	k	k7c3	k
zdobení	zdobení	k1gNnSc3	zdobení
Morany	Morana	k1gFnSc2	Morana
<g/>
.	.	kIx.	.
</s>
<s>
Duté	dutý	k2eAgFnPc1d1	dutá
malované	malovaný	k2eAgFnPc1d1	malovaná
kraslice	kraslice	k1gFnPc1	kraslice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
výdumky	výdumka	k1gFnSc2	výdumka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
až	až	k6eAd1	až
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
také	také	k9	také
výhodným	výhodný	k2eAgInSc7d1	výhodný
komerčním	komerční	k2eAgInSc7d1	komerční
artiklem	artikl	k1gInSc7	artikl
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stihlo	stihnout	k5eAaPmAgNnS	stihnout
namalovat	namalovat	k5eAaPmF	namalovat
dostatek	dostatek	k1gInSc4	dostatek
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
malovaly	malovat	k5eAaImAgInP	malovat
se	se	k3xPyFc4	se
předem	předem	k6eAd1	předem
až	až	k9	až
průběžně	průběžně	k6eAd1	průběžně
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
tudíž	tudíž	k8xC	tudíž
možné	možný	k2eAgNnSc1d1	možné
malovat	malovat	k5eAaImF	malovat
vajíčka	vajíčko	k1gNnPc4	vajíčko
plná	plný	k2eAgNnPc4d1	plné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zkazila	zkazit	k5eAaPmAgFnS	zkazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fabergého	Fabergého	k2eAgNnPc1d1	Fabergého
vejce	vejce	k1gNnPc1	vejce
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
tradice	tradice	k1gFnSc2	tradice
ruských	ruský	k2eAgFnPc2d1	ruská
kraslic	kraslice	k1gFnPc2	kraslice
pochází	pocházet	k5eAaImIp3nS	pocházet
slavná	slavný	k2eAgFnSc1d1	slavná
velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
Fabergého	Fabergého	k2eAgNnSc4d1	Fabergého
vejce	vejce	k1gNnSc4	vejce
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
od	od	k7c2	od
petrohradského	petrohradský	k2eAgMnSc2d1	petrohradský
klenotníka	klenotník	k1gMnSc2	klenotník
Petera	Peter	k1gMnSc2	Peter
Carla	Carl	k1gMnSc2	Carl
Fabergého	Fabergý	k1gMnSc2	Fabergý
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
polodrahokamů	polodrahokam	k1gInPc2	polodrahokam
a	a	k8xC	a
ušlechtilých	ušlechtilý	k2eAgInPc2d1	ušlechtilý
kovů	kov	k1gInPc2	kov
zdobená	zdobený	k2eAgFnSc1d1	zdobená
různými	různý	k2eAgFnPc7d1	různá
zlatnickými	zlatnický	k2eAgFnPc7d1	Zlatnická
technikami	technika	k1gFnPc7	technika
a	a	k8xC	a
skrývají	skrývat	k5eAaImIp3nP	skrývat
různá	různý	k2eAgNnPc4d1	různé
překvapení	překvapení	k1gNnPc4	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
původně	původně	k6eAd1	původně
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
tvořené	tvořený	k2eAgFnPc1d1	tvořená
i	i	k9	i
pro	pro	k7c4	pro
další	další	k2eAgMnPc4d1	další
objednatele	objednatel	k1gMnPc4	objednatel
a	a	k8xC	a
také	také	k9	také
vznikaly	vznikat	k5eAaImAgFnP	vznikat
imitace	imitace	k1gFnPc1	imitace
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejobdivovanějším	obdivovaný	k2eAgInPc3d3	nejobdivovanější
šperkům	šperk	k1gInPc3	šperk
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Techniky	technika	k1gFnPc1	technika
zdobení	zdobení	k1gNnSc2	zdobení
==	==	k?	==
</s>
</p>
<p>
<s>
Technik	technik	k1gMnSc1	technik
zdobení	zdobení	k1gNnSc2	zdobení
vajec	vejce	k1gNnPc2	vejce
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
od	od	k7c2	od
nejjednodušších	jednoduchý	k2eAgFnPc2d3	nejjednodušší
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgNnPc7	jaký
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
reliéfní	reliéfní	k2eAgFnSc1d1	reliéfní
kresba	kresba	k1gFnSc1	kresba
voskem	vosk	k1gInSc7	vosk
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
batikování	batikování	k1gNnSc1	batikování
<g/>
,	,	kIx,	,
<g/>
přes	přes	k7c4	přes
složitější	složitý	k2eAgNnSc4d2	složitější
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
vícebarevná	vícebarevný	k2eAgFnSc1d1	vícebarevná
batika	batika	k1gFnSc1	batika
či	či	k8xC	či
</s>
</p>
<p>
<s>
leptání	leptání	k1gNnSc1	leptání
<g/>
,	,	kIx,	,
<g/>
až	až	k9	až
k	k	k7c3	k
nejobtížnějším	obtížný	k2eAgFnPc3d3	nejobtížnější
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
jsou	být	k5eAaImIp3nP	být
</s>
</p>
<p>
<s>
polepování	polepování	k1gNnSc1	polepování
slámou	sláma	k1gFnSc7	sláma
či	či	k8xC	či
sítinou	sítina	k1gFnSc7	sítina
a	a	k8xC	a
</s>
</p>
<p>
<s>
vyškrabování	vyškrabování	k1gNnSc1	vyškrabování
neboli	neboli	k8xC	neboli
gravírování	gravírování	k1gNnSc1	gravírování
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
komplikovanou	komplikovaný	k2eAgFnSc7d1	komplikovaná
technikou	technika	k1gFnSc7	technika
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
drátování	drátování	k1gNnSc1	drátování
<g/>
,	,	kIx,	,
<g/>
patrně	patrně	k6eAd1	patrně
nejkomplikovanější	komplikovaný	k2eAgFnSc1d3	nejkomplikovanější
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
</s>
</p>
<p>
<s>
prořezávání	prořezávání	k1gNnSc1	prořezávání
a	a	k8xC	a
vrtání	vrtání	k1gNnSc1	vrtání
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
techniky	technika	k1gFnPc4	technika
ovšem	ovšem	k9	ovšem
nepatří	patřit	k5eNaImIp3nP	patřit
ke	k	k7c3	k
klasickým	klasický	k2eAgFnPc3d1	klasická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vcelku	vcelku	k6eAd1	vcelku
moderní	moderní	k2eAgInPc4d1	moderní
způsoby	způsob	k1gInPc4	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
</s>
</p>
<p>
<s>
polepování	polepování	k1gNnSc1	polepování
vajíček	vajíčko	k1gNnPc2	vajíčko
obtisky	obtisk	k1gInPc7	obtisk
či	či	k8xC	či
samolepkami	samolepka	k1gFnPc7	samolepka
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
obrázky	obrázek	k1gInPc7	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
<s>
Velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
kraslice	kraslice	k1gFnSc1	kraslice
z	z	k7c2	z
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
folklorního	folklorní	k2eAgInSc2d1	folklorní
festivalu	festival	k1gInSc2	festival
ve	v	k7c6	v
Valašském	valašský	k2eAgNnSc6d1	Valašské
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
Rožnov	Rožnov	k1gInSc1	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gInSc7	Radhošť
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kraslice	kraslice	k1gFnSc2	kraslice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kraslice	kraslice	k1gFnSc2	kraslice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Kraslice	kraslice	k1gFnSc2	kraslice
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
/	/	kIx~	/
<g/>
Malujeme	malovat	k5eAaImIp1nP	malovat
vajíčka	vajíčko	k1gNnPc4	vajíčko
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
</s>
</p>
<p>
<s>
Vacenovické	Vacenovický	k2eAgFnSc2d1	Vacenovický
kraslice	kraslice	k1gFnSc2	kraslice
–	–	k?	–
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
kamera	kamera	k1gFnSc1	kamera
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
2016-03-13	[number]	k4	2016-03-13
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
