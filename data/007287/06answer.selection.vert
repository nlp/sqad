<s>
Příčné	příčný	k2eAgInPc1d1	příčný
prahy	práh	k1gInPc1	práh
bývají	bývat	k5eAaImIp3nP	bývat
zvýrazňovány	zvýrazňovat	k5eAaImNgInP	zvýrazňovat
vodorovným	vodorovný	k2eAgNnSc7d1	vodorovné
i	i	k8xC	i
svislým	svislý	k2eAgNnSc7d1	svislé
dopravním	dopravní	k2eAgNnSc7d1	dopravní
značením	značení	k1gNnSc7	značení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
též	též	k9	též
dalšími	další	k2eAgInPc7d1	další
reflexními	reflexní	k2eAgInPc7d1	reflexní
prvky	prvek	k1gInPc7	prvek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
odrazkami	odrazka	k1gFnPc7	odrazka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
