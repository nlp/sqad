<s>
Zpomalovací	zpomalovací	k2eAgInSc1d1	zpomalovací
práh	práh	k1gInSc1	práh
(	(	kIx(	(
<g/>
též	též	k9	též
příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
<g/>
,	,	kIx,	,
retardér	retardér	k1gInSc1	retardér
nebo	nebo	k8xC	nebo
ležící	ležící	k2eAgFnSc1d1	ležící
policajt	policajt	k?	policajt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
vozovce	vozovka	k1gFnSc6	vozovka
pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
donutit	donutit	k5eAaPmF	donutit
řidiče	řidič	k1gMnPc4	řidič
silničních	silniční	k2eAgInPc2d1	silniční
vozidel	vozidlo	k1gNnPc2	vozidlo
k	k	k7c3	k
pomalé	pomalý	k2eAgFnSc3d1	pomalá
jízdě	jízda	k1gFnSc3	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
dopravními	dopravní	k2eAgFnPc7d1	dopravní
značkami	značka	k1gFnPc7	značka
omezujícími	omezující	k2eAgFnPc7d1	omezující
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgInPc7d1	jiný
prostředky	prostředek	k1gInPc7	prostředek
k	k	k7c3	k
dopravnímu	dopravní	k2eAgNnSc3d1	dopravní
zklidnění	zklidnění	k1gNnSc3	zklidnění
komunikace	komunikace	k1gFnSc2	komunikace
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
zúžení	zúžený	k2eAgMnPc1d1	zúžený
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
,	,	kIx,	,
náhlá	náhlý	k2eAgFnSc1d1	náhlá
změna	změna	k1gFnSc1	změna
směrových	směrový	k2eAgFnPc2d1	směrová
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
šikana	šikana	k1gFnSc1	šikana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
barvy	barva	k1gFnSc2	barva
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
změna	změna	k1gFnSc1	změna
povrchu	povrch	k1gInSc2	povrch
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
,	,	kIx,	,
opticko-akustická	optickokustický	k2eAgFnSc1d1	opticko-akustický
brzda	brzda	k1gFnSc1	brzda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různě	různě	k6eAd1	různě
kombinovány	kombinovat	k5eAaImNgFnP	kombinovat
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
efekt	efekt	k1gInSc1	efekt
i	i	k8xC	i
vzhled	vzhled	k1gInSc1	vzhled
jako	jako	k8xC	jako
zpomalovací	zpomalovací	k2eAgInSc1d1	zpomalovací
práh	práh	k1gInSc1	práh
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
kabelový	kabelový	k2eAgInSc4d1	kabelový
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
však	však	k9	však
není	být	k5eNaImIp3nS	být
zpomalení	zpomalení	k1gNnSc1	zpomalení
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožnění	umožnění	k1gNnSc1	umožnění
přejezdu	přejezd	k1gInSc2	přejezd
přes	přes	k7c4	přes
kabely	kabel	k1gInPc4	kabel
vedené	vedený	k2eAgInPc4d1	vedený
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
buď	buď	k8xC	buď
demontovatelnými	demontovatelný	k2eAgInPc7d1	demontovatelný
díly	díl	k1gInPc7	díl
připevněnými	připevněný	k2eAgInPc7d1	připevněný
k	k	k7c3	k
vozovce	vozovka	k1gFnSc3	vozovka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stavebním	stavební	k2eAgNnSc7d1	stavební
zvýšením	zvýšení	k1gNnSc7	zvýšení
samotné	samotný	k2eAgFnSc2d1	samotná
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Demontovatelné	demontovatelný	k2eAgInPc1d1	demontovatelný
prahy	práh	k1gInPc1	práh
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
strmější	strmý	k2eAgInPc1d2	strmější
nájezdy	nájezd	k1gInPc1	nájezd
a	a	k8xC	a
vynucují	vynucovat	k5eAaImIp3nP	vynucovat
si	se	k3xPyFc3	se
tak	tak	k9	tak
téměř	téměř	k6eAd1	téměř
zastavení	zastavení	k1gNnSc1	zastavení
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
případě	případ	k1gInSc6	případ
stavebního	stavební	k2eAgNnSc2d1	stavební
zvýšení	zvýšení	k1gNnSc2	zvýšení
vozovky	vozovka	k1gFnSc2	vozovka
bývá	bývat	k5eAaImIp3nS	bývat
nájezd	nájezd	k1gInSc1	nájezd
tvořen	tvořen	k2eAgInSc1d1	tvořen
pozvolně	pozvolně	k6eAd1	pozvolně
šikmou	šikmý	k2eAgFnSc7d1	šikmá
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Speciálními	speciální	k2eAgInPc7d1	speciální
případy	případ	k1gInPc7	případ
příčného	příčný	k2eAgInSc2d1	příčný
prahu	práh	k1gInSc2	práh
jsou	být	k5eAaImIp3nP	být
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
přechod	přechod	k1gInSc4	přechod
pro	pro	k7c4	pro
chodce	chodec	k1gMnSc4	chodec
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
barevným	barevný	k2eAgNnSc7d1	barevné
zvýrazněním	zvýraznění	k1gNnSc7	zvýraznění
tato	tento	k3xDgFnSc1	tento
úprava	úprava	k1gFnSc1	úprava
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
přechod	přechod	k1gInSc4	přechod
pro	pro	k7c4	pro
řidiče	řidič	k1gMnPc4	řidič
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
touto	tento	k3xDgFnSc7	tento
úpravou	úprava	k1gFnSc7	úprava
řešen	řešen	k2eAgInSc1d1	řešen
bezbariérový	bezbariérový	k2eAgInSc1d1	bezbariérový
přístup	přístup	k1gInSc1	přístup
na	na	k7c4	na
přechod	přechod	k1gInSc4	přechod
<g/>
)	)	kIx)	)
a	a	k8xC	a
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
zastávka	zastávka	k1gFnSc1	zastávka
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Příčné	příčný	k2eAgInPc1d1	příčný
prahy	práh	k1gInPc1	práh
bývají	bývat	k5eAaImIp3nP	bývat
zvýrazňovány	zvýrazňovat	k5eAaImNgInP	zvýrazňovat
vodorovným	vodorovný	k2eAgNnSc7d1	vodorovné
i	i	k8xC	i
svislým	svislý	k2eAgNnSc7d1	svislé
dopravním	dopravní	k2eAgNnSc7d1	dopravní
značením	značení	k1gNnSc7	značení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
též	též	k9	též
dalšími	další	k2eAgInPc7d1	další
reflexními	reflexní	k2eAgInPc7d1	reflexní
prvky	prvek	k1gInPc7	prvek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
odrazkami	odrazka	k1gFnPc7	odrazka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčné	příčný	k2eAgInPc1d1	příčný
prahy	práh	k1gInPc1	práh
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
zejména	zejména	k9	zejména
na	na	k7c6	na
komunikacích	komunikace	k1gFnPc6	komunikace
v	v	k7c6	v
obytných	obytný	k2eAgFnPc6d1	obytná
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
škol	škola	k1gFnPc2	škola
či	či	k8xC	či
přechodů	přechod	k1gInPc2	přechod
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
přínos	přínos	k1gInSc1	přínos
je	být	k5eAaImIp3nS	být
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
takto	takto	k6eAd1	takto
vynucené	vynucený	k2eAgNnSc4d1	vynucené
zpomalení	zpomalení	k1gNnSc4	zpomalení
dopravy	doprava	k1gFnSc2	doprava
považují	považovat	k5eAaImIp3nP	považovat
někteří	některý	k3yIgMnPc1	některý
řidiči	řidič	k1gMnPc1	řidič
za	za	k7c4	za
nepřiměřené	přiměřený	k2eNgFnPc4d1	nepřiměřená
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
opotřebení	opotřebení	k1gNnSc4	opotřebení
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
nápravy	náprava	k1gFnPc1	náprava
a	a	k8xC	a
tlumiče	tlumič	k1gInPc1	tlumič
pérování	pérování	k1gNnSc2	pérování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelům	obyvatel	k1gMnPc3	obyvatel
může	moct	k5eAaImIp3nS	moct
vadit	vadit	k5eAaImF	vadit
zvýšení	zvýšení	k1gNnSc4	zvýšení
hlučnosti	hlučnost	k1gFnSc3	hlučnost
i	i	k8xC	i
prašnosti	prašnost	k1gFnSc3	prašnost
průjezdu	průjezd	k1gInSc2	průjezd
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
prodloužení	prodloužení	k1gNnSc2	prodloužení
doby	doba	k1gFnSc2	doba
jejich	jejich	k3xOp3gInSc3	jejich
průjezdu	průjezd	k1gInSc3	průjezd
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc3	zvýšení
množství	množství	k1gNnSc2	množství
spalovacích	spalovací	k2eAgFnPc2d1	spalovací
zplodin	zplodina	k1gFnPc2	zplodina
vlivem	vlivem	k7c2	vlivem
zpomalení	zpomalení	k1gNnSc2	zpomalení
a	a	k8xC	a
opětovného	opětovný	k2eAgInSc2d1	opětovný
rozjezdu	rozjezd	k1gInSc2	rozjezd
každého	každý	k3xTgNnSc2	každý
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
zpomalovací	zpomalovací	k2eAgInPc1d1	zpomalovací
prahy	práh	k1gInPc1	práh
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c4	na
čerstvě	čerstvě	k6eAd1	čerstvě
rekonstruovanou	rekonstruovaný	k2eAgFnSc4d1	rekonstruovaná
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
upozorňováno	upozorňován	k2eAgNnSc1d1	upozorňováno
na	na	k7c4	na
paradox	paradox	k1gInSc4	paradox
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledný	výsledný	k2eAgInSc1d1	výsledný
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
stavu	stav	k1gInSc3	stav
před	před	k7c7	před
opravou	oprava	k1gFnSc7	oprava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
řidiče	řidič	k1gInPc4	řidič
nutil	nutit	k5eAaImAgMnS	nutit
k	k	k7c3	k
pomalé	pomalý	k2eAgFnSc3d1	pomalá
jízdě	jízda	k1gFnSc3	jízda
špatný	špatný	k2eAgInSc4d1	špatný
stavební	stavební	k2eAgInSc4d1	stavební
stav	stav	k1gInSc4	stav
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Ztížen	ztížen	k2eAgInSc1d1	ztížen
je	být	k5eAaImIp3nS	být
i	i	k9	i
úklid	úklid	k1gInSc4	úklid
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
poškozování	poškozování	k1gNnSc3	poškozování
prahů	práh	k1gInPc2	práh
i	i	k8xC	i
úklidové	úklidový	k2eAgFnSc2d1	úklidová
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
automobilová	automobilový	k2eAgFnSc1d1	automobilová
asociace	asociace	k1gFnSc1	asociace
údajně	údajně	k6eAd1	údajně
provedla	provést	k5eAaPmAgFnS	provést
výzkum	výzkum	k1gInSc4	výzkum
srovnání	srovnání	k1gNnSc2	srovnání
spotřeby	spotřeba	k1gFnSc2	spotřeba
paliva	palivo	k1gNnSc2	palivo
při	při	k7c6	při
stálé	stálý	k2eAgFnSc6d1	stálá
rychlosti	rychlost	k1gFnSc6	rychlost
30	[number]	k4	30
mph	mph	k?	mph
(	(	kIx(	(
<g/>
48	[number]	k4	48
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
oproti	oproti	k7c3	oproti
jízdě	jízda	k1gFnSc3	jízda
přes	přes	k7c4	přes
zpomalovací	zpomalovací	k2eAgInPc4d1	zpomalovací
prahy	práh	k1gInPc4	práh
a	a	k8xC	a
zjistila	zjistit	k5eAaPmAgFnS	zjistit
vzrůst	vzrůst	k1gInSc4	vzrůst
spotřeby	spotřeba	k1gFnSc2	spotřeba
ze	z	k7c2	z
4,9	[number]	k4	4,9
na	na	k7c4	na
9,1	[number]	k4	9,1
litrů	litr	k1gInPc2	litr
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prostém	prostý	k2eAgNnSc6d1	prosté
snížení	snížení	k1gNnSc6	snížení
rychlosti	rychlost	k1gFnSc2	rychlost
ze	z	k7c2	z
30	[number]	k4	30
na	na	k7c4	na
20	[number]	k4	20
mph	mph	k?	mph
(	(	kIx(	(
<g/>
32	[number]	k4	32
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
spotřeba	spotřeba	k1gFnSc1	spotřeba
jen	jen	k9	jen
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
výzkum	výzkum	k1gInSc4	výzkum
údajně	údajně	k6eAd1	údajně
provedla	provést	k5eAaPmAgFnS	provést
britská	britský	k2eAgFnSc1d1	britská
Laboratoř	laboratoř	k1gFnSc1	laboratoř
dopravního	dopravní	k2eAgInSc2d1	dopravní
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
se	s	k7c7	s
zpomalovacími	zpomalovací	k2eAgInPc7d1	zpomalovací
prahy	práh	k1gInPc7	práh
jsou	být	k5eAaImIp3nP	být
zvýšeny	zvýšen	k2eAgFnPc4d1	zvýšena
emise	emise	k1gFnPc4	emise
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
až	až	k9	až
o	o	k7c4	o
82	[number]	k4	82
%	%	kIx~	%
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
dusného	dusný	k2eAgInSc2d1	dusný
až	až	k9	až
o	o	k7c4	o
37	[number]	k4	37
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
krádežím	krádež	k1gFnPc3	krádež
zpomalovacích	zpomalovací	k2eAgMnPc2d1	zpomalovací
prahů	práh	k1gInPc2	práh
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dopouštět	dopouštět	k5eAaImF	dopouštět
jejich	jejich	k3xOp3gMnPc1	jejich
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Příčné	příčný	k2eAgInPc1d1	příčný
prahy	práh	k1gInPc1	práh
také	také	k9	také
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
rychlý	rychlý	k2eAgInSc4d1	rychlý
průjezd	průjezd	k1gInSc4	průjezd
vozidel	vozidlo	k1gNnPc2	vozidlo
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
záchranného	záchranný	k2eAgInSc2d1	záchranný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
umisťování	umisťování	k1gNnSc4	umisťování
příčných	příčný	k2eAgInPc2d1	příčný
prahů	práh	k1gInPc2	práh
stanovilo	stanovit	k5eAaPmAgNnS	stanovit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
pravidlech	pravidlo	k1gNnPc6	pravidlo
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
je	být	k5eAaImIp3nS	být
příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
č.	č.	k?	č.
30	[number]	k4	30
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
dvěma	dva	k4xCgInPc7	dva
různými	různý	k2eAgInPc7d1	různý
názvy	název	k1gInPc7	název
<g/>
:	:	kIx,	:
výstražná	výstražný	k2eAgFnSc1d1	výstražná
dopravní	dopravní	k2eAgFnSc1d1	dopravní
značka	značka	k1gFnSc1	značka
A	a	k9	a
7	[number]	k4	7
<g/>
b	b	k?	b
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Zpomalovací	zpomalovací	k2eAgInSc1d1	zpomalovací
práh	práh	k1gInSc1	práh
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
měla	mít	k5eAaImAgFnS	mít
podobu	podoba	k1gFnSc4	podoba
značky	značka	k1gFnSc2	značka
"	"	kIx"	"
<g/>
Nerovnost	nerovnost	k1gFnSc1	nerovnost
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
"	"	kIx"	"
s	s	k7c7	s
textovou	textový	k2eAgFnSc7d1	textová
dodatkovou	dodatkový	k2eAgFnSc7d1	dodatková
tabulkou	tabulka	k1gFnSc7	tabulka
"	"	kIx"	"
<g/>
Příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
<g/>
"	"	kIx"	"
a	a	k8xC	a
dodatkovou	dodatkový	k2eAgFnSc7d1	dodatková
tabulkou	tabulka	k1gFnSc7	tabulka
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
samo	sám	k3xTgNnSc1	sám
dopravní	dopravní	k2eAgNnSc1d1	dopravní
zařízení	zařízení	k1gNnSc1	zařízení
Z	z	k7c2	z
12	[number]	k4	12
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
"	"	kIx"	"
<g/>
Krátký	krátký	k2eAgInSc1d1	krátký
příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krátký	krátký	k2eAgInSc1d1	krátký
příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
v	v	k7c4	v
§	§	k?	§
26	[number]	k4	26
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
písm	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
n	n	k0	n
<g/>
)	)	kIx)	)
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
30	[number]	k4	30
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gNnSc1	jeho
užití	užití	k1gNnSc1	užití
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
opomnělo	opomnět	k5eAaPmAgNnS	opomnět
stanovit	stanovit	k5eAaPmF	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
Prahy	Praha	k1gFnPc1	Praha
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
umístěny	umístit	k5eAaPmNgInP	umístit
i	i	k9	i
mimo	mimo	k7c4	mimo
vozovku	vozovka	k1gFnSc4	vozovka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
vodorovnou	vodorovný	k2eAgFnSc7d1	vodorovná
dopravní	dopravní	k2eAgFnSc7d1	dopravní
značkou	značka	k1gFnSc7	značka
V	v	k7c6	v
13	[number]	k4	13
<g/>
a	a	k8xC	a
"	"	kIx"	"
<g/>
Šikmé	šikmý	k2eAgFnPc1d1	šikmá
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
čáry	čára	k1gFnPc1	čára
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uvedení	uvedení	k1gNnSc2	uvedení
příčného	příčný	k2eAgInSc2d1	příčný
prahu	práh	k1gInSc2	práh
mezi	mezi	k7c7	mezi
dopravními	dopravní	k2eAgNnPc7d1	dopravní
zařízeními	zařízení	k1gNnPc7	zařízení
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
výkladu	výklad	k1gInSc6	výklad
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
nebo	nebo	k8xC	nebo
odstranění	odstranění	k1gNnSc4	odstranění
prahu	práh	k1gInSc2	práh
je	být	k5eAaImIp3nS	být
stanovením	stanovení	k1gNnSc7	stanovení
místní	místní	k2eAgFnSc2d1	místní
úpravy	úprava	k1gFnSc2	úprava
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
o	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
dopravních	dopravní	k2eAgFnPc2d1	dopravní
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Příčné	příčný	k2eAgInPc1d1	příčný
prahy	práh	k1gInPc1	práh
ve	v	k7c6	v
vyznačené	vyznačený	k2eAgFnSc6d1	vyznačená
obytné	obytný	k2eAgFnSc6d1	obytná
zóně	zóna	k1gFnSc6	zóna
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
informativní	informativní	k2eAgFnSc7d1	informativní
dopravní	dopravní	k2eAgFnSc7d1	dopravní
značkou	značka	k1gFnSc7	značka
IP	IP	kA	IP
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgFnPc1d1	technická
podmínky	podmínka	k1gFnPc1	podmínka
TP-85	TP-85	k1gFnSc2	TP-85
v	v	k7c6	v
části	část	k1gFnSc6	část
věnované	věnovaný	k2eAgNnSc1d1	věnované
názvosloví	názvosloví	k1gNnSc1	názvosloví
(	(	kIx(	(
<g/>
1.2	[number]	k4	1.2
<g/>
)	)	kIx)	)
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zpomalovací	zpomalovací	k2eAgInSc1d1	zpomalovací
práh	práh	k1gInSc1	práh
je	být	k5eAaImIp3nS	být
stavebně-dopravní	stavebněopravní	k2eAgNnSc4d1	stavebně-dopravní
zařízení	zařízení	k1gNnSc4	zařízení
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
rychlostí	rychlost	k1gFnPc2	rychlost
vozidel	vozidlo	k1gNnPc2	vozidlo
nebo	nebo	k8xC	nebo
k	k	k7c3	k
dodržování	dodržování	k1gNnSc3	dodržování
dovolených	dovolený	k2eAgFnPc2d1	dovolená
rychlostí	rychlost	k1gFnPc2	rychlost
vozidel	vozidlo	k1gNnPc2	vozidlo
na	na	k7c6	na
místních	místní	k2eAgFnPc6d1	místní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zařízení	zařízení	k1gNnSc1	zařízení
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
řidiče	řidič	k1gMnPc4	řidič
nejen	nejen	k6eAd1	nejen
opticky	opticky	k6eAd1	opticky
a	a	k8xC	a
zvukově	zvukově	k6eAd1	zvukově
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
fyzicky	fyzicky	k6eAd1	fyzicky
umělou	umělý	k2eAgFnSc7d1	umělá
změnou	změna	k1gFnSc7	změna
výškových	výškový	k2eAgFnPc2d1	výšková
podmínek	podmínka	k1gFnPc2	podmínka
na	na	k7c6	na
vozovce	vozovka	k1gFnSc6	vozovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
průjezd	průjezd	k1gInSc4	průjezd
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odlišení	odlišení	k1gNnSc3	odlišení
od	od	k7c2	od
opticko-akustických	optickokustický	k2eAgFnPc2d1	opticko-akustický
brzd	brzda	k1gFnPc2	brzda
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
nadvýšení	nadvýšení	k1gNnSc1	nadvýšení
15	[number]	k4	15
mm	mm	kA	mm
jako	jako	k8xS	jako
hraniční	hraniční	k2eAgFnSc1d1	hraniční
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
zpomalovací	zpomalovací	k2eAgInPc1d1	zpomalovací
prahy	práh	k1gInPc1	práh
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
Široký	široký	k2eAgInSc1d1	široký
příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
Úzký	úzký	k2eAgInSc1d1	úzký
příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
Zpomalovací	zpomalovací	k2eAgInSc1d1	zpomalovací
polštář	polštář	k1gInSc1	polštář
Široký	široký	k2eAgInSc1d1	široký
příčný	příčný	k2eAgInSc1d1	příčný
práh	práh	k1gInSc1	práh
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
s	s	k7c7	s
přechodem	přechod	k1gInSc7	přechod
pro	pro	k7c4	pro
pěší	pěší	k1gMnPc4	pěší
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
plocha	plocha	k1gFnSc1	plocha
Podle	podle	k7c2	podle
technických	technický	k2eAgFnPc2d1	technická
podmínek	podmínka	k1gFnPc2	podmínka
TP	TP	kA	TP
85	[number]	k4	85
se	se	k3xPyFc4	se
zpomalovací	zpomalovací	k2eAgInPc1d1	zpomalovací
prahy	práh	k1gInPc1	práh
používají	používat	k5eAaImIp3nP	používat
zásadně	zásadně	k6eAd1	zásadně
jen	jen	k9	jen
na	na	k7c6	na
místních	místní	k2eAgFnPc6d1	místní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
funkčních	funkční	k2eAgFnPc2d1	funkční
tříd	třída	k1gFnPc2	třída
D1	D1	k1gFnSc2	D1
a	a	k8xC	a
C1	C1	k1gFnSc2	C1
až	až	k9	až
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
i	i	k9	i
na	na	k7c6	na
průtazích	průtah	k1gInPc6	průtah
silnic	silnice	k1gFnPc2	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
tříd	třída	k1gFnPc2	třída
obcemi	obec	k1gFnPc7	obec
(	(	kIx(	(
<g/>
funkční	funkční	k2eAgFnSc1d1	funkční
třída	třída	k1gFnSc1	třída
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
větší	veliký	k2eAgFnSc1d2	veliký
intenzita	intenzita	k1gFnSc1	intenzita
chodců	chodec	k1gMnPc2	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Výjimky	výjimka	k1gFnPc4	výjimka
může	moct	k5eAaImIp3nS	moct
schválit	schválit	k5eAaPmF	schválit
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
etapě	etapa	k1gFnSc6	etapa
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
trati	trať	k1gFnSc2	trať
na	na	k7c6	na
Černokostelecké	Černokostelecký	k2eAgFnSc6d1	Černokostelecká
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
použity	použit	k2eAgInPc4d1	použit
příčné	příčný	k2eAgInPc4d1	příčný
prahy	práh	k1gInPc4	práh
ke	k	k7c3	k
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
zákazu	zákaz	k1gInSc2	zákaz
vjezdu	vjezd	k1gInSc2	vjezd
na	na	k7c4	na
zatravněný	zatravněný	k2eAgInSc4d1	zatravněný
traťový	traťový	k2eAgInSc4d1	traťový
svršek	svršek	k1gInSc4	svršek
tramvajového	tramvajový	k2eAgInSc2d1	tramvajový
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Takzvané	takzvaný	k2eAgNnSc1d1	takzvané
"	"	kIx"	"
<g/>
pozitivně	pozitivně	k6eAd1	pozitivně
negativní	negativní	k2eAgInPc1d1	negativní
retardéry	retardér	k1gInPc1	retardér
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc1d1	tvořen
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
retardérem	retardér	k1gInSc7	retardér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
t.	t.	k?	t.
j.	j.	k?	j.
příčným	příčný	k2eAgInSc7d1	příčný
prahem	práh	k1gInSc7	práh
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
cca	cca	kA	cca
3	[number]	k4	3
cm	cm	kA	cm
mezi	mezi	k7c7	mezi
kolejnicemi	kolejnice	k1gFnPc7	kolejnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
negativním	negativní	k2eAgInSc7d1	negativní
retardérem	retardér	k1gInSc7	retardér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
t.	t.	k?	t.
j.	j.	k?	j.
asi	asi	k9	asi
1	[number]	k4	1
m	m	kA	m
širokým	široký	k2eAgInSc7d1	široký
pásem	pás	k1gInSc7	pás
otevřeného	otevřený	k2eAgInSc2d1	otevřený
svršku	svršek	k1gInSc2	svršek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
prohlubně	prohlubeň	k1gFnPc1	prohlubeň
oproti	oproti	k7c3	oproti
niveletě	niveleta	k1gFnSc3	niveleta
hlavy	hlava	k1gFnSc2	hlava
kolejnic	kolejnice	k1gFnPc2	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
Zpomalovací	zpomalovací	k2eAgInPc1d1	zpomalovací
prahy	práh	k1gInPc1	práh
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
provedeních	provedení	k1gNnPc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
firma	firma	k1gFnSc1	firma
Doznač	Doznač	k1gInSc4	Doznač
nabízí	nabízet	k5eAaImIp3nS	nabízet
plastové	plastový	k2eAgNnSc1d1	plastové
<g/>
,	,	kIx,	,
kovovou	kovový	k2eAgFnSc7d1	kovová
kostrou	kostra	k1gFnSc7	kostra
vyztužené	vyztužený	k2eAgInPc1d1	vyztužený
dílce	dílec	k1gInPc1	dílec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
vozovce	vozovka	k1gFnSc3	vozovka
připevňují	připevňovat	k5eAaImIp3nP	připevňovat
pomocí	pomocí	k7c2	pomocí
vrutů	vrut	k1gInPc2	vrut
nebo	nebo	k8xC	nebo
šroubů	šroub	k1gInPc2	šroub
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgNnSc4d2	vyšší
provedení	provedení	k1gNnSc4	provedení
(	(	kIx(	(
<g/>
výška	výška	k1gFnSc1	výška
60	[number]	k4	60
mm	mm	kA	mm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
udávána	udáván	k2eAgFnSc1d1	udávána
nominální	nominální	k2eAgFnSc1d1	nominální
rychlost	rychlost	k1gFnSc1	rychlost
10	[number]	k4	10
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
pro	pro	k7c4	pro
nižší	nízký	k2eAgNnSc4d2	nižší
provedení	provedení	k1gNnSc4	provedení
(	(	kIx(	(
<g/>
výška	výška	k1gFnSc1	výška
50	[number]	k4	50
mm	mm	kA	mm
<g/>
)	)	kIx)	)
20	[number]	k4	20
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jízdy	jízda	k1gFnSc2	jízda
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
prahu	práh	k1gInSc6	práh
430	[number]	k4	430
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
včetně	včetně	k7c2	včetně
montáže	montáž	k1gFnSc2	montáž
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
2000	[number]	k4	2000
Kč	Kč	kA	Kč
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
metr	metr	k1gInSc4	metr
šířky	šířka	k1gFnSc2	šířka
prahu	práh	k1gInSc2	práh
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
ADOZ	ADOZ	kA	ADOZ
nabízí	nabízet	k5eAaImIp3nS	nabízet
tři	tři	k4xCgFnPc4	tři
typové	typový	k2eAgFnPc4d1	typová
řady	řada	k1gFnPc4	řada
<g/>
:	:	kIx,	:
vysoce	vysoce	k6eAd1	vysoce
účinný	účinný	k2eAgInSc1d1	účinný
typ	typ	k1gInSc1	typ
CZ	CZ	kA	CZ
(	(	kIx(	(
<g/>
šířka	šířka	k1gFnSc1	šířka
po	po	k7c6	po
směru	směr	k1gInSc6	směr
jízdy	jízda	k1gFnSc2	jízda
je	být	k5eAaImIp3nS	být
430	[number]	k4	430
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
hlučný	hlučný	k2eAgInSc1d1	hlučný
typ	typ	k1gInSc1	typ
SES	SES	kA	SES
(	(	kIx(	(
<g/>
šířka	šířka	k1gFnSc1	šířka
po	po	k7c6	po
směru	směr	k1gInSc6	směr
jízdy	jízda	k1gFnSc2	jízda
je	být	k5eAaImIp3nS	být
600	[number]	k4	600
až	až	k9	až
900	[number]	k4	900
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpomalovací	zpomalovací	k2eAgInSc1d1	zpomalovací
polštář	polštář	k1gInSc1	polštář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provedení	provedení	k1gNnSc1	provedení
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
mají	mít	k5eAaImIp3nP	mít
výšku	výška	k1gFnSc4	výška
30	[number]	k4	30
mm	mm	kA	mm
<g/>
,	,	kIx,	,
provedení	provedení	k1gNnSc6	provedení
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
20	[number]	k4	20
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
nebo	nebo	k8xC	nebo
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
mají	mít	k5eAaImIp3nP	mít
výšku	výška	k1gFnSc4	výška
50	[number]	k4	50
mm	mm	kA	mm
a	a	k8xC	a
provedení	provedení	k1gNnSc2	provedení
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
10	[number]	k4	10
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
má	mít	k5eAaImIp3nS	mít
výšku	výška	k1gFnSc4	výška
60	[number]	k4	60
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Kwesto	Kwesta	k1gMnSc5	Kwesta
dodává	dodávat	k5eAaImIp3nS	dodávat
varianty	variant	k1gInPc4	variant
prahů	práh	k1gInPc2	práh
pro	pro	k7c4	pro
rychlosti	rychlost	k1gFnPc4	rychlost
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
a	a	k8xC	a
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
o	o	k7c6	o
výškách	výška	k1gFnPc6	výška
60	[number]	k4	60
<g/>
,	,	kIx,	,
50	[number]	k4	50
a	a	k8xC	a
30	[number]	k4	30
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pokynech	pokyn	k1gInPc6	pokyn
k	k	k7c3	k
montáži	montáž	k1gFnSc3	montáž
bývá	bývat	k5eAaImIp3nS	bývat
upozornění	upozornění	k1gNnSc1	upozornění
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
narušeno	narušen	k2eAgNnSc4d1	narušeno
odvodnění	odvodnění	k1gNnSc4	odvodnění
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Automatický	automatický	k2eAgInSc1d1	automatický
zpomalovací	zpomalovací	k2eAgInSc1d1	zpomalovací
práh	práh	k1gInSc1	práh
Podélný	podélný	k2eAgInSc1d1	podélný
dělicí	dělicí	k2eAgInSc4d1	dělicí
práh	práh	k1gInSc4	práh
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
svislé	svislý	k2eAgNnSc4d1	svislé
vychylování	vychylování	k1gNnSc4	vychylování
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc1	Wikidat
<g/>
:	:	kIx,	:
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Vertical	Vertical	k1gFnSc1	Vertical
deflection	deflection	k1gInSc4	deflection
devices	devices	k1gInSc1	devices
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Speed	Speed	k1gInSc1	Speed
bumps	bumps	k0	bumps
Zpomalovací	zpomalovací	k2eAgInPc4d1	zpomalovací
prahy	práh	k1gInPc7	práh
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Technická	technický	k2eAgFnSc1d1	technická
správa	správa	k1gFnSc1	správa
komunikací	komunikace	k1gFnPc2	komunikace
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
Retardéry	retardér	k1gInPc1	retardér
<g/>
,	,	kIx,	,
diskuse	diskuse	k1gFnPc1	diskuse
na	na	k7c6	na
webu	web	k1gInSc6	web
města	město	k1gNnSc2	město
Kadaň	Kadaň	k1gFnSc4	Kadaň
Absurdní	absurdní	k2eAgInSc1d1	absurdní
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Polanský	Polanský	k1gMnSc1	Polanský
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Polanka	polanka	k1gFnSc1	polanka
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
Dvacítky	dvacítka	k1gFnSc2	dvacítka
v	v	k7c4	v
Sokolovské	sokolovský	k2eAgNnSc4d1	Sokolovské
III	III	kA	III
<g/>
:	:	kIx,	:
Platí	platit	k5eAaImIp3nS	platit
jiné	jiný	k2eAgInPc4d1	jiný
zákony	zákon	k1gInPc4	zákon
ve	v	k7c6	v
Vysočanech	Vysočany	k1gInPc6	Vysočany
než	než	k8xS	než
na	na	k7c6	na
Břevnově	Břevnov	k1gInSc6	Břevnov
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
web	web	k1gInSc4	web
Preference	preference	k1gFnSc2	preference
pražských	pražský	k2eAgFnPc2d1	Pražská
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
Zpomalovací	zpomalovací	k2eAgInPc1d1	zpomalovací
prahy	práh	k1gInPc1	práh
<g/>
,	,	kIx,	,
Nadace	nadace	k1gFnSc1	nadace
Partnerství	partnerství	k1gNnSc2	partnerství
</s>
