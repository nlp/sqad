<s>
Side	Side	k6eAd1
</s>
<s>
Side	Side	k1gFnSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
36	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
31	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
185	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
Side	Side	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
11	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Správa	správa	k1gFnSc1
PSČ	PSČ	kA
</s>
<s>
07	#num#	k4
330	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Side	Sidat	k5eAaPmIp3nS
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Σ	Σ	k?
<g/>
,	,	kIx,
Sidé	Sidé	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
antického	antický	k2eAgInSc2d1
původu	původ	k1gInSc2
v	v	k7c6
jihoturecké	jihoturecký	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
Antalya	Antaly	k1gInSc2
v	v	k7c6
okrese	okres	k1gInSc6
Manavgat	Manavgat	k1gInSc1
ležící	ležící	k2eAgInSc1d1
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Turecka	Turecko	k1gNnSc2
u	u	k7c2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
asi	asi	k9
75	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Antalye	Antaly	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Side	Side	k6eAd1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
rekreační	rekreační	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
teplých	teplý	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
vyhledávané	vyhledávaný	k2eAgMnPc1d1
rekreanty	rekreant	k1gMnPc7
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
písčité	písčitý	k2eAgFnPc4d1
pláže	pláž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
dobře	dobře	k6eAd1
zachované	zachovaný	k2eAgFnPc4d1
zříceniny	zřícenina	k1gFnPc4
z	z	k7c2
římského	římský	k2eAgNnSc2d1
období	období	k1gNnSc2
patří	patřit	k5eAaImIp3nS
město	město	k1gNnSc1
k	k	k7c3
nejvýznamnějším	významný	k2eAgNnPc3d3
archeologickým	archeologický	k2eAgNnPc3d1
nalezištím	naleziště	k1gNnPc3
Turecka	Turecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Historické	historický	k2eAgInPc1d1
prameny	pramen	k1gInPc1
udávají	udávat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c4
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
město	město	k1gNnSc1
mělo	mít	k5eAaImAgNnS
přístav	přístav	k1gInSc4
<g/>
,	,	kIx,
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgNnPc2d3
měst	město	k1gNnPc2
Pamfýlie	Pamfýlie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
významným	významný	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
zakladatelé	zakladatel	k1gMnPc1
města	město	k1gNnSc2
postupně	postupně	k6eAd1
přejali	přejmout	k5eAaPmAgMnP
jazyk	jazyk	k1gInSc4
místního	místní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samo	sám	k3xTgNnSc1
jméno	jméno	k1gNnSc1
Side	Side	k1gNnSc7
zřejmě	zřejmě	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
místního	místní	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
řecky	řecky	k6eAd1
znamená	znamenat	k5eAaImIp3nS
granátové	granátový	k2eAgNnSc4d1
jablko	jablko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgFnPc4d1
mince	mince	k1gFnPc4
zobrazovaly	zobrazovat	k5eAaImAgInP
hlavu	hlava	k1gFnSc4
bohyně	bohyně	k1gFnSc2
Pallas	Pallas	k1gMnSc1
Athény	Athéna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
století	století	k1gNnSc2
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
pod	pod	k7c7
nadvládou	nadvláda	k1gFnSc7
Lýdie	Lýdia	k1gFnSc2
a	a	k8xC
Persie	Persie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
333	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
města	město	k1gNnSc2
zmocnil	zmocnit	k5eAaPmAgMnS
Alexandr	Alexandr	k1gMnSc1
Makedonský	makedonský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
helénistická	helénistický	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
trvala	trvat	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
Side	Sid	k1gFnSc2
po	po	k7c6
Alexandrově	Alexandrův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
spravoval	spravovat	k5eAaImAgMnS
jedem	jed	k1gInSc7
z	z	k7c2
Alexandrových	Alexandrův	k2eAgMnPc2d1
generálů	generál	k1gMnPc2
Ptolemaios	Ptolemaios	k1gMnSc1
I	I	kA
Sótér	Sótér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
dvou	dva	k4xCgNnPc6
stoletích	století	k1gNnPc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
několika	několik	k4yIc3
dalším	další	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
v	v	k7c6
držení	držení	k1gNnSc6
města	město	k1gNnSc2
<g/>
,	,	kIx,
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
78	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
přešlo	přejít	k5eAaPmAgNnS
město	město	k1gNnSc1
do	do	k7c2
rukou	ruka	k1gFnPc2
Římanů	Říman	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
císaře	císař	k1gMnSc2
Augusta	August	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
25	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
Pamfýlie	Pamfýlie	k1gFnSc1
i	i	k9
se	s	k7c7
Side	Side	k1gFnSc7
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
součástí	součást	k1gFnSc7
provincie	provincie	k1gFnSc2
Galacie	Galacie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Side	Sid	k1gInSc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
centrem	centrum	k1gNnSc7
obchodu	obchod	k1gInSc2
s	s	k7c7
otroky	otrok	k1gMnPc7
ve	v	k7c6
Středomoří	středomoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
dochovaných	dochovaný	k2eAgFnPc2d1
antických	antický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
pochází	pocházet	k5eAaImIp3nS
právě	právě	k9
z	z	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
prosperity	prosperita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
se	se	k3xPyFc4
Side	Side	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
období	období	k1gNnSc4
postupného	postupný	k2eAgInSc2d1
poklesu	pokles	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
silné	silný	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
neodolaly	odolat	k5eNaPmAgFnP
ničivým	ničivý	k2eAgNnSc7d1
nájezdům	nájezd	k1gInPc3
kočovníků	kočovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
války	válka	k1gFnPc4
mezi	mezi	k7c4
křesťany	křesťan	k1gMnPc4
a	a	k8xC
Araby	Arab	k1gMnPc4
i	i	k8xC
ničivé	ničivý	k2eAgNnSc4d1
zemětřesení	zemětřesení	k1gNnSc4
způsobily	způsobit	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
místo	místo	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
opuštěno	opustit	k5eAaPmNgNnS
a	a	k8xC
obyvatelé	obyvatel	k1gMnPc1
se	se	k3xPyFc4
odstěhovali	odstěhovat	k5eAaPmAgMnP
do	do	k7c2
Antalye	Antaly	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakrátko	nakrátko	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
definitivně	definitivně	k6eAd1
<g/>
,	,	kIx,
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
obnoveno	obnovit	k5eAaPmNgNnS
ještě	ještě	k9
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dobytí	dobytí	k1gNnSc2
seldžuckými	seldžucký	k2eAgInPc7d1
Turky	turek	k1gInPc7
ve	v	k7c4
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
definitivně	definitivně	k6eAd1
opuštěno	opustit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
historie	historie	k1gFnSc1
Side	Sid	k1gFnSc2
začala	začít	k5eAaPmAgFnS
v	v	k7c6
19	#num#	k4
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
usadili	usadit	k5eAaPmAgMnP
Turci	Turek	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
opustili	opustit	k5eAaPmAgMnP
Krétu	Kréta	k1gFnSc4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
jejím	její	k3xOp3gNnSc7
přičleněním	přičlenění	k1gNnSc7
k	k	k7c3
Řecku	Řecko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osada	osada	k1gFnSc1
později	pozdě	k6eAd2
získala	získat	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
zaniklého	zaniklý	k2eAgNnSc2d1
antického	antický	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Side	Side	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
nárůstem	nárůst	k1gInSc7
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Antalye	Antalye	k1gNnSc2
<g/>
,	,	kIx,
střediska	středisko	k1gNnSc2
Turecké	turecký	k2eAgFnSc2d1
riviéry	riviéra	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
následně	následně	k6eAd1
i	i	k9
Side	Side	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
centrem	centr	k1gInSc7
letní	letní	k2eAgFnSc2d1
rekreace	rekreace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
hotely	hotel	k1gInPc1
vznikly	vzniknout	k5eAaPmAgInP
na	na	k7c6
pobřežním	pobřežní	k2eAgInSc6d1
pásu	pás	k1gInSc6
a	a	k8xC
nezasáhly	zasáhnout	k5eNaPmAgInP
výrazněji	výrazně	k6eAd2
do	do	k7c2
původního	původní	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
Památky	památka	k1gFnPc1
Side	Sid	k1gFnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
na	na	k7c6
výběžku	výběžek	k1gInSc6
pevniny	pevnina	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
i	i	k9
antický	antický	k2eAgInSc1d1
přístav	přístav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
poloostrov	poloostrov	k1gInSc4
je	být	k5eAaImIp3nS
cca	cca	kA
1	#num#	k4
km	km	kA
dlouhý	dlouhý	k2eAgMnSc1d1
a	a	k8xC
400	#num#	k4
m	m	kA
široký	široký	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antické	antický	k2eAgNnSc1d1
město	město	k1gNnSc1
chránilo	chránit	k5eAaImAgNnS
proti	proti	k7c3
útokům	útok	k1gInPc3
z	z	k7c2
pevniny	pevnina	k1gFnSc2
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
opevnění	opevnění	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
zčásti	zčásti	k6eAd1
dodnes	dodnes	k6eAd1
zachovalo	zachovat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
těchto	tento	k3xDgNnPc2
míst	místo	k1gNnPc2
míří	mířit	k5eAaImIp3nS
místní	místní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
zahájit	zahájit	k5eAaPmF
prohlídku	prohlídka	k1gFnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejbližší	blízký	k2eAgFnSc7d3
památkou	památka	k1gFnSc7
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
před	před	k7c7
hradbami	hradba	k1gFnPc7
starověkého	starověký	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Nymfeum	Nymfeum	k1gInSc1
<g/>
,	,	kIx,
původně	původně	k6eAd1
velká	velký	k2eAgFnSc1d1
fontána	fontána	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Nymfea	nymfea	k1gFnSc1
za	za	k7c7
hradbami	hradba	k1gFnPc7
města	město	k1gNnSc2
se	se	k3xPyFc4
cesty	cesta	k1gFnPc1
rozdvojují	rozdvojovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
sledují	sledovat	k5eAaImIp3nP
původní	původní	k2eAgFnSc4d1
antickou	antický	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
a	a	k8xC
byly	být	k5eAaImAgInP
vyzdobeny	vyzdobit	k5eAaPmNgInP
sloupořadím	sloupořadí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levá	levá	k1gFnSc1
cesta	cesta	k1gFnSc1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
části	část	k1gFnSc3
antického	antický	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
(	(	kIx(
<g/>
agora	agora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
veřejné	veřejný	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
blízkosti	blízkost	k1gFnSc6
této	tento	k3xDgFnSc2
cesty	cesta	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
stavba	stavba	k1gFnSc1
z	z	k7c2
byzantského	byzantský	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
účel	účel	k1gInSc4
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
jasný	jasný	k2eAgInSc1d1
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
hospitál	hospitál	k1gInSc4
<g/>
/	/	kIx~
<g/>
nemocnici	nemocnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravý	pravý	k2eAgInSc1d1
<g/>
,	,	kIx,
přímý	přímý	k2eAgInSc1d1
směr	směr	k1gInSc1
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
nejvýznamnějším	významný	k2eAgFnPc3d3
dochovaným	dochovaný	k2eAgFnPc3d1
památkám	památka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
ulice	ulice	k1gFnSc2
jsou	být	k5eAaImIp3nP
zbytky	zbytek	k1gInPc1
původních	původní	k2eAgNnPc2d1
obydlí	obydlí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
nachází	nacházet	k5eAaImIp3nS
komerční	komerční	k2eAgFnSc1d1
část	část	k1gFnSc1
agory	agora	k1gFnSc2
se	s	k7c7
zbytky	zbytek	k1gInPc7
chrámů	chrám	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
fórem	fórum	k1gNnSc7
se	se	k3xPyFc4
vypíná	vypínat	k5eAaImIp3nS
dobře	dobře	k6eAd1
zachované	zachovaný	k2eAgNnSc1d1
římské	římský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
(	(	kIx(
<g/>
theatron	theatron	k1gInSc1
<g/>
)	)	kIx)
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc4d1
takovou	takový	k3xDgFnSc7
stavbou	stavba	k1gFnSc7
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Středomoří	středomoří	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
hlediště	hlediště	k1gNnSc1
nebylo	být	k5eNaImAgNnS
vyhloubeno	vyhloubit	k5eAaPmNgNnS
ve	v	k7c6
svahu	svah	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
vystavěno	vystavěn	k2eAgNnSc1d1
do	do	k7c2
výšky	výška	k1gFnSc2
z	z	k7c2
kamene	kámen	k1gInSc2
(	(	kIx(
<g/>
obdobně	obdobně	k6eAd1
jako	jako	k8xC,k8xS
Kolosseum	Kolosseum	k1gInSc4
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
tohoto	tento	k3xDgNnSc2
řešení	řešení	k1gNnSc2
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
Side	Side	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
rovině	rovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
opačné	opačný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
divadla	divadlo	k1gNnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
zbytky	zbytek	k1gInPc7
městských	městský	k2eAgFnPc2d1
lázní	lázeň	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
je	být	k5eAaImIp3nS
po	po	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
umístěno	umístěn	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
antických	antický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
špičce	špička	k1gFnSc6
poloostrova	poloostrov	k1gInSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
sloupy	sloup	k1gInPc1
Apollónova	Apollónův	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
a	a	k8xC
zbytky	zbytek	k1gInPc4
Afroditina	Afroditin	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
jakož	jakož	k8xC
i	i	k8xC
zříceniny	zřícenina	k1gFnPc1
antických	antický	k2eAgFnPc2d1
lázní	lázeň	k1gFnPc2
a	a	k8xC
křesťanské	křesťanský	k2eAgFnPc1d1
baziliky	bazilika	k1gFnPc1
z	z	k7c2
byzantského	byzantský	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
blízkosti	blízkost	k1gFnSc6
těchto	tento	k3xDgFnPc2
staveb	stavba	k1gFnPc2
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgInS
antický	antický	k2eAgInSc1d1
přístav	přístav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
dnes	dnes	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
přístaviště	přístaviště	k1gNnSc1
jachet	jachta	k1gFnPc2
</s>
<s>
Počasí	počasí	k1gNnSc1
</s>
<s>
Side	Side	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
specifickou	specifický	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
do	do	k7c2
počasí	počasí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denní	denní	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
se	se	k3xPyFc4
zde	zde	k6eAd1
pohybují	pohybovat	k5eAaImIp3nP
okolo	okolo	k7c2
35	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
přímořským	přímořský	k2eAgNnSc7d1
klimatem	klima	k1gNnSc7
činí	činit	k5eAaImIp3nS
ideální	ideální	k2eAgNnSc4d1
počasí	počasí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejteplejší	teplý	k2eAgInPc1d3
měsíce	měsíc	k1gInPc1
v	v	k7c6
roce	rok	k1gInSc6
jsou	být	k5eAaImIp3nP
červenec	červenec	k1gInSc4
a	a	k8xC
srpen	srpen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
nejsou	být	k5eNaImIp3nP
ani	ani	k8xC
teploty	teplota	k1gFnPc1
nad	nad	k7c7
40	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgFnSc2d1
statistiky	statistika	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
přes	přes	k7c4
léto	léto	k1gNnSc4
zde	zde	k6eAd1
sprchne	sprchnout	k5eAaPmIp3nS
maximálně	maximálně	k6eAd1
jednou	jeden	k4xCgFnSc7
do	do	k7c2
měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
obvykle	obvykle	k6eAd1
proprší	propršet	k5eAaPmIp3nS
polovina	polovina	k1gFnSc1
měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Side	Side	k1gNnSc1
<g/>
,	,	kIx,
Nympheum	Nympheum	k1gNnSc1
-	-	kIx~
trosky	troska	k1gFnPc1
antické	antický	k2eAgFnPc1d1
fontány	fontána	k1gFnPc1
před	před	k7c7
branami	brána	k1gFnPc7
antického	antický	k2eAgInSc2d1
města	město	k1gNnSc2
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
brána	brána	k1gFnSc1
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
opevnění	opevnění	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
chránilo	chránit	k5eAaImAgNnS
antické	antický	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
zřícenina	zřícenina	k1gFnSc1
hospitálu	hospitál	k1gInSc2
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
agora	agora	k1gFnSc1
-	-	kIx~
komerční	komerční	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
antického	antický	k2eAgNnSc2d1
města	město	k1gNnSc2
</s>
<s>
Side	Sid	k1gFnPc1
<g/>
,	,	kIx,
ruiny	ruina	k1gFnPc1
kruhového	kruhový	k2eAgInSc2d1
chrámku	chrámek	k1gInSc2
na	na	k7c6
agoře	agora	k1gFnSc6
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
do	do	k7c2
amfiteátru	amfiteátr	k1gInSc2
směrem	směr	k1gInSc7
od	od	k7c2
agory	agora	k1gFnSc2
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
monumentální	monumentální	k2eAgFnSc1d1
brána	brána	k1gFnSc1
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
amfiteátru	amfiteátr	k1gInSc2
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
vnější	vnější	k2eAgFnSc1d1
<g/>
,	,	kIx,
vyvýšená	vyvýšený	k2eAgFnSc1d1
část	část	k1gFnSc1
amfiteátru	amfiteátr	k1gInSc2
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
do	do	k7c2
interiéru	interiér	k1gInSc2
amfiteátru	amfiteátr	k1gInSc2
</s>
<s>
Side	Side	k1gNnSc1
<g/>
,	,	kIx,
atrium	atrium	k1gNnSc1
archeologického	archeologický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
</s>
<s>
Side	Side	k1gInSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgInSc1d1
sál	sál	k1gInSc1
archeologického	archeologický	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
</s>
<s>
Side	Sid	k1gFnPc1
<g/>
,	,	kIx,
ruiny	ruina	k1gFnPc1
Apollónova	Apollónův	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
průčelní	průčelní	k2eAgFnSc1d1
fasáda	fasáda	k1gFnSc1
Apollónova	Apollónův	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
</s>
<s>
Side	Sid	k1gFnPc1
<g/>
,	,	kIx,
Medúzy	Medúza	k1gFnPc1
z	z	k7c2
Apollónova	Apollónův	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
v	v	k7c6
místním	místní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
rekonstrukce	rekonstrukce	k1gFnSc1
chrámu	chrám	k1gInSc2
bohyně	bohyně	k1gFnSc2
Atény	Atény	k1gFnPc1
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
bazilika	bazilika	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
pozadí	pozadí	k1gNnSc6
Apollónův	Apollónův	k2eAgInSc1d1
chrám	chrám	k1gInSc1
</s>
<s>
Side	Side	k1gFnSc1
<g/>
,	,	kIx,
přístav	přístav	k1gInSc1
jachet	jachta	k1gFnPc2
<g/>
,	,	kIx,
místo	místo	k7c2
antického	antický	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Side	Sid	k1gInSc2
<g/>
,	,	kIx,
Turkey	Turke	k2eAgInPc1d1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Side	Sid	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2010009865	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
237291019	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2010009865	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
