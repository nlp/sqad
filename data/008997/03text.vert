<p>
<s>
Výbojka	výbojka	k1gFnSc1	výbojka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
trubice	trubice	k1gFnSc1	trubice
<g/>
,	,	kIx,	,
naplněná	naplněný	k2eAgFnSc1d1	naplněná
směsí	směs	k1gFnSc7	směs
různých	různý	k2eAgFnPc2d1	různá
par	para	k1gFnPc2	para
a	a	k8xC	a
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
výbojky	výbojka	k1gFnSc2	výbojka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
trubice	trubice	k1gFnSc2	trubice
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
z	z	k7c2	z
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
elektrod	elektroda	k1gFnPc2	elektroda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zavedení	zavedení	k1gNnSc4	zavedení
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
do	do	k7c2	do
plynové	plynový	k2eAgFnSc2d1	plynová
náplně	náplň	k1gFnSc2	náplň
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
využití	využití	k1gNnSc1	využití
výbojek	výbojka	k1gFnPc2	výbojka
je	být	k5eAaImIp3nS	být
přeměna	přeměna	k1gFnSc1	přeměna
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tlaku	tlak	k1gInSc2	tlak
plynové	plynový	k2eAgFnSc2d1	plynová
náplně	náplň	k1gFnSc2	náplň
výbojky	výbojka	k1gFnSc2	výbojka
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
vysokotlaké	vysokotlaký	k2eAgNnSc4d1	vysokotlaké
(	(	kIx(	(
<g/>
sodíkové	sodíkový	k2eAgNnSc4d1	sodíkové
<g/>
,	,	kIx,	,
rtuťové	rtuťový	k2eAgNnSc4d1	rtuťové
<g/>
,	,	kIx,	,
halogenidové	halogenidový	k2eAgNnSc4d1	halogenidový
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
nízkotlaké	nízkotlaký	k2eAgNnSc4d1	nízkotlaké
(	(	kIx(	(
<g/>
rtuťové	rtuťový	k2eAgNnSc4d1	rtuťové
<g/>
,	,	kIx,	,
sodíkové	sodíkový	k2eAgNnSc4d1	sodíkové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
těleso	těleso	k1gNnSc1	těleso
výbojky	výbojka	k1gFnSc2	výbojka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
korundu	korund	k1gInSc2	korund
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
výbojky	výbojka	k1gFnPc4	výbojka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
tzv.	tzv.	kA	tzv.
obloukové	obloukový	k2eAgFnSc2d1	oblouková
lampy	lampa	k1gFnSc2	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
těleso	těleso	k1gNnSc1	těleso
osvětlovacích	osvětlovací	k2eAgFnPc2d1	osvětlovací
výbojek	výbojka	k1gFnPc2	výbojka
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
baňce	baňka	k1gFnSc6	baňka
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
luminoforem	luminofor	k1gInSc7	luminofor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
čiré	čirý	k2eAgFnPc1d1	čirá
<g/>
.	.	kIx.	.
</s>
<s>
Baňka	baňka	k1gFnSc1	baňka
bývá	bývat	k5eAaImIp3nS	bývat
plněna	plnit	k5eAaImNgFnS	plnit
inertní	inertní	k2eAgFnSc7d1	inertní
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vakuovaná	vakuovaný	k2eAgFnSc1d1	vakuovaná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
tepelných	tepelný	k2eAgFnPc2d1	tepelná
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rtuťové	rtuťový	k2eAgFnPc1d1	rtuťová
nízkotlaké	nízkotlaký	k2eAgFnPc1d1	nízkotlaká
výbojky	výbojka	k1gFnPc1	výbojka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
zářivky	zářivka	k1gFnPc1	zářivka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
výbojek	výbojka	k1gFnPc2	výbojka
určených	určený	k2eAgFnPc2d1	určená
jako	jako	k8xC	jako
osvětlovací	osvětlovací	k2eAgInPc1d1	osvětlovací
existují	existovat	k5eAaImIp3nP	existovat
výbojky	výbojka	k1gFnPc1	výbojka
usměrňovací	usměrňovací	k2eAgFnSc2d1	usměrňovací
určené	určený	k2eAgFnSc2d1	určená
k	k	k7c3	k
usměrňování	usměrňování	k1gNnSc3	usměrňování
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
baňku	baňka	k1gFnSc4	baňka
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
případě	případ	k1gInSc6	případ
větších	veliký	k2eAgFnPc2d2	veliký
výbojek	výbojka	k1gFnPc2	výbojka
o	o	k7c4	o
kovovou	kovový	k2eAgFnSc4d1	kovová
komoru	komora	k1gFnSc4	komora
<g/>
,	,	kIx,	,
vyplněnou	vyplněný	k2eAgFnSc4d1	vyplněná
inertním	inertní	k2eAgInSc7d1	inertní
plynem	plyn	k1gInSc7	plyn
(	(	kIx(	(
<g/>
s	s	k7c7	s
případnou	případný	k2eAgFnSc7d1	případná
příměsí	příměs	k1gFnSc7	příměs
par	para	k1gFnPc2	para
kovového	kovový	k2eAgInSc2d1	kovový
prvku	prvek	k1gInSc2	prvek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
izolovaně	izolovaně	k6eAd1	izolovaně
umístěná	umístěný	k2eAgFnSc1d1	umístěná
žhavá	žhavý	k2eAgFnSc1d1	žhavá
katoda	katoda	k1gFnSc1	katoda
a	a	k8xC	a
studená	studený	k2eAgFnSc1d1	studená
anoda	anoda	k1gFnSc1	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
elektrody	elektroda	k1gFnPc1	elektroda
jsou	být	k5eAaImIp3nP	být
vyvedeny	vyvést	k5eAaPmNgFnP	vyvést
skrz	skrz	k7c4	skrz
baňku	baňka	k1gFnSc4	baňka
ven	ven	k6eAd1	ven
ke	k	k7c3	k
kontaktům	kontakt	k1gInPc3	kontakt
na	na	k7c4	na
patici	patice	k1gFnSc4	patice
<g/>
.	.	kIx.	.
</s>
<s>
Katodu	katoda	k1gFnSc4	katoda
tvoří	tvořit	k5eAaImIp3nS	tvořit
nejčastěji	často	k6eAd3	často
silný	silný	k2eAgInSc1d1	silný
wolframový	wolframový	k2eAgInSc1d1	wolframový
drát	drát	k1gInSc1	drát
(	(	kIx(	(
<g/>
žhavený	žhavený	k2eAgMnSc1d1	žhavený
z	z	k7c2	z
pomocného	pomocný	k2eAgInSc2d1	pomocný
zdroje	zdroj	k1gInSc2	zdroj
<g/>
)	)	kIx)	)
pokrytý	pokrytý	k2eAgInSc1d1	pokrytý
speciální	speciální	k2eAgFnSc7d1	speciální
směsí	směs	k1gFnSc7	směs
oxidů	oxid	k1gInPc2	oxid
alkalických	alkalický	k2eAgInPc2d1	alkalický
prvků	prvek	k1gInPc2	prvek
schopných	schopný	k2eAgInPc2d1	schopný
emise	emise	k1gFnPc4	emise
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
oxidem	oxid	k1gInSc7	oxid
barnatým	barnatý	k2eAgInSc7d1	barnatý
<g/>
,	,	kIx,	,
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
a	a	k8xC	a
strontnatým	strontnatý	k2eAgInSc7d1	strontnatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anoda	anoda	k1gFnSc1	anoda
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
z	z	k7c2	z
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnSc2	ocel
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
přímo	přímo	k6eAd1	přímo
kovová	kovový	k2eAgFnSc1d1	kovová
stěna	stěna	k1gFnSc1	stěna
výbojky	výbojka	k1gFnSc2	výbojka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Usměrňovací	usměrňovací	k2eAgFnSc1d1	usměrňovací
výbojka	výbojka	k1gFnSc1	výbojka
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
v	v	k7c6	v
elektrickém	elektrický	k2eAgInSc6d1	elektrický
obvodu	obvod	k1gInSc6	obvod
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
elektronka	elektronka	k1gFnSc1	elektronka
–	–	k?	–
dioda	dioda	k1gFnSc1	dioda
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
průchod	průchod	k1gInSc1	průchod
elektronů	elektron	k1gInPc2	elektron
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
žhavé	žhavý	k2eAgFnSc2d1	žhavá
katody	katoda	k1gFnSc2	katoda
k	k	k7c3	k
anodě	anoda	k1gFnSc3	anoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgMnSc7	ten
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
zapojení	zapojení	k1gNnSc6	zapojení
do	do	k7c2	do
obvodu	obvod	k1gInSc2	obvod
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jednocestnému	jednocestný	k2eAgNnSc3d1	jednocestné
usměrnění	usměrnění	k1gNnSc3	usměrnění
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Usměrňovací	usměrňovací	k2eAgFnPc1d1	usměrňovací
výbojky	výbojka	k1gFnPc1	výbojka
se	se	k3xPyFc4	se
před	před	k7c7	před
hromadným	hromadný	k2eAgInSc7d1	hromadný
nástupem	nástup	k1gInSc7	nástup
výkonových	výkonový	k2eAgInPc2d1	výkonový
polovodičů	polovodič	k1gInPc2	polovodič
hojně	hojně	k6eAd1	hojně
užívaly	užívat	k5eAaImAgFnP	užívat
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Usměrňovací	usměrňovací	k2eAgFnPc1d1	usměrňovací
výbojky	výbojka	k1gFnPc1	výbojka
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgNnSc4d2	vyšší
napětí	napětí	k1gNnSc4	napětí
plnily	plnit	k5eAaImAgFnP	plnit
nejčastěji	často	k6eAd3	často
čistými	čistý	k2eAgFnPc7d1	čistá
parami	para	k1gFnPc7	para
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nízká	nízký	k2eAgNnPc4d1	nízké
napětí	napětí	k1gNnPc4	napětí
pak	pak	k8xC	pak
argonem	argon	k1gInSc7	argon
<g/>
.	.	kIx.	.
</s>
<s>
Výbojky	výbojka	k1gFnPc1	výbojka
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgNnPc4d1	malé
napětí	napětí	k1gNnPc4	napětí
(	(	kIx(	(
<g/>
používaná	používaný	k2eAgFnSc1d1	používaná
např.	např.	kA	např.
v	v	k7c6	v
nabíječkách	nabíječka	k1gFnPc6	nabíječka
autoakumulátorů	autoakumulátor	k1gInPc2	autoakumulátor
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označovaly	označovat	k5eAaImAgFnP	označovat
termínem	termín	k1gInSc7	termín
tungarové	tungarová	k1gFnSc2	tungarová
lampy	lampa	k1gFnSc2	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
předností	přednost	k1gFnSc7	přednost
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
klasickou	klasický	k2eAgFnSc7d1	klasická
vakuovou	vakuový	k2eAgFnSc7d1	vakuová
diodou	dioda	k1gFnSc7	dioda
byl	být	k5eAaImAgInS	být
menší	malý	k2eAgInSc1d2	menší
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
odpor	odpor	k1gInSc1	odpor
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc1d2	veliký
proudová	proudový	k2eAgFnSc1d1	proudová
zatížitelnost	zatížitelnost	k1gFnSc1	zatížitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stinným	stinný	k2eAgFnPc3d1	stinná
stránkám	stránka	k1gFnPc3	stránka
patřila	patřit	k5eAaImAgFnS	patřit
náchylnost	náchylnost	k1gFnSc1	náchylnost
na	na	k7c6	na
přehřátí	přehřátí	k1gNnSc6	přehřátí
plynové	plynový	k2eAgFnSc2d1	plynová
náplně	náplň	k1gFnSc2	náplň
a	a	k8xC	a
nutnost	nutnost	k1gFnSc4	nutnost
nažhavit	nažhavit	k5eAaPmF	nažhavit
katodu	katoda	k1gFnSc4	katoda
ještě	ještě	k9	ještě
před	před	k7c7	před
zapojením	zapojení	k1gNnSc7	zapojení
anodového	anodový	k2eAgInSc2d1	anodový
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
speciální	speciální	k2eAgFnPc1d1	speciální
výbojky	výbojka	k1gFnPc1	výbojka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
přepětí	přepětí	k1gNnSc2	přepětí
<g/>
,	,	kIx,	,
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xS	jako
jiskřiště	jiskřiště	k1gNnSc1	jiskřiště
nebo	nebo	k8xC	nebo
bleskojistka	bleskojistka	k1gFnSc1	bleskojistka
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
provozním	provozní	k2eAgNnSc6d1	provozní
napětí	napětí	k1gNnSc6	napětí
výbojkou	výbojka	k1gFnSc7	výbojka
proud	proud	k1gInSc1	proud
neprotéká	protékat	k5eNaImIp3nS	protékat
<g/>
,	,	kIx,	,
zápalné	zápalný	k2eAgNnSc1d1	zápalné
napětí	napětí	k1gNnSc1	napětí
výbojky	výbojka	k1gFnSc2	výbojka
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
napětí	napětí	k1gNnSc1	napětí
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
nad	nad	k7c4	nad
mez	mez	k1gFnSc4	mez
zápalného	zápalný	k2eAgNnSc2d1	zápalné
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
protékat	protékat	k5eAaImF	protékat
výbojkou	výbojka	k1gFnSc7	výbojka
dosti	dosti	k6eAd1	dosti
značný	značný	k2eAgInSc1d1	značný
proud	proud	k1gInSc1	proud
a	a	k8xC	a
napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
omezí	omezit	k5eAaPmIp3nS	omezit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Barvy	barva	k1gFnSc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
plyn	plyn	k1gInSc1	plyn
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
struktuře	struktura	k1gFnSc6	struktura
atomu	atom	k1gInSc2	atom
emituje	emitovat	k5eAaBmIp3nS	emitovat
určité	určitý	k2eAgFnPc4d1	určitá
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
září	zářit	k5eAaImIp3nP	zářit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
barvách	barva	k1gFnPc6	barva
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
způsob	způsob	k1gInSc1	způsob
hodnocení	hodnocení	k1gNnSc2	hodnocení
schopnosti	schopnost	k1gFnSc2	schopnost
světelného	světelný	k2eAgInSc2d1	světelný
zdroje	zdroj	k1gInSc2	zdroj
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
barvy	barva	k1gFnPc4	barva
různých	různý	k2eAgInPc2d1	různý
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
osvětlené	osvětlený	k2eAgInPc4d1	osvětlený
zdroji	zdroj	k1gInPc7	zdroj
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
osvětlování	osvětlování	k1gNnSc4	osvětlování
(	(	kIx(	(
<g/>
CIE	CIE	kA	CIE
<g/>
)	)	kIx)	)
představila	představit	k5eAaPmAgFnS	představit
index	index	k1gInSc4	index
podání	podání	k1gNnSc2	podání
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
plynové	plynový	k2eAgFnPc1d1	plynová
výbojky	výbojka	k1gFnPc1	výbojka
mají	mít	k5eAaImIp3nP	mít
relativně	relativně	k6eAd1	relativně
nízký	nízký	k2eAgInSc4d1	nízký
CRI	CRI	kA	CRI
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
barvy	barva	k1gFnPc1	barva
podstatně	podstatně	k6eAd1	podstatně
odlišné	odlišný	k2eAgFnPc1d1	odlišná
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgNnPc2d1	jiné
osvětlení	osvětlení	k1gNnPc2	osvětlení
s	s	k7c7	s
vysokám	vysokat	k5eAaPmIp1nS	vysokat
CRI	CRI	kA	CRI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zářivka	zářivka	k1gFnSc1	zářivka
</s>
</p>
<p>
<s>
Xenonová	xenonový	k2eAgFnSc1d1	xenonová
výbojka	výbojka	k1gFnSc1	výbojka
</s>
</p>
<p>
<s>
Doutnavka	doutnavka	k1gFnSc1	doutnavka
</s>
</p>
<p>
<s>
Elektronka	elektronka	k1gFnSc1	elektronka
</s>
</p>
