<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gInSc4
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gInSc4
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
,	,	kIx,
Operace	operace	k1gFnSc1
Forager	Foragra	k1gFnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gInSc4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc4
až	až	k9
1	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1944	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Tinian	Tinian	k1gInSc1
<g/>
,	,	kIx,
Mariany	Mariana	k1gFnPc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Americké	americký	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
USA	USA	kA
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Harry	Harra	k1gMnSc2
Schmidt	Schmidt	k1gMnSc1
</s>
<s>
Kjóči	Kjóč	k1gFnSc3
Ogata	Ogata	k1gMnSc1
†	†	k?
Kakudži	Kakudž	k1gFnSc6
Kakuta	Kakut	k1gMnSc2
†	†	k?
Goiči	Goič	k1gFnSc3
Oja	Oja	k1gMnSc1
†	†	k?
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
30	#num#	k4
000	#num#	k4
mariňáků	mariňáků	k?
</s>
<s>
4	#num#	k4
700	#num#	k4
vojáků	voják	k1gMnPc2
4	#num#	k4
110	#num#	k4
námořních	námořní	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
328	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
1	#num#	k4
571	#num#	k4
zraněných	zraněný	k1gMnPc2
</s>
<s>
přes	přes	k7c4
8	#num#	k4
010	#num#	k4
mrtvých	mrtvý	k1gMnPc2
313	#num#	k4
zajatých	zajatý	k1gMnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gInSc4
představovala	představovat	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc4d1
fázi	fáze	k1gFnSc4
amerického	americký	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
na	na	k7c6
Marianách	Mariana	k1gFnPc6
během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
všech	všecek	k3xTgInPc2
tří	tři	k4xCgInPc2
obojživelných	obojživelný	k2eAgInPc2d1
útoku	útok	k1gInSc3
na	na	k7c4
marianské	marianský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
Saipan	Saipany	k1gInPc2
<g/>
,	,	kIx,
Tinian	Tiniany	k1gInPc2
a	a	k8xC
Guam	Guama	k1gFnPc2
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
nejúspěšnější	úspěšný	k2eAgFnSc4d3
operaci	operace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
osm	osm	k4xCc4
dní	den	k1gInPc2
se	se	k3xPyFc4
Američanům	Američan	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
ostrov	ostrov	k1gInSc4
dobýt	dobýt	k5eAaPmF
a	a	k8xC
to	ten	k3xDgNnSc4
za	za	k7c4
cenu	cena	k1gFnSc4
328	#num#	k4
padlých	padlý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
útoky	útok	k1gInPc7
na	na	k7c4
Guam	Guam	k1gInSc4
a	a	k8xC
Saipan	Saipan	k1gInSc4
poměrně	poměrně	k6eAd1
málo	málo	k4c1
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
byl	být	k5eAaImAgInS
po	po	k7c4
dobytí	dobytí	k1gNnSc4
Američany	Američan	k1gMnPc4
přebudován	přebudován	k2eAgInSc1d1
na	na	k7c4
leteckou	letecký	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
startovaly	startovat	k5eAaBmAgFnP
bombardéry	bombardér	k1gInPc4
B-29	B-29	k1gMnPc2
na	na	k7c4
Japonsko	Japonsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
srpnu	srpen	k1gInSc6
1945	#num#	k4
také	také	k9
z	z	k7c2
ostrova	ostrov	k1gInSc2
odstartovala	odstartovat	k5eAaPmAgFnS
obě	dva	k4xCgNnPc4
letadla	letadlo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
shodila	shodit	k5eAaPmAgFnS
atomové	atomový	k2eAgFnPc4d1
bomby	bomba	k1gFnPc4
na	na	k7c4
Hirošimu	Hirošima	k1gFnSc4
a	a	k8xC
Nagasaki	Nagasaki	k1gNnSc4
<g/>
,	,	kIx,
B-29	B-29	k1gMnSc1
Enola	Enola	k1gMnSc1
Gay	gay	k1gMnSc1
a	a	k8xC
B-29	B-29	k1gMnSc1
Bockscar	Bockscar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Přípravy	příprava	k1gFnPc1
k	k	k7c3
vylodění	vylodění	k1gNnSc3
</s>
<s>
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Saipanu	Saipan	k1gInSc2
zbyly	zbýt	k5eAaPmAgFnP
na	na	k7c6
Marianách	Mariana	k1gFnPc6
pouze	pouze	k6eAd1
ostrovy	ostrov	k1gInPc1
Guam	Guama	k1gFnPc2
<g/>
,	,	kIx,
Tinian	Tiniana	k1gFnPc2
a	a	k8xC
Rota	rota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rota	rota	k1gFnSc1
byla	být	k5eAaImAgFnS
letecky	letecky	k6eAd1
neutralizována	neutralizován	k2eAgFnSc1d1
a	a	k8xC
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
na	na	k7c6
řadě	řada	k1gFnSc6
Tinian	Tiniana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tinian	Tinian	k1gInSc1
měl	mít	k5eAaImAgInS
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
byl	být	k5eAaImAgInS
ze	z	k7c2
všech	všecek	k3xTgInPc2
ostrovů	ostrov	k1gInPc2
na	na	k7c6
Marianách	Mariana	k1gFnPc6
nejméně	málo	k6eAd3
hornatý	hornatý	k2eAgMnSc1d1
a	a	k8xC
skýtal	skýtat	k5eAaImAgMnS
skvělé	skvělý	k2eAgNnSc4d1
místo	místo	k1gNnSc4
pro	pro	k7c4
zbudování	zbudování	k1gNnSc4
letišť	letiště	k1gNnPc2
a	a	k8xC
také	také	k6eAd1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
pouze	pouze	k6eAd1
tři	tři	k4xCgFnPc4
míle	míle	k1gFnPc4
jižně	jižně	k6eAd1
od	od	k7c2
Saipanu	Saipan	k1gInSc2
a	a	k8xC
znamenal	znamenat	k5eAaImAgInS
pro	pro	k7c4
něj	on	k3xPp3gInSc2
hrozbu	hrozba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc7
na	na	k7c6
jihu	jih	k1gInSc6
ostrova	ostrov	k1gInSc2
vybudovali	vybudovat	k5eAaPmAgMnP
dvě	dva	k4xCgFnPc4
letiště	letiště	k1gNnSc2
dlouhá	dlouhý	k2eAgFnSc1d1
1	#num#	k4
600	#num#	k4
m.	m.	k?
a	a	k8xC
třetí	třetí	k4xOgFnSc7
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
ve	v	k7c6
středu	střed	k1gInSc6
ostrova	ostrov	k1gInSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
ve	v	k7c6
výstavbě	výstavba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostrově	ostrov	k1gInSc6
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
dvě	dva	k4xCgNnPc4
místa	místo	k1gNnPc4
vhodná	vhodný	k2eAgFnSc1d1
k	k	k7c3
útoku	útok	k1gInSc3
<g/>
,	,	kIx,
neboť	neboť	k8xC
všude	všude	k6eAd1
jinde	jinde	k6eAd1
byly	být	k5eAaImAgInP
útesy	útes	k1gInPc4
i	i	k8xC
30	#num#	k4
m.	m.	k?
vysoké	vysoká	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc2d1
pláže	pláž	k1gFnSc2
byly	být	k5eAaImAgInP
nazvány	nazvat	k5eAaPmNgInP,k5eAaBmNgInP
Bílá	bílé	k1gNnPc4
1	#num#	k4
a	a	k8xC
Bílá	bílý	k2eAgFnSc1d1
2	#num#	k4
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc1d1
pláž	pláž	k1gFnSc1
byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
Žlutá	žlutat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
obou	dva	k4xCgMnPc2
se	se	k3xPyFc4
se	se	k3xPyFc4
vylodili	vylodit	k5eAaPmAgMnP
jednotky	jednotka	k1gFnPc1
žabích	žabí	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
že	že	k8xS
vhodnější	vhodný	k2eAgInSc4d2
pro	pro	k7c4
vylodění	vyloděný	k2eAgMnPc1d1
budou	být	k5eAaImBp3nP
bílé	bílý	k2eAgFnPc1d1
pláže	pláž	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
den	den	k1gInSc1
D	D	kA
byl	být	k5eAaImAgInS
stanoven	stanovit	k5eAaPmNgInS
24	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
útoku	útok	k1gInSc3
byly	být	k5eAaImAgFnP
vybrány	vybrán	k2eAgFnPc4d1
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vylodění	vylodění	k1gNnSc1
</s>
<s>
Kolem	kolem	k7c2
šesté	šestý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
ráno	ráno	k6eAd1
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
zahájil	zahájit	k5eAaPmAgInS
fingovaný	fingovaný	k2eAgInSc1d1
útok	útok	k1gInSc1
Demonstrační	demonstrační	k2eAgInSc1d1
oddíl	oddíl	k1gInSc4
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Tinian	Tiniana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plukovník	plukovník	k1gMnSc1
Ogata	Ogata	k1gMnSc1
si	se	k3xPyFc3
myslel	myslet	k5eAaImAgMnS
že	že	k8xS
k	k	k7c3
vylodění	vylodění	k1gNnSc3
dojde	dojít	k5eAaPmIp3nS
tam	tam	k6eAd1
a	a	k8xC
proto	proto	k8xC
když	když	k8xS
v	v	k7c4
půl	půl	k1xP
osmé	osmý	k4xOgFnSc2
začal	začít	k5eAaPmAgInS
útok	útok	k1gInSc4
na	na	k7c6
Bílých	bílý	k2eAgFnPc6d1
plážích	pláž	k1gFnPc6
nebyly	být	k5eNaImAgFnP
tam	tam	k6eAd1
síly	síla	k1gFnPc1
<g/>
,	,	kIx,
co	co	k9
by	by	kYmCp3nP
je	on	k3xPp3gNnSc4
zastavily	zastavit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pláži	pláž	k1gFnSc6
Bílá	bílý	k2eAgFnSc1d1
1	#num#	k4
proběhlo	proběhnout	k5eAaPmAgNnS
vylodění	vylodění	k1gNnSc1
bez	bez	k7c2
větších	veliký	k2eAgInPc2d2
problémů	problém	k1gInPc2
a	a	k8xC
Američané	Američan	k1gMnPc1
postoupili	postoupit	k5eAaPmAgMnP
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pláži	pláž	k1gFnSc6
Bílá	bílý	k2eAgFnSc1d1
2	#num#	k4
bránily	bránit	k5eAaImAgFnP
postupu	postup	k1gInSc2
dva	dva	k4xCgInPc1
betonové	betonový	k2eAgInPc1d1
bunkry	bunkr	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
povedlo	povést	k5eAaPmAgNnS
zneškodnit	zneškodnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonci	Japonec	k1gMnPc1
však	však	k9
měli	mít	k5eAaImAgMnP
v	v	k7c6
tomto	tento	k3xDgInSc6
prostoru	prostor	k1gInSc6
děla	dělo	k1gNnSc2
a	a	k8xC
minomety	minomet	k1gInPc4
a	a	k8xC
ty	ten	k3xDgMnPc4
neustále	neustále	k6eAd1
střílely	střílet	k5eAaImAgInP
na	na	k7c4
americké	americký	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
večera	večer	k1gInSc2
bylo	být	k5eAaImAgNnS
na	na	k7c6
ostrově	ostrov	k1gInSc6
15	#num#	k4
000	#num#	k4
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Boj	boj	k1gInSc1
o	o	k7c4
ostrov	ostrov	k1gInSc4
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
24	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
provedly	provést	k5eAaPmAgInP
dva	dva	k4xCgInPc1
japonské	japonský	k2eAgInPc1d1
prapory	prapor	k1gInPc1
za	za	k7c2
podpory	podpora	k1gFnSc2
tanků	tank	k1gInPc2
protiútok	protiútok	k1gInSc1
ve	v	k7c6
třech	tři	k4xCgFnPc6
vlnách	vlna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlice	světlice	k1gFnSc1
z	z	k7c2
amerických	americký	k2eAgInPc2d1
torpédoborců	torpédoborec	k1gInPc2
osvětlily	osvětlit	k5eAaPmAgFnP
krajinu	krajina	k1gFnSc4
a	a	k8xC
Japonci	Japonec	k1gMnPc1
byli	být	k5eAaImAgMnP
pro	pro	k7c4
americké	americký	k2eAgInPc4d1
kulomety	kulomet	k1gInPc4
snadný	snadný	k2eAgInSc4d1
cíl	cíl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
byl	být	k5eAaImAgInS
bez	bez	k7c2
větších	veliký	k2eAgInPc2d2
problémů	problém	k1gInPc2
odražen	odražen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
se	se	k3xPyFc4
vylodila	vylodit	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
a	a	k8xC
tak	tak	k6eAd1
začal	začít	k5eAaPmAgInS
rychlý	rychlý	k2eAgInSc1d1
a	a	k8xC
nezadržitelný	zadržitelný	k2eNgInSc1d1
postup	postup	k1gInSc1
amerických	americký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
bylo	být	k5eAaImAgNnS
obsazeno	obsazen	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
u	u	k7c2
výběžku	výběžek	k1gInSc2
Uši	ucho	k1gNnPc1
a	a	k8xC
celá	celý	k2eAgFnSc1d1
severní	severní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tlak	tlak	k1gInSc1
amerických	americký	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
se	se	k3xPyFc4
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
obrátil	obrátit	k5eAaPmAgInS
k	k	k7c3
jihu	jih	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpoledne	odpoledne	k6eAd1
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
bylo	být	k5eAaImAgNnS
obsazeno	obsadit	k5eAaPmNgNnS
město	město	k1gNnSc1
Tinian	Tiniany	k1gInPc2
a	a	k8xC
poté	poté	k6eAd1
minolovky	minolovka	k1gFnPc1
rychle	rychle	k6eAd1
vyčistily	vyčistit	k5eAaPmAgFnP
Sunharonskou	Sunharonský	k2eAgFnSc4d1
zátoku	zátoka	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gFnPc4
pláže	pláž	k1gFnPc4
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
hlavním	hlavní	k2eAgNnSc7d1
výkladištěm	výkladiště	k1gNnSc7
zásob	zásoba	k1gFnPc2
a	a	k8xC
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelé	velitel	k1gMnPc1
předpokládali	předpokládat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
zahnaní	zahnaný	k2eAgMnPc1d1
na	na	k7c4
konec	konec	k1gInSc4
ostrova	ostrov	k1gInSc2
budou	být	k5eAaImBp3nP
bránit	bránit	k5eAaImF
do	do	k7c2
posledního	poslední	k2eAgInSc2d1
dechu	dech	k1gInSc2
<g/>
,	,	kIx,
proto	proto	k8xC
učinili	učinit	k5eAaPmAgMnP,k5eAaImAgMnP
všechna	všechen	k3xTgNnPc4
opatření	opatření	k1gNnPc4
aby	aby	kYmCp3nS
vojákům	voják	k1gMnPc3
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
pomohli	pomoct	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěrečný	závěrečný	k2eAgInSc4d1
útok	útok	k1gInSc4
provedli	provést	k5eAaPmAgMnP
ve	v	k7c6
dnech	den	k1gInPc6
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
vypořádat	vypořádat	k5eAaPmF
se	s	k7c7
silným	silný	k2eAgInSc7d1
odporem	odpor	k1gInSc7
japonských	japonský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
Japonci	Japonec	k1gMnPc1
provedli	provést	k5eAaPmAgMnP
poslední	poslední	k2eAgInPc4d1
organizované	organizovaný	k2eAgInPc4d1
protiútoky	protiútok	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
za	za	k7c2
pomoci	pomoc	k1gFnSc2
děl	dělo	k1gNnPc2
odraženy	odražen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgInSc2
dne	den	k1gInSc2
v	v	k7c6
18	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
generálmajor	generálmajor	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
ostrov	ostrov	k1gInSc4
za	za	k7c4
dobytý	dobytý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
bitvě	bitva	k1gFnSc6
</s>
<s>
Dobytí	dobytí	k1gNnSc1
ostrova	ostrov	k1gInSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
vždy	vždy	k6eAd1
kontroverzní	kontroverzní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Tinianu	Tinian	k1gInSc2
to	ten	k3xDgNnSc4
platilo	platit	k5eAaImAgNnS
obzvlášť	obzvlášť	k9
protože	protože	k8xS
se	se	k3xPyFc4
v	v	k7c6
džungli	džungle	k1gFnSc6
a	a	k8xC
v	v	k7c6
horách	hora	k1gFnPc6
skrývali	skrývat	k5eAaImAgMnP
japonští	japonský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
pokoušeli	pokoušet	k5eAaImAgMnP
vést	vést	k5eAaImF
gerilovou	gerilový	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
většina	většina	k1gFnSc1
se	se	k3xPyFc4
buď	buď	k8xC
zabila	zabít	k5eAaPmAgFnS
<g/>
,	,	kIx,
vzdala	vzdát	k5eAaPmAgFnS
nebo	nebo	k8xC
vyhladověla	vyhladovět	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
skrývalo	skrývat	k5eAaImAgNnS
i	i	k9
dlouho	dlouho	k6eAd1
po	po	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
nechtěli	chtít	k5eNaImAgMnP
uvěřit	uvěřit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
Japonsko	Japonsko	k1gNnSc1
kapitulovalo	kapitulovat	k5eAaBmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Malajsii	Malajsie	k1gFnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
•	•	k?
Boje	boj	k1gInPc4
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
Guadalcanal	Guadalcanal	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrova	ostrov	k1gInSc2
Savo	Savo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Šalomounů	Šalomoun	k1gMnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
mysu	mys	k1gInSc2
Esperance	Esperance	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrovů	ostrov	k1gInPc2
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Guadalcanalu	Guadalcanal	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tassafarongy	Tassafarong	k1gInPc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Rennellova	Rennellův	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Bismarckově	Bismarckův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Komandorských	Komandorský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Tarawu	Tarawus	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Operace	operace	k1gFnSc1
Forager	Forager	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Saipan	Saipan	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Peleliu	Pelelium	k1gNnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Leyte	Leyt	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Manilu	Manila	k1gFnSc4
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Iwodžimu	Iwodžimo	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Okinawu	Okinawa	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
