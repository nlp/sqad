<s desamb="1">
Za	za	k7c4
osm	osm	k4xCc4
dní	den	k1gInPc2
se	se	k3xPyFc4
Američanům	Američan	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
ostrov	ostrov	k1gInSc4
dobýt	dobýt	k5eAaPmF
a	a	k8xC
to	ten	k3xDgNnSc4
za	za	k7c4
cenu	cena	k1gFnSc4
328	#num#	k4
padlých	padlý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
útoky	útok	k1gInPc7
na	na	k7c4
Guam	Guam	k1gInSc4
a	a	k8xC
Saipan	Saipan	k1gInSc4
poměrně	poměrně	k6eAd1
málo	málo	k4c1
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>