<p>
<s>
Láhev	láhev	k1gFnSc1	láhev
(	(	kIx(	(
<g/>
nespisovně	spisovně	k6eNd1	spisovně
flaška	flaška	k1gFnSc1	flaška
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
die	die	k?	die
Flasche	Flasch	k1gInSc2	Flasch
<g/>
,	,	kIx,	,
přípustná	přípustný	k2eAgFnSc1d1	přípustná
je	být	k5eAaImIp3nS	být
i	i	k9	i
pravopisná	pravopisný	k2eAgFnSc1d1	pravopisná
varianta	varianta	k1gFnSc1	varianta
lahev	lahev	k1gFnSc1	lahev
<g/>
,	,	kIx,	,
vojenská	vojenský	k2eAgFnSc1d1	vojenská
polní	polní	k2eAgFnSc1d1	polní
láhev	láhev	k1gFnSc1	láhev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
čutora	čutora	k1gFnSc1	čutora
<g/>
,	,	kIx,	,
nespisovně	spisovně	k6eNd1	spisovně
feldflaška	feldflaška	k1gFnSc1	feldflaška
–	–	k?	–
das	das	k?	das
Feld	Feld	k1gInSc1	Feld
–	–	k?	–
něm.	něm.	k?	něm.
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
dutý	dutý	k2eAgInSc1d1	dutý
zásobník	zásobník	k1gInSc1	zásobník
s	s	k7c7	s
přístupovým	přístupový	k2eAgInSc7d1	přístupový
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
úzkým	úzký	k2eAgNnSc7d1	úzké
hrdlem	hrdlo	k1gNnSc7	hrdlo
a	a	k8xC	a
širším	široký	k2eAgNnSc7d2	širší
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Otvor	otvor	k1gInSc1	otvor
bývá	bývat	k5eAaImIp3nS	bývat
uzavíratelný	uzavíratelný	k2eAgInSc1d1	uzavíratelný
zátkou	zátka	k1gFnSc7	zátka
či	či	k8xC	či
špuntem	špuntem	k?	špuntem
<g/>
,	,	kIx,	,
šroubovacím	šroubovací	k2eAgInSc7d1	šroubovací
uzávěrem	uzávěr	k1gInSc7	uzávěr
nebo	nebo	k8xC	nebo
jednorázovým	jednorázový	k2eAgNnSc7d1	jednorázové
plechovým	plechový	k2eAgNnSc7d1	plechové
víčkem	víčko	k1gNnSc7	víčko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
strojově	strojově	k6eAd1	strojově
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
přes	přes	k7c4	přes
obrubu	obruba	k1gFnSc4	obruba
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lahve	lahev	k1gFnPc1	lahev
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
nejčastěji	často	k6eAd3	často
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
plastu	plast	k1gInSc2	plast
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
primárně	primárně	k6eAd1	primárně
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
úschově	úschova	k1gFnSc3	úschova
kapalin	kapalina	k1gFnPc2	kapalina
jako	jako	k8xS	jako
(	(	kIx(	(
<g/>
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
nápoje	nápoj	k1gInPc1	nápoj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
kečup	kečup	k1gInSc1	kečup
<g/>
,	,	kIx,	,
ředidlo	ředidlo	k1gNnSc1	ředidlo
<g/>
,	,	kIx,	,
šampon	šampon	k1gInSc1	šampon
<g/>
,	,	kIx,	,
inkoust	inkoust	k1gInSc1	inkoust
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
skladování	skladování	k1gNnSc6	skladování
plynů	plyn	k1gInPc2	plyn
(	(	kIx(	(
<g/>
tlakové	tlakový	k2eAgFnPc4d1	tlaková
lahve	lahev	k1gFnPc4	lahev
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ocelové	ocelový	k2eAgInPc1d1	ocelový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termínem	termín	k1gInSc7	termín
polní	polní	k2eAgFnSc1d1	polní
láhev	láhev	k1gFnSc1	láhev
neboli	neboli	k8xC	neboli
čutora	čutora	k1gFnSc1	čutora
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
speciální	speciální	k2eAgFnSc1d1	speciální
plechová	plechový	k2eAgFnSc1d1	plechová
láhev	láhev	k1gFnSc1	láhev
z	z	k7c2	z
výzbroje	výzbroj	k1gFnSc2	výzbroj
pěšího	pěší	k1gMnSc2	pěší
vojáka	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
čutory	čutora	k1gFnSc2	čutora
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
používány	používat	k5eAaImNgFnP	používat
při	při	k7c6	při
výletech	výlet	k1gInPc6	výlet
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
či	či	k8xC	či
při	při	k7c6	při
táboření	táboření	k1gNnSc6	táboření
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
myslivost	myslivost	k1gFnSc1	myslivost
<g/>
,	,	kIx,	,
tramping	tramping	k1gInSc1	tramping
<g/>
,	,	kIx,	,
skauting	skauting	k1gInSc1	skauting
<g/>
,	,	kIx,	,
turistika	turistika	k1gFnSc1	turistika
<g/>
)	)	kIx)	)
coby	coby	k?	coby
nádoby	nádoba	k1gFnPc1	nádoba
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc4	čaj
či	či	k8xC	či
jiné	jiný	k2eAgInPc4d1	jiný
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
kojenců	kojenec	k1gMnPc2	kojenec
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
malá	malý	k2eAgFnSc1d1	malá
speciální	speciální	k2eAgFnSc1d1	speciální
lahev	lahev	k1gFnSc1	lahev
na	na	k7c4	na
mléko	mléko	k1gNnSc4	mléko
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
kojenecká	kojenecký	k2eAgFnSc1d1	kojenecká
láhev	láhev	k1gFnSc1	láhev
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
lahví	lahev	k1gFnSc7	lahev
je	být	k5eAaImIp3nS	být
i	i	k9	i
skleněná	skleněný	k2eAgFnSc1d1	skleněná
nádoba	nádoba	k1gFnSc1	nádoba
na	na	k7c4	na
víno	víno	k1gNnSc4	víno
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
demižon	demižon	k1gInSc1	demižon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
malých	malý	k2eAgFnPc2d1	malá
skleněných	skleněný	k2eAgFnPc2d1	skleněná
lahviček	lahvička	k1gFnPc2	lahvička
bývají	bývat	k5eAaImIp3nP	bývat
ukládána	ukládat	k5eAaImNgNnP	ukládat
též	též	k9	též
léčiva	léčivo	k1gNnPc4	léčivo
<g/>
,	,	kIx,	,
těmto	tento	k3xDgFnPc3	tento
lahvičkám	lahvička	k1gFnPc3	lahvička
se	se	k3xPyFc4	se
říkává	říkávat	k5eAaImIp3nS	říkávat
lékovky	lékovka	k1gFnSc2	lékovka
<g/>
.	.	kIx.	.
</s>
<s>
Parfémy	parfém	k1gInPc1	parfém
jsou	být	k5eAaImIp3nP	být
plněny	plnit	k5eAaImNgInP	plnit
do	do	k7c2	do
flaconů	flacon	k1gInPc2	flacon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
lahví	lahev	k1gFnPc2	lahev
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
znovu	znovu	k6eAd1	znovu
použít	použít	k5eAaPmF	použít
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
lahve	lahev	k1gFnSc2	lahev
skleněné	skleněný	k2eAgFnSc2d1	skleněná
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zpětně	zpětně	k6eAd1	zpětně
vykupovány	vykupován	k2eAgFnPc1d1	vykupována
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
končí	končit	k5eAaImIp3nP	končit
v	v	k7c6	v
recyklačních	recyklační	k2eAgInPc6d1	recyklační
kontejnerech	kontejner	k1gInPc6	kontejner
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
následně	následně	k6eAd1	následně
vyráběny	vyráběn	k2eAgInPc4d1	vyráběn
nové	nový	k2eAgInPc4d1	nový
výrobky	výrobek	k1gInPc4	výrobek
(	(	kIx(	(
<g/>
láhve	láhev	k1gFnPc4	láhev
z	z	k7c2	z
umělé	umělý	k2eAgFnSc2d1	umělá
hmoty	hmota	k1gFnSc2	hmota
neboli	neboli	k8xC	neboli
PET	PET	kA	PET
lahve	lahev	k1gFnSc2	lahev
<g/>
,	,	kIx,	,
skleněné	skleněný	k2eAgFnSc2d1	skleněná
láhve	láhev	k1gFnSc2	láhev
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
až	až	k9	až
80	[number]	k4	80
procent	procento	k1gNnPc2	procento
recyklovaného	recyklovaný	k2eAgNnSc2d1	recyklované
skla	sklo	k1gNnSc2	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
bandaska	bandaska	k1gFnSc1	bandaska
</s>
</p>
<p>
<s>
vratná	vratný	k2eAgFnSc1d1	vratná
láhev	láhev	k1gFnSc1	láhev
</s>
</p>
<p>
<s>
pivní	pivní	k2eAgFnSc1d1	pivní
láhev	láhev	k1gFnSc1	láhev
</s>
</p>
<p>
<s>
láhev	láhev	k1gFnSc1	láhev
na	na	k7c4	na
víno	víno	k1gNnSc4	víno
</s>
</p>
<p>
<s>
PET	PET	kA	PET
láhev	láhev	k1gFnSc1	láhev
</s>
</p>
<p>
<s>
kojenecká	kojenecký	k2eAgFnSc1d1	kojenecká
lahev	lahev	k1gFnSc1	lahev
</s>
</p>
<p>
<s>
polní	polní	k2eAgFnSc1d1	polní
láhev	láhev	k1gFnSc1	láhev
neboli	neboli	k8xC	neboli
čutora	čutora	k1gFnSc1	čutora
</s>
</p>
<p>
<s>
termoska	termoska	k1gFnSc1	termoska
</s>
</p>
<p>
<s>
konvička	konvička	k1gFnSc1	konvička
(	(	kIx(	(
<g/>
na	na	k7c4	na
mléko	mléko	k1gNnSc4	mléko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tlaková	tlakový	k2eAgFnSc1d1	tlaková
láhev	láhev	k1gFnSc1	láhev
</s>
</p>
<p>
<s>
gumová	gumový	k2eAgFnSc1d1	gumová
zahřívací	zahřívací	k2eAgFnSc1d1	zahřívací
láhev	láhev	k1gFnSc1	láhev
–	–	k?	–
termofor	termofor	k1gInSc1	termofor
</s>
</p>
<p>
<s>
Molotovův	Molotovův	k2eAgInSc4d1	Molotovův
koktejl	koktejl	k1gInSc4	koktejl
</s>
</p>
<p>
<s>
leydenská	leydenský	k2eAgFnSc1d1	leydenská
láhev	láhev	k1gFnSc1	láhev
–	–	k?	–
první	první	k4xOgInSc4	první
záměrně	záměrně	k6eAd1	záměrně
konstruovaný	konstruovaný	k2eAgInSc4d1	konstruovaný
kondenzátor	kondenzátor	k1gInSc4	kondenzátor
</s>
</p>
<p>
<s>
Kleinova	Kleinův	k2eAgFnSc1d1	Kleinova
láhev	láhev	k1gFnSc1	láhev
–	–	k?	–
čtyřrozměrný	čtyřrozměrný	k2eAgInSc1d1	čtyřrozměrný
geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
</s>
</p>
<p>
<s>
sklenice	sklenice	k1gFnSc1	sklenice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
láhev	láhev	k1gFnSc1	láhev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
láhev	láhev	k1gFnSc1	láhev
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
