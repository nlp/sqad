<p>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
poloostrov	poloostrov	k1gInSc1	poloostrov
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
poloostrov	poloostrov	k1gInSc1	poloostrov
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
města	město	k1gNnSc2	město
Seattle	Seattle	k1gFnSc2	Seattle
jej	on	k3xPp3gMnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Pugetův	Pugetův	k2eAgInSc1d1	Pugetův
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
úžina	úžina	k1gFnSc1	úžina
Juana	Juan	k1gMnSc2	Juan
de	de	k?	de
Fucy	Fuca	k1gMnSc2	Fuca
<g/>
.	.	kIx.	.
</s>
<s>
Álavův	Álavův	k2eAgInSc1d1	Álavův
mys	mys	k1gInSc1	mys
<g/>
,	,	kIx,	,
nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc4	bod
pevninských	pevninský	k2eAgInPc2d1	pevninský
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
mys	mys	k1gInSc4	mys
Flattery	Flatter	k1gInPc7	Flatter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nejseverozápadnějším	severozápadný	k2eAgInSc7d3	severozápadný
bodem	bod	k1gInSc7	bod
pevninských	pevninský	k2eAgInPc2d1	pevninský
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
ležely	ležet	k5eAaImAgFnP	ležet
jedny	jeden	k4xCgInPc4	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
neprozkoumaných	prozkoumaný	k2eNgNnPc2d1	neprozkoumané
míst	místo	k1gNnPc2	místo
pevninské	pevninský	k2eAgFnSc2d1	pevninská
části	část	k1gFnSc2	část
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
nebyla	být	k5eNaImAgFnS	být
zmapována	zmapovat	k5eAaPmNgFnS	zmapovat
před	před	k7c7	před
prací	práce	k1gFnSc7	práce
Arthura	Arthur	k1gMnSc2	Arthur
Dodwella	Dodwell	k1gMnSc2	Dodwell
a	a	k8xC	a
Theodora	Theodor	k1gMnSc2	Theodor
Rixona	Rixon	k1gMnSc2	Rixon
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
dokončili	dokončit	k5eAaPmAgMnP	dokončit
mapu	mapa	k1gFnSc4	mapa
poloostrova	poloostrov	k1gInSc2	poloostrov
s	s	k7c7	s
topografií	topografie	k1gFnSc7	topografie
a	a	k8xC	a
zdroji	zdroj	k1gInPc7	zdroj
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Poloostrov	poloostrov	k1gInSc1	poloostrov
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
deštných	deštný	k2eAgInPc2d1	deštný
pralesů	prales	k1gInPc2	prales
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
Hohský	Hohský	k2eAgInSc1d1	Hohský
prales	prales	k1gInSc1	prales
a	a	k8xC	a
Quinaultský	Quinaultský	k2eAgInSc1d1	Quinaultský
prales	prales	k1gInSc1	prales
<g/>
.	.	kIx.	.
</s>
<s>
Hustá	hustý	k2eAgFnSc1d1	hustá
pralesní	pralesní	k2eAgFnSc1d1	pralesní
vegetace	vegetace	k1gFnSc1	vegetace
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
především	především	k9	především
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
východ	východ	k1gInSc1	východ
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
srážkovém	srážkový	k2eAgInSc6d1	srážkový
stínu	stín	k1gInSc6	stín
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
vlhké	vlhký	k2eAgNnSc4d1	vlhké
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
poloostrova	poloostrov	k1gInSc2	poloostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Olympijské	olympijský	k2eAgNnSc4d1	Olympijské
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Mount	Mount	k1gMnSc1	Mount
Olympus	Olympus	k1gMnSc1	Olympus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
lososovité	lososovitý	k2eAgFnPc4d1	lososovitá
řeky	řeka	k1gFnPc4	řeka
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
patří	patřit	k5eAaImIp3nS	patřit
Humptulips	Humptulips	k1gInSc1	Humptulips
<g/>
,	,	kIx,	,
Quinault	Quinault	k1gInSc1	Quinault
<g/>
,	,	kIx,	,
Queets	Queets	k1gInSc1	Queets
<g/>
,	,	kIx,	,
Quillayute	Quillayut	k1gMnSc5	Quillayut
<g/>
,	,	kIx,	,
Bogachiel	Bogachiel	k1gInSc1	Bogachiel
<g/>
,	,	kIx,	,
Sol	sol	k1gInSc1	sol
Duc	duc	k0	duc
<g/>
,	,	kIx,	,
Lyre	Lyr	k1gFnPc1	Lyr
<g/>
,	,	kIx,	,
Elwha	Elwha	k1gFnSc1	Elwha
<g/>
,	,	kIx,	,
Dungeness	Dungeness	k1gInSc1	Dungeness
<g/>
,	,	kIx,	,
Dosewallips	Dosewallips	k1gInSc1	Dosewallips
<g/>
,	,	kIx,	,
Hamma	Hamma	k1gFnSc1	Hamma
Hamma	Hamma	k1gFnSc1	Hamma
<g/>
,	,	kIx,	,
Skokomish	Skokomish	k1gInSc1	Skokomish
a	a	k8xC	a
Wynoochee	Wynoochee	k1gInSc1	Wynoochee
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
přírodní	přírodní	k2eAgNnPc4d1	přírodní
jezera	jezero	k1gNnPc4	jezero
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
patří	patřit	k5eAaImIp3nS	patřit
Kitsapovo	Kitsapův	k2eAgNnSc1d1	Kitsapův
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Půlměsícové	půlměsícový	k2eAgNnSc1d1	půlměsícový
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Ozette	Ozett	k1gMnSc5	Ozett
<g/>
,	,	kIx,	,
Sutherlandovo	Sutherlandův	k2eAgNnSc1d1	Sutherlandovo
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Quinaultské	Quinaultský	k2eAgNnSc1d1	Quinaultský
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
Příjemné	příjemný	k2eAgNnSc1d1	příjemné
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čtyřech	čtyři	k4xCgFnPc6	čtyři
přehrazených	přehrazený	k2eAgFnPc6d1	přehrazená
řekách	řeka	k1gFnPc6	řeka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
mj.	mj.	kA	mj.
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Aldwellovo	Aldwellův	k2eAgNnSc1d1	Aldwellův
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Millsovo	Millsův	k2eAgNnSc1d1	Millsův
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
Cushmanovo	Cushmanův	k2eAgNnSc1d1	Cushmanův
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
státních	státní	k2eAgInPc2d1	státní
parků	park	k1gInPc2	park
nachází	nacházet	k5eAaImIp3nS	nacházet
Olympijský	olympijský	k2eAgInSc1d1	olympijský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
a	a	k8xC	a
Olympijský	olympijský	k2eAgInSc1d1	olympijský
národní	národní	k2eAgInSc1d1	národní
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pět	pět	k4xCc1	pět
divočin	divočina	k1gFnPc2	divočina
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
divočina	divočina	k1gFnSc1	divočina
The	The	k1gFnSc2	The
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
,	,	kIx,	,
Buckhornská	Buckhornský	k2eAgFnSc1d1	Buckhornský
divočina	divočina	k1gFnSc1	divočina
<g/>
,	,	kIx,	,
divočina	divočina	k1gFnSc1	divočina
plukovníka	plukovník	k1gMnSc2	plukovník
Boba	Bob	k1gMnSc2	Bob
<g/>
,	,	kIx,	,
divočina	divočina	k1gFnSc1	divočina
Mt	Mt	k1gFnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Skokomish	Skokomish	k1gInSc1	Skokomish
a	a	k8xC	a
divočina	divočina	k1gFnSc1	divočina
Wonder	Wondra	k1gFnPc2	Wondra
Mountain	Mountaina	k1gFnPc2	Mountaina
<g/>
.	.	kIx.	.
</s>
<s>
Kousek	kousek	k1gInSc1	kousek
od	od	k7c2	od
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
poloostrova	poloostrov	k1gInSc2	poloostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
divočina	divočina	k1gFnSc1	divočina
Washingtonovy	Washingtonův	k2eAgInPc4d1	Washingtonův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nyní	nyní	k6eAd1	nyní
probíhá	probíhat	k5eAaImIp3nS	probíhat
další	další	k2eAgFnSc1d1	další
snaha	snaha	k1gFnSc1	snaha
chránit	chránit	k5eAaImF	chránit
další	další	k2eAgFnPc4d1	další
divočiny	divočina	k1gFnPc4	divočina
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
chránit	chránit	k5eAaImF	chránit
lososovité	lososovitý	k2eAgInPc4d1	lososovitý
vodní	vodní	k2eAgInPc4d1	vodní
toky	tok	k1gInPc4	tok
pod	pod	k7c7	pod
zákonem	zákon	k1gInSc7	zákon
divokých	divoký	k2eAgInPc2d1	divoký
a	a	k8xC	a
malebných	malebný	k2eAgFnPc2d1	malebná
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
poskytnout	poskytnout	k5eAaPmF	poskytnout
národnímu	národní	k2eAgInSc3d1	národní
parku	park	k1gInSc6	park
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
odkoupení	odkoupení	k1gNnSc3	odkoupení
další	další	k2eAgFnSc2d1	další
půdy	půda	k1gFnSc2	půda
od	od	k7c2	od
nynějších	nynější	k2eAgMnPc2d1	nynější
vlastníků	vlastník	k1gMnPc2	vlastník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celé	celý	k2eAgInPc4d1	celý
okresy	okres	k1gInPc4	okres
Clallam	Clallam	k1gInSc1	Clallam
<g/>
,	,	kIx,	,
Kitsap	Kitsap	k1gMnSc1	Kitsap
<g/>
,	,	kIx,	,
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
okresů	okres	k1gInPc2	okres
Grays	Grays	k1gInSc1	Grays
Harbor	Harbor	k1gMnSc1	Harbor
a	a	k8xC	a
Mason	mason	k1gMnSc1	mason
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
Kitsapův	Kitsapův	k2eAgInSc1d1	Kitsapův
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
části	část	k1gFnSc2	část
poloostrova	poloostrov	k1gInSc2	poloostrov
oddělen	oddělit	k5eAaPmNgInS	oddělit
Hoodovým	Hoodův	k2eAgInSc7d1	Hoodův
kanálem	kanál	k1gInSc7	kanál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
státu	stát	k1gInSc2	stát
Olympie	Olympia	k1gFnSc2	Olympia
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
všech	všecek	k3xTgNnPc6	všecek
pobřežích	pobřeží	k1gNnPc6	pobřeží
poloostrova	poloostrov	k1gInSc2	poloostrov
silnice	silnice	k1gFnSc2	silnice
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Route	Rout	k1gInSc5	Rout
101	[number]	k4	101
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
poloostrov	poloostrov	k1gInSc1	poloostrov
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
Norman	Norman	k1gMnSc1	Norman
D.	D.	kA	D.
Dicks	Dicks	k1gInSc1	Dicks
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnPc1	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Bremerton	Bremerton	k1gInSc1	Bremerton
</s>
</p>
<p>
<s>
Port	port	k1gInSc1	port
Angeles	Angelesa	k1gFnPc2	Angelesa
</s>
</p>
<p>
<s>
Port	port	k1gInSc1	port
Orchard	Orcharda	k1gFnPc2	Orcharda
</s>
</p>
<p>
<s>
Shelton	Shelton	k1gInSc1	Shelton
</s>
</p>
<p>
<s>
Poulsbo	Poulsba	k1gMnSc5	Poulsba
</s>
</p>
<p>
<s>
Port	port	k1gInSc1	port
Townsend	Townsenda	k1gFnPc2	Townsenda
</s>
</p>
<p>
<s>
Hoquiam	Hoquiam	k6eAd1	Hoquiam
</s>
</p>
<p>
<s>
Sequim	Sequim	k6eAd1	Sequim
</s>
</p>
<p>
<s>
Ocean	Ocean	k1gMnSc1	Ocean
Shores	Shores	k1gMnSc1	Shores
</s>
</p>
<p>
<s>
Forks	Forks	k6eAd1	Forks
</s>
</p>
<p>
<s>
Port	port	k1gInSc1	port
Hadlock-Irondale	Hadlock-Irondal	k1gMnSc5	Hadlock-Irondal
</s>
</p>
<p>
<s>
Port	port	k1gInSc1	port
Ludlow	Ludlow	k1gFnSc2	Ludlow
</s>
</p>
<p>
<s>
Brinnon	Brinnon	k1gMnSc1	Brinnon
</s>
</p>
<p>
<s>
Neah	Neah	k1gMnSc1	Neah
Bay	Bay	k1gMnSc1	Bay
</s>
</p>
<p>
<s>
Belfair	Belfair	k1gMnSc1	Belfair
</s>
</p>
<p>
<s>
Quilcene	Quilcen	k1gMnSc5	Quilcen
</s>
</p>
<p>
<s>
Humptulips	Humptulips	k6eAd1	Humptulips
</s>
</p>
<p>
<s>
Ocean	Ocean	k1gInSc1	Ocean
City	City	k1gFnSc2	City
</s>
</p>
<p>
<s>
Moclips	Moclips	k6eAd1	Moclips
</s>
</p>
<p>
<s>
Quinault	Quinault	k1gMnSc1	Quinault
</s>
</p>
<p>
<s>
Pacific	Pacific	k1gMnSc1	Pacific
Beach	Beach	k1gMnSc1	Beach
</s>
</p>
<p>
<s>
Amanda	Amanda	k1gFnSc1	Amanda
Park	park	k1gInSc1	park
</s>
</p>
<p>
<s>
Chimacum	Chimacum	k1gInSc1	Chimacum
</s>
</p>
<p>
<s>
Clallam	Clallam	k1gInSc1	Clallam
Bay	Bay	k1gFnSc2	Bay
</s>
</p>
<p>
<s>
Eldon	Eldon	k1gMnSc1	Eldon
</s>
</p>
<p>
<s>
Hoodsport	Hoodsport	k1gInSc1	Hoodsport
</s>
</p>
<p>
<s>
Kalaloch	Kalaloch	k1gMnSc1	Kalaloch
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Push	Pusha	k1gFnPc2	Pusha
</s>
</p>
<p>
<s>
Lilliwaup	Lilliwaup	k1gMnSc1	Lilliwaup
</s>
</p>
<p>
<s>
Ozette	Ozette	k5eAaPmIp2nP	Ozette
</s>
</p>
<p>
<s>
Potlatch	Potlatch	k1gMnSc1	Potlatch
</s>
</p>
<p>
<s>
Sekiu	Sekius	k1gMnSc3	Sekius
</s>
</p>
<p>
<s>
Union	union	k1gInSc1	union
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Olympic	Olympice	k1gFnPc2	Olympice
Peninsula	Peninsulum	k1gNnSc2	Peninsulum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olympijský	olympijský	k2eAgInSc4d1	olympijský
poloostrov	poloostrov	k1gInSc4	poloostrov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
