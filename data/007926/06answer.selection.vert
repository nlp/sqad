<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
fragmentací	fragmentace	k1gFnPc2	fragmentace
cytoplasmy	cytoplasma	k1gFnSc2	cytoplasma
obrovských	obrovský	k2eAgFnPc2d1	obrovská
buněk	buňka	k1gFnPc2	buňka
megakaryocytů	megakaryocyt	k1gInPc2	megakaryocyt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
prekursorové	prekursorový	k2eAgFnSc2d1	prekursorový
buňky	buňka	k1gFnSc2	buňka
megakaryoblastu	megakaryoblast	k1gInSc2	megakaryoblast
<g/>
.	.	kIx.	.
</s>
