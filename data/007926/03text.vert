<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
destička	destička	k1gFnSc1	destička
(	(	kIx(	(
<g/>
též	též	k9	též
trombocyt	trombocyt	k1gInSc1	trombocyt
<g/>
)	)	kIx)	)
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
bezjaderné	bezjaderný	k2eAgNnSc1d1	bezjaderné
tělísko	tělísko	k1gNnSc1	tělísko
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
přilnavosti	přilnavost	k1gFnSc2	přilnavost
(	(	kIx(	(
<g/>
adhezivity	adhezivita	k1gFnSc2	adhezivita
<g/>
)	)	kIx)	)
a	a	k8xC	a
shlukování	shlukování	k1gNnSc1	shlukování
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
agregaci	agregace	k1gFnSc4	agregace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
procesu	proces	k1gInSc6	proces
zástavy	zástava	k1gFnSc2	zástava
krvácení	krvácení	k1gNnSc4	krvácení
a	a	k8xC	a
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
obratlovců	obratlovec	k1gMnPc2	obratlovec
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jadernou	jaderný	k2eAgFnSc4d1	jaderná
buňku	buňka	k1gFnSc4	buňka
(	(	kIx(	(
<g/>
koagulocyt	koagulocyt	k1gInSc4	koagulocyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
destička	destička	k1gFnSc1	destička
má	mít	k5eAaImIp3nS	mít
oválný	oválný	k2eAgInSc1d1	oválný
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
2	[number]	k4	2
–	–	k?	–
4.0	[number]	k4	4.0
μ	μ	k?	μ
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
(	(	kIx(	(
<g/>
erytrocyt	erytrocyt	k1gInSc1	erytrocyt
člověka	člověk	k1gMnSc2	člověk
-	-	kIx~	-
7,5	[number]	k4	7,5
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
má	mít	k5eAaImIp3nS	mít
zoubkované	zoubkovaný	k2eAgInPc4d1	zoubkovaný
okraje	okraj	k1gInPc4	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
okrajové	okrajový	k2eAgFnSc2d1	okrajová
hyalomery	hyalomera	k1gFnSc2	hyalomera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
barví	barvit	k5eAaImIp3nS	barvit
světle	světle	k6eAd1	světle
modře	modro	k6eAd1	modro
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tzv.	tzv.	kA	tzv.
marginální	marginální	k2eAgInSc4d1	marginální
svazek	svazek	k1gInSc4	svazek
mikrotubulů	mikrotubul	k1gMnPc2	mikrotubul
a	a	k8xC	a
aktinová	aktinový	k2eAgNnPc1d1	aktinový
mikrofilamenta	mikrofilamento	k1gNnPc1	mikrofilamento
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
udržují	udržovat	k5eAaImIp3nP	udržovat
tvar	tvar	k1gInSc4	tvar
trombocytu	trombocyt	k1gInSc2	trombocyt
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
mu	on	k3xPp3gMnSc3	on
tvořit	tvořit	k5eAaImF	tvořit
výběžky	výběžek	k1gInPc1	výběžek
a	a	k8xC	a
panožky	panožka	k1gFnPc1	panožka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
krevní	krevní	k2eAgFnSc2d1	krevní
destičky	destička	k1gFnSc2	destička
je	být	k5eAaImIp3nS	být
granulomera	granulomera	k1gFnSc1	granulomera
<g/>
,	,	kIx,	,
shluk	shluk	k1gInSc1	shluk
acidofilních	acidofilní	k2eAgMnPc2d1	acidofilní
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
červeně	červeně	k6eAd1	červeně
se	se	k3xPyFc4	se
barvících	barvící	k2eAgFnPc2d1	barvící
<g/>
)	)	kIx)	)
granul	granula	k1gFnPc2	granula
a	a	k8xC	a
ojedinělé	ojedinělý	k2eAgFnSc2d1	ojedinělá
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnPc1d1	krevní
destičky	destička	k1gFnPc1	destička
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
jsou	být	k5eAaImIp3nP	být
plnohodnotné	plnohodnotný	k2eAgFnPc1d1	plnohodnotná
buňky	buňka	k1gFnPc1	buňka
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
jádrem	jádro	k1gNnSc7	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Granulomera	Granulomera	k1gFnSc1	Granulomera
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
granula	granula	k1gFnSc1	granula
trojího	trojí	k4xRgInSc2	trojí
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
alfa-granula	alfaranout	k5eAaPmAgFnS	alfa-granout
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
fibrinogen	fibrinogen	k1gInSc4	fibrinogen
<g/>
,	,	kIx,	,
trombokinázu	trombokináza	k1gFnSc4	trombokináza
<g/>
,	,	kIx,	,
směs	směs	k1gFnSc1	směs
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
růstový	růstový	k2eAgInSc1d1	růstový
faktor	faktor	k1gInSc1	faktor
s	s	k7c7	s
mitogenními	mitogenní	k2eAgInPc7d1	mitogenní
účinky	účinek	k1gInPc7	účinek
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
fibroblasty	fibroblast	k1gInPc4	fibroblast
<g/>
,	,	kIx,	,
destičkový	destičkový	k2eAgInSc4d1	destičkový
faktor	faktor	k1gInSc4	faktor
4	[number]	k4	4
(	(	kIx(	(
<g/>
antagonista	antagonista	k1gMnSc1	antagonista
heparinu	heparin	k1gInSc2	heparin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
glie	gli	k1gFnSc2	gli
a	a	k8xC	a
hladké	hladký	k2eAgFnSc2d1	hladká
svalové	svalový	k2eAgFnSc2d1	svalová
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
β	β	k?	β
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
faktor	faktor	k1gInSc1	faktor
V.	V.	kA	V.
aj.	aj.	kA	aj.
delta-granula	deltaranout	k5eAaPmAgFnS	delta-granout
(	(	kIx(	(
<g/>
denzní	denznit	k5eAaPmIp3nP	denznit
<g/>
)	)	kIx)	)
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
serotonin	serotonin	k1gInSc4	serotonin
<g/>
,	,	kIx,	,
vápenaté	vápenatý	k2eAgInPc4d1	vápenatý
ionty	ion	k1gInPc4	ion
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
pyrofosfát	pyrofosfát	k1gInSc1	pyrofosfát
<g/>
,	,	kIx,	,
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
ADP	ADP	kA	ADP
lambda-granula	lambdaranout	k5eAaPmAgFnS	lambda-granout
(	(	kIx(	(
<g/>
lysosomy	lysosom	k1gInPc4	lysosom
<g/>
)	)	kIx)	)
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
lysosomální	lysosomální	k2eAgInPc4d1	lysosomální
enzymy	enzym	k1gInPc4	enzym
Krevní	krevní	k2eAgFnSc2d1	krevní
destičky	destička	k1gFnSc2	destička
jsou	být	k5eAaImIp3nP	být
stálou	stálý	k2eAgFnSc7d1	stálá
součástí	součást	k1gFnSc7	součást
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
na	na	k7c4	na
mikrolitr	mikrolitr	k1gInSc4	mikrolitr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
pod	pod	k7c4	pod
určitou	určitý	k2eAgFnSc4d1	určitá
úroveň	úroveň	k1gFnSc4	úroveň
začnou	začít	k5eAaPmIp3nP	začít
játra	játra	k1gNnPc4	játra
produkovat	produkovat	k5eAaImF	produkovat
hormon	hormon	k1gInSc4	hormon
thrombopoetin	thrombopoetin	k2eAgInSc4d1	thrombopoetin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
tvorbu	tvorba	k1gFnSc4	tvorba
dalších	další	k2eAgFnPc2d1	další
destiček	destička	k1gFnPc2	destička
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
počtu	počet	k1gInSc2	počet
destiček	destička	k1gFnPc2	destička
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
trombocytopenie	trombocytopenie	k1gFnSc1	trombocytopenie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
vážných	vážný	k2eAgFnPc2d1	vážná
poruch	porucha	k1gFnPc2	porucha
srážlivosti	srážlivost	k1gFnSc2	srážlivost
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnPc1d1	krevní
destičky	destička	k1gFnPc1	destička
nemají	mít	k5eNaImIp3nP	mít
jádro	jádro	k1gNnSc1	jádro
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
se	se	k3xPyFc4	se
samy	sám	k3xTgFnPc4	sám
množit	množit	k5eAaImF	množit
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
fragmentací	fragmentace	k1gFnPc2	fragmentace
cytoplasmy	cytoplasma	k1gFnSc2	cytoplasma
obrovských	obrovský	k2eAgFnPc2d1	obrovská
buněk	buňka	k1gFnPc2	buňka
megakaryocytů	megakaryocyt	k1gInPc2	megakaryocyt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
prekursorové	prekursorový	k2eAgFnSc2d1	prekursorový
buňky	buňka	k1gFnSc2	buňka
megakaryoblastu	megakaryoblast	k1gInSc2	megakaryoblast
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
životnost	životnost	k1gFnSc1	životnost
je	být	k5eAaImIp3nS	být
8-12	[number]	k4	8-12
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jsou	být	k5eAaImIp3nP	být
degradovány	degradovat	k5eAaBmNgFnP	degradovat
ve	v	k7c6	v
slezině	slezina	k1gFnSc6	slezina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poranění	poranění	k1gNnSc6	poranění
cévy	céva	k1gFnSc2	céva
se	se	k3xPyFc4	se
obnaží	obnažit	k5eAaPmIp3nS	obnažit
kolagen	kolagen	k1gInSc1	kolagen
<g/>
,	,	kIx,	,
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
bílkovina	bílkovina	k1gFnSc1	bílkovina
přítomná	přítomný	k2eAgFnSc1d1	přítomná
v	v	k7c6	v
mezibuněčné	mezibuněčný	k2eAgFnSc6d1	mezibuněčná
hmotě	hmota	k1gFnSc6	hmota
vaziva	vazivo	k1gNnSc2	vazivo
<g/>
.	.	kIx.	.
</s>
<s>
Destičky	destička	k1gFnPc1	destička
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
na	na	k7c4	na
kolagen	kolagen	k1gInSc4	kolagen
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
aktivovány	aktivovat	k5eAaBmNgInP	aktivovat
thrombinem	thrombin	k1gInSc7	thrombin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
srážení	srážení	k1gNnSc6	srážení
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ADP	ADP	kA	ADP
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
uvolňován	uvolňovat	k5eAaImNgInS	uvolňovat
ostatními	ostatní	k2eAgFnPc7d1	ostatní
destičkami	destička	k1gFnPc7	destička
<g/>
.	.	kIx.	.
</s>
<s>
Aktivované	aktivovaný	k2eAgInPc1d1	aktivovaný
thrombocyty	thrombocyt	k1gInPc1	thrombocyt
změní	změnit	k5eAaPmIp3nP	změnit
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
fibrinogenu	fibrinogen	k1gInSc2	fibrinogen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
aktivované	aktivovaný	k2eAgFnPc4d1	aktivovaná
destičky	destička	k1gFnPc4	destička
váže	vázat	k5eAaImIp3nS	vázat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
destičkovou	destičkový	k2eAgFnSc4d1	destičková
zátku	zátka	k1gFnSc4	zátka
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
trombus	trombus	k1gInSc4	trombus
<g/>
.	.	kIx.	.
</s>
<s>
Destičky	destička	k1gFnPc1	destička
se	se	k3xPyFc4	se
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
<g/>
,	,	kIx,	,
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
obsah	obsah	k1gInSc4	obsah
svých	svůj	k3xOyFgFnPc2	svůj
granul	granula	k1gFnPc2	granula
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozeběhnutí	rozeběhnutí	k1gNnSc3	rozeběhnutí
kaskády	kaskáda	k1gFnSc2	kaskáda
reakcí	reakce	k1gFnPc2	reakce
koagulačních	koagulační	k2eAgInPc2d1	koagulační
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
z	z	k7c2	z
fibrinogenu	fibrinogen	k1gInSc2	fibrinogen
stane	stanout	k5eAaPmIp3nS	stanout
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
fibrin	fibrin	k1gInSc1	fibrin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zpevní	zpevnit	k5eAaPmIp3nS	zpevnit
masu	masa	k1gFnSc4	masa
rozpadlých	rozpadlý	k2eAgFnPc2d1	rozpadlá
destiček	destička	k1gFnPc2	destička
a	a	k8xC	a
zachycených	zachycený	k2eAgFnPc2d1	zachycená
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
červený	červený	k2eAgInSc4d1	červený
trombus	trombus	k1gInSc4	trombus
<g/>
,	,	kIx,	,
definitivní	definitivní	k2eAgFnSc4d1	definitivní
krevní	krevní	k2eAgFnSc4d1	krevní
sraženinu	sraženina	k1gFnSc4	sraženina
<g/>
.	.	kIx.	.
</s>
