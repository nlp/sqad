<s>
Měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
slabou	slabý	k2eAgFnSc4d1	slabá
kyslíkovou	kyslíkový	k2eAgFnSc4d1	kyslíková
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
molekulami	molekula	k1gFnPc7	molekula
O	O	kA	O
<g/>
,	,	kIx,	,
O2	O2	k1gFnSc2	O2
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
