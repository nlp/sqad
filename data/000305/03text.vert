<s>
Skandinávský	skandinávský	k2eAgInSc1d1	skandinávský
poloostrov	poloostrov	k1gInSc1	poloostrov
je	být	k5eAaImIp3nS	být
poloostrov	poloostrov	k1gInSc4	poloostrov
položený	položený	k2eAgInSc4d1	položený
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Finska	Finsko	k1gNnSc2	Finsko
526	[number]	k4	526
km	km	kA	km
širokou	široký	k2eAgFnSc7d1	široká
šíjí	šíj	k1gFnSc7	šíj
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
od	od	k7c2	od
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Skandinávském	skandinávský	k2eAgInSc6d1	skandinávský
poloostrově	poloostrov	k1gInSc6	poloostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Norsko	Norsko	k1gNnSc1	Norsko
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
pás	pás	k1gInSc4	pás
hor	hora	k1gFnPc2	hora
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgInSc1d3	veliký
poloostrov	poloostrov	k1gInSc1	poloostrov
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
870	[number]	k4	870
km	km	kA	km
<g/>
,	,	kIx,	,
šířce	šířka	k1gFnSc6	šířka
370	[number]	k4	370
<g/>
-	-	kIx~	-
<g/>
750	[number]	k4	750
km	km	kA	km
a	a	k8xC	a
rozloze	rozloha	k1gFnSc6	rozloha
774000	[number]	k4	774000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Laponsko	Laponsko	k1gNnSc1	Laponsko
<g/>
.	.	kIx.	.
</s>
