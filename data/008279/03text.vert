<p>
<s>
Liška	Liška	k1gMnSc1	Liška
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc4d1	české
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
rody	rod	k1gInPc4	rod
psovitých	psovitý	k2eAgFnPc2d1	psovitá
šelem	šelma	k1gFnPc2	šelma
z	z	k7c2	z
tribu	trib	k1gInSc2	trib
Vulpini	Vulpin	k2eAgMnPc1d1	Vulpin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
liška	liška	k1gFnSc1	liška
jezerní	jezerní	k2eAgFnSc1d1	jezerní
<g/>
,	,	kIx,	,
japonská	japonský	k2eAgFnSc1d1	japonská
nebo	nebo	k8xC	nebo
mořská	mořský	k2eAgFnSc1d1	mořská
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jiná	jiný	k2eAgFnSc1d1	jiná
psovitá	psovitý	k2eAgFnSc1d1	psovitá
šelma	šelma	k1gFnSc1	šelma
psík	psík	k1gMnSc1	psík
mývalovitý	mývalovitý	k2eAgMnSc1d1	mývalovitý
<g/>
.	.	kIx.	.
</s>
<s>
Liška	liška	k1gFnSc1	liška
patagonská	patagonský	k2eAgFnSc1d1	patagonská
je	být	k5eAaImIp3nS	být
alternativní	alternativní	k2eAgInSc4d1	alternativní
název	název	k1gInSc4	název
pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
horského	horský	k2eAgMnSc4d1	horský
a	a	k8xC	a
liška	liška	k1gFnSc1	liška
Azarova	Azarův	k2eAgFnSc1d1	Azarův
pro	pro	k7c4	pro
psa	pes	k1gMnSc4	pes
šedého	šedý	k2eAgMnSc4d1	šedý
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Lycalopex	Lycalopex	k1gInSc1	Lycalopex
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
liška	liška	k1gFnSc1	liška
habešská	habešský	k2eAgFnSc1d1	habešská
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
vlček	vlček	k1gInSc1	vlček
etiopský	etiopský	k2eAgInSc1d1	etiopský
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
krátkouchý	krátkouchý	k2eAgMnSc1d1	krátkouchý
(	(	kIx(	(
<g/>
Atelocynus	Atelocynus	k1gInSc1	Atelocynus
microtis	microtis	k1gInSc1	microtis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
liška	liška	k1gFnSc1	liška
krátkouchá	krátkouchat	k5eAaPmIp3nS	krátkouchat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
druhů	druh	k1gInPc2	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Urocyon	Urocyon	k1gMnSc1	Urocyon
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
šedá	šedá	k1gFnSc1	šedá
=	=	kIx~	=
liška	liška	k1gFnSc1	liška
šedostříbrná	šedostříbrný	k2eAgFnSc1d1	šedostříbrná
(	(	kIx(	(
<g/>
Urocyon	Urocyon	k1gMnSc1	Urocyon
cinereoargenteus	cinereoargenteus	k1gMnSc1	cinereoargenteus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
(	(	kIx(	(
<g/>
Urocyon	Urocyon	k1gMnSc1	Urocyon
littoralis	littoralis	k1gFnSc2	littoralis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vulpes	Vulpes	k1gMnSc1	Vulpes
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
polární	polární	k2eAgFnSc1d1	polární
=	=	kIx~	=
liška	liška	k1gFnSc1	liška
lední	lední	k2eAgFnSc1d1	lední
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
lagopus	lagopus	k1gMnSc1	lagopus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
džunglová	džunglový	k2eAgFnSc1d1	džunglová
=	=	kIx~	=
liška	liška	k1gFnSc1	liška
šedorudá	šedorudý	k2eAgFnSc1d1	šedorudý
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
bengalensis	bengalensis	k1gFnSc2	bengalensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
kana	kanout	k5eAaImSgInS	kanout
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
cana	cana	k1gMnSc1	cana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
chama	chama	k1gFnSc1	chama
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gInSc1	Vulpes
chama	chama	k1gNnSc1	chama
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
horská	horský	k2eAgFnSc1d1	horská
=	=	kIx~	=
liška	liška	k1gFnSc1	liška
tibetská	tibetský	k2eAgFnSc1d1	tibetská
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
ferrilata	ferrile	k1gNnPc5	ferrile
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
písečná	písečný	k2eAgFnSc1d1	písečná
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
pallida	pallid	k1gMnSc2	pallid
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
pouštní	pouštní	k2eAgFnSc1d1	pouštní
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
rueppelli	rueppelle	k1gFnSc4	rueppelle
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
šedohnědá	šedohnědý	k2eAgFnSc1d1	šedohnědá
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gInSc1	Vulpes
velox	velox	k1gInSc1	velox
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
velkouchá	velkouchat	k5eAaPmIp3nS	velkouchat
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gInSc1	Vulpes
macrotis	macrotis	k1gFnSc2	macrotis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
vulpes	vulpes	k1gMnSc1	vulpes
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
vulpes	vulpes	k1gMnSc1	vulpes
vulpes	vulpes	k1gMnSc1	vulpes
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
evropská	evropský	k2eAgFnSc1d1	Evropská
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
vulpes	vulpes	k1gMnSc1	vulpes
crucigera	crucigera	k1gFnSc1	crucigera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liška	liška	k1gFnSc1	liška
plavá	plavý	k2eAgFnSc1d1	plavá
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
vulpes	vulpes	k1gMnSc1	vulpes
fulva	fulva	k6eAd1	fulva
<g/>
)	)	kIx)	)
<g/>
Do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Vulpes	Vulpes	k1gInSc1	Vulpes
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
korsak	korsak	k1gMnSc1	korsak
a	a	k8xC	a
fenek	fenek	k1gMnSc1	fenek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
liška	liška	k1gFnSc1	liška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Liška	liška	k1gFnSc1	liška
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
