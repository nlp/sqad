<s>
Dendrologie	dendrologie	k1gFnSc1	dendrologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
dendron	dendron	k1gInSc1	dendron
-	-	kIx~	-
strom	strom	k1gInSc1	strom
a	a	k8xC	a
logie	logie	k1gFnSc1	logie
-	-	kIx~	-
nauka	nauka	k1gFnSc1	nauka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
dřevinách	dřevina	k1gFnPc6	dřevina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
o	o	k7c6	o
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
keřích	keř	k1gInPc6	keř
a	a	k8xC	a
polokeřích	polokeř	k1gInPc6	polokeř
<g/>
.	.	kIx.	.
</s>
<s>
Dendrologie	dendrologie	k1gFnSc1	dendrologie
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
systematické	systematický	k2eAgFnSc2d1	systematická
botaniky	botanika	k1gFnSc2	botanika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
především	především	k6eAd1	především
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
německého	německý	k2eAgNnSc2d1	německé
lesnictví	lesnictví	k1gNnSc2	lesnictví
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dendrologické	dendrologický	k2eAgInPc1d1	dendrologický
učební	učební	k2eAgInPc1d1	učební
texty	text	k1gInPc1	text
byly	být	k5eAaImAgInP	být
mnohdy	mnohdy	k6eAd1	mnohdy
součástí	součást	k1gFnSc7	součást
učebnic	učebnice	k1gFnPc2	učebnice
pěstění	pěstění	k1gNnPc2	pěstění
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyčleňovaly	vyčleňovat	k5eAaImAgInP	vyčleňovat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
aplikované	aplikovaný	k2eAgInPc1d1	aplikovaný
obory	obor	k1gInPc1	obor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dendrologie	dendrologie	k1gFnSc1	dendrologie
<g/>
:	:	kIx,	:
lesnická	lesnický	k2eAgFnSc1d1	lesnická
(	(	kIx(	(
<g/>
poskytující	poskytující	k2eAgInPc1d1	poskytující
podklady	podklad	k1gInPc1	podklad
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
lesů	les	k1gInPc2	les
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
též	též	k9	též
dřevařská	dřevařský	k2eAgFnSc1d1	dřevařská
sadovnická	sadovnický	k2eAgFnSc1d1	Sadovnická
(	(	kIx(	(
<g/>
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
okrasnými	okrasný	k2eAgFnPc7d1	okrasná
dřevinami	dřevina	k1gFnPc7	dřevina
<g/>
)	)	kIx)	)
ovocnářská	ovocnářský	k2eAgFnSc1d1	Ovocnářská
(	(	kIx(	(
<g/>
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
ovocnými	ovocný	k2eAgFnPc7d1	ovocná
dřevinami	dřevina	k1gFnPc7	dřevina
<g/>
)	)	kIx)	)
meliorační	meliorační	k2eAgFnPc1d1	meliorační
(	(	kIx(	(
<g/>
dřeviny	dřevina	k1gFnPc1	dřevina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
zlepšování	zlepšování	k1gNnSc4	zlepšování
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
)	)	kIx)	)
arboristika	arboristika	k1gFnSc1	arboristika
(	(	kIx(	(
<g/>
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
stromy	strom	k1gInPc4	strom
<g/>
)	)	kIx)	)
Součástí	součást	k1gFnSc7	součást
dendrologických	dendrologický	k2eAgFnPc2d1	Dendrologická
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
zkoumání	zkoumání	k1gNnSc1	zkoumání
možností	možnost	k1gFnPc2	možnost
introdukce	introdukce	k1gFnSc2	introdukce
a	a	k8xC	a
aklimatizace	aklimatizace	k1gFnSc2	aklimatizace
nepůvodních	původní	k2eNgFnPc2d1	nepůvodní
dřevin	dřevina	k1gFnPc2	dřevina
a	a	k8xC	a
pěstování	pěstování	k1gNnSc4	pěstování
dřevin	dřevina	k1gFnPc2	dřevina
v	v	k7c6	v
arboretech	arboretum	k1gNnPc6	arboretum
<g/>
.	.	kIx.	.
</s>
<s>
Dendrologie	dendrologie	k1gFnSc1	dendrologie
se	se	k3xPyFc4	se
neomezuje	omezovat	k5eNaImIp3nS	omezovat
na	na	k7c4	na
pouhou	pouhý	k2eAgFnSc4d1	pouhá
charakteristiku	charakteristika	k1gFnSc4	charakteristika
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
především	především	k9	především
snažit	snažit	k5eAaImF	snažit
o	o	k7c4	o
pochopení	pochopení	k1gNnSc4	pochopení
jejich	jejich	k3xOp3gFnSc2	jejich
úlohy	úloha	k1gFnSc2	úloha
v	v	k7c6	v
ekosystému	ekosystém	k1gInSc6	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
studia	studio	k1gNnSc2	studio
dendrologie	dendrologie	k1gFnSc2	dendrologie
i	i	k8xC	i
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
stanovištích	stanoviště	k1gNnPc6	stanoviště
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
taxonů	taxon	k1gInPc2	taxon
<g/>
)	)	kIx)	)
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
o	o	k7c6	o
území	území	k1gNnSc6	území
jejich	jejich	k3xOp3gNnSc2	jejich
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
rozšíření	rozšíření	k1gNnSc2	rozšíření
(	(	kIx(	(
<g/>
o	o	k7c6	o
areálech	areál	k1gInPc6	areál
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c6	o
klimatu	klima	k1gNnSc6	klima
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tam	tam	k6eAd1	tam
panuje	panovat	k5eAaImIp3nS	panovat
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejich	jejich	k3xOp3gFnPc6	jejich
ekologických	ekologický	k2eAgFnPc6d1	ekologická
interakcích	interakce	k1gFnPc6	interakce
atd.	atd.	kA	atd.
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
lze	lze	k6eAd1	lze
dendrologii	dendrologie	k1gFnSc4	dendrologie
studovat	studovat	k5eAaImF	studovat
např.	např.	kA	např.
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
lesnické	lesnický	k2eAgFnSc2d1	lesnická
ČZU	ČZU	kA	ČZU
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Lesnické	lesnický	k2eAgFnSc6d1	lesnická
a	a	k8xC	a
dřevařské	dřevařský	k2eAgFnSc6d1	dřevařská
fakultě	fakulta	k1gFnSc6	fakulta
MZLU	MZLU	kA	MZLU
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
dendrologická	dendrologický	k2eAgFnSc1d1	Dendrologická
zahrada	zahrada	k1gFnSc1	zahrada
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Průhonicích	Průhonice	k1gFnPc6	Průhonice
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dendrologie	dendrologie	k1gFnSc2	dendrologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Park	park	k1gInSc1	park
Arboretum	arboretum	k1gNnSc4	arboretum
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
architektura	architektura	k1gFnSc1	architektura
</s>
