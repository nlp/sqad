<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
modrým	modrý	k2eAgInSc7d1	modrý
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
<g/>
Modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
nebe	nebe	k1gNnSc1	nebe
a	a	k8xC	a
žlutý	žlutý	k2eAgInSc1d1	žlutý
pruh	pruh	k1gInSc1	pruh
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
zralá	zralý	k2eAgNnPc4d1	zralé
pšeničná	pšeničný	k2eAgNnPc4d1	pšeničné
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
882	[number]	k4	882
<g/>
–	–	k?	–
<g/>
1169	[number]	k4	1169
bylo	být	k5eAaImAgNnS	být
dnešní	dnešní	k2eAgNnSc1d1	dnešní
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
–	–	k?	–
Kyjev	Kyjev	k1gInSc1	Kyjev
–	–	k?	–
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nejstaršího	starý	k2eAgInSc2d3	nejstarší
východoslovanského	východoslovanský	k2eAgInSc2d1	východoslovanský
státu	stát	k1gInSc2	stát
Rus	Rus	k1gMnSc1	Rus
<g/>
,	,	kIx,	,
historiky	historik	k1gMnPc4	historik
později	pozdě	k6eAd2	pozdě
nazvaného	nazvaný	k2eAgInSc2d1	nazvaný
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
na	na	k7c4	na
několik	několik	k4yIc4	několik
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
knížectví	knížectví	k1gNnPc2	knížectví
(	(	kIx(	(
<g/>
1132	[number]	k4	1132
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1240	[number]	k4	1240
při	při	k7c6	při
mongolském	mongolský	k2eAgInSc6d1	mongolský
vpádu	vpád	k1gInSc6	vpád
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
Kyjev	Kyjev	k1gInSc1	Kyjev
dobyt	dobyt	k2eAgInSc1d1	dobyt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
Polsko	Polsko	k1gNnSc4	Polsko
a	a	k8xC	a
Litvu	Litva	k1gFnSc4	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
v	v	k7c6	v
letech	let	k1gInPc6	let
1648	[number]	k4	1648
<g/>
–	–	k?	–
<g/>
1654	[number]	k4	1654
však	však	k8xC	však
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
připojování	připojování	k1gNnSc3	připojování
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
území	území	k1gNnSc2	území
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
dvou	dva	k4xCgNnPc2	dva
impérií	impérium	k1gNnPc2	impérium
–	–	k?	–
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
užívat	užívat	k5eAaImF	užívat
modro-žluté	modro-žlutý	k2eAgFnPc1d1	modro-žlutá
vlajky	vlajka	k1gFnPc1	vlajka
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
lvem	lev	k1gInSc7	lev
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
inspirovány	inspirován	k2eAgMnPc4d1	inspirován
starým	starý	k2eAgInSc7d1	starý
haličským	haličský	k2eAgInSc7d1	haličský
znakem	znak	k1gInSc7	znak
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Haličsko-volyňské	haličskoolyňský	k2eAgNnSc4d1	haličsko-volyňský
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
však	však	k9	však
byl	být	k5eAaImAgInS	být
lev	lev	k1gInSc1	lev
odstraněn	odstranit	k5eAaPmNgInS	odstranit
a	a	k8xC	a
užívala	užívat	k5eAaImAgFnS	užívat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
modro-žlutá	modro-žlutý	k2eAgFnSc1d1	modro-žlutá
<g/>
,	,	kIx,	,
vodorovná	vodorovný	k2eAgFnSc1d1	vodorovná
bikolóra	bikolóra	k1gFnSc1	bikolóra
<g/>
,	,	kIx,	,
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
ruské	ruský	k2eAgFnSc6d1	ruská
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
založená	založený	k2eAgFnSc1d1	založená
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
rada	rada	k1gFnSc1	rada
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
Ruské	ruský	k2eAgFnSc6d1	ruská
prozatímní	prozatímní	k2eAgFnSc6d1	prozatímní
vládě	vláda	k1gFnSc6	vláda
požadovat	požadovat	k5eAaImF	požadovat
autonomii	autonomie	k1gFnSc4	autonomie
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bolševické	bolševický	k2eAgFnSc6d1	bolševická
revoluci	revoluce	k1gFnSc6	revoluce
pak	pak	k6eAd1	pak
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
protisovětskou	protisovětský	k2eAgFnSc4d1	protisovětská
Ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
lidovou	lidový	k2eAgFnSc4d1	lidová
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
na	na	k7c6	na
části	část	k1gFnSc6	část
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
území	území	k1gNnSc2	území
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
prosovětská	prosovětský	k2eAgFnSc1d1	prosovětská
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
sovětů	sovět	k1gInPc2	sovět
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
nezávislých	závislý	k2eNgInPc2d1	nezávislý
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
ULR	ULR	kA	ULR
užívala	užívat	k5eAaImAgFnS	užívat
žluto-modré	žlutoodrý	k2eAgFnPc4d1	žluto-modrá
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
s	s	k7c7	s
obráceným	obrácený	k2eAgNnSc7d1	obrácené
pořadím	pořadí	k1gNnSc7	pořadí
než	než	k8xS	než
u	u	k7c2	u
současné	současný	k2eAgFnSc2d1	současná
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
ULRS	ULRS	kA	ULRS
užívala	užívat	k5eAaImAgFnS	užívat
(	(	kIx(	(
<g/>
zdroje	zdroj	k1gInPc1	zdroj
informací	informace	k1gFnPc2	informace
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
sporné	sporný	k2eAgInPc1d1	sporný
<g/>
)	)	kIx)	)
červený	červený	k2eAgInSc1d1	červený
list	list	k1gInSc1	list
se	s	k7c7	s
žluto-modrou	žlutoodrý	k2eAgFnSc7d1	žluto-modrá
bikolórou	bikolóra	k1gFnSc7	bikolóra
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
chopily	chopit	k5eAaPmAgInP	chopit
moci	moct	k5eAaImF	moct
sovětské	sovětský	k2eAgInPc1d1	sovětský
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
rada	rada	k1gFnSc1	rada
však	však	k9	však
podepsala	podepsat	k5eAaPmAgFnS	podepsat
separátní	separátní	k2eAgInSc4d1	separátní
mír	mír	k1gInSc4	mír
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
německá	německý	k2eAgFnSc1d1	německá
a	a	k8xC	a
rakouská	rakouský	k2eAgFnSc1d1	rakouská
armáda	armáda	k1gFnSc1	armáda
začala	začít	k5eAaPmAgFnS	začít
obsazovat	obsazovat	k5eAaImF	obsazovat
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nastolena	nastolen	k2eAgFnSc1d1	nastolena
loutková	loutkový	k2eAgFnSc1d1	loutková
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
s	s	k7c7	s
hejtmanem	hejtman	k1gMnSc7	hejtman
Pavlem	Pavel	k1gMnSc7	Pavel
Skoropadským	Skoropadský	k2eAgMnSc7d1	Skoropadský
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
modro-žlutá	modro-žlutý	k2eAgFnSc1d1	modro-žlutá
bikolóra	bikolóra	k1gFnSc1	bikolóra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Meziválečné	meziválečný	k2eAgNnSc4d1	meziválečné
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
nepřehledné	přehledný	k2eNgFnSc6d1	nepřehledná
době	doba	k1gFnSc6	doba
změn	změna	k1gFnPc2	změna
hranic	hranice	k1gFnPc2	hranice
mezi	mezi	k7c7	mezi
I.	I.	kA	I.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
(	(	kIx(	(
<g/>
rozpad	rozpad	k1gInSc1	rozpad
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncem	koncem	k7c2	koncem
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgNnP	být
některá	některý	k3yIgNnPc1	některý
území	území	k1gNnPc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
součástí	součást	k1gFnPc2	součást
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
a	a	k8xC	a
užívaly	užívat	k5eAaImAgFnP	užívat
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
příslušné	příslušný	k2eAgFnPc1d1	příslušná
státní	státní	k2eAgFnPc4d1	státní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
stát	stát	k1gInSc1	stát
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
do	do	k7c2	do
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
Moldávie	Moldávie	k1gFnSc2	Moldávie
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
První	první	k4xOgFnPc1	první
republiky	republika	k1gFnPc1	republika
v	v	k7c6	v
letech	let	k1gInPc6	let
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
součástí	součást	k1gFnSc7	součást
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zemského	zemský	k2eAgNnSc2d1	zemské
uspořádání	uspořádání	k1gNnSc2	uspořádání
užívala	užívat	k5eAaImAgFnS	užívat
i	i	k8xC	i
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
okupována	okupovat	k5eAaBmNgFnS	okupovat
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
éra	éra	k1gFnSc1	éra
===	===	k?	===
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
začleněna	začlenit	k5eAaPmNgFnS	začlenit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
vlajka	vlajka	k1gFnSc1	vlajka
poplatná	poplatný	k2eAgFnSc1d1	poplatná
sovětskému	sovětský	k2eAgInSc3d1	sovětský
vzoru	vzor	k1gInSc3	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Tvořil	tvořit	k5eAaImAgMnS	tvořit
ji	on	k3xPp3gFnSc4	on
červený	červený	k2eAgInSc1d1	červený
list	list	k1gInSc1	list
se	s	k7c7	s
žlutou	žlutý	k2eAgFnSc7d1	žlutá
zkratkou	zkratka	k1gFnSc7	zkratka
USSR	USSR	kA	USSR
v	v	k7c6	v
azbuce	azbuka	k1gFnSc6	azbuka
(	(	kIx(	(
<g/>
У	У	k?	У
<g/>
)	)	kIx)	)
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
zkratka	zkratka	k1gFnSc1	zkratka
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c6	na
У	У	k?	У
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
názvu	název	k1gInSc3	název
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
ukrajinštině	ukrajinština	k1gFnSc6	ukrajinština
<g/>
.30	.30	k4	.30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1937	[number]	k4	1937
se	se	k3xPyFc4	se
iniciály	iniciála	k1gFnPc1	iniciála
změnily	změnit	k5eAaPmAgFnP	změnit
na	na	k7c6	na
У	У	k?	У
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
pod	pod	k7c7	pod
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
žlutý	žlutý	k2eAgInSc4d1	žlutý
srp	srp	k1gInSc4	srp
a	a	k8xC	a
kladivo	kladivo	k1gNnSc4	kladivo
<g/>
,	,	kIx,	,
symboly	symbol	k1gInPc4	symbol
komunistické	komunistický	k2eAgFnSc2d1	komunistická
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
změnilo	změnit	k5eAaPmAgNnS	změnit
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
tučnější	tučný	k2eAgMnSc1d2	tučnější
<g/>
)	)	kIx)	)
a	a	k8xC	a
srp	srp	k1gInSc1	srp
s	s	k7c7	s
kladivem	kladivo	k1gNnSc7	kladivo
se	se	k3xPyFc4	se
přesunuly	přesunout	k5eAaPmAgInP	přesunout
nad	nad	k7c4	nad
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
výnosem	výnos	k1gInSc7	výnos
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
USSR	USSR	kA	USSR
změněna	změnit	k5eAaPmNgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
zavedla	zavést	k5eAaPmAgFnS	zavést
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
vlajku	vlajka	k1gFnSc4	vlajka
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
převzaly	převzít	k5eAaPmAgFnP	převzít
později	pozdě	k6eAd2	pozdě
i	i	k8xC	i
ostatní	ostatní	k2eAgFnPc1d1	ostatní
republiky	republika	k1gFnPc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
doplnění	doplnění	k1gNnSc4	doplnění
vlajky	vlajka	k1gFnSc2	vlajka
SSSR	SSSR	kA	SSSR
jedním	jeden	k4xCgInSc7	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
USSR	USSR	kA	USSR
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c6	o
světle	světlo	k1gNnSc6	světlo
modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
při	při	k7c6	při
dolním	dolní	k2eAgInSc6d1	dolní
okraji	okraj	k1gInSc6	okraj
<g/>
.	.	kIx.	.
<g/>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
vlajka	vlajka	k1gFnSc1	vlajka
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
společně	společně	k6eAd1	společně
s	s	k7c7	s
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
vlajkou	vlajka	k1gFnSc7	vlajka
Běloruské	běloruský	k2eAgFnSc2d1	Běloruská
SSR	SSR	kA	SSR
byly	být	k5eAaImAgInP	být
jako	jako	k9	jako
jediné	jediný	k2eAgInPc1d1	jediný
z	z	k7c2	z
vlajek	vlajka	k1gFnPc2	vlajka
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
běžně	běžně	k6eAd1	běžně
užívány	užíván	k2eAgFnPc1d1	užívána
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
také	také	k9	také
vyvěšeny	vyvěšen	k2eAgFnPc1d1	vyvěšena
před	před	k7c7	před
sídlem	sídlo	k1gNnSc7	sídlo
OSN	OSN	kA	OSN
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Samostatnost	samostatnost	k1gFnSc4	samostatnost
===	===	k?	===
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1989	[number]	k4	1989
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
založeno	založen	k2eAgNnSc1d1	založeno
Národní	národní	k2eAgNnSc1d1	národní
hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
přestavbu	přestavba	k1gFnSc4	přestavba
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
Ruch	ruch	k1gInSc1	ruch
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
modro-žlutá	modro-žlutý	k2eAgFnSc1d1	modro-žlutá
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1990	[number]	k4	1990
povolil	povolit	k5eAaPmAgInS	povolit
kyjevský	kyjevský	k2eAgInSc1d1	kyjevský
sovět	sovět	k1gInSc1	sovět
užívání	užívání	k1gNnSc2	užívání
této	tento	k3xDgFnSc2	tento
vlajky	vlajka	k1gFnSc2	vlajka
společně	společně	k6eAd1	společně
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
dosavadní	dosavadní	k2eAgFnSc7d1	dosavadní
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
zavlála	zavlát	k5eAaPmAgFnS	zavlát
i	i	k9	i
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nebyla	být	k5eNaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
(	(	kIx(	(
<g/>
současná	současný	k2eAgFnSc1d1	současná
<g/>
)	)	kIx)	)
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
až	až	k6eAd1	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
tmavší	tmavý	k2eAgFnSc1d2	tmavší
než	než	k8xS	než
barva	barva	k1gFnSc1	barva
historické	historický	k2eAgFnSc2d1	historická
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
zemskou	zemský	k2eAgFnSc7d1	zemská
vlajkou	vlajka	k1gFnSc7	vlajka
Dolních	dolní	k2eAgInPc2d1	dolní
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
,	,	kIx,	,
přijatou	přijatý	k2eAgFnSc4d1	přijatá
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnSc2	vlajka
ukrajinských	ukrajinský	k2eAgInPc2d1	ukrajinský
správních	správní	k2eAgInPc2d1	správní
celků	celek	k1gInPc2	celek
==	==	k?	==
</s>
</p>
<p>
<s>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
27	[number]	k4	27
správních	správní	k2eAgInPc2d1	správní
celků	celek	k1gInPc2	celek
<g/>
:	:	kIx,	:
2	[number]	k4	2
města	město	k1gNnSc2	město
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
statutem	statut	k1gInSc7	statut
(	(	kIx(	(
<g/>
Kyjev	Kyjev	k1gInSc1	Kyjev
a	a	k8xC	a
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
autonomní	autonomní	k2eAgFnSc4d1	autonomní
republiku	republika	k1gFnSc4	republika
(	(	kIx(	(
<g/>
Autonomní	autonomní	k2eAgFnSc1d1	autonomní
republika	republika	k1gFnSc1	republika
Krym	Krym	k1gInSc1	Krym
<g/>
)	)	kIx)	)
a	a	k8xC	a
24	[number]	k4	24
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
celky	celek	k1gInPc1	celek
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
anexe	anexe	k1gFnSc2	anexe
Krymu	Krym	k1gInSc2	Krym
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
město	město	k1gNnSc4	město
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
a	a	k8xC	a
Autonomní	autonomní	k2eAgFnSc1d1	autonomní
republika	republika	k1gFnSc1	republika
Krym	Krym	k1gInSc1	Krym
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
sporná	sporný	k2eAgFnSc1d1	sporná
Republika	republika	k1gFnSc1	republika
Krym	Krym	k1gInSc1	Krym
<g/>
)	)	kIx)	)
de	de	k?	de
facto	facto	k1gNnSc4	facto
součástí	součást	k1gFnPc2	součást
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
</s>
</p>
<p>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
