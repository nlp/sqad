<p>
<s>
Hokkaidó	Hokkaidó	k?	Hokkaidó
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
北	北	k?	北
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
japonský	japonský	k2eAgInSc1d1	japonský
ostrov	ostrov	k1gInSc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
chladné	chladný	k2eAgNnSc1d1	chladné
klima	klima	k1gNnSc1	klima
a	a	k8xC	a
na	na	k7c4	na
japonské	japonský	k2eAgInPc4d1	japonský
poměry	poměr	k1gInPc4	poměr
i	i	k8xC	i
nízkou	nízký	k2eAgFnSc4d1	nízká
hustotu	hustota	k1gFnSc4	hustota
osídlení	osídlení	k1gNnSc2	osídlení
(	(	kIx(	(
<g/>
66,4	[number]	k4	66,4
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
–	–	k?	–
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
77	[number]	k4	77
900	[number]	k4	900
km2	km2	k4	km2
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
5,76	[number]	k4	5,76
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
v	v	k7c6	v
Sapporu	Sappor	k1gInSc6	Sappor
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
kolonizován	kolonizovat	k5eAaBmNgInS	kolonizovat
centrální	centrální	k2eAgFnSc7d1	centrální
vládou	vláda	k1gFnSc7	vláda
období	období	k1gNnSc2	období
Meidži	Meidž	k1gFnSc3	Meidž
až	až	k6eAd1	až
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
nynější	nynější	k2eAgNnSc4d1	nynější
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgNnSc4d1	znamenající
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
správní	správní	k2eAgNnSc1d1	správní
<g/>
)	)	kIx)	)
oblast	oblast	k1gFnSc1	oblast
severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
pejorativním	pejorativní	k2eAgNnSc7d1	pejorativní
označením	označení	k1gNnSc7	označení
Ezo	Ezo	k1gFnSc2	Ezo
(	(	kIx(	(
<g/>
蝦	蝦	k?	蝦
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
barbarský	barbarský	k2eAgMnSc1d1	barbarský
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
obsazení	obsazení	k1gNnSc2	obsazení
nově	nově	k6eAd1	nově
vzniklým	vzniklý	k2eAgNnSc7d1	vzniklé
císařstvím	císařství	k1gNnSc7	císařství
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
obýván	obývat	k5eAaImNgInS	obývat
původním	původní	k2eAgInSc7d1	původní
národem	národ	k1gInSc7	národ
Ainuů	Ainu	k1gMnPc2	Ainu
s	s	k7c7	s
menšími	malý	k2eAgInPc7d2	menší
sídly	sídlo	k1gNnPc7	sídlo
japonských	japonský	k2eAgMnPc2d1	japonský
osadníků	osadník	k1gMnPc2	osadník
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
pozdní	pozdní	k2eAgFnSc1d1	pozdní
japonská	japonský	k2eAgFnSc1d1	japonská
kolonizace	kolonizace	k1gFnSc1	kolonizace
také	také	k9	také
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Japonska	Japonsko	k1gNnSc2	Japonsko
řídce	řídce	k6eAd1	řídce
osídlený	osídlený	k2eAgMnSc1d1	osídlený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
malými	malý	k2eAgInPc7d1	malý
ostrůvky	ostrůvek	k1gInPc7	ostrůvek
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
administrativně	administrativně	k6eAd1	administrativně
největší	veliký	k2eAgFnSc4d3	veliký
japonskou	japonský	k2eAgFnSc4d1	japonská
prefekturu	prefektura	k1gFnSc4	prefektura
<g/>
.	.	kIx.	.
</s>
<s>
Prefektura	prefektura	k1gFnSc1	prefektura
Hokkaidó	Hokkaidó	k1gFnPc2	Hokkaidó
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
podprefektury	podprefektura	k1gFnPc4	podprefektura
(	(	kIx(	(
(	(	kIx(	(
<g/>
総	総	k?	総
<g/>
)	)	kIx)	)
<g/>
振	振	k?	振
[	[	kIx(	[
<g/>
(	(	kIx(	(
<g/>
sógó	sógó	k?	sógó
<g/>
)	)	kIx)	)
<g/>
šinkókjoku	šinkókjok	k1gInSc2	šinkókjok
<g/>
]	]	kIx)	]
neboli	neboli	k8xC	neboli
支	支	k?	支
<g/>
[	[	kIx(	[
<g/>
šičó	šičó	k?	šičó
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Hokkaidó	Hokkaidó	k1gFnSc2	Hokkaidó
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
