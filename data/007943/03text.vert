<s>
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Karlstein	Karlstein	k1gInSc1	Karlstein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
obrozenecké	obrozenecký	k2eAgFnSc6d1	obrozenecká
době	doba	k1gFnSc6	doba
nazýván	nazývat	k5eAaImNgInS	nazývat
také	také	k9	také
Karlův	Karlův	k2eAgInSc1d1	Karlův
Týn	Týn	k1gInSc1	Týn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
středověký	středověký	k2eAgInSc4d1	středověký
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Budňany	Budňan	k1gMnPc4	Budňan
v	v	k7c6	v
městysi	městys	k1gInSc6	městys
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Beroun	Beroun	k1gInSc1	Beroun
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Český	český	k2eAgInSc1d1	český
kras	kras	k1gInSc1	kras
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
jako	jako	k8xC	jako
soukromé	soukromý	k2eAgNnSc4d1	soukromé
reprezentativní	reprezentativní	k2eAgNnSc4d1	reprezentativní
sídlo	sídlo	k1gNnSc4	sídlo
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
po	po	k7c6	po
Karlově	Karlův	k2eAgFnSc6d1	Karlova
císařské	císařský	k2eAgFnSc6d1	císařská
korunovaci	korunovace	k1gFnSc6	korunovace
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
<g/>
,	,	kIx,	,
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
císař	císař	k1gMnSc1	císař
účel	účel	k1gInSc4	účel
hradu	hrad	k1gInSc2	hrad
coby	coby	k?	coby
klenotnice	klenotnice	k1gFnSc2	klenotnice
pro	pro	k7c4	pro
uschování	uschování	k1gNnSc4	uschování
říšských	říšský	k2eAgInPc2d1	říšský
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
a	a	k8xC	a
souboru	soubor	k1gInSc2	soubor
svatých	svatý	k2eAgInPc2d1	svatý
ostatků	ostatek	k1gInPc2	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
převezeny	převézt	k5eAaPmNgInP	převézt
až	až	k9	až
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc1	hrad
významnou	významný	k2eAgFnSc7d1	významná
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgInPc2d3	nejnavštěvovanější
hradů	hrad	k1gInPc2	hrad
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
hrad	hrad	k1gInSc1	hrad
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
200	[number]	k4	200
000	[number]	k4	000
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Výzdoba	výzdoba	k1gFnSc1	výzdoba
nejcennějšího	cenný	k2eAgInSc2d3	nejcennější
prostoru	prostor	k1gInSc2	prostor
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
věži	věž	k1gFnSc6	věž
je	být	k5eAaImIp3nS	být
ukázkou	ukázka	k1gFnSc7	ukázka
vyspělého	vyspělý	k2eAgNnSc2d1	vyspělé
gotického	gotický	k2eAgNnSc2d1	gotické
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
správu	správa	k1gFnSc4	správa
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
veřejnosti	veřejnost	k1gFnSc3	veřejnost
přístupný	přístupný	k2eAgInSc4d1	přístupný
<g/>
.	.	kIx.	.
</s>
<s>
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
českým	český	k2eAgInSc7d1	český
a	a	k8xC	a
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
císařem	císař	k1gMnSc7	císař
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
poměrně	poměrně	k6eAd1	poměrně
skromná	skromný	k2eAgFnSc1d1	skromná
stavba	stavba	k1gFnSc1	stavba
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
uložit	uložit	k5eAaPmF	uložit
zde	zde	k6eAd1	zde
a	a	k8xC	a
ochraňovat	ochraňovat	k5eAaImF	ochraňovat
Korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
relikvií	relikvie	k1gFnPc2	relikvie
(	(	kIx(	(
<g/>
svatých	svatý	k2eAgInPc2d1	svatý
ostatků	ostatek	k1gInPc2	ostatek
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
výzkumů	výzkum	k1gInPc2	výzkum
pojal	pojmout	k5eAaPmAgMnS	pojmout
Karel	Karel	k1gMnSc1	Karel
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1356	[number]	k4	1356
<g/>
,	,	kIx,	,
a	a	k8xC	a
úměrně	úměrně	k6eAd1	úměrně
tomu	ten	k3xDgMnSc3	ten
byly	být	k5eAaImAgFnP	být
přistavovány	přistavován	k2eAgFnPc1d1	přistavována
nové	nový	k2eAgFnPc1d1	nová
věže	věž	k1gFnPc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
zahájení	zahájení	k1gNnSc3	zahájení
stavby	stavba	k1gFnSc2	stavba
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1348	[number]	k4	1348
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
první	první	k4xOgNnSc4	první
pražských	pražský	k2eAgMnPc2d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1348	[number]	k4	1348
<g/>
–	–	k?	–
<g/>
1357	[number]	k4	1357
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
výzdoba	výzdoba	k1gFnSc1	výzdoba
hradu	hrad	k1gInSc2	hrad
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1365	[number]	k4	1365
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sem	sem	k6eAd1	sem
byly	být	k5eAaImAgFnP	být
též	též	k9	též
přivezeny	přivezen	k2eAgInPc4d1	přivezen
české	český	k2eAgInPc4d1	český
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
Karlova	Karlův	k2eAgMnSc2d1	Karlův
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g/>
–	–	k?	–
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
převozu	převoz	k1gInSc6	převoz
však	však	k9	však
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
korunovaci	korunovace	k1gFnSc3	korunovace
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
(	(	kIx(	(
<g/>
1420	[number]	k4	1420
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
přivezeny	přivézt	k5eAaPmNgInP	přivézt
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
z	z	k7c2	z
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
byly	být	k5eAaImAgInP	být
klenoty	klenot	k1gInPc1	klenot
opět	opět	k6eAd1	opět
vráceny	vrácen	k2eAgInPc1d1	vrácen
na	na	k7c4	na
Karlštejn	Karlštejn	k1gInSc4	Karlštejn
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
však	však	k9	však
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
husitská	husitský	k2eAgFnSc1d1	husitská
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
Zikmund	Zikmund	k1gMnSc1	Zikmund
nechal	nechat	k5eAaPmAgMnS	nechat
jak	jak	k6eAd1	jak
české	český	k2eAgFnPc4d1	Česká
<g/>
,	,	kIx,	,
tak	tak	k9	tak
říšské	říšský	k2eAgInPc4d1	říšský
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
odvézt	odvézt	k5eAaPmF	odvézt
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
revoluce	revoluce	k1gFnSc2	revoluce
odolal	odolat	k5eAaPmAgInS	odolat
hrad	hrad	k1gInSc1	hrad
obléhání	obléhání	k1gNnSc2	obléhání
husitskými	husitský	k2eAgNnPc7d1	husitské
vojsky	vojsko	k1gNnPc7	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Korybutoviče	Korybutovič	k1gMnSc2	Korybutovič
(	(	kIx(	(
<g/>
1422	[number]	k4	1422
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Zikmund	Zikmund	k1gMnSc1	Zikmund
české	český	k2eAgFnSc2d1	Česká
korunovační	korunovační	k2eAgFnSc2d1	korunovační
klenoty	klenot	k1gInPc7	klenot
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Karlštejn	Karlštejn	k1gInSc4	Karlštejn
(	(	kIx(	(
<g/>
1436	[number]	k4	1436
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
zůstaly	zůstat	k5eAaPmAgInP	zůstat
uloženy	uložit	k5eAaPmNgInP	uložit
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Říšské	říšský	k2eAgInPc1d1	říšský
klenoty	klenot	k1gInPc1	klenot
již	již	k6eAd1	již
zůstaly	zůstat	k5eAaPmAgInP	zůstat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Husitskou	husitský	k2eAgFnSc7d1	husitská
revolucí	revoluce	k1gFnSc7	revoluce
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
původní	původní	k2eAgFnSc2d1	původní
funkce	funkce	k1gFnSc2	funkce
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Karlštejnští	karlštejnský	k2eAgMnPc1d1	karlštejnský
purkrabí	purkrabí	k1gMnPc1	purkrabí
střežili	střežit	k5eAaImAgMnP	střežit
české	český	k2eAgInPc4d1	český
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
a	a	k8xC	a
zemský	zemský	k2eAgInSc4d1	zemský
archiv	archiv	k1gInSc4	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
též	též	k9	též
ukládány	ukládán	k2eAgInPc1d1	ukládán
opisy	opis	k1gInPc1	opis
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Vladislavské	vladislavský	k2eAgNnSc1d1	Vladislavské
zřízení	zřízení	k1gNnSc1	zřízení
zemské	zemský	k2eAgFnSc2d1	zemská
vydané	vydaný	k2eAgFnSc2d1	vydaná
králem	král	k1gMnSc7	král
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Jagellonským	jagellonský	k2eAgMnSc7d1	jagellonský
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
přísahou	přísaha	k1gFnSc7	přísaha
karlštejnské	karlštejnský	k2eAgMnPc4d1	karlštejnský
purkrabí	purkrabí	k1gMnPc4	purkrabí
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
chránit	chránit	k5eAaImF	chránit
hrad	hrad	k1gInSc4	hrad
i	i	k9	i
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
klenoty	klenot	k1gInPc7	klenot
a	a	k8xC	a
zemskými	zemský	k2eAgNnPc7d1	zemské
privilegii	privilegium	k1gNnPc7	privilegium
pod	pod	k7c7	pod
trestem	trest	k1gInSc7	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
ztráty	ztráta	k1gFnSc2	ztráta
cti	čest	k1gFnSc2	čest
a	a	k8xC	a
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
vyhnání	vyhnání	k1gNnSc1	vyhnání
potomků	potomek	k1gMnPc2	potomek
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Korunu	koruna	k1gFnSc4	koruna
směli	smět	k5eAaImAgMnP	smět
vydat	vydat	k5eAaPmF	vydat
pouze	pouze	k6eAd1	pouze
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgInS	být
předtím	předtím	k6eAd1	předtím
řádně	řádně	k6eAd1	řádně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
klenoty	klenot	k1gInPc1	klenot
vraceny	vracet	k5eAaImNgInP	vracet
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
správy	správa	k1gFnSc2	správa
Jáchyma	Jáchym	k1gMnSc2	Jáchym
Novohradského	novohradský	k2eAgMnSc2d1	novohradský
z	z	k7c2	z
Kolovrat	kolovrat	k1gInSc4	kolovrat
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
renesanční	renesanční	k2eAgFnSc3d1	renesanční
přestavbě	přestavba	k1gFnSc3	přestavba
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Přestavba	přestavba	k1gFnSc1	přestavba
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
především	především	k6eAd1	především
Velké	velký	k2eAgFnSc2d1	velká
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
budovy	budova	k1gFnSc2	budova
purkrabství	purkrabství	k1gNnPc2	purkrabství
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejvíce	hodně	k6eAd3	hodně
využívána	využívat	k5eAaPmNgFnS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
byly	být	k5eAaImAgInP	být
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
direktorů	direktor	k1gMnPc2	direktor
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1619	[number]	k4	1619
odvezeny	odvézt	k5eAaPmNgFnP	odvézt
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
vojenské	vojenský	k2eAgFnSc2d1	vojenská
techniky	technika	k1gFnSc2	technika
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
obranná	obranný	k2eAgFnSc1d1	obranná
funkce	funkce	k1gFnSc1	funkce
hradu	hrad	k1gInSc2	hrad
shledána	shledat	k5eAaPmNgFnS	shledat
již	již	k6eAd1	již
jako	jako	k8xC	jako
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
revizi	revize	k1gFnSc6	revize
zemského	zemský	k2eAgNnSc2d1	zemské
zřízení	zřízení	k1gNnSc2	zřízení
roku	rok	k1gInSc2	rok
1625	[number]	k4	1625
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
zrušil	zrušit	k5eAaPmAgInS	zrušit
úřad	úřad	k1gInSc1	úřad
karlštejnských	karlštejnský	k2eAgMnPc2d1	karlštejnský
purkrabí	purkrabí	k1gMnSc1	purkrabí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pozbyl	pozbýt	k5eAaPmAgMnS	pozbýt
své	svůj	k3xOyFgFnPc4	svůj
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
českých	český	k2eAgFnPc2d1	Česká
královen	královna	k1gFnPc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
císařovna	císařovna	k1gFnSc1	císařovna
Eleonora	Eleonora	k1gFnSc1	Eleonora
zastavila	zastavit	k5eAaPmAgFnS	zastavit
hrad	hrad	k1gInSc4	hrad
Janu	Jan	k1gMnSc3	Jan
Kavkovi	Kavka	k1gMnSc3	Kavka
z	z	k7c2	z
Říčan	Říčany	k1gInPc2	Říčany
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nechal	nechat	k5eAaPmAgMnS	nechat
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
odvézt	odvézt	k5eAaPmF	odvézt
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
zbylé	zbylý	k2eAgInPc4d1	zbylý
svátostniny	svátostnin	k1gInPc4	svátostnin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
dobyt	dobyt	k2eAgInSc1d1	dobyt
švédským	švédský	k2eAgNnSc7d1	švédské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
značně	značně	k6eAd1	značně
zchátralý	zchátralý	k2eAgInSc1d1	zchátralý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
českých	český	k2eAgFnPc2d1	Česká
královen	královna	k1gFnPc2	královna
a	a	k8xC	a
císařoven	císařovna	k1gFnPc2	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
jej	on	k3xPp3gMnSc4	on
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnPc4	Terezie
darovala	darovat	k5eAaPmAgFnS	darovat
nově	nově	k6eAd1	nově
založenému	založený	k2eAgInSc3d1	založený
Ústavu	ústav	k1gInSc3	ústav
šlechtičen	šlechtična	k1gFnPc2	šlechtična
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
hrad	hrad	k1gInSc1	hrad
využíval	využívat	k5eAaImAgInS	využívat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
centrum	centrum	k1gNnSc1	centrum
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
správy	správa	k1gFnSc2	správa
pro	pro	k7c4	pro
okolní	okolní	k2eAgInPc4d1	okolní
statky	statek	k1gInPc4	statek
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
i	i	k9	i
nadále	nadále	k6eAd1	nadále
chátral	chátrat	k5eAaImAgInS	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
Pozornosti	pozornost	k1gFnPc1	pozornost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
národním	národní	k2eAgNnSc7d1	národní
obrozením	obrození	k1gNnSc7	obrození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
navštívil	navštívit	k5eAaPmAgMnS	navštívit
hrad	hrad	k1gInSc4	hrad
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vzápětí	vzápětí	k6eAd1	vzápětí
věnoval	věnovat	k5eAaPmAgMnS	věnovat
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
opravu	oprava	k1gFnSc4	oprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
probíhala	probíhat	k5eAaImAgFnS	probíhat
přestavba	přestavba	k1gFnSc1	přestavba
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
tzv.	tzv.	kA	tzv.
purismu	purismus	k1gInSc2	purismus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
hradu	hrad	k1gInSc2	hrad
navrátit	navrátit	k5eAaPmF	navrátit
původní	původní	k2eAgFnSc4d1	původní
gotickou	gotický	k2eAgFnSc4d1	gotická
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Prováděli	provádět	k5eAaImAgMnP	provádět
ji	on	k3xPp3gFnSc4	on
Josef	Josef	k1gMnSc1	Josef
Mocker	Mocker	k1gMnSc1	Mocker
nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
základě	základ	k1gInSc6	základ
koncepce	koncepce	k1gFnSc2	koncepce
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
profesora	profesor	k1gMnSc2	profesor
Friedricha	Friedrich	k1gMnSc2	Friedrich
von	von	k1gInSc1	von
Schmidta	Schmidt	k1gMnSc2	Schmidt
až	až	k8xS	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mockerově	Mockerův	k2eAgFnSc6d1	Mockerova
smrti	smrt	k1gFnSc6	smrt
1899	[number]	k4	1899
stavbu	stavba	k1gFnSc4	stavba
dokončil	dokončit	k5eAaPmAgMnS	dokončit
stavební	stavební	k2eAgMnSc1d1	stavební
rada	rada	k1gMnSc1	rada
inženýr	inženýr	k1gMnSc1	inženýr
Krch	Krch	k1gMnSc1	Krch
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
této	tento	k3xDgFnSc2	tento
přestavby	přestavba	k1gFnSc2	přestavba
byly	být	k5eAaImAgFnP	být
kritizovány	kritizovat	k5eAaImNgFnP	kritizovat
již	již	k6eAd1	již
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
hodnoceny	hodnotit	k5eAaImNgFnP	hodnotit
jako	jako	k8xC	jako
projev	projev	k1gInSc1	projev
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dobroslav	Dobroslav	k1gMnSc1	Dobroslav
Líbal	Líbal	k1gMnSc1	Líbal
však	však	k9	však
ostře	ostro	k6eAd1	ostro
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
Schmidtovy	Schmidtův	k2eAgFnPc4d1	Schmidtova
a	a	k8xC	a
Mockerovy	Mockerův	k2eAgFnPc4d1	Mockerova
úpravy	úprava	k1gFnPc4	úprava
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
ještě	ještě	k9	ještě
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
stého	stý	k4xOgInSc2	stý
výročí	výročí	k1gNnSc3	výročí
Mockerova	Mockerův	k2eAgNnSc2d1	Mockerův
úmrtí	úmrtí	k1gNnSc2	úmrtí
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc4	hrad
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zpřístupněn	zpřístupněn	k2eAgInSc1d1	zpřístupněn
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejnavštěvovanějších	navštěvovaný	k2eAgInPc2d3	nejnavštěvovanější
hradů	hrad	k1gInPc2	hrad
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
a	a	k8xC	a
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
Územní	územní	k2eAgFnSc2d1	územní
památkové	památkový	k2eAgFnSc2d1	památková
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vlastních	vlastní	k2eAgInPc2d1	vlastní
tří	tři	k4xCgInPc2	tři
prohlídkových	prohlídkový	k2eAgInPc2d1	prohlídkový
okruhů	okruh	k1gInPc2	okruh
s	s	k7c7	s
průvodcem	průvodce	k1gMnSc7	průvodce
nabízí	nabízet	k5eAaImIp3nS	nabízet
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
každoročně	každoročně	k6eAd1	každoročně
také	také	k9	také
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
akcí	akce	k1gFnPc2	akce
-	-	kIx~	-
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc4	výstava
<g/>
,	,	kIx,	,
Karlštejnské	karlštejnský	k2eAgNnSc4d1	Karlštejnské
kulturní	kulturní	k2eAgNnSc4d1	kulturní
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
Královský	královský	k2eAgInSc4d1	královský
průvod	průvod	k1gInSc4	průvod
či	či	k8xC	či
tradiční	tradiční	k2eAgNnSc4d1	tradiční
Karlštejnské	karlštejnský	k2eAgNnSc4d1	Karlštejnské
vinobraní	vinobraní	k1gNnSc4	vinobraní
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
možnost	možnost	k1gFnSc1	možnost
uspořádání	uspořádání	k1gNnSc2	uspořádání
svatebních	svatební	k2eAgInPc2d1	svatební
obřadů	obřad	k1gInPc2	obřad
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
ve	v	k7c6	v
svatební	svatební	k2eAgFnSc6d1	svatební
síni	síň	k1gFnSc6	síň
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Purkrabství	purkrabství	k1gNnSc2	purkrabství
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádvoří	nádvoří	k1gNnSc6	nádvoří
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Diamantová	diamantový	k2eAgFnSc1d1	Diamantová
síň	síň	k1gFnSc1	síň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Hodovní	hodovní	k2eAgFnSc6d1	hodovní
síni	síň	k1gFnSc6	síň
v	v	k7c6	v
Císařském	císařský	k2eAgInSc6d1	císařský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
či	či	k8xC	či
open	open	k1gInSc1	open
air	air	k?	air
svatební	svatební	k2eAgInSc4d1	svatební
obřad	obřad	k1gInSc4	obřad
pod	pod	k7c7	pod
Mariánskou	mariánský	k2eAgFnSc7d1	Mariánská
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
navštívit	navštívit	k5eAaPmF	navštívit
tři	tři	k4xCgInPc4	tři
prohlídkové	prohlídkový	k2eAgInPc4d1	prohlídkový
okruhy	okruh	k1gInPc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
okruh	okruh	k1gInSc1	okruh
-	-	kIx~	-
Soukromé	soukromý	k2eAgFnPc1d1	soukromá
a	a	k8xC	a
reprezentační	reprezentační	k2eAgFnPc1d1	reprezentační
prostory	prostora	k1gFnPc1	prostora
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Císařský	císařský	k2eAgInSc1d1	císařský
palác	palác	k1gInSc1	palác
a	a	k8xC	a
I.	I.	kA	I.
patro	patro	k1gNnSc1	patro
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
věže	věž	k1gFnPc1	věž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
okruh	okruh	k1gInSc4	okruh
-	-	kIx~	-
Posvátné	posvátný	k2eAgInPc4d1	posvátný
prostory	prostor	k1gInPc4	prostor
hradu	hrad	k1gInSc2	hrad
vč.	vč.	k?	vč.
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
Mariánské	mariánský	k2eAgFnSc2d1	Mariánská
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
přízemí	přízemí	k1gNnSc1	přízemí
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
Velké	velký	k2eAgFnSc2d1	velká
věže	věž	k1gFnSc2	věž
<g/>
)	)	kIx)	)
a	a	k8xC	a
nově	nově	k6eAd1	nově
také	také	k9	také
III	III	kA	III
<g/>
.	.	kIx.	.
prohlídkový	prohlídkový	k2eAgInSc1d1	prohlídkový
okruh	okruh	k1gInSc1	okruh
-	-	kIx~	-
Vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
věže	věž	k1gFnSc2	věž
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
Velké	velký	k2eAgFnSc2d1	velká
věže	věž	k1gFnSc2	věž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
je	být	k5eAaImIp3nS	být
mohutný	mohutný	k2eAgInSc1d1	mohutný
kamenný	kamenný	k2eAgInSc1d1	kamenný
gotický	gotický	k2eAgInSc1d1	gotický
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
vápencové	vápencový	k2eAgFnSc6d1	vápencová
skalní	skalní	k2eAgFnSc6d1	skalní
ostrožně	ostrožna	k1gFnSc6	ostrožna
(	(	kIx(	(
<g/>
316	[number]	k4	316
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
obcí	obec	k1gFnSc7	obec
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
samostatně	samostatně	k6eAd1	samostatně
opevněných	opevněný	k2eAgFnPc2d1	opevněná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vinice	vinice	k1gFnPc1	vinice
a	a	k8xC	a
hluboké	hluboký	k2eAgInPc1d1	hluboký
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
listnaté	listnatý	k2eAgInPc1d1	listnatý
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
částmi	část	k1gFnPc7	část
hradu	hrad	k1gInSc2	hrad
jsou	být	k5eAaImIp3nP	být
studniční	studniční	k2eAgFnSc1d1	studniční
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
Purkrabství	purkrabství	k1gNnSc2	purkrabství
<g/>
,	,	kIx,	,
Hodinová	hodinový	k2eAgFnSc1d1	hodinová
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
Císařský	císařský	k2eAgInSc1d1	císařský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
architektonické	architektonický	k2eAgInPc4d1	architektonický
prvky	prvek	k1gInPc4	prvek
hradu	hrad	k1gInSc2	hrad
jsou	být	k5eAaImIp3nP	být
stupňovitě	stupňovitě	k6eAd1	stupňovitě
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
podle	podle	k7c2	podle
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
přikládal	přikládat	k5eAaImAgMnS	přikládat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvýše	nejvýše	k6eAd1	nejvýše
je	být	k5eAaImIp3nS	být
položena	položen	k2eAgFnSc1d1	položena
Velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Hradní	hradní	k2eAgFnSc1d1	hradní
studna	studna	k1gFnSc1	studna
je	být	k5eAaImIp3nS	být
hluboká	hluboký	k2eAgFnSc1d1	hluboká
78	[number]	k4	78
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
přiváděla	přivádět	k5eAaImAgFnS	přivádět
štolou	štola	k1gFnSc7	štola
z	z	k7c2	z
Budňanského	Budňanský	k2eAgInSc2d1	Budňanský
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
tekoucího	tekoucí	k2eAgInSc2d1	tekoucí
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
na	na	k7c4	na
vytahování	vytahování	k1gNnSc4	vytahování
a	a	k8xC	a
spouštění	spouštění	k1gNnSc4	spouštění
okovu	okov	k1gInSc2	okov
tvoří	tvořit	k5eAaImIp3nS	tvořit
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
šlapali	šlapat	k5eAaImAgMnP	šlapat
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
uváděli	uvádět	k5eAaImAgMnP	uvádět
je	on	k3xPp3gMnPc4	on
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Purkrabství	purkrabství	k1gNnSc1	purkrabství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
mnohokrát	mnohokrát	k6eAd1	mnohokrát
přestavováno	přestavován	k2eAgNnSc1d1	přestavováno
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
přestavby	přestavba	k1gFnSc2	přestavba
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
správě	správa	k1gFnSc3	správa
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
slavnostním	slavnostní	k2eAgInSc7d1	slavnostní
obřadům	obřad	k1gInPc3	obřad
obecního	obecní	k2eAgInSc2d1	obecní
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
reprezentačním	reprezentační	k2eAgInPc3d1	reprezentační
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
jako	jako	k9	jako
obydlí	obydlí	k1gNnSc2	obydlí
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
místnosti	místnost	k1gFnSc2	místnost
sloužící	sloužící	k1gFnSc2	sloužící
panovníkovu	panovníkův	k2eAgInSc3d1	panovníkův
dvoru	dvůr	k1gInSc3	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
patro	patro	k1gNnSc4	patro
obýval	obývat	k5eAaImAgMnS	obývat
panovník	panovník	k1gMnSc1	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Karlova	Karlův	k2eAgFnSc1d1	Karlova
ložnice	ložnice	k1gFnSc1	ložnice
a	a	k8xC	a
reprezentační	reprezentační	k2eAgFnPc1d1	reprezentační
prostory	prostora	k1gFnPc1	prostora
jako	jako	k8xS	jako
sál	sál	k1gInSc1	sál
předků	předek	k1gMnPc2	předek
(	(	kIx(	(
<g/>
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
sál	sál	k1gInSc1	sál
<g/>
)	)	kIx)	)
či	či	k8xC	či
audienční	audienční	k2eAgFnSc1d1	audienční
síň	síň	k1gFnSc1	síň
–	–	k?	–
nejlépe	dobře	k6eAd3	dobře
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
interiér	interiér	k1gInSc4	interiér
paláce	palác	k1gInSc2	palác
s	s	k7c7	s
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
kazetovým	kazetový	k2eAgNnSc7d1	kazetové
obložením	obložení	k1gNnSc7	obložení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ložnice	ložnice	k1gFnSc2	ložnice
vede	vést	k5eAaImIp3nS	vést
točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
obývaných	obývaný	k2eAgFnPc2d1	obývaná
královnou	královna	k1gFnSc7	královna
a	a	k8xC	a
jejími	její	k3xOp3gFnPc7	její
dámami	dáma	k1gFnPc7	dáma
(	(	kIx(	(
<g/>
fraucimorem	fraucimorem	k?	fraucimorem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgMnSc1d2	menší
z	z	k7c2	z
věží	věž	k1gFnPc2	věž
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
můstkem	můstek	k1gInSc7	můstek
s	s	k7c7	s
císařským	císařský	k2eAgInSc7d1	císařský
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kostel	kostel	k1gInSc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
se	s	k7c7	s
sakristií	sakristie	k1gFnSc7	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
cenné	cenný	k2eAgFnPc1d1	cenná
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
s	s	k7c7	s
biblickými	biblický	k2eAgInPc7d1	biblický
výjevy	výjev	k1gInPc7	výjev
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
ostatkovými	ostatkový	k2eAgFnPc7d1	ostatková
scénami	scéna	k1gFnPc7	scéna
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
přijímá	přijímat	k5eAaImIp3nS	přijímat
a	a	k8xC	a
ukládá	ukládat	k5eAaImIp3nS	ukládat
ostatky	ostatek	k1gInPc7	ostatek
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
maleb	malba	k1gFnPc2	malba
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Wurmser	Wurmser	k1gMnSc1	Wurmser
ze	z	k7c2	z
Štrasburku	Štrasburk	k1gInSc2	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
slouží	sloužit	k5eAaImIp3nS	sloužit
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
v	v	k7c4	v
den	den	k1gInSc4	den
výročí	výročí	k1gNnSc4	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
pak	pak	k6eAd1	pak
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
soukromá	soukromý	k2eAgFnSc1d1	soukromá
oratoř	oratoř	k1gFnSc1	oratoř
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
kaple	kaple	k1gFnSc2	kaple
jsou	být	k5eAaImIp3nP	být
obloženy	obložen	k2eAgInPc1d1	obložen
polodrahokamy	polodrahokam	k1gInPc1	polodrahokam
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
vchodem	vchod	k1gInSc7	vchod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvojportrét	dvojportrét	k1gInSc1	dvojportrét
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
třetí	třetí	k4xOgFnPc1	třetí
manželky	manželka	k1gFnPc1	manželka
Anny	Anna	k1gFnSc2	Anna
Svídnické	Svídnický	k2eAgInPc1d1	Svídnický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pokladnice	pokladnice	k1gFnPc1	pokladnice
a	a	k8xC	a
klenotnice	klenotnice	k1gFnPc1	klenotnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
vystaveny	vystavit	k5eAaPmNgInP	vystavit
předměty	předmět	k1gInPc1	předmět
náležející	náležející	k2eAgInPc1d1	náležející
ke	k	k7c3	k
karlštejnskému	karlštejnský	k2eAgNnSc3d1	Karlštejnské
pokladu	poklást	k5eAaPmIp1nS	poklást
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klenotnici	klenotnice	k1gFnSc6	klenotnice
je	být	k5eAaImIp3nS	být
vystavena	vystaven	k2eAgFnSc1d1	vystavena
kopie	kopie	k1gFnSc1	kopie
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Prostory	prostora	k1gFnPc1	prostora
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xS	jako
Červenka	červenka	k1gFnSc1	červenka
<g/>
,	,	kIx,	,
sloužily	sloužit	k5eAaImAgInP	sloužit
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
jako	jako	k8xC	jako
hradní	hradní	k2eAgNnPc1d1	hradní
vězení	vězení	k1gNnPc1	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Mariánskou	mariánský	k2eAgFnSc7d1	Mariánská
věží	věž	k1gFnSc7	věž
krytým	krytý	k2eAgInSc7d1	krytý
dřevěným	dřevěný	k2eAgInSc7d1	dřevěný
mostem	most	k1gInSc7	most
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
místě	místo	k1gNnSc6	místo
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
největšímu	veliký	k2eAgInSc3d3	veliký
duchovnímu	duchovní	k2eAgInSc3d1	duchovní
významu	význam	k1gInSc3	význam
a	a	k8xC	a
účelu	účel	k1gInSc3	účel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jí	on	k3xPp3gFnSc3	on
zakladatel	zakladatel	k1gMnSc1	zakladatel
hradu	hrad	k1gInSc2	hrad
určil	určit	k5eAaPmAgMnS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
schodišti	schodiště	k1gNnSc6	schodiště
s	s	k7c7	s
nástěnnými	nástěnný	k2eAgFnPc7d1	nástěnná
malbami	malba	k1gFnPc7	malba
ze	z	k7c2	z
života	život	k1gInSc2	život
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc4	Václav
a	a	k8xC	a
sv.	sv.	kA	sv.
Ludmily	Ludmila	k1gFnSc2	Ludmila
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
nejcennějšího	cenný	k2eAgInSc2d3	nejcennější
prostoru	prostor	k1gInSc2	prostor
hradu	hrad	k1gInSc2	hrad
–	–	k?	–
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
uloženy	uložen	k2eAgInPc4d1	uložen
říšské	říšský	k2eAgInPc4d1	říšský
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
a	a	k8xC	a
svaté	svatý	k2eAgInPc4d1	svatý
ostatky	ostatek	k1gInPc4	ostatek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
české	český	k2eAgInPc4d1	český
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
a	a	k8xC	a
zemský	zemský	k2eAgInSc4d1	zemský
archiv	archiv	k1gInSc4	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
kaple	kaple	k1gFnSc2	kaple
je	být	k5eAaImIp3nS	být
ozdoben	ozdobit	k5eAaPmNgInS	ozdobit
zlacením	zlacení	k1gNnSc7	zlacení
<g/>
,	,	kIx,	,
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
a	a	k8xC	a
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
měsícem	měsíc	k1gInSc7	měsíc
z	z	k7c2	z
benátského	benátský	k2eAgNnSc2d1	benátské
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
pozlacenou	pozlacený	k2eAgFnSc7d1	pozlacená
mříží	mříž	k1gFnSc7	mříž
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Karel	Karel	k1gMnSc1	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
na	na	k7c4	na
důkaz	důkaz	k1gInSc4	důkaz
pokory	pokora	k1gFnSc2	pokora
a	a	k8xC	a
úcty	úcta	k1gFnSc2	úcta
vstupoval	vstupovat	k5eAaImAgMnS	vstupovat
za	za	k7c4	za
zlatou	zlatý	k2eAgFnSc4d1	zlatá
mříž	mříž	k1gFnSc4	mříž
bos	bos	k1gMnSc1	bos
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
unikátní	unikátní	k2eAgInSc1d1	unikátní
soubor	soubor	k1gInSc1	soubor
deskových	deskový	k2eAgInPc2d1	deskový
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Mistr	mistr	k1gMnSc1	mistr
Theodorik	Theodorik	k1gMnSc1	Theodorik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obraz	obraz	k1gInSc1	obraz
Ukřižování	ukřižování	k1gNnSc2	ukřižování
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
obrazy	obraz	k1gInPc1	obraz
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
nebeské	nebeský	k2eAgNnSc4d1	nebeské
vojsko	vojsko	k1gNnSc4	vojsko
–	–	k?	–
svaté	svatý	k2eAgMnPc4d1	svatý
mučedníky	mučedník	k1gMnPc4	mučedník
<g/>
,	,	kIx,	,
svaté	svatý	k2eAgMnPc4d1	svatý
vdovy	vdova	k1gFnSc2	vdova
a	a	k8xC	a
panny	panna	k1gFnSc2	panna
<g/>
,	,	kIx,	,
svaté	svatý	k1gMnPc4	svatý
rytíře	rytíř	k1gMnSc4	rytíř
<g/>
,	,	kIx,	,
biskupy	biskup	k1gMnPc4	biskup
<g/>
,	,	kIx,	,
papeže	papež	k1gMnPc4	papež
a	a	k8xC	a
svaté	svatý	k2eAgMnPc4d1	svatý
vládce	vládce	k1gMnPc4	vládce
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Velikým	veliký	k2eAgMnSc7d1	veliký
a	a	k8xC	a
svatým	svatý	k2eAgMnSc7d1	svatý
Václavem	Václav	k1gMnSc7	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
obrazy	obraz	k1gInPc1	obraz
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
i	i	k9	i
na	na	k7c6	na
rámech	rám	k1gInPc6	rám
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
zároveň	zároveň	k6eAd1	zároveň
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
relikviáře	relikviář	k1gInPc1	relikviář
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
zabezpečena	zabezpečit	k5eAaPmNgFnS	zabezpečit
čtverými	čtverý	k4xRgFnPc7	čtverý
dveřmi	dveře	k1gFnPc7	dveře
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
železnými	železný	k2eAgInPc7d1	železný
pláty	plát	k1gInPc7	plát
<g/>
.	.	kIx.	.
</s>
<s>
Uzamčena	uzamčen	k2eAgFnSc1d1	uzamčena
byla	být	k5eAaImAgFnS	být
devíti	devět	k4xCc2	devět
zámky	zámek	k1gInPc7	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
byl	být	k5eAaImAgInS	být
hlídán	hlídat	k5eAaImNgInS	hlídat
strážními	strážní	k2eAgFnPc7d1	strážní
službami	služba	k1gFnPc7	služba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sídlily	sídlit	k5eAaImAgFnP	sídlit
nad	nad	k7c7	nad
kaplí	kaple	k1gFnSc7	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
kaple	kaple	k1gFnSc2	kaple
byl	být	k5eAaImAgInS	být
povolen	povolit	k5eAaPmNgInS	povolit
jen	jen	k6eAd1	jen
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jehož	jenž	k3xRgNnSc2	jenž
nařízení	nařízení	k1gNnSc2	nařízení
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věži	věž	k1gFnSc6	věž
nikdo	nikdo	k3yNnSc1	nikdo
nesměl	smět	k5eNaImAgMnS	smět
"	"	kIx"	"
<g/>
spáti	spát	k5eAaImF	spát
nebo	nebo	k8xC	nebo
ležeti	ležet	k5eAaImF	ležet
<g/>
"	"	kIx"	"
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
zákonitá	zákonitý	k2eAgFnSc1d1	zákonitá
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
jen	jen	k9	jen
zvlášť	zvlášť	k6eAd1	zvlášť
významné	významný	k2eAgFnPc4d1	významná
mše	mše	k1gFnPc4	mše
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
směli	smět	k5eAaImAgMnP	smět
sloužit	sloužit	k5eAaImF	sloužit
pouze	pouze	k6eAd1	pouze
arcibiskupové	arcibiskup	k1gMnPc1	arcibiskup
a	a	k8xC	a
karlštejnský	karlštejnský	k2eAgMnSc1d1	karlštejnský
děkan	děkan	k1gMnSc1	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Karlštejn	Karlštejn	k1gInSc4	Karlštejn
Budňanská	Budňanský	k2eAgFnSc1d1	Budňanská
skála	skála	k1gFnSc1	skála
–	–	k?	–
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
geologický	geologický	k2eAgInSc1d1	geologický
odkryv	odkryv	k1gInSc1	odkryv
zvrásněných	zvrásněný	k2eAgFnPc2d1	zvrásněná
vrstev	vrstva	k1gFnPc2	vrstva
vápenců	vápenec	k1gInPc2	vápenec
a	a	k8xC	a
břidlic	břidlice	k1gFnPc2	břidlice
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
Dub	dub	k1gInSc4	dub
sedmi	sedm	k4xCc2	sedm
bratří	bratr	k1gMnPc2	bratr
–	–	k?	–
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
dub	dub	k1gInSc4	dub
letní	letní	k2eAgInSc4d1	letní
o	o	k7c6	o
stáří	stáří	k1gNnSc6	stáří
cca	cca	kA	cca
350	[number]	k4	350
let	léto	k1gNnPc2	léto
Mořina	Mořino	k1gNnSc2	Mořino
–	–	k?	–
velké	velký	k2eAgInPc4d1	velký
opuštěné	opuštěný	k2eAgInPc4d1	opuštěný
vápencové	vápencový	k2eAgInPc4d1	vápencový
lomy	lom	k1gInPc4	lom
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Velká	velká	k1gFnSc1	velká
a	a	k8xC	a
Malá	malý	k2eAgFnSc1d1	malá
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
propojené	propojený	k2eAgInPc1d1	propojený
navzájem	navzájem	k6eAd1	navzájem
podzemními	podzemní	k2eAgFnPc7d1	podzemní
šachtami	šachta	k1gFnPc7	šachta
Svatý	svatý	k1gMnSc1	svatý
Jan	Jan	k1gMnSc1	Jan
pod	pod	k7c7	pod
Skalou	Skala	k1gMnSc7	Skala
–	–	k?	–
ves	ves	k1gFnSc1	ves
s	s	k7c7	s
klášterem	klášter	k1gInSc7	klášter
a	a	k8xC	a
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
u	u	k7c2	u
jeskyně	jeskyně	k1gFnSc2	jeskyně
legendárního	legendární	k2eAgMnSc4d1	legendární
poustevníka	poustevník	k1gMnSc4	poustevník
Sv.	sv.	kA	sv.
Ivana	Ivan	k1gMnSc4	Ivan
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
5	[number]	k4	5
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
význam	význam	k1gInSc4	význam
hradu	hrad	k1gInSc2	hrad
Karlštejna	Karlštejn	k1gInSc2	Karlštejn
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
domácím	domácí	k2eAgMnPc3d1	domácí
i	i	k8xC	i
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
turistům	turist	k1gMnPc3	turist
a	a	k8xC	a
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
turistický	turistický	k2eAgInSc1d1	turistický
Region	region	k1gInSc1	region
Karlštejnsko	Karlštejnsko	k1gNnSc1	Karlštejnsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
městyse	městys	k1gInSc2	městys
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
a	a	k8xC	a
hradu	hrad	k1gInSc2	hrad
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
množství	množství	k1gNnSc4	množství
naučných	naučný	k2eAgFnPc2d1	naučná
stezek	stezka	k1gFnPc2	stezka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gNnPc6	jejichž
zastaveních	zastavení	k1gNnPc6	zastavení
se	se	k3xPyFc4	se
návštěvníci	návštěvník	k1gMnPc1	návštěvník
mohou	moct	k5eAaImIp3nP	moct
seznámit	seznámit	k5eAaPmF	seznámit
s	s	k7c7	s
historickými	historický	k2eAgFnPc7d1	historická
<g/>
,	,	kIx,	,
architektonickými	architektonický	k2eAgFnPc7d1	architektonická
<g/>
,	,	kIx,	,
přírodními	přírodní	k2eAgFnPc7d1	přírodní
a	a	k8xC	a
technickými	technický	k2eAgFnPc7d1	technická
pozoruhodnostmi	pozoruhodnost	k1gFnPc7	pozoruhodnost
Karlštejnska	Karlštejnsko	k1gNnSc2	Karlštejnsko
<g/>
.	.	kIx.	.
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
český	český	k2eAgInSc4d1	český
hudební	hudební	k2eAgInSc4d1	hudební
film	film	k1gInSc4	film
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
natočený	natočený	k2eAgInSc4d1	natočený
na	na	k7c4	na
náměty	námět	k1gInPc4	námět
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Vrchlického	Vrchlického	k2eAgFnSc1d1	Vrchlického
kniha	kniha	k1gFnSc1	kniha
Karštejnské	Karštejnský	k2eAgFnSc2d1	Karštejnský
vigilie	vigilie	k1gFnSc2	vigilie
Františka	František	k1gMnSc2	František
Kubky	Kubka	k1gMnSc2	Kubka
kniha	kniha	k1gFnSc1	kniha
Návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
ráje	ráj	k1gInSc2	ráj
Čestmíra	Čestmír	k1gMnSc2	Čestmír
Vejdělka	Vejdělek	k1gMnSc2	Vejdělek
</s>
