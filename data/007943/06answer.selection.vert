<s>
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
českým	český	k2eAgInSc7d1	český
a	a	k8xC	a
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
císařem	císař	k1gMnSc7	císař
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
poměrně	poměrně	k6eAd1	poměrně
skromná	skromný	k2eAgFnSc1d1	skromná
stavba	stavba	k1gFnSc1	stavba
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
