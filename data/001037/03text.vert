<s>
Tišnov	Tišnov	k1gInSc1	Tišnov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Tischnowitz	Tischnowitz	k1gInSc1	Tischnowitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
Boskovické	boskovický	k2eAgFnSc6d1	boskovická
brázdě	brázda	k1gFnSc6	brázda
<g/>
,	,	kIx,	,
22	[number]	k4	22
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
soutoku	soutok	k1gInSc2	soutok
Svratky	Svratka	k1gFnSc2	Svratka
a	a	k8xC	a
Loučky	loučka	k1gFnSc2	loučka
<g/>
.	.	kIx.	.
</s>
<s>
Tišnov	Tišnov	k1gInSc1	Tišnov
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
ocenění	ocenění	k1gNnSc3	ocenění
Obec	obec	k1gFnSc1	obec
přátelská	přátelský	k2eAgFnSc1d1	přátelská
rodině	rodina	k1gFnSc3	rodina
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
částí	část	k1gFnPc2	část
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
tři	tři	k4xCgInPc1	tři
nesousedící	sousedící	k2eNgInPc1d1	sousedící
územní	územní	k2eAgInPc1d1	územní
celky	celek	k1gInPc1	celek
<g/>
:	:	kIx,	:
Jeden	jeden	k4xCgInSc1	jeden
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
částmi	část	k1gFnPc7	část
Hajánky	Hajánka	k1gFnSc2	Hajánka
<g/>
,	,	kIx,	,
Hájek	Hájek	k1gMnSc1	Hájek
(	(	kIx(	(
<g/>
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Hájek	Hájek	k1gMnSc1	Hájek
u	u	k7c2	u
Tišnova	Tišnov	k1gInSc2	Tišnov
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jamné	Jamné	k2eAgMnPc1d1	Jamné
(	(	kIx(	(
<g/>
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Jamné	Jamná	k1gFnSc2	Jamná
u	u	k7c2	u
Tišnova	Tišnov	k1gInSc2	Tišnov
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
druhý	druhý	k4xOgMnSc1	druhý
tvoří	tvořit	k5eAaImIp3nS	tvořit
část	část	k1gFnSc1	část
Pejškov	Pejškov	k1gInSc1	Pejškov
(	(	kIx(	(
<g/>
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Pejškov	Pejškov	k1gInSc1	Pejškov
u	u	k7c2	u
Tišnova	Tišnov	k1gInSc2	Tišnov
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetí	třetí	k4xOgInSc1	třetí
samotný	samotný	k2eAgInSc1d1	samotný
Tišnov	Tišnov	k1gInSc1	Tišnov
(	(	kIx(	(
<g/>
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Tišnov	Tišnov	k1gInSc1	Tišnov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Tišnově	Tišnov	k1gInSc6	Tišnov
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
název	název	k1gInSc1	název
Tusnovice	Tusnovice	k1gFnSc2	Tusnovice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1233	[number]	k4	1233
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgInSc4d1	zmiňován
cisterciácký	cisterciácký	k2eAgInSc4d1	cisterciácký
klášter	klášter	k1gInSc4	klášter
Porta	porto	k1gNnSc2	porto
Coeli	Coel	k1gMnPc1	Coel
založený	založený	k2eAgMnSc1d1	založený
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
Konstancií	Konstancie	k1gFnSc7	Konstancie
Uherskou	uherský	k2eAgFnSc7d1	uherská
(	(	kIx(	(
<g/>
1181	[number]	k4	1181
<g/>
-	-	kIx~	-
<g/>
1240	[number]	k4	1240
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
Přemyslu	Přemysl	k1gMnSc6	Přemysl
Otakarovi	Otakarův	k2eAgMnPc1d1	Otakarův
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1155	[number]	k4	1155
<g/>
-	-	kIx~	-
<g/>
1230	[number]	k4	1230
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
kláštera	klášter	k1gInSc2	klášter
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
Tišnov	Tišnov	k1gInSc1	Tišnov
moravským	moravský	k2eAgMnSc7d1	moravský
markrabětem	markrabě	k1gMnSc7	markrabě
Přemyslem	Přemysl	k1gMnSc7	Přemysl
(	(	kIx(	(
<g/>
bratrem	bratr	k1gMnSc7	bratr
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
<g/>
)	)	kIx)	)
darován	darovat	k5eAaPmNgInS	darovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
kláštera	klášter	k1gInSc2	klášter
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
zrušení	zrušení	k1gNnSc2	zrušení
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
<g/>
.	.	kIx.	.
</s>
<s>
Tišnov	Tišnov	k1gInSc1	Tišnov
se	se	k3xPyFc4	se
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
v	v	k7c4	v
městečko	městečko	k1gNnSc4	městečko
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1416	[number]	k4	1416
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
králem	král	k1gMnSc7	král
Václavem	Václav	k1gMnSc7	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
uděleno	udělen	k2eAgNnSc4d1	uděleno
právo	právo	k1gNnSc4	právo
konat	konat	k5eAaImF	konat
výroční	výroční	k2eAgInSc4d1	výroční
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
město	město	k1gNnSc4	město
byl	být	k5eAaImAgInS	být
Tišnov	Tišnov	k1gInSc1	Tišnov
povýšen	povýšen	k2eAgInSc1d1	povýšen
roku	rok	k1gInSc2	rok
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Tišnov	Tišnov	k1gInSc1	Tišnov
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
sídlem	sídlo	k1gNnSc7	sídlo
správy	správa	k1gFnSc2	správa
tišnovského	tišnovský	k2eAgNnSc2d1	tišnovské
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Tišnov	Tišnov	k1gInSc1	Tišnov
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Tišnov	Tišnov	k1gInSc1	Tišnov
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869-1890	[number]	k4	1869-1890
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-okolí	Brnokolí	k1gNnSc2	Brno-okolí
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900-1950	[number]	k4	1900-1950
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Tišnov	Tišnov	k1gInSc1	Tišnov
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
byl	být	k5eAaImAgInS	být
Tišnov	Tišnov	k1gInSc1	Tišnov
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961-1970	[number]	k4	1961-1970
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
za	za	k7c2	za
husitských	husitský	k2eAgInPc2d1	husitský
bojů	boj	k1gInPc2	boj
roku	rok	k1gInSc2	rok
1428	[number]	k4	1428
a	a	k8xC	a
škod	škoda	k1gFnPc2	škoda
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
ušetřeno	ušetřit	k5eAaPmNgNnS	ušetřit
ani	ani	k8xC	ani
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
impuls	impuls	k1gInSc1	impuls
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
znamenala	znamenat	k5eAaImAgFnS	znamenat
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
zavedená	zavedený	k2eAgFnSc1d1	zavedená
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
město	město	k1gNnSc4	město
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Brnem	Brno	k1gNnSc7	Brno
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
navazující	navazující	k2eAgFnSc1d1	navazující
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
přes	přes	k7c4	přes
Tišnov	Tišnov	k1gInSc4	Tišnov
nová	nový	k2eAgFnSc1d1	nová
dvojkolejná	dvojkolejný	k2eAgFnSc1d1	dvojkolejná
trať	trať	k1gFnSc1	trať
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
stará	starat	k5eAaImIp3nS	starat
Tišnovka	Tišnovka	k1gFnSc1	Tišnovka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Tišnově	Tišnov	k1gInSc6	Tišnov
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
sanatorium	sanatorium	k1gNnSc1	sanatorium
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
krajská	krajský	k2eAgFnSc1d1	krajská
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
Tišnov	Tišnov	k1gInSc4	Tišnov
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
správní	správní	k2eAgInSc1d1	správní
obvod	obvod	k1gInSc1	obvod
čítá	čítat	k5eAaImIp3nS	čítat
celkem	celkem	k6eAd1	celkem
59	[number]	k4	59
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Tišnov	Tišnov	k1gInSc1	Tišnov
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
silnic	silnice	k1gFnPc2	silnice
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
385	[number]	k4	385
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
379	[number]	k4	379
(	(	kIx(	(
<g/>
Vyškov	Vyškov	k1gInSc1	Vyškov
-	-	kIx~	-
Velká	velký	k2eAgFnSc1d1	velká
Bíteš	Bíteš	k1gFnSc1	Bíteš
<g/>
)	)	kIx)	)
a	a	k8xC	a
377	[number]	k4	377
(	(	kIx(	(
<g/>
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
-	-	kIx~	-
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nájezd	nájezd	k1gInSc1	nájezd
na	na	k7c4	na
dálnici	dálnice	k1gFnSc4	dálnice
D1	D1	k1gFnSc2	D1
u	u	k7c2	u
Velké	velký	k2eAgFnSc2d1	velká
Bíteše	Bíteše	k1gFnSc2	Bíteše
(	(	kIx(	(
<g/>
exit	exit	k1gInSc1	exit
162	[number]	k4	162
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Tišnova	Tišnov	k1gInSc2	Tišnov
vzdálen	vzdálen	k2eAgInSc4d1	vzdálen
21	[number]	k4	21
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
procházejí	procházet	k5eAaImIp3nP	procházet
železniční	železniční	k2eAgFnPc4d1	železniční
tratě	trať	k1gFnPc4	trať
250	[number]	k4	250
(	(	kIx(	(
<g/>
Kúty	Kúta	k1gFnSc2	Kúta
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
a	a	k8xC	a
251	[number]	k4	251
(	(	kIx(	(
<g/>
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Bystřice	Bystřice	k1gFnSc1	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
-	-	kIx~	-
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tišnov	Tišnov	k1gInSc1	Tišnov
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
přestupním	přestupní	k2eAgInSc7d1	přestupní
uzlem	uzel	k1gInSc7	uzel
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
330	[number]	k4	330
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
S3	S3	k1gFnSc2	S3
(	(	kIx(	(
<g/>
Břeclav	Břeclav	k1gFnSc1	Břeclav
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Níhov	Níhov	k1gInSc1	Níhov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
S31	S31	k1gFnSc1	S31
(	(	kIx(	(
<g/>
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Rovné-Divišov	Rovné-Divišov	k1gInSc1	Rovné-Divišov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
S1	S1	k1gFnSc1	S1
(	(	kIx(	(
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
-Slatina	-Slatina	k1gFnSc1	-Slatina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlíky	rychlík	k1gInPc1	rychlík
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
R3	R3	k1gFnSc2	R3
(	(	kIx(	(
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
a	a	k8xC	a
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
153	[number]	k4	153
<g/>
,	,	kIx,	,
163	[number]	k4	163
<g/>
,	,	kIx,	,
311	[number]	k4	311
<g/>
,	,	kIx,	,
312	[number]	k4	312
<g/>
,	,	kIx,	,
330	[number]	k4	330
<g/>
,	,	kIx,	,
331	[number]	k4	331
<g/>
,	,	kIx,	,
332	[number]	k4	332
<g/>
,	,	kIx,	,
333	[number]	k4	333
<g/>
,	,	kIx,	,
334	[number]	k4	334
<g/>
,	,	kIx,	,
335	[number]	k4	335
a	a	k8xC	a
336	[number]	k4	336
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Tišnov	Tišnov	k1gInSc4	Tišnov
vede	vést	k5eAaImIp3nS	vést
několik	několik	k4yIc4	několik
cyklotras	cyklotrasa	k1gFnPc2	cyklotrasa
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
některé	některý	k3yIgFnSc3	některý
značené	značený	k2eAgFnSc3d1	značená
<g/>
:	:	kIx,	:
Pražská	pražský	k2eAgFnSc1d1	Pražská
trasa	trasa	k1gFnSc1	trasa
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Březina	Březina	k1gMnSc1	Březina
-	-	kIx~	-
Kopaniny	kopanina	k1gFnPc1	kopanina
-	-	kIx~	-
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
-	-	kIx~	-
hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgInSc1d1	veveří
-	-	kIx~	-
Brno-Bystrc	Brno-Bystrc	k1gInSc1	Brno-Bystrc
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
1	[number]	k4	1
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Dvůr	Dvůr	k1gInSc1	Dvůr
-	-	kIx~	-
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
-	-	kIx~	-
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
-	-	kIx~	-
Seč	seč	k1gFnSc1	seč
-	-	kIx~	-
Ronov	Ronov	k1gInSc1	Ronov
nad	nad	k7c7	nad
Doubravou	Doubrava	k1gMnSc7	Doubrava
Částečně	částečně	k6eAd1	částečně
značené	značený	k2eAgFnSc2d1	značená
a	a	k8xC	a
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
trasy	trasa	k1gFnSc2	trasa
<g/>
:	:	kIx,	:
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
5172	[number]	k4	5172
<g/>
:	:	kIx,	:
Tišnov	Tišnov	k1gInSc1	Tišnov
-	-	kIx~	-
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
-	-	kIx~	-
Rosice	Rosice	k1gFnPc1	Rosice
-	-	kIx~	-
Bratčice	Bratčice	k1gFnPc1	Bratčice
-	-	kIx~	-
Židlochovice	Židlochovice	k1gFnPc1	Židlochovice
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Tišnově	Tišnov	k1gInSc6	Tišnov
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
památkou	památka	k1gFnSc7	památka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Tišnova	Tišnov	k1gInSc2	Tišnov
je	být	k5eAaImIp3nS	být
cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
klášter	klášter	k1gInSc1	klášter
Porta	porto	k1gNnSc2	porto
Coeli	Coele	k1gFnSc4	Coele
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Předklášteří	Předklášteří	k1gNnSc2	Předklášteří
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
stojí	stát	k5eAaImIp3nS	stát
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
tišnovského	tišnovský	k2eAgNnSc2d1	tišnovské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
centrálním	centrální	k2eAgNnSc6d1	centrální
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
stojí	stát	k5eAaImIp3nS	stát
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
nedaleko	nedaleko	k7c2	nedaleko
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Na	na	k7c6	na
Hrádku	Hrádok	k1gInSc6	Hrádok
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
chráněné	chráněný	k2eAgNnSc1d1	chráněné
stromořadí	stromořadí	k1gNnSc1	stromořadí
lip	lípa	k1gFnPc2	lípa
velkolistých	velkolistý	k2eAgFnPc2d1	velkolistá
(	(	kIx(	(
<g/>
evidovány	evidován	k2eAgFnPc4d1	evidována
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Lipové	lipový	k2eAgNnSc1d1	lipové
stromořadí	stromořadí	k1gNnSc1	stromořadí
Na	na	k7c6	na
Hrádku	Hrádok	k1gInSc6	Hrádok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
zachováno	zachovat	k5eAaPmNgNnS	zachovat
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
původních	původní	k2eAgInPc2d1	původní
barokních	barokní	k2eAgInPc2d1	barokní
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Riegrově	Riegrův	k2eAgFnSc6d1	Riegrova
ulici	ulice	k1gFnSc6	ulice
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
secesních	secesní	k2eAgFnPc2d1	secesní
vil	vila	k1gFnPc2	vila
chráněných	chráněný	k2eAgFnPc2d1	chráněná
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vilou	vila	k1gFnSc7	vila
Franke	Frank	k1gMnSc5	Frank
roste	růst	k5eAaImIp3nS	růst
skupina	skupina	k1gFnSc1	skupina
dvou	dva	k4xCgFnPc2	dva
borovic	borovice	k1gFnPc2	borovice
černých	černá	k1gFnPc2	černá
<g/>
,	,	kIx,	,
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Borovice	borovice	k1gFnSc2	borovice
u	u	k7c2	u
vily	vila	k1gFnSc2	vila
Franke	Frank	k1gMnSc5	Frank
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
stavby	stavba	k1gFnPc4	stavba
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
budova	budova	k1gFnSc1	budova
spořitelny	spořitelna	k1gFnSc2	spořitelna
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
sídlo	sídlo	k1gNnSc1	sídlo
Komerční	komerční	k2eAgFnSc2d1	komerční
banky	banka	k1gFnSc2	banka
<g/>
)	)	kIx)	)
postavená	postavený	k2eAgFnSc1d1	postavená
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
českých	český	k2eAgMnPc2d1	český
architektů	architekt	k1gMnPc2	architekt
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Fuchse	Fuchs	k1gMnSc4	Fuchs
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Kumpošta	Kumpošt	k1gMnSc4	Kumpošt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
byl	být	k5eAaImAgInS	být
také	také	k9	také
postaven	postavit	k5eAaPmNgInS	postavit
Jamborův	Jamborův	k2eAgInSc1d1	Jamborův
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc1	galerie
prací	práce	k1gFnPc2	práce
akademického	akademický	k2eAgMnSc2d1	akademický
malíře	malíř	k1gMnSc2	malíř
Josefa	Josef	k1gMnSc2	Josef
Jambora	Jambor	k1gMnSc2	Jambor
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
dalších	další	k2eAgFnPc2d1	další
třech	tři	k4xCgFnPc6	tři
tišnovských	tišnovský	k2eAgFnPc2d1	Tišnovská
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
také	také	k9	také
urbanismem	urbanismus	k1gInSc7	urbanismus
hlavního	hlavní	k2eAgNnSc2d1	hlavní
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
přilehlého	přilehlý	k2eAgNnSc2d1	přilehlé
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgFnSc3d1	severozápadní
části	část	k1gFnSc3	část
města	město	k1gNnSc2	město
dominuje	dominovat	k5eAaImIp3nS	dominovat
470	[number]	k4	470
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
vysoký	vysoký	k2eAgInSc4d1	vysoký
vrch	vrch	k1gInSc4	vrch
Květnice	Květnice	k1gFnSc2	Květnice
<g/>
,	,	kIx,	,
významné	významný	k2eAgNnSc1d1	významné
naleziště	naleziště	k1gNnSc1	naleziště
vzácných	vzácný	k2eAgInPc2d1	vzácný
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
útrobách	útroba	k1gFnPc6	útroba
Květnice	Květnice	k1gFnSc2	Květnice
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
jeskyní	jeskyně	k1gFnPc2	jeskyně
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
místním	místní	k2eAgNnSc6d1	místní
čestném	čestný	k2eAgNnSc6d1	čestné
občanovi	občan	k1gMnSc3	občan
Aloisi	Alois	k1gMnSc3	Alois
Královi	Král	k1gMnSc3	Král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květnice	Květnice	k1gFnSc1	Květnice
je	být	k5eAaImIp3nS	být
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
přírodní	přírodní	k2eAgFnSc7d1	přírodní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
silnice	silnice	k1gFnSc2	silnice
od	od	k7c2	od
barokní	barokní	k2eAgFnSc2d1	barokní
restaurace	restaurace	k1gFnSc2	restaurace
Na	na	k7c4	na
Humpolce	Humpolec	k1gInPc4	Humpolec
k	k	k7c3	k
Drásovu	Drásův	k2eAgNnSc3d1	Drásův
stojí	stát	k5eAaImIp3nS	stát
chráněné	chráněný	k2eAgNnSc1d1	chráněné
stromořadí	stromořadí	k1gNnSc1	stromořadí
lip	lípa	k1gFnPc2	lípa
velkolistých	velkolistý	k2eAgInPc2d1	velkolistý
(	(	kIx(	(
<g/>
registrace	registrace	k1gFnSc1	registrace
jako	jako	k8xC	jako
Lipové	lipový	k2eAgNnSc1d1	lipové
stromořadí	stromořadí	k1gNnSc1	stromořadí
v	v	k7c6	v
Tišnově	Tišnov	k1gInSc6	Tišnov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
kopce	kopec	k1gInSc2	kopec
Klucanina	Klucanina	k1gFnSc1	Klucanina
nad	nad	k7c7	nad
východní	východní	k2eAgFnSc7d1	východní
částí	část	k1gFnSc7	část
Tišnova	Tišnov	k1gInSc2	Tišnov
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nP	tyčit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
obnovená	obnovený	k2eAgFnSc1d1	obnovená
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
sokolovny	sokolovna	k1gFnSc2	sokolovna
a	a	k8xC	a
gymnázia	gymnázium	k1gNnSc2	gymnázium
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
koná	konat	k5eAaImIp3nS	konat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
expozice	expozice	k1gFnSc1	expozice
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
letního	letní	k2eAgNnSc2d1	letní
kina	kino	k1gNnSc2	kino
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
koná	konat	k5eAaImIp3nS	konat
charitativní	charitativní	k2eAgInSc4d1	charitativní
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Hudbou	hudba	k1gFnSc7	hudba
pro	pro	k7c4	pro
UNICEF	UNICEF	kA	UNICEF
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
organizují	organizovat	k5eAaBmIp3nP	organizovat
občanská	občanský	k2eAgNnPc1d1	občanské
sdružení	sdružení	k1gNnPc1	sdružení
Pro	pro	k7c4	pro
Tišnov	Tišnov	k1gInSc4	Tišnov
a	a	k8xC	a
SK	Sk	kA	Sk
HC	HC	kA	HC
Tišnov	Tišnov	k1gInSc1	Tišnov
Moldava	Moldava	k1gFnSc1	Moldava
nad	nad	k7c7	nad
Bodvou	Bodva	k1gFnSc7	Bodva
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc4	Slovensko
Sereď	Sereď	k1gFnSc2	Sereď
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Sulejów	Sulejów	k1gFnSc2	Sulejów
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
