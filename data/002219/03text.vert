<s>
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
a	a	k8xC	a
s	s	k7c7	s
necelými	celý	k2eNgNnPc7d1	necelé
2,9	[number]	k4	2,9
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
aglomerace	aglomerace	k1gFnSc1	aglomerace
pak	pak	k6eAd1	pak
čítá	čítat	k5eAaImIp3nS	čítat
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
203	[number]	k4	203
km2	km2	k4	km2
a	a	k8xC	a
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
středovýchodě	středovýchoda	k1gFnSc6	středovýchoda
státu	stát	k1gInSc2	stát
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Río	Río	k1gFnSc2	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
města	město	k1gNnSc2	město
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
osada	osada	k1gFnSc1	osada
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zde	zde	k6eAd1	zde
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1536	[number]	k4	1536
a	a	k8xC	a
nesla	nést	k5eAaImAgFnS	nést
jméno	jméno	k1gNnSc4	jméno
Puerto	Puerta	k1gFnSc5	Puerta
de	de	k?	de
Nuestra	Nuestr	k1gMnSc2	Nuestr
Señ	Señ	k1gMnSc2	Señ
Santa	Santa	k1gMnSc1	Santa
María	María	k1gMnSc1	María
del	del	k?	del
Buen	Buen	k1gMnSc1	Buen
Aire	Air	k1gMnSc2	Air
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Přístav	přístav	k1gInSc1	přístav
naší	náš	k3xOp1gFnSc2	náš
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
dobrého	dobrý	k2eAgInSc2d1	dobrý
větru	vítr	k1gInSc2	vítr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Španělští	španělský	k2eAgMnPc1d1	španělský
osadníci	osadník	k1gMnPc1	osadník
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
domorodými	domorodý	k2eAgMnPc7d1	domorodý
indiány	indián	k1gMnPc7	indián
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
byli	být	k5eAaImAgMnP	být
přinuceni	přinutit	k5eAaPmNgMnP	přinutit
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1541	[number]	k4	1541
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
osadu	osada	k1gFnSc4	osada
vypálili	vypálit	k5eAaPmAgMnP	vypálit
a	a	k8xC	a
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
získali	získat	k5eAaPmAgMnP	získat
opět	opět	k6eAd1	opět
indiáni	indián	k1gMnPc1	indián
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1580	[number]	k4	1580
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
vrátili	vrátit	k5eAaPmAgMnP	vrátit
a	a	k8xC	a
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vypálené	vypálený	k2eAgFnSc2d1	vypálená
osady	osada	k1gFnSc2	osada
nechali	nechat	k5eAaPmAgMnP	nechat
postavit	postavit	k5eAaPmF	postavit
město	město	k1gNnSc1	město
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
la	la	k1gNnSc1	la
Trinidad	Trinidad	k1gInSc1	Trinidad
<g/>
,	,	kIx,	,
y	y	k?	y
Puerto	Puerta	k1gFnSc5	Puerta
de	de	k?	de
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
del	del	k?	del
Buen	Buen	k1gInSc1	Buen
Aire	Aire	k1gFnSc1	Aire
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Město	město	k1gNnSc1	město
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
a	a	k8xC	a
přístav	přístav	k1gInSc1	přístav
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
dobrého	dobrý	k2eAgInSc2d1	dobrý
větru	vítr	k1gInSc2	vítr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
porovnání	porovnání	k1gNnSc6	porovnání
z	z	k7c2	z
dnešním	dnešní	k2eAgInSc6d1	dnešní
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
toto	tento	k3xDgNnSc4	tento
městečko	městečko	k1gNnSc1	městečko
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
uboze	uboze	k6eAd1	uboze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1606	[number]	k4	1606
mělo	mít	k5eAaImAgNnS	mít
všehovšudy	všehovšudy	k6eAd1	všehovšudy
pouhých	pouhý	k2eAgMnPc2d1	pouhý
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nenacházely	nacházet	k5eNaImAgFnP	nacházet
žádné	žádný	k3yNgFnPc1	žádný
vidiny	vidina	k1gFnPc1	vidina
zisku	zisk	k1gInSc2	zisk
pro	pro	k7c4	pro
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
osada	osada	k1gFnSc1	osada
ležela	ležet	k5eAaImAgFnS	ležet
mimo	mimo	k7c4	mimo
zájem	zájem	k1gInSc4	zájem
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgFnSc4d2	významnější
funkci	funkce	k1gFnSc4	funkce
město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
španělského	španělský	k2eAgNnSc2d1	španělské
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Río	Río	k1gMnSc2	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
mělo	mít	k5eAaImAgNnS	mít
status	status	k1gInSc4	status
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Spojených	spojený	k2eAgFnPc2d1	spojená
provincií	provincie	k1gFnPc2	provincie
La	la	k1gNnSc2	la
Platy	plat	k1gInPc4	plat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Argentinské	argentinský	k2eAgFnSc2d1	Argentinská
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
se	se	k3xPyFc4	se
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nově	nově	k6eAd1	nově
vzniknuvší	vzniknuvší	k2eAgFnSc2d1	vzniknuvší
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
dána	dát	k5eAaPmNgFnS	dát
první	první	k4xOgFnSc1	první
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1994	[number]	k4	1994
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
nejhoršího	zlý	k2eAgInSc2d3	Nejhorší
teroristického	teroristický	k2eAgInSc2d1	teroristický
činu	čin	k1gInSc2	čin
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
země	zem	k1gFnSc2	zem
-	-	kIx~	-
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
židovské	židovský	k2eAgNnSc4d1	Židovské
centrum	centrum	k1gNnSc4	centrum
Asociación	Asociación	k1gMnSc1	Asociación
Mutual	Mutual	k1gMnSc1	Mutual
Israelita	Israelita	k1gFnSc1	Israelita
Argentina	Argentina	k1gFnSc1	Argentina
(	(	kIx(	(
<g/>
AMIA	AMIA	kA	AMIA
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
85	[number]	k4	85
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
argentinský	argentinský	k2eAgInSc1d1	argentinský
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
při	při	k7c6	při
společném	společný	k2eAgNnSc6d1	společné
ústí	ústí	k1gNnSc6	ústí
řek	řeka	k1gFnPc2	řeka
Uruguay	Uruguay	k1gFnSc1	Uruguay
a	a	k8xC	a
Paraná	Paraná	k1gFnSc1	Paraná
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
34	[number]	k4	34
<g/>
.	.	kIx.	.
stupni	stupeň	k1gInPc7	stupeň
jižní	jižní	k2eAgFnSc2d1	jižní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
jako	jako	k9	jako
Sydney	Sydney	k1gNnSc1	Sydney
nebo	nebo	k8xC	nebo
Kapské	kapský	k2eAgNnSc1d1	Kapské
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
25	[number]	k4	25
m.	m.	k?	m.
Převládá	převládat	k5eAaImIp3nS	převládat
zde	zde	k6eAd1	zde
mírně	mírně	k6eAd1	mírně
vlhké	vlhký	k2eAgNnSc4d1	vlhké
subtropické	subtropický	k2eAgNnSc4d1	subtropické
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
horká	horký	k2eAgFnSc1d1	horká
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
března	březen	k1gInSc2	březen
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
něco	něco	k6eAd1	něco
pod	pod	k7c4	pod
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
vlhké	vlhký	k2eAgFnPc1d1	vlhká
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
neklesá	klesat	k5eNaImIp3nS	klesat
nikterak	nikterak	k6eAd1	nikterak
nízko	nízko	k6eAd1	nízko
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
můžou	můžou	k?	můžou
teploty	teplota	k1gFnSc2	teplota
klesat	klesat	k5eAaImF	klesat
pod	pod	k7c7	pod
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
005	[number]	k4	005
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Převládajícím	převládající	k2eAgInSc7d1	převládající
typem	typ	k1gInSc7	typ
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
pampa	pampa	k1gFnSc1	pampa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
místa	místo	k1gNnPc4	místo
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nS	patřit
Plaza	plaz	k1gMnSc2	plaz
de	de	k?	de
Mayo	Mayo	k1gNnSc1	Mayo
s	s	k7c7	s
budovou	budova	k1gFnSc7	budova
Casa	Casa	k1gFnSc1	Casa
Rosada	Rosada	k1gFnSc1	Rosada
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
např.	např.	kA	např.
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
katedrála	katedrála	k1gFnSc1	katedrála
a	a	k8xC	a
budovy	budova	k1gFnSc2	budova
zvané	zvaný	k2eAgNnSc4d1	zvané
Cabildo	Cabildo	k1gNnSc4	Cabildo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Plaza	plaz	k1gMnSc2	plaz
de	de	k?	de
Mayo	Mayo	k1gMnSc1	Mayo
se	se	k3xPyFc4	se
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1,5	[number]	k4	1,5
km	km	kA	km
táhne	táhnout	k5eAaImIp3nS	táhnout
třída	třída	k1gFnSc1	třída
Avenida	Avenida	k1gFnSc1	Avenida
de	de	k?	de
Mayo	Mayo	k6eAd1	Mayo
až	až	k9	až
k	k	k7c3	k
náměstí	náměstí	k1gNnSc3	náměstí
Plaza	plaz	k1gMnSc2	plaz
del	del	k?	del
Congreso	Congresa	k1gFnSc5	Congresa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
budova	budova	k1gFnSc1	budova
Congreso	Congresa	k1gFnSc5	Congresa
de	de	k?	de
la	la	k1gNnPc2	la
Nación	Nación	k1gInSc1	Nación
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
a	a	k8xC	a
pomník	pomník	k1gInSc4	pomník
Dvou	dva	k4xCgInPc2	dva
kongresů	kongres	k1gInPc2	kongres
(	(	kIx(	(
<g/>
Monumento	Monumento	k1gNnSc1	Monumento
a	a	k8xC	a
los	los	k1gInSc1	los
dos	dos	k?	dos
Congresos	Congresos	k1gInSc1	Congresos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
zajímavým	zajímavý	k2eAgNnSc7d1	zajímavé
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
hřbitov	hřbitov	k1gInSc1	hřbitov
Cementario	Cementario	k6eAd1	Cementario
de	de	k?	de
la	la	k1gNnSc1	la
Recoleta	Recoleta	k1gFnSc1	Recoleta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
svými	svůj	k3xOyFgInPc7	svůj
hroby	hrob	k1gInPc7	hrob
miniaturu	miniatura	k1gFnSc4	miniatura
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Spousta	spousta	k1gFnSc1	spousta
kulturních	kulturní	k2eAgNnPc2d1	kulturní
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Palermo	Palermo	k1gNnSc4	Palermo
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
zde	zde	k6eAd1	zde
botanická	botanický	k2eAgFnSc1d1	botanická
a	a	k8xC	a
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
planetárium	planetárium	k1gNnSc1	planetárium
<g/>
,	,	kIx,	,
dostihová	dostihový	k2eAgFnSc1d1	dostihová
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
hřiště	hřiště	k1gNnSc1	hřiště
na	na	k7c4	na
pólo	pólo	k1gNnSc4	pólo
a	a	k8xC	a
růžový	růžový	k2eAgInSc4d1	růžový
sad	sad	k1gInSc4	sad
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
symbolem	symbol	k1gInSc7	symbol
metropole	metropol	k1gFnSc2	metropol
je	být	k5eAaImIp3nS	být
obelisk	obelisk	k1gInSc4	obelisk
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Avenida	Avenida	k1gFnSc1	Avenida
9	[number]	k4	9
de	de	k?	de
Julio	Julio	k1gMnSc1	Julio
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
turistů	turist	k1gMnPc2	turist
jsou	být	k5eAaImIp3nP	být
čtvrtě	čtvrt	k1gFnPc1	čtvrt
San	San	k1gFnSc2	San
Telmo	Telma	k1gFnSc5	Telma
a	a	k8xC	a
La	la	k1gNnPc4	la
Boca	Boc	k2eAgNnPc4d1	Boc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
sídlí	sídlet	k5eAaImIp3nS	sídlet
nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
argentinský	argentinský	k2eAgInSc1d1	argentinský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
CA	ca	kA	ca
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniors	k1gInSc1	Juniors
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
Seattlu	Seattl	k1gInSc2	Seattl
<g/>
:	:	kIx,	:
</s>
