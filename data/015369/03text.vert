<s>
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
</s>
<s>
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
Jaguár	jaguár	k1gMnSc1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
šelmy	šelma	k1gFnPc1
(	(	kIx(
<g/>
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
kočkovití	kočkovití	k1gMnPc1
(	(	kIx(
<g/>
Felidae	Felidae	k1gInSc1
<g/>
)	)	kIx)
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
(	(	kIx(
<g/>
Pantherinae	Pantherinae	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Pocock	Pocock	k1gMnSc1
<g/>
,	,	kIx,
1917	#num#	k4
Rody	rod	k1gInPc1
</s>
<s>
Neofelis	Neofelis	k1gFnSc1
</s>
<s>
Panthera	Panthera	k1gFnSc1
</s>
<s>
Sesterská	sesterský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
(	(	kIx(
<g/>
Felinae	Felinae	k1gInSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
(	(	kIx(
<g/>
Pantherinae	Pantherina	k1gFnPc1
<g/>
)	)	kIx)
vytvářejí	vytvářet	k5eAaImIp3nP
podčeleď	podčeleď	k1gFnSc4
kočkovitých	kočkovitý	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
rody	rod	k1gInPc4
koček	kočka	k1gFnPc2
<g/>
:	:	kIx,
Panthera	Panthera	k1gFnSc1
a	a	k8xC
Neofelis	Neofelis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
rod	rod	k1gInSc4
Panthera	Panthero	k1gNnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
patří	patřit	k5eAaImIp3nS
lev	lev	k1gMnSc1
<g/>
,	,	kIx,
tygr	tygr	k1gMnSc1
<g/>
,	,	kIx,
levhart	levhart	k1gMnSc1
<g/>
,	,	kIx,
jaguár	jaguár	k1gMnSc1
a	a	k8xC
irbis	irbis	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
široce	široko	k6eAd1
známý	známý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
Africe	Afrika	k1gFnSc6
i	i	k8xC
v	v	k7c6
Americe	Amerika	k1gFnSc6
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
žily	žít	k5eAaImAgFnP
i	i	k9
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
)	)	kIx)
<g/>
;	;	kIx,
obývají	obývat	k5eAaImIp3nP
savany	savana	k1gFnPc4
<g/>
,	,	kIx,
pralesy	prales	k1gInPc4
i	i	k8xC
horské	horský	k2eAgInPc4d1
štíty	štít	k1gInPc4
Himálaje	Himálaj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
jejich	jejich	k3xOp3gInSc2
největšího	veliký	k2eAgInSc2d3
výskytu	výskyt	k1gInSc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
také	také	k9
místem	místo	k1gNnSc7
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
hustotou	hustota	k1gFnSc7
lidského	lidský	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
jsou	být	k5eAaImIp3nP
pronásledovány	pronásledován	k2eAgFnPc1d1
kvůli	kvůli	k7c3
ochraně	ochrana	k1gFnSc3
dobytka	dobytek	k1gMnSc2
i	i	k8xC
samotných	samotný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
žijících	žijící	k2eAgMnPc2d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
-	-	kIx~
někteří	některý	k3yIgMnPc1
jedinci	jedinec	k1gMnPc1
lvů	lev	k1gInPc2
<g/>
,	,	kIx,
tygrů	tygr	k1gMnPc2
a	a	k8xC
levhartů	levhart	k1gMnPc2
se	se	k3xPyFc4
za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
mohou	moct	k5eAaImIp3nP
stát	stát	k5eAaImF,k5eAaPmF
lidožrouty	lidožrout	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
jsou	být	k5eAaImIp3nP
ale	ale	k9
plaché	plachý	k2eAgFnPc1d1
a	a	k8xC
lidem	člověk	k1gMnPc3
se	se	k3xPyFc4
vyhýbají	vyhýbat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
mají	mít	k5eAaImIp3nP
jen	jen	k9
zčásti	zčásti	k6eAd1
zkostnatělou	zkostnatělý	k2eAgFnSc4d1
jazylku	jazylka	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jim	on	k3xPp3gMnPc3
umožňuje	umožňovat	k5eAaImIp3nS
řvát	řvát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
hlavní	hlavní	k2eAgInSc1d1
rozlišovací	rozlišovací	k2eAgInSc1d1
znak	znak	k1gInSc1
od	od	k7c2
malých	malý	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
mají	mít	k5eAaImIp3nP
rozdílnou	rozdílný	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
čenichu	čenich	k1gInSc2
a	a	k8xC
obecně	obecně	k6eAd1
větší	veliký	k2eAgFnSc4d2
velikost	velikost	k1gFnSc4
(	(	kIx(
<g/>
nicméně	nicméně	k8xC
nejde	jít	k5eNaImIp3nS
o	o	k7c4
jednoznačné	jednoznačný	k2eAgNnSc4d1
rozlišovací	rozlišovací	k2eAgNnSc4d1
pravidlo	pravidlo	k1gNnSc4
a	a	k8xC
například	například	k6eAd1
malá	malý	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
puma	puma	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
podstatně	podstatně	k6eAd1
větší	veliký	k2eAgMnSc1d2
než	než	k8xS
levhart	levhart	k1gMnSc1
obláčkový	obláčkový	k2eAgMnSc1d1
či	či	k8xC
Diardův	Diardův	k2eAgMnSc1d1
a	a	k8xC
mnohdy	mnohdy	k6eAd1
i	i	k9
než	než	k8xS
irbis	irbis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
<g/>
,	,	kIx,
kromě	kromě	k7c2
dospělého	dospělý	k2eAgMnSc2d1
lva	lev	k1gMnSc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
srst	srst	k1gFnSc4
s	s	k7c7
kresbou	kresba	k1gFnSc7
–	–	k?
nejčastěji	často	k6eAd3
skvrnami	skvrna	k1gFnPc7
nebo	nebo	k8xC
rozetami	rozeta	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
tygra	tygr	k1gMnSc2
s	s	k7c7
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atraktivní	atraktivní	k2eAgFnSc7d1
zbarvení	zbarvení	k1gNnSc4
srsti	srst	k1gFnSc2
je	být	k5eAaImIp3nS
příčinou	příčina	k1gFnSc7
dalšího	další	k2eAgInSc2d1
problému	problém	k1gInSc2
–	–	k?
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
jsou	být	k5eAaImIp3nP
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
ilegálně	ilegálně	k6eAd1
<g/>
)	)	kIx)
loveny	lovit	k5eAaImNgFnP
kvůli	kvůli	k7c3
kožešině	kožešina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
majestátní	majestátní	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
a	a	k8xC
síla	síla	k1gFnSc1
je	být	k5eAaImIp3nS
učinila	učinit	k5eAaPmAgFnS,k5eAaImAgFnS
symbolem	symbol	k1gInSc7
v	v	k7c6
mnoha	mnoho	k4c6
kulturách	kultura	k1gFnPc6
a	a	k8xC
části	část	k1gFnSc6
jejich	jejich	k3xOp3gNnPc2
těl	tělo	k1gNnPc2
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
talismany	talisman	k1gInPc4
nebo	nebo	k8xC
mají	mít	k5eAaImIp3nP
mít	mít	k5eAaImF
léčivou	léčivý	k2eAgFnSc4d1
či	či	k8xC
jinou	jiný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
problém	problém	k1gInSc4
hlavně	hlavně	k9
v	v	k7c6
případě	případ	k1gInSc6
tygra	tygr	k1gMnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
kosti	kost	k1gFnPc1
jsou	být	k5eAaImIp3nP
ceněnou	ceněný	k2eAgFnSc7d1
surovinou	surovina	k1gFnSc7
v	v	k7c6
tradiční	tradiční	k2eAgFnSc6d1
čínské	čínský	k2eAgFnSc6d1
medicíně	medicína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tygr	tygr	k1gMnSc1
a	a	k8xC
irbis	irbis	k1gFnSc1
jsou	být	k5eAaImIp3nP
zapsáni	zapsán	k2eAgMnPc1d1
na	na	k7c6
Červeném	červený	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
jako	jako	k9
druhy	druh	k1gInPc1
ohrožené	ohrožený	k2eAgInPc1d1
vyhynutím	vyhynutí	k1gNnSc7
<g/>
,	,	kIx,
lev	lev	k1gInSc1
<g/>
,	,	kIx,
levhart	levhart	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
<g/>
,	,	kIx,
levhart	levhart	k1gMnSc1
obláčkový	obláčkový	k2eAgMnSc1d1
a	a	k8xC
levhart	levhart	k1gMnSc1
Diardův	Diardův	k2eAgMnSc1d1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
klasifikovány	klasifikovat	k5eAaImNgInP
jako	jako	k8xS,k8xC
druhy	druh	k1gInPc1
zranitelné	zranitelný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Relativně	relativně	k6eAd1
nejhojnějším	hojný	k2eAgMnSc7d3
druhem	druh	k1gMnSc7
je	být	k5eAaImIp3nS
jaguár	jaguár	k1gMnSc1
americký	americký	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
hodnocen	hodnotit	k5eAaImNgMnS
jako	jako	k9
téměř	téměř	k6eAd1
ohrožený	ohrožený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Početní	početní	k2eAgInPc4d1
stavy	stav	k1gInPc4
všech	všecek	k3xTgFnPc2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
klesají	klesat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
</s>
<s>
Rod	rod	k1gInSc1
Neofelis	Neofelis	k1gFnSc2
</s>
<s>
levhart	levhart	k1gMnSc1
obláčkový	obláčkový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Neofelis	Neofelis	k1gInSc1
nebulosa	nebulosa	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
lesích	les	k1gInPc6
jižní	jižní	k2eAgFnSc2d1
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc2d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
</s>
<s>
levhart	levhart	k1gMnSc1
Diardův	Diardův	k2eAgMnSc1d1
(	(	kIx(
<g/>
Neofelis	Neofelis	k1gInSc4
diardi	diard	k1gMnPc1
<g/>
)	)	kIx)
v	v	k7c6
lesích	les	k1gInPc6
tropické	tropický	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
(	(	kIx(
<g/>
ostrovy	ostrov	k1gInPc1
Sumatra	Sumatra	k1gFnSc1
a	a	k8xC
Borneo	Borneo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Rod	rod	k1gInSc1
Panthera	Panthero	k1gNnSc2
</s>
<s>
lev	lev	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
leo	leo	k?
<g/>
)	)	kIx)
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
</s>
<s>
levhart	levhart	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
pardus	pardus	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
</s>
<s>
irbis	irbis	k1gInSc4
horský	horský	k2eAgInSc4d1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Asii	Asie	k1gFnSc6
</s>
<s>
tygr	tygr	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
tigris	tigris	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
Asii	Asie	k1gFnSc6
</s>
<s>
jaguár	jaguár	k1gMnSc1
americký	americký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Panthera	Panther	k1gMnSc4
onca	oncus	k1gMnSc4
<g/>
)	)	kIx)
ve	v	k7c6
Střední	střední	k2eAgFnSc6d1
a	a	k8xC
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
</s>
<s>
Do	do	k7c2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
byla	být	k5eAaImAgFnS
dříve	dříve	k6eAd2
řazena	řadit	k5eAaImNgFnS
také	také	k9
kočka	kočka	k1gFnSc1
mramorovaná	mramorovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Pardofelis	Pardofelis	k1gFnSc1
marmorata	marmorata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
ale	ale	k9
ukázalo	ukázat	k5eAaPmAgNnS
neoprávněné	oprávněný	k2eNgNnSc1d1
<g/>
,	,	kIx,
kočka	kočka	k1gFnSc1
mramorovaná	mramorovaný	k2eAgFnSc1d1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
ostatním	ostatní	k2eAgFnPc3d1
kočkám	kočka	k1gFnPc3
do	do	k7c2
podčeledi	podčeleď	k1gFnSc2
malé	malý	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
(	(	kIx(
<g/>
Felinae	Felinae	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
velkými	velký	k2eAgFnPc7d1
a	a	k8xC
malými	malý	k2eAgFnPc7d1
kočkami	kočka	k1gFnPc7
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
velké	velký	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Pantherinae	Pantherinae	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kočkovití	kočkovití	k1gMnPc1
Malé	Malé	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
(	(	kIx(
<g/>
Felinae	Felinae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
divoká	divoký	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
šedá	šedý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
bažinná	bažinný	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
pouštní	pouštní	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
černonohá	černonohý	k2eAgFnSc1d1
•	•	k?
manul	manul	k1gMnSc1
•	•	k?
kočka	kočka	k1gFnSc1
bornejská	bornejský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
Temminckova	Temminckův	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
mramorovaná	mramorovaný	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
bengálská	bengálský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
plochočelá	plochočelý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
cejlonská	cejlonský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
rybářská	rybářský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
pampová	pampový	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
slaništní	slaništní	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
tmavá	tmavý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
horská	horský	k2eAgFnSc1d1
•	•	k?
ocelot	ocelot	k1gMnSc1
velký	velký	k2eAgMnSc1d1
•	•	k?
ocelot	ocelot	k1gMnSc1
stromový	stromový	k2eAgMnSc1d1
•	•	k?
margay	margay	k1gInPc4
•	•	k?
rys	rys	k1gInSc1
kanadský	kanadský	k2eAgInSc1d1
•	•	k?
rys	rys	k1gInSc1
ostrovid	ostrovid	k1gMnSc1
•	•	k?
rys	rys	k1gMnSc1
iberský	iberský	k2eAgMnSc1d1
•	•	k?
rys	rys	k1gMnSc1
červený	červený	k2eAgMnSc1d1
•	•	k?
karakal	karakal	k1gMnSc1
•	•	k?
serval	serval	k1gMnSc1
•	•	k?
kočka	kočka	k1gFnSc1
zlatá	zlatý	k2eAgFnSc1d1
•	•	k?
jaguarundi	jaguarundit	k5eAaPmRp2nS
•	•	k?
puma	puma	k1gFnSc1
•	•	k?
gepard	gepard	k1gMnSc1
Velké	velká	k1gFnSc2
kočky	kočka	k1gFnSc2
(	(	kIx(
<g/>
Pantherinae	Pantherinae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
levhart	levhart	k1gMnSc1
obláčkový	obláčkový	k2eAgMnSc1d1
•	•	k?
levhart	levhart	k1gMnSc1
Diardův	Diardův	k2eAgMnSc1d1
•	•	k?
irbis	irbis	k1gInSc1
•	•	k?
lev	lev	k1gMnSc1
•	•	k?
tygr	tygr	k1gMnSc1
•	•	k?
levhart	levhart	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
•	•	k?
jaguár	jaguár	k1gMnSc1
†	†	k?
<g/>
Machairodontinae	Machairodontinae	k1gInSc1
</s>
<s>
Adelphailurus	Adelphailurus	k1gMnSc1
•	•	k?
Amphimachairodus	Amphimachairodus	k1gMnSc1
•	•	k?
Dinofelis	Dinofelis	k1gFnSc2
•	•	k?
Hemimachairodus	Hemimachairodus	k1gMnSc1
•	•	k?
Homotherium	Homotherium	k1gNnSc4
•	•	k?
Lokotunjailurus	Lokotunjailurus	k1gMnSc1
•	•	k?
Machairodus	Machairodus	k1gMnSc1
•	•	k?
Megantereon	Megantereon	k1gMnSc1
•	•	k?
Metailurus	Metailurus	k1gMnSc1
•	•	k?
Miomachairodus	Miomachairodus	k1gMnSc1
•	•	k?
Nimravides	Nimravides	k1gMnSc1
•	•	k?
Paramachairodus	Paramachairodus	k1gMnSc1
•	•	k?
Pontosmilus	Pontosmilus	k1gMnSc1
•	•	k?
Rhizosmilodon	Rhizosmilodon	k1gMnSc1
•	•	k?
Smilodon	Smilodon	k1gMnSc1
•	•	k?
Stenailurus	Stenailurus	k1gMnSc1
•	•	k?
Xenosmilus	Xenosmilus	k1gMnSc1
hodsonae	hodsona	k1gFnSc2
†	†	k?
<g/>
Proailurinae	Proailurina	k1gFnSc2
</s>
<s>
Proailurus	Proailurus	k1gInSc1
•	•	k?
Stenogale	Stenogal	k1gMnSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4158284-6	4158284-6	k4
</s>
