<s>
Miroslav	Miroslav	k1gMnSc1
Kučera	Kučera	k1gMnSc1
(	(	kIx(
<g/>
kriminalista	kriminalista	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Kučera	Kučera	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1936	#num#	k4
(	(	kIx(
<g/>
85	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Praha	Praha	k1gFnSc1
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Povolání	povolání	k1gNnPc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Kučera	Kučera	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1936	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
kriminalista	kriminalista	k1gMnSc1
<g/>
,	,	kIx,
spolupracovník	spolupracovník	k1gMnSc1
Československé	československý	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
autor	autor	k1gMnSc1
literatury	literatura	k1gFnSc2
non-fiction	non-fiction	k1gInSc1
<g/>
,	,	kIx,
detektivek	detektivka	k1gFnPc2
ap.	ap.	kA
užívá	užívat	k5eAaImIp3nS
též	též	k9
pseudonym	pseudonym	k1gInSc1
J.	J.	kA
M.	M.	kA
Kusta	Kusta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Studia	studio	k1gNnSc2
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
1950	#num#	k4
až	až	k9
1957	#num#	k4
studoval	studovat	k5eAaImAgInS
na	na	k7c6
grafických	grafický	k2eAgFnPc6d1
školách	škola	k1gFnPc6
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Liberci	Liberec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1970	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
ústředně	ústředna	k1gFnSc6
Kriminální	kriminální	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
je	být	k5eAaImIp3nS
ve	v	k7c6
starobním	starobní	k2eAgInSc6d1
důchodu	důchod	k1gInSc6
<g/>
,	,	kIx,
bydlí	bydlet	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
literárních	literární	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
KALF	KALF	kA
a	a	k8xC
AEIP	AEIP	kA
<g/>
.	.	kIx.
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
faktu	fakt	k1gInSc2
</s>
<s>
Byl	být	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
hlasatelem	hlasatel	k1gMnSc7
špatných	špatný	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Erika	Erika	k1gFnSc1
1993	#num#	k4
</s>
<s>
Muži	muž	k1gMnPc1
proti	proti	k7c3
zločinu	zločin	k1gInSc3
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1993	#num#	k4
</s>
<s>
Zločiny	zločin	k1gInPc1
z	z	k7c2
obrazovky	obrazovka	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1995	#num#	k4
</s>
<s>
Vražda	vražda	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
mne	já	k3xPp1nSc4
případ	případ	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1996	#num#	k4
</s>
<s>
Muži	muž	k1gMnPc1
proti	proti	k7c3
vraždě	vražda	k1gFnSc3
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1996	#num#	k4
</s>
<s>
Omyl	omyl	k1gInSc1
v	v	k7c6
Bělském	Bělský	k2eAgInSc6d1
lese	les	k1gInSc6
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1996	#num#	k4
</s>
<s>
Případ	případ	k1gInSc1
domýšlivého	domýšlivý	k2eAgMnSc2d1
policisty	policista	k1gMnSc2
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1997	#num#	k4
</s>
<s>
Ztracené	ztracený	k2eAgFnPc1d1
stopy	stopa	k1gFnPc1
I.	I.	kA
<g/>
-II	-II	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
</s>
<s>
Podnikání	podnikání	k1gNnSc1
po	po	k7c6
česku	česk	k1gInSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
SAGA	SAGA	kA
1998	#num#	k4
</s>
<s>
Vražda	vražda	k1gFnSc1
bílého	bílý	k1gMnSc2
koně	kůň	k1gMnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
NAVA	NAVA	k?
2000	#num#	k4
</s>
<s>
Amnestie	amnestie	k1gFnPc1
a	a	k8xC
milosti	milost	k1gFnPc1
očima	oko	k1gNnPc7
hradu	hrad	k1gInSc2
a	a	k8xC
podhradí	podhradí	k1gNnSc2
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Erika	Erika	k1gFnSc1
2002	#num#	k4
</s>
<s>
Výhry	výhra	k1gFnPc1
nad	nad	k7c7
zločinem	zločin	k1gInSc7
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Rodiče	rodič	k1gMnPc1
2002	#num#	k4
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
</s>
<s>
Otrokyně	otrokyně	k1gFnSc1
lásky	láska	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Magnet	magnet	k1gInSc1
1993	#num#	k4
</s>
<s>
Zvěrokruh	zvěrokruh	k1gInSc1
sexu	sex	k1gInSc2
a	a	k8xC
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1997	#num#	k4
</s>
<s>
Vzkaz	vzkaz	k1gInSc1
ze	z	k7c2
záhrobí	záhrobí	k1gNnSc2
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1997	#num#	k4
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1
drak	drak	k1gMnSc1
pro	pro	k7c4
štěstí	štěstí	k1gNnSc4
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Víkend	víkend	k1gInSc1
1998	#num#	k4
</s>
<s>
Krvavý	krvavý	k2eAgInSc1d1
mejdan	mejdan	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Nava	Nava	k?
2001	#num#	k4
</s>
<s>
Střelecké	střelecký	k2eAgFnPc1d1
orgie	orgie	k1gFnPc1
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Nava	Nava	k?
2002	#num#	k4
</s>
<s>
Smrt	smrt	k1gFnSc1
na	na	k7c6
malé	malý	k2eAgFnSc6d1
policejní	policejní	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Nava	Nava	k?
2002	#num#	k4
</s>
<s>
Vražda	vražda	k1gFnSc1
podle	podle	k7c2
Agáty	Agáta	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Nava	Nava	k?
2003	#num#	k4
</s>
<s>
Alibi	alibi	k1gNnSc1
pro	pro	k7c4
mrtvou	mrtvý	k2eAgFnSc4d1
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Nava	Nava	k?
2003	#num#	k4
</s>
<s>
Krvavé	krvavý	k2eAgNnSc1d1
tajemství	tajemství	k1gNnSc1
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1
past	past	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
Policajt	Policajt	k?
v	v	k7c6
nočním	noční	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
,	,	kIx,
2005	#num#	k4
</s>
<s>
Smrt	smrt	k1gFnSc1
cudností	cudnost	k1gFnPc2
neoplývá	oplývat	k5eNaImIp3nS
<g/>
,	,	kIx,
2005	#num#	k4
</s>
<s>
Smrtonosné	smrtonosný	k2eAgFnPc4d1
náhody	náhoda	k1gFnPc4
<g/>
,	,	kIx,
2006	#num#	k4
</s>
<s>
Chlapi	chlap	k1gMnPc1
pláčou	plakat	k5eAaImIp3nP
zaťatými	zaťatý	k2eAgFnPc7d1
pěstmi	pěst	k1gFnPc7
<g/>
,	,	kIx,
2006	#num#	k4
</s>
<s>
Oprátka	oprátka	k1gFnSc1
na	na	k7c4
ně	on	k3xPp3gMnPc4
nezbyla	zbýt	k5eNaPmAgFnS
(	(	kIx(
<g/>
s	s	k7c7
J.	J.	kA
Stachem	Stach	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Police	police	k1gFnSc1
History	Histor	k1gInPc4
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1
mordpatrta	mordpatrta	k1gFnSc1
řádí	řádit	k5eAaImIp3nS
<g/>
,	,	kIx,
2005	#num#	k4
</s>
<s>
Zločin	zločin	k1gInSc1
dostal	dostat	k5eAaPmAgInS
mat	mat	k2eAgMnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Epocha	epocha	k1gFnSc1
2005	#num#	k4
</s>
<s>
Publicistická	publicistický	k2eAgFnSc1d1
a	a	k8xC
jiná	jiný	k2eAgFnSc1d1
literární	literární	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Publikační	publikační	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
v	v	k7c6
periodickém	periodický	k2eAgInSc6d1
tisku	tisk	k1gInSc6
<g/>
,	,	kIx,
např.	např.	kA
Signál	signál	k1gInSc1
<g/>
,	,	kIx,
Linka	linka	k1gFnSc1
158	#num#	k4
<g/>
,	,	kIx,
Krimi	krimi	k1gFnSc1
<g/>
,	,	kIx,
Hot	hot	k0
Line	linout	k5eAaImIp3nS
<g/>
,	,	kIx,
Ring	ring	k1gInSc1
<g/>
,	,	kIx,
Policista	policista	k1gMnSc1
aj.	aj.	kA
<g/>
,	,	kIx,
účast	účast	k1gFnSc1
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
pitavalu	pitaval	k1gInSc6
<g/>
,	,	kIx,
Zapomenuté	zapomenutý	k2eAgInPc4d1
zločiny	zločin	k1gInPc4
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
Rádio	rádio	k1gNnSc1
Rio	Rio	k1gFnPc2
aj.	aj.	kA
<g/>
,	,	kIx,
Přísně	přísně	k6eAd1
tajné	tajný	k2eAgFnPc1d1
<g/>
,	,	kIx,
Super	super	k1gInPc1
<g/>
,	,	kIx,
Story	story	k1gFnPc1
<g/>
,	,	kIx,
Špígl	špígl	k1gInSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Malý	malý	k2eAgInSc1d1
slovník	slovník	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
…	…	k?
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Pražská	pražský	k2eAgFnSc1d1
vydav	vydávit	k5eAaPmRp2nS
<g/>
.	.	kIx.
společnost	společnost	k1gFnSc1
1995	#num#	k4
</s>
<s>
Průvodce	průvodce	k1gMnSc1
po	po	k7c6
nových	nový	k2eAgNnPc6d1
jménech	jméno	k1gNnPc6
české	český	k2eAgFnSc2d1
prózy	próza	k1gFnSc2
a	a	k8xC
poezie	poezie	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Rubico	Rubico	k6eAd1
1996	#num#	k4
</s>
<s>
Slovník	slovník	k1gInSc1
KALF	KALF	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1996	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Miroslav	Miroslav	k1gMnSc1
Kučera	Kučera	k1gMnSc1
(	(	kIx(
<g/>
kriminalista	kriminalista	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Klub	klub	k1gInSc1
autorů	autor	k1gMnPc2
literatury	literatura	k1gFnSc2
faktu	fakt	k1gInSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
autorů	autor	k1gMnPc2
detektivní	detektivní	k2eAgFnSc2d1
a	a	k8xC
dobrodružné	dobrodružný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20001103505	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8028	#num#	k4
9537	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
94054164	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
36145499	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
94054164	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
