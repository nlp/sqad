<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k8xC	ale
také	také	k9	také
negativní	negativní	k2eAgInPc1d1	negativní
účinky	účinek	k1gInPc1	účinek
při	při	k7c6	při
nadměrném	nadměrný	k2eAgNnSc6d1	nadměrné
pití	pití	k1gNnSc6	pití
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
kofeinu	kofein	k1gInSc2	kofein
v	v	k7c6	v
zeleném	zelený	k2eAgInSc6d1	zelený
čaji	čaj	k1gInSc6	čaj
<g/>
.	.	kIx.	.
</s>
