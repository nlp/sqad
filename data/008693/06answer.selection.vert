<s>
Sergej	Sergej	k1gMnSc1	Sergej
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
В	В	k?	В
Р	Р	k?	Р
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1873	[number]	k4	1873
Semjonovo	Semjonův	k2eAgNnSc4d1	Semjonův
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc4	Rusko
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
pozdně	pozdně	k6eAd1	pozdně
romantického	romantický	k2eAgNnSc2d1	romantické
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
pianista	pianista	k1gMnSc1	pianista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
.	.	kIx.	.
</s>
