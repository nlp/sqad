<p>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
В	В	k?	В
Р	Р	k?	Р
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1873	[number]	k4	1873
Semjonovo	Semjonův	k2eAgNnSc4d1	Semjonův
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc4	Rusko
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
pozdně	pozdně	k6eAd1	pozdně
romantického	romantický	k2eAgNnSc2d1	romantické
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
pianista	pianista	k1gMnSc1	pianista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Neklidné	klidný	k2eNgNnSc1d1	neklidné
dětství	dětství	k1gNnSc1	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
aristokratické	aristokratický	k2eAgFnSc6d1	aristokratická
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Vasil	Vasila	k1gFnPc2	Vasila
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
bohatého	bohatý	k2eAgMnSc2d1	bohatý
generála	generál	k1gMnSc2	generál
<g/>
,	,	kIx,	,
Lubov	Lubov	k1gInSc1	Lubov
Butakovou	Butakový	k2eAgFnSc4d1	Butakový
<g/>
,	,	kIx,	,
věnem	věno	k1gNnSc7	věno
pět	pět	k4xCc4	pět
venkovských	venkovský	k2eAgInPc2d1	venkovský
statků	statek	k1gInPc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
nezřízeným	zřízený	k2eNgInSc7d1	nezřízený
životním	životní	k2eAgInSc7d1	životní
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
karbanictvím	karbanictví	k1gNnSc7	karbanictví
a	a	k8xC	a
sukničkářstvím	sukničkářství	k1gNnSc7	sukničkářství
však	však	k9	však
ničil	ničit	k5eAaImAgMnS	ničit
rodinné	rodinný	k2eAgNnSc4d1	rodinné
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vést	vést	k5eAaImF	vést
život	život	k1gInSc4	život
bohatého	bohatý	k2eAgMnSc2d1	bohatý
statkáře	statkář	k1gMnSc2	statkář
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
začátek	začátek	k1gInSc1	začátek
jeho	on	k3xPp3gInSc2	on
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byla	být	k5eAaImAgNnP	být
panství	panství	k1gNnPc1	panství
rozprodávána	rozprodáván	k2eAgNnPc1d1	rozprodáváno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
otec	otec	k1gMnSc1	otec
mohl	moct	k5eAaImAgMnS	moct
splácet	splácet	k5eAaImF	splácet
své	svůj	k3xOyFgInPc4	svůj
hráčské	hráčský	k2eAgInPc4d1	hráčský
dluhy	dluh	k1gInPc4	dluh
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
platit	platit	k5eAaImF	platit
za	za	k7c4	za
drahé	drahý	k2eAgFnPc4d1	drahá
milenky	milenka	k1gFnPc4	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
chlapci	chlapec	k1gMnSc6	chlapec
Sergejovi	Sergej	k1gMnSc3	Sergej
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zbylo	zbýt	k5eAaPmAgNnS	zbýt
rodině	rodina	k1gFnSc3	rodina
pouze	pouze	k6eAd1	pouze
jediné	jediný	k2eAgNnSc4d1	jediné
panství	panství	k1gNnSc4	panství
<g/>
,	,	kIx,	,
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
však	však	k9	však
rodina	rodina	k1gFnSc1	rodina
také	také	k9	také
přišla	přijít	k5eAaPmAgFnS	přijít
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
se	se	k3xPyFc4	se
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
do	do	k7c2	do
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tíhou	tíha	k1gFnSc7	tíha
rodinných	rodinný	k2eAgInPc2d1	rodinný
problémů	problém	k1gInPc2	problém
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
i	i	k9	i
manželství	manželství	k1gNnPc1	manželství
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
i	i	k8xC	i
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
však	však	k8xC	však
rozvod	rozvod	k1gInSc4	rozvod
nepřicházel	přicházet	k5eNaImAgMnS	přicházet
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
otec	otec	k1gMnSc1	otec
od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
přenechal	přenechat	k5eAaPmAgMnS	přenechat
manželce	manželka	k1gFnSc3	manželka
byt	byt	k1gInSc4	byt
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
tak	tak	k6eAd1	tak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
se	s	k7c7	s
šesti	šest	k4xCc7	šest
dětmi	dítě	k1gFnPc7	dítě
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
rodičů	rodič	k1gMnPc2	rodič
byla	být	k5eAaImAgFnS	být
malému	malý	k2eAgMnSc3d1	malý
chlapci	chlapec	k1gMnSc3	chlapec
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měla	mít	k5eAaImAgFnS	mít
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
návštěvách	návštěva	k1gFnPc6	návštěva
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
mu	on	k3xPp3gMnSc3	on
věnovala	věnovat	k5eAaPmAgFnS	věnovat
mnoho	mnoho	k6eAd1	mnoho
času	čas	k1gInSc2	čas
a	a	k8xC	a
povzbuzovala	povzbuzovat	k5eAaImAgFnS	povzbuzovat
jeho	jeho	k3xOp3gInSc4	jeho
hudební	hudební	k2eAgInSc4d1	hudební
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Žádala	žádat	k5eAaImAgFnS	žádat
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
přehrával	přehrávat	k5eAaImAgMnS	přehrávat
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
oba	dva	k4xCgMnPc1	dva
slyšeli	slyšet	k5eAaImAgMnP	slyšet
při	při	k7c6	při
nedělních	nedělní	k2eAgFnPc6d1	nedělní
bohoslužbách	bohoslužba	k1gFnPc6	bohoslužba
v	v	k7c6	v
pravoslavných	pravoslavný	k2eAgInPc6d1	pravoslavný
chrámech	chrám	k1gInPc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	jeho	k3xOp3gInPc4	jeho
výkony	výkon	k1gInPc4	výkon
ho	on	k3xPp3gNnSc2	on
pak	pak	k6eAd1	pak
vždy	vždy	k6eAd1	vždy
odměňovala	odměňovat	k5eAaImAgFnS	odměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
koupila	koupit	k5eAaPmAgFnS	koupit
statek	statek	k1gInSc4	statek
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Volchov	Volchov	k1gInSc4	Volchov
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Borisovo	Borisův	k2eAgNnSc1d1	Borisovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Sergej	Sergej	k1gMnSc1	Sergej
trávil	trávit	k5eAaImAgMnS	trávit
letní	letní	k2eAgFnPc4d1	letní
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
hrál	hrát	k5eAaImAgMnS	hrát
od	od	k7c2	od
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
jeho	jeho	k3xOp3gNnSc4	jeho
velké	velký	k2eAgNnSc4d1	velké
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
petrohradskou	petrohradský	k2eAgFnSc4d1	Petrohradská
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Nepatřil	patřit	k5eNaImAgMnS	patřit
mezi	mezi	k7c4	mezi
vzorné	vzorný	k2eAgMnPc4d1	vzorný
žáky	žák	k1gMnPc4	žák
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
chodil	chodit	k5eAaImAgMnS	chodit
za	za	k7c4	za
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
při	při	k7c6	při
vyučování	vyučování	k1gNnSc6	vyučování
byla	být	k5eAaImAgFnS	být
také	také	k9	také
neuspokojující	uspokojující	k2eNgFnSc1d1	neuspokojující
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
výsledcích	výsledek	k1gInPc6	výsledek
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
matce	matka	k1gFnSc3	matka
tajil	tajit	k5eAaImAgMnS	tajit
a	a	k8xC	a
falšoval	falšovat	k5eAaImAgMnS	falšovat
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgMnS	moct
oklamat	oklamat	k5eAaPmF	oklamat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jeho	jeho	k3xOp3gInPc1	jeho
klamy	klam	k1gInPc1	klam
však	však	k9	však
byly	být	k5eAaImAgInP	být
odhaleny	odhalit	k5eAaPmNgInP	odhalit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
učitelé	učitel	k1gMnPc1	učitel
jeho	jeho	k3xOp3gInPc4	jeho
teoretické	teoretický	k2eAgInPc4d1	teoretický
nedostatky	nedostatek	k1gInPc4	nedostatek
dosud	dosud	k6eAd1	dosud
přehlíželi	přehlížet	k5eAaImAgMnP	přehlížet
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
výjimečnému	výjimečný	k2eAgInSc3d1	výjimečný
talentu	talent	k1gInSc3	talent
a	a	k8xC	a
brilantní	brilantní	k2eAgFnSc3d1	brilantní
klavírní	klavírní	k2eAgFnSc3d1	klavírní
technice	technika	k1gFnSc3	technika
<g/>
,	,	kIx,	,
hrozilo	hrozit	k5eAaImAgNnS	hrozit
mu	on	k3xPp3gMnSc3	on
odebrání	odebrání	k1gNnSc4	odebrání
stipendia	stipendium	k1gNnSc2	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
Záchrana	záchrana	k1gFnSc1	záchrana
přišla	přijít	k5eAaPmAgFnS	přijít
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
matčina	matčin	k2eAgMnSc2d1	matčin
synovce	synovec	k1gMnSc2	synovec
Alexandra	Alexandr	k1gMnSc2	Alexandr
Silotiho	Siloti	k1gMnSc2	Siloti
<g/>
,	,	kIx,	,
také	také	k9	také
výborného	výborný	k2eAgMnSc2d1	výborný
klavíristy	klavírista	k1gMnSc2	klavírista
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Sergeje	Sergej	k1gMnSc4	Sergej
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
k	k	k7c3	k
jeho	jeho	k3xOp3gMnSc3	jeho
vlastnímu	vlastní	k2eAgMnSc3d1	vlastní
bývalému	bývalý	k2eAgMnSc3d1	bývalý
učiteli	učitel	k1gMnSc3	učitel
<g/>
,	,	kIx,	,
přísnému	přísný	k2eAgMnSc3d1	přísný
Nikolaji	Nikolaj	k1gMnSc3	Nikolaj
Zverevovi	Zvereva	k1gMnSc3	Zvereva
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moskevská	moskevský	k2eAgNnPc1d1	moskevské
studia	studio	k1gNnPc1	studio
a	a	k8xC	a
nejisté	jistý	k2eNgInPc1d1	nejistý
začátky	začátek	k1gInPc1	začátek
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
dvanáctiletého	dvanáctiletý	k2eAgMnSc4d1	dvanáctiletý
chlapce	chlapec	k1gMnSc4	chlapec
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
vzdálit	vzdálit	k5eAaPmF	vzdálit
od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
od	od	k7c2	od
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Obával	obávat	k5eAaImAgMnS	obávat
se	se	k3xPyFc4	se
nového	nový	k2eAgInSc2d1	nový
a	a	k8xC	a
přísného	přísný	k2eAgInSc2d1	přísný
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
Rachmaninova	Rachmaninův	k2eAgMnSc4d1	Rachmaninův
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
zprvu	zprvu	k6eAd1	zprvu
snesitelné	snesitelný	k2eAgNnSc1d1	snesitelné
<g/>
.	.	kIx.	.
</s>
<s>
Vynikající	vynikající	k2eAgMnSc1d1	vynikající
učitel	učitel	k1gMnSc1	učitel
Zverev	Zverva	k1gFnPc2	Zverva
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
nadané	nadaný	k2eAgMnPc4d1	nadaný
žáky	žák	k1gMnPc4	žák
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
podroboval	podrobovat	k5eAaImAgMnS	podrobovat
je	být	k5eAaImIp3nS	být
přísné	přísný	k2eAgFnSc3d1	přísná
disciplíně	disciplína	k1gFnSc3	disciplína
a	a	k8xC	a
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
své	svůj	k3xOyFgFnPc4	svůj
sestry	sestra	k1gFnPc4	sestra
jim	on	k3xPp3gMnPc3	on
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
celkové	celkový	k2eAgNnSc4d1	celkové
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Žáci	Žák	k1gMnPc1	Žák
však	však	k9	však
také	také	k9	také
často	často	k6eAd1	často
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
se	se	k3xPyFc4	se
setkávali	setkávat	k5eAaImAgMnP	setkávat
a	a	k8xC	a
hráli	hrát	k5eAaImAgMnP	hrát
před	před	k7c7	před
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
moskevskými	moskevský	k2eAgMnPc7d1	moskevský
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
před	před	k7c7	před
slavnými	slavný	k2eAgMnPc7d1	slavný
skladateli	skladatel	k1gMnPc7	skladatel
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Petr	Petr	k1gMnSc1	Petr
Iljič	Iljič	k1gMnSc1	Iljič
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
Rubinstein	Rubinstein	k1gMnSc1	Rubinstein
a	a	k8xC	a
Alexandr	Alexandr	k1gMnSc1	Alexandr
Konstantinovič	Konstantinovič	k1gMnSc1	Konstantinovič
Glazunov	Glazunov	k1gInSc4	Glazunov
<g/>
,	,	kIx,	,
když	když	k8xS	když
tito	tento	k3xDgMnPc1	tento
pobývali	pobývat	k5eAaImAgMnP	pobývat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
u	u	k7c2	u
Zvereva	Zverevo	k1gNnSc2	Zverevo
byla	být	k5eAaImAgFnS	být
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žáci	žák	k1gMnPc1	žák
nemohli	moct	k5eNaImAgMnP	moct
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c4	na
prázdniny	prázdniny	k1gFnPc4	prázdniny
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
léto	léto	k1gNnSc4	léto
1886	[number]	k4	1886
odjel	odjet	k5eAaPmAgInS	odjet
Zverev	Zverev	k1gFnSc4	Zverev
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
žáky	žák	k1gMnPc7	žák
na	na	k7c4	na
Krym	Krym	k1gInSc4	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Pobývali	pobývat	k5eAaImAgMnP	pobývat
u	u	k7c2	u
moskevského	moskevský	k2eAgMnSc2d1	moskevský
milionáře	milionář	k1gMnSc2	milionář
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
děti	dítě	k1gFnPc1	dítě
Zverev	Zverva	k1gFnPc2	Zverva
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Rachmaninova	Rachmaninův	k2eAgNnPc4d1	Rachmaninův
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
pobyt	pobyt	k1gInSc4	pobyt
natolik	natolik	k6eAd1	natolik
vzrušující	vzrušující	k2eAgInSc4d1	vzrušující
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
o	o	k7c4	o
první	první	k4xOgFnPc4	první
vlastní	vlastní	k2eAgFnPc4d1	vlastní
kompozice	kompozice	k1gFnPc4	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
získal	získat	k5eAaPmAgMnS	získat
Čajkovského	Čajkovský	k2eAgMnSc4d1	Čajkovský
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
přepracování	přepracování	k1gNnSc3	přepracování
jeho	jeho	k3xOp3gFnSc2	jeho
symfonie	symfonie	k1gFnSc2	symfonie
Manfred	Manfred	k1gInSc1	Manfred
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Zverevovi	Zvereva	k1gMnSc3	Zvereva
věnoval	věnovat	k5eAaImAgMnS	věnovat
Rachmaninov	Rachmaninovo	k1gNnPc2	Rachmaninovo
soubor	soubor	k1gInSc1	soubor
klavírních	klavírní	k2eAgFnPc2d1	klavírní
nokturn	nokturn	k1gInSc1	nokturn
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
žákem	žák	k1gMnSc7	žák
se	se	k3xPyFc4	se
však	však	k9	však
začaly	začít	k5eAaPmAgInP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
spory	spor	k1gInPc1	spor
a	a	k8xC	a
neshody	neshoda	k1gFnPc1	neshoda
<g/>
.	.	kIx.	.
</s>
<s>
Zverev	Zverev	k1gFnSc4	Zverev
kladl	klást	k5eAaImAgMnS	klást
důraz	důraz	k1gInSc4	důraz
spíše	spíše	k9	spíše
na	na	k7c4	na
samotnou	samotný	k2eAgFnSc4d1	samotná
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
však	však	k9	však
byl	být	k5eAaImAgMnS	být
fascinován	fascinovat	k5eAaBmNgMnS	fascinovat
hudební	hudební	k2eAgFnSc7d1	hudební
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Rachmaninovi	Rachmaninův	k2eAgMnPc1d1	Rachmaninův
šestnáct	šestnáct	k4xCc4	šestnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS	požádat
učitele	učitel	k1gMnSc4	učitel
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
místnost	místnost	k1gFnSc4	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
cvičit	cvičit	k5eAaImF	cvičit
a	a	k8xC	a
komponovat	komponovat	k5eAaImF	komponovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
ostatních	ostatní	k2eAgMnPc6d1	ostatní
žácích	žák	k1gMnPc6	žák
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
doposud	doposud	k6eAd1	doposud
musel	muset	k5eAaImAgInS	muset
místnost	místnost	k1gFnSc4	místnost
sdílet	sdílet	k5eAaImF	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žádost	žádost	k1gFnSc1	žádost
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
vyslyšena	vyslyšen	k2eAgFnSc1d1	vyslyšena
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc4	její
podání	podání	k1gNnSc4	podání
učiteli	učitel	k1gMnSc3	učitel
přišlo	přijít	k5eAaPmAgNnS	přijít
drzé	drzý	k2eAgNnSc1d1	drzé
a	a	k8xC	a
nevkusné	vkusný	k2eNgNnSc1d1	nevkusné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
neřešitelného	řešitelný	k2eNgInSc2d1	neřešitelný
sporu	spor	k1gInSc2	spor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
Zverev	Zverev	k1gFnSc4	Zverev
odmítal	odmítat	k5eAaImAgMnS	odmítat
i	i	k9	i
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
nedaleko	daleko	k6eNd1	daleko
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
,	,	kIx,	,
rodině	rodina	k1gFnSc3	rodina
Satinových	Satinová	k1gFnPc2	Satinová
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
příjemní	příjemný	k2eAgMnPc1d1	příjemný
a	a	k8xC	a
laskaví	laskavý	k2eAgMnPc1d1	laskavý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
mu	on	k3xPp3gMnSc3	on
znovu	znovu	k6eAd1	znovu
pocit	pocit	k1gInSc4	pocit
bezpečí	bezpečí	k1gNnSc2	bezpečí
a	a	k8xC	a
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tak	tak	k6eAd1	tak
postrádal	postrádat	k5eAaImAgInS	postrádat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
spory	spor	k1gInPc4	spor
jej	on	k3xPp3gMnSc4	on
Zverev	Zvervo	k1gNnPc2	Zvervo
učil	učit	k5eAaImAgMnS	učit
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
potíží	potíž	k1gFnPc2	potíž
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ho	on	k3xPp3gMnSc4	on
kompozici	kompozice	k1gFnSc4	kompozice
vyučovali	vyučovat	k5eAaImAgMnP	vyučovat
Anton	Anton	k1gMnSc1	Anton
Arenskij	Arenskij	k1gMnSc1	Arenskij
a	a	k8xC	a
Sergej	Sergej	k1gMnSc1	Sergej
Tanějev	Tanějev	k1gFnSc2	Tanějev
<g/>
,	,	kIx,	,
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
Alexander	Alexandra	k1gFnPc2	Alexandra
Siloti	Silot	k1gMnPc5	Silot
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc1	potíž
přišly	přijít	k5eAaPmAgFnP	přijít
však	však	k9	však
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
Siloti	Silot	k1gMnPc1	Silot
se	se	k3xPyFc4	se
neshodl	shodnout	k5eNaPmAgInS	shodnout
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
ředitelem	ředitel	k1gMnSc7	ředitel
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nemohl	moct	k5eNaImAgInS	moct
smířit	smířit	k5eAaPmF	smířit
<g/>
,	,	kIx,	,
jiného	jiný	k2eAgMnSc2d1	jiný
učitele	učitel	k1gMnSc2	učitel
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zažádal	zažádat	k5eAaPmAgMnS	zažádat
o	o	k7c4	o
předčasné	předčasný	k2eAgNnSc4d1	předčasné
složení	složení	k1gNnSc4	složení
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žádost	žádost	k1gFnSc1	žádost
byla	být	k5eAaImAgFnS	být
vyslyšena	vyslyšet	k5eAaPmNgFnS	vyslyšet
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
skládal	skládat	k5eAaImAgInS	skládat
rok	rok	k1gInSc1	rok
před	před	k7c7	před
termínem	termín	k1gInSc7	termín
a	a	k8xC	a
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
přípravu	příprava	k1gFnSc4	příprava
měl	mít	k5eAaImAgInS	mít
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
složil	složit	k5eAaPmAgMnS	složit
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odbornou	odborný	k2eAgFnSc4d1	odborná
komisi	komise	k1gFnSc4	komise
nadchl	nadchnout	k5eAaPmAgMnS	nadchnout
i	i	k9	i
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
zkoušce	zkouška	k1gFnSc6	zkouška
z	z	k7c2	z
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
měli	mít	k5eAaImAgMnP	mít
složit	složit	k5eAaPmF	složit
jednoaktovou	jednoaktový	k2eAgFnSc4d1	jednoaktová
operu	opera	k1gFnSc4	opera
na	na	k7c4	na
námět	námět	k1gInSc4	námět
podle	podle	k7c2	podle
básně	báseň	k1gFnSc2	báseň
Alexandra	Alexandr	k1gMnSc2	Alexandr
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Puškina	Puškin	k1gMnSc2	Puškin
Aleko	Aleko	k1gNnSc4	Aleko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
osmnáct	osmnáct	k4xCc4	osmnáct
dnů	den	k1gInPc2	den
složil	složit	k5eAaPmAgInS	složit
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
operní	operní	k2eAgNnSc4d1	operní
dílo	dílo	k1gNnSc4	dílo
stejného	stejný	k2eAgInSc2d1	stejný
názvu	název	k1gInSc2	název
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Puškinova	Puškinův	k2eAgFnSc1d1	Puškinova
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Aleko	Aleko	k1gNnSc1	Aleko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
neobyčejný	obyčejný	k2eNgInSc4d1	neobyčejný
výkon	výkon	k1gInSc4	výkon
byl	být	k5eAaImAgInS	být
odměněn	odměnit	k5eAaPmNgInS	odměnit
Velkou	velký	k2eAgFnSc7d1	velká
zlatou	zlatý	k2eAgFnSc7d1	zlatá
medailí	medaile	k1gFnSc7	medaile
moskevské	moskevský	k2eAgFnSc2d1	Moskevská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
teprve	teprve	k6eAd1	teprve
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úspěch	úspěch	k1gInSc1	úspěch
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
k	k	k7c3	k
usmíření	usmíření	k1gNnSc3	usmíření
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
bývalým	bývalý	k2eAgMnSc7d1	bývalý
učitelem	učitel	k1gMnSc7	učitel
Zverevem	Zverev	k1gInSc7	Zverev
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gInSc7	jeho
úspěchem	úspěch	k1gInSc7	úspěch
natolik	natolik	k6eAd1	natolik
nadšen	nadchnout	k5eAaPmNgMnS	nadchnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
zlaté	zlatý	k2eAgFnPc4d1	zlatá
hodinky	hodinka	k1gFnPc4	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
opera	opera	k1gFnSc1	opera
úspěšně	úspěšně	k6eAd1	úspěšně
uvedena	uvést	k5eAaPmNgFnS	uvést
ve	v	k7c6	v
Velkém	velký	k2eAgNnSc6d1	velké
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
klavírního	klavírní	k2eAgInSc2d1	klavírní
koncertu	koncert	k1gInSc2	koncert
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
skladeb	skladba	k1gFnPc2	skladba
během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
studia	studio	k1gNnSc2	studio
předvedl	předvést	k5eAaPmAgMnS	předvést
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
veřejný	veřejný	k2eAgInSc4d1	veřejný
recitál	recitál	k1gInSc4	recitál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
studia	studio	k1gNnSc2	studio
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
hudebnímu	hudební	k2eAgNnSc3d1	hudební
vydavateli	vydavatel	k1gMnSc3	vydavatel
Gutheilovi	Gutheil	k1gMnSc3	Gutheil
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
operu	opera	k1gFnSc4	opera
Aleko	Aleko	k1gNnSc1	Aleko
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
soubor	soubor	k1gInSc1	soubor
skladeb	skladba	k1gFnPc2	skladba
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
tehdy	tehdy	k6eAd1	tehdy
značnou	značný	k2eAgFnSc4d1	značná
částku	částka	k1gFnSc4	částka
pět	pět	k4xCc4	pět
set	sto	k4xCgNnPc2	sto
rublů	rubl	k1gInPc2	rubl
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
složil	složit	k5eAaPmAgMnS	složit
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
Preludium	preludium	k1gNnSc4	preludium
cis	cis	k1gNnSc2	cis
moll	moll	k1gNnSc2	moll
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
a	a	k8xC	a
které	který	k3yIgFnPc4	který
pak	pak	k6eAd1	pak
musel	muset	k5eAaImAgMnS	muset
hrát	hrát	k5eAaImF	hrát
alespoň	alespoň	k9	alespoň
jako	jako	k9	jako
přídavek	přídavek	k1gInSc4	přídavek
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
svých	svůj	k3xOyFgInPc6	svůj
pozdějších	pozdní	k2eAgInPc6d2	pozdější
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Nevěřil	věřit	k5eNaImAgMnS	věřit
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
skladbou	skladba	k1gFnSc7	skladba
a	a	k8xC	a
klavírní	klavírní	k2eAgFnSc7d1	klavírní
hrou	hra	k1gFnSc7	hra
uživil	uživit	k5eAaPmAgInS	uživit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
stal	stát	k5eAaPmAgInS	stát
učitelem	učitel	k1gMnSc7	učitel
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
tuto	tento	k3xDgFnSc4	tento
profesi	profese	k1gFnSc4	profese
neměl	mít	k5eNaImAgMnS	mít
rád	rád	k2eAgMnSc1d1	rád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nastalo	nastat	k5eAaPmAgNnS	nastat
vzrušující	vzrušující	k2eAgNnSc1d1	vzrušující
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neklidné	klidný	k2eNgNnSc4d1	neklidné
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rodiny	rodina	k1gFnSc2	rodina
Satinových	Satinův	k2eAgMnPc2d1	Satinův
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
bydlel	bydlet	k5eAaImAgMnS	bydlet
se	s	k7c7	s
spolužákem	spolužák	k1gMnSc7	spolužák
a	a	k8xC	a
poté	poté	k6eAd1	poté
krátce	krátce	k6eAd1	krátce
i	i	k9	i
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
přijel	přijet	k5eAaPmAgMnS	přijet
pracovně	pracovně	k6eAd1	pracovně
<g/>
.	.	kIx.	.
</s>
<s>
Nakrátko	nakrátko	k6eAd1	nakrátko
bydlel	bydlet	k5eAaImAgMnS	bydlet
i	i	k9	i
v	v	k7c6	v
pronajatém	pronajatý	k2eAgInSc6d1	pronajatý
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
Satinovým	Satinová	k1gFnPc3	Satinová
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
nového	nový	k2eAgInSc2d1	nový
a	a	k8xC	a
většího	veliký	k2eAgInSc2d2	veliký
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ložnici	ložnice	k1gFnSc4	ložnice
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pracovnu	pracovna	k1gFnSc4	pracovna
<g/>
.	.	kIx.	.
</s>
<s>
Ranou	Rana	k1gFnSc7	Rana
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
smrt	smrt	k1gFnSc1	smrt
jeho	on	k3xPp3gMnSc2	on
učitele	učitel	k1gMnSc2	učitel
Zvereva	Zverev	k1gMnSc2	Zverev
a	a	k8xC	a
obzvláště	obzvláště	k6eAd1	obzvláště
Petra	Petra	k1gFnSc1	Petra
Iljiče	Iljič	k1gMnSc2	Iljič
Čajkovského	Čajkovský	k2eAgMnSc2d1	Čajkovský
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gMnSc4	on
skutečného	skutečný	k2eAgMnSc4d1	skutečný
přítele	přítel	k1gMnSc4	přítel
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
vzrušující	vzrušující	k2eAgInSc4d1	vzrušující
i	i	k8xC	i
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Bláznivě	bláznivě	k6eAd1	bláznivě
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
vdané	vdaný	k2eAgFnSc2d1	vdaná
Anny	Anna	k1gFnSc2	Anna
Lodžinské	Lodžinský	k2eAgFnSc2d1	Lodžinský
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgMnS	věnovat
jí	jíst	k5eAaImIp3nS	jíst
svou	svůj	k3xOyFgFnSc4	svůj
1	[number]	k4	1
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc6	symfonie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
své	svůj	k3xOyFgInPc4	svůj
vášnivé	vášnivý	k2eAgInPc4d1	vášnivý
city	cit	k1gInPc4	cit
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
rána	rána	k1gFnSc1	rána
však	však	k9	však
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nedala	dát	k5eNaPmAgNnP	dát
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Rachmaninovova	Rachmaninovův	k2eAgFnSc1d1	Rachmaninovův
1	[number]	k4	1
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnSc1	symfonie
byla	být	k5eAaImAgFnS	být
nedostatečně	dostatečně	k6eNd1	dostatečně
nacvičena	nacvičit	k5eAaBmNgFnS	nacvičit
a	a	k8xC	a
při	při	k7c6	při
premiéře	premiéra	k1gFnSc6	premiéra
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ji	on	k3xPp3gFnSc4	on
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Alexandr	Alexandr	k1gMnSc1	Alexandr
Glazunov	Glazunov	k1gInSc4	Glazunov
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
špatně	špatně	k6eAd1	špatně
provedena	provést	k5eAaPmNgFnS	provést
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
nemohl	moct	k5eNaImAgMnS	moct
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
ani	ani	k8xC	ani
poslouchat	poslouchat	k5eAaImF	poslouchat
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
koncertní	koncertní	k2eAgFnSc2d1	koncertní
síně	síň	k1gFnSc2	síň
předčasně	předčasně	k6eAd1	předčasně
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
ztrháno	ztrhán	k2eAgNnSc1d1	ztrhán
i	i	k8xC	i
hudebními	hudební	k2eAgMnPc7d1	hudební
kritiky	kritik	k1gMnPc7	kritik
a	a	k8xC	a
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
se	se	k3xPyFc4	se
nervově	nervově	k6eAd1	nervově
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
tři	tři	k4xCgInPc1	tři
roky	rok	k1gInPc1	rok
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
naprosto	naprosto	k6eAd1	naprosto
neplodné	plodný	k2eNgNnSc1d1	neplodné
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
byl	být	k5eAaImAgInS	být
otřesen	otřást	k5eAaPmNgInS	otřást
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
neúspěchu	neúspěch	k1gInSc2	neúspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návrat	návrat	k1gInSc1	návrat
ztracené	ztracený	k2eAgFnSc2d1	ztracená
síly	síla	k1gFnSc2	síla
===	===	k?	===
</s>
</p>
<p>
<s>
Štěstěna	Štěstěna	k1gFnSc1	Štěstěna
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
jinou	jiný	k2eAgFnSc4d1	jiná
možnost	možnost	k1gFnSc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
řediteli	ředitel	k1gMnSc3	ředitel
moskevské	moskevský	k2eAgFnSc2d1	Moskevská
soukromé	soukromý	k2eAgFnSc2d1	soukromá
operní	operní	k2eAgFnSc2d1	operní
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
milionáři	milionář	k1gMnSc6	milionář
Savvovi	Savva	k1gMnSc6	Savva
Mamontovovi	Mamontova	k1gMnSc6	Mamontova
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
místo	místo	k6eAd1	místo
druhého	druhý	k4xOgMnSc4	druhý
dirigenta	dirigent	k1gMnSc4	dirigent
pro	pro	k7c4	pro
sezónu	sezóna	k1gFnSc4	sezóna
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
také	také	k9	také
začalo	začít	k5eAaPmAgNnS	začít
jeho	jeho	k3xOp3gNnSc1	jeho
celoživotní	celoživotní	k2eAgNnSc1d1	celoživotní
přátelství	přátelství	k1gNnSc1	přátelství
se	s	k7c7	s
slavným	slavný	k2eAgMnSc7d1	slavný
operním	operní	k2eAgMnSc7d1	operní
pěvcem	pěvec	k1gMnSc7	pěvec
Fjodorem	Fjodor	k1gMnSc7	Fjodor
Ivanovičem	Ivanovič	k1gMnSc7	Ivanovič
Šaljapinem	Šaljapin	k1gMnSc7	Šaljapin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1898	[number]	k4	1898
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
svatbě	svatba	k1gFnSc6	svatba
s	s	k7c7	s
italskou	italský	k2eAgFnSc7d1	italská
baletkou	baletka	k1gFnSc7	baletka
Tornaghiovou	Tornaghiový	k2eAgFnSc7d1	Tornaghiový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
navštívil	navštívit	k5eAaPmAgMnS	navštívit
pracovně	pracovně	k6eAd1	pracovně
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řídil	řídit	k5eAaImAgMnS	řídit
provedení	provedení	k1gNnSc4	provedení
symfonické	symfonický	k2eAgFnSc2d1	symfonická
básně	báseň	k1gFnSc2	báseň
Útes	útes	k1gInSc4	útes
v	v	k7c4	v
Queen	Queen	k1gInSc4	Queen
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
a	a	k8xC	a
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
filharmonické	filharmonický	k2eAgFnSc2d1	filharmonická
společnosti	společnost	k1gFnSc2	společnost
přislíbil	přislíbit	k5eAaPmAgInS	přislíbit
nový	nový	k2eAgInSc1d1	nový
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nervový	nervový	k2eAgInSc1d1	nervový
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
však	však	k9	však
stále	stále	k6eAd1	stále
ochromen	ochromen	k2eAgMnSc1d1	ochromen
a	a	k8xC	a
dělalo	dělat	k5eAaImAgNnS	dělat
mu	on	k3xPp3gMnSc3	on
problém	problém	k1gInSc4	problém
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
tohoto	tento	k3xDgInSc2	tento
klavírního	klavírní	k2eAgInSc2d1	klavírní
koncertu	koncert	k1gInSc2	koncert
pokročit	pokročit	k5eAaPmF	pokročit
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
tedy	tedy	k9	tedy
odbornou	odborný	k2eAgFnSc4d1	odborná
pomoc	pomoc	k1gFnSc4	pomoc
moskevského	moskevský	k2eAgMnSc2d1	moskevský
lékaře	lékař	k1gMnSc2	lékař
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Dahla	Dahl	k1gMnSc2	Dahl
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborníka	odborník	k1gMnSc2	odborník
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
hypnózou	hypnóza	k1gFnSc7	hypnóza
<g/>
.	.	kIx.	.
</s>
<s>
Dahl	dahl	k1gInSc1	dahl
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k6eAd1	rovněž
talentovaným	talentovaný	k2eAgMnSc7d1	talentovaný
amatérským	amatérský	k2eAgMnSc7d1	amatérský
hudebníkem	hudebník	k1gMnSc7	hudebník
a	a	k8xC	a
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
trpělivé	trpělivý	k2eAgFnSc3d1	trpělivá
péči	péče	k1gFnSc3	péče
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vrátit	vrátit	k5eAaPmF	vrátit
Rachmaninovovi	Rachmaninova	k1gMnSc3	Rachmaninova
chuť	chuť	k1gFnSc4	chuť
komponovat	komponovat	k5eAaImF	komponovat
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
velké	velký	k2eAgNnSc4d1	velké
tvůrčí	tvůrčí	k2eAgNnSc4d1	tvůrčí
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
během	během	k7c2	během
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
dokončil	dokončit	k5eAaPmAgInS	dokončit
dvě	dva	k4xCgFnPc1	dva
věty	věta	k1gFnPc1	věta
nového	nový	k2eAgInSc2d1	nový
klavírního	klavírní	k2eAgInSc2d1	klavírní
koncertu	koncert	k1gInSc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
tento	tento	k3xDgInSc1	tento
2	[number]	k4	2
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
dokončil	dokončit	k5eAaPmAgInS	dokončit
s	s	k7c7	s
věnováním	věnování	k1gNnSc7	věnování
doktoru	doktor	k1gMnSc3	doktor
Dahlovi	Dahl	k1gMnSc3	Dahl
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
jej	on	k3xPp3gInSc4	on
uvedl	uvést	k5eAaPmAgMnS	uvést
za	za	k7c2	za
nadšeného	nadšený	k2eAgNnSc2d1	nadšené
aplaudování	aplaudování	k1gNnSc2	aplaudování
publika	publikum	k1gNnSc2	publikum
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
koncert	koncert	k1gInSc1	koncert
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
uveden	uvést	k5eAaPmNgInS	uvést
také	také	k9	také
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
<g/>
Bratranec	bratranec	k1gMnSc1	bratranec
Siloti	Silot	k1gMnPc1	Silot
mu	on	k3xPp3gNnSc3	on
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
po	po	k7c4	po
následující	následující	k2eAgInSc4d1	následující
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
stálý	stálý	k2eAgInSc4d1	stálý
plat	plat	k1gInSc4	plat
jako	jako	k8xC	jako
důvěru	důvěra	k1gFnSc4	důvěra
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
schopnosti	schopnost	k1gFnSc6	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Nadšený	nadšený	k2eAgMnSc1d1	nadšený
a	a	k8xC	a
existenčně	existenčně	k6eAd1	existenčně
zabezpečený	zabezpečený	k2eAgMnSc1d1	zabezpečený
požádal	požádat	k5eAaPmAgInS	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
Natatalii	Natatalie	k1gFnSc4	Natatalie
Satinovou	Satinová	k1gFnSc4	Satinová
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
však	však	k9	však
tento	tento	k3xDgInSc4	tento
sňatek	sňatek	k1gInSc4	sňatek
zprvu	zprvu	k6eAd1	zprvu
neschválila	schválit	k5eNaPmAgFnS	schválit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Natalia	Natalia	k1gFnSc1	Natalia
byla	být	k5eAaImAgFnS	být
Sergejova	Sergejův	k2eAgFnSc1d1	Sergejova
sestřenice	sestřenice	k1gFnSc1	sestřenice
<g/>
.	.	kIx.	.
</s>
<s>
Snoubenci	snoubenec	k1gMnPc1	snoubenec
však	však	k9	však
získali	získat	k5eAaPmAgMnP	získat
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
povolení	povolení	k1gNnSc4	povolení
od	od	k7c2	od
cara	car	k1gMnSc2	car
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1902	[number]	k4	1902
slavili	slavit	k5eAaImAgMnP	slavit
svatbu	svatba	k1gFnSc4	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
snad	snad	k9	snad
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
období	období	k1gNnSc1	období
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
narodily	narodit	k5eAaPmAgFnP	narodit
dvě	dva	k4xCgFnPc1	dva
dcery	dcera	k1gFnPc1	dcera
<g/>
:	:	kIx,	:
Irina	Irina	k1gFnSc1	Irina
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
a	a	k8xC	a
Taťána	Taťána	k1gFnSc1	Taťána
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dirigent	dirigent	k1gMnSc1	dirigent
strávil	strávit	k5eAaPmAgMnS	strávit
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
dvě	dva	k4xCgFnPc1	dva
sezóny	sezóna	k1gFnPc1	sezóna
v	v	k7c6	v
moskevském	moskevský	k2eAgNnSc6d1	moskevské
Velkém	velký	k2eAgNnSc6d1	velké
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
tohoto	tento	k3xDgNnSc2	tento
angažmá	angažmá	k1gNnSc2	angažmá
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c4	po
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
trávil	trávit	k5eAaImAgMnS	trávit
zimní	zimní	k2eAgInPc4d1	zimní
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
kde	kde	k6eAd1	kde
dokončil	dokončit	k5eAaPmAgMnS	dokončit
svou	svůj	k3xOyFgFnSc4	svůj
2	[number]	k4	2
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc6	symfonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
podnikl	podniknout	k5eAaPmAgMnS	podniknout
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uveden	uveden	k2eAgInSc1d1	uveden
jeho	jeho	k3xOp3gInSc1	jeho
proslulý	proslulý	k2eAgInSc1d1	proslulý
3	[number]	k4	3
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Waltera	Walter	k1gMnSc2	Walter
Damrosche	Damrosch	k1gMnSc2	Damrosch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgInPc6	tento
dílech	díl	k1gInPc6	díl
následovala	následovat	k5eAaImAgFnS	následovat
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
Ostrov	ostrov	k1gInSc1	ostrov
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
obrazem	obraz	k1gInSc7	obraz
malíře	malíř	k1gMnSc2	malíř
Arnolda	Arnold	k1gMnSc2	Arnold
Böcklina	Böcklina	k1gFnSc1	Böcklina
<g/>
,	,	kIx,	,
a	a	k8xC	a
chorální	chorální	k2eAgFnPc1d1	chorální
symfonie	symfonie	k1gFnPc1	symfonie
Zvony	zvon	k1gInPc4	zvon
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
básní	básnit	k5eAaImIp3nP	básnit
Edgara	Edgar	k1gMnSc4	Edgar
Allana	Allan	k1gMnSc4	Allan
Poea	Poeus	k1gMnSc4	Poeus
<g/>
.	.	kIx.	.
</s>
<s>
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stal	stát	k5eAaPmAgInS	stát
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
"	"	kIx"	"
<g/>
Říšské	říšský	k2eAgFnSc2d1	říšská
ruské	ruský	k2eAgFnSc2d1	ruská
hudební	hudební	k2eAgFnSc2d1	hudební
společnosti	společnost	k1gFnSc2	společnost
<g/>
"	"	kIx"	"
v	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgNnSc1d1	pracovní
nasazení	nasazení	k1gNnSc1	nasazení
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
času	čas	k1gInSc2	čas
na	na	k7c6	na
komponování	komponování	k1gNnSc6	komponování
vyvolávaly	vyvolávat	k5eAaImAgInP	vyvolávat
u	u	k7c2	u
Rachmaninova	Rachmaninův	k2eAgFnSc1d1	Rachmaninova
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
problémy	problém	k1gInPc7	problém
se	se	k3xPyFc4	se
svěřoval	svěřovat	k5eAaImAgInS	svěřovat
přátelům	přítel	k1gMnPc3	přítel
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc2	který
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
také	také	k9	také
pochybnosti	pochybnost	k1gFnPc4	pochybnost
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
<g/>
.	.	kIx.	.
</s>
<s>
Útěchou	útěcha	k1gFnSc7	útěcha
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
ruské	ruský	k2eAgNnSc1d1	ruské
pravoslavné	pravoslavný	k2eAgNnSc1d1	pravoslavné
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
silně	silně	k6eAd1	silně
ovlivňován	ovlivňovat	k5eAaImNgMnS	ovlivňovat
bohoslužbami	bohoslužba	k1gFnPc7	bohoslužba
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obzvláště	obzvláště	k6eAd1	obzvláště
pak	pak	k6eAd1	pak
církevní	církevní	k2eAgFnSc7d1	církevní
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Složil	složit	k5eAaPmAgMnS	složit
dvě	dva	k4xCgNnPc4	dva
náboženská	náboženský	k2eAgNnPc4d1	náboženské
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
Liturgii	liturgie	k1gFnSc4	liturgie
Jana	Jan	k1gMnSc2	Jan
Chrysostoma	Chrysostom	k1gMnSc2	Chrysostom
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
a	a	k8xC	a
Celonoční	celonoční	k2eAgNnSc1d1	celonoční
bdění	bdění	k1gNnSc1	bdění
(	(	kIx(	(
<g/>
Vigilie	vigilie	k1gFnSc1	vigilie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Emigrace	emigrace	k1gFnPc4	emigrace
a	a	k8xC	a
život	život	k1gInSc4	život
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
===	===	k?	===
</s>
</p>
<p>
<s>
Politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stále	stále	k6eAd1	stále
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc1d2	vyšší
společenské	společenský	k2eAgFnPc1d1	společenská
vrstvy	vrstva	k1gFnPc1	vrstva
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
bát	bát	k5eAaImF	bát
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gInPc1	jeho
politické	politický	k2eAgInPc1d1	politický
názory	názor	k1gInPc1	názor
nebyly	být	k5eNaImAgInP	být
vyhraněné	vyhraněný	k2eAgInPc1d1	vyhraněný
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
hudebníky	hudebník	k1gMnPc7	hudebník
prohlášení	prohlášení	k1gNnSc2	prohlášení
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
zavedení	zavedení	k1gNnSc4	zavedení
nových	nový	k2eAgFnPc2d1	nová
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jako	jako	k8xC	jako
šlechtic	šlechtic	k1gMnSc1	šlechtic
vlastnící	vlastnící	k2eAgInSc4d1	vlastnící
statek	statek	k1gInSc4	statek
Ivanovku	Ivanovka	k1gFnSc4	Ivanovka
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
sám	sám	k3xTgMnSc1	sám
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
o	o	k7c4	o
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
vesnického	vesnický	k2eAgNnSc2d1	vesnické
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
natolik	natolik	k6eAd1	natolik
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
hledal	hledat	k5eAaImAgInS	hledat
útočiště	útočiště	k1gNnSc4	útočiště
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
kdekoliv	kdekoliv	k6eAd1	kdekoliv
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Příležitost	příležitost	k1gFnSc4	příležitost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
při	pře	k1gFnSc4	pře
nabídce	nabídka	k1gFnSc3	nabídka
koncertního	koncertní	k2eAgNnSc2d1	koncertní
turné	turné	k1gNnSc2	turné
po	po	k7c6	po
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ihned	ihned	k6eAd1	ihned
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
obstaral	obstarat	k5eAaPmAgMnS	obstarat
potřebné	potřebný	k2eAgFnPc4d1	potřebná
formality	formalita	k1gFnPc4	formalita
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
již	již	k9	již
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgFnSc6d1	velká
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nevrátila	vrátit	k5eNaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
středních	střední	k2eAgNnPc6d1	střední
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
dobře	dobře	k6eAd1	dobře
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodinu	rodina	k1gFnSc4	rodina
uživí	uživit	k5eAaPmIp3nS	uživit
nejlépe	dobře	k6eAd3	dobře
jako	jako	k9	jako
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
39	[number]	k4	39
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
konečných	konečný	k2eAgNnPc2d1	konečné
45	[number]	k4	45
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
strávené	strávený	k2eAgFnSc2d1	strávená
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
a	a	k8xC	a
Stockholmu	Stockholm	k1gInSc2	Stockholm
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
usadil	usadit	k5eAaPmAgInS	usadit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
trávil	trávit	k5eAaImAgMnS	trávit
veškerý	veškerý	k3xTgInSc4	veškerý
čas	čas	k1gInSc4	čas
nahráváním	nahrávání	k1gNnSc7	nahrávání
svých	svůj	k3xOyFgFnPc2	svůj
klavírních	klavírní	k2eAgFnPc2d1	klavírní
interpretací	interpretace	k1gFnPc2	interpretace
na	na	k7c4	na
gramofonové	gramofonový	k2eAgFnPc4d1	gramofonová
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
koncertováním	koncertování	k1gNnSc7	koncertování
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
udržet	udržet	k5eAaPmF	udržet
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
manželé	manžel	k1gMnPc1	manžel
Rachmaninovi	Rachmaninův	k2eAgMnPc1d1	Rachmaninův
setkali	setkat	k5eAaPmAgMnP	setkat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dcerami	dcera	k1gFnPc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Šťastné	Šťastné	k2eAgNnSc1d1	Šťastné
shledání	shledání	k1gNnSc1	shledání
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
narušeno	narušen	k2eAgNnSc1d1	narušeno
smrtí	smrt	k1gFnSc7	smrt
manžela	manžel	k1gMnSc4	manžel
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
dcer	dcera	k1gFnPc2	dcera
<g/>
,	,	kIx,	,
Iriny	Irina	k1gFnSc2	Irina
<g/>
,	,	kIx,	,
knížete	kníže	k1gMnSc2	kníže
Volkonského	Volkonský	k2eAgMnSc2d1	Volkonský
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
otec	otec	k1gMnSc1	otec
zemřelého	zemřelý	k2eAgMnSc2d1	zemřelý
manžela	manžel	k1gMnSc2	manžel
se	se	k3xPyFc4	se
i	i	k9	i
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
snažil	snažit	k5eAaImAgMnS	snažit
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
založil	založit	k5eAaPmAgMnS	založit
Vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
TAIR	TAIR	kA	TAIR
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
díla	dílo	k1gNnPc4	dílo
ruských	ruský	k2eAgMnPc2d1	ruský
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
také	také	k9	také
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
společnosti	společnost	k1gFnSc2	společnost
přenechal	přenechat	k5eAaPmAgMnS	přenechat
svým	svůj	k3xOyFgNnSc7	svůj
dvěma	dva	k4xCgFnPc3	dva
dcerám	dcera	k1gFnPc3	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
trávil	trávit	k5eAaImAgMnS	trávit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
nájem	nájem	k1gInSc1	nájem
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
Clairfontaine	Clairfontain	k1gInSc5	Clairfontain
nedaleko	nedaleko	k7c2	nedaleko
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
scházel	scházet	k5eAaImAgMnS	scházet
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
ruskými	ruský	k2eAgMnPc7d1	ruský
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
emigranty	emigrant	k1gMnPc7	emigrant
byly	být	k5eAaImAgFnP	být
i	i	k9	i
rodiny	rodina	k1gFnPc1	rodina
Silotiových	Silotiový	k2eAgInPc2d1	Silotiový
a	a	k8xC	a
Satinových	Satinův	k2eAgInPc2d1	Satinův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
vilu	vila	k1gFnSc4	vila
ve	v	k7c6	v
Weggisu	Weggis	k1gInSc6	Weggis
u	u	k7c2	u
Lucernského	Lucernský	k2eAgNnSc2d1	Lucernský
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Senar	Senar	k1gMnSc1	Senar
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nacházel	nacházet	k5eAaImAgMnS	nacházet
potřebný	potřebný	k2eAgInSc4d1	potřebný
klid	klid	k1gInSc4	klid
po	po	k7c6	po
rušných	rušný	k2eAgFnPc6d1	rušná
pracovních	pracovní	k2eAgFnPc6d1	pracovní
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
koncertování	koncertování	k1gNnSc6	koncertování
<g/>
,	,	kIx,	,
dirigování	dirigování	k1gNnSc4	dirigování
a	a	k8xC	a
nahrávání	nahrávání	k1gNnSc4	nahrávání
pro	pro	k7c4	pro
Victor	Victor	k1gMnSc1	Victor
Talking	Talking	k1gInSc1	Talking
Machine	Machin	k1gInSc5	Machin
Company	Compan	k1gMnPc7	Compan
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
podepsal	podepsat	k5eAaPmAgInS	podepsat
manifest	manifest	k1gInSc1	manifest
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
The	The	k1gFnSc6	The
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
a	a	k8xC	a
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
kulturní	kulturní	k2eAgFnSc4d1	kulturní
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
hrát	hrát	k5eAaImF	hrát
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
tisk	tisk	k1gInSc1	tisk
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
pouhého	pouhý	k2eAgMnSc4d1	pouhý
imitátora	imitátor	k1gMnSc4	imitátor
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
prý	prý	k9	prý
ztratila	ztratit	k5eAaPmAgFnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
avantgardní	avantgardní	k2eAgInSc4d1	avantgardní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
složil	složit	k5eAaPmAgMnS	složit
některá	některý	k3yIgNnPc1	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
3	[number]	k4	3
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc4	symfonie
<g/>
,	,	kIx,	,
Rapsodii	rapsodie	k1gFnSc4	rapsodie
na	na	k7c4	na
Paganiniho	Paganini	k1gMnSc4	Paganini
téma	téma	k1gNnSc2	téma
a	a	k8xC	a
Symfonické	symfonický	k2eAgInPc4d1	symfonický
tance	tanec	k1gInPc4	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
jím	jíst	k5eAaImIp1nS	jíst
hluboce	hluboko	k6eAd1	hluboko
otřásla	otřást	k5eAaPmAgFnS	otřást
smrt	smrt	k1gFnSc4	smrt
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
pěvce	pěvec	k1gMnPc4	pěvec
(	(	kIx(	(
<g/>
basisty	basista	k1gMnSc2	basista
<g/>
)	)	kIx)	)
Fjodora	Fjodor	k1gMnSc2	Fjodor
Šaljapina	Šaljapin	k2eAgMnSc2d1	Šaljapin
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
několikrát	několikrát	k6eAd1	několikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
nové	nový	k2eAgFnSc2d1	nová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
starší	starý	k2eAgFnSc7d2	starší
dcerou	dcera	k1gFnSc7	dcera
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tíživou	tíživý	k2eAgFnSc4d1	tíživá
situaci	situace	k1gFnSc4	situace
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
snažil	snažit	k5eAaImAgMnS	snažit
skládat	skládat	k5eAaImF	skládat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obavy	obava	k1gFnPc1	obava
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
silnější	silný	k2eAgFnPc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Tížila	tížit	k5eAaImAgFnS	tížit
ho	on	k3xPp3gNnSc4	on
i	i	k9	i
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vpadla	vpadnout	k5eAaPmAgNnP	vpadnout
německá	německý	k2eAgNnPc1d1	německé
nacistická	nacistický	k2eAgNnPc1d1	nacistické
vojska	vojsko	k1gNnPc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
původní	původní	k2eAgFnSc4d1	původní
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
komunistické	komunistický	k2eAgFnSc2d1	komunistická
vlády	vláda	k1gFnSc2	vláda
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gMnSc3	on
přispíval	přispívat	k5eAaImAgInS	přispívat
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
do	do	k7c2	do
sovětského	sovětský	k2eAgInSc2d1	sovětský
"	"	kIx"	"
<g/>
Fondu	fond	k1gInSc2	fond
válečné	válečný	k2eAgFnSc2d1	válečná
podpory	podpora	k1gFnSc2	podpora
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
ho	on	k3xPp3gMnSc4	on
proto	proto	k6eAd1	proto
opět	opět	k6eAd1	opět
přijala	přijmout	k5eAaPmAgFnS	přijmout
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
uznávaných	uznávaný	k2eAgMnPc2d1	uznávaný
ruských	ruský	k2eAgMnPc2d1	ruský
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
v	v	k7c4	v
Beverly	Beverl	k1gInPc4	Beverl
Hills	Hillsa	k1gFnPc2	Hillsa
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
po	po	k7c6	po
náhlém	náhlý	k2eAgNnSc6d1	náhlé
zhoršení	zhoršení	k1gNnSc6	zhoršení
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
na	na	k7c6	na
ruském	ruský	k2eAgInSc6d1	ruský
pravoslavném	pravoslavný	k2eAgInSc6d1	pravoslavný
hřbitově	hřbitov	k1gInSc6	hřbitov
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kensico	Kensico	k6eAd1	Kensico
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
styl	styl	k1gInSc1	styl
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Osobitý	osobitý	k2eAgInSc4d1	osobitý
styl	styl	k1gInSc4	styl
===	===	k?	===
</s>
</p>
<p>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
posledním	poslední	k2eAgMnPc3d1	poslední
ruským	ruský	k2eAgMnPc3d1	ruský
romantickým	romantický	k2eAgMnPc3d1	romantický
skladatelům	skladatel	k1gMnPc3	skladatel
a	a	k8xC	a
protože	protože	k8xS	protože
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
jak	jak	k6eAd1	jak
starými	starý	k2eAgMnPc7d1	starý
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
novými	nový	k2eAgInPc7d1	nový
tvůrčími	tvůrčí	k2eAgInPc7d1	tvůrčí
styly	styl	k1gInPc7	styl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
dělalo	dělat	k5eAaImAgNnS	dělat
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Miloval	milovat	k5eAaImAgInS	milovat
hudbu	hudba	k1gFnSc4	hudba
Rimského-Korsakova	Rimského-Korsakův	k2eAgMnSc2d1	Rimského-Korsakův
a	a	k8xC	a
cítil	cítit	k5eAaImAgMnS	cítit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
náležitě	náležitě	k6eAd1	náležitě
zavázán	zavázán	k2eAgMnSc1d1	zavázán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
na	na	k7c6	na
petrohradské	petrohradský	k2eAgFnSc6d1	Petrohradská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
P.	P.	kA	P.
I.	I.	kA	I.
Čajkovským	Čajkovský	k2eAgMnSc7d1	Čajkovský
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
sdíleli	sdílet	k5eAaImAgMnP	sdílet
stejný	stejný	k2eAgInSc4d1	stejný
názor	názor	k1gInSc4	názor
na	na	k7c4	na
důležitost	důležitost	k1gFnSc4	důležitost
silné	silný	k2eAgFnSc2d1	silná
melodie	melodie	k1gFnSc2	melodie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
přetrvat	přetrvat	k5eAaPmF	přetrvat
navěky	navěky	k6eAd1	navěky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
původní	původní	k2eAgInSc4d1	původní
neúspěch	neúspěch	k1gInSc4	neúspěch
1	[number]	k4	1
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnPc1	symfonie
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
jím	jíst	k5eAaImIp1nS	jíst
silně	silně	k6eAd1	silně
otřásl	otřást	k5eAaPmAgMnS	otřást
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
se	se	k3xPyFc4	se
nedalo	dát	k5eNaPmAgNnS	dát
skrýt	skrýt	k5eAaPmF	skrýt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokázal	dokázat	k5eAaPmAgInS	dokázat
1	[number]	k4	1
<g/>
.	.	kIx.	.
klavírním	klavírní	k2eAgInSc7d1	klavírní
koncertem	koncert	k1gInSc7	koncert
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
slavným	slavný	k2eAgInPc3d1	slavný
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
Preludia	preludium	k1gNnSc2	preludium
cis-moll	cisolla	k1gFnPc2	cis-molla
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
bylo	být	k5eAaImAgNnS	být
publikum	publikum	k1gNnSc1	publikum
naprosto	naprosto	k6eAd1	naprosto
uchváceno	uchvácet	k5eAaImNgNnS	uchvácet
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
jej	on	k3xPp3gNnSc4	on
poté	poté	k6eAd1	poté
musel	muset	k5eAaImAgMnS	muset
hrát	hrát	k5eAaImF	hrát
alespoň	alespoň	k9	alespoň
jako	jako	k9	jako
přídavek	přídavek	k1gInSc4	přídavek
při	při	k7c6	při
svých	svůj	k3xOyFgNnPc6	svůj
koncertních	koncertní	k2eAgNnPc6d1	koncertní
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
skladatele	skladatel	k1gMnSc2	skladatel
sužovaly	sužovat	k5eAaImAgFnP	sužovat
pochyby	pochyba	k1gFnPc1	pochyba
o	o	k7c6	o
sobě	se	k3xPyFc3	se
samém	samý	k3xTgMnSc6	samý
a	a	k8xC	a
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
jeho	jeho	k3xOp3gFnSc1	jeho
citlivá	citlivý	k2eAgFnSc1d1	citlivá
povaha	povaha	k1gFnSc1	povaha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
mistrovskému	mistrovský	k2eAgInSc3d1	mistrovský
a	a	k8xC	a
mezi	mezi	k7c7	mezi
posluchači	posluchač	k1gMnPc7	posluchač
velmi	velmi	k6eAd1	velmi
oblíbenému	oblíbený	k2eAgInSc3d1	oblíbený
2	[number]	k4	2
<g/>
.	.	kIx.	.
klavírnímu	klavírní	k2eAgInSc3d1	klavírní
koncertu	koncert	k1gInSc2	koncert
c-moll	colla	k1gFnPc2	c-molla
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dílo	dílo	k1gNnSc1	dílo
plné	plný	k2eAgFnSc2d1	plná
tajemné	tajemný	k2eAgFnSc2d1	tajemná
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
,	,	kIx,	,
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
zde	zde	k6eAd1	zde
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
svou	svůj	k3xOyFgFnSc4	svůj
ideu	idea	k1gFnSc4	idea
nadčasového	nadčasový	k2eAgInSc2d1	nadčasový
<g/>
,	,	kIx,	,
velikého	veliký	k2eAgMnSc2d1	veliký
a	a	k8xC	a
nádherně	nádherně	k6eAd1	nádherně
melodického	melodický	k2eAgNnSc2d1	melodické
díla	dílo	k1gNnSc2	dílo
plného	plný	k2eAgInSc2d1	plný
lyrických	lyrický	k2eAgInPc2d1	lyrický
momentů	moment	k1gInPc2	moment
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
virtuosním	virtuosní	k2eAgNnSc7d1	virtuosní
klavírním	klavírní	k2eAgNnSc7d1	klavírní
podáním	podání	k1gNnSc7	podání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
sláva	sláva	k1gFnSc1	sláva
a	a	k8xC	a
věhlas	věhlas	k1gInSc1	věhlas
jeho	jeho	k3xOp3gNnSc2	jeho
jedinečného	jedinečný	k2eAgNnSc2d1	jedinečné
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
šířila	šířit	k5eAaImAgFnS	šířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
bez	bez	k7c2	bez
obtíží	obtíž	k1gFnPc2	obtíž
zahrát	zahrát	k5eAaPmF	zahrát
jeho	jeho	k3xOp3gFnPc2	jeho
pověstně	pověstně	k6eAd1	pověstně
náročné	náročný	k2eAgInPc4d1	náročný
akordy	akord	k1gInPc4	akord
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
zvládnout	zvládnout	k5eAaPmF	zvládnout
pouze	pouze	k6eAd1	pouze
někteří	některý	k3yIgMnPc1	některý
<g/>
,	,	kIx,	,
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
technikou	technika	k1gFnSc7	technika
vybavení	vybavení	k1gNnSc2	vybavení
klavírní	klavírní	k2eAgFnSc2d1	klavírní
virtuózové	virtuózové	k?	virtuózové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgNnPc2	svůj
skladatelsky	skladatelsky	k6eAd1	skladatelsky
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
let	léto	k1gNnPc2	léto
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
skladby	skladba	k1gFnPc4	skladba
jako	jako	k9	jako
2	[number]	k4	2
<g/>
.	.	kIx.	.
symfonii	symfonie	k1gFnSc6	symfonie
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
skladeb	skladba	k1gFnPc2	skladba
nahrával	nahrávat	k5eAaImAgInS	nahrávat
na	na	k7c4	na
válečky	váleček	k1gInPc4	váleček
jako	jako	k8xC	jako
historické	historický	k2eAgFnPc4d1	historická
nahrávky	nahrávka	k1gFnPc4	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Přednost	přednost	k1gFnSc1	přednost
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
dával	dávat	k5eAaImAgMnS	dávat
živému	živý	k2eAgNnSc3d1	živé
koncertnímu	koncertní	k2eAgNnSc3d1	koncertní
vystoupení	vystoupení	k1gNnSc3	vystoupení
<g/>
,	,	kIx,	,
odmítal	odmítat	k5eAaImAgMnS	odmítat
totiž	totiž	k9	totiž
rozhlasová	rozhlasový	k2eAgNnPc4d1	rozhlasové
vysílání	vysílání	k1gNnPc4	vysílání
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
špatnou	špatný	k2eAgFnSc4d1	špatná
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těm	ten	k3xDgMnPc3	ten
málo	málo	k6eAd1	málo
zvukovým	zvukový	k2eAgFnPc3d1	zvuková
nahrávkám	nahrávka	k1gFnPc3	nahrávka
však	však	k8xC	však
i	i	k9	i
dnes	dnes	k6eAd1	dnes
můžeme	moct	k5eAaImIp1nP	moct
obdivovat	obdivovat	k5eAaImF	obdivovat
jeho	jeho	k3xOp3gInSc4	jeho
osobitý	osobitý	k2eAgInSc4d1	osobitý
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
označovaný	označovaný	k2eAgInSc4d1	označovaný
za	za	k7c4	za
naprosto	naprosto	k6eAd1	naprosto
originální	originální	k2eAgInPc4d1	originální
a	a	k8xC	a
také	také	k9	také
avantgardní	avantgardní	k2eAgFnSc1d1	avantgardní
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
umělecké	umělecký	k2eAgFnSc6d1	umělecká
síle	síla	k1gFnSc6	síla
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dílo	dílo	k1gNnSc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
1	[number]	k4	1
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
fis-moll	fisolla	k1gFnPc2	fis-molla
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
Dva	dva	k4xCgInPc1	dva
kusy	kus	k1gInPc1	kus
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
:	:	kIx,	:
Pět	pět	k4xCc1	pět
Morceaux	Morceaux	k1gInSc4	Morceaux
de	de	k?	de
Fantaisie	Fantaisie	k1gFnSc2	Fantaisie
(	(	kIx(	(
<g/>
Fantastických	fantastický	k2eAgInPc2d1	fantastický
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
:	:	kIx,	:
Šest	šest	k4xCc1	šest
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
:	:	kIx,	:
Fantaisie-tableaux	Fantaisieableaux	k1gInSc4	Fantaisie-tableaux
(	(	kIx(	(
<g/>
Fantastické	fantastický	k2eAgInPc4d1	fantastický
obrazy	obraz	k1gInPc4	obraz
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
:	:	kIx,	:
Dva	dva	k4xCgInPc1	dva
kusy	kus	k1gInPc1	kus
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
:	:	kIx,	:
Fantazie	fantazie	k1gFnSc2	fantazie
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
Skály	skála	k1gFnSc2	skála
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Lermontova	Lermontův	k2eAgMnSc2d1	Lermontův
<g/>
)	)	kIx)	)
h-moll	holl	k1gInSc1	h-moll
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
:	:	kIx,	:
Šest	šest	k4xCc1	šest
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
:	:	kIx,	:
Trio	trio	k1gNnSc1	trio
élégiaque	élégiaqu	k1gFnSc2	élégiaqu
(	(	kIx(	(
<g/>
Elegické	elegický	k2eAgNnSc1d1	elegické
trio	trio	k1gNnSc1	trio
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
d-moll	dolla	k1gFnPc2	d-molla
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
:	:	kIx,	:
Sedm	sedm	k4xCc1	sedm
Morceaux	Morceaux	k1gInSc4	Morceaux
de	de	k?	de
Salon	salon	k1gInSc1	salon
(	(	kIx(	(
<g/>
Salónních	salónní	k2eAgInPc2d1	salónní
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
/	/	kIx~	/
<g/>
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
:	:	kIx,	:
Šest	šest	k4xCc1	šest
kusů	kus	k1gInPc2	kus
pro	pro	k7c4	pro
čtyřruční	čtyřruční	k2eAgInSc4d1	čtyřruční
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
:	:	kIx,	:
Capriccio	capriccio	k1gNnSc1	capriccio
na	na	k7c4	na
cikánské	cikánský	k2eAgNnSc4d1	cikánské
téma	téma	k1gNnSc4	téma
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
h-moll	holla	k1gFnPc2	h-molla
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
:	:	kIx,	:
Symfonie	symfonie	k1gFnSc1	symfonie
č.	č.	k?	č.
1	[number]	k4	1
d-moll	dolla	k1gFnPc2	d-molla
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
:	:	kIx,	:
Dvanáct	dvanáct	k4xCc1	dvanáct
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
:	:	kIx,	:
Šest	šest	k4xCc4	šest
ženských	ženský	k2eAgInPc2d1	ženský
nebo	nebo	k8xC	nebo
dětských	dětský	k2eAgInPc2d1	dětský
sborů	sbor	k1gInPc2	sbor
s	s	k7c7	s
klavírním	klavírní	k2eAgInSc7d1	klavírní
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
:	:	kIx,	:
Šest	šest	k4xCc1	šest
Moments	Momentsa	k1gFnPc2	Momentsa
Musicaux	Musicaux	k1gInSc1	Musicaux
(	(	kIx(	(
<g/>
Hudebních	hudební	k2eAgInPc2d1	hudební
momentů	moment	k1gInPc2	moment
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
:	:	kIx,	:
Suita	suita	k1gFnSc1	suita
č.	č.	k?	č.
2	[number]	k4	2
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
/	/	kIx~	/
<g/>
1901	[number]	k4	1901
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
2	[number]	k4	2
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
c-moll	colla	k1gFnPc2	c-molla
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
/	/	kIx~	/
<g/>
1901	[number]	k4	1901
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
:	:	kIx,	:
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
g-moll	golla	k1gFnPc2	g-molla
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
:	:	kIx,	:
Vesna	Vesna	k1gFnSc1	Vesna
(	(	kIx(	(
<g/>
Jaro	jaro	k1gNnSc1	jaro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kantáta	kantáta	k1gFnSc1	kantáta
pro	pro	k7c4	pro
baryton	baryton	k1gInSc4	baryton
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
na	na	k7c4	na
verše	verš	k1gInPc4	verš
N.	N.	kA	N.
A.	A.	kA	A.
Někrasova	Někrasův	k2eAgMnSc2d1	Někrasův
E-dur	Eur	k1gMnSc1	E-dur
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
:	:	kIx,	:
Dvanáct	dvanáct	k4xCc1	dvanáct
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
:	:	kIx,	:
22	[number]	k4	22
variací	variace	k1gFnPc2	variace
na	na	k7c4	na
Chopinovo	Chopinův	k2eAgNnSc4d1	Chopinovo
téma	téma	k1gNnSc4	téma
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
c-moll	colla	k1gFnPc2	c-molla
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
/	/	kIx~	/
<g/>
1903	[number]	k4	1903
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
:	:	kIx,	:
Deset	deset	k4xCc1	deset
preludií	preludie	k1gFnPc2	preludie
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
:	:	kIx,	:
Skupoj	Skupoj	k1gInSc1	Skupoj
rycar	rycar	k1gMnSc1	rycar
(	(	kIx(	(
<g/>
Skoupý	Skoupý	k1gMnSc1	Skoupý
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktová	jednoaktový	k2eAgFnSc1d1	jednoaktová
opera	opera	k1gFnSc1	opera
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
tragédie	tragédie	k1gFnSc2	tragédie
A.	A.	kA	A.
S.	S.	kA	S.
Puškina	Puškina	k1gMnSc1	Puškina
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
/	/	kIx~	/
<g/>
1904	[number]	k4	1904
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
:	:	kIx,	:
Francesca	Francesc	k1gInSc2	Francesc
da	da	k?	da
Rimini	Rimin	k1gMnPc1	Rimin
<g/>
,	,	kIx,	,
jednoaktová	jednoaktový	k2eAgFnSc1d1	jednoaktová
opera	opera	k1gFnSc1	opera
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Božské	božský	k2eAgFnSc2d1	božská
komedie	komedie	k1gFnSc2	komedie
Danta	Dante	k1gMnSc2	Dante
Alighieriho	Alighieri	k1gMnSc2	Alighieri
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
/	/	kIx~	/
<g/>
1905	[number]	k4	1905
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
:	:	kIx,	:
Patnáct	patnáct	k4xCc1	patnáct
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
:	:	kIx,	:
Symfonie	symfonie	k1gFnSc1	symfonie
č.	č.	k?	č.
2	[number]	k4	2
e-moll	eolla	k1gFnPc2	e-molla
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
/	/	kIx~	/
<g/>
1907	[number]	k4	1907
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
:	:	kIx,	:
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
č.	č.	k?	č.
1	[number]	k4	1
d-moll	dolla	k1gFnPc2	d-molla
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
:	:	kIx,	:
Ostrov	ostrov	k1gInSc1	ostrov
mertvych	mertvych	k1gInSc1	mertvych
(	(	kIx(	(
<g/>
Ostrov	ostrov	k1gInSc1	ostrov
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
a-moll	aolla	k1gFnPc2	a-molla
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
obrazu	obraz	k1gInSc2	obraz
Arnolda	Arnold	k1gMnSc2	Arnold
Böcklina	Böcklin	k2eAgMnSc2d1	Böcklin
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
č.	č.	k?	č.
3	[number]	k4	3
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
d-moll	dolla	k1gFnPc2	d-molla
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
:	:	kIx,	:
Liturgija	Liturgij	k2eAgFnSc1d1	Liturgij
Ioanna	Ioanen	k2eAgFnSc1d1	Ioanna
Zlatousta	Zlatousta	k1gFnSc1	Zlatousta
(	(	kIx(	(
<g/>
Liturgie	liturgie	k1gFnSc1	liturgie
Jana	Jan	k1gMnSc2	Jan
Zlatoústého	zlatoústý	k2eAgMnSc2d1	zlatoústý
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
capella	capella	k6eAd1	capella
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
32	[number]	k4	32
<g/>
:	:	kIx,	:
Třináct	třináct	k4xCc1	třináct
preludií	preludie	k1gFnPc2	preludie
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
33	[number]	k4	33
<g/>
:	:	kIx,	:
Šest	šest	k4xCc1	šest
Études	Étudesa	k1gFnPc2	Étudesa
tableaux	tableaux	k1gInSc1	tableaux
(	(	kIx(	(
<g/>
Etud-obrazů	Etudbraz	k1gMnPc2	Etud-obraz
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
34	[number]	k4	34
<g/>
:	:	kIx,	:
Čtrnáct	čtrnáct	k4xCc4	čtrnáct
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
35	[number]	k4	35
<g/>
:	:	kIx,	:
Kolokola	Kolokola	k1gFnSc1	Kolokola
(	(	kIx(	(
<g/>
Zvony	zvon	k1gInPc1	zvon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
pro	pro	k7c4	pro
sólisty	sólista	k1gMnPc4	sólista
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
na	na	k7c4	na
verše	verš	k1gInPc4	verš
E.	E.	kA	E.
A.	A.	kA	A.
Poea	Poea	k1gMnSc1	Poea
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
36	[number]	k4	36
<g/>
:	:	kIx,	:
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
č.	č.	k?	č.
2	[number]	k4	2
b-moll	bolla	k1gFnPc2	b-molla
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
redakce	redakce	k1gFnSc1	redakce
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
37	[number]	k4	37
<g/>
:	:	kIx,	:
Vsenočnoe	Vsenočnoe	k1gFnSc1	Vsenočnoe
bdenie	bdenie	k1gFnSc1	bdenie
(	(	kIx(	(
<g/>
Vigilie	vigilie	k1gFnSc1	vigilie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úprava	úprava	k1gFnSc1	úprava
pravoslavných	pravoslavný	k2eAgInPc2d1	pravoslavný
liturgických	liturgický	k2eAgInPc2d1	liturgický
textů	text	k1gInPc2	text
pro	pro	k7c4	pro
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
capella	capella	k6eAd1	capella
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
38	[number]	k4	38
<g/>
:	:	kIx,	:
Šest	šest	k4xCc1	šest
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
39	[number]	k4	39
<g/>
:	:	kIx,	:
Devět	devět	k4xCc1	devět
Études	Étudesa	k1gFnPc2	Étudesa
tableaux	tableaux	k1gInSc4	tableaux
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
<g/>
/	/	kIx~	/
<g/>
1917	[number]	k4	1917
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
:	:	kIx,	:
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
č.	č.	k?	č.
4	[number]	k4	4
g-moll	golla	k1gFnPc2	g-molla
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
redakce	redakce	k1gFnSc1	redakce
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
41	[number]	k4	41
<g/>
:	:	kIx,	:
Tři	tři	k4xCgFnPc1	tři
ruské	ruský	k2eAgFnPc1d1	ruská
písně	píseň	k1gFnPc1	píseň
pro	pro	k7c4	pro
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
42	[number]	k4	42
<g/>
:	:	kIx,	:
Variace	variace	k1gFnPc1	variace
na	na	k7c4	na
Correliho	Correli	k1gMnSc4	Correli
téma	téma	k1gFnSc1	téma
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
d-moll	dolla	k1gFnPc2	d-molla
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
43	[number]	k4	43
<g/>
:	:	kIx,	:
Rapsodie	rapsodie	k1gFnSc1	rapsodie
na	na	k7c4	na
Paganiniho	Paganini	k1gMnSc4	Paganini
téma	téma	k1gFnSc1	téma
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
a-moll	aolla	k1gFnPc2	a-molla
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
44	[number]	k4	44
<g/>
:	:	kIx,	:
Symfonie	symfonie	k1gFnSc1	symfonie
č.	č.	k?	č.
3	[number]	k4	3
a-moll	aolla	k1gFnPc2	a-molla
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
/	/	kIx~	/
<g/>
1936	[number]	k4	1936
</s>
</p>
<p>
<s>
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
45	[number]	k4	45
<g/>
:	:	kIx,	:
Symfonické	symfonický	k2eAgInPc4d1	symfonický
tance	tanec	k1gInPc4	tanec
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
Seznam	seznam	k1gInSc1	seznam
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
autor	autor	k1gMnSc1	autor
přidělil	přidělit	k5eAaPmAgMnS	přidělit
opusové	opusový	k2eAgNnSc4d1	opusové
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
ně	on	k3xPp3gInPc4	on
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
za	za	k7c2	za
skladatelova	skladatelův	k2eAgInSc2d1	skladatelův
života	život	k1gInSc2	život
ještě	ještě	k9	ještě
deset	deset	k4xCc1	deset
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
opusové	opusový	k2eAgNnSc1d1	opusové
číslo	číslo	k1gNnSc1	číslo
nepřidělil	přidělit	k5eNaPmAgMnS	přidělit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rachmaninově	Rachmaninově	k1gFnSc6	Rachmaninově
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
řada	řada	k1gFnSc1	řada
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BIESOLD	BIESOLD	kA	BIESOLD
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
<g/>
:	:	kIx,	:
Sergej	Sergej	k1gMnSc1	Sergej
Rachmaninoff	Rachmaninoff	k1gMnSc1	Rachmaninoff
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Zwischen	Zwischen	k1gInSc1	Zwischen
Moskau	Moskaus	k1gInSc2	Moskaus
und	und	k?	und
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GAMMOND	GAMMOND	kA	GAMMOND
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
:	:	kIx,	:
Velcí	velký	k2eAgMnPc1d1	velký
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Magdalena	Magdalena	k1gFnSc1	Magdalena
Pechová	Pechová	k1gFnSc1	Pechová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
SVOJTKA	SVOJTKA	kA	SVOJTKA
a	a	k8xC	a
VAŠUT	VAŠUT	kA	VAŠUT
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOPKINS	HOPKINS	kA	HOPKINS
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc1	Anthon
<g/>
:	:	kIx,	:
Velikáni	velikán	k1gMnPc1	velikán
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgInS	přeložit
kol	kol	k7c2	kol
<g/>
.	.	kIx.	.
<g/>
překladatelů	překladatel	k1gMnPc2	překladatel
agentury	agentura	k1gFnSc2	agentura
ALEA	ALEA	kA	ALEA
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
PERFEKT	perfektum	k1gNnPc2	perfektum
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JUNGHEINRICH	JUNGHEINRICH	kA	JUNGHEINRICH
<g/>
,	,	kIx,	,
Hans-Klaus	Hans-Klaus	k1gMnSc1	Hans-Klaus
<g/>
:	:	kIx,	:
Toteninsel	Toteninsel	k1gMnSc1	Toteninsel
als	als	k?	als
Ziel	Ziel	k1gMnSc1	Ziel
und	und	k?	und
Ausgangspunkt	Ausgangspunkt	k1gInSc1	Ausgangspunkt
<g/>
.	.	kIx.	.
</s>
<s>
Zur	Zur	k?	Zur
künstlerischen	künstlerischna	k1gFnPc2	künstlerischna
Physiognomie	Physiognomie	k1gFnSc1	Physiognomie
von	von	k1gInSc1	von
Sergej	Sergej	k1gMnSc1	Sergej
Rachmaninoff	Rachmaninoff	k1gMnSc1	Rachmaninoff
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Piano-Jahrbuch	Piano-Jahrbuch	k1gMnSc1	Piano-Jahrbuch
<g/>
,	,	kIx,	,
Recklinghausen	Recklinghausen	k2eAgMnSc1d1	Recklinghausen
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
s.	s.	k?	s.
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PIGGOTT	PIGGOTT	kA	PIGGOTT
<g/>
,	,	kIx,	,
Patrick	Patrick	k1gMnSc1	Patrick
<g/>
:	:	kIx,	:
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
orchestral	orchestrat	k5eAaPmAgMnS	orchestrat
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Washington	Washington	k1gInSc1	Washington
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SWAN	SWAN	kA	SWAN
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
–	–	k?	–
SWAN	SWAN	kA	SWAN
<g/>
,	,	kIx,	,
Katherine	Katherin	k1gInSc5	Katherin
<g/>
:	:	kIx,	:
Rachmaninoff	Rachmaninoff	k1gMnSc1	Rachmaninoff
<g/>
:	:	kIx,	:
Personal	Personal	k1gMnSc1	Personal
Reminiscences	Reminiscences	k1gMnSc1	Reminiscences
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Musical	musical	k1gInSc1	musical
Quarterly	Quarterla	k1gFnSc2	Quarterla
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
s.	s.	k?	s.
174	[number]	k4	174
<g/>
–	–	k?	–
<g/>
191	[number]	k4	191
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
</s>
</p>
<p>
<s>
Romantismus	romantismus	k1gInSc1	romantismus
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Symfonie	symfonie	k1gFnSc1	symfonie
</s>
</p>
<p>
<s>
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Iljin	Iljin	k2eAgMnSc1d1	Iljin
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
+	+	kIx~	+
fotografie	fotografia	k1gFnPc1	fotografia
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
Jarmily	Jarmila	k1gFnSc2	Jarmila
Gabrielové	Gabrielová	k1gFnSc2	Gabrielová
v	v	k7c6	v
Harmonii	harmonie	k1gFnSc6	harmonie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
Petra	Petra	k1gFnSc1	Petra
Feldmanna	Feldmanen	k2eAgFnSc1d1	Feldmanen
Forma	forma	k1gFnSc1	forma
preludia	preludium	k1gNnSc2	preludium
v	v	k7c6	v
klavírní	klavírní	k2eAgFnSc6d1	klavírní
tvorbě	tvorba	k1gFnSc6	tvorba
Sergeje	Sergej	k1gMnSc4	Sergej
Vasiljeviče	Vasiljevič	k1gMnSc4	Vasiljevič
Rachmaninova	Rachmaninův	k2eAgMnSc4d1	Rachmaninův
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
obhájená	obhájená	k1gFnSc1	obhájená
na	na	k7c6	na
JAMU	jam	k1gInSc6	jam
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
na	na	k7c6	na
Britannice	Britannika	k1gFnSc6	Britannika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
hraje	hrát	k5eAaImIp3nS	hrát
Preludium	preludium	k1gNnSc4	preludium
g-moll	goll	k1gMnSc1	g-moll
Op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
23	[number]	k4	23
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sergej	Sergej	k1gMnSc1	Sergej
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Sergej	Sergej	k1gMnSc1	Sergej
Rachmaninov	Rachmaninov	k1gInSc4	Rachmaninov
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaBmAgMnS	dít
od	od	k7c2	od
S.	S.	kA	S.
Rachmaninova	Rachmaninův	k2eAgFnSc1d1	Rachmaninova
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
