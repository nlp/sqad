<s>
Podmíněné	podmíněný	k2eAgFnPc1d1	podmíněná
finanční	finanční	k2eAgFnPc1d1	finanční
dávky	dávka	k1gFnPc1	dávka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Conditional	Conditional	k1gMnSc1	Conditional
cash	cash	k1gFnSc2	cash
transfers	transfers	k1gInSc1	transfers
–	–	k?	–
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
snížit	snížit	k5eAaPmF	snížit
chudobu	chudoba	k1gFnSc4	chudoba
<g/>
.	.	kIx.	.
</s>
