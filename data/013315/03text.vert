<p>
<s>
Podmíněné	podmíněný	k2eAgFnPc1d1	podmíněná
finanční	finanční	k2eAgFnPc1d1	finanční
dávky	dávka	k1gFnPc1	dávka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Conditional	Conditional	k1gMnSc1	Conditional
cash	cash	k1gFnSc2	cash
transfers	transfers	k1gInSc1	transfers
–	–	k?	–
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
snížit	snížit	k5eAaPmF	snížit
chudobu	chudoba	k1gFnSc4	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
či	či	k8xC	či
charity	charita	k1gFnPc1	charita
pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gMnPc2	on
přerozdělují	přerozdělovat	k5eAaImIp3nP	přerozdělovat
peníze	peníz	k1gInPc4	peníz
chudým	chudý	k2eAgFnPc3d1	chudá
domácnostem	domácnost	k1gFnPc3	domácnost
z	z	k7c2	z
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
musejí	muset	k5eAaImIp3nP	muset
domácnosti	domácnost	k1gFnPc1	domácnost
zapojené	zapojený	k2eAgFnPc1d1	zapojená
do	do	k7c2	do
programu	program	k1gInSc2	program
splnit	splnit	k5eAaPmF	splnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomoc	pomoc	k1gFnSc4	pomoc
dostaly	dostat	k5eAaPmAgInP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
bývá	bývat	k5eAaImIp3nS	bývat
z	z	k7c2	z
pravidla	pravidlo	k1gNnSc2	pravidlo
posílání	posílání	k1gNnSc2	posílání
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
kontroly	kontrola	k1gFnPc4	kontrola
u	u	k7c2	u
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
očkování	očkování	k1gNnSc1	očkování
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
rady	rada	k1gFnSc2	rada
svobodným	svobodný	k2eAgFnPc3d1	svobodná
matkám	matka	k1gFnPc3	matka
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
tento	tento	k3xDgInSc4	tento
instrument	instrument	k1gInSc4	instrument
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
roky	rok	k1gInPc4	rok
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
</s>
</p>
<p>
<s>
==	==	k?	==
Programy	program	k1gInPc1	program
==	==	k?	==
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
–	–	k?	–
Bolsa	Bolsa	k1gFnSc1	Bolsa
Família	Família	k1gFnSc1	Família
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bolsa	Bolsa	k1gFnSc1	Bolsa
Escola	Escola	k1gFnSc1	Escola
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mexiko	Mexiko	k1gNnSc4	Mexiko
–	–	k?	–
Oportunidades	Oportunidades	k1gInSc1	Oportunidades
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
-	-	kIx~	-
Bono	bona	k1gFnSc5	bona
de	de	k?	de
Desarrollo	Desarrollo	k1gNnSc1	Desarrollo
Humano	Humana	k1gFnSc5	Humana
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
–	–	k?	–
Familias	Familias	k1gInSc1	Familias
en	en	k?	en
Acción	Acción	k1gInSc1	Acción
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Subsido	Subsida	k1gFnSc5	Subsida
Condicionado	Condicionada	k1gFnSc5	Condicionada
a	a	k8xC	a
la	la	k1gNnSc4	la
Asistencia	Asistencius	k1gMnSc2	Asistencius
Escolar	Escolar	k1gMnSc1	Escolar
Bogota	Bogot	k1gMnSc2	Bogot
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jamaika	Jamaika	k1gFnSc1	Jamaika
–	–	k?	–
Program	program	k1gInSc1	program
of	of	k?	of
Advancement	Advancement	k1gInSc1	Advancement
through	through	k1gMnSc1	through
Health	Health	k1gMnSc1	Health
and	and	k?	and
Education	Education	k1gInSc1	Education
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Indonésia	Indonésia	k1gFnSc1	Indonésia
–	–	k?	–
Jaring	Jaring	k1gInSc1	Jaring
Pangamanan	Pangamanan	k1gMnSc1	Pangamanan
Sosial	Sosial	k1gInSc1	Sosial
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chile	Chile	k1gNnSc1	Chile
–	–	k?	–
Solidario	Solidario	k1gNnSc1	Solidario
<g/>
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Turecko	Turecko	k1gNnSc4	Turecko
–	–	k?	–
Social	Social	k1gInSc1	Social
Risk	risk	k1gInSc1	risk
Mitigation	Mitigation	k1gInSc1	Mitigation
Project	Project	k1gInSc1	Project
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bangladesh	Bangladesh	k1gInSc1	Bangladesh
–	–	k?	–
Female	Femala	k1gFnSc3	Femala
Secondary	Secondara	k1gFnSc2	Secondara
School	Schoola	k1gFnPc2	Schoola
Assistance	Assistance	k1gFnSc2	Assistance
Program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kambodža	Kambodža	k1gFnSc1	Kambodža
–	–	k?	–
Japan	japan	k1gInSc4	japan
Fund	fund	k1gInSc4	fund
for	forum	k1gNnPc2	forum
Poverty	Povert	k1gInPc4	Povert
Reduction	Reduction	k1gInSc1	Reduction
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Education	Education	k1gInSc4	Education
for	forum	k1gNnPc2	forum
support	support	k1gInSc4	support
project	project	k5eAaPmF	project
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jemen	Jemen	k1gInSc1	Jemen
–	–	k?	–
Basic	Basic	kA	Basic
Education	Education	k1gInSc1	Education
Development	Development	k1gInSc1	Development
project	project	k1gInSc1	project
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Honduras	Honduras	k1gInSc1	Honduras
–	–	k?	–
Programa	Program	k1gMnSc2	Program
de	de	k?	de
Asignación	Asignación	k1gMnSc1	Asignación
Familiar	Familiar	k1gMnSc1	Familiar
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Keňa	Keňa	k1gFnSc1	Keňa
–	–	k?	–
Cash	cash	k1gFnSc4	cash
Transfers	Transfersa	k1gFnPc2	Transfersa
for	forum	k1gNnPc2	forum
Orphans	Orphans	k1gInSc1	Orphans
and	and	k?	and
Vulnerable	Vulnerable	k1gFnSc2	Vulnerable
Children	Childrna	k1gFnPc2	Childrna
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
–	–	k?	–
Atención	Atención	k1gInSc1	Atención
a	a	k8xC	a
Crisis	Crisis	k1gInSc1	Crisis
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Red	Red	k1gMnSc1	Red
de	de	k?	de
Protección	Protección	k1gMnSc1	Protección
Social	Social	k1gMnSc1	Social
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pákistán	Pákistán	k1gInSc1	Pákistán
–	–	k?	–
Punjab	Punjab	k1gInSc1	Punjab
Education	Education	k1gInSc1	Education
Sector	Sector	k1gInSc1	Sector
Reform	Reform	k1gInSc1	Reform
Program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Bolsa	Bols	k1gMnSc4	Bols
Família	Famílius	k1gMnSc4	Famílius
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
projektem	projekt	k1gInSc7	projekt
je	být	k5eAaImIp3nS	být
Bolsa	Bolsa	k1gFnSc1	Bolsa
Família	Família	k1gFnSc1	Família
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgFnSc1d1	probíhající
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
prováděn	provádět	k5eAaImNgInS	provádět
brazilskou	brazilský	k2eAgFnSc7d1	brazilská
vládou	vláda	k1gFnSc7	vláda
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
milionům	milion	k4xCgInPc3	milion
rodinám	rodina	k1gFnPc3	rodina
(	(	kIx(	(
<g/>
46	[number]	k4	46
milionům	milion	k4xCgInPc3	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
chudých	chudý	k2eAgMnPc2d1	chudý
obyvatel	obyvatel	k1gMnPc2	obyvatel
Brazílie	Brazílie	k1gFnSc2	Brazílie
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
programy	program	k1gInPc4	program
s	s	k7c7	s
podmíněnými	podmíněný	k2eAgFnPc7d1	podmíněná
finančními	finanční	k2eAgFnPc7d1	finanční
dávkami	dávka	k1gFnPc7	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Rodiny	rodina	k1gFnPc1	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
dostávají	dostávat	k5eAaImIp3nP	dostávat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
70	[number]	k4	70
Reálů	reál	k1gInPc2	reál
(	(	kIx(	(
<g/>
35	[number]	k4	35
$	$	kIx~	$
<g/>
)	)	kIx)	)
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
zapsání	zapsání	k1gNnSc4	zapsání
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
prohlídky	prohlídka	k1gFnPc1	prohlídka
u	u	k7c2	u
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
celkové	celkový	k2eAgFnSc2d1	celková
chudoby	chudoba	k1gFnSc2	chudoba
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
signifikantní	signifikantní	k2eAgFnSc7d1	signifikantní
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
celé	celý	k2eAgFnSc2d1	celá
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vzoru	vzor	k1gInSc6	vzor
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
City	City	k1gFnSc2	City
obdobný	obdobný	k2eAgInSc4d1	obdobný
program	program	k1gInSc4	program
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Opportunity	Opportunita	k1gFnSc2	Opportunita
NYC	NYC	kA	NYC
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
příklad	příklad	k1gInSc1	příklad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
země	země	k1gFnSc1	země
poučila	poučit	k5eAaPmAgFnS	poučit
ze	z	k7c2	z
zkušeností	zkušenost	k1gFnPc2	zkušenost
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oportunidades	Oportunidades	k1gInSc4	Oportunidades
===	===	k?	===
</s>
</p>
<p>
<s>
Obdoba	obdoba	k1gFnSc1	obdoba
brazilského	brazilský	k2eAgInSc2d1	brazilský
projektu	projekt	k1gInSc2	projekt
Bolsa	Bolsa	k1gFnSc1	Bolsa
Família	Família	k1gFnSc1	Família
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgFnSc1d1	probíhající
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
s	s	k7c7	s
názvem	název	k1gInSc7	název
Progresa	Progresa	k1gFnSc1	Progresa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prováděn	prováděn	k2eAgInSc1d1	prováděn
mexickou	mexický	k2eAgFnSc7d1	mexická
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
snižování	snižování	k1gNnSc4	snižování
chudoby	chudoba	k1gFnSc2	chudoba
rodin	rodina	k1gFnPc2	rodina
v	v	k7c6	v
rurálních	rurální	k2eAgFnPc6d1	rurální
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
městských	městský	k2eAgFnPc6d1	městská
komunitách	komunita	k1gFnPc6	komunita
investicemi	investice	k1gFnPc7	investice
do	do	k7c2	do
rozvoje	rozvoj	k1gInSc2	rozvoj
lidského	lidský	k2eAgInSc2d1	lidský
kapitálu	kapitál	k1gInSc2	kapitál
podmíněnými	podmíněný	k2eAgFnPc7d1	podmíněná
finančními	finanční	k2eAgFnPc7d1	finanční
dávkami	dávka	k1gFnPc7	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
rodin	rodina	k1gFnPc2	rodina
benefitujích	benefitují	k1gNnPc6	benefitují
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
4	[number]	k4	4
miliony	milion	k4xCgInPc7	milion
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
v	v	k7c6	v
rurálních	rurální	k2eAgFnPc6d1	rurální
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
v	v	k7c6	v
urbánních	urbánní	k2eAgFnPc6d1	urbánní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
vláda	vláda	k1gFnSc1	vláda
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
investuje	investovat	k5eAaBmIp3nS	investovat
46,5	[number]	k4	46,5
%	%	kIx~	%
federálního	federální	k2eAgInSc2d1	federální
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
určeného	určený	k2eAgInSc2d1	určený
na	na	k7c6	na
snížení	snížení	k1gNnSc6	snížení
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
odůvodnění	odůvodnění	k1gNnSc1	odůvodnění
přímého	přímý	k2eAgNnSc2d1	přímé
přerozdělování	přerozdělování	k1gNnSc2	přerozdělování
finančních	finanční	k2eAgFnPc2d1	finanční
dávek	dávka	k1gFnPc2	dávka
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
podstatné	podstatný	k2eAgInPc1d1	podstatný
argumenty	argument	k1gInPc1	argument
pro	pro	k7c4	pro
přímé	přímý	k2eAgNnSc4d1	přímé
přerozdělování	přerozdělování	k1gNnSc4	přerozdělování
veřejných	veřejný	k2eAgInPc2d1	veřejný
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
v	v	k7c6	v
chudých	chudý	k2eAgFnPc6d1	chudá
zemích	zem	k1gFnPc6	zem
často	často	k6eAd1	často
nemají	mít	k5eNaImIp3nP	mít
přímý	přímý	k2eAgInSc4d1	přímý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
chudé	chudý	k2eAgMnPc4d1	chudý
obyvatele	obyvatel	k1gMnPc4	obyvatel
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
v	v	k7c6	v
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
mělo	mít	k5eAaImAgNnS	mít
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
<g/>
%	%	kIx~	%
dolní	dolní	k2eAgFnSc1d1	dolní
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
obyvatel	obyvatel	k1gMnPc2	obyvatel
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
elektrické	elektrický	k2eAgFnSc3d1	elektrická
energii	energie	k1gFnSc3	energie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
90	[number]	k4	90
<g/>
%	%	kIx~	%
horní	horní	k2eAgFnSc1d1	horní
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nestálost	nestálost	k1gFnSc1	nestálost
příjmů	příjem	k1gInPc2	příjem
během	během	k7c2	během
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
argument	argument	k1gInSc1	argument
pro	pro	k7c4	pro
cílené	cílený	k2eAgNnSc4d1	cílené
přerozdělování	přerozdělování	k1gNnSc4	přerozdělování
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
může	moct	k5eAaImIp3nS	moct
zlepšit	zlepšit	k5eAaPmF	zlepšit
situaci	situace	k1gFnSc4	situace
obyvatel	obyvatel	k1gMnPc2	obyvatel
při	při	k7c6	při
výpadku	výpadek	k1gInSc6	výpadek
příjmu	příjem	k1gInSc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
jedinců	jedinec	k1gMnPc2	jedinec
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dědičné	dědičný	k2eAgNnSc4d1	dědičné
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
onoho	onen	k3xDgMnSc2	onen
jedince	jedinec	k1gMnSc2	jedinec
(	(	kIx(	(
<g/>
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
rase	rasa	k1gFnSc3	rasa
<g/>
,	,	kIx,	,
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
rodinné	rodinný	k2eAgNnSc4d1	rodinné
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodným	vhodný	k2eAgInSc7d1	vhodný
nástrojem	nástroj	k1gInSc7	nástroj
přímého	přímý	k2eAgNnSc2d1	přímé
přerozdělování	přerozdělování	k1gNnSc2	přerozdělování
prostředků	prostředek	k1gInPc2	prostředek
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
finanční	finanční	k2eAgFnPc4d1	finanční
dávky	dávka	k1gFnPc4	dávka
(	(	kIx(	(
<g/>
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
nerovné	rovný	k2eNgFnPc1d1	nerovná
příležitosti	příležitost	k1gFnPc1	příležitost
rodinám	rodina	k1gFnPc3	rodina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tímto	tento	k3xDgNnSc7	tento
strádají	strádat	k5eAaImIp3nP	strádat
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
také	také	k9	také
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
argumenty	argument	k1gInPc4	argument
proti	proti	k7c3	proti
přímému	přímý	k2eAgNnSc3d1	přímé
přerozdělování	přerozdělování	k1gNnSc3	přerozdělování
finančních	finanční	k2eAgFnPc2d1	finanční
dávek	dávka	k1gFnPc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
chudoby	chudoba	k1gFnSc2	chudoba
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgInSc1d2	silnější
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
nepřímo	přímo	k6eNd1	přímo
přerozdělovat	přerozdělovat	k5eAaImF	přerozdělovat
prostředky	prostředek	k1gInPc4	prostředek
většími	veliký	k2eAgFnPc7d2	veliký
investicemi	investice	k1gFnPc7	investice
do	do	k7c2	do
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
(	(	kIx(	(
<g/>
cesty	cesta	k1gFnPc1	cesta
<g/>
,	,	kIx,	,
přístavy	přístav	k1gInPc1	přístav
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
lepší	dobrý	k2eAgFnSc2d2	lepší
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
kliniky	klinika	k1gFnSc2	klinika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
špatný	špatný	k2eAgInSc4d1	špatný
stimul	stimul	k1gInSc4	stimul
svým	svůj	k3xOyFgMnPc3	svůj
příjemcům	příjemce	k1gMnPc3	příjemce
<g/>
.	.	kIx.	.
</s>
<s>
Mohly	moct	k5eAaImAgFnP	moct
by	by	kYmCp3nP	by
narušit	narušit	k5eAaPmF	narušit
budoucí	budoucí	k2eAgFnSc4d1	budoucí
efektivitu	efektivita	k1gFnSc4	efektivita
práce	práce	k1gFnSc2	práce
příjemců	příjemce	k1gMnPc2	příjemce
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Když	když	k8xS	když
stát	stát	k1gInSc1	stát
zajistí	zajistit	k5eAaPmIp3nS	zajistit
základní	základní	k2eAgFnPc4d1	základní
potřeby	potřeba	k1gFnPc4	potřeba
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
lidé	člověk	k1gMnPc1	člověk
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
tvrdých	tvrdý	k2eAgFnPc6d1	tvrdá
podmínkách	podmínka	k1gFnPc6	podmínka
za	za	k7c4	za
málo	málo	k4c4	málo
peněz	peníze	k1gInPc2	peníze
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
rysy	rys	k1gInPc1	rys
a	a	k8xC	a
implementace	implementace	k1gFnPc1	implementace
programů	program	k1gInPc2	program
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tyto	tento	k3xDgInPc1	tento
programy	program	k1gInPc1	program
fungují	fungovat	k5eAaImIp3nP	fungovat
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
stejné	stejný	k2eAgInPc4d1	stejný
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kterékoliv	kterýkoliv	k3yIgInPc4	kterýkoliv
jiné	jiný	k2eAgInPc4d1	jiný
přerozdělovací	přerozdělovací	k2eAgInPc4d1	přerozdělovací
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k6eAd1	potřeba
stanovit	stanovit	k5eAaPmF	stanovit
způsobilost	způsobilost	k1gFnSc4	způsobilost
příjemců	příjemce	k1gMnPc2	příjemce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
zapsat	zapsat	k5eAaPmF	zapsat
do	do	k7c2	do
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
způsoby	způsob	k1gInPc1	způsob
plateb	platba	k1gFnPc2	platba
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
monitoring	monitoring	k1gInSc4	monitoring
a	a	k8xC	a
evaluační	evaluační	k2eAgInSc4d1	evaluační
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nutná	nutný	k2eAgFnSc1d1	nutná
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
institucemi	instituce	k1gFnPc7	instituce
<g/>
.	.	kIx.	.
<g/>
Přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
zemí	zem	k1gFnPc2	zem
využívají	využívat	k5eAaPmIp3nP	využívat
geografické	geografický	k2eAgNnSc4d1	geografické
cílení	cílení	k1gNnSc4	cílení
dávek	dávka	k1gFnPc2	dávka
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
cílí	cílit	k5eAaImIp3nS	cílit
na	na	k7c6	na
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pomocí	pomocí	k7c2	pomocí
metody	metoda	k1gFnSc2	metoda
"	"	kIx"	"
<g/>
Proxy	Prox	k1gInPc1	Prox
Means	Meansa	k1gFnPc2	Meansa
Testing	Testing	k1gInSc1	Testing
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
statistické	statistický	k2eAgFnSc2d1	statistická
analýzy	analýza	k1gFnSc2	analýza
datového	datový	k2eAgInSc2d1	datový
souboru	soubor	k1gInSc2	soubor
průzkumu	průzkum	k1gInSc2	průzkum
domácností	domácnost	k1gFnPc2	domácnost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
určitých	určitý	k2eAgNnPc2d1	určité
kritérií	kritérion	k1gNnPc2	kritérion
jako	jako	k8xC	jako
lokace	lokace	k1gFnSc1	lokace
<g/>
,	,	kIx,	,
kvalita	kvalita	k1gFnSc1	kvalita
obydlí	obydlí	k1gNnSc2	obydlí
<g/>
,	,	kIx,	,
demografická	demografický	k2eAgFnSc1d1	demografická
struktura	struktura	k1gFnSc1	struktura
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc1	vzdělání
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
zemí	zem	k1gFnPc2	zem
používá	používat	k5eAaImIp3nS	používat
obě	dva	k4xCgFnPc1	dva
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
programů	program	k1gInPc2	program
také	také	k9	také
využívá	využívat	k5eAaImIp3nS	využívat
metodu	metoda	k1gFnSc4	metoda
"	"	kIx"	"
<g/>
Community-based	Communityased	k1gInSc1	Community-based
Targeting	Targeting	k1gInSc1	Targeting
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
skupiny	skupina	k1gFnSc2	skupina
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
komunit	komunita	k1gFnPc2	komunita
či	či	k8xC	či
jejich	jejich	k3xOp3gMnPc2	jejich
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
aktivity	aktivita	k1gFnPc1	aktivita
nejsou	být	k5eNaImIp3nP	být
vztaženy	vztažen	k2eAgFnPc1d1	vztažena
k	k	k7c3	k
fungování	fungování	k1gNnSc3	fungování
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
z	z	k7c2	z
daných	daný	k2eAgFnPc2d1	daná
komunit	komunita	k1gFnPc2	komunita
má	mít	k5eAaImIp3nS	mít
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
transparentnost	transparentnost	k1gFnSc1	transparentnost
<g/>
.	.	kIx.	.
</s>
<s>
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
byly	být	k5eAaImAgFnP	být
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
pro	pro	k7c4	pro
rozvojové	rozvojový	k2eAgFnPc4d1	rozvojová
země	zem	k1gFnPc4	zem
ve	v	k7c6	v
zlepšení	zlepšení	k1gNnSc6	zlepšení
mapování	mapování	k1gNnSc2	mapování
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
podporu	podpora	k1gFnSc4	podpora
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
Kambodža	Kambodža	k1gFnSc1	Kambodža
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Panama	panama	k1gNnSc1	panama
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Platby	platba	k1gFnPc1	platba
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
většinou	většina	k1gFnSc7	většina
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
programů	program	k1gInPc2	program
přerozděluje	přerozdělovat	k5eAaImIp3nS	přerozdělovat
benefity	benefit	k2eAgFnPc4d1	benefit
rodinám	rodina	k1gFnPc3	rodina
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
horní	horní	k2eAgInPc1d1	horní
limity	limit	k1gInPc1	limit
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
Bolsa	Bols	k1gMnSc2	Bols
Família	Famílius	k1gMnSc2	Famílius
45	[number]	k4	45
$	$	kIx~	$
-	-	kIx~	-
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
3	[number]	k4	3
dětí	dítě	k1gFnPc2	dítě
<g/>
;	;	kIx,	;
Mexiko	Mexiko	k1gNnSc1	Mexiko
153	[number]	k4	153
$	$	kIx~	$
-	-	kIx~	-
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
programů	program	k1gInPc2	program
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
<g/>
,	,	kIx,	,
Salvadoru	Salvador	k1gInSc6	Salvador
<g/>
,	,	kIx,	,
Panamě	Panama	k1gFnSc6	Panama
a	a	k8xC	a
Peru	Peru	k1gNnSc6	Peru
<g/>
)	)	kIx)	)
vyplácejí	vyplácet	k5eAaImIp3nP	vyplácet
rovný	rovný	k2eAgInSc4d1	rovný
příspěvek	příspěvek	k1gInSc4	příspěvek
domácnostem	domácnost	k1gFnPc3	domácnost
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
mají	mít	k5eAaImIp3nP	mít
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Platby	platba	k1gFnPc1	platba
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
či	či	k8xC	či
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Oportunidades	Oportunidades	k1gInSc1	Oportunidades
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Familias	Familias	k1gMnSc1	Familias
en	en	k?	en
Acción	Acción	k1gMnSc1	Acción
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Social	Social	k1gInSc1	Social
Mitigation	Mitigation	k1gInSc1	Mitigation
Project	Project	k1gInSc1	Project
(	(	kIx(	(
<g/>
SRMP	SRMP	kA	SRMP
<g/>
)	)	kIx)	)
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
PATH	PATH	kA	PATH
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
vyplácejí	vyplácet	k5eAaImIp3nP	vyplácet
větší	veliký	k2eAgInPc4d2	veliký
obnosy	obnos	k1gInPc4	obnos
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
stupni	stupeň	k1gInSc6	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
než	než	k8xS	než
na	na	k7c6	na
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
uvědomují	uvědomovat	k5eAaImIp3nP	uvědomovat
větší	veliký	k2eAgInPc4d2	veliký
výdaje	výdaj	k1gInPc4	výdaj
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
starších	starý	k2eAgMnPc2d2	starší
žáků	žák	k1gMnPc2	žák
(	(	kIx(	(
<g/>
dražší	drahý	k2eAgFnSc1d2	dražší
učebnice	učebnice	k1gFnPc1	učebnice
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc1	škola
jsou	být	k5eAaImIp3nP	být
vzdálenější	vzdálený	k2eAgMnSc1d2	vzdálenější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oportunidades	Oportunidades	k1gInSc1	Oportunidades
a	a	k8xC	a
SRMP	SRMP	kA	SRMP
vyplácejí	vyplácet	k5eAaImIp3nP	vyplácet
větší	veliký	k2eAgFnPc4d2	veliký
dávky	dávka	k1gFnPc4	dávka
dívkám	dívka	k1gFnPc3	dívka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
uvědomují	uvědomovat	k5eAaImIp3nP	uvědomovat
jejich	jejich	k3xOp3gFnSc4	jejich
znevýhodněnou	znevýhodněný	k2eAgFnSc4d1	znevýhodněná
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
<g/>
Způsoby	způsob	k1gInPc1	způsob
vyplácení	vyplácení	k1gNnSc1	vyplácení
dávek	dávka	k1gFnPc2	dávka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
program	program	k1gInSc1	program
od	od	k7c2	od
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Brazílie	Brazílie	k1gFnPc4	Brazílie
jsou	být	k5eAaImIp3nP	být
výplaty	výplata	k1gFnPc1	výplata
prováděné	prováděný	k2eAgFnPc1d1	prováděná
pomocí	pomocí	k7c2	pomocí
debetních	debetní	k2eAgFnPc2d1	debetní
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yQgFnPc2	který
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
peníze	peníz	k1gInPc4	peníz
v	v	k7c6	v
bankomatech	bankomat	k1gInPc6	bankomat
<g/>
,	,	kIx,	,
bankách	banka	k1gFnPc6	banka
apod.	apod.	kA	apod.
V	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
jsou	být	k5eAaImIp3nP	být
vypláceny	vyplácen	k2eAgInPc1d1	vyplácen
peníze	peníz	k1gInPc1	peníz
skrze	skrze	k?	skrze
státní	státní	k2eAgFnSc4d1	státní
banku	banka	k1gFnSc4	banka
hotově	hotově	k6eAd1	hotově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
v	v	k7c6	v
zaostalých	zaostalý	k2eAgFnPc6d1	zaostalá
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
peníze	peníz	k1gInPc1	peníz
rozváženy	rozvážen	k2eAgInPc1d1	rozvážen
a	a	k8xC	a
ve	v	k7c6	v
městech	město	k1gNnPc6	město
vypláceny	vyplácet	k5eAaImNgFnP	vyplácet
přes	přes	k7c4	přes
bankovní	bankovní	k2eAgInSc4d1	bankovní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
jsou	být	k5eAaImIp3nP	být
peníze	peníz	k1gInPc1	peníz
distribuovány	distribuován	k2eAgInPc1d1	distribuován
přes	přes	k7c4	přes
poštovní	poštovní	k2eAgFnPc4d1	poštovní
pobočky	pobočka	k1gFnPc4	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
jsou	být	k5eAaImIp3nP	být
peníze	peníz	k1gInPc1	peníz
posílány	posílán	k2eAgInPc1d1	posílán
zástupci	zástupce	k1gMnPc7	zástupce
komunit	komunita	k1gFnPc2	komunita
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
dále	daleko	k6eAd2	daleko
přerozdělí	přerozdělit	k5eAaPmIp3nS	přerozdělit
<g/>
.	.	kIx.	.
<g/>
Stanovení	stanovení	k1gNnSc1	stanovení
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
vyplácení	vyplácení	k1gNnSc4	vyplácení
dávek	dávka	k1gFnPc2	dávka
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
součástí	součást	k1gFnSc7	součást
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s.	s.	k?	s.
Většina	většina	k1gFnSc1	většina
programů	program	k1gInPc2	program
podmiňuje	podmiňovat	k5eAaImIp3nS	podmiňovat
vyplácení	vyplácení	k1gNnSc1	vyplácení
dávek	dávka	k1gFnPc2	dávka
zápisem	zápis	k1gInSc7	zápis
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
pravidelnými	pravidelný	k2eAgFnPc7d1	pravidelná
kontrolami	kontrola	k1gFnPc7	kontrola
u	u	k7c2	u
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
očkování	očkování	k1gNnSc1	očkování
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
0	[number]	k4	0
–	–	k?	–
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
zdravé	zdravá	k1gFnPc1	zdravá
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
)	)	kIx)	)
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
i	i	k9	i
návštěvy	návštěva	k1gFnPc1	návštěva
lékaře	lékař	k1gMnSc2	lékař
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
se	se	k3xPyFc4	se
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
programy	program	k1gInPc7	program
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
programů	program	k1gInPc2	program
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
minimální	minimální	k2eAgFnSc4d1	minimální
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
80	[number]	k4	80
–	–	k?	–
85	[number]	k4	85
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
opakovat	opakovat	k5eAaImF	opakovat
ročník	ročník	k1gInSc1	ročník
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
podmínek	podmínka	k1gFnPc2	podmínka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
edukačním	edukační	k2eAgInSc6d1	edukační
výživovém	výživový	k2eAgInSc6d1	výživový
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
většina	většina	k1gFnSc1	většina
programů	program	k1gInPc2	program
má	mít	k5eAaImIp3nS	mít
jasně	jasně	k6eAd1	jasně
daná	daný	k2eAgNnPc4d1	dané
pravidla	pravidlo	k1gNnPc4	pravidlo
sankcí	sankce	k1gFnPc2	sankce
pro	pro	k7c4	pro
neplnění	neplnění	k1gNnSc4	neplnění
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
většinovým	většinový	k2eAgInSc7d1	většinový
postihem	postih	k1gInSc7	postih
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
dočasné	dočasný	k2eAgNnSc4d1	dočasné
pozastavení	pozastavení	k1gNnSc4	pozastavení
vyplácení	vyplácení	k1gNnSc2	vyplácení
dávek	dávka	k1gFnPc2	dávka
<g/>
,	,	kIx,	,
než	než	k8xS	než
dojde	dojít	k5eAaPmIp3nS	dojít
opět	opět	k6eAd1	opět
k	k	k7c3	k
dodržování	dodržování	k1gNnSc3	dodržování
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
vyplácení	vyplácení	k1gNnSc2	vyplácení
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
případě	případ	k1gInSc6	případ
opětovného	opětovný	k2eAgNnSc2d1	opětovné
nedodržování	nedodržování	k1gNnSc2	nedodržování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dopad	dopad	k1gInSc4	dopad
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
na	na	k7c4	na
spotřebu	spotřeba	k1gFnSc4	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
zaměstnanost	zaměstnanost	k1gFnSc4	zaměstnanost
(	(	kIx(	(
<g/>
Krátkodobý	krátkodobý	k2eAgInSc1d1	krátkodobý
efekt	efekt	k1gInSc1	efekt
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Dopad	dopad	k1gInSc1	dopad
podmíněných	podmíněný	k2eAgFnPc2d1	podmíněná
finančních	finanční	k2eAgFnPc2d1	finanční
dávek	dávka	k1gFnPc2	dávka
na	na	k7c4	na
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
spotřebu	spotřeba	k1gFnSc4	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
snižování	snižování	k1gNnSc4	snižování
chudoby	chudoba	k1gFnSc2	chudoba
v	v	k7c6	v
krátkodobé	krátkodobý	k2eAgFnSc6d1	krátkodobá
perspektivě	perspektiva	k1gFnSc6	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Šíře	šíře	k1gFnSc1	šíře
dávek	dávka	k1gFnPc2	dávka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
země	zem	k1gFnPc1	zem
od	od	k7c2	od
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
tvoří	tvořit	k5eAaImIp3nP	tvořit
dávky	dávka	k1gFnPc1	dávka
30	[number]	k4	30
%	%	kIx~	%
spotřeby	spotřeba	k1gFnSc2	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
Kambodžou	Kambodža	k1gFnSc7	Kambodža
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
skladbu	skladba	k1gFnSc4	skladba
spotřeby	spotřeba	k1gFnSc2	spotřeba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
jídla	jídlo	k1gNnSc2	jídlo
(	(	kIx(	(
<g/>
lepší	dobrý	k2eAgFnPc1d2	lepší
nutriční	nutriční	k2eAgFnPc1d1	nutriční
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výdaje	výdaj	k1gInPc1	výdaj
domácností	domácnost	k1gFnPc2	domácnost
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
kvalitu	kvalita	k1gFnSc4	kvalita
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
domácnosti	domácnost	k1gFnPc1	domácnost
z	z	k7c2	z
programu	program	k1gInSc2	program
Familias	Familias	k1gMnSc1	Familias
en	en	k?	en
Acción	Acción	k1gMnSc1	Acción
z	z	k7c2	z
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
kupovaly	kupovat	k5eAaImAgInP	kupovat
více	hodně	k6eAd2	hodně
potravin	potravina	k1gFnPc2	potravina
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
proteiny	protein	k1gInPc4	protein
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
vejce	vejce	k1gNnSc1	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
snižují	snižovat	k5eAaImIp3nP	snižovat
práci	práce	k1gFnSc4	práce
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
věku	věk	k1gInSc6	věk
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nutí	nutit	k5eAaImIp3nP	nutit
rodiče	rodič	k1gMnPc1	rodič
(	(	kIx(	(
<g/>
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zapsali	zapsat	k5eAaPmAgMnP	zapsat
své	svůj	k3xOyFgNnSc4	svůj
dítě	dítě	k1gNnSc4	dítě
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c2	za
finanční	finanční	k2eAgFnSc2d1	finanční
dávky	dávka	k1gFnSc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
důležitost	důležitost	k1gFnSc4	důležitost
vzdělání	vzdělání	k1gNnSc2	vzdělání
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
dítě	dítě	k1gNnSc4	dítě
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
finanční	finanční	k2eAgFnSc1d1	finanční
podpora	podpora	k1gFnSc1	podpora
domácností	domácnost	k1gFnPc2	domácnost
snižuje	snižovat	k5eAaImIp3nS	snižovat
jejich	jejich	k3xOp3gFnSc4	jejich
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
příjmech	příjem	k1gInPc6	příjem
z	z	k7c2	z
dětské	dětský	k2eAgFnSc2d1	dětská
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
průzkumů	průzkum	k1gInPc2	průzkum
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
programy	program	k1gInPc1	program
snížily	snížit	k5eAaPmAgInP	snížit
zaměstnanost	zaměstnanost	k1gFnSc4	zaměstnanost
dětí	dítě	k1gFnPc2	dítě
o	o	k7c4	o
desítky	desítka	k1gFnPc4	desítka
procent	procento	k1gNnPc2	procento
(	(	kIx(	(
<g/>
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
země	zem	k1gFnPc1	zem
od	od	k7c2	od
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
mohou	moct	k5eAaImIp3nP	moct
zhoršit	zhoršit	k5eAaPmF	zhoršit
pracovní	pracovní	k2eAgInPc4d1	pracovní
návyky	návyk	k1gInPc4	návyk
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
některé	některý	k3yIgInPc1	některý
výzkumy	výzkum	k1gInPc1	výzkum
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dospělí	dospělí	k1gMnPc1	dospělí
záměrně	záměrně	k6eAd1	záměrně
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zůstali	zůstat	k5eAaPmAgMnP	zůstat
"	"	kIx"	"
<g/>
chudí	chudý	k2eAgMnPc1d1	chudý
<g/>
"	"	kIx"	"
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
mohli	moct	k5eAaImAgMnP	moct
pobírat	pobírat	k5eAaImF	pobírat
dávky	dávka	k1gFnPc4	dávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dopad	dopad	k1gInSc4	dopad
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
programů	program	k1gInPc2	program
na	na	k7c4	na
akumulaci	akumulace	k1gFnSc4	akumulace
lidského	lidský	k2eAgInSc2d1	lidský
kapitálu	kapitál	k1gInSc2	kapitál
(	(	kIx(	(
<g/>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
efekt	efekt	k1gInSc1	efekt
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
programy	program	k1gInPc7	program
se	se	k3xPyFc4	se
také	také	k6eAd1	také
snaží	snažit	k5eAaImIp3nS	snažit
zlepšit	zlepšit	k5eAaPmF	zlepšit
situaci	situace	k1gFnSc4	situace
chudých	chudý	k2eAgFnPc2d1	chudá
pomocí	pomoc	k1gFnPc2	pomoc
přesvědčování	přesvědčování	k1gNnSc2	přesvědčování
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
investování	investování	k1gNnSc2	investování
do	do	k7c2	do
lidského	lidský	k2eAgInSc2d1	lidský
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jejich	jejich	k3xOp3gFnPc2	jejich
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc1d2	lepší
výživa	výživa	k1gFnSc1	výživa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
bylo	být	k5eAaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
děti	dítě	k1gFnPc1	dítě
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nP	kdyby
nebyly	být	k5eNaImAgFnP	být
nuceny	nucen	k2eAgFnPc4d1	nucena
školy	škola	k1gFnPc4	škola
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Získávají	získávat	k5eAaImIp3nP	získávat
lepší	dobrý	k2eAgNnSc4d2	lepší
pracovní	pracovní	k2eAgNnSc4d1	pracovní
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
,	,	kIx,	,
udržují	udržovat	k5eAaImIp3nP	udržovat
si	se	k3xPyFc3	se
zdravé	zdravý	k2eAgInPc4d1	zdravý
stravovací	stravovací	k2eAgInPc4d1	stravovací
návyky	návyk	k1gInPc4	návyk
a	a	k8xC	a
dožívají	dožívat	k5eAaImIp3nP	dožívat
se	se	k3xPyFc4	se
vyššího	vysoký	k2eAgInSc2d2	vyšší
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Efektivita	efektivita	k1gFnSc1	efektivita
Podmíněných	podmíněný	k2eAgFnPc2d1	podmíněná
finančních	finanční	k2eAgFnPc2d1	finanční
dávek	dávka	k1gFnPc2	dávka
==	==	k?	==
</s>
</p>
<p>
<s>
Efektivita	efektivita	k1gFnSc1	efektivita
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
je	být	k5eAaImIp3nS	být
měřena	měřit	k5eAaImNgFnS	měřit
několika	několik	k4yIc7	několik
rozvojovými	rozvojový	k2eAgFnPc7d1	rozvojová
agenturami	agentura	k1gFnPc7	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
agentury	agentura	k1gFnPc1	agentura
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
měřit	měřit	k5eAaImF	měřit
dopad	dopad	k1gInSc4	dopad
podmíněných	podmíněný	k2eAgFnPc2d1	podmíněná
finančních	finanční	k2eAgFnPc2d1	finanční
dávek	dávka	k1gFnPc2	dávka
na	na	k7c4	na
chování	chování	k1gNnSc4	chování
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
efektivně	efektivně	k6eAd1	efektivně
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
cílí	cílit	k5eAaImIp3nP	cílit
na	na	k7c6	na
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
změřeno	změřen	k2eAgNnSc1d1	změřeno
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byly	být	k5eAaImAgFnP	být
domácnosti	domácnost	k1gFnPc1	domácnost
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
nedostaly	dostat	k5eNaPmAgFnP	dostat
finanční	finanční	k2eAgFnPc4d1	finanční
dávky	dávka	k1gFnPc4	dávka
a	a	k8xC	a
potom	potom	k6eAd1	potom
udělat	udělat	k5eAaPmF	udělat
hodnocení	hodnocení	k1gNnSc1	hodnocení
podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
porovnání	porovnání	k1gNnSc4	porovnání
spotřeby	spotřeba	k1gFnSc2	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
s	s	k7c7	s
dávkami	dávka	k1gFnPc7	dávka
se	se	k3xPyFc4	se
spotřebou	spotřeba	k1gFnSc7	spotřeba
domácností	domácnost	k1gFnPc2	domácnost
bez	bez	k7c2	bez
dávek	dávka	k1gFnPc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
chudý	chudý	k2eAgMnSc1d1	chudý
i	i	k8xC	i
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
dávek	dávka	k1gFnPc2	dávka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
chudý	chudý	k2eAgMnSc1d1	chudý
i	i	k9	i
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
kladné	kladný	k2eAgInPc4d1	kladný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
domácnosti	domácnost	k1gFnPc1	domácnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dostávají	dostávat	k5eAaImIp3nP	dostávat
tyto	tento	k3xDgFnPc4	tento
finanční	finanční	k2eAgFnPc4d1	finanční
dávky	dávka	k1gFnPc4	dávka
<g/>
,	,	kIx,	,
utrácejí	utrácet	k5eAaImIp3nP	utrácet
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
za	za	k7c4	za
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
jídlo	jídlo	k1gNnSc4	jídlo
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
kalorickou	kalorický	k2eAgFnSc7d1	kalorická
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
,	,	kIx,	,
než	než	k8xS	než
domácnosti	domácnost	k1gFnPc1	domácnost
s	s	k7c7	s
obdobným	obdobný	k2eAgInSc7d1	obdobný
příjmem	příjem	k1gInSc7	příjem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dávky	dávka	k1gFnPc1	dávka
nedostávají	dostávat	k5eNaImIp3nP	dostávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc6	Pákistán
a	a	k8xC	a
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
dívek	dívka	k1gFnPc2	dívka
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
pomohly	pomoct	k5eAaPmAgInP	pomoct
programy	program	k1gInPc1	program
CCTs	CCTsa	k1gFnPc2	CCTsa
zmenšit	zmenšit	k5eAaPmF	zmenšit
tuto	tento	k3xDgFnSc4	tento
mezeru	mezera	k1gFnSc4	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
CCTs	CCTs	k1gInSc1	CCTs
pomohly	pomoct	k5eAaPmAgInP	pomoct
snížit	snížit	k5eAaPmF	snížit
chudobu	chudoba	k1gFnSc4	chudoba
mezi	mezi	k7c7	mezi
příjemci	příjemce	k1gMnPc7	příjemce
<g/>
,	,	kIx,	,
ochránily	ochránit	k5eAaPmAgFnP	ochránit
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
negativními	negativní	k2eAgInPc7d1	negativní
dopady	dopad	k1gInPc7	dopad
špatně	špatně	k6eAd1	špatně
fungující	fungující	k2eAgFnSc2d1	fungující
ekonomiky	ekonomika	k1gFnSc2	ekonomika
(	(	kIx(	(
<g/>
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
<g/>
,	,	kIx,	,
nemocemi	nemoc	k1gFnPc7	nemoc
nebo	nebo	k8xC	nebo
dalšími	další	k2eAgInPc7d1	další
náhlými	náhlý	k2eAgInPc7d1	náhlý
šoky	šok	k1gInPc7	šok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Problémy	problém	k1gInPc1	problém
při	při	k7c6	při
implementaci	implementace	k1gFnSc6	implementace
CCTs	CCTs	k1gInSc4	CCTs
a	a	k8xC	a
neúspěšné	úspěšný	k2eNgInPc4d1	neúspěšný
programy	program	k1gInPc4	program
(	(	kIx(	(
<g/>
Kritiky	kritika	k1gFnPc4	kritika
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Dopad	dopad	k1gInSc1	dopad
podmíněných	podmíněný	k2eAgFnPc2d1	podmíněná
finančních	finanční	k2eAgFnPc2d1	finanční
dávek	dávka	k1gFnPc2	dávka
na	na	k7c4	na
snižování	snižování	k1gNnSc4	snižování
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
nerovností	nerovnost	k1gFnPc2	nerovnost
byl	být	k5eAaImAgInS	být
celosvětově	celosvětově	k6eAd1	celosvětově
uznán	uznat	k5eAaPmNgInS	uznat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
několik	několik	k4yIc1	několik
překážek	překážka	k1gFnPc2	překážka
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
implementaci	implementace	k1gFnSc6	implementace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
programů	program	k1gInPc2	program
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
neefektivnosti	neefektivnost	k1gFnSc3	neefektivnost
či	či	k8xC	či
úplnému	úplný	k2eAgNnSc3d1	úplné
zastavení	zastavení	k1gNnSc3	zastavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
faktory	faktor	k1gInPc1	faktor
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
prováděné	prováděný	k2eAgFnSc2d1	prováděná
analytiky	analytika	k1gFnSc2	analytika
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
Laurou	Laura	k1gFnSc7	Laura
Rawlingovou	Rawlingový	k2eAgFnSc7d1	Rawlingová
a	a	k8xC	a
Gloriou	Gloria	k1gFnSc7	Gloria
Rubio	Rubio	k6eAd1	Rubio
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
každého	každý	k3xTgInSc2	každý
programu	program	k1gInSc2	program
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
si	se	k3xPyFc3	se
stanovit	stanovit	k5eAaPmF	stanovit
harmonogram	harmonogram	k1gInSc4	harmonogram
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
narušen	narušit	k5eAaPmNgInS	narušit
politickými	politický	k2eAgFnPc7d1	politická
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
přírodními	přírodní	k2eAgFnPc7d1	přírodní
pohromami	pohroma	k1gFnPc7	pohroma
nebo	nebo	k8xC	nebo
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
administraci	administrace	k1gFnSc6	administrace
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vézt	vézt	k5eAaImF	vézt
k	k	k7c3	k
neefektivnosti	neefektivnost	k1gFnSc3	neefektivnost
programů	program	k1gInPc2	program
či	či	k8xC	či
jejich	jejich	k3xOp3gNnSc3	jejich
úplnému	úplný	k2eAgNnSc3d1	úplné
zastavení	zastavení	k1gNnSc3	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takto	takto	k6eAd1	takto
postižených	postižený	k2eAgInPc2d1	postižený
programů	program	k1gInPc2	program
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
Rozvojového	rozvojový	k2eAgInSc2d1	rozvojový
programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
Social	Social	k1gMnSc1	Social
Protection	Protection	k1gInSc4	Protection
Network	network	k1gInSc2	network
z	z	k7c2	z
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
administrativy	administrativa	k1gFnSc2	administrativa
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
jeho	jeho	k3xOp3gFnSc2	jeho
efektivity	efektivita	k1gFnSc2	efektivita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
až	až	k9	až
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
úplnému	úplný	k2eAgNnSc3d1	úplné
zrušení	zrušení	k1gNnSc3	zrušení
<g/>
.	.	kIx.	.
</s>
<s>
Obtíže	obtíž	k1gFnPc1	obtíž
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
nastat	nastat	k5eAaPmF	nastat
se	s	k7c7	s
zaváděním	zavádění	k1gNnSc7	zavádění
Manažerského	manažerský	k2eAgInSc2d1	manažerský
informačního	informační	k2eAgInSc2d1	informační
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
MIT	MIT	kA	MIT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mexický	mexický	k2eAgInSc1d1	mexický
program	program	k1gInSc1	program
Oportunidades	Oportunidadesa	k1gFnPc2	Oportunidadesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
27	[number]	k4	27
%	%	kIx~	%
účastníků	účastník	k1gMnPc2	účastník
nedostalo	dostat	k5eNaPmAgNnS	dostat
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
po	po	k7c6	po
2	[number]	k4	2
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
zavedení	zavedení	k1gNnSc2	zavedení
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
flexibilita	flexibilita	k1gFnSc1	flexibilita
programů	program	k1gInPc2	program
podmíněných	podmíněný	k2eAgFnPc2d1	podmíněná
finančních	finanční	k2eAgFnPc2d1	finanční
dávek	dávka	k1gFnPc2	dávka
během	během	k7c2	během
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
rozšířit	rozšířit	k5eAaPmF	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
kapacitu	kapacita	k1gFnSc4	kapacita
o	o	k7c4	o
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
spadli	spadnout	k5eAaPmAgMnP	spadnout
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
pouze	pouze	k6eAd1	pouze
během	během	k7c2	během
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyloučení	vyloučení	k1gNnSc2	vyloučení
===	===	k?	===
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
běžnou	běžný	k2eAgFnSc7d1	běžná
překážkou	překážka	k1gFnSc7	překážka
je	být	k5eAaImIp3nS	být
vyloučení	vyloučení	k1gNnSc1	vyloučení
potřebných	potřebný	k2eAgFnPc2d1	potřebná
domácností	domácnost	k1gFnPc2	domácnost
z	z	k7c2	z
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
znemožněním	znemožnění	k1gNnSc7	znemožnění
domácností	domácnost	k1gFnPc2	domácnost
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
ke	k	k7c3	k
klinikám	klinika	k1gFnPc3	klinika
či	či	k8xC	či
školám	škola	k1gFnPc3	škola
<g/>
.	.	kIx.	.
</s>
<s>
Nevyplatí	vyplatit	k5eNaPmIp3nS	vyplatit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
cestovat	cestovat	k5eAaImF	cestovat
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
zařízením	zařízení	k1gNnPc3	zařízení
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
když	když	k8xS	když
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
transport	transport	k1gInSc4	transport
převyšují	převyšovat	k5eAaImIp3nP	převyšovat
získanou	získaný	k2eAgFnSc4d1	získaná
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
splnit	splnit	k5eAaPmF	splnit
podmínky	podmínka	k1gFnPc1	podmínka
jejího	její	k3xOp3gNnSc2	její
čerpání	čerpání	k1gNnSc2	čerpání
<g/>
.	.	kIx.	.
</s>
<s>
Ba	ba	k9	ba
co	co	k9	co
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
mexického	mexický	k2eAgInSc2d1	mexický
programu	program	k1gInSc2	program
Oportunidades	Oportunidades	k1gMnSc1	Oportunidades
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
chorobou	choroba	k1gFnSc7	choroba
nebyly	být	k5eNaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
cestovat	cestovat	k5eAaImF	cestovat
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
tyto	tento	k3xDgFnPc4	tento
podmínky	podmínka	k1gFnPc4	podmínka
splnit	splnit	k5eAaPmF	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémovou	problémový	k2eAgFnSc7d1	problémová
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
pracující	pracující	k2eAgFnPc1d1	pracující
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
nemohou	moct	k5eNaImIp3nP	moct
dovolit	dovolit	k5eAaPmF	dovolit
zmeškat	zmeškat	k5eAaPmF	zmeškat
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nejsou	být	k5eNaImIp3nP	být
rovněž	rovněž	k9	rovněž
schopny	schopen	k2eAgFnPc1d1	schopna
splnit	splnit	k5eAaPmF	splnit
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
čerpání	čerpání	k1gNnSc4	čerpání
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
===	===	k?	===
</s>
</p>
<p>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
důvěry	důvěra	k1gFnSc2	důvěra
mezi	mezi	k7c7	mezi
cílenou	cílený	k2eAgFnSc7d1	cílená
populací	populace	k1gFnSc7	populace
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
špatnou	špatný	k2eAgFnSc7d1	špatná
informovaností	informovanost	k1gFnSc7	informovanost
<g/>
,	,	kIx,	,
špatným	špatný	k2eAgInSc7d1	špatný
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vézt	vézt	k5eAaImF	vézt
k	k	k7c3	k
neefektivnosti	neefektivnost	k1gFnSc3	neefektivnost
či	či	k8xC	či
úpadku	úpadek	k1gInSc3	úpadek
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
neúspěšný	úspěšný	k2eNgInSc4d1	neúspěšný
program	program	k1gInSc4	program
z	z	k7c2	z
Nikaragui	Nikaragua	k1gFnSc3	Nikaragua
(	(	kIx(	(
<g/>
Social	Social	k1gInSc1	Social
Protection	Protection	k1gInSc1	Protection
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgMnS	potýkat
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
nedůvěrou	nedůvěra	k1gFnSc7	nedůvěra
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
zpolitizovaný	zpolitizovaný	k2eAgInSc1d1	zpolitizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
případ	případ	k1gInSc1	případ
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
neměli	mít	k5eNaImAgMnP	mít
důvěru	důvěra	k1gFnSc4	důvěra
v	v	k7c4	v
místní	místní	k2eAgFnSc4d1	místní
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
řešením	řešení	k1gNnSc7	řešení
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
přerozdělování	přerozdělování	k1gNnSc1	přerozdělování
peněz	peníze	k1gInPc2	peníze
pomocí	pomocí	k7c2	pomocí
nevladních	vladní	k2eNgFnPc2d1	vladní
neziskových	ziskový	k2eNgFnPc2d1	nezisková
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
NGO	NGO	kA	NGO
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podmíněné	podmíněný	k2eAgFnPc1d1	podmíněná
finanční	finanční	k2eAgFnPc1d1	finanční
dávky	dávka	k1gFnPc1	dávka
nebo	nebo	k8xC	nebo
nepodmíněné	podmíněný	k2eNgFnPc1d1	nepodmíněná
finanční	finanční	k2eAgFnPc1d1	finanční
dávky	dávka	k1gFnPc1	dávka
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Argumenty	argument	k1gInPc1	argument
pro	pro	k7c4	pro
a	a	k8xC	a
proti	proti	k7c3	proti
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
čtěte	číst	k5eAaImRp2nP	číst
<g/>
:	:	kIx,	:
Unconditional	Unconditional	k1gFnSc4	Unconditional
cash	cash	k1gFnSc2	cash
transfers	transfers	k1gInSc1	transfers
(	(	kIx(	(
<g/>
UCT	UCT	kA	UCT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
se	se	k3xPyFc4	se
vede	vést	k5eAaImIp3nS	vést
diskuse	diskuse	k1gFnSc1	diskuse
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
poskytovat	poskytovat	k5eAaImF	poskytovat
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
finanční	finanční	k2eAgFnPc4d1	finanční
dávky	dávka	k1gFnPc4	dávka
nebo	nebo	k8xC	nebo
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
lepší	dobrý	k2eAgFnPc4d2	lepší
nepodmíněné	podmíněný	k2eNgFnPc4d1	nepodmíněná
finanční	finanční	k2eAgFnPc4d1	finanční
dávky	dávka	k1gFnPc4	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Argumenty	argument	k1gInPc1	argument
pro	pro	k7c4	pro
CCT	CCT	kA	CCT
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
rodiny	rodina	k1gFnPc1	rodina
chovají	chovat	k5eAaImIp3nP	chovat
racionálně	racionálně	k6eAd1	racionálně
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
zde	zde	k6eAd1	zde
být	být	k5eAaImF	být
rozpory	rozpor	k1gInPc4	rozpor
v	v	k7c6	v
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
budoucnost	budoucnost	k1gFnSc4	budoucnost
mezi	mezi	k7c7	mezi
rodiči	rodič	k1gMnPc7	rodič
(	(	kIx(	(
<g/>
kteří	který	k3yIgMnPc1	který
platí	platit	k5eAaImIp3nP	platit
za	za	k7c4	za
studium	studium	k1gNnSc4	studium
<g/>
)	)	kIx)	)
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
(	(	kIx(	(
<g/>
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
benefitovat	benefitovat	k5eAaBmF	benefitovat
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
argumentem	argument	k1gInSc7	argument
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlády	vláda	k1gFnPc1	vláda
jsou	být	k5eAaImIp3nP	být
ovlivňovány	ovlivňován	k2eAgInPc1d1	ovlivňován
zájmovými	zájmový	k2eAgFnPc7d1	zájmová
skupinami	skupina	k1gFnPc7	skupina
a	a	k8xC	a
proto	proto	k8xC	proto
podmíněné	podmíněný	k2eAgFnPc1d1	podmíněná
dávky	dávka	k1gFnPc1	dávka
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
zajistit	zajistit	k5eAaPmF	zajistit
lepší	dobrý	k2eAgFnSc4d2	lepší
rovnost	rovnost	k1gFnSc4	rovnost
při	při	k7c6	při
přerozdělování	přerozdělování	k1gNnSc6	přerozdělování
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	s	k7c7	s
programem	program	k1gInSc7	program
Oportunidades	Oportunidades	k1gInSc1	Oportunidades
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
dětí	dítě	k1gFnPc2	dítě
docházející	docházející	k2eAgMnSc1d1	docházející
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
u	u	k7c2	u
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dostávají	dostávat	k5eAaImIp3nP	dostávat
podporu	podpora	k1gFnSc4	podpora
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ji	on	k3xPp3gFnSc4	on
nedostávají	dostávat	k5eNaImIp3nP	dostávat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
dobrým	dobrý	k2eAgInSc7d1	dobrý
příkladem	příklad	k1gInSc7	příklad
těchto	tento	k3xDgInPc2	tento
programů	program	k1gInPc2	program
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Malawi	Malawi	k1gNnSc4	Malawi
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prováděl	provádět	k5eAaImAgInS	provádět
výzkum	výzkum	k1gInSc1	výzkum
rodin	rodina	k1gFnPc2	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
(	(	kIx(	(
<g/>
dívkami	dívka	k1gFnPc7	dívka
<g/>
)	)	kIx)	)
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Rodiny	rodina	k1gFnPc1	rodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dostávaly	dostávat	k5eAaImAgFnP	dostávat
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
opět	opět	k6eAd1	opět
větší	veliký	k2eAgFnSc4d2	veliký
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
docházce	docházka	k1gFnSc6	docházka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
dcery	dcera	k1gFnPc1	dcera
vyšly	vyjít	k5eAaPmAgFnP	vyjít
z	z	k7c2	z
průzkumu	průzkum	k1gInSc2	průzkum
gramotnosti	gramotnost	k1gFnSc2	gramotnost
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
dívky	dívka	k1gFnPc1	dívka
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
<g/>
Silný	silný	k2eAgInSc1d1	silný
argument	argument	k1gInSc1	argument
pro	pro	k7c4	pro
nepodmíněné	podmíněný	k2eNgFnPc4d1	nepodmíněná
finanční	finanční	k2eAgFnPc4d1	finanční
dávky	dávka	k1gFnPc4	dávka
zaznívá	zaznívat	k5eAaImIp3nS	zaznívat
z	z	k7c2	z
tábora	tábor	k1gInSc2	tábor
zastánců	zastánce	k1gMnPc2	zastánce
rovného	rovný	k2eAgInSc2d1	rovný
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
(	(	kIx(	(
<g/>
Rights-based	Rightsased	k1gMnSc1	Rights-based
approach	approach	k1gMnSc1	approach
to	ten	k3xDgNnSc4	ten
development	development	k1gMnSc1	development
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
finanční	finanční	k2eAgFnPc4d1	finanční
dávky	dávka	k1gFnPc4	dávka
pomáhat	pomáhat	k5eAaImF	pomáhat
všem	všecek	k3xTgMnPc3	všecek
obyvatelům	obyvatel	k1gMnPc3	obyvatel
státu	stát	k1gInSc2	stát
zlepšit	zlepšit	k5eAaPmF	zlepšit
jejich	jejich	k3xOp3gFnSc3	jejich
životní	životní	k2eAgFnSc3d1	životní
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistil	zajistit	k5eAaPmAgMnS	zajistit
všem	všecek	k3xTgMnPc3	všecek
rovný	rovný	k2eAgInSc4d1	rovný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
potravinám	potravina	k1gFnPc3	potravina
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kritika	kritika	k1gFnSc1	kritika
(	(	kIx(	(
<g/>
argumenty	argument	k1gInPc1	argument
proti	proti	k7c3	proti
<g/>
)	)	kIx)	)
na	na	k7c4	na
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
finanční	finanční	k2eAgFnPc4d1	finanční
dávky	dávka	k1gFnPc4	dávka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
vyloučily	vyloučit	k5eAaPmAgFnP	vyloučit
z	z	k7c2	z
programu	program	k1gInSc2	program
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
to	ten	k3xDgNnSc4	ten
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
nejvíce	hodně	k6eAd3	hodně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
příliš	příliš	k6eAd1	příliš
drahé	drahý	k2eAgNnSc1d1	drahé
jejich	jejich	k3xOp3gNnSc1	jejich
plnění	plnění	k1gNnSc1	plnění
(	(	kIx(	(
<g/>
kliniky	klinika	k1gFnPc1	klinika
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
<g/>
;	;	kIx,	;
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
děti	dítě	k1gFnPc4	dítě
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohly	pomoct	k5eAaPmAgInP	pomoct
rodičům	rodič	k1gMnPc3	rodič
sklidit	sklidit	k5eAaPmF	sklidit
úrodu	úroda	k1gFnSc4	úroda
pro	pro	k7c4	pro
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
porušily	porušit	k5eAaPmAgFnP	porušit
princip	princip	k1gInSc1	princip
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
nediskriminaci	nediskriminace	k1gFnSc4	nediskriminace
a	a	k8xC	a
rovnost	rovnost	k1gFnSc4	rovnost
<g/>
.	.	kIx.	.
</s>
<s>
Otázkou	otázka	k1gFnSc7	otázka
také	také	k9	také
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kvalita	kvalita	k1gFnSc1	kvalita
služeb	služba	k1gFnPc2	služba
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
by	by	kYmCp3nS	by
lepší	dobrý	k2eAgNnSc1d2	lepší
investovat	investovat	k5eAaBmF	investovat
peníze	peníz	k1gInPc4	peníz
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
zlepšení	zlepšení	k1gNnSc2	zlepšení
kvality	kvalita	k1gFnSc2	kvalita
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
či	či	k8xC	či
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
vyučujících	vyučující	k2eAgNnPc2d1	vyučující
<g/>
?	?	kIx.	?
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
mohou	moct	k5eAaImIp3nP	moct
považovat	považovat	k5eAaImF	považovat
nucení	nucení	k1gNnPc4	nucení
dětí	dítě	k1gFnPc2	dítě
docházet	docházet	k5eAaImF	docházet
na	na	k7c4	na
nekvalitní	kvalitní	k2eNgFnSc4d1	nekvalitní
školu	škola	k1gFnSc4	škola
či	či	k8xC	či
kliniku	klinika	k1gFnSc4	klinika
za	za	k7c4	za
plýtvání	plýtvání	k1gNnSc4	plýtvání
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
raději	rád	k6eAd2	rád
strávit	strávit	k5eAaPmF	strávit
účelněji	účelně	k6eAd2	účelně
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
<g/>
Příkladem	příklad	k1gInSc7	příklad
programu	program	k1gInSc2	program
nepodmíněných	podmíněný	k2eNgFnPc2d1	nepodmíněná
finančních	finanční	k2eAgFnPc2d1	finanční
dávkek	dávkky	k1gFnPc2	dávkky
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
v	v	k7c6	v
Malawi	Malawi	k1gNnSc6	Malawi
Social	Social	k1gInSc1	Social
Protection	Protection	k1gInSc4	Protection
Policy	Polica	k1gFnSc2	Polica
and	and	k?	and
Framework	Framework	k1gInSc1	Framework
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
financovaný	financovaný	k2eAgInSc4d1	financovaný
organizací	organizace	k1gFnSc7	organizace
UNICEF	UNICEF	kA	UNICEF
a	a	k8xC	a
Národní	národní	k2eAgFnSc7d1	národní
komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
AIDS	AIDS	kA	AIDS
(	(	kIx(	(
<g/>
National	National	k1gMnSc2	National
Commission	Commission	k1gInSc1	Commission
on	on	k3xPp3gMnSc1	on
AIDS	AIDS	kA	AIDS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
cílí	cílit	k5eAaImIp3nS	cílit
na	na	k7c4	na
úplně	úplně	k6eAd1	úplně
nejchudší	chudý	k2eAgFnPc4d3	nejchudší
vrstvy	vrstva	k1gFnPc4	vrstva
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
22	[number]	k4	22
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Malawi	Malawi	k1gNnSc2	Malawi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
snížit	snížit	k5eAaPmF	snížit
na	na	k7c6	na
10	[number]	k4	10
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
dá	dát	k5eAaPmIp3nS	dát
600	[number]	k4	600
(	(	kIx(	(
<g/>
4	[number]	k4	4
$	$	kIx~	$
<g/>
)	)	kIx)	)
Kwacha	Kwacha	k1gFnSc1	Kwacha
každé	každý	k3xTgFnSc6	každý
rodině	rodina	k1gFnSc6	rodina
za	za	k7c4	za
osobu	osoba	k1gFnSc4	osoba
do	do	k7c2	do
max	max	kA	max
1	[number]	k4	1
800	[number]	k4	800
Kwacha	Kwacha	k1gFnSc1	Kwacha
(	(	kIx(	(
<g/>
13	[number]	k4	13
$	$	kIx~	$
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
4	[number]	k4	4
a	a	k8xC	a
více	hodně	k6eAd2	hodně
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
extra	extra	k2eAgInSc4d1	extra
bonus	bonus	k1gInSc4	bonus
200	[number]	k4	200
Kwacha	Kwacha	k1gFnSc1	Kwacha
za	za	k7c4	za
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zapíše	zapsat	k5eAaPmIp3nS	zapsat
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
400	[number]	k4	400
Kwacha	Kwacha	k1gFnSc1	Kwacha
za	za	k7c4	za
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
