<s>
Pojem	pojem	k1gInSc1	pojem
heavy	heava	k1gFnSc2	heava
metal	metat	k5eAaImAgInS	metat
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
v	v	k7c6	v
několika	několik	k4yIc6	několik
významech	význam	k1gInPc6	význam
<g/>
:	:	kIx,	:
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
představuje	představovat	k5eAaImIp3nS	představovat
tradiční	tradiční	k2eAgFnPc4d1	tradiční
heavy	heava	k1gFnPc4	heava
metal	metal	k1gInSc4	metal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
eliminováním	eliminování	k1gNnSc7	eliminování
bluesového	bluesový	k2eAgInSc2d1	bluesový
vlivu	vliv	k1gInSc2	vliv
<g/>
;	;	kIx,	;
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
představuje	představovat	k5eAaImIp3nS	představovat
heavy	heava	k1gFnPc4	heava
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
zkráceně	zkráceně	k6eAd1	zkráceně
metal	metat	k5eAaImAgMnS	metat
<g/>
)	)	kIx)	)
celý	celý	k2eAgInSc4d1	celý
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgFnPc2	všecek
jeho	jeho	k3xOp3gFnSc1	jeho
odnoží	odnožit	k5eAaPmIp3nS	odnožit
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
pojem	pojem	k1gInSc4	pojem
heavy	heava	k1gFnSc2	heava
metal	metal	k1gInSc1	metal
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
<g/>
,	,	kIx,	,
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
Heavy	Heava	k1gFnSc2	Heava
metal	metal	k1gInSc1	metal
není	být	k5eNaImIp3nS	být
totéž	týž	k3xTgNnSc1	týž
jako	jako	k9	jako
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Heavy	Heava	k1gFnPc1	Heava
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
rovněž	rovněž	k9	rovněž
"	"	kIx"	"
<g/>
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
vymezený	vymezený	k2eAgInSc1d1	vymezený
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
hardrockových	hardrockový	k2eAgFnPc6d1	hardrocková
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
kombinováním	kombinování	k1gNnSc7	kombinování
blues	blues	k1gFnPc2	blues
a	a	k8xC	a
rocku	rock	k1gInSc2	rock
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
nový	nový	k2eAgInSc4d1	nový
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
používáním	používání	k1gNnSc7	používání
elektrických	elektrický	k2eAgFnPc2d1	elektrická
kytar	kytara	k1gFnPc2	kytara
a	a	k8xC	a
bicích	bicí	k2eAgInPc2d1	bicí
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
hlasitým	hlasitý	k2eAgInSc7d1	hlasitý
a	a	k8xC	a
zkresleným	zkreslený	k2eAgInSc7d1	zkreslený
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
All	All	k1gMnSc1	All
Music	Music	k1gMnSc1	Music
Guide	Guid	k1gInSc5	Guid
"	"	kIx"	"
<g/>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
nespočetných	spočetný	k2eNgFnPc2d1	nespočetná
forem	forma	k1gFnPc2	forma
rock	rock	k1gInSc1	rock
and	and	k?	and
rollu	rollat	k5eAaPmIp1nS	rollat
je	on	k3xPp3gFnPc4	on
heavy	heava	k1gFnPc4	heava
metal	metat	k5eAaImAgInS	metat
nejextrémnější	extrémní	k2eAgInSc1d3	nejextrémnější
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hlasitosti	hlasitost	k1gFnSc2	hlasitost
<g/>
,	,	kIx,	,
mužnosti	mužnost	k1gFnSc2	mužnost
nebo	nebo	k8xC	nebo
teatrálnosti	teatrálnost	k1gFnSc2	teatrálnost
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
variací	variace	k1gFnPc2	variace
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgInPc1	všechen
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
o	o	k7c4	o
zkreslený	zkreslený	k2eAgInSc4d1	zkreslený
zvuk	zvuk	k1gInSc4	zvuk
kytar	kytara	k1gFnPc2	kytara
a	a	k8xC	a
kytarové	kytarový	k2eAgInPc4d1	kytarový
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc4d1	zvaný
riffy	riff	k1gInPc4	riff
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mírně	mírně	k6eAd1	mírně
kovově	kovově	k6eAd1	kovově
znějící	znějící	k2eAgFnSc1d1	znějící
baskytara	baskytara	k1gFnSc1	baskytara
a	a	k8xC	a
úderné	úderný	k2eAgNnSc1d1	úderné
tempo	tempo	k1gNnSc1	tempo
bicích	bicí	k2eAgFnPc2d1	bicí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Průkopníci	průkopník	k1gMnPc1	průkopník
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
jako	jako	k8xC	jako
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
a	a	k8xC	a
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gFnSc2	Purple
si	se	k3xPyFc3	se
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
posluchačskou	posluchačský	k2eAgFnSc4d1	posluchačská
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
navzdory	navzdory	k7c3	navzdory
nepřízni	nepřízeň	k1gFnSc3	nepřízeň
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
žánru	žánr	k1gInSc2	žánr
nezřídka	nezřídka	k6eAd1	nezřídka
stává	stávat	k5eAaImIp3nS	stávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
britská	britský	k2eAgFnSc1d1	britská
kapela	kapela	k1gFnSc1	kapela
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
pomohla	pomoct	k5eAaPmAgFnS	pomoct
vytříbit	vytříbit	k5eAaPmF	vytříbit
žánr	žánr	k1gInSc4	žánr
pominutím	pominutí	k1gNnSc7	pominutí
většiny	většina	k1gFnSc2	většina
bluesových	bluesový	k2eAgInPc2d1	bluesový
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Kapely	kapela	k1gFnPc1	kapela
Nové	Nové	k2eAgFnSc2d1	Nové
vlny	vlna	k1gFnSc2	vlna
britského	britský	k2eAgInSc2d1	britský
heavy	heav	k1gInPc7	heav
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
a	a	k8xC	a
Motörhead	Motörhead	k1gInSc4	Motörhead
<g/>
,	,	kIx,	,
držíce	držet	k5eAaImSgFnP	držet
se	se	k3xPyFc4	se
téhle	tenhle	k3xDgFnSc2	tenhle
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
vnesly	vnést	k5eAaPmAgInP	vnést
do	do	k7c2	do
žánru	žánr	k1gInSc2	žánr
rebelii	rebelie	k1gFnSc4	rebelie
punk	punk	k1gInSc4	punk
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
rychlost	rychlost	k1gFnSc4	rychlost
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
typickými	typický	k2eAgFnPc7d1	typická
představiteli	představitel	k1gMnSc3	představitel
heavymetalu	heavymetal	k1gMnSc3	heavymetal
Helloween	Helloween	k1gInSc4	Helloween
<g/>
,	,	kIx,	,
Accept	Accept	k1gInSc4	Accept
a	a	k8xC	a
Manowar	Manowar	k1gInSc4	Manowar
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
počátků	počátek	k1gInPc2	počátek
měl	mít	k5eAaImAgInS	mít
heavy	heava	k1gFnPc4	heava
metal	metal	k1gInSc4	metal
velkou	velký	k2eAgFnSc7d1	velká
základnou	základna	k1gFnSc7	základna
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
známí	známý	k2eAgMnPc1d1	známý
jako	jako	k8xS	jako
metalisti	metalist	k1gMnPc1	metalist
<g/>
,	,	kIx,	,
metaláci	metalák	k1gMnPc1	metalák
či	či	k8xC	či
metláci	metlák	k1gMnPc1	metlák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xC	jako
Mötley	Mötle	k2eAgInPc1d1	Mötle
Crüe	Crü	k1gInPc1	Crü
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
náležely	náležet	k5eAaImAgInP	náležet
do	do	k7c2	do
popem	pop	k1gInSc7	pop
ovlivněného	ovlivněný	k2eAgInSc2d1	ovlivněný
žánru	žánr	k1gInSc2	žánr
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
glam	glam	k6eAd1	glam
metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
undergroundu	underground	k1gInSc6	underground
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
nové	nový	k2eAgInPc1d1	nový
<g/>
,	,	kIx,	,
agresivnější	agresivní	k2eAgInPc1d2	agresivnější
styly	styl	k1gInPc1	styl
<g/>
:	:	kIx,	:
thrash	thrash	k1gMnSc1	thrash
metal	metat	k5eAaImAgMnS	metat
přinesly	přinést	k5eAaPmAgFnP	přinést
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
proudu	proud	k1gInSc2	proud
kapely	kapela	k1gFnSc2	kapela
typu	typ	k1gInSc2	typ
Metallica	Metallicum	k1gNnSc2	Metallicum
a	a	k8xC	a
Megadeth	Megadetha	k1gFnPc2	Megadetha
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgInPc1d1	ostatní
styly	styl	k1gInPc1	styl
jako	jako	k8xC	jako
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
a	a	k8xC	a
black	black	k1gMnSc1	black
metal	metal	k1gInSc4	metal
zůstaly	zůstat	k5eAaPmAgInP	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
záležitostí	záležitost	k1gFnSc7	záležitost
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
získávají	získávat	k5eAaImIp3nP	získávat
ohlas	ohlas	k1gInSc4	ohlas
nové	nový	k2eAgInPc1d1	nový
styly	styl	k1gInPc1	styl
jako	jako	k8xS	jako
nu	nu	k9	nu
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
maje	mít	k5eAaImSgMnS	mít
prvky	prvek	k1gInPc4	prvek
funku	funk	k1gInSc2	funk
a	a	k8xC	a
hip	hip	k0	hip
hopu	hopu	k5eAaPmIp1nS	hopu
<g/>
)	)	kIx)	)
a	a	k8xC	a
metalcore	metalcor	k1gInSc5	metalcor
(	(	kIx(	(
<g/>
slévají	slévat	k5eAaImIp3nP	slévat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
extrémní	extrémní	k2eAgInSc1d1	extrémní
metal	metat	k5eAaImAgInS	metat
a	a	k8xC	a
hardcore	hardcor	k1gInSc5	hardcor
punk	punk	k1gInSc1	punk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nové	nový	k2eAgInPc1d1	nový
styly	styl	k1gInPc1	styl
posunuly	posunout	k5eAaPmAgFnP	posunout
hranice	hranice	k1gFnPc1	hranice
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Parels	Parels	k1gInSc1	Parels
<g/>
,	,	kIx,	,
kritik	kritik	k1gMnSc1	kritik
The	The	k1gFnSc2	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
v	v	k7c6	v
spletitém	spletitý	k2eAgInSc6d1	spletitý
světe	svět	k1gInSc5	svět
populární	populární	k2eAgFnSc3d1	populární
hudby	hudba	k1gFnSc2	hudba
je	on	k3xPp3gInPc4	on
heavy	heav	k1gInPc4	heav
metal	metat	k5eAaImAgMnS	metat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
odnoží	odnoží	k1gNnSc4	odnoží
hard	hard	k1gMnSc1	hard
rocku	rock	k1gInSc2	rock
–	–	k?	–
méně	málo	k6eAd2	málo
synkopickou	synkopický	k2eAgFnSc4d1	synkopická
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
bluesovou	bluesový	k2eAgFnSc4d1	bluesová
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
nakloněnou	nakloněný	k2eAgFnSc7d1	nakloněná
showmanství	showmanství	k1gNnSc4	showmanství
a	a	k8xC	a
hrubé	hrubý	k2eAgFnSc6d1	hrubá
síle	síla	k1gFnSc6	síla
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
<s>
V	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
metalové	metalový	k2eAgFnSc6d1	metalová
kapele	kapela	k1gFnSc6	kapela
nesmí	smět	k5eNaImIp3nS	smět
chybět	chybět	k5eAaImF	chybět
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
taky	taky	k9	taky
instrumentalista	instrumentalista	k1gMnSc1	instrumentalista
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
Nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
však	však	k9	však
zkreslený	zkreslený	k2eAgInSc4d1	zkreslený
kytarový	kytarový	k2eAgInSc4d1	kytarový
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
metalovými	metalový	k2eAgInPc7d1	metalový
uskupeními	uskupení	k1gNnPc7	uskupení
byly	být	k5eAaImAgInP	být
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
Hammondovy	Hammondův	k2eAgInPc1d1	Hammondův
varhany	varhany	k1gInPc1	varhany
a	a	k8xC	a
občas	občas	k6eAd1	občas
taky	taky	k9	taky
mellotron	mellotron	k1gInSc1	mellotron
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
v	v	k7c4	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
nahradily	nahradit	k5eAaPmAgInP	nahradit
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
syntezátory	syntezátor	k1gInPc7	syntezátor
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
stylech	styl	k1gInPc6	styl
jako	jako	k8xC	jako
progresivní	progresivní	k2eAgInSc1d1	progresivní
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
power	power	k1gInSc1	power
metal	metat	k5eAaImAgInS	metat
a	a	k8xC	a
symfonický	symfonický	k2eAgInSc1d1	symfonický
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uplatnění	uplatnění	k1gNnSc1	uplatnění
naleznou	naleznout	k5eAaPmIp3nP	naleznout
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
metalových	metalový	k2eAgInPc6d1	metalový
subžánrech	subžánr	k1gInPc6	subžánr
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
nu	nu	k9	nu
metalové	metalový	k2eAgFnPc1d1	metalová
skupiny	skupina	k1gFnPc1	skupina
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
používají	používat	k5eAaImIp3nP	používat
prvky	prvek	k1gInPc1	prvek
hip	hip	k0	hip
hopu	hopum	k1gNnSc3	hopum
<g/>
:	:	kIx,	:
scratch	scratch	k1gInSc4	scratch
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Zesilovači	zesilovač	k1gInSc3	zesilovač
zvýrazněná	zvýrazněný	k2eAgFnSc1d1	zvýrazněná
akustická	akustický	k2eAgFnSc1d1	akustická
síla	síla	k1gFnSc1	síla
elektrické	elektrický	k2eAgFnSc2d1	elektrická
kytary	kytara	k1gFnSc2	kytara
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
žánru	žánr	k1gInSc2	žánr
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
se	se	k3xPyFc4	se
často	často	k6eAd1	často
užívá	užívat	k5eAaImIp3nS	užívat
tzv.	tzv.	kA	tzv.
overdrive	overdriev	k1gFnSc2	overdriev
pedálu	pedál	k1gInSc2	pedál
společně	společně	k6eAd1	společně
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
zkreslení	zkreslení	k1gNnSc2	zkreslení
samotného	samotný	k2eAgInSc2d1	samotný
elektronkového	elektronkový	k2eAgInSc2d1	elektronkový
zesilovače	zesilovač	k1gInSc2	zesilovač
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
hutný	hutný	k2eAgInSc1d1	hutný
<g/>
,	,	kIx,	,
mocný	mocný	k2eAgMnSc1d1	mocný
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
těžký	těžký	k2eAgInSc1d1	těžký
<g/>
"	"	kIx"	"
zvuk	zvuk	k1gInSc1	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
některé	některý	k3yIgFnSc2	některý
populární	populární	k2eAgFnSc2d1	populární
skupiny	skupina	k1gFnSc2	skupina
začaly	začít	k5eAaPmAgFnP	začít
hrávat	hrávat	k5eAaImF	hrávat
s	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
kytaristy	kytarista	k1gMnPc7	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgNnSc1d1	přední
místo	místo	k1gNnSc1	místo
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
skupinami	skupina	k1gFnPc7	skupina
zastávaly	zastávat	k5eAaImAgFnP	zastávat
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gInSc1	Priest
a	a	k8xC	a
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
si	se	k3xPyFc3	se
kytaristé	kytarista	k1gMnPc1	kytarista
dělí	dělit	k5eAaImIp3nP	dělit
funkce	funkce	k1gFnPc4	funkce
sólové	sólový	k2eAgFnPc4d1	sólová
a	a	k8xC	a
rytmické	rytmický	k2eAgFnPc4d1	rytmická
kytary	kytara	k1gFnPc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
složkou	složka	k1gFnSc7	složka
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
je	být	k5eAaImIp3nS	být
kytarové	kytarový	k2eAgNnSc1d1	kytarové
sólo	sólo	k1gNnSc1	sólo
<g/>
,	,	kIx,	,
forma	forma	k1gFnSc1	forma
kadence	kadence	k1gFnSc1	kadence
<g/>
.	.	kIx.	.
</s>
<s>
Složitá	složitý	k2eAgNnPc1d1	složité
sóla	sólo	k1gNnPc1	sólo
a	a	k8xC	a
riffy	riff	k1gInPc1	riff
jsou	být	k5eAaImIp3nP	být
neoddělitelnou	oddělitelný	k2eNgFnSc7d1	neoddělitelná
součástí	součást	k1gFnSc7	součást
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgFnPc4	jeden
z	z	k7c2	z
pokročilých	pokročilý	k2eAgFnPc2d1	pokročilá
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
využívaných	využívaný	k2eAgFnPc2d1	využívaná
sólových	sólový	k2eAgFnPc2d1	sólová
technik	technika	k1gFnPc2	technika
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
sweep-picking	sweepicking	k1gInSc1	sweep-picking
(	(	kIx(	(
<g/>
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
rozeznívány	rozeznívat	k5eAaImNgFnP	rozeznívat
smýkáním	smýkání	k1gNnSc7	smýkání
trsátka	trsátko	k1gNnSc2	trsátko
po	po	k7c6	po
strunách	struna	k1gFnPc6	struna
bez	bez	k7c2	bez
častého	častý	k2eAgNnSc2d1	časté
střídání	střídání	k1gNnSc2	střídání
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zbytek	zbytek	k1gInSc1	zbytek
zvuku	zvuk	k1gInSc2	zvuk
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
druhou	druhý	k4xOgFnSc7	druhý
rukou	ruka	k1gFnSc7	ruka
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
arpeggio	arpeggio	k1gNnSc4	arpeggio
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tapping	tapping	k1gInSc1	tapping
(	(	kIx(	(
<g/>
ťukání	ťukání	k1gNnSc1	ťukání
prsty	prst	k1gInPc7	prst
do	do	k7c2	do
strun	struna	k1gFnPc2	struna
nad	nad	k7c7	nad
hmatníkem	hmatník	k1gInSc7	hmatník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
úloha	úloha	k1gFnSc1	úloha
kytary	kytara	k1gFnSc2	kytara
v	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
tradičním	tradiční	k2eAgNnSc7d1	tradiční
postavením	postavení	k1gNnSc7	postavení
"	"	kIx"	"
<g/>
frontmana	frontman	k1gMnSc4	frontman
<g/>
"	"	kIx"	"
respektive	respektive	k9	respektive
s	s	k7c7	s
rolí	role	k1gFnSc7	role
vokalisty	vokalista	k1gMnSc2	vokalista
jako	jako	k8xS	jako
lídra	lídr	k1gMnSc2	lídr
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
vzniká	vznikat	k5eAaImIp3nS	vznikat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
napětí	napětí	k1gNnSc1	napětí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
"	"	kIx"	"
<g/>
přátelské	přátelský	k2eAgNnSc4d1	přátelské
soupeření	soupeření	k1gNnSc4	soupeření
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
od	od	k7c2	od
zpěvu	zpěv	k1gInSc2	zpěv
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nedostával	dostávat	k5eNaImAgMnS	dostávat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
před	před	k7c4	před
celkový	celkový	k2eAgInSc4d1	celkový
sound	sound	k1gInSc4	sound
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
metalové	metalový	k2eAgInPc1d1	metalový
kořeny	kořen	k1gInPc1	kořen
sahají	sahat	k5eAaImIp3nP	sahat
ke	k	k7c3	k
kultuře	kultura	k1gFnSc3	kultura
hippies	hippiesa	k1gFnPc2	hippiesa
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
u	u	k7c2	u
zpěvu	zpěv	k1gInSc2	zpěv
"	"	kIx"	"
<g/>
city	cit	k1gInPc1	cit
dávaly	dávat	k5eAaImAgInP	dávat
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
autenticita	autenticita	k1gFnSc1	autenticita
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Kritik	kritik	k1gMnSc1	kritik
Simon	Simon	k1gMnSc1	Simon
Frith	Frith	k1gMnSc1	Frith
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
tón	tón	k1gInSc1	tón
hlasu	hlas	k1gInSc2	hlas
<g/>
"	"	kIx"	"
metalového	metalový	k2eAgMnSc2d1	metalový
zpěváka	zpěvák	k1gMnSc2	zpěvák
má	mít	k5eAaImIp3nS	mít
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
obsahem	obsah	k1gInSc7	obsah
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postřeh	postřeh	k1gInSc1	postřeh
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
častém	častý	k2eAgNnSc6d1	časté
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
úpadku	úpadek	k1gInSc2	úpadek
metalové	metalový	k2eAgFnSc2d1	metalová
tvorby	tvorba	k1gFnSc2	tvorba
do	do	k7c2	do
klišé	klišé	k1gNnSc2	klišé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metalové	metalový	k2eAgFnSc6d1	metalová
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
;	;	kIx,	;
operní	operní	k2eAgInPc4d1	operní
vokály	vokál	k1gInPc4	vokál
Roba	roba	k1gFnSc1	roba
Halforda	Halforda	k1gFnSc1	Halforda
z	z	k7c2	z
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
a	a	k8xC	a
Bruce	Bruce	k1gFnSc1	Bruce
Dickinsona	Dickinsona	k1gFnSc1	Dickinsona
z	z	k7c2	z
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
,	,	kIx,	,
chraplavé	chraplavý	k2eAgInPc1d1	chraplavý
hlasy	hlas	k1gInPc1	hlas
Lemmyho	Lemmy	k1gMnSc2	Lemmy
z	z	k7c2	z
Motörhead	Motörhead	k1gInSc1	Motörhead
a	a	k8xC	a
Jamese	Jamese	k1gFnSc1	Jamese
Hetfielda	Hetfielda	k1gFnSc1	Hetfielda
z	z	k7c2	z
Metallicy	Metallica	k1gFnSc2	Metallica
<g/>
,	,	kIx,	,
přímočaré	přímočarý	k2eAgInPc4d1	přímočarý
vřískavé	vřískavý	k2eAgInPc4d1	vřískavý
a	a	k8xC	a
bručící	bručící	k2eAgInPc4d1	bručící
vokály	vokál	k1gInPc4	vokál
Tomase	Tomasa	k1gFnSc6	Tomasa
Lindberga	Lindberg	k1gMnSc2	Lindberg
z	z	k7c2	z
At	At	k1gFnSc2	At
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
<g/>
,	,	kIx,	,
ďábelské	ďábelský	k2eAgInPc4d1	ďábelský
vokály	vokál	k1gInPc4	vokál
blackmetalových	blackmetalův	k2eAgMnPc2d1	blackmetalův
zpěváků	zpěvák	k1gMnPc2	zpěvák
jako	jako	k8xS	jako
například	například	k6eAd1	například
Attilu	Attila	k1gMnSc4	Attila
Csihare	Csihar	k1gMnSc5	Csihar
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Mayhem	Mayh	k1gInSc7	Mayh
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
baskytary	baskytara	k1gFnSc2	baskytara
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
složkou	složka	k1gFnSc7	složka
metalového	metalový	k2eAgInSc2d1	metalový
zvuku	zvuk	k1gInSc2	zvuk
<g/>
;	;	kIx,	;
souhra	souhra	k1gFnSc1	souhra
baskytary	baskytara	k1gFnSc2	baskytara
a	a	k8xC	a
sólové	sólový	k2eAgFnSc2d1	sólová
kytary	kytara	k1gFnSc2	kytara
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
tónech	tón	k1gInPc6	tón
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
"	"	kIx"	"
<g/>
těžký	těžký	k2eAgInSc4d1	těžký
<g/>
"	"	kIx"	"
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Metalové	metalový	k2eAgFnPc1d1	metalová
basové	basový	k2eAgFnPc1d1	basová
linky	linka	k1gFnPc1	linka
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
různí	různit	k5eAaImIp3nP	různit
v	v	k7c6	v
komplexnosti	komplexnost	k1gFnSc6	komplexnost
<g/>
,	,	kIx,	,
od	od	k7c2	od
"	"	kIx"	"
<g/>
pedálového	pedálový	k2eAgInSc2d1	pedálový
tónu	tón	k1gInSc2	tón
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
akordy	akord	k1gInPc4	akord
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
tón	tón	k1gInSc4	tón
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
základu	základ	k1gInSc2	základ
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
zdvojené	zdvojený	k2eAgInPc4d1	zdvojený
komplexní	komplexní	k2eAgInSc4d1	komplexní
riffy	riff	k1gInPc4	riff
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
lick	lick	k1gMnSc1	lick
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
krátká	krátký	k2eAgFnSc1d1	krátká
fráze	fráze	k1gFnSc1	fráze
během	během	k7c2	během
sóla	sólo	k1gNnSc2	sólo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
sólovou	sólový	k2eAgFnSc7d1	sólová
nebo	nebo	k8xC	nebo
rytmickou	rytmický	k2eAgFnSc7d1	rytmická
kytarou	kytara	k1gFnSc7	kytara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kapelách	kapela	k1gFnPc6	kapela
má	mít	k5eAaImIp3nS	mít
basa	basa	k1gFnSc1	basa
výraznější	výrazný	k2eAgNnSc1d2	výraznější
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
však	však	k9	však
spíše	spíše	k9	spíše
jen	jen	k9	jen
zhutňuje	zhutňovat	k5eAaImIp3nS	zhutňovat
zvuk	zvuk	k1gInSc4	zvuk
kytar	kytara	k1gFnPc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Metaloví	metalový	k2eAgMnPc1d1	metalový
basisté	basista	k1gMnPc1	basista
někdy	někdy	k6eAd1	někdy
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
trsátka	trsátko	k1gNnPc1	trsátko
před	před	k7c7	před
prsty	prst	k1gInPc7	prst
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
zvuk	zvuk	k1gInSc1	zvuk
stává	stávat	k5eAaImIp3nS	stávat
ostřejším	ostrý	k2eAgInSc7d2	ostřejší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
stylech	styl	k1gInPc6	styl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c4	v
thrash	thrash	k1gInSc4	thrash
nebo	nebo	k8xC	nebo
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zkreslení	zkreslení	k1gNnSc1	zkreslení
basy	basa	k1gFnSc2	basa
pomocí	pomocí	k7c2	pomocí
overdrive	overdriev	k1gFnSc2	overdriev
pedálu	pedál	k1gInSc2	pedál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dodává	dodávat	k5eAaImIp3nS	dodávat
tónu	tón	k1gInSc3	tón
baskytary	baskytara	k1gFnSc2	baskytara
hutnější	hutný	k2eAgFnSc2d2	hutnější
a	a	k8xC	a
více	hodně	k6eAd2	hodně
komprimovaný	komprimovaný	k2eAgInSc4d1	komprimovaný
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Numetaloví	Numetalový	k2eAgMnPc1d1	Numetalový
a	a	k8xC	a
deathmetaloví	deathmetalový	k2eAgMnPc1d1	deathmetalový
basisti	basisti	k?	basisti
mnohdy	mnohdy	k6eAd1	mnohdy
používají	používat	k5eAaImIp3nP	používat
pěti-	pěti-	k?	pěti-
nebo	nebo	k8xC	nebo
šestistrunnou	šestistrunný	k2eAgFnSc4d1	šestistrunná
baskytaru	baskytara	k1gFnSc4	baskytara
s	s	k7c7	s
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
zvukovým	zvukový	k2eAgInSc7d1	zvukový
rozsahem	rozsah	k1gInSc7	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Podstata	podstata	k1gFnSc1	podstata
metalového	metalový	k2eAgNnSc2d1	metalové
bubnování	bubnování	k1gNnSc2	bubnování
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
hlasitého	hlasitý	k2eAgNnSc2d1	hlasité
bušení	bušení	k1gNnSc2	bušení
využitím	využití	k1gNnSc7	využití
trojitého	trojitý	k2eAgInSc2d1	trojitý
efektu	efekt	k1gInSc2	efekt
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
preciznosti	preciznost	k1gFnSc2	preciznost
<g/>
.	.	kIx.	.
</s>
<s>
Metalové	metalový	k2eAgNnSc1d1	metalové
bubnování	bubnování	k1gNnSc1	bubnování
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
dávku	dávka	k1gFnSc4	dávka
trpělivosti	trpělivost	k1gFnSc2	trpělivost
<g/>
,	,	kIx,	,
a	a	k8xC	a
hráči	hráč	k1gMnPc1	hráč
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nP	muset
vypěstovat	vypěstovat	k5eAaPmF	vypěstovat
pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
schopnost	schopnost	k1gFnSc4	schopnost
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
koordinace	koordinace	k1gFnSc2	koordinace
a	a	k8xC	a
obratnosti	obratnost	k1gFnSc2	obratnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dokázali	dokázat	k5eAaPmAgMnP	dokázat
zahrát	zahrát	k5eAaPmF	zahrát
někdy	někdy	k6eAd1	někdy
komplikované	komplikovaný	k2eAgFnPc4d1	komplikovaná
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
technik	technika	k1gFnPc2	technika
bubnování	bubnování	k1gNnSc2	bubnování
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
cymbal	cymbal	k1gInSc1	cymbal
choke	chok	k1gInSc2	chok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
utlumení	utlumení	k1gNnSc6	utlumení
činelů	činel	k1gInPc2	činel
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Heavymetalová	heavymetalový	k2eAgFnSc1d1	heavymetalová
souprava	souprava	k1gFnSc1	souprava
bicích	bicí	k2eAgInPc2d1	bicí
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
omnoho	omnoho	k6eAd1	omnoho
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
rockových	rockový	k2eAgInPc6d1	rockový
stylech	styl	k1gInPc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
bicích	bicí	k2eAgInPc2d1	bicí
instrumentů	instrument	k1gInPc2	instrument
v	v	k7c6	v
soupravě	souprava	k1gFnSc6	souprava
(	(	kIx(	(
<g/>
tom-tom	tomom	k1gInSc4	tom-tom
<g/>
,	,	kIx,	,
basový	basový	k2eAgInSc4d1	basový
buben	buben	k1gInSc4	buben
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc4d1	malý
buben	buben	k1gInSc4	buben
<g/>
,	,	kIx,	,
hi-hat	hiat	k5eAaPmF	hi-hat
činel	činel	k1gInSc4	činel
<g/>
,	,	kIx,	,
činely	činela	k1gFnPc4	činela
crash	crasha	k1gFnPc2	crasha
a	a	k8xC	a
ride	ride	k1gFnPc2	ride
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
rovněž	rovněž	k9	rovněž
dvoupedálové	dvoupedálový	k2eAgInPc4d1	dvoupedálový
bicí	bicí	k2eAgInPc4d1	bicí
<g/>
,	,	kIx,	,
přídavné	přídavný	k2eAgInPc4d1	přídavný
tom-tomy	tomom	k1gInPc4	tom-tom
a	a	k8xC	a
množství	množství	k1gNnSc4	množství
přídavných	přídavný	k2eAgInPc2d1	přídavný
činelů	činel	k1gInPc2	činel
(	(	kIx(	(
<g/>
splash	splash	k1gInSc1	splash
činely	činela	k1gFnSc2	činela
a	a	k8xC	a
extra	extra	k2eAgFnPc2d1	extra
crash	crasha	k1gFnPc2	crasha
činely	činela	k1gFnSc2	činela
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
jiné	jiný	k2eAgInPc1d1	jiný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kravské	kravský	k2eAgInPc4d1	kravský
zvonce	zvonec	k1gInPc4	zvonec
<g/>
.	.	kIx.	.
</s>
<s>
Neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
živých	živý	k2eAgNnPc2d1	živé
vystoupení	vystoupení	k1gNnPc2	vystoupení
je	být	k5eAaImIp3nS	být
hluk	hluk	k1gInSc1	hluk
–	–	k?	–
"	"	kIx"	"
<g/>
zvukový	zvukový	k2eAgInSc4d1	zvukový
masakr	masakr	k1gInSc4	masakr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jej	on	k3xPp3gInSc4	on
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
Deena	Deena	k1gFnSc1	Deena
Weinstein	Weinsteina	k1gFnPc2	Weinsteina
<g/>
.	.	kIx.	.
</s>
<s>
Jeffrey	Jeffrea	k1gFnPc1	Jeffrea
Arnett	Arnetta	k1gFnPc2	Arnetta
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Metalheads	Metalheads	k1gInSc1	Metalheads
o	o	k7c4	o
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
smyslovém	smyslový	k2eAgInSc6d1	smyslový
ekvivalentu	ekvivalent	k1gInSc6	ekvivalent
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kráčejíc	kráčet	k5eAaImSgFnS	kráčet
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
Jimiho	Jimi	k1gMnSc2	Jimi
Hendrixe	Hendrixe	k1gFnSc1	Hendrixe
a	a	k8xC	a
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
<g/>
,	,	kIx,	,
průkopníci	průkopník	k1gMnPc1	průkopník
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
jako	jako	k8xS	jako
např.	např.	kA	např.
Blue	Blue	k1gFnSc7	Blue
Cheer	Cheer	k1gInSc4	Cheer
zavedli	zavést	k5eAaPmAgMnP	zavést
nové	nový	k2eAgInPc4d1	nový
standardy	standard	k1gInPc4	standard
ohledně	ohledně	k7c2	ohledně
hlasitosti	hlasitost	k1gFnSc2	hlasitost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
praví	pravit	k5eAaBmIp3nS	pravit
Dickie	Dickie	k1gFnSc1	Dickie
Peterson	Peterson	k1gInSc4	Peterson
z	z	k7c2	z
Blue	Blu	k1gInSc2	Blu
Cheer	Chera	k1gFnPc2	Chera
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Věděli	vědět	k5eAaImAgMnP	vědět
jsme	být	k5eAaImIp1nP	být
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chceme	chtít	k5eAaImIp1nP	chtít
intenzivnější	intenzivní	k2eAgFnSc4d2	intenzivnější
muziku	muzika	k1gFnSc4	muzika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Paul	Paul	k1gMnSc1	Paul
Sutcliffe	Sutcliff	k1gInSc5	Sutcliff
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
koncertu	koncert	k1gInSc6	koncert
Motörhead	Motörhead	k1gInSc1	Motörhead
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
především	především	k9	především
neúměrná	úměrný	k2eNgFnSc1d1	neúměrná
hlasitost	hlasitost	k1gFnSc1	hlasitost
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
na	na	k7c4	na
vrub	vrub	k1gInSc4	vrub
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
bavíme	bavit	k5eAaImIp1nP	bavit
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
vlivu	vliv	k1gInSc6	vliv
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
taky	taky	k6eAd1	taky
Weinstein	Weinstein	k2eAgMnSc1d1	Weinstein
<g/>
,	,	kIx,	,
když	když	k8xS	když
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
melodie	melodie	k1gFnSc1	melodie
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
prvkem	prvek	k1gInSc7	prvek
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
house	house	k1gNnSc1	house
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
rytmiku	rytmika	k1gFnSc4	rytmika
<g/>
,	,	kIx,	,
a	a	k8xC	a
heavy	heava	k1gFnPc1	heava
metalu	metal	k1gInSc2	metal
trůní	trůnit	k5eAaImIp3nP	trůnit
energický	energický	k2eAgInSc4d1	energický
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
hlasu	hlas	k1gInSc2	hlas
a	a	k8xC	a
hlasitost	hlasitost	k1gFnSc1	hlasitost
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hluk	hluk	k1gInSc1	hluk
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
"	"	kIx"	"
<g/>
vtáhnout	vtáhnout	k5eAaPmF	vtáhnout
posluchače	posluchač	k1gMnPc4	posluchač
do	do	k7c2	do
zvuku	zvuk	k1gInSc2	zvuk
<g/>
"	"	kIx"	"
a	a	k8xC	a
poskytnout	poskytnout	k5eAaPmF	poskytnout
"	"	kIx"	"
<g/>
injekci	injekce	k1gFnSc4	injekce
mladistvé	mladistvý	k2eAgFnSc2d1	mladistvá
živosti	živost	k1gFnSc2	živost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Soustředění	soustředění	k1gNnSc1	soustředění
na	na	k7c4	na
hlučnost	hlučnost	k1gFnSc4	hlučnost
se	se	k3xPyFc4	se
vysmívá	vysmívat	k5eAaImIp3nS	vysmívat
rockový	rockový	k2eAgInSc1d1	rockový
pseudo-dokument	pseudookument	k1gInSc1	pseudo-dokument
Hraje	hrát	k5eAaImIp3nS	hrát
skupina	skupina	k1gFnSc1	skupina
Spinal	spinal	k1gInSc4	spinal
Tap	Tap	k1gFnSc2	Tap
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
jistý	jistý	k2eAgMnSc1d1	jistý
kytarista	kytarista	k1gMnSc1	kytarista
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
aparaturu	aparatura	k1gFnSc4	aparatura
nastavenou	nastavený	k2eAgFnSc4d1	nastavená
"	"	kIx"	"
<g/>
až	až	k6eAd1	až
na	na	k7c4	na
jedenáct	jedenáct	k4xCc4	jedenáct
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Heavy	Heava	k1gFnPc4	Heava
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
energickým	energický	k2eAgNnSc7d1	energické
tempem	tempo	k1gNnSc7	tempo
a	a	k8xC	a
razantností	razantnost	k1gFnSc7	razantnost
<g/>
.	.	kIx.	.
</s>
<s>
Strohé	strohý	k2eAgFnPc1d1	strohá
<g/>
,	,	kIx,	,
prudké	prudký	k2eAgFnPc1d1	prudká
a	a	k8xC	a
izolované	izolovaný	k2eAgFnPc1d1	izolovaná
rytmické	rytmický	k2eAgFnPc1d1	rytmická
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
do	do	k7c2	do
rytmických	rytmický	k2eAgFnPc2d1	rytmická
frází	fráze	k1gFnPc2	fráze
s	s	k7c7	s
osobitou	osobitý	k2eAgFnSc7d1	osobitá
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
trhavou	trhavý	k2eAgFnSc7d1	trhavá
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
fráze	fráze	k1gFnPc1	fráze
slouží	sloužit	k5eAaImIp3nP	sloužit
za	za	k7c4	za
základ	základ	k1gInSc4	základ
rytmických	rytmický	k2eAgInPc2d1	rytmický
průvodů	průvod	k1gInPc2	průvod
a	a	k8xC	a
melodických	melodický	k2eAgFnPc2d1	melodická
figur	figura	k1gFnPc2	figura
zvaných	zvaný	k2eAgInPc2d1	zvaný
riffy	riff	k1gInPc7	riff
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
"	"	kIx"	"
<g/>
chytlavost	chytlavost	k1gFnSc1	chytlavost
<g/>
"	"	kIx"	"
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
heavymetalových	heavymetalový	k2eAgFnPc6d1	heavymetalová
písních	píseň	k1gFnPc6	píseň
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
delší	dlouhý	k2eAgFnPc1d2	delší
rytmické	rytmický	k2eAgFnPc1d1	rytmická
figury	figura	k1gFnPc1	figura
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
celá	celý	k2eAgFnSc1d1	celá
nota	nota	k1gFnSc1	nota
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
čtvrťová	čtvrťový	k2eAgFnSc1d1	čtvrťová
nota	nota	k1gFnSc1	nota
s	s	k7c7	s
tečkou	tečka	k1gFnSc7	tečka
nebo	nebo	k8xC	nebo
delší	dlouhý	k2eAgInPc1d2	delší
akordy	akord	k1gInPc1	akord
v	v	k7c6	v
pomalých	pomalý	k2eAgNnPc6d1	pomalé
"	"	kIx"	"
<g/>
power	power	k1gMnSc1	power
baladách	balada	k1gFnPc6	balada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
mělo	mít	k5eAaImAgNnS	mít
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
pomalosti	pomalost	k1gFnSc3	pomalost
až	až	k8xS	až
těžkopádnosti	těžkopádnost	k1gFnSc3	těžkopádnost
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
živou	živý	k2eAgFnSc7d1	živá
součástí	součást	k1gFnSc7	součást
žánru	žánr	k1gInSc2	žánr
rozmanité	rozmanitý	k2eAgFnPc4d1	rozmanitá
tempa	tempo	k1gNnPc4	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Metalové	metalový	k2eAgNnSc1d1	metalové
tempa	tempo	k1gNnSc2	tempo
první	první	k4xOgFnSc2	první
dekády	dekáda	k1gFnSc2	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
pomalých	pomalý	k2eAgNnPc2d1	pomalé
baladických	baladický	k2eAgNnPc2d1	baladické
temp	tempo	k1gNnPc2	tempo
(	(	kIx(	(
<g/>
čtvrtinová	čtvrtinový	k2eAgFnSc1d1	čtvrtinová
nota	nota	k1gFnSc1	nota
=	=	kIx~	=
60	[number]	k4	60
úderů	úder	k1gInPc2	úder
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
extrémně	extrémně	k6eAd1	extrémně
rychlé	rychlý	k2eAgNnSc4d1	rychlé
tempa	tempo	k1gNnPc4	tempo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
blast	blast	k5eAaPmF	blast
beaty	beat	k1gInPc4	beat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čtvrtinová	čtvrtinový	k2eAgFnSc1d1	čtvrtinová
nota	nota	k1gFnSc1	nota
=	=	kIx~	=
350	[number]	k4	350
úderů	úder	k1gInPc2	úder
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
heavymetalových	heavymetalový	k2eAgFnPc2d1	heavymetalová
skladeb	skladba	k1gFnPc2	skladba
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
krátké	krátká	k1gFnPc1	krátká
dvounotové	dvounotový	k2eAgFnPc1d1	dvounotový
nebo	nebo	k8xC	nebo
třínotové	třínotový	k2eAgInPc1d1	třínotový
akordy	akord	k1gInPc1	akord
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
barré	barrý	k2eAgInPc1d1	barrý
hmaty	hmat	k1gInPc1	hmat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
akordy	akord	k1gInPc1	akord
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
přednášené	přednášený	k2eAgFnPc1d1	přednášená
pomocí	pomocí	k7c2	pomocí
staccatového	staccatový	k2eAgInSc2d1	staccatový
útoku	útok	k1gInSc2	útok
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
použitím	použití	k1gNnSc7	použití
kytarové	kytarový	k2eAgFnSc2d1	kytarová
techniky	technika	k1gFnSc2	technika
zvané	zvaný	k2eAgFnSc2d1	zvaná
"	"	kIx"	"
<g/>
palm	palma	k1gFnPc2	palma
muting	muting	k1gInSc1	muting
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
výrazovým	výrazový	k2eAgInSc7d1	výrazový
prostředkem	prostředek	k1gInSc7	prostředek
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
power	power	k1gInSc1	power
akordy	akord	k1gInPc1	akord
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
silové	silový	k2eAgInPc1d1	silový
akordy	akord	k1gInPc1	akord
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
silový	silový	k2eAgInSc4d1	silový
akord	akord	k1gInSc4	akord
<g/>
"	"	kIx"	"
poměrně	poměrně	k6eAd1	poměrně
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
souzvuk	souzvuk	k1gInSc4	souzvuk
dvou	dva	k4xCgInPc2	dva
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
základního	základní	k2eAgInSc2d1	základní
tónu	tón	k1gInSc2	tón
a	a	k8xC	a
kvinty	kvinta	k1gFnSc2	kvinta
<g/>
.	.	kIx.	.
</s>
<s>
Souzvuk	souzvuk	k1gInSc1	souzvuk
kvinty	kvinta	k1gFnSc2	kvinta
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
ještě	ještě	k6eAd1	ještě
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k9	tak
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
plnějšího	plný	k2eAgInSc2d2	plnější
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
harmonická	harmonický	k2eAgFnSc1d1	harmonická
průzračnost	průzračnost	k1gFnSc1	průzračnost
a	a	k8xC	a
čitelnost	čitelnost	k1gFnSc1	čitelnost
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
také	také	k9	také
u	u	k7c2	u
zkresleného	zkreslený	k2eAgInSc2d1	zkreslený
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Dvojhmaty	dvojhmat	k1gInPc1	dvojhmat
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
hrají	hrát	k5eAaImIp3nP	hrát
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ukazováček	ukazováček	k1gInSc1	ukazováček
vždy	vždy	k6eAd1	vždy
drží	držet	k5eAaImIp3nS	držet
určitý	určitý	k2eAgInSc1d1	určitý
pražec	pražec	k1gInSc1	pražec
a	a	k8xC	a
prsteník	prsteník	k1gInSc1	prsteník
zároveň	zároveň	k6eAd1	zároveň
mačká	mačkat	k5eAaImIp3nS	mačkat
na	na	k7c6	na
tenčí	tenký	k2eAgFnSc6d2	tenčí
struně	struna	k1gFnSc6	struna
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
pražce	pražec	k1gInPc4	pražec
vyšší	vysoký	k2eAgInSc4d2	vyšší
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Dvojhmaty	dvojhmat	k1gInPc1	dvojhmat
jsou	být	k5eAaImIp3nP	být
výhodné	výhodný	k2eAgFnPc4d1	výhodná
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prstoklad	prstoklad	k1gInSc1	prstoklad
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
můžeme	moct	k5eAaImIp1nP	moct
ho	on	k3xPp3gMnSc4	on
posunovat	posunovat	k5eAaImF	posunovat
kdekoliv	kdekoliv	k6eAd1	kdekoliv
po	po	k7c6	po
hmatníku	hmatník	k1gInSc6	hmatník
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
dalšího	další	k2eAgInSc2d1	další
prstu	prst	k1gInSc2	prst
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
méně	málo	k6eAd2	málo
používaný	používaný	k2eAgInSc1d1	používaný
trojhmat	trojhmat	k1gInSc1	trojhmat
<g/>
.	.	kIx.	.
</s>
<s>
Heavy	Heava	k1gFnPc1	Heava
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
riffech	riff	k1gInPc6	riff
<g/>
.	.	kIx.	.
</s>
<s>
Riffy	Riff	k1gInPc1	Riff
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
tvoří	tvořit	k5eAaImIp3nP	tvořit
pomocí	pomocí	k7c2	pomocí
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
harmonických	harmonický	k2eAgFnPc2d1	harmonická
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
těchto	tento	k3xDgInPc6	tento
<g/>
:	:	kIx,	:
modální	modální	k2eAgFnSc1d1	modální
harmonie	harmonie	k1gFnSc1	harmonie
<g/>
,	,	kIx,	,
tritón	tritón	k1gInSc1	tritón
plus	plus	k1gInSc1	plus
chromatismus	chromatismus	k1gInSc4	chromatismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
pedálový	pedálový	k2eAgInSc1d1	pedálový
tón	tón	k1gInSc1	tón
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
heavy	heava	k1gFnPc1	heava
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
nejednou	jednou	k6eNd1	jednou
využívají	využívat	k5eAaImIp3nP	využívat
modální	modální	k2eAgFnPc4d1	modální
stupnice	stupnice	k1gFnPc4	stupnice
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
aiolský	aiolský	k2eAgInSc4d1	aiolský
a	a	k8xC	a
frygický	frygický	k2eAgInSc4d1	frygický
modus	modus	k1gInSc4	modus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žánr	žánr	k1gInSc1	žánr
využívá	využívat	k5eAaImIp3nS	využívat
modální	modální	k2eAgFnPc4d1	modální
akordové	akordový	k2eAgFnPc4d1	akordová
progrese	progrese	k1gFnPc4	progrese
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
aiolská	aiolský	k2eAgFnSc1d1	aiolská
progrese	progrese	k1gFnSc1	progrese
I-VI-VII	I-VI-VII	k1gFnSc1	I-VI-VII
<g/>
,	,	kIx,	,
I-VII-	I-VII-	k1gFnSc1	I-VII-
<g/>
(	(	kIx(	(
<g/>
VI	VI	kA	VI
<g/>
)	)	kIx)	)
případně	případně	k6eAd1	případně
I-VI-IV-VII	I-VI-IV-VII	k1gFnSc1	I-VI-IV-VII
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
frygické	frygický	k2eAgFnPc1d1	frygická
progrese	progrese	k1gFnPc1	progrese
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
I	I	kA	I
a	a	k8xC	a
♭	♭	k?	♭
<g/>
II	II	kA	II
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
I-	I-	k1gMnSc1	I-
<g/>
♭	♭	k?	♭
<g/>
II-I	II-I	k1gMnSc1	II-I
<g/>
,	,	kIx,	,
I-	I-	k1gMnSc1	I-
<g/>
♭	♭	k?	♭
<g/>
II-III	II-III	k1gMnSc1	II-III
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
I-	I-	k1gMnSc1	I-
<g/>
♭	♭	k?	♭
<g/>
II-VII	II-VII	k1gMnSc1	II-VII
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
využití	využití	k1gNnSc2	využití
aiolského	aiolský	k2eAgInSc2d1	aiolský
módu	mód	k1gInSc2	mód
<g/>
:	:	kIx,	:
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
-	-	kIx~	-
Breaking	Breaking	k1gInSc1	Breaking
the	the	k?	the
Law	Law	k1gFnSc2	Law
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
riff	riff	k1gMnSc1	riff
I-VI-VII	I-VI-VII	k1gMnSc1	I-VI-VII
<g/>
)	)	kIx)	)
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
-	-	kIx~	-
Hallowed	Hallowed	k1gInSc1	Hallowed
Be	Be	k1gMnSc2	Be
Thy	Thy	k1gMnSc2	Thy
Name	Nam	k1gMnSc2	Nam
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
riff	riff	k1gMnSc1	riff
I-VI-VII	I-VI-VII	k1gMnSc1	I-VI-VII
<g/>
)	)	kIx)	)
Accept	Accept	k1gMnSc1	Accept
-	-	kIx~	-
Princess	Princess	k1gInSc1	Princess
of	of	k?	of
the	the	k?	the
Dawn	Dawn	k1gInSc1	Dawn
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
riff	riff	k1gMnSc1	riff
I-VI-VII	I-VI-VII	k1gMnSc1	I-VI-VII
<g/>
)	)	kIx)	)
Příklady	příklad	k1gInPc1	příklad
využití	využití	k1gNnSc1	využití
frygického	frygický	k2eAgInSc2d1	frygický
módu	mód	k1gInSc2	mód
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Mercyful	Mercyful	k1gInSc1	Mercyful
Fate	Fate	k1gNnSc1	Fate
-	-	kIx~	-
Gypsy	gyps	k1gInPc1	gyps
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
riff	riff	k1gMnSc1	riff
I-	I-	k1gMnSc1	I-
<g/>
♭	♭	k?	♭
<g/>
II-I-VI-V	II-I-VI-V	k1gMnSc1	II-I-VI-V
<g/>
)	)	kIx)	)
Megadeth	Megadeth	k1gMnSc1	Megadeth
-	-	kIx~	-
Symphony	Symphon	k1gInPc1	Symphon
of	of	k?	of
Destruction	Destruction	k1gInSc1	Destruction
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
riff	riff	k1gMnSc1	riff
♭	♭	k?	♭
<g/>
II-I	II-I	k1gMnSc1	II-I
<g/>
)	)	kIx)	)
Sodom	Sodoma	k1gFnPc2	Sodoma
-	-	kIx~	-
Remember	Remember	k1gInSc1	Remember
the	the	k?	the
Fallen	Fallen	k1gInSc1	Fallen
(	(	kIx(	(
<g/>
intro	intro	k1gNnSc1	intro
+	+	kIx~	+
hlavní	hlavní	k2eAgInSc1d1	hlavní
riff	riff	k1gInSc1	riff
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
frygická	frygický	k2eAgFnSc1d1	frygická
kadence	kadence	k1gFnSc1	kadence
<g/>
:	:	kIx,	:
I-	I-	k1gMnSc1	I-
<g/>
♭	♭	k?	♭
<g/>
II-III	II-III	k1gMnSc1	II-III
<g/>
)	)	kIx)	)
Tritón	Tritón	k1gMnSc1	Tritón
a	a	k8xC	a
chromatika	chromatika	k1gFnSc1	chromatika
se	se	k3xPyFc4	se
v	v	k7c4	v
heavy	heav	k1gInPc4	heav
metalu	metal	k1gInSc2	metal
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
akordových	akordový	k2eAgFnPc2d1	akordová
progresí	progrese	k1gFnPc2	progrese
<g/>
.	.	kIx.	.
</s>
<s>
Tritón	tritón	k1gInSc1	tritón
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
zvětšená	zvětšený	k2eAgFnSc1d1	zvětšená
kvarta	kvarta	k1gFnSc1	kvarta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
interval	interval	k1gInSc4	interval
obsahující	obsahující	k2eAgInPc4d1	obsahující
tři	tři	k4xCgInPc4	tři
celé	celý	k2eAgInPc4d1	celý
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
středověkém	středověký	k2eAgInSc6d1	středověký
<g/>
,	,	kIx,	,
přísně	přísně	k6eAd1	přísně
harmonickém	harmonický	k2eAgInSc6d1	harmonický
stylu	styl	k1gInSc6	styl
povolený	povolený	k2eAgInSc4d1	povolený
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
mnichy	mnich	k1gMnPc4	mnich
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
diabolus	diabolus	k1gMnSc1	diabolus
in	in	k?	in
musica	musica	k1gMnSc1	musica
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
se	se	k3xPyFc4	se
tritón	tritón	k1gInSc1	tritón
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
něco	něco	k3yInSc1	něco
"	"	kIx"	"
<g/>
ďábelského	ďábelský	k2eAgMnSc2d1	ďábelský
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
má	mít	k5eAaImIp3nS	mít
tritón	tritón	k1gMnSc1	tritón
nezastupitelné	zastupitelný	k2eNgNnSc4d1	nezastupitelné
místo	místo	k1gNnSc4	místo
při	při	k7c6	při
kytarových	kytarový	k2eAgNnPc6d1	kytarové
sólech	sólo	k1gNnPc6	sólo
a	a	k8xC	a
riffech	riff	k1gInPc6	riff
–	–	k?	–
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
začátek	začátek	k1gInSc1	začátek
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
jako	jako	k9	jako
harmonický	harmonický	k2eAgInSc1d1	harmonický
základ	základ	k1gInSc1	základ
hodně	hodně	k6eAd1	hodně
využívá	využívat	k5eAaPmIp3nS	využívat
pedálový	pedálový	k2eAgInSc4d1	pedálový
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hluboký	hluboký	k2eAgInSc4d1	hluboký
permanentní	permanentní	k2eAgInSc4d1	permanentní
tón	tón	k1gInSc4	tón
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
cizí	cizí	k2eAgInSc4d1	cizí
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
disharmonický	disharmonický	k2eAgInSc1d1	disharmonický
<g/>
)	)	kIx)	)
souzvuk	souzvuk	k1gInSc1	souzvuk
<g/>
.	.	kIx.	.
</s>
<s>
Heavymetalové	heavymetalový	k2eAgInPc1d1	heavymetalový
riffy	riff	k1gInPc1	riff
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
zakládají	zakládat	k5eAaImIp3nP	zakládat
na	na	k7c6	na
nepřetržitém	přetržitý	k2eNgNnSc6d1	nepřetržité
opakování	opakování	k1gNnSc6	opakování
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
nejhrubších	hrubý	k2eAgFnPc6d3	nejhrubší
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
nejnižších	nízký	k2eAgFnPc6d3	nejnižší
<g/>
)	)	kIx)	)
strunách	struna	k1gFnPc6	struna
basové	basový	k2eAgFnSc2d1	basová
nebo	nebo	k8xC	nebo
rytmické	rytmický	k2eAgFnSc2d1	rytmická
kytary	kytara	k1gFnSc2	kytara
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
struny	struna	k1gFnPc4	struna
E	E	kA	E
<g/>
,	,	kIx,	,
A	A	kA	A
nebo	nebo	k8xC	nebo
D.	D.	kA	D.
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
basový	basový	k2eAgInSc1d1	basový
tón	tón	k1gInSc1	tón
<g/>
–	–	k?	–
<g/>
nejčastěji	často	k6eAd3	často
nízké	nízký	k2eAgInPc1d1	nízký
tóny	tón	k1gInPc1	tón
E	E	kA	E
nebo	nebo	k8xC	nebo
A	A	kA	A
<g/>
–	–	k?	–
<g/>
se	se	k3xPyFc4	se
soustavně	soustavně	k6eAd1	soustavně
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
hrají	hrát	k5eAaImIp3nP	hrát
různé	různý	k2eAgInPc4d1	různý
akordy	akord	k1gInPc4	akord
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
akordů	akord	k1gInPc2	akord
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obvykle	obvykle	k6eAd1	obvykle
nezahrnují	zahrnovat	k5eNaImIp3nP	zahrnovat
onen	onen	k3xDgInSc4	onen
basový	basový	k2eAgInSc4d1	basový
tón	tón	k1gInSc4	tón
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
zdání	zdání	k1gNnSc1	zdání
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
úvodní	úvodní	k2eAgInSc4d1	úvodní
riff	riff	k1gInSc4	riff
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
You	You	k1gFnPc4	You
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
Another	Anothra	k1gFnPc2	Anothra
Thing	Thing	k1gMnSc1	Thing
Comin	Comin	k1gMnSc1	Comin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
od	od	k7c2	od
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jedna	jeden	k4xCgFnSc1	jeden
kytara	kytara	k1gFnSc1	kytara
hraje	hrát	k5eAaImIp3nS	hrát
pedálový	pedálový	k2eAgInSc4d1	pedálový
tón	tón	k1gInSc4	tón
F	F	kA	F
<g/>
#	#	kIx~	#
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
kytara	kytara	k1gFnSc1	kytara
hraje	hrát	k5eAaImIp3nS	hrát
akordy	akord	k1gInPc4	akord
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Roberta	Robert	k1gMnSc2	Robert
Walsere	Walser	k1gInSc5	Walser
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
heavy	heava	k1gFnPc4	heava
metal	metal	k1gInSc4	metal
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
už	už	k6eAd1	už
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
žánru	žánr	k1gInSc2	žánr
různé	různý	k2eAgInPc1d1	různý
hudební	hudební	k2eAgInPc1d1	hudební
styly	styl	k1gInPc1	styl
známé	známý	k2eAgInPc1d1	známý
pod	pod	k7c7	pod
společným	společný	k2eAgInSc7d1	společný
názvem	název	k1gInSc7	název
'	'	kIx"	'
<g/>
klasická	klasický	k2eAgFnSc1d1	klasická
hudba	hudba	k1gFnSc1	hudba
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
metalové	metalový	k2eAgMnPc4d1	metalový
hráče	hráč	k1gMnPc4	hráč
patří	patřit	k5eAaImIp3nP	patřit
kytaristé	kytarista	k1gMnPc1	kytarista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaPmAgMnP	věnovat
rovněž	rovněž	k9	rovněž
studiu	studio	k1gNnSc3	studio
klasické	klasický	k2eAgFnSc2d1	klasická
(	(	kIx(	(
<g/>
vážné	vážný	k2eAgFnSc2d1	vážná
<g/>
)	)	kIx)	)
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Osvojením	osvojení	k1gNnSc7	osvojení
a	a	k8xC	a
adaptováním	adaptování	k1gNnSc7	adaptování
předloh	předloha	k1gFnPc2	předloha
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
položili	položit	k5eAaPmAgMnP	položit
základ	základ	k1gInSc4	základ
novému	nový	k2eAgMnSc3d1	nový
druhu	druh	k1gMnSc3	druh
kytarové	kytarový	k2eAgFnSc2d1	kytarová
virtuozity	virtuozita	k1gFnSc2	virtuozita
a	a	k8xC	a
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
harmonickém	harmonický	k2eAgInSc6d1	harmonický
a	a	k8xC	a
melodickém	melodický	k2eAgInSc6d1	melodický
jazyce	jazyk	k1gInSc6	jazyk
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hudebních	hudební	k2eAgInPc2d1	hudební
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
osvojili	osvojit	k5eAaPmAgMnP	osvojit
heavymetaloví	heavymetalový	k2eAgMnPc1d1	heavymetalový
muzikanti	muzikant	k1gMnPc1	muzikant
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
baroku	baroko	k1gNnSc6	baroko
<g/>
,	,	kIx,	,
klasicismu	klasicismus	k1gInSc6	klasicismus
a	a	k8xC	a
romantismu	romantismus	k1gInSc6	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
z	z	k7c2	z
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc2	Purple
a	a	k8xC	a
Rainbow	Rainbow	k1gMnSc2	Rainbow
<g/>
,	,	kIx,	,
a	a	k8xC	a
Uli	Uli	k1gMnSc1	Uli
Jon	Jon	k1gMnSc1	Jon
Roth	Roth	k1gMnSc1	Roth
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
ze	z	k7c2	z
Scorpions	Scorpionsa	k1gFnPc2	Scorpionsa
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
počátkem	počátkem	k7c2	počátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
hudebními	hudební	k2eAgFnPc7d1	hudební
figurami	figura	k1gFnPc7	figura
vypůjčenými	vypůjčený	k2eAgFnPc7d1	vypůjčená
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
kytaristé	kytarista	k1gMnPc1	kytarista
Randy	rand	k1gInPc4	rand
Rhoads	Rhoadsa	k1gFnPc2	Rhoadsa
a	a	k8xC	a
Yngwie	Yngwie	k1gFnSc2	Yngwie
Malmsteen	Malmstena	k1gFnPc2	Malmstena
nechali	nechat	k5eAaPmAgMnP	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
barokními	barokní	k2eAgInPc7d1	barokní
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
taktéž	taktéž	k?	taktéž
klasicistickými	klasicistický	k2eAgFnPc7d1	klasicistická
hudebními	hudební	k2eAgFnPc7d1	hudební
formami	forma	k1gFnPc7	forma
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
neoklasicistické	neoklasicistický	k2eAgMnPc4d1	neoklasicistický
metalové	metalový	k2eAgMnPc4d1	metalový
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgInPc3	který
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Michael	Michael	k1gMnSc1	Michael
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Angelo	Angela	k1gFnSc5	Angela
Batio	Batio	k6eAd1	Batio
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tony	Tony	k1gMnSc1	Tony
MacAlpine	MacAlpin	k1gInSc5	MacAlpin
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
množství	množství	k1gNnSc1	množství
metalových	metalový	k2eAgMnPc2d1	metalový
muzikantů	muzikant	k1gMnPc2	muzikant
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nechali	nechat	k5eAaPmAgMnP	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
skladateli	skladatel	k1gMnSc3	skladatel
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
heavy	heavo	k1gNnPc7	heavo
metal	metal	k1gInSc1	metal
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
přímo	přímo	k6eAd1	přímo
nenavazuje	navazovat	k5eNaImIp3nS	navazovat
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
a	a	k8xC	a
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
hudba	hudba	k1gFnSc1	hudba
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odlišných	odlišný	k2eAgFnPc2d1	odlišná
kulturních	kulturní	k2eAgFnPc2d1	kulturní
tradicí	tradice	k1gFnPc2	tradice
a	a	k8xC	a
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
:	:	kIx,	:
klasická	klasický	k2eAgFnSc1d1	klasická
hudba	hudba	k1gFnSc1	hudba
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
umělecké	umělecký	k2eAgFnSc2d1	umělecká
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
metal	metal	k1gInSc1	metal
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Muzikologové	muzikolog	k1gMnPc1	muzikolog
Nicolas	Nicolas	k1gMnSc1	Nicolas
Cook	Cook	k1gMnSc1	Cook
a	a	k8xC	a
Nicola	Nicola	k1gFnSc1	Nicola
Dibben	Dibbna	k1gFnPc2	Dibbna
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
poznamenávají	poznamenávat	k5eAaImIp3nP	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
analýza	analýza	k1gFnSc1	analýza
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
často	často	k6eAd1	často
ukáže	ukázat	k5eAaPmIp3nS	ukázat
vliv	vliv	k1gInSc4	vliv
'	'	kIx"	'
<g/>
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
tradic	tradice	k1gFnPc2	tradice
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Walserovo	Walserův	k2eAgNnSc1d1	Walserovo
spojení	spojení	k1gNnSc1	spojení
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
s	s	k7c7	s
filozofií	filozofie	k1gFnSc7	filozofie
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
s	s	k7c7	s
vnějšími	vnější	k2eAgInPc7d1	vnější
projevy	projev	k1gInPc7	projev
romantismu	romantismus	k1gInSc2	romantismus
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
mylné	mylný	k2eAgNnSc4d1	mylné
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
blues	blues	k1gInSc1	blues
<g/>
,	,	kIx,	,
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
heavy	heava	k1gFnPc1	heava
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
rap	rap	k1gMnSc1	rap
či	či	k8xC	či
taneční	taneční	k2eAgFnSc1d1	taneční
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
odvíjejí	odvíjet	k5eAaImIp3nP	odvíjet
od	od	k7c2	od
'	'	kIx"	'
<g/>
umělecké	umělecký	k2eAgFnSc2d1	umělecká
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
Výpůjčky	výpůjčka	k1gFnSc2	výpůjčka
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
zejména	zejména	k9	zejména
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
melodií	melodie	k1gFnPc2	melodie
a	a	k8xC	a
stupnic	stupnice	k1gFnPc2	stupnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
komplikovaných	komplikovaný	k2eAgFnPc2d1	komplikovaná
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
kontrapunkt	kontrapunkt	k1gInSc4	kontrapunkt
<g/>
,	,	kIx,	,
vícehlas	vícehlas	k1gInSc4	vícehlas
nebo	nebo	k8xC	nebo
klasická	klasický	k2eAgNnPc4d1	klasické
strukturální	strukturální	k2eAgNnPc4d1	strukturální
schémata	schéma	k1gNnPc4	schéma
<g/>
.	.	kIx.	.
</s>
<s>
Heavymetalové	heavymetalový	k2eAgFnPc1d1	heavymetalová
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
progresivních	progresivní	k2eAgFnPc2d1	progresivní
a	a	k8xC	a
neoklasických	neoklasický	k2eAgFnPc2d1	neoklasická
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obyčejně	obyčejně	k6eAd1	obyčejně
nesnaží	snažit	k5eNaImIp3nS	snažit
dodržovat	dodržovat	k5eAaImF	dodržovat
kompoziční	kompoziční	k2eAgInPc4d1	kompoziční
a	a	k8xC	a
estetické	estetický	k2eAgInPc4d1	estetický
požadavky	požadavek	k1gInPc4	požadavek
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Heavy	Heava	k1gFnPc1	Heava
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
blues	blues	k1gFnSc1	blues
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc7	kořen
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
často	často	k6eAd1	často
sexuální	sexuální	k2eAgInSc1d1	sexuální
podtón	podtón	k1gInSc1	podtón
–	–	k?	–
počínaje	počínaje	k7c7	počínaje
podmanivými	podmanivý	k2eAgFnPc7d1	podmanivá
nahrávkami	nahrávka	k1gFnPc7	nahrávka
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgMnSc1d1	Zeppelin
a	a	k8xC	a
konče	konče	k7c7	konče
současnou	současný	k2eAgFnSc7d1	současná
numetalovou	numetalův	k2eAgFnSc7d1	numetalův
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
cizí	cizí	k2eAgFnPc1d1	cizí
otevřené	otevřený	k2eAgFnPc1d1	otevřená
sexuální	sexuální	k2eAgFnPc1d1	sexuální
narážky	narážka	k1gFnPc1	narážka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
oblibou	obliba	k1gFnSc7	obliba
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnPc4d2	veliký
množství	množství	k1gNnPc4	množství
metalových	metalový	k2eAgFnPc2d1	metalová
písní	píseň	k1gFnPc2	píseň
začalo	začít	k5eAaPmAgNnS	začít
zabývat	zabývat	k5eAaImF	zabývat
politikou	politika	k1gFnSc7	politika
a	a	k8xC	a
sociálními	sociální	k2eAgFnPc7d1	sociální
otázkami	otázka	k1gFnPc7	otázka
(	(	kIx(	(
<g/>
System	Syst	k1gInSc7	Syst
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gMnSc1	Down
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Romantika	romantika	k1gFnSc1	romantika
a	a	k8xC	a
baladičnost	baladičnost	k1gFnSc1	baladičnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
obvyklé	obvyklý	k2eAgInPc4d1	obvyklý
motivy	motiv	k1gInPc4	motiv
gothic	gothice	k1gFnPc2	gothice
a	a	k8xC	a
doom	dooma	k1gFnPc2	dooma
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
nu	nu	k9	nu
metalu	metal	k1gInSc3	metal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
zabývá	zabývat	k5eAaImIp3nS	zabývat
problémy	problém	k1gInPc4	problém
dospívající	dospívající	k2eAgFnSc2d1	dospívající
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
styly	styl	k1gInPc1	styl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
melodický	melodický	k2eAgInSc1d1	melodický
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
<g/>
,	,	kIx,	,
progresivní	progresivní	k2eAgInSc4d1	progresivní
metal	metal	k1gInSc4	metal
<g/>
,	,	kIx,	,
a	a	k8xC	a
black	black	k1gMnSc1	black
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
oblibují	oblibovat	k5eAaImIp3nP	oblibovat
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
tematiku	tematika	k1gFnSc4	tematika
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k9	co
textová	textový	k2eAgFnSc1d1	textová
stránka	stránka	k1gFnSc1	stránka
extrémnějších	extrémní	k2eAgNnPc2d2	extrémnější
odnoží	odnoží	k1gNnPc2	odnoží
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
grindcoru	grindcor	k1gInSc2	grindcor
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
výlučně	výlučně	k6eAd1	výlučně
z	z	k7c2	z
útočného	útočný	k2eAgNnSc2d1	útočné
<g/>
,	,	kIx,	,
nelidského	lidský	k2eNgMnSc2d1	nelidský
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
nerozumného	rozumný	k2eNgInSc2d1	nerozumný
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
hudby	hudba	k1gFnSc2	hudba
jsou	být	k5eAaImIp3nP	být
podivné	podivný	k2eAgInPc1d1	podivný
<g/>
,	,	kIx,	,
fantastické	fantastický	k2eAgInPc1d1	fantastický
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
vyvolávající	vyvolávající	k2eAgInSc1d1	vyvolávající
rozptýlení	rozptýlení	k1gNnSc3	rozptýlení
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
texty	text	k1gInPc4	text
kapely	kapela	k1gFnSc2	kapela
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
často	často	k6eAd1	často
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
nesou	nést	k5eAaImIp3nP	nést
znaky	znak	k1gInPc7	znak
bájesloví	bájesloví	k1gNnSc2	bájesloví
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
poezie	poezie	k1gFnSc2	poezie
<g/>
;	;	kIx,	;
k	k	k7c3	k
takovým	takový	k3xDgFnPc3	takový
písním	píseň	k1gFnPc3	píseň
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Rime	Rim	k1gMnSc2	Rim
of	of	k?	of
the	the	k?	the
Ancient	Ancient	k1gMnSc1	Ancient
Mariner	Mariner	k1gMnSc1	Mariner
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
starém	staré	k1gNnSc6	staré
námořníkovi	námořníkův	k2eAgMnPc1d1	námořníkův
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
básni	báseň	k1gFnSc6	báseň
Samuela	Samuel	k1gMnSc2	Samuel
Taylora	Taylor	k1gMnSc2	Taylor
Coleridge	Coleridg	k1gMnSc2	Coleridg
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
ukázkové	ukázkový	k2eAgInPc4d1	ukázkový
příklady	příklad	k1gInPc4	příklad
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Wizard	Wizard	k1gMnSc1	Wizard
<g/>
"	"	kIx"	"
od	od	k7c2	od
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Conjuring	Conjuring	k1gInSc1	Conjuring
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Five	Five	k1gNnSc1	Five
Magics	Magicsa	k1gFnPc2	Magicsa
<g/>
"	"	kIx"	"
od	od	k7c2	od
Megadeth	Megadetha	k1gFnPc2	Megadetha
či	či	k8xC	či
"	"	kIx"	"
<g/>
Dreamer	Dreamer	k1gInSc1	Dreamer
Deceiver	Deceivra	k1gFnPc2	Deceivra
<g/>
"	"	kIx"	"
od	od	k7c2	od
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
interpreti	interpret	k1gMnPc1	interpret
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
nukleární	nukleární	k2eAgFnSc4d1	nukleární
zkázu	zkáza	k1gFnSc4	zkáza
<g/>
,	,	kIx,	,
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
či	či	k8xC	či
náboženské	náboženský	k2eAgInPc4d1	náboženský
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vzorovými	vzorový	k2eAgInPc7d1	vzorový
příklady	příklad	k1gInPc7	příklad
jsou	být	k5eAaImIp3nP	být
písně	píseň	k1gFnSc2	píseň
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
War	War	k1gFnSc1	War
Pigs	Pigsa	k1gFnPc2	Pigsa
<g/>
"	"	kIx"	"
od	od	k7c2	od
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Killer	Killer	k1gInSc1	Killer
of	of	k?	of
Giants	Giants	k1gInSc1	Giants
<g/>
"	"	kIx"	"
od	od	k7c2	od
Ozzyho	Ozzy	k1gMnSc2	Ozzy
Osbourna	Osbourno	k1gNnSc2	Osbourno
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
And	Anda	k1gFnPc2	Anda
Justice	justice	k1gFnSc1	justice
for	forum	k1gNnPc2	forum
All	All	k1gFnSc2	All
<g/>
"	"	kIx"	"
od	od	k7c2	od
Metallicy	Metallica	k1gFnSc2	Metallica
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
2	[number]	k4	2
Minutes	Minutes	k1gInSc1	Minutes
to	ten	k3xDgNnSc1	ten
Midnight	Midnight	k2eAgMnSc1d1	Midnight
<g/>
"	"	kIx"	"
od	od	k7c2	od
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Balls	Balls	k1gInSc1	Balls
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Wall	Wall	k1gInSc1	Wall
<g/>
"	"	kIx"	"
od	od	k7c2	od
Accept	Accepta	k1gFnPc2	Accepta
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
Peace	Peace	k1gFnSc1	Peace
Sells	Sellsa	k1gFnPc2	Sellsa
<g/>
"	"	kIx"	"
od	od	k7c2	od
Megadeth	Megadetha	k1gFnPc2	Megadetha
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zastávající	zastávající	k2eAgFnSc1d1	zastávající
přední	přední	k2eAgNnSc4d1	přední
místo	místo	k1gNnSc4	místo
v	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
textech	text	k1gInPc6	text
takových	takový	k3xDgFnPc2	takový
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xS	jako
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
,	,	kIx,	,
Slayer	Slayer	k1gMnSc1	Slayer
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
W.	W.	kA	W.
<g/>
A.S.	A.S.	k1gFnSc2	A.S.
<g/>
P.	P.	kA	P.
Powermetalové	Powermetal	k1gMnPc1	Powermetal
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
textové	textový	k2eAgInPc4d1	textový
a	a	k8xC	a
hudební	hudební	k2eAgInPc4d1	hudební
motivy	motiv	k1gInPc4	motiv
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
pompézní	pompézní	k2eAgFnPc1d1	pompézní
a	a	k8xC	a
optimistické	optimistický	k2eAgFnPc1d1	optimistická
<g/>
,	,	kIx,	,
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
výjimku	výjimka	k1gFnSc4	výjimka
tohoto	tento	k3xDgNnSc2	tento
temného	temný	k2eAgNnSc2d1	temné
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
powermetalové	powermetal	k1gMnPc1	powermetal
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Helloween	Helloween	k2eAgInSc1d1	Helloween
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
metal	metal	k1gInSc1	metal
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
inspirující	inspirující	k2eAgFnSc7d1	inspirující
a	a	k8xC	a
optimistickou	optimistický	k2eAgFnSc7d1	optimistická
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tematika	tematika	k1gFnSc1	tematika
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
čelí	čelit	k5eAaImIp3nS	čelit
již	již	k6eAd1	již
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
roky	rok	k1gInPc4	rok
ostré	ostrý	k2eAgFnSc3d1	ostrá
kritice	kritika	k1gFnSc3	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Jona	Jon	k1gInSc2	Jon
Parelese	Parelese	k1gFnSc2	Parelese
"	"	kIx"	"
<g/>
ústřední	ústřední	k2eAgInSc1d1	ústřední
motiv	motiv	k1gInSc1	motiv
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
a	a	k8xC	a
skutečně	skutečně	k6eAd1	skutečně
univerzální	univerzální	k2eAgMnSc1d1	univerzální
<g/>
.	.	kIx.	.
</s>
<s>
Chrochtáním	chrochtání	k1gNnSc7	chrochtání
<g/>
,	,	kIx,	,
sténáním	sténání	k1gNnSc7	sténání
a	a	k8xC	a
povznesenými	povznesený	k2eAgInPc7d1	povznesený
texty	text	k1gInPc7	text
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
<g/>
...	...	k?	...
<g/>
nespoutanou	spoutaný	k2eNgFnSc4d1	nespoutaná
zábavu	zábava	k1gFnSc4	zábava
<g/>
...	...	k?	...
Jádro	jádro	k1gNnSc1	jádro
muziky	muzika	k1gFnSc2	muzika
je	být	k5eAaImIp3nS	být
upjaté	upjatý	k2eAgNnSc1d1	upjaté
a	a	k8xC	a
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgFnPc1d1	daná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hudební	hudební	k2eAgMnPc1d1	hudební
kritici	kritik	k1gMnPc1	kritik
častokrát	častokrát	k6eAd1	častokrát
považují	považovat	k5eAaImIp3nP	považovat
metalové	metalový	k2eAgInPc4d1	metalový
texty	text	k1gInPc4	text
za	za	k7c4	za
nezralé	zralý	k2eNgFnPc4d1	nezralá
a	a	k8xC	a
otřepané	otřepaný	k2eAgFnPc4d1	otřepaná
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
případech	případ	k1gInPc6	případ
zastávající	zastávající	k2eAgFnSc2d1	zastávající
se	se	k3xPyFc4	se
mizogynie	mizogynie	k1gFnSc2	mizogynie
a	a	k8xC	a
okultizmu	okultizmus	k1gInSc2	okultizmus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
členové	člen	k1gMnPc1	člen
organizace	organizace	k1gFnSc2	organizace
"	"	kIx"	"
<g/>
Parents	Parents	k1gInSc1	Parents
Music	Musice	k1gFnPc2	Musice
Resource	Resourka	k1gFnSc6	Resourka
Center	centrum	k1gNnPc2	centrum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sdružení	sdružení	k1gNnSc1	sdružení
rodičů	rodič	k1gMnPc2	rodič
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
hudby	hudba	k1gFnSc2	hudba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
uspořádaly	uspořádat	k5eAaPmAgFnP	uspořádat
velkou	velký	k2eAgFnSc4d1	velká
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
populární	populární	k2eAgFnSc3d1	populární
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Žádali	žádat	k5eAaImAgMnP	žádat
Kongres	kongres	k1gInSc4	kongres
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
omezil	omezit	k5eAaPmAgInS	omezit
hudební	hudební	k2eAgInSc1d1	hudební
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	on	k3xPp3gNnSc4	on
pobuřovaly	pobuřovat	k5eAaImAgInP	pobuřovat
urážlivé	urážlivý	k2eAgInPc1d1	urážlivý
–	–	k?	–
především	především	k9	především
heavymetalové	heavymetalový	k2eAgInPc4d1	heavymetalový
–	–	k?	–
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
musela	muset	k5eAaImAgFnS	muset
kapela	kapela	k1gFnSc1	kapela
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
čelit	čelit	k5eAaImF	čelit
obžalobě	obžaloba	k1gFnSc3	obžaloba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
vznesli	vznést	k5eAaPmAgMnP	vznést
rodiče	rodič	k1gMnPc1	rodič
dvou	dva	k4xCgNnPc2	dva
mládenců	mládenec	k1gMnPc2	mládenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
před	před	k7c7	před
pěti	pět	k4xCc7	pět
lety	let	k1gInPc7	let
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
si	se	k3xPyFc3	se
vyslechli	vyslechnout	k5eAaPmAgMnP	vyslechnout
podprahové	podprahový	k2eAgNnSc4d1	podprahové
poselství	poselství	k1gNnSc4	poselství
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yQgNnSc1	který
znělo	znět	k5eAaImAgNnS	znět
"	"	kIx"	"
<g/>
do	do	k7c2	do
it	it	k?	it
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
udělej	udělat	k5eAaPmRp2nS	udělat
to	ten	k3xDgNnSc1	ten
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgInS	získat
velkou	velká	k1gFnSc4	velká
pozornosti	pozornost	k1gFnSc3	pozornost
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
hudebníci	hudebník	k1gMnPc1	hudebník
byli	být	k5eAaImAgMnP	být
posléze	posléze	k6eAd1	posléze
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
nevinné	vinný	k2eNgFnPc4d1	nevinná
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
i	i	k9	i
vizuální	vizuální	k2eAgFnSc1d1	vizuální
stránka	stránka	k1gFnSc1	stránka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
heavy	heav	k1gInPc4	heav
metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Obaly	obal	k1gInPc1	obal
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc1	kostým
kapel	kapela	k1gFnPc2	kapela
a	a	k8xC	a
videoklipy	videoklip	k1gInPc4	videoklip
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
důležité	důležitý	k2eAgNnSc1d1	důležité
jako	jako	k8xC	jako
hudba	hudba	k1gFnSc1	hudba
samotná	samotný	k2eAgFnSc1d1	samotná
<g/>
,	,	kIx,	,
no	no	k9	no
jen	jen	k9	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
jsou	být	k5eAaImIp3nP	být
přednější	přední	k2eAgFnPc1d2	přednější
než	než	k8xS	než
hudební	hudební	k2eAgFnPc1d1	hudební
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každého	každý	k3xTgNnSc2	každý
díla	dílo	k1gNnSc2	dílo
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
získat	získat	k5eAaPmF	získat
množství	množství	k1gNnSc4	množství
dojmů	dojem	k1gInPc2	dojem
<g/>
;	;	kIx,	;
heavy	heava	k1gFnSc2	heava
metal	metat	k5eAaImAgInS	metat
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
umělecké	umělecký	k2eAgFnSc2d1	umělecká
formy	forma	k1gFnSc2	forma
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
široký	široký	k2eAgInSc1d1	široký
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mu	on	k3xPp3gInSc3	on
nedominuje	dominovat	k5eNaImIp3nS	dominovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
vyjadřovací	vyjadřovací	k2eAgInSc1d1	vyjadřovací
prostředek	prostředek	k1gInSc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
malba	malba	k1gFnSc1	malba
má	mít	k5eAaImIp3nS	mít
vizuální	vizuální	k2eAgFnSc4d1	vizuální
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
symfonie	symfonie	k1gFnSc1	symfonie
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
<g/>
,	,	kIx,	,
v	v	k7c6	v
metalových	metalový	k2eAgFnPc6d1	metalová
kapelách	kapela	k1gFnPc6	kapela
se	se	k3xPyFc4	se
zvuková	zvukový	k2eAgFnSc1d1	zvuková
podoba	podoba	k1gFnSc1	podoba
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
vizuální	vizuální	k2eAgFnSc7d1	vizuální
(	(	kIx(	(
<g/>
obal	obal	k1gInSc4	obal
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc6	oblečení
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgNnSc1	některý
rané	raný	k2eAgNnSc1d1	rané
heavymetalové	heavymetalový	k2eAgNnSc1d1	heavymetalové
seskupení	seskupení	k1gNnSc1	seskupení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Alice	Alice	k1gFnSc1	Alice
Cooper	Coopra	k1gFnPc2	Coopra
a	a	k8xC	a
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
novější	nový	k2eAgFnSc2d2	novější
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
GWAR	GWAR	kA	GWAR
<g/>
,	,	kIx,	,
Mushroomhead	Mushroomhead	k1gInSc4	Mushroomhead
či	či	k8xC	či
Marilyn	Marilyn	k1gFnSc4	Marilyn
Manson	Mansona	k1gFnPc2	Mansona
<g/>
,	,	kIx,	,
získaly	získat	k5eAaPmAgInP	získat
popularitu	popularita	k1gFnSc4	popularita
nejenom	nejenom	k6eAd1	nejenom
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
také	také	k9	také
jejich	jejich	k3xOp3gNnPc4	jejich
vystoupení	vystoupení	k1gNnPc4	vystoupení
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
a	a	k8xC	a
hanlivé	hanlivý	k2eAgNnSc1d1	hanlivé
chování	chování	k1gNnSc1	chování
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhatánské	dlouhatánský	k2eAgInPc1d1	dlouhatánský
vlasy	vlas	k1gInPc1	vlas
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Weinsteina	Weinsteino	k1gNnSc2	Weinsteino
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
poznávacím	poznávací	k2eAgInSc7d1	poznávací
prvkem	prvek	k1gInSc7	prvek
metalové	metalový	k2eAgFnSc2d1	metalová
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Heavy	Heava	k1gFnSc2	Heava
metal	metal	k1gInSc1	metal
převzal	převzít	k5eAaPmAgInS	převzít
tento	tento	k3xDgInSc4	tento
rys	rys	k1gInSc4	rys
původně	původně	k6eAd1	původně
z	z	k7c2	z
kultury	kultura	k1gFnSc2	kultura
hippies	hippiesa	k1gFnPc2	hippiesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
heavymetalové	heavymetalový	k2eAgNnSc1d1	heavymetalové
háro	háro	k1gNnSc1	háro
"	"	kIx"	"
<g/>
symbolizovalo	symbolizovat	k5eAaImAgNnS	symbolizovat
nenávist	nenávist	k1gFnSc4	nenávist
<g/>
,	,	kIx,	,
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
rozčarování	rozčarování	k1gNnSc4	rozčarování
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
očividně	očividně	k6eAd1	očividně
necítí	cítit	k5eNaImIp3nS	cítit
nikde	nikde	k6eAd1	nikde
doma	doma	k6eAd1	doma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
novinář	novinář	k1gMnSc1	novinář
Nader	nadrat	k5eAaBmRp2nS	nadrat
Rahman	Rahman	k1gMnSc1	Rahman
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgInPc4d1	Dlouhé
vlasy	vlas	k1gInPc4	vlas
dodaly	dodat	k5eAaPmAgFnP	dodat
členům	člen	k1gMnPc3	člen
metalové	metalový	k2eAgFnSc2d1	metalová
komunity	komunita	k1gFnSc2	komunita
"	"	kIx"	"
<g/>
sílu	síla	k1gFnSc4	síla
nevyhnutnou	vyhnutný	k2eNgFnSc4d1	nevyhnutná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
bouřit	bouřit	k5eAaImF	bouřit
vesměs	vesměs	k6eAd1	vesměs
proti	proti	k7c3	proti
ničemu	nic	k3yNnSc3	nic
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Klasický	klasický	k2eAgInSc1d1	klasický
metalový	metalový	k2eAgInSc1d1	metalový
stejnokroj	stejnokroj	k1gInSc1	stejnokroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
modrých	modrý	k2eAgInPc2d1	modrý
džínů	džíny	k1gInPc2	džíny
<g/>
,	,	kIx,	,
černého	černý	k2eAgNnSc2d1	černé
trička	tričko	k1gNnSc2	tričko
<g/>
,	,	kIx,	,
bot	bota	k1gFnPc2	bota
a	a	k8xC	a
černé	černý	k2eAgFnSc2d1	černá
kožené	kožený	k2eAgFnSc2d1	kožená
nebo	nebo	k8xC	nebo
džínové	džínový	k2eAgFnSc2d1	džínová
bundy	bunda	k1gFnSc2	bunda
<g/>
.	.	kIx.	.
</s>
<s>
Trička	tričko	k1gNnPc1	tričko
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
ozdobené	ozdobený	k2eAgInPc1d1	ozdobený
logy	log	k1gInPc1	log
nebo	nebo	k8xC	nebo
kresbami	kresba	k1gFnPc7	kresba
představujícími	představující	k2eAgInPc7d1	představující
milovanou	milovaný	k2eAgFnSc4d1	milovaná
metalovou	metalový	k2eAgFnSc4d1	metalová
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
zásluhou	zásluhou	k7c2	zásluhou
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gInSc4	Priest
a	a	k8xC	a
Motörhead	Motörhead	k1gInSc4	Motörhead
ujala	ujmout	k5eAaPmAgFnS	ujmout
mezi	mezi	k7c7	mezi
heavymetalistami	heavymetalista	k1gFnPc7	heavymetalista
kožená	kožený	k2eAgFnSc1d1	kožená
móda	móda	k1gFnSc1	móda
a	a	k8xC	a
motorkářská	motorkářský	k2eAgFnSc1d1	motorkářská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
metalovou	metalový	k2eAgFnSc4d1	metalová
módu	móda	k1gFnSc4	móda
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
punk	punk	k1gInSc1	punk
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
gothic	gothic	k1gMnSc1	gothic
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
hororové	hororový	k2eAgInPc4d1	hororový
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
glammetalové	glammetal	k1gMnPc1	glammetal
kapely	kapela	k1gFnSc2	kapela
onoho	onen	k3xDgNnSc2	onen
období	období	k1gNnSc2	období
si	se	k3xPyFc3	se
zakládaly	zakládat	k5eAaImAgFnP	zakládat
na	na	k7c6	na
vzhledu	vzhled	k1gInSc6	vzhled
a	a	k8xC	a
osobním	osobní	k2eAgInSc6d1	osobní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
zalíbení	zalíbený	k2eAgMnPc1d1	zalíbený
v	v	k7c6	v
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
<g/>
,	,	kIx,	,
barvených	barvený	k2eAgInPc6d1	barvený
<g/>
,	,	kIx,	,
natupírovaných	natupírovaný	k2eAgInPc6d1	natupírovaný
vlasech	vlas	k1gInPc6	vlas
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
s	s	k7c7	s
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
přezdívka	přezdívka	k1gFnSc1	přezdívka
"	"	kIx"	"
<g/>
hair	hair	k1gInSc1	hair
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hair	hair	k1gInSc1	hair
=	=	kIx~	=
vlasy	vlas	k1gInPc1	vlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
líčení	líčení	k1gNnSc1	líčení
<g/>
,	,	kIx,	,
vyfintěných	vyfintěný	k2eAgInPc6d1	vyfintěný
kostýmech	kostým	k1gInPc6	kostým
<g/>
,	,	kIx,	,
a	a	k8xC	a
doplňkách	doplněk	k1gInPc6	doplněk
jako	jako	k8xS	jako
čelenky	čelenka	k1gFnPc1	čelenka
a	a	k8xC	a
bižuterie	bižuterie	k1gFnPc1	bižuterie
<g/>
.	.	kIx.	.
</s>
<s>
Přepracované	přepracovaný	k2eAgInPc4d1	přepracovaný
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
make-up	makep	k1gInSc4	make-up
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgFnSc4d1	populární
taktéž	taktéž	k?	taktéž
u	u	k7c2	u
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
nemetalových	metalový	k2eNgInPc2d1	metalový
<g/>
)	)	kIx)	)
japonského	japonský	k2eAgNnSc2d1	Japonské
hnutí	hnutí	k1gNnSc2	hnutí
"	"	kIx"	"
<g/>
visual	visual	k1gInSc1	visual
kei	kei	k?	kei
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
položila	položit	k5eAaPmAgFnS	položit
základ	základ	k1gInSc4	základ
kapela	kapela	k1gFnSc1	kapela
X	X	kA	X
Japan	japan	k1gInSc4	japan
koncem	konec	k1gInSc7	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
živých	živý	k2eAgNnPc6d1	živé
vystoupeních	vystoupení	k1gNnPc6	vystoupení
mají	mít	k5eAaImIp3nP	mít
metaloví	metalový	k2eAgMnPc1d1	metalový
hudebníci	hudebník	k1gMnPc1	hudebník
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
metlení	metlení	k1gNnSc2	metlení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
hází	házet	k5eAaImIp3nS	házet
hlavou	hlava	k1gFnSc7	hlava
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
do	do	k7c2	do
rytmu	rytmus	k1gInSc2	rytmus
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vizuálně	vizuálně	k6eAd1	vizuálně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
efektní	efektní	k2eAgNnSc1d1	efektní
<g/>
,	,	kIx,	,
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Paroháč	paroháč	k1gMnSc1	paroháč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
rozšířené	rozšířený	k2eAgNnSc4d1	rozšířené
gesto	gesto	k1gNnSc4	gesto
<g/>
,	,	kIx,	,
zpopularizoval	zpopularizovat	k5eAaPmAgMnS	zpopularizovat
vokalista	vokalista	k1gMnSc1	vokalista
Ronnie	Ronnie	k1gFnSc2	Ronnie
James	James	k1gMnSc1	James
Dio	Dio	k1gMnSc1	Dio
během	během	k7c2	během
účinkovaní	účinkovaný	k2eAgMnPc1d1	účinkovaný
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
a	a	k8xC	a
Dio	Dio	k1gMnSc1	Dio
<g/>
.	.	kIx.	.
</s>
<s>
Gene	gen	k1gInSc5	gen
Simmons	Simmons	k1gInSc1	Simmons
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kiss	k1gInSc1	Kiss
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgInS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
udělal	udělat	k5eAaPmAgMnS	udělat
toto	tento	k3xDgNnSc4	tento
gesto	gesto	k1gNnSc4	gesto
během	během	k7c2	během
koncertu	koncert	k1gInSc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
metalových	metalový	k2eAgInPc2d1	metalový
koncertů	koncert	k1gInPc2	koncert
netančí	tančit	k5eNaImIp3nS	tančit
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
;	;	kIx,	;
Deena	Deena	k1gFnSc1	Deena
Weinstein	Weinsteina	k1gFnPc2	Weinsteina
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
publikum	publikum	k1gNnSc1	publikum
bývá	bývat	k5eAaImIp3nS	bývat
převážně	převážně	k6eAd1	převážně
mužské	mužský	k2eAgNnSc1d1	mužské
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
heterosexuální	heterosexuální	k2eAgFnSc7d1	heterosexuální
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
především	především	k9	především
dva	dva	k4xCgInPc4	dva
tělesné	tělesný	k2eAgInPc4d1	tělesný
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
tanec	tanec	k1gInSc4	tanec
<g/>
:	:	kIx,	:
metlení	metlení	k1gNnSc1	metlení
a	a	k8xC	a
šťouchání	šťouchání	k?	šťouchání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
výrazem	výraz	k1gInSc7	výraz
porozumění	porozumění	k1gNnSc2	porozumění
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
rytmickým	rytmický	k2eAgNnSc7d1	rytmické
gestem	gesto	k1gNnSc7	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
"	"	kIx"	"
<g/>
tancem	tanec	k1gInSc7	tanec
<g/>
"	"	kIx"	"
posluchačů	posluchač	k1gMnPc2	posluchač
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
doma	doma	k6eAd1	doma
nebo	nebo	k8xC	nebo
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
předstírání	předstírání	k1gNnSc4	předstírání
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
"	"	kIx"	"
<g/>
air	air	k?	air
guitar	guitar	k1gInSc1	guitar
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
činnosti	činnost	k1gFnPc4	činnost
diváků	divák	k1gMnPc2	divák
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
skákání	skákání	k1gNnSc1	skákání
do	do	k7c2	do
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
(	(	kIx(	(
<g/>
stage	stage	k1gFnSc1	stage
diving	diving	k1gInSc1	diving
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
surfování	surfování	k1gNnSc1	surfování
v	v	k7c6	v
davu	dav	k1gInSc6	dav
(	(	kIx(	(
<g/>
crowd	crowd	k1gInSc1	crowd
surfing	surfing	k1gInSc1	surfing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tlačení	tlačený	k2eAgMnPc1d1	tlačený
a	a	k8xC	a
postrkovaní	postrkovaný	k2eAgMnPc1d1	postrkovaný
v	v	k7c6	v
chaotických	chaotický	k2eAgFnPc6d1	chaotická
šarvátkách	šarvátka	k1gFnPc6	šarvátka
(	(	kIx(	(
<g/>
moshing	moshing	k1gInSc4	moshing
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
paroháč	paroháč	k1gMnSc1	paroháč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
koncertech	koncert	k1gInPc6	koncert
i	i	k9	i
pogo	pogo	k6eAd1	pogo
<g/>
.	.	kIx.	.
</s>
<s>
Deena	Deena	k1gFnSc1	Deena
Weinstein	Weinsteina	k1gFnPc2	Weinsteina
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
mužská	mužský	k2eAgFnSc1d1	mužská
subkultura	subkultura	k1gFnSc1	subkultura
umožnila	umožnit	k5eAaPmAgFnS	umožnit
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
vytrvat	vytrvat	k5eAaPmF	vytrvat
na	na	k7c6	na
výsluní	výsluní	k1gNnSc6	výsluní
podstatně	podstatně	k6eAd1	podstatně
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
než	než	k8xS	než
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
mnohým	mnohý	k2eAgMnPc3d1	mnohý
jiným	jiný	k2eAgMnPc3d1	jiný
rockovým	rockový	k2eAgMnPc3d1	rockový
žánrům	žánr	k1gInPc3	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Metalové	metalový	k2eAgNnSc1d1	metalové
publikum	publikum	k1gNnSc1	publikum
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
zejména	zejména	k9	zejména
z	z	k7c2	z
pracujících	pracující	k2eAgMnPc2d1	pracující
bělochů	běloch	k1gMnPc2	běloch
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
(	(	kIx(	(
<g/>
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
hrabství	hrabství	k1gNnSc6	hrabství
West	West	k2eAgInSc4d1	West
Midlands	Midlands	k1gInSc4	Midlands
a	a	k8xC	a
státech	stát	k1gInPc6	stát
USA	USA	kA	USA
<g/>
:	:	kIx,	:
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
a	a	k8xC	a
Washington	Washington	k1gInSc1	Washington
byl	být	k5eAaImAgInS	být
metal	metal	k1gInSc1	metal
většinou	většinou	k6eAd1	většinou
hudbou	hudba	k1gFnSc7	hudba
'	'	kIx"	'
<g/>
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
mládeže	mládež	k1gFnSc2	mládež
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
tolerantní	tolerantní	k2eAgFnSc1d1	tolerantní
i	i	k9	i
vůči	vůči	k7c3	vůči
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
stojí	stát	k5eAaImIp3nP	stát
mimo	mimo	k7c4	mimo
"	"	kIx"	"
<g/>
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
jádra	jádro	k1gNnSc2	jádro
<g/>
"	"	kIx"	"
subkultury	subkultura	k1gFnSc2	subkultura
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
nepsaného	nepsaný	k2eAgInSc2d1	nepsaný
metalového	metalový	k2eAgInSc2d1	metalový
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
vzhledu	vzhled	k1gInSc2	vzhled
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
projevům	projev	k1gInPc3	projev
spolupatřičnosti	spolupatřičnost	k1gFnSc2	spolupatřičnost
se	s	k7c7	s
subkulturou	subkultura	k1gFnSc7	subkultura
patří	patřit	k5eAaImIp3nP	patřit
nejenom	nejenom	k6eAd1	nejenom
návštěvy	návštěva	k1gFnPc1	návštěva
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
společná	společný	k2eAgFnSc1d1	společná
móda	móda	k1gFnSc1	móda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přispívání	přispívání	k1gNnSc4	přispívání
do	do	k7c2	do
metalových	metalový	k2eAgInPc2d1	metalový
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
rovněž	rovněž	k9	rovněž
příspěvky	příspěvek	k1gInPc4	příspěvek
na	na	k7c4	na
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Heavymetalové	heavymetalový	k2eAgFnSc3d1	heavymetalová
scéně	scéna	k1gFnSc3	scéna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
nepsané	nepsaný	k2eAgInPc4d1	nepsaný
zákony	zákon	k1gInPc4	zákon
ohledně	ohledně	k7c2	ohledně
autenticity	autenticita	k1gFnSc2	autenticita
<g/>
,	,	kIx,	,
přischla	přischnout	k5eAaPmAgFnS	přischnout
přezdívka	přezdívka	k1gFnSc1	přezdívka
"	"	kIx"	"
<g/>
odcizená	odcizený	k2eAgFnSc1d1	odcizená
subkultura	subkultura	k1gFnSc1	subkultura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
subculture	subcultur	k1gMnSc5	subcultur
of	of	k?	of
alienation	alienation	k1gInSc4	alienation
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohohle	tenhle	k3xDgInSc2	tenhle
zákona	zákon	k1gInSc2	zákon
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
muzikanti	muzikant	k1gMnPc1	muzikant
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
oddání	oddání	k1gNnSc4	oddání
muzice	muzika	k1gFnSc3	muzika
a	a	k8xC	a
subkultuře	subkultura	k1gFnSc3	subkultura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gInPc4	on
podporuje	podporovat	k5eAaImIp3nS	podporovat
<g/>
,	,	kIx,	,
nesmějí	smát	k5eNaImIp3nP	smát
se	se	k3xPyFc4	se
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
pozornost	pozornost	k1gFnSc4	pozornost
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nesmějí	smát	k5eNaImIp3nP	smát
"	"	kIx"	"
<g/>
zradit	zradit	k5eAaPmF	zradit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
od	od	k7c2	od
fanoušků	fanoušek	k1gMnPc2	fanoušek
se	se	k3xPyFc4	se
zase	zase	k9	zase
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
"	"	kIx"	"
<g/>
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
systému	systém	k1gInSc3	systém
a	a	k8xC	a
odluka	odluka	k1gFnSc1	odluka
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lidé	člověk	k1gMnPc1	člověk
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
metalem	metal	k1gInSc7	metal
i	i	k9	i
oddaní	oddaný	k2eAgMnPc1d1	oddaný
fanoušci	fanoušek	k1gMnPc1	fanoušek
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
měly	mít	k5eAaImAgFnP	mít
značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
považovat	považovat	k5eAaImF	považovat
některé	některý	k3yIgMnPc4	některý
interprety	interpret	k1gMnPc4	interpret
(	(	kIx(	(
<g/>
a	a	k8xC	a
též	též	k9	též
některé	některý	k3yIgMnPc4	některý
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
)	)	kIx)	)
za	za	k7c2	za
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pozéry	pozér	k1gMnPc4	pozér
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
předstírají	předstírat	k5eAaImIp3nP	předstírat
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
subkultuře	subkultura	k1gFnSc3	subkultura
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nevědí	vědět	k5eNaImIp3nP	vědět
nic	nic	k6eAd1	nic
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
textech	text	k1gInPc6	text
ani	ani	k8xC	ani
poselstvích	poselství	k1gNnPc6	poselství
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
muziku	muzika	k1gFnSc4	muzika
poslouchají	poslouchat	k5eAaImIp3nP	poslouchat
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
pojmu	pojem	k1gInSc2	pojem
heavy	heava	k1gFnSc2	heava
metal	metat	k5eAaImAgMnS	metat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
je	být	k5eAaImIp3nS	být
neurčitý	určitý	k2eNgInSc1d1	neurčitý
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
"	"	kIx"	"
<g/>
heavy	heav	k1gInPc4	heav
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
těžký	těžký	k2eAgInSc1d1	těžký
kov	kov	k1gInSc1	kov
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
používal	používat	k5eAaImAgMnS	používat
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
a	a	k8xC	a
hutnictví	hutnictví	k1gNnSc6	hutnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novodobé	novodobý	k2eAgFnSc6d1	novodobá
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
použil	použít	k5eAaPmAgMnS	použít
tento	tento	k3xDgInSc4	tento
výraz	výraz	k1gInSc4	výraz
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
americký	americký	k2eAgMnSc1d1	americký
beatnik	beatnik	k1gMnSc1	beatnik
William	William	k1gInSc4	William
S.	S.	kA	S.
Burroughs	Burroughs	k1gInSc1	Burroughs
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
novele	novela	k1gFnSc6	novela
Hebká	hebký	k2eAgFnSc1d1	hebká
mašinka	mašinka	k1gFnSc1	mašinka
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Soft	Soft	k?	Soft
Machine	Machin	k1gMnSc5	Machin
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
další	další	k2eAgFnSc6d1	další
knize	kniha	k1gFnSc6	kniha
Nova	novum	k1gNnSc2	novum
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
heavy	heava	k1gFnSc2	heava
metal	metal	k1gInSc1	metal
tu	tu	k6eAd1	tu
používá	používat	k5eAaImIp3nS	používat
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k9	jako
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
návykové	návykový	k2eAgFnPc4d1	návyková
drogy	droga	k1gFnPc4	droga
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
nemocemi	nemoc	k1gFnPc7	nemoc
a	a	k8xC	a
drogami	droga	k1gFnPc7	droga
vyvolávajícími	vyvolávající	k2eAgInPc7d1	vyvolávající
orgasmus	orgasmus	k1gInSc1	orgasmus
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
asexuálními	asexuální	k2eAgFnPc7d1	asexuální
<g/>
,	,	kIx,	,
parazitickými	parazitický	k2eAgFnPc7d1	parazitická
formami	forma	k1gFnPc7	forma
života	život	k1gInSc2	život
–	–	k?	–
národ	národ	k1gInSc1	národ
heavymetalistů	heavymetalista	k1gMnPc2	heavymetalista
z	z	k7c2	z
Uranu	Uran	k1gInSc2	Uran
obestřen	obestřen	k2eAgInSc1d1	obestřen
chladným	chladný	k2eAgInSc7d1	chladný
<g/>
,	,	kIx,	,
modrým	modrý	k2eAgInSc7d1	modrý
oparem	opar	k1gInSc7	opar
vypařených	vypařený	k2eAgFnPc2d1	vypařená
bankovek	bankovka	k1gFnPc2	bankovka
–	–	k?	–
a	a	k8xC	a
hmyzí	hmyzí	k2eAgMnPc1d1	hmyzí
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Minraudu	Minraud	k1gInSc2	Minraud
ze	z	k7c2	z
svou	svůj	k3xOyFgFnSc7	svůj
metalovou	metalový	k2eAgFnSc7d1	metalová
muzikou	muzika	k1gFnSc7	muzika
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
Ian	Ian	k1gMnSc5	Ian
Christe	Christ	k1gMnSc5	Christ
<g/>
,	,	kIx,	,
heavymetalový	heavymetalový	k2eAgMnSc1d1	heavymetalový
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
opisuje	opisovat	k5eAaImIp3nS	opisovat
význam	význam	k1gInSc4	význam
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
slangu	slang	k1gInSc6	slang
hippies	hippiesa	k1gFnPc2	hippiesa
<g/>
:	:	kIx,	:
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
heavy	heava	k1gFnSc2	heava
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
popisovalo	popisovat	k5eAaImAgNnS	popisovat
cokoli	cokoli	k3yInSc4	cokoli
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
mělo	mít	k5eAaImAgNnS	mít
silnou	silný	k2eAgFnSc4d1	silná
či	či	k8xC	či
opojnou	opojný	k2eAgFnSc4d1	opojná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
metal	metat	k5eAaImAgMnS	metat
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
těžké	těžký	k2eAgNnSc4d1	těžké
duševní	duševní	k2eAgNnSc4d1	duševní
rozpoložení	rozpoložení	k1gNnSc4	rozpoložení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
slangu	slang	k1gInSc6	slang
beatniků	beatnik	k1gMnPc2	beatnik
se	se	k3xPyFc4	se
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
heavy	heava	k1gFnPc1	heava
<g/>
"	"	kIx"	"
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
vážný	vážný	k2eAgInSc4d1	vážný
<g/>
"	"	kIx"	"
používal	používat	k5eAaImAgMnS	používat
už	už	k9	už
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
předtím	předtím	k6eAd1	předtím
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
"	"	kIx"	"
<g/>
tvrdé	tvrdý	k2eAgFnSc3d1	tvrdá
hudbě	hudba	k1gFnSc3	hudba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
heavy	heav	k1gInPc1	heav
music	musice	k1gFnPc2	musice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
pomalejší	pomalý	k2eAgFnSc4d2	pomalejší
<g/>
,	,	kIx,	,
hlasitější	hlasitý	k2eAgFnSc4d2	hlasitější
variaci	variace	k1gFnSc4	variace
standardního	standardní	k2eAgInSc2d1	standardní
popu	pop	k1gInSc2	pop
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Iron	iron	k1gInSc1	iron
Butterfly	butterfly	k1gInSc4	butterfly
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
"	"	kIx"	"
<g/>
Heavy	Heava	k1gFnSc2	Heava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
zdokumentované	zdokumentovaný	k2eAgNnSc1d1	zdokumentované
použití	použití	k1gNnSc1	použití
výrazu	výraz	k1gInSc2	výraz
heavy	heava	k1gFnSc2	heava
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
roku	rok	k1gInSc2	rok
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Born	Born	k1gMnSc1	Born
to	ten	k3xDgNnSc4	ten
Be	Be	k1gFnPc1	Be
Wild	Wilda	k1gFnPc2	Wilda
<g/>
"	"	kIx"	"
od	od	k7c2	od
Steppenwolfu	Steppenwolf	k1gInSc2	Steppenwolf
<g/>
,	,	kIx,	,
poukazujíc	poukazovat	k5eAaImSgFnS	poukazovat
na	na	k7c4	na
motocykl	motocykl	k1gInSc4	motocykl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
I	i	k9	i
like	like	k6eAd1	like
smoke	smokat	k5eAaPmIp3nS	smokat
and	and	k?	and
lightning	lightning	k1gInSc1	lightning
/	/	kIx~	/
Heavy	Heava	k1gFnSc2	Heava
metal	metat	k5eAaImAgMnS	metat
thunder	thunder	k1gMnSc1	thunder
/	/	kIx~	/
Racin	Racin	k1gMnSc1	Racin
<g/>
'	'	kIx"	'
with	with	k1gMnSc1	with
the	the	k?	the
wind	wind	k1gMnSc1	wind
/	/	kIx~	/
And	Anda	k1gFnPc2	Anda
the	the	k?	the
feelin	feelin	k1gInSc1	feelin
<g/>
'	'	kIx"	'
that	that	k1gInSc1	that
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
under	under	k1gMnSc1	under
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
rád	rád	k6eAd1	rád
dým	dým	k1gInSc1	dým
a	a	k8xC	a
blesk	blesk	k1gInSc1	blesk
/	/	kIx~	/
burácení	burácení	k1gNnSc1	burácení
těžkého	těžký	k2eAgInSc2d1	těžký
kovu	kov	k1gInSc2	kov
/	/	kIx~	/
závody	závod	k1gInPc1	závod
s	s	k7c7	s
větrem	vítr	k1gInSc7	vítr
/	/	kIx~	/
a	a	k8xC	a
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podléhám	podléhat	k5eAaImIp1nS	podléhat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
spornou	sporný	k2eAgFnSc4d1	sporná
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
původu	původ	k1gInSc6	původ
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
přinesl	přinést	k5eAaPmAgInS	přinést
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1995	[number]	k4	1995
"	"	kIx"	"
<g/>
Chas	chasa	k1gFnPc2	chasa
<g/>
"	"	kIx"	"
Chandler	Chandler	k1gMnSc1	Chandler
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
manažer	manažer	k1gMnSc1	manažer
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Experience	Experienka	k1gFnSc6	Experienka
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
pořad	pořad	k1gInSc4	pořad
"	"	kIx"	"
<g/>
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vysílala	vysílat	k5eAaImAgFnS	vysílat
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
PBS	PBS	kA	PBS
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
heavy	heav	k1gInPc4	heav
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
článku	článek	k1gInSc6	článek
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
vystoupení	vystoupení	k1gNnSc4	vystoupení
Jimiho	Jimi	k1gMnSc2	Jimi
Hendrixe	Hendrixe	k1gFnSc2	Hendrixe
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
autor	autor	k1gMnSc1	autor
opisoval	opisovat	k5eAaImAgMnS	opisovat
Jimiho	Jimi	k1gMnSc4	Jimi
Hendrixe	Hendrixe	k1gFnSc1	Hendrixe
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
jakoby	jakoby	k8xS	jakoby
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
těžký	těžký	k2eAgInSc4d1	těžký
kov	kov	k1gInSc4	kov
(	(	kIx(	(
<g/>
heavy	heav	k1gInPc4	heav
metal	metal	k1gInSc1	metal
<g/>
)	)	kIx)	)
padající	padající	k2eAgMnSc1d1	padající
z	z	k7c2	z
nebes	nebesa	k1gNnPc2	nebesa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
hodnověrnost	hodnověrnost	k1gFnSc1	hodnověrnost
tohoto	tento	k3xDgNnSc2	tento
tvrzení	tvrzení	k1gNnSc2	tvrzení
je	být	k5eAaImIp3nS	být
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
zdokumentované	zdokumentovaný	k2eAgNnSc1d1	zdokumentované
použití	použití	k1gNnSc1	použití
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
heavy	heav	k1gInPc4	heav
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
označení	označení	k1gNnSc1	označení
stylu	styl	k1gInSc2	styl
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
recenzi	recenze	k1gFnSc6	recenze
Mikea	Mike	k2eAgFnSc1d1	Mikea
Saunderse	Saunderse	k1gFnSc1	Saunderse
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
hodnotil	hodnotit	k5eAaImAgMnS	hodnotit
album	album	k1gNnSc4	album
vydané	vydaný	k2eAgFnSc2d1	vydaná
předešlého	předešlý	k2eAgInSc2d1	předešlý
roku	rok	k1gInSc2	rok
britskou	britský	k2eAgFnSc7d1	britská
kapelou	kapela	k1gFnSc7	kapela
Humble	Humble	k1gFnSc2	Humble
Pie	Pius	k1gMnPc4	Pius
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Humble	Humble	k1gMnSc5	Humble
Pie	Pius	k1gMnSc5	Pius
svým	svůj	k3xOyFgInSc7	svůj
americkým	americký	k2eAgInSc7d1	americký
debutem	debut	k1gInSc7	debut
As	as	k9	as
Safe	safe	k1gInSc4	safe
As	as	k1gNnSc2	as
Yesterday	Yesterdaa	k1gFnSc2	Yesterdaa
Is	Is	k1gMnSc3	Is
dokázali	dokázat	k5eAaPmAgMnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nuda	nuda	k1gFnSc1	nuda
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
mnoho	mnoho	k4c4	mnoho
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Předvedli	předvést	k5eAaPmAgMnP	předvést
se	se	k3xPyFc4	se
jako	jako	k9	jako
hlasitá	hlasitý	k2eAgFnSc1d1	hlasitá
<g/>
,	,	kIx,	,
nemelodická	melodický	k2eNgFnSc1d1	nemelodická
<g/>
,	,	kIx,	,
sračková	sračkový	k2eAgFnSc1d1	sračkový
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
těžkopádná	těžkopádný	k2eAgFnSc1d1	těžkopádná
jako	jako	k8xS	jako
těžký	těžký	k2eAgInSc1d1	těžký
kov	kov	k1gInSc1	kov
(	(	kIx(	(
<g/>
heavy	heava	k1gFnSc2	heava
metal	metal	k1gInSc1	metal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
hřmotnými	hřmotný	k2eAgFnPc7d1	hřmotná
a	a	k8xC	a
ohlušujícími	ohlušující	k2eAgFnPc7d1	ohlušující
částmi	část	k1gFnPc7	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mluví	mluvit	k5eAaImIp3nP	mluvit
za	za	k7c4	za
všechno	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
že	že	k8xS	že
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
všechny	všechen	k3xTgFnPc1	všechen
písně	píseň	k1gFnPc1	píseň
byly	být	k5eAaImAgFnP	být
špatné	špatný	k2eAgFnPc1d1	špatná
<g/>
...	...	k?	...
<g/>
ale	ale	k8xC	ale
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
hovno	hovno	k1gNnSc4	hovno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jejich	jejich	k3xOp3gNnSc1	jejich
album	album	k1gNnSc1	album
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
"	"	kIx"	"
<g/>
Humble	Humble	k1gMnSc5	Humble
Pie	Pius	k1gMnSc5	Pius
<g/>
"	"	kIx"	"
charakterizoval	charakterizovat	k5eAaBmAgInS	charakterizovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nic	nic	k3yNnSc1	nic
nového	nový	k2eAgNnSc2d1	nové
pod	pod	k7c7	pod
sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
heavymetalové	heavymetalový	k2eAgNnSc1d1	heavymetalové
hovno	hovno	k1gNnSc1	hovno
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgMnPc4	který
<g />
.	.	kIx.	.
</s>
<s>
nikdo	nikdo	k3yNnSc1	nikdo
ani	ani	k9	ani
nezakopne	zakopnout	k5eNaPmIp3nS	zakopnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
recenzi	recenze	k1gFnSc6	recenze
alba	album	k1gNnSc2	album
Kingdom	Kingdom	k1gInSc4	Kingdom
Come	Com	k1gInSc2	Com
od	od	k7c2	od
Sir	sir	k1gMnSc1	sir
Lord	lord	k1gMnSc1	lord
Baltimore	Baltimore	k1gInSc4	Baltimore
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1971	[number]	k4	1971
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Creem	Creem	k1gInSc1	Creem
<g/>
,	,	kIx,	,
Saunders	Saunders	k1gInSc1	Saunders
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sir	sir	k1gMnSc1	sir
Lord	lord	k1gMnSc1	lord
Baltimore	Baltimore	k1gInSc4	Baltimore
mají	mít	k5eAaImIp3nP	mít
nastudované	nastudovaný	k2eAgInPc1d1	nastudovaný
všechny	všechen	k3xTgInPc1	všechen
heavymetalové	heavymetalový	k2eAgInPc1d1	heavymetalový
triky	trik	k1gInPc1	trik
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kritik	kritik	k1gMnSc1	kritik
časopisu	časopis	k1gInSc2	časopis
Creem	Creem	k1gInSc1	Creem
Lester	Lester	k1gInSc1	Lester
Bangs	Bangs	k1gInSc4	Bangs
je	být	k5eAaImIp3nS	být
známý	známý	k1gMnSc1	známý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
počátkem	počátkem	k7c2	počátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zpopularizoval	zpopularizovat	k5eAaPmAgMnS	zpopularizovat
výraz	výraz	k1gInSc4	výraz
"	"	kIx"	"
<g/>
heavy	heav	k1gInPc4	heav
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
recenzoval	recenzovat	k5eAaImAgMnS	recenzovat
kapely	kapela	k1gFnPc4	kapela
jako	jako	k8xC	jako
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
a	a	k8xC	a
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
určití	určitý	k2eAgMnPc1d1	určitý
kritici	kritik	k1gMnPc1	kritik
používali	používat	k5eAaImAgMnP	používat
výraz	výraz	k1gInSc4	výraz
heavy	heava	k1gFnSc2	heava
metal	metat	k5eAaImAgMnS	metat
víceméně	víceméně	k9	víceméně
automaticky	automaticky	k6eAd1	automaticky
v	v	k7c6	v
hanlivém	hanlivý	k2eAgInSc6d1	hanlivý
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
popřední	popřední	k2eAgMnPc1d1	popřední
hudební	hudební	k2eAgMnPc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
John	John	k1gMnSc1	John
Rockwell	Rockwell	k1gMnSc1	Rockwell
z	z	k7c2	z
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
heavy	heava	k1gFnSc2	heava
metal	metat	k5eAaImAgInS	metat
rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
brutální	brutální	k2eAgFnSc4d1	brutální
agresivní	agresivní	k2eAgFnSc4d1	agresivní
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
hraje	hrát	k5eAaImIp3nS	hrát
pro	pro	k7c4	pro
nadrogované	nadrogovaný	k2eAgMnPc4d1	nadrogovaný
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
článku	článek	k1gInSc6	článek
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
rokenrol	rokenrol	k1gInSc1	rokenrol
dovedený	dovedený	k2eAgInSc1d1	dovedený
do	do	k7c2	do
krajnosti	krajnost	k1gFnSc2	krajnost
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
u	u	k7c2	u
bílých	bílý	k2eAgMnPc2d1	bílý
výrostků	výrostek	k1gMnPc2	výrostek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pojmy	pojem	k1gInPc4	pojem
"	"	kIx"	"
<g/>
heavy	heav	k1gInPc4	heav
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
hard	hard	k1gInSc1	hard
rock	rock	k1gInSc1	rock
<g/>
"	"	kIx"	"
často	často	k6eAd1	často
označují	označovat	k5eAaImIp3nP	označovat
stejný	stejný	k2eAgInSc4d1	stejný
typ	typ	k1gInSc4	typ
muziky	muzika	k1gFnSc2	muzika
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c6	o
kapelách	kapela	k1gFnPc6	kapela
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
často	často	k6eAd1	často
vzájemně	vzájemně	k6eAd1	vzájemně
zaměňovaly	zaměňovat	k5eAaImAgFnP	zaměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
kniha	kniha	k1gFnSc1	kniha
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Rock	rock	k1gInSc1	rock
&	&	k?	&
Roll	Roll	k1gInSc1	Roll
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
takovouhle	takovýhle	k3xDgFnSc4	takovýhle
pasáž	pasáž	k1gFnSc4	pasáž
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Aerosmith	Aerosmith	k1gInSc1	Aerosmith
<g/>
,	,	kIx,	,
proslulý	proslulý	k2eAgInSc1d1	proslulý
svým	svůj	k3xOyFgInSc7	svůj
agresivním	agresivní	k2eAgInSc7d1	agresivní
hardrockovým	hardrockový	k2eAgInSc7d1	hardrockový
stylem	styl	k1gInSc7	styl
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c6	na
bluesu	blues	k1gInSc6	blues
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
heavymetalové	heavymetalový	k2eAgFnPc4d1	heavymetalová
skupiny	skupina	k1gFnPc4	skupina
poloviny	polovina	k1gFnSc2	polovina
sedmdesátých	sedmdesátý	k4xOgInPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
by	by	kYmCp3nS	by
už	už	k9	už
asi	asi	k9	asi
sotva	sotva	k6eAd1	sotva
kdosi	kdosi	k3yInSc1	kdosi
charakterizoval	charakterizovat	k5eAaBmAgInS	charakterizovat
klasickou	klasický	k2eAgFnSc4d1	klasická
tvorbu	tvorba	k1gFnSc4	tvorba
Aerosmithu	Aerosmith	k1gInSc2	Aerosmith
<g/>
,	,	kIx,	,
jasně	jasně	k6eAd1	jasně
odkazující	odkazující	k2eAgInSc1d1	odkazující
na	na	k7c4	na
tradiční	tradiční	k2eAgInSc4d1	tradiční
rokenrol	rokenrol	k1gInSc4	rokenrol
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
heavy	heava	k1gFnSc2	heava
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stály	stát	k5eAaImAgFnP	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
a	a	k8xC	a
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
<g/>
,	,	kIx,	,
nebývají	bývat	k5eNaImIp3nP	bývat
současnou	současný	k2eAgFnSc7d1	současná
metalovou	metalový	k2eAgFnSc7d1	metalová
komunitou	komunita	k1gFnSc7	komunita
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c2	za
heavymetalové	heavymetalový	k2eAgFnSc2d1	heavymetalová
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgNnSc1d1	americké
blues	blues	k1gNnSc1	blues
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
první	první	k4xOgMnPc4	první
britské	britský	k2eAgMnPc4d1	britský
rockery	rocker	k1gMnPc4	rocker
<g/>
;	;	kIx,	;
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xC	jako
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
a	a	k8xC	a
The	The	k1gFnPc1	The
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
nahrály	nahrát	k5eAaBmAgFnP	nahrát
cover	cover	k1gInSc4	cover
verze	verze	k1gFnSc2	verze
mnohých	mnohý	k2eAgFnPc2d1	mnohá
klasických	klasický	k2eAgFnPc2d1	klasická
bluesových	bluesový	k2eAgFnPc2d1	bluesová
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
zrychlujíc	zrychlovat	k5eAaImSgNnS	zrychlovat
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
využívajíc	využívat	k5eAaImSgFnS	využívat
elektrické	elektrický	k2eAgNnSc1d1	elektrické
kytary	kytara	k1gFnPc4	kytara
místo	místo	k7c2	místo
původních	původní	k2eAgMnPc2d1	původní
akustických	akustický	k2eAgMnPc2d1	akustický
(	(	kIx(	(
<g/>
podobné	podobný	k2eAgFnPc4d1	podobná
adaptace	adaptace	k1gFnPc4	adaptace
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
"	"	kIx"	"
<g/>
černé	černý	k2eAgFnSc2d1	černá
hudby	hudba	k1gFnSc2	hudba
<g/>
"	"	kIx"	"
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
základ	základ	k1gInSc4	základ
původního	původní	k2eAgNnSc2d1	původní
rock	rock	k1gInSc4	rock
and	and	k?	and
rollu	roll	k1gInSc2	roll
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgFnPc1d1	britská
bluesově	bluesově	k6eAd1	bluesově
orientované	orientovaný	k2eAgFnPc1d1	orientovaná
kapely	kapela	k1gFnPc1	kapela
(	(	kIx(	(
<g/>
a	a	k8xC	a
jimi	on	k3xPp3gMnPc7	on
ovlivněné	ovlivněný	k2eAgFnSc2d1	ovlivněná
americké	americký	k2eAgFnSc2d1	americká
kapely	kapela	k1gFnSc2	kapela
<g/>
)	)	kIx)	)
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
hudebních	hudební	k2eAgInPc6d1	hudební
experimentech	experiment	k1gInPc6	experiment
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
:	:	kIx,	:
základem	základ	k1gInSc7	základ
byl	být	k5eAaImAgMnS	být
hlasitý	hlasitý	k2eAgMnSc1d1	hlasitý
<g/>
,	,	kIx,	,
zkreslený	zkreslený	k2eAgInSc1d1	zkreslený
kytarový	kytarový	k2eAgInSc1d1	kytarový
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
na	na	k7c6	na
silových	silový	k2eAgInPc6d1	silový
akordech	akord	k1gInPc6	akord
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
širšího	široký	k2eAgNnSc2d2	širší
povědomí	povědomí	k1gNnSc2	povědomí
dostaly	dostat	k5eAaPmAgFnP	dostat
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
The	The	k1gFnSc2	The
Kinks	Kinksa	k1gFnPc2	Kinksa
svojí	svojit	k5eAaImIp3nS	svojit
písní	píseň	k1gFnPc2	píseň
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
Really	Realla	k1gFnSc2	Realla
Got	Got	k1gMnSc1	Got
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
novému	nový	k2eAgInSc3d1	nový
kytarovému	kytarový	k2eAgInSc3d1	kytarový
zvuku	zvuk	k1gInSc3	zvuk
významně	významně	k6eAd1	významně
přispěla	přispět	k5eAaPmAgFnS	přispět
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
zesilovačů	zesilovač	k1gInPc2	zesilovač
se	se	k3xPyFc4	se
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
co	co	k9	co
blues-rockový	bluesockový	k2eAgInSc4d1	blues-rockový
styl	styl	k1gInSc4	styl
bicích	bicí	k2eAgFnPc2d1	bicí
byl	být	k5eAaImAgInS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInSc1d1	bicí
v	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
byly	být	k5eAaImAgFnP	být
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
<g/>
,	,	kIx,	,
komplexnější	komplexní	k2eAgFnPc1d2	komplexnější
a	a	k8xC	a
hlasitější	hlasitý	k2eAgFnPc1d2	hlasitější
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
přidával	přidávat	k5eAaImAgInS	přidávat
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
hlasitější	hlasitý	k2eAgInSc1d2	hlasitější
zvuk	zvuk	k1gInSc4	zvuk
elektrické	elektrický	k2eAgFnSc2d1	elektrická
kytary	kytara	k1gFnSc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Zpěváci	Zpěvák	k1gMnPc1	Zpěvák
změnili	změnit	k5eAaPmAgMnP	změnit
techniku	technika	k1gFnSc4	technika
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
stále	stále	k6eAd1	stále
závislejší	závislý	k2eAgMnPc1d2	závislejší
na	na	k7c4	na
zesilovaní	zesilovaný	k2eAgMnPc1d1	zesilovaný
zvuku	zvuk	k1gInSc2	zvuk
<g/>
;	;	kIx,	;
také	také	k9	také
využívali	využívat	k5eAaImAgMnP	využívat
nové	nový	k2eAgInPc4d1	nový
stylistické	stylistický	k2eAgInPc4d1	stylistický
a	a	k8xC	a
dramatické	dramatický	k2eAgInPc4d1	dramatický
postupy	postup	k1gInPc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
úroveň	úroveň	k1gFnSc1	úroveň
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
umožnil	umožnit	k5eAaPmAgInS	umožnit
plnohodnotný	plnohodnotný	k2eAgInSc1d1	plnohodnotný
záznam	záznam	k1gInSc1	záznam
této	tento	k3xDgFnSc2	tento
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
muziky	muzika	k1gFnSc2	muzika
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
blues-rocku	bluesock	k1gInSc2	blues-rock
a	a	k8xC	a
psychedelického	psychedelický	k2eAgInSc2d1	psychedelický
rocku	rock	k1gInSc2	rock
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
původní	původní	k2eAgFnSc1d1	původní
podstata	podstata	k1gFnSc1	podstata
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
definovaly	definovat	k5eAaBmAgFnP	definovat
hranice	hranice	k1gFnPc4	hranice
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rockové	rockový	k2eAgNnSc1d1	rockové
trio	trio	k1gNnSc1	trio
Cream	Cream	k1gInSc1	Cream
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
mohutný	mohutný	k2eAgInSc1d1	mohutný
<g/>
,	,	kIx,	,
hutný	hutný	k2eAgInSc1d1	hutný
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
Claptonovy	Claptonův	k2eAgFnSc2d1	Claptonova
bluesové	bluesový	k2eAgFnSc2d1	bluesová
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
silného	silný	k2eAgMnSc2d1	silný
Bruceovho	Bruceov	k1gMnSc2	Bruceov
hlasu	hlas	k1gInSc2	hlas
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
dynamické	dynamický	k2eAgFnSc2d1	dynamická
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
basovou	basový	k2eAgFnSc4d1	basová
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
jazzem	jazz	k1gInSc7	jazz
ovlivněných	ovlivněný	k2eAgInPc2d1	ovlivněný
rytmů	rytmus	k1gInPc2	rytmus
bubeníka	bubeník	k1gMnSc2	bubeník
Gingera	Ginger	k1gMnSc2	Ginger
Bakera	Baker	k1gMnSc2	Baker
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejich	jejich	k3xOp3gFnPc6	jejich
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
deskách	deska	k1gFnPc6	deska
<g/>
,	,	kIx,	,
Fresh	Fresh	k1gInSc1	Fresh
Cream	Cream	k1gInSc1	Cream
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
a	a	k8xC	a
Disraeli	Disrael	k1gInSc6	Disrael
Gears	Gearsa	k1gFnPc2	Gearsa
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
významně	významně	k6eAd1	významně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
celý	celý	k2eAgInSc4d1	celý
budoucí	budoucí	k2eAgInSc4d1	budoucí
žánr	žánr	k1gInSc4	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Are	ar	k1gInSc5	ar
You	You	k1gMnSc7	You
Experienced	Experienced	k1gMnSc1	Experienced
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
od	od	k7c2	od
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
nemalý	malý	k2eNgInSc4d1	nemalý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
metal	metal	k1gInSc4	metal
<g/>
.	.	kIx.	.
</s>
<s>
Hendrixovou	Hendrixový	k2eAgFnSc4d1	Hendrixová
kytarovou	kytarový	k2eAgFnSc4d1	kytarová
virtuozitu	virtuozita	k1gFnSc4	virtuozita
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
napodobit	napodobit	k5eAaPmF	napodobit
mnozí	mnohý	k2eAgMnPc1d1	mnohý
metaloví	metalový	k2eAgMnPc1d1	metalový
kytaristi	kytaristi	k?	kytaristi
<g/>
;	;	kIx,	;
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
šlágr	šlágr	k1gInSc1	šlágr
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Purple	Purple	k1gFnSc1	Purple
Haze	Haz	k1gFnSc2	Haz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejednou	jednou	k6eNd1	jednou
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
první	první	k4xOgInSc4	první
heavymetalový	heavymetalový	k2eAgInSc4d1	heavymetalový
hit	hit	k1gInSc4	hit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
počal	počnout	k5eAaPmAgMnS	počnout
utvářet	utvářet	k5eAaImF	utvářet
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
známý	známý	k1gMnSc1	známý
jako	jako	k9	jako
heavy	heava	k1gFnPc4	heava
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
na	na	k7c6	na
debutovém	debutový	k2eAgNnSc6d1	debutové
albu	album	k1gNnSc6	album
sanfranciské	sanfranciský	k2eAgFnSc2d1	sanfranciská
kapely	kapela	k1gFnSc2	kapela
Blue	Blu	k1gInSc2	Blu
Cheer	Cheero	k1gNnPc2	Cheero
s	s	k7c7	s
názvem	název	k1gInSc7	název
Vincebus	Vincebus	k1gMnSc1	Vincebus
Eruptum	Eruptum	k1gNnSc1	Eruptum
objevila	objevit	k5eAaPmAgFnS	objevit
cover	cover	k1gInSc4	cover
verze	verze	k1gFnSc2	verze
známého	známý	k2eAgInSc2d1	známý
hitu	hit	k1gInSc2	hit
"	"	kIx"	"
<g/>
Summertime	Summertim	k1gInSc5	Summertim
Blues	blues	k1gNnSc1	blues
<g/>
"	"	kIx"	"
od	od	k7c2	od
Eddieho	Eddie	k1gMnSc2	Eddie
Cochrana	Cochran	k1gMnSc2	Cochran
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všeobecnosti	všeobecnost	k1gFnSc6	všeobecnost
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
první	první	k4xOgFnSc4	první
heavymetalovou	heavymetalový	k2eAgFnSc4d1	heavymetalová
nahrávku	nahrávka	k1gFnSc4	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
měsíc	měsíc	k1gInSc1	měsíc
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
rovněž	rovněž	k9	rovněž
album	album	k1gNnSc4	album
Steppenwolf	Steppenwolf	k1gInSc1	Steppenwolf
od	od	k7c2	od
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
také	také	k9	také
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Born	Born	k1gMnSc1	Born
to	ten	k3xDgNnSc4	ten
Be	Be	k1gFnSc7	Be
Wild	Wild	k1gMnSc1	Wild
<g/>
"	"	kIx"	"
zmiňující	zmiňující	k2eAgMnSc1d1	zmiňující
se	se	k3xPyFc4	se
o	o	k7c4	o
"	"	kIx"	"
<g/>
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
t.	t.	k?	t.
r.	r.	kA	r.
se	se	k3xPyFc4	se
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
dostaly	dostat	k5eAaPmAgFnP	dostat
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zapsaly	zapsat	k5eAaPmAgFnP	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Think	Think	k1gMnSc1	Think
about	about	k1gMnSc1	about
It	It	k1gMnSc1	It
<g/>
"	"	kIx"	"
od	od	k7c2	od
The	The	k1gFnSc2	The
Yardbirds	Yardbirds	k1gInSc1	Yardbirds
(	(	kIx(	(
<g/>
strana	strana	k1gFnSc1	strana
B	B	kA	B
jejich	jejich	k3xOp3gInSc2	jejich
posledního	poslední	k2eAgInSc2d1	poslední
singlu	singl	k1gInSc2	singl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kytarista	kytarista	k1gMnSc1	kytarista
Jimmy	Jimma	k1gFnSc2	Jimma
Page	Page	k1gNnPc2	Page
uvedl	uvést	k5eAaPmAgInS	uvést
metalový	metalový	k2eAgInSc1d1	metalový
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ohromně	ohromně	k6eAd1	ohromně
populární	populární	k2eAgMnSc1d1	populární
<g/>
;	;	kIx,	;
a	a	k8xC	a
horký	horký	k2eAgMnSc1d1	horký
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
první	první	k4xOgNnSc4	první
heavymetalové	heavymetalový	k2eAgNnSc4d1	heavymetalové
album	album	k1gNnSc4	album
vůbec	vůbec	k9	vůbec
<g/>
:	:	kIx,	:
In-A-Gadda-Da-Vida	In-A-Gadda-Da-Vida	k1gFnSc1	In-A-Gadda-Da-Vida
od	od	k7c2	od
Iron	iron	k1gInSc4	iron
Butterfly	butterfly	k1gInSc4	butterfly
se	s	k7c7	s
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
17	[number]	k4	17
<g/>
minutovou	minutový	k2eAgFnSc7d1	minutová
titulní	titulní	k2eAgFnSc7d1	titulní
skladbou	skladba	k1gFnSc7	skladba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
t.	t.	k?	t.
r.	r.	kA	r.
Beatles	Beatles	k1gFnSc2	Beatles
svým	svůj	k3xOyFgInSc7	svůj
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
Revolution	Revolution	k1gInSc1	Revolution
<g/>
"	"	kIx"	"
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
nový	nový	k2eAgInSc4d1	nový
standard	standard	k1gInSc4	standard
pro	pro	k7c4	pro
zkreslení	zkreslení	k1gNnSc4	zkreslení
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
při	při	k7c6	při
nejúspěšnějších	úspěšný	k2eAgInPc6d3	nejúspěšnější
hitech	hit	k1gInPc6	hit
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Jeff	Jeff	k1gMnSc1	Jeff
Beck	Beck	k1gMnSc1	Beck
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc1	jejichž
lídr	lídr	k1gMnSc1	lídr
předtím	předtím	k6eAd1	předtím
zastával	zastávat	k5eAaImAgMnS	zastávat
místo	místo	k1gNnSc4	místo
kytaristy	kytarista	k1gMnSc2	kytarista
v	v	k7c6	v
The	The	k1gFnSc6	The
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
toho	ten	k3xDgNnSc2	ten
samého	samý	k3xTgInSc2	samý
měsíce	měsíc	k1gInSc2	měsíc
<g/>
:	:	kIx,	:
Truth	Truth	k1gInSc1	Truth
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
jedny	jeden	k4xCgFnPc4	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
nejžhavějších	žhavý	k2eAgInPc2d3	nejžhavější
<g/>
,	,	kIx,	,
nejostřejších	ostrý	k2eAgInPc2d3	nejostřejší
<g/>
,	,	kIx,	,
nejpřímočařejších	přímočarý	k2eAgInPc2d3	nejpřímočařejší
humorných	humorný	k2eAgInPc2d1	humorný
zvuků	zvuk	k1gInPc2	zvuk
všech	všecek	k3xTgInPc2	všecek
časů	čas	k1gInPc2	čas
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
čímž	což	k3yQnSc7	což
připravil	připravit	k5eAaPmAgInS	připravit
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
generaci	generace	k1gFnSc4	generace
metalových	metalový	k2eAgMnPc2d1	metalový
kytaristů	kytarista	k1gMnPc2	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
debutovala	debutovat	k5eAaBmAgFnS	debutovat
živou	živý	k2eAgFnSc7d1	živá
nahrávkou	nahrávka	k1gFnSc7	nahrávka
Pageova	Pageův	k2eAgFnSc1d1	Pageova
nová	nový	k2eAgFnSc1d1	nová
kapela	kapela	k1gFnSc1	kapela
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
kapela	kapela	k1gFnSc1	kapela
Love	lov	k1gInSc5	lov
Sculpture	Sculptur	k1gMnSc5	Sculptur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
kytaristou	kytarista	k1gMnSc7	kytarista
Dave	Dav	k1gInSc5	Dav
Edmunds	Edmunds	k1gInSc4	Edmunds
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Blues	blues	k1gFnSc2	blues
Helping	Helping	k1gInSc4	Helping
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
představila	představit	k5eAaPmAgFnS	představit
hřmotivou	hřmotivý	k2eAgFnSc7d1	hřmotivý
<g/>
,	,	kIx,	,
agresivní	agresivní	k2eAgFnSc7d1	agresivní
verzi	verze	k1gFnSc4	verze
Chačaturjanovy	Chačaturjanův	k2eAgFnPc4d1	Chačaturjanův
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Šavlový	šavlový	k2eAgInSc1d1	šavlový
tanec	tanec	k1gInSc1	tanec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Beatlesácké	Beatlesácký	k2eAgNnSc1d1	Beatlesácký
tzv.	tzv.	kA	tzv.
Bílé	bílý	k2eAgNnSc1d1	bílé
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
rovněž	rovněž	k9	rovněž
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
,	,	kIx,	,
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
"	"	kIx"	"
<g/>
Helter	Helter	k1gInSc1	Helter
Skelter	Skelter	k1gInSc1	Skelter
<g/>
"	"	kIx"	"
–	–	k?	–
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejhlučnějších	hlučný	k2eAgFnPc2d3	nejhlučnější
písní	píseň	k1gFnPc2	píseň
vydanou	vydaný	k2eAgFnSc4d1	vydaná
takovou	takový	k3xDgFnSc7	takový
významnou	významný	k2eAgFnSc7d1	významná
kapelou	kapela	k1gFnSc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1969	[number]	k4	1969
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
vydal	vydat	k5eAaPmAgInS	vydat
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
albovém	albový	k2eAgInSc6d1	albový
žebříčku	žebříček	k1gInSc6	žebříček
amerického	americký	k2eAgInSc2d1	americký
hudebního	hudební	k2eAgInSc2d1	hudební
týdeníku	týdeník	k1gInSc2	týdeník
Billboard	billboard	k1gInSc1	billboard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
Atlanta	Atlanta	k1gFnSc1	Atlanta
Pop	pop	k1gMnSc1	pop
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
rockové	rockový	k2eAgNnSc1d1	rockové
trio	trio	k1gNnSc1	trio
Grand	grand	k1gMnSc1	grand
Funk	funk	k1gInSc1	funk
Railroad	Railroad	k1gInSc1	Railroad
inspirované	inspirovaný	k2eAgFnSc2d1	inspirovaná
skupinou	skupina	k1gFnSc7	skupina
Cream	Cream	k1gInSc4	Cream
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
Creamem	Cream	k1gInSc7	Cream
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
seskupení	seskupení	k1gNnSc1	seskupení
–	–	k?	–
Leslie	Leslie	k1gFnSc2	Leslie
West	West	k1gMnSc1	West
–	–	k?	–
vydalo	vydat	k5eAaPmAgNnS	vydat
t.	t.	k?	t.
m.	m.	k?	m.
album	album	k1gNnSc1	album
Mountain	Mountain	k1gMnSc1	Mountain
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
blues-rockové	bluesockový	k2eAgFnPc4d1	blues-rocková
kytary	kytara	k1gFnPc4	kytara
a	a	k8xC	a
vřeštivé	vřeštivý	k2eAgInPc4d1	vřeštivý
vokály	vokál	k1gInPc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
–	–	k?	–
mezitím	mezitím	k6eAd1	mezitím
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Mountain	Mountain	k1gInSc4	Mountain
–	–	k?	–
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
na	na	k7c4	na
Woodstocku	Woodstocka	k1gFnSc4	Woodstocka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Grand	grand	k1gMnSc1	grand
Funk	funk	k1gInSc4	funk
Railroad	Railroad	k1gInSc4	Railroad
s	s	k7c7	s
názvem	název	k1gInSc7	název
On	on	k3xPp3gMnSc1	on
Time	Time	k1gFnPc3	Time
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
fenomenální	fenomenální	k2eAgInSc4d1	fenomenální
úspěch	úspěch	k1gInSc4	úspěch
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
II	II	kA	II
a	a	k8xC	a
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Whole	Whole	k1gInSc1	Whole
Lotta	Lott	k1gInSc2	Lott
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
časopisu	časopis	k1gInSc2	časopis
Billboard	billboard	k1gInSc1	billboard
<g/>
.	.	kIx.	.
</s>
<s>
Metalová	metalový	k2eAgFnSc1d1	metalová
revoluce	revoluce	k1gFnSc1	revoluce
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
začala	začít	k5eAaPmAgFnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
definovali	definovat	k5eAaBmAgMnP	definovat
klíčové	klíčový	k2eAgInPc4d1	klíčový
prvky	prvek	k1gInPc4	prvek
nového	nový	k2eAgInSc2d1	nový
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
Pageova	Pageův	k2eAgFnSc1d1	Pageova
značně	značně	k6eAd1	značně
zkreslená	zkreslený	k2eAgFnSc1d1	zkreslená
kytarová	kytarový	k2eAgFnSc1d1	kytarová
hra	hra	k1gFnSc1	hra
a	a	k8xC	a
Plantovy	Plantův	k2eAgInPc1d1	Plantův
dramatické	dramatický	k2eAgInPc1d1	dramatický
<g/>
,	,	kIx,	,
srdcervoucí	srdcervoucí	k2eAgInPc1d1	srdcervoucí
vokály	vokál	k1gInPc1	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konstituovaní	konstituovaný	k2eAgMnPc1d1	konstituovaný
žánru	žánr	k1gInSc3	žánr
byli	být	k5eAaImAgMnP	být
právě	právě	k9	právě
tak	tak	k6eAd1	tak
důležité	důležitý	k2eAgFnPc1d1	důležitá
i	i	k8xC	i
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
byl	být	k5eAaImAgMnS	být
více	hodně	k6eAd2	hodně
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
stabilnější	stabilní	k2eAgMnSc1d2	stabilnější
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ryzejší	ryzí	k2eAgMnSc1d2	ryzejší
<g/>
"	"	kIx"	"
heavymetalový	heavymetalový	k2eAgInSc1d1	heavymetalový
sound	sound	k1gInSc1	sound
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
byli	být	k5eAaImAgMnP	být
klíčovými	klíčový	k2eAgInPc7d1	klíčový
nahrávky	nahrávka	k1gFnPc1	nahrávka
od	od	k7c2	od
Black	Blacka	k1gFnPc2	Blacka
Sabbathu	Sabbath	k1gInSc2	Sabbath
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
a	a	k8xC	a
Paranoid	Paranoid	k1gInSc1	Paranoid
<g/>
)	)	kIx)	)
a	a	k8xC	a
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
(	(	kIx(	(
<g/>
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
in	in	k?	in
Rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
mimořádně	mimořádně	k6eAd1	mimořádně
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
sound	sound	k1gInSc4	sound
<g/>
.	.	kIx.	.
</s>
<s>
Zčásti	zčásti	k6eAd1	zčásti
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
zásluhu	zásluha	k1gFnSc4	zásluha
pracovní	pracovní	k2eAgFnSc1d1	pracovní
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
přihodila	přihodit	k5eAaPmAgFnS	přihodit
Tonymu	Tony	k1gMnSc3	Tony
Iommimu	Iommima	k1gFnSc4	Iommima
v	v	k7c6	v
roku	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
nebyl	být	k5eNaImAgMnS	být
schopný	schopný	k2eAgInSc4d1	schopný
normální	normální	k2eAgFnSc4d1	normální
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
přeladit	přeladit	k5eAaPmF	přeladit
svou	svůj	k3xOyFgFnSc4	svůj
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
hra	hra	k1gFnSc1	hra
nezpůsobovala	způsobovat	k5eNaImAgFnS	způsobovat
takovou	takový	k3xDgFnSc4	takový
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
silové	silový	k2eAgInPc4d1	silový
akordy	akord	k1gInPc4	akord
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInSc3	jejich
poměrně	poměrně	k6eAd1	poměrně
jednoduchému	jednoduchý	k2eAgInSc3d1	jednoduchý
prstokladu	prstoklad	k1gInSc3	prstoklad
<g/>
.	.	kIx.	.
</s>
<s>
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
létech	léto	k1gNnPc6	léto
neměli	mít	k5eNaImAgMnP	mít
vyhraněný	vyhraněný	k2eAgInSc4d1	vyhraněný
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
počnouc	počnout	k5eAaImSgFnS	počnout
rokem	rok	k1gInSc7	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
vokalistou	vokalista	k1gMnSc7	vokalista
Ianem	Ianus	k1gMnSc7	Ianus
Gillanem	Gillan	k1gMnSc7	Gillan
a	a	k8xC	a
kytaristou	kytarista	k1gMnSc7	kytarista
Ritchiem	Ritchius	k1gMnSc7	Ritchius
Blackmorem	Blackmor	k1gMnSc7	Blackmor
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
ubírat	ubírat	k5eAaImF	ubírat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
novovznikajícímu	novovznikající	k2eAgInSc3d1	novovznikající
heavymetalovému	heavymetalový	k2eAgInSc3d1	heavymetalový
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
1970	[number]	k4	1970
bodovali	bodovat	k5eAaImAgMnP	bodovat
ve	v	k7c6	v
významných	významný	k2eAgFnPc6d1	významná
britských	britský	k2eAgFnPc6d1	britská
hitparádách	hitparáda	k1gFnPc6	hitparáda
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Paranoid	Paranoid	k1gInSc1	Paranoid
<g/>
"	"	kIx"	"
a	a	k8xC	a
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
s	s	k7c7	s
"	"	kIx"	"
<g/>
Black	Black	k1gMnSc1	Black
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tři	tři	k4xCgFnPc1	tři
další	další	k2eAgFnPc1d1	další
britské	britský	k2eAgFnPc1d1	britská
kapely	kapela	k1gFnPc1	kapela
vydali	vydat	k5eAaPmAgMnP	vydat
toho	ten	k3xDgNnSc2	ten
roku	rok	k1gInSc2	rok
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
nesli	nést	k5eAaImAgMnP	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
:	:	kIx,	:
Very	Ver	k2eAgInPc4d1	Ver
'	'	kIx"	'
<g/>
eavy	eavy	k1gInPc4	eavy
<g/>
...	...	k?	...
Very	Ver	k2eAgFnPc4d1	Vera
'	'	kIx"	'
<g/>
umble	umble	k1gFnPc4	umble
od	od	k7c2	od
Uriah	Uriaha	k1gFnPc2	Uriaha
Heep	Heep	k1gMnSc1	Heep
<g/>
,	,	kIx,	,
UFO	UFO	kA	UFO
1	[number]	k4	1
od	od	k7c2	od
UFO	UFO	kA	UFO
a	a	k8xC	a
Sacrifice	Sacrifice	k1gFnSc1	Sacrifice
od	od	k7c2	od
Black	Blacka	k1gFnPc2	Blacka
Widow	Widow	k1gFnSc2	Widow
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
obyčejně	obyčejně	k6eAd1	obyčejně
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
heavy	heava	k1gFnPc4	heava
metal	metal	k1gInSc4	metal
<g/>
,	,	kIx,	,
Wishbone	Wishbon	k1gMnSc5	Wishbon
Ash	Ash	k1gMnSc5	Ash
zavedli	zavést	k5eAaPmAgMnP	zavést
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
styl	styl	k1gInSc1	styl
dvou	dva	k4xCgFnPc2	dva
kytar	kytara	k1gFnPc2	kytara
–	–	k?	–
rytmické	rytmický	k2eAgFnSc6d1	rytmická
a	a	k8xC	a
sólové	sólový	k2eAgFnSc6d1	sólová
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
mnohé	mnohé	k1gNnSc4	mnohé
budoucí	budoucí	k2eAgFnSc2d1	budoucí
metalové	metalový	k2eAgFnSc2d1	metalová
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Okultní	okultní	k2eAgFnSc1d1	okultní
tematika	tematika	k1gFnSc1	tematika
a	a	k8xC	a
obrazotvornost	obrazotvornost	k1gFnSc1	obrazotvornost
kapel	kapela	k1gFnPc2	kapela
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
,	,	kIx,	,
Uriah	Uriah	k1gMnSc1	Uriah
Heep	Heep	k1gMnSc1	Heep
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Widow	Widow	k1gFnSc2	Widow
měly	mít	k5eAaImAgFnP	mít
rovněž	rovněž	k9	rovněž
podstatný	podstatný	k2eAgInSc4d1	podstatný
vliv	vliv	k1gInSc4	vliv
<g/>
;	;	kIx,	;
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
začal	začít	k5eAaPmAgInS	začít
klást	klást	k5eAaImF	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
podobné	podobný	k2eAgInPc4d1	podobný
prvky	prvek	k1gInPc4	prvek
od	od	k7c2	od
svého	své	k1gNnSc2	své
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
alba	album	k1gNnSc2	album
vydaného	vydaný	k2eAgInSc2d1	vydaný
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
břehu	břeh	k1gInSc6	břeh
Atlantiku	Atlantik	k1gInSc2	Atlantik
určovala	určovat	k5eAaImAgFnS	určovat
kurz	kurz	k1gInSc1	kurz
Grand	grand	k1gMnSc1	grand
Funk	funk	k1gInSc1	funk
Railroad	Railroad	k1gInSc4	Railroad
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
americká	americký	k2eAgFnSc1d1	americká
heavymetalová	heavymetalový	k2eAgFnSc1d1	heavymetalová
kapela	kapela	k1gFnSc1	kapela
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
rozpadu	rozpad	k1gInSc2	rozpad
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
uvedla	uvést	k5eAaPmAgFnS	uvést
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
nový	nový	k2eAgInSc4d1	nový
výraz	výraz	k1gInSc4	výraz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
synonymem	synonymum	k1gNnSc7	synonymum
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
:	:	kIx,	:
soustavné	soustavný	k2eAgNnSc1d1	soustavné
koncertování	koncertování	k1gNnSc1	koncertování
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dalšími	další	k2eAgFnPc7d1	další
metalovými	metalový	k2eAgFnPc7d1	metalová
kapelami	kapela	k1gFnPc7	kapela
z	z	k7c2	z
USA	USA	kA	USA
byly	být	k5eAaImAgInP	být
Dust	Dust	k1gInSc4	Dust
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
deska	deska	k1gFnSc1	deska
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Blue	Blue	k1gFnSc1	Blue
Öyster	Öyster	k1gMnSc1	Öyster
Cult	Cult	k1gMnSc1	Cult
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
debutovala	debutovat	k5eAaBmAgFnS	debutovat
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
albem	album	k1gNnSc7	album
Lonesome	Lonesom	k1gInSc5	Lonesom
Crow	Crow	k1gFnPc7	Crow
kapela	kapela	k1gFnSc1	kapela
Scorpions	Scorpions	k1gInSc1	Scorpions
<g/>
.	.	kIx.	.
</s>
<s>
Blackmore	Blackmor	k1gMnSc5	Blackmor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
virtuozita	virtuozita	k1gFnSc1	virtuozita
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
na	na	k7c6	na
albu	album	k1gNnSc6	album
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
Machine	Machin	k1gInSc5	Machin
Head	Head	k1gMnSc1	Head
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
kapelu	kapela	k1gFnSc4	kapela
Rainbow	Rainbow	k1gMnSc1	Rainbow
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
kapely	kapela	k1gFnPc4	kapela
si	se	k3xPyFc3	se
získali	získat	k5eAaPmAgMnP	získat
přízeň	přízeň	k1gFnSc4	přízeň
publika	publikum	k1gNnSc2	publikum
neustálým	neustálý	k2eAgNnSc7d1	neustálé
koncertováním	koncertování	k1gNnSc7	koncertování
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zlepšujícím	zlepšující	k2eAgNnSc7d1	zlepšující
šoumenstvím	šoumenství	k1gNnSc7	šoumenství
<g/>
.	.	kIx.	.
</s>
<s>
Spornou	sporný	k2eAgFnSc7d1	sporná
otázkou	otázka	k1gFnSc7	otázka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
tvorbu	tvorba	k1gFnSc4	tvorba
těchto	tento	k3xDgInPc2	tento
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
průkopnických	průkopnický	k2eAgFnPc2d1	průkopnická
kapel	kapela	k1gFnPc2	kapela
možno	možno	k6eAd1	možno
považovat	považovat	k5eAaImF	považovat
vskutku	vskutku	k9	vskutku
za	za	k7c4	za
"	"	kIx"	"
<g/>
heavy	heav	k1gInPc4	heav
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
prostě	prostě	k9	prostě
jenom	jenom	k9	jenom
za	za	k7c4	za
"	"	kIx"	"
<g/>
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc4d1	mající
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
bluesovým	bluesový	k2eAgInPc3d1	bluesový
kořenům	kořen	k1gInPc3	kořen
či	či	k8xC	či
zakládající	zakládající	k2eAgMnPc1d1	zakládající
si	se	k3xPyFc3	se
na	na	k7c6	na
melodice	melodika	k1gFnSc6	melodika
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
většinou	většinou	k6eAd1	většinou
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Vynikajícím	vynikající	k2eAgInSc7d1	vynikající
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
debutovali	debutovat	k5eAaBmAgMnP	debutovat
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
albem	album	k1gNnSc7	album
High	Higha	k1gFnPc2	Higha
Voltage	Voltage	k1gNnSc2	Voltage
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
"	"	kIx"	"
<g/>
australské	australský	k2eAgFnSc3d1	australská
heavymetalové	heavymetalový	k2eAgFnSc3d1	heavymetalová
kapele	kapela	k1gFnSc3	kapela
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Rockový	rockový	k2eAgMnSc1d1	rockový
historik	historik	k1gMnSc1	historik
Clinton	Clinton	k1gMnSc1	Clinton
Walker	Walker	k1gMnSc1	Walker
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nazývat	nazývat	k5eAaImF	nazývat
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
heavymetalovou	heavymetalový	k2eAgFnSc7d1	heavymetalová
kapelou	kapela	k1gFnSc7	kapela
bylo	být	k5eAaImAgNnS	být
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
v	v	k7c6	v
létech	léto	k1gNnPc6	léto
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g />
.	.	kIx.	.
</s>
<s>
nezměnilo	změnit	k5eNaPmAgNnS	změnit
ani	ani	k8xC	ani
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
...	...	k?	...
Jsou	být	k5eAaImIp3nP	být
rokenrolovou	rokenrolový	k2eAgFnSc7d1	rokenrolová
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zkrátka	zkrátka	k6eAd1	zkrátka
hraje	hrát	k5eAaImIp3nS	hrát
tak	tak	k6eAd1	tak
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
v	v	k7c6	v
metalu	metal	k1gInSc6	metal
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
otázku	otázka	k1gFnSc4	otázka
definicí	definice	k1gFnPc2	definice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jasně	jasně	k6eAd1	jasně
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
samotným	samotný	k2eAgInSc7d1	samotný
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemu	co	k3yInSc3	co
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
fanoušci	fanoušek	k1gMnPc1	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Ian	Ian	k?	Ian
Christe	Christ	k1gMnSc5	Christ
píše	psát	k5eAaImIp3nS	psát
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
Ozzy	Ozza	k1gFnPc1	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
mluvčím	mluvčit	k5eAaImIp1nS	mluvčit
šílenosti	šílenost	k1gFnPc4	šílenost
rockových	rockový	k2eAgFnPc2d1	rocková
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
naprostou	naprostý	k2eAgFnSc4d1	naprostá
oddanost	oddanost	k1gFnSc4	oddanost
heavymetalových	heavymetalový	k2eAgMnPc2d1	heavymetalový
zatracenců	zatracenec	k1gMnPc2	zatracenec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Britská	britský	k2eAgFnSc1d1	britská
kapela	kapela	k1gFnSc1	kapela
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
,	,	kIx,	,
debutující	debutující	k2eAgFnSc1d1	debutující
albem	album	k1gNnSc7	album
Rocka	Rocek	k1gInSc2	Rocek
Rolla	Rollo	k1gNnSc2	Rollo
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
Black	Black	k1gInSc4	Black
Sabbathu	Sabbath	k1gInSc2	Sabbath
další	další	k2eAgFnSc7d1	další
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
brát	brát	k5eAaImF	brát
do	do	k7c2	do
úvahy	úvaha	k1gFnSc2	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Christe	Christ	k1gMnSc5	Christ
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
"	"	kIx"	"
<g/>
...	...	k?	...
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
posluchačům	posluchač	k1gMnPc3	posluchač
něco	něco	k6eAd1	něco
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgInPc4	tři
akordy	akord	k1gInPc4	akord
a	a	k8xC	a
dobrou	dobrý	k2eAgFnSc4d1	dobrá
show	show	k1gFnSc4	show
–	–	k?	–
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc1	jejich
obecenstvo	obecenstvo	k1gNnSc1	obecenstvo
chtělo	chtít	k5eAaImAgNnS	chtít
pátrat	pátrat	k5eAaImF	pátrat
dál	daleko	k6eAd2	daleko
po	po	k7c6	po
zvucích	zvuk	k1gInPc6	zvuk
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
účinkem	účinek	k1gInSc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
metalovou	metalový	k2eAgFnSc4d1	metalová
estetiku	estetika	k1gFnSc4	estetika
vystopovat	vystopovat	k5eAaPmF	vystopovat
jako	jako	k9	jako
bájnou	bájný	k2eAgFnSc4d1	bájná
bestii	bestie	k1gFnSc4	bestie
v	v	k7c6	v
náladové	náladový	k2eAgFnSc6d1	náladová
base	basa	k1gFnSc6	basa
a	a	k8xC	a
složitých	složitý	k2eAgFnPc6d1	složitá
zdvojených	zdvojený	k2eAgFnPc6d1	zdvojená
kytarách	kytara	k1gFnPc6	kytara
Thin	Thina	k1gFnPc2	Thina
Lizzy	Lizza	k1gFnSc2	Lizza
<g/>
,	,	kIx,	,
v	v	k7c6	v
jevištní	jevištní	k2eAgFnSc6d1	jevištní
stylizaci	stylizace	k1gFnSc6	stylizace
Alice	Alice	k1gFnSc1	Alice
Coopera	Coopera	k1gFnSc1	Coopera
<g/>
,	,	kIx,	,
v	v	k7c6	v
excelující	excelující	k2eAgFnSc6d1	excelující
kytaře	kytara	k1gFnSc6	kytara
a	a	k8xC	a
pompézních	pompézní	k2eAgInPc6d1	pompézní
vokálech	vokál	k1gInPc6	vokál
Queenu	Queen	k1gInSc2	Queen
a	a	k8xC	a
hřmotných	hřmotný	k2eAgInPc6d1	hřmotný
středověkých	středověký	k2eAgInPc6d1	středověký
námětech	námět	k1gInPc6	námět
Rainbow	Rainbow	k1gFnSc2	Rainbow
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
následujíce	následovat	k5eAaImSgFnP	následovat
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
z	z	k7c2	z
Birminghamu	Birmingham	k1gInSc2	Birmingham
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tato	tento	k3xDgNnPc1	tento
rozličná	rozličný	k2eAgNnPc1d1	rozličné
nejsvětlejší	světlý	k2eAgNnPc1d3	nejsvětlejší
místa	místo	k1gNnPc1	místo
hardrockové	hardrockový	k2eAgFnSc2d1	hardrocková
zvukové	zvukový	k2eAgFnSc2d1	zvuková
palety	paleta	k1gFnSc2	paleta
spojili	spojit	k5eAaPmAgMnP	spojit
a	a	k8xC	a
zesílili	zesílit	k5eAaPmAgMnP	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
z	z	k7c2	z
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
stal	stát	k5eAaPmAgInS	stát
žánr	žánr	k1gInSc1	žánr
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
jedno	jeden	k4xCgNnSc1	jeden
album	album	k1gNnSc1	album
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
až	až	k6eAd1	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
nedostalo	dostat	k5eNaPmAgNnS	dostat
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
40	[number]	k4	40
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
bývá	bývat	k5eAaImIp3nS	bývat
pokládána	pokládán	k2eAgFnSc1d1	pokládána
za	za	k7c4	za
následovníka	následovník	k1gMnSc4	následovník
sabbathovského	sabbathovský	k2eAgMnSc4d1	sabbathovský
heavy	heavo	k1gNnPc7	heavo
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Používajíc	používat	k5eAaImSgFnS	používat
dvě	dva	k4xCgFnPc1	dva
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
prudké	prudký	k2eAgInPc1d1	prudký
tempa	tempo	k1gNnSc2	tempo
a	a	k8xC	a
čistější	čistý	k2eAgInSc1d2	čistší
kovový	kovový	k2eAgInSc1d1	kovový
zvuk	zvuk	k1gInSc1	zvuk
bez	bez	k7c2	bez
bluesových	bluesový	k2eAgInPc2d1	bluesový
vlivů	vliv	k1gInPc2	vliv
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
standard	standard	k1gInSc4	standard
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
řídilo	řídit	k5eAaImAgNnS	řídit
nesčetné	sčetný	k2eNgNnSc1d1	nesčetné
množství	množství	k1gNnSc1	množství
budoucích	budoucí	k2eAgFnPc2d1	budoucí
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Heavy	Heav	k1gMnPc4	Heav
metal	metat	k5eAaImAgMnS	metat
sice	sice	k8xC	sice
získával	získávat	k5eAaImAgMnS	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc4d2	veliký
popularitu	popularita	k1gFnSc4	popularita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kritici	kritik	k1gMnPc1	kritik
tuto	tento	k3xDgFnSc4	tento
muziku	muzika	k1gFnSc4	muzika
neměli	mít	k5eNaImAgMnP	mít
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Nelíbilo	líbit	k5eNaImAgNnS	líbit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
sice	sice	k8xC	sice
<g/>
,	,	kIx,	,
že	že	k8xS	že
heavy	heava	k1gFnPc4	heava
metal	metal	k1gInSc1	metal
převzal	převzít	k5eAaPmAgInS	převzít
ze	z	k7c2	z
šoubyznysu	šoubyznys	k1gInSc2	šoubyznys
různé	různý	k2eAgInPc1d1	různý
obchodnické	obchodnický	k2eAgInPc1d1	obchodnický
triky	trik	k1gInPc1	trik
(	(	kIx(	(
<g/>
vizuální	vizuální	k2eAgFnSc1d1	vizuální
stránka	stránka	k1gFnSc1	stránka
projevu	projev	k1gInSc2	projev
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
největší	veliký	k2eAgFnSc2d3	veliký
námitky	námitka	k1gFnSc2	námitka
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
bezmyšlenkovitosti	bezmyšlenkovitost	k1gFnSc3	bezmyšlenkovitost
hudebního	hudební	k2eAgMnSc2d1	hudební
a	a	k8xC	a
textového	textový	k2eAgInSc2d1	textový
pojímaní	pojímaný	k2eAgMnPc1d1	pojímaný
<g/>
:	:	kIx,	:
v	v	k7c6	v
recenzi	recenze	k1gFnSc6	recenze
alba	album	k1gNnSc2	album
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
napsal	napsat	k5eAaPmAgInS	napsat
počátkem	počátkem	k7c2	počátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
let	let	k1gInSc4	let
přední	přední	k2eAgMnSc1d1	přední
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
Robert	Robert	k1gMnSc1	Robert
Christgau	Christgaa	k1gFnSc4	Christgaa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
metal	metal	k1gInSc1	metal
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pochmurný	pochmurný	k2eAgInSc4d1	pochmurný
a	a	k8xC	a
dekadentní	dekadentní	k2eAgNnSc4d1	dekadentní
<g/>
...	...	k?	...
<g/>
nedůvtipné	důvtipný	k2eNgNnSc4d1	nedůvtipné
<g/>
,	,	kIx,	,
nestydaté	nestydatý	k2eAgNnSc4d1	nestydaté
ožebračování	ožebračování	k1gNnSc4	ožebračování
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
punk	punk	k1gInSc1	punk
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
jako	jako	k9	jako
reakce	reakce	k1gFnPc4	reakce
jednak	jednak	k8xC	jednak
na	na	k7c4	na
moderní	moderní	k2eAgFnSc4d1	moderní
měšťáckou	měšťácký	k2eAgFnSc4d1	měšťácká
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
na	na	k7c4	na
rokenrol	rokenrol	k1gInSc4	rokenrol
jakoby	jakoby	k8xS	jakoby
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
na	na	k7c6	na
montážní	montážní	k2eAgFnSc6d1	montážní
lince	linka	k1gFnSc6	linka
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
heavy	heava	k1gFnPc1	heava
metal	metal	k1gInSc1	metal
<g/>
)	)	kIx)	)
těch	ten	k3xDgFnPc2	ten
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
prodej	prodej	k1gInSc1	prodej
heavymetalových	heavymetalový	k2eAgFnPc2d1	heavymetalová
nahrávek	nahrávka	k1gFnPc2	nahrávka
prudce	prudko	k6eAd1	prudko
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
punku	punk	k1gInSc2	punk
<g/>
,	,	kIx,	,
disca	disco	k1gNnSc2	disco
a	a	k8xC	a
komerčního	komerční	k2eAgInSc2d1	komerční
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
významná	významný	k2eAgNnPc1d1	významné
vydavatelství	vydavatelství	k1gNnPc1	vydavatelství
se	se	k3xPyFc4	se
orientovala	orientovat	k5eAaBmAgNnP	orientovat
na	na	k7c4	na
punk	punk	k1gInSc4	punk
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc4	množství
novějších	nový	k2eAgFnPc2d2	novější
britských	britský	k2eAgFnPc2d1	britská
heavymetalových	heavymetalový	k2eAgFnPc2d1	heavymetalová
kapel	kapela	k1gFnPc2	kapela
nacházelo	nacházet	k5eAaImAgNnS	nacházet
inspiraci	inspirace	k1gFnSc4	inspirace
právě	právě	k6eAd1	právě
v	v	k7c6	v
agresivitě	agresivita	k1gFnSc6	agresivita
<g/>
,	,	kIx,	,
energickém	energický	k2eAgInSc6d1	energický
zvuku	zvuk	k1gInSc6	zvuk
<g/>
,	,	kIx,	,
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
zvukové	zvukový	k2eAgFnSc6d1	zvuková
kvalitě	kvalita	k1gFnSc6	kvalita
a	a	k8xC	a
v	v	k7c6	v
postoji	postoj	k1gInSc6	postoj
"	"	kIx"	"
<g/>
udělej	udělat	k5eAaPmRp2nS	udělat
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
DIY	DIY	kA	DIY
"	"	kIx"	"
<g/>
do	do	k7c2	do
it	it	k?	it
yourself	yourself	k1gInSc1	yourself
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Undergroundové	undergroundový	k2eAgFnPc1d1	undergroundová
metalové	metalový	k2eAgFnPc1d1	metalová
kapely	kapela	k1gFnPc1	kapela
se	se	k3xPyFc4	se
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
vzít	vzít	k5eAaPmF	vzít
všechno	všechen	k3xTgNnSc4	všechen
do	do	k7c2	do
vlastních	vlastní	k2eAgFnPc2d1	vlastní
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
založit	založit	k5eAaPmF	založit
nezávislá	závislý	k2eNgNnPc1d1	nezávislé
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
vydavatelství	vydavatelství	k1gNnPc1	vydavatelství
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
hnutí	hnutí	k1gNnSc1	hnutí
šířilo	šířit	k5eAaImAgNnS	šířit
<g/>
,	,	kIx,	,
zavedené	zavedený	k2eAgInPc4d1	zavedený
britské	britský	k2eAgInPc4d1	britský
hudební	hudební	k2eAgInPc4d1	hudební
týdeníky	týdeník	k1gInPc4	týdeník
jako	jako	k8xS	jako
Sounds	Sounds	k1gInSc1	Sounds
a	a	k8xC	a
New	New	k1gFnSc1	New
Musical	musical	k1gInSc1	musical
Express	express	k1gInSc1	express
začaly	začít	k5eAaPmAgInP	začít
jevit	jevit	k5eAaImF	jevit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
metalové	metalový	k2eAgFnPc4d1	metalová
kapely	kapela	k1gFnPc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Geoff	Geoff	k1gInSc1	Geoff
Burton	Burton	k1gInSc1	Burton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
psal	psát	k5eAaImAgInS	psát
pro	pro	k7c4	pro
Sounds	Sounds	k1gInSc4	Sounds
<g/>
,	,	kIx,	,
zpopularizoval	zpopularizovat	k5eAaPmAgInS	zpopularizovat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
New	New	k1gFnSc1	New
Wave	Wav	k1gFnSc2	Wav
of	of	k?	of
British	British	k1gMnSc1	British
Heavy	Heava	k1gFnSc2	Heava
Metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zkracoval	zkracovat	k5eAaImAgMnS	zkracovat
do	do	k7c2	do
akronymu	akronym	k1gInSc2	akronym
NWOBHM	NWOBHM	kA	NWOBHM
<g/>
.	.	kIx.	.
</s>
<s>
Kapely	kapela	k1gFnPc1	kapela
NWOBHM	NWOBHM	kA	NWOBHM
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
,	,	kIx,	,
Motörhead	Motörhead	k1gInSc1	Motörhead
<g/>
,	,	kIx,	,
Saxon	Saxon	k1gMnSc1	Saxon
<g/>
,	,	kIx,	,
Diamond	Diamond	k1gMnSc1	Diamond
Head	Head	k1gMnSc1	Head
a	a	k8xC	a
Def	Def	k1gFnSc1	Def
Leppard	Lepparda	k1gFnPc2	Lepparda
daly	dát	k5eAaPmAgFnP	dát
heavy	heava	k1gFnPc1	heava
metalu	metal	k1gInSc2	metal
nový	nový	k2eAgInSc4d1	nový
dech	dech	k1gInSc4	dech
<g/>
.	.	kIx.	.
</s>
<s>
Následujíc	následovat	k5eAaImSgFnS	následovat
příklad	příklad	k1gInSc1	příklad
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
zhutnily	zhutnit	k5eAaPmAgFnP	zhutnit
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
omezily	omezit	k5eAaPmAgInP	omezit
bluesové	bluesový	k2eAgInPc1d1	bluesový
vlivy	vliv	k1gInPc1	vliv
a	a	k8xC	a
potrpěly	potrpět	k5eAaPmAgFnP	potrpět
si	se	k3xPyFc3	se
více	hodně	k6eAd2	hodně
na	na	k7c4	na
rychlé	rychlý	k2eAgFnPc4d1	rychlá
tempa	tempo	k1gNnPc4	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
<g/>
,	,	kIx,	,
Motörhead	Motörhead	k1gInSc1	Motörhead
a	a	k8xC	a
Saxon	Saxona	k1gFnPc2	Saxona
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
britských	britský	k2eAgFnPc2d1	britská
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
NWOBHM	NWOBHM	kA	NWOBHM
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
středního	střední	k2eAgInSc2d1	střední
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Motörhead	Motörhead	k1gInSc4	Motörhead
stali	stát	k5eAaPmAgMnP	stát
první	první	k4xOgMnPc1	první
kapelou	kapela	k1gFnSc7	kapela
z	z	k7c2	z
onoho	onen	k3xDgNnSc2	onen
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
písní	píseň	k1gFnSc7	píseň
No	no	k9	no
Sleep	Sleep	k1gInSc1	Sleep
'	'	kIx"	'
<g/>
til	til	k1gInSc1	til
Hammersmith	Hammersmitha	k1gFnPc2	Hammersmitha
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
britských	britský	k2eAgInPc2d1	britský
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
kapely	kapela	k1gFnPc1	kapela
NWOBHM	NWOBHM	kA	NWOBHM
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Diamond	Diamond	k1gMnSc1	Diamond
Head	Head	k1gMnSc1	Head
a	a	k8xC	a
Venom	Venom	k1gInSc1	Venom
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
rovněž	rovněž	k9	rovněž
nemalou	malý	k2eNgFnSc4d1	nemalá
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
takový	takový	k3xDgInSc4	takový
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Prvá	prvý	k4xOgFnSc1	prvý
generace	generace	k1gFnSc1	generace
metalových	metalový	k2eAgFnPc2d1	metalová
kapel	kapela	k1gFnPc2	kapela
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
vytrácela	vytrácet	k5eAaImAgFnS	vytrácet
ze	z	k7c2	z
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gFnSc2	Purple
se	se	k3xPyFc4	se
rozpadli	rozpadnout	k5eAaPmAgMnP	rozpadnout
zanedlouho	zanedlouho	k6eAd1	zanedlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Blackmore	Blackmor	k1gInSc5	Blackmor
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
opustil	opustit	k5eAaPmAgMnS	opustit
kapelu	kapela	k1gFnSc4	kapela
<g/>
,	,	kIx,	,
a	a	k8xC	a
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
skončili	skončit	k5eAaPmAgMnP	skončit
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Losangeleští	losangeleský	k2eAgMnPc1d1	losangeleský
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
předskakovali	předskakovat	k5eAaImAgMnP	předskakovat
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
,	,	kIx,	,
strhávali	strhávat	k5eAaImAgMnP	strhávat
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
stárnoucích	stárnoucí	k2eAgMnPc2d1	stárnoucí
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
Eddie	Eddie	k1gFnSc1	Eddie
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejoslnivějších	oslnivý	k2eAgInPc2d3	nejoslnivější
metalových	metalový	k2eAgInPc2d1	metalový
virtuózů	virtuóz	k1gInPc2	virtuóz
onoho	onen	k3xDgNnSc2	onen
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Christeho	Christe	k1gMnSc2	Christe
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
technika	technika	k1gFnSc1	technika
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
byla	být	k5eAaImAgFnS	být
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
a	a	k8xC	a
parádní	parádní	k2eAgNnPc1d1	parádní
sóla	sólo	k1gNnPc1	sólo
jako	jako	k8xS	jako
'	'	kIx"	'
<g/>
Eruption	Eruption	k1gInSc1	Eruption
<g/>
'	'	kIx"	'
vynesla	vynést	k5eAaPmAgFnS	vynést
heavymetalovou	heavymetalový	k2eAgFnSc4d1	heavymetalová
kytaru	kytara	k1gFnSc4	kytara
na	na	k7c4	na
úplný	úplný	k2eAgInSc4d1	úplný
vrchol	vrchol	k1gInSc4	vrchol
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pověstné	pověstný	k2eAgFnPc4d1	pověstná
virtuózy	virtuóza	k1gFnPc4	virtuóza
se	se	k3xPyFc4	se
zařadili	zařadit	k5eAaPmAgMnP	zařadit
rovněž	rovněž	k9	rovněž
muzikanti	muzikant	k1gMnPc1	muzikant
Randy	rand	k1gInPc1	rand
Rhoads	Rhoads	k1gInSc1	Rhoads
a	a	k8xC	a
Yngwie	Yngwie	k1gFnSc1	Yngwie
Malmsteen	Malmsteno	k1gNnPc2	Malmsteno
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
spájejí	spájet	k5eAaImIp3nP	spájet
se	s	k7c7	s
stylem	styl	k1gInSc7	styl
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
známém	známý	k2eAgNnSc6d1	známé
jako	jako	k9	jako
"	"	kIx"	"
<g/>
neoklasický	neoklasický	k2eAgInSc1d1	neoklasický
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
interpretů	interpret	k1gMnPc2	interpret
inspirovaných	inspirovaný	k2eAgInPc2d1	inspirovaný
klasickou	klasický	k2eAgFnSc7d1	klasická
hudbou	hudba	k1gFnSc7	hudba
byli	být	k5eAaImAgMnP	být
Blackmore	Blackmor	k1gInSc5	Blackmor
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
Scorpions	Scorpionsa	k1gFnPc2	Scorpionsa
Uli	Uli	k1gMnSc1	Uli
Jon	Jon	k1gMnSc1	Jon
Roth	Roth	k1gMnSc1	Roth
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
používání	používání	k1gNnSc1	používání
klasicistických	klasicistický	k2eAgFnPc2d1	klasicistická
kytar	kytara	k1gFnPc2	kytara
s	s	k7c7	s
nylonovými	nylonový	k2eAgFnPc7d1	nylonová
strunami	struna	k1gFnPc7	struna
–	–	k?	–
činil	činit	k5eAaImAgInS	činit
tak	tak	k9	tak
například	například	k6eAd1	například
Rhoads	Rhoads	k1gInSc4	Rhoads
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Dee	Dee	k1gFnPc6	Dee
<g/>
"	"	kIx"	"
na	na	k7c6	na
nahrávce	nahrávka	k1gFnSc6	nahrávka
Blizzard	Blizzard	k1gMnSc1	Blizzard
of	of	k?	of
Ozz	Ozz	k1gMnSc1	Ozz
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
debutovém	debutový	k2eAgNnSc6d1	debutové
albu	album	k1gNnSc6	album
bývalého	bývalý	k2eAgMnSc2d1	bývalý
zpěváka	zpěvák	k1gMnSc2	zpěvák
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
Ozzyho	Ozzy	k1gMnSc2	Ozzy
Osbourna	Osbourno	k1gNnSc2	Osbourno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
metalová	metalový	k2eAgFnSc1d1	metalová
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
povzbuzená	povzbuzený	k2eAgFnSc1d1	povzbuzená
úspěchem	úspěch	k1gInSc7	úspěch
Van	vana	k1gFnPc2	vana
Halenovců	Halenovec	k1gMnPc2	Halenovec
<g/>
,	,	kIx,	,
počala	počnout	k5eAaPmAgFnS	počnout
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klubech	klub	k1gInPc6	klub
na	na	k7c4	na
Sunset	Sunset	k1gInSc4	Sunset
Strip	Strip	k1gInSc4	Strip
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
mají	mít	k5eAaImIp3nP	mít
kořeny	kořen	k1gInPc1	kořen
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xC	jako
Quiet	Quiet	k1gMnSc1	Quiet
Riot	Riot	k1gMnSc1	Riot
<g/>
,	,	kIx,	,
Ratt	Ratt	k1gMnSc1	Ratt
<g/>
,	,	kIx,	,
Mötley	Mötlea	k1gMnSc2	Mötlea
Crüe	Crü	k1gMnSc2	Crü
a	a	k8xC	a
W.	W.	kA	W.
<g/>
A.S.	A.S.	k1gFnSc2	A.S.
<g/>
P.	P.	kA	P.
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nechaly	nechat	k5eAaPmAgFnP	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
tradičním	tradiční	k2eAgFnPc3d1	tradiční
heavy	heav	k1gInPc1	heav
metalem	metal	k1gInSc7	metal
počátku	počátek	k1gInSc2	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
od	od	k7c2	od
glamrockových	glamrockových	k2eAgNnSc2d1	glamrockových
seskupení	seskupení	k1gNnSc2	seskupení
typu	typ	k1gInSc2	typ
Alice	Alice	k1gFnSc2	Alice
Cooper	Cooper	k1gInSc1	Cooper
a	a	k8xC	a
Kiss	Kiss	k1gInSc1	Kiss
si	se	k3xPyFc3	se
vypůjčily	vypůjčit	k5eAaPmAgInP	vypůjčit
divadelní	divadelní	k2eAgInSc4d1	divadelní
prvky	prvek	k1gInPc4	prvek
(	(	kIx(	(
<g/>
a	a	k8xC	a
občas	občas	k6eAd1	občas
též	též	k9	též
oblibu	obliba	k1gFnSc4	obliba
líčení	líčení	k1gNnSc2	líčení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Glam	Glam	k1gInSc1	Glam
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
hédonistickými	hédonistický	k2eAgInPc7d1	hédonistický
texty	text	k1gInPc7	text
a	a	k8xC	a
živelným	živelný	k2eAgNnSc7d1	živelné
chováním	chování	k1gNnSc7	chování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
stránce	stránka	k1gFnSc6	stránka
ho	on	k3xPp3gInSc4	on
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
rychlé	rychlý	k2eAgNnSc4d1	rychlé
kytarové	kytarový	k2eAgNnSc4d1	kytarové
sóla	sólo	k1gNnPc4	sólo
<g/>
,	,	kIx,	,
hymnické	hymnický	k2eAgInPc4d1	hymnický
refrény	refrén	k1gInPc4	refrén
a	a	k8xC	a
melodičnost	melodičnost	k1gFnSc1	melodičnost
více-méně	víceéeň	k1gFnSc2	více-méeň
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
popem	pop	k1gMnSc7	pop
<g/>
.	.	kIx.	.
</s>
<s>
Glammetalové	Glammetal	k1gMnPc1	Glammetal
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
typu	typ	k1gInSc2	typ
Twisted	Twisted	k1gMnSc1	Twisted
Sister	Sister	k1gMnSc1	Sister
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
sílou	síla	k1gFnSc7	síla
v	v	k7c6	v
metalu	metal	k1gInSc6	metal
a	a	k8xC	a
rockové	rockový	k2eAgFnSc3d1	rocková
hudbě	hudba	k1gFnSc3	hudba
jako	jako	k9	jako
takové	takový	k3xDgFnSc3	takový
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
NWOBHM	NWOBHM	kA	NWOBHM
a	a	k8xC	a
po	po	k7c6	po
přelomovém	přelomový	k2eAgNnSc6d1	přelomové
albu	album	k1gNnSc6	album
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
British	British	k1gMnSc1	British
Steel	Steel	k1gMnSc1	Steel
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
heavy	heava	k1gFnSc2	heava
metal	metal	k1gInSc1	metal
stával	stávat	k5eAaImAgInS	stávat
stále	stále	k6eAd1	stále
oblíbenější	oblíbený	k2eAgInSc1d2	oblíbenější
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
na	na	k7c6	na
MTV	MTV	kA	MTV
měla	mít	k5eAaImAgFnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
propagaci	propagace	k1gFnSc4	propagace
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
případech	případ	k1gInPc6	případ
pomohla	pomoct	k5eAaPmAgFnS	pomoct
znásobit	znásobit	k5eAaPmF	znásobit
prodej	prodej	k1gInSc4	prodej
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Videoklipy	videoklip	k1gInPc1	videoklip
Def	Def	k1gFnSc1	Def
Leppard	Leppard	k1gInSc1	Leppard
k	k	k7c3	k
albu	album	k1gNnSc3	album
Pyromania	Pyromanium	k1gNnSc2	Pyromanium
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
udělaly	udělat	k5eAaPmAgFnP	udělat
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
hvězdy	hvězda	k1gFnSc2	hvězda
první	první	k4xOgFnSc2	první
kategorie	kategorie	k1gFnSc2	kategorie
a	a	k8xC	a
Quiet	Quiet	k1gMnSc1	Quiet
Riot	Riot	k1gMnSc1	Riot
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
první	první	k4xOgFnSc7	první
tamější	tamější	k2eAgFnSc7d1	tamější
heavymetalovou	heavymetalový	k2eAgFnSc7d1	heavymetalová
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Metal	metat	k5eAaImAgMnS	metat
Health	Health	k1gMnSc1	Health
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležitým	důležitý	k2eAgInSc7d1	důležitý
při	při	k7c6	při
rozšiřovaní	rozšiřovaný	k2eAgMnPc1d1	rozšiřovaný
posluchačské	posluchačský	k2eAgFnSc2d1	posluchačská
základny	základna	k1gFnSc2	základna
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
"	"	kIx"	"
<g/>
US	US	kA	US
Festival	festival	k1gInSc1	festival
<g/>
"	"	kIx"	"
konaný	konaný	k2eAgInSc1d1	konaný
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
třídenním	třídenní	k2eAgInSc6d1	třídenní
festivalu	festival	k1gInSc6	festival
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
čistě	čistě	k6eAd1	čistě
jenom	jenom	k6eAd1	jenom
heavymetalovému	heavymetalový	k2eAgInSc3d1	heavymetalový
žánru	žánr	k1gInSc3	žánr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
tu	tu	k6eAd1	tu
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
Ozzy	Ozz	k2eAgFnPc4d1	Ozz
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
,	,	kIx,	,
Van	van	k1gInSc4	van
Halen	halena	k1gFnPc2	halena
<g/>
,	,	kIx,	,
Scorpions	Scorpionsa	k1gFnPc2	Scorpionsa
<g/>
,	,	kIx,	,
Mötley	Mötle	k2eAgFnPc1d1	Mötle
Crüe	Crü	k1gFnPc1	Crü
<g/>
,	,	kIx,	,
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
,	,	kIx,	,
a	a	k8xC	a
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přitáhnout	přitáhnout	k5eAaPmF	přitáhnout
největší	veliký	k2eAgFnSc4d3	veliký
pozornost	pozornost	k1gFnSc4	pozornost
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1983	[number]	k4	1983
a	a	k8xC	a
1984	[number]	k4	1984
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
prodejnost	prodejnost	k1gFnSc1	prodejnost
heavymetalových	heavymetalový	k2eAgFnPc2d1	heavymetalová
nahrávek	nahrávka	k1gFnPc2	nahrávka
z	z	k7c2	z
8	[number]	k4	8
na	na	k7c4	na
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
všech	všecek	k3xTgFnPc2	všecek
nahrávek	nahrávka	k1gFnPc2	nahrávka
prodaných	prodaný	k2eAgFnPc2d1	prodaná
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vícero	vícero	k1gNnSc1	vícero
popředních	popřední	k2eAgInPc2d1	popřední
odborných	odborný	k2eAgInPc2d1	odborný
časopisů	časopis	k1gInPc2	časopis
zasvěcených	zasvěcený	k2eAgInPc2d1	zasvěcený
žánru	žánr	k1gInSc3	žánr
začalo	začít	k5eAaPmAgNnS	začít
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
například	například	k6eAd1	například
Kerrang	Kerranga	k1gFnPc2	Kerranga
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
založený	založený	k2eAgInSc4d1	založený
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
a	a	k8xC	a
Metal	metal	k1gInSc1	metal
Hammer	Hammra	k1gFnPc2	Hammra
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
Billboard	billboard	k1gInSc1	billboard
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
metal	metal	k1gInSc1	metal
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
diváckou	divácký	k2eAgFnSc4d1	divácká
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Metalová	metalový	k2eAgFnSc1d1	metalová
muzika	muzika	k1gFnSc1	muzika
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
výhradní	výhradní	k2eAgFnSc7d1	výhradní
doménou	doména	k1gFnSc7	doména
výrostků	výrostek	k1gInPc2	výrostek
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Metalové	metalový	k2eAgNnSc1d1	metalové
posluchačstvo	posluchačstvo	k1gNnSc1	posluchačstvo
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
o	o	k7c4	o
starší	starší	k1gMnPc4	starší
(	(	kIx(	(
<g/>
vysokoškoláky	vysokoškolák	k1gMnPc7	vysokoškolák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc1d2	mladší
(	(	kIx(	(
<g/>
mladší	mladý	k2eAgFnSc1d2	mladší
žáky	žák	k1gMnPc7	žák
<g/>
)	)	kIx)	)
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
měl	mít	k5eAaImAgInS	mít
největší	veliký	k2eAgNnSc4d3	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgFnPc6d1	hudební
televizích	televize	k1gFnPc6	televize
a	a	k8xC	a
na	na	k7c6	na
koncertních	koncertní	k2eAgFnPc6d1	koncertní
šňůrách	šňůra	k1gFnPc6	šňůra
glam	glam	k6eAd1	glam
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgMnPc3d1	hlavní
představitelům	představitel	k1gMnPc3	představitel
patří	patřit	k5eAaImIp3nS	patřit
skupiny	skupina	k1gFnSc2	skupina
Warrant	Warrant	k1gMnSc1	Warrant
z	z	k7c2	z
L.A.	L.A.	k1gFnSc2	L.A.
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnPc1	skupina
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
jako	jako	k8xS	jako
Poison	Poison	k1gMnSc1	Poison
<g/>
,	,	kIx,	,
Cinderella	Cinderella	k1gMnSc1	Cinderella
<g/>
,	,	kIx,	,
či	či	k8xC	či
neméně	málo	k6eNd2	málo
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
kapely	kapela	k1gFnPc1	kapela
Mötley	Mötle	k2eAgFnPc1d1	Mötle
Crüe	Crüe	k1gInSc4	Crüe
a	a	k8xC	a
Ratt	Ratt	k1gInSc4	Ratt
<g/>
.	.	kIx.	.
</s>
<s>
Stylistickou	stylistický	k2eAgFnSc4d1	stylistická
mezeru	mezera	k1gFnSc4	mezera
mezi	mezi	k7c4	mezi
hard	hard	k1gInSc4	hard
rockem	rock	k1gInSc7	rock
a	a	k8xC	a
glam	glam	k6eAd1	glam
metalem	metal	k1gInSc7	metal
zaplnila	zaplnit	k5eAaPmAgFnS	zaplnit
skupina	skupina	k1gFnSc1	skupina
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
z	z	k7c2	z
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nesmírnou	smírný	k2eNgFnSc4d1	nesmírná
popularitu	popularita	k1gFnSc4	popularita
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
třetímu	třetí	k4xOgMnSc3	třetí
albu	album	k1gNnSc6	album
Slippery	Slippera	k1gFnSc2	Slippera
When	When	k1gMnSc1	When
Wet	Wet	k1gMnSc1	Wet
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
1987	[number]	k4	1987
začala	začít	k5eAaPmAgFnS	začít
MTV	MTV	kA	MTV
vysílat	vysílat	k5eAaImF	vysílat
pořad	pořad	k1gInSc4	pořad
Headbanger	Headbanger	k1gInSc1	Headbanger
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ball	Ball	k1gInSc1	Ball
<g/>
,	,	kIx,	,
zasvěcený	zasvěcený	k2eAgMnSc1d1	zasvěcený
výlučně	výlučně	k6eAd1	výlučně
heavymetalovým	heavymetalový	k2eAgInPc3d1	heavymetalový
videoklipům	videoklip	k1gInPc3	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Metalová	metalový	k2eAgFnSc1d1	metalová
komunita	komunita	k1gFnSc1	komunita
se	se	k3xPyFc4	se
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
na	na	k7c4	na
příznivce	příznivec	k1gMnPc4	příznivec
komerčního	komerční	k2eAgInSc2d1	komerční
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
na	na	k7c4	na
přívržence	přívrženec	k1gMnSc4	přívrženec
undergroundu	underground	k1gInSc2	underground
pohrdajících	pohrdající	k2eAgInPc2d1	pohrdající
populárním	populární	k2eAgInSc7d1	populární
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
"	"	kIx"	"
<g/>
lite	lit	k1gInSc5	lit
metal	metat	k5eAaImAgMnS	metat
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
hair	hair	k1gMnSc1	hair
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Skupině	skupina	k1gFnSc3	skupina
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gInSc1	Roses
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
pozornost	pozornost	k1gFnSc4	pozornost
fanoušků	fanoušek	k1gMnPc2	fanoušek
různých	různý	k2eAgInPc2d1	různý
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
glammetalových	glammetalův	k2eAgFnPc2d1	glammetalův
kapel	kapela	k1gFnPc2	kapela
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byli	být	k5eAaImAgMnP	být
omnoho	omnoho	k6eAd1	omnoho
surovější	surový	k2eAgMnPc1d2	surovější
a	a	k8xC	a
jízlivější	jízlivý	k2eAgMnPc1d2	jízlivý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nesmírném	smírný	k2eNgInSc6d1	nesmírný
úspěchu	úspěch	k1gInSc6	úspěch
jejich	jejich	k3xOp3gFnSc2	jejich
debutové	debutový	k2eAgFnSc2d1	debutová
desky	deska	k1gFnSc2	deska
Appetite	Appetit	k1gInSc5	Appetit
for	forum	k1gNnPc2	forum
Destruction	Destruction	k1gInSc1	Destruction
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
stala	stát	k5eAaPmAgFnS	stát
cílem	cíl	k1gInSc7	cíl
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
všechen	všechen	k3xTgInSc4	všechen
další	další	k2eAgInSc4d1	další
metal	metal	k1gInSc4	metal
po	po	k7c4	po
následující	následující	k2eAgFnSc4d1	následující
dekádu	dekáda	k1gFnSc4	dekáda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rok	rok	k1gInSc1	rok
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
samé	samý	k3xTgFnSc2	samý
losangeleské	losangeleský	k2eAgFnSc2d1	losangeleská
hardrockové	hardrockový	k2eAgFnSc2d1	hardrocková
klubové	klubový	k2eAgFnSc2d1	klubová
scény	scéna	k1gFnSc2	scéna
vynořila	vynořit	k5eAaPmAgFnS	vynořit
kapela	kapela	k1gFnSc1	kapela
Jane	Jan	k1gMnSc5	Jan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Addiction	Addiction	k1gInSc1	Addiction
s	s	k7c7	s
debutem	debut	k1gInSc7	debut
Nothing	Nothing	k1gInSc1	Nothing
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Shocking	Shocking	k1gInSc1	Shocking
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Jane	Jan	k1gMnSc5	Jan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Addiction	Addiction	k1gInSc1	Addiction
jsou	být	k5eAaImIp3nP	být
právoplatným	právoplatný	k2eAgMnSc7d1	právoplatný
dědicem	dědic	k1gMnSc7	dědic
odkazu	odkaz	k1gInSc2	odkaz
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žádná	žádný	k3yNgFnSc1	žádný
kapela	kapela	k1gFnSc1	kapela
jim	on	k3xPp3gMnPc3	on
doposud	doposud	k6eAd1	doposud
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
věrná	věrný	k2eAgFnSc1d1	věrná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
kapel	kapela	k1gFnPc2	kapela
spájených	spájený	k2eAgFnPc2d1	spájená
z	z	k7c2	z
"	"	kIx"	"
<g/>
alternativním	alternativní	k2eAgInSc7d1	alternativní
metalem	metal	k1gInSc7	metal
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
glam	glama	k1gFnPc2	glama
metalu	metal	k1gInSc2	metal
mezičasem	mezičas	k1gInSc7	mezičas
udržovaly	udržovat	k5eAaImAgFnP	udržovat
nové	nový	k2eAgFnPc1d1	nová
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xS	jako
newyorští	newyorský	k2eAgMnPc1d1	newyorský
Winger	Winger	k1gMnSc1	Winger
anebo	anebo	k8xC	anebo
Skid	Skid	k1gMnSc1	Skid
Row	Row	k1gMnSc1	Row
z	z	k7c2	z
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
spousta	spousta	k1gFnSc1	spousta
heavymetalových	heavymetalový	k2eAgInPc2d1	heavymetalový
stylů	styl	k1gInPc2	styl
mimo	mimo	k6eAd1	mimo
komerčního	komerční	k2eAgInSc2d1	komerční
středního	střední	k2eAgInSc2d1	střední
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Mapování	mapování	k1gNnSc1	mapování
spletitého	spletitý	k2eAgInSc2d1	spletitý
světa	svět	k1gInSc2	svět
undergroundového	undergroundový	k2eAgInSc2d1	undergroundový
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řade	řad	k1gInSc5	řad
editoři	editor	k1gMnPc1	editor
hudebního	hudební	k2eAgInSc2d1	hudební
serveru	server	k1gInSc2	server
Allmusic	Allmusic	k1gMnSc1	Allmusic
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
kritik	kritik	k1gMnSc1	kritik
Garry	Garra	k1gFnSc2	Garra
Sharpe-Young	Sharpe-Young	k1gMnSc1	Sharpe-Young
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sharpe-Youngové	Sharpe-Youngový	k2eAgFnSc6d1	Sharpe-Youngový
několikasvazkové	několikasvazkový	k2eAgFnSc6d1	několikasvazková
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
je	být	k5eAaImIp3nS	být
metal	metal	k1gInSc4	metal
rozdělený	rozdělený	k2eAgInSc4d1	rozdělený
do	do	k7c2	do
pěti	pět	k4xCc2	pět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
thrash	thrasha	k1gFnPc2	thrasha
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
black	black	k1gInSc1	black
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
power	power	k1gInSc1	power
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
plus	plus	k6eAd1	plus
spříbuzněné	spříbuzněný	k2eAgInPc1d1	spříbuzněný
styly	styl	k1gInPc1	styl
doom	dooma	k1gFnPc2	dooma
a	a	k8xC	a
gothic	gothice	k1gInPc2	gothice
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Thrash	Thrash	k1gMnSc1	Thrash
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
Thrash	Thrash	k1gInSc1	Thrash
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
počátkem	počátkem	k7c2	počátkem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
příbuzný	příbuzný	k1gMnSc1	příbuzný
–	–	k?	–
speed	speed	k1gInSc1	speed
metal	metat	k5eAaImAgInS	metat
–	–	k?	–
rovněž	rovněž	k9	rovněž
thrash	thrash	k1gMnSc1	thrash
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
hardcore	hardcor	k1gInSc5	hardcor
punku	punk	k1gInSc6	punk
a	a	k8xC	a
Nové	Nové	k2eAgFnPc1d1	Nové
vlny	vlna	k1gFnPc1	vlna
britského	britský	k2eAgInSc2d1	britský
heavy	heavo	k1gNnPc7	heavo
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
scéna	scéna	k1gFnSc1	scéna
v	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
thrash	thrash	k1gMnSc1	thrash
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sanfranciského	sanfranciský	k2eAgInSc2d1	sanfranciský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Thrashové	Thrashový	k2eAgFnPc1d1	Thrashový
kapely	kapela	k1gFnPc1	kapela
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
nový	nový	k2eAgInSc4d1	nový
sound	sound	k1gInSc4	sound
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
větší	veliký	k2eAgFnSc7d2	veliký
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
agresivitou	agresivita	k1gFnSc7	agresivita
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
původním	původní	k2eAgMnSc7d1	původní
heavy	heav	k1gMnPc4	heav
metalem	metal	k1gInSc7	metal
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
následovníkem	následovník	k1gMnSc7	následovník
–	–	k?	–
glam	glam	k6eAd1	glam
metalem	metal	k1gInSc7	metal
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
prolínají	prolínat	k5eAaImIp3nP	prolínat
nízké	nízký	k2eAgInPc4d1	nízký
kytarové	kytarový	k2eAgInPc4d1	kytarový
riffy	riff	k1gInPc4	riff
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
shredovou	shredův	k2eAgFnSc7d1	shredův
kytarou	kytara	k1gFnSc7	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
převážně	převážně	k6eAd1	převážně
rozebírají	rozebírat	k5eAaImIp3nP	rozebírat
nihilistický	nihilistický	k2eAgInSc4d1	nihilistický
náhled	náhled	k1gInSc4	náhled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
společenskými	společenský	k2eAgInPc7d1	společenský
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
využívajíc	využívat	k5eAaImSgFnS	využívat
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
drsný	drsný	k2eAgInSc4d1	drsný
<g/>
,	,	kIx,	,
neslušný	slušný	k2eNgInSc4d1	neslušný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Charakterizovali	charakterizovat	k5eAaBmAgMnP	charakterizovat
jej	on	k3xPp3gMnSc4	on
slovními	slovní	k2eAgNnPc7d1	slovní
spojeními	spojení	k1gNnPc7	spojení
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zkažená	zkažený	k2eAgFnSc1d1	zkažená
městská	městský	k2eAgFnSc1d1	městská
muzika	muzika	k1gFnSc1	muzika
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
bělošský	bělošský	k2eAgMnSc1d1	bělošský
bratranec	bratranec	k1gMnSc1	bratranec
rapu	rapa	k1gFnSc4	rapa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Thrash	Thrash	k1gInSc4	Thrash
zpopularizovala	zpopularizovat	k5eAaPmAgFnS	zpopularizovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
thrashová	thrashový	k2eAgFnSc1d1	thrashová
čtyřka	čtyřka	k1gFnSc1	čtyřka
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Anthrax	Anthrax	k1gInSc1	Anthrax
<g/>
,	,	kIx,	,
Megadeth	Megadeth	k1gMnSc1	Megadeth
<g/>
,	,	kIx,	,
Metallica	Metallica	k1gMnSc1	Metallica
a	a	k8xC	a
Slayer	Slayer	k1gMnSc1	Slayer
<g/>
.	.	kIx.	.
</s>
<s>
Kreator	Kreator	k1gInSc1	Kreator
<g/>
,	,	kIx,	,
Sodom	Sodoma	k1gFnPc2	Sodoma
a	a	k8xC	a
Destruction	Destruction	k1gInSc1	Destruction
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
německé	německý	k2eAgFnPc1d1	německá
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tenhle	tenhle	k3xDgInSc4	tenhle
styl	styl	k1gInSc4	styl
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležité	důležitý	k2eAgFnPc1d1	důležitá
byly	být	k5eAaImAgFnP	být
také	také	k9	také
kapely	kapela	k1gFnPc1	kapela
Testament	testament	k1gInSc1	testament
ze	z	k7c2	z
San	San	k1gFnSc2	San
Franciska	Francisko	k1gNnSc2	Francisko
<g/>
,	,	kIx,	,
Exodus	Exodus	k1gInSc1	Exodus
z	z	k7c2	z
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
brazilská	brazilský	k2eAgFnSc1d1	brazilská
Sepultura	Sepultura	k1gFnSc1	Sepultura
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
thrash	thrash	k1gMnSc1	thrash
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
undergroundu	underground	k1gInSc2	underground
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
více-méně	víceéně	k6eAd1	více-méně
jen	jen	k9	jen
undergroundovým	undergroundový	k2eAgInSc7d1	undergroundový
fenoménem	fenomén	k1gInSc7	fenomén
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některým	některý	k3yIgFnPc3	některý
skupinám	skupina	k1gFnPc3	skupina
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
rovněž	rovněž	k9	rovněž
komerční	komerční	k2eAgNnSc1d1	komerční
publikum	publikum	k1gNnSc1	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Metallice	Metallice	k1gFnSc1	Metallice
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
mezi	mezi	k7c4	mezi
40	[number]	k4	40
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc4	billboard
s	s	k7c7	s
albem	album	k1gNnSc7	album
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppets	k1gInSc1	Puppets
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nato	nato	k6eAd1	nato
s	s	k7c7	s
albem	album	k1gNnSc7	album
...	...	k?	...
<g/>
And	Anda	k1gFnPc2	Anda
Justice	justice	k1gFnSc2	justice
for	forum	k1gNnPc2	forum
All	All	k1gMnPc2	All
dobyla	dobýt	k5eAaPmAgFnS	dobýt
šesté	šestý	k4xOgNnSc1	šestý
místo	místo	k1gNnSc1	místo
<g/>
;	;	kIx,	;
alba	alba	k1gFnSc1	alba
kapel	kapela	k1gFnPc2	kapela
Megadeth	Megadeth	k1gInSc4	Megadeth
a	a	k8xC	a
Anthrax	Anthrax	k1gInSc4	Anthrax
měli	mít	k5eAaImAgMnP	mít
rovněž	rovněž	k9	rovněž
alba	album	k1gNnSc2	album
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Slayer	Slayer	k1gInSc1	Slayer
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
nevydobyli	vydobýt	k5eNaPmAgMnP	vydobýt
takovou	takový	k3xDgFnSc4	takový
popularitu	popularita	k1gFnSc4	popularita
jako	jako	k8xC	jako
zbytek	zbytek	k1gInSc4	zbytek
Velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
,	,	kIx,	,
no	no	k9	no
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
vydali	vydat	k5eAaPmAgMnP	vydat
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
směrodajných	směrodajný	k2eAgNnPc2d1	směrodajný
alb	album	k1gNnPc2	album
žánru	žánr	k1gInSc2	žánr
<g/>
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
Kerrang	Kerranga	k1gFnPc2	Kerranga
<g/>
!	!	kIx.	!
</s>
<s>
označil	označit	k5eAaPmAgMnS	označit
album	album	k1gNnSc4	album
Reign	Reign	k1gMnSc1	Reign
in	in	k?	in
Blood	Blood	k1gInSc1	Blood
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejtvrdší	tvrdý	k2eAgNnSc4d3	nejtvrdší
album	album	k1gNnSc4	album
všech	všecek	k3xTgInPc2	všecek
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
nato	nato	k6eAd1	nato
jej	on	k3xPp3gNnSc4	on
zas	zas	k6eAd1	zas
magazín	magazín	k1gInSc1	magazín
Metal	metal	k1gInSc1	metal
Hammer	Hammer	k1gInSc1	Hammer
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Slayer	Slayer	k1gMnSc1	Slayer
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
škodu	škoda	k1gFnSc4	škoda
<g/>
,	,	kIx,	,
posluchačstvo	posluchačstvo	k1gNnSc4	posluchačstvo
z	z	k7c2	z
radů	rada	k1gMnPc2	rada
krajně	krajně	k6eAd1	krajně
pravicových	pravicový	k2eAgMnPc2d1	pravicový
skinheadů	skinhead	k1gMnPc2	skinhead
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
zabývá	zabývat	k5eAaImIp3nS	zabývat
násilím	násilí	k1gNnSc7	násilí
a	a	k8xC	a
nacizmem	nacizmus	k1gInSc7	nacizmus
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
thrash	thrash	k1gInSc1	thrash
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
nevídaný	vídaný	k2eNgInSc4d1	nevídaný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dal	dát	k5eAaPmAgMnS	dát
novou	nový	k2eAgFnSc4d1	nová
tvář	tvář	k1gFnSc4	tvář
komerčnímu	komerční	k2eAgInSc3d1	komerční
metalu	metal	k1gInSc3	metal
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Černé	Černé	k2eAgNnSc4d1	Černé
album	album	k1gNnSc4	album
<g/>
"	"	kIx"	"
od	od	k7c2	od
Metallicy	Metallica	k1gFnSc2	Metallica
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
<g/>
,	,	kIx,	,
Countdown	Countdown	k1gInSc1	Countdown
to	ten	k3xDgNnSc1	ten
Extinction	Extinction	k1gInSc1	Extinction
od	od	k7c2	od
Megadeth	Megadetha	k1gFnPc2	Megadetha
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
<g/>
,	,	kIx,	,
Anthrax	Anthrax	k1gInSc1	Anthrax
a	a	k8xC	a
Slayer	Slayer	k1gInSc1	Slayer
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
<g/>
,	,	kIx,	,
a	a	k8xC	a
alba	album	k1gNnPc1	album
lokálních	lokální	k2eAgFnPc2d1	lokální
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xS	jako
Testament	testament	k1gInSc1	testament
a	a	k8xC	a
Sepultura	Sepultura	k1gFnSc1	Sepultura
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
stovky	stovka	k1gFnSc2	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Death	Death	k1gMnSc1	Death
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
Thrash	Thrash	k1gMnSc1	Thrash
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
začal	začít	k5eAaPmAgInS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
a	a	k8xC	a
vyčlenily	vyčlenit	k5eAaPmAgInP	vyčlenit
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
extrémnější	extrémní	k2eAgInPc1d2	extrémnější
metalové	metalový	k2eAgInPc1d1	metalový
styly	styl	k1gInPc1	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
MTV	MTV	kA	MTV
News	News	k1gInSc1	News
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
muzika	muzika	k1gFnSc1	muzika
kapely	kapela	k1gFnSc2	kapela
Slayer	Slayra	k1gFnPc2	Slayra
byla	být	k5eAaImAgFnS	být
přímo	přímo	k6eAd1	přímo
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
za	za	k7c4	za
vzestup	vzestup	k1gInSc4	vzestup
death	death	k1gInSc1	death
metalu	metal	k1gInSc2	metal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Venom	Venom	k1gInSc1	Venom
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
NWOBHM	NWOBHM	kA	NWOBHM
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
taktéž	taktéž	k?	taktéž
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Rouhání	rouhání	k1gNnSc1	rouhání
se	se	k3xPyFc4	se
a	a	k8xC	a
satanizmus	satanizmus	k1gInSc1	satanizmus
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc1	prvek
uplatňované	uplatňovaný	k2eAgInPc1d1	uplatňovaný
u	u	k7c2	u
podobných	podobný	k2eAgFnPc2d1	podobná
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
našly	najít	k5eAaPmAgInP	najít
místo	místo	k1gNnSc4	místo
také	také	k9	také
v	v	k7c6	v
severoamerických	severoamerický	k2eAgFnPc6d1	severoamerická
i	i	k8xC	i
evropských	evropský	k2eAgFnPc6d1	Evropská
deathmetalových	deathmetalův	k2eAgFnPc6d1	deathmetalův
kapelách	kapela	k1gFnPc6	kapela
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvotním	prvotní	k2eAgFnPc3d1	prvotní
skupinám	skupina	k1gFnPc3	skupina
stylu	styl	k1gInSc2	styl
patří	patřit	k5eAaImIp3nP	patřit
floridští	floridský	k2eAgMnPc1d1	floridský
Death	Death	k1gMnSc1	Death
a	a	k8xC	a
kalifornští	kalifornský	k2eAgMnPc1d1	kalifornský
Possessed	Possessed	k1gInSc4	Possessed
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
kapel	kapela	k1gFnPc2	kapela
<g/>
;	;	kIx,	;
Possessed	Possessed	k1gMnSc1	Possessed
vydali	vydat	k5eAaPmAgMnP	vydat
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
demo	demo	k2eAgInSc1d1	demo
Death	Death	k1gInSc1	Death
Metal	metat	k5eAaImAgInS	metat
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
debutovém	debutový	k2eAgNnSc6d1	debutové
albu	album	k1gNnSc6	album
Seven	Seven	k2eAgInSc1d1	Seven
Churches	Churches	k1gInSc1	Churches
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
skladba	skladba	k1gFnSc1	skladba
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Death	Death	k1gInSc1	Death
Metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gInSc1	Death
metal	metal	k1gInSc1	metal
zužitkoval	zužitkovat	k5eAaPmAgInS	zužitkovat
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
agresivitu	agresivita	k1gFnSc4	agresivita
thrashe	thrash	k1gInSc2	thrash
i	i	k8xC	i
hardcoreu	hardcoreus	k1gInSc2	hardcoreus
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přidal	přidat	k5eAaPmAgMnS	přidat
texty	text	k1gInPc4	text
inspirované	inspirovaný	k2eAgInPc4d1	inspirovaný
nízkorozpočtovými	nízkorozpočtový	k2eAgInPc7d1	nízkorozpočtový
hororovými	hororový	k2eAgInPc7d1	hororový
filmy	film	k1gInPc7	film
"	"	kIx"	"
<g/>
zetkové	zetkový	k2eAgFnPc4d1	zetkový
<g/>
"	"	kIx"	"
kategorie	kategorie	k1gFnPc4	kategorie
a	a	k8xC	a
satanismem	satanismus	k1gInSc7	satanismus
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
ponurostí	ponurost	k1gFnSc7	ponurost
<g/>
,	,	kIx,	,
hrdelností	hrdelnost	k1gFnSc7	hrdelnost
a	a	k8xC	a
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
ječením	ječení	k1gNnSc7	ječení
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
smrtelným	smrtelný	k2eAgInSc7d1	smrtelný
skřípotem	skřípot	k1gInSc7	skřípot
<g/>
"	"	kIx"	"
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
nezvyklými	zvyklý	k2eNgFnPc7d1	nezvyklá
technikami	technika	k1gFnPc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Hluboký	hluboký	k2eAgInSc4d1	hluboký
<g/>
,	,	kIx,	,
agresivní	agresivní	k2eAgInSc4d1	agresivní
vokální	vokální	k2eAgInSc4d1	vokální
styl	styl	k1gInSc4	styl
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
podladěné	podladěný	k2eAgInPc1d1	podladěný
<g/>
,	,	kIx,	,
mimořádně	mimořádně	k6eAd1	mimořádně
zkreslené	zkreslený	k2eAgFnPc1d1	zkreslená
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
rychlé	rychlý	k2eAgInPc4d1	rychlý
bicí	bicí	k2eAgInPc4d1	bicí
(	(	kIx(	(
<g/>
obyčejně	obyčejně	k6eAd1	obyčejně
dvojpedálové	dvojpedálový	k2eAgNnSc1d1	dvojpedálový
<g/>
)	)	kIx)	)
a	a	k8xC	a
mohutná	mohutný	k2eAgFnSc1d1	mohutná
"	"	kIx"	"
<g/>
zvuková	zvukový	k2eAgFnSc1d1	zvuková
stěna	stěna	k1gFnSc1	stěna
<g/>
"	"	kIx"	"
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
z	z	k7c2	z
blast	blast	k1gInSc4	blast
beatů	beat	k1gInPc2	beat
<g/>
.	.	kIx.	.
</s>
<s>
Příznačné	příznačný	k2eAgInPc1d1	příznačný
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
frekventované	frekventovaný	k2eAgFnPc1d1	frekventovaná
změny	změna	k1gFnPc1	změna
tempa	tempo	k1gNnSc2	tempo
a	a	k8xC	a
taktu	takt	k1gInSc2	takt
<g/>
,	,	kIx,	,
a	a	k8xC	a
synkopa	synkopa	k1gFnSc1	synkopa
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gMnSc1	Death
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
thrash	thrash	k1gInSc1	thrash
<g/>
,	,	kIx,	,
odmítá	odmítat	k5eAaImIp3nS	odmítat
vyumělkovanost	vyumělkovanost	k1gFnSc1	vyumělkovanost
původních	původní	k2eAgInPc2d1	původní
metalových	metalový	k2eAgInPc2d1	metalový
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
potrpí	potrpět	k5eAaPmIp3nS	potrpět
na	na	k7c4	na
všední	všední	k2eAgInSc4d1	všední
styl	styl	k1gInSc4	styl
odívání	odívání	k1gNnSc2	odívání
<g/>
:	:	kIx,	:
roztrhané	roztrhaný	k2eAgFnPc1d1	roztrhaná
džíny	džíny	k1gFnPc1	džíny
a	a	k8xC	a
tuctové	tuctový	k2eAgFnPc1d1	tuctová
kožené	kožený	k2eAgFnPc1d1	kožená
bundy	bunda	k1gFnPc1	bunda
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
výjimek	výjimka	k1gFnPc2	výjimka
je	být	k5eAaImIp3nS	být
Glen	Glen	k1gMnSc1	Glen
Benton	Benton	k1gInSc1	Benton
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Deicide	Deicid	k1gInSc5	Deicid
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mívá	mívat	k5eAaImIp3nS	mívat
při	při	k7c6	při
vystoupeních	vystoupení	k1gNnPc6	vystoupení
obrácený	obrácený	k2eAgInSc4d1	obrácený
kříž	kříž	k1gInSc4	kříž
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
a	a	k8xC	a
nosívá	nosívat	k5eAaImIp3nS	nosívat
brnění	brnění	k1gNnSc1	brnění
<g/>
.	.	kIx.	.
</s>
<s>
Morbid	Morbid	k1gInSc1	Morbid
Angel	angel	k1gMnSc1	angel
si	se	k3xPyFc3	se
přisvojili	přisvojit	k5eAaPmAgMnP	přisvojit
neofašistickou	neofašistický	k2eAgFnSc4d1	neofašistická
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Death	Deatha	k1gFnPc2	Deatha
a	a	k8xC	a
Obituary	Obituara	k1gFnSc2	Obituara
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
skupiny	skupina	k1gFnPc4	skupina
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
deathmetalové	deathmetal	k1gMnPc1	deathmetal
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
povstala	povstat	k5eAaPmAgFnS	povstat
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
z	z	k7c2	z
anarcho-punkového	anarchounkový	k2eAgNnSc2d1	anarcho-punkový
hnutí	hnutí	k1gNnSc2	hnutí
vyčlenil	vyčlenit	k5eAaPmAgInS	vyčlenit
podobný	podobný	k2eAgInSc1d1	podobný
styl	styl	k1gInSc1	styl
zvaný	zvaný	k2eAgInSc1d1	zvaný
grindcore	grindcor	k1gInSc5	grindcor
<g/>
,	,	kIx,	,
zastupovaný	zastupovaný	k2eAgInSc1d1	zastupovaný
skupinami	skupina	k1gFnPc7	skupina
jako	jako	k9	jako
Napalm	napalm	k1gInSc4	napalm
Death	Deatha	k1gFnPc2	Deatha
a	a	k8xC	a
Extreme	Extrem	k1gInSc5	Extrem
Noise	Noise	k1gFnPc4	Noise
Terror	Terror	k1gInSc1	Terror
<g/>
.	.	kIx.	.
</s>
<s>
Počala	počnout	k5eAaPmAgFnS	počnout
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
deathmetalová	deathmetalový	k2eAgFnSc1d1	deathmetalová
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
reprezentovaná	reprezentovaný	k2eAgFnSc1d1	reprezentovaná
kapelami	kapela	k1gFnPc7	kapela
Entombed	Entombed	k1gMnSc1	Entombed
a	a	k8xC	a
Dismember	Dismember	k1gMnSc1	Dismember
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
toho	ten	k3xDgMnSc4	ten
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
melodický	melodický	k2eAgInSc1d1	melodický
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezi	mezi	k7c4	mezi
čelné	čelný	k2eAgMnPc4d1	čelný
představitele	představitel	k1gMnPc4	představitel
patří	patřit	k5eAaImIp3nP	patřit
Švédové	Švéd	k1gMnPc1	Švéd
In	In	k1gMnSc1	In
Flames	Flames	k1gMnSc1	Flames
a	a	k8xC	a
Dark	Dark	k1gMnSc1	Dark
Tranquillity	Tranquillita	k1gFnSc2	Tranquillita
a	a	k8xC	a
Finové	Fin	k1gMnPc1	Fin
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
a	a	k8xC	a
Kalmah	Kalmah	k1gInSc1	Kalmah
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Black	Black	k1gMnSc1	Black
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
black	black	k6eAd1	black
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgFnP	začít
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
hrát	hrát	k5eAaImF	hrát
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xS	jako
Venom	Venom	k1gInSc1	Venom
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mercyful	Mercyful	k1gInSc1	Mercyful
Fate	Fate	k1gFnSc1	Fate
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hellhammer	Hellhammero	k1gNnPc2	Hellhammero
a	a	k8xC	a
Celtic	Celtice	k1gFnPc2	Celtice
Frost	Frost	k1gFnSc1	Frost
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bathory	Bathor	k1gInPc1	Bathor
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
stály	stát	k5eAaImAgFnP	stát
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
druhé	druhý	k4xOgFnSc2	druhý
vlny	vlna	k1gFnSc2	vlna
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
Mayhem	Mayh	k1gInSc7	Mayh
<g/>
,	,	kIx,	,
Burzum	Burzum	k1gInSc1	Burzum
<g/>
,	,	kIx,	,
Darkthrone	Darkthron	k1gInSc5	Darkthron
a	a	k8xC	a
Emperor	Emperora	k1gFnPc2	Emperora
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
black	black	k1gInSc4	black
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
specifická	specifický	k2eAgFnSc1d1	specifická
různorodost	různorodost	k1gFnSc1	různorodost
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
kvality	kvalita	k1gFnSc2	kvalita
provedení	provedení	k1gNnSc2	provedení
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
má	mít	k5eAaImIp3nS	mít
většina	většina	k1gFnSc1	většina
skupin	skupina	k1gFnPc2	skupina
několik	několik	k4yIc4	několik
společných	společný	k2eAgInPc2d1	společný
rysů	rys	k1gInPc2	rys
<g/>
:	:	kIx,	:
vřeštivé	vřeštivý	k2eAgInPc4d1	vřeštivý
a	a	k8xC	a
vrčivé	vrčivý	k2eAgInPc4d1	vrčivý
vokály	vokál	k1gInPc4	vokál
zvané	zvaný	k2eAgInPc4d1	zvaný
screaming	screaming	k1gInSc4	screaming
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
zkreslené	zkreslený	k2eAgFnPc4d1	zkreslená
kytary	kytara	k1gFnPc4	kytara
využívající	využívající	k2eAgNnSc4d1	využívající
tremolování	tremolování	k1gNnSc4	tremolování
(	(	kIx(	(
<g/>
tremolo	tremolo	k6eAd1	tremolo
picking	picking	k1gInSc1	picking
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
temnou	temný	k2eAgFnSc4d1	temná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
"	"	kIx"	"
a	a	k8xC	a
záměrně	záměrně	k6eAd1	záměrně
nekvalitní	kvalitní	k2eNgInSc4d1	nekvalitní
zvuk	zvuk	k1gInSc4	zvuk
s	s	k7c7	s
okolními	okolní	k2eAgInPc7d1	okolní
zvuky	zvuk	k1gInPc7	zvuk
a	a	k8xC	a
šumy	šum	k1gInPc7	šum
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tematika	tematika	k1gFnSc1	tematika
satanizmu	satanizmus	k1gInSc2	satanizmus
má	mít	k5eAaImIp3nS	mít
v	v	k7c4	v
black	black	k1gInSc4	black
metalu	metal	k1gInSc2	metal
největší	veliký	k2eAgNnSc4d3	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
kapely	kapela	k1gFnPc1	kapela
se	se	k3xPyFc4	se
nechaly	nechat	k5eAaPmAgFnP	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
rovněž	rovněž	k9	rovněž
starověkým	starověký	k2eAgNnSc7d1	starověké
pohanstvím	pohanství	k1gNnSc7	pohanství
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
podporovaly	podporovat	k5eAaImAgInP	podporovat
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
předkřesťanským	předkřesťanský	k2eAgFnPc3d1	předkřesťanská
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
blackmetalové	blackmetal	k1gMnPc1	blackmetal
skupiny	skupina	k1gFnSc2	skupina
rovněž	rovněž	k9	rovněž
"	"	kIx"	"
<g/>
experimentují	experimentovat	k5eAaImIp3nP	experimentovat
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
všech	všecek	k3xTgFnPc2	všecek
myslitelných	myslitelný	k2eAgFnPc2d1	myslitelná
forem	forma	k1gFnPc2	forma
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
folku	folk	k1gInSc2	folk
<g/>
,	,	kIx,	,
elektroniky	elektronika	k1gFnSc2	elektronika
a	a	k8xC	a
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bubeník	Bubeník	k1gMnSc1	Bubeník
Fenriz	Fenriz	k1gMnSc1	Fenriz
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Darkthrone	Darkthron	k1gInSc5	Darkthron
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
o	o	k7c6	o
produkci	produkce	k1gFnSc6	produkce
<g/>
,	,	kIx,	,
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
oblékají	oblékat	k5eAaImIp3nP	oblékat
a	a	k8xC	a
o	o	k7c6	o
závazku	závazek	k1gInSc6	závazek
udělat	udělat	k5eAaPmF	udělat
odporný	odporný	k2eAgInSc1d1	odporný
<g/>
,	,	kIx,	,
surový	surový	k2eAgInSc1d1	surový
<g/>
,	,	kIx,	,
ponurý	ponurý	k2eAgInSc1d1	ponurý
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Nespájel	spájet	k5eNaImAgMnS	spájet
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc4d1	společný
sound	sound	k1gInSc4	sound
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Především	především	k9	především
mezi	mezi	k7c7	mezi
skandinávskými	skandinávský	k2eAgFnPc7d1	skandinávská
skupinami	skupina	k1gFnPc7	skupina
jako	jako	k8xC	jako
Mayhem	Mayh	k1gInSc7	Mayh
či	či	k8xC	či
Immortal	Immortal	k1gFnSc7	Immortal
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
populární	populární	k2eAgInSc4d1	populární
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
corpse	corps	k1gInSc5	corps
paint	painto	k1gNnPc2	painto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
mrtvolní	mrtvolní	k2eAgNnSc1d1	mrtvolní
líčení	líčení	k1gNnSc1	líčení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
líčily	líčit	k5eAaImAgFnP	líčit
jako	jako	k9	jako
mrtvoly	mrtvola	k1gFnPc1	mrtvola
<g/>
,	,	kIx,	,
lebky	lebka	k1gFnPc1	lebka
nebo	nebo	k8xC	nebo
duchové	duch	k1gMnPc1	duch
<g/>
.	.	kIx.	.
</s>
<s>
Bathory	Bathor	k1gInPc4	Bathor
položili	položit	k5eAaPmAgMnP	položit
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
viking	viking	k1gMnSc1	viking
a	a	k8xC	a
folk	folk	k1gInSc1	folk
metal	metat	k5eAaImAgInS	metat
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
Immortal	Immortal	k1gFnSc3	Immortal
si	se	k3xPyFc3	se
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
získaly	získat	k5eAaPmAgInP	získat
blast	blast	k5eAaPmF	blast
beaty	beat	k1gInPc1	beat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
měli	mít	k5eAaImAgMnP	mít
blackmetaloví	blackmetalový	k2eAgMnPc1d1	blackmetalový
vandalové	vandal	k1gMnPc1	vandal
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
značné	značný	k2eAgFnSc2d1	značná
násilnosti	násilnost	k1gFnSc2	násilnost
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupin	skupina	k1gFnPc2	skupina
Mayhem	Mayh	k1gInSc7	Mayh
a	a	k8xC	a
Burzum	Burzum	k1gNnSc4	Burzum
byli	být	k5eAaImAgMnP	být
zakladateli	zakladatel	k1gMnPc7	zakladatel
spolku	spolek	k1gInSc2	spolek
Inner	Inner	k1gMnSc1	Inner
Circle	Circl	k1gMnSc2	Circl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
vypálení	vypálení	k1gNnSc4	vypálení
množství	množství	k1gNnSc2	množství
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
norských	norský	k2eAgInPc2d1	norský
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Narůstající	narůstající	k2eAgInSc1d1	narůstající
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
death	death	k1gInSc4	death
metal	metal	k1gInSc1	metal
způsobil	způsobit	k5eAaPmAgInS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
black	black	k1gInSc1	black
metal	metal	k1gInSc1	metal
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgInS	začít
získávat	získávat	k5eAaImF	získávat
podporu	podpora	k1gFnSc4	podpora
skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
metalového	metalový	k2eAgInSc2d1	metalový
undergroundu	underground	k1gInSc2	underground
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
odmítavé	odmítavý	k2eAgInPc4d1	odmítavý
postoje	postoj	k1gInPc4	postoj
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
samotných	samotný	k2eAgMnPc2d1	samotný
blackmetalistů	blackmetalista	k1gMnPc2	blackmetalista
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
skupiny	skupina	k1gFnSc2	skupina
Gorgoroth	Gorgoroth	k1gMnSc1	Gorgoroth
Gaahl	Gaahl	k1gMnSc1	Gaahl
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
black	black	k1gInSc1	black
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepokoušel	pokoušet	k5eNaImAgInS	pokoušet
získat	získat	k5eAaPmF	získat
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
...	...	k?	...
Měli	mít	k5eAaImAgMnP	mít
jsme	být	k5eAaImIp1nP	být
společného	společný	k2eAgMnSc4d1	společný
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
cokoliv	cokoliv	k3yInSc4	cokoliv
společné	společný	k2eAgFnPc4d1	společná
s	s	k7c7	s
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začínaly	začínat	k5eAaImAgInP	začínat
vytvářet	vytvářet	k5eAaImF	vytvářet
blackmetalové	blackmetal	k1gMnPc1	blackmetal
scény	scéna	k1gFnSc2	scéna
i	i	k9	i
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
mimo	mimo	k7c4	mimo
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
nebo	nebo	k8xC	nebo
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Násilí	násilí	k1gNnSc1	násilí
této	tento	k3xDgFnSc2	tento
scény	scéna	k1gFnSc2	scéna
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
základy	základ	k1gInPc4	základ
brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
když	když	k8xS	když
zakladatele	zakladatel	k1gMnSc2	zakladatel
skupiny	skupina	k1gFnSc2	skupina
Mayhem	Mayh	k1gInSc7	Mayh
Euronymouse	Euronymouse	k1gFnSc2	Euronymouse
ubodal	ubodat	k5eAaPmAgInS	ubodat
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
Varg	Varg	k1gMnSc1	Varg
Vikernes	Vikernes	k1gMnSc1	Vikernes
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Burzum	Burzum	k1gInSc1	Burzum
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Mayhem	Mayh	k1gInSc7	Mayh
však	však	k9	však
nahrával	nahrávat	k5eAaImAgInS	nahrávat
basovou	basový	k2eAgFnSc4d1	basová
linku	linka	k1gFnSc4	linka
pro	pro	k7c4	pro
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
případ	případ	k1gInSc4	případ
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
ze	z	k7c2	z
scény	scéna	k1gFnSc2	scéna
začali	začít	k5eAaPmAgMnP	začít
pociťovat	pociťovat	k5eAaImF	pociťovat
stagnaci	stagnace	k1gFnSc4	stagnace
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
prominentní	prominentní	k2eAgFnPc1d1	prominentní
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xC	jako
Burzum	Burzum	k1gNnSc1	Burzum
či	či	k8xC	či
Beherit	Beherit	k1gInSc1	Beherit
začaly	začít	k5eAaPmAgInP	začít
začleňovat	začleňovat	k5eAaImF	začleňovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
prvky	prvek	k1gInPc4	prvek
ambientu	ambient	k1gInSc2	ambient
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
symfonického	symfonický	k2eAgInSc2d1	symfonický
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
Tiamat	Tiamat	k1gInSc1	Tiamat
<g/>
,	,	kIx,	,
Samael	Samael	k1gInSc1	Samael
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Norská	norský	k2eAgFnSc1d1	norská
skupina	skupina	k1gFnSc1	skupina
Dimmu	Dimm	k1gInSc2	Dimm
Borgir	Borgira	k1gFnPc2	Borgira
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
koncem	koncem	k7c2	koncem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
black	black	k1gInSc1	black
metal	metat	k5eAaImAgInS	metat
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
střednímu	střední	k2eAgInSc3d1	střední
proudu	proud	k1gInSc3	proud
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
zpopularizovaní	zpopularizovaný	k2eAgMnPc1d1	zpopularizovaný
žánru	žánr	k1gInSc3	žánr
mají	mít	k5eAaImIp3nP	mít
rovněž	rovněž	k9	rovněž
Angličané	Angličan	k1gMnPc1	Angličan
Cradle	Cradle	k1gMnSc1	Cradle
of	of	k?	of
Filth	Filth	k1gMnSc1	Filth
<g/>
,	,	kIx,	,
časopisem	časopis	k1gInSc7	časopis
Metal	metal	k1gInSc1	metal
Hammer	Hammero	k1gNnPc2	Hammero
prohlášeni	prohlásit	k5eAaPmNgMnP	prohlásit
za	za	k7c4	za
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
metalovou	metalový	k2eAgFnSc4d1	metalová
kapelu	kapela	k1gFnSc4	kapela
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
miláčky	miláček	k1gMnPc4	miláček
kritiky	kritika	k1gFnSc2	kritika
švédští	švédský	k2eAgMnPc1d1	švédský
tradicionalisti	tradicionalisti	k?	tradicionalisti
Watain	Wataina	k1gFnPc2	Wataina
<g/>
,	,	kIx,	,
francouzští	francouzský	k2eAgMnPc1d1	francouzský
experimentátoři	experimentátor	k1gMnPc1	experimentátor
Deathspell	Deathspella	k1gFnPc2	Deathspella
Omega	omega	k1gNnSc2	omega
<g/>
,	,	kIx,	,
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
jednočlenná	jednočlenný	k2eAgFnSc1d1	jednočlenná
kapela	kapela	k1gFnSc1	kapela
Xasthur	Xasthura	k1gFnPc2	Xasthura
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Power	Power	k1gMnSc1	Power
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
Powermetalová	Powermetalový	k2eAgFnSc1d1	Powermetalový
scéna	scéna	k1gFnSc1	scéna
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
příkrosti	příkrost	k1gFnSc3	příkrost
death	death	k1gMnSc1	death
a	a	k8xC	a
black	black	k1gMnSc1	black
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
power	power	k1gMnSc1	power
metal	metal	k1gInSc4	metal
držel	držet	k5eAaImAgMnS	držet
víceméně	víceméně	k9	víceméně
v	v	k7c6	v
undergroundu	underground	k1gInSc6	underground
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
obrovské	obrovský	k2eAgFnSc3d1	obrovská
oblibě	obliba	k1gFnSc3	obliba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
power	power	k1gInSc4	power
metalu	metal	k1gInSc2	metal
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
vzletné	vzletný	k2eAgInPc1d1	vzletný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
epická	epický	k2eAgFnSc1d1	epická
melodika	melodika	k1gFnSc1	melodika
a	a	k8xC	a
tematika	tematika	k1gFnSc1	tematika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
"	"	kIx"	"
<g/>
apelují	apelovat	k5eAaImIp3nP	apelovat
na	na	k7c4	na
posluchačův	posluchačův	k2eAgInSc4d1	posluchačův
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
udatnost	udatnost	k1gFnSc4	udatnost
a	a	k8xC	a
krásu	krása	k1gFnSc4	krása
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Základy	základ	k1gInPc4	základ
tohoto	tento	k3xDgInSc2	tento
soundu	sound	k1gInSc2	sound
položila	položit	k5eAaPmAgFnS	položit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
německá	německý	k2eAgFnSc1d1	německá
skupina	skupina	k1gFnSc1	skupina
Helloween	Hellowena	k1gFnPc2	Hellowena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
skloubila	skloubit	k5eAaPmAgFnS	skloubit
power	power	k1gInSc4	power
<g />
.	.	kIx.	.
</s>
<s>
riffy	riff	k1gInPc4	riff
<g/>
,	,	kIx,	,
melodiku	melodika	k1gFnSc4	melodika
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
"	"	kIx"	"
zpěv	zpěv	k1gInSc1	zpěv
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xS	jako
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
a	a	k8xC	a
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
,	,	kIx,	,
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
energií	energie	k1gFnSc7	energie
thrashe	thrashat	k5eAaPmIp3nS	thrashat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vytvářeje	vytvářet	k5eAaImSgInS	vytvářet
tak	tak	k9	tak
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
dnes	dnes	k6eAd1	dnes
poznáme	poznat	k5eAaPmIp1nP	poznat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
power	power	k1gInSc1	power
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
USA	USA	kA	USA
patřily	patřit	k5eAaImAgFnP	patřit
mezi	mezi	k7c4	mezi
průkopníky	průkopník	k1gMnPc4	průkopník
stylu	styl	k1gInSc2	styl
newyorské	newyorský	k2eAgFnSc2d1	newyorská
skupiny	skupina	k1gFnSc2	skupina
Manowar	Manowar	k1gMnSc1	Manowar
a	a	k8xC	a
Virgin	Virgin	k1gMnSc1	Virgin
Steele	Steel	k1gInSc2	Steel
<g/>
.	.	kIx.	.
</s>
<s>
Fošna	fošna	k1gFnSc1	fošna
Rising	Rising	k1gInSc1	Rising
Force	force	k1gFnSc4	force
od	od	k7c2	od
Yngwieho	Yngwie	k1gMnSc2	Yngwie
Malmsteena	Malmsteen	k1gMnSc2	Malmsteen
měla	mít	k5eAaImAgFnS	mít
nemalou	malý	k2eNgFnSc4d1	nemalá
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
zpopularizování	zpopularizování	k1gNnPc4	zpopularizování
pekelně	pekelně	k6eAd1	pekelně
rychlého	rychlý	k2eAgInSc2d1	rychlý
kytarového	kytarový	k2eAgInSc2d1	kytarový
stylu	styl	k1gInSc2	styl
známého	známý	k2eAgInSc2d1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
shredding	shredding	k1gInSc1	shredding
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
přičiněním	přičinění	k1gNnSc7	přičinění
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
ujal	ujmout	k5eAaPmAgInS	ujmout
metal	metal	k1gInSc1	metal
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
power	power	k1gInSc4	power
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Sound	Sound	k1gInSc1	Sound
tradičnějších	tradiční	k2eAgMnPc2d2	tradičnější
powermetalistů	powermetalista	k1gMnPc2	powermetalista
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
HammerFall	HammerFall	k1gInSc4	HammerFall
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
DragonForce	DragonForka	k1gFnSc6	DragonForka
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Iced	Iced	k1gMnSc1	Iced
Earth	Earth	k1gMnSc1	Earth
z	z	k7c2	z
Floridy	Florida	k1gFnSc2	Florida
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spjatý	spjatý	k2eAgInSc1d1	spjatý
s	s	k7c7	s
klasickým	klasický	k2eAgInSc7d1	klasický
stylem	styl	k1gInSc7	styl
NWOBHM	NWOBHM	kA	NWOBHM
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
powermetalové	powermetal	k1gMnPc1	powermetal
skupiny	skupina	k1gFnSc2	skupina
používají	používat	k5eAaImIp3nP	používat
klávesisty	klávesista	k1gMnPc4	klávesista
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
se	s	k7c7	s
symfonickými	symfonický	k2eAgInPc7d1	symfonický
orchestry	orchestr	k1gInPc7	orchestr
či	či	k8xC	či
opěrnými	opěrný	k2eAgMnPc7d1	opěrný
zpěváky	zpěvák	k1gMnPc7	zpěvák
a	a	k8xC	a
zpěvačkami	zpěvačka	k1gFnPc7	zpěvačka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dodávají	dodávat	k5eAaImIp3nP	dodávat
této	tento	k3xDgFnSc3	tento
hudbě	hudba	k1gFnSc3	hudba
"	"	kIx"	"
<g/>
symfonický	symfonický	k2eAgInSc4d1	symfonický
<g/>
"	"	kIx"	"
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takovým	takový	k3xDgFnPc3	takový
kapelám	kapela	k1gFnPc3	kapela
patří	patřit	k5eAaImIp3nS	patřit
Kamelot	kamelot	k1gMnSc1	kamelot
z	z	k7c2	z
Floridy	Florida	k1gFnSc2	Florida
<g/>
,	,	kIx,	,
Nightwish	Nightwish	k1gMnSc1	Nightwish
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
of	of	k?	of
Fire	Fir	k1gFnSc2	Fir
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Stratovarius	Stratovarius	k1gMnSc1	Stratovarius
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
Catharsis	Catharsis	k1gFnSc2	Catharsis
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Power	Power	k1gInSc1	Power
metal	metal	k1gInSc1	metal
má	mít	k5eAaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
diváckou	divácký	k2eAgFnSc4d1	divácká
základnu	základna	k1gFnSc4	základna
jednak	jednak	k8xC	jednak
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
popularitě	popularita	k1gFnSc3	popularita
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
Angra	Angro	k1gNnSc2	Angro
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
či	či	k8xC	či
Rata	Ratus	k1gMnSc2	Ratus
Blanca	Blanc	k1gMnSc2	Blanc
z	z	k7c2	z
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Blízkým	blízký	k2eAgMnPc3d1	blízký
příbuzným	příbuzný	k1gMnPc3	příbuzný
power	power	k1gInSc4	power
metalu	metal	k1gInSc2	metal
je	být	k5eAaImIp3nS	být
progresivní	progresivní	k2eAgInSc1d1	progresivní
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
osvojil	osvojit	k5eAaPmAgInS	osvojit
komplexní	komplexní	k2eAgInSc4d1	komplexní
kompoziční	kompoziční	k2eAgInSc4d1	kompoziční
přístup	přístup	k1gInSc4	přístup
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
Rush	Rush	k1gMnSc1	Rush
nebo	nebo	k8xC	nebo
King	King	k1gMnSc1	King
Crimson	Crimson	k1gMnSc1	Crimson
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníky	průkopník	k1gMnPc4	průkopník
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
jsou	být	k5eAaImIp3nP	být
americké	americký	k2eAgFnPc1d1	americká
kapely	kapela	k1gFnPc1	kapela
Queensrÿ	Queensrÿ	k1gMnSc1	Queensrÿ
<g/>
,	,	kIx,	,
Fates	Fates	k1gMnSc1	Fates
Warning	Warning	k1gInSc1	Warning
a	a	k8xC	a
Dream	Dream	k1gInSc1	Dream
Theater	Theatra	k1gFnPc2	Theatra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
představitelům	představitel	k1gMnPc3	představitel
fúze	fúze	k1gFnSc1	fúze
power	power	k1gMnSc1	power
a	a	k8xC	a
prog-metalu	progetal	k1gMnSc3	prog-metal
patří	patřit	k5eAaImIp3nP	patřit
Symphony	Symphona	k1gFnPc1	Symphona
X	X	kA	X
z	z	k7c2	z
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
jejíchž	jejíž	k3xOyRp3gFnPc2	jejíž
kytarista	kytarista	k1gMnSc1	kytarista
Michael	Michael	k1gMnSc1	Michael
Romeo	Romeo	k1gMnSc1	Romeo
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
shred	shred	k1gInSc4	shred
kytaristů	kytarista	k1gMnPc2	kytarista
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Doom	Dooma	k1gFnPc2	Dooma
metal	metat	k5eAaImAgMnS	metat
a	a	k8xC	a
Gothic	Gothic	k1gMnSc1	Gothic
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgInSc1d1	pohybující
protiklad	protiklad	k1gInSc1	protiklad
typického	typický	k2eAgNnSc2d1	typické
heavy	heava	k1gFnPc1	heava
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zrodila	zrodit	k5eAaPmAgFnS	zrodit
nová	nový	k2eAgFnSc1d1	nová
metalová	metalový	k2eAgFnSc1d1	metalová
odnož	odnož	k1gFnSc1	odnož
–	–	k?	–
doom	doom	k6eAd1	doom
metal	metat	k5eAaImAgInS	metat
–	–	k?	–
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
naživu	naživu	k6eAd1	naživu
kapelami	kapela	k1gFnPc7	kapela
jako	jako	k8xC	jako
Saint	Saint	k1gMnSc1	Saint
Vitus	Vitus	k1gMnSc1	Vitus
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Obsessed	Obsessed	k1gMnSc1	Obsessed
<g/>
,	,	kIx,	,
Trouble	Trouble	k1gMnSc1	Trouble
či	či	k8xC	či
Candlemass	Candlemass	k1gInSc1	Candlemass
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
i	i	k8xC	i
textové	textový	k2eAgFnSc6d1	textová
stránce	stránka	k1gFnSc6	stránka
vychází	vycházet	k5eAaImIp3nS	vycházet
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
rané	raný	k2eAgFnSc2d1	raná
tvorby	tvorba	k1gFnSc2	tvorba
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
současníků	současník	k1gMnPc2	současník
Blue	Blu	k1gFnSc2	Blu
Cheer	Cheer	k1gInSc1	Cheer
<g/>
,	,	kIx,	,
Pentagram	pentagram	k1gInSc1	pentagram
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Widow	Widow	k1gMnSc1	Widow
<g/>
.	.	kIx.	.
</s>
<s>
Doom	Doom	k6eAd1	Doom
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
odnože	odnož	k1gFnSc2	odnož
pomohla	pomoct	k5eAaPmAgFnS	pomoct
definovat	definovat	k5eAaBmF	definovat
rovněž	rovněž	k9	rovněž
skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnSc2	The
Melvins	Melvinsa	k1gFnPc2	Melvinsa
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
heavymetalových	heavymetalový	k2eAgInPc2d1	heavymetalový
stylů	styl	k1gInPc2	styl
preferuje	preferovat	k5eAaImIp3nS	preferovat
rychlejší	rychlý	k2eAgInSc1d2	rychlejší
tempa	tempo	k1gNnSc2	tempo
a	a	k8xC	a
technickou	technický	k2eAgFnSc4d1	technická
dokonalost	dokonalost	k1gFnSc4	dokonalost
<g/>
,	,	kIx,	,
doom	doom	k1gInSc1	doom
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
pomalejším	pomalý	k2eAgNnSc7d2	pomalejší
tempem	tempo	k1gNnSc7	tempo
<g/>
,	,	kIx,	,
hutnými	hutný	k2eAgInPc7d1	hutný
tóny	tón	k1gInPc7	tón
<g/>
,	,	kIx,	,
ponurostí	ponurost	k1gFnSc7	ponurost
<g/>
,	,	kIx,	,
melancholií	melancholie	k1gFnSc7	melancholie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
atmosféričností	atmosféričnost	k1gFnSc7	atmosféričnost
<g/>
,	,	kIx,	,
využívanou	využívaný	k2eAgFnSc7d1	využívaná
však	však	k9	však
i	i	k8xC	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
metalových	metalový	k2eAgFnPc6d1	metalová
odnožích	odnož	k1gFnPc6	odnož
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
anglické	anglický	k2eAgFnSc2d1	anglická
skupiny	skupina	k1gFnSc2	skupina
Cathedral	Cathedral	k1gFnSc2	Cathedral
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Forest	Forest	k1gInSc4	Forest
of	of	k?	of
Equilibrium	Equilibrium	k1gNnSc4	Equilibrium
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
vyvolat	vyvolat	k5eAaPmF	vyvolat
novou	nova	k1gFnSc7	nova
doommetalovou	doommetalův	k2eAgFnSc7d1	doommetalův
vlnu	vlna	k1gFnSc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
Paradise	Paradise	k1gFnSc2	Paradise
Lost	Losta	k1gFnPc2	Losta
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
Dying	Dying	k1gInSc1	Dying
Bride	Brid	k1gInSc5	Brid
<g/>
,	,	kIx,	,
Anathema	anathema	k1gNnSc1	anathema
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
směs	směs	k1gFnSc1	směs
doom	doom	k6eAd1	doom
a	a	k8xC	a
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
mají	mít	k5eAaImIp3nP	mít
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
evropského	evropský	k2eAgInSc2d1	evropský
gothic	gothice	k1gFnPc2	gothice
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
zastupovaného	zastupovaný	k2eAgInSc2d1	zastupovaný
např.	např.	kA	např.
norskými	norský	k2eAgFnPc7d1	norská
kapelami	kapela	k1gFnPc7	kapela
Theatre	Theatr	k1gInSc5	Theatr
of	of	k?	of
Tragedy	Trageda	k1gMnSc2	Trageda
a	a	k8xC	a
Tristania	Tristanium	k1gNnPc1	Tristanium
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgInSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
vícero	vícero	k1gNnSc4	vícero
zpěváků	zpěvák	k1gMnPc2	zpěvák
či	či	k8xC	či
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
uvedli	uvést	k5eAaPmAgMnP	uvést
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
Newyorčané	Newyorčan	k1gMnPc1	Newyorčan
Type	typ	k1gInSc5	typ
O	o	k7c6	o
Negative	negativ	k1gInSc5	negativ
<g/>
.	.	kIx.	.
</s>
<s>
Gothicmetalové	Gothicmetal	k1gMnPc1	Gothicmetal
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
především	především	k9	především
Therion	Therion	k1gInSc1	Therion
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
přimíchávali	přimíchávat	k5eAaImAgMnP	přimíchávat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
orchestrální	orchestrální	k2eAgInPc1d1	orchestrální
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
podhoubí	podhoubí	k1gNnSc1	podhoubí
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterého	který	k3yRgMnSc4	který
vzešli	vzejít	k5eAaPmAgMnP	vzejít
symfometalové	symfometal	k1gMnPc1	symfometal
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
Virgin	Virgin	k1gMnSc1	Virgin
Black	Black	k1gMnSc1	Black
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Nightwish	Nightwish	k1gMnSc1	Nightwish
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
či	či	k8xC	či
Within	Within	k2eAgInSc1d1	Within
Temptation	Temptation	k1gInSc1	Temptation
a	a	k8xC	a
After	After	k1gMnSc1	After
Forever	Forever	k1gMnSc1	Forever
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
metalové	metalový	k2eAgFnSc6d1	metalová
scéně	scéna	k1gFnSc6	scéna
objevila	objevit	k5eAaPmAgFnS	objevit
nová	nový	k2eAgFnSc1d1	nová
odnož	odnož	k1gFnSc1	odnož
jménem	jméno	k1gNnSc7	jméno
sludge	sludge	k1gNnSc2	sludge
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mísila	mísit	k5eAaImAgFnS	mísit
dohromady	dohromady	k6eAd1	dohromady
hardcore	hardcor	k1gInSc5	hardcor
a	a	k8xC	a
doom	doomo	k1gNnPc2	doomo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc4	Orleans
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
patřily	patřit	k5eAaImAgFnP	patřit
mezi	mezi	k7c7	mezi
vedoucí	vedoucí	k1gFnSc7	vedoucí
kapely	kapela	k1gFnSc2	kapela
Eyehategod	Eyehategoda	k1gFnPc2	Eyehategoda
a	a	k8xC	a
Crowbar	Crowbara	k1gFnPc2	Crowbara
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc6	počátek
následujícího	následující	k2eAgNnSc2d1	následující
desetiletí	desetiletí	k1gNnSc2	desetiletí
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
kalifornské	kalifornský	k2eAgFnPc1d1	kalifornská
kapely	kapela	k1gFnPc1	kapela
Kyuss	Kyussa	k1gFnPc2	Kyussa
a	a	k8xC	a
Sleep	Sleep	k1gInSc1	Sleep
<g/>
,	,	kIx,	,
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
staršími	starý	k2eAgFnPc7d2	starší
doommetalovými	doommetalův	k2eAgFnPc7d1	doommetalův
kapelami	kapela	k1gFnPc7	kapela
<g/>
,	,	kIx,	,
vlnu	vlna	k1gFnSc4	vlna
stoner	stoner	k1gInSc4	stoner
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Seattli	Seattle	k1gFnSc6	Seattle
skupina	skupina	k1gFnSc1	skupina
Earth	Earth	k1gInSc4	Earth
pomohla	pomoct	k5eAaPmAgFnS	pomoct
vytvořit	vytvořit	k5eAaPmF	vytvořit
další	další	k2eAgFnSc4d1	další
doomovou	doomový	k2eAgFnSc4d1	doomový
odnož	odnož	k1gFnSc4	odnož
zvanou	zvaný	k2eAgFnSc4d1	zvaná
drone	dronout	k5eAaPmIp3nS	dronout
metal	metal	k1gInSc4	metal
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
nesl	nést	k5eAaImAgInS	nést
v	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
nových	nový	k2eAgFnPc2d1	nová
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Goatsnake	Goatsnak	k1gInPc1	Goatsnak
(	(	kIx(	(
<g/>
klasický	klasický	k2eAgInSc1d1	klasický
stoner	stoner	k1gInSc1	stoner
<g/>
/	/	kIx~	/
<g/>
doom	doom	k1gInSc1	doom
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sunn	sunna	k1gFnPc2	sunna
O	o	k7c6	o
<g/>
)))	)))	k?	)))
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
doom	doom	k1gInSc1	doom
<g/>
,	,	kIx,	,
drone	dronout	k5eAaPmIp3nS	dronout
a	a	k8xC	a
dark	dark	k6eAd1	dark
ambient	ambient	k1gInSc1	ambient
metalu	metal	k1gInSc2	metal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
jejich	jejich	k3xOp3gFnSc4	jejich
tvorbu	tvorba	k1gFnSc4	tvorba
k	k	k7c3	k
"	"	kIx"	"
<g/>
indickému	indický	k2eAgInSc3d1	indický
raga	rag	k2eAgNnPc1d1	rag
v	v	k7c6	v
ohnisku	ohnisko	k1gNnSc6	ohnisko
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Alternativní	alternativní	k2eAgInSc4d1	alternativní
metal	metal	k1gInSc4	metal
a	a	k8xC	a
Nu	nu	k9	nu
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začaly	začít	k5eAaPmAgFnP	začít
grungové	grungový	k2eAgFnPc1d1	grungový
kapely	kapela	k1gFnPc1	kapela
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Nirvanou	Nirvaný	k2eAgFnSc7d1	Nirvaný
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
komerční	komerční	k2eAgInSc4d1	komerční
metal	metal	k1gInSc4	metal
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
nástup	nástup	k1gInSc4	nástup
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Grunge	Grunge	k1gInSc1	Grunge
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
ovlivněný	ovlivněný	k2eAgInSc1d1	ovlivněný
heavymetalovým	heavymetalový	k2eAgInSc7d1	heavymetalový
soundem	sound	k1gInSc7	sound
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
odmítal	odmítat	k5eAaImAgInS	odmítat
výstřelky	výstřelek	k1gInPc4	výstřelek
komerčních	komerční	k2eAgMnPc2d1	komerční
metalistů	metalista	k1gMnPc2	metalista
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
"	"	kIx"	"
<g/>
okázalá	okázalý	k2eAgNnPc1d1	okázalé
virtuózní	virtuózní	k2eAgNnPc1d1	virtuózní
sóla	sólo	k1gNnPc1	sólo
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
dychtivost	dychtivost	k1gFnSc1	dychtivost
po	po	k7c4	po
účinkování	účinkování	k1gNnSc4	účinkování
na	na	k7c6	na
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
</s>
<s>
Obliba	obliba	k1gFnSc1	obliba
glam	gla	k1gNnSc7	gla
metalu	metal	k1gInSc2	metal
klesala	klesat	k5eAaImAgFnS	klesat
nejenom	nejenom	k6eAd1	nejenom
následkem	následkem	k7c2	následkem
úspěchu	úspěch	k1gInSc2	úspěch
grungových	grungův	k2eAgFnPc2d1	grungův
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
zásluhou	zásluha	k1gFnSc7	zásluha
narůstající	narůstající	k2eAgFnSc2d1	narůstající
popularity	popularita	k1gFnSc2	popularita
Metallicou	Metallica	k1gFnSc7	Metallica
a	a	k8xC	a
thrashem	thrash	k1gInSc7	thrash
ovlivněného	ovlivněný	k2eAgInSc2d1	ovlivněný
groove	groov	k1gInSc5	groov
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgMnSc4	jenž
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
kapely	kapela	k1gFnPc1	kapela
Pantera	panter	k1gMnSc2	panter
a	a	k8xC	a
White	Whit	k1gMnSc5	Whit
Zombie	Zombius	k1gMnSc5	Zombius
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgFnPc3	některý
novým	nový	k2eAgFnPc3d1	nová
skupinám	skupina	k1gFnPc3	skupina
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
;	;	kIx,	;
např.	např.	kA	např.
album	album	k1gNnSc1	album
Far	fara	k1gFnPc2	fara
Beyond	Beyonda	k1gFnPc2	Beyonda
Driven	Drivna	k1gFnPc2	Drivna
od	od	k7c2	od
Pantery	panter	k1gInPc1	panter
bodovalo	bodovat	k5eAaImAgNnS	bodovat
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
"	"	kIx"	"
<g/>
v	v	k7c6	v
bezvýrazných	bezvýrazný	k2eAgNnPc6d1	bezvýrazné
očích	oko	k1gNnPc6	oko
mainstreamu	mainstream	k1gInSc2	mainstream
byl	být	k5eAaImAgInS	být
metal	metat	k5eAaImAgInS	metat
již	již	k6eAd1	již
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Některé	některý	k3yIgFnPc1	některý
kapely	kapela	k1gFnPc1	kapela
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
novým	nový	k2eAgFnPc3d1	nová
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
1996	[number]	k4	1996
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Lollapalooza	Lollapalooza	k1gFnSc1	Lollapalooza
skupina	skupina	k1gFnSc1	skupina
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
členové	člen	k1gMnPc1	člen
si	se	k3xPyFc3	se
nechali	nechat	k5eAaPmAgMnP	nechat
předtím	předtím	k6eAd1	předtím
ostříhat	ostříhat	k5eAaImF	ostříhat
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
kroku	krok	k1gInSc6	krok
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
dostavily	dostavit	k5eAaPmAgFnP	dostavit
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
hromadící	hromadící	k2eAgFnPc1d1	hromadící
reakce	reakce	k1gFnPc1	reakce
a	a	k8xC	a
odklon	odklon	k1gInSc1	odklon
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Avšak	avšak	k8xC	avšak
nemožno	možno	k6eNd1	možno
popřít	popřít	k5eAaPmF	popřít
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
koncem	koncem	k7c2	koncem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
Metallica	Metallica	k1gFnSc1	Metallica
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
moderní	moderní	k2eAgFnSc7d1	moderní
rockovou	rockový	k2eAgFnSc7d1	rocková
skupinou	skupina	k1gFnSc7	skupina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Jane	Jan	k1gMnSc5	Jan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Addiction	Addiction	k1gInSc1	Addiction
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
jiné	jiný	k2eAgFnPc1d1	jiná
populární	populární	k2eAgFnPc1d1	populární
hudební	hudební	k2eAgFnPc1d1	hudební
skupiny	skupina	k1gFnPc1	skupina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
mající	mající	k2eAgInPc1d1	mající
kořeny	kořen	k1gInPc1	kořen
v	v	k7c4	v
heavy	heava	k1gFnPc4	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
nesourodého	sourodý	k2eNgInSc2d1	nesourodý
hudebního	hudební	k2eAgInSc2d1	hudební
směru	směr	k1gInSc2	směr
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
"	"	kIx"	"
<g/>
alternativní	alternativní	k2eAgInSc4d1	alternativní
metal	metal	k1gInSc4	metal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vnášely	vnášet	k5eAaImAgFnP	vnášet
do	do	k7c2	do
metalu	metal	k1gInSc2	metal
prvky	prvek	k1gInPc4	prvek
mnohých	mnohý	k2eAgInPc2d1	mnohý
jiných	jiný	k2eAgInPc2d1	jiný
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
alternativní	alternativní	k2eAgInSc4d1	alternativní
metal	metal	k1gInSc4	metal
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rovněž	rovněž	k9	rovněž
grungová	grungový	k2eAgFnSc1d1	grungový
kapela	kapela	k1gFnSc1	kapela
Alice	Alice	k1gFnSc2	Alice
in	in	k?	in
Chains	Chains	k1gInSc1	Chains
<g/>
,	,	kIx,	,
či	či	k8xC	či
různé	různý	k2eAgFnPc1d1	různá
cross-overové	crossverový	k2eAgFnPc1d1	cross-overový
kapely	kapela	k1gFnPc1	kapela
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Faith	Faith	k1gMnSc1	Faith
No	no	k9	no
More	mor	k1gInSc5	mor
(	(	kIx(	(
<g/>
alternativní	alternativní	k2eAgInSc4d1	alternativní
rock	rock	k1gInSc4	rock
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
punku	punk	k1gInSc2	punk
<g/>
,	,	kIx,	,
funku	funk	k1gInSc2	funk
<g/>
,	,	kIx,	,
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
hip	hip	k0	hip
hopu	hop	k1gMnSc6	hop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Primus	primus	k1gMnSc1	primus
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc1	prvek
funku	funk	k1gInSc2	funk
<g/>
,	,	kIx,	,
punku	punk	k1gInSc2	punk
<g/>
,	,	kIx,	,
thrash	thrash	k1gMnSc1	thrash
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
experimentální	experimentální	k2eAgFnSc2d1	experimentální
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tool	Tool	k1gInSc1	Tool
(	(	kIx(	(
<g/>
metal	metal	k1gInSc1	metal
smíchaný	smíchaný	k2eAgInSc1d1	smíchaný
s	s	k7c7	s
progresivním	progresivní	k2eAgInSc7d1	progresivní
rockem	rock	k1gInSc7	rock
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ministry	ministr	k1gMnPc4	ministr
(	(	kIx(	(
<g/>
metal	metal	k1gInSc1	metal
plus	plus	k6eAd1	plus
industriální	industriální	k2eAgFnSc1d1	industriální
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marilyn	Marilyn	k1gFnSc1	Marilyn
Manson	Manson	k1gMnSc1	Manson
(	(	kIx(	(
<g/>
ubíral	ubírat	k5eAaImAgMnS	ubírat
se	se	k3xPyFc4	se
podobným	podobný	k2eAgInSc7d1	podobný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mimoto	mimoto	k6eAd1	mimoto
využíval	využívat	k5eAaPmAgInS	využívat
šokový	šokový	k2eAgInSc1d1	šokový
efekt	efekt	k1gInSc1	efekt
zpopularizovaný	zpopularizovaný	k2eAgInSc1d1	zpopularizovaný
Alicem	Alice	k1gMnSc7	Alice
Cooperem	Cooper	k1gMnSc7	Cooper
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
také	také	k9	také
formovat	formovat	k5eAaImF	formovat
další	další	k2eAgFnSc1d1	další
významná	významný	k2eAgFnSc1d1	významná
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hrála	hrát	k5eAaImAgFnS	hrát
styl	styl	k1gInSc4	styl
industrální	industrální	k2eAgInSc4d1	industrální
metal	metal	k1gInSc4	metal
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
německá	německý	k2eAgFnSc1d1	německá
skupina	skupina	k1gFnSc1	skupina
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvíce	hodně	k6eAd3	hodně
populární	populární	k2eAgFnPc4d1	populární
evropské	evropský	k2eAgFnPc4d1	Evropská
heavy	heava	k1gFnPc4	heava
metalové	metalový	k2eAgFnSc2d1	metalová
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
alternativní	alternativní	k2eAgMnPc1d1	alternativní
metalisté	metalista	k1gMnPc1	metalista
nevytvářeli	vytvářet	k5eNaImAgMnP	vytvářet
jednolitou	jednolitý	k2eAgFnSc4d1	jednolitá
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
spájela	spájet	k5eAaImAgFnS	spájet
je	on	k3xPp3gNnPc4	on
ochota	ochota	k1gFnSc1	ochota
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
metalem	metal	k1gInSc7	metal
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
nafintěnému	nafintěný	k2eAgInSc3d1	nafintěný
glam	glam	k6eAd1	glam
metalu	metal	k1gInSc3	metal
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
neplatí	platit	k5eNaImIp3nS	platit
o	o	k7c4	o
Marylin	Marylin	k2eAgInSc4d1	Marylin
Manson	Manson	k1gInSc4	Manson
a	a	k8xC	a
White	Whit	k1gMnSc5	Whit
Zombie	Zombius	k1gMnSc5	Zombius
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
spájení	spájený	k2eAgMnPc1d1	spájený
s	s	k7c7	s
alternativním	alternativní	k2eAgInSc7d1	alternativní
metalem	metal	k1gInSc7	metal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slátanina	slátanina	k1gFnSc1	slátanina
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
soundů	sound	k1gInPc2	sound
reprezentovala	reprezentovat	k5eAaImAgNnP	reprezentovat
"	"	kIx"	"
<g/>
barvitý	barvitý	k2eAgInSc4d1	barvitý
výsledek	výsledek	k1gInSc4	výsledek
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
stanuly	stanout	k5eAaPmAgFnP	stanout
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
okolnímu	okolní	k2eAgInSc3d1	okolní
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
najednou	najednou	k6eAd1	najednou
začalo	začít	k5eAaPmAgNnS	začít
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c4	o
oživení	oživení	k1gNnSc4	oživení
metalu	metal	k1gInSc2	metal
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
vycházejícím	vycházející	k2eAgFnPc3d1	vycházející
z	z	k7c2	z
vlny	vlna	k1gFnSc2	vlna
alternativního	alternativní	k2eAgInSc2d1	alternativní
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
kapely	kapela	k1gFnPc4	kapela
jako	jako	k8xC	jako
P.	P.	kA	P.
<g/>
O.D.	O.D.	k1gFnSc1	O.D.
-	-	kIx~	-
vz.	vz.	k?	vz.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
Korn	Korn	k1gMnSc1	Korn
-	-	kIx~	-
(	(	kIx(	(
<g/>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nu	nu	k9	nu
metal	metal	k1gInSc4	metal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Papa	papa	k1gMnSc1	papa
Roach	Roach	k1gMnSc1	Roach
-	-	kIx~	-
vz.	vz.	k?	vz.
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
-	-	kIx~	-
vz.	vz.	k?	vz.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Slipknot	Slipknota	k1gFnPc2	Slipknota
-	-	kIx~	-
vz.	vz.	k?	vz.
1993	[number]	k4	1993
a	a	k8xC	a
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
-	-	kIx~	-
vz.	vz.	k?	vz.
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
přezdívat	přezdívat	k5eAaImF	přezdívat
"	"	kIx"	"
<g/>
nu	nu	k9	nu
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
daly	dát	k5eAaPmAgFnP	dát
dohromady	dohromady	k6eAd1	dohromady
nový	nový	k2eAgInSc4d1	nový
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
kolébal	kolébat	k5eAaImAgInS	kolébat
mezi	mezi	k7c4	mezi
rytmy	rytmus	k1gInPc4	rytmus
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
funku	funk	k1gInSc2	funk
a	a	k8xC	a
hip	hip	k0	hip
hopu	hopu	k6eAd1	hopu
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
surových	surový	k2eAgFnPc2d1	surová
kytar	kytara	k1gFnPc2	kytara
s	s	k7c7	s
hutným	hutný	k2eAgInSc7d1	hutný
zvukem	zvuk	k1gInSc7	zvuk
<g/>
,	,	kIx,	,
rapových	rapový	k2eAgInPc2d1	rapový
vokálů	vokál	k1gInPc2	vokál
a	a	k8xC	a
zvukových	zvukový	k2eAgInPc2d1	zvukový
triků	trik	k1gInPc2	trik
dokázala	dokázat	k5eAaPmAgNnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
všekulturní	všekulturní	k2eAgInSc1d1	všekulturní
metal	metal	k1gInSc1	metal
může	moct	k5eAaImIp3nS	moct
vyplatit	vyplatit	k5eAaPmF	vyplatit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nu	nu	k9	nu
metal	metal	k1gInSc4	metal
získal	získat	k5eAaPmAgMnS	získat
přízeň	přízeň	k1gFnSc4	přízeň
komerčního	komerční	k2eAgNnSc2d1	komerční
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
dopomohly	dopomoct	k5eAaPmAgFnP	dopomoct
především	především	k9	především
dvě	dva	k4xCgFnPc1	dva
skutečnosti	skutečnost	k1gFnPc1	skutečnost
<g/>
:	:	kIx,	:
přítomnost	přítomnost	k1gFnSc1	přítomnost
na	na	k7c6	na
MTV	MTV	kA	MTV
a	a	k8xC	a
putovní	putovní	k2eAgInSc1d1	putovní
festival	festival	k1gInSc1	festival
Ozzyho	Ozzy	k1gMnSc2	Ozzy
Osbourna	Osbourna	k1gFnSc1	Osbourna
"	"	kIx"	"
<g/>
Ozzfest	Ozzfest	k1gInSc1	Ozzfest
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
uvedený	uvedený	k2eAgInSc1d1	uvedený
roku	rok	k1gInSc3	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
mezi	mezi	k7c7	mezi
novináři	novinář	k1gMnPc7	novinář
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
obrodě	obroda	k1gFnSc6	obroda
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Life	Lif	k1gMnSc2	Lif
Is	Is	k1gMnSc2	Is
Peachy	Peacha	k1gMnSc2	Peacha
od	od	k7c2	od
Korn	Korna	k1gFnPc2	Korna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgMnSc6	první
numetalovým	numetalův	k2eAgNnSc7d1	numetalův
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
hitparády	hitparáda	k1gFnSc2	hitparáda
top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
další	další	k2eAgInSc1d1	další
studiový	studiový	k2eAgInSc1d1	studiový
počin	počin	k1gInSc1	počin
Follow	Follow	k1gFnSc2	Follow
the	the	k?	the
Leader	leader	k1gMnSc1	leader
umístil	umístit	k5eAaPmAgMnS	umístit
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Billboard	billboard	k1gInSc1	billboard
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
500	[number]	k4	500
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
metalových	metalový	k2eAgInPc2d1	metalový
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
pořadů	pořad	k1gInPc2	pořad
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
trojnásobek	trojnásobek	k1gInSc4	trojnásobek
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
obrovské	obrovský	k2eAgFnSc3d1	obrovská
popularitě	popularita	k1gFnSc3	popularita
nu	nu	k9	nu
metalu	metal	k1gInSc3	metal
<g/>
,	,	kIx,	,
metalisté	metalista	k1gMnPc1	metalista
ho	on	k3xPp3gMnSc4	on
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
míře	míra	k1gFnSc6	míra
uznat	uznat	k5eAaPmF	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
nu	nu	k9	nu
metal	metat	k5eAaImAgInS	metat
a	a	k8xC	a
jemu	on	k3xPp3gMnSc3	on
spřízněné	spřízněný	k2eAgInPc1d1	spřízněný
styly	styl	k1gInPc1	styl
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
vrchol	vrchol	k1gInSc4	vrchol
popularity	popularita	k1gFnSc2	popularita
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vytrácet	vytrácet	k5eAaImF	vytrácet
do	do	k7c2	do
úzadí	úzadí	k1gNnSc2	úzadí
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gMnSc1	Down
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
nadále	nadále	k6eAd1	nadále
udržovaly	udržovat	k5eAaImAgFnP	udržovat
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002-3	[number]	k4	2002-3
se	se	k3xPyFc4	se
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
dostal	dostat	k5eAaPmAgInS	dostat
nový	nový	k2eAgInSc1d1	nový
styl	styl	k1gInSc1	styl
zvaný	zvaný	k2eAgInSc1d1	zvaný
metalcore	metalcor	k1gInSc5	metalcor
<g/>
,	,	kIx,	,
směs	směs	k1gFnSc4	směs
thrash	thrash	k1gInSc4	thrash
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
melodického	melodický	k2eAgInSc2d1	melodický
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
hardcoreu	hardcoreus	k1gInSc2	hardcoreus
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
stylu	styl	k1gInSc6	styl
crossover	crossover	k1gMnSc1	crossover
thrash	thrash	k1gMnSc1	thrash
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
rozvinuly	rozvinout	k5eAaPmAgFnP	rozvinout
kapely	kapela	k1gFnPc1	kapela
typu	typ	k1gInSc2	typ
Suicidal	Suicidal	k1gMnSc1	Suicidal
Tendencies	Tendencies	k1gMnSc1	Tendencies
<g/>
,	,	kIx,	,
Dirty	Dirt	k1gInPc1	Dirt
Rotten	Rotten	k2eAgInSc1d1	Rotten
Imbeciles	Imbeciles	k1gInSc4	Imbeciles
a	a	k8xC	a
Stormtroopers	Stormtroopers	k1gInSc4	Stormtroopers
of	of	k?	of
Death	Death	k1gInSc1	Death
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
metalcore	metalcor	k1gInSc5	metalcor
držel	držet	k5eAaImAgInS	držet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
vynesly	vynést	k5eAaPmAgInP	vynést
nahrávky	nahrávka	k1gFnPc4	nahrávka
jako	jako	k8xS	jako
The	The	k1gFnPc4	The
End	End	k1gFnPc2	End
of	of	k?	of
Heartache	Heartache	k1gFnPc2	Heartache
od	od	k7c2	od
Killswitch	Killswitcha	k1gFnPc2	Killswitcha
Engage	Engag	k1gFnSc2	Engag
a	a	k8xC	a
The	The	k1gFnSc2	The
War	War	k1gMnSc1	War
Within	Within	k1gMnSc1	Within
od	od	k7c2	od
Shadows	Shadowsa	k1gFnPc2	Shadowsa
Fall	Falla	k1gFnPc2	Falla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
albové	albový	k2eAgFnSc6d1	albová
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc1	billboard
<g/>
;	;	kIx,	;
Trivium	trivium	k1gNnSc1	trivium
se	se	k3xPyFc4	se
s	s	k7c7	s
albem	album	k1gNnSc7	album
The	The	k1gFnSc2	The
Crusade	Crusad	k1gInSc5	Crusad
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
pětky	pětka	k1gFnSc2	pětka
americké	americký	k2eAgFnSc2d1	americká
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k9	co
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Bullet	Bullet	k1gInSc1	Bullet
for	forum	k1gNnPc2	forum
My	my	k3xPp1nPc1	my
Valentine	Valentin	k1gMnSc5	Valentin
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
s	s	k7c7	s
albem	album	k1gNnSc7	album
Scream	Scream	k1gInSc1	Scream
Aim	Aim	k1gMnSc2	Aim
Fire	Fir	k1gMnSc2	Fir
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
v	v	k7c6	v
britských	britský	k2eAgFnPc6d1	britská
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
metalcore	metalcor	k1gInSc5	metalcor
těší	těšit	k5eAaImIp3nS	těšit
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc3d2	veliký
pozornosti	pozornost	k1gFnSc3	pozornost
na	na	k7c6	na
Ozzfestu	Ozzfest	k1gInSc6	Ozzfest
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
na	na	k7c4	na
Download	Download	k1gInSc4	Download
Festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Skupině	skupina	k1gFnSc3	skupina
Lamb	Lamb	k1gMnSc1	Lamb
of	of	k?	of
God	God	k1gMnSc1	God
<g/>
,	,	kIx,	,
slévající	slévající	k2eAgFnPc4d1	slévající
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
několik	několik	k4yIc4	několik
metalových	metalový	k2eAgInPc2d1	metalový
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
albem	album	k1gNnSc7	album
Sacrament	Sacrament	k1gMnSc1	Sacrament
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
obsadit	obsadit	k5eAaPmF	obsadit
první	první	k4xOgFnSc2	první
příčky	příčka	k1gFnSc2	příčka
hitparády	hitparáda	k1gFnSc2	hitparáda
Billboard	billboard	k1gInSc1	billboard
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
těchto	tento	k3xDgFnPc2	tento
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Mastodon	mastodon	k1gMnSc1	mastodon
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
progresivního	progresivní	k2eAgNnSc2d1	progresivní
a	a	k8xC	a
sludge	sludge	k1gInSc1	sludge
metalu	metal	k1gInSc2	metal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
obrodě	obroda	k1gFnSc6	obroda
amerického	americký	k2eAgInSc2d1	americký
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
přezdívají	přezdívat	k5eAaImIp3nP	přezdívat
Nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
amerického	americký	k2eAgMnSc2d1	americký
heavy	heavo	k1gNnPc7	heavo
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
metal	metat	k5eAaImAgInS	metat
neztrácí	ztrácet	k5eNaImIp3nP	ztrácet
nic	nic	k3yNnSc4	nic
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
obrovské	obrovský	k2eAgFnSc2d1	obrovská
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
thrasheři	thrasher	k1gMnPc1	thrasher
The	The	k1gMnSc1	The
Haunted	Haunted	k1gMnSc1	Haunted
<g/>
,	,	kIx,	,
melodičtí	melodický	k2eAgMnPc1d1	melodický
deathmetalisté	deathmetalista	k1gMnPc1	deathmetalista
In	In	k1gMnSc1	In
Flames	Flames	k1gMnSc1	Flames
<g/>
,	,	kIx,	,
Kalmah	Kalmah	k1gMnSc1	Kalmah
a	a	k8xC	a
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
<g/>
,	,	kIx,	,
extrémní	extrémní	k2eAgMnPc1d1	extrémní
symfonici	symfonik	k1gMnPc1	symfonik
Dimmu	Dimm	k1gInSc2	Dimm
Borgir	Borgira	k1gFnPc2	Borgira
a	a	k8xC	a
Cradle	Cradle	k1gFnPc2	Cradle
of	of	k?	of
Filth	Filtha	k1gFnPc2	Filtha
<g/>
,	,	kIx,	,
a	a	k8xC	a
powermetalisté	powermetalista	k1gMnPc1	powermetalista
HammerFall	HammerFalla	k1gFnPc2	HammerFalla
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
retro	retro	k1gNnSc1	retro
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
hrajícími	hrající	k2eAgFnPc7d1	hrající
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Angličanů	Angličan	k1gMnPc2	Angličan
The	The	k1gMnPc2	The
Darkness	Darknessa	k1gFnPc2	Darknessa
nebo	nebo	k8xC	nebo
Australanů	Australan	k1gMnPc2	Australan
Wolfmother	Wolfmothra	k1gFnPc2	Wolfmothra
<g/>
.	.	kIx.	.
</s>
<s>
Pětinásobně	pětinásobně	k6eAd1	pětinásobně
platinové	platinový	k2eAgNnSc1d1	platinové
album	album	k1gNnSc1	album
Permission	Permission	k1gInSc1	Permission
to	ten	k3xDgNnSc4	ten
Land	Land	k1gMnSc1	Land
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
od	od	k7c2	od
The	The	k1gFnSc2	The
Darkness	Darkness	k1gInSc1	Darkness
<g/>
,	,	kIx,	,
charakterizované	charakterizovaný	k2eAgInPc1d1	charakterizovaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
nevrle	nevrle	k6eAd1	nevrle
realistická	realistický	k2eAgFnSc1d1	realistická
napodobenina	napodobenina	k1gFnSc1	napodobenina
metalu	metal	k1gInSc2	metal
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
glamu	glam	k1gInSc2	glam
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obsadil	obsadit	k5eAaPmAgMnS	obsadit
první	první	k4xOgFnPc4	první
příčky	příčka	k1gFnPc4	příčka
britských	britský	k2eAgFnPc2d1	britská
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Wolfmother	Wolfmothra	k1gFnPc2	Wolfmothra
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
podle	podle	k7c2	podle
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
"	"	kIx"	"
<g/>
Deep	Deep	k1gInSc1	Deep
purplovskými	purplovský	k2eAgNnPc7d1	purplovský
organy	organon	k1gNnPc7	organon
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
akordovým	akordový	k2eAgNnSc7d1	akordové
riffovaním	riffovaní	k1gNnSc7	riffovaní
hodným	hodný	k2eAgNnSc7d1	hodné
Jimmyho	Jimmy	k1gMnSc4	Jimmy
Page	Page	k1gFnSc6	Page
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
a	a	k8xC	a
zavíjením	zavíjení	k1gNnSc7	zavíjení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
vokalistu	vokalista	k1gMnSc4	vokalista
Andrewa	Andrewus	k1gMnSc2	Andrewus
Stockdale	Stockdala	k1gFnSc3	Stockdala
"	"	kIx"	"
<g/>
na	na	k7c4	na
které	který	k3yQgFnPc4	který
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k8xC	ani
Robert	Robert	k1gMnSc1	Robert
Plant	planta	k1gFnPc2	planta
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Woman	Woman	k1gInSc1	Woman
<g/>
"	"	kIx"	"
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hardrockový	hardrockový	k2eAgInSc4d1	hardrockový
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k9	co
"	"	kIx"	"
<g/>
Eyes	Eyes	k1gInSc1	Eyes
of	of	k?	of
the	the	k?	the
Insane	Insan	k1gMnSc5	Insan
<g/>
"	"	kIx"	"
od	od	k7c2	od
Slayer	Slayra	k1gFnPc2	Slayra
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
metalový	metalový	k2eAgInSc4d1	metalový
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
2008	[number]	k4	2008
Slayer	Slayer	k1gMnSc1	Slayer
dostal	dostat	k5eAaPmAgMnS	dostat
opět	opět	k6eAd1	opět
Grammy	Gramm	k1gInPc4	Gramm
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
metalový	metalový	k2eAgInSc4d1	metalový
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
protentokrát	protentokrát	k6eAd1	protentokrát
za	za	k7c4	za
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Final	Final	k1gMnSc1	Final
Six	Six	k1gMnSc1	Six
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
