<s>
Pojem	pojem	k1gInSc1	pojem
heavy	heava	k1gFnSc2	heava
metal	metat	k5eAaImAgInS	metat
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
v	v	k7c6	v
několika	několik	k4yIc6	několik
významech	význam	k1gInPc6	význam
<g/>
:	:	kIx,	:
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
představuje	představovat	k5eAaImIp3nS	představovat
tradiční	tradiční	k2eAgFnPc4d1	tradiční
heavy	heava	k1gFnPc4	heava
metal	metal	k1gInSc4	metal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
eliminováním	eliminování	k1gNnSc7	eliminování
bluesového	bluesový	k2eAgInSc2d1	bluesový
vlivu	vliv	k1gInSc2	vliv
<g/>
;	;	kIx,	;
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
představuje	představovat	k5eAaImIp3nS	představovat
heavy	heava	k1gFnPc4	heava
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
zkráceně	zkráceně	k6eAd1	zkráceně
metal	metat	k5eAaImAgMnS	metat
<g/>
)	)	kIx)	)
celý	celý	k2eAgInSc4d1	celý
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgFnPc2	všecek
jeho	jeho	k3xOp3gFnSc1	jeho
odnoží	odnožit	k5eAaPmIp3nS	odnožit
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
pojem	pojem	k1gInSc4	pojem
heavy	heava	k1gFnSc2	heava
metal	metal	k1gInSc1	metal
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
<g/>
,	,	kIx,	,
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
Heavy	Heava	k1gFnSc2	Heava
metal	metal	k1gInSc1	metal
není	být	k5eNaImIp3nS	být
totéž	týž	k3xTgNnSc1	týž
jako	jako	k9	jako
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
