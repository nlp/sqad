<s>
Rockefeller	Rockefeller	k1gInSc1
Center	centrum	k1gNnPc2
</s>
<s>
Rockefeller	Rockefeller	k1gInSc1
Plaza	plaz	k1gMnSc2
</s>
<s>
Rockefeller	Rockefeller	k1gInSc1
Center	centrum	k1gNnSc1
je	být	k5eAaImIp3nS
komplex	komplex	k1gInSc1
19	#num#	k4
obchodních	obchodní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
pokrývající	pokrývající	k2eAgFnSc4d1
rozlohu	rozloha	k1gFnSc4
22	#num#	k4
akrů	akr	k1gInPc2
mezi	mezi	k7c7
ulicemi	ulice	k1gFnPc7
48	#num#	k4
<g/>
th	th	k?
a	a	k8xC
51	#num#	k4
<g/>
st	st	kA
Street	Street	k1gInSc1
v	v	k7c6
New	New	k1gInSc6
York	York	k1gInSc6
City	city	k1gInSc6
<g/>
,	,	kIx,
stát	stát	k1gInSc1
New	New	k1gInSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplex	komplex	k1gInSc1
<g/>
,	,	kIx,
postavený	postavený	k2eAgInSc1d1
rodinou	rodina	k1gFnSc7
Rockefellerů	Rockefeller	k1gMnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
Midtown	Midtown	k1gInSc2
Manhattanu	Manhattan	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
ulicemi	ulice	k1gFnPc7
Fifth	Fifth	k4
Avenue	avenue	k1gFnSc1
a	a	k8xC
Seventh	Seventha	k4
Avenue	avenue	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
byl	být	k5eAaImAgInS
komplex	komplex	k1gInSc1
vyhlášen	vyhlásit	k5eAaPmNgInS
Národní	národní	k2eAgFnSc7d1
historickou	historický	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgInSc4d3
privátně	privátně	k6eAd1
vlastněný	vlastněný	k2eAgInSc4d1
komplex	komplex	k1gInSc4
budov	budova	k1gFnPc2
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
budování	budování	k1gNnSc2
sahají	sahat	k5eAaImIp3nP
do	do	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavět	stavět	k5eAaImF
ho	on	k3xPp3gNnSc4
začal	začít	k5eAaPmAgMnS
John	John	k1gMnSc1
D.	D.	kA
Rockefeller	Rockefeller	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Součásti	součást	k1gFnSc3
Rockefeller	Rockefeller	k1gMnSc1
Center	centrum	k1gNnPc2
</s>
<s>
Radio	radio	k1gNnSc1
City	City	k1gFnSc2
Music	Musice	k1gFnPc2
Hall	Hall	k1gMnSc1
</s>
<s>
30	#num#	k4
Rockefeller	Rockefeller	k1gInSc1
Plaza	plaz	k1gMnSc4
</s>
<s>
Center	centrum	k1gNnPc2
Art	Art	k1gMnSc2
</s>
<s>
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
budov	budova	k1gFnPc2
na	na	k7c4
Rockefeller	Rockefeller	k1gInSc4
Plaza	plaz	k1gMnSc2
<g/>
,	,	kIx,
Avenue	avenue	k1gFnSc2
of	of	k?
the	the	k?
Americas	Americas	k1gMnSc1
<g/>
,	,	kIx,
Fifth	Fifth	k1gMnSc1
Avenue	avenue	k1gFnSc2
a	a	k8xC
Seventh	Seventh	k1gMnSc1
Avenue	avenue	k1gFnSc2
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Radio	radio	k1gNnSc1
City	City	k1gFnSc2
Music	Musice	k1gFnPc2
Hall	Hall	k1gMnSc1
</s>
<s>
Radio	radio	k1gNnSc4
City	City	k1gFnSc2
Music	Music	k1gMnSc1
Hall	Hall	k1gMnSc1
o	o	k7c6
vánocích	vánoce	k1gFnPc6
2007	#num#	k4
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Atlas	Atlas	k1gInSc1
</s>
<s>
Prometheus	Prometheus	k1gMnSc1
</s>
<s>
30	#num#	k4
Rockefeller	Rockefeller	k1gInSc1
Plaza	plaz	k1gMnSc4
</s>
<s>
kluziště	kluziště	k1gNnSc1
u	u	k7c2
sochy	socha	k1gFnSc2
Promethea	Prometheus	k1gMnSc2
</s>
<s>
Lower	Lower	k1gInSc1
Plaza	plaz	k1gMnSc2
</s>
<s>
Palazzo	Palazza	k1gFnSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Italia	Italius	k1gMnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Museum	museum	k1gNnSc1
of	of	k?
Modern	Modern	k1gMnSc1
Art	Art	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rockefeller	Rockefeller	k1gInSc1
Center	centrum	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Rockefeller	Rockefeller	k1gInSc1
center	centrum	k1gNnPc2
na	na	k7c4
Google	Google	k1gNnSc4
Maps	Mapsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4661318-3	4661318-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2228	#num#	k4
5797	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50053842	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500303627	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
151238684	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50053842	#num#	k4
</s>
