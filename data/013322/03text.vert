<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Svídnická	Svídnický	k2eAgFnSc1d1	Svídnická
též	též	k9	též
Opolská	opolský	k2eAgFnSc1d1	Opolská
(	(	kIx(	(
<g/>
†	†	k?	†
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
opolskou	opolský	k2eAgFnSc7d1	Opolská
kněžnou	kněžna	k1gFnSc7	kněžna
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
slezských	slezský	k2eAgInPc2d1	slezský
Piastovců	Piastovec	k1gInPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Bernarda	Bernard	k1gMnSc2	Bernard
Svídnického	Svídnický	k2eAgMnSc2d1	Svídnický
a	a	k8xC	a
manželkou	manželka	k1gFnSc7	manželka
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
Opolského	opolský	k2eAgMnSc4d1	opolský
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
měla	mít	k5eAaImAgFnS	mít
sedm	sedm	k4xCc4	sedm
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc4	tři
syny	syn	k1gMnPc4	syn
–	–	k?	–
Vladislava	Vladislava	k1gFnSc1	Vladislava
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Boleslava	Boleslav	k1gMnSc4	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc4	Jindřich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
FUKALA	Fukal	k1gMnSc4	Fukal
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
<g/>
.	.	kIx.	.
</s>
<s>
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Neznámá	známý	k2eNgFnSc1d1	neznámá
země	země	k1gFnSc1	země
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Knížecí	knížecí	k2eAgNnSc1d1	knížecí
a	a	k8xC	a
stavovské	stavovský	k2eAgNnSc1d1	Stavovské
Slezsko	Slezsko	k1gNnSc1	Slezsko
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
344	[number]	k4	344
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
95	[number]	k4	95
<g/>
,	,	kIx,	,
312	[number]	k4	312
<g/>
.	.	kIx.	.
</s>
</p>
