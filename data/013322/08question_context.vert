<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Svídnická	Svídnický	k2eAgFnSc1d1	Svídnická
též	též	k9	též
Opolská	opolský	k2eAgFnSc1d1	Opolská
(	(	kIx(	(
<g/>
†	†	k?	†
1348	[number]	k4	1348
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
opolskou	opolský	k2eAgFnSc7d1	Opolská
kněžnou	kněžna	k1gFnSc7	kněžna
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
slezských	slezský	k2eAgInPc2d1	slezský
Piastovců	Piastovec	k1gInPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Bernarda	Bernard	k1gMnSc2	Bernard
Svídnického	Svídnický	k2eAgMnSc2d1	Svídnický
a	a	k8xC	a
manželkou	manželka	k1gFnSc7	manželka
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
Opolského	opolský	k2eAgMnSc4d1	opolský
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
měla	mít	k5eAaImAgFnS	mít
sedm	sedm	k4xCc4	sedm
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tři	tři	k4xCgInPc4	tři
syny	syn	k1gMnPc4	syn
–	–	k?	–
Vladislava	Vladislava	k1gFnSc1	Vladislava
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Boleslava	Boleslav	k1gMnSc4	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc4	Jindřich
<g/>
.	.	kIx.	.
</s>
