<s>
Antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
též	též	k9	též
kontracepce	kontracepce	k1gFnSc1	kontracepce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
neplánovaným	plánovaný	k2eNgNnSc7d1	neplánované
těhotenstvím	těhotenství	k1gNnSc7	těhotenství
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zabránění	zabránění	k1gNnSc4	zabránění
mužským	mužský	k2eAgFnPc3d1	mužská
spermiím	spermie	k1gFnPc3	spermie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
potkaly	potkat	k5eAaPmAgInP	potkat
s	s	k7c7	s
ženskými	ženský	k2eAgNnPc7d1	ženské
vajíčky	vajíčko	k1gNnPc7	vajíčko
a	a	k8xC	a
oplodnily	oplodnit	k5eAaPmAgInP	oplodnit
je	on	k3xPp3gFnPc4	on
<g/>
.	.	kIx.	.
</s>
