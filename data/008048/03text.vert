<s>
Antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
též	též	k9	též
kontracepce	kontracepce	k1gFnSc1	kontracepce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
neplánovaným	plánovaný	k2eNgNnSc7d1	neplánované
těhotenstvím	těhotenství	k1gNnSc7	těhotenství
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zabránění	zabránění	k1gNnSc4	zabránění
mužským	mužský	k2eAgFnPc3d1	mužská
spermiím	spermie	k1gFnPc3	spermie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
potkaly	potkat	k5eAaPmAgInP	potkat
s	s	k7c7	s
ženskými	ženský	k2eAgNnPc7d1	ženské
vajíčky	vajíčko	k1gNnPc7	vajíčko
a	a	k8xC	a
oplodnily	oplodnit	k5eAaPmAgInP	oplodnit
je	on	k3xPp3gFnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
antikoncepčních	antikoncepční	k2eAgFnPc2d1	antikoncepční
metod	metoda	k1gFnPc2	metoda
není	být	k5eNaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
u	u	k7c2	u
ženy	žena	k1gFnSc2	žena
k	k	k7c3	k
početí	početí	k1gNnSc3	početí
<g/>
.	.	kIx.	.
</s>
<s>
Antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
také	také	k9	také
přenosu	přenos	k1gInSc2	přenos
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Plánování	plánování	k1gNnSc1	plánování
<g/>
,	,	kIx,	,
poskytování	poskytování	k1gNnSc1	poskytování
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
plánování	plánování	k1gNnSc1	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
sex	sex	k1gInSc4	sex
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
používání	používání	k1gNnSc1	používání
mužského	mužský	k2eAgInSc2d1	mužský
nebo	nebo	k8xC	nebo
ženského	ženský	k2eAgInSc2d1	ženský
kondomu	kondom	k1gInSc2	kondom
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
rovněž	rovněž	k9	rovněž
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
prevence	prevence	k1gFnSc1	prevence
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
metody	metoda	k1gFnPc1	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
opravdu	opravdu	k6eAd1	opravdu
spolehlivé	spolehlivý	k2eAgFnPc1d1	spolehlivá
a	a	k8xC	a
bezpečné	bezpečný	k2eAgFnPc1d1	bezpečná
metody	metoda	k1gFnPc1	metoda
se	se	k3xPyFc4	se
ale	ale	k9	ale
objevily	objevit	k5eAaPmAgInP	objevit
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kultury	kultura	k1gFnPc1	kultura
záměrně	záměrně	k6eAd1	záměrně
omezují	omezovat	k5eAaImIp3nP	omezovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
morálně	morálně	k6eAd1	morálně
nebo	nebo	k8xC	nebo
politicky	politicky	k6eAd1	politicky
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
<s>
Hormonální	hormonální	k2eAgFnSc1d1	hormonální
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
také	také	k9	také
prokazatelně	prokazatelně	k6eAd1	prokazatelně
negativní	negativní	k2eAgInSc4d1	negativní
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
plodnost	plodnost	k1gFnSc4	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejspolehlivějším	spolehlivý	k2eAgFnPc3d3	nejspolehlivější
metodám	metoda	k1gFnPc3	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
patří	patřit	k5eAaImIp3nS	patřit
sterilizace	sterilizace	k1gFnSc1	sterilizace
vasektomií	vasektomie	k1gFnPc2	vasektomie
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
podvázáním	podvázání	k1gNnSc7	podvázání
vejcovodů	vejcovod	k1gInPc2	vejcovod
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
nitroděložní	nitroděložní	k2eAgNnPc4d1	nitroděložní
tělíska	tělísko	k1gNnPc4	tělísko
(	(	kIx(	(
<g/>
IUD	IUD	kA	IUD
<g/>
)	)	kIx)	)
a	a	k8xC	a
implantovaná	implantovaný	k2eAgFnSc1d1	implantovaná
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
<g/>
.	.	kIx.	.
<g/>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
hormonální	hormonální	k2eAgFnSc2d1	hormonální
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
jako	jako	k8xC	jako
například	například	k6eAd1	například
ústně	ústně	k6eAd1	ústně
užívané	užívaný	k2eAgInPc1d1	užívaný
tablety	tablet	k1gInPc1	tablet
<g/>
,	,	kIx,	,
náplasti	náplast	k1gFnPc1	náplast
<g/>
,	,	kIx,	,
vaginální	vaginální	k2eAgInPc1d1	vaginální
kroužky	kroužek	k1gInPc1	kroužek
a	a	k8xC	a
injekce	injekce	k1gFnPc1	injekce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
méně	málo	k6eAd2	málo
spolehlivým	spolehlivý	k2eAgFnPc3d1	spolehlivá
metodám	metoda	k1gFnPc3	metoda
patří	patřit	k5eAaImIp3nS	patřit
bariérová	bariérový	k2eAgFnSc1d1	bariérová
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
-	-	kIx~	-
kondomy	kondom	k1gInPc4	kondom
<g/>
,	,	kIx,	,
poševní	poševní	k2eAgInPc4d1	poševní
pesary	pesar	k1gInPc4	pesar
a	a	k8xC	a
antikoncepční	antikoncepční	k2eAgFnPc4d1	antikoncepční
houbičky	houbička	k1gFnPc4	houbička
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
metody	metoda	k1gFnPc4	metoda
sledování	sledování	k1gNnSc2	sledování
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
spolehlivými	spolehlivý	k2eAgFnPc7d1	spolehlivá
metodami	metoda	k1gFnPc7	metoda
jsou	být	k5eAaImIp3nP	být
spermicidní	spermicidní	k2eAgFnSc1d1	spermicidní
látky	látka	k1gFnPc1	látka
a	a	k8xC	a
přerušení	přerušení	k1gNnSc1	přerušení
soulože	soulož	k1gFnSc2	soulož
před	před	k7c7	před
ejakulací	ejakulace	k1gFnSc7	ejakulace
<g/>
.	.	kIx.	.
</s>
<s>
Sterilizace	sterilizace	k1gFnSc1	sterilizace
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
vysoce	vysoce	k6eAd1	vysoce
spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nevratná	vratný	k2eNgFnSc1d1	nevratná
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
vratné	vratný	k2eAgFnPc1d1	vratná
většinou	většina	k1gFnSc7	většina
ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přestanou	přestat	k5eAaPmIp3nP	přestat
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Nouzová	nouzový	k2eAgFnSc1d1	nouzová
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
může	moct	k5eAaImIp3nS	moct
zabránit	zabránit	k5eAaPmF	zabránit
početí	početí	k1gNnSc4	početí
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
po	po	k7c6	po
nechráněném	chráněný	k2eNgInSc6d1	nechráněný
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
za	za	k7c4	za
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
považována	považován	k2eAgFnSc1d1	považována
také	také	k9	také
sexuální	sexuální	k2eAgFnSc1d1	sexuální
abstinence	abstinence	k1gFnSc1	abstinence
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
výchova	výchova	k1gFnSc1	výchova
k	k	k7c3	k
sexuální	sexuální	k2eAgFnSc3d1	sexuální
abstinenci	abstinence	k1gFnSc3	abstinence
může	moct	k5eAaImIp3nS	moct
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
četnost	četnost	k1gFnSc4	četnost
těhotenství	těhotenství	k1gNnSc2	těhotenství
mezi	mezi	k7c7	mezi
nezletilými	nezletilý	k1gMnPc7	nezletilý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
bez	bez	k7c2	bez
poučení	poučení	k1gNnSc2	poučení
o	o	k7c6	o
antikoncepci	antikoncepce	k1gFnSc6	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nezletilých	nezletilá	k1gFnPc2	nezletilá
hrozí	hrozit	k5eAaImIp3nS	hrozit
větší	veliký	k2eAgNnSc4d2	veliký
riziko	riziko	k1gNnSc4	riziko
komplikací	komplikace	k1gFnPc2	komplikace
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
a	a	k8xC	a
porodu	porod	k1gInSc2	porod
i	i	k9	i
riziko	riziko	k1gNnSc4	riziko
pro	pro	k7c4	pro
dítě	dítě	k1gNnSc4	dítě
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
nežádoucích	žádoucí	k2eNgNnPc2d1	nežádoucí
těhotenství	těhotenství	k1gNnPc2	těhotenství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
snižuje	snižovat	k5eAaImIp3nS	snižovat
komplexní	komplexní	k2eAgFnSc1d1	komplexní
sexuální	sexuální	k2eAgFnSc1d1	sexuální
výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
dostupná	dostupný	k2eAgFnSc1d1	dostupná
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
všechny	všechen	k3xTgFnPc1	všechen
formy	forma	k1gFnPc1	forma
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
největší	veliký	k2eAgInSc1d3	veliký
přínos	přínos	k1gInSc1	přínos
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
snižování	snižování	k1gNnSc1	snižování
četnosti	četnost	k1gFnSc2	četnost
těhotenství	těhotenství	k1gNnSc2	těhotenství
u	u	k7c2	u
nezletilých	nezletilá	k1gFnPc2	nezletilá
má	mít	k5eAaImIp3nS	mít
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
účinkující	účinkující	k2eAgFnPc4d1	účinkující
reverzibilní	reverzibilní	k2eAgFnPc4d1	reverzibilní
antikoncepce	antikoncepce	k1gFnPc4	antikoncepce
jako	jako	k8xS	jako
implantáty	implantát	k1gInPc4	implantát
<g/>
,	,	kIx,	,
nitroděložní	nitroděložní	k2eAgNnPc4d1	nitroděložní
tělíska	tělísko	k1gNnPc4	tělísko
nebo	nebo	k8xC	nebo
vaginální	vaginální	k2eAgInPc4d1	vaginální
kroužky	kroužek	k1gInPc4	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
žena	žena	k1gFnSc1	žena
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
plně	plně	k6eAd1	plně
nekojí	kojit	k5eNaImIp3nS	kojit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
znovu	znovu	k6eAd1	znovu
otěhotnět	otěhotnět	k5eAaPmF	otěhotnět
už	už	k9	už
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
až	až	k9	až
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
metody	metoda	k1gFnPc4	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
začít	začít	k5eAaPmF	začít
používat	používat	k5eAaImF	používat
hned	hned	k6eAd1	hned
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k1gMnPc2	jiný
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyčkat	vyčkat	k5eAaPmF	vyčkat
až	až	k9	až
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kojících	kojící	k2eAgFnPc2d1	kojící
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
před	před	k7c7	před
kombinovanou	kombinovaný	k2eAgFnSc7d1	kombinovaná
ústně	ústně	k6eAd1	ústně
užívanou	užívaný	k2eAgFnSc7d1	užívaná
antikoncepcí	antikoncepce	k1gFnSc7	antikoncepce
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
tabletám	tableta	k1gFnPc3	tableta
obsahujícím	obsahující	k2eAgFnPc3d1	obsahující
pouze	pouze	k6eAd1	pouze
progestin	progestin	k2eAgMnSc1d1	progestin
<g/>
.	.	kIx.	.
</s>
<s>
Ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
menopauzy	menopauza	k1gFnPc4	menopauza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
ještě	ještě	k6eAd1	ještě
jeden	jeden	k4xCgInSc1	jeden
rok	rok	k1gInSc1	rok
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
menstruaci	menstruace	k1gFnSc6	menstruace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
222	[number]	k4	222
milionů	milion	k4xCgInPc2	milion
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
chtěly	chtít	k5eAaImAgFnP	chtít
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
otěhotnění	otěhotnění	k1gNnSc4	otěhotnění
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nepoužívají	používat	k5eNaImIp3nP	používat
žádnou	žádný	k3yNgFnSc4	žádný
z	z	k7c2	z
moderních	moderní	k2eAgFnPc2d1	moderní
metod	metoda	k1gFnPc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
používání	používání	k1gNnSc3	používání
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
se	se	k3xPyFc4	se
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
snížila	snížit	k5eAaPmAgFnS	snížit
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
matek	matka	k1gFnPc2	matka
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
předešlo	předejít	k5eAaPmAgNnS	předejít
až	až	k9	až
270	[number]	k4	270
000	[number]	k4	000
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plném	plný	k2eAgNnSc6d1	plné
uspokojení	uspokojení	k1gNnSc6	uspokojení
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
antikoncepci	antikoncepce	k1gFnSc6	antikoncepce
by	by	kYmCp3nS	by
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zabránit	zabránit	k5eAaPmF	zabránit
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
může	moct	k5eAaImIp3nS	moct
prodlužováním	prodlužování	k1gNnSc7	prodlužování
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
těhotenstvími	těhotenství	k1gNnPc7	těhotenství
snižovat	snižovat	k5eAaImF	snižovat
porodní	porodní	k2eAgNnPc4d1	porodní
rizika	riziko	k1gNnPc4	riziko
u	u	k7c2	u
dospělých	dospělý	k2eAgFnPc2d1	dospělá
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
jejich	jejich	k3xOp3gFnPc2	jejich
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojovém	rozvojový	k2eAgInSc6d1	rozvojový
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
lepšímu	dobrý	k2eAgInSc3d2	lepší
přístupu	přístup	k1gInSc3	přístup
k	k	k7c3	k
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
příjem	příjem	k1gInSc1	příjem
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
tělesná	tělesný	k2eAgFnSc1d1	tělesná
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc3	jejich
dětem	dítě	k1gFnPc3	dítě
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
lepšího	dobrý	k2eAgNnSc2d2	lepší
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
také	také	k9	také
jejich	jejich	k3xOp3gInSc1	jejich
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
ekonomickému	ekonomický	k2eAgInSc3d1	ekonomický
růstu	růst	k1gInSc3	růst
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
klesá	klesat	k5eAaImIp3nS	klesat
počet	počet	k1gInSc1	počet
vyživovaných	vyživovaný	k2eAgFnPc2d1	vyživovaná
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
žen	žena	k1gFnPc2	žena
jako	jako	k8xC	jako
pracovní	pracovní	k2eAgFnPc1d1	pracovní
síly	síla	k1gFnPc1	síla
a	a	k8xC	a
spotřeba	spotřeba	k1gFnSc1	spotřeba
vzácných	vzácný	k2eAgInPc2d1	vzácný
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Egyptský	egyptský	k2eAgInSc1d1	egyptský
Ebersův	Ebersův	k2eAgInSc1d1	Ebersův
papyrus	papyrus	k1gInSc1	papyrus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1550	[number]	k4	1550
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
Kahúnský	Kahúnský	k2eAgInSc1d1	Kahúnský
papyrus	papyrus	k1gInSc1	papyrus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nejranější	raný	k2eAgInPc1d3	nejranější
zdokumentované	zdokumentovaný	k2eAgInPc1d1	zdokumentovaný
popisy	popis	k1gInPc1	popis
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
-	-	kIx~	-
používání	používání	k1gNnSc1	používání
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
lístků	lístek	k1gInPc2	lístek
akácie	akácie	k1gFnSc1	akácie
a	a	k8xC	a
cupaniny	cupanina	k1gFnPc1	cupanina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vkládaly	vkládat	k5eAaImAgFnP	vkládat
do	do	k7c2	do
vagíny	vagína	k1gFnSc2	vagína
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zabránily	zabránit	k5eAaPmAgFnP	zabránit
proniknutí	proniknutí	k1gNnSc4	proniknutí
spermatu	sperma	k1gNnSc2	sperma
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgFnPc1d1	starověká
egyptské	egyptský	k2eAgFnPc1d1	egyptská
kresby	kresba	k1gFnPc1	kresba
rovněž	rovněž	k9	rovněž
znázorňují	znázorňovat	k5eAaImIp3nP	znázorňovat
používání	používání	k1gNnSc4	používání
kondomů	kondom	k1gInPc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Genesis	Genesis	k1gFnSc1	Genesis
popisuje	popisovat	k5eAaImIp3nS	popisovat
vyjmutí	vyjmutí	k1gNnSc1	vyjmutí
údu	úd	k1gInSc2	úd
neboli	neboli	k8xC	neboli
přerušovanou	přerušovaný	k2eAgFnSc4d1	přerušovaná
soulož	soulož	k1gFnSc4	soulož
jako	jako	k8xS	jako
způsob	způsob	k1gInSc4	způsob
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
,	,	kIx,	,
když	když	k8xS	když
Onan	Onan	k1gInSc1	Onan
"	"	kIx"	"
<g/>
vyplýtval	vyplýtvat	k5eAaPmAgInS	vyplýtvat
své	svůj	k3xOyFgNnSc4	svůj
semeno	semeno	k1gNnSc4	semeno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ejakuloval	ejakulovat	k5eAaPmAgInS	ejakulovat
<g/>
)	)	kIx)	)
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezplodil	zplodit	k5eNaPmAgInS	zplodit
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
ženou	žena	k1gFnSc7	žena
svého	svůj	k3xOyFgMnSc2	svůj
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
Támar	Támar	k1gInSc1	Támar
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
se	se	k3xPyFc4	se
jako	jako	k9	jako
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
používal	používat	k5eAaImAgInS	používat
hladýš	hladýš	k1gInSc1	hladýš
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
účinnosti	účinnost	k1gFnSc3	účinnost
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
žádoucnosti	žádoucnost	k1gFnSc3	žádoucnost
hromadně	hromadně	k6eAd1	hromadně
sklízen	sklízet	k5eAaImNgInS	sklízet
až	až	k9	až
do	do	k7c2	do
vymizení	vymizení	k1gNnSc2	vymizení
této	tento	k3xDgFnSc2	tento
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
snaha	snaha	k1gFnSc1	snaha
zabránit	zabránit	k5eAaPmF	zabránit
těhotenství	těhotenství	k1gNnSc2	těhotenství
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
nemorální	morální	k2eNgMnPc4d1	nemorální
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
používaly	používat	k5eAaImAgInP	používat
řadu	řada	k1gFnSc4	řada
antikoncepčních	antikoncepční	k2eAgNnPc2d1	antikoncepční
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
přerušovanou	přerušovaný	k2eAgFnSc4d1	přerušovaná
soulož	soulož	k1gFnSc4	soulož
nebo	nebo	k8xC	nebo
vkládání	vkládání	k1gNnSc4	vkládání
kořenů	kořen	k1gInPc2	kořen
lilie	lilie	k1gFnSc2	lilie
a	a	k8xC	a
routy	routa	k1gFnSc2	routa
do	do	k7c2	do
vagíny	vagína	k1gFnSc2	vagína
<g/>
.	.	kIx.	.
</s>
<s>
Casanova	Casanův	k2eAgFnSc1d1	Casanova
(	(	kIx(	(
<g/>
1725	[number]	k4	1725
<g/>
–	–	k?	–
<g/>
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
používání	používání	k1gNnSc2	používání
návleku	návlek	k1gInSc2	návlek
z	z	k7c2	z
jehněčího	jehněčí	k2eAgNnSc2d1	jehněčí
střívka	střívko	k1gNnSc2	střívko
pro	pro	k7c4	pro
prevenci	prevence	k1gFnSc4	prevence
otěhotnění	otěhotnění	k1gNnSc2	otěhotnění
<g/>
;	;	kIx,	;
všeobecně	všeobecně	k6eAd1	všeobecně
dostupné	dostupný	k2eAgInPc4d1	dostupný
kondomy	kondom	k1gInPc4	kondom
se	se	k3xPyFc4	se
však	však	k9	však
objevily	objevit	k5eAaPmAgInP	objevit
až	až	k9	až
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
Richard	Richard	k1gMnSc1	Richard
Richter	Richter	k1gMnSc1	Richter
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
první	první	k4xOgNnSc4	první
nitroděložní	nitroděložní	k2eAgNnSc4d1	nitroděložní
tělísko	tělísko	k1gNnSc4	tělísko
ze	z	k7c2	z
střívka	střívko	k1gNnSc2	střívko
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pak	pak	k6eAd1	pak
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dále	daleko	k6eAd2	daleko
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
a	a	k8xC	a
prodával	prodávat	k5eAaImAgMnS	prodávat
Ernst	Ernst	k1gMnSc1	Ernst
Gräfenberg	Gräfenberg	k1gMnSc1	Gräfenberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
Margaret	Margareta	k1gFnPc2	Margareta
Sangerová	Sangerová	k1gFnSc1	Sangerová
otevřela	otevřít	k5eAaPmAgFnS	otevřít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
první	první	k4xOgFnSc4	první
kliniku	klinika	k1gFnSc4	klinika
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
antikoncepcí	antikoncepce	k1gFnSc7	antikoncepce
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
byla	být	k5eAaImAgNnP	být
zatčena	zatčen	k2eAgNnPc1d1	zatčeno
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
příklad	příklad	k1gInSc1	příklad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
následovala	následovat	k5eAaImAgFnS	následovat
první	první	k4xOgFnSc1	první
klinika	klinika	k1gFnSc1	klinika
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
otevřená	otevřený	k2eAgFnSc1d1	otevřená
Marií	Maria	k1gFnSc7	Maria
Stopesovou	Stopesový	k2eAgFnSc7d1	Stopesová
<g/>
.	.	kIx.	.
</s>
<s>
Gregory	Gregor	k1gMnPc4	Gregor
Pincus	Pincus	k1gMnSc1	Pincus
a	a	k8xC	a
John	John	k1gMnSc1	John
Rock	rock	k1gInSc4	rock
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Americké	americký	k2eAgFnSc2d1	americká
federace	federace	k1gFnSc2	federace
pro	pro	k7c4	pro
plánované	plánovaný	k2eAgNnSc4d1	plánované
rodičovství	rodičovství	k1gNnSc4	rodičovství
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
první	první	k4xOgFnPc4	první
antikoncepční	antikoncepční	k2eAgFnPc4d1	antikoncepční
tabletky	tabletka	k1gFnPc4	tabletka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
staly	stát	k5eAaPmAgInP	stát
veřejně	veřejně	k6eAd1	veřejně
dostupnými	dostupný	k2eAgMnPc7d1	dostupný
<g/>
.	.	kIx.	.
</s>
<s>
Potrat	potrat	k1gInSc1	potrat
pomocí	pomocí	k7c2	pomocí
léků	lék	k1gInPc2	lék
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
chirurgickému	chirurgický	k2eAgInSc3d1	chirurgický
potratu	potrat	k1gInSc3	potrat
díky	díky	k7c3	díky
dostupnosti	dostupnost	k1gFnSc3	dostupnost
prostaglandinových	prostaglandinový	k2eAgInPc2d1	prostaglandinový
analogů	analog	k1gInPc2	analog
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
mifepristonu	mifepriston	k1gInSc2	mifepriston
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
metodám	metoda	k1gFnPc3	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
patří	patřit	k5eAaImIp3nS	patřit
bariérové	bariérový	k2eAgFnSc2d1	bariérová
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
hormonální	hormonální	k2eAgFnPc4d1	hormonální
antikoncepce	antikoncepce	k1gFnPc4	antikoncepce
<g/>
,	,	kIx,	,
nitroděložní	nitroděložní	k2eAgNnPc4d1	nitroděložní
tělíska	tělísko	k1gNnPc4	tělísko
<g/>
,	,	kIx,	,
sterilizace	sterilizace	k1gFnPc4	sterilizace
a	a	k8xC	a
metody	metoda	k1gFnPc4	metoda
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
jistém	jistý	k2eAgInSc6d1	jistý
způsobu	způsob	k1gInSc6	způsob
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
před	před	k7c7	před
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nouzová	nouzový	k2eAgFnSc1d1	nouzová
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
obecně	obecně	k6eAd1	obecně
jako	jako	k9	jako
procento	procento	k1gNnSc1	procento
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
otěhotní	otěhotnět	k5eAaPmIp3nP	otěhotnět
při	při	k7c6	při
používání	používání	k1gNnSc6	používání
určité	určitý	k2eAgFnSc2d1	určitá
metody	metoda	k1gFnSc2	metoda
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
vysoce	vysoce	k6eAd1	vysoce
spolehlivých	spolehlivý	k2eAgFnPc2d1	spolehlivá
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
podvázání	podvázání	k1gNnSc1	podvázání
vejcovodů	vejcovod	k1gInPc2	vejcovod
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
jako	jako	k9	jako
četnost	četnost	k1gFnSc4	četnost
selhání	selhání	k1gNnSc2	selhání
během	během	k7c2	během
života	život	k1gInSc2	život
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Nejspolehlivější	spolehlivý	k2eAgFnPc1d3	nejspolehlivější
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
a	a	k8xC	a
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
průběžné	průběžný	k2eAgFnPc4d1	průběžná
návštěvy	návštěva	k1gFnPc4	návštěva
lékaře	lékař	k1gMnSc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Chirurgická	chirurgický	k2eAgFnSc1d1	chirurgická
sterilizace	sterilizace	k1gFnSc1	sterilizace
<g/>
,	,	kIx,	,
implantovaná	implantovaný	k2eAgFnSc1d1	implantovaná
hormonální	hormonální	k2eAgFnPc4d1	hormonální
antikoncepce	antikoncepce	k1gFnPc4	antikoncepce
a	a	k8xC	a
nitroděložní	nitroděložní	k2eAgNnPc4d1	nitroděložní
tělíska	tělísko	k1gNnPc4	tělísko
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
četnost	četnost	k1gFnSc4	četnost
selhání	selhání	k1gNnPc4	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
nižší	nízký	k2eAgFnSc7d2	nižší
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
či	či	k8xC	či
v	v	k7c6	v
případě	případ	k1gInSc6	případ
metody	metoda	k1gFnSc2	metoda
laktační	laktační	k2eAgFnSc2d1	laktační
amenorey	amenorea	k1gFnSc2	amenorea
<g/>
,	,	kIx,	,
LAM	lama	k1gFnPc2	lama
<g/>
,	,	kIx,	,
během	během	k7c2	během
prvních	první	k4xOgMnPc2	první
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
četnost	četnost	k1gFnSc4	četnost
selhání	selhání	k1gNnSc3	selhání
hormonálních	hormonální	k2eAgFnPc2d1	hormonální
antikoncepčních	antikoncepční	k2eAgFnPc2d1	antikoncepční
tablet	tableta	k1gFnPc2	tableta
<g/>
,	,	kIx,	,
náplastí	náplast	k1gFnPc2	náplast
nebo	nebo	k8xC	nebo
kroužků	kroužek	k1gInPc2	kroužek
a	a	k8xC	a
metody	metoda	k1gFnSc2	metoda
laktační	laktační	k2eAgFnSc2d1	laktační
amenorey	amenorea	k1gFnSc2	amenorea
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
důsledně	důsledně	k6eAd1	důsledně
užívány	užíván	k2eAgFnPc1d1	užívána
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
typickém	typický	k2eAgNnSc6d1	typické
používání	používání	k1gNnSc6	používání
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
četnost	četnost	k1gFnSc4	četnost
selhání	selhání	k1gNnPc4	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
9	[number]	k4	9
%	%	kIx~	%
a	a	k8xC	a
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
nesprávné	správný	k2eNgNnSc1d1	nesprávné
používání	používání	k1gNnSc1	používání
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
metody	metoda	k1gFnPc1	metoda
jako	jako	k8xC	jako
přirozené	přirozený	k2eAgNnSc1d1	přirozené
plánování	plánování	k1gNnSc1	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
<g/>
,	,	kIx,	,
kondomy	kondom	k1gInPc1	kondom
<g/>
,	,	kIx,	,
vaginální	vaginální	k2eAgInPc1d1	vaginální
pesary	pesar	k1gInPc1	pesar
a	a	k8xC	a
spermicidy	spermicid	k1gInPc1	spermicid
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
četnost	četnost	k1gFnSc4	četnost
selhání	selhání	k1gNnPc4	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
i	i	k8xC	i
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
určité	určitý	k2eAgInPc4d1	určitý
potenciální	potenciální	k2eAgInPc4d1	potenciální
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
spojeny	spojen	k2eAgInPc4d1	spojen
se	se	k3xPyFc4	se
všemi	všecek	k3xTgFnPc7	všecek
metodami	metoda	k1gFnPc7	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc1	takový
riziko	riziko	k1gNnSc1	riziko
vždy	vždy	k6eAd1	vždy
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
riziko	riziko	k1gNnSc1	riziko
otěhotnění	otěhotnění	k1gNnSc2	otěhotnění
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
metod	metoda	k1gFnPc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ústně	ústně	k6eAd1	ústně
užívané	užívaný	k2eAgFnSc2d1	užívaná
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
,	,	kIx,	,
nitroděložního	nitroděložní	k2eAgNnSc2d1	nitroděložní
tělíska	tělísko	k1gNnSc2	tělísko
<g/>
,	,	kIx,	,
implantátu	implantát	k1gInSc2	implantát
nebo	nebo	k8xC	nebo
injekcí	injekce	k1gFnPc2	injekce
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
přestanou	přestat	k5eAaPmIp3nP	přestat
používat	používat	k5eAaImF	používat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
odstraní	odstranit	k5eAaPmIp3nS	odstranit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
četnost	četnost	k1gFnSc4	četnost
otěhotnění	otěhotnění	k1gNnSc2	otěhotnění
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
nepoužívaly	používat	k5eNaImAgInP	používat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
zdravotními	zdravotní	k2eAgFnPc7d1	zdravotní
potížemi	potíž	k1gFnPc7	potíž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
některých	některý	k3yIgFnPc2	některý
forem	forma	k1gFnPc2	forma
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
nezbytné	zbytný	k2eNgNnSc4d1	nezbytné
další	další	k2eAgNnSc4d1	další
lékařské	lékařský	k2eAgNnSc4d1	lékařské
vyšetření	vyšetření	k1gNnSc4	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
žena	žena	k1gFnSc1	žena
jinak	jinak	k6eAd1	jinak
zdravá	zdravý	k2eAgFnSc1d1	zdravá
<g/>
,	,	kIx,	,
nemělo	mít	k5eNaImAgNnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
metod	metoda	k1gFnPc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
antikoncepčních	antikoncepční	k2eAgFnPc2d1	antikoncepční
tablet	tableta	k1gFnPc2	tableta
<g/>
,	,	kIx,	,
injekcí	injekce	k1gFnPc2	injekce
<g/>
,	,	kIx,	,
implantátů	implantát	k1gInPc2	implantát
nebo	nebo	k8xC	nebo
kondomu	kondom	k1gInSc2	kondom
<g/>
,	,	kIx,	,
lékařské	lékařský	k2eAgNnSc1d1	lékařské
vyšetření	vyšetření	k1gNnSc1	vyšetření
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
gynekologické	gynekologický	k2eAgNnSc1d1	gynekologické
vyšetření	vyšetření	k1gNnSc1	vyšetření
<g/>
,	,	kIx,	,
vyšetření	vyšetření	k1gNnSc1	vyšetření
prsou	prsa	k1gNnPc2	prsa
a	a	k8xC	a
krevní	krevní	k2eAgInPc1d1	krevní
testy	test	k1gInPc1	test
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
užívání	užívání	k1gNnSc2	užívání
antikoncepčních	antikoncepční	k2eAgFnPc2d1	antikoncepční
tablet	tableta	k1gFnPc2	tableta
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výsledek	výsledek	k1gInSc4	výsledek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
nutná	nutný	k2eAgNnPc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
podrobný	podrobný	k2eAgInSc4d1	podrobný
seznam	seznam	k1gInSc4	seznam
medicínských	medicínský	k2eAgNnPc2d1	medicínské
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
typů	typ	k1gInPc2	typ
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
splněna	splněn	k2eAgFnSc1d1	splněna
<g/>
.	.	kIx.	.
</s>
<s>
Hormonální	hormonální	k2eAgFnSc1d1	hormonální
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
působí	působit	k5eAaImIp3nS	působit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
brání	bránit	k5eAaImIp3nS	bránit
ovulaci	ovulace	k1gFnSc4	ovulace
a	a	k8xC	a
početí	početí	k1gNnSc4	početí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jako	jako	k9	jako
tablety	tablet	k1gInPc1	tablet
užívané	užívaný	k2eAgInPc1d1	užívaný
ústně	ústně	k6eAd1	ústně
<g/>
,	,	kIx,	,
podkožní	podkožní	k2eAgInPc4d1	podkožní
implantáty	implantát	k1gInPc4	implantát
<g/>
,	,	kIx,	,
injekce	injekce	k1gFnPc4	injekce
<g/>
,	,	kIx,	,
náplasti	náplast	k1gFnPc4	náplast
<g/>
,	,	kIx,	,
nitroděložní	nitroděložní	k2eAgNnPc4d1	nitroděložní
tělíska	tělísko	k1gNnPc4	tělísko
nebo	nebo	k8xC	nebo
vaginální	vaginální	k2eAgInPc4d1	vaginální
kroužky	kroužek	k1gInPc4	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
antikoncepce	antikoncepce	k1gFnPc1	antikoncepce
užívané	užívaný	k2eAgFnPc1d1	užívaná
ústně	ústně	k6eAd1	ústně
-	-	kIx~	-
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
orální	orální	k2eAgFnSc1d1	orální
kontracepce	kontracepce	k1gFnSc1	kontracepce
a	a	k8xC	a
tablety	tablet	k1gInPc1	tablet
obsahující	obsahující	k2eAgInPc1d1	obsahující
pouze	pouze	k6eAd1	pouze
progestogen	progestogen	k1gInSc4	progestogen
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
užívány	užívat	k5eAaImNgInP	užívat
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
,	,	kIx,	,
nezvyšují	zvyšovat	k5eNaImIp3nP	zvyšovat
riziko	riziko	k1gNnSc4	riziko
potratu	potrat	k1gInSc2	potrat
ani	ani	k8xC	ani
nezpůsobují	způsobovat	k5eNaImIp3nP	způsobovat
vrozené	vrozený	k2eAgFnPc4d1	vrozená
vady	vada	k1gFnPc4	vada
<g/>
.	.	kIx.	.
</s>
<s>
Preparáty	preparát	k1gInPc1	preparát
působí	působit	k5eAaImIp3nP	působit
několika	několik	k4yIc7	několik
mechanismy	mechanismus	k1gInPc1	mechanismus
–	–	k?	–
primárním	primární	k2eAgMnPc3d1	primární
je	být	k5eAaImIp3nS	být
inhibice	inhibice	k1gFnPc4	inhibice
ovulace	ovulace	k1gFnSc2	ovulace
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgInPc4d1	sekundární
účinky	účinek	k1gInPc4	účinek
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
preimplantační	preimplantační	k2eAgFnSc4d1	preimplantační
(	(	kIx(	(
<g/>
zahušťují	zahušťovat	k5eAaImIp3nP	zahušťovat
hlen	hlen	k1gInSc4	hlen
<g/>
,	,	kIx,	,
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
motilitu	motilita	k1gFnSc4	motilita
vejcovodů	vejcovod	k1gInPc2	vejcovod
a	a	k8xC	a
řasinkového	řasinkový	k2eAgInSc2d1	řasinkový
epitelu	epitel	k1gInSc2	epitel
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
periimplantační	periimplantační	k2eAgFnSc1d1	periimplantační
(	(	kIx(	(
<g/>
ovlivněním	ovlivnění	k1gNnSc7	ovlivnění
endometria	endometrium	k1gNnSc2	endometrium
brání	bránit	k5eAaImIp3nP	bránit
nidaci	nidace	k1gFnSc4	nidace
<g/>
)	)	kIx)	)
a	a	k8xC	a
postimplantační	postimplantační	k2eAgFnSc1d1	postimplantační
(	(	kIx(	(
<g/>
nezabrání	zabránit	k5eNaPmIp3nS	zabránit
samotné	samotný	k2eAgFnSc3d1	samotná
nidaci	nidace	k1gFnSc3	nidace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udržení	udržení	k1gNnSc1	udržení
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Periimplantační	Periimplantační	k2eAgInPc1d1	Periimplantační
a	a	k8xC	a
postimplantační	postimplantační	k2eAgInPc1d1	postimplantační
sekundární	sekundární	k2eAgInPc1d1	sekundární
účinky	účinek	k1gInPc4	účinek
jsou	být	k5eAaImIp3nP	být
postfertilizační	postfertilizační	k2eAgMnPc1d1	postfertilizační
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
abortivní	abortivní	k2eAgInSc1d1	abortivní
(	(	kIx(	(
<g/>
způsobující	způsobující	k2eAgInSc1d1	způsobující
potrat	potrat	k1gInSc1	potrat
<g/>
)	)	kIx)	)
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
nich	on	k3xPp3gFnPc6	on
k	k	k7c3	k
odumření	odumření	k1gNnSc3	odumření
oplodněného	oplodněný	k2eAgNnSc2d1	oplodněné
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
<g/>
Preparáty	preparát	k1gInPc1	preparát
působí	působit	k5eAaImIp3nP	působit
několika	několik	k4yIc7	několik
mechanismy	mechanismus	k1gInPc1	mechanismus
–	–	k?	–
primárním	primární	k2eAgMnPc3d1	primární
je	být	k5eAaImIp3nS	být
inhibice	inhibice	k1gFnPc4	inhibice
ovulace	ovulace	k1gFnSc2	ovulace
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgInPc4d1	sekundární
účinky	účinek	k1gInPc4	účinek
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
preimplantační	preimplantační	k2eAgFnSc4d1	preimplantační
(	(	kIx(	(
<g/>
zahušťují	zahušťovat	k5eAaImIp3nP	zahušťovat
hlen	hlen	k1gInSc4	hlen
<g/>
,	,	kIx,	,
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
motilitu	motilita	k1gFnSc4	motilita
vejcovodů	vejcovod	k1gInPc2	vejcovod
a	a	k8xC	a
řasinkového	řasinkový	k2eAgInSc2d1	řasinkový
epitelu	epitel	k1gInSc2	epitel
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
periimplantační	periimplantační	k2eAgFnSc1d1	periimplantační
(	(	kIx(	(
<g/>
ovlivněním	ovlivnění	k1gNnSc7	ovlivnění
endometria	endometrium	k1gNnSc2	endometrium
brání	bránit	k5eAaImIp3nP	bránit
nidaci	nidace	k1gFnSc4	nidace
<g/>
)	)	kIx)	)
a	a	k8xC	a
postimplantační	postimplantační	k2eAgFnSc1d1	postimplantační
(	(	kIx(	(
<g/>
nezabrání	zabránit	k5eNaPmIp3nS	zabránit
samotné	samotný	k2eAgFnSc3d1	samotná
nidaci	nidace	k1gFnSc3	nidace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udržení	udržení	k1gNnSc1	udržení
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Periimplantační	Periimplantační	k2eAgInPc1d1	Periimplantační
a	a	k8xC	a
postimplantační	postimplantační	k2eAgInPc1d1	postimplantační
sekundární	sekundární	k2eAgInPc1d1	sekundární
účinky	účinek	k1gInPc4	účinek
jsou	být	k5eAaImIp3nP	být
postfertilizační	postfertilizační	k2eAgMnPc1d1	postfertilizační
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
abortivní	abortivní	k2eAgInSc1d1	abortivní
(	(	kIx(	(
<g/>
způsobující	způsobující	k2eAgInSc1d1	způsobující
potrat	potrat	k1gInSc1	potrat
<g/>
)	)	kIx)	)
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
nich	on	k3xPp3gFnPc6	on
k	k	k7c3	k
odumření	odumření	k1gNnSc3	odumření
oplodněného	oplodněný	k2eAgNnSc2d1	oplodněné
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kombinovanou	kombinovaný	k2eAgFnSc7d1	kombinovaná
hormonální	hormonální	k2eAgFnSc7d1	hormonální
antikoncepcí	antikoncepce	k1gFnSc7	antikoncepce
je	být	k5eAaImIp3nS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
mírně	mírně	k6eAd1	mírně
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
riziko	riziko	k1gNnSc1	riziko
žilní	žilní	k2eAgFnSc2d1	žilní
a	a	k8xC	a
arteriální	arteriální	k2eAgFnSc2d1	arteriální
trombózy	trombóza	k1gFnSc2	trombóza
(	(	kIx(	(
<g/>
tvorby	tvorba	k1gFnPc1	tvorba
krevních	krevní	k2eAgFnPc2d1	krevní
sraženin	sraženina	k1gFnPc2	sraženina
v	v	k7c6	v
žilách	žíla	k1gFnPc6	žíla
nebo	nebo	k8xC	nebo
v	v	k7c6	v
tepnách	tepna	k1gFnPc6	tepna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc4	tento
riziko	riziko	k1gNnSc4	riziko
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
riziko	riziko	k1gNnSc1	riziko
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
otěhotněním	otěhotnění	k1gNnSc7	otěhotnění
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
ženám	žena	k1gFnPc3	žena
starším	starý	k2eAgFnPc3d2	starší
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepřestanou	přestat	k5eNaPmIp3nP	přestat
kouřit	kouřit	k5eAaImF	kouřit
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
touhu	touha	k1gFnSc4	touha
je	být	k5eAaImIp3nS	být
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
touha	touha	k1gFnSc1	touha
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
ale	ale	k8xC	ale
žádný	žádný	k3yNgInSc1	žádný
vliv	vliv	k1gInSc1	vliv
nepociťuje	pociťovat	k5eNaImIp3nS	pociťovat
<g/>
.	.	kIx.	.
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
užívaná	užívaný	k2eAgFnSc1d1	užívaná
ústně	ústně	k6eAd1	ústně
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc4	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
vaječníku	vaječník	k1gInSc2	vaječník
a	a	k8xC	a
rakoviny	rakovina	k1gFnSc2	rakovina
dělohy	děloha	k1gFnSc2	děloha
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
riziko	riziko	k1gNnSc4	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
sporná	sporný	k2eAgFnSc1d1	sporná
a	a	k8xC	a
výsledky	výsledek	k1gInPc4	výsledek
studií	studie	k1gFnPc2	studie
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
jsou	být	k5eAaImIp3nP	být
nekonzistentní	konzistentní	k2eNgInPc1d1	nekonzistentní
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
také	také	k9	také
omezuje	omezovat	k5eAaImIp3nS	omezovat
menstruační	menstruační	k2eAgNnSc4d1	menstruační
krvácení	krvácení	k1gNnSc4	krvácení
a	a	k8xC	a
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
bolestivé	bolestivý	k2eAgFnPc4d1	bolestivá
křeče	křeč	k1gFnPc4	křeč
při	při	k7c6	při
menstruaci	menstruace	k1gFnSc6	menstruace
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnPc1d2	nižší
dávky	dávka	k1gFnPc1	dávka
estrogenu	estrogen	k1gInSc2	estrogen
ve	v	k7c6	v
vaginálním	vaginální	k2eAgInSc6d1	vaginální
kroužku	kroužek	k1gInSc6	kroužek
mohou	moct	k5eAaImIp3nP	moct
snižovat	snižovat	k5eAaImF	snižovat
riziko	riziko	k1gNnSc4	riziko
citlivosti	citlivost	k1gFnSc2	citlivost
prsou	prsa	k1gNnPc2	prsa
<g/>
,	,	kIx,	,
nauzey	nauzea	k1gFnPc1	nauzea
a	a	k8xC	a
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgInP	spojovat
s	s	k7c7	s
přípravky	přípravek	k1gInPc7	přípravek
obsahujícími	obsahující	k2eAgInPc7d1	obsahující
vyšší	vysoký	k2eAgFnSc4d2	vyšší
dávku	dávka	k1gFnSc4	dávka
estrogenu	estrogen	k1gInSc2	estrogen
<g/>
.	.	kIx.	.
</s>
<s>
Progestinové	Progestinový	k2eAgFnPc4d1	Progestinový
tablety	tableta	k1gFnPc4	tableta
<g/>
,	,	kIx,	,
injekce	injekce	k1gFnPc4	injekce
a	a	k8xC	a
nitroděložní	nitroděložní	k2eAgNnPc4d1	nitroděložní
tělíska	tělísko	k1gNnPc4	tělísko
riziko	riziko	k1gNnSc1	riziko
tvorby	tvorba	k1gFnSc2	tvorba
krevních	krevní	k2eAgFnPc2d1	krevní
sraženin	sraženina	k1gFnPc2	sraženina
nezvyšují	zvyšovat	k5eNaImIp3nP	zvyšovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	on	k3xPp3gInPc4	on
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
i	i	k9	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
trpěly	trpět	k5eAaImAgFnP	trpět
krevními	krevní	k2eAgFnPc7d1	krevní
sraženinami	sraženina	k1gFnPc7	sraženina
v	v	k7c6	v
žilách	žíla	k1gFnPc6	žíla
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
tvorba	tvorba	k1gFnSc1	tvorba
krevních	krevní	k2eAgFnPc2d1	krevní
sraženin	sraženina	k1gFnPc2	sraženina
v	v	k7c6	v
tepnách	tepna	k1gFnPc6	tepna
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
používat	používat	k5eAaImF	používat
nehormonální	hormonální	k2eNgFnSc4d1	nehormonální
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
nebo	nebo	k8xC	nebo
metodu	metoda	k1gFnSc4	metoda
založenou	založený	k2eAgFnSc4d1	založená
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
progestinu	progestina	k1gFnSc4	progestina
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
injekční	injekční	k2eAgFnSc2d1	injekční
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Progestinové	Progestinový	k2eAgFnPc1d1	Progestinový
tablety	tableta	k1gFnPc1	tableta
mohou	moct	k5eAaImIp3nP	moct
zmírňovat	zmírňovat	k5eAaImF	zmírňovat
příznaky	příznak	k1gInPc1	příznak
během	během	k7c2	během
menstruace	menstruace	k1gFnSc2	menstruace
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
je	on	k3xPp3gFnPc4	on
užívat	užívat	k5eAaImF	užívat
i	i	k9	i
kojící	kojící	k2eAgFnPc4d1	kojící
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
tvorbu	tvorba	k1gFnSc4	tvorba
mateřského	mateřský	k2eAgNnSc2d1	mateřské
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
používání	používání	k1gNnSc6	používání
metod	metoda	k1gFnPc2	metoda
založených	založený	k2eAgFnPc2d1	založená
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
progestinu	progestin	k1gInSc6	progestin
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
nepravidelnému	pravidelný	k2eNgNnSc3d1	nepravidelné
menstruačnímu	menstruační	k2eAgNnSc3d1	menstruační
krvácení	krvácení	k1gNnSc3	krvácení
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
k	k	k7c3	k
zástavě	zástava	k1gFnSc3	zástava
menstruace	menstruace	k1gFnSc2	menstruace
<g/>
.	.	kIx.	.
</s>
<s>
Progestiny	Progestin	k1gInPc1	Progestin
drospirenon	drospirenona	k1gFnPc2	drospirenona
a	a	k8xC	a
desogestrel	desogestrela	k1gFnPc2	desogestrela
minimalizují	minimalizovat	k5eAaBmIp3nP	minimalizovat
androgenní	androgenní	k2eAgInPc1d1	androgenní
vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zároveň	zároveň	k6eAd1	zároveň
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
riziko	riziko	k1gNnSc4	riziko
tvorby	tvorba	k1gFnSc2	tvorba
krevních	krevní	k2eAgFnPc2d1	krevní
sraženin	sraženina	k1gFnPc2	sraženina
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nepatří	patřit	k5eNaImIp3nS	patřit
k	k	k7c3	k
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
první	první	k4xOgFnSc2	první
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
progestinu	progestin	k1gInSc2	progestin
Depo-Provera	Depo-Provero	k1gNnSc2	Depo-Provero
aplikovaného	aplikovaný	k2eAgNnSc2d1	aplikované
formou	forma	k1gFnSc7	forma
injekcí	injekce	k1gFnPc2	injekce
se	se	k3xPyFc4	se
odborníci	odborník	k1gMnPc1	odborník
neshodují	shodovat	k5eNaImIp3nP	shodovat
na	na	k7c4	na
obvyklé	obvyklý	k2eAgFnPc4d1	obvyklá
četnosti	četnost	k1gFnPc4	četnost
selhání	selhání	k1gNnSc2	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
kolísají	kolísat	k5eAaImIp3nP	kolísat
od	od	k7c2	od
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
až	až	k6eAd1	až
do	do	k7c2	do
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Bariérová	bariérový	k2eAgFnSc1d1	bariérová
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
jsou	být	k5eAaImIp3nP	být
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
bránit	bránit	k5eAaImF	bránit
početí	početí	k1gNnSc4	početí
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzicky	fyzicky	k6eAd1	fyzicky
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
pronikání	pronikání	k1gNnSc4	pronikání
spermatu	sperma	k1gNnSc2	sperma
do	do	k7c2	do
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
mužský	mužský	k2eAgInSc4d1	mužský
kondom	kondom	k1gInSc4	kondom
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
kondom	kondom	k1gInSc4	kondom
<g/>
,	,	kIx,	,
cervikální	cervikální	k2eAgInSc1d1	cervikální
klobouček	klobouček	k1gInSc1	klobouček
<g/>
,	,	kIx,	,
pesar	pesar	k1gInSc1	pesar
a	a	k8xC	a
antikoncepční	antikoncepční	k2eAgFnSc1d1	antikoncepční
houbička	houbička	k1gFnSc1	houbička
se	s	k7c7	s
spermicidem	spermicid	k1gInSc7	spermicid
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
metodou	metoda	k1gFnSc7	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
je	být	k5eAaImIp3nS	být
kondom	kondom	k1gInSc1	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Mužský	mužský	k2eAgInSc1d1	mužský
kondom	kondom	k1gInSc1	kondom
se	se	k3xPyFc4	se
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
na	na	k7c4	na
mužův	mužův	k2eAgInSc4d1	mužův
ztopořený	ztopořený	k2eAgInSc4d1	ztopořený
penis	penis	k1gInSc4	penis
<g/>
.	.	kIx.	.
</s>
<s>
Fyzicky	fyzicky	k6eAd1	fyzicky
brání	bránit	k5eAaImIp3nS	bránit
proniknutí	proniknutí	k1gNnSc1	proniknutí
spermatu	sperma	k1gNnSc2	sperma
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
sexuálního	sexuální	k2eAgMnSc4d1	sexuální
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
kondomy	kondom	k1gInPc1	kondom
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
latexu	latex	k1gInSc2	latex
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
výrobě	výroba	k1gFnSc3	výroba
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
materiály	materiál	k1gInPc4	materiál
jako	jako	k8xS	jako
polyuretan	polyuretan	k1gInSc4	polyuretan
nebo	nebo	k8xC	nebo
jehněčí	jehněčí	k2eAgNnSc4d1	jehněčí
střívko	střívko	k1gNnSc4	střívko
<g/>
.	.	kIx.	.
<g/>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
ženský	ženský	k2eAgInSc4d1	ženský
kondom	kondom	k1gInSc4	kondom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
nitrilu	nitril	k1gInSc2	nitril
<g/>
,	,	kIx,	,
latexu	latex	k1gInSc2	latex
nebo	nebo	k8xC	nebo
polyuretanu	polyuretan	k1gInSc2	polyuretan
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
mužských	mužský	k2eAgInPc2d1	mužský
kondomů	kondom	k1gInPc2	kondom
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
levné	levný	k2eAgFnPc1d1	levná
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
používá	používat	k5eAaImIp3nS	používat
kondom	kondom	k1gInSc1	kondom
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
%	%	kIx~	%
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
chrání	chránit	k5eAaImIp3nP	chránit
před	před	k7c7	před
početím	početí	k1gNnSc7	početí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
25	[number]	k4	25
%	%	kIx~	%
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
18	[number]	k4	18
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
četnost	četnost	k1gFnSc1	četnost
selhání	selhání	k1gNnSc2	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mužských	mužský	k2eAgInPc2d1	mužský
kondomů	kondom	k1gInPc2	kondom
15	[number]	k4	15
%	%	kIx~	%
a	a	k8xC	a
u	u	k7c2	u
vaginálních	vaginální	k2eAgInPc2d1	vaginální
pesarů	pesar	k1gInPc2	pesar
se	s	k7c7	s
spermicidem	spermicid	k1gInSc7	spermicid
16	[number]	k4	16
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správném	správný	k2eAgNnSc6d1	správné
používání	používání	k1gNnSc6	používání
jsou	být	k5eAaImIp3nP	být
spolehlivější	spolehlivý	k2eAgInPc1d2	spolehlivější
kondomy	kondom	k1gInPc1	kondom
s	s	k7c7	s
2	[number]	k4	2
<g/>
%	%	kIx~	%
četností	četnost	k1gFnPc2	četnost
selhání	selhání	k1gNnSc2	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
četnost	četnost	k1gFnSc4	četnost
selhání	selhání	k1gNnSc2	selhání
vaginálního	vaginální	k2eAgInSc2d1	vaginální
pesaru	pesar	k1gInSc2	pesar
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kondom	kondom	k1gInSc1	kondom
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
ještě	ještě	k6eAd1	ještě
tu	ten	k3xDgFnSc4	ten
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
bránit	bránit	k5eAaImF	bránit
šíření	šíření	k1gNnSc4	šíření
některých	některý	k3yIgFnPc2	některý
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
infekcí	infekce	k1gFnPc2	infekce
jako	jako	k8xS	jako
například	například	k6eAd1	například
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Antikoncepční	antikoncepční	k2eAgFnSc1d1	antikoncepční
houbička	houbička	k1gFnSc1	houbička
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
bariérové	bariérový	k2eAgFnSc2d1	bariérová
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
se	s	k7c7	s
spermicidem	spermicid	k1gInSc7	spermicid
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
pesar	pesar	k1gInSc1	pesar
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
vagíny	vagína	k1gFnSc2	vagína
před	před	k7c7	před
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
až	až	k9	až
před	před	k7c4	před
děložní	děložní	k2eAgInSc4d1	děložní
čípek	čípek	k1gInSc4	čípek
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
četnost	četnost	k1gFnSc1	četnost
selhání	selhání	k1gNnSc2	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
žena	žena	k1gFnSc1	žena
už	už	k6eAd1	už
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
rodila	rodit	k5eAaImAgFnS	rodit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
už	už	k6eAd1	už
rodily	rodit	k5eAaImAgFnP	rodit
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
četnost	četnost	k1gFnSc1	četnost
selhání	selhání	k1gNnSc1	selhání
24	[number]	k4	24
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosud	dosud	k6eAd1	dosud
nerodily	rodit	k5eNaImAgFnP	rodit
<g/>
,	,	kIx,	,
12	[number]	k4	12
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Houbičku	houbička	k1gFnSc4	houbička
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zavést	zavést	k5eAaPmF	zavést
až	až	k9	až
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
před	před	k7c7	před
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
a	a	k8xC	a
ve	v	k7c6	v
vagíně	vagína	k1gFnSc6	vagína
musí	muset	k5eAaImIp3nS	muset
zůstat	zůstat	k5eAaPmF	zůstat
aspoň	aspoň	k9	aspoň
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
byly	být	k5eAaImAgFnP	být
alergické	alergický	k2eAgFnPc1d1	alergická
reakce	reakce	k1gFnPc1	reakce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
některé	některý	k3yIgInPc4	některý
závažnější	závažný	k2eAgInPc4d2	závažnější
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
jako	jako	k8xS	jako
syndrom	syndrom	k1gInSc4	syndrom
toxického	toxický	k2eAgInSc2d1	toxický
šoku	šok	k1gInSc2	šok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
přišel	přijít	k5eAaPmAgInS	přijít
na	na	k7c4	na
domácí	domácí	k2eAgInSc4d1	domácí
trh	trh	k1gInSc4	trh
původní	původní	k2eAgInSc4d1	původní
český	český	k2eAgInSc4d1	český
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
tělísko	tělísko	k1gNnSc4	tělísko
DANA	Dan	k1gMnSc2	Dan
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
ve	v	k7c6	v
Státním	státní	k2eAgInSc6d1	státní
výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
textilním	textilní	k2eAgNnSc7d1	textilní
-	-	kIx~	-
Centrum	centrum	k1gNnSc1	centrum
radiačních	radiační	k2eAgFnPc2d1	radiační
technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
SVÚT-CRT	SVÚT-CRT	k1gFnSc1	SVÚT-CRT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Václavská	václavský	k2eAgFnSc1d1	Václavská
6	[number]	k4	6
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
Veverské	Veverský	k2eAgFnSc6d1	Veverská
Bítýšce	Bítýška	k1gFnSc6	Bítýška
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
byly	být	k5eAaImAgInP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
také	také	k9	také
zavaděče	zavaděč	k1gInPc4	zavaděč
a	a	k8xC	a
měrky	měrka	k1gFnPc4	měrka
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
státního	státní	k2eAgInSc2d1	státní
úkolu	úkol	k1gInSc2	úkol
a	a	k8xC	a
řešitelského	řešitelský	k2eAgInSc2d1	řešitelský
týmu	tým	k1gInSc2	tým
byla	být	k5eAaImAgFnS	být
RNDr.	RNDr.	kA	RNDr.
Dana	Dana	k1gFnSc1	Dana
Provazníková	Provazníková	k1gFnSc1	Provazníková
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yQgFnSc6	který
dostala	dostat	k5eAaPmAgFnS	dostat
tělíska	tělísko	k1gNnSc2	tělísko
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
nejen	nejen	k6eAd1	nejen
různé	různý	k2eAgFnPc1d1	různá
velikosti	velikost	k1gFnPc1	velikost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vybrat	vybrat	k5eAaPmF	vybrat
ten	ten	k3xDgInSc4	ten
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
tělíska	tělísko	k1gNnPc4	tělísko
přidán	přidán	k2eAgInSc4d1	přidán
měděný	měděný	k2eAgInSc4d1	měděný
kroužek	kroužek	k1gInSc4	kroužek
nebo	nebo	k8xC	nebo
trubička	trubička	k1gFnSc1	trubička
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
antimikrobiální	antimikrobiální	k2eAgNnPc1d1	antimikrobiální
opatření	opatření	k1gNnPc1	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Tělíska	tělísko	k1gNnPc1	tělísko
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
a	a	k8xC	a
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
povolení	povolení	k1gNnSc2	povolení
od	od	k7c2	od
SÚKL	SÚKL	kA	SÚKL
sériově	sériově	k6eAd1	sériově
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
Gama	gama	k1gNnSc2	gama
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
klinického	klinický	k2eAgNnSc2d1	klinické
testování	testování	k1gNnSc2	testování
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgNnPc4	tento
tělíska	tělísko	k1gNnPc4	tělísko
určena	určit	k5eAaPmNgNnP	určit
pro	pro	k7c4	pro
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
antikoncepční	antikoncepční	k2eAgFnSc4d1	antikoncepční
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
dobu	doba	k1gFnSc4	doba
předepisoval	předepisovat	k5eAaImAgMnS	předepisovat
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kontroly	kontrola	k1gFnSc2	kontrola
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
<g/>
x	x	k?	x
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Předepsaného	předepsaný	k2eAgInSc2d1	předepsaný
rozsahu	rozsah	k1gInSc2	rozsah
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
desítky	desítka	k1gFnPc1	desítka
doktorů	doktor	k1gMnPc2	doktor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
dva	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
mylně	mylně	k6eAd1	mylně
uváděni	uvádět	k5eAaImNgMnP	uvádět
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odkazu	odkaz	k1gInSc2	odkaz
[	[	kIx(	[
<g/>
60	[number]	k4	60
<g/>
]	]	kIx)	]
jako	jako	k8xC	jako
autoři	autor	k1gMnPc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnPc1d1	moderní
nitroděložní	nitroděložní	k2eAgNnPc1d1	nitroděložní
tělíska	tělísko	k1gNnPc1	tělísko
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
tělíska	tělísko	k1gNnPc4	tělísko
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
T	T	kA	T
obsahující	obsahující	k2eAgInSc1d1	obsahující
měděný	měděný	k2eAgInSc1d1	měděný
drátek	drátek	k1gInSc1	drátek
nebo	nebo	k8xC	nebo
levonorgestrel	levonorgestrel	k1gInSc1	levonorgestrel
<g/>
.	.	kIx.	.
</s>
<s>
Zavádějí	zavádět	k5eAaImIp3nP	zavádět
se	se	k3xPyFc4	se
do	do	k7c2	do
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
forma	forma	k1gFnSc1	forma
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
účinkující	účinkující	k2eAgFnSc1d1	účinkující
reverzibilní	reverzibilní	k2eAgFnSc1d1	reverzibilní
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
selhání	selhání	k1gNnSc2	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
měděných	měděný	k2eAgNnPc2d1	měděné
nitroděložních	nitroděložní	k2eAgNnPc2d1	nitroděložní
tělísek	tělísko	k1gNnPc2	tělísko
kolem	kolem	k7c2	kolem
0,8	[number]	k4	0,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
nitroděložních	nitroděložní	k2eAgNnPc2d1	nitroděložní
tělísek	tělísko	k1gNnPc2	tělísko
s	s	k7c7	s
levonorgestrelem	levonorgestrel	k1gInSc7	levonorgestrel
0,2	[number]	k4	0,2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
patří	patřit	k5eAaImIp3nS	patřit
společně	společně	k6eAd1	společně
s	s	k7c7	s
antikoncepčními	antikoncepční	k2eAgInPc7d1	antikoncepční
implantáty	implantát	k1gInPc7	implantát
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
jsou	být	k5eAaImIp3nP	být
uživatelky	uživatelka	k1gFnPc1	uživatelka
nejspokojenější	spokojený	k2eAgFnPc1d3	nejspokojenější
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
u	u	k7c2	u
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
i	i	k8xC	i
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosud	dosud	k6eAd1	dosud
nerodily	rodit	k5eNaImAgFnP	rodit
<g/>
.	.	kIx.	.
</s>
<s>
Nitroděložní	nitroděložní	k2eAgNnPc1d1	nitroděložní
tělíska	tělísko	k1gNnPc1	tělísko
nemají	mít	k5eNaImIp3nP	mít
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
kojení	kojení	k1gNnSc4	kojení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
zavádět	zavádět	k5eAaImF	zavádět
hned	hned	k6eAd1	hned
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
také	také	k9	také
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
potratu	potrat	k1gInSc6	potrat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
se	se	k3xPyFc4	se
plodnost	plodnost	k1gFnSc1	plodnost
okamžitě	okamžitě	k6eAd1	okamžitě
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c4	na
normální	normální	k2eAgFnSc4d1	normální
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
po	po	k7c6	po
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
používání	používání	k1gNnSc6	používání
<g/>
.	.	kIx.	.
</s>
<s>
Měděná	měděný	k2eAgNnPc1d1	měděné
nitroděložní	nitroděložní	k2eAgNnPc1d1	nitroděložní
tělíska	tělísko	k1gNnPc1	tělísko
mohou	moct	k5eAaImIp3nP	moct
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
menstruační	menstruační	k2eAgNnSc4d1	menstruační
krvácení	krvácení	k1gNnSc4	krvácení
a	a	k8xC	a
způsobovat	způsobovat	k5eAaImF	způsobovat
bolestivější	bolestivý	k2eAgFnPc4d2	bolestivější
křeče	křeč	k1gFnPc4	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Hormonální	hormonální	k2eAgNnPc1d1	hormonální
nitroděložní	nitroděložní	k2eAgNnPc1d1	nitroděložní
tělíska	tělísko	k1gNnPc1	tělísko
mohou	moct	k5eAaImIp3nP	moct
menstruační	menstruační	k2eAgNnSc4d1	menstruační
krvácení	krvácení	k1gNnSc4	krvácení
naopak	naopak	k6eAd1	naopak
potlačovat	potlačovat	k5eAaImF	potlačovat
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
možným	možný	k2eAgFnPc3d1	možná
komplikacím	komplikace	k1gFnPc3	komplikace
patří	patřit	k5eAaImIp3nS	patřit
vypuzení	vypuzení	k1gNnSc1	vypuzení
(	(	kIx(	(
<g/>
ve	v	k7c6	v
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
perforace	perforace	k1gFnPc1	perforace
(	(	kIx(	(
<g/>
proděravění	proděravění	k1gNnPc1	proděravění
<g/>
)	)	kIx)	)
dělohy	děloha	k1gFnSc2	děloha
(	(	kIx(	(
<g/>
v	v	k7c6	v
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,7	[number]	k4	0,7
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křeče	křeč	k1gFnPc4	křeč
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
léčit	léčit	k5eAaImF	léčit
nesteroidními	steroidní	k2eNgFnPc7d1	nesteroidní
antiflogistiky	antiflogistika	k1gFnPc5	antiflogistika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
nitroděložní	nitroděložní	k2eAgFnSc1d1	nitroděložní
tělíska	tělísko	k1gNnSc2	tělísko
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
reverzibilní	reverzibilní	k2eAgFnSc7d1	reverzibilní
metodou	metoda	k1gFnSc7	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
180	[number]	k4	180
miliony	milion	k4xCgInPc1	milion
uživatelek	uživatelka	k1gFnPc2	uživatelka
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInSc1d1	dřívější
model	model	k1gInSc1	model
nitroděložního	nitroděložní	k2eAgNnSc2d1	nitroděložní
tělíska	tělísko	k1gNnSc2	tělísko
(	(	kIx(	(
<g/>
Dalkonův	Dalkonův	k2eAgInSc1d1	Dalkonův
štít	štít	k1gInSc1	štít
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
rizikem	riziko	k1gNnSc7	riziko
hlubokého	hluboký	k2eAgInSc2d1	hluboký
pánevního	pánevní	k2eAgInSc2d1	pánevní
zánětu	zánět	k1gInSc2	zánět
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stávající	stávající	k2eAgInPc4d1	stávající
modely	model	k1gInPc4	model
toto	tento	k3xDgNnSc1	tento
riziko	riziko	k1gNnSc1	riziko
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zavedení	zavedení	k1gNnSc2	zavedení
netrpěly	trpět	k5eNaImAgFnP	trpět
sexuálně	sexuálně	k6eAd1	sexuálně
přenosnou	přenosný	k2eAgFnSc7d1	přenosná
infekcí	infekce	k1gFnSc7	infekce
<g/>
,	,	kIx,	,
nijak	nijak	k6eAd1	nijak
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Chirurgická	chirurgický	k2eAgFnSc1d1	chirurgická
sterilizace	sterilizace	k1gFnSc1	sterilizace
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
formou	forma	k1gFnSc7	forma
podvázání	podvázání	k1gNnSc2	podvázání
vejcovodů	vejcovod	k1gInPc2	vejcovod
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
vasektomie	vasektomie	k1gFnSc2	vasektomie
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgNnSc1	žádný
významné	významný	k2eAgInPc4d1	významný
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
podvázání	podvázání	k1gNnPc4	podvázání
vejcovodů	vejcovod	k1gInPc2	vejcovod
naopak	naopak	k6eAd1	naopak
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc4	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
vaječníku	vaječník	k1gInSc2	vaječník
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
krátkodobých	krátkodobý	k2eAgFnPc2d1	krátkodobá
komplikací	komplikace	k1gFnPc2	komplikace
je	být	k5eAaImIp3nS	být
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
nižší	nízký	k2eAgFnSc1d2	nižší
u	u	k7c2	u
vasektomie	vasektomie	k1gFnSc2	vasektomie
než	než	k8xS	než
u	u	k7c2	u
podvázání	podvázání	k1gNnSc2	podvázání
vejcovodů	vejcovod	k1gInPc2	vejcovod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vasektomii	vasektomie	k1gFnSc6	vasektomie
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
otoku	otok	k1gInSc3	otok
a	a	k8xC	a
bolestivosti	bolestivost	k1gFnSc3	bolestivost
šourku	šourek	k1gInSc2	šourek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
potíže	potíž	k1gFnPc4	potíž
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
odezní	odeznět	k5eAaPmIp3nP	odeznět
během	během	k7c2	během
jednoho	jeden	k4xCgMnSc2	jeden
až	až	k8xS	až
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
podvázání	podvázání	k1gNnSc2	podvázání
vejcovodů	vejcovod	k1gInPc2	vejcovod
se	se	k3xPyFc4	se
komplikace	komplikace	k1gFnPc1	komplikace
projevují	projevovat	k5eAaImIp3nP	projevovat
v	v	k7c6	v
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
%	%	kIx~	%
zákroků	zákrok	k1gInPc2	zákrok
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
případným	případný	k2eAgFnPc3d1	případná
závažným	závažný	k2eAgFnPc3d1	závažná
komplikacím	komplikace	k1gFnPc3	komplikace
dochází	docházet	k5eAaImIp3nS	docházet
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
anestezie	anestezie	k1gFnSc2	anestezie
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
metod	metoda	k1gFnPc2	metoda
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
sexuálně	sexuálně	k6eAd1	sexuálně
přenosnými	přenosný	k2eAgFnPc7d1	přenosná
infekcemi	infekce	k1gFnPc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
ženy	žena	k1gFnPc1	žena
svého	svůj	k3xOyFgNnSc2	svůj
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
později	pozdě	k6eAd2	pozdě
litují	litovat	k5eAaImIp3nP	litovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
starších	starší	k1gMnPc2	starší
30	[number]	k4	30
let	léto	k1gNnPc2	léto
a	a	k8xC	a
kolem	kolem	k7c2	kolem
20	[number]	k4	20
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
mladších	mladý	k2eAgFnPc2d2	mladší
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
zákroku	zákrok	k1gInSc6	zákrok
litovat	litovat	k5eAaImF	litovat
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc1d2	nižší
(	(	kIx(	(
<g/>
<	<	kIx(	<
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vyšší	vysoký	k2eAgMnSc1d2	vyšší
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
mladším	mladý	k2eAgInSc6d2	mladší
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
dětmi	dítě	k1gFnPc7	dítě
nebo	nebo	k8xC	nebo
bezdětných	bezdětný	k2eAgFnPc2d1	bezdětná
a	a	k8xC	a
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
nestabilním	stabilní	k2eNgNnSc6d1	nestabilní
manželství	manželství	k1gNnSc6	manželství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
průzkumu	průzkum	k1gInSc6	průzkum
provedeném	provedený	k2eAgInSc6d1	provedený
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
uvedlo	uvést	k5eAaPmAgNnS	uvést
9	[number]	k4	9
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
děti	dítě	k1gFnPc4	dítě
neměli	mít	k5eNaImAgMnP	mít
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
tehdy	tehdy	k6eAd1	tehdy
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
sterilizace	sterilizace	k1gFnSc1	sterilizace
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
trvalý	trvalý	k2eAgInSc4d1	trvalý
zákrok	zákrok	k1gInSc4	zákrok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
pokusit	pokusit	k5eAaPmF	pokusit
o	o	k7c4	o
reverzi	reverze	k1gFnSc4	reverze
podvázání	podvázání	k1gNnSc2	podvázání
vejcovodů	vejcovod	k1gInPc2	vejcovod
obnovením	obnovení	k1gNnSc7	obnovení
průchodnosti	průchodnost	k1gFnSc2	průchodnost
vejcovodů	vejcovod	k1gInPc2	vejcovod
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
o	o	k7c4	o
reverzi	reverze	k1gFnSc4	reverze
vasektomie	vasektomie	k1gFnSc2	vasektomie
obnovením	obnovení	k1gNnSc7	obnovení
průchodnosti	průchodnost	k1gFnSc2	průchodnost
chámovodů	chámovod	k1gInPc2	chámovod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
souvisí	souviset	k5eAaImIp3nS	souviset
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
reverzi	reverze	k1gFnSc6	reverze
zákroku	zákrok	k1gInSc2	zákrok
často	často	k6eAd1	často
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
partnera	partner	k1gMnSc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
početí	početí	k1gNnSc2	početí
po	po	k7c6	po
reverzi	reverze	k1gFnSc6	reverze
podvázání	podvázání	k1gNnSc2	podvázání
vejcovodů	vejcovod	k1gInPc2	vejcovod
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
31	[number]	k4	31
do	do	k7c2	do
88	[number]	k4	88
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
možným	možný	k2eAgFnPc3d1	možná
komplikacím	komplikace	k1gFnPc3	komplikace
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k8xC	i
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
mimoděložního	mimoděložní	k2eAgNnSc2d1	mimoděložní
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mužů	muž	k1gMnPc2	muž
žádajících	žádající	k2eAgMnPc2d1	žádající
o	o	k7c6	o
reverzi	reverze	k1gFnSc6	reverze
zákroku	zákrok	k1gInSc2	zákrok
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
2	[number]	k4	2
do	do	k7c2	do
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
zplození	zplození	k1gNnSc2	zplození
dítěte	dítě	k1gNnSc2	dítě
po	po	k7c6	po
reverzi	reverze	k1gFnSc6	reverze
zákroku	zákrok	k1gInSc2	zákrok
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
38	[number]	k4	38
do	do	k7c2	do
84	[number]	k4	84
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
tím	ten	k3xDgNnSc7	ten
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
delší	dlouhý	k2eAgFnSc1d2	delší
doba	doba	k1gFnSc1	doba
uplyne	uplynout	k5eAaPmIp3nS	uplynout
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
zákroku	zákrok	k1gInSc2	zákrok
do	do	k7c2	do
reverze	reverze	k1gFnSc2	reverze
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
odběr	odběr	k1gInSc1	odběr
spermatu	sperma	k1gNnSc2	sperma
s	s	k7c7	s
následným	následný	k2eAgNnSc7d1	následné
oplodněním	oplodnění	k1gNnSc7	oplodnění
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
metodám	metoda	k1gFnPc3	metoda
založeným	založený	k2eAgFnPc3d1	založená
na	na	k7c6	na
jistém	jistý	k2eAgInSc6d1	jistý
způsobu	způsob	k1gInSc6	způsob
chování	chování	k1gNnSc2	chování
patří	patřit	k5eAaImIp3nP	patřit
načasování	načasování	k1gNnSc4	načasování
nebo	nebo	k8xC	nebo
takový	takový	k3xDgInSc1	takový
způsob	způsob	k1gInSc1	způsob
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
proniknutí	proniknutí	k1gNnSc3	proniknutí
spermatu	sperma	k1gNnSc2	sperma
do	do	k7c2	do
ženského	ženský	k2eAgNnSc2d1	ženské
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
dodržovány	dodržovat	k5eAaImNgFnP	dodržovat
bezchybně	bezchybně	k6eAd1	bezchybně
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc4	jejich
četnost	četnost	k1gFnSc4	četnost
selhání	selhání	k1gNnPc4	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
pohybovat	pohybovat	k5eAaImF	pohybovat
kolem	kolem	k7c2	kolem
3,4	[number]	k4	3,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nesprávného	správný	k2eNgNnSc2d1	nesprávné
dodržování	dodržování	k1gNnSc2	dodržování
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
blížit	blížit	k5eAaImF	blížit
i	i	k9	i
85	[number]	k4	85
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
sledování	sledování	k1gNnSc2	sledování
plodných	plodný	k2eAgInPc2d1	plodný
dnů	den	k1gInPc2	den
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
určování	určování	k1gNnSc6	určování
nejplodnějších	plodný	k2eAgInPc2d3	nejplodnější
dnů	den	k1gInPc2	den
menstruačního	menstruační	k2eAgInSc2d1	menstruační
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
nechráněnému	chráněný	k2eNgInSc3d1	nechráněný
pohlavnímu	pohlavní	k2eAgInSc3d1	pohlavní
styku	styk	k1gInSc3	styk
<g/>
.	.	kIx.	.
</s>
<s>
Plodnost	plodnost	k1gFnSc4	plodnost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určovat	určovat	k5eAaImF	určovat
například	například	k6eAd1	například
na	na	k7c6	na
základě	základ	k1gInSc6	základ
bazální	bazální	k2eAgFnSc2d1	bazální
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
cervikálního	cervikální	k2eAgInSc2d1	cervikální
hlenu	hlen	k1gInSc2	hlen
nebo	nebo	k8xC	nebo
dne	den	k1gInSc2	den
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
selhání	selhání	k1gNnSc2	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
metod	metoda	k1gFnPc2	metoda
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
zpravidla	zpravidla	k6eAd1	zpravidla
od	od	k7c2	od
12	[number]	k4	12
do	do	k7c2	do
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
bezchybného	bezchybný	k2eAgNnSc2d1	bezchybné
používání	používání	k1gNnSc2	používání
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
používané	používaný	k2eAgFnSc6d1	používaná
metodě	metoda	k1gFnSc6	metoda
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
9	[number]	k4	9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
odhady	odhad	k1gInPc1	odhad
vycházejí	vycházet	k5eAaImIp3nP	vycházet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
málo	málo	k4c1	málo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
většina	většina	k1gFnSc1	většina
účastníků	účastník	k1gMnPc2	účastník
studií	studie	k1gFnSc7	studie
tyto	tento	k3xDgFnPc4	tento
metody	metoda	k1gFnPc4	metoda
přestává	přestávat	k5eAaImIp3nS	přestávat
používat	používat	k5eAaImF	používat
předčasně	předčasně	k6eAd1	předčasně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
tyto	tento	k3xDgFnPc4	tento
metody	metoda	k1gFnPc4	metoda
používá	používat	k5eAaImIp3nS	používat
přibližně	přibližně	k6eAd1	přibližně
3,6	[number]	k4	3,6
%	%	kIx~	%
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
určení	určení	k1gNnSc2	určení
bazální	bazální	k2eAgFnSc2d1	bazální
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
primárního	primární	k2eAgInSc2d1	primární
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
symptotermální	symptotermální	k2eAgFnSc1d1	symptotermální
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
neplánovanému	plánovaný	k2eNgNnSc3d1	neplánované
otěhotnění	otěhotnění	k1gNnSc3	otěhotnění
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
typických	typický	k2eAgMnPc2d1	typický
uživatelů	uživatel	k1gMnPc2	uživatel
symptotermální	symptotermální	k2eAgFnSc2d1	symptotermální
metody	metoda	k1gFnSc2	metoda
v	v	k7c6	v
1	[number]	k4	1
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Přerušovaná	přerušovaný	k2eAgFnSc1d1	přerušovaná
soulož	soulož	k1gFnSc1	soulož
(	(	kIx(	(
<g/>
odborně	odborně	k6eAd1	odborně
též	též	k9	též
coitus	coitus	k1gMnSc1	coitus
interruptus	interruptus	k1gMnSc1	interruptus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
praktika	praktika	k1gFnSc1	praktika
spočívající	spočívající	k2eAgFnSc1d1	spočívající
v	v	k7c6	v
ukončení	ukončení	k1gNnSc6	ukončení
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
styku	styk	k1gInSc2	styk
(	(	kIx(	(
<g/>
vyjmutí	vyjmutí	k1gNnSc2	vyjmutí
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
údu	úd	k1gInSc2	úd
<g/>
)	)	kIx)	)
před	před	k7c7	před
ejakulací	ejakulace	k1gFnSc7	ejakulace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
rizikem	riziko	k1gNnSc7	riziko
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
muž	muž	k1gMnSc1	muž
neprovede	provést	k5eNaPmIp3nS	provést
správně	správně	k6eAd1	správně
nebo	nebo	k8xC	nebo
včas	včas	k6eAd1	včas
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
selhání	selhání	k1gNnSc2	selhání
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
4	[number]	k4	4
%	%	kIx~	%
při	při	k7c6	při
bezchybném	bezchybný	k2eAgNnSc6d1	bezchybné
používání	používání	k1gNnSc6	používání
až	až	k9	až
po	po	k7c4	po
27	[number]	k4	27
%	%	kIx~	%
při	při	k7c6	při
běžném	běžný	k2eAgNnSc6d1	běžné
používání	používání	k1gNnSc6	používání
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lékaři	lékař	k1gMnPc1	lékař
tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
spermatu	sperma	k1gNnSc2	sperma
v	v	k7c6	v
preejakulátu	preejakulát	k1gInSc6	preejakulát
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pouze	pouze	k6eAd1	pouze
málo	málo	k4c1	málo
důkazů	důkaz	k1gInPc2	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
předběžných	předběžný	k2eAgFnPc6d1	předběžná
studiích	studie	k1gFnPc6	studie
žádné	žádný	k3yNgNnSc1	žádný
sperma	sperma	k1gNnSc1	sperma
v	v	k7c6	v
preejakulátu	preejakulát	k1gInSc6	preejakulát
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
nebylo	být	k5eNaImAgNnS	být
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
klinických	klinický	k2eAgNnPc2d1	klinické
hodnocení	hodnocení	k1gNnPc2	hodnocení
bylo	být	k5eAaImAgNnS	být
sperma	sperma	k1gNnSc1	sperma
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
u	u	k7c2	u
10	[number]	k4	10
z	z	k7c2	z
27	[number]	k4	27
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Metodu	metoda	k1gFnSc4	metoda
přerušované	přerušovaný	k2eAgFnSc2d1	přerušovaná
soulože	soulož	k1gFnSc2	soulož
jako	jako	k8xS	jako
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
používají	používat	k5eAaImIp3nP	používat
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
%	%	kIx~	%
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
propagují	propagovat	k5eAaImIp3nP	propagovat
absolutní	absolutní	k2eAgFnSc4d1	absolutní
sexuální	sexuální	k2eAgFnSc4d1	sexuální
abstinenci	abstinence	k1gFnSc4	abstinence
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
rozumějí	rozumět	k5eAaImIp3nP	rozumět
vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
jakékoli	jakýkoli	k3yIgFnSc3	jakýkoli
sexuální	sexuální	k2eAgFnSc3d1	sexuální
aktivitě	aktivita	k1gFnSc3	aktivita
<g/>
,	,	kIx,	,
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
termínem	termín	k1gInSc7	termín
obvykle	obvykle	k6eAd1	obvykle
myslí	myslet	k5eAaImIp3nS	myslet
vyhýbání	vyhýbání	k1gNnSc4	vyhýbání
se	se	k3xPyFc4	se
vaginálnímu	vaginální	k2eAgInSc3d1	vaginální
styku	styk	k1gInSc3	styk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
způsob	způsob	k1gInSc1	způsob
zabránění	zabránění	k1gNnSc2	zabránění
těhotenství	těhotenství	k1gNnSc2	těhotenství
je	být	k5eAaImIp3nS	být
sexuální	sexuální	k2eAgFnSc1d1	sexuální
abstinence	abstinence	k1gFnSc1	abstinence
100	[number]	k4	100
<g/>
%	%	kIx~	%
spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
chovat	chovat	k5eAaImF	chovat
zdrženlivě	zdrženlivě	k6eAd1	zdrženlivě
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k9	ale
vzdá	vzdát	k5eAaPmIp3nS	vzdát
veškeré	veškerý	k3xTgFnPc4	veškerý
sexuální	sexuální	k2eAgFnPc4d1	sexuální
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
populacích	populace	k1gFnPc6	populace
je	být	k5eAaImIp3nS	být
také	také	k9	také
významné	významný	k2eAgNnSc1d1	významné
riziko	riziko	k1gNnSc1	riziko
otěhotnění	otěhotnění	k1gNnSc1	otěhotnění
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
znásilnění	znásilnění	k1gNnSc2	znásilnění
<g/>
.	.	kIx.	.
</s>
<s>
Výchova	výchova	k1gFnSc1	výchova
k	k	k7c3	k
sexuální	sexuální	k2eAgFnSc3d1	sexuální
abstinenci	abstinence	k1gFnSc3	abstinence
nesnižuje	snižovat	k5eNaImIp3nS	snižovat
četnost	četnost	k1gFnSc1	četnost
těhotenství	těhotenství	k1gNnSc2	těhotenství
u	u	k7c2	u
nezletilých	nezletilá	k1gFnPc2	nezletilá
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
těhotenství	těhotenství	k1gNnSc2	těhotenství
u	u	k7c2	u
nezletilých	nezletilá	k1gFnPc2	nezletilá
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
u	u	k7c2	u
studentek	studentka	k1gFnPc2	studentka
s	s	k7c7	s
výchovou	výchova	k1gFnSc7	výchova
k	k	k7c3	k
pohlavní	pohlavní	k2eAgFnSc3d1	pohlavní
abstinenci	abstinence	k1gFnSc3	abstinence
než	než	k8xS	než
u	u	k7c2	u
studentek	studentka	k1gFnPc2	studentka
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
komplexní	komplexní	k2eAgFnSc2d1	komplexní
sexuální	sexuální	k2eAgFnSc2d1	sexuální
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
používají	používat	k5eAaImIp3nP	používat
sexuální	sexuální	k2eAgFnSc4d1	sexuální
abstinenci	abstinence	k1gFnSc4	abstinence
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
metodu	metoda	k1gFnSc4	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
používali	používat	k5eAaImAgMnP	používat
ještě	ještě	k6eAd1	ještě
nějakou	nějaký	k3yIgFnSc4	nějaký
další	další	k2eAgFnSc4d1	další
metodu	metoda	k1gFnSc4	metoda
nebo	nebo	k8xC	nebo
metody	metoda	k1gFnPc4	metoda
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kondom	kondom	k1gInSc4	kondom
nebo	nebo	k8xC	nebo
nouzovou	nouzový	k2eAgFnSc4d1	nouzová
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
také	také	k9	také
sex	sex	k1gInSc4	sex
bez	bez	k7c2	bez
penetrace	penetrace	k1gFnSc2	penetrace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
vaginálního	vaginální	k2eAgInSc2d1	vaginální
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
a	a	k8xC	a
orální	orální	k2eAgInSc4d1	orální
sex	sex	k1gInSc4	sex
bez	bez	k7c2	bez
vaginálního	vaginální	k2eAgInSc2d1	vaginální
styku	styk	k1gInSc2	styk
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
sice	sice	k8xC	sice
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
interfemorální	interfemorální	k2eAgInSc4d1	interfemorální
styk	styk	k1gInSc4	styk
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
metody	metoda	k1gFnPc4	metoda
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
penis	penis	k1gInSc1	penis
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vagíny	vagína	k1gFnSc2	vagína
(	(	kIx(	(
<g/>
tření	tření	k1gNnSc1	tření
genitálií	genitálie	k1gFnPc2	genitálie
<g/>
)	)	kIx)	)
a	a	k8xC	a
anální	anální	k2eAgInSc4d1	anální
styk	styk	k1gInSc4	styk
<g/>
)	)	kIx)	)
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
otěhotnění	otěhotněný	k2eAgMnPc1d1	otěhotněný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
sperma	sperma	k1gNnSc1	sperma
ejakulováno	ejakulovat	k5eAaBmNgNnS	ejakulovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
vagíny	vagína	k1gFnSc2	vagína
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
<g/>
-li	i	k?	-li
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
proniknout	proniknout	k5eAaPmF	proniknout
společně	společně	k6eAd1	společně
s	s	k7c7	s
tekutinou	tekutina	k1gFnSc7	tekutina
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
lubrikaci	lubrikace	k1gFnSc4	lubrikace
vagíny	vagína	k1gFnSc2	vagína
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
laktační	laktační	k2eAgFnSc2d1	laktační
amenorey	amenorea	k1gFnSc2	amenorea
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
využívání	využívání	k1gNnSc6	využívání
ženiny	ženin	k2eAgFnSc2d1	ženina
přirozené	přirozený	k2eAgFnSc2d1	přirozená
poporodní	poporodní	k2eAgFnSc2d1	poporodní
neplodnosti	neplodnost	k1gFnSc2	neplodnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nastává	nastávat	k5eAaImIp3nS	nastávat
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
kojením	kojení	k1gNnSc7	kojení
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
nedostavující	dostavující	k2eNgFnPc4d1	nedostavující
se	se	k3xPyFc4	se
menstruace	menstruace	k1gFnPc4	menstruace
<g/>
,	,	kIx,	,
výhradní	výhradní	k2eAgNnSc4d1	výhradní
kojení	kojení	k1gNnSc4	kojení
novorozence	novorozenec	k1gMnSc2	novorozenec
a	a	k8xC	a
věk	věk	k1gInSc1	věk
dítěte	dítě	k1gNnSc2	dítě
do	do	k7c2	do
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kojení	kojení	k1gNnSc1	kojení
jediným	jediný	k2eAgInSc7d1	jediný
zdrojem	zdroj	k1gInSc7	zdroj
výživy	výživa	k1gFnSc2	výživa
kojence	kojenec	k1gMnSc2	kojenec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
četnost	četnost	k1gFnSc4	četnost
selhání	selhání	k1gNnPc4	selhání
během	během	k7c2	během
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klinických	klinický	k2eAgNnPc6d1	klinické
hodnoceních	hodnocení	k1gNnPc6	hodnocení
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
četnost	četnost	k1gFnSc1	četnost
selhání	selhání	k1gNnSc2	selhání
od	od	k7c2	od
0	[number]	k4	0
%	%	kIx~	%
do	do	k7c2	do
7,5	[number]	k4	7,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
četnost	četnost	k1gFnSc1	četnost
selhání	selhání	k1gNnSc2	selhání
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
na	na	k7c4	na
4	[number]	k4	4
až	až	k9	až
7	[number]	k4	7
%	%	kIx~	%
a	a	k8xC	a
během	během	k7c2	během
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
až	až	k9	až
na	na	k7c4	na
13	[number]	k4	13
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
podávání	podávání	k1gNnSc3	podávání
kojenecké	kojenecký	k2eAgFnSc2d1	kojenecká
mléčné	mléčný	k2eAgFnSc2d1	mléčná
výživy	výživa	k1gFnSc2	výživa
<g/>
,	,	kIx,	,
odstřikování	odstřikování	k1gNnSc1	odstřikování
mateřského	mateřský	k2eAgNnSc2d1	mateřské
mléka	mléko	k1gNnSc2	mléko
místo	místo	k7c2	místo
přímého	přímý	k2eAgNnSc2d1	přímé
kojení	kojení	k1gNnSc2	kojení
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc2	používání
dudlíku	dudlík	k1gInSc2	dudlík
a	a	k8xC	a
krmení	krmení	k1gNnSc4	krmení
pevnou	pevný	k2eAgFnSc7d1	pevná
stravou	strava	k1gFnSc7	strava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
plně	plně	k6eAd1	plně
kojí	kojit	k5eAaImIp3nP	kojit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
10	[number]	k4	10
%	%	kIx~	%
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
menstruační	menstruační	k2eAgInSc1d1	menstruační
cyklus	cyklus	k1gInSc1	cyklus
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
%	%	kIx~	%
do	do	k7c2	do
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nekojících	kojící	k2eNgFnPc2d1	nekojící
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
plodnost	plodnost	k1gFnSc1	plodnost
obnovit	obnovit	k5eAaPmF	obnovit
už	už	k6eAd1	už
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
týdnech	týden	k1gInPc6	týden
od	od	k7c2	od
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
nouzové	nouzový	k2eAgFnSc2d1	nouzová
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
pilulek	pilulka	k1gFnPc2	pilulka
"	"	kIx"	"
<g/>
následujícího	následující	k2eAgNnSc2d1	následující
rána	ráno	k1gNnSc2	ráno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
používání	používání	k1gNnSc1	používání
prostředků	prostředek	k1gInPc2	prostředek
po	po	k7c6	po
nechráněném	chráněný	k2eNgInSc6d1	nechráněný
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
styku	styk	k1gInSc6	styk
s	s	k7c7	s
nadějí	naděje	k1gFnSc7	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabrání	zabránit	k5eAaPmIp3nS	zabránit
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
převážně	převážně	k6eAd1	převážně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
brání	bránit	k5eAaImIp3nS	bránit
ovulaci	ovulace	k1gFnSc4	ovulace
nebo	nebo	k8xC	nebo
oplodnění	oplodnění	k1gNnSc4	oplodnění
<g/>
.	.	kIx.	.
<g/>
avšak	avšak	k8xC	avšak
možná	možná	k9	možná
take	take	k6eAd1	take
existovat	existovat	k5eAaImF	existovat
mechanism	mechanism	k6eAd1	mechanism
ovlivněním	ovlivnění	k1gNnSc7	ovlivnění
endometria	endometrium	k1gNnSc2	endometrium
brání	bránit	k5eAaImIp3nP	bránit
nidaci	nidace	k1gFnSc4	nidace
oplodněného	oplodněný	k2eAgInSc2d1	oplodněný
vajíčkaavšak	vajíčkaavšak	k6eAd1	vajíčkaavšak
možná	možná	k9	možná
take	takat	k5eAaPmIp3nS	takat
existovat	existovat	k5eAaImF	existovat
mechanism	mechanism	k6eAd1	mechanism
ovlivněním	ovlivnění	k1gNnSc7	ovlivnění
endometria	endometrium	k1gNnSc2	endometrium
brání	bránit	k5eAaImIp3nP	bránit
nidaci	nidace	k1gFnSc4	nidace
oplodněného	oplodněný	k2eAgNnSc2d1	oplodněné
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc4d1	různá
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
antikoncepční	antikoncepční	k2eAgFnPc1d1	antikoncepční
tablety	tableta	k1gFnPc1	tableta
obsahující	obsahující	k2eAgFnSc4d1	obsahující
vysokou	vysoký	k2eAgFnSc4d1	vysoká
dávku	dávka	k1gFnSc4	dávka
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
levonorgestrel	levonorgestrela	k1gFnPc2	levonorgestrela
<g/>
,	,	kIx,	,
mifepriston	mifepriston	k1gInSc4	mifepriston
<g/>
,	,	kIx,	,
ulipristal	ulipristat	k5eAaPmAgMnS	ulipristat
nebo	nebo	k8xC	nebo
nitroděložní	nitroděložní	k2eAgNnPc4d1	nitroděložní
tělíska	tělísko	k1gNnPc4	tělísko
<g/>
.	.	kIx.	.
</s>
<s>
Tablety	tableta	k1gFnPc1	tableta
levonorgestrelu	levonorgestrel	k1gInSc2	levonorgestrel
snižují	snižovat	k5eAaImIp3nP	snižovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
otěhotnění	otěhotnění	k1gNnSc2	otěhotnění
o	o	k7c4	o
70	[number]	k4	70
%	%	kIx~	%
(	(	kIx(	(
<g/>
četnost	četnost	k1gFnSc1	četnost
těhotenství	těhotenství	k1gNnSc1	těhotenství
2,2	[number]	k4	2,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
užity	užít	k5eAaPmNgFnP	užít
do	do	k7c2	do
3	[number]	k4	3
dnů	den	k1gInPc2	den
po	po	k7c6	po
nechráněném	chráněný	k2eNgInSc6d1	nechráněný
styku	styk	k1gInSc6	styk
nebo	nebo	k8xC	nebo
selhání	selhání	k1gNnSc6	selhání
kondomu	kondom	k1gInSc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Ulipristal	Ulipristat	k5eAaImAgMnS	Ulipristat
snižuje	snižovat	k5eAaImIp3nS	snižovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
otěhotnění	otěhotnění	k1gNnSc3	otěhotnění
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
85	[number]	k4	85
%	%	kIx~	%
(	(	kIx(	(
<g/>
četnost	četnost	k1gFnSc1	četnost
těhotenství	těhotenství	k1gNnSc1	těhotenství
1,4	[number]	k4	1,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
něco	něco	k3yInSc4	něco
účinnější	účinný	k2eAgMnSc1d2	účinnější
než	než	k8xS	než
levonorgestrel	levonorgestrel	k1gMnSc1	levonorgestrel
<g/>
.	.	kIx.	.
</s>
<s>
Mifepriston	Mifepriston	k1gInSc1	Mifepriston
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
účinnější	účinný	k2eAgMnSc1d2	účinnější
než	než	k8xS	než
levonorgestrel	levonorgestrel	k1gMnSc1	levonorgestrel
<g/>
.	.	kIx.	.
</s>
<s>
Nejspolehlivější	spolehlivý	k2eAgFnSc7d3	nejspolehlivější
metodou	metoda	k1gFnSc7	metoda
jsou	být	k5eAaImIp3nP	být
měděná	měděný	k2eAgNnPc1d1	měděné
nitroděložní	nitroděložní	k2eAgNnPc1d1	nitroděložní
tělíska	tělísko	k1gNnPc1	tělísko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
zavádět	zavádět	k5eAaImF	zavádět
až	až	k9	až
5	[number]	k4	5
dnů	den	k1gInPc2	den
po	po	k7c6	po
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
styku	styk	k1gInSc6	styk
a	a	k8xC	a
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
přibližně	přibližně	k6eAd1	přibližně
99	[number]	k4	99
%	%	kIx~	%
těhotenství	těhotenství	k1gNnSc2	těhotenství
(	(	kIx(	(
<g/>
četnost	četnost	k1gFnSc1	četnost
těhotenství	těhotenství	k1gNnSc2	těhotenství
0,1	[number]	k4	0,1
až	až	k9	až
0,2	[number]	k4	0,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
nejspolehlivější	spolehlivý	k2eAgFnSc7d3	nejspolehlivější
metodou	metoda	k1gFnSc7	metoda
nouzové	nouzový	k2eAgFnSc2d1	nouzová
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Poskytování	poskytování	k1gNnSc1	poskytování
pilulek	pilulka	k1gFnPc2	pilulka
"	"	kIx"	"
<g/>
následujícího	následující	k2eAgNnSc2d1	následující
rána	ráno	k1gNnSc2	ráno
<g/>
"	"	kIx"	"
ženám	žena	k1gFnPc3	žena
v	v	k7c6	v
předstihu	předstih	k1gInSc6	předstih
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
sexuálně	sexuálně	k6eAd1	sexuálně
přenášených	přenášený	k2eAgFnPc2d1	přenášená
infekcí	infekce	k1gFnPc2	infekce
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
kondomů	kondom	k1gInPc2	kondom
<g/>
,	,	kIx,	,
četnost	četnost	k1gFnSc4	četnost
těhotenství	těhotenství	k1gNnSc2	těhotenství
ani	ani	k8xC	ani
rizikové	rizikový	k2eAgNnSc4d1	rizikové
sexuální	sexuální	k2eAgNnSc4d1	sexuální
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
minimální	minimální	k2eAgInPc1d1	minimální
vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Dvojí	dvojí	k4xRgFnSc7	dvojí
ochranou	ochrana	k1gFnSc7	ochrana
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
použití	použití	k1gNnSc1	použití
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
jak	jak	k8xC	jak
nákaze	nákaza	k1gFnSc3	nákaza
sexuálně	sexuálně	k6eAd1	sexuálně
přenosnými	přenosný	k2eAgFnPc7d1	přenosná
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Dvojí	dvojí	k4xRgFnSc1	dvojí
ochrana	ochrana	k1gFnSc1	ochrana
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
použití	použití	k1gNnSc2	použití
buď	buď	k8xC	buď
pouhého	pouhý	k2eAgInSc2d1	pouhý
kondomu	kondom	k1gInSc2	kondom
<g/>
,	,	kIx,	,
kondomu	kondom	k1gInSc2	kondom
současně	současně	k6eAd1	současně
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
druhem	druh	k1gInSc7	druh
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
nebo	nebo	k8xC	nebo
vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
pohlavnímu	pohlavní	k2eAgInSc3d1	pohlavní
styku	styk	k1gInSc3	styk
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
obava	obava	k1gFnSc1	obava
z	z	k7c2	z
otěhotnění	otěhotnění	k1gNnSc2	otěhotnění
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
současné	současný	k2eAgNnSc4d1	současné
používání	používání	k1gNnSc4	používání
dvou	dva	k4xCgFnPc2	dva
metod	metoda	k1gFnPc2	metoda
rozumné	rozumný	k2eAgFnPc1d1	rozumná
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
dvou	dva	k4xCgFnPc2	dva
forem	forma	k1gFnPc2	forma
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
osobám	osoba	k1gFnPc3	osoba
užívajícím	užívající	k2eAgFnPc3d1	užívající
lék	lék	k1gInSc4	lék
proti	proti	k7c3	proti
akné	akné	k1gFnSc3	akné
izotretinoin	izotretinoina	k1gFnPc2	izotretinoina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vysokého	vysoký	k2eAgNnSc2d1	vysoké
rizika	riziko	k1gNnSc2	riziko
vzniku	vznik	k1gInSc2	vznik
vrozených	vrozený	k2eAgFnPc2d1	vrozená
vad	vada	k1gFnPc2	vada
při	při	k7c6	při
užívání	užívání	k1gNnSc6	užívání
léku	lék	k1gInSc2	lék
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
snížilo	snížit	k5eAaPmAgNnS	snížit
počet	počet	k1gInSc1	počet
mateřské	mateřský	k2eAgFnSc2d1	mateřská
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
zabránila	zabránit	k5eAaPmAgFnS	zabránit
úmrtí	úmrtí	k1gNnSc4	úmrtí
přibližně	přibližně	k6eAd1	přibližně
270	[number]	k4	270
000	[number]	k4	000
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
zabránit	zabránit	k5eAaPmF	zabránit
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
antikoncepci	antikoncepce	k1gFnSc6	antikoncepce
zcela	zcela	k6eAd1	zcela
uspokojena	uspokojit	k5eAaPmNgFnS	uspokojit
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
příznivého	příznivý	k2eAgInSc2d1	příznivý
výsledku	výsledek	k1gInSc2	výsledek
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
snížením	snížení	k1gNnSc7	snížení
počtu	počet	k1gInSc2	počet
neplánovaných	plánovaný	k2eNgNnPc2d1	neplánované
otěhotnění	otěhotnění	k1gNnPc2	otěhotnění
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
následně	následně	k6eAd1	následně
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
nelegálně	legálně	k6eNd1	legálně
prováděným	prováděný	k2eAgInPc3d1	prováděný
potratům	potrat	k1gInPc3	potrat
<g/>
,	,	kIx,	,
a	a	k8xC	a
prevencí	prevence	k1gFnSc7	prevence
otěhotnění	otěhotnění	k1gNnSc2	otěhotnění
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
podléhajících	podléhající	k2eAgFnPc2d1	podléhající
vysokému	vysoký	k2eAgNnSc3d1	vysoké
riziku	riziko	k1gNnSc3	riziko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
rovněž	rovněž	k9	rovněž
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
přežití	přežití	k1gNnSc2	přežití
dítěte	dítě	k1gNnSc2	dítě
díky	díky	k7c3	díky
prodloužení	prodloužení	k1gNnSc3	prodloužení
doby	doba	k1gFnSc2	doba
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
těhotenstvími	těhotenství	k1gNnPc7	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
populaci	populace	k1gFnSc6	populace
jsou	být	k5eAaImIp3nP	být
výsledky	výsledek	k1gInPc1	výsledek
horší	zlý	k2eAgInPc1d2	horší
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
matka	matka	k1gFnSc1	matka
otěhotní	otěhotnět	k5eAaPmIp3nS	otěhotnět
během	během	k7c2	během
osmnácti	osmnáct	k4xCc2	osmnáct
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
oddálení	oddálení	k1gNnSc1	oddálení
dalšího	další	k2eAgNnSc2d1	další
těhotenství	těhotenství	k1gNnSc2	těhotenství
po	po	k7c6	po
potratu	potrat	k1gInSc6	potrat
riziko	riziko	k1gNnSc1	riziko
nikterak	nikterak	k6eAd1	nikterak
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
a	a	k8xC	a
ženám	žena	k1gFnPc3	žena
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
je	být	k5eAaImIp3nS	být
doporučováno	doporučován	k2eAgNnSc1d1	doporučováno
pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
otěhotnění	otěhotnění	k1gNnSc4	otěhotnění
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nP	cítit
připraveny	připravit	k5eAaPmNgFnP	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Těhotenství	těhotenství	k1gNnSc1	těhotenství
v	v	k7c6	v
mladistvém	mladistvý	k2eAgInSc6d1	mladistvý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
obzvláště	obzvláště	k6eAd1	obzvláště
mladých	mladý	k2eAgFnPc2d1	mladá
nezletilých	nezletilá	k1gFnPc2	nezletilá
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
nepříznivých	příznivý	k2eNgInPc2d1	nepříznivý
výsledků	výsledek	k1gInPc2	výsledek
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
předčasný	předčasný	k2eAgInSc1d1	předčasný
porod	porod	k1gInSc1	porod
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
porodní	porodní	k2eAgFnSc1d1	porodní
váha	váha	k1gFnSc1	váha
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc1	úmrtí
novorozeněte	novorozeně	k1gNnSc2	novorozeně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
82	[number]	k4	82
%	%	kIx~	%
těhotenství	těhotenství	k1gNnSc1	těhotenství
u	u	k7c2	u
dívek	dívka	k1gFnPc2	dívka
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
neplánovaných	plánovaný	k2eNgMnPc2d1	neplánovaný
<g/>
.	.	kIx.	.
</s>
<s>
Zevrubná	zevrubný	k2eAgFnSc1d1	zevrubná
sexuální	sexuální	k2eAgFnSc1d1	sexuální
výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
účinně	účinně	k6eAd1	účinně
snižují	snižovat	k5eAaImIp3nP	snižovat
počet	počet	k1gInSc4	počet
případů	případ	k1gInPc2	případ
těhotenství	těhotenství	k1gNnSc2	těhotenství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
menšího	malý	k2eAgInSc2d2	menší
počtu	počet	k1gInSc2	počet
závislých	závislý	k2eAgFnPc2d1	závislá
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
žen	žena	k1gFnPc2	žena
podílejících	podílející	k2eAgFnPc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
pracovní	pracovní	k2eAgFnSc6d1	pracovní
síle	síla	k1gFnSc6	síla
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
lepšímu	dobrý	k2eAgInSc3d2	lepší
přístupu	přístup	k1gInSc3	přístup
k	k	k7c3	k
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
se	se	k3xPyFc4	se
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
příjmy	příjem	k1gInPc1	příjem
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
index	index	k1gInSc1	index
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
i	i	k8xC	i
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
index	index	k1gInSc1	index
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
jejich	jejich	k3xOp3gFnPc2	jejich
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Plánování	plánování	k1gNnSc1	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
používání	používání	k1gNnSc2	používání
moderní	moderní	k2eAgFnSc2d1	moderní
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nákladově	nákladově	k6eAd1	nákladově
nejefektivnějších	efektivní	k2eAgNnPc2d3	nejefektivnější
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
dolar	dolar	k1gInSc4	dolar
vynaložený	vynaložený	k2eAgInSc1d1	vynaložený
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
připadá	připadat	k5eAaPmIp3nS	připadat
dva	dva	k4xCgInPc1	dva
až	až	k6eAd1	až
šest	šest	k4xCc1	šest
dolarů	dolar	k1gInPc2	dolar
ušetřených	ušetřený	k2eAgInPc2d1	ušetřený
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
úspory	úspora	k1gFnPc1	úspora
nákladů	náklad	k1gInPc2	náklad
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
prevencí	prevence	k1gFnSc7	prevence
neplánovaných	plánovaný	k2eNgNnPc2d1	neplánované
těhotenství	těhotenství	k1gNnPc2	těhotenství
a	a	k8xC	a
menším	malý	k2eAgNnSc7d2	menší
rozšířením	rozšíření	k1gNnSc7	rozšíření
sexuálně	sexuálně	k6eAd1	sexuálně
přenosných	přenosný	k2eAgFnPc2d1	přenosná
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
finančně	finančně	k6eAd1	finančně
přínosné	přínosný	k2eAgFnPc1d1	přínosná
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
úspory	úspora	k1gFnPc1	úspora
lze	lze	k6eAd1	lze
přičítat	přičítat	k5eAaImF	přičítat
používání	používání	k1gNnSc4	používání
měděných	měděný	k2eAgNnPc2d1	měděné
nitroděložních	nitroděložní	k2eAgNnPc2d1	nitroděložní
tělísek	tělísko	k1gNnPc2	tělísko
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
zdravotnické	zdravotnický	k2eAgInPc1d1	zdravotnický
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
těhotenství	těhotenství	k1gNnSc4	těhotenství
<g/>
,	,	kIx,	,
porod	porod	k1gInSc4	porod
a	a	k8xC	a
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
novorozence	novorozenec	k1gMnSc4	novorozenec
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hodnoty	hodnota	k1gFnSc2	hodnota
21	[number]	k4	21
000	[number]	k4	000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
případě	případ	k1gInSc6	případ
porodu	porod	k1gInSc2	porod
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
31	[number]	k4	31
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
případě	případ	k1gInSc6	případ
porodu	porod	k1gInSc2	porod
císařským	císařský	k2eAgInSc7d1	císařský
řezem	řez	k1gInSc7	řez
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zemí	zem	k1gFnPc2	zem
jsou	být	k5eAaImIp3nP	být
náklady	náklad	k1gInPc1	náklad
méně	málo	k6eAd2	málo
než	než	k8xS	než
poloviční	poloviční	k2eAgMnSc1d1	poloviční
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výchovu	výchova	k1gFnSc4	výchova
dítěte	dítě	k1gNnSc2	dítě
narozeného	narozený	k2eAgNnSc2d1	narozené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
utratí	utratit	k5eAaPmIp3nS	utratit
průměrná	průměrný	k2eAgFnSc1d1	průměrná
americká	americký	k2eAgFnSc1d1	americká
rodina	rodina	k1gFnSc1	rodina
za	za	k7c4	za
dalších	další	k2eAgNnPc2d1	další
17	[number]	k4	17
let	léto	k1gNnPc2	léto
235	[number]	k4	235
000	[number]	k4	000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
používalo	používat	k5eAaImAgNnS	používat
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
žijících	žijící	k2eAgFnPc2d1	žijící
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
mít	mít	k5eAaImF	mít
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
způsoby	způsob	k1gInPc1	způsob
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
používány	používán	k2eAgInPc1d1	používán
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
nejběžnějšími	běžný	k2eAgInPc7d3	nejběžnější
způsoby	způsob	k1gInPc7	způsob
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
kondomy	kondom	k1gInPc4	kondom
a	a	k8xC	a
orálně	orálně	k6eAd1	orálně
užívané	užívaný	k2eAgFnSc2d1	užívaná
antikoncepční	antikoncepční	k2eAgFnSc2d1	antikoncepční
tabletky	tabletka	k1gFnSc2	tabletka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
orální	orální	k2eAgFnSc1d1	orální
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
a	a	k8xC	a
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
sterilizace	sterilizace	k1gFnSc2	sterilizace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgNnSc2d1	celkové
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
z	z	k7c2	z
35	[number]	k4	35
%	%	kIx~	%
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
ženskou	ženský	k2eAgFnSc7d1	ženská
sterilizací	sterilizace	k1gFnSc7	sterilizace
<g/>
,	,	kIx,	,
z	z	k7c2	z
30	[number]	k4	30
%	%	kIx~	%
nitroděložními	nitroděložní	k2eAgNnPc7d1	nitroděložní
tělísky	tělísko	k1gNnPc7	tělísko
<g/>
,	,	kIx,	,
z	z	k7c2	z
12	[number]	k4	12
%	%	kIx~	%
orální	orální	k2eAgFnSc7d1	orální
antikoncepcí	antikoncepce	k1gFnSc7	antikoncepce
<g/>
,	,	kIx,	,
z	z	k7c2	z
11	[number]	k4	11
%	%	kIx~	%
kondomy	kondom	k1gInPc4	kondom
a	a	k8xC	a
ze	z	k7c2	z
4	[number]	k4	4
%	%	kIx~	%
mužskou	mužský	k2eAgFnSc7d1	mužská
sterilizací	sterilizace	k1gFnSc7	sterilizace
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
nitroděložní	nitroděložní	k2eAgNnPc4d1	nitroděložní
tělíska	tělísko	k1gNnPc4	tělísko
používána	používán	k2eAgFnSc1d1	používána
častěji	často	k6eAd2	často
než	než	k8xS	než
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
žen	žena	k1gFnPc2	žena
používajících	používající	k2eAgInPc2d1	používající
tuto	tento	k3xDgFnSc4	tento
metodu	metoda	k1gFnSc4	metoda
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
180	[number]	k4	180
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
sexu	sex	k1gInSc3	sex
během	během	k7c2	během
plodných	plodný	k2eAgInPc2d1	plodný
dnů	den	k1gInPc2	den
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
přibližně	přibližně	k6eAd1	přibližně
3,6	[number]	k4	3,6
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
plodném	plodný	k2eAgInSc6d1	plodný
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
využívalo	využívat	k5eAaPmAgNnS	využívat
mužskou	mužský	k2eAgFnSc4d1	mužská
formu	forma	k1gFnSc4	forma
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
(	(	kIx(	(
<g/>
kondomy	kondom	k1gInPc4	kondom
nebo	nebo	k8xC	nebo
vasektomii	vasektomie	k1gFnSc4	vasektomie
<g/>
)	)	kIx)	)
12	[number]	k4	12
%	%	kIx~	%
párů	pár	k1gInPc2	pár
zejména	zejména	k9	zejména
z	z	k7c2	z
rozvinutých	rozvinutý	k2eAgFnPc2d1	rozvinutá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
používání	používání	k1gNnSc2	používání
mužských	mužský	k2eAgFnPc2d1	mužská
forem	forma	k1gFnPc2	forma
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1985	[number]	k4	1985
a	a	k8xC	a
2009	[number]	k4	2009
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
používání	používání	k1gNnSc2	používání
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
těhotenství	těhotenství	k1gNnSc2	těhotenství
chtělo	chtít	k5eAaImAgNnS	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
57	[number]	k4	57
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
plodném	plodný	k2eAgInSc6d1	plodný
věku	věk	k1gInSc6	věk
(	(	kIx(	(
<g/>
867	[number]	k4	867
z	z	k7c2	z
1520	[number]	k4	1520
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
222	[number]	k4	222
milionů	milion	k4xCgInPc2	milion
žen	žena	k1gFnPc2	žena
však	však	k9	však
nemělo	mít	k5eNaImAgNnS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
53	[number]	k4	53
milionů	milion	k4xCgInPc2	milion
ze	z	k7c2	z
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
97	[number]	k4	97
milionů	milion	k4xCgInPc2	milion
žen	žena	k1gFnPc2	žena
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
má	mít	k5eAaImIp3nS	mít
ročně	ročně	k6eAd1	ročně
za	za	k7c4	za
následek	následek	k1gInSc4	následek
54	[number]	k4	54
milionů	milion	k4xCgInPc2	milion
neplánovaných	plánovaný	k2eNgInPc2d1	neplánovaný
těhotenství	těhotenství	k1gNnSc2	těhotenství
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
80	[number]	k4	80
000	[number]	k4	000
případů	případ	k1gInPc2	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Částečným	částečný	k2eAgInSc7d1	částečný
důvodem	důvod	k1gInSc7	důvod
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mnoho	mnoho	k4c1	mnoho
žen	žena	k1gFnPc2	žena
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
zemí	zem	k1gFnPc2	zem
omezuje	omezovat	k5eAaImIp3nS	omezovat
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
či	či	k8xC	či
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dalším	další	k2eAgInSc7d1	další
přispívajícím	přispívající	k2eAgInSc7d1	přispívající
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
chudoba	chudoba	k1gFnSc1	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přísných	přísný	k2eAgInPc2d1	přísný
zákonů	zákon	k1gInPc2	zákon
omezujících	omezující	k2eAgInPc2d1	omezující
potraty	potrat	k1gInPc7	potrat
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nechtěného	chtěný	k2eNgNnSc2d1	nechtěné
otěhotnění	otěhotnění	k1gNnSc2	otěhotnění
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
osoby	osoba	k1gFnPc4	osoba
provádějící	provádějící	k2eAgFnPc4d1	provádějící
potraty	potrat	k1gInPc4	potrat
nelegálně	legálně	k6eNd1	legálně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
každoročně	každoročně	k6eAd1	každoročně
vede	vést	k5eAaImIp3nS	vést
přibližně	přibližně	k6eAd1	přibližně
2-4	[number]	k4	2-4
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
k	k	k7c3	k
podstoupení	podstoupení	k1gNnSc3	podstoupení
nelegálního	legální	k2eNgInSc2d1	nelegální
potratu	potrat	k1gInSc2	potrat
<g/>
.	.	kIx.	.
</s>
<s>
Dohody	dohoda	k1gFnPc1	dohoda
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
po	po	k7c6	po
většině	většina	k1gFnSc6	většina
vlád	vláda	k1gFnPc2	vláda
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
plánování	plánování	k1gNnSc1	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
a	a	k8xC	a
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
i	i	k9	i
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
národního	národní	k2eAgInSc2d1	národní
plánu	plán	k1gInSc2	plán
pro	pro	k7c4	pro
služby	služba	k1gFnPc4	služba
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
plánováním	plánování	k1gNnSc7	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
<g/>
,	,	kIx,	,
zrušení	zrušení	k1gNnSc1	zrušení
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
omezují	omezovat	k5eAaImIp3nP	omezovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
plánování	plánování	k1gNnSc3	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
<g/>
,	,	kIx,	,
zajištění	zajištění	k1gNnSc2	zajištění
dostupnosti	dostupnost	k1gFnSc2	dostupnost
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
bezpečných	bezpečný	k2eAgFnPc2d1	bezpečná
a	a	k8xC	a
účinných	účinný	k2eAgFnPc2d1	účinná
metod	metoda	k1gFnPc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
včetně	včetně	k7c2	včetně
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
nouzové	nouzový	k2eAgFnPc1d1	nouzová
<g/>
,	,	kIx,	,
zajištění	zajištění	k1gNnSc1	zajištění
náležitě	náležitě	k6eAd1	náležitě
vyškolených	vyškolený	k2eAgMnPc2d1	vyškolený
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
za	za	k7c4	za
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
procesu	proces	k1gInSc2	proces
kontroly	kontrola	k1gFnSc2	kontrola
realizovaných	realizovaný	k2eAgInPc2d1	realizovaný
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vlády	vláda	k1gFnPc1	vláda
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnPc1d1	uvedená
nezajistí	zajistit	k5eNaPmIp3nP	zajistit
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
rozporu	rozpor	k1gInSc2	rozpor
se	s	k7c7	s
závazky	závazek	k1gInPc7	závazek
vyplývajícími	vyplývající	k2eAgInPc7d1	vyplývající
z	z	k7c2	z
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
hnutí	hnutí	k1gNnSc4	hnutí
Every	Evera	k1gMnSc2	Evera
Woman	Woman	k1gMnSc1	Woman
Every	Evera	k1gMnSc2	Evera
Child	Child	k1gMnSc1	Child
(	(	kIx(	(
<g/>
Každá	každý	k3xTgFnSc1	každý
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
dítě	dítě	k1gNnSc1	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
pokrok	pokrok	k1gInSc4	pokrok
ke	k	k7c3	k
splnění	splnění	k1gNnSc3	splnění
potřeb	potřeba	k1gFnPc2	potřeba
žen	žena	k1gFnPc2	žena
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Iniciativa	iniciativa	k1gFnSc1	iniciativa
si	se	k3xPyFc3	se
stanovila	stanovit	k5eAaPmAgFnS	stanovit
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zvýšit	zvýšit	k5eAaPmF	zvýšit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
počet	počet	k1gInSc1	počet
uživatelek	uživatelka	k1gFnPc2	uživatelka
moderní	moderní	k2eAgFnSc2d1	moderní
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
v	v	k7c6	v
69	[number]	k4	69
nejchudších	chudý	k2eAgFnPc6d3	nejchudší
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
o	o	k7c4	o
120	[number]	k4	120
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
iniciativa	iniciativa	k1gFnSc1	iniciativa
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c6	o
odstranění	odstranění	k1gNnSc6	odstranění
diskriminace	diskriminace	k1gFnSc2	diskriminace
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
mladých	mladý	k2eAgFnPc2d1	mladá
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
užívají	užívat	k5eAaImIp3nP	užívat
či	či	k8xC	či
chtějí	chtít	k5eAaImIp3nP	chtít
užívat	užívat	k5eAaImF	užívat
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
se	se	k3xPyFc4	se
v	v	k7c6	v
pohledu	pohled	k1gInSc6	pohled
na	na	k7c6	na
antikoncepci	antikoncepce	k1gFnSc6	antikoncepce
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
etiky	etika	k1gFnSc2	etika
značně	značně	k6eAd1	značně
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
oficiálně	oficiálně	k6eAd1	oficiálně
uznává	uznávat	k5eAaImIp3nS	uznávat
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
případech	případ	k1gInPc6	případ
pouze	pouze	k6eAd1	pouze
přirozené	přirozený	k2eAgNnSc1d1	přirozené
plánování	plánování	k1gNnSc1	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
řada	řada	k1gFnSc1	řada
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
moderní	moderní	k2eAgFnSc2d1	moderní
metody	metoda	k1gFnSc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
uznává	uznávat	k5eAaImIp3nS	uznávat
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
protestanty	protestant	k1gMnPc7	protestant
existuje	existovat	k5eAaImIp3nS	existovat
na	na	k7c4	na
používání	používání	k1gNnSc4	používání
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
široká	široký	k2eAgFnSc1d1	široká
řada	řada	k1gFnSc1	řada
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
od	od	k7c2	od
těch	ten	k3xDgFnPc2	ten
neuznávajících	uznávající	k2eNgFnPc2d1	neuznávající
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
až	až	k9	až
po	po	k7c4	po
skupiny	skupina	k1gFnPc4	skupina
připouštějící	připouštějící	k2eAgFnPc1d1	připouštějící
veškeré	veškerý	k3xTgFnPc1	veškerý
antikoncepční	antikoncepční	k2eAgFnPc1d1	antikoncepční
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Postoj	postoj	k1gInSc1	postoj
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
judaismu	judaismus	k1gInSc2	judaismus
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
-	-	kIx~	-
od	od	k7c2	od
přísnějšího	přísný	k2eAgInSc2d2	přísnější
v	v	k7c6	v
ortodoxní	ortodoxní	k2eAgFnSc6d1	ortodoxní
větvi	větev	k1gFnSc6	větev
po	po	k7c6	po
uvolněnější	uvolněný	k2eAgFnSc6d2	uvolněnější
ve	v	k7c6	v
větvi	větev	k1gFnSc6	větev
reformní	reformní	k2eAgNnSc4d1	reformní
<g/>
.	.	kIx.	.
</s>
<s>
Hinduismus	hinduismus	k1gInSc1	hinduismus
povoluje	povolovat	k5eAaImIp3nS	povolovat
používání	používání	k1gNnSc1	používání
přirozené	přirozený	k2eAgFnSc2d1	přirozená
i	i	k8xC	i
umělé	umělý	k2eAgFnSc2d1	umělá
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Jednotným	jednotný	k2eAgInSc7d1	jednotný
buddhistickým	buddhistický	k2eAgInSc7d1	buddhistický
názorem	názor	k1gInSc7	názor
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabránění	zabránění	k1gNnSc1	zabránění
početí	početí	k1gNnSc2	početí
je	být	k5eAaImIp3nS	být
přípustné	přípustný	k2eAgNnSc1d1	přípustné
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
zásah	zásah	k1gInSc4	zásah
po	po	k7c6	po
početí	početí	k1gNnSc6	početí
už	už	k9	už
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
islámu	islám	k1gInSc6	islám
je	být	k5eAaImIp3nS	být
antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
povolena	povolit	k5eAaPmNgFnS	povolit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
někteří	některý	k3yIgMnPc1	některý
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
používání	používání	k1gNnSc2	používání
zrazují	zrazovat	k5eAaImIp3nP	zrazovat
<g/>
.	.	kIx.	.
</s>
<s>
Korán	korán	k1gInSc1	korán
se	se	k3xPyFc4	se
o	o	k7c6	o
morálnosti	morálnost	k1gFnSc6	morálnost
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
nijak	nijak	k6eAd1	nijak
výslovně	výslovně	k6eAd1	výslovně
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
však	však	k9	však
výroky	výrok	k1gInPc4	výrok
podporující	podporující	k2eAgInPc4d1	podporující
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
mít	mít	k5eAaImF	mít
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Prorok	prorok	k1gMnSc1	prorok
Mohamed	Mohamed	k1gMnSc1	Mohamed
prý	prý	k9	prý
rovněž	rovněž	k9	rovněž
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vstupujte	vstupovat	k5eAaImRp2nP	vstupovat
ve	v	k7c4	v
svazek	svazek	k1gInSc4	svazek
manželský	manželský	k2eAgInSc4d1	manželský
a	a	k8xC	a
množte	množit	k5eAaImRp2nP	množit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
stávajících	stávající	k2eAgFnPc2d1	stávající
metod	metoda	k1gFnPc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
jsou	být	k5eAaImIp3nP	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
určitá	určitý	k2eAgNnPc1d1	určité
zlepšení	zlepšení	k1gNnPc1	zlepšení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
neúmyslně	úmyslně	k6eNd1	úmyslně
otěhotní	otěhotnět	k5eAaPmIp3nP	otěhotnět
<g/>
,	,	kIx,	,
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
úpravy	úprava	k1gFnPc4	úprava
stávajících	stávající	k2eAgFnPc2d1	stávající
metod	metoda	k1gFnPc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lepšího	dobrý	k2eAgInSc2d2	lepší
ženského	ženský	k2eAgInSc2d1	ženský
kondomu	kondom	k1gInSc2	kondom
<g/>
,	,	kIx,	,
zdokonaleného	zdokonalený	k2eAgInSc2d1	zdokonalený
pesaru	pesar	k1gInSc2	pesar
<g/>
,	,	kIx,	,
náplastí	náplast	k1gFnPc2	náplast
obsahujících	obsahující	k2eAgFnPc6d1	obsahující
pouze	pouze	k6eAd1	pouze
progestin	progestin	k2eAgMnSc1d1	progestin
a	a	k8xC	a
vaginálního	vaginální	k2eAgInSc2d1	vaginální
kroužku	kroužek	k1gInSc2	kroužek
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
působící	působící	k2eAgInSc1d1	působící
progesteron	progesteron	k1gInSc1	progesteron
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vaginální	vaginální	k2eAgInSc1d1	vaginální
kroužek	kroužek	k1gInSc1	kroužek
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
účinným	účinný	k2eAgInSc7d1	účinný
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgInPc2	tři
nebo	nebo	k8xC	nebo
čtyř	čtyři	k4xCgInPc2	čtyři
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
již	již	k6eAd1	již
dostupný	dostupný	k2eAgInSc1d1	dostupný
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
i	i	k9	i
řadou	řada	k1gFnSc7	řada
způsobů	způsob	k1gInPc2	způsob
provedení	provedení	k1gNnSc2	provedení
sterilizace	sterilizace	k1gFnSc2	sterilizace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
děložního	děložní	k2eAgNnSc2d1	děložní
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vložení	vložení	k1gNnSc4	vložení
quinakrinu	quinakrin	k1gInSc2	quinakrin
do	do	k7c2	do
dělohy	děloha	k1gFnSc2	děloha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
zjizvení	zjizvení	k1gNnSc4	zjizvení
a	a	k8xC	a
neplodnost	neplodnost	k1gFnSc4	neplodnost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
nenákladný	nákladný	k2eNgInSc1d1	nenákladný
a	a	k8xC	a
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
zásah	zásah	k1gInSc1	zásah
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
obavy	obava	k1gFnPc4	obava
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
polidokanol	polidokanol	k1gInSc1	polidokanol
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
dalších	další	k2eAgNnPc2d1	další
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
schváleno	schválit	k5eAaPmNgNnS	schválit
zařízení	zařízení	k1gNnSc1	zařízení
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Essure	Essur	k1gMnSc5	Essur
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
vložení	vložení	k1gNnSc6	vložení
do	do	k7c2	do
vejcovodů	vejcovod	k1gInPc2	vejcovod
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
a	a	k8xC	a
zablokuje	zablokovat	k5eAaPmIp3nS	zablokovat
je	on	k3xPp3gNnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
antikoncepční	antikoncepční	k2eAgFnPc4d1	antikoncepční
metody	metoda	k1gFnPc4	metoda
užívané	užívaný	k2eAgFnSc2d1	užívaná
muži	muž	k1gMnSc3	muž
patří	patřit	k5eAaImIp3nP	patřit
kondomy	kondom	k1gInPc4	kondom
<g/>
,	,	kIx,	,
vasektomie	vasektomie	k1gFnSc1	vasektomie
a	a	k8xC	a
vyjmutí	vyjmutí	k1gNnSc1	vyjmutí
penisu	penis	k1gInSc2	penis
z	z	k7c2	z
vagíny	vagína	k1gFnSc2	vagína
před	před	k7c7	před
ejakulací	ejakulace	k1gFnSc7	ejakulace
<g/>
.	.	kIx.	.
</s>
<s>
Hormonální	hormonální	k2eAgFnSc4d1	hormonální
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
by	by	kYmCp3nS	by
používalo	používat	k5eAaImAgNnS	používat
25	[number]	k4	25
až	až	k8xS	až
75	[number]	k4	75
%	%	kIx~	%
sexuálně	sexuálně	k6eAd1	sexuálně
aktivních	aktivní	k2eAgMnPc2d1	aktivní
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byla	být	k5eAaImAgFnS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
hormonálních	hormonální	k2eAgFnPc2d1	hormonální
i	i	k8xC	i
nehormonálních	hormonální	k2eNgFnPc2d1	nehormonální
metod	metoda	k1gFnPc2	metoda
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
probíhá	probíhat	k5eAaImIp3nS	probíhat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
antikoncepčních	antikoncepční	k2eAgFnPc2d1	antikoncepční
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zkoumanou	zkoumaný	k2eAgFnSc7d1	zkoumaná
metodou	metoda	k1gFnSc7	metoda
vratného	vratný	k2eAgInSc2d1	vratný
chirurgického	chirurgický	k2eAgInSc2d1	chirurgický
zákroku	zákrok	k1gInSc2	zákrok
je	být	k5eAaImIp3nS	být
reverzibilní	reverzibilní	k2eAgFnSc1d1	reverzibilní
kontrolovaná	kontrolovaný	k2eAgFnSc1d1	kontrolovaná
inhibice	inhibice	k1gFnSc1	inhibice
spermatu	sperma	k1gNnSc2	sperma
(	(	kIx(	(
<g/>
RISUG	RISUG	kA	RISUG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
injekční	injekční	k2eAgFnSc2d1	injekční
aplikace	aplikace	k1gFnSc2	aplikace
polymerového	polymerový	k2eAgInSc2d1	polymerový
gelu	gel	k1gInSc2	gel
<g/>
,	,	kIx,	,
styren-maleinanhydridu	styrenaleinanhydrid	k1gInSc2	styren-maleinanhydrid
v	v	k7c6	v
dimethylsulfoxidu	dimethylsulfoxid	k1gInSc6	dimethylsulfoxid
<g/>
,	,	kIx,	,
do	do	k7c2	do
chámovodu	chámovod	k1gInSc2	chámovod
<g/>
.	.	kIx.	.
</s>
<s>
Injekce	injekce	k1gFnSc1	injekce
hydrogenuhličitanu	hydrogenuhličitan	k1gInSc2	hydrogenuhličitan
sodného	sodný	k2eAgInSc2d1	sodný
tuto	tento	k3xDgFnSc4	tento
látku	látka	k1gFnSc4	látka
vypláchne	vypláchnout	k5eAaPmIp3nS	vypláchnout
a	a	k8xC	a
obnoví	obnovit	k5eAaPmIp3nS	obnovit
tak	tak	k9	tak
plodnost	plodnost	k1gFnSc1	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
nitrochámovodové	nitrochámovodový	k2eAgNnSc4d1	nitrochámovodový
tělísko	tělísko	k1gNnSc4	tělísko
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vložení	vložení	k1gNnSc4	vložení
uretanové	uretanový	k2eAgFnSc2d1	uretanová
zátky	zátka	k1gFnSc2	zátka
do	do	k7c2	do
chámovodu	chámovod	k1gInSc2	chámovod
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
následné	následný	k2eAgNnSc4d1	následné
ucpání	ucpání	k1gNnSc4	ucpání
<g/>
.	.	kIx.	.
</s>
<s>
Slibnou	slibný	k2eAgFnSc7d1	slibná
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
kombinace	kombinace	k1gFnSc1	kombinace
estrogenu	estrogen	k1gInSc2	estrogen
a	a	k8xC	a
progestinu	progestin	k1gInSc2	progestin
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
selektivní	selektivní	k2eAgInPc1d1	selektivní
modulátory	modulátor	k1gInPc1	modulátor
estrogenních	estrogenní	k2eAgInPc2d1	estrogenní
receptorů	receptor	k1gInPc2	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
předběžných	předběžný	k2eAgNnPc2d1	předběžné
studií	studio	k1gNnPc2	studio
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
ultrazvuku	ultrazvuk	k1gInSc2	ultrazvuk
a	a	k8xC	a
způsobů	způsob	k1gInPc2	způsob
zahřívání	zahřívání	k1gNnPc2	zahřívání
varlat	varle	k1gNnPc2	varle
<g/>
.	.	kIx.	.
</s>
<s>
Sterilizace	sterilizace	k1gFnSc1	sterilizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
odnětí	odnětí	k1gNnSc4	odnětí
některých	některý	k3yIgFnPc2	některý
z	z	k7c2	z
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
prováděným	prováděný	k2eAgInSc7d1	prováděný
způsobem	způsob	k1gInSc7	způsob
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
u	u	k7c2	u
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zvířecích	zvířecí	k2eAgInPc2d1	zvířecí
útulků	útulek	k1gInPc2	útulek
tyto	tento	k3xDgInPc4	tento
postupy	postup	k1gInPc4	postup
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velkých	velký	k2eAgNnPc2d1	velké
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
chirurgický	chirurgický	k2eAgInSc4d1	chirurgický
zákrok	zákrok	k1gInSc4	zákrok
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
kastrace	kastrace	k1gFnSc1	kastrace
<g/>
.	.	kIx.	.
</s>
<s>
Antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
zvažována	zvažován	k2eAgFnSc1d1	zvažována
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc1	prostředek
regulace	regulace	k1gFnSc2	regulace
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
populace	populace	k1gFnSc2	populace
divokých	divoký	k2eAgNnPc2d1	divoké
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
řady	řada	k1gFnSc2	řada
různých	různý	k2eAgFnPc2d1	různá
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
populací	populace	k1gFnPc2	populace
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
účinné	účinný	k2eAgFnPc4d1	účinná
antikoncepční	antikoncepční	k2eAgFnPc4d1	antikoncepční
vakcíny	vakcína	k1gFnPc4	vakcína
<g/>
.	.	kIx.	.
</s>
