<p>
<s>
Joseph	Joseph	k1gInSc1	Joseph
Mallord	Mallorda	k1gFnPc2	Mallorda
William	William	k1gInSc1	William
Turner	turner	k1gMnSc1	turner
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1775	[number]	k4	1775
v	v	k7c4	v
Covent	Covent	k1gInSc4	Covent
Garden	Gardna	k1gFnPc2	Gardna
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1851	[number]	k4	1851
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Chelsea	Chelseus	k1gMnSc4	Chelseus
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
romantický	romantický	k2eAgMnSc1d1	romantický
krajinář	krajinář	k1gMnSc1	krajinář
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
impresionismu	impresionismus	k1gInSc2	impresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
rozporuplnou	rozporuplný	k2eAgFnSc4d1	rozporuplná
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
považujeme	považovat	k5eAaImIp1nP	považovat
jej	on	k3xPp3gInSc4	on
dnes	dnes	k6eAd1	dnes
za	za	k7c4	za
umělce	umělec	k1gMnSc4	umělec
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
poměrně	poměrně	k6eAd1	poměrně
přehlížené	přehlížený	k2eAgFnSc3d1	přehlížená
krajinomalbě	krajinomalba	k1gFnSc3	krajinomalba
dostalo	dostat	k5eAaPmAgNnS	dostat
téhož	týž	k3xTgNnSc2	týž
uznání	uznání	k1gNnSc2	uznání
<g/>
,	,	kIx,	,
jakému	jaký	k3yRgNnSc3	jaký
se	se	k3xPyFc4	se
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
těšila	těšit	k5eAaImAgFnS	těšit
malba	malba	k1gFnSc1	malba
historická	historický	k2eAgFnSc1d1	historická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Turner	turner	k1gMnSc1	turner
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Covent	Covent	k1gInSc1	Covent
Garden	Gardna	k1gFnPc2	Gardna
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
nad	nad	k7c7	nad
holičstvím	holičství	k1gNnSc7	holičství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
patřilo	patřit	k5eAaImAgNnS	patřit
jeho	jeho	k3xOp3gMnSc6	jeho
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
trpěla	trpět	k5eAaImAgFnS	trpět
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
dcery	dcera	k1gFnSc2	dcera
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1786	[number]	k4	1786
značnými	značný	k2eAgFnPc7d1	značná
depresemi	deprese	k1gFnPc7	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
právě	právě	k9	právě
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
William	William	k1gInSc1	William
jako	jako	k8xC	jako
desetiletý	desetiletý	k2eAgInSc1d1	desetiletý
poslán	poslán	k2eAgInSc1d1	poslán
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
do	do	k7c2	do
Brentfordu	Brentford	k1gInSc2	Brentford
západně	západně	k6eAd1	západně
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
poprvé	poprvé	k6eAd1	poprvé
projevil	projevit	k5eAaPmAgInS	projevit
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
kreslení	kreslení	k1gNnSc4	kreslení
a	a	k8xC	a
malování	malování	k1gNnSc4	malování
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgMnS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Margate	Margat	k1gInSc5	Margat
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
hrabství	hrabství	k1gNnSc2	hrabství
Kent	Kenta	k1gFnPc2	Kenta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
namaloval	namalovat	k5eAaPmAgMnS	namalovat
řadu	řada	k1gFnSc4	řada
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
ve	v	k7c6	v
výloze	výloha	k1gFnSc6	výloha
svého	svůj	k3xOyFgNnSc2	svůj
holičství	holičství	k1gNnSc2	holičství
a	a	k8xC	a
prodával	prodávat	k5eAaImAgMnS	prodávat
zájemcům	zájemce	k1gMnPc3	zájemce
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
William	William	k1gInSc1	William
dostal	dostat	k5eAaPmAgInS	dostat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	let	k1gInPc6	let
na	na	k7c4	na
Královskou	královský	k2eAgFnSc4d1	královská
akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
kopíroval	kopírovat	k5eAaImAgInS	kopírovat
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgMnPc4d1	bohatý
zákazníky	zákazník	k1gMnPc4	zákazník
obrazy	obraz	k1gInPc4	obraz
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
vybarvoval	vybarvovat	k5eAaImAgMnS	vybarvovat
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
různé	různý	k2eAgInPc4d1	různý
černobílé	černobílý	k2eAgInPc4d1	černobílý
tisky	tisk	k1gInPc4	tisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
zkušební	zkušební	k2eAgFnSc4d1	zkušební
dobu	doba	k1gFnSc4	doba
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Královskou	královský	k2eAgFnSc4d1	královská
akademii	akademie	k1gFnSc4	akademie
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
každoroční	každoroční	k2eAgFnSc6d1	každoroční
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
pro	pro	k7c4	pro
britské	britský	k2eAgMnPc4d1	britský
malíře	malíř	k1gMnPc4	malíř
znamená	znamenat	k5eAaImIp3nS	znamenat
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
pro	pro	k7c4	pro
francouzské	francouzský	k2eAgInPc4d1	francouzský
Salon	salon	k1gInSc4	salon
<g/>
,	,	kIx,	,
vystaven	vystavit	k5eAaPmNgInS	vystavit
jeho	jeho	k3xOp3gInSc4	jeho
akvarel	akvarel	k1gInSc4	akvarel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
oficiálně	oficiálně	k6eAd1	oficiálně
vystavený	vystavený	k2eAgInSc1d1	vystavený
Turnerův	turnerův	k2eAgInSc1d1	turnerův
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
dva	dva	k4xCgInPc4	dva
akvarely	akvarel	k1gInPc4	akvarel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
objevovaly	objevovat	k5eAaImAgInP	objevovat
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
Akademie	akademie	k1gFnSc2	akademie
téměř	téměř	k6eAd1	téměř
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
obdržel	obdržet	k5eAaPmAgMnS	obdržet
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
plaketu	plaketa	k1gFnSc4	plaketa
Akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
krajinomalbu	krajinomalba	k1gFnSc4	krajinomalba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
vystavil	vystavit	k5eAaPmAgMnS	vystavit
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
olejomalbu	olejomalba	k1gFnSc4	olejomalba
Rybáři	rybář	k1gMnPc1	rybář
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
přidruženým	přidružený	k2eAgMnSc7d1	přidružený
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Řádného	řádný	k2eAgNnSc2d1	řádné
členství	členství	k1gNnSc2	členství
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgNnPc4	jenž
léta	léto	k1gNnPc4	léto
usiloval	usilovat	k5eAaImAgMnS	usilovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
<g/>
.	.	kIx.	.
</s>
<s>
Zvolení	zvolení	k1gNnSc1	zvolení
do	do	k7c2	do
akademické	akademický	k2eAgFnSc2d1	akademická
rady	rada	k1gFnSc2	rada
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
nedalo	dát	k5eNaPmAgNnS	dát
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
přišlo	přijít	k5eAaPmAgNnS	přijít
jmenování	jmenování	k1gNnSc1	jmenování
profesorem	profesor	k1gMnSc7	profesor
perspektivy	perspektiva	k1gFnSc2	perspektiva
na	na	k7c4	na
Akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Umělec	umělec	k1gMnSc1	umělec
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
přednášet	přednášet	k5eAaImF	přednášet
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
přednáškového	přednáškový	k2eAgInSc2d1	přednáškový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
nejen	nejen	k6eAd1	nejen
pravidla	pravidlo	k1gNnSc2	pravidlo
lineární	lineární	k2eAgFnSc2d1	lineární
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zabýval	zabývat	k5eAaImAgMnS	zabývat
se	s	k7c7	s
prostorovým	prostorový	k2eAgNnSc7d1	prostorové
zobrazením	zobrazení	k1gNnSc7	zobrazení
jako	jako	k8xS	jako
celkem	celek	k1gInSc7	celek
včetně	včetně	k7c2	včetně
pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
způsobech	způsob	k1gInPc6	způsob
členění	členění	k1gNnSc2	členění
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
zabrala	zabrat	k5eAaPmAgFnS	zabrat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Turnerova	turnerův	k2eAgFnSc1d1	Turnerova
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
o	o	k7c4	o
chod	chod	k1gInSc4	chod
jeho	jeho	k3xOp3gFnSc2	jeho
domácnosti	domácnost	k1gFnSc2	domácnost
staral	starat	k5eAaImAgInS	starat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
r.	r.	kA	r.
1829	[number]	k4	1829
jeho	jeho	k3xOp3gNnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
mladý	mladý	k2eAgMnSc1d1	mladý
umělec	umělec	k1gMnSc1	umělec
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
londýnském	londýnský	k2eAgInSc6d1	londýnský
domě	dům	k1gInSc6	dům
galerii	galerie	k1gFnSc4	galerie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
začal	začít	k5eAaPmAgInS	začít
vystavovat	vystavovat	k5eAaImF	vystavovat
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turner	turner	k1gMnSc1	turner
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
sbíral	sbírat	k5eAaImAgMnS	sbírat
podněty	podnět	k1gInPc4	podnět
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Skotska	Skotsko	k1gNnSc2	Skotsko
se	se	k3xPyFc4	se
vydával	vydávat	k5eAaImAgInS	vydávat
i	i	k9	i
za	za	k7c4	za
kanál	kanál	k1gInSc4	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1817	[number]	k4	1817
a	a	k8xC	a
1825	[number]	k4	1825
putoval	putovat	k5eAaImAgInS	putovat
po	po	k7c6	po
krajině	krajina	k1gFnSc6	krajina
podél	podél	k7c2	podél
kultovní	kultovní	k2eAgFnSc2d1	kultovní
německé	německý	k2eAgFnSc2d1	německá
řeky	řeka	k1gFnSc2	řeka
Rýna	Rýn	k1gInSc2	Rýn
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
navštívil	navštívit	k5eAaPmAgMnS	navštívit
poprvé	poprvé	k6eAd1	poprvé
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
poznávat	poznávat	k5eAaImF	poznávat
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgFnPc2	tento
cest	cesta	k1gFnPc2	cesta
bylo	být	k5eAaImAgNnS	být
takové	takový	k3xDgNnSc1	takový
množství	množství	k1gNnSc1	množství
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
nucen	nutit	k5eAaImNgMnS	nutit
svoji	svůj	k3xOyFgFnSc4	svůj
galerii	galerie	k1gFnSc4	galerie
značně	značně	k6eAd1	značně
rozšířit	rozšířit	k5eAaPmF	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vypravil	vypravit	k5eAaPmAgInS	vypravit
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
Přes	přes	k7c4	přes
Berlín	Berlín	k1gInSc4	Berlín
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
Turnera	turner	k1gMnSc2	turner
stával	stávat	k5eAaImAgInS	stávat
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
podivín	podivín	k1gMnSc1	podivín
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
žil	žít	k5eAaImAgMnS	žít
po	po	k7c4	po
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
domácnosti	domácnost	k1gFnSc6	domácnost
a	a	k8xC	a
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
vypomáhal	vypomáhat	k5eAaImAgInS	vypomáhat
i	i	k9	i
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
hrstku	hrstka	k1gFnSc4	hrstka
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Otcova	otcův	k2eAgFnSc1d1	otcova
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
umělce	umělec	k1gMnSc2	umělec
hluboce	hluboko	k6eAd1	hluboko
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
víc	hodně	k6eAd2	hodně
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
záležitostí	záležitost	k1gFnPc2	záležitost
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
Akademií	akademie	k1gFnSc7	akademie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
znamenala	znamenat	k5eAaImAgFnS	znamenat
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
odřezával	odřezávat	k5eAaImAgMnS	odřezávat
od	od	k7c2	od
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
letech	let	k1gInPc6	let
dokonce	dokonce	k9	dokonce
tajil	tajit	k5eAaImAgInS	tajit
svoji	svůj	k3xOyFgFnSc4	svůj
adresu	adresa	k1gFnSc4	adresa
a	a	k8xC	a
vydával	vydávat	k5eAaPmAgMnS	vydávat
se	se	k3xPyFc4	se
za	za	k7c4	za
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Nestýkal	stýkat	k5eNaImAgInS	stýkat
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dvěma	dva	k4xCgFnPc7	dva
nemanželskými	manželský	k2eNgFnPc7d1	nemanželská
dcerami	dcera	k1gFnPc7	dcera
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
Sarah	Sarah	k1gFnSc7	Sarah
Danbyovou	Danbyový	k2eAgFnSc7d1	Danbyový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
přednášení	přednášení	k1gNnSc2	přednášení
na	na	k7c6	na
Královské	královský	k2eAgFnSc6d1	královská
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
opětovně	opětovně	k6eAd1	opětovně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Benátky	Benátky	k1gFnPc4	Benátky
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
,	,	kIx,	,
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
a	a	k8xC	a
severní	severní	k2eAgFnSc4d1	severní
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc7	jeho
posledními	poslední	k2eAgFnPc7d1	poslední
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
cestami	cesta	k1gFnPc7	cesta
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc4	dva
krátké	krátký	k2eAgFnPc4d1	krátká
návštěvy	návštěva	k1gFnPc4	návštěva
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
člen	člen	k1gMnSc1	člen
Královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jejím	její	k3xOp3gMnSc7	její
auditorem	auditor	k1gMnSc7	auditor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
i	i	k9	i
umělcův	umělcův	k2eAgInSc1d1	umělcův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Turnerova	turnerův	k2eAgFnSc1d1	Turnerova
poslední	poslední	k2eAgFnSc1d1	poslední
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
Královské	královský	k2eAgFnSc6d1	královská
akademii	akademie	k1gFnSc6	akademie
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
Joseph	Josepha	k1gFnPc2	Josepha
Mallord	Mallorda	k1gFnPc2	Mallorda
William	William	k1gInSc1	William
Turner	turner	k1gMnSc1	turner
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
své	svůj	k3xOyFgFnSc2	svůj
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
Sophie	Sophie	k1gFnSc2	Sophie
Caroliny	Carolin	k2eAgFnSc2d1	Carolina
Boothové	Boothová	k1gFnSc2	Boothová
v	v	k7c4	v
Chelsea	Chelseus	k1gMnSc4	Chelseus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
poslední	poslední	k2eAgNnPc4d1	poslední
slova	slovo	k1gNnPc4	slovo
prý	prý	k9	prý
zněla	znět	k5eAaImAgFnS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
žádost	žádost	k1gFnSc4	žádost
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
Turner	turner	k1gMnSc1	turner
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
na	na	k7c6	na
řadu	řad	k1gInSc6	řad
míst	místo	k1gNnPc2	místo
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
vracel	vracet	k5eAaImAgMnS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
si	se	k3xPyFc3	se
vypěstoval	vypěstovat	k5eAaPmAgMnS	vypěstovat
cit	cit	k1gInSc4	cit
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
nálada	nálada	k1gFnSc1	nálada
a	a	k8xC	a
vzhled	vzhled	k1gInSc1	vzhled
určitého	určitý	k2eAgNnSc2d1	určité
místa	místo	k1gNnSc2	místo
mění	měnit	k5eAaImIp3nP	měnit
působením	působení	k1gNnSc7	působení
vlivů	vliv	k1gInPc2	vliv
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Častokrát	častokrát	k6eAd1	častokrát
opakovaně	opakovaně	k6eAd1	opakovaně
maloval	malovat	k5eAaImAgMnS	malovat
stejné	stejný	k2eAgNnSc4d1	stejné
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tyto	tento	k3xDgFnPc4	tento
změny	změna	k1gFnPc4	změna
zachytil	zachytit	k5eAaPmAgMnS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
nesmírně	smírně	k6eNd1	smírně
rád	rád	k6eAd1	rád
slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
malovat	malovat	k5eAaImF	malovat
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
nikdo	nikdo	k3yNnSc1	nikdo
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
rád	rád	k6eAd1	rád
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vydával	vydávat	k5eAaImAgInS	vydávat
na	na	k7c4	na
plavby	plavba	k1gFnPc4	plavba
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
rybářských	rybářský	k2eAgFnPc6d1	rybářská
lodích	loď	k1gFnPc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
takové	takový	k3xDgFnSc3	takový
plavbě	plavba	k1gFnSc3	plavba
přes	přes	k7c4	přes
kanál	kanál	k1gInSc4	kanál
La	la	k1gNnSc2	la
Manche	Manch	k1gFnSc2	Manch
ho	on	k3xPp3gMnSc4	on
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
málem	málem	k6eAd1	málem
potopila	potopit	k5eAaPmAgFnS	potopit
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
přistáli	přistát	k5eAaImAgMnP	přistát
<g/>
,	,	kIx,	,
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
svůj	svůj	k3xOyFgInSc4	svůj
náčrtník	náčrtník	k1gInSc4	náčrtník
a	a	k8xC	a
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
a	a	k8xC	a
ve	v	k7c6	v
větru	vítr	k1gInSc6	vítr
začal	začít	k5eAaPmAgMnS	začít
rozbouřené	rozbouřený	k2eAgNnSc4d1	rozbouřené
moře	moře	k1gNnSc4	moře
kreslit	kreslit	k5eAaImF	kreslit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
skici	skica	k1gFnSc2	skica
obraz	obraz	k1gInSc1	obraz
Přístaviště	přístaviště	k1gNnSc2	přístaviště
v	v	k7c6	v
Calais	Calais	k1gNnSc6	Calais
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
i	i	k9	i
historka	historka	k1gFnSc1	historka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
od	od	k7c2	od
námořníků	námořník	k1gMnPc2	námořník
na	na	k7c6	na
parníku	parník	k1gInSc6	parník
přivázat	přivázat	k5eAaPmF	přivázat
ke	k	k7c3	k
stěžni	stěžeň	k1gInSc3	stěžeň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
zakusit	zakusit	k5eAaPmF	zakusit
bouři	bouře	k1gFnSc4	bouře
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
Parník	parník	k1gInSc1	parník
ve	v	k7c6	v
sněhové	sněhový	k2eAgFnSc6d1	sněhová
bouři	bouř	k1gFnSc6	bouř
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turner	turner	k1gMnSc1	turner
maloval	malovat	k5eAaImAgMnS	malovat
často	často	k6eAd1	často
i	i	k9	i
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
a	a	k8xC	a
odkud	odkud	k6eAd1	odkud
vytěžil	vytěžit	k5eAaPmAgMnS	vytěžit
námět	námět	k1gInSc4	námět
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
dramatických	dramatický	k2eAgInPc2d1	dramatický
akvarelů	akvarel	k1gInPc2	akvarel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
Benátek	Benátky	k1gFnPc2	Benátky
byly	být	k5eAaImAgFnP	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
snové	snový	k2eAgFnSc3d1	snová
náladě	nálada	k1gFnSc3	nálada
a	a	k8xC	a
nádhernému	nádherný	k2eAgNnSc3d1	nádherné
světlu	světlo	k1gNnSc3	světlo
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
sběratelů	sběratel	k1gMnPc2	sběratel
značně	značně	k6eAd1	značně
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
dílo	dílo	k1gNnSc4	dílo
však	však	k9	však
sám	sám	k3xTgMnSc1	sám
považoval	považovat	k5eAaImAgMnS	považovat
obraz	obraz	k1gInSc4	obraz
Dido	Dido	k6eAd1	Dido
budující	budující	k2eAgNnSc4d1	budující
Kartágo	Kartágo	k1gNnSc4	Kartágo
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
krajinám	krajina	k1gFnPc3	krajina
francouzského	francouzský	k2eAgMnSc2d1	francouzský
barokního	barokní	k2eAgMnSc2d1	barokní
malíře	malíř	k1gMnSc2	malíř
Claude	Claud	k1gInSc5	Claud
Lorraina	Lorrain	k2eAgMnSc2d1	Lorrain
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
Turner	turner	k1gMnSc1	turner
velmi	velmi	k6eAd1	velmi
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
pozornost	pozornost	k1gFnSc4	pozornost
jako	jako	k8xC	jako
příběhu	příběh	k1gInSc3	příběh
královny	královna	k1gFnSc2	královna
Dido	Dido	k6eAd1	Dido
zakládající	zakládající	k2eAgNnSc4d1	zakládající
starověké	starověký	k2eAgNnSc4d1	starověké
Kartágo	Kartágo	k1gNnSc4	Kartágo
zde	zde	k6eAd1	zde
Turner	turner	k1gMnSc1	turner
věnoval	věnovat	k5eAaImAgMnS	věnovat
měkkému	měkký	k2eAgNnSc3d1	měkké
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
svitu	svit	k1gInSc2	svit
zaplňujícímu	zaplňující	k2eAgInSc3d1	zaplňující
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
<g/>
Turner	turner	k1gMnSc1	turner
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
skvělého	skvělý	k2eAgNnSc2d1	skvělé
mistrovství	mistrovství	k1gNnSc2	mistrovství
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
olejomalbě	olejomalba	k1gFnSc6	olejomalba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
i	i	k9	i
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
britských	britský	k2eAgMnPc2d1	britský
akvarelistů	akvarelista	k1gMnPc2	akvarelista
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
čelného	čelný	k2eAgMnSc4d1	čelný
britského	britský	k2eAgMnSc4d1	britský
krajináře	krajinář	k1gMnSc4	krajinář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vybraná	vybraný	k2eAgNnPc1d1	vybrané
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
1799	[number]	k4	1799
–	–	k?	–
Zámek	zámek	k1gInSc1	zámek
Dolbadern	Dolbadern	k1gInSc1	Dolbadern
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
<g/>
,	,	kIx,	,
tužka	tužka	k1gFnSc1	tužka
a	a	k8xC	a
akvarel	akvarel	k1gInSc1	akvarel
67,7	[number]	k4	67,7
x	x	k?	x
97,2	[number]	k4	97,2
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Tate	Tate	k1gNnSc2	Tate
Gallery	Galler	k1gMnPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1803	[number]	k4	1803
–	–	k?	–
Přístaviště	přístaviště	k1gNnSc2	přístaviště
v	v	k7c6	v
Calais	Calais	k1gNnSc6	Calais
s	s	k7c7	s
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
rybáři	rybář	k1gMnPc7	rybář
chystajícími	chystající	k2eAgMnPc7d1	chystající
se	se	k3xPyFc4	se
na	na	k7c6	na
moře	mora	k1gFnSc6	mora
<g/>
.	.	kIx.	.
</s>
<s>
Příjezd	příjezd	k1gInSc1	příjezd
anglické	anglický	k2eAgFnSc2d1	anglická
osobní	osobní	k2eAgFnSc2d1	osobní
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
olej	olej	k1gInSc4	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
172	[number]	k4	172
x	x	k?	x
240	[number]	k4	240
cm	cm	kA	cm
<g/>
,	,	kIx,	,
National	National	k1gFnSc1	National
Gallery	Galler	k1gInPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1806	[number]	k4	1806
–	–	k?	–
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Trafalgaru	Trafalgar	k1gInSc2	Trafalgar
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
od	od	k7c2	od
úponů	úpon	k1gInPc2	úpon
vratiplachty	vratiplachta	k1gFnSc2	vratiplachta
na	na	k7c6	na
pravoboku	pravobok	k1gInSc6	pravobok
lodi	loď	k1gFnSc2	loď
Victory	Victor	k1gMnPc4	Victor
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
172,7	[number]	k4	172,7
x	x	k?	x
238,8	[number]	k4	238,8
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Tate	Tate	k1gNnSc2	Tate
Gallery	Galler	k1gMnPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1812	[number]	k4	1812
–	–	k?	–
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
bouře	bouře	k1gFnSc1	bouře
<g/>
:	:	kIx,	:
Hannibal	Hannibal	k1gInSc1	Hannibal
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
přecházejí	přecházet	k5eAaImIp3nP	přecházet
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
146	[number]	k4	146
x	x	k?	x
237,5	[number]	k4	237,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
<g/>
Tate	Tate	k1gNnSc2	Tate
Gallery	Galler	k1gMnPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1817	[number]	k4	1817
–	–	k?	–
Výbuch	výbuch	k1gInSc1	výbuch
Vesuvu	Vesuv	k1gInSc2	Vesuv
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
Yale	Yale	k1gFnSc6	Yale
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
British	British	k1gMnSc1	British
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Haven	Haven	k1gInSc1	Haven
<g/>
,	,	kIx,	,
Connecticut	Connecticut	k1gInSc1	Connecticut
</s>
</p>
<p>
<s>
1822	[number]	k4	1822
–	–	k?	–
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Trafalgaru	Trafalgar	k1gInSc2	Trafalgar
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
National	National	k1gMnSc5	National
Maritime	Maritim	k1gMnSc5	Maritim
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Greenwich	Greenwich	k1gInSc1	Greenwich
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1829	[number]	k4	1829
–	–	k?	–
Odysseus	Odysseus	k1gMnSc1	Odysseus
vysmívající	vysmívající	k2eAgMnSc1d1	vysmívající
se	se	k3xPyFc4	se
Polyfémovi	Polyfémův	k2eAgMnPc1d1	Polyfémův
–	–	k?	–
Homérova	Homérův	k2eAgFnSc1d1	Homérova
Odyssea	Odyssea	k1gFnSc1	Odyssea
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
132,5	[number]	k4	132,5
x	x	k?	x
203	[number]	k4	203
cm	cm	kA	cm
National	National	k1gFnSc2	National
Gallery	Galler	k1gMnPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1835	[number]	k4	1835
–	–	k?	–
Požár	požár	k1gInSc1	požár
Horní	horní	k2eAgInSc1d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgFnPc1d1	dolní
sněmovny	sněmovna	k1gFnPc1	sněmovna
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
92	[number]	k4	92
x	x	k?	x
132	[number]	k4	132
<g/>
,	,	kIx,	,
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gFnSc1	Art
<g/>
,	,	kIx,	,
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
</s>
</p>
<p>
<s>
1835	[number]	k4	1835
–	–	k?	–
Canal	Canal	k1gMnSc1	Canal
Grande	grand	k1gMnSc5	grand
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
90	[number]	k4	90
x	x	k?	x
120,3	[number]	k4	120,3
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Metropolitan	metropolitan	k1gInSc4	metropolitan
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
</s>
</p>
<p>
<s>
1838	[number]	k4	1838
–	–	k?	–
Válečná	válečný	k2eAgFnSc1d1	válečná
loď	loď	k1gFnSc1	loď
Téméraire	Témérair	k1gInSc5	Témérair
je	být	k5eAaImIp3nS	být
vlečena	vlečen	k2eAgFnSc1d1	vlečena
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
posledního	poslední	k2eAgNnSc2d1	poslední
přístaviště	přístaviště	k1gNnSc2	přístaviště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bude	být	k5eAaImBp3nS	být
rozebrána	rozebrán	k2eAgFnSc1d1	rozebrána
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
91	[number]	k4	91
x	x	k?	x
122	[number]	k4	122
cm	cm	kA	cm
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
National	National	k1gFnSc2	National
Gallery	Galler	k1gInPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1840	[number]	k4	1840
–	–	k?	–
Otrokářská	otrokářský	k2eAgFnSc1d1	otrokářská
loď	loď	k1gFnSc1	loď
–	–	k?	–
Otrokáři	otrokář	k1gMnPc7	otrokář
házející	házející	k2eAgNnSc1d1	házející
přes	přes	k7c4	přes
palubu	paluba	k1gFnSc4	paluba
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
a	a	k8xC	a
umírající	umírající	k2eAgFnPc1d1	umírající
<g/>
.	.	kIx.	.
</s>
<s>
Tajfun	tajfun	k1gInSc1	tajfun
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
90,8	[number]	k4	90,8
x	x	k?	x
122,6	[number]	k4	122,6
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
</s>
</p>
<p>
<s>
1844	[number]	k4	1844
–	–	k?	–
Déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
pára	pára	k1gFnSc1	pára
a	a	k8xC	a
rychlost	rychlost	k1gFnSc1	rychlost
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
západní	západní	k2eAgFnSc1d1	západní
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
90,8	[number]	k4	90,8
x	x	k?	x
122	[number]	k4	122
cm	cm	kA	cm
National	National	k1gFnSc2	National
Gallery	Galler	k1gMnPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1840	[number]	k4	1840
–	–	k?	–
Východ	východ	k1gInSc1	východ
slunce	slunce	k1gNnSc2	slunce
s	s	k7c7	s
netvory	netvor	k1gMnPc7	netvor
<g/>
,	,	kIx,	,
olej	olej	k1gInSc4	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
91,5	[number]	k4	91,5
x	x	k?	x
122	[number]	k4	122
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Tate	Tate	k1gNnSc2	Tate
Gallery	Galler	k1gMnPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
1843	[number]	k4	1843
–	–	k?	–
Světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
Goethova	Goethův	k2eAgFnSc1d1	Goethova
teorie	teorie	k1gFnSc1	teorie
<g/>
)	)	kIx)	)
–	–	k?	–
ráno	ráno	k6eAd1	ráno
po	po	k7c6	po
Potopě	potopa	k1gFnSc6	potopa
–	–	k?	–
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
píšící	píšící	k2eAgFnSc4d1	píšící
knihu	kniha	k1gFnSc4	kniha
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
78,5	[number]	k4	78,5
x	x	k?	x
78,5	[number]	k4	78,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Tate	Tate	k1gNnSc2	Tate
Gallery	Galler	k1gMnPc4	Galler
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
B.	B.	kA	B.
Mráz	mráz	k1gInSc1	mráz
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Mrázová	Mrázová	k1gFnSc1	Mrázová
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
světového	světový	k2eAgNnSc2d1	světové
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
přeprac	přeprac	k1gFnSc1	přeprac
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
.	.	kIx.	.
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
632	[number]	k4	632
s.	s.	k?	s.
bez	bez	k7c2	bez
ISBN	ISBN	kA	ISBN
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
světového	světový	k2eAgNnSc2d1	světové
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon-Artia	Odeon-Artia	k1gFnSc1	Odeon-Artia
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-207-0023-4	[number]	k4	80-207-0023-4
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Bockemühl	Bockemühl	k1gFnSc1	Bockemühl
<g/>
.	.	kIx.	.
</s>
<s>
Turner	turner	k1gMnSc1	turner
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-7391-045-7	[number]	k4	978-80-7391-045-7
</s>
</p>
<p>
<s>
Charlie	Charlie	k1gMnPc4	Charlie
Ayresová	Ayresový	k2eAgFnSc1d1	Ayresová
<g/>
.	.	kIx.	.
</s>
<s>
Životy	život	k1gInPc1	život
slavných	slavný	k2eAgMnPc2d1	slavný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
98	[number]	k4	98
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-7391-090-7	[number]	k4	978-80-7391-090-7
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Obrazová	obrazový	k2eAgFnSc1d1	obrazová
kompozice	kompozice	k1gFnSc1	kompozice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc1	William
Turner	turner	k1gMnSc1	turner
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
William	William	k1gInSc1	William
Turner	turner	k1gMnSc1	turner
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
William	William	k1gInSc1	William
Turner	turner	k1gMnSc1	turner
</s>
</p>
<p>
<s>
J.	J.	kA	J.
M.	M.	kA	M.
W.	W.	kA	W.
Turner	turner	k1gMnSc1	turner
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
</s>
</p>
<p>
<s>
galerie	galerie	k1gFnSc1	galerie
Tate	Tate	k1gFnSc1	Tate
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Turnerova	turnerův	k2eAgFnSc1d1	Turnerova
galerie	galerie	k1gFnSc1	galerie
</s>
</p>
<p>
<s>
J.	J.	kA	J.
M.	M.	kA	M.
W.	W.	kA	W.
Turner	turner	k1gMnSc1	turner
v	v	k7c6	v
Olžině	Olžin	k2eAgFnSc6d1	Olžina
galerii	galerie	k1gFnSc6	galerie
(	(	kIx(	(
<g/>
Olga	Olga	k1gFnSc1	Olga
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gallery	Galler	k1gInPc7	Galler
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Independent	independent	k1gMnSc1	independent
Turner	turner	k1gMnSc1	turner
Society	societa	k1gFnSc2	societa
</s>
</p>
