<s>
Barbie	Barbie	k1gFnSc1	Barbie
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
nejznámější	známý	k2eAgFnSc1d3	nejznámější
a	a	k8xC	a
nejprodávanější	prodávaný	k2eAgFnSc1d3	nejprodávanější
panenka	panenka	k1gFnSc1	panenka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
představená	představená	k1gFnSc1	představená
na	na	k7c4	na
American	American	k1gInSc4	American
International	International	k1gMnSc2	International
Toy	Toy	k1gMnSc2	Toy
Fair	fair	k2eAgFnPc1d1	fair
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
veletrh	veletrh	k1gInSc1	veletrh
hraček	hračka	k1gFnPc2	hračka
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Panenku	panenka	k1gFnSc4	panenka
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
společnost	společnost	k1gFnSc1	společnost
Mattel	Mattel	k1gMnSc1	Mattel
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
aj	aj	kA	aj
Barbie	Barbie	k1gFnSc2	Barbie
DVD	DVD	kA	DVD
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
prodávaná	prodávaný	k2eAgFnSc1d1	prodávaná
Barbie	Barbie	k1gFnSc1	Barbie
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
firmy	firma	k1gFnSc2	firma
byla	být	k5eAaImAgFnS	být
Totally	Totall	k1gMnPc4	Totall
Hair	Haira	k1gFnPc2	Haira
Barbie	Barbie	k1gFnSc1	Barbie
(	(	kIx(	(
<g/>
známá	známý	k2eAgFnSc1d1	známá
mimo	mimo	k7c4	mimo
USA	USA	kA	USA
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Ultra	ultra	k2eAgInSc1d1	ultra
Hair	Hair	k1gInSc1	Hair
Barbie	Barbie	k1gFnSc1	Barbie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
panenek	panenka	k1gFnPc2	panenka
Barbie	Barbie	k1gFnSc2	Barbie
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
příslušenství	příslušenství	k1gNnSc2	příslušenství
je	být	k5eAaImIp3nS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Vynálezkyní	vynálezkyně	k1gFnSc7	vynálezkyně
panenky	panenka	k1gFnSc2	panenka
Barbie	Barbie	k1gFnSc2	Barbie
byla	být	k5eAaImAgFnS	být
paní	paní	k1gFnSc1	paní
Ruth	Ruth	k1gFnSc1	Ruth
Handlerová	Handlerová	k1gFnSc1	Handlerová
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
jí	jíst	k5eAaImIp3nS	jíst
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Barbarru	Barbarr	k1gInSc2	Barbarr
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
panenku	panenka	k1gFnSc4	panenka
také	také	k9	také
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Barbie	Barbie	k1gFnSc1	Barbie
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
také	také	k6eAd1	také
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Barbie	Barbie	k1gFnSc1	Barbie
má	mít	k5eAaImIp3nS	mít
romantický	romantický	k2eAgInSc1d1	romantický
vztah	vztah	k1gInSc1	vztah
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Kenem	Ken	k1gMnSc7	Ken
(	(	kIx(	(
<g/>
Ken	Ken	k1gMnSc1	Ken
Carson	Carson	k1gMnSc1	Carson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
boku	bok	k1gInSc6	bok
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
reklamě	reklama	k1gFnSc6	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Skipper	Skipper	k1gInSc1	Skipper
-	-	kIx~	-
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
sestra	sestra	k1gFnSc1	sestra
Barbie	Barbie	k1gFnSc1	Barbie
Stacie	Stacie	k1gFnSc1	Stacie
-	-	kIx~	-
dvanáctiletá	dvanáctiletý	k2eAgFnSc1d1	dvanáctiletá
sestra	sestra	k1gFnSc1	sestra
Barbie	Barbie	k1gFnSc1	Barbie
Chelsea	Chelsea	k1gFnSc1	Chelsea
-	-	kIx~	-
třiletá	třiletý	k2eAgFnSc1d1	třiletý
sestra	sestra	k1gFnSc1	sestra
Barbie	Barbie	k1gFnSc2	Barbie
Todd	Todd	k1gMnSc1	Todd
-	-	kIx~	-
dvouletý	dvouletý	k2eAgMnSc1d1	dvouletý
bratranec	bratranec	k1gMnSc1	bratranec
Barbie	Barbie	k1gFnSc2	Barbie
Ken	Ken	k1gMnSc1	Ken
-	-	kIx~	-
přítel	přítel	k1gMnSc1	přítel
Barbie	Barbie	k1gFnSc2	Barbie
Sauhy	Sauha	k1gFnSc2	Sauha
-	-	kIx~	-
blízký	blízký	k2eAgMnSc1d1	blízký
kamarád	kamarád	k1gMnSc1	kamarád
tříleté	tříletý	k2eAgFnSc2d1	tříletá
Chelesy	Chelesa	k1gFnSc2	Chelesa
Scott	Scott	k1gMnSc1	Scott
-	-	kIx~	-
přítel	přítel	k1gMnSc1	přítel
Skipper	Skipper	k1gMnSc1	Skipper
Midge	Midge	k1gFnSc1	Midge
-	-	kIx~	-
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamarádka	kamarádka	k1gFnSc1	kamarádka
Barbie	Barbie	k1gFnSc2	Barbie
Spoooty	Spooota	k1gFnSc2	Spooota
-	-	kIx~	-
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamarádka	kamarádka	k1gFnSc1	kamarádka
Skipper	Skippra	k1gFnPc2	Skippra
Kessy	Kessa	k1gFnSc2	Kessa
-	-	kIx~	-
dítě	dítě	k1gNnSc1	dítě
Midge	Midg	k1gInSc2	Midg
Olia	Olia	k?	Olia
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
sestra	sestra	k1gFnSc1	sestra
Midge	Midg	k1gInSc2	Midg
Mahyts	Mahytsa	k1gFnPc2	Mahytsa
-	-	kIx~	-
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Chelesy	Chelesa	k1gFnSc2	Chelesa
Barbara	Barbara	k1gFnSc1	Barbara
-	-	kIx~	-
sestřenice	sestřenice	k1gFnSc1	sestřenice
Midge	Midg	k1gFnSc2	Midg
Niky	nika	k1gFnSc2	nika
-	-	kIx~	-
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamarádka	kamarádka	k1gFnSc1	kamarádka
Barbie	Barbie	k1gFnSc1	Barbie
Tereza-	Tereza-	k1gFnSc1	Tereza-
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamarádka	kamarádka	k1gFnSc1	kamarádka
Barbie	Barbie	k1gFnSc2	Barbie
Summer	Summra	k1gFnPc2	Summra
-	-	kIx~	-
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamrádka	kamrádka	k1gFnSc1	kamrádka
Barbie	Barbie	k1gFnSc2	Barbie
Rakel	Rakela	k1gFnPc2	Rakela
-	-	kIx~	-
slečna	slečna	k1gFnSc1	slečna
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
Kena	Kenus	k1gMnSc4	Kenus
Ryan	Ryan	k1gMnSc1	Ryan
-	-	kIx~	-
bratr	bratr	k1gMnSc1	bratr
Rakel	Rakel	k1gMnSc1	Rakel
(	(	kIx(	(
<g/>
chce	chtít	k5eAaImIp3nS	chtít
Kenovi	Kena	k1gMnSc3	Kena
ukrást	ukrást	k5eAaPmF	ukrást
Barbie	Barbie	k1gFnSc1	Barbie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Louskáček	louskáček	k1gInSc1	louskáček
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Růženka	Růženka	k1gFnSc1	Růženka
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Labutí	labutí	k2eAgNnSc1d1	labutí
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
princezna	princezna	k1gFnSc1	princezna
a	a	k8xC	a
švadlenka	švadlenka	k1gFnSc1	švadlenka
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Fairytopia	Fairytopia	k1gFnSc1	Fairytopia
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
pěti	pět	k4xCc2	pět
dílů	díl	k1gInPc2	díl
Fairytopia	Fairytopium	k1gNnSc2	Fairytopium
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
<g />
.	.	kIx.	.
</s>
<s>
kouzlo	kouzlo	k1gNnSc4	kouzlo
pegasů	pegas	k1gMnPc2	pegas
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnPc1	Barbie
sestry	sestra	k1gFnPc1	sestra
jedou	jet	k5eAaImIp3nP	jet
kempovat	kempovat	k5eAaImF	kempovat
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Hvězdy	hvězda	k1gFnSc2	hvězda
Hollywood	Hollywood	k1gInSc1	Hollywood
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Mermaidia	Mermaidium	k1gNnSc2	Mermaidium
(	(	kIx(	(
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
Fairytopia	Fairytopium	k1gNnSc2	Fairytopium
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
deníček	deníček	k1gInSc1	deníček
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
<g />
.	.	kIx.	.
</s>
<s>
12	[number]	k4	12
tančících	tančící	k2eAgFnPc2d1	tančící
princezen	princezna	k1gFnPc2	princezna
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Kouzlo	kouzlo	k1gNnSc1	kouzlo
duhy	duha	k1gFnSc2	duha
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgInSc1	třetí
díl	díl	k1gInSc1	díl
Fairytopia	Fairytopium	k1gNnSc2	Fairytopium
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
motýlí	motýlí	k2eAgFnSc1d1	motýlí
víla	víla	k1gFnSc1	víla
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
díl	díl	k1gInSc1	díl
Fairytopia	Fairytopium	k1gNnSc2	Fairytopium
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc2	Barbie
diamantový	diamantový	k2eAgInSc1d1	diamantový
zámek	zámek	k1gInSc1	zámek
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
a	a	k8xC	a
Kouzelné	kouzelný	k2eAgFnPc1d1	kouzelná
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Malenka	Malenka	k1gFnSc1	Malenka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnPc4	Barbie
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
mušketýrky	mušketýrka	k1gFnPc4	mušketýrka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Příběh	příběh	k1gInSc1	příběh
mořské	mořský	k2eAgFnSc2d1	mořská
panny	panna	k1gFnSc2	panna
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
a	a	k8xC	a
Kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
módní	módní	k2eAgInSc1d1	módní
salón	salón	k1gInSc1	salón
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
<g />
.	.	kIx.	.
</s>
<s>
tajemství	tajemství	k1gNnSc1	tajemství
víl	víla	k1gFnPc2	víla
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
a	a	k8xC	a
Škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
princezny	princezna	k1gFnPc4	princezna
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
a	a	k8xC	a
Dokonalé	dokonalý	k2eAgFnPc1d1	dokonalá
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Příběh	příběh	k1gInSc1	příběh
mořské	mořský	k2eAgFnSc2d1	mořská
panny	panna	k1gFnSc2	panna
2	[number]	k4	2
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Princezna	princezna	k1gFnSc1	princezna
a	a	k8xC	a
Popová	popový	k2eAgFnSc1d1	popová
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc2	Barbie
a	a	k8xC	a
Růžové	růžový	k2eAgFnSc2d1	růžová
balerínky	balerínka	k1gFnSc2	balerínka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Mariposa	Mariposa	k1gFnSc1	Mariposa
a	a	k8xC	a
Květinová	květinový	k2eAgFnSc1d1	květinová
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
pátý	pátý	k4xOgInSc1	pátý
díl	díl	k1gInSc1	díl
Fairytopia	Fairytopium	k1gNnSc2	Fairytopium
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
a	a	k8xC	a
Poníková	Poníkový	k2eAgFnSc1d1	Poníkový
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Perlová	perlová	k1gFnSc1	perlová
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnPc1	Barbie
a	a	k8xC	a
kouzelná	kouzelný	k2eAgNnPc1d1	kouzelné
dvířka	dvířka	k1gNnPc1	dvířka
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Dream	Dream	k1gInSc1	Dream
<g />
.	.	kIx.	.
</s>
<s>
House	house	k1gNnSc1	house
Barbie	Barbie	k1gFnSc2	Barbie
odvážná	odvážný	k2eAgFnSc1d1	odvážná
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
Royals	Royalsa	k1gFnPc2	Royalsa
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Sestřičky	sestřička	k1gFnSc2	sestřička
a	a	k8xC	a
psí	psí	k2eAgNnPc1d1	psí
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
jako	jako	k8xS	jako
tajná	tajný	k2eAgFnSc1d1	tajná
agentka	agentka	k1gFnSc1	agentka
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Dreamtopia	Dreamtopia	k1gFnSc1	Dreamtopia
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
Sestřičky	sestřička	k1gFnSc2	sestřička
a	a	k8xC	a
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
pejsky	pejsek	k1gMnPc7	pejsek
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Barbie	Barbie	k1gFnSc1	Barbie
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Barbie	Barbie	k1gFnSc2	Barbie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Barbie	Barbie	k1gFnSc2	Barbie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
Facebook	Facebook	k1gInSc1	Facebook
profil	profil	k1gInSc1	profil
Barbie	Barbie	k1gFnSc2	Barbie
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
YouTube	YouTub	k1gInSc5	YouTub
kanál	kanál	k1gInSc1	kanál
Barbie	Barbie	k1gFnSc2	Barbie
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Stálá	stálý	k2eAgFnSc1d1	stálá
výstava	výstava	k1gFnSc1	výstava
panenky	panenka	k1gFnSc2	panenka
Barbie	Barbie	k1gFnSc1	Barbie
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
DollsLand	DollsLanda	k1gFnPc2	DollsLanda
</s>
