<s>
Velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgMnSc1d1	malý
Tisý	Tisý	k1gMnSc1	Tisý
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnPc4d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
ev.	ev.	k?	ev.
č.	č.	k?	č.
498	[number]	k4	498
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Lomnice	Lomnice	k1gFnSc2	Lomnice
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
ležící	ležící	k2eAgInSc1d1	ležící
na	na	k7c6	na
území	území	k1gNnSc6	území
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
rybniční	rybniční	k2eAgFnPc4d1	rybniční
rezervace	rezervace	k1gFnPc4	rezervace
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgNnPc4d1	významné
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
litorálním	litorální	k2eAgInSc7d1	litorální
porostem	porost	k1gInSc7	porost
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
spravuje	spravovat	k5eAaImIp3nS	spravovat
AOPK	AOPK	kA	AOPK
ČR	ČR	kA	ČR
Správa	správa	k1gFnSc1	správa
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
evidována	evidovat	k5eAaImNgFnS	evidovat
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
světové	světový	k2eAgFnSc2d1	světová
organizace	organizace	k1gFnSc2	organizace
UNESCO	UNESCO	kA	UNESCO
jako	jako	k8xC	jako
biosférická	biosférický	k2eAgFnSc1d1	biosférická
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
,	,	kIx,	,
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
ornitologických	ornitologický	k2eAgFnPc2d1	ornitologická
rezervací	rezervace	k1gFnPc2	rezervace
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
entomologického	entomologický	k2eAgInSc2d1	entomologický
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rezervace	rezervace	k1gFnSc2	rezervace
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
větších	veliký	k2eAgInPc2d2	veliký
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k8xC	i
dvojice	dvojice	k1gFnSc2	dvojice
rybníků	rybník	k1gInPc2	rybník
Velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
Malý	Malý	k1gMnSc1	Malý
Tisý	Tisý	k1gFnPc2	Tisý
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
daly	dát	k5eAaPmAgFnP	dát
lokalitě	lokalita	k1gFnSc3	lokalita
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rybníky	rybník	k1gInPc4	rybník
v	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
členité	členitý	k2eAgNnSc4d1	členité
pobřeží	pobřeží	k1gNnSc4	pobřeží
tvořené	tvořený	k2eAgInPc4d1	tvořený
zarostlými	zarostlý	k2eAgInPc7d1	zarostlý
břehy	břeh	k1gInPc7	břeh
<g/>
,	,	kIx,	,
zátokami	zátoka	k1gFnPc7	zátoka
<g/>
,	,	kIx,	,
poloostrovy	poloostrov	k1gInPc1	poloostrov
a	a	k8xC	a
ostrůvky	ostrůvek	k1gInPc1	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
břehy	břeh	k1gInPc4	břeh
volně	volně	k6eAd1	volně
navazují	navazovat	k5eAaImIp3nP	navazovat
podmáčené	podmáčený	k2eAgFnPc1d1	podmáčená
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
vřesoviště	vřesoviště	k1gNnPc1	vřesoviště
a	a	k8xC	a
pole	pole	k1gNnPc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
různých	různý	k2eAgNnPc2d1	různé
stanovišť	stanoviště	k1gNnPc2	stanoviště
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
bohatá	bohatý	k2eAgFnSc1d1	bohatá
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
z	z	k7c2	z
flory	flora	k1gFnSc2	flora
i	i	k8xC	i
fauny	fauna	k1gFnSc2	fauna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nP	sídlet
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
ptactvo	ptactvo	k1gNnSc1	ptactvo
využívá	využívat	k5eAaPmIp3nS	využívat
lokalitu	lokalita	k1gFnSc4	lokalita
jako	jako	k8xC	jako
důležitou	důležitý	k2eAgFnSc4d1	důležitá
migrační	migrační	k2eAgFnSc4d1	migrační
zastávku	zastávka	k1gFnSc4	zastávka
či	či	k8xC	či
shromaždiště	shromaždiště	k1gNnSc4	shromaždiště
před	před	k7c7	před
pravidelnými	pravidelný	k2eAgInPc7d1	pravidelný
tahy	tah	k1gInPc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lokalita	lokalita	k1gFnSc1	lokalita
po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
chráněna	chránit	k5eAaImNgFnS	chránit
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
nevhodnými	vhodný	k2eNgInPc7d1	nevhodný
hospodářskými	hospodářský	k2eAgInPc7d1	hospodářský
zásahy	zásah	k1gInPc7	zásah
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nadměrného	nadměrný	k2eAgInSc2d1	nadměrný
chovu	chov	k1gInSc2	chov
ryb	ryba	k1gFnPc2	ryba
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
degradaci	degradace	k1gFnSc3	degradace
a	a	k8xC	a
ústupu	ústup	k1gInSc2	ústup
litorálních	litorální	k2eAgInPc2d1	litorální
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ochranáři	ochranář	k1gMnPc1	ochranář
snaží	snažit	k5eAaImIp3nP	snažit
snižováním	snižování	k1gNnSc7	snižování
počtu	počet	k1gInSc2	počet
nasazovaných	nasazovaný	k2eAgFnPc2d1	nasazovaná
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
jejich	jejich	k3xOp3gFnSc2	jejich
druhové	druhový	k2eAgFnSc2d1	druhová
skladby	skladba	k1gFnSc2	skladba
společně	společně	k6eAd1	společně
s	s	k7c7	s
vodohospodářskými	vodohospodářský	k2eAgInPc7d1	vodohospodářský
zásahy	zásah	k1gInPc7	zásah
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
rybníku	rybník	k1gInSc6	rybník
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
podpořit	podpořit	k5eAaPmF	podpořit
rozvoj	rozvoj	k1gInSc4	rozvoj
rákosových	rákosový	k2eAgInPc2d1	rákosový
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
těchto	tento	k3xDgInPc2	tento
opatření	opatření	k1gNnPc2	opatření
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
porostů	porost	k1gInPc2	porost
by	by	kYmCp3nS	by
i	i	k9	i
za	za	k7c2	za
vhodných	vhodný	k2eAgFnPc2d1	vhodná
podmínek	podmínka	k1gFnPc2	podmínka
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k6eAd1	potřeba
doba	doba	k1gFnSc1	doba
dosahující	dosahující	k2eAgFnSc1d1	dosahující
až	až	k8xS	až
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
národní	národní	k2eAgFnSc2d1	národní
rezervace	rezervace	k1gFnSc2	rezervace
Velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgMnSc1d1	malý
Tisý	Tisý	k1gMnSc1	Tisý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Lomnice	Lomnice	k1gFnSc2	Lomnice
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Třeboní	Třeboň	k1gFnPc2	Třeboň
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
rybniční	rybniční	k2eAgFnSc4d1	rybniční
soustavu	soustava	k1gFnSc4	soustava
11	[number]	k4	11
rybníků	rybník	k1gInPc2	rybník
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
však	však	k9	však
uvádí	uvádět	k5eAaImIp3nS	uvádět
14	[number]	k4	14
rybníků	rybník	k1gInPc2	rybník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
rybník	rybník	k1gInSc4	rybník
Velký	velký	k2eAgInSc4d1	velký
Tisý	Tisý	k1gFnSc7	Tisý
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
mezi	mezi	k7c7	mezi
425	[number]	k4	425
až	až	k9	až
430	[number]	k4	430
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
polemi	pole	k1gFnPc7	pole
a	a	k8xC	a
loukami	louka	k1gFnPc7	louka
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
silnice	silnice	k1gFnSc1	silnice
procházející	procházející	k2eAgFnSc1d1	procházející
přes	přes	k7c4	přes
hráz	hráz	k1gFnSc4	hráz
rybníku	rybník	k1gInSc3	rybník
Koclířov	Koclířovo	k1gNnPc2	Koclířovo
(	(	kIx(	(
<g/>
leží	ležet	k5eAaImIp3nS	ležet
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
tok	tok	k1gInSc4	tok
umělého	umělý	k2eAgInSc2d1	umělý
vodního	vodní	k2eAgInSc2d1	vodní
kanálu	kanál	k1gInSc2	kanál
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
hranice	hranice	k1gFnSc1	hranice
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Lomnice	Lomnice	k1gFnSc2	Lomnice
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
–	–	k?	–
Třeboň	Třeboň	k1gFnSc1	Třeboň
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Koridor	koridor	k1gInSc1	koridor
D	D	kA	D
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
část	část	k1gFnSc4	část
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Rybniční	rybniční	k2eAgFnSc1d1	rybniční
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
rybníky	rybník	k1gInPc7	rybník
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
<g/>
,	,	kIx,	,
Malý	Malý	k1gMnSc1	Malý
Tisý	Tisý	k1gMnSc1	Tisý
<g/>
,	,	kIx,	,
Kubínský	Kubínský	k2eAgInSc1d1	Kubínský
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgInSc1d1	dolní
přesecký	přesecký	k2eAgInSc1d1	přesecký
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Horní	horní	k2eAgInSc1d1	horní
přesecký	přesecký	k2eAgInSc1d1	přesecký
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Jezero	jezero	k1gNnSc1	jezero
pod	pod	k7c7	pod
Tisým	Tisé	k1gNnSc7	Tisé
<g/>
,	,	kIx,	,
rybník	rybník	k1gInSc1	rybník
Šatlavy	šatlava	k1gFnSc2	šatlava
<g/>
,	,	kIx,	,
rybník	rybník	k1gInSc4	rybník
Smyček	smyčka	k1gFnPc2	smyčka
<g/>
,	,	kIx,	,
Malý	malý	k2eAgInSc1d1	malý
Dubovec	Dubovec	k1gInSc1	Dubovec
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
Dubovec	Dubovec	k1gInSc1	Dubovec
a	a	k8xC	a
Velký	velký	k2eAgInSc1d1	velký
Paneský	Paneský	k2eAgInSc1d1	Paneský
rybník	rybník	k1gInSc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Rybníkářství	rybníkářství	k1gNnSc1	rybníkářství
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
samotný	samotný	k2eAgInSc1d1	samotný
ústřední	ústřední	k2eAgInSc1d1	ústřední
rybník	rybník	k1gInSc1	rybník
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
třeboňských	třeboňský	k2eAgInPc2d1	třeboňský
rybníků	rybník	k1gInPc2	rybník
postaven	postaven	k2eAgMnSc1d1	postaven
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1505	[number]	k4	1505
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Štěpánka	Štěpánek	k1gMnSc2	Štěpánek
Netolického	netolický	k2eAgMnSc2d1	netolický
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
historické	historický	k2eAgInPc4d1	historický
prameny	pramen	k1gInPc4	pramen
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyměřovací	vyměřovací	k2eAgFnPc1d1	vyměřovací
práce	práce	k1gFnPc1	práce
začaly	začít	k5eAaPmAgFnP	začít
již	již	k6eAd1	již
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
1505	[number]	k4	1505
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
i	i	k9	i
Malý	malý	k2eAgMnSc1d1	malý
Tisý	Tisý	k1gMnSc1	Tisý
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1506	[number]	k4	1506
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
stoky	stoka	k1gFnSc2	stoka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1520	[number]	k4	1520
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
umělý	umělý	k2eAgInSc1d1	umělý
vodní	vodní	k2eAgInSc1d1	vodní
kanál	kanál	k1gInSc1	kanál
prochází	procházet	k5eAaImIp3nS	procházet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
přivádět	přivádět	k5eAaImF	přivádět
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
rybníků	rybník	k1gInPc2	rybník
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
stoka	stoka	k1gFnSc1	stoka
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
národní	národní	k2eAgFnSc2d1	národní
rezervace	rezervace	k1gFnSc2	rezervace
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
nacházela	nacházet	k5eAaImAgFnS	nacházet
dvojice	dvojice	k1gFnSc1	dvojice
starších	starý	k2eAgInPc2d2	starší
rybníků	rybník	k1gInPc2	rybník
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
Starý	starý	k2eAgMnSc1d1	starý
a	a	k8xC	a
Nový	nový	k2eAgMnSc1d1	nový
Tisý	Tisý	k1gMnSc1	Tisý
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
postupně	postupně	k6eAd1	postupně
přestaly	přestat	k5eAaPmAgFnP	přestat
vyhovovat	vyhovovat	k5eAaImF	vyhovovat
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
místě	místo	k1gNnSc6	místo
postaveny	postavit	k5eAaPmNgInP	postavit
nový	nový	k2eAgInSc4d1	nový
rybník	rybník	k1gInSc4	rybník
<g/>
,	,	kIx,	,
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1596	[number]	k4	1596
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
Velkého	velký	k2eAgNnSc2d1	velké
Tisého	Tisé	k1gNnSc2	Tisé
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
rozlohu	rozloha	k1gFnSc4	rozloha
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
dalšího	další	k2eAgMnSc2d1	další
významného	významný	k2eAgMnSc2d1	významný
rybníkáře	rybníkář	k1gMnSc2	rybníkář
Jakuba	Jakub	k1gMnSc2	Jakub
Krčína	Krčín	k1gMnSc2	Krčín
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
břehy	břeh	k1gInPc1	břeh
rybníka	rybník	k1gInSc2	rybník
byly	být	k5eAaImAgInP	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
intenzivně	intenzivně	k6eAd1	intenzivně
využívány	využívat	k5eAaImNgInP	využívat
k	k	k7c3	k
pastevectví	pastevectví	k1gNnSc3	pastevectví
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
odlesnění	odlesnění	k1gNnSc3	odlesnění
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
nízkou	nízký	k2eAgFnSc7d1	nízká
intenzitou	intenzita	k1gFnSc7	intenzita
chovu	chov	k1gInSc2	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
rybníce	rybník	k1gInSc6	rybník
panovala	panovat	k5eAaImAgFnS	panovat
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
příznivě	příznivě	k6eAd1	příznivě
projevilo	projevit	k5eAaPmAgNnS	projevit
na	na	k7c4	na
rozrůstání	rozrůstání	k1gNnSc4	rozrůstání
litorálních	litorální	k2eAgNnPc2d1	litorální
(	(	kIx(	(
<g/>
břežních	břežní	k2eAgNnPc2d1	břežní
<g/>
)	)	kIx)	)
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
mokřady	mokřad	k1gInPc1	mokřad
přecházející	přecházející	k2eAgInPc1d1	přecházející
volně	volně	k6eAd1	volně
v	v	k7c4	v
pastviště	pastviště	k1gNnSc4	pastviště
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
nacházela	nacházet	k5eAaImAgFnS	nacházet
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
pozorovatelna	pozorovatelna	k1gFnSc1	pozorovatelna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
lokalita	lokalita	k1gFnSc1	lokalita
známá	známý	k2eAgFnSc1d1	známá
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
botaniky	botanika	k1gFnPc4	botanika
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
řady	řada	k1gFnSc2	řada
vzácných	vzácný	k2eAgFnPc2d1	vzácná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgMnSc1d1	malý
Tisý	Tisý	k1gMnSc1	Tisý
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
výnosem	výnos	k1gInSc7	výnos
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
ČSR	ČSR	kA	ČSR
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1956	[number]	k4	1956
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1957	[number]	k4	1957
jako	jako	k8xC	jako
státní	státní	k2eAgFnSc2d1	státní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Rezervace	rezervace	k1gFnSc1	rezervace
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
615	[number]	k4	615
ha	ha	kA	ha
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ornitologického	ornitologický	k2eAgInSc2d1	ornitologický
výzkumu	výzkum	k1gInSc2	výzkum
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
měl	mít	k5eAaImAgInS	mít
hlavní	hlavní	k2eAgInSc1d1	hlavní
vliv	vliv	k1gInSc1	vliv
Jan	Jan	k1gMnSc1	Jan
Hanzák	Hanzák	k1gMnSc1	Hanzák
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
i	i	k8xC	i
amatérských	amatérský	k2eAgMnPc2d1	amatérský
ornitologů	ornitolog	k1gMnPc2	ornitolog
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
rezervace	rezervace	k1gFnSc2	rezervace
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zákazu	zákaz	k1gInSc3	zákaz
lovu	lov	k1gInSc2	lov
ptactva	ptactvo	k1gNnSc2	ptactvo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
zamezení	zamezení	k1gNnSc4	zamezení
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
terénních	terénní	k2eAgFnPc2d1	terénní
úprav	úprava	k1gFnPc2	úprava
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vyhláška	vyhláška	k1gFnSc1	vyhláška
neupravovala	upravovat	k5eNaImAgFnS	upravovat
nikterak	nikterak	k6eAd1	nikterak
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
později	pozdě	k6eAd2	pozdě
jako	jako	k8xS	jako
zásadní	zásadní	k2eAgInSc1d1	zásadní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
extenzivní	extenzivní	k2eAgInSc4d1	extenzivní
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
devastaci	devastace	k1gFnSc3	devastace
litorálního	litorální	k2eAgInSc2d1	litorální
porostu	porost	k1gInSc2	porost
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Dopady	dopad	k1gInPc1	dopad
chovu	chov	k1gInSc2	chov
ryb	ryba	k1gFnPc2	ryba
se	se	k3xPyFc4	se
na	na	k7c4	na
plno	plno	k1gNnSc4	plno
začaly	začít	k5eAaPmAgFnP	začít
projevovat	projevovat	k5eAaImF	projevovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
po	po	k7c6	po
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
po	po	k7c6	po
nabití	nabití	k1gNnSc6	nabití
účinnosti	účinnost	k1gFnSc2	účinnost
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
státní	státní	k2eAgFnSc2d1	státní
rezervace	rezervace	k1gFnSc2	rezervace
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
součástí	součást	k1gFnSc7	součást
1	[number]	k4	1
<g/>
.	.	kIx.	.
zóny	zóna	k1gFnSc2	zóna
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc4	Třeboňsko
a	a	k8xC	a
biosférické	biosférický	k2eAgFnSc2d1	biosférická
rezervace	rezervace	k1gFnSc2	rezervace
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
evidované	evidovaný	k2eAgNnSc1d1	evidované
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
světové	světový	k2eAgFnSc2d1	světová
organizace	organizace	k1gFnSc2	organizace
UNESCO	Unesco	k1gNnSc1	Unesco
jako	jako	k8xS	jako
část	část	k1gFnSc1	část
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
nadregionálního	nadregionální	k2eAgNnSc2d1	nadregionální
biocentra	biocentrum	k1gNnSc2	biocentrum
ÚSES	ÚSES	kA	ÚSES
Stará	starý	k2eAgFnSc1d1	stará
řeka	řeka	k1gFnSc1	řeka
–	–	k?	–
Rožmberk	Rožmberk	k1gInSc4	Rožmberk
–	–	k?	–
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
<g/>
.	.	kIx.	.
</s>
<s>
Lokalita	lokalita	k1gFnSc1	lokalita
je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ramsarské	Ramsarský	k2eAgFnSc2d1	Ramsarská
konvence	konvence	k1gFnSc2	konvence
<g/>
,	,	kIx,	,
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
do	do	k7c2	do
evropsky	evropsky	k6eAd1	evropsky
významných	významný	k2eAgNnPc2d1	významné
ptačích	ptačí	k2eAgNnPc2d1	ptačí
území	území	k1gNnPc2	území
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
soustavy	soustava	k1gFnSc2	soustava
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
rybniční	rybniční	k2eAgFnSc7d1	rybniční
soustavou	soustava	k1gFnSc7	soustava
11	[number]	k4	11
rybníků	rybník	k1gInPc2	rybník
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
rezervace	rezervace	k1gFnSc2	rezervace
tvoří	tvořit	k5eAaImIp3nS	tvořit
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
zmiňovaných	zmiňovaný	k2eAgInPc2d1	zmiňovaný
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
podmáčené	podmáčený	k2eAgFnPc4d1	podmáčená
louky	louka	k1gFnPc4	louka
<g/>
,	,	kIx,	,
louky	louka	k1gFnPc4	louka
<g/>
,	,	kIx,	,
vřesoviště	vřesoviště	k1gNnPc4	vřesoviště
a	a	k8xC	a
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
převážně	převážně	k6eAd1	převážně
smíšeným	smíšený	k2eAgInSc7d1	smíšený
kulturním	kulturní	k2eAgInSc7d1	kulturní
porostem	porost	k1gInSc7	porost
místy	místy	k6eAd1	místy
přecházející	přecházející	k2eAgInPc1d1	přecházející
až	až	k9	až
v	v	k7c4	v
přirozené	přirozený	k2eAgInPc4d1	přirozený
lesy	les	k1gInPc4	les
různého	různý	k2eAgNnSc2d1	různé
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
lesní	lesní	k2eAgInPc1d1	lesní
porosty	porost	k1gInPc1	porost
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
vysázením	vysázení	k1gNnSc7	vysázení
na	na	k7c4	na
nevyužívané	využívaný	k2eNgFnPc4d1	nevyužívaná
pastviny	pastvina	k1gFnPc4	pastvina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
formou	forma	k1gFnSc7	forma
náletů	nálet	k1gInPc2	nálet
dřevin	dřevina	k1gFnPc2	dřevina
a	a	k8xC	a
postupnou	postupný	k2eAgFnSc7d1	postupná
přeměnou	přeměna	k1gFnSc7	přeměna
v	v	k7c4	v
lesní	lesní	k2eAgInSc4d1	lesní
porost	porost	k1gInSc4	porost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hrázi	hráz	k1gFnSc6	hráz
rybníků	rybník	k1gInPc2	rybník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
duby	dub	k1gInPc1	dub
letní	letní	k2eAgInPc1d1	letní
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
robur	robura	k1gFnPc2	robura
<g/>
)	)	kIx)	)
a	a	k8xC	a
lípy	lípa	k1gFnSc2	lípa
srdčité	srdčitý	k2eAgInPc1d1	srdčitý
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
o	o	k7c6	o
stáří	stáří	k1gNnSc6	stáří
několikaseti	několikaset	k2eAgMnPc1d1	několikaset
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
podmáčené	podmáčený	k2eAgFnPc4d1	podmáčená
olšiny	olšina	k1gFnPc4	olšina
a	a	k8xC	a
porosty	porost	k1gInPc4	porost
vrb	vrba	k1gFnPc2	vrba
<g/>
.	.	kIx.	.
</s>
<s>
Podloží	podloží	k1gNnSc1	podloží
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
nachází	nacházet	k5eAaImIp3nS	nacházet
bělošedé	bělošedý	k2eAgInPc4d1	bělošedý
kaolinické	kaolinický	k2eAgInPc4d1	kaolinický
pískovce	pískovec	k1gInPc4	pískovec
<g/>
,	,	kIx,	,
slepence	slepenec	k1gInPc4	slepenec
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
rudočervené	rudočervený	k2eAgInPc1d1	rudočervený
a	a	k8xC	a
bělošedé	bělošedý	k2eAgInPc1d1	bělošedý
jílovce	jílovec	k1gInPc1	jílovec
<g/>
,	,	kIx,	,
jílovité	jílovitý	k2eAgInPc1d1	jílovitý
pískovce	pískovec	k1gInPc1	pískovec
a	a	k8xC	a
svrchnokřídlové	svrchnokřídlový	k2eAgFnSc6d1	svrchnokřídlový
prachovce	prachovka	k1gFnSc6	prachovka
spadající	spadající	k2eAgFnSc6d1	spadající
do	do	k7c2	do
svrchního	svrchní	k2eAgInSc2d1	svrchní
oddílu	oddíl	k1gInSc2	oddíl
klikovského	klikovský	k2eAgNnSc2d1	klikovské
souvrství	souvrství	k1gNnSc2	souvrství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
třetihorní	třetihorní	k2eAgMnSc1d1	třetihorní
montmorilloitické	montmorilloitický	k2eAgInPc4d1	montmorilloitický
jíly	jíl	k1gInPc4	jíl
a	a	k8xC	a
diatomové	diatomový	k2eAgInPc4d1	diatomový
jíly	jíl	k1gInPc4	jíl
až	až	k8xS	až
diatomity	diatomita	k1gFnPc4	diatomita
domanínského	domanínský	k2eAgNnSc2d1	domanínský
souvrství	souvrství	k1gNnSc2	souvrství
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
Velkého	velký	k2eAgNnSc2d1	velké
Tisého	Tisé	k1gNnSc2	Tisé
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
poloostrov	poloostrov	k1gInSc4	poloostrov
Lůsy	lůs	k1gInPc7	lůs
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
neogenními	neogenní	k2eAgInPc7d1	neogenní
sedimenty	sediment	k1gInPc7	sediment
zlivského	zlivský	k2eAgNnSc2d1	zlivský
souvrství	souvrství	k1gNnSc2	souvrství
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
souvrství	souvrství	k1gNnSc1	souvrství
tvoří	tvořit	k5eAaImIp3nS	tvořit
prokřemenělé	prokřemenělý	k2eAgInPc4d1	prokřemenělý
jílovité	jílovitý	k2eAgInPc4d1	jílovitý
pískovce	pískovec	k1gInPc4	pískovec
až	až	k8xS	až
slepence	slepenec	k1gInPc4	slepenec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
spodních	spodní	k2eAgFnPc6d1	spodní
partiích	partie	k1gFnPc6	partie
nachází	nacházet	k5eAaImIp3nS	nacházet
nezpevněné	zpevněný	k2eNgInPc4d1	nezpevněný
jílovité	jílovitý	k2eAgInPc4d1	jílovitý
písky	písek	k1gInPc4	písek
a	a	k8xC	a
písčité	písčitý	k2eAgInPc4d1	písčitý
jíly	jíl	k1gInPc4	jíl
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
lokality	lokalita	k1gFnSc2	lokalita
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
kvartérními	kvartérní	k2eAgInPc7d1	kvartérní
sedimenty	sediment	k1gInPc7	sediment
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
spraší	spraš	k1gFnPc2	spraš
a	a	k8xC	a
sprašových	sprašový	k2eAgFnPc2d1	sprašová
hlín	hlína	k1gFnPc2	hlína
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
holocénní	holocénní	k2eAgInPc4d1	holocénní
sedimenty	sediment	k1gInPc4	sediment
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
v	v	k7c6	v
tekoucí	tekoucí	k2eAgFnSc6d1	tekoucí
či	či	k8xC	či
stojaté	stojatý	k2eAgFnSc6d1	stojatá
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Půdní	půdní	k2eAgInSc1d1	půdní
profil	profil	k1gInSc1	profil
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
kambizemí	kambizemí	k1gNnPc4	kambizemí
a	a	k8xC	a
pseudoglejem	pseudoglej	k1gInSc7	pseudoglej
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
rybník	rybník	k1gInSc4	rybník
na	na	k7c6	na
území	území	k1gNnSc6	území
národní	národní	k2eAgFnSc2d1	národní
rezervace	rezervace	k1gFnSc2	rezervace
o	o	k7c6	o
katastrální	katastrální	k2eAgFnSc6d1	katastrální
výměře	výměra	k1gFnSc6	výměra
313	[number]	k4	313
ha	ha	kA	ha
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
přibližně	přibližně	k6eAd1	přibližně
275	[number]	k4	275
ha	ha	kA	ha
tvoří	tvořit	k5eAaImIp3nS	tvořit
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
221	[number]	k4	221
ha	ha	kA	ha
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pátý	pátý	k4xOgInSc4	pátý
největší	veliký	k2eAgInSc4d3	veliký
rybník	rybník	k1gInSc4	rybník
na	na	k7c6	na
území	území	k1gNnSc6	území
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
rybníku	rybník	k1gInSc3	rybník
naháněna	naháněn	k2eAgFnSc1d1	naháněna
skrze	skrze	k?	skrze
umělý	umělý	k2eAgInSc4d1	umělý
kanál	kanál	k1gInSc4	kanál
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
stoku	stoka	k1gFnSc4	stoka
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
skrze	skrze	k?	skrze
sousední	sousední	k2eAgInSc4d1	sousední
rybník	rybník	k1gInSc4	rybník
Koclířov	Koclířov	k1gInSc1	Koclířov
<g/>
)	)	kIx)	)
a	a	k8xC	a
odvodňován	odvodňovat	k5eAaImNgInS	odvodňovat
Miletínským	Miletínský	k2eAgInSc7d1	Miletínský
potokem	potok	k1gInSc7	potok
skrze	skrze	k?	skrze
výpustní	výpustní	k2eAgInSc1d1	výpustní
kamenný	kamenný	k2eAgInSc1d1	kamenný
kanál	kanál	k1gInSc1	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
zadržované	zadržovaný	k2eAgFnSc2d1	zadržovaná
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
2,75	[number]	k4	2,75
miliónu	milión	k4xCgInSc2	milión
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
hladina	hladina	k1gFnSc1	hladina
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
regulována	regulován	k2eAgFnSc1d1	regulována
na	na	k7c4	na
425,2	[number]	k4	425,2
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nicméně	nicméně	k8xC	nicméně
hloubka	hloubka	k1gFnSc1	hloubka
rybníku	rybník	k1gInSc3	rybník
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Délká	Délký	k2eAgFnSc1d1	Délký
hráze	hráze	k1gFnSc1	hráze
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
2	[number]	k4	2
km	km	kA	km
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
porostlá	porostlý	k2eAgFnSc1d1	porostlá
starými	starý	k2eAgInPc7d1	starý
duby	dub	k1gInPc7	dub
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
rybníku	rybník	k1gInSc6	rybník
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
členité	členitý	k2eAgNnSc1d1	členité
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
poloostrovů	poloostrov	k1gInPc2	poloostrov
porostlé	porostlý	k2eAgMnPc4d1	porostlý
litorálním	litorální	k2eAgInSc7d1	litorální
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zarostlé	zarostlý	k2eAgNnSc4d1	zarostlé
pobřeží	pobřeží	k1gNnSc4	pobřeží
navazují	navazovat	k5eAaImIp3nP	navazovat
okolní	okolní	k2eAgFnPc4d1	okolní
podmáčené	podmáčený	k2eAgFnPc4d1	podmáčená
louky	louka	k1gFnPc4	louka
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
prvkem	prvek	k1gInSc7	prvek
rybníku	rybník	k1gInSc3	rybník
je	být	k5eAaImIp3nS	být
nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
poloostrov	poloostrov	k1gInSc4	poloostrov
Lůsy	lůs	k1gInPc7	lůs
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
z	z	k7c2	z
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
strany	strana	k1gFnSc2	strana
kolem	kolem	k7c2	kolem
429	[number]	k4	429
metrů	metr	k1gInPc2	metr
vysokého	vysoký	k2eAgInSc2d1	vysoký
vrchu	vrch	k1gInSc2	vrch
Na	na	k7c6	na
Lůsech	lůs	k1gInPc6	lůs
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
rybníka	rybník	k1gInSc2	rybník
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
ornitologickou	ornitologický	k2eAgFnSc7d1	ornitologická
lokalitou	lokalita	k1gFnSc7	lokalita
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
ptáci	pták	k1gMnPc1	pták
před	před	k7c7	před
tahy	tah	k1gInPc7	tah
převážně	převážně	k6eAd1	převážně
koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
rybníce	rybník	k1gInSc6	rybník
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tisícihlavá	tisícihlavý	k2eAgNnPc4d1	tisícihlavé
hejna	hejno	k1gNnPc4	hejno
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
hus	husa	k1gFnPc2	husa
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
rybník	rybník	k1gInSc1	rybník
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Malý	Malý	k1gMnSc1	Malý
Tisý	Tisý	k1gMnSc2	Tisý
vybudován	vybudován	k2eAgInSc1d1	vybudován
roku	rok	k1gInSc2	rok
1505	[number]	k4	1505
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
32	[number]	k4	32
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
okraji	okraj	k1gInSc6	okraj
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
poblíž	poblíž	k7c2	poblíž
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
jako	jako	k8xC	jako
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
i	i	k9	i
tento	tento	k3xDgInSc4	tento
rybník	rybník	k1gInSc4	rybník
je	být	k5eAaImIp3nS	být
napájen	napájen	k2eAgInSc4d1	napájen
vodou	voda	k1gFnSc7	voda
přicházející	přicházející	k2eAgFnSc7d1	přicházející
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
stokou	stoka	k1gFnSc7	stoka
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
nacházela	nacházet	k5eAaImAgFnS	nacházet
lužní	lužní	k2eAgFnSc1d1	lužní
střemchová	střemchový	k2eAgFnSc1d1	střemchová
doubrava	doubrava	k1gFnSc1	doubrava
a	a	k8xC	a
olšina	olšina	k1gFnSc1	olšina
(	(	kIx(	(
<g/>
společenstva	společenstvo	k1gNnPc1	společenstvo
Quercus	Quercus	k1gMnSc1	Quercus
robur-Padus	robur-Padus	k1gMnSc1	robur-Padus
avium	avium	k1gInSc1	avium
a	a	k8xC	a
Alnus	Alnus	k1gInSc1	Alnus
glutinosa-Padus	glutinosa-Padus	k1gMnSc1	glutinosa-Padus
avium	avium	k1gInSc1	avium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zakryta	zakryt	k2eAgFnSc1d1	zakryta
vodní	vodní	k2eAgFnSc7d1	vodní
plochou	plocha	k1gFnSc7	plocha
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
na	na	k7c6	na
několika	několik	k4yIc6	několik
částech	část	k1gFnPc6	část
území	území	k1gNnPc2	území
se	se	k3xPyFc4	se
podmáčené	podmáčený	k2eAgInPc1d1	podmáčený
lesy	les	k1gInPc1	les
zachovaly	zachovat	k5eAaPmAgInP	zachovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
porostů	porost	k1gInPc2	porost
křovitých	křovitý	k2eAgFnPc2d1	křovitá
vrb	vrba	k1gFnPc2	vrba
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Salicion	Salicion	k1gInSc1	Salicion
cinereae	cinereae	k1gInSc1	cinereae
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
porost	porost	k1gInSc4	porost
olše	olše	k1gFnSc2	olše
lepkavé	lepkavý	k2eAgNnSc1d1	lepkavé
(	(	kIx(	(
<g/>
Alnus	Alnus	k1gInSc1	Alnus
glutinosa	glutinosa	k1gFnSc1	glutinosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dříve	dříve	k6eAd2	dříve
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
pastviny	pastvina	k1gFnPc1	pastvina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
mění	měnit	k5eAaImIp3nS	měnit
vlivem	vliv	k1gInSc7	vliv
náletů	nálet	k1gInPc2	nálet
břízy	bříza	k1gFnSc2	bříza
a	a	k8xC	a
borovice	borovice	k1gFnSc2	borovice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ústup	ústup	k1gInSc4	ústup
trnky	trnka	k1gFnSc2	trnka
obecné	obecná	k1gFnSc2	obecná
(	(	kIx(	(
<g/>
Prunus	Prunus	k1gInSc1	Prunus
spinosa	spinosa	k1gFnSc1	spinosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlohu	hloh	k1gInSc2	hloh
jednosemenného	jednosemenný	k2eAgInSc2d1	jednosemenný
(	(	kIx(	(
<g/>
Crataegus	Crataegus	k1gInSc1	Crataegus
monogyna	monogyn	k1gInSc2	monogyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlohu	hloh	k1gInSc2	hloh
obecného	obecný	k2eAgNnSc2d1	obecné
(	(	kIx(	(
<g/>
C.	C.	kA	C.
laevigata	laevigata	k1gFnSc1	laevigata
<g/>
)	)	kIx)	)
a	a	k8xC	a
jalovce	jalovec	k1gInPc1	jalovec
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Juniperus	Juniperus	k1gInSc1	Juniperus
communis	communis	k1gFnSc2	communis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
<g />
.	.	kIx.	.
</s>
<s>
jednou	jeden	k4xCgFnSc7	jeden
do	do	k7c2	do
roka	rok	k1gInSc2	rok
k	k	k7c3	k
sečení	sečení	k1gNnSc3	sečení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
domovem	domov	k1gInSc7	domov
následujících	následující	k2eAgInPc2d1	následující
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
všivec	všivec	k1gInSc1	všivec
ladní	ladní	k2eAgInSc1d1	ladní
(	(	kIx(	(
<g/>
Pedicularis	Pedicularis	k1gInSc1	Pedicularis
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hadí	hadí	k2eAgInSc1d1	hadí
mord	mord	k1gInSc1	mord
nízký	nízký	k2eAgInSc1d1	nízký
(	(	kIx(	(
<g/>
Scorzonera	Scorzoner	k1gMnSc2	Scorzoner
humilis	humilis	k1gFnSc2	humilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vstavač	vstavač	k1gInSc1	vstavač
kukačka	kukačka	k1gFnSc1	kukačka
(	(	kIx(	(
<g/>
Orchis	Orchis	k1gFnSc1	Orchis
morio	morio	k1gMnSc1	morio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prstnatec	prstnatec	k1gMnSc1	prstnatec
májový	májový	k2eAgMnSc1d1	májový
(	(	kIx(	(
<g/>
Dactylorhiza	Dactylorhiza	k1gFnSc1	Dactylorhiza
majalis	majalis	k1gFnSc2	majalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pupečník	pupečník	k1gInSc4	pupečník
obecný	obecný	k2eAgInSc4d1	obecný
(	(	kIx(	(
<g/>
Hydrocotyle	Hydrocotyl	k1gInSc5	Hydrocotyl
vulgaris	vulgaris	k1gFnPc6	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pampeliška	pampeliška	k1gFnSc1	pampeliška
Nordstedtova	Nordstedtův	k2eAgFnSc1d1	Nordstedtův
(	(	kIx(	(
<g/>
Taraxacum	Taraxacum	k1gInSc1	Taraxacum
nordstedtii	nordstedtie	k1gFnSc4	nordstedtie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jetel	jetel	k1gInSc1	jetel
kaštanový	kaštanový	k2eAgInSc1d1	kaštanový
(	(	kIx(	(
<g/>
Trifolium	Trifolium	k1gNnSc1	Trifolium
spadiceum	spadiceum	k1gNnSc1	spadiceum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kozlík	kozlík	k1gInSc4	kozlík
dvoudomý	dvoudomý	k2eAgInSc4d1	dvoudomý
(	(	kIx(	(
<g/>
Valeriana	Valeriana	k1gFnSc1	Valeriana
dioica	dioica	k1gFnSc1	dioica
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Litorální	litorální	k2eAgInSc1d1	litorální
porost	porost	k1gInSc1	porost
rákosin	rákosina	k1gFnPc2	rákosina
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
rákosem	rákos	k1gInSc7	rákos
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
Phragmites	Phragmites	k1gInSc1	Phragmites
australis	australis	k1gFnSc2	australis
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
šípatkou	šípatka	k1gFnSc7	šípatka
střelolistou	střelolistý	k2eAgFnSc7d1	střelolistá
(	(	kIx(	(
<g/>
Sagittaria	Sagittarium	k1gNnSc2	Sagittarium
sagittifolia	sagittifolium	k1gNnSc2	sagittifolium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodou	voda	k1gFnSc7	voda
bohatých	bohatý	k2eAgFnPc6d1	bohatá
oblastech	oblast	k1gFnPc6	oblast
rezervace	rezervace	k1gFnSc2	rezervace
roste	růst	k5eAaImIp3nS	růst
kapradník	kapradník	k1gInSc1	kapradník
bažinný	bažinný	k2eAgInSc1d1	bažinný
(	(	kIx(	(
<g/>
Thelypteris	Thelypteris	k1gInSc1	Thelypteris
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ďáblík	ďáblík	k1gInSc1	ďáblík
bahenní	bahenní	k2eAgInSc1d1	bahenní
(	(	kIx(	(
<g/>
Calla	Calla	k1gFnSc1	Calla
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbahněných	zbahněný	k2eAgFnPc6d1	zbahněný
částech	část	k1gFnPc6	část
rezervace	rezervace	k1gFnSc2	rezervace
rozpuk	rozpuk	k1gInSc1	rozpuk
jízlivý	jízlivý	k2eAgInSc1d1	jízlivý
(	(	kIx(	(
<g/>
Cicuta	Cicut	k2eAgFnSc1d1	Cicuta
virosa	virosa	k1gFnSc1	virosa
<g/>
)	)	kIx)	)
či	či	k8xC	či
čarovník	čarovník	k1gMnSc1	čarovník
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
(	(	kIx(	(
<g/>
Ciraea	Cirae	k2eAgNnPc1d1	Cirae
intermedia	intermedium	k1gNnPc1	intermedium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
ostrůvek	ostrůvek	k1gInSc4	ostrůvek
v	v	k7c6	v
Malém	malý	k2eAgNnSc6d1	malé
Tisém	Tisé	k1gNnSc6	Tisé
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
ostřicomechové	ostřicomechový	k2eAgNnSc4d1	ostřicomechový
rašeliniště	rašeliniště	k1gNnSc4	rašeliniště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
druhy	druh	k1gInPc1	druh
ostřice	ostřice	k1gFnSc2	ostřice
mokřadní	mokřadní	k2eAgInPc1d1	mokřadní
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
limosa	limosa	k1gFnSc1	limosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vachta	vachta	k1gFnSc1	vachta
trojlistá	trojlistý	k2eAgFnSc1d1	trojlistá
(	(	kIx(	(
<g/>
Menyanthes	Menyanthes	k1gMnSc1	Menyanthes
trifoliata	trifoliat	k1gMnSc2	trifoliat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klikva	klikva	k1gFnSc1	klikva
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Oxycoccus	Oxycoccus	k1gMnSc1	Oxycoccus
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
a	a	k8xC	a
rosnatka	rosnatka	k1gFnSc1	rosnatka
okrouhlolistá	okrouhlolistý	k2eAgFnSc1d1	okrouhlolistá
(	(	kIx(	(
<g/>
Drosera	Drosera	k1gFnSc1	Drosera
rotundifolia	rotundifolia	k1gFnSc1	rotundifolia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
maloplošných	maloplošný	k2eAgInPc6d1	maloplošný
rybnících	rybník	k1gInPc6	rybník
roste	růst	k5eAaImIp3nS	růst
lakušník	lakušník	k1gInSc1	lakušník
niťolistý	niťolistý	k2eAgInSc1d1	niťolistý
(	(	kIx(	(
<g/>
Batrachium	Batrachium	k1gNnSc1	Batrachium
trichophyllum	trichophyllum	k1gNnSc1	trichophyllum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rdest	rdest	k1gInSc1	rdest
světlý	světlý	k2eAgInSc1d1	světlý
(	(	kIx(	(
<g/>
Potamogeton	Potamogeton	k1gInSc1	Potamogeton
lucens	lucens	k1gInSc1	lucens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rdest	rdest	k1gInSc1	rdest
ostrolistý	ostrolistý	k2eAgInSc1d1	ostrolistý
(	(	kIx(	(
<g/>
P.	P.	kA	P.
acutifolium	acutifolium	k1gNnSc4	acutifolium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rdest	rdest	k1gInSc1	rdest
vláskovitý	vláskovitý	k2eAgInSc1d1	vláskovitý
(	(	kIx(	(
<g/>
P.	P.	kA	P.
trichoides	trichoides	k1gInSc1	trichoides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voďanka	voďanka	k1gFnSc1	voďanka
žabí	žabí	k2eAgFnSc1d1	žabí
(	(	kIx(	(
<g/>
Hydrocharis	Hydrocharis	k1gFnSc1	Hydrocharis
morsus-ranae	morsusanaat	k5eAaPmIp3nS	morsus-ranaat
<g/>
)	)	kIx)	)
a	a	k8xC	a
řečanka	řečanka	k1gFnSc1	řečanka
přímořská	přímořský	k2eAgFnSc1d1	přímořská
(	(	kIx(	(
<g/>
Najas	Najas	k1gMnSc1	Najas
marina	marina	k1gMnSc1	marina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
například	například	k6eAd1	například
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
vstavač	vstavač	k1gInSc1	vstavač
kukačka	kukačka	k1gFnSc1	kukačka
(	(	kIx(	(
<g/>
Orchis	Orchis	k1gFnSc1	Orchis
morio	morio	k1gMnSc1	morio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
masnice	masnice	k1gFnSc1	masnice
vodní	vodní	k2eAgFnSc1d1	vodní
(	(	kIx(	(
<g/>
Tillaea	Tillae	k2eAgFnSc1d1	Tillae
aquatica	aquatica	k1gFnSc1	aquatica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bublinatka	bublinatka	k1gFnSc1	bublinatka
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
(	(	kIx(	(
<g/>
Utricularia	Utricularium	k1gNnSc2	Utricularium
intermedia	intermedium	k1gNnSc2	intermedium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všivec	všivec	k1gInSc4	všivec
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Pedicularis	Pedicularis	k1gInSc4	Pedicularis
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
hadilka	hadilka	k1gFnSc1	hadilka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Ophioglossum	Ophioglossum	k1gInSc1	Ophioglossum
vulgatum	vulgatum	k1gNnSc1	vulgatum
<g/>
)	)	kIx)	)
v	v	k7c6	v
podrostu	podrost	k1gInSc6	podrost
krušiny	krušina	k1gFnSc2	krušina
olšové	olšový	k2eAgNnSc1d1	olšové
(	(	kIx(	(
<g/>
Frangula	Frangula	k1gFnSc1	Frangula
alnus	alnus	k1gMnSc1	alnus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavuň	plavuň	k1gFnSc1	plavuň
pučivá	pučivý	k2eAgFnSc1d1	pučivá
(	(	kIx(	(
<g/>
Lycopodium	Lycopodium	k1gNnSc1	Lycopodium
annotinum	annotinum	k1gNnSc1	annotinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pupečník	pupečník	k1gInSc4	pupečník
obecný	obecný	k2eAgInSc4d1	obecný
(	(	kIx(	(
<g/>
Hydrocotyle	Hydrocotyl	k1gInSc5	Hydrocotyl
vulgaris	vulgaris	k1gFnPc6	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tolije	tolije	k1gFnSc1	tolije
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Parnassia	Parnassia	k1gFnSc1	Parnassia
palustris	palustris	k1gFnSc2	palustris
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žebratka	žebratka	k1gFnSc1	žebratka
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Hottonia	Hottonium	k1gNnPc1	Hottonium
palustris	palustris	k1gFnPc2	palustris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
vemeník	vemeník	k1gInSc1	vemeník
dvoulistý	dvoulistý	k2eAgInSc1d1	dvoulistý
(	(	kIx(	(
<g/>
Platanthera	Platanthera	k1gFnSc1	Platanthera
bifolia	bifolia	k1gFnSc1	bifolia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prha	prha	k1gFnSc1	prha
arnika	arnika	k1gFnSc1	arnika
(	(	kIx(	(
<g/>
Arnica	Arnic	k2eAgFnSc1d1	Arnica
montana	montana	k1gFnSc1	montana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řebříček	řebříček	k1gInSc1	řebříček
bertrám	bertrám	k1gInSc1	bertrám
(	(	kIx(	(
<g/>
Achillea	Achilleus	k1gMnSc2	Achilleus
ptarmica	ptarmicus	k1gMnSc2	ptarmicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pcháč	pcháč	k1gInSc1	pcháč
různolistý	různolistý	k2eAgInSc1d1	různolistý
(	(	kIx(	(
<g/>
Cirsium	Cirsium	k1gNnSc1	Cirsium
helenioides	helenioidesa	k1gFnPc2	helenioidesa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
halucha	halucha	k1gFnSc1	halucha
vodní	vodní	k2eAgFnSc1d1	vodní
(	(	kIx(	(
<g/>
Oenanthe	Oenanthe	k1gFnSc1	Oenanthe
aquatica	aquatica	k1gMnSc1	aquatica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kamyšník	kamyšník	k1gInSc1	kamyšník
vrcholičnatý	vrcholičnatý	k2eAgInSc1d1	vrcholičnatý
(	(	kIx(	(
<g/>
Bolboschoenus	Bolboschoenus	k1gInSc1	Bolboschoenus
yagara	yagar	k1gMnSc2	yagar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šmel	šmel	k1gInSc1	šmel
okoličnatý	okoličnatý	k2eAgInSc1d1	okoličnatý
(	(	kIx(	(
<g/>
Butomus	Butomus	k1gInSc1	Butomus
umbellatus	umbellatus	k1gInSc1	umbellatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tajnička	tajnička	k1gFnSc1	tajnička
rýžovitá	rýžovitý	k2eAgFnSc1d1	rýžovitý
(	(	kIx(	(
<g/>
Leersia	Leersia	k1gFnSc1	Leersia
oryzoides	oryzoides	k1gMnSc1	oryzoides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostřice	ostřice	k1gFnSc1	ostřice
česká	český	k2eAgFnSc1d1	Česká
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
bohemica	bohemic	k1gInSc2	bohemic
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skřípina	skřípina	k1gFnSc1	skřípina
kořenující	kořenující	k2eAgFnSc1d1	kořenující
(	(	kIx(	(
<g/>
Scirpus	Scirpus	k1gInSc1	Scirpus
radicans	radicans	k1gInSc1	radicans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sítina	sítina	k1gFnSc1	sítina
rybniční	rybniční	k2eAgFnSc1d1	rybniční
(	(	kIx(	(
<g/>
Juncus	Juncus	k1gInSc1	Juncus
tenageia	tenageium	k1gNnSc2	tenageium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostřice	ostřice	k1gFnSc1	ostřice
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
acuta	acut	k1gInSc2	acut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zábělník	zábělník	k1gInSc1	zábělník
bahenní	bahenní	k2eAgInSc1d1	bahenní
(	(	kIx(	(
<g/>
Comarum	Comarum	k1gInSc1	Comarum
palustre	palustr	k1gInSc5	palustr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bazanovec	bazanovec	k1gMnSc1	bazanovec
kytkokvětý	kytkokvětý	k2eAgMnSc1d1	kytkokvětý
(	(	kIx(	(
<g/>
Naumburgia	Naumburgius	k1gMnSc4	Naumburgius
thyrsiflora	thyrsiflor	k1gMnSc4	thyrsiflor
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tolije	tolije	k1gFnSc1	tolije
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Parnassia	Parnassia	k1gFnSc1	Parnassia
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozrazil	rozrazil	k1gInSc1	rozrazil
štítkovitý	štítkovitý	k2eAgInSc1d1	štítkovitý
(	(	kIx(	(
<g/>
Veronica	Veronic	k2eAgNnPc1d1	Veronic
scutellata	scutelle	k1gNnPc1	scutelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třtina	třtina	k1gFnSc1	třtina
šedavá	šedavý	k2eAgFnSc1d1	šedavá
(	(	kIx(	(
<g/>
Calamagrostis	Calamagrostis	k1gFnSc1	Calamagrostis
canescens	canescensa	k1gFnPc2	canescensa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štírovník	štírovník	k1gInSc1	štírovník
bažinný	bažinný	k2eAgInSc1d1	bažinný
(	(	kIx(	(
<g/>
Lotus	Lotus	kA	Lotus
uliginosus	uliginosus	k1gInSc1	uliginosus
<g/>
)	)	kIx)	)
či	či	k8xC	či
dřípatka	dřípatka	k1gFnSc1	dřípatka
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
Soldanella	Soldanella	k1gFnSc1	Soldanella
montana	montana	k1gFnSc1	montana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
nachází	nacházet	k5eAaImIp3nS	nacházet
další	další	k2eAgFnPc4d1	další
desítky	desítka	k1gFnPc4	desítka
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozlehlosti	rozlehlost	k1gFnSc3	rozlehlost
rezervace	rezervace	k1gFnSc2	rezervace
a	a	k8xC	a
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
lokalitu	lokalita	k1gFnSc4	lokalita
bohatou	bohatý	k2eAgFnSc4d1	bohatá
na	na	k7c4	na
rozdílná	rozdílný	k2eAgNnPc4d1	rozdílné
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
z	z	k7c2	z
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
i	i	k8xC	i
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
z	z	k7c2	z
fauny	fauna	k1gFnSc2	fauna
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
vázání	vázání	k1gNnSc4	vázání
na	na	k7c4	na
mokřadní	mokřadní	k2eAgInSc4d1	mokřadní
ekosystém	ekosystém	k1gInSc4	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
širokou	široký	k2eAgFnSc7d1	široká
škálou	škála	k1gFnSc7	škála
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
významnou	významný	k2eAgFnSc4d1	významná
entomologickou	entomologický	k2eAgFnSc4d1	entomologická
lokalitu	lokalita	k1gFnSc4	lokalita
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
například	například	k6eAd1	například
o	o	k7c4	o
brouka	brouk	k1gMnSc4	brouk
páchníka	páchník	k1gMnSc2	páchník
hnědého	hnědý	k2eAgMnSc2d1	hnědý
(	(	kIx(	(
<g/>
Osmoderma	Osmoderm	k1gMnSc2	Osmoderm
eremita	eremit	k1gMnSc2	eremit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
motýlů	motýl	k1gMnPc2	motýl
batolec	batolec	k1gMnSc1	batolec
duhový	duhový	k2eAgMnSc1d1	duhový
(	(	kIx(	(
<g/>
Apatura	Apatura	k1gFnSc1	Apatura
iris	iris	k1gFnSc2	iris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
batolec	batolec	k1gMnSc1	batolec
červený	červený	k2eAgMnSc1d1	červený
(	(	kIx(	(
<g/>
Apatura	Apatura	k1gFnSc1	Apatura
ilia	ilia	k1gMnSc1	ilia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otakárek	otakárek	k1gMnSc1	otakárek
fenyklový	fenyklový	k2eAgMnSc1d1	fenyklový
(	(	kIx(	(
<g/>
Papilio	Papilio	k1gMnSc1	Papilio
machaon	machaon	k1gMnSc1	machaon
<g/>
)	)	kIx)	)
a	a	k8xC	a
bělopásek	bělopásek	k1gMnSc1	bělopásek
tavolníkový	tavolníkový	k2eAgMnSc1d1	tavolníkový
(	(	kIx(	(
<g/>
Neptis	Neptis	k1gInSc1	Neptis
rivularis	rivularis	k1gFnSc2	rivularis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pavouků	pavouk	k1gMnPc2	pavouk
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
skákavka	skákavka	k1gFnSc1	skákavka
Marpissa	Marpissa	k1gFnSc1	Marpissa
pomatia	pomatia	k1gFnSc1	pomatia
<g/>
,	,	kIx,	,
Gnaphosa	Gnaphosa	k1gFnSc1	Gnaphosa
nigerrima	nigerrima	k1gFnSc1	nigerrima
<g/>
,	,	kIx,	,
plachetnatka	plachetnatka	k1gFnSc1	plachetnatka
Walckenaeria	Walckenaerium	k1gNnSc2	Walckenaerium
nodosa	nodosa	k1gFnSc1	nodosa
<g/>
,	,	kIx,	,
lovčík	lovčík	k1gMnSc1	lovčík
Dolomedes	Dolomedes	k1gMnSc1	Dolomedes
plantarius	plantarius	k1gMnSc1	plantarius
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
rašelinného	rašelinný	k2eAgMnSc4d1	rašelinný
mravence	mravenec	k1gMnSc4	mravenec
Formica	Formicus	k1gMnSc4	Formicus
picea	piceus	k1gMnSc4	piceus
<g/>
,	,	kIx,	,
dvojici	dvojice	k1gFnSc4	dvojice
střevlíků	střevlík	k1gMnPc2	střevlík
Paratachys	Paratachys	k1gInSc4	Paratachys
fulvicollis	fulvicollis	k1gFnPc2	fulvicollis
a	a	k8xC	a
Perigona	Perigona	k1gFnSc1	Perigona
nigriceps	nigricepsa	k1gFnPc2	nigricepsa
<g/>
,	,	kIx,	,
z	z	k7c2	z
drabčíků	drabčík	k1gMnPc2	drabčík
Hypnogyra	Hypnogyra	k1gMnSc1	Hypnogyra
glabra	glabra	k1gMnSc1	glabra
a	a	k8xC	a
Paederus	Paederus	k1gMnSc1	Paederus
limnophilus	limnophilus	k1gMnSc1	limnophilus
<g/>
,	,	kIx,	,
vodomil	vodomil	k1gMnSc1	vodomil
Berosus	Berosus	k1gMnSc1	Berosus
spinosus	spinosus	k1gMnSc1	spinosus
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
saranče	saranče	k1gNnSc4	saranče
Tetrix	Tetrix	k1gInSc4	Tetrix
ceperoi	ceperoi	k6eAd1	ceperoi
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
popsán	popsán	k2eAgMnSc1d1	popsán
zástupce	zástupce	k1gMnSc1	zástupce
blanokřídlého	blanokřídlý	k2eAgInSc2d1	blanokřídlý
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
poskočilky	poskočilka	k1gFnSc2	poskočilka
Aquaencyrtus	Aquaencyrtus	k1gMnSc1	Aquaencyrtus
bohemicus	bohemicus	k1gMnSc1	bohemicus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
mlžů	mlž	k1gMnPc2	mlž
škeble	škeble	k1gFnSc1	škeble
rybničná	rybničný	k2eAgFnSc1d1	rybničná
(	(	kIx(	(
<g/>
Anodonta	Anodonta	k1gFnSc1	Anodonta
cygnea	cygnea	k1gMnSc1	cygnea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
skokan	skokan	k1gMnSc1	skokan
ostronosý	ostronosý	k2eAgMnSc1d1	ostronosý
(	(	kIx(	(
<g/>
Rana	Rana	k1gFnSc1	Rana
arvalis	arvalis	k1gFnSc2	arvalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
početné	početný	k2eAgFnSc2d1	početná
populace	populace	k1gFnSc2	populace
zde	zde	k6eAd1	zde
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kuňka	kuňka	k1gFnSc1	kuňka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Bombina	Bombin	k2eAgFnSc1d1	Bombina
bombina	bombina	k1gFnSc1	bombina
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
menších	malý	k2eAgInPc6d2	menší
rybnících	rybník	k1gInPc6	rybník
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
rosnička	rosnička	k1gFnSc1	rosnička
zelená	zelený	k2eAgFnSc1d1	zelená
(	(	kIx(	(
<g/>
Hyla	Hyl	k2eAgFnSc1d1	Hyla
arborea	arborea	k1gFnSc1	arborea
<g/>
)	)	kIx)	)
a	a	k8xC	a
skokan	skokan	k1gMnSc1	skokan
krátkonohý	krátkonohý	k2eAgMnSc1d1	krátkonohý
(	(	kIx(	(
<g/>
Pelophylax	Pelophylax	k1gInSc1	Pelophylax
lessonae	lessona	k1gInSc2	lessona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
ptačí	ptačí	k2eAgFnSc7d1	ptačí
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jak	jak	k6eAd1	jak
ke	k	k7c3	k
hnízdění	hnízdění	k1gNnSc3	hnízdění
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jako	jako	k8xC	jako
shromaždiště	shromaždiště	k1gNnSc1	shromaždiště
ptactva	ptactvo	k1gNnSc2	ptactvo
před	před	k7c7	před
tahy	tah	k1gInPc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
prováděný	prováděný	k2eAgInSc1d1	prováděný
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prokázal	prokázat	k5eAaPmAgInS	prokázat
výskyt	výskyt	k1gInSc1	výskyt
minimálně	minimálně	k6eAd1	minimálně
155	[number]	k4	155
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
107	[number]	k4	107
druhů	druh	k1gInPc2	druh
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
jako	jako	k9	jako
hnízdících	hnízdící	k2eAgFnPc2d1	hnízdící
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
např.	např.	kA	např.
orla	orel	k1gMnSc2	orel
mořského	mořský	k2eAgMnSc2d1	mořský
(	(	kIx(	(
<g/>
Haliaeetus	Haliaeetus	k1gInSc1	Haliaeetus
albicilla	albicillo	k1gNnSc2	albicillo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pochopa	pochop	k1gMnSc2	pochop
rákosního	rákosní	k2eAgMnSc2d1	rákosní
(	(	kIx(	(
<g/>
Circus	Circus	k1gInSc1	Circus
aeruginosus	aeruginosus	k1gInSc1	aeruginosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rybáka	rybák	k1gMnSc2	rybák
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Sterna	sternum	k1gNnSc2	sternum
hirundo	hirundo	k6eAd1	hirundo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slavíka	slavík	k1gMnSc4	slavík
modráčka	modráček	k1gMnSc4	modráček
(	(	kIx(	(
<g/>
Luscinia	Luscinium	k1gNnSc2	Luscinium
svecica	svecic	k1gInSc2	svecic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chřástla	chřástnout	k5eAaPmAgFnS	chřástnout
vodního	vodní	k2eAgInSc2d1	vodní
(	(	kIx(	(
<g/>
Rallus	Rallus	k1gInSc1	Rallus
<g />
.	.	kIx.	.
</s>
<s>
aquaticus	aquaticus	k1gInSc1	aquaticus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chřástala	chřástal	k1gMnSc2	chřástal
malého	malý	k2eAgMnSc2d1	malý
(	(	kIx(	(
<g/>
Porzana	Porzan	k1gMnSc2	Porzan
parva	parva	k?	parva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moudliváčka	moudliváček	k1gMnSc2	moudliváček
lužního	lužní	k2eAgMnSc2d1	lužní
(	(	kIx(	(
<g/>
Remiz	Remiz	k1gInSc1	Remiz
pendulinus	pendulinus	k1gInSc1	pendulinus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
luňáka	luňák	k1gMnSc2	luňák
hnědého	hnědý	k2eAgMnSc2d1	hnědý
(	(	kIx(	(
<g/>
Milvus	Milvus	k1gInSc1	Milvus
migrans	migrans	k1gInSc1	migrans
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sýkořici	sýkořice	k1gFnSc4	sýkořice
vousatou	vousatý	k2eAgFnSc4d1	vousatá
(	(	kIx(	(
<g/>
Panurus	Panurus	k1gMnSc1	Panurus
biarmicus	biarmicus	k1gMnSc1	biarmicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostralku	ostralka	k1gFnSc4	ostralka
štíhhlou	štíhhlý	k2eAgFnSc4d1	štíhhlý
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc1	Anas
acuta	acut	k1gInSc2	acut
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zrzohlávka	zrzohlávka	k1gFnSc1	zrzohlávka
rudozobého	rudozobý	k2eAgInSc2d1	rudozobý
(	(	kIx(	(
<g/>
Netta	Nett	k1gInSc2	Nett
rufina	rufino	k1gNnSc2	rufino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volavku	volavka	k1gFnSc4	volavka
popelavou	popelavý	k2eAgFnSc4d1	popelavá
(	(	kIx(	(
<g/>
Ardea	Ardea	k1gMnSc1	Ardea
cinerea	cinerea	k1gMnSc1	cinerea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volavku	volavka	k1gFnSc4	volavka
červenou	červený	k2eAgFnSc4d1	červená
(	(	kIx(	(
<g/>
Ardea	Ardea	k1gMnSc1	Ardea
purpurea	purpurea	k1gMnSc1	purpurea
<g/>
)	)	kIx)	)
bukače	bukač	k1gMnSc2	bukač
velkého	velký	k2eAgMnSc2d1	velký
(	(	kIx(	(
<g/>
Botaurus	Botaurus	k1gInSc1	Botaurus
stellaris	stellaris	k1gFnSc2	stellaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kopřivku	kopřivka	k1gFnSc4	kopřivka
obecnou	obecná	k1gFnSc4	obecná
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc1	Anas
strepera	strepero	k1gNnSc2	strepero
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
čírku	čírka	k1gFnSc4	čírka
obecnou	obecná	k1gFnSc4	obecná
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc1	Anas
crecca	crecc	k1gInSc2	crecc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hohola	hohol	k1gMnSc2	hohol
severního	severní	k2eAgMnSc2d1	severní
(	(	kIx(	(
<g/>
Bucephala	Bucephal	k1gMnSc2	Bucephal
clangula	clangul	k1gMnSc2	clangul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostříže	ostříž	k1gMnSc2	ostříž
lesního	lesní	k2eAgMnSc2d1	lesní
(	(	kIx(	(
<g/>
Falco	Falco	k1gNnSc1	Falco
subbuteo	subbuteo	k1gNnSc1	subbuteo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bekasinu	bekasina	k1gFnSc4	bekasina
otavní	otavní	k2eAgMnSc1d1	otavní
(	(	kIx(	(
<g/>
Gallinago	Gallinago	k1gMnSc1	Gallinago
gallinago	gallinago	k1gMnSc1	gallinago
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vodouše	vodouš	k1gMnSc2	vodouš
rudonohého	rudonohý	k2eAgMnSc2d1	rudonohý
(	(	kIx(	(
<g/>
Tringa	Tring	k1gMnSc2	Tring
totanus	totanus	k1gMnSc1	totanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cvrličku	cvrlička	k1gFnSc4	cvrlička
slavíkovou	slavíkový	k2eAgFnSc4d1	slavíková
(	(	kIx(	(
<g/>
Locustella	Locustella	k1gMnSc1	Locustella
luscinoides	luscinoides	k1gMnSc1	luscinoides
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
labuť	labuť	k1gFnSc4	labuť
velkou	velký	k2eAgFnSc4d1	velká
(	(	kIx(	(
<g/>
Cygnus	Cygnus	k1gMnSc1	Cygnus
olor	olor	k1gMnSc1	olor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
během	během	k7c2	během
shromažďování	shromažďování	k1gNnSc2	shromažďování
před	před	k7c7	před
tahy	tah	k1gInPc7	tah
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
volavka	volavka	k1gFnSc1	volavka
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
Egretta	Egretta	k1gFnSc1	Egretta
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kormorán	kormorán	k1gMnSc1	kormorán
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Phalacrocorax	Phalacrocorax	k1gInSc1	Phalacrocorax
carbo	carba	k1gFnSc5	carba
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
hus	husa	k1gFnPc2	husa
<g/>
.	.	kIx.	.
</s>
<s>
Nepravidelně	pravidelně	k6eNd1	pravidelně
se	se	k3xPyFc4	se
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
taktéž	taktéž	k?	taktéž
zástupci	zástupce	k1gMnPc1	zástupce
druhů	druh	k1gInPc2	druh
břehouš	břehouš	k1gMnSc1	břehouš
černoocasý	černoocasý	k2eAgMnSc1d1	černoocasý
(	(	kIx(	(
<g/>
Limosa	Limosa	k1gFnSc1	Limosa
limosa	limosa	k1gFnSc1	limosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
významné	významný	k2eAgNnSc1d1	významné
shromaždiště	shromaždiště	k1gNnSc1	shromaždiště
ptactva	ptactvo	k1gNnSc2	ptactvo
před	před	k7c7	před
odletem	odlet	k1gInSc7	odlet
do	do	k7c2	do
zimních	zimní	k2eAgNnPc2d1	zimní
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
významnou	významný	k2eAgFnSc4d1	významná
migrační	migrační	k2eAgFnSc4d1	migrační
zastávku	zastávka	k1gFnSc4	zastávka
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
husy	husa	k1gFnSc2	husa
velké	velká	k1gFnSc2	velká
(	(	kIx(	(
<g/>
Anser	Anser	k1gMnSc1	Anser
anser	anser	k1gMnSc1	anser
<g/>
)	)	kIx)	)
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnSc4d1	maximální
četnost	četnost	k1gFnSc4	četnost
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
až	až	k9	až
5000	[number]	k4	5000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
taktéž	taktéž	k?	taktéž
zástupci	zástupce	k1gMnPc1	zástupce
volavky	volavka	k1gFnSc2	volavka
bílé	bílé	k1gNnSc1	bílé
(	(	kIx(	(
<g/>
Egretta	Egretta	k1gFnSc1	Egretta
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
husy	husa	k1gFnPc4	husa
polní	polní	k2eAgFnPc4d1	polní
(	(	kIx(	(
<g/>
Anser	Anser	k1gInSc4	Anser
fabalis	fabalis	k1gFnSc2	fabalis
<g/>
)	)	kIx)	)
a	a	k8xC	a
husy	husa	k1gFnPc1	husa
běločelé	běločelý	k2eAgFnPc1d1	běločelá
(	(	kIx(	(
<g/>
Anser	Anser	k1gInSc1	Anser
albifrons	albifrons	k1gInSc1	albifrons
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čírky	čírka	k1gFnPc1	čírka
obecné	obecný	k2eAgFnPc1d1	obecná
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc1	Anas
crecca	crecc	k1gInSc2	crecc
<g/>
)	)	kIx)	)
a	a	k8xC	a
lžičák	lžičák	k1gMnSc1	lžičák
pestrý	pestrý	k2eAgMnSc1d1	pestrý
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc1	Anas
clypeata	clypeat	k1gMnSc2	clypeat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
spatřen	spatřen	k2eAgMnSc1d1	spatřen
rybák	rybák	k1gMnSc1	rybák
malý	malý	k1gMnSc1	malý
(	(	kIx(	(
<g/>
Sternula	Sternula	k1gFnSc1	Sternula
albifrons	albifronsa	k1gFnPc2	albifronsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stabilní	stabilní	k2eAgInSc4d1	stabilní
výskyt	výskyt	k1gInSc4	výskyt
několika	několik	k4yIc2	několik
jedinců	jedinec	k1gMnPc2	jedinec
vydry	vydra	k1gFnSc2	vydra
říční	říční	k2eAgMnSc1d1	říční
(	(	kIx(	(
<g/>
Lutra	Lutra	k1gMnSc1	Lutra
lutra	lutra	k1gMnSc1	lutra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
kuna	kuna	k1gFnSc1	kuna
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Martes	Martes	k1gMnSc1	Martes
martes	martes	k1gMnSc1	martes
<g/>
)	)	kIx)	)
a	a	k8xC	a
kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnSc1d1	skalní
(	(	kIx(	(
<g/>
Martes	Martes	k1gInSc1	Martes
foina	foin	k1gInSc2	foin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
(	(	kIx(	(
<g/>
Sus	Sus	k1gMnSc4	Sus
scrofa	scrof	k1gMnSc4	scrof
<g/>
)	)	kIx)	)
poškozující	poškozující	k2eAgInSc1d1	poškozující
rákosový	rákosový	k2eAgInSc1d1	rákosový
porost	porost	k1gInSc1	porost
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ze	z	k7c2	z
širšího	široký	k2eAgInSc2d2	širší
pohledu	pohled	k1gInSc2	pohled
na	na	k7c6	na
území	území	k1gNnSc6	území
CHKO	CHKO	kA	CHKO
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
losa	los	k1gMnSc2	los
evropského	evropský	k2eAgMnSc2d1	evropský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gInSc1	alces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rybniční	rybniční	k2eAgFnSc1d1	rybniční
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
hospodářsky	hospodářsky	k6eAd1	hospodářsky
využívaná	využívaný	k2eAgFnSc1d1	využívaná
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rybářství	rybářství	k1gNnSc2	rybářství
Třeboň	Třeboň	k1gFnSc1	Třeboň
a.s.	a.s.	k?	a.s.
Oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
využívání	využívání	k1gNnSc2	využívání
lokality	lokalita	k1gFnSc2	lokalita
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rybí	rybí	k2eAgFnSc1d1	rybí
osádka	osádka	k1gFnSc1	osádka
až	až	k6eAd1	až
trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
standardní	standardní	k2eAgFnSc1d1	standardní
rybí	rybí	k2eAgFnSc1d1	rybí
osádka	osádka	k1gFnSc1	osádka
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
kapra	kapr	k1gMnSc2	kapr
redukována	redukovat	k5eAaBmNgFnS	redukovat
na	na	k7c4	na
kompromisní	kompromisní	k2eAgFnSc4d1	kompromisní
hodnotu	hodnota	k1gFnSc4	hodnota
okolo	okolo	k7c2	okolo
70	[number]	k4	70
kg	kg	kA	kg
K2	K2	k1gFnSc2	K2
na	na	k7c4	na
1	[number]	k4	1
ha	ha	kA	ha
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
finanční	finanční	k2eAgFnSc1d1	finanční
újma	újma	k1gFnSc1	újma
je	on	k3xPp3gNnSc4	on
Rybářství	rybářství	k1gNnSc4	rybářství
Třeboň	Třeboň	k1gFnSc1	Třeboň
a.	a.	k?	a.
s.	s.	k?	s.
kompenzována	kompenzovat	k5eAaBmNgFnS	kompenzovat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Rybniční	rybniční	k2eAgFnSc1d1	rybniční
soustava	soustava	k1gFnSc1	soustava
má	mít	k5eAaImIp3nS	mít
taktéž	taktéž	k?	taktéž
vodohospodářský	vodohospodářský	k2eAgInSc1d1	vodohospodářský
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zadržet	zadržet	k5eAaPmF	zadržet
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
povodňovou	povodňový	k2eAgFnSc4d1	povodňová
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaImNgNnS	využívat
jako	jako	k9	jako
protipovodňového	protipovodňový	k2eAgNnSc2d1	protipovodňové
opatření	opatření	k1gNnSc2	opatření
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
retenční	retenční	k2eAgFnSc2d1	retenční
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tímto	tento	k3xDgNnSc7	tento
použitím	použití	k1gNnSc7	použití
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
poškozován	poškozován	k2eAgInSc4d1	poškozován
ekosystém	ekosystém	k1gInSc4	ekosystém
dotčených	dotčený	k2eAgInPc2d1	dotčený
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
u	u	k7c2	u
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vliv	vliv	k1gInSc4	vliv
dlouhodobější	dlouhodobý	k2eAgInSc4d2	dlouhodobější
<g/>
.	.	kIx.	.
</s>
<s>
Soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
hladiny	hladina	k1gFnSc2	hladina
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
rybničních	rybniční	k2eAgFnPc6d1	rybniční
soustavách	soustava	k1gFnPc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
postihla	postihnout	k5eAaPmAgFnS	postihnout
řada	řada	k1gFnSc1	řada
povodní	povodeň	k1gFnPc2	povodeň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
například	například	k6eAd1	například
katastrofální	katastrofální	k2eAgFnSc4d1	katastrofální
srpnovou	srpnový	k2eAgFnSc4d1	srpnová
povodeň	povodeň	k1gFnSc4	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jarní	jarní	k2eAgFnSc1d1	jarní
povodeň	povodeň	k1gFnSc1	povodeň
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
povodeň	povodeň	k1gFnSc1	povodeň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
negativně	negativně	k6eAd1	negativně
promítla	promítnout	k5eAaPmAgFnS	promítnout
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
zaplavení	zaplavení	k1gNnSc2	zaplavení
porostů	porost	k1gInPc2	porost
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
následný	následný	k2eAgInSc4d1	následný
úhyn	úhyn	k1gInSc4	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrný	nadměrný	k2eAgInSc1d1	nadměrný
chov	chov	k1gInSc1	chov
ryb	ryba	k1gFnPc2	ryba
provozovaný	provozovaný	k2eAgMnSc1d1	provozovaný
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
litorálního	litorální	k2eAgInSc2d1	litorální
porostu	porost	k1gInSc2	porost
na	na	k7c6	na
Velkém	velký	k2eAgNnSc6d1	velké
Tisém	Tisé	k1gNnSc6	Tisé
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
srovnání	srovnání	k1gNnSc2	srovnání
leteckých	letecký	k2eAgInPc2d1	letecký
snímků	snímek	k1gInPc2	snímek
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
a	a	k8xC	a
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
jasně	jasně	k6eAd1	jasně
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
porosty	porost	k1gInPc1	porost
se	se	k3xPyFc4	se
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
o	o	k7c4	o
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
polovinu	polovina	k1gFnSc4	polovina
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
zmiňovaného	zmiňovaný	k2eAgInSc2d1	zmiňovaný
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
porovnání	porovnání	k1gNnSc2	porovnání
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
postupně	postupně	k6eAd1	postupně
zmizely	zmizet	k5eAaPmAgFnP	zmizet
desítky	desítka	k1gFnPc1	desítka
hektarů	hektar	k1gInPc2	hektar
rákosin	rákosina	k1gFnPc2	rákosina
sloužící	sloužící	k1gFnSc2	sloužící
jako	jako	k8xS	jako
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
i	i	k9	i
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prováděna	provádět	k5eAaImNgFnS	provádět
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zvrácení	zvrácení	k1gNnSc4	zvrácení
negativního	negativní	k2eAgInSc2d1	negativní
trendu	trend	k1gInSc2	trend
rozpadu	rozpad	k1gInSc2	rozpad
litorálního	litorální	k2eAgInSc2d1	litorální
porostu	porost	k1gInSc2	porost
řadou	řada	k1gFnSc7	řada
opatření	opatření	k1gNnSc2	opatření
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
Správy	správa	k1gFnSc2	správa
CHKO	CHKO	kA	CHKO
a	a	k8xC	a
BR	br	k0	br
Třeboňsko	Třeboňsko	k1gNnSc1	Třeboňsko
a	a	k8xC	a
vlastníkem	vlastník	k1gMnSc7	vlastník
pozemků	pozemek	k1gInPc2	pozemek
Rybářství	rybářství	k1gNnSc2	rybářství
Třeboň	Třeboň	k1gFnSc1	Třeboň
a.	a.	k?	a.
s.	s.	k?	s.
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
experiment	experiment	k1gInSc1	experiment
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
vodní	vodní	k2eAgFnSc1d1	vodní
hladina	hladina	k1gFnSc1	hladina
a	a	k8xC	a
trojnásobně	trojnásobně	k6eAd1	trojnásobně
vysoká	vysoká	k1gFnSc1	vysoká
populace	populace	k1gFnSc2	populace
ryb	ryba	k1gFnPc2	ryba
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
faktory	faktor	k1gInPc1	faktor
negativně	negativně	k6eAd1	negativně
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
vitalitu	vitalita	k1gFnSc4	vitalita
litorálních	litorální	k2eAgInPc2d1	litorální
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
experimentu	experiment	k1gInSc2	experiment
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
snižována	snižován	k2eAgFnSc1d1	snižována
hladina	hladina	k1gFnSc1	hladina
rybníku	rybník	k1gInSc6	rybník
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
lichém	lichý	k2eAgInSc6d1	lichý
roce	rok	k1gInSc6	rok
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnažení	obnažení	k1gNnSc3	obnažení
pásu	pás	k1gInSc2	pás
dna	dno	k1gNnSc2	dno
podél	podél	k7c2	podél
břehu	břeh	k1gInSc2	břeh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
projevit	projevit	k5eAaPmF	projevit
rozrůstáním	rozrůstání	k1gNnSc7	rozrůstání
rákosového	rákosový	k2eAgInSc2d1	rákosový
porostu	porost	k1gInSc2	porost
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přehrazení	přehrazení	k1gNnSc3	přehrazení
40	[number]	k4	40
ha	ha	kA	ha
části	část	k1gFnSc2	část
rybníku	rybník	k1gInSc6	rybník
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tzv.	tzv.	kA	tzv.
Přesecké	Přesecký	k2eAgFnSc2d1	Přesecký
zátoky	zátoka	k1gFnSc2	zátoka
pomocí	pomocí	k7c2	pomocí
drátěného	drátěný	k2eAgInSc2d1	drátěný
plotu	plot	k1gInSc2	plot
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
nasazena	nasazen	k2eAgFnSc1d1	nasazena
alternativní	alternativní	k2eAgFnSc1d1	alternativní
rybí	rybí	k2eAgFnSc1d1	rybí
osádka	osádka	k1gFnSc1	osádka
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
lína	lín	k1gMnSc2	lín
(	(	kIx(	(
<g/>
Tinca	Tinc	k2eAgFnSc1d1	Tinca
tinca	tinca	k1gFnSc1	tinca
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
či	či	k8xC	či
žádným	žádný	k3yNgNnSc7	žádný
zastoupením	zastoupení	k1gNnSc7	zastoupení
kapra	kapr	k1gMnSc2	kapr
obecného	obecný	k2eAgMnSc2d1	obecný
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
carpio	carpio	k1gNnSc1	carpio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ohrazení	ohrazení	k1gNnSc1	ohrazení
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
sloužilo	sloužit	k5eAaImAgNnS	sloužit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
plot	plot	k1gInSc1	plot
odstraněn	odstranit	k5eAaPmNgInS	odstranit
a	a	k8xC	a
rybí	rybí	k2eAgFnSc1d1	rybí
osádka	osádka	k1gFnSc1	osádka
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
plochy	plocha	k1gFnSc2	plocha
rybníku	rybník	k1gInSc3	rybník
měla	mít	k5eAaImAgFnS	mít
opět	opět	k6eAd1	opět
do	do	k7c2	do
lokality	lokalita	k1gFnSc2	lokalita
volný	volný	k2eAgInSc4d1	volný
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Experiment	experiment	k1gInSc1	experiment
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
a	a	k8xC	a
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
rybí	rybí	k2eAgFnSc1d1	rybí
posádka	posádka	k1gFnSc1	posádka
měla	mít	k5eAaImAgFnS	mít
negativní	negativní	k2eAgInSc4d1	negativní
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
litorální	litorální	k2eAgInSc4d1	litorální
porost	porost	k1gInSc4	porost
společně	společně	k6eAd1	společně
s	s	k7c7	s
nadměrným	nadměrný	k2eAgInSc7d1	nadměrný
výskytem	výskyt	k1gInSc7	výskyt
řas	řasa	k1gFnPc2	řasa
a	a	k8xC	a
zákalem	zákal	k1gInSc7	zákal
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Přijatými	přijatý	k2eAgNnPc7d1	přijaté
opatřeními	opatření	k1gNnPc7	opatření
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1998	[number]	k4	1998
až	až	k9	až
2006	[number]	k4	2006
rozšířit	rozšířit	k5eAaPmF	rozšířit
rákosový	rákosový	k2eAgInSc4d1	rákosový
porost	porost	k1gInSc4	porost
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
2	[number]	k4	2
metry	metr	k1gInPc4	metr
(	(	kIx(	(
<g/>
přepočteno	přepočíst	k5eAaPmNgNnS	přepočíst
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
např.	např.	kA	např.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
poloostrova	poloostrov	k1gInSc2	poloostrov
Lůsy	lůs	k1gInPc4	lůs
o	o	k7c6	o
0,5	[number]	k4	0,5
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Nicméně	nicméně	k8xC	nicméně
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
porostech	porost	k1gInPc6	porost
napáchala	napáchat	k5eAaBmAgFnS	napáchat
katastrofální	katastrofální	k2eAgFnSc4d1	katastrofální
povodeň	povodeň	k1gFnSc4	povodeň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
následně	následně	k6eAd1	následně
uhynuly	uhynout	k5eAaPmAgInP	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
tak	tak	k9	tak
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
za	za	k7c2	za
vhodných	vhodný	k2eAgFnPc2d1	vhodná
podmínek	podmínka	k1gFnPc2	podmínka
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k1gFnSc1	potřeba
doba	doba	k1gFnSc1	doba
dosahující	dosahující	k2eAgFnSc1d1	dosahující
až	až	k8xS	až
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
i	i	k9	i
pouhé	pouhý	k2eAgNnSc4d1	pouhé
částečné	částečný	k2eAgNnSc4d1	částečné
zvýšení	zvýšení	k1gNnSc4	zvýšení
plochy	plocha	k1gFnSc2	plocha
rákosin	rákosina	k1gFnPc2	rákosina
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hnojení	hnojení	k1gNnSc1	hnojení
rybníku	rybník	k1gInSc6	rybník
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
omezeno	omezen	k2eAgNnSc1d1	omezeno
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
je	být	k5eAaImIp3nS	být
dán	dán	k2eAgInSc1d1	dán
historickým	historický	k2eAgNnSc7d1	historické
hnojením	hnojení	k1gNnSc7	hnojení
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
a	a	k8xC	a
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
rezervace	rezervace	k1gFnSc2	rezervace
přitéká	přitékat	k5eAaImIp3nS	přitékat
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
rybničních	rybniční	k2eAgFnPc2d1	rybniční
soustav	soustava	k1gFnPc2	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
hnojení	hnojení	k1gNnSc3	hnojení
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
snížení	snížení	k1gNnSc2	snížení
hnojení	hnojení	k1gNnSc2	hnojení
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
do	do	k7c2	do
lokality	lokalita	k1gFnSc2	lokalita
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
navrátily	navrátit	k5eAaPmAgInP	navrátit
některé	některý	k3yIgInPc1	některý
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc1d1	ohrožený
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zachování	zachování	k1gNnSc2	zachování
lokality	lokalita	k1gFnSc2	lokalita
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
výběrová	výběrový	k2eAgFnSc1d1	výběrová
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sukcese	sukcese	k1gFnSc1	sukcese
dřevin	dřevina	k1gFnPc2	dřevina
mění	měnit	k5eAaImIp3nS	měnit
charakter	charakter	k1gInSc4	charakter
cenných	cenný	k2eAgNnPc2d1	cenné
nezalesněných	zalesněný	k2eNgNnPc2d1	nezalesněné
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
omezen	omezen	k2eAgInSc1d1	omezen
lov	lov	k1gInSc1	lov
divoké	divoký	k2eAgFnSc2d1	divoká
zvěře	zvěř	k1gFnSc2	zvěř
myslivci	myslivec	k1gMnSc3	myslivec
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
povolené	povolený	k2eAgInPc4d1	povolený
druhy	druh	k1gInPc4	druh
k	k	k7c3	k
odstřelu	odstřel	k1gInSc3	odstřel
patří	patřit	k5eAaImIp3nS	patřit
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
(	(	kIx(	(
<g/>
Sus	Sus	k1gMnSc4	Sus
scrofa	scrof	k1gMnSc4	scrof
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
(	(	kIx(	(
<g/>
Vulpes	Vulpes	k1gMnSc1	Vulpes
vulpes	vulpes	k1gMnSc1	vulpes
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
kuny	kuna	k1gFnSc2	kuna
(	(	kIx(	(
<g/>
kuna	kuna	k1gFnSc1	kuna
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Martes	Martes	k1gMnSc1	Martes
martes	martes	k1gMnSc1	martes
<g/>
)	)	kIx)	)
a	a	k8xC	a
kuna	kuna	k1gFnSc1	kuna
skalní	skalní	k2eAgFnSc1d1	skalní
(	(	kIx(	(
<g/>
Martes	Martes	k1gInSc1	Martes
foina	foin	k1gInSc2	foin
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
totiž	totiž	k9	totiž
přebývá	přebývat	k5eAaImIp3nS	přebývat
v	v	k7c6	v
porostu	porost	k1gInSc6	porost
rákosin	rákosina	k1gFnPc2	rákosina
a	a	k8xC	a
okusuje	okusovat	k5eAaImIp3nS	okusovat
zde	zde	k6eAd1	zde
jejich	jejich	k3xOp3gInPc4	jejich
mladé	mladý	k2eAgInPc4d1	mladý
výhonky	výhonek	k1gInPc4	výhonek
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
rostliny	rostlina	k1gFnPc1	rostlina
utopí	utopit	k5eAaPmIp3nP	utopit
po	po	k7c6	po
zaplavení	zaplavení	k1gNnSc6	zaplavení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
voda	voda	k1gFnSc1	voda
vyplní	vyplnit	k5eAaPmIp3nS	vyplnit
jejich	jejich	k3xOp3gFnSc4	jejich
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
ventilaci	ventilace	k1gFnSc4	ventilace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ochrany	ochrana	k1gFnSc2	ochrana
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
u	u	k7c2	u
prasete	prase	k1gNnSc2	prase
divokého	divoký	k2eAgInSc2d1	divoký
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
redukci	redukce	k1gFnSc3	redukce
jeho	jeho	k3xOp3gFnSc2	jeho
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
na	na	k7c6	na
území	území	k1gNnSc6	území
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
pozemcích	pozemek	k1gInPc6	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
na	na	k7c6	na
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
zemědělských	zemědělský	k2eAgFnPc6d1	zemědělská
plochách	plocha	k1gFnPc6	plocha
je	být	k5eAaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
individuální	individuální	k2eAgNnSc1d1	individuální
hospodaření	hospodaření	k1gNnSc1	hospodaření
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
péče	péče	k1gFnSc2	péče
taktéž	taktéž	k?	taktéž
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rybníky	rybník	k1gInPc1	rybník
Velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgInSc1d1	malý
Dubovec	Dubovec	k1gInSc1	Dubovec
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
využívány	využívat	k5eAaImNgInP	využívat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
plůdkové	plůdkový	k2eAgInPc1d1	plůdkový
či	či	k8xC	či
výtažníkové	výtažníkový	k2eAgInPc1d1	výtažníkový
rybníky	rybník	k1gInPc1	rybník
s	s	k7c7	s
občasným	občasný	k2eAgInSc7d1	občasný
chovem	chov	k1gInSc7	chov
generačních	generační	k2eAgFnPc2d1	generační
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
rybníků	rybník	k1gInPc2	rybník
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
vypuštěn	vypustit	k5eAaPmNgMnS	vypustit
vždy	vždy	k6eAd1	vždy
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
napuštěn	napuštěn	k2eAgMnSc1d1	napuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
rybníku	rybník	k1gInSc6	rybník
Velký	velký	k2eAgMnSc1d1	velký
Tisý	Tisý	k1gMnSc1	Tisý
nachází	nacházet	k5eAaImIp3nS	nacházet
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
terénní	terénní	k2eAgFnSc1d1	terénní
stanice	stanice	k1gFnSc1	stanice
podléhající	podléhající	k2eAgFnSc2d1	podléhající
správě	správa	k1gFnSc3	správa
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
kuchyňkou	kuchyňka	k1gFnSc7	kuchyňka
a	a	k8xC	a
krbovými	krbový	k2eAgNnPc7d1	krbové
kamny	kamna	k1gNnPc7	kamna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zavedena	zaveden	k2eAgFnSc1d1	zavedena
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stanice	stanice	k1gFnSc1	stanice
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
ubytování	ubytování	k1gNnSc4	ubytování
a	a	k8xC	a
jako	jako	k9	jako
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
ornitologické	ornitologický	k2eAgFnPc4d1	ornitologická
výzkumné	výzkumný	k2eAgFnPc4d1	výzkumná
expedice	expedice	k1gFnPc4	expedice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
dvojice	dvojice	k1gFnSc2	dvojice
zdrojů	zdroj	k1gInPc2	zdroj
volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
po	po	k7c6	po
veřejných	veřejný	k2eAgFnPc6d1	veřejná
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
další	další	k2eAgInPc1d1	další
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
nerušení	nerušení	k1gNnSc4	nerušení
místního	místní	k2eAgNnSc2d1	místní
ptactva	ptactvo	k1gNnSc2	ptactvo
není	být	k5eNaImIp3nS	být
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
rezervace	rezervace	k1gFnSc2	rezervace
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
povolen	povolen	k2eAgMnSc1d1	povolen
<g/>
.	.	kIx.	.
</s>
