<s>
Alžběta	Alžběta	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Württemberskáprincezna	Württemberskáprincezna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Bourbonsko-Sicilská	bourbonsko-sicilský	k2eAgFnSc1d1
Manžel	manžel	k1gMnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Bourbonsko-Sicilský	bourbonsko-sicilský	k2eAgMnSc1d1
Úplné	úplný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Elisabeth	Elisabeth	k1gInSc1
Maria	Mario	k1gMnSc2
Margarethe	Margareth	k1gMnSc2
Alix	Alix	k1gInSc1
Helene	Helen	k1gInSc5
Rosa	Rosa	k1gFnSc1
Philippine	Philippin	k1gInSc5
Christine	Christin	k1gInSc5
Josepha	Joseph	k1gMnSc2
Therese	Therese	k1gFnSc2
vom	vom	k?
Kinde	Kind	k1gInSc5
Jesu	Jesa	k1gFnSc4
Narození	narozený	k2eAgMnPc5d1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1933	#num#	k4
(	(	kIx(
<g/>
88	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Stuttgart	Stuttgart	k1gInSc1
<g/>
,	,	kIx,
Württemberský	Württemberský	k2eAgInSc1d1
stát	stát	k1gInSc1
svobodného	svobodný	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
Výmarská	výmarský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Rod	rod	k1gInSc1
</s>
<s>
Württemberkové	Württemberkové	k2eAgMnSc1d1
Otec	otec	k1gMnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
Matka	matka	k1gFnSc1
</s>
<s>
Rosa	Rosa	k1gFnSc1
Rakousko-Toskánská	rakousko-toskánský	k2eAgFnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Elisabeth	Elisabeth	k1gInSc1
Maria	Mario	k1gMnSc2
Margarethe	Margareth	k1gMnSc2
Alix	Alix	k1gInSc1
Helene	Helen	k1gInSc5
Rosa	Rosa	k1gFnSc1
Philippine	Philippin	k1gInSc5
Christine	Christin	k1gInSc5
Josepha	Joseph	k1gMnSc2
Therese	Therese	k1gFnSc2
vom	vom	k?
Kinde	Kind	k1gInSc5
Jesu	Jesum	k1gNnSc6
<g/>
,	,	kIx,
Herzogin	Herzogin	k2eAgInSc1d1
von	von	k1gInSc1
Württemberg	Württemberg	k1gInSc1
<g/>
;	;	kIx,
*	*	kIx~
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1933	#num#	k4
Stuttgart	Stuttgart	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
členka	členka	k1gFnSc1
rodu	rod	k1gInSc2
Württembergů	Württemberg	k1gInPc2
a	a	k8xC
württemberská	württemberský	k2eAgFnSc1d1
vévodkyně	vévodkyně	k1gFnSc1
a	a	k8xC
sňatkem	sňatek	k1gInSc7
s	s	k7c7
princem	princ	k1gMnSc7
Antonínem	Antonín	k1gMnSc7
Bourbonsko-Sicilským	bourbonsko-sicilský	k2eAgMnSc7d1
členka	členka	k1gFnSc1
rodu	rod	k1gInSc2
Bourbon-Obojí	Bourbon-Obojí	k1gFnSc1
Sicílie	Sicílie	k1gFnSc1
a	a	k8xC
bourbonsko-sicilská	bourbonsko-sicilský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
jako	jako	k9
třetí	třetí	k4xOgNnSc4
dítě	dítě	k1gNnSc4
a	a	k8xC
druhá	druhý	k4xOgFnSc1
dcera	dcera	k1gFnSc1
württemberského	württemberský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Filipa	Filip	k1gMnSc2
Albrechta	Albrecht	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
druhé	druhý	k4xOgFnSc2
manželky	manželka	k1gFnSc2
Rosy	Rosa	k1gFnSc2
Rakousko-Toskánské	rakousko-toskánský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Württembergu	Württemberg	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
stávající	stávající	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
rodu	rod	k1gInSc2
Württembergů	Württemberg	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sňatek	sňatek	k1gInSc1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
prince	princ	k1gMnPc4
Antonína	Antonín	k1gMnSc4
Bourbonsko-Sicilského	bourbonsko-sicilský	k2eAgMnSc4d1
<g/>
,	,	kIx,
jediného	jediný	k2eAgMnSc4d1
potomka	potomek	k1gMnSc4
prince	princ	k1gMnSc2
Gabriela	Gabriel	k1gMnSc2
Bourbonsko-Sicilského	bourbonsko-sicilský	k2eAgMnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
první	první	k4xOgFnPc1
manželky	manželka	k1gFnPc1
Malgorzaty	Malgorzata	k1gFnSc2
Izabelly	Izabella	k1gFnSc2
Czartoryské	Czartoryská	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občanský	občanský	k2eAgInSc1d1
sňatek	sňatek	k1gInSc1
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1958	#num#	k4
a	a	k8xC
církevní	církevní	k2eAgInSc4d1
o	o	k7c4
den	den	k1gInSc4
později	pozdě	k6eAd2
v	v	k7c4
Altshausenu	Altshausen	k2eAgFnSc4d1
v	v	k7c6
Bádensko-Württembersku	Bádensko-Württembersek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alžběta	Alžběta	k1gFnSc1
porodila	porodit	k5eAaPmAgFnS
Antonínovi	Antonín	k1gMnSc3
čtyři	čtyři	k4xCgMnPc4
dětiː	dětiː	k?
</s>
<s>
František	František	k1gMnSc1
Filip	Filip	k1gMnSc1
Maria	Mario	k1gMnSc2
Josef	Josef	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Bourbonsko-Sicilský	bourbonsko-sicilský	k2eAgMnSc1d1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Karolina	Karolinum	k1gNnSc2
Johana	Johana	k1gFnSc1
Rosa	Rosa	k1gFnSc1
Cecílie	Cecílie	k1gFnSc1
Bourbonsko-Sicilská	bourbonsko-sicilský	k2eAgFnSc1d1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1962	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gennaro	Gennara	k1gFnSc5
Maria	Maria	k1gFnSc5
Pio	Pio	k1gMnPc1
Kazimír	Kazimír	k1gMnSc1
Bourbonsko-Sicilský	bourbonsko-sicilský	k2eAgMnSc1d1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Annunziata	Annunziata	k1gFnSc1
Urraca	Urraca	k1gFnSc1
Markéta	Markéta	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Bourbonsko-Sicilský	bourbonsko-sicilský	k2eAgMnSc1d1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tituly	titul	k1gInPc1
a	a	k8xC
oslovení	oslovení	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1933	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1858	#num#	k4
<g/>
ː	ː	k?
Její	její	k3xOp3gInSc4
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
vévodkyně	vévodkyně	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1958	#num#	k4
<g/>
ː	ː	k?
Její	její	k3xOp3gInSc4
královská	královský	k2eAgFnSc1d1
Výsost	výsost	k1gFnSc1
princezna	princezna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Bourbonsko-Sicilská	bourbonsko-sicilský	k2eAgFnSc1d1
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnSc1
württemberská	württemberský	k2eAgFnSc1d1
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Filip	Filip	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Orleánská	orleánský	k2eAgFnSc1d1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Rakousko-Těšínský	rakousko-těšínský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Rakousko-Těšínská	rakousko-těšínský	k2eAgFnSc1d1
</s>
<s>
Hildegarda	Hildegarda	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Filip	Filip	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
František	František	k1gMnSc1
Karel	Karel	k1gMnSc1
Habsbursko-Lotrinský	habsbursko-lotrinský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Rakousko-Uherský	rakousko-uherský	k2eAgMnSc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Frederika	Frederika	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Sofie	Sofia	k1gFnSc2
Rakouská	rakouský	k2eAgFnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Annunziata	Annunziat	k2eAgFnSc1d1
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Izabela	Izabela	k1gFnSc1
Rakouská	rakouský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toskánský	toskánský	k2eAgInSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Antonie	Antonie	k1gFnSc2
Sicilská	sicilský	k2eAgFnSc1d1
</s>
<s>
Petr	Petr	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Toskánský	toskánský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parmský	parmský	k2eAgInSc1d1
</s>
<s>
Alice	Alice	k1gFnSc1
Bourbonsko-Parmská	bourbonsko-parmský	k2eAgFnSc1d1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
z	z	k7c2
Artois	Artois	k1gFnSc2
</s>
<s>
Rosa	Rosa	k1gFnSc1
Rakousko-Toskánská	rakousko-toskánský	k2eAgFnSc1d1
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgInSc1d1
</s>
<s>
Alfons	Alfons	k1gMnSc1
Neapolsko-Sicilský	neapolsko-sicilský	k2eAgMnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Izabela	Izabela	k1gFnSc1
Rakouská	rakouský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
Bourbonsko-Sicilská	bourbonsko-sicilský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Trapani	Trapaň	k1gFnSc5
</s>
<s>
Marie	Marie	k1gFnSc1
Antonie	Antonie	k1gFnSc2
Neapolsko-Sicilská	neapolsko-sicilský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Izabela	Izabela	k1gFnSc1
Toskánská	toskánský	k2eAgFnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Duchess	Duchess	k1gInSc1
Elisabeth	Elisabeth	k1gInSc1
of	of	k?
Württemberg	Württemberg	k1gInSc1
(	(	kIx(
<g/>
b.	b.	k?
1933	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
2129154075983611860002	#num#	k4
</s>
