<p>
<s>
Demografie	demografie	k1gFnSc1	demografie
(	(	kIx(	(
<g/>
démos	démos	k1gInSc1	démos
-	-	kIx~	-
lid	lid	k1gInSc1	lid
graféin	graféin	k1gInSc1	graféin
-	-	kIx~	-
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
popisovat	popisovat	k5eAaImF	popisovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
proces	proces	k1gInSc4	proces
reprodukce	reprodukce	k1gFnSc2	reprodukce
lidských	lidský	k2eAgFnPc2d1	lidská
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Objektem	objekt	k1gInSc7	objekt
studia	studio	k1gNnSc2	studio
demografie	demografie	k1gFnSc2	demografie
tedy	tedy	k9	tedy
jsou	být	k5eAaImIp3nP	být
lidské	lidský	k2eAgFnPc1d1	lidská
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
předmětem	předmět	k1gInSc7	předmět
jejího	její	k3xOp3gNnSc2	její
studia	studio	k1gNnSc2	studio
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
demografické	demografický	k2eAgFnSc2d1	demografická
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přirozený	přirozený	k2eAgInSc1d1	přirozený
proces	proces	k1gInSc1	proces
obnovy	obnova	k1gFnSc2	obnova
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
důsledkem	důsledek	k1gInSc7	důsledek
rození	rození	k1gNnSc4	rození
a	a	k8xC	a
vymírání	vymírání	k1gNnSc4	vymírání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Procesy	proces	k1gInPc1	proces
demografické	demografický	k2eAgFnSc2d1	demografická
reprodukce	reprodukce	k1gFnSc2	reprodukce
jsou	být	k5eAaImIp3nP	být
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
(	(	kIx(	(
<g/>
též	též	k9	též
mortalita	mortalita	k1gFnSc1	mortalita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemocnost	nemocnost	k1gFnSc1	nemocnost
<g/>
,	,	kIx,	,
porodnost	porodnost	k1gFnSc1	porodnost
(	(	kIx(	(
<g/>
též	též	k9	též
natalita	natalita	k1gFnSc1	natalita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potratovost	potratovost	k1gFnSc1	potratovost
<g/>
,	,	kIx,	,
sňatečnost	sňatečnost	k1gFnSc1	sňatečnost
a	a	k8xC	a
rozvodovost	rozvodovost	k1gFnSc1	rozvodovost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
procesy	proces	k1gInPc1	proces
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
demografického	demografický	k2eAgInSc2d1	demografický
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c4	za
spoluzakladatele	spoluzakladatel	k1gMnSc4	spoluzakladatel
oboru	obor	k1gInSc2	obor
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
John	John	k1gMnSc1	John
Graunt	Graunt	k1gMnSc1	Graunt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
především	především	k9	především
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
a	a	k8xC	a
objevil	objevit	k5eAaPmAgInS	objevit
nerovnoměrnost	nerovnoměrnost	k1gFnSc4	nerovnoměrnost
mezi	mezi	k7c7	mezi
počtem	počet	k1gInSc7	počet
narozených	narozený	k2eAgMnPc2d1	narozený
chlapců	chlapec	k1gMnPc2	chlapec
a	a	k8xC	a
děvčat	děvče	k1gNnPc2	děvče
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
určil	určit	k5eAaPmAgInS	určit
stabilní	stabilní	k2eAgInSc1d1	stabilní
poměr	poměr	k1gInSc1	poměr
této	tento	k3xDgFnSc2	tento
nerovnoměrnosti	nerovnoměrnost	k1gFnSc2	nerovnoměrnost
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
demografem	demograf	k1gMnSc7	demograf
byl	být	k5eAaImAgMnS	být
Adolf	Adolf	k1gMnSc1	Adolf
Lambert	Lambert	k1gMnSc1	Lambert
Quetelet	Quetelet	k1gInSc4	Quetelet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
moderní	moderní	k2eAgInSc1d1	moderní
pravidla	pravidlo	k1gNnSc2	pravidlo
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
české	český	k2eAgFnSc2d1	Česká
demografie	demografie	k1gFnSc2	demografie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Antonín	Antonín	k1gMnSc1	Antonín
Boháč	Boháč	k1gMnSc1	Boháč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
demografických	demografický	k2eAgNnPc2d1	demografické
dat	datum	k1gNnPc2	datum
==	==	k?	==
</s>
</p>
<p>
<s>
Demografie	demografie	k1gFnSc1	demografie
čerpá	čerpat	k5eAaImIp3nS	čerpat
data	datum	k1gNnPc4	datum
z	z	k7c2	z
několika	několik	k4yIc2	několik
hlavních	hlavní	k2eAgInPc2d1	hlavní
pramenů	pramen	k1gInPc2	pramen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
-	-	kIx~	-
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
census	census	k1gInSc4	census
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
sčítání	sčítání	k1gNnSc1	sčítání
na	na	k7c6	na
území	území	k1gNnSc6	území
Zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
České	český	k2eAgFnSc2d1	Česká
provedla	provést	k5eAaPmAgFnS	provést
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
roku	rok	k1gInSc2	rok
1753	[number]	k4	1753
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dosud	dosud	k6eAd1	dosud
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
sčítání	sčítání	k1gNnSc1	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
evidence	evidence	k1gFnSc1	evidence
přirozené	přirozený	k2eAgFnSc2d1	přirozená
měny	měna	k1gFnSc2	měna
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
zahrnující	zahrnující	k2eAgInPc1d1	zahrnující
všechny	všechen	k3xTgInPc1	všechen
demografické	demografický	k2eAgInPc1d1	demografický
ukazatele	ukazatel	k1gInPc1	ukazatel
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
migrace	migrace	k1gFnSc2	migrace
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
<g/>
:	:	kIx,	:
porodnost	porodnost	k1gFnSc1	porodnost
<g/>
,	,	kIx,	,
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
,	,	kIx,	,
sňatečnost	sňatečnost	k1gFnSc1	sňatečnost
<g/>
,	,	kIx,	,
rozvodovost	rozvodovost	k1gFnSc1	rozvodovost
<g/>
,	,	kIx,	,
potratovost	potratovost	k1gFnSc1	potratovost
či	či	k8xC	či
nemocnost	nemocnost	k1gFnSc1	nemocnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zachycení	zachycení	k1gNnSc3	zachycení
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
soustava	soustava	k1gFnSc1	soustava
matrik	matrika	k1gFnPc2	matrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
evidence	evidence	k1gFnSc1	evidence
migrací	migrace	k1gFnPc2	migrace
je	být	k5eAaImIp3nS	být
sledována	sledovat	k5eAaImNgFnS	sledovat
pomocí	pomocí	k7c2	pomocí
povinného	povinný	k2eAgNnSc2d1	povinné
hlášení	hlášení	k1gNnSc2	hlášení
o	o	k7c6	o
trvalém	trvalý	k2eAgInSc6d1	trvalý
pobytu	pobyt	k1gInSc6	pobyt
</s>
</p>
<p>
<s>
evidence	evidence	k1gFnSc1	evidence
nemocnosti	nemocnost	k1gFnSc2	nemocnost
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
úplné	úplný	k2eAgFnSc2d1	úplná
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
výběrová	výběrový	k2eAgNnPc1d1	výběrové
šetření	šetření	k1gNnPc1	šetření
</s>
</p>
<p>
<s>
registry	registr	k1gInPc1	registr
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pocházejí	pocházet	k5eAaImIp3nP	pocházet
většinou	většina	k1gFnSc7	většina
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
</s>
</p>
<p>
<s>
historické	historický	k2eAgInPc4d1	historický
prameny	pramen	k1gInPc4	pramen
</s>
</p>
<p>
<s>
==	==	k?	==
Obory	obor	k1gInPc1	obor
demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Demografická	demografický	k2eAgFnSc1d1	demografická
analýza	analýza	k1gFnSc1	analýza
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
rozborem	rozbor	k1gInSc7	rozbor
složek	složka	k1gFnPc2	složka
demografické	demografický	k2eAgFnSc2d1	demografická
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
;	;	kIx,	;
hledá	hledat	k5eAaImIp3nS	hledat
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
znaky	znak	k1gInPc4	znak
demografických	demografický	k2eAgMnPc2d1	demografický
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jejich	jejich	k3xOp3gInSc4	jejich
průběh	průběh	k1gInSc4	průběh
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
demografické	demografický	k2eAgInPc4d1	demografický
ukazatele	ukazatel	k1gInPc4	ukazatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Demografická	demografický	k2eAgFnSc1d1	demografická
metodologie	metodologie	k1gFnSc1	metodologie
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
matematickou	matematický	k2eAgFnSc4d1	matematická
demografii	demografie	k1gFnSc4	demografie
<g/>
,	,	kIx,	,
demografickou	demografický	k2eAgFnSc4d1	demografická
statistiku	statistika	k1gFnSc4	statistika
atd.	atd.	kA	atd.
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Teoretická	teoretický	k2eAgFnSc1d1	teoretická
demografie	demografie	k1gFnSc1	demografie
-	-	kIx~	-
formuluje	formulovat	k5eAaImIp3nS	formulovat
hypotézy	hypotéza	k1gFnPc4	hypotéza
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
zákony	zákon	k1gInPc4	zákon
demografických	demografický	k2eAgInPc2d1	demografický
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
zobecňuje	zobecňovat	k5eAaImIp3nS	zobecňovat
pravidelnosti	pravidelnost	k1gFnSc3	pravidelnost
vývoje	vývoj	k1gInSc2	vývoj
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
demografie	demografie	k1gFnSc1	demografie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
demografickým	demografický	k2eAgNnSc7d1	demografické
studiem	studio	k1gNnSc7	studio
historických	historický	k2eAgFnPc2d1	historická
populací	populace	k1gFnPc2	populace
</s>
</p>
<p>
<s>
Paleodemografie	Paleodemografie	k1gFnSc1	Paleodemografie
–	–	k?	–
podobor	podobor	k1gInSc4	podobor
historické	historický	k2eAgFnSc2d1	historická
demografie	demografie	k1gFnSc2	demografie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
demografickým	demografický	k2eAgInSc7d1	demografický
rozborem	rozbor	k1gInSc7	rozbor
pravěkých	pravěký	k2eAgFnPc2d1	pravěká
populací	populace	k1gFnPc2	populace
</s>
</p>
<p>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
demografie	demografie	k1gFnSc1	demografie
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
vývojem	vývoj	k1gInSc7	vývoj
populací	populace	k1gFnPc2	populace
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
demografie	demografie	k1gFnSc1	demografie
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
vztahem	vztah	k1gInSc7	vztah
populačních	populační	k2eAgInPc2d1	populační
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geografie	geografie	k1gFnSc1	geografie
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
též	též	k9	též
geodemografie	geodemografie	k1gFnSc1	geodemografie
<g/>
,	,	kIx,	,
demogeografie	demogeografie	k1gFnSc1	demogeografie
<g/>
)	)	kIx)	)
-	-	kIx~	-
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
migrační	migrační	k2eAgInPc4d1	migrační
procesy	proces	k1gInPc4	proces
a	a	k8xC	a
rozmístění	rozmístění	k1gNnSc4	rozmístění
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gradologie	Gradologie	k1gFnSc1	Gradologie
-	-	kIx~	-
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
přemnožování	přemnožování	k1gNnSc1	přemnožování
škůdců	škůdce	k1gMnPc2	škůdce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lesních	lesní	k2eAgInPc6d1	lesní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aktuárská	Aktuárský	k2eAgFnSc1d1	Aktuárský
demografie	demografie	k1gFnSc1	demografie
(	(	kIx(	(
<g/>
též	též	k9	též
pojistná	pojistný	k2eAgFnSc1d1	pojistná
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pojistná	pojistný	k2eAgFnSc1d1	pojistná
technika	technika	k1gFnSc1	technika
<g/>
)	)	kIx)	)
-	-	kIx~	-
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
a	a	k8xC	a
životním	životní	k2eAgNnSc7d1	životní
pojištěním	pojištění	k1gNnSc7	pojištění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografická	demografický	k2eAgFnSc1d1	demografická
analýza	analýza	k1gFnSc1	analýza
==	==	k?	==
</s>
</p>
<p>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
rozborem	rozbor	k1gInSc7	rozbor
základních	základní	k2eAgFnPc2d1	základní
složek	složka	k1gFnPc2	složka
demografické	demografický	k2eAgFnSc2d1	demografická
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
proměn	proměna	k1gFnPc2	proměna
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
podmíněností	podmíněnost	k1gFnSc7	podmíněnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
a	a	k8xC	a
nemocnost	nemocnost	k1gFnSc1	nemocnost
</s>
</p>
<p>
<s>
porodnost	porodnost	k1gFnSc1	porodnost
a	a	k8xC	a
plodnost	plodnost	k1gFnSc1	plodnost
</s>
</p>
<p>
<s>
potratovost	potratovost	k1gFnSc1	potratovost
</s>
</p>
<p>
<s>
sňatečnost	sňatečnost	k1gFnSc1	sňatečnost
</s>
</p>
<p>
<s>
rozvodovost	rozvodovost	k1gFnSc1	rozvodovost
</s>
</p>
<p>
<s>
mobilita	mobilita	k1gFnSc1	mobilita
(	(	kIx(	(
<g/>
migrace	migrace	k1gFnSc1	migrace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Demografické	demografický	k2eAgInPc1d1	demografický
ukazatele	ukazatel	k1gInPc1	ukazatel
===	===	k?	===
</s>
</p>
<p>
<s>
Demografické	demografický	k2eAgInPc1d1	demografický
ukazatele	ukazatel	k1gInPc1	ukazatel
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
veškerá	veškerý	k3xTgNnPc4	veškerý
data	datum	k1gNnPc4	datum
vztahující	vztahující	k2eAgNnPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
procesům	proces	k1gInPc3	proces
demografické	demografický	k2eAgFnSc2d1	demografická
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
k	k	k7c3	k
úmrtnosti	úmrtnost	k1gFnSc3	úmrtnost
<g/>
,	,	kIx,	,
porodnosti	porodnost	k1gFnSc6	porodnost
<g/>
,	,	kIx,	,
sňatečnosti	sňatečnost	k1gFnSc6	sňatečnost
<g/>
,	,	kIx,	,
rozvodovosti	rozvodovost	k1gFnSc6	rozvodovost
<g/>
,	,	kIx,	,
nemocnosti	nemocnost	k1gFnSc6	nemocnost
a	a	k8xC	a
potratovosti	potratovost	k1gFnSc6	potratovost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnPc1d1	základní
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
čistá	čistý	k2eAgNnPc1d1	čisté
čísla	číslo	k1gNnPc1	číslo
získaná	získaný	k2eAgNnPc1d1	získané
z	z	k7c2	z
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
demografických	demografický	k2eAgInPc2d1	demografický
zdrojů	zdroj	k1gInPc2	zdroj
jako	jako	k8xS	jako
například	například	k6eAd1	například
ze	z	k7c2	z
sčítaní	sčítaný	k2eAgMnPc1d1	sčítaný
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dáváme	dávat	k5eAaImIp1nP	dávat
<g/>
-li	i	k?	-li
základní	základní	k2eAgNnPc4d1	základní
data	datum	k1gNnPc4	datum
do	do	k7c2	do
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
zkonstruovali	zkonstruovat	k5eAaPmAgMnP	zkonstruovat
nové	nový	k2eAgInPc4d1	nový
ukazatele	ukazatel	k1gInPc4	ukazatel
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
analytických	analytický	k2eAgNnPc6d1	analytické
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
ukazatele	ukazatel	k1gInPc1	ukazatel
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Poměrná	poměrný	k2eAgNnPc1d1	poměrné
čísla	číslo	k1gNnPc1	číslo
extenzitní	extenzitní	k2eAgInPc4d1	extenzitní
-	-	kIx~	-
ukazatele	ukazatel	k1gInPc4	ukazatel
====	====	k?	====
</s>
</p>
<p>
<s>
Poměrná	poměrný	k2eAgNnPc1d1	poměrné
čísla	číslo	k1gNnPc1	číslo
extenzitní	extenzitní	k2eAgNnPc1d1	extenzitní
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
srovnáváme	srovnávat	k5eAaImIp1nP	srovnávat
<g/>
-li	i	k?	-li
dva	dva	k4xCgInPc4	dva
údaje	údaj	k1gInPc4	údaj
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
časovém	časový	k2eAgInSc6d1	časový
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
povahy	povaha	k1gFnPc1	povaha
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
prostorově	prostorově	k6eAd1	prostorově
vymezeny	vymezen	k2eAgFnPc1d1	vymezena
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
ukazatele	ukazatel	k1gInSc2	ukazatel
často	často	k6eAd1	často
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
strukturu	struktura	k1gFnSc4	struktura
určitého	určitý	k2eAgInSc2d1	určitý
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
poměrným	poměrný	k2eAgNnSc7d1	poměrné
číslem	číslo	k1gNnSc7	číslo
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ukazatel	ukazatel	k1gInSc4	ukazatel
maskulinity	maskulinita	k1gFnSc2	maskulinita
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
udává	udávat	k5eAaImIp3nS	udávat
procento	procento	k1gNnSc4	procento
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Poměrná	poměrný	k2eAgNnPc1d1	poměrné
čísla	číslo	k1gNnPc1	číslo
intenzitní	intenzitní	k2eAgFnSc2d1	intenzitní
-	-	kIx~	-
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
kvocienty	kvocient	k1gInPc1	kvocient
====	====	k?	====
</s>
</p>
<p>
<s>
Poměrná	poměrný	k2eAgNnPc1d1	poměrné
čísla	číslo	k1gNnPc1	číslo
intenzitní	intenzitní	k2eAgFnSc2d1	intenzitní
neboli	neboli	k8xC	neboli
míry	míra	k1gFnSc2	míra
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
čitateli	čitatel	k1gInSc6	čitatel
uvedeny	uveden	k2eAgMnPc4d1	uveden
jevy	jev	k1gInPc4	jev
či	či	k8xC	či
události	událost	k1gFnPc4	událost
a	a	k8xC	a
ve	v	k7c6	v
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
jejich	jejich	k3xOp3gMnPc1	jejich
nositelé	nositel	k1gMnPc1	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
ve	v	k7c6	v
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
se	se	k3xPyFc4	se
konstruuje	konstruovat	k5eAaImIp3nS	konstruovat
buď	buď	k8xC	buď
výpočtem	výpočet	k1gInSc7	výpočet
průměru	průměr	k1gInSc2	průměr
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
a	a	k8xC	a
konce	konec	k1gInSc2	konec
sledovaného	sledovaný	k2eAgNnSc2d1	sledované
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
použitím	použití	k1gNnSc7	použití
údaje	údaj	k1gInSc2	údaj
z	z	k7c2	z
prostředku	prostředek	k1gInSc2	prostředek
sledovaného	sledovaný	k2eAgNnSc2d1	sledované
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
středním	střední	k2eAgInSc6d1	střední
stavu	stav	k1gInSc6	stav
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Míry	mír	k1gInPc1	mír
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
nositelů	nositel	k1gMnPc2	nositel
událostí	událost	k1gFnSc7	událost
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
míry	mír	k1gInPc1	mír
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
jako	jako	k9	jako
nositele	nositel	k1gMnPc4	nositel
událostí	událost	k1gFnSc7	událost
jen	jen	k9	jen
ty	ten	k3xDgFnPc4	ten
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
událost	událost	k1gFnSc1	událost
může	moct	k5eAaImIp3nS	moct
přihodit	přihodit	k5eAaPmF	přihodit
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
míra	míra	k1gFnSc1	míra
sňatečnosti	sňatečnost	k1gFnSc2	sňatečnost
svobodných	svobodný	k2eAgMnPc2d1	svobodný
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
střední	střední	k2eAgInSc4d1	střední
stav	stav	k1gInSc4	stav
svobodných	svobodný	k2eAgMnPc2d1	svobodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
typem	typ	k1gInSc7	typ
jsou	být	k5eAaImIp3nP	být
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
redukované	redukovaný	k2eAgFnPc4d1	redukovaná
míry	míra	k1gFnPc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
redukovaných	redukovaný	k2eAgFnPc2d1	redukovaná
měr	míra	k1gFnPc2	míra
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
uvedeny	uveden	k2eAgFnPc1d1	uvedena
populace	populace	k1gFnPc1	populace
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
k	k	k7c3	k
sledované	sledovaný	k2eAgFnSc3d1	sledovaná
události	událost	k1gFnSc3	událost
dojít	dojít	k5eAaPmF	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Redukce	redukce	k1gFnSc1	redukce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvojího	dvojí	k4xRgMnSc2	dvojí
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Redukce	redukce	k1gFnPc4	redukce
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rodinnému	rodinný	k2eAgInSc3d1	rodinný
stavu	stav	k1gInSc3	stav
-	-	kIx~	-
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
míra	míra	k1gFnSc1	míra
sňatečnosti	sňatečnost	k1gFnSc2	sňatečnost
-	-	kIx~	-
poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
sňatků	sňatek	k1gInPc2	sňatek
svobodných	svobodný	k2eAgInPc2d1	svobodný
v	v	k7c6	v
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
věku	věk	k1gInSc6	věk
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
populaci	populace	k1gFnSc3	populace
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rodinný	rodinný	k2eAgInSc4d1	rodinný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Redukce	redukce	k1gFnPc4	redukce
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
trvání	trvání	k1gNnSc2	trvání
-	-	kIx~	-
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
míra	míra	k1gFnSc1	míra
rozvodovosti	rozvodovost	k1gFnSc2	rozvodovost
je	být	k5eAaImIp3nS	být
poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
rozvodů	rozvod	k1gInPc2	rozvod
k	k	k7c3	k
výchozímu	výchozí	k2eAgInSc3d1	výchozí
počtu	počet	k1gInSc3	počet
sňatků	sňatek	k1gInPc2	sňatek
</s>
</p>
<p>
<s>
Kvocienty	kvocient	k1gInPc1	kvocient
-	-	kIx~	-
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
měr	míra	k1gFnPc2	míra
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoby	osoba	k1gFnPc1	osoba
uváděné	uváděný	k2eAgFnPc4d1	uváděná
ve	v	k7c6	v
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
nejsou	být	k5eNaImIp3nP	být
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
událost	událost	k1gFnSc1	událost
přihodit	přihodit	k5eAaPmF	přihodit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přímo	přímo	k6eAd1	přímo
soubor	soubor	k1gInSc4	soubor
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
se	se	k3xPyFc4	se
daná	daný	k2eAgFnSc1d1	daná
událost	událost	k1gFnSc1	událost
skutečně	skutečně	k6eAd1	skutečně
přihodila	přihodit	k5eAaPmAgFnS	přihodit
<g/>
.	.	kIx.	.
</s>
<s>
Kvocient	kvocient	k1gInSc1	kvocient
kojenecké	kojenecký	k2eAgFnSc2d1	kojenecká
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
například	například	k6eAd1	například
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
počet	počet	k1gInSc1	počet
zemřelých	zemřelý	k1gMnPc2	zemřelý
v	v	k7c6	v
dokončeném	dokončený	k2eAgInSc6d1	dokončený
věku	věk	k1gInSc6	věk
0	[number]	k4	0
k	k	k7c3	k
výchozímu	výchozí	k2eAgInSc3d1	výchozí
počtu	počet	k1gInSc3	počet
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgInPc2d1	narozený
a	a	k8xC	a
ne	ne	k9	ne
ke	k	k7c3	k
střednímu	střední	k2eAgInSc3d1	střední
stavu	stav	k1gInSc3	stav
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
dokončeném	dokončený	k2eAgInSc6d1	dokončený
věku	věk	k1gInSc6	věk
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Poměrná	poměrný	k2eAgNnPc1d1	poměrné
čísla	číslo	k1gNnPc1	číslo
srovnávací	srovnávací	k2eAgInPc4d1	srovnávací
-	-	kIx~	-
indexy	index	k1gInPc4	index
====	====	k?	====
</s>
</p>
<p>
<s>
Indexy	index	k1gInPc1	index
srovnávají	srovnávat	k5eAaImIp3nP	srovnávat
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
buď	buď	k8xC	buď
nesouvisí	souviset	k5eNaImIp3nS	souviset
časově	časově	k6eAd1	časově
nebo	nebo	k8xC	nebo
nejsou	být	k5eNaImIp3nP	být
stejně	stejně	k6eAd1	stejně
prostorově	prostorově	k6eAd1	prostorově
vymezena	vymezit	k5eAaPmNgFnS	vymezit
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
rozvodovosti	rozvodovost	k1gFnSc2	rozvodovost
například	například	k6eAd1	například
udává	udávat	k5eAaImIp3nS	udávat
poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
rozvodů	rozvod	k1gInPc2	rozvod
k	k	k7c3	k
počtu	počet	k1gInSc3	počet
sňatků	sňatek	k1gInPc2	sňatek
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čas	čas	k1gInSc1	čas
v	v	k7c6	v
demografické	demografický	k2eAgFnSc6d1	demografická
analýze	analýza	k1gFnSc6	analýza
===	===	k?	===
</s>
</p>
<p>
<s>
Čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
demografii	demografie	k1gFnSc6	demografie
důležitou	důležitý	k2eAgFnSc7d1	důležitá
proměnou	proměna	k1gFnSc7	proměna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
ročník	ročník	k1gInSc1	ročník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
osob	osoba	k1gFnPc2	osoba
narozených	narozený	k2eAgFnPc2d1	narozená
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kohorta	kohorta	k1gFnSc1	kohorta
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
demografické	demografický	k2eAgFnSc3d1	demografická
události	událost	k1gFnSc3	událost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
sňatku	sňatek	k1gInSc2	sňatek
<g/>
,	,	kIx,	,
potratu	potrat	k1gInSc2	potrat
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Metody	metoda	k1gFnPc1	metoda
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
znázorňování	znázorňování	k1gNnSc3	znázorňování
demografických	demografický	k2eAgInPc2d1	demografický
jevů	jev	k1gInPc2	jev
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
používány	používat	k5eAaImNgInP	používat
některé	některý	k3yIgInPc1	některý
speciální	speciální	k2eAgInPc1d1	speciální
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
Witthauerův	Witthauerův	k2eAgInSc4d1	Witthauerův
diagram	diagram	k1gInSc4	diagram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dvě	dva	k4xCgNnPc4	dva
základní	základní	k2eAgNnPc4d1	základní
dělení	dělení	k1gNnPc4	dělení
struktur	struktura	k1gFnPc2	struktura
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
poznatkem	poznatek	k1gInSc7	poznatek
vyplývajícím	vyplývající	k2eAgInSc7d1	vyplývající
z	z	k7c2	z
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
struktury	struktura	k1gFnSc2	struktura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
nerovnoměrnost	nerovnoměrnost	k1gFnSc1	nerovnoměrnost
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Chlapců	chlapec	k1gMnPc2	chlapec
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
vyššího	vysoký	k2eAgInSc2d2	vyšší
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
faktorem	faktor	k1gInSc7	faktor
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
strukturu	struktura	k1gFnSc4	struktura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
pracovní	pracovní	k2eAgFnSc1d1	pracovní
migrace	migrace	k1gFnSc1	migrace
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
věkové	věkový	k2eAgFnSc2d1	věková
struktury	struktura	k1gFnSc2	struktura
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
skupiny	skupina	k1gFnPc4	skupina
<g/>
;	;	kIx,	;
dětskou	dětský	k2eAgFnSc4d1	dětská
složku	složka	k1gFnSc4	složka
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reprodukční	reprodukční	k2eAgFnSc4d1	reprodukční
složku	složka	k1gFnSc4	složka
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
a	a	k8xC	a
postreprodukční	postreprodukční	k2eAgFnSc4d1	postreprodukční
složku	složka	k1gFnSc4	složka
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
znázornění	znázornění	k1gNnSc3	znázornění
věkové	věkový	k2eAgFnSc2d1	věková
struktury	struktura	k1gFnSc2	struktura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
věková	věkový	k2eAgFnSc1d1	věková
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
možné	možný	k2eAgNnSc1d1	možné
dělení	dělení	k1gNnSc1	dělení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
dosaženého	dosažený	k2eAgNnSc2d1	dosažené
formálního	formální	k2eAgNnSc2d1	formální
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
LeGrand	legranda	k1gFnPc2	legranda
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Auerhan	Auerhan	k1gMnSc1	Auerhan
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Boháč	Boháč	k1gMnSc1	Boháč
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Fajfr	Fajfr	k1gMnSc1	Fajfr
</s>
</p>
<p>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Gompertz	Gompertz	k1gMnSc1	Gompertz
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Graunt	Graunt	k1gMnSc1	Graunt
</s>
</p>
<p>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
Halley	Halley	k1gMnSc1	Halley
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Hampl	Hampl	k1gMnSc1	Hampl
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Korčák	Korčák	k1gMnSc1	Korčák
</s>
</p>
<p>
<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Lexis	Lexis	k1gFnSc2	Lexis
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Matiegka	Matiegka	k1gMnSc1	Matiegka
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Melič	melič	k1gMnSc1	melič
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Müller	Müller	k1gMnSc1	Müller
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Netušil	Netušil	k1gMnSc1	Netušil
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Pavlík	Pavlík	k1gMnSc1	Pavlík
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Lambert	Lambert	k1gMnSc1	Lambert
Quetelet	Quetelet	k1gInSc4	Quetelet
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
Roslong	Roslong	k1gMnSc1	Roslong
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
národnost	národnost	k1gFnSc1	národnost
<g/>
,	,	kIx,	,
Jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc1	náboženství
</s>
</p>
<p>
<s>
Demografická	demografický	k2eAgFnSc1d1	demografická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
Druhý	druhý	k4xOgInSc1	druhý
demografický	demografický	k2eAgInSc1d1	demografický
přechod	přechod	k1gInSc1	přechod
</s>
</p>
<p>
<s>
Populační	populační	k2eAgFnSc1d1	populační
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
Kontrola	kontrola	k1gFnSc1	kontrola
porodnosti	porodnost	k1gFnSc2	porodnost
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
</s>
</p>
<p>
<s>
List	list	k1gInSc1	list
of	of	k?	of
population	population	k1gInSc1	population
concern	concern	k1gMnSc1	concern
organizations	organizations	k1gInSc1	organizations
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
.	.	kIx.	.
<g/>
wikipedia	wikipedium	k1gNnSc2	wikipedium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
demografie	demografie	k1gFnSc2	demografie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
http://demografie.info	[url]	k6eAd1	http://demografie.info
-	-	kIx~	-
Demografický	demografický	k2eAgInSc1d1	demografický
informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
http://www.nationmaster.com/	[url]	k?	http://www.nationmaster.com/
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
https://web.archive.org/web/20090604214506/http://prb.org/Reports/2004/PopulationHandbook5thedition.asgx	[url]	k1gInSc1	https://web.archive.org/web/20090604214506/http://prb.org/Reports/2004/PopulationHandbook5thedition.asgx
-	-	kIx~	-
Population	Population	k1gInSc1	Population
HandBook	handbook	k1gInSc1	handbook
(	(	kIx(	(
<g/>
pdf	pdf	k?	pdf
<g/>
)	)	kIx)	)
-	-	kIx~	-
shrnutí	shrnutí	k1gNnSc4	shrnutí
všech	všecek	k3xTgInPc2	všecek
demografických	demografický	k2eAgInPc2d1	demografický
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
způsobů	způsob	k1gInPc2	způsob
</s>
</p>
<p>
<s>
Geografická	geografický	k2eAgFnSc1d1	geografická
bibliografie	bibliografie	k1gFnSc1	bibliografie
ČR	ČR	kA	ČR
online	onlinout	k5eAaPmIp3nS	onlinout
Informace	informace	k1gFnPc4	informace
o	o	k7c6	o
geografické	geografický	k2eAgFnSc6d1	geografická
a	a	k8xC	a
demografické	demografický	k2eAgFnSc6d1	demografická
produkci	produkce	k1gFnSc6	produkce
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
bibliografie	bibliografie	k1gFnSc1	bibliografie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vybrané	vybraný	k2eAgInPc4d1	vybraný
plnotextové	plnotextový	k2eAgInPc4d1	plnotextový
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
