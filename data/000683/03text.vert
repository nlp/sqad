<s>
Bounce	Bounec	k1gInPc4	Bounec
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
americké	americký	k2eAgFnSc2d1	americká
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Undivided	Undivided	k1gInSc4	Undivided
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
53	[number]	k4	53
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Everyday	Everyda	k2eAgFnPc1d1	Everyda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Distance	distance	k1gFnSc1	distance
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
48	[number]	k4	48
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Joey	Joe	k2eAgFnPc1d1	Joea
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Misunderstood	Misunderstood	k1gInSc1	Misunderstood
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
About	About	k1gMnSc1	About
Lovin	Lovin	k1gMnSc1	Lovin
<g/>
'	'	kIx"	'
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Hook	Hook	k1gMnSc1	Hook
Me	Me	k1gMnSc1	Me
Up	Up	k1gMnSc1	Up
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Right	Right	k1gMnSc1	Right
Side	Sid	k1gFnSc2	Sid
Of	Of	k1gMnSc1	Of
Wrong	Wrong	k1gMnSc1	Wrong
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Me	Me	k1gMnPc3	Me
Back	Backa	k1gFnPc2	Backa
To	to	k9	to
Life	Life	k1gNnSc2	Life
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
Had	had	k1gMnSc1	had
Me	Me	k1gFnSc1	Me
From	Fro	k1gNnSc7	Fro
Hello	Hello	k1gNnSc1	Hello
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Bounce	Bounec	k1gInPc1	Bounec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Open	Open	k1gMnSc1	Open
All	All	k1gMnSc1	All
Night	Night	k1gMnSc1	Night
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
píseň	píseň	k1gFnSc1	píseň
Bounce	Bounec	k1gInSc2	Bounec
byla	být	k5eAaImAgFnS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
jako	jako	k8xS	jako
singl	singl	k1gInSc1	singl
pro	pro	k7c4	pro
americká	americký	k2eAgNnPc4d1	americké
rocková	rockový	k2eAgNnPc4d1	rockové
rádia	rádio	k1gNnPc4	rádio
album	album	k1gNnSc1	album
Bounce	Bounec	k1gInSc2	Bounec
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
umístilo	umístit	k5eAaPmAgNnS	umístit
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
příčka	příčka	k1gFnSc1	příčka
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
pátá	pátý	k4xOgFnSc1	pátý
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
tato	tento	k3xDgNnPc4	tento
ocenění	ocenění	k1gNnPc4	ocenění
<g/>
:	:	kIx,	:
platinové	platinový	k2eAgNnSc4d1	platinové
1	[number]	k4	1
<g/>
x	x	k?	x
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgInPc1d1	zlatý
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
album	album	k1gNnSc1	album
Bounce	Bounec	k1gInSc2	Bounec
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
rockových	rockový	k2eAgFnPc2d1	rocková
kapel	kapela	k1gFnPc2	kapela
byly	být	k5eAaImAgFnP	být
nejúspěšnější	úspěšný	k2eAgFnPc1d3	nejúspěšnější
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k8xS	jako
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
či	či	k8xC	či
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
a	a	k8xC	a
právě	právě	k6eAd1	právě
mírný	mírný	k2eAgInSc4d1	mírný
příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
numetalu	numetal	k1gMnSc3	numetal
můžeme	moct	k5eAaImIp1nP	moct
cítit	cítit	k5eAaImF	cítit
i	i	k9	i
z	z	k7c2	z
desky	deska	k1gFnSc2	deska
Bounce	Bounec	k1gInSc2	Bounec
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
píseň	píseň	k1gFnSc1	píseň
Everyday	Everydaa	k1gFnSc2	Everydaa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
dle	dle	k7c2	dle
nich	on	k3xPp3gNnPc2	on
předpoklady	předpoklad	k1gInPc1	předpoklad
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
větším	veliký	k2eAgInSc7d2	veliký
hitem	hit	k1gInSc7	hit
než	než	k8xS	než
It	It	k1gFnSc7	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Life	Life	k1gFnPc7	Life
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
byla	být	k5eAaImAgFnS	být
patrná	patrný	k2eAgFnSc1d1	patrná
z	z	k7c2	z
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
propagace	propagace	k1gFnSc2	propagace
a	a	k8xC	a
promo	promo	k6eAd1	promo
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
výsledek	výsledek	k1gInSc4	výsledek
byl	být	k5eAaImAgMnS	být
zklamáním	zklamání	k1gNnSc7	zklamání
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Jon	Jon	k1gMnSc1	Jon
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
píseň	píseň	k1gFnSc1	píseň
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
příčce	příčka	k1gFnSc6	příčka
tamní	tamní	k2eAgFnSc2d1	tamní
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
skvělý	skvělý	k2eAgInSc1d1	skvělý
výsledek	výsledek	k1gInSc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc1	Jovi
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
Richie	Richie	k1gFnSc1	Richie
Sambora	Sambora	k1gFnSc1	Sambora
-	-	kIx~	-
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
Tico	Tico	k6eAd1	Tico
Torres	Torresa	k1gFnPc2	Torresa
-	-	kIx~	-
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
David	David	k1gMnSc1	David
Bryan	Bryan	k1gMnSc1	Bryan
-	-	kIx~	-
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
Hugh	Hugha	k1gFnPc2	Hugha
McDonald	McDonalda	k1gFnPc2	McDonalda
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
Bounce	Bounka	k1gFnSc3	Bounka
Tour	Toura	k1gFnPc2	Toura
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
turné	turné	k1gNnSc1	turné
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každou	každý	k3xTgFnSc4	každý
noc	noc	k1gFnSc4	noc
jednu	jeden	k4xCgFnSc4	jeden
píseň	píseň	k1gFnSc4	píseň
odzpíval	odzpívat	k5eAaPmAgMnS	odzpívat
Richie	Richie	k1gFnSc2	Richie
Sambora	Sambor	k1gMnSc2	Sambor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Be	Be	k1gMnSc5	Be
There	Ther	k1gMnSc5	Ther
For	forum	k1gNnPc2	forum
You	You	k1gFnSc4	You
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k8xC	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
kdy	kdy	k6eAd1	kdy
nahráli	nahrát	k5eAaBmAgMnP	nahrát
<g/>
.	.	kIx.	.
</s>
<s>
Předskokany	předskokan	k1gMnPc4	předskokan
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Goo	Goa	k1gFnSc5	Goa
Goo	Goa	k1gFnSc5	Goa
Dolls	Dolls	k1gInSc1	Dolls
<g/>
,	,	kIx,	,
Live	Live	k1gInSc1	Live
<g/>
,	,	kIx,	,
Krezip	Krezip	k1gInSc1	Krezip
<g/>
,	,	kIx,	,
a	a	k8xC	a
Sheryl	Sheryl	k1gInSc1	Sheryl
Crow	Crow	k1gFnSc2	Crow
<g/>
.	.	kIx.	.
</s>
