<s>
První	první	k4xOgFnSc1	první
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
roku	rok	k1gInSc2	rok
1095	[number]	k4	1095
na	na	k7c6	na
clermontském	clermontský	k2eAgInSc6d1	clermontský
koncilu	koncil	k1gInSc6	koncil
papežem	papež	k1gMnSc7	papež
Urbanem	Urban	k1gMnSc7	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
směřovala	směřovat	k5eAaImAgFnS	směřovat
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
za	za	k7c4	za
znovudobytí	znovudobytí	k1gNnSc4	znovudobytí
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
od	od	k7c2	od
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
