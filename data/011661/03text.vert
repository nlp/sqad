<p>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
1907	[number]	k4	1907
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Automobile	automobil	k1gInSc6	automobil
Club	club	k1gInSc4	club
de	de	k?	de
France	Franc	k1gMnSc4	Franc
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Dieppe	Diepp	k1gInSc5	Diepp
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Závod	závod	k1gInSc4	závod
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
startu	start	k1gInSc6	start
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
38	[number]	k4	38
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
ujet	ujet	k5eAaPmF	ujet
10	[number]	k4	10
kol	kolo	k1gNnPc2	kolo
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
po	po	k7c6	po
veřejných	veřejný	k2eAgFnPc6d1	veřejná
silnicích	silnice	k1gFnPc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
okruh	okruh	k1gInSc4	okruh
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
téměř	téměř	k6eAd1	téměř
77	[number]	k4	77
km	km	kA	km
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Dieppe	Diepp	k1gInSc5	Diepp
pouštěny	pouštěn	k2eAgInPc1d1	pouštěn
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
startujícím	startující	k2eAgMnSc7d1	startující
byl	být	k5eAaImAgInS	být
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Lancia	Lancia	k1gFnSc1	Lancia
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
Fiat	fiat	k1gInSc1	fiat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnPc4	první
tři	tři	k4xCgNnPc4	tři
kola	kolo	k1gNnPc4	kolo
vedl	vést	k5eAaImAgInS	vést
Louis	Louis	k1gMnSc1	Louis
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
,	,	kIx,	,
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
Arthur	Arthura	k1gFnPc2	Arthura
Duray	Duraa	k1gFnSc2	Duraa
<g/>
.	.	kIx.	.
</s>
<s>
Duray	Duray	k1gInPc4	Duray
zajel	zajet	k5eAaPmAgMnS	zajet
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
kolo	kolo	k1gNnSc4	kolo
závodu	závod	k1gInSc2	závod
s	s	k7c7	s
časem	čas	k1gInSc7	čas
a	a	k8xC	a
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
rychlostí	rychlost	k1gFnSc7	rychlost
121,34	[number]	k4	121,34
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
v	v	k7c6	v
devátém	devátý	k4xOgInSc6	devátý
kole	kolo	k1gNnSc6	kolo
však	však	k9	však
musel	muset	k5eAaImAgMnS	muset
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
závodu	závod	k1gInSc2	závod
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
Felice	Felice	k1gFnSc2	Felice
Nazzaro	Nazzara	k1gFnSc5	Nazzara
(	(	kIx(	(
<g/>
Fiat	fiat	k1gInSc1	fiat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
vítěze	vítěz	k1gMnSc4	vítěz
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
46	[number]	k4	46
minut	minuta	k1gFnPc2	minuta
33	[number]	k4	33
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
dojel	dojet	k5eAaPmAgMnS	dojet
maďarský	maďarský	k2eAgMnSc1d1	maďarský
závodník	závodník	k1gMnSc1	závodník
Ferenc	Ferenc	k1gMnSc1	Ferenc
Szisz	Szisz	k1gMnSc1	Szisz
(	(	kIx(	(
<g/>
Renault	renault	k1gInSc1	renault
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
francouzský	francouzský	k2eAgMnSc1d1	francouzský
jezdec	jezdec	k1gMnSc1	jezdec
Paul	Paul	k1gMnSc1	Paul
Baras	Baras	k1gMnSc1	Baras
(	(	kIx(	(
<g/>
Brasier	Brasier	k1gMnSc1	Brasier
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nazzarova	Nazzarův	k2eAgFnSc1d1	Nazzarův
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
byla	být	k5eAaImAgFnS	být
113,6	[number]	k4	113,6
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
===	===	k?	===
Nehoda	nehoda	k1gFnSc1	nehoda
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
tréninku	trénink	k1gInSc6	trénink
před	před	k7c7	před
závodem	závod	k1gInSc7	závod
se	se	k3xPyFc4	se
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
smrtelně	smrtelně	k6eAd1	smrtelně
zranil	zranit	k5eAaPmAgMnS	zranit
Albert	Albert	k1gMnSc1	Albert
Clément	Clément	k1gMnSc1	Clément
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
majitele	majitel	k1gMnSc2	majitel
továrny	továrna	k1gFnSc2	továrna
a	a	k8xC	a
týmu	tým	k1gInSc2	tým
Clément-Bayard	Clément-Bayarda	k1gFnPc2	Clément-Bayarda
Adolphe	Adolphe	k1gFnSc1	Adolphe
Clémenta-Bayarda	Clémenta-Bayarda	k1gFnSc1	Clémenta-Bayarda
<g/>
.	.	kIx.	.
</s>
<s>
Vzávodě	Vzávodě	k6eAd1	Vzávodě
jej	on	k3xPp3gNnSc4	on
nahradil	nahradit	k5eAaPmAgInS	nahradit
Alezi	Aleze	k1gFnSc4	Aleze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
poruchu	porucha	k1gFnSc4	porucha
nedojel	dojet	k5eNaPmAgMnS	dojet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
ztratil	ztratit	k5eAaPmAgMnS	ztratit
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
motoristický	motoristický	k2eAgInSc4d1	motoristický
sport	sport	k1gInSc4	sport
a	a	k8xC	a
značka	značka	k1gFnSc1	značka
Clement-Bayard	Clement-Bayarda	k1gFnPc2	Clement-Bayarda
se	se	k3xPyFc4	se
přestala	přestat	k5eAaPmAgFnS	přestat
následujícího	následující	k2eAgInSc2d1	následující
ročníku	ročník	k1gInSc2	ročník
závodů	závod	k1gInPc2	závod
účastnit	účastnit	k5eAaImF	účastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
1907	[number]	k4	1907
French	French	k1gMnSc1	French
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
1906	[number]	k4	1906
</s>
</p>
<p>
<s>
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
1908	[number]	k4	1908
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Francie	Francie	k1gFnSc2	Francie
1907	[number]	k4	1907
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
