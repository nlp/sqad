<p>
<s>
Svízel	svízel	k1gInSc1	svízel
sudetský	sudetský	k2eAgInSc1d1	sudetský
(	(	kIx(	(
<g/>
Galium	galium	k1gNnSc1	galium
sudeticum	sudeticum	k1gNnSc1	sudeticum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drobná	drobný	k2eAgFnSc1d1	drobná
<g/>
,	,	kIx,	,
nenápadná	nápadný	k2eNgFnSc1d1	nenápadná
<g/>
,	,	kIx,	,
bíle	bíle	k6eAd1	bíle
kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
bylina	bylina	k1gFnSc1	bylina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
svízel	svízel	k1gFnSc4	svízel
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
jen	jen	k9	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
málo	málo	k6eAd1	málo
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
potenciálně	potenciálně	k6eAd1	potenciálně
ohroženou	ohrožený	k2eAgFnSc4d1	ohrožená
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
neoendemit	neoendemit	k1gInSc4	neoendemit
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
sporadicky	sporadicky	k6eAd1	sporadicky
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
křížením	křížení	k1gNnSc7	křížení
mezi	mezi	k7c7	mezi
svízelem	svízel	k1gInSc7	svízel
nestejnolistým	stejnolistý	k2eNgInSc7d1	stejnolistý
a	a	k8xC	a
svízelem	svízel	k1gInSc7	svízel
moravským	moravský	k2eAgInSc7d1	moravský
v	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
před	před	k7c7	před
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
ledovou	ledový	k2eAgFnSc7d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
glaciální	glaciální	k2eAgInSc1d1	glaciální
relikt	relikt	k1gInSc1	relikt
a	a	k8xC	a
živou	živý	k2eAgFnSc7d1	živá
připomínkou	připomínka	k1gFnSc7	připomínka
předglaciálního	předglaciální	k2eAgNnSc2d1	předglaciální
křížení	křížení	k1gNnSc2	křížení
<g/>
.	.	kIx.	.
<g/>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
až	až	k6eAd1	až
třech	tři	k4xCgFnPc6	tři
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Prvá	prvý	k4xOgFnSc1	prvý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
kde	kde	k6eAd1	kde
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
Obřím	obří	k2eAgInSc6d1	obří
dole	dol	k1gInSc6	dol
v	v	k7c6	v
karech	kar	k1gInPc6	kar
Čertova	čertův	k2eAgFnSc1d1	Čertova
zahrádka	zahrádka	k1gFnSc1	zahrádka
a	a	k8xC	a
Rudník	rudník	k1gMnSc1	rudník
a	a	k8xC	a
na	na	k7c6	na
hřebeni	hřeben	k1gInSc6	hřeben
mezi	mezi	k7c7	mezi
Velkou	velký	k2eAgFnSc7d1	velká
a	a	k8xC	a
Malou	malý	k2eAgFnSc7d1	malá
Kotelní	kotelní	k2eAgFnSc7d1	kotelní
jámou	jáma	k1gFnSc7	jáma
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
na	na	k7c6	na
území	území	k1gNnSc6	území
Polska	Polsko	k1gNnSc2	Polsko
se	se	k3xPyFc4	se
ojediněle	ojediněle	k6eAd1	ojediněle
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
a	a	k8xC	a
Velké	velký	k2eAgFnSc6d1	velká
sněžné	sněžný	k2eAgFnSc6d1	sněžná
jámě	jáma	k1gFnSc6	jáma
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Slavkovském	slavkovský	k2eAgInSc6d1	slavkovský
lese	les	k1gInSc6	les
kde	kde	k6eAd1	kde
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
devíti	devět	k4xCc6	devět
lokalitách	lokalita	k1gFnPc6	lokalita
na	na	k7c6	na
hadcových	hadcový	k2eAgFnPc6d1	hadcová
skalkách	skalka	k1gFnPc6	skalka
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
rezervacích	rezervace	k1gFnPc6	rezervace
Vlček	vlček	k1gInSc1	vlček
a	a	k8xC	a
Planý	planý	k2eAgInSc1d1	planý
vrch	vrch	k1gInSc1	vrch
a	a	k8xC	a
v	v	k7c6	v
národní	národní	k2eAgFnSc3d1	národní
přírodní	přírodní	k2eAgFnSc3d1	přírodní
památce	památka	k1gFnSc3	památka
Křížky	křížek	k1gInPc4	křížek
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Cheb	Cheb	k1gInSc1	Cheb
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc3d1	přírodní
památce	památka	k1gFnSc3	památka
Dominova	Dominův	k2eAgFnSc1d1	Dominova
skalka	skalka	k1gFnSc1	skalka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Sokolov	Sokolov	k1gInSc1	Sokolov
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
ze	z	k7c2	z
Slavkovském	slavkovský	k2eAgInSc6d1	slavkovský
lesa	les	k1gInSc2	les
mají	mít	k5eAaImIp3nP	mít
drobné	drobný	k2eAgFnPc4d1	drobná
morfologické	morfologický	k2eAgFnPc4d1	morfologická
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
krkonošských	krkonošský	k2eAgMnPc2d1	krkonošský
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
uváděna	uváděn	k2eAgFnSc1d1	uváděna
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
kotlině	kotlina	k1gFnSc6	kotlina
v	v	k7c6	v
Hrubém	hrubý	k2eAgInSc6d1	hrubý
Jeseníku	Jeseník	k1gInSc6	Jeseník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ale	ale	k9	ale
svízel	svízel	k1gInSc1	svízel
sudetský	sudetský	k2eAgInSc1d1	sudetský
nepodařilo	podařit	k5eNaPmAgNnS	podařit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
od	od	k7c2	od
1000	[number]	k4	1000
do	do	k7c2	do
1300	[number]	k4	1300
m	m	kA	m
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Slavkovském	slavkovský	k2eAgInSc6d1	slavkovský
lese	les	k1gInSc6	les
mezi	mezi	k7c7	mezi
650	[number]	k4	650
až	až	k9	až
880	[number]	k4	880
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
v	v	k7c6	v
montánním	montánní	k2eAgInSc6d1	montánní
až	až	k8xS	až
subalpínském	subalpínský	k2eAgInSc6d1	subalpínský
výškovém	výškový	k2eAgInSc6d1	výškový
stupni	stupeň	k1gInSc6	stupeň
v	v	k7c6	v
rozvolněných	rozvolněný	k2eAgInPc6d1	rozvolněný
trávnících	trávník	k1gInPc6	trávník
a	a	k8xC	a
sutích	suť	k1gFnPc6	suť
zásaditých	zásaditý	k2eAgFnPc2d1	zásaditá
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
čedič	čedič	k1gInSc4	čedič
<g/>
,	,	kIx,	,
žula	žout	k5eAaImAgFnS	žout
<g/>
,	,	kIx,	,
serpentinit	serpentinit	k5eAaPmF	serpentinit
nebo	nebo	k8xC	nebo
vápenec	vápenec	k1gInSc4	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
půdy	půda	k1gFnPc4	půda
sušší	suchý	k2eAgFnPc4d2	sušší
<g/>
,	,	kIx,	,
mělké	mělký	k2eAgFnPc4d1	mělká
<g/>
,	,	kIx,	,
na	na	k7c4	na
humus	humus	k1gInSc4	humus
chudé	chudý	k2eAgNnSc1d1	chudé
<g/>
,	,	kIx,	,
minerálně	minerálně	k6eAd1	minerálně
bohaté	bohatý	k2eAgFnPc1d1	bohatá
a	a	k8xC	a
zásadité	zásaditý	k2eAgFnPc1d1	zásaditá
až	až	k6eAd1	až
neutrální	neutrální	k2eAgFnPc1d1	neutrální
<g/>
,	,	kIx,	,
nutně	nutně	k6eAd1	nutně
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
nezastíněné	zastíněný	k2eNgFnPc4d1	nezastíněná
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
teplých	teplý	k2eAgFnPc6d1	teplá
<g/>
,	,	kIx,	,
ke	k	k7c3	k
slunci	slunce	k1gNnSc3	slunce
přikloněných	přikloněný	k2eAgFnPc6d1	přikloněná
stráních	stráň	k1gFnPc6	stráň
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
často	často	k6eAd1	často
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sněžné	sněžný	k2eAgFnPc1d1	sněžná
laviny	lavina	k1gFnPc1	lavina
narušují	narušovat	k5eAaImIp3nP	narušovat
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
semenáčům	semenáč	k1gInPc3	semenáč
snáze	snadno	k6eAd2	snadno
kořenit	kořenit	k5eAaImF	kořenit
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ovšem	ovšem	k9	ovšem
lavina	lavina	k1gFnSc1	lavina
smete	smést	k5eAaPmIp3nS	smést
i	i	k9	i
dospělé	dospělý	k2eAgFnPc4d1	dospělá
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
trsnatá	trsnatý	k2eAgFnSc1d1	trsnatá
rostlina	rostlina	k1gFnSc1	rostlina
která	který	k3yIgFnSc1	který
nebývá	bývat	k5eNaImIp3nS	bývat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Lodyhy	lodyha	k1gFnPc4	lodyha
má	mít	k5eAaImIp3nS	mít
vystoupavé	vystoupavý	k2eAgNnSc1d1	vystoupavé
nebo	nebo	k8xC	nebo
přímé	přímý	k2eAgNnSc1d1	přímé
<g/>
,	,	kIx,	,
v	v	k7c6	v
průřezu	průřez	k1gInSc6	průřez
hranaté	hranatý	k2eAgFnPc1d1	hranatá
<g/>
,	,	kIx,	,
rozvětvené	rozvětvený	k2eAgFnPc1d1	rozvětvená
<g/>
,	,	kIx,	,
lysé	lysý	k2eAgFnPc1d1	Lysá
a	a	k8xC	a
hladké	hladký	k2eAgFnPc1d1	hladká
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
krátkého	krátký	k2eAgInSc2d1	krátký
oddenku	oddenek	k1gInSc2	oddenek
s	s	k7c7	s
tenkými	tenký	k2eAgInPc7d1	tenký
kořínky	kořínek	k1gInPc7	kořínek
i	i	k8xC	i
sterilní	sterilní	k2eAgInPc4d1	sterilní
nekořenující	kořenující	k2eNgInPc4d1	kořenující
prýty	prýt	k1gInPc4	prýt
<g/>
.	.	kIx.	.
</s>
<s>
Lodyhy	lodyha	k1gFnPc1	lodyha
mají	mít	k5eAaImIp3nP	mít
střední	střední	k2eAgNnPc4d1	střední
internodia	internodium	k1gNnPc4	internodium
1,5	[number]	k4	1,5
až	až	k8xS	až
2	[number]	k4	2
<g/>
krát	krát	k6eAd1	krát
delší	dlouhý	k2eAgInPc1d2	delší
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gInPc1	jejich
listy	list	k1gInPc1	list
které	který	k3yRgInPc4	který
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
lodyhy	lodyha	k1gFnSc2	lodyha
bývají	bývat	k5eAaImIp3nP	bývat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
mm	mm	kA	mm
a	a	k8xC	a
široké	široký	k2eAgNnSc1d1	široké
1	[number]	k4	1
až	až	k9	až
1,5	[number]	k4	1,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
průřezu	průřez	k1gInSc6	průřez
spíše	spíše	k9	spíše
tlustší	tlustý	k2eAgMnPc1d2	tlustší
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
po	po	k7c6	po
pěti	pět	k4xCc6	pět
až	až	k8xS	až
šesti	šest	k4xCc2	šest
v	v	k7c6	v
nepravých	pravý	k2eNgInPc6d1	nepravý
přeslenech	přeslen	k1gInPc6	přeslen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vynikle	vynikle	k6eAd1	vynikle
jednožilné	jednožilný	k2eAgFnPc1d1	jednožilná
<g/>
,	,	kIx,	,
tuhé	tuhý	k2eAgFnPc1d1	tuhá
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
úzce	úzko	k6eAd1	úzko
obkopinaté	obkopinatý	k2eAgFnPc1d1	obkopinatá
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
krátce	krátce	k6eAd1	krátce
zahrocené	zahrocený	k2eAgInPc1d1	zahrocený
<g/>
,	,	kIx,	,
po	po	k7c6	po
okraji	okraj	k1gInSc6	okraj
jsou	být	k5eAaImIp3nP	být
hladké	hladký	k2eAgInPc1d1	hladký
a	a	k8xC	a
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
porůstají	porůstat	k5eAaImIp3nP	porůstat
nazpět	nazpět	k6eAd1	nazpět
ohnutými	ohnutý	k2eAgInPc7d1	ohnutý
chlupy	chlup	k1gInPc7	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
při	při	k7c6	při
sušení	sušení	k1gNnSc6	sušení
tmavnou	tmavnout	k5eAaImIp3nP	tmavnout
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
zespodu	zespodu	k7c2	zespodu
lodyhy	lodyha	k1gFnSc2	lodyha
opadávají	opadávat	k5eAaImIp3nP	opadávat
již	již	k6eAd1	již
při	při	k7c6	při
kvetení	kvetení	k1gNnSc6	kvetení
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnPc1d1	vrchní
naopak	naopak	k6eAd1	naopak
mohou	moct	k5eAaImIp3nP	moct
někdy	někdy	k6eAd1	někdy
přečkat	přečkat	k5eAaPmF	přečkat
zimu	zima	k1gFnSc4	zima
i	i	k9	i
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
úžlabí	úžlabí	k1gNnSc2	úžlabí
listů	list	k1gInPc2	list
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c4	na
1,5	[number]	k4	1,5
mm	mm	kA	mm
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
stopkách	stopka	k1gFnPc6	stopka
drobné	drobný	k2eAgInPc4d1	drobný
bílé	bílý	k2eAgInPc4d1	bílý
květy	květ	k1gInPc4	květ
sestavené	sestavený	k2eAgInPc4d1	sestavený
do	do	k7c2	do
latovitého	latovitý	k2eAgNnSc2d1	latovité
květenství	květenství	k1gNnSc2	květenství
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obrysu	obrys	k1gInSc6	obrys
široce	široko	k6eAd1	široko
až	až	k9	až
protáhle	protáhle	k6eAd1	protáhle
vejčité	vejčitý	k2eAgNnSc1d1	vejčité
<g/>
.	.	kIx.	.
</s>
<s>
Oboupohlavné	oboupohlavný	k2eAgInPc1d1	oboupohlavný
květy	květ	k1gInPc1	květ
mají	mít	k5eAaImIp3nP	mít
kališní	kališní	k2eAgInPc1d1	kališní
lístky	lístek	k1gInPc1	lístek
téměř	téměř	k6eAd1	téměř
neznatelné	znatelný	k2eNgInPc1d1	neznatelný
<g/>
,	,	kIx,	,
kolovitá	kolovitý	k2eAgFnSc1d1	kolovitá
srostloplátečná	srostloplátečný	k2eAgFnSc1d1	srostloplátečná
koruna	koruna	k1gFnSc1	koruna
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
do	do	k7c2	do
3	[number]	k4	3
mm	mm	kA	mm
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc1	čtyři
bílé	bílý	k2eAgInPc1d1	bílý
lístky	lístek	k1gInPc1	lístek
které	který	k3yQgMnPc4	který
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
protáhlé	protáhlý	k2eAgFnPc1d1	protáhlá
a	a	k8xC	a
špičaté	špičatý	k2eAgFnPc1d1	špičatá
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
tyčinky	tyčinka	k1gFnPc1	tyčinka
jsou	být	k5eAaImIp3nP	být
ke	k	k7c3	k
koruně	koruna	k1gFnSc3	koruna
přirostlé	přirostlý	k2eAgFnSc2d1	přirostlá
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc2d1	spodní
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
semeník	semeník	k1gInSc4	semeník
nese	nést	k5eAaImIp3nS	nést
dvě	dva	k4xCgFnPc4	dva
volné	volný	k2eAgFnPc4d1	volná
čnělky	čnělka	k1gFnPc4	čnělka
<g/>
.	.	kIx.	.
</s>
<s>
Kvetou	kvést	k5eAaImIp3nP	kvést
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
opylovány	opylován	k2eAgFnPc1d1	opylována
jsou	být	k5eAaImIp3nP	být
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Svízel	svízel	k1gInSc1	svízel
sudetský	sudetský	k2eAgInSc1d1	sudetský
je	být	k5eAaImIp3nS	být
tetraploid	tetraploid	k1gInSc1	tetraploid
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
počet	počet	k1gInSc1	počet
chromozomů	chromozom	k1gInPc2	chromozom
je	být	k5eAaImIp3nS	být
n	n	k0	n
=	=	kIx~	=
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
dvounažka	dvounažka	k1gFnSc1	dvounažka
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
tmavě	tmavě	k6eAd1	tmavě
hnědých	hnědý	k2eAgFnPc2d1	hnědá
až	až	k8xS	až
hnědě	hnědě	k6eAd1	hnědě
černých	černý	k2eAgFnPc2d1	černá
<g/>
,	,	kIx,	,
lysých	lysý	k2eAgFnPc2d1	Lysá
<g/>
,	,	kIx,	,
protáhle	protáhle	k6eAd1	protáhle
ledvinovitých	ledvinovitý	k2eAgMnPc2d1	ledvinovitý
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
mm	mm	kA	mm
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
a	a	k8xC	a
1	[number]	k4	1
mm	mm	kA	mm
širokých	široký	k2eAgInPc2d1	široký
merikarpů	merikarp	k1gInPc2	merikarp
s	s	k7c7	s
pravidelně	pravidelně	k6eAd1	pravidelně
uspořádanými	uspořádaný	k2eAgInPc7d1	uspořádaný
polokulovitými	polokulovitý	k2eAgInPc7d1	polokulovitý
hrbolky	hrbolek	k1gInPc7	hrbolek
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
(	(	kIx(	(
<g/>
merikarpy	merikarp	k1gInPc1	merikarp
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
rozšiřována	rozšiřován	k2eAgFnSc1d1	rozšiřována
endozoochoricky	endozoochoricky	k6eAd1	endozoochoricky
nebo	nebo	k8xC	nebo
epizoochoricky	epizoochoricky	k6eAd1	epizoochoricky
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
semeny	semeno	k1gNnPc7	semeno
a	a	k8xC	a
jen	jen	k9	jen
omezeně	omezeně	k6eAd1	omezeně
rozrůstáním	rozrůstání	k1gNnSc7	rozrůstání
oddenků	oddenek	k1gInPc2	oddenek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Svízel	svízel	k1gInSc1	svízel
sudetský	sudetský	k2eAgInSc1d1	sudetský
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
druhům	druh	k1gInPc3	druh
které	který	k3yIgFnSc2	který
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
poměrně	poměrně	k6eAd1	poměrně
malých	malý	k2eAgInPc6d1	malý
areálech	areál	k1gInPc6	areál
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
vytvářet	vytvářet	k5eAaImF	vytvářet
populace	populace	k1gFnPc4	populace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
původních	původní	k2eAgInPc2d1	původní
několika	několik	k4yIc7	několik
znaky	znak	k1gInPc7	znak
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
popisovány	popisován	k2eAgMnPc4d1	popisován
jako	jako	k8xS	jako
samostatné	samostatný	k2eAgMnPc4d1	samostatný
druhy	druh	k1gMnPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
lokalit	lokalita	k1gFnPc2	lokalita
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
(	(	kIx(	(
<g/>
mohou	moct	k5eAaImIp3nP	moct
proto	proto	k8xC	proto
snadno	snadno	k6eAd1	snadno
vymizet	vymizet	k5eAaPmF	vymizet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
jejich	jejich	k3xOp3gFnPc2	jejich
vzácností	vzácnost	k1gFnPc2	vzácnost
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
je	být	k5eAaImIp3nS	být
ohrožován	ohrožovat	k5eAaImNgInS	ohrožovat
zarůstáním	zarůstání	k1gNnSc7	zarůstání
jinými	jiný	k2eAgFnPc7d1	jiná
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
přílišným	přílišný	k2eAgNnSc7d1	přílišné
sešlapem	sešlapem	k?	sešlapem
lidmi	člověk	k1gMnPc7	člověk
či	či	k8xC	či
zvířaty	zvíře	k1gNnPc7	zvíře
a	a	k8xC	a
lavinami	lavina	k1gFnPc7	lavina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
rostlin	rostlina	k1gFnPc2	rostlina
ČR	ČR	kA	ČR
zapsán	zapsán	k2eAgMnSc1d1	zapsán
jako	jako	k8xC	jako
druh	druh	k1gInSc1	druh
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
i	i	k9	i
vyhláškou	vyhláška	k1gFnSc7	vyhláška
MŽP	MŽP	kA	MŽP
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
§	§	k?	§
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
jako	jako	k9	jako
druh	druh	k1gInSc1	druh
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
(	(	kIx(	(
<g/>
VU	VU	kA	VU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
č.	č.	k?	č.
II	II	kA	II
(	(	kIx(	(
<g/>
seznamu	seznam	k1gInSc6	seznam
přísně	přísně	k6eAd1	přísně
chráněných	chráněný	k2eAgFnPc2d1	chráněná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
Bernské	bernský	k2eAgFnSc2d1	Bernská
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Foto	foto	k1gNnSc1	foto
svízele	svízel	k1gInSc2	svízel
sudetského	sudetský	k2eAgInSc2d1	sudetský
</s>
</p>
<p>
<s>
Botanický	botanický	k2eAgInSc1d1	botanický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
-	-	kIx~	-
výskyt	výskyt	k1gInSc1	výskyt
svízele	svízel	k1gInSc2	svízel
sudetského	sudetský	k2eAgInSc2d1	sudetský
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
