<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
London	London	k1gMnSc1	London
Underground	underground	k1gInSc1	underground
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podzemní	podzemní	k2eAgFnSc1d1	podzemní
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
blízkého	blízký	k2eAgNnSc2d1	blízké
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kolejový	kolejový	k2eAgInSc4d1	kolejový
systém	systém	k1gInSc4	systém
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
svému	svůj	k3xOyFgInSc3	svůj
názvu	název	k1gInSc3	název
vedou	vést	k5eAaImIp3nP	vést
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
části	část	k1gFnPc1	část
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
země	zem	k1gFnSc2	zem
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
úseky	úsek	k1gInPc1	úsek
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jménem	jméno	k1gNnSc7	jméno
Tube	tubus	k1gInSc5	tubus
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
292	[number]	k4	292
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
408	[number]	k4	408
km	km	kA	km
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2004	[number]	k4	2004
až	až	k8xS	až
2005	[number]	k4	2005
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
metro	metro	k1gNnSc1	metro
rekordní	rekordní	k2eAgNnSc1d1	rekordní
počet	počet	k1gInSc4	počet
cestujících	cestující	k1gFnPc2	cestující
976	[number]	k4	976
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
2	[number]	k4	2
670	[number]	k4	670
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
podzemní	podzemní	k2eAgFnSc1d1	podzemní
dráha	dráha	k1gFnSc1	dráha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
vlaky	vlak	k1gInPc1	vlak
zde	zde	k6eAd1	zde
projely	projet	k5eAaPmAgInP	projet
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1863	[number]	k4	1863
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
Metropolitan	metropolitan	k1gInSc4	metropolitan
Railway	Railwaa	k1gMnSc2	Railwaa
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Paddington	Paddington	k1gInSc4	Paddington
a	a	k8xC	a
Farringdon	Farringdon	k1gInSc4	Farringdon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
den	den	k1gInSc4	den
bylo	být	k5eAaImAgNnS	být
přepraveno	přepravit	k5eAaPmNgNnS	přepravit
40	[number]	k4	40
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
jezdily	jezdit	k5eAaImAgInP	jezdit
v	v	k7c6	v
odstupech	odstup	k1gInPc6	odstup
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
metro	metro	k1gNnSc1	metro
40	[number]	k4	40
miliónů	milión	k4xCgInPc2	milión
cestujících	cestující	k1gFnPc2	cestující
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
další	další	k2eAgInPc1d1	další
trasy	tras	k1gInPc1	tras
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
celá	celý	k2eAgFnSc1d1	celá
trasa	trasa	k1gFnSc1	trasa
Circle	Circle	k1gFnSc2	Circle
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInPc1d1	tehdejší
vlaky	vlak	k1gInPc1	vlak
byly	být	k5eAaImAgInP	být
poháněny	pohánět	k5eAaImNgInP	pohánět
parou	para	k1gFnSc7	para
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyžadovalo	vyžadovat	k5eAaImAgNnS	vyžadovat
účinný	účinný	k2eAgInSc4d1	účinný
systém	systém	k1gInSc4	systém
ventilace	ventilace	k1gFnSc2	ventilace
<g/>
.	.	kIx.	.
</s>
<s>
Pokrok	pokrok	k1gInSc1	pokrok
v	v	k7c6	v
elektrické	elektrický	k2eAgFnSc6d1	elektrická
trakci	trakce	k1gFnSc6	trakce
později	pozdě	k6eAd2	pozdě
dovolil	dovolit	k5eAaPmAgMnS	dovolit
umístit	umístit	k5eAaPmF	umístit
dráhu	dráha	k1gFnSc4	dráha
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
(	(	kIx(	(
<g/>
deep-level	deepevel	k1gInSc1	deep-level
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlouběji	hluboko	k6eAd2	hluboko
než	než	k8xS	než
původní	původní	k2eAgFnPc4d1	původní
podpovrchové	podpovrchový	k2eAgFnPc4d1	podpovrchová
(	(	kIx(	(
<g/>
cut-and-cover	cutndover	k1gInSc4	cut-and-cover
<g/>
)	)	kIx)	)
trasy	trasa	k1gFnSc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
takováto	takovýto	k3xDgFnSc1	takovýto
podzemní	podzemní	k2eAgFnSc1d1	podzemní
trasa	trasa	k1gFnSc1	trasa
-	-	kIx~	-
City	city	k1gNnSc1	city
&	&	k?	&
South	South	k1gMnSc1	South
London	London	k1gMnSc1	London
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
část	část	k1gFnSc1	část
trasy	trasa	k1gFnSc2	trasa
Northern	Northerna	k1gFnPc2	Northerna
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
provozovalo	provozovat	k5eAaImAgNnS	provozovat
různé	různý	k2eAgFnPc4d1	různá
trasy	trasa	k1gFnPc4	trasa
metra	metro	k1gNnSc2	metro
šest	šest	k4xCc1	šest
nezávislých	závislý	k2eNgInPc2d1	nezávislý
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
působilo	působit	k5eAaImAgNnS	působit
potíže	potíž	k1gFnPc4	potíž
cestujícím	cestující	k1gMnPc3	cestující
–	–	k?	–
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
museli	muset	k5eAaImAgMnP	muset
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
aby	aby	kYmCp3nP	aby
přešli	přejít	k5eAaPmAgMnP	přejít
mezi	mezi	k7c7	mezi
zastávkami	zastávka	k1gFnPc7	zastávka
různých	různý	k2eAgFnPc2d1	různá
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
byly	být	k5eAaImAgFnP	být
značné	značný	k2eAgFnPc1d1	značná
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
hledány	hledat	k5eAaImNgInP	hledat
zdroje	zdroj	k1gInPc1	zdroj
pro	pro	k7c4	pro
financování	financování	k1gNnSc4	financování
rozšíření	rozšíření	k1gNnSc2	rozšíření
tras	trasa	k1gFnPc2	trasa
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
na	na	k7c4	na
elektrifikaci	elektrifikace	k1gFnSc4	elektrifikace
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
používal	používat	k5eAaImAgMnS	používat
parní	parní	k2eAgInPc4d1	parní
vlaky	vlak	k1gInPc4	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
investorem	investor	k1gMnSc7	investor
byl	být	k5eAaImAgMnS	být
Američan	Američan	k1gMnSc1	Američan
Charles	Charles	k1gMnSc1	Charles
Yerkes	Yerkes	k1gMnSc1	Yerkes
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1902	[number]	k4	1902
založil	založit	k5eAaPmAgInS	založit
Underground	underground	k1gInSc1	underground
Electric	Electric	k1gMnSc1	Electric
Railways	Railwaysa	k1gFnPc2	Railwaysa
of	of	k?	of
London	London	k1gMnSc1	London
Company	Compana	k1gFnSc2	Compana
Ltd	ltd	kA	ltd
(	(	kIx(	(
<g/>
Underground	underground	k1gInSc1	underground
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
–	–	k?	–
Metropolitan	metropolitan	k1gInSc4	metropolitan
District	Districta	k1gFnPc2	Districta
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
,	,	kIx,	,
rozestavěnou	rozestavěný	k2eAgFnSc7d1	rozestavěná
Charing	Charing	k1gInSc4	Charing
Cross	Cross	k1gInSc1	Cross
<g/>
,	,	kIx,	,
Euston	Euston	k1gInSc1	Euston
&	&	k?	&
Hampstead	Hampstead	k1gInSc1	Hampstead
Railway	Railwaa	k1gMnSc2	Railwaa
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
část	část	k1gFnSc1	část
trasy	trasa	k1gFnSc2	trasa
Northern	Northerna	k1gFnPc2	Northerna
Line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Great	Great	k2eAgInSc1d1	Great
Northern	Northern	k1gInSc1	Northern
&	&	k?	&
Strand	strand	k1gInSc1	strand
Railway	Railwaa	k1gMnSc2	Railwaa
<g/>
,	,	kIx,	,
Brompton	Brompton	k1gInSc1	Brompton
&	&	k?	&
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
Circus	Circus	k1gMnSc1	Circus
Railway	Railwaa	k1gMnSc2	Railwaa
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
základ	základ	k1gInSc1	základ
trasy	trasa	k1gFnSc2	trasa
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
Line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
a	a	k8xC	a
Baker	Baker	k1gMnSc1	Baker
Street	Street	k1gMnSc1	Street
&	&	k?	&
Waterloo	Waterloo	k1gNnSc2	Waterloo
Railway	Railwaa	k1gMnSc2	Railwaa
(	(	kIx(	(
<g/>
trasa	trasa	k1gFnSc1	trasa
Bakerloo	Bakerloo	k6eAd1	Bakerloo
Line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
také	také	k9	také
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
mnoho	mnoho	k4c4	mnoho
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
London	London	k1gMnSc1	London
General	General	k1gMnSc1	General
Omnibus	omnibus	k1gInSc4	omnibus
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
společnost	společnost	k1gFnSc1	společnost
London	London	k1gMnSc1	London
Passenger	Passenger	k1gMnSc1	Passenger
Transport	transporta	k1gFnPc2	transporta
Board	Board	k1gMnSc1	Board
(	(	kIx(	(
<g/>
LPTB	LPTB	kA	LPTB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
správy	správa	k1gFnSc2	správa
byly	být	k5eAaImAgFnP	být
začleněny	začleněn	k2eAgFnPc1d1	začleněna
Underground	underground	k1gInSc1	underground
Group	Group	k1gInSc4	Group
<g/>
,	,	kIx,	,
Metropolitan	metropolitan	k1gInSc4	metropolitan
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnSc2d1	městská
a	a	k8xC	a
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
autobusové	autobusový	k2eAgFnSc2d1	autobusová
a	a	k8xC	a
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
rozsah	rozsah	k1gInSc4	rozsah
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
současný	současný	k2eAgInSc1d1	současný
Transport	transport	k1gInSc1	transport
for	forum	k1gNnPc2	forum
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
TfL	TfL	k1gMnSc1	TfL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
plán	plán	k1gInSc4	plán
rozvoje	rozvoj	k1gInSc2	rozvoj
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
pro	pro	k7c4	pro
léta	léto	k1gNnPc4	léto
1935	[number]	k4	1935
až	až	k8xS	až
1940	[number]	k4	1940
–	–	k?	–
plán	plán	k1gInSc1	plán
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
návrh	návrh	k1gInSc1	návrh
rozšíření	rozšíření	k1gNnSc2	rozšíření
některých	některý	k3yIgFnPc2	některý
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
převzetí	převzetí	k1gNnSc4	převzetí
některých	některý	k3yIgMnPc2	některý
provozovatelů	provozovatel	k1gMnPc2	provozovatel
dopravy	doprava	k1gFnSc2	doprava
pod	pod	k7c4	pod
LPTB	LPTB	kA	LPTB
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
rozsah	rozsah	k1gInSc1	rozsah
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
trasa	trasa	k1gFnSc1	trasa
Victoria	Victorium	k1gNnSc2	Victorium
Line	linout	k5eAaImIp3nS	linout
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
severovýchod-jihozápad	severovýchodihozápad	k1gInSc4	severovýchod-jihozápad
přes	přes	k7c4	přes
centrum	centrum	k1gNnSc4	centrum
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
převzala	převzít	k5eAaPmAgFnS	převzít
většinu	většina	k1gFnSc4	většina
nárůstu	nárůst	k1gInSc2	nárůst
poválečné	poválečný	k2eAgFnSc2d1	poválečná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
Line	linout	k5eAaImIp3nS	linout
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
Heathrow	Heathrow	k1gFnSc2	Heathrow
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
a	a	k8xC	a
Jubilee	Jubilee	k1gInSc1	Jubilee
Line	linout	k5eAaImIp3nS	linout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
londýnské	londýnský	k2eAgNnSc1d1	Londýnské
metro	metro	k1gNnSc1	metro
provozováno	provozovat	k5eAaImNgNnS	provozovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
veřejnoprávně	veřejnoprávně	k6eAd1	veřejnoprávně
soukromého	soukromý	k2eAgNnSc2d1	soukromé
obchodního	obchodní	k2eAgNnSc2d1	obchodní
sdružení	sdružení	k1gNnSc2	sdružení
(	(	kIx(	(
<g/>
Public-Private	Public-Privat	k1gInSc5	Public-Privat
Partnership	Partnership	k1gMnSc1	Partnership
-	-	kIx~	-
PPP	PPP	kA	PPP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
celá	celý	k2eAgFnSc1d1	celá
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
je	být	k5eAaImIp3nS	být
udržována	udržovat	k5eAaImNgFnS	udržovat
soukromými	soukromý	k2eAgFnPc7d1	soukromá
firmami	firma	k1gFnPc7	firma
s	s	k7c7	s
30	[number]	k4	30
<g/>
letými	letý	k2eAgInPc7d1	letý
kontrakty	kontrakt	k1gInPc7	kontrakt
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
TfL	TfL	k1gFnSc1	TfL
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
JNP	JNP	kA	JNP
(	(	kIx(	(
<g/>
trasy	trasa	k1gFnPc1	trasa
Jubilee	Jubilee	k1gNnPc2	Jubilee
<g/>
,	,	kIx,	,
Northern	Northerna	k1gFnPc2	Northerna
a	a	k8xC	a
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
–	–	k?	–
udržuje	udržovat	k5eAaImIp3nS	udržovat
firma	firma	k1gFnSc1	firma
Tube	tubus	k1gInSc5	tubus
Lines	Linesa	k1gFnPc2	Linesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BCV	BCV	kA	BCV
(	(	kIx(	(
<g/>
trasy	trasa	k1gFnPc4	trasa
Bakerloo	Bakerloo	k6eAd1	Bakerloo
<g/>
,	,	kIx,	,
Central	Central	k1gFnPc2	Central
a	a	k8xC	a
Victoria	Victorium	k1gNnSc2	Victorium
-	-	kIx~	-
udržuje	udržovat	k5eAaImIp3nS	udržovat
firma	firma	k1gFnSc1	firma
Metronet	Metroneta	k1gFnPc2	Metroneta
<g/>
)	)	kIx)	)
a	a	k8xC	a
SSL	SSL	kA	SSL
(	(	kIx(	(
<g/>
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
trasy	trasa	k1gFnSc2	trasa
District	District	k1gInSc1	District
<g/>
,	,	kIx,	,
Metropolitan	metropolitan	k1gInSc1	metropolitan
<g/>
,	,	kIx,	,
East	East	k1gMnSc1	East
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Circle	Circle	k1gFnSc1	Circle
a	a	k8xC	a
Hammersmith	Hammersmith	k1gInSc1	Hammersmith
&	&	k?	&
City	city	k1gNnSc1	city
-	-	kIx~	-
udržuje	udržovat	k5eAaImIp3nS	udržovat
firma	firma	k1gFnSc1	firma
Metronet	Metroneta	k1gFnPc2	Metroneta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
články	článek	k1gInPc1	článek
o	o	k7c6	o
systému	systém	k1gInSc6	systém
jízdenek	jízdenka	k1gFnPc2	jízdenka
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
městské	městský	k2eAgFnSc6d1	městská
dopravě	doprava	k1gFnSc6	doprava
–	–	k?	–
Travelcard	Travelcarda	k1gFnPc2	Travelcarda
a	a	k8xC	a
Oyster	Oystra	k1gFnPc2	Oystra
card	card	k6eAd1	card
V	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
metru	metro	k1gNnSc6	metro
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
rozdělení	rozdělení	k1gNnSc1	rozdělení
stanic	stanice	k1gFnPc2	stanice
do	do	k7c2	do
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
v	v	k7c6	v
systému	systém	k1gInSc6	systém
TfL	TfL	k1gMnSc1	TfL
Travelcard	Travelcard	k1gMnSc1	Travelcard
<g/>
.	.	kIx.	.
</s>
<s>
Zóna	zóna	k1gFnSc1	zóna
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
zóna	zóna	k1gFnSc1	zóna
s	s	k7c7	s
hranicemi	hranice	k1gFnPc7	hranice
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
zhruba	zhruba	k6eAd1	zhruba
trase	trasa	k1gFnSc6	trasa
Circle	Circla	k1gFnSc6	Circla
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Zóna	zóna	k1gFnSc1	zóna
6	[number]	k4	6
je	být	k5eAaImIp3nS	být
vnější	vnější	k2eAgFnSc1d1	vnější
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
stanice	stanice	k1gFnSc1	stanice
letiště	letiště	k1gNnSc2	letiště
Heathrow	Heathrow	k1gFnSc2	Heathrow
<g/>
.	.	kIx.	.
</s>
<s>
Zóny	zóna	k1gFnPc1	zóna
1	[number]	k4	1
–	–	k?	–
6	[number]	k4	6
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
několik	několik	k4yIc1	několik
zastávek	zastávka	k1gFnPc2	zastávka
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
stanice	stanice	k1gFnPc4	stanice
mimo	mimo	k6eAd1	mimo
Velkého	velký	k2eAgInSc2d1	velký
Londýna	Londýn	k1gInSc2	Londýn
jsou	být	k5eAaImIp3nP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
zóny	zóna	k1gFnPc1	zóna
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
D.	D.	kA	D.
Zóna	zóna	k1gFnSc1	zóna
D	D	kA	D
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejvzdálenější	vzdálený	k2eAgFnSc1d3	nejvzdálenější
stanice	stanice	k1gFnSc1	stanice
jako	jako	k8xS	jako
například	například	k6eAd1	například
Amersham	Amersham	k1gInSc1	Amersham
a	a	k8xC	a
Chesham	Chesham	k1gInSc1	Chesham
<g/>
.	.	kIx.	.
</s>
<s>
Zóny	zóna	k1gFnPc1	zóna
označené	označený	k2eAgFnPc1d1	označená
písmenem	písmeno	k1gNnSc7	písmeno
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Metropolitan	metropolitan	k1gInSc4	metropolitan
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	čí	k3xOyRgInSc7	čí
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
zón	zóna	k1gFnPc2	zóna
probíhá	probíhat	k5eAaImIp3nS	probíhat
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
dražší	drahý	k2eAgMnSc1d2	dražší
je	být	k5eAaImIp3nS	být
jízdné	jízdné	k1gNnSc1	jízdné
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdné	k1gNnSc1	jízdné
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
dražší	drahý	k2eAgNnSc1d2	dražší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jízdné	jízdné	k1gNnSc1	jízdné
ve	v	k7c6	v
vnějších	vnější	k2eAgFnPc6d1	vnější
zónách	zóna	k1gFnPc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
známých	známý	k2eAgFnPc2d1	známá
turistických	turistický	k2eAgFnPc2d1	turistická
pozoruhodností	pozoruhodnost	k1gFnPc2	pozoruhodnost
je	být	k5eAaImIp3nS	být
dosažitelná	dosažitelný	k2eAgFnSc1d1	dosažitelná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zóny	zóna	k1gFnSc2	zóna
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jízdenky	jízdenka	k1gFnPc4	jízdenka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zakoupit	zakoupit	k5eAaPmF	zakoupit
v	v	k7c6	v
úředních	úřední	k2eAgFnPc6d1	úřední
hodinách	hodina	k1gFnPc6	hodina
v	v	k7c6	v
kancelářích	kancelář	k1gFnPc6	kancelář
TfL	TfL	k1gFnPc6	TfL
a	a	k8xC	a
v	v	k7c6	v
automatech	automat	k1gInPc6	automat
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
vydávaných	vydávaný	k2eAgFnPc2d1	vydávaná
jízdenek	jízdenka	k1gFnPc2	jízdenka
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Podrobné	podrobný	k2eAgFnSc2d1	podrobná
aktuální	aktuální	k2eAgFnSc2d1	aktuální
informace	informace	k1gFnSc2	informace
WWW	WWW	kA	WWW
stránky	stránka	k1gFnSc2	stránka
Transport	transporta	k1gFnPc2	transporta
for	forum	k1gNnPc2	forum
London	London	k1gMnSc1	London
.	.	kIx.	.
</s>
<s>
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
metro	metro	k1gNnSc1	metro
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
292	[number]	k4	292
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
DLR	DLR	kA	DLR
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
zrušených	zrušený	k2eAgFnPc2d1	zrušená
nebo	nebo	k8xC	nebo
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trasy	tras	k1gInPc4	tras
metra	metro	k1gNnSc2	metro
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
–	–	k?	–
podpovrchové	podpovrchový	k2eAgFnSc6d1	podpovrchová
a	a	k8xC	a
podzemní	podzemní	k2eAgFnSc6d1	podzemní
<g/>
.	.	kIx.	.
</s>
<s>
Jízdní	jízdní	k2eAgFnPc1d1	jízdní
dráhy	dráha	k1gFnPc1	dráha
podpovrchových	podpovrchový	k2eAgFnPc2d1	podpovrchová
tras	trasa	k1gFnPc2	trasa
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
cca	cca	kA	cca
5	[number]	k4	5
m	m	kA	m
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
jezdí	jezdit	k5eAaImIp3nP	jezdit
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
profil	profil	k1gInSc4	profil
jako	jako	k8xC	jako
vlakové	vlakový	k2eAgFnSc2d1	vlaková
soupravy	souprava	k1gFnSc2	souprava
na	na	k7c6	na
běžných	běžný	k2eAgFnPc6d1	běžná
železnicích	železnice	k1gFnPc6	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgFnPc1d1	podzemní
trasy	trasa	k1gFnPc1	trasa
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
cca	cca	kA	cca
20	[number]	k4	20
m	m	kA	m
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
vlakové	vlakový	k2eAgFnPc1d1	vlaková
soupravy	souprava	k1gFnPc1	souprava
jezdí	jezdit	k5eAaImIp3nP	jezdit
v	v	k7c6	v
oddělených	oddělený	k2eAgInPc6d1	oddělený
tunelech	tunel	k1gInPc6	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Tunely	tunel	k1gInPc1	tunel
těchto	tento	k3xDgFnPc2	tento
tras	trasa	k1gFnPc2	trasa
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc1	průměr
3,56	[number]	k4	3,56
m	m	kA	m
takže	takže	k8xS	takže
rozměry	rozměr	k1gInPc1	rozměr
vlakových	vlakový	k2eAgFnPc2d1	vlaková
souprav	souprava	k1gFnPc2	souprava
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
podpovrchových	podpovrchový	k2eAgFnPc2d1	podpovrchová
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Trasy	trasa	k1gFnPc1	trasa
obou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
mimo	mimo	k7c4	mimo
centra	centrum	k1gNnPc4	centrum
města	město	k1gNnSc2	město
vedou	vést	k5eAaImIp3nP	vést
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
trasy	trasa	k1gFnSc2	trasa
Victoria	Victorium	k1gNnSc2	Victorium
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
v	v	k7c6	v
podzemních	podzemní	k2eAgInPc6d1	podzemní
tunelech	tunel	k1gInPc6	tunel
a	a	k8xC	a
Waterloo	Waterloo	k1gNnSc2	Waterloo
&	&	k?	&
City	city	k1gNnSc1	city
Line	linout	k5eAaImIp3nS	linout
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
krátká	krátký	k2eAgFnSc1d1	krátká
(	(	kIx(	(
<g/>
2	[number]	k4	2
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
délky	délka	k1gFnSc2	délka
metra	metro	k1gNnSc2	metro
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podzemních	podzemní	k2eAgInPc6d1	podzemní
tunelech	tunel	k1gInPc6	tunel
pouze	pouze	k6eAd1	pouze
45	[number]	k4	45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
napojeno	napojen	k2eAgNnSc1d1	napojeno
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
vlakové	vlakový	k2eAgNnSc4d1	vlakové
spojení	spojení	k1gNnSc4	spojení
Eurostar	Eurostara	k1gFnPc2	Eurostara
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Crossa	k1gFnPc2	Crossa
St.	st.	kA	st.
Pancras	Pancras	k1gInSc1	Pancras
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
Line	linout	k5eAaImIp3nS	linout
spojuje	spojovat	k5eAaImIp3nS	spojovat
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
s	s	k7c7	s
letištěm	letiště	k1gNnSc7	letiště
Heathrow	Heathrow	k1gFnSc2	Heathrow
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
dopravy	doprava	k1gFnSc2	doprava
mnohdy	mnohdy	k6eAd1	mnohdy
přeplněný	přeplněný	k2eAgMnSc1d1	přeplněný
a	a	k8xC	a
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
(	(	kIx(	(
<g/>
cca	cca	kA	cca
52	[number]	k4	52
minut	minuta	k1gFnPc2	minuta
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Green	Green	k2eAgInSc1d1	Green
Park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
než	než	k8xS	než
Heathrow	Heathrow	k1gFnSc1	Heathrow
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
letištěm	letiště	k1gNnSc7	letiště
Stansted	Stansted	k1gInSc1	Stansted
<g/>
,	,	kIx,	,
zajišťované	zajišťovaný	k2eAgFnSc2d1	zajišťovaná
Stansted	Stansted	k1gInSc4	Stansted
Expressem	express	k1gInSc7	express
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosažitelné	dosažitelný	k2eAgNnSc1d1	dosažitelné
ze	z	k7c2	z
stanic	stanice	k1gFnPc2	stanice
Liverpool	Liverpool	k1gInSc1	Liverpool
Street	Street	k1gMnSc1	Street
(	(	kIx(	(
<g/>
trasy	trasa	k1gFnPc1	trasa
Central	Central	k1gFnPc1	Central
<g/>
,	,	kIx,	,
Circle	Circle	k1gFnPc1	Circle
<g/>
,	,	kIx,	,
Hammersmith	Hammersmith	k1gInSc1	Hammersmith
&	&	k?	&
City	city	k1gNnSc1	city
a	a	k8xC	a
Metropolitan	metropolitan	k1gInSc1	metropolitan
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tottenham	Tottenham	k1gInSc1	Tottenham
Hale	hala	k1gFnSc3	hala
(	(	kIx(	(
<g/>
trasa	trasa	k1gFnSc1	trasa
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
spojení	spojení	k1gNnSc1	spojení
se	se	k3xPyFc4	se
Stansted	Stansted	k1gInSc4	Stansted
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Stratford	Stratforda	k1gFnPc2	Stratforda
dosažitelné	dosažitelný	k2eAgFnPc1d1	dosažitelná
metrem	metro	k1gNnSc7	metro
(	(	kIx(	(
<g/>
trasy	trasa	k1gFnSc2	trasa
Central	Central	k1gFnSc2	Central
a	a	k8xC	a
Jubilee	Jubile	k1gFnSc2	Jubile
<g/>
)	)	kIx)	)
a	a	k8xC	a
DLR	DLR	kA	DLR
bylo	být	k5eAaImAgNnS	být
zprovozněno	zprovozněn	k2eAgNnSc1d1	zprovozněno
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
letištěm	letiště	k1gNnSc7	letiště
Gatwick	Gatwicka	k1gFnPc2	Gatwicka
<g/>
,	,	kIx,	,
zajišťované	zajišťovaný	k2eAgFnSc2d1	zajišťovaná
Gatwick	Gatwick	k1gInSc4	Gatwick
Expressem	express	k1gInSc7	express
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosažitelné	dosažitelný	k2eAgNnSc1d1	dosažitelné
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Victoria	Victorium	k1gNnSc2	Victorium
(	(	kIx(	(
<g/>
trasy	trasa	k1gFnPc1	trasa
District	Districtum	k1gNnPc2	Districtum
<g/>
,	,	kIx,	,
Circle	Circle	k1gNnSc2	Circle
a	a	k8xC	a
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
trasy	trasa	k1gFnPc4	trasa
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
barevné	barevný	k2eAgNnSc4d1	barevné
označení	označení	k1gNnSc4	označení
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
otevření	otevření	k1gNnSc4	otevření
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
trasy	trasa	k1gFnSc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trasách	trasa	k1gFnPc6	trasa
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
historie	historie	k1gFnSc2	historie
používaly	používat	k5eAaImAgInP	používat
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
poháněny	pohánět	k5eAaImNgFnP	pohánět
parou	para	k1gFnSc7	para
(	(	kIx(	(
<g/>
zrušené	zrušený	k2eAgInPc1d1	zrušený
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podpovrchových	podpovrchový	k2eAgFnPc6d1	podpovrchová
trasách	trasa	k1gFnPc6	trasa
(	(	kIx(	(
<g/>
Sub-Surface	Sub-Surface	k1gFnSc1	Sub-Surface
<g/>
)	)	kIx)	)
jezdí	jezdit	k5eAaImIp3nP	jezdit
soupravy	souprava	k1gFnPc1	souprava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
víceméně	víceméně	k9	víceméně
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
rozměrům	rozměr	k1gInPc3	rozměr
vlaků	vlak	k1gInPc2	vlak
provozovaných	provozovaný	k2eAgInPc2d1	provozovaný
na	na	k7c6	na
běžných	běžný	k2eAgFnPc6d1	běžná
železničních	železniční	k2eAgFnPc6d1	železniční
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podzemních	podzemní	k2eAgFnPc6d1	podzemní
trasách	trasa	k1gFnPc6	trasa
(	(	kIx(	(
<g/>
Tube	tubus	k1gInSc5	tubus
<g/>
)	)	kIx)	)
jezdí	jezdit	k5eAaImIp3nP	jezdit
jednotky	jednotka	k1gFnPc4	jednotka
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
menším	malý	k2eAgInSc7d2	menší
profilem	profil	k1gInSc7	profil
<g/>
,	,	kIx,	,
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
přizpůsobené	přizpůsobený	k2eAgNnSc1d1	přizpůsobené
rozměrům	rozměr	k1gInPc3	rozměr
tunelů	tunel	k1gInPc2	tunel
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
přehled	přehled	k1gInSc1	přehled
vozového	vozový	k2eAgInSc2d1	vozový
parku	park	k1gInSc2	park
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
Teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.tfl.gov.uk/tube/	[url]	k?	http://www.tfl.gov.uk/tube/
-	-	kIx~	-
WWW	WWW	kA	WWW
stránky	stránka	k1gFnSc2	stránka
Transport	transporta	k1gFnPc2	transporta
for	forum	k1gNnPc2	forum
London	London	k1gMnSc1	London
–	–	k?	–
metro	metro	k1gNnSc4	metro
http://www.journeyplanner.org/	[url]	k?	http://www.journeyplanner.org/
-	-	kIx~	-
Plánovač	plánovač	k1gMnSc1	plánovač
jízd	jízda	k1gFnPc2	jízda
TfL	TfL	k1gMnSc1	TfL
http://www.tfl.gov.uk/tube/maps/	[url]	k?	http://www.tfl.gov.uk/tube/maps/
-	-	kIx~	-
Mapy	mapa	k1gFnPc1	mapa
metra	metro	k1gNnSc2	metro
TfL	TfL	k1gFnPc2	TfL
http://tube.tfl.gov.uk/content/faq/tourism/introduction.asp	[url]	k1gMnSc1	http://tube.tfl.gov.uk/content/faq/tourism/introduction.asp
-	-	kIx~	-
Informace	informace	k1gFnSc1	informace
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
http://www.ltmuseum.co.uk/	[url]	k?	http://www.ltmuseum.co.uk/
-	-	kIx~	-
Londýnské	londýnský	k2eAgNnSc1d1	Londýnské
dopravní	dopravní	k2eAgNnSc1d1	dopravní
muzeum	muzeum	k1gNnSc1	muzeum
http://www.goingunderground.net/maps.html	[url]	k1gInSc1	http://www.goingunderground.net/maps.html
-	-	kIx~	-
Historie	historie	k1gFnSc1	historie
mapy	mapa	k1gFnSc2	mapa
metra	metro	k1gNnSc2	metro
http://www.londontransport.info	[url]	k1gNnSc1	http://www.londontransport.info
-	-	kIx~	-
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
(	(	kIx(	(
<g/>
metro	metro	k1gNnSc1	metro
<g/>
)	)	kIx)	)
</s>
