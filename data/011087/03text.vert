<p>
<s>
Kófukudži	Kófukudzat	k5eAaPmIp1nS	Kófukudzat
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
chrám	chrám	k1gInSc1	chrám
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nara	Nar	k1gInSc2	Nar
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Nara	Narum	k1gNnSc2	Narum
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
chrámem	chrám	k1gInSc7	chrám
buddhistické	buddhistický	k2eAgFnSc2d1	buddhistická
školy	škola	k1gFnSc2	škola
Hossó	Hossó	k1gFnSc2	Hossó
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
počítán	počítat	k5eAaImNgMnS	počítat
mezi	mezi	k7c4	mezi
sedm	sedm	k4xCc4	sedm
velkých	velký	k2eAgInPc2d1	velký
narských	narský	k2eAgInPc2d1	narský
chrámů	chrám	k1gInPc2	chrám
(	(	kIx(	(
<g/>
南	南	k?	南
<g/>
,	,	kIx,	,
Nanto	Nanto	k1gNnSc1	Nanto
šičidaidži	šičidaidzat	k5eAaPmIp1nS	šičidaidzat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
objektem	objekt	k1gInSc7	objekt
uctívání	uctívání	k1gNnSc2	uctívání
je	být	k5eAaImIp3nS	být
Šaka	Šaka	k1gFnSc1	Šaka
Njorai	Njora	k1gFnSc2	Njora
(	(	kIx(	(
<g/>
釈	釈	k?	釈
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgMnSc1d1	historický
Buddha	Buddha	k1gMnSc1	Buddha
Šákjamuni	Šákjamuň	k1gMnSc3	Šákjamuň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
prvopočátky	prvopočátek	k1gInPc1	prvopočátek
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
669	[number]	k4	669
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kagami	Kaga	k1gFnPc7	Kaga
no	no	k9	no
Ókimi	Óki	k1gFnPc7	Óki
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Fudžiwara	Fudžiwar	k1gMnSc4	Fudžiwar
no	no	k9	no
Kamatariho	Kamatari	k1gMnSc4	Kamatari
<g/>
,	,	kIx,	,
založila	založit	k5eAaPmAgFnS	založit
na	na	k7c6	na
rodových	rodový	k2eAgInPc6d1	rodový
pozemcích	pozemek	k1gInPc6	pozemek
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
kjótská	kjótský	k2eAgFnSc1d1	kjótská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Jamašina	Jamašin	k2eAgMnSc2d1	Jamašin
buddhistický	buddhistický	k2eAgInSc4d1	buddhistický
chrám	chrám	k1gInSc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
klanovým	klanový	k2eAgInSc7d1	klanový
chrámem	chrám	k1gInSc7	chrám
(	(	kIx(	(
<g/>
氏	氏	k?	氏
<g/>
,	,	kIx,	,
udžidera	udžidera	k1gFnSc1	udžidera
<g/>
)	)	kIx)	)
Fudžiwarů	Fudžiwar	k1gInPc2	Fudžiwar
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Jamašina-dera	Jamašinaera	k1gFnSc1	Jamašina-dera
(	(	kIx(	(
<g/>
山	山	k?	山
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
byl	být	k5eAaImAgInS	být
přestěhován	přestěhovat	k5eAaPmNgInS	přestěhovat
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Fudžiwary	Fudžiwara	k1gFnSc2	Fudžiwara
a	a	k8xC	a
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c4	na
Umajasaka-dera	Umajasakaero	k1gNnPc4	Umajasaka-dero
(	(	kIx(	(
<g/>
厩	厩	k?	厩
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
710	[number]	k4	710
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Heidžó-kjó	Heidžójó	k1gFnSc1	Heidžó-kjó
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Nara	Nara	k1gFnSc1	Nara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dostal	dostat	k5eAaPmAgMnS	dostat
své	svůj	k3xOyFgNnSc4	svůj
definitivní	definitivní	k2eAgNnSc4d1	definitivní
jméno	jméno	k1gNnSc4	jméno
Kófukudži	Kófukudž	k1gFnSc6	Kófukudž
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Naře	Naře	k1gFnSc6	Naře
byl	být	k5eAaImAgInS	být
chrámový	chrámový	k2eAgInSc1d1	chrámový
komplex	komplex	k1gInSc1	komplex
budován	budovat	k5eAaImNgInS	budovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
710	[number]	k4	710
do	do	k7c2	do
roku	rok	k1gInSc2	rok
730	[number]	k4	730
<g/>
.	.	kIx.	.
</s>
<s>
Pětiposchoďovou	pětiposchoďový	k2eAgFnSc4d1	pětiposchoďová
pagodu	pagoda	k1gFnSc4	pagoda
chrámu	chrámat	k5eAaImIp1nS	chrámat
nechala	nechat	k5eAaPmAgFnS	nechat
postavit	postavit	k5eAaPmF	postavit
císařovna	císařovna	k1gFnSc1	císařovna
Kómjó	Kómjó	k1gFnSc2	Kómjó
v	v	k7c6	v
roce	rok	k1gInSc6	rok
725	[number]	k4	725
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1426	[number]	k4	1426
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
dnes	dnes	k6eAd1	dnes
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
pagodu	pagoda	k1gFnSc4	pagoda
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
50,1	[number]	k4	50,1
m.	m.	k?	m.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1143	[number]	k4	1143
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
manželky	manželka	k1gFnSc2	manželka
císaře	císař	k1gMnSc2	císař
Sutoku	Sutok	k1gInSc2	Sutok
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
tříposchoďová	tříposchoďový	k2eAgFnSc1d1	tříposchoďová
pagoda	pagoda	k1gFnSc1	pagoda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
současná	současný	k2eAgFnSc1d1	současná
podoba	podoba	k1gFnSc1	podoba
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
období	období	k1gNnSc2	období
Kamakura	Kamakura	k1gFnSc1	Kamakura
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
Kófukudži	Kófukudž	k1gFnSc3	Kófukudž
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgFnPc7d1	další
památkami	památka	k1gFnPc7	památka
v	v	k7c6	v
Naře	Naře	k1gFnSc6	Naře
<g/>
,	,	kIx,	,
zapsán	zapsán	k2eAgMnSc1d1	zapsán
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
pod	pod	k7c7	pod
společným	společný	k2eAgNnSc7d1	společné
označením	označení	k1gNnSc7	označení
Památky	památka	k1gFnSc2	památka
na	na	k7c4	na
starobylou	starobylý	k2eAgFnSc4d1	starobylá
Naru	Nara	k1gFnSc4	Nara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kófukudži	Kófukudž	k1gFnSc6	Kófukudž
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
