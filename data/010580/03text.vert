<p>
<s>
Vrchní	vrchní	k1gMnSc1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
je	být	k5eAaImIp3nS	být
československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Pražského	pražský	k2eAgMnSc4d1	pražský
knihkupce	knihkupec	k1gMnSc4	knihkupec
Dalibora	Dalibor	k1gMnSc4	Dalibor
Vránu	Vrána	k1gMnSc4	Vrána
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Abrhám	Abrha	k1gFnPc3	Abrha
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
v	v	k7c6	v
pohostinství	pohostinství	k1gNnSc6	pohostinství
spletou	splést	k5eAaPmIp3nP	splést
s	s	k7c7	s
vrchním	vrchní	k2eAgMnSc7d1	vrchní
číšníkem	číšník	k1gMnSc7	číšník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
své	svůj	k3xOyFgFnSc2	svůj
neveselé	veselý	k2eNgFnSc2d1	neveselá
finanční	finanční	k2eAgFnSc2d1	finanční
situace	situace	k1gFnSc2	situace
(	(	kIx(	(
<g/>
postižené	postižený	k2eAgFnSc2d1	postižená
zejména	zejména	k9	zejména
nutností	nutnost	k1gFnSc7	nutnost
platit	platit	k5eAaImF	platit
alimenty	alimenty	k1gInPc4	alimenty
svým	svůj	k3xOyFgFnPc3	svůj
minulým	minulý	k2eAgFnPc3d1	minulá
partnerkám	partnerka	k1gFnPc3	partnerka
<g/>
)	)	kIx)	)
bere	brát	k5eAaImIp3nS	brát
Dalibor	Dalibor	k1gMnSc1	Dalibor
příležitost	příležitost	k1gFnSc4	příležitost
pevně	pevně	k6eAd1	pevně
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
falešný	falešný	k2eAgInSc4d1	falešný
vrchní	vrchní	k1gFnSc7	vrchní
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
si	se	k3xPyFc3	se
obleče	obléct	k5eAaPmIp3nS	obléct
smoking	smoking	k1gInSc4	smoking
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
lokálu	lokál	k1gInSc2	lokál
<g/>
,	,	kIx,	,
zkasíruje	zkasírovat	k5eAaPmIp3nS	zkasírovat
několik	několik	k4yIc4	několik
hostů	host	k1gMnPc2	host
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
vytratí	vytratit	k5eAaPmIp3nP	vytratit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
doma	doma	k6eAd1	doma
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
náhle	náhle	k6eAd1	náhle
zvýšené	zvýšený	k2eAgInPc4d1	zvýšený
příjmy	příjem	k1gInPc4	příjem
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
za	za	k7c4	za
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
barového	barový	k2eAgMnSc4d1	barový
houslistu	houslista	k1gMnSc4	houslista
<g/>
.	.	kIx.	.
</s>
<s>
Nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
daří	dařit	k5eAaImIp3nS	dařit
<g/>
,	,	kIx,	,
i	i	k8xC	i
díky	díky	k7c3	díky
střídání	střídání	k1gNnSc3	střídání
lokalit	lokalita	k1gFnPc2	lokalita
a	a	k8xC	a
několika	několik	k4yIc2	několik
namaskování	namaskování	k1gNnPc2	namaskování
<g/>
.	.	kIx.	.
</s>
<s>
Pohár	pohár	k1gInSc1	pohár
začne	začít	k5eAaPmIp3nS	začít
přetékat	přetékat	k5eAaImF	přetékat
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
skuteční	skutečný	k2eAgMnPc1d1	skutečný
vrchní	vrchní	k1gMnPc1	vrchní
uspořádají	uspořádat	k5eAaPmIp3nP	uspořádat
hon	hon	k1gInSc4	hon
-	-	kIx~	-
falešný	falešný	k2eAgInSc4d1	falešný
vrchní	vrchní	k1gFnSc4	vrchní
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
uteče	utéct	k5eAaPmIp3nS	utéct
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
stihne	stihnout	k5eAaPmIp3nS	stihnout
zkasírovat	zkasírovat	k5eAaPmF	zkasírovat
další	další	k2eAgFnSc4d1	další
lázeňskou	lázeňský	k2eAgFnSc4d1	lázeňská
restauraci	restaurace	k1gFnSc4	restaurace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Dalibor	Dalibor	k1gMnSc1	Dalibor
doplatí	doplatit	k5eAaPmIp3nS	doplatit
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
vloudilého	vloudilý	k2eAgMnSc4d1	vloudilý
souseda	soused	k1gMnSc4	soused
pana	pan	k1gMnSc4	pan
Pařízka	Pařízek	k1gMnSc4	Pařízek
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
pozná	poznat	k5eAaPmIp3nS	poznat
podobu	podoba	k1gFnSc4	podoba
s	s	k7c7	s
falešným	falešný	k2eAgMnSc7d1	falešný
vrchním	vrchní	k2eAgMnSc7d1	vrchní
<g/>
.	.	kIx.	.
</s>
<s>
Vrána	Vrána	k1gMnSc1	Vrána
je	být	k5eAaImIp3nS	být
Bezpečností	bezpečnost	k1gFnPc2	bezpečnost
dopaden	dopaden	k2eAgMnSc1d1	dopaden
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
knihkupectví	knihkupectví	k1gNnSc6	knihkupectví
a	a	k8xC	a
usvědčen	usvědčen	k2eAgMnSc1d1	usvědčen
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
podpisu	podpis	k1gInSc2	podpis
na	na	k7c6	na
stvrzenkách	stvrzenka	k1gFnPc6	stvrzenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symboly	symbol	k1gInPc1	symbol
filmu	film	k1gInSc2	film
jsou	být	k5eAaImIp3nP	být
tříkolové	tříkolový	k2eAgNnSc4d1	tříkolové
vozítko	vozítko	k1gNnSc4	vozítko
Velorex	Velorex	k1gInSc1	Velorex
a	a	k8xC	a
píseň	píseň	k1gFnSc1	píseň
Severní	severní	k2eAgFnSc1d1	severní
vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
J.	J.	kA	J.
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
Z.	Z.	kA	Z.
Svěrák	svěrák	k1gInSc1	svěrák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
scénám	scéna	k1gFnPc3	scéna
patří	patřit	k5eAaImIp3nS	patřit
testování	testování	k1gNnSc1	testování
funkcí	funkce	k1gFnPc2	funkce
Pařízkovic	Pařízkovice	k1gFnPc2	Pařízkovice
nové	nový	k2eAgFnSc2d1	nová
škodovky	škodovka	k1gFnSc2	škodovka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
stěrače	stěrač	k1gInPc1	stěrač
stírají	stírat	k5eAaImIp3nP	stírat
<g/>
,	,	kIx,	,
klakson	klakson	k1gInSc1	klakson
troubí	troubit	k5eAaImIp3nS	troubit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vránovo	Vránův	k2eAgNnSc4d1	Vránův
předstírání	předstírání	k1gNnSc4	předstírání
před	před	k7c7	před
vlastní	vlastní	k2eAgFnSc7d1	vlastní
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
inženýr	inženýr	k1gMnSc1	inženýr
Králík	Králík	k1gMnSc1	Králík
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
Vrána	vrána	k1gFnSc1	vrána
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Králík	Králík	k1gMnSc1	Králík
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Námět	námět	k1gInSc1	námět
a	a	k8xC	a
scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
</s>
</p>
<p>
<s>
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
</s>
</p>
<p>
<s>
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Šlapeta	Šlapet	k1gMnSc2	Šlapet
</s>
</p>
<p>
<s>
Střih	střih	k1gInSc1	střih
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Brožek	Brožek	k1gMnSc1	Brožek
</s>
</p>
<p>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
<g/>
:	:	kIx,	:
barevný	barevný	k2eAgInSc1d1	barevný
<g/>
,	,	kIx,	,
85	[number]	k4	85
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc2	komedie
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
díle	dílo	k1gNnSc6	dílo
zábavného	zábavný	k2eAgInSc2d1	zábavný
pořadu	pořad	k1gInSc2	pořad
Veselé	Veselé	k2eAgFnSc2d1	Veselé
příhody	příhoda	k1gFnSc2	příhoda
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
mystifikační	mystifikační	k2eAgFnPc4d1	mystifikační
historky	historka	k1gFnPc4	historka
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
vzniku	vznik	k1gInSc3	vznik
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Vrchní	vrchní	k1gMnSc1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Vrchní	vrchní	k1gMnSc1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Vrchní	vrchní	k1gMnSc1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vrchní	vrchní	k1gMnSc1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
ve	v	k7c6	v
Filmovém	filmový	k2eAgInSc6d1	filmový
přehledu	přehled	k1gInSc6	přehled
</s>
</p>
