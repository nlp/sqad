<s>
Motto	motto	k1gNnSc1	motto
(	(	kIx(	(
<g/>
též	též	k9	též
moto	moto	k1gNnSc1	moto
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fráze	fráze	k1gFnSc1	fráze
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
motivaci	motivace	k1gFnSc4	motivace
nebo	nebo	k8xC	nebo
záměr	záměr	k1gInSc4	záměr
člověka	člověk	k1gMnSc2	člověk
či	či	k8xC	či
skupiny	skupina	k1gFnSc2	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Motto	motto	k1gNnSc1	motto
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
heslo	heslo	k1gNnSc4	heslo
<g/>
,	,	kIx,	,
citát	citát	k1gInSc4	citát
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
sentence	sentence	k1gFnSc1	sentence
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
základní	základní	k2eAgFnSc3d1	základní
myšlence	myšlenka	k1gFnSc3	myšlenka
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
význam	význam	k1gInSc1	význam
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyjádření	vyjádření	k1gNnSc4	vyjádření
státního	státní	k2eAgInSc2d1	státní
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Veritas	Veritas	k1gMnSc1	Veritas
vincit	vincit	k1gMnSc1	vincit
(	(	kIx(	(
<g/>
Pravda	pravda	k9	pravda
vítězí	vítězit	k5eAaImIp3nS	vítězit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
motto	motto	k1gNnSc1	motto
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
motto	motto	k1gNnSc1	motto
=	=	kIx~	=
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
le	le	k?	le
mot	moto	k1gNnPc2	moto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
motto	motto	k1gNnSc1	motto
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
heslo	heslo	k1gNnSc1	heslo
nebo	nebo	k8xC	nebo
deviza	deviza	k1gFnSc1	deviza
<g/>
)	)	kIx)	)
stuha	stuha	k1gFnSc1	stuha
umístěná	umístěný	k2eAgFnSc1d1	umístěná
pod	pod	k7c7	pod
erbem	erb	k1gInSc7	erb
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
se	se	k3xPyFc4	se
v	v	k7c6	v
základních	základní	k2eAgFnPc6d1	základní
tinkturách	tinktura	k1gFnPc6	tinktura
erbu	erb	k1gInSc2	erb
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
erbu	erb	k1gInSc2	erb
štítonoši	štítonoš	k1gMnPc1	štítonoš
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
pásce	páska	k1gFnSc6	páska
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
heraldice	heraldika	k1gFnSc6	heraldika
tato	tento	k3xDgNnPc1	tento
slovní	slovní	k2eAgNnPc1d1	slovní
spojení	spojení	k1gNnPc1	spojení
obvykle	obvykle	k6eAd1	obvykle
reflektují	reflektovat	k5eAaImIp3nP	reflektovat
rodovou	rodový	k2eAgFnSc4d1	rodová
tradici	tradice	k1gFnSc4	tradice
nebo	nebo	k8xC	nebo
úctu	úcta	k1gFnSc4	úcta
k	k	k7c3	k
panovníkovi	panovník	k1gMnSc3	panovník
a	a	k8xC	a
bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
hesla	heslo	k1gNnPc1	heslo
byla	být	k5eAaImAgNnP	být
psána	psát	k5eAaImNgNnP	psát
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
především	především	k9	především
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Pokřik	pokřik	k1gInSc1	pokřik
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
válečný	válečný	k2eAgInSc1d1	válečný
pokřik	pokřik	k1gInSc1	pokřik
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
páska	páska	k1gFnSc1	páska
umísťována	umísťován	k2eAgFnSc1d1	umísťována
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
erbu	erb	k1gInSc2	erb
ke	k	k7c3	k
klenotu	klenot	k1gInSc3	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
krátká	krátký	k2eAgFnSc1d1	krátká
věta	věta	k1gFnSc1	věta
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
např.	např.	kA	např.
skotské	skotský	k2eAgInPc1d1	skotský
kmeny	kmen	k1gInPc1	kmen
svolávaly	svolávat	k5eAaImAgInP	svolávat
do	do	k7c2	do
boje	boj	k1gInSc2	boj
nebo	nebo	k8xC	nebo
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
vlasti	vlast	k1gFnSc2	vlast
(	(	kIx(	(
<g/>
skotský	skotský	k2eAgInSc1d1	skotský
národní	národní	k2eAgInSc1d1	národní
pokřik	pokřik	k1gInSc1	pokřik
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
In	In	k1gFnSc1	In
My	my	k3xPp1nPc1	my
Defens	Defensa	k1gFnPc2	Defensa
God	God	k1gMnSc1	God
Me	Me	k1gMnSc1	Me
Defend	Defend	k1gMnSc1	Defend
<g/>
,	,	kIx,	,
na	na	k7c4	na
erb	erb	k1gInSc4	erb
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
zkrácené	zkrácený	k2eAgFnSc2d1	zkrácená
In	In	k1gFnSc2	In
Defens	Defensa	k1gFnPc2	Defensa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
slogan	slogan	k1gInSc1	slogan
claim	claim	k1gInSc1	claim
(	(	kIx(	(
<g/>
reklama	reklama	k1gFnSc1	reklama
<g/>
)	)	kIx)	)
</s>
