<p>
<s>
LOTUS-Simulator	LOTUS-Simulator	k1gInSc1	LOTUS-Simulator
je	být	k5eAaImIp3nS	být
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
společností	společnost	k1gFnPc2	společnost
Oriolus	Oriolus	k1gMnSc1	Oriolus
Software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
Steam	Steam	k1gInSc4	Steam
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
Early	earl	k1gMnPc4	earl
Access	Access	k1gInSc4	Access
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
komplexní	komplexní	k2eAgFnSc4d1	komplexní
simulaci	simulace	k1gFnSc4	simulace
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
celku	celek	k1gInSc6	celek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
klade	klást	k5eAaImIp3nS	klást
silný	silný	k2eAgInSc4d1	silný
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
detailů	detail	k1gInPc2	detail
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
prozatím	prozatím	k6eAd1	prozatím
primárně	primárně	k6eAd1	primárně
určena	určit	k5eAaPmNgFnS	určit
tvůrcům	tvůrce	k1gMnPc3	tvůrce
přídavků	přídavek	k1gInPc2	přídavek
(	(	kIx(	(
<g/>
addonů	addon	k1gMnPc2	addon
<g/>
)	)	kIx)	)
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zatím	zatím	k6eAd1	zatím
nabízí	nabízet	k5eAaImIp3nS	nabízet
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
testovací	testovací	k2eAgFnSc4d1	testovací
trať	trať	k1gFnSc4	trať
a	a	k8xC	a
model	model	k1gInSc4	model
tramvaje	tramvaj	k1gFnSc2	tramvaj
GT	GT	kA	GT
<g/>
6	[number]	k4	6
<g/>
N.	N.	kA	N.
Autory	autor	k1gMnPc7	autor
projektu	projekt	k1gInSc2	projekt
LOTUS	Lotus	kA	Lotus
jsou	být	k5eAaImIp3nP	být
Janine	Janin	k1gInSc5	Janin
Kuhnt	Kuhnt	k1gMnSc1	Kuhnt
a	a	k8xC	a
Marcel	Marcel	k1gMnSc1	Marcel
Kuhnt	Kuhnt	k1gMnSc1	Kuhnt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
simulátor	simulátor	k1gInSc1	simulátor
autobusů	autobus	k1gInPc2	autobus
OMSI	OMSI	kA	OMSI
–	–	k?	–
Der	drát	k5eAaImRp2nS	drát
Omnibus	omnibus	k1gInSc1	omnibus
Simulator	Simulator	k1gInSc1	Simulator
<g/>
.	.	kIx.	.
</s>
<s>
LOTUS	Lotus	kA	Lotus
nyní	nyní	k6eAd1	nyní
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
herní	herní	k2eAgFnSc1d1	herní
engine	enginout	k5eAaPmIp3nS	enginout
obsahující	obsahující	k2eAgFnSc4d1	obsahující
flexibilitu	flexibilita	k1gFnSc4	flexibilita
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
jízdních	jízdní	k2eAgInPc2d1	jízdní
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
import	import	k1gInSc1	import
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
přichází	přicházet	k5eAaImIp3nS	přicházet
i	i	k9	i
mapový	mapový	k2eAgInSc1d1	mapový
editor	editor	k1gInSc1	editor
a	a	k8xC	a
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
vkládání	vkládání	k1gNnSc4	vkládání
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
scenérií	scenérie	k1gFnPc2	scenérie
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
vývojářů	vývojář	k1gMnPc2	vývojář
zároveň	zároveň	k6eAd1	zároveň
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Come	Com	k1gMnSc2	Com
on	on	k3xPp3gMnSc1	on
in	in	k?	in
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
culture	cultur	k1gMnSc5	cultur
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
hráče	hráč	k1gMnSc4	hráč
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
způsob	způsob	k1gInSc1	způsob
podílení	podílení	k1gNnSc2	podílení
se	se	k3xPyFc4	se
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
simulátoru	simulátor	k1gInSc2	simulátor
v	v	k7c6	v
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
formě	forma	k1gFnSc6	forma
–	–	k?	–
ať	ať	k9	ať
už	už	k6eAd1	už
samostatným	samostatný	k2eAgInSc7d1	samostatný
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
diskuzí	diskuze	k1gFnSc7	diskuze
či	či	k8xC	či
propagací	propagace	k1gFnSc7	propagace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zkratka	zkratka	k1gFnSc1	zkratka
LOTUS	Lotus	kA	Lotus
==	==	k?	==
</s>
</p>
<p>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výrobce	výrobce	k1gMnPc4	výrobce
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
shodu	shoda	k1gFnSc4	shoda
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc4	každý
jednotlivé	jednotlivý	k2eAgNnSc4d1	jednotlivé
písmeno	písmeno	k1gNnSc4	písmeno
představuje	představovat	k5eAaImIp3nS	představovat
určitý	určitý	k2eAgInSc1d1	určitý
segment	segment	k1gInSc1	segment
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
hromadné	hromadný	k2eAgFnSc6d1	hromadná
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L	L	kA	L
–	–	k?	–
LEITSTELLE	LEITSTELLE	kA	LEITSTELLE
–	–	k?	–
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
centrálu	centrála	k1gFnSc4	centrála
<g/>
,	,	kIx,	,
seskupení	seskupení	k1gNnSc1	seskupení
více	hodně	k6eAd2	hodně
věcí	věc	k1gFnSc7	věc
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
<g/>
,	,	kIx,	,
v	v	k7c6	v
plánovaném	plánovaný	k2eAgInSc6d1	plánovaný
multiplayeru	multiplayer	k1gInSc6	multiplayer
zahrne	zahrnout	k5eAaPmIp3nS	zahrnout
toto	tento	k3xDgNnSc4	tento
písmeno	písmeno	k1gNnSc4	písmeno
dispečera	dispečer	k1gMnSc2	dispečer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
–	–	k?	–
OMNIBUS	omnibus	k1gInSc1	omnibus
–	–	k?	–
představuje	představovat	k5eAaImIp3nS	představovat
veškerou	veškerý	k3xTgFnSc4	veškerý
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
včetně	včetně	k7c2	včetně
trolejbusů	trolejbus	k1gInPc2	trolejbus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
T	T	kA	T
–	–	k?	–
TRAM	tram	k1gInSc1	tram
–	–	k?	–
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
simulátoru	simulátor	k1gInSc6	simulátor
můžete	moct	k5eAaImIp2nP	moct
jezdit	jezdit	k5eAaImF	jezdit
s	s	k7c7	s
tramvají	tramvaj	k1gFnSc7	tramvaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
–	–	k?	–
UNDERGROUND	underground	k1gInSc1	underground
–	–	k?	–
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dopravy	doprava	k1gFnSc2	doprava
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
metro	metro	k1gNnSc1	metro
a	a	k8xC	a
veškeré	veškerý	k3xTgFnPc1	veškerý
podzemní	podzemní	k2eAgFnPc1d1	podzemní
dráhy	dráha	k1gFnPc1	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
–	–	k?	–
SUBURBAN	SUBURBAN	kA	SUBURBAN
TRAIN	TRAIN	kA	TRAIN
–	–	k?	–
poslední	poslední	k2eAgFnSc7d1	poslední
součástí	součást	k1gFnSc7	součást
kolejové	kolejový	k2eAgFnSc2d1	kolejová
dopravy	doprava	k1gFnSc2	doprava
jsou	být	k5eAaImIp3nP	být
příměstské	příměstský	k2eAgInPc1d1	příměstský
vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
==	==	k?	==
</s>
</p>
<p>
<s>
LOTUS	Lotus	kA	Lotus
bude	být	k5eAaImBp3nS	být
stavěn	stavit	k5eAaImNgInS	stavit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
modulů	modul	k1gInPc2	modul
–	–	k?	–
silnice	silnice	k1gFnPc4	silnice
<g/>
,	,	kIx,	,
koleje	kolej	k1gFnPc4	kolej
a	a	k8xC	a
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jezdit	jezdit	k5eAaImF	jezdit
s	s	k7c7	s
autobusem	autobus	k1gInSc7	autobus
nebo	nebo	k8xC	nebo
trolejbusem	trolejbus	k1gInSc7	trolejbus
<g/>
,	,	kIx,	,
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
s	s	k7c7	s
tramvají	tramvaj	k1gFnSc7	tramvaj
<g/>
,	,	kIx,	,
metrem	metro	k1gNnSc7	metro
nebo	nebo	k8xC	nebo
vlakem	vlak	k1gInSc7	vlak
a	a	k8xC	a
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgInSc1d1	oficiální
německý	německý	k2eAgInSc1d1	německý
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
CZ	CZ	kA	CZ
<g/>
/	/	kIx~	/
<g/>
SK	Sk	kA	Sk
portál	portál	k1gInSc1	portál
</s>
</p>
