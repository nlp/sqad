<p>
<s>
Autobus	autobus	k1gInSc1	autobus
je	být	k5eAaImIp3nS	být
motorové	motorový	k2eAgNnSc4d1	motorové
vozidlo	vozidlo	k1gNnSc4	vozidlo
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
osob	osoba	k1gFnPc2	osoba
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poháněn	pohánět	k5eAaImNgInS	pohánět
většinou	většinou	k6eAd1	většinou
vznětovým	vznětový	k2eAgInSc7d1	vznětový
motorem	motor	k1gInSc7	motor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
moderní	moderní	k2eAgFnSc2d1	moderní
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
autobusu	autobus	k1gInSc2	autobus
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
autobus	autobus	k1gInSc1	autobus
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
omnibus	omnibus	k1gInSc1	omnibus
<g/>
)	)	kIx)	)
se	s	k7c7	s
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
představil	představit	k5eAaPmAgMnS	představit
Karl	Karl	k1gMnSc1	Karl
Benz	Benz	k1gMnSc1	Benz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
do	do	k7c2	do
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
linkového	linkový	k2eAgInSc2d1	linkový
provozu	provoz	k1gInSc2	provoz
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
nespolehlivá	spolehlivý	k2eNgNnPc4d1	nespolehlivé
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
omezený	omezený	k2eAgInSc4d1	omezený
počet	počet	k1gInSc4	počet
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
10-20	[number]	k4	10-20
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
však	však	k9	však
šel	jít	k5eAaImAgInS	jít
rychle	rychle	k6eAd1	rychle
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
již	již	k6eAd1	již
autobusy	autobus	k1gInPc1	autobus
uvezly	uvézt	k5eAaPmAgInP	uvézt
až	až	k6eAd1	až
50	[number]	k4	50
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
poháněny	poháněn	k2eAgInPc1d1	poháněn
různými	různý	k2eAgNnPc7d1	různé
palivy	palivo	k1gNnPc7	palivo
–	–	k?	–
na	na	k7c4	na
svítiplyn	svítiplyn	k1gInSc4	svítiplyn
<g/>
,	,	kIx,	,
na	na	k7c4	na
naftu	nafta	k1gFnSc4	nafta
<g/>
,	,	kIx,	,
na	na	k7c4	na
dřevoplyn	dřevoplyn	k1gInSc4	dřevoplyn
atp.	atp.	kA	atp.
Hlavní	hlavní	k2eAgInSc4d1	hlavní
rozvoj	rozvoj	k1gInSc4	rozvoj
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
však	však	k9	však
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
pro	pro	k7c4	pro
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
dopravu	doprava	k1gFnSc4	doprava
(	(	kIx(	(
<g/>
zlevnil	zlevnit	k5eAaPmAgInS	zlevnit
benzín	benzín	k1gInSc1	benzín
a	a	k8xC	a
nafta	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
kvalitnější	kvalitní	k2eAgFnPc1d2	kvalitnější
silnice	silnice	k1gFnPc1	silnice
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
autobusy	autobus	k1gInPc4	autobus
postupně	postupně	k6eAd1	postupně
převládly	převládnout	k5eAaPmAgInP	převládnout
v	v	k7c6	v
MHD	MHD	kA	MHD
a	a	k8xC	a
doplnily	doplnit	k5eAaPmAgFnP	doplnit
tak	tak	k9	tak
tramvaje	tramvaj	k1gFnPc1	tramvaj
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
nevyžadujících	vyžadující	k2eNgFnPc6d1	nevyžadující
vysoké	vysoký	k2eAgNnSc4d1	vysoké
přepravní	přepravní	k2eAgFnPc4d1	přepravní
kapacity	kapacita	k1gFnPc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gNnPc2	on
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
manévrovatelnější	manévrovatelný	k2eAgInPc1d2	manévrovatelný
<g/>
,	,	kIx,	,
levnější	levný	k2eAgInPc1d2	levnější
<g/>
,	,	kIx,	,
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgInPc1d1	nízký
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
budování	budování	k1gNnSc4	budování
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
autobusy	autobus	k1gInPc1	autobus
mohou	moct	k5eAaImIp3nP	moct
dovolit	dovolit	k5eAaPmF	dovolit
i	i	k8xC	i
některá	některý	k3yIgNnPc1	některý
menší	malý	k2eAgNnPc1d2	menší
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
autobus	autobus	k1gInSc1	autobus
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
autobus	autobus	k1gInSc4	autobus
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
používal	používat	k5eAaImAgMnS	používat
stejný	stejný	k2eAgInSc4d1	stejný
termín	termín	k1gInSc4	termín
jako	jako	k8xS	jako
pro	pro	k7c4	pro
dřívější	dřívější	k2eAgInSc4d1	dřívější
koněspřežný	koněspřežný	k2eAgInSc4d1	koněspřežný
prostředek	prostředek	k1gInSc4	prostředek
nekolejové	kolejový	k2eNgFnSc2d1	nekolejová
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
omnibus	omnibus	k1gInSc1	omnibus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
označení	označení	k1gNnSc3	označení
automobilní	automobilní	k2eAgInSc4d1	automobilní
omnibus	omnibus	k1gInSc4	omnibus
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
pak	pak	k6eAd1	pak
zkrácením	zkrácení	k1gNnSc7	zkrácení
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
současný	současný	k2eAgInSc1d1	současný
termín	termín	k1gInSc1	termín
autobus	autobus	k1gInSc1	autobus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Technické	technický	k2eAgInPc1d1	technický
požadavky	požadavek	k1gInPc1	požadavek
===	===	k?	===
</s>
</p>
<p>
<s>
Autobusy	autobus	k1gInPc1	autobus
musí	muset	k5eAaImIp3nP	muset
vyhovovat	vyhovovat	k5eAaImF	vyhovovat
zkoušce	zkouška	k1gFnSc3	zkouška
stability	stabilita	k1gFnSc2	stabilita
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
být	být	k5eAaImF	být
použity	použit	k2eAgFnPc1d1	použita
hořlavé	hořlavý	k2eAgFnPc1d1	hořlavá
a	a	k8xC	a
nasákavé	nasákavý	k2eAgInPc1d1	nasákavý
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
elektroinstalace	elektroinstalace	k1gFnSc1	elektroinstalace
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
odolná	odolný	k2eAgFnSc1d1	odolná
vůči	vůči	k7c3	vůči
teplotě	teplota	k1gFnSc3	teplota
i	i	k8xC	i
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
autobus	autobus	k1gInSc1	autobus
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
nejméně	málo	k6eAd3	málo
dvoje	dvoje	k4xRgFnPc4	dvoje
dveře	dveře	k1gFnPc4	dveře
(	(	kIx(	(
<g/>
jedny	jeden	k4xCgInPc1	jeden
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nouzové	nouzový	k2eAgNnSc4d1	nouzové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
výška	výška	k1gFnSc1	výška
schůdku	schůdek	k1gInSc2	schůdek
je	být	k5eAaImIp3nS	být
340	[number]	k4	340
mm	mm	kA	mm
pro	pro	k7c4	pro
autobusy	autobus	k1gInPc4	autobus
určené	určený	k2eAgInPc4d1	určený
i	i	k9	i
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
stojících	stojící	k2eAgMnPc2d1	stojící
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
třídy	třída	k1gFnSc2	třída
I	I	kA	I
a	a	k8xC	a
A	A	kA	A
<g/>
)	)	kIx)	)
a	a	k8xC	a
380	[number]	k4	380
mm	mm	kA	mm
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
třídy	třída	k1gFnPc4	třída
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
III	III	kA	III
a	a	k8xC	a
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
pásy	pás	k1gInPc1	pás
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
sedadlech	sedadlo	k1gNnPc6	sedadlo
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
autobusy	autobus	k1gInPc1	autobus
tříd	třída	k1gFnPc2	třída
III	III	kA	III
a	a	k8xC	a
B	B	kA	B
schvalované	schvalovaný	k2eAgInPc1d1	schvalovaný
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
<g/>
Obecně	obecně	k6eAd1	obecně
není	být	k5eNaImIp3nS	být
zakázána	zakázán	k2eAgFnSc1d1	zakázána
přeprava	přeprava	k1gFnSc1	přeprava
stojících	stojící	k2eAgMnPc2d1	stojící
cestujících	cestující	k1gMnPc2	cestující
na	na	k7c6	na
dálnicích	dálnice	k1gFnPc6	dálnice
<g/>
,	,	kIx,	,
dálnice	dálnice	k1gFnPc1	dálnice
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
i	i	k9	i
autobusy	autobus	k1gInPc7	autobus
městského	městský	k2eAgNnSc2d1	Městské
provedení	provedení	k1gNnSc2	provedení
se	s	k7c7	s
stojícími	stojící	k2eAgFnPc7d1	stojící
cestujícími	cestující	k1gFnPc7	cestující
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
vedena	veden	k2eAgFnSc1d1	vedena
i	i	k8xC	i
linková	linkový	k2eAgFnSc1d1	Linková
doprava	doprava	k1gFnSc1	doprava
autobusy	autobus	k1gInPc1	autobus
městského	městský	k2eAgNnSc2d1	Městské
provedení	provedení	k1gNnSc2	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
přepravy	přeprava	k1gFnSc2	přeprava
stojících	stojící	k2eAgMnPc2d1	stojící
cestujících	cestující	k1gMnPc2	cestující
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
dán	dát	k5eAaPmNgInS	dát
podmínkami	podmínka	k1gFnPc7	podmínka
provozu	provoz	k1gInSc2	provoz
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
typu	typ	k1gInSc2	typ
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
autobusů	autobus	k1gInPc2	autobus
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
dělení	dělení	k1gNnSc1	dělení
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
vozidel	vozidlo	k1gNnPc2	vozidlo
v	v	k7c6	v
EU	EU	kA	EU
definuje	definovat	k5eAaBmIp3nS	definovat
rámcová	rámcový	k2eAgFnSc1d1	rámcová
směrnice	směrnice	k1gFnSc1	směrnice
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
46	[number]	k4	46
<g/>
/	/	kIx~	/
<g/>
ES	es	k1gNnPc2	es
(	(	kIx(	(
<g/>
jež	jenž	k3xRgNnSc1	jenž
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
dřívější	dřívější	k2eAgInSc1d1	dřívější
70	[number]	k4	70
<g/>
/	/	kIx~	/
<g/>
156	[number]	k4	156
<g/>
/	/	kIx~	/
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vozidla	vozidlo	k1gNnPc1	vozidlo
M	M	kA	M
=	=	kIx~	=
Motorová	motorový	k2eAgNnPc1d1	motorové
vozidla	vozidlo	k1gNnPc1	vozidlo
s	s	k7c7	s
nejméně	málo	k6eAd3	málo
čtyřmi	čtyři	k4xCgInPc7	čtyři
koly	kola	k1gFnSc2	kola
konstruovaná	konstruovaný	k2eAgNnPc1d1	konstruované
a	a	k8xC	a
vyrobená	vyrobený	k2eAgNnPc1d1	vyrobené
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
M1	M1	k4	M1
=	=	kIx~	=
nejvýše	vysoce	k6eAd3	vysoce
osm	osm	k4xCc1	osm
sedadel	sedadlo	k1gNnPc2	sedadlo
kromě	kromě	k7c2	kromě
sedadla	sedadlo	k1gNnSc2	sedadlo
řidiče	řidič	k1gMnSc2	řidič
<g/>
.	.	kIx.	.
</s>
<s>
M2	M2	k4	M2
=	=	kIx~	=
více	hodně	k6eAd2	hodně
než	než	k8xS	než
osm	osm	k4xCc4	osm
sedadel	sedadlo	k1gNnPc2	sedadlo
kromě	kromě	k7c2	kromě
sedadla	sedadlo	k1gNnSc2	sedadlo
řidiče	řidič	k1gMnSc2	řidič
a	a	k8xC	a
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hmotnost	hmotnost	k1gFnSc1	hmotnost
nepřevyšující	převyšující	k2eNgFnSc1d1	nepřevyšující
5	[number]	k4	5
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
M3	M3	k4	M3
=	=	kIx~	=
více	hodně	k6eAd2	hodně
než	než	k8xS	než
osm	osm	k4xCc4	osm
sedadel	sedadlo	k1gNnPc2	sedadlo
kromě	kromě	k7c2	kromě
sedadla	sedadlo	k1gNnSc2	sedadlo
řidiče	řidič	k1gMnSc2	řidič
a	a	k8xC	a
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hmotnost	hmotnost	k1gFnSc4	hmotnost
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
5	[number]	k4	5
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nákladní	nákladní	k2eAgNnPc4d1	nákladní
vozidla	vozidlo	k1gNnPc4	vozidlo
N	N	kA	N
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
téže	tenže	k3xDgFnSc2	tenže
směrnice	směrnice	k1gFnSc2	směrnice
dělí	dělit	k5eAaImIp3nP	dělit
dle	dle	k7c2	dle
maximální	maximální	k2eAgFnSc2d1	maximální
hmotnosti	hmotnost	k1gFnSc2	hmotnost
na	na	k7c4	na
N1	N1	k1gFnSc4	N1
nepřevyšující	převyšující	k2eNgFnSc4d1	nepřevyšující
3.5	[number]	k4	3.5
<g/>
t	t	k?	t
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
N2	N2	k1gFnSc4	N2
nepřevyšující	převyšující	k2eNgFnSc4d1	nepřevyšující
12	[number]	k4	12
<g/>
t	t	k?	t
a	a	k8xC	a
N3	N3	k1gFnSc4	N3
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
12	[number]	k4	12
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Směrnice	směrnice	k1gFnSc1	směrnice
dále	daleko	k6eAd2	daleko
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
třídy	třída	k1gFnPc4	třída
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
přepravovaných	přepravovaný	k2eAgFnPc2d1	přepravovaná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
nejvýše	nejvýše	k6eAd1	nejvýše
22	[number]	k4	22
cestujících	cestující	k1gFnPc2	cestující
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
B	B	kA	B
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
sedící	sedící	k2eAgFnSc4d1	sedící
cestující	cestující	k1gFnSc4	cestující
<g/>
)	)	kIx)	)
a	a	k8xC	a
třídy	třída	k1gFnSc2	třída
A	A	kA	A
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
sedících	sedící	k2eAgMnPc2d1	sedící
i	i	k8xC	i
stojících	stojící	k2eAgMnPc2d1	stojící
cestujících	cestující	k1gMnPc2	cestující
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
než	než	k8xS	než
22	[number]	k4	22
cestujících	cestující	k1gFnPc2	cestující
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
III	III	kA	III
(	(	kIx(	(
<g/>
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
sedící	sedící	k2eAgMnPc4d1	sedící
cestující	cestující	k1gMnPc4	cestující
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dálkové	dálkový	k2eAgFnPc4d1	dálková
linky	linka	k1gFnPc4	linka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třídy	třída	k1gFnPc4	třída
II	II	kA	II
(	(	kIx(	(
<g/>
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
sedící	sedící	k2eAgFnSc4d1	sedící
cestující	cestující	k1gFnSc4	cestující
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
umožňující	umožňující	k2eAgFnPc4d1	umožňující
i	i	k9	i
přepravu	přeprava	k1gFnSc4	přeprava
stojících	stojící	k2eAgNnPc2d1	stojící
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
meziměstskou	meziměstský	k2eAgFnSc4d1	meziměstská
dopravu	doprava	k1gFnSc4	doprava
<g/>
)	)	kIx)	)
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
třídy	třída	k1gFnPc4	třída
I	i	k9	i
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
sedící	sedící	k2eAgFnSc4d1	sedící
i	i	k8xC	i
stojící	stojící	k2eAgFnSc4d1	stojící
cestující	cestující	k1gFnSc4	cestující
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
MHD	MHD	kA	MHD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
nízkopodlažní	nízkopodlažní	k2eAgNnSc4d1	nízkopodlažní
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
autobus	autobus	k1gInSc1	autobus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nejméně	málo	k6eAd3	málo
35	[number]	k4	35
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
pro	pro	k7c4	pro
stojící	stojící	k2eAgMnPc4d1	stojící
cestující	cestující	k1gMnPc4	cestující
je	být	k5eAaImIp3nS	být
dosažitelných	dosažitelný	k2eAgFnPc2d1	dosažitelná
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
jediným	jediný	k2eAgInSc7d1	jediný
stupněm	stupeň	k1gInSc7	stupeň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
stanovena	stanoven	k2eAgFnSc1d1	stanovena
maximální	maximální	k2eAgFnSc1d1	maximální
výška	výška	k1gFnSc1	výška
schůdku	schůdek	k1gInSc2	schůdek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Směrnici	směrnice	k1gFnSc3	směrnice
EHS	EHS	kA	EHS
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
nahradila	nahradit	k5eAaPmAgFnS	nahradit
směrnice	směrnice	k1gFnSc1	směrnice
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
46	[number]	k4	46
<g/>
/	/	kIx~	/
<g/>
EHS	EHS	kA	EHS
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
rámec	rámec	k1gInSc1	rámec
pro	pro	k7c4	pro
typové	typový	k2eAgNnSc4d1	typové
schvalování	schvalování	k1gNnSc4	schvalování
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc2	jejich
přípojných	přípojný	k2eAgNnPc2d1	přípojné
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
komponent	komponenta	k1gFnPc2	komponenta
a	a	k8xC	a
samostatných	samostatný	k2eAgFnPc2d1	samostatná
technických	technický	k2eAgFnPc2d1	technická
jednotek	jednotka	k1gFnPc2	jednotka
pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
směrnici	směrnice	k1gFnSc6	směrnice
je	být	k5eAaImIp3nS	být
terminologie	terminologie	k1gFnSc1	terminologie
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
typů	typ	k1gInPc2	typ
vozidel	vozidlo	k1gNnPc2	vozidlo
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
<g/>
,	,	kIx,	,
směrnice	směrnice	k1gFnSc1	směrnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
názvoslovnou	názvoslovný	k2eAgFnSc4d1	názvoslovná
normu	norma	k1gFnSc4	norma
ISO	ISO	kA	ISO
3833	[number]	k4	3833
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Neoficiální	neoficiální	k2eAgNnSc1d1	neoficiální
dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
kapacity	kapacita	k1gFnSc2	kapacita
===	===	k?	===
</s>
</p>
<p>
<s>
Obsaditelnost	obsaditelnost	k1gFnSc1	obsaditelnost
autobusů	autobus	k1gInPc2	autobus
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
X	X	kA	X
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
X	X	kA	X
označuje	označovat	k5eAaImIp3nS	označovat
počet	počet	k1gInSc1	počet
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
a	a	k8xC	a
+1	+1	k4	+1
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
řidiče	řidič	k1gMnPc4	řidič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
užívaná	užívaný	k2eAgFnSc1d1	užívaná
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
terminologie	terminologie	k1gFnSc1	terminologie
není	být	k5eNaImIp3nS	být
ustálená	ustálený	k2eAgFnSc1d1	ustálená
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zejména	zejména	k9	zejména
vymezení	vymezení	k1gNnSc4	vymezení
mikrobusů	mikrobus	k1gInPc2	mikrobus
<g/>
,	,	kIx,	,
minibusů	minibus	k1gInPc2	minibus
<g/>
,	,	kIx,	,
midibusů	midibus	k1gInPc2	midibus
nebo	nebo	k8xC	nebo
metrobusů	metrobus	k1gInPc2	metrobus
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
různých	různý	k2eAgInPc6d1	různý
zdrojích	zdroj	k1gInPc6	zdroj
podstatně	podstatně	k6eAd1	podstatně
lišit	lišit	k5eAaImF	lišit
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
midibusy	midibus	k1gInPc1	midibus
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
podskupinu	podskupina	k1gFnSc4	podskupina
minibusů	minibus	k1gInPc2	minibus
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
9	[number]	k4	9
až	až	k9	až
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
řazeny	řadit	k5eAaImNgFnP	řadit
mezi	mezi	k7c7	mezi
midibusy	midibus	k1gMnPc7	midibus
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
mezi	mezi	k7c4	mezi
standardní	standardní	k2eAgInPc4d1	standardní
autobusy	autobus	k1gInPc4	autobus
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
mikrobusy	mikrobus	k1gInPc7	mikrobus
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
řazeny	řazen	k2eAgInPc1d1	řazen
i	i	k8xC	i
malé	malý	k2eAgInPc1d1	malý
minibusy	minibus	k1gInPc1	minibus
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
mikrobusy	mikrobus	k1gInPc1	mikrobus
–	–	k?	–
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označují	označovat	k5eAaImIp3nP	označovat
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
7	[number]	k4	7
až	až	k9	až
9	[number]	k4	9
míst	místo	k1gNnPc2	místo
včetně	včetně	k7c2	včetně
řidiče	řidič	k1gInSc2	řidič
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc4d1	celková
hmotnost	hmotnost	k1gFnSc4	hmotnost
do	do	k7c2	do
3,5	[number]	k4	3,5
t.	t.	k?	t.
Spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
do	do	k7c2	do
8	[number]	k4	8
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
míst	místo	k1gNnPc2	místo
postačuje	postačovat	k5eAaImIp3nS	postačovat
řidičský	řidičský	k2eAgInSc4d1	řidičský
průkaz	průkaz	k1gInSc4	průkaz
skupiny	skupina	k1gFnSc2	skupina
B.	B.	kA	B.
Bývají	bývat	k5eAaImIp3nP	bývat
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
z	z	k7c2	z
dodávkových	dodávkový	k2eAgInPc2d1	dodávkový
automobilů	automobil	k1gInPc2	automobil
s	s	k7c7	s
karosérií	karosérie	k1gFnSc7	karosérie
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
kombi	kombi	k1gNnSc2	kombi
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sedadla	sedadlo	k1gNnPc1	sedadlo
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
řadách	řada	k1gFnPc6	řada
<g/>
,	,	kIx,	,
ke	k	k7c3	k
třetí	třetí	k4xOgFnSc3	třetí
řadě	řada	k1gFnSc3	řada
se	se	k3xPyFc4	se
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
bočními	boční	k2eAgFnPc7d1	boční
posuvnými	posuvný	k2eAgFnPc7d1	posuvná
dveřmi	dveře	k1gFnPc7	dveře
vedle	vedle	k7c2	vedle
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
opěradlo	opěradlo	k1gNnSc1	opěradlo
pravého	pravý	k2eAgMnSc2d1	pravý
cestujícího	cestující	k1gMnSc2	cestující
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
sklopné	sklopný	k2eAgNnSc1d1	sklopné
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
bývá	bývat	k5eAaImIp3nS	bývat
druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
sedadel	sedadlo	k1gNnPc2	sedadlo
užší	úzký	k2eAgFnSc1d2	užší
než	než	k8xS	než
řada	řada	k1gFnSc1	řada
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vpravo	vpravo	k6eAd1	vpravo
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zemí	zem	k1gFnPc2	zem
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
pravostranným	pravostranný	k2eAgInSc7d1	pravostranný
provozem	provoz	k1gInSc7	provoz
<g/>
)	)	kIx)	)
zbylo	zbýt	k5eAaPmAgNnS	zbýt
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
nástup	nástup	k1gInSc4	nástup
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
minibusy	minibus	k1gInPc1	minibus
–	–	k?	–
Mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8	[number]	k4	8
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
maximálně	maximálně	k6eAd1	maximálně
16	[number]	k4	16
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
řidičský	řidičský	k2eAgInSc4d1	řidičský
průkaz	průkaz	k1gInSc4	průkaz
skupiny	skupina	k1gFnSc2	skupina
D1	D1	k1gFnSc2	D1
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
karosářských	karosářský	k2eAgNnPc2d1	karosářské
provedení	provedení	k1gNnPc2	provedení
dodávkových	dodávkový	k2eAgInPc2d1	dodávkový
automobilů	automobil	k1gInPc2	automobil
–	–	k?	–
mívají	mívat	k5eAaImIp3nP	mívat
největší	veliký	k2eAgInSc4d3	veliký
možný	možný	k2eAgInSc4d1	možný
rozvor	rozvor	k1gInSc4	rozvor
dané	daný	k2eAgFnSc2d1	daná
typové	typový	k2eAgFnSc2d1	typová
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
vozu	vůz	k1gInSc2	vůz
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnPc1d1	zadní
řady	řada	k1gFnPc1	řada
sedadel	sedadlo	k1gNnPc2	sedadlo
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
dvojsedačka-ulička-sedačka	dvojsedačkaličkaedačka	k1gFnSc1	dvojsedačka-ulička-sedačka
<g/>
.	.	kIx.	.
</s>
<s>
Nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
jako	jako	k9	jako
u	u	k7c2	u
dodávky	dodávka	k1gFnSc2	dodávka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kokpit	kokpit	k1gInSc1	kokpit
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
řidiče	řidič	k1gMnPc4	řidič
a	a	k8xC	a
1-2	[number]	k4	1-2
spolujezdce	spolujezdec	k1gMnSc2	spolujezdec
<g/>
,	,	kIx,	,
a	a	k8xC	a
oddělený	oddělený	k2eAgInSc4d1	oddělený
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
cestující	cestující	k2eAgInSc4d1	cestující
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
z	z	k7c2	z
nákladového	nákladový	k2eAgInSc2d1	nákladový
prostoru	prostor	k1gInSc2	prostor
dodávky	dodávka	k1gFnSc2	dodávka
<g/>
,	,	kIx,	,
s	s	k7c7	s
přístupem	přístup	k1gInSc7	přístup
posuvnými	posuvný	k2eAgFnPc7d1	posuvná
dveřmi	dveře	k1gFnPc7	dveře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
dražší	drahý	k2eAgInPc1d2	dražší
modely	model	k1gInPc1	model
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
u	u	k7c2	u
autobusu	autobus	k1gInSc2	autobus
–	–	k?	–
dálkově	dálkově	k6eAd1	dálkově
ovládanými	ovládaný	k2eAgFnPc7d1	ovládaná
dveřmi	dveře	k1gFnPc7	dveře
vpravo	vpravo	k6eAd1	vpravo
vedle	vedle	k7c2	vedle
řidiče	řidič	k1gMnSc2	řidič
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
velikosti	velikost	k1gFnSc2	velikost
dále	daleko	k6eAd2	daleko
bývá	bývat	k5eAaImIp3nS	bývat
zadní	zadní	k2eAgMnSc1d1	zadní
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
náprav	náprava	k1gFnPc2	náprava
osazována	osazovat	k5eAaImNgFnS	osazovat
dvojmontáží	dvojmontáž	k1gFnPc2	dvojmontáž
kol	kola	k1gFnPc2	kola
<g/>
.	.	kIx.	.
<g/>
midibusy	midibus	k1gInPc1	midibus
–	–	k?	–
Malé	Malé	k2eAgInPc1d1	Malé
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
sedící	sedící	k2eAgMnPc4d1	sedící
cestující	cestující	k1gMnPc4	cestující
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
7-10	[number]	k4	7-10
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nasazovány	nasazován	k2eAgInPc1d1	nasazován
na	na	k7c4	na
málo	málo	k1gNnSc4	málo
vytížené	vytížený	k2eAgFnSc2d1	vytížená
linky	linka	k1gFnSc2	linka
nebo	nebo	k8xC	nebo
linky	linka	k1gFnSc2	linka
vedené	vedený	k2eAgFnSc2d1	vedená
extrémní	extrémní	k2eAgFnSc2d1	extrémní
trasou	trasa	k1gFnSc7	trasa
(	(	kIx(	(
<g/>
úzké	úzký	k2eAgFnSc2d1	úzká
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
prudké	prudký	k2eAgFnSc2d1	prudká
zatáčky	zatáčka	k1gFnSc2	zatáčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
velikosti	velikost	k1gFnSc2	velikost
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
řidičský	řidičský	k2eAgInSc4d1	řidičský
průkaz	průkaz	k1gInSc4	průkaz
skupiny	skupina	k1gFnSc2	skupina
D	D	kA	D
a	a	k8xC	a
sedadla	sedadlo	k1gNnSc2	sedadlo
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
příčném	příčný	k2eAgNnSc6d1	příčné
uspořádání	uspořádání	k1gNnSc6	uspořádání
2	[number]	k4	2
<g/>
-ulička-	lička-	k?	-ulička-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
buď	buď	k8xC	buď
o	o	k7c4	o
kratší	krátký	k2eAgInPc4d2	kratší
modely	model	k1gInPc4	model
běžných	běžný	k2eAgInPc2d1	běžný
autobusů	autobus	k1gInPc2	autobus
(	(	kIx(	(
<g/>
autobusy	autobus	k1gInPc1	autobus
SOR	SOR	kA	SOR
7,5	[number]	k4	7,5
Lili	Lili	k1gFnPc2	Lili
a	a	k8xC	a
9,5	[number]	k4	9,5
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
o	o	k7c4	o
zásadní	zásadní	k2eAgFnSc4d1	zásadní
přestavbu	přestavba	k1gFnSc4	přestavba
velké	velký	k2eAgFnSc2d1	velká
dodávky	dodávka	k1gFnSc2	dodávka
(	(	kIx(	(
<g/>
malého	malý	k2eAgInSc2d1	malý
nákladního	nákladní	k2eAgInSc2d1	nákladní
vozu	vůz	k1gInSc2	vůz
<g/>
)	)	kIx)	)
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
širší	široký	k2eAgFnSc2d2	širší
karoserie	karoserie	k1gFnSc2	karoserie
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
např.	např.	kA	např.
na	na	k7c6	na
podvozku	podvozek	k1gInSc6	podvozek
Iveco	Iveco	k6eAd1	Iveco
Daily	Dailo	k1gNnPc7	Dailo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dříve	dříve	k6eAd2	dříve
Ikarus	Ikarus	k1gMnSc1	Ikarus
553	[number]	k4	553
na	na	k7c6	na
podvozku	podvozek	k1gInSc6	podvozek
Avia	Avia	k1gFnSc1	Avia
A	a	k8xC	a
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
standardní	standardní	k2eAgInPc1d1	standardní
autobusy	autobus	k1gInPc1	autobus
–	–	k?	–
Dvounápravové	dvounápravový	k2eAgInPc4d1	dvounápravový
vozy	vůz	k1gInPc4	vůz
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
nejčastěji	často	k6eAd3	často
kolem	kolem	k7c2	kolem
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
druh	druh	k1gInSc1	druh
autobusu	autobus	k1gInSc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Obsaditelnost	obsaditelnost	k1gFnSc1	obsaditelnost
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
55	[number]	k4	55
sedících	sedící	k2eAgMnPc2d1	sedící
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
linkové	linkový	k2eAgInPc1d1	linkový
autobusy	autobus	k1gInPc1	autobus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
30	[number]	k4	30
sedících	sedící	k2eAgMnPc2d1	sedící
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
až	až	k9	až
70	[number]	k4	70
stojících	stojící	k2eAgMnPc2d1	stojící
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
městské	městský	k2eAgInPc1d1	městský
autobusy	autobus	k1gInPc1	autobus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
dvoupatrové	dvoupatrový	k2eAgInPc1d1	dvoupatrový
autobusy	autobus	k1gInPc1	autobus
–	–	k?	–
Dvounápravové	dvounápravový	k2eAgInPc1d1	dvounápravový
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
městské	městský	k2eAgNnSc1d1	Městské
<g/>
)	)	kIx)	)
i	i	k9	i
třínápravové	třínápravový	k2eAgFnPc1d1	třínápravová
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
dálkové	dálkový	k2eAgInPc4d1	dálkový
<g/>
)	)	kIx)	)
vozy	vůz	k1gInPc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
druhému	druhý	k4xOgNnSc3	druhý
podlaží	podlaží	k1gNnSc3	podlaží
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
celková	celkový	k2eAgFnSc1d1	celková
kapacita	kapacita	k1gFnSc1	kapacita
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
nebo	nebo	k8xC	nebo
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
klasických	klasický	k2eAgInPc2d1	klasický
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
manévrovatelnosti	manévrovatelnost	k1gFnSc2	manévrovatelnost
a	a	k8xC	a
průjezdnosti	průjezdnost	k1gFnSc2	průjezdnost
úzkých	úzký	k2eAgFnPc2d1	úzká
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
<g/>
třínápravové	třínápravový	k2eAgInPc1d1	třínápravový
autobusy	autobus	k1gInPc1	autobus
–	–	k?	–
většinou	většinou	k6eAd1	většinou
vozy	vůz	k1gInPc7	vůz
délky	délka	k1gFnSc2	délka
nad	nad	k7c4	nad
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzadu	vzadu	k6eAd1	vzadu
mají	mít	k5eAaImIp3nP	mít
místo	místo	k7c2	místo
jedné	jeden	k4xCgFnSc2	jeden
nápravy	náprava	k1gFnSc2	náprava
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
u	u	k7c2	u
klasických	klasický	k2eAgInPc2d1	klasický
autobusů	autobus	k1gInPc2	autobus
<g/>
)	)	kIx)	)
nápravy	náprava	k1gFnSc2	náprava
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Manévrovatelnost	manévrovatelnost	k1gFnSc1	manévrovatelnost
zhoršená	zhoršený	k2eAgFnSc1d1	zhoršená
délkou	délka	k1gFnSc7	délka
autobusu	autobus	k1gInSc2	autobus
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
řiditelnou	řiditelný	k2eAgFnSc7d1	řiditelná
poslední	poslední	k2eAgFnSc7d1	poslední
nápravou	náprava	k1gFnSc7	náprava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
otáčením	otáčení	k1gNnSc7	otáčení
volantu	volant	k1gInSc2	volant
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
natáčení	natáčení	k1gNnSc3	natáčení
první	první	k4xOgNnSc1	první
i	i	k8xC	i
poslední	poslední	k2eAgFnPc1d1	poslední
nápravy	náprava	k1gFnPc1	náprava
<g/>
,	,	kIx,	,
ta	ten	k3xDgNnPc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
natáčí	natáčet	k5eAaImIp3nS	natáčet
méně	málo	k6eAd2	málo
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
na	na	k7c4	na
opačnou	opačný	k2eAgFnSc4d1	opačná
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
prostřední	prostřední	k2eAgFnSc1d1	prostřední
<g/>
)	)	kIx)	)
náprava	náprava	k1gFnSc1	náprava
se	se	k3xPyFc4	se
neotáčí	otáčet	k5eNaImIp3nS	otáčet
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
hnací	hnací	k2eAgMnSc1d1	hnací
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
řízených	řízený	k2eAgFnPc2d1	řízená
náprav	náprava	k1gFnPc2	náprava
bývá	bývat	k5eAaImIp3nS	bývat
osazena	osadit	k5eAaPmNgFnS	osadit
dvojmontáží	dvojmontáž	k1gFnSc7	dvojmontáž
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
pojmou	pojmout	k5eAaPmIp3nP	pojmout
buď	buď	k8xC	buď
120	[number]	k4	120
(	(	kIx(	(
<g/>
městské	městský	k2eAgFnSc2d1	městská
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
70	[number]	k4	70
(	(	kIx(	(
<g/>
dálkové	dálkový	k2eAgNnSc4d1	dálkové
<g/>
,	,	kIx,	,
linkové	linkový	k2eAgNnSc4d1	linkové
<g/>
)	)	kIx)	)
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
<g/>
kloubové	kloubový	k2eAgInPc1d1	kloubový
autobusy	autobus	k1gInPc1	autobus
–	–	k?	–
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
z	z	k7c2	z
požadavku	požadavek	k1gInSc2	požadavek
větší	veliký	k2eAgFnPc4d2	veliký
přepravní	přepravní	k2eAgFnPc4d1	přepravní
kapacity	kapacita	k1gFnPc4	kapacita
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
patrové	patrový	k2eAgInPc1d1	patrový
autobusy	autobus	k1gInPc1	autobus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
jejich	jejich	k3xOp3gFnSc7	jejich
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
této	tento	k3xDgFnSc3	tento
délce	délka	k1gFnSc3	délka
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
kloubem	kloub	k1gInSc7	kloub
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
přední	přední	k2eAgFnSc4d1	přední
část	část	k1gFnSc4	část
vůči	vůči	k7c3	vůči
zadní	zadní	k2eAgFnSc3d1	zadní
může	moct	k5eAaImIp3nS	moct
natočit	natočit	k5eAaBmF	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
hnací	hnací	k2eAgFnSc1d1	hnací
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
motor	motor	k1gInSc4	motor
a	a	k8xC	a
hnací	hnací	k2eAgFnSc4d1	hnací
nápravu	náprava	k1gFnSc4	náprava
<g/>
)	)	kIx)	)
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
přívěs	přívěs	k1gInSc1	přívěs
<g/>
,	,	kIx,	,
stěny	stěna	k1gFnPc1	stěna
autobusu	autobus	k1gInSc2	autobus
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
kloubu	kloub	k1gInSc2	kloub
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
měchem	měch	k1gInSc7	měch
z	z	k7c2	z
pružných	pružný	k2eAgInPc2d1	pružný
gumových	gumový	k2eAgInPc2d1	gumový
dílců	dílec	k1gInPc2	dílec
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
"	"	kIx"	"
<g/>
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
slangový	slangový	k2eAgInSc1d1	slangový
termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
kloubový	kloubový	k2eAgInSc4d1	kloubový
autobus	autobus	k1gInSc4	autobus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
využívané	využívaný	k2eAgFnPc1d1	využívaná
jako	jako	k8xS	jako
městské	městský	k2eAgFnPc1d1	městská
nebo	nebo	k8xC	nebo
příměstské	příměstský	k2eAgFnPc1d1	příměstská
(	(	kIx(	(
<g/>
linkové	linkový	k2eAgFnPc1d1	Linková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
obsaditelnost	obsaditelnost	k1gFnSc1	obsaditelnost
stojících	stojící	k2eAgMnPc2d1	stojící
a	a	k8xC	a
sedících	sedící	k2eAgMnPc2d1	sedící
cestujících	cestující	k1gMnPc2	cestující
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
150	[number]	k4	150
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
městská	městský	k2eAgFnSc1d1	městská
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
dvoukloubové	dvoukloubový	k2eAgInPc1d1	dvoukloubový
autobusy	autobus	k1gInPc1	autobus
–	–	k?	–
Jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
většinou	většinou	k6eAd1	většinou
24	[number]	k4	24
m	m	kA	m
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nasazovány	nasazovat	k5eAaImNgFnP	nasazovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
extrémní	extrémní	k2eAgFnSc2d1	extrémní
zátěže	zátěž	k1gFnSc2	zátěž
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgFnPc1d3	nejrozšířenější
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gInPc2	on
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
nejvíc	nejvíc	k6eAd1	nejvíc
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zavádějí	zavádět	k5eAaImIp3nP	zavádět
nebo	nebo	k8xC	nebo
testují	testovat	k5eAaImIp3nP	testovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
nejsou	být	k5eNaImIp3nP	být
a	a	k8xC	a
od	od	k7c2	od
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
se	se	k3xPyFc4	se
testují	testovat	k5eAaImIp3nP	testovat
.	.	kIx.	.
</s>
<s>
Obsaditelnost	obsaditelnost	k1gFnSc1	obsaditelnost
je	být	k5eAaImIp3nS	být
až	až	k9	až
200	[number]	k4	200
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	on	k3xPp3gMnPc4	on
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
velkokapacitních	velkokapacitní	k2eAgFnPc2d1	velkokapacitní
tramvají	tramvaj	k1gFnPc2	tramvaj
nebo	nebo	k8xC	nebo
lehkého	lehký	k2eAgNnSc2d1	lehké
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
zavádí	zavádět	k5eAaImIp3nS	zavádět
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gNnPc4	on
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
metrobus	metrobus	k1gInSc1	metrobus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohde	mnohde	k6eAd1	mnohde
užíván	užíván	k2eAgInSc1d1	užíván
i	i	k9	i
pro	pro	k7c4	pro
autobusy	autobus	k1gInPc4	autobus
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Kombinace	kombinace	k1gFnSc1	kombinace
předchozích	předchozí	k2eAgInPc2d1	předchozí
typů	typ	k1gInPc2	typ
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
třínápravové	třínápravový	k2eAgInPc4d1	třínápravový
patrové	patrový	k2eAgInPc4d1	patrový
<g/>
,	,	kIx,	,
kloubové	kloubový	k2eAgInPc4d1	kloubový
patrovéLetištní	patrovéLetištní	k2eAgInPc4d1	patrovéLetištní
autobusy	autobus	k1gInPc4	autobus
nesplňující	splňující	k2eNgFnSc2d1	nesplňující
podmínky	podmínka	k1gFnSc2	podmínka
pro	pro	k7c4	pro
jízdu	jízda	k1gFnSc4	jízda
na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
(	(	kIx(	(
<g/>
širší	široký	k2eAgFnSc1d2	širší
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
kapacitu	kapacita	k1gFnSc4	kapacita
a	a	k8xC	a
na	na	k7c6	na
letištních	letištní	k2eAgFnPc6d1	letištní
plochách	plocha	k1gFnPc6	plocha
odpadá	odpadat	k5eAaImIp3nS	odpadat
problém	problém	k1gInSc1	problém
se	s	k7c7	s
zatáčením	zatáčení	k1gNnSc7	zatáčení
<g/>
,	,	kIx,	,
otáčením	otáčení	k1gNnSc7	otáčení
atd	atd	kA	atd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
autobusové	autobusový	k2eAgInPc1d1	autobusový
přívěsy	přívěs	k1gInPc1	přívěs
–	–	k?	–
Nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
polské	polský	k2eAgInPc4d1	polský
autobusové	autobusový	k2eAgInPc4d1	autobusový
přívěsy	přívěs	k1gInPc4	přívěs
Jelcz	Jelcz	k1gInSc1	Jelcz
(	(	kIx(	(
<g/>
prosklený	prosklený	k2eAgInSc1d1	prosklený
dvouosý	dvouosý	k2eAgInSc1d1	dvouosý
přívěs	přívěs	k1gInSc1	přívěs
se	s	k7c7	s
sedačkami	sedačka	k1gFnPc7	sedačka
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
z	z	k7c2	z
komponent	komponenta	k1gFnPc2	komponenta
licenční	licenční	k2eAgFnSc2d1	licenční
výroby	výroba	k1gFnSc2	výroba
autobusů	autobus	k1gInPc2	autobus
RTO	RTO	kA	RTO
<g/>
)	)	kIx)	)
za	za	k7c4	za
autobusy	autobus	k1gInPc4	autobus
Škoda	škoda	k6eAd1	škoda
706	[number]	k4	706
RO	RO	kA	RO
a	a	k8xC	a
RTO	RTO	kA	RTO
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
autobusy	autobus	k1gInPc1	autobus
Karosa	karosa	k1gFnSc1	karosa
ŠL	ŠL	kA	ŠL
11	[number]	k4	11
později	pozdě	k6eAd2	pozdě
prvními	první	k4xOgInPc7	první
linkovými	linkový	k2eAgInPc7d1	linkový
kloubovými	kloubový	k2eAgInPc7d1	kloubový
autobusy	autobus	k1gInPc7	autobus
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
Karosa	karosa	k1gFnSc1	karosa
C	C	kA	C
744	[number]	k4	744
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
autobusové	autobusový	k2eAgInPc4d1	autobusový
přívěsy	přívěs	k1gInPc4	přívěs
např.	např.	kA	např.
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
připojené	připojený	k2eAgInPc4d1	připojený
za	za	k7c4	za
trolejbusy	trolejbus	k1gInPc4	trolejbus
<g/>
.	.	kIx.	.
<g/>
autobusové	autobusový	k2eAgInPc1d1	autobusový
návěsy	návěs	k1gInPc1	návěs
–	–	k?	–
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byl	být	k5eAaImAgInS	být
provozován	provozovat	k5eAaImNgInS	provozovat
patrně	patrně	k6eAd1	patrně
jen	jen	k6eAd1	jen
prototyp	prototyp	k1gInSc4	prototyp
autobusového	autobusový	k2eAgInSc2d1	autobusový
návěsu	návěs	k1gInSc2	návěs
postaveného	postavený	k2eAgInSc2d1	postavený
na	na	k7c6	na
základě	základ	k1gInSc6	základ
autobusu	autobus	k1gInSc2	autobus
RTO	RTO	kA	RTO
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tahač	tahač	k1gInSc1	tahač
sloužil	sloužit	k5eAaImAgInS	sloužit
dvounápravový	dvounápravový	k2eAgInSc1d1	dvounápravový
sedlový	sedlový	k2eAgInSc1d1	sedlový
tahač	tahač	k1gInSc1	tahač
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
<g/>
.	.	kIx.	.
<g/>
turistické	turistický	k2eAgFnSc2d1	turistická
vláčky	vláčka	k1gFnSc2	vláčka
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Turistické	turistický	k2eAgInPc1d1	turistický
vláčky	vláček	k1gInPc1	vláček
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
stavěny	stavit	k5eAaImNgInP	stavit
na	na	k7c6	na
upravených	upravený	k2eAgFnPc6d1	upravená
dodávkách	dodávka	k1gFnPc6	dodávka
s	s	k7c7	s
cca	cca	kA	cca
jedním	jeden	k4xCgInSc7	jeden
až	až	k6eAd1	až
dvěma	dva	k4xCgInPc7	dva
přívěsy	přívěs	k1gInPc7	přívěs
(	(	kIx(	(
<g/>
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
jízda	jízda	k1gFnSc1	jízda
městem	město	k1gNnSc7	město
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
motorovým	motorový	k2eAgNnSc7d1	motorové
vozidlem	vozidlo	k1gNnSc7	vozidlo
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
historické	historický	k2eAgFnSc2d1	historická
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
táhne	táhnout	k5eAaImIp3nS	táhnout
obvykle	obvykle	k6eAd1	obvykle
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
čtyři	čtyři	k4xCgInPc4	čtyři
přívěsy	přívěs	k1gInPc4	přívěs
(	(	kIx(	(
<g/>
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
jízda	jízda	k1gFnSc1	jízda
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
jízda	jízda	k1gFnSc1	jízda
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Plitvická	Plitvická	k1gFnSc1	Plitvická
jezera	jezero	k1gNnSc2	jezero
jezdí	jezdit	k5eAaImIp3nS	jezdit
velkokapacitní	velkokapacitní	k2eAgFnPc4d1	velkokapacitní
soupravy	souprava	k1gFnPc4	souprava
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
nákladním	nákladní	k2eAgInSc7d1	nákladní
vozem	vůz	k1gInSc7	vůz
Mercedes	mercedes	k1gInSc1	mercedes
Unimog	Unimoga	k1gFnPc2	Unimoga
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
dvouosými	dvouosý	k2eAgInPc7d1	dvouosý
přívěsy	přívěs	k1gInPc7	přívěs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
použití	použití	k1gNnSc2	použití
===	===	k?	===
</s>
</p>
<p>
<s>
autobusy	autobus	k1gInPc1	autobus
městské	městský	k2eAgInPc1d1	městský
–	–	k?	–
Jsou	být	k5eAaImIp3nP	být
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
intravilánu	intravilán	k1gInSc6	intravilán
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
dveří	dveře	k1gFnPc2	dveře
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
stání	stání	k1gNnSc3	stání
+	+	kIx~	+
madla	madla	k1gNnPc1	madla
a	a	k8xC	a
tyče	tyč	k1gFnPc1	tyč
na	na	k7c6	na
držení	držení	k1gNnSc6	držení
za	za	k7c4	za
jízdy	jízda	k1gFnPc4	jízda
<g/>
,	,	kIx,	,
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
sedačky	sedačka	k1gFnPc4	sedačka
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
menší	malý	k2eAgInSc4d2	menší
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
kočárek	kočárek	k1gInSc4	kočárek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jako	jako	k9	jako
nízkopodlažní	nízkopodlažní	k2eAgFnPc1d1	nízkopodlažní
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
low	low	k?	low
floor	floor	k1gInSc1	floor
–	–	k?	–
LF	LF	kA	LF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
budou	být	k5eAaImBp3nP	být
všechna	všechen	k3xTgNnPc4	všechen
městská	městský	k2eAgNnPc4d1	Městské
vozidla	vozidlo	k1gNnPc4	vozidlo
pouze	pouze	k6eAd1	pouze
nízkopodlažní	nízkopodlažní	k2eAgMnSc1d1	nízkopodlažní
(	(	kIx(	(
<g/>
trend	trend	k1gInSc1	trend
i	i	k8xC	i
nařízení	nařízení	k1gNnSc1	nařízení
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Standardem	standard	k1gInSc7	standard
se	se	k3xPyFc4	se
také	také	k9	také
stává	stávat	k5eAaImIp3nS	stávat
zavádění	zavádění	k1gNnSc1	zavádění
mnoha	mnoho	k4c2	mnoho
informačních	informační	k2eAgInPc2d1	informační
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
světelných	světelný	k2eAgInPc2d1	světelný
panelů	panel	k1gInPc2	panel
a	a	k8xC	a
zvukových	zvukový	k2eAgInPc2d1	zvukový
hlásičů	hlásič	k1gInPc2	hlásič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
výměnu	výměna	k1gFnSc4	výměna
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
3	[number]	k4	3
dveře	dveře	k1gFnPc1	dveře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
přepravného	přepravné	k1gNnSc2	přepravné
neprovádí	provádět	k5eNaImIp3nS	provádět
řidič	řidič	k1gMnSc1	řidič
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
)	)	kIx)	)
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
označováním	označování	k1gNnSc7	označování
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
čipovými	čipový	k2eAgInPc7d1	čipový
systémy	systém	k1gInPc7	systém
<g/>
;	;	kIx,	;
autobusy	autobus	k1gInPc7	autobus
zvládají	zvládat	k5eAaImIp3nP	zvládat
rychlejší	rychlý	k2eAgInPc1d2	rychlejší
rozjezdy	rozjezd	k1gInPc1	rozjezd
a	a	k8xC	a
brzdění	brzdění	k1gNnSc1	brzdění
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
mají	mít	k5eAaImIp3nP	mít
automatickou	automatický	k2eAgFnSc4d1	automatická
převodovku	převodovka	k1gFnSc4	převodovka
s	s	k7c7	s
retardérem	retardér	k1gInSc7	retardér
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
bývá	bývat	k5eAaImIp3nS	bývat
75	[number]	k4	75
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
<g/>
autobusy	autobus	k1gInPc1	autobus
příměstské	příměstský	k2eAgInPc1d1	příměstský
(	(	kIx(	(
<g/>
linkové	linkový	k2eAgInPc1d1	linkový
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInPc1d1	regionální
<g/>
)	)	kIx)	)
–	–	k?	–
Určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
blízkostech	blízkost	k1gFnPc6	blízkost
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
nebo	nebo	k8xC	nebo
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dopravní	dopravní	k2eAgFnSc4d1	dopravní
obslužnost	obslužnost	k1gFnSc4	obslužnost
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
manuální	manuální	k2eAgFnSc7d1	manuální
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
převodovkou	převodovka	k1gFnSc7	převodovka
(	(	kIx(	(
<g/>
úspora	úspora	k1gFnSc1	úspora
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc1d2	vyšší
rychlost	rychlost	k1gFnSc1	rychlost
–	–	k?	–
většinou	většinou	k6eAd1	většinou
maximálně	maximálně	k6eAd1	maximálně
90	[number]	k4	90
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
autobusu	autobus	k1gInSc2	autobus
se	se	k3xPyFc4	se
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
předními	přední	k2eAgFnPc7d1	přední
dveřmi	dveře	k1gFnPc7	dveře
<g/>
,	,	kIx,	,
jízdenky	jízdenka	k1gFnPc4	jízdenka
vydává	vydávat	k5eAaImIp3nS	vydávat
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
<g/>
)	)	kIx)	)
řidič	řidič	k1gMnSc1	řidič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
low	low	k?	low
entry	entr	k1gInPc1	entr
–	–	k?	–
(	(	kIx(	(
<g/>
low	low	k?	low
entry	entra	k1gFnSc2	entra
=	=	kIx~	=
nízký	nízký	k2eAgInSc1d1	nízký
vstup	vstup	k1gInSc1	vstup
<g/>
)	)	kIx)	)
Úplnou	úplný	k2eAgFnSc7d1	úplná
novinkou	novinka	k1gFnSc7	novinka
je	být	k5eAaImIp3nS	být
řešení	řešení	k1gNnSc1	řešení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
městský	městský	k2eAgInSc1d1	městský
nízkopodlažní	nízkopodlažní	k2eAgInSc1d1	nízkopodlažní
a	a	k8xC	a
příměstský	příměstský	k2eAgInSc1d1	příměstský
středně	středně	k6eAd1	středně
podlažní	podlažní	k2eAgInSc1d1	podlažní
autobus	autobus	k1gInSc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
obsluhu	obsluha	k1gFnSc4	obsluha
blízkého	blízký	k2eAgNnSc2d1	blízké
okolí	okolí	k1gNnSc2	okolí
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
sedadel	sedadlo	k1gNnPc2	sedadlo
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
snížený	snížený	k2eAgInSc4d1	snížený
vstup	vstup	k1gInSc4	vstup
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc4	možnost
přepravy	přeprava	k1gFnSc2	přeprava
invalidních	invalidní	k2eAgFnPc2d1	invalidní
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převodovku	převodovka	k1gFnSc4	převodovka
manuální	manuální	k2eAgFnSc4d1	manuální
či	či	k8xC	či
automatickou	automatický	k2eAgFnSc4d1	automatická
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
autobusu	autobus	k1gInSc2	autobus
je	být	k5eAaImIp3nS	být
nízkopodlažní	nízkopodlažní	k2eAgFnSc1d1	nízkopodlažní
až	až	k9	až
po	po	k7c4	po
prostřední	prostřední	k2eAgFnPc4d1	prostřední
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
podlaha	podlaha	k1gFnSc1	podlaha
zvedne	zvednout	k5eAaPmIp3nS	zvednout
na	na	k7c4	na
standardní	standardní	k2eAgFnSc4d1	standardní
středněpodlažní	středněpodlažní	k2eAgFnSc4d1	středněpodlažní
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgNnSc2	tento
řešení	řešení	k1gNnSc2	řešení
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
cena	cena	k1gFnSc1	cena
oproti	oproti	k7c3	oproti
nízkopodlažnímu	nízkopodlažní	k2eAgInSc3d1	nízkopodlažní
autobusu	autobus	k1gInSc3	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
autobusů	autobus	k1gInPc2	autobus
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
zkratkou	zkratka	k1gFnSc7	zkratka
LE	LE	kA	LE
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
český	český	k2eAgMnSc1d1	český
SOR	SOR	kA	SOR
<g/>
)	)	kIx)	)
jej	on	k3xPp3gInSc4	on
z	z	k7c2	z
marketingových	marketingový	k2eAgInPc2d1	marketingový
důvodů	důvod	k1gInPc2	důvod
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
částečně	částečně	k6eAd1	částečně
nízkopodlažní	nízkopodlažní	k2eAgInPc4d1	nízkopodlažní
<g/>
.	.	kIx.	.
<g/>
autobusy	autobus	k1gInPc4	autobus
meziměstské	meziměstský	k2eAgInPc4d1	meziměstský
(	(	kIx(	(
<g/>
dálkové	dálkový	k2eAgInPc4d1	dálkový
<g/>
)	)	kIx)	)
–	–	k?	–
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
autobusy	autobus	k1gInPc1	autobus
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
a	a	k8xC	a
obcemi	obec	k1gFnPc7	obec
<g/>
,	,	kIx,	,
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
150	[number]	k4	150
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
jedny	jeden	k4xCgFnPc1	jeden
nebo	nebo	k8xC	nebo
dvoje	dvoje	k4xRgFnPc1	dvoje
dveře	dveře	k1gFnPc1	dveře
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
povoleno	povolit	k5eAaPmNgNnS	povolit
stání	stání	k1gNnSc1	stání
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
autobusy	autobus	k1gInPc1	autobus
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnPc4d2	vyšší
rychlosti	rychlost	k1gFnPc4	rychlost
(	(	kIx(	(
<g/>
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
výkonný	výkonný	k2eAgInSc4d1	výkonný
motor	motor	k1gInSc4	motor
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
nikdy	nikdy	k6eAd1	nikdy
kloubové	kloubový	k2eAgFnPc1d1	kloubová
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
klasické	klasický	k2eAgInPc4d1	klasický
12	[number]	k4	12
<g/>
metrové	metrový	k2eAgInPc1d1	metrový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
třínápravové	třínápravový	k2eAgFnPc1d1	třínápravová
15	[number]	k4	15
<g/>
metrové	metrový	k2eAgFnPc1d1	metrová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
mají	mít	k5eAaImIp3nP	mít
předepsány	předepsán	k2eAgInPc1d1	předepsán
nové	nový	k2eAgInPc1d1	nový
dálkové	dálkový	k2eAgInPc1d1	dálkový
autobusy	autobus	k1gInPc1	autobus
taktéž	taktéž	k?	taktéž
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
pásy	pás	k1gInPc1	pás
jako	jako	k8xS	jako
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
automobilu	automobil	k1gInSc6	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
bývají	bývat	k5eAaImIp3nP	bývat
vybaveny	vybavit	k5eAaPmNgInP	vybavit
záchodem	záchod	k1gInSc7	záchod
<g/>
,	,	kIx,	,
nápojovým	nápojový	k2eAgInSc7d1	nápojový
automatem	automat	k1gInSc7	automat
<g/>
,	,	kIx,	,
přístroji	přístroj	k1gInPc7	přístroj
na	na	k7c4	na
pouštění	pouštění	k1gNnSc4	pouštění
filmu	film	k1gInSc2	film
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
klimatizací	klimatizace	k1gFnSc7	klimatizace
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
neexistuje	existovat	k5eNaImIp3nS	existovat
ostrá	ostrý	k2eAgFnSc1d1	ostrá
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
dálkovými	dálkový	k2eAgInPc7d1	dálkový
a	a	k8xC	a
turistickými	turistický	k2eAgInPc7d1	turistický
autobusy	autobus	k1gInPc7	autobus
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
turistické	turistický	k2eAgFnPc1d1	turistická
luxusnější	luxusní	k2eAgFnPc1d2	luxusnější
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dálkové	dálkový	k2eAgFnSc2d1	dálková
vyrovnaly	vyrovnat	k5eAaBmAgInP	vyrovnat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
autokary	autokar	k1gInPc4	autokar
(	(	kIx(	(
<g/>
turistické	turistický	k2eAgInPc1d1	turistický
autobusy	autobus	k1gInPc1	autobus
<g/>
)	)	kIx)	)
–	–	k?	–
Jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
nelinkový	linkový	k2eNgInSc4d1	linkový
provoz	provoz	k1gInSc4	provoz
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
i	i	k9	i
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
trasy	trasa	k1gFnPc4	trasa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
objednávek	objednávka	k1gFnPc2	objednávka
na	na	k7c6	na
zájezdech	zájezd	k1gInPc6	zájezd
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
trasách	trasa	k1gFnPc6	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukcí	konstrukce	k1gFnSc7	konstrukce
jsou	být	k5eAaImIp3nP	být
shodné	shodný	k2eAgInPc1d1	shodný
buďto	buďto	k8xC	buďto
s	s	k7c7	s
autobusy	autobus	k1gInPc7	autobus
meziměstskými	meziměstský	k2eAgInPc7d1	meziměstský
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dálkovými	dálkový	k2eAgMnPc7d1	dálkový
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
záchodem	záchod	k1gInSc7	záchod
<g/>
,	,	kIx,	,
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
větším	veliký	k2eAgInSc7d2	veliký
prostorem	prostor	k1gInSc7	prostor
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
a	a	k8xC	a
zavazadla	zavazadlo	k1gNnPc4	zavazadlo
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
táhnout	táhnout	k5eAaImF	táhnout
přívěs	přívěs	k1gInSc4	přívěs
(	(	kIx(	(
<g/>
na	na	k7c4	na
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
jízdní	jízdní	k2eAgNnPc1d1	jízdní
kola	kolo	k1gNnPc1	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mít	mít	k5eAaImF	mít
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
instalovanou	instalovaný	k2eAgFnSc4d1	instalovaná
skříň	skříň	k1gFnSc4	skříň
na	na	k7c4	na
lyže	lyže	k1gFnPc4	lyže
<g/>
.	.	kIx.	.
</s>
<s>
ČESMAD	ČESMAD	kA	ČESMAD
Bohemia	bohemia	k1gFnSc1	bohemia
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
turistické	turistický	k2eAgInPc4d1	turistický
autobusy	autobus	k1gInPc4	autobus
podle	podle	k7c2	podle
32	[number]	k4	32
kritérií	kritérion	k1gNnPc2	kritérion
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
kategorií	kategorie	k1gFnPc2	kategorie
(	(	kIx(	(
<g/>
autokar	autokar	k1gInSc1	autokar
<g/>
,	,	kIx,	,
výletní	výletní	k2eAgInSc1d1	výletní
autokar	autokar	k1gInSc1	autokar
<g/>
,	,	kIx,	,
turistický	turistický	k2eAgInSc1d1	turistický
autokar	autokar	k1gInSc1	autokar
a	a	k8xC	a
luxusní	luxusní	k2eAgInSc1d1	luxusní
turistický	turistický	k2eAgInSc1d1	turistický
autokar	autokar	k1gInSc1	autokar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozhodující	rozhodující	k2eAgInPc1d1	rozhodující
pro	pro	k7c4	pro
zařazení	zařazení	k1gNnSc4	zařazení
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
standard	standard	k1gInSc1	standard
pohodlí	pohodlí	k1gNnSc2	pohodlí
(	(	kIx(	(
<g/>
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
nastavitelnost	nastavitelnost	k1gFnSc1	nastavitelnost
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
kapacita	kapacita	k1gFnSc1	kapacita
zavazadlového	zavazadlový	k2eAgInSc2d1	zavazadlový
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
další	další	k2eAgFnSc1d1	další
výbava	výbava	k1gFnSc1	výbava
(	(	kIx(	(
<g/>
klimatizace	klimatizace	k1gFnSc1	klimatizace
<g/>
,	,	kIx,	,
WC	WC	kA	WC
<g/>
,	,	kIx,	,
videopřehrávač	videopřehrávač	k1gInSc1	videopřehrávač
<g/>
,	,	kIx,	,
ohřívač	ohřívač	k1gInSc1	ohřívač
jídla	jídlo	k1gNnSc2	jídlo
<g/>
)	)	kIx)	)
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
zařazení	zařazení	k1gNnSc4	zařazení
vliv	vliv	k1gInSc1	vliv
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
vyšších	vysoký	k2eAgFnPc2d2	vyšší
kategorií	kategorie	k1gFnPc2	kategorie
doporučené	doporučený	k2eAgInPc1d1	doporučený
<g/>
;	;	kIx,	;
chladnička	chladnička	k1gFnSc1	chladnička
je	být	k5eAaImIp3nS	být
vyžadována	vyžadovat	k5eAaImNgFnS	vyžadovat
pro	pro	k7c4	pro
luxusní	luxusní	k2eAgInSc4d1	luxusní
turistický	turistický	k2eAgInSc4d1	turistický
autokar	autokar	k1gInSc4	autokar
<g/>
.	.	kIx.	.
<g/>
hotelbusy	hotelbus	k1gInPc1	hotelbus
–	–	k?	–
Tyto	tento	k3xDgInPc4	tento
autobusy	autobus	k1gInPc4	autobus
jsou	být	k5eAaImIp3nP	být
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
nejdelších	dlouhý	k2eAgFnPc6d3	nejdelší
trasách	trasa	k1gFnPc6	trasa
-	-	kIx~	-
často	často	k6eAd1	často
až	až	k9	až
5000	[number]	k4	5000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgFnPc4d1	speciální
dálkové	dálkový	k2eAgFnPc4d1	dálková
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
patrové	patrový	k2eAgInPc1d1	patrový
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
uvnitř	uvnitř	k7c2	uvnitř
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
,	,	kIx,	,
koupelnu	koupelna	k1gFnSc4	koupelna
a	a	k8xC	a
spací	spací	k2eAgFnPc4d1	spací
kóje	kóje	k1gFnPc4	kóje
s	s	k7c7	s
lůžky	lůžko	k1gNnPc7	lůžko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
sedačky	sedačka	k1gFnPc1	sedačka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sklopit	sklopit	k5eAaPmF	sklopit
a	a	k8xC	a
přestavět	přestavět	k5eAaPmF	přestavět
na	na	k7c4	na
postele	postel	k1gFnPc4	postel
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
variantou	varianta	k1gFnSc7	varianta
jsou	být	k5eAaImIp3nP	být
různá	různý	k2eAgNnPc1d1	různé
obytná	obytný	k2eAgNnPc1d1	obytné
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
luxusní	luxusní	k2eAgNnSc4d1	luxusní
provedení	provedení	k1gNnSc4	provedení
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnSc4	sloužící
jako	jako	k8xC	jako
zázemí	zázemí	k1gNnSc4	zázemí
např.	např.	kA	např.
hudebním	hudební	k2eAgFnPc3d1	hudební
skupinám	skupina	k1gFnPc3	skupina
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Přeprava	přeprava	k1gFnSc1	přeprava
spících	spící	k2eAgMnPc2d1	spící
cestujících	cestující	k1gMnPc2	cestující
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
<g/>
autobusy	autobus	k1gInPc4	autobus
školní	školní	k2eAgFnSc2d1	školní
–	–	k?	–
Jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgInPc4d2	menší
rozestupy	rozestup	k1gInPc4	rozestup
mezi	mezi	k7c7	mezi
sedadly	sedadlo	k1gNnPc7	sedadlo
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
řešen	řešen	k2eAgInSc4d1	řešen
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
školní	školní	k2eAgFnPc4d1	školní
brašny	brašna	k1gFnPc4	brašna
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
představitelem	představitel	k1gMnSc7	představitel
je	být	k5eAaImIp3nS	být
Karosa	karosa	k1gFnSc1	karosa
Récréoautobusy	Récréoautobus	k1gInPc1	Récréoautobus
speciální	speciální	k2eAgInSc1d1	speciální
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
účelové	účelový	k2eAgFnSc6d1	účelová
(	(	kIx(	(
<g/>
např.	např.	kA	např.
letištní	letištní	k2eAgInPc1d1	letištní
<g/>
)	)	kIx)	)
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
terénní	terénní	k2eAgInPc1d1	terénní
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
nákladní	nákladní	k2eAgInPc4d1	nákladní
automobily	automobil	k1gInPc4	automobil
přestavěné	přestavěný	k2eAgNnSc1d1	přestavěné
použitím	použití	k1gNnSc7	použití
nástavby	nástavba	k1gFnSc2	nástavba
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
přepravníky	přepravník	k1gInPc4	přepravník
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Značky	značka	k1gFnPc1	značka
a	a	k8xC	a
typy	typ	k1gInPc1	typ
autobusů	autobus	k1gInPc2	autobus
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Výrobci	výrobce	k1gMnPc1	výrobce
autobusů	autobus	k1gInPc2	autobus
===	===	k?	===
</s>
</p>
<p>
<s>
ACEV	ACEV	kA	ACEV
(	(	kIx(	(
<g/>
karosárna	karosárna	k1gFnSc1	karosárna
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
podvozky	podvozek	k1gInPc7	podvozek
Mercedes-Benz	Mercedes-Benza	k1gFnPc2	Mercedes-Benza
<g/>
,	,	kIx,	,
MAN	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
Renault	renault	k1gInSc1	renault
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Autosan	Autosan	k1gMnSc1	Autosan
</s>
</p>
<p>
<s>
Durisotti	Durisott	k5eAaImF	Durisott
(	(	kIx(	(
<g/>
PSA	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
Fiat	fiat	k1gInSc1	fiat
<g/>
,	,	kIx,	,
Renault	renault	k1gInSc1	renault
Trucks	Trucks	k1gInSc1	Trucks
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Auwärter	Auwärter	k1gMnSc1	Auwärter
(	(	kIx(	(
<g/>
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Evobus	Evobus	k1gMnSc1	Evobus
(	(	kIx(	(
<g/>
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
<g/>
,	,	kIx,	,
Setra	setr	k1gMnSc2	setr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fast	Fast	k2eAgMnSc1d1	Fast
Concept	Concept	k1gMnSc1	Concept
Car	car	k1gMnSc1	car
(	(	kIx(	(
<g/>
Renault	renault	k1gInSc1	renault
<g/>
,	,	kIx,	,
Mercedes-Benz	Mercedes-Benz	k1gInSc1	Mercedes-Benz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gruau	Gruau	k6eAd1	Gruau
</s>
</p>
<p>
<s>
Heuliez	Heuliez	k1gMnSc1	Heuliez
</s>
</p>
<p>
<s>
Ikarus	Ikarus	k1gMnSc1	Ikarus
</s>
</p>
<p>
<s>
Iveco	Iveco	k6eAd1	Iveco
Bus	bus	k1gInSc1	bus
(	(	kIx(	(
<g/>
Renault	renault	k1gInSc1	renault
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Iveco	Iveco	k1gNnSc1	Iveco
<g/>
,	,	kIx,	,
Matra	Matra	k1gFnSc1	Matra
<g/>
,	,	kIx,	,
Ikarus	Ikarus	k1gMnSc1	Ikarus
<g/>
,	,	kIx,	,
Iveco	Iveco	k1gMnSc1	Iveco
Czech	Czecha	k1gFnPc2	Czecha
Republic	Republice	k1gFnPc2	Republice
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Karosa	karosa	k1gFnSc1	karosa
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
Jelcz	Jelcz	k1gMnSc1	Jelcz
</s>
</p>
<p>
<s>
MAVE	MAVE	kA	MAVE
</s>
</p>
<p>
<s>
LAZ	lazit	k5eAaImRp2nS	lazit
</s>
</p>
<p>
<s>
LiAZ	liaz	k1gInSc1	liaz
</s>
</p>
<p>
<s>
MAN	Man	k1gMnSc1	Man
</s>
</p>
<p>
<s>
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
</s>
</p>
<p>
<s>
Neoplan	Neoplan	k1gMnSc1	Neoplan
</s>
</p>
<p>
<s>
Novabus	Novabus	k1gMnSc1	Novabus
</s>
</p>
<p>
<s>
Renault	renault	k1gInSc1	renault
Trucks	Trucksa	k1gFnPc2	Trucksa
</s>
</p>
<p>
<s>
Scania	Scanium	k1gNnPc1	Scanium
</s>
</p>
<p>
<s>
Setra	setr	k1gMnSc4	setr
</s>
</p>
<p>
<s>
Solaris	Solaris	k1gFnSc1	Solaris
</s>
</p>
<p>
<s>
Solbus	Solbus	k1gMnSc1	Solbus
</s>
</p>
<p>
<s>
SOR	SOR	kA	SOR
</s>
</p>
<p>
<s>
Tedom	Tedom	k1gInSc1	Tedom
</s>
</p>
<p>
<s>
Temsa	Temsa	k1gFnSc1	Temsa
</s>
</p>
<p>
<s>
Van	van	k1gInSc1	van
Hool	Hoola	k1gFnPc2	Hoola
</s>
</p>
<p>
<s>
VDL	VDL	kA	VDL
Bova	Bova	k1gFnSc1	Bova
</s>
</p>
<p>
<s>
VehiXel	VehiXel	k1gMnSc1	VehiXel
</s>
</p>
<p>
<s>
Volvo	Volvo	k1gNnSc1	Volvo
</s>
</p>
<p>
<s>
===	===	k?	===
Historické	historický	k2eAgInPc1d1	historický
autobusy	autobus	k1gInPc1	autobus
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
NDO	NDO	kA	NDO
</s>
</p>
<p>
<s>
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
RO	RO	kA	RO
</s>
</p>
<p>
<s>
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
RTO	RTO	kA	RTO
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
ŠM	ŠM	kA	ŠM
11	[number]	k4	11
</s>
</p>
<p>
<s>
Ikarus	Ikarus	k1gMnSc1	Ikarus
280	[number]	k4	280
(	(	kIx(	(
<g/>
kloubový	kloubový	k2eAgMnSc1d1	kloubový
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tatra	Tatra	k1gFnSc1	Tatra
500	[number]	k4	500
HB	HB	kA	HB
</s>
</p>
<p>
<s>
===	===	k?	===
Současné	současný	k2eAgNnSc1d1	současné
městské	městský	k2eAgNnSc1d1	Městské
===	===	k?	===
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
731	[number]	k4	731
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
732	[number]	k4	732
a	a	k8xC	a
kloubový	kloubový	k2eAgInSc1d1	kloubový
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
741	[number]	k4	741
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
931	[number]	k4	931
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
932	[number]	k4	932
a	a	k8xC	a
kloubový	kloubový	k2eAgInSc1d1	kloubový
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
941	[number]	k4	941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
951	[number]	k4	951
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
952	[number]	k4	952
a	a	k8xC	a
kloubový	kloubový	k2eAgInSc1d1	kloubový
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
961	[number]	k4	961
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SOR	SOR	kA	SOR
B	B	kA	B
7,5	[number]	k4	7,5
<g/>
,	,	kIx,	,
B	B	kA	B
9,5	[number]	k4	9,5
a	a	k8xC	a
10,5	[number]	k4	10,5
</s>
</p>
<p>
<s>
SOR	SOR	kA	SOR
BN	BN	kA	BN
9,5	[number]	k4	9,5
<g/>
,	,	kIx,	,
BN	BN	kA	BN
10,5	[number]	k4	10,5
a	a	k8xC	a
BN	BN	kA	BN
12	[number]	k4	12
–	–	k?	–
částečně	částečně	k6eAd1	částečně
nízkopodlažní	nízkopodlažní	k2eAgFnSc1d1	nízkopodlažní
</s>
</p>
<p>
<s>
TEDOM	TEDOM	kA	TEDOM
Kronos	Kronos	k1gMnSc1	Kronos
123	[number]	k4	123
<g/>
,	,	kIx,	,
TEDOM	TEDOM	kA	TEDOM
C	C	kA	C
12	[number]	k4	12
</s>
</p>
<p>
<s>
Irisbus	Irisbus	k1gInSc1	Irisbus
Citybus	Citybus	k1gInSc1	Citybus
12M	[number]	k4	12M
–	–	k?	–
nejčastější	častý	k2eAgNnSc1d3	nejčastější
nízkopodlažní	nízkopodlažní	k2eAgNnSc1d1	nízkopodlažní
vozidlo	vozidlo	k1gNnSc1	vozidlo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
též	též	k9	též
kloubový	kloubový	k2eAgMnSc1d1	kloubový
Irisbus	Irisbus	k1gMnSc1	Irisbus
Citybus	Citybus	k1gMnSc1	Citybus
18	[number]	k4	18
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Irisbus	Irisbus	k1gInSc1	Irisbus
Citelis	Citelis	k1gFnSc1	Citelis
12M	[number]	k4	12M
–	–	k?	–
nástupce	nástupce	k1gMnSc1	nástupce
předešlého	předešlý	k2eAgInSc2d1	předešlý
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
též	též	k9	též
kloubový	kloubový	k2eAgInSc1d1	kloubový
Irisbus	Irisbus	k1gInSc1	Irisbus
Citelis	Citelis	k1gFnSc1	Citelis
18	[number]	k4	18
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Solaris	Solaris	k1gInSc1	Solaris
Urbino	Urbino	k1gNnSc1	Urbino
</s>
</p>
<p>
<s>
Ikarus	Ikarus	k1gMnSc1	Ikarus
E91	E91	k1gMnSc1	E91
</s>
</p>
<p>
<s>
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
Citaro	citara	k1gFnSc5	citara
</s>
</p>
<p>
<s>
MAN	Man	k1gMnSc1	Man
Lion	Lion	k1gMnSc1	Lion
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
City	City	k1gFnSc7	City
</s>
</p>
<p>
<s>
Volvo	Volvo	k1gNnSc1	Volvo
7700	[number]	k4	7700
</s>
</p>
<p>
<s>
Tam	tam	k6eAd1	tam
Bus	bus	k1gInSc1	bus
232	[number]	k4	232
</s>
</p>
<p>
<s>
===	===	k?	===
Linkové	linkový	k2eAgInPc1d1	linkový
autobusy	autobus	k1gInPc1	autobus
===	===	k?	===
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
C	C	kA	C
734	[number]	k4	734
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
C	C	kA	C
<g/>
735	[number]	k4	735
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
744	[number]	k4	744
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
934	[number]	k4	934
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
943	[number]	k4	943
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
954	[number]	k4	954
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
955	[number]	k4	955
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
956	[number]	k4	956
<g/>
,	,	kIx,	,
LC	LC	kA	LC
<g/>
956	[number]	k4	956
<g/>
,	,	kIx,	,
Recreo	Recreo	k1gMnSc1	Recreo
<g/>
,	,	kIx,	,
Axer	Axer	k1gMnSc1	Axer
12	[number]	k4	12
<g/>
m	m	kA	m
a	a	k8xC	a
12,8	[number]	k4	12,8
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SOR	SOR	kA	SOR
C	C	kA	C
(	(	kIx(	(
<g/>
typy	typ	k1gInPc7	typ
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
v	v	k7c6	v
metrech	metr	k1gInPc6	metr
<g/>
:	:	kIx,	:
7,5	[number]	k4	7,5
<g/>
;	;	kIx,	;
9,5	[number]	k4	9,5
<g/>
;	;	kIx,	;
10,5	[number]	k4	10,5
<g/>
;	;	kIx,	;
12	[number]	k4	12
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Novoplan	Novoplan	k1gInSc4	Novoplan
–	–	k?	–
slovenské	slovenský	k2eAgInPc4d1	slovenský
autobusy	autobus	k1gInPc4	autobus
z	z	k7c2	z
SAO	SAO	kA	SAO
Lučenec	Lučenec	k1gInSc1	Lučenec
</s>
</p>
<p>
<s>
Mercedes	mercedes	k1gInSc1	mercedes
Conecto	Conecto	k1gNnSc1	Conecto
–	–	k?	–
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
</s>
</p>
<p>
<s>
MAN	Man	k1gMnSc1	Man
–	–	k?	–
mnoho	mnoho	k4c4	mnoho
typů	typ	k1gInPc2	typ
</s>
</p>
<p>
<s>
Irisbus	Irisbus	k1gInSc1	Irisbus
Crossway	Crosswaa	k1gFnSc2	Crosswaa
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
Récréo	Récréo	k6eAd1	Récréo
</s>
</p>
<p>
<s>
===	===	k?	===
Low	Low	k1gMnSc2	Low
entry	entra	k1gMnSc2	entra
===	===	k?	===
</s>
</p>
<p>
<s>
Irisbus	Irisbus	k1gMnSc1	Irisbus
Crossway	Crosswaa	k1gMnSc2	Crosswaa
LE	LE	kA	LE
(	(	kIx(	(
<g/>
český	český	k2eAgMnSc1d1	český
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sor	Sor	k?	Sor
CN	CN	kA	CN
12	[number]	k4	12
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
i	i	k8xC	i
BN	BN	kA	BN
12	[number]	k4	12
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
český	český	k2eAgMnSc1d1	český
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Solaris	Solaris	k1gFnPc1	Solaris
Urbino	Urbino	k1gNnSc1	Urbino
12	[number]	k4	12
LE	LE	kA	LE
(	(	kIx(	(
<g/>
polský	polský	k2eAgMnSc1d1	polský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
Citaro	citara	k1gFnSc5	citara
LE	LE	kA	LE
(	(	kIx(	(
<g/>
německý	německý	k2eAgMnSc1d1	německý
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
často	často	k6eAd1	často
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PRE	PRE	kA	PRE
–	–	k?	–
OS	OS	kA	OS
Volvo	Volvo	k1gNnSc1	Volvo
(	(	kIx(	(
<g/>
slovenská	slovenský	k2eAgFnSc1d1	slovenská
karoserie	karoserie	k1gFnSc1	karoserie
<g/>
,	,	kIx,	,
polsko-švédský	polsko-švédský	k2eAgInSc1d1	polsko-švédský
podvozek	podvozek	k1gInSc1	podvozek
a	a	k8xC	a
pohon	pohon	k1gInSc1	pohon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dálkové	dálkový	k2eAgInPc1d1	dálkový
a	a	k8xC	a
turistické	turistický	k2eAgInPc1d1	turistický
autobusy	autobus	k1gInPc1	autobus
===	===	k?	===
</s>
</p>
<p>
<s>
Autosan	Autosan	k1gInSc4	Autosan
Gemini	Gemin	k1gMnPc1	Gemin
</s>
</p>
<p>
<s>
Autosan	Autosan	k1gMnSc1	Autosan
Eurolider	Eurolider	k1gMnSc1	Eurolider
</s>
</p>
<p>
<s>
Scania	Scanium	k1gNnPc4	Scanium
Irizar	Irizar	k1gInSc1	Irizar
</s>
</p>
<p>
<s>
Man	Man	k1gMnSc1	Man
Lion	Lion	k1gMnSc1	Lion
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Star	star	k1gFnSc7	star
</s>
</p>
<p>
<s>
Neoplan	Neoplan	k1gMnSc1	Neoplan
Starliner	Starliner	k1gMnSc1	Starliner
</s>
</p>
<p>
<s>
Ayats	Ayats	k6eAd1	Ayats
Atlantis	Atlantis	k1gFnSc1	Atlantis
</s>
</p>
<p>
<s>
Mercedes	mercedes	k1gInSc1	mercedes
Tourismo	Tourisma	k1gFnSc5	Tourisma
</s>
</p>
<p>
<s>
Mercedes	mercedes	k1gInSc1	mercedes
Travego	Travego	k6eAd1	Travego
</s>
</p>
<p>
<s>
Irisbus	Irisbus	k1gMnSc1	Irisbus
Illiade	Illiad	k1gInSc5	Illiad
</s>
</p>
<p>
<s>
Bova	Bov	k2eAgNnPc1d1	Bov
Futura	futurum	k1gNnPc1	futurum
</s>
</p>
<p>
<s>
Beulas	Beulas	k1gInSc1	Beulas
Aura	aura	k1gFnSc1	aura
</s>
</p>
<p>
<s>
Beulas	Beulas	k1gInSc1	Beulas
Glory	Glora	k1gFnSc2	Glora
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
LC	LC	kA	LC
735	[number]	k4	735
</s>
</p>
<p>
<s>
Karosa	karosa	k1gFnSc1	karosa
LC	LC	kA	LC
736	[number]	k4	736
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
<s>
Trolejbus	trolejbus	k1gInSc1	trolejbus
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc4	seznam
výrobců	výrobce	k1gMnPc2	výrobce
užitkových	užitkový	k2eAgInPc2d1	užitkový
automobilů	automobil	k1gInPc2	automobil
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
autobus	autobus	k1gInSc1	autobus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
autobus	autobus	k1gInSc4	autobus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
autobus	autobus	k1gInSc1	autobus
ve	v	k7c6	v
WikislovníkuCo	WikislovníkuCo	k6eAd1	WikislovníkuCo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
autobus	autobus	k1gInSc1	autobus
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
BUSportál	BUSportál	k1gInSc1	BUSportál
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
dabra	dabra	k1gMnSc1	dabra
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
BUSportál	BUSportál	k1gInSc1	BUSportál
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
internetový	internetový	k2eAgInSc1d1	internetový
magazín	magazín	k1gInSc1	magazín
o	o	k7c6	o
autobusech	autobus	k1gInPc6	autobus
a	a	k8xC	a
autobusové	autobusový	k2eAgFnSc6d1	autobusová
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
provozuje	provozovat	k5eAaImIp3nS	provozovat
ČSAD	ČSAD	kA	ČSAD
SVT	SVT	kA	SVT
Praha	Praha	k1gFnSc1	Praha
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
ČESMAD	ČESMAD	kA	ČESMAD
Bohemia	bohemia	k1gFnSc1	bohemia
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
subjekty	subjekt	k1gInPc7	subjekt
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
českými	český	k2eAgMnPc7d1	český
výrobci	výrobce	k1gMnPc7	výrobce
a	a	k8xC	a
dopravci	dopravce	k1gMnPc7	dopravce
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založen	založen	k2eAgMnSc1d1	založen
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
BUSportál	BUSportál	k1gInSc1	BUSportál
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
<g/>
,	,	kIx,	,
provozuje	provozovat	k5eAaImIp3nS	provozovat
SVT	SVT	kA	SVT
Slovakia	Slovakia	k1gFnSc1	Slovakia
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
od	od	k7c2	od
českého	český	k2eAgInSc2d1	český
BUSportálu	BUSportál	k1gInSc2	BUSportál
odštěpen	odštěpen	k2eAgInSc1d1	odštěpen
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Autobusové	autobusový	k2eAgFnPc1d1	autobusová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
on-line	onin	k1gInSc5	on-lin
deník	deník	k1gInSc1	deník
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaPmIp3nS	vydávat
Daniel	Daniel	k1gMnSc1	Daniel
Srb	Srb	k1gMnSc1	Srb
Group	Group	k1gMnSc1	Group
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISSN	ISSN	kA	ISSN
1802-1107	[number]	k4	1802-1107
</s>
</p>
<p>
<s>
Iveco	Iveco	k1gMnSc1	Iveco
Czech	Czech	k1gMnSc1	Czech
republic	republice	k1gFnPc2	republice
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Karosa	karosa	k1gFnSc1	karosa
</s>
</p>
<p>
<s>
SOR	SOR	kA	SOR
<g/>
,	,	kIx,	,
web	web	k1gInSc1	web
SOR	SOR	kA	SOR
Libchavy	Libchava	k1gFnSc2	Libchava
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
dceřiných	dceřin	k2eAgFnPc2d1	dceřina
firem	firma	k1gFnPc2	firma
</s>
</p>
<p>
<s>
Solaris	Solaris	k1gInSc1	Solaris
Bus	bus	k1gInSc1	bus
&	&	k?	&
Coach	Coach	k1gInSc1	Coach
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
výrobce	výrobce	k1gMnSc1	výrobce
autobusů	autobus	k1gInPc2	autobus
</s>
</p>
<p>
<s>
Typy	typ	k1gInPc1	typ
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
nedatován	datován	k2eNgInSc1d1	datován
<g/>
,	,	kIx,	,
Stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
Městské	městský	k2eAgFnSc6d1	městská
Hromadné	hromadný	k2eAgFnSc6d1	hromadná
Dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
Tonda	Tonda	k1gMnSc1	Tonda
Ježek	Ježek	k1gMnSc1	Ježek
a	a	k8xC	a
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
Havlas	Havlas	k1gMnSc1	Havlas
(	(	kIx(	(
<g/>
ELfkaM	ELfkaM	k1gMnSc1	ELfkaM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Neoplan	Neoplan	k1gMnSc1	Neoplan
<g/>
.	.	kIx.	.
<g/>
info	info	k1gMnSc1	info
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
zájezdových	zájezdový	k2eAgInPc2d1	zájezdový
autobusů	autobus	k1gInPc2	autobus
Neoplan	Neoplana	k1gFnPc2	Neoplana
<g/>
,	,	kIx,	,
technické	technický	k2eAgInPc1d1	technický
popisy	popis	k1gInPc1	popis
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
anonymní	anonymní	k2eAgFnSc2d1	anonymní
skupiny	skupina	k1gFnSc2	skupina
označené	označený	k2eAgFnSc2d1	označená
NEOPLAN	NEOPLAN	kA	NEOPLAN
<g/>
.	.	kIx.	.
<g/>
INFO	INFO	kA	INFO
team	team	k1gInSc1	team
</s>
</p>
<p>
<s>
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
evidence	evidence	k1gFnPc4	evidence
českých	český	k2eAgMnPc2d1	český
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
,	,	kIx,	,
seznam-autobusu	seznamutobus	k1gInSc2	seznam-autobus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
EvoBus	EvoBus	k1gInSc1	EvoBus
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
Mercedes-Benz	Mercedes-Benza	k1gFnPc2	Mercedes-Benza
a	a	k8xC	a
Setra	setr	k1gMnSc2	setr
</s>
</p>
