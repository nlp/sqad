<s>
Piñ	Piñ	k?	Piñ
Colada	Colada	k1gFnSc1	Colada
je	být	k5eAaImIp3nS	být
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
pitý	pitý	k2eAgInSc4d1	pitý
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
<g/>
,	,	kIx,	,
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
varianta	varianta	k1gFnSc1	varianta
bez	bez	k7c2	bez
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Virgin	Virgin	k1gInSc1	Virgin
Colada	Colada	k1gFnSc1	Colada
<g/>
.	.	kIx.	.
</s>
<s>
Piñ	Piñ	k?	Piñ
Colada	Colada	k1gFnSc1	Colada
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bílý	bílý	k2eAgInSc4d1	bílý
rum	rum	k1gInSc4	rum
<g/>
,	,	kIx,	,
kokosové	kokosový	k2eAgNnSc4d1	kokosové
mléko	mléko	k1gNnSc4	mléko
a	a	k8xC	a
šťávu	šťáva	k1gFnSc4	šťáva
z	z	k7c2	z
plodů	plod	k1gInPc2	plod
ananasu	ananas	k1gInSc2	ananas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
připravuje	připravovat	k5eAaImIp3nS	připravovat
z	z	k7c2	z
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
kokosu	kokos	k1gInSc2	kokos
a	a	k8xC	a
ananasu	ananas	k1gInSc2	ananas
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
rozmixují	rozmixovat	k5eAaPmIp3nP	rozmixovat
v	v	k7c6	v
mixéru	mixér	k1gInSc6	mixér
a	a	k8xC	a
následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
ve	v	k7c6	v
sklenici	sklenice	k1gFnSc6	sklenice
<g/>
,	,	kIx,	,
doplní	doplnit	k5eAaPmIp3nS	doplnit
o	o	k7c4	o
bílý	bílý	k2eAgInSc4d1	bílý
rum	rum	k1gInSc4	rum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
kokos	kokos	k1gInSc1	kokos
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
kokosovým	kokosový	k2eAgInSc7d1	kokosový
sirupem	sirup	k1gInSc7	sirup
a	a	k8xC	a
ananas	ananas	k1gInSc1	ananas
zase	zase	k9	zase
ananasovým	ananasový	k2eAgInSc7d1	ananasový
džusem	džus	k1gInSc7	džus
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
variantou	varianta	k1gFnSc7	varianta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
kokosový	kokosový	k2eAgInSc1d1	kokosový
sirup	sirup	k1gInSc1	sirup
doplněn	doplnit	k5eAaPmNgInS	doplnit
či	či	k8xC	či
zcela	zcela	k6eAd1	zcela
nahrazen	nahradit	k5eAaPmNgInS	nahradit
smetanou	smetana	k1gFnSc7	smetana
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ještě	ještě	k6eAd1	ještě
existuje	existovat	k5eAaImIp3nS	existovat
varianta	varianta	k1gFnSc1	varianta
Malibu	Malib	k1gInSc2	Malib
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
rum	rum	k1gInSc1	rum
s	s	k7c7	s
kokosovým	kokosový	k2eAgInSc7d1	kokosový
sirupem	sirup	k1gInSc7	sirup
<g/>
)	)	kIx)	)
+	+	kIx~	+
ananasový	ananasový	k2eAgInSc1d1	ananasový
džus	džus	k1gInSc1	džus
+	+	kIx~	+
smetana	smetana	k1gFnSc1	smetana
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pravou	pravá	k1gFnSc7	pravá
Piňa	Piňum	k1gNnSc2	Piňum
coladou	colást	k5eAaPmIp3nP	colást
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
má	mít	k5eAaImIp3nS	mít
společný	společný	k2eAgInSc4d1	společný
už	už	k6eAd1	už
jen	jen	k9	jen
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
servírovat	servírovat	k5eAaBmF	servírovat
s	s	k7c7	s
plátkem	plátek	k1gInSc7	plátek
ananasu	ananas	k1gInSc2	ananas
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Piñ	Piñ	k1gFnPc2	Piñ
Colada	Colada	k1gFnSc1	Colada
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Kniha	kniha	k1gFnSc1	kniha
Wikikuchařka	Wikikuchařka	k1gFnSc1	Wikikuchařka
<g/>
/	/	kIx~	/
<g/>
Piñ	Piñ	k1gFnSc1	Piñ
Colada	Colada	k1gFnSc1	Colada
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
</s>
