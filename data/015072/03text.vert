<s>
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
</s>
<s>
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
Zelení	Zelený	k1gMnPc1
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1989	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Michal	Michal	k1gMnSc1
BergMagdalena	BergMagdalen	k2eAgFnSc1d1
Davis	Davis	k1gFnSc1
Místopředseda	místopředseda	k1gMnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
GümplováPetr	GümplováPetr	k1gMnSc1
Globočník	Globočník	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Náměstí	náměstí	k1gNnSc1
Winstona	Winston	k1gMnSc2
Churchilla	Churchill	k1gMnSc2
1800	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
130	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Ideologie	ideologie	k1gFnSc2
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
environmentalismussociální	environmentalismussociální	k2eAgInSc1d1
liberalismusprogresivismuspro-evropanismus	liberalismusprogresivismuspro-evropanismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
středolevice	středolevice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Global	globat	k5eAaImAgInS
Greens	Greens	k1gInSc1
Evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
Politická	politický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
EP	EP	kA
</s>
<s>
Zelení	zelení	k1gNnSc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
Počet	počet	k1gInSc1
členů	člen	k1gMnPc2
</s>
<s>
1	#num#	k4
201	#num#	k4
(	(	kIx(
<g/>
únor	únor	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Barvy	barva	k1gFnSc2
</s>
<s>
Zelená	Zelená	k1gFnSc1
(	(	kIx(
<g/>
Pantone	Panton	k1gInSc5
368	#num#	k4
C	C	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
</s>
<s>
1,47	1,47	k4
%	%	kIx~
(	(	kIx(
<g/>
PS	PS	kA
PČR	PČR	kA
2017	#num#	k4
<g/>
)	)	kIx)
IČO	IČO	kA
</s>
<s>
00409740	#num#	k4
(	(	kIx(
<g/>
PSH	PSH	kA
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.zeleni.cz	www.zeleni.cz	k1gInSc1
Zisk	zisk	k1gInSc1
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
<g/>
2017	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
Senát	senát	k1gInSc1
<g/>
2020	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
2014	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
2018	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Zastupitelstva	zastupitelstvo	k1gNnPc1
krajů	kraj	k1gInPc2
<g/>
2020	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
Zastupitelstva	zastupitelstvo	k1gNnPc1
obcí	obec	k1gFnPc2
<g/>
2018	#num#	k4
</s>
<s>
182	#num#	k4
<g/>
/	/	kIx~
<g/>
61900	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Strana	strana	k1gFnSc1
zelených	zelený	k1gFnPc2
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
Zelení	zelený	k2eAgMnPc1d1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
SZ	SZ	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
prosazující	prosazující	k2eAgFnSc4d1
zelenou	zelený	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
–	–	k?
k	k	k7c3
základním	základní	k2eAgInPc3d1
programovým	programový	k2eAgInPc3d1
dokumentům	dokument	k1gInPc3
patří	patřit	k5eAaImIp3nS
mj.	mj.	kA
Charta	charta	k1gFnSc1
globálních	globální	k2eAgFnPc2d1
zelených	zelený	k1gFnPc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Charta	charta	k1gFnSc1
Evropských	evropský	k2eAgNnPc2d1
zelených	zelený	k1gNnPc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
vznikla	vzniknout	k5eAaPmAgFnS
krátce	krátce	k6eAd1
po	po	k7c6
listopadové	listopadový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
na	na	k7c4
podzim	podzim	k1gInSc4
1989	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
poprvé	poprvé	k6eAd1
získala	získat	k5eAaPmAgFnS
poslanecká	poslanecký	k2eAgFnSc1d1
křesla	křesnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většího	veliký	k2eAgInSc2d2
významu	význam	k1gInSc2
a	a	k8xC
výraznějšího	výrazný	k2eAgInSc2d2
vlivu	vliv	k1gInSc2
na	na	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
debatu	debata	k1gFnSc4
v	v	k7c6
ČR	ČR	kA
se	se	k3xPyFc4
dočkala	dočkat	k5eAaPmAgFnS
až	až	k9
kolem	kolem	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
ní	on	k3xPp3gFnSc2
masově	masově	k6eAd1
vstoupili	vstoupit	k5eAaPmAgMnP
lidé	člověk	k1gMnPc1
z	z	k7c2
prostředí	prostředí	k1gNnSc2
ekologických	ekologický	k2eAgFnPc2d1
občanských	občanský	k2eAgFnPc2d1
iniciativ	iniciativa	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
ji	on	k3xPp3gFnSc4
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
ignorovali	ignorovat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
podzim	podzim	k1gInSc4
2004	#num#	k4
získala	získat	k5eAaPmAgFnS
podruhé	podruhé	k6eAd1
parlamentní	parlamentní	k2eAgNnPc4d1
zastoupení	zastoupení	k1gNnPc4
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgMnS
do	do	k7c2
Senátu	senát	k1gInSc2
PČR	PČR	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
za	za	k7c4
Zelené	zelené	k1gNnSc4
zvolen	zvolen	k2eAgMnSc1d1
nestraník	nestraník	k1gMnSc1
Jaromír	Jaromír	k1gMnSc1
Štětina	štětina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sněmovních	sněmovní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
Zelení	zelenit	k5eAaImIp3nP
poprvé	poprvé	k6eAd1
přesáhli	přesáhnout	k5eAaPmAgMnP
5	#num#	k4
<g/>
%	%	kIx~
hranici	hranice	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
potřebná	potřebný	k2eAgFnSc1d1
pro	pro	k7c4
vstup	vstup	k1gInSc4
strany	strana	k1gFnSc2
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nakonec	nakonec	k6eAd1
zasedlo	zasednout	k5eAaPmAgNnS
6	#num#	k4
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zeleň	k1gFnPc2
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
součástí	součást	k1gFnSc7
pravostředové	pravostředový	k2eAgFnSc2d1
vládní	vládní	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
zastávali	zastávat	k5eAaImAgMnP
čtyři	čtyři	k4xCgFnPc4
ministerské	ministerský	k2eAgFnPc4d1
posty	posta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2010	#num#	k4
Zelení	zelení	k1gNnSc2
své	svůj	k3xOyFgInPc4
zastoupení	zastoupení	k1gNnSc4
v	v	k7c6
Poslanecké	poslanecký	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
neobhájili	obhájit	k5eNaPmAgMnP
<g/>
,	,	kIx,
avšak	avšak	k8xC
již	již	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Senátu	senát	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
dva	dva	k4xCgMnPc1
senátoři	senátor	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
Zelení	Zelený	k1gMnPc1
se	se	k3xPyFc4
tak	tak	k6eAd1
opět	opět	k6eAd1
stali	stát	k5eAaPmAgMnP
parlamentní	parlamentní	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Strana	strana	k1gFnSc1
má	mít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1200	#num#	k4
členů	člen	k1gInPc2
(	(	kIx(
<g/>
k	k	k7c3
únoru	únor	k1gInSc3
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
přibližně	přibližně	k6eAd1
čtvrtinu	čtvrtina	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
nejúspěšnější	úspěšný	k2eAgFnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
má	mít	k5eAaImIp3nS
ve	v	k7c6
vrcholných	vrcholný	k2eAgInPc6d1
orgánech	orgán	k1gInPc6
i	i	k8xC
na	na	k7c6
kandidátních	kandidátní	k2eAgFnPc6d1
listinách	listina	k1gFnPc6
vysoký	vysoký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zelení	Zelený	k1gMnPc1
jsou	být	k5eAaImIp3nP
řádným	řádný	k2eAgInSc7d1
členem	člen	k1gInSc7
Evropské	evropský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
a	a	k8xC
Global	globat	k5eAaImAgInS
Greens	Greens	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
</s>
<s>
Některé	některý	k3yIgInPc1
ekonomické	ekonomický	k2eAgInPc1d1
názory	názor	k1gInPc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
lze	lze	k6eAd1
chápat	chápat	k5eAaImF
jako	jako	k8xS,k8xC
liberální	liberální	k2eAgMnPc1d1
(	(	kIx(
<g/>
mírné	mírný	k2eAgNnSc1d1
snížení	snížení	k1gNnSc1
celkové	celkový	k2eAgFnSc2d1
daňové	daňový	k2eAgFnSc2d1
kvóty	kvóta	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
snížení	snížení	k1gNnSc2
přímých	přímý	k2eAgFnPc2d1
daní	daň	k1gFnPc2
a	a	k8xC
vedlejších	vedlejší	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
práce	práce	k1gFnSc2
jako	jako	k8xC,k8xS
sociální	sociální	k2eAgNnSc4d1
pojištění	pojištění	k1gNnSc4
<g/>
,	,	kIx,
zjednodušení	zjednodušení	k1gNnSc4
a	a	k8xC
zefektivnění	zefektivnění	k1gNnSc4
úřadů	úřad	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
další	další	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
levicové	levicový	k2eAgNnSc1d1
(	(	kIx(
<g/>
podpora	podpora	k1gFnSc1
progresivního	progresivní	k2eAgNnSc2d1
zdanění	zdanění	k1gNnSc2
<g/>
,	,	kIx,
úloha	úloha	k1gFnSc1
státu	stát	k1gInSc2
v	v	k7c6
regulaci	regulace	k1gFnSc6
aktivit	aktivita	k1gFnPc2
poškozujících	poškozující	k2eAgInPc2d1
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
a	a	k8xC
prosazování	prosazování	k1gNnSc4
trvale	trvale	k6eAd1
udržitelného	udržitelný	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
před	před	k7c7
volbami	volba	k1gFnPc7
2006	#num#	k4
odmítal	odmítat	k5eAaImAgMnS
uvést	uvést	k5eAaPmF
preferovaného	preferovaný	k2eAgMnSc4d1
koaličního	koaliční	k2eAgMnSc4d1
partnera	partner	k1gMnSc4
se	s	k7c7
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
nejsme	být	k5eNaImIp1nP
pravicoví	pravicový	k2eAgMnPc1d1
ani	ani	k8xC
levicoví	levicový	k2eAgMnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
zelení	zelenit	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
po	po	k7c6
vstupu	vstup	k1gInSc6
Zelených	Zelených	k2eAgInSc6d1
do	do	k7c2
pravicové	pravicový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
ODS	ODS	kA
se	se	k3xPyFc4
však	však	k9
zařazení	zařazení	k1gNnSc1
strany	strana	k1gFnSc2
na	na	k7c6
pravolevém	pravolevý	k2eAgNnSc6d1
spektru	spektrum	k1gNnSc6
stalo	stát	k5eAaPmAgNnS
předmětem	předmět	k1gInSc7
rostoucích	rostoucí	k2eAgInPc2d1
sporů	spor	k1gInPc2
<g/>
,	,	kIx,
vrcholících	vrcholící	k2eAgFnPc2d1
sjezdem	sjezd	k1gInSc7
v	v	k7c6
září	září	k1gNnSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
zvolení	zvolení	k1gNnPc2
Ondřeje	Ondřej	k1gMnSc2
Lišky	Liška	k1gMnSc2
do	do	k7c2
vedení	vedení	k1gNnSc2
strany	strana	k1gFnSc2
v	v	k7c6
prosinci	prosinec	k1gInSc6
2009	#num#	k4
a	a	k8xC
zejména	zejména	k9
od	od	k7c2
zvolení	zvolení	k1gNnSc2
Matěje	Matěj	k1gMnSc2
Stropnického	stropnický	k2eAgMnSc2d1
v	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
se	se	k3xPyFc4
strana	strana	k1gFnSc1
zřetelně	zřetelně	k6eAd1
posouvá	posouvat	k5eAaImIp3nS
ke	k	k7c3
středolevicové	středolevicový	k2eAgFnSc3d1
politice	politika	k1gFnSc3
typické	typický	k2eAgFnSc6d1
pro	pro	k7c4
evropské	evropský	k2eAgFnPc4d1
zelené	zelený	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ideová	ideový	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
</s>
<s>
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
si	se	k3xPyFc3
dle	dle	k7c2
svých	svůj	k3xOyFgFnPc2
stanov	stanova	k1gFnPc2
(	(	kIx(
<g/>
schválených	schválený	k2eAgInPc2d1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
)	)	kIx)
zejména	zejména	k9
klade	klást	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
svou	svůj	k3xOyFgFnSc7
politikou	politika	k1gFnSc7
prosazovat	prosazovat	k5eAaImF
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
vybudování	vybudování	k1gNnSc4
ekologicky	ekologicky	k6eAd1
a	a	k8xC
sociálně	sociálně	k6eAd1
udržitelné	udržitelný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
co	co	k9
nejvyšší	vysoký	k2eAgFnSc4d3
kvalitu	kvalita	k1gFnSc4
demokracie	demokracie	k1gFnSc1
s	s	k7c7
co	co	k9
největší	veliký	k2eAgFnSc7d3
mírou	míra	k1gFnSc7
zapojení	zapojení	k1gNnSc2
veřejnosti	veřejnost	k1gFnSc2
do	do	k7c2
rozhodování	rozhodování	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
důsledné	důsledný	k2eAgNnSc1d1
dodržování	dodržování	k1gNnSc1
mezinárodních	mezinárodní	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
o	o	k7c6
lidských	lidský	k2eAgNnPc6d1
právech	právo	k1gNnPc6
<g/>
,	,	kIx,
</s>
<s>
ochranu	ochrana	k1gFnSc4
práv	právo	k1gNnPc2
sociálních	sociální	k2eAgNnPc2d1
<g/>
,	,	kIx,
etnických	etnický	k2eAgFnPc2d1
<g/>
,	,	kIx,
kulturních	kulturní	k2eAgFnPc2d1
i	i	k8xC
jiných	jiný	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
rovnoprávnost	rovnoprávnost	k1gFnSc1
mužů	muž	k1gMnPc2
a	a	k8xC
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
globální	globální	k2eAgFnSc4d1
odpovědnost	odpovědnost	k1gFnSc4
a	a	k8xC
solidaritu	solidarita	k1gFnSc4
vůči	vůči	k7c3
potřebným	potřebný	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
<g/>
,	,	kIx,
</s>
<s>
sociální	sociální	k2eAgFnSc4d1
spravedlnost	spravedlnost	k1gFnSc4
a	a	k8xC
sociální	sociální	k2eAgFnSc4d1
solidaritu	solidarita	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
zvyšování	zvyšování	k1gNnSc1
vzdělanosti	vzdělanost	k1gFnSc2
a	a	k8xC
kulturnosti	kulturnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInPc4d1
programové	programový	k2eAgInPc4d1
body	bod	k1gInPc4
Zelených	Zelených	k2eAgInPc4d1
ve	v	k7c6
volbách	volba	k1gFnPc6
2006	#num#	k4
byly	být	k5eAaImAgInP
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modernizace	modernizace	k1gFnSc1
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
založená	založený	k2eAgFnSc1d1
mj.	mj.	kA
na	na	k7c6
odklonu	odklon	k1gInSc6
od	od	k7c2
využívání	využívání	k1gNnSc2
ropy	ropa	k1gFnSc2
a	a	k8xC
jaderné	jaderný	k2eAgFnSc2d1
energie	energie	k1gFnSc2
a	a	k8xC
ve	v	k7c6
využívání	využívání	k1gNnSc6
alternativních	alternativní	k2eAgMnPc2d1
<g/>
,	,	kIx,
k	k	k7c3
přírodě	příroda	k1gFnSc3
ohleduplných	ohleduplný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cíle	cíl	k1gInSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
dosaženo	dosáhnout	k5eAaPmNgNnS
zejména	zejména	k9
ekonomickými	ekonomický	k2eAgInPc7d1
nástroji	nástroj	k1gInPc7
–	–	k?
ekologickou	ekologický	k2eAgFnSc7d1
daňovou	daňový	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zvýhodní	zvýhodnit	k5eAaPmIp3nS
ekologicky	ekologicky	k6eAd1
šetrné	šetrný	k2eAgInPc4d1
postupy	postup	k1gInPc4
před	před	k7c7
nešetrnými	šetrný	k2eNgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamněji	významně	k6eAd3
chce	chtít	k5eAaImIp3nS
SZ	SZ	kA
změnit	změnit	k5eAaPmF
dopravu	doprava	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c6
přenesení	přenesení	k1gNnSc6
nákladní	nákladní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c4
železnici	železnice	k1gFnSc4
<g/>
,	,	kIx,
zvýhodnění	zvýhodnění	k1gNnPc4
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
před	před	k7c4
individuální	individuální	k2eAgFnSc4d1
<g/>
,	,	kIx,
zvýhodnění	zvýhodnění	k1gNnSc4
využívání	využívání	k1gNnSc2
alternativních	alternativní	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
(	(	kIx(
<g/>
zejména	zejména	k9
bioplynu	bioplyn	k1gInSc3
<g/>
)	)	kIx)
před	před	k7c7
benzínem	benzín	k1gInSc7
a	a	k8xC
naftou	nafta	k1gFnSc7
<g/>
,	,	kIx,
zrovnoprávnění	zrovnoprávnění	k1gNnSc4
automobilismu	automobilismus	k1gInSc2
s	s	k7c7
cyklistickou	cyklistický	k2eAgFnSc7d1
a	a	k8xC
pěší	pěší	k2eAgFnSc7d1
dopravou	doprava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
průmyslu	průmysl	k1gInSc2
a	a	k8xC
energetiky	energetika	k1gFnSc2
SZ	SZ	kA
požaduje	požadovat	k5eAaImIp3nS
urychlené	urychlený	k2eAgNnSc4d1
snížení	snížení	k1gNnSc4
emisí	emise	k1gFnPc2
CO	co	k8xS
<g/>
2	#num#	k4
<g/>
,	,	kIx,
odmítá	odmítat	k5eAaImIp3nS
výstavbu	výstavba	k1gFnSc4
nových	nový	k2eAgFnPc2d1
jaderných	jaderný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
a	a	k8xC
prolomení	prolomení	k1gNnSc2
limitů	limit	k1gInPc2
těžby	těžba	k1gFnSc2
uhlí	uhlí	k1gNnSc2
v	v	k7c6
severních	severní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nejlevnější	levný	k2eAgFnSc1d3
energie	energie	k1gFnSc1
je	být	k5eAaImIp3nS
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
jaká	jaký	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
vůbec	vůbec	k9
nevyrobí	vyrobit	k5eNaPmIp3nS
<g/>
,	,	kIx,
tj.	tj.	kA
že	že	k8xS
největší	veliký	k2eAgInSc1d3
potenciál	potenciál	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
úsporách	úspora	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ochrana	ochrana	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
práv	právo	k1gNnPc2
spotřebitelů	spotřebitel	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
SZ	SZ	kA
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
zdravou	zdravý	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
<g/>
,	,	kIx,
šetrné	šetrný	k2eAgNnSc4d1
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
maximální	maximální	k2eAgFnSc4d1
možnou	možný	k2eAgFnSc4d1
míru	míra	k1gFnSc4
informování	informování	k1gNnSc2
spotřebitelů	spotřebitel	k1gMnPc2
o	o	k7c6
výrobcích	výrobek	k1gInPc6
<g/>
,	,	kIx,
omezování	omezování	k1gNnSc4
využívání	využívání	k1gNnSc2
jedovatých	jedovatý	k2eAgFnPc2d1
a	a	k8xC
škodlivých	škodlivý	k2eAgFnPc2d1
chemikálií	chemikálie	k1gFnPc2
při	při	k7c6
výrobě	výroba	k1gFnSc6
potravin	potravina	k1gFnPc2
i	i	k8xC
průmyslového	průmyslový	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
zvýšení	zvýšení	k1gNnSc1
podílu	podíl	k1gInSc2
recyklovaných	recyklovaný	k2eAgInPc2d1
odpadů	odpad	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c4
posílení	posílení	k1gNnSc4
práv	právo	k1gNnPc2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
využití	využití	k1gNnSc1
alternativních	alternativní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
energie	energie	k1gFnSc2
(	(	kIx(
<g/>
bionafta	bionafta	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
o	o	k7c4
zdravý	zdravý	k2eAgInSc4d1
život	život	k1gInSc4
občanů	občan	k1gMnPc2
ČR	ČR	kA
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Otevřená	otevřený	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
s	s	k7c7
důrazem	důraz	k1gInSc7
na	na	k7c4
rozvoj	rozvoj	k1gInSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
:	:	kIx,
SZ	SZ	kA
zejména	zejména	k9
podporuje	podporovat	k5eAaImIp3nS
zvýšení	zvýšení	k1gNnSc1
podílu	podíl	k1gInSc2
občanů	občan	k1gMnPc2
na	na	k7c6
rozhodování	rozhodování	k1gNnSc6
formou	forma	k1gFnSc7
referend	referendum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
programu	program	k1gInSc6
měla	mít	k5eAaImAgFnS
i	i	k9
snížení	snížení	k1gNnSc4
věku	věk	k1gInSc2
pro	pro	k7c4
aktivní	aktivní	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
na	na	k7c4
16	#num#	k4
let	léto	k1gNnPc2
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podpora	podpora	k1gFnSc1
školství	školství	k1gNnSc2
a	a	k8xC
vědy	věda	k1gFnSc2
<g/>
:	:	kIx,
SZ	SZ	kA
požaduje	požadovat	k5eAaImIp3nS
zvýšení	zvýšení	k1gNnSc4
výdajů	výdaj	k1gInPc2
státu	stát	k1gInSc2
na	na	k7c4
kulturu	kultura	k1gFnSc4
na	na	k7c4
1	#num#	k4
%	%	kIx~
ze	z	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
priority	priorita	k1gFnPc4
patří	patřit	k5eAaImIp3nS
regulace	regulace	k1gFnSc1
reklamy	reklama	k1gFnSc2
a	a	k8xC
stanovení	stanovení	k1gNnSc2
přesnějších	přesný	k2eAgInPc2d2
mantinelů	mantinel	k1gInPc2
pro	pro	k7c4
tzv.	tzv.	kA
radniční	radniční	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
:	:	kIx,
SZ	SZ	kA
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
zdravotní	zdravotní	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
<g/>
,	,	kIx,
adresnou	adresný	k2eAgFnSc4d1
sociální	sociální	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
práce	práce	k1gFnSc2
na	na	k7c4
částečný	částečný	k2eAgInSc4d1
úvazek	úvazek	k1gInSc4
<g/>
,	,	kIx,
rovný	rovný	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
heterosexuálům	heterosexuál	k1gMnPc3
i	i	k8xC
homosexuálům	homosexuál	k1gMnPc3
<g/>
,	,	kIx,
právo	právo	k1gNnSc4
žen	žena	k1gFnPc2
na	na	k7c4
interrupce	interrupce	k1gFnPc4
<g/>
,	,	kIx,
ochranu	ochrana	k1gFnSc4
dětí	dítě	k1gFnPc2
před	před	k7c7
pornografií	pornografie	k1gFnSc7
a	a	k8xC
zvýšení	zvýšení	k1gNnSc1
počtu	počet	k1gInSc2
žen	žena	k1gFnPc2
ve	v	k7c6
veřejném	veřejný	k2eAgInSc6d1
životě	život	k1gInSc6
prostřednictví	prostřednictví	k1gNnSc2
gender	gender	k1gInSc4
mainstreamingu	mainstreaming	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genderové	Genderový	k2eAgFnPc4d1
kvóty	kvóta	k1gFnPc4
pro	pro	k7c4
ženy	žena	k1gFnPc4
navrhuje	navrhovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
rámci	rámec	k1gInSc6
volebního	volební	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Perspektivně	perspektivně	k6eAd1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
zrušeny	zrušit	k5eAaPmNgInP
dětské	dětský	k2eAgInPc1d1
domovy	domov	k1gInPc1
a	a	k8xC
nahrazeny	nahradit	k5eAaPmNgFnP
původní	původní	k2eAgFnSc7d1
či	či	k8xC
adoptivní	adoptivní	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
založena	založit	k5eAaPmNgFnS
na	na	k7c6
výrazném	výrazný	k2eAgNnSc6d1
posílení	posílení	k1gNnSc6
evropské	evropský	k2eAgFnSc2d1
integrace	integrace	k1gFnSc2
a	a	k8xC
upřednostnění	upřednostnění	k1gNnSc2
zahraniční	zahraniční	k2eAgFnSc2d1
rozvojové	rozvojový	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
před	před	k7c7
vojenskou	vojenský	k2eAgFnSc7d1
účastí	účast	k1gFnSc7
ČR	ČR	kA
v	v	k7c6
konfliktech	konflikt	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
SZ	SZ	kA
podporuje	podporovat	k5eAaImIp3nS
právo	právo	k1gNnSc1
Palestinců	Palestinec	k1gMnPc2
na	na	k7c4
teritoriálně	teritoriálně	k6eAd1
souvislý	souvislý	k2eAgInSc4d1
samostatný	samostatný	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
zahraničně-politického	zahraničně-politický	k2eAgMnSc2d1
experta	expert	k1gMnSc2
Ondřeje	Ondřej	k1gMnSc2
Horkého-Hlucháně	Horkého-Hluchána	k1gFnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
Palestina	Palestina	k1gFnSc1
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
zahraniční	zahraniční	k2eAgFnSc6d1
pomoci	pomoc	k1gFnSc6
v	v	k7c6
důsledku	důsledek	k1gInSc6
porušování	porušování	k1gNnSc2
základních	základní	k2eAgNnPc2d1
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
palestinských	palestinský	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
žen	žena	k1gFnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
Izraele	Izrael	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
podzim	podzim	k1gInSc4
2011	#num#	k4
se	se	k3xPyFc4
vyslovila	vyslovit	k5eAaPmAgFnS
pro	pro	k7c4
podporu	podpora	k1gFnSc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
plného	plný	k2eAgNnSc2d1
členství	členství	k1gNnSc2
v	v	k7c6
OSN	OSN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základním	základní	k2eAgInSc7d1
ekonomickým	ekonomický	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
chce	chtít	k5eAaImIp3nS
SZ	SZ	kA
dosáhnout	dosáhnout	k5eAaPmF
zlepšení	zlepšení	k1gNnSc3
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ekologická	ekologický	k2eAgFnSc1d1
daňová	daňový	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
spočívající	spočívající	k2eAgFnSc1d1
v	v	k7c6
přesunu	přesun	k1gInSc6
od	od	k7c2
zdanění	zdanění	k1gNnSc2
lidské	lidský	k2eAgFnSc2d1
práce	práce	k1gFnSc2
ke	k	k7c3
zdanění	zdanění	k1gNnSc3
surovin	surovina	k1gFnPc2
a	a	k8xC
energií	energie	k1gFnPc2
a	a	k8xC
užití	užití	k1gNnSc2
motorových	motorový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
reformy	reforma	k1gFnSc2
veřejných	veřejný	k2eAgFnPc2d1
financí	finance	k1gFnPc2
platné	platný	k2eAgFnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
začlenilo	začlenit	k5eAaPmAgNnS
ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
tzv.	tzv.	kA
první	první	k4xOgFnSc4
etapu	etapa	k1gFnSc4
zavedení	zavedení	k1gNnSc2
ekologických	ekologický	k2eAgFnPc2d1
daní	daň	k1gFnPc2
(	(	kIx(
<g/>
ze	z	k7c2
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
,	,	kIx,
pevných	pevný	k2eAgNnPc2d1
paliv	palivo	k1gNnPc2
a	a	k8xC
elektřiny	elektřina	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
minimální	minimální	k2eAgFnSc6d1
výši	výše	k1gFnSc6,k1gFnSc6wB
požadované	požadovaný	k2eAgFnSc6d1
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s>
Propojení	propojení	k1gNnSc1
s	s	k7c7
nevládními	vládní	k2eNgFnPc7d1
organizacemi	organizace	k1gFnPc7
</s>
<s>
Strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
provázána	provázán	k2eAgFnSc1d1
s	s	k7c7
neziskovými	ziskový	k2eNgFnPc7d1
organizacemi	organizace	k1gFnPc7
(	(	kIx(
<g/>
především	především	k6eAd1
orientovanými	orientovaný	k2eAgFnPc7d1
na	na	k7c4
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
odpor	odpor	k1gInSc1
proti	proti	k7c3
jaderné	jaderný	k2eAgFnSc3d1
energii	energie	k1gFnSc3
a	a	k8xC
na	na	k7c4
ochranu	ochrana	k1gFnSc4
spotřebitele	spotřebitel	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řada	řada	k1gFnSc1
členů	člen	k1gMnPc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
neziskového	ziskový	k2eNgInSc2d1
sektoru	sektor	k1gInSc2
a	a	k8xC
někteří	některý	k3yIgMnPc1
nestraníci	nestraník	k1gMnPc1
na	na	k7c6
kandidátkách	kandidátka	k1gFnPc6
jsou	být	k5eAaImIp3nP
aktivními	aktivní	k2eAgInPc7d1
členy	člen	k1gInPc7
neziskových	ziskový	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
s	s	k7c7
neziskovými	ziskový	k2eNgFnPc7d1
organizacemi	organizace	k1gFnPc7
také	také	k6eAd1
aktivně	aktivně	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
na	na	k7c6
přípravě	příprava	k1gFnSc6
volebního	volební	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
někdy	někdy	k6eAd1
vede	vést	k5eAaImIp3nS
zejména	zejména	k9
u	u	k7c2
odpůrců	odpůrce	k1gMnPc2
Zelených	Zelených	k2eAgMnPc2d1
k	k	k7c3
připisování	připisování	k1gNnSc3
všech	všecek	k3xTgFnPc2
aktivit	aktivita	k1gFnPc2
konaných	konaný	k2eAgFnPc2d1
neziskovými	ziskový	k2eNgFnPc7d1
environmentálními	environmentální	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
také	také	k6eAd1
přímo	přímo	k6eAd1
Straně	strana	k1gFnSc3
zelených	zelená	k1gFnPc2
<g/>
,	,	kIx,
nezávisle	závisle	k6eNd1
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
se	se	k3xPyFc4
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
s	s	k7c7
takovýmito	takovýto	k3xDgFnPc7
aktivitami	aktivita	k1gFnPc7
ztotožňuje	ztotožňovat	k5eAaImIp3nS
nebo	nebo	k8xC
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s>
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
SZ	SZ	kA
</s>
<s>
Strukturu	struktura	k1gFnSc4
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
určují	určovat	k5eAaImIp3nP
Stanovy	stanova	k1gFnPc4
přijaté	přijatý	k2eAgFnPc4d1
sjezdem	sjezd	k1gInSc7
konaným	konaný	k2eAgFnPc3d1
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2003	#num#	k4
v	v	k7c6
Táboře	Tábor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novelizovány	novelizován	k2eAgFnPc4d1
byly	být	k5eAaImAgInP
na	na	k7c6
sjezdech	sjezd	k1gInPc6
v	v	k7c6
Praze-Vršovicích	Praze-Vršovice	k1gFnPc6
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Brně-Štýřicích	Brně-Štýřik	k1gInPc6
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Brně-Pisárkách	Brně-Pisárka	k1gFnPc6
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
České	český	k2eAgFnSc6d1
Třebové	Třebová	k1gFnSc6
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
a	a	k8xC
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Organizační	organizační	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
–	–	k?
základní	základní	k2eAgInSc1d1
<g/>
,	,	kIx,
městské	městský	k2eAgFnSc2d1
a	a	k8xC
krajské	krajský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
</s>
<s>
Organizační	organizační	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
na	na	k7c6
místní	místní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
ZO	ZO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
minimálně	minimálně	k6eAd1
třemi	tři	k4xCgInPc7
členy	člen	k1gInPc7
SZ.	SZ.	k1gFnSc2
Zakládá	zakládat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
obci	obec	k1gFnSc6
<g/>
,	,	kIx,
resp.	resp.	kA
v	v	k7c6
případě	případ	k1gInSc6
územně	územně	k6eAd1
členěných	členěný	k2eAgNnPc2d1
statutárních	statutární	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
samosprávném	samosprávný	k2eAgInSc6d1
městském	městský	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
či	či	k8xC
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
daném	daný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
smí	smět	k5eAaImIp3nS
působit	působit	k5eAaImF
jen	jen	k9
jedna	jeden	k4xCgFnSc1
ZO	ZO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plenárním	plenární	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
ZO	ZO	kA
je	být	k5eAaImIp3nS
členská	členský	k2eAgFnSc1d1
schůze	schůze	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
účastnit	účastnit	k5eAaImF
všichni	všechen	k3xTgMnPc1
členové	člen	k1gMnPc1
dané	daný	k2eAgFnSc2d1
ZO	ZO	kA
a	a	k8xC
jež	jenž	k3xRgFnSc1
volí	volit	k5eAaImIp3nS
radu	rada	k1gFnSc4
ZO	ZO	kA
(	(	kIx(
<g/>
včetně	včetně	k7c2
předsedy	předseda	k1gMnSc2
a	a	k8xC
případně	případně	k6eAd1
místopředsedy	místopředseda	k1gMnSc2
ZO	ZO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokladníka	pokladník	k1gMnSc2
ZO	ZO	kA
a	a	k8xC
revizora	revizor	k1gMnSc2
ZO	ZO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členská	členský	k2eAgFnSc1d1
schůze	schůze	k1gFnSc1
dále	daleko	k6eAd2
volí	volit	k5eAaImIp3nS
delegáty	delegát	k1gMnPc4
na	na	k7c4
městskou	městský	k2eAgFnSc4d1
konferenci	konference	k1gFnSc4
<g/>
,	,	kIx,
krajskou	krajský	k2eAgFnSc4d1
konferenci	konference	k1gFnSc4
a	a	k8xC
sjezd	sjezd	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
schvaluje	schvalovat	k5eAaImIp3nS
rozpočet	rozpočet	k1gInSc1
ZO	ZO	kA
na	na	k7c4
daný	daný	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
,	,	kIx,
kandidátní	kandidátní	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
a	a	k8xC
případnou	případný	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
resp.	resp.	kA
městského	městský	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
či	či	k8xC
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
ustavovat	ustavovat	k5eAaImF
oblastní	oblastní	k2eAgFnPc4d1
základní	základní	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mohou	moct	k5eAaImIp3nP
působit	působit	k5eAaImF
<g/>
:	:	kIx,
</s>
<s>
na	na	k7c6
souvislém	souvislý	k2eAgNnSc6d1
území	území	k1gNnSc6
více	hodně	k6eAd2
obcí	obec	k1gFnPc2
téhož	týž	k3xTgInSc2
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
více	hodně	k6eAd2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
se	s	k7c7
souhlasem	souhlas	k1gInSc7
příslušné	příslušný	k2eAgFnSc2d1
krajské	krajský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
na	na	k7c6
souvislém	souvislý	k2eAgNnSc6d1
území	území	k1gNnSc6
více	hodně	k6eAd2
městských	městský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
či	či	k8xC
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
téhož	týž	k3xTgNnSc2
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
(	(	kIx(
<g/>
se	s	k7c7
souhlasem	souhlas	k1gInSc7
příslušné	příslušný	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
MO	MO	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
ustavuje	ustavovat	k5eAaImIp3nS
v	v	k7c6
územně	územně	k6eAd1
členěném	členěný	k2eAgNnSc6d1
statutárním	statutární	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
(	(	kIx(
<g/>
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
městských	městský	k2eAgInPc6d1
obvodech	obvod	k1gInPc6
či	či	k8xC
městských	městský	k2eAgFnPc6d1
částech	část	k1gFnPc6
<g/>
)	)	kIx)
působí	působit	k5eAaImIp3nS
více	hodně	k6eAd2
základních	základní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
sjednocuje	sjednocovat	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
těchto	tento	k3xDgFnPc2
ZO	ZO	kA
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plenárním	plenární	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
MO	MO	kA
je	být	k5eAaImIp3nS
městská	městský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
tvoří	tvořit	k5eAaImIp3nP
delegáti	delegát	k1gMnPc1
zvolení	zvolení	k1gNnSc1
základními	základní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
a	a	k8xC
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
volí	volit	k5eAaImIp3nP
radu	rada	k1gFnSc4
MO	MO	kA
(	(	kIx(
<g/>
včetně	včetně	k7c2
předsedy	předseda	k1gMnSc2
a	a	k8xC
místopředsedů	místopředseda	k1gMnPc2
MO	MO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokladníka	pokladník	k1gMnSc2
MO	MO	kA
a	a	k8xC
revizora	revizor	k1gMnSc2
MO	MO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
schvaluje	schvalovat	k5eAaImIp3nS
rozpočet	rozpočet	k1gInSc1
MO	MO	kA
na	na	k7c4
daný	daný	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
,	,	kIx,
kandidátní	kandidátní	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
a	a	k8xC
případně	případně	k6eAd1
koalici	koalice	k1gFnSc4
pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
krajské	krajský	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
je	být	k5eAaImIp3nS
organizační	organizační	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
krajská	krajský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
KO	KO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
tvoří	tvořit	k5eAaImIp3nP
všechny	všechen	k3xTgInPc1
ZO	ZO	kA
na	na	k7c6
území	území	k1gNnSc6
daného	daný	k2eAgInSc2d1
samosprávného	samosprávný	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
na	na	k7c6
území	území	k1gNnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plenárním	plenární	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
KO	KO	kA
je	být	k5eAaImIp3nS
krajská	krajský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
tvoří	tvořit	k5eAaImIp3nP
delegáti	delegát	k1gMnPc1
zvolení	zvolení	k1gNnSc1
základními	základní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
a	a	k8xC
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
volí	volit	k5eAaImIp3nP
radu	rada	k1gFnSc4
KO	KO	kA
(	(	kIx(
<g/>
včetně	včetně	k7c2
předsedy	předseda	k1gMnSc2
a	a	k8xC
místopředsedů	místopředseda	k1gMnPc2
KO	KO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokladníka	pokladník	k1gMnSc2
KO	KO	kA
a	a	k8xC
krajskou	krajský	k2eAgFnSc4d1
revizní	revizní	k2eAgFnSc4d1
komisi	komise	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
schvaluje	schvalovat	k5eAaImIp3nS
rozpočet	rozpočet	k1gInSc1
KO	KO	kA
na	na	k7c4
daný	daný	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
,	,	kIx,
kandidátní	kandidátní	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
a	a	k8xC
případně	případně	k6eAd1
koalici	koalice	k1gFnSc4
pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
krajského	krajský	k2eAgNnSc2d1
zastupitelstva	zastupitelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
tuto	tento	k3xDgFnSc4
strukturu	struktura	k1gFnSc4
umožňují	umožňovat	k5eAaImIp3nP
stanovy	stanova	k1gFnPc4
základním	základní	k2eAgFnPc3d1
organizacím	organizace	k1gFnPc3
vytvářet	vytvářet	k5eAaImF
lokální	lokální	k2eAgNnPc4d1
sdružení	sdružení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
nejsou	být	k5eNaImIp3nP
součástí	součást	k1gFnSc7
organizační	organizační	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
SZ	SZ	kA
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
vnitřní	vnitřní	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
je	být	k5eAaImIp3nS
věcí	věc	k1gFnSc7
jejich	jejich	k3xOp3gFnSc2
vzájemné	vzájemný	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnost	možnost	k1gFnSc4
ustavovat	ustavovat	k5eAaImF
lokální	lokální	k2eAgNnSc4d1
sdružení	sdružení	k1gNnSc4
je	být	k5eAaImIp3nS
náhradou	náhrada	k1gFnSc7
za	za	k7c4
okresní	okresní	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
zrušené	zrušený	k2eAgFnPc4d1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
však	však	k9
sdružování	sdružování	k1gNnSc4
ZO	ZO	kA
i	i	k8xC
na	na	k7c6
jiném	jiný	k2eAgInSc6d1
než	než	k8xS
územním	územní	k2eAgInSc6d1
principu	princip	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vrcholné	vrcholný	k2eAgInPc1d1
orgány	orgán	k1gInPc1
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3
orgánem	orgán	k1gInSc7
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
celostátní	celostátní	k2eAgInSc1d1
sjezd	sjezd	k1gInSc4
tvořený	tvořený	k2eAgInSc4d1
nejméně	málo	k6eAd3
250	#num#	k4
delegáty	delegát	k1gMnPc7
zvolenými	zvolený	k2eAgMnPc7d1
v	v	k7c6
základních	základní	k2eAgFnPc6d1
organizacích	organizace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sjezd	sjezd	k1gInSc1
je	být	k5eAaImIp3nS
svoláván	svolávat	k5eAaImNgInS
alespoň	alespoň	k9
jednou	jednou	k6eAd1
ročně	ročně	k6eAd1
rozhodnutím	rozhodnutí	k1gNnSc7
předsednictva	předsednictvo	k1gNnSc2
SZ.	SZ.	k1gFnSc2
Mimořádný	mimořádný	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
lze	lze	k6eAd1
svolat	svolat	k5eAaPmF
na	na	k7c6
základě	základ	k1gInSc6
požadavku	požadavek	k1gInSc2
organizačních	organizační	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
SZ.	SZ.	k1gFnSc2
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3
výkonným	výkonný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
SZ	SZ	kA
mezi	mezi	k7c7
sjezdy	sjezd	k1gInPc7
je	být	k5eAaImIp3nS
republiková	republikový	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
RR	RR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
předsedy	předseda	k1gMnPc7
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
volenými	volený	k2eAgMnPc7d1
zástupci	zástupce	k1gMnPc7
krajských	krajský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
předsednictvem	předsednictvo	k1gNnSc7
SZ	SZ	kA
a	a	k8xC
poslanci	poslanec	k1gMnPc7
a	a	k8xC
senátory	senátor	k1gMnPc7
zvolenými	zvolený	k2eAgInPc7d1
za	za	k7c4
SZ	SZ	kA
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
členy	člen	k1gMnPc7
SZ.	SZ.	k1gFnSc2
Republiková	republikový	k2eAgFnSc1d1
rada	rada	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
sjezdu	sjezd	k1gInSc2
za	za	k7c4
plnění	plnění	k1gNnSc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
usnesení	usnesení	k1gNnSc2
<g/>
,	,	kIx,
cílů	cíl	k1gInPc2
a	a	k8xC
hlavních	hlavní	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
SZ	SZ	kA
a	a	k8xC
politického	politický	k2eAgInSc2d1
programu	program	k1gInSc2
SZ	SZ	kA
<g/>
,	,	kIx,
pověřuje	pověřovat	k5eAaImIp3nS
předsednictvo	předsednictvo	k1gNnSc1
realizací	realizace	k1gFnPc2
svých	svůj	k3xOyFgNnPc2
usnesení	usnesení	k1gNnPc2
a	a	k8xC
schvaluje	schvalovat	k5eAaImIp3nS
s	s	k7c7
pověřením	pověření	k1gNnSc7
sjezdu	sjezd	k1gInSc2
některé	některý	k3yIgInPc4
vnitřní	vnitřní	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
schvaluje	schvalovat	k5eAaImIp3nS
rozpočet	rozpočet	k1gInSc1
<g/>
,	,	kIx,
volební	volební	k2eAgInPc1d1
programy	program	k1gInPc1
<g/>
,	,	kIx,
uzavření	uzavření	k1gNnSc1
republikových	republikový	k2eAgFnPc2d1
předvolebních	předvolební	k2eAgFnPc2d1
či	či	k8xC
povolebních	povolební	k2eAgFnPc2d1
koalic	koalice	k1gFnPc2
<g/>
,	,	kIx,
společnou	společný	k2eAgFnSc4d1
strategii	strategie	k1gFnSc4
voleb	volba	k1gFnPc2
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
závažných	závažný	k2eAgInPc6d1
a	a	k8xC
odůvodněných	odůvodněný	k2eAgInPc6d1
případech	případ	k1gInPc6
úpravy	úprava	k1gFnSc2
krajských	krajský	k2eAgFnPc2d1
a	a	k8xC
komunálních	komunální	k2eAgFnPc2d1
kandidátek	kandidátka	k1gFnPc2
<g/>
,	,	kIx,
strategii	strategie	k1gFnSc4
voleb	volba	k1gFnPc2
a	a	k8xC
kandidáty	kandidát	k1gMnPc7
do	do	k7c2
parlamentu	parlament	k1gInSc2
a	a	k8xC
zastupitelských	zastupitelský	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
ČR	ČR	kA
v	v	k7c6
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s>
Předsednictvo	předsednictvo	k1gNnSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Perla	perla	k1gFnSc1
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
Žákovská	žákovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
<g/>
,	,	kIx,
Magdalena	Magdalena	k1gFnSc1
Davis	Davis	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
<g/>
,	,	kIx,
Petra	Petra	k1gFnSc1
Jelínková	Jelínková	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Vosecký	Vosecký	k1gMnSc1
a	a	k8xC
Vít	Vít	k1gMnSc1
Masare	Masar	k1gInSc5
</s>
<s>
Užším	úzký	k2eAgInSc7d2
výkonným	výkonný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
Strany	strana	k1gFnSc2
zelených	zelený	k2eAgMnPc2d1
v	v	k7c6
období	období	k1gNnSc6
mezi	mezi	k7c7
sjezdy	sjezd	k1gInPc7
a	a	k8xC
zasedáními	zasedání	k1gNnPc7
republikové	republikový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
je	být	k5eAaImIp3nS
sedmičlenné	sedmičlenný	k2eAgNnSc1d1
předsednictvo	předsednictvo	k1gNnSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
(	(	kIx(
<g/>
PSZ	PSZ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
je	být	k5eAaImIp3nS
voleno	volit	k5eAaImNgNnS
sjezdem	sjezd	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nP
je	on	k3xPp3gFnPc4
předsedové	předseda	k1gMnPc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
tři	tři	k4xCgMnPc1
místopředsedové	místopředseda	k1gMnPc1
strany	strana	k1gFnSc2
a	a	k8xC
tři	tři	k4xCgMnPc1
další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
pravomoci	pravomoc	k1gFnPc4
patří	patřit	k5eAaImIp3nS
zajišťovat	zajišťovat	k5eAaImF
vedení	vedení	k1gNnSc4
a	a	k8xC
chod	chod	k1gInSc4
strany	strana	k1gFnSc2
v	v	k7c6
organizačních	organizační	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
a	a	k8xC
přijímat	přijímat	k5eAaImF
neodkladná	odkladný	k2eNgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
<g/>
,	,	kIx,
svolávat	svolávat	k5eAaImF
jednání	jednání	k1gNnPc4
sjezdu	sjezd	k1gInSc2
<g/>
,	,	kIx,
připravovat	připravovat	k5eAaImF
program	program	k1gInSc4
a	a	k8xC
materiály	materiál	k1gInPc4
pro	pro	k7c4
jednání	jednání	k1gNnSc4
sjezdu	sjezd	k1gInSc2
a	a	k8xC
RR	RR	kA
<g/>
,	,	kIx,
navrhovat	navrhovat	k5eAaImF
a	a	k8xC
realizovat	realizovat	k5eAaBmF
plán	plán	k1gInSc4
činnosti	činnost	k1gFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
zaujímat	zaujímat	k5eAaImF
stanoviska	stanovisko	k1gNnPc4
k	k	k7c3
aktuální	aktuální	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
průběžně	průběžně	k6eAd1
kontrolovat	kontrolovat	k5eAaImF
dodržování	dodržování	k1gNnSc4
rozpočtu	rozpočet	k1gInSc2
a	a	k8xC
navrhovat	navrhovat	k5eAaImF
jeho	jeho	k3xOp3gFnPc4
změny	změna	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
nefunkčnosti	nefunkčnost	k1gFnSc2
některých	některý	k3yIgMnPc2
funkcionářů	funkcionář	k1gMnPc2
na	na	k7c6
nižších	nízký	k2eAgInPc6d2
organizačních	organizační	k2eAgInPc6d1
stupních	stupeň	k1gInPc6
řešit	řešit	k5eAaImF
situaci	situace	k1gFnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
dát	dát	k5eAaPmF
návrh	návrh	k1gInSc4
na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
zbavení	zbavení	k1gNnSc4
funkce	funkce	k1gFnSc2
<g/>
,	,	kIx,
zajišťovat	zajišťovat	k5eAaImF
informační	informační	k2eAgInSc4d1
servis	servis	k1gInSc4
všem	všecek	k3xTgFnPc3
nižším	nízký	k2eAgFnPc3d2
organizačním	organizační	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
<g/>
,	,	kIx,
vydávat	vydávat	k5eAaPmF,k5eAaImF
souhlas	souhlas	k1gInSc4
ke	k	k7c3
kandidatuře	kandidatura	k1gFnSc3
člena	člen	k1gMnSc2
SZ	SZ	kA
za	za	k7c4
jiný	jiný	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
subjekt	subjekt	k1gInSc4
nebo	nebo	k8xC
jako	jako	k9
nezávislého	závislý	k2eNgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsednictvo	předsednictvo	k1gNnSc1
dále	daleko	k6eAd2
může	moct	k5eAaImIp3nS
vytvářet	vytvářet	k5eAaImF
poradní	poradní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
(	(	kIx(
<g/>
pracovní	pracovní	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stranu	strana	k1gFnSc4
zelených	zelená	k1gFnPc2
navenek	navenek	k6eAd1
reprezentuje	reprezentovat	k5eAaImIp3nS
sjezdem	sjezd	k1gInSc7
volená	volený	k2eAgFnSc1d1
dvojice	dvojice	k1gFnSc1
spolupředsedkyně	spolupředsedkyně	k1gFnSc2
a	a	k8xC
spolupředsedy	spolupředseda	k1gMnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
zároveň	zároveň	k6eAd1
rozhodují	rozhodovat	k5eAaImIp3nP
v	v	k7c6
neodkladných	odkladný	k2eNgFnPc6d1
záležitostech	záležitost	k1gFnPc6
a	a	k8xC
provádí	provádět	k5eAaImIp3nS
neodkladné	odkladný	k2eNgInPc4d1
úkony	úkon	k1gInPc4
<g/>
,	,	kIx,
nevyžadují	vyžadovat	k5eNaImIp3nP
<g/>
-li	-li	k?
souhlas	souhlas	k1gInSc4
jiného	jiný	k2eAgInSc2d1
orgánu	orgán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místopředsedové	místopředseda	k1gMnPc1
zastupují	zastupovat	k5eAaImIp3nP
spolupředsedy	spolupředseda	k1gMnPc4
v	v	k7c6
době	doba	k1gFnSc6
jejich	jejich	k3xOp3gFnSc2
nepřítomnosti	nepřítomnost	k1gFnSc2
nebo	nebo	k8xC
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nemůžou	nemůžou	k?
vykonávat	vykonávat	k5eAaImF
svoji	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
zejména	zejména	k9
ze	z	k7c2
zdravotních	zdravotní	k2eAgInPc2d1
<g/>
,	,	kIx,
pracovních	pracovní	k2eAgInPc2d1
či	či	k8xC
jiných	jiný	k2eAgInPc2d1
závažných	závažný	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dohled	dohled	k1gInSc1
nad	nad	k7c7
dodržováním	dodržování	k1gNnSc7
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
stanov	stanova	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
vnitrostranických	vnitrostranický	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
zabezpečuje	zabezpečovat	k5eAaImIp3nS
nejméně	málo	k6eAd3
sedmičlenná	sedmičlenný	k2eAgFnSc1d1
ústřední	ústřední	k2eAgFnSc1d1
revizní	revizní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
(	(	kIx(
<g/>
ÚRK	ÚRK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
volena	volit	k5eAaImNgFnS
sjezdem	sjezd	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
pravomocí	pravomoc	k1gFnSc7
je	být	k5eAaImIp3nS
mj.	mj.	kA
v	v	k7c6
případě	případ	k1gInSc6
porušení	porušení	k1gNnSc2
stanov	stanova	k1gFnPc2
navrhovat	navrhovat	k5eAaImF
pořádková	pořádkový	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
včetně	včetně	k7c2
vyloučení	vyloučení	k1gNnSc2
členů	člen	k1gInPc2
a	a	k8xC
zrušení	zrušení	k1gNnSc4
organizačních	organizační	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontrolu	kontrola	k1gFnSc4
hospodaření	hospodaření	k1gNnSc4
strany	strana	k1gFnSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
finanční	finanční	k2eAgFnSc1d1
kontrolní	kontrolní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
(	(	kIx(
<g/>
FKK	FKK	kA
<g/>
)	)	kIx)
volená	volený	k2eAgFnSc1d1
republikovou	republikový	k2eAgFnSc7d1
radou	rada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
řešení	řešení	k1gNnSc3
sporů	spor	k1gInPc2
mezi	mezi	k7c4
členy	člen	k1gMnPc4
SZ	SZ	kA
<g/>
,	,	kIx,
orgány	orgán	k1gInPc7
a	a	k8xC
organizačními	organizační	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
strany	strana	k1gFnSc2
a	a	k8xC
dále	daleko	k6eAd2
k	k	k7c3
rozhodování	rozhodování	k1gNnSc3
o	o	k7c4
odvolání	odvolání	k1gNnSc4
proti	proti	k7c3
uložení	uložení	k1gNnSc3
pořádkových	pořádkový	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
vyloučení	vyloučení	k1gNnSc2
ze	z	k7c2
strany	strana	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
zřizují	zřizovat	k5eAaImIp3nP
rozhodčí	rozhodčí	k1gMnPc1
a	a	k8xC
smírčí	smírčí	k2eAgInPc1d1
orgány	orgán	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmi	ten	k3xDgInPc7
jsou	být	k5eAaImIp3nP
jednak	jednak	k8xC
ad	ad	k7c4
hoc	hoc	k?
rozhodčí	rozhodčí	k1gMnSc1
senáty	senát	k1gInPc4
<g/>
,	,	kIx,
zřizované	zřizovaný	k2eAgInPc4d1
na	na	k7c4
návrh	návrh	k1gInSc4
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
k	k	k7c3
řešení	řešení	k1gNnSc3
konkrétních	konkrétní	k2eAgInPc2d1
sporů	spor	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
nově	nově	k6eAd1
zavedený	zavedený	k2eAgMnSc1d1
stálý	stálý	k2eAgMnSc1d1
ombudsman	ombudsman	k1gMnSc1
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
zvolený	zvolený	k2eAgMnSc1d1
na	na	k7c6
sjezdu	sjezd	k1gInSc6
v	v	k7c6
listopadu	listopad	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Každodenní	každodenní	k2eAgInSc1d1
provoz	provoz	k1gInSc1
strany	strana	k1gFnSc2
zabezpečuje	zabezpečovat	k5eAaImIp3nS
hlavní	hlavní	k2eAgMnSc1d1
manažer	manažer	k1gMnSc1
jmenovaný	jmenovaný	k1gMnSc1
předsednictvem	předsednictvo	k1gNnSc7
SZ.	SZ.	k1gFnSc1
Odpovídá	odpovídat	k5eAaImIp3nS
za	za	k7c4
nakládání	nakládání	k1gNnSc4
s	s	k7c7
majetkem	majetek	k1gInSc7
SZ	SZ	kA
<g/>
,	,	kIx,
řídí	řídit	k5eAaImIp3nS
činnost	činnost	k1gFnSc4
hlavní	hlavní	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
(	(	kIx(
<g/>
HK	HK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
schválení	schválení	k1gNnSc6
PSZ	PSZ	kA
uzavírá	uzavírat	k5eAaPmIp3nS,k5eAaImIp3nS
a	a	k8xC
po	po	k7c6
vyjádření	vyjádření	k1gNnSc6
PSZ	PSZ	kA
ukončuje	ukončovat	k5eAaImIp3nS
pracovní	pracovní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
se	s	k7c7
zaměstnanci	zaměstnanec	k1gMnPc7
HK	HK	kA
<g/>
,	,	kIx,
na	na	k7c4
návrh	návrh	k1gInSc4
RKO	RKO	kA
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
a	a	k8xC
ukončuje	ukončovat	k5eAaImIp3nS
pracovní	pracovní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
se	s	k7c7
zaměstnanci	zaměstnanec	k1gMnPc7
působícími	působící	k2eAgMnPc7d1
v	v	k7c6
krajských	krajský	k2eAgFnPc6d1
kancelářích	kancelář	k1gFnPc6
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
jménem	jméno	k1gNnSc7
SZ	SZ	kA
v	v	k7c6
majetkových	majetkový	k2eAgInPc6d1
<g/>
,	,	kIx,
hospodářských	hospodářský	k2eAgInPc6d1
<g/>
,	,	kIx,
pracovních	pracovní	k2eAgInPc6d1
a	a	k8xC
právních	právní	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
v	v	k7c6
rozsahu	rozsah	k1gInSc6
pověření	pověření	k1gNnSc2
předsednictvem	předsednictvo	k1gNnSc7
SZ.	SZ.	k1gFnSc2
</s>
<s>
Odborné	odborný	k2eAgFnPc1d1
sekce	sekce	k1gFnPc1
</s>
<s>
Pro	pro	k7c4
hlubší	hluboký	k2eAgFnSc4d2
diskuzi	diskuze	k1gFnSc4
nad	nad	k7c7
konkrétními	konkrétní	k2eAgInPc7d1
programovými	programový	k2eAgInPc7d1
body	bod	k1gInPc7
zřizuje	zřizovat	k5eAaImIp3nS
předsednictvo	předsednictvo	k1gNnSc1
strany	strana	k1gFnSc2
odborné	odborný	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
jako	jako	k8xC,k8xS
poradní	poradní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gFnSc6
činnosti	činnost	k1gFnSc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
podílet	podílet	k5eAaImF
i	i	k9
nečlenové	nečlen	k1gMnPc1
SZ.	SZ.	k1gFnSc2
Odborné	odborný	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
připravují	připravovat	k5eAaImIp3nP
podklady	podklad	k1gInPc4
pro	pro	k7c4
stanoviska	stanovisko	k1gNnPc4
a	a	k8xC
programové	programový	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
SZ	SZ	kA
<g/>
,	,	kIx,
vymezují	vymezovat	k5eAaImIp3nP
a	a	k8xC
diskutují	diskutovat	k5eAaImIp3nP
konkrétní	konkrétní	k2eAgNnPc4d1
i	i	k8xC
obecná	obecný	k2eAgNnPc4d1
odborná	odborný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
<g/>
,	,	kIx,
projednávají	projednávat	k5eAaImIp3nP
plnění	plnění	k1gNnSc1
volebního	volební	k2eAgInSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
programu	program	k1gInSc2
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
zpracovávají	zpracovávat	k5eAaImIp3nP
expertní	expertní	k2eAgNnPc4d1
stanoviska	stanovisko	k1gNnPc4
nebo	nebo	k8xC
předkládají	předkládat	k5eAaImIp3nP
náměty	námět	k1gInPc4
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yIgFnPc3,k3yRgFnPc3,k3yQgFnPc3
by	by	kYmCp3nS
SZ	SZ	kA
měla	mít	k5eAaImAgFnS
zaujmout	zaujmout	k5eAaPmF
stanovisko	stanovisko	k1gNnSc4
<g/>
,	,	kIx,
připravují	připravovat	k5eAaImIp3nP
rešerše	rešerše	k1gFnPc1
a	a	k8xC
podklady	podklad	k1gInPc1
podle	podle	k7c2
zadání	zadání	k1gNnSc2
PSZ	PSZ	kA
<g/>
,	,	kIx,
sledují	sledovat	k5eAaImIp3nP
legislativní	legislativní	k2eAgFnPc4d1
změny	změna	k1gFnPc4
a	a	k8xC
komunikují	komunikovat	k5eAaImIp3nP
s	s	k7c7
parlamentními	parlamentní	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
,	,	kIx,
připomínkují	připomínkovat	k5eAaImIp3nP
právní	právní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
<g/>
,	,	kIx,
vyhledávají	vyhledávat	k5eAaImIp3nP
a	a	k8xC
vyzývají	vyzývat	k5eAaImIp3nP
<g />
.	.	kIx.
</s>
<s hack="1">
k	k	k7c3
diskusi	diskuse	k1gFnSc3
odborníky	odborník	k1gMnPc4
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
;	;	kIx,
pořádají	pořádat	k5eAaImIp3nP
odborné	odborný	k2eAgInPc4d1
semináře	seminář	k1gInPc4
nebo	nebo	k8xC
jinak	jinak	k6eAd1
prezentují	prezentovat	k5eAaBmIp3nP
politiku	politika	k1gFnSc4
SZ	SZ	kA
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
směrem	směr	k1gInSc7
ke	k	k7c3
členům	člen	k1gInPc3
i	i	k8xC
veřejnosti	veřejnost	k1gFnSc6
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
PSZ	PSZ	kA
a	a	k8xC
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
programovými	programový	k2eAgInPc7d1
cíli	cíl	k1gInPc7
<g/>
,	,	kIx,
programem	program	k1gInSc7
a	a	k8xC
vnitřními	vnitřní	k2eAgInPc7d1
předpisy	předpis	k1gInPc7
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
vyjadřují	vyjadřovat	k5eAaImIp3nP
se	se	k3xPyFc4
k	k	k7c3
dalším	další	k2eAgFnPc3d1
otázkám	otázka	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jim	on	k3xPp3gMnPc3
předloží	předložit	k5eAaPmIp3nP
příslušný	příslušný	k2eAgInSc4d1
orgán	orgán	k1gInSc4
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Historie	historie	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1989	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
léta	léto	k1gNnSc2
1989	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
Vimperku	Vimperk	k1gInSc6
uskutečnilo	uskutečnit	k5eAaPmAgNnS
setkání	setkání	k1gNnSc1
ekologických	ekologický	k2eAgInPc2d1
aktivistů	aktivista	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
promluvili	promluvit	k5eAaPmAgMnP
mj.	mj.	kA
poslanec	poslanec	k1gMnSc1
Bundestagu	Bundestag	k1gInSc2
za	za	k7c2
německé	německý	k2eAgFnSc2d1
zelené	zelená	k1gFnSc2
Milan	Milan	k1gMnSc1
Horáček	Horáček	k1gMnSc1
nebo	nebo	k8xC
ekolog	ekolog	k1gMnSc1
a	a	k8xC
signatář	signatář	k1gMnSc1
Charty	charta	k1gFnSc2
77	#num#	k4
Ivan	Ivan	k1gMnSc1
Dejmal	Dejmal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
rozdrobené	rozdrobený	k2eAgInPc1d1
ekologické	ekologický	k2eAgInPc1d1
spolky	spolek	k1gInPc1
a	a	k8xC
uskupení	uskupení	k1gNnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
sjednotily	sjednotit	k5eAaPmAgFnP
do	do	k7c2
Ekologického	ekologický	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
postupně	postupně	k6eAd1
transformovat	transformovat	k5eAaBmF
na	na	k7c4
Československou	československý	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
zelených	zelená	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgInPc2
zdrojů	zdroj	k1gInPc2
zhatila	zhatit	k5eAaPmAgFnS
vznik	vznik	k1gInSc4
strany	strana	k1gFnSc2
před	před	k7c7
listopadem	listopad	k1gInSc7
1989	#num#	k4
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
zaregistrována	zaregistrovat	k5eAaPmNgFnS
byla	být	k5eAaImAgFnS
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1990	#num#	k4
a	a	k8xC
ustavující	ustavující	k2eAgInSc1d1
sjezd	sjezd	k1gInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1990	#num#	k4
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
byly	být	k5eAaImAgInP
zřízeny	zřízen	k2eAgInPc1d1
tři	tři	k4xCgInPc1
zemské	zemský	k2eAgInPc1d1
bloky	blok	k1gInPc1
(	(	kIx(
<g/>
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
moravský	moravský	k2eAgMnSc1d1
a	a	k8xC
slovenský	slovenský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
blok	blok	k1gInSc1
pražský	pražský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prvních	první	k4xOgFnPc6
svobodných	svobodný	k2eAgFnPc6d1
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
červnu	červen	k1gInSc6
1990	#num#	k4
SZ	SZ	kA
kandidovala	kandidovat	k5eAaImAgFnS
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
pětiprocentní	pětiprocentní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
parlamentu	parlament	k1gInSc2
těsně	těsně	k6eAd1
nepřekonala	překonat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neuspěla	uspět	k5eNaPmAgFnS
ani	ani	k9
v	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
na	na	k7c4
podzim	podzim	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
například	například	k6eAd1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
SZ	SZ	kA
výrazně	výrazně	k6eAd1
uspěla	uspět	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInSc4
kandidáti	kandidát	k1gMnPc1
kandidující	kandidující	k2eAgMnPc1d1
společně	společně	k6eAd1
s	s	k7c7
OF	OF	kA
v	v	k7c6
tzv.	tzv.	kA
Chartě	charta	k1gFnSc6
demokratické	demokratický	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
obdrželi	obdržet	k5eAaPmAgMnP
až	až	k6eAd1
45	#num#	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
SZ	SZ	kA
zde	zde	k6eAd1
získala	získat	k5eAaPmAgFnS
posty	posta	k1gFnPc4
náměstka	náměstek	k1gMnSc2
primátora	primátor	k1gMnSc2
(	(	kIx(
<g/>
Leopold	Leopold	k1gMnSc1
Kukačka	kukačka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
starosty	starosta	k1gMnSc2
obvodu	obvod	k1gInSc6
Neštěmice	Neštěmika	k1gFnSc3
(	(	kIx(
<g/>
Václav	Václav	k1gMnSc1
Pucherna	Pucherna	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
zástupce	zástupce	k1gMnSc1
starosty	starosta	k1gMnSc2
obvodu	obvod	k1gInSc2
Ústí	ústit	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
-	-	kIx~
město	město	k1gNnSc1
(	(	kIx(
<g/>
Marian	Marian	k1gMnSc1
Páleník	Páleník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
skončila	skončit	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
nejúspěšnější	úspěšný	k2eAgFnSc1d3
a	a	k8xC
získala	získat	k5eAaPmAgFnS
více	hodně	k6eAd2
mandátů	mandát	k1gInPc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
než	než	k8xS
např.	např.	kA
lidovci	lidovec	k1gMnPc7
nebo	nebo	k8xC
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
kandidovala	kandidovat	k5eAaImAgFnS
v	v	k7c6
koalici	koalice	k1gFnSc6
se	s	k7c7
Zemědělskou	zemědělský	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
a	a	k8xC
Československou	československý	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
socialistickou	socialistický	k2eAgFnSc7d1
v	v	k7c6
seskupení	seskupení	k1gNnSc6
nazvaném	nazvaný	k2eAgNnSc6d1
Liberálně	liberálně	k6eAd1
sociální	sociální	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejím	její	k3xOp3gInSc6
rámci	rámec	k1gInSc6
získala	získat	k5eAaPmAgFnS
SZ	SZ	kA
po	po	k7c6
třech	tři	k4xCgMnPc6
poslancích	poslanec	k1gMnPc6
ve	v	k7c6
Federálním	federální	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
i	i	k8xC
v	v	k7c6
České	český	k2eAgFnSc6d1
národní	národní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1993	#num#	k4
rozhodl	rozhodnout	k5eAaPmAgInS
celostátní	celostátní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
strany	strana	k1gFnSc2
o	o	k7c4
vystoupení	vystoupení	k1gNnSc4
z	z	k7c2
LSU	LSU	kA
<g/>
,	,	kIx,
poslanecký	poslanecký	k2eAgInSc1d1
klub	klub	k1gInSc1
unie	unie	k1gFnSc2
zelení	zeleň	k1gFnPc2
poslanci	poslanec	k1gMnPc1
opustili	opustit	k5eAaPmAgMnP
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
v	v	k7c6
Černé	Černá	k1gFnSc6
v	v	k7c6
Pošumaví	Pošumaví	k1gNnSc6
v	v	k7c6
prosinci	prosinec	k1gInSc6
1994	#num#	k4
byl	být	k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
znovuzvolen	znovuzvolit	k5eAaPmNgMnS
Jaroslav	Jaroslav	k1gMnSc1
Vlček	Vlček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
1995	#num#	k4
Vlček	Vlček	k1gMnSc1
odešel	odejít	k5eAaPmAgMnS
ze	z	k7c2
SZ	SZ	kA
do	do	k7c2
České	český	k2eAgFnSc2d1
strany	strana	k1gFnSc2
sociálně	sociálně	k6eAd1
demokratické	demokratický	k2eAgFnSc2d1
(	(	kIx(
<g/>
odešli	odejít	k5eAaPmAgMnP
s	s	k7c7
ním	on	k3xPp3gMnSc7
i	i	k9
dva	dva	k4xCgMnPc1
místopředsedové	místopředseda	k1gMnPc1
a	a	k8xC
nemalá	malý	k2eNgFnSc1d1
část	část	k1gFnSc1
členské	členský	k2eAgFnSc2d1
základny	základna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
výkonný	výkonný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
strany	strana	k1gFnSc2
zvolil	zvolit	k5eAaPmAgInS
Emila	Emil	k1gMnSc4
Zemana	Zeman	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sněmovních	sněmovní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
strana	strana	k1gFnSc1
nekandidovala	kandidovat	k5eNaImAgFnS
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
získala	získat	k5eAaPmAgFnS
1,1	1,1	k4
%	%	kIx~
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
2,4	2,4	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
voleb	volba	k1gFnPc2
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
(	(	kIx(
<g/>
zisk	zisk	k1gInSc1
3,2	3,2	k4
%	%	kIx~
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
SZ	SZ	kA
status	status	k1gInSc4
nejsilnější	silný	k2eAgFnSc2d3
mimoparlamentní	mimoparlamentní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Tmavozelený	tmavozelený	k2eAgInSc1d1
převrat	převrat	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc1
v	v	k7c6
letech	let	k1gInPc6
2002	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
2002	#num#	k4
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
získala	získat	k5eAaPmAgFnS
i	i	k8xC
díky	díky	k7c3
podpoře	podpora	k1gFnSc3
Brandýského	brandýský	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
nevládních	vládní	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
2,36	2,36	k4
%	%	kIx~
a	a	k8xC
vyšlo	vyjít	k5eAaPmAgNnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vedení	vedení	k1gNnSc1
hodlá	hodlat	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
státního	státní	k2eAgInSc2d1
příspěvku	příspěvek	k1gInSc2
za	za	k7c4
hlasy	hlas	k1gInPc4
vyplatit	vyplatit	k5eAaPmF
hlavnímu	hlavní	k2eAgMnSc3d1
manažerovi	manažer	k1gMnSc3
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
financoval	financovat	k5eAaBmAgInS
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
volební	volební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nato	nato	k6eAd1
přední	přední	k2eAgMnSc1d1
člen	člen	k1gMnSc1
BF	BF	kA
<g/>
,	,	kIx,
ekologický	ekologický	k2eAgMnSc1d1
aktivista	aktivista	k1gMnSc1
a	a	k8xC
šéfredaktor	šéfredaktor	k1gMnSc1
Literárních	literární	k2eAgFnPc2d1
novin	novina	k1gFnPc2
Jakub	Jakub	k1gMnSc1
Patočka	Patočka	k1gMnSc1
vyhlásil	vyhlásit	k5eAaPmAgMnS
akci	akce	k1gFnSc4
Zelená	Zelená	k1gFnSc1
padesátka	padesátka	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
později	pozdě	k6eAd2
známou	známý	k2eAgFnSc4d1
též	též	k9
jako	jako	k8xC,k8xS
tmavozelená	tmavozelený	k2eAgFnSc1d1
výzva	výzva	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
SZ	SZ	kA
hromadně	hromadně	k6eAd1
vstoupili	vstoupit	k5eAaPmAgMnP
lidé	člověk	k1gMnPc1
z	z	k7c2
prostředí	prostředí	k1gNnSc2
ekologických	ekologický	k2eAgFnPc2d1
občanských	občanský	k2eAgFnPc2d1
iniciativ	iniciativa	k1gFnPc2
s	s	k7c7
cílem	cíl	k1gInSc7
přečíslit	přečíslit	k5eAaPmF
původní	původní	k2eAgInPc4d1
členy	člen	k1gInPc4
(	(	kIx(
<g/>
„	„	k?
<g/>
starozelené	starozelený	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
změnit	změnit	k5eAaPmF
politický	politický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
směřování	směřování	k1gNnSc4
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
členů	člen	k1gMnPc2
se	se	k3xPyFc4
během	během	k7c2
roku	rok	k1gInSc2
ztrojnásobil	ztrojnásobit	k5eAaPmAgInS
z	z	k7c2
250	#num#	k4
na	na	k7c4
750	#num#	k4
s	s	k7c7
řadou	řada	k1gFnSc7
známých	známý	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
jako	jako	k8xS,k8xC
Ivan	Ivan	k1gMnSc1
Dejmal	Dejmal	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Jech	Jech	k1gMnSc1
<g/>
,	,	kIx,
Dalibor	Dalibor	k1gMnSc1
Stráský	Stráský	k1gMnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
Vavroušková	Vavroušková	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Wünsch	Wünsch	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
Lužný	Lužný	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Uhl	Uhl	k1gMnSc1
<g/>
,	,	kIx,
Fedor	Fedor	k1gMnSc1
Gál	Gál	k1gMnSc1
<g/>
,	,	kIx,
Lumír	Lumír	k1gMnSc1
Kolíbal	kolíbat	k5eAaImAgMnS
či	či	k8xC
Jana	Jana	k1gFnSc1
Strunecká	Strunecký	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
mimořádném	mimořádný	k2eAgInSc6d1
brněnském	brněnský	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
v	v	k7c6
dubnu	duben	k1gInSc6
2003	#num#	k4
tak	tak	k6eAd1
byl	být	k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
zvolen	zvolit	k5eAaPmNgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
iniciátorů	iniciátor	k1gMnPc2
„	„	k?
<g/>
revoluce	revoluce	k1gFnSc2
<g/>
“	“	k?
Jan	Jan	k1gMnSc1
Beránek	Beránek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nová	k1gFnSc6
vedení	vedení	k1gNnSc1
jednalo	jednat	k5eAaImAgNnS
s	s	k7c7
bývalým	bývalý	k2eAgMnSc7d1
ministrem	ministr	k1gMnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
v	v	k7c6
Tošovského	Tošovského	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
Martinem	Martin	k1gMnSc7
Bursíkem	Bursík	k1gMnSc7
o	o	k7c6
možnosti	možnost	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
vedl	vést	k5eAaImAgInS
kandidátku	kandidátka	k1gFnSc4
SZ	SZ	kA
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
červnu	červen	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dohodě	dohoda	k1gFnSc3
však	však	k9
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
lídr	lídr	k1gMnSc1
kandidátky	kandidátka	k1gFnSc2
nebyl	být	k5eNaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
ani	ani	k8xC
na	na	k7c6
sjezdu	sjezd	k1gInSc6
konaném	konaný	k2eAgInSc6d1
v	v	k7c6
září	září	k1gNnSc6
2003	#num#	k4
v	v	k7c6
Táboře	Tábor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Republiková	republikový	k2eAgFnSc1d1
rada	rada	k1gFnSc1
nakonec	nakonec	k6eAd1
z	z	k7c2
trojice	trojice	k1gFnSc2
Jakub	Jakub	k1gMnSc1
Patočka	Patočka	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Jařab	Jařab	k1gMnSc1
<g/>
,	,	kIx,
Bursík	Bursík	k1gMnSc1
vybrala	vybrat	k5eAaPmAgFnS
Beránkova	Beránkův	k2eAgMnSc4d1
přítele	přítel	k1gMnSc4
Patočku	Patočka	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
získala	získat	k5eAaPmAgFnS
3,16	3,16	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
do	do	k7c2
EP	EP	kA
se	se	k3xPyFc4
tak	tak	k6eAd1
nedostala	dostat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Straně	strana	k1gFnSc6
zelených	zelená	k1gFnPc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
vytvořily	vytvořit	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
znepřátelené	znepřátelený	k2eAgFnPc1d1
frakce	frakce	k1gFnPc1
<g/>
:	:	kIx,
příznivci	příznivec	k1gMnPc1
Beránkova	Beránkův	k2eAgNnSc2d1
vedení	vedení	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
odpůrci	odpůrce	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
vedení	vedený	k2eAgMnPc1d1
strany	strana	k1gFnSc2
obviňovali	obviňovat	k5eAaImAgMnP
z	z	k7c2
autoritářských	autoritářský	k2eAgFnPc2d1
a	a	k8xC
sektářských	sektářský	k2eAgFnPc2d1
praktik	praktika	k1gFnPc2
a	a	k8xC
kritizovali	kritizovat	k5eAaImAgMnP
radikalismus	radikalismus	k1gInSc4
Patočkou	Patočka	k1gMnSc7
prosazeného	prosazený	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
programu	program	k1gInSc2
Vize	vize	k1gFnSc2
ekologické	ekologický	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
„	„	k?
<g/>
nové	nový	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
resp.	resp.	kA
„	„	k?
<g/>
neodemokracie	neodemokracie	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
listopadu	listopad	k1gInSc2
2003	#num#	k4
protiberánkovská	protiberánkovský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
Prohlášení	prohlášení	k1gNnSc4
demokratů	demokrat	k1gMnPc2
v	v	k7c6
SZ	SZ	kA
a	a	k8xC
Přehled	přehled	k1gInSc1
některých	některý	k3yIgNnPc2
porušení	porušení	k1gNnPc2
stanov	stanova	k1gFnPc2
a	a	k8xC
demokratických	demokratický	k2eAgInPc2d1
postupů	postup	k1gInPc2
v	v	k7c6
SZ.	SZ.	k1gFnSc6
I	i	k9
tuto	tento	k3xDgFnSc4
kritiku	kritika	k1gFnSc4
poměrů	poměr	k1gInPc2
ve	v	k7c6
straně	strana	k1gFnSc6
vedení	vedení	k1gNnSc1
odmítlo	odmítnout	k5eAaPmAgNnS
jako	jako	k9
neopodstatněnou	opodstatněný	k2eNgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2004	#num#	k4
někteří	některý	k3yIgMnPc1
signatáři	signatář	k1gMnPc1
Prohlášení	prohlášení	k1gNnSc1
demokratů	demokrat	k1gMnPc2
založili	založit	k5eAaPmAgMnP
Otevřenou	otevřený	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
demokratizace	demokratizace	k1gFnSc1
vnitrostranického	vnitrostranický	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
před	před	k7c7
sjezdem	sjezd	k1gInSc7
ji	on	k3xPp3gFnSc4
formálně	formálně	k6eAd1
rozpustili	rozpustit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
v	v	k7c6
září	září	k1gNnSc6
2004	#num#	k4
Beránek	Beránek	k1gMnSc1
předsednictví	předsednictví	k1gNnSc2
těsně	těsně	k6eAd1
obhájil	obhájit	k5eAaPmAgInS
v	v	k7c6
souboji	souboj	k1gInSc6
s	s	k7c7
Petrem	Petr	k1gMnSc7
Štěpánkem	Štěpánek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitrostranická	vnitrostranický	k2eAgFnSc1d1
opozice	opozice	k1gFnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
sjednotila	sjednotit	k5eAaPmAgFnS
kolem	kolem	k7c2
Martina	Martin	k1gMnSc2
Bursíka	Bursík	k1gMnSc2
a	a	k8xC
postupně	postupně	k6eAd1
získala	získat	k5eAaPmAgFnS
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mimořádném	mimořádný	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
24	#num#	k4
<g/>
.	.	kIx.
až	až	k9
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
tak	tak	k9
už	už	k6eAd1
Beránek	Beránek	k1gMnSc1
znovu	znovu	k6eAd1
nekandidoval	kandidovat	k5eNaImAgMnS
a	a	k8xC
novým	nový	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Bursík	Bursík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
Beránkových	Beránkových	k2eAgMnPc2d1
a	a	k8xC
Patočkových	Patočkových	k2eAgMnPc2d1
příznivců	příznivec	k1gMnPc2
nato	nato	k6eAd1
vystoupilo	vystoupit	k5eAaPmAgNnS
ze	z	k7c2
strany	strana	k1gFnSc2
a	a	k8xC
koncem	koncem	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
vytvořili	vytvořit	k5eAaPmAgMnP
politické	politický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
Zelení	zeleň	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
však	však	k9
vyvíjí	vyvíjet	k5eAaImIp3nP
jen	jen	k9
minimální	minimální	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sjezd	sjezd	k1gInSc4
také	také	k9
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
SZ	SZ	kA
půjde	jít	k5eAaImIp3nS
do	do	k7c2
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
2006	#num#	k4
samostatně	samostatně	k6eAd1
a	a	k8xC
ne	ne	k9
v	v	k7c6
koalici	koalice	k1gFnSc6
s	s	k7c7
menšími	malý	k2eAgFnPc7d2
liberálními	liberální	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
prosazoval	prosazovat	k5eAaImAgMnS
Beránek	Beránek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bursík	Bursík	k1gMnSc1
následně	následně	k6eAd1
zmírnil	zmírnit	k5eAaPmAgMnS
vymezování	vymezování	k1gNnSc4
SZ	SZ	kA
proti	proti	k7c3
ODS	ODS	kA
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
charakteristické	charakteristický	k2eAgNnSc1d1
pro	pro	k7c4
Patočkovo	Patočkův	k2eAgNnSc4d1
vystupování	vystupování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Úspěch	úspěch	k1gInSc1
ve	v	k7c6
volbách	volba	k1gFnPc6
2006	#num#	k4
</s>
<s>
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgMnPc1d1
její	její	k3xOp3gMnPc1
členové	člen	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
února	únor	k1gInSc2
2006	#num#	k4
překročily	překročit	k5eAaPmAgFnP
volební	volební	k2eAgFnPc1d1
preference	preference	k1gFnPc1
SZ	SZ	kA
5	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
potřebných	potřebný	k2eAgFnPc2d1
pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
značný	značný	k2eAgInSc4d1
zájem	zájem	k1gInSc4
médií	médium	k1gNnPc2
o	o	k7c4
stranu	strana	k1gFnSc4
a	a	k8xC
zjevně	zjevně	k6eAd1
další	další	k2eAgInSc4d1
růst	růst	k1gInSc4
preferencí	preference	k1gFnPc2
od	od	k7c2
nerozhodnutých	rozhodnutý	k2eNgMnPc2d1
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
se	se	k3xPyFc4
většina	většina	k1gFnSc1
agentur	agentura	k1gFnPc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
shodla	shodnout	k5eAaBmAgFnS,k5eAaPmAgFnS
na	na	k7c4
10	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
%	%	kIx~
preferencích	preference	k1gFnPc6
(	(	kIx(
<g/>
v	v	k7c6
Praze	Praha	k1gFnSc6
až	až	k9
17	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bursík	Bursík	k1gMnSc1
se	se	k3xPyFc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
cílem	cíl	k1gInSc7
strany	strana	k1gFnSc2
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
dvouciferný	dvouciferný	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
ve	v	k7c6
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
podpořil	podpořit	k5eAaPmAgMnS
SZ	SZ	kA
a	a	k8xC
jmenovitě	jmenovitě	k6eAd1
středočeské	středočeský	k2eAgMnPc4d1
kandidáty	kandidát	k1gMnPc4
Matěje	Matěj	k1gMnSc4
Stropnického	stropnický	k2eAgMnSc4d1
a	a	k8xC
Danielu	Daniela	k1gFnSc4
Matějkovou	Matějková	k1gFnSc4
<g/>
,	,	kIx,
blízké	blízký	k2eAgFnPc1d1
Patočkovi	Patočka	k1gMnSc3
a	a	k8xC
vedoucí	vedoucí	k2eAgFnSc4d1
osobní	osobní	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
z	z	k7c2
nevolitelných	volitelný	k2eNgNnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
následně	následně	k6eAd1
podpořil	podpořit	k5eAaPmAgInS
i	i	k9
pražské	pražský	k2eAgMnPc4d1
lídry	lídr	k1gMnPc4
Bursíka	Bursík	k1gMnSc4
a	a	k8xC
Kateřinu	Kateřina	k1gFnSc4
Jacques	Jacques	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Necelý	celý	k2eNgInSc4d1
týden	týden	k1gInSc4
před	před	k7c7
volbami	volba	k1gFnPc7
byla	být	k5eAaImAgFnS
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnSc2
kandidátky	kandidátka	k1gFnSc2
Moravskoslezského	moravskoslezský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
vyškrtnuta	vyškrtnut	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
republikové	republikový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Eva	Eva	k1gFnSc1
Holubová	Holubová	k1gFnSc1
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
vyhlásila	vyhlásit	k5eAaPmAgNnP
utvoření	utvoření	k1gNnPc1
tzv.	tzv.	kA
levé	levý	k2eAgFnSc2d1
frakce	frakce	k1gFnSc2
(	(	kIx(
<g/>
dle	dle	k7c2
vedení	vedení	k1gNnSc2
strany	strana	k1gFnSc2
a	a	k8xC
některých	některý	k3yIgInPc2
členů	člen	k1gInPc2
SZ	SZ	kA
<g/>
,	,	kIx,
popřevších	popřevší	k2eAgNnPc2d1
tvrzení	tvrzení	k1gNnPc2
Holubové	Holubová	k1gFnSc2
o	o	k7c4
jejich	jejich	k3xOp3gFnPc4
příslušnosti	příslušnost	k1gFnPc4
ke	k	k7c3
frakci	frakce	k1gFnSc3
<g/>
,	,	kIx,
fiktivní	fiktivní	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
vedla	vést	k5eAaImAgFnS
stranou	strana	k1gFnSc7
neschválená	schválený	k2eNgNnPc1d1
jednání	jednání	k1gNnPc1
s	s	k7c7
předsedou	předseda	k1gMnSc7
ČSSD	ČSSD	kA
Jiřím	Jiří	k1gMnSc7
Paroubkem	Paroubek	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Republiková	republikový	k2eAgFnSc1d1
rada	rada	k1gFnSc1
ji	on	k3xPp3gFnSc4
po	po	k7c6
volbách	volba	k1gFnPc6
vyloučila	vyloučit	k5eAaPmAgFnS
ze	z	k7c2
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
SZ	SZ	kA
získala	získat	k5eAaPmAgFnS
6,29	6,29	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
šest	šest	k4xCc1
poslaneckých	poslanecký	k2eAgNnPc2d1
křesel	křeslo	k1gNnPc2
(	(	kIx(
<g/>
3	#num#	k4
%	%	kIx~
mandátů	mandát	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepšího	dobrý	k2eAgInSc2d3
výsledku	výsledek	k1gInSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
v	v	k7c6
Libereckém	liberecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
(	(	kIx(
<g/>
9,58	9,58	k4
%	%	kIx~
<g/>
)	)	kIx)
patrně	patrně	k6eAd1
díky	díky	k7c3
silné	silný	k2eAgFnSc3d1
podpoře	podpora	k1gFnSc3
Petra	Petr	k1gMnSc2
Pávka	pávek	k1gMnSc2
(	(	kIx(
<g/>
získal	získat	k5eAaPmAgInS
největší	veliký	k2eAgInSc1d3
podíl	podíl	k1gInSc1
preferenčních	preferenční	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
mezi	mezi	k7c7
kandidáty	kandidát	k1gMnPc7
SZ	SZ	kA
<g/>
,	,	kIx,
17	#num#	k4
%	%	kIx~
a	a	k8xC
v	v	k7c6
Jindřichovicích	Jindřichovice	k1gFnPc6
pod	pod	k7c7
Smrkem	smrk	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
starostou	starosta	k1gMnSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
SZ	SZ	kA
se	s	k7c7
ziskem	zisk	k1gInSc7
32,95	32,95	k4
%	%	kIx~
absolutním	absolutní	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
paradoxně	paradoxně	k6eAd1
tam	tam	k6eAd1
mandát	mandát	k1gInSc4
o	o	k7c4
266	#num#	k4
hlasů	hlas	k1gInPc2
nezískala	získat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Královéhradeckém	královéhradecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
unikl	uniknout	k5eAaPmAgInS
mandát	mandát	k1gInSc1
Stanislavu	Stanislava	k1gFnSc4
Pencovi	Pencův	k2eAgMnPc1d1
o	o	k7c4
75	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
získala	získat	k5eAaPmAgFnS
SZ	SZ	kA
9,19	9,19	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
dva	dva	k4xCgMnPc4
poslance	poslanec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepoměr	nepoměr	k1gInSc1
mezi	mezi	k7c7
počtem	počet	k1gInSc7
hlasů	hlas	k1gInPc2
a	a	k8xC
mandátů	mandát	k1gInPc2
byl	být	k5eAaImAgMnS
zapříčiněn	zapříčiněn	k2eAgMnSc1d1
některými	některý	k3yIgInPc7
prvky	prvek	k1gInPc7
volebního	volební	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
především	především	k6eAd1
počtem	počet	k1gInSc7
a	a	k8xC
velikostí	velikost	k1gFnSc7
14	#num#	k4
volebních	volební	k2eAgInPc2d1
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zvyšují	zvyšovat	k5eAaImIp3nP
tzv.	tzv.	kA
přirozenou	přirozený	k2eAgFnSc4d1
klauzuli	klauzule	k1gFnSc4
pro	pro	k7c4
získání	získání	k1gNnSc4
mandátu	mandát	k1gInSc2
v	v	k7c6
menších	malý	k2eAgInPc6d2
krajích	kraj	k1gInPc6
nad	nad	k7c7
10	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečný	částečný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
měla	mít	k5eAaImAgFnS
též	též	k9
použitá	použitý	k2eAgFnSc1d1
d	d	k?
<g/>
'	'	kIx"
<g/>
Hondtova	Hondtův	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
přepočítávání	přepočítávání	k1gNnSc2
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
zisk	zisk	k1gInSc4
větších	veliký	k2eAgFnPc2d2
stran	strana	k1gFnPc2
na	na	k7c4
úkor	úkor	k1gInSc4
menších	malý	k2eAgFnPc2d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
nevýhodný	výhodný	k2eNgInSc1d1
pro	pro	k7c4
menší	malý	k2eAgFnPc4d2
strany	strana	k1gFnPc4
s	s	k7c7
vyrovnanou	vyrovnaný	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
bez	bez	k7c2
silné	silný	k2eAgFnSc2d1
regionální	regionální	k2eAgFnSc2d1
základny	základna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Účast	účast	k1gFnSc1
ve	v	k7c6
vládě	vláda	k1gFnSc6
po	po	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
</s>
<s>
Vnitrostranický	vnitrostranický	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
</s>
<s>
Po	po	k7c6
vstupu	vstup	k1gInSc6
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
do	do	k7c2
pravicové	pravicový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
s	s	k7c7
ODS	ODS	kA
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gFnSc1
se	se	k3xPyFc4
ve	v	k7c6
straně	strana	k1gFnSc6
zformovaly	zformovat	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
frakce	frakce	k1gFnPc1
–	–	k?
příznivci	příznivec	k1gMnPc1
této	tento	k3xDgFnSc2
spolupráce	spolupráce	k1gFnSc2
(	(	kIx(
<g/>
zelení	zelenit	k5eAaImIp3nP
liberálové	liberál	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gMnPc1
odpůrci	odpůrce	k1gMnPc1
(	(	kIx(
<g/>
levicovější	levicový	k2eAgFnSc2d2
frakce	frakce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
spokojeni	spokojen	k2eAgMnPc1d1
s	s	k7c7
účastí	účast	k1gFnSc7
strany	strana	k1gFnSc2
v	v	k7c6
pravicové	pravicový	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
,	,	kIx,
argumentovali	argumentovat	k5eAaImAgMnP
výrazným	výrazný	k2eAgNnSc7d1
uplatněním	uplatnění	k1gNnSc7
programu	program	k1gInSc2
SZ	SZ	kA
ve	v	k7c6
vládním	vládní	k2eAgInSc6d1
programu	program	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
převyšoval	převyšovat	k5eAaImAgInS
sílu	síla	k1gFnSc4
šesti	šest	k4xCc2
zelených	zelený	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
–	–	k?
3	#num#	k4
%	%	kIx~
v	v	k7c6
poslanecké	poslanecký	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgMnSc3
kritici	kritik	k1gMnPc1
tvrdili	tvrdit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
prosazení	prosazení	k1gNnSc1
programu	program	k1gInSc2
při	při	k7c6
jednání	jednání	k1gNnSc6
o	o	k7c6
koalici	koalice	k1gFnSc6
s	s	k7c7
ČSSD	ČSSD	kA
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
ještě	ještě	k6eAd1
výraznější	výrazný	k2eAgNnSc1d2
a	a	k8xC
že	že	k8xS
účast	účast	k1gFnSc1
v	v	k7c6
pravicové	pravicový	k2eAgFnSc6d1
koalici	koalice	k1gFnSc6
byla	být	k5eAaImAgFnS
jejími	její	k3xOp3gNnPc7
zastánci	zastánce	k1gMnPc1
udržována	udržovat	k5eAaImNgFnS
i	i	k9
na	na	k7c4
úkor	úkor	k1gInSc4
některých	některý	k3yIgFnPc2
programových	programový	k2eAgFnPc2d1
priorit	priorita	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
radar	radar	k1gInSc1
v	v	k7c6
Brdech	brdo	k1gNnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konflikt	konflikt	k1gInSc1
se	se	k3xPyFc4
vyhrotil	vyhrotit	k5eAaPmAgInS
v	v	k7c6
poslaneckém	poslanecký	k2eAgInSc6d1
klubu	klub	k1gInSc6
SZ	SZ	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
skupinu	skupina	k1gFnSc4
příznivců	příznivec	k1gMnPc2
aktivní	aktivní	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
ODS	ODS	kA
tvořili	tvořit	k5eAaImAgMnP
vedle	vedle	k7c2
předsedy	předseda	k1gMnSc2
strany	strana	k1gFnSc2
ještě	ještě	k6eAd1
Kateřina	Kateřina	k1gFnSc1
Jacques	Jacques	k1gMnSc1
<g/>
,	,	kIx,
Přemysl	Přemysl	k1gMnSc1
Rabas	Rabas	k1gMnSc1
a	a	k8xC
částečně	částečně	k6eAd1
též	též	k9
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opoziční	opoziční	k2eAgInSc1d1
křídlo	křídlo	k1gNnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
vedla	vést	k5eAaImAgFnS
bývalá	bývalý	k2eAgFnSc1d1
ministryně	ministryně	k1gFnSc1
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
Dana	Dana	k1gFnSc1
Kuchtová	Kuchtová	k1gFnSc1
podporovaná	podporovaný	k2eAgFnSc1d1
„	„	k?
<g/>
mladým	mladý	k2eAgMnSc7d1
rebelem	rebel	k1gMnSc7
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Matějem	Matěj	k1gMnSc7
Stropnickým	stropnický	k2eAgMnSc7d1
<g/>
,	,	kIx,
zahrnovala	zahrnovat	k5eAaImAgFnS
mj.	mj.	kA
poslankyně	poslankyně	k1gFnSc1
Olgu	Olga	k1gFnSc4
Zubovou	zubový	k2eAgFnSc4d1
(	(	kIx(
<g/>
bývalou	bývalý	k2eAgFnSc4d1
předsedkyni	předsedkyně	k1gFnSc4
republikové	republikový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Věru	Věra	k1gFnSc4
Jakubkovou	Jakubkový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Skupiny	skupina	k1gFnPc4
rozděloval	rozdělovat	k5eAaImAgMnS
i	i	k9
spor	spor	k1gInSc4
o	o	k7c4
rozdělení	rozdělení	k1gNnSc4
vnitřní	vnitřní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
ve	v	k7c6
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bursíkovo	Bursíkův	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
usilovalo	usilovat	k5eAaImAgNnS
o	o	k7c4
omezení	omezení	k1gNnSc4
pravomocí	pravomoc	k1gFnPc2
širšího	široký	k2eAgNnSc2d2
vedení	vedení	k1gNnSc2
SZ	SZ	kA
–	–	k?
republikové	republikový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
rada	rada	k1gFnSc1
dlouhodobě	dlouhodobě	k6eAd1
fungovala	fungovat	k5eAaImAgFnS
vedle	vedle	k7c2
předsednictva	předsednictvo	k1gNnSc2
jako	jako	k8xC,k8xS
druhé	druhý	k4xOgNnSc1
mocenské	mocenský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
dvojkolejnost	dvojkolejnost	k1gFnSc1
vedení	vedení	k1gNnSc2
strany	strana	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhli	navrhnout	k5eAaPmAgMnP
přetvořit	přetvořit	k5eAaPmF
radu	rada	k1gFnSc4
na	na	k7c4
obdobný	obdobný	k2eAgInSc4d1
typ	typ	k1gInSc4
širšího	široký	k2eAgNnSc2d2
vedení	vedení	k1gNnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
jakým	jaký	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
je	být	k5eAaImIp3nS
výkonná	výkonný	k2eAgFnSc1d1
rada	rada	k1gFnSc1
ODS	ODS	kA
nebo	nebo	k8xC
ústřední	ústřední	k2eAgInSc1d1
výkonný	výkonný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
a	a	k8xC
zrušit	zrušit	k5eAaPmF
přímou	přímý	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
části	část	k1gFnSc2
členů	člen	k1gMnPc2
sjezdem	sjezd	k1gInSc7
<g/>
,	,	kIx,
takže	takže	k8xS
měli	mít	k5eAaImAgMnP
zůstat	zůstat	k5eAaPmF
jen	jen	k9
zástupci	zástupce	k1gMnPc1
krajských	krajský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protistrana	protistrana	k1gFnSc1
naopak	naopak	k6eAd1
požadovala	požadovat	k5eAaImAgFnS
zachování	zachování	k1gNnSc3
stávajícího	stávající	k2eAgMnSc2d1
rozsah	rozsah	k1gInSc4
vnitrostranické	vnitrostranický	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
a	a	k8xC
případně	případně	k6eAd1
její	její	k3xOp3gNnSc4
posílení	posílení	k1gNnSc4
o	o	k7c4
kolektivní	kolektivní	k2eAgNnSc4d1
předsednictví	předsednictví	k1gNnSc4
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teplický	teplický	k2eAgInSc1d1
sjezd	sjezd	k1gInSc1
a	a	k8xC
vyhrocení	vyhrocení	k1gNnSc1
vnitrostranického	vnitrostranický	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
</s>
<s>
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
vyřešit	vyřešit	k5eAaPmF
konflikt	konflikt	k1gInSc4
ještě	ještě	k9
před	před	k7c7
podzimními	podzimní	k2eAgFnPc7d1
krajskými	krajský	k2eAgFnPc7d1
volbami	volba	k1gFnPc7
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2008	#num#	k4
prosadil	prosadit	k5eAaPmAgMnS
v	v	k7c6
předsednictvu	předsednictvo	k1gNnSc6
poměrem	poměr	k1gInSc7
hlasů	hlas	k1gInPc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
svolání	svolání	k1gNnSc6
sjezdu	sjezd	k1gInSc2
SZ	SZ	kA
na	na	k7c4
první	první	k4xOgInSc4
zářijový	zářijový	k2eAgInSc4d1
víkend	víkend	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Dana	Dana	k1gFnSc1
Kuchtová	Kuchtová	k1gFnSc1
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
že	že	k8xS
bude	být	k5eAaImBp3nS
kandidovat	kandidovat	k5eAaImF
na	na	k7c4
předsedkyni	předsedkyně	k1gFnSc4
proti	proti	k7c3
němu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Stranická	stranický	k2eAgFnSc1d1
opozice	opozice	k1gFnSc1
poté	poté	k6eAd1
stoupence	stoupenec	k1gMnSc2
předsedy	předseda	k1gMnSc2
Bursíka	Bursík	k1gMnSc2
obvinila	obvinit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
před	před	k7c7
svoláním	svolání	k1gNnSc7
sjezdu	sjezd	k1gInSc2
navyšovali	navyšovat	k5eAaImAgMnP
počet	počet	k1gInSc4
členů	člen	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
počet	počet	k1gInSc1
delegátů	delegát	k1gMnPc2
sjezdu	sjezd	k1gInSc2
<g/>
)	)	kIx)
účelovým	účelový	k2eAgNnSc7d1
zakládáním	zakládání	k1gNnSc7
nových	nový	k2eAgFnPc2d1
základních	základní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Bursík	Bursík	k1gMnSc1
obvinění	obvinění	k1gNnSc4
popřel	popřít	k5eAaPmAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nové	nový	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
nesouvisejí	souviset	k5eNaImIp3nP
se	s	k7c7
sjezdem	sjezd	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
pokračováním	pokračování	k1gNnSc7
boje	boj	k1gInSc2
proti	proti	k7c3
stavbě	stavba	k1gFnSc3
dálnice	dálnice	k1gFnSc2
D	D	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
tempo	tempo	k1gNnSc1
růstu	růst	k1gInSc2
členské	členský	k2eAgFnSc2d1
základny	základna	k1gFnSc2
a	a	k8xC
počtu	počet	k1gInSc2
základních	základní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
v	v	k7c6
předsjezdovém	předsjezdový	k2eAgNnSc6d1
období	období	k1gNnSc6
se	se	k3xPyFc4
neliší	lišit	k5eNaImIp3nS
od	od	k7c2
dřívějška	dřívějšek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
SZ	SZ	kA
konaném	konaný	k2eAgInSc6d1
v	v	k7c6
Teplicích	Teplice	k1gFnPc6
ve	v	k7c6
dnech	den	k1gInPc6
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2008	#num#	k4
byl	být	k5eAaImAgInS
předsedou	předseda	k1gMnSc7
SZ	SZ	kA
znovu	znovu	k6eAd1
zvolen	zvolen	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
<g/>
,	,	kIx,
místopředsedy	místopředseda	k1gMnPc4
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
Jacques	Jacques	k1gMnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
Ander	Ander	k1gMnSc1
<g/>
,	,	kIx,
dalšími	další	k2eAgInPc7d1
členy	člen	k1gInPc7
PSZ	PSZ	kA
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
také	také	k9
„	„	k?
<g/>
Bursíkovi	Bursíkův	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Nepodařilo	podařit	k5eNaPmAgNnS
se	se	k3xPyFc4
prosadit	prosadit	k5eAaPmF
projednání	projednání	k1gNnSc4
změn	změna	k1gFnPc2
stanov	stanova	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
závěru	závěr	k1gInSc6
jednání	jednání	k1gNnSc2
byli	být	k5eAaImAgMnP
z	z	k7c2
republikové	republikový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
odvoláni	odvolán	k2eAgMnPc1d1
všichni	všechen	k3xTgMnPc1
členové	člen	k1gMnPc1
volení	volený	k2eAgMnPc1d1
sjezdem	sjezd	k1gInSc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yQgMnPc7,k3yRgMnPc7,k3yIgMnPc7
měla	mít	k5eAaImAgFnS
většinu	většina	k1gFnSc4
opozice	opozice	k1gFnSc2
a	a	k8xC
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
stoupencům	stoupenec	k1gMnPc3
předsedy	předseda	k1gMnSc2
Bursíka	Bursík	k1gMnSc2
podařilo	podařit	k5eAaPmAgNnS
ovládnout	ovládnout	k5eAaPmF
i	i	k9
tento	tento	k3xDgInSc4
orgán	orgán	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
teplickém	teplický	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
zformovala	zformovat	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
kolem	kolem	k7c2
Dany	Dana	k1gFnSc2
Kuchtové	Kuchtová	k1gFnSc2
a	a	k8xC
Martina	Martin	k1gMnSc2
Čáslavky	Čáslavka	k1gMnSc2
vnitrostranickou	vnitrostranický	k2eAgFnSc4d1
iniciativu	iniciativa	k1gFnSc4
Výzva	výzva	k1gFnSc1
předsednictvu	předsednictvo	k1gNnSc6
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
kritizovala	kritizovat	k5eAaImAgFnS
vedení	vedení	k1gNnSc2
strany	strana	k1gFnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Martinem	Martin	k1gMnSc7
Bursíkem	Bursík	k1gMnSc7
za	za	k7c4
odklon	odklon	k1gInSc4
reálné	reálný	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
od	od	k7c2
politického	politický	k2eAgInSc2d1
a	a	k8xC
volebního	volební	k2eAgInSc2d1
programu	program	k1gInSc2
strany	strana	k1gFnSc2
a	a	k8xC
potlačování	potlačování	k1gNnSc2
vnitrostranické	vnitrostranický	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedení	vedení	k1gNnSc1
kritiku	kritika	k1gFnSc4
nadále	nadále	k6eAd1
odmítalo	odmítat	k5eAaImAgNnS
a	a	k8xC
tak	tak	k6eAd1
31	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
vnitrostranická	vnitrostranický	k2eAgFnSc1d1
frakce	frakce	k1gFnSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
Demokratická	demokratický	k2eAgFnSc1d1
výzva	výzva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
čela	čelo	k1gNnSc2
byli	být	k5eAaImAgMnP
zvoleni	zvolen	k2eAgMnPc1d1
Dana	Dana	k1gFnSc1
Kuchtová	Kuchtová	k1gFnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
Čáslavka	Čáslavka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgFnSc1d1
revizní	revizní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
strany	strana	k1gFnSc2
o	o	k7c4
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
později	pozdě	k6eAd2
obdržela	obdržet	k5eAaPmAgFnS
podnět	podnět	k1gInSc4
Jihomoravské	jihomoravský	k2eAgFnSc2d1
krajské	krajský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
obviňující	obviňující	k2eAgFnPc1d1
členy	člen	k1gMnPc7
frakce	frakce	k1gFnSc2
z	z	k7c2
porušování	porušování	k1gNnSc2
stanov	stanova	k1gFnPc2
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
Republikové	republikový	k2eAgFnSc2d1
radě	rada	k1gFnSc3
vyloučit	vyloučit	k5eAaPmF
Danu	Dana	k1gFnSc4
Kuchtovou	Kuchtová	k1gFnSc4
<g/>
,	,	kIx,
Martina	Martin	k1gMnSc4
Čáslavku	Čáslavka	k1gMnSc4
<g/>
,	,	kIx,
Olgu	Olga	k1gFnSc4
Zubovou	zubový	k2eAgFnSc4d1
a	a	k8xC
Věru	Věra	k1gFnSc4
Jakubkovou	Jakubkový	k2eAgFnSc4d1
ze	z	k7c2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
Republiková	republikový	k2eAgFnSc1d1
rada	rada	k1gFnSc1
8	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
schválila	schválit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
</s>
<s>
Eurovolby	Eurovolba	k1gFnPc1
2009	#num#	k4
a	a	k8xC
zvolení	zvolení	k1gNnSc4
Ondřeje	Ondřej	k1gMnSc2
Lišky	Liška	k1gMnSc2
předsedou	předseda	k1gMnSc7
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
předseda	předseda	k1gMnSc1
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
post	post	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
byla	být	k5eAaImAgFnS
prohra	prohra	k1gFnSc1
SZ	SZ	kA
v	v	k7c6
eurovolbách	eurovolba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řízení	řízení	k1gNnSc6
strany	strana	k1gFnSc2
převzal	převzít	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
Zelené	zelené	k1gNnSc4
vést	vést	k5eAaImF
do	do	k7c2
říjnových	říjnový	k2eAgFnPc2d1
předčasných	předčasný	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
však	však	k9
byly	být	k5eAaImAgFnP
nakonec	nakonec	k6eAd1
zrušeny	zrušit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
května	květen	k1gInSc2
2009	#num#	k4
nebyli	být	k5eNaImAgMnP
Zelení	Zelený	k1gMnPc1
přímo	přímo	k6eAd1
zastoupeni	zastoupit	k5eAaPmNgMnP
ve	v	k7c6
vládě	vláda	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
vládla	vládnout	k5eAaImAgFnS
tzv.	tzv.	kA
úřednická	úřednický	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Jana	Jan	k1gMnSc2
Fischera	Fischer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
SZ	SZ	kA
nominovala	nominovat	k5eAaBmAgFnS
ministra	ministr	k1gMnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Ladislava	Ladislav	k1gMnSc2
Mika	Mik	k1gMnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Jana	Jan	k1gMnSc2
Dusíka	Dusík	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
ministra	ministr	k1gMnSc2
pro	pro	k7c4
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
menšiny	menšina	k1gFnPc4
Michaela	Michael	k1gMnSc2
Kocába	Kocáb	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
března	březen	k1gInSc2
2010	#num#	k4
Zelení	zelenit	k5eAaImIp3nP
vládu	vláda	k1gFnSc4
nepodporovali	podporovat	k5eNaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgMnS
Liška	Liška	k1gMnSc1
na	na	k7c6
sjezdu	sjezd	k1gInSc6
konaném	konaný	k2eAgInSc6d1
v	v	k7c6
Brně	Brno	k1gNnSc6
zvolen	zvolit	k5eAaPmNgMnS
řádným	řádný	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
místopředsedou	místopředseda	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
František	František	k1gMnSc1
Pelc	Pelc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
utrpěla	utrpět	k5eAaPmAgFnS
strana	strana	k1gFnSc1
drtivou	drtivý	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
získala	získat	k5eAaPmAgFnS
jen	jen	k9
2,44	2,44	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2010	#num#	k4
dal	dát	k5eAaPmAgMnS
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
spolu	spolu	k6eAd1
s	s	k7c7
celým	celý	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
SZ	SZ	kA
k	k	k7c3
dispozici	dispozice	k1gFnSc3
své	svůj	k3xOyFgFnSc2
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komunální	komunální	k2eAgFnPc1d1
volby	volba	k1gFnPc1
2010	#num#	k4
</s>
<s>
Ve	v	k7c6
dnech	den	k1gInPc6
15	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
proběhly	proběhnout	k5eAaPmAgFnP
komunální	komunální	k2eAgFnPc1d1
volby	volba	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
SZ	SZ	kA
získala	získat	k5eAaPmAgFnS
celkem	celkem	k6eAd1
323	#num#	k4
zastupitelských	zastupitelský	k2eAgNnPc2d1
křesel	křeslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Kvůli	kvůli	k7c3
volebnímu	volební	k2eAgNnSc3d1
inženýrství	inženýrství	k1gNnSc3
ODS	ODS	kA
–	–	k?
rozdělení	rozdělení	k1gNnSc1
města	město	k1gNnSc2
na	na	k7c4
7	#num#	k4
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
gerrymandering	gerrymandering	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
–	–	k?
se	se	k3xPyFc4
koalice	koalice	k1gFnSc1
SZ	SZ	kA
a	a	k8xC
SNK-ED	SNK-ED	k1gFnSc1
nedostala	dostat	k5eNaPmAgFnS
do	do	k7c2
pražského	pražský	k2eAgNnSc2d1
zastupitelstva	zastupitelstvo	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
získala	získat	k5eAaPmAgFnS
5,9	5,9	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
překonala	překonat	k5eAaPmAgFnS
tak	tak	k6eAd1
pětiprocentní	pětiprocentní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zelení	k1gNnSc1
<g/>
,	,	kIx,
SNK-ED	SNK-ED	k1gFnPc1
a	a	k8xC
Věci	věc	k1gFnPc1
veřejné	veřejný	k2eAgFnSc2d1
podali	podat	k5eAaPmAgMnP
ústavnímu	ústavní	k2eAgInSc3d1
soudu	soud	k1gInSc2
společnou	společný	k2eAgFnSc4d1
žalobu	žaloba	k1gFnSc4
s	s	k7c7
požadavkem	požadavek	k1gInSc7
výsledek	výsledek	k1gInSc1
voleb	volba	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
anulovat	anulovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soud	soud	k1gInSc4
však	však	k9
tento	tento	k3xDgInSc1
požadavek	požadavek	k1gInSc1
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
poměrem	poměr	k1gInSc7
hlasů	hlas	k1gInPc2
7	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
odmítnul	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předvolební	předvolební	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
v	v	k7c6
Ústeckém	ústecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
tehdy	tehdy	k6eAd1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
využila	využít	k5eAaPmAgFnS
slogan	slogan	k1gInSc4
„	„	k?
<g/>
A	a	k8xC
co	co	k9
děti	dítě	k1gFnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
si	se	k3xPyFc3
kde	kde	k6eAd1
hrát	hrát	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
z	z	k7c2
písně	píseň	k1gFnSc2
„	„	k?
<g/>
Až	až	k8xS
<g/>
“	“	k?
hudební	hudební	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Katapult	katapulta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
strana	strana	k1gFnSc1
měla	mít	k5eAaImAgFnS
souhlas	souhlas	k1gInSc4
frontmana	frontman	k1gMnSc2
kapely	kapela	k1gFnSc2
Oldřicha	Oldřich	k1gMnSc2
Říhy	Říha	k1gMnSc2
<g/>
,	,	kIx,
následně	následně	k6eAd1
ji	on	k3xPp3gFnSc4
žaloval	žalovat	k5eAaImAgMnS
autor	autor	k1gMnSc1
písně	píseň	k1gFnSc2
Ladislav	Ladislav	k1gMnSc1
Vostárek	Vostárek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumentoval	argumentovat	k5eAaImAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jako	jako	k8xS,k8xC
advokát	advokát	k1gMnSc1
zastupoval	zastupovat	k5eAaImAgMnS
průmyslové	průmyslový	k2eAgMnPc4d1
klienty	klient	k1gMnPc4
<g/>
,	,	kIx,
tedy	tedy	k9
opačnou	opačný	k2eAgFnSc4d1
zájmovou	zájmový	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
nežli	nežli	k8xS
SZ	SZ	kA
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jej	on	k3xPp3gMnSc4
použití	použití	k1gNnSc1
sloganu	slogan	k1gInSc2
mělo	mít	k5eAaImAgNnS
poškodit	poškodit	k5eAaPmF
na	na	k7c6
pověsti	pověst	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soud	soud	k1gInSc4
mu	on	k3xPp3gMnSc3
přiznal	přiznat	k5eAaPmAgMnS
omluvu	omluva	k1gFnSc4
v	v	k7c6
celostátním	celostátní	k2eAgInSc6d1
deníku	deník	k1gInSc6
Blesk	blesk	k1gInSc1
a	a	k8xC
v	v	k7c6
dubnu	duben	k1gInSc6
2019	#num#	k4
také	také	k6eAd1
finanční	finanční	k2eAgNnSc4d1
odškodnění	odškodnění	k1gNnSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
350	#num#	k4
tisíc	tisíc	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
ve	v	k7c6
straně	strana	k1gFnSc6
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
</s>
<s>
Na	na	k7c6
žižkovském	žižkovský	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
konaném	konaný	k2eAgInSc6d1
12	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
byl	být	k5eAaImAgMnS
Liška	Liška	k1gMnSc1
zvolen	zvolit	k5eAaPmNgMnS
řádným	řádný	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
užšího	úzký	k2eAgNnSc2d2
vedení	vedení	k1gNnSc2
strany	strana	k1gFnSc2
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
mj.	mj.	kA
bývalá	bývalý	k2eAgFnSc1d1
pražská	pražský	k2eAgFnSc1d1
zastupitelka	zastupitelka	k1gFnSc1
Petra	Petra	k1gFnSc1
Kolínská	kolínská	k1gFnSc1
<g/>
,	,	kIx,
Přemysl	Přemysl	k1gMnSc1
Rabas	Rabas	k1gMnSc1
nebo	nebo	k8xC
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
Liškova	Liškův	k2eAgNnSc2d1
předsednictví	předsednictví	k1gNnSc2
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
zaznamenala	zaznamenat	k5eAaPmAgFnS
znatelný	znatelný	k2eAgInSc4d1
posun	posun	k1gInSc4
k	k	k7c3
levé	levá	k1gFnSc3
části	část	k1gFnSc2
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	Zelený	k1gMnPc1
vyzvali	vyzvat	k5eAaPmAgMnP
ke	k	k7c3
zprůhlednění	zprůhlednění	k1gNnSc3
financování	financování	k1gNnSc2
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
podpořili	podpořit	k5eAaPmAgMnP
odborovou	odborový	k2eAgFnSc4d1
stávku	stávka	k1gFnSc4
proti	proti	k7c3
reformám	reforma	k1gFnPc3
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
zesílili	zesílit	k5eAaPmAgMnP
protijadernou	protijaderný	k2eAgFnSc4d1
rétoriku	rétorika	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
aktivní	aktivní	k2eAgFnSc7d1
účastí	účast	k1gFnSc7
členů	člen	k1gInPc2
předsednictva	předsednictvo	k1gNnSc2
na	na	k7c6
blokádě	blokáda	k1gFnSc6
se	se	k3xPyFc4
zúčastnili	zúčastnit	k5eAaPmAgMnP
odporu	odpor	k1gInSc2
proti	proti	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
kůrovcovému	kůrovcový	k2eAgNnSc3d1
kácení	kácení	k1gNnSc3
v	v	k7c6
národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
Šumava	Šumava	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
kritizovali	kritizovat	k5eAaImAgMnP
z	z	k7c2
jejich	jejich	k3xOp3gInSc2
pohledu	pohled	k1gInSc2
asociální	asociální	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
DPH	DPH	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
představili	představit	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
pohled	pohled	k1gInSc4
na	na	k7c4
řešení	řešení	k1gNnSc4
sociální	sociální	k2eAgFnSc2d1
situace	situace	k1gFnSc2
na	na	k7c6
Šluknovsku	Šluknovsko	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
prosazovali	prosazovat	k5eAaImAgMnP
zásady	zásada	k1gFnPc4
moderního	moderní	k2eAgInSc2d1
urbanismu	urbanismus	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
nesouhlas	nesouhlas	k1gInSc4
s	s	k7c7
tvorbou	tvorba	k1gFnSc7
sídelní	sídelní	k2eAgFnSc2d1
kaše	kaše	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Byli	být	k5eAaImAgMnP
hlavní	hlavní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
boje	boj	k1gInSc2
za	za	k7c4
odchod	odchod	k1gInSc4
Ladislava	Ladislav	k1gMnSc2
Bátory	Bátora	k1gFnSc2
z	z	k7c2
vysoké	vysoký	k2eAgFnSc2d1
manažerské	manažerský	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Byli	být	k5eAaImAgMnP
součástí	součást	k1gFnSc7
říjnové	říjnový	k2eAgFnSc2d1
protivládní	protivládní	k2eAgFnSc2d1
demonstrace	demonstrace	k1gFnSc2
hnutí	hnutí	k1gNnSc2
ProAlt	ProAlta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Kritický	kritický	k2eAgInSc1d1
k	k	k7c3
vládní	vládní	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
byl	být	k5eAaImAgInS
i	i	k9
Liškův	Liškův	k2eAgInSc1d1
projev	projev	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
přednesl	přednést	k5eAaPmAgMnS
na	na	k7c6
sjezdu	sjezd	k1gInSc6
TOP	topit	k5eAaImRp2nS
09	#num#	k4
v	v	k7c6
říjnu	říjen	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2011	#num#	k4
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
regionální	regionální	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
SZ	SZ	kA
přišel	přijít	k5eAaPmAgMnS
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
o	o	k7c4
poslední	poslední	k2eAgFnSc4d1
stranickou	stranický	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
neobhájil	obhájit	k5eNaPmAgMnS
mandát	mandát	k1gInSc4
člena	člen	k1gMnSc2
republikové	republikový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konferenci	konference	k1gFnSc6
konané	konaný	k2eAgFnSc6d1
v	v	k7c6
březnu	březen	k1gInSc6
2012	#num#	k4
byl	být	k5eAaImAgInS
však	však	k9
do	do	k7c2
republikové	republikový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
znovu	znovu	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2012	#num#	k4
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
oznámil	oznámit	k5eAaPmAgMnS
zvažování	zvažování	k1gNnSc4
opětovné	opětovný	k2eAgFnSc2d1
kandidatury	kandidatura	k1gFnSc2
na	na	k7c4
předsedu	předseda	k1gMnSc4
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjádřil	vyjádřit	k5eAaPmAgMnS
nespokojenost	nespokojenost	k1gFnSc4
s	s	k7c7
přílišným	přílišný	k2eAgNnSc7d1
směřováním	směřování	k1gNnSc7
Strany	strana	k1gFnSc2
zelených	zelený	k2eAgInPc2d1
doleva	doleva	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
českotřebovském	českotřebovský	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
v	v	k7c6
listopadu	listopad	k1gInSc6
2012	#num#	k4
byl	být	k5eAaImAgMnS
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
znovu	znovu	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
předsedou	předseda	k1gMnSc7
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
přímém	přímý	k2eAgInSc6d1
duelu	duel	k1gInSc6
o	o	k7c4
tento	tento	k3xDgInSc4
post	post	k1gInSc4
porazil	porazit	k5eAaPmAgMnS
Martina	Martin	k1gMnSc4
Bursíka	Bursík	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
místopředsedkyní	místopředsedkyně	k1gFnPc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Pavla	Pavla	k1gFnSc1
Brady	brada	k1gFnSc2
<g/>
,	,	kIx,
dalšími	další	k2eAgInPc7d1
místopředsedy	místopředseda	k1gMnPc7
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
a	a	k8xC
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
řadová	řadový	k2eAgNnPc4d1
místa	místo	k1gNnPc4
v	v	k7c6
předsednictvu	předsednictvo	k1gNnSc6
delegáti	delegát	k1gMnPc1
sjezdu	sjezd	k1gInSc2
vybrali	vybrat	k5eAaPmAgMnP
Evu	Eva	k1gFnSc4
Tylovou	Tylová	k1gFnSc4
<g/>
,	,	kIx,
Tomáše	Tomáš	k1gMnSc4
Průšu	Průša	k1gMnSc4
a	a	k8xC
Danu	Dana	k1gFnSc4
Kuchtovou	Kuchtová	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
vyloučení	vyloučení	k1gNnSc1
ze	z	k7c2
strany	strana	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
létě	léto	k1gNnSc6
zrušeno	zrušit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
také	také	k9
zvolen	zvolen	k2eAgMnSc1d1
první	první	k4xOgMnSc1
vnitrostranický	vnitrostranický	k2eAgMnSc1d1
ombudsman	ombudsman	k1gMnSc1
<g/>
,	,	kIx,
jímž	jenž	k3xRgMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Milan	Milan	k1gMnSc1
Mátl	mást	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
strany	strana	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
ve	v	k7c6
dnech	den	k1gInPc6
25	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
až	až	k9
26	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
byl	být	k5eAaImAgInS
již	již	k6eAd1
po	po	k7c6
čtvrté	čtvrtá	k1gFnSc6
zvolen	zvolit	k5eAaPmNgMnS
předsedou	předseda	k1gMnSc7
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
získal	získat	k5eAaPmAgMnS
125	#num#	k4
ze	z	k7c2
198	#num#	k4
odevzdaných	odevzdaný	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
63	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
porazil	porazit	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
protikandidáta	protikandidát	k1gMnSc4
Petra	Petr	k1gMnSc4
Štěpánka	Štěpánek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Post	post	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
strany	strana	k1gFnSc2
obsadila	obsadit	k5eAaPmAgFnS
Jana	Jana	k1gFnSc1
Drápalová	Drápalová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jakožto	jakožto	k8xS
jediná	jediný	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
získala	získat	k5eAaPmAgFnS
150	#num#	k4
hlasů	hlas	k1gInPc2
ve	v	k7c6
veřejné	veřejný	k2eAgFnSc6d1
volbě	volba	k1gFnSc6
v	v	k7c6
plénu	plénum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
místopředsedy	místopředseda	k1gMnPc7
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
Pavla	Pavel	k1gMnSc4
Brady	Brada	k1gMnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc2
SZ	SZ	kA
<g/>
)	)	kIx)
a	a	k8xC
Tomáš	Tomáš	k1gMnSc1
Průša	Průša	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
SZ	SZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadovými	řadový	k2eAgInPc7d1
členy	člen	k1gInPc7
Předsednictva	předsednictvo	k1gNnSc2
strany	strana	k1gFnSc2
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Ander	Ander	k1gMnSc1
a	a	k8xC
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
byla	být	k5eAaImAgFnS
předsedkyní	předsedkyně	k1gFnSc7
zvolena	zvolen	k2eAgFnSc1d1
Jana	Jana	k1gFnSc1
Drápalová	Drápalová	k1gFnSc1
<g/>
,	,	kIx,
proti	proti	k7c3
níž	jenž	k3xRgFnSc3
kandidoval	kandidovat	k5eAaImAgMnS
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
a	a	k8xC
dnes	dnes	k6eAd1
již	již	k6eAd1
bývalý	bývalý	k2eAgMnSc1d1
člen	člen	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Schejbal	Schejbal	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
kandidátský	kandidátský	k2eAgInSc1d1
projev	projev	k1gInSc1
donutil	donutit	k5eAaPmAgInS
spoustu	spousta	k1gFnSc4
delegátů	delegát	k1gMnPc2
odejít	odejít	k5eAaPmF
ze	z	k7c2
sálu	sál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
České	český	k2eAgFnSc6d1
Třebové	Třebová	k1gFnSc6
programový	programový	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
v	v	k7c6
Karlíně	Karlín	k1gInSc6
v	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
byl	být	k5eAaImAgInS
předsedou	předseda	k1gMnSc7
zvolen	zvolen	k2eAgMnSc1d1
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
těsnou	těsný	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protikandidáty	protikandidát	k1gMnPc4
byli	být	k5eAaImAgMnP
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Šlechta	Šlechta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
členy	člen	k1gInPc7
předsednictva	předsednictvo	k1gNnSc2
byli	být	k5eAaImAgMnP
zvoleni	zvolen	k2eAgMnPc1d1
Irena	Irena	k1gFnSc1
Moudrá	moudrý	k2eAgFnSc1d1
Wunschová	Wunschová	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Berg	Berg	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Kutílek	Kutílek	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Matouš	Matouš	k1gMnSc1
Vencálek	Vencálek	k1gMnSc1
<g/>
,	,	kIx,
Monika	Monika	k1gFnSc1
Horáková	Horáková	k1gFnSc1
a	a	k8xC
Daniel	Daniel	k1gMnSc1
Pitek	pitka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2016	#num#	k4
na	na	k7c4
své	svůj	k3xOyFgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
předsednictvu	předsednictvo	k1gNnSc6
rezignoval	rezignovat	k5eAaBmAgMnS
Daniel	Daniel	k1gMnSc1
Pitek	pitka	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
lednu	leden	k1gInSc6
2017	#num#	k4
jej	on	k3xPp3gMnSc4
nahradila	nahradit	k5eAaPmAgFnS
Hana	Hana	k1gFnSc1
Veronika	Veronika	k1gFnSc1
Konvalinková	Konvalinková	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
neočekávaně	očekávaně	k6eNd1
nízkém	nízký	k2eAgInSc6d1
volebním	volební	k2eAgInSc6d1
výsledku	výsledek	k1gInSc6
ve	v	k7c6
sněmovních	sněmovní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
říjnu	říjen	k1gInSc6
2017	#num#	k4
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
funkci	funkce	k1gFnSc4
předsedy	předseda	k1gMnSc2
i	i	k9
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
předsednictvu	předsednictvo	k1gNnSc6
jej	on	k3xPp3gMnSc4
nahradil	nahradit	k5eAaPmAgMnS
Jakub	Jakub	k1gMnSc1
Kutílek	Kutílek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Zelení	Zelený	k1gMnPc1
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
</s>
<s>
Karavan	karavan	k1gInSc1
s	s	k7c7
připojenými	připojený	k2eAgInPc7d1
solárními	solární	k2eAgInPc7d1
panely	panel	k1gInPc7
při	při	k7c6
kampani	kampaň	k1gFnSc6
Zelených	Zelená	k1gFnPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karavany	karavana	k1gFnPc1
s	s	k7c7
posádkou	posádka	k1gFnSc7
Zelených	Zelená	k1gFnPc2
měly	mít	k5eAaImAgInP
trasu	trasa	k1gFnSc4
po	po	k7c6
celé	celý	k2eAgFnSc6d1
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
strany	strana	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
Třebové	Třebová	k1gFnSc6
ve	v	k7c6
dnech	den	k1gInPc6
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
byl	být	k5eAaImAgInS
novým	nový	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
strany	strana	k1gFnSc2
zvolen	zvolen	k2eAgMnSc1d1
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
novou	nova	k1gFnSc7
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyní	místopředsedkyně	k1gFnPc2
Magdalena	Magdalena	k1gFnSc1
Davis	Davis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Pozici	pozice	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
obsadila	obsadit	k5eAaPmAgFnS
Petra	Petr	k1gMnSc4
Jelínková	Jelínková	k1gFnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
místopředsedkyní	místopředsedkyně	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Karolína	Karolína	k1gFnSc1
Žákovská	žákovský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
členy	člen	k1gInPc7
předsednictva	předsednictvo	k1gNnSc2
strany	strana	k1gFnSc2
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
Jenda	Jenda	k1gMnSc1
Perla	perla	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Vosecký	Vosecký	k1gMnSc1
a	a	k8xC
Vít	Vít	k1gMnSc1
Masare	Masar	k1gMnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
říjnu	říjen	k1gInSc6
2018	#num#	k4
na	na	k7c6
funkci	funkce	k1gFnSc6
člena	člen	k1gMnSc2
předsednictva	předsednictvo	k1gNnSc2
rezignoval	rezignovat	k5eAaBmAgMnS
František	František	k1gMnSc1
Vosecký	Vosecký	k1gMnSc1
<g/>
,	,	kIx,
nahradila	nahradit	k5eAaPmAgFnS
jej	on	k3xPp3gMnSc4
Zdeňka	Zdeněk	k1gMnSc4
Perglová	Perglová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
2019	#num#	k4
na	na	k7c6
funkci	funkce	k1gFnSc6
místopředsedkyně	místopředsedkyně	k1gFnSc2
rezignovala	rezignovat	k5eAaBmAgFnS
Petra	Petra	k1gFnSc1
Jelínková	Jelínková	k1gFnSc1
<g/>
,	,	kIx,
nahradil	nahradit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
Michal	Michal	k1gMnSc1
Kudrnáč	kudrnáč	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Voleb	volba	k1gFnPc2
do	do	k7c2
evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
se	se	k3xPyFc4
Zelení	Zelený	k1gMnPc1
účastní	účastnit	k5eAaImIp3nP
na	na	k7c6
společné	společný	k2eAgFnSc6d1
kandidátce	kandidátka	k1gFnSc6
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
STAN	stan	k1gInSc1
a	a	k8xC
dalších	další	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
pod	pod	k7c7
heslem	heslo	k1gNnSc7
"	"	kIx"
<g/>
Evropa	Evropa	k1gFnSc1
jsme	být	k5eAaImIp1nP
my	my	k3xPp1nPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc4
krok	krok	k1gInSc4
kritizovali	kritizovat	k5eAaImAgMnP
někteří	některý	k3yIgMnPc1
členové	člen	k1gMnPc1
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
např.	např.	kA
Miroslav	Miroslav	k1gMnSc1
Kalousek	Kalousek	k1gMnSc1
nebo	nebo	k8xC
Dominik	Dominik	k1gMnSc1
Feri	Fer	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
Zelených	Zelených	k2eAgMnPc2d1
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
však	však	k9
předsednictva	předsednictvo	k1gNnPc4
obou	dva	k4xCgMnPc2
stran	strana	k1gFnPc2
společnou	společný	k2eAgFnSc4d1
kandidátku	kandidátka	k1gFnSc4
odsouhlasila	odsouhlasit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
dalším	další	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
ve	v	k7c6
dnech	den	k1gInPc6
25	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
strany	strana	k1gFnSc2
zvolena	zvolen	k2eAgFnSc1d1
spolupředsednická	spolupředsednický	k2eAgFnSc1d1
dvojice	dvojice	k1gFnSc1
(	(	kIx(
<g/>
muž	muž	k1gMnSc1
a	a	k8xC
žena	žena	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgInSc4
politický	politický	k2eAgInSc4d1
subjekt	subjekt	k1gInSc4
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
genderově	genderově	k6eAd1
vyvážené	vyvážený	k2eAgNnSc4d1
spolupředsednictví	spolupředsednictví	k1gNnPc4
zavedl	zavést	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
spolupředsednickou	spolupředsednický	k2eAgFnSc7d1
dvojicí	dvojice	k1gFnSc7
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
vsetínský	vsetínský	k2eAgMnSc1d1
zastupitel	zastupitel	k1gMnSc1
Michal	Michal	k1gMnSc1
Berg	Berg	k1gMnSc1
a	a	k8xC
starostka	starostka	k1gFnSc1
Mníšku	Mníšek	k1gInSc2
pod	pod	k7c4
Brdy	Brdy	k1gInPc4
Magdalena	Magdalena	k1gFnSc1
Davis	Davis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
si	se	k3xPyFc3
členové	člen	k1gMnPc1
též	též	k9
připomněli	připomnět	k5eAaPmAgMnP
30	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
vzniku	vznik	k1gInSc2
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Místopředsedy	místopředseda	k1gMnSc2
strany	strana	k1gFnSc2
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
Anna	Anna	k1gFnSc1
Gümplová	Gümplová	k1gFnSc1
a	a	k8xC
Petr	Petr	k1gMnSc1
Globočník	Globočník	k1gMnSc1
<g/>
,	,	kIx,
řadovými	řadový	k2eAgInPc7d1
členy	člen	k1gInPc7
předsednictva	předsednictvo	k1gNnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
stali	stát	k5eAaPmAgMnP
Zdeňka	Zdeněk	k1gMnSc4
Perglová	Perglová	k1gFnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Kučera	Kučera	k1gMnSc1
a	a	k8xC
Jiří	Jiří	k1gMnSc1
Kulich	Kulich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
nového	nový	k2eAgNnSc2d1
předsednictva	předsednictvo	k1gNnSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
tématem	téma	k1gNnSc7
sněmovních	sněmovní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
klimatická	klimatický	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
nejdůležitějším	důležitý	k2eAgInSc7d3
problémem	problém	k1gInSc7
dneška	dnešek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Jako	jako	k9
první	první	k4xOgFnSc1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
použila	použít	k5eAaPmAgFnS
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
hudební	hudební	k2eAgInSc1d1
videoklip	videoklip	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaImAgInS,k5eAaBmAgInS
„	„	k?
<g/>
Můj	můj	k3xOp1gInSc4
hlas	hlas	k1gInSc4
je	být	k5eAaImIp3nS
zelenej	zelenat	k5eAaImRp2nS
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
hlavního	hlavní	k2eAgNnSc2d1
rapera	rapero	k1gNnSc2
zahrál	zahrát	k5eAaPmAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Zelených	Zelená	k1gFnPc2
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
doby	doba	k1gFnSc2
Ondřeje	Ondřej	k1gMnSc2
Lišky	Liška	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
předvolebním	předvolební	k2eAgInSc6d1
boji	boj	k1gInSc6
jako	jako	k9
první	první	k4xOgMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
volebních	volební	k2eAgInPc6d1
spotech	spot	k1gInPc6
užívala	užívat	k5eAaImAgFnS
překladatele	překladatel	k1gMnSc4
do	do	k7c2
znakové	znakový	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Spolu	spolu	k6eAd1
s	s	k7c7
Českou	český	k2eAgFnSc7d1
pirátskou	pirátský	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
začali	začít	k5eAaPmAgMnP
v	v	k7c6
rámci	rámec	k1gInSc6
předvolebního	předvolební	k2eAgInSc2d1
boje	boj	k1gInSc2
čistit	čistit	k5eAaImF
tlakovými	tlakový	k2eAgFnPc7d1
pistolemi	pistol	k1gFnPc7
špinavé	špinavý	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zanechávali	zanechávat	k5eAaImAgMnP
tím	ten	k3xDgNnSc7
na	na	k7c6
špinavých	špinavý	k2eAgFnPc6d1
zdích	zeď	k1gFnPc6
své	svůj	k3xOyFgFnSc2
loga	logo	k1gNnPc1
a	a	k8xC
předvolební	předvolební	k2eAgInPc1d1
texty	text	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
zvolili	zvolit	k5eAaPmAgMnP
jako	jako	k9
první	první	k4xOgFnSc1
netradiční	tradiční	k2eNgFnSc1d1
kontaktní	kontaktní	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
slaňováním	slaňování	k1gNnSc7
ze	z	k7c2
střech	střecha	k1gFnPc2
panelových	panelový	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
předvolební	předvolební	k2eAgFnSc6d1
kampani	kampaň	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
do	do	k7c2
voleb	volba	k1gFnPc2
o	o	k7c4
mandát	mandát	k1gInSc4
v	v	k7c6
poslanecké	poslanecký	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
zvolila	zvolit	k5eAaPmAgFnS
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
kontaktní	kontaktní	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
skrze	skrze	k?
vození	vození	k1gNnPc2
lidí	člověk	k1gMnPc2
rikšou	rikša	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
možnost	možnost	k1gFnSc4
si	se	k3xPyFc3
promluvit	promluvit	k5eAaPmF
s	s	k7c7
politiky	politik	k1gMnPc7
ze	z	k7c2
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
mnoho	mnoho	k6eAd1
marketingových	marketingový	k2eAgMnPc2d1
expertů	expert	k1gMnPc2
pozitivně	pozitivně	k6eAd1
překvapila	překvapit	k5eAaPmAgFnS
závěrečná	závěrečný	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
Zelených	Zelený	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
užili	užít	k5eAaPmAgMnP
24	#num#	k4
<g/>
h	h	k?
službu	služba	k1gFnSc4
telefonních	telefonní	k2eAgMnPc2d1
operátorů	operátor	k1gMnPc2
pro	pro	k7c4
dotazy	dotaz	k1gInPc4
voličů	volič	k1gMnPc2
k	k	k7c3
programu	program	k1gInSc3
a	a	k8xC
voleb	volba	k1gFnPc2
samotných	samotný	k2eAgInPc2d1
(	(	kIx(
<g/>
telefonními	telefonní	k2eAgInPc7d1
operátory	operátor	k1gInPc7
byli	být	k5eAaImAgMnP
jak	jak	k6eAd1
kandidáti	kandidát	k1gMnPc1
za	za	k7c2
Zelené	Zelená	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
proškolení	proškolený	k2eAgMnPc1d1
dobrovolníci	dobrovolník	k1gMnPc1
a	a	k8xC
spolupracovníci	spolupracovník	k1gMnPc1
Zelených	Zelených	k2eAgMnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
kampani	kampaň	k1gFnSc6
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
využili	využít	k5eAaPmAgMnP
Zelení	zelenit	k5eAaImIp3nP
obytné	obytný	k2eAgInPc4d1
vozy	vůz	k1gInPc4
s	s	k7c7
připojenými	připojený	k2eAgInPc7d1
solárními	solární	k2eAgInPc7d1
panely	panel	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Solární	solární	k2eAgInSc1d1
systém	systém	k1gInSc1
poskytoval	poskytovat	k5eAaImAgInS
dostatek	dostatek	k1gInSc4
energie	energie	k1gFnSc2
pro	pro	k7c4
zázemí	zázemí	k1gNnSc4
posádky	posádka	k1gFnSc2
nebo	nebo	k8xC
např.	např.	kA
pro	pro	k7c4
napájení	napájení	k1gNnSc4
soustavy	soustava	k1gFnSc2
při	při	k7c6
projevech	projev	k1gInPc6
kandidátů	kandidát	k1gMnPc2
a	a	k8xC
hostů	host	k1gMnPc2
při	při	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
občany	občan	k1gMnPc7
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
Zelených	Zelená	k1gFnPc2
působí	působit	k5eAaImIp3nS
od	od	k7c2
února	únor	k1gInSc2
2015	#num#	k4
také	také	k6eAd1
levicová	levicový	k2eAgFnSc1d1
programová	programový	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
Zelená	zelenat	k5eAaImIp3nS
re	re	k9
<g/>
:	:	kIx,
<g/>
vize	vize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
zavádějí	zavádět	k5eAaImIp3nP
Zelení	Zelený	k1gMnPc1
na	na	k7c6
několika	několik	k4yIc6
úrovních	úroveň	k1gFnPc6
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
praxi	praxe	k1gFnSc6
spolupředsednictví	spolupředsednictví	k1gNnSc4
jako	jako	k8xS,k8xC
model	model	k1gInSc4
vedení	vedení	k1gNnSc2
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
není	být	k5eNaImIp3nS
moc	moc	k6eAd1
soustředěna	soustředit	k5eAaPmNgFnS
v	v	k7c6
rukou	ruka	k1gFnPc6
jednoho	jeden	k4xCgMnSc2
lídra	lídr	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
dva	dva	k4xCgMnPc4
předsedy	předseda	k1gMnPc4
<g/>
/	/	kIx~
<g/>
předsedkyně	předsedkyně	k1gFnSc1
se	s	k7c7
stejnými	stejný	k2eAgNnPc7d1
právy	právo	k1gNnPc7
a	a	k8xC
podílem	podíl	k1gInSc7
na	na	k7c6
moci	moc	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
vedení	vedení	k1gNnPc4
krajské	krajský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Zelených	Zelených	k2eAgFnSc2d1
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
Plzeňském	plzeňský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
nebo	nebo	k8xC
organizace	organizace	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Předsedové	předseda	k1gMnPc1
a	a	k8xC
předsedkyně	předsedkyně	k1gFnSc1
Strany	strana	k1gFnSc2
zelených	zelený	k2eAgMnPc2d1
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Osoba	osoba	k1gFnSc1
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Martin	Martin	k1gMnSc1
Ječmínek	Ječmínek	k1gMnSc1
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Aleš	Aleš	k1gMnSc1
Mucha	Mucha	k1gMnSc1
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Vlček	Vlček	k1gMnSc1
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Emil	Emil	k1gMnSc1
Zeman	Zeman	k1gMnSc1
</s>
<s>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
Čejka	Čejka	k1gMnSc1
</s>
<s>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Rokos	Rokos	k1gMnSc1
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Beránek	Beránek	k1gMnSc1
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jana	Jana	k1gFnSc1
Drápalová	Drápalová	k1gFnSc1
</s>
<s>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Michal	Michal	k1gMnSc1
Berg	Berg	k1gMnSc1
a	a	k8xC
Magdalena	Magdalena	k1gFnSc1
Davis	Davis	k1gFnSc1
(	(	kIx(
<g/>
spolupředsedové	spolupředseda	k1gMnPc1
<g/>
,	,	kIx,
abecedně	abecedně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
2020	#num#	k4
</s>
<s>
Sjezdy	sjezd	k1gInPc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pisárecký	pisárecký	k2eAgInSc1d1
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2003	#num#	k4
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
táborský	táborský	k2eAgInSc1d1
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2003	#num#	k4
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
olomoucký	olomoucký	k2eAgMnSc1d1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2004	#num#	k4
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
vršovický	vršovický	k2eAgMnSc1d1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2004	#num#	k4
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
pardubický	pardubický	k2eAgInSc1d1
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
holešovický	holešovický	k2eAgInSc1d1
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
teplický	teplický	k2eAgInSc1d1
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
až	až	k9
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
štýřický	štýřický	k2eAgInSc1d1
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
až	až	k9
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2009	#num#	k4
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
žižkovský	žižkovský	k2eAgInSc1d1
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
až	až	k9
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
druhý	druhý	k4xOgInSc1
pardubický	pardubický	k2eAgInSc1d1
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
českotřebovský	českotřebovský	k2eAgInSc1d1
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
až	až	k9
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
karlínský	karlínský	k2eAgInSc1d1
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
druhý	druhý	k4xOgInSc1
pisárecký	pisárecký	k2eAgInSc1d1
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
druhý	druhý	k4xOgInSc1
českotřebovský	českotřebovský	k2eAgInSc1d1
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
druhý	druhý	k4xOgInSc1
karlínský	karlínský	k2eAgInSc1d1
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
královéhradecký	královéhradecký	k2eAgMnSc1d1
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
třetí	třetí	k4xOgMnSc1
českotřebovský	českotřebovský	k2eAgMnSc1d1
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ústecký	ústecký	k2eAgInSc1d1
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
druhý	druhý	k4xOgInSc1
ústecký	ústecký	k2eAgInSc1d1
–	–	k?
25	#num#	k4
a	a	k8xC
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
</s>
<s>
online	onlinout	k5eAaPmIp3nS
-	-	kIx~
30	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
</s>
<s>
Volební	volební	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1
lidu	lid	k1gInSc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
±	±	k?
</s>
<s>
1990	#num#	k4
</s>
<s>
224	#num#	k4
432	#num#	k4
</s>
<s>
3,1	3,1	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
150	#num#	k4
</s>
<s>
▼	▼	k?
<g/>
2	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
1992	#num#	k4
</s>
<s>
v	v	k7c6
rámci	rámec	k1gInSc6
LSU	LSU	kA
</s>
<s>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
150	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
2	#num#	k4
</s>
<s>
Opozice	opozice	k1gFnSc1
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1
národů	národ	k1gInPc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
±	±	k?
</s>
<s>
1990	#num#	k4
</s>
<s>
248	#num#	k4
944	#num#	k4
</s>
<s>
3,4	3,4	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
150	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
1992	#num#	k4
</s>
<s>
v	v	k7c6
rámci	rámec	k1gInSc6
LSU	LSU	kA
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
150	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
1	#num#	k4
</s>
<s>
Opozice	opozice	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
±	±	k?
</s>
<s>
1990	#num#	k4
</s>
<s>
295	#num#	k4
844	#num#	k4
</s>
<s>
4,1	4,1	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
1992	#num#	k4
</s>
<s>
v	v	k7c6
rámci	rámec	k1gInSc6
LSU	LSU	kA
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
3	#num#	k4
</s>
<s>
Opozice	opozice	k1gFnSc1
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
Zelených	Zelený	k1gMnPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozložení	rozložení	k1gNnSc1
voličské	voličský	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
Zelených	Zelených	k2eAgFnSc2d1
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
2010	#num#	k4
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
±	±	k?
</s>
<s>
19961	#num#	k4
</s>
<s>
neúčastnila	účastnit	k5eNaImAgFnS
se	se	k3xPyFc4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
▼	▼	k?
<g/>
3	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
1998	#num#	k4
</s>
<s>
67	#num#	k4
143	#num#	k4
</s>
<s>
1,1	1,1	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
-	-	kIx~
</s>
<s>
2002	#num#	k4
</s>
<s>
112	#num#	k4
929	#num#	k4
</s>
<s>
2,4	2,4	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
-	-	kIx~
</s>
<s>
2006	#num#	k4
</s>
<s>
336	#num#	k4
487	#num#	k4
</s>
<s>
6,3	6,3	k4
</s>
<s>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
6	#num#	k4
</s>
<s>
Vláda	vláda	k1gFnSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
127	#num#	k4
831	#num#	k4
</s>
<s>
2,4	2,4	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
▼	▼	k?
<g/>
6	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
2013	#num#	k4
</s>
<s>
159	#num#	k4
025	#num#	k4
</s>
<s>
3,2	3,2	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
-	-	kIx~
</s>
<s>
2017	#num#	k4
</s>
<s>
74	#num#	k4
335	#num#	k4
</s>
<s>
1,5	1,5	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
-	-	kIx~
</s>
<s>
1	#num#	k4
Před	před	k7c7
volbami	volba	k1gFnPc7
1996	#num#	k4
tehdejší	tehdejší	k2eAgFnSc7d1
vedení	vedení	k1gNnSc4
strany	strana	k1gFnSc2
nezaplatilo	zaplatit	k5eNaPmAgNnS
volební	volební	k2eAgFnSc4d1
kauci	kauce	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
neúčastnila	účastnit	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
regionálního	regionální	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
byla	být	k5eAaImAgFnS
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2006	#num#	k4
nejúspěšnější	úspěšný	k2eAgFnSc2d3
v	v	k7c6
okrese	okres	k1gInSc6
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
11,60	11,60	k4
%	%	kIx~
<g/>
,	,	kIx,
v	v	k7c6
celém	celý	k2eAgInSc6d1
kraji	kraj	k1gInSc6
9,58	9,58	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
9,19	9,19	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
okrese	okres	k1gInSc6
Brno-město	Brno-města	k1gMnSc5
(	(	kIx(
<g/>
8,74	8,74	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
nepřesáhla	přesáhnout	k5eNaPmAgFnS
pětiprocentní	pětiprocentní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
nutnou	nutný	k2eAgFnSc4d1
ke	k	k7c3
vstupu	vstup	k1gInSc3
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
(	(	kIx(
<g/>
nejúspěšnější	úspěšný	k2eAgFnSc1d3
byla	být	k5eAaImAgFnS
v	v	k7c6
Praze	Praha	k1gFnSc6
se	s	k7c7
ziskem	zisk	k1gInSc7
4,78	4,78	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
předčasných	předčasný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
2013	#num#	k4
mírně	mírně	k6eAd1
posílila	posílit	k5eAaPmAgFnS
<g/>
,	,	kIx,
více	hodně	k6eAd2
než	než	k8xS
pět	pět	k4xCc4
procent	procento	k1gNnPc2
hlasů	hlas	k1gInPc2
získala	získat	k5eAaPmAgFnS
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
6,45	6,45	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
okresech	okres	k1gInPc6
Praha-západ	Praha-západ	k1gInSc1
(	(	kIx(
<g/>
5,18	5,18	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Brno-město	Brno-města	k1gMnSc5
(	(	kIx(
<g/>
5,29	5,29	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
2017	#num#	k4
se	se	k3xPyFc4
však	však	k9
propadla	propadnout	k5eAaPmAgFnS
na	na	k7c4
1,46	1,46	k4
%	%	kIx~
a	a	k8xC
poprvé	poprvé	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
tak	tak	k9
nedosáhla	dosáhnout	k5eNaPmAgFnS
na	na	k7c4
státní	státní	k2eAgInSc4d1
příspěvek	příspěvek	k1gInSc4
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2017	#num#	k4
nabídla	nabídnout	k5eAaPmAgFnS
společnou	společný	k2eAgFnSc4d1
kandidátku	kandidátka	k1gFnSc4
České	český	k2eAgFnSc3d1
pirátské	pirátský	k2eAgFnSc3d1
straně	strana	k1gFnSc3
pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
,	,	kIx,
tu	tu	k6eAd1
ale	ale	k8xC
Piráti	pirát	k1gMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senát	senát	k1gInSc1
</s>
<s>
Eliška	Eliška	k1gFnSc1
Wagnerová	Wagnerová	k1gFnSc1
<g/>
,	,	kIx,
senátorka	senátorka	k1gFnSc1
zvolená	zvolený	k2eAgFnSc1d1
za	za	k7c4
Stranu	strana	k1gFnSc4
zelených	zelená	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
První	první	k4xOgNnSc1
kolo	kolo	k1gNnSc1
</s>
<s>
Druhé	druhý	k4xOgNnSc1
kolo	kolo	k1gNnSc1
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
V	v	k7c6
Senátu	senát	k1gInSc6
celkem	celkem	k6eAd1
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
%	%	kIx~
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
%	%	kIx~
</s>
<s>
1996121	#num#	k4
1460,8	1460,8	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▬	▬	k?
<g/>
0	#num#	k4
</s>
<s>
19987	#num#	k4
8030,8	8030,8	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▬	▬	k?
<g/>
0	#num#	k4
</s>
<s>
20008500,1	20008500,1	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▬	▬	k?
<g/>
0	#num#	k4
</s>
<s>
20022	#num#	k4
6390,4	6390,4	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▬	▬	k?
<g/>
0	#num#	k4
</s>
<s>
20047	#num#	k4
1371,013	1371,013	k4
2962,8	2962,8	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
1	#num#	k4
</s>
<s>
200650	#num#	k4
5084,78	5084,78	k4
0	#num#	k4
<g/>
441,4	441,4	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▬	▬	k?
<g/>
0	#num#	k4
</s>
<s>
200721	#num#	k4
10110,1	10110,1	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▬	▬	k?
<g/>
0	#num#	k4
</s>
<s>
200841	#num#	k4
8364,0	8364,0	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▬	▬	k?
<g/>
0	#num#	k4
</s>
<s>
201010	#num#	k4
4730,9	4730,9	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▼	▼	k?
<g/>
1	#num#	k4
</s>
<s>
201218	#num#	k4
3912,110	3912,110	k4
7562,1	7562,1	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
1	#num#	k4
</s>
<s>
201432	#num#	k4
0	#num#	k4
<g/>
9215,84	9215,84	k4
66450,9	66450,9	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
1	#num#	k4
</s>
<s>
201437	#num#	k4
0	#num#	k4
<g/>
783,622	783,622	k4
2754,7	2754,7	k4
</s>
<s>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
2	#num#	k4
</s>
<s>
201618	#num#	k4
7982,112	7982,112	k4
5653,0	5653,0	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▬	▬	k?
<g/>
0	#num#	k4
</s>
<s>
20181	#num#	k4
8180,2	8180,2	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▼	▼	k?
<g/>
1	#num#	k4
</s>
<s>
202011	#num#	k4
3151,18	3151,18	k4
0	#num#	k4
<g/>
851,8	851,8	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
27	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
▼	▼	k?
<g/>
2	#num#	k4
</s>
<s>
1	#num#	k4
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
celý	celý	k2eAgInSc1d1
Senát	senát	k1gInSc1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
třetina	třetina	k1gFnSc1
ale	ale	k8xC
na	na	k7c4
plných	plný	k2eAgNnPc2d1
6	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
Doplňovací	doplňovací	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
obvodě	obvod	k1gInSc6
Chomutov	Chomutov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
Doplňovací	doplňovací	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
obvodě	obvod	k1gInSc6
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
za	za	k7c4
obvod	obvod	k1gInSc4
25	#num#	k4
Praha	Praha	k1gFnSc1
10	#num#	k4
nezávislý	závislý	k2eNgMnSc1d1
kandidát	kandidát	k1gMnSc1
Strany	strana	k1gFnSc2
zelených	zelený	k2eAgMnPc2d1
Jaromír	Jaromír	k1gMnSc1
Štětina	štětina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
však	však	k9
byl	být	k5eAaImAgInS
v	v	k7c6
dalších	další	k2eAgFnPc6d1
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2010	#num#	k4
znovu	znovu	k6eAd1
zvolen	zvolit	k5eAaPmNgInS
už	už	k9
za	za	k7c4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
doplňovacích	doplňovací	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
za	za	k7c4
obvod	obvod	k1gInSc4
č.	č.	k?
30	#num#	k4
Kladno	Kladno	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
podporovala	podporovat	k5eAaImAgFnS
kandidáta	kandidát	k1gMnSc4
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
Jiřího	Jiří	k1gMnSc2
Dienstbiera	Dienstbier	k1gMnSc2
ml.	ml.	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
mandát	mandát	k1gInSc1
skutečně	skutečně	k6eAd1
získal	získat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2012	#num#	k4
byla	být	k5eAaImAgFnS
za	za	k7c4
samostatně	samostatně	k6eAd1
kandidující	kandidující	k2eAgFnSc4d1
Stranu	strana	k1gFnSc4
zelených	zelená	k1gFnPc2
do	do	k7c2
Senátu	senát	k1gInSc2
ve	v	k7c6
volebním	volební	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
č.	č.	k?
59	#num#	k4
Brno-město	Brno-města	k1gMnSc5
zvolena	zvolit	k5eAaPmNgFnS
Eliška	Eliška	k1gFnSc1
Wagnerová	Wagnerová	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
koalici	koalice	k1gFnSc4
KDU-ČSL	KDU-ČSL	k1gMnPc2
<g/>
,	,	kIx,
Zelených	Zelený	k1gMnPc2
a	a	k8xC
Pirátů	pirát	k1gMnPc2
byl	být	k5eAaImAgInS
ve	v	k7c6
volebním	volební	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
č.	č.	k?
26	#num#	k4
Praha	Praha	k1gFnSc1
2	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
Libor	Libor	k1gMnSc1
Michálek	Michálek	k1gMnSc1
(	(	kIx(
<g/>
oba	dva	k4xCgMnPc1
nestraníci	nestraník	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
doplňovacích	doplňovací	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
ve	v	k7c6
volebním	volební	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
č.	č.	k?
22	#num#	k4
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgInP
v	v	k7c6
září	září	k1gNnSc6
2014	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
na	na	k7c4
uprázdněné	uprázdněný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
po	po	k7c4
Jaromíru	Jaromíra	k1gFnSc4
Štětinovi	Štětinův	k2eAgMnPc1d1
zvolena	zvolit	k5eAaPmNgFnS
za	za	k7c4
koalici	koalice	k1gFnSc4
Zelených	zelené	k1gNnPc2
a	a	k8xC
ČSSD	ČSSD	kA
Ivana	Ivana	k1gFnSc1
Cabrnochová	Cabrnochová	k1gFnSc1
(	(	kIx(
<g/>
členka	členka	k1gFnSc1
Zelených	Zelená	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
řádných	řádný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
do	do	k7c2
třetiny	třetina	k1gFnSc2
Senátu	senát	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
o	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
Václav	Václav	k1gMnSc1
Hampl	Hampl	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Láska	láska	k1gFnSc1
a	a	k8xC
Jitka	Jitka	k1gFnSc1
Seitlová	Seitlová	k1gFnSc1
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
tři	tři	k4xCgMnPc1
nestraníci	nestraník	k1gMnPc1
za	za	k7c4
koalici	koalice	k1gFnSc4
KDU-ČSL	KDU-ČSL	k1gFnSc2
a	a	k8xC
Zelené	Zelená	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2016	#num#	k4
kandidovaly	kandidovat	k5eAaImAgFnP
za	za	k7c4
Stranu	strana	k1gFnSc4
zelených	zelená	k1gFnPc2
nebo	nebo	k8xC
koalice	koalice	k1gFnSc2
s	s	k7c7
její	její	k3xOp3gFnSc7
účastí	účast	k1gFnSc7
osobnosti	osobnost	k1gFnSc2
ve	v	k7c6
14	#num#	k4
z	z	k7c2
27	#num#	k4
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvoleni	zvolen	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
Petr	Petr	k1gMnSc1
Orel	Orel	k1gMnSc1
v	v	k7c6
obvodu	obvod	k1gInSc6
č.	č.	k?
67	#num#	k4
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
a	a	k8xC
Ladislav	Ladislav	k1gMnSc1
Kos	Kos	k1gMnSc1
v	v	k7c6
obvodu	obvod	k1gInSc6
č.	č.	k?
19	#num#	k4
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volebním	volební	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
č.	č.	k?
22	#num#	k4
Praha	Praha	k1gFnSc1
10	#num#	k4
však	však	k9
Ivana	Ivana	k1gFnSc1
Cabrnochová	Cabrnochová	k1gFnSc1
neobhájila	obhájit	k5eNaPmAgFnS
mandát	mandát	k1gInSc4
získaný	získaný	k2eAgInSc4d1
přede	před	k7c7
dvěma	dva	k4xCgInPc7
roky	rok	k1gInPc7
v	v	k7c6
doplňovacích	doplňovací	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
,	,	kIx,
počet	počet	k1gInSc1
zástupců	zástupce	k1gMnPc2
Strany	strana	k1gFnSc2
zelených	zelený	k2eAgMnPc2d1
v	v	k7c6
horní	horní	k2eAgFnSc6d1
parlamentní	parlamentní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
se	se	k3xPyFc4
tak	tak	k6eAd1
oproti	oproti	k7c3
stavu	stav	k1gInSc3
před	před	k7c7
volbami	volba	k1gFnPc7
zvedl	zvednout	k5eAaPmAgMnS
o	o	k7c4
jeden	jeden	k4xCgInSc4
mandát	mandát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
vedl	vést	k5eAaImAgMnS
Zelené	Zelené	k2eAgMnSc1d1
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
coby	coby	k?
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
mottem	motto	k1gNnSc7
Zelených	zelené	k1gNnPc2
bylo	být	k5eAaImAgNnS
"	"	kIx"
<g/>
Česko	Česko	k1gNnSc1
dopředu	dopředu	k6eAd1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Body	bod	k1gInPc4
tehdejšího	tehdejší	k2eAgInSc2d1
programu	program	k1gInSc2
bylo	být	k5eAaImAgNnS
např.	např.	kA
postupné	postupný	k2eAgNnSc1d1
uskutečnění	uskutečnění	k1gNnSc1
vize	vize	k1gFnSc2
jednotné	jednotný	k2eAgFnSc2d1
a	a	k8xC
energeticky	energeticky	k6eAd1
soběstačné	soběstačný	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
v	v	k7c6
EP	EP	kA
mj.	mj.	kA
podpořit	podpořit	k5eAaPmF
evropské	evropský	k2eAgInPc4d1
programy	program	k1gInPc4
na	na	k7c4
zateplování	zateplování	k1gNnSc4
budov	budova	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnSc2d1
energetické	energetický	k2eAgFnSc2d1
úspory	úspora	k1gFnSc2
a	a	k8xC
v	v	k7c6
programu	program	k1gInSc6
pro	pro	k7c4
evropské	evropský	k2eAgFnPc4d1
volby	volba	k1gFnPc4
odmítla	odmítnout	k5eAaPmAgFnS
možnost	možnost	k1gFnSc1
dotací	dotace	k1gFnPc2
pro	pro	k7c4
nové	nový	k2eAgInPc4d1
jaderné	jaderný	k2eAgInPc4d1
reaktory	reaktor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
±	±	k?
</s>
<s>
2004	#num#	k4
</s>
<s>
73	#num#	k4
932	#num#	k4
</s>
<s>
3,2	3,2	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
48	#num#	k4
621	#num#	k4
</s>
<s>
2,1	2,1	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
2014	#num#	k4
</s>
<s>
57	#num#	k4
240	#num#	k4
</s>
<s>
3,8	3,8	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
2019	#num#	k4
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
STAN	stan	k1gInSc1
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
Zastupitelstva	zastupitelstvo	k1gNnPc1
krajů	kraj	k1gInPc2
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Hlasy	hlas	k1gInPc1
<g/>
1	#num#	k4
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
%	%	kIx~
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
±	±	k?
</s>
<s>
2000	#num#	k4
</s>
<s>
13	#num#	k4
577	#num#	k4
</s>
<s>
0,6	0,6	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
Jen	jen	k9
na	na	k7c6
koaličních	koaliční	k2eAgFnPc6d1
kandidátkách	kandidátka	k1gFnPc6
</s>
<s>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
2	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
92	#num#	k4
057	#num#	k4
</s>
<s>
3,2	3,2	k4
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
▼	▼	k?
<g/>
2	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
46	#num#	k4
401	#num#	k4
</s>
<s>
1,8	1,8	k4
</s>
<s>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
9	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
4	#num#	k4
417	#num#	k4
</s>
<s>
0,2	0,2	k4
</s>
<s>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
▼	▼	k?
<g/>
4	#num#	k4
</s>
<s>
20202	#num#	k4
</s>
<s>
4	#num#	k4
868	#num#	k4
</s>
<s>
0,2	0,2	k4
</s>
<s>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
▲	▲	k?
<g/>
3	#num#	k4
</s>
<s>
1	#num#	k4
Zahrnuje	zahrnovat	k5eAaImIp3nS
hlasy	hlas	k1gInPc4
jen	jen	k9
pro	pro	k7c4
samostatné	samostatný	k2eAgFnPc4d1
kandidátky	kandidátka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
Zahrnuje	zahrnovat	k5eAaImIp3nS
dodatečný	dodatečný	k2eAgInSc4d1
mandát	mandát	k1gInSc4
Martina	Martin	k1gMnSc2
Hanouska	Hanousek	k1gMnSc2
v	v	k7c6
Královéhradeckém	královéhradecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
získal	získat	k5eAaPmAgInS
z	z	k7c2
náhradnické	náhradnický	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
kandidovala	kandidovat	k5eAaImAgFnS
SZ	SZ	kA
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
postavila	postavit	k5eAaPmAgFnS
s	s	k7c7
Liberální	liberální	k2eAgFnSc7d1
reformní	reformní	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
společnou	společný	k2eAgFnSc4d1
kandidátku	kandidátka	k1gFnSc4
Zelená	zelenat	k5eAaImIp3nS
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
získala	získat	k5eAaPmAgFnS
5,08	5,08	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
tři	tři	k4xCgNnPc4
křesla	křeslo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
počtu	počet	k1gInSc2
preferenčních	preferenční	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
<g/>
:	:	kIx,
nestraník	nestraník	k1gMnSc1
Mojmír	Mojmír	k1gMnSc1
Vlašín	Vlašín	k1gInSc4
navržený	navržený	k2eAgInSc4d1
SZ	SZ	kA
z	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnSc2
kandidátky	kandidátka	k1gFnSc2
<g/>
,	,	kIx,
členka	členka	k1gFnSc1
SZ	SZ	kA
a	a	k8xC
starostka	starostka	k1gFnSc1
MČ	MČ	kA
Brno-Nový	Brno-Nový	k2eAgInSc4d1
Lískovec	Lískovec	k1gInSc4
Jana	Jana	k1gFnSc1
Drápalová	Drápalová	k1gFnSc1
z	z	k7c2
22	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
volební	volební	k2eAgMnSc1d1
lídr	lídr	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Löw	Löw	k1gMnSc1
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
navržený	navržený	k2eAgInSc4d1
LiRA	lira	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
krajských	krajský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
utrpěla	utrpět	k5eAaPmAgFnS
strana	strana	k1gFnSc1
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
navzdory	navzdory	k6eAd1
optimistickým	optimistický	k2eAgNnSc7d1
očekáváním	očekávání	k1gNnSc7
předsedy	předseda	k1gMnSc2
Bursíka	Bursík	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
předpokládal	předpokládat	k5eAaImAgInS
nárůst	nárůst	k1gInSc4
počtu	počet	k1gInSc2
zastupitelů	zastupitel	k1gMnPc2
o	o	k7c4
stovky	stovka	k1gFnPc4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
názor	názor	k1gInSc4
podporovala	podporovat	k5eAaImAgFnS
i	i	k9
většina	většina	k1gFnSc1
průzkumů	průzkum	k1gInPc2
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
přisuzovaly	přisuzovat	k5eAaImAgInP
Zeleným	Zelený	k1gMnPc3
zisk	zisk	k1gInSc4
od	od	k7c2
7	#num#	k4
do	do	k7c2
9	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
strana	strana	k1gFnSc1
zcela	zcela	k6eAd1
propadla	propadnout	k5eAaPmAgFnS
a	a	k8xC
nezískala	získat	k5eNaPmAgFnS
zastoupení	zastoupení	k1gNnPc4
v	v	k7c6
žádném	žádný	k3yNgInSc6
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
12	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2012	#num#	k4
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
postavila	postavit	k5eAaPmAgFnS
kandidátní	kandidátní	k2eAgFnPc4d1
listiny	listina	k1gFnPc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
krajích	kraj	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pěti	pět	k4xCc6
krajích	kraj	k1gInPc6
kandidovala	kandidovat	k5eAaImAgFnS
v	v	k7c6
koalicích	koalice	k1gFnPc6
<g/>
:	:	kIx,
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
jako	jako	k8xC,k8xS
KOALICE	koalice	k1gFnSc2
PRO	pro	k7c4
KARLOVARSKÝ	karlovarský	k2eAgInSc4d1
KRAJ	kraj	k1gInSc4
(	(	kIx(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc1
+	+	kIx~
SZ	SZ	kA
+	+	kIx~
O	o	k7c4
co	co	k9
jim	on	k3xPp3gFnPc3
jde	jít	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Ústeckém	ústecký	k2eAgMnSc6d1
jako	jako	k8xS,k8xC
Hnutí	hnutí	k1gNnSc2
PRO	pro	k7c4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc1
+	+	kIx~
SZ	SZ	kA
+	+	kIx~
HRM	hrma	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Libereckém	liberecký	k2eAgMnSc6d1
jako	jako	k8xS,k8xC
Změna	změna	k1gFnSc1
pro	pro	k7c4
Liberecký	liberecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
SZ	SZ	kA
+	+	kIx~
Změna	změna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Královéhradeckém	královéhradecký	k2eAgMnSc6d1
jako	jako	k8xS,k8xC
Změna	změna	k1gFnSc1
pro	pro	k7c4
Královéhradecký	královéhradecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
SZ	SZ	kA
+	+	kIx~
Změna	změna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Olomouckém	olomoucký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
jako	jako	k9
Koalice	koalice	k1gFnSc1
pro	pro	k7c4
Olomoucký	olomoucký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
společně	společně	k6eAd1
se	se	k3xPyFc4
starosty	starosta	k1gMnSc2
(	(	kIx(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc1
+	+	kIx~
SZ	SZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
zbývajících	zbývající	k2eAgInPc6d1
osmi	osm	k4xCc6
krajích	kraj	k1gInPc6
kandidovala	kandidovat	k5eAaImAgFnS
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
samostatně	samostatně	k6eAd1
pod	pod	k7c7
svým	svůj	k3xOyFgInSc7
názvem	název	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejlepšího	dobrý	k2eAgInSc2d3
výsledku	výsledek	k1gInSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
strana	strana	k1gFnSc1
v	v	k7c6
Libereckém	liberecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
koalice	koalice	k1gFnSc1
získala	získat	k5eAaPmAgFnS
16,85	16,85	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
celkem	celkem	k6eAd1
10	#num#	k4
křesel	křeslo	k1gNnPc2
v	v	k7c6
krajském	krajský	k2eAgNnSc6d1
zastupitelstvu	zastupitelstvo	k1gNnSc6
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
5	#num#	k4
členové	člen	k1gMnPc1
nominovaní	nominovaný	k2eAgMnPc1d1
SZ	SZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhého	druhý	k4xOgInSc2
nejlepšího	dobrý	k2eAgInSc2d3
výsledku	výsledek	k1gInSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
v	v	k7c6
Olomouckém	olomoucký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
koalici	koalice	k1gFnSc3
volilo	volit	k5eAaImAgNnS
11	#num#	k4
%	%	kIx~
voličů	volič	k1gMnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
zde	zde	k6eAd1
všech	všecek	k3xTgInPc2
8	#num#	k4
mandátů	mandát	k1gInPc2
připadlo	připadnout	k5eAaPmAgNnS
KDU-ČSL	KDU-ČSL	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
její	její	k3xOp3gFnSc1
silná	silný	k2eAgFnSc1d1
voličská	voličský	k2eAgFnSc1d1
základna	základna	k1gFnSc1
„	„	k?
<g/>
vykroužkovala	vykroužkovat	k5eAaPmAgFnS
<g/>
“	“	k?
křesťanskodemokratické	křesťanskodemokratický	k2eAgMnPc4d1
kandidáty	kandidát	k1gMnPc4
před	před	k7c4
kandidáty	kandidát	k1gMnPc4
SZ.	SZ.	k1gFnSc4
Třetí	třetí	k4xOgInSc1
nejlepší	dobrý	k2eAgInSc1d3
výsledek	výsledek	k1gInSc1
Zelení	zelenit	k5eAaImIp3nP
zaznamenali	zaznamenat	k5eAaPmAgMnP
v	v	k7c6
rámci	rámec	k1gInSc6
Hnutí	hnutí	k1gNnSc2
PRO	pro	k7c4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
kraj	kraj	k1gInSc1
na	na	k7c4
Labskoústecku	Labskoústecka	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gNnSc4
volilo	volit	k5eAaImAgNnS
8,15	8,15	k4
%	%	kIx~
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koalice	koalice	k1gFnSc1
zde	zde	k6eAd1
získala	získat	k5eAaPmAgFnS
6	#num#	k4
mandátů	mandát	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
4	#num#	k4
pro	pro	k7c4
kandidáty	kandidát	k1gMnPc4
nominované	nominovaný	k2eAgMnPc4d1
Stranou	stranou	k6eAd1
zelených	zelený	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
kandidátku	kandidátka	k1gFnSc4
vedl	vést	k5eAaImAgMnS
bývalý	bývalý	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
za	za	k7c4
Stranu	strana	k1gFnSc4
zelených	zelená	k1gFnPc2
Přemysl	Přemysl	k1gMnSc1
Rabas	Rabas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
těsně	těsně	k6eAd1
nedosáhli	dosáhnout	k5eNaPmAgMnP
Zelení	zelený	k2eAgMnPc1d1
na	na	k7c4
křesla	křeslo	k1gNnPc4
zastupitelů	zastupitel	k1gMnPc2
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jejich	jejich	k3xOp3gFnSc1
koalice	koalice	k1gFnSc1
získala	získat	k5eAaPmAgFnS
4,66	4,66	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
I	i	k9
v	v	k7c6
případě	případ	k1gInSc6
překonání	překonání	k1gNnSc1
5	#num#	k4
%	%	kIx~
klauzule	klauzule	k1gFnSc1
by	by	kYmCp3nS
však	však	k9
zřejmě	zřejmě	k6eAd1
kvůli	kvůli	k7c3
preferenčním	preferenční	k2eAgInPc3d1
hlasům	hlas	k1gInPc3
SZ	SZ	kA
žádný	žádný	k3yNgInSc4
mandát	mandát	k1gInSc4
nezískala	získat	k5eNaPmAgFnS
-	-	kIx~
její	její	k3xOp3gMnSc1
nejúspěšnější	úspěšný	k2eAgMnSc1d3
kandidát	kandidát	k1gMnSc1
Petr	Petr	k1gMnSc1
Němec	Němec	k1gMnSc1
by	by	kYmCp3nS
skončil	skončit	k5eAaPmAgMnS
až	až	k9
na	na	k7c4
6	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Podobně	podobně	k6eAd1
dopadla	dopadnout	k5eAaPmAgFnS
strana	strana	k1gFnSc1
v	v	k7c6
Královéhradeckém	královéhradecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
koaliční	koaliční	k2eAgNnSc1d1
uskupení	uskupení	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
4,09	4,09	k4
%	%	kIx~
platných	platný	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepšího	dobrý	k2eAgInSc2d3
výsledku	výsledek	k1gInSc2
jako	jako	k8xS,k8xC
samostatně	samostatně	k6eAd1
kandidující	kandidující	k2eAgFnSc1d1
strana	strana	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
SZ	SZ	kA
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získala	získat	k5eAaPmAgFnS
3,68	3,68	k4
%	%	kIx~
platných	platný	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
následována	následován	k2eAgFnSc1d1
kraji	kraj	k1gInSc6
Moravskoslezským	moravskoslezský	k2eAgInSc7d1
(	(	kIx(
<g/>
2,79	2,79	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Středočeským	středočeský	k2eAgInSc7d1
(	(	kIx(
<g/>
2,59	2,59	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Plzeňským	plzeňské	k1gNnSc7
(	(	kIx(
<g/>
2,09	2,09	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vysočinou	vysočina	k1gFnSc7
(	(	kIx(
<g/>
2,04	2,04	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jihočeským	jihočeský	k2eAgInSc7d1
(	(	kIx(
<g/>
1,67	1,67	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pardubickým	pardubický	k2eAgInSc7d1
(	(	kIx(
<g/>
1,66	1,66	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Zlínským	zlínský	k2eAgInSc7d1
(	(	kIx(
<g/>
1,57	1,57	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
kandidovala	kandidovat	k5eAaImAgFnS
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
většinou	většinou	k6eAd1
v	v	k7c6
koalicích	koalice	k1gFnPc6
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
a	a	k8xC
Jihočeském	jihočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Zlínském	zlínský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Královéhradeckém	královéhradecký	k2eAgNnSc6d1
<g/>
,	,	kIx,
Ústeckém	ústecký	k2eAgNnSc6d1
a	a	k8xC
Jihomoravském	jihomoravský	k2eAgNnSc6d1
šlo	jít	k5eAaImAgNnS
o	o	k7c6
dvojkoalice	dvojkoalika	k1gFnSc6
s	s	k7c7
Českou	český	k2eAgFnSc7d1
pirátskou	pirátský	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
Pardubickém	pardubický	k2eAgInSc6d1
kraji	kraj	k1gInSc6
kandidovala	kandidovat	k5eAaImAgFnS
SZ	SZ	kA
v	v	k7c4
trojkoalici	trojkoalice	k1gFnSc4
s	s	k7c7
Piráty	pirát	k1gMnPc7
a	a	k8xC
hnutím	hnutí	k1gNnSc7
Změna	změna	k1gFnSc1
(	(	kIx(
<g/>
Pro	pro	k7c4
otevřený	otevřený	k2eAgInSc4d1
kraj	kraj	k1gInSc4
-	-	kIx~
Piráti	pirát	k1gMnPc1
<g/>
,	,	kIx,
Zelení	zelený	k2eAgMnPc1d1
<g/>
,	,	kIx,
Změna	změna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Libereckém	liberecký	k2eAgInSc6d1
<g />
.	.	kIx.
</s>
<s hack="1">
kraji	kraj	k1gInPc7
v	v	k7c6
dvojkoalici	dvojkoalice	k1gFnSc6
s	s	k7c7
hnutím	hnutí	k1gNnSc7
Změna	změna	k1gFnSc1
(	(	kIx(
<g/>
Změna	změna	k1gFnSc1
pro	pro	k7c4
Liberecký	liberecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Olomouckém	olomoucký	k2eAgMnSc6d1
(	(	kIx(
<g/>
Koalice	koalice	k1gFnSc1
pro	pro	k7c4
Olomoucký	olomoucký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
společně	společně	k6eAd1
se	s	k7c7
starosty	starosta	k1gMnPc7
<g/>
)	)	kIx)
a	a	k8xC
Plzeňském	plzeňský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
v	v	k7c6
dvojkoalici	dvojkoalice	k1gFnSc6
s	s	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
(	(	kIx(
<g/>
Koalice	koalice	k1gFnSc1
pro	pro	k7c4
Plzeňský	plzeňský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
kraji	kraj	k1gInSc6
Vysočina	vysočina	k1gFnSc1
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
dvojkoalici	dvojkoalice	k1gFnSc4
s	s	k7c7
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
Žijeme	žít	k5eAaImIp1nP
Vysočinou	vysočina	k1gFnSc7
-	-	kIx~
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
Zelení	Zelený	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
usilovala	usilovat	k5eAaImAgFnS
o	o	k7c4
krajské	krajský	k2eAgInPc4d1
mandáty	mandát	k1gInPc4
v	v	k7c6
trojkoalici	trojkoalice	k1gFnSc6
s	s	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
a	a	k8xC
SNK	SNK	kA
ED	ED	kA
(	(	kIx(
<g/>
Spolu	spolu	k6eAd1
pro	pro	k7c4
kraj	kraj	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Moravskoslezském	moravskoslezský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
v	v	k7c6
dvojkoalici	dvojkoalice	k1gFnSc6
s	s	k7c7
SNK	SNK	kA
ED	ED	kA
(	(	kIx(
<g/>
Koalice	koalice	k1gFnSc1
pro	pro	k7c4
Region	region	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajské	krajský	k2eAgFnPc4d1
zastupitele	zastupitel	k1gMnSc2
měli	mít	k5eAaImAgMnP
Zelení	Zelený	k1gMnPc1
po	po	k7c6
volbách	volba	k1gFnPc6
2016	#num#	k4
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
Libereckém	liberecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
a	a	k8xC
Královéhradeckém	královéhradecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
kandidovali	kandidovat	k5eAaImAgMnP
členové	člen	k1gMnPc1
a	a	k8xC
členky	členka	k1gFnPc1
Zelených	Zelených	k2eAgFnPc1d1
v	v	k7c6
Moravskoslezském	moravskoslezský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
společně	společně	k6eAd1
s	s	k7c7
hnutím	hnutí	k1gNnSc7
Starostové	Starostová	k1gFnSc2
a	a	k8xC
Nezávislí	závislý	k2eNgMnPc1d1
a	a	k8xC
hnutím	hnutí	k1gNnPc3
Nezávislí	závislý	k2eNgMnPc1d1
<g/>
,	,	kIx,
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
v	v	k7c6
koalici	koalice	k1gFnSc6
s	s	k7c7
TOP09	TOP09	k1gFnSc7
a	a	k8xC
hnutím	hnutí	k1gNnSc7
Hlas	hlas	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
Libereckém	liberecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
vedli	vést	k5eAaImAgMnP
kandidátní	kandidátní	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
Pro	pro	k7c4
Krajinu	Krajina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
kandidovali	kandidovat	k5eAaImAgMnP
společně	společně	k6eAd1
s	s	k7c7
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
Hnutím	hnutí	k1gNnSc7
Idealisté	idealista	k1gMnPc1
<g/>
,	,	kIx,
Moravským	moravský	k2eAgNnSc7d1
zemským	zemský	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
a	a	k8xC
Liberálně	liberálně	k6eAd1
ekologickou	ekologický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
jako	jako	k8xC,k8xS
Spolu	spolu	k6eAd1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Královéhradeckém	královéhradecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
utvořili	utvořit	k5eAaPmAgMnP
koalici	koalice	k1gFnSc4
s	s	k7c7
ČSSD	ČSSD	kA
pod	pod	k7c7
názvem	název	k1gInSc7
SPOLU	spol	k1gInSc2
PRO	pro	k7c4
KRAJ	kraj	k1gInSc4
–	–	k?
Osobnosti	osobnost	k1gFnPc4
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
ČSSD	ČSSD	kA
a	a	k8xC
Zelení	Zelený	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
Plzeňském	plzeňský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
kandidovali	kandidovat	k5eAaImAgMnP
jako	jako	k9
součást	součást	k1gFnSc4
uskupení	uskupení	k1gNnSc2
STAROSTOVÉ	Starostová	k1gFnSc2
(	(	kIx(
<g/>
STAN	stan	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
JOSEFEM	Josef	k1gMnSc7
BERNARDEM	Bernard	k1gMnSc7
a	a	k8xC
podporou	podpora	k1gFnSc7
Zelených	Zelená	k1gFnPc2
<g/>
,	,	kIx,
PRO	pro	k7c4
Plzeň	Plzeň	k1gFnSc4
a	a	k8xC
Idealistů	idealista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Ústeckém	ústecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
byli	být	k5eAaImAgMnP
součástí	součást	k1gFnSc7
koalice	koalice	k1gFnSc1
Spojenci	spojenec	k1gMnSc3
pro	pro	k7c4
kraj	kraj	k1gInSc4
tvořené	tvořený	k2eAgInPc4d1
kromě	kromě	k7c2
Zelených	Zelená	k1gFnPc2
také	také	k9
TOP	topit	k5eAaImRp2nS
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
Jsme	být	k5eAaImIp1nP
PRO	pro	k7c4
Kraj	kraj	k1gInSc4
<g/>
,	,	kIx,
Liberálně	liberálně	k6eAd1
ekologickou	ekologický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
a	a	k8xC
SNK-ED	SNK-ED	k1gFnSc7
a	a	k8xC
v	v	k7c6
Olomouckém	olomoucký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
kandidovali	kandidovat	k5eAaImAgMnP
jako	jako	k9
Spojenci	spojenec	k1gMnPc1
–	–	k?
Koalice	koalice	k1gFnSc1
pro	pro	k7c4
Olomoucký	olomoucký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc5
<g/>
,	,	kIx,
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
<g/>
,	,	kIx,
ProOlomouc	ProOlomouc	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Zlínském	zlínský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
kandidovali	kandidovat	k5eAaImAgMnP
členové	člen	k1gMnPc1
Zelených	Zelená	k1gFnPc2
na	na	k7c6
kandidátce	kandidátka	k1gFnSc6
ČSSD	ČSSD	kA
a	a	k8xC
v	v	k7c6
Jihočeském	jihočeský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Karlovarském	karlovarský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Pardubickém	pardubický	k2eAgInSc6d1
kraji	kraj	k1gInSc6
a	a	k8xC
na	na	k7c6
Vysočině	vysočina	k1gFnSc6
kandidovali	kandidovat	k5eAaImAgMnP
Zelení	Zelený	k1gMnPc1
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
získala	získat	k5eAaPmAgFnS
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
ve	v	k7c6
volbách	volba	k1gFnPc6
7	#num#	k4
krajských	krajský	k2eAgMnPc2d1
zastupitelů	zastupitel	k1gMnPc2
<g/>
,	,	kIx,
o	o	k7c4
dva	dva	k4xCgInPc4
více	hodně	k6eAd2
než	než	k8xS
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Olomouckém	olomoucký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
tak	tak	k9
má	mít	k5eAaImIp3nS
nově	nově	k6eAd1
dva	dva	k4xCgMnPc4
zastupitele	zastupitel	k1gMnPc4
<g/>
,	,	kIx,
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Středočeském	středočeský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Plzeňském	plzeňské	k1gNnSc6
<g/>
,	,	kIx,
Ústeckém	ústecký	k2eAgInSc6d1
a	a	k8xC
Zlínském	zlínský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
po	po	k7c6
jednom	jeden	k4xCgMnSc6
zastupiteli	zastupitel	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koaliční	koaliční	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
v	v	k7c6
Královéhradeckém	královéhradecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
rovněž	rovněž	k9
uspěla	uspět	k5eAaPmAgNnP
<g/>
,	,	kIx,
avšak	avšak	k8xC
mandáty	mandát	k1gInPc4
získali	získat	k5eAaPmAgMnP
pouze	pouze	k6eAd1
kandidáti	kandidát	k1gMnPc1
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
úmrtí	úmrtí	k1gNnSc6
Pavla	Pavel	k1gMnSc2
Hečka	Heček	k1gMnSc2
(	(	kIx(
<g/>
ČSSD	ČSSD	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
však	však	k9
z	z	k7c2
pozice	pozice	k1gFnSc2
prvního	první	k4xOgNnSc2
náhradníka	náhradník	k1gMnSc2
stal	stát	k5eAaPmAgMnS
zastupitelem	zastupitel	k1gMnSc7
Královéhradeckého	královéhradecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
Martin	Martin	k1gMnSc1
Hanousek	Hanousek	k1gMnSc1
a	a	k8xC
Zelení	Zelený	k1gMnPc1
tak	tak	k6eAd1
získali	získat	k5eAaPmAgMnP
osmého	osmý	k4xOgMnSc4
krajského	krajský	k2eAgMnSc4d1
zastupitele	zastupitel	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komunální	komunální	k2eAgFnPc1d1
volby	volba	k1gFnPc1
</s>
<s>
2006	#num#	k4
–	–	k?
446	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
(	(	kIx(
<g/>
0,71	0,71	k4
%	%	kIx~
<g/>
,	,	kIx,
s	s	k7c7
odstupem	odstup	k1gInSc7
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5,3	5,3	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
přepočtu	přepočet	k1gInSc6
na	na	k7c4
voliče	volič	k1gMnPc4
cca	cca	kA
4,1	4,1	k4
%	%	kIx~
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
úspěch	úspěch	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Martin	Martin	k1gMnSc1
Ander	Ander	k1gMnSc1
stal	stát	k5eAaPmAgMnS
náměstkem	náměstek	k1gMnSc7
primátora	primátor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
–	–	k?
323	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Petra	Petra	k1gFnSc1
Kolínská	kolínská	k1gFnSc1
mezi	mezi	k7c7
roky	rok	k1gInPc7
2016	#num#	k4
až	až	k9
2018	#num#	k4
působila	působit	k5eAaImAgFnS
jako	jako	k9
náměstka	náměstek	k1gMnSc4
primátorky	primátorka	k1gFnSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
pro	pro	k7c4
oblast	oblast	k1gFnSc4
územního	územní	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
pražského	pražský	k2eAgInSc2d1
magistrátu	magistrát	k1gInSc2
i	i	k9
několika	několik	k4yIc2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
předvolební	předvolební	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
s	s	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
a	a	k8xC
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
<g/>
,	,	kIx,
s	s	k7c7
celkem	celkem	k6eAd1
slušným	slušný	k2eAgInSc7d1
ziskem	zisk	k1gInSc7
mandátů	mandát	k1gInPc2
pro	pro	k7c4
Zelené	zelené	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
–	–	k?
265	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
a	a	k8xC
zastupitelek	zastupitelka	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
Hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
Petra	Petra	k1gFnSc1
Kolínská	kolínská	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
Martin	Martin	k1gMnSc1
Ander	Ander	k1gMnSc1
<g/>
,	,	kIx,
Jasna	jasno	k1gNnPc1
Flamiková	Flamikový	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
Opava	Opava	k1gFnSc1
(	(	kIx(
<g/>
Pavla	Pavla	k1gFnSc1
Brady	brada	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Vsetín	Vsetín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
–	–	k?
224	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
a	a	k8xC
zastupitelek	zastupitelka	k1gFnPc2
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgMnSc2
starostové	starosta	k1gMnPc1
či	či	k8xC
primátoři	primátor	k1gMnPc1
za	za	k7c2
Zelené	Zelená	k1gFnSc2
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
v	v	k7c6
obcích	obec	k1gFnPc6
a	a	k8xC
městech	město	k1gNnPc6
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Boršice	Boršice	k1gFnSc1
–	–	k?
Roman	Roman	k1gMnSc1
Jílek	Jílek	k1gMnSc1
<g/>
,	,	kIx,
Brno-Jundrov	Brno-Jundrov	k1gInSc1
–	–	k?
Ivana	Ivana	k1gFnSc1
Fajnorová	Fajnorová	k1gFnSc1
<g/>
,	,	kIx,
Brno-Komín	Brno-Komín	k1gInSc1
–	–	k?
Milada	Milada	k1gFnSc1
Blatná	blatný	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Brno-Nový	Brno-Nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
–	–	k?
Jana	Jana	k1gFnSc1
Drápalová	Drápalová	k1gFnSc1
<g/>
,	,	kIx,
Horní	horní	k2eAgInSc1d1
Jiřetín	Jiřetín	k1gInSc1
–	–	k?
Vladimír	Vladimír	k1gMnSc1
Buřt	Buřt	k?
<g/>
,	,	kIx,
Mníšek	Mníšek	k1gInSc1
pod	pod	k7c4
Brdy	Brdy	k1gInPc4
–	–	k?
Magdalena	Magdalena	k1gFnSc1
Davis	Davis	k1gFnSc1
<g/>
,	,	kIx,
Mšeno	Mšeno	k6eAd1
–	–	k?
Martin	Martin	k1gMnSc1
Mach	Mach	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
4	#num#	k4
–	–	k?
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
<g/>
:	:	kIx,
Zuzana	Zuzana	k1gFnSc1
Vejvodová	Vejvodová	k1gFnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
p.	p.	k?
<g/>
p.	p.	k?
za	za	k7c2
Zelené	Zelená	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
loga	logo	k1gNnSc2
strany	strana	k1gFnSc2
</s>
<s>
Logo	logo	k1gNnSc1
strany	strana	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
2006	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
Původním	původní	k2eAgNnSc7d1
logem	logo	k1gNnSc7
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
z	z	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
byl	být	k5eAaImAgInS
čtyřlístek	čtyřlístek	k1gInSc1
vytvořený	vytvořený	k2eAgInSc1d1
ze	z	k7c2
čtyř	čtyři	k4xCgNnPc2
srdcí	srdce	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
tři	tři	k4xCgInPc1
byla	být	k5eAaImAgNnP
zelená	zelený	k2eAgFnSc1d1
a	a	k8xC
jedno	jeden	k4xCgNnSc1
rudé	rudý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tmavozelené	tmavozelený	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
(	(	kIx(
<g/>
nejvýrazněji	výrazně	k6eAd3
ve	v	k7c6
volební	volební	k2eAgFnSc6d1
kampani	kampaň	k1gFnSc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
na	na	k7c6
jaře	jaro	k1gNnSc6
2004	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
používat	používat	k5eAaImF
druhé	druhý	k4xOgFnSc6
<g/>
,	,	kIx,
neformální	formální	k2eNgNnSc4d1
logo	logo	k1gNnSc4
–	–	k?
slunečnice	slunečnice	k1gFnSc1
<g/>
,	,	kIx,
nejčastější	častý	k2eAgInSc1d3
námět	námět	k1gInSc1
log	log	kA
zelených	zelený	k2eAgFnPc2d1
stran	strana	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Logo	logo	k1gNnSc1
strany	strana	k1gFnSc2
používané	používaný	k2eAgFnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
autory	autor	k1gMnPc7
vizuálního	vizuální	k2eAgInSc2d1
stylu	styl	k1gInSc2
jsou	být	k5eAaImIp3nP
Babeta	Babeta	k1gFnSc1
Ondrová	Ondrová	k1gFnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Slovák	Slovák	k1gMnSc1
</s>
<s>
Autorem	autor	k1gMnSc7
loga	logo	k1gNnSc2
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
–	–	k?
celistvého	celistvý	k2eAgInSc2d1
zeleného	zelený	k2eAgInSc2d1
čtyřlístku	čtyřlístek	k1gInSc2
<g/>
,	,	kIx,
používáno	používán	k2eAgNnSc1d1
od	od	k7c2
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
,	,	kIx,
zeleného	zelený	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
ABCGreen	ABCGreen	k1gInSc1
(	(	kIx(
<g/>
klon	klon	k1gInSc1
fontu	font	k1gInSc2
JohnSans	JohnSansa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
souvisejícího	související	k2eAgInSc2d1
grafického	grafický	k2eAgInSc2d1
manuálu	manuál	k1gInSc2
strany	strana	k1gFnSc2
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
přední	přední	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
typograf	typograf	k1gMnSc1
František	František	k1gMnSc1
Štorm	Štorm	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
První	první	k4xOgFnSc1
střešovická	střešovický	k2eAgFnSc1d1
písmolijna	písmolijna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
ledna	leden	k1gInSc2
2017	#num#	k4
strana	strana	k1gFnSc1
užívá	užívat	k5eAaImIp3nS
nové	nový	k2eAgNnSc4d1
logo	logo	k1gNnSc4
ve	v	k7c6
tvaru	tvar	k1gInSc6
písmene	písmeno	k1gNnSc2
Z.	Z.	kA
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současní	současný	k2eAgMnPc1d1
významní	významný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
strany	strana	k1gFnSc2
</s>
<s>
Michal	Michal	k1gMnSc1
Berg	Berg	k1gMnSc1
<g/>
,	,	kIx,
spolupředseda	spolupředseda	k1gMnSc1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Davis	Davis	k1gFnSc1
<g/>
,	,	kIx,
spolupředsedkyně	spolupředsedkyně	k1gFnSc1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
místopředsedkyně	místopředsedkyně	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
zastupitelka	zastupitelka	k1gFnSc1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
starostka	starostka	k1gFnSc1
města	město	k1gNnSc2
Mníšek	Mníšek	k1gInSc1
pod	pod	k7c7
Brdy	Brdy	k1gInPc7
</s>
<s>
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Prahy	Praha	k1gFnSc2
4	#num#	k4
a	a	k8xC
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Mirovský	Mirovský	k1gMnSc1
<g/>
,	,	kIx,
kandidát	kandidát	k1gMnSc1
na	na	k7c4
primátora	primátor	k1gMnSc4
Prahy	Praha	k1gFnSc2
za	za	k7c4
Zelené	zelené	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
,	,	kIx,
radní	radní	k1gFnSc1
na	na	k7c6
Praze	Praha	k1gFnSc6
7	#num#	k4
</s>
<s>
Senátor	senátor	k1gMnSc1
Petr	Petr	k1gMnSc1
Orel	Orel	k1gMnSc1
(	(	kIx(
<g/>
Zelení	Zelený	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvolen	zvolen	k2eAgMnSc1d1
za	za	k7c4
Stranu	strana	k1gFnSc4
zelených	zelená	k1gFnPc2
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gFnPc2
s	s	k7c7
podporou	podpora	k1gFnSc7
dalších	další	k2eAgFnPc2d1
demokratických	demokratický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
v	v	k7c6
obvodu	obvod	k1gInSc6
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
strany	strana	k1gFnSc2
</s>
<s>
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
opakovaně	opakovaně	k6eAd1
vyhrává	vyhrávat	k5eAaImIp3nS
soutěž	soutěž	k1gFnSc4
občanského	občanský	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
Fórum	fórum	k1gNnSc1
50	#num#	k4
%	%	kIx~
Strana	strana	k1gFnSc1
otevřená	otevřený	k2eAgFnSc1d1
ženám	žena	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
získala	získat	k5eAaPmAgFnS
místostarostka	místostarostka	k1gFnSc1
Prahy	Praha	k1gFnSc2
10	#num#	k4
Ivana	Ivana	k1gFnSc1
Cabrnochová	Cabrnochová	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Nadějná	nadějný	k2eAgFnSc1d1
politička	politička	k1gFnSc1
roku	rok	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
také	také	k9
vyhlašuje	vyhlašovat	k5eAaImIp3nS
Fórum	fórum	k1gNnSc1
50	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1
program	program	k1gInSc1
Zelených	Zelených	k2eAgInSc1d1
pro	pro	k7c4
parlamentní	parlamentní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc7
autory	autor	k1gMnPc7
byli	být	k5eAaImAgMnP
Miroslav	Miroslav	k1gMnSc1
Zámečník	Zámečník	k1gMnSc1
a	a	k8xC
Tomáš	Tomáš	k1gMnSc1
Sedláček	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
oceněn	ocenit	k5eAaPmNgInS
některými	některý	k3yIgMnPc7
ekonomickými	ekonomický	k2eAgMnPc7d1
experty	expert	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
získala	získat	k5eAaPmAgFnS
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
spolu	spolu	k6eAd1
s	s	k7c7
Českou	český	k2eAgFnSc7d1
pirátskou	pirátský	k2eAgFnSc7d1
stranu	strana	k1gFnSc4
nejlepší	dobrý	k2eAgNnSc1d3
hodnocení	hodnocení	k1gNnSc1
(	(	kIx(
<g/>
známka	známka	k1gFnSc1
1,1	1,1	k4
<g/>
)	)	kIx)
v	v	k7c6
projektu	projekt	k1gInSc6
transparentní	transparentní	k2eAgFnSc2d1
volby	volba	k1gFnSc2
Transparency	Transparenca	k1gFnSc2
International	International	k1gFnSc2
–	–	k?
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
zaměřeného	zaměřený	k2eAgInSc2d1
na	na	k7c6
hodnocení	hodnocení	k1gNnSc6
transparentnosti	transparentnost	k1gFnSc2
politické	politický	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
před	před	k7c7
volbami	volba	k1gFnPc7
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnocení	hodnocení	k1gNnSc1
bylo	být	k5eAaImAgNnS
zaměřeno	zaměřit	k5eAaPmNgNnS
mj.	mj.	kA
na	na	k7c6
zveřejňování	zveřejňování	k1gNnSc6
celkových	celkový	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
na	na	k7c4
kampaň	kampaň	k1gFnSc4
<g/>
,	,	kIx,
transparentní	transparentní	k2eAgInPc1d1
běžné	běžný	k2eAgInPc1d1
účty	účet	k1gInPc1
vč.	vč.	k?
označení	označení	k1gNnSc2
a	a	k8xC
popisu	popis	k1gInSc2
výdajů	výdaj	k1gInPc2
<g/>
,	,	kIx,
zveřejnění	zveřejnění	k1gNnSc1
informací	informace	k1gFnPc2
o	o	k7c6
darech	dar	k1gInPc6
nebo	nebo	k8xC
informace	informace	k1gFnPc1
o	o	k7c6
volebním	volební	k2eAgInSc6d1
týmu	tým	k1gInSc6
a	a	k8xC
dobrovolnících	dobrovolník	k1gMnPc6
v	v	k7c6
kampani	kampaň	k1gFnSc6
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Témata	téma	k1gNnPc4
</s>
<s>
Předvolební	předvolební	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
v	v	k7c6
do	do	k7c2
voleb	volba	k1gFnPc2
v	v	k7c6
Poslanecké	poslanecký	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
v	v	k7c6
květnu	květen	k1gInSc6
2010	#num#	k4
ve	v	k7c6
formě	forma	k1gFnSc6
koncertu	koncert	k1gInSc2
–	–	k?
„	„	k?
<g/>
Koncert	koncert	k1gInSc1
pro	pro	k7c4
Zelené	zelené	k1gNnSc4
<g/>
“	“	k?
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Žižkově	Žižkov	k1gInSc6
</s>
<s>
Energetika	energetika	k1gFnSc1
</s>
<s>
Zelení	Zelený	k1gMnPc1
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
nutnost	nutnost	k1gFnSc4
přechodu	přechod	k1gInSc2
na	na	k7c4
obnovitelné	obnovitelný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
energie	energie	k1gFnSc2
–	–	k?
biomasu	biomasa	k1gFnSc4
<g/>
,	,	kIx,
větrnou	větrný	k2eAgFnSc4d1
a	a	k8xC
sluneční	sluneční	k2eAgFnSc4d1
energii	energie	k1gFnSc4
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
se	s	k7c7
snížením	snížení	k1gNnSc7
spotřeby	spotřeba	k1gFnSc2
energií	energie	k1gFnSc7
a	a	k8xC
jednoznačně	jednoznačně	k6eAd1
odmítají	odmítat	k5eAaImIp3nP
pokračování	pokračování	k1gNnSc4
v	v	k7c6
těžbě	těžba	k1gFnSc6
a	a	k8xC
spalování	spalování	k1gNnSc6
uhlí	uhlí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Strana	strana	k1gFnSc1
deklaruje	deklarovat	k5eAaBmIp3nS
svůj	svůj	k3xOyFgInSc4
odpor	odpor	k1gInSc4
k	k	k7c3
výstavbě	výstavba	k1gFnSc3
dalších	další	k2eAgFnPc2d1
jaderných	jaderný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
,	,	kIx,
kritizuje	kritizovat	k5eAaImIp3nS
nevyřešený	vyřešený	k2eNgInSc1d1
problém	problém	k1gInSc1
s	s	k7c7
ukládáním	ukládání	k1gNnSc7
vyhořelého	vyhořelý	k2eAgNnSc2d1
jaderného	jaderný	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
a	a	k8xC
hodlá	hodlat	k5eAaImIp3nS
jednak	jednak	k8xC
„	„	k?
<g/>
sledovat	sledovat	k5eAaImF
jako	jako	k8xS,k8xC
střednědobý	střednědobý	k2eAgInSc1d1
cíl	cíl	k1gInSc1
vystoupení	vystoupení	k1gNnSc2
ČR	ČR	kA
z	z	k7c2
jaderné	jaderný	k2eAgFnSc2d1
energetiky	energetika	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jednak	jednak	k8xC
„	„	k?
<g/>
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
nejrychleji	rychle	k6eAd3
zastavit	zastavit	k5eAaPmF
produkci	produkce	k1gFnSc4
dalšího	další	k2eAgNnSc2d1
vyhořelého	vyhořelý	k2eAgNnSc2d1
jaderného	jaderný	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
programového	programový	k2eAgNnSc2d1
prohlášení	prohlášení	k1gNnSc2
druhé	druhý	k4xOgFnSc2
vlády	vláda	k1gFnSc2
Mirka	Mirek	k1gMnSc2
Topolánka	Topolánek	k1gMnSc2
SZ	SZ	kA
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
prosadila	prosadit	k5eAaPmAgFnS
závazek	závazek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
vláda	vláda	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
nebude	být	k5eNaImBp3nS
připravovat	připravovat	k5eAaImF
stavbu	stavba	k1gFnSc4
dalších	další	k2eAgInPc2d1
jaderných	jaderný	k2eAgInPc2d1
bloků	blok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zároveň	zároveň	k6eAd1
podle	podle	k7c2
SZ	SZ	kA
„	„	k?
<g/>
přechodnou	přechodný	k2eAgFnSc4d1
roli	role	k1gFnSc4
k	k	k7c3
energetické	energetický	k2eAgFnSc3d1
budoucnosti	budoucnost	k1gFnSc3
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
bude	být	k5eAaImBp3nS
hrát	hrát	k5eAaImF
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
kritizováno	kritizován	k2eAgNnSc1d1
vzhledem	vzhledem	k7c3
k	k	k7c3
nutnosti	nutnost	k1gFnSc3
dovážení	dovážení	k1gNnSc2
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
z	z	k7c2
nespolehlivých	spolehlivý	k2eNgFnPc2d1
a	a	k8xC
politicky	politicky	k6eAd1
nestabilních	stabilní	k2eNgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
plynoucích	plynoucí	k2eAgNnPc2d1
rizik	riziko	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
jaderné	jaderný	k2eAgNnSc1d1
palivo	palivo	k1gNnSc1
do	do	k7c2
českých	český	k2eAgFnPc2d1
jaderných	jaderný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
se	se	k3xPyFc4
odebírá	odebírat	k5eAaImIp3nS
od	od	k7c2
ruské	ruský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
TVEL	TVEL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritici	kritik	k1gMnPc1
těchto	tento	k3xDgInPc2
plánů	plán	k1gInPc2
však	však	k9
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Česku	Česko	k1gNnSc6
jako	jako	k8xC,k8xS
vnitrozemském	vnitrozemský	k2eAgInSc6d1
státu	stát	k1gInSc6
jsou	být	k5eAaImIp3nP
možnosti	možnost	k1gFnPc1
využití	využití	k1gNnSc2
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
omezené	omezený	k2eAgNnSc1d1
<g/>
,	,	kIx,
nehledě	hledět	k5eNaImSgMnS
na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
další	další	k2eAgFnPc1d1
současné	současný	k2eAgFnPc1d1
nevýhody	nevýhoda	k1gFnPc1
(	(	kIx(
<g/>
nevyvážená	vyvážený	k2eNgFnSc1d1
výroba	výroba	k1gFnSc1
zejména	zejména	k9
u	u	k7c2
větrné	větrný	k2eAgFnSc2d1
a	a	k8xC
sluneční	sluneční	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
nemožnost	nemožnost	k1gFnSc4
pokrýt	pokrýt	k5eAaPmF
celou	celá	k1gFnSc4
nebo	nebo	k8xC
aspoň	aspoň	k9
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
spotřeby	spotřeba	k1gFnSc2
energie	energie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
(	(	kIx(
<g/>
a	a	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc7
spojovaná	spojovaný	k2eAgFnSc1d1
sdružení	sdružení	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
často	často	k6eAd1
kritizována	kritizovat	k5eAaImNgFnS
také	také	k9
kvůli	kvůli	k7c3
odporu	odpor	k1gInSc3
proti	proti	k7c3
jaderné	jaderný	k2eAgFnSc3d1
energii	energie	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Tyto	tento	k3xDgInPc4
programové	programový	k2eAgInPc4d1
body	bod	k1gInPc4
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
některými	některý	k3yIgMnPc7
lidmi	člověk	k1gMnPc7
za	za	k7c2
ideologické	ideologický	k2eAgFnSc2d1
a	a	k8xC
iracionální	iracionální	k2eAgFnSc2d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
jaderná	jaderný	k2eAgFnSc1d1
energetika	energetika	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
významný	významný	k2eAgInSc4d1
podíl	podíl	k1gInSc4
na	na	k7c6
výrobě	výroba	k1gFnSc6
i	i	k8xC
spotřebě	spotřeba	k1gFnSc6
elektřiny	elektřina	k1gFnSc2
(	(	kIx(
<g/>
31	#num#	k4
%	%	kIx~
a	a	k8xC
47	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
bude	být	k5eAaImBp3nS
v	v	k7c6
nejbližší	blízký	k2eAgFnSc6d3
době	doba	k1gFnSc6
nutné	nutný	k2eAgNnSc1d1
rozhodnout	rozhodnout	k5eAaPmF
o	o	k7c6
náhradě	náhrada	k1gFnSc6
především	především	k6eAd1
tepelných	tepelný	k2eAgFnPc2d1
(	(	kIx(
<g/>
uhelných	uhelný	k2eAgFnPc2d1
<g/>
)	)	kIx)
elektráren	elektrárna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
dosluhují	dosluhovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Postoj	postoj	k1gInSc4
ke	k	k7c3
KSČM	KSČM	kA
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
byla	být	k5eAaImAgFnS
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
kritizována	kritizovat	k5eAaImNgFnS
rovněž	rovněž	k6eAd1
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
její	její	k3xOp3gMnPc1
představitelé	představitel	k1gMnPc1
odmítali	odmítat	k5eAaImAgMnP
jasně	jasně	k6eAd1
vyslovit	vyslovit	k5eAaPmF
proti	proti	k7c3
komunistům	komunista	k1gMnPc3
<g/>
,	,	kIx,
odpor	odpor	k1gInSc1
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
s	s	k7c7
touto	tento	k3xDgFnSc7
stranou	strana	k1gFnSc7
začal	začít	k5eAaPmAgMnS
být	být	k5eAaImF
jasný	jasný	k2eAgMnSc1d1
až	až	k9
po	po	k7c6
vítězství	vítězství	k1gNnSc6
Bursíkovy	Bursíkův	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
na	na	k7c6
sjezdu	sjezd	k1gInSc6
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritikem	kritik	k1gMnSc7
KSČM	KSČM	kA
zůstává	zůstávat	k5eAaImIp3nS
i	i	k9
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Snížení	snížení	k1gNnSc1
věkové	věkový	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
pro	pro	k7c4
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
</s>
<s>
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
volebním	volební	k2eAgInSc6d1
programu	program	k1gInSc6
mj.	mj.	kA
prosazovala	prosazovat	k5eAaImAgFnS
snížení	snížení	k1gNnSc4
věkové	věkový	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
pro	pro	k7c4
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
na	na	k7c4
16	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
cíl	cíl	k1gInSc4
byl	být	k5eAaImAgMnS
často	často	k6eAd1
citován	citovat	k5eAaBmNgMnS
v	v	k7c6
médiích	médium	k1gNnPc6
ve	v	k7c6
volební	volební	k2eAgFnSc6d1
kampani	kampaň	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
nesetkal	setkat	k5eNaPmAgMnS
se	se	k3xPyFc4
po	po	k7c6
vstupu	vstup	k1gInSc6
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
do	do	k7c2
vlády	vláda	k1gFnSc2
s	s	k7c7
širší	široký	k2eAgFnSc7d2
podporou	podpora	k1gFnSc7
a	a	k8xC
téma	téma	k1gNnSc1
nadlouho	nadlouho	k6eAd1
utichlo	utichnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
tehdejší	tehdejší	k2eAgFnSc1d1
ministryně	ministryně	k1gFnSc1
Džamila	Džamila	k1gFnSc1
Stehlíková	Stehlíková	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
chce	chtít	k5eAaImIp3nS
otevřít	otevřít	k5eAaPmF
diskusi	diskuse	k1gFnSc4
o	o	k7c4
snížení	snížení	k1gNnSc4
věkové	věkový	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
na	na	k7c4
16	#num#	k4
let	léto	k1gNnPc2
pro	pro	k7c4
komunální	komunální	k2eAgFnPc4d1
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zelení	Zelený	k1gMnPc1
s	s	k7c7
Láskou	láska	k1gFnSc7
do	do	k7c2
parlamentu	parlament	k1gInSc2
<g/>
:	:	kIx,
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
zvolila	zvolit	k5eAaPmAgFnS
kandidáty	kandidát	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zelení	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-08	2013-09-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FRANTIŠEK	František	k1gMnSc1
<g/>
,	,	kIx,
Nejedlý	Nejedlý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelená	zelený	k2eAgFnSc1d1
politika	politika	k1gFnSc1
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zelení	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Program	program	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zelení	k1gNnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
:	:	kIx,
Minimální	minimální	k2eAgFnSc1d1
mzda	mzda	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
patnáct	patnáct	k4xCc4
tisíc	tisíc	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chceme	chtít	k5eAaImIp1nP
také	také	k9
zdanit	zdanit	k5eAaPmF
Airbnb	Airbnb	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WIRNITZER	WIRNITZER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politická	politický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
podle	podle	k7c2
čtenářů	čtenář	k1gMnPc2
<g/>
:	:	kIx,
Úsvit	úsvit	k1gInSc1
a	a	k8xC
ANO	ano	k9
neuchopitelní	uchopitelný	k2eNgMnPc1d1
<g/>
,	,	kIx,
o	o	k7c4
KSČM	KSČM	kA
máte	mít	k5eAaImIp2nP
jasno	jasno	k1gNnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-09-13	2016-09-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zelení	Zelený	k1gMnPc1
-	-	kIx~
Pro	pro	k7c4
členy	člen	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zeleni	zeleň	k1gFnSc3
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Usnesení	usnesení	k1gNnSc1
PSZ	PSZ	kA
k	k	k7c3
vizuální	vizuální	k2eAgFnSc3d1
identitě	identita	k1gFnSc3
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
ne	ne	k9
SZ	SZ	kA
<g/>
,	,	kIx,
nově	nově	k6eAd1
jen	jen	k9
Zelení	Zelený	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
změnila	změnit	k5eAaPmAgFnS
zkratku	zkratka	k1gFnSc4
<g/>
,	,	kIx,
napodobila	napodobit	k5eAaPmAgFnS
Piráty	pirát	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
přestává	přestávat	k5eAaImIp3nS
používat	používat	k5eAaImF
zkratku	zkratka	k1gFnSc4
SZ.	SZ.	k1gFnSc2
České	český	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Charta	charta	k1gFnSc1
globálních	globální	k2eAgFnPc2d1
zelených	zelená	k1gFnPc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Charta	charta	k1gFnSc1
Evropských	evropský	k2eAgNnPc2d1
zelených	zelené	k1gNnPc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Informace	informace	k1gFnPc1
o	o	k7c6
první	první	k4xOgFnSc6
aktivitě	aktivita	k1gFnSc6
vedoucí	vedoucí	k1gFnSc2
k	k	k7c3
založení	založení	k1gNnSc3
SZ	SZ	kA
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Jak	jak	k6eAd1
se	se	k3xPyFc4
čistila	čistit	k5eAaImAgFnS
strana	strana	k1gFnSc1
aneb	aneb	k?
konec	konec	k1gInSc1
Pseudozelených	Pseudozelený	k2eAgMnPc2d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
↑	↑	k?
Na	na	k7c4
rovnost	rovnost	k1gFnSc4
žen	žena	k1gFnPc2
a	a	k8xC
mužů	muž	k1gMnPc2
dbají	dbát	k5eAaImIp3nP
nejvíc	nejvíc	k6eAd1,k6eAd3
zelení	zelený	k2eAgMnPc1d1
<g/>
,	,	kIx,
nejmíň	málo	k6eAd3
zemanovci	zemanovec	k1gMnPc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
↑	↑	k?
Zelení	zelený	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
ženám	žena	k1gFnPc3
(	(	kIx(
<g/>
opět	opět	k6eAd1
<g/>
)	)	kIx)
nejotevřenější	otevřený	k2eAgMnSc1d3
–	–	k?
neplatný	platný	k2eNgInSc1d1
odkaz	odkaz	k1gInSc1
!	!	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Rovnou	rovnou	k6eAd1
daň	daň	k1gFnSc4
nepřijmeme	přijmout	k5eNaPmIp1nP
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
Martin	Martin	k1gMnSc1
Bursík	Bursík	k1gMnSc1
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
DNES	dnes	k6eAd1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
<g/>
↑	↑	k?
Stanovy	stanova	k1gFnSc2
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
<g/>
,	,	kIx,
článek	článek	k1gInSc4
2	#num#	k4
odstavec	odstavec	k1gInSc1
4	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
http://strana.zeleni.cz/59/rubrika/volebni-program-kvalita-zivota/%5B%5D	http://strana.zeleni.cz/59/rubrika/volebni-program-kvalita-zivota/%5B%5D	k4
<g/>
↑	↑	k?
Program	program	k1gInSc1
Kvalita	kvalita	k1gFnSc1
života	život	k1gInSc2
-	-	kIx~
Modernizace	modernizace	k1gFnSc1
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Program	program	k1gInSc1
Kvalita	kvalita	k1gFnSc1
života	život	k1gInSc2
-	-	kIx~
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
a	a	k8xC
ochrana	ochrana	k1gFnSc1
spotřebitele	spotřebitel	k1gMnSc2
<g/>
.	.	kIx.
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Program	program	k1gInSc1
Kvalita	kvalita	k1gFnSc1
života	život	k1gInSc2
-	-	kIx~
Otevřená	otevřený	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
a	a	k8xC
demokratická	demokratický	k2eAgFnSc1d1
účast	účast	k1gFnSc1
-	-	kIx~
posilme	posílit	k5eAaPmRp1nP
ochranu	ochrana	k1gFnSc4
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Program	program	k1gInSc1
Kvalita	kvalita	k1gFnSc1
života	život	k1gInSc2
-	-	kIx~
Podpora	podpora	k1gFnSc1
školství	školství	k1gNnSc2
a	a	k8xC
vědy	věda	k1gFnSc2
-	-	kIx~
kvalitní	kvalitní	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
<g/>
.	.	kIx.
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.ceskenoviny.cz/zpravy/kocab-chce-kvoty-pro-zeny-a-nabidl-rezignaci/446753&	http://www.ceskenoviny.cz/zpravy/kocab-chce-kvoty-pro-zeny-a-nabidl-rezignaci/446753&	k?
–	–	k?
neplatný	platný	k2eNgInSc4d1
odkaz	odkaz	k1gInSc4
!	!	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Program	program	k1gInSc1
Kvalita	kvalita	k1gFnSc1
života	život	k1gInSc2
-	-	kIx~
Sociální	sociální	k2eAgNnPc1d1
politika	politikum	k1gNnPc1
<g/>
:	:	kIx,
stejná	stejný	k2eAgNnPc1d1
práva	právo	k1gNnPc1
-	-	kIx~
stejné	stejný	k2eAgFnPc4d1
šance	šance	k1gFnPc4
<g/>
.	.	kIx.
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Program	program	k1gInSc1
Kvalita	kvalita	k1gFnSc1
života	život	k1gInSc2
-	-	kIx~
Život	život	k1gInSc1
v	v	k7c6
jednom	jeden	k4xCgInSc6
světě	svět	k1gInSc6
<g/>
:	:	kIx,
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
zelených	zelená	k1gFnPc2
<g/>
.	.	kIx.
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Israel	Israel	k1gInSc1
není	být	k5eNaImIp3nS
Československo	Československo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Přijetí	přijetí	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
do	do	k7c2
OSN	OSN	kA
je	být	k5eAaImIp3nS
nezbytným	nezbytný	k2eAgInSc7d1,k2eNgInSc7d1
krokem	krok	k1gInSc7
k	k	k7c3
míru	mír	k1gInSc3
a	a	k8xC
dodržování	dodržování	k1gNnSc4
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
na	na	k7c6
blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Přijetí	přijetí	k1gNnSc1
Palestiny	Palestina	k1gFnSc2
do	do	k7c2
OSN	OSN	kA
je	být	k5eAaImIp3nS
nezbytným	nezbytný	k2eAgInSc7d1,k2eNgInSc7d1
krokem	krok	k1gInSc7
k	k	k7c3
míru	mír	k1gInSc3
a	a	k8xC
dodržování	dodržování	k1gNnSc4
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
Východě	východ	k1gInSc6
<g/>
“	“	k?
|	|	kIx~
Židovské	židovský	k2eAgInPc1d1
listy	list	k1gInPc1
–	–	k?
zprávy	zpráva	k1gFnSc2
pro	pro	k7c4
židovskou	židovský	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
<g/>
.	.	kIx.
zidovskelisty	zidovskelista	k1gMnPc4
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Stanovy	stanova	k1gFnPc4
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
<g/>
↑	↑	k?
Zápis	zápis	k1gInSc1
z	z	k7c2
mimořádného	mimořádný	k2eAgInSc2d1
sjezdu	sjezd	k1gInSc2
SZ	SZ	kA
konaného	konaný	k2eAgInSc2d1
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2003	#num#	k4
v	v	k7c6
kongresovém	kongresový	k2eAgInSc6d1
sále	sál	k1gInSc6
Jihočeské	jihočeský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Táboře	Tábor	k1gInSc6
<g/>
,	,	kIx,
bod	bod	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
http://www.tyden.cz/tema/kostrbata-historie-zelenych_260.html	http://www.tyden.cz/tema/kostrbata-historie-zelenych_260.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.blisty.cz/art/15467.html	http://www.blisty.cz/art/15467.html	k1gMnSc1
<g/>
↑	↑	k?
Pavel	Pavel	k1gMnSc1
Pečínka	pečínka	k1gFnSc1
<g/>
:	:	kIx,
Boj	boj	k1gInSc1
o	o	k7c4
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
změnu	změna	k1gFnSc4
v	v	k7c6
zeleném	zelené	k1gNnSc6
<g/>
,	,	kIx,
Listy	list	k1gInPc1
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
<g/>
↑	↑	k?
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.listy.cz/archiv.php?cislo=042&	http://www.listy.cz/archiv.php?cislo=042&	k?
<g/>
↑	↑	k?
http://zpravy.idnes.cz/domaci.asp?c=A060517_111552_domaci_cen	http://zpravy.idnes.cz/domaci.asp?c=A060517_111552_domaci_tit	k5eAaImNgInS,k5eAaPmNgInS
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://strana.zeleni.cz/1389/clanek/%5B%5D	http://strana.zeleni.cz/1389/clanek/%5B%5D	k4
<g/>
↑	↑	k?
http://strana.zeleni.cz/1355/clanek/	http://strana.zeleni.cz/1355/clanek/	k4
<g/>
–	–	k?
<g/>
%	%	kIx~
<g/>
5	#num#	k4
<g/>
B	B	kA
<g/>
%	%	kIx~
<g/>
5	#num#	k4
<g/>
D	D	kA
neplatný	platný	k2eNgInSc4d1
odkaz	odkaz	k1gInSc4
!	!	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
http://strana.zeleni.cz/1459/clanek/%5B%5D	http://strana.zeleni.cz/1459/clanek/%5B%5D	k4
<g/>
↑	↑	k?
http://www.czso.cz/csu/redakce.nsf/itisk/metody_pro_prepocet_hlasu_na_mandaty	http://www.czso.cz/csu/redakce.nsf/itisk/metody_pro_prepocet_hlasu_na_mandata	k1gFnSc2
–	–	k?
neplatný	platný	k2eNgInSc1d1
odkaz	odkaz	k1gInSc1
!	!	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
http://blog.matejstropnicky.cz/strana-zelenych/v-mediich/rebel-na-startu-pravo%5B%5D	http://blog.matejstropnicky.cz/strana-zelenych/v-mediich/rebel-na-startu-pravo%5B%5D	k4
–	–	k?
neplatný	platný	k2eNgInSc4d1
odkaz	odkaz	k1gInSc4
!	!	kIx.
</s>
<s desamb="1">
http://www.jaromirstetina.cz/media/zari-2008/mlceti-zlato.html	http://www.jaromirstetina.cz/media/zari-2008/mlceti-zlato.html	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
http://strana.zeleni.cz/9852/clanek/podminkou-je-zahrabat-prikop/%5B%5D	http://strana.zeleni.cz/9852/clanek/podminkou-je-zahrabat-prikop/%5B%5D	k4
<g/>
↑	↑	k?
Bursíkovo	Bursíkův	k2eAgNnSc4d1
zdůvodnění	zdůvodnění	k1gNnSc4
svolat	svolat	k5eAaPmF
sjezd	sjezd	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Rozhodnutí	rozhodnutí	k1gNnSc1
Dany	Dana	k1gFnSc2
Kuchtové	Kuchtové	k2eAgMnSc1d1
kandidovat	kandidovat	k5eAaImF
proti	proti	k7c3
Bursíkovi	Bursík	k1gMnSc3
na	na	k7c6
sjezdu	sjezd	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
↑	↑	k?
Obvinění	obvinění	k1gNnSc4
protibursíkovské	protibursíkovský	k2eAgFnSc2d1
opozice	opozice	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
že	že	k8xS
protistrana	protistrana	k1gFnSc1
vytvářela	vytvářet	k5eAaImAgFnS
účelové	účelový	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
kvůli	kvůli	k7c3
sjezdu	sjezd	k1gInSc3
2008	#num#	k4
<g/>
↑	↑	k?
Bursík	Bursík	k1gMnSc1
odmítnul	odmítnout	k5eAaPmAgMnS
informaci	informace	k1gFnSc4
o	o	k7c6
účelovém	účelový	k2eAgNnSc6d1
zakládání	zakládání	k1gNnSc6
organizací	organizace	k1gFnSc7
kvůli	kvůli	k7c3
sjezdu	sjezd	k1gInSc3
2008	#num#	k4
<g/>
↑	↑	k?
Informace	informace	k1gFnPc1
o	o	k7c6
výsledku	výsledek	k1gInSc6
Teplického	teplický	k2eAgInSc2d1
sjezdu	sjezd	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
www.lidovky.cz	www.lidovky.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Informace	informace	k1gFnPc1
o	o	k7c4
ovládnutí	ovládnutí	k1gNnSc4
republikové	republikový	k2eAgFnSc2d1
rady	rada	k1gFnSc2
SZ	SZ	kA
Bursíkovým	Bursíkův	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
<g/>
↑	↑	k?
Bursík	Bursík	k1gMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
funkci	funkce	k1gFnSc4
předsedy	předseda	k1gMnSc2
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
<g/>
↑	↑	k?
Zelení	Zelený	k1gMnPc1
už	už	k6eAd1
nebudou	být	k5eNaImBp3nP
podporovat	podporovat	k5eAaImF
Fischerovu	Fischerův	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
↑	↑	k?
Kateřina	Kateřina	k1gFnSc1
Eliášová	Eliášová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	Zelený	k1gMnPc1
zvolili	zvolit	k5eAaPmAgMnP
předsedou	předseda	k1gMnSc7
Lišku	Liška	k1gMnSc4
<g/>
,	,	kIx,
mužem	muž	k1gMnSc7
č.	č.	k?
2	#num#	k4
je	být	k5eAaImIp3nS
Pelc	Pelc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-12-05	2009-12-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Liškův	Liškův	k2eAgInSc4d1
projev	projev	k1gInSc4
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
výsledků	výsledek	k1gInPc2
voleb	volba	k1gFnPc2
2010	#num#	k4
<g/>
↑	↑	k?
Experti	expert	k1gMnPc1
pro	pro	k7c4
Ústavní	ústavní	k2eAgInSc4d1
soud	soud	k1gInSc4
<g/>
:	:	kIx,
Volby	volba	k1gFnPc1
v	v	k7c6
Praze	Praha	k1gFnSc6
nebyly	být	k5eNaImAgInP
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
↑	↑	k?
Nové	Nové	k2eAgFnPc1d1
volby	volba	k1gFnPc1
v	v	k7c6
Praze	Praha	k1gFnSc6
nebudou	být	k5eNaImBp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústavní	ústavní	k2eAgInSc1d1
soud	soud	k1gInSc1
zamítl	zamítnout	k5eAaPmAgInS
stížnost	stížnost	k1gFnSc4
menších	malý	k2eAgFnPc2d2
stran	strana	k1gFnPc2
<g/>
↑	↑	k?
han	hana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zeleň	k1gFnPc2
musí	muset	k5eAaImIp3nS
zaplatit	zaplatit	k5eAaPmF
350	#num#	k4
tisíc	tisíc	k4xCgInPc2
textaři	textař	k1gMnSc6
Vostárkovi	Vostárek	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
použili	použít	k5eAaPmAgMnP
část	část	k1gFnSc4
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
textu	text	k1gInSc2
bez	bez	k7c2
dovolení	dovolení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2019-04-08	2019-04-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
‚	‚	k?
<g/>
A	a	k8xC
co	co	k9
děti	dítě	k1gFnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
si	se	k3xPyFc3
kde	kde	k6eAd1
hrát	hrát	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
’	’	k?
Někdejší	někdejší	k2eAgMnSc1d1
textař	textař	k1gMnSc1
Katapultu	katapult	k1gInSc2
vysoudil	vysoudit	k5eAaPmAgMnS
350	#num#	k4
tisíc	tisíc	k4xCgInPc2
na	na	k7c6
Straně	strana	k1gFnSc6
zelených	zelená	k1gFnPc2
|	|	kIx~
Domov	domov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2019-04-08	2019-04-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Noví	Nový	k1gMnPc1
zelení	zelení	k1gNnSc2
<g/>
:	:	kIx,
Liška	Liška	k1gMnSc1
<g/>
,	,	kIx,
Rabas	Rabas	k1gMnSc1
<g/>
,	,	kIx,
Kolínská	kolínská	k1gFnSc1
<g/>
,	,	kIx,
Fajnorová	Fajnorová	k1gFnSc1
a	a	k8xC
rebel	rebel	k1gMnSc1
<g/>
↑	↑	k?
Podpořte	podpořit	k5eAaPmRp2nP
zákon	zákon	k1gInSc1
proti	proti	k7c3
černému	černý	k2eAgNnSc3d1
financování	financování	k1gNnSc3
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Reforma	reforma	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
kompromisem	kompromis	k1gInSc7
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ne	ne	k9
diktátem	diktát	k1gInSc7
vlády	vláda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Výstup	výstup	k1gInSc1
z	z	k7c2
jádra	jádro	k1gNnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rychlý	rychlý	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Proti	proti	k7c3
kácení	kácení	k1gNnSc3
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
protestují	protestovat	k5eAaBmIp3nP
politici	politik	k1gMnPc1
<g/>
↑	↑	k?
Kalouskova	Kalouskův	k2eAgFnSc1d1
DPH	DPH	kA
chudým	chudý	k2eAgMnPc3d1
vezme	vzít	k5eAaPmIp3nS
a	a	k8xC
rozpočtu	rozpočet	k1gInSc2
nepřidá	přidat	k5eNaPmIp3nS
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Komplexní	komplexní	k2eAgInSc4d1
návrh	návrh	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
řešit	řešit	k5eAaImF
situaci	situace	k1gFnSc4
v	v	k7c6
českých	český	k2eAgNnPc6d1
ghettech	ghetto	k1gNnPc6
<g/>
.	.	kIx.
www.zeleni.cz	www.zeleni.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Desatero	desatero	k1gNnSc4
zelené	zelený	k2eAgFnSc2d1
Prahy	Praha	k1gFnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Požadavek	požadavek	k1gInSc1
nevládních	vládní	k2eNgFnPc2d1
organizací	organizace	k1gFnPc2
na	na	k7c6
odvolání	odvolání	k1gNnSc6
Bátory	Bátora	k1gFnSc2
podporují	podporovat	k5eAaImIp3nP
Zelení	zeleň	k1gFnSc7
a	a	k8xC
ČSSD	ČSSD	kA
<g/>
↑	↑	k?
Ondřej	Ondřej	k1gMnSc1
Liška	Liška	k1gMnSc1
<g/>
:	:	kIx,
Tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
by	by	kYmCp3nS
dostal	dostat	k5eAaPmAgMnS
pětku	pětka	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Dobeš	Dobeš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Bátora	Bátora	k1gFnSc1
je	být	k5eAaImIp3nS
Klausova	Klausův	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
<g/>
↑	↑	k?
Tisíce	tisíc	k4xCgInPc1
lidí	člověk	k1gMnPc2
v	v	k7c6
ulicích	ulice	k1gFnPc6
dnes	dnes	k6eAd1
volaly	volat	k5eAaImAgInP
po	po	k7c4
demisi	demise	k1gFnSc4
Nečasova	Nečasův	k2eAgInSc2d1
kabinetu	kabinet	k1gInSc2
<g/>
↑	↑	k?
Projev	projev	k1gInSc1
Ondřeje	Ondřej	k1gMnSc2
Lišky	Liška	k1gMnSc2
na	na	k7c6
sjezdu	sjezd	k1gInSc6
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Zelení	zeleň	k1gFnPc2
se	se	k3xPyFc4
sjeli	sjet	k5eAaPmAgMnP
na	na	k7c6
konferenci	konference	k1gFnSc6
<g/>
,	,	kIx,
Bursík	Bursík	k1gMnSc1
odešel	odejít	k5eAaPmAgMnS
poražen	poražen	k2eAgMnSc1d1
<g/>
↑	↑	k?
Chci	chtít	k5eAaImIp1nS
se	se	k3xPyFc4
vrátit	vrátit	k5eAaPmF
do	do	k7c2
čela	čelo	k1gNnSc2
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
<g/>
,	,	kIx,
ohlásil	ohlásit	k5eAaPmAgMnS
Bursík	Bursík	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-05-12	2012-05-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ve	v	k7c6
vedení	vedení	k1gNnSc6
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
jsou	být	k5eAaImIp3nP
i	i	k9
Bursík	Bursík	k1gMnSc1
<g/>
,	,	kIx,
Stropnický	stropnický	k2eAgMnSc1d1
či	či	k8xC
Kuchtová	Kuchtová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-11-25	2012-11-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nová	nový	k2eAgFnSc1d1
členka	členka	k1gFnSc1
předsednictva	předsednictvo	k1gNnSc2
Zelených	Zelená	k1gFnPc2
<g/>
:	:	kIx,
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kraji	kraj	k1gInSc6
bez	bez	k7c2
problému	problém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Referendum	referendum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-01-24	2017-01-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
idnes	idnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Zelené	Zelená	k1gFnSc2
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
Štěpánek	Štěpánek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-01-20	2018-01-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Předsedou	předseda	k1gMnSc7
Zelených	Zelená	k1gFnPc2
je	být	k5eAaImIp3nS
biolog	biolog	k1gMnSc1
a	a	k8xC
starosta	starosta	k1gMnSc1
Prahy	Praha	k1gFnSc2
4	#num#	k4
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-01-20	2018-01-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zelení	Zelený	k1gMnPc1
si	se	k3xPyFc3
zvolili	zvolit	k5eAaPmAgMnP
nové	nový	k2eAgNnSc4d1
předsednictvo	předsednictvo	k1gNnSc4
<g/>
,	,	kIx,
podpořili	podpořit	k5eAaPmAgMnP
Drahoše	Drahoš	k1gMnSc4
<g/>
,	,	kIx,
odmítli	odmítnout	k5eAaPmAgMnP
prohlubování	prohlubování	k1gNnSc2
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
Čínou	Čína	k1gFnSc7
a	a	k8xC
vyzvali	vyzvat	k5eAaPmAgMnP
k	k	k7c3
omezení	omezení	k1gNnSc3
jednorázových	jednorázový	k2eAgInPc2d1
plastových	plastový	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zelení	k1gNnSc1
<g/>
,	,	kIx,
2018-01-21	2018-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vedení	vedení	k1gNnSc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
Předsednictvo	předsednictvo	k1gNnSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zelení	k1gNnSc1
<g/>
,	,	kIx,
2018-10-24	2018-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zápis	zápis	k1gInSc1
PSZ	PSZ	kA
17.2	17.2	k4
<g/>
.	.	kIx.
2019	#num#	k4
Jihlava	Jihlava	k1gFnSc1
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zelení	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.zeleni.cz/zeleni-pujdou-do-evropskych-voleb-spolecne-se-stan-a-dalsimi-regionalnimi-hnutimi//	https://www.zeleni.cz/zeleni-pujdou-do-evropskych-voleb-spolecne-se-stan-a-dalsimi-regionalnimi-hnutimi//	k?
<g/>
↑	↑	k?
https://zpravy.aktualne.cz/domaci/sef-zelenych-stepanek-jde-do-voleb-s-pravicovou-koalici-nevi/r~bc1f8d86310d11e9813eac1f6b220ee8/	https://zpravy.aktualne.cz/domaci/sef-zelenych-stepanek-jde-do-voleb-s-pravicovou-koalici-nevi/r~bc1f8d86310d11e9813eac1f6b220ee8/	k4
<g/>
↑	↑	k?
http://www.zelenarevize.cz/recept-na-uspech-zelenych-stat-bez-ostychu-za-aktualnim-stranickym-programem/%5B%5D	http://www.zelenarevize.cz/recept-na-uspech-zelenych-stat-bez-ostychu-za-aktualnim-stranickym-programem/%5B%5D	k4
<g/>
↑	↑	k?
https://twitter.com/BobCZap/status/1095229003647143937	https://twitter.com/BobCZap/status/1095229003647143937	k4
<g/>
↑	↑	k?
Spolupředsedy	spolupředseda	k1gMnPc4
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
jsou	být	k5eAaImIp3nP
Davis	Davis	k1gInSc1
a	a	k8xC
Berg	Berg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-25	2020-01-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nové	Nové	k2eAgNnSc1d1
předsednictvo	předsednictvo	k1gNnSc1
Zelených	Zelená	k1gFnPc2
kritizuje	kritizovat	k5eAaImIp3nS
nedostatečnou	dostatečný	k2eNgFnSc4d1
činnost	činnost	k1gFnSc4
vlády	vláda	k1gFnSc2
v	v	k7c6
tématu	téma	k1gNnSc6
klimatické	klimatický	k2eAgFnSc2d1
krize	krize	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
<g/>
,	,	kIx,
2020-01-26	2020-01-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zdroj	zdroj	k1gInSc4
Hudebního	hudební	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
klipu	klip	k1gInSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
youtube	youtubat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
17.10	17.10	k4
<g/>
.2013	.2013	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
První	první	k4xOgMnSc1
politický	politický	k2eAgInSc1d1
spot	spot	k1gInSc1
ve	v	k7c6
znakovou	znakový	k2eAgFnSc7d1
řečí	řeč	k1gFnSc7
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
<g/>
.	.	kIx.
oficiální	oficiální	k2eAgInSc1d1
veřejný	veřejný	k2eAgInSc1d1
profil	profil	k1gInSc1
Strany	strana	k1gFnSc2
Zelených	Zelená	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Netradiční	tradiční	k2eNgFnSc1d1
politická	politický	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
tlakovými	tlakový	k2eAgFnPc7d1
pistolemi	pistol	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
youtube	youtubat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
com	com	k?
nahrané	nahraný	k2eAgInPc1d1
samotnou	samotný	k2eAgFnSc7d1
Stranou	strana	k1gFnSc7
Zelených	Zelených	k2eAgInSc2d1
dne	den	k1gInSc2
13.10	13.10	k4
<g/>
.2013	.2013	k4
(	(	kIx(
<g/>
v	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
2013	#num#	k4
a	a	k8xC
volebním	volební	k2eAgInSc6d1
boji	boj	k1gInSc6
o	o	k7c4
mandáty	mandát	k1gInPc4
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
tuto	tento	k3xDgFnSc4
netradiční	tradiční	k2eNgFnSc4d1
kampaň	kampaň	k1gFnSc4
užila	užít	k5eAaPmAgFnS
i	i	k9
Česká	český	k2eAgFnSc1d1
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Strana	strana	k1gFnSc1
Zelených	Zelených	k2eAgFnSc1d1
do	do	k7c2
předvolebního	předvolební	k2eAgInSc2d1
boje	boj	k1gInSc2
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
netradiční	tradiční	k2eNgFnSc4d1
kontaktní	kontaktní	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
přes	přes	k7c4
Okna	okno	k1gNnPc4
panelových	panelový	k2eAgInPc2d1
bytů	byt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
video	video	k1gNnSc1
na	na	k7c4
youtube	youtubat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
20.5	20.5	k4
<g/>
.2014	.2014	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kontaktní	kontaktní	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
v	v	k7c6
oknech	okno	k1gNnPc6
<g/>
,	,	kIx,
nasazená	nasazený	k2eAgFnSc1d1
Stranou	stranou	k6eAd1
Zelených	Zelených	k2eAgFnSc1d1
18.5	18.5	k4
<g/>
.2014	.2014	k4
do	do	k7c2
předvolebního	předvolební	k2eAgInSc2d1
boje	boj	k1gInSc2
do	do	k7c2
europarlamentu	europarlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
oficiální	oficiální	k2eAgInSc1d1
Facebookový	Facebookový	k2eAgInSc4d1
veřejný	veřejný	k2eAgInSc4d1
profil	profil	k1gInSc4
Strany	strana	k1gFnSc2
Zelených	Zelený	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kontaktní	kontaktní	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
:	:	kIx,
Převážení	převážení	k1gNnSc1
lidí	člověk	k1gMnPc2
rikšou	rikša	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
youtube	youtubat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21.10	21.10	k4
<g/>
.2013	.2013	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hodnocení	hodnocení	k1gNnSc1
kampaně	kampaň	k1gFnSc2
voleb	volba	k1gFnPc2
do	do	k7c2
PS	PS	kA
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdroj	zdroj	k1gInSc1
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Odstartováno	odstartován	k2eAgNnSc1d1
<g/>
:	:	kIx,
zelené	zelený	k2eAgInPc1d1
karavany	karavan	k1gInPc1
startují	startovat	k5eAaBmIp3nP
do	do	k7c2
obcí	obec	k1gFnPc2
a	a	k8xC
měst	město	k1gNnPc2
|	|	kIx~
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zelenit	k5eAaImIp3nP
si	se	k3xPyFc3
schválili	schválit	k5eAaPmAgMnP
zavedení	zavedení	k1gNnSc4
spolupředsednictví	spolupředsednictví	k1gNnSc2
<g/>
.	.	kIx.
brnensky	brnensky	k6eAd1
<g/>
.	.	kIx.
<g/>
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
spolupředsednictví	spolupředsednictví	k1gNnSc1
|	|	kIx~
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
Strany	strana	k1gFnSc2
zelených	zelený	k2eAgInPc2d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Stropnický	stropnický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01-23	2016-01-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pisárecký	pisárecký	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2003	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Táborský	táborský	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2003	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Olomoucký	olomoucký	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2004	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vršovický	vršovický	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2004	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Pardubický	pardubický	k2eAgInSc1d1
sjezd	sjezd	k1gInSc1
2005	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Holešovický	holešovický	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2007	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Teplický	teplický	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2008	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Štýřický	Štýřický	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2009	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Žižkovský	žižkovský	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2010	#num#	k4
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Druhý	druhý	k4xOgInSc1
pardubický	pardubický	k2eAgInSc1d1
sjezd	sjezd	k1gInSc1
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Českotřebovský	českotřebovský	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Karlínský	karlínský	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2014	#num#	k4
<g/>
.	.	kIx.
www.zeleni.cz	www.zeleni.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sjezd	sjezd	k1gInSc1
|	|	kIx~
nový	nový	k2eAgInSc1d1
intranet	intranet	k1gInSc1
<g/>
.	.	kIx.
i.	i.	k?
<g/>
zeleni	zeleň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sjezd	sjezd	k1gInSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
(	(	kIx(
<g/>
kalendář	kalendář	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.zeleni.cz	www.zeleni.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sjezd	sjezd	k1gInSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
(	(	kIx(
<g/>
kalendář	kalendář	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.zeleni.cz	www.zeleni.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sjezd	sjezd	k1gInSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
(	(	kIx(
<g/>
kalendář	kalendář	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.zeleni.cz	www.zeleni.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zelení	Zelený	k1gMnPc1
-	-	kIx~
Zelený	zelený	k2eAgInSc4d1
sjezd	sjezd	k1gInSc4
2018	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zelení	Zelený	k1gMnPc1
-	-	kIx~
Pozvánka	pozvánka	k1gFnSc1
pro	pro	k7c4
média	médium	k1gNnPc4
<g/>
:	:	kIx,
Sjezd	sjezd	k1gInSc1
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
a	a	k8xC
debata	debata	k1gFnSc1
k	k	k7c3
evropským	evropský	k2eAgFnPc3d1
volbám	volba	k1gFnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stát	stát	k1gInSc1
zaplatí	zaplatit	k5eAaPmIp3nS
stranám	strana	k1gFnPc3
za	za	k7c7
hlasy	hlas	k1gInPc7
voličů	volič	k1gMnPc2
482	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíc	hodně	k6eAd3,k6eAd1
ztratila	ztratit	k5eAaPmAgFnS
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-22	2017-10-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zelení	Zelený	k1gMnPc1
navrhli	navrhnout	k5eAaPmAgMnP
Pirátům	pirát	k1gMnPc3
spolupráci	spolupráce	k1gFnSc3
ve	v	k7c6
sněmovních	sněmovní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
ale	ale	k8xC
návrh	návrh	k1gInSc4
odmítli	odmítnout	k5eAaPmAgMnP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeleni	zeleň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016-11-29	2016-11-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://aktualne.centrum.cz/domaci/podzimni-volby-2008/clanek.phtml?id=619563	http://aktualne.centrum.cz/domaci/podzimni-volby-2008/clanek.phtml?id=619563	k4
<g/>
↑	↑	k?
http://strana.zeleni.cz/11008//stredocesky-kraj-9-pro-stranu-zelenych/%5B%5D	http://strana.zeleni.cz/11008//stredocesky-kraj-9-pro-stranu-zelenych/%5B%5D	k4
<g/>
↑	↑	k?
www.cvvm.cas.cz	www.cvvm.cas.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.ceskenoviny.cz/index_view.php?id=339416	http://www.ceskenoviny.cz/index_view.php?id=339416	k4
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
http://volby.cz/pls/kz2012/kz11?xjazyk=CZ&	http://volby.cz/pls/kz2012/kz11?xjazyk=CZ&	k?
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
)	)	kIx)
2012	#num#	k4
<g/>
↑	↑	k?
http://volby.cz/pls/kz2012/kz2?xjazyk=CZ&	http://volby.cz/pls/kz2012/kz2?xjazyk=CZ&	k?
Výsledky	výsledek	k1gInPc4
voleb	volba	k1gFnPc2
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
2012	#num#	k4
<g/>
↑	↑	k?
Zelení	zeleň	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zelení	Zelený	k1gMnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-10-04	2020-10-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
čtyřiapadesátiletý	čtyřiapadesátiletý	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Meziměstí	meziměstí	k1gNnSc2
Pavel	Pavel	k1gMnSc1
Hečko	Hečko	k1gNnSc1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
těžký	těžký	k2eAgInSc1d1
průběh	průběh	k1gInSc1
covidu-	covidu-	k?
<g/>
19	#num#	k4
|	|	kIx~
Lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-24	2021-03-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://lidovky.zpravy.cz/ln_noviny.asp?c=A061027_000049_ln_noviny_sko	http://lidovky.zpravy.cz/ln_noviny.asp?c=A061027_000049_ln_noviny_sko	k6eAd1
<g/>
↑	↑	k?
http://www.zeleni.cz/strana/nasi-lide/starostove-a-zastupitele-strany-zelenych/	http://www.zeleni.cz/strana/nasi-lide/starostove-a-zastupitele-strany-zelenych/	k?
Archivováno	archivován	k2eAgNnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Starostové	Starosta	k1gMnPc1
a	a	k8xC
zastupitelé	zastupitel	k1gMnPc1
SZ	SZ	kA
<g/>
↑	↑	k?
Zelení	zelení	k1gNnSc1
-	-	kIx~
Zelení	zelení	k1gNnSc1
v	v	k7c6
regionech	region	k1gInPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zelení	Zelený	k1gMnPc1
-	-	kIx~
Zelení	Zelený	k1gMnPc1
v	v	k7c6
regionech	region	k1gInPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pokyny	pokyn	k1gInPc1
pro	pro	k7c4
použití	použití	k1gNnSc4
stranického	stranický	k2eAgInSc2d1
fontu	font	k1gInSc2
ABCGreen	ABCGreen	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
http://strana.zeleni.cz/247/99/file/%5B%5D	http://strana.zeleni.cz/247/99/file/%5B%5D	k4
Portable	portable	k1gInSc1
Document	Document	k1gInSc4
Format	Format	k1gInSc1
2	#num#	k4
megabyte	megabyt	k1gInSc5
<g/>
↑	↑	k?
http://zpravy.idnes.cz/i-loga-politickych-stran-podlehaji-mode-podivejte-se-f9b-/domaci.asp?c=A090909_150529_domaci_adb	http://zpravy.idnes.cz/i-loga-politickych-stran-podlehaji-mode-podivejte-se-f9b-/domaci.asp?c=A090909_150529_domaci_adbat	k5eAaPmRp2nS
<g/>
↑	↑	k?
Nová	nový	k2eAgFnSc1d1
vizuální	vizuální	k2eAgFnSc1d1
identita	identita	k1gFnSc1
-	-	kIx~
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
<g/>
.	.	kIx.
www.zeleni.cz	www.zeleni.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zelení	zelený	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
ženám	žena	k1gFnPc3
(	(	kIx(
<g/>
opět	opět	k6eAd1
<g/>
)	)	kIx)
nejotevřenější	otevřený	k2eAgInPc1d3
<g/>
↑	↑	k?
Výsledky	výsledek	k1gInPc1
soutěže	soutěž	k1gFnSc2
Nadějná	nadějný	k2eAgFnSc1d1
politička	politička	k1gFnSc1
2013	#num#	k4
<g/>
↑	↑	k?
http://www.e15.cz/domaci/politika/nova-ikona-zelenych-ekonom-zamecnik%5B%5D	http://www.e15.cz/domaci/politika/nova-ikona-zelenych-ekonom-zamecnik%5B%5D	k4
Nová	nový	k2eAgFnSc1d1
ikona	ikona	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
<g/>
:	:	kIx,
ekonom	ekonom	k1gMnSc1
Zámečník	Zámečník	k1gMnSc1
<g/>
↑	↑	k?
Miroslav	Miroslav	k1gMnSc1
Zámečník	Zámečník	k1gMnSc1
on-line	on-lin	k1gInSc5
<g/>
:	:	kIx,
Která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
strana	strana	k1gFnSc1
má	mít	k5eAaImIp3nS
„	„	k?
<g/>
nej	nej	k?
<g/>
“	“	k?
ekonomický	ekonomický	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Ekonomové	ekonom	k1gMnPc1
chválí	chválit	k5eAaImIp3nP
program	program	k1gInSc4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
↑	↑	k?
Analytici	analytik	k1gMnPc1
chválí	chválit	k5eAaImIp3nP
ekonomický	ekonomický	k2eAgInSc4d1
program	program	k1gInSc4
Strany	strana	k1gFnSc2
zelených	zelená	k1gFnPc2
<g/>
↑	↑	k?
MEDIAENERGY	MEDIAENERGY	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
,	,	kIx,
Transparency	Transparency	k1gInPc7
cz	cz	k?
<g/>
,	,	kIx,
web	web	k1gInSc1
<g/>
:	:	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	zeleň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transparentní	transparentní	k2eAgFnPc1d1
volby	volba	k1gFnPc1
|	|	kIx~
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
2017	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
zeleni	zeleň	k1gFnSc2wR
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.cizp.cz/(ejadl255bfxqlsnelme0b1zv	http://www.cizp.cz/(ejadl255bfxqlsnelme0b1zv	k1gInSc1
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
default	default	k1gInSc1
<g/>
.	.	kIx.
<g/>
aspx	aspx	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
id	idy	k1gFnPc2
<g/>
=	=	kIx~
<g/>
1129	#num#	k4
<g/>
&	&	k?
<g/>
ido	ido	k1gNnSc4
<g/>
=	=	kIx~
<g/>
365	#num#	k4
<g/>
&	&	k?
<g/>
sh	sh	k?
<g/>
=	=	kIx~
<g/>
671383224	#num#	k4
<g/>
↑	↑	k?
http://www.enviweb.cz/?env=energie_archiv_gcafh/Energetika_uspory_nebudou_stacit.html	http://www.enviweb.cz/?env=energie_archiv_gcafh/Energetika_uspory_nebudou_stacit.html	k1gMnSc1
<g/>
↑	↑	k?
Temelín	Temelín	k1gInSc1
nakoupí	nakoupit	k5eAaPmIp3nS
jaderné	jaderný	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
od	od	k7c2
ruského	ruský	k2eAgInSc2d1
TVEL	TVEL	kA
|	|	kIx~
Informační	informační	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
|	|	kIx~
Skupina	skupina	k1gFnSc1
ČEZ	ČEZ	kA
<g/>
.	.	kIx.
www.cez.cz	www.cez.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
bohumildolezal	bohumildolezat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
http://blog.aktualne.centrum.cz/blogy/frantisek-janouch.php?itemid=2997	http://blog.aktualne.centrum.cz/blogy/frantisek-janouch.php?itemid=2997	k4
<g/>
↑	↑	k?
http://www.lidovky.cz/kuchtova-chce-kvuli-pacesove-komisi-koalicni-schuzku-pdn-/ln_domov.asp?c=A080706_152824_ln_domov_ter	http://www.lidovky.cz/kuchtova-chce-kvuli-pacesove-komisi-koalicni-schuzku-pdn-/ln_domov.asp?c=A080706_152824_ln_domov_ter	k1gInSc1
<g/>
↑	↑	k?
http://www.virtually.cz/?art=16952	http://www.virtually.cz/?art=16952	k4
<g/>
↑	↑	k?
http://www.radio.cz/cz/clanek/106338	http://www.radio.cz/cz/clanek/106338	k4
<g/>
↑	↑	k?
http://hp.ujf.cas.cz/~wagner/popclan/bl/spolecnostaenergieweb.htm	http://hp.ujf.cas.cz/~wagner/popclan/bl/spolecnostaenergieweb.htm	k1gMnSc1
<g/>
↑	↑	k?
http://www.csvts.cz/cns/news07/070730b.htm	http://www.csvts.cz/cns/news07/070730b.htm	k1gMnSc1
<g/>
↑	↑	k?
On-line	On-lin	k1gInSc5
rozhovor	rozhovor	k1gInSc4
<g/>
:	:	kIx,
Předseda	předseda	k1gMnSc1
Strany	strana	k1gFnSc2
zelených	zelený	k2eAgMnPc2d1
Matěj	Matěj	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.ceskenoviny.cz/zpravy/index_view.php?id=331636	http://www.ceskenoviny.cz/zpravy/index_view.php?id=331636	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Pečínka	pečínka	k1gFnSc1
<g/>
:	:	kIx,
Zelená	zelenat	k5eAaImIp3nS
zleva	zleva	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
aneb	aneb	k?
Historie	historie	k1gFnSc2
ekologických	ekologický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
G	G	kA
plus	plus	k1gInSc1
G	G	kA
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-86103-58-7	80-86103-58-7	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Strana	strana	k1gFnSc1
zelených	zelený	k2eAgMnPc2d1
volí	volit	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc4
předsedu	předseda	k1gMnSc4
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
–	–	k?
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
<g/>
;	;	kIx,
archivní	archivní	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
<g/>
:	:	kIx,
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc4
hudební	hudební	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
videoklip	videoklip	k1gInSc4
v	v	k7c6
ČR	ČR	kA
na	na	k7c6
YouTube	YouTub	k1gInSc5
</s>
<s>
Téma	téma	k1gNnSc1
„	„	k?
<g/>
Zelená	zelenat	k5eAaImIp3nS
politika	politik	k1gMnSc4
<g/>
“	“	k?
Archivováno	archivován	k2eAgNnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
na	na	k7c6
webu	web	k1gInSc6
Literárních	literární	k2eAgFnPc2d1
novin	novina	k1gFnPc2
–	–	k?
propatočkovské	propatočkovský	k2eAgFnSc2d1
</s>
<s>
Téma	téma	k1gNnSc1
„	„	k?
<g/>
Zelení	zelenit	k5eAaImIp3nS
<g/>
“	“	k?
(	(	kIx(
<g/>
ev.	ev.	k?
články	článek	k1gInPc1
Pavla	Pavel	k1gMnSc2
Pečínky	pečínka	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
Britských	britský	k2eAgInPc6d1
listech	list	k1gInPc6
–	–	k?
převážně	převážně	k6eAd1
protipatočkovské	protipatočkovský	k2eAgInPc1d1
</s>
<s>
Beránek	Beránek	k1gMnSc1
versus	versus	k7c1
Štěpánek	Štěpánek	k1gMnSc1
–	–	k?
ochočená	ochočený	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
podrobné	podrobný	k2eAgNnSc4d1
líčení	líčení	k1gNnSc4
vnitrostranického	vnitrostranický	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
v	v	k7c6
letech	léto	k1gNnPc6
2003-4	2003-4	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Feřtek	Feřtek	k1gMnSc1
<g/>
:	:	kIx,
Spor	spor	k1gInSc1
o	o	k7c4
Bursíka	Bursík	k1gMnSc4
–	–	k?
článek	článek	k1gInSc4
v	v	k7c6
časopise	časopis	k1gInSc6
Reflex	reflex	k1gInSc1
o	o	k7c6
mimořádném	mimořádný	k2eAgInSc6d1
sjezdu	sjezd	k1gInSc6
v	v	k7c6
září	září	k1gNnSc6
2003	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Zelená	zelený	k2eAgNnPc1d1
politika	politikum	k1gNnPc1
Klíčová	klíčový	k2eAgNnPc1d1
témata	téma	k1gNnPc1
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
politika	politika	k1gFnSc1
•	•	k?
Zelená	zelenat	k5eAaImIp3nS
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Seznam	seznam	k1gInSc1
zelených	zelená	k1gFnPc2
priorit	priorita	k1gFnPc2
Čtyři	čtyři	k4xCgInPc1
pilíře	pilíř	k1gInPc1
zelené	zelený	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
</s>
<s>
Ekologické	ekologický	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
•	•	k?
Sociální	sociální	k2eAgFnSc1d1
spravedlnost	spravedlnost	k1gFnSc1
•	•	k?
Účastnická	účastnický	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
•	•	k?
Nenásilí	nenásilí	k1gNnSc2
Základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
</s>
<s>
Světlezelený	světlezelený	k2eAgInSc1d1
environmentalismus	environmentalismus	k1gInSc1
•	•	k?
Hlubinná	hlubinný	k2eAgFnSc1d1
ekologie	ekologie	k1gFnSc1
•	•	k?
Ekofeminismus	Ekofeminismus	k1gInSc1
•	•	k?
Ekosocialismus	Ekosocialismus	k1gInSc1
•	•	k?
Zelený	zelený	k2eAgInSc1d1
anarchismus	anarchismus	k1gInSc1
•	•	k?
Zelený	zelený	k2eAgInSc1d1
konzervatismus	konzervatismus	k1gInSc1
•	•	k?
Zelená	Zelená	k1gFnSc1
levice	levice	k1gFnSc2
•	•	k?
Zelený	zelený	k2eAgInSc1d1
liberalismus	liberalismus	k1gInSc1
•	•	k?
Zelený	zelený	k2eAgInSc1d1
libertarianismus	libertarianismus	k1gInSc1
•	•	k?
Zelený	zelený	k2eAgInSc1d1
syndikalismus	syndikalismus	k1gInSc1
•	•	k?
Sociální	sociální	k2eAgFnSc2d1
ekologie	ekologie	k1gFnSc2
•	•	k?
Zelený	zelený	k2eAgInSc4d1
sionismus	sionismus	k1gInSc4
Organizace	organizace	k1gFnSc2
</s>
<s>
Afrika	Afrika	k1gFnSc1
•	•	k?
Amerika	Amerika	k1gFnSc1
•	•	k?
Asie	Asie	k1gFnSc2
a	a	k8xC
Pacifik	Pacifik	k1gInSc1
•	•	k?
Evropa	Evropa	k1gFnSc1
•	•	k?
Federace	federace	k1gFnSc2
mladých	mladý	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
zelených	zelená	k1gFnPc2
•	•	k?
Global	globat	k5eAaImAgMnS
Greens	Greens	k1gInSc4
•	•	k?
Mladí	mladý	k2eAgMnPc1d1
globální	globální	k2eAgInPc4d1
zelení	zeleň	k1gFnSc7
•	•	k?
Zelená	zelený	k2eAgFnSc1d1
sionistická	sionistický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
•	•	k?
Seznam	seznam	k1gInSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
Související	související	k2eAgFnSc2d1
témata	téma	k1gNnPc4
</s>
<s>
Hnutí	hnutí	k1gNnSc1
za	za	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
protředí	protředit	k5eAaPmIp3nS
•	•	k?
Ekocentrismus	Ekocentrismus	k1gInSc1
•	•	k?
Ekologická	ekologický	k2eAgFnSc1d1
modernizace	modernizace	k1gFnSc1
•	•	k?
Environmentalismus	Environmentalismus	k1gInSc1
•	•	k?
Problematika	problematika	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
Portál	portál	k1gInSc1
politika	politika	k1gFnSc1
</s>
<s>
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
a	a	k8xC
hnutí	hnutí	k1gNnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
(	(	kIx(
<g/>
200	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
2011	#num#	k4
(	(	kIx(
<g/>
78	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
63	#num#	k4
ANO	ano	k9
<g/>
,	,	kIx,
15	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
21	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
2	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
přímá	přímý	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
SPD	SPD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
13	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
</s>
<s>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
3	#num#	k4
JAP	JAP	kA
<g/>
,	,	kIx,
3	#num#	k4
Trikolóra	trikolóra	k1gFnSc1
<g/>
)	)	kIx)
Senát	senát	k1gInSc1
(	(	kIx(
<g/>
81	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ODS	ODS	kA
a	a	k8xC
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
16	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
4	#num#	k4
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
6	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
3	#num#	k4
za	za	k7c4
ODS	ODS	kA
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
a	a	k8xC
2	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
SLK	SLK	kA
<g/>
,	,	kIx,
1	#num#	k4
Ostravak	Ostravak	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
OPAT	opat	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
MHS	MHS	kA
a	a	k8xC
13	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
–	–	k?
11	#num#	k4
za	za	k7c7
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SD	SD	kA
<g/>
–	–	k?
<g/>
SN	SN	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
KDU-ČSL	KDU-ČSL	k1gFnPc2
a	a	k8xC
5	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
4	#num#	k4
za	za	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
NK	NK	kA
<g/>
)	)	kIx)
</s>
<s>
PROREGION	PROREGION	kA
(	(	kIx(
<g/>
9	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
2	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
ANO	ano	k9
a	a	k9
6	#num#	k4
bezpartijních	bezpartijní	k2eAgMnPc2d1
<g/>
,	,	kIx,
4	#num#	k4
za	za	k7c4
ANO	ano	k9
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
ČSSD	ČSSD	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
„	„	k?
<g/>
OSN	OSN	kA
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
SEN	sen	k1gInSc1
21	#num#	k4
a	a	k8xC
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
7	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
SEN	sena	k1gFnPc2
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Zelení	zeleň	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
HPP	HPP	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
3	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
;	;	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
Piráty	pirát	k1gMnPc4
a	a	k8xC
1	#num#	k4
za	za	k7c4
HDK	HDK	kA
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
S.	S.	kA
<g/>
cz	cz	k?
a	a	k8xC
2	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
1	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
a	a	k8xC
1	#num#	k4
za	za	k7c4
Svobodní	svobodný	k2eAgMnPc1d1
<g/>
)	)	kIx)
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
RE	re	k9
</s>
<s>
ODS	ODS	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ECR	ECR	kA
</s>
<s>
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
Greens	Greens	k1gInSc4
<g/>
/	/	kIx~
<g/>
EFA	EFA	kA
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
STAN	stan	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
SPD	SPD	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ID	ido	k1gNnPc2
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
KSČM	KSČM	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
GUE-NGL	GUE-NGL	k1gFnSc6
Neparlamentní	parlamentní	k2eNgFnSc2d1
</s>
<s>
Alternativa	alternativa	k1gFnSc1
</s>
<s>
ANK	ANK	kA
2020	#num#	k4
</s>
<s>
APAČI	Apač	k1gMnSc3
2017	#num#	k4
</s>
<s>
ČP	ČP	kA
</s>
<s>
EU	EU	kA
TROLL	troll	k1gMnSc1
</s>
<s>
DSSS	DSSS	kA
</s>
<s>
DV	DV	kA
2016	#num#	k4
</s>
<s>
HLAS	hlas	k1gInSc1
</s>
<s>
IO	IO	kA
</s>
<s>
JsmePRO	JsmePRO	k?
<g/>
!	!	kIx.
</s>
<s>
M	M	kA
</s>
<s>
NBPLK	NBPLK	kA
</s>
<s>
Nezávislí	závislý	k2eNgMnPc1d1
</s>
<s>
NeKa	NeKa	k6eAd1
</s>
<s>
ODA	ODA	kA
</s>
<s>
HPP	HPP	kA
</s>
<s>
PV	PV	kA
</s>
<s>
PZS	PZS	kA
</s>
<s>
Rozumní	rozumný	k2eAgMnPc1d1
</s>
<s>
SNK	SNK	kA
ED	ED	kA
</s>
<s>
SNK1	SNK1	k4
</s>
<s>
SPOLEHNUTÍ	spolehnutí	k1gNnSc1
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
osobnosti	osobnost	k1gFnPc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
</s>
<s>
Strana	strana	k1gFnSc1
Práv	právo	k1gNnPc2
Občanů	občan	k1gMnPc2
</s>
<s>
Strana	strana	k1gFnSc1
soukromníků	soukromník	k1gMnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
T2020	T2020	k4
</s>
<s>
UFO	UFO	kA
</s>
<s>
USZ	USZ	kA
</s>
<s>
ZA	za	k7c4
OBČANY	občan	k1gMnPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Z	z	k7c2
2020	#num#	k4
Seznam	seznam	k1gInSc1
neparlamentních	parlamentní	k2eNgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
není	být	k5eNaImIp3nS
úplný	úplný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
