<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
(	(	kIx(
<g/>
Předín	Předín	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
Místo	místo	k7c2
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Vysočina	vysočina	k1gFnSc1
Okres	okres	k1gInSc1
</s>
<s>
Třebíč	Třebíč	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Předín	Předín	k1gInSc1
Lokalita	lokalita	k1gFnSc1
</s>
<s>
náves	náves	k1gFnSc1
obce	obec	k1gFnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
51,81	51,81	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
26,86	26,86	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
moravská	moravský	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
brněnská	brněnský	k2eAgFnSc1d1
Děkanát	děkanát	k1gInSc1
</s>
<s>
Třebíč	Třebíč	k1gFnSc1
Farnost	farnost	k1gFnSc1
</s>
<s>
Předín	Předín	k1gInSc1
Status	status	k1gInSc1
</s>
<s>
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Užívání	užívání	k1gNnSc1
</s>
<s>
užíván	užíván	k2eAgInSc1d1
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1842	#num#	k4
Specifikace	specifikace	k1gFnSc1
Stavební	stavební	k2eAgFnSc1d1
materiál	materiál	k1gInSc4
</s>
<s>
kámen	kámen	k1gInSc1
Další	další	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
41703	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2988	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
je	být	k5eAaImIp3nS
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
římskokatolické	římskokatolický	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
Předín	Předín	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Předíně	Předína	k1gFnSc6
v	v	k7c6
centru	centrum	k1gNnSc6
obce	obec	k1gFnSc2
na	na	k7c6
návsi	náves	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
klasicistvní	klasicistvní	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1849	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
přestavěn	přestavět	k5eAaPmNgInS
ze	z	k7c2
staršího	starý	k2eAgInSc2d2
zbořeného	zbořený	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc2
součástí	součást	k1gFnPc2
je	být	k5eAaImIp3nS
pravoúhlé	pravoúhlý	k2eAgNnSc1d1
kněžiště	kněžiště	k1gNnSc1
a	a	k8xC
původní	původní	k2eAgFnSc1d1
středověká	středověký	k2eAgFnSc1d1
pozdně	pozdně	k6eAd1
románská	románský	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
z	z	k7c2
původního	původní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
dochovala	dochovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
kostele	kostel	k1gInSc6
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc1d1
oltář	oltář	k1gInSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
zasvěcen	zasvětit	k5eAaPmNgInS
svatému	svatý	k1gMnSc3
Václavovi	Václav	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xS,k8xC
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
na	na	k7c6
základech	základ	k1gInPc6
zbořeného	zbořený	k2eAgInSc2d1
původního	původní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
klasicistní	klasicistní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
havíři	havíř	k1gMnSc3
z	z	k7c2
místních	místní	k2eAgFnPc2d1
dolů	dolů	k6eAd1
na	na	k7c4
stříbro	stříbro	k1gNnSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc7
součástí	součást	k1gFnSc7
byla	být	k5eAaImAgFnS
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
i	i	k9
přes	přes	k7c4
zboření	zboření	k1gNnSc4
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
obci	obec	k1gFnSc6
nacházel	nacházet	k5eAaImAgMnS
asi	asi	k9
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1366	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Bílé	bílý	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc1
prázdný	prázdný	k2eAgInSc1d1
a	a	k8xC
Předín	Předín	k1gInSc1
spadal	spadat	k5eAaPmAgInS,k5eAaImAgInS
pod	pod	k7c4
farnost	farnost	k1gFnSc4
v	v	k7c6
Heralticích	Heraltice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1785	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Předíně	Předína	k1gFnSc6
zřízena	zřízen	k2eAgFnSc1d1
lokálie	lokálie	k1gFnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1859	#num#	k4
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
věže	věž	k1gFnSc2
je	být	k5eAaImIp3nS
zvon	zvon	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1597	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
kostel	kostel	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
DVORSKÝ	Dvorský	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastivěda	vlastivěda	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebický	Třebický	k2eAgInSc1d1
okres	okres	k1gInSc1
<g/>
..	..	k?
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Musejní	musejní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
.	.	kIx.
453	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
337	#num#	k4
<g/>
-	-	kIx~
<g/>
344	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Předín	Předín	k1gMnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Předín	Předína	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Farnost	farnost	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
biskupství	biskupství	k1gNnPc2
brněnského	brněnský	k2eAgNnSc2d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
