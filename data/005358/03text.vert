<s>
Jáchymovský	jáchymovský	k2eAgInSc1d1	jáchymovský
tolar	tolar	k1gInSc1	tolar
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Joachimstaler	Joachimstaler	k1gInSc1	Joachimstaler
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
historických	historický	k2eAgFnPc2d1	historická
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
mincí	mince	k1gFnPc2	mince
ražených	ražený	k2eAgFnPc2d1	ražená
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1519	[number]	k4	1519
až	až	k9	až
1528	[number]	k4	1528
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
vidíme	vidět	k5eAaImIp1nP	vidět
reliéfní	reliéfní	k2eAgFnSc4d1	reliéfní
postavu	postava	k1gFnSc4	postava
svatého	svatý	k2eAgMnSc2d1	svatý
Jáchyma	Jáchym	k1gMnSc2	Jáchym
se	s	k7c7	s
Šlikovským	Šlikovský	k2eAgInSc7d1	Šlikovský
erbem	erb	k1gInSc7	erb
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
Svatý	svatý	k2eAgInSc4d1	svatý
Jáchym	Jáchym	k1gMnSc1	Jáchym
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
mince	mince	k1gFnSc2	mince
čteme	číst	k5eAaImIp1nP	číst
nápis	nápis	k1gInSc4	nápis
<g/>
:	:	kIx,	:
ARMA	ARMA	kA	ARMA
<g/>
=	=	kIx~	=
<g/>
DUOB	DUOB	kA	DUOB
<g/>
=	=	kIx~	=
<g/>
SLICOUV	SLICOUV	kA	SLICOUV
<g/>
=	=	kIx~	=
<g/>
STEFANI	STEFANI	kA	STEFANI
<g/>
=	=	kIx~	=
<g/>
ET	ET	kA	ET
<g/>
=	=	kIx~	=
<g/>
BRATR	bratr	k1gMnSc1	bratr
<g/>
=	=	kIx~	=
<g/>
COMITU	COMITU	kA	COMITU
<g/>
=	=	kIx~	=
<g/>
D	D	kA	D
<g/>
=	=	kIx~	=
<g/>
BASAIA	BASAIA	kA	BASAIA
-	-	kIx~	-
Mince	mince	k1gFnSc1	mince
pánů	pan	k1gMnPc2	pan
Šliků	Šliek	k1gMnPc2	Šliek
Štěpána	Štěpán	k1gMnSc2	Štěpán
a	a	k8xC	a
bratří	bratřit	k5eAaImIp3nS	bratřit
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Bassana	Bassan	k1gMnSc2	Bassan
(	(	kIx(	(
<g/>
Arma	Armus	k1gMnSc2	Armus
Dominorum	Dominorum	k1gInSc1	Dominorum
Slickorum	Slickorum	k1gNnSc4	Slickorum
Stephani	Stephaň	k1gFnSc3	Stephaň
et	et	k?	et
Fratrum	Fratrum	k1gNnSc1	Fratrum
Comitum	Comitum	k1gNnSc1	Comitum
de	de	k?	de
Bassano	Bassana	k1gFnSc5	Bassana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
svatý	svatý	k2eAgMnSc1d1	svatý
Jáchym	Jáchym	k1gMnSc1	Jáchym
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
několika	několik	k4yIc2	několik
ražeb	ražba	k1gFnPc2	ražba
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
zpočátku	zpočátku	k6eAd1	zpočátku
<g/>
)	)	kIx)	)
nahrazen	nahradit	k5eAaPmNgInS	nahradit
iniciálami	iniciála	k1gFnPc7	iniciála
SI	si	k1gNnSc2	si
a	a	k8xC	a
letopočtem	letopočet	k1gInSc7	letopočet
ražby	ražba	k1gFnSc2	ražba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
Tolaru	tolar	k1gInSc2	tolar
vidíme	vidět	k5eAaImIp1nP	vidět
Českého	český	k2eAgInSc2d1	český
královského	královský	k2eAgInSc2d1	královský
lva	lev	k1gInSc2	lev
a	a	k8xC	a
nápis	nápis	k1gInSc1	nápis
LUDOVICUS-PRIM-D-GRACIA-REX-BOE	LUDOVICUS-PRIM-D-GRACIA-REX-BOE	k1gFnSc2	LUDOVICUS-PRIM-D-GRACIA-REX-BOE
=	=	kIx~	=
Ludvík	Ludvík	k1gMnSc1	Ludvík
první	první	k4xOgFnSc2	první
z	z	k7c2	z
Boží	boží	k2eAgFnSc2d1	boží
milosti	milost	k1gFnSc2	milost
král	král	k1gMnSc1	král
Český	český	k2eAgMnSc1d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
panovníka	panovník	k1gMnSc2	panovník
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
měnilo	měnit	k5eAaImAgNnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Vzorem	vzor	k1gInSc7	vzor
jáchymovského	jáchymovský	k2eAgInSc2d1	jáchymovský
tolaru	tolar	k1gInSc2	tolar
byla	být	k5eAaImAgFnS	být
saská	saský	k2eAgFnSc1d1	saská
mince	mince	k1gFnSc1	mince
<g/>
,	,	kIx,	,
ražená	ražený	k2eAgFnSc1d1	ražená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1492-1525	[number]	k4	1492-1525
ve	v	k7c6	v
Cvikově	Cvikov	k1gInSc6	Cvikov
a	a	k8xC	a
Schneebergu	Schneeberg	k1gInSc2	Schneeberg
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
podle	podle	k7c2	podle
čepic	čepice	k1gFnPc2	čepice
vyobrazených	vyobrazený	k2eAgMnPc2d1	vyobrazený
panovníků	panovník	k1gMnPc2	panovník
německy	německy	k6eAd1	německy
Klappmützentaler	Klappmützentaler	k1gMnSc1	Klappmützentaler
<g/>
.	.	kIx.	.
</s>
<s>
Jáchymovský	jáchymovský	k2eAgInSc4d1	jáchymovský
tolar	tolar	k1gInSc4	tolar
jí	jíst	k5eAaImIp3nS	jíst
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
jak	jak	k6eAd1	jak
svojí	svůj	k3xOyFgFnSc7	svůj
váhou	váha	k1gFnSc7	váha
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ryzostí	ryzost	k1gFnPc2	ryzost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
však	však	k9	však
byly	být	k5eAaImAgInP	být
jáchymovské	jáchymovský	k2eAgInPc1d1	jáchymovský
tolary	tolar	k1gInPc1	tolar
opatřeny	opatřen	k2eAgInPc1d1	opatřen
letopočty	letopočet	k1gInPc1	letopočet
<g/>
:	:	kIx,	:
1520	[number]	k4	1520
<g/>
,	,	kIx,	,
1525	[number]	k4	1525
<g/>
,	,	kIx,	,
1526	[number]	k4	1526
<g/>
,	,	kIx,	,
1527	[number]	k4	1527
a	a	k8xC	a
1528	[number]	k4	1528
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
mince	mince	k1gFnPc1	mince
vážily	vážit	k5eAaImAgFnP	vážit
29,23	[number]	k4	29,23
g	g	kA	g
při	při	k7c6	při
obsahu	obsah	k1gInSc6	obsah
27,41	[number]	k4	27,41
g	g	kA	g
čistého	čistý	k2eAgNnSc2d1	čisté
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Ražba	ražba	k1gFnSc1	ražba
jáchymovského	jáchymovský	k2eAgInSc2d1	jáchymovský
tolaru	tolar	k1gInSc2	tolar
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
rodu	rod	k1gInSc2	rod
Šliků	Šliek	k1gMnPc2	Šliek
kteří	který	k3yRgMnPc1	který
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
těžbu	těžba	k1gFnSc4	těžba
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stříbro	stříbro	k1gNnSc4	stříbro
vyváželi	vyvážet	k5eAaImAgMnP	vyvážet
do	do	k7c2	do
Norimberka	Norimberk	k1gInSc2	Norimberk
<g/>
;	;	kIx,	;
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
si	se	k3xPyFc3	se
však	však	k8xC	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
ražbou	ražba	k1gFnSc7	ražba
mincí	mince	k1gFnPc2	mince
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
většího	veliký	k2eAgInSc2d2	veliký
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Ražbu	ražba	k1gFnSc4	ražba
Šlikům	Šliek	k1gMnPc3	Šliek
oficiálně	oficiálně	k6eAd1	oficiálně
povolil	povolit	k5eAaPmAgMnS	povolit
český	český	k2eAgInSc4d1	český
zemský	zemský	k2eAgInSc4d1	zemský
sněm	sněm	k1gInSc4	sněm
svým	svůj	k3xOyFgNnSc7	svůj
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1520	[number]	k4	1520
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
jim	on	k3xPp3gMnPc3	on
povolil	povolit	k5eAaPmAgMnS	povolit
razit	razit	k5eAaImF	razit
"	"	kIx"	"
<g/>
větší	veliký	k2eAgInPc4d2	veliký
groše	groš	k1gInPc4	groš
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
rýnských	rýnský	k2eAgFnPc2d1	Rýnská
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
polovin	polovina	k1gFnPc2	polovina
a	a	k8xC	a
čtvrtin	čtvrtina	k1gFnPc2	čtvrtina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnPc1	mince
byly	být	k5eAaImAgFnP	být
raženy	razit	k5eAaImNgFnP	razit
v	v	k7c6	v
jáchymovské	jáchymovský	k2eAgFnSc6d1	Jáchymovská
královské	královský	k2eAgFnSc6d1	královská
mincovně	mincovna	k1gFnSc6	mincovna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
několika	několik	k4yIc2	několik
zdrojů	zdroj	k1gInPc2	zdroj
(	(	kIx(	(
<g/>
jáchymovský	jáchymovský	k2eAgMnSc1d1	jáchymovský
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
latiny	latina	k1gFnSc2	latina
Johannes	Johannes	k1gMnSc1	Johannes
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Georgius	Georgius	k1gMnSc1	Georgius
Agricola	Agricola	k1gFnSc1	Agricola
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
Miesel	Miesel	k1gMnSc1	Miesel
<g/>
)	)	kIx)	)
však	však	k9	však
byly	být	k5eAaImAgInP	být
tolary	tolar	k1gInPc1	tolar
raženy	razit	k5eAaImNgInP	razit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
úředního	úřední	k2eAgNnSc2d1	úřední
svolení	svolení	k1gNnSc2	svolení
vznikaly	vznikat	k5eAaImAgFnP	vznikat
zřejmě	zřejmě	k6eAd1	zřejmě
ve	v	k7c6	v
sklepech	sklep	k1gInPc6	sklep
šlikovského	šlikovský	k2eAgInSc2d1	šlikovský
hrádku	hrádek	k1gInSc2	hrádek
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Jáchymova	Jáchymov	k1gInSc2	Jáchymov
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
běžných	běžný	k2eAgInPc2d1	běžný
tolarů	tolar	k1gInPc2	tolar
byly	být	k5eAaImAgFnP	být
raženy	ražen	k2eAgFnPc1d1	ražena
mince	mince	k1gFnPc1	mince
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
čtvrt	čtvrt	k1xP	čtvrt
nebo	nebo	k8xC	nebo
půl	půl	k1xP	půl
tolaru	tolar	k1gInSc2	tolar
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1520	[number]	k4	1520
byly	být	k5eAaImAgInP	být
vzácně	vzácně	k6eAd1	vzácně
raženy	ražen	k2eAgInPc1d1	ražen
dvou-	dvou-	k?	dvou-
<g/>
,	,	kIx,	,
tří-	tří-	k?	tří-
a	a	k8xC	a
čtyřtolary	čtyřtolara	k1gFnSc2	čtyřtolara
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
byly	být	k5eAaImAgInP	být
jáchymovské	jáchymovský	k2eAgInPc1d1	jáchymovský
tolary	tolar	k1gInPc1	tolar
raženy	ražen	k2eAgInPc1d1	ražen
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
mincovně	mincovna	k1gFnSc6	mincovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
Horním	horní	k2eAgInSc6d1	horní
Slavkově	Slavkov	k1gInSc6	Slavkov
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
vyraženo	vyrazit	k5eAaPmNgNnS	vyrazit
asi	asi	k9	asi
1,3	[number]	k4	1,3
miliónu	milión	k4xCgInSc2	milión
jáchymovských	jáchymovský	k2eAgInPc2d1	jáchymovský
tolarů	tolar	k1gInPc2	tolar
<g/>
.	.	kIx.	.
</s>
<s>
Ražba	ražba	k1gFnSc1	ražba
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1528	[number]	k4	1528
z	z	k7c2	z
příkazu	příkaz	k1gInSc2	příkaz
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
Od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
jáchymovského	jáchymovský	k2eAgInSc2d1	jáchymovský
tolaru	tolar	k1gInSc2	tolar
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
odvozovaly	odvozovat	k5eAaImAgInP	odvozovat
názvy	název	k1gInPc1	název
dalších	další	k2eAgFnPc2d1	další
mincí	mince	k1gFnPc2	mince
a	a	k8xC	a
měn	měna	k1gFnPc2	měna
-	-	kIx~	-
tolary	tolar	k1gInPc1	tolar
a	a	k8xC	a
dolary	dolar	k1gInPc1	dolar
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Např.	např.	kA	např.
ještě	ještě	k9	ještě
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
originále	originál	k1gInSc6	originál
děl	dělo	k1gNnPc2	dělo
Karla	Karel	k1gMnSc2	Karel
Maye	May	k1gMnSc2	May
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
označení	označení	k1gNnSc1	označení
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
německým	německý	k2eAgInSc7d1	německý
výrazem	výraz	k1gInSc7	výraz
Taler	Taler	k1gMnSc1	Taler
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Joachimstaler	Joachimstaler	k1gInSc1	Joachimstaler
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
SMOLÍK	Smolík	k1gMnSc1	Smolík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
s	s	k7c7	s
pány	pan	k1gMnPc7	pan
Šliky	šlika	k1gFnSc2	šlika
o	o	k7c4	o
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
minci	mince	k1gFnSc4	mince
Jáchymovskou	jáchymovský	k2eAgFnSc4d1	Jáchymovská
<g/>
.	.	kIx.	.
</s>
<s>
Památky	památka	k1gFnPc1	památka
archaeologické	archaeologický	k2eAgFnSc2d1	archaeologický
a	a	k8xC	a
místopisné	místopisný	k2eAgFnSc2d1	místopisná
<g/>
.	.	kIx.	.
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
333	[number]	k4	333
<g/>
-	-	kIx~	-
<g/>
340	[number]	k4	340
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
VYTLAČIL	Vytlačil	k1gMnSc1	Vytlačil
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
renesančního	renesanční	k2eAgInSc2d1	renesanční
Jáchymova	Jáchymov	k1gInSc2	Jáchymov
<g/>
;	;	kIx,	;
in	in	k?	in
<g/>
:	:	kIx,	:
Evangelicus	Evangelicus	k1gInSc1	Evangelicus
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
s.	s.	k?	s.
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgFnPc1d1	dostupná
on-line	onin	k1gInSc5	on-lin
<g/>
)	)	kIx)	)
Platidlo	platidlo	k1gNnSc1	platidlo
Mince	mince	k1gFnSc2	mince
Groš	groš	k1gInSc1	groš
Dukát	dukát	k1gInSc1	dukát
</s>
