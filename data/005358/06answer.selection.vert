<s>
Jáchymovský	jáchymovský	k2eAgInSc1d1	jáchymovský
tolar	tolar	k1gInSc1	tolar
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Joachimstaler	Joachimstaler	k1gInSc1	Joachimstaler
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
historických	historický	k2eAgFnPc2d1	historická
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
mincí	mince	k1gFnPc2	mince
ražených	ražený	k2eAgFnPc2d1	ražená
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1519	[number]	k4	1519
až	až	k9	až
1528	[number]	k4	1528
<g/>
.	.	kIx.	.
</s>
