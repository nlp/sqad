<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
O	o	k7c6
mužském	mužský	k2eAgInSc6d1
basketbalovém	basketbalový	k2eAgInSc6d1
klubu	klub	k1gInSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
USK	USK	kA
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
Název	název	k1gInSc1
</s>
<s>
Závody	závod	k1gInPc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
vzduchotechnických	vzduchotechnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Praha	Praha	k1gFnSc1
Země	země	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Město	město	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Holešovice	Holešovice	k1gFnPc1
<g/>
)	)	kIx)
Založení	založení	k1gNnSc1
</s>
<s>
1953	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Asociace	asociace	k1gFnSc2
</s>
<s>
ČBF	ČBF	kA
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Barvy	barva	k1gFnSc2
</s>
<s>
žlutá	žlutý	k2eAgFnSc1d1
a	a	k8xC
modrá	modrý	k2eAgFnSc1d1
Stadion	stadion	k1gNnSc4
Název	název	k1gInSc1
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
Královka	Královka	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
Kapacita	kapacita	k1gFnSc1
</s>
<s>
2	#num#	k4
500	#num#	k4
</s>
<s>
Management	management	k1gInSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Kučera	Kučera	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Natália	Natália	k1gFnSc1
Hejková	Hejkový	k2eAgFnSc1d1
Největší	veliký	k2eAgFnSc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
8	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Československa	Československo	k1gNnSc2
v	v	k7c6
soutěži	soutěž	k1gFnSc6
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
)	)	kIx)
15	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
soutěži	soutěž	k1gFnSc6
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
,	,	kIx,
Domácí	domácí	k2eAgInPc1d1
trofeje	trofej	k1gInPc1
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
Český	český	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
Pohár	pohár	k1gInSc4
Ronchettiové	Ronchettiový	k2eAgFnSc2d1
1	#num#	k4
<g/>
×	×	k?
Euroliga	Euroliga	k1gFnSc1
v	v	k7c6
basketbale	basketbal	k1gInSc6
žen	žena	k1gFnPc2
1	#num#	k4
<g/>
×	×	k?
Superpohár	superpohár	k1gInSc4
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Pohár	pohár	k1gInSc1
Ronchettiové	Ronchettiový	k2eAgFnSc2d1
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
Euroliga	Euroliga	k1gFnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
Superpohár	superpohár	k1gInSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
2015	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
1957	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
ITVS	ITVS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
1973	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1975	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
1980	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
1981	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
1988	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
</s>
<s>
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
</s>
<s>
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
</s>
<s>
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
</s>
<s>
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
</s>
<s>
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
</s>
<s>
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Závody	závod	k1gInPc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
vzduchotechnických	vzduchotechnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
ženský	ženský	k2eAgInSc1d1
basketbalový	basketbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
pražských	pražský	k2eAgFnPc6d1
Holešovicích	Holešovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
1993	#num#	k4
působí	působit	k5eAaImIp3nS
v	v	k7c6
české	český	k2eAgFnSc6d1
nejvyšší	vysoký	k2eAgFnSc6d3
basketbalové	basketbalový	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
známé	známý	k2eAgInPc4d1
pod	pod	k7c7
názvem	název	k1gInSc7
Ženská	ženský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
klub	klub	k1gInSc4
nikdy	nikdy	k6eAd1
nesestoupil	sestoupit	k5eNaPmAgMnS
z	z	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubové	klubový	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
jsou	být	k5eAaImIp3nP
žlutá	žlutý	k2eAgFnSc1d1
a	a	k8xC
modrá	modrý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jednadvacetinásobného	jednadvacetinásobný	k2eAgMnSc4d1
mistra	mistr	k1gMnSc4
české	český	k2eAgNnSc1d1
<g/>
,	,	kIx,
potažmo	potažmo	k6eAd1
československé	československý	k2eAgFnSc2d1
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
počet	počet	k1gInSc1
titulů	titul	k1gInPc2
jej	on	k3xPp3gMnSc4
umisťuje	umisťovat	k5eAaImIp3nS
na	na	k7c4
celkové	celkový	k2eAgNnSc4d1
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
za	za	k7c7
první	první	k4xOgFnSc7
Spartou	Sparta	k1gFnSc7
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
třiadvacet	třiadvacet	k4xCc4
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
domácím	domácí	k2eAgInSc6d1
poháru	pohár	k1gInSc6
má	mít	k5eAaImIp3nS
USK	USK	kA
na	na	k7c6
kontě	konto	k1gNnSc6
pět	pět	k4xCc4
vítězství	vítězství	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jej	on	k3xPp3gMnSc4
umisťuje	umisťovat	k5eAaImIp3nS
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
za	za	k7c4
první	první	k4xOgInSc4
Basket	basket	k1gInSc4
Žabiny	Žabiny	k?
Brno	Brno	k1gNnSc1
se	s	k7c7
šestnácti	šestnáct	k4xCc2
prvenstvími	prvenství	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
je	být	k5eAaImIp3nS
největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
vítězství	vítězství	k1gNnSc2
v	v	k7c6
Poháru	pohár	k1gInSc6
Ronchettiové	Ronchettius	k1gMnPc1
ze	z	k7c2
sezóny	sezóna	k1gFnSc2
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Eurolize	Euroliza	k1gFnSc6
ze	z	k7c2
sezóny	sezóna	k1gFnSc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
a	a	k8xC
následně	následně	k6eAd1
i	i	k9
v	v	k7c6
Superpoháru	superpohár	k1gInSc6
FIBA	FIBA	kA
v	v	k7c6
ročníku	ročník	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
tento	tento	k3xDgInSc4
klub	klub	k1gInSc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
hráli	hrát	k5eAaImAgMnP
současně	současně	k6eAd1
tři	tři	k4xCgFnPc4
vicemistryně	vicemistryně	k1gFnPc4
světa	svět	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
a	a	k8xC
české	český	k2eAgFnPc1d1
reprezentantky	reprezentantka	k1gFnPc1
Ilona	Ilona	k1gFnSc1
Burgrová	Burgrová	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
r.	r.	kA
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
Elhotová	Elhotový	k2eAgFnSc1d1
(	(	kIx(
<g/>
dosud	dosud	k6eAd1
v	v	k7c6
klubu	klub	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
Alena	Alena	k1gFnSc1
Hanušová	Hanušová	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
r.	r.	kA
2019	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrává	odehrávat	k5eAaImIp3nS
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
hale	hala	k1gFnSc6
Královka	Královka	k1gFnSc1
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
2	#num#	k4
500	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1953	#num#	k4
–	–	k?
Slavia	Slavia	k1gFnSc1
ITVS	ITVS	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Slavia	Slavia	k1gFnSc1
Institut	institut	k1gInSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1960	#num#	k4
–	–	k?
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Slavia	Slavia	k1gFnSc1
Vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
–	–	k?
USK	USK	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
–	–	k?
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
–	–	k?
USK	USK	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1999	#num#	k4
–	–	k?
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
–	–	k?
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Závody	závod	k1gInPc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
vzduchotechnických	vzduchotechnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Získané	získaný	k2eAgFnPc4d1
trofeje	trofej	k1gFnPc4
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
domácí	domácí	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
žen	žena	k1gFnPc2
/	/	kIx~
Ženská	ženský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
23	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Český	český	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
(	(	kIx(
6	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Vyhrané	vyhraný	k2eAgFnPc1d1
mezinárodní	mezinárodní	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
</s>
<s>
Pohár	pohár	k1gInSc4
Ronchettiové	Ronchettius	k1gMnPc1
(	(	kIx(
1	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Euroliga	Euroliga	k1gFnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
(	(	kIx(
1	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Superpohár	superpohár	k1gInSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
(	(	kIx(
1	#num#	k4
<g/>
×	×	k?
)	)	kIx)
</s>
<s>
2015	#num#	k4
</s>
<s>
Soupiska	soupiska	k1gFnSc1
sezóny	sezóna	k1gFnSc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Hráčka	hráčka	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Výška	výška	k1gFnSc1
</s>
<s>
Příchod	příchod	k1gInSc1
z	z	k7c2
</s>
<s>
1	#num#	k4
<g/>
Kristýna	Kristýna	k1gFnSc1
Brabencová	Brabencová	k1gFnSc1
<g/>
1999181	#num#	k4
cmBasket	cmBasket	k1gMnSc1
Žabiny	Žabiny	k?
Brno	Brno	k1gNnSc4
–	–	k?
mládež	mládež	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
Leticia	Leticia	k1gFnSc1
Romero	Romero	k1gNnSc1
<g/>
1995173	#num#	k4
cmFlorida	cmFlorida	k1gFnSc1
State	status	k1gInSc5
Seminoles	Seminoles	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
,	,	kIx,
univerzita	univerzita	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
<g/>
Julie	Julie	k1gFnSc1
Pospíšilova	Pospíšilův	k2eAgNnSc2d1
<g/>
1999182	#num#	k4
cmAritma	cmAritmum	k1gNnSc2
Praha	Praha	k1gFnSc1
–	–	k?
mládež	mládež	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
Karolína	Karolína	k1gFnSc1
Elhotová	Elhotový	k2eAgFnSc1d1
<g/>
1992182	#num#	k4
cmVŠ	cmVŠ	k?
Praha	Praha	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
Alena	Alena	k1gFnSc1
Hanušová	Hanušová	k1gFnSc1
<g/>
1991191	#num#	k4
cmBasket	cmBasket	k1gInSc1
Žabiny	Žabiny	k?
Brno	Brno	k1gNnSc1
</s>
<s>
8	#num#	k4
<g/>
Veronika	Veronika	k1gFnSc1
Voráčková	Voráčková	k1gFnSc1
<g/>
1999175	#num#	k4
cmBK	cmBK	k?
Strakonice	Strakonice	k1gFnPc1
</s>
<s>
9	#num#	k4
<g/>
Marija	Marija	k1gMnSc1
Režan	Režan	k1gMnSc1
<g/>
1989175	#num#	k4
cmCCC	cmCCC	k?
Polkowice	Polkowice	k1gFnSc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
Marta	Marta	k1gFnSc1
Xargay	Xargaa	k1gFnSc2
<g/>
1990180	#num#	k4
cmPhoenix	cmPhoenix	k1gInSc1
Mercury	Mercura	k1gFnSc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
,	,	kIx,
WNBA	WNBA	kA
<g/>
)	)	kIx)
</s>
<s>
11	#num#	k4
<g/>
Kateřina	Kateřina	k1gFnSc1
Elhotová	Elhotový	k2eAgFnSc1d1
<g/>
1989180	#num#	k4
cmZVVZ	cmZVVZ	k?
USK	USK	kA
Praha	Praha	k1gFnSc1
–	–	k?
mládež	mládež	k1gFnSc1
</s>
<s>
12	#num#	k4
<g/>
Tereza	Tereza	k1gFnSc1
Vyoralová	Vyoralová	k1gFnSc1
<g/>
1994176	#num#	k4
cmVŠ	cmVŠ	k?
Praha	Praha	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
Veronika	Veronika	k1gFnSc1
Šípová	Šípová	k1gFnSc1
<g/>
1999181	#num#	k4
cmBK	cmBK	k?
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
–	–	k?
mládež	mládež	k1gFnSc1
</s>
<s>
15	#num#	k4
<g/>
Aneta	Aneta	k1gFnSc1
Mainclová	Mainclová	k1gFnSc1
<g/>
1996191	#num#	k4
cmVŠ	cmVŠ	k?
Praha	Praha	k1gFnSc1
</s>
<s>
17	#num#	k4
<g/>
Amanda	Amanda	k1gFnSc1
Zahui	Zahu	k1gFnSc2
<g/>
1993196	#num#	k4
cmNaděžda	cmNaděžda	k1gMnSc1
Orenburg	Orenburg	k1gMnSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
20	#num#	k4
<g/>
Michaela	Michaela	k1gFnSc1
Krejzová	Krejzová	k1gFnSc1
<g/>
1998178	#num#	k4
cmVŠ	cmVŠ	k?
Praha	Praha	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
Dewanna	Dewann	k1gInSc2
Bonner	Bonner	k1gMnSc1
<g/>
1987180	#num#	k4
cmPhoenix	cmPhoenix	k1gInSc1
Mercury	Mercura	k1gFnSc2
(	(	kIx(
<g/>
USA	USA	kA
<g/>
,	,	kIx,
WNBA	WNBA	kA
<g/>
)	)	kIx)
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
ZČ	ZČ	kA
-	-	kIx~
základní	základní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1954	#num#	k4
–	–	k?
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
v	v	k7c6
ročníku	ročník	k1gInSc6
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
ZČ	ZČ	kA
</s>
<s>
Play-off	Play-off	k1gMnSc1
</s>
<s>
1954	#num#	k4
<g/>
/	/	kIx~
<g/>
55	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
ITVS	ITVS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
ČSR	ČSR	kA
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1955	#num#	k4
<g/>
/	/	kIx~
<g/>
56	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
ITVS	ITVS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
ITVS	ITVS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
ITVS	ITVS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
ITVS	ITVS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
ITVS	ITVS	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
Slavia	Slavia	k1gFnSc1
VŠ	vš	k0
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
prohra	prohra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
prohra	prohra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
v	v	k7c6
ročníku	ročník	k1gInSc6
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
ZČ	ZČ	kA
</s>
<s>
Play-off	Play-off	k1gMnSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
USK	USK	kA
Erpet	Erpet	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
USK	USK	kA
Blex	Blex	k1gInSc1
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
úroveň	úroveň	k1gFnSc1
</s>
<s>
ŽBL	ŽBL	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
mezinárodních	mezinárodní	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výkonnostní	výkonnostní	k2eAgFnPc1d1
úrovně	úroveň	k1gFnPc1
</s>
<s>
superpohár	superpohár	k1gInSc1
–	–	k?
Superpohár	superpohár	k1gInSc4
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
–	–	k?
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
Euroliga	Euroliga	k1gFnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
–	–	k?
Pohár	pohár	k1gInSc1
Ronchettiové	Ronchettius	k1gMnPc1
<g/>
,	,	kIx,
EuroCup	EuroCup	k1gMnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
EL	Ela	k1gFnPc2
-	-	kIx~
Euroliga	Euroliga	k1gFnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
PMEZ	PMEZ	kA
-	-	kIx~
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
EC	EC	kA
-	-	kIx~
EuroCup	EuroCup	k1gMnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
SP	SP	kA
-	-	kIx~
Superpohár	superpohár	k1gInSc4
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
PR	pr	k0
-	-	kIx~
Pohár	pohár	k1gInSc4
Ronchettiové	Ronchettiový	k2eAgFnSc2d1
</s>
<s>
PMEZ	PMEZ	kA
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
–	–	k?
Finále	finále	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
</s>
<s>
PR	pr	k0
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc2
</s>
<s>
PR	pr	k0
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
PR	pr	k0
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc2
</s>
<s>
PR	pr	k0
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc2
</s>
<s>
PMEZ	PMEZ	kA
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
</s>
<s>
PR	pr	k0
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PR	pr	k0
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PR	pr	k0
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
–	–	k?
Předkolo	předkolo	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PMEZ	PMEZ	kA
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
PR	pr	k0
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc2
</s>
<s>
PR	pr	k0
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
–	–	k?
Šestnáctifinále	Šestnáctifinále	k1gNnSc2
</s>
<s>
PR	pr	k0
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
H	H	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
E	E	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EC	EC	kA
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EL	Ela	k1gFnPc2
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EC	EC	kA
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
<g/>
,	,	kIx,
Střed	střed	k1gInSc1
</s>
<s>
EL	Ela	k1gFnPc2
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EL	Ela	k1gFnPc2
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
EL	Ela	k1gFnPc2
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EL	Ela	k1gFnPc2
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc2
</s>
<s>
EL	Ela	k1gFnPc2
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
EL	Ela	k1gFnPc2
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
EL	Ela	k1gFnPc2
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
EL	Ela	k1gFnPc2
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
–	–	k?
Osmifinále	osmifinále	k1gNnSc2
</s>
<s>
EL	Ela	k1gFnPc2
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
–	–	k?
Čtvrtfinálová	čtvrtfinálový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
EL	Ela	k1gFnPc2
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
</s>
<s>
SP	SP	kA
2015	#num#	k4
–	–	k?
Vítěz	vítěz	k1gMnSc1
</s>
<s>
EL	Ela	k1gFnPc2
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
–	–	k?
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
prohra	prohra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
EL	Ela	k1gFnPc2
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
–	–	k?
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
prohra	prohra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
EL	Ela	k1gFnPc2
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
EL	Ela	k1gFnPc2
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
–	–	k?
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Historie	historie	k1gFnSc1
společnosti	společnost	k1gFnSc2
ZVVZ	ZVVZ	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
zvvz	zvvz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
O	o	k7c6
klubu	klub	k1gInSc6
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
uskpraha	uskpraha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Přehled	přehled	k1gInSc1
soutěží	soutěž	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
cbf	cbf	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Titul	titul	k1gInSc1
číslo	číslo	k1gNnSc1
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basketbalistky	basketbalistka	k1gFnSc2
USK	USK	kA
Praha	Praha	k1gFnSc1
smázly	smáznout	k5eAaPmAgInP
Žabiny	Žabiny	k?
i	i	k9
bez	bez	k7c2
dvou	dva	k4xCgFnPc2
opor	opora	k1gFnPc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
24.04	24.04	k4
<g/>
.2019	.2019	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Tým	tým	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
uskbasket	uskbasket	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ing.	ing.	kA
Pavel	Pavel	k1gMnSc1
Šimák	Šimák	k1gMnSc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
československého	československý	k2eAgInSc2d1
basketbalu	basketbal	k1gInSc2
v	v	k7c6
číslech	číslo	k1gNnPc6
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Basketbalový	basketbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ÚV	ÚV	kA
ČSTV	ČSTV	kA
<g/>
,	,	kIx,
květen	květen	k1gInSc4
1985	#num#	k4
<g/>
,	,	kIx,
174	#num#	k4
stran	strana	k1gFnPc2
<g/>
↑	↑	k?
Ing.	ing.	kA
Pavel	Pavel	k1gMnSc1
Šimák	Šimák	k1gMnSc1
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Historie	historie	k1gFnSc1
československého	československý	k2eAgInSc2d1
basketbalu	basketbal	k1gInSc2
v	v	k7c6
číslech	číslo	k1gNnPc6
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
část	část	k1gFnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
-	-	kIx~
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
a	a	k8xC
slovenská	slovenský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
,	,	kIx,
březen	březen	k1gInSc1
1993	#num#	k4
<g/>
,	,	kIx,
130	#num#	k4
stran	strana	k1gFnPc2
<g/>
↑	↑	k?
Juraj	Juraj	k1gMnSc1
Gacík	Gacík	k1gMnSc1
<g/>
:	:	kIx,
Kronika	kronika	k1gFnSc1
československého	československý	k2eAgInSc2d1
a	a	k8xC
slovenského	slovenský	k2eAgInSc2d1
basketbalu	basketbal	k1gInSc2
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-	-	kIx~
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
2000	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
,	,	kIx,
slovensky	slovensky	k6eAd1
<g/>
,	,	kIx,
BADEM	BADEM	kA
<g/>
,	,	kIx,
Žilina	Žilina	k1gFnSc1
<g/>
,	,	kIx,
943	#num#	k4
stran	strana	k1gFnPc2
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Women	Women	k2eAgInSc1d1
Basketball	Basketball	k1gInSc1
European	Europeana	k1gFnPc2
Champions	Championsa	k1gFnPc2
Cup	cup	k1gInSc1
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
todor	todor	k1gInSc1
<g/>
66	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Women	Women	k2eAgInSc1d1
Basketball	Basketball	k1gInSc1
European	Europeana	k1gFnPc2
Ronchetti	Ronchetť	k1gFnSc2
Cup	cup	k1gInSc1
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
todor	todor	k1gInSc1
<g/>
66	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
History	Histor	k1gInPc4
(	(	kIx(
<g/>
Ronchetti	Ronchetti	k1gNnSc3
Cup	cup	k1gInSc1
1972	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fibaeurope	fibaeurop	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
History	Histor	k1gInPc1
(	(	kIx(
<g/>
FIBA	FIBA	kA
Euroleague	Euroleague	k1gFnSc1
2001	#num#	k4
<g/>
–	–	k?
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
archive	archiv	k1gInSc5
<g/>
.	.	kIx.
<g/>
fiba	fibum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
History	Histor	k1gInPc1
(	(	kIx(
<g/>
FIBA	FIBA	kA
Europe	Europ	k1gInSc5
Cup	cup	k1gInSc1
2003	#num#	k4
<g/>
–	–	k?
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fibaeurope	fibaeurop	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Historický	historický	k2eAgInSc1d1
bronz	bronz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
USK	USK	kA
Praha	Praha	k1gFnSc1
slaví	slavit	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc4
euroligovou	euroligový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
14.04	14.04	k4
<g/>
.2019	.2019	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ženská	ženský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
a	a	k8xC
basketbalové	basketbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
Kluby	klub	k1gInPc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
KP	KP	kA
Brno	Brno	k1gNnSc1
•	•	k?
BK	BK	kA
Žabiny	Žabiny	k?
Brno	Brno	k1gNnSc4
•	•	k?
Sokol	Sokol	k1gInSc1
Nilfisk	Nilfisk	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
•	•	k?
Levharti	levhart	k1gMnPc1
Chomutov	Chomutov	k1gInSc1
•	•	k?
Slovanka	Slovanka	k1gFnSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
BLK	BLK	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
ZVVZ	ZVVZ	kA
USK	USK	kA
Praha	Praha	k1gFnSc1
•	•	k?
SBŠ	SBŠ	kA
Ostrava	Ostrava	k1gFnSc1
•	•	k?
BK	BK	kA
Strakonice	Strakonice	k1gFnPc1
•	•	k?
BK	BK	kA
Loko	Loko	k6eAd1
Trutnov	Trutnov	k1gInSc1
ŽBL	ŽBL	kA
<g/>
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
<g/>
:	:	kIx,
1993	#num#	k4
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
ŽBL	ŽBL	kA
<g/>
:	:	kIx,
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
SK	Sk	kA
NEPA	NEPA	kA
Univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Slávia	Slávia	k1gFnSc1
Havířov	Havířov	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Aritma	Aritma	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BK	BK	kA
PAVEXIM	PAVEXIM	kA
Králův	Králův	k2eAgInSc4d1
Dvůr	Dvůr	k1gInSc4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
BC	BC	kA
Libochovice	Libochovice	k1gFnPc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BK	BK	kA
Žďas	Žďas	k1gInSc1
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BK	BK	kA
Kaučuk	kaučuk	k1gInSc1
Kralupy	Kralupy	k1gInPc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BK	BK	kA
Kompresory	kompresor	k1gInPc4
Praha	Praha	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Basketsport	Basketsport	k1gInSc1
EMOS	EMOS	kA
Přerov	Přerov	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
UP	UP	kA
Olomouc	Olomouc	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BK	BK	kA
Skřivánek	Skřivánek	k1gMnSc1
Ústí	ústit	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
2007	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BLC	BLC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BK	BK	kA
Tábor	Tábor	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BA	ba	k9
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
BK	BK	kA
Studánka	studánka	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BK	BK	kA
SŠMH	SŠMH	kA
Brno	Brno	k1gNnSc4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DSK	DSK	kA
Karlín	Karlín	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
•	•	k?
VŠ	vš	k0
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Teamstore	Teamstor	k1gMnSc5
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
BK	BK	kA
Lokomotiva	lokomotiva	k1gFnSc1
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc7
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DSK	DSK	kA
Basketball	Basketball	k1gInSc1
Nymburk	Nymburk	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
•	•	k?
Český	český	k2eAgInSc1d1
pohár	pohár	k1gInSc1
-	-	kIx~
ženy	žena	k1gFnPc1
•	•	k?
Basketbalista	basketbalista	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Česká	český	k2eAgFnSc1d1
basketbalistka	basketbalistka	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Basketbal	basketbal	k1gInSc4
muži	muž	k1gMnPc1
:	:	kIx,
Národní	národní	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
•	•	k?
All-Star	All-Star	k1gInSc1
zápasy	zápas	k1gInPc1
(	(	kIx(
<g/>
utkání	utkání	k1gNnPc1
hvězd	hvězda	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Český	český	k2eAgInSc4d1
pohár	pohár	k1gInSc4
•	•	k?
Česká	český	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
•	•	k?
Český	český	k2eAgMnSc1d1
basketbalista	basketbalista	k1gMnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
