<s>
Kyselina	kyselina	k1gFnSc1
3	#num#	k4
<g/>
-dehydrochinová	-dehydrochinová	k1gFnSc1
(	(	kIx(
<g/>
její	její	k3xOp3gInSc1
aniont	aniont	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
3	#num#	k4
<g/>
-dehydrochinát	-dehydrochinát	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
DHQ	DHQ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prvním	první	k4xOgInSc7
karbocyklickým	karbocyklický	k2eAgInSc7d1
produktem	produkt	k1gInSc7
v	v	k7c6
šikimátové	šikimátový	k2eAgFnSc6d1
metabolické	metabolický	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
</s>