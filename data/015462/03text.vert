<s>
Kyselina	kyselina	k1gFnSc1
3	#num#	k4
<g/>
-dehydrochinová	-dehydrochinová	k1gFnSc1
</s>
<s>
Kyselina	kyselina	k1gFnSc1
3	#num#	k4
<g/>
-dehydrochinová	-dehydrochinová	k1gFnSc1
</s>
<s>
Strukturní	strukturní	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
R	R	kA
<g/>
,3	,3	k4
<g/>
R	R	kA
<g/>
,4	,4	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
1,3	1,3	k4
<g/>
,4	,4	k4
<g/>
-trihydroxy-	-trihydroxy-	k?
<g/>
5	#num#	k4
<g/>
-oxocyklohexankarboxylová	-oxocyklohexankarboxylový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
-Dehydroquinic	-Dehydroquinice	k1gFnPc2
acid	acid	k6eAd1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
C7H10O6	C7H10O6	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
439351	#num#	k4
</s>
<s>
ChEBI	ChEBI	k?
</s>
<s>
17947	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
O	O	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
1	#num#	k4
<g/>
C	C	kA
<g/>
[	[	kIx(
<g/>
C	C	kA
<g/>
@@	@@	k?
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
O	o	k7c4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
[	[	kIx(
<g/>
C	C	kA
<g/>
@@	@@	k?
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
O	o	k7c4
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
C	C	kA
<g/>
[	[	kIx(
<g/>
C	C	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
@@	@@	k?
<g/>
H	H	kA
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
O	o	k7c4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
C	C	kA
<g/>
@@	@@	k?
<g/>
H	H	kA
<g/>
]	]	kIx)
<g/>
1	#num#	k4
<g/>
OO	OO	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
1	#num#	k4
<g/>
[	[	kIx(
<g/>
C	C	kA
<g/>
@@	@@	k?
<g/>
H	H	kA
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
O	o	k7c4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
C	C	kA
<g/>
@	@	kIx~
<g/>
H	H	kA
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
C	C	kA
<g/>
[	[	kIx(
<g/>
C	C	kA
<g/>
@	@	kIx~
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
O	o	k7c4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
(	(	kIx(
<g/>
=	=	kIx~
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
C	C	kA
<g/>
1	#num#	k4
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
7	#num#	k4
<g/>
H	H	kA
<g/>
10	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
13,6	13,6	k4
<g/>
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
5	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
3,5	3,5	k4
<g/>
,8	,8	k4
<g/>
,10	,10	k4
<g/>
,13	,13	k4
<g/>
H	H	kA
<g/>
,1	,1	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
<g/>
(	(	kIx(
<g/>
H	H	kA
<g/>
,11	,11	k4
<g/>
,12	,12	k4
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
t	t	k?
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
,5	,5	k4
<g/>
+	+	kIx~
<g/>
,7	,7	k4
<g/>
-	-	kIx~
<g/>
/	/	kIx~
<g/>
m	m	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
1	#num#	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
190,15	190,15	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kyselina	kyselina	k1gFnSc1
3	#num#	k4
<g/>
-dehydrochinová	-dehydrochinová	k1gFnSc1
(	(	kIx(
<g/>
její	její	k3xOp3gInSc1
aniont	aniont	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
3	#num#	k4
<g/>
-dehydrochinát	-dehydrochinát	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
DHQ	DHQ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prvním	první	k4xOgInSc7
karbocyklickým	karbocyklický	k2eAgInSc7d1
produktem	produkt	k1gInSc7
v	v	k7c6
šikimátové	šikimátový	k2eAgFnSc6d1
metabolické	metabolický	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
z	z	k7c2
3	#num#	k4
<g/>
-deoxyarabinoheptulózonát	-deoxyarabinoheptulózonát	k1gInSc1
7	#num#	k4
<g/>
-fosfátu	-fosfát	k1gInSc2
<g/>
,	,	kIx,
sedmiuhlíkaté	sedmiuhlíkatý	k2eAgFnSc2d1
ulonové	ulonový	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
<g/>
,	,	kIx,
působením	působení	k1gNnSc7
enzymu	enzym	k1gInSc2
3	#num#	k4
<g/>
-dehydrochinátsyntázy	-dehydrochinátsyntáza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechanismus	mechanismus	k1gInSc1
uzavření	uzavření	k1gNnSc2
kruhu	kruh	k1gInSc2
je	být	k5eAaImIp3nS
složitější	složitý	k2eAgMnSc1d2
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
aldolovou	aldolový	k2eAgFnSc4d1
kondenzaci	kondenzace	k1gFnSc4
na	na	k7c6
druhém	druhý	k4xOgInSc6
a	a	k8xC
sedmém	sedmý	k4xOgInSc6
uhlíku	uhlík	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
kyselina	kyselina	k1gFnSc1
má	mít	k5eAaImIp3nS
téměř	téměř	k6eAd1
stejnou	stejný	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
jako	jako	k8xS,k8xC
kyselina	kyselina	k1gFnSc1
chinová	chinová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
kávě	káva	k1gFnSc6
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
hydroxyl	hydroxyl	k1gInSc1
na	na	k7c6
třetím	třetí	k4xOgInSc6
uhlíku	uhlík	k1gInSc6
je	být	k5eAaImIp3nS
zoxidovaný	zoxidovaný	k2eAgInSc1d1
na	na	k7c4
ketonovou	ketonový	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
pěti	pět	k4xCc7
enzymatickými	enzymatický	k2eAgFnPc7d1
reakcemi	reakce	k1gFnPc7
v	v	k7c6
šikimátové	šikimátový	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
z	z	k7c2
ní	on	k3xPp3gFnSc2
vznikne	vzniknout	k5eAaPmIp3nS
kyselina	kyselina	k1gFnSc1
chorismová	chorismová	k1gFnSc1
<g/>
,	,	kIx,
prekurzor	prekurzor	k1gInSc1
tyrosinu	tyrosina	k1gFnSc4
<g/>
,	,	kIx,
fenylalaninu	fenylalanina	k1gFnSc4
<g/>
,	,	kIx,
tryptofanu	tryptofana	k1gFnSc4
a	a	k8xC
některých	některý	k3yIgInPc2
vitaminů	vitamin	k1gInPc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
vitamín	vitamín	k1gInSc4
K	K	kA
a	a	k8xC
kyselina	kyselina	k1gFnSc1
listová	listový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
-dehydrochinát	-dehydrochinát	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k9
prekurzorem	prekurzor	k1gInSc7
pyrrolochinolinchinonu	pyrrolochinolinchinon	k1gInSc2
(	(	kIx(
<g/>
PQQ	PQQ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
alternativního	alternativní	k2eAgInSc2d1
redoxního	redoxní	k2eAgInSc2d1
koenzymu	koenzym	k1gInSc2
v	v	k7c6
oxidativní	oxidativní	k2eAgFnSc6d1
fosforylaci	fosforylace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Metabolismus	metabolismus	k1gInSc1
</s>
<s>
U	u	k7c2
3	#num#	k4
<g/>
-dehydrochinátu	-dehydrochinát	k1gInSc2
probíhá	probíhat	k5eAaImIp3nS
beta-oxidace	beta-oxidace	k1gFnSc1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
u	u	k7c2
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
transaminaci	transaminace	k1gFnSc3
vzniklého	vzniklý	k2eAgNnSc2d1
6	#num#	k4
<g/>
-oxo-	-oxo-	k?
<g/>
3	#num#	k4
<g/>
-dehydrochinátu	-dehydrochinát	k1gInSc2
za	za	k7c2
vzniku	vznik	k1gInSc2
6	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
3	#num#	k4
<g/>
-dehydrochinátu	-dehydrochinát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
dehydratován	dehydratovat	k5eAaBmNgInS
a	a	k8xC
redukován	redukovat	k5eAaBmNgInS
na	na	k7c6
6	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
4	#num#	k4
<g/>
-desoxy-	-desoxy-	k?
<g/>
3	#num#	k4
<g/>
-ketochinát	-ketochinát	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
reaguje	reagovat	k5eAaBmIp3nS
s	s	k7c7
dehydroalaninem	dehydroalanin	k1gInSc7
a	a	k8xC
2	#num#	k4
<g/>
-oxoglutarátem	-oxoglutarát	k1gInSc7
za	za	k7c2
vzniku	vznik	k1gInSc2
hexahydropyrrolochinolinchinonu	hexahydropyrrolochinolinchinon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
sloučenina	sloučenina	k1gFnSc1
je	být	k5eAaImIp3nS
oxidována	oxidovat	k5eAaBmNgFnS
FAD	FAD	kA
na	na	k7c4
PQQ	PQQ	kA
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
3	#num#	k4
<g/>
-Dehydroquinic	-Dehydroquinice	k1gFnPc2
acid	acid	k6eAd1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
|	|	kIx~
Biologie	biologie	k1gFnSc1
</s>
