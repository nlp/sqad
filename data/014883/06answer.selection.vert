<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vypnutí	vypnutí	k1gNnSc3
(	(	kIx(
<g/>
tzv.	tzv.	kA
blackoutu	blackout	k1gMnSc3
<g/>
)	)	kIx)
některých	některý	k3yIgFnPc2
jazykových	jazykový	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
internetové	internetový	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
Wikipedie	Wikipedie	k1gFnSc2
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
připravované	připravovaný	k2eAgFnSc3d1
evropské	evropský	k2eAgFnSc3d1
směrnici	směrnice	k1gFnSc3
o	o	k7c6
autorském	autorský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
na	na	k7c6
jednotném	jednotný	k2eAgInSc6d1
digitálním	digitální	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
o	o	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
parlamentní	parlamentní	k2eAgNnSc1d1
plénum	plénum	k1gNnSc1
chystalo	chystat	k5eAaImAgNnS
hlasovat	hlasovat	k5eAaImF
před	před	k7c7
koncem	konec	k1gInSc7
března	březen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Konkrétně	konkrétně	k6eAd1
se	se	k3xPyFc4
tohoto	tento	k3xDgInSc2
protestu	protest	k1gInSc2
zúčastnily	zúčastnit	k5eAaPmAgFnP
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
slovenská	slovenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
německá	německý	k2eAgFnSc1d1
a	a	k8xC
dánská	dánský	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>