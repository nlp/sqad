<s>
Vypnutí	vypnutí	k1gNnSc4
Wikipedií	Wikipedie	k1gFnPc2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
</s>
<s>
Vypnutí	vypnutí	k1gNnSc4
Wikipedií	Wikipedie	k1gFnPc2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
Snímek	snímek	k1gInSc1
obrazovky	obrazovka	k1gFnSc2
české	český	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
pořízený	pořízený	k2eAgInSc4d1
při	při	k7c6
průběhu	průběh	k1gInSc6
blackoutu	blackout	k1gMnSc3
Místo	místo	k7c2
</s>
<s>
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
slovenská	slovenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
německá	německý	k2eAgFnSc1d1
a	a	k8xC
dánská	dánský	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
Datum	datum	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vypnutí	vypnutí	k1gNnSc3
(	(	kIx(
<g/>
tzv.	tzv.	kA
blackoutu	blackout	k1gMnSc3
<g/>
)	)	kIx)
některých	některý	k3yIgFnPc2
jazykových	jazykový	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
internetové	internetový	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
Wikipedie	Wikipedie	k1gFnSc2
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
připravované	připravovaný	k2eAgFnSc3d1
evropské	evropský	k2eAgFnSc3d1
směrnici	směrnice	k1gFnSc3
o	o	k7c6
autorském	autorský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
na	na	k7c6
jednotném	jednotný	k2eAgInSc6d1
digitálním	digitální	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
o	o	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
parlamentní	parlamentní	k2eAgNnSc1d1
plénum	plénum	k1gNnSc1
chystalo	chystat	k5eAaImAgNnS
hlasovat	hlasovat	k5eAaImF
před	před	k7c7
koncem	konec	k1gInSc7
března	březen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Konkrétně	konkrétně	k6eAd1
se	se	k3xPyFc4
tohoto	tento	k3xDgInSc2
protestu	protest	k1gInSc2
zúčastnily	zúčastnit	k5eAaPmAgFnP
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
slovenská	slovenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
německá	německý	k2eAgFnSc1d1
a	a	k8xC
dánská	dánský	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
jazykové	jazykový	k2eAgFnPc1d1
verze	verze	k1gFnPc1
<g/>
,	,	kIx,
italská	italský	k2eAgNnPc1d1
<g/>
,	,	kIx,
portugalská	portugalský	k2eAgNnPc1d1
<g/>
,	,	kIx,
španělská	španělský	k2eAgNnPc1d1
nebo	nebo	k8xC
polská	polský	k2eAgFnSc1d1
<g/>
,	,	kIx,
zvolily	zvolit	k5eAaPmAgFnP
podobný	podobný	k2eAgInSc4d1
styl	styl	k1gInSc4
protestu	protest	k1gInSc2
již	již	k6eAd1
v	v	k7c6
červenci	červenec	k1gInSc6
2018	#num#	k4
<g/>
,	,	kIx,
před	před	k7c7
tehdy	tehdy	k6eAd1
očekávaným	očekávaný	k2eAgNnSc7d1
hlasováním	hlasování	k1gNnSc7
o	o	k7c6
směrnici	směrnice	k1gFnSc6
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c4
protest	protest	k1gInSc4
vypnuta	vypnut	k2eAgFnSc1d1
italská	italský	k2eAgFnSc1d1
<g/>
,	,	kIx,
galicijská	galicijský	k2eAgFnSc1d1
<g/>
,	,	kIx,
asturská	asturský	k2eAgFnSc1d1
a	a	k8xC
katalánská	katalánský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
</s>
<s>
V	v	k7c6
pondělí	pondělí	k1gNnSc6
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
se	se	k3xPyFc4
komunita	komunita	k1gFnSc1
české	český	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
dohodla	dohodnout	k5eAaPmAgFnS
na	na	k7c6
jednodenním	jednodenní	k2eAgNnSc6d1
vypnutí	vypnutí	k1gNnSc6
české	český	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
novou	nový	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlasování	hlasování	k1gNnSc1
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
celkem	celkem	k6eAd1
61	#num#	k4
wikipedistů	wikipedista	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
8	#num#	k4
wikipedistů	wikipedista	k1gMnPc2
hlasovalo	hlasovat	k5eAaImAgNnS
proti	proti	k7c3
protestu	protest	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Komunitě	komunita	k1gFnSc3
české	český	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
se	se	k3xPyFc4
nelíbily	líbit	k5eNaImAgInP
především	především	k9
články	článek	k1gInPc1
11	#num#	k4
a	a	k8xC
13	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
součástí	součást	k1gFnSc7
směrnice	směrnice	k1gFnSc2
o	o	k7c6
autorském	autorský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
na	na	k7c6
jednotném	jednotný	k2eAgInSc6d1
digitálním	digitální	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
„	„	k?
<g/>
Wikipedie	Wikipedie	k1gFnSc1
tak	tak	k6eAd1
bude	být	k5eAaImBp3nS
hůře	zle	k6eAd2
přebírat	přebírat	k5eAaImF
obsah	obsah	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
<g/>
,	,	kIx,
tvorba	tvorba	k1gFnSc1
nového	nový	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
se	se	k3xPyFc4
proto	proto	k8xC
značně	značně	k6eAd1
ztíží	ztížet	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
považujeme	považovat	k5eAaImIp1nP
za	za	k7c4
velký	velký	k2eAgInSc4d1
problém	problém	k1gInSc4
pro	pro	k7c4
budoucí	budoucí	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
i	i	k8xC
samotnou	samotný	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
Wikipedie	Wikipedie	k1gFnSc2
<g/>
,	,	kIx,
<g/>
“	“	k?
sdělil	sdělit	k5eAaPmAgMnS
Vojtěch	Vojtěch	k1gMnSc1
Dostál	Dostál	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
spolku	spolek	k1gInSc2
Wikimedia	Wikimedium	k1gNnSc2
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Nekomerční	komerční	k2eNgInPc1d1
internetové	internetový	k2eAgInPc1d1
projekty	projekt	k1gInPc1
typu	typ	k1gInSc2
Wikipedie	Wikipedie	k1gFnSc2
byly	být	k5eAaImAgFnP
sice	sice	k8xC
z	z	k7c2
pravidel	pravidlo	k1gNnPc2
obsažených	obsažený	k2eAgNnPc2d1
v	v	k7c6
článku	článek	k1gInSc6
13	#num#	k4
vyňaty	vyňat	k2eAgInPc1d1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
přesto	přesto	k8xC
se	se	k3xPyFc4
Nadace	nadace	k1gFnSc1
Wikimedia	Wikimedium	k1gNnSc2
obávala	obávat	k5eAaImAgFnS
nepřímých	přímý	k2eNgMnPc2d1
dopadů	dopad	k1gInPc2
na	na	k7c4
činnost	činnost	k1gFnSc4
přispěvatelů	přispěvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nadace	nadace	k1gFnSc1
zároveň	zároveň	k6eAd1
některé	některý	k3yIgInPc4
aspekty	aspekt	k1gInPc4
reformy	reforma	k1gFnSc2
ocenila	ocenit	k5eAaPmAgFnS
<g/>
,	,	kIx,
např.	např.	kA
zaručení	zaručení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
vůči	vůči	k7c3
originálním	originální	k2eAgFnPc3d1
reprodukcím	reprodukce	k1gFnPc3
veřejně	veřejně	k6eAd1
dostupných	dostupný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
nebudou	být	k5eNaImBp3nP
uplatňována	uplatňovat	k5eAaImNgNnP
žádná	žádný	k3yNgNnPc4
nová	nový	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
byla	být	k5eAaImAgFnS
vypnuta	vypnut	k2eAgFnSc1d1
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
na	na	k7c4
24	#num#	k4
hodin	hodina	k1gFnPc2
od	od	k7c2
půlnoci	půlnoc	k1gFnSc2
do	do	k7c2
půlnoci	půlnoc	k1gFnSc2
místního	místní	k2eAgInSc2d1
středoevropského	středoevropský	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
stránek	stránka	k1gFnPc2
byl	být	k5eAaImAgInS
začerněn	začerněn	k2eAgInSc1d1
a	a	k8xC
přes	přes	k7c4
něj	on	k3xPp3gMnSc4
se	se	k3xPyFc4
zobrazovala	zobrazovat	k5eAaImAgFnS
informace	informace	k1gFnPc4
o	o	k7c6
důvodech	důvod	k1gInPc6
vypnutí	vypnutí	k1gNnSc2
spolu	spolu	k6eAd1
s	s	k7c7
výzvou	výzva	k1gFnSc7
čtenářům	čtenář	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
oslovili	oslovit	k5eAaPmAgMnP
své	svůj	k3xOyFgInPc4
europoslance	europoslanec	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
historicky	historicky	k6eAd1
první	první	k4xOgNnSc4
vypnutí	vypnutí	k1gNnPc2
české	český	k2eAgFnSc2d1
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
čtenáře	čtenář	k1gMnSc4
nepříjemné	příjemný	k2eNgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
něco	něco	k3yInSc1
mnohem	mnohem	k6eAd1
horšího	zlý	k2eAgNnSc2d2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
realitou	realita	k1gFnSc7
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
uvedená	uvedený	k2eAgFnSc1d1
směrnice	směrnice	k1gFnSc1
bude	být	k5eAaImBp3nS
v	v	k7c6
předložené	předložený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
schválena	schválen	k2eAgFnSc1d1
a	a	k8xC
začne	začít	k5eAaPmIp3nS
platit	platit	k5eAaImF
<g/>
,	,	kIx,
<g/>
“	“	k?
uvedl	uvést	k5eAaPmAgMnS
Vojtěch	Vojtěch	k1gMnSc1
Dostál	Dostál	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komentátor	komentátor	k1gMnSc1
serveru	server	k1gInSc2
Info	Info	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Adam	Adam	k1gMnSc1
Kotrbatý	Kotrbatý	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
publikoval	publikovat	k5eAaBmAgMnS
na	na	k7c6
webech	web	k1gInPc6
vydavatelského	vydavatelský	k2eAgInSc2d1
domu	dům	k1gInSc2
Czech	Czech	k1gInSc1
News	News	k1gInSc1
Center	centrum	k1gNnPc2
Info	Info	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
komentář	komentář	k1gInSc4
kritizující	kritizující	k2eAgInPc4d1
údajné	údajný	k2eAgInPc4d1
„	„	k?
<g/>
pokrytectví	pokrytectví	k1gNnSc1
a	a	k8xC
hlad	hlad	k1gInSc1
po	po	k7c6
penězích	peníze	k1gInPc6
<g/>
“	“	k?
samotné	samotný	k2eAgFnPc4d1
Wikipedie	Wikipedie	k1gFnPc4
ve	v	k7c6
sporu	spor	k1gInSc6
o	o	k7c4
předmětnou	předmětný	k2eAgFnSc4d1
směrnici	směrnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokládal	dokládat	k5eAaImAgMnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
<g/>
,	,	kIx,
že	že	k8xS
Nadace	nadace	k1gFnSc1
Wikimedia	Wikimedium	k1gNnSc2
obdržela	obdržet	k5eAaPmAgFnS
v	v	k7c6
průběhu	průběh	k1gInSc6
předchozích	předchozí	k2eAgNnPc2d1
10	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
společnosti	společnost	k1gFnSc2
Google	Google	k1gFnSc2
finanční	finanční	k2eAgInPc4d1
příspěvky	příspěvek	k1gInPc4
v	v	k7c6
celkové	celkový	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
asi	asi	k9
7,5	7,5	k4
milionu	milion	k4xCgInSc2
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
přepočtu	přepočet	k1gInSc6
asi	asi	k9
170	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nich	on	k3xPp3gFnPc2
naposledy	naposledy	k6eAd1
3,1	3,1	k4
milionu	milion	k4xCgInSc2
v	v	k7c6
lednu	leden	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
zdůvodnil	zdůvodnit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
Wikipedie	Wikipedie	k1gFnSc1
nevystupuje	vystupovat	k5eNaImIp3nS
v	v	k7c6
záležitosti	záležitost	k1gFnSc6
nezávisle	závisle	k6eNd1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
ve	v	k7c6
vleku	vlek	k1gInSc6
svých	svůj	k3xOyFgMnPc2
finančních	finanční	k2eAgMnPc2d1
zájmů	zájem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
směrnice	směrnice	k1gFnSc2
se	se	k3xPyFc4
Wikipedie	Wikipedie	k1gFnSc1
přímo	přímo	k6eAd1
nedotkne	dotknout	k5eNaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
dle	dle	k7c2
něj	on	k3xPp3gNnSc2
nehrozí	hrozit	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
cenzura	cenzura	k1gFnSc1
ani	ani	k8xC
preventivní	preventivní	k2eAgNnSc1d1
mazání	mazání	k1gNnSc1
obsahu	obsah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Kotrbatého	Kotrbatý	k2eAgInSc2d1
by	by	kYmCp3nS
právě	právě	k9
směrnice	směrnice	k1gFnSc1
měla	mít	k5eAaImAgFnS
„	„	k?
<g/>
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
pomoci	pomoct	k5eAaPmF
vydavatelům	vydavatel	k1gMnPc3
k	k	k7c3
férovějšímu	férový	k2eAgNnSc3d2
postavení	postavení	k1gNnSc3
při	při	k7c6
jednáních	jednání	k1gNnPc6
s	s	k7c7
duopolem	duopol	k1gInSc7
technologických	technologický	k2eAgInPc2d1
gigantů	gigant	k1gInPc2
Googlu	Googl	k1gInSc2
a	a	k8xC
Facebooku	Facebook	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
zpravodajské	zpravodajský	k2eAgInPc4d1
články	článek	k1gInPc4
pojednávající	pojednávající	k2eAgInPc4d1
o	o	k7c6
chystaném	chystaný	k2eAgInSc6d1
protestu	protest	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
původně	původně	k6eAd1
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
publikovány	publikován	k2eAgInPc1d1
na	na	k7c6
některých	některý	k3yIgFnPc6
ze	z	k7c2
serverů	server	k1gInPc2
Czech	Czech	k1gInSc4
News	News	k1gInSc4
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
později	pozdě	k6eAd2
již	již	k6eAd1
nedostupné	dostupný	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
článek	článek	k1gInSc1
„	„	k?
<g/>
Česká	český	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
se	se	k3xPyFc4
už	už	k6eAd1
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
vypne	vypnout	k5eAaPmIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
Bojí	bát	k5eAaImIp3nS
se	se	k3xPyFc4
zhoršení	zhoršení	k1gNnSc1
kvality	kvalita	k1gFnSc2
<g/>
“	“	k?
na	na	k7c4
Blesk	blesk	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
článek	článek	k1gInSc1
„	„	k?
<g/>
Česká	český	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
protestuje	protestovat	k5eAaBmIp3nS
proti	proti	k7c3
reformě	reforma	k1gFnSc3
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
nebude	být	k5eNaImBp3nS
fungovat	fungovat	k5eAaImF
<g/>
“	“	k?
na	na	k7c4
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Německá	německý	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
</s>
<s>
Německá	německý	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
na	na	k7c4
nesouhlas	nesouhlas	k1gInSc4
se	se	k3xPyFc4
směrnicí	směrnice	k1gFnSc7
upozornila	upozornit	k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
září	září	k1gNnSc6
2018	#num#	k4
zobrazováním	zobrazování	k1gNnPc3
varovného	varovný	k2eAgInSc2d1
banneru	banner	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Hlasování	hlasování	k1gNnPc4
o	o	k7c4
vypnutí	vypnutí	k1gNnSc4
německé	německý	k2eAgFnSc2d1
verze	verze	k1gFnSc2
v	v	k7c6
březnu	březen	k1gInSc6
2019	#num#	k4
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
celkem	celkem	k6eAd1
233	#num#	k4
editorů	editor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ŠKOPEK	Škopek	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německá	německý	k2eAgFnSc1d1
Wikipedia	Wikipedium	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
nové	nový	k2eAgFnSc3d1
evropské	evropský	k2eAgFnSc3d1
směrnici	směrnice	k1gFnSc3
sama	sám	k3xTgMnSc4
vypne	vypnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
www.denik.cz	www.denik.cz	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Contra	Contra	k1gFnSc1
EU-Urheberrechtsreform	EU-Urheberrechtsreform	k1gInSc1
-	-	kIx~
Wikipedia	Wikipedium	k1gNnSc2
schaltet	schaltet	k5eAaImF,k5eAaPmF,k5eAaBmF
mal	málit	k5eAaImRp2nS
ab	ab	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schweizer	Schweizer	k1gMnSc1
Radio	radio	k1gNnSc1
und	und	k?
Fernsehen	Fernsehna	k1gFnPc2
(	(	kIx(
<g/>
SRF	SRF	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-09	2019-03-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
POLESNÝ	polesný	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrnice	směrnice	k1gFnSc1
o	o	k7c6
autorském	autorský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
jde	jít	k5eAaImIp3nS
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikipedie	Wikipedie	k1gFnSc1
protestuje	protestovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
Unie	unie	k1gFnSc1
vydavatelů	vydavatel	k1gMnPc2
podporuje	podporovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živě	živě	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KLUSKA	KLUSKA	kA
<g/>
,	,	kIx,
Vladislav	Vladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikipedie	Wikipedie	k1gFnSc1
upozorňuje	upozorňovat	k5eAaImIp3nS
na	na	k7c4
blížící	blížící	k2eAgNnSc4d1
se	se	k3xPyFc4
hlasování	hlasování	k1gNnSc4
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živě	živě	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Polish	Polish	k1gInSc1
Wikipedia	Wikipedium	k1gNnSc2
shuts	shutsa	k1gFnPc2
down	down	k1gMnSc1
in	in	k?
protest	protest	k1gInSc1
at	at	k?
EU	EU	kA
copyright	copyright	k1gInSc1
law	law	k?
<g/>
.	.	kIx.
polandin	polandin	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telewizja	Telewizja	k1gFnSc1
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
2018-07-04	2018-07-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SIY	SIY	kA
<g/>
,	,	kIx,
Sherwin	Sherwin	k1gMnSc1
<g/>
;	;	kIx,
GERLACH	GERLACH	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Four	Four	k1gMnSc1
Wikipedias	Wikipedias	k1gMnSc1
to	ten	k3xDgNnSc1
‘	‘	k?
<g/>
black	black	k1gInSc1
out	out	k?
<g/>
’	’	k?
over	over	k1gInSc1
EU	EU	kA
Copyright	copyright	k1gInSc1
Directive	Directiv	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikimedia	Wikimedium	k1gNnSc2
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
2019-03-20	2019-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Wikipedie	Wikipedie	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Pod	pod	k7c7
lípou	lípa	k1gFnSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Page	Page	k1gInSc1
Version	Version	k1gInSc1
ID	Ida	k1gFnPc2
<g/>
:	:	kIx,
17065092	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikipedia	Wikipedium	k1gNnPc4
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
vypnutá	vypnutý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problematická	problematický	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
v	v	k7c6
EU	EU	kA
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cnews	Cnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-21	2019-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikipedie	Wikipedie	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
je	být	k5eAaImIp3nS
vypnutá	vypnutý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
provozovatelé	provozovatel	k1gMnPc1
tak	tak	k6eAd1
protestují	protestovat	k5eAaBmIp3nP
proti	proti	k7c3
evropským	evropský	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
v	v	k7c6
autorském	autorský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-21	2019-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Adam	Adam	k1gMnSc1
Kotrbatý	Kotrbatý	k2eAgMnSc1d1
|	|	kIx~
info	info	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.info.cz	www.info.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOTRBATÝ	KOTRBATÝ	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kotrbatý	Kotrbatý	k2eAgInSc4d1
<g/>
:	:	kIx,
Wikipedie	Wikipedie	k1gFnSc2
stávkou	stávka	k1gFnSc7
odkryla	odkrýt	k5eAaPmAgFnS
vlastní	vlastní	k2eAgNnSc4d1
pokrytectví	pokrytectví	k1gNnSc4
a	a	k8xC
hlad	hlad	k1gInSc4
po	po	k7c6
penězích	peníze	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Info	Info	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
News	News	k1gInSc4
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
2019-03-21	2019-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
KOTRBATÝ	KOTRBATÝ	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wikipedie	Wikipedie	k1gFnSc1
stávkou	stávka	k1gFnSc7
odkryla	odkrýt	k5eAaPmAgFnS
vlastní	vlastní	k2eAgNnSc4d1
pokrytectví	pokrytectví	k1gNnSc4
a	a	k8xC
hlad	hlad	k1gInSc4
po	po	k7c6
penězích	peníze	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
News	News	k1gInSc4
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
2019-03-21	2019-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KOTRBATÝ	KOTRBATÝ	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komentář	komentář	k1gInSc1
Adama	Adam	k1gMnSc2
Kotrbatého	Kotrbatý	k2eAgMnSc2d1
<g/>
:	:	kIx,
Wikipedie	Wikipedie	k1gFnSc1
stávkou	stávka	k1gFnSc7
odkryla	odkrýt	k5eAaPmAgFnS
vlastní	vlastní	k2eAgNnSc4d1
pokrytectví	pokrytectví	k1gNnSc4
a	a	k8xC
hlad	hlad	k1gInSc4
po	po	k7c6
penězích	peníze	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
News	News	k1gInSc4
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
2019-03-21	2019-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOTRBATÝ	KOTRBATÝ	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komentář	komentář	k1gInSc1
Wikipedie	Wikipedie	k1gFnSc2
stávkou	stávka	k1gFnSc7
odkryla	odkrýt	k5eAaPmAgFnS
vlastní	vlastní	k2eAgNnSc4d1
pokrytectví	pokrytectví	k1gNnSc4
a	a	k8xC
hlad	hlad	k1gInSc4
po	po	k7c6
penězích	peníze	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
News	News	k1gInSc4
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
2019-03-21	2019-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MATSAKIS	MATSAKIS	kA
<g/>
,	,	kIx,
Louise	Louis	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
Gives	Givesa	k1gFnPc2
Wikimedia	Wikimedium	k1gNnSc2
Millions	Millionsa	k1gFnPc2
<g/>
—	—	k?
<g/>
Plus	plus	k1gInSc1
Machine	Machin	k1gInSc5
Learning	Learning	k1gInSc4
Tools	Toolsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wired	Wired	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-01-22	2019-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1059	#num#	k4
<g/>
-	-	kIx~
<g/>
1028	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Česká	český	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
se	se	k3xPyFc4
už	už	k6eAd1
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
vypne	vypnout	k5eAaPmIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
Bojí	bát	k5eAaImIp3nS
se	se	k3xPyFc4
zhoršení	zhoršení	k1gNnSc1
kvality	kvalita	k1gFnSc2
<g/>
.	.	kIx.
g	g	kA
<g/>
4	#num#	k4
<g/>
a.	a.	k?
<g/>
fun	fun	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
site	site	k1gInSc1
<g/>
%	%	kIx~
<g/>
3	#num#	k4
<g/>
A	A	kA
<g/>
www.blesk.cz	www.blesk.cza	k1gFnPc2
<g/>
%	%	kIx~
<g/>
20	#num#	k4
<g/>
Wikipedie	Wikipedie	k1gFnSc1
<g/>
%	%	kIx~
<g/>
20	#num#	k4
<g/>
se	s	k7c7
<g/>
%	%	kIx~
<g/>
20	#num#	k4
<g/>
vypne	vypnout	k5eAaPmIp3nS
–	–	k?
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zprávy	zpráva	k1gFnSc2
Google	Google	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Omlouváme	omlouvat	k5eAaImIp1nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
tento	tento	k3xDgInSc1
obsah	obsah	k1gInSc1
je	být	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
nedostupný	dostupný	k2eNgInSc1d1
<g/>
..	..	k?
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
News	News	k1gInSc4
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
protestuje	protestovat	k5eAaBmIp3nS
proti	proti	k7c3
reformě	reforma	k1gFnSc3
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
nebude	být	k5eNaImBp3nS
fungovat	fungovat	k5eAaImF
<g/>
.	.	kIx.
g	g	kA
<g/>
4	#num#	k4
<g/>
a.	a.	k?
<g/>
fun	fun	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Česká	český	k2eAgFnSc1d1
Wikipedie	Wikipedie	k1gFnSc1
protestuje	protestovat	k5eAaBmIp3nS
proti	proti	k7c3
reformě	reforma	k1gFnSc3
autorského	autorský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
nebude	být	k5eNaImBp3nS
fungovat	fungovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtusi	Čtuse	k1gFnSc4
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Omlouváme	omlouvat	k5eAaImIp1nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
tento	tento	k3xDgInSc1
obsah	obsah	k1gInSc1
je	být	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
nedostupný	dostupný	k2eNgMnSc1d1
<g/>
..	..	k?
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
News	News	k1gInSc4
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
ČÍŽEK	Čížek	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropa	Evropa	k1gFnSc1
se	se	k3xPyFc4
chystá	chystat	k5eAaImIp3nS
na	na	k7c4
nový	nový	k2eAgInSc4d1
autorský	autorský	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
rok	rok	k1gInSc4
budí	budit	k5eAaImIp3nP
emoce	emoce	k1gFnPc1
mezi	mezi	k7c7
vydavateli	vydavatel	k1gMnPc7
a	a	k8xC
kritiky	kritik	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Connect	Connect	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vypnutí	vypnutí	k1gNnSc2
Wikipedií	Wikipedie	k1gFnPc2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Právo	právo	k1gNnSc1
</s>
