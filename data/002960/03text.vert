<s>
Obec	obec	k1gFnSc1	obec
Horní	horní	k2eAgFnSc1d1	horní
Branná	branný	k2eAgFnSc1d1	Branná
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Brennei	Brennei	k1gNnSc1	Brennei
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Semily	Semily	k1gInPc1	Semily
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1833	[number]	k4	1833
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1357	[number]	k4	1357
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ves	ves	k1gFnSc1	ves
zde	zde	k6eAd1	zde
musela	muset	k5eAaImAgFnS	muset
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
existovat	existovat	k5eAaImF	existovat
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1387	[number]	k4	1387
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Valdštejnů	Valdštejn	k1gInPc2	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c4	v
Horní	horní	k2eAgFnPc4d1	horní
Branné	branný	k2eAgFnPc4d1	Branná
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Horní	horní	k2eAgFnSc1d1	horní
Branná	branný	k2eAgFnSc1d1	Branná
Myslivna	myslivna	k1gFnSc1	myslivna
za	za	k7c7	za
zámkem	zámek	k1gInSc7	zámek
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
Špitál	špitál	k1gInSc1	špitál
-	-	kIx~	-
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Aloise	Alois	k1gMnSc2	Alois
Socha	Socha	k1gMnSc1	Socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
spisovatele	spisovatel	k1gMnSc2	spisovatel
Josefa	Josef	k1gMnSc2	Josef
Šíra	Šír	k1gMnSc2	Šír
Střížkův	Střížkův	k2eAgInSc1d1	Střížkův
plátenický	plátenický	k2eAgInSc1d1	plátenický
dům	dům	k1gInSc1	dům
Hrobka	hrobka	k1gFnSc1	hrobka
-	-	kIx~	-
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
Mandlovna	mandlovna	k1gFnSc1	mandlovna
Horní	horní	k2eAgFnSc1d1	horní
Branná	branný	k2eAgFnSc1d1	Branná
Valteřice	Valteřice	k1gFnSc1	Valteřice
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
<g/>
:	:	kIx,	:
Bohdaneč	Bohdaneč	k1gInSc1	Bohdaneč
<g/>
,	,	kIx,	,
Dolení	dolení	k2eAgInSc1d1	dolení
Konec	konec	k1gInSc1	konec
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Výsplachy	Výsplach	k1gInPc1	Výsplach
<g/>
,	,	kIx,	,
Hradsko	Hradsko	k1gNnSc1	Hradsko
<g/>
,	,	kIx,	,
Končiny	končina	k1gFnPc1	končina
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Hradem	hrad	k1gInSc7	hrad
<g/>
,	,	kIx,	,
Pilousek	Pilousek	k1gMnSc1	Pilousek
Jan	Jan	k1gMnSc1	Jan
Vejrych	Vejrych	k1gMnSc1	Vejrych
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
František	František	k1gMnSc1	František
Kaván	Kaván	k1gMnSc1	Kaván
-	-	kIx~	-
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Jan	Jan	k1gMnSc1	Jan
Slavomír	Slavomír	k1gMnSc1	Slavomír
Tomíček	Tomíček	k1gMnSc1	Tomíček
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Šír	Šír	k1gMnSc1	Šír
-	-	kIx~	-
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
František	František	k1gMnSc1	František
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hakl	Hakl	k1gMnSc1	Hakl
Jiří	Jiří	k1gMnSc1	Jiří
Havel	Havel	k1gMnSc1	Havel
Mirovít	Mirovít	k1gMnSc1	Mirovít
Josef	Josef	k1gMnSc1	Josef
Král	Král	k1gMnSc1	Král
Dalibor	Dalibor	k1gMnSc1	Dalibor
Matouš	Matouš	k1gMnSc1	Matouš
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g />
.	.	kIx.	.
</s>
<s>
Pochop	Pochop	k1gMnSc1	Pochop
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Reich	Reich	k?	Reich
Václav	Václav	k1gMnSc1	Václav
Jón	Jón	k1gMnSc1	Jón
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hák	Hák	k1gMnSc1	Hák
Arnošt	Arnošt	k1gMnSc1	Arnošt
Novák	Novák	k1gMnSc1	Novák
Hynek	Hynek	k1gMnSc1	Hynek
Gross	Gross	k1gMnSc1	Gross
Bohdan	Bohdan	k1gMnSc1	Bohdan
Řehák	Řehák	k1gMnSc1	Řehák
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Střížek	střížek	k1gInSc1	střížek
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Bonaventura	Bonaventura	k1gFnSc1	Bonaventura
z	z	k7c2	z
Harrachů	Harrach	k1gInPc2	Harrach
Josef	Josef	k1gMnSc1	Josef
Semela	Semel	k1gMnSc2	Semel
Karel	Karel	k1gMnSc1	Karel
Nosál	nosál	k1gMnSc1	nosál
Anna	Anna	k1gFnSc1	Anna
Truhlářová	Truhlářová	k1gFnSc1	Truhlářová
Josef	Josef	k1gMnSc1	Josef
Janda	Janda	k1gMnSc1	Janda
Ludvík	Ludvík	k1gMnSc1	Ludvík
Šmíd	Šmíd	k1gMnSc1	Šmíd
Ilja	Ilja	k1gMnSc1	Ilja
Matouš	Matouš	k1gMnSc1	Matouš
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šimůnek	Šimůnek	k1gMnSc1	Šimůnek
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Lantzl	Lantzl	k1gMnSc1	Lantzl
Jan	Jan	k1gMnSc1	Jan
Šípař	šípař	k1gMnSc1	šípař
Zásmucký	zásmucký	k2eAgMnSc1d1	zásmucký
ze	z	k7c2	z
Zásmuk	Zásmuky	k1gInPc2	Zásmuky
Václav	Václav	k1gMnSc1	Václav
Weyrych	Weyrych	k1gMnSc1	Weyrych
z	z	k7c2	z
Gemsenfelsu	Gemsenfels	k1gInSc2	Gemsenfels
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Branné	branný	k2eAgFnSc6d1	Branná
je	být	k5eAaImIp3nS	být
silniční	silniční	k2eAgInSc1d1	silniční
a	a	k8xC	a
železniční	železniční	k2eAgInSc1d1	železniční
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
křižovatek	křižovatka	k1gFnPc2	křižovatka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
křížení	křížení	k1gNnSc1	křížení
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgInSc4d1	probíhající
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
2954	[number]	k4	2954
na	na	k7c6	na
Jilemnici	Jilemnice	k1gFnSc6	Jilemnice
a	a	k8xC	a
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
silnicí	silnice	k1gFnSc7	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
148	[number]	k4	148
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
Valteřic	Valteřice	k1gFnPc2	Valteřice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Branné	branný	k2eAgFnPc1d1	Branná
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
kříží	křížit	k5eAaImIp3nP	křížit
silnice	silnice	k1gFnPc1	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
2936	[number]	k4	2936
do	do	k7c2	do
Jilemnice	Jilemnice	k1gFnSc2	Jilemnice
a	a	k8xC	a
silnice	silnice	k1gFnSc2	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
2954	[number]	k4	2954
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
Branná	branný	k2eAgFnSc1d1	Branná
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
železniční	železniční	k2eAgFnSc4d1	železniční
stanici	stanice	k1gFnSc4	stanice
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
040	[number]	k4	040
Chlumec	Chlumec	k1gInSc1	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
-	-	kIx~	-
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
148	[number]	k4	148
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
2936	[number]	k4	2936
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
2954	[number]	k4	2954
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
2955	[number]	k4	2955
Horní	horní	k2eAgFnSc1d1	horní
Branná	branný	k2eAgFnSc1d1	Branná
Valteřice	Valteřice	k1gFnSc1	Valteřice
Horní	horní	k2eAgFnSc1d1	horní
Branná	branný	k2eAgFnSc1d1	Branná
Valteřice	Valteřice	k1gFnSc1	Valteřice
u	u	k7c2	u
mostu	most	k1gInSc2	most
Horní	horní	k2eAgFnSc1d1	horní
Branná	branný	k2eAgFnSc1d1	Branná
pošta	pošta	k1gFnSc1	pošta
Horní	horní	k2eAgFnSc1d1	horní
Branná	branný	k2eAgFnSc1d1	Branná
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Krkonošském	krkonošský	k2eAgNnSc6d1	Krkonošské
podhůří	podhůří	k1gNnSc6	podhůří
<g/>
,	,	kIx,	,
v	v	k7c6	v
Podkrkonošské	podkrkonošský	k2eAgFnSc6d1	Podkrkonošská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Lánovská	Lánovský	k2eAgFnSc1d1	Lánovský
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
;	;	kIx,	;
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
Pilousek	Pilousek	k1gInSc1	Pilousek
(	(	kIx(	(
<g/>
707	[number]	k4	707
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
části	část	k1gFnSc6	část
Valteřice	Valteřice	k1gFnSc2	Valteřice
<g/>
.	.	kIx.	.
</s>
<s>
Pilousek	Pilousek	k1gInSc1	Pilousek
(	(	kIx(	(
<g/>
707	[number]	k4	707
m	m	kA	m
<g/>
)	)	kIx)	)
Skalka	Skalka	k1gMnSc1	Skalka
(	(	kIx(	(
<g/>
687	[number]	k4	687
m	m	kA	m
<g/>
)	)	kIx)	)
Malý	malý	k2eAgInSc1d1	malý
Kozinec	kozinec	k1gInSc1	kozinec
(	(	kIx(	(
<g/>
571	[number]	k4	571
m	m	kA	m
<g/>
)	)	kIx)	)
Kozinec	kozinec	k1gInSc1	kozinec
(	(	kIx(	(
<g/>
561	[number]	k4	561
m	m	kA	m
<g/>
)	)	kIx)	)
Zuzánek	Zuzánek	k1gMnSc1	Zuzánek
(	(	kIx(	(
<g/>
617	[number]	k4	617
m	m	kA	m
<g/>
)	)	kIx)	)
Na	na	k7c6	na
Vrších	vrš	k1gFnPc6	vrš
(	(	kIx(	(
<g/>
Krkonošské	krkonošský	k2eAgNnSc1d1	Krkonošské
podhůří	podhůří	k1gNnSc1	podhůří
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
534	[number]	k4	534
m	m	kA	m
<g/>
)	)	kIx)	)
Principálek	principálek	k1gMnSc1	principálek
(	(	kIx(	(
<g/>
523	[number]	k4	523
m	m	kA	m
<g/>
)	)	kIx)	)
Veselý	veselý	k2eAgInSc1d1	veselý
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
Krkonošské	krkonošský	k2eAgNnSc1d1	Krkonošské
podhůří	podhůří	k1gNnSc1	podhůří
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
497	[number]	k4	497
m	m	kA	m
<g/>
)	)	kIx)	)
Horka	horka	k1gFnSc1	horka
(	(	kIx(	(
<g/>
511	[number]	k4	511
m	m	kA	m
<g/>
)	)	kIx)	)
Při	při	k7c6	při
trhovci	trhovec	k1gMnSc6	trhovec
(	(	kIx(	(
<g/>
506	[number]	k4	506
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
