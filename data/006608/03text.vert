<s>
Downův	Downův	k2eAgInSc1d1	Downův
syndrom	syndrom	k1gInSc1	syndrom
je	být	k5eAaImIp3nS	být
geneticky	geneticky	k6eAd1	geneticky
podmíněné	podmíněný	k2eAgNnSc1d1	podmíněné
onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobené	způsobený	k2eAgNnSc1d1	způsobené
poruchou	porucha	k1gFnSc7	porucha
21	[number]	k4	21
<g/>
.	.	kIx.	.
chromozomu	chromozom	k1gInSc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
nadbytečná	nadbytečný	k2eAgFnSc1d1	nadbytečná
kopie	kopie	k1gFnSc1	kopie
jednoho	jeden	k4xCgInSc2	jeden
chromozomu	chromozom	k1gInSc2	chromozom
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
trisomie	trisomie	k1gFnSc1	trisomie
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemocný	mocný	k2eNgMnSc1d1	nemocný
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
47	[number]	k4	47
chromozomů	chromozom	k1gInPc2	chromozom
(	(	kIx(	(
<g/>
22	[number]	k4	22
párů	pár	k1gInPc2	pár
a	a	k8xC	a
1	[number]	k4	1
trojici	trojice	k1gFnSc4	trojice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
podstata	podstata	k1gFnSc1	podstata
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
translokace	translokace	k1gFnSc1	translokace
(	(	kIx(	(
<g/>
připojení	připojení	k1gNnSc1	připojení
<g/>
)	)	kIx)	)
chromozomu	chromozom	k1gInSc2	chromozom
21	[number]	k4	21
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
chromozom	chromozom	k1gInSc4	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
takový	takový	k3xDgMnSc1	takový
nemocný	nemocný	k1gMnSc1	nemocný
má	mít	k5eAaImIp3nS	mít
fakticky	fakticky	k6eAd1	fakticky
tři	tři	k4xCgFnPc4	tři
kopie	kopie	k1gFnPc4	kopie
chromozomu	chromozom	k1gInSc2	chromozom
21	[number]	k4	21
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
46	[number]	k4	46
samostatných	samostatný	k2eAgInPc2d1	samostatný
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Downův	Downův	k2eAgInSc1d1	Downův
syndrom	syndrom	k1gInSc1	syndrom
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
chromozomech	chromozom	k1gInPc6	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Nondisjunkce	Nondisjunkce	k1gFnSc1	Nondisjunkce
(	(	kIx(	(
<g/>
prostá	prostý	k2eAgFnSc1d1	prostá
trisomie	trisomie	k1gFnSc1	trisomie
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
nejběžnější	běžný	k2eAgFnSc4d3	nejběžnější
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
93	[number]	k4	93
%	%	kIx~	%
postižených	postižený	k1gMnPc2	postižený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
všech	všecek	k3xTgFnPc2	všecek
buněk	buňka	k1gFnPc2	buňka
nemocného	nemocný	k1gMnSc2	nemocný
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgFnPc4d1	přítomna
tři	tři	k4xCgFnPc4	tři
samostatné	samostatný	k2eAgFnPc4d1	samostatná
kopie	kopie	k1gFnPc4	kopie
chromozomu	chromozom	k1gInSc2	chromozom
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
není	být	k5eNaImIp3nS	být
jednoduše	jednoduše	k6eAd1	jednoduše
dědičná	dědičný	k2eAgFnSc1d1	dědičná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
u	u	k7c2	u
postižených	postižený	k1gMnPc2	postižený
vznikají	vznikat	k5eAaImIp3nP	vznikat
gamety	gameta	k1gFnPc4	gameta
s	s	k7c7	s
normálním	normální	k2eAgInSc7d1	normální
počtem	počet	k1gInSc7	počet
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Translokace	translokace	k1gFnSc1	translokace
představuje	představovat	k5eAaImIp3nS	představovat
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
přítomna	přítomen	k2eAgFnSc1d1	přítomna
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
u	u	k7c2	u
čtyř	čtyři	k4xCgNnPc2	čtyři
procent	procento	k1gNnPc2	procento
nemocných	nemocná	k1gFnPc2	nemocná
<g/>
;	;	kIx,	;
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zděděnou	zděděný	k2eAgFnSc4d1	zděděná
poruchu	porucha	k1gFnSc4	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Translokační	translokační	k2eAgFnSc1d1	translokační
forma	forma	k1gFnSc1	forma
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chromozom	chromozom	k1gInSc1	chromozom
21	[number]	k4	21
má	mít	k5eAaImIp3nS	mít
centromeru	centromera	k1gFnSc4	centromera
periferně	periferně	k6eAd1	periferně
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
akrocentrický	akrocentrický	k2eAgInSc1d1	akrocentrický
chromozom	chromozom	k1gInSc1	chromozom
<g/>
)	)	kIx)	)
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
dva	dva	k4xCgInPc4	dva
chromozomy	chromozom	k1gInPc4	chromozom
21	[number]	k4	21
fúzovat	fúzovat	k5eAaBmF	fúzovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
chovat	chovat	k5eAaImF	chovat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
chromozom	chromozom	k1gInSc4	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
fúzního	fúzní	k2eAgInSc2d1	fúzní
chromozomu	chromozom	k1gInSc2	chromozom
21-21	[number]	k4	21-21
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
k	k	k7c3	k
fúzi	fúze	k1gFnSc3	fúze
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
akrocentrickými	akrocentrický	k2eAgInPc7d1	akrocentrický
chromozomy	chromozom	k1gInPc7	chromozom
<g/>
,	,	kIx,	,
např.	např.	kA	např.
14	[number]	k4	14
nebo	nebo	k8xC	nebo
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Translokace	translokace	k1gFnSc1	translokace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
balancovaná	balancovaný	k2eAgFnSc1d1	balancovaná
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
postižený	postižený	k2eAgMnSc1d1	postižený
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
karyotypu	karyotyp	k1gInSc6	karyotyp
pouze	pouze	k6eAd1	pouze
45	[number]	k4	45
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInSc1	jeho
chromozom	chromozom	k1gInSc1	chromozom
21	[number]	k4	21
je	být	k5eAaImIp3nS	být
zdvojený	zdvojený	k2eAgInSc1d1	zdvojený
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgMnSc1	takový
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
fenotypově	fenotypově	k6eAd1	fenotypově
zdravý	zdravý	k2eAgMnSc1d1	zdravý
<g/>
,	,	kIx,	,
netrpí	trpět	k5eNaImIp3nP	trpět
Downovým	Downův	k2eAgInSc7d1	Downův
syndromem	syndrom	k1gInSc7	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
gamety	gameta	k1gFnPc1	gameta
ale	ale	k9	ale
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
genovou	genový	k2eAgFnSc4d1	genová
dávku	dávka	k1gFnSc4	dávka
chromozomu	chromozom	k1gInSc2	chromozom
21	[number]	k4	21
a	a	k8xC	a
tedy	tedy	k9	tedy
všichni	všechen	k3xTgMnPc1	všechen
potomci	potomek	k1gMnPc1	potomek
takového	takový	k3xDgInSc2	takový
člověka	člověk	k1gMnSc4	člověk
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
Downův	Downův	k2eAgInSc4d1	Downův
syndrom	syndrom	k1gInSc4	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Mozaika	mozaika	k1gFnSc1	mozaika
je	být	k5eAaImIp3nS	být
mírnější	mírný	k2eAgFnSc1d2	mírnější
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
postihuje	postihovat	k5eAaImIp3nS	postihovat
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
%	%	kIx~	%
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Mozaika	mozaika	k1gFnSc1	mozaika
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trizomické	trizomický	k2eAgInPc1d1	trizomický
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc1	některý
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Tíže	tíže	k1gFnSc1	tíže
projevů	projev	k1gInPc2	projev
pak	pak	k6eAd1	pak
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
podílu	podíl	k1gInSc6	podíl
postižených	postižený	k2eAgFnPc2d1	postižená
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgNnSc4d1	časté
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
jednoho	jeden	k4xCgNnSc2	jeden
narozeného	narozený	k2eAgNnSc2d1	narozené
dítěte	dítě	k1gNnSc2	dítě
ze	z	k7c2	z
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
<g/>
,	,	kIx,	,
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
1500	[number]	k4	1500
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vznik	vznik	k1gInSc1	vznik
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
stáří	stáří	k1gNnSc4	stáří
matky	matka	k1gFnSc2	matka
–	–	k?	–
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
matka	matka	k1gFnSc1	matka
starší	starý	k2eAgFnSc1d2	starší
45	[number]	k4	45
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
Downovým	Downův	k2eAgInSc7d1	Downův
syndromem	syndrom	k1gInSc7	syndrom
trpí	trpět	k5eAaImIp3nS	trpět
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
malformacemi	malformace	k1gFnPc7	malformace
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
duševní	duševní	k2eAgInSc1d1	duševní
vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
zpomalený	zpomalený	k2eAgInSc1d1	zpomalený
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
výskytu	výskyt	k1gInSc2	výskyt
některých	některý	k3yIgFnPc2	některý
nemocí	nemoc	k1gFnPc2	nemoc
než	než	k8xS	než
běžná	běžný	k2eAgFnSc1d1	běžná
populace	populace	k1gFnSc1	populace
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgInPc1d1	následující
<g/>
:	:	kIx,	:
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
zploštěná	zploštěný	k2eAgFnSc1d1	zploštěná
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
vyvolávající	vyvolávající	k2eAgInSc4d1	vyvolávající
dojem	dojem	k1gInSc4	dojem
neobvykle	obvykle	k6eNd1	obvykle
kulatého	kulatý	k2eAgInSc2d1	kulatý
obličeje	obličej	k1gInSc2	obličej
zploštělá	zploštělý	k2eAgFnSc1d1	zploštělá
tvář	tvář	k1gFnSc1	tvář
<g/>
,	,	kIx,	,
nevýrazné	výrazný	k2eNgInPc4d1	nevýrazný
rysy	rys	k1gInPc4	rys
šikmý	šikmý	k2eAgInSc4d1	šikmý
tvar	tvar	k1gInSc4	tvar
oči	oko	k1gNnPc4	oko
způsobený	způsobený	k2eAgInSc1d1	způsobený
úzkými	úzký	k2eAgInPc7d1	úzký
očními	oční	k2eAgInPc7d1	oční
víčky	víčko	k1gNnPc7	víčko
a	a	k8xC	a
kožní	kožní	k2eAgFnSc7d1	kožní
řasou	řasa	k1gFnSc7	řasa
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
koutku	koutek	k1gInSc6	koutek
oka	oko	k1gNnSc2	oko
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
krk	krk	k1gInSc4	krk
malá	malý	k2eAgNnPc4d1	malé
ústa	ústa	k1gNnPc4	ústa
větší	veliký	k2eAgInSc4d2	veliký
jazyk	jazyk	k1gInSc4	jazyk
krátké	krátká	k1gFnSc2	krátká
a	a	k8xC	a
široké	široký	k2eAgFnSc6d1	široká
ruce	ruka	k1gFnSc6	ruka
krátké	krátký	k2eAgFnSc2d1	krátká
<g />
.	.	kIx.	.
</s>
<s>
prsty	prst	k1gInPc4	prst
velká	velký	k2eAgFnSc1d1	velká
mezera	mezera	k1gFnSc1	mezera
mezi	mezi	k7c7	mezi
palcem	palec	k1gInSc7	palec
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
a	a	k8xC	a
ostatními	ostatní	k2eAgInPc7d1	ostatní
prsty	prst	k1gInPc7	prst
nepřerušená	přerušený	k2eNgFnSc1d1	nepřerušená
příčná	příčný	k2eAgFnSc1d1	příčná
rýha	rýha	k1gFnSc1	rýha
na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
opičí	opičí	k2eAgFnSc1d1	opičí
rýha	rýha	k1gFnSc1	rýha
Brushfieldovy	Brushfieldův	k2eAgFnSc2d1	Brushfieldův
skvrny	skvrna	k1gFnSc2	skvrna
-	-	kIx~	-
malé	malý	k2eAgFnSc2d1	malá
bílé	bílý	k2eAgFnSc2d1	bílá
až	až	k8xS	až
nažloutlé	nažloutlý	k2eAgFnSc2d1	nažloutlá
tečky	tečka	k1gFnSc2	tečka
na	na	k7c6	na
krajích	kraj	k1gInPc6	kraj
duhovky	duhovka	k1gFnSc2	duhovka
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
35-70	[number]	k4	35-70
%	%	kIx~	%
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
,	,	kIx,	,
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
často	často	k6eAd1	často
vymizí	vymizet	k5eAaPmIp3nS	vymizet
mentální	mentální	k2eAgFnSc1d1	mentální
retardace	retardace	k1gFnSc1	retardace
<g/>
,	,	kIx,	,
IQ	iq	kA	iq
nejčastěji	často	k6eAd3	často
kolem	kolem	k7c2	kolem
50-70	[number]	k4	50-70
porucha	porucha	k1gFnSc1	porucha
motoriky	motorik	k1gMnPc7	motorik
snížená	snížený	k2eAgFnSc1d1	snížená
plodnost	plodnost	k1gFnSc1	plodnost
vrozené	vrozený	k2eAgFnSc2d1	vrozená
srdeční	srdeční	k2eAgFnSc2d1	srdeční
vady	vada	k1gFnSc2	vada
vývojové	vývojový	k2eAgFnSc2d1	vývojová
anomálie	anomálie	k1gFnSc2	anomálie
v	v	k7c6	v
trávicím	trávicí	k2eAgInSc6d1	trávicí
traktu	trakt	k1gInSc6	trakt
možnost	možnost	k1gFnSc4	možnost
vzniku	vznik	k1gInSc2	vznik
šedého	šedý	k2eAgInSc2d1	šedý
zákalu	zákal	k1gInSc2	zákal
<g/>
,	,	kIx,	,
keratokonu	keratokonus	k1gInSc2	keratokonus
vyšší	vysoký	k2eAgFnSc1d2	vyšší
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
vzniku	vznik	k1gInSc2	vznik
leukémie	leukémie	k1gFnSc2	leukémie
narušená	narušený	k2eAgFnSc1d1	narušená
funkce	funkce	k1gFnSc1	funkce
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
snížená	snížený	k2eAgFnSc1d1	snížená
imunita	imunita	k1gFnSc1	imunita
Děti	dítě	k1gFnPc1	dítě
trpí	trpět	k5eAaImIp3nS	trpět
celkově	celkově	k6eAd1	celkově
sníženým	snížený	k2eAgInSc7d1	snížený
svalovým	svalový	k2eAgInSc7d1	svalový
tonusem	tonus	k1gInSc7	tonus
(	(	kIx(	(
<g/>
hypotonií	hypotonie	k1gFnPc2	hypotonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůst	vzrůst	k1gInSc1	vzrůst
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
mívají	mívat	k5eAaImIp3nP	mívat
kolem	kolem	k7c2	kolem
147	[number]	k4	147
<g/>
–	–	k?	–
<g/>
162	[number]	k4	162
cm	cm	kA	cm
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
asi	asi	k9	asi
135	[number]	k4	135
<g/>
–	–	k?	–
<g/>
155	[number]	k4	155
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
epileptické	epileptický	k2eAgInPc4d1	epileptický
záchvaty	záchvat	k1gInPc4	záchvat
<g/>
,	,	kIx,	,
postižení	postižený	k2eAgMnPc1d1	postižený
jsou	být	k5eAaImIp3nP	být
náchylnější	náchylný	k2eAgFnPc1d2	náchylnější
ke	k	k7c3	k
kardiovaskulárním	kardiovaskulární	k2eAgNnPc3d1	kardiovaskulární
onemocněním	onemocnění	k1gNnPc3	onemocnění
a	a	k8xC	a
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
×	×	k?	×
častěji	často	k6eAd2	často
postihuje	postihovat	k5eAaImIp3nS	postihovat
Alzheimerova	Alzheimerův	k2eAgFnSc1d1	Alzheimerova
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
vzdělání	vzdělání	k1gNnSc1	vzdělání
většinou	většinou	k6eAd1	většinou
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
praktické	praktický	k2eAgNnSc4d1	praktické
<g/>
.	.	kIx.	.
</s>
<s>
Downův	Downův	k2eAgInSc1d1	Downův
syndrom	syndrom	k1gInSc1	syndrom
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
možné	možný	k2eAgNnSc1d1	možné
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
pouze	pouze	k6eAd1	pouze
vyšetřením	vyšetření	k1gNnSc7	vyšetření
vzorku	vzorek	k1gInSc2	vzorek
placenty	placenta	k1gFnSc2	placenta
(	(	kIx(	(
<g/>
biopsie	biopsie	k1gFnSc1	biopsie
choria-	choria-	k?	choria-
CVS	CVS	kA	CVS
<g/>
)	)	kIx)	)
-	-	kIx~	-
či	či	k8xC	či
vody	voda	k1gFnSc2	voda
plodové	plodový	k2eAgInPc1d1	plodový
-	-	kIx~	-
amniocentéza	amniocentéza	k1gFnSc1	amniocentéza
<g/>
.	.	kIx.	.
</s>
<s>
CVS	CVS	kA	CVS
i	i	k8xC	i
amniocentéza	amniocentéza	k1gFnSc1	amniocentéza
mají	mít	k5eAaImIp3nP	mít
bohužel	bohužel	k9	bohužel
riziko	riziko	k1gNnSc4	riziko
ztráty	ztráta	k1gFnSc2	ztráta
těhotenství	těhotenství	k1gNnSc2	těhotenství
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
<g/>
:	:	kIx,	:
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
amniocentézy	amniocentéza	k1gFnSc2	amniocentéza
a	a	k8xC	a
CVS	CVS	kA	CVS
ve	v	k7c6	v
zkušených	zkušený	k2eAgFnPc6d1	zkušená
rukou	ruka	k1gFnPc6	ruka
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tyto	tento	k3xDgInPc4	tento
výkony	výkon	k1gInPc4	výkon
provádí	provádět	k5eAaImIp3nS	provádět
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
zkušenost	zkušenost	k1gFnSc4	zkušenost
s	s	k7c7	s
provedením	provedení	k1gNnSc7	provedení
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
výkonů	výkon	k1gInPc2	výkon
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
a	a	k8xC	a
provádět	provádět	k5eAaImF	provádět
alespoň	alespoň	k9	alespoň
100	[number]	k4	100
výkonů	výkon	k1gInPc2	výkon
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
2	[number]	k4	2
týdně	týdně	k6eAd1	týdně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgMnS	udržet
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
erudici	erudice	k1gFnSc4	erudice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stanovení	stanovení	k1gNnSc1	stanovení
rizika	riziko	k1gNnSc2	riziko
výskytu	výskyt	k1gInSc2	výskyt
Downova	Downův	k2eAgInSc2d1	Downův
syndromu	syndrom	k1gInSc2	syndrom
analýzou	analýza	k1gFnSc7	analýza
DNA	dno	k1gNnSc2	dno
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
matky	matka	k1gFnSc2	matka
testem	test	k1gInSc7	test
MaterniT	maternita	k1gFnPc2	maternita
<g/>
21	[number]	k4	21
americké	americký	k2eAgFnSc2d1	americká
firmy	firma	k1gFnSc2	firma
Sequenom	Sequenom	k1gInSc1	Sequenom
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
nabízí	nabízet	k5eAaImIp3nS	nabízet
prenatální	prenatální	k2eAgInSc4d1	prenatální
neinvazivní	invazivní	k2eNgInSc4d1	neinvazivní
test	test	k1gInSc4	test
Downova	Downův	k2eAgInSc2d1	Downův
syndromu	syndrom	k1gInSc2	syndrom
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
častých	častý	k2eAgFnPc2d1	častá
chromozomálních	chromozomální	k2eAgFnPc2d1	chromozomální
vad	vada	k1gFnPc2	vada
(	(	kIx(	(
<g/>
trizomie	trizomie	k1gFnSc1	trizomie
chromozomů	chromozom	k1gInPc2	chromozom
13	[number]	k4	13
a	a	k8xC	a
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
zkratky	zkratka	k1gFnSc2	zkratka
NIPD	NIPD	kA	NIPD
<g/>
,	,	kIx,	,
NIPT	NIPT	kA	NIPT
<g/>
))	))	k?	))
další	další	k2eAgFnSc2d1	další
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Ariosa	arioso	k1gNnSc2	arioso
<g/>
,	,	kIx,	,
Verinata	Verinata	k1gFnSc1	Verinata
<g/>
,	,	kIx,	,
Natera	Natera	k1gFnSc1	Natera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neinvazivní	Neinvazivnět	k5eAaPmIp3nS	Neinvazivnět
prenatální	prenatální	k2eAgInSc4d1	prenatální
test	test	k1gInSc4	test
častých	častý	k2eAgFnPc2d1	častá
trisomií	trisomie	k1gFnPc2	trisomie
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
sekvenaci	sekvenace	k1gFnSc6	sekvenace
(	(	kIx(	(
<g/>
zjištění	zjištění	k1gNnSc1	zjištění
sledu	sled	k1gInSc2	sled
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přiřazení	přiřazení	k1gNnSc1	přiřazení
úseků	úsek	k1gInPc2	úsek
volné	volný	k2eAgFnSc2d1	volná
DNA	DNA	kA	DNA
v	v	k7c6	v
plasmě	plasma	k1gFnSc6	plasma
matky	matka	k1gFnSc2	matka
ke	k	k7c3	k
sledovaným	sledovaný	k2eAgInPc3d1	sledovaný
chromozomům	chromozom	k1gInPc3	chromozom
a	a	k8xC	a
statistickém	statistický	k2eAgNnSc6d1	statistické
porovnání	porovnání	k1gNnSc6	porovnání
jejich	jejich	k3xOp3gFnSc2	jejich
četnosti	četnost	k1gFnSc2	četnost
<g/>
.	.	kIx.	.
</s>
<s>
Neinvazivní	Neinvazivní	k2eAgInSc1d1	Neinvazivní
test	test	k1gInSc1	test
častých	častý	k2eAgNnPc2d1	časté
aneuplodií	aneuplodium	k1gNnPc2	aneuplodium
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
screeningové	screeningový	k2eAgNnSc4d1	screeningové
(	(	kIx(	(
<g/>
vyhledávací	vyhledávací	k2eAgNnSc4d1	vyhledávací
<g/>
)	)	kIx)	)
vyšetření	vyšetření	k1gNnSc4	vyšetření
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
citlivostí	citlivost	k1gFnSc7	citlivost
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99	[number]	k4	99
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc7d1	nízká
falešnou	falešný	k2eAgFnSc7d1	falešná
pozitivitou	pozitivita	k1gFnSc7	pozitivita
(	(	kIx(	(
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
parametry	parametr	k1gInPc1	parametr
podobné	podobný	k2eAgInPc1d1	podobný
klasickému	klasický	k2eAgNnSc3d1	klasické
vyšetření	vyšetření	k1gNnSc3	vyšetření
plodové	plodový	k2eAgFnSc2d1	plodová
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
choria	chorium	k1gNnSc2	chorium
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
výsledku	výsledek	k1gInSc2	výsledek
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
klasické	klasický	k2eAgNnSc4d1	klasické
vyšetření	vyšetření	k1gNnSc4	vyšetření
choria	chorium	k1gNnSc2	chorium
nebo	nebo	k8xC	nebo
plodové	plodový	k2eAgFnSc2d1	plodová
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
tohoto	tento	k3xDgNnSc2	tento
screeningového	screeningový	k2eAgNnSc2d1	screeningové
vyšetření	vyšetření	k1gNnSc2	vyšetření
již	již	k6eAd1	již
klesla	klesnout	k5eAaPmAgFnS	klesnout
v	v	k7c6	v
ČR	ČR	kA	ČR
pod	pod	k7c4	pod
17	[number]	k4	17
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
a	a	k8xC	a
cílová	cílový	k2eAgFnSc1d1	cílová
cena	cena	k1gFnSc1	cena
pro	pro	k7c4	pro
pacienta	pacient	k1gMnSc4	pacient
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
jakou	jaký	k3yRgFnSc4	jaký
část	část	k1gFnSc4	část
hradí	hradit	k5eAaImIp3nP	hradit
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
pojišťovny	pojišťovna	k1gFnPc1	pojišťovna
(	(	kIx(	(
<g/>
V	v	k7c6	v
ČR	ČR	kA	ČR
zatím	zatím	k6eAd1	zatím
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NIPT	NIPT	kA	NIPT
(	(	kIx(	(
<g/>
non-invasive	nonnvasivat	k5eAaPmIp3nS	non-invasivat
prenatal	prenatal	k1gMnSc1	prenatal
testing	testing	k1gInSc1	testing
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
ceně	cena	k1gFnSc3	cena
zatím	zatím	k6eAd1	zatím
asi	asi	k9	asi
nestane	stanout	k5eNaPmIp3nS	stanout
univerzálním	univerzální	k2eAgInSc7d1	univerzální
screningovým	screningový	k2eAgInSc7d1	screningový
testem	test	k1gInSc7	test
Downova	Downův	k2eAgInSc2d1	Downův
syndromu	syndrom	k1gInSc2	syndrom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určitá	určitý	k2eAgFnSc1d1	určitá
část	část	k1gFnSc1	část
těhotných	těhotná	k1gFnPc2	těhotná
s	s	k7c7	s
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
současným	současný	k2eAgInSc7d1	současný
screeningovým	screeningový	k2eAgInSc7d1	screeningový
testem	test	k1gInSc7	test
(	(	kIx(	(
<g/>
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
test	test	k1gInSc1	test
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
trimestru	trimestr	k1gInSc6	trimestr
nebo	nebo	k8xC	nebo
triple	tripl	k1gInSc5	tripl
test	test	k1gInSc1	test
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
trimestru	trimestr	k1gInSc6	trimestr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
oplození	oplození	k1gNnSc6	oplození
metodami	metoda	k1gFnPc7	metoda
asistované	asistovaný	k2eAgFnSc2d1	asistovaná
reprodukce	reprodukce	k1gFnSc2	reprodukce
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vyšším	vysoký	k2eAgInSc6d2	vyšší
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
NIPT	NIPT	kA	NIPT
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
jako	jako	k9	jako
pro	pro	k7c4	pro
alternativu	alternativa	k1gFnSc4	alternativa
přímého	přímý	k2eAgNnSc2d1	přímé
vyšetření	vyšetření	k1gNnSc2	vyšetření
buněk	buňka	k1gFnPc2	buňka
plodu	plod	k1gInSc2	plod
získaných	získaný	k2eAgInPc2d1	získaný
amniocentesou	amniocentesa	k1gFnSc7	amniocentesa
nebo	nebo	k8xC	nebo
CVS	CVS	kA	CVS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
test	test	k1gInSc1	test
Harmony	Harmona	k1gFnSc2	Harmona
Materni	Materni	k1gFnSc2	Materni
<g/>
21	[number]	k4	21
a	a	k8xC	a
PRENASCAN	PRENASCAN	kA	PRENASCAN
<g/>
.	.	kIx.	.
</s>
<s>
Porovnání	porovnání	k1gNnSc1	porovnání
nabídky	nabídka	k1gFnSc2	nabídka
testů	test	k1gInPc2	test
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
Porovnání	porovnání	k1gNnSc6	porovnání
nabídky	nabídka	k1gFnSc2	nabídka
neinvazivních	invazivní	k2eNgInPc2d1	neinvazivní
prenatálních	prenatální	k2eAgInPc2d1	prenatální
testů	test	k1gInPc2	test
Nejstarší	starý	k2eAgInSc1d3	nejstarší
archeologický	archeologický	k2eAgInSc1d1	archeologický
doklad	doklad	k1gInSc1	doklad
výskytu	výskyt	k1gInSc2	výskyt
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
nález	nález	k1gInSc1	nález
saské	saský	k2eAgFnSc2d1	saská
lebky	lebka	k1gFnSc2	lebka
z	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
i	i	k9	i
doklady	doklad	k1gInPc1	doklad
plastik	plastika	k1gFnPc2	plastika
olmécké	olmécký	k2eAgFnSc2d1	olmécká
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
jedince	jedinec	k1gMnPc4	jedinec
postižené	postižený	k2eAgMnPc4d1	postižený
Downovým	Downův	k2eAgInSc7d1	Downův
syndromem	syndrom	k1gInSc7	syndrom
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
i	i	k9	i
dítě	dítě	k1gNnSc4	dítě
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Panna	Panna	k1gFnSc1	Panna
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
Andrea	Andrea	k1gFnSc1	Andrea
Mantegny	Mantegno	k1gNnPc7	Mantegno
(	(	kIx(	(
<g/>
1430	[number]	k4	1430
<g/>
-	-	kIx~	-
<g/>
1506	[number]	k4	1506
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c6	na
portrétu	portrét	k1gInSc6	portrét
Joshuy	Joshua	k1gFnSc2	Joshua
Reynoldse	Reynoldse	k1gFnSc2	Reynoldse
Portrét	portrét	k1gInSc1	portrét
lady	lady	k1gFnSc2	lady
Cockburnové	Cockburnová	k1gFnSc2	Cockburnová
s	s	k7c7	s
jejími	její	k3xOp3gMnPc7	její
třemi	tři	k4xCgMnPc7	tři
staršími	starý	k2eAgMnPc7d2	starší
syny	syn	k1gMnPc7	syn
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
informace	informace	k1gFnSc1	informace
dokumentující	dokumentující	k2eAgNnSc4d1	dokumentující
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
Downovým	Downův	k2eAgInSc7d1	Downův
syndromem	syndrom	k1gInSc7	syndrom
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Étienne	Étienn	k1gInSc5	Étienn
Esquirola	Esquirola	k1gFnSc1	Esquirola
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
kretenismus	kretenismus	k1gInSc4	kretenismus
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc4	nemoc
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
John	John	k1gMnSc1	John
Langdon	Langdon	k1gMnSc1	Langdon
Down	Down	k1gMnSc1	Down
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
podstatu	podstata	k1gFnSc4	podstata
-	-	kIx~	-
trisomii	trisomie	k1gFnSc6	trisomie
21	[number]	k4	21
<g/>
.	.	kIx.	.
chromozómu	chromozóm	k1gInSc2	chromozóm
–	–	k?	–
objevil	objevit	k5eAaPmAgMnS	objevit
a	a	k8xC	a
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
genetik	genetik	k1gMnSc1	genetik
Jérôme	Jérôm	k1gMnSc5	Jérôm
Lejeune	Lejeun	k1gMnSc5	Lejeun
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
