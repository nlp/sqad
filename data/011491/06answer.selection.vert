<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Nietzsche	Nietzsche	k1gFnSc1	Nietzsche
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1844	[number]	k4	1844
Röcken	Röckna	k1gFnPc2	Röckna
u	u	k7c2	u
Lützenu	Lützen	k2eAgFnSc4d1	Lützen
poblíž	poblíž	k7c2	poblíž
Lipska	Lipsko	k1gNnSc2	Lipsko
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1900	[number]	k4	1900
Výmar	Výmar	k1gInSc1	Výmar
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
klasický	klasický	k2eAgMnSc1d1	klasický
filolog	filolog	k1gMnSc1	filolog
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
kompozice	kompozice	k1gFnPc1	kompozice
byly	být	k5eAaImAgFnP	být
zastíněny	zastínit	k5eAaPmNgFnP	zastínit
jeho	jeho	k3xOp3gInPc7	jeho
pozdějšími	pozdní	k2eAgInPc7d2	pozdější
filosofickými	filosofický	k2eAgInPc7d1	filosofický
spisy	spis	k1gInPc7	spis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
