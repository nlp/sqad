<s>
Eva	Eva	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
české	český	k2eAgFnSc6d1
herečce	herečka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Eva	Eva	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
(	(	kIx(
<g/>
zpěvačka	zpěvačka	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
českou	český	k2eAgFnSc4d1
jazzovou	jazzový	k2eAgFnSc4d1
zpěvačku	zpěvačka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Eva	Eva	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
Eva	Eva	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Cora	Cora	k1gFnSc1
<g/>
:	:	kIx,
<g/>
“	“	k?
v	v	k7c6
Schnitzlerově	Schnitzlerův	k2eAgFnSc6d1
hře	hra	k1gFnSc6
„	„	k?
<g/>
Anatol	Anatola	k1gFnPc2
<g/>
“	“	k?
Narození	narození	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1907	#num#	k4
Praha	Praha	k1gFnSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1992	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
84	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Stivín	Stivín	k1gMnSc1
a	a	k8xC
Zuzana	Zuzana	k1gFnSc1
Stivínová	Stivínová	k1gFnSc1
starší	starý	k2eAgMnPc4d2
Rodiče	rodič	k1gMnPc4
</s>
<s>
Milan	Milan	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
bratr	bratr	k1gMnSc1
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Stivínová	Stivínový	k2eAgFnSc1d1
mladší	mladý	k2eAgFnSc1d2
vnučka	vnučka	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eva	Eva	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1907	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1992	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
česká	český	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
hudebníka	hudebník	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
Stivína	Stivín	k1gMnSc2
a	a	k8xC
herečky	herečka	k1gFnSc2
Zuzany	Zuzana	k1gFnSc2
Stivínové	Stivínová	k2eAgFnSc1d1
starší	starší	k1gMnSc1
a	a	k8xC
babička	babička	k1gFnSc1
Zuzany	Zuzana	k1gFnSc2
Stivínové	Stivínová	k2eAgFnSc1d1
mladší	mladý	k2eAgFnSc3d2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
umělecké	umělecký	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
jejím	její	k3xOp3gMnSc7
otcem	otec	k1gMnSc7
byl	být	k5eAaImAgMnS
režisér	režisér	k1gMnSc1
a	a	k8xC
divadelní	divadelní	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
Milan	Milan	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
hercem	herec	k1gMnSc7
byl	být	k5eAaImAgMnS
i	i	k9
její	její	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1926	#num#	k4
až	až	k9
1929	#num#	k4
studovala	studovat	k5eAaImAgFnS
na	na	k7c6
dramatickém	dramatický	k2eAgNnSc6d1
oddělení	oddělení	k1gNnSc6
Státní	státní	k2eAgFnSc2d1
konzervatoře	konzervatoř	k1gFnSc2
hudby	hudba	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
si	se	k3xPyFc3
zahrála	zahrát	k5eAaPmAgFnS
ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
stovce	stovka	k1gFnSc3
českých	český	k2eAgInPc2d1
filmů	film	k1gInPc2
jakož	jakož	k8xC
i	i	k9
v	v	k7c6
mnoha	mnoho	k4c6
televizních	televizní	k2eAgInPc6d1
pořadech	pořad	k1gInPc6
a	a	k8xC
inscenacích	inscenace	k1gFnPc6
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
malé	malý	k2eAgFnPc4d1
epizodní	epizodní	k2eAgFnPc4d1
role	role	k1gFnPc4
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
často	často	k6eAd1
hrála	hrát	k5eAaImAgFnS
prosté	prostý	k2eAgFnPc4d1
ženy	žena	k1gFnPc4
z	z	k7c2
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
divadle	divadlo	k1gNnSc6
působila	působit	k5eAaImAgFnS
v	v	k7c6
letech	let	k1gInPc6
1929	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
v	v	k7c6
Divadle	divadlo	k1gNnSc6
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1950	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
a	a	k8xC
pak	pak	k6eAd1
ještě	ještě	k9
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
v	v	k7c6
Městských	městský	k2eAgNnPc6d1
divadlech	divadlo	k1gNnPc6
pražských	pražský	k2eAgNnPc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zahrála	zahrát	k5eAaPmAgFnS
si	se	k3xPyFc3
také	také	k9
roli	role	k1gFnSc4
správcové	správcová	k1gFnSc2
ve	v	k7c6
známé	známý	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc3d1
písničce	písnička	k1gFnSc3
(	(	kIx(
<g/>
tj.	tj.	kA
ve	v	k7c6
starém	starý	k2eAgInSc6d1
videoklipu	videoklip	k1gInSc6
<g/>
)	)	kIx)
se	s	k7c7
známou	známý	k2eAgFnSc7d1
písní	píseň	k1gFnSc7
Chtěl	chtít	k5eAaImAgMnS
bych	by	kYmCp1nS
mít	mít	k5eAaImF
kapelu	kapela	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
jejím	její	k3xOp3gInSc6
životě	život	k1gInSc6
její	její	k3xOp3gInSc4
syn	syn	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Stivín	Stivín	k1gMnSc1
natočil	natočit	k5eAaBmAgMnS
střihový	střihový	k2eAgInSc4d1
dokumentární	dokumentární	k2eAgInSc4d1
film	film	k1gInSc4
Máma	máma	k1gFnSc1
a	a	k8xC
já	já	k3xPp1nSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
uvedla	uvést	k5eAaPmAgFnS
i	i	k8xC
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Citát	citát	k1gInSc1
</s>
<s>
„	„	k?
</s>
<s>
S	s	k7c7
Evou	Eva	k1gFnSc7
mne	já	k3xPp1nSc4
spojuje	spojovat	k5eAaImIp3nS
vzpomínka	vzpomínka	k1gFnSc1
na	na	k7c6
Roudnici	Roudnice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1
profesor	profesor	k1gMnSc1
Milan	Milan	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
učil	učit	k5eAaImAgMnS,k5eAaPmAgMnS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
jsem	být	k5eAaImIp1nS
ho	on	k3xPp3gMnSc4
poznal	poznat	k5eAaPmAgMnS
později	pozdě	k6eAd2
jako	jako	k8xS,k8xC
žák	žák	k1gMnSc1
konzervatoře	konzervatoř	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
pedagogů	pedagog	k1gMnPc2
<g/>
...	...	k?
<g/>
Eva	Eva	k1gFnSc1
tedy	tedy	k9
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
divadelní	divadelní	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
jejích	její	k3xOp3gMnPc2
bratrů	bratr	k1gMnPc2
–	–	k?
Miroslav	Miroslav	k1gMnSc1
–	–	k?
hrál	hrát	k5eAaImAgMnS
ve	v	k7c6
Vančurově	Vančurův	k2eAgInSc6d1
filmu	film	k1gInSc6
"	"	kIx"
<g/>
Před	před	k7c7
maturitou	maturita	k1gFnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Svatopluk	Svatopluk	k1gMnSc1
Beneš	Beneš	k1gMnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnosti	farnost	k1gFnSc2
při	při	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
<g/>
Ludmily	Ludmila	k1gFnSc2
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
↑	↑	k?
Svatopluk	Svatopluk	k1gMnSc1
Beneš	Beneš	k1gMnSc1
<g/>
:	:	kIx,
Být	být	k5eAaImF
hercem	herec	k1gMnSc7
<g/>
,	,	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
150	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7023	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
118	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
↑	↑	k?
Vlastimil	Vlastimil	k1gMnSc1
Blažek	Blažek	k1gMnSc1
<g/>
:	:	kIx,
Sborník	sborník	k1gInSc1
na	na	k7c4
paměť	paměť	k1gFnSc4
125	#num#	k4
let	léto	k1gNnPc2
Konservatoře	konservatoř	k1gFnSc2
hudby	hudba	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
515	#num#	k4
↑	↑	k?
Z.	Z.	kA
Sílová	sílový	k2eAgFnSc1d1
<g/>
,	,	kIx,
R.	R.	kA
Hrdinová	Hrdinová	k1gFnSc1
<g/>
,	,	kIx,
A.	A.	kA
Kožíková	Kožíková	k1gFnSc1
<g/>
,	,	kIx,
V.	V.	kA
Mohylová	mohylový	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Divadlo	divadlo	k1gNnSc1
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
1907	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
–	–	k?
Vinohradský	vinohradský	k2eAgInSc4d1
ansámbl	ansámbl	k1gInSc4
<g/>
,	,	kIx,
vydalo	vydat	k5eAaPmAgNnS
Divadlo	divadlo	k1gNnSc1
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
193	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
9604	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Marie	Marie	k1gFnSc1
Valtrová	Valtrová	k1gFnSc1
<g/>
:	:	kIx,
ORNESTINUM	ORNESTINUM	kA
<g/>
,	,	kIx,
Slavná	slavný	k2eAgFnSc1d1
éra	éra	k1gFnSc1
Městských	městský	k2eAgNnPc2d1
divadel	divadlo	k1gNnPc2
pražských	pražský	k2eAgNnPc2d1
<g/>
,	,	kIx,
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
192	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
121	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
↑	↑	k?
Svatopluk	Svatopluk	k1gMnSc1
Beneš	Beneš	k1gMnSc1
<g/>
:	:	kIx,
Být	být	k5eAaImF
hercem	herec	k1gMnSc7
<g/>
,	,	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
90	#num#	k4
<g/>
,	,	kIx,
111	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7023-118-1	80-7023-118-1	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1
Beneš	Beneš	k1gMnSc1
<g/>
:	:	kIx,
Být	být	k5eAaImF
hercem	herec	k1gMnSc7
<g/>
,	,	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
31	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
148	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
,	,	kIx,
150	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7023-118-1	80-7023-118-1	k4
</s>
<s>
B.	B.	kA
Bezouška	Bezouška	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
Pivcová	Pivcová	k1gFnSc1
<g/>
,	,	kIx,
J.	J.	kA
Švehla	Švehla	k1gMnSc1
<g/>
:	:	kIx,
Thespidova	Thespidův	k2eAgFnSc1d1
kára	kára	k1gFnSc1
Jana	Jan	k1gMnSc2
Pivce	Pivce	k1gMnSc2
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
58	#num#	k4
<g/>
,	,	kIx,
201	#num#	k4
</s>
<s>
Eva	Eva	k1gFnSc1
Högerová	Högerová	k1gFnSc1
<g/>
,	,	kIx,
Ljuba	Ljuba	k1gFnSc1
Klosová	Klosová	k1gFnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Justl	Justl	k1gFnSc2
<g/>
:	:	kIx,
Faustovské	faustovský	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
Karla	Karel	k1gMnSc2
Högera	Höger	k1gMnSc2
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
91	#num#	k4
<g/>
,	,	kIx,
110	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-204-0493-7	80-204-0493-7	k4
</s>
<s>
František	František	k1gMnSc1
Kovářík	kovářík	k1gMnSc1
<g/>
:	:	kIx,
Kudy	kudy	k6eAd1
všudy	všudy	k6eAd1
za	za	k7c7
divadlem	divadlo	k1gNnSc7
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
255	#num#	k4
<g/>
,	,	kIx,
258	#num#	k4
</s>
<s>
V.	V.	kA
Müller	Müller	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Padesát	padesát	k4xCc1
let	léto	k1gNnPc2
Městských	městský	k2eAgNnPc2d1
divadel	divadlo	k1gNnPc2
pražských	pražský	k2eAgInPc2d1
1907	#num#	k4
–	–	k?
1957	#num#	k4
<g/>
,	,	kIx,
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
180	#num#	k4
</s>
<s>
Z.	Z.	kA
Sílová	sílový	k2eAgFnSc1d1
<g/>
,	,	kIx,
R.	R.	kA
Hrdinová	Hrdinová	k1gFnSc1
<g/>
,	,	kIx,
A.	A.	kA
Kožíková	Kožíková	k1gFnSc1
<g/>
,	,	kIx,
V.	V.	kA
Mohylová	mohylový	k2eAgFnSc1d1
:	:	kIx,
Divadlo	divadlo	k1gNnSc1
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
1907	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
–	–	k?
Vinohradský	vinohradský	k2eAgInSc4d1
ansámbl	ansámbl	k1gInSc4
<g/>
,	,	kIx,
vydalo	vydat	k5eAaPmAgNnS
Divadlo	divadlo	k1gNnSc1
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
193	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-239-9604-3	978-80-239-9604-3	k4
</s>
<s>
Marie	Marie	k1gFnSc1
Valtrová	Valtrová	k1gFnSc1
<g/>
:	:	kIx,
Kronika	kronika	k1gFnSc1
rodu	rod	k1gInSc2
Hrušínských	Hrušínský	k2eAgMnPc2d1
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
133	#num#	k4
<g/>
,	,	kIx,
177	#num#	k4
<g/>
,	,	kIx,
186	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-207-0485-X	80-207-0485-X	k4
</s>
<s>
Marie	Marie	k1gFnSc1
Valtrová	Valtrová	k1gFnSc1
<g/>
:	:	kIx,
ORNESTINUM	ORNESTINUM	kA
<g/>
,	,	kIx,
Slavná	slavný	k2eAgFnSc1d1
éra	éra	k1gFnSc1
Městských	městský	k2eAgNnPc2d1
divadel	divadlo	k1gNnPc2
pražských	pražský	k2eAgNnPc2d1
<g/>
,	,	kIx,
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
16	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
<g/>
,	,	kIx,
66	#num#	k4
<g/>
,	,	kIx,
135	#num#	k4
<g/>
,	,	kIx,
140	#num#	k4
<g/>
,	,	kIx,
192	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7243-121-8	80-7243-121-8	k4
</s>
<s>
Marie	Marie	k1gFnSc1
Valtrová	Valtrová	k1gFnSc1
–	–	k?
Ota	Ota	k1gMnSc1
Ornest	Ornest	k1gMnSc1
<g/>
:	:	kIx,
Hraje	hrát	k5eAaImIp3nS
váš	váš	k3xOp2gMnSc1
tatínek	tatínek	k1gMnSc1
ještě	ještě	k9
na	na	k7c4
housle	housle	k1gFnPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Primus	primus	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
225	#num#	k4
<g/>
,	,	kIx,
242	#num#	k4
<g/>
,	,	kIx,
256	#num#	k4
<g/>
,	,	kIx,
314	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85625-19-9	80-85625-19-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
k	k	k7c3
tématu	téma	k1gNnSc3
Eva	Eva	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
na	na	k7c4
Obalkyknih	Obalkyknih	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1
Jiřího	Jiří	k1gMnSc2
Stivína	Stivín	k1gMnSc2
na	na	k7c4
svoji	svůj	k3xOyFgFnSc4
maminku	maminka	k1gFnSc4
Evu	Eva	k1gFnSc4
Svobodovou	Svobodová	k1gFnSc4
</s>
<s>
Eva	Eva	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
článek	článek	k1gInSc1
o	o	k7c6
filmu	film	k1gInSc6
Máma	máma	k1gFnSc1
a	a	k8xC
já	já	k3xPp1nSc1
Jiřího	Jiří	k1gMnSc4
Stivína	Stivín	k1gMnSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
20031208003	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1034562258	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5634	#num#	k4
2094	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2011120103	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84230682	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2011120103	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Film	film	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
