<p>
<s>
Taťána	Taťána	k1gFnSc1	Taťána
Golovinová	Golovinová	k1gFnSc1	Golovinová
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Т	Т	k?	Т
Г	Г	k?	Г
<g/>
;	;	kIx,	;
narozená	narozený	k2eAgFnSc1d1	narozená
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
francouzská	francouzský	k2eAgFnSc1d1	francouzská
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
krajanem	krajan	k1gMnSc7	krajan
Richardem	Richard	k1gMnSc7	Richard
Gasquetem	Gasquet	k1gMnSc7	Gasquet
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
smíšenou	smíšený	k2eAgFnSc4d1	smíšená
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
French	French	k1gInSc1	French
Open	Open	k1gNnSc1	Open
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
dvouhry	dvouhra	k1gFnSc2	dvouhra
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
dvouhru	dvouhra	k1gFnSc4	dvouhra
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Profesionální	profesionální	k2eAgFnSc4d1	profesionální
kariéru	kariéra	k1gFnSc4	kariéra
ukončila	ukončit	k5eAaPmAgFnS	ukončit
předčasně	předčasně	k6eAd1	předčasně
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
během	během	k7c2	během
sezóny	sezóna	k1gFnSc2	sezóna
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
zánětlivé	zánětlivý	k2eAgNnSc4d1	zánětlivé
onemocnění	onemocnění	k1gNnSc4	onemocnění
bederní	bederní	k2eAgFnSc2d1	bederní
páteře	páteř	k1gFnSc2	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
aktivní	aktivní	k2eAgFnSc2d1	aktivní
dráhy	dráha	k1gFnSc2	dráha
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
tenisová	tenisový	k2eAgFnSc1d1	tenisová
expertka	expertka	k1gFnSc1	expertka
pro	pro	k7c4	pro
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
veřejnoprávní	veřejnoprávní	k2eAgFnSc4d1	veřejnoprávní
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
Canal	Canal	k1gInSc4	Canal
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
získala	získat	k5eAaPmAgFnS	získat
francouzské	francouzský	k2eAgNnSc4d1	francouzské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
Olgu	Olga	k1gFnSc4	Olga
a	a	k8xC	a
Oxanu	Oxana	k1gFnSc4	Oxana
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
strávila	strávit	k5eAaPmAgFnS	strávit
jako	jako	k9	jako
talentované	talentovaný	k2eAgNnSc4d1	talentované
dítě	dítě	k1gNnSc4	dítě
v	v	k7c6	v
Tenisové	tenisový	k2eAgFnSc6d1	tenisová
akademii	akademie	k1gFnSc6	akademie
Nicka	nicka	k1gFnSc1	nicka
Bollettieriho	Bollettieri	k1gMnSc2	Bollettieri
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ji	on	k3xPp3gFnSc4	on
trénovala	trénovat	k5eAaImAgFnS	trénovat
bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
Mats	Matsa	k1gFnPc2	Matsa
Wilander	Wilandra	k1gFnPc2	Wilandra
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
jejími	její	k3xOp3gMnPc7	její
kouči	kouč	k1gMnPc7	kouč
byli	být	k5eAaImAgMnP	být
Brad	brada	k1gFnPc2	brada
Gilbert	Gilbert	k1gMnSc1	Gilbert
a	a	k8xC	a
Dean	Dean	k1gMnSc1	Dean
Goldfine	Goldfin	k1gMnSc5	Goldfin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Hampsteadu	Hampstead	k1gInSc6	Hampstead
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Samirem	Samir	k1gMnSc7	Samir
Nasrim	Nasrim	k1gInSc4	Nasrim
<g/>
,	,	kIx,	,
fotbalistou	fotbalista	k1gMnSc7	fotbalista
Manchester	Manchester	k1gInSc4	Manchester
City	City	k1gFnSc2	City
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Finálová	finálový	k2eAgNnPc1d1	finálové
utkání	utkání	k1gNnPc1	utkání
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Smíšená	smíšený	k2eAgFnSc1d1	smíšená
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Vítězka	vítězka	k1gFnSc1	vítězka
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Finále	finále	k1gNnSc1	finále
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
WTA	WTA	kA	WTA
Tour	Toura	k1gFnPc2	Toura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dvouhra	dvouhra	k1gFnSc1	dvouhra
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Výhry	výhra	k1gFnPc1	výhra
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Prohry	prohra	k1gFnSc2	prohra
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tatiana	Tatian	k1gMnSc2	Tatian
Golovin	Golovina	k1gFnPc2	Golovina
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Taťána	Taťána	k1gFnSc1	Taťána
Golovinová	Golovinový	k2eAgFnSc1d1	Golovinový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Taťána	Taťána	k1gFnSc1	Taťána
Golovinová	Golovinový	k2eAgFnSc1d1	Golovinový
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Ženské	ženský	k2eAgFnSc2d1	ženská
tenisové	tenisový	k2eAgFnSc2d1	tenisová
asociace	asociace	k1gFnSc2	asociace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Taťána	Taťána	k1gFnSc1	Taťána
Golovinová	Golovinový	k2eAgFnSc1d1	Golovinový
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
tenisové	tenisový	k2eAgFnSc2d1	tenisová
federace	federace	k1gFnSc2	federace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Taťána	Taťána	k1gFnSc1	Taťána
Golovinová	Golovinový	k2eAgFnSc1d1	Golovinový
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
</s>
</p>
