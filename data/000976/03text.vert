<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
956	[number]	k4	956
<g/>
-	-	kIx~	-
<g/>
997	[number]	k4	997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
známý	známý	k2eAgMnSc1d1	známý
spíše	spíše	k9	spíše
pod	pod	k7c7	pod
biřmovacím	biřmovací	k2eAgNnSc7d1	biřmovací
jménem	jméno	k1gNnSc7	jméno
Adalbert	Adalbert	k1gMnSc1	Adalbert
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgInS	vystupovat
proti	proti	k7c3	proti
některým	některý	k3yIgInPc3	některý
jevům	jev	k1gInPc3	jev
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
neslučovaly	slučovat	k5eNaImAgFnP	slučovat
s	s	k7c7	s
církevním	církevní	k2eAgNnSc7d1	církevní
učením	učení	k1gNnSc7	učení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
pohanství	pohanství	k1gNnSc3	pohanství
<g/>
,	,	kIx,	,
obchodování	obchodování	k1gNnPc2	obchodování
s	s	k7c7	s
křesťanskými	křesťanský	k2eAgMnPc7d1	křesťanský
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
kněžskému	kněžský	k2eAgNnSc3d1	kněžské
manželství	manželství	k1gNnSc3	manželství
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
rozšířenému	rozšířený	k2eAgInSc3d1	rozšířený
alkoholismu	alkoholismus	k1gInSc3	alkoholismus
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
domácího	domácí	k2eAgNnSc2d1	domácí
latinského	latinský	k2eAgNnSc2d1	latinské
písemnictví	písemnictví	k1gNnSc2	písemnictví
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
respektoval	respektovat	k5eAaImAgMnS	respektovat
staroslověnskou	staroslověnský	k2eAgFnSc4d1	staroslověnská
kulturní	kulturní	k2eAgFnSc4d1	kulturní
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c2	za
autora	autor	k1gMnSc2	autor
nejstarších	starý	k2eAgFnPc2d3	nejstarší
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
polských	polský	k2eAgFnPc2d1	polská
duchovních	duchovní	k2eAgFnPc2d1	duchovní
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
např.	např.	kA	např.
známé	známý	k2eAgFnPc4d1	známá
písně	píseň	k1gFnPc4	píseň
Hospodine	Hospodin	k1gMnSc5	Hospodin
pomiluj	pomilovat	k5eAaPmRp2nS	pomilovat
ny	ny	k?	ny
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
působení	působení	k1gNnSc1	působení
bylo	být	k5eAaImAgNnS	být
komplikováno	komplikovat	k5eAaBmNgNnS	komplikovat
krizí	krize	k1gFnSc7	krize
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
projevovala	projevovat	k5eAaImAgFnS	projevovat
i	i	k9	i
rostoucími	rostoucí	k2eAgInPc7d1	rostoucí
spory	spor	k1gInPc7	spor
mezi	mezi	k7c7	mezi
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
a	a	k8xC	a
Slavníkovci	Slavníkovec	k1gMnPc7	Slavníkovec
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
pro	pro	k7c4	pro
spory	spor	k1gInPc4	spor
s	s	k7c7	s
knížetem	kníže	k1gNnSc7wR	kníže
opustil	opustit	k5eAaPmAgInS	opustit
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
odchod	odchod	k1gInSc4	odchod
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
vyvražděním	vyvraždění	k1gNnSc7	vyvraždění
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
zachránil	zachránit	k5eAaPmAgInS	zachránit
život	život	k1gInSc1	život
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgInS	ukázat
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
definitivním	definitivní	k2eAgInSc7d1	definitivní
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
když	když	k8xS	když
jako	jako	k8xC	jako
misionář	misionář	k1gMnSc1	misionář
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
území	území	k1gNnSc6	území
pohanských	pohanský	k2eAgMnPc2d1	pohanský
Prusů	Prus	k1gMnPc2	Prus
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
mučedníka	mučedník	k1gMnSc4	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
Slavníka	Slavník	k1gMnSc2	Slavník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rod	rod	k1gInSc1	rod
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Libici	Libice	k1gFnSc6	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
není	být	k5eNaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
955	[number]	k4	955
<g/>
-	-	kIx~	-
<g/>
957	[number]	k4	957
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
5	[number]	k4	5
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
962	[number]	k4	962
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
malého	malý	k2eAgMnSc4d1	malý
chlapce	chlapec	k1gMnSc4	chlapec
biřmoval	biřmovat	k5eAaImAgMnS	biřmovat
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
Libice	Libice	k1gFnPc4	Libice
magdeburský	magdeburský	k2eAgMnSc1d1	magdeburský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Adalbert	Adalbert	k1gMnSc1	Adalbert
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc1	vzdělání
Vojtěchovi	Vojtěch	k1gMnSc3	Vojtěch
zprostředkovali	zprostředkovat	k5eAaPmAgMnP	zprostředkovat
kněží	kněz	k1gMnPc1	kněz
z	z	k7c2	z
Libice	Libice	k1gFnPc1	Libice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
Magdeburku	Magdeburk	k1gInSc6	Magdeburk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
nadaný	nadaný	k2eAgMnSc1d1	nadaný
žák	žák	k1gMnSc1	žák
působil	působit	k5eAaImAgMnS	působit
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
972	[number]	k4	972
<g/>
-	-	kIx~	-
<g/>
981	[number]	k4	981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
studií	studie	k1gFnPc2	studie
získal	získat	k5eAaPmAgInS	získat
první	první	k4xOgInSc1	první
stupeň	stupeň	k1gInSc1	stupeň
vyššího	vysoký	k2eAgNnSc2d2	vyšší
kněžského	kněžský	k2eAgNnSc2d1	kněžské
svěcení	svěcení	k1gNnSc2	svěcení
-	-	kIx~	-
podjáhenství	podjáhenství	k1gNnSc1	podjáhenství
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pomocníkem	pomocník	k1gMnSc7	pomocník
prvního	první	k4xOgMnSc2	první
českého	český	k2eAgMnSc2d1	český
biskupa	biskup	k1gMnSc2	biskup
Dětmara	Dětmar	k1gMnSc2	Dětmar
<g/>
;	;	kIx,	;
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
stal	stát	k5eAaPmAgMnS	stát
druhým	druhý	k4xOgMnSc7	druhý
pražským	pražský	k2eAgMnSc7d1	pražský
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
volbě	volba	k1gFnSc3	volba
došlo	dojít	k5eAaPmAgNnS	dojít
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
982	[number]	k4	982
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c6	na
Levém	levý	k2eAgInSc6d1	levý
Hradci	Hradec	k1gInSc6	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěchova	Vojtěchův	k2eAgFnSc1d1	Vojtěchova
investitura	investitura	k1gFnSc1	investitura
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ve	v	k7c6	v
Veroně	Verona	k1gFnSc6	Verona
císařem	císař	k1gMnSc7	císař
(	(	kIx(	(
<g/>
Otou	Ota	k1gMnSc7	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biskupské	biskupský	k2eAgNnSc1d1	Biskupské
svěcení	svěcení	k1gNnSc1	svěcení
mu	on	k3xPp3gMnSc3	on
poté	poté	k6eAd1	poté
udělil	udělit	k5eAaPmAgMnS	udělit
metropolita	metropolita	k1gMnSc1	metropolita
Willigis	Willigis	k1gFnSc2	Willigis
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
biskup	biskup	k1gMnSc1	biskup
byl	být	k5eAaImAgMnS	být
odhodlaným	odhodlaný	k2eAgMnSc7d1	odhodlaný
šiřitelem	šiřitel	k1gMnSc7	šiřitel
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
převážně	převážně	k6eAd1	převážně
pohanských	pohanský	k2eAgInPc6d1	pohanský
krajích	kraj	k1gInPc6	kraj
-	-	kIx~	-
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
-	-	kIx~	-
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
vymýtit	vymýtit	k5eAaPmF	vymýtit
největší	veliký	k2eAgFnPc4d3	veliký
nešvary	nešvara	k1gFnPc4	nešvara
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
mnohoženství	mnohoženství	k1gNnSc3	mnohoženství
a	a	k8xC	a
alkoholismus	alkoholismus	k1gInSc1	alkoholismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
radikální	radikální	k2eAgInPc1d1	radikální
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
očistu	očista	k1gFnSc4	očista
věřících	věřící	k1gMnPc2	věřící
v	v	k7c6	v
diecézi	diecéze	k1gFnSc6	diecéze
však	však	k9	však
narazily	narazit	k5eAaPmAgFnP	narazit
na	na	k7c4	na
zažité	zažitý	k2eAgInPc4d1	zažitý
stereotypy	stereotyp	k1gInPc4	stereotyp
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
988	[number]	k4	988
znechuceně	znechuceně	k6eAd1	znechuceně
opustil	opustit	k5eAaPmAgMnS	opustit
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Radimem	Radim	k1gMnSc7	Radim
k	k	k7c3	k
papeži	papež	k1gMnSc3	papež
Janu	Jan	k1gMnSc3	Jan
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
Monte	Mont	k1gInSc5	Mont
Cassino	Cassina	k1gFnSc5	Cassina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
řeckého	řecký	k2eAgInSc2d1	řecký
kláštera	klášter	k1gInSc2	klášter
ve	v	k7c6	v
Valle	Vall	k1gInSc6	Vall
Luca	Lucus	k1gMnSc2	Lucus
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
reformního	reformní	k2eAgInSc2d1	reformní
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Aventinu	Aventin	k1gInSc6	Aventin
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
papeže	papež	k1gMnSc2	papež
složil	složit	k5eAaPmAgMnS	složit
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
990	[number]	k4	990
řeholní	řeholní	k2eAgInSc4d1	řeholní
slib	slib	k1gInSc4	slib
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
biskupa	biskup	k1gMnSc2	biskup
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
působiště	působiště	k1gNnSc2	působiště
nebyl	být	k5eNaImAgInS	být
pro	pro	k7c4	pro
české	český	k2eAgNnSc4d1	české
knížectví	knížectví	k1gNnSc4	knížectví
dobrou	dobrý	k2eAgFnSc7d1	dobrá
vizitkou	vizitka	k1gFnSc7	vizitka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
vyslal	vyslat	k5eAaPmAgMnS	vyslat
poselstvo	poselstvo	k1gNnSc4	poselstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
žádosti	žádost	k1gFnPc1	žádost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
992	[number]	k4	992
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přivedl	přivést	k5eAaPmAgMnS	přivést
skupinu	skupina	k1gFnSc4	skupina
italských	italský	k2eAgMnPc2d1	italský
mnichů	mnich	k1gMnPc2	mnich
a	a	k8xC	a
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
založil	založit	k5eAaPmAgInS	založit
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
první	první	k4xOgInSc1	první
mužský	mužský	k2eAgInSc1d1	mužský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
vysvěcen	vysvěcen	k2eAgInSc1d1	vysvěcen
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
993	[number]	k4	993
<g/>
.	.	kIx.	.
</s>
<s>
Poměry	poměr	k1gInPc1	poměr
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
knížectví	knížectví	k1gNnSc6	knížectví
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
Vojtěchova	Vojtěchův	k2eAgInSc2d1	Vojtěchův
odchodu	odchod	k1gInSc2	odchod
nijak	nijak	k6eAd1	nijak
nezměnily	změnit	k5eNaPmAgFnP	změnit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
994	[number]	k4	994
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
Vyvraždění	vyvraždění	k1gNnSc1	vyvraždění
slavníkovského	slavníkovský	k2eAgInSc2d1	slavníkovský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgNnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
druhý	druhý	k4xOgInSc4	druhý
odchod	odchod	k1gInSc4	odchod
definitivní	definitivní	k2eAgFnSc1d1	definitivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
996	[number]	k4	996
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
dohodnout	dohodnout	k5eAaPmF	dohodnout
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
opětného	opětný	k2eAgInSc2d1	opětný
návratu	návrat	k1gInSc2	návrat
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
však	však	k9	však
vyzněly	vyznět	k5eAaImAgFnP	vyznět
naprázdno	naprázdno	k6eAd1	naprázdno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
vyvraždění	vyvraždění	k1gNnSc4	vyvraždění
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
sv.	sv.	kA	sv.
Alexia	Alexium	k1gNnSc2	Alexium
na	na	k7c4	na
Aventinu	Aventina	k1gFnSc4	Aventina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
ke	k	k7c3	k
knížeti	kníže	k1gMnSc3	kníže
Boleslavu	Boleslav	k1gMnSc3	Boleslav
Chrabrému	chrabrý	k2eAgMnSc3d1	chrabrý
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podniknout	podniknout	k5eAaPmF	podniknout
misijní	misijní	k2eAgFnSc4d1	misijní
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
pohanským	pohanský	k2eAgMnPc3d1	pohanský
Luticům	Lutic	k1gMnPc3	Lutic
nebo	nebo	k8xC	nebo
Prusům	Prus	k1gMnPc3	Prus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
byly	být	k5eAaImAgFnP	být
nakonec	nakonec	k6eAd1	nakonec
vybrány	vybrán	k2eAgMnPc4d1	vybrán
pobaltské	pobaltský	k2eAgMnPc4d1	pobaltský
Prusy	Prus	k1gMnPc4	Prus
(	(	kIx(	(
<g/>
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Misie	misie	k1gFnSc1	misie
vedla	vést	k5eAaImAgFnS	vést
podél	podél	k7c2	podél
břehů	břeh	k1gInPc2	břeh
Visly	Visla	k1gFnSc2	Visla
a	a	k8xC	a
Gdaňského	gdaňský	k2eAgInSc2d1	gdaňský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
997	[number]	k4	997
dorazil	dorazit	k5eAaPmAgInS	dorazit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
druhy	druh	k1gInPc7	druh
a	a	k8xC	a
polským	polský	k2eAgInSc7d1	polský
vojenským	vojenský	k2eAgInSc7d1	vojenský
doprovodem	doprovod	k1gInSc7	doprovod
do	do	k7c2	do
Sambie	Sambie	k1gFnSc2	Sambie
<g/>
,	,	kIx,	,
nejbohatšího	bohatý	k2eAgInSc2d3	nejbohatší
kraje	kraj	k1gInSc2	kraj
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
obtížné	obtížný	k2eAgFnSc2d1	obtížná
evangelizace	evangelizace	k1gFnSc2	evangelizace
nechal	nechat	k5eAaPmAgMnS	nechat
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
mýtit	mýtit	k5eAaImF	mýtit
i	i	k9	i
pohanské	pohanský	k2eAgInPc4d1	pohanský
posvátné	posvátný	k2eAgInPc4d1	posvátný
duby	dub	k1gInPc4	dub
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
misijním	misijní	k2eAgNnSc6d1	misijní
působení	působení	k1gNnSc6	působení
byl	být	k5eAaImAgMnS	být
varován	varovat	k5eAaImNgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
misii	misie	k1gFnSc4	misie
pohansky	pohansky	k6eAd1	pohansky
věřící	věřící	k2eAgMnPc1d1	věřící
Prusové	Prus	k1gMnPc1	Prus
nepřejí	přát	k5eNaImIp3nP	přát
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
potrestán	potrestat	k5eAaPmNgInS	potrestat
i	i	k9	i
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
řečeno	řečen	k2eAgNnSc1d1	řečeno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Prusko	Prusko	k1gNnSc1	Prusko
jako	jako	k8xC	jako
misionář	misionář	k1gMnSc1	misionář
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
ale	ale	k8xC	ale
misie	misie	k1gFnSc2	misie
neukončil	ukončit	k5eNaPmAgInS	ukončit
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
997	[number]	k4	997
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
pro	pro	k7c4	pro
pohany	pohana	k1gFnPc4	pohana
posvátných	posvátný	k2eAgNnPc2d1	posvátné
míst	místo	k1gNnPc2	místo
-	-	kIx~	-
posvátném	posvátný	k2eAgInSc6d1	posvátný
háji	háj	k1gInSc6	háj
Kunterna	Kunterna	k1gFnSc1	Kunterna
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgMnSc4	který
byl	být	k5eAaImAgMnS	být
lidem	člověk	k1gMnPc3	člověk
zakázán	zakázán	k2eAgInSc4d1	zakázán
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
při	při	k7c6	při
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
jen	jen	k9	jen
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
náhodné	náhodný	k2eAgFnSc6d1	náhodná
návštěvě	návštěva	k1gFnSc6	návštěva
<g/>
,	,	kIx,	,
chycen	chycen	k2eAgInSc4d1	chycen
<g/>
,	,	kIx,	,
zajat	zajat	k2eAgInSc4d1	zajat
a	a	k8xC	a
rituálně	rituálně	k6eAd1	rituálně
zabit	zabít	k5eAaPmNgMnS	zabít
ochránci	ochránce	k1gMnPc1	ochránce
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	on	k3xPp3gInSc2	on
doprovodu	doprovod	k1gInSc2	doprovod
byla	být	k5eAaImAgNnP	být
ušetřena	ušetřit	k5eAaPmNgNnP	ušetřit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pro	pro	k7c4	pro
výstrahu	výstraha	k1gFnSc4	výstraha
jiným	jiný	k2eAgInSc7d1	jiný
mohla	moct	k5eAaImAgNnP	moct
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
podat	podat	k5eAaPmF	podat
svědectví	svědectví	k1gNnSc4	svědectví
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
při	při	k7c6	při
evangelizační	evangelizační	k2eAgFnSc6d1	evangelizační
činnosti	činnost	k1gFnSc6	činnost
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
mučedníka	mučedník	k1gMnSc4	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
příležitosti	příležitost	k1gFnPc4	příležitost
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
mladé	mladý	k2eAgNnSc4d1	mladé
polské	polský	k2eAgNnSc4d1	polské
knížectví	knížectví	k1gNnSc4	knížectví
vlastního	vlastní	k2eAgMnSc2d1	vlastní
mučedníka	mučedník	k1gMnSc2	mučedník
a	a	k8xC	a
světce	světec	k1gMnSc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěchovy	Vojtěchův	k2eAgInPc4d1	Vojtěchův
ostatky	ostatek	k1gInPc4	ostatek
od	od	k7c2	od
Prusů	Prus	k1gMnPc2	Prus
vykoupil	vykoupit	k5eAaPmAgInS	vykoupit
(	(	kIx(	(
<g/>
prý	prý	k9	prý
je	být	k5eAaImIp3nS	být
vyvážil	vyvážit	k5eAaPmAgMnS	vyvážit
zlatem	zlato	k1gNnSc7	zlato
<g/>
)	)	kIx)	)
a	a	k8xC	a
uložil	uložit	k5eAaPmAgMnS	uložit
je	být	k5eAaImIp3nS	být
katedrále	katedrála	k1gFnSc6	katedrála
v	v	k7c6	v
sídelním	sídelní	k2eAgNnSc6d1	sídelní
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
(	(	kIx(	(
<g/>
Gniezno	Gniezna	k1gFnSc5	Gniezna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
je	být	k5eAaImIp3nS	být
odvezl	odvézt	k5eAaPmAgMnS	odvézt
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1039	[number]	k4	1039
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
polské	polský	k2eAgFnSc2d1	polská
verze	verze	k1gFnSc2	verze
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
tělo	tělo	k1gNnSc1	tělo
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
výprava	výprava	k1gMnSc1	výprava
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
omylem	omylem	k6eAd1	omylem
vyzvedla	vyzvednout	k5eAaPmAgFnS	vyzvednout
a	a	k8xC	a
odvezla	odvézt	k5eAaPmAgFnS	odvézt
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
ostatky	ostatek	k1gInPc1	ostatek
jiného	jiný	k2eAgMnSc2d1	jiný
světce	světec	k1gMnSc2	světec
(	(	kIx(	(
<g/>
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
Vojtěchův	Vojtěchův	k2eAgMnSc1d1	Vojtěchův
bratr	bratr	k1gMnSc1	bratr
Radim	Radim	k1gMnSc1	Radim
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Gaudentius	Gaudentius	k1gInSc1	Gaudentius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
lebku	lebka	k1gFnSc4	lebka
Sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
polské	polský	k2eAgFnSc2d1	polská
tradice	tradice	k1gFnSc2	tradice
také	také	k9	také
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1127	[number]	k4	1127
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
odcizena	odcizen	k2eAgFnSc1d1	odcizena
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
další	další	k2eAgInSc1d1	další
osud	osud	k1gInSc1	osud
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
zkoumání	zkoumání	k1gNnSc1	zkoumání
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
antropologem	antropolog	k1gMnSc7	antropolog
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Vlčkem	Vlček	k1gMnSc7	Vlček
<g/>
,	,	kIx,	,
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
autentické	autentický	k2eAgInPc1d1	autentický
Vojtěchovy	Vojtěchův	k2eAgInPc1d1	Vojtěchův
ostatky	ostatek	k1gInPc1	ostatek
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
,	,	kIx,	,
ostatky	ostatek	k1gInPc4	ostatek
z	z	k7c2	z
Hnězdna	Hnězdno	k1gNnSc2	Hnězdno
a	a	k8xC	a
Cách	Cáchy	k1gFnPc2	Cáchy
jsou	být	k5eAaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
falzem	falzum	k1gNnSc7	falzum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
999	[number]	k4	999
byl	být	k5eAaImAgMnS	být
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
papežem	papež	k1gMnSc7	papež
Silvestrem	Silvestr	k1gMnSc7	Silvestr
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
císaře	císař	k1gMnSc2	císař
Oty	Ota	k1gMnSc2	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
prohlášen	prohlášen	k2eAgMnSc1d1	prohlášen
za	za	k7c4	za
svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vzápětí	vzápětí	k6eAd1	vzápětí
císař	císař	k1gMnSc1	císař
vykonal	vykonat	k5eAaPmAgMnS	vykonat
pouť	pouť	k1gFnSc4	pouť
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
hrobu	hrob	k1gInSc3	hrob
a	a	k8xC	a
udělil	udělit	k5eAaPmAgMnS	udělit
polskému	polský	k2eAgMnSc3d1	polský
knížeti	kníže	k1gMnSc3	kníže
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
Chrabrému	chrabrý	k2eAgMnSc3d1	chrabrý
významná	významný	k2eAgNnPc4d1	významné
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Vojtěchových	Vojtěchových	k2eAgInPc2d1	Vojtěchových
ostatků	ostatek	k1gInPc2	ostatek
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvezl	odvézt	k5eAaPmAgInS	odvézt
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
vystavět	vystavět	k5eAaPmF	vystavět
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
na	na	k7c6	na
Tiberském	tiberský	k2eAgInSc6d1	tiberský
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gInSc2	jeho
portálu	portál	k1gInSc2	portál
býval	bývat	k5eAaImAgInS	bývat
v	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
staletích	staletí	k1gNnPc6	staletí
nápis	nápis	k1gInSc4	nápis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
oznamoval	oznamovat	k5eAaImAgInS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kostele	kostel	k1gInSc6	kostel
spočívají	spočívat	k5eAaImIp3nP	spočívat
dvě	dva	k4xCgFnPc1	dva
ruce	ruka	k1gFnPc1	ruka
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
držela	držet	k5eAaImAgFnS	držet
arcibiskupskou	arcibiskupský	k2eAgFnSc4d1	arcibiskupská
berlu	berla	k1gFnSc4	berla
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
křtila	křtít	k5eAaImAgFnS	křtít
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
sv.	sv.	kA	sv.
Štěpána	Štěpána	k1gFnSc1	Štěpána
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
svatý	svatý	k2eAgMnSc1d1	svatý
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
druhého	druhý	k4xOgMnSc4	druhý
patrona	patron	k1gMnSc4	patron
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
po	po	k7c6	po
svatém	svatý	k2eAgMnSc6d1	svatý
Václavovi	Václav	k1gMnSc6	Václav
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
také	také	k9	také
hlavním	hlavní	k2eAgMnSc7d1	hlavní
patronem	patron	k1gMnSc7	patron
polské	polský	k2eAgFnSc2d1	polská
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
Uher	Uhry	k1gFnPc2	Uhry
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
994	[number]	k4	994
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
budoucího	budoucí	k2eAgMnSc4d1	budoucí
prvního	první	k4xOgMnSc2	první
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Štěpána	Štěpán	k1gMnSc2	Štěpán
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
jej	on	k3xPp3gMnSc4	on
apoštolským	apoštolský	k2eAgInSc7d1	apoštolský
listem	list	k1gInSc7	list
Praga	Praga	k1gFnSc1	Praga
urbis	urbis	k1gFnSc1	urbis
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1965	[number]	k4	1965
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
hlavním	hlavní	k2eAgMnSc7d1	hlavní
patronem	patron	k1gMnSc7	patron
pražské	pražský	k2eAgFnSc2d1	Pražská
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
(	(	kIx(	(
<g/>
stolec	stolec	k1gInSc1	stolec
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěchův	Vojtěchův	k2eAgMnSc1d1	Vojtěchův
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
Radim	Radim	k1gMnSc1	Radim
(	(	kIx(	(
<g/>
Gaudencius	Gaudencius	k1gMnSc1	Gaudencius
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
hnězdenským	hnězdenský	k2eAgMnSc7d1	hnězdenský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěchův	Vojtěchův	k2eAgMnSc1d1	Vojtěchův
rádce	rádce	k1gMnSc1	rádce
Radla	radlo	k1gNnSc2	radlo
<g/>
/	/	kIx~	/
<g/>
Anastázius	Anastázius	k1gMnSc1	Anastázius
založil	založit	k5eAaPmAgMnS	založit
uherské	uherský	k2eAgNnSc4d1	Uherské
opatství	opatství	k1gNnSc4	opatství
benediktinů	benediktin	k1gMnPc2	benediktin
Pannonhalma	Pannonhalmum	k1gNnSc2	Pannonhalmum
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
vrátil	vrátit	k5eAaPmAgInS	vrátit
roku	rok	k1gInSc2	rok
1039	[number]	k4	1039
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kníže	kníže	k1gMnSc1	kníže
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
podnikl	podniknout	k5eAaPmAgMnS	podniknout
válečnou	válečný	k2eAgFnSc4d1	válečná
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
slavnostně	slavnostně	k6eAd1	slavnostně
vyzvedl	vyzvednout	k5eAaPmAgMnS	vyzvednout
ostatky	ostatek	k1gInPc7	ostatek
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Radima	Radim	k1gMnSc4	Radim
a	a	k8xC	a
Pěti	pět	k4xCc2	pět
svatých	svatá	k1gFnPc2	svatá
bratří	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Světcovy	světcův	k2eAgInPc1d1	světcův
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
ve	v	k7c6	v
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
kapli	kaple	k1gFnSc6	kaple
<g/>
,	,	kIx,	,
přistavěné	přistavěný	k2eAgNnSc4d1	přistavěné
k	k	k7c3	k
rotundě	rotunda	k1gFnSc3	rotunda
sv.	sv.	kA	sv.
<g/>
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Spytihněv	Spytihněv	k1gMnSc1	Spytihněv
II	II	kA	II
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgMnS	založit
novou	nový	k2eAgFnSc4d1	nová
baziliku	bazilika	k1gFnSc4	bazilika
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
ji	on	k3xPp3gFnSc4	on
zasvětit	zasvětit	k5eAaPmF	zasvětit
společně	společně	k6eAd1	společně
sv.	sv.	kA	sv.
Vítu	Víta	k1gFnSc4	Víta
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Václavu	Václava	k1gFnSc4	Václava
i	i	k8xC	i
sv.	sv.	kA	sv.
Vojtěchu	Vojtěch	k1gMnSc3	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1143	[number]	k4	1143
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
ostatků	ostatek	k1gInPc2	ostatek
oddělena	oddělen	k2eAgFnSc1d1	oddělena
lebka	lebka	k1gFnSc1	lebka
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
relikviáři	relikviář	k1gInSc6	relikviář
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
lebky	lebka	k1gFnSc2	lebka
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
jiná	jiný	k2eAgFnSc1d1	jiná
lebka	lebka	k1gFnSc1	lebka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Cách	Cáchy	k1gFnPc2	Cáchy
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
svatovojtěšská	svatovojtěšský	k2eAgFnSc1d1	Svatovojtěšská
relikvie	relikvie	k1gFnSc1	relikvie
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vyobrazován	vyobrazovat	k5eAaImNgInS	vyobrazovat
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
<g/>
,	,	kIx,	,
slovenském	slovenský	k2eAgNnSc6d1	slovenské
<g/>
,	,	kIx,	,
polském	polský	k2eAgNnSc6d1	polské
a	a	k8xC	a
uherském	uherský	k2eAgNnSc6d1	Uherské
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentační	reprezentační	k2eAgNnSc1d1	reprezentační
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
<g/>
:	:	kIx,	:
klidně	klidně	k6eAd1	klidně
stojí	stát	k5eAaImIp3nS	stát
nebo	nebo	k8xC	nebo
žehná	žehnat	k5eAaImIp3nS	žehnat
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
baroka	baroko	k1gNnSc2	baroko
jeho	jeho	k3xOp3gNnSc1	jeho
pravá	pravý	k2eAgFnSc1d1	pravá
podoba	podoba	k1gFnSc1	podoba
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vera	vera	k1gMnSc1	vera
effigies	effigies	k1gMnSc1	effigies
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
oděv	oděv	k1gInSc1	oděv
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
biskupský	biskupský	k2eAgInSc1d1	biskupský
(	(	kIx(	(
<g/>
kasule	kasule	k1gFnSc1	kasule
<g/>
,	,	kIx,	,
pluviál	pluviál	k1gInSc1	pluviál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
biskupská	biskupský	k2eAgFnSc1d1	biskupská
mitra	mitra	k1gFnSc1	mitra
<g/>
,	,	kIx,	,
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
biskupská	biskupský	k2eAgFnSc1d1	biskupská
berla	berla	k1gFnSc1	berla
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
může	moct	k5eAaImIp3nS	moct
držet	držet	k5eAaImF	držet
své	svůj	k3xOyFgInPc4	svůj
atributy	atribut	k1gInPc4	atribut
mučednictví	mučednictví	k1gNnPc2	mučednictví
<g/>
:	:	kIx,	:
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
kopí	kopí	k1gNnSc1	kopí
<g/>
,	,	kIx,	,
veslo	veslo	k1gNnSc1	veslo
(	(	kIx(	(
<g/>
pádlo	pádlo	k1gNnSc1	pádlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
heraldické	heraldický	k2eAgFnSc2d1	heraldická
<g />
.	.	kIx.	.
</s>
<s>
znaky	znak	k1gInPc1	znak
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
a	a	k8xC	a
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
<g/>
)	)	kIx)	)
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Břevnově	Břevnov	k1gInSc6	Břevnov
(	(	kIx(	(
<g/>
břevno	břevno	k1gNnSc4	břevno
odklizené	odklizený	k2eAgNnSc4d1	odklizené
z	z	k7c2	z
pramene	pramen	k1gInSc2	pramen
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgInSc2d1	zvaný
nadále	nadále	k6eAd1	nadále
Vojtěška	Vojtěška	k1gFnSc1	Vojtěška
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vyobrazen	vyobrazit	k5eAaPmNgMnS	vyobrazit
jako	jako	k9	jako
mnich	mnich	k1gMnSc1	mnich
či	či	k8xC	či
misionář	misionář	k1gMnSc1	misionář
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
plnovous	plnovous	k1gInSc4	plnovous
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc4d1	černá
kutnu	kutna	k1gFnSc4	kutna
benediktina	benediktin	k1gMnSc4	benediktin
<g/>
,	,	kIx,	,
hůl	hůl	k1gFnSc4	hůl
a	a	k8xC	a
knihu	kniha	k1gFnSc4	kniha
řádových	řádový	k2eAgNnPc2d1	řádové
pravidel	pravidlo	k1gNnPc2	pravidlo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
na	na	k7c6	na
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
věži	věž	k1gFnSc6	věž
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
<g/>
)	)	kIx)	)
Legendární	legendární	k2eAgFnSc1d1	legendární
scéna	scéna	k1gFnSc1	scéna
<g/>
:	:	kIx,	:
misionář	misionář	k1gMnSc1	misionář
slouží	sloužit	k5eAaImIp3nS	sloužit
mši	mše	k1gFnSc4	mše
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
pozdvihuje	pozdvihovat	k5eAaImIp3nS	pozdvihovat
kalich	kalich	k1gInSc4	kalich
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jej	on	k3xPp3gMnSc4	on
pohané	pohan	k1gMnPc1	pohan
zezadu	zezadu	k6eAd1	zezadu
napadají	napadat	k5eAaImIp3nP	napadat
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
kopím	kopit	k5eAaImIp1nS	kopit
či	či	k8xC	či
pádlem	pádlo	k1gNnSc7	pádlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jak	jak	k8xC	jak
přivolává	přivolávat	k5eAaImIp3nS	přivolávat
déšť	déšť	k1gInSc1	déšť
Druhotné	druhotný	k2eAgFnSc2d1	druhotná
památky	památka	k1gFnSc2	památka
<g/>
:	:	kIx,	:
stopa	stopa	k1gFnSc1	stopa
(	(	kIx(	(
<g/>
šlépěj	šlépěj	k1gFnSc1	šlépěj
<g/>
)	)	kIx)	)
otisknutá	otisknutý	k2eAgFnSc1d1	otisknutá
v	v	k7c6	v
kameni	kámen	k1gInSc6	kámen
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
prošel	projít	k5eAaPmAgMnS	projít
relikvie	relikvie	k1gFnPc4	relikvie
<g/>
:	:	kIx,	:
lebka	lebka	k1gFnSc1	lebka
v	v	k7c6	v
relikviářovém	relikviářový	k2eAgNnSc6d1	relikviářový
poprsí	poprsí	k1gNnSc6	poprsí
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
Svatovítského	svatovítský	k2eAgInSc2d1	svatovítský
pokladu	poklad	k1gInSc2	poklad
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
celý	celý	k2eAgInSc4d1	celý
soubor	soubor	k1gInSc4	soubor
osobních	osobní	k2eAgInPc2d1	osobní
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
2	[number]	k4	2
prsteny	prsten	k1gInPc4	prsten
<g/>
,	,	kIx,	,
2	[number]	k4	2
křížky	křížek	k1gInPc1	křížek
<g/>
,	,	kIx,	,
hřeben	hřeben	k1gInSc1	hřeben
<g/>
,	,	kIx,	,
mitra	mitra	k1gFnSc1	mitra
<g/>
,	,	kIx,	,
rukavice	rukavice	k1gFnSc1	rukavice
a	a	k8xC	a
roucho	roucho	k1gNnSc1	roucho
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
polské	polský	k2eAgFnSc2d1	polská
kroniky	kronika	k1gFnSc2	kronika
Poláci	Polák	k1gMnPc1	Polák
nevydali	vydat	k5eNaPmAgMnP	vydat
Čechům	Čech	k1gMnPc3	Čech
Vojtěchovo	Vojtěchův	k2eAgNnSc4d1	Vojtěchovo
pravé	pravý	k2eAgNnSc4d1	pravé
tělo	tělo	k1gNnSc4	tělo
ani	ani	k8xC	ani
lebku	lebka	k1gFnSc4	lebka
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
je	on	k3xPp3gFnPc4	on
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
v	v	k7c6	v
Cáchách-Burtscheidu	Cáchách-Burtscheid	k1gInSc6	Cáchách-Burtscheid
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
hrob	hrob	k1gInSc1	hrob
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
kenotaf	kenotaf	k1gInSc4	kenotaf
v	v	k7c6	v
podlaze	podlaha	k1gFnSc6	podlaha
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kaple	kaple	k1gFnSc2	kaple
Libice	Libice	k1gFnPc1	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
-	-	kIx~	-
sídlo	sídlo	k1gNnSc4	sídlo
Vojtěchova	Vojtěchův	k2eAgInSc2d1	Vojtěchův
rodu	rod	k1gInSc2	rod
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
Břevnovský	břevnovský	k2eAgInSc4d1	břevnovský
klášter	klášter	k1gInSc4	klášter
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
který	který	k3yQgMnSc1	který
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
založil	založit	k5eAaPmAgMnS	založit
Broumovský	broumovský	k2eAgInSc4d1	broumovský
klášter	klášter	k1gInSc4	klášter
-	-	kIx~	-
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
Vojtěchovi	Vojtěch	k1gMnSc6	Vojtěch
zasvěceném	zasvěcený	k2eAgInSc6d1	zasvěcený
a	a	k8xC	a
kam	kam	k6eAd1	kam
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
za	za	k7c4	za
husitství	husitství	k1gNnSc4	husitství
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
břevnovští	břevnovský	k2eAgMnPc1d1	břevnovský
mniši	mnich	k1gMnPc1	mnich
Rajhradský	rajhradský	k2eAgInSc4d1	rajhradský
klášter	klášter	k1gInSc4	klášter
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
Bylany	Bylana	k1gFnSc2	Bylana
u	u	k7c2	u
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
Lštění	Lštění	k2eAgInSc1d1	Lštění
u	u	k7c2	u
Prachatic	Prachatice	k1gFnPc2	Prachatice
Milavče	Milavec	k1gMnSc5	Milavec
u	u	k7c2	u
Domažlic	Domažlice	k1gFnPc2	Domažlice
Neratovice	Neratovice	k1gFnSc1	Neratovice
Poleň	Poleň	k1gFnSc1	Poleň
na	na	k7c6	na
Klatovsku	Klatovsko	k1gNnSc6	Klatovsko
Vrčeň	Vrčeň	k1gFnSc4	Vrčeň
u	u	k7c2	u
Nepomuku	Nepomuk	k1gInSc2	Nepomuk
Všeruby	Všeruby	k1gInPc1	Všeruby
Přerov	Přerov	k1gInSc1	Přerov
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
boží	božit	k5eAaImIp3nS	božit
muka	muka	k1gFnSc1	muka
a	a	k8xC	a
studánka	studánka	k1gFnSc1	studánka
Na	na	k7c6	na
Vrších	vrš	k1gFnPc6	vrš
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g />
.	.	kIx.	.
</s>
<s>
Svatovojtěšská	svatovojtěšský	k2eAgFnSc1d1	Svatovojtěšská
studánka	studánka	k1gFnSc1	studánka
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
Svatý	svatý	k2eAgMnSc1d1	svatý
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
zlikvidovaná	zlikvidovaný	k2eAgFnSc1d1	zlikvidovaná
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
Vojtěšská	vojtěšský	k2eAgFnSc1d1	Vojtěšská
studánka	studánka	k1gFnSc1	studánka
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Vojtěchsbrunnen	Vojtěchsbrunnen	k1gInSc1	Vojtěchsbrunnen
<g/>
)	)	kIx)	)
Levý	levý	k2eAgInSc1d1	levý
Hradec	Hradec	k1gInSc1	Hradec
Starý	starý	k2eAgInSc4d1	starý
Plzenec	Plzenec	k1gInSc4	Plzenec
Votice	Votice	k1gFnPc4	Votice
u	u	k7c2	u
Benešova	Benešov	k1gInSc2	Benešov
katedrála	katedrála	k1gFnSc1	katedrála
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
pohřben	pohřben	k2eAgInSc1d1	pohřben
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
Opatství	opatství	k1gNnSc2	opatství
sv.	sv.	kA	sv.
sv.	sv.	kA	sv.
Bonifáce	Bonifác	k1gMnSc2	Bonifác
a	a	k8xC	a
Alexia	Alexius	k1gMnSc2	Alexius
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
na	na	k7c6	na
Aventinu	Aventin	k1gInSc6	Aventin
Opatství	opatství	k1gNnSc2	opatství
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc4	Benedikt
Monte	Mont	k1gInSc5	Mont
Cassino	Cassin	k2eAgNnSc1d1	Cassino
Tenkitten	Tenkitten	k2eAgInSc1d1	Tenkitten
u	u	k7c2	u
Königsbergu	Königsberg	k1gInSc2	Königsberg
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
,	,	kIx,	,
Cáchy	Cáchy	k1gFnPc1	Cáchy
-	-	kIx~	-
Burtscheid	Burtscheid	k1gInSc1	Burtscheid
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
mezi	mezi	k7c7	mezi
kostely	kostel	k1gInPc7	kostel
zasvěcenými	zasvěcený	k2eAgInPc7d1	zasvěcený
svatému	svatý	k2eAgMnSc3d1	svatý
Vojtěchovi	Vojtěch	k1gMnSc3	Vojtěch
<g/>
)	)	kIx)	)
</s>
