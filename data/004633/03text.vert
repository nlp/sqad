<s>
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
Rossumovi	Rossumův	k2eAgMnPc1d1	Rossumův
univerzální	univerzální	k2eAgMnSc1d1	univerzální
roboti	robot	k1gMnPc1	robot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědeckofantastické	vědeckofantastický	k2eAgNnSc4d1	vědeckofantastické
drama	drama	k1gNnSc4	drama
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
zde	zde	k6eAd1	zde
varuje	varovat	k5eAaImIp3nS	varovat
před	před	k7c7	před
případnými	případný	k2eAgInPc7d1	případný
negativními	negativní	k2eAgInPc7d1	negativní
vlivy	vliv	k1gInPc7	vliv
techniky	technika	k1gFnSc2	technika
na	na	k7c4	na
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
svým	svůj	k3xOyFgInSc7	svůj
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
obavami	obava	k1gFnPc7	obava
o	o	k7c4	o
budoucnost	budoucnost	k1gFnSc4	budoucnost
lidstva	lidstvo	k1gNnSc2	lidstvo
(	(	kIx(	(
<g/>
destrukce	destrukce	k1gFnSc1	destrukce
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
hře	hra	k1gFnSc6	hra
poprvé	poprvé	k6eAd1	poprvé
zaznělo	zaznít	k5eAaPmAgNnS	zaznít
slovo	slovo	k1gNnSc1	slovo
robot	robota	k1gFnPc2	robota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
na	na	k7c4	na
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
výzvu	výzva	k1gFnSc4	výzva
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jeho	jeho	k3xOp3gFnSc4	jeho
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
já	já	k3xPp1nSc1	já
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jak	jak	k8xC	jak
mám	mít	k5eAaImIp1nS	mít
ty	ten	k3xDgMnPc4	ten
umělé	umělý	k2eAgMnPc4d1	umělý
dělníky	dělník	k1gMnPc4	dělník
nazvat	nazvat	k5eAaPmF	nazvat
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
bych	by	kYmCp1nS	by
jim	on	k3xPp3gInPc3	on
laboři	laboř	k1gFnSc3	laboř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připadá	připadat	k5eAaPmIp3nS	připadat
mně	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
nějak	nějak	k6eAd1	nějak
papírové	papírový	k2eAgInPc1d1	papírový
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Tak	tak	k6eAd1	tak
jim	on	k3xPp3gMnPc3	on
řekni	říct	k5eAaPmRp2nS	říct
roboti	robot	k1gMnPc1	robot
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
mumlal	mumlat	k5eAaImAgMnS	mumlat
malíř	malíř	k1gMnSc1	malíř
se	s	k7c7	s
štětcem	štětec	k1gInSc7	štětec
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
a	a	k8xC	a
maloval	malovat	k5eAaImAgMnS	malovat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
původně	původně	k6eAd1	původně
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
slově	slovo	k1gNnSc6	slovo
"	"	kIx"	"
<g/>
labor	labor	k1gMnSc1	labor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc2	třicet
jazyků	jazyk	k1gInPc2	jazyk
vč.	vč.	k?	vč.
esperanta	esperanto	k1gNnSc2	esperanto
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgNnSc1d1	úvodní
dějství	dějství	k1gNnSc1	dějství
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
nespecifikované	specifikovaný	k2eNgFnSc6d1	nespecifikovaná
budoucnosti	budoucnost	k1gFnSc6	budoucnost
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
výrobny	výrobna	k1gFnSc2	výrobna
robotů	robot	k1gInPc2	robot
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
výrobu	výroba	k1gFnSc4	výroba
provádějí	provádět	k5eAaImIp3nP	provádět
roboti	robot	k1gMnPc1	robot
<g/>
.	.	kIx.	.
</s>
<s>
Připluje	připlout	k5eAaPmIp3nS	připlout
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
pěkná	pěkný	k2eAgFnSc1d1	pěkná
dcera	dcera	k1gFnSc1	dcera
prezidenta	prezident	k1gMnSc2	prezident
Helena	Helena	k1gFnSc1	Helena
Gloryová	Gloryová	k1gFnSc1	Gloryová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
roboty	robota	k1gFnPc4	robota
zrovnoprávnit	zrovnoprávnit	k5eAaPmF	zrovnoprávnit
a	a	k8xC	a
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
jí	on	k3xPp3gFnSc2	on
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
příběh	příběh	k1gInSc4	příběh
vynálezu	vynález	k1gInSc6	vynález
prvních	první	k4xOgInPc2	první
robotů	robot	k1gInPc2	robot
starého	starý	k2eAgNnSc2d1	staré
Rossumema	Rossumemum	k1gNnSc2	Rossumemum
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
synovce	synovec	k1gMnSc2	synovec
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vdá	vdát	k5eAaPmIp3nS	vdát
za	za	k7c2	za
ředitele	ředitel	k1gMnSc2	ředitel
Harryho	Harry	k1gMnSc2	Harry
Domina	domino	k1gNnSc2	domino
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
roboti	robot	k1gMnPc1	robot
široce	široko	k6eAd1	široko
rozšířeni	rozšířen	k2eAgMnPc1d1	rozšířen
<g/>
.	.	kIx.	.
</s>
<s>
Heleně	Helena	k1gFnSc3	Helena
je	být	k5eAaImIp3nS	být
smutno	smutno	k6eAd1	smutno
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
roboti	robot	k1gMnPc1	robot
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
velmi	velmi	k6eAd1	velmi
podobní	podobný	k2eAgMnPc1d1	podobný
lidem	lido	k1gNnSc7	lido
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
mít	mít	k5eAaImF	mít
city	cit	k1gInPc1	cit
jako	jako	k8xS	jako
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
přiměje	přimět	k5eAaPmIp3nS	přimět
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Galla	Gall	k1gMnSc2	Gall
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
začal	začít	k5eAaPmAgInS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
"	"	kIx"	"
<g/>
duší	duše	k1gFnSc7	duše
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
původním	původní	k2eAgInSc7d1	původní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
ráj	ráj	k1gInSc4	ráj
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nemuseli	muset	k5eNaImAgMnP	muset
lidé	člověk	k1gMnPc1	člověk
umírat	umírat	k5eAaImF	umírat
hlady	hlady	k6eAd1	hlady
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
všeho	všecek	k3xTgMnSc4	všecek
dostatek	dostatek	k1gInSc4	dostatek
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
upadly	upadnout	k5eAaPmAgInP	upadnout
mravy	mrav	k1gInPc1	mrav
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
zlenivěli	zlenivět	k5eAaPmAgMnP	zlenivět
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
roboty	robot	k1gInPc4	robot
původně	původně	k6eAd1	původně
určené	určený	k2eAgInPc4d1	určený
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
vojáky	voják	k1gMnPc4	voják
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
války	válek	k1gInPc4	válek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
robotů	robot	k1gInPc2	robot
vzbouřilo	vzbouřit	k5eAaPmAgNnS	vzbouřit
<g/>
,	,	kIx,	,
ustanovilo	ustanovit	k5eAaPmAgNnS	ustanovit
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
robotů	robot	k1gInPc2	robot
a	a	k8xC	a
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
válku	válka	k1gFnSc4	válka
lidstvu	lidstvo	k1gNnSc3	lidstvo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
drtivě	drtivě	k6eAd1	drtivě
porazilo	porazit	k5eAaPmAgNnS	porazit
a	a	k8xC	a
nenechalo	nechat	k5eNaPmAgNnS	nechat
nikoho	nikdo	k3yNnSc4	nikdo
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dorazili	dorazit	k5eAaPmAgMnP	dorazit
až	až	k9	až
na	na	k7c4	na
výrobní	výrobní	k2eAgInSc4d1	výrobní
ostrov	ostrov	k1gInSc4	ostrov
a	a	k8xC	a
pozabíjeli	pozabíjet	k5eAaPmAgMnP	pozabíjet
všechny	všechen	k3xTgMnPc4	všechen
až	až	k6eAd1	až
na	na	k7c4	na
Alquista	Alquist	k1gMnSc4	Alquist
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
ještě	ještě	k6eAd1	ještě
pracoval	pracovat	k5eAaImAgMnS	pracovat
rukama	ruka	k1gFnPc7	ruka
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
uklidňovalo	uklidňovat	k5eAaImAgNnS	uklidňovat
<g/>
;	;	kIx,	;
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
práce	práce	k1gFnSc1	práce
smyslem	smysl	k1gInSc7	smysl
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
roboti	robot	k1gMnPc1	robot
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
opotřebovávat	opotřebovávat	k5eAaImF	opotřebovávat
a	a	k8xC	a
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
objevil	objevit	k5eAaPmAgInS	objevit
původní	původní	k2eAgInSc1d1	původní
Rossumův	Rossumův	k2eAgInSc1d1	Rossumův
výrobní	výrobní	k2eAgInSc1d1	výrobní
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
spálila	spálit	k5eAaPmAgFnS	spálit
Helena	Helena	k1gFnSc1	Helena
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
ji	on	k3xPp3gFnSc4	on
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ho	on	k3xPp3gMnSc4	on
vzbudili	vzbudit	k5eAaPmAgMnP	vzbudit
robot	robot	k1gMnSc1	robot
Primus	primus	k1gMnSc1	primus
a	a	k8xC	a
robotka	robotka	k1gFnSc1	robotka
Helena	Helena	k1gFnSc1	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
chtěl	chtít	k5eAaImAgMnS	chtít
jednoho	jeden	k4xCgMnSc4	jeden
pitvat	pitvat	k5eAaImF	pitvat
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
nabízel	nabízet	k5eAaImAgInS	nabízet
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
se	se	k3xPyFc4	se
nechovali	chovat	k5eNaImAgMnP	chovat
logicky	logicky	k6eAd1	logicky
jako	jako	k8xC	jako
roboti	robot	k1gMnPc1	robot
(	(	kIx(	(
<g/>
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
chránit	chránit	k5eAaImF	chránit
<g/>
;	;	kIx,	;
záleželo	záležet	k5eAaImAgNnS	záležet
jim	on	k3xPp3gMnPc3	on
jednomu	jeden	k4xCgNnSc3	jeden
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
poslal	poslat	k5eAaPmAgMnS	poslat
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
udrželi	udržet	k5eAaPmAgMnP	udržet
život	život	k1gInSc4	život
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
znovu	znovu	k6eAd1	znovu
jako	jako	k9	jako
Adam	Adam	k1gMnSc1	Adam
a	a	k8xC	a
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
jasný	jasný	k2eAgInSc1d1	jasný
odkaz	odkaz	k1gInSc1	odkaz
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Bibli	bible	k1gFnSc3	bible
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
symbolický	symbolický	k2eAgMnSc1d1	symbolický
<g/>
.	.	kIx.	.
</s>
<s>
Roboti	robot	k1gMnPc1	robot
v	v	k7c6	v
Čapkově	Čapkův	k2eAgFnSc6d1	Čapkova
hře	hra	k1gFnSc6	hra
nejsou	být	k5eNaImIp3nP	být
roboty	robot	k1gInPc1	robot
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
biologičtí	biologický	k2eAgMnPc1d1	biologický
tvorové	tvor	k1gMnPc1	tvor
stvoření	stvoření	k1gNnSc2	stvoření
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
stylu	styl	k1gInSc6	styl
jako	jako	k9	jako
Frankensteinovo	Frankensteinův	k2eAgNnSc1d1	Frankensteinovo
monstrum	monstrum	k1gNnSc1	monstrum
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
píše	psát	k5eAaImIp3nS	psát
Mary	Mary	k1gFnSc1	Mary
Shelley	Shellea	k1gFnSc2	Shellea
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
biologickými	biologický	k2eAgFnPc7d1	biologická
entitami	entita	k1gFnPc7	entita
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
montují	montovat	k5eAaImIp3nP	montovat
dohromady	dohromady	k6eAd1	dohromady
jako	jako	k9	jako
automobily	automobil	k1gInPc1	automobil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
nejmodernější	moderní	k2eAgFnSc3d3	nejmodernější
koncepci	koncepce	k1gFnSc3	koncepce
živých	živý	k2eAgFnPc2d1	živá
forem	forma	k1gFnPc2	forma
stvořených	stvořený	k2eAgFnPc2d1	stvořená
člověkem	člověk	k1gMnSc7	člověk
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
replikanti	replikant	k1gMnPc1	replikant
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Blade	Blad	k1gInSc5	Blad
Runner	Runner	k1gMnSc1	Runner
nebo	nebo	k8xC	nebo
Cyloni	Cylon	k1gMnPc1	Cylon
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
seriálu	seriál	k1gInSc2	seriál
Battlestar	Battlestar	k1gMnSc1	Battlestar
Galactica	Galactica	k1gMnSc1	Galactica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čapkově	Čapkův	k2eAgFnSc6d1	Čapkova
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
neexistovala	existovat	k5eNaImAgFnS	existovat
žádná	žádný	k3yNgNnPc4	žádný
koncepce	koncepce	k1gFnSc1	koncepce
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
roboti	robot	k1gMnPc1	robot
jsou	být	k5eAaImIp3nP	být
biologické	biologický	k2eAgInPc4d1	biologický
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sestaveni	sestaven	k2eAgMnPc1d1	sestaven
jako	jako	k8xS	jako
protiklad	protiklad	k1gInSc1	protiklad
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
rostli	růst	k5eAaImAgMnP	růst
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
krátký	krátký	k2eAgInSc4d1	krátký
35	[number]	k4	35
<g/>
minutový	minutový	k2eAgInSc4d1	minutový
britský	britský	k2eAgInSc4d1	britský
film	film	k1gInSc4	film
produkovaný	produkovaný	k2eAgInSc4d1	produkovaný
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
natočen	natočen	k2eAgInSc1d1	natočen
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bude	být	k5eAaImBp3nS	být
režírovat	režírovat	k5eAaImF	režírovat
James	James	k1gMnSc1	James
Kerwin	Kerwin	k1gMnSc1	Kerwin
<g/>
..	..	k?	..
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
napsal	napsat	k5eAaPmAgMnS	napsat
podle	podle	k7c2	podle
hry	hra	k1gFnSc2	hra
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
hry	hra	k1gFnSc2	hra
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
na	na	k7c6	na
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
uvedly	uvést	k5eAaPmAgFnP	uvést
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
termín	termín	k1gInSc1	termín
byl	být	k5eAaImAgInS	být
odložen	odložit	k5eAaPmNgInS	odložit
na	na	k7c6	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
ochotnický	ochotnický	k2eAgInSc1d1	ochotnický
soubor	soubor	k1gInSc1	soubor
Klicpera	Klicper	k1gMnSc2	Klicper
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
mít	mít	k5eAaImF	mít
premiéru	premiéra	k1gFnSc4	premiéra
téže	týž	k3xTgFnSc2	týž
hry	hra	k1gFnSc2	hra
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c4	o
odložení	odložení	k1gNnSc4	odložení
nedozvěděl	dozvědět	k5eNaPmAgMnS	dozvědět
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
odehrála	odehrát	k5eAaPmAgFnS	odehrát
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1921	[number]	k4	1921
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
