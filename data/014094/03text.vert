<s>
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Irská	irský	k2eAgFnSc1d1
librapunt	librapunt	k1gInSc1
Éireannach	Éireannach	k1gInSc1
(	(	kIx(
<g/>
irština	irština	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Země	země	k1gFnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
Irsko	Irsko	k1gNnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
IEP	IEP	kA
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
subunit	subunit	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
½	½	k?
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
pencí	pence	k1gFnPc2
a	a	k8xC
1	#num#	k4
libra	libra	k1gFnSc1
Bankovky	bankovka	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
100	#num#	k4
liber	libra	k1gFnPc2
</s>
<s>
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
(	(	kIx(
<g/>
irsky	irsky	k6eAd1
punt	punt	k1gInSc1
Éireannach	Éireannacha	k1gFnPc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
měna	měna	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
v	v	k7c6
Irsku	Irsko	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1979	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
vystřídala	vystřídat	k5eAaPmAgFnS
britskou	britský	k2eAgFnSc4d1
libru	libra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irskou	irský	k2eAgFnSc4d1
libru	libra	k1gFnSc4
vystřídalo	vystřídat	k5eAaPmAgNnS
euro	euro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
měny	měna	k1gFnSc2
byl	být	k5eAaImAgMnS
IEP	IEP	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnPc1
nahrazené	nahrazený	k2eAgFnPc1d1
eurem	euro	k1gNnSc7
</s>
<s>
Belgický	belgický	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Estonská	estonský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Finská	finský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Francouzský	francouzský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Italská	italský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Kyperská	kyperský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Litevský	litevský	k2eAgInSc1d1
litas	litas	k1gInSc1
•	•	k?
Lotyšský	lotyšský	k2eAgInSc1d1
lat	lat	k1gInSc1
•	•	k?
Lucemburský	lucemburský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Maltská	maltský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Monacký	monacký	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Nizozemský	nizozemský	k2eAgInSc1d1
gulden	gulden	k1gInSc1
•	•	k?
Portugalské	portugalský	k2eAgNnSc4d1
escudo	escudo	k1gNnSc4
•	•	k?
Rakouský	rakouský	k2eAgInSc1d1
šilink	šilink	k1gInSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
drachma	drachma	k1gFnSc1
•	•	k?
Sanmarinská	Sanmarinský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Slovenská	slovenský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Slovinský	slovinský	k2eAgInSc1d1
tolar	tolar	k1gInSc1
•	•	k?
Španělská	španělský	k2eAgFnSc1d1
peseta	peseta	k1gFnSc1
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
</s>
