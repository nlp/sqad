<s>
Její	její	k3xOp3gNnSc1	její
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Andorra	Andorra	k1gFnSc1	Andorra
la	la	k1gNnSc1	la
Vella	Vell	k1gMnSc2	Vell
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Valira	Valir	k1gInSc2	Valir
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvýše	nejvýše	k6eAd1	nejvýše
položeným	položený	k2eAgNnSc7d1	položené
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
1023	[number]	k4	1023
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
