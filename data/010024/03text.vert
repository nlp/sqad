<p>
<s>
Bayon	Bayon	k1gInSc1	Bayon
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
kamenný	kamenný	k2eAgInSc1d1	kamenný
chrám	chrám	k1gInSc1	chrám
na	na	k7c6	na
území	území	k1gNnSc6	území
současné	současný	k2eAgFnSc2d1	současná
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
chrámů	chrám	k1gInPc2	chrám
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Angkorské	Angkorský	k2eAgFnSc2d1	Angkorská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Džajavarmana	Džajavarman	k1gMnSc2	Džajavarman
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
ústřední	ústřední	k2eAgInSc1d1	ústřední
chrám	chrám	k1gInSc1	chrám
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
-	-	kIx~	-
Angkor	Angkor	k1gInSc1	Angkor
Thomu	Thom	k1gInSc2	Thom
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
přesně	přesně	k6eAd1	přesně
uprostřed	uprostřed	k7c2	uprostřed
tohoto	tento	k3xDgInSc2	tento
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
městského	městský	k2eAgInSc2d1	městský
komplexu	komplex	k1gInSc2	komplex
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
střetu	střet	k1gInSc2	střet
severo-jižní	severoižní	k2eAgFnSc2d1	severo-jižní
a	a	k8xC	a
východo-západní	východoápadní	k2eAgFnSc2d1	východo-západní
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
typ	typ	k1gInSc4	typ
pyramidovitých	pyramidovitý	k2eAgInPc2d1	pyramidovitý
chrámů	chrám	k1gInPc2	chrám
symbolisujících	symbolisující	k2eAgFnPc2d1	symbolisující
posvátnou	posvátný	k2eAgFnSc4d1	posvátná
hinduistickou	hinduistický	k2eAgFnSc4d1	hinduistická
horu	hora	k1gFnSc4	hora
Méru	Mérus	k1gInSc2	Mérus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
se	se	k3xPyFc4	se
od	od	k7c2	od
těch	ten	k3xDgFnPc2	ten
předcházejících	předcházející	k2eAgFnPc2d1	předcházející
množstvím	množství	k1gNnSc7	množství
věží	věž	k1gFnPc2	věž
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
snad	snad	k9	snad
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
49	[number]	k4	49
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
stran	strana	k1gFnPc2	strana
zdobeny	zdobit	k5eAaImNgInP	zdobit
obřími	obří	k2eAgFnPc7d1	obří
tvářemi	tvář	k1gFnPc7	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bayon	Bayon	k1gInSc1	Bayon
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
Angkor	Angkor	k1gInSc4	Angkor
Thom	Thomo	k1gNnPc2	Thomo
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
mahájánové	mahájánový	k2eAgFnSc6d1	mahájánový
éře	éra	k1gFnSc6	éra
khmerské	khmerský	k2eAgFnSc2d1	khmerská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
tváře	tvář	k1gFnPc1	tvář
zřejmě	zřejmě	k6eAd1	zřejmě
mají	mít	k5eAaImIp3nP	mít
představovat	představovat	k5eAaImF	představovat
bódhisattvu	bódhisattva	k1gFnSc4	bódhisattva
Avalókitéšvaru	Avalókitéšvar	k1gInSc2	Avalókitéšvar
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
hypotesy	hypotes	k1gInPc1	hypotes
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tvář	tvář	k1gFnSc4	tvář
samotného	samotný	k2eAgMnSc2d1	samotný
Džajavarmana	Džajavarman	k1gMnSc2	Džajavarman
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
věž	věž	k1gFnSc1	věž
Bayonu	Bayon	k1gInSc2	Bayon
má	mít	k5eAaImIp3nS	mít
kruhový	kruhový	k2eAgInSc4d1	kruhový
půdorys	půdorys	k1gInSc4	půdorys
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
rovněž	rovněž	k9	rovněž
představuje	představovat	k5eAaImIp3nS	představovat
inovaci	inovace	k1gFnSc4	inovace
v	v	k7c6	v
angkorské	angkorský	k2eAgFnSc6d1	angkorský
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
<g/>
Bayon	Bayon	k1gInSc1	Bayon
vyniká	vynikat	k5eAaImIp3nS	vynikat
i	i	k9	i
množstvím	množství	k1gNnSc7	množství
basreliéfů	basreliéf	k1gInPc2	basreliéf
v	v	k7c6	v
dolních	dolní	k2eAgInPc6d1	dolní
ochozech	ochoz	k1gInPc6	ochoz
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
řadu	řada	k1gFnSc4	řada
realistických	realistický	k2eAgFnPc2d1	realistická
scén	scéna	k1gFnPc2	scéna
z	z	k7c2	z
dobového	dobový	k2eAgInSc2d1	dobový
života	život	k1gInSc2	život
starých	starý	k2eAgInPc2d1	starý
Khmerů	Khmer	k1gInPc2	Khmer
<g/>
;	;	kIx,	;
nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
výjev	výjev	k1gInSc4	výjev
bitvy	bitva	k1gFnSc2	bitva
Khmerů	Khmer	k1gMnPc2	Khmer
s	s	k7c7	s
Čamy	Čam	k1gMnPc7	Čam
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Angkor	Angkor	k1gInSc4	Angkor
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
Angkor	Angkor	k1gInSc4	Angkor
Thomu	Thom	k1gInSc2	Thom
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
basreliéfy	basreliéf	k1gInPc1	basreliéf
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
detaily	detail	k1gInPc4	detail
z	z	k7c2	z
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
kohoutí	kohoutí	k2eAgInPc4d1	kohoutí
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
hostiny	hostina	k1gFnPc4	hostina
<g/>
,	,	kIx,	,
kejklíře	kejklíř	k1gMnPc4	kejklíř
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
míře	míra	k1gFnSc6	míra
vůbec	vůbec	k9	vůbec
prvně	prvně	k?	prvně
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
angkorské	angkorský	k2eAgFnSc6d1	angkorský
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Kuriosní	kuriosní	k2eAgInPc1d1	kuriosní
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
některé	některý	k3yIgInPc1	některý
vtipné	vtipný	k2eAgInPc1d1	vtipný
detaily	detail	k1gInPc1	detail
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
autoři	autor	k1gMnPc1	autor
do	do	k7c2	do
scén	scéna	k1gFnPc2	scéna
zapojili	zapojit	k5eAaPmAgMnP	zapojit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
Bayon	Bayon	k1gInSc1	Bayon
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
Angkor	Angkor	k1gInSc1	Angkor
Thom	Thoma	k1gFnPc2	Thoma
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
angkorskými	angkorský	k2eAgFnPc7d1	angkorský
památkami	památka	k1gFnPc7	památka
zapsán	zapsat	k5eAaPmNgMnS	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ALBANESOVÁ	ALBANESOVÁ	kA	ALBANESOVÁ
<g/>
,	,	kIx,	,
Marilia	Marilia	k1gFnSc1	Marilia
<g/>
:	:	kIx,	:
Poklady	poklad	k1gInPc1	poklad
Angkoru	Angkor	k1gInSc2	Angkor
<g/>
.	.	kIx.	.
</s>
<s>
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
924	[number]	k4	924
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JACQUES	Jacques	k1gMnSc1	Jacques
<g/>
,	,	kIx,	,
Claude	Claud	k1gInSc5	Claud
a	a	k8xC	a
FREEMAN	FREEMAN	kA	FREEMAN
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
:	:	kIx,	:
Angkor	Angkor	k1gMnSc1	Angkor
-	-	kIx~	-
cité	cité	k6eAd1	cité
khmè	khmè	k?	khmè
<g/>
.	.	kIx.	.
</s>
<s>
River	River	k1gInSc1	River
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KRÁSA	krása	k1gFnSc1	krása
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
:	:	kIx,	:
Angkor	Angkor	k1gInSc1	Angkor
-	-	kIx~	-
umění	umění	k1gNnSc1	umění
staré	starý	k2eAgFnSc2d1	stará
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
MABBETT	MABBETT	kA	MABBETT
<g/>
,	,	kIx,	,
Ian	Ian	k1gFnSc1	Ian
a	a	k8xC	a
CHANDLER	CHANDLER	kA	CHANDLER
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
:	:	kIx,	:
Khmerové	Khmer	k1gMnPc1	Khmer
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOŽINA	NOŽINA	kA	NOŽINA
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
897	[number]	k4	897
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Freeman	Freeman	k1gMnSc1	Freeman
<g/>
,	,	kIx,	,
Claude	Claud	k1gInSc5	Claud
Jacques	Jacques	k1gMnSc1	Jacques
<g/>
:	:	kIx,	:
Ancient	Ancient	k1gMnSc1	Ancient
Angkor	Angkor	k1gMnSc1	Angkor
<g/>
.	.	kIx.	.
</s>
<s>
Asia	Asia	k6eAd1	Asia
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
Bangkok	Bangkok	k1gInSc1	Bangkok
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Vittorio	Vittorio	k1gMnSc1	Vittorio
Roveda	Roveda	k1gMnSc1	Roveda
<g/>
:	:	kIx,	:
Khmer	Khmer	k1gMnSc1	Khmer
Mythology	mytholog	k1gMnPc7	mytholog
<g/>
.	.	kIx.	.
</s>
<s>
River	River	k1gInSc1	River
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
Bangkok	Bangkok	k1gInSc1	Bangkok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Gabriele	Gabriela	k1gFnSc3	Gabriela
Fahr-Becker	Fahr-Becker	k1gMnSc1	Fahr-Becker
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ostasiatische	Ostasiatische	k1gFnSc1	Ostasiatische
Kunst	Kunst	k1gFnSc1	Kunst
<g/>
.	.	kIx.	.
</s>
<s>
Könemann	Könemann	k1gInSc1	Könemann
<g/>
,	,	kIx,	,
Köln	Köln	k1gInSc1	Köln
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
Dagens	Dagens	k1gInSc1	Dagens
<g/>
:	:	kIx,	:
Angkor	Angkor	k1gInSc1	Angkor
-	-	kIx~	-
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
an	an	k?	an
Asian	Asian	k1gInSc1	Asian
Empire	empir	k1gInSc5	empir
<g/>
.	.	kIx.	.
</s>
<s>
Engl	Engl	k1gInSc1	Engl
<g/>
.	.	kIx.	.
v.	v.	k?	v.
Ruth	Ruth	k1gFnPc2	Ruth
Sharman	Sharman	k1gMnSc1	Sharman
<g/>
.	.	kIx.	.
</s>
<s>
Thames	Thames	k1gMnSc1	Thames
&	&	k?	&
Hudson	Hudson	k1gMnSc1	Hudson
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bayon	Bayona	k1gFnPc2	Bayona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
UNESCO	UNESCO	kA	UNESCO
</s>
</p>
<p>
<s>
The	The	k?	The
Bayon	Bayon	k1gInSc1	Bayon
Symposium	symposium	k1gNnSc1	symposium
(	(	kIx(	(
<g/>
Unesco	Unesco	k1gNnSc1	Unesco
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1996	[number]	k4	1996
-	-	kIx~	-
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Photos	Photos	k1gMnSc1	Photos
Of	Of	k1gMnSc1	Of
Bayon	Bayon	k1gMnSc1	Bayon
</s>
</p>
<p>
<s>
Bayon	Bayon	k1gMnSc1	Bayon
Goddesses	Goddesses	k1gMnSc1	Goddesses
-	-	kIx~	-
Devata	Devata	k1gFnSc1	Devata
of	of	k?	of
King	King	k1gMnSc1	King
Jayavarman	Jayavarman	k1gMnSc1	Jayavarman
VII	VII	kA	VII
</s>
</p>
<p>
<s>
Are	ar	k1gInSc5	ar
Ancient	Ancient	k1gInSc4	Ancient
Goddesses	Goddesses	k1gInSc1	Goddesses
Actually	Actualla	k1gFnSc2	Actualla
12	[number]	k4	12
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Khmer	Khmer	k1gMnSc1	Khmer
Queens	Queens	k1gInSc1	Queens
<g/>
?	?	kIx.	?
</s>
</p>
