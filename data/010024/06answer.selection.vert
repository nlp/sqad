<s>
Bayon	Bayon	k1gInSc1	Bayon
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
kamenný	kamenný	k2eAgInSc1d1	kamenný
chrám	chrám	k1gInSc1	chrám
na	na	k7c6	na
území	území	k1gNnSc6	území
současné	současný	k2eAgFnSc2d1	současná
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
chrámů	chrám	k1gInPc2	chrám
zachovaných	zachovaný	k2eAgInPc2d1	zachovaný
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Angkorské	Angkorský	k2eAgFnSc2d1	Angkorská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
