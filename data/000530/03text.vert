<s>
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Siddhártha	Siddhárth	k1gMnSc2	Siddhárth
Gautama	Gautam	k1gMnSc2	Gautam
<g/>
,	,	kIx,	,
v	v	k7c6	v
páli	pál	k1gFnSc6	pál
Siddhattha	Siddhatth	k1gMnSc2	Siddhatth
Gótama	Gótama	k?	Gótama
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
564	[number]	k4	564
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kapilavastu	Kapilavast	k1gInSc2	Kapilavast
/	/	kIx~	/
<g/>
v	v	k7c6	v
páli	pál	k1gInSc6	pál
Kapilavatthu	Kapilavatth	k1gInSc2	Kapilavatth
<g/>
/	/	kIx~	/
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Lumbiní	Lumbiní	k1gFnSc6	Lumbiní
-	-	kIx~	-
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
zemřel	zemřít	k5eAaPmAgMnS	zemřít
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
484	[number]	k4	484
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kušinagara	Kušinagara	k1gFnSc1	Kušinagara
/	/	kIx~	/
<g/>
v	v	k7c6	v
páli	pál	k1gInSc6	pál
Kusinára	Kusinár	k1gMnSc2	Kusinár
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
théravádové	théravádový	k2eAgFnSc2d1	théravádový
tradice	tradice	k1gFnSc2	tradice
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
623	[number]	k4	623
<g/>
-	-	kIx~	-
<g/>
543	[number]	k4	543
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Buddha	Buddha	k1gMnSc1	Buddha
znamená	znamenat	k5eAaImIp3nS	znamenat
probuzený	probuzený	k2eAgMnSc1d1	probuzený
<g/>
,	,	kIx,	,
osvícený	osvícený	k2eAgMnSc1d1	osvícený
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
princ	princ	k1gMnSc1	princ
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
Šákjů	Šákj	k1gMnPc2	Šákj
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
nazýván	nazývat	k5eAaImNgMnS	nazývat
Buddhou	Buddha	k1gMnSc7	Buddha
Šákjamunim	Šákjamunim	k1gMnSc1	Šákjamunim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
29	[number]	k4	29
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgInS	žít
pohodlným	pohodlný	k2eAgInSc7d1	pohodlný
životem	život	k1gInSc7	život
syna	syn	k1gMnSc2	syn
místního	místní	k2eAgMnSc2d1	místní
vládce	vládce	k1gMnSc2	vládce
<g/>
,	,	kIx,	,
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
(	(	kIx(	(
<g/>
manželka	manželka	k1gFnSc1	manželka
Jasódhara	Jasódhara	k1gFnSc1	Jasódhara
<g/>
)	)	kIx)	)
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
syna	syn	k1gMnSc4	syn
(	(	kIx(	(
<g/>
Ráhula	Ráhul	k1gMnSc4	Ráhul
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
-	-	kIx~	-
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
spatřil	spatřit	k5eAaPmAgMnS	spatřit
právě	právě	k6eAd1	právě
narozeného	narozený	k2eAgMnSc4d1	narozený
syna	syn	k1gMnSc4	syn
-	-	kIx~	-
opustil	opustit	k5eAaPmAgMnS	opustit
domov	domov	k1gInSc4	domov
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
nejrůznějším	různý	k2eAgFnPc3d3	nejrůznější
praktikám	praktika	k1gFnPc3	praktika
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgFnPc2	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
osvobození	osvobození	k1gNnSc4	osvobození
z	z	k7c2	z
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
existencí	existence	k1gFnSc7	existence
<g/>
,	,	kIx,	,
a	a	k8xC	a
neustálého	neustálý	k2eAgInSc2d1	neustálý
koloběhu	koloběh	k1gInSc2	koloběh
znovuzrozování	znovuzrozování	k1gNnSc1	znovuzrozování
(	(	kIx(	(
<g/>
samsáry	samsár	k1gInPc1	samsár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
35	[number]	k4	35
let	let	k1gInSc1	let
objevil	objevit	k5eAaPmAgInS	objevit
střední	střední	k2eAgFnSc4d1	střední
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
všem	všecek	k3xTgFnPc3	všecek
krajnostem	krajnost	k1gFnPc3	krajnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
probuzení	probuzení	k1gNnSc2	probuzení
<g/>
,	,	kIx,	,
konečného	konečný	k2eAgNnSc2d1	konečné
osvobození	osvobození	k1gNnSc2	osvobození
z	z	k7c2	z
utrpení	utrpení	k1gNnSc2	utrpení
a	a	k8xC	a
koloběhu	koloběh	k1gInSc2	koloběh
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgNnPc2d1	následující
45	[number]	k4	45
let	léto	k1gNnPc2	léto
předával	předávat	k5eAaImAgMnS	předávat
své	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
mnichům	mnich	k1gMnPc3	mnich
(	(	kIx(	(
<g/>
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgNnSc4	první
mnišské	mnišský	k2eAgNnSc4d1	mnišské
společenství	společenství	k1gNnSc4	společenství
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
laikům	laik	k1gMnPc3	laik
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
u	u	k7c2	u
Kušinagary	Kušinagara	k1gFnSc2	Kušinagara
obklopen	obklopit	k5eAaPmNgMnS	obklopit
svými	svůj	k3xOyFgMnPc7	svůj
mnišskými	mnišský	k2eAgMnPc7d1	mnišský
následovníky	následovník	k1gMnPc7	následovník
<g/>
.	.	kIx.	.
</s>
<s>
Buddhismus	buddhismus	k1gInSc1	buddhismus
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgNnSc4d3	nejstarší
univerzální	univerzální	k2eAgNnSc4d1	univerzální
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvěma	dva	k4xCgInPc7	dva
tisíci	tisíc	k4xCgInPc7	tisíc
lety	let	k1gInPc7	let
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
duchovní	duchovní	k2eAgInSc1d1	duchovní
život	život	k1gInSc1	život
a	a	k8xC	a
směřování	směřování	k1gNnSc1	směřování
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgMnS	zrodit
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Indočíně	Indočína	k1gFnSc6	Indočína
<g/>
,	,	kIx,	,
Tibetu	Tibet	k1gInSc6	Tibet
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Utvářel	utvářet	k5eAaImAgInS	utvářet
kulturu	kultura	k1gFnSc4	kultura
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
svého	svůj	k3xOyFgInSc2	svůj
náboženského	náboženský	k2eAgInSc2d1	náboženský
významu	význam	k1gInSc2	význam
určujícím	určující	k2eAgInSc7d1	určující
elementem	element	k1gInSc7	element
jejich	jejich	k3xOp3gFnSc2	jejich
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Přibývá	přibývat	k5eAaImIp3nS	přibývat
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
získávají	získávat	k5eAaImIp3nP	získávat
buddhistické	buddhistický	k2eAgFnSc2d1	buddhistická
ideje	idea	k1gFnSc2	idea
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Buddhistické	buddhistický	k2eAgNnSc1d1	buddhistické
hnutí	hnutí	k1gNnSc1	hnutí
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc4	svůj
historického	historický	k2eAgMnSc4d1	historický
zakladatele	zakladatel	k1gMnSc4	zakladatel
v	v	k7c6	v
Gautamovi	Gautam	k1gMnSc6	Gautam
Siddhárthovi	Siddhárth	k1gMnSc6	Siddhárth
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Probuzený	probuzený	k2eAgMnSc1d1	probuzený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nestal	stát	k5eNaPmAgMnS	stát
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Siddhártha	Siddhárth	k1gMnSc2	Siddhárth
nebo	nebo	k8xC	nebo
příjmením	příjmení	k1gNnSc7	příjmení
Gautama	Gautama	k1gFnSc1	Gautama
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
právě	právě	k9	právě
pod	pod	k7c7	pod
titulem	titul	k1gInSc7	titul
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Gautama	Gautamum	k1gNnSc2	Gautamum
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
klasických	klasický	k2eAgFnPc2d1	klasická
biografií	biografie	k1gFnPc2	biografie
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
možné	možný	k2eAgNnSc1d1	možné
chronologicky	chronologicky	k6eAd1	chronologicky
seřadit	seřadit	k5eAaPmF	seřadit
důležité	důležitý	k2eAgFnPc4d1	důležitá
události	událost	k1gFnPc4	událost
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prameny	pramen	k1gInPc1	pramen
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
přesné	přesný	k2eAgNnSc4d1	přesné
datování	datování	k1gNnSc4	datování
<g/>
.	.	kIx.	.
</s>
<s>
Buddhistická	buddhistický	k2eAgFnSc1d1	buddhistická
tradice	tradice	k1gFnSc1	tradice
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
kolísají	kolísat	k5eAaImIp3nP	kolísat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
předpokladech	předpoklad	k1gInPc6	předpoklad
mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k8xS	až
polovinou	polovina	k1gFnSc7	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
zasazuje	zasazovat	k5eAaImIp3nS	zasazovat
Gautamův	Gautamův	k2eAgInSc4d1	Gautamův
život	život	k1gInSc4	život
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
let	léto	k1gNnPc2	léto
560	[number]	k4	560
<g/>
-	-	kIx~	-
<g/>
480	[number]	k4	480
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
novější	nový	k2eAgFnSc2d2	novější
úvahy	úvaha	k1gFnSc2	úvaha
ale	ale	k8xC	ale
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
o	o	k7c4	o
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
Indie	Indie	k1gFnSc2	Indie
stály	stát	k5eAaImAgInP	stát
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
dva	dva	k4xCgInPc4	dva
politické	politický	k2eAgInPc4d1	politický
systémy	systém	k1gInPc4	systém
<g/>
:	:	kIx,	:
starší	starý	k2eAgFnSc1d2	starší
forma	forma	k1gFnSc1	forma
oligarchické	oligarchický	k2eAgFnSc2d1	oligarchická
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
jako	jako	k9	jako
Liččhaví	Liččhavý	k2eAgMnPc1d1	Liččhavý
<g/>
,	,	kIx,	,
Šákja	Šákja	k1gFnSc1	Šákja
a	a	k8xC	a
Malla	Malla	k1gFnSc1	Malla
byla	být	k5eAaImAgFnS	být
konfrontována	konfrontovat	k5eAaBmNgFnS	konfrontovat
s	s	k7c7	s
královskými	královský	k2eAgFnPc7d1	královská
říšemi	říš	k1gFnPc7	říš
jako	jako	k8xS	jako
Magadha	Magadha	k1gFnSc1	Magadha
<g/>
,	,	kIx,	,
Kóšala	Kóšala	k1gFnSc1	Kóšala
<g/>
,	,	kIx,	,
Vatsa	Vatsa	k1gFnSc1	Vatsa
a	a	k8xC	a
Avanti	avanti	k0	avanti
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
dědičnou	dědičný	k2eAgFnSc7d1	dědičná
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
republikách	republika	k1gFnPc6	republika
byl	být	k5eAaImAgInS	být
horní	horní	k2eAgFnSc7d1	horní
vrstvou	vrstva	k1gFnSc7	vrstva
kasty	kasta	k1gFnSc2	kasta
válečníků	válečník	k1gMnPc2	válečník
volen	volit	k5eAaImNgMnS	volit
regent	regent	k1gMnSc1	regent
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
řídit	řídit	k5eAaImF	řídit
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
poradního	poradní	k2eAgNnSc2d1	poradní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
podléhal	podléhat	k5eAaImAgMnS	podléhat
jeho	jeho	k3xOp3gFnSc3	jeho
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
platila	platit	k5eAaImAgFnS	platit
vůle	vůle	k1gFnSc1	vůle
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ještě	ještě	k9	ještě
v	v	k7c6	v
předhůří	předhůří	k1gNnSc6	předhůří
Himálaje	Himálaj	k1gFnSc2	Himálaj
přetrvávaly	přetrvávat	k5eAaImAgFnP	přetrvávat
republiky	republika	k1gFnPc1	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
úrodných	úrodný	k2eAgFnPc6d1	úrodná
pánvích	pánev	k1gFnPc6	pánev
řek	řeka	k1gFnPc2	řeka
Gangy	Ganga	k1gFnSc2	Ganga
a	a	k8xC	a
Jamuny	Jamun	k1gInPc1	Jamun
dominovaly	dominovat	k5eAaImAgInP	dominovat
monarchie	monarchie	k1gFnPc4	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
ke	k	k7c3	k
koncentraci	koncentrace	k1gFnSc3	koncentrace
moci	moc	k1gFnSc3	moc
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
osobu	osoba	k1gFnSc4	osoba
odrážel	odrážet	k5eAaImAgMnS	odrážet
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
převrat	převrat	k1gInSc4	převrat
<g/>
:	:	kIx,	:
jestliže	jestliže	k8xS	jestliže
společnost	společnost	k1gFnSc1	společnost
chovatelů	chovatel	k1gMnPc2	chovatel
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
poháněli	pohánět	k5eAaImAgMnP	pohánět
svá	svůj	k3xOyFgNnPc4	svůj
stáda	stádo	k1gNnPc4	stádo
přes	přes	k7c4	přes
volné	volný	k2eAgFnPc4d1	volná
pastviny	pastvina	k1gFnPc4	pastvina
<g/>
,	,	kIx,	,
nepotřebovala	potřebovat	k5eNaImAgFnS	potřebovat
silnou	silný	k2eAgFnSc4d1	silná
centrální	centrální	k2eAgFnSc4d1	centrální
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
podporovalo	podporovat	k5eAaImAgNnS	podporovat
naopak	naopak	k6eAd1	naopak
zemědělství	zemědělství	k1gNnSc1	zemědělství
postavení	postavení	k1gNnSc2	postavení
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
střežil	střežit	k5eAaImAgMnS	střežit
hranice	hranice	k1gFnPc4	hranice
polí	pole	k1gNnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Gautamově	Gautamově	k1gFnSc6	Gautamově
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
poslední	poslední	k2eAgFnPc1d1	poslední
republiky	republika	k1gFnPc1	republika
ještě	ještě	k9	ještě
omezeně	omezeně	k6eAd1	omezeně
autonomní	autonomní	k2eAgNnSc1d1	autonomní
<g/>
.	.	kIx.	.
</s>
<s>
Stály	stát	k5eAaImAgInP	stát
pod	pod	k7c7	pod
hegemonií	hegemonie	k1gFnSc7	hegemonie
monarchií	monarchie	k1gFnPc2	monarchie
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
navíc	navíc	k6eAd1	navíc
postoupily	postoupit	k5eAaPmAgFnP	postoupit
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
suverenitu	suverenita	k1gFnSc4	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
změny	změna	k1gFnPc1	změna
rozkolísaly	rozkolísat	k5eAaPmAgFnP	rozkolísat
normy	norma	k1gFnPc4	norma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
platily	platit	k5eAaImAgFnP	platit
celé	celý	k2eAgNnSc4d1	celé
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
<g/>
,	,	kIx,	,
a	a	k8xC	a
připravily	připravit	k5eAaPmAgFnP	připravit
změnu	změna	k1gFnSc4	změna
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
byla	být	k5eAaImAgFnS	být
patrná	patrný	k2eAgFnSc1d1	patrná
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
rovinách	rovina	k1gFnPc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
měnit	měnit	k5eAaImF	měnit
překonané	překonaný	k2eAgNnSc4d1	překonané
pořadí	pořadí	k1gNnSc4	pořadí
kast	kasta	k1gFnPc2	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
stavovsky	stavovsky	k6eAd1	stavovsky
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
do	do	k7c2	do
kast	kasta	k1gFnPc2	kasta
válečníků	válečník	k1gMnPc2	válečník
<g/>
,	,	kIx,	,
bráhmanů	bráhman	k1gMnPc2	bráhman
<g/>
,	,	kIx,	,
obchodníků	obchodník	k1gMnPc2	obchodník
a	a	k8xC	a
zemědělců	zemědělec	k1gMnPc2	zemědělec
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
kasta	kasta	k1gFnSc1	kasta
válečníků	válečník	k1gMnPc2	válečník
v	v	k7c6	v
republikách	republika	k1gFnPc6	republika
volila	volit	k5eAaImAgFnS	volit
regenty	regens	k1gMnPc7	regens
ze	z	k7c2	z
svého	své	k1gNnSc2	své
středu	střed	k1gInSc2	střed
a	a	k8xC	a
kolektivně	kolektivně	k6eAd1	kolektivně
střežila	střežit	k5eAaImAgFnS	střežit
jejich	jejich	k3xOp3gNnSc4	jejich
úřadování	úřadování	k1gNnSc4	úřadování
<g/>
,	,	kIx,	,
prohlašovala	prohlašovat	k5eAaImAgFnS	prohlašovat
se	se	k3xPyFc4	se
za	za	k7c4	za
horní	horní	k2eAgFnSc4d1	horní
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
přechod	přechod	k1gInSc1	přechod
moci	moct	k5eAaImF	moct
na	na	k7c4	na
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
předával	předávat	k5eAaImAgMnS	předávat
dědici	dědic	k1gMnSc3	dědic
<g/>
,	,	kIx,	,
znamenal	znamenat	k5eAaImAgMnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
významu	význam	k1gInSc2	význam
válečnického	válečnický	k2eAgInSc2d1	válečnický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
něho	on	k3xPp3gNnSc2	on
usilovali	usilovat	k5eAaImAgMnP	usilovat
bráhmani	bráhman	k1gMnPc1	bráhman
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
své	svůj	k3xOyFgFnSc2	svůj
kasty	kasta	k1gFnSc2	kasta
za	za	k7c4	za
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Bráhmani	bráhman	k1gMnPc1	bráhman
drželi	držet	k5eAaImAgMnP	držet
monopol	monopol	k1gInSc4	monopol
náboženského	náboženský	k2eAgInSc2d1	náboženský
kultu	kult	k1gInSc2	kult
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
praxe	praxe	k1gFnSc1	praxe
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c4	v
obětování	obětování	k1gNnSc4	obětování
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
kultovních	kultovní	k2eAgInPc2d1	kultovní
úkonů	úkon	k1gInPc2	úkon
<g/>
,	,	kIx,	,
hymnů	hymnus	k1gInPc2	hymnus
a	a	k8xC	a
svatých	svatý	k2eAgFnPc2d1	svatá
formulí	formule	k1gFnPc2	formule
se	se	k3xPyFc4	se
předával	předávat	k5eAaImAgMnS	předávat
jen	jen	k9	jen
uvnitř	uvnitř	k7c2	uvnitř
kasty	kasta	k1gFnSc2	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
bráhmani	bráhman	k1gMnPc1	bráhman
znali	znát	k5eAaImAgMnP	znát
védy	véd	k1gInPc7	véd
<g/>
,	,	kIx,	,
texty	text	k1gInPc7	text
zvěstování	zvěstování	k1gNnSc2	zvěstování
o	o	k7c6	o
bozích	bůh	k1gMnPc6	bůh
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
rituálech	rituál	k1gInPc6	rituál
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
přimět	přimět	k5eAaPmF	přimět
vyšší	vysoký	k2eAgFnPc4d2	vyšší
mocnosti	mocnost	k1gFnPc4	mocnost
k	k	k7c3	k
žádané	žádaný	k2eAgFnSc3d1	žádaná
pomoci	pomoc	k1gFnSc3	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
lidé	člověk	k1gMnPc1	člověk
nové	nový	k2eAgFnSc2d1	nová
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
a	a	k8xC	a
městské	městský	k2eAgFnSc2d1	městská
kultury	kultura	k1gFnSc2	kultura
už	už	k6eAd1	už
neměli	mít	k5eNaImAgMnP	mít
k	k	k7c3	k
božstvům	božstvo	k1gNnPc3	božstvo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
ztělesňovala	ztělesňovat	k5eAaImAgFnS	ztělesňovat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
bezprostřední	bezprostřední	k2eAgInSc4d1	bezprostřední
vztah	vztah	k1gInSc4	vztah
jako	jako	k8xS	jako
dřívější	dřívější	k2eAgFnSc2d1	dřívější
generace	generace	k1gFnSc2	generace
chovatelů	chovatel	k1gMnPc2	chovatel
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
požadavku	požadavek	k1gInSc6	požadavek
po	po	k7c6	po
zhodnocení	zhodnocení	k1gNnSc6	zhodnocení
svého	svůj	k3xOyFgNnSc2	svůj
sociálního	sociální	k2eAgNnSc2d1	sociální
postavení	postavení	k1gNnSc2	postavení
poznáváme	poznávat	k5eAaImIp1nP	poznávat
proto	proto	k8xC	proto
pokus	pokus	k1gInSc4	pokus
zajistit	zajistit	k5eAaPmF	zajistit
svému	svůj	k3xOyFgMnSc3	svůj
kněžskému	kněžský	k2eAgMnSc3d1	kněžský
stavu	stav	k1gInSc6	stav
vážnost	vážnost	k1gFnSc4	vážnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
už	už	k9	už
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
neměl	mít	k5eNaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
nesrozumitelný	srozumitelný	k2eNgInSc1d1	nesrozumitelný
rituál	rituál	k1gInSc1	rituál
a	a	k8xC	a
mýty	mýtus	k1gInPc1	mýtus
starých	starý	k2eAgMnPc2d1	starý
bohů	bůh	k1gMnPc2	bůh
nenabízely	nabízet	k5eNaImAgFnP	nabízet
odpovědi	odpověď	k1gFnPc1	odpověď
na	na	k7c4	na
existenciální	existenciální	k2eAgFnPc4d1	existenciální
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
převratné	převratný	k2eAgFnSc6d1	převratná
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
kladeny	klást	k5eAaImNgInP	klást
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
vznikalo	vznikat	k5eAaImAgNnS	vznikat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
kultu	kult	k1gInSc6	kult
bráhmanů	bráhman	k1gMnPc2	bráhman
množství	množství	k1gNnSc1	množství
nových	nový	k2eAgInPc2d1	nový
mytických	mytický	k2eAgInPc2d1	mytický
a	a	k8xC	a
filosofických	filosofický	k2eAgInPc2d1	filosofický
výkladů	výklad	k1gInPc2	výklad
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
nositeli	nositel	k1gMnSc3	nositel
a	a	k8xC	a
šiřiteli	šiřitel	k1gMnSc3	šiřitel
byli	být	k5eAaImAgMnP	být
šramani	šraman	k1gMnPc1	šraman
<g/>
,	,	kIx,	,
poutníci	poutník	k1gMnPc1	poutník
bez	bez	k7c2	bez
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
z	z	k7c2	z
darů	dar	k1gInPc2	dar
usedlého	usedlý	k2eAgNnSc2d1	usedlé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Stoupající	stoupající	k2eAgInPc1d1	stoupající
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
výnosy	výnos	k1gInPc1	výnos
<g/>
,	,	kIx,	,
řemesla	řemeslo	k1gNnPc1	řemeslo
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
ve	v	k7c6	v
městech	město	k1gNnPc6	město
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
živit	živit	k5eAaImF	živit
velké	velký	k2eAgNnSc1d1	velké
hnutí	hnutí	k1gNnSc1	hnutí
poutníků	poutník	k1gMnPc2	poutník
v	v	k7c6	v
bezdomoví	bezdomoví	k1gNnSc6	bezdomoví
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
usedlých	usedlý	k2eAgMnPc2d1	usedlý
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
nepovažovala	považovat	k5eNaImAgFnS	považovat
za	za	k7c4	za
obtížné	obtížný	k2eAgNnSc4d1	obtížné
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
přijímáni	přijímán	k2eAgMnPc1d1	přijímán
se	s	k7c7	s
zvědavostí	zvědavost	k1gFnSc7	zvědavost
a	a	k8xC	a
vážností	vážnost	k1gFnSc7	vážnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
<g/>
,	,	kIx,	,
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
změn	změna	k1gFnPc2	změna
byla	být	k5eAaImAgFnS	být
silná	silný	k2eAgFnSc1d1	silná
potřeba	potřeba	k1gFnSc1	potřeba
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
odvrhl	odvrhnout	k5eAaPmAgMnS	odvrhnout
rodinné	rodinný	k2eAgInPc4d1	rodinný
svazky	svazek	k1gInPc4	svazek
<g/>
,	,	kIx,	,
stavovské	stavovský	k2eAgFnPc4d1	stavovská
povinnosti	povinnost	k1gFnPc4	povinnost
a	a	k8xC	a
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Kruhy	kruh	k1gInPc1	kruh
šramanů	šraman	k1gInPc2	šraman
usilovaly	usilovat	k5eAaImAgInP	usilovat
o	o	k7c4	o
nová	nový	k2eAgNnPc4d1	nové
pořadí	pořadí	k1gNnPc4	pořadí
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nabízela	nabízet	k5eAaImAgFnS	nabízet
orientaci	orientace	k1gFnSc4	orientace
nejdříve	dříve	k6eAd3	dříve
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
tazatelům	tazatel	k1gMnPc3	tazatel
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
široké	široký	k2eAgFnSc3d1	široká
obci	obec	k1gFnSc3	obec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bráhmani	bráhman	k1gMnPc1	bráhman
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
svého	svůj	k3xOyFgNnSc2	svůj
uznání	uznání	k1gNnSc2	uznání
za	za	k7c4	za
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
vrstvu	vrstva	k1gFnSc4	vrstva
jen	jen	k9	jen
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
obětní	obětní	k2eAgInPc4d1	obětní
kulty	kult	k1gInPc4	kult
našli	najít	k5eAaPmAgMnP	najít
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
bázi	báze	k1gFnSc4	báze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
opírala	opírat	k5eAaImAgFnS	opírat
o	o	k7c6	o
učení	učení	k1gNnSc6	učení
šramanů	šraman	k1gInPc2	šraman
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
literatura	literatura	k1gFnSc1	literatura
upanišad	upanišada	k1gFnPc2	upanišada
<g/>
.	.	kIx.	.
</s>
<s>
Osobnost	osobnost	k1gFnSc1	osobnost
Gautamy	Gautam	k1gInPc1	Gautam
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
ohnisku	ohnisko	k1gNnSc6	ohnisko
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
<g/>
:	:	kIx,	:
jako	jako	k8xC	jako
příslušník	příslušník	k1gMnSc1	příslušník
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
válečnického	válečnický	k2eAgInSc2d1	válečnický
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
zažil	zažít	k5eAaPmAgMnS	zažít
pád	pád	k1gInSc4	pád
starého	starý	k2eAgInSc2d1	starý
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vůdce	vůdce	k1gMnSc1	vůdce
velkého	velký	k2eAgNnSc2d1	velké
hnutí	hnutí	k1gNnSc2	hnutí
mezi	mezi	k7c7	mezi
poutníky	poutník	k1gMnPc7	poutník
v	v	k7c6	v
bezdomoví	bezdomoví	k1gNnSc6	bezdomoví
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
kritice	kritika	k1gFnSc6	kritika
překonaných	překonaný	k2eAgFnPc2d1	překonaná
hodnost	hodnost	k1gFnSc4	hodnost
a	a	k8xC	a
na	na	k7c6	na
formulování	formulování	k1gNnSc6	formulování
nových	nový	k2eAgInPc2d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Gautamova	Gautamův	k2eAgFnSc1d1	Gautamův
rodina	rodina	k1gFnSc1	rodina
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
Šákjům	Šákj	k1gMnPc3	Šákj
<g/>
,	,	kIx,	,
rodu	rod	k1gInSc3	rod
válečníků	válečník	k1gMnPc2	válečník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
Šákja	Šákj	k1gInSc2	Šákj
ztratila	ztratit	k5eAaPmAgFnS	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
plnou	plný	k2eAgFnSc4d1	plná
suverenitu	suverenita	k1gFnSc4	suverenita
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
poslušna	poslušen	k2eAgFnSc1d1	poslušna
monarchy	monarcha	k1gMnSc2	monarcha
z	z	k7c2	z
Kóšaly	Kóšala	k1gFnSc2	Kóšala
<g/>
.	.	kIx.	.
</s>
<s>
Gautamův	Gautamův	k2eAgMnSc1d1	Gautamův
otec	otec	k1gMnSc1	otec
Šuddhódana	Šuddhódan	k1gMnSc4	Šuddhódan
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
zvolený	zvolený	k2eAgMnSc1d1	zvolený
regent	regent	k1gMnSc1	regent
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Kapilavastu	Kapilavast	k1gInSc2	Kapilavast
pověřen	pověřit	k5eAaPmNgInS	pověřit
vedením	vedení	k1gNnSc7	vedení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jako	jako	k9	jako
okleštěná	okleštěný	k2eAgFnSc1d1	okleštěná
autonomie	autonomie	k1gFnSc1	autonomie
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
předsednictvím	předsednictví	k1gNnSc7	předsednictví
civilní	civilní	k2eAgFnSc2d1	civilní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
vybíráním	vybírání	k1gNnSc7	vybírání
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
soudnictvím	soudnictví	k1gNnSc7	soudnictví
<g/>
.	.	kIx.	.
</s>
<s>
Regent	regent	k1gMnSc1	regent
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgMnS	být
označován	označovat	k5eAaImNgMnS	označovat
slovem	slovem	k6eAd1	slovem
rádža	rádža	k1gMnSc1	rádža
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
panovníci	panovník	k1gMnPc1	panovník
dědičné	dědičný	k2eAgFnSc2d1	dědičná
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
legendě	legenda	k1gFnSc3	legenda
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gautama	Gautam	k1gMnSc2	Gautam
byl	být	k5eAaImAgMnS	být
královským	královský	k2eAgMnSc7d1	královský
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
epochy	epocha	k1gFnPc1	epocha
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
spojovaly	spojovat	k5eAaImAgInP	spojovat
své	svůj	k3xOyFgFnPc4	svůj
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
životě	život	k1gInSc6	život
prince	princ	k1gMnSc2	princ
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
paláce	palác	k1gInPc1	palác
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
mluví	mluvit	k5eAaImIp3nS	mluvit
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
sotva	sotva	k6eAd1	sotva
něco	něco	k3yInSc4	něco
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
cihlové	cihlový	k2eAgFnPc1d1	cihlová
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
hliněných	hliněný	k2eAgInPc2d1	hliněný
domů	dům	k1gInPc2	dům
ve	v	k7c6	v
městě	město	k1gNnSc6	město
lišily	lišit	k5eAaImAgFnP	lišit
jen	jen	k6eAd1	jen
rozměry	rozměra	k1gFnSc2	rozměra
-	-	kIx~	-
délkou	délka	k1gFnSc7	délka
a	a	k8xC	a
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
svých	svůj	k3xOyFgInPc2	svůj
politických	politický	k2eAgInPc2d1	politický
úkolů	úkol	k1gInPc2	úkol
vedl	vést	k5eAaImAgMnS	vést
rádža	rádža	k1gMnSc1	rádža
často	často	k6eAd1	často
také	také	k9	také
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domě	dům	k1gInSc6	dům
rádži	rádža	k1gMnSc2	rádža
z	z	k7c2	z
Kapilavastu	Kapilavast	k1gInSc2	Kapilavast
bydlela	bydlet	k5eAaImAgFnS	bydlet
velká	velký	k2eAgFnSc1d1	velká
rodina	rodina	k1gFnSc1	rodina
<g/>
:	:	kIx,	:
Šuddhódana	Šuddhódana	k1gFnSc1	Šuddhódana
byl	být	k5eAaImAgInS	být
ženatý	ženatý	k2eAgInSc1d1	ženatý
se	s	k7c7	s
sestrami	sestra	k1gFnPc7	sestra
Májou	Mája	k1gFnSc7	Mája
a	a	k8xC	a
Mahápradžápatí	Mahápradžápatý	k2eAgMnPc1d1	Mahápradžápatý
Gautamí	Gautamí	k1gNnSc2	Gautamí
<g/>
,	,	kIx,	,
příslušnicemi	příslušnice	k1gFnPc7	příslušnice
rodu	rod	k1gInSc2	rod
Šákjů	Šákj	k1gInPc2	Šákj
<g/>
,	,	kIx,	,
v	v	k7c6	v
domě	dům	k1gInSc6	dům
tedy	tedy	k8xC	tedy
žili	žít	k5eAaImAgMnP	žít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rádžovy	rádžův	k2eAgFnPc4d1	rádžův
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
další	další	k2eAgMnPc1d1	další
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Májou	Mája	k1gFnSc7	Mája
měl	mít	k5eAaImAgMnS	mít
dítě	dítě	k1gNnSc4	dítě
Gautamu	Gautam	k1gInSc2	Gautam
<g/>
,	,	kIx,	,
s	s	k7c7	s
Mahápradžápatí	Mahápradžápatý	k2eAgMnPc1d1	Mahápradžápatý
Gautamí	Gautamí	k1gNnSc6	Gautamí
syna	syn	k1gMnSc4	syn
Nandu	nandu	k1gMnSc4	nandu
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Sundarínandu	Sundarínand	k1gInSc2	Sundarínand
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
členů	člen	k1gInPc2	člen
rodiny	rodina	k1gFnSc2	rodina
patřila	patřit	k5eAaImAgFnS	patřit
ke	k	k7c3	k
dvoru	dvůr	k1gInSc3	dvůr
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
rádžově	rádžův	k2eAgInSc6d1	rádžův
blahobytu	blahobyt	k1gInSc6	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
královně	královna	k1gFnSc3	královna
Máje	máj	k1gFnSc2	máj
se	se	k3xPyFc4	se
před	před	k7c7	před
Buddhovým	Buddhův	k2eAgNnSc7d1	Buddhovo
početím	početí	k1gNnSc7	početí
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
do	do	k7c2	do
pravého	pravý	k2eAgInSc2d1	pravý
boku	bok	k1gInSc2	bok
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
bílý	bílý	k1gMnSc1	bílý
slon	slon	k1gMnSc1	slon
<g/>
.	.	kIx.	.
</s>
<s>
Královští	královský	k2eAgMnPc1d1	královský
věštci	věštec	k1gMnPc1	věštec
vyložili	vyložit	k5eAaPmAgMnP	vyložit
sen	sen	k1gInSc4	sen
jako	jako	k8xC	jako
znamení	znamení	k1gNnSc4	znamení
<g/>
,	,	kIx,	,
že	že	k8xS	že
královna	královna	k1gFnSc1	královna
porodí	porodit	k5eAaPmIp3nS	porodit
výjimečného	výjimečný	k2eAgMnSc4d1	výjimečný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
klasických	klasický	k2eAgInPc2d1	klasický
textů	text	k1gInPc2	text
se	se	k3xPyFc4	se
Gautama	Gautamum	k1gNnSc2	Gautamum
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
Mája	Mája	k1gFnSc1	Mája
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Kapilavastu	Kapilavast	k1gInSc2	Kapilavast
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
otcovského	otcovský	k2eAgInSc2d1	otcovský
domu	dům	k1gInSc2	dům
<g/>
:	:	kIx,	:
během	během	k7c2	během
odpočinku	odpočinek	k1gInSc2	odpočinek
v	v	k7c6	v
háji	háj	k1gInSc6	háj
u	u	k7c2	u
Lumbiní	Lumbiní	k1gFnSc2	Lumbiní
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc1	vesnice
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
porodu	porod	k1gInSc3	porod
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
se	se	k3xPyFc4	se
zpříma	zpříma	k6eAd1	zpříma
stojící	stojící	k2eAgFnSc1d1	stojící
a	a	k8xC	a
jásavě	jásavě	k6eAd1	jásavě
zpívající	zpívající	k2eAgFnSc1d1	zpívající
matka	matka	k1gFnSc1	matka
držela	držet	k5eAaImAgFnS	držet
větve	větev	k1gFnPc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
dostal	dostat	k5eAaPmAgInS	dostat
jméno	jméno	k1gNnSc4	jméno
Siddhártha	Siddhárth	k1gMnSc2	Siddhárth
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
dní	den	k1gInPc2	den
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
Mája	Mája	k1gFnSc1	Mája
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
sestra	sestra	k1gFnSc1	sestra
Mahápradžápatí	Mahápradžápatý	k2eAgMnPc1d1	Mahápradžápatý
Gautamí	Gautamí	k1gNnSc2	Gautamí
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
jen	jen	k9	jen
o	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
mladší	mladý	k2eAgFnSc1d2	mladší
než	než	k8xS	než
Gautama	Gautama	k1gFnSc1	Gautama
<g/>
,	,	kIx,	,
nahradila	nahradit	k5eAaPmAgFnS	nahradit
dítěti	dítě	k1gNnSc3	dítě
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
prožil	prožít	k5eAaPmAgInS	prožít
dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
v	v	k7c6	v
dobovém	dobový	k2eAgInSc6d1	dobový
luxusu	luxus	k1gInSc6	luxus
<g/>
:	:	kIx,	:
Žil	žít	k5eAaImAgMnS	žít
jsem	být	k5eAaImIp1nS	být
zhýčkaně	zhýčkaně	k6eAd1	zhýčkaně
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
zhýčkaně	zhýčkaně	k6eAd1	zhýčkaně
<g/>
,	,	kIx,	,
nanejvýš	nanejvýš	k6eAd1	nanejvýš
zhýčkaně	zhýčkaně	k6eAd1	zhýčkaně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
domu	dům	k1gInSc2	dům
mého	můj	k3xOp1gMnSc2	můj
otce	otec	k1gMnSc2	otec
mi	já	k3xPp1nSc3	já
nechali	nechat	k5eAaPmAgMnP	nechat
zřídit	zřídit	k5eAaPmF	zřídit
lotosové	lotosový	k2eAgInPc4d1	lotosový
rybníky	rybník	k1gInPc4	rybník
<g/>
:	:	kIx,	:
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
kvetly	kvést	k5eAaImAgFnP	kvést
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
,	,	kIx,	,
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
na	na	k7c6	na
jiném	jiné	k1gNnSc6	jiné
červené	červený	k2eAgInPc4d1	červený
lotosové	lotosový	k2eAgInPc4d1	lotosový
květy	květ	k1gInPc4	květ
<g/>
;	;	kIx,	;
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
jen	jen	k9	jen
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
</s>
<s>
Nepoužíval	používat	k5eNaImAgMnS	používat
jsem	být	k5eAaImIp1nS	být
jiné	jiný	k2eAgFnSc3d1	jiná
masti	mast	k1gFnSc3	mast
než	než	k8xS	než
z	z	k7c2	z
Benaresu	Benares	k1gInSc2	Benares
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Benaresu	Benares	k1gInSc2	Benares
pocházel	pocházet	k5eAaImAgInS	pocházet
šátek	šátek	k1gInSc1	šátek
<g/>
,	,	kIx,	,
pokrývající	pokrývající	k2eAgFnSc1d1	pokrývající
mou	můj	k3xOp1gFnSc4	můj
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
kazajka	kazajka	k1gFnSc1	kazajka
<g/>
,	,	kIx,	,
mé	můj	k3xOp1gNnSc4	můj
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
<g/>
,	,	kIx,	,
můj	můj	k3xOp1gInSc1	můj
přehoz	přehoz	k1gInSc1	přehoz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dne	den	k1gInSc2	den
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
nade	nad	k7c7	nad
mnou	já	k3xPp1nSc7	já
drželi	držet	k5eAaImAgMnP	držet
bílý	bílý	k2eAgInSc4d1	bílý
deštník	deštník	k1gInSc4	deštník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mě	já	k3xPp1nSc4	já
neobtěžoval	obtěžovat	k5eNaImAgInS	obtěžovat
chlad	chlad	k1gInSc1	chlad
<g/>
,	,	kIx,	,
horko	horko	k1gNnSc1	horko
<g/>
,	,	kIx,	,
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
stébla	stéblo	k1gNnSc2	stéblo
trávy	tráva	k1gFnSc2	tráva
nebo	nebo	k8xC	nebo
rosa	rosa	k1gFnSc1	rosa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	let	k1gInPc6	let
oženili	oženit	k5eAaPmAgMnP	oženit
Gautamu	Gautam	k1gInSc2	Gautam
s	s	k7c7	s
šákijskou	šákijský	k2eAgFnSc7d1	šákijský
dívkou	dívka	k1gFnSc7	dívka
Jašódharou	Jašódharý	k2eAgFnSc7d1	Jašódharý
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bude	být	k5eAaImBp3nS	být
učit	učit	k5eAaImF	učit
<g/>
:	:	kIx,	:
Neznám	znát	k5eNaImIp1nS	znát
žádné	žádný	k3yNgNnSc4	žádný
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc4	žádný
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
žádnou	žádný	k3yNgFnSc4	žádný
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
žádnou	žádný	k3yNgFnSc4	žádný
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
dotek	dotek	k1gInSc1	dotek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
poutají	poutat	k5eAaImIp3nP	poutat
mysl	mysl	k1gFnSc4	mysl
muže	muž	k1gMnSc2	muž
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
a	a	k8xC	a
dotek	dotek	k1gInSc1	dotek
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Neznám	neznat	k5eAaImIp1nS	neznat
žádné	žádný	k3yNgNnSc4	žádný
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc4	žádný
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
žádnou	žádný	k3yNgFnSc4	žádný
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
žádnou	žádný	k3yNgFnSc4	žádný
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
dotek	dotek	k1gInSc1	dotek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
poutají	poutat	k5eAaImIp3nP	poutat
mysl	mysl	k1gFnSc4	mysl
ženy	žena	k1gFnSc2	žena
tak	tak	k9	tak
jako	jako	k8xS	jako
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
a	a	k8xC	a
dotek	dotek	k1gInSc1	dotek
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
mládí	mládí	k1gNnSc6	mládí
neoddával	oddávat	k5eNaImAgInS	oddávat
jen	jen	k6eAd1	jen
požitkům	požitek	k1gInPc3	požitek
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
zabývat	zabývat	k5eAaImF	zabývat
právem	právo	k1gNnSc7	právo
a	a	k8xC	a
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
prokázal	prokázat	k5eAaPmAgInS	prokázat
jako	jako	k9	jako
zkušený	zkušený	k2eAgMnSc1d1	zkušený
organizátor	organizátor	k1gMnSc1	organizátor
a	a	k8xC	a
zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
své	svůj	k3xOyFgFnSc2	svůj
obce	obec	k1gFnSc2	obec
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
šikovný	šikovný	k2eAgMnSc1d1	šikovný
taktik	taktik	k1gMnSc1	taktik
v	v	k7c6	v
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
své	svůj	k3xOyFgFnSc2	svůj
nauky	nauka	k1gFnSc2	nauka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
povinnostech	povinnost	k1gFnPc6	povinnost
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
mládí	mládí	k1gNnSc2	mládí
byl	být	k5eAaImAgMnS	být
Gautama	Gautamum	k1gNnSc2	Gautamum
zcela	zcela	k6eAd1	zcela
zaujat	zaujmout	k5eAaPmNgMnS	zaujmout
pomíjivostí	pomíjivost	k1gFnSc7	pomíjivost
všech	všecek	k3xTgFnPc2	všecek
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
ještě	ještě	k6eAd1	ještě
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
přijímal	přijímat	k5eAaImAgMnS	přijímat
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
co	co	k8xS	co
vstupovalo	vstupovat	k5eAaImAgNnS	vstupovat
do	do	k7c2	do
vědomí	vědomí	k1gNnSc2	vědomí
pěti	pět	k4xCc3	pět
smysly	smysl	k1gInPc4	smysl
<g/>
:	:	kIx,	:
zrak	zrak	k1gInSc4	zrak
těšily	těšit	k5eAaImAgFnP	těšit
viditelné	viditelný	k2eAgFnPc1d1	viditelná
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
ucho	ucho	k1gNnSc1	ucho
tóny	tón	k1gInPc4	tón
<g/>
,	,	kIx,	,
čich	čich	k1gInSc4	čich
vůně	vůně	k1gFnSc2	vůně
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc4	chuť
šťávy	šťáva	k1gFnSc2	šťáva
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc1	tělo
doteky	dotek	k1gInPc4	dotek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vítané	vítaný	k2eAgNnSc1d1	vítané
<g/>
,	,	kIx,	,
milé	milý	k2eAgNnSc1d1	milé
<g/>
,	,	kIx,	,
příjemné	příjemný	k2eAgNnSc1d1	příjemné
a	a	k8xC	a
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Po	po	k7c4	po
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
období	období	k1gNnSc4	období
dešťů	dešť	k1gInPc2	dešť
mě	já	k3xPp1nSc2	já
obklopovaly	obklopovat	k5eAaImAgFnP	obklopovat
hudebnice	hudebnice	k1gFnSc1	hudebnice
a	a	k8xC	a
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
neopouštěl	opouštět	k5eNaImAgMnS	opouštět
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jsem	být	k5eAaImIp1nS	být
poznal	poznat	k5eAaPmAgMnS	poznat
podle	podle	k7c2	podle
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
smyslové	smyslový	k2eAgFnPc1d1	smyslová
radosti	radost	k1gFnPc1	radost
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
štěstím	štěstí	k1gNnSc7	štěstí
a	a	k8xC	a
utrpením	utrpení	k1gNnSc7	utrpení
a	a	k8xC	a
jak	jak	k6eAd1	jak
jim	on	k3xPp3gFnPc3	on
lze	lze	k6eAd1	lze
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yRnSc6	co
zde	zde	k6eAd1	zde
Gautama	Gautama	k1gFnSc1	Gautama
promlouvá	promlouvat	k5eAaImIp3nS	promlouvat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
aspektem	aspekt	k1gInSc7	aspekt
nauky	nauka	k1gFnSc2	nauka
<g/>
:	:	kIx,	:
pomíjivost	pomíjivost	k1gFnSc1	pomíjivost
všech	všecek	k3xTgFnPc2	všecek
věcí	věc	k1gFnPc2	věc
kalí	kalit	k5eAaImIp3nS	kalit
radost	radost	k1gFnSc4	radost
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
a	a	k8xC	a
činí	činit	k5eAaImIp3nP	činit
je	on	k3xPp3gFnPc4	on
strastiplnými	strastiplný	k2eAgFnPc7d1	strastiplná
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
prohlédne	prohlédnout	k5eAaPmIp3nS	prohlédnout
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
postřehu	postřeh	k1gInSc3	postřeh
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgInPc1	žádný
prožitky	prožitek	k1gInPc1	prožitek
a	a	k8xC	a
věci	věc	k1gFnPc1	věc
nejsou	být	k5eNaImIp3nP	být
trvalé	trvalý	k2eAgFnPc1d1	trvalá
<g/>
,	,	kIx,	,
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
také	také	k9	také
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
jako	jako	k8xC	jako
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
pomíjivý	pomíjivý	k2eAgMnSc1d1	pomíjivý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zhýčkaném	zhýčkaný	k2eAgInSc6d1	zhýčkaný
životě	život	k1gInSc6	život
mě	já	k3xPp1nSc4	já
napadla	napadnout	k5eAaPmAgFnS	napadnout
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Nevědomý	vědomý	k2eNgMnSc1d1	nevědomý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
podroben	podroben	k2eAgMnSc1d1	podroben
stáří	stáří	k1gNnSc4	stáří
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
mohl	moct	k5eAaImAgMnS	moct
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sklíčen	sklíčit	k5eAaPmNgMnS	sklíčit
<g/>
,	,	kIx,	,
zděšen	zděsit	k5eAaPmNgMnS	zděsit
a	a	k8xC	a
zhnusen	zhnusen	k2eAgMnSc1d1	zhnusen
<g/>
,	,	kIx,	,
když	když	k8xS	když
vidí	vidět	k5eAaImIp3nP	vidět
starce	stařec	k1gMnSc4	stařec
<g/>
,	,	kIx,	,
nemocného	nemocný	k1gMnSc4	nemocný
<g/>
,	,	kIx,	,
mrtvého	mrtvý	k1gMnSc4	mrtvý
<g/>
;	;	kIx,	;
avšak	avšak	k8xC	avšak
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
nemyslí	myslet	k5eNaImIp3nS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
já	já	k3xPp1nSc1	já
podléhám	podléhat	k5eAaImIp1nS	podléhat
stáří	stáří	k1gNnSc3	stáří
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
,	,	kIx,	,
smrti	smrt	k1gFnSc3	smrt
a	a	k8xC	a
nemohu	moct	k5eNaImIp1nS	moct
jim	on	k3xPp3gMnPc3	on
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
by	by	kYmCp3nS	by
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
staré	starý	k2eAgNnSc4d1	staré
<g/>
,	,	kIx,	,
nemocné	nemocný	k2eAgNnSc4d1	nemocný
<g/>
,	,	kIx,	,
mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
nebyl	být	k5eNaImAgMnS	být
sklíčen	sklíčen	k2eAgMnSc1d1	sklíčen
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podléhám	podléhat	k5eAaImIp1nS	podléhat
stáří	stáří	k1gNnSc4	stáří
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
nemohu	moct	k5eNaImIp1nS	moct
jim	on	k3xPp3gMnPc3	on
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zatím	zatím	k6eAd1	zatím
co	co	k9	co
jsem	být	k5eAaImIp1nS	být
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
<g/>
,	,	kIx,	,
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
mé	můj	k3xOp1gNnSc1	můj
opojení	opojení	k1gNnSc2	opojení
mládím	mládí	k1gNnSc7	mládí
<g/>
,	,	kIx,	,
zdravím	zdraví	k1gNnSc7	zdraví
a	a	k8xC	a
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vnímání	vnímání	k1gNnSc3	vnímání
subjektivní	subjektivní	k2eAgFnSc2d1	subjektivní
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
a	a	k8xC	a
prchavého	prchavý	k2eAgInSc2d1	prchavý
charakteru	charakter	k1gInSc2	charakter
všech	všecek	k3xTgFnPc2	všecek
věcí	věc	k1gFnPc2	věc
stály	stát	k5eAaImAgInP	stát
spontánní	spontánní	k2eAgInPc1d1	spontánní
prožitky	prožitek	k1gInPc1	prožitek
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
svobody	svoboda	k1gFnSc2	svoboda
<g/>
:	:	kIx,	:
Vzpomínám	vzpomínat	k5eAaImIp1nS	vzpomínat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
Šákja	Šákja	k1gMnSc1	Šákja
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
<g/>
,	,	kIx,	,
seděl	sedět	k5eAaImAgMnS	sedět
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
chladivém	chladivý	k2eAgInSc6d1	chladivý
stínu	stín	k1gInSc6	stín
jambovníku	jambovník	k1gInSc2	jambovník
<g/>
.	.	kIx.	.
</s>
<s>
Daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
neprospěšných	prospěšný	k2eNgFnPc2d1	neprospěšná
věcí	věc	k1gFnPc2	věc
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
jsem	být	k5eAaImIp1nS	být
radosti	radost	k1gFnPc4	radost
a	a	k8xC	a
štěstí	štěstí	k1gNnSc4	štěstí
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautam	k1gMnSc4	Gautam
nechtěl	chtít	k5eNaImAgMnS	chtít
prožívat	prožívat	k5eAaImF	prožívat
jen	jen	k9	jen
náhodně	náhodně	k6eAd1	náhodně
takové	takový	k3xDgInPc4	takový
okamžiky	okamžik	k1gInPc4	okamžik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
už	už	k6eAd1	už
pomíjivost	pomíjivost	k1gFnSc4	pomíjivost
světa	svět	k1gInSc2	svět
a	a	k8xC	a
vlastní	vlastní	k2eAgFnPc1d1	vlastní
bytosti	bytost	k1gFnPc1	bytost
nehrají	hrát	k5eNaImIp3nP	hrát
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kontrolou	kontrola	k1gFnSc7	kontrola
a	a	k8xC	a
nakonec	nakonec	k9	nakonec
překonáním	překonání	k1gNnSc7	překonání
každé	každý	k3xTgFnSc2	každý
žádosti	žádost	k1gFnSc2	žádost
ujde	ujít	k5eAaPmIp3nS	ujít
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
touhy	touha	k1gFnSc2	touha
po	po	k7c6	po
pomíjivých	pomíjivý	k2eAgFnPc6d1	pomíjivá
radostech	radost	k1gFnPc6	radost
<g/>
:	:	kIx,	:
Bylo	být	k5eAaImAgNnS	být
mi	já	k3xPp1nSc3	já
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
:	:	kIx,	:
štěstí	štěstí	k1gNnSc1	štěstí
a	a	k8xC	a
radost	radost	k1gFnSc1	radost
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
světě	svět	k1gInSc6	svět
skrze	skrze	k?	skrze
prožitky	prožitek	k1gInPc4	prožitek
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
je	být	k5eAaImIp3nS	být
ubohé	ubohý	k2eAgNnSc1d1	ubohé
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
pomíjivý	pomíjivý	k2eAgInSc1d1	pomíjivý
<g/>
,	,	kIx,	,
strastiplný	strastiplný	k2eAgInSc1d1	strastiplný
a	a	k8xC	a
podléhá	podléhat	k5eAaImIp3nS	podléhat
změně	změna	k1gFnSc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Únik	únik	k1gInSc1	únik
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
je	být	k5eAaImIp3nS	být
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
a	a	k8xC	a
vzdání	vzdání	k1gNnSc4	vzdání
se	se	k3xPyFc4	se
žádosti	žádost	k1gFnSc2	žádost
<g/>
.	.	kIx.	.
</s>
<s>
Gautamovo	Gautamův	k2eAgNnSc4d1	Gautamovo
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
prchavosti	prchavost	k1gFnSc2	prchavost
všech	všecek	k3xTgInPc2	všecek
fenoménů	fenomén	k1gInPc2	fenomén
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
ustání	ustání	k1gNnSc6	ustání
žádosti	žádost	k1gFnSc2	žádost
neprozrazují	prozrazovat	k5eNaImIp3nP	prozrazovat
žádnou	žádný	k3yNgFnSc4	žádný
rezignaci	rezignace	k1gFnSc4	rezignace
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
naopak	naopak	k6eAd1	naopak
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
vznikáním	vznikání	k1gNnSc7	vznikání
a	a	k8xC	a
zanikáním	zanikání	k1gNnSc7	zanikání
jsou	být	k5eAaImIp3nP	být
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
cennější	cenný	k2eAgFnPc1d2	cennější
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
:	:	kIx,	:
Toužil	toužit	k5eAaImAgInS	toužit
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
podléhá	podléhat	k5eAaImIp3nS	podléhat
zákonu	zákon	k1gInSc3	zákon
zrození	zrození	k1gNnSc4	zrození
<g/>
,	,	kIx,	,
stárnutí	stárnutí	k1gNnSc4	stárnutí
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc4	nemoc
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
starostí	starost	k1gFnPc2	starost
a	a	k8xC	a
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jsem	být	k5eAaImIp1nS	být
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
<g/>
:	:	kIx,	:
proč	proč	k6eAd1	proč
usiluji	usilovat	k5eAaImIp1nS	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
podléhá	podléhat	k5eAaImIp3nS	podléhat
tomuto	tento	k3xDgInSc3	tento
zákonu	zákon	k1gInSc3	zákon
<g/>
?	?	kIx.	?
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
bych	by	kYmCp1nS	by
raději	rád	k6eAd2	rád
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
tomuto	tento	k3xDgInSc3	tento
zákonu	zákon	k1gInSc3	zákon
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
<g/>
,	,	kIx,	,
o	o	k7c4	o
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsem	být	k5eAaImIp1nS	být
poznal	poznat	k5eAaPmAgMnS	poznat
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
toho	ten	k3xDgNnSc2	ten
všeho	všecek	k3xTgNnSc2	všecek
<g/>
?	?	kIx.	?
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uprostřed	uprostřed	k7c2	uprostřed
rodinných	rodinný	k2eAgInPc2d1	rodinný
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
povinností	povinnost	k1gFnPc2	povinnost
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
sotva	sotva	k6eAd1	sotva
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
jeho	jeho	k3xOp3gFnSc1	jeho
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
pomíjivosti	pomíjivost	k1gFnSc3	pomíjivost
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
jsem	být	k5eAaImIp1nS	být
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
<g/>
:	:	kIx,	:
dům	dům	k1gInSc4	dům
je	být	k5eAaImIp3nS	být
vězení	vězení	k1gNnSc1	vězení
<g/>
,	,	kIx,	,
špinavé	špinavý	k2eAgNnSc1d1	špinavé
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Šramana	Šraman	k1gMnSc2	Šraman
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Nemám	mít	k5eNaImIp1nS	mít
si	se	k3xPyFc3	se
ostříhat	ostříhat	k5eAaPmF	ostříhat
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
vousy	vous	k1gInPc4	vous
<g/>
,	,	kIx,	,
obléci	obléct	k5eAaPmF	obléct
žluté	žlutý	k2eAgNnSc4d1	žluté
roucho	roucho	k1gNnSc4	roucho
a	a	k8xC	a
putovat	putovat	k5eAaImF	putovat
do	do	k7c2	do
bezdomoví	bezdomoví	k1gNnSc2	bezdomoví
<g/>
?	?	kIx.	?
</s>
<s>
Rádžova	rádžův	k2eAgFnSc1d1	rádžův
rodina	rodina	k1gFnSc1	rodina
snad	snad	k9	snad
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
plány	plán	k1gInPc7	plán
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
jen	jen	k9	jen
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
existence	existence	k1gFnSc2	existence
dědice	dědic	k1gMnSc2	dědic
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Gautama	Gautama	k1gNnSc4	Gautama
provedl	provést	k5eAaPmAgMnS	provést
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
syna	syn	k1gMnSc2	syn
Ráhuly	Ráhula	k1gFnSc2	Ráhula
<g/>
:	:	kIx,	:
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
ještě	ještě	k6eAd1	ještě
mladý	mladý	k2eAgMnSc1d1	mladý
a	a	k8xC	a
tmavovlasý	tmavovlasý	k2eAgMnSc1d1	tmavovlasý
<g/>
,	,	kIx,	,
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
síle	síla	k1gFnSc6	síla
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
proti	proti	k7c3	proti
vůli	vůle	k1gFnSc3	vůle
mých	můj	k3xOp1gMnPc2	můj
naříkajících	naříkající	k2eAgMnPc2d1	naříkající
rodičů	rodič	k1gMnPc2	rodič
ostříhat	ostříhat	k5eAaPmF	ostříhat
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
vousy	vous	k1gInPc4	vous
<g/>
,	,	kIx,	,
oblékl	obléct	k5eAaPmAgMnS	obléct
jsem	být	k5eAaImIp1nS	být
žluté	žlutý	k2eAgNnSc4d1	žluté
roucho	roucho	k1gNnSc4	roucho
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
bezdomoví	bezdomoví	k1gNnSc2	bezdomoví
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
bylo	být	k5eAaImAgNnS	být
Gautamovi	Gautam	k1gMnSc3	Gautam
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gInSc2	on
odchodu	odchod	k1gInSc2	odchod
29	[number]	k4	29
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
také	také	k9	také
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
známá	známý	k2eAgFnSc1d1	známá
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
znameních	znamení	k1gNnPc6	znamení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
mladému	mladý	k1gMnSc3	mladý
Gautamovi	Gautam	k1gMnSc3	Gautam
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
vyjevila	vyjevit	k5eAaPmAgFnS	vyjevit
strastiplnost	strastiplnost	k1gFnSc4	strastiplnost
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgMnSc1d2	pozdější
Buddha	Buddha	k1gMnSc1	Buddha
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
zábavných	zábavný	k2eAgFnPc6d1	zábavná
jízdách	jízda	k1gFnPc6	jízda
třikrát	třikrát	k6eAd1	třikrát
hluboce	hluboko	k6eAd1	hluboko
otřesen	otřást	k5eAaPmNgInS	otřást
setkáním	setkání	k1gNnSc7	setkání
s	s	k7c7	s
pomíjivostí	pomíjivost	k1gFnSc7	pomíjivost
<g/>
.	.	kIx.	.
</s>
<s>
Spatří	spatřit	k5eAaPmIp3nS	spatřit
starého	starý	k1gMnSc4	starý
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
zlomeného	zlomený	k2eAgMnSc4d1	zlomený
<g/>
,	,	kIx,	,
zkřiveného	zkřivený	k2eAgMnSc4d1	zkřivený
<g/>
,	,	kIx,	,
opírajícího	opírající	k2eAgMnSc4d1	opírající
se	se	k3xPyFc4	se
o	o	k7c6	o
berle	berla	k1gFnSc6	berla
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vrávoravě	vrávoravě	k6eAd1	vrávoravě
vleče	vléct	k5eAaImIp3nS	vléct
<g/>
,	,	kIx,	,
neduživý	duživý	k2eNgInSc1d1	neduživý
a	a	k8xC	a
vrásčitý	vrásčitý	k2eAgInSc1d1	vrásčitý
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vidí	vidět	k5eAaImIp3nS	vidět
nemocného	nemocný	k1gMnSc4	nemocný
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
leží	ležet	k5eAaImIp3nS	ležet
pošpiněn	pošpinit	k5eAaPmNgInS	pošpinit
zvratky	zvratek	k1gInPc7	zvratek
a	a	k8xC	a
močí	moč	k1gFnSc7	moč
<g/>
,	,	kIx,	,
ošetřován	ošetřován	k2eAgInSc4d1	ošetřován
a	a	k8xC	a
nošen	nošen	k2eAgInSc4d1	nošen
jinými	jiný	k2eAgNnPc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
pohřebnímu	pohřební	k2eAgInSc3d1	pohřební
průvodu	průvod	k1gInSc3	průvod
konfrontován	konfrontován	k2eAgMnSc1d1	konfrontován
s	s	k7c7	s
mrtvolou	mrtvola	k1gFnSc7	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kočím	kočí	k1gMnSc7	kočí
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
nemůže	moct	k5eNaImIp3nS	moct
uniknout	uniknout	k5eAaPmF	uniknout
stáří	stáří	k1gNnSc3	stáří
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnSc3	nemoc
a	a	k8xC	a
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
rádža	rádža	k1gMnSc1	rádža
vyplní	vyplnit	k5eAaPmIp3nS	vyplnit
každé	každý	k3xTgNnSc4	každý
přání	přání	k1gNnSc4	přání
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
zadržel	zadržet	k5eAaPmAgMnS	zadržet
<g/>
,	,	kIx,	,
setkání	setkání	k1gNnPc4	setkání
se	s	k7c7	s
šramanou	šramaný	k2eAgFnSc7d1	šramaný
ho	on	k3xPp3gMnSc4	on
podnítí	podnítit	k5eAaPmIp3nP	podnítit
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
do	do	k7c2	do
bezdomoví	bezdomoví	k1gNnSc2	bezdomoví
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgNnPc4	tento
vyprávění	vyprávění	k1gNnPc4	vyprávění
vyplynula	vyplynout	k5eAaPmAgFnS	vyplynout
z	z	k7c2	z
Gautamova	Gautamův	k2eAgNnSc2d1	Gautamovo
pozorování	pozorování	k1gNnSc2	pozorování
a	a	k8xC	a
postupného	postupný	k2eAgNnSc2d1	postupné
uvědomování	uvědomování	k1gNnSc2	uvědomování
si	se	k3xPyFc3	se
nejistoty	nejistota	k1gFnSc2	nejistota
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
luxus	luxus	k1gInSc1	luxus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
dopřával	dopřávat	k5eAaImAgMnS	dopřávat
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
nejistém	jistý	k2eNgInSc6d1	nejistý
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pocitech	pocit	k1gInPc6	pocit
nebyl	být	k5eNaImAgInS	být
Gautama	Gautam	k1gMnSc2	Gautam
osamocený	osamocený	k2eAgMnSc1d1	osamocený
<g/>
,	,	kIx,	,
o	o	k7c6	o
náladě	nálada	k1gFnSc6	nálada
zániku	zánik	k1gInSc2	zánik
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
Gautamovi	Gautam	k1gMnSc6	Gautam
volili	volit	k5eAaImAgMnP	volit
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
bezdomoví	bezdomoví	k1gNnSc2	bezdomoví
také	také	k9	také
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
pěstounka	pěstounka	k1gFnSc1	pěstounka
<g/>
,	,	kIx,	,
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Ráhula	Ráhula	k1gFnSc1	Ráhula
a	a	k8xC	a
bratranci	bratranec	k1gMnPc1	bratranec
Ánanda	Ánanda	k1gFnSc1	Ánanda
a	a	k8xC	a
Dévadatta	Dévadatta	k1gFnSc1	Dévadatta
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
později	pozdě	k6eAd2	pozdě
putovali	putovat	k5eAaImAgMnP	putovat
s	s	k7c7	s
Gautamou	Gautama	k1gMnSc7	Gautama
jako	jako	k8xC	jako
šramani	šraman	k1gMnPc1	šraman
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
válečníci	válečník	k1gMnPc1	válečník
<g/>
,	,	kIx,	,
jen	jen	k9	jen
někteří	některý	k3yIgMnPc1	některý
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
rozvíjející	rozvíjející	k2eAgFnSc2d1	rozvíjející
se	se	k3xPyFc4	se
obchodnické	obchodnický	k2eAgFnSc2d1	obchodnická
kasty	kasta	k1gFnSc2	kasta
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
právě	právě	k9	právě
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
pak	pak	k6eAd1	pak
vzešli	vzejít	k5eAaPmAgMnP	vzejít
početní	početní	k2eAgMnPc1d1	početní
přívrženci	přívrženec	k1gMnPc1	přívrženec
a	a	k8xC	a
pokračovatelé	pokračovatel	k1gMnPc1	pokračovatel
jeho	on	k3xPp3gNnSc2	on
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Osvobození	osvobození	k1gNnSc1	osvobození
od	od	k7c2	od
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
vazeb	vazba	k1gFnPc2	vazba
se	se	k3xPyFc4	se
jevilo	jevit	k5eAaImAgNnS	jevit
Gautamovi	Gautam	k1gMnSc3	Gautam
i	i	k9	i
mnohým	mnohý	k2eAgMnPc3d1	mnohý
válečníkům	válečník	k1gMnPc3	válečník
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
ke	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
v	v	k7c6	v
bohatství	bohatství	k1gNnSc6	bohatství
a	a	k8xC	a
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
pomíjivost	pomíjivost	k1gFnSc1	pomíjivost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tehdejších	tehdejší	k2eAgFnPc6d1	tehdejší
dobách	doba	k1gFnPc6	doba
pociťována	pociťován	k2eAgMnSc4d1	pociťován
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
jindy	jindy	k6eAd1	jindy
<g/>
.	.	kIx.	.
</s>
<s>
Gautamovo	Gautamův	k2eAgNnSc1d1	Gautamovo
hledání	hledání	k1gNnSc1	hledání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
zákonu	zákon	k1gInSc3	zákon
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
poutníků	poutník	k1gMnPc2	poutník
v	v	k7c6	v
bezdomoví	bezdomoví	k1gNnSc6	bezdomoví
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
naukami	nauka	k1gFnPc7	nauka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
společné	společný	k2eAgInPc1d1	společný
obsahy	obsah	k1gInPc1	obsah
nebyly	být	k5eNaImAgInP	být
tím	ten	k3xDgMnSc7	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
šramany	šramany	k?	šramany
sjednocovalo	sjednocovat	k5eAaImAgNnS	sjednocovat
<g/>
.	.	kIx.	.
</s>
<s>
Spojující	spojující	k2eAgInSc1d1	spojující
prvek	prvek	k1gInSc1	prvek
spočíval	spočívat	k5eAaImAgInS	spočívat
ve	v	k7c6	v
zřeknutí	zřeknutí	k1gNnSc6	zřeknutí
se	se	k3xPyFc4	se
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
kastovním	kastovní	k2eAgInSc6d1	kastovní
řádu	řád	k1gInSc6	řád
<g/>
,	,	kIx,	,
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
povinností	povinnost	k1gFnPc2	povinnost
rodinného	rodinný	k2eAgInSc2d1	rodinný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
protichůdné	protichůdný	k2eAgFnPc4d1	protichůdná
teorie	teorie	k1gFnPc4	teorie
a	a	k8xC	a
praktiky	praktik	k1gMnPc4	praktik
<g/>
:	:	kIx,	:
našli	najít	k5eAaPmAgMnP	najít
se	se	k3xPyFc4	se
věštci	věštec	k1gMnPc1	věštec
a	a	k8xC	a
čarodějové	čaroděj	k1gMnPc1	čaroděj
<g/>
,	,	kIx,	,
učitelé	učitel	k1gMnPc1	učitel
vysvobození	vysvobození	k1gNnPc2	vysvobození
<g/>
,	,	kIx,	,
filosofové	filosof	k1gMnPc1	filosof
a	a	k8xC	a
asketi	asketa	k1gMnPc1	asketa
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
šramanů	šraman	k1gMnPc2	šraman
přitahoval	přitahovat	k5eAaImAgInS	přitahovat
zpočátku	zpočátku	k6eAd1	zpočátku
jen	jen	k9	jen
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
árijce	árijec	k1gMnPc4	árijec
<g/>
,	,	kIx,	,
v	v	k7c6	v
Gautamově	Gautamově	k1gFnSc6	Gautamově
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
bezdomoví	bezdomoví	k1gNnSc1	bezdomoví
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgFnPc4d1	mnohá
alternativu	alternativa	k1gFnSc4	alternativa
nehybnosti	nehybnost	k1gFnSc3	nehybnost
kastovního	kastovní	k2eAgInSc2d1	kastovní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
válečníka	válečník	k1gMnSc2	válečník
nebo	nebo	k8xC	nebo
obchodníka	obchodník	k1gMnSc2	obchodník
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
při	při	k7c6	při
nejsilnějším	silný	k2eAgInSc6d3	nejsilnější
zájmu	zájem	k1gInSc6	zájem
o	o	k7c6	o
náboženství	náboženství	k1gNnSc6	náboženství
nemohl	moct	k5eNaImAgMnS	moct
stát	stát	k5eAaImF	stát
bráhman	bráhman	k1gMnSc1	bráhman
<g/>
,	,	kIx,	,
obětující	obětující	k2eAgFnSc1d1	obětující
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
šramanství	šramanství	k1gNnSc2	šramanství
byla	být	k5eAaImAgFnS	být
svobodou	svoboda	k1gFnSc7	svoboda
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
ke	k	k7c3	k
kastovnímu	kastovní	k2eAgInSc3d1	kastovní
řádu	řád	k1gInSc3	řád
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
každému	každý	k3xTgMnSc3	každý
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
určil	určit	k5eAaPmAgMnS	určit
jeho	jeho	k3xOp3gFnSc2	jeho
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
šramany	šramany	k?	šramany
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgMnSc2	jenž
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
Gautama	Gautama	k1gNnSc4	Gautama
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
neomezoval	omezovat	k5eNaImAgMnS	omezovat
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
oddávali	oddávat	k5eAaImAgMnP	oddávat
šamanským	šamanský	k2eAgFnPc3d1	šamanská
disciplínám	disciplína	k1gFnPc3	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Archaické	archaický	k2eAgFnPc1d1	archaická
praktiky	praktika	k1gFnPc1	praktika
se	se	k3xPyFc4	se
rozvinuly	rozvinout	k5eAaPmAgFnP	rozvinout
do	do	k7c2	do
komplexních	komplexní	k2eAgInPc2d1	komplexní
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
šramani	šraman	k1gMnPc1	šraman
dávno	dávno	k6eAd1	dávno
připadli	připadnout	k5eAaPmAgMnP	připadnout
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
ideje	idea	k1gFnPc4	idea
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
původními	původní	k2eAgFnPc7d1	původní
drávidskými	drávidský	k2eAgFnPc7d1	drávidská
a	a	k8xC	a
árijskými	árijský	k2eAgFnPc7d1	árijská
tradicemi	tradice	k1gFnPc7	tradice
a	a	k8xC	a
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
bráhmanské	bráhmanský	k2eAgFnPc1d1	bráhmanská
filosofie	filosofie	k1gFnPc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Šramané	šramaný	k2eAgNnSc1d1	šramaný
se	se	k3xPyFc4	se
těšili	těšit	k5eAaImAgMnP	těšit
podpoře	podpora	k1gFnSc3	podpora
u	u	k7c2	u
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
filozofické	filozofický	k2eAgFnPc4d1	filozofická
teorie	teorie	k1gFnPc4	teorie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
nalézt	nalézt	k5eAaBmF	nalézt
odpovědi	odpověď	k1gFnPc1	odpověď
na	na	k7c4	na
nejzákladnější	základní	k2eAgFnPc4d3	nejzákladnější
existenciální	existenciální	k2eAgFnPc4d1	existenciální
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
si	se	k3xPyFc3	se
tak	tak	k9	tak
mohl	moct	k5eAaImAgMnS	moct
vybrat	vybrat	k5eAaPmF	vybrat
mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
teorií	teorie	k1gFnPc2	teorie
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
osobního	osobní	k2eAgNnSc2d1	osobní
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Šramanské	Šramanský	k2eAgFnPc1d1	Šramanský
filosofie	filosofie	k1gFnPc1	filosofie
tak	tak	k9	tak
sahaly	sahat	k5eAaImAgFnP	sahat
od	od	k7c2	od
nejryzejšího	ryzí	k2eAgInSc2d3	nejryzejší
materialismu	materialismus	k1gInSc2	materialismus
až	až	k9	až
po	po	k7c4	po
čistý	čistý	k2eAgInSc4d1	čistý
teismus	teismus	k1gInSc4	teismus
<g/>
.	.	kIx.	.
</s>
<s>
Spekulující	spekulující	k2eAgMnPc1d1	spekulující
či	či	k8xC	či
skeptičtí	skeptický	k2eAgMnPc1d1	skeptický
teoretikové	teoretik	k1gMnPc1	teoretik
přitahovali	přitahovat	k5eAaImAgMnP	přitahovat
Gautamu	Gautam	k1gInSc3	Gautam
právě	právě	k9	právě
tak	tak	k6eAd1	tak
málo	málo	k6eAd1	málo
jako	jako	k8xC	jako
šramani	šraman	k1gMnPc1	šraman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
předpovídáním	předpovídání	k1gNnSc7	předpovídání
<g/>
,	,	kIx,	,
zaklínáním	zaklínání	k1gNnSc7	zaklínání
duchů	duch	k1gMnPc2	duch
nebo	nebo	k8xC	nebo
zaříkávacími	zaříkávací	k2eAgFnPc7d1	zaříkávací
formulemi	formule	k1gFnPc7	formule
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
problém	problém	k1gInSc1	problém
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
všeho	všecek	k3xTgInSc2	všecek
existujícího	existující	k2eAgInSc2d1	existující
byl	být	k5eAaImAgMnS	být
problémem	problém	k1gInSc7	problém
náboženským	náboženský	k2eAgInSc7d1	náboženský
<g/>
.	.	kIx.	.
</s>
<s>
Teistické	teistický	k2eAgInPc1d1	teistický
přístupy	přístup	k1gInPc1	přístup
nabízely	nabízet	k5eAaImAgInP	nabízet
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
neuspokojovalo	uspokojovat	k5eNaImAgNnS	uspokojovat
<g/>
.	.	kIx.	.
</s>
<s>
Šramanům	Šraman	k1gMnPc3	Šraman
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
všechno	všechen	k3xTgNnSc4	všechen
odvozovali	odvozovat	k5eAaImAgMnP	odvozovat
z	z	k7c2	z
tvorby	tvorba	k1gFnSc2	tvorba
boha	bůh	k1gMnSc2	bůh
Išvary	Išvara	k1gFnSc2	Išvara
<g/>
,	,	kIx,	,
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
<g/>
:	:	kIx,	:
Takže	takže	k9	takže
na	na	k7c6	na
základě	základ	k1gInSc6	základ
stvoření	stvoření	k1gNnSc2	stvoření
boha	bůh	k1gMnSc4	bůh
Išvary	Išvara	k1gFnSc2	Išvara
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
vrazi	vrah	k1gMnPc1	vrah
<g/>
,	,	kIx,	,
zloději	zloděj	k1gMnPc1	zloděj
<g/>
,	,	kIx,	,
hanebníci	hanebník	k1gMnPc5	hanebník
<g/>
,	,	kIx,	,
lháři	lhář	k1gMnPc5	lhář
<g/>
,	,	kIx,	,
udavači	udavač	k1gMnPc5	udavač
<g/>
,	,	kIx,	,
posměváčci	posměváček	k1gMnPc5	posměváček
<g/>
,	,	kIx,	,
žvanilové	žvanil	k1gMnPc5	žvanil
<g/>
,	,	kIx,	,
lakomci	lakomec	k1gMnPc5	lakomec
<g/>
,	,	kIx,	,
nevraživci	nevraživec	k1gMnPc1	nevraživec
a	a	k8xC	a
bloudi	bloud	k1gMnPc1	bloud
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
trpěl	trpět	k5eAaImAgMnS	trpět
pomíjivostí	pomíjivost	k1gFnSc7	pomíjivost
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
nevyžadoval	vyžadovat	k5eNaImAgInS	vyžadovat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
ukázaly	ukázat	k5eAaPmAgInP	ukázat
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
šramani	šraman	k1gMnPc1	šraman
nabízeli	nabízet	k5eAaImAgMnP	nabízet
nauky	nauka	k1gFnPc4	nauka
o	o	k7c4	o
vysvobození	vysvobození	k1gNnSc4	vysvobození
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
takové	takový	k3xDgFnPc1	takový
zkušenosti	zkušenost	k1gFnPc1	zkušenost
slibovaly	slibovat	k5eAaImAgFnP	slibovat
<g/>
.	.	kIx.	.
</s>
<s>
Vycházeli	vycházet	k5eAaImAgMnP	vycházet
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
z	z	k7c2	z
ideje	idea	k1gFnSc2	idea
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
smrt	smrt	k1gFnSc1	smrt
každé	každý	k3xTgFnSc2	každý
bytosti	bytost	k1gFnSc2	bytost
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
zrození	zrození	k1gNnSc3	zrození
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgMnPc4	tento
učitele	učitel	k1gMnPc4	učitel
vysvobození	vysvobození	k1gNnSc3	vysvobození
stály	stát	k5eAaImAgInP	stát
lidské	lidský	k2eAgInPc1d1	lidský
činy	čin	k1gInPc1	čin
a	a	k8xC	a
nekonání	nekonání	k1gNnSc1	nekonání
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
svoboda	svoboda	k1gFnSc1	svoboda
konání	konání	k1gNnSc2	konání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
určováno	určován	k2eAgNnSc1d1	určováno
ničím	ničí	k3xOyNgInSc7	ničí
předchozím	předchozí	k2eAgMnSc7d1	předchozí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
myslitelný	myslitelný	k2eAgInSc4d1	myslitelný
únik	únik	k1gInSc4	únik
z	z	k7c2	z
koloběhu	koloběh	k1gInSc2	koloběh
zrození	zrození	k1gNnSc2	zrození
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
Árádovi	Áráda	k1gMnSc3	Áráda
Kálámovi	Kálám	k1gMnSc3	Kálám
a	a	k8xC	a
Udrakovi	Udrak	k1gMnSc3	Udrak
Rámaputrovi	Rámaputr	k1gMnSc3	Rámaputr
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgFnPc7	dva
učitelům	učitel	k1gMnPc3	učitel
vysvobození	vysvobození	k1gNnSc2	vysvobození
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
:	:	kIx,	:
Moje	můj	k3xOp1gFnSc1	můj
nauka	nauka	k1gFnSc1	nauka
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
rozumný	rozumný	k2eAgMnSc1d1	rozumný
člověk	člověk	k1gMnSc1	člověk
rychle	rychle	k6eAd1	rychle
pochopí	pochopit	k5eAaPmIp3nS	pochopit
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
mistrem	mistr	k1gMnSc7	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
Árády	Áráda	k1gFnSc2	Áráda
Kálámy	Káláma	k1gFnSc2	Káláma
měla	mít	k5eAaImAgFnS	mít
vést	vést	k5eAaImF	vést
do	do	k7c2	do
sféry	sféra	k1gFnSc2	sféra
ničeho	nic	k3yNnSc2	nic
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
šel	jít	k5eAaImAgMnS	jít
Udraka	Udrak	k1gMnSc4	Udrak
Rámaputra	Rámaputr	k1gMnSc4	Rámaputr
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
metoda	metoda	k1gFnSc1	metoda
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
hraniční	hraniční	k2eAgFnSc3d1	hraniční
sféře	sféra	k1gFnSc3	sféra
vnímání	vnímání	k1gNnSc2	vnímání
a	a	k8xC	a
nevnímání	nevnímání	k1gNnSc2	nevnímání
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
mizí	mizet	k5eAaImIp3nS	mizet
i	i	k9	i
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
předtím	předtím	k6eAd1	předtím
vnímal	vnímat	k5eAaImAgMnS	vnímat
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
meditační	meditační	k2eAgInPc4d1	meditační
stavy	stav	k1gInPc4	stav
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
buddhismu	buddhismus	k1gInSc6	buddhismus
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xS	jako
arúpa	arúp	k1gMnSc2	arúp
džhány	džhána	k1gFnSc2	džhána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
<g/>
,	,	kIx,	,
Gautama	Gautama	k1gFnSc1	Gautama
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
učení	učení	k1gNnSc4	učení
těchto	tento	k3xDgInPc2	tento
šramanů	šraman	k1gInPc2	šraman
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
mu	on	k3xPp3gMnSc3	on
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
vedoucí	vedoucí	k1gMnPc1	vedoucí
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
obcích	obec	k1gFnPc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
akceptoval	akceptovat	k5eAaBmAgMnS	akceptovat
takové	takový	k3xDgInPc4	takový
výjimečné	výjimečný	k2eAgInPc4d1	výjimečný
psychické	psychický	k2eAgInPc4d1	psychický
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
existovaly	existovat	k5eAaImAgInP	existovat
při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
posledních	poslední	k2eAgInPc2d1	poslední
zbytků	zbytek	k1gInPc2	zbytek
subjektivity	subjektivita	k1gFnSc2	subjektivita
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
jako	jako	k8xS	jako
cestu	cesta	k1gFnSc4	cesta
ze	z	k7c2	z
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
zkušenostech	zkušenost	k1gFnPc6	zkušenost
se	se	k3xPyFc4	se
Gautama	Gautama	k1gFnSc1	Gautama
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
šramany	šramany	k?	šramany
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysvobození	vysvobození	k1gNnSc4	vysvobození
ze	z	k7c2	z
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
nebo	nebo	k8xC	nebo
zlepšení	zlepšení	k1gNnSc2	zlepšení
postavení	postavení	k1gNnSc2	postavení
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
bytostí	bytost	k1gFnPc2	bytost
extrémní	extrémní	k2eAgFnSc7d1	extrémní
askezí	askeze	k1gFnSc7	askeze
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
důsledným	důsledný	k2eAgNnSc7d1	důsledné
přijetím	přijetí	k1gNnSc7	přijetí
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
forem	forma	k1gFnPc2	forma
chování	chování	k1gNnSc2	chování
se	se	k3xPyFc4	se
zrodí	zrodit	k5eAaPmIp3nS	zrodit
mezi	mezi	k7c7	mezi
bohy	bůh	k1gMnPc7	bůh
<g/>
:	:	kIx,	:
psí	psí	k2eAgMnPc1d1	psí
asketi	asketa	k1gMnPc1	asketa
lezli	lézt	k5eAaImAgMnP	lézt
po	po	k7c6	po
čtyřech	čtyři	k4xCgMnPc6	čtyři
<g/>
,	,	kIx,	,
choulili	choulit	k5eAaImAgMnP	choulit
se	se	k3xPyFc4	se
k	k	k7c3	k
spánku	spánek	k1gInSc3	spánek
a	a	k8xC	a
jedli	jedle	k1gFnSc3	jedle
pouze	pouze	k6eAd1	pouze
vleže	vleže	k6eAd1	vleže
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Imitovali	imitovat	k5eAaBmAgMnP	imitovat
také	také	k6eAd1	také
jiná	jiný	k2eAgNnPc4d1	jiné
zvířata	zvíře	k1gNnPc4	zvíře
v	v	k7c4	v
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
ponížení	ponížení	k1gNnSc4	ponížení
za	za	k7c2	za
života	život	k1gInSc2	život
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
snad	snad	k9	snad
dočasně	dočasně	k6eAd1	dočasně
něco	něco	k3yInSc4	něco
podobného	podobný	k2eAgNnSc2d1	podobné
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
:	:	kIx,	:
Chodil	chodit	k5eAaImAgMnS	chodit
jsem	být	k5eAaImIp1nS	být
k	k	k7c3	k
dobytčím	dobytčí	k2eAgFnPc3d1	dobytčí
stájím	stáj	k1gFnPc3	stáj
<g/>
,	,	kIx,	,
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
pastevci	pastevec	k1gMnPc1	pastevec
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
živil	živit	k5eAaImAgMnS	živit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
výkaly	výkal	k1gInPc7	výkal
mladých	mladý	k2eAgInPc2d1	mladý
<g/>
,	,	kIx,	,
sajících	sající	k2eAgNnPc2d1	sající
telat	tele	k1gNnPc2	tele
<g/>
.	.	kIx.	.
</s>
<s>
Jedl	jíst	k5eAaImAgMnS	jíst
jsem	být	k5eAaImIp1nS	být
také	také	k9	také
nestrávené	strávený	k2eNgInPc4d1	nestrávený
zbytky	zbytek	k1gInPc4	zbytek
v	v	k7c6	v
mých	můj	k3xOp1gNnPc6	můj
lejnech	lejno	k1gNnPc6	lejno
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
asketické	asketický	k2eAgFnPc1d1	asketická
představy	představa	k1gFnPc1	představa
věřily	věřit	k5eAaImAgFnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohrdáním	pohrdání	k1gNnSc7	pohrdání
pomíjivým	pomíjivý	k2eAgNnSc7d1	pomíjivé
tělem	tělo	k1gNnSc7	tělo
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
osvobození	osvobození	k1gNnSc1	osvobození
trvalého	trvalý	k2eAgNnSc2d1	trvalé
džívy	džív	k1gMnPc7	džív
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
svázán	svázat	k5eAaPmNgInS	svázat
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
také	také	k9	také
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
má	můj	k3xOp1gFnSc1	můj
trýzeň	trýzeň	k1gFnSc1	trýzeň
<g/>
:	:	kIx,	:
běhal	běhat	k5eAaImAgMnS	běhat
jsem	být	k5eAaImIp1nS	být
nahý	nahý	k2eAgMnSc1d1	nahý
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnohaleté	mnohaletý	k2eAgFnSc2d1	mnohaletá
špíny	špína	k1gFnSc2	špína
na	na	k7c6	na
mém	můj	k3xOp1gNnSc6	můj
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vrstva	vrstva	k1gFnSc1	vrstva
jako	jako	k8xS	jako
na	na	k7c6	na
pahýlu	pahýl	k1gInSc6	pahýl
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Nemyslel	myslet	k5eNaImAgMnS	myslet
jsem	být	k5eAaImIp1nS	být
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
se	se	k3xPyFc4	se
očistil	očistit	k5eAaPmAgInS	očistit
nebo	nebo	k8xC	nebo
nechal	nechat	k5eAaPmAgInS	nechat
očistit	očistit	k5eAaPmF	očistit
od	od	k7c2	od
špíny	špína	k1gFnSc2	špína
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
snad	snad	k9	snad
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c6	na
scestí	scestí	k1gNnSc6	scestí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
šramanů	šraman	k1gInPc2	šraman
<g/>
,	,	kIx,	,
bohatém	bohatý	k2eAgNnSc6d1	bohaté
na	na	k7c6	na
zvláštnosti	zvláštnost	k1gFnSc6	zvláštnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
setkával	setkávat	k5eAaImAgMnS	setkávat
s	s	k7c7	s
výsměchem	výsměch	k1gInSc7	výsměch
<g/>
.	.	kIx.	.
</s>
<s>
Šel	jít	k5eAaImAgMnS	jít
jsem	být	k5eAaImIp1nS	být
na	na	k7c4	na
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
lože	lože	k1gNnSc2	lože
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
přišli	přijít	k5eAaPmAgMnP	přijít
pastevci	pastevec	k1gMnPc1	pastevec
a	a	k8xC	a
plivali	plivat	k5eAaImAgMnP	plivat
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
močili	močit	k5eAaImAgMnP	močit
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
házeli	házet	k5eAaImAgMnP	házet
po	po	k7c6	po
mně	já	k3xPp1nSc6	já
lejny	lejno	k1gNnPc7	lejno
a	a	k8xC	a
strkali	strkat	k5eAaImAgMnP	strkat
mi	já	k3xPp1nSc3	já
stébla	stéblo	k1gNnPc4	stéblo
trávy	tráva	k1gFnSc2	tráva
do	do	k7c2	do
uší	ucho	k1gNnPc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Gautamových	Gautamový	k2eAgInPc2d1	Gautamový
popisů	popis	k1gInPc2	popis
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
prováděl	provádět	k5eAaImAgMnS	provádět
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
svého	svůj	k3xOyFgNnSc2	svůj
hledání	hledání	k1gNnSc2	hledání
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
soudit	soudit	k5eAaImF	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
právě	právě	k6eAd1	právě
o	o	k7c4	o
džinistická	džinistický	k2eAgNnPc4d1	džinistický
nebo	nebo	k8xC	nebo
tímto	tento	k3xDgInSc7	tento
směrem	směr	k1gInSc7	směr
inspirovaná	inspirovaný	k2eAgNnPc4d1	inspirované
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
džinismu	džinismus	k1gInSc2	džinismus
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
špatnému	špatný	k2eAgNnSc3d1	špatné
zrození	zrození	k1gNnSc3	zrození
i	i	k8xC	i
neúmyslné	úmyslný	k2eNgInPc4d1	neúmyslný
činy	čin	k1gInPc4	čin
jako	jako	k8xS	jako
např.	např.	kA	např.
náhodné	náhodný	k2eAgNnSc1d1	náhodné
zašlápnutí	zašlápnutí	k1gNnSc1	zašlápnutí
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Osvobození	osvobozený	k2eAgMnPc1d1	osvobozený
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
jen	jen	k9	jen
eliminací	eliminace	k1gFnSc7	eliminace
všech	všecek	k3xTgInPc2	všecek
negativních	negativní	k2eAgInPc2d1	negativní
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
i	i	k9	i
neúmyslných	úmyslný	k2eNgFnPc2d1	neúmyslná
<g/>
.	.	kIx.	.
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1	stoupenec
džinismu	džinismus	k1gInSc2	džinismus
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
snažili	snažit	k5eAaImAgMnP	snažit
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
veškeré	veškerý	k3xTgInPc4	veškerý
tělesné	tělesný	k2eAgInPc4d1	tělesný
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
také	také	k9	také
Gautama	Gautam	k1gMnSc2	Gautam
se	se	k3xPyFc4	se
podrobil	podrobit	k5eAaPmAgMnS	podrobit
této	tento	k3xDgFnSc3	tento
disciplíně	disciplína	k1gFnSc3	disciplína
<g/>
:	:	kIx,	:
Našlapoval	našlapovat	k5eAaImAgMnS	našlapovat
jsem	být	k5eAaImIp1nS	být
pozorně	pozorně	k6eAd1	pozorně
a	a	k8xC	a
všímal	všímat	k5eAaImAgMnS	všímat
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
pln	pln	k2eAgMnSc1d1	pln
soucitu	soucit	k1gInSc2	soucit
dokonce	dokonce	k9	dokonce
vodní	vodní	k2eAgFnSc1d1	vodní
kapky	kapka	k1gFnPc1	kapka
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
nezranil	zranit	k5eNaPmAgMnS	zranit
ani	ani	k8xC	ani
nejmenší	malý	k2eAgFnPc1d3	nejmenší
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zde	zde	k6eAd1	zde
zabloudily	zabloudit	k5eAaPmAgFnP	zabloudit
<g/>
.	.	kIx.	.
</s>
<s>
Džínisté	Džínista	k1gMnPc1	Džínista
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaImAgMnP	věnovat
také	také	k6eAd1	také
praktikám	praktika	k1gFnPc3	praktika
potlačujícím	potlačující	k2eAgFnPc3d1	potlačující
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgFnPc4d1	důležitá
tělesné	tělesný	k2eAgFnPc4d1	tělesná
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
cvičil	cvičit	k5eAaImAgInS	cvičit
zadržení	zadržení	k1gNnSc4	zadržení
dechu	dech	k1gInSc2	dech
nosem	nos	k1gInSc7	nos
nebo	nebo	k8xC	nebo
ústy	ústa	k1gNnPc7	ústa
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
toto	tento	k3xDgNnSc1	tento
cvičení	cvičení	k1gNnSc1	cvičení
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
pocitu	pocit	k1gInSc3	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
vzduch	vzduch	k1gInSc1	vzduch
proudí	proudit	k5eAaImIp3nS	proudit
uchem	ucho	k1gNnSc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzduch	vzduch	k1gInSc1	vzduch
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
bolestem	bolest	k1gFnPc3	bolest
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
nějaký	nějaký	k3yIgInSc4	nějaký
silný	silný	k2eAgMnSc1d1	silný
muž	muž	k1gMnSc1	muž
navrtával	navrtávat	k5eAaImAgMnS	navrtávat
dýkou	dýka	k1gFnSc7	dýka
lebku	lebka	k1gFnSc4	lebka
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
řezník	řezník	k1gMnSc1	řezník
rozřezával	rozřezávat	k5eAaImAgMnS	rozřezávat
nožem	nůž	k1gInSc7	nůž
břicho	břicho	k1gNnSc4	břicho
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k9	jako
by	by	kYmCp3nS	by
mě	já	k3xPp1nSc4	já
dva	dva	k4xCgMnPc1	dva
silní	silný	k2eAgMnPc1d1	silný
muži	muž	k1gMnPc1	muž
vhodili	vhodit	k5eAaPmAgMnP	vhodit
do	do	k7c2	do
jámy	jáma	k1gFnSc2	jáma
se	s	k7c7	s
žhavým	žhavý	k2eAgNnSc7d1	žhavé
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
Gautama	Gautam	k1gMnSc4	Gautam
také	také	k6eAd1	také
začal	začít	k5eAaPmAgMnS	začít
redukovat	redukovat	k5eAaBmF	redukovat
své	svůj	k3xOyFgNnSc4	svůj
jídlo	jídlo	k1gNnSc4	jídlo
na	na	k7c4	na
minimální	minimální	k2eAgNnSc4d1	minimální
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Skrovnou	skrovný	k2eAgFnSc7d1	skrovná
stravou	strava	k1gFnSc7	strava
jsem	být	k5eAaImIp1nS	být
velmi	velmi	k6eAd1	velmi
zhubl	zhubnout	k5eAaPmAgMnS	zhubnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
dotknout	dotknout	k5eAaPmF	dotknout
kůže	kůže	k1gFnSc2	kůže
svého	svůj	k3xOyFgNnSc2	svůj
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
nahmatal	nahmatat	k5eAaPmAgMnS	nahmatat
jsem	být	k5eAaImIp1nS	být
obratle	obratel	k1gInPc4	obratel
<g/>
;	;	kIx,	;
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
dotknout	dotknout	k5eAaPmF	dotknout
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
nahmatal	nahmatat	k5eAaPmAgMnS	nahmatat
jsem	být	k5eAaImIp1nS	být
kůži	kůže	k1gFnSc4	kůže
břicha	břicho	k1gNnSc2	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
těsně	těsně	k6eAd1	těsně
přiléhalo	přiléhat	k5eAaImAgNnS	přiléhat
skrovnou	skrovný	k2eAgFnSc7d1	skrovná
výživou	výživa	k1gFnSc7	výživa
mé	můj	k3xOp1gFnSc2	můj
břicho	břicho	k1gNnSc1	břicho
k	k	k7c3	k
obratlům	obratel	k1gInPc3	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
vyprázdnit	vyprázdnit	k5eAaPmF	vyprázdnit
<g/>
,	,	kIx,	,
převrátil	převrátit	k5eAaPmAgMnS	převrátit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
slabostí	slabost	k1gFnSc7	slabost
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
třel	třít	k5eAaImAgMnS	třít
rukou	ruka	k1gFnSc7	ruka
údy	úd	k1gInPc7	úd
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
posílil	posílit	k5eAaPmAgInS	posílit
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
vypadávaly	vypadávat	k5eAaImAgInP	vypadávat
mi	já	k3xPp1nSc3	já
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
skrovné	skrovný	k2eAgFnSc2d1	skrovná
výživy	výživa	k1gFnSc2	výživa
chlupy	chlup	k1gInPc1	chlup
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
uvolněné	uvolněný	k2eAgInPc1d1	uvolněný
z	z	k7c2	z
kořínků	kořínek	k1gInPc2	kořínek
<g/>
.	.	kIx.	.
</s>
<s>
Gautamovo	Gautamův	k2eAgNnSc1d1	Gautamovo
rigorózní	rigorózní	k2eAgNnSc1d1	rigorózní
cvičení	cvičení	k1gNnSc1	cvičení
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
učinilo	učinit	k5eAaImAgNnS	učinit
vzor	vzor	k1gInSc4	vzor
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
askety	asketa	k1gMnPc4	asketa
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
jej	on	k3xPp3gMnSc4	on
pět	pět	k4xCc1	pět
šramanů	šraman	k1gMnPc2	šraman
-	-	kIx~	-
Kaundinja	Kaundinja	k1gMnSc1	Kaundinja
<g/>
,	,	kIx,	,
Ašvadžit	Ašvadžit	k1gMnSc1	Ašvadžit
<g/>
,	,	kIx,	,
Váspa	Váspa	k1gFnSc1	Váspa
<g/>
,	,	kIx,	,
Mahánáma	Mahánáma	k1gFnSc1	Mahánáma
a	a	k8xC	a
Bhadradžit	Bhadradžit	k1gMnPc1	Bhadradžit
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
šramana	šraman	k1gMnSc2	šraman
Gautama	Gautam	k1gMnSc2	Gautam
najde	najít	k5eAaPmIp3nS	najít
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
sdělí	sdělit	k5eAaPmIp3nS	sdělit
nám	my	k3xPp1nPc3	my
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ale	ale	k9	ale
Gautama	Gautama	k1gNnSc4	Gautama
pochyboval	pochybovat	k5eAaImAgMnS	pochybovat
o	o	k7c6	o
nastoupené	nastoupený	k2eAgFnSc6d1	nastoupená
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vzal	vzít	k5eAaPmAgMnS	vzít
nejkrajnější	krajní	k2eAgNnSc4d3	nejkrajnější
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc4	jaký
kdy	kdy	k6eAd1	kdy
šramana	šraman	k1gMnSc2	šraman
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
<g/>
,	,	kIx,	,
necítil	cítit	k5eNaImAgMnS	cítit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
blíže	blízce	k6eAd2	blízce
svému	svůj	k3xOyFgInSc3	svůj
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jej	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgInS	přivést
půst	půst	k1gInSc1	půst
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
konfrontován	konfrontován	k2eAgInSc1d1	konfrontován
s	s	k7c7	s
pomíjivostí	pomíjivost	k1gFnSc7	pomíjivost
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
pocítil	pocítit	k5eAaPmAgMnS	pocítit
vytouženou	vytoužený	k2eAgFnSc4d1	vytoužená
zkušenost	zkušenost	k1gFnSc4	zkušenost
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
askeze	askeze	k1gFnSc1	askeze
se	se	k3xPyFc4	se
zdála	zdát	k5eAaImAgFnS	zdát
nesmyslná	smyslný	k2eNgFnSc1d1	nesmyslná
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
Gautama	Gautama	k1gNnSc4	Gautama
o	o	k7c4	o
sebetrýznění	sebetrýznění	k1gNnSc4	sebetrýznění
džinistů	džinista	k1gMnPc2	džinista
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
nelze	lze	k6eNd1	lze
za	za	k7c2	za
žádných	žádný	k3yNgFnPc2	žádný
okolností	okolnost	k1gFnPc2	okolnost
uspokojivě	uspokojivě	k6eAd1	uspokojivě
odůvodnit	odůvodnit	k5eAaPmF	odůvodnit
<g/>
:	:	kIx,	:
Jestliže	jestliže	k8xS	jestliže
štěstí	štěstí	k1gNnSc1	štěstí
a	a	k8xC	a
utrpení	utrpení	k1gNnSc1	utrpení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
činech	čin	k1gInPc6	čin
v	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
životech	život	k1gInPc6	život
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
džinisté	džinista	k1gMnPc1	džinista
dříve	dříve	k6eAd2	dříve
zločinci	zločinec	k1gMnPc7	zločinec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
teď	teď	k6eAd1	teď
tolik	tolik	k6eAd1	tolik
trpí	trpět	k5eAaImIp3nP	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
radost	radost	k1gFnSc4	radost
a	a	k8xC	a
utrpení	utrpení	k1gNnSc4	utrpení
určuje	určovat	k5eAaImIp3nS	určovat
stvořitel	stvořitel	k1gMnSc1	stvořitel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
zlý	zlý	k2eAgMnSc1d1	zlý
stvořitel	stvořitel	k1gMnSc1	stvořitel
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
džinisty	džinista	k1gMnPc7	džinista
stvořil	stvořit	k5eAaPmAgMnS	stvořit
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
radost	radost	k1gFnSc1	radost
a	a	k8xC	a
utrpení	utrpení	k1gNnSc1	utrpení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
náhodě	náhoda	k1gFnSc6	náhoda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
zlá	zlý	k2eAgFnSc1d1	zlá
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
džinisty	džinista	k1gMnPc4	džinista
postihla	postihnout	k5eAaPmAgFnS	postihnout
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
-li	i	k?	-li
radost	radost	k1gFnSc4	radost
a	a	k8xC	a
utrpení	utrpení	k1gNnSc4	utrpení
zrození	zrození	k1gNnPc2	zrození
<g/>
,	,	kIx,	,
potkalo	potkat	k5eAaPmAgNnS	potkat
džinisty	džinista	k1gMnPc4	džinista
špatné	špatný	k2eAgNnSc1d1	špatné
zrození	zrození	k1gNnSc1	zrození
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
<g/>
-li	i	k?	-li
radost	radost	k1gFnSc4	radost
a	a	k8xC	a
utrpení	utrpení	k1gNnSc4	utrpení
na	na	k7c4	na
úsilí	úsilí	k1gNnSc4	úsilí
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úsilí	úsilí	k1gNnSc1	úsilí
džinistů	džinista	k1gMnPc2	džinista
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
životě	život	k1gInSc6	život
špatné	špatný	k2eAgInPc1d1	špatný
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
opět	opět	k6eAd1	opět
normálně	normálně	k6eAd1	normálně
jíst	jíst	k5eAaImF	jíst
a	a	k8xC	a
přestal	přestat	k5eAaPmAgMnS	přestat
být	být	k5eAaImF	být
proto	proto	k8xC	proto
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
svých	svůj	k3xOyFgNnPc2	svůj
pět	pět	k4xCc4	pět
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Neporozuměli	porozumět	k5eNaPmAgMnP	porozumět
změně	změna	k1gFnSc3	změna
v	v	k7c4	v
jeho	jeho	k3xOp3gNnSc4	jeho
smýšlení	smýšlení	k1gNnSc4	smýšlení
a	a	k8xC	a
opustili	opustit	k5eAaPmAgMnP	opustit
jej	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Odvrácení	odvrácení	k1gNnSc1	odvrácení
od	od	k7c2	od
askeze	askeze	k1gFnSc2	askeze
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
stát	stát	k5eAaImF	stát
se	s	k7c7	s
šramanou	šramaný	k2eAgFnSc7d1	šramaný
druhým	druhý	k4xOgInSc7	druhý
velkým	velký	k2eAgInSc7d1	velký
bodem	bod	k1gInSc7	bod
obratu	obrat	k1gInSc2	obrat
v	v	k7c6	v
Gautamově	Gautamově	k1gFnSc6	Gautamově
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
se	se	k3xPyFc4	se
rozloučil	rozloučit	k5eAaPmAgMnS	rozloučit
s	s	k7c7	s
luxusem	luxus	k1gInSc7	luxus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
nabízel	nabízet	k5eAaImAgInS	nabízet
blahobyt	blahobyt	k1gInSc1	blahobyt
v	v	k7c6	v
rádžově	rádžův	k2eAgInSc6d1	rádžův
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
nyní	nyní	k6eAd1	nyní
strastiplné	strastiplný	k2eAgFnPc4d1	strastiplná
praktiky	praktika	k1gFnPc4	praktika
šramanů	šraman	k1gInPc2	šraman
<g/>
,	,	kIx,	,
hledajících	hledající	k2eAgFnPc2d1	hledající
spásu	spása	k1gFnSc4	spása
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
střední	střední	k2eAgFnSc1d1	střední
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
utrpení	utrpení	k1gNnSc2	utrpení
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
extrémům	extrém	k1gInPc3	extrém
světského	světský	k2eAgInSc2d1	světský
života	život	k1gInSc2	život
a	a	k8xC	a
odříkání	odříkání	k1gNnSc2	odříkání
<g/>
.	.	kIx.	.
</s>
<s>
Šramana	Šraman	k1gMnSc2	Šraman
<g/>
,	,	kIx,	,
hledající	hledající	k2eAgFnSc4d1	hledající
spásu	spása	k1gFnSc4	spása
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
pro	pro	k7c4	pro
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
cítit	cítit	k5eAaImF	cítit
vysvobozen	vysvobodit	k5eAaPmNgInS	vysvobodit
ze	z	k7c2	z
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
netrpěl	trpět	k5eNaImAgMnS	trpět
pomíjivostí	pomíjivost	k1gFnSc7	pomíjivost
<g/>
.	.	kIx.	.
</s>
<s>
Gautamovo	Gautamův	k2eAgNnSc1d1	Gautamovo
objevení	objevení	k1gNnSc1	objevení
střední	střední	k2eAgFnSc2d1	střední
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
probuzením	probuzení	k1gNnSc7	probuzení
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nazýván	nazývat	k5eAaImNgInS	nazývat
probuzeným	probuzený	k2eAgMnSc7d1	probuzený
(	(	kIx(	(
<g/>
Buddhou	Buddha	k1gMnSc7	Buddha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pozvedl	pozvednout	k5eAaPmAgMnS	pozvednout
ze	z	k7c2	z
snové	snový	k2eAgFnSc2d1	snová
existence	existence	k1gFnSc2	existence
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
potácejícího	potácející	k2eAgMnSc2d1	potácející
se	se	k3xPyFc4	se
v	v	k7c6	v
radosti	radost	k1gFnSc6	radost
a	a	k8xC	a
bídě	bída	k1gFnSc6	bída
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
prý	prý	k9	prý
dospěl	dochvít	k5eAaPmAgMnS	dochvít
Gautama	Gautam	k1gMnSc4	Gautam
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
doby	doba	k1gFnSc2	doba
svého	svůj	k3xOyFgNnSc2	svůj
hledání	hledání	k1gNnSc2	hledání
putoval	putovat	k5eAaImAgInS	putovat
Gautama	Gautamum	k1gNnPc4	Gautamum
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Magadha	Magadh	k1gMnSc2	Magadh
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
došel	dojít	k5eAaPmAgInS	dojít
až	až	k6eAd1	až
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
pevnosti	pevnost	k1gFnSc2	pevnost
Uruvéla	Uruvéla	k1gFnSc1	Uruvéla
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jsem	být	k5eAaImIp1nS	být
viděl	vidět	k5eAaImAgMnS	vidět
pěkné	pěkný	k2eAgNnSc4d1	pěkné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
klidný	klidný	k2eAgInSc4d1	klidný
háj	háj	k1gInSc4	háj
s	s	k7c7	s
průzračnou	průzračný	k2eAgFnSc7d1	průzračná
řekou	řeka	k1gFnSc7	řeka
<g/>
,	,	kIx,	,
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
<g/>
.	.	kIx.	.
</s>
<s>
Zastavil	zastavit	k5eAaPmAgInS	zastavit
se	se	k3xPyFc4	se
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Néraňdžará	Néraňdžarý	k2eAgFnSc1d1	Néraňdžarý
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dnešní	dnešní	k2eAgFnSc2d1	dnešní
vesnice	vesnice	k1gFnSc2	vesnice
Bódhgaja	Bódhgaj	k1gInSc2	Bódhgaj
<g/>
,	,	kIx,	,
vychrtlý	vychrtlý	k2eAgInSc4d1	vychrtlý
půstem	půst	k1gInSc7	půst
a	a	k8xC	a
konfrontován	konfrontován	k2eAgInSc1d1	konfrontován
se	se	k3xPyFc4	se
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Líčení	líčení	k1gNnSc1	líčení
personifikuje	personifikovat	k5eAaBmIp3nS	personifikovat
smrt	smrt	k1gFnSc4	smrt
jako	jako	k8xS	jako
Máru	Mára	k1gFnSc4	Mára
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
smrt	smrt	k1gFnSc1	smrt
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žárlivě	žárlivě	k6eAd1	žárlivě
střeží	střežit	k5eAaImIp3nS	střežit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
bytosti	bytost	k1gFnSc3	bytost
zůstaly	zůstat	k5eAaPmAgInP	zůstat
podrobeny	podroben	k2eAgInPc1d1	podroben
v	v	k7c6	v
koloběhu	koloběh	k1gInSc6	koloběh
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
(	(	kIx(	(
<g/>
samutpáda	samutpáda	k1gFnSc1	samutpáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
Gautama	Gautama	k1gFnSc1	Gautama
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
Mára	Mára	k1gFnSc1	Mára
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
hovořil	hovořit	k5eAaImAgMnS	hovořit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hubeně	hubeně	k6eAd1	hubeně
a	a	k8xC	a
ošklivě	ošklivě	k6eAd1	ošklivě
vypadáš	vypadat	k5eAaPmIp2nS	vypadat
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Tisíce	tisíc	k4xCgInPc1	tisíc
tvých	tvůj	k3xOp2gFnPc2	tvůj
částí	část	k1gFnPc2	část
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
ještě	ještě	k9	ještě
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mára	Mára	k1gMnSc1	Mára
se	se	k3xPyFc4	se
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
svého	svůj	k3xOyFgNnSc2	svůj
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Gautama	Gautam	k1gMnSc4	Gautam
vytrval	vytrvat	k5eAaPmAgMnS	vytrvat
v	v	k7c6	v
hledání	hledání	k1gNnSc6	hledání
východiska	východisko	k1gNnSc2	východisko
z	z	k7c2	z
koloběhu	koloběh	k1gInSc2	koloběh
zrození	zrození	k1gNnSc2	zrození
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Mára	Mára	k1gMnSc1	Mára
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
,	,	kIx,	,
poznal	poznat	k5eAaPmAgMnS	poznat
Gautama	Gautam	k1gMnSc4	Gautam
<g/>
,	,	kIx,	,
že	že	k8xS	že
celé	celý	k2eAgNnSc1d1	celé
sebetrýznění	sebetrýznění	k1gNnSc1	sebetrýznění
nevedlo	vést	k5eNaImAgNnS	vést
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
<g/>
.	.	kIx.	.
</s>
<s>
Ptal	ptat	k5eAaImAgMnS	ptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
ještě	ještě	k6eAd1	ještě
jiná	jiný	k2eAgFnSc1d1	jiná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
nalezl	naleznout	k5eAaPmAgMnS	naleznout
odpověď	odpověď	k1gFnSc4	odpověď
ve	v	k7c6	v
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
na	na	k7c4	na
zážitek	zážitek	k1gInSc4	zážitek
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Příhoda	příhoda	k1gFnSc1	příhoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
vzpomněl	vzpomnít	k5eAaPmAgMnS	vzpomnít
<g/>
,	,	kIx,	,
hrála	hrát	k5eAaImAgFnS	hrát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
vývoji	vývoj	k1gInSc6	vývoj
k	k	k7c3	k
cestě	cesta	k1gFnSc3	cesta
šramany	šramany	k?	šramany
<g/>
:	:	kIx,	:
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
Šákja	Šákja	k1gMnSc1	Šákja
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
<g/>
,	,	kIx,	,
seděl	sedět	k5eAaImAgMnS	sedět
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
chladivém	chladivý	k2eAgInSc6d1	chladivý
stínu	stín	k1gInSc6	stín
jambovníku	jambovník	k1gInSc2	jambovník
<g/>
.	.	kIx.	.
</s>
<s>
Daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
žádostí	žádost	k1gFnPc2	žádost
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
nespásných	spásný	k2eNgFnPc2d1	nespásná
věcí	věc	k1gFnPc2	věc
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
jsem	být	k5eAaImIp1nS	být
prvního	první	k4xOgMnSc2	první
pohroužení	pohroužení	k1gNnSc2	pohroužení
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
spočívajícího	spočívající	k2eAgMnSc2d1	spočívající
v	v	k7c6	v
radosti	radost	k1gFnSc6	radost
a	a	k8xC	a
štěstí	štěstí	k1gNnSc6	štěstí
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
jsem	být	k5eAaImIp1nS	být
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
:	:	kIx,	:
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
skutečná	skutečný	k2eAgFnSc1d1	skutečná
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
alternativa	alternativa	k1gFnSc1	alternativa
askeze	askeze	k1gFnSc1	askeze
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
přerušit	přerušit	k5eAaPmF	přerušit
půst	půst	k1gInSc4	půst
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
posílil	posílit	k5eAaPmAgInS	posílit
rýží	rýže	k1gFnSc7	rýže
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyhublému	vyhublý	k2eAgMnSc3d1	vyhublý
asketovi	asketa	k1gMnSc3	asketa
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
okolojdoucí	okolojdoucí	k2eAgFnSc1d1	okolojdoucí
Sudžátá	Sudžátý	k2eAgFnSc1d1	Sudžátý
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
z	z	k7c2	z
blízké	blízký	k2eAgFnSc2d1	blízká
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
znovu	znovu	k6eAd1	znovu
nabytými	nabytý	k2eAgFnPc7d1	nabytá
silami	síla	k1gFnPc7	síla
se	se	k3xPyFc4	se
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
dne	den	k1gInSc2	den
zabýval	zabývat	k5eAaImAgInS	zabývat
novým	nový	k2eAgInSc7d1	nový
způsobem	způsob	k1gInSc7	způsob
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
naznačila	naznačit	k5eAaPmAgFnS	naznačit
jeho	jeho	k3xOp3gFnSc1	jeho
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
nálada	nálada	k1gFnSc1	nálada
zážitku	zážitek	k1gInSc2	zážitek
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
dosavadním	dosavadní	k2eAgNnSc7d1	dosavadní
utrpením	utrpení	k1gNnSc7	utrpení
půstu	půst	k1gInSc2	půst
a	a	k8xC	a
zadržováním	zadržování	k1gNnSc7	zadržování
dechu	dech	k1gInSc2	dech
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautam	k1gMnSc2	Gautam
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
ideologií	ideologie	k1gFnSc7	ideologie
<g/>
,	,	kIx,	,
že	že	k8xS	že
současné	současný	k2eAgNnSc1d1	současné
utrpení	utrpení	k1gNnSc1	utrpení
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
budoucímu	budoucí	k2eAgNnSc3d1	budoucí
štěstí	štěstí	k1gNnSc3	štěstí
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
o	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
radostnými	radostný	k2eAgInPc7d1	radostný
vjemy	vjem	k1gInPc7	vjem
<g/>
.	.	kIx.	.
</s>
<s>
Bál	bát	k5eAaImAgMnS	bát
se	se	k3xPyFc4	se
šťastného	šťastný	k2eAgInSc2d1	šťastný
pocitu	pocit	k1gInSc2	pocit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
jej	on	k3xPp3gMnSc4	on
akceptoval	akceptovat	k5eAaBmAgMnS	akceptovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
byl	být	k5eAaImAgMnS	být
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
žádostí	žádost	k1gFnPc2	žádost
a	a	k8xC	a
nespásných	spásný	k2eNgFnPc2d1	nespásná
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Gautamovou	Gautamový	k2eAgFnSc7d1	Gautamový
praxí	praxe	k1gFnSc7	praxe
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
asketických	asketický	k2eAgNnPc2d1	asketické
let	léto	k1gNnPc2	léto
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
podstatný	podstatný	k2eAgInSc4d1	podstatný
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Nauky	nauka	k1gFnPc1	nauka
o	o	k7c4	o
vysvobození	vysvobození	k1gNnSc4	vysvobození
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
namáhavou	namáhavý	k2eAgFnSc4d1	namáhavá
změnu	změna	k1gFnSc4	změna
přítomných	přítomný	k2eAgFnPc2d1	přítomná
podmínek	podmínka	k1gFnPc2	podmínka
člověka	člověk	k1gMnSc2	člověk
potlačením	potlačení	k1gNnSc7	potlačení
přirozených	přirozený	k2eAgFnPc2d1	přirozená
funkcí	funkce	k1gFnPc2	funkce
jako	jako	k8xC	jako
dýchání	dýchání	k1gNnSc2	dýchání
a	a	k8xC	a
přijímání	přijímání	k1gNnSc2	přijímání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
disciplínu	disciplína	k1gFnSc4	disciplína
těla	tělo	k1gNnSc2	tělo
nebo	nebo	k8xC	nebo
navození	navození	k1gNnSc2	navození
výjimečných	výjimečný	k2eAgInPc2d1	výjimečný
psychických	psychický	k2eAgInPc2d1	psychický
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Gautamův	Gautamův	k2eAgInSc1d1	Gautamův
zážitek	zážitek	k1gInSc1	zážitek
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
vyznačoval	vyznačovat	k5eAaImAgMnS	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
neměnilo	měnit	k5eNaImAgNnS	měnit
<g/>
,	,	kIx,	,
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
se	se	k3xPyFc4	se
neusilovalo	usilovat	k5eNaImAgNnS	usilovat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
prodlévání	prodlévání	k1gNnSc1	prodlévání
v	v	k7c6	v
chladivém	chladivý	k2eAgInSc6d1	chladivý
stínu	stín	k1gInSc6	stín
jambovníku	jambovník	k1gInSc2	jambovník
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
příjemném	příjemný	k2eAgNnSc6d1	příjemné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
žádostí	žádost	k1gFnPc2	žádost
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
štěstí	štěstí	k1gNnSc2	štěstí
bez	bez	k7c2	bez
přání	přání	k1gNnSc2	přání
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
seděl	sedět	k5eAaImAgMnS	sedět
<g/>
,	,	kIx,	,
spokojen	spokojen	k2eAgMnSc1d1	spokojen
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
právě	právě	k6eAd1	právě
bylo	být	k5eAaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Vnímal	vnímat	k5eAaImAgMnS	vnímat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc4	nic
víc	hodně	k6eAd2	hodně
si	se	k3xPyFc3	se
nepřál	přát	k5eNaImAgMnS	přát
<g/>
,	,	kIx,	,
nechtěl	chtít	k5eNaImAgMnS	chtít
nic	nic	k3yNnSc1	nic
měnit	měnit	k5eAaImF	měnit
na	na	k7c6	na
momentální	momentální	k2eAgFnSc6d1	momentální
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
omezí	omezit	k5eAaPmIp3nS	omezit
se	se	k3xPyFc4	se
na	na	k7c4	na
prosté	prostý	k2eAgNnSc4d1	prosté
pozorování	pozorování	k1gNnSc4	pozorování
existence	existence	k1gFnSc2	existence
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgNnSc2	svůj
sezení	sezení	k1gNnSc2	sezení
<g/>
,	,	kIx,	,
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
cítění	cítění	k1gNnSc2	cítění
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
pokusil	pokusit	k5eAaPmAgMnS	pokusit
opět	opět	k6eAd1	opět
vnímat	vnímat	k5eAaImF	vnímat
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
bez	bez	k7c2	bez
přání	přání	k1gNnSc2	přání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgInSc6	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vnitřnímu	vnitřní	k2eAgInSc3d1	vnitřní
prožitku	prožitek	k1gInSc3	prožitek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
popsal	popsat	k5eAaPmAgInS	popsat
jako	jako	k9	jako
proces	proces	k1gInSc1	proces
čtyř	čtyři	k4xCgInPc2	čtyři
vnorů	vnor	k1gInPc2	vnor
(	(	kIx(	(
<g/>
dhján	dhján	k1gInSc1	dhján
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
vnor	vnor	k1gMnSc1	vnor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
prožitku	prožitek	k1gInSc3	prožitek
ze	z	k7c2	z
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
stav	stav	k1gInSc1	stav
bez	bez	k7c2	bez
žádosti	žádost	k1gFnSc2	žádost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
přání	přání	k1gNnPc2	přání
změnit	změnit	k5eAaPmF	změnit
okamžik	okamžik	k1gInSc4	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
se	se	k3xPyFc4	se
spojovaly	spojovat	k5eAaImAgFnP	spojovat
s	s	k7c7	s
bezprostředním	bezprostřední	k2eAgNnSc7d1	bezprostřední
vnímáním	vnímání	k1gNnSc7	vnímání
<g/>
:	:	kIx,	:
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
předmětem	předmět	k1gInSc7	předmět
vnímání	vnímání	k1gNnSc2	vnímání
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
kroužily	kroužit	k5eAaImAgFnP	kroužit
myšlenky	myšlenka	k1gFnPc4	myšlenka
okolo	okolo	k7c2	okolo
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
registrovaly	registrovat	k5eAaBmAgInP	registrovat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
nádechy	nádech	k1gInPc1	nádech
a	a	k8xC	a
výdechy	výdech	k1gInPc1	výdech
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nenucené	nucený	k2eNgNnSc1d1	nenucené
přijetí	přijetí	k1gNnSc1	přijetí
reflektování	reflektování	k1gNnSc2	reflektování
ve	v	k7c6	v
cvičení	cvičení	k1gNnSc6	cvičení
bylo	být	k5eAaImAgNnS	být
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
asketické	asketický	k2eAgFnSc3d1	asketická
cestě	cesta	k1gFnSc3	cesta
patřilo	patřit	k5eAaImAgNnS	patřit
zastavit	zastavit	k5eAaPmF	zastavit
vedle	vedle	k7c2	vedle
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
funkcí	funkce	k1gFnPc2	funkce
také	také	k9	také
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
;	;	kIx,	;
Gautama	Gautama	k1gFnSc1	Gautama
uvádí	uvádět	k5eAaImIp3nS	uvádět
o	o	k7c6	o
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
hledání	hledání	k1gNnSc2	hledání
<g/>
:	:	kIx,	:
Zatínal	zatínat	k5eAaImAgMnS	zatínat
jsem	být	k5eAaImIp1nS	být
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
tiskl	tisknout	k5eAaImAgMnS	tisknout
jazyk	jazyk	k1gInSc4	jazyk
na	na	k7c4	na
patro	patro	k1gNnSc4	patro
a	a	k8xC	a
zastavoval	zastavovat	k5eAaImAgMnS	zastavovat
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
potlačoval	potlačovat	k5eAaImAgMnS	potlačovat
a	a	k8xC	a
dusil	dusit	k5eAaImAgMnS	dusit
jsem	být	k5eAaImIp1nS	být
jej	on	k3xPp3gInSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pot	pot	k1gInSc1	pot
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
řinul	řinout	k5eAaImAgMnS	řinout
s	s	k7c7	s
ramenou	rameno	k1gNnPc6	rameno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
silný	silný	k2eAgMnSc1d1	silný
muž	muž	k1gMnSc1	muž
uchopil	uchopit	k5eAaPmAgMnS	uchopit
slabšího	slabý	k2eAgMnSc4d2	slabší
za	za	k7c4	za
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
bolestivého	bolestivý	k2eAgInSc2d1	bolestivý
postupu	postup	k1gInSc2	postup
se	se	k3xPyFc4	se
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
formě	forma	k1gFnSc6	forma
cvičení	cvičení	k1gNnSc2	cvičení
objevoval	objevovat	k5eAaImAgInS	objevovat
blažený	blažený	k2eAgInSc1d1	blažený
tělesný	tělesný	k2eAgInSc1d1	tělesný
pocit	pocit	k1gInSc1	pocit
a	a	k8xC	a
radostné	radostný	k2eAgFnPc1d1	radostná
emoce	emoce	k1gFnPc1	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautam	k1gMnSc4	Gautam
pak	pak	k6eAd1	pak
zažil	zažít	k5eAaPmAgMnS	zažít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
myšlení	myšlení	k1gNnSc4	myšlení
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
samo	sám	k3xTgNnSc1	sám
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
uklidnilo	uklidnit	k5eAaPmAgNnS	uklidnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
tichém	tichý	k2eAgInSc6d1	tichý
a	a	k8xC	a
soustředěném	soustředěný	k2eAgInSc6d1	soustředěný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Dostavilo	dostavit	k5eAaPmAgNnS	dostavit
se	se	k3xPyFc4	se
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
ztišení	ztišení	k1gNnSc1	ztišení
<g/>
,	,	kIx,	,
nadále	nadále	k6eAd1	nadále
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
pocitem	pocit	k1gInSc7	pocit
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
radosti	radost	k1gFnPc4	radost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
označil	označit	k5eAaPmAgInS	označit
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
vnor	vnor	k1gInSc1	vnor
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
vnor	vnor	k1gInSc1	vnor
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vyrovnaností	vyrovnanost	k1gFnSc7	vyrovnanost
<g/>
:	:	kIx,	:
setrvával	setrvávat	k5eAaImAgMnS	setrvávat
v	v	k7c6	v
bdělosti	bdělost	k1gFnSc6	bdělost
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
dělo	dít	k5eAaImAgNnS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Nerušen	rušen	k2eNgInSc1d1	nerušen
myšlenkami	myšlenka	k1gFnPc7	myšlenka
nebo	nebo	k8xC	nebo
přáními	přání	k1gNnPc7	přání
<g/>
,	,	kIx,	,
prostě	prostě	k6eAd1	prostě
vnímal	vnímat	k5eAaImAgInS	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Radostné	radostný	k2eAgFnPc1d1	radostná
emoce	emoce	k1gFnPc1	emoce
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
odezněly	odeznět	k5eAaPmAgInP	odeznět
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tělesný	tělesný	k2eAgInSc4d1	tělesný
pocit	pocit	k1gInSc4	pocit
štěstí	štěstí	k1gNnSc2	štěstí
přetrvával	přetrvávat	k5eAaImAgMnS	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
nechal	nechat	k5eAaPmAgMnS	nechat
za	za	k7c7	za
sebou	se	k3xPyFc7	se
také	také	k6eAd1	také
toto	tento	k3xDgNnSc4	tento
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
vnoru	vnor	k1gInSc6	vnor
zažil	zažít	k5eAaPmAgMnS	zažít
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
pouze	pouze	k6eAd1	pouze
vyrovnaností	vyrovnanost	k1gFnSc7	vyrovnanost
a	a	k8xC	a
čistým	čistý	k2eAgNnSc7d1	čisté
bytím	bytí	k1gNnSc7	bytí
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
<g/>
:	:	kIx,	:
prostě	prostě	k9	prostě
vnímal	vnímat	k5eAaImAgInS	vnímat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
očekávání	očekávání	k1gNnSc1	očekávání
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
zármutek	zármutek	k1gInSc1	zármutek
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
radost	radost	k1gFnSc4	radost
nekalily	kalit	k5eNaImAgInP	kalit
jeho	jeho	k3xOp3gInPc1	jeho
vnímání	vnímání	k1gNnSc4	vnímání
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
Gautama	Gautamum	k1gNnSc2	Gautamum
věnoval	věnovat	k5eAaImAgMnS	věnovat
těmto	tento	k3xDgNnPc3	tento
pohroužením	pohroužení	k1gNnPc3	pohroužení
<g/>
.	.	kIx.	.
</s>
<s>
Myšlení	myšlení	k1gNnSc1	myšlení
nebo	nebo	k8xC	nebo
pociťování	pociťování	k1gNnSc1	pociťování
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
nimž	jenž	k3xRgFnPc3	jenž
jako	jako	k9	jako
asketa	asketa	k1gMnSc1	asketa
bojoval	bojovat	k5eAaImAgMnS	bojovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nevázal	vázat	k5eNaImAgMnS	vázat
na	na	k7c4	na
svět	svět	k1gInSc4	svět
a	a	k8xC	a
znovuzrození	znovuzrození	k1gNnSc1	znovuzrození
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
jeho	jeho	k3xOp3gInPc7	jeho
motivujícími	motivující	k2eAgInPc7d1	motivující
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pohroužení	pohroužení	k1gNnSc2	pohroužení
ztratily	ztratit	k5eAaPmAgFnP	ztratit
na	na	k7c6	na
významu	význam	k1gInSc6	význam
a	a	k8xC	a
uvolnily	uvolnit	k5eAaPmAgFnP	uvolnit
místo	místo	k7c2	místo
soustředěnému	soustředěný	k2eAgInSc3d1	soustředěný
stavu	stav	k1gInSc3	stav
čistého	čistý	k2eAgNnSc2d1	čisté
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
si	se	k3xPyFc3	se
Gautama	Gautama	k1gNnSc4	Gautama
sedl	sednout	k5eAaPmAgMnS	sednout
se	s	k7c7	s
zkříženýma	zkřížený	k2eAgFnPc7d1	zkřížená
nohama	noha	k1gFnPc7	noha
pod	pod	k7c4	pod
strom	strom	k1gInSc4	strom
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevstane	vstát	k5eNaPmIp3nS	vstát
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
vysvobození	vysvobození	k1gNnSc1	vysvobození
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
přistoupil	přistoupit	k5eAaPmAgInS	přistoupit
Mára	Márus	k1gMnSc4	Márus
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
udivenou	udivený	k2eAgFnSc4d1	udivená
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc4	on
Gautama	Gautama	k1gFnSc1	Gautama
nebojí	bát	k5eNaImIp3nS	bát
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
dobrotivosti	dobrotivost	k1gFnSc3	dobrotivost
a	a	k8xC	a
jiným	jiný	k2eAgFnPc3d1	jiná
ctnostem	ctnost	k1gFnPc3	ctnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Gautama	Gautama	k1gNnSc1	Gautama
praktikoval	praktikovat	k5eAaImAgInS	praktikovat
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
statečný	statečný	k2eAgMnSc1d1	statečný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Mára	Mára	k1gMnSc1	Mára
pochyboval	pochybovat	k5eAaImAgMnS	pochybovat
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
tyto	tento	k3xDgFnPc4	tento
věci	věc	k1gFnPc4	věc
dosvědčit	dosvědčit	k5eAaPmF	dosvědčit
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
Gautama	Gautama	k1gNnSc4	Gautama
<g/>
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
širá	širý	k2eAgFnSc1d1	širá
země	země	k1gFnSc1	země
ať	ať	k9	ať
je	být	k5eAaImIp3nS	být
mým	můj	k3xOp1gMnSc7	můj
svědkem	svědek	k1gMnSc7	svědek
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zachvěje	zachvět	k5eAaPmIp3nS	zachvět
v	v	k7c6	v
šestkrát	šestkrát	k6eAd1	šestkrát
sedmi	sedm	k4xCc6	sedm
nárazech	náraz	k1gInPc6	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
zvedlo	zvednout	k5eAaPmAgNnS	zvednout
dunivé	dunivý	k2eAgNnSc1d1	dunivé
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
legenda	legenda	k1gFnSc1	legenda
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
častému	častý	k2eAgNnSc3d1	časté
zobrazování	zobrazování	k1gNnSc2	zobrazování
Gautamy	Gautam	k1gInPc4	Gautam
v	v	k7c4	v
umění	umění	k1gNnSc4	umění
<g/>
:	:	kIx,	:
Pravicí	pravice	k1gFnSc7	pravice
se	se	k3xPyFc4	se
sedě	sedě	k6eAd1	sedě
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tak	tak	k6eAd1	tak
volá	volat	k5eAaImIp3nS	volat
za	za	k7c4	za
svědka	svědek	k1gMnSc4	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
vyzvedla	vyzvednout	k5eAaPmAgFnS	vyzvednout
mytickou	mytický	k2eAgFnSc4d1	mytická
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
triumfu	triumf	k1gInSc6	triumf
nad	nad	k7c7	nad
Márou	Mára	k1gFnSc7	Mára
nad	nad	k7c4	nad
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
probuzení	probuzení	k1gNnSc6	probuzení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vysvobození	vysvobození	k1gNnSc1	vysvobození
ze	z	k7c2	z
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
znamená	znamenat	k5eAaImIp3nS	znamenat
současně	současně	k6eAd1	současně
vítězství	vítězství	k1gNnSc1	vítězství
nad	nad	k7c7	nad
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
stránka	stránka	k1gFnSc1	stránka
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
vnorem	vnor	k1gInSc7	vnor
získal	získat	k5eAaPmAgInS	získat
Gautama	Gautama	k1gNnSc4	Gautama
schopnost	schopnost	k1gFnSc4	schopnost
být	být	k5eAaImF	být
dokonale	dokonale	k6eAd1	dokonale
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
usiloval	usilovat	k5eAaImAgMnS	usilovat
v	v	k7c6	v
myšlenkách	myšlenka	k1gFnPc6	myšlenka
nebo	nebo	k8xC	nebo
žádostech	žádost	k1gFnPc6	žádost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nastala	nastat	k5eAaPmAgFnS	nastat
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
výchozí	výchozí	k2eAgInSc4d1	výchozí
bod	bod	k1gInSc4	bod
všech	všecek	k3xTgFnPc2	všecek
nauk	nauka	k1gFnPc2	nauka
o	o	k7c6	o
vysvobození	vysvobození	k1gNnSc6	vysvobození
<g/>
,	,	kIx,	,
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
zapletení	zapletení	k1gNnSc4	zapletení
do	do	k7c2	do
koloběhu	koloběh	k1gInSc2	koloběh
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
a	a	k8xC	a
tak	tak	k6eAd1	tak
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
probuzení	probuzení	k1gNnSc4	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nasměroval	nasměrovat	k5eAaPmAgInS	nasměrovat
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
zpřítomnění	zpřítomnění	k1gNnSc4	zpřítomnění
předešlých	předešlý	k2eAgFnPc2d1	předešlá
existencí	existence	k1gFnPc2	existence
<g/>
,	,	kIx,	,
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
vědomí	vědomí	k1gNnSc2	vědomí
biografie	biografie	k1gFnSc2	biografie
minulých	minulý	k2eAgInPc2d1	minulý
životů	život	k1gInPc2	život
<g/>
:	:	kIx,	:
Tam	tam	k6eAd1	tam
jsem	být	k5eAaImIp1nS	být
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
byl	být	k5eAaImAgInS	být
můj	můj	k3xOp1gInSc4	můj
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
povolání	povolání	k1gNnSc4	povolání
<g/>
,	,	kIx,	,
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
zármutek	zármutek	k1gInSc4	zármutek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
má	můj	k3xOp1gFnSc1	můj
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
tam	tam	k6eAd1	tam
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
zrodil	zrodit	k5eAaPmAgMnS	zrodit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
byl	být	k5eAaImAgInS	být
vědom	vědom	k2eAgInSc4d1	vědom
statisíce	statisíce	k1gInPc4	statisíce
předchozích	předchozí	k2eAgMnPc2d1	předchozí
životů	život	k1gInPc2	život
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
sahala	sahat	k5eAaImAgFnS	sahat
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
do	do	k7c2	do
dříve	dříve	k6eAd2	dříve
existujících	existující	k2eAgInPc2d1	existující
světů	svět	k1gInPc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
světy	svět	k1gInPc1	svět
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
životy	život	k1gInPc1	život
následují	následovat	k5eAaImIp3nP	následovat
jeden	jeden	k4xCgMnSc1	jeden
za	za	k7c4	za
druhým	druhý	k4xOgNnSc7	druhý
v	v	k7c6	v
nekonečném	konečný	k2eNgNnSc6d1	nekonečné
zanikání	zanikání	k1gNnSc6	zanikání
a	a	k8xC	a
vznikání	vznikání	k1gNnSc6	vznikání
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
Gautama	Gautamum	k1gNnSc2	Gautamum
obrátil	obrátit	k5eAaPmAgMnS	obrátit
k	k	k7c3	k
zákonům	zákon	k1gInPc3	zákon
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
.	.	kIx.	.
</s>
<s>
Pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
mizení	mizení	k1gNnSc4	mizení
a	a	k8xC	a
objevování	objevování	k1gNnSc4	objevování
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
v	v	k7c6	v
myšlenkách	myšlenka	k1gFnPc6	myšlenka
<g/>
,	,	kIx,	,
slovech	slovo	k1gNnPc6	slovo
a	a	k8xC	a
činech	čin	k1gInPc6	čin
jednaly	jednat	k5eAaImAgFnP	jednat
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
dostávají	dostávat	k5eAaImIp3nP	dostávat
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
do	do	k7c2	do
dolního	dolní	k2eAgInSc2d1	dolní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
konaly	konat	k5eAaImAgFnP	konat
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
příznivějšího	příznivý	k2eAgNnSc2d2	příznivější
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
lidé	člověk	k1gMnPc1	člověk
svými	svůj	k3xOyFgInPc7	svůj
činy	čin	k1gInPc7	čin
(	(	kIx(	(
<g/>
karmou	karma	k1gFnSc7	karma
<g/>
)	)	kIx)	)
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
průběhu	průběh	k1gInSc3	průběh
vlastního	vlastní	k2eAgInSc2d1	vlastní
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
koloběhu	koloběh	k1gInSc6	koloběh
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
podrobeni	podroben	k2eAgMnPc1d1	podroben
stálému	stálý	k2eAgInSc3d1	stálý
vzestupu	vzestup	k1gInSc3	vzestup
a	a	k8xC	a
sestupu	sestup	k1gInSc3	sestup
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Gautama	Gautama	k1gNnSc4	Gautama
namířil	namířit	k5eAaPmAgMnS	namířit
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
vystoupení	vystoupení	k1gNnSc4	vystoupení
z	z	k7c2	z
koloběhu	koloběh	k1gInSc2	koloběh
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
,	,	kIx,	,
na	na	k7c4	na
zničení	zničení	k1gNnSc4	zničení
zákalů	zákal	k1gInPc2	zákal
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
označoval	označovat	k5eAaImAgInS	označovat
v	v	k7c6	v
džinistické	džinistický	k2eAgFnSc6d1	džinistická
terminologii	terminologie	k1gFnSc6	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Džinisté	Džinista	k1gMnPc1	Džinista
vysvětlovali	vysvětlovat	k5eAaImAgMnP	vysvětlovat
vztah	vztah	k1gInSc4	vztah
trvalého	trvalý	k2eAgNnSc2d1	trvalé
džívy	džíva	k1gFnPc1	džíva
k	k	k7c3	k
pomíjivým	pomíjivý	k2eAgNnPc3d1	pomíjivé
tělům	tělo	k1gNnPc3	tělo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
džívy	džíva	k1gFnSc2	džíva
vtékají	vtékat	k5eAaImIp3nP	vtékat
během	během	k7c2	během
tělesných	tělesný	k2eAgInPc2d1	tělesný
skutků	skutek	k1gInPc2	skutek
jemné	jemný	k2eAgFnSc2d1	jemná
materiální	materiální	k2eAgFnSc2d1	materiální
substance	substance	k1gFnSc2	substance
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ho	on	k3xPp3gMnSc4	on
vážou	vázat	k5eAaImIp3nP	vázat
k	k	k7c3	k
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zničení	zničení	k1gNnSc1	zničení
těchto	tento	k3xDgInPc2	tento
zákalů	zákal	k1gInPc2	zákal
(	(	kIx(	(
<g/>
ásrava	ásrava	k1gFnSc1	ásrava
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
osvobozením	osvobození	k1gNnSc7	osvobození
džívy	džíva	k1gFnSc2	džíva
ze	z	k7c2	z
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
se	se	k3xPyFc4	se
již	již	k6eAd1	již
vzdal	vzdát	k5eAaPmAgMnS	vzdát
úsilí	úsilí	k1gNnSc4	úsilí
zastavit	zastavit	k5eAaPmF	zastavit
vlivy	vliv	k1gInPc4	vliv
potlačením	potlačení	k1gNnSc7	potlačení
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Překonal	překonat	k5eAaPmAgMnS	překonat
znovuzrození	znovuzrození	k1gNnSc1	znovuzrození
poznáním	poznání	k1gNnSc7	poznání
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
buddhistické	buddhistický	k2eAgInPc1d1	buddhistický
texty	text	k1gInPc1	text
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
ušlechtilých	ušlechtilý	k2eAgFnPc6d1	ušlechtilá
pravdách	pravda	k1gFnPc6	pravda
jako	jako	k8xS	jako
esenci	esence	k1gFnSc6	esence
Gautamovy	Gautamův	k2eAgFnSc2d1	Gautamův
nauky	nauka	k1gFnSc2	nauka
<g/>
:	:	kIx,	:
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
strast	strast	k1gFnSc1	strast
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
strasti	strast	k1gFnSc2	strast
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
zánik	zánik	k1gInSc1	zánik
strasti	strast	k1gFnSc2	strast
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
strasti	strast	k1gFnSc2	strast
<g/>
.	.	kIx.	.
</s>
<s>
Idea	idea	k1gFnSc1	idea
<g/>
,	,	kIx,	,
že	že	k8xS	že
bezprostřední	bezprostřední	k2eAgNnSc4d1	bezprostřední
a	a	k8xC	a
soustředěné	soustředěný	k2eAgNnSc4d1	soustředěné
pozorování	pozorování	k1gNnSc4	pozorování
věci	věc	k1gFnSc2	věc
odhalí	odhalit	k5eAaPmIp3nP	odhalit
skryté	skrytý	k2eAgFnPc1d1	skrytá
podmínky	podmínka	k1gFnPc1	podmínka
a	a	k8xC	a
principy	princip	k1gInPc1	princip
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základním	základní	k2eAgInSc7d1	základní
názorem	názor	k1gInSc7	názor
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
poznal	poznat	k5eAaPmAgInS	poznat
život	život	k1gInSc1	život
jako	jako	k8xC	jako
sled	sled	k1gInSc1	sled
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInSc7	jeho
základem	základ	k1gInSc7	základ
a	a	k8xC	a
východiskem	východisko	k1gNnSc7	východisko
<g/>
,	,	kIx,	,
Gautama	Gautam	k1gMnSc2	Gautam
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
:	:	kIx,	:
Koloběh	koloběh	k1gInSc1	koloběh
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
je	být	k5eAaImIp3nS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Nápadné	nápadný	k2eAgNnSc1d1	nápadné
na	na	k7c4	na
líčení	líčení	k1gNnSc4	líčení
probuzení	probuzení	k1gNnSc2	probuzení
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
nenalezneme	naleznout	k5eNaPmIp1nP	naleznout
ani	ani	k8xC	ani
slovo	slovo	k1gNnSc4	slovo
o	o	k7c4	o
osvobození	osvobození	k1gNnSc4	osvobození
džívy	džíva	k1gFnSc2	džíva
nebo	nebo	k8xC	nebo
átmana	átmana	k1gFnSc1	átmana
ze	z	k7c2	z
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
<g/>
.	.	kIx.	.
</s>
<s>
Neznamená	znamenat	k5eNaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
Gautama	Gautam	k1gMnSc4	Gautam
objevil	objevit	k5eAaPmAgMnS	objevit
za	za	k7c7	za
pomíjivým	pomíjivý	k2eAgInSc7d1	pomíjivý
světem	svět	k1gInSc7	svět
něco	něco	k3yInSc4	něco
trvalého	trvalý	k2eAgNnSc2d1	trvalé
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
pomíjivost	pomíjivost	k1gFnSc1	pomíjivost
tvořila	tvořit	k5eAaImAgFnS	tvořit
obsah	obsah	k1gInSc4	obsah
probuzení	probuzení	k1gNnSc2	probuzení
<g/>
,	,	kIx,	,
proces	proces	k1gInSc1	proces
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
,	,	kIx,	,
zanikání	zanikání	k1gNnSc2	zanikání
a	a	k8xC	a
vznikání	vznikání	k1gNnSc2	vznikání
světů	svět	k1gInPc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
čtyři	čtyři	k4xCgFnPc1	čtyři
ušlechtilé	ušlechtilý	k2eAgFnPc1d1	ušlechtilá
pravdy	pravda	k1gFnPc1	pravda
jako	jako	k8xS	jako
cesta	cesta	k1gFnSc1	cesta
ze	z	k7c2	z
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
a	a	k8xC	a
cíl	cíl	k1gInSc1	cíl
nehovoří	hovořit	k5eNaImIp3nS	hovořit
o	o	k7c6	o
nalezení	nalezení	k1gNnSc6	nalezení
něčeho	něco	k3yInSc2	něco
trvalého	trvalý	k2eAgNnSc2d1	trvalé
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
strasti	strast	k1gFnSc2	strast
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
lišila	lišit	k5eAaImAgFnS	lišit
nejen	nejen	k6eAd1	nejen
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Gautama	Gautama	k1gFnSc1	Gautama
zvolil	zvolit	k5eAaPmAgMnS	zvolit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
její	její	k3xOp3gInSc4	její
cíl	cíl	k1gInSc4	cíl
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
indických	indický	k2eAgMnPc2d1	indický
učitelů	učitel	k1gMnPc2	učitel
vysvobození	vysvobození	k1gNnSc2	vysvobození
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sedm	sedm	k4xCc4	sedm
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
následovaly	následovat	k5eAaImAgInP	následovat
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
<g/>
,	,	kIx,	,
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
Gautama	Gautama	k1gNnSc4	Gautama
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
prožil	prožít	k5eAaPmAgMnS	prožít
<g/>
.	.	kIx.	.
</s>
<s>
Našel	najít	k5eAaPmAgMnS	najít
nový	nový	k2eAgInSc4d1	nový
výklad	výklad	k1gInSc4	výklad
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
jeho	jeho	k3xOp3gFnPc4	jeho
nové	nový	k2eAgFnPc4d1	nová
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
:	:	kIx,	:
nikoli	nikoli	k9	nikoli
neproměnný	proměnný	k2eNgMnSc1d1	neproměnný
džíva	džíva	k6eAd1	džíva
nebo	nebo	k8xC	nebo
átman	átman	k1gInSc1	átman
putuje	putovat	k5eAaImIp3nS	putovat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
tělech	tělo	k1gNnPc6	tělo
smrtí	smrtit	k5eAaImIp3nP	smrtit
a	a	k8xC	a
zrozením	zrození	k1gNnSc7	zrození
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
proces	proces	k1gInSc1	proces
bytí	bytí	k1gNnSc2	bytí
bez	bez	k7c2	bez
začátku	začátek	k1gInSc2	začátek
a	a	k8xC	a
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
přináší	přinášet	k5eAaImIp3nS	přinášet
stále	stále	k6eAd1	stále
nové	nový	k2eAgInPc4d1	nový
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
spojuje	spojovat	k5eAaImIp3nS	spojovat
za	za	k7c7	za
sebou	se	k3xPyFc7	se
následující	následující	k2eAgInPc4d1	následující
životy	život	k1gInPc4	život
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
neměnná	neměnný	k2eAgFnSc1d1	neměnná
podstata	podstata	k1gFnSc1	podstata
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
nepřerušované	přerušovaný	k2eNgNnSc1d1	nepřerušované
plynutí	plynutí	k1gNnSc1	plynutí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
jedna	jeden	k4xCgFnSc1	jeden
pomíjivá	pomíjivý	k2eAgFnSc1d1	pomíjivá
bytost	bytost	k1gFnSc1	bytost
za	za	k7c7	za
druhou	druhý	k4xOgFnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
tvarů	tvar	k1gInPc2	tvar
přitom	přitom	k6eAd1	přitom
není	být	k5eNaImIp3nS	být
svévolný	svévolný	k2eAgInSc1d1	svévolný
<g/>
;	;	kIx,	;
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
zákonitosti	zákonitost	k1gFnSc2	zákonitost
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvalita	kvalita	k1gFnSc1	kvalita
přítomných	přítomný	k2eAgInPc2d1	přítomný
činů	čin	k1gInPc2	čin
(	(	kIx(	(
<g/>
karma	karma	k1gFnSc1	karma
<g/>
)	)	kIx)	)
formuje	formovat	k5eAaImIp3nS	formovat
činy	čin	k1gInPc4	čin
budoucího	budoucí	k2eAgInSc2d1	budoucí
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
souvislosti	souvislost	k1gFnPc4	souvislost
shrnul	shrnout	k5eAaPmAgMnS	shrnout
Gautama	Gautama	k1gNnSc1	Gautama
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
sedmého	sedmý	k4xOgInSc2	sedmý
dne	den	k1gInSc2	den
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
do	do	k7c2	do
poučky	poučka	k1gFnSc2	poučka
o	o	k7c6	o
závislém	závislý	k2eAgNnSc6d1	závislé
vznikání	vznikání	k1gNnSc6	vznikání
<g/>
,	,	kIx,	,
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
procesu	proces	k1gInSc2	proces
znovuzrozování	znovuzrozování	k1gNnSc2	znovuzrozování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jádrem	jádro	k1gNnSc7	jádro
filosofického	filosofický	k2eAgNnSc2d1	filosofické
učení	učení	k1gNnSc2	učení
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
týden	týden	k1gInSc1	týden
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
šel	jít	k5eAaImAgMnS	jít
kolem	kolem	k7c2	kolem
bráhman	bráhma	k1gNnPc2	bráhma
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
jej	on	k3xPp3gMnSc4	on
poučil	poučit	k5eAaPmAgInS	poučit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
pravého	pravý	k2eAgMnSc2d1	pravý
bráhmana	bráhman	k1gMnSc2	bráhman
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
nikoliv	nikoliv	k9	nikoliv
narození	narození	k1gNnSc1	narození
v	v	k7c6	v
kastě	kasta	k1gFnSc6	kasta
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
překonání	překonání	k1gNnPc4	překonání
špatného	špatný	k2eAgNnSc2d1	špatné
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
tím	ten	k3xDgNnSc7	ten
chtěla	chtít	k5eAaImAgFnS	chtít
tradice	tradice	k1gFnSc1	tradice
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c6	o
probuzení	probuzení	k1gNnSc6	probuzení
nechat	nechat	k5eAaPmF	nechat
zaznít	zaznít	k5eAaPmF	zaznít
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
myšlenku	myšlenka	k1gFnSc4	myšlenka
dalšího	další	k2eAgNnSc2d1	další
Gautamova	Gautamův	k2eAgNnSc2d1	Gautamovo
působení	působení	k1gNnSc2	působení
<g/>
:	:	kIx,	:
Neplatí	platit	k5eNaImIp3nS	platit
výsada	výsada	k1gFnSc1	výsada
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
vlastní	vlastní	k2eAgNnSc4d1	vlastní
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
týden	týden	k1gInSc4	týden
nepřetržitě	přetržitě	k6eNd1	přetržitě
pršelo	pršet	k5eAaImAgNnS	pršet
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
seděl	sedět	k5eAaImAgMnS	sedět
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
a	a	k8xC	a
větru	vítr	k1gInSc6	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
mýtus	mýtus	k1gInSc1	mýtus
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ruch	ruch	k1gInSc1	ruch
světa	svět	k1gInSc2	svět
nemůže	moct	k5eNaImIp3nS	moct
Buddhu	Buddha	k1gMnSc4	Buddha
vyrušit	vyrušit	k5eAaPmF	vyrušit
<g/>
:	:	kIx,	:
Mučalinda	Mučalinda	k1gFnSc1	Mučalinda
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Nágů	Nág	k1gMnPc2	Nág
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgMnPc2d1	přírodní
duchů	duch	k1gMnPc2	duch
s	s	k7c7	s
hadí	hadí	k2eAgFnSc7d1	hadí
podobou	podoba	k1gFnSc7	podoba
<g/>
,	,	kIx,	,
chránil	chránit	k5eAaImAgInS	chránit
Gautamu	Gautam	k1gInSc2	Gautam
před	před	k7c7	před
přívalem	příval	k1gInSc7	příval
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
prvkem	prvek	k1gInSc7	prvek
buddhistického	buddhistický	k2eAgNnSc2d1	buddhistické
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
motiv	motiv	k1gInSc1	motiv
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
král	král	k1gMnSc1	král
Nágů	Nág	k1gInPc2	Nág
s	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
svých	svůj	k3xOyFgMnPc2	svůj
sedmi	sedm	k4xCc2	sedm
hlav	hlava	k1gFnPc2	hlava
zaštiťuje	zaštiťovat	k5eAaImIp3nS	zaštiťovat
Gautamu	Gautam	k1gInSc2	Gautam
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bouřce	bouřka	k1gFnSc6	bouřka
přišla	přijít	k5eAaPmAgFnS	přijít
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
obchodní	obchodní	k2eAgFnSc1d1	obchodní
karavana	karavana	k1gFnSc1	karavana
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
Trápusa	Trápus	k1gMnSc2	Trápus
a	a	k8xC	a
Bhallika	Bhallik	k1gMnSc2	Bhallik
<g/>
,	,	kIx,	,
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
Gautamovi	Gautamův	k2eAgMnPc1d1	Gautamův
své	svůj	k3xOyFgNnSc4	svůj
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
posílil	posílit	k5eAaPmAgInS	posílit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
osobnost	osobnost	k1gFnSc1	osobnost
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
tak	tak	k6eAd1	tak
zapůsobila	zapůsobit	k5eAaPmAgFnS	zapůsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
putovali	putovat	k5eAaImAgMnP	putovat
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
spontánně	spontánně	k6eAd1	spontánně
za	za	k7c7	za
jeho	jeho	k3xOp3gMnPc7	jeho
žáky	žák	k1gMnPc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
až	až	k9	až
dosud	dosud	k6eAd1	dosud
nemyslel	myslet	k5eNaImAgMnS	myslet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
sdílel	sdílet	k5eAaImAgInS	sdílet
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
obchodníky	obchodník	k1gMnPc7	obchodník
přineslo	přinést	k5eAaPmAgNnS	přinést
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
objevené	objevený	k2eAgInPc4d1	objevený
předávat	předávat	k5eAaImF	předávat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Pochyboval	pochybovat	k5eAaImAgMnS	pochybovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gFnPc1	jeho
zkušenosti	zkušenost	k1gFnPc1	zkušenost
srozumitelné	srozumitelný	k2eAgFnPc1d1	srozumitelná
ostatním	ostatní	k2eAgMnPc3d1	ostatní
<g/>
:	:	kIx,	:
Kdybych	kdyby	kYmCp1nS	kdyby
hlásal	hlásat	k5eAaImAgMnS	hlásat
nauku	nauka	k1gFnSc4	nauka
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
bych	by	kYmCp1nS	by
pochopen	pochopen	k2eAgMnSc1d1	pochopen
<g/>
,	,	kIx,	,
přineslo	přinést	k5eAaPmAgNnS	přinést
by	by	kYmCp3nS	by
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
nesmyslnou	smyslný	k2eNgFnSc4d1	nesmyslná
<g/>
)	)	kIx)	)
námahu	námaha	k1gFnSc4	námaha
a	a	k8xC	a
vysílení	vysílení	k1gNnSc4	vysílení
<g/>
.	.	kIx.	.
</s>
<s>
Bráhma	Bráhma	k1gMnSc1	Bráhma
Svajampati	Svajampati	k1gMnSc1	Svajampati
<g/>
,	,	kIx,	,
mocný	mocný	k2eAgMnSc1d1	mocný
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
vnímal	vnímat	k5eAaImAgInS	vnímat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
nebeském	nebeský	k2eAgInSc6d1	nebeský
světě	svět	k1gInSc6	svět
tyto	tento	k3xDgFnPc4	tento
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
zjevil	zjevit	k5eAaPmAgMnS	zjevit
se	se	k3xPyFc4	se
před	před	k7c7	před
Gautamou	Gautama	k1gFnSc7	Gautama
"	"	kIx"	"
<g/>
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yRgFnSc7	jaký
silný	silný	k2eAgMnSc1d1	silný
muž	muž	k1gMnSc1	muž
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
svou	svůj	k3xOyFgFnSc4	svůj
paži	paže	k1gFnSc4	paže
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
gestu	gesto	k1gNnSc6	gesto
hluboké	hluboký	k2eAgFnSc2d1	hluboká
úcty	úcta	k1gFnSc2	úcta
obnažil	obnažit	k5eAaPmAgMnS	obnažit
bůh	bůh	k1gMnSc1	bůh
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
poklekl	pokleknout	k5eAaPmAgMnS	pokleknout
a	a	k8xC	a
prosil	prosít	k5eAaPmAgMnS	prosít
Gautamu	Gautam	k1gInSc3	Gautam
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvěstoval	zvěstovat	k5eAaImAgMnS	zvěstovat
svou	svůj	k3xOyFgFnSc4	svůj
nauku	nauka	k1gFnSc4	nauka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
jen	jen	k9	jen
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nP	by
porozuměli	porozumět	k5eAaPmAgMnP	porozumět
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
učinit	učinit	k5eAaImF	učinit
kvůli	kvůli	k7c3	kvůli
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
epizoda	epizoda	k1gFnSc1	epizoda
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
Gautamův	Gautamův	k2eAgInSc4d1	Gautamův
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
bohové	bůh	k1gMnPc1	bůh
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
pomíjivé	pomíjivý	k2eAgFnPc1d1	pomíjivá
bytosti	bytost	k1gFnPc1	bytost
v	v	k7c6	v
koloběhu	koloběh	k1gInSc6	koloběh
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jejich	jejich	k3xOp3gFnSc1	jejich
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
život	život	k1gInSc1	život
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Buddha	Buddha	k1gMnSc1	Buddha
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
nad	nad	k7c4	nad
vznikání	vznikání	k1gNnSc4	vznikání
a	a	k8xC	a
zanikání	zanikání	k1gNnSc4	zanikání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uctíván	uctívat	k5eAaImNgMnS	uctívat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
učitel	učitel	k1gMnSc1	učitel
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
prosbě	prosba	k1gFnSc3	prosba
boha	bůh	k1gMnSc4	bůh
výrokem	výrok	k1gInSc7	výrok
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
nesmrtelného	nesmrtelný	k1gMnSc2	nesmrtelný
budiž	budiž	k9	budiž
otevřena	otevřen	k2eAgFnSc1d1	otevřena
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
má	mít	k5eAaImIp3nS	mít
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
slyš	slyšet	k5eAaImRp2nS	slyšet
a	a	k8xC	a
důvěřuj	důvěřovat	k5eAaImRp2nS	důvěřovat
<g/>
!	!	kIx.	!
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
chtěl	chtít	k5eAaImAgMnS	chtít
sdělit	sdělit	k5eAaPmF	sdělit
nejprve	nejprve	k6eAd1	nejprve
bývalým	bývalý	k2eAgMnPc3d1	bývalý
učitelům	učitel	k1gMnPc3	učitel
Árádovi	Áráda	k1gMnSc3	Áráda
Kálámovi	Kálám	k1gMnSc3	Kálám
a	a	k8xC	a
Udrakovi	Udrak	k1gMnSc3	Udrak
Rámaputrovi	Rámaputr	k1gMnSc3	Rámaputr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bohové	bůh	k1gMnPc1	bůh
jej	on	k3xPp3gMnSc4	on
zpravili	zpravit	k5eAaPmAgMnP	zpravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
již	již	k6eAd1	již
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
Benaresu	Benares	k1gInSc2	Benares
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
našel	najít	k5eAaPmAgMnS	najít
pět	pět	k4xCc4	pět
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
před	před	k7c7	před
měsícem	měsíc	k1gInSc7	měsíc
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
jej	on	k3xPp3gMnSc4	on
potkal	potkat	k5eAaPmAgInS	potkat
Upaga	Upag	k1gMnSc4	Upag
<g/>
,	,	kIx,	,
asketický	asketický	k2eAgMnSc1d1	asketický
šramana	šraman	k1gMnSc4	šraman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ohromeně	ohromeně	k6eAd1	ohromeně
zeptal	zeptat	k5eAaPmAgMnS	zeptat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tvé	tvůj	k3xOp2gInPc1	tvůj
rysy	rys	k1gInPc1	rys
září	zářit	k5eAaImIp3nP	zářit
jasným	jasný	k2eAgInSc7d1	jasný
klidem	klid	k1gInSc7	klid
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgMnS	být
tvůj	tvůj	k3xOp2gMnSc1	tvůj
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
nauku	nauka	k1gFnSc4	nauka
následuješ	následovat	k5eAaImIp2nS	následovat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Když	když	k8xS	když
Gautama	Gautamum	k1gNnPc4	Gautamum
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
našel	najít	k5eAaPmAgMnS	najít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
probuzení	probuzení	k1gNnSc3	probuzení
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
mezi	mezi	k7c7	mezi
bohy	bůh	k1gMnPc7	bůh
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nevyrovná	vyrovnat	k5eNaPmIp3nS	vyrovnat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
asketu	asketa	k1gMnSc4	asketa
zřejmě	zřejmě	k6eAd1	zřejmě
příliš	příliš	k6eAd1	příliš
<g/>
.	.	kIx.	.
</s>
<s>
Potřásl	potřást	k5eAaPmAgMnS	potřást
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
šel	jít	k5eAaImAgInS	jít
svou	svůj	k3xOyFgFnSc7	svůj
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Epizoda	epizoda	k1gFnSc1	epizoda
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
neporozumění	neporozumění	k1gNnSc4	neporozumění
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
musely	muset	k5eAaImAgFnP	muset
Gautamovy	Gautamův	k2eAgInPc4d1	Gautamův
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
narazit	narazit	k5eAaPmF	narazit
v	v	k7c6	v
kruzích	kruh	k1gInPc6	kruh
tradičních	tradiční	k2eAgMnPc2d1	tradiční
šramanů	šraman	k1gMnPc2	šraman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Gazelím	gazelí	k2eAgInSc6d1	gazelí
parku	park	k1gInSc6	park
Isipataně	Isipataně	k1gFnSc2	Isipataně
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Sárnáth	Sárnáth	k1gInSc1	Sárnáth
<g/>
,	,	kIx,	,
u	u	k7c2	u
Benaresu	Benares	k1gInSc2	Benares
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgMnS	najít
svých	svůj	k3xOyFgInPc2	svůj
pět	pět	k4xCc4	pět
bývalých	bývalý	k2eAgInPc2d1	bývalý
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gInSc4	on
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
domluvili	domluvit	k5eAaPmAgMnP	domluvit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gautamu	Gautam	k1gInSc2	Gautam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
přestal	přestat	k5eAaPmAgInS	přestat
postit	postit	k5eAaImF	postit
<g/>
,	,	kIx,	,
nijak	nijak	k6eAd1	nijak
úctyhodně	úctyhodně	k6eAd1	úctyhodně
nepřivítají	přivítat	k5eNaPmIp3nP	přivítat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
když	když	k8xS	když
přišel	přijít	k5eAaPmAgMnS	přijít
blíž	blíž	k1gFnSc4	blíž
<g/>
,	,	kIx,	,
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
mu	on	k3xPp3gMnSc3	on
přece	přece	k9	přece
jen	jen	k9	jen
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sled	sled	k1gInSc1	sled
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
promluvil	promluvit	k5eAaPmAgMnS	promluvit
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
buddhistickému	buddhistický	k2eAgNnSc3d1	buddhistické
podání	podání	k1gNnSc3	podání
jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautam	k1gMnSc4	Gautam
vycházel	vycházet	k5eAaImAgMnS	vycházet
ze	z	k7c2	z
společné	společný	k2eAgFnSc2d1	společná
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
objasnil	objasnit	k5eAaPmAgMnS	objasnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
předtím	předtím	k6eAd1	předtím
praktikovaná	praktikovaný	k2eAgFnSc1d1	praktikovaná
askeze	askeze	k1gFnSc1	askeze
byla	být	k5eAaImAgFnS	být
stejně	stejně	k6eAd1	stejně
extrémní	extrémní	k2eAgFnSc1d1	extrémní
jako	jako	k8xS	jako
život	život	k1gInSc1	život
pro	pro	k7c4	pro
požitky	požitek	k1gInPc4	požitek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
šramana	šraman	k1gMnSc4	šraman
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gNnSc4	Gautama
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
svým	svůj	k3xOyFgMnPc3	svůj
posluchačům	posluchač	k1gMnPc3	posluchač
střední	střední	k2eAgFnSc4d1	střední
cestu	cesta	k1gFnSc4	cesta
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ušlechtilé	ušlechtilý	k2eAgFnSc2d1	ušlechtilá
osmidílné	osmidílný	k2eAgFnSc2d1	osmidílná
stezky	stezka	k1gFnSc2	stezka
<g/>
,	,	kIx,	,
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
života	život	k1gInSc2	život
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
prvky	prvek	k1gInPc4	prvek
pochopení	pochopení	k1gNnSc2	pochopení
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnSc1	jednání
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
snažení	snažení	k1gNnSc1	snažení
<g/>
,	,	kIx,	,
bdělost	bdělost	k1gFnSc1	bdělost
<g/>
,	,	kIx,	,
soustředění	soustředění	k1gNnSc1	soustředění
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
stezku	stezka	k1gFnSc4	stezka
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
z	z	k7c2	z
ušlechtilých	ušlechtilý	k2eAgFnPc2d1	ušlechtilá
pravd	pravda	k1gFnPc2	pravda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nalezl	naleznout	k5eAaPmAgInS	naleznout
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
probuzení	probuzení	k1gNnSc2	probuzení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
pravda	pravda	k1gFnSc1	pravda
popisuje	popisovat	k5eAaImIp3nS	popisovat
existenciální	existenciální	k2eAgFnSc4d1	existenciální
situaci	situace	k1gFnSc4	situace
člověka	člověk	k1gMnSc2	člověk
<g/>
:	:	kIx,	:
zrození	zrození	k1gNnSc1	zrození
<g/>
,	,	kIx,	,
stárnutí	stárnutí	k1gNnSc1	stárnutí
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
umírání	umírání	k1gNnSc1	umírání
jsou	být	k5eAaImIp3nP	být
utrpení	utrpení	k1gNnSc1	utrpení
<g/>
;	;	kIx,	;
zármutek	zármutek	k1gInSc1	zármutek
<g/>
,	,	kIx,	,
nářek	nářek	k1gInSc1	nářek
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
a	a	k8xC	a
neklid	neklid	k1gInSc4	neklid
jsou	být	k5eAaImIp3nP	být
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
;	;	kIx,	;
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
nemilým	milý	k2eNgInSc7d1	nemilý
<g/>
,	,	kIx,	,
oddělení	oddělení	k1gNnSc1	oddělení
od	od	k7c2	od
milého	milý	k1gMnSc2	milý
jsou	být	k5eAaImIp3nP	být
utrpení	utrpení	k1gNnSc4	utrpení
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
pravda	pravda	k1gFnSc1	pravda
se	se	k3xPyFc4	se
táže	tázat	k5eAaImIp3nS	tázat
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ona	onen	k3xDgFnSc1	onen
žízeň	žízeň	k1gFnSc1	žízeň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
zrození	zrození	k1gNnSc3	zrození
<g/>
,	,	kIx,	,
provázena	provázen	k2eAgFnSc1d1	provázena
potěšením	potěšení	k1gNnSc7	potěšení
a	a	k8xC	a
chtíčem	chtíč	k1gInSc7	chtíč
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
a	a	k8xC	a
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
z	z	k7c2	z
něčeho	něco	k3yInSc2	něco
těšící	těšící	k2eAgFnPc4d1	těšící
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
žízeň	žízeň	k1gFnSc4	žízeň
po	po	k7c4	po
smyslové	smyslový	k2eAgFnPc4d1	smyslová
radosti	radost	k1gFnPc4	radost
<g/>
,	,	kIx,	,
žízeň	žízeň	k1gFnSc4	žízeň
po	po	k7c6	po
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
<g/>
)	)	kIx)	)
ušlechtilá	ušlechtilý	k2eAgFnSc1d1	ušlechtilá
pravda	pravda	k1gFnSc1	pravda
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zánik	zánik	k1gInSc1	zánik
žízně	žízně	k6eAd1	žízně
jejím	její	k3xOp3gNnSc7	její
dokonalým	dokonalý	k2eAgNnSc7d1	dokonalé
zničením	zničení	k1gNnSc7	zničení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
pomocí	pomocí	k7c2	pomocí
osmidílné	osmidílný	k2eAgFnSc2d1	osmidílná
stezky	stezka	k1gFnSc2	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
Gautamova	Gautamův	k2eAgMnSc2d1	Gautamův
prvního	první	k4xOgInSc2	první
přednesu	přednes	k1gInSc2	přednes
(	(	kIx(	(
<g/>
dharmačakrapavatana	dharmačakrapavatana	k1gFnSc1	dharmačakrapavatana
sútra	sútra	k1gFnSc1	sútra
-	-	kIx~	-
sútra	sútra	k1gFnSc1	sútra
o	o	k7c6	o
roztočení	roztočení	k1gNnSc6	roztočení
kola	kolo	k1gNnSc2	kolo
nauky	nauka	k1gFnSc2	nauka
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gInSc7	jeho
bývalým	bývalý	k2eAgInSc7d1	bývalý
pěti	pět	k4xCc3	pět
společníkům	společník	k1gMnPc3	společník
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Kaundija	Kaundija	k1gMnSc1	Kaundija
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
probuzení	probuzení	k1gNnSc2	probuzení
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
následovali	následovat	k5eAaImAgMnP	následovat
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
čtyři	čtyři	k4xCgMnPc1	čtyři
asketi	asketa	k1gMnPc1	asketa
<g/>
.	.	kIx.	.
</s>
<s>
Stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
základem	základ	k1gInSc7	základ
mnišského	mnišský	k2eAgNnSc2d1	mnišské
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
sanghy	sangha	k1gFnSc2	sangha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznešené	vznešený	k2eAgNnSc1d1	vznešené
učení	učení	k1gNnSc1	učení
bylo	být	k5eAaImAgNnS	být
předáno	předat	k5eAaPmNgNnS	předat
<g/>
,	,	kIx,	,
roztočilo	roztočit	k5eAaPmAgNnS	roztočit
se	se	k3xPyFc4	se
kolo	kolo	k1gNnSc1	kolo
(	(	kIx(	(
<g/>
čakra	čakra	k1gFnSc1	čakra
<g/>
)	)	kIx)	)
nauky	nauka	k1gFnPc1	nauka
(	(	kIx(	(
<g/>
dharma	dharma	k1gFnSc1	dharma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
se	se	k3xPyFc4	se
sotva	sotva	k6eAd1	sotva
dalo	dát	k5eAaPmAgNnS	dát
tušit	tušit	k5eAaImF	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
družiny	družina	k1gFnSc2	družina
šramany	šramany	k?	šramany
Gautamy	Gautam	k1gInPc4	Gautam
v	v	k7c6	v
Gazelím	gazelí	k2eAgInSc6d1	gazelí
parku	park	k1gInSc6	park
Isipataně	Isipataně	k1gMnSc1	Isipataně
vzejde	vzejít	k5eAaPmIp3nS	vzejít
hnutí	hnutí	k1gNnSc4	hnutí
se	s	k7c7	s
světovým	světový	k2eAgInSc7d1	světový
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
příběhy	příběh	k1gInPc1	příběh
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Gautama	Gautamum	k1gNnSc2	Gautamum
získal	získat	k5eAaPmAgMnS	získat
Trápusu	Trápus	k1gMnSc3	Trápus
<g/>
,	,	kIx,	,
Bhalliku	Bhallik	k1gMnSc3	Bhallik
a	a	k8xC	a
pět	pět	k4xCc1	pět
šramanů	šraman	k1gMnPc2	šraman
a	a	k8xC	a
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInPc4d1	budoucí
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gFnSc1	jeho
učitelská	učitelský	k2eAgFnSc1d1	učitelská
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
vynikající	vynikající	k2eAgNnSc1d1	vynikající
argumentování	argumentování	k1gNnSc1	argumentování
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přiváděly	přivádět	k5eAaImAgFnP	přivádět
další	další	k2eAgMnPc4d1	další
stoupence	stoupenec	k1gMnPc4	stoupenec
<g/>
.	.	kIx.	.
</s>
<s>
Stěží	stěží	k6eAd1	stěží
se	se	k3xPyFc4	se
najde	najít	k5eAaPmIp3nS	najít
téma	téma	k1gNnSc1	téma
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Gautama	Gautama	k1gNnSc1	Gautama
za	za	k7c4	za
45	[number]	k4	45
let	léto	k1gNnPc2	léto
mezi	mezi	k7c7	mezi
probuzením	probuzení	k1gNnSc7	probuzení
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
nedotkl	dotknout	k5eNaPmAgMnS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Šramani	Šraman	k1gMnPc1	Šraman
v	v	k7c6	v
bezdomoví	bezdomoví	k1gNnSc6	bezdomoví
jej	on	k3xPp3gMnSc4	on
viděli	vidět	k5eAaImAgMnP	vidět
především	především	k9	především
jako	jako	k8xC	jako
učitele	učitel	k1gMnPc4	učitel
vysvobození	vysvobození	k1gNnSc2	vysvobození
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
svými	svůj	k3xOyFgMnPc7	svůj
pokyny	pokyn	k1gInPc1	pokyn
ke	k	k7c3	k
cvičení	cvičení	k1gNnSc3	cvičení
pohroužení	pohroužení	k1gNnSc3	pohroužení
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
východisko	východisko	k1gNnSc4	východisko
z	z	k7c2	z
koloběhu	koloběh	k1gInSc2	koloběh
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
působení	působení	k1gNnSc1	působení
nevyčerpalo	vyčerpat	k5eNaPmAgNnS	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
lidem	lid	k1gInSc7	lid
všech	všecek	k3xTgFnPc2	všecek
vrstev	vrstva	k1gFnPc2	vrstva
jako	jako	k9	jako
rádce	rádce	k1gMnSc1	rádce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
všemi	všecek	k3xTgNnPc7	všecek
myslitelnými	myslitelný	k2eAgNnPc7d1	myslitelné
tématy	téma	k1gNnPc7	téma
od	od	k7c2	od
povinností	povinnost	k1gFnPc2	povinnost
vladaře	vladař	k1gMnSc2	vladař
přes	přes	k7c4	přes
zákonitosti	zákonitost	k1gFnPc4	zákonitost
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
až	až	k9	až
po	po	k7c4	po
chování	chování	k1gNnSc4	chování
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
a	a	k8xC	a
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
od	od	k7c2	od
správného	správný	k2eAgNnSc2d1	správné
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
majetkem	majetek	k1gInSc7	majetek
přes	přes	k7c4	přes
světy	svět	k1gInPc4	svět
bohů	bůh	k1gMnPc2	bůh
až	až	k6eAd1	až
po	po	k7c6	po
čištění	čištění	k1gNnSc6	čištění
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Gautamovo	Gautamův	k2eAgNnSc4d1	Gautamovo
putování	putování	k1gNnSc4	putování
a	a	k8xC	a
vykládání	vykládání	k1gNnSc4	vykládání
jeho	jeho	k3xOp3gFnSc2	jeho
nauky	nauka	k1gFnSc2	nauka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
trvalo	trvat	k5eAaImAgNnS	trvat
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
,	,	kIx,	,
nezůstalo	zůstat	k5eNaPmAgNnS	zůstat
bez	bez	k7c2	bez
následků	následek	k1gInPc2	následek
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátém	osmdesátý	k4xOgInSc6	osmdesátý
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
nastupující	nastupující	k2eAgFnSc1d1	nastupující
slabost	slabost	k1gFnSc1	slabost
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
patrná	patrný	k2eAgFnSc1d1	patrná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jídle	jídlo	k1gNnSc6	jídlo
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
stoupence	stoupenec	k1gMnSc2	stoupenec
<g/>
,	,	kIx,	,
kováře	kovář	k1gMnSc2	kovář
Čundy	Čunda	k1gMnSc2	Čunda
<g/>
,	,	kIx,	,
Gautama	Gautam	k1gMnSc2	Gautam
těžce	těžce	k6eAd1	těžce
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgFnPc4d1	silná
bolesti	bolest	k1gFnPc4	bolest
snášel	snášet	k5eAaImAgMnS	snášet
s	s	k7c7	s
jasným	jasný	k2eAgNnSc7d1	jasné
vědomím	vědomí	k1gNnSc7	vědomí
a	a	k8xC	a
nenechal	nechat	k5eNaPmAgMnS	nechat
jimi	on	k3xPp3gMnPc7	on
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
svou	svůj	k3xOyFgFnSc4	svůj
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
háji	háj	k1gInSc6	háj
u	u	k7c2	u
Kušinagarí	Kušinagarí	k1gFnSc2	Kušinagarí
si	se	k3xPyFc3	se
Gautama	Gautama	k1gNnSc4	Gautama
nechal	nechat	k5eAaPmAgMnS	nechat
zřídit	zřídit	k5eAaPmF	zřídit
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chtěl	chtít	k5eAaImAgMnS	chtít
zemřít	zemřít	k5eAaPmF	zemřít
a	a	k8xC	a
lehl	lehnout	k5eAaPmAgMnS	lehnout
si	se	k3xPyFc3	se
"	"	kIx"	"
<g/>
s	s	k7c7	s
jasným	jasný	k2eAgNnSc7d1	jasné
vědomím	vědomí	k1gNnSc7	vědomí
na	na	k7c4	na
pravý	pravý	k2eAgInSc4d1	pravý
bok	bok	k1gInSc4	bok
<g/>
,	,	kIx,	,
podoben	podobit	k5eAaImNgInS	podobit
lvu	lev	k1gInSc3	lev
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
nohou	noha	k1gFnSc7	noha
přes	přes	k7c4	přes
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Gautamův	Gautamův	k2eAgMnSc1d1	Gautamův
přítel	přítel	k1gMnSc1	přítel
Ánanda	Ánand	k1gMnSc4	Ánand
byl	být	k5eAaImAgMnS	být
hluboce	hluboko	k6eAd1	hluboko
sklíčený	sklíčený	k2eAgMnSc1d1	sklíčený
a	a	k8xC	a
plakal	plakat	k5eAaImAgInS	plakat
<g/>
.	.	kIx.	.
</s>
<s>
Gautama	Gautama	k1gFnSc1	Gautama
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
těšil	těšit	k5eAaImAgMnS	těšit
těmito	tento	k3xDgFnPc7	tento
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
Ánando	Ánando	k1gNnSc1	Ánando
<g/>
,	,	kIx,	,
nebuď	budit	k5eNaImRp2nS	budit
smutný	smutný	k2eAgMnSc1d1	smutný
<g/>
,	,	kIx,	,
nenaříkej	naříkat	k5eNaBmRp2nS	naříkat
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
jsem	být	k5eAaImIp1nS	být
přece	přece	k9	přece
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
drahé	drahá	k1gFnSc2	drahá
a	a	k8xC	a
milé	milá	k1gFnSc2	milá
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
zaniknout	zaniknout	k5eAaPmF	zaniknout
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
ustat	ustat	k5eAaPmF	ustat
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
přetrvat	přetrvat	k5eAaPmF	přetrvat
na	na	k7c4	na
věky	věk	k1gInPc4	věk
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaImF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
povstalo	povstat	k5eAaPmAgNnS	povstat
<g/>
,	,	kIx,	,
nerozpadlo	rozpadnout	k5eNaPmAgNnS	rozpadnout
<g/>
?	?	kIx.	?
</s>
<s>
To	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Gautamově	Gautamově	k1gFnSc6	Gautamově
smrti	smrt	k1gFnSc2	smrt
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
spáleno	spálit	k5eAaPmNgNnS	spálit
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
a	a	k8xC	a
po	po	k7c6	po
spálení	spálení	k1gNnSc6	spálení
mrtvoly	mrtvola	k1gFnSc2	mrtvola
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
mezi	mezi	k7c7	mezi
delegacemi	delegace	k1gFnPc7	delegace
různých	různý	k2eAgMnPc2d1	různý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mezitím	mezitím	k6eAd1	mezitím
dorazily	dorazit	k5eAaPmAgInP	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vznesla	vznést	k5eAaPmAgFnS	vznést
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
relikvie	relikvie	k1gFnPc4	relikvie
v	v	k7c6	v
popelu	popel	k1gInSc6	popel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
postavila	postavit	k5eAaPmAgFnS	postavit
pomník	pomník	k1gInSc4	pomník
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
přítomných	přítomný	k2eAgMnPc2d1	přítomný
bráhmanů	bráhman	k1gMnPc2	bráhman
<g/>
,	,	kIx,	,
Drona	Drona	k1gFnSc1	Drona
<g/>
,	,	kIx,	,
mínil	mínit	k5eAaImAgMnS	mínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
přít	přít	k5eAaImF	přít
se	se	k3xPyFc4	se
o	o	k7c6	o
kosti	kost	k1gFnSc6	kost
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
učil	učit	k5eAaImAgMnS	učit
mírumilovnosti	mírumilovnost	k1gFnSc2	mírumilovnost
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
relikvie	relikvie	k1gFnPc4	relikvie
rozdělit	rozdělit	k5eAaPmF	rozdělit
a	a	k8xC	a
zřídit	zřídit	k5eAaPmF	zřídit
náhrobky	náhrobek	k1gInPc4	náhrobek
(	(	kIx(	(
<g/>
stúpy	stúpa	k1gFnPc4	stúpa
<g/>
)	)	kIx)	)
na	na	k7c4	na
Gautamovu	Gautamův	k2eAgFnSc4d1	Gautamův
památku	památka	k1gFnSc4	památka
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
krajích	kraj	k1gInPc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bhágavata	Bhágavat	k1gMnSc2	Bhágavat
purány	purána	k1gFnSc2	purána
1.3	[number]	k4	1.3
<g/>
.24	.24	k4	.24
(	(	kIx(	(
<g/>
Šrímad-Bhágavatam	Šrímad-Bhágavatam	k1gInSc1	Šrímad-Bhágavatam
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Buddha	Buddha	k1gMnSc1	Buddha
devátým	devátý	k4xOgInSc7	devátý
avatárem	avatár	k1gInSc7	avatár
Višnua	Višnu	k1gInSc2	Višnu
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
odvrátit	odvrátit	k5eAaPmF	odvrátit
indický	indický	k2eAgInSc4d1	indický
lid	lid	k1gInSc4	lid
od	od	k7c2	od
tehdy	tehdy	k6eAd1	tehdy
velice	velice	k6eAd1	velice
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
učil	učit	k5eAaImAgMnS	učit
o	o	k7c6	o
neubližování	neubližování	k1gNnSc6	neubližování
(	(	kIx(	(
<g/>
ahimsa	ahimsa	k1gFnSc1	ahimsa
<g/>
)	)	kIx)	)
živým	živý	k1gMnPc3	živý
tvorům	tvor	k1gMnPc3	tvor
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
již	již	k6eAd1	již
z	z	k7c2	z
upanišad	upanišada	k1gFnPc2	upanišada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Totožnost	totožnost	k1gFnSc4	totožnost
tohoto	tento	k3xDgMnSc2	tento
Buddhy	Buddha	k1gMnSc2	Buddha
se	s	k7c7	s
Siddhárthou	Siddhártha	k1gMnSc7	Siddhártha
Gautamou	Gautama	k1gMnSc7	Gautama
je	být	k5eAaImIp3nS	být
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnější	pravděpodobný	k2eAgNnSc1d2	pravděpodobnější
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Buddhu	Buddha	k1gMnSc4	Buddha
z	z	k7c2	z
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
jug	jug	k?	jug
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
purána	purána	k1gFnSc1	purána
(	(	kIx(	(
<g/>
2.7	[number]	k4	2.7
<g/>
.37	.37	k4	.37
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
ještě	ještě	k9	ještě
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
,	,	kIx,	,
dřívějšího	dřívější	k2eAgMnSc4d1	dřívější
Buddhu	Buddha	k1gMnSc4	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Avatárů	Avatár	k1gInPc2	Avatár
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
(	(	kIx(	(
<g/>
1.3	[number]	k4	1.3
<g/>
.26	.26	k4	.26
<g/>
)	)	kIx)	)
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
obdobné	obdobný	k2eAgNnSc4d1	obdobné
pojetí	pojetí	k1gNnSc4	pojetí
nekonečně	konečně	k6eNd1	konečně
mnoha	mnoho	k4c2	mnoho
Buddhů	Buddha	k1gMnPc2	Buddha
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
z	z	k7c2	z
mahájány	mahájána	k1gFnSc2	mahájána
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mahájánová	mahájánový	k2eAgFnSc1d1	mahájánový
ikonografie	ikonografie	k1gFnSc1	ikonografie
je	být	k5eAaImIp3nS	být
blízká	blízký	k2eAgFnSc1d1	blízká
védské	védský	k2eAgInPc1d1	védský
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lotos	lotos	k1gInSc1	lotos
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
Buddhové	Buddha	k1gMnPc1	Buddha
sedí	sedit	k5eAaImIp3nP	sedit
a	a	k8xC	a
který	který	k3yQgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
jeho	jeho	k3xOp3gFnPc4	jeho
Šakti	Šakť	k1gFnPc4	Šakť
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
mantra	mantra	k1gFnSc1	mantra
óm	óm	k1gInSc1	óm
mani	mani	k6eAd1	mani
padmé	padmý	k2eAgFnSc2d1	padmý
húm	húm	k?	húm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
jsou	být	k5eAaImIp3nP	být
ikonografie	ikonografie	k1gFnPc1	ikonografie
obou	dva	k4xCgFnPc2	dva
tradic	tradice	k1gFnPc2	tradice
téměř	téměř	k6eAd1	téměř
totožné	totožný	k2eAgFnPc1d1	totožná
<g/>
.	.	kIx.	.
</s>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
popisů	popis	k1gInPc2	popis
Buddhy	Buddha	k1gMnSc2	Buddha
v	v	k7c6	v
buddhistických	buddhistický	k2eAgInPc6d1	buddhistický
textech	text	k1gInPc6	text
a	a	k8xC	a
Krišny	Krišna	k1gMnPc7	Krišna
v	v	k7c6	v
Bhagavadgítě	Bhagavadgít	k1gInSc6	Bhagavadgít
neušla	ujít	k5eNaPmAgFnS	ujít
pozornosti	pozornost	k1gFnSc2	pozornost
učenců	učenec	k1gMnPc2	učenec
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
<g/>
N.	N.	kA	N.
Upadhyaya	Upadhyaya	k1gMnSc1	Upadhyaya
píše	psát	k5eAaImIp3nS	psát
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
impact	impact	k1gInSc1	impact
of	of	k?	of
the	the	k?	the
bhakti	bhakť	k1gFnSc3	bhakť
movement	movement	k1gMnSc1	movement
on	on	k3xPp3gMnSc1	on
the	the	k?	the
development	development	k1gMnSc1	development
of	of	k?	of
Mahayana	Mahayan	k1gMnSc4	Mahayan
Buddhism	Buddhism	k1gMnSc1	Buddhism
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Udivující	udivující	k2eAgFnSc1d1	udivující
svojí	svojit	k5eAaImIp3nS	svojit
podobností	podobnost	k1gFnSc7	podobnost
s	s	k7c7	s
Bhagavadgítou	Bhagavadgíta	k1gFnSc7	Bhagavadgíta
<g/>
,	,	kIx,	,
samotná	samotný	k2eAgFnSc1d1	samotná
podoba	podoba	k1gFnSc1	podoba
a	a	k8xC	a
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Buddha	Buddha	k1gMnSc1	Buddha
zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
v	v	k7c4	v
Sad	sad	k1gInSc4	sad
dharma	dharma	k1gFnSc1	dharma
Pundaríce	Pundaríce	k1gFnSc1	Pundaríce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úžasně	úžasně	k6eAd1	úžasně
nadpřirozená	nadpřirozený	k2eAgFnSc1d1	nadpřirozená
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k8xS	jako
kosmická	kosmický	k2eAgFnSc1d1	kosmická
podoba	podoba	k1gFnSc1	podoba
Krišny	Krišna	k1gMnSc2	Krišna
v	v	k7c6	v
Bhagavad-gítě	Bhagavadíta	k1gFnSc6	Bhagavad-gíta
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgInSc1d1	popisován
jako	jako	k8xC	jako
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vydává	vydávat	k5eAaImIp3nS	vydávat
blyštivou	blyštivý	k2eAgFnSc4d1	blyštivá
záři	záře	k1gFnSc4	záře
oslňující	oslňující	k2eAgInSc4d1	oslňující
nesmírný	smírný	k2eNgInSc4d1	nesmírný
prostor	prostor	k1gInSc4	prostor
od	od	k7c2	od
pekla	peklo	k1gNnSc2	peklo
po	po	k7c4	po
18	[number]	k4	18
000	[number]	k4	000
oblastí	oblast	k1gFnPc2	oblast
Buddhů	Buddha	k1gMnPc2	Buddha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
buddhisté	buddhista	k1gMnPc1	buddhista
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
théravádoví	théravádový	k2eAgMnPc1d1	théravádový
<g/>
,	,	kIx,	,
nepovažují	považovat	k5eNaImIp3nP	považovat
Buddhu	Buddha	k1gMnSc4	Buddha
za	za	k7c4	za
Boha	bůh	k1gMnSc4	bůh
nebo	nebo	k8xC	nebo
vtělení	vtělení	k1gNnSc4	vtělení
nějakého	nějaký	k3yIgNnSc2	nějaký
božstva	božstvo	k1gNnSc2	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
hinduismu	hinduismus	k1gInSc2	hinduismus
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
snaha	snaha	k1gFnSc1	snaha
absorbovat	absorbovat	k5eAaBmF	absorbovat
buddhismus	buddhismus	k1gInSc4	buddhismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
významné	významný	k2eAgNnSc4d1	významné
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Dašaratha	Dašaratha	k1gFnSc1	Dašaratha
džátaka	džátak	k1gMnSc2	džátak
však	však	k9	však
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
Buddhu	Buddha	k1gMnSc4	Buddha
s	s	k7c7	s
Rámou	Ráma	k1gMnSc7	Ráma
<g/>
,	,	kIx,	,
sedmým	sedmý	k4xOgInSc7	sedmý
avatárem	avatár	k1gInSc7	avatár
Višnua	Višnu	k1gInSc2	Višnu
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
buddhismu	buddhismus	k1gInSc2	buddhismus
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
není	být	k5eNaImIp3nS	být
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
tomuto	tento	k3xDgInSc3	tento
zahrnutí	zahrnutí	k1gNnSc6	zahrnutí
Buddhy	Buddha	k1gMnSc2	Buddha
mezi	mezi	k7c4	mezi
náboženské	náboženský	k2eAgFnPc4d1	náboženská
postavy	postava	k1gFnPc4	postava
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šíření	šíření	k1gNnSc1	šíření
neduální	duální	k2eNgFnSc2d1	neduální
védantové	védantový	k2eAgFnSc2d1	védantový
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
postavit	postavit	k5eAaPmF	postavit
jemné	jemný	k2eAgFnSc3d1	jemná
a	a	k8xC	a
propracované	propracovaný	k2eAgFnSc3d1	propracovaná
filosofii	filosofie	k1gFnSc3	filosofie
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
muslimové	muslim	k1gMnPc1	muslim
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Siddhártha	Siddhártha	k1gFnSc1	Siddhártha
Gautama	Gautamum	k1gNnSc2	Gautamum
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
popisována	popisovat	k5eAaImNgFnS	popisovat
v	v	k7c6	v
Koránu	korán	k1gInSc6	korán
jako	jako	k8xS	jako
Dhul-Kifl	Dhul-Kifl	k1gInSc1	Dhul-Kifl
<g/>
,	,	kIx,	,
že	že	k8xS	že
tedy	tedy	k9	tedy
Gautama	Gautama	k1gNnSc4	Gautama
byl	být	k5eAaImAgMnS	být
prorokem	prorok	k1gMnSc7	prorok
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
Dhul-Kifl	Dhul-Kifla	k1gFnPc2	Dhul-Kifla
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jeho	on	k3xPp3gInSc4	on
význam	význam	k1gInSc4	význam
vyložit	vyložit	k5eAaPmF	vyložit
jako	jako	k9	jako
muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Kifl	Kifla	k1gFnPc2	Kifla
<g/>
.	.	kIx.	.
</s>
<s>
Kifl	Kifl	k1gInSc1	Kifl
je	být	k5eAaImIp3nS	být
přepis	přepis	k1gInSc4	přepis
arabské	arabský	k2eAgFnSc2d1	arabská
výslovnosti	výslovnost	k1gFnSc2	výslovnost
slova	slovo	k1gNnSc2	slovo
Kapilavastu	Kapilavast	k1gInSc2	Kapilavast
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Gautama	Gautama	k1gNnSc4	Gautama
žil	žít	k5eAaImAgMnS	žít
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
29	[number]	k4	29
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
převládajícím	převládající	k2eAgInSc7d1	převládající
názorem	názor	k1gInSc7	názor
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dhul-Kifl	Dhul-Kifl	k1gInSc4	Dhul-Kifl
byla	být	k5eAaImAgFnS	být
osoba	osoba	k1gFnSc1	osoba
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
Siddhárthy	Siddhártha	k1gFnSc2	Siddhártha
Gautamy	Gautam	k1gInPc1	Gautam
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
proroka	prorok	k1gMnSc4	prorok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Dhul-Kifl	Dhul-Kifl	k1gInSc4	Dhul-Kifl
byl	být	k5eAaImAgMnS	být
prorok	prorok	k1gMnSc1	prorok
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
jako	jako	k8xS	jako
Ezechiel	Ezechiel	k1gMnSc1	Ezechiel
<g/>
.	.	kIx.	.
</s>
