<p>
<s>
Vznik	vznik	k1gInSc1	vznik
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgInS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
ustavení	ustavení	k1gNnSc3	ustavení
samostatného	samostatný	k2eAgInSc2d1	samostatný
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
hranice	hranice	k1gFnPc1	hranice
byly	být	k5eAaImAgFnP	být
vymezeny	vymezen	k2eAgInPc1d1	vymezen
mírovými	mírový	k2eAgFnPc7d1	mírová
smlouvami	smlouva	k1gFnPc7	smlouva
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vycházejícími	vycházející	k2eAgNnPc7d1	vycházející
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
versailleského	versailleský	k2eAgInSc2d1	versailleský
mírového	mírový	k2eAgInSc2d1	mírový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Souvislosti	souvislost	k1gFnPc1	souvislost
==	==	k?	==
</s>
</p>
<p>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1914	[number]	k4	1914
byly	být	k5eAaImAgFnP	být
odsunuty	odsunut	k2eAgFnPc1d1	odsunuta
národnostní	národnostní	k2eAgFnPc1d1	národnostní
otázky	otázka	k1gFnPc1	otázka
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Odsunutí	odsunutí	k1gNnSc1	odsunutí
národnostní	národnostní	k2eAgFnSc2d1	národnostní
otázky	otázka	k1gFnSc2	otázka
vedlo	vést	k5eAaImAgNnS	vést
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
některé	některý	k3yIgFnPc1	některý
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yInSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
české	český	k2eAgFnSc2d1	Česká
politiky	politika	k1gFnSc2	politika
k	k	k7c3	k
upřednostnění	upřednostnění	k1gNnSc3	upřednostnění
požadavku	požadavek	k1gInSc2	požadavek
samostatnosti	samostatnost	k1gFnSc2	samostatnost
před	před	k7c7	před
federálním	federální	k2eAgNnSc7d1	federální
uspořádáním	uspořádání	k1gNnSc7	uspořádání
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
počátečních	počáteční	k2eAgFnPc6d1	počáteční
represích	represe	k1gFnPc6	represe
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
čeští	český	k2eAgMnPc1d1	český
politici	politik	k1gMnPc1	politik
doma	doma	k6eAd1	doma
pasivní	pasivní	k2eAgMnPc1d1	pasivní
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
nevytvářet	vytvářet	k5eNaImF	vytvářet
záminky	záminka	k1gFnPc4	záminka
k	k	k7c3	k
postupům	postup	k1gInPc3	postup
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1915	[number]	k4	1915
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
formulovali	formulovat	k5eAaImAgMnP	formulovat
své	svůj	k3xOyFgFnPc4	svůj
úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
samostatného	samostatný	k2eAgInSc2d1	samostatný
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Prohlášení	prohlášení	k1gNnSc6	prohlášení
Českého	český	k2eAgInSc2d1	český
komitétu	komitét	k1gInSc2	komitét
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
zástupci	zástupce	k1gMnPc7	zástupce
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
akce	akce	k1gFnSc2	akce
(	(	kIx(	(
<g/>
poslanci	poslanec	k1gMnPc1	poslanec
říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
T.G.	T.G.	k1gMnSc1	T.G.
<g/>
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Dürich	Dürich	k1gMnSc1	Dürich
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
za	za	k7c4	za
spolky	spolek	k1gInPc4	spolek
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Bohumil	Bohumil	k1gMnSc1	Bohumil
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
za	za	k7c4	za
Národní	národní	k2eAgNnSc4d1	národní
sdružení	sdružení	k1gNnSc4	sdružení
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g />
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Pergler	Pergler	k1gMnSc1	Pergler
a	a	k8xC	a
Emanuel	Emanuel	k1gMnSc1	Emanuel
Voska	Voska	k1gMnSc1	Voska
<g/>
,	,	kIx,	,
za	za	k7c4	za
Výbor	výbor	k1gInSc4	výbor
české	český	k2eAgFnSc2d1	Česká
kolonie	kolonie	k1gFnSc2	kolonie
a	a	k8xC	a
českých	český	k2eAgMnPc2d1	český
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
František	František	k1gMnSc1	František
Kupka	Kupka	k1gMnSc1	Kupka
<g/>
,	,	kIx,	,
za	za	k7c4	za
Československý	československý	k2eAgInSc4d1	československý
socialistický	socialistický	k2eAgInSc4d1	socialistický
spolek	spolek	k1gInSc4	spolek
Rovnost	rovnost	k1gFnSc1	rovnost
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Antonín	Antonín	k1gMnSc1	Antonín
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
za	za	k7c4	za
České	český	k2eAgNnSc4d1	české
konání	konání	k1gNnSc4	konání
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
Jan	Jan	k1gMnSc1	Jan
Sýkora	Sýkora	k1gMnSc1	Sýkora
a	a	k8xC	a
František	František	k1gMnSc1	František
Kopecký	Kopecký	k1gMnSc1	Kopecký
a	a	k8xC	a
za	za	k7c4	za
redakci	redakce	k1gFnSc4	redakce
Čechoslováka	Čechoslovák	k1gMnSc2	Čechoslovák
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
Bohdan	Bohdan	k1gMnSc1	Bohdan
Pavlů	Pavlů	k1gMnSc1	Pavlů
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
záklaním	záklaní	k2eAgInSc7d1	záklaní
kamenem	kámen	k1gInSc7	kámen
československé	československý	k2eAgFnSc2d1	Československá
akce	akce	k1gFnSc2	akce
za	za	k7c4	za
prosazení	prosazení	k1gNnSc4	prosazení
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
uznání	uznání	k1gNnSc2	uznání
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
český	český	k2eAgInSc4d1	český
exil	exil	k1gInSc4	exil
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
říšským	říšský	k2eAgMnSc7d1	říšský
poslancem	poslanec	k1gMnSc7	poslanec
Tomášem	Tomáš	k1gMnSc7	Tomáš
Garriguem	Garrigu	k1gMnSc7	Garrigu
Masarykem	Masaryk	k1gMnSc7	Masaryk
<g/>
,	,	kIx,	,
francouzským	francouzský	k2eAgMnSc7d1	francouzský
generálem	generál	k1gMnSc7	generál
Milanem	Milan	k1gMnSc7	Milan
Rastislavem	Rastislav	k1gMnSc7	Rastislav
Štefánikem	Štefánik	k1gMnSc7	Štefánik
a	a	k8xC	a
diplomatem	diplomat	k1gMnSc7	diplomat
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
během	během	k7c2	během
války	válka	k1gFnSc2	válka
u	u	k7c2	u
mocností	mocnost	k1gFnSc7	mocnost
Dohody	dohoda	k1gFnSc2	dohoda
vznik	vznik	k1gInSc4	vznik
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
(	(	kIx(	(
<g/>
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
a	a	k8xC	a
Štefánikovo	Štefánikův	k2eAgNnSc4d1	Štefánikovo
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Češi	Čech	k1gMnPc1	Čech
jsou	být	k5eAaImIp3nP	být
Slováci	Slovák	k1gMnPc1	Slovák
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
jedno	jeden	k4xCgNnSc1	jeden
<g/>
)	)	kIx)	)
a	a	k8xC	a
územní	územní	k2eAgNnSc4d1	územní
spojení	spojení	k1gNnSc4	spojení
bývalých	bývalý	k2eAgFnPc2d1	bývalá
Zemí	zem	k1gFnPc2	zem
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
napomáhaly	napomáhat	k5eAaBmAgFnP	napomáhat
různé	různý	k2eAgFnPc1d1	různá
krajanské	krajanský	k2eAgFnPc1d1	krajanská
sbírky	sbírka	k1gFnPc1	sbírka
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
československé	československý	k2eAgFnSc2d1	Československá
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
legie	legie	k1gFnPc1	legie
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
z	z	k7c2	z
Čechů	Čech	k1gMnPc2	Čech
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1914	[number]	k4	1914
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Česká	český	k2eAgFnSc1d1	Česká
družina	družina	k1gFnSc1	družina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
příkladem	příklad	k1gInSc7	příklad
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
carské	carský	k2eAgFnSc2d1	carská
armády	armáda	k1gFnSc2	armáda
také	také	k6eAd1	také
dalším	další	k2eAgFnPc3d1	další
slovanským	slovanský	k2eAgFnPc3d1	Slovanská
menšinám	menšina	k1gFnPc3	menšina
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
Chorvatům	Chorvat	k1gMnPc3	Chorvat
<g/>
,	,	kIx,	,
Srbům	Srb	k1gMnPc3	Srb
atd.	atd.	kA	atd.
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
ruským	ruský	k2eAgNnSc7d1	ruské
velením	velení	k1gNnSc7	velení
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zpočátku	zpočátku	k6eAd1	zpočátku
přezíravým	přezíravý	k2eAgMnPc3d1	přezíravý
a	a	k8xC	a
krutým	krutý	k2eAgMnPc3d1	krutý
<g/>
,	,	kIx,	,
až	až	k8xS	až
příchodem	příchod	k1gInSc7	příchod
plukovníka	plukovník	k1gMnSc2	plukovník
Václava	Václav	k1gMnSc2	Václav
Platonoviče	Platonovič	k1gMnSc2	Platonovič
Trojanova	Trojanův	k2eAgMnSc2d1	Trojanův
se	se	k3xPyFc4	se
poměry	poměr	k1gInPc7	poměr
zlepšily	zlepšit	k5eAaPmAgInP	zlepšit
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byla	být	k5eAaImAgFnS	být
doplněná	doplněná	k1gFnSc1	doplněná
o	o	k7c4	o
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odmítali	odmítat	k5eAaImAgMnP	odmítat
zůstávat	zůstávat	k5eAaImF	zůstávat
v	v	k7c6	v
zajateckých	zajatecký	k2eAgInPc6d1	zajatecký
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
dobrovolně	dobrovolně	k6eAd1	dobrovolně
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
československého	československý	k2eAgNnSc2d1	Československé
vojska	vojsko	k1gNnSc2	vojsko
(	(	kIx(	(
<g/>
po	po	k7c6	po
Zborovské	Zborovský	k2eAgFnSc6d1	Zborovská
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
československého	československý	k2eAgNnSc2d1	Československé
vojska	vojsko	k1gNnSc2	vojsko
téměř	téměř	k6eAd1	téměř
zautomatizoval	zautomatizovat	k5eAaPmAgInS	zautomatizovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc3	Rakousku-Uhersko
a	a	k8xC	a
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zajetí	zajetí	k1gNnSc2	zajetí
hrozil	hrozit	k5eAaImAgInS	hrozit
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
za	za	k7c4	za
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
zajetí	zajetí	k1gNnSc2	zajetí
se	se	k3xPyFc4	se
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
českoslovenští	československý	k2eAgMnPc1d1	československý
legionáři	legionář	k1gMnPc1	legionář
byli	být	k5eAaImAgMnP	být
povětšinou	povětšina	k1gFnSc7	povětšina
nasazeni	nasadit	k5eAaPmNgMnP	nasadit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
linie	linie	k1gFnSc2	linie
bojů	boj	k1gInPc2	boj
(	(	kIx(	(
<g/>
Italské	italský	k2eAgFnPc4d1	italská
legie	legie	k1gFnPc4	legie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sloužili	sloužit	k5eAaImAgMnP	sloužit
pro	pro	k7c4	pro
dobrou	dobrý	k2eAgFnSc4d1	dobrá
znalost	znalost	k1gFnSc4	znalost
německého	německý	k2eAgMnSc2d1	německý
i	i	k8xC	i
slovanského	slovanský	k2eAgMnSc2d1	slovanský
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
jazyka	jazyk	k1gInSc2	jazyk
jako	jako	k8xS	jako
vyzvědači	vyzvědač	k1gMnPc1	vyzvědač
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
vojenské	vojenský	k2eAgInPc1d1	vojenský
úspěchy	úspěch	k1gInPc1	úspěch
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
Zborov	Zborov	k1gInSc1	Zborov
<g/>
,	,	kIx,	,
Bachmač	Bachmač	k1gInSc1	Bachmač
<g/>
,	,	kIx,	,
Kazaň	Kazaň	k1gFnSc1	Kazaň
<g/>
)	)	kIx)	)
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c4	o
nespokojenosti	nespokojenost	k1gFnPc4	nespokojenost
českého	český	k2eAgMnSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Slováků	Slováky	k1gInPc2	Slováky
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
legiích	legie	k1gFnPc6	legie
kolem	kolem	k7c2	kolem
5500	[number]	k4	5500
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Čechů	Čech	k1gMnPc2	Čech
kolem	kolem	k7c2	kolem
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
československé	československý	k2eAgNnSc4d1	Československé
vojsko	vojsko	k1gNnSc4	vojsko
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
USA	USA	kA	USA
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
především	především	k6eAd1	především
slovenských	slovenský	k2eAgMnPc2d1	slovenský
krajanů	krajan	k1gMnPc2	krajan
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
otcem	otec	k1gMnSc7	otec
myšlenky	myšlenka	k1gFnSc2	myšlenka
byl	být	k5eAaImAgMnS	být
Štefánik	Štefánik	k1gInSc4	Štefánik
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ale	ale	k9	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
tam	tam	k6eAd1	tam
prováděn	provádět	k5eAaImNgInS	provádět
jen	jen	k9	jen
omezený	omezený	k2eAgInSc1d1	omezený
výcvik	výcvik	k1gInSc1	výcvik
československého	československý	k2eAgNnSc2d1	Československé
legionářského	legionářský	k2eAgNnSc2d1	legionářské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
československé	československý	k2eAgFnPc1d1	Československá
legie	legie	k1gFnPc1	legie
převzaly	převzít	k5eAaPmAgFnP	převzít
úlohu	úloha	k1gFnSc4	úloha
československého	československý	k2eAgNnSc2d1	Československé
vojska	vojsko	k1gNnSc2	vojsko
-	-	kIx~	-
armády	armáda	k1gFnPc4	armáda
dosud	dosud	k6eAd1	dosud
neexistujícího	existující	k2eNgInSc2d1	neexistující
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Složit	složit	k5eAaPmF	složit
vojsko	vojsko	k1gNnSc4	vojsko
z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
problematické	problematický	k2eAgNnSc1d1	problematické
(	(	kIx(	(
<g/>
porušení	porušení	k1gNnSc1	porušení
předchozí	předchozí	k2eAgFnSc2d1	předchozí
přísahy	přísaha	k1gFnSc2	přísaha
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
poddané	poddaná	k1gFnSc6	poddaná
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zajetí	zajetí	k1gNnSc2	zajetí
čekala	čekat	k5eAaImAgFnS	čekat
poprava	poprava	k1gFnSc1	poprava
za	za	k7c4	za
vlastizradu	vlastizrada	k1gFnSc4	vlastizrada
<g/>
,	,	kIx,	,
únava	únava	k1gFnSc1	únava
z	z	k7c2	z
předchozích	předchozí	k2eAgInPc2d1	předchozí
bojů	boj	k1gInPc2	boj
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
zajistil	zajistit	k5eAaPmAgMnS	zajistit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
Československé	československý	k2eAgFnPc1d1	Československá
legie	legie	k1gFnPc1	legie
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
součástí	součást	k1gFnPc2	součást
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
domácí	domácí	k2eAgMnPc1d1	domácí
spisovatelé	spisovatel	k1gMnPc1	spisovatel
nečinnost	nečinnost	k1gFnSc4	nečinnost
českých	český	k2eAgInPc2d1	český
poslanců	poslanec	k1gMnPc2	poslanec
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
veřejného	veřejný	k2eAgNnSc2d1	veřejné
prohlášení	prohlášení	k1gNnSc2	prohlášení
Manifest	manifest	k1gInSc1	manifest
českých	český	k2eAgInPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Leden	leden	k1gInSc1	leden
-	-	kIx~	-
září	září	k1gNnSc2	září
1918	[number]	k4	1918
===	===	k?	===
</s>
</p>
<p>
<s>
Domácí	domácí	k2eAgMnPc1d1	domácí
politici	politik	k1gMnPc1	politik
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
při	při	k7c6	při
Tříkrálové	tříkrálový	k2eAgFnSc6d1	Tříkrálová
deklaraci	deklarace	k1gFnSc6	deklarace
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
reagoval	reagovat	k5eAaBmAgInS	reagovat
na	na	k7c4	na
zdánlivě	zdánlivě	k6eAd1	zdánlivě
bezvýznamné	bezvýznamný	k2eAgNnSc4d1	bezvýznamné
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
separátním	separátní	k2eAgInSc6d1	separátní
míru	mír	k1gInSc6	mír
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
nebyli	být	k5eNaImAgMnP	být
pozvání	pozvánět	k5eAaImIp3nP	pozvánět
zástupci	zástupce	k1gMnPc1	zástupce
národů	národ	k1gInPc2	národ
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc2	Rakousko-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
požadavek	požadavek	k1gInSc4	požadavek
samostatnosti	samostatnost	k1gFnSc2	samostatnost
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
ani	ani	k8xC	ani
slovo	slovo	k1gNnSc1	slovo
o	o	k7c6	o
Rakousko-Uhersku	Rakousko-Uhersek	k1gInSc6	Rakousko-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
zahájen	zahájen	k2eAgInSc4d1	zahájen
společný	společný	k2eAgInSc4d1	společný
postup	postup	k1gInSc4	postup
osamostatnění	osamostatnění	k1gNnPc2	osamostatnění
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
14	[number]	k4	14
bodů	bod	k1gInPc2	bod
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
Woodrowa	Woodrowus	k1gMnSc2	Woodrowus
Wilsona	Wilson	k1gMnSc2	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInSc4d1	budoucí
československý	československý	k2eAgInSc4d1	československý
stát	stát	k1gInSc4	stát
je	být	k5eAaImIp3nS	být
nesporný	sporný	k2eNgInSc1d1	nesporný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
prohlášení	prohlášení	k1gNnSc1	prohlášení
ještě	ještě	k9	ještě
negarantovalo	garantovat	k5eNaBmAgNnS	garantovat
vznik	vznik	k1gInSc4	vznik
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
požadovalo	požadovat	k5eAaImAgNnS	požadovat
autonomii	autonomie	k1gFnSc4	autonomie
pro	pro	k7c4	pro
rakousko-uherské	rakouskoherský	k2eAgInPc4d1	rakousko-uherský
národy	národ	k1gInPc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
upozornit	upozornit	k5eAaPmF	upozornit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
garance	garance	k1gFnSc1	garance
samostatnosti	samostatnost	k1gFnSc2	samostatnost
přišla	přijít	k5eAaPmAgFnS	přijít
až	až	k9	až
s	s	k7c7	s
odpovědí	odpověď	k1gFnSc7	odpověď
prezidenta	prezident	k1gMnSc2	prezident
Wilsona	Wilson	k1gMnSc2	Wilson
na	na	k7c4	na
nabídku	nabídka	k1gFnSc4	nabídka
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
příslibu	příslib	k1gInSc2	příslib
uznání	uznání	k1gNnSc2	uznání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
od	od	k7c2	od
hlavních	hlavní	k2eAgFnPc2d1	hlavní
západních	západní	k2eAgFnPc2d1	západní
mocností	mocnost	k1gFnPc2	mocnost
Dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
československou	československý	k2eAgFnSc4d1	Československá
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
na	na	k7c6	na
domácím	domácí	k2eAgNnSc6d1	domácí
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
československého	československý	k2eAgInSc2d1	československý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
připravovat	připravovat	k5eAaImF	připravovat
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maffie	Maffie	k1gFnSc1	Maffie
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
1918	[number]	k4	1918
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
převzetí	převzetí	k1gNnSc2	převzetí
moci	moct	k5eAaImF	moct
nekrvavým	krvavý	k2eNgInSc7d1	nekrvavý
vojenským	vojenský	k2eAgInSc7d1	vojenský
převratem	převrat	k1gInSc7	převrat
<g/>
.	.	kIx.	.
<g/>
Již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
budoucí	budoucí	k2eAgInSc1d1	budoucí
Československý	československý	k2eAgInSc1d1	československý
stát	stát	k1gInSc1	stát
mezinárodně	mezinárodně	k6eAd1	mezinárodně
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
legie	legie	k1gFnPc1	legie
byly	být	k5eAaImAgFnP	být
de	de	k?	de
facto	facta	k1gFnSc5	facta
uznány	uznat	k5eAaPmNgFnP	uznat
za	za	k7c4	za
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Československou	československý	k2eAgFnSc4d1	Československá
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
uznaly	uznat	k5eAaPmAgFnP	uznat
za	za	k7c4	za
základ	základ	k1gInSc4	základ
budoucí	budoucí	k2eAgFnSc2d1	budoucí
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
postupně	postupně	k6eAd1	postupně
následující	následující	k2eAgInPc1d1	následující
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1918	[number]	k4	1918
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
1918	[number]	k4	1918
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1918	[number]	k4	1918
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
československou	československý	k2eAgFnSc4d1	Československá
vládu	vláda	k1gFnSc4	vláda
ustavenou	ustavený	k2eAgFnSc4d1	ustavená
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
uznaly	uznat	k5eAaPmAgFnP	uznat
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Uznání	uznání	k1gNnSc1	uznání
dalších	další	k2eAgInPc2d1	další
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
států	stát	k1gInPc2	stát
následovalo	následovat	k5eAaImAgNnS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
zcela	zcela	k6eAd1	zcela
výjimečné	výjimečný	k2eAgFnSc2d1	výjimečná
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jediným	jediný	k2eAgInSc7d1	jediný
nástupnickým	nástupnický	k2eAgInSc7d1	nástupnický
státem	stát	k1gInSc7	stát
Rakouska-Uherska	Rakouska-Uherska	k1gFnSc1	Rakouska-Uherska
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
budoucí	budoucí	k2eAgFnSc4d1	budoucí
existenci	existence	k1gFnSc4	existence
spojenci	spojenec	k1gMnPc1	spojenec
uznali	uznat	k5eAaPmAgMnP	uznat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
konáním	konání	k1gNnSc7	konání
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Říjen	říjen	k1gInSc1	říjen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
federalizaci	federalizace	k1gFnSc4	federalizace
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
a	a	k8xC	a
Washingtonská	washingtonský	k2eAgFnSc1d1	Washingtonská
deklarace	deklarace	k1gFnSc1	deklarace
===	===	k?	===
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
Německo	Německo	k1gNnSc1	Německo
přišlo	přijít	k5eAaPmAgNnS	přijít
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgInP	přidat
Rakousko	Rakousko	k1gNnSc1	Rakousko
i	i	k8xC	i
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Rakousko	Rakousko	k1gNnSc1	Rakousko
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
příměří	příměří	k1gNnSc4	příměří
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Československo	Československo	k1gNnSc1	Československo
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vznikalo	vznikat	k5eAaImAgNnS	vznikat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nebylo	být	k5eNaImAgNnS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vůbec	vůbec	k9	vůbec
k	k	k7c3	k
nějakým	nějaký	k3yIgFnPc3	nějaký
změnám	změna	k1gFnPc3	změna
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Na	na	k7c6	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
svolala	svolat	k5eAaPmAgFnS	svolat
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
rada	rada	k1gFnSc1	rada
generální	generální	k2eAgFnSc4d1	generální
stávku	stávka	k1gFnSc4	stávka
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
socialistický	socialistický	k2eAgInSc1d1	socialistický
stát	stát	k1gInSc1	stát
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
zástupce	zástupce	k1gMnSc1	zástupce
velitele	velitel	k1gMnSc2	velitel
pražské	pražský	k2eAgFnSc2d1	Pražská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
maršál	maršál	k1gMnSc1	maršál
Eduard	Eduard	k1gMnSc1	Eduard
Zanantoni	Zananton	k1gMnPc1	Zananton
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
rozmístit	rozmístit	k5eAaPmF	rozmístit
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgInS	obávat
krveprolití	krveprolití	k1gNnPc4	krveprolití
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
připravené	připravený	k2eAgFnPc1d1	připravená
demonstrace	demonstrace	k1gFnPc1	demonstrace
odvolat	odvolat	k5eAaPmF	odvolat
a	a	k8xC	a
distancoval	distancovat	k5eAaBmAgMnS	distancovat
se	se	k3xPyFc4	se
od	od	k7c2	od
zamýšleného	zamýšlený	k2eAgNnSc2d1	zamýšlené
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
,	,	kIx,	,
demonstrace	demonstrace	k1gFnPc1	demonstrace
se	se	k3xPyFc4	se
ale	ale	k9	ale
odvolat	odvolat	k5eAaPmF	odvolat
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
násilí	násilí	k1gNnSc3	násilí
ale	ale	k8xC	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
rady	rada	k1gFnSc2	rada
byli	být	k5eAaImAgMnP	být
posléze	posléze	k6eAd1	posléze
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
vyděsily	vyděsit	k5eAaPmAgFnP	vyděsit
rakousko-uherskou	rakouskoherský	k2eAgFnSc4d1	rakousko-uherská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
obávala	obávat	k5eAaImAgFnS	obávat
chaosu	chaos	k1gInSc3	chaos
a	a	k8xC	a
vzestupu	vzestup	k1gInSc3	vzestup
bolševismu	bolševismus	k1gInSc2	bolševismus
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
poslala	poslat	k5eAaPmAgFnS	poslat
majora	major	k1gMnSc4	major
Rudolfa	Rudolf	k1gMnSc4	Rudolf
Kalhouse	Kalhouse	k1gFnSc1	Kalhouse
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
českými	český	k2eAgMnPc7d1	český
vůdci	vůdce	k1gMnPc7	vůdce
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
udržení	udržení	k1gNnSc4	udržení
kázně	kázeň	k1gFnSc2	kázeň
u	u	k7c2	u
českých	český	k2eAgInPc2d1	český
pluků	pluk	k1gInPc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Rašín	Rašín	k1gInSc1	Rašín
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
slíbil	slíbit	k5eAaPmAgMnS	slíbit
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
slib	slib	k1gInSc4	slib
lepšího	dobrý	k2eAgInSc2d2	lepší
přístupu	přístup	k1gInSc2	přístup
Vídně	Vídeň	k1gFnSc2	Vídeň
vůči	vůči	k7c3	vůči
Čechům	Čech	k1gMnPc3	Čech
<g/>
.16	.16	k4	.16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
poslední	poslední	k2eAgFnSc1d1	poslední
rakouská	rakouský	k2eAgFnSc1d1	rakouská
nabídka	nabídka	k1gFnSc1	nabídka
adresovaná	adresovaný	k2eAgFnSc1d1	adresovaná
prezidentu	prezident	k1gMnSc3	prezident
Wilsonovi	Wilson	k1gMnSc3	Wilson
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
Rakouskem	Rakousko	k1gNnSc7	Rakousko
švédské	švédský	k2eAgInPc1d1	švédský
diplomacii	diplomacie	k1gFnSc4	diplomacie
již	již	k6eAd1	již
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
snažil	snažit	k5eAaImAgMnS	snažit
zachránit	zachránit	k5eAaPmF	zachránit
svoji	svůj	k3xOyFgFnSc4	svůj
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nabídka	nabídka	k1gFnSc1	nabídka
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
autonomii	autonomie	k1gFnSc4	autonomie
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Slováky	Slovák	k1gMnPc4	Slovák
nikoliv	nikoliv	k9	nikoliv
<g/>
,	,	kIx,	,
citát	citát	k1gInSc4	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
úprava	úprava	k1gFnSc1	úprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
celistvosti	celistvost	k1gFnPc4	celistvost
zemí	zem	k1gFnPc2	zem
Svaté	svatý	k2eAgFnSc2d1	svatá
koruny	koruna	k1gFnSc2	koruna
uherské	uherský	k2eAgNnSc1d1	Uherské
...	...	k?	...
<g/>
"	"	kIx"	"
Neřešila	řešit	k5eNaImAgFnS	řešit
řadu	řada	k1gFnSc4	řada
problémů	problém	k1gInPc2	problém
<g/>
:	:	kIx,	:
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
národy	národ	k1gInPc1	národ
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
(	(	kIx(	(
<g/>
Uhersko	Uhersko	k1gNnSc1	Uhersko
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
federalizace	federalizace	k1gFnSc2	federalizace
úplně	úplně	k6eAd1	úplně
vyloučeno	vyloučen	k2eAgNnSc1d1	vyloučeno
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
si	se	k3xPyFc3	se
zachovat	zachovat	k5eAaPmF	zachovat
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chybělo	chybět	k5eAaImAgNnS	chybět
také	také	k9	také
řešení	řešení	k1gNnSc4	řešení
Jihoslovanské	jihoslovanský	k2eAgFnSc2d1	Jihoslovanská
otázky	otázka	k1gFnSc2	otázka
atd.	atd.	kA	atd.
Odmítnutím	odmítnutí	k1gNnSc7	odmítnutí
Karlova	Karlův	k2eAgInSc2d1	Karlův
manifestu	manifest	k1gInSc2	manifest
jeho	jeho	k3xOp3gMnPc7	jeho
hlavními	hlavní	k2eAgMnPc7d1	hlavní
protivníky	protivník	k1gMnPc7	protivník
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
státy	stát	k1gInPc1	stát
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
sympatizující	sympatizující	k2eAgInPc4d1	sympatizující
s	s	k7c7	s
Habsburky	Habsburk	k1gMnPc7	Habsburk
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
upozorňovaly	upozorňovat	k5eAaImAgInP	upozorňovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
šílené	šílený	k2eAgNnSc4d1	šílené
<g/>
"	"	kIx"	"
načasování	načasování	k1gNnSc4	načasování
manifestu	manifest	k1gInSc2	manifest
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zaslán	zaslat	k5eAaPmNgInS	zaslat
minimálně	minimálně	k6eAd1	minimálně
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
již	již	k6eAd1	již
někdy	někdy	k6eAd1	někdy
během	během	k7c2	během
podzimu	podzim	k1gInSc2	podzim
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
tuto	tento	k3xDgFnSc4	tento
deklaraci	deklarace	k1gFnSc4	deklarace
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
také	také	k9	také
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
<g/>
Poslední	poslední	k2eAgNnPc1d1	poslední
slova	slovo	k1gNnPc1	slovo
habsburského	habsburský	k2eAgMnSc2d1	habsburský
panovníka	panovník	k1gMnSc2	panovník
vládnoucího	vládnoucí	k2eAgNnSc2d1	vládnoucí
území	území	k1gNnSc2	území
budoucí	budoucí	k2eAgFnSc2d1	budoucí
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
12	[number]	k4	12
dní	den	k1gInPc2	den
před	před	k7c7	před
oficiálním	oficiální	k2eAgInSc7d1	oficiální
rozpadem	rozpad	k1gInSc7	rozpad
Podunajské	podunajský	k2eAgFnSc2d1	Podunajská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
zněla	znět	k5eAaImAgFnS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
státi	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
chtějí	chtít	k5eAaImIp3nP	chtít
jeho	jeho	k3xOp3gNnSc4	jeho
národové	národový	k2eAgNnSc4d1	národové
<g/>
,	,	kIx,	,
státem	stát	k1gInSc7	stát
spolkovým	spolkový	k2eAgInSc7d1	spolkový
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
každý	každý	k3xTgInSc1	každý
národní	národní	k2eAgInSc1d1	národní
kmen	kmen	k1gInSc1	kmen
tvoří	tvořit	k5eAaImIp3nS	tvořit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
obývá	obývat	k5eAaImIp3nS	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nepředbíhá	předbíhat	k5eNaImIp3nS	předbíhat
spojení	spojení	k1gNnSc1	spojení
polských	polský	k2eAgNnPc2d1	polské
území	území	k1gNnPc2	území
Rakouska	Rakousko	k1gNnSc2	Rakousko
s	s	k7c7	s
polským	polský	k2eAgInSc7d1	polský
neodvislým	odvislý	k2eNgInSc7d1	neodvislý
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
Terstu	Terst	k1gInSc6	Terst
s	s	k7c7	s
obvodem	obvod	k1gInSc7	obvod
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
přání	přání	k1gNnSc2	přání
jeho	jeho	k3xOp3gNnSc2	jeho
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
postavení	postavení	k1gNnSc2	postavení
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
bylo	být	k5eAaImAgNnS	být
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
zasláno	zaslat	k5eAaPmNgNnS	zaslat
prezidentu	prezident	k1gMnSc3	prezident
Wilsonovi	Wilson	k1gMnSc3	Wilson
"	"	kIx"	"
<g/>
Prohlášení	prohlášení	k1gNnSc3	prohlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
československého	československý	k2eAgInSc2d1	československý
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
odpověď	odpověď	k1gFnSc4	odpověď
Habsburkům	Habsburk	k1gMnPc3	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
text	text	k1gInSc1	text
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
označen	označit	k5eAaPmNgInS	označit
jako	jako	k8xS	jako
Washingtonská	washingtonský	k2eAgFnSc1d1	Washingtonská
deklarace	deklarace	k1gFnSc1	deklarace
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
o	o	k7c6	o
plánech	plán	k1gInPc6	plán
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
Habsburského	habsburský	k2eAgInSc2d1	habsburský
na	na	k7c6	na
federalizaci	federalizace	k1gFnSc6	federalizace
věděl	vědět	k5eAaImAgMnS	vědět
a	a	k8xC	a
proto	proto	k8xC	proto
text	text	k1gInSc1	text
prohlášení	prohlášení	k1gNnSc2	prohlášení
připravil	připravit	k5eAaPmAgInS	připravit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
<g/>
;	;	kIx,	;
z	z	k7c2	z
části	část	k1gFnSc2	část
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
pracoval	pracovat	k5eAaImAgMnS	pracovat
přímo	přímo	k6eAd1	přímo
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Wilson	Wilson	k1gMnSc1	Wilson
odepsal	odepsat	k5eAaPmAgMnS	odepsat
Masarykovi	Masaryk	k1gMnSc3	Masaryk
<g/>
,	,	kIx,	,
že	že	k8xS	že
deklaraci	deklarace	k1gFnSc4	deklarace
četl	číst	k5eAaImAgMnS	číst
<g/>
,	,	kIx,	,
dojala	dojmout	k5eAaPmAgFnS	dojmout
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
již	již	k6eAd1	již
dává	dávat	k5eAaImIp3nS	dávat
odpověď	odpověď	k1gFnSc4	odpověď
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
bude	být	k5eAaImBp3nS	být
Masaryk	Masaryk	k1gMnSc1	Masaryk
spokojen	spokojit	k5eAaPmNgMnS	spokojit
<g/>
.	.	kIx.	.
</s>
<s>
Washingtonská	washingtonský	k2eAgFnSc1d1	Washingtonská
deklarace	deklarace	k1gFnSc1	deklarace
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Důležitější	důležitý	k2eAgNnSc1d2	důležitější
ale	ale	k9	ale
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
obdržel	obdržet	k5eAaPmAgMnS	obdržet
také	také	k6eAd1	také
švédský	švédský	k2eAgMnSc1d1	švédský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
působící	působící	k2eAgMnSc1d1	působící
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
Wilsonovu	Wilsonův	k2eAgFnSc4d1	Wilsonova
odpověď	odpověď	k1gFnSc4	odpověď
pro	pro	k7c4	pro
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
bylo	být	k5eAaImAgNnS	být
pověřeno	pověřit	k5eAaPmNgNnS	pověřit
zastupováním	zastupování	k1gNnSc7	zastupování
zájmů	zájem	k1gInPc2	zájem
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
vůči	vůči	k7c3	vůči
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
Wilsonovy	Wilsonův	k2eAgFnSc2d1	Wilsonova
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
velvyslanci	velvyslanec	k1gMnPc1	velvyslanec
Švédska	Švédsko	k1gNnSc2	Švédsko
předal	předat	k5eAaPmAgMnS	předat
Robert	Robert	k1gMnSc1	Robert
Lansing	Lansing	k1gInSc4	Lansing
<g/>
,	,	kIx,	,
nenechal	nechat	k5eNaPmAgMnS	nechat
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
rakouské	rakouský	k2eAgInPc4d1	rakouský
vládní	vládní	k2eAgInPc4d1	vládní
kruhy	kruh	k1gInPc4	kruh
na	na	k7c6	na
pochybách	pochyba	k1gFnPc6	pochyba
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouhá	pouhý	k2eAgFnSc1d1	pouhá
autonomie	autonomie	k1gFnSc1	autonomie
rakousko-uherských	rakouskoherský	k2eAgInPc2d1	rakousko-uherský
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Wilson	Wilson	k1gInSc1	Wilson
zapsal	zapsat	k5eAaPmAgInS	zapsat
ještě	ještě	k9	ještě
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
do	do	k7c2	do
svých	svůj	k3xOyFgMnPc2	svůj
čtrnácti	čtrnáct	k4xCc2	čtrnáct
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
nemůže	moct	k5eNaImIp3nS	moct
už	už	k6eAd1	už
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
podklad	podklad	k1gInSc4	podklad
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Citát	citát	k1gInSc1	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mezi	mezi	k7c7	mezi
Čechoslováky	Čechoslovák	k1gMnPc7	Čechoslovák
a	a	k8xC	a
říší	říš	k1gFnSc7	říš
německou	německý	k2eAgFnSc7d1	německá
i	i	k8xC	i
rakousko-uherskou	rakouskoherský	k2eAgFnSc7d1	rakousko-uherská
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
válečný	válečný	k2eAgInSc1d1	válečný
a	a	k8xC	a
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	facto	k1gNnSc4	facto
válčící	válčící	k2eAgFnSc1d1	válčící
strana	strana	k1gFnSc1	strana
...	...	k?	...
a	a	k8xC	a
ony	onen	k3xDgMnPc4	onen
<g/>
,	,	kIx,	,
ne	ne	k9	ne
on	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
býti	být	k5eAaImF	být
soudci	soudce	k1gMnPc1	soudce
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
akce	akce	k1gFnPc4	akce
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vlády	vláda	k1gFnSc2	vláda
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
uspokojí	uspokojit	k5eAaPmIp3nP	uspokojit
aspirace	aspirace	k1gFnSc2	aspirace
a	a	k8xC	a
mínění	mínění	k1gNnSc2	mínění
národů	národ	k1gInPc2	národ
o	o	k7c6	o
jejich	jejich	k3xOp3gNnPc6	jejich
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
o	o	k7c6	o
určení	určení	k1gNnSc6	určení
jejich	jejich	k3xOp3gInPc1	jejich
jakožto	jakožto	k8xS	jakožto
členů	člen	k1gMnPc2	člen
rodiny	rodina	k1gFnSc2	rodina
národů	národ	k1gInPc2	národ
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
ve	v	k7c4	v
3	[number]	k4	3
hodiny	hodina	k1gFnSc2	hodina
odpoledne	odpoledne	k1gNnSc2	odpoledne
je	být	k5eAaImIp3nS	být
americkým	americký	k2eAgNnSc7d1	americké
velvyslanectvím	velvyslanectví	k1gNnSc7	velvyslanectví
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
úředně	úředně	k6eAd1	úředně
Benešovi	Beneš	k1gMnSc3	Beneš
sdělena	sdělen	k2eAgFnSc1d1	sdělena
odpověď	odpověď	k1gFnSc1	odpověď
prezidenta	prezident	k1gMnSc2	prezident
Wilsona	Wilson	k1gMnSc2	Wilson
rakousko-uherské	rakouskoherský	k2eAgFnSc3d1	rakousko-uherská
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
mír	mír	k1gInSc1	mír
bude	být	k5eAaImBp3nS	být
jen	jen	k9	jen
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
uznání	uznání	k1gNnSc2	uznání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
a	a	k8xC	a
Jihoslovanů	Jihoslovan	k1gMnPc2	Jihoslovan
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
přestalo	přestat	k5eAaPmAgNnS	přestat
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
mezinárodně	mezinárodně	k6eAd1	mezinárodně
<g/>
,	,	kIx,	,
právně	právně	k6eAd1	právně
a	a	k8xC	a
diplomaticky	diplomaticky	k6eAd1	diplomaticky
pro	pro	k7c4	pro
Spojence	spojenec	k1gMnPc4	spojenec
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
fakticky	fakticky	k6eAd1	fakticky
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
stát	stát	k1gInSc1	stát
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
udržoval	udržovat	k5eAaImAgMnS	udržovat
na	na	k7c6	na
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
než	než	k8xS	než
kapituloval	kapitulovat	k5eAaBmAgInS	kapitulovat
také	také	k9	také
vojensky	vojensky	k6eAd1	vojensky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
s	s	k7c7	s
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
Československého	československý	k2eAgInSc2d1	československý
Václavem	Václav	k1gMnSc7	Václav
Klofáčem	klofáč	k1gInSc7	klofáč
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	k9	aby
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
následující	následující	k2eAgFnPc1d1	následující
události	událost	k1gFnPc1	událost
obešly	obejít	k5eAaPmAgFnP	obejít
bez	bez	k7c2	bez
krveprolití	krveprolití	k1gNnSc2	krveprolití
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
velení	velení	k1gNnSc1	velení
pražské	pražský	k2eAgFnSc2d1	Pražská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
posádky	posádka	k1gFnSc2	posádka
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
klást	klást	k5eAaImF	klást
událostem	událost	k1gFnPc3	událost
žádný	žádný	k3yNgInSc4	žádný
odpor	odpor	k1gInSc1	odpor
a	a	k8xC	a
úloha	úloha	k1gFnSc1	úloha
vojska	vojsko	k1gNnSc2	vojsko
se	se	k3xPyFc4	se
omezí	omezit	k5eAaPmIp3nS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
pořádku	pořádek	k1gInSc2	pořádek
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
jednali	jednat	k5eAaImAgMnP	jednat
Maďaři	Maďar	k1gMnPc1	Maďar
s	s	k7c7	s
Milanem	Milan	k1gMnSc7	Milan
Hodžou	Hodža	k1gMnSc7	Hodža
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
politiky	politik	k1gMnPc7	politik
<g/>
,	,	kIx,	,
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
vytvoření	vytvoření	k1gNnSc4	vytvoření
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
slovenského	slovenský	k2eAgNnSc2d1	slovenské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Události	událost	k1gFnSc2	událost
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
delegace	delegace	k1gFnSc2	delegace
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
vedená	vedený	k2eAgFnSc1d1	vedená
Karlem	Karel	k1gMnSc7	Karel
Kramářem	kramář	k1gMnSc7	kramář
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
představitelem	představitel	k1gMnSc7	představitel
protirakouského	protirakouský	k2eAgInSc2d1	protirakouský
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
a	a	k8xC	a
podobě	podoba	k1gFnSc6	podoba
samostatného	samostatný	k2eAgInSc2d1	samostatný
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
bude	být	k5eAaImBp3nS	být
republikou	republika	k1gFnSc7	republika
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
uvažována	uvažovat	k5eAaImNgFnS	uvažovat
i	i	k9	i
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
Kramář	kramář	k1gMnSc1	kramář
bude	být	k5eAaImBp3nS	být
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
kolem	kolem	k7c2	kolem
9	[number]	k4	9
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k2eAgFnSc2d1	ranní
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
Antonín	Antonín	k1gMnSc1	Antonín
Švehla	Švehla	k1gMnSc1	Švehla
a	a	k8xC	a
František	František	k1gMnSc1	František
Soukup	Soukup	k1gMnSc1	Soukup
jménem	jméno	k1gNnSc7	jméno
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
převzít	převzít	k5eAaPmF	převzít
Obilní	obilní	k2eAgInSc4d1	obilní
ústav	ústav	k1gInSc4	ústav
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zabránili	zabránit	k5eAaPmAgMnP	zabránit
odvozu	odvoz	k1gInSc3	odvoz
obilí	obilí	k1gNnSc2	obilí
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechali	nechat	k5eAaPmAgMnP	nechat
zaměstnance	zaměstnanec	k1gMnSc4	zaměstnanec
ústavu	ústav	k1gInSc2	ústav
přísahat	přísahat	k5eAaImF	přísahat
věrnost	věrnost	k1gFnSc4	věrnost
nově	nově	k6eAd1	nově
vznikajícímu	vznikající	k2eAgInSc3d1	vznikající
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
uznání	uznání	k1gNnSc6	uznání
podmínek	podmínka	k1gFnPc2	podmínka
míru	míra	k1gFnSc4	míra
Rakousko-Uherskem	Rakousko-Uhersek	k1gInSc7	Rakousko-Uhersek
<g/>
.	.	kIx.	.
<g/>
Podmínky	podmínka	k1gFnPc1	podmínka
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
i	i	k9	i
uznání	uznání	k1gNnSc4	uznání
autonomie	autonomie	k1gFnSc2	autonomie
národů	národ	k1gInPc2	národ
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
lid	lid	k1gInSc1	lid
vyložil	vyložit	k5eAaPmAgInS	vyložit
jako	jako	k9	jako
uznání	uznání	k1gNnSc4	uznání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
impulsem	impuls	k1gInSc7	impuls
k	k	k7c3	k
živelným	živelný	k2eAgFnPc3d1	živelná
demonstracím	demonstrace	k1gFnPc3	demonstrace
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
lid	lid	k1gInSc1	lid
jásal	jásat	k5eAaImAgMnS	jásat
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
ničil	ničit	k5eAaImAgMnS	ničit
symboly	symbol	k1gInPc4	symbol
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
u	u	k7c2	u
pomníku	pomník	k1gInSc2	pomník
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
promluvil	promluvit	k5eAaPmAgMnS	promluvit
k	k	k7c3	k
davům	dav	k1gInPc3	dav
kněz	kněz	k1gMnSc1	kněz
Isidor	Isidor	k1gMnSc1	Isidor
Zahradník	Zahradník	k1gMnSc1	Zahradník
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
samostatný	samostatný	k2eAgInSc1d1	samostatný
Československý	československý	k2eAgInSc1d1	československý
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
<g/>
Večer	večer	k1gInSc1	večer
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vydal	vydat	k5eAaPmAgInS	vydat
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
první	první	k4xOgInSc1	první
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
československého	československý	k2eAgInSc2d1	československý
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
zveřejněno	zveřejněn	k2eAgNnSc1d1	zveřejněno
provolání	provolání	k1gNnSc1	provolání
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
"	"	kIx"	"
<g/>
Lide	lid	k1gInSc5	lid
československý	československý	k2eAgMnSc1d1	československý
<g/>
.	.	kIx.	.
</s>
<s>
Tvůj	tvůj	k3xOp2gInSc4	tvůj
odvěký	odvěký	k2eAgInSc4d1	odvěký
sen	sen	k1gInSc4	sen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
skutkem	skutek	k1gInSc7	skutek
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
"	"	kIx"	"
Pod	pod	k7c7	pod
oběma	dva	k4xCgInPc7	dva
dokumenty	dokument	k1gInPc7	dokument
byli	být	k5eAaImAgMnP	být
podepsáni	podepsat	k5eAaPmNgMnP	podepsat
Antonín	Antonín	k1gMnSc1	Antonín
Švehla	Švehla	k1gMnSc1	Švehla
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Stříbrný	stříbrný	k1gInSc1	stříbrný
<g/>
,	,	kIx,	,
Vavro	Vavro	k1gNnSc1	Vavro
Šrobár	Šrobár	k1gMnSc1	Šrobár
a	a	k8xC	a
František	František	k1gMnSc1	František
Soukup	Soukup	k1gMnSc1	Soukup
–	–	k?	–
později	pozdě	k6eAd2	pozdě
zvaní	zvaní	k1gNnSc1	zvaní
"	"	kIx"	"
<g/>
Muži	muž	k1gMnSc3	muž
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
přibráni	přibrán	k2eAgMnPc1d1	přibrán
4	[number]	k4	4
zástupci	zástupce	k1gMnPc7	zástupce
<g/>
,	,	kIx,	,
zástupci	zástupce	k1gMnPc1	zástupce
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
Maďarů	Maďar	k1gMnPc2	Maďar
nebyli	být	k5eNaImAgMnP	být
přizváni	přizván	k2eAgMnPc1d1	přizván
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyvrcholení	vyvrcholení	k1gNnSc4	vyvrcholení
převratu	převrat	k1gInSc2	převrat
==	==	k?	==
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
jednal	jednat	k5eAaImAgInS	jednat
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
místodržitelem	místodržitel	k1gMnSc7	místodržitel
Maxem	Max	k1gMnSc7	Max
Coudenhovem	Coudenhov	k1gInSc7	Coudenhov
o	o	k7c4	o
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
kompromis	kompromis	k1gInSc1	kompromis
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
přihlásil	přihlásit	k5eAaPmAgInS	přihlásit
k	k	k7c3	k
prozatímnímu	prozatímní	k2eAgNnSc3d1	prozatímní
zřízení	zřízení	k1gNnSc3	zřízení
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Navečer	navečer	k6eAd1	navečer
se	se	k3xPyFc4	se
pražské	pražský	k2eAgNnSc1d1	Pražské
vojenské	vojenský	k2eAgNnSc1d1	vojenské
velitelství	velitelství	k1gNnSc1	velitelství
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
s	s	k7c7	s
Národním	národní	k2eAgInSc7d1	národní
výborem	výbor	k1gInSc7	výbor
střetnutí	střetnutí	k1gNnSc2	střetnutí
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
němečtí	německý	k2eAgMnPc1d1	německý
poslanci	poslanec	k1gMnPc1	poslanec
usnesli	usnést	k5eAaPmAgMnP	usnést
vydělit	vydělit	k5eAaPmF	vydělit
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
pohraničí	pohraničí	k1gNnSc2	pohraničí
Čech	Čechy	k1gFnPc2	Čechy
provincii	provincie	k1gFnSc6	provincie
Německé	německý	k2eAgMnPc4d1	německý
Čechy	Čech	k1gMnPc4	Čech
(	(	kIx(	(
<g/>
Deutschböhmen	Deutschböhmen	k2eAgMnSc1d1	Deutschböhmen
<g/>
)	)	kIx)	)
a	a	k8xC	a
začlenit	začlenit	k5eAaPmF	začlenit
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xS	jako
autonomní	autonomní	k2eAgFnSc4d1	autonomní
jednotku	jednotka	k1gFnSc4	jednotka
do	do	k7c2	do
příštího	příští	k2eAgNnSc2d1	příští
Německého	německý	k2eAgNnSc2d1	německé
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
konstituovala	konstituovat	k5eAaBmAgFnS	konstituovat
československá	československý	k2eAgFnSc1d1	Československá
státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Pardubicích	Pardubice	k1gInPc6	Pardubice
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německý	německý	k2eAgMnSc1d1	německý
generální	generální	k2eAgMnSc1d1	generální
konzul	konzul	k1gMnSc1	konzul
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
reprezentant	reprezentant	k1gMnSc1	reprezentant
německé	německý	k2eAgFnSc2d1	německá
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Gebsattel	Gebsattel	k1gMnSc1	Gebsattel
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
blahopřál	blahopřát	k5eAaImAgInS	blahopřát
Národnímu	národní	k2eAgInSc3d1	národní
výboru	výbor	k1gInSc3	výbor
k	k	k7c3	k
revoluci	revoluce	k1gFnSc3	revoluce
a	a	k8xC	a
přál	přát	k5eAaImAgMnS	přát
si	se	k3xPyFc3	se
co	co	k3yRnSc4	co
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
navázání	navázání	k1gNnSc4	navázání
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
vznikající	vznikající	k2eAgFnSc7d1	vznikající
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zajištění	zajištění	k1gNnSc1	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
německých	německý	k2eAgMnPc2d1	německý
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pobývali	pobývat	k5eAaImAgMnP	pobývat
na	na	k7c6	na
území	území	k1gNnSc6	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
byl	být	k5eAaImAgInS	být
závěrečným	závěrečný	k2eAgNnSc7d1	závěrečné
dnem	dno	k1gNnSc7	dno
pražského	pražský	k2eAgInSc2d1	pražský
převratu	převrat	k1gInSc2	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Rakouské	rakouský	k2eAgNnSc1d1	rakouské
vojenské	vojenský	k2eAgNnSc1d1	vojenské
velitelství	velitelství	k1gNnSc1	velitelství
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
před	před	k7c7	před
Národním	národní	k2eAgInSc7d1	národní
výborem	výbor	k1gInSc7	výbor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
maďarští	maďarský	k2eAgMnPc1d1	maďarský
a	a	k8xC	a
rumunští	rumunský	k2eAgMnPc1d1	rumunský
vojáci	voják	k1gMnPc1	voják
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
v	v	k7c6	v
Turčianském	Turčianský	k2eAgInSc6d1	Turčianský
Svätém	Svätý	k2eAgInSc6d1	Svätý
Martinu	Martina	k1gFnSc4	Martina
sjelo	sjet	k5eAaPmAgNnS	sjet
200	[number]	k4	200
politických	politický	k2eAgMnPc2d1	politický
vůdců	vůdce	k1gMnPc2	vůdce
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
přijali	přijmout	k5eAaPmAgMnP	přijmout
Martinskou	martinský	k2eAgFnSc4d1	Martinská
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
se	se	k3xPyFc4	se
Slovensko	Slovensko	k1gNnSc1	Slovensko
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
ustanovili	ustanovit	k5eAaPmAgMnP	ustanovit
dvacetičlennou	dvacetičlenný	k2eAgFnSc4d1	dvacetičlenná
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
jako	jako	k8xC	jako
výkonný	výkonný	k2eAgInSc4d1	výkonný
orgán	orgán	k1gInSc4	orgán
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
Matúšem	Matúš	k1gMnSc7	Matúš
Dulou	Dula	k1gMnSc7	Dula
<g/>
.	.	kIx.	.
</s>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
bez	bez	k7c2	bez
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pouhým	pouhý	k2eAgInSc7d1	pouhý
hlasitým	hlasitý	k2eAgInSc7d1	hlasitý
souhlasem	souhlas	k1gInSc7	souhlas
vůdců	vůdce	k1gMnPc2	vůdce
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
aktem	akt	k1gInSc7	akt
se	se	k3xPyFc4	se
zřekli	zřeknout	k5eAaPmAgMnP	zřeknout
uherské	uherský	k2eAgFnPc4d1	uherská
nadvlády	nadvláda	k1gFnPc4	nadvláda
a	a	k8xC	a
Uhersku	Uhersko	k1gNnSc6	Uhersko
odepřeli	odepřít	k5eAaPmAgMnP	odepřít
právo	právo	k1gNnSc4	právo
mluvit	mluvit	k5eAaImF	mluvit
jménem	jméno	k1gNnSc7	jméno
slovenského	slovenský	k2eAgInSc2d1	slovenský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
jednání	jednání	k1gNnSc4	jednání
představitelů	představitel	k1gMnPc2	představitel
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Sudetoněmecký	sudetoněmecký	k2eAgMnSc1d1	sudetoněmecký
poslanec	poslanec	k1gMnSc1	poslanec
R.	R.	kA	R.
Lodgmann	Lodgmann	k1gNnSc1	Lodgmann
von	von	k1gInSc1	von
Auen	Auen	k1gMnSc1	Auen
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
Národním	národní	k2eAgInSc7d1	národní
výborem	výbor	k1gInSc7	výbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
chtěl	chtít	k5eAaImAgInS	chtít
získat	získat	k5eAaPmF	získat
německé	německý	k2eAgMnPc4d1	německý
politiky	politik	k1gMnPc4	politik
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Lodgmann	Lodgmann	k1gMnSc1	Lodgmann
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
spolupráci	spolupráce	k1gFnSc4	spolupráce
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
poslanci	poslanec	k1gMnPc1	poslanec
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
severu	sever	k1gInSc3	sever
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
R.	R.	kA	R.
Freisslerem	Freissler	k1gMnSc7	Freissler
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
zřízení	zřízení	k1gNnSc4	zřízení
provincie	provincie	k1gFnSc2	provincie
Sudetsko	Sudetsko	k1gNnSc1	Sudetsko
(	(	kIx(	(
<g/>
Sudetenland	Sudetenland	k1gInSc1	Sudetenland
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
Fritz	Fritz	k1gMnSc1	Fritz
Gebsattel	Gebsattel	k1gMnSc1	Gebsattel
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
německé	německý	k2eAgFnSc3d1	německá
vládě	vláda	k1gFnSc3	vláda
navázání	navázání	k1gNnSc2	navázání
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
završena	završit	k5eAaPmNgFnS	završit
i	i	k9	i
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
liberální	liberální	k2eAgFnSc1d1	liberální
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
vstanul	vstanout	k5eAaPmAgInS	vstanout
Mihály	Mihál	k1gInPc4	Mihál
Károlyi	Károly	k1gFnSc2	Károly
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
zaslala	zaslat	k5eAaPmAgFnS	zaslat
do	do	k7c2	do
Martina	Martin	k1gMnSc2	Martin
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
radě	rada	k1gFnSc3	rada
oficiální	oficiální	k2eAgInSc4d1	oficiální
pozdrav	pozdrav	k1gInSc4	pozdrav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
zřekla	zřeknout	k5eAaPmAgFnS	zřeknout
dřívější	dřívější	k2eAgFnSc1d1	dřívější
maďarizace	maďarizace	k1gFnSc1	maďarizace
prováděné	prováděný	k2eAgInPc1d1	prováděný
uherskou	uherský	k2eAgFnSc7d1	uherská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
nastínila	nastínit	k5eAaPmAgNnP	nastínit
budoucí	budoucí	k2eAgNnPc1d1	budoucí
soužití	soužití	k1gNnPc1	soužití
Maďarů	Maďar	k1gMnPc2	Maďar
se	s	k7c7	s
Slováky	Slovák	k1gMnPc7	Slovák
v	v	k7c6	v
maďarském	maďarský	k2eAgInSc6d1	maďarský
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
budoucnost	budoucnost	k1gFnSc4	budoucnost
však	však	k9	však
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
strikně	strikně	k6eAd1	strikně
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
ovládala	ovládat	k5eAaImAgFnS	ovládat
jen	jen	k9	jen
omezené	omezený	k2eAgNnSc4d1	omezené
území	území	k1gNnSc4	území
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
zapotřebí	zapotřebí	k6eAd1	zapotřebí
vojenské	vojenský	k2eAgFnSc2d1	vojenská
pomoci	pomoc	k1gFnSc2	pomoc
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
soustátí	soustátí	k1gNnSc2	soustátí
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
prakticky	prakticky	k6eAd1	prakticky
zcela	zcela	k6eAd1	zcela
vybudovat	vybudovat	k5eAaPmF	vybudovat
celý	celý	k2eAgInSc4d1	celý
slovenský	slovenský	k2eAgInSc4d1	slovenský
státní	státní	k2eAgInSc4d1	státní
aparát	aparát	k1gInSc4	aparát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
rozpadu	rozpad	k1gInSc6	rozpad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
události	událost	k1gFnPc1	událost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
ve	v	k7c6	v
městech	město	k1gNnPc6	město
i	i	k8xC	i
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
oslavy	oslava	k1gFnSc2	oslava
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgInPc1d1	slavnostní
průvody	průvod	k1gInPc1	průvod
a	a	k8xC	a
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Lid	lid	k1gInSc1	lid
jásal	jásat	k5eAaImAgInS	jásat
nad	nad	k7c7	nad
nabytou	nabytý	k2eAgFnSc7d1	nabytá
samostatností	samostatnost	k1gFnSc7	samostatnost
i	i	k8xC	i
nad	nad	k7c7	nad
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
oslavách	oslava	k1gFnPc6	oslava
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
monarchii	monarchie	k1gFnSc4	monarchie
byť	byť	k8xS	byť
jen	jen	k6eAd1	jen
připomínaly	připomínat	k5eAaImAgFnP	připomínat
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
dav	dav	k1gInSc1	dav
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Franty	Franta	k1gMnSc2	Franta
Sauera	Sauer	k1gMnSc2	Sauer
zničit	zničit	k5eAaPmF	zničit
historickou	historický	k2eAgFnSc4d1	historická
památku	památka	k1gFnSc4	památka
-	-	kIx~	-
mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xC	jako
připomínka	připomínka	k1gFnSc1	připomínka
na	na	k7c4	na
Švédské	švédský	k2eAgNnSc4d1	švédské
obléhání	obléhání	k1gNnSc4	obléhání
Prahy	Praha	k1gFnSc2	Praha
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Fritz	Fritz	k1gMnSc1	Fritz
Gebsattel	Gebsattel	k1gMnSc1	Gebsattel
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
konzul	konzul	k1gMnSc1	konzul
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
vyslechli	vyslechnout	k5eAaPmAgMnP	vyslechnout
Švehla	Švehla	k1gMnSc1	Švehla
<g/>
,	,	kIx,	,
Stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
a	a	k8xC	a
Soukup	Soukup	k1gMnSc1	Soukup
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
uznává	uznávat	k5eAaImIp3nS	uznávat
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
proti	proti	k7c3	proti
navázání	navázání	k1gNnSc3	navázání
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
však	však	k9	však
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
bez	bez	k7c2	bez
předchozí	předchozí	k2eAgFnSc2d1	předchozí
konzultace	konzultace	k1gFnSc2	konzultace
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistil	zajistit	k5eAaPmAgMnS	zajistit
bezpečí	bezpečí	k1gNnSc3	bezpečí
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
toto	tento	k3xDgNnSc4	tento
nakonec	nakonec	k6eAd1	nakonec
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
s	s	k7c7	s
výhradou	výhrada	k1gFnSc7	výhrada
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
ČSR	ČSR	kA	ČSR
dojde	dojít	k5eAaPmIp3nS	dojít
až	až	k9	až
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
uznání	uznání	k1gNnSc2	uznání
tzv.	tzv.	kA	tzv.
Německého	německý	k2eAgNnSc2d1	německé
Rakouska	Rakousko	k1gNnSc2	Rakousko
Československem	Československo	k1gNnSc7	Československo
<g/>
,	,	kIx,	,
a	a	k8xC	a
až	až	k9	až
ČSR	ČSR	kA	ČSR
vyšle	vyslat	k5eAaPmIp3nS	vyslat
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
svého	svůj	k3xOyFgMnSc2	svůj
zmocněnce	zmocněnec	k1gMnSc2	zmocněnec
<g/>
.	.	kIx.	.
</s>
<s>
Fritz	Fritz	k1gMnSc1	Fritz
Gebsattel	Gebsattel	k1gMnSc1	Gebsattel
poté	poté	k6eAd1	poté
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Národní	národní	k2eAgInSc4d1	národní
výbor	výbor	k1gInSc4	výbor
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zástupci	zástupce	k1gMnPc7	zástupce
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
proklamovali	proklamovat	k5eAaBmAgMnP	proklamovat
ustanovení	ustanovení	k1gNnSc4	ustanovení
provincie	provincie	k1gFnSc2	provincie
Deutschsüdmähren	Deutschsüdmährna	k1gFnPc2	Deutschsüdmährna
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
vznik	vznik	k1gInSc4	vznik
provincie	provincie	k1gFnSc2	provincie
Böhmerwaldgau	Böhmerwaldgaus	k1gInSc2	Böhmerwaldgaus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
připojeny	připojit	k5eAaPmNgInP	připojit
k	k	k7c3	k
Německému	německý	k2eAgNnSc3d1	německé
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
překročily	překročit	k5eAaPmAgInP	překročit
první	první	k4xOgInPc1	první
české	český	k2eAgInPc1d1	český
vojenské	vojenský	k2eAgInPc1d1	vojenský
oddíly	oddíl	k1gInPc1	oddíl
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
síle	síla	k1gFnSc6	síla
asi	asi	k9	asi
1100	[number]	k4	1100
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
četníků	četník	k1gMnPc2	četník
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
měly	mít	k5eAaImAgFnP	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
předvoj	předvoj	k1gInSc4	předvoj
větší	veliký	k2eAgFnSc2d2	veliký
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
postupně	postupně	k6eAd1	postupně
přicházet	přicházet	k5eAaImF	přicházet
z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
bojišť	bojiště	k1gNnPc2	bojiště
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
takto	takto	k6eAd1	takto
malém	malý	k2eAgInSc6d1	malý
počtu	počet	k1gInSc6	počet
osvobodily	osvobodit	k5eAaPmAgFnP	osvobodit
Malacky	Malacky	k1gFnPc1	Malacky
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
obce	obec	k1gFnPc1	obec
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
zatímní	zatímní	k2eAgFnSc1d1	zatímní
slovenská	slovenský	k2eAgFnSc1d1	slovenská
vláda	vláda	k1gFnSc1	vláda
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Vávro	Vávra	k1gMnSc5	Vávra
Šrobár	Šrobár	k1gMnSc1	Šrobár
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Dérer	Dérer	k1gMnSc1	Dérer
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Blaho	blaho	k6eAd1	blaho
a	a	k8xC	a
Anton	Anton	k1gMnSc1	Anton
Štefánek	Štefánek	k1gMnSc1	Štefánek
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
úřadovala	úřadovat	k5eAaImAgFnS	úřadovat
ve	v	k7c6	v
Skalici	Skalice	k1gFnSc6	Skalice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
moravských	moravský	k2eAgFnPc2d1	Moravská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
měla	mít	k5eAaImAgFnS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
nabírat	nabírat	k5eAaImF	nabírat
nové	nový	k2eAgMnPc4d1	nový
slovenské	slovenský	k2eAgMnPc4d1	slovenský
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
se	se	k3xPyFc4	se
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
odebralo	odebrat	k5eAaPmAgNnS	odebrat
i	i	k9	i
100	[number]	k4	100
četníků	četník	k1gMnPc2	četník
jako	jako	k8xS	jako
jejich	jejich	k3xOp3gFnSc1	jejich
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
Americká	americký	k2eAgFnSc1d1	americká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
uherských	uherský	k2eAgInPc2d1	uherský
Rusínů	rusín	k1gInPc2	rusín
v	v	k7c6	v
Scrantonu	Scranton	k1gInSc6	Scranton
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
připojit	připojit	k5eAaPmF	připojit
Podkarpatskou	podkarpatský	k2eAgFnSc4d1	Podkarpatská
Rus	Rus	k1gFnSc4	Rus
k	k	k7c3	k
Československu	Československo	k1gNnSc3	Československo
<g/>
.13	.13	k4	.13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byla	být	k5eAaImAgFnS	být
schválená	schválený	k2eAgFnSc1d1	schválená
prozatimní	prozatimní	k2eAgFnSc1d1	prozatimní
ústava	ústava	k1gFnSc1	ústava
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
národní	národní	k2eAgInSc4d1	národní
výbor	výbor	k1gInSc4	výbor
na	na	k7c4	na
256	[number]	k4	256
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
Revoluční	revoluční	k2eAgNnSc4d1	revoluční
národní	národní	k2eAgNnSc4d1	národní
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
příměří	příměří	k1gNnSc1	příměří
mezi	mezi	k7c7	mezi
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
Spojenci	spojenec	k1gMnPc1	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
článku	článek	k1gInSc2	článek
tohoto	tento	k3xDgNnSc2	tento
příměří	příměří	k1gNnSc2	příměří
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
prozatím	prozatím	k6eAd1	prozatím
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
veškeré	veškerý	k3xTgNnSc4	veškerý
území	území	k1gNnSc4	území
Uher	uher	k1gInSc1	uher
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
Slavonie	Slavonie	k1gFnSc2	Slavonie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
pak	pak	k6eAd1	pak
Maďaři	Maďar	k1gMnPc1	Maďar
považovali	považovat	k5eAaImAgMnP	považovat
pronikání	pronikání	k1gNnSc4	pronikání
českých	český	k2eAgMnPc2d1	český
vojáků	voják	k1gMnPc2	voják
na	na	k7c4	na
slovenské	slovenský	k2eAgNnSc4d1	slovenské
území	území	k1gNnSc4	území
za	za	k7c4	za
agresi	agrese	k1gFnSc4	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Maďarům	Maďar	k1gMnPc3	Maďar
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
dařilo	dařit	k5eAaImAgNnS	dařit
ze	z	k7c2	z
slovenského	slovenský	k2eAgNnSc2d1	slovenské
území	území	k1gNnSc2	území
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
české	český	k2eAgFnSc2d1	Česká
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Trnavy	Trnava	k1gFnSc2	Trnava
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
do	do	k7c2	do
Žiliny	Žilina	k1gFnSc2	Žilina
a	a	k8xC	a
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
i	i	k9	i
do	do	k7c2	do
Martina	Martin	k1gMnSc2	Martin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zajali	zajmout	k5eAaPmAgMnP	zajmout
předsedu	předseda	k1gMnSc4	předseda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
Matúše	Matúše	k1gFnSc2	Matúše
Dulu	Dulus	k1gInSc2	Dulus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
sešlo	sejít	k5eAaPmAgNnS	sejít
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gMnSc1	jeho
předseda	předseda	k1gMnSc1	předseda
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
oznámil	oznámit	k5eAaPmAgMnS	oznámit
konec	konec	k1gInSc4	konec
vlády	vláda	k1gFnSc2	vláda
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
republiku	republika	k1gFnSc4	republika
československou	československý	k2eAgFnSc4d1	Československá
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgInS	zahájit
volbu	volba	k1gFnSc4	volba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
aklamací	aklamace	k1gFnSc7	aklamace
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
zvolení	zvolení	k1gNnSc1	zvolení
vedení	vedení	k1gNnSc2	vedení
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
první	první	k4xOgFnSc2	první
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jenž	k3xRgNnSc2	jenž
čela	čelo	k1gNnSc2	čelo
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
dále	daleko	k6eAd2	daleko
vydalo	vydat	k5eAaPmAgNnS	vydat
zákon	zákon	k1gInSc4	zákon
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
osmihodinové	osmihodinový	k2eAgFnSc6d1	osmihodinová
pracovní	pracovní	k2eAgFnSc6d1	pracovní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
maďarský	maďarský	k2eAgMnSc1d1	maďarský
emisar	emisar	k1gMnSc1	emisar
Géza	géz	k1gMnSc2	géz
Supka	Supek	k1gInSc2	Supek
s	s	k7c7	s
oficiální	oficiální	k2eAgFnSc7d1	oficiální
nótou	nóta	k1gFnSc7	nóta
oznamující	oznamující	k2eAgNnSc4d1	oznamující
právo	právo	k1gNnSc4	právo
převzetí	převzetí	k1gNnSc2	převzetí
území	území	k1gNnSc2	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
bělehradského	bělehradský	k2eAgNnSc2d1	Bělehradské
příměří	příměří	k1gNnSc2	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
odpovědělo	odpovědět	k5eAaPmAgNnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
samostatnost	samostatnost	k1gFnSc1	samostatnost
ČSR	ČSR	kA	ČSR
byla	být	k5eAaImAgFnS	být
uznána	uznat	k5eAaPmNgFnS	uznat
Spojenci	spojenec	k1gMnPc7	spojenec
již	již	k6eAd1	již
před	před	k7c7	před
podepsáním	podepsání	k1gNnSc7	podepsání
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebylo	být	k5eNaImAgNnS	být
Slovensko	Slovensko	k1gNnSc1	Slovensko
součástí	součást	k1gFnPc2	součást
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
stanovil	stanovit	k5eAaPmAgMnS	stanovit
hranice	hranice	k1gFnPc4	hranice
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
i	i	k9	i
představitelé	představitel	k1gMnPc1	představitel
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
oficiálním	oficiální	k2eAgInSc7d1	oficiální
dopisem	dopis	k1gInSc7	dopis
francouzskému	francouzský	k2eAgInSc3d1	francouzský
ministru	ministr	k1gMnSc6	ministr
Pichonovi	Pichon	k1gMnSc6	Pichon
požádal	požádat	k5eAaPmAgMnS	požádat
Beneš	Beneš	k1gMnSc1	Beneš
o	o	k7c4	o
stažení	stažení	k1gNnSc4	stažení
maďarské	maďarský	k2eAgFnSc2d1	maďarská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
z	z	k7c2	z
území	území	k1gNnSc2	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
obsadila	obsadit	k5eAaPmAgFnS	obsadit
rodící	rodící	k2eAgFnSc1d1	rodící
se	se	k3xPyFc4	se
československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
Most	most	k1gInSc1	most
a	a	k8xC	a
zahájila	zahájit	k5eAaPmAgFnS	zahájit
tak	tak	k6eAd1	tak
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
likvidaci	likvidace	k1gFnSc4	likvidace
sudetoněmeckých	sudetoněmecký	k2eAgFnPc2d1	Sudetoněmecká
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
převzala	převzít	k5eAaPmAgFnS	převzít
armáda	armáda	k1gFnSc1	armáda
správu	správa	k1gFnSc4	správa
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Nálada	nálada	k1gFnSc1	nálada
se	se	k3xPyFc4	se
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
oblastech	oblast	k1gFnPc6	oblast
opět	opět	k6eAd1	opět
vyostřila	vyostřit	k5eAaPmAgFnS	vyostřit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
když	když	k8xS	když
české	český	k2eAgInPc1d1	český
úřady	úřad	k1gInPc1	úřad
zakázaly	zakázat	k5eAaPmAgInP	zakázat
českým	český	k2eAgMnPc3d1	český
Němcům	Němec	k1gMnPc3	Němec
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
rakouského	rakouský	k2eAgInSc2d1	rakouský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následných	následný	k2eAgFnPc6d1	následná
demonstracích	demonstrace	k1gFnPc6	demonstrace
4	[number]	k4	4
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1919	[number]	k4	1919
bylo	být	k5eAaImAgNnS	být
českou	český	k2eAgFnSc7d1	Česká
armádou	armáda	k1gFnSc7	armáda
zabito	zabít	k5eAaPmNgNnS	zabít
54	[number]	k4	54
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
84	[number]	k4	84
osob	osoba	k1gFnPc2	osoba
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
březnový	březnový	k2eAgInSc1d1	březnový
incident	incident	k1gInSc1	incident
značně	značně	k6eAd1	značně
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
další	další	k2eAgInPc4d1	další
česko-německé	českoěmecký	k2eAgInPc4d1	česko-německý
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
šlechtické	šlechtický	k2eAgInPc1d1	šlechtický
tituly	titul	k1gInPc1	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
vydalo	vydat	k5eAaPmAgNnS	vydat
francouzské	francouzský	k2eAgNnSc1d1	francouzské
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Československo	Československo	k1gNnSc1	Československo
jako	jako	k8xC	jako
spojenecký	spojenecký	k2eAgInSc1d1	spojenecký
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
podržet	podržet	k5eAaPmF	podržet
až	až	k9	až
do	do	k7c2	do
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
území	území	k1gNnSc2	území
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
historických	historický	k2eAgFnPc2d1	historická
hranic	hranice	k1gFnPc2	hranice
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
uzákonilo	uzákonit	k5eAaPmAgNnS	uzákonit
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
osmi	osm	k4xCc2	osm
hodinovou	hodinový	k2eAgFnSc4d1	hodinová
pracovní	pracovní	k2eAgFnSc4d1	pracovní
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
slavnostně	slavnostně	k6eAd1	slavnostně
uvítán	uvítán	k2eAgInSc1d1	uvítán
Národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
v	v	k7c6	v
železniční	železniční	k2eAgFnSc6d1	železniční
stanici	stanice	k1gFnSc6	stanice
Horní	horní	k2eAgNnSc4d1	horní
Dvořiště	dvořiště	k1gNnSc4	dvořiště
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
poprvé	poprvé	k6eAd1	poprvé
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
zaplněném	zaplněný	k2eAgNnSc6d1	zaplněné
velkém	velký	k2eAgNnSc6d1	velké
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Dvořišti	dvořiště	k1gNnSc6	dvořiště
si	se	k3xPyFc3	se
prezident	prezident	k1gMnSc1	prezident
Masaryk	Masaryk	k1gMnSc1	Masaryk
postěžoval	postěžovat	k5eAaPmAgMnS	postěžovat
předsedovi	předseda	k1gMnSc3	předseda
ústavního	ústavní	k2eAgInSc2d1	ústavní
výboru	výbor	k1gInSc2	výbor
parlamentu	parlament	k1gInSc2	parlament
Alfrédu	Alfréd	k1gMnSc3	Alfréd
Meissnerovi	Meissner	k1gMnSc3	Meissner
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nespokojen	spokojit	k5eNaPmNgInS	spokojit
s	s	k7c7	s
pravomocemi	pravomoc	k1gFnPc7	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
dány	dát	k5eAaPmNgInP	dát
v	v	k7c6	v
prozatimní	prozatimní	k2eAgFnSc6d1	prozatimní
ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
prezident	prezident	k1gMnSc1	prezident
Masaryk	Masaryk	k1gMnSc1	Masaryk
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
triumfální	triumfální	k2eAgFnSc6d1	triumfální
cestě	cesta	k1gFnSc6	cesta
vlakem	vlak	k1gInSc7	vlak
z	z	k7c2	z
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
jej	on	k3xPp3gNnSc2	on
v	v	k7c6	v
železničních	železniční	k2eAgFnPc6d1	železniční
stanicích	stanice	k1gFnPc6	stanice
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
(	(	kIx(	(
<g/>
zastávky	zastávka	k1gFnSc2	zastávka
Veselí-Mezimostí	Veselí-Mezimost	k1gFnPc2	Veselí-Mezimost
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
Benešov	Benešov	k1gInSc1	Benešov
<g/>
)	)	kIx)	)
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
vítaly	vítat	k5eAaImAgFnP	vítat
tisíce	tisíc	k4xCgInPc1	tisíc
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
odpoledne	odpoledne	k6eAd1	odpoledne
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
vlaku	vlak	k1gInSc2	vlak
na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
královské	královský	k2eAgFnSc2d1	královská
čekárny	čekárna	k1gFnSc2	čekárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pozdravil	pozdravit	k5eAaPmAgMnS	pozdravit
s	s	k7c7	s
reprezentací	reprezentace	k1gFnSc7	reprezentace
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
čekali	čekat	k5eAaImAgMnP	čekat
ministři	ministr	k1gMnPc1	ministr
<g/>
,	,	kIx,	,
podnikatelé	podnikatel	k1gMnPc1	podnikatel
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
oblečen	obléct	k5eAaPmNgMnS	obléct
v	v	k7c6	v
zimníku	zimník	k1gInSc6	zimník
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgNnSc7	který
měl	mít	k5eAaImAgInS	mít
žaketový	žaketový	k2eAgInSc4d1	žaketový
oblek	oblek	k1gInSc4	oblek
s	s	k7c7	s
pruhovanými	pruhovaný	k2eAgFnPc7d1	pruhovaná
kalhotami	kalhoty	k1gFnPc7	kalhoty
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přivítání	přivítání	k1gNnSc6	přivítání
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Kramářem	kramář	k1gMnSc7	kramář
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
premiérem	premiér	k1gMnSc7	premiér
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
pronesl	pronést	k5eAaPmAgMnS	pronést
Masaryk	Masaryk	k1gMnSc1	Masaryk
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
proslov	proslov	k1gInSc4	proslov
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
mu	on	k3xPp3gMnSc3	on
Sylva	Sylva	k1gFnSc1	Sylva
Macharová	Macharový	k2eAgFnSc1d1	Macharový
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
básníka	básník	k1gMnSc2	básník
Josefa	Josef	k1gMnSc2	Josef
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Machara	Machar	k1gMnSc2	Machar
<g/>
,	,	kIx,	,
předala	předat	k5eAaPmAgFnS	předat
proutek	proutek	k1gInSc4	proutek
vavřínu	vavřín	k1gInSc2	vavřín
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
stuhou	stuha	k1gFnSc7	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
motiv	motiv	k1gInSc4	motiv
poté	poté	k6eAd1	poté
využil	využít	k5eAaPmAgMnS	využít
Max	Max	k1gMnSc1	Max
Švabinský	Švabinský	k2eAgMnSc1d1	Švabinský
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
prvního	první	k4xOgInSc2	první
oficiálního	oficiální	k2eAgInSc2d1	oficiální
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
portrétu	portrét	k1gInSc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
odpoledne	odpoledne	k6eAd1	odpoledne
složil	složit	k5eAaPmAgMnS	složit
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
přistaveného	přistavený	k2eAgNnSc2d1	přistavené
vozidla	vozidlo	k1gNnSc2	vozidlo
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Charlottou	Charlotta	k1gFnSc7	Charlotta
na	na	k7c4	na
Veleslavín	Veleslavín	k1gInSc4	Veleslavín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
tamním	tamní	k2eAgNnSc6d1	tamní
sanatoriu	sanatorium	k1gNnSc6	sanatorium
již	již	k6eAd1	již
od	od	k7c2	od
května	květen	k1gInSc2	květen
1918	[number]	k4	1918
léčila	léčit	k5eAaImAgFnS	léčit
a	a	k8xC	a
setrvala	setrvat	k5eAaPmAgFnS	setrvat
zde	zde	k6eAd1	zde
až	až	k6eAd1	až
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
22	[number]	k4	22
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
přednesl	přednést	k5eAaPmAgMnS	přednést
Masaryk	Masaryk	k1gMnSc1	Masaryk
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
Hradě	hrad	k1gInSc6	hrad
své	svůj	k3xOyFgNnSc4	svůj
První	první	k4xOgNnSc4	první
poselství	poselství	k1gNnSc4	poselství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
nastínil	nastínit	k5eAaPmAgMnS	nastínit
koncepci	koncepce	k1gFnSc4	koncepce
Československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
podvolilo	podvolit	k5eAaPmAgNnS	podvolit
nótě	nóta	k1gFnSc6	nóta
Dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c4	o
stanovení	stanovení	k1gNnSc4	stanovení
demarkační	demarkační	k2eAgFnSc2d1	demarkační
čáry	čára	k1gFnSc2	čára
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
zbytek	zbytek	k1gInSc1	zbytek
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
zaslala	zaslat	k5eAaPmAgFnS	zaslat
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
polské	polský	k2eAgFnSc2d1	polská
vládě	vláda	k1gFnSc3	vláda
memorandum	memorandum	k1gNnSc1	memorandum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
fakticky	fakticky	k6eAd1	fakticky
ohlašovalo	ohlašovat	k5eAaImAgNnS	ohlašovat
vojenské	vojenský	k2eAgNnSc4d1	vojenské
obsazování	obsazování	k1gNnSc4	obsazování
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
trvaly	trvat	k5eAaImAgInP	trvat
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
demarkační	demarkační	k2eAgFnSc6d1	demarkační
čáře	čára	k1gFnSc6	čára
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
okolkování	okolkování	k1gNnSc4	okolkování
bankovek	bankovka	k1gFnPc2	bankovka
Rakousko-Uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájen	k2eAgNnSc1d1	zahájeno
vytváření	vytváření	k1gNnSc1	vytváření
vlastní	vlastní	k2eAgFnSc2d1	vlastní
československé	československý	k2eAgFnSc2d1	Československá
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
stanné	stanný	k2eAgNnSc1d1	stanné
právo	právo	k1gNnSc1	právo
proti	proti	k7c3	proti
vůdcům	vůdce	k1gMnPc3	vůdce
bolševického	bolševický	k2eAgNnSc2d1	bolševické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
republiky	republika	k1gFnSc2	republika
rad	rada	k1gFnPc2	rada
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
března	březen	k1gInSc2	březen
překročily	překročit	k5eAaPmAgInP	překročit
oddíly	oddíl	k1gInPc1	oddíl
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
demarkační	demarkační	k2eAgFnSc4d1	demarkační
čáru	čára	k1gFnSc4	čára
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
zahájily	zahájit	k5eAaPmAgFnP	zahájit
intervenci	intervence	k1gFnSc4	intervence
proti	proti	k7c3	proti
Maďarské	maďarský	k2eAgFnSc3d1	maďarská
republice	republika	k1gFnSc3	republika
rad	rada	k1gFnPc2	rada
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byl	být	k5eAaImAgInS	být
přijat	přijat	k2eAgInSc1d1	přijat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
pozemkové	pozemkový	k2eAgFnSc6d1	pozemková
reformě	reforma	k1gFnSc6	reforma
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
záboru	zábor	k1gInSc6	zábor
pozemkového	pozemkový	k2eAgInSc2d1	pozemkový
majetku	majetek	k1gInSc2	majetek
nad	nad	k7c4	nad
150	[number]	k4	150
hektarů	hektar	k1gInPc2	hektar
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
nebo	nebo	k8xC	nebo
250	[number]	k4	250
hektarů	hektar	k1gInPc2	hektar
půdy	půda	k1gFnSc2	půda
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
nehodě	nehoda	k1gFnSc6	nehoda
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
v	v	k7c6	v
Užhorodě	Užhorod	k1gInSc6	Užhorod
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
porada	porada	k1gFnSc1	porada
rusínských	rusínský	k2eAgFnPc2d1	rusínská
národních	národní	k2eAgFnPc2d1	národní
rad	rada	k1gFnPc2	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
výsledek	výsledek	k1gInSc4	výsledek
plebiscitu	plebiscit	k1gInSc2	plebiscit
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
k	k	k7c3	k
Československu	Československo	k1gNnSc3	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přešla	přejít	k5eAaPmAgFnS	přejít
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
republika	republika	k1gFnSc1	republika
rad	rada	k1gFnPc2	rada
do	do	k7c2	do
protiútoku	protiútok	k1gInSc2	protiútok
a	a	k8xC	a
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Slovenska	Slovensko	k1gNnSc2	Slovensko
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
rad	rada	k1gFnPc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Zvolen	Zvolen	k1gInSc4	Zvolen
však	však	k9	však
musela	muset	k5eAaImAgNnP	muset
maďarská	maďarský	k2eAgNnPc1d1	Maďarské
vojska	vojsko	k1gNnPc1	vojsko
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
postupně	postupně	k6eAd1	postupně
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
rad	rada	k1gFnPc2	rada
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
Československo	Československo	k1gNnSc4	Československo
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předložil	předložit	k5eAaPmAgMnS	předložit
státníkům	státník	k1gMnPc3	státník
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
memorand	memorandum	k1gNnPc2	memorandum
týkajících	týkající	k2eAgNnPc2d1	týkající
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
Československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zde	zde	k6eAd1	zde
Beneš	Beneš	k1gMnSc1	Beneš
předložil	předložit	k5eAaPmAgMnS	předložit
byla	být	k5eAaImAgFnS	být
Nóta	nóta	k1gFnSc1	nóta
o	o	k7c6	o
národnostním	národnostní	k2eAgInSc6d1	národnostní
režimu	režim	k1gInSc6	režim
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
republika	republika	k1gFnSc1	republika
"	"	kIx"	"
<g/>
zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
vybudovat	vybudovat	k5eAaPmF	vybudovat
organizaci	organizace	k1gFnSc4	organizace
státu	stát	k1gInSc2	stát
na	na	k7c6	na
přijetí	přijetí	k1gNnSc6	přijetí
národních	národní	k2eAgNnPc2d1	národní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
zásad	zásada	k1gFnPc2	zásada
uplatňovaných	uplatňovaný	k2eAgFnPc2d1	uplatňovaná
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
nótě	nóta	k1gFnSc6	nóta
uvedeno	uvést	k5eAaPmNgNnS	uvést
např.	např.	kA	např.
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
bude	být	k5eAaImBp3nS	být
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
rovnoprávným	rovnoprávný	k2eAgInSc7d1	rovnoprávný
jazykem	jazyk	k1gInSc7	jazyk
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
bude	být	k5eAaImBp3nS	být
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
veřejné	veřejný	k2eAgInPc1d1	veřejný
úřady	úřad	k1gInPc1	úřad
budou	být	k5eAaImBp3nP	být
otevřeny	otevřít	k5eAaPmNgInP	otevřít
všem	všecek	k3xTgMnPc3	všecek
národnostem	národnost	k1gFnPc3	národnost
obývajícím	obývající	k2eAgNnSc6d1	obývající
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ze	z	k7c2	z
slibů	slib	k1gInPc2	slib
daných	daný	k2eAgInPc2d1	daný
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nótě	nóta	k1gFnSc6	nóta
následně	následně	k6eAd1	následně
splněny	splnit	k5eAaPmNgFnP	splnit
nebyly	být	k5eNaImAgFnP	být
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
dáno	dán	k2eAgNnSc4d1	dáno
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoby	osoba	k1gFnPc1	osoba
zaměstnané	zaměstnaný	k2eAgFnPc1d1	zaměstnaná
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
službě	služba	k1gFnSc6	služba
musí	muset	k5eAaImIp3nP	muset
ovládat	ovládat	k5eAaImF	ovládat
státní	státní	k2eAgInSc4d1	státní
jazyk	jazyk	k1gInSc4	jazyk
–	–	k?	–
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
němčina	němčina	k1gFnSc1	němčina
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Beneš	Beneš	k1gMnSc1	Beneš
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
předložil	předložit	k5eAaPmAgMnS	předložit
také	také	k9	také
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
koridoru	koridor	k1gInSc2	koridor
(	(	kIx(	(
<g/>
pásu	pás	k1gInSc2	pás
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
zaručoval	zaručovat	k5eAaImAgInS	zaručovat
spojení	spojení	k1gNnSc4	spojení
Prahy	Praha	k1gFnSc2	Praha
s	s	k7c7	s
Terstem	Terst	k1gInSc7	Terst
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
Hamburkem	Hamburk	k1gInSc7	Hamburk
(	(	kIx(	(
<g/>
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
o	o	k7c4	o
Koblenzi	Koblenze	k1gFnSc4	Koblenze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
spojit	spojit	k5eAaPmF	spojit
Československo	Československo	k1gNnSc4	Československo
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
myšlenky	myšlenka	k1gFnSc2	myšlenka
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
předložil	předložit	k5eAaPmAgMnS	předložit
britskému	britský	k2eAgMnSc3d1	britský
ministrovi	ministr	k1gMnSc3	ministr
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
věcí	věc	k1gFnSc7	věc
plán	plán	k1gInSc4	plán
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
Chorvatů	Chorvat	k1gMnPc2	Chorvat
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
–	–	k?	–
předpokládané	předpokládaný	k2eAgFnPc4d1	předpokládaná
"	"	kIx"	"
<g/>
Svobodné	svobodný	k2eAgFnPc4d1	svobodná
Čechy	Čechy	k1gFnPc4	Čechy
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
předpokládaným	předpokládaný	k2eAgNnSc7d1	předpokládané
"	"	kIx"	"
<g/>
Srbochorvatskem	Srbochorvatsko	k1gNnSc7	Srbochorvatsko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
koridoru	koridor	k1gInSc2	koridor
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
usilovných	usilovný	k2eAgFnPc2d1	usilovná
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
československo	československo	k1gNnSc4	československo
–	–	k?	–
jugoslávské	jugoslávský	k2eAgNnSc4d1	jugoslávské
spojenectví	spojenectví	k1gNnSc4	spojenectví
(	(	kIx(	(
<g/>
snaha	snaha	k1gFnSc1	snaha
byla	být	k5eAaImAgFnS	být
především	především	k6eAd1	především
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
cítilo	cítit	k5eAaImAgNnS	cítit
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
Malá	malý	k2eAgFnSc1d1	malá
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
plán	plán	k1gInSc1	plán
s	s	k7c7	s
koridorem	koridor	k1gInSc7	koridor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nereálným	reálný	k2eNgInSc7d1	nereálný
i	i	k9	i
další	další	k2eAgInSc1d1	další
Československý	československý	k2eAgInSc1d1	československý
požadavek	požadavek	k1gInSc1	požadavek
přednesený	přednesený	k2eAgInSc1d1	přednesený
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
začlenění	začlenění	k1gNnSc6	začlenění
německého	německý	k2eAgNnSc2d1	německé
území	území	k1gNnSc2	území
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
,	,	kIx,	,
obývané	obývaný	k2eAgNnSc1d1	obývané
Lužickými	lužický	k2eAgMnPc7d1	lužický
Srby	Srb	k1gMnPc7	Srb
<g/>
,	,	kIx,	,
do	do	k7c2	do
hranic	hranice	k1gFnPc2	hranice
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tento	tento	k3xDgInSc1	tento
požadavek	požadavek	k1gInSc1	požadavek
byl	být	k5eAaImAgInS	být
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
by	by	kYmCp3nS	by
Československo	Československo	k1gNnSc1	Československo
muselo	muset	k5eAaImAgNnS	muset
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lužickými	lužický	k2eAgMnPc7d1	lužický
Srby	Srb	k1gMnPc7	Srb
pojmout	pojmout	k5eAaPmF	pojmout
i	i	k9	i
další	další	k2eAgInPc4d1	další
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
Lužici	Lužice	k1gFnSc6	Lužice
žilo	žít	k5eAaImAgNnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
hranic	hranice	k1gFnPc2	hranice
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
názor	názor	k1gInSc1	názor
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
pak	pak	k6eAd1	pak
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
–	–	k?	–
zachovat	zachovat	k5eAaPmF	zachovat
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
Československo	Československo	k1gNnSc4	Československo
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
hranicích	hranice	k1gFnPc6	hranice
tzv.	tzv.	kA	tzv.
zemí	zem	k1gFnPc2	zem
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Odmítnuta	odmítnut	k2eAgFnSc1d1	odmítnuta
byla	být	k5eAaImAgFnS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c4	na
připojení	připojení	k1gNnSc4	připojení
německých	německý	k2eAgFnPc2d1	německá
oblastí	oblast	k1gFnPc2	oblast
Československa	Československo	k1gNnSc2	Československo
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
nebo	nebo	k8xC	nebo
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
Československo	Československo	k1gNnSc4	Československo
oslabilo	oslabit	k5eAaPmAgNnS	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
hranicích	hranice	k1gFnPc6	hranice
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
tak	tak	k9	tak
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
nejen	nejen	k6eAd1	nejen
důvody	důvod	k1gInPc1	důvod
historické	historický	k2eAgInPc1d1	historický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
strategické	strategický	k2eAgInPc4d1	strategický
<g/>
,	,	kIx,	,
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	Hranice	k1gFnPc1	Hranice
Československa	Československo	k1gNnSc2	Československo
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
dokumenty	dokument	k1gInPc7	dokument
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
určila	určit	k5eAaPmAgFnS	určit
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
Německo	Německo	k1gNnSc1	Německo
uznávalo	uznávat	k5eAaImAgNnS	uznávat
nezávislost	nezávislost	k1gFnSc4	nezávislost
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
odstupovalo	odstupovat	k5eAaImAgNnS	odstupovat
mu	on	k3xPp3gMnSc3	on
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
hornoslezského	hornoslezský	k2eAgInSc2d1	hornoslezský
okresu	okres	k1gInSc2	okres
Ratiboř	Ratiboř	k1gFnSc1	Ratiboř
(	(	kIx(	(
<g/>
Hlučínsko	Hlučínsko	k1gNnSc1	Hlučínsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavazovalo	zavazovat	k5eAaImAgNnS	zavazovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
poskytnout	poskytnout	k5eAaPmF	poskytnout
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
99	[number]	k4	99
let	léto	k1gNnPc2	léto
svobodná	svobodný	k2eAgNnPc1d1	svobodné
pásma	pásmo	k1gNnPc1	pásmo
v	v	k7c6	v
přístavech	přístav	k1gInPc6	přístav
Hamburk	Hamburk	k1gInSc1	Hamburk
a	a	k8xC	a
Štětín	Štětín	k1gInSc1	Štětín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
86	[number]	k4	86
se	se	k3xPyFc4	se
Československo	Československo	k1gNnSc1	Československo
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
přijmout	přijmout	k5eAaPmF	přijmout
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
případná	případný	k2eAgNnPc1d1	případné
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
by	by	kYmCp3nP	by
vítězné	vítězný	k2eAgFnPc1d1	vítězná
mocnosti	mocnost	k1gFnPc1	mocnost
udělaly	udělat	k5eAaPmAgFnP	udělat
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
menšin	menšina	k1gFnPc2	menšina
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
určila	určit	k5eAaPmAgFnS	určit
Saint-Germainská	Saint-Germainský	k2eAgFnSc1d1	Saint-Germainská
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
smlouvou	smlouva	k1gFnSc7	smlouva
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
uznán	uznat	k5eAaPmNgInS	uznat
vznik	vznik	k1gInSc1	vznik
svrchované	svrchovaný	k2eAgFnSc2d1	svrchovaná
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
uznávalo	uznávat	k5eAaImAgNnS	uznávat
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
územní	územní	k2eAgFnSc4d1	územní
nedotknutelnost	nedotknutelnost	k1gFnSc4	nedotknutelnost
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
zříkalo	zříkat	k5eAaImAgNnS	zříkat
se	se	k3xPyFc4	se
územních	územní	k2eAgInPc2d1	územní
nároků	nárok	k1gInPc2	nárok
vůči	vůči	k7c3	vůči
Československu	Československo	k1gNnSc3	Československo
a	a	k8xC	a
odstupovalo	odstupovat	k5eAaImAgNnS	odstupovat
Československu	Československo	k1gNnSc6	Československo
dolnorakouská	dolnorakouský	k2eAgNnPc1d1	dolnorakouské
území	území	k1gNnPc1	území
(	(	kIx(	(
<g/>
Vitorazsko	Vitorazsko	k1gNnSc1	Vitorazsko
<g/>
,	,	kIx,	,
Valticko	Valticko	k1gNnSc1	Valticko
a	a	k8xC	a
Dyjský	dyjský	k2eAgInSc1d1	dyjský
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
Trianonskou	trianonský	k2eAgFnSc7d1	Trianonská
smlouvou	smlouva	k1gFnSc7	smlouva
4	[number]	k4	4
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
Těšínsku	Těšínsko	k1gNnSc6	Těšínsko
<g/>
,	,	kIx,	,
Oravě	Orava	k1gFnSc3	Orava
a	a	k8xC	a
Spiši	Spiš	k1gFnSc3	Spiš
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
Rada	rada	k1gFnSc1	rada
vyslanců	vyslanec	k1gMnPc2	vyslanec
28	[number]	k4	28
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
hranice	hranice	k1gFnSc1	hranice
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
zdokumentována	zdokumentován	k2eAgFnSc1d1	zdokumentována
10	[number]	k4	10
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
Sè	Sè	k1gFnSc1	Sè
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
ratifikována	ratifikován	k2eAgFnSc1d1	ratifikována
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
tak	tak	k6eAd1	tak
nakonec	nakonec	k6eAd1	nakonec
zdědilo	zdědit	k5eAaPmAgNnS	zdědit
21	[number]	k4	21
<g/>
%	%	kIx~	%
území	území	k1gNnSc6	území
zaniklého	zaniklý	k2eAgInSc2d1	zaniklý
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
a	a	k8xC	a
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
25	[number]	k4	25
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
této	tento	k3xDgFnSc2	tento
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.29	.29	k4	.29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
schválilo	schválit	k5eAaPmAgNnS	schválit
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
formálně	formálně	k6eAd1	formálně
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
Československo	Československo	k1gNnSc4	Československo
za	za	k7c4	za
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
vykonávalo	vykonávat	k5eAaImAgNnS	vykonávat
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
ze	z	k7c2	z
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KALVODA	Kalvoda	k1gMnSc1	Kalvoda
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Genese	genese	k1gFnSc1	genese
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panevropa	Panevropa	k1gFnSc1	Panevropa
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
607	[number]	k4	607
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85846	[number]	k4	85846
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KÁRNÍK	Kárník	k1gMnSc1	Kárník
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
první	první	k4xOgInSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
<g/>
,	,	kIx,	,
budování	budování	k1gNnSc4	budování
a	a	k8xC	a
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
léta	léto	k1gNnPc4	léto
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
Hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Vnitropolitický	vnitropolitický	k2eAgInSc4d1	vnitropolitický
vývoj	vývoj	k1gInSc4	vývoj
Československa	Československo	k1gNnSc2	Československo
1918-1926	[number]	k4	1918-1926
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
prezidentské	prezidentský	k2eAgNnSc4d1	prezidentské
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
Pětka	pětka	k1gFnSc1	pětka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panevropa	Panevropa	k1gFnSc1	Panevropa
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
432	[number]	k4	432
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85846	[number]	k4	85846
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Říjen	říjen	k1gInSc1	říjen
1918	[number]	k4	1918
:	:	kIx,	:
vznik	vznik	k1gInSc4	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
279	[number]	k4	279
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
175	[number]	k4	175
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
822	[number]	k4	822
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
328	[number]	k4	328
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOTVUN	KOTVUN	kA	KOTVUN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
v	v	k7c6	v
nebezpečném	bezpečný	k2eNgInSc6d1	nebezpečný
světě	svět	k1gInSc6	svět
:	:	kIx,	:
éra	éra	k1gFnSc1	éra
prezidenta	prezident	k1gMnSc2	prezident
Masaryka	Masaryk	k1gMnSc2	Masaryk
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Torst	Torst	k1gMnSc1	Torst
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
898	[number]	k4	898
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7215	[number]	k4	7215
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEROUTKA	Peroutka	k1gMnSc1	Peroutka
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
státu	stát	k1gInSc2	stát
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
867	[number]	k4	867
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1095	[number]	k4	1095
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRECLÍK	preclík	k1gInSc1	preclík
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
legie	legie	k1gFnSc1	legie
<g/>
,	,	kIx,	,
váz	váza	k1gFnPc2	váza
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
219	[number]	k4	219
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Paris	Paris	k1gMnSc1	Paris
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
Žižkova	Žižkův	k2eAgFnSc1d1	Žižkova
2379	[number]	k4	2379
(	(	kIx(	(
<g/>
734	[number]	k4	734
01	[number]	k4	01
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
)	)	kIx)	)
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Masarykovým	Masarykův	k2eAgNnSc7d1	Masarykovo
demokratickým	demokratický	k2eAgNnSc7d1	demokratické
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87173	[number]	k4	87173
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.12	.12	k4	.12
-	-	kIx~	-
25	[number]	k4	25
<g/>
,	,	kIx,	,
77	[number]	k4	77
-	-	kIx~	-
83	[number]	k4	83
<g/>
,	,	kIx,	,
140	[number]	k4	140
-	-	kIx~	-
148	[number]	k4	148
<g/>
,	,	kIx,	,
159	[number]	k4	159
-	-	kIx~	-
164	[number]	k4	164
<g/>
,	,	kIx,	,
165	[number]	k4	165
-	-	kIx~	-
199	[number]	k4	199
</s>
</p>
<p>
<s>
RYCHLÍK	Rychlík	k1gMnSc1	Rychlík
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
:	:	kIx,	:
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
konflikty	konflikt	k1gInPc1	konflikt
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
688	[number]	k4	688
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
133	[number]	k4	133
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SLÁDEK	Sládek	k1gMnSc1	Sládek
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
:	:	kIx,	:
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
Československu	Československo	k1gNnSc6	Československo
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Pragma	Pragma	k1gNnSc1	Pragma
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
205	[number]	k4	205
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7205	[number]	k4	7205
<g/>
-	-	kIx~	-
<g/>
901	[number]	k4	901
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROZEHNALOVÁ	Rozehnalová	k1gFnSc1	Rozehnalová
<g/>
,	,	kIx,	,
Zdenka	Zdenka	k1gFnSc1	Zdenka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
i	i	k9	i
tvá	tvůj	k3xOp2gFnSc1	tvůj
<g/>
:	:	kIx,	:
ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
zrození	zrození	k1gNnSc2	zrození
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
:	:	kIx,	:
E-SMILE	E-SMILE	k1gFnSc1	E-SMILE
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
288	[number]	k4	288
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
88050	[number]	k4	88050
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Clevelandská	Clevelandský	k2eAgFnSc1d1	Clevelandská
dohoda	dohoda	k1gFnSc1	dohoda
</s>
</p>
<p>
<s>
Pittsburská	pittsburský	k2eAgFnSc1d1	Pittsburská
dohoda	dohoda	k1gFnSc1	dohoda
</s>
</p>
<p>
<s>
Zánik	zánik	k1gInSc1	zánik
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vznik	vznik	k1gInSc1	vznik
Československa	Československo	k1gNnSc2	Československo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Prohlášení	prohlášení	k1gNnSc2	prohlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
československého	československý	k2eAgInSc2d1	československý
národa	národ	k1gInSc2	národ
jeho	jeho	k3xOp3gInPc4	jeho
prozatímní	prozatímní	k2eAgInPc4d1	prozatímní
vládou	vláda	k1gFnSc7	vláda
československou	československý	k2eAgFnSc7d1	Československá
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
ČSR	ČSR	kA	ČSR
v	v	k7c6	v
r.	r.	kA	r.
1918	[number]	k4	1918
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
aktivit	aktivita	k1gFnPc2	aktivita
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Osudy	osud	k1gInPc7	osud
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
"	"	kIx"	"
-	-	kIx~	-
popis	popis	k1gInSc1	popis
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
na	na	k7c4	na
www.ct24.cz	www.ct24.cz	k1gInSc4	www.ct24.cz
</s>
</p>
<p>
<s>
Blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
devadesáté	devadesátý	k4xOgFnPc4	devadesátý
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc3	vznik
Československa	Československo	k1gNnSc2	Československo
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
reportáž	reportáž	k1gFnSc4	reportáž
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Říjen	říjen	k1gInSc1	říjen
1918	[number]	k4	1918
nejen	nejen	k6eAd1	nejen
očima	oko	k1gNnPc7	oko
Jana	Jan	k1gMnSc2	Jan
Wericha	Werich	k1gMnSc2	Werich
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
Jana	Jan	k1gMnSc2	Jan
Wericha	Werich	k1gMnSc2	Werich
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
Steinbacha	Steinbach	k1gMnSc2	Steinbach
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
