<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
širokou	široký	k2eAgFnSc7d1	široká
paletou	paleta	k1gFnSc7	paleta
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
(	(	kIx(	(
<g/>
lesní	lesní	k2eAgInPc4d1	lesní
plody	plod	k1gInPc4	plod
<g/>
,	,	kIx,	,
kořínky	kořínek	k1gInPc4	kořínek
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgFnPc4d1	zemědělská
plodiny	plodina	k1gFnPc4	plodina
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc4	houba
<g/>
)	)	kIx)	)
i	i	k9	i
živočišné	živočišný	k2eAgFnSc2d1	živočišná
stravy	strava	k1gFnSc2	strava
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc4	hmyz
nebo	nebo	k8xC	nebo
malí	malý	k2eAgMnPc1d1	malý
až	až	k8xS	až
středně	středně	k6eAd1	středně
velcí	velký	k2eAgMnPc1d1	velký
savci	savec	k1gMnPc1	savec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
