<s>
Rutherfordium	Rutherfordium	k1gNnSc1
</s>
<s>
Rutherfordium	Rutherfordium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
2	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
265	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rf	Rf	kA
</s>
<s>
104	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Rutherfordium	Rutherfordium	k1gNnSc1
<g/>
,	,	kIx,
Rf	Rf	k1gFnSc1
<g/>
,	,	kIx,
104	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Rutherfordium	Rutherfordium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
53850-36-5	53850-36-5	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
265,12	265,12	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
předpokládaný	předpokládaný	k2eAgInSc1d1
150	#num#	k4
pm	pm	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
předpokládaný	předpokládaný	k2eAgInSc1d1
157	#num#	k4
pm	pm	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
14	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
2	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
experimentálně	experimentálně	k6eAd1
ověřené	ověřený	k2eAgInPc1d1
IV	IV	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
předpokládané	předpokládaný	k2eAgFnSc2d1
III	III	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
579,9	579,9	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
1389,4	1389,4	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
2296,4	2296,4	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
23	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
předpokládané	předpokládaný	k2eAgInPc4d1
pevné	pevný	k2eAgInPc4d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
2100	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
předpokládaná	předpokládaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
373,15	373,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
5500	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
předpokládaná	předpokládaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
5	#num#	k4
773,15	773,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hf	Hf	k?
<g/>
⋏	⋏	k?
</s>
<s>
Lawrencium	Lawrencium	k1gNnSc1
≺	≺	k?
<g/>
Rf	Rf	k1gMnSc2
<g/>
≻	≻	k?
Dubnium	Dubnium	k1gNnSc1
</s>
<s>
Rutherfordium	Rutherfordium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Rf	Rf	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
12	#num#	k4
<g/>
.	.	kIx.
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
nebo	nebo	k8xC
urychlovači	urychlovač	k1gInSc6
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rutherfordium	Rutherfordium	k1gNnSc1
doposud	doposud	k6eAd1
nebylo	být	k5eNaImAgNnS
izolováno	izolovat	k5eAaBmNgNnS
v	v	k7c6
dostatečně	dostatečně	k6eAd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
určit	určit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
jeho	jeho	k3xOp3gFnPc4
fyzikální	fyzikální	k2eAgFnPc4d1
konstanty	konstanta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
své	svůj	k3xOyFgFnSc6
poloze	poloha	k1gFnSc6
v	v	k7c6
periodické	periodický	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
by	by	kYmCp3nS
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
mělo	mít	k5eAaImAgNnS
připomínat	připomínat	k5eAaImF
hafnium	hafnium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Sir	sir	k1gMnSc1
Ernest	Ernest	k1gMnSc1
Rutherford	Rutherford	k1gMnSc1
</s>
<s>
První	první	k4xOgFnSc1
příprava	příprava	k1gFnSc1
prvku	prvek	k1gInSc2
s	s	k7c7
atomovým	atomový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
104	#num#	k4
byla	být	k5eAaImAgFnS
ohlášena	ohlášen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1964	#num#	k4
v	v	k7c6
laboratořích	laboratoř	k1gFnPc6
Ústavu	ústav	k1gInSc2
jaderného	jaderný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
v	v	k7c6
Dubně	Dubna	k1gFnSc6
v	v	k7c6
bývalém	bývalý	k2eAgInSc6d1
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
pomocí	pomocí	k7c2
urychlovače	urychlovač	k1gInSc2
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
přitom	přitom	k6eAd1
bombardován	bombardován	k2eAgInSc1d1
terč	terč	k1gInSc1
z	z	k7c2
plutonia	plutonium	k1gNnSc2
urychlenými	urychlený	k2eAgInPc7d1
ionty	ion	k1gInPc7
neonu	neon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvek	prvek	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgInS
kurčatovium	kurčatovium	k1gNnSc1
(	(	kIx(
<g/>
chemický	chemický	k2eAgInSc1d1
symbol	symbol	k1gInSc1
Ku	k	k7c3
<g/>
)	)	kIx)
na	na	k7c4
počest	počest	k1gFnSc4
vedoucího	vedoucí	k2eAgInSc2d1
sovětského	sovětský	k2eAgInSc2d1
jaderného	jaderný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
Igora	Igor	k1gMnSc2
V.	V.	kA
Kurčatova	Kurčatův	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
24294	#num#	k4
Pu	Pu	k1gFnSc1
+	+	kIx~
2210	#num#	k4
Ne	Ne	kA
→	→	k?
264104	#num#	k4
Rf	Rf	k1gFnSc1
</s>
<s>
Vědci	vědec	k1gMnPc1
kalifornské	kalifornský	k2eAgFnSc2d1
university	universita	k1gFnSc2
v	v	k7c4
Berkeley	Berkeley	k1gInPc4
se	se	k3xPyFc4
neúspěšně	úspěšně	k6eNd1
pokoušeli	pokoušet	k5eAaImAgMnP
opakovat	opakovat	k5eAaImF
výše	vysoce	k6eAd2
uvedený	uvedený	k2eAgInSc4d1
experiment	experiment	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zato	zato	k6eAd1
však	však	k9
uspěli	uspět	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
za	za	k7c2
pomoci	pomoc	k1gFnSc2
lineárního	lineární	k2eAgInSc2d1
urychlovače	urychlovač	k1gInSc2
částic	částice	k1gFnPc2
bombardovali	bombardovat	k5eAaImAgMnP
terče	terč	k1gInPc4
z	z	k7c2
izotopů	izotop	k1gInPc2
kalifornia	kalifornium	k1gNnSc2
jádry	jádro	k1gNnPc7
uhlíku	uhlík	k1gInSc2
12	#num#	k4
<g/>
C.	C.	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
24998	#num#	k4
Cf	Cf	k1gFnSc1
+	+	kIx~
126	#num#	k4
C	C	kA
→	→	k?
257104	#num#	k4
Rf	Rf	k1gFnPc2
+	+	kIx~
4	#num#	k4
10	#num#	k4
n	n	k0
</s>
<s>
Získali	získat	k5eAaPmAgMnP
nový	nový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
rutherfordium	rutherfordium	k1gNnSc1
na	na	k7c4
počest	počest	k1gFnSc4
jaderného	jaderný	k2eAgMnSc2d1
fyzika	fyzik	k1gMnSc2
Ernesta	Ernest	k1gMnSc2
Rutherforda	Rutherford	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedený	uvedený	k2eAgInSc1d1
název	název	k1gInSc1
byl	být	k5eAaImAgInS
definitivně	definitivně	k6eAd1
schválen	schválit	k5eAaPmNgInS
na	na	k7c6
zasedání	zasedání	k1gNnSc6
IUPAC	IUPAC	kA
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
celkem	celkem	k6eAd1
19	#num#	k4
izotopů	izotop	k1gInPc2
rutherfordia	rutherfordium	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
IzotopRok	IzotopRok	k1gInSc1
objevuReakcePoločas	objevuReakcePoločasa	k1gFnPc2
přeměny	přeměna	k1gFnSc2
</s>
<s>
253	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
<g/>
1994204	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
50	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,048	0,048	k4
ms	ms	k?
</s>
<s>
254	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
<g/>
1994206	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
50	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,023	0,023	k4
ms	ms	k?
</s>
<s>
255	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
<g/>
1974	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
1985207	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
50	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
1,68	1,68	k4
s	s	k7c7
</s>
<s>
256	#num#	k4
<g/>
Rfg	Rfg	k1gFnSc1
<g/>
1974	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
1985208	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
50	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
6,67	6,67	k4
ms	ms	k?
</s>
<s>
256	#num#	k4
<g/>
Rfm	Rfm	k1gFnSc1
<g/>
12007208	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
50	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,025	0,025	k4
ms	ms	k?
</s>
<s>
256	#num#	k4
<g/>
Rfm²	Rfm²	k1gFnSc1
<g/>
2007208	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
50	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,017	0,017	k4
ms	ms	k?
</s>
<s>
256	#num#	k4
<g/>
Rfm³	Rfm³	k1gFnSc1
<g/>
2007208	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
50	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
,2	,2	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
0,027	0,027	k4
ms	ms	k?
</s>
<s>
257	#num#	k4
<g/>
Rfg	Rfg	k1gMnSc1
<g/>
,	,	kIx,
<g/>
m	m	kA
<g/>
1969249	#num#	k4
<g/>
Cf	Cf	k1gFnPc1
<g/>
(	(	kIx(
<g/>
12	#num#	k4
<g/>
C	C	kA
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
4,4	4,4	k4
s	s	k7c7
</s>
<s>
258	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
<g/>
1969249	#num#	k4
<g/>
Cf	Cf	k1gFnPc2
<g/>
(	(	kIx(
<g/>
13	#num#	k4
<g/>
C	C	kA
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
14,7	14,7	k4
ms	ms	k?
</s>
<s>
259	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
<g/>
1969249	#num#	k4
<g/>
Cf	Cf	k1gFnPc2
<g/>
(	(	kIx(
<g/>
13	#num#	k4
<g/>
C	C	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
2,4	2,4	k4
s	s	k7c7
</s>
<s>
260	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
<g/>
1969248	#num#	k4
<g/>
Cm	cm	kA
<g/>
(	(	kIx(
<g/>
16	#num#	k4
<g/>
O	o	k7c4
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
21	#num#	k4
ms	ms	k?
</s>
<s>
261	#num#	k4
<g/>
Rfa	Rfa	k1gFnSc1
<g/>
1970248	#num#	k4
<g/>
Cm	cm	kA
<g/>
(	(	kIx(
<g/>
18	#num#	k4
<g/>
O	o	k7c4
<g/>
,5	,5	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
1,1	1,1	k4
min	mina	k1gFnPc2
</s>
<s>
261	#num#	k4
<g/>
Rfb	Rfb	k1gFnSc1
<g/>
1996208	#num#	k4
<g/>
Pb	Pb	k1gFnPc2
<g/>
(	(	kIx(
<g/>
70	#num#	k4
<g/>
Zn	zn	kA
<g/>
,	,	kIx,
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
68	#num#	k4
s	s	k7c7
</s>
<s>
262	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
<g/>
1996244	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
(	(	kIx(
<g/>
22	#num#	k4
<g/>
Ne	ne	k9
<g/>
,4	,4	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
2,3	2,3	k4
s	s	k7c7
</s>
<s>
263	#num#	k4
<g/>
Rfa	Rfa	k1gFnSc1
<g/>
1990	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
248	#num#	k4
<g/>
Cm	cm	kA
<g/>
(	(	kIx(
<g/>
18	#num#	k4
<g/>
O	o	k7c4
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
10	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
263	#num#	k4
<g/>
Rfb	Rfb	k1gFnSc1
<g/>
2004248	#num#	k4
<g/>
Cm	cm	kA
<g/>
(	(	kIx(
<g/>
26	#num#	k4
<g/>
Mg	mg	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
8	#num#	k4
s	s	k7c7
</s>
<s>
264	#num#	k4
<g/>
Rfb	Rfb	k1gFnPc2
?	?	kIx.
</s>
<s>
265	#num#	k4
<g/>
Rfb	Rfb	k1gFnSc1
<g/>
1,0	1,0	k4
min	mina	k1gFnPc2
</s>
<s>
266	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
2006237	#num#	k4
<g/>
Np	Np	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
?	?	kIx.
</s>
<s>
267	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004238	#num#	k4
<g/>
U	U	kA
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
?	?	kIx.
</s>
<s>
268	#num#	k4
<g/>
Rf	Rf	k1gFnSc1
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
2003243	#num#	k4
<g/>
Am	Am	k1gFnPc2
<g/>
(	(	kIx(
<g/>
48	#num#	k4
<g/>
Ca	ca	kA
<g/>
,3	,3	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
?	?	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
HAIRE	HAIRE	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
G.	G.	kA
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Chemistry	Chemistr	k1gMnPc4
of	of	k?
the	the	k?
Actinide	Actinid	k1gInSc5
and	and	k?
Transactinide	Transactinid	k1gInSc5
Elements	Elements	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Morss	Morssa	k1gFnPc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
rd	rd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dordrecht	Dordrecht	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Netherlands	Netherlands	k1gInSc1
<g/>
:	:	kIx,
Springer	Springer	k1gInSc1
Science	Science	k1gFnSc1
<g/>
+	+	kIx~
<g/>
Business	business	k1gInSc1
Media	medium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4020	#num#	k4
<g/>
-	-	kIx~
<g/>
3555	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Transactinides	Transactinidesa	k1gFnPc2
and	and	k?
the	the	k?
future	futur	k1gMnSc5
elements	elements	k1gInSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Chemical	Chemical	k1gFnPc2
Data	datum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rutherfordium	Rutherfordium	k1gNnSc1
-	-	kIx~
Rf	Rf	k1gMnSc1
<g/>
,	,	kIx,
Royal	Royal	k1gMnSc1
Chemical	Chemical	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
↑	↑	k?
BARBER	BARBER	kA
<g/>
,	,	kIx,
R.	R.	kA
C.	C.	kA
<g/>
,	,	kIx,
Greenwood	Greenwood	k1gInSc1
<g/>
,	,	kIx,
N.	N.	kA
N.	N.	kA
<g/>
;	;	kIx,
Hrynkiewicz	Hrynkiewicz	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
Z.	Z.	kA
<g/>
;	;	kIx,
Jeannin	Jeannin	k1gInSc1
<g/>
,	,	kIx,
Y.	Y.	kA
P.	P.	kA
<g/>
;	;	kIx,
Lefort	Lefort	k1gInSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
Sakai	Sakai	k1gNnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
Ulehla	ulehnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
I.	I.	kA
<g/>
;	;	kIx,
Wapstra	Wapstr	k1gMnSc2
<g/>
,	,	kIx,
A.	A.	kA
P.	P.	kA
<g/>
;	;	kIx,
Wilkinson	Wilkinson	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
H.	H.	kA
Discovery	Discovera	k1gFnPc1
of	of	k?
the	the	k?
transfermium	transfermium	k1gNnSc4
elements	elements	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Part	part	k1gInSc1
II	II	kA
<g/>
:	:	kIx,
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc4
discovery	discovera	k1gFnPc1
profiles	profilesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Part	part	k1gInSc1
III	III	kA
<g/>
:	:	kIx,
Discovery	Discover	k1gInPc1
profiles	profilesa	k1gFnPc2
of	of	k?
the	the	k?
transfermium	transfermium	k1gNnSc4
elements	elements	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pure	Pur	k1gInSc2
and	and	k?
Applied	Applied	k1gMnSc1
Chemistry	Chemistr	k1gMnPc7
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
65	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1757	#num#	k4
<g/>
–	–	k?
<g/>
1814	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.135	10.135	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
pac	pac	k1gInSc1
<g/>
199365081757	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GHIORSO	GHIORSO	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
,	,	kIx,
Nurmia	Nurmium	k1gNnSc2
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
Harris	Harris	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
Eskola	Eskola	k1gFnSc1
<g/>
,	,	kIx,
K.	K.	kA
<g/>
;	;	kIx,
Eskola	Eskola	k1gFnSc1
<g/>
,	,	kIx,
P.	P.	kA
Positive	positiv	k1gInSc5
Identification	Identification	k1gInSc1
of	of	k?
Two	Two	k1gFnSc1
Alpha-Particle-Emitting	Alpha-Particle-Emitting	k1gInSc1
Isotopes	Isotopes	k1gInSc1
of	of	k?
Element	element	k1gInSc1
104	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
Letters	Lettersa	k1gFnPc2
<g/>
.	.	kIx.
1969	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
22	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1317	#num#	k4
<g/>
–	–	k?
<g/>
1320	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRevLett	PhysRevLett	k1gInSc1
<g/>
.22	.22	k4
<g/>
.1317	.1317	k4
<g/>
.	.	kIx.
↑	↑	k?
BEMIS	BEMIS	kA
<g/>
,	,	kIx,
C.	C.	kA
E.	E.	kA
C.	C.	kA
E.	E.	kA
Bemis	Bemis	k1gFnPc2
<g/>
,	,	kIx,
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Silva	Silva	k1gFnSc1
<g/>
,	,	kIx,
R.	R.	kA
J.	J.	kA
<g/>
;	;	kIx,
Hensley	Henslea	k1gMnSc2
<g/>
,	,	kIx,
D.	D.	kA
C.	C.	kA
<g/>
;	;	kIx,
Keller	Keller	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
O.	O.	kA
L.	L.	kA
<g/>
;	;	kIx,
Tarrant	Tarrant	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
R.	R.	kA
<g/>
;	;	kIx,
Hunt	hunt	k1gInSc1
<g/>
,	,	kIx,
L.	L.	kA
D.	D.	kA
<g/>
;	;	kIx,
Dittner	Dittner	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
F.	F.	kA
<g/>
;	;	kIx,
Hahn	Hahn	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
L.	L.	kA
<g/>
;	;	kIx,
Goodman	Goodman	k1gMnSc1
<g/>
,	,	kIx,
C.	C.	kA
D.	D.	kA
X-Ray	X-Raa	k1gMnSc2
Identification	Identification	k1gInSc1
of	of	k?
Element	element	k1gInSc1
104	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Physical	Physical	k1gFnSc1
Review	Review	k1gFnSc2
Letters	Lettersa	k1gFnPc2
<g/>
.	.	kIx.
1973	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
31	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
647	#num#	k4
<g/>
–	–	k?
<g/>
650	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
PhysRevLett	PhysRevLett	k1gInSc1
<g/>
.31	.31	k4
<g/>
.647	.647	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rutherfordium	rutherfordium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
rutherfordium	rutherfordium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4508940-1	4508940-1	k4
</s>
