<s>
Ropná	ropný	k2eAgFnSc1d1
rafinerie	rafinerie	k1gFnSc1
je	být	k5eAaImIp3nS
petrochemický	petrochemický	k2eAgInSc4d1
závod	závod	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ropa	ropa	k1gFnSc1
čistí	čistit	k5eAaImIp3nS
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
destilace	destilace	k1gFnSc2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
různé	různý	k2eAgFnPc4d1
frakce	frakce	k1gFnPc4
podle	podle	k7c2
teploty	teplota	k1gFnSc2
varu	var	k1gInSc2
a	a	k8xC
dále	daleko	k6eAd2
zpracovává	zpracovávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>