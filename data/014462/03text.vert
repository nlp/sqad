<s>
Ropná	ropný	k2eAgFnSc1d1
rafinerie	rafinerie	k1gFnSc1
</s>
<s>
Ropná	ropný	k2eAgFnSc1d1
rafinerie	rafinerie	k1gFnSc1
firmy	firma	k1gFnSc2
Shell	Shella	k1gFnPc2
v	v	k7c4
Martinez	Martinez	k1gInSc4
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
</s>
<s>
Rafinerie	rafinerie	k1gFnSc1
Slovnaft	Slovnafta	k1gFnPc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
</s>
<s>
Ropná	ropný	k2eAgFnSc1d1
rafinerie	rafinerie	k1gFnSc1
je	být	k5eAaImIp3nS
petrochemický	petrochemický	k2eAgInSc4d1
závod	závod	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ropa	ropa	k1gFnSc1
čistí	čistit	k5eAaImIp3nS
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
destilace	destilace	k1gFnSc2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
různé	různý	k2eAgFnPc4d1
frakce	frakce	k1gFnPc4
podle	podle	k7c2
teploty	teplota	k1gFnSc2
varu	var	k1gInSc2
a	a	k8xC
dále	daleko	k6eAd2
zpracovává	zpracovávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
užitečnější	užitečný	k2eAgInPc1d2
ropné	ropný	k2eAgInPc1d1
produkty	produkt	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
motorový	motorový	k2eAgInSc1d1
benzín	benzín	k1gInSc1
<g/>
,	,	kIx,
motorová	motorový	k2eAgFnSc1d1
nafta	nafta	k1gFnSc1
<g/>
,	,	kIx,
silniční	silniční	k2eAgInPc1d1
<g/>
,	,	kIx,
modifikované	modifikovaný	k2eAgInPc1d1
a	a	k8xC
stavebně	stavebně	k6eAd1
izolační	izolační	k2eAgInPc1d1
asfalty	asfalt	k1gInPc1
<g/>
,	,	kIx,
lehké	lehký	k2eAgNnSc1d1
a	a	k8xC
těžké	těžký	k2eAgNnSc1d1
topné	topné	k1gNnSc1
oleje	olej	k1gInSc2
<g/>
,	,	kIx,
letecký	letecký	k2eAgInSc1d1
petrolej	petrolej	k1gInSc1
a	a	k8xC
letecký	letecký	k2eAgInSc1d1
benzin	benzin	k1gInSc1
<g/>
,	,	kIx,
zkapalněné	zkapalněný	k2eAgInPc1d1
uhlovodíkové	uhlovodíkový	k2eAgInPc1d1
plyny	plyn	k1gInPc1
(	(	kIx(
<g/>
LPG	LPG	kA
<g/>
,	,	kIx,
propan	propan	k1gInSc1
<g/>
,	,	kIx,
butan	butan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vakuové	vakuový	k2eAgInPc4d1
destiláty	destilát	k1gInPc4
<g/>
,	,	kIx,
parafíny	parafín	k1gInPc4
a	a	k8xC
gače	gač	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
vedlejší	vedlejší	k2eAgInSc1d1
produkt	produkt	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
síra	síra	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
užitečné	užitečný	k2eAgFnPc1d1
suroviny	surovina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Ložiska	ložisko	k1gNnPc1
ropy	ropa	k1gFnSc2
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
vzdálená	vzdálený	k2eAgFnSc1d1
od	od	k7c2
místa	místo	k1gNnSc2
spotřeby	spotřeba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doprava	doprava	k1gFnSc1
ropy	ropa	k1gFnSc2
velkými	velký	k2eAgInPc7d1
námořními	námořní	k2eAgInPc7d1
tankery	tanker	k1gInPc7
nebo	nebo	k8xC
ropovody	ropovod	k1gInPc7
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
levnější	levný	k2eAgMnSc1d2
než	než	k8xS
doprava	doprava	k6eAd1
mnoha	mnoho	k4c2
různých	různý	k2eAgInPc2d1
ropných	ropný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
odděleně	odděleně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rafinerie	rafinerie	k1gFnSc1
se	se	k3xPyFc4
proto	proto	k8xC
budují	budovat	k5eAaImIp3nP
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
míst	místo	k1gNnPc2
spotřeby	spotřeba	k1gFnSc2
<g/>
,	,	kIx,
nejlépe	dobře	k6eAd3
u	u	k7c2
mořských	mořský	k2eAgInPc2d1
přístavů	přístav	k1gInPc2
a	a	k8xC
velkých	velký	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kde	kde	k6eAd1
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
dopravuje	dopravovat	k5eAaImIp3nS
se	se	k3xPyFc4
surovina	surovina	k1gFnSc1
do	do	k7c2
rafinerií	rafinerie	k1gFnPc2
ropovody	ropovod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Technologie	technologie	k1gFnSc1
</s>
<s>
Surová	surový	k2eAgFnSc1d1
ropa	ropa	k1gFnSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
už	už	k6eAd1
v	v	k7c6
místě	místo	k1gNnSc6
těžby	těžba	k1gFnSc2
zbavuje	zbavovat	k5eAaImIp3nS
například	například	k6eAd1
písku	písek	k1gInSc2
a	a	k8xC
vody	voda	k1gFnSc2
a	a	k8xC
ropovodem	ropovod	k1gInSc7
nebo	nebo	k8xC
tankerem	tanker	k1gInSc7
převáží	převázat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
rafinerie	rafinerie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
zbavuje	zbavovat	k5eAaImIp3nS
soli	sůl	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odsolování	odsolování	k1gNnPc1
probíhá	probíhat	k5eAaImIp3nS
při	při	k7c6
teplotě	teplota	k1gFnSc6
90	#num#	k4
–	–	k?
150	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
při	při	k7c6
dvoustupňovém	dvoustupňový	k2eAgNnSc6d1
odsolování	odsolování	k1gNnSc6
se	se	k3xPyFc4
dosahuje	dosahovat	k5eAaImIp3nS
účinnosti	účinnost	k1gFnPc1
odsolení	odsolení	k1gNnSc2
až	až	k9
99	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Schéma	schéma	k1gNnSc1
destilační	destilační	k2eAgFnSc2d1
věže	věž	k1gFnSc2
s	s	k7c7
vyznačenými	vyznačený	k2eAgInPc7d1
odebíranými	odebíraný	k2eAgInPc7d1
produkty	produkt	k1gInPc7
<g/>
.	.	kIx.
1	#num#	k4
–	–	k?
destilační	destilační	k2eAgFnSc1d1
věž	věž	k1gFnSc1
2	#num#	k4
–	–	k?
pomocná	pomocný	k2eAgFnSc1d1
věž	věž	k1gFnSc1
</s>
<s>
A	a	k9
–	–	k?
ropa	ropa	k1gFnSc1
B	B	kA
–	–	k?
mazací	mazací	k2eAgInSc4d1
olej	olej	k1gInSc4
C	C	kA
–	–	k?
petrolej	petrolej	k1gInSc1
D	D	kA
–	–	k?
benzín	benzín	k1gInSc1
E	E	kA
–	–	k?
plynné	plynný	k2eAgInPc4d1
uhlovodíky	uhlovodík	k1gInPc4
F	F	kA
–	–	k?
mazut	mazut	k1gInSc1
G	G	kA
–	–	k?
těžký	těžký	k2eAgInSc4d1
topný	topný	k2eAgInSc4d1
olej	olej	k1gInSc4
H	H	kA
–	–	k?
lehký	lehký	k2eAgInSc4d1
topný	topný	k2eAgInSc4d1
olej	olej	k1gInSc4
I	i	k9
–	–	k?
nafta	nafta	k1gFnSc1
J	J	kA
–	–	k?
propan-butan	propan-butan	k1gInSc1
K	K	kA
–	–	k?
rafinační	rafinační	k2eAgInSc4d1
plyn	plyn	k1gInSc4
</s>
<s>
Následuje	následovat	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc1d1
destilace	destilace	k1gFnSc1
(	(	kIx(
<g/>
atmosférická	atmosférický	k2eAgFnSc1d1
rektifikace	rektifikace	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
kolonách	kolona	k1gFnPc6
až	až	k9
50	#num#	k4
m	m	kA
vysokých	vysoká	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
teplota	teplota	k1gFnSc1
směrem	směr	k1gInSc7
vzhůru	vzhůru	k6eAd1
klesá	klesat	k5eAaImIp3nS
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
lze	lze	k6eAd1
odebírat	odebírat	k5eAaImF
jednotlivé	jednotlivý	k2eAgFnPc4d1
frakce	frakce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ropa	ropa	k1gFnSc1
se	se	k3xPyFc4
s	s	k7c7
využitím	využití	k1gNnSc7
odpadního	odpadní	k2eAgNnSc2d1
tepla	teplo	k1gNnSc2
zahřívá	zahřívat	k5eAaImIp3nS
na	na	k7c4
asi	asi	k9
400	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
za	za	k7c2
atmosférického	atmosférický	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
vhání	vhánět	k5eAaImIp3nS
do	do	k7c2
dolního	dolní	k2eAgInSc2d1
konce	konec	k1gInSc2
kolony	kolona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
zahřát	zahřát	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
lehčí	lehčit	k5eAaImIp3nP
frakce	frakce	k1gFnPc4
nemohly	moct	k5eNaImAgFnP
kondenzovat	kondenzovat	k5eAaImF
a	a	k8xC
tak	tak	k6eAd1
stoupají	stoupat	k5eAaImIp3nP
vzhůru	vzhůru	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
horním	horní	k2eAgInSc6d1
konci	konec	k1gInSc6
se	se	k3xPyFc4
odebírají	odebírat	k5eAaImIp3nP
plyny	plyn	k1gInPc1
<g/>
,	,	kIx,
o	o	k7c4
něco	něco	k3yInSc4
níže	nízce	k6eAd2
lehký	lehký	k2eAgInSc1d1
benzin	benzin	k1gInSc1
atd.	atd.	kA
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
na	na	k7c6
dně	dno	k1gNnSc6
zůstávají	zůstávat	k5eAaImIp3nP
nejtěžší	těžký	k2eAgFnPc4d3
složky	složka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
ProduktPodíl	ProduktPodíl	k1gMnSc1
</s>
<s>
propan	propan	k1gInSc1
<g/>
,	,	kIx,
butan	butan	k1gInSc1
aj.	aj.	kA
</s>
<s>
~	~	kIx~
<g/>
3	#num#	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
surový	surový	k2eAgInSc1d1
benzin	benzin	k1gInSc1
(	(	kIx(
<g/>
crude	crude	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
~	~	kIx~
<g/>
9	#num#	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
motorový	motorový	k2eAgInSc1d1
benzín	benzín	k1gInSc1
</s>
<s>
~	~	kIx~
<g/>
24	#num#	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
Kerosin	kerosin	k1gInSc1
pro	pro	k7c4
tryskové	tryskový	k2eAgInPc4d1
motory	motor	k1gInPc4
</s>
<s>
~	~	kIx~
<g/>
4	#num#	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
motorová	motorový	k2eAgFnSc1d1
nafta	nafta	k1gFnSc1
<g/>
,	,	kIx,
lehký	lehký	k2eAgInSc1d1
topný	topný	k2eAgInSc1d1
olej	olej	k1gInSc1
</s>
<s>
<	<	kIx(
<g/>
21	#num#	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
těžký	těžký	k2eAgInSc1d1
topný	topný	k2eAgInSc1d1
olej	olej	k1gInSc1
</s>
<s>
~	~	kIx~
<g/>
11	#num#	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
asfalt	asfalt	k1gInSc1
<g/>
,	,	kIx,
těžký	těžký	k2eAgInSc1d1
topný	topný	k2eAgInSc1d1
olej	olej	k1gInSc1
</s>
<s>
~	~	kIx~
<g/>
3,5	3,5	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
mazadla	mazadlo	k1gNnPc1
</s>
<s>
~	~	kIx~
<g/>
1,5	1,5	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
ostatní	ostatní	k2eAgFnPc1d1
<g/>
,	,	kIx,
ztráty	ztráta	k1gFnPc1
atd.	atd.	kA
</s>
<s>
~	~	kIx~
<g/>
2	#num#	k4
%	%	kIx~
<g/>
,000	,000	k4
</s>
<s>
Ty	ten	k3xDgInPc1
se	se	k3xPyFc4
odvádějí	odvádět	k5eAaImIp3nP
do	do	k7c2
vakuové	vakuový	k2eAgFnSc2d1
kolony	kolona	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zahřívají	zahřívat	k5eAaImIp3nP
na	na	k7c4
ještě	ještě	k6eAd1
vyšší	vysoký	k2eAgFnSc4d2
teplotu	teplota	k1gFnSc4
a	a	k8xC
opět	opět	k6eAd1
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
frakce	frakce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
destilace	destilace	k1gFnSc1
musí	muset	k5eAaImIp3nS
probíhat	probíhat	k5eAaImF
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
molekuly	molekula	k1gFnPc1
těžkých	těžký	k2eAgInPc2d1
olejů	olej	k1gInPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
při	při	k7c6
vyšších	vysoký	k2eAgFnPc6d2
teplotách	teplota	k1gFnPc6
na	na	k7c6
vzduchu	vzduch	k1gInSc6
štěpily	štěpit	k5eAaImAgFnP
(	(	kIx(
<g/>
krakovaly	krakovat	k5eAaImAgInP
<g/>
)	)	kIx)
a	a	k8xC
neoddělovaly	oddělovat	k5eNaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
frakcí	frakce	k1gFnPc2
vakuové	vakuový	k2eAgFnSc2d1
destilace	destilace	k1gFnSc2
se	se	k3xPyFc4
potom	potom	k6eAd1
krakuje	krakovat	k5eAaImIp3nS
na	na	k7c4
lehčí	lehký	k2eAgFnPc4d2
frakce	frakce	k1gFnPc4
a	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
termickým	termický	k2eAgInSc7d1
<g/>
,	,	kIx,
katalytickým	katalytický	k2eAgInSc7d1
nebo	nebo	k8xC
vodíkovým	vodíkový	k2eAgInSc7d1
procesem	proces	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dalším	další	k2eAgMnSc6d1
se	se	k3xPyFc4
získané	získaný	k2eAgInPc1d1
produkty	produkt	k1gInPc1
zbavují	zbavovat	k5eAaImIp3nP
síry	síra	k1gFnPc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
při	při	k7c6
dalším	další	k2eAgNnSc6d1
zpracování	zpracování	k1gNnSc6
ničila	ničit	k5eAaImAgFnS
užívané	užívaný	k2eAgInPc4d1
katalyzátory	katalyzátor	k1gInPc4
a	a	k8xC
při	při	k7c6
spalování	spalování	k1gNnSc6
znečišťovala	znečišťovat	k5eAaImAgFnS
ovzduší	ovzduší	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
k	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
proces	proces	k1gInSc4
zvaný	zvaný	k2eAgInSc4d1
hydrogenační	hydrogenační	k2eAgFnSc4d1
rafinace	rafinace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
odsíření	odsíření	k1gNnSc6
se	se	k3xPyFc4
smíchají	smíchat	k5eAaPmIp3nP
s	s	k7c7
vodíkem	vodík	k1gInSc7
a	a	k8xC
zahřejí	zahřát	k5eAaPmIp3nP
na	na	k7c4
asi	asi	k9
350	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
kovových	kovový	k2eAgInPc2d1
katalyzátorů	katalyzátor	k1gInPc2
vodík	vodík	k1gInSc1
reaguje	reagovat	k5eAaBmIp3nS
se	s	k7c7
sloučeninami	sloučenina	k1gFnPc7
síry	síra	k1gFnSc2
<g/>
,	,	kIx,
dusíku	dusík	k1gInSc2
a	a	k8xC
kyslíku	kyslík	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
vzniká	vznikat	k5eAaImIp3nS
sirovodík	sirovodík	k1gInSc1
<g/>
,	,	kIx,
čpavek	čpavek	k1gInSc1
a	a	k8xC
voda	voda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgInPc6d1
krocích	krok	k1gInPc6
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
například	například	k6eAd1
zvyšovat	zvyšovat	k5eAaImF
oktanové	oktanový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
katalytický	katalytický	k2eAgInSc1d1
reforming	reforming	k1gInSc1
<g/>
,	,	kIx,
isomerizace	isomerizace	k1gFnSc1
<g/>
,	,	kIx,
alkylování	alkylování	k1gNnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
přidávat	přidávat	k5eAaImF
různá	různý	k2eAgNnPc4d1
aditiva	aditivum	k1gNnPc4
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
krakování	krakování	k1gNnSc6
těžkých	těžký	k2eAgFnPc2d1
složek	složka	k1gFnPc2
vzniká	vznikat	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
sirovodíku	sirovodík	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
smíchá	smíchat	k5eAaPmIp3nS
se	s	k7c7
vzduchem	vzduch	k1gInSc7
a	a	k8xC
v	v	k7c6
reaktoru	reaktor	k1gInSc6
proměňuje	proměňovat	k5eAaImIp3nS
na	na	k7c4
síru	síra	k1gFnSc4
a	a	k8xC
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síra	síra	k1gFnSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
dále	daleko	k6eAd2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c6
chemickém	chemický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ropné	ropný	k2eAgFnPc1d1
rafinerie	rafinerie	k1gFnPc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Mapka	mapka	k1gFnSc1
ropovodů	ropovod	k1gInPc2
a	a	k8xC
míst	místo	k1gNnPc2
rafinérií	rafinérie	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
provozuje	provozovat	k5eAaImIp3nS
ropné	ropný	k2eAgFnSc2d1
rafinerie	rafinerie	k1gFnSc2
společnost	společnost	k1gFnSc1
UNIPETROL	UNIPETROL	kA
se	se	k3xPyFc4
svými	svůj	k3xOyFgInPc7
provozy	provoz	k1gInPc7
v	v	k7c6
Litvínově	Litvínov	k1gInSc6
a	a	k8xC
v	v	k7c6
Kralupech	Kralupy	k1gInPc6
n.	n.	k?
Vltavou	Vltava	k1gFnSc7
a	a	k8xC
firma	firma	k1gFnSc1
PARAMO	PARAMO	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc1d1
rafinerie	rafinerie	k1gFnPc1
jsou	být	k5eAaImIp3nP
závislé	závislý	k2eAgFnPc1d1
na	na	k7c6
dodávkách	dodávka	k1gFnPc6
ropy	ropa	k1gFnSc2
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
ropovody	ropovod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rafinerie	rafinerie	k1gFnSc1
v	v	k7c6
Kralupech	Kralupy	k1gInPc6
odebírá	odebírat	k5eAaImIp3nS
ropu	ropa	k1gFnSc4
z	z	k7c2
ropovodu	ropovod	k1gInSc2
IKL	IKL	kA
(	(	kIx(
<g/>
Ingolstadt	Ingolstadt	k1gInSc1
–	–	k?
Kralupy	Kralupy	k1gInPc1
–	–	k?
Litvínov	Litvínov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
rafinerie	rafinerie	k1gFnPc1
jsou	být	k5eAaImIp3nP
napojeny	napojit	k5eAaPmNgFnP
na	na	k7c4
ropovod	ropovod	k1gInSc4
Družba	družba	k1gMnSc1
z	z	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Paramo	Parama	k1gFnSc5
<g/>
.	.	kIx.
www.paramo.cz	www.paramo.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Benzín	benzín	k1gInSc1
</s>
<s>
Biorafinerie	Biorafinerie	k1gFnSc1
</s>
<s>
UNIPETROL	UNIPETROL	kA
</s>
<s>
PARAMO	PARAMO	kA
</s>
<s>
Ropa	ropa	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
ropná	ropný	k2eAgFnSc1d1
rafinerie	rafinerie	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
rafinérie	rafinérie	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Základní	základní	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
ropy	ropa	k1gFnSc2
(	(	kIx(
<g/>
odsolování	odsolování	k1gNnSc1
<g/>
,	,	kIx,
atmosférická	atmosférický	k2eAgFnSc1d1
destilace	destilace	k1gFnSc1
<g/>
,	,	kIx,
vakuová	vakuový	k2eAgFnSc1d1
destilace	destilace	k1gFnSc1
<g/>
,	,	kIx,
petrochemická	petrochemický	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
pohonných	pohonný	k2eAgFnPc2d1
hmot	hmota	k1gFnPc2
</s>
<s>
Význam	význam	k1gInSc1
štěpných	štěpný	k2eAgInPc2d1
procesů	proces	k1gInPc2
(	(	kIx(
<g/>
krakování	krakování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
