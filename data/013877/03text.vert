<s>
Videotrénink	Videotrénink	k1gInSc1
interakcí	interakce	k1gFnPc2
</s>
<s>
Videotrénink	Videotrénink	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
nástrojem	nástroj	k1gInSc7
zlepšení	zlepšení	k1gNnSc2
komunikace	komunikace	k1gFnSc2
</s>
<s>
Videotrénink	Videotrénink	k1gInSc1
interakcí	interakce	k1gFnPc2
VTI	VTI	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Video	video	k1gNnSc1
Home	Home	k1gInSc1
Training	Training	k1gInSc1
VHT	VHT	kA
nebo	nebo	k8xC
Video	video	k1gNnSc1
Interaction	Interaction	k1gFnSc1
Guidance	Guidance	k1gFnSc1
VIG	VIG	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
krátkodobou	krátkodobý	k2eAgFnSc7d1
intervenční	intervenční	k2eAgFnSc7d1
a	a	k8xC
terapeutickou	terapeutický	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
práce	práce	k1gFnSc2
s	s	k7c7
videozáznamem	videozáznam	k1gInSc7
<g/>
,	,	kIx,
užívanou	užívaný	k2eAgFnSc7d1
při	při	k7c6
poruchách	porucha	k1gFnPc6
interakce	interakce	k1gFnSc2
mezi	mezi	k7c7
aktéry	aktér	k1gMnPc7
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
videotréninku	videotréninka	k1gFnSc4
je	být	k5eAaImIp3nS
rozvíjení	rozvíjení	k1gNnSc3
sociálních	sociální	k2eAgFnPc2d1
dovedností	dovednost	k1gFnPc2
<g/>
,	,	kIx,
naučení	naučení	k1gNnPc1
principům	princip	k1gInPc3
úspěšné	úspěšný	k2eAgFnSc2d1
verbální	verbální	k2eAgFnSc2d1
i	i	k8xC
neverbální	verbální	k2eNgFnSc2d1
komunikace	komunikace	k1gFnSc2
a	a	k8xC
podávání	podávání	k1gNnSc2
pozitivní	pozitivní	k2eAgFnSc2d1
zpětné	zpětný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
VTI	VTI	kA
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS
silné	silný	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
zúčastněných	zúčastněný	k2eAgMnPc2d1
spíše	spíše	k9
než	než	k8xS
problémy	problém	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
rodinném	rodinný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
při	při	k7c6
řešení	řešení	k1gNnSc6
výchovných	výchovný	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
vedení	vedení	k1gNnSc1
dětí	dítě	k1gFnPc2
a	a	k8xC
problémech	problém	k1gInPc6
v	v	k7c6
interakci	interakce	k1gFnSc6
mezi	mezi	k7c7
rodiči	rodič	k1gMnPc7
a	a	k8xC
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
klient	klient	k1gMnSc1
požádá	požádat	k5eAaPmIp3nS
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
videotrenér	videotrenér	k1gMnSc1
(	(	kIx(
<g/>
terapeut	terapeut	k1gMnSc1
<g/>
)	)	kIx)
dochází	docházet	k5eAaImIp3nS
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
do	do	k7c2
rodiny	rodina	k1gFnSc2
a	a	k8xC
natáčí	natáčet	k5eAaImIp3nS
běžnou	běžný	k2eAgFnSc4d1
interakci	interakce	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
při	při	k7c6
hře	hra	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořízený	pořízený	k2eAgInSc1d1
záznam	záznam	k1gInSc1
videotrenér	videotrenér	k1gInSc1
analyzuje	analyzovat	k5eAaImIp3nS
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
klientem	klient	k1gMnSc7
navrhne	navrhnout	k5eAaPmIp3nS
kroky	krok	k1gInPc1
vedoucí	vedoucí	k2eAgInPc1d1
k	k	k7c3
nápravě	náprava	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nějaké	nějaký	k3yIgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
natáčení	natáčení	k1gNnSc1
opět	opět	k6eAd1
opakuje	opakovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovu	znovu	k6eAd1
se	se	k3xPyFc4
vyhodnocují	vyhodnocovat	k5eAaImIp3nP
případné	případný	k2eAgInPc1d1
úspěchy	úspěch	k1gInPc1
a	a	k8xC
navrhují	navrhovat	k5eAaImIp3nP
další	další	k2eAgInPc1d1
postupy	postup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
se	se	k3xPyFc4
pokračuje	pokračovat	k5eAaImIp3nS
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dokud	dokud	k8xS
není	být	k5eNaImIp3nS
klient	klient	k1gMnSc1
s	s	k7c7
výsledkem	výsledek	k1gInSc7
spokojený	spokojený	k2eAgMnSc1d1
a	a	k8xC
dokud	dokud	k6eAd1
nejsou	být	k5eNaImIp3nP
dosaženy	dosažen	k2eAgInPc4d1
předem	předem	k6eAd1
stanovené	stanovený	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobným	podobný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
VTI	VTI	kA
využít	využít	k5eAaPmF
i	i	k9
ve	v	k7c6
školním	školní	k2eAgInSc6d1
a	a	k8xC
firemním	firemní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
vzniku	vznik	k1gInSc2
VTI	VTI	kA
</s>
<s>
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
videotrénink	videotrénink	k1gInSc1
použit	použít	k5eAaPmNgInS
v	v	k7c6
klinické	klinický	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
v	v	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspirací	inspirace	k1gFnPc2
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
byla	být	k5eAaImAgFnS
práce	práce	k1gFnSc1
etologů	etolog	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
kameru	kamera	k1gFnSc4
již	již	k6eAd1
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
využívali	využívat	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
,	,	kIx,
pro	pro	k7c4
záznam	záznam	k1gInSc4
chování	chování	k1gNnSc2
zvířat	zvíře	k1gNnPc2
v	v	k7c6
přirozeném	přirozený	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
<g/>
,	,	kIx,
zakladatelé	zakladatel	k1gMnPc1
videotréninku	videotréninka	k1gFnSc4
interakcí	interakce	k1gFnPc2
Maria	Maria	k1gFnSc1
Aarts	Aarts	k1gInSc1
a	a	k8xC
Harrie	Harrie	k1gFnSc1
Biemans	Biemansa	k1gFnPc2
<g/>
,	,	kIx,
využili	využít	k5eAaPmAgMnP
kameru	kamera	k1gFnSc4
v	v	k7c6
běžných	běžný	k2eAgFnPc6d1
<g/>
,	,	kIx,
neproblémových	problémový	k2eNgFnPc6d1
situacích	situace	k1gFnPc6
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zhlédnutí	zhlédnutí	k1gNnSc6
záznamu	záznam	k1gInSc2
účastníci	účastník	k1gMnPc1
poznávali	poznávat	k5eAaImAgMnP
dobré	dobrý	k2eAgInPc4d1
sociální	sociální	k2eAgInPc4d1
návyky	návyk	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
tak	tak	k6eAd1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
terapeutem	terapeut	k1gMnSc7
dále	daleko	k6eAd2
posilovány	posilovat	k5eAaImNgInP
a	a	k8xC
rozvíjeny	rozvíjet	k5eAaImNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Videotrénink	Videotrénink	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgInS
alternativou	alternativa	k1gFnSc7
k	k	k7c3
institucionálnímu	institucionální	k2eAgNnSc3d1
umístění	umístění	k1gNnSc3
dětí	dítě	k1gFnPc2
s	s	k7c7
poruchami	porucha	k1gFnPc7
v	v	k7c6
sociálním	sociální	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
a	a	k8xC
to	ten	k3xDgNnSc1
přímo	přímo	k6eAd1
v	v	k7c6
domácím	domácí	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
významnými	významný	k2eAgFnPc7d1
osobnostmi	osobnost	k1gFnPc7
VTI	VTI	kA
jsou	být	k5eAaImIp3nP
manželé	manžel	k1gMnPc1
Hanuš	Hanuš	k1gMnSc1
a	a	k8xC
Mechthild	Mechthild	k1gMnSc1
Papouškovi	Papoušek	k1gMnSc3
<g/>
,	,	kIx,
Colwyn	Colwyn	k1gInSc1
Trevarthen	Trevarthen	k2eAgInSc1d1
a	a	k8xC
Reuven	Reuven	k2eAgInSc1d1
Feuerstein	Feuerstein	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
VTI	VTI	kA
dostal	dostat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
nevládní	vládní	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
SPIN	spina	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jediná	jediný	k2eAgFnSc1d1
autorita	autorita	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
s	s	k7c7
oprávněním	oprávnění	k1gNnSc7
udělovat	udělovat	k5eAaImF
certifikáty	certifikát	k1gInPc1
pro	pro	k7c4
VTI	VTI	kA
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Asertivita	asertivita	k1gFnSc1
</s>
<s>
Sociální	sociální	k2eAgNnSc1d1
učení	učení	k1gNnSc1
</s>
<s>
Tvarování	tvarování	k1gNnSc1
(	(	kIx(
<g/>
psychologie	psychologie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
SPIN	spin	k1gInSc1
-	-	kIx~
nestátní	státní	k2eNgFnSc1d1
nezisková	ziskový	k2eNgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
metody	metoda	k1gFnSc2
videotrénink	videotrénink	k1gInSc1
interakcí	interakce	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
