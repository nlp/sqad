<s>
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
sumárním	sumární	k2eAgInSc7d1	sumární
vzorcem	vzorec	k1gInSc7	vzorec
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
racionálním	racionální	k2eAgInSc7d1	racionální
<g/>
)	)	kIx)	)
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
oxidan	oxidana	k1gFnPc2	oxidana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zemskou	zemský	k2eAgFnSc7d1	zemská
atmosférou	atmosféra	k1gFnSc7	atmosféra
tvoří	tvořit	k5eAaImIp3nP	tvořit
základní	základní	k2eAgFnPc1d1	základní
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
života	život	k1gInSc2	život
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
čirá	čirý	k2eAgFnSc1d1	čirá
kapalina	kapalina	k1gFnSc1	kapalina
bez	bez	k7c2	bez
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
v	v	k7c6	v
silnější	silný	k2eAgFnSc6d2	silnější
vrstvě	vrstva	k1gFnSc6	vrstva
namodralá	namodralý	k2eAgFnSc1d1	namodralá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
skupenstvích	skupenství	k1gNnPc6	skupenství
<g/>
:	:	kIx,	:
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
–	–	k?	–
led	led	k1gInSc1	led
a	a	k8xC	a
sníh	sníh	k1gInSc1	sníh
<g/>
,	,	kIx,	,
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
–	–	k?	–
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
v	v	k7c6	v
plynném	plynný	k2eAgNnSc6d1	plynné
–	–	k?	–
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
oxidan	oxidan	k1gInSc4	oxidan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
od	od	k7c2	od
názvosloví	názvosloví	k1gNnSc2	názvosloví
IUPAC	IUPAC	kA	IUPAC
93	[number]	k4	93
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
anorganický	anorganický	k2eAgInSc4d1	anorganický
jednojaderný	jednojaderný	k2eAgInSc4d1	jednojaderný
hydrid	hydrid	k1gInSc4	hydrid
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
končí	končit	k5eAaImIp3nS	končit
na	na	k7c4	na
příponu	přípona	k1gFnSc4	přípona
"	"	kIx"	"
<g/>
-an	n	k?	-an
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
IUPAC	IUPAC	kA	IUPAC
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
názvoslovných	názvoslovný	k2eAgNnPc6d1	názvoslovné
doporučeních	doporučení	k1gNnPc6	doporučení
i	i	k9	i
anglický	anglický	k2eAgInSc1d1	anglický
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
dihydrogen	dihydrogen	k1gInSc1	dihydrogen
oxide	oxid	k1gInSc5	oxid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obdobný	obdobný	k2eAgInSc1d1	obdobný
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
oxid	oxid	k1gInSc1	oxid
vodný	vodný	k2eAgInSc1d1	vodný
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
odborném	odborný	k2eAgInSc6d1	odborný
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
však	však	k9	však
používalo	používat	k5eAaImAgNnS	používat
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
triviální	triviální	k2eAgNnSc1d1	triviální
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
voda	voda	k1gFnSc1	voda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
a	a	k8xC	a
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádné	mimořádný	k2eAgFnPc1d1	mimořádná
chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
geometrie	geometrie	k1gFnSc2	geometrie
její	její	k3xOp3gFnSc2	její
molekuly	molekula	k1gFnSc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vázané	vázaný	k2eAgFnSc6d1	vázaná
nejsou	být	k5eNaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
lineárně	lineárně	k6eAd1	lineárně
(	(	kIx(	(
<g/>
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chemické	chemický	k2eAgFnPc1d1	chemická
vazby	vazba	k1gFnPc1	vazba
mezi	mezi	k7c7	mezi
atomy	atom	k1gInPc7	atom
svírají	svírat	k5eAaImIp3nP	svírat
úhel	úhel	k1gInSc4	úhel
přibližně	přibližně	k6eAd1	přibližně
105	[number]	k4	105
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Polaritě	polarita	k1gFnSc3	polarita
vazeb	vazba	k1gFnPc2	vazba
(	(	kIx(	(
<g/>
různé	různý	k2eAgFnSc6d1	různá
afinitě	afinita	k1gFnSc6	afinita
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
a	a	k8xC	a
zmíněné	zmíněný	k2eAgFnSc3d1	zmíněná
nelinearitě	nelinearita	k1gFnSc3	nelinearita
molekuly	molekula	k1gFnSc2	molekula
vděčí	vděčit	k5eAaImIp3nS	vděčit
molekula	molekula	k1gFnSc1	molekula
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
polaritu	polarita	k1gFnSc4	polarita
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
molekul	molekula	k1gFnPc2	molekula
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
rozpustnost	rozpustnost	k1gFnSc4	rozpustnost
polárních	polární	k2eAgFnPc2d1	polární
a	a	k8xC	a
iontových	iontový	k2eAgFnPc2d1	iontová
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
důvodem	důvod	k1gInSc7	důvod
vysoké	vysoký	k2eAgFnSc2d1	vysoká
elektrické	elektrický	k2eAgFnSc2d1	elektrická
permitivity	permitivita	k1gFnSc2	permitivita
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gFnSc3	jejich
schopnosti	schopnost	k1gFnSc3	schopnost
zapojovat	zapojovat	k5eAaImF	zapojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
vodíkových	vodíkový	k2eAgFnPc2d1	vodíková
vazeb	vazba	k1gFnPc2	vazba
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgInPc1d1	zvaný
též	též	k9	též
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
můstky	můstek	k1gInPc1	můstek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
důvodem	důvod	k1gInSc7	důvod
i	i	k9	i
významné	významný	k2eAgFnSc2d1	významná
hustotní	hustotní	k2eAgFnSc2d1	hustotní
anomálie	anomálie	k1gFnSc2	anomálie
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejen	nejen	k6eAd1	nejen
izotop	izotop	k1gInSc4	izotop
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
1H	[number]	k4	1H
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
16	[number]	k4	16
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
těžší	těžký	k2eAgInPc4d2	těžší
stabilní	stabilní	k2eAgInPc4d1	stabilní
izotopy	izotop	k1gInPc4	izotop
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
oceánská	oceánský	k2eAgFnSc1d1	oceánská
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
např.	např.	kA	např.
pro	pro	k7c4	pro
definici	definice	k1gFnSc4	definice
kelvinu	kelvin	k1gInSc2	kelvin
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc2d1	hlavní
jednotky	jednotka	k1gFnSc2	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
na	na	k7c4	na
1	[number]	k4	1
mol	mol	k1gInSc4	mol
vodíku	vodík	k1gInSc2	vodík
1H	[number]	k4	1H
0,15576	[number]	k4	0,15576
mmolu	mmol	k1gInSc6	mmol
deuteria	deuterium	k1gNnSc2	deuterium
2H	[number]	k4	2H
a	a	k8xC	a
na	na	k7c4	na
1	[number]	k4	1
mol	mol	k1gInSc4	mol
kyslíku	kyslík	k1gInSc2	kyslík
16O	[number]	k4	16O
0,3799	[number]	k4	0,3799
mmol	mmola	k1gFnPc2	mmola
kyslíku	kyslík	k1gInSc2	kyslík
17O	[number]	k4	17O
a	a	k8xC	a
2,005	[number]	k4	2,005
<g/>
2	[number]	k4	2
mmol	mmola	k1gFnPc2	mmola
kyslíku	kyslík	k1gInSc2	kyslík
18	[number]	k4	18
<g/>
O.	O.	kA	O.
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
voda	voda	k1gFnSc1	voda
mimo	mimo	k7c4	mimo
oceány	oceán	k1gInPc4	oceán
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
těžkých	těžký	k2eAgInPc2d1	těžký
izotopů	izotop	k1gInPc2	izotop
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
rozdílnost	rozdílnost	k1gFnSc4	rozdílnost
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
izotopicky	izotopicky	k6eAd1	izotopicky
jednotné	jednotný	k2eAgFnPc1d1	jednotná
formy	forma	k1gFnPc1	forma
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
neutronů	neutron	k1gInPc2	neutron
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
vodíku	vodík	k1gInSc2	vodík
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
lehkou	lehký	k2eAgFnSc4d1	lehká
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
vodíky	vodík	k1gInPc1	vodík
jsou	být	k5eAaImIp3nP	být
protia	protium	k1gNnPc1	protium
<g/>
,	,	kIx,	,
strukturní	strukturní	k2eAgInSc1d1	strukturní
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polotěžkou	polotěžký	k2eAgFnSc4d1	polotěžká
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
vodík	vodík	k1gInSc1	vodík
je	být	k5eAaImIp3nS	být
protium	protium	k1gNnSc4	protium
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
deuterium	deuterium	k1gNnSc1	deuterium
<g/>
,	,	kIx,	,
strukturní	strukturní	k2eAgInSc1d1	strukturní
vzorec	vzorec	k1gInSc1	vzorec
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
<g />
.	.	kIx.	.
</s>
<s>
HDO	HDO	kA	HDO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těžkou	těžký	k2eAgFnSc4d1	těžká
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
vodíky	vodík	k1gInPc1	vodík
jsou	být	k5eAaImIp3nP	být
deuteria	deuterium	k1gNnPc1	deuterium
<g/>
,	,	kIx,	,
strukturní	strukturní	k2eAgInSc1d1	strukturní
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
také	také	k9	také
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k9	jako
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
;	;	kIx,	;
voda	voda	k1gFnSc1	voda
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
těžkých	těžký	k2eAgInPc2d1	těžký
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zcela	zcela	k6eAd1	zcela
běžně	běžně	k6eAd1	běžně
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
normální	normální	k2eAgFnSc7d1	normální
vodou	voda	k1gFnSc7	voda
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
koncentraci	koncentrace	k1gFnSc6	koncentrace
<g/>
,	,	kIx,	,
těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
sloužila	sloužit	k5eAaImAgFnS	sloužit
ke	k	k7c3	k
konstrukci	konstrukce	k1gFnSc3	konstrukce
prvních	první	k4xOgInPc2	první
atomových	atomový	k2eAgMnPc2d1	atomový
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
)	)	kIx)	)
a	a	k8xC	a
tritiovou	tritiový	k2eAgFnSc4d1	tritiová
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
zvána	zván	k2eAgFnSc1d1	zvána
též	též	k9	též
supertěžká	supertěžký	k2eAgFnSc1d1	supertěžká
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
oba	dva	k4xCgInPc4	dva
vodíky	vodík	k1gInPc4	vodík
jsou	být	k5eAaImIp3nP	být
radioaktivně	radioaktivně	k6eAd1	radioaktivně
nestabilním	stabilní	k2eNgNnSc7d1	nestabilní
tritiem	tritium	k1gNnSc7	tritium
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
též	též	k9	též
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
lze	lze	k6eAd1	lze
také	také	k9	také
obohatit	obohatit	k5eAaPmF	obohatit
o	o	k7c4	o
těžší	těžký	k2eAgInPc4d2	těžší
izotopy	izotop	k1gInPc4	izotop
kyslíku	kyslík	k1gInSc2	kyslík
17O	[number]	k4	17O
a	a	k8xC	a
18	[number]	k4	18
<g/>
O.	O.	kA	O.
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
izotopicky	izotopicky	k6eAd1	izotopicky
jednotné	jednotný	k2eAgFnPc1d1	jednotná
formy	forma	k1gFnPc1	forma
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
znatelně	znatelně	k6eAd1	znatelně
liší	lišit	k5eAaImIp3nP	lišit
svými	svůj	k3xOyFgMnPc7	svůj
fyzikálními	fyzikální	k2eAgMnPc7d1	fyzikální
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
některými	některý	k3yIgFnPc7	některý
chemickými	chemický	k2eAgFnPc7d1	chemická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
odlišná	odlišný	k2eAgFnSc1d1	odlišná
hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
má	mít	k5eAaImIp3nS	mít
částečný	částečný	k2eAgInSc4d1	částečný
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
vazebnou	vazebný	k2eAgFnSc4d1	vazebná
energii	energie	k1gFnSc4	energie
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
atomovém	atomový	k2eAgInSc6d1	atomový
obalu	obal	k1gInSc6	obal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
body	bod	k1gInPc1	bod
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
a	a	k8xC	a
pH	ph	kA	ph
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
18O	[number]	k4	18O
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
hustotě	hustota	k1gFnSc3	hustota
těžké	těžký	k2eAgFnSc2d1	těžká
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
17O	[number]	k4	17O
je	být	k5eAaImIp3nS	být
hustotou	hustota	k1gFnSc7	hustota
podobná	podobný	k2eAgFnSc1d1	podobná
vodě	voda	k1gFnSc6	voda
polotěžké	polotěžký	k2eAgNnSc1d1	polotěžké
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
týkají	týkat	k5eAaImIp3nP	týkat
pouze	pouze	k6eAd1	pouze
přirozeného	přirozený	k2eAgInSc2d1	přirozený
izotopické	izotopický	k2eAgNnSc4d1	izotopické
složení	složení	k1gNnSc4	složení
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jinak	jinak	k6eAd1	jinak
<g/>
)	)	kIx)	)
a	a	k8xC	a
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
vztahovat	vztahovat	k5eAaImF	vztahovat
na	na	k7c4	na
izotopicky	izotopicky	k6eAd1	izotopicky
čisté	čistý	k2eAgFnPc4d1	čistá
formy	forma	k1gFnPc4	forma
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ne	ne	k9	ne
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
s	s	k7c7	s
těžkými	těžký	k2eAgInPc7d1	těžký
izotopy	izotop	k1gInPc7	izotop
vodíku	vodík	k1gInSc2	vodík
či	či	k8xC	či
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
hustotu	hustota	k1gFnSc4	hustota
nemá	mít	k5eNaImIp3nS	mít
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
při	při	k7c6	při
3,95	[number]	k4	3,95
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
dalším	další	k2eAgNnSc7d1	další
snižováním	snižování	k1gNnSc7	snižování
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
objem	objem	k1gInSc1	objem
jednotkové	jednotkový	k2eAgFnSc2d1	jednotková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vody	voda	k1gFnSc2	voda
zase	zase	k9	zase
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
polymerizací	polymerizace	k1gFnSc7	polymerizace
vodních	vodní	k2eAgFnPc2d1	vodní
molekul	molekula	k1gFnPc2	molekula
vodíkovými	vodíkový	k2eAgFnPc7d1	vodíková
vazbami	vazba	k1gFnPc7	vazba
a	a	k8xC	a
úhlem	úhel	k1gInSc7	úhel
mezi	mezi	k7c7	mezi
atomy	atom	k1gInPc7	atom
vodíku	vodík	k1gInSc2	vodík
–	–	k?	–
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
molekula	molekula	k1gFnSc1	molekula
v	v	k7c6	v
ledu	led	k1gInSc6	led
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
sousedy	soused	k1gMnPc4	soused
a	a	k8xC	a
v	v	k7c6	v
krystalové	krystalový	k2eAgFnSc6d1	krystalová
struktuře	struktura	k1gFnSc6	struktura
vznikají	vznikat	k5eAaImIp3nP	vznikat
prázdné	prázdný	k2eAgInPc4d1	prázdný
prostory	prostor	k1gInPc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zvláštnost	zvláštnost	k1gFnSc1	zvláštnost
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
tyto	tento	k3xDgInPc4	tento
důsledky	důsledek	k1gInPc4	důsledek
<g/>
:	:	kIx,	:
Led	led	k1gInSc1	led
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nezmrzlou	zmrzlý	k2eNgFnSc4d1	nezmrzlá
vodu	voda	k1gFnSc4	voda
izoluje	izolovat	k5eAaBmIp3nS	izolovat
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
tolik	tolik	k6eAd1	tolik
nepromrzá	promrzat	k5eNaImIp3nS	promrzat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
voda	voda	k1gFnSc1	voda
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
3,95	[number]	k4	3,95
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nP	hromadit
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
vodních	vodní	k2eAgInPc2d1	vodní
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
zvětrávání	zvětrávání	k1gNnSc4	zvětrávání
–	–	k?	–
voda	voda	k1gFnSc1	voda
zvětšující	zvětšující	k2eAgFnPc1d1	zvětšující
svůj	svůj	k3xOyFgInSc4	svůj
objem	objem	k1gInSc4	objem
"	"	kIx"	"
<g/>
trhá	trhat	k5eAaImIp3nS	trhat
<g/>
"	"	kIx"	"
horniny	hornina	k1gFnPc4	hornina
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Zvětšování	zvětšování	k1gNnSc1	zvětšování
objemu	objem	k1gInSc2	objem
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
–	–	k?	–
při	při	k7c6	při
mrznutí	mrznutí	k1gNnSc6	mrznutí
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kypření	kypření	k1gNnSc3	kypření
ornice	ornice	k1gFnSc2	ornice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
vodivost	vodivost	k1gFnSc1	vodivost
<g/>
.	.	kIx.	.
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1	měrná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
(	(	kIx(	(
<g/>
specifické	specifický	k2eAgNnSc1d1	specifické
teplo	teplo	k1gNnSc1	teplo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgNnPc1	tři
až	až	k8xS	až
desetkrát	desetkrát	k6eAd1	desetkrát
(	(	kIx(	(
<g/>
desetkrát	desetkrát	k6eAd1	desetkrát
u	u	k7c2	u
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
<s>
Proto	proto	k8xC	proto
má	můj	k3xOp1gFnSc1	můj
voda	voda	k1gFnSc1	voda
svou	svůj	k3xOyFgFnSc7	svůj
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
velký	velký	k2eAgInSc1d1	velký
klimatický	klimatický	k2eAgInSc1d1	klimatický
vliv	vliv	k1gInSc1	vliv
a	a	k8xC	a
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
transportu	transport	k1gInSc3	transport
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ústřední	ústřední	k2eAgNnSc1d1	ústřední
topení	topení	k1gNnSc1	topení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupenské	skupenský	k2eAgFnPc4d1	skupenská
přeměny	přeměna	k1gFnPc4	přeměna
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
fázový	fázový	k2eAgInSc1d1	fázový
diagram	diagram	k1gInSc1	diagram
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
zjednodušené	zjednodušený	k2eAgFnSc6d1	zjednodušená
podobě	podoba	k1gFnSc6	podoba
uveden	uvést	k5eAaPmNgMnS	uvést
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
existuje	existovat	k5eAaImIp3nS	existovat
pevné	pevný	k2eAgNnSc4d1	pevné
skupenství	skupenství	k1gNnSc4	skupenství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
<g/>
,	,	kIx,	,
lišících	lišící	k2eAgFnPc6d1	lišící
se	s	k7c7	s
krystalovou	krystalový	k2eAgFnSc7d1	krystalová
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Led	led	k1gInSc1	led
<g/>
#	#	kIx~	#
<g/>
Exotické	exotický	k2eAgFnSc2d1	exotická
fáze	fáze	k1gFnSc2	fáze
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
fázovém	fázový	k2eAgInSc6d1	fázový
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
kapalným	kapalný	k2eAgNnSc7d1	kapalné
a	a	k8xC	a
pevným	pevný	k2eAgNnSc7d1	pevné
skupenstvím	skupenství	k1gNnSc7	skupenství
podrobněji	podrobně	k6eAd2	podrobně
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
odstavec	odstavec	k1gInSc1	odstavec
hustota	hustota	k1gFnSc1	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zmrznutí	zmrznutí	k1gNnSc4	zmrznutí
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
existovala	existovat	k5eAaImAgNnP	existovat
krystalizační	krystalizační	k2eAgNnPc1d1	krystalizační
centra	centrum	k1gNnPc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
čistá	čistý	k2eAgFnSc1d1	čistá
a	a	k8xC	a
ustálená	ustálený	k2eAgFnSc1d1	ustálená
voda	voda	k1gFnSc1	voda
byla	být	k5eAaImAgFnS	být
podchlazena	podchladit	k5eAaPmNgFnS	podchladit
i	i	k9	i
pod	pod	k7c4	pod
teplotu	teplota	k1gFnSc4	teplota
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nS	aby
horká	horký	k2eAgFnSc1d1	horká
voda	voda	k1gFnSc1	voda
zmrzla	zmrznout	k5eAaPmAgFnS	zmrznout
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
ustálená	ustálený	k2eAgFnSc1d1	ustálená
voda	voda	k1gFnSc1	voda
studená	studený	k2eAgFnSc1d1	studená
(	(	kIx(	(
<g/>
Mpembův	Mpembův	k2eAgInSc1d1	Mpembův
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
náhlému	náhlý	k2eAgNnSc3d1	náhlé
zmrznutí	zmrznutí	k1gNnSc3	zmrznutí
podchlazené	podchlazený	k2eAgFnSc2d1	podchlazená
vody	voda	k1gFnSc2	voda
stačí	stačit	k5eAaBmIp3nS	stačit
i	i	k9	i
mechanický	mechanický	k2eAgInSc1d1	mechanický
podnět	podnět	k1gInSc1	podnět
(	(	kIx(	(
<g/>
zatřesení	zatřesení	k1gNnSc1	zatřesení
<g/>
,	,	kIx,	,
vhození	vhození	k1gNnSc1	vhození
tělíska	tělísko	k1gNnSc2	tělísko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Specifická	specifický	k2eAgNnPc1d1	specifické
skupenská	skupenský	k2eAgNnPc1d1	skupenské
tepla	teplo	k1gNnPc1	teplo
(	(	kIx(	(
<g/>
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
)	)	kIx)	)
–	–	k?	–
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
parametru	parametr	k1gInSc6	parametr
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
naprosto	naprosto	k6eAd1	naprosto
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
výparné	výparný	k2eAgNnSc1d1	výparné
teplo	teplo	k1gNnSc1	teplo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
efektivní	efektivní	k2eAgNnSc4d1	efektivní
ochlazování	ochlazování	k1gNnSc4	ochlazování
teplokrevných	teplokrevný	k2eAgMnPc2d1	teplokrevný
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
–	–	k?	–
bez	bez	k7c2	bez
pocení	pocení	k1gNnSc2	pocení
by	by	kYmCp3nP	by
nepřežili	přežít	k5eNaPmAgMnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
varu	var	k1gInSc2	var
–	–	k?	–
Obecný	obecný	k2eAgInSc1d1	obecný
trend	trend	k1gInSc1	trend
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
hmotností	hmotnost	k1gFnSc7	hmotnost
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
můstky	můstek	k1gInPc1	můstek
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInSc4d2	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
varu	var	k1gInSc2	var
než	než	k8xS	než
hmotnost	hmotnost	k1gFnSc4	hmotnost
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
periodě	perioda	k1gFnSc6	perioda
–	–	k?	–
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
NH3	NH3	k1gFnSc1	NH3
a	a	k8xC	a
HF	HF	kA	HF
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
výjimkou	výjimka	k1gFnSc7	výjimka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
trendu	trend	k1gInSc6	trend
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
vysokou	vysoký	k2eAgFnSc7d1	vysoká
permitivitou	permitivita	k1gFnSc7	permitivita
(	(	kIx(	(
<g/>
relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
elektricky	elektricky	k6eAd1	elektricky
vodivá	vodivý	k2eAgFnSc1d1	vodivá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
příměsí	příměs	k1gFnPc2	příměs
výrazně	výrazně	k6eAd1	výrazně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
její	její	k3xOp3gFnSc4	její
vodivost	vodivost	k1gFnSc4	vodivost
<g/>
,	,	kIx,	,
až	až	k9	až
řádově	řádově	k6eAd1	řádově
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
setkáváme	setkávat	k5eAaImIp1nP	setkávat
spíše	spíše	k9	spíše
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
ne	ne	k9	ne
úplně	úplně	k6eAd1	úplně
destilovanou	destilovaný	k2eAgFnSc4d1	destilovaná
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
tvrzení	tvrzení	k1gNnSc1	tvrzení
obrátit	obrátit	k5eAaPmF	obrátit
<g/>
:	:	kIx,	:
s	s	k7c7	s
poklesem	pokles	k1gInSc7	pokles
koncentrace	koncentrace	k1gFnSc2	koncentrace
iontů	ion	k1gInPc2	ion
příměsí	příměs	k1gFnPc2	příměs
vodivost	vodivost	k1gFnSc1	vodivost
vody	voda	k1gFnSc2	voda
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
úplným	úplný	k2eAgNnSc7d1	úplné
odstraněním	odstranění	k1gNnSc7	odstranění
dokonce	dokonce	k9	dokonce
prudce	prudko	k6eAd1	prudko
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
extrémní	extrémní	k2eAgFnSc1d1	extrémní
situace	situace	k1gFnSc1	situace
<g/>
:	:	kIx,	:
I	i	k9	i
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
přes	přes	k7c4	přes
její	její	k3xOp3gFnSc4	její
velkou	velký	k2eAgFnSc4d1	velká
schopnost	schopnost	k1gFnSc4	schopnost
polarizace	polarizace	k1gFnSc2	polarizace
do	do	k7c2	do
kondenzátorů	kondenzátor	k1gInPc2	kondenzátor
nepoužíváme	používat	k5eNaImIp1nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
dávána	dávat	k5eAaImNgFnS	dávat
za	za	k7c4	za
příklad	příklad	k1gInSc4	příklad
nestlačitelné	stlačitelný	k2eNgFnSc2d1	nestlačitelná
kapaliny	kapalina	k1gFnSc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
skutečná	skutečný	k2eAgFnSc1d1	skutečná
stlačitelnost	stlačitelnost	k1gFnSc1	stlačitelnost
sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
zanedbává	zanedbávat	k5eAaImIp3nS	zanedbávat
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
prudkým	prudký	k2eAgNnSc7d1	prudké
až	až	k8xS	až
explozivním	explozivní	k2eAgNnSc7d1	explozivní
slučováním	slučování	k1gNnSc7	slučování
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
(	(	kIx(	(
<g/>
hořením	hoření	k2eAgInSc7d1	hoření
bezbarvým	bezbarvý	k2eAgInSc7d1	bezbarvý
plamenem	plamen	k1gInSc7	plamen
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
2H2	[number]	k4	2H2
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
→	→	k?	→
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
za	za	k7c2	za
vývinu	vývin	k1gInSc2	vývin
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
exotermní	exotermní	k2eAgFnPc1d1	exotermní
reakce	reakce	k1gFnPc1	reakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
vedle	vedle	k7c2	vedle
solí	sůl	k1gFnPc2	sůl
při	při	k7c6	při
neutralizaci	neutralizace	k1gFnSc6	neutralizace
kyselin	kyselina	k1gFnPc2	kyselina
zásadami	zásada	k1gFnPc7	zásada
<g/>
,	,	kIx,	,
např.	např.	kA	např.
HCl	HCl	k1gFnSc1	HCl
+	+	kIx~	+
NaOH	NaOH	k1gMnSc1	NaOH
→	→	k?	→
H2O	H2O	k1gMnSc1	H2O
+	+	kIx~	+
NaCl	NaCl	k1gMnSc1	NaCl
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
ve	v	k7c6	v
spalných	spalný	k2eAgInPc6d1	spalný
plynech	plyn	k1gInPc6	plyn
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
většiny	většina	k1gFnSc2	většina
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
methanu	methan	k1gInSc2	methan
CH4	CH4	k1gFnSc2	CH4
+	+	kIx~	+
2O2	[number]	k4	2O2
→	→	k?	→
2H2O	[number]	k4	2H2O
+	+	kIx~	+
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
oktanu	oktan	k1gInSc2	oktan
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnPc1d1	hlavní
složky	složka	k1gFnPc1	složka
benzínu	benzín	k1gInSc2	benzín
<g/>
)	)	kIx)	)
2C8H18	[number]	k4	2C8H18
+	+	kIx~	+
25O2	[number]	k4	25O2
→	→	k?	→
18H2O	[number]	k4	18H2O
+	+	kIx~	+
16	[number]	k4	16
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vodné	vodný	k2eAgInPc1d1	vodný
roztoky	roztok	k1gInPc1	roztok
mohou	moct	k5eAaImIp3nP	moct
vykazovat	vykazovat	k5eAaImF	vykazovat
kyselou	kyselá	k1gFnSc4	kyselá
<g/>
,	,	kIx,	,
neutrální	neutrální	k2eAgFnSc4d1	neutrální
nebo	nebo	k8xC	nebo
zásaditou	zásaditý	k2eAgFnSc4d1	zásaditá
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Kyselost	kyselost	k1gFnSc1	kyselost
(	(	kIx(	(
<g/>
acidita	acidita	k1gFnSc1	acidita
<g/>
)	)	kIx)	)
a	a	k8xC	a
zásaditost	zásaditost	k1gFnSc1	zásaditost
(	(	kIx(	(
<g/>
bazicita	bazicita	k1gFnSc1	bazicita
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
ve	v	k7c6	v
stupnici	stupnice	k1gFnSc6	stupnice
hodnot	hodnota	k1gFnPc2	hodnota
pH	ph	kA	ph
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
stupnice	stupnice	k1gFnSc2	stupnice
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
14	[number]	k4	14
pH	ph	kA	ph
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hodnotě	hodnota	k1gFnSc3	hodnota
pH	ph	kA	ph
7	[number]	k4	7
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
roztok	roztok	k1gInSc1	roztok
neutrální	neutrální	k2eAgInPc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
nižší	nízký	k2eAgFnPc1d2	nižší
označují	označovat	k5eAaImIp3nP	označovat
roztok	roztok	k1gInSc1	roztok
kyselý	kyselý	k2eAgInSc1d1	kyselý
<g/>
,	,	kIx,	,
hodnoty	hodnota	k1gFnPc4	hodnota
vyšší	vysoký	k2eAgInSc4d2	vyšší
zásaditý	zásaditý	k2eAgInSc4d1	zásaditý
čili	čili	k8xC	čili
alkalický	alkalický	k2eAgInSc4d1	alkalický
<g/>
.	.	kIx.	.
</s>
<s>
Vody	voda	k1gFnPc1	voda
kyselé	kyselá	k1gFnSc2	kyselá
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
bez	bez	k7c2	bez
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
plankton	plankton	k1gInSc1	plankton
ani	ani	k8xC	ani
baktérie	baktérie	k1gFnPc1	baktérie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
tvrdost	tvrdost	k1gFnSc1	tvrdost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Veličina	veličina	k1gFnSc1	veličina
nejčastěji	často	k6eAd3	často
udávající	udávající	k2eAgFnSc6d1	udávající
koncentraci	koncentrace	k1gFnSc6	koncentrace
kationtů	kation	k1gInPc2	kation
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
hořčíku	hořčík	k1gInSc2	hořčík
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
však	však	k9	však
nejednotná	jednotný	k2eNgFnSc1d1	nejednotná
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označuje	označovat	k5eAaImIp3nS	označovat
koncentrace	koncentrace	k1gFnSc1	koncentrace
dvojmocných	dvojmocný	k2eAgInPc2d1	dvojmocný
kationtů	kation	k1gInPc2	kation
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
stroncia	stroncium	k1gNnSc2	stroncium
a	a	k8xC	a
barya	baryum	k1gNnSc2	baryum
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
všech	všecek	k3xTgInPc2	všecek
kationtů	kation	k1gInPc2	kation
s	s	k7c7	s
nábojem	náboj	k1gInSc7	náboj
větším	veliký	k2eAgInSc7d2	veliký
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
nejednotnosti	nejednotnost	k1gFnSc3	nejednotnost
se	se	k3xPyFc4	se
moderní	moderní	k2eAgFnSc1d1	moderní
hydrochemie	hydrochemie	k1gFnSc1	hydrochemie
termínu	termín	k1gInSc2	termín
tvrdost	tvrdost	k1gFnSc1	tvrdost
vody	voda	k1gFnSc2	voda
snaží	snažit	k5eAaImIp3nS	snažit
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
mnoha	mnoho	k4c2	mnoho
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
akvaristiky	akvaristika	k1gFnSc2	akvaristika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
pojem	pojem	k1gInSc4	pojem
tvrdost	tvrdost	k1gFnSc4	tvrdost
vody	voda	k1gFnSc2	voda
stále	stále	k6eAd1	stále
často	často	k6eAd1	často
užívá	užívat	k5eAaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Koloběh	koloběh	k1gInSc1	koloběh
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířením	rozšíření	k1gNnSc7	rozšíření
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
pohybem	pohyb	k1gInSc7	pohyb
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
hydrologie	hydrologie	k1gFnSc1	hydrologie
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
též	též	k9	též
meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
planetě	planeta	k1gFnSc6	planeta
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
planetách	planeta	k1gFnPc6	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
vypadá	vypadat	k5eAaPmIp3nS	vypadat
Země	země	k1gFnSc1	země
jako	jako	k8xC	jako
modrobílá	modrobílý	k2eAgFnSc1d1	modrobílá
planeta	planeta	k1gFnSc1	planeta
<g/>
:	:	kIx,	:
bílá	bílý	k2eAgFnSc1d1	bílá
od	od	k7c2	od
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
a	a	k8xC	a
modrá	modrat	k5eAaImIp3nS	modrat
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zákonitě	zákonitě	k6eAd1	zákonitě
všechny	všechen	k3xTgFnPc4	všechen
formy	forma	k1gFnPc4	forma
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
tak	tak	k9	tak
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
<g/>
)	)	kIx)	)
závisejí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc4	část
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
nazýváme	nazývat	k5eAaImIp1nP	nazývat
hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
71	[number]	k4	71
%	%	kIx~	%
<g/>
)	)	kIx)	)
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
tvoří	tvořit	k5eAaImIp3nS	tvořit
97	[number]	k4	97
%	%	kIx~	%
celého	celý	k2eAgNnSc2d1	celé
vodstva	vodstvo	k1gNnSc2	vodstvo
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
průměrně	průměrně	k6eAd1	průměrně
35	[number]	k4	35
g	g	kA	g
solí	solit	k5eAaImIp3nS	solit
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
77,8	[number]	k4	77,8
%	%	kIx~	%
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
NaCl	NaClum	k1gNnPc2	NaClum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10,9	[number]	k4	10,9
%	%	kIx~	%
chloridu	chlorid	k1gInSc2	chlorid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
(	(	kIx(	(
<g/>
MgCl	MgCl	k1gInSc1	MgCl
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
soli	sůl	k1gFnPc4	sůl
jako	jako	k8xS	jako
síran	síran	k1gInSc4	síran
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
<g/>
,	,	kIx,	,
síran	síran	k1gInSc1	síran
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
síran	síran	k1gInSc1	síran
draselný	draselný	k2eAgInSc1d1	draselný
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Sladká	sladký	k2eAgFnSc1d1	sladká
voda	voda	k1gFnSc1	voda
tvoří	tvořit	k5eAaImIp3nS	tvořit
jen	jen	k9	jen
nepatrnou	nepatrný	k2eAgFnSc4d1	nepatrná
část	část	k1gFnSc4	část
hydrosféry	hydrosféra	k1gFnSc2	hydrosféra
–	–	k?	–
3	[number]	k4	3
%	%	kIx~	%
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
69	[number]	k4	69
%	%	kIx~	%
této	tento	k3xDgFnSc2	tento
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ledovcích	ledovec	k1gInPc6	ledovec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
30	[number]	k4	30
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
podzemní	podzemní	k2eAgFnSc1d1	podzemní
a	a	k8xC	a
jen	jen	k9	jen
necelé	celý	k2eNgNnSc4d1	necelé
procento	procento	k1gNnSc4	procento
tvoří	tvořit	k5eAaImIp3nS	tvořit
voda	voda	k1gFnSc1	voda
povrchová	povrchový	k2eAgFnSc1d1	povrchová
a	a	k8xC	a
atmosférická	atmosférický	k2eAgFnSc1d1	atmosférická
<g/>
.	.	kIx.	.
</s>
<s>
Koloběh	koloběh	k1gInSc1	koloběh
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
kontinentech	kontinent	k1gInPc6	kontinent
začíná	začínat	k5eAaImIp3nS	začínat
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dopadnou	dopadnout	k5eAaPmIp3nP	dopadnout
z	z	k7c2	z
mraků	mrak	k1gInPc2	mrak
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
putovat	putovat	k5eAaImF	putovat
třemi	tři	k4xCgFnPc7	tři
cestami	cesta	k1gFnPc7	cesta
<g/>
:	:	kIx,	:
zpravidla	zpravidla	k6eAd1	zpravidla
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
100	[number]	k4	100
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vypaří	vypařit	k5eAaPmIp3nS	vypařit
méně	málo	k6eAd2	málo
než	než	k8xS	než
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
10	[number]	k4	10
%	%	kIx~	%
–	–	k?	–
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
steče	stéct	k5eAaPmIp3nS	stéct
do	do	k7c2	do
potoků	potok	k1gInPc2	potok
<g/>
,	,	kIx,	,
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
10	[number]	k4	10
%	%	kIx~	%
a	a	k8xC	a
méně	málo	k6eAd2	málo
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
také	také	k9	také
nic	nic	k3yNnSc1	nic
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vsáknout	vsáknout	k5eAaPmF	vsáknout
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
rozpuštěného	rozpuštěný	k2eAgInSc2d1	rozpuštěný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
maximální	maximální	k2eAgFnSc4d1	maximální
koncentraci	koncentrace	k1gFnSc4	koncentrace
14	[number]	k4	14
mg	mg	kA	mg
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c4	na
litr	litr	k1gInSc4	litr
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
teplotou	teplota	k1gFnSc7	teplota
pak	pak	k6eAd1	pak
množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
tak	tak	k9	tak
voda	voda	k1gFnSc1	voda
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
například	například	k6eAd1	například
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
9	[number]	k4	9
mg	mg	kA	mg
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c4	na
litr	litr	k1gInSc4	litr
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejčastější	častý	k2eAgFnSc1d3	nejčastější
látka	látka	k1gFnSc1	látka
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
molekulárních	molekulární	k2eAgNnPc6d1	molekulární
mračnech	mračno	k1gNnPc6	mračno
v	v	k7c6	v
mezihvězdném	mezihvězdný	k2eAgInSc6d1	mezihvězdný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
protoplanetární	protoplanetární	k2eAgFnSc1d1	protoplanetární
mlhovina	mlhovina	k1gFnSc1	mlhovina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
v	v	k7c6	v
Oortově	Oortův	k2eAgInSc6d1	Oortův
oblaku	oblak	k1gInSc6	oblak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
zřejmě	zřejmě	k6eAd1	zřejmě
ještě	ještě	k6eAd1	ještě
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
nové	nový	k2eAgFnPc4d1	nová
komety	kometa	k1gFnPc4	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Jádra	jádro	k1gNnSc2	jádro
komet	kometa	k1gFnPc2	kometa
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
desítky	desítka	k1gFnPc1	desítka
procent	procent	k1gInSc4	procent
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
právě	právě	k6eAd1	právě
komety	kometa	k1gFnPc1	kometa
zanesly	zanést	k5eAaPmAgFnP	zanést
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
většinu	většina	k1gFnSc4	většina
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
některé	některý	k3yIgInPc1	některý
měsíce	měsíc	k1gInPc1	měsíc
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
tělesa	těleso	k1gNnPc1	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
a	a	k8xC	a
transneptunická	transneptunický	k2eAgNnPc1d1	transneptunické
tělesa	těleso	k1gNnPc1	těleso
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
tvořena	tvořit	k5eAaImNgFnS	tvořit
vodou	voda	k1gFnSc7	voda
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
oceán	oceán	k1gInSc1	oceán
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
na	na	k7c6	na
Jupiterově	Jupiterův	k2eAgInSc6d1	Jupiterův
měsíci	měsíc	k1gInSc6	měsíc
Europa	Europa	k1gFnSc1	Europa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
plynném	plynný	k2eAgNnSc6d1	plynné
skupenství	skupenství	k1gNnSc6	skupenství
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
na	na	k7c6	na
extrasolární	extrasolární	k2eAgFnSc6d1	extrasolární
planetě	planeta	k1gFnSc6	planeta
–	–	k?	–
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
HD	HD	kA	HD
189733	[number]	k4	189733
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	s	k7c7	s
63	[number]	k4	63
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Lištičky	lištička	k1gFnSc2	lištička
<g/>
.	.	kIx.	.
plynná	plynný	k2eAgFnSc1d1	plynná
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
)	)	kIx)	)
Merkur	Merkur	k1gInSc1	Merkur
3,4	[number]	k4	3,4
%	%	kIx~	%
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	zem	k1gFnSc2	zem
–	–	k?	–
stopy	stopa	k1gFnSc2	stopa
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
podnebí	podnebí	k1gNnSc6	podnebí
Mars	Mars	k1gInSc1	Mars
0,03	[number]	k4	0,03
%	%	kIx~	%
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiter	Jupiter	k1gMnSc1	Jupiter
0,1	[number]	k4	0,1
%	%	kIx~	%
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Saturn	Saturn	k1gInSc1	Saturn
0,1	[number]	k4	0,1
%	%	kIx~	%
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Enceladus	Enceladus	k1gInSc4	Enceladus
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc1	Saturn
<g/>
)	)	kIx)	)
–	–	k?	–
100	[number]	k4	100
%	%	kIx~	%
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
kapalná	kapalný	k2eAgFnSc1d1	kapalná
<g/>
:	:	kIx,	:
Země	země	k1gFnSc1	země
–	–	k?	–
71	[number]	k4	71
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
Mars	Mars	k1gInSc1	Mars
–	–	k?	–
podle	podle	k7c2	podle
NASA	NASA	kA	NASA
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
kapalná	kapalný	k2eAgFnSc1d1	kapalná
voda	voda	k1gFnSc1	voda
Europa	Europa	k1gFnSc1	Europa
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
)	)	kIx)	)
–	–	k?	–
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
led	led	k1gInSc1	led
Io	Io	k1gFnSc2	Io
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
)	)	kIx)	)
–	–	k?	–
málo	málo	k1gNnSc1	málo
nebo	nebo	k8xC	nebo
žádná	žádný	k3yNgFnSc1	žádný
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
)	)	kIx)	)
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
led	led	k1gInSc1	led
<g/>
)	)	kIx)	)
Mars	Mars	k1gInSc1	Mars
–	–	k?	–
výskyt	výskyt	k1gInSc1	výskyt
<g />
.	.	kIx.	.
</s>
<s>
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
orbitální	orbitální	k2eAgFnSc1d1	orbitální
sonda	sonda	k1gFnSc1	sonda
Mars	Mars	k1gInSc1	Mars
Express	express	k1gInSc1	express
Pluto	Pluto	k1gMnSc1	Pluto
–	–	k?	–
odhad	odhad	k1gInSc1	odhad
<g/>
,	,	kIx,	,
že	že	k8xS	že
led	led	k1gInSc1	led
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
30	[number]	k4	30
%	%	kIx~	%
Pluta	plut	k2eAgFnSc1d1	Pluta
Europa	Europa	k1gFnSc1	Europa
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
)	)	kIx)	)
–	–	k?	–
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
led	led	k1gInSc1	led
Merkur	Merkur	k1gInSc1	Merkur
–	–	k?	–
výskyt	výskyt	k1gInSc4	výskyt
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
kráterech	kráter	k1gInPc6	kráter
blízko	blízko	k7c2	blízko
pólů	pól	k1gInPc2	pól
Phoebe	Phoeb	k1gInSc5	Phoeb
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
měsíc	měsíc	k1gInSc4	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gInSc1	Saturn
<g/>
)	)	kIx)	)
–	–	k?	–
předpoklad	předpoklad	k1gInSc1	předpoklad
podle	podle	k7c2	podle
hustoty	hustota	k1gFnSc2	hustota
Enceladus	Enceladus	k1gInSc1	Enceladus
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc1	měsíc
planety	planeta	k1gFnSc2	planeta
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
)	)	kIx)	)
–	–	k?	–
velmi	velmi	k6eAd1	velmi
pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
předpoklad	předpoklad	k1gInSc1	předpoklad
komety	kometa	k1gFnSc2	kometa
–	–	k?	–
předpoklad	předpoklad	k1gInSc1	předpoklad
okraje	okraj	k1gInSc2	okraj
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Oortově	Oortův	k2eAgInSc6d1	Oortův
oblaku	oblak	k1gInSc6	oblak
–	–	k?	–
předpoklad	předpoklad	k1gInSc1	předpoklad
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
<g/>
:	:	kIx,	:
Venuše	Venuše	k1gFnSc2	Venuše
"	"	kIx"	"
<g/>
Nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
živiny	živina	k1gFnPc4	živina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
organizmus	organizmus	k1gInSc4	organizmus
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozpouštědlem	rozpouštědlo	k1gNnSc7	rozpouštědlo
většiny	většina	k1gFnSc2	většina
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
regulovat	regulovat	k5eAaImF	regulovat
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
trávicí	trávicí	k2eAgInPc4d1	trávicí
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pravidelné	pravidelný	k2eAgFnSc3d1	pravidelná
výměně	výměna	k1gFnSc3	výměna
vody	voda	k1gFnSc2	voda
můžeme	moct	k5eAaImIp1nP	moct
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
vyplavovat	vyplavovat	k5eAaImF	vyplavovat
škodlivé	škodlivý	k2eAgFnPc4d1	škodlivá
látky	látka	k1gFnPc4	látka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
u	u	k7c2	u
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
usazování	usazování	k1gNnSc3	usazování
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
člověk	člověk	k1gMnSc1	člověk
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
denně	denně	k6eAd1	denně
přijmout	přijmout	k5eAaPmF	přijmout
2-3	[number]	k4	2-3
litry	litr	k1gInPc4	litr
vhodných	vhodný	k2eAgFnPc2d1	vhodná
tekutin	tekutina	k1gFnPc2	tekutina
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
včetně	včetně	k7c2	včetně
vody	voda	k1gFnSc2	voda
obsažené	obsažený	k2eAgFnSc2d1	obsažená
v	v	k7c6	v
jídle	jídlo	k1gNnSc6	jídlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
fyzické	fyzický	k2eAgFnSc6d1	fyzická
zátěži	zátěž	k1gFnSc6	zátěž
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
však	však	k9	však
přijímat	přijímat	k5eAaImF	přijímat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
nárazově	nárazově	k6eAd1	nárazově
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zatížené	zatížený	k2eAgFnPc1d1	zatížená
ledviny	ledvina	k1gFnPc1	ledvina
zvládly	zvládnout	k5eAaPmAgFnP	zvládnout
vyloučit	vyloučit	k5eAaPmF	vyloučit
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
vydrží	vydržet	k5eAaPmIp3nS	vydržet
nejdéle	dlouho	k6eAd3	dlouho
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
7-10	[number]	k4	7-10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
nedostatku	nedostatek	k1gInSc6	nedostatek
vody	voda	k1gFnSc2	voda
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
ledvinových	ledvinový	k2eAgInPc2d1	ledvinový
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
nedostatek	nedostatek	k1gInSc4	nedostatek
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
žízní	žízeň	k1gFnSc7	žízeň
<g/>
,	,	kIx,	,
nevolností	nevolnost	k1gFnSc7	nevolnost
<g/>
,	,	kIx,	,
slabostí	slabost	k1gFnSc7	slabost
a	a	k8xC	a
křečemi	křeč	k1gFnPc7	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
pitný	pitný	k2eAgInSc4d1	pitný
režim	režim	k1gInSc4	režim
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
seniorů	senior	k1gMnPc2	senior
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
náchylnější	náchylný	k2eAgFnPc1d2	náchylnější
k	k	k7c3	k
dehydrataci	dehydratace	k1gFnSc3	dehydratace
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnSc3	onemocnění
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
močových	močový	k2eAgFnPc2d1	močová
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
se	se	k3xPyFc4	se
pít	pít	k5eAaImF	pít
při	při	k7c6	při
jídle	jídlo	k1gNnSc6	jídlo
ani	ani	k8xC	ani
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nesnižovala	snižovat	k5eNaImAgFnS	snižovat
účinnost	účinnost	k1gFnSc1	účinnost
trávení	trávení	k1gNnSc2	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
ztrácíme	ztrácet	k5eAaImIp1nP	ztrácet
denně	denně	k6eAd1	denně
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
moči	moč	k1gFnSc2	moč
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocením	pocení	k1gNnSc7	pocení
<g/>
,	,	kIx,	,
plícemi	plíce	k1gFnPc7	plíce
(	(	kIx(	(
<g/>
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
se	se	k3xPyFc4	se
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
asi	asi	k9	asi
400	[number]	k4	400
ml	ml	kA	ml
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
stolicí	stolice	k1gFnSc7	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
rostliny	rostlina	k1gFnPc4	rostlina
více	hodně	k6eAd2	hodně
významů	význam	k1gInPc2	význam
než	než	k8xS	než
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
prostředím	prostředí	k1gNnSc7	prostředí
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
zvýšení	zvýšení	k1gNnSc1	zvýšení
jejího	její	k3xOp3gInSc2	její
obsahu	obsah	k1gInSc2	obsah
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
semenech	semeno	k1gNnPc6	semeno
<g/>
)	)	kIx)	)
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgInPc2	některý
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
dopravovat	dopravovat	k5eAaImF	dopravovat
látky	látka	k1gFnPc4	látka
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
(	(	kIx(	(
<g/>
transpirační	transpirační	k2eAgInSc1d1	transpirační
proud	proud	k1gInSc1	proud
a	a	k8xC	a
asimilační	asimilační	k2eAgInSc1d1	asimilační
proud	proud	k1gInSc1	proud
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
procesů	proces	k1gInPc2	proces
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
fyzikálních	fyzikální	k2eAgInPc6d1	fyzikální
a	a	k8xC	a
chemických	chemický	k2eAgInPc6d1	chemický
principech	princip	k1gInPc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zdroj	zdroj	k1gInSc1	zdroj
H	H	kA	H
<g/>
+	+	kIx~	+
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
termoregulace	termoregulace	k1gFnSc1	termoregulace
<g/>
.	.	kIx.	.
</s>
<s>
Udržuje	udržovat	k5eAaImIp3nS	udržovat
buněčné	buněčný	k2eAgNnSc1d1	buněčné
napětí	napětí	k1gNnSc1	napětí
(	(	kIx(	(
<g/>
turgor	turgor	k1gInSc1	turgor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
nahradit	nahradit	k5eAaPmF	nahradit
substrát	substrát	k1gInSc4	substrát
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
hydroponie	hydroponie	k1gFnPc4	hydroponie
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vodní	vodní	k2eAgNnSc1d1	vodní
hospodářství	hospodářství	k1gNnSc1	hospodářství
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgNnSc1d1	vodní
hospodářství	hospodářství	k1gNnSc1	hospodářství
obecně	obecně	k6eAd1	obecně
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dodávky	dodávka	k1gFnPc4	dodávka
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgInSc1d1	vodní
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
vodárna	vodárna	k1gFnSc1	vodárna
<g/>
,	,	kIx,	,
vodojem	vodojem	k1gInSc1	vodojem
<g/>
,	,	kIx,	,
vodovod	vodovod	k1gInSc1	vodovod
<g/>
,	,	kIx,	,
vodovodní	vodovodní	k2eAgFnSc1d1	vodovodní
přípojka	přípojka	k1gFnSc1	přípojka
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
domácnosti	domácnost	k1gFnPc4	domácnost
i	i	k8xC	i
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
nakládá	nakládat	k5eAaImIp3nS	nakládat
s	s	k7c7	s
odpadními	odpadní	k2eAgFnPc7d1	odpadní
vodami	voda	k1gFnPc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Provádí	provádět	k5eAaImIp3nS	provádět
odběr	odběr	k1gInSc1	odběr
<g/>
,	,	kIx,	,
transport	transport	k1gInSc1	transport
(	(	kIx(	(
<g/>
stoková	stokový	k2eAgFnSc1d1	stoková
síť	síť	k1gFnSc1	síť
neboli	neboli	k8xC	neboli
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
čištění	čištění	k1gNnSc4	čištění
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
čistírnách	čistírna	k1gFnPc6	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Předchází	předcházet	k5eAaImIp3nS	předcházet
znečištění	znečištění	k1gNnSc4	znečištění
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
úpravou	úprava	k1gFnSc7	úprava
surové	surový	k2eAgFnSc2d1	surová
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Surová	surový	k2eAgFnSc1d1	surová
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
podzemních	podzemní	k2eAgInPc2d1	podzemní
nebo	nebo	k8xC	nebo
povrchových	povrchový	k2eAgInPc2d1	povrchový
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
–	–	k?	–
zejména	zejména	k9	zejména
podpovrchových	podpovrchový	k2eAgInPc2d1	podpovrchový
–	–	k?	–
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
bez	bez	k7c2	bez
úpravy	úprava	k1gFnSc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
shromažďování	shromažďování	k1gNnSc3	shromažďování
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vody	voda	k1gFnSc2	voda
slouží	sloužit	k5eAaImIp3nS	sloužit
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
nádrž	nádrž	k1gFnSc1	nádrž
(	(	kIx(	(
<g/>
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
odběrová	odběrový	k2eAgFnSc1d1	odběrová
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
několika	několik	k4yIc7	několik
odběrovými	odběrový	k2eAgFnPc7d1	odběrová
šachtami	šachta	k1gFnPc7	šachta
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Odebírá	odebírat	k5eAaImIp3nS	odebírat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
příkazu	příkaz	k1gInSc2	příkaz
z	z	k7c2	z
úpravny	úpravna	k1gFnSc2	úpravna
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Vhodná	vhodný	k2eAgFnSc1d1	vhodná
teplota	teplota	k1gFnSc1	teplota
pro	pro	k7c4	pro
odběr	odběr	k1gInSc4	odběr
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
12	[number]	k4	12
°	°	k?	°
<g/>
C.	C.	kA	C.
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
umělé	umělý	k2eAgFnPc4d1	umělá
filtrace	filtrace	k1gFnPc4	filtrace
a	a	k8xC	a
sorpční	sorpční	k2eAgFnPc4d1	sorpční
schopnosti	schopnost	k1gFnPc4	schopnost
půdního	půdní	k2eAgInSc2d1	půdní
sedimentu	sediment	k1gInSc2	sediment
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
řasy	řasa	k1gFnPc1	řasa
často	často	k6eAd1	často
ucpávají	ucpávat	k5eAaImIp3nP	ucpávat
filtraci	filtrace	k1gFnSc4	filtrace
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
z	z	k7c2	z
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
infiltrovat	infiltrovat	k5eAaBmF	infiltrovat
z	z	k7c2	z
umělých	umělý	k2eAgFnPc2d1	umělá
nádrží	nádrž	k1gFnPc2	nádrž
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
a	a	k8xC	a
z	z	k7c2	z
podzemí	podzemí	k1gNnSc2	podzemí
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
čerpá	čerpat	k5eAaImIp3nS	čerpat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vodárna	vodárna	k1gFnSc1	vodárna
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Káraný	káraný	k2eAgInSc1d1	káraný
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
r.	r.	kA	r.
1911	[number]	k4	1911
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
pro	pro	k7c4	pro
přibližně	přibližně	k6eAd1	přibližně
třetinu	třetina	k1gFnSc4	třetina
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
řadu	řad	k1gInSc2	řad
dalších	další	k2eAgFnPc2d1	další
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
měst	město	k1gNnPc2	město
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Čerpání	čerpání	k1gNnSc1	čerpání
z	z	k7c2	z
podpovrchových	podpovrchový	k2eAgInPc2d1	podpovrchový
zdrojů	zdroj	k1gInPc2	zdroj
z	z	k7c2	z
podzemních	podzemní	k2eAgInPc2d1	podzemní
vrtů	vrt	k1gInPc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Surová	surový	k2eAgFnSc1d1	surová
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
odvádí	odvádět	k5eAaImIp3nS	odvádět
do	do	k7c2	do
úpravny	úpravna	k1gFnSc2	úpravna
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
upravuje	upravovat	k5eAaImIp3nS	upravovat
(	(	kIx(	(
<g/>
mechanické	mechanický	k2eAgNnSc1d1	mechanické
předčištění	předčištění	k1gNnSc1	předčištění
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgNnSc1d1	chemické
čeření	čeření	k1gNnSc1	čeření
<g/>
,	,	kIx,	,
filtrace	filtrace	k1gFnSc1	filtrace
přes	přes	k7c4	přes
pískové	pískový	k2eAgInPc4d1	pískový
filtry	filtr	k1gInPc4	filtr
<g/>
,	,	kIx,	,
odstranění	odstranění	k1gNnSc1	odstranění
iontů	ion	k1gInPc2	ion
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
částečné	částečný	k2eAgNnSc4d1	částečné
odstranění	odstranění	k1gNnSc4	odstranění
dusičnanů	dusičnan	k1gInPc2	dusičnan
a	a	k8xC	a
dusitanů	dusitan	k1gInPc2	dusitan
<g/>
,	,	kIx,	,
dezinfekce	dezinfekce	k1gFnSc1	dezinfekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
vodojemů	vodojem	k1gInPc2	vodojem
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
se	se	k3xPyFc4	se
vodovody	vodovod	k1gInPc7	vodovod
dopravuje	dopravovat	k5eAaImIp3nS	dopravovat
k	k	k7c3	k
spotřebitelům	spotřebitel	k1gMnPc3	spotřebitel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
miliarda	miliarda	k4xCgFnSc1	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zajištění	zajištění	k1gNnSc1	zajištění
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
usnesení	usnesení	k1gNnSc2	usnesení
OSN	OSN	kA	OSN
Rozvojové	rozvojový	k2eAgInPc4d1	rozvojový
cíle	cíl	k1gInPc4	cíl
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
také	také	k6eAd1	také
česká	český	k2eAgFnSc1d1	Česká
humanitární	humanitární	k2eAgFnSc1d1	humanitární
organizace	organizace	k1gFnSc1	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
balených	balený	k2eAgFnPc2d1	balená
vod	voda	k1gFnPc2	voda
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
vystopovat	vystopovat	k5eAaPmF	vystopovat
až	až	k9	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
šlo	jít	k5eAaImAgNnS	jít
výhradně	výhradně	k6eAd1	výhradně
o	o	k7c4	o
vody	voda	k1gFnPc4	voda
léčivé	léčivý	k2eAgFnPc4d1	léčivá
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
už	už	k6eAd1	už
se	s	k7c7	s
skutečným	skutečný	k2eAgInSc7d1	skutečný
nebo	nebo	k8xC	nebo
domnělým	domnělý	k2eAgInSc7d1	domnělý
účinkem	účinek	k1gInSc7	účinek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stáčené	stáčený	k2eAgInPc1d1	stáčený
do	do	k7c2	do
kameninových	kameninový	k2eAgInPc2d1	kameninový
džbánků	džbánek	k1gInPc2	džbánek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
přidaly	přidat	k5eAaPmAgFnP	přidat
i	i	k9	i
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
chuť	chuť	k1gFnSc4	chuť
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
osvěžující	osvěžující	k2eAgInSc4d1	osvěžující
nápoj	nápoj	k1gInSc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
o	o	k7c4	o
minerální	minerální	k2eAgFnPc4d1	minerální
vody	voda	k1gFnPc4	voda
nebo	nebo	k8xC	nebo
o	o	k7c4	o
vody	voda	k1gFnPc4	voda
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ať	ať	k9	ať
původu	původ	k1gInSc2	původ
přirozeného	přirozený	k2eAgInSc2d1	přirozený
(	(	kIx(	(
<g/>
kyselky	kyselka	k1gFnSc2	kyselka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
uměle	uměle	k6eAd1	uměle
připravované	připravovaný	k2eAgNnSc1d1	připravované
<g/>
,	,	kIx,	,
stáčené	stáčený	k2eAgNnSc1d1	stáčené
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
udržel	udržet	k5eAaPmAgInS	udržet
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jednak	jednak	k8xC	jednak
skleněné	skleněný	k2eAgInPc1d1	skleněný
obaly	obal	k1gInPc1	obal
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
postupně	postupně	k6eAd1	postupně
vytlačovány	vytlačován	k2eAgInPc4d1	vytlačován
plastickými	plastický	k2eAgInPc7d1	plastický
a	a	k8xC	a
jednak	jednak	k8xC	jednak
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k9	ještě
k	k	k7c3	k
jiné	jiný	k2eAgFnSc3d1	jiná
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
revolučnější	revoluční	k2eAgFnSc6d2	revolučnější
změně	změna	k1gFnSc6	změna
<g/>
:	:	kIx,	:
balené	balený	k2eAgFnPc1d1	balená
vody	voda	k1gFnPc1	voda
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
používány	používat	k5eAaImNgFnP	používat
též	též	k9	též
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc4	zdroj
"	"	kIx"	"
<g/>
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
<g/>
"	"	kIx"	"
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
řešení	řešení	k1gNnSc4	řešení
občasných	občasný	k2eAgFnPc2d1	občasná
havarijních	havarijní	k2eAgFnPc2d1	havarijní
situací	situace	k1gFnPc2	situace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
distribuovanou	distribuovaný	k2eAgFnSc7d1	distribuovaná
veřejnými	veřejný	k2eAgInPc7d1	veřejný
vodovody	vodovod	k1gInPc7	vodovod
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
vybraných	vybraný	k2eAgInPc2d1	vybraný
druhů	druh	k1gInPc2	druh
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
začaly	začít	k5eAaPmAgFnP	začít
stáčet	stáčet	k5eAaImF	stáčet
i	i	k9	i
vody	voda	k1gFnPc4	voda
z	z	k7c2	z
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
podzemních	podzemní	k2eAgInPc2d1	podzemní
zdrojů	zdroj	k1gInPc2	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nevykazovaly	vykazovat	k5eNaImAgFnP	vykazovat
ani	ani	k8xC	ani
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
farmakologický	farmakologický	k2eAgInSc1d1	farmakologický
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
nejen	nejen	k6eAd1	nejen
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
<g/>
.	.	kIx.	.
</s>
<s>
Pětina	pětina	k1gFnSc1	pětina
lidstva	lidstvo	k1gNnSc2	lidstvo
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
nezávadné	závadný	k2eNgFnSc3d1	nezávadná
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
2,6	[number]	k4	2,6
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
postrádá	postrádat	k5eAaImIp3nS	postrádat
hygienické	hygienický	k2eAgNnSc4d1	hygienické
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
50	[number]	k4	50
<g/>
%	%	kIx~	%
světových	světový	k2eAgInPc2d1	světový
mokřadů	mokřad	k1gInPc2	mokřad
<g/>
.	.	kIx.	.
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
ročně	ročně	k6eAd1	ročně
umírají	umírat	k5eAaImIp3nP	umírat
na	na	k7c4	na
choroby	choroba	k1gFnPc4	choroba
způsobené	způsobený	k2eAgFnPc4d1	způsobená
kontaminovanou	kontaminovaný	k2eAgFnSc7d1	kontaminovaná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
špatnou	špatný	k2eAgFnSc7d1	špatná
hygienou	hygiena	k1gFnSc7	hygiena
(	(	kIx(	(
<g/>
např.	např.	kA	např.
průjmová	průjmový	k2eAgNnPc1d1	průjmové
onemocnění	onemocnění	k1gNnPc1	onemocnění
a	a	k8xC	a
malárie	malárie	k1gFnPc1	malárie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
90	[number]	k4	90
<g/>
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
do	do	k7c2	do
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
to	ten	k3xDgNnSc4	ten
nepříjemnější	příjemný	k2eNgFnSc1d2	nepříjemnější
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zásoby	zásoba	k1gFnPc1	zásoba
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
(	(	kIx(	(
<g/>
FAO	FAO	kA	FAO
<g/>
)	)	kIx)	)
klesly	klesnout	k5eAaPmAgFnP	klesnout
zásoby	zásoba	k1gFnPc1	zásoba
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
čtvrtiny	čtvrtina	k1gFnPc4	čtvrtina
a	a	k8xC	a
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
zásobami	zásoba	k1gFnPc7	zásoba
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
spotřebou	spotřeba	k1gFnSc7	spotřeba
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
prohlubují	prohlubovat	k5eAaImIp3nP	prohlubovat
a	a	k8xC	a
lze	lze	k6eAd1	lze
přitom	přitom	k6eAd1	přitom
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spotřeba	spotřeba	k1gFnSc1	spotřeba
vody	voda	k1gFnSc2	voda
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
stále	stále	k6eAd1	stále
stoupat	stoupat	k5eAaImF	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
znečištění	znečištění	k1gNnSc2	znečištění
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
zemědělství	zemědělství	k1gNnSc2	zemědělství
(	(	kIx(	(
<g/>
pesticidy	pesticid	k1gInPc4	pesticid
<g/>
,	,	kIx,	,
hnojiva	hnojivo	k1gNnPc4	hnojivo
i	i	k8xC	i
zvířecí	zvířecí	k2eAgInPc4d1	zvířecí
exkrementy	exkrement	k1gInPc4	exkrement
<g/>
)	)	kIx)	)
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
vodní	vodní	k2eAgInPc4d1	vodní
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Pitná	pitný	k2eAgFnSc1d1	pitná
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
strategickou	strategický	k2eAgFnSc7d1	strategická
surovinou	surovina	k1gFnSc7	surovina
a	a	k8xC	a
do	do	k7c2	do
intenzivně	intenzivně	k6eAd1	intenzivně
využívaných	využívaný	k2eAgFnPc2d1	využívaná
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
oblastí	oblast	k1gFnPc2	oblast
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
přivádět	přivádět	k5eAaImF	přivádět
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgNnSc1d1	chemické
znečištění	znečištění	k1gNnSc1	znečištění
vody	voda	k1gFnSc2	voda
nelze	lze	k6eNd1	lze
převařením	převaření	k1gNnSc7	převaření
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriologické	bakteriologický	k2eAgNnSc4d1	bakteriologické
znečištění	znečištění	k1gNnSc4	znečištění
odstraníme	odstranit	k5eAaPmIp1nP	odstranit
povařením	povaření	k1gNnSc7	povaření
aspoň	aspoň	k9	aspoň
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
viry	vir	k1gInPc1	vir
jsou	být	k5eAaImIp3nP	být
usmrceny	usmrtit	k5eAaPmNgInP	usmrtit
až	až	k9	až
po	po	k7c6	po
30	[number]	k4	30
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
jakost	jakost	k1gFnSc4	jakost
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
koupání	koupání	k1gNnSc4	koupání
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
upravuje	upravovat	k5eAaImIp3nS	upravovat
vyhláška	vyhláška	k1gFnSc1	vyhláška
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
č.	č.	k?	č.
238	[number]	k4	238
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Celková	celkový	k2eAgFnSc1d1	celková
spotřeba	spotřeba	k1gFnSc1	spotřeba
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
osobní	osobní	k2eAgInPc1d1	osobní
i	i	k8xC	i
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
a	a	k8xC	a
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
<g/>
)	)	kIx)	)
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
zhruba	zhruba	k6eAd1	zhruba
milión	milión	k4xCgInSc1	milión
litrů	litr	k1gInPc2	litr
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
na	na	k7c4	na
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
2,8	[number]	k4	2,8
miliónu	milión	k4xCgInSc2	milión
litrů	litr	k1gInPc2	litr
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
decilitr	decilitr	k1gInSc4	decilitr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
spotřeba	spotřeba	k1gFnSc1	spotřeba
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
41	[number]	k4	41
tisíc	tisíc	k4xCgInPc2	tisíc
litrů	litr	k1gInPc2	litr
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
například	například	k6eAd1	například
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
1	[number]	k4	1
kg	kg	kA	kg
rýže	rýže	k1gFnSc2	rýže
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
1000	[number]	k4	1000
až	až	k9	až
3000	[number]	k4	3000
litrů	litr	k1gInPc2	litr
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
na	na	k7c4	na
1	[number]	k4	1
kg	kg	kA	kg
hovězího	hovězí	k1gNnSc2	hovězí
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
13	[number]	k4	13
až	až	k9	až
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
litrů	litr	k1gInPc2	litr
a	a	k8xC	a
1	[number]	k4	1
kg	kg	kA	kg
čokolády	čokoláda	k1gFnSc2	čokoláda
až	až	k6eAd1	až
17	[number]	k4	17
tisíc	tisíc	k4xCgInPc2	tisíc
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
ze	z	k7c2	z
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
38	[number]	k4	38
litrů	litr	k1gInPc2	litr
na	na	k7c4	na
MWh	MWh	k1gFnSc4	MWh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
uhlí	uhlí	k1gNnSc2	uhlí
530	[number]	k4	530
až	až	k9	až
2000	[number]	k4	2000
litrů	litr	k1gInPc2	litr
<g/>
/	/	kIx~	/
<g/>
MWh	MWh	k1gFnPc2	MWh
a	a	k8xC	a
z	z	k7c2	z
biopaliv	biopalit	k5eAaPmDgInS	biopalit
dokonce	dokonce	k9	dokonce
stotisíce	stotisíce	k?	stotisíce
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Iónský	iónský	k2eAgMnSc1d1	iónský
filosof	filosof	k1gMnSc1	filosof
Thalés	Thalésa	k1gFnPc2	Thalésa
z	z	k7c2	z
Milétu	Milét	k1gInSc2	Milét
v	v	k7c4	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pokládal	pokládat	k5eAaImAgMnS	pokládat
vodu	voda	k1gFnSc4	voda
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
element	element	k1gInSc4	element
své	svůj	k3xOyFgFnSc2	svůj
kosmologie	kosmologie	k1gFnSc2	kosmologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
až	až	k9	až
do	do	k7c2	do
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
přidali	přidat	k5eAaPmAgMnP	přidat
další	další	k2eAgInPc4d1	další
základní	základní	k2eAgInPc4d1	základní
elementy	element	k1gInPc4	element
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potom	potom	k6eAd1	potom
dominovalo	dominovat	k5eAaImAgNnS	dominovat
islámskému	islámský	k2eAgNnSc3d1	islámské
a	a	k8xC	a
křesťanskému	křesťanský	k2eAgNnSc3d1	křesťanské
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřprvkový	čtyřprvkový	k2eAgInSc1d1	čtyřprvkový
princip	princip	k1gInSc1	princip
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
až	až	k9	až
do	do	k7c2	do
Isaaca	Isaacus	k1gMnSc2	Isaacus
Newtona	Newton	k1gMnSc2	Newton
(	(	kIx(	(
<g/>
De	De	k?	De
Natura	Natura	k1gFnSc1	Natura
Acidorum	Acidorum	k1gInSc1	Acidorum
–	–	k?	–
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc4	všechen
látky	látka	k1gFnPc4	látka
lze	lze	k6eAd1	lze
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přejal	přejmout	k5eAaPmAgMnS	přejmout
roli	role	k1gFnSc4	role
vody	voda	k1gFnSc2	voda
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgInPc1d1	přesný
stechiometrické	stechiometrický	k2eAgInPc1d1	stechiometrický
výpočty	výpočet	k1gInPc1	výpočet
atomových	atomový	k2eAgFnPc2d1	atomová
hmotností	hmotnost	k1gFnPc2	hmotnost
jiných	jiný	k2eAgInPc2d1	jiný
prvků	prvek	k1gInPc2	prvek
však	však	k9	však
později	pozdě	k6eAd2	pozdě
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
násobky	násobek	k1gInPc7	násobek
atomové	atomový	k2eAgFnSc2d1	atomová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
