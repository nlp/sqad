<s>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Commonwealth	Commonwealth	k1gInSc1	Commonwealth
of	of	k?	of
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
regionu	region	k1gInSc2	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
New	New	k1gFnSc7	New
Hampshire	Hampshir	k1gInSc5	Hampshir
a	a	k8xC	a
Vermontem	Vermont	k1gMnSc7	Vermont
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
New	New	k1gFnPc2	New
Yorkem	York	k1gInSc7	York
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Connecticutem	Connecticut	k1gMnSc7	Connecticut
a	a	k8xC	a
Rhode	Rhodos	k1gInSc5	Rhodos
Islandem	Island	k1gInSc7	Island
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgNnSc1d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgNnSc1d1	jihovýchodní
ohraničení	ohraničení	k1gNnSc1	ohraničení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
27	[number]	k4	27
336	[number]	k4	336
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Massachusetts	Massachusetts	k1gNnSc4	Massachusetts
sedmým	sedmý	k4xOgInSc7	sedmý
nejmenším	malý	k2eAgInSc7d3	nejmenší
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
9,8	[number]	k4	9,8
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
15	[number]	k4	15
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
324	[number]	k4	324
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Boston	Boston	k1gInSc1	Boston
se	s	k7c7	s
670	[number]	k4	670
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
největšími	veliký	k2eAgMnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Worcester	Worcester	k1gInSc4	Worcester
se	s	k7c7	s
185	[number]	k4	185
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
Springfield	Springfield	k1gMnSc1	Springfield
(	(	kIx(	(
<g/>
155	[number]	k4	155
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lowell	Lowell	k1gMnSc1	Lowell
(	(	kIx(	(
<g/>
110	[number]	k4	110
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cambridge	Cambridge	k1gFnSc1	Cambridge
(	(	kIx(	(
<g/>
110	[number]	k4	110
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
patří	patřit	k5eAaImIp3nS	patřit
309	[number]	k4	309
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Mount	Mounta	k1gFnPc2	Mounta
Greylock	Greylock	k1gInSc4	Greylock
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
1063	[number]	k4	1063
m.	m.	k?	m.
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Connecticut	Connecticut	k1gMnSc1	Connecticut
<g/>
,	,	kIx,	,
Westfield	Westfield	k1gMnSc1	Westfield
<g/>
,	,	kIx,	,
Merrimack	Merrimack	k1gMnSc1	Merrimack
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
dorazili	dorazit	k5eAaPmAgMnP	dorazit
první	první	k4xOgMnPc1	první
angličtí	anglický	k2eAgMnPc1d1	anglický
osadníci	osadník	k1gMnPc1	osadník
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Otcové	otec	k1gMnPc1	otec
poutníci	poutník	k1gMnPc1	poutník
<g/>
,	,	kIx,	,
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Mayflower	Mayflowra	k1gFnPc2	Mayflowra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
zde	zde	k6eAd1	zde
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
později	pozdě	k6eAd2	pozdě
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
město	město	k1gNnSc1	město
Plymouth	Plymouth	k1gInSc1	Plymouth
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
úspěšně	úspěšně	k6eAd1	úspěšně
zřízenou	zřízený	k2eAgFnSc4d1	zřízená
trvalou	trvalý	k2eAgFnSc4d1	trvalá
anglickou	anglický	k2eAgFnSc4d1	anglická
kolonii	kolonie	k1gFnSc4	kolonie
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešních	dnešní	k2eAgMnPc2d1	dnešní
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
je	on	k3xPp3gNnSc4	on
puritáni	puritán	k1gMnPc1	puritán
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
Massachusettském	massachusettský	k2eAgInSc6d1	massachusettský
zálivu	záliv	k1gInSc6	záliv
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
Bostonu	Boston	k1gInSc2	Boston
další	další	k2eAgFnSc4d1	další
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
podle	podle	k7c2	podle
místního	místní	k2eAgInSc2d1	místní
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Mesečusetů	Mesečuset	k1gMnPc2	Mesečuset
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
jazyce	jazyk	k1gInSc6	jazyk
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
označovali	označovat	k5eAaImAgMnP	označovat
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
lid	lid	k1gInSc1	lid
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
kopců	kopec	k1gInPc2	kopec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
kolonie	kolonie	k1gFnPc1	kolonie
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1692	[number]	k4	1692
královskou	královský	k2eAgFnSc7d1	královská
listinou	listina	k1gFnSc7	listina
sloučeny	sloučit	k5eAaPmNgFnP	sloučit
do	do	k7c2	do
společné	společný	k2eAgFnSc2d1	společná
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
třinácti	třináct	k4xCc2	třináct
zakládajících	zakládající	k2eAgInPc2d1	zakládající
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
jako	jako	k8xS	jako
šestý	šestý	k4xOgInSc1	šestý
stát	stát	k1gInSc1	stát
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
Ústavu	ústava	k1gFnSc4	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
sídlí	sídlet	k5eAaImIp3nS	sídlet
Harvardova	Harvardův	k2eAgFnSc1d1	Harvardova
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
Massachusettský	massachusettský	k2eAgInSc1d1	massachusettský
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
první	první	k4xOgMnPc1	první
Otcové	otec	k1gMnPc1	otec
poutníci	poutník	k1gMnPc1	poutník
-	-	kIx~	-
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
Plymouth	Plymoutha	k1gFnPc2	Plymoutha
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
tvořilo	tvořit	k5eAaImAgNnS	tvořit
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
i	i	k8xC	i
dnešní	dnešní	k2eAgInSc1d1	dnešní
Maine	Main	k1gMnSc5	Main
a	a	k8xC	a
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
odtrhly	odtrhnout	k5eAaPmAgFnP	odtrhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
kolonie	kolonie	k1gFnSc2	kolonie
Massachusettská	massachusettský	k2eAgFnSc1d1	Massachusettská
zátoka	zátoka	k1gFnSc1	zátoka
<g/>
,	,	kIx,	,
Plymouth	Plymouth	k1gInSc1	Plymouth
<g/>
,	,	kIx,	,
Connecticut	Connecticut	k1gInSc1	Connecticut
a	a	k8xC	a
New	New	k1gFnSc1	New
Haven	Haven	k1gInSc4	Haven
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
Novoanglickou	Novoanglický	k2eAgFnSc4d1	Novoanglický
konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
bylo	být	k5eAaImAgNnS	být
silně	silně	k6eAd1	silně
puritánskou	puritánský	k2eAgFnSc7d1	puritánská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
netolerovala	tolerovat	k5eNaImAgFnS	tolerovat
ostatní	ostatní	k2eAgNnSc4d1	ostatní
náboženství	náboženství	k1gNnSc4	náboženství
-	-	kIx~	-
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1660	[number]	k4	1660
se	se	k3xPyFc4	se
odtrhly	odtrhnout	k5eAaPmAgFnP	odtrhnout
kvakerské	kvakerský	k2eAgFnPc1d1	kvakerská
Connecticut	Connecticut	k1gInSc4	Connecticut
a	a	k8xC	a
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1675	[number]	k4	1675
se	se	k3xPyFc4	se
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
rozmohl	rozmoct	k5eAaPmAgInS	rozmoct
zvyk	zvyk	k1gInSc4	zvyk
"	"	kIx"	"
<g/>
otroka	otrok	k1gMnSc2	otrok
do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
počestné	počestný	k2eAgFnSc2d1	počestná
rodiny	rodina	k1gFnSc2	rodina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
jako	jako	k8xS	jako
takové	takový	k3xDgFnPc4	takový
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
1691	[number]	k4	1691
sloučením	sloučení	k1gNnSc7	sloučení
Plymouthu	Plymouth	k1gInSc2	Plymouth
a	a	k8xC	a
Massachusettské	massachusettský	k2eAgFnSc2d1	Massachusettská
zátoky	zátoka	k1gFnSc2	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Salemské	Salemský	k2eAgInPc1d1	Salemský
procesy	proces	k1gInPc1	proces
s	s	k7c7	s
čarodějnicemi	čarodějnice	k1gFnPc7	čarodějnice
zde	zde	k6eAd1	zde
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1692	[number]	k4	1692
<g/>
.	.	kIx.	.
</s>
<s>
Životem	život	k1gInSc7	život
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
24	[number]	k4	24
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
také	také	k9	také
dva	dva	k4xCgMnPc1	dva
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Massachusetts	Massachusetts	k1gNnPc1	Massachusetts
staly	stát	k5eAaPmAgInP	stát
centrem	centrum	k1gNnSc7	centrum
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konal	konat	k5eAaImAgInS	konat
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
britskému	britský	k2eAgNnSc3d1	Britské
impériu	impérium	k1gNnSc3	impérium
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Bostonské	bostonský	k2eAgNnSc1d1	Bostonské
pití	pití	k1gNnSc1	pití
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Americkým	americký	k2eAgInSc7d1	americký
státem	stát	k1gInSc7	stát
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgInP	být
Massachusetts	Massachusetts	k1gNnSc4	Massachusetts
centrem	centrum	k1gNnSc7	centrum
abolicionismu	abolicionismus	k1gInSc2	abolicionismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
6	[number]	k4	6
547	[number]	k4	547
629	[number]	k4	629
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
80,4	[number]	k4	80,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
6,6	[number]	k4	6,6
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
5,3	[number]	k4	5,3
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
4,7	[number]	k4	4,7
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,6	[number]	k4	2,6
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
9,6	[number]	k4	9,6
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bostonu	Boston	k1gInSc2	Boston
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
se	se	k3xPyFc4	se
usídlilo	usídlit	k5eAaPmAgNnS	usídlit
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
irského	irský	k2eAgInSc2d1	irský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
taky	taky	k6eAd1	taky
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
známý	známý	k2eAgInSc1d1	známý
klan	klan	k1gInSc1	klan
Kennedyů	Kennedy	k1gMnPc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
ve	v	k7c6	v
městech	město	k1gNnPc6	město
žijí	žít	k5eAaImIp3nP	žít
často	často	k6eAd1	často
i	i	k9	i
původem	původ	k1gInSc7	původ
Italové	Ital	k1gMnPc1	Ital
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
západní	západní	k2eAgFnSc7d1	západní
částí	část	k1gFnSc7	část
státu	stát	k1gInSc2	stát
v	v	k7c6	v
průmyslových	průmyslový	k2eAgNnPc6d1	průmyslové
městech	město	k1gNnPc6	město
převažují	převažovat	k5eAaImIp3nP	převažovat
původem	původ	k1gInSc7	původ
Frankokanaďané	Frankokanaďaná	k1gFnSc2	Frankokanaďaná
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usídlili	usídlit	k5eAaPmAgMnP	usídlit
jako	jako	k9	jako
dělníci	dělník	k1gMnPc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ve	v	k7c6	v
venkovských	venkovský	k2eAgNnPc6d1	venkovské
městečkách	městečko	k1gNnPc6	městečko
ještě	ještě	k6eAd1	ještě
existuje	existovat	k5eAaImIp3nS	existovat
původní	původní	k2eAgFnSc1d1	původní
kultura	kultura	k1gFnSc1	kultura
presbyteriánských	presbyteriánský	k2eAgFnPc2d1	presbyteriánská
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
blízko	blízko	k6eAd1	blízko
idejím	idea	k1gFnPc3	idea
původních	původní	k2eAgMnPc2d1	původní
otců	otec	k1gMnPc2	otec
zakladatelů	zakladatel	k1gMnPc2	zakladatel
<g/>
.	.	kIx.	.
stav	stav	k1gInSc1	stav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
Křesťané	křesťan	k1gMnPc1	křesťan
–	–	k?	–
79	[number]	k4	79
%	%	kIx~	%
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
–	–	k?	–
47	[number]	k4	47
%	%	kIx~	%
protestanti	protestant	k1gMnPc1	protestant
–	–	k?	–
31	[number]	k4	31
%	%	kIx~	%
United	United	k1gMnSc1	United
Church	Church	k1gMnSc1	Church
of	of	k?	of
Christ	Christ	k1gMnSc1	Christ
-	-	kIx~	-
4	[number]	k4	4
%	%	kIx~	%
baptisté	baptista	k1gMnPc1	baptista
–	–	k?	–
4	[number]	k4	4
%	%	kIx~	%
episkopální	episkopální	k2eAgFnSc2d1	episkopální
církve	církev	k1gFnSc2	církev
-	-	kIx~	-
3	[number]	k4	3
%	%	kIx~	%
metodisté	metodista	k1gMnPc1	metodista
–	–	k?	–
2	[number]	k4	2
%	%	kIx~	%
letniční	letniční	k2eAgFnSc2d1	letniční
církve	církev	k1gFnSc2	církev
–	–	k?	–
2	[number]	k4	2
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
protestanti	protestant	k1gMnPc1	protestant
-	-	kIx~	-
16	[number]	k4	16
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
křesťané	křesťan	k1gMnPc1	křesťan
–	–	k?	–
1	[number]	k4	1
%	%	kIx~	%
mormoni	mormon	k1gMnPc1	mormon
-	-	kIx~	-
1	[number]	k4	1
%	%	kIx~	%
židovství	židovství	k1gNnSc6	židovství
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
unitáři	unitář	k1gMnPc1	unitář
-	-	kIx~	-
1	[number]	k4	1
%	%	kIx~	%
jiná	jiný	k2eAgNnPc1d1	jiné
náboženství	náboženství	k1gNnPc1	náboženství
–	–	k?	–
1	[number]	k4	1
%	%	kIx~	%
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
–	–	k?	–
17	[number]	k4	17
%	%	kIx~	%
</s>
