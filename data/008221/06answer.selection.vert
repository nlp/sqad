<s>
Turing	Turing	k1gInSc4	Turing
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
inteligentních	inteligentní	k2eAgInPc2d1	inteligentní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
myšlenky	myšlenka	k1gFnSc2	myšlenka
tzv.	tzv.	kA	tzv.
Turingova	Turingův	k2eAgInSc2d1	Turingův
testu	test	k1gInSc2	test
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
inteligentní	inteligentní	k2eAgFnSc4d1	inteligentní
můžeme	moct	k5eAaImIp1nP	moct
stroj	stroj	k1gInSc1	stroj
považovat	považovat	k5eAaImF	považovat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
nejsme	být	k5eNaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
odlišit	odlišit	k5eAaPmF	odlišit
jeho	on	k3xPp3gInSc4	on
výstup	výstup	k1gInSc4	výstup
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jeho	jeho	k3xOp3gFnPc4	jeho
odpovědi	odpověď	k1gFnPc4	odpověď
<g/>
)	)	kIx)	)
od	od	k7c2	od
výstupu	výstup	k1gInSc2	výstup
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
