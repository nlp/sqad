<p>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Liberijská	liberijský	k2eAgFnSc1d1	liberijská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
břehy	břeh	k1gInPc4	břeh
omývá	omývat	k5eAaImIp3nS	omývat
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Guinea	Guinea	k1gFnSc1	Guinea
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
a	a	k8xC	a
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
.	.	kIx.	.
</s>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
má	mít	k5eAaImIp3nS	mít
teplé	teplý	k2eAgNnSc4d1	teplé
rovníkové	rovníkový	k2eAgNnSc4d1	rovníkové
klima	klima	k1gNnSc4	klima
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
množstvím	množství	k1gNnSc7	množství
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
větry	vítr	k1gInPc7	vítr
v	v	k7c6	v
suchém	suchý	k2eAgNnSc6d1	suché
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Zalidněné	zalidněný	k2eAgNnSc1d1	zalidněné
Pepřonosné	Pepřonosný	k2eAgNnSc1d1	Pepřonosný
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
převážně	převážně	k6eAd1	převážně
mangrovovými	mangrovový	k2eAgInPc7d1	mangrovový
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
je	být	k5eAaImIp3nS	být
zalesněno	zalesnit	k5eAaPmNgNnS	zalesnit
jen	jen	k6eAd1	jen
řídce	řídce	k6eAd1	řídce
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
rovinu	rovina	k1gFnSc4	rovina
se	s	k7c7	s
suššími	suchý	k2eAgFnPc7d2	sušší
pastvinami	pastvina	k1gFnPc7	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
Libérie	Libérie	k1gFnSc1	Libérie
svědkem	svědek	k1gMnSc7	svědek
dvou	dva	k4xCgFnPc6	dva
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyhnaly	vyhnat	k5eAaPmAgFnP	vyhnat
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
zdevastovaly	zdevastovat	k5eAaPmAgInP	zdevastovat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
–	–	k?	–
země	zem	k1gFnSc2	zem
svobody	svoboda	k1gFnSc2	svoboda
–	–	k?	–
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
bezpráví	bezpráví	k1gNnSc3	bezpráví
a	a	k8xC	a
útlaku	útlak	k1gInSc3	útlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
cílem	cíl	k1gInSc7	cíl
osvobozených	osvobozený	k2eAgMnPc2d1	osvobozený
amerických	americký	k2eAgMnPc2d1	americký
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
kolonizaci	kolonizace	k1gFnSc4	kolonizace
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Libérie	Libérie	k1gFnSc2	Libérie
usídlilo	usídlit	k5eAaPmAgNnS	usídlit
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
000	[number]	k4	000
Afroameričanů	Afroameričan	k1gMnPc2	Afroameričan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
v	v	k7c6	v
obavě	obava	k1gFnSc6	obava
z	z	k7c2	z
možné	možný	k2eAgFnSc2d1	možná
britské	britský	k2eAgFnSc2d1	britská
kolonizace	kolonizace	k1gFnSc2	kolonizace
<g/>
,	,	kIx,	,
ustavili	ustavit	k5eAaPmAgMnP	ustavit
druhý	druhý	k4xOgInSc4	druhý
nejstarší	starý	k2eAgInSc4d3	nejstarší
moderní	moderní	k2eAgInSc4d1	moderní
africký	africký	k2eAgInSc4d1	africký
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgMnS	stanout
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Joseph	Joseph	k1gMnSc1	Joseph
J.	J.	kA	J.
Roberts	Roberts	k1gInSc1	Roberts
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
elementem	element	k1gInSc7	element
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgNnSc4d2	pozdější
vyhrocení	vyhrocení	k1gNnSc4	vyhrocení
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
situace	situace	k1gFnSc2	situace
byly	být	k5eAaImAgInP	být
spory	spor	k1gInPc1	spor
s	s	k7c7	s
místním	místní	k2eAgNnSc7d1	místní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
projevům	projev	k1gInPc3	projev
nadřazenosti	nadřazenost	k1gFnSc2	nadřazenost
a	a	k8xC	a
diskriminace	diskriminace	k1gFnSc2	diskriminace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
relativně	relativně	k6eAd1	relativně
vzdělanějších	vzdělaný	k2eAgMnPc2d2	vzdělanější
a	a	k8xC	a
kosmopolitnějších	kosmopolitní	k2eAgMnPc2d2	kosmopolitnější
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
sociální	sociální	k2eAgFnSc1d1	sociální
stratifikace	stratifikace	k1gFnSc1	stratifikace
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
etnického	etnický	k2eAgInSc2d1	etnický
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
integraci	integrace	k1gFnSc6	integrace
původního	původní	k2eAgNnSc2d1	původní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
poměrně	poměrně	k6eAd1	poměrně
dynamicky	dynamicky	k6eAd1	dynamicky
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgFnPc1d1	rozvíjející
ekonomiky	ekonomika	k1gFnPc1	ekonomika
vytvářené	vytvářený	k2eAgMnPc4d1	vytvářený
potomky	potomek	k1gMnPc4	potomek
Afroameričanů	Afroameričan	k1gMnPc2	Afroameričan
byly	být	k5eAaImAgFnP	být
poměrně	poměrně	k6eAd1	poměrně
nedůrazné	důrazný	k2eNgFnPc1d1	nedůrazná
(	(	kIx(	(
<g/>
přesto	přesto	k8xC	přesto
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
například	například	k6eAd1	například
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
sjednocovací	sjednocovací	k2eAgFnSc1d1	sjednocovací
politika	politika	k1gFnSc1	politika
prezidenta	prezident	k1gMnSc2	prezident
Tubmana	Tubman	k1gMnSc2	Tubman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
investice	investice	k1gFnPc1	investice
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
zejména	zejména	k9	zejména
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
sice	sice	k8xC	sice
napomohly	napomoct	k5eAaPmAgFnP	napomoct
celkovému	celkový	k2eAgInSc3d1	celkový
rozvoji	rozvoj	k1gInSc3	rozvoj
místního	místní	k2eAgNnSc2d1	místní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rozevírání	rozevírání	k1gNnSc1	rozevírání
sociálních	sociální	k2eAgFnPc2d1	sociální
nůžek	nůžky	k1gFnPc2	nůžky
mezi	mezi	k7c7	mezi
Afroameričany	Afroameričan	k1gMnPc7	Afroameričan
a	a	k8xC	a
příslušníky	příslušník	k1gMnPc7	příslušník
místních	místní	k2eAgInPc2d1	místní
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Frustrace	frustrace	k1gFnSc1	frustrace
způsobená	způsobený	k2eAgFnSc1d1	způsobená
jednak	jednak	k8xC	jednak
statusem	status	k1gInSc7	status
druhořadých	druhořadý	k2eAgMnPc2d1	druhořadý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
efektivností	efektivnost	k1gFnSc7	efektivnost
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
dovedla	dovést	k5eAaPmAgFnS	dovést
původní	původní	k2eAgNnSc4d1	původní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
k	k	k7c3	k
vojenskému	vojenský	k2eAgInSc3d1	vojenský
převratu	převrat	k1gInSc3	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgMnS	stanout
seržant	seržant	k1gMnSc1	seržant
Samuel	Samuel	k1gMnSc1	Samuel
Kanyon	Kanyon	k1gMnSc1	Kanyon
Doe	Doe	k1gMnSc1	Doe
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
údajně	údajně	k6eAd1	údajně
osobně	osobně	k6eAd1	osobně
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
prezidenta	prezident	k1gMnSc4	prezident
Tolberta	Tolbert	k1gMnSc4	Tolbert
<g/>
.	.	kIx.	.
</s>
<s>
Doe	Doe	k?	Doe
vládl	vládnout	k5eAaImAgInS	vládnout
velice	velice	k6eAd1	velice
autoritativním	autoritativní	k2eAgInSc7d1	autoritativní
způsobem	způsob	k1gInSc7	způsob
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
USA	USA	kA	USA
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
potkal	potkat	k5eAaPmAgInS	potkat
ho	on	k3xPp3gInSc4	on
však	však	k9	však
stejný	stejný	k2eAgInSc4d1	stejný
osud	osud	k1gInSc4	osud
jako	jako	k8xS	jako
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
"	"	kIx"	"
<g/>
princem	princ	k1gMnSc7	princ
<g/>
"	"	kIx"	"
Yormie	Yormie	k1gFnSc1	Yormie
Johnsonem	Johnson	k1gInSc7	Johnson
<g/>
,	,	kIx,	,
vůdcem	vůdce	k1gMnSc7	vůdce
Nezávislé	závislý	k2eNgFnSc2d1	nezávislá
národní	národní	k2eAgFnSc2d1	národní
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
fronty	fronta	k1gFnSc2	fronta
Libérie	Libérie	k1gFnSc2	Libérie
(	(	kIx(	(
<g/>
INPFL	INPFL	kA	INPFL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgFnPc4d1	oddělená
frakce	frakce	k1gFnPc4	frakce
Taylorovy	Taylorův	k2eAgFnSc2d1	Taylorova
národní	národní	k2eAgFnSc2d1	národní
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
fronty	fronta	k1gFnSc2	fronta
Libérie	Libérie	k1gFnSc2	Libérie
(	(	kIx(	(
<g/>
NPFL	NPFL	kA	NPFL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
až	až	k9	až
1996	[number]	k4	1996
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
první	první	k4xOgFnSc1	první
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
NPFL	NPFL	kA	NPFL
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
opoziční	opoziční	k2eAgNnSc1d1	opoziční
Spojené	spojený	k2eAgNnSc1d1	spojené
osvobozenecké	osvobozenecký	k2eAgNnSc1d1	osvobozenecké
hnutí	hnutí	k1gNnSc1	hnutí
Libérie	Libérie	k1gFnSc2	Libérie
za	za	k7c4	za
demokracii	demokracie	k1gFnSc4	demokracie
(	(	kIx(	(
<g/>
ULIMO	ULIMO	kA	ULIMO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
sjednotili	sjednotit	k5eAaPmAgMnP	sjednotit
stoupenci	stoupenec	k1gMnPc1	stoupenec
zavražděného	zavražděný	k2eAgNnSc2d1	zavražděné
S.	S.	kA	S.
Doa	Doa	k1gMnSc1	Doa
<g/>
.	.	kIx.	.
</s>
<s>
INPFL	INPFL	kA	INPFL
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Taylor	Taylor	k1gInSc1	Taylor
upevnil	upevnit	k5eAaPmAgInS	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
Johnsonovi	Johnson	k1gMnSc3	Johnson
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
prezidenta	prezident	k1gMnSc2	prezident
Libérie	Libérie	k1gFnSc2	Libérie
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
v	v	k7c6	v
Nigérii	Nigérie	k1gFnSc6	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
NPFL	NPFL	kA	NPFL
vytrvale	vytrvale	k6eAd1	vytrvale
vzdorovalo	vzdorovat	k5eAaImAgNnS	vzdorovat
úsilí	úsilí	k1gNnSc1	úsilí
jednotek	jednotka	k1gFnPc2	jednotka
ECOMOG	ECOMOG	kA	ECOMOG
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
odzbrojení	odzbrojení	k1gNnSc6	odzbrojení
a	a	k8xC	a
porušovalo	porušovat	k5eAaImAgNnS	porušovat
postupně	postupně	k6eAd1	postupně
uzavírané	uzavíraný	k2eAgFnPc4d1	uzavíraná
mírové	mírový	k2eAgFnPc4d1	mírová
dohody	dohoda	k1gFnPc4	dohoda
zprostředkované	zprostředkovaný	k2eAgFnPc4d1	zprostředkovaná
ECOMOG	ECOMOG	kA	ECOMOG
–	–	k?	–
Dohoda	dohoda	k1gFnSc1	dohoda
z	z	k7c2	z
Yamoussoukro	Yamoussoukro	k1gNnSc4	Yamoussoukro
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
Cotonou	Cotona	k1gFnSc7	Cotona
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Akosombo	Akosomba	k1gMnSc5	Akosomba
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
Abuji	Abuj	k1gInSc6	Abuj
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
které	který	k3yRgFnSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přechodná	přechodný	k2eAgFnSc1d1	přechodná
vláda	vláda	k1gFnSc1	vláda
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
NPFL	NPFL	kA	NPFL
měla	mít	k5eAaImAgFnS	mít
pozitivnější	pozitivní	k2eAgInPc4d2	pozitivnější
dopady	dopad	k1gInPc4	dopad
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
střety	střet	k1gInPc1	střet
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
kvůli	kvůli	k7c3	kvůli
odporu	odpor	k1gInSc3	odpor
Johnsonových	Johnsonův	k2eAgMnPc2d1	Johnsonův
přívrženců	přívrženec	k1gMnPc2	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
Abuji	Abuj	k1gInSc6	Abuj
byly	být	k5eAaImAgFnP	být
dohodnuty	dohodnout	k5eAaPmNgFnP	dohodnout
řádné	řádný	k2eAgFnPc1d1	řádná
volby	volba	k1gFnPc1	volba
a	a	k8xC	a
NPFL	NPFL	kA	NPFL
se	se	k3xPyFc4	se
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
v	v	k7c4	v
politickou	politický	k2eAgFnSc4d1	politická
Národní	národní	k2eAgFnSc4d1	národní
vlasteneckou	vlastenecký	k2eAgFnSc4d1	vlastenecká
stranu	strana	k1gFnSc4	strana
(	(	kIx(	(
<g/>
NPP	NPP	kA	NPP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
Charles	Charles	k1gMnSc1	Charles
Taylor	Taylor	k1gMnSc1	Taylor
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
válka	válka	k1gFnSc1	válka
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevila	objevit	k5eAaPmAgFnS	objevit
povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
frakce	frakce	k1gFnSc1	frakce
Liberijci	Liberijce	k1gMnPc1	Liberijce
svorní	svorný	k2eAgMnPc1d1	svorný
v	v	k7c6	v
usmíření	usmíření	k1gNnSc6	usmíření
a	a	k8xC	a
demokracii	demokracie	k1gFnSc6	demokracie
(	(	kIx(	(
<g/>
LURD	LURD	kA	LURD
<g/>
)	)	kIx)	)
podporovaní	podporovaný	k2eAgMnPc1d1	podporovaný
ze	z	k7c2	z
sousední	sousední	k2eAgFnSc2d1	sousední
Guineje	Guinea	k1gFnSc2	Guinea
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Taylorovi	Taylor	k1gMnSc3	Taylor
postavila	postavit	k5eAaPmAgFnS	postavit
ještě	ještě	k9	ještě
druhá	druhý	k4xOgFnSc1	druhý
povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
demokracii	demokracie	k1gFnSc4	demokracie
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
(	(	kIx(	(
<g/>
MoDeL	model	k1gInSc1	model
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povstalcům	povstalec	k1gMnPc3	povstalec
z	z	k7c2	z
obou	dva	k4xCgMnPc2	dva
táborů	tábor	k1gMnPc2	tábor
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
podařilo	podařit	k5eAaPmAgNnS	podařit
ovládnout	ovládnout	k5eAaPmF	ovládnout
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
a	a	k8xC	a
zatlačit	zatlačit	k5eAaPmF	zatlačit
Taylorovy	Taylorův	k2eAgFnPc4d1	Taylorova
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Monrovie	Monrovia	k1gFnSc2	Monrovia
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
povstaleckého	povstalecký	k2eAgInSc2d1	povstalecký
postupu	postup	k1gInSc2	postup
a	a	k8xC	a
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
úsilí	úsilí	k1gNnSc2	úsilí
Charles	Charles	k1gMnSc1	Charles
Taylor	Taylor	k1gMnSc1	Taylor
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgInS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
v	v	k7c6	v
Nigérii	Nigérie	k1gFnSc6	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
Mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
LURD	LURD	kA	LURD
a	a	k8xC	a
MoDeL	model	k1gInSc4	model
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
v	v	k7c6	v
ghanské	ghanský	k2eAgFnSc6d1	ghanská
Akkře	Akkra	k1gFnSc6	Akkra
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
Taylor	Taylor	k1gInSc1	Taylor
vydán	vydat	k5eAaPmNgInS	vydat
z	z	k7c2	z
Nigérie	Nigérie	k1gFnSc2	Nigérie
do	do	k7c2	do
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
obviněn	obviněn	k2eAgInSc1d1	obviněn
tribunálem	tribunál	k1gInSc7	tribunál
pro	pro	k7c4	pro
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgNnSc1d1	soudní
přelíčení	přelíčení	k1gNnSc1	přelíčení
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
zločinů	zločin	k1gMnPc2	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
,	,	kIx,	,
porušení	porušení	k1gNnSc3	porušení
Ženevských	ženevský	k2eAgFnPc2d1	Ženevská
konvencí	konvence	k1gFnPc2	konvence
a	a	k8xC	a
dalšímu	další	k2eAgNnSc3d1	další
vážnému	vážný	k2eAgNnSc3d1	vážné
porušování	porušování	k1gNnSc3	porušování
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
lidských	lidský	k2eAgFnPc2d1	lidská
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přechodná	přechodný	k2eAgFnSc1d1	přechodná
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
volby	volba	k1gFnPc1	volba
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Taylorově	Taylorův	k2eAgInSc6d1	Taylorův
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
přechodné	přechodný	k2eAgFnSc2d1	přechodná
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Gyude	Gyud	k1gInSc5	Gyud
Bryant	Bryant	k1gInSc4	Bryant
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
úlohou	úloha	k1gFnSc7	úloha
přechodné	přechodný	k2eAgFnSc2d1	přechodná
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
připravit	připravit	k5eAaPmF	připravit
zemi	zem	k1gFnSc4	zem
na	na	k7c4	na
spravedlivé	spravedlivý	k2eAgFnPc4d1	spravedlivá
a	a	k8xC	a
pokojné	pokojný	k2eAgFnPc4d1	pokojná
demokratické	demokratický	k2eAgFnPc4d1	demokratická
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
Mise	mise	k1gFnSc1	mise
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
(	(	kIx(	(
<g/>
UNMIL	UNMIL	kA	UNMIL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zajišťovaly	zajišťovat	k5eAaImAgFnP	zajišťovat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
Libérie	Libérie	k1gFnSc1	Libérie
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Ellen	Ellen	k2eAgInSc4d1	Ellen
Johnson-Sirleaf	Johnson-Sirleaf	k1gInSc4	Johnson-Sirleaf
<g/>
,	,	kIx,	,
vystudovaná	vystudovaný	k2eAgFnSc1d1	vystudovaná
ekonomka	ekonomka	k1gFnSc1	ekonomka
z	z	k7c2	z
Harvardu	Harvard	k1gInSc2	Harvard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
post	post	k1gInSc4	post
Ellen	Ellen	k2eAgInSc1d1	Ellen
Johnson-Sirleaf	Johnson-Sirleaf	k1gInSc1	Johnson-Sirleaf
===	===	k?	===
</s>
</p>
<p>
<s>
Ellen	Ellen	k2eAgInSc4d1	Ellen
Johnson-Sirleaf	Johnson-Sirleaf	k1gInSc4	Johnson-Sirleaf
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
zvolení	zvolení	k1gNnSc1	zvolení
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
velkou	velký	k2eAgFnSc4d1	velká
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
liberijský	liberijský	k2eAgInSc4d1	liberijský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k8xC	jako
zaměstnankyně	zaměstnankyně	k1gFnSc1	zaměstnankyně
Citibank	Citibanka	k1gFnPc2	Citibanka
a	a	k8xC	a
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
kariéra	kariéra	k1gFnSc1	kariéra
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
vedení	vedení	k1gNnSc1	vedení
Rozvojového	rozvojový	k2eAgInSc2d1	rozvojový
programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
UNDP	UNDP	kA	UNDP
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
vězněna	věznit	k5eAaImNgFnS	věznit
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
Samuela	Samuel	k1gMnSc2	Samuel
Kanyon	Kanyona	k1gFnPc2	Kanyona
Doa	Doa	k1gMnSc2	Doa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
prezidentka	prezidentka	k1gFnSc1	prezidentka
chce	chtít	k5eAaImIp3nS	chtít
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
pomocí	pomoc	k1gFnSc7	pomoc
přebudovat	přebudovat	k5eAaPmF	přebudovat
liberijskou	liberijský	k2eAgFnSc4d1	liberijská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
vymazat	vymazat	k5eAaPmF	vymazat
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
dluh	dluh	k1gInSc4	dluh
3,5	[number]	k4	3,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
(	(	kIx(	(
<g/>
IMF	IMF	kA	IMF
<g/>
)	)	kIx)	)
s	s	k7c7	s
poskytováním	poskytování	k1gNnSc7	poskytování
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Pozvala	pozvat	k5eAaPmAgFnS	pozvat
také	také	k9	také
obchodní	obchodní	k2eAgFnSc4d1	obchodní
komunitu	komunita	k1gFnSc4	komunita
z	z	k7c2	z
Nigérie	Nigérie	k1gFnSc2	Nigérie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
obchodních	obchodní	k2eAgFnPc2d1	obchodní
příležitostí	příležitost	k1gFnPc2	příležitost
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dík	dík	k1gInSc1	dík
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
chránění	chránění	k1gNnSc6	chránění
míru	mír	k1gInSc2	mír
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Liberijci	Liberijce	k1gMnPc1	Liberijce
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
investují	investovat	k5eAaBmIp3nP	investovat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
liberijskou	liberijský	k2eAgFnSc4d1	liberijská
obnovu	obnova	k1gFnSc4	obnova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
jejích	její	k3xOp3gFnPc2	její
prvotních	prvotní	k2eAgFnPc2d1	prvotní
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
základních	základní	k2eAgFnPc2d1	základní
služeb	služba	k1gFnPc2	služba
jako	jako	k8xS	jako
dodávek	dodávka	k1gFnPc2	dodávka
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
elektřiny	elektřina	k1gFnSc2	elektřina
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Monrovie	Monrovia	k1gFnSc2	Monrovia
<g/>
,	,	kIx,	,
založila	založit	k5eAaPmAgFnS	založit
Ellen	Ellen	k2eAgInSc4d1	Ellen
Johnson-Sirleaf	Johnson-Sirleaf	k1gInSc4	Johnson-Sirleaf
komisi	komise	k1gFnSc4	komise
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
zločinů	zločin	k1gInPc2	zločin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
během	během	k7c2	během
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
liberijské	liberijský	k2eAgFnSc2d1	liberijská
potravinové	potravinový	k2eAgFnSc2d1	potravinová
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Požadovala	požadovat	k5eAaImAgFnS	požadovat
také	také	k9	také
po	po	k7c4	po
Nigérii	Nigérie	k1gFnSc4	Nigérie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vydala	vydat	k5eAaPmAgFnS	vydat
obviněné	obviněný	k2eAgMnPc4d1	obviněný
válečné	válečný	k2eAgMnPc4d1	válečný
zločince	zločinec	k1gMnPc4	zločinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
hlav	hlava	k1gFnPc2	hlava
státu	stát	k1gInSc2	stát
===	===	k?	===
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1856	[number]	k4	1856
–	–	k?	–
Joseph	Joseph	k1gInSc1	Joseph
Jenkins	Jenkinsa	k1gFnPc2	Jenkinsa
Roberts	Robertsa	k1gFnPc2	Robertsa
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
bezp	bezp	k1gMnSc1	bezp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1864	[number]	k4	1864
–	–	k?	–
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Allen	Allen	k1gMnSc1	Allen
Benson	Benson	k1gMnSc1	Benson
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
bezp	bezp	k1gMnSc1	bezp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1868	[number]	k4	1868
–	–	k?	–
Daniel	Daniel	k1gMnSc1	Daniel
Bashiel	Bashiel	k1gMnSc1	Bashiel
Warner	Warner	k1gMnSc1	Warner
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1870	[number]	k4	1870
–	–	k?	–
James	James	k1gInSc1	James
Spriggs	Spriggs	k1gInSc1	Spriggs
Payne	Payn	k1gInSc5	Payn
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
RP	RP	kA	RP
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1871	[number]	k4	1871
–	–	k?	–
Edward	Edward	k1gMnSc1	Edward
James	James	k1gMnSc1	James
Roye	Roy	k1gMnSc2	Roy
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1871	[number]	k4	1871
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
Benedict	Benedict	k1gMnSc1	Benedict
Dunbar	Dunbar	k1gMnSc1	Dunbar
<g/>
,	,	kIx,	,
Reginald	Reginald	k1gMnSc1	Reginald
A.	A.	kA	A.
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
,	,	kIx,	,
Amos	Amos	k1gMnSc1	Amos
Herring	Herring	k1gInSc1	Herring
–	–	k?	–
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1872	[number]	k4	1872
–	–	k?	–
James	Jamesa	k1gFnPc2	Jamesa
Skivring	Skivring	k1gInSc1	Skivring
Smith	Smith	k1gMnSc1	Smith
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1876	[number]	k4	1876
–	–	k?	–
Joseph	Joseph	k1gInSc1	Joseph
Jenkins	Jenkinsa	k1gFnPc2	Jenkinsa
Roberts	Robertsa	k1gFnPc2	Robertsa
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
RP	RP	kA	RP
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1878	[number]	k4	1878
–	–	k?	–
James	James	k1gInSc1	James
Spriggs	Spriggs	k1gInSc1	Spriggs
Payne	Payn	k1gInSc5	Payn
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
RP	RP	kA	RP
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1878	[number]	k4	1878
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1883	[number]	k4	1883
–	–	k?	–
Anthony	Anthona	k1gFnSc2	Anthona
William	William	k1gInSc1	William
Gerdner	Gerdner	k1gMnSc1	Gerdner
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1884	[number]	k4	1884
–	–	k?	–
Alfred	Alfred	k1gMnSc1	Alfred
Francis	Francis	k1gFnSc2	Francis
Russell	Russell	k1gMnSc1	Russell
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1892	[number]	k4	1892
–	–	k?	–
Hilary	Hilara	k1gFnSc2	Hilara
Richard	Richard	k1gMnSc1	Richard
Wright	Wright	k1gMnSc1	Wright
Johnson	Johnson	k1gMnSc1	Johnson
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1896	[number]	k4	1896
–	–	k?	–
Joseph	Joseph	k1gMnSc1	Joseph
James	James	k1gMnSc1	James
Cheeseman	Cheeseman	k1gMnSc1	Cheeseman
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1900	[number]	k4	1900
–	–	k?	–
William	William	k1gInSc1	William
David	David	k1gMnSc1	David
Coleman	Coleman	k1gMnSc1	Coleman
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1902	[number]	k4	1902
–	–	k?	–
Garretson	Garretson	k1gMnSc1	Garretson
Wilmot	Wilmot	k1gMnSc1	Wilmot
Gibson	Gibson	k1gMnSc1	Gibson
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1904	[number]	k4	1904
–	–	k?	–
Garretson	Garretson	k1gMnSc1	Garretson
Wilmot	Wilmot	k1gMnSc1	Wilmot
Gibson	Gibson	k1gMnSc1	Gibson
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1912	[number]	k4	1912
-	-	kIx~	-
Arthur	Arthur	k1gMnSc1	Arthur
Barclay	Barclaa	k1gFnSc2	Barclaa
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
–	–	k?	–
Daniel	Daniel	k1gMnSc1	Daniel
Edward	Edward	k1gMnSc1	Edward
Howard	Howard	k1gMnSc1	Howard
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1930	[number]	k4	1930
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
Dunbar	Dunbar	k1gMnSc1	Dunbar
Burgess	Burgessa	k1gFnPc2	Burgessa
King	King	k1gMnSc1	King
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1932	[number]	k4	1932
–	–	k?	–
Edwin	Edwin	k2eAgMnSc1d1	Edwin
James	James	k1gMnSc1	James
Barclay	Barclaa	k1gFnSc2	Barclaa
–	–	k?	–
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1944	[number]	k4	1944
–	–	k?	–
Edwin	Edwin	k2eAgMnSc1d1	Edwin
James	James	k1gMnSc1	James
Barclay	Barclaa	k1gFnSc2	Barclaa
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
–	–	k?	–
William	William	k1gInSc1	William
Vacanarat	Vacanarat	k1gMnSc1	Vacanarat
Shadrach	Shadrach	k1gMnSc1	Shadrach
Tubman	Tubman	k1gMnSc1	Tubman
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
–	–	k?	–
William	William	k1gInSc1	William
Richard	Richard	k1gMnSc1	Richard
Tolbert	Tolbert	k1gMnSc1	Tolbert
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
TWP	TWP	kA	TWP
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
–	–	k?	–
Samuel	Samuel	k1gMnSc1	Samuel
Kanyon	Kanyon	k1gMnSc1	Kanyon
Doe	Doe	k1gMnSc1	Doe
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Rady	rada	k1gFnSc2	rada
lidové	lidový	k2eAgFnSc2d1	lidová
spásy	spása	k1gFnSc2	spása
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
–	–	k?	–
Samuel	Samuel	k1gMnSc1	Samuel
Kanyon	Kanyon	k1gMnSc1	Kanyon
Doe	Doe	k1gMnSc1	Doe
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NDP	NDP	kA	NDP
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
G.	G.	kA	G.
Taylor	Taylor	k1gMnSc1	Taylor
–	–	k?	–
hlava	hlava	k1gFnSc1	hlava
prozatímní	prozatímní	k2eAgFnPc1d1	prozatímní
vlády	vláda	k1gFnPc1	vláda
<g/>
;	;	kIx,	;
Gbarnga	Gbarnga	k1gFnSc1	Gbarnga
<g/>
;	;	kIx,	;
NPFL	NPFL	kA	NPFL
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
–	–	k?	–
Amos	Amos	k1gMnSc1	Amos
Claudius	Claudius	k1gMnSc1	Claudius
Sawyer	Sawyer	k1gMnSc1	Sawyer
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
<g/>
;	;	kIx,	;
LPP	LPP	kA	LPP
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1990-1990	[number]	k4	1990-1990
–	–	k?	–
Prince	princa	k1gFnSc3	princa
Johnson	Johnson	k1gMnSc1	Johnson
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
INPF	INPF	kA	INPF
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
–	–	k?	–
David	David	k1gMnSc1	David
Nimley	Nimlea	k1gFnSc2	Nimlea
–	–	k?	–
hlava	hlava	k1gFnSc1	hlava
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
<g/>
/	/	kIx~	/
<g/>
NDP	NDP	kA	NDP
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
G.	G.	kA	G.
Taylor	Taylor	k1gMnSc1	Taylor
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NPFL	NPFL	kA	NPFL
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
–	–	k?	–
Amos	Amos	k1gMnSc1	Amos
Claudius	Claudius	k1gMnSc1	Claudius
Sawyer	Sawyer	k1gMnSc1	Sawyer
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
<g/>
;	;	kIx,	;
LPP	LPP	kA	LPP
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
–	–	k?	–
David	David	k1gMnSc1	David
Donald	Donald	k1gMnSc1	Donald
Kpormakor	Kpormakor	k1gMnSc1	Kpormakor
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
státní	státní	k2eAgMnSc1d1	státní
rady	rada	k1gFnPc4	rada
<g/>
;	;	kIx,	;
bezp	bezp	k1gMnSc1	bezp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
–	–	k?	–
Wilton	Wilton	k1gInSc1	Wilton
G.	G.	kA	G.
S.	S.	kA	S.
Sankawulo	Sankawula	k1gFnSc5	Sankawula
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
státní	státní	k2eAgMnSc1d1	státní
rady	rada	k1gFnPc4	rada
<g/>
;	;	kIx,	;
bezp	bezp	k1gMnSc1	bezp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
–	–	k?	–
Ruth	Ruth	k1gFnPc2	Ruth
Sando	Sando	k1gNnSc4	Sando
Perryová	Perryová	k1gFnSc1	Perryová
–	–	k?	–
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
<g/>
;	;	kIx,	;
NDP	NDP	kA	NDP
<g/>
/	/	kIx~	/
<g/>
bezp	bezp	k1gMnSc1	bezp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
G.	G.	kA	G.
Taylor	Taylor	k1gMnSc1	Taylor
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NPP	NPP	kA	NPP
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
–	–	k?	–
Moses	Moses	k1gMnSc1	Moses
Zeh	Zeh	k1gMnSc1	Zeh
Blah	blaho	k1gNnPc2	blaho
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NPP	NPP	kA	NPP
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
Gyude	Gyud	k1gInSc5	Gyud
Bryant	Bryant	k1gMnSc1	Bryant
–	–	k?	–
předseda	předseda	k1gMnSc1	předseda
Přechodné	přechodný	k2eAgFnSc2d1	přechodná
národní	národní	k2eAgFnSc2d1	národní
vlády	vláda	k1gFnSc2	vláda
<g/>
;	;	kIx,	;
LAP	lap	k1gInSc1	lap
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
–	–	k?	–
Ellen	Ellna	k1gFnPc2	Ellna
Johnson-Sirleafová	Johnson-Sirleafový	k2eAgFnSc1d1	Johnson-Sirleafová
–	–	k?	–
prezidentka	prezidentka	k1gFnSc1	prezidentka
<g/>
;	;	kIx,	;
UPod	UPod	k1gInSc1	UPod
22	[number]	k4	22
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
–	–	k?	–
George	George	k1gNnSc1	George
Weah	Weah	k1gMnSc1	Weah
–	–	k?	–
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
CDC	CDC	kA	CDC
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
má	mít	k5eAaImIp3nS	mít
64	[number]	k4	64
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
Senát	senát	k1gInSc1	senát
30	[number]	k4	30
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Weah	Weah	k1gMnSc1	Weah
(	(	kIx(	(
<g/>
CDC	CDC	kA	CDC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
od	od	k7c2	od
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
konaných	konaný	k2eAgFnPc2d1	konaná
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
je	být	k5eAaImIp3nS	být
Jewel	Jewel	k1gMnSc1	Jewel
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
vládnoucím	vládnoucí	k2eAgMnSc7d1	vládnoucí
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Libérie	Libérie	k1gFnSc2	Libérie
byl	být	k5eAaImAgMnS	být
William	William	k1gInSc4	William
Tubman	Tubman	k1gMnSc1	Tubman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Nejkratší	krátký	k2eAgFnSc4d3	nejkratší
dobu	doba	k1gFnSc4	doba
pak	pak	k6eAd1	pak
vládl	vládnout	k5eAaImAgInS	vládnout
James	James	k1gInSc1	James
Skivring	Skivring	k1gInSc1	Skivring
Smith	Smith	k1gInSc4	Smith
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
moc	moc	k6eAd1	moc
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
široce	široko	k6eAd1	široko
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
korupci	korupce	k1gFnSc4	korupce
byl	být	k5eAaImAgInS	být
politický	politický	k2eAgInSc1d1	politický
proces	proces	k1gInSc1	proces
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Libérie	Libérie	k1gFnSc2	Libérie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc1d1	stabilní
až	až	k9	až
do	do	k7c2	do
vojenského	vojenský	k2eAgInSc2d1	vojenský
převratu	převrat	k1gInSc2	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
organizacích	organizace	k1gFnPc6	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
mnoha	mnoho	k4c2	mnoho
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
:	:	kIx,	:
ACP	ACP	kA	ACP
<g/>
,	,	kIx,	,
AfDB	AfDB	k1gFnSc1	AfDB
<g/>
,	,	kIx,	,
CCC	CCC	kA	CCC
<g/>
,	,	kIx,	,
ECA	ECA	kA	ECA
<g/>
,	,	kIx,	,
ECOWAS	ECOWAS	kA	ECOWAS
<g/>
,	,	kIx,	,
FAO	FAO	kA	FAO
<g/>
,	,	kIx,	,
G-	G-	k1gFnSc1	G-
<g/>
77	[number]	k4	77
<g/>
,	,	kIx,	,
IAEA	IAEA	kA	IAEA
<g/>
,	,	kIx,	,
IBRD	IBRD	kA	IBRD
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
,	,	kIx,	,
ICFTU	ICFTU	kA	ICFTU
<g/>
,	,	kIx,	,
ICRM	ICRM	kA	ICRM
<g/>
,	,	kIx,	,
IDA	ido	k1gNnSc2	ido
<g/>
,	,	kIx,	,
IFAD	IFAD	kA	IFAD
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
IFC	IFC	kA	IFC
<g/>
,	,	kIx,	,
IFRCS	IFRCS	kA	IFRCS
<g/>
,	,	kIx,	,
ILO	ILO	kA	ILO
<g/>
,	,	kIx,	,
IMF	IMF	kA	IMF
<g/>
,	,	kIx,	,
IMO	IMO	kA	IMO
<g/>
,	,	kIx,	,
Inmarsat	Inmarsat	k1gFnSc1	Inmarsat
<g/>
,	,	kIx,	,
Intelsat	Intelsat	k1gFnSc1	Intelsat
<g/>
,	,	kIx,	,
Interpol	interpol	k1gInSc1	interpol
<g/>
,	,	kIx,	,
IOC	IOC	kA	IOC
<g/>
,	,	kIx,	,
IOM	IOM	kA	IOM
<g/>
,	,	kIx,	,
ITU	ITU	kA	ITU
<g/>
,	,	kIx,	,
NAM	NAM	kA	NAM
<g/>
,	,	kIx,	,
OAU	OAU	kA	OAU
<g/>
,	,	kIx,	,
OPCW	OPCW	kA	OPCW
<g/>
,	,	kIx,	,
UN	UN	kA	UN
<g/>
,	,	kIx,	,
UNCTAD	UNCTAD	kA	UNCTAD
<g/>
,	,	kIx,	,
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
UNIDO	UNIDO	kA	UNIDO
<g/>
,	,	kIx,	,
UPU	UPU	kA	UPU
<g/>
,	,	kIx,	,
WCL	WCL	kA	WCL
<g/>
,	,	kIx,	,
WFTU	WFTU	kA	WFTU
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
WIPO	WIPO	kA	WIPO
<g/>
,	,	kIx,	,
WMO	WMO	kA	WMO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
===	===	k?	===
</s>
</p>
<p>
<s>
Aliance	aliance	k1gFnSc1	aliance
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
(	(	kIx(	(
<g/>
APD	APD	kA	APD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koalice	koalice	k1gFnSc1	koalice
pro	pro	k7c4	pro
transformaci	transformace	k1gFnSc4	transformace
Libérie	Libérie	k1gFnSc2	Libérie
(	(	kIx(	(
<g/>
COTOL	COTOL	kA	COTOL
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kongres	kongres	k1gInSc1	kongres
pro	pro	k7c4	pro
demokratické	demokratický	k2eAgFnPc4d1	demokratická
změny	změna	k1gFnPc4	změna
(	(	kIx(	(
<g/>
CDC	CDC	kA	CDC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Liberijská	liberijský	k2eAgFnSc1d1	liberijská
strana	strana	k1gFnSc1	strana
akce	akce	k1gFnSc1	akce
(	(	kIx(	(
<g/>
LAP	lap	k1gInSc1	lap
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
LP	LP	kA	LP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
patriotická	patriotický	k2eAgFnSc1d1	patriotická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
NPP	NPP	kA	NPP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
jednoty	jednota	k1gFnSc2	jednota
(	(	kIx(	(
<g/>
UP	UP	kA	UP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Správní	správní	k2eAgNnSc4d1	správní
členění	členění	k1gNnSc4	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
15	[number]	k4	15
regionů	region	k1gInPc2	region
<g/>
:	:	kIx,	:
Bomi	Bomi	k1gNnSc1	Bomi
<g/>
,	,	kIx,	,
Bong	bongo	k1gNnPc2	bongo
<g/>
,	,	kIx,	,
Gbarpolu	Gbarpol	k1gInSc2	Gbarpol
<g/>
,	,	kIx,	,
Gran	Gran	k1gMnSc1	Gran
Bassa	Bassa	k?	Bassa
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Cape	capat	k5eAaImIp3nS	capat
Mount	Mount	k1gMnSc1	Mount
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Gedeh	Gedeh	k1gMnSc1	Gedeh
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Kru	kra	k1gFnSc4	kra
<g/>
,	,	kIx,	,
Lofa	Lofa	k1gFnSc1	Lofa
<g/>
,	,	kIx,	,
Margibi	Margibi	k1gNnSc1	Margibi
<g/>
,	,	kIx,	,
Maryland	Maryland	k1gInSc1	Maryland
<g/>
,	,	kIx,	,
Montserrado	Montserrada	k1gFnSc5	Montserrada
<g/>
,	,	kIx,	,
Nimba	Nimba	k1gMnSc1	Nimba
<g/>
,	,	kIx,	,
Rivercess	Rivercess	k1gInSc1	Rivercess
<g/>
,	,	kIx,	,
River	River	k1gMnSc1	River
Gee	Gee	k1gMnSc1	Gee
<g/>
,	,	kIx,	,
Sinoe	Sinoe	k1gFnSc1	Sinoe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Monrovia	Monrovia	k1gFnSc1	Monrovia
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
-	-	kIx~	-
1,3	[number]	k4	1,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Harper	Harper	k1gMnSc1	Harper
</s>
</p>
<p>
<s>
Greenville	Greenville	k6eAd1	Greenville
</s>
</p>
<p>
<s>
Buchanan	Buchanan	k1gMnSc1	Buchanan
</s>
</p>
<p>
<s>
Robertsport	Robertsport	k1gInSc1	Robertsport
</s>
</p>
<p>
<s>
Gbanga	Gbanga	k1gFnSc1	Gbanga
</s>
</p>
<p>
<s>
Zwedru	Zwedr	k1gMnSc3	Zwedr
</s>
</p>
<p>
<s>
Harper	Harper	k1gMnSc1	Harper
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	se	k3xPyFc4	se
státy	stát	k1gInPc1	stát
Guinea	guinea	k1gFnPc2	guinea
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
a	a	k8xC	a
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
převážně	převážně	k6eAd1	převážně
písčiny	písčina	k1gFnSc2	písčina
<g/>
,	,	kIx,	,
kosy	kosa	k1gFnSc2	kosa
a	a	k8xC	a
laguny	laguna	k1gFnSc2	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Nížiny	nížina	k1gFnPc1	nížina
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
km	km	kA	km
širokém	široký	k2eAgInSc6d1	široký
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
bažiny	bažina	k1gFnPc1	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
poté	poté	k6eAd1	poté
stoupá	stoupat	k5eAaImIp3nS	stoupat
až	až	k9	až
do	do	k7c2	do
středohoří	středohoří	k1gNnSc2	středohoří
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
z	z	k7c2	z
části	část	k1gFnSc2	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
Hornoguinejskou	Hornoguinejský	k2eAgFnSc4d1	Hornoguinejský
vysočinu	vysočina	k1gFnSc4	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
hranic	hranice	k1gFnPc2	hranice
státu	stát	k1gInSc2	stát
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
zeměmi	zem	k1gFnPc7	zem
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
587	[number]	k4	587
km	km	kA	km
<g/>
:	:	kIx,	:
306	[number]	k4	306
km	km	kA	km
se	se	k3xPyFc4	se
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
,	,	kIx,	,
563	[number]	k4	563
km	km	kA	km
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Guinea	guinea	k1gFnPc2	guinea
a	a	k8xC	a
716	[number]	k4	716
km	km	kA	km
s	s	k7c7	s
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
579	[number]	k4	579
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
povrch	povrch	k1gInSc1	povrch
Libérie	Libérie	k1gFnSc2	Libérie
je	být	k5eAaImIp3nS	být
111	[number]	k4	111
370	[number]	k4	370
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
96	[number]	k4	96
320	[number]	k4	320
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
15	[number]	k4	15
050	[number]	k4	050
km2	km2	k4	km2
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
přírodní	přírodní	k2eAgInPc4d1	přírodní
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
diamanty	diamant	k1gInPc1	diamant
a	a	k8xC	a
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
hranici	hranice	k1gFnSc4	hranice
země	zem	k1gFnSc2	zem
přetíná	přetínat	k5eAaImIp3nS	přetínat
řeka	řeka	k1gFnSc1	řeka
Mano	mana	k1gFnSc5	mana
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
řekou	řeka	k1gFnSc7	řeka
Cavally	Cavalla	k1gFnSc2	Cavalla
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
největší	veliký	k2eAgFnPc1d3	veliký
řeky	řeka	k1gFnPc1	řeka
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
jsou	být	k5eAaImIp3nP	být
St.	st.	kA	st.
Paul	Paula	k1gFnPc2	Paula
pramenící	pramenící	k2eAgFnSc1d1	pramenící
poblíž	poblíž	k7c2	poblíž
Monrovie	Monrovia	k1gFnSc2	Monrovia
<g/>
,	,	kIx,	,
St.	st.	kA	st.
John	John	k1gMnSc1	John
a	a	k8xC	a
Cestos	Cestos	k1gMnSc1	Cestos
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
se	se	k3xPyFc4	se
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Mount	Mount	k1gMnSc1	Mount
Wutivi	Wutiev	k1gFnSc3	Wutiev
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
1	[number]	k4	1
440	[number]	k4	440
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Horský	horský	k2eAgInSc1d1	horský
masiv	masiv	k1gInSc1	masiv
Nimba	Nimb	k1gMnSc2	Nimb
poblíž	poblíž	k7c2	poblíž
Yekepa	Yekep	k1gMnSc2	Yekep
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
vyšší	vysoký	k2eAgFnSc1d2	vyšší
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
1	[number]	k4	1
752	[number]	k4	752
metry	metr	k1gInPc4	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neleží	ležet	k5eNaImIp3nS	ležet
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
sdílí	sdílet	k5eAaImIp3nS	sdílet
hranice	hranice	k1gFnSc1	hranice
se	s	k7c7	s
státy	stát	k1gInPc7	stát
Guinea	guinea	k1gFnSc2	guinea
a	a	k8xC	a
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
jejich	jejich	k3xOp3gFnSc4	jejich
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
vrchem	vrch	k1gInSc7	vrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Rovníkové	rovníkový	k2eAgNnSc1d1	rovníkové
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
celoročně	celoročně	k6eAd1	celoročně
teplé	teplý	k2eAgNnSc1d1	teplé
s	s	k7c7	s
prudkými	prudký	k2eAgInPc7d1	prudký
dešti	dešť	k1gInPc7	dešť
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
krátkou	krátký	k2eAgFnSc7d1	krátká
dvoutýdenní	dvoutýdenní	k2eAgFnSc7d1	dvoutýdenní
přestávkou	přestávka	k1gFnSc7	přestávka
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zimních	zimní	k2eAgInPc2d1	zimní
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
března	březen	k1gInSc2	březen
vanou	vanout	k5eAaImIp3nP	vanout
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
suché	suchý	k2eAgInPc4d1	suchý
a	a	k8xC	a
prašné	prašný	k2eAgInPc4d1	prašný
větry	vítr	k1gInPc4	vítr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
obyvatelům	obyvatel	k1gMnPc3	obyvatel
mnoho	mnoho	k6eAd1	mnoho
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
i	i	k8xC	i
samotná	samotný	k2eAgFnSc1d1	samotná
společnost	společnost	k1gFnSc1	společnost
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
silně	silně	k6eAd1	silně
poznamenaná	poznamenaný	k2eAgFnSc1d1	poznamenaná
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
byla	být	k5eAaImAgFnS	být
takřka	takřka	k6eAd1	takřka
naprosto	naprosto	k6eAd1	naprosto
zničena	zničit	k5eAaPmNgFnS	zničit
veškerá	veškerý	k3xTgFnSc1	veškerý
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
nové	nový	k2eAgFnSc2d1	nová
liberijské	liberijský	k2eAgFnSc2d1	liberijská
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
především	především	k9	především
na	na	k7c4	na
zlepšování	zlepšování	k1gNnSc4	zlepšování
negativního	negativní	k2eAgInSc2d1	negativní
obrazu	obraz	k1gInSc2	obraz
země	zem	k1gFnSc2	zem
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
a	a	k8xC	a
přilákání	přilákání	k1gNnSc6	přilákání
co	co	k9	co
největšího	veliký	k2eAgNnSc2d3	veliký
množství	množství	k1gNnSc2	množství
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
a	a	k8xC	a
rozvojových	rozvojový	k2eAgInPc2d1	rozvojový
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
roce	rok	k1gInSc6	rok
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vláda	vláda	k1gFnSc1	vláda
několika	několik	k4yIc2	několik
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
rostlo	růst	k5eAaImAgNnS	růst
7,8	[number]	k4	7,8
%	%	kIx~	%
a	a	k8xC	a
díky	díky	k7c3	díky
četným	četný	k2eAgFnPc3d1	četná
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
cestám	cesta	k1gFnPc3	cesta
prezidentky	prezidentka	k1gFnSc2	prezidentka
byly	být	k5eAaImAgInP	být
navázány	navázán	k2eAgInPc1d1	navázán
aktivní	aktivní	k2eAgInPc1d1	aktivní
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
donory	donor	k1gInPc7	donor
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
EU	EU	kA	EU
<g/>
,	,	kIx,	,
WB	WB	kA	WB
<g/>
,	,	kIx,	,
IMF	IMF	kA	IMF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vládě	Vláďa	k1gFnSc3	Vláďa
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uzavřít	uzavřít	k5eAaPmF	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
s	s	k7c7	s
koncernem	koncern	k1gInSc7	koncern
Mittal	Mittal	k1gMnSc1	Mittal
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
embargo	embargo	k1gNnSc1	embargo
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
diamantů	diamant	k1gInPc2	diamant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
podílí	podílet	k5eAaImIp3nS	podílet
necelými	celý	k2eNgNnPc7d1	necelé
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
malý	malý	k2eAgInSc1d1	malý
výrobní	výrobní	k2eAgInSc1d1	výrobní
sektor	sektor	k1gInSc1	sektor
(	(	kIx(	(
<g/>
cement	cement	k1gInSc1	cement
<g/>
,	,	kIx,	,
nápoje	nápoj	k1gInPc1	nápoj
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trpící	trpící	k2eAgFnSc1d1	trpící
nedostatkem	nedostatek	k1gInSc7	nedostatek
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
konkurencí	konkurence	k1gFnSc7	konkurence
levných	levný	k2eAgInPc2d1	levný
dovozů	dovoz	k1gInPc2	dovoz
<g/>
.	.	kIx.	.
</s>
<s>
Těžební	těžební	k2eAgInSc1d1	těžební
průmysl	průmysl	k1gInSc1	průmysl
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nevzpamatoval	vzpamatovat	k5eNaPmAgMnS	vzpamatovat
z	z	k7c2	z
následků	následek	k1gInPc2	následek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
páteří	páteř	k1gFnSc7	páteř
liberijské	liberijský	k2eAgFnSc2d1	liberijská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
sektor	sektor	k1gInSc1	sektor
podílí	podílet	k5eAaImIp3nS	podílet
70	[number]	k4	70
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
však	však	k9	však
až	až	k9	až
75	[number]	k4	75
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
pěstovanými	pěstovaný	k2eAgFnPc7d1	pěstovaná
plodinami	plodina	k1gFnPc7	plodina
jsou	být	k5eAaImIp3nP	být
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
kasava	kasava	k1gFnSc1	kasava
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
a	a	k8xC	a
kakao	kakao	k1gNnSc1	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
potravinově	potravinově	k6eAd1	potravinově
nesoběstačná	soběstačný	k2eNgFnSc1d1	nesoběstačná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
dovozu	dovoz	k1gInSc6	dovoz
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
Libérie	Libérie	k1gFnSc1	Libérie
významným	významný	k2eAgMnSc7d1	významný
pěstitelem	pěstitel	k1gMnSc7	pěstitel
kaučukovníku	kaučukovník	k1gInSc2	kaučukovník
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc4d3	veliký
plantáž	plantáž	k1gFnSc4	plantáž
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
patří	patřit	k5eAaImIp3nS	patřit
koncernu	koncern	k1gInSc2	koncern
Firestone	Fireston	k1gInSc5	Fireston
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
Libérie	Libérie	k1gFnSc2	Libérie
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k6eAd1	kolem
92	[number]	k4	92
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
velké	velký	k2eAgFnPc4d1	velká
zásoby	zásoba	k1gFnPc4	zásoba
tropického	tropický	k2eAgNnSc2d1	tropické
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Služby	služba	k1gFnPc1	služba
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
26	[number]	k4	26
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Panamě	Panama	k1gFnSc6	Panama
druhou	druhý	k4xOgFnSc7	druhý
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
zemí	zem	k1gFnSc7	zem
poskytující	poskytující	k2eAgFnSc1d1	poskytující
svoji	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
cizím	cizí	k2eAgFnPc3d1	cizí
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
liberijskou	liberijský	k2eAgFnSc7d1	liberijská
vlajkou	vlajka	k1gFnSc7	vlajka
plaví	plavit	k5eAaImIp3nS	plavit
1683	[number]	k4	1683
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
tonáží	tonáž	k1gFnSc7	tonáž
70	[number]	k4	70
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Poplatky	poplatek	k1gInPc1	poplatek
za	za	k7c4	za
registraci	registrace	k1gFnSc4	registrace
jsou	být	k5eAaImIp3nP	být
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
vládních	vládní	k2eAgInPc2d1	vládní
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
kolem	kolem	k7c2	kolem
10	[number]	k4	10
600	[number]	k4	600
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
660	[number]	k4	660
km	km	kA	km
asfaltovaných	asfaltovaný	k2eAgFnPc2d1	asfaltovaná
–	–	k?	–
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
490	[number]	k4	490
km	km	kA	km
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Monrovii	Monrovia	k1gFnSc6	Monrovia
má	mít	k5eAaImIp3nS	mít
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Bruselem	Brusel	k1gInSc7	Brusel
(	(	kIx(	(
<g/>
Brussels	Brussels	k1gInSc1	Brussels
Air	Air	k1gFnSc2	Air
<g/>
)	)	kIx)	)
a	a	k8xC	a
okolními	okolní	k2eAgInPc7d1	okolní
západoafrickými	západoafrický	k2eAgInPc7d1	západoafrický
státy	stát	k1gInPc7	stát
(	(	kIx(	(
<g/>
Bellview	Bellview	k1gMnSc1	Bellview
<g/>
,	,	kIx,	,
Slokair	Slokair	k1gMnSc1	Slokair
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgInPc1d1	námořní
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Monrovia	Monrovia	k1gFnSc1	Monrovia
<g/>
,	,	kIx,	,
Buchanan	Buchanan	k1gInSc1	Buchanan
<g/>
,	,	kIx,	,
Greensville	Greensville	k1gInSc1	Greensville
<g/>
,	,	kIx,	,
Harper	Harper	k1gInSc1	Harper
a	a	k8xC	a
Robertsport	Robertsport	k1gInSc1	Robertsport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
3	[number]	k4	3
334	[number]	k4	334
587	[number]	k4	587
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populační	populační	k2eAgNnSc1d1	populační
tempo	tempo	k1gNnSc1	tempo
růstu	růst	k1gInSc2	růst
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
3,661	[number]	k4	3,661
<g/>
%	%	kIx~	%
</s>
</p>
<p>
<s>
===	===	k?	===
Věková	věkový	k2eAgFnSc1d1	věková
struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
0-14	[number]	k4	0-14
let	let	k1gInSc1	let
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
44	[number]	k4	44
%	%	kIx~	%
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
734	[number]	k4	734
375	[number]	k4	375
<g/>
;	;	kIx,	;
ženy	žena	k1gFnSc2	žena
731	[number]	k4	731
287	[number]	k4	287
<g/>
)	)	kIx)	)
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
53,3	[number]	k4	53,3
%	%	kIx~	%
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
879	[number]	k4	879
848	[number]	k4	848
<g/>
;	;	kIx,	;
ženy	žena	k1gFnSc2	žena
896	[number]	k4	896
319	[number]	k4	319
<g/>
)	)	kIx)	)
<g/>
65	[number]	k4	65
let	léto	k1gNnPc2	léto
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2,8	[number]	k4	2,8
%	%	kIx~	%
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
45	[number]	k4	45
175	[number]	k4	175
<g/>
;	;	kIx,	;
ženy	žena	k1gFnSc2	žena
47	[number]	k4	47
583	[number]	k4	583
<g/>
)	)	kIx)	)
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
===	===	k?	===
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
===	===	k?	===
</s>
</p>
<p>
<s>
celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
41,13	[number]	k4	41,13
letmuži	letmuzat	k5eAaPmIp1nS	letmuzat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
39,85	[number]	k4	39,85
letženy	letžen	k2eAgFnPc4d1	letžen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
42,46	[number]	k4	42,46
let	let	k1gInSc1	let
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
je	být	k5eAaImIp3nS	být
etnicky	etnicky	k6eAd1	etnicky
neobyčejně	obyčejně	k6eNd1	obyčejně
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
Kpellové	Kpell	k1gMnPc1	Kpell
(	(	kIx(	(
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc1d1	další
etnika	etnikum	k1gNnPc1	etnikum
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
méně	málo	k6eAd2	málo
četná	četný	k2eAgFnSc1d1	četná
<g/>
:	:	kIx,	:
Bassové	Bassové	k2eAgFnSc1d1	Bassové
(	(	kIx(	(
<g/>
asi	asi	k9	asi
14	[number]	k4	14
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Danové	Dan	k1gMnPc1	Dan
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kruové	Kruové	k2eAgFnSc1d1	Kruové
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
desítky	desítka	k1gFnPc1	desítka
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Amerikanoliberijci	Amerikanoliberijce	k1gMnPc1	Amerikanoliberijce
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
hlavními	hlavní	k2eAgInPc7d1	hlavní
užívanými	užívaný	k2eAgInPc7d1	užívaný
jazyky	jazyk	k1gInPc7	jazyk
jsou	být	k5eAaImIp3nP	být
kpelština	kpelština	k1gFnSc1	kpelština
(	(	kIx(	(
<g/>
kpelle	kpelle	k1gInSc1	kpelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
golla	golla	k1gFnSc1	golla
<g/>
,	,	kIx,	,
mande	mande	k6eAd1	mande
<g/>
,	,	kIx,	,
kru	kra	k1gFnSc4	kra
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
je	být	k5eAaImIp3nS	být
také	také	k9	také
početná	početný	k2eAgFnSc1d1	početná
skupina	skupina	k1gFnSc1	skupina
Libanonců	Libanonec	k1gMnPc2	Libanonec
<g/>
,	,	kIx,	,
Indů	Ind	k1gMnPc2	Ind
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
národů	národ	k1gInPc2	národ
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
liberijské	liberijský	k2eAgFnSc2d1	liberijská
obchodní	obchodní	k2eAgFnSc2d1	obchodní
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojenými	spojený	k2eAgInPc7d1	spojený
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
,	,	kIx,	,
situovaný	situovaný	k2eAgInSc1d1	situovaný
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
Monrovie	Monrovia	k1gFnSc2	Monrovia
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
bezprostředního	bezprostřední	k2eAgNnSc2d1	bezprostřední
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Liberijská	liberijský	k2eAgFnSc1d1	liberijská
ústava	ústava	k1gFnSc1	ústava
omezuje	omezovat	k5eAaImIp3nS	omezovat
udělování	udělování	k1gNnSc4	udělování
občanství	občanství	k1gNnSc2	občanství
pouze	pouze	k6eAd1	pouze
lidem	lid	k1gInSc7	lid
s	s	k7c7	s
africkým	africký	k2eAgInSc7d1	africký
původem	původ	k1gInSc7	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
věřících	věřící	k1gMnPc2	věřící
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
domorodému	domorodý	k2eAgNnSc3d1	domorodé
náboženství	náboženství	k1gNnSc3	náboženství
(	(	kIx(	(
<g/>
40	[number]	k4	40
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
křesťanství	křesťanství	k1gNnSc4	křesťanství
(	(	kIx(	(
<g/>
40	[number]	k4	40
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
5,8	[number]	k4	5,8
%	%	kIx~	%
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
<g/>
)	)	kIx)	)
a	a	k8xC	a
početnou	početný	k2eAgFnSc7d1	početná
minoritou	minorita	k1gFnSc7	minorita
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
muslimové	muslim	k1gMnPc1	muslim
(	(	kIx(	(
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
jen	jen	k9	jen
0,06	[number]	k4	0,06
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Libérie	Libérie	k1gFnSc2	Libérie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Libérie	Libérie	k1gFnSc2	Libérie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Liberia	Liberium	k1gNnPc1	Liberium
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Liberia	Liberium	k1gNnPc1	Liberium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Liberia	Liberium	k1gNnSc2	Liberium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Liberia	Liberium	k1gNnSc2	Liberium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-01	[number]	k4	2011-07-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Liberia	Liberium	k1gNnSc2	Liberium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-14	[number]	k4	2011-07-14
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Akkře	Akkra	k1gFnSc6	Akkra
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Libérie	Libérie	k1gFnSc1	Libérie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2008-10-15	[number]	k4	2008-10-15
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOLSOE	HOLSOE	kA	HOLSOE
<g/>
,	,	kIx,	,
Svend	Svend	k1gInSc1	Svend
E	E	kA	E
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Liberia	Liberium	k1gNnPc1	Liberium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
