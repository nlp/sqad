<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Řehoř	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
asi	asi	k9	asi
540	[number]	k4	540
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
604	[number]	k4	604
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
Řehoř	Řehoř	k1gMnSc1	Řehoř
Dvojeslov	Dvojeslov	k1gInSc1	Dvojeslov
(	(	kIx(	(
<g/>
Dialogist	Dialogist	k1gInSc1	Dialogist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
64	[number]	k4	64
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
590	[number]	k4	590
–	–	k?	–
604	[number]	k4	604
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
