<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Řehoř	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
asi	asi	k9	asi
540	[number]	k4	540
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
604	[number]	k4	604
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
Řehoř	Řehoř	k1gMnSc1	Řehoř
Dvojeslov	Dvojeslov	k1gInSc1	Dvojeslov
(	(	kIx(	(
<g/>
Dialogist	Dialogist	k1gInSc1	Dialogist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
64	[number]	k4	64
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
590	[number]	k4	590
–	–	k?	–
604	[number]	k4	604
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
služebník	služebník	k1gMnSc1	služebník
služebníků	služebník	k1gMnPc2	služebník
Božích	boží	k2eAgMnPc2d1	boží
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
servus	servus	k?	servus
servorum	servorum	k1gInSc1	servorum
Dei	Dei	k1gFnSc1	Dei
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
světské	světský	k2eAgFnSc2d1	světská
moci	moc	k1gFnSc2	moc
papežské	papežský	k2eAgFnSc2d1	Papežská
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
duchovní	duchovní	k2eAgFnSc2d1	duchovní
funkce	funkce	k1gFnSc2	funkce
byl	být	k5eAaImAgInS	být
také	také	k9	také
známým	známý	k1gMnSc7	známý
církevním	církevní	k2eAgMnSc7d1	církevní
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
autorem	autor	k1gMnSc7	autor
mnoha	mnoho	k4c2	mnoho
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Řehoř	Řehoř	k1gMnSc1	Řehoř
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
významné	významný	k2eAgFnSc2d1	významná
patricijské	patricijský	k2eAgFnSc2d1	patricijská
rodiny	rodina	k1gFnSc2	rodina
Annici	Annice	k1gFnSc4	Annice
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
570	[number]	k4	570
zastával	zastávat	k5eAaImAgMnS	zastávat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
úřad	úřad	k1gInSc1	úřad
praetora	praetor	k1gMnSc2	praetor
<g/>
.	.	kIx.	.
</s>
<s>
Disponoval	disponovat	k5eAaBmAgMnS	disponovat
tak	tak	k9	tak
neobyčejnou	obyčejný	k2eNgFnSc7d1	neobyčejná
světskou	světský	k2eAgFnSc7d1	světská
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Rozčarován	rozčarován	k2eAgInSc1d1	rozčarován
římskou	římský	k2eAgFnSc7d1	římská
politikou	politika	k1gFnSc7	politika
však	však	k9	však
brzy	brzy	k6eAd1	brzy
úřad	úřad	k1gInSc1	úřad
opustil	opustit	k5eAaPmAgInS	opustit
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
církevní	církevní	k2eAgFnSc4d1	církevní
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
roku	rok	k1gInSc2	rok
575	[number]	k4	575
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velkým	velký	k2eAgMnSc7d1	velký
propagátorem	propagátor	k1gMnSc7	propagátor
benediktinské	benediktinský	k2eAgFnSc2d1	benediktinská
řehole	řehole	k1gFnSc2	řehole
a	a	k8xC	a
zakládal	zakládat	k5eAaImAgMnS	zakládat
kláštery	klášter	k1gInPc7	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
rodinných	rodinný	k2eAgInPc6d1	rodinný
statcích	statek	k1gInPc6	statek
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
v	v	k7c6	v
poté	poté	k6eAd1	poté
v	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
domě	dům	k1gInSc6	dům
Monte	Mont	k1gInSc5	Mont
Celio	Celio	k6eAd1	Celio
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
577	[number]	k4	577
jej	on	k3xPp3gInSc4	on
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
I.	I.	kA	I.
pověřil	pověřit	k5eAaPmAgInS	pověřit
prací	práce	k1gFnSc7	práce
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
chudinou	chudina	k1gFnSc7	chudina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
579	[number]	k4	579
jej	on	k3xPp3gInSc4	on
papež	papež	k1gMnSc1	papež
Pelagius	Pelagius	k1gMnSc1	Pelagius
II	II	kA	II
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
svým	svůj	k3xOyFgInSc7	svůj
legátem	legát	k1gInSc7	legát
u	u	k7c2	u
císařského	císařský	k2eAgInSc2d1	císařský
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
roku	rok	k1gInSc2	rok
586	[number]	k4	586
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
jáhen	jáhen	k1gMnSc1	jáhen
v	v	k7c6	v
papežské	papežský	k2eAgFnSc6d1	Papežská
kanceláři	kancelář	k1gFnSc6	kancelář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Papežem	Papež	k1gMnSc7	Papež
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Okolnosti	okolnost	k1gFnSc6	okolnost
zvolení	zvolení	k1gNnSc4	zvolení
===	===	k?	===
</s>
</p>
<p>
<s>
Papežem	Papež	k1gMnSc7	Papež
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
–	–	k?	–
dle	dle	k7c2	dle
tradice	tradice	k1gFnSc2	tradice
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
–	–	k?	–
roku	rok	k1gInSc2	rok
590	[number]	k4	590
během	během	k7c2	během
dohasínající	dohasínající	k2eAgFnSc2d1	dohasínající
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
padl	padnout	k5eAaPmAgMnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
i	i	k8xC	i
sám	sám	k3xTgMnSc1	sám
papež	papež	k1gMnSc1	papež
Pelagius	Pelagius	k1gMnSc1	Pelagius
<g/>
.	.	kIx.	.
</s>
<s>
Řehoř	Řehoř	k1gMnSc1	Řehoř
údajně	údajně	k6eAd1	údajně
poslal	poslat	k5eAaPmAgMnS	poslat
byzantskému	byzantský	k2eAgMnSc3d1	byzantský
císaři	císař	k1gMnSc3	císař
Maurikiovi	Maurikius	k1gMnSc3	Maurikius
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
stvrdit	stvrdit	k5eAaPmF	stvrdit
podpisem	podpis	k1gInSc7	podpis
volbu	volba	k1gFnSc4	volba
jeho	jeho	k3xOp3gInSc2	jeho
pontifikátu	pontifikát	k1gInSc2	pontifikát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
psaní	psaní	k1gNnSc1	psaní
se	se	k3xPyFc4	se
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
adresáta	adresát	k1gMnSc2	adresát
nedostalo	dostat	k5eNaPmAgNnS	dostat
včas	včas	k6eAd1	včas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
činnost	činnost	k1gFnSc1	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Řehoř	Řehoř	k1gMnSc1	Řehoř
navázal	navázat	k5eAaPmAgMnS	navázat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
579	[number]	k4	579
–	–	k?	–
586	[number]	k4	586
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
vlivnými	vlivný	k2eAgFnPc7d1	vlivná
osobnostmi	osobnost	k1gFnPc7	osobnost
a	a	k8xC	a
osvojil	osvojit	k5eAaPmAgMnS	osvojit
si	se	k3xPyFc3	se
taktiky	taktika	k1gFnSc2	taktika
politického	politický	k2eAgInSc2d1	politický
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
nabytých	nabytý	k2eAgFnPc2d1	nabytá
vědomostí	vědomost	k1gFnPc2	vědomost
schopen	schopen	k2eAgMnSc1d1	schopen
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
odvrátit	odvrátit	k5eAaPmF	odvrátit
útoky	útok	k1gInPc4	útok
expandujících	expandující	k2eAgInPc2d1	expandující
Langobardů	Langobard	k1gInPc2	Langobard
<g/>
.	.	kIx.	.
</s>
<s>
Vynaložil	vynaložit	k5eAaPmAgMnS	vynaložit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
úsilí	úsilí	k1gNnSc4	úsilí
při	při	k7c6	při
mírových	mírový	k2eAgNnPc6d1	Mírové
jednáních	jednání	k1gNnPc6	jednání
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
nechal	nechat	k5eAaPmAgInS	nechat
opevnit	opevnit	k5eAaPmF	opevnit
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
nejvíce	nejvíce	k6eAd1	nejvíce
ohrožena	ohrožen	k2eAgNnPc1d1	ohroženo
jejich	jejich	k3xOp3gInPc7	jejich
vpády	vpád	k1gInPc7	vpád
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jádro	jádro	k1gNnSc1	jádro
budoucího	budoucí	k2eAgInSc2d1	budoucí
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tříleté	tříletý	k2eAgNnSc4d1	tříleté
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vyjednáno	vyjednat	k5eAaPmNgNnS	vyjednat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
584	[number]	k4	584
<g/>
,	,	kIx,	,
dodrženo	dodržen	k2eAgNnSc1d1	dodrženo
nebylo	být	k5eNaImAgNnS	být
a	a	k8xC	a
na	na	k7c6	na
Řehořovi	Řehoř	k1gMnSc6	Řehoř
spočíval	spočívat	k5eAaImAgInS	spočívat
nelehký	lehký	k2eNgInSc1d1	nelehký
úkol	úkol	k1gInSc1	úkol
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
byzantského	byzantský	k2eAgMnSc4d1	byzantský
císaře	císař	k1gMnSc4	císař
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohl	pomoct	k5eAaPmAgInS	pomoct
bránit	bránit	k5eAaImF	bránit
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchu	úspěch	k1gInSc2	úspěch
ale	ale	k9	ale
nedocílil	docílit	k5eNaPmAgInS	docílit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
papežem	papež	k1gMnSc7	papež
Pelagiem	Pelagius	k1gMnSc7	Pelagius
II	II	kA	II
<g/>
.	.	kIx.	.
odvolán	odvolat	k5eAaPmNgMnS	odvolat
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
nuncia	nuncius	k1gMnSc2	nuncius
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
arcijáhnem	arcijáhen	k1gMnSc7	arcijáhen
Lorenzem	Lorenz	k1gMnSc7	Lorenz
<g/>
.	.	kIx.	.
<g/>
Mír	mír	k1gInSc4	mír
s	s	k7c7	s
langobardským	langobardský	k2eAgMnSc7d1	langobardský
králem	král	k1gMnSc7	král
Agilulfem	Agilulf	k1gMnSc7	Agilulf
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vykoupit	vykoupit	k5eAaPmF	vykoupit
značným	značný	k2eAgNnSc7d1	značné
množstvím	množství	k1gNnSc7	množství
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
roce	rok	k1gInSc6	rok
593	[number]	k4	593
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
až	až	k9	až
za	za	k7c2	za
pontifikátu	pontifikát	k1gInSc2	pontifikát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
přátelství	přátelství	k1gNnSc1	přátelství
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
Teolindou	Teolinda	k1gFnSc7	Teolinda
<g/>
,	,	kIx,	,
manželkou	manželka	k1gFnSc7	manželka
Agilfula	Agilfulum	k1gNnSc2	Agilfulum
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
záležitosti	záležitost	k1gFnSc6	záležitost
smíru	smír	k1gInSc2	smír
u	u	k7c2	u
krále	král	k1gMnSc2	král
přimluvit	přimluvit	k5eAaPmF	přimluvit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
596	[number]	k4	596
vyslal	vyslat	k5eAaPmAgMnS	vyslat
benediktinského	benediktinský	k2eAgMnSc4d1	benediktinský
opata	opat	k1gMnSc4	opat
Augustina	Augustin	k1gMnSc4	Augustin
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
mnichů	mnich	k1gMnPc2	mnich
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
a	a	k8xC	a
položil	položit	k5eAaPmAgInS	položit
tak	tak	k9	tak
základy	základ	k1gInPc4	základ
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
pokřesťanštění	pokřesťanštění	k1gNnSc3	pokřesťanštění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Význam	význam	k1gInSc1	význam
===	===	k?	===
</s>
</p>
<p>
<s>
Zpětně	zpětně	k6eAd1	zpětně
se	se	k3xPyFc4	se
Řehořův	Řehořův	k2eAgInSc1d1	Řehořův
úřad	úřad	k1gInSc1	úřad
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
téměř	téměř	k6eAd1	téměř
reprezentativní	reprezentativní	k2eAgInSc1d1	reprezentativní
model	model	k1gInSc1	model
výkonu	výkon	k1gInSc2	výkon
papežství	papežství	k1gNnSc2	papežství
<g/>
.	.	kIx.	.
</s>
<s>
Nezájem	nezájem	k1gInSc1	nezájem
o	o	k7c4	o
intrikářství	intrikářství	k1gNnSc4	intrikářství
kvůli	kvůli	k7c3	kvůli
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
touha	touha	k1gFnSc1	touha
řešit	řešit	k5eAaImF	řešit
spory	spor	k1gInPc4	spor
mírovou	mírový	k2eAgFnSc7d1	mírová
cestou	cesta	k1gFnSc7	cesta
byly	být	k5eAaImAgFnP	být
odrazem	odraz	k1gInSc7	odraz
snahy	snaha	k1gFnSc2	snaha
jednat	jednat	k5eAaImF	jednat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pokyny	pokyn	k1gInPc7	pokyn
benediktinské	benediktinský	k2eAgFnSc2d1	benediktinská
řehole	řehole	k1gFnSc2	řehole
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
církev	církev	k1gFnSc1	církev
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
institucí	instituce	k1gFnPc2	instituce
významně	významně	k6eAd1	významně
pečující	pečující	k2eAgMnPc1d1	pečující
o	o	k7c4	o
nemocné	mocný	k2eNgMnPc4d1	nemocný
a	a	k8xC	a
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
každého	každý	k3xTgInSc2	každý
měsíce	měsíc	k1gInSc2	měsíc
rozděloval	rozdělovat	k5eAaImAgInS	rozdělovat
část	část	k1gFnSc4	část
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
mezi	mezi	k7c4	mezi
lid	lid	k1gInSc4	lid
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
doba	doba	k1gFnSc1	doba
nesnází	nesnáz	k1gFnPc2	nesnáz
<g/>
,	,	kIx,	,
zatížená	zatížený	k2eAgFnSc1d1	zatížená
množstvím	množství	k1gNnSc7	množství
různých	různý	k2eAgFnPc2d1	různá
pohrom	pohroma	k1gFnPc2	pohroma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
blížícího	blížící	k2eAgMnSc2d1	blížící
se	se	k3xPyFc4	se
konce	konec	k1gInSc2	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
Reformoval	reformovat	k5eAaBmAgInS	reformovat
mešní	mešní	k2eAgFnSc4d1	mešní
liturgii	liturgie	k1gFnSc4	liturgie
(	(	kIx(	(
<g/>
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
misál	misál	k1gInSc1	misál
Sacramentarium	Sacramentarium	k1gNnSc1	Sacramentarium
Gregorianum	Gregorianum	k1gInSc1	Gregorianum
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
mnoha	mnoho	k4c2	mnoho
vylepšení	vylepšení	k1gNnPc2	vylepšení
hudební	hudební	k2eAgFnSc2d1	hudební
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
notového	notový	k2eAgInSc2d1	notový
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připisován	připisovat	k5eAaImNgInS	připisovat
vznik	vznik	k1gInSc1	vznik
liturgického	liturgický	k2eAgInSc2d1	liturgický
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
chorál	chorál	k1gInSc1	chorál
(	(	kIx(	(
<g/>
Cantus	Cantus	k1gMnSc1	Cantus
gregorianus	gregorianus	k1gMnSc1	gregorianus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jej	on	k3xPp3gMnSc4	on
zavedl	zavést	k5eAaPmAgMnS	zavést
až	až	k9	až
papež	papež	k1gMnSc1	papež
Vitalián	Vitalián	k1gMnSc1	Vitalián
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
672	[number]	k4	672
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
596	[number]	k4	596
vyslal	vyslat	k5eAaPmAgMnS	vyslat
benediktinského	benediktinský	k2eAgMnSc4d1	benediktinský
opata	opat	k1gMnSc4	opat
Augustina	Augustin	k1gMnSc4	Augustin
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
mnichů	mnich	k1gMnPc2	mnich
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
položil	položit	k5eAaPmAgInS	položit
tak	tak	k9	tak
základy	základ	k1gInPc4	základ
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
pokřesťanštění	pokřesťanštění	k1gNnSc3	pokřesťanštění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
spisy	spis	k1gInPc1	spis
mají	mít	k5eAaImIp3nP	mít
výrazně	výrazně	k6eAd1	výrazně
pastorační	pastorační	k2eAgInSc4d1	pastorační
a	a	k8xC	a
praktický	praktický	k2eAgInSc4d1	praktický
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
hluboce	hluboko	k6eAd1	hluboko
mystický	mystický	k2eAgInSc4d1	mystický
a	a	k8xC	a
pedagogický	pedagogický	k2eAgInSc4d1	pedagogický
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
učitelů	učitel	k1gMnPc2	učitel
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
světlo	světlo	k1gNnSc1	světlo
vrhá	vrhat	k5eAaImIp3nS	vrhat
na	na	k7c4	na
Řehoře	Řehoř	k1gMnPc4	Řehoř
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
duchovně	duchovně	k6eAd1	duchovně
založený	založený	k2eAgMnSc1d1	založený
papež	papež	k1gMnSc1	papež
byl	být	k5eAaImAgMnS	být
horlivým	horlivý	k2eAgMnSc7d1	horlivý
stoupencem	stoupenec	k1gMnSc7	stoupenec
a	a	k8xC	a
podporovatelem	podporovatel	k1gMnSc7	podporovatel
kultu	kult	k1gInSc2	kult
relikvií	relikvie	k1gFnSc7	relikvie
<g/>
.	.	kIx.	.
</s>
<s>
Uctívání	uctívání	k1gNnSc1	uctívání
předmětů	předmět	k1gInPc2	předmět
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
tělesných	tělesný	k2eAgInPc2d1	tělesný
ostatků	ostatek	k1gInPc2	ostatek
světců	světec	k1gMnPc2	světec
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
plodným	plodný	k2eAgInSc7d1	plodný
zdrojem	zdroj	k1gInSc7	zdroj
legendárních	legendární	k2eAgInPc2d1	legendární
příběhů	příběh	k1gInPc2	příběh
ať	ať	k8xC	ať
již	již	k9	již
o	o	k7c6	o
světcích	světec	k1gMnPc6	světec
samotných	samotný	k2eAgInPc2d1	samotný
nebo	nebo	k8xC	nebo
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
ostatky	ostatek	k1gInPc7	ostatek
měli	mít	k5eAaImAgMnP	mít
manipulovat	manipulovat	k5eAaImF	manipulovat
<g/>
.	.	kIx.	.
</s>
<s>
Uctívání	uctívání	k1gNnSc1	uctívání
relikvií	relikvie	k1gFnPc2	relikvie
však	však	k9	však
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
k	k	k7c3	k
historickým	historický	k2eAgFnPc3d1	historická
mystifikacím	mystifikace	k1gFnPc3	mystifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Mravní	mravní	k2eAgInPc1d1	mravní
výklady	výklad	k1gInPc1	výklad
knihy	kniha	k1gFnSc2	kniha
Jób	Jób	k1gFnSc3	Jób
–	–	k?	–
soubor	soubor	k1gInSc1	soubor
kázání	kázání	k1gNnSc2	kázání
a	a	k8xC	a
přednášek	přednáška	k1gFnPc2	přednáška
pro	pro	k7c4	pro
řeholníky	řeholník	k1gMnPc4	řeholník
</s>
</p>
<p>
<s>
Dialogy	dialog	k1gInPc1	dialog
–	–	k?	–
Celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc1	čtyři
knihy	kniha	k1gFnPc1	kniha
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
jako	jako	k8xS	jako
výraz	výraz	k1gInSc1	výraz
úcty	úcta	k1gFnSc2	úcta
lombardské	lombardský	k2eAgFnSc2d1	Lombardská
královně	královna	k1gFnSc3	královna
Teolindě	Teolinda	k1gFnSc3	Teolinda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
má	mít	k5eAaImIp3nS	mít
12	[number]	k4	12
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
38	[number]	k4	38
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
38	[number]	k4	38
a	a	k8xC	a
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
je	být	k5eAaImIp3nS	být
všech	všecek	k3xTgFnPc2	všecek
kapitol	kapitola	k1gFnPc2	kapitola
jako	jako	k8xC	jako
žalmů	žalm	k1gInPc2	žalm
v	v	k7c6	v
žaltáři	žaltář	k1gInSc6	žaltář
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
má	mít	k5eAaImIp3nS	mít
formu	forma	k1gFnSc4	forma
dialogu	dialog	k1gInSc2	dialog
s	s	k7c7	s
jáhnem	jáhen	k1gMnSc7	jáhen
Petrem	Petr	k1gMnSc7	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
celého	celý	k2eAgNnSc2d1	celé
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
tou	ten	k3xDgFnSc7	ten
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yRgFnSc6	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
celá	celý	k2eAgFnSc1d1	celá
druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sv.	sv.	kA	sv.
Benedikt	benedikt	k1gInSc1	benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kniha	kniha	k1gFnSc1	kniha
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
posmrtný	posmrtný	k2eAgInSc4d1	posmrtný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
a	a	k8xC	a
třetí	třetí	k4xOgFnPc1	třetí
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
dalších	další	k2eAgFnPc6d1	další
významných	významný	k2eAgFnPc6d1	významná
postavách	postava	k1gFnPc6	postava
(	(	kIx(	(
<g/>
12	[number]	k4	12
a	a	k8xC	a
37	[number]	k4	37
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
je	být	k5eAaImIp3nS	být
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
osob	osoba	k1gFnPc2	osoba
padesát	padesát	k4xCc1	padesát
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zdroje	zdroj	k1gInPc1	zdroj
svých	svůj	k3xOyFgFnPc2	svůj
vědomostí	vědomost	k1gFnPc2	vědomost
Řehoř	Řehoř	k1gMnSc1	Řehoř
uvádí	uvádět	k5eAaImIp3nS	uvádět
Konstantina	Konstantin	k1gMnSc4	Konstantin
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
opata	opat	k1gMnSc2	opat
Monte	Mont	k1gInSc5	Mont
Cassina	Cassin	k2eAgNnSc2d1	Cassino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Simplicia	simplicia	k1gNnPc1	simplicia
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
opata	opat	k1gMnSc2	opat
Monte	Mont	k1gInSc5	Mont
Cassina	Cassin	k2eAgNnSc2d1	Cassino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Valentiniána	Valentinián	k1gMnSc2	Valentinián
(	(	kIx(	(
<g/>
opata	opat	k1gMnSc2	opat
v	v	k7c6	v
Lateránu	Laterán	k1gInSc6	Laterán
<g/>
)	)	kIx)	)
a	a	k8xC	a
Honoratia	Honoratius	k1gMnSc2	Honoratius
(	(	kIx(	(
<g/>
opata	opat	k1gMnSc2	opat
v	v	k7c6	v
Subiacu	Subiacus	k1gInSc6	Subiacus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Geografický	geografický	k2eAgInSc1d1	geografický
horizont	horizont	k1gInSc1	horizont
vyprávění	vyprávění	k1gNnSc2	vyprávění
je	být	k5eAaImIp3nS	být
položen	položit	k5eAaPmNgInS	položit
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Říma	Řím	k1gInSc2	Řím
–	–	k?	–
Enfide	Enfid	k1gMnSc5	Enfid
<g/>
,	,	kIx,	,
Subiaco	Subiaca	k1gMnSc5	Subiaca
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Cassino	Cassina	k1gFnSc5	Cassina
<g/>
,	,	kIx,	,
Terracina	Terracina	k1gMnSc1	Terracina
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
života	život	k1gInSc2	život
osob	osoba	k1gFnPc2	osoba
zmiňovaných	zmiňovaný	k2eAgFnPc2d1	zmiňovaná
v	v	k7c6	v
Dialozích	dialog	k1gInPc6	dialog
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
především	především	k9	především
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc4	Benedikt
z	z	k7c2	z
Nursie	Nursie	k1gFnSc2	Nursie
–	–	k?	–
nemůžeme	moct	k5eNaImIp1nP	moct
chápat	chápat	k5eAaImF	chápat
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
historicko-biografickém	historickoiografický	k2eAgInSc6d1	historicko-biografický
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
legendistický	legendistický	k2eAgInSc4d1	legendistický
portrét	portrét	k1gInSc4	portrét
podléhající	podléhající	k2eAgFnSc2d1	podléhající
konvencím	konvence	k1gFnPc3	konvence
(	(	kIx(	(
<g/>
symbolismus	symbolismus	k1gInSc4	symbolismus
<g/>
)	)	kIx)	)
let	let	k1gInSc4	let
593	[number]	k4	593
až	až	k9	až
594	[number]	k4	594
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
knihy	kniha	k1gFnPc1	kniha
sepsány	sepsán	k2eAgFnPc1d1	sepsána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dialozích	dialog	k1gInPc6	dialog
také	také	k6eAd1	také
mnoho	mnoho	k4c4	mnoho
badatelů	badatel	k1gMnPc2	badatel
nachází	nacházet	k5eAaImIp3nS	nacházet
první	první	k4xOgFnSc4	první
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
Řeholi	řehole	k1gFnSc6	řehole
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc2	Benedikt
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
ovšem	ovšem	k9	ovšem
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Řehoř	Řehoř	k1gMnSc1	Řehoř
Benedikta	Benedikt	k1gMnSc2	Benedikt
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
"	"	kIx"	"
<g/>
řehole	řehole	k1gFnSc1	řehole
pro	pro	k7c4	pro
mnichy	mnich	k1gInPc4	mnich
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyniká	vynikat	k5eAaImIp3nS	vynikat
svou	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
umírněností	umírněnost	k1gFnSc7	umírněnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
discretio	discretio	k1gMnSc1	discretio
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
srozumitelností	srozumitelnost	k1gFnPc2	srozumitelnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
neexistuje	existovat	k5eNaImIp3nS	existovat
přímý	přímý	k2eAgInSc4d1	přímý
doklad	doklad	k1gInSc4	doklad
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tzv.	tzv.	kA	tzv.
Řehole	řehole	k1gFnSc2	řehole
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc2	Benedikt
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
první	první	k4xOgInSc1	první
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
exemplář	exemplář	k1gInSc1	exemplář
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
750	[number]	k4	750
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
řeholí	řehole	k1gFnSc7	řehole
zmiňovanou	zmiňovaný	k2eAgFnSc7d1	zmiňovaná
Řehořem	Řehoř	k1gMnSc7	Řehoř
v	v	k7c6	v
Dialozích	dialog	k1gInPc6	dialog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
užívalo	užívat	k5eAaImAgNnS	užívat
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
řeholí	řehole	k1gFnPc2	řehole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Homilie	homilie	k1gFnSc1	homilie
sv.	sv.	kA	sv.
Řehoře	Řehoř	k1gMnSc2	Řehoř
I.	I.	kA	I.
–	–	k?	–
posmrtně	posmrtně	k6eAd1	posmrtně
vydaná	vydaný	k2eAgFnSc1d1	vydaná
sbírka	sbírka	k1gFnSc1	sbírka
kázání	kázání	k1gNnSc2	kázání
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejčtenějších	čtený	k2eAgFnPc2d3	Nejčtenější
knih	kniha	k1gFnPc2	kniha
středověku	středověk	k1gInSc2	středověk
</s>
</p>
<p>
<s>
Řehořovi	Řehoř	k1gMnSc3	Řehoř
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
autorství	autorství	k1gNnSc1	autorství
Liturgie	liturgie	k1gFnSc2	liturgie
předem	předem	k6eAd1	předem
posvěcených	posvěcený	k2eAgInPc2d1	posvěcený
darů	dar	k1gInPc2	dar
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
byzantského	byzantský	k2eAgInSc2d1	byzantský
ritu	rit	k1gInSc2	rit
v	v	k7c6	v
postní	postní	k2eAgFnSc6d1	postní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ikonografie	ikonografie	k1gFnSc2	ikonografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
umění	umění	k1gNnSc6	umění
je	být	k5eAaImIp3nS	být
Řehoř	Řehoř	k1gMnSc1	Řehoř
obvykle	obvykle	k6eAd1	obvykle
zobrazován	zobrazovat	k5eAaImNgMnS	zobrazovat
v	v	k7c6	v
rouchu	roucho	k1gNnSc6	roucho
s	s	k7c7	s
diadémem	diadém	k1gInSc7	diadém
a	a	k8xC	a
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
zobrazení	zobrazení	k1gNnSc1	zobrazení
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
více	hodně	k6eAd2	hodně
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
s	s	k7c7	s
tonzurou	tonzura	k1gFnSc7	tonzura
a	a	k8xC	a
v	v	k7c6	v
prostých	prostý	k2eAgInPc6d1	prostý
šatech	šat	k1gInPc6	šat
<g/>
.	.	kIx.	.
</s>
<s>
Ortodoxní	ortodoxní	k2eAgFnPc4d1	ortodoxní
tradiční	tradiční	k2eAgFnPc4d1	tradiční
ikony	ikona	k1gFnPc4	ikona
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
svatého	svatý	k2eAgMnSc4d1	svatý
Řehoře	Řehoř	k1gMnSc4	Řehoř
jako	jako	k8xS	jako
biskupa	biskup	k1gMnSc4	biskup
držícího	držící	k2eAgInSc2d1	držící
evangelium	evangelium	k1gNnSc4	evangelium
a	a	k8xC	a
žehnajícího	žehnající	k2eAgNnSc2d1	žehnající
pravou	pravá	k1gFnSc4	pravá
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
dovolil	dovolit	k5eAaPmAgInS	dovolit
jeho	jeho	k3xOp3gNnSc4	jeho
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
se	s	k7c7	s
čtyřhrannou	čtyřhranný	k2eAgFnSc7d1	čtyřhranná
svatozáří	svatozář	k1gFnSc7	svatozář
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
krmí	krmit	k5eAaImIp3nS	krmit
chudé	chudý	k2eAgNnSc1d1	chudé
u	u	k7c2	u
svého	svůj	k3xOyFgInSc2	svůj
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
"	"	kIx"	"
<g/>
Řehořova	Řehořův	k2eAgFnSc1d1	Řehořova
mše	mše	k1gFnSc1	mše
<g/>
"	"	kIx"	"
často	často	k6eAd1	často
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
<g/>
:	:	kIx,	:
Řehoř	Řehoř	k1gMnSc1	Řehoř
slouží	sloužit	k5eAaImIp3nS	sloužit
mši	mše	k1gFnSc4	mše
svatou	svatý	k2eAgFnSc4d1	svatá
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
Kristus	Kristus	k1gMnSc1	Kristus
jako	jako	k8xS	jako
muž	muž	k1gMnSc1	muž
bolesti	bolest	k1gFnSc2	bolest
<g/>
;	;	kIx,	;
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
připojuje	připojovat	k5eAaImIp3nS	připojovat
ještě	ještě	k9	ještě
očistec	očistec	k1gInSc1	očistec
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
ubohé	ubohý	k2eAgFnPc4d1	ubohá
duše	duše	k1gFnPc4	duše
vysvobozovány	vysvobozován	k2eAgFnPc4d1	vysvobozován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Dvojeslov	Dvojeslov	k1gInSc1	Dvojeslov
<g/>
"	"	kIx"	"
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Dvojeslov	Dvojeslovo	k1gNnPc2	Dvojeslovo
bylo	být	k5eAaImAgNnS	být
přiřazeno	přiřadit	k5eAaPmNgNnS	přiřadit
Řehoři	Řehoř	k1gMnPc7	Řehoř
Velikému	veliký	k2eAgInSc3d1	veliký
v	v	k7c6	v
pravoslavné	pravoslavný	k2eAgFnSc6d1	pravoslavná
tradici	tradice	k1gFnSc6	tradice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
prací	práce	k1gFnPc2	práce
-	-	kIx~	-
Dialogy	dialog	k1gInPc1	dialog
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
taky	taky	k9	taky
Rozhovor	rozhovor	k1gInSc1	rozhovor
o	o	k7c6	o
životě	život	k1gInSc6	život
italských	italský	k2eAgMnPc2d1	italský
otců	otec	k1gMnPc2	otec
a	a	k8xC	a
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
Dvojeslov	Dvojeslov	k1gInSc4	Dvojeslov
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
špatným	špatný	k2eAgInSc7d1	špatný
překladem	překlad	k1gInSc7	překlad
řeckého	řecký	k2eAgNnSc2d1	řecké
Δ	Δ	k?	Δ
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
původně	původně	k6eAd1	původně
znamená	znamenat	k5eAaImIp3nS	znamenat
rozhovor	rozhovor	k1gInSc1	rozhovor
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
dialog	dialog	k1gInSc4	dialog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŘEHOŘ	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
VELIKÝ	veliký	k2eAgMnSc5d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
sv.	sv.	kA	sv.
Otce	otec	k1gMnSc2	otec
Benedikta	Benedikt	k1gMnSc2	Benedikt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Čeští	český	k2eAgMnPc1d1	český
benediktini	benediktin	k1gMnPc1	benediktin
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŘEHOŘ	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
VELIKÝ	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
40	[number]	k4	40
homilií	homilie	k1gFnPc2	homilie
na	na	k7c4	na
evangelia	evangelium	k1gNnPc4	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Krystal	krystal	k1gInSc1	krystal
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historický	historický	k2eAgInSc1d1	historický
a	a	k8xC	a
literární	literární	k2eAgInSc1d1	literární
kontext	kontext	k1gInSc1	kontext
řehole	řehole	k1gFnSc2	řehole
svatého	svatý	k2eAgMnSc2d1	svatý
Benedikta	Benedikt	k1gMnSc2	Benedikt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LAWRENCE	LAWRENCE	kA	LAWRENCE
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gInSc1	Hugh
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
středověkého	středověký	k2eAgNnSc2d1	středověké
mnišství	mnišství	k1gNnSc2	mnišství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
<g/>
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
117	[number]	k4	117
<g/>
-	-	kIx~	-
<g/>
124	[number]	k4	124
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FRÖLICH	FRÖLICH	kA	FRÖLICH
<g/>
,	,	kIx,	,
Roland	Roland	k1gInSc1	Roland
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
let	léto	k1gNnPc2	léto
dějin	dějiny	k1gFnPc2	dějiny
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VRÁNA	Vrána	k1gMnSc1	Vrána
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
společenství	společenství	k1gNnSc6	společenství
<g/>
:	:	kIx,	:
Životní	životní	k2eAgFnSc1d1	životní
příběhy	příběh	k1gInPc4	příběh
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
552	[number]	k4	552
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
990	[number]	k4	990
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
390	[number]	k4	390
<g/>
-	-	kIx~	-
<g/>
391	[number]	k4	391
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sedm	sedm	k4xCc1	sedm
ctností	ctnost	k1gFnPc2	ctnost
</s>
</p>
