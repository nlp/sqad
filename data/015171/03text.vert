<s>
Kozohlody	Kozohloda	k1gFnPc1
</s>
<s>
Kozohlody	Kozohloda	k1gFnPc1
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgMnPc2
svatýchLokalita	svatýchLokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
vesnice	vesnice	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Vlkaneč	Vlkaneč	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
Kraj	kraj	k1gInSc1
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
159	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Kozohlody	Kozohloda	k1gFnPc1
(	(	kIx(
<g/>
3,8	3,8	k4
km²	km²	k?
<g/>
)	)	kIx)
PSČ	PSČ	kA
</s>
<s>
286	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
64	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kozohlody	Kozohloda	k1gFnPc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
183954	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kozohlody	Kozohlody	k1gFnPc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Kosohlod	Kosohlod	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Vlkaneč	Vlkaneč	k1gInSc1
v	v	k7c6
okrese	okres	k1gInSc6
Kutná	kutný	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
asi	asi	k9
1	#num#	k4
km	km	kA
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
obce	obec	k1gFnSc2
Vlkaneč	Vlkaneč	k1gInSc1
<g/>
,	,	kIx,
5	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Golčova	Golčův	k2eAgNnSc2d1
Jeníkova	Jeníkův	k2eAgNnSc2d1
a	a	k8xC
14	#num#	k4
km	km	kA
od	od	k7c2
Čáslavi	Čáslav	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
evidováno	evidovat	k5eAaImNgNnS
65	#num#	k4
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Trvale	trvale	k6eAd1
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
168	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kozohlody	Kozohloda	k1gFnPc4
je	být	k5eAaImIp3nS
také	také	k9
název	název	k1gInSc1
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
3,8	3,8	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ves	ves	k1gFnSc1
Kozohlody	Kozohloda	k1gFnSc2
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
počátkem	počátkem	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
řádem	řád	k1gInSc7
německých	německý	k2eAgMnPc2d1
křižovníků	křižovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představeným	představený	k1gMnSc7
tohoto	tento	k3xDgInSc2
řádu	řád	k1gInSc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgMnS
komtur	komtur	k1gMnSc1
Ratmir	Ratmir	k1gMnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něj	on	k3xPp3gInSc2
se	se	k3xPyFc4
původně	původně	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
nazývala	nazývat	k5eAaImAgFnS
Radimírova	Radimírův	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Ratimirivilla	Ratimirivilla	k1gFnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Ratmersdorf	Ratmersdorf	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
již	již	k6eAd1
roku	rok	k1gInSc2
1352	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
církevních	církevní	k2eAgInPc6d1
záznamech	záznam	k1gInPc6
doložen	doložen	k2eAgInSc1d1
současný	současný	k2eAgInSc1d1
název	název	k1gInSc1
obce	obec	k1gFnSc2
Kozohlody	Kozohloda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
původu	původ	k1gInSc6
tohoto	tento	k3xDgInSc2
názvu	název	k1gInSc2
vypráví	vyprávět	k5eAaImIp3nS
pověst	pověst	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
brzy	brzy	k6eAd1
po	po	k7c6
založení	založení	k1gNnSc6
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
pozemky	pozemek	k1gInPc1
jsou	být	k5eAaImIp3nP
silně	silně	k6eAd1
zamokřeny	zamokřen	k2eAgFnPc1d1
a	a	k8xC
nevhodné	vhodný	k2eNgFnPc1d1
pro	pro	k7c4
zemědělství	zemědělství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chovali	chovat	k5eAaImAgMnP
proto	proto	k8xC
kozy	koza	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
však	však	k9
nerady	nerad	k2eAgFnPc1d1
spásaly	spásat	k5eAaImAgInP,k5eAaPmAgInP
kyselou	kyselý	k2eAgFnSc4d1
trávu	tráva	k1gFnSc4
se	se	k3xPyFc4
zamokřených	zamokřený	k2eAgInPc2d1
pozemků	pozemek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celá	k1gFnSc1
stáda	stádo	k1gNnSc2
se	se	k3xPyFc4
proto	proto	k8xC
pásla	pásnout	k5eAaImAgFnS
na	na	k7c6
panských	panský	k2eAgInPc6d1
pozemcích	pozemek	k1gInPc6
<g/>
,	,	kIx,
patřících	patřící	k2eAgInPc2d1
k	k	k7c3
nedalekému	daleký	k2eNgInSc3d1
hrádku	hrádek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
toto	tento	k3xDgNnSc4
ničení	ničení	k1gNnSc4
cizích	cizí	k2eAgInPc2d1
porostů	porost	k1gInPc2
pána	pán	k1gMnSc2
natolik	natolik	k6eAd1
rozzlobilo	rozzlobit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
celé	celý	k2eAgNnSc4d1
stádo	stádo	k1gNnSc4
koz	koza	k1gFnPc2
pobil	pobít	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
obci	obec	k1gFnSc3
začalo	začít	k5eAaPmAgNnS
říkat	říkat	k5eAaImF
Kozohlody	Kozohlod	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
<g/>
:	:	kIx,
tvoří	tvořit	k5eAaImIp3nS
dominantu	dominanta	k1gFnSc4
obce	obec	k1gFnSc2
a	a	k8xC
postaven	postaven	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
spíše	spíše	k9
kaple	kaple	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
zčásti	zčásti	k6eAd1
zachována	zachovat	k5eAaPmNgFnS
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
krátce	krátce	k6eAd1
po	po	k7c6
založení	založení	k1gNnSc6
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc1
vyzdoben	vyzdobit	k5eAaPmNgInS
nástěnnými	nástěnný	k2eAgFnPc7d1
gotickými	gotický	k2eAgFnPc7d1
malbami	malba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
obsazení	obsazení	k1gNnSc2
kraje	kraj	k1gInSc2
husity	husita	k1gMnSc2
byly	být	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
malby	malba	k1gFnPc1
zčásti	zčásti	k6eAd1
zničeny	zničen	k2eAgFnPc1d1
a	a	k8xC
zčásti	zčásti	k6eAd1
zakryty	zakrýt	k5eAaPmNgFnP
novou	nový	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
štukové	štukový	k2eAgFnSc2d1
omítky	omítka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
(	(	kIx(
<g/>
při	při	k7c6
provádění	provádění	k1gNnSc6
elektroinstalace	elektroinstalace	k1gFnSc2
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
cenné	cenný	k2eAgFnPc1d1
původní	původní	k2eAgFnPc1d1
malby	malba	k1gFnPc1
odhaleny	odhalen	k2eAgFnPc1d1
a	a	k8xC
částečně	částečně	k6eAd1
opraveny	opraven	k2eAgFnPc1d1
technikou	technika	k1gFnSc7
fresco-seco	fresco-seco	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Kostel	kostel	k1gInSc1
s	s	k7c7
malbami	malba	k1gFnPc7
je	být	k5eAaImIp3nS
cennou	cenný	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
zapsanou	zapsaný	k2eAgFnSc7d1
do	do	k7c2
Ústředního	ústřední	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
nemovitých	movitý	k2eNgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
ČR	ČR	kA
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
rejstříku	rejstřík	k1gInSc2
ÚSKP	ÚSKP	kA
34010	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
1215	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
kostela	kostel	k1gInSc2
(	(	kIx(
<g/>
u	u	k7c2
křižovatky	křižovatka	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
křížek	křížek	k1gInSc1
obklopený	obklopený	k2eAgInSc1d1
vzrostlými	vzrostlý	k2eAgInPc7d1
stromy	strom	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Počet	počet	k1gInSc1
domů	dům	k1gInPc2
podle	podle	k7c2
databáze	databáze	k1gFnSc2
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
k	k	k7c3
9	#num#	k4
<g/>
.	.	kIx.
říjnu	říjen	k1gInSc6
2009	#num#	k4
<g/>
↑	↑	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
dle	dle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
domů	dům	k1gInPc2
a	a	k8xC
bytů	byt	k1gInPc2
podle	podle	k7c2
databáze	databáze	k1gFnSc2
ČSÚ	ČSÚ	kA
20011	#num#	k4
2	#num#	k4
Informační	informační	k2eAgFnSc2d1
tabule	tabule	k1gFnSc2
v	v	k7c6
obci	obec	k1gFnSc6
(	(	kIx(
<g/>
nedaleko	nedaleko	k7c2
křížku	křížek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
145690	#num#	k4
:	:	kIx,
kostel	kostel	k1gInSc4
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
a	a	k8xC
rybník	rybník	k1gInSc1
</s>
<s>
Obecní	obecní	k2eAgInSc1d1
dům	dům	k1gInSc1
</s>
<s>
Hasičská	hasičský	k2eAgFnSc1d1
zbrojnice	zbrojnice	k1gFnSc1
a	a	k8xC
knihovna	knihovna	k1gFnSc1
</s>
<s>
Dům	dům	k1gInSc1
čp.	čp.	k?
1	#num#	k4
</s>
<s>
Brána	brána	k1gFnSc1
jednoho	jeden	k4xCgInSc2
domu	dům	k1gInSc2
</s>
<s>
Zátiší	zátiší	k1gNnSc1
u	u	k7c2
křížku	křížek	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kozohlody	Kozohloda	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
vesnici	vesnice	k1gFnSc6
ze	z	k7c2
stránek	stránka	k1gFnPc2
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
Vlkaneč	Vlkaneč	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
Vlkaneč	Vlkaneč	k1gInSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
KozohlodyPřibyslaviceVlkaneč	KozohlodyPřibyslaviceVlkaneč	k1gMnSc1
</s>
