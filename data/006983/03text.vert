<s>
Ján	Ján	k1gMnSc1	Ján
Čajak	Čajak	k1gMnSc1	Čajak
(	(	kIx(	(
<g/>
pseudonymy	pseudonym	k1gInPc1	pseudonym
Aliquis	Aliquis	k1gFnSc2	Aliquis
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Drienčanský	Drienčanský	k2eAgMnSc1d1	Drienčanský
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Kopanický	Kopanický	k2eAgMnSc1d1	Kopanický
<g/>
,	,	kIx,	,
Nihil	Nihil	k1gMnSc1	Nihil
<g/>
,	,	kIx,	,
Pozorovateľ	Pozorovateľ	k1gMnSc1	Pozorovateľ
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc4d1	starý
Hodžov	Hodžov	k1gInSc4	Hodžov
volič	volič	k1gMnSc1	volič
<g/>
,	,	kIx,	,
Strýčko	strýčko	k1gMnSc1	strýčko
Ján	Ján	k1gMnSc1	Ján
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
Liptovský	liptovský	k2eAgMnSc1d1	liptovský
Ján	Ján	k1gMnSc1	Ján
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Báčsky	Báčska	k1gFnPc1	Báčska
Petrovec	Petrovec	k1gMnSc1	Petrovec
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
štúrovského	štúrovský	k2eAgMnSc2d1	štúrovský
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
evangelického	evangelický	k2eAgMnSc2d1	evangelický
faráře	farář	k1gMnSc2	farář
Janka	Janek	k1gMnSc2	Janek
Čajaka	Čajak	k1gMnSc2	Čajak
a	a	k8xC	a
Agneši	Agnech	k1gMnPc1	Agnech
Čajakové	Čajakový	k2eAgFnSc2d1	Čajakový
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgNnSc1d1	rozené
Medvecké	Medvecký	k2eAgNnSc1d1	Medvecký
(	(	kIx(	(
<g/>
sestra	sestra	k1gFnSc1	sestra
Terézie	Terézie	k1gFnSc2	Terézie
Vansové	Vansová	k1gFnSc2	Vansová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
jako	jako	k8xC	jako
čtyřletému	čtyřletý	k2eAgInSc3d1	čtyřletý
mu	on	k3xPp3gInSc3	on
předčasně	předčasně	k6eAd1	předčasně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c4	za
Pavola	Pavola	k1gFnSc1	Pavola
Dobšinského	dobšinský	k2eAgMnSc2d1	dobšinský
<g/>
.	.	kIx.	.
</s>
<s>
Základního	základní	k2eAgNnSc2d1	základní
vzdělání	vzdělání	k1gNnSc2	vzdělání
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Martině	Martina	k1gFnSc6	Martina
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
ho	on	k3xPp3gNnSc4	on
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
na	na	k7c6	na
maďarském	maďarský	k2eAgNnSc6d1	Maďarské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Rimavské	rimavský	k2eAgFnSc6d1	Rimavská
Sobotě	sobota	k1gFnSc6	sobota
a	a	k8xC	a
německém	německý	k2eAgNnSc6d1	německé
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Kežmarku	Kežmarok	k1gInSc6	Kežmarok
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
studoval	studovat	k5eAaImAgMnS	studovat
i	i	k9	i
v	v	k7c6	v
Lučenci	Lučenec	k1gInSc6	Lučenec
<g/>
,	,	kIx,	,
Banské	banský	k2eAgFnSc6d1	Banská
Štiavnici	Štiavnica	k1gFnSc6	Štiavnica
<g/>
,	,	kIx,	,
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byl	být	k5eAaImAgInS	být
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
politické	politický	k2eAgInPc4d1	politický
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Učitelské	učitelský	k2eAgFnPc4d1	učitelská
zkoušky	zkouška	k1gFnPc4	zkouška
nakonec	nakonec	k6eAd1	nakonec
složil	složit	k5eAaPmAgMnS	složit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
v	v	k7c6	v
Kláštore	Kláštor	k1gMnSc5	Kláštor
pod	pod	k7c7	pod
Znievem	Zniev	k1gInSc7	Zniev
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
advokátní	advokátní	k2eAgFnSc6d1	advokátní
kanceláři	kancelář	k1gFnSc6	kancelář
Petra	Petr	k1gMnSc2	Petr
Jamnického	Jamnický	k2eAgMnSc2d1	Jamnický
v	v	k7c6	v
Pezinku	Pezink	k1gInSc6	Pezink
<g/>
,	,	kIx,	,
učil	učít	k5eAaPmAgMnS	učít
v	v	k7c6	v
Liptovské	liptovský	k2eAgFnSc6d1	Liptovská
Kokavě	Kokava	k1gFnSc6	Kokava
<g/>
,	,	kIx,	,
Rajci	Rajce	k1gFnSc6	Rajce
a	a	k8xC	a
v	v	k7c6	v
Kráľové	Kráľové	k2eAgFnSc6d1	Kráľové
Lehotě	Lehota	k1gFnSc6	Lehota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
na	na	k7c4	na
Dolní	dolní	k2eAgFnSc4d1	dolní
zem	zem	k1gFnSc4	zem
do	do	k7c2	do
Báčky	Báčka	k1gFnSc2	Báčka
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Selenče	Selenec	k1gMnSc5	Selenec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
jugoslávském	jugoslávský	k2eAgMnSc6d1	jugoslávský
Petrovci	Petrovec	k1gMnSc6	Petrovec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
let	léto	k1gNnPc2	léto
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
snah	snaha	k1gFnPc2	snaha
dolnozemských	dolnozemský	k2eAgMnPc2d1	dolnozemský
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
slovenského	slovenský	k2eAgNnSc2d1	slovenské
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Petrovci	Petrovec	k1gMnSc6	Petrovec
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
však	však	k9	však
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
literárního	literární	k2eAgMnSc2d1	literární
a	a	k8xC	a
národopisného	národopisný	k2eAgInSc2d1	národopisný
odboru	odbor	k1gInSc2	odbor
Matice	matice	k1gFnSc2	matice
slovenské	slovenský	k2eAgFnSc2d1	slovenská
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
pokročilém	pokročilý	k2eAgNnSc6d1	pokročilé
stáří	stáří	k1gNnSc6	stáří
sledoval	sledovat	k5eAaImAgInS	sledovat
politický	politický	k2eAgInSc1d1	politický
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
vývoj	vývoj	k1gInSc1	vývoj
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Začátky	začátek	k1gInPc1	začátek
jeho	jeho	k3xOp3gFnSc2	jeho
literární	literární	k2eAgFnSc2d1	literární
tvorby	tvorba	k1gFnSc2	tvorba
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
období	období	k1gNnSc2	období
jeho	on	k3xPp3gNnSc2	on
přestěhování	přestěhování	k1gNnSc2	přestěhování
do	do	k7c2	do
Petrovce	Petrovec	k1gMnSc2	Petrovec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
a	a	k8xC	a
tvorbou	tvorba	k1gFnSc7	tvorba
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
vlny	vlna	k1gFnSc2	vlna
realistických	realistický	k2eAgMnPc2d1	realistický
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
Svetozárem	Svetozár	k1gInSc7	Svetozár
Hurbanem-Vajanským	Hurbanem-Vajanský	k2eAgInSc7d1	Hurbanem-Vajanský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
začal	začít	k5eAaPmAgMnS	začít
přispívat	přispívat	k5eAaImF	přispívat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
periodik	periodikum	k1gNnPc2	periodikum
(	(	kIx(	(
<g/>
Národnie	Národnie	k1gFnPc1	Národnie
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgInPc1d1	slovenský
pohľady	pohľad	k1gInPc1	pohľad
<g/>
,	,	kIx,	,
Dennica	Dennicum	k1gNnPc1	Dennicum
<g/>
,	,	kIx,	,
Slovenský	slovenský	k2eAgInSc1d1	slovenský
týždenník	týždenník	k1gInSc1	týždenník
<g/>
,	,	kIx,	,
Národný	Národný	k2eAgInSc1d1	Národný
hlásnik	hlásnik	k1gInSc1	hlásnik
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
sa	sa	k?	sa
stálým	stálý	k2eAgMnSc7d1	stálý
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
pražského	pražský	k2eAgInSc2d1	pražský
měsíčníku	měsíčník	k1gInSc2	měsíčník
Naše	náš	k3xOp1gNnSc1	náš
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
slovenský	slovenský	k2eAgInSc1d1	slovenský
politický	politický	k2eAgInSc1d1	politický
týdeník	týdeník	k1gInSc1	týdeník
Národná	Národný	k2eAgFnSc1d1	Národná
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
kulturní	kulturní	k2eAgMnSc1d1	kulturní
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
měsíčník	měsíčník	k1gMnSc1	měsíčník
Náš	náš	k3xOp1gInSc4	náš
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
prozaická	prozaický	k2eAgFnSc1d1	prozaická
práce	práce	k1gFnSc1	práce
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
v	v	k7c6	v
Národních	národní	k2eAgFnPc6d1	národní
novinách	novina	k1gFnPc6	novina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
čerpal	čerpat	k5eAaImAgInS	čerpat
ze	z	k7c2	z
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
Liptov	Liptov	k1gInSc4	Liptov
a	a	k8xC	a
Gemer	Gemer	k1gInSc4	Gemer
<g/>
,	,	kIx,	,
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
předkládá	předkládat	k5eAaImIp3nS	předkládat
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgInS	psát
také	také	k9	také
anekdotické	anekdotický	k2eAgInPc4d1	anekdotický
příběhy	příběh	k1gInPc4	příběh
a	a	k8xC	a
žánrové	žánrový	k2eAgFnPc4d1	žánrová
črty	črta	k1gFnPc4	črta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
psaní	psaní	k1gNnSc2	psaní
beletrie	beletrie	k1gFnSc2	beletrie
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgInS	zaměřovat
i	i	k9	i
na	na	k7c4	na
publicistiku	publicistika	k1gFnSc4	publicistika
<g/>
,	,	kIx,	,
lidovýchovnou	lidovýchovný	k2eAgFnSc4d1	lidovýchovný
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
překlady	překlad	k1gInPc4	překlad
(	(	kIx(	(
<g/>
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Puškin	Puškin	k1gMnSc1	Puškin
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ján	Ján	k1gMnSc1	Ján
Čajak	Čajak	k1gMnSc1	Čajak
1903	[number]	k4	1903
–	–	k?	–
Predaj	Predaj	k1gMnSc1	Predaj
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
1903	[number]	k4	1903
–	–	k?	–
Pred	Pred	k1gInSc1	Pred
oltárom	oltárom	k1gInSc1	oltárom
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
(	(	kIx(	(
<g/>
vydaly	vydat	k5eAaPmAgFnP	vydat
Slovenské	slovenský	k2eAgFnPc1d1	slovenská
pohľady	pohľada	k1gFnPc1	pohľada
<g/>
)	)	kIx)	)
1905	[number]	k4	1905
–	–	k?	–
Z	z	k7c2	z
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
(	(	kIx(	(
<g/>
vydaly	vydat	k5eAaPmAgFnP	vydat
Slovenské	slovenský	k2eAgFnPc1d1	slovenská
pohľady	pohľada	k1gFnPc1	pohľada
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
1905	[number]	k4	1905
–	–	k?	–
Vysťahovalec	Vysťahovalec	k1gMnSc1	Vysťahovalec
1906	[number]	k4	1906
–	–	k?	–
Báťa	báťa	k1gMnSc1	báťa
Kalinský	Kalinský	k2eAgMnSc1d1	Kalinský
1906	[number]	k4	1906
–	–	k?	–
Obyčajná	Obyčajný	k2eAgFnSc1d1	Obyčajný
história	histórium	k1gNnSc2	histórium
1907	[number]	k4	1907
–	–	k?	–
Tri	Tri	k1gMnSc4	Tri
rozprávky	rozprávka	k1gFnSc2	rozprávka
1908	[number]	k4	1908
–	–	k?	–
Suchoty	suchota	k1gFnSc2	suchota
1908	[number]	k4	1908
–	–	k?	–
Vohľady	Vohľada	k1gFnSc2	Vohľada
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
1909	[number]	k4	1909
–	–	k?	–
Rodina	rodina	k1gFnSc1	rodina
Rovesných	Rovesný	k2eAgMnPc2d1	Rovesný
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
psán	psán	k2eAgInSc1d1	psán
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Aliquis	Aliquis	k1gInSc1	Aliquis
<g/>
)	)	kIx)	)
1910	[number]	k4	1910
–	–	k?	–
Ja	Ja	k?	Ja
si	se	k3xPyFc3	se
svoj	svojit	k5eAaImRp2nS	svojit
mliečnik	mliečnik	k1gInSc1	mliečnik
načnem	načnem	k?	načnem
<g/>
,	,	kIx,	,
anekdotický	anekdotický	k2eAgInSc1d1	anekdotický
příběh	příběh	k1gInSc1	příběh
<g />
.	.	kIx.	.
</s>
<s>
1910	[number]	k4	1910
–	–	k?	–
Strýc	strýc	k1gMnSc1	strýc
Miško	Miško	k1gNnSc1	Miško
<g/>
,	,	kIx,	,
anekdotický	anekdotický	k2eAgInSc1d1	anekdotický
příběh	příběh	k1gInSc1	příběh
1910	[number]	k4	1910
/	/	kIx~	/
1911	[number]	k4	1911
–	–	k?	–
V	v	k7c6	v
tretej	tretat	k5eAaImRp2nS	tretat
triede	tried	k1gMnSc5	tried
<g/>
,	,	kIx,	,
cyklus	cyklus	k1gInSc1	cyklus
črt	črt	k1gInSc1	črt
1912	[number]	k4	1912
–	–	k?	–
Ujčekov	Ujčekov	k1gInSc1	Ujčekov
Mikušov	Mikušov	k1gInSc1	Mikušov
posledný	posledný	k2eAgInSc1d1	posledný
deň	deň	k?	deň
<g/>
,	,	kIx,	,
anekdotický	anekdotický	k2eAgInSc1d1	anekdotický
příběh	příběh	k1gInSc1	příběh
1913	[number]	k4	1913
–	–	k?	–
Jožkova	Jožkův	k2eAgFnSc1d1	Jožkova
svadba	svadba	k1gFnSc1	svadba
a	a	k8xC	a
iné	iné	k?	iné
rozprávky	rozprávka	k1gFnPc1	rozprávka
<g/>
,	,	kIx,	,
anekdotický	anekdotický	k2eAgInSc1d1	anekdotický
příběh	příběh	k1gInSc1	příběh
1913	[number]	k4	1913
–	–	k?	–
Ecce	Ecce	k1gNnSc1	Ecce
homo	homo	k1gMnSc1	homo
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
1914	[number]	k4	1914
–	–	k?	–
Cholera	cholera	k1gFnSc1	cholera
<g />
.	.	kIx.	.
</s>
<s>
1914	[number]	k4	1914
–	–	k?	–
Dejepis	Dejepis	k1gInSc1	Dejepis
Slovákov	Slovákov	k1gInSc1	Slovákov
1918	[number]	k4	1918
–	–	k?	–
Návrat	návrat	k1gInSc1	návrat
1922	[number]	k4	1922
–	–	k?	–
Tridsaťpäť	Tridsaťpäť	k1gFnSc1	Tridsaťpäť
dekagramov	dekagramov	k1gInSc1	dekagramov
1923	[number]	k4	1923
–	–	k?	–
Zápisky	zápisek	k1gInPc1	zápisek
z	z	k7c2	z
rukojemstva	rukojemstvo	k1gNnSc2	rukojemstvo
<g/>
,	,	kIx,	,
dokumentární	dokumentární	k2eAgFnSc1d1	dokumentární
próza	próza	k1gFnSc1	próza
1924	[number]	k4	1924
–	–	k?	–
Fuksi	Fuks	k1gMnSc3	Fuks
<g/>
,	,	kIx,	,
črta	črta	k1gFnSc1	črta
1929	[number]	k4	1929
–	–	k?	–
V	v	k7c6	v
druhej	druhat	k5eAaBmRp2nS	druhat
triede	tried	k1gMnSc5	tried
<g/>
,	,	kIx,	,
cyklus	cyklus	k1gInSc1	cyklus
črt	črt	k1gInSc1	črt
1929	[number]	k4	1929
–	–	k?	–
Len	len	k1gInSc1	len
pekne	peknout	k5eAaPmIp3nS	peknout
<g/>
,	,	kIx,	,
dramatická	dramatický	k2eAgFnSc1d1	dramatická
scénka	scénka	k1gFnSc1	scénka
1931	[number]	k4	1931
–	–	k?	–
Búrka	Búrka	k1gMnSc1	Búrka
<g/>
,	,	kIx,	,
žánrový	žánrový	k2eAgMnSc1d1	žánrový
črta	črta	k1gFnSc1	črta
1936	[number]	k4	1936
–	–	k?	–
Malí	malý	k2eAgMnPc1d1	malý
ubehlíci	ubehlík	k1gMnPc1	ubehlík
1936	[number]	k4	1936
–	–	k?	–
Ďuro	Ďuro	k1gMnSc1	Ďuro
Kožuch	Kožuch	k1gMnSc1	Kožuch
<g/>
,	,	kIx,	,
úryvek	úryvek	k1gInSc1	úryvek
uveřejněn	uveřejněn	k2eAgInSc1d1	uveřejněn
v	v	k7c6	v
Našem	náš	k3xOp1gInSc6	náš
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
plánoval	plánovat	k5eAaImAgMnS	plánovat
napsat	napsat	k5eAaPmF	napsat
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
to	ten	k3xDgNnSc4	ten
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ján	Ján	k1gMnSc1	Ján
Čajak	Čajak	k1gMnSc1	Čajak
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
