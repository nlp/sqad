<s>
Ján	Ján	k1gMnSc1	Ján
Čajak	Čajak	k1gMnSc1	Čajak
(	(	kIx(	(
<g/>
pseudonymy	pseudonym	k1gInPc1	pseudonym
Aliquis	Aliquis	k1gFnSc2	Aliquis
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
Drienčanský	Drienčanský	k2eAgMnSc1d1	Drienčanský
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Kopanický	Kopanický	k2eAgMnSc1d1	Kopanický
<g/>
,	,	kIx,	,
Nihil	Nihil	k1gMnSc1	Nihil
<g/>
,	,	kIx,	,
Pozorovateľ	Pozorovateľ	k1gMnSc1	Pozorovateľ
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc4d1	starý
Hodžov	Hodžov	k1gInSc4	Hodžov
volič	volič	k1gMnSc1	volič
<g/>
,	,	kIx,	,
Strýčko	strýčko	k1gMnSc1	strýčko
Ján	Ján	k1gMnSc1	Ján
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
Liptovský	liptovský	k2eAgMnSc1d1	liptovský
Ján	Ján	k1gMnSc1	Ján
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Báčsky	Báčska	k1gFnPc1	Báčska
Petrovec	Petrovec	k1gMnSc1	Petrovec
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
