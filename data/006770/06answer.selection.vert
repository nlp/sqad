<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
je	být	k5eAaImIp3nS	být
Norman	Norman	k1gMnSc1	Norman
Finkelstein	Finkelstein	k1gMnSc1	Finkelstein
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
židovského	židovský	k2eAgMnSc4d1	židovský
původu	původa	k1gMnSc4	původa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
napsal	napsat	k5eAaBmAgInS	napsat
knihu	kniha	k1gFnSc4	kniha
Průmysl	průmysl	k1gInSc1	průmysl
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
16	[number]	k4	16
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
