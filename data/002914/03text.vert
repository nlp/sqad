<s>
Koníček	koníček	k1gInSc1	koníček
mořský	mořský	k2eAgInSc1d1	mořský
či	či	k8xC	či
také	také	k9	také
koníček	koníček	k1gInSc1	koníček
mořský	mořský	k2eAgInSc1d1	mořský
skvrnitý	skvrnitý	k2eAgInSc1d1	skvrnitý
(	(	kIx(	(
<g/>
Hippocampus	Hippocampus	k1gMnSc1	Hippocampus
guttulatus	guttulatus	k1gMnSc1	guttulatus
<g/>
;	;	kIx,	;
Cuvier	Cuvier	k1gMnSc1	Cuvier
<g/>
,	,	kIx,	,
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
mořské	mořský	k2eAgFnSc2d1	mořská
ryby	ryba	k1gFnSc2	ryba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jehlovitých	jehlovitý	k2eAgMnPc2d1	jehlovitý
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgInSc7	svůj
netypickým	typický	k2eNgInSc7d1	netypický
tvarem	tvar	k1gInSc7	tvar
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
částečně	částečně	k6eAd1	částečně
připomíná	připomínat	k5eAaImIp3nS	připomínat
hlavu	hlava	k1gFnSc4	hlava
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
malou	malý	k2eAgFnSc4d1	malá
rybku	rybka	k1gFnSc4	rybka
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
okolo	okolo	k7c2	okolo
15	[number]	k4	15
cm	cm	kA	cm
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomocí	pomocí	k7c2	pomocí
chápavého	chápavý	k2eAgInSc2d1	chápavý
ocasu	ocas	k1gInSc2	ocas
žije	žít	k5eAaImIp3nS	žít
přichycena	přichycen	k2eAgFnSc1d1	přichycena
k	k	k7c3	k
řasám	řasa	k1gFnPc3	řasa
či	či	k8xC	či
rostlinám	rostlina	k1gFnPc3	rostlina
a	a	k8xC	a
rypcem	rypec	k1gInSc7	rypec
nasává	nasávat	k5eAaImIp3nS	nasávat
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
filtruje	filtrovat	k5eAaImIp3nS	filtrovat
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
drobných	drobný	k2eAgMnPc2d1	drobný
korýšů	korýš	k1gMnPc2	korýš
a	a	k8xC	a
planktonu	plankton	k1gInSc2	plankton
<g/>
.	.	kIx.	.
</s>
<s>
Koníček	koníček	k1gInSc1	koníček
mořský	mořský	k2eAgInSc1d1	mořský
se	se	k3xPyFc4	se
příležitostně	příležitostně	k6eAd1	příležitostně
česky	česky	k6eAd1	česky
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
koník	koník	k1gMnSc1	koník
mořský	mořský	k2eAgMnSc1d1	mořský
<g/>
,	,	kIx,	,
či	či	k8xC	či
koníček	koníček	k1gInSc1	koníček
skvrnitý	skvrnitý	k2eAgInSc1d1	skvrnitý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědeckém	vědecký	k2eAgNnSc6d1	vědecké
názvosloví	názvosloví	k1gNnSc6	názvosloví
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
synonymních	synonymní	k2eAgNnPc2d1	synonymní
pojmenování	pojmenování	k1gNnPc2	pojmenování
<g/>
:	:	kIx,	:
Hippocampus	Hippocampus	k1gInSc1	Hippocampus
guttulatus	guttulatus	k1gMnSc1	guttulatus
multiannularis	multiannularis	k1gFnPc2	multiannularis
Ginsburg	Ginsburg	k1gMnSc1	Ginsburg
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
Hippocampus	Hippocampus	k1gMnSc1	Hippocampus
hippocampus	hippocampus	k1gMnSc1	hippocampus
microcoronatus	microcoronatus	k1gMnSc1	microcoronatus
Slastenenko	Slastenenka	k1gFnSc5	Slastenenka
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
Hippocampus	Hippocampus	k1gMnSc1	Hippocampus
hippocampus	hippocampus	k1gMnSc1	hippocampus
microstephanus	microstephanus	k1gMnSc1	microstephanus
Slastenenko	Slastenenka	k1gFnSc5	Slastenenka
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
Hippocampus	Hippocampus	k1gInSc1	Hippocampus
longirostris	longirostris	k1gFnSc2	longirostris
Schinz	Schinza	k1gFnPc2	Schinza
<g/>
,	,	kIx,	,
1822	[number]	k4	1822
Hippocampus	Hippocampus	k1gMnSc1	Hippocampus
ramulosus	ramulosus	k1gMnSc1	ramulosus
Leach	Leach	k1gMnSc1	Leach
<g/>
,	,	kIx,	,
1814	[number]	k4	1814
Koníček	koníček	k1gInSc1	koníček
mořský	mořský	k2eAgInSc1d1	mořský
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
ryba	ryba	k1gFnSc1	ryba
s	s	k7c7	s
netypickým	typický	k2eNgInSc7d1	netypický
tvarem	tvar	k1gInSc7	tvar
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
jeví	jevit	k5eAaImIp3nS	jevit
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
samec	samec	k1gMnSc1	samec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximálně	maximálně	k6eAd1	maximálně
16	[number]	k4	16
cm	cm	kA	cm
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
až	až	k9	až
18	[number]	k4	18
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
i	i	k9	i
vzhledově	vzhledově	k6eAd1	vzhledově
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
delší	dlouhý	k2eAgInSc4d2	delší
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
břicha	břich	k1gInSc2	břich
břišní	břišní	k2eAgInSc4d1	břišní
vak	vak	k1gInSc4	vak
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
přechovává	přechovávat	k5eAaImIp3nS	přechovávat
plůdek	plůdek	k1gInSc4	plůdek
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
esovitě	esovitě	k6eAd1	esovitě
prohnuté	prohnutý	k2eAgNnSc1d1	prohnuté
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
krku	krk	k1gInSc2	krk
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
ocasu	ocas	k1gInSc2	ocas
pod	pod	k7c7	pod
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
připomíná	připomínat	k5eAaImIp3nS	připomínat
koňskou	koňský	k2eAgFnSc4d1	koňská
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
značně	značně	k6eAd1	značně
protáhlý	protáhlý	k2eAgInSc1d1	protáhlý
rypec	rypec	k1gInSc1	rypec
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
drobná	drobný	k2eAgNnPc4d1	drobné
ústa	ústa	k1gNnPc4	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
rypcem	rypec	k1gInSc7	rypec
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
dvojice	dvojice	k1gFnSc1	dvojice
malých	malý	k2eAgNnPc2d1	malé
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
širší	široký	k2eAgNnSc4d2	širší
zorné	zorný	k2eAgNnSc4d1	zorné
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
kostěnými	kostěný	k2eAgFnPc7d1	kostěná
destičkami	destička	k1gFnPc7	destička
<g/>
,	,	kIx,	,
kterých	který	k3yIgNnPc2	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
48	[number]	k4	48
až	až	k9	až
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
drobné	drobný	k2eAgInPc1d1	drobný
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
výrůstky	výrůstek	k1gInPc1	výrůstek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hřbetní	hřbetní	k2eAgFnSc6d1	hřbetní
ploutvi	ploutev	k1gFnSc6	ploutev
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
18	[number]	k4	18
až	až	k9	až
21	[number]	k4	21
ploutevních	ploutevní	k2eAgInPc2d1	ploutevní
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
v	v	k7c4	v
řitní	řitní	k2eAgInPc4d1	řitní
4	[number]	k4	4
až	až	k8xS	až
5	[number]	k4	5
a	a	k8xC	a
prsní	prsní	k2eAgFnPc1d1	prsní
ploutve	ploutev	k1gFnPc1	ploutev
jsou	být	k5eAaImIp3nP	být
vyztuženy	vyztužen	k2eAgInPc4d1	vyztužen
15	[number]	k4	15
až	až	k8xS	až
16	[number]	k4	16
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
má	mít	k5eAaImIp3nS	mít
nejčastěji	často	k6eAd3	často
zelenou	zelený	k2eAgFnSc4d1	zelená
až	až	k8xS	až
nahnědlou	nahnědlý	k2eAgFnSc4d1	nahnědlá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kostěných	kostěný	k2eAgInPc2d1	kostěný
štítků	štítek	k1gInPc2	štítek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevovat	objevovat	k5eAaImF	objevovat
malé	malý	k2eAgFnPc4d1	malá
bílé	bílý	k2eAgFnPc4d1	bílá
tečky	tečka	k1gFnPc4	tečka
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
druhům	druh	k1gInPc3	druh
ryb	ryba	k1gFnPc2	ryba
má	mít	k5eAaImIp3nS	mít
koníček	koníček	k1gInSc4	koníček
chápavý	chápavý	k2eAgInSc4d1	chápavý
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přichytit	přichytit	k5eAaPmF	přichytit
se	se	k3xPyFc4	se
na	na	k7c4	na
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
či	či	k8xC	či
skalní	skalní	k2eAgNnSc1d1	skalní
podloží	podloží	k1gNnSc1	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Koníček	Koníček	k1gMnSc1	Koníček
mořský	mořský	k2eAgMnSc1d1	mořský
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pomocí	pomocí	k7c2	pomocí
kmitání	kmitání	k1gNnSc2	kmitání
hřbetní	hřbetní	k2eAgFnSc2d1	hřbetní
ploutve	ploutev	k1gFnSc2	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
ale	ale	k8xC	ale
stojí	stát	k5eAaImIp3nS	stát
nehybně	hybně	k6eNd1	hybně
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
přichycen	přichycen	k2eAgMnSc1d1	přichycen
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
pomocí	pomocí	k7c2	pomocí
chápavého	chápavý	k2eAgInSc2d1	chápavý
ocasu	ocas	k1gInSc2	ocas
za	za	k7c4	za
řasy	řasa	k1gFnPc4	řasa
či	či	k8xC	či
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
neplave	plavat	k5eNaImIp3nS	plavat
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnPc4d1	ostatní
ryby	ryba	k1gFnPc4	ryba
hlavou	hlava	k1gFnSc7	hlava
napřed	napřed	k6eAd1	napřed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
vzpřímený	vzpřímený	k2eAgInSc1d1	vzpřímený
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
Atlantik	Atlantik	k1gInSc4	Atlantik
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
pobřeží	pobřeží	k1gNnSc2	pobřeží
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
oblastí	oblast	k1gFnPc2	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
s	s	k7c7	s
koníčkem	koníček	k1gInSc7	koníček
mořským	mořský	k2eAgFnPc3d1	mořská
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
Madeiry	Madeira	k1gFnSc2	Madeira
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
souřadnic	souřadnice	k1gFnPc2	souřadnice
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
jeho	jeho	k3xOp3gFnSc1	jeho
oblast	oblast	k1gFnSc1	oblast
výskytu	výskyt	k1gInSc2	výskyt
ohraničit	ohraničit	k5eAaPmF	ohraničit
20	[number]	k4	20
<g/>
°	°	k?	°
až	až	k9	až
63	[number]	k4	63
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
32	[number]	k4	32
<g/>
°	°	k?	°
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
až	až	k9	až
36	[number]	k4	36
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dno	dno	k1gNnSc4	dno
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
rostlinami	rostlina	k1gFnPc7	rostlina
a	a	k8xC	a
řasami	řasa	k1gFnPc7	řasa
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přichytit	přichytit	k5eAaPmF	přichytit
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
hloubka	hloubka	k1gFnSc1	hloubka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
koníček	koníček	k1gMnSc1	koníček
nacházel	nacházet	k5eAaImAgMnS	nacházet
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
do	do	k7c2	do
hlubších	hluboký	k2eAgFnPc2d2	hlubší
vod	voda	k1gFnPc2	voda
s	s	k7c7	s
kamenitým	kamenitý	k2eAgNnSc7d1	kamenité
podložím	podloží	k1gNnSc7	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Koníček	koníček	k1gInSc1	koníček
přijímá	přijímat	k5eAaImIp3nS	přijímat
potravu	potrava	k1gFnSc4	potrava
ústy	ústa	k1gNnPc7	ústa
umístěnými	umístěný	k2eAgInPc7d1	umístěný
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vystouplého	vystouplý	k2eAgInSc2d1	vystouplý
rypce	rypec	k1gInSc2	rypec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
nasávána	nasáván	k2eAgFnSc1d1	nasávána
potrava	potrava	k1gFnSc1	potrava
do	do	k7c2	do
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
drobné	drobný	k2eAgMnPc4d1	drobný
korýše	korýš	k1gMnPc4	korýš
a	a	k8xC	a
plankton	plankton	k1gInSc4	plankton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
vznáší	vznášet	k5eAaImIp3nS	vznášet
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
dospělost	dospělost	k1gFnSc1	dospělost
nastává	nastávat	k5eAaImIp3nS	nastávat
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
koníček	koníček	k1gInSc1	koníček
měří	měřit	k5eAaImIp3nS	měřit
10	[number]	k4	10
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Koníček	koníček	k1gInSc1	koníček
se	se	k3xPyFc4	se
tře	třít	k5eAaImIp3nS	třít
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
června	červen	k1gInSc2	červen
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
výběru	výběr	k1gInSc3	výběr
partnera	partner	k1gMnSc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Samička	samička	k1gFnSc1	samička
následně	následně	k6eAd1	následně
naklade	naklást	k5eAaPmIp3nS	naklást
vajíčka	vajíčko	k1gNnPc4	vajíčko
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
2	[number]	k4	2
mm	mm	kA	mm
do	do	k7c2	do
břišního	břišní	k2eAgInSc2d1	břišní
vaku	vak	k1gInSc2	vak
samce	samec	k1gInSc2	samec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c6	po
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
chová	chovat	k5eAaImIp3nS	chovat
zárodky	zárodek	k1gInPc4	zárodek
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
zcela	zcela	k6eAd1	zcela
vyvinutá	vyvinutý	k2eAgNnPc1d1	vyvinuté
mláďata	mládě	k1gNnPc1	mládě
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
12	[number]	k4	12
mm	mm	kA	mm
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Koníček	koníček	k1gInSc1	koníček
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
loven	loven	k2eAgInSc4d1	loven
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
akvaristice	akvaristika	k1gFnSc6	akvaristika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
chován	chovat	k5eAaImNgInS	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
suší	sušit	k5eAaImIp3nS	sušit
a	a	k8xC	a
prodává	prodávat	k5eAaImIp3nS	prodávat
jako	jako	k9	jako
turistický	turistický	k2eAgInSc1d1	turistický
suvenýr	suvenýr	k1gInSc1	suvenýr
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
je	být	k5eAaImIp3nS	být
koníček	koníček	k1gInSc4	koníček
mořský	mořský	k2eAgInSc4d1	mořský
umístěn	umístěn	k2eAgInSc4d1	umístěn
v	v	k7c6	v
Červené	Červené	k2eAgFnSc6d1	Červené
knize	kniha	k1gFnSc6	kniha
<g/>
.	.	kIx.	.
</s>
