<s>
Smích	smích	k1gInSc1	smích
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
mimických	mimický	k2eAgFnPc2d1	mimická
vlastností	vlastnost	k1gFnPc2	vlastnost
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
a	a	k8xC	a
vyšších	vysoký	k2eAgInPc2d2	vyšší
primátů	primát	k1gInPc2	primát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlasitým	hlasitý	k2eAgInSc7d1	hlasitý
projevem	projev	k1gInSc7	projev
veselí	veselí	k1gNnSc2	veselí
<g/>
,	,	kIx,	,
radosti	radost	k1gFnSc2	radost
<g/>
,	,	kIx,	,
nadšení	nadšený	k2eAgMnPc1d1	nadšený
apod.	apod.	kA	apod.
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
úsměvu	úsměv	k1gInSc2	úsměv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
pouze	pouze	k6eAd1	pouze
specifická	specifický	k2eAgFnSc1d1	specifická
grimasa	grimasa	k1gFnSc1	grimasa
<g/>
,	,	kIx,	,
smích	smích	k1gInSc1	smích
je	být	k5eAaImIp3nS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
především	především	k6eAd1	především
bránice	bránice	k1gFnSc2	bránice
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
jako	jako	k9	jako
psychologická	psychologický	k2eAgFnSc1d1	psychologická
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
vtip	vtip	k1gInSc4	vtip
<g/>
,	,	kIx,	,
lechtání	lechtání	k1gNnSc4	lechtání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc1d1	jiný
podněty	podnět	k1gInPc1	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Inhalace	inhalace	k1gFnSc1	inhalace
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
smích	smích	k1gInSc4	smích
<g/>
.	.	kIx.	.
</s>
<s>
Drogy	droga	k1gFnPc1	droga
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
marihuana	marihuana	k1gFnSc1	marihuana
<g/>
,	,	kIx,	,
můžou	můžou	k?	můžou
způsobit	způsobit	k5eAaPmF	způsobit
silný	silný	k2eAgInSc4d1	silný
smích	smích	k1gInSc4	smích
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
smích	smích	k1gInSc1	smích
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
slzy	slza	k1gFnPc4	slza
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
tekutého	tekutý	k2eAgNnSc2d1	tekuté
jídla	jídlo	k1gNnSc2	jídlo
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
samovolnému	samovolný	k2eAgNnSc3d1	samovolné
vypuzení	vypuzení	k1gNnSc3	vypuzení
nosem	nos	k1gInSc7	nos
<g/>
.	.	kIx.	.
</s>
<s>
Štěstí	štěstit	k5eAaImIp3nS	štěstit
Radost	radost	k1gFnSc1	radost
Úsměv	úsměv	k1gInSc4	úsměv
Pláč	pláč	k1gInSc4	pláč
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Smích	smích	k1gInSc1	smích
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
smích	smích	k1gInSc1	smích
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
smích	smích	k1gInSc1	smích
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
.	.	kIx.	.
</s>
