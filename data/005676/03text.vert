<s>
Baroko	baroko	k1gNnSc1	baroko
či	či	k8xC	či
barok	barok	k1gInSc1	barok
je	být	k5eAaImIp3nS	být
umělecko-kulturní	uměleckoulturní	k2eAgInSc1d1	umělecko-kulturní
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
znaky	znak	k1gInPc7	znak
baroka	baroko	k1gNnSc2	baroko
jsou	být	k5eAaImIp3nP	být
dynamika	dynamika	k1gFnSc1	dynamika
–	–	k?	–
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
vyjádření	vyjádření	k1gNnSc4	vyjádření
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
emotivnost	emotivnost	k1gFnSc1	emotivnost
až	až	k8xS	až
citová	citový	k2eAgFnSc1d1	citová
vypjatost	vypjatost	k1gFnSc1	vypjatost
<g/>
,	,	kIx,	,
bohatost	bohatost	k1gFnSc1	bohatost
tvarů	tvar	k1gInPc2	tvar
i	i	k8xC	i
zdobnosti	zdobnost	k1gFnPc4	zdobnost
a	a	k8xC	a
velkolepost	velkolepost	k1gFnSc4	velkolepost
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
původně	původně	k6eAd1	původně
snad	snad	k9	snad
z	z	k7c2	z
port	porta	k1gFnPc2	porta
<g/>
.	.	kIx.	.
pérola	pérola	k1gFnSc1	pérola
barroca	barroc	k2eAgFnSc1d1	barroc
–	–	k?	–
perla	perla	k1gFnSc1	perla
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
posměšný	posměšný	k2eAgInSc1d1	posměšný
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgMnS	vžít
jako	jako	k9	jako
běžné	běžný	k2eAgNnSc4d1	běžné
označení	označení	k1gNnSc4	označení
<g/>
.	.	kIx.	.
</s>
<s>
Slovem	slovem	k6eAd1	slovem
baroko	baroko	k1gNnSc1	baroko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
i	i	k9	i
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Nejpravděpodobnější	pravděpodobný	k2eAgInSc1d3	Nejpravděpodobnější
výklad	výklad	k1gInSc1	výklad
slova	slovo	k1gNnSc2	slovo
baroko	baroko	k1gNnSc1	baroko
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
,	,	kIx,	,
výraz	výraz	k1gInSc4	výraz
perles	perlesa	k1gFnPc2	perlesa
baroques	baroques	k1gMnSc1	baroques
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
perly	perla	k1gFnPc4	perla
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
tvaru	tvar	k1gInSc2	tvar
používal	používat	k5eAaImAgInS	používat
již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
logice	logika	k1gFnSc6	logika
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označovalo	označovat	k5eAaImAgNnS	označovat
vše	všechen	k3xTgNnSc1	všechen
nabubřelé	nabubřelý	k2eAgNnSc1d1	nabubřelé
<g/>
,	,	kIx,	,
nesprávné	správný	k2eNgNnSc1d1	nesprávné
a	a	k8xC	a
směšné	směšný	k2eAgNnSc1d1	směšné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnáctém	sedmnáctý	k4xOgNnSc6	sedmnáctý
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
užití	užití	k1gNnSc1	užití
termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
barokní	barokní	k2eAgInPc1d1	barokní
<g/>
"	"	kIx"	"
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
tordovaný	tordovaný	k2eAgMnSc1d1	tordovaný
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kroucený	kroucený	k2eAgMnSc1d1	kroucený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
označení	označení	k1gNnSc1	označení
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
slohů	sloh	k1gInPc2	sloh
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
i	i	k8xC	i
baroko	baroko	k1gNnSc1	baroko
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
až	až	k6eAd1	až
zpětně	zpětně	k6eAd1	zpětně
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
mělo	mít	k5eAaImAgNnS	mít
toto	tento	k3xDgNnSc4	tento
dodatečně	dodatečně	k6eAd1	dodatečně
používané	používaný	k2eAgNnSc4d1	používané
označení	označení	k1gNnSc4	označení
pejorativní	pejorativní	k2eAgInSc1d1	pejorativní
přídech	přídech	k1gInSc1	přídech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
a	a	k8xC	a
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgNnSc6	devatenáctý
století	století	k1gNnSc6	století
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
baroko	baroko	k1gNnSc4	baroko
často	často	k6eAd1	často
používáno	používat	k5eAaImNgNnS	používat
také	také	k6eAd1	také
hanlivého	hanlivý	k2eAgNnSc2d1	hanlivé
označení	označení	k1gNnSc2	označení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
sloh	sloh	k1gInSc4	sloh
copový	copový	k2eAgInSc4d1	copový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
sloh	sloh	k1gInSc4	sloh
parukový	parukový	k2eAgInSc4d1	parukový
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristiku	charakteristika	k1gFnSc4	charakteristika
baroka	baroko	k1gNnSc2	baroko
jako	jako	k9	jako
pozdního	pozdní	k2eAgInSc2d1	pozdní
<g/>
,	,	kIx,	,
krouceného	kroucený	k2eAgInSc2d1	kroucený
a	a	k8xC	a
pravidla	pravidlo	k1gNnSc2	pravidlo
překračujícího	překračující	k2eAgInSc2d1	překračující
slohu	sloh	k1gInSc2	sloh
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnPc2	století
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Birnbaum	Birnbaum	k1gInSc1	Birnbaum
při	při	k7c6	při
formulaci	formulace	k1gFnSc6	formulace
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
barokní	barokní	k2eAgFnSc2d1	barokní
teorie	teorie	k1gFnSc2	teorie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
rysy	rys	k1gInPc1	rys
barokního	barokní	k2eAgNnSc2d1	barokní
umění	umění	k1gNnSc2	umění
už	už	k9	už
jsou	být	k5eAaImIp3nP	být
geneticky	geneticky	k6eAd1	geneticky
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
renesančním	renesanční	k2eAgNnSc6d1	renesanční
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
baroko	baroko	k1gNnSc1	baroko
je	být	k5eAaImIp3nS	být
rozšířením	rozšíření	k1gNnSc7	rozšíření
a	a	k8xC	a
domyšlením	domyšlení	k1gNnSc7	domyšlení
renesančních	renesanční	k2eAgFnPc2d1	renesanční
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
tendencí	tendence	k1gFnPc2	tendence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
však	však	k9	však
baroko	baroko	k1gNnSc1	baroko
zároveň	zároveň	k6eAd1	zároveň
znamená	znamenat	k5eAaImIp3nS	znamenat
popření	popření	k1gNnSc4	popření
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
sobě	se	k3xPyFc3	se
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
komponentu	komponenta	k1gFnSc4	komponenta
vycházející	vycházející	k2eAgFnSc1d1	vycházející
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
ze	z	k7c2	z
středověkého	středověký	k2eAgNnSc2d1	středověké
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
komponenta	komponenta	k1gFnSc1	komponenta
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
projevila	projevit	k5eAaPmAgFnS	projevit
především	především	k9	především
po	po	k7c6	po
Tridentském	tridentský	k2eAgInSc6d1	tridentský
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ní	on	k3xPp3gFnSc6	on
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
renesance	renesance	k1gFnSc1	renesance
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
tak	tak	k6eAd1	tak
ve	v	k7c6	v
filosofii	filosofie	k1gFnSc6	filosofie
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
po	po	k7c4	po
dosažení	dosažení	k1gNnSc4	dosažení
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
pomyslného	pomyslný	k2eAgInSc2d1	pomyslný
vrcholu	vrchol	k1gInSc2	vrchol
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1520	[number]	k4	1520
začala	začít	k5eAaPmAgFnS	začít
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
vyčerpávat	vyčerpávat	k5eAaImF	vyčerpávat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
tzv.	tzv.	kA	tzv.
manýrismu	manýrismus	k1gInSc2	manýrismus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
liboval	libovat	k5eAaImAgMnS	libovat
v	v	k7c6	v
grotesknosti	grotesknost	k1gFnSc6	grotesknost
<g/>
,	,	kIx,	,
hravosti	hravost	k1gFnSc6	hravost
<g/>
,	,	kIx,	,
optických	optický	k2eAgInPc6d1	optický
klamech	klam	k1gInPc6	klam
atp.	atp.	kA	atp.
Vznik	vznik	k1gInSc1	vznik
baroka	baroko	k1gNnSc2	baroko
je	být	k5eAaImIp3nS	být
překonáním	překonání	k1gNnSc7	překonání
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
právě	právě	k6eAd1	právě
Tridentským	tridentský	k2eAgInSc7d1	tridentský
koncilem	koncil	k1gInSc7	koncil
<g/>
)	)	kIx)	)
formulován	formulován	k2eAgInSc4d1	formulován
jasný	jasný	k2eAgInSc4d1	jasný
a	a	k8xC	a
srozumitelný	srozumitelný	k2eAgInSc4d1	srozumitelný
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
obroda	obroda	k1gFnSc1	obroda
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
často	často	k6eAd1	často
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
protireformaci	protireformace	k1gFnSc4	protireformace
hovoří	hovořit	k5eAaImIp3nS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
katolické	katolický	k2eAgFnSc3d1	katolická
reformaci	reformace	k1gFnSc3	reformace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jasné	jasný	k2eAgNnSc1d1	jasné
vytyčení	vytyčení	k1gNnSc1	vytyčení
a	a	k8xC	a
formulování	formulování	k1gNnSc1	formulování
cílů	cíl	k1gInPc2	cíl
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
pevného	pevný	k2eAgInSc2d1	pevný
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
konzistentního	konzistentní	k2eAgInSc2d1	konzistentní
uměleckého	umělecký	k2eAgInSc2d1	umělecký
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
obecně	obecně	k6eAd1	obecně
nazýváme	nazývat	k5eAaImIp1nP	nazývat
barokem	barok	k1gInSc7	barok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
představitelem	představitel	k1gMnSc7	představitel
uměleckého	umělecký	k2eAgInSc2d1	umělecký
přechodu	přechod	k1gInSc2	přechod
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
renesančního	renesanční	k2eAgNnSc2d1	renesanční
umění	umění	k1gNnSc2	umění
k	k	k7c3	k
manýrismu	manýrismus	k1gInSc3	manýrismus
a	a	k8xC	a
baroku	baroko	k1gNnSc6	baroko
je	být	k5eAaImIp3nS	být
Michelangelo	Michelangela	k1gFnSc5	Michelangela
Buonarroti	Buonarrot	k1gMnPc1	Buonarrot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
baroko	baroko	k1gNnSc1	baroko
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ruska	Rusko	k1gNnSc2	Rusko
i	i	k9	i
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
sibiřského	sibiřský	k2eAgNnSc2d1	sibiřské
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Proniklo	proniknout	k5eAaPmAgNnS	proniknout
do	do	k7c2	do
všech	všecek	k3xTgMnPc2	všecek
uměleckých	umělecký	k2eAgMnPc2d1	umělecký
a	a	k8xC	a
životních	životní	k2eAgInPc2d1	životní
projevů	projev	k1gInPc2	projev
(	(	kIx(	(
<g/>
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc4	ten
poslední	poslední	k2eAgNnSc4d1	poslední
univerzální	univerzální	k2eAgInSc4d1	univerzální
a	a	k8xC	a
jednotný	jednotný	k2eAgInSc4d1	jednotný
umělecký	umělecký	k2eAgInSc4d1	umělecký
styl	styl	k1gInSc4	styl
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tvůrčích	tvůrčí	k2eAgInPc2d1	tvůrčí
podnětů	podnět	k1gInPc2	podnět
vyšla	vyjít	k5eAaPmAgFnS	vyjít
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Příznačné	příznačný	k2eAgNnSc1d1	příznačné
je	být	k5eAaImIp3nS	být
stírání	stírání	k1gNnSc1	stírání
hranic	hranice	k1gFnPc2	hranice
mezi	mezi	k7c7	mezi
architekturou	architektura	k1gFnSc7	architektura
<g/>
,	,	kIx,	,
plastikou	plastika	k1gFnSc7	plastika
a	a	k8xC	a
malbou	malba	k1gFnSc7	malba
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vytvoření	vytvoření	k1gNnSc2	vytvoření
jednotného	jednotný	k2eAgInSc2d1	jednotný
společného	společný	k2eAgInSc2d1	společný
účinku	účinek	k1gInSc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc1d1	německá
dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc2	umění
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
trefně	trefně	k6eAd1	trefně
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Gesamtkunstwerku	Gesamtkunstwerka	k1gFnSc4	Gesamtkunstwerka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
česky	česky	k6eAd1	česky
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
celostní	celostní	k2eAgNnSc1d1	celostní
umění	umění	k1gNnSc1	umění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Malba	malba	k1gFnSc1	malba
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
iluzionistickou	iluzionistický	k2eAgFnSc7d1	iluzionistická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
předstírá	předstírat	k5eAaImIp3nS	předstírat
plastické	plastický	k2eAgInPc4d1	plastický
prvky	prvek	k1gInPc4	prvek
–	–	k?	–
architektura	architektura	k1gFnSc1	architektura
používá	používat	k5eAaImIp3nS	používat
malbu	malba	k1gFnSc4	malba
napodobených	napodobený	k2eAgInPc2d1	napodobený
stavebních	stavební	k2eAgInPc2d1	stavební
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
klenby	klenba	k1gFnPc1	klenba
<g/>
,	,	kIx,	,
žebra	žebro	k1gNnPc1	žebro
<g/>
,	,	kIx,	,
pilastry	pilastr	k1gInPc1	pilastr
<g/>
)	)	kIx)	)
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
působivosti	působivost	k1gFnSc2	působivost
<g/>
,	,	kIx,	,
plastika	plastika	k1gFnSc1	plastika
nechce	chtít	k5eNaImIp3nS	chtít
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
ojedinělé	ojedinělý	k2eAgNnSc4d1	ojedinělé
umělecké	umělecký	k2eAgNnSc4d1	umělecké
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podřizuje	podřizovat	k5eAaImIp3nS	podřizovat
se	se	k3xPyFc4	se
celku	celek	k1gInSc2	celek
a	a	k8xC	a
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
malbu	malba	k1gFnSc4	malba
(	(	kIx(	(
<g/>
drapérie	drapérie	k1gFnSc1	drapérie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
ornamentální	ornamentální	k2eAgFnSc1d1	ornamentální
štukatura	štukatura	k1gFnSc1	štukatura
<g/>
.	.	kIx.	.
</s>
<s>
Baroko	baroko	k1gNnSc1	baroko
obecně	obecně	k6eAd1	obecně
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
asymetrickým	asymetrický	k2eAgFnPc3d1	asymetrická
formám	forma	k1gFnPc3	forma
<g/>
,	,	kIx,	,
vyklenutým	vyklenutý	k2eAgNnSc7d1	vyklenuté
a	a	k8xC	a
vydutým	vydutý	k2eAgNnSc7d1	vyduté
zaoblením	zaoblení	k1gNnSc7	zaoblení
<g/>
,	,	kIx,	,
nadsazeným	nadsazený	k2eAgFnPc3d1	nadsazená
proporcím	proporce	k1gFnPc3	proporce
<g/>
,	,	kIx,	,
prostorově	prostorově	k6eAd1	prostorově
rozvinutým	rozvinutý	k2eAgNnPc3d1	rozvinuté
gestům	gesto	k1gNnPc3	gesto
<g/>
,	,	kIx,	,
efektním	efektní	k2eAgFnPc3d1	efektní
perspektivám	perspektiva	k1gFnPc3	perspektiva
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc3	využití
souhry	souhra	k1gFnSc2	souhra
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
malby	malba	k1gFnSc2	malba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
prvkem	prvek	k1gInSc7	prvek
barokního	barokní	k2eAgNnSc2d1	barokní
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
renesanční	renesanční	k2eAgNnSc1d1	renesanční
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
statické	statický	k2eAgNnSc1d1	statické
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
ideálem	ideál	k1gInSc7	ideál
je	být	k5eAaImIp3nS	být
stavba	stavba	k1gFnSc1	stavba
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
baroko	baroko	k1gNnSc1	baroko
je	být	k5eAaImIp3nS	být
dynamické	dynamický	k2eAgInPc4d1	dynamický
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
ideálem	ideál	k1gInSc7	ideál
je	být	k5eAaImIp3nS	být
ovál	ovál	k1gInSc1	ovál
či	či	k8xC	či
elipsa	elipsa	k1gFnSc1	elipsa
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgFnSc4d1	barokní
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
sochu	socha	k1gFnSc4	socha
<g/>
,	,	kIx,	,
kašnu	kašna	k1gFnSc4	kašna
atp.	atp.	kA	atp.
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vnímat	vnímat	k5eAaImF	vnímat
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
-	-	kIx~	-
obcházet	obcházet	k5eAaImF	obcházet
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgFnPc1d1	barokní
stavby	stavba	k1gFnPc1	stavba
také	také	k9	také
byly	být	k5eAaImAgFnP	být
vědomě	vědomě	k6eAd1	vědomě
komponovány	komponovat	k5eAaImNgFnP	komponovat
k	k	k7c3	k
obcházení	obcházení	k1gNnSc3	obcházení
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stálému	stálý	k2eAgNnSc3d1	stálé
objevování	objevování	k1gNnSc3	objevování
nových	nový	k2eAgFnPc2d1	nová
pohledových	pohledový	k2eAgFnPc2d1	pohledová
os	osa	k1gFnPc2	osa
<g/>
,	,	kIx,	,
k	k	k7c3	k
rafinované	rafinovaný	k2eAgFnSc3d1	rafinovaná
hře	hra	k1gFnSc3	hra
se	s	k7c7	s
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
s	s	k7c7	s
krajinou	krajina	k1gFnSc7	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
klasicismus	klasicismus	k1gInSc1	klasicismus
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
tzv.	tzv.	kA	tzv.
barokního	barokní	k2eAgInSc2d1	barokní
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
)	)	kIx)	)
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
"	"	kIx"	"
<g/>
nekonečné	konečný	k2eNgInPc4d1	nekonečný
<g/>
"	"	kIx"	"
pohledy	pohled	k1gInPc4	pohled
do	do	k7c2	do
dáli	dál	k1gFnSc2	dál
(	(	kIx(	(
<g/>
Versailles	Versailles	k1gFnSc1	Versailles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
barokní	barokní	k2eAgFnSc1d1	barokní
architektura	architektura	k1gFnSc1	architektura
je	být	k5eAaImIp3nS	být
komponována	komponovat	k5eAaImNgFnS	komponovat
také	také	k9	také
na	na	k7c6	na
pohledových	pohledový	k2eAgFnPc6d1	pohledová
osách	osa	k1gFnPc6	osa
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
klasicismus	klasicismus	k1gInSc1	klasicismus
cítí	cítit	k5eAaImIp3nS	cítit
"	"	kIx"	"
<g/>
nekonečný	konečný	k2eNgInSc1d1	nekonečný
výhled	výhled	k1gInSc1	výhled
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
baroko	baroko	k1gNnSc4	baroko
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
point	pointa	k1gFnPc2	pointa
de	de	k?	de
vue	vue	k?	vue
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jakýsi	jakýsi	k3yIgInSc1	jakýsi
úběžník	úběžník	k1gInSc1	úběžník
pohledu	pohled	k1gInSc2	pohled
–	–	k?	–
sochu	socha	k1gFnSc4	socha
<g/>
,	,	kIx,	,
letohrádek	letohrádek	k1gInSc4	letohrádek
<g/>
,	,	kIx,	,
kapli	kaple	k1gFnSc4	kaple
–	–	k?	–
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
rafinovanější	rafinovaný	k2eAgNnSc1d2	rafinovanější
a	a	k8xC	a
také	také	k9	také
intimnější	intimní	k2eAgFnSc4d2	intimnější
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Rokoko	rokoko	k1gNnSc1	rokoko
a	a	k8xC	a
Klasicismus	klasicismus	k1gInSc1	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
výkladů	výklad	k1gInPc2	výklad
baroko	baroko	k1gNnSc4	baroko
končí	končit	k5eAaImIp3nS	končit
již	již	k6eAd1	již
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
patrné	patrný	k2eAgNnSc1d1	patrné
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1721	[number]	k4	1721
a	a	k8xC	a
1729	[number]	k4	1729
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
blahoslavení	blahoslavení	k1gNnSc3	blahoslavení
a	a	k8xC	a
svatořečení	svatořečení	k1gNnSc3	svatořečení
nejvýznamnějšího	významný	k2eAgMnSc2d3	nejvýznamnější
českého	český	k2eAgMnSc2d1	český
barokního	barokní	k2eAgMnSc2d1	barokní
světce	světec	k1gMnSc2	světec
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tehdy	tehdy	k6eAd1	tehdy
probíhal	probíhat	k5eAaImAgInS	probíhat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
opsat	opsat	k5eAaPmF	opsat
biblickým	biblický	k2eAgNnSc7d1	biblické
podobenstvím	podobenství	k1gNnSc7	podobenství
o	o	k7c6	o
soli	sůl	k1gFnSc6	sůl
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ztratila	ztratit	k5eAaPmAgFnS	ztratit
slanost	slanost	k1gFnSc4	slanost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
vskutku	vskutku	k9	vskutku
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
patrné	patrný	k2eAgFnSc6d1	patrná
ztrátě	ztráta	k1gFnSc6	ztráta
vědomí	vědomí	k1gNnSc2	vědomí
původního	původní	k2eAgNnSc2d1	původní
zacílení	zacílení	k1gNnSc2	zacílení
a	a	k8xC	a
poslání	poslání	k1gNnSc2	poslání
<g/>
.	.	kIx.	.
</s>
<s>
Burcující	burcující	k2eAgNnSc1d1	burcující
baroko	baroko	k1gNnSc1	baroko
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
"	"	kIx"	"
<g/>
usměvavé	usměvavý	k2eAgFnSc3d1	usměvavá
<g/>
"	"	kIx"	"
rokoko	rokoko	k1gNnSc1	rokoko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
už	už	k6eAd1	už
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
než	než	k8xS	než
nebeskými	nebeský	k2eAgFnPc7d1	nebeská
věcmi	věc	k1gFnPc7	věc
zabývá	zabývat	k5eAaImIp3nS	zabývat
vlastními	vlastní	k2eAgInPc7d1	vlastní
prožitky	prožitek	k1gInPc7	prožitek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamněji	významně	k6eAd3	významně
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
proměně	proměna	k1gFnSc3	proměna
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
projevy	projev	k1gInPc7	projev
poživačného	poživačný	k2eAgNnSc2d1	poživačné
rokoka	rokoko	k1gNnSc2	rokoko
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
však	však	k9	však
asi	asi	k9	asi
opravdu	opravdu	k6eAd1	opravdu
lepší	dobrý	k2eAgNnSc1d2	lepší
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
pozdním	pozdní	k2eAgInSc6d1	pozdní
baroku	barok	k1gInSc6	barok
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
tak	tak	k9	tak
výrazné	výrazný	k2eAgFnSc3d1	výrazná
proměně	proměna	k1gFnSc3	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgNnSc1d1	pozdní
baroko	baroko	k1gNnSc1	baroko
je	být	k5eAaImIp3nS	být
virtuózní	virtuózní	k2eAgFnSc7d1	virtuózní
podobou	podoba	k1gFnSc7	podoba
slohu	sloh	k1gInSc2	sloh
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
například	například	k6eAd1	například
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
náš	náš	k3xOp1gMnSc1	náš
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
<g/>
,	,	kIx,	,
v	v	k7c6	v
malbě	malba	k1gFnSc6	malba
Václav	Václav	k1gMnSc1	Václav
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
Reiner	Reiner	k1gMnSc1	Reiner
či	či	k8xC	či
Cosmas	Cosmas	k1gMnSc1	Cosmas
Damian	Damian	k1gMnSc1	Damian
Asam	Asam	k1gMnSc1	Asam
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nesmírně	smírně	k6eNd1	smírně
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
barokní	barokní	k2eAgFnSc1d1	barokní
kultura	kultura	k1gFnSc1	kultura
svou	svůj	k3xOyFgFnSc4	svůj
sílu	síla	k1gFnSc4	síla
definitivně	definitivně	k6eAd1	definitivně
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
až	až	k9	až
hluboko	hluboko	k6eAd1	hluboko
po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pozdní	pozdní	k2eAgNnSc1d1	pozdní
baroko	baroko	k1gNnSc1	baroko
postupně	postupně	k6eAd1	postupně
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
strohý	strohý	k2eAgInSc1d1	strohý
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
klasicismus	klasicismus	k1gInSc1	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barokní	barokní	k2eAgFnSc1d1	barokní
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc4d1	barokní
architekturu	architektura	k1gFnSc4	architektura
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
ředitelé	ředitel	k1gMnPc1	ředitel
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
stavby	stavba	k1gFnSc2	stavba
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
svatopetrského	svatopetrský	k2eAgInSc2d1	svatopetrský
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
Giacomo	Giacoma	k1gFnSc5	Giacoma
della	dello	k1gNnPc1	dello
Porta	porta	k1gFnSc1	porta
<g/>
,	,	kIx,	,
Jacopo	Jacopa	k1gFnSc5	Jacopa
Barozzi	Barozze	k1gFnSc4	Barozze
da	da	k?	da
Vignola	Vignola	k1gFnSc1	Vignola
–	–	k?	–
také	také	k9	také
autoři	autor	k1gMnPc1	autor
vzorového	vzorový	k2eAgInSc2d1	vzorový
jezuitského	jezuitský	k2eAgInSc2d1	jezuitský
chrámu	chrám	k1gInSc2	chrám
Il	Il	k1gMnSc1	Il
Gesù	Gesù	k1gMnSc1	Gesù
<g/>
,	,	kIx,	,
a	a	k8xC	a
Carlo	Carlo	k1gNnSc1	Carlo
Maderno	Maderna	k1gFnSc5	Maderna
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
sloup	sloup	k1gInSc4	sloup
před	před	k7c7	před
bazilikou	bazilika	k1gFnSc7	bazilika
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
stavbu	stavba	k1gFnSc4	stavba
nesčetných	sčetný	k2eNgInPc2d1	nesčetný
mariánských	mariánský	k2eAgInPc2d1	mariánský
sloupů	sloup	k1gInPc2	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
barokními	barokní	k2eAgMnPc7d1	barokní
architekty	architekt	k1gMnPc7	architekt
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
trojice	trojice	k1gFnSc1	trojice
Italů	Ital	k1gMnPc2	Ital
<g/>
:	:	kIx,	:
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
také	také	k9	také
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
<g/>
)	)	kIx)	)
Gian	Gian	k1gMnSc1	Gian
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Bernini	Bernin	k2eAgMnPc1d1	Bernin
<g/>
,	,	kIx,	,
Francesco	Francesco	k6eAd1	Francesco
Borromini	Borromin	k2eAgMnPc1d1	Borromin
a	a	k8xC	a
Guarino	Guarino	k1gNnSc4	Guarino
Guarini	Guarin	k2eAgMnPc1d1	Guarin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgMnPc4	tento
vrcholné	vrcholný	k2eAgMnPc4d1	vrcholný
italské	italský	k2eAgMnPc4d1	italský
architekty	architekt	k1gMnPc4	architekt
navazuje	navazovat	k5eAaImIp3nS	navazovat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
významných	významný	k2eAgMnPc2d1	významný
italských	italský	k2eAgMnPc2d1	italský
i	i	k8xC	i
zaalpských	zaalpský	k2eAgMnPc2d1	zaalpský
architektů	architekt	k1gMnPc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
Pietro	Pietro	k1gNnSc1	Pietro
da	da	k?	da
Cortona	Corton	k1gMnSc2	Corton
<g/>
,	,	kIx,	,
Filippo	Filippa	k1gFnSc5	Filippa
Juvarra	Juvarra	k1gMnSc1	Juvarra
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Bernhard	Bernhard	k1gMnSc1	Bernhard
Fischer	Fischer	k1gMnSc1	Fischer
von	von	k1gInSc4	von
Erlach	Erlach	k1gMnSc1	Erlach
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Lukas	Lukas	k1gMnSc1	Lukas
von	von	k1gInSc4	von
Hildebrandt	Hildebrandt	k1gMnSc1	Hildebrandt
<g/>
,	,	kIx,	,
Balthasar	Balthasar	k1gMnSc1	Balthasar
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
rozvětvená	rozvětvený	k2eAgFnSc1d1	rozvětvená
rodina	rodina	k1gFnSc1	rodina
Dientzenhoferů	Dientzenhofer	k1gMnPc2	Dientzenhofer
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
<g/>
,	,	kIx,	,
působili	působit	k5eAaImAgMnP	působit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
architekty	architekt	k1gMnPc7	architekt
působícími	působící	k2eAgMnPc7d1	působící
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
Jan	Jan	k1gMnSc1	Jan
Blažej	Blažej	k1gMnSc1	Blažej
Santini-Aichel	Santini-Aichel	k1gMnSc1	Santini-Aichel
<g/>
,	,	kIx,	,
Giovanni	Giovann	k1gMnPc1	Giovann
Battista	Battista	k1gMnSc1	Battista
Alliprandi	Alliprand	k1gMnPc1	Alliprand
a	a	k8xC	a
František	František	k1gMnSc1	František
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Kaňka	Kaňka	k1gMnSc1	Kaňka
<g/>
.	.	kIx.	.
projev	projev	k1gInSc1	projev
monumentality	monumentalita	k1gFnSc2	monumentalita
<g/>
:	:	kIx,	:
snaha	snaha	k1gFnSc1	snaha
ohromit	ohromit	k5eAaPmF	ohromit
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
citovost	citovost	k1gFnSc4	citovost
až	až	k8xS	až
exaltovanost	exaltovanost	k1gFnSc4	exaltovanost
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
patos	patos	k1gInSc4	patos
<g/>
,	,	kIx,	,
nadsázka	nadsázka	k1gFnSc1	nadsázka
kostely	kostel	k1gInPc1	kostel
měly	mít	k5eAaImAgInP	mít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
<g />
.	.	kIx.	.
</s>
<s>
obraz	obraz	k1gInSc1	obraz
nebe	nebe	k1gNnSc2	nebe
přenesený	přenesený	k2eAgInSc1d1	přenesený
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
paláce	palác	k1gInPc1	palác
a	a	k8xC	a
zámky	zámek	k1gInPc1	zámek
reprezentovaly	reprezentovat	k5eAaImAgInP	reprezentovat
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
moc	moc	k6eAd1	moc
půdorysem	půdorys	k1gInSc7	půdorys
staveb	stavba	k1gFnPc2	stavba
byla	být	k5eAaImAgFnS	být
elipsa	elipsa	k1gFnSc1	elipsa
nebo	nebo	k8xC	nebo
průniky	průnik	k1gInPc1	průnik
elips	elipsa	k1gFnPc2	elipsa
(	(	kIx(	(
<g/>
nekonečno	nekonečno	k1gNnSc1	nekonečno
<g/>
)	)	kIx)	)
častý	častý	k2eAgInSc1d1	častý
architektonický	architektonický	k2eAgInSc1d1	architektonický
prvek	prvek	k1gInSc1	prvek
<g/>
:	:	kIx,	:
kopule	kopule	k1gFnSc1	kopule
<g/>
,	,	kIx,	,
putto	putto	k1gNnSc1	putto
drahé	drahý	k2eAgInPc1d1	drahý
materiály	materiál	k1gInPc1	materiál
<g/>
:	:	kIx,	:
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
vzácná	vzácný	k2eAgNnPc4d1	vzácné
dřeva	dřevo	k1gNnPc4	dřevo
<g/>
,	,	kIx,	,
barevné	barevný	k2eAgInPc4d1	barevný
mramory	mramor	k1gInPc4	mramor
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
barokním	barokní	k2eAgMnPc3d1	barokní
malířům	malíř	k1gMnPc3	malíř
patří	patřit	k5eAaImIp3nP	patřit
Caravaggio	Caravaggio	k1gMnSc1	Caravaggio
<g/>
,	,	kIx,	,
Diego	Diego	k1gMnSc1	Diego
Velázquez	Velázquez	k1gMnSc1	Velázquez
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Paul	Paul	k1gMnSc1	Paul
Rubens	Rubens	k1gInSc4	Rubens
<g/>
,	,	kIx,	,
Rembrandt	Rembrandt	k2eAgInSc4d1	Rembrandt
van	van	k1gInSc4	van
Rijn	Rijna	k1gFnPc2	Rijna
<g/>
,	,	kIx,	,
Giovanni	Giovann	k1gMnPc1	Giovann
Battista	Battista	k1gMnSc1	Battista
Tiepolo	Tiepola	k1gFnSc5	Tiepola
<g/>
,	,	kIx,	,
Pietro	Pietro	k1gNnSc1	Pietro
da	da	k?	da
Cortona	Cortona	k1gFnSc1	Cortona
<g/>
,	,	kIx,	,
Pieter	Pieter	k1gMnSc1	Pieter
Claesz	Claesz	k1gMnSc1	Claesz
nebo	nebo	k8xC	nebo
Andrea	Andrea	k1gFnSc1	Andrea
Pozzo	Pozza	k1gFnSc5	Pozza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
působili	působit	k5eAaImAgMnP	působit
především	především	k6eAd1	především
Karel	Karel	k1gMnSc1	Karel
Škréta	Škréta	k1gMnSc1	Škréta
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Brandl	Brandl	k1gMnSc1	Brandl
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
Reiner	Reiner	k1gMnSc1	Reiner
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
znakem	znak	k1gInSc7	znak
barokního	barokní	k2eAgNnSc2d1	barokní
sochařství	sochařství	k1gNnSc2	sochařství
jsou	být	k5eAaImIp3nP	být
rozevláté	rozevlátý	k2eAgInPc1d1	rozevlátý
šaty	šat	k1gInPc1	šat
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
synonymem	synonymum	k1gNnSc7	synonymum
barokního	barokní	k2eAgNnSc2d1	barokní
sochařství	sochařství	k1gNnSc2	sochařství
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
jednou	jednou	k6eAd1	jednou
zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
Gian	Gian	k1gInSc1	Gian
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Bernini	Bernin	k2eAgMnPc5d1	Bernin
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
Ital	Ital	k1gMnSc1	Ital
svým	svůj	k3xOyFgInSc7	svůj
slohotvorným	slohotvorný	k2eAgInSc7d1	slohotvorný
významem	význam	k1gInSc7	význam
prakticky	prakticky	k6eAd1	prakticky
závazně	závazně	k6eAd1	závazně
určil	určit	k5eAaPmAgInS	určit
podobu	podoba	k1gFnSc4	podoba
barokní	barokní	k2eAgFnSc2d1	barokní
sochy	socha	k1gFnSc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
navazují	navazovat	k5eAaImIp3nP	navazovat
takřka	takřka	k6eAd1	takřka
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
barokní	barokní	k2eAgMnPc1d1	barokní
sochaři	sochař	k1gMnPc1	sochař
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
našeho	náš	k3xOp1gMnSc2	náš
nejvýznamnějšího	významný	k2eAgMnSc2d3	nejvýznamnější
sochaře	sochař	k1gMnSc2	sochař
Matyáše	Matyáš	k1gMnSc2	Matyáš
Bernarda	Bernard	k1gMnSc2	Bernard
Brauna	Braun	k1gMnSc2	Braun
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
českým	český	k2eAgMnSc7d1	český
sochařem	sochař	k1gMnSc7	sochař
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Brokoff	Brokoff	k1gMnSc1	Brokoff
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
spíše	spíše	k9	spíše
klasičtěji	klasicky	k6eAd2	klasicky
chápané	chápaný	k2eAgNnSc4d1	chápané
sochařství	sochařství	k1gNnSc4	sochařství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
Braunovým	Braunův	k2eAgInSc7d1	Braunův
sochařským	sochařský	k2eAgInSc7d1	sochařský
protikladem	protiklad	k1gInSc7	protiklad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mostech	most	k1gInPc6	most
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
socha	socha	k1gFnSc1	socha
nového	nový	k2eAgMnSc2d1	nový
patrona	patron	k1gMnSc2	patron
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barokní	barokní	k2eAgNnSc1d1	barokní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barokní	barokní	k2eAgFnSc1d1	barokní
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
skladatele	skladatel	k1gMnSc4	skladatel
barokní	barokní	k2eAgFnSc2d1	barokní
hudby	hudba	k1gFnSc2	hudba
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
konec	konec	k1gInSc4	konec
barokní	barokní	k2eAgFnSc2d1	barokní
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Přidat	přidat	k5eAaPmF	přidat
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
lze	lze	k6eAd1	lze
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
slunného	slunný	k2eAgMnSc4d1	slunný
Itala	Ital	k1gMnSc4	Ital
<g/>
"	"	kIx"	"
Antonia	Antonio	k1gMnSc4	Antonio
Vivaldiho	Vivaldi	k1gMnSc4	Vivaldi
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc4	tvůrce
mnoha	mnoho	k4c2	mnoho
barokních	barokní	k2eAgNnPc2d1	barokní
oratorií	oratorium	k1gNnPc2	oratorium
Georga	Georg	k1gMnSc2	Georg
Friedricha	Friedrich	k1gMnSc2	Friedrich
Händela	Händela	k1gFnSc1	Händela
nebo	nebo	k8xC	nebo
Claudia	Claudia	k1gFnSc1	Claudia
Monteverdiho	Monteverdi	k1gMnSc2	Monteverdi
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
tvůrců	tvůrce	k1gMnPc2	tvůrce
se	se	k3xPyFc4	se
rozhodně	rozhodně	k6eAd1	rozhodně
sluší	slušet	k5eAaImIp3nS	slušet
zmínit	zmínit	k5eAaPmF	zmínit
Adama	Adam	k1gMnSc4	Adam
Michnu	Michna	k1gMnSc4	Michna
z	z	k7c2	z
Otradovic	Otradovice	k1gFnPc2	Otradovice
<g/>
,	,	kIx,	,
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Matěje	Matěj	k1gMnSc2	Matěj
Černohorského	Černohorský	k1gMnSc2	Černohorský
a	a	k8xC	a
především	především	k6eAd1	především
nejvýznamnějšího	významný	k2eAgMnSc2d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
:	:	kIx,	:
Jana	Jana	k1gFnSc1	Jana
Dismase	Dismasa	k1gFnSc3	Dismasa
Zelenku	zelenka	k1gFnSc4	zelenka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barokní	barokní	k2eAgFnSc1d1	barokní
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgNnSc1d1	barokní
kázání	kázání	k1gNnSc1	kázání
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
květnatou	květnatý	k2eAgFnSc7d1	květnatá
mluvou	mluva	k1gFnSc7	mluva
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
stylovými	stylový	k2eAgInPc7d1	stylový
prostředky	prostředek	k1gInPc7	prostředek
jsou	být	k5eAaImIp3nP	být
opakování	opakování	k1gNnSc1	opakování
(	(	kIx(	(
<g/>
repetitio	repetitio	k1gNnSc1	repetitio
<g/>
)	)	kIx)	)
a	a	k8xC	a
hromadění	hromadění	k1gNnSc4	hromadění
epitet	epiteton	k1gNnPc2	epiteton
<g/>
,	,	kIx,	,
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
emblémů	emblém	k1gInPc2	emblém
<g/>
,	,	kIx,	,
alegorií	alegorie	k1gFnPc2	alegorie
a	a	k8xC	a
metafor	metafora	k1gFnPc2	metafora
<g/>
.	.	kIx.	.
</s>
<s>
Bajky	bajka	k1gFnPc1	bajka
a	a	k8xC	a
pohádky	pohádka	k1gFnPc1	pohádka
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
ponejvíce	ponejvíce	k6eAd1	ponejvíce
morální	morální	k2eAgNnSc4d1	morální
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
přirovnání	přirovnání	k1gNnSc2	přirovnání
a	a	k8xC	a
pořekadel	pořekadlo	k1gNnPc2	pořekadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
forma	forma	k1gFnSc1	forma
a	a	k8xC	a
myšlenková	myšlenkový	k2eAgFnSc1d1	myšlenková
struktura	struktura	k1gFnSc1	struktura
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
kázání	kázání	k1gNnPc2	kázání
pevně	pevně	k6eAd1	pevně
daná	daný	k2eAgFnSc1d1	daná
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kazatelů	kazatel	k1gMnPc2	kazatel
užívá	užívat	k5eAaImIp3nS	užívat
narážky	narážka	k1gFnPc4	narážka
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
Bibli	bible	k1gFnSc4	bible
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
na	na	k7c4	na
antická	antický	k2eAgNnPc4d1	antické
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
přímo	přímo	k6eAd1	přímo
cituje	citovat	k5eAaBmIp3nS	citovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
z	z	k7c2	z
kázání	kázání	k1gNnSc2	kázání
stává	stávat	k5eAaImIp3nS	stávat
hromadný	hromadný	k2eAgInSc4d1	hromadný
sdělovací	sdělovací	k2eAgInSc4d1	sdělovací
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
knihtisku	knihtisk	k1gInSc2	knihtisk
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
také	také	k9	také
záměr	záměr	k1gInSc1	záměr
kazatelův	kazatelův	k2eAgInSc1d1	kazatelův
<g/>
,	,	kIx,	,
k	k	k7c3	k
teologickému	teologický	k2eAgNnSc3d1	teologické
poselství	poselství	k1gNnSc3	poselství
se	se	k3xPyFc4	se
v	v	k7c6	v
kázání	kázání	k1gNnSc6	kázání
druží	družit	k5eAaImIp3nS	družit
také	také	k9	také
poselství	poselství	k1gNnSc4	poselství
politické	politický	k2eAgFnSc2d1	politická
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Církve	církev	k1gFnSc2	církev
tohoto	tento	k3xDgInSc2	tento
potenciálu	potenciál	k1gInSc2	potenciál
kázání	kázání	k1gNnSc6	kázání
využívaly	využívat	k5eAaImAgFnP	využívat
<g/>
;	;	kIx,	;
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zavedla	zavést	k5eAaPmAgFnS	zavést
pro	pro	k7c4	pro
kněze	kněz	k1gMnPc4	kněz
závazek	závazek	k1gInSc4	závazek
kázat	kázat	k5eAaImF	kázat
jako	jako	k8xC	jako
povinnost	povinnost	k1gFnSc4	povinnost
na	na	k7c6	na
Tridentském	tridentský	k2eAgInSc6d1	tridentský
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Spektrum	spektrum	k1gNnSc1	spektrum
témat	téma	k1gNnPc2	téma
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Násilná	násilný	k2eAgFnSc1d1	násilná
rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
<g/>
,	,	kIx,	,
centralizace	centralizace	k1gFnSc1	centralizace
státu	stát	k1gInSc2	stát
a	a	k8xC	a
klasicismus	klasicismus	k1gInSc1	klasicismus
komplexu	komplex	k1gInSc2	komplex
El	Ela	k1gFnPc2	Ela
Escorial	Escorial	k1gMnSc1	Escorial
byly	být	k5eAaImAgFnP	být
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
model	model	k1gInSc4	model
iberské	iberský	k2eAgFnSc2d1	iberská
architektury	architektura	k1gFnSc2	architektura
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
je	on	k3xPp3gFnPc4	on
výraznější	výrazný	k2eAgFnPc4d2	výraznější
hierarchizace	hierarchizace	k1gFnPc4	hierarchizace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
díky	díky	k7c3	díky
plastickému	plastický	k2eAgNnSc3d1	plastické
zpracováni	zpracovat	k5eAaPmNgMnP	zpracovat
stěn	stěna	k1gFnPc2	stěna
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Takhle	takhle	k6eAd1	takhle
se	se	k3xPyFc4	se
baroko	baroko	k1gNnSc1	baroko
dostává	dostávat	k5eAaImIp3nS	dostávat
i	i	k9	i
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
ale	ale	k9	ale
projevuje	projevovat	k5eAaImIp3nS	projevovat
dále	daleko	k6eAd2	daleko
ode	ode	k7c2	ode
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Granadě	Granada	k1gFnSc6	Granada
klasické	klasický	k2eAgInPc1d1	klasický
prvky	prvek	k1gInPc1	prvek
obohacují	obohacovat	k5eAaImIp3nP	obohacovat
o	o	k7c4	o
dekorativní	dekorativní	k2eAgInPc4d1	dekorativní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
je	být	k5eAaImIp3nS	být
Francisco	Francisco	k1gMnSc1	Francisco
Hurtado	Hurtada	k1gFnSc5	Hurtada
Izquierdo	Izquierdo	k1gNnSc5	Izquierdo
<g/>
.	.	kIx.	.
</s>
