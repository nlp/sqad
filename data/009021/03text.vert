<p>
<s>
Dozimetr	dozimetr	k1gInSc1	dozimetr
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnPc4	zařízení
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
dávek	dávka	k1gFnPc2	dávka
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c4	v
lékařství	lékařství	k1gNnSc4	lékařství
a	a	k8xC	a
vojenství	vojenství	k1gNnSc4	vojenství
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
hodnoty	hodnota	k1gFnSc2	hodnota
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
.	.	kIx.	.
</s>
<s>
Dozimetr	dozimetr	k1gInSc1	dozimetr
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
změn	změna	k1gFnPc2	změna
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
obsažené	obsažený	k2eAgInPc1d1	obsažený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dozimetry	dozimetr	k1gInPc1	dozimetr
digitální	digitální	k2eAgInPc1d1	digitální
a	a	k8xC	a
filmové	filmový	k2eAgInPc1d1	filmový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dozimetrie	dozimetrie	k1gFnSc1	dozimetrie
</s>
</p>
<p>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
dozimetr	dozimetr	k1gInSc1	dozimetr
</s>
</p>
<p>
<s>
Prstový	prstový	k2eAgInSc1d1	prstový
dozimetr	dozimetr	k1gInSc1	dozimetr
</s>
</p>
<p>
<s>
Geiger-Müllerův	Geiger-Müllerův	k2eAgInSc1d1	Geiger-Müllerův
čítač	čítač	k1gInSc1	čítač
</s>
</p>
<p>
<s>
Scintilometr	Scintilometr	k1gInSc1	Scintilometr
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dozimetr	dozimetr	k1gInSc1	dozimetr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
dozimetru	dozimetr	k1gInSc6	dozimetr
RBGT	RBGT	kA	RBGT
62	[number]	k4	62
<g/>
a	a	k8xC	a
</s>
</p>
<p>
<s>
http://fbmi.sirdik.org/1-kapitola/16/166.html	[url]	k1gMnSc1	http://fbmi.sirdik.org/1-kapitola/16/166.html
</s>
</p>
