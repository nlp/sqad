<s>
Chinovník	chinovník	k1gInSc1	chinovník
(	(	kIx(	(
<g/>
Cinchona	Cinchona	k1gFnSc1	Cinchona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
také	také	k9	také
chininovník	chininovník	k1gMnSc1	chininovník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
mořenovité	mořenovitý	k2eAgFnSc2d1	mořenovitý
<g/>
.	.	kIx.	.
</s>
