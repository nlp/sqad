<p>
<s>
Chinovník	chinovník	k1gInSc1	chinovník
(	(	kIx(	(
<g/>
Cinchona	Cinchona	k1gFnSc1	Cinchona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
také	také	k9	také
chininovník	chininovník	k1gMnSc1	chininovník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
mořenovité	mořenovitý	k2eAgFnSc2d1	mořenovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
stromy	strom	k1gInPc1	strom
nebo	nebo	k8xC	nebo
keře	keř	k1gInPc1	keř
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
vstřícnými	vstřícný	k2eAgInPc7d1	vstřícný
listy	list	k1gInPc7	list
a	a	k8xC	a
pětičetnými	pětičetný	k2eAgInPc7d1	pětičetný
květy	květ	k1gInPc7	květ
v	v	k7c6	v
bohatých	bohatý	k2eAgNnPc6d1	bohaté
vrcholových	vrcholový	k2eAgNnPc6d1	vrcholové
květenstvích	květenství	k1gNnPc6	květenství
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
tobolka	tobolka	k1gFnSc1	tobolka
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
24	[number]	k4	24
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Kůra	kůra	k1gFnSc1	kůra
chinovníků	chinovník	k1gInPc2	chinovník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
účinné	účinný	k2eAgInPc4d1	účinný
alkaloidy	alkaloid	k1gInPc4	alkaloid
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
chinin	chinin	k1gInSc1	chinin
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
zejména	zejména	k9	zejména
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
malárie	malárie	k1gFnSc2	malárie
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
chinovník	chinovník	k1gInSc1	chinovník
lékařský	lékařský	k2eAgInSc1d1	lékařský
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
chininu	chinin	k1gInSc2	chinin
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
převážně	převážně	k6eAd1	převážně
pěstován	pěstovat	k5eAaImNgInS	pěstovat
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
druh	druh	k1gInSc1	druh
Cinchona	Cinchon	k1gMnSc2	Cinchon
calisaya	calisayus	k1gMnSc2	calisayus
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgInSc4d1	vyznačující
se	se	k3xPyFc4	se
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
alkaloidů	alkaloid	k1gInPc2	alkaloid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Chinovníky	chinovník	k1gInPc1	chinovník
jsou	být	k5eAaImIp3nP	být
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
stromy	strom	k1gInPc1	strom
nebo	nebo	k8xC	nebo
keře	keř	k1gInPc1	keř
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
<g/>
,	,	kIx,	,
vstřícnými	vstřícný	k2eAgInPc7d1	vstřícný
<g/>
,	,	kIx,	,
řapíkatými	řapíkatý	k2eAgInPc7d1	řapíkatý
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
opadavé	opadavý	k2eAgInPc1d1	opadavý
<g/>
,	,	kIx,	,
interpetiolární	interpetiolární	k2eAgInPc1d1	interpetiolární
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
pětičetné	pětičetný	k2eAgInPc1d1	pětičetný
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgInPc1d1	uspořádaný
v	v	k7c6	v
bohatých	bohatý	k2eAgInPc6d1	bohatý
<g/>
,	,	kIx,	,
vrcholových	vrcholový	k2eAgMnPc6d1	vrcholový
<g/>
,	,	kIx,	,
latovitých	latovitý	k2eAgNnPc6d1	latovité
květenstvích	květenství	k1gNnPc6	květenství
se	s	k7c7	s
vstřícnými	vstřícný	k2eAgFnPc7d1	vstřícná
větévkami	větévka	k1gFnPc7	větévka
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
zvonkovitý	zvonkovitý	k2eAgMnSc1d1	zvonkovitý
<g/>
,	,	kIx,	,
zakončený	zakončený	k2eAgMnSc1d1	zakončený
5	[number]	k4	5
zuby	zub	k1gInPc7	zub
či	či	k8xC	či
laloky	lalok	k1gInPc7	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
řepicovitá	řepicovitý	k2eAgFnSc1d1	řepicovitá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
pýřitá	pýřitý	k2eAgFnSc1d1	pýřitá
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
lysá	lysat	k5eAaImIp3nS	lysat
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
zanořené	zanořený	k2eAgFnPc1d1	zanořená
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
vyčnívají	vyčnívat	k5eAaImIp3nP	vyčnívat
z	z	k7c2	z
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2	[number]	k4	2
komůrky	komůrka	k1gFnPc4	komůrka
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
vajíčky	vajíčko	k1gNnPc7	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
tobolka	tobolka	k1gFnSc1	tobolka
pukající	pukající	k2eAgFnSc1d1	pukající
2	[number]	k4	2
chlopněmi	chlopeň	k1gFnPc7	chlopeň
a	a	k8xC	a
obsahující	obsahující	k2eAgFnSc7d1	obsahující
mnoho	mnoho	k4c1	mnoho
křídlatých	křídlatý	k2eAgNnPc2d1	křídlaté
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
blízce	blízce	k6eAd1	blízce
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
rodů	rod	k1gInPc2	rod
se	se	k3xPyFc4	se
tobolky	tobolka	k1gFnPc1	tobolka
otevírají	otevírat	k5eAaImIp3nP	otevírat
od	od	k7c2	od
báze	báze	k1gFnSc2	báze
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
24	[number]	k4	24
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Americe	Amerika	k1gFnSc6	Amerika
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
od	od	k7c2	od
Kostariky	Kostarika	k1gFnSc2	Kostarika
po	po	k7c6	po
Bolívii	Bolívie	k1gFnSc6	Bolívie
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
chinovník	chinovník	k1gInSc1	chinovník
pýřitý	pýřitý	k2eAgInSc1d1	pýřitý
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
dávné	dávný	k2eAgFnSc2d1	dávná
introdukce	introdukce	k1gFnSc2	introdukce
v	v	k7c6	v
předkolumbovských	předkolumbovský	k2eAgFnPc6d1	předkolumbovská
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc4d1	obsahová
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
účinek	účinek	k1gInSc4	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
chinovníku	chinovník	k1gInSc2	chinovník
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
celkem	celkem	k6eAd1	celkem
25	[number]	k4	25
různých	různý	k2eAgInPc2d1	různý
chinolových	chinolový	k2eAgInPc2d1	chinolový
alkaloidů	alkaloid	k1gInPc2	alkaloid
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
chinin	chinin	k1gInSc1	chinin
<g/>
,	,	kIx,	,
chinidin	chinidin	k1gInSc1	chinidin
<g/>
,	,	kIx,	,
cinchonidin	cinchonidin	k1gInSc1	cinchonidin
a	a	k8xC	a
cinchonin	cinchonin	k1gInSc1	cinchonin
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
celkový	celkový	k2eAgInSc1d1	celkový
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
vybraných	vybraný	k2eAgInPc2d1	vybraný
druhů	druh	k1gInPc2	druh
chinovníku	chinovník	k1gInSc2	chinovník
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
obvykle	obvykle	k6eAd1	obvykle
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
%	%	kIx~	%
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
14	[number]	k4	14
až	až	k9	až
16	[number]	k4	16
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
alkaloidy	alkaloid	k1gInPc1	alkaloid
jsou	být	k5eAaImIp3nP	být
biologicky	biologicky	k6eAd1	biologicky
aktivní	aktivní	k2eAgFnPc1d1	aktivní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc1d1	významný
zejména	zejména	k9	zejména
antimalarickým	antimalarický	k2eAgInSc7d1	antimalarický
účinkem	účinek	k1gInSc7	účinek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
toxické	toxický	k2eAgFnPc1d1	toxická
pro	pro	k7c4	pro
původce	původce	k1gMnPc4	původce
malárie	malárie	k1gFnSc2	malárie
<g/>
,	,	kIx,	,
prvoky	prvok	k1gMnPc4	prvok
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Plasmodium	plasmodium	k1gNnSc1	plasmodium
<g/>
.	.	kIx.	.
</s>
<s>
Chinidin	Chinidin	k1gInSc1	Chinidin
navíc	navíc	k6eAd1	navíc
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
fibrilaci	fibrilace	k1gFnSc4	fibrilace
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
srdeční	srdeční	k2eAgFnSc6d1	srdeční
arytmii	arytmie	k1gFnSc6	arytmie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
chinolových	chinolový	k2eAgInPc2d1	chinolový
alkaloidů	alkaloid	k1gInPc2	alkaloid
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
zejména	zejména	k9	zejména
u	u	k7c2	u
chinovníku	chinovník	k1gInSc2	chinovník
lékařského	lékařský	k2eAgMnSc2d1	lékařský
a	a	k8xC	a
u	u	k7c2	u
druhů	druh	k1gInPc2	druh
Cinchona	Cinchon	k1gMnSc2	Cinchon
succirubra	succirubr	k1gMnSc2	succirubr
<g/>
,	,	kIx,	,
C.	C.	kA	C.
ledgeriana	ledgeriana	k1gFnSc1	ledgeriana
<g/>
,	,	kIx,	,
C.	C.	kA	C.
lancifolia	lancifolia	k1gFnSc1	lancifolia
a	a	k8xC	a
C.	C.	kA	C.
calisaya	calisaya	k6eAd1	calisaya
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Cinchona	Cinchon	k1gMnSc2	Cinchon
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čeledi	čeleď	k1gFnSc2	čeleď
Rubiaceae	Rubiacea	k1gInSc2	Rubiacea
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
Cinchonoideae	Cinchonoidea	k1gFnSc2	Cinchonoidea
a	a	k8xC	a
tribu	trib	k1gInSc2	trib
Cinchoneae	Cinchonea	k1gFnSc2	Cinchonea
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
rodem	rod	k1gInSc7	rod
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
výsledků	výsledek	k1gInPc2	výsledek
molekulárních	molekulární	k2eAgNnPc2d1	molekulární
studií	studio	k1gNnPc2	studio
rod	rod	k1gInSc1	rod
Ladenbergia	Ladenbergia	k1gFnSc1	Ladenbergia
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgInPc4d1	příbuzný
rody	rod	k1gInPc4	rod
náleží	náležet	k5eAaImIp3nS	náležet
Remijia	Remijia	k1gFnSc1	Remijia
<g/>
,	,	kIx,	,
Cinchonopsis	Cinchonopsis	k1gFnSc1	Cinchonopsis
<g/>
,	,	kIx,	,
Stilpnophyllum	Stilpnophyllum	k1gNnSc1	Stilpnophyllum
a	a	k8xC	a
Joosia	Joosia	k1gFnSc1	Joosia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
chinovník	chinovník	k1gInSc1	chinovník
lékařský	lékařský	k2eAgInSc1d1	lékařský
(	(	kIx(	(
<g/>
Cinchona	Cinchona	k1gFnSc1	Cinchona
officinalis	officinalis	k1gFnSc1	officinalis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
chinovník	chinovník	k1gInSc1	chinovník
pýřitý	pýřitý	k2eAgInSc1d1	pýřitý
(	(	kIx(	(
<g/>
Cinchona	Cinchona	k1gFnSc1	Cinchona
pubescens	pubescensa	k1gFnPc2	pubescensa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
a	a	k8xC	a
historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
připisují	připisovat	k5eAaImIp3nP	připisovat
objev	objev	k1gInSc1	objev
léčivých	léčivý	k2eAgInPc2d1	léčivý
účinků	účinek	k1gInPc2	účinek
Jezuitům	jezuita	k1gMnPc3	jezuita
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kůru	kůra	k1gFnSc4	kůra
chinovníku	chinovník	k1gInSc2	chinovník
používali	používat	k5eAaImAgMnP	používat
jihoameričtí	jihoamerický	k2eAgMnPc1d1	jihoamerický
Indiáni	Indián	k1gMnPc1	Indián
již	již	k6eAd1	již
v	v	k7c6	v
předkolumbovských	předkolumbovský	k2eAgFnPc6d1	předkolumbovská
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domorodé	domorodý	k2eAgFnSc6d1	domorodá
medicíně	medicína	k1gFnSc6	medicína
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
horečky	horečka	k1gFnSc2	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
začali	začít	k5eAaPmAgMnP	začít
kůru	kůra	k1gFnSc4	kůra
chinovníku	chinovník	k1gInSc2	chinovník
začali	začít	k5eAaPmAgMnP	začít
používat	používat	k5eAaImF	používat
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
malárie	malárie	k1gFnSc2	malárie
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mletá	mletý	k2eAgFnSc1d1	mletá
kůra	kůra	k1gFnSc1	kůra
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
prášek	prášek	k1gInSc1	prášek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chinin	chinin	k1gInSc1	chinin
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
poprvé	poprvé	k6eAd1	poprvé
izolován	izolovat	k5eAaBmNgMnS	izolovat
začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
chemická	chemický	k2eAgFnSc1d1	chemická
struktura	struktura	k1gFnSc1	struktura
však	však	k9	však
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
chininová	chininový	k2eAgFnSc1d1	chininová
kůra	kůra	k1gFnSc1	kůra
vyvážena	vyvážet	k5eAaImNgFnS	vyvážet
v	v	k7c6	v
obrovských	obrovský	k2eAgNnPc6d1	obrovské
množstvích	množství	k1gNnPc6	množství
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
zejména	zejména	k9	zejména
z	z	k7c2	z
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
a	a	k8xC	a
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
sklizeň	sklizeň	k1gFnSc1	sklizeň
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
poptávkou	poptávka	k1gFnSc7	poptávka
vedla	vést	k5eAaImAgFnS	vést
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
vyhubení	vyhubení	k1gNnSc3	vyhubení
divoce	divoce	k6eAd1	divoce
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
není	být	k5eNaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
chininu	chinin	k1gInSc2	chinin
chinovník	chinovník	k1gInSc1	chinovník
lékařský	lékařský	k2eAgInSc1d1	lékařský
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k2eAgInSc4d1	příbuzný
druh	druh	k1gInSc4	druh
Cinchona	Cinchona	k1gFnSc1	Cinchona
calisaya	calisaya	k1gFnSc1	calisaya
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kůra	kůra	k1gFnSc1	kůra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
množství	množství	k1gNnSc4	množství
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
východních	východní	k2eAgInPc2d1	východní
svahů	svah	k1gInPc2	svah
And	Anda	k1gFnPc2	Anda
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
a	a	k8xC	a
Bolívii	Bolívie	k1gFnSc6	Bolívie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pěstován	pěstovat	k5eAaImNgInS	pěstovat
zejména	zejména	k9	zejména
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
na	na	k7c6	na
Jávě	Jáva	k1gFnSc6	Jáva
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
chininu	chinin	k1gInSc2	chinin
a	a	k8xC	a
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
alkaloidů	alkaloid	k1gInPc2	alkaloid
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
chinidin	chinidin	k1gInSc1	chinidin
a	a	k8xC	a
cinchonidin	cinchonidin	k1gInSc1	cinchonidin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
kultivarů	kultivar	k1gInPc2	kultivar
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
různý	různý	k2eAgInSc1d1	různý
<g/>
.	.	kIx.	.
</s>
<s>
Chinovník	chinovník	k1gInSc1	chinovník
pýřitý	pýřitý	k2eAgInSc1d1	pýřitý
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
a	a	k8xC	a
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
druh	druh	k1gInSc1	druh
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
alkaloidů	alkaloid	k1gInPc2	alkaloid
však	však	k9	však
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
Chinovníková	Chinovníkový	k2eAgFnSc1d1	Chinovníkový
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
produkována	produkovat	k5eAaImNgFnS	produkovat
také	také	k9	také
v	v	k7c6	v
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
Zaire	Zair	k1gMnSc5	Zair
<g/>
,	,	kIx,	,
Bolívii	Bolívie	k1gFnSc6	Bolívie
<g/>
,	,	kIx,	,
Guatemale	Guatemala	k1gFnSc6	Guatemala
a	a	k8xC	a
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgInSc7d1	významný
druhem	druh	k1gInSc7	druh
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
chininu	chinin	k1gInSc2	chinin
i	i	k8xC	i
druh	druh	k1gInSc4	druh
C.	C.	kA	C.
micrantha	micranth	k1gMnSc2	micranth
<g/>
.	.	kIx.	.
</s>
<s>
Chinin	chinin	k1gInSc1	chinin
dodává	dodávat	k5eAaImIp3nS	dodávat
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
chuť	chuť	k1gFnSc4	chuť
nealkoholickému	alkoholický	k2eNgInSc3d1	nealkoholický
nápoji	nápoj	k1gInSc3	nápoj
zvanému	zvaný	k2eAgInSc3d1	zvaný
tonik	tonikum	k1gNnPc2	tonikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chinovník	chinovník	k1gInSc1	chinovník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Cinchona	Cinchon	k1gMnSc2	Cinchon
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
