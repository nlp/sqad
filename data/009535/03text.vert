<p>
<s>
Don	Don	k1gMnSc1	Don
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
starořecky	starořecky	k6eAd1	starořecky
Tanaï	Tanaï	k1gFnSc1	Tanaï
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Tulské	tulský	k2eAgFnSc6d1	Tulská
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lipecké	Lipecká	k1gFnSc6	Lipecká
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Voroněžské	voroněžský	k2eAgFnSc6d1	Voroněžská
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Volgogradské	volgogradský	k2eAgFnSc6d1	Volgogradská
a	a	k8xC	a
v	v	k7c6	v
Rostovské	Rostovský	k2eAgFnSc6d1	Rostovská
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
1870	[number]	k4	1870
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
řeky	řeka	k1gFnSc2	řeka
činí	činit	k5eAaImIp3nS	činit
422	[number]	k4	422
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pátou	pátá	k1gFnSc4	pátá
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
evropskou	evropský	k2eAgFnSc7d1	Evropská
řekou	řeka	k1gFnSc7	řeka
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
přítok	přítok	k1gInSc1	přítok
Azovského	azovský	k2eAgNnSc2d1	Azovské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sloce	sloka	k1gFnSc6	sloka
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
hymna	hymna	k1gFnSc1	hymna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Novomoskovsku	Novomoskovsko	k1gNnSc6	Novomoskovsko
v	v	k7c6	v
Tulské	tulský	k2eAgFnSc6d1	Tulská
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
východních	východní	k2eAgInPc6d1	východní
svazích	svah	k1gInPc6	svah
Středoruské	středoruský	k2eAgFnSc2d1	středoruský
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
nad	nad	k7c7	nad
ústím	ústí	k1gNnSc7	ústí
Tiché	Tiché	k2eAgFnSc2d1	Tiché
Sosny	sosna	k1gFnSc2	sosna
teče	téct	k5eAaImIp3nS	téct
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
úzké	úzký	k2eAgFnSc6d1	úzká
dolině	dolina	k1gFnSc6	dolina
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
břeh	břeh	k1gInSc1	břeh
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
místy	místy	k6eAd1	místy
až	až	k9	až
90	[number]	k4	90
m	m	kA	m
a	a	k8xC	a
silně	silně	k6eAd1	silně
zbrázděn	zbrázdit	k5eAaPmNgMnS	zbrázdit
stržemi	strž	k1gFnPc7	strž
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
levý	levý	k2eAgInSc4d1	levý
břeh	břeh	k1gInSc4	břeh
je	být	k5eAaImIp3nS	být
pozvolný	pozvolný	k2eAgMnSc1d1	pozvolný
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
členité	členitý	k2eAgNnSc1d1	členité
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
mnoho	mnoho	k4c1	mnoho
peřejí	peřej	k1gFnPc2	peřej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
Kalač	kalač	k1gInSc1	kalač
na	na	k7c6	na
Donu	Don	k1gInSc6	Don
se	se	k3xPyFc4	se
údolí	údolí	k1gNnSc1	údolí
znatelně	znatelně	k6eAd1	znatelně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
a	a	k8xC	a
dolina	dolina	k1gFnSc1	dolina
řeky	řeka	k1gFnSc2	řeka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
u	u	k7c2	u
Serafimovičů	Serafimovič	k1gMnPc2	Serafimovič
šířky	šířka	k1gFnSc2	šířka
až	až	k9	až
6	[number]	k4	6
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
břeh	břeh	k1gInSc1	břeh
se	se	k3xPyFc4	se
i	i	k9	i
zde	zde	k6eAd1	zde
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
větší	veliký	k2eAgFnSc7d2	veliký
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
tok	tok	k1gInSc1	tok
zakončuje	zakončovat	k5eAaImIp3nS	zakončovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
Cimljanská	Cimljanský	k2eAgFnSc1d1	Cimljanský
přehrada	přehrada	k1gFnSc1	přehrada
jež	jenž	k3xRgFnSc1	jenž
zvedla	zvednout	k5eAaPmAgFnS	zvednout
hladinu	hladina	k1gFnSc4	hladina
řeky	řeka	k1gFnSc2	řeka
o	o	k7c4	o
26	[number]	k4	26
m	m	kA	m
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
stavbu	stavba	k1gFnSc4	stavba
Volžsko-donské	volžskoonský	k2eAgFnSc2d1	volžsko-donský
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
přehradou	přehrada	k1gFnSc7	přehrada
teče	téct	k5eAaImIp3nS	téct
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
dolině	dolina	k1gFnSc6	dolina
široké	široký	k2eAgFnSc6d1	široká
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
místy	místy	k6eAd1	místy
až	až	k9	až
20	[number]	k4	20
m.	m.	k?	m.
Pod	pod	k7c7	pod
Rostovem	Rostov	k1gInSc7	Rostov
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
začíná	začínat	k5eAaImIp3nS	začínat
delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
340	[number]	k4	340
km2	km2	k4	km2
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Taganrogského	Taganrogský	k2eAgInSc2d1	Taganrogský
zálivu	záliv	k1gInSc2	záliv
Azovského	azovský	k2eAgNnSc2d1	Azovské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
horní	horní	k2eAgInSc4d1	horní
tok	tok	k1gInSc4	tok
===	===	k?	===
</s>
</p>
<p>
<s>
zleva	zleva	k6eAd1	zleva
–	–	k?	–
Voroněž	Voroněž	k1gFnSc1	Voroněž
</s>
</p>
<p>
<s>
zprava	zprava	k6eAd1	zprava
–	–	k?	–
Něprjadva	Něprjadva	k1gFnSc1	Něprjadva
<g/>
,	,	kIx,	,
Krasivaja	Krasivaj	k2eAgFnSc1d1	Krasivaj
Meča	Meča	k1gFnSc1	Meča
<g/>
,	,	kIx,	,
Sosna	sosna	k1gFnSc1	sosna
</s>
</p>
<p>
<s>
===	===	k?	===
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
===	===	k?	===
</s>
</p>
<p>
<s>
zleva	zleva	k6eAd1	zleva
–	–	k?	–
Bitjug	Bitjug	k1gMnSc1	Bitjug
<g/>
,	,	kIx,	,
Chopjor	Chopjor	k1gMnSc1	Chopjor
<g/>
,	,	kIx,	,
Medvědica	Medvědica	k1gMnSc1	Medvědica
<g/>
,	,	kIx,	,
Ilovlja	Ilovlja	k1gMnSc1	Ilovlja
</s>
</p>
<p>
<s>
zprava	zprava	k6eAd1	zprava
–	–	k?	–
Tichá	Tichá	k1gFnSc1	Tichá
Sosna	sosna	k1gFnSc1	sosna
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Kalitva	Kalitva	k1gFnSc1	Kalitva
</s>
</p>
<p>
<s>
===	===	k?	===
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
===	===	k?	===
</s>
</p>
<p>
<s>
zleva	zleva	k6eAd1	zleva
–	–	k?	–
Sal	Sal	k1gFnSc1	Sal
<g/>
,	,	kIx,	,
Manyč	Manyč	k1gInSc1	Manyč
</s>
</p>
<p>
<s>
zprava	zprava	k6eAd1	zprava
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
Doněc	Doněc	k1gFnSc1	Doněc
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
sněhové	sněhový	k2eAgFnPc1d1	sněhová
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
70	[number]	k4	70
%	%	kIx~	%
ročního	roční	k2eAgInSc2d1	roční
odtoku	odtok	k1gInSc2	odtok
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
měsíce	měsíc	k1gInPc4	měsíc
březen	březno	k1gNnPc2	březno
<g/>
,	,	kIx,	,
duben	duben	k1gInSc4	duben
a	a	k8xC	a
květen	květen	k1gInSc4	květen
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
Donu	Don	k1gInSc2	Don
neoplývá	oplývat	k5eNaImIp3nS	oplývat
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
odtok	odtok	k1gInSc1	odtok
bez	bez	k7c2	bez
započtení	započtení	k1gNnSc2	započtení
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
představoval	představovat	k5eAaImAgMnS	představovat
29,5	[number]	k4	29,5
km3	km3	k4	km3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
průměrnému	průměrný	k2eAgInSc3d1	průměrný
průtoku	průtok	k1gInSc3	průtok
935	[number]	k4	935
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
Cimljanské	Cimljanský	k2eAgFnSc2d1	Cimljanský
přehrady	přehrada	k1gFnSc2	přehrada
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
o	o	k7c6	o
cca	cca	kA	cca
200	[number]	k4	200
-	-	kIx~	-
250	[number]	k4	250
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
řeka	řeka	k1gFnSc1	řeka
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
třetině	třetina	k1gFnSc6	třetina
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
však	však	k9	však
ledy	led	k1gInPc1	led
lámou	lámat	k5eAaImIp3nP	lámat
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
řeka	řeka	k1gFnSc1	řeka
odnese	odnést	k5eAaPmIp3nS	odnést
až	až	k9	až
14	[number]	k4	14
Mt	Mt	k1gMnPc2	Mt
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
6,2	[number]	k4	6,2
Mt	Mt	k1gMnPc2	Mt
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
sklon	sklon	k1gInSc1	sklon
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
velmi	velmi	k6eAd1	velmi
pomalého	pomalý	k2eAgInSc2d1	pomalý
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dalo	dát	k5eAaPmAgNnS	dát
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
pojmenování	pojmenování	k1gNnSc1	pojmenování
"	"	kIx"	"
<g/>
Tichý	tichý	k2eAgMnSc1d1	tichý
Don	Don	k1gMnSc1	Don
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
k	k	k7c3	k
městu	město	k1gNnSc3	město
Liski	Lisk	k1gFnSc2	Lisk
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1355	[number]	k4	1355
km	km	kA	km
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
až	až	k9	až
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Chlevnoje	Chlevnoj	k1gInSc2	Chlevnoj
ještě	ještě	k6eAd1	ještě
dalších	další	k2eAgFnPc2d1	další
235	[number]	k4	235
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgInPc7d1	důležitý
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
Liski	Liske	k1gFnSc4	Liske
<g/>
,	,	kIx,	,
Kalač	kalač	k1gInSc4	kalač
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
<g/>
,	,	kIx,	,
Volgodonsk	Volgodonsk	k1gInSc4	Volgodonsk
(	(	kIx(	(
<g/>
na	na	k7c6	na
Cimljanské	Cimljanský	k2eAgFnSc6d1	Cimljanský
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rostov	Rostov	k1gInSc1	Rostov
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
<g/>
,	,	kIx,	,
Azov	Azov	k1gInSc4	Azov
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Volhou	Volha	k1gFnSc7	Volha
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
spojena	spojit	k5eAaPmNgFnS	spojit
Volžsko-donským	volžskoonský	k2eAgInSc7d1	volžsko-donský
kanálem	kanál	k1gInSc7	kanál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
105	[number]	k4	105
km	km	kA	km
a	a	k8xC	a
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
Baltským	baltský	k2eAgNnSc7d1	Baltské
<g/>
,	,	kIx,	,
Bílým	bílý	k2eAgNnSc7d1	bílé
a	a	k8xC	a
Kaspickým	kaspický	k2eAgNnSc7d1	Kaspické
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
se	se	k3xPyFc4	se
převáží	převážet	k5eAaImIp3nS	převážet
===	===	k?	===
</s>
</p>
<p>
<s>
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
–	–	k?	–
obilí	obilí	k1gNnSc4	obilí
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
cement	cement	k1gInSc4	cement
<g/>
,	,	kIx,	,
ropné	ropný	k2eAgInPc4d1	ropný
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc4d1	obchodní
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInPc4d1	stavební
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
–	–	k?	–
stavební	stavební	k2eAgInPc4d1	stavební
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
vsázka	vsázka	k1gFnSc1	vsázka
<g/>
,	,	kIx,	,
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc4d1	obchodní
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Volhu	Volha	k1gFnSc4	Volha
–	–	k?	–
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInPc4d1	stavební
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Volhy	Volha	k1gFnSc2	Volha
–	–	k?	–
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
kovová	kovový	k2eAgFnSc1d1	kovová
vsázka	vsázka	k1gFnSc1	vsázka
<g/>
,	,	kIx,	,
ruda	rudo	k1gNnSc2	rudo
<g/>
,	,	kIx,	,
hnojiva	hnojivo	k1gNnSc2	hnojivo
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
je	být	k5eAaImIp3nS	být
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
rybářství	rybářství	k1gNnSc4	rybářství
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
candáti	candát	k1gMnPc1	candát
<g/>
,	,	kIx,	,
cejni	cejn	k1gMnPc1	cejn
<g/>
,	,	kIx,	,
kapři	kapr	k1gMnPc1	kapr
<g/>
,	,	kIx,	,
ostruchy	ostrucha	k1gFnPc1	ostrucha
<g/>
,	,	kIx,	,
placky	placka	k1gFnPc1	placka
<g/>
,	,	kIx,	,
jeseter	jeseter	k1gMnSc1	jeseter
ruský	ruský	k2eAgMnSc1d1	ruský
a	a	k8xC	a
hvězdnatý	hvězdnatý	k2eAgMnSc1d1	hvězdnatý
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
a	a	k8xC	a
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
cestou	cesta	k1gFnSc7	cesta
mezi	mezi	k7c7	mezi
centrálními	centrální	k2eAgFnPc7d1	centrální
oblastmi	oblast	k1gFnPc7	oblast
současného	současný	k2eAgNnSc2d1	současné
Ruska	Rusko	k1gNnSc2	Rusko
včetně	včetně	k7c2	včetně
Povolží	Povolží	k1gNnSc2	Povolží
a	a	k8xC	a
pobřežím	pobřeží	k1gNnSc7	pobřeží
Azovské	azovský	k2eAgNnSc4d1	Azovské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
řecká	řecký	k2eAgFnSc1d1	řecká
kolonie	kolonie	k1gFnSc1	kolonie
Tanaï	Tanaï	k1gFnSc2	Tanaï
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
probíhal	probíhat	k5eAaImAgInS	probíhat
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
antickými	antický	k2eAgFnPc7d1	antická
koloniemi	kolonie	k1gFnPc7	kolonie
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
a	a	k8xC	a
na	na	k7c6	na
Tamanském	Tamanský	k2eAgInSc6d1	Tamanský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
územím	území	k1gNnSc7	území
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
procházela	procházet	k5eAaImAgFnS	procházet
cesta	cesta	k1gFnSc1	cesta
kudy	kudy	k6eAd1	kudy
pronikaly	pronikat	k5eAaImAgFnP	pronikat
hordy	horda	k1gFnPc1	horda
Hunů	Hun	k1gMnPc2	Hun
a	a	k8xC	a
Bulharů	Bulhar	k1gMnPc2	Bulhar
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
ústupu	ústup	k1gInSc6	ústup
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
osídleno	osídlit	k5eAaPmNgNnS	osídlit
Východními	východní	k2eAgInPc7d1	východní
Slovany	Slovan	k1gInPc7	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obydleli	obydlet	k5eAaPmAgMnP	obydlet
dolní	dolní	k2eAgInSc4d1	dolní
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
Chazarové	Chazarové	k?	Chazarové
<g/>
.	.	kIx.	.
</s>
<s>
Nájezdy	nájezd	k1gInPc1	nájezd
Maďarů	Maďar	k1gMnPc2	Maďar
a	a	k8xC	a
Pečeněgů	Pečeněg	k1gMnPc2	Pečeněg
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
slovanské	slovanský	k2eAgNnSc4d1	slovanské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
z	z	k7c2	z
povodí	povodí	k1gNnSc2	povodí
horního	horní	k2eAgInSc2d1	horní
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dolní	dolní	k2eAgInSc4d1	dolní
tok	tok	k1gInSc4	tok
obývali	obývat	k5eAaImAgMnP	obývat
kočovní	kočovní	k2eAgMnPc1d1	kočovní
Polovci	Polovec	k1gMnPc1	Polovec
a	a	k8xC	a
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
mongolsko-tatarské	mongolskoatarský	k2eAgFnSc2d1	mongolsko-tatarský
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Hordy	horda	k1gFnSc2	horda
a	a	k8xC	a
pustnul	pustnout	k5eAaPmAgInS	pustnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
stává	stávat	k5eAaImIp3nS	stávat
tržní	tržní	k2eAgFnSc7d1	tržní
cestou	cesta	k1gFnSc7	cesta
mezi	mezi	k7c7	mezi
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
janovskými	janovský	k2eAgFnPc7d1	janovská
koloniemi	kolonie	k1gFnPc7	kolonie
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
kolonie	kolonie	k1gFnSc1	kolonie
Tana	tanout	k5eAaImSgMnS	tanout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
začínají	začínat	k5eAaImIp3nP	začínat
vznikat	vznikat	k5eAaImF	vznikat
sídla	sídlo	k1gNnPc4	sídlo
svobodných	svobodný	k2eAgMnPc2d1	svobodný
lidí	člověk	k1gMnPc2	člověk
-	-	kIx~	-
kozáků	kozák	k1gMnPc2	kozák
a	a	k8xC	a
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
vzniká	vznikat	k5eAaImIp3nS	vznikat
Donské	donský	k2eAgNnSc1d1	donský
kozácké	kozácký	k2eAgNnSc1d1	kozácké
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
byla	být	k5eAaImAgFnS	být
řeka	řek	k1gMnSc4	řek
opěrnou	opěrný	k2eAgFnSc7d1	opěrná
základnou	základna	k1gFnSc7	základna
rolnického	rolnický	k2eAgNnSc2d1	rolnické
povstání	povstání	k1gNnSc2	povstání
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Stěnky	stěnka	k1gFnSc2	stěnka
Razina	Razino	k1gNnSc2	Razino
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1707-09	[number]	k4	1707-09
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
Bulavinskému	Bulavinský	k2eAgNnSc3d1	Bulavinský
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prudce	prudko	k6eAd1	prudko
stoupá	stoupat	k5eAaImIp3nS	stoupat
hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přechodem	přechod	k1gInSc7	přechod
kozáků	kozák	k1gInPc2	kozák
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
a	a	k8xC	a
přílivem	příliv	k1gInSc7	příliv
Rolníků	rolník	k1gMnPc2	rolník
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
vzniká	vznikat	k5eAaImIp3nS	vznikat
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
oblast	oblast	k1gFnSc1	oblast
Donbas	Donbas	k1gInSc1	Donbas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
povodí	povodí	k1gNnSc6	povodí
probíhaly	probíhat	k5eAaImAgInP	probíhat
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
jak	jak	k8xC	jak
1918-1920	[number]	k4	1918-1920
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941-1945	[number]	k4	1941-1945
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
velké	velký	k2eAgFnSc2d1	velká
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Davydov	Davydov	k1gInSc1	Davydov
L.	L.	kA	L.
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Hydrografie	hydrografie	k1gFnSc1	hydrografie
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Д	Д	k?	Д
Л	Л	k?	Л
К	К	k?	К
<g/>
,	,	kIx,	,
Г	Г	k?	Г
С	С	k?	С
<g/>
,	,	kIx,	,
т	т	k?	т
2	[number]	k4	2
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Kama	Kama	k1gFnSc1	Kama
<g/>
,	,	kIx,	,
Volha	Volha	k1gFnSc1	Volha
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
<g/>
,	,	kIx,	,
průvodce	průvodce	k1gMnSc1	průvodce
Perm	perm	k1gInSc4	perm
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
К	К	k?	К
<g/>
,	,	kIx,	,
В	В	k?	В
<g/>
,	,	kIx,	,
Д	Д	k?	Д
<g/>
.	.	kIx.	.
П	П	k?	П
<g/>
,	,	kIx,	,
П	П	k?	П
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Д	Д	k?	Д
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Don	dona	k1gFnPc2	dona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
