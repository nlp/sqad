<s>
První	první	k4xOgFnSc4	první
geneticky	geneticky	k6eAd1	geneticky
modifikovanou	modifikovaný	k2eAgFnSc4d1	modifikovaná
plodinou	plodina	k1gFnSc7	plodina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
povoleno	povolit	k5eAaPmNgNnS	povolit
komerčně	komerčně	k6eAd1	komerčně
pěstovat	pěstovat	k5eAaImF	pěstovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kukuřice	kukuřice	k1gFnSc1	kukuřice
firmy	firma	k1gFnSc2	firma
Monsanto	Monsanta	k1gFnSc5	Monsanta
typu	typ	k1gInSc2	typ
MON	MON	kA	MON
810	[number]	k4	810
<g/>
.	.	kIx.	.
</s>
