<s>
Kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
D-dur	Dur	k1gMnSc1	D-dur
(	(	kIx(	(
<g/>
plným	plný	k2eAgInSc7d1	plný
německým	německý	k2eAgInSc7d1	německý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Kanon	kanon	k1gInSc1	kanon
und	und	k?	und
Gigue	Gigu	k1gFnSc2	Gigu
in	in	k?	in
D-Dur	D-Dur	k1gMnSc1	D-Dur
für	für	k?	für
drei	drei	k1gNnPc2	drei
Violinen	Violinno	k1gNnPc2	Violinno
und	und	k?	und
Basso	Bassa	k1gFnSc5	Bassa
Continuo	Continua	k1gMnSc5	Continua
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
od	od	k7c2	od
Johanna	Johann	k1gMnSc2	Johann
Pachelbela	Pachelbel	k1gMnSc2	Pachelbel
<g/>
.	.	kIx.	.
</s>
