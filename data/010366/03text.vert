<p>
<s>
Kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
D-dur	Dur	k1gMnSc1	D-dur
(	(	kIx(	(
<g/>
plným	plný	k2eAgInSc7d1	plný
německým	německý	k2eAgInSc7d1	německý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Kanon	kanon	k1gInSc1	kanon
und	und	k?	und
Gigue	Gigu	k1gFnSc2	Gigu
in	in	k?	in
D-Dur	D-Dur	k1gMnSc1	D-Dur
für	für	k?	für
drei	drei	k1gNnPc2	drei
Violinen	Violinno	k1gNnPc2	Violinno
und	und	k?	und
Basso	Bassa	k1gFnSc5	Bassa
Continuo	Continua	k1gMnSc5	Continua
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
od	od	k7c2	od
Johanna	Johann	k1gMnSc2	Johann
Pachelbela	Pachelbel	k1gMnSc2	Pachelbel
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
sepsána	sepsat	k5eAaPmNgFnS	sepsat
v	v	k7c6	v
období	období	k1gNnSc6	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
<g/>
,	,	kIx,	,
během	během	k7c2	během
barokního	barokní	k2eAgNnSc2d1	barokní
období	období	k1gNnSc2	období
jako	jako	k8xC	jako
chrámová	chrámový	k2eAgFnSc1d1	chrámová
hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
tři	tři	k4xCgFnPc4	tři
violy	viola	k1gFnPc4	viola
a	a	k8xC	a
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k6eAd1	continuo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
přepsána	přepsat	k5eAaPmNgFnS	přepsat
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Kánon	kánon	k1gInSc1	kánon
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
originále	originál	k1gInSc6	originál
spárován	spárován	k2eAgMnSc1d1	spárován
s	s	k7c7	s
gigou	giga	k1gFnSc7	giga
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
tónině	tónina	k1gFnSc6	tónina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
hraje	hrát	k5eAaImIp3nS	hrát
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
předlohou	předloha	k1gFnSc7	předloha
i	i	k9	i
pro	pro	k7c4	pro
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
jí	jíst	k5eAaImIp3nS	jíst
inspirován	inspirován	k2eAgInSc1d1	inspirován
např.	např.	kA	např.
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
See	See	k1gMnSc1	See
You	You	k1gMnSc1	You
When	When	k1gMnSc1	When
You	You	k1gMnSc1	You
Get	Get	k1gMnSc1	Get
There	Ther	k1gInSc5	Ther
<g/>
"	"	kIx"	"
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
americkým	americký	k2eAgInSc7d1	americký
rapperem	rapper	k1gInSc7	rapper
Cooliem	Coolium	k1gNnSc7	Coolium
<g/>
.	.	kIx.	.
</s>
</p>
