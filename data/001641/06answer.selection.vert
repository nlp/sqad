<s>
Owen	Owen	k1gMnSc1	Owen
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
Kalifornia	kalifornium	k1gNnSc2	kalifornium
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
výzkumem	výzkum	k1gInSc7	výzkum
srážek	srážka	k1gFnPc2	srážka
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
návrhy	návrh	k1gInPc4	návrh
detektorů	detektor	k1gInPc2	detektor
částic	částice	k1gFnPc2	částice
přispěl	přispět	k5eAaPmAgInS	přispět
nemalou	malý	k2eNgFnSc4d1	nemalá
měrou	míra	k1gFnSc7wR	míra
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
experimentální	experimentální	k2eAgFnSc2d1	experimentální
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
