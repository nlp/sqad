<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Adolf	Adolf	k1gMnSc1	Adolf
Müller	Müller	k1gMnSc1	Müller
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1899	[number]	k4	1899
-	-	kIx~	-
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
zápasník	zápasník	k1gMnSc1	zápasník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
stylu	styl	k1gInSc6	styl
ve	v	k7c6	v
velterové	velterový	k2eAgFnSc6d1	velterová
váze	váha	k1gFnSc6	váha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
titul	titul	k1gInSc4	titul
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
stylu	styl	k1gInSc6	styl
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
váze	váha	k1gFnSc6	váha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
www.sports-reference.com	www.sportseference.com	k1gInSc4	www.sports-reference.com
</s>
</p>
