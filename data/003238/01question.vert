<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
prvky	prvek	k1gInPc4	prvek
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
ambientu	ambienta	k1gFnSc4	ambienta
nebo	nebo	k8xC	nebo
dark	dark	k1gInSc4	dark
ambientu	ambient	k1gInSc2	ambient
<g/>
?	?	kIx.	?
</s>
