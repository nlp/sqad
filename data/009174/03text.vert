<p>
<s>
Korintský	korintský	k2eAgInSc1d1	korintský
řád	řád	k1gInSc1	řád
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
klasických	klasický	k2eAgInPc2d1	klasický
architektonických	architektonický	k2eAgInPc2d1	architektonický
stylů	styl	k1gInPc2	styl
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Promítl	promítnout	k5eAaPmAgInS	promítnout
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
do	do	k7c2	do
egyptských	egyptský	k2eAgInPc2d1	egyptský
chrámů	chrám	k1gInPc2	chrám
v	v	k7c6	v
době	doba	k1gFnSc6	doba
dobytí	dobytí	k1gNnSc2	dobytí
cca	cca	kA	cca
330	[number]	k4	330
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
28	[number]	k4	28
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Korintský	korintský	k2eAgInSc4d1	korintský
řád	řád	k1gInSc4	řád
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
větší	veliký	k2eAgFnPc4d2	veliký
obliby	obliba	k1gFnPc4	obliba
až	až	k9	až
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Korinstský	Korinstský	k2eAgInSc1d1	Korinstský
styl	styl	k1gInSc1	styl
má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
Iónskému	iónský	k2eAgInSc3d1	iónský
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	s	k7c7	s
štíhlými	štíhlý	k2eAgInPc7d1	štíhlý
vysokými	vysoký	k2eAgInPc7d1	vysoký
sloupy	sloup	k1gInPc7	sloup
a	a	k8xC	a
dekorativností	dekorativnost	k1gFnSc7	dekorativnost
sloupových	sloupový	k2eAgFnPc2d1	sloupová
hlavic	hlavice	k1gFnPc2	hlavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavice	hlavice	k1gFnSc1	hlavice
je	být	k5eAaImIp3nS	být
obrůstána	obrůstat	k5eAaImNgFnS	obrůstat
trsy	trs	k1gInPc7	trs
listů	list	k1gInPc2	list
akantu	akant	k1gInSc2	akant
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
konkrétně	konkrétně	k6eAd1	konkrétně
druhů	druh	k1gInPc2	druh
paznehtníku	paznehtník	k1gInSc2	paznehtník
měkkého	měkký	k2eAgInSc2d1	měkký
(	(	kIx(	(
<g/>
v	v	k7c6	v
antickém	antický	k2eAgInSc6d1	antický
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
či	či	k8xC	či
paznehtníku	paznehtník	k1gInSc2	paznehtník
ostnitého	ostnitý	k2eAgNnSc2d1	ostnité
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
)	)	kIx)	)
–	–	k?	–
středomořských	středomořský	k2eAgInPc2d1	středomořský
bodláků	bodlák	k1gInPc2	bodlák
<g/>
;	;	kIx,	;
na	na	k7c6	na
rozích	roh	k1gInPc6	roh
jsou	být	k5eAaImIp3nP	být
voluty	voluta	k1gFnPc1	voluta
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
objevuje	objevovat	k5eAaImIp3nS	objevovat
motiv	motiv	k1gInSc4	motiv
tzv.	tzv.	kA	tzv.
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
:	:	kIx,	:
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
třetině	třetina	k1gFnSc6	třetina
sloupu	sloup	k1gInSc2	sloup
jsou	být	k5eAaImIp3nP	být
de	de	k?	de
drážek	drážka	k1gNnPc2	drážka
kanelování	kanelování	k1gNnPc2	kanelování
vloženy	vložen	k2eAgFnPc1d1	vložena
(	(	kIx(	(
<g/>
vytesány	vytesán	k2eAgFnPc1d1	vytesána
<g/>
)	)	kIx)	)
hůlky	hůlka	k1gFnPc1	hůlka
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
motiv	motiv	k1gInSc4	motiv
zubořezu	zubořez	k1gInSc2	zubořez
v	v	k7c4	v
poloměr	poloměr	k1gInSc4	poloměr
cca	cca	kA	cca
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
–	–	k?	–
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
vymezeno	vymezit	k5eAaPmNgNnS	vymezit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legendární	legendární	k2eAgInSc4d1	legendární
původ	původ	k1gInSc4	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
nespolehlivé	spolehlivý	k2eNgFnSc2d1	nespolehlivá
informace	informace	k1gFnSc2	informace
Vitruvia	Vitruvius	k1gMnSc2	Vitruvius
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
autorem	autor	k1gMnSc7	autor
tohoto	tento	k3xDgInSc2	tento
slohu	sloh	k1gInSc2	sloh
sochař	sochař	k1gMnSc1	sochař
Kallimachos	Kallimachos	k1gMnSc1	Kallimachos
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
prý	prý	k9	prý
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
najít	najít	k5eAaPmF	najít
nějaký	nějaký	k3yIgInSc4	nějaký
nový	nový	k2eAgInSc4d1	nový
architektonický	architektonický	k2eAgInSc4d1	architektonický
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Kallimachos	Kallimachos	k1gMnSc1	Kallimachos
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
nemohl	moct	k5eNaImAgInS	moct
přijít	přijít	k5eAaPmF	přijít
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
značně	značně	k6eAd1	značně
zoufalý	zoufalý	k2eAgMnSc1d1	zoufalý
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
šel	jít	k5eAaImAgMnS	jít
po	po	k7c6	po
pohřebišti	pohřebiště	k1gNnSc6	pohřebiště
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
zrak	zrak	k1gInSc1	zrak
utkvěl	utkvět	k5eAaPmAgInS	utkvět
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
dětském	dětský	k2eAgInSc6d1	dětský
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starořecké	starořecký	k2eAgFnSc2d1	starořecká
tradice	tradice	k1gFnSc2	tradice
když	když	k8xS	když
zemře	zemřít	k5eAaPmIp3nS	zemřít
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vezmou	vzít	k5eAaPmIp3nP	vzít
všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
hračky	hračka	k1gFnPc1	hračka
a	a	k8xC	a
odnesou	odnést	k5eAaPmIp3nP	odnést
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
hrob	hrob	k1gInSc4	hrob
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
Kallimachos	Kallimachos	k1gMnSc1	Kallimachos
díval	dívat	k5eAaImAgMnS	dívat
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
to	ten	k3xDgNnSc4	ten
uviděl	uvidět	k5eAaPmAgMnS	uvidět
<g/>
.	.	kIx.	.
</s>
<s>
Košík	košík	k1gInSc1	košík
s	s	k7c7	s
hračkami	hračka	k1gFnPc7	hračka
prorostly	prorůst	k5eAaPmAgFnP	prorůst
všudypřítomné	všudypřítomný	k2eAgInPc4d1	všudypřítomný
bodláky	bodlák	k1gInPc4	bodlák
a	a	k8xC	a
Kallimachos	Kallimachos	k1gInSc4	Kallimachos
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
hned	hned	k6eAd1	hned
uviděl	uvidět	k5eAaPmAgMnS	uvidět
budoucí	budoucí	k2eAgFnSc4d1	budoucí
hlavici	hlavice	k1gFnSc4	hlavice
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
staveb	stavba	k1gFnPc2	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc4	příklad
staveb	stavba	k1gFnPc2	stavba
od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
po	po	k7c4	po
moderní	moderní	k2eAgFnSc4d1	moderní
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
Dia	Dia	k1gMnSc2	Dia
Olympského	olympský	k2eAgMnSc2d1	olympský
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
</s>
</p>
<p>
<s>
Pantheon	Pantheon	k1gInSc1	Pantheon
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Madeleine	Madeleine	k1gFnSc1	Madeleine
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
</s>
</p>
<p>
<s>
Kapitol	Kapitol	k1gInSc1	Kapitol
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dórský	dórský	k2eAgInSc1d1	dórský
řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
Iónský	iónský	k2eAgInSc1d1	iónský
řád	řád	k1gInSc1	řád
</s>
</p>
<p>
<s>
Kompozitní	kompozitní	k2eAgInSc1d1	kompozitní
řád	řád	k1gInSc1	řád
</s>
</p>
