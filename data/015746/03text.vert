<s>
Push	Push	k1gMnSc1
to	ten	k3xDgNnSc4
talk	talk	k6eAd1
</s>
<s>
Push	Push	k1gMnSc1
to	to	k3xDgNnSc4
talk	talk	k1gMnSc1
(	(	kIx(
<g/>
PTT	PTT	kA
<g/>
,	,	kIx,
česky	česky	k6eAd1
„	„	k?
<g/>
stlač	stlačit	k5eAaPmRp2nS
a	a	k8xC
mluv	mluva	k1gFnPc2
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
zmáčkni	zmáčknout	k5eAaPmRp2nS
a	a	k8xC
mluv	mluvit	k5eAaImRp2nS
<g/>
“	“	k?
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
způsob	způsob	k1gInSc4
komunikace	komunikace	k1gFnSc2
po	po	k7c4
half-duplex	half-duplex	k1gInSc4
spojeních	spojení	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
možná	možný	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
vždy	vždy	k6eAd1
pouze	pouze	k6eAd1
jedním	jeden	k4xCgInSc7
směrem	směr	k1gInSc7
a	a	k8xC
chce	chtít	k5eAaImIp3nS
<g/>
-li	-li	k?
operátor	operátor	k1gInSc4
mluvit	mluvit	k5eAaImF
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
stisknout	stisknout	k5eAaPmF
tlačítko	tlačítko	k1gNnSc4
(	(	kIx(
<g/>
a	a	k8xC
po	po	k7c4
tu	ten	k3xDgFnSc4
dobu	doba	k1gFnSc4
neslyší	slyšet	k5eNaImIp3nS
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
u	u	k7c2
vysílaček	vysílačka	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
radioamatérské	radioamatérský	k2eAgFnPc1d1
radiostanice	radiostanice	k1gFnPc1
<g/>
,	,	kIx,
občanské	občanský	k2eAgFnPc1d1
radiostanice	radiostanice	k1gFnPc1
<g/>
,	,	kIx,
tedy	tedy	k9
CB	CB	kA
pracující	pracující	k2eAgFnSc1d1
v	v	k7c6
pásmu	pásmo	k1gNnSc6
27	#num#	k4
MHz	Mhz	kA
nebo	nebo	k8xC
oblíbené	oblíbený	k2eAgFnSc2d1
radiostanice	radiostanice	k1gFnSc2
PMR	PMR	kA
v	v	k7c6
pásmu	pásmo	k1gNnSc6
446	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkratka	zkratka	k1gFnSc1
PTT	PTT	kA
také	také	k9
označuje	označovat	k5eAaImIp3nS
samotné	samotný	k2eAgNnSc4d1
tlačítko	tlačítko	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc7
stlačením	stlačení	k1gNnSc7
přejde	přejít	k5eAaPmIp3nS
radiostanice	radiostanice	k1gFnSc1
z	z	k7c2
příjmu	příjem	k1gInSc2
na	na	k7c4
vysílání	vysílání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
PTT	PTT	kA
u	u	k7c2
mobilních	mobilní	k2eAgInPc2d1
operátorů	operátor	k1gInPc2
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
se	se	k3xPyFc4
zřídka	zřídka	k6eAd1
mluví	mluvit	k5eAaImIp3nS
také	také	k9
o	o	k7c6
PTT	PTT	kA
<g/>
,	,	kIx,
ovšem	ovšem	k9
má	mít	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
zásadní	zásadní	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
oproti	oproti	k7c3
radiostanicím	radiostanice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
klasické	klasický	k2eAgFnPc1d1
radiostanice	radiostanice	k1gFnPc1
pracují	pracovat	k5eAaImIp3nP
čistě	čistě	k6eAd1
na	na	k7c6
analogovém	analogový	k2eAgInSc6d1
principu	princip	k1gInSc6
–	–	k?
tedy	tedy	k9
vysílání	vysílání	k1gNnSc1
modulované	modulovaný	k2eAgFnSc2d1
radiové	radiový	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
do	do	k7c2
okolí	okolí	k1gNnSc2
a	a	k8xC
jejím	její	k3xOp3gNnSc7
zachycení	zachycení	k1gNnSc2
druhou	druhý	k4xOgFnSc7
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
PTT	PTT	kA
v	v	k7c6
mobilních	mobilní	k2eAgInPc6d1
telefonech	telefon	k1gInPc6
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
datový	datový	k2eAgInSc1d1
přenos	přenos	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
radiová	radiový	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
šíří	šířit	k5eAaImIp3nS
mezi	mezi	k7c7
telefonem	telefon	k1gInSc7
a	a	k8xC
základnovou	základnový	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
BTS	BTS	kA
<g/>
,	,	kIx,
spojení	spojení	k1gNnSc1
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
realizováno	realizován	k2eAgNnSc1d1
sítí	sítí	k1gNnSc1
operátora	operátor	k1gMnSc2
až	až	k9
k	k	k7c3
příjemci	příjemce	k1gMnSc3
na	na	k7c4
mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
či	či	k8xC
odpovídající	odpovídající	k2eAgInSc4d1
software	software	k1gInSc4
v	v	k7c6
počítači	počítač	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
vyplývají	vyplývat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
hlavní	hlavní	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
oproti	oproti	k7c3
analogovému	analogový	k2eAgInSc3d1
přenosu	přenos	k1gInSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
obě	dva	k4xCgFnPc1
hovořící	hovořící	k2eAgFnPc1d1
strany	strana	k1gFnPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
v	v	k7c6
radiovém	radiový	k2eAgInSc6d1
dosahu	dosah	k1gInSc6
(	(	kIx(
<g/>
pro	pro	k7c4
PMR	PMR	kA
v	v	k7c6
řádu	řád	k1gInSc6
kilometrů	kilometr	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
takové	takový	k3xDgNnSc1
spojení	spojení	k1gNnSc1
je	být	k5eAaImIp3nS
úplně	úplně	k6eAd1
zadarmo	zadarmo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datové	datový	k2eAgInPc1d1
přenosy	přenos	k1gInPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
šířit	šířit	k5eAaImF
všude	všude	k6eAd1
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
pokrytí	pokrytí	k1gNnSc1
signálem	signál	k1gInSc7
mobilní	mobilní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
GPRS	GPRS	kA
<g/>
/	/	kIx~
<g/>
EDGE	EDGE	kA
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
službu	služba	k1gFnSc4
zpoplatněnou	zpoplatněný	k2eAgFnSc4d1
-	-	kIx~
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Tuto	tento	k3xDgFnSc4
technologii	technologie	k1gFnSc4
do	do	k7c2
svých	svůj	k3xOyFgInPc2
telefonů	telefon	k1gInPc2
integruje	integrovat	k5eAaBmIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
pouze	pouze	k6eAd1
Nokia	Nokia	kA
<g/>
,	,	kIx,
neboť	neboť	k8xC
mobilní	mobilní	k2eAgFnSc1d1
divize	divize	k1gFnSc1
firmy	firma	k1gFnSc2
Siemens	siemens	k1gInSc1
postupem	postupem	k7c2
času	čas	k1gInSc2
zanikla	zaniknout	k5eAaPmAgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
však	však	k9
podíváme	podívat	k5eAaPmIp1nP,k5eAaImIp1nP
na	na	k7c4
poslední	poslední	k2eAgInPc4d1
modely	model	k1gInPc4
značky	značka	k1gFnSc2
<g/>
,	,	kIx,
prakticky	prakticky	k6eAd1
žádný	žádný	k3yNgInSc4
PTT	PTT	kA
nenabízí	nabízet	k5eNaImIp3nS
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
tedy	tedy	k9
pravděpodobně	pravděpodobně	k6eAd1
o	o	k7c4
dožívající	dožívající	k2eAgFnSc4d1
technologii	technologie	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
v	v	k7c6
světle	světlo	k1gNnSc6
rozšiřování	rozšiřování	k1gNnSc2
3G	3G	k4
sítí	síť	k1gFnPc2
<g/>
,	,	kIx,
pomalu	pomalu	k6eAd1
ale	ale	k8xC
jistě	jistě	k6eAd1
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Tuto	tento	k3xDgFnSc4
službu	služba	k1gFnSc4
aktuálně	aktuálně	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
v	v	k7c6
ČR	ČR	kA
pouze	pouze	k6eAd1
Telefónica	Telefónica	k1gFnSc1
O2	O2	k1gMnPc2
Czech	Czech	k1gInSc4
Republic	Republice	k1gFnPc2
pod	pod	k7c7
názvem	název	k1gInSc7
O2	O2	k1gFnSc2
Přepínám	přepínat	k5eAaImIp1nS
a	a	k8xC
U	u	k7c2
<g/>
:	:	kIx,
<g/>
fon	fon	k?
pod	pod	k7c7
názvem	název	k1gInSc7
U	U	kA
<g/>
:	:	kIx,
<g/>
fonova	fonův	k2eAgFnSc1d1
vysílačka	vysílačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
tohoto	tento	k3xDgNnSc2
spojení	spojení	k1gNnSc2
je	být	k5eAaImIp3nS
především	především	k9
možnost	možnost	k1gFnSc4
rychlého	rychlý	k2eAgNnSc2d1
navázání	navázání	k1gNnSc2
hovoru	hovor	k1gInSc2
i	i	k8xC
s	s	k7c7
několika	několik	k4yIc7
přednastavenými	přednastavený	k2eAgFnPc7d1
telefonními	telefonní	k2eAgFnPc7d1
čísly	čísnout	k5eAaPmAgFnP
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
výhoda	výhoda	k1gFnSc1
spočívá	spočívat	k5eAaImIp3nS
také	také	k9
v	v	k7c6
šifrování	šifrování	k1gNnSc6
<g/>
,	,	kIx,
tedy	tedy	k9
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
Vámi	vy	k3xPp2nPc7
definované	definovaný	k2eAgFnPc1d1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníkem	účastník	k1gMnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
uživatel	uživatel	k1gMnSc1
vybavený	vybavený	k2eAgMnSc1d1
počítačem	počítač	k1gMnSc7
s	s	k7c7
vhodnými	vhodný	k2eAgFnPc7d1
perifériemi	periférie	k1gFnPc7
a	a	k8xC
software	software	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
citelnou	citelný	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc1
účtování	účtování	k1gNnSc2
služby	služba	k1gFnSc2
-	-	kIx~
na	na	k7c4
24	#num#	k4
hodin	hodina	k1gFnPc2
za	za	k7c4
cca	cca	kA
47	#num#	k4
<g/>
,	,	kIx,
<g/>
-	-	kIx~
či	či	k8xC
na	na	k7c4
celý	celý	k2eAgInSc4d1
měsíc	měsíc	k1gInSc4
za	za	k7c4
cca	cca	kA
475	#num#	k4
<g/>
,	,	kIx,
<g/>
-	-	kIx~
(	(	kIx(
<g/>
obojí	oboj	k1gFnSc7
vč.	vč.	k?
DPH	DPH	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tarifu	tarif	k1gInSc6
nejsou	být	k5eNaImIp3nP
žádná	žádný	k3yNgNnPc1
omezení	omezení	k1gNnPc1
počtu	počet	k1gInSc2
spojení	spojení	k1gNnSc2
<g/>
,	,	kIx,
denní	denní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
hovoru	hovor	k1gInSc2
či	či	k8xC
délky	délka	k1gFnSc2
spojení	spojení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
příklad	příklad	k1gInSc1
využití	využití	k1gNnSc2
se	se	k3xPyFc4
například	například	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
spojení	spojení	k1gNnSc1
pozemní	pozemní	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
a	a	k8xC
jeřábník	jeřábník	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
logistice	logistika	k1gFnSc6
<g/>
,	,	kIx,
autodopravě	autodoprava	k1gFnSc6
či	či	k8xC
obecně	obecně	k6eAd1
jako	jako	k9
náhrada	náhrada	k1gFnSc1
vysílaček	vysílačka	k1gFnPc2
<g/>
,	,	kIx,
tam	tam	k6eAd1
kde	kde	k6eAd1
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
nestačí	stačit	k5eNaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktickou	praktický	k2eAgFnSc7d1
nevýhodou	nevýhoda	k1gFnSc7
je	být	k5eAaImIp3nS
prodleva	prodleva	k1gFnSc1
při	při	k7c6
zahájení	zahájení	k1gNnSc6
komunikace	komunikace	k1gFnSc2
-	-	kIx~
než	než	k8xS
se	se	k3xPyFc4
sestaví	sestavit	k5eAaPmIp3nS
datové	datový	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
a	a	k8xC
také	také	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
datové	datový	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
telefon	telefon	k1gInSc1
a	a	k8xC
hlavně	hlavně	k9
jeho	jeho	k3xOp3gFnSc4
baterii	baterie	k1gFnSc4
značně	značně	k6eAd1
zatěžuje	zatěžovat	k5eAaImIp3nS
a	a	k8xC
hovorový	hovorový	k2eAgInSc1d1
čas	čas	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
v	v	k7c6
řádu	řád	k1gInSc6
jednotek	jednotka	k1gFnPc2
hodin	hodina	k1gFnPc2
aktivního	aktivní	k2eAgNnSc2d1
používání	používání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Služba	služba	k1gFnSc1
O2	O2	k1gFnPc2
Přepínám	přepínat	k5eAaImIp1nS
na	na	k7c6
stránce	stránka	k1gFnSc6
Telefónica	Telefónic	k1gInSc2
O2	O2	k1gFnSc2
Czech	Czecha	k1gFnPc2
Republic	Republice	k1gFnPc2
</s>
<s>
Zpráva	zpráva	k1gFnSc1
o	o	k7c4
spuštění	spuštění	k1gNnSc4
služby	služba	k1gFnSc2
U	u	k7c2
<g/>
:	:	kIx,
<g/>
fonova	fonov	k1gInSc2
vysílačka	vysílačka	k1gFnSc1
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
technologii	technologie	k1gFnSc6
včetně	včetně	k7c2
návodu	návod	k1gInSc2
k	k	k7c3
použití	použití	k1gNnSc3
na	na	k7c6
stránkách	stránka	k1gFnPc6
společnosti	společnost	k1gFnSc2
Nokia	Nokia	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Standardy	standard	k1gInPc1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
0G	0G	k4
</s>
<s>
PTT	PTT	kA
</s>
<s>
MTS	MTS	kA
</s>
<s>
IMTS	IMTS	kA
</s>
<s>
AMTS	AMTS	kA
</s>
<s>
AMR	AMR	kA
</s>
<s>
0.5	0.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
Autotel	Autotel	k1gMnSc1
<g/>
/	/	kIx~
<g/>
PALM	PALM	kA
</s>
<s>
ARP	ARP	kA
1G	1G	k4
</s>
<s>
NMT	NMT	kA
</s>
<s>
AMPS	AMPS	kA
2G	2G	k4
</s>
<s>
GSM	GSM	kA
</s>
<s>
cdmaOne	cdmaOnout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
iDEN	iDEN	k?
</s>
<s>
D-AMPS	D-AMPS	k?
<g/>
/	/	kIx~
<g/>
IS-	IS-	k1gFnSc1
<g/>
136	#num#	k4
<g/>
/	/	kIx~
<g/>
TDMA	TDMA	kA
</s>
<s>
PDC	PDC	kA
</s>
<s>
2.5	2.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
GPRS	GPRS	kA
</s>
<s>
2.75	2.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
CDMA2000	CDMA2000	k1gFnSc1
1	#num#	k4
<g/>
xRTT	xRTT	k?
</s>
<s>
EDGE	EDGE	kA
</s>
<s>
EGPRS	EGPRS	kA
3G	3G	k4
</s>
<s>
W-CDMA	W-CDMA	k?
</s>
<s>
UMTS	UMTS	kA
</s>
<s>
FOMA	FOMA	kA
</s>
<s>
CDMA2000	CDMA2000	k4
1	#num#	k4
<g/>
xEV	xEV	k?
</s>
<s>
TD-SCDMA	TD-SCDMA	k?
</s>
<s>
3.5	3.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSDPA	HSDPA	kA
</s>
<s>
3.75	3.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSUPA	HSUPA	kA
Pre-	Pre-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
G	G	kA
</s>
<s>
Mobile	mobile	k1gNnSc1
WiMAX	WiMAX	k1gFnSc2
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
e	e	k0
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
(	(	kIx(
<g/>
E-UTRA	E-UTRA	k1gMnSc1
<g/>
)	)	kIx)
4G	4G	k4
</s>
<s>
WiMAX-Advanced	WiMAX-Advanced	k1gMnSc1
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
Advanced	Advanced	k1gInSc4
5G	5G	k4
</s>
<s>
eMBB	eMBB	k?
</s>
<s>
URLLC	URLLC	kA
</s>
<s>
MMTC	MMTC	kA
</s>
