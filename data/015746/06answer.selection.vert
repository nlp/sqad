<s>
Push	Push	k1gMnSc1
to	to	k3xDgNnSc4
talk	talk	k1gMnSc1
(	(	kIx(
<g/>
PTT	PTT	kA
<g/>
,	,	kIx,
česky	česky	k6eAd1
„	„	k?
<g/>
stlač	stlačit	k5eAaPmRp2nS
a	a	k8xC
mluv	mluva	k1gFnPc2
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
zmáčkni	zmáčknout	k5eAaPmRp2nS
a	a	k8xC
mluv	mluvit	k5eAaImRp2nS
<g/>
“	“	k?
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
způsob	způsob	k1gInSc4
komunikace	komunikace	k1gFnSc2
po	po	k7c4
half-duplex	half-duplex	k1gInSc4
spojeních	spojení	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
možná	možný	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
vždy	vždy	k6eAd1
pouze	pouze	k6eAd1
jedním	jeden	k4xCgInSc7
směrem	směr	k1gInSc7
a	a	k8xC
chce	chtít	k5eAaImIp3nS
<g/>
-li	-li	k?
operátor	operátor	k1gInSc4
mluvit	mluvit	k5eAaImF
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
stisknout	stisknout	k5eAaPmF
tlačítko	tlačítko	k1gNnSc4
(	(	kIx(
<g/>
a	a	k8xC
po	po	k7c4
tu	ten	k3xDgFnSc4
dobu	doba	k1gFnSc4
neslyší	slyšet	k5eNaImIp3nS
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>