<s>
Kurt	Kurt	k1gMnSc1	Kurt
Donald	Donald	k1gMnSc1	Donald
Cobain	Cobain	k1gMnSc1	Cobain
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1967	[number]	k4	1967
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
grungeové	grungeové	k2eAgFnSc2d1	grungeové
skupiny	skupina	k1gFnSc2	skupina
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
magazín	magazín	k1gInSc1	magazín
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
žebříčku	žebříček	k1gInSc6	žebříček
hudebního	hudební	k2eAgMnSc2d1	hudební
redaktora	redaktor	k1gMnSc2	redaktor
Davida	David	k1gMnSc2	David
Frickeho	Fricke	k1gMnSc2	Fricke
označil	označit	k5eAaPmAgMnS	označit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
za	za	k7c4	za
12	[number]	k4	12
<g/>
.	.	kIx.	.
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kytaristu	kytarista	k1gMnSc4	kytarista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
žebříčku	žebříček	k1gInSc6	žebříček
času	čas	k1gInSc2	čas
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
73	[number]	k4	73
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1967	[number]	k4	1967
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
americkém	americký	k2eAgNnSc6d1	americké
městečku	městečko	k1gNnSc6	městečko
Aberdeen	Aberdena	k1gFnPc2	Aberdena
ležícím	ležící	k2eAgMnSc6d1	ležící
140	[number]	k4	140
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Seattlu	Seattl	k1gInSc2	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Wendy	Wenda	k1gFnPc4	Wenda
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
servírka	servírka	k1gFnSc1	servírka
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Donald	Donald	k1gMnSc1	Donald
byl	být	k5eAaImAgMnS	být
automechanik	automechanik	k1gMnSc1	automechanik
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
bydleli	bydlet	k5eAaImAgMnP	bydlet
v	v	k7c6	v
Hoquiamu	Hoquiam	k1gInSc6	Hoquiam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Kurtovi	Kurt	k1gMnSc3	Kurt
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Aberdeenu	Aberdeen	k1gInSc2	Aberdeen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jeho	jeho	k3xOp3gFnSc1	jeho
skupina	skupina	k1gFnSc1	skupina
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
.	.	kIx.	.
</s>
<s>
Kurtův	Kurtův	k2eAgMnSc1d1	Kurtův
otec	otec	k1gMnSc1	otec
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
našel	najít	k5eAaPmAgMnS	najít
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
drtičce	drtička	k1gFnSc6	drtička
odpadu	odpad	k1gInSc2	odpad
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
byl	být	k5eAaImAgMnS	být
už	už	k6eAd1	už
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
hodně	hodně	k6eAd1	hodně
hubený	hubený	k2eAgMnSc1d1	hubený
kvůli	kvůli	k7c3	kvůli
léku	lék	k1gInSc3	lék
Ritalin	Ritalin	k1gInSc1	Ritalin
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
bral	brát	k5eAaImAgMnS	brát
proti	proti	k7c3	proti
hyperaktivitě	hyperaktivita	k1gFnSc3	hyperaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
také	také	k6eAd1	také
trpěl	trpět	k5eAaImAgMnS	trpět
BAP	BAP	kA	BAP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kurtových	Kurtův	k2eAgNnPc6d1	Kurtovo
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Cobainovi	Cobainův	k2eAgMnPc1d1	Cobainův
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
profesi	profes	k1gFnSc3	profes
trenéra	trenér	k1gMnSc2	trenér
a	a	k8xC	a
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
v	v	k7c6	v
amatérském	amatérský	k2eAgInSc6d1	amatérský
baseballu	baseball	k1gInSc6	baseball
<g/>
.	.	kIx.	.
</s>
<s>
Kurta	kurta	k1gFnSc1	kurta
to	ten	k3xDgNnSc4	ten
velmi	velmi	k6eAd1	velmi
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
<g/>
,	,	kIx,	,
míval	mívat	k5eAaImAgMnS	mívat
deprese	deprese	k1gFnSc2	deprese
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgInSc2	ten
začal	začít	k5eAaPmAgInS	začít
kouřit	kouřit	k5eAaImF	kouřit
marihuanu	marihuana	k1gFnSc4	marihuana
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
nenávidět	nenávidět	k5eAaImF	nenávidět
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
nová	nový	k2eAgNnPc4d1	nové
manželství	manželství	k1gNnPc4	manželství
a	a	k8xC	a
Kurtovo	Kurtův	k2eAgNnSc4d1	Kurtovo
dětství	dětství	k1gNnSc4	dětství
bylo	být	k5eAaImAgNnS	být
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
neustálým	neustálý	k2eAgNnSc7d1	neustálé
stěhováním	stěhování	k1gNnSc7	stěhování
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
k	k	k7c3	k
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
čas	čas	k1gInSc4	čas
bydlel	bydlet	k5eAaImAgMnS	bydlet
dokonce	dokonce	k9	dokonce
u	u	k7c2	u
prarodičů	prarodič	k1gMnPc2	prarodič
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
strýce	strýc	k1gMnSc2	strýc
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
Aerosmith	Aerosmitha	k1gFnPc2	Aerosmitha
či	či	k8xC	či
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
(	(	kIx(	(
<g/>
posledním	poslední	k2eAgMnSc6d1	poslední
dvěma	dva	k4xCgFnPc7	dva
jmenovaným	jmenovaný	k1gMnSc7	jmenovaný
později	pozdě	k6eAd2	pozdě
věnoval	věnovat	k5eAaPmAgMnS	věnovat
píseň	píseň	k1gFnSc4	píseň
Aero	aero	k1gNnSc4	aero
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgMnPc3	svůj
přátelům	přítel	k1gMnPc3	přítel
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
punkem	punk	k1gInSc7	punk
(	(	kIx(	(
<g/>
snad	snad	k9	snad
nejvíc	hodně	k6eAd3	hodně
na	na	k7c6	na
Kurta	Kurt	k1gMnSc2	Kurt
zapůsobily	zapůsobit	k5eAaPmAgFnP	zapůsobit
skupiny	skupina	k1gFnPc1	skupina
Melvins	Melvinsa	k1gFnPc2	Melvinsa
a	a	k8xC	a
Black	Blacka	k1gFnPc2	Blacka
Flag	flaga	k1gFnPc2	flaga
<g/>
)	)	kIx)	)
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
kapela	kapela	k1gFnSc1	kapela
měla	mít	k5eAaImAgFnS	mít
název	název	k1gInSc4	název
Fecal	Fecal	k1gMnSc1	Fecal
Matter	Matter	k1gMnSc1	Matter
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
Melvins	Melvinsa	k1gFnPc2	Melvinsa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
punková	punkový	k2eAgFnSc1d1	punková
kapela	kapela	k1gFnSc1	kapela
neměla	mít	k5eNaImAgFnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
nevydali	vydat	k5eNaPmAgMnP	vydat
jediné	jediný	k2eAgNnSc4d1	jediné
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
neoficiální	neoficiální	k2eAgFnPc1d1	neoficiální
demo	demo	k2eAgFnPc1d1	demo
Illiteracy	Illiteraca	k1gFnPc1	Illiteraca
Will	Will	k1gMnSc1	Will
Prevail	Prevail	k1gMnSc1	Prevail
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
však	však	k9	však
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgMnSc4d1	založen
Nirvana	Nirvan	k1gMnSc4	Nirvan
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
drtivé	drtivý	k2eAgFnSc2d1	drtivá
většiny	většina	k1gFnSc2	většina
skladeb	skladba	k1gFnPc2	skladba
Nirvany	Nirvan	k1gMnPc4	Nirvan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
název	název	k1gInSc1	název
Bleach	Bleacha	k1gFnPc2	Bleacha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
neprosadilo	prosadit	k5eNaPmAgNnS	prosadit
–	–	k?	–
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
se	se	k3xPyFc4	se
až	až	k9	až
po	po	k7c4	po
Nevermind	Nevermind	k1gInSc4	Nevermind
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
průlomové	průlomový	k2eAgNnSc4d1	průlomové
album	album	k1gNnSc4	album
Nevermind	Neverminda	k1gFnPc2	Neverminda
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
hity	hit	k1gInPc4	hit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
In	In	k1gFnSc1	In
Bloom	Bloom	k1gInSc1	Bloom
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Come	Come	k1gNnPc7	Come
As	as	k9	as
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Breed	Breed	k1gInSc1	Breed
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lithium	lithium	k1gNnSc4	lithium
<g/>
"	"	kIx"	"
a	a	k8xC	a
především	především	k9	především
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Like	k1gNnSc1	Like
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirita	k1gFnPc2	Spirita
<g/>
"	"	kIx"	"
–	–	k?	–
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vydala	vydat	k5eAaPmAgFnS	vydat
poslední	poslední	k2eAgNnSc4d1	poslední
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
In	In	k1gMnSc2	In
Utero	Utero	k1gNnSc4	Utero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
hudebně	hudebně	k6eAd1	hudebně
i	i	k8xC	i
textově	textově	k6eAd1	textově
mnohem	mnohem	k6eAd1	mnohem
propracovanější	propracovaný	k2eAgNnSc1d2	propracovanější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neproslavilo	proslavit	k5eNaPmAgNnS	proslavit
se	se	k3xPyFc4	se
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
míře	míra	k1gFnSc6	míra
jako	jako	k9	jako
Nevermind	Nevermind	k1gInSc1	Nevermind
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
ponurosti	ponurost	k1gFnSc3	ponurost
a	a	k8xC	a
agresivitě	agresivita	k1gFnSc3	agresivita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyšly	vyjít	k5eAaPmAgFnP	vyjít
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
hity	hit	k1gInPc1	hit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Heart-shaped	Hearthaped	k1gInSc1	Heart-shaped
box	box	k1gInSc1	box
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Apologies	Apologies	k1gMnSc1	Apologies
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Rape	rape	k1gNnPc1	rape
me	me	k?	me
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Pennyroyal	Pennyroyal	k1gInSc1	Pennyroyal
Tea	Tea	k1gFnSc1	Tea
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1992	[number]	k4	1992
v	v	k7c6	v
25	[number]	k4	25
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Kurt	kurt	k1gInSc1	kurt
Cobain	Cobaina	k1gFnPc2	Cobaina
na	na	k7c4	na
Waikiki	Waikike	k1gFnSc4	Waikike
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Courtney	Courtnea	k1gFnSc2	Courtnea
Love	lov	k1gInSc5	lov
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Hole	hole	k1gFnSc2	hole
<g/>
,	,	kIx,	,
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Frances	Frances	k1gMnSc1	Frances
Bean	Bean	k1gMnSc1	Bean
Cobain	Cobain	k1gMnSc1	Cobain
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	kurt	k1gInSc1	kurt
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
kvůli	kvůli	k7c3	kvůli
těžké	těžký	k2eAgFnSc3d1	těžká
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c4	na
heroinu	heroina	k1gFnSc4	heroina
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Courtney	Courtne	k2eAgFnPc1d1	Courtne
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
Courtney	Courtne	k2eAgMnPc4d1	Courtne
<g/>
,	,	kIx,	,
depresi	deprese	k1gFnSc4	deprese
<g/>
,	,	kIx,	,
drogám	droga	k1gFnPc3	droga
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
trpěl	trpět	k5eAaImAgMnS	trpět
a	a	k8xC	a
jíž	jenž	k3xRgFnSc7	jenž
nikdo	nikdo	k3yNnSc1	nikdo
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
vyléčit	vyléčit	k5eAaPmF	vyléčit
<g/>
,	,	kIx,	,
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
poslední	poslední	k2eAgFnSc4d1	poslední
chuť	chuť	k1gFnSc4	chuť
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
byl	být	k5eAaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
elektrikářem	elektrikář	k1gMnSc7	elektrikář
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
mrtev	mrtev	k2eAgInSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
bez	bez	k7c2	bez
většího	veliký	k2eAgNnSc2d2	veliký
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
smrt	smrt	k1gFnSc4	smrt
vlastním	vlastní	k2eAgNnSc7d1	vlastní
přičiněním	přičinění	k1gNnSc7	přičinění
<g/>
.	.	kIx.	.
</s>
<s>
Seděl	sedět	k5eAaImAgMnS	sedět
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
brokovnicí	brokovnice	k1gFnSc7	brokovnice
prostřelenou	prostřelený	k2eAgFnSc4d1	prostřelená
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
krvi	krev	k1gFnSc6	krev
nalezena	nalezen	k2eAgFnSc1d1	nalezena
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
heroinu	heroin	k1gInSc2	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
zvláštnímu	zvláštní	k2eAgInSc3d1	zvláštní
způsobu	způsob	k1gInSc3	způsob
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
neproveditelnému	proveditelný	k2eNgInSc3d1	neproveditelný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
vykonána	vykonán	k2eAgFnSc1d1	vykonána
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
vynořila	vynořit	k5eAaPmAgFnS	vynořit
řada	řada	k1gFnSc1	řada
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
nespáchal	spáchat	k5eNaPmAgMnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
svojí	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
rozvod	rozvod	k1gInSc4	rozvod
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yRgInSc4	který
Kurt	kurt	k1gInSc4	kurt
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Kurtově	Kurtův	k2eAgFnSc6d1	Kurtova
sebevraždě	sebevražda	k1gFnSc6	sebevražda
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pochybuje	pochybovat	k5eAaImIp3nS	pochybovat
právě	právě	k6eAd1	právě
kvůli	kvůli	k7c3	kvůli
dávce	dávka	k1gFnSc3	dávka
heroinu	heroina	k1gFnSc4	heroina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
takovém	takový	k3xDgNnSc6	takový
množství	množství	k1gNnSc6	množství
heroinu	heroin	k1gInSc2	heroin
údajně	údajně	k6eAd1	údajně
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
uklidit	uklidit	k5eAaPmF	uklidit
drogové	drogový	k2eAgNnSc4d1	drogové
náčiní	náčiní	k1gNnSc4	náčiní
do	do	k7c2	do
krabičky	krabička	k1gFnSc2	krabička
<g/>
,	,	kIx,	,
zvednout	zvednout	k5eAaPmF	zvednout
brokovnici	brokovnice	k1gFnSc4	brokovnice
a	a	k8xC	a
namířit	namířit	k5eAaPmF	namířit
ji	on	k3xPp3gFnSc4	on
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
dopis	dopis	k1gInSc1	dopis
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
pro	pro	k7c4	pro
manželku	manželka	k1gFnSc4	manželka
Courtney	Courtnea	k1gMnSc2	Courtnea
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
Frances	Francesa	k1gFnPc2	Francesa
adresovaný	adresovaný	k2eAgMnSc1d1	adresovaný
imaginárnímu	imaginární	k2eAgMnSc3d1	imaginární
kamarádovi	kamarád	k1gMnSc3	kamarád
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Boddahovi	Boddah	k1gMnSc3	Boddah
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
miluju	milovat	k5eAaImIp1nS	milovat
vás	vy	k3xPp2nPc4	vy
<g/>
,	,	kIx,	,
miluju	milovat	k5eAaImIp1nS	milovat
vás	vy	k3xPp2nPc4	vy
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopisu	dopis	k1gInSc6	dopis
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
uvedl	uvést	k5eAaPmAgMnS	uvést
citát	citát	k1gInSc4	citát
z	z	k7c2	z
písně	píseň	k1gFnSc2	píseň
Neila	Neil	k1gMnSc2	Neil
Younga	Young	k1gMnSc2	Young
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
better	better	k1gInSc1	better
to	ten	k3xDgNnSc4	ten
burn	burn	k1gNnSc4	burn
out	out	k?	out
<g/>
,	,	kIx,	,
than	than	k1gNnSc4	than
to	ten	k3xDgNnSc4	ten
fade	fade	k1gNnSc4	fade
away	awaa	k1gFnSc2	awaa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
shořet	shořet	k5eAaPmF	shořet
<g/>
,	,	kIx,	,
než	než	k8xS	než
vyhasnout	vyhasnout	k5eAaPmF	vyhasnout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c6	o
Kurtově	Kurtův	k2eAgFnSc6d1	Kurtova
sebevraždě	sebevražda	k1gFnSc6	sebevražda
koluje	kolovat	k5eAaImIp3nS	kolovat
nespočetně	spočetně	k6eNd1	spočetně
mnoho	mnoho	k4c4	mnoho
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
předčasnou	předčasný	k2eAgFnSc7d1	předčasná
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Ricochet	Ricochet	k1gInSc1	Ricochet
<g/>
"	"	kIx"	"
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Faith	Faitha	k1gFnPc2	Faitha
No	no	k9	no
More	mor	k1gInSc5	mor
se	se	k3xPyFc4	se
povídá	povídat	k5eAaImIp3nS	povídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaPmNgFnS	napsat
v	v	k7c4	v
den	den	k1gInSc4	den
Kurtovy	Kurtův	k2eAgFnSc2d1	Kurtova
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
často	často	k6eAd1	často
na	na	k7c6	na
setlistech	setlista	k1gMnPc6	setlista
kapely	kapela	k1gFnSc2	kapela
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Nirvana	Nirvan	k1gMnSc4	Nirvan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
Kurtovy	Kurtův	k2eAgFnSc2d1	Kurtova
smrti	smrt	k1gFnSc2	smrt
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
přehrávači	přehrávač	k1gInSc6	přehrávač
album	album	k1gNnSc4	album
Automatic	Automatice	k1gFnPc2	Automatice
for	forum	k1gNnPc2	forum
the	the	k?	the
People	People	k1gMnSc2	People
od	od	k7c2	od
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Michaelem	Michael	k1gMnSc7	Michael
Stipem	Stip	k1gMnSc7	Stip
se	se	k3xPyFc4	se
Kurt	kurt	k1gInSc4	kurt
přátelil	přátelit	k5eAaImAgInS	přátelit
a	a	k8xC	a
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
kmotrem	kmotr	k1gMnSc7	kmotr
Kurtovy	Kurtův	k2eAgFnSc2d1	Kurtova
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
později	pozdě	k6eAd2	pozdě
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c6	na
albu	album	k1gNnSc6	album
Monster	monstrum	k1gNnPc2	monstrum
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
Me	Me	k1gMnSc2	Me
In	In	k1gMnSc2	In
<g/>
"	"	kIx"	"
jako	jako	k9	jako
čest	čest	k1gFnSc4	čest
Kurtově	Kurtův	k2eAgFnSc3d1	Kurtova
památce	památka	k1gFnSc3	památka
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Red	Red	k1gFnSc2	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
(	(	kIx(	(
<g/>
s	s	k7c7	s
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
basistou	basista	k1gMnSc7	basista
Flea	Fle	k1gInSc2	Fle
se	se	k3xPyFc4	se
Kurt	Kurt	k1gMnSc1	Kurt
také	také	k9	také
přátelil	přátelit	k5eAaImAgMnS	přátelit
<g/>
)	)	kIx)	)
vydala	vydat	k5eAaPmAgFnS	vydat
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
na	na	k7c6	na
albu	album	k1gNnSc6	album
One	One	k1gFnSc2	One
Hot	hot	k0	hot
Minute	Minut	k1gInSc5	Minut
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Tearjerker	Tearjerker	k1gInSc1	Tearjerker
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
Kurtově	Kurtův	k2eAgInSc6d1	Kurtův
životě	život	k1gInSc6	život
a	a	k8xC	a
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Foo	Foo	k1gFnSc2	Foo
Fighters	Fightersa	k1gFnPc2	Fightersa
se	se	k3xPyFc4	se
mnohokrát	mnohokrát	k6eAd1	mnohokrát
nechala	nechat	k5eAaPmAgFnS	nechat
Nirvanou	Nirvaná	k1gFnSc7	Nirvaná
a	a	k8xC	a
Kurtem	kurt	k1gInSc7	kurt
takovým	takový	k3xDgInSc7	takový
inspirovat	inspirovat	k5eAaBmF	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Hero	Hera	k1gMnSc5	Hera
<g/>
"	"	kIx"	"
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
frontmana	frontman	k1gMnSc2	frontman
Dave	Dav	k1gInSc5	Dav
Grohla	Grohla	k1gMnSc7	Grohla
ke	k	k7c3	k
Kurtu	Kurt	k1gMnSc3	Kurt
Cobainovi	Cobain	k1gMnSc3	Cobain
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
autorů	autor	k1gMnPc2	autor
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potom	potom	k6eAd1	potom
například	například	k6eAd1	například
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc1	klus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
píseň	píseň	k1gFnSc4	píseň
Čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Těžší	těžký	k2eAgNnSc1d2	těžší
než	než	k8xS	než
nebe	nebe	k1gNnSc1	nebe
<g/>
:	:	kIx,	:
Životopis	životopis	k1gInSc1	životopis
Kurta	Kurt	k1gMnSc2	Kurt
Cobaina	Cobain	k1gMnSc2	Cobain
Nirvana	Nirvan	k1gMnSc2	Nirvan
-	-	kIx~	-
Pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
příběh	příběh	k1gInSc1	příběh
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
:	:	kIx,	:
Come	Com	k1gMnSc2	Com
As	as	k9	as
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gInSc1	Cobain
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
Nirvana	Nirvan	k1gMnSc2	Nirvan
Years	Yearsa	k1gFnPc2	Yearsa
Nirvana	Nirvan	k1gMnSc2	Nirvan
Začátky	začátek	k1gInPc1	začátek
a	a	k8xC	a
vzestup	vzestup	k1gInSc1	vzestup
Nirvana	Nirvan	k1gMnSc2	Nirvan
Historie	historie	k1gFnSc1	historie
nahrávek	nahrávka	k1gFnPc2	nahrávka
Nirvana	Nirvan	k1gMnSc2	Nirvan
-	-	kIx~	-
Kompletní	kompletní	k2eAgFnSc1d1	kompletní
ilustrovaná	ilustrovaný	k2eAgFnSc1d1	ilustrovaná
monografie	monografie	k1gFnSc1	monografie
Last	Last	k1gInSc4	Last
Days	Days	k1gInSc1	Days
Kurt	kurt	k1gInSc1	kurt
&	&	k?	&
Courtney	Courtnea	k1gFnSc2	Courtnea
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
<g/>
:	:	kIx,	:
About	About	k1gInSc1	About
a	a	k8xC	a
Son	son	k1gInSc1	son
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
<g/>
:	:	kIx,	:
Montage	Montage	k1gInSc1	Montage
of	of	k?	of
Heck	Heck	k1gInSc1	Heck
Soaked	Soaked	k1gMnSc1	Soaked
in	in	k?	in
Bleach	Bleach	k1gMnSc1	Bleach
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k2eAgMnSc1d1	Cobain
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Kurt	kurt	k1gInSc1	kurt
Cobain	Cobain	k1gInSc1	Cobain
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
