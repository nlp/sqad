<s>
Ivan	Ivan	k1gMnSc1	Ivan
Jevstafjevič	Jevstafjevič	k1gMnSc1	Jevstafjevič
Vlasov	Vlasov	k1gInSc1	Vlasov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
Е	Е	k?	Е
В	В	k?	В
<g/>
;	;	kIx,	;
1628	[number]	k4	1628
<g/>
–	–	k?	–
<g/>
1710	[number]	k4	1710
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
šlechtic	šlechtic	k1gMnSc1	šlechtic
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
vojevoda	vojevoda	k1gMnSc1	vojevoda
v	v	k7c6	v
Irkutsku	Irkutsk	k1gInSc6	Irkutsk
a	a	k8xC	a
Něrčinsku	Něrčinsko	k1gNnSc6	Něrčinsko
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
jednání	jednání	k1gNnSc2	jednání
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
Něrčinské	Něrčinský	k2eAgFnSc2d1	Něrčinský
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
