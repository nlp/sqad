<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
ruský	ruský	k2eAgMnSc1d1	ruský
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
šlechtic	šlechtic	k1gMnSc1	šlechtic
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
vojevoda	vojevoda	k1gMnSc1	vojevoda
v	v	k7c6	v
Irkutsku	Irkutsk	k1gInSc6	Irkutsk
a	a	k8xC	a
Něrčinsku	Něrčinsko	k1gNnSc6	Něrčinsko
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
jednání	jednání	k1gNnSc2	jednání
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
Něrčinské	Něrčinský	k2eAgFnSc2d1	Něrčinský
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
?	?	kIx.	?
</s>
