<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Jevstafjevič	Jevstafjevič	k1gMnSc1	Jevstafjevič
Vlasov	Vlasov	k1gInSc1	Vlasov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
Е	Е	k?	Е
В	В	k?	В
<g/>
;	;	kIx,	;
1628	[number]	k4	1628
<g/>
–	–	k?	–
<g/>
1710	[number]	k4	1710
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
šlechtic	šlechtic	k1gMnSc1	šlechtic
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
vojevoda	vojevoda	k1gMnSc1	vojevoda
v	v	k7c6	v
Irkutsku	Irkutsk	k1gInSc6	Irkutsk
a	a	k8xC	a
Něrčinsku	Něrčinsko	k1gNnSc6	Něrčinsko
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
jednání	jednání	k1gNnSc2	jednání
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
Něrčinské	Něrčinský	k2eAgFnSc2d1	Něrčinský
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Vlasov	Vlasov	k1gInSc4	Vlasov
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
Jevstafiji	Jevstafiji	k1gFnSc2	Jevstafiji
Ivanoviči	Ivanovič	k1gMnSc3	Ivanovič
Vlasovovi	Vlasova	k1gMnSc3	Vlasova
<g/>
,	,	kIx,	,
řeckému	řecký	k2eAgMnSc3d1	řecký
přistěhovalci	přistěhovalec	k1gMnSc3	přistěhovalec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
z	z	k7c2	z
Istanbulu	Istanbul	k1gInSc2	Istanbul
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1647	[number]	k4	1647
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
mezi	mezi	k7c4	mezi
šlechtice	šlechtic	k1gMnPc4	šlechtic
moskevského	moskevský	k2eAgInSc2d1	moskevský
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Vlasov	Vlasov	k1gInSc4	Vlasov
dostal	dostat	k5eAaPmAgMnS	dostat
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
vzdělání	vzdělání	k1gNnSc4	vzdělání
včetně	včetně	k7c2	včetně
znalosti	znalost	k1gFnSc2	znalost
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1656	[number]	k4	1656
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
poselství	poselství	k1gNnSc4	poselství
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1671	[number]	k4	1671
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
Dělostřeleckém	dělostřelecký	k2eAgInSc6d1	dělostřelecký
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Maloruském	maloruský	k2eAgInSc6d1	maloruský
prikaze	prikaz	k1gInSc6	prikaz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1674	[number]	k4	1674
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
vojevoda	vojevoda	k1gMnSc1	vojevoda
v	v	k7c6	v
Arzamasu	Arzamas	k1gInSc6	Arzamas
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1677	[number]	k4	1677
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
Putivli	Putivli	k1gFnSc2	Putivli
k	k	k7c3	k
pluku	pluk	k1gInSc3	pluk
knížete	kníže	k1gNnSc2wR	kníže
Golovina	Golovina	k1gFnSc1	Golovina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1677	[number]	k4	1677
obdržel	obdržet	k5eAaPmAgMnS	obdržet
titul	titul	k1gInSc4	titul
stolníka	stolník	k1gMnSc2	stolník
(	(	kIx(	(
<g/>
с	с	k?	с
<g/>
)	)	kIx)	)
a	a	k8xC	a
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1680	[number]	k4	1680
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
k	k	k7c3	k
Sibiřskému	sibiřský	k2eAgInSc3d1	sibiřský
prikazu	prikaz	k1gInSc3	prikaz
a	a	k8xC	a
jmenován	jmenován	k2eAgInSc4d1	jmenován
(	(	kIx(	(
<g/>
prvním	první	k4xOgMnSc6	první
<g/>
)	)	kIx)	)
vojevodou	vojevoda	k1gMnSc7	vojevoda
v	v	k7c6	v
Irkutsku	Irkutsk	k1gInSc6	Irkutsk
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1681	[number]	k4	1681
<g/>
.	.	kIx.	.
</s>
<s>
Podléhaly	podléhat	k5eAaImAgFnP	podléhat
mu	on	k3xPp3gMnSc3	on
i	i	k9	i
ostrohy	ostroha	k1gFnPc1	ostroha
v	v	k7c6	v
Barguzinsku	Barguzinsko	k1gNnSc6	Barguzinsko
<g/>
,	,	kIx,	,
Selenginsku	Selenginsko	k1gNnSc6	Selenginsko
<g/>
,	,	kIx,	,
Bauntovsku	Bauntovsko	k1gNnSc6	Bauntovsko
a	a	k8xC	a
Udinsku	Udinsko	k1gNnSc6	Udinsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svěřeném	svěřený	k2eAgInSc6d1	svěřený
regionu	region	k1gInSc6	region
podporoval	podporovat	k5eAaImAgInS	podporovat
hledání	hledání	k1gNnSc4	hledání
a	a	k8xC	a
těžbu	těžba	k1gFnSc4	těžba
rud	ruda	k1gFnPc2	ruda
(	(	kIx(	(
<g/>
a	a	k8xC	a
slídy	slída	k1gFnSc2	slída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
výrobu	výroba	k1gFnSc4	výroba
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
v	v	k7c6	v
šíření	šíření	k1gNnSc6	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
mezi	mezi	k7c7	mezi
místními	místní	k2eAgMnPc7d1	místní
Evenky	Evenek	k1gMnPc7	Evenek
a	a	k8xC	a
Burjaty	Burjat	k1gMnPc7	Burjat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1684	[number]	k4	1684
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
něrčinským	něrčinský	k2eAgMnSc7d1	něrčinský
vojevodou	vojevoda	k1gMnSc7	vojevoda
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
zkušební	zkušební	k2eAgFnSc4d1	zkušební
těžbu	těžba	k1gFnSc4	těžba
stříbra	stříbro	k1gNnSc2	stříbro
z	z	k7c2	z
naleziště	naleziště	k1gNnSc2	naleziště
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Argun	Arguna	k1gFnPc2	Arguna
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1704	[number]	k4	1704
se	se	k3xPyFc4	se
rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
těžba	těžba	k1gFnSc1	těžba
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
rozvoj	rozvoj	k1gInSc1	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Šilky	Šilka	k1gFnSc2	Šilka
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
funkčním	funkční	k2eAgNnSc6d1	funkční
období	období	k1gNnSc6	období
začala	začít	k5eAaPmAgFnS	začít
težba	težba	k1gFnSc1	težba
soli	sůl	k1gFnSc2	sůl
z	z	k7c2	z
Borzinského	Borzinský	k2eAgNnSc2d1	Borzinský
solného	solný	k2eAgNnSc2d1	solné
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
dobývání	dobývání	k1gNnSc2	dobývání
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
u	u	k7c2	u
Telembinsku	Telembinsko	k1gNnSc6	Telembinsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1684	[number]	k4	1684
byl	být	k5eAaImAgInS	být
též	též	k9	též
v	v	k7c6	v
Něrčinsku	Něrčinsko	k1gNnSc6	Něrčinsko
pokřtěn	pokřtěn	k2eAgMnSc1d1	pokřtěn
evencký	evencký	k1gMnSc1	evencký
kníže	kníže	k1gMnSc1	kníže
Gantimur	Gantimur	k1gMnSc1	Gantimur
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1685	[number]	k4	1685
organizoval	organizovat	k5eAaBmAgMnS	organizovat
obnovu	obnova	k1gFnSc4	obnova
Albazinu	Albazin	k1gInSc2	Albazin
po	po	k7c4	po
jeho	jeho	k3xOp3gNnSc4	jeho
zničení	zničení	k1gNnSc4	zničení
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
obléhání	obléhání	k1gNnSc6	obléhání
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
Fjodoru	Fjodor	k1gInSc2	Fjodor
Golovinovi	Golovin	k1gMnSc6	Golovin
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
říše	říš	k1gFnSc2	říš
Čching	Čching	k1gInSc1	Čching
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
setkáním	setkání	k1gNnSc7	setkání
v	v	k7c6	v
Něrčinsku	Něrčinsko	k1gNnSc6	Něrčinsko
a	a	k8xC	a
uzavřením	uzavření	k1gNnSc7	uzavření
Něrčinské	Něrčinský	k2eAgFnSc2d1	Něrčinský
smlouvy	smlouva	k1gFnSc2	smlouva
roku	rok	k1gInSc2	rok
1689	[number]	k4	1689
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
podepsání	podepsání	k1gNnSc4	podepsání
Něrčinské	Něrčinský	k2eAgFnSc2d1	Něrčinský
smlouvy	smlouva	k1gFnSc2	smlouva
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1691	[number]	k4	1691
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
Něrčinsku	Něrčinsko	k1gNnSc6	Něrčinsko
nahradil	nahradit	k5eAaPmAgMnS	nahradit
plukovník	plukovník	k1gMnSc1	plukovník
F.	F.	kA	F.
I.	I.	kA	I.
Skripicyn	Skripicyn	k1gInSc1	Skripicyn
a	a	k8xC	a
Vlasov	Vlasov	k1gInSc1	Vlasov
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgInS	jmenovat
členem	člen	k1gInSc7	člen
Dumy	duma	k1gFnSc2	duma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1695	[number]	k4	1695
a	a	k8xC	a
1696	[number]	k4	1696
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
azovských	azovský	k2eAgInPc2d1	azovský
pochodů	pochod	k1gInPc2	pochod
<g/>
,	,	kIx,	,
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
za	za	k7c4	za
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
za	za	k7c4	za
výrobu	výroba	k1gFnSc4	výroba
potaše	potaš	k1gInSc2	potaš
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
výslužby	výslužba	k1gFnSc2	výslužba
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1710	[number]	k4	1710
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
В	В	k?	В
И	И	k?	И
Е	Е	k?	Е
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
ostrog	ostrog	k1gMnSc1	ostrog
<g/>
.	.	kIx.	.
<g/>
ucoz	ucoz	k1gMnSc1	ucoz
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
<g/>
,	,	kIx,	,
2013-01-20	[number]	k4	2013-01-20
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Včetně	včetně	k7c2	včetně
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
zprávy	zpráva	k1gFnPc4	zpráva
sibiřských	sibiřský	k2eAgMnPc2d1	sibiřský
úředníků	úředník	k1gMnPc2	úředník
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
Vlasova	Vlasův	k2eAgFnSc1d1	Vlasova
<g/>
,	,	kIx,	,
Vlasovovy	Vlasovův	k2eAgFnPc1d1	Vlasovova
zprávy	zpráva	k1gFnPc1	zpráva
a	a	k8xC	a
články	článek	k1gInPc1	článek
A.	A.	kA	A.
R.	R.	kA	R.
Artěmjeva	Artěmjev	k1gMnSc2	Artěmjev
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
</s>
</p>
