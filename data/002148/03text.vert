<s>
Atlanta	Atlanta	k1gFnSc1	Atlanta
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Georgie	Georgie	k1gFnSc2	Georgie
a	a	k8xC	a
centrální	centrální	k2eAgNnSc4d1	centrální
město	město	k1gNnSc4	město
deváté	devátý	k4xOgFnSc2	devátý
největší	veliký	k2eAgFnSc2d3	veliký
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Fulton	Fulton	k1gInSc1	Fulton
County	Counta	k1gFnPc1	Counta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
DeKalb	DeKalba	k1gFnPc2	DeKalba
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
samotném	samotný	k2eAgNnSc6d1	samotné
448	[number]	k4	448
tisíc	tisíc	k4xCgInSc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
40	[number]	k4	40
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5,52	[number]	k4	5,52
milióny	milión	k4xCgInPc1	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
<g/>
.	.	kIx.	.
</s>
<s>
Atlanta	Atlanta	k1gFnSc1	Atlanta
je	být	k5eAaImIp3nS	být
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
centrem	centrum	k1gNnSc7	centrum
amerického	americký	k2eAgInSc2d1	americký
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
<g/>
,	,	kIx,	,
po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
začalo	začít	k5eAaPmAgNnS	začít
město	město	k1gNnSc1	město
vzkvétat	vzkvétat	k5eAaImF	vzkvétat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
novými	nový	k2eAgFnPc7d1	nová
pracovními	pracovní	k2eAgFnPc7d1	pracovní
příležitostmi	příležitost	k1gFnPc7	příležitost
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
v	v	k7c6	v
textilním	textilní	k2eAgInSc6d1	textilní
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
sem	sem	k6eAd1	sem
přišlo	přijít	k5eAaPmAgNnS	přijít
spousta	spousta	k6eAd1	spousta
afroamerických	afroamerický	k2eAgMnPc2d1	afroamerický
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
etnickou	etnický	k2eAgFnSc4d1	etnická
skupinu	skupina	k1gFnSc4	skupina
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
54,0	[number]	k4	54,0
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
přítomnosti	přítomnost	k1gFnSc3	přítomnost
černochů	černoch	k1gMnPc2	černoch
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
kulturu	kultura	k1gFnSc4	kultura
se	se	k3xPyFc4	se
Atlantě	Atlanta	k1gFnSc3	Atlanta
někdy	někdy	k6eAd1	někdy
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
černošská	černošský	k2eAgFnSc1d1	černošská
Mekka	Mekka	k1gFnSc1	Mekka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Black	Black	k1gMnSc1	Black
Mecca	Mecca	k1gMnSc1	Mecca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
je	být	k5eAaImIp3nS	být
ústředí	ústředí	k1gNnSc2	ústředí
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgFnPc2d1	významná
společností	společnost	k1gFnPc2	společnost
jako	jako	k8xC	jako
Coca-Cola	cocaola	k1gFnSc1	coca-cola
nebo	nebo	k8xC	nebo
CNN	CNN	kA	CNN
a	a	k8xC	a
Hartsfield-Jackson	Hartsfield-Jackson	k1gMnSc1	Hartsfield-Jackson
International	International	k1gFnSc2	International
Airport	Airport	k1gInSc1	Airport
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
letiště	letiště	k1gNnSc4	letiště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
420	[number]	k4	420
003	[number]	k4	003
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
38,4	[number]	k4	38,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
54,0	[number]	k4	54,0
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,1	[number]	k4	3,1
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,0	[number]	k4	2,0
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
5,2	[number]	k4	5,2
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Atlanta	Atlanta	k1gFnSc1	Atlanta
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
podle	podle	k7c2	podle
různých	různý	k2eAgFnPc2d1	různá
definic	definice	k1gFnPc2	definice
4,5	[number]	k4	4,5
-	-	kIx~	-
6,2	[number]	k4	6,2
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
měla	mít	k5eAaImAgFnS	mít
Atlanta	Atlanta	k1gFnSc1	Atlanta
MSA	MSA	kA	MSA
(	(	kIx(	(
<g/>
Metropolitan	metropolitan	k1gInSc1	metropolitan
Statistical	Statistical	k1gMnSc2	Statistical
Area	Ares	k1gMnSc2	Ares
<g/>
)	)	kIx)	)
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
268	[number]	k4	268
860	[number]	k4	860
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
55,4	[number]	k4	55,4
<g/>
%	%	kIx~	%
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
32,4	[number]	k4	32,4
<g/>
%	%	kIx~	%
černochů	černoch	k1gMnPc2	černoch
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
byli	být	k5eAaImAgMnP	být
Hispánci	Hispánek	k1gMnPc1	Hispánek
a	a	k8xC	a
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Atlanty	Atlanta	k1gFnSc2	Atlanta
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
množství	množství	k1gNnSc3	množství
černochů	černoch	k1gMnPc2	černoch
výrazně	výrazně	k6eAd1	výrazně
prostorově	prostorově	k6eAd1	prostorově
segregována	segregován	k2eAgFnSc1d1	segregována
a	a	k8xC	a
suburbanizována	suburbanizován	k2eAgFnSc1d1	suburbanizován
<g/>
.	.	kIx.	.
</s>
<s>
Běloši	běloch	k1gMnPc1	běloch
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
severních	severní	k2eAgNnPc6d1	severní
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
,	,	kIx,	,
černoši	černoch	k1gMnPc1	černoch
zase	zase	k9	zase
na	na	k7c6	na
jižních	jižní	k2eAgFnPc6d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
veškerá	veškerý	k3xTgFnSc1	veškerý
doprava	doprava	k1gFnSc1	doprava
probíhá	probíhat	k5eAaImIp3nS	probíhat
osobními	osobní	k2eAgInPc7d1	osobní
automobily	automobil	k1gInPc7	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Atlanta	Atlanta	k1gFnSc1	Atlanta
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
známou	známý	k2eAgFnSc7d1	známá
zejména	zejména	k9	zejména
hip-hopem	hipop	k1gInSc7	hip-hop
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
a	a	k8xC	a
crunkem	crunek	k1gMnSc7	crunek
(	(	kIx(	(
<g/>
proslaveným	proslavený	k2eAgMnSc7d1	proslavený
zejména	zejména	k9	zejména
rapperem	rapper	k1gInSc7	rapper
Lil	lít	k5eAaImAgMnS	lít
Jonem	Jonem	k1gInSc4	Jonem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
sídlem	sídlo	k1gNnSc7	sídlo
<g/>
"	"	kIx"	"
jedné	jeden	k4xCgFnSc2	jeden
odnože	odnož	k1gFnSc2	odnož
amerického	americký	k2eAgInSc2d1	americký
hip-hopu	hipop	k1gInSc2	hip-hop
-	-	kIx~	-
Dirty	Dirta	k1gFnPc1	Dirta
South	South	k1gInSc4	South
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
atlantské	atlantský	k2eAgMnPc4d1	atlantský
hip-hopové	hipopový	k2eAgMnPc4d1	hip-hopový
hudebníky	hudebník	k1gMnPc4	hudebník
patří	patřit	k5eAaImIp3nS	patřit
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
Lil	lít	k5eAaImAgMnS	lít
Jon	Jon	k1gMnSc3	Jon
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
2	[number]	k4	2
Chainz	Chainza	k1gFnPc2	Chainza
<g/>
,	,	kIx,	,
Bow	Bow	k1gMnSc1	Bow
Wow	Wow	k1gMnSc1	Wow
<g/>
,	,	kIx,	,
OutKast	OutKast	k1gMnSc1	OutKast
<g/>
,	,	kIx,	,
Usher	Usher	k1gMnSc1	Usher
<g/>
,	,	kIx,	,
Ludacris	Ludacris	k1gFnSc1	Ludacris
a	a	k8xC	a
T.I.	T.I.	k1gFnSc1	T.I.
Basketbal	basketbal	k1gInSc1	basketbal
(	(	kIx(	(
<g/>
NBA	NBA	kA	NBA
<g/>
)	)	kIx)	)
-	-	kIx~	-
Atlanta	Atlanta	k1gFnSc1	Atlanta
Hawks	Hawks	k1gInSc1	Hawks
Baseball	baseball	k1gInSc1	baseball
(	(	kIx(	(
<g/>
MLB	MLB	kA	MLB
<g/>
)	)	kIx)	)
-	-	kIx~	-
Atlanta	Atlanta	k1gFnSc1	Atlanta
Braves	Braves	k1gInSc4	Braves
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
(	(	kIx(	(
<g/>
NFL	NFL	kA	NFL
<g/>
)	)	kIx)	)
-	-	kIx~	-
Atlanta	Atlanta	k1gFnSc1	Atlanta
Falcons	Falconsa	k1gFnPc2	Falconsa
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgFnP	konat
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgNnSc4d1	ústřední
sídlo	sídlo	k1gNnSc4	sídlo
společnosti	společnost	k1gFnSc2	společnost
The	The	k1gFnSc2	The
Coca-Cola	cocaola	k1gFnSc1	coca-cola
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
výrobce	výrobce	k1gMnSc1	výrobce
nealkoholických	alkoholický	k2eNgMnPc2d1	nealkoholický
nápojů	nápoj	k1gInPc2	nápoj
a	a	k8xC	a
také	také	k9	také
muzeum	muzeum	k1gNnSc1	muzeum
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Margaret	Margareta	k1gFnPc2	Margareta
Mitchell	Mitchella	k1gFnPc2	Mitchella
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
-	-	kIx~	-
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
DeForest	DeForest	k1gFnSc1	DeForest
Kelley	Kellea	k1gFnSc2	Kellea
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
-	-	kIx~	-
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
-	-	kIx~	-
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baptistický	baptistický	k2eAgMnSc1d1	baptistický
kazatel	kazatel	k1gMnSc1	kazatel
<g/>
,	,	kIx,	,
bojovník	bojovník	k1gMnSc1	bojovník
za	za	k7c4	za
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
Mildred	Mildred	k1gInSc1	Mildred
McDanielová	McDanielová	k1gFnSc1	McDanielová
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skokanka	skokanka	k1gFnSc1	skokanka
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
Larry	Larra	k1gFnSc2	Larra
McDonald	McDonald	k1gMnSc1	McDonald
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
-	-	kIx~	-
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Jerry	Jerra	k1gFnSc2	Jerra
Reed	Reed	k1gMnSc1	Reed
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
country	country	k2eAgMnSc1d1	country
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
Roy	Roy	k1gMnSc1	Roy
Dunbard	Dunbard	k1gMnSc1	Dunbard
Bridges	Bridges	k1gMnSc1	Bridges
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronaut	astronaut	k1gMnSc1	astronaut
Cynthia	Cynthium	k1gNnSc2	Cynthium
McKinney	McKinnea	k1gFnSc2	McKinnea
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
Spike	Spike	k1gInSc1	Spike
Lee	Lea	k1gFnSc3	Lea
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
Will	Will	k1gMnSc1	Will
Wright	Wright	k1gMnSc1	Wright
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herní	herní	k2eAgMnSc1d1	herní
vývojář	vývojář	k1gMnSc1	vývojář
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
vývojářského	vývojářský	k2eAgNnSc2d1	vývojářské
studia	studio	k1gNnSc2	studio
Maxis	Maxis	k1gFnSc2	Maxis
Steven	Steven	k2eAgInSc4d1	Steven
Soderbergh	Soderbergh	k1gInSc4	Soderbergh
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Julia	Julius	k1gMnSc2	Julius
Robertsová	Robertsová	k1gFnSc1	Robertsová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Lil	lít	k5eAaImAgMnS	lít
Jon	Jon	k1gMnSc1	Jon
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rapper	rapper	k1gMnSc1	rapper
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
Adam	Adam	k1gMnSc1	Adam
Nelson	Nelson	k1gMnSc1	Nelson
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
medailista	medailista	k1gMnSc1	medailista
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
vrhu	vrh	k1gInSc6	vrh
koulí	koulet	k5eAaImIp3nS	koulet
Milton	Milton	k1gInSc1	Milton
Campbell	Campbella	k1gFnPc2	Campbella
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
-	-	kIx~	-
běžec	běžec	k1gMnSc1	běžec
Kip	Kip	k1gMnSc2	Kip
Pardue	Pardue	k1gNnSc2	Pardue
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Brittany	Brittan	k1gInPc4	Brittan
Murphyová	Murphyová	k1gFnSc1	Murphyová
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Kanye	Kany	k1gFnSc2	Kany
West	Westa	k1gFnPc2	Westa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
Terrence	Terrence	k1gFnSc2	Terrence
Trammell	Trammell	k1gMnSc1	Trammell
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
-	-	kIx~	-
překážkar	překážkar	k1gMnSc1	překážkar
Kelly	Kella	k1gFnSc2	Kella
Rowland	Rowland	k1gInSc1	Rowland
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
skladatelka	skladatelka	k1gFnSc1	skladatelka
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
Destiny	Destina	k1gFnPc1	Destina
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Child	Childa	k1gFnPc2	Childa
<g/>
)	)	kIx)	)
Dwight	Dwight	k2eAgInSc1d1	Dwight
Howard	Howard	k1gInSc1	Howard
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
basketbalista	basketbalista	k1gMnSc1	basketbalista
Jeezy	Jeeza	k1gFnSc2	Jeeza
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rapper	rapper	k1gInSc4	rapper
Edina	Edien	k2eAgFnSc1d1	Edina
Gallovitsová-Hallová	Gallovitsová-Hallová	k1gFnSc1	Gallovitsová-Hallová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rumunská	rumunský	k2eAgFnSc1d1	rumunská
tenistka	tenistka	k1gFnSc1	tenistka
Odette	Odett	k1gInSc5	Odett
Annable	Annable	k1gMnPc6	Annable
(	(	kIx(	(
<g/>
*	*	kIx~	*
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
modelka	modelka	k1gFnSc1	modelka
Nina	Nina	k1gFnSc1	Nina
Dobrev	Dobrev	k1gFnSc1	Dobrev
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bulharsko-kanadská	bulharskoanadský	k2eAgFnSc1d1	bulharsko-kanadský
herečka	herečka	k1gFnSc1	herečka
Justin	Justin	k1gMnSc1	Justin
Bieber	Bieber	k1gMnSc1	Bieber
(	(	kIx(	(
<g/>
*	*	kIx~	*
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgInSc1d1	kanadský
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
rapper	rapper	k1gMnSc1	rapper
</s>
