<s>
Důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
výskyt	výskyt	k1gInSc1	výskyt
koniklece	koniklec	k1gInSc2	koniklec
velkokvětého	velkokvětý	k2eAgInSc2d1	velkokvětý
(	(	kIx(	(
<g/>
Pulsatilla	Pulsatilla	k1gFnSc1	Pulsatilla
grandis	grandis	k1gFnSc2	grandis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
největší	veliký	k2eAgFnSc4d3	veliký
populaci	populace	k1gFnSc4	populace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgMnSc1d1	čítající
55	[number]	k4	55
tisíc	tisíc	k4xCgInPc2	tisíc
trsů	trs	k1gInPc2	trs
těchto	tento	k3xDgFnPc2	tento
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
