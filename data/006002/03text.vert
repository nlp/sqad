<s>
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
み	み	k?	み
<g/>
,	,	kIx,	,
Micubači	Micubač	k1gInSc3	Micubač
Mája	Mája	k1gFnSc1	Mája
no	no	k9	no
bóken	bóken	k1gInSc1	bóken
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dětský	dětský	k2eAgInSc1d1	dětský
koprodukční	koprodukční	k2eAgInSc1d1	koprodukční
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
volně	volně	k6eAd1	volně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
knihu	kniha	k1gFnSc4	kniha
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
příhody	příhoda	k1gFnSc2	příhoda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
německý	německý	k2eAgMnSc1d1	německý
autor	autor	k1gMnSc1	autor
Waldemar	Waldemar	k1gMnSc1	Waldemar
Bonsels	Bonsels	k1gInSc4	Bonsels
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
nadabován	nadabovat	k5eAaPmNgInS	nadabovat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
vysílán	vysílán	k2eAgInSc1d1	vysílán
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
,	,	kIx,	,
Finska	Finsko	k1gNnSc2	Finsko
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc2	Libanon
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Zena	Zena	k1gFnSc1	Zena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Níkú	Níkú	k1gFnSc1	Níkú
<g/>
"	"	kIx"	"
ن	ن	k?	ن
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
55	[number]	k4	55
<g/>
dílná	dílný	k2eAgFnSc1d1	dílná
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
série	série	k1gFnSc2	série
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
v	v	k7c6	v
koprodukci	koprodukce	k1gFnSc6	koprodukce
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Produkovala	produkovat	k5eAaImAgFnS	produkovat
ji	on	k3xPp3gFnSc4	on
společnost	společnost	k1gFnSc1	společnost
Nippon	Nippon	k1gMnSc1	Nippon
Animation	Animation	k1gInSc1	Animation
<g/>
.	.	kIx.	.
</s>
<s>
Premiérově	premiérově	k6eAd1	premiérově
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
na	na	k7c6	na
japonské	japonský	k2eAgFnSc6d1	japonská
televizní	televizní	k2eAgFnSc6d1	televizní
stanici	stanice	k1gFnSc6	stanice
Asahi	Asah	k1gFnSc2	Asah
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
uvedení	uvedení	k1gNnSc1	uvedení
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
první	první	k4xOgFnSc2	první
série	série	k1gFnSc2	série
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
natočeno	natočit	k5eAaBmNgNnS	natočit
52	[number]	k4	52
<g/>
dílné	dílný	k2eAgNnSc4d1	dílné
pokračování	pokračování	k1gNnSc4	pokračování
Nová	nový	k2eAgFnSc1d1	nová
včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
新	新	k?	新
<g/>
,	,	kIx,	,
Šin	Šin	k1gMnSc6	Šin
micubači	micubač	k1gMnSc6	micubač
Mája	Mája	k1gFnSc1	Mája
no	no	k9	no
bóken	bóken	k2eAgMnSc1d1	bóken
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
produkováno	produkován	k2eAgNnSc1d1	produkováno
také	také	k9	také
společností	společnost	k1gFnPc2	společnost
Nippon	Nippon	k1gMnSc1	Nippon
Animation	Animation	k1gInSc1	Animation
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
série	série	k1gFnPc1	série
(	(	kIx(	(
<g/>
dohromady	dohromady	k6eAd1	dohromady
107	[number]	k4	107
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dílů	díl	k1gInPc2	díl
<g/>
)	)	kIx)	)
vysílají	vysílat	k5eAaImIp3nP	vysílat
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
jazykových	jazykový	k2eAgFnPc6d1	jazyková
mutacích	mutace	k1gFnPc6	mutace
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
počtem	počet	k1gInSc7	počet
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
má	mít	k5eAaImIp3nS	mít
104	[number]	k4	104
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
první	první	k4xOgFnSc7	první
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navazují	navazovat	k5eAaImIp3nP	navazovat
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
naprosto	naprosto	k6eAd1	naprosto
minimálně	minimálně	k6eAd1	minimálně
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
přibytím	přibytí	k1gNnSc7	přibytí
myšáka	myšák	k1gMnSc2	myšák
Alexandra	Alexandr	k1gMnSc2	Alexandr
nebo	nebo	k8xC	nebo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
postavy	postava	k1gFnPc4	postava
trochu	trochu	k6eAd1	trochu
jinak	jinak	k6eAd1	jinak
nakreslené	nakreslený	k2eAgInPc1d1	nakreslený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vyšly	vyjít	k5eAaPmAgInP	vyjít
nové	nový	k2eAgInPc1d1	nový
díly	díl	k1gInPc1	díl
Včelky	včelka	k1gFnSc2	včelka
Máji	Mája	k1gFnSc2	Mája
v	v	k7c6	v
3	[number]	k4	3
<g/>
D.	D.	kA	D.
V	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
dílech	díl	k1gInPc6	díl
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Studio	studio	k1gNnSc1	studio
100	[number]	k4	100
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgNnSc2	ten
hodně	hodně	k6eAd1	hodně
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
v	v	k7c6	v
původních	původní	k2eAgInPc6d1	původní
dílech	díl	k1gInPc6	díl
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
obměnily	obměnit	k5eAaPmAgFnP	obměnit
hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
<g/>
:	:	kIx,	:
Mája	Mája	k1gFnSc1	Mája
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
svého	svůj	k3xOyFgInSc2	svůj
úlu	úl	k1gInSc2	úl
a	a	k8xC	a
Vilík	Vilík	k1gMnSc1	Vilík
v	v	k7c6	v
úlu	úl	k1gInSc6	úl
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Máji	Mája	k1gFnSc2	Mája
přespává	přespávat	k5eAaImIp3nS	přespávat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
spíše	spíše	k9	spíše
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Nových	Nových	k2eAgInPc2d1	Nových
dílů	díl	k1gInPc2	díl
je	být	k5eAaImIp3nS	být
78	[number]	k4	78
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
10-12	[number]	k4	10-12
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
staré	starý	k2eAgInPc1d1	starý
díly	díl	k1gInPc1	díl
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
20-24	[number]	k4	20-24
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
díly	díl	k1gInPc1	díl
vysílají	vysílat	k5eAaImIp3nP	vysílat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
Déčko	déčko	k1gNnSc1	déčko
patřícímu	patřící	k2eAgInSc3d1	patřící
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
nové	nový	k2eAgInPc1d1	nový
díly	díl	k1gInPc1	díl
vysílají	vysílat	k5eAaImIp3nP	vysílat
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
Jednotka	jednotka	k1gFnSc1	jednotka
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
od	od	k7c2	od
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
do	do	k7c2	do
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dílech	díl	k1gInPc6	díl
je	být	k5eAaImIp3nS	být
i	i	k9	i
několik	několik	k4yIc1	několik
nepříliš	příliš	k6eNd1	příliš
výrazných	výrazný	k2eAgFnPc2d1	výrazná
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
postavách	postava	k1gFnPc6	postava
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dřívějších	dřívější	k2eAgInPc2d1	dřívější
dílů	díl	k1gInPc2	díl
Májin	Májin	k2eAgInSc1d1	Májin
rodný	rodný	k2eAgInSc1d1	rodný
úl	úl	k1gInSc1	úl
přátelí	přátelit	k5eAaImIp3nS	přátelit
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
<g/>
,	,	kIx,	,
vzdáleným	vzdálený	k2eAgInSc7d1	vzdálený
úlem	úl	k1gInSc7	úl
Holy	hola	k1gFnSc2	hola
Hok	Hok	k1gFnSc2	Hok
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
některé	některý	k3yIgFnPc1	některý
postavy	postava	k1gFnPc1	postava
ubyly	ubýt	k5eAaPmAgFnP	ubýt
a	a	k8xC	a
jiné	jiná	k1gFnPc1	jiná
zase	zase	k9	zase
přibyly	přibýt	k5eAaPmAgFnP	přibýt
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
chování	chování	k1gNnSc1	chování
změnilo	změnit	k5eAaPmAgNnS	změnit
k	k	k7c3	k
přátelštějšímu	přátelský	k2eAgNnSc3d2	přátelštější
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
japonská	japonský	k2eAgFnSc1d1	japonská
znělka	znělka	k1gFnSc1	znělka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Micubači	Micubač	k1gInSc3	Micubač
Mája	Mája	k1gFnSc1	Mája
no	no	k9	no
bóken	bóken	k2eAgMnSc1d1	bóken
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
včelky	včelka	k1gFnSc2	včelka
Máji	Mája	k1gFnSc2	Mája
<g/>
"	"	kIx"	"
-	-	kIx~	-
蜜	蜜	k?	蜜
<g/>
)	)	kIx)	)
a	a	k8xC	a
znělka	znělka	k1gFnSc1	znělka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
seriálu	seriál	k1gInSc2	seriál
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc1	název
Ojasumi	Ojasu	k1gFnPc7	Ojasu
Mája	Mája	k1gFnSc1	Mája
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dobrou	dobrý	k2eAgFnSc4d1	dobrá
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
Májo	Mája	k1gFnSc5	Mája
<g/>
"	"	kIx"	"
-	-	kIx~	-
お	お	k?	お
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
písničky	písnička	k1gFnPc4	písnička
složil	složit	k5eAaPmAgMnS	složit
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
Šózó	Šózó	k1gFnPc2	Šózó
Ise	Ise	k1gFnSc2	Ise
(	(	kIx(	(
<g/>
伊	伊	k?	伊
正	正	k?	正
<g/>
)	)	kIx)	)
a	a	k8xC	a
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
Cheetah	Cheetah	k1gInSc4	Cheetah
se	s	k7c7	s
sborem	sbor	k1gInSc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
první	první	k4xOgFnSc1	první
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
verzí	verze	k1gFnSc7	verze
titulní	titulní	k2eAgFnSc2d1	titulní
písně	píseň	k1gFnSc2	píseň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ta	ten	k3xDgFnSc1	ten
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
složil	složit	k5eAaPmAgMnS	složit
Karel	Karel	k1gMnSc1	Karel
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
kromě	kromě	k7c2	kromě
německé	německý	k2eAgFnSc2d1	německá
použita	použít	k5eAaPmNgNnP	použít
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
verze	verze	k1gFnPc4	verze
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
,	,	kIx,	,
český	český	k2eAgInSc4d1	český
text	text	k1gInSc4	text
napsal	napsat	k5eAaBmAgMnS	napsat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Rytíř	Rytíř	k1gMnSc1	Rytíř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
<g/>
,	,	kIx,	,
portugalskou	portugalský	k2eAgFnSc4d1	portugalská
<g/>
,	,	kIx,	,
finskou	finský	k2eAgFnSc4d1	finská
<g/>
,	,	kIx,	,
nizozemskou	nizozemský	k2eAgFnSc4d1	nizozemská
či	či	k8xC	či
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
anglických	anglický	k2eAgFnPc2d1	anglická
verzí	verze	k1gFnPc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
rozšířenosti	rozšířenost	k1gFnSc3	rozšířenost
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
nejhranější	hraný	k2eAgFnSc7d3	nejhranější
skladbou	skladba	k1gFnSc7	skladba
moderního	moderní	k2eAgMnSc2d1	moderní
českého	český	k2eAgMnSc2d1	český
autora	autor	k1gMnSc2	autor
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
narozená	narozený	k2eAgFnSc1d1	narozená
včela	včela	k1gFnSc1	včela
dělnice	dělnice	k1gFnSc2	dělnice
jménem	jméno	k1gNnSc7	jméno
Mája	Mája	k1gFnSc1	Mája
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
namluvila	namluvit	k5eAaBmAgFnS	namluvit
Aťka	Aťk	k2eAgFnSc1d1	Aťka
Janoušková	Janoušková	k1gFnSc1	Janoušková
<g/>
)	)	kIx)	)
zvědavě	zvědavě	k6eAd1	zvědavě
poznává	poznávat	k5eAaImIp3nS	poznávat
svět	svět	k1gInSc1	svět
kolem	kolem	k7c2	kolem
svého	svůj	k3xOyFgInSc2	svůj
úlu	úl	k1gInSc2	úl
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
:	:	kIx,	:
trubce	trubec	k1gMnSc4	trubec
Vilíka	Vilík	k1gMnSc4	Vilík
<g/>
,	,	kIx,	,
tetu	teta	k1gFnSc4	teta
Kasandru	Kasandra	k1gFnSc4	Kasandra
<g/>
,	,	kIx,	,
myšáka	myšák	k1gMnSc4	myšák
Alexandra	Alexandr	k1gMnSc4	Alexandr
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
dílů	díl	k1gInPc2	díl
provází	provázet	k5eAaImIp3nS	provázet
luční	luční	k2eAgMnSc1d1	luční
koník	koník	k1gMnSc1	koník
Hop	hop	k0	hop
(	(	kIx(	(
<g/>
namluvil	namluvit	k5eAaBmAgMnS	namluvit
Boris	Boris	k1gMnSc1	Boris
Rösner	Rösner	k1gMnSc1	Rösner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
původní	původní	k2eAgFnSc2d1	původní
knihy	kniha	k1gFnSc2	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
na	na	k7c6	na
audiokazetách	audiokazeta	k1gFnPc6	audiokazeta
a	a	k8xC	a
gramodeskách	gramodeska	k1gFnPc6	gramodeska
namluvená	namluvený	k2eAgNnPc4d1	namluvené
vyprávění	vyprávění	k1gNnPc4	vyprávění
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
originálu	originál	k1gInSc3	originál
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
pocházely	pocházet	k5eAaImAgInP	pocházet
jen	jen	k6eAd1	jen
obrázky	obrázek	k1gInPc1	obrázek
na	na	k7c6	na
přebalu	přebal	k1gInSc6	přebal
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
hlasy	hlas	k1gInPc1	hlas
postavám	postava	k1gFnPc3	postava
propůjčili	propůjčit	k5eAaPmAgMnP	propůjčit
jiní	jiný	k2eAgMnPc1d1	jiný
herci	herec	k1gMnPc1	herec
(	(	kIx(	(
<g/>
Máju	Mája	k1gFnSc4	Mája
mluvila	mluvit	k5eAaImAgFnS	mluvit
Naďa	Naďa	k1gFnSc1	Naďa
Konvalinková	Konvalinková	k1gFnSc1	Konvalinková
<g/>
,	,	kIx,	,
Hopa	Hopa	k1gMnSc1	Hopa
Luděk	Luděk	k1gMnSc1	Luděk
Sobota	Sobota	k1gMnSc1	Sobota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
