<s>
USS	USS	kA
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
(	(	kIx(
<g/>
BB-	BB-	k1gFnSc1
<g/>
40	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Bitevní	bitevní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
Třída	třída	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
New	New	k?
Mexico	Mexico	k6eAd1
</s>
<s>
Číslo	číslo	k1gNnSc1
trupu	trup	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
BB-40	BB-40	k4
</s>
<s>
Jméno	jméno	k1gNnSc1
podle	podle	k7c2
<g/>
:	:	kIx,
</s>
<s>
americký	americký	k2eAgInSc1d1
stát	stát	k1gInSc1
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
</s>
<s>
Zahájení	zahájení	k1gNnSc1
stavby	stavba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1915	#num#	k4
</s>
<s>
Spuštěna	spuštěn	k2eAgNnPc1d1
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1917	#num#	k4
</s>
<s>
Uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1918	#num#	k4
</s>
<s>
Osud	osud	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
prodána	prodat	k5eAaPmNgFnS
do	do	k7c2
šrotu	šrot	k1gInSc2
<g/>
,	,	kIx,
1947	#num#	k4
</s>
<s>
Takticko-technická	takticko-technický	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Výtlak	výtlak	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
32	#num#	k4
514	#num#	k4
t	t	k?
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
190	#num#	k4
m	m	kA
</s>
<s>
Šířka	šířka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
29,69	29,69	k4
m	m	kA
</s>
<s>
Ponor	ponor	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
9,1	9,1	k4
m	m	kA
</s>
<s>
Pohon	pohon	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
9	#num#	k4
×	×	k?
Babcock	Babcock	k1gInSc1
&	&	k?
Wilcox	Wilcox	k1gInSc1
<g/>
27	#num#	k4
500	#num#	k4
shp	shp	k?
(	(	kIx(
<g/>
20	#num#	k4
500	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
</s>
<s>
Palivo	palivo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
nafta	nafta	k1gFnSc1
</s>
<s>
Rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
21	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
39	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Dosah	dosah	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
000	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
15	#num#	k4
000	#num#	k4
km	km	kA
<g/>
)	)	kIx)
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
10	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
19	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
084	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
důstojníků	důstojník	k1gMnPc2
</s>
<s>
Pancíř	pancíř	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
boky	boka	k1gFnPc1
<g/>
:	:	kIx,
203	#num#	k4
mm	mm	kA
-	-	kIx~
343	#num#	k4
mmpaluba	mmpaluba	k1gFnSc1
<g/>
:	:	kIx,
89	#num#	k4
mmbarbety	mmbarbeta	k1gFnPc1
<g/>
:	:	kIx,
330	#num#	k4
mmdělová	mmdělový	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
:	:	kIx,
457	#num#	k4
mmvelitelská	mmvelitelský	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
:	:	kIx,
292	#num#	k4
mm	mm	kA
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
12	#num#	k4
x	x	k?
356	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
x	x	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
22	#num#	k4
x	x	k?
127	#num#	k4
<g/>
mm	mm	kA
(	(	kIx(
<g/>
12	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
×	×	k?
76	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
×	×	k?
torpédomet	torpédomet	k1gInSc1
</s>
<s>
USS	USS	kA
New	New	k1gFnSc1
Mexico	Mexico	k6eAd1
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc7
lodí	loď	k1gFnSc7
třídy	třída	k1gFnSc2
New	New	k1gFnPc2
Mexico	Mexico	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenována	pojmenován	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
doktrínou	doktrína	k1gFnSc7
USA	USA	kA
po	po	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
amerických	americký	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
po	po	k7c6
47	#num#	k4
státě	stát	k1gInSc6
–	–	k?
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
dvě	dva	k4xCgFnPc4
sesterské	sesterský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
Mississippi	Mississippi	k1gFnSc2
a	a	k8xC
Idaho	Ida	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Kýl	kýl	k1gInSc1
byl	být	k5eAaImAgInS
položen	položit	k5eAaPmNgInS
v	v	k7c6
loděnicích	loděnice	k1gFnPc6
New	New	k1gFnSc2
York	York	k1gInSc1
Navy	Navy	k?
Yard	yard	k1gInSc1
v	v	k7c6
Brooklynu	Brooklyn	k1gInSc6
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vodu	voda	k1gFnSc4
byla	být	k5eAaImAgFnS
spuštěna	spuštěn	k2eAgFnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1917	#num#	k4
a	a	k8xC
americké	americký	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
převzalo	převzít	k5eAaPmAgNnS
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pohon	pohon	k1gInSc1
a	a	k8xC
pancéřování	pancéřování	k1gNnSc1
</s>
<s>
Pohon	pohon	k1gInSc4
zabezpečovalo	zabezpečovat	k5eAaImAgNnS
9	#num#	k4
kotlů	kotel	k1gInPc2
Babcock	Babcock	k1gInSc4
&	&	k?
Wilcox	Wilcox	k1gInSc1
pohánějící	pohánějící	k2eAgInSc1d1
4	#num#	k4
turbíny	turbína	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
následně	následně	k6eAd1
poháněly	pohánět	k5eAaImAgFnP
elektrické	elektrický	k2eAgInPc4d1
agregáty	agregát	k1gInPc4
<g/>
,	,	kIx,
napájející	napájející	k2eAgInPc4d1
elektromotory	elektromotor	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
otáčely	otáčet	k5eAaImAgInP
hřídeli	hřídel	k1gFnSc6
čtyř	čtyři	k4xCgInPc2
lodních	lodní	k2eAgInPc2d1
šroubů	šroub	k1gInPc2
s	s	k7c7
výsledným	výsledný	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
27	#num#	k4
500	#num#	k4
hp	hp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
lodi	loď	k1gFnSc2
byla	být	k5eAaImAgFnS
21	#num#	k4
u.	u.	k?
Pohonem	pohon	k1gInSc7
pomocí	pomocí	k7c2
elektrických	elektrický	k2eAgInPc2d1
agregátů	agregát	k1gInPc2
a	a	k8xC
elektromotorů	elektromotor	k1gInPc2
se	se	k3xPyFc4
USS	USS	kA
New	New	k1gFnSc1
Mexico	Mexico	k6eAd1
lišila	lišit	k5eAaImAgFnS
od	od	k7c2
svých	svůj	k3xOyFgFnPc2
sesterských	sesterský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
Mississippi	Mississippi	k1gFnSc2
a	a	k8xC
Idaho	Ida	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Boční	boční	k2eAgInSc1d1
pancéřový	pancéřový	k2eAgInSc1d1
pás	pás	k1gInSc1
byl	být	k5eAaImAgInS
silný	silný	k2eAgInSc1d1
279	#num#	k4
<g/>
–	–	k?
<g/>
203	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paluba	paluba	k1gFnSc1
byla	být	k5eAaImAgFnS
chráněna	chráněn	k2eAgFnSc1d1
152,5	152,5	k4
–	–	k?
76,2	76,2	k4
mm	mm	kA
pásem	pás	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
uloženo	uložit	k5eAaPmNgNnS
ve	v	k7c6
věžích	věž	k1gFnPc6
o	o	k7c6
síle	síla	k1gFnSc6
pancíře	pancíř	k1gInSc2
457	#num#	k4
-	-	kIx~
228	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sekundární	sekundární	k2eAgInSc4d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
pancéřování	pancéřování	k1gNnSc1
152	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
výzbroj	výzbroj	k1gFnSc1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
z	z	k7c2
dvanácti	dvanáct	k4xCc2
děl	dělo	k1gNnPc2
ráže	ráže	k1gFnSc1
365	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
14	#num#	k4
"	"	kIx"
<g/>
/	/	kIx~
50	#num#	k4
cal	cal	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Ve	v	k7c6
čtyřech	čtyři	k4xCgFnPc6
trojhlavňových	trojhlavňový	k2eAgFnPc6d1
věžích	věž	k1gFnPc6
(	(	kIx(
<g/>
dvě	dva	k4xCgFnPc4
vpředu	vpředu	k6eAd1
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
vzadu	vzadu	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocnou	pomocný	k2eAgFnSc4d1
výzbroj	výzbroj	k1gFnSc4
tvořilo	tvořit	k5eAaImAgNnS
22	#num#	k4
děl	dělo	k1gNnPc2
ráže	ráže	k1gFnSc1
127	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
5	#num#	k4
"	"	kIx"
<g/>
/	/	kIx~
51	#num#	k4
<g/>
)	)	kIx)
uložených	uložený	k2eAgInPc2d1
v	v	k7c6
kasematech	kasematy	k1gInPc6
po	po	k7c4
11	#num#	k4
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
krátké	krátký	k2eAgFnSc2d1
doby	doba	k1gFnSc2
bylo	být	k5eAaImAgNnS
odstraněno	odstranit	k5eAaPmNgNnS
8	#num#	k4
děl	dělo	k1gNnPc2
z	z	k7c2
nejnižších	nízký	k2eAgFnPc2d3
kasemat	kasemata	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
pomocná	pomocný	k2eAgFnSc1d1
výzbroj	výzbroj	k1gFnSc1
dala	dát	k5eAaPmAgFnS
použít	použít	k5eAaPmF
jen	jen	k9
při	při	k7c6
klidném	klidný	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
přidáno	přidán	k2eAgNnSc4d1
8	#num#	k4
x	x	k?
127	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
5	#num#	k4
"	"	kIx"
<g/>
/	/	kIx~
25	#num#	k4
cal	cal	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loď	loď	k1gFnSc1
nesla	nést	k5eAaImAgFnS
dva	dva	k4xCgInPc4
533	#num#	k4
mm	mm	kA
torpédomety	torpédomet	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Služba	služba	k1gFnSc1
</s>
<s>
USS	USS	kA
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
v	v	k7c6
Puget	Puget	k?
Sound	Sound	k1gInSc1
Navy	Navy	k?
Yard	yard	k1gInSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1943	#num#	k4
</s>
<s>
Zakotvená	zakotvený	k2eAgFnSc1d1
v	v	k7c6
Tokijském	tokijský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
pozadí	pozadí	k1gNnSc6
hora	hora	k1gFnSc1
Fudži	Fudž	k1gFnSc5
</s>
<s>
Do	do	k7c2
služby	služba	k1gFnSc2
byla	být	k5eAaImAgFnS
zařazena	zařazen	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1918	#num#	k4
<g/>
,	,	kIx,
krátce	krátce	k6eAd1
před	před	k7c7
koncem	konec	k1gInSc7
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
ještě	ještě	k6eAd1
stihla	stihnout	k5eAaPmAgFnS
zapojit	zapojit	k5eAaPmF
do	do	k7c2
hlídkování	hlídkování	k1gNnSc2
podél	podél	k7c2
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
převezla	převézt	k5eAaPmAgFnS
z	z	k7c2
Francie	Francie	k1gFnSc2
do	do	k7c2
USA	USA	kA
prezidenta	prezident	k1gMnSc2
Wilsona	Wilson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nato	nato	k6eAd1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
Pacifiku	Pacifik	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
americké	americký	k2eAgFnSc2d1
tichomořské	tichomořský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujících	následující	k2eAgInPc2d1
dvacet	dvacet	k4xCc1
let	léto	k1gNnPc2
působila	působit	k5eAaImAgFnS
střídavě	střídavě	k6eAd1
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
a	a	k8xC
Karibiku	Karibik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
loděnicích	loděnice	k1gFnPc6
Philadelphia	Philadelphia	k1gFnSc1
Navy	Navy	k?
Yard	yard	k1gInSc1
absolvovala	absolvovat	k5eAaPmAgFnS
od	od	k7c2
března	březen	k1gInSc2
1931	#num#	k4
do	do	k7c2
ledna	leden	k1gInSc2
1933	#num#	k4
modernizaci	modernizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
odstraněny	odstraněn	k2eAgInPc1d1
elektrické	elektrický	k2eAgInPc1d1
agregáty	agregát	k1gInPc1
a	a	k8xC
elektromotory	elektromotor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
změnou	změna	k1gFnSc7
pohonného	pohonný	k2eAgInSc2d1
systému	systém	k1gInSc2
se	se	k3xPyFc4
zvýšil	zvýšit	k5eAaPmAgInS
výkon	výkon	k1gInSc1
na	na	k7c4
32	#num#	k4
500	#num#	k4
hp	hp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvětšen	zvětšen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
náměr	náměr	k1gInSc1
děl	dělo	k1gNnPc2
(	(	kIx(
<g/>
delší	dlouhý	k2eAgInSc1d2
dostřel	dostřel	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
stožáry	stožár	k1gInPc1
trubkové	trubkový	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
byly	být	k5eAaImAgFnP
zrušeny	zrušit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
se	se	k3xPyFc4
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
vod	voda	k1gFnPc2
Atlantiku	Atlantik	k1gInSc2
vzhledem	vzhledem	k7c3
k	k	k7c3
sílící	sílící	k2eAgFnSc3d1
činnost	činnost	k1gFnSc4
německých	německý	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
vyhnula	vyhnout	k5eAaPmAgFnS
pohromě	pohroma	k1gFnSc3
v	v	k7c6
Pearl	Pearla	k1gFnPc2
Harboru	Harbor	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yRgNnSc6,k3yIgNnSc6,k3yQgNnSc6
byla	být	k5eAaImAgFnS
okamžitě	okamžitě	k6eAd1
převelena	převelet	k5eAaPmNgFnS
na	na	k7c4
Havajské	havajský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nahradila	nahradit	k5eAaPmAgFnS
ztráty	ztráta	k1gFnPc4
způsobené	způsobený	k2eAgFnPc4d1
tímto	tento	k3xDgInSc7
náletem	nálet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
kapitulaci	kapitulace	k1gFnSc6
Japonska	Japonsko	k1gNnSc2
se	se	k3xPyFc4
z	z	k7c2
Tokijského	tokijský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
cestu	cesta	k1gFnSc4
na	na	k7c4
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
USA	USA	kA
přes	přes	k7c4
Panamský	panamský	k2eAgInSc4d1
průplav	průplav	k1gInSc4
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1946	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Bostonu	Boston	k1gInSc6
vyřazena	vyřazen	k2eAgFnSc1d1
ze	z	k7c2
služby	služba	k1gFnSc2
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1947	#num#	k4
prodána	prodat	k5eAaPmNgFnS
do	do	k7c2
šrotu	šrot	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
USS	USS	kA
New	New	k1gFnSc1
Mexico	Mexico	k1gMnSc1
(	(	kIx(
<g/>
BB-	BB-	k1gFnSc1
<g/>
40	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
USS	USS	kA
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
(	(	kIx(
<g/>
BB-	BB-	k1gFnSc1
<g/>
40	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Třída	třída	k1gFnSc1
New	New	k1gFnSc2
Mexico	Mexico	k6eAd1
</s>
<s>
Seznam	seznam	k1gInSc1
amerických	americký	k2eAgFnPc2d1
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
USS	USS	kA
New	New	k1gFnSc2
Mexico	Mexico	k6eAd1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
USS	USS	kA
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
na	na	k7c4
military	militara	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Americké	americký	k2eAgFnPc1d1
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
třídy	třída	k1gFnSc2
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
</s>
<s>
New	New	k?
Mexico	Mexico	k1gMnSc1
•	•	k?
Mississippi	Mississippi	k1gFnSc2
•	•	k?
Idaho	Ida	k1gMnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
Pennsylania	Pennsylanium	k1gNnSc2
•	•	k?
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
Tennessee	Tennessee	k1gFnPc2
Seznam	seznam	k1gInSc4
amerických	americký	k2eAgFnPc2d1
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
</s>
