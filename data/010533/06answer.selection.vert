<s>
Jindřichohradecké	jindřichohradecký	k2eAgFnPc1d1	jindřichohradecká
místní	místní	k2eAgFnPc1d1	místní
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
VKM	VKM	kA	VKM
<g/>
:	:	kIx,	:
JHMD	JHMD	kA	JHMD
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
provozující	provozující	k2eAgFnSc1d1	provozující
jihočeské	jihočeský	k2eAgFnPc4d1	Jihočeská
úzkokolejné	úzkokolejný	k2eAgFnPc4d1	úzkokolejná
tratě	trať	k1gFnPc4	trať
z	z	k7c2	z
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
Hradce	Hradec	k1gInSc2	Hradec
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Bystřice	Bystřice	k1gFnSc2	Bystřice
a	a	k8xC	a
Obrataně	Obrataň	k1gFnSc2	Obrataň
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
provozování	provozování	k1gNnSc2	provozování
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
