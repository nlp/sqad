<p>
<s>
Jindřichohradecké	jindřichohradecký	k2eAgFnPc1d1	jindřichohradecká
místní	místní	k2eAgFnPc1d1	místní
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
VKM	VKM	kA	VKM
<g/>
:	:	kIx,	:
JHMD	JHMD	kA	JHMD
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
provozující	provozující	k2eAgFnSc1d1	provozující
jihočeské	jihočeský	k2eAgFnPc4d1	Jihočeská
úzkokolejné	úzkokolejný	k2eAgFnPc4d1	úzkokolejná
tratě	trať	k1gFnPc4	trať
z	z	k7c2	z
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
Hradce	Hradec	k1gInSc2	Hradec
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Bystřice	Bystřice	k1gFnSc2	Bystřice
a	a	k8xC	a
Obrataně	Obrataň	k1gFnSc2	Obrataň
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
provozování	provozování	k1gNnSc2	provozování
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
názvem	název	k1gInSc7	název
tohoto	tento	k3xDgInSc2	tento
železničního	železniční	k2eAgInSc2d1	železniční
subsystému	subsystém	k1gInSc2	subsystém
<g/>
.	.	kIx.	.
</s>
<s>
Jindřichohradecké	jindřichohradecký	k2eAgFnPc1d1	jindřichohradecká
místní	místní	k2eAgFnPc1d1	místní
dráhy	dráha	k1gFnPc1	dráha
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
soukromých	soukromý	k2eAgMnPc2d1	soukromý
provozovatelů	provozovatel	k1gMnPc2	provozovatel
veřejné	veřejný	k2eAgFnSc2d1	veřejná
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
finanční	finanční	k2eAgInSc4d1	finanční
rok	rok	k1gInSc4	rok
končící	končící	k2eAgFnSc2d1	končící
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
vykázaly	vykázat	k5eAaPmAgInP	vykázat
JHMD	JHMD	kA	JHMD
tržby	tržba	k1gFnSc2	tržba
za	za	k7c4	za
přepravu	přeprava	k1gFnSc4	přeprava
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
dotace	dotace	k1gFnPc4	dotace
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
66	[number]	k4	66
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1994	[number]	k4	1994
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
začala	začít	k5eAaPmAgFnS	začít
po	po	k7c6	po
zápisu	zápis	k1gInSc6	zápis
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
podnikat	podnikat	k5eAaImF	podnikat
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
uvedených	uvedený	k2eAgFnPc6d1	uvedená
tratích	trať	k1gFnPc6	trať
provozovala	provozovat	k5eAaImAgFnS	provozovat
letní	letní	k2eAgInPc4d1	letní
vlaky	vlak	k1gInPc4	vlak
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
po	po	k7c6	po
zastavení	zastavení	k1gNnSc6	zastavení
provozu	provoz	k1gInSc2	provoz
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Bystřice	Bystřice	k1gFnSc2	Bystřice
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vládou	vláda	k1gFnSc7	vláda
schválena	schválen	k2eAgFnSc1d1	schválena
privatizace	privatizace	k1gFnSc1	privatizace
obou	dva	k4xCgFnPc2	dva
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
JHMD	JHMD	kA	JHMD
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
cenu	cena	k1gFnSc4	cena
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
Kč	Kč	kA	Kč
koupila	koupit	k5eAaPmAgFnS	koupit
<g/>
.	.	kIx.	.
</s>
<s>
Fyzicky	fyzicky	k6eAd1	fyzicky
však	však	k9	však
byly	být	k5eAaImAgFnP	být
tratě	trať	k1gFnPc1	trať
předány	předat	k5eAaPmNgFnP	předat
až	až	k6eAd1	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Provoz	provoz	k1gInSc1	provoz
úzkokolejky	úzkokolejka	k1gFnSc2	úzkokolejka
byl	být	k5eAaImAgInS	být
ztrátový	ztrátový	k2eAgInSc1d1	ztrátový
i	i	k9	i
pro	pro	k7c4	pro
České	český	k2eAgFnPc4d1	Česká
dráhy	dráha	k1gFnPc4	dráha
<g/>
,	,	kIx,	,
úzkokolejka	úzkokolejka	k1gFnSc1	úzkokolejka
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k6eAd1	potřeba
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
uranu	uran	k1gInSc2	uran
jako	jako	k8xC	jako
strategické	strategický	k2eAgFnSc2d1	strategická
suroviny	surovina	k1gFnSc2	surovina
z	z	k7c2	z
dolu	dol	k1gInSc2	dol
Okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
Radouň	Radouň	k1gFnSc1	Radouň
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
železničních	železniční	k2eAgFnPc6d1	železniční
stanicích	stanice	k1gFnPc6	stanice
Jindřichohradeckých	jindřichohradecký	k2eAgFnPc2d1	jindřichohradecká
místních	místní	k2eAgFnPc2d1	místní
drah	draha	k1gFnPc2	draha
jsou	být	k5eAaImIp3nP	být
vydávány	vydáván	k2eAgFnPc1d1	vydávána
dobové	dobový	k2eAgFnPc1d1	dobová
lepenkové	lepenkový	k2eAgFnPc1d1	lepenková
jízdenky	jízdenka	k1gFnPc1	jízdenka
jak	jak	k8xC	jak
pro	pro	k7c4	pro
vlaky	vlak	k1gInPc4	vlak
motorové	motorový	k2eAgInPc4d1	motorový
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
parní	parní	k2eAgFnPc1d1	parní
trakce	trakce	k1gFnPc1	trakce
<g/>
.	.	kIx.	.
</s>
<s>
JHMD	JHMD	kA	JHMD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
odkoupily	odkoupit	k5eAaPmAgFnP	odkoupit
od	od	k7c2	od
NADASu	NADASus	k1gInSc2	NADASus
strojní	strojní	k2eAgNnSc1d1	strojní
vybavení	vybavení	k1gNnSc1	vybavení
jediné	jediný	k2eAgFnSc2d1	jediná
české	český	k2eAgFnSc2d1	Česká
tiskárny	tiskárna	k1gFnSc2	tiskárna
lepenkových	lepenkový	k2eAgFnPc2d1	lepenková
jízdenek	jízdenka	k1gFnPc2	jízdenka
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
provozují	provozovat	k5eAaImIp3nP	provozovat
v	v	k7c6	v
Kamenici	Kamenice	k1gFnSc6	Kamenice
nad	nad	k7c7	nad
Lipou	lípa	k1gFnSc7	lípa
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Třetinu	třetina	k1gFnSc4	třetina
její	její	k3xOp3gFnSc2	její
produkce	produkce	k1gFnSc2	produkce
používají	používat	k5eAaImIp3nP	používat
JHMD	JHMD	kA	JHMD
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
odebírají	odebírat	k5eAaImIp3nP	odebírat
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
pro	pro	k7c4	pro
nostalgické	nostalgický	k2eAgFnPc4d1	nostalgická
jízdy	jízda	k1gFnPc4	jízda
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc1d1	různá
muzejní	muzejní	k2eAgFnPc1d1	muzejní
železnice	železnice	k1gFnPc1	železnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
formátu	formát	k1gInSc6	formát
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
vstupenky	vstupenka	k1gFnPc4	vstupenka
<g/>
,	,	kIx,	,
losy	los	k1gInPc4	los
či	či	k8xC	či
vizitky	vizitka	k1gFnPc4	vizitka
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
osobní	osobní	k2eAgFnSc2d1	osobní
a	a	k8xC	a
nákladní	nákladní	k2eAgFnSc2d1	nákladní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
provozuje	provozovat	k5eAaImIp3nS	provozovat
JHMD	JHMD	kA	JHMD
v	v	k7c6	v
měsících	měsíc	k1gInPc6	měsíc
červen	červeno	k1gNnPc2	červeno
až	až	k9	až
září	zářit	k5eAaImIp3nP	zářit
také	také	k9	také
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
spoje	spoj	k1gInPc1	spoj
<g/>
,	,	kIx,	,
tažené	tažený	k2eAgInPc1d1	tažený
parní	parní	k2eAgFnSc7d1	parní
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
<g/>
,	,	kIx,	,
vlaky	vlak	k1gInPc7	vlak
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
s	s	k7c7	s
parní	parní	k2eAgFnSc7d1	parní
i	i	k8xC	i
motorovou	motorový	k2eAgFnSc7d1	motorová
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
<g/>
,	,	kIx,	,
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
připojení	připojení	k1gNnSc2	připojení
historické	historický	k2eAgFnSc2d1	historická
soupravy	souprava	k1gFnSc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
výlety	výlet	k1gInPc4	výlet
historickým	historický	k2eAgInSc7d1	historický
autobusem	autobus	k1gInSc7	autobus
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Landštejn	Landštejna	k1gFnPc2	Landštejna
a	a	k8xC	a
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
rakouské	rakouský	k2eAgFnSc2d1	rakouská
úzkokolejky	úzkokolejka	k1gFnSc2	úzkokolejka
<g/>
.	.	kIx.	.
</s>
<s>
JHMD	JHMD	kA	JHMD
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
staly	stát	k5eAaPmAgFnP	stát
prvním	první	k4xOgMnSc6	první
železničním	železniční	k2eAgNnSc7d1	železniční
dopravcem	dopravce	k1gMnSc7	dopravce
zapojeným	zapojený	k2eAgFnPc3d1	zapojená
do	do	k7c2	do
autobusového	autobusový	k2eAgInSc2d1	autobusový
místenkového	místenkový	k2eAgInSc2d1	místenkový
systému	systém	k1gInSc2	systém
AMSBUS	AMSBUS	kA	AMSBUS
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
se	se	k3xPyFc4	se
Jindřichohradecké	jindřichohradecký	k2eAgFnPc1d1	jindřichohradecká
místní	místní	k2eAgFnPc1d1	místní
dráhy	dráha	k1gFnPc1	dráha
také	také	k9	také
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
normálněrozchodných	normálněrozchodný	k2eAgInPc2d1	normálněrozchodný
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
tratě	trať	k1gFnPc4	trať
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
-	-	kIx~	-
Doupovskou	Doupovská	k1gFnSc4	Doupovská
a	a	k8xC	a
Švestkovou	švestkový	k2eAgFnSc4d1	švestková
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
v	v	k7c6	v
knižním	knižní	k2eAgInSc6d1	knižní
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
označené	označený	k2eAgFnSc2d1	označená
jako	jako	k8xC	jako
113	[number]	k4	113
a	a	k8xC	a
164	[number]	k4	164
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
Bystřice	Bystřice	k1gFnSc1	Bystřice
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
Obrataň	Obrataň	k1gFnSc1	Obrataň
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
železničních	železniční	k2eAgMnPc2d1	železniční
dopravců	dopravce	k1gMnPc2	dopravce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jindřichohradecké	jindřichohradecký	k2eAgFnSc2d1	jindřichohradecká
místní	místní	k2eAgFnSc2d1	místní
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jindřichohradecké	jindřichohradecký	k2eAgFnPc1d1	jindřichohradecká
místní	místní	k2eAgFnPc1d1	místní
dráhy	dráha	k1gFnPc1	dráha
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Brabenec	Brabenec	k1gMnSc1	Brabenec
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Lužný	Lužný	k1gMnSc1	Lužný
<g/>
:	:	kIx,	:
Jindřichohradecké	jindřichohradecký	k2eAgFnSc2d1	jindřichohradecká
úzkorozchodky	úzkorozchodka	k1gFnSc2	úzkorozchodka
(	(	kIx(	(
<g/>
spz	spz	k?	spz
<g/>
.	.	kIx.	.
<g/>
logout	logout	k1gMnSc1	logout
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VIDEO	video	k1gNnSc1	video
<g/>
:	:	kIx,	:
Motorový	motorový	k2eAgInSc1d1	motorový
osobní	osobní	k2eAgInSc1d1	osobní
vlak	vlak	k1gInSc1	vlak
JHMD	JHMD	kA	JHMD
opouští	opouštět	k5eAaImIp3nS	opouštět
stanici	stanice	k1gFnSc3	stanice
Nová	nový	k2eAgFnSc1d1	nová
Bystřice	Bystřice	k1gFnSc1	Bystřice
</s>
</p>
<p>
<s>
..	..	k?	..
<g/>
:	:	kIx,	:
Jindřichohradecké	jindřichohradecký	k2eAgFnPc1d1	jindřichohradecká
úkokolejky	úkokolejka	k1gFnPc1	úkokolejka
znovu	znovu	k6eAd1	znovu
ožily	ožít	k5eAaPmAgFnP	ožít
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
turistické	turistický	k2eAgFnSc2d1	turistická
sezóny	sezóna	k1gFnSc2	sezóna
:	:	kIx,	:
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Doupovská	Doupovský	k2eAgFnSc1d1	Doupovská
dráha	dráha	k1gFnSc1	dráha
</s>
</p>
