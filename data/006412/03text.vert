<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
filmový	filmový	k2eAgInSc1d1	filmový
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
čelní	čelní	k2eAgFnSc1d1	čelní
postava	postava	k1gFnSc1	postava
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc7d1	původní
profesí	profes	k1gFnSc7	profes
byl	být	k5eAaImAgMnS	být
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
také	také	k9	také
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
psal	psát	k5eAaImAgMnS	psát
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
o	o	k7c6	o
českém	český	k2eAgMnSc6d1	český
fiktivním	fiktivní	k2eAgMnSc6d1	fiktivní
géniovi	génius	k1gMnSc6	génius
Járovi	Jára	k1gMnSc6	Jára
Cimrmanovi	Cimrman	k1gMnSc6	Cimrman
a	a	k8xC	a
scénáře	scénář	k1gInPc4	scénář
filmových	filmový	k2eAgFnPc2d1	filmová
komedií	komedie	k1gFnPc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
divadelní	divadelní	k2eAgFnSc2d1	divadelní
režie	režie	k1gFnSc2	režie
na	na	k7c6	na
DAMU	DAMU	kA	DAMU
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
asistentem	asistent	k1gMnSc7	asistent
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
technické	technický	k2eAgFnSc2d1	technická
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fyziky	fyzika	k1gFnSc2	fyzika
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
středoškolským	středoškolský	k2eAgMnSc7d1	středoškolský
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
Gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Brandýse	Brandýs	k1gInSc6	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
Mladého	mladý	k2eAgInSc2d1	mladý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
během	během	k7c2	během
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
redakci	redakce	k1gFnSc6	redakce
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
čtenářské	čtenářský	k2eAgFnSc2d1	čtenářská
ankety	anketa	k1gFnSc2	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
nakladatelský	nakladatelský	k2eAgMnSc1d1	nakladatelský
redaktor	redaktor	k1gMnSc1	redaktor
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
scenáristou	scenárista	k1gMnSc7	scenárista
Filmového	filmový	k2eAgNnSc2d1	filmové
studia	studio	k1gNnSc2	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Šebánkem	Šebánek	k1gMnSc7	Šebánek
založili	založit	k5eAaPmAgMnP	založit
Divadlo	divadlo	k1gNnSc4	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
psali	psát	k5eAaImAgMnP	psát
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
divadlo	divadlo	k1gNnSc4	divadlo
i	i	k9	i
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gFnPc2	jeho
<g/>
"	"	kIx"	"
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
také	také	k9	také
autorsky	autorsky	k6eAd1	autorsky
a	a	k8xC	a
herecky	herecky	k6eAd1	herecky
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
Smoljak	Smoljak	k1gInSc1	Smoljak
i	i	k9	i
režíroval	režírovat	k5eAaImAgInS	režírovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
lidé	člověk	k1gMnPc1	člověk
pletli	plést	k5eAaImAgMnP	plést
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Petrem	Petr	k1gMnSc7	Petr
Spáleným	spálený	k2eAgMnSc7d1	spálený
<g/>
.	.	kIx.	.
</s>
<s>
Smoljak	Smoljak	k1gMnSc1	Smoljak
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jednou	k6eAd1	jednou
nastane	nastat	k5eAaPmIp3nS	nastat
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
lidé	člověk	k1gMnPc1	člověk
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
se	s	k7c7	s
Smoljakem	Smoljak	k1gMnSc7	Smoljak
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bude	být	k5eAaImBp3nS	být
Petr	Petr	k1gMnSc1	Petr
Spálený	spálený	k2eAgInSc1d1	spálený
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
třikrát	třikrát	k6eAd1	třikrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
rozvedený	rozvedený	k2eAgMnSc1d1	rozvedený
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
4	[number]	k4	4
děti	dítě	k1gFnPc4	dítě
–	–	k?	–
Davida	David	k1gMnSc2	David
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Filipa	Filip	k1gMnSc4	Filip
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kateřinu	Kateřina	k1gFnSc4	Kateřina
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
a	a	k8xC	a
Alžbětu	Alžběta	k1gFnSc4	Alžběta
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
v	v	k7c6	v
kladenské	kladenský	k2eAgFnSc6d1	kladenská
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
byl	být	k5eAaImAgInS	být
činný	činný	k2eAgMnSc1d1	činný
také	také	k9	také
ve	v	k7c6	v
Studiu	studio	k1gNnSc6	studio
Láďa	Láďa	k1gFnSc1	Láďa
Ladislava	Ladislav	k1gMnSc2	Ladislav
Smoljaka	Smoljak	k1gMnSc2	Smoljak
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Studio	studio	k1gNnSc1	studio
Jára	Jára	k1gFnSc1	Jára
<g/>
)	)	kIx)	)
<g/>
..	..	k?	..
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
uskupení	uskupení	k1gNnSc4	uskupení
napsal	napsat	k5eAaBmAgMnS	napsat
několik	několik	k4yIc4	několik
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Hymna	hymna	k1gFnSc1	hymna
aneb	aneb	k?	aneb
Urfidlovačka	Urfidlovačka	k1gFnSc1	Urfidlovačka
<g/>
)	)	kIx)	)
Sledoval	sledovat	k5eAaImAgMnS	sledovat
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
nacházeli	nacházet	k5eAaImAgMnP	nacházet
diváci	divák	k1gMnPc1	divák
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
divadelních	divadelní	k2eAgFnPc6d1	divadelní
hrách	hra	k1gFnPc6	hra
politické	politický	k2eAgFnSc2d1	politická
paralely	paralela	k1gFnSc2	paralela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
také	také	k6eAd1	také
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
popularitě	popularita	k1gFnSc3	popularita
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
vyhýbal	vyhýbat	k5eAaImAgMnS	vyhýbat
prvoplánové	prvoplánový	k2eAgFnSc3d1	prvoplánová
politické	politický	k2eAgFnSc3d1	politická
satiře	satira	k1gFnSc3	satira
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
také	také	k9	také
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
režimu	režim	k1gInSc2	režim
k	k	k7c3	k
Divadlu	divadlo	k1gNnSc3	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
často	často	k6eAd1	často
stěhovat	stěhovat	k5eAaImF	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
podporoval	podporovat	k5eAaImAgMnS	podporovat
změnu	změna	k1gFnSc4	změna
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
často	často	k6eAd1	často
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
komentoval	komentovat	k5eAaBmAgMnS	komentovat
a	a	k8xC	a
angažoval	angažovat	k5eAaBmAgMnS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
občanských	občanský	k2eAgInPc6d1	občanský
protestech	protest	k1gInPc6	protest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
výzvě	výzva	k1gFnSc3	výzva
divadelníků	divadelník	k1gMnPc2	divadelník
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
vlády	vláda	k1gFnSc2	vláda
před	před	k7c7	před
chystaným	chystaný	k2eAgNnSc7d1	chystané
hlasováním	hlasování	k1gNnSc7	hlasování
o	o	k7c6	o
vyslovení	vyslovení	k1gNnSc6	vyslovení
důvěry	důvěra	k1gFnSc2	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
podepsal	podepsat	k5eAaPmAgMnS	podepsat
pod	pod	k7c4	pod
otevřený	otevřený	k2eAgInSc4d1	otevřený
dopis	dopis	k1gInSc4	dopis
umělců	umělec	k1gMnPc2	umělec
předsedovi	předseda	k1gMnSc3	předseda
vlády	vláda	k1gFnSc2	vláda
Miloši	Miloš	k1gMnSc3	Miloš
Zemanovi	Zeman	k1gMnSc3	Zeman
protestující	protestující	k2eAgMnPc1d1	protestující
proti	proti	k7c3	proti
odvolání	odvolání	k1gNnSc3	odvolání
ředitelky	ředitelka	k1gFnSc2	ředitelka
pražské	pražský	k2eAgFnSc2d1	Pražská
vinohradské	vinohradský	k2eAgFnSc2d1	Vinohradská
nemocnice	nemocnice	k1gFnSc2	nemocnice
Zuzany	Zuzana	k1gFnSc2	Zuzana
Roithové	Roithová	k1gFnSc2	Roithová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
televizní	televizní	k2eAgFnSc2d1	televizní
krize	krize	k1gFnSc2	krize
podpořil	podpořit	k5eAaPmAgMnS	podpořit
stávkující	stávkující	k2eAgMnPc4d1	stávkující
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
podpořil	podpořit	k5eAaPmAgMnS	podpořit
stavbu	stavba	k1gFnSc4	stavba
radarové	radarový	k2eAgFnSc2d1	radarová
základny	základna	k1gFnSc2	základna
USA	USA	kA	USA
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Cibulkových	Cibulkových	k2eAgInPc2d1	Cibulkových
seznamů	seznam	k1gInPc2	seznam
byl	být	k5eAaImAgMnS	být
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
StB	StB	k1gFnSc2	StB
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
důvěrník	důvěrník	k1gMnSc1	důvěrník
<g/>
,	,	kIx,	,
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
Stožický	Stožický	k2eAgMnSc1d1	Stožický
<g/>
,	,	kIx,	,
evidenční	evidenční	k2eAgNnPc4d1	evidenční
čísla	číslo	k1gNnPc4	číslo
23	[number]	k4	23
0	[number]	k4	0
<g/>
47	[number]	k4	47
<g/>
,	,	kIx,	,
8	[number]	k4	8
823	[number]	k4	823
0	[number]	k4	0
<g/>
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
seznamech	seznam	k1gInPc6	seznam
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
však	však	k8xC	však
uveden	uvést	k5eAaPmNgInS	uvést
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
nálezu	nález	k1gInSc2	nález
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
totiž	totiž	k9	totiž
nelze	lze	k6eNd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvěrníci	důvěrník	k1gMnPc1	důvěrník
byli	být	k5eAaImAgMnP	být
vědomými	vědomý	k2eAgMnPc7d1	vědomý
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejnověji	nově	k6eAd3	nově
se	se	k3xPyFc4	se
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
chybě	chyba	k1gFnSc3	chyba
při	při	k7c6	při
metodice	metodika	k1gFnSc6	metodika
přepisu	přepis	k1gInSc2	přepis
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Smoljak	Smoljak	k1gMnSc1	Smoljak
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgMnS	být
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k8xS	jako
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
StB	StB	k1gMnPc1	StB
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
hrající	hrající	k2eAgMnSc1d1	hrající
<g/>
,	,	kIx,	,
bdící	bdící	k2eAgMnSc1d1	bdící
-	-	kIx~	-
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Fragment	fragment	k1gInSc1	fragment
1974	[number]	k4	1974
Jáchyme	Jáchym	k1gMnSc5	Jáchym
<g/>
,	,	kIx,	,
hoď	hodit	k5eAaImRp2nS	hodit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
a	a	k8xC	a
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Lipským	lipský	k2eAgInSc7d1	lipský
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
Marečku	Mareček	k1gMnSc5	Mareček
<g/>
,	,	kIx,	,
podejte	podat	k5eAaPmRp2nP	podat
mi	já	k3xPp1nSc3	já
pero	pero	k1gNnSc5	pero
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
Na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
lesa	les	k1gInSc2	les
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
Kulový	kulový	k2eAgInSc1d1	kulový
blesk	blesk	k1gInSc1	blesk
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
a	a	k8xC	a
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Podskalským	podskalský	k2eAgInSc7d1	podskalský
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
Trhák	trhák	k1gInSc1	trhák
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgMnSc1d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgMnSc1d1	spící
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
Rozpuštěný	rozpuštěný	k2eAgMnSc1d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgMnSc1d1	vypuštěný
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Svěrákem	Svěrák	k1gMnSc7	Svěrák
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
Kulový	kulový	k2eAgInSc1d1	kulový
blesk	blesk	k1gInSc1	blesk
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Podskalským	podskalský	k2eAgInSc7d1	podskalský
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
Vrchní	vrchní	k2eAgInPc1d1	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
1983	[number]	k4	1983
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgInSc4d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgInSc4d1	spící
1984	[number]	k4	1984
Rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgInSc4d1	vypuštěný
1987	[number]	k4	1987
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
1990	[number]	k4	1990
Tvrdý	Tvrdý	k1gMnSc1	Tvrdý
chleba	chléb	k1gInSc2	chléb
–	–	k?	–
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
1990	[number]	k4	1990
Motýl	motýl	k1gMnSc1	motýl
na	na	k7c6	na
anténě	anténa	k1gFnSc6	anténa
–	–	k?	–
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
1992	[number]	k4	1992
Osvětová	osvětový	k2eAgFnSc1d1	osvětová
přednáška	přednáška	k1gFnSc1	přednáška
v	v	k7c6	v
Suché	Suché	k2eAgFnSc6d1	Suché
Vrbici	vrbice	k1gFnSc6	vrbice
–	–	k?	–
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
1992	[number]	k4	1992
Ať	ať	k8xS	ať
ten	ten	k3xDgMnSc1	ten
kůň	kůň	k1gMnSc1	kůň
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
1996	[number]	k4	1996
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
policejní	policejní	k2eAgFnSc2d1	policejní
brašny	brašna	k1gFnSc2	brašna
–	–	k?	–
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
1968	[number]	k4	1968
Zločin	zločin	k1gInSc1	zločin
v	v	k7c6	v
šantánu	šantán	k1gInSc6	šantán
1970	[number]	k4	1970
Pane	Pan	k1gMnSc5	Pan
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
vdova	vdova	k1gFnSc1	vdova
<g/>
!	!	kIx.	!
</s>
<s>
1972	[number]	k4	1972
Homolka	homolka	k1gFnSc1	homolka
a	a	k8xC	a
tobolka	tobolka	k1gFnSc1	tobolka
1974	[number]	k4	1974
Jáchyme	Jáchym	k1gMnSc5	Jáchym
<g/>
,	,	kIx,	,
hoď	hodit	k5eAaPmRp2nS	hodit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
<g/>
!	!	kIx.	!
</s>
<s>
1976	[number]	k4	1976
Marečku	Mareček	k1gMnSc5	Mareček
<g/>
,	,	kIx,	,
podejte	podat	k5eAaPmRp2nP	podat
mi	já	k3xPp1nSc3	já
pero	pero	k1gNnSc1	pero
<g/>
!	!	kIx.	!
</s>
<s>
1976	[number]	k4	1976
Na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
lesa	les	k1gInSc2	les
1976	[number]	k4	1976
To	to	k9	to
byla	být	k5eAaImAgFnS	být
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
strýčku	strýček	k1gMnSc5	strýček
<g/>
!	!	kIx.	!
</s>
<s>
1978	[number]	k4	1978
Kulový	kulový	k2eAgInSc4d1	kulový
blesk	blesk	k1gInSc4	blesk
1980	[number]	k4	1980
Trhák	trhák	k1gInSc1	trhák
1980	[number]	k4	1980
Vrchní	vrchní	k1gFnSc2	vrchní
<g/>
,	,	kIx,	,
prchni	prchnout	k5eAaPmRp2nS	prchnout
<g/>
!	!	kIx.	!
</s>
<s>
1981	[number]	k4	1981
Loutka	loutka	k1gFnSc1	loutka
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
člověka	člověk	k1gMnSc2	člověk
1983	[number]	k4	1983
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgMnSc1d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgMnSc1d1	spící
1983	[number]	k4	1983
Svatební	svatební	k2eAgFnSc1d1	svatební
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Jiljí	Jiljí	k1gMnSc2	Jiljí
1984	[number]	k4	1984
Co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
doktore	doktor	k1gMnSc5	doktor
<g/>
?	?	kIx.	?
</s>
<s>
1984	[number]	k4	1984
Rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgInSc4d1	vypuštěný
1987	[number]	k4	1987
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
1990	[number]	k4	1990
Wette	Wett	k1gInSc5	Wett
<g/>
,	,	kIx,	,
Die	Die	k1gFnPc2	Die
1993	[number]	k4	1993
Nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
teta	teta	k1gFnSc1	teta
1994	[number]	k4	1994
Akumulátor	akumulátor	k1gInSc1	akumulátor
1	[number]	k4	1
1996	[number]	k4	1996
Kolja	Kolj	k1gInSc2	Kolj
2006	[number]	k4	2006
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
Byl	být	k5eAaImAgInS	být
režisérem	režisér	k1gMnSc7	režisér
i	i	k8xC	i
hercem	herc	k1gInSc7	herc
všech	všecek	k3xTgFnPc2	všecek
her	hra	k1gFnPc2	hra
DJC	DJC	kA	DJC
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
napsal	napsat	k5eAaPmAgInS	napsat
společně	společně	k6eAd1	společně
se	s	k7c7	s
Z.	Z.	kA	Z.
Svěrákem	svěrák	k1gInSc7	svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
hry	hra	k1gFnPc1	hra
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Akt	akt	k1gInSc1	akt
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
-	-	kIx~	-
autorem	autor	k1gMnSc7	autor
Z.	Z.	kA	Z.
Svěrák	svěrák	k1gInSc1	svěrák
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
ztráty	ztráta	k1gFnSc2	ztráta
třídní	třídní	k2eAgFnSc2d1	třídní
knihy	kniha	k1gFnSc2	kniha
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
-	-	kIx~	-
autorem	autor	k1gMnSc7	autor
L.	L.	kA	L.
Smoljak	Smoljak	k1gMnSc1	Smoljak
Domácí	domácí	k1gMnSc1	domácí
zabijačka	zabijačka	k1gFnSc1	zabijačka
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
-	-	kIx~	-
autorem	autor	k1gMnSc7	autor
J.	J.	kA	J.
Šebánek	Šebánek	k1gMnSc1	Šebánek
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
DJC	DJC	kA	DJC
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
Hospoda	Hospoda	k?	Hospoda
<g />
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mýtince	mýtinka	k1gFnSc6	mýtinka
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salonním	salonní	k2eAgInSc6d1	salonní
coupé	coupý	k2eAgNnSc1d1	coupé
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Němý	němý	k2eAgMnSc1d1	němý
Bobeš	Bobeš	k1gMnSc1	Bobeš
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Posel	posít	k5eAaPmAgInS	posít
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
Liptákova	Liptákův	k2eAgFnSc1d1	Liptákova
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Lijavec	lijavec	k1gInSc1	lijavec
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Dobytí	dobytí	k1gNnSc6	dobytí
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Blaník	Blaník	k1gInSc1	Blaník
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Záskok	záskok	k1gInSc1	záskok
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Švestka	švestka	k1gFnSc1	švestka
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Hymna	hymna	k1gFnSc1	hymna
aneb	aneb	k?	aneb
Urfidlovačka	Urfidlovačka	k1gFnSc1	Urfidlovačka
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Malý	malý	k2eAgInSc1d1	malý
Říjen	říjen	k1gInSc1	říjen
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Fantom	fantom	k1gInSc4	fantom
realistického	realistický	k2eAgNnSc2d1	realistické
divadla	divadlo	k1gNnSc2	divadlo
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
DNZ	DNZ	kA	DNZ
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Hus	Hus	k1gMnSc1	Hus
<g/>
:	:	kIx,	:
Alia	Alia	k1gMnSc1	Alia
minora	minor	k1gMnSc2	minor
Kostnického	kostnický	k2eAgInSc2d1	kostnický
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
historická	historický	k2eAgFnSc1d1	historická
lekce	lekce	k1gFnSc1	lekce
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
DNZ	DNZ	kA	DNZ
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Hezoučká	hezoučký	k2eAgFnSc1d1	hezoučká
má	mít	k5eAaImIp3nS	mít
milá	milá	k1gFnSc1	milá
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
lidová	lidový	k2eAgFnSc1d1	lidová
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgInSc1d1	mladý
svět	svět	k1gInSc1	svět
4	[number]	k4	4
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
,	,	kIx,	,
s.	s.	k?	s.
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
