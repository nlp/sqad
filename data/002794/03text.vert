<s>
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammer	k1gInSc1	Hammer
je	být	k5eAaImIp3nS	být
black	black	k6eAd1	black
metalová	metalový	k2eAgFnSc1d1	metalová
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
demo	demo	k2eAgFnPc2d1	demo
nahrávek	nahrávka	k1gFnPc2	nahrávka
vydala	vydat	k5eAaPmAgFnS	vydat
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Ritual	Ritual	k1gMnSc1	Ritual
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
další	další	k2eAgInSc4d1	další
s	s	k7c7	s
názvem	název	k1gInSc7	název
Jilemnický	jilemnický	k2eAgMnSc1d1	jilemnický
okultista	okultista	k1gMnSc1	okultista
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
undergroundu	underground	k1gInSc2	underground
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
kultovní	kultovní	k2eAgInSc4d1	kultovní
status	status	k1gInSc4	status
dokonce	dokonce	k9	dokonce
i	i	k9	i
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
skladbu	skladba	k1gFnSc4	skladba
Jáma	jáma	k1gFnSc1	jáma
pekel	peklo	k1gNnPc2	peklo
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
např.	např.	kA	např.
plzenští	plzenský	k2eAgMnPc1d1	plzenský
Stíny	stín	k1gInPc7	stín
plamenů	plamen	k1gInPc2	plamen
nebo	nebo	k8xC	nebo
polští	polský	k2eAgMnPc1d1	polský
Behemoth	Behemotha	k1gFnPc2	Behemotha
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Brutal	Brutal	k1gMnSc1	Brutal
Assault	Assault	k1gMnSc1	Assault
zahráli	zahrát	k5eAaPmAgMnP	zahrát
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Big	Big	k1gMnSc7	Big
Bossem	boss	k1gMnSc7	boss
z	z	k7c2	z
Root	Roota	k1gFnPc2	Roota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Indiánská	indiánský	k2eAgFnSc1d1	indiánská
píseň	píseň	k1gFnSc1	píseň
hrůzy	hrůza	k1gFnSc2	hrůza
<g/>
"	"	kIx"	"
vydala	vydat	k5eAaPmAgFnS	vydat
jako	jako	k9	jako
coververzi	coververze	k1gFnSc4	coververze
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
kapela	kapela	k1gFnSc1	kapela
Drudkh	Drudkha	k1gFnPc2	Drudkha
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kompilačním	kompilační	k2eAgNnSc6d1	kompilační
albu	album	k1gNnSc6	album
Eastern	Eastern	k1gMnSc1	Eastern
Frontier	Frontier	k1gMnSc1	Frontier
in	in	k?	in
Flames	Flames	k1gMnSc1	Flames
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hrány	hrát	k5eAaImNgFnP	hrát
také	také	k9	také
další	další	k2eAgFnPc1d1	další
coververze	coververze	k1gFnPc1	coververze
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
ranou	raný	k2eAgFnSc4d1	raná
norskou	norský	k2eAgFnSc4d1	norská
black	black	k6eAd1	black
metalovou	metalový	k2eAgFnSc4d1	metalová
scénu	scéna	k1gFnSc4	scéna
<g/>
;	;	kIx,	;
norský	norský	k2eAgMnSc1d1	norský
hudebník	hudebník	k1gMnSc1	hudebník
Fenriz	Fenriz	k1gMnSc1	Fenriz
z	z	k7c2	z
Darkthrone	Darkthron	k1gInSc5	Darkthron
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
k	k	k7c3	k
první	první	k4xOgFnSc3	první
desce	deska	k1gFnSc3	deska
Ritual	Ritual	k1gInSc1	Ritual
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc2	první
norské	norský	k2eAgFnSc2d1	norská
black	black	k1gInSc4	black
metalové	metalový	k2eAgNnSc4d1	metalové
album	album	k1gNnSc4	album
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
(	(	kIx(	(
<g/>
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammra	k1gFnPc2	Hammra
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Štorm	Štorm	k1gInSc1	Štorm
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
/	/	kIx~	/
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
/	/	kIx~	/
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
/	/	kIx~	/
<g/>
syntezátor	syntezátor	k1gInSc1	syntezátor
Honza	Honza	k1gMnSc1	Honza
Přibyl	Přibyl	k1gMnSc1	Přibyl
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kohout	Kohout	k1gMnSc1	Kohout
(	(	kIx(	(
<g/>
Necrocock	Necrocock	k1gMnSc1	Necrocock
<g/>
)	)	kIx)	)
Silenthell	Silenthell	k1gMnSc1	Silenthell
-	-	kIx~	-
tympány	tympán	k1gInPc7	tympán
Miroslav	Miroslav	k1gMnSc1	Miroslav
Valenta	Valenta	k1gMnSc1	Valenta
Ferenc	Ferenc	k1gMnSc1	Ferenc
Fečo	Fečo	k1gMnSc1	Fečo
Míla	Míla	k1gMnSc1	Míla
Křovina	křovina	k1gFnSc1	křovina
Ulric	Ulrice	k1gFnPc2	Ulrice
For	forum	k1gNnPc2	forum
Bathory	Bathora	k1gFnSc2	Bathora
(	(	kIx(	(
<g/>
Milan	Milan	k1gMnSc1	Milan
Fibiger	Fibiger	k1gMnSc1	Fibiger
<g/>
)	)	kIx)	)
Carles	Carles	k1gMnSc1	Carles
R.	R.	kA	R.
Apron	Apron	k1gMnSc1	Apron
The	The	k1gMnSc1	The
Ritual	Ritual	k1gMnSc1	Ritual
<g />
.	.	kIx.	.
</s>
<s>
Murder	Murder	k1gInSc1	Murder
(	(	kIx(	(
<g/>
demo	demo	k2eAgFnSc1d1	demo
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Finished	Finished	k1gInSc1	Finished
(	(	kIx(	(
<g/>
demo	demo	k2eAgFnSc1d1	demo
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Mass	Massa	k1gFnPc2	Massa
(	(	kIx(	(
<g/>
demo	demo	k2eAgInPc1d1	demo
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
Idol	idol	k1gInSc1	idol
(	(	kIx(	(
<g/>
demo	demo	k2eAgFnSc1d1	demo
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Jilemnický	jilemnický	k2eAgMnSc1d1	jilemnický
okultista	okultista	k1gMnSc1	okultista
(	(	kIx(	(
<g/>
demo	demo	k2eAgMnSc1d1	demo
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Ritual	Ritual	k1gInSc1	Ritual
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Jilemnický	jilemnický	k2eAgMnSc1d1	jilemnický
okultista	okultista	k1gMnSc1	okultista
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Šlágry	šlágr	k1gInPc1	šlágr
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Mantras	Mantrasa	k1gFnPc2	Mantrasa
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Vracejte	vracet	k5eAaImRp2nP	vracet
konve	konev	k1gFnSc2	konev
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Vagus	vagus	k1gInSc1	vagus
Vetus	Vetus	k1gInSc1	Vetus
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Formulæ	Formulæ	k1gFnSc2	Formulæ
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Klavierstück	Klavierstück	k1gMnSc1	Klavierstück
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammra	k1gFnPc2	Hammra
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Ritual	Ritual	k1gMnSc1	Ritual
/	/	kIx~	/
The	The	k1gMnSc1	The
Jilemnice	Jilemnice	k1gFnSc2	Jilemnice
Occultist	Occultist	k1gMnSc1	Occultist
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Demo	demo	k2eAgInSc1d1	demo
Collection	Collection	k1gInSc1	Collection
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Ritual	Ritual	k1gMnSc1	Ritual
Murder	Murder	k1gMnSc1	Murder
/	/	kIx~	/
Live	Live	k1gFnSc1	Live
in	in	k?	in
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Demo	demo	k2eAgInSc1d1	demo
Collection	Collection	k1gInSc1	Collection
#	#	kIx~	#
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Finished	Finished	k1gInSc1	Finished
/	/	kIx~	/
The	The	k1gFnSc1	The
Mass	Massa	k1gFnPc2	Massa
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Demo	demo	k2eAgInSc1d1	demo
Collection	Collection	k1gInSc1	Collection
#	#	kIx~	#
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
Idol	idol	k1gInSc1	idol
/	/	kIx~	/
Jilemnický	jilemnický	k2eAgMnSc1d1	jilemnický
okultista	okultista	k1gMnSc1	okultista
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Demos	Demosa	k1gFnPc2	Demosa
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
18.5	[number]	k4	18.5
<g/>
.1989	.1989	k4	.1989
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammra	k1gFnPc2	Hammra
/	/	kIx~	/
Blackosh	Blackosh	k1gInSc1	Blackosh
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
split	split	k1gInSc1	split
s	s	k7c7	s
Blackosh	Blackosh	k1gMnSc1	Blackosh
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammra	k1gFnPc2	Hammra
/	/	kIx~	/
Blackosh	Blackosh	k1gInSc1	Blackosh
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
split	split	k1gInSc1	split
s	s	k7c7	s
Blackosh	Blackosh	k1gMnSc1	Blackosh
</s>
