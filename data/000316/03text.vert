<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Donau	donau	k1gNnSc1	donau
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
slovinsky	slovinsky	k6eAd1	slovinsky
Donava	Donava	k1gFnSc1	Donava
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Duna	duna	k1gFnSc1	duna
<g/>
,	,	kIx,	,
chorvatsky	chorvatsky	k6eAd1	chorvatsky
Dunav	Dunav	k1gInSc1	Dunav
<g/>
,	,	kIx,	,
srbsky	srbsky	k6eAd1	srbsky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
bulharsky	bulharsky	k6eAd1	bulharsky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
Dunărea	Dunăre	k2eAgFnSc1d1	Dunăre
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Д	Д	k?	Д
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Danube	danub	k1gInSc5	danub
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Volze	Volha	k1gFnSc6	Volha
druhá	druhý	k4xOgFnSc1	druhý
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
nebo	nebo	k8xC	nebo
tvoří	tvořit	k5eAaImIp3nS	tvořit
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
2811	[number]	k4	2811
km	km	kA	km
(	(	kIx(	(
<g/>
s	s	k7c7	s
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
zdrojnicí	zdrojnice	k1gFnSc7	zdrojnice
Breg	Breg	k1gMnSc1	Breg
2857	[number]	k4	2857
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
801	[number]	k4	801
463	[number]	k4	463
km2	km2	k4	km2
(	(	kIx(	(
<g/>
7,9	[number]	k4	7,9
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
řekami	řeka	k1gFnPc7	řeka
několik	několik	k4yIc4	několik
prvenství	prvenství	k1gNnPc2	prvenství
<g/>
:	:	kIx,	:
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
nejvíce	hodně	k6eAd3	hodně
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
a	a	k8xC	a
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
nejvíce	nejvíce	k6eAd1	nejvíce
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
povodí	povodí	k1gNnSc6	povodí
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Dunaj	Dunaj	k1gInSc1	Dunaj
(	(	kIx(	(
<g/>
a	a	k8xC	a
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
tvary	tvar	k1gInPc1	tvar
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
jména	jméno	k1gNnSc2	jméno
římského	římský	k2eAgMnSc2d1	římský
boha	bůh	k1gMnSc2	bůh
Danubia	Danubius	k1gMnSc2	Danubius
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
keltského	keltský	k2eAgMnSc4d1	keltský
<g/>
,	,	kIx,	,
skytského	skytský	k2eAgMnSc4d1	skytský
nebo	nebo	k8xC	nebo
sarmatského	sarmatský	k2eAgMnSc4d1	sarmatský
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
také	také	k9	také
jako	jako	k8xS	jako
Tonach	Tonach	k1gMnSc1	Tonach
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Donaw	Donaw	k1gFnSc1	Donaw
<g/>
.	.	kIx.	.
</s>
<s>
Starým	starý	k2eAgNnSc7d1	staré
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
<g/>
)	)	kIx)	)
dolního	dolní	k2eAgInSc2d1	dolní
Dunaje	Dunaj	k1gInSc2	Dunaj
bylo	být	k5eAaImAgNnS	být
latinské	latinský	k2eAgInPc4d1	latinský
Hister	Hister	k1gInSc4	Hister
<g/>
,	,	kIx,	,
Ister	Ister	k1gInSc4	Ister
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc4d1	pocházející
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
Ἴ	Ἴ	k?	Ἴ
(	(	kIx(	(
<g/>
Ístros	Ístrosa	k1gFnPc2	Ístrosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řeckou	řecký	k2eAgFnSc4d1	řecká
podobu	podoba	k1gFnSc4	podoba
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Hésiodos	Hésiodos	k1gMnSc1	Hésiodos
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Theogonii	theogonie	k1gFnSc6	theogonie
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
jím	on	k3xPp3gInSc7	on
syna	syn	k1gMnSc2	syn
Ókeána	Ókeán	k1gMnSc2	Ókeán
a	a	k8xC	a
Téthydy	Téthyda	k1gMnSc2	Téthyda
(	(	kIx(	(
<g/>
Theogonia	Theogonium	k1gNnSc2	Theogonium
339	[number]	k4	339
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
Dunaje	Dunaj	k1gInSc2	Dunaj
pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
Schwarzwaldu	Schwarzwald	k1gInSc6	Schwarzwald
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
veletok	veletok	k1gInSc1	veletok
ústí	ústit	k5eAaImIp3nS	ústit
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
deltou	delta	k1gFnSc7	delta
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
protéká	protékat	k5eAaImIp3nS	protékat
geologicky	geologicky	k6eAd1	geologicky
a	a	k8xC	a
geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
velmi	velmi	k6eAd1	velmi
pestrým	pestrý	k2eAgNnSc7d1	pestré
a	a	k8xC	a
složitým	složitý	k2eAgNnSc7d1	složité
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
Českým	český	k2eAgInSc7d1	český
masivem	masiv	k1gInSc7	masiv
a	a	k8xC	a
Alpami	Alpy	k1gFnPc7	Alpy
<g/>
,	,	kIx,	,
Karpatskou	karpatský	k2eAgFnSc7d1	Karpatská
kotlinou	kotlina	k1gFnSc7	kotlina
<g/>
,	,	kIx,	,
Valašskou	valašský	k2eAgFnSc7d1	Valašská
nížinou	nížina	k1gFnSc7	nížina
a	a	k8xC	a
několika	několik	k4yIc7	několik
velkými	velký	k2eAgInPc7d1	velký
průlomy	průlom	k1gInPc7	průlom
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
znali	znát	k5eAaImAgMnP	znát
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
tvořila	tvořit	k5eAaImAgFnS	tvořit
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
jejich	jejich	k3xOp3gNnSc2	jejich
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
latinským	latinský	k2eAgInSc7d1	latinský
názvem	název	k1gInSc7	název
Danubius	Danubius	k1gInSc1	Danubius
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
vyslal	vyslat	k5eAaPmAgMnS	vyslat
římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
hledat	hledat	k5eAaImF	hledat
pramen	pramen	k1gInSc1	pramen
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
narazili	narazit	k5eAaPmAgMnP	narazit
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Schwarzwaldu	Schwarzwald	k1gInSc2	Schwarzwald
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Donaueschingen	Donaueschingen	k1gInSc1	Donaueschingen
<g/>
,	,	kIx,	,
na	na	k7c4	na
silný	silný	k2eAgInSc4d1	silný
minerální	minerální	k2eAgInSc4d1	minerální
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
sice	sice	k8xC	sice
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gInSc1	její
počátek	počátek	k1gInSc1	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Vojákům	voják	k1gMnPc3	voják
se	se	k3xPyFc4	se
nechtělo	chtít	k5eNaImAgNnS	chtít
prodírat	prodírat	k5eAaImF	prodírat
černými	černý	k2eAgInPc7d1	černý
lesy	les	k1gInPc7	les
ke	k	k7c3	k
skutečnému	skutečný	k2eAgInSc3d1	skutečný
prameni	pramen	k1gInSc3	pramen
a	a	k8xC	a
označili	označit	k5eAaPmAgMnP	označit
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
za	za	k7c4	za
pramen	pramen	k1gInSc4	pramen
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
nese	nést	k5eAaImIp3nS	nést
řeka	řeka	k1gFnSc1	řeka
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
až	až	k9	až
od	od	k7c2	od
města	město	k1gNnSc2	město
Donaueschingen	Donaueschingen	k1gInSc1	Donaueschingen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
zřídlo	zřídlo	k1gNnSc1	zřídlo
obezděné	obezděný	k2eAgNnSc1d1	obezděné
jako	jako	k8xC	jako
její	její	k3xOp3gInSc1	její
symbolický	symbolický	k2eAgInSc1d1	symbolický
pramen	pramen	k1gInSc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
vzniká	vznikat	k5eAaImIp3nS	vznikat
Dunaj	Dunaj	k1gInSc1	Dunaj
především	především	k9	především
soutokem	soutok	k1gInSc7	soutok
říček	říčka	k1gFnPc2	říčka
Breg	Brega	k1gFnPc2	Brega
a	a	k8xC	a
Brigach	Brigacha	k1gFnPc2	Brigacha
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
672	[number]	k4	672
m.	m.	k?	m.
Delší	dlouhý	k2eAgFnSc7d2	delší
zdrojnicí	zdrojnice	k1gFnSc7	zdrojnice
je	být	k5eAaImIp3nS	být
Breg	Breg	k1gInSc1	Breg
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vodnatější	vodnatý	k2eAgInSc1d2	vodnatější
je	být	k5eAaImIp3nS	být
Brigach	Brigach	k1gInSc1	Brigach
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
Bregu	Breg	k1gInSc2	Breg
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
označen	označit	k5eAaPmNgInS	označit
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
Bregquelle	Bregquelle	k1gFnSc1	Bregquelle
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pramen	pramen	k1gInSc1	pramen
Bregu	Breg	k1gInSc2	Breg
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
informační	informační	k2eAgFnPc4d1	informační
tabulky	tabulka	k1gFnPc4	tabulka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
ho	on	k3xPp3gMnSc4	on
nazývají	nazývat	k5eAaImIp3nP	nazývat
Donauquelle	Donauquelle	k1gFnPc1	Donauquelle
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pramen	pramen	k1gInSc1	pramen
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
turistickou	turistický	k2eAgFnSc4d1	turistická
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouhých	pouhý	k2eAgInPc2d1	pouhý
100	[number]	k4	100
m	m	kA	m
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
evropského	evropský	k2eAgNnSc2d1	Evropské
rozvodí	rozvodí	k1gNnSc2	rozvodí
mezi	mezi	k7c7	mezi
Dunajem	Dunaj	k1gInSc7	Dunaj
a	a	k8xC	a
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1078	[number]	k4	1078
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
třikrát	třikrát	k6eAd1	třikrát
blíže	blízce	k6eAd2	blízce
Severnímu	severní	k2eAgNnSc3d1	severní
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
blíže	blízce	k6eAd2	blízce
Středozemnímu	středozemní	k2eAgNnSc3d1	středozemní
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
než	než	k8xS	než
Černému	černý	k2eAgNnSc3d1	černé
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgInSc2	jenž
jeho	jeho	k3xOp3gFnSc1	jeho
vody	voda	k1gFnPc1	voda
odtékají	odtékat	k5eAaImIp3nP	odtékat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Propadání	propadání	k1gNnSc2	propadání
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
za	za	k7c7	za
soutokem	soutok	k1gInSc7	soutok
Brigachu	Brigach	k1gInSc2	Brigach
a	a	k8xC	a
Bregu	Breg	k1gInSc2	Breg
prochází	procházet	k5eAaImIp3nS	procházet
Dunaj	Dunaj	k1gInSc1	Dunaj
jižní	jižní	k2eAgFnSc2d1	jižní
částí	část	k1gFnSc7	část
krasového	krasový	k2eAgInSc2d1	krasový
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Švábská	švábský	k2eAgFnSc1d1	Švábská
Alba	alba	k1gFnSc1	alba
<g/>
,	,	kIx,	,
nazývaného	nazývaný	k2eAgMnSc2d1	nazývaný
také	také	k9	také
Švábská	švábský	k2eAgFnSc1d1	Švábská
Jura	jura	k1gFnSc1	jura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
obcí	obec	k1gFnSc7	obec
Immendingen	Immendingen	k1gInSc1	Immendingen
a	a	k8xC	a
městem	město	k1gNnSc7	město
Fridingen	Fridingen	k1gInSc1	Fridingen
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
propadání	propadání	k1gNnSc1	propadání
Dunaje	Dunaj	k1gInSc2	Dunaj
(	(	kIx(	(
<g/>
Donauversinkung	Donauversinkung	k1gInSc1	Donauversinkung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
vod	voda	k1gFnPc2	voda
Dunaje	Dunaj	k1gInSc2	Dunaj
zde	zde	k6eAd1	zde
prosakuje	prosakovat	k5eAaImIp3nS	prosakovat
do	do	k7c2	do
podzemních	podzemní	k2eAgInPc2d1	podzemní
jeskynních	jeskynní	k2eAgInPc2d1	jeskynní
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
asi	asi	k9	asi
12	[number]	k4	12
km	km	kA	km
jižněji	jižně	k6eAd2	jižně
u	u	k7c2	u
města	město	k1gNnSc2	město
Aach	Aach	k1gInSc1	Aach
jako	jako	k8xC	jako
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
řeka	řeka	k1gFnSc1	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Aach	Aacha	k1gFnPc2	Aacha
pak	pak	k6eAd1	pak
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Bodamského	bodamský	k2eAgNnSc2d1	Bodamské
jezera	jezero	k1gNnSc2	jezero
na	na	k7c4	na
Rýnu	rýna	k1gFnSc4	rýna
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
nejhořejšího	horní	k2eAgInSc2d3	nejhořejší
Dunaje	Dunaj	k1gInSc2	Dunaj
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
oblastí	oblast	k1gFnSc7	oblast
tak	tak	k6eAd1	tak
vlastně	vlastně	k9	vlastně
patří	patřit	k5eAaImIp3nS	patřit
současně	současně	k6eAd1	současně
do	do	k7c2	do
úmoří	úmoří	k1gNnSc2	úmoří
Severního	severní	k2eAgNnSc2d1	severní
i	i	k8xC	i
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
Dunaj	Dunaj	k1gInSc4	Dunaj
náhorní	náhorní	k2eAgFnSc7d1	náhorní
řekou	řeka	k1gFnSc7	řeka
postupně	postupně	k6eAd1	postupně
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
ve	v	k7c4	v
veletok	veletok	k1gInSc4	veletok
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
hercynikem	hercynikum	k1gNnSc7	hercynikum
(	(	kIx(	(
<g/>
Jihozápadoněmecká	Jihozápadoněmecký	k2eAgFnSc1d1	Jihozápadoněmecký
stupňovina	stupňovina	k1gFnSc1	stupňovina
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
masiv	masiv	k1gInSc1	masiv
<g/>
)	)	kIx)	)
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
alpinikem	alpinik	k1gInSc7	alpinik
(	(	kIx(	(
<g/>
Alpenvorland	Alpenvorland	k1gInSc1	Alpenvorland
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
břeh	břeh	k1gInSc1	břeh
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
a	a	k8xC	a
Dunaj	Dunaj	k1gInSc1	Dunaj
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
přijímá	přijímat	k5eAaImIp3nS	přijímat
větší	veliký	k2eAgInPc4d2	veliký
přítoky	přítok	k1gInPc4	přítok
<g/>
:	:	kIx,	:
Iller	Iller	k1gMnSc1	Iller
<g/>
,	,	kIx,	,
Lech	Lech	k1gMnSc1	Lech
<g/>
,	,	kIx,	,
Isar	Isar	k1gMnSc1	Isar
<g/>
,	,	kIx,	,
Vils	Vils	k1gInSc1	Vils
a	a	k8xC	a
zejména	zejména	k9	zejména
Inn	Inn	k1gFnSc1	Inn
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gInPc3	on
získává	získávat	k5eAaImIp3nS	získávat
Dunaj	Dunaj	k1gInSc1	Dunaj
průtokové	průtokový	k2eAgFnSc2d1	průtoková
charakteristiky	charakteristika	k1gFnSc2	charakteristika
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgInSc4d1	vodní
režim	režim	k1gInSc4	režim
<g/>
)	)	kIx)	)
alpských	alpský	k2eAgFnPc2d1	alpská
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jí	on	k3xPp3gFnSc2	on
sám	sám	k3xTgMnSc1	sám
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Rakouska	Rakousko	k1gNnSc2	Rakousko
se	se	k3xPyFc4	se
Dunaj	Dunaj	k1gInSc1	Dunaj
zařezává	zařezávat	k5eAaImIp3nS	zařezávat
do	do	k7c2	do
jižního	jižní	k2eAgInSc2d1	jižní
okraje	okraj	k1gInSc2	okraj
Českého	český	k2eAgInSc2d1	český
masivu	masiv	k1gInSc2	masiv
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
malebnými	malebný	k2eAgInPc7d1	malebný
kaňony	kaňon	k1gInPc7	kaňon
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgFnSc4d3	veliký
je	být	k5eAaImIp3nS	být
Wachau	Wachaa	k1gFnSc4	Wachaa
<g/>
.	.	kIx.	.
</s>
<s>
Zprava	zprava	k6eAd1	zprava
se	se	k3xPyFc4	se
vlévají	vlévat	k5eAaImIp3nP	vlévat
Traun	Traun	k1gInSc1	Traun
<g/>
,	,	kIx,	,
Enže	Enže	k1gInSc1	Enže
<g/>
,	,	kIx,	,
Ybbs	Ybbs	k1gInSc1	Ybbs
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
Aist	Aist	k2eAgMnSc1d1	Aist
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Kremží	Kremže	k1gFnSc7	Kremže
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Dunaj	Dunaj	k1gInSc1	Dunaj
do	do	k7c2	do
Tullnské	Tullnský	k2eAgFnSc2d1	Tullnský
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
zleva	zleva	k6eAd1	zleva
Kamp	kamp	k1gInSc4	kamp
a	a	k8xC	a
zprava	zprava	k6eAd1	zprava
Traisen	Traisen	k2eAgMnSc1d1	Traisen
<g/>
,	,	kIx,	,
a	a	k8xC	a
Vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
(	(	kIx(	(
<g/>
či	či	k8xC	či
Korneuburskou	Korneuburský	k2eAgFnSc7d1	Korneuburský
<g/>
)	)	kIx)	)
branou	brána	k1gFnSc7	brána
se	se	k3xPyFc4	se
prolamuje	prolamovat	k5eAaImIp3nS	prolamovat
do	do	k7c2	do
Panonie	Panonie	k1gFnSc2	Panonie
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
koryta	koryto	k1gNnSc2	koryto
nad	nad	k7c4	nad
Ulmem	Ulmem	k1gInSc4	Ulmem
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
20	[number]	k4	20
až	až	k9	až
100	[number]	k4	100
m	m	kA	m
a	a	k8xC	a
níže	nízce	k6eAd2	nízce
roste	růst	k5eAaImIp3nS	růst
na	na	k7c4	na
350	[number]	k4	350
m	m	kA	m
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
2,8	[number]	k4	2,8
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Místy	místy	k6eAd1	místy
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc1	koryto
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
napřimovacími	napřimovací	k2eAgFnPc7d1	napřimovací
a	a	k8xC	a
protipovodňovými	protipovodňový	k2eAgFnPc7d1	protipovodňová
hrázemi	hráz	k1gFnPc7	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
tok	tok	k1gInSc1	tok
začíná	začínat	k5eAaImIp3nS	začínat
nad	nad	k7c7	nad
Vídní	Vídeň	k1gFnSc7	Vídeň
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
soutěskou	soutěska	k1gFnSc7	soutěska
Železná	železný	k2eAgNnPc1d1	železné
vrata	vrata	k1gNnPc1	vrata
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
zde	zde	k6eAd1	zde
protéká	protékat	k5eAaImIp3nS	protékat
Panonskou	panonský	k2eAgFnSc7d1	Panonská
pánví	pánev	k1gFnSc7	pánev
neboli	neboli	k8xC	neboli
Karpatskou	karpatský	k2eAgFnSc7d1	Karpatská
kotlinou	kotlina	k1gFnSc7	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgNnSc1d1	říční
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
5	[number]	k4	5
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
a	a	k8xC	a
často	často	k6eAd1	často
zaplavované	zaplavovaný	k2eAgNnSc1d1	zaplavované
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
členité	členitý	k2eAgNnSc1d1	členité
<g/>
,	,	kIx,	,
rozvětvené	rozvětvený	k2eAgNnSc1d1	rozvětvené
a	a	k8xC	a
rychlost	rychlost	k1gFnSc1	rychlost
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
0,3	[number]	k4	0,3
do	do	k7c2	do
1,1	[number]	k4	1,1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Dunaj	Dunaj	k1gInSc1	Dunaj
překonává	překonávat	k5eAaImIp3nS	překonávat
příčná	příčný	k2eAgNnPc4d1	příčné
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
tok	tok	k1gInSc1	tok
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
150	[number]	k4	150
m	m	kA	m
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc6	údolí
na	na	k7c4	na
0,6	[number]	k4	0,6
až	až	k9	až
1,5	[number]	k4	1,5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
(	(	kIx(	(
<g/>
na	na	k7c4	na
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
(	(	kIx(	(
<g/>
na	na	k7c4	na
2,2	[number]	k4	2,2
až	až	k9	až
4,7	[number]	k4	4,7
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
regulačních	regulační	k2eAgFnPc2d1	regulační
prací	práce	k1gFnPc2	práce
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
koryto	koryto	k1gNnSc1	koryto
mnohde	mnohde	k6eAd1	mnohde
napřímeno	napřímit	k5eAaPmNgNnS	napřímit
a	a	k8xC	a
kolem	kolem	k7c2	kolem
dunajské	dunajský	k2eAgFnSc2d1	Dunajská
nivy	niva	k1gFnSc2	niva
s	s	k7c7	s
mnoha	mnoho	k4c2	mnoho
rameny	rameno	k1gNnPc7	rameno
<g/>
,	,	kIx,	,
meandry	meandr	k1gInPc7	meandr
a	a	k8xC	a
lužními	lužní	k2eAgInPc7d1	lužní
lesy	les	k1gInPc7	les
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc1d1	postavena
ochranné	ochranný	k2eAgFnPc1d1	ochranná
hráze	hráz	k1gFnPc1	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Vídní	Vídeň	k1gFnSc7	Vídeň
protéká	protékat	k5eAaImIp3nS	protékat
Dunaj	Dunaj	k1gInSc4	Dunaj
Vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
pánví	pánev	k1gFnSc7	pánev
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Moravského	moravský	k2eAgNnSc2d1	Moravské
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
přirozený	přirozený	k2eAgInSc1d1	přirozený
tok	tok	k1gInSc1	tok
chráněn	chránit	k5eAaImNgInS	chránit
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Donau-Auen	Donau-Auen	k1gInSc1	Donau-Auen
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
přijme	přijmout	k5eAaPmIp3nS	přijmout
svůj	svůj	k3xOyFgInSc4	svůj
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgInSc4d3	veliký
levý	levý	k2eAgInSc4d1	levý
přítok	přítok	k1gInSc4	přítok
<g/>
,	,	kIx,	,
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Devínské	Devínský	k2eAgFnSc2d1	Devínská
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
překonává	překonávat	k5eAaImIp3nS	překonávat
jižní	jižní	k2eAgInSc1d1	jižní
okraj	okraj	k1gInSc1	okraj
Malých	Malých	k2eAgInPc2d1	Malých
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
slovenská	slovenský	k2eAgFnSc1d1	slovenská
metropole	metropole	k1gFnSc1	metropole
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
Malá	malý	k2eAgFnSc1d1	malá
uherská	uherský	k2eAgFnSc1d1	uherská
nížina	nížina	k1gFnSc1	nížina
(	(	kIx(	(
<g/>
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
straně	strana	k1gFnSc6	strana
Podunajská	podunajský	k2eAgFnSc1d1	Podunajská
nížina	nížina	k1gFnSc1	nížina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Dunaj	Dunaj	k1gInSc1	Dunaj
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Slovenskem	Slovensko	k1gNnSc7	Slovensko
a	a	k8xC	a
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
rozlévá	rozlévat	k5eAaImIp3nS	rozlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
ploché	plochý	k2eAgFnSc2d1	plochá
vnitrozemské	vnitrozemský	k2eAgFnSc2d1	vnitrozemská
delty	delta	k1gFnSc2	delta
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
vedle	vedle	k7c2	vedle
hlavního	hlavní	k2eAgNnSc2d1	hlavní
toku	tok	k1gInSc6	tok
rameny	rameno	k1gNnPc7	rameno
Malý	malý	k2eAgInSc4d1	malý
Dunaj	Dunaj	k1gInSc4	Dunaj
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Mošonský	Mošonský	k2eAgInSc1d1	Mošonský
Dunaj	Dunaj	k1gInSc1	Dunaj
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
obtékají	obtékat	k5eAaImIp3nP	obtékat
velké	velký	k2eAgInPc1d1	velký
říční	říční	k2eAgInPc1d1	říční
ostrovy	ostrov	k1gInPc1	ostrov
Žitný	žitný	k2eAgInSc1d1	žitný
a	a	k8xC	a
Szigetköz	Szigetköz	k1gInSc1	Szigetköz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Mošonského	Mošonský	k2eAgInSc2d1	Mošonský
Dunaje	Dunaj	k1gInSc2	Dunaj
se	se	k3xPyFc4	se
zprava	zprava	k6eAd1	zprava
vlévají	vlévat	k5eAaImIp3nP	vlévat
Litava	Litava	k1gFnSc1	Litava
a	a	k8xC	a
Rába	Rába	k1gFnSc1	Rába
<g/>
.	.	kIx.	.
</s>
<s>
Delta	delta	k1gFnSc1	delta
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
Komárně	Komárno	k1gNnSc6	Komárno
opětovným	opětovný	k2eAgNnSc7d1	opětovné
sjednocením	sjednocení	k1gNnSc7	sjednocení
dunajských	dunajský	k2eAgNnPc2d1	Dunajské
ramen	rameno	k1gNnPc2	rameno
<g/>
,	,	kIx,	,
obohacených	obohacený	k2eAgFnPc2d1	obohacená
zleva	zleva	k6eAd1	zleva
o	o	k7c4	o
přítok	přítok	k1gInSc4	přítok
Váhu	Váh	k1gInSc2	Váh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
Hronu	Hron	k1gInSc2	Hron
a	a	k8xC	a
Ipľu	Ipľus	k1gInSc2	Ipľus
(	(	kIx(	(
<g/>
oba	dva	k4xCgInPc1	dva
zleva	zleva	k6eAd1	zleva
<g/>
)	)	kIx)	)
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Dunaj	Dunaj	k1gInSc1	Dunaj
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
velké	velký	k2eAgFnSc2d1	velká
soutěsky	soutěska	k1gFnSc2	soutěska
<g/>
,	,	kIx,	,
na	na	k7c6	na
přechodu	přechod	k1gInSc6	přechod
Maďarského	maďarský	k2eAgNnSc2d1	Maďarské
středohoří	středohoří	k1gNnSc2	středohoří
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
Dunajské	dunajský	k2eAgNnSc1d1	Dunajské
ohbí	ohbí	k1gNnSc1	ohbí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Dunaj	Dunaj	k1gInSc1	Dunaj
zde	zde	k6eAd1	zde
svůj	svůj	k3xOyFgInSc4	svůj
dosud	dosud	k6eAd1	dosud
zhruba	zhruba	k6eAd1	zhruba
východní	východní	k2eAgInSc1d1	východní
směr	směr	k1gInSc1	směr
ostře	ostro	k6eAd1	ostro
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
směr	směr	k1gInSc4	směr
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
protéká	protékat	k5eAaImIp3nS	protékat
Budapeští	Budapešť	k1gFnSc7	Budapešť
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
velký	velký	k2eAgInSc4d1	velký
ostrov	ostrov	k1gInSc4	ostrov
Csepel	Csepela	k1gFnPc2	Csepela
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
po	po	k7c6	po
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Velké	velký	k2eAgFnSc2d1	velká
uherské	uherský	k2eAgFnSc2d1	uherská
nížiny	nížina	k1gFnSc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
žádné	žádný	k3yNgInPc1	žádný
velké	velký	k2eAgInPc1d1	velký
přítoky	přítok	k1gInPc1	přítok
a	a	k8xC	a
vlivem	vlivem	k7c2	vlivem
suchého	suchý	k2eAgNnSc2d1	suché
stepního	stepní	k2eAgNnSc2d1	stepní
klimatu	klima	k1gNnSc2	klima
vodu	voda	k1gFnSc4	voda
spíše	spíše	k9	spíše
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
Karpatské	karpatský	k2eAgFnSc2d1	Karpatská
kotliny	kotlina	k1gFnSc2	kotlina
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opět	opět	k6eAd1	opět
mění	měnit	k5eAaImIp3nS	měnit
směr	směr	k1gInSc4	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
vlévá	vlévat	k5eAaImIp3nS	vlévat
několik	několik	k4yIc4	několik
velkých	velký	k2eAgFnPc2d1	velká
řek	řeka	k1gFnPc2	řeka
<g/>
:	:	kIx,	:
Dráva	Dráva	k1gFnSc1	Dráva
<g/>
,	,	kIx,	,
Tisa	Tisa	k1gFnSc1	Tisa
(	(	kIx(	(
<g/>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
přítok	přítok	k1gInSc1	přítok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srbská	srbský	k2eAgFnSc1d1	Srbská
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
vůbec	vůbec	k9	vůbec
nejmohutnější	mohutný	k2eAgInSc1d3	nejmohutnější
dunajský	dunajský	k2eAgInSc1d1	dunajský
přítok	přítok	k1gInSc1	přítok
Sáva	Sáva	k1gFnSc1	Sáva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
vede	vést	k5eAaImIp3nS	vést
Dunaj	Dunaj	k1gInSc1	Dunaj
největším	veliký	k2eAgMnPc3d3	veliký
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
průlomových	průlomový	k2eAgNnPc2d1	průlomové
údolí	údolí	k1gNnPc2	údolí
<g/>
,	,	kIx,	,
Železnými	železný	k2eAgInPc7d1	železný
vraty	vrat	k1gInPc7	vrat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původně	původně	k6eAd1	původně
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
peřejnatý	peřejnatý	k2eAgInSc1d1	peřejnatý
tok	tok	k1gInSc1	tok
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
vzedmut	vzedmout	k5eAaPmNgInS	vzedmout
a	a	k8xC	a
zastaven	zastavit	k5eAaPmNgInS	zastavit
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
toku	tok	k1gInSc2	tok
v	v	k7c6	v
nejužším	úzký	k2eAgNnSc6d3	nejužší
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc3d1	velká
Kazanské	Kazanský	k2eAgFnSc3d1	Kazanská
soutěsce	soutěska	k1gFnSc3	soutěska
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
před	před	k7c7	před
regulací	regulace	k1gFnSc7	regulace
přesahovala	přesahovat	k5eAaImAgFnS	přesahovat
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
Železných	železný	k2eAgNnPc2d1	železné
vrat	vrata	k1gNnPc2	vrata
Dunaj	Dunaj	k1gInSc1	Dunaj
teče	téct	k5eAaImIp3nS	téct
jako	jako	k9	jako
nížinná	nížinný	k2eAgFnSc1d1	nížinná
řeka	řeka	k1gFnSc1	řeka
Dolnodunajskou	Dolnodunajský	k2eAgFnSc7d1	Dolnodunajská
rovinou	rovina	k1gFnSc7	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Zaplavované	zaplavovaný	k2eAgNnSc1d1	zaplavované
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
7	[number]	k4	7
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
rozvětvuje	rozvětvovat	k5eAaImIp3nS	rozvětvovat
na	na	k7c4	na
mnohá	mnohý	k2eAgNnPc4d1	mnohé
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
průtoky	průtok	k1gInPc4	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
toku	tok	k1gInSc2	tok
místy	místy	k6eAd1	místy
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
km	km	kA	km
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
m	m	kA	m
a	a	k8xC	a
rychlost	rychlost	k1gFnSc1	rychlost
toku	tok	k1gInSc2	tok
0,5	[number]	k4	0,5
až	až	k9	až
1	[number]	k4	1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Na	na	k7c4	na
skoro	skoro	k6eAd1	skoro
500	[number]	k4	500
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
a	a	k8xC	a
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
překlenují	překlenovat	k5eAaImIp3nP	překlenovat
Dunaj	Dunaj	k1gInSc4	Dunaj
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
mosty	most	k1gInPc4	most
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc1d1	jiné
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
trajektem	trajekt	k1gInSc7	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
Přítoky	přítok	k1gInPc1	přítok
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
Dunaji	Dunaj	k1gInSc3	Dunaj
řádově	řádově	k6eAd1	řádově
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nejsou	být	k5eNaImIp3nP	být
bezvýznamné	bezvýznamný	k2eAgFnPc1d1	bezvýznamná
<g/>
:	:	kIx,	:
zleva	zleva	k6eAd1	zleva
Jiu	Jiu	k1gMnSc1	Jiu
<g/>
,	,	kIx,	,
Olt	Olt	k1gMnSc1	Olt
<g/>
,	,	kIx,	,
Argeș	Argeș	k1gMnSc1	Argeș
<g/>
,	,	kIx,	,
Siret	Siret	k1gMnSc1	Siret
<g/>
,	,	kIx,	,
Prut	prut	k1gInSc1	prut
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
Iskar	Iskar	k1gMnSc1	Iskar
<g/>
,	,	kIx,	,
Osam	Osam	k1gMnSc1	Osam
<g/>
,	,	kIx,	,
Jantra	Jantra	k1gFnSc1	Jantra
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
limanová	limanový	k2eAgNnPc4d1	limanový
ústí	ústí	k1gNnPc4	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
před	před	k7c7	před
černomořským	černomořský	k2eAgNnSc7d1	černomořské
pobřežím	pobřeží	k1gNnSc7	pobřeží
Dunaj	Dunaj	k1gInSc1	Dunaj
ještě	ještě	k9	ještě
uhýbá	uhýbat	k5eAaImIp3nS	uhýbat
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
Galaț	Galaț	k1gFnSc2	Galaț
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgNnSc2d1	poslední
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
na	na	k7c6	na
toku	tok	k1gInSc6	tok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
obrací	obracet	k5eAaImIp3nS	obracet
definitivně	definitivně	k6eAd1	definitivně
na	na	k7c4	na
východ	východ	k1gInSc4	východ
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
před	před	k7c7	před
městem	město	k1gNnSc7	město
Tulcea	Tulce	k1gInSc2	Tulce
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
jeho	jeho	k3xOp3gNnSc1	jeho
deltové	deltový	k2eAgNnSc1d1	deltové
ústí	ústí	k1gNnSc1	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Delta	delta	k1gNnSc2	delta
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deltě	delta	k1gFnSc6	delta
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
3500	[number]	k4	3500
km2	km2	k4	km2
se	se	k3xPyFc4	se
Dunaj	Dunaj	k1gInSc1	Dunaj
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
hlavní	hlavní	k2eAgNnPc4d1	hlavní
ramena	rameno	k1gNnPc4	rameno
<g/>
:	:	kIx,	:
od	od	k7c2	od
severu	sever	k1gInSc2	sever
Kilijské	Kilijský	k2eAgNnSc1d1	Kilijský
(	(	kIx(	(
<g/>
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
a	a	k8xC	a
nejmohutnější	mohutný	k2eAgFnSc1d3	nejmohutnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sulinské	Sulinský	k2eAgNnSc4d1	Sulinský
(	(	kIx(	(
<g/>
nejpřímější	přímý	k2eAgNnSc4d3	nejpřímější
<g/>
)	)	kIx)	)
a	a	k8xC	a
Svatojiřské	svatojiřský	k2eAgFnPc1d1	Svatojiřská
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
delty	delta	k1gFnSc2	delta
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc7d1	přírodní
lokalitou	lokalita	k1gFnSc7	lokalita
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
Dunaj	Dunaj	k1gInSc1	Dunaj
vlévá	vlévat	k5eAaImIp3nS	vlévat
jako	jako	k9	jako
jeho	jeho	k3xOp3gInSc1	jeho
největší	veliký	k2eAgInSc1d3	veliký
přítok	přítok	k1gInSc1	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
přijímá	přijímat	k5eAaImIp3nS	přijímat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
přítoků	přítok	k1gInPc2	přítok
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
na	na	k7c4	na
34	[number]	k4	34
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
má	mít	k5eAaImIp3nS	mít
složitý	složitý	k2eAgInSc4d1	složitý
vodní	vodní	k2eAgInSc4d1	vodní
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
jsou	být	k5eAaImIp3nP	být
výrazné	výrazný	k2eAgFnPc4d1	výrazná
tři	tři	k4xCgFnPc4	tři
fáze	fáze	k1gFnPc4	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nárazovým	nárazový	k2eAgFnPc3d1	nárazová
povodním	povodeň	k1gFnPc3	povodeň
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
podzimu	podzim	k1gInSc2	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
začíná	začínat	k5eAaImIp3nS	začínat
stoupat	stoupat	k5eAaImF	stoupat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k8xS	až
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
vysoká	vysoká	k1gFnSc1	vysoká
do	do	k7c2	do
května	květen	k1gInSc2	květen
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
až	až	k8xS	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
vzestup	vzestup	k1gInSc1	vzestup
hladiny	hladina	k1gFnSc2	hladina
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
táním	tání	k1gNnSc7	tání
sněhu	sníh	k1gInSc2	sníh
v	v	k7c6	v
rovinách	rovina	k1gFnPc6	rovina
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
smíšená	smíšený	k2eAgFnSc1d1	smíšená
dešti	dešť	k1gInPc7	dešť
a	a	k8xC	a
táním	tání	k1gNnSc7	tání
sněhu	sníh	k1gInSc2	sníh
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Nerovnoměrnost	nerovnoměrnost	k1gFnSc1	nerovnoměrnost
tání	tání	k1gNnSc2	tání
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
povodí	povodí	k1gNnSc2	povodí
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
prudké	prudký	k2eAgNnSc4d1	prudké
kolísání	kolísání	k1gNnSc4	kolísání
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
povodňové	povodňový	k2eAgFnPc1d1	povodňová
vlny	vlna	k1gFnPc1	vlna
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
přítoků	přítok	k1gInPc2	přítok
(	(	kIx(	(
<g/>
Dráva	Dráva	k1gFnSc1	Dráva
<g/>
,	,	kIx,	,
Tisa	Tisa	k1gFnSc1	Tisa
<g/>
,	,	kIx,	,
Sáva	Sáva	k1gFnSc1	Sáva
<g/>
)	)	kIx)	)
přijdou	přijít	k5eAaPmIp3nP	přijít
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
mohutná	mohutný	k2eAgFnSc1d1	mohutná
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
povodně	povodně	k6eAd1	povodně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
léta	léto	k1gNnSc2	léto
hladina	hladina	k1gFnSc1	hladina
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
však	však	k9	však
přerušováno	přerušovat	k5eAaImNgNnS	přerušovat
náhlými	náhlý	k2eAgInPc7d1	náhlý
vzestupy	vzestup	k1gInPc7	vzestup
při	při	k7c6	při
deštích	dešť	k1gInPc6	dešť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
deště	dešť	k1gInSc2	dešť
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
podzimní	podzimní	k2eAgFnPc1d1	podzimní
povodně	povodeň	k1gFnPc1	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
teplá	teplý	k2eAgFnSc1d1	teplá
zima	zima	k1gFnSc1	zima
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
setrvat	setrvat	k5eAaPmF	setrvat
vysoký	vysoký	k2eAgInSc1d1	vysoký
vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
a	a	k8xC	a
vzestupy	vzestup	k1gInPc1	vzestup
hladiny	hladina	k1gFnSc2	hladina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
také	také	k6eAd1	také
ledovými	ledový	k2eAgInPc7d1	ledový
zátarasy	zátaras	k1gInPc7	zátaras
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
vylití	vylití	k1gNnSc3	vylití
řeky	řeka	k1gFnSc2	řeka
z	z	k7c2	z
břehů	břeh	k1gInPc2	břeh
a	a	k8xC	a
zatopení	zatopení	k1gNnSc1	zatopení
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zima	zima	k1gFnSc1	zima
studená	studený	k2eAgFnSc1d1	studená
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
amplituda	amplituda	k1gFnSc1	amplituda
kolísání	kolísání	k1gNnSc2	kolísání
úrovně	úroveň	k1gFnSc2	úroveň
hladiny	hladina	k1gFnSc2	hladina
činí	činit	k5eAaImIp3nS	činit
u	u	k7c2	u
Reni	Ren	k1gFnSc2	Ren
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
4,5	[number]	k4	4,5
až	až	k9	až
5,5	[number]	k4	5,5
m	m	kA	m
a	a	k8xC	a
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
6	[number]	k4	6
až	až	k9	až
8	[number]	k4	8
m.	m.	k?	m.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
420	[number]	k4	420
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
1900	[number]	k4	1900
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
6430	[number]	k4	6430
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Průměrně	průměrně	k6eAd1	průměrně
tak	tak	k6eAd1	tak
odvede	odvést	k5eAaPmIp3nS	odvést
za	za	k7c4	za
rok	rok	k1gInSc4	rok
203	[number]	k4	203
km3	km3	k4	km3
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
průtok	průtok	k1gInSc1	průtok
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
000	[number]	k4	000
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
a	a	k8xC	a
nejmenší	malý	k2eAgInSc4d3	nejmenší
1800	[number]	k4	1800
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Do	do	k7c2	do
moře	moře	k1gNnSc2	moře
odnáší	odnášet	k5eAaImIp3nS	odnášet
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
Mt	Mt	k1gFnPc2	Mt
nánosů	nános	k1gInPc2	nános
a	a	k8xC	a
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
přiměřeně	přiměřeně	k6eAd1	přiměřeně
chladných	chladný	k2eAgFnPc6d1	chladná
zimách	zima	k1gFnPc6	zima
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
1,5	[number]	k4	1,5
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
už	už	k9	už
odedávna	odedávna	k6eAd1	odedávna
hrál	hrát	k5eAaImAgMnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
mezi	mezi	k7c7	mezi
střední	střední	k2eAgFnSc7d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc7d1	jihovýchodní
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
intenzitu	intenzita	k1gFnSc4	intenzita
jeho	on	k3xPp3gNnSc2	on
využití	využití	k1gNnSc2	využití
snižuje	snižovat	k5eAaImIp3nS	snižovat
relativně	relativně	k6eAd1	relativně
nízké	nízký	k2eAgNnSc1d1	nízké
soustředění	soustředění	k1gNnSc1	soustředění
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
březích	břeh	k1gInPc6	břeh
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
západoevropskými	západoevropský	k2eAgInPc7d1	západoevropský
toky	tok	k1gInPc7	tok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Splavný	splavný	k2eAgMnSc1d1	splavný
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Ulmu	Ulmus	k1gInSc2	Ulmus
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
od	od	k7c2	od
Řezna	Řezno	k1gNnSc2	Řezno
pro	pro	k7c4	pro
větší	veliký	k2eAgFnPc4d2	veliký
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Brăily	Brăila	k1gFnSc2	Brăila
jej	on	k3xPp3gMnSc4	on
mohou	moct	k5eAaImIp3nP	moct
využívat	využívat	k5eAaPmF	využívat
i	i	k9	i
námořní	námořní	k2eAgFnPc4d1	námořní
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deltě	delta	k1gFnSc6	delta
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
vedena	vést	k5eAaImNgFnS	vést
Sulinským	Sulinský	k2eAgNnSc7d1	Sulinský
ramenem	rameno	k1gNnSc7	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
vodní	vodní	k2eAgFnSc1d1	vodní
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kanálu	kanál	k1gInSc2	kanál
Rýn-Mohan-Dunaj	Rýn-Mohan-Dunaj	k1gFnSc1	Rýn-Mohan-Dunaj
propojena	propojit	k5eAaPmNgFnS	propojit
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Severním	severní	k2eAgNnSc7d1	severní
i	i	k8xC	i
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
plánované	plánovaný	k2eAgNnSc1d1	plánované
splavné	splavný	k2eAgNnSc1d1	splavné
propojení	propojení	k1gNnSc1	propojení
Dunaje	Dunaj	k1gInSc2	Dunaj
přes	přes	k7c4	přes
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
s	s	k7c7	s
povodím	povodí	k1gNnSc7	povodí
Odry	Odra	k1gFnSc2	Odra
a	a	k8xC	a
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kanál	kanál	k1gInSc1	kanál
Dunaj-Odra-Labe	Dunaj-Odra-Lab	k1gMnSc5	Dunaj-Odra-Lab
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prozatím	prozatím	k6eAd1	prozatím
stále	stále	k6eAd1	stále
jen	jen	k9	jen
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
zvažování	zvažování	k1gNnSc2	zvažování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
lze	lze	k6eAd1	lze
zkrátit	zkrátit	k5eAaPmF	zkrátit
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
plavbou	plavba	k1gFnSc7	plavba
průplavem	průplav	k1gInSc7	průplav
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
Konstanca	Konstanc	k1gInSc2	Konstanc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nákladní	nákladní	k2eAgFnSc6d1	nákladní
dopravě	doprava	k1gFnSc6	doprava
převažují	převažovat	k5eAaImIp3nP	převažovat
rudné	rudný	k2eAgFnPc1d1	Rudná
i	i	k8xC	i
nerudné	rudný	k2eNgFnPc1d1	nerudná
suroviny	surovina	k1gFnPc1	surovina
a	a	k8xC	a
také	také	k9	také
šrot	šrot	k1gInSc1	šrot
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
cement	cement	k1gInSc1	cement
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
produkty	produkt	k1gInPc1	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
stoupá	stoupat	k5eAaImIp3nS	stoupat
i	i	k9	i
přeprava	přeprava	k1gFnSc1	přeprava
kontejnerů	kontejner	k1gInPc2	kontejner
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Reni	Ren	k1gFnPc1	Ren
<g/>
,	,	kIx,	,
Ismail	Ismail	k1gInSc1	Ismail
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
březích	břeh	k1gInPc6	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
vede	vést	k5eAaImIp3nS	vést
známá	známý	k2eAgFnSc1d1	známá
cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
trasa	trasa	k1gFnSc1	trasa
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
Eurovelo	Eurovela	k1gFnSc5	Eurovela
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
je	být	k5eAaImIp3nS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
v	v	k7c6	v
soutěsce	soutěska	k1gFnSc6	soutěska
Železná	železný	k2eAgNnPc1d1	železné
vrata	vrata	k1gNnPc1	vrata
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
velká	velká	k1gFnSc1	velká
(	(	kIx(	(
<g/>
Gabčíkovo	Gabčíkovo	k1gNnSc1	Gabčíkovo
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
několik	několik	k4yIc1	několik
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
žádné	žádný	k3yNgNnSc1	žádný
vodní	vodní	k2eAgNnSc1d1	vodní
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
dunajský	dunajský	k2eAgInSc1d1	dunajský
veletok	veletok	k1gInSc1	veletok
přehradilo	přehradit	k5eAaPmAgNnS	přehradit
<g/>
,	,	kIx,	,
od	od	k7c2	od
některých	některý	k3yIgInPc2	některý
projektů	projekt	k1gInPc2	projekt
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
upuštěno	upustit	k5eAaPmNgNnS	upustit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
od	od	k7c2	od
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Nagymarosi	Nagymarose	k1gFnSc6	Nagymarose
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přítoků	přítok	k1gInPc2	přítok
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
využívány	využíván	k2eAgInPc1d1	využíván
Váh	Váh	k1gInSc1	Váh
<g/>
,	,	kIx,	,
Dráva	Dráva	k1gFnSc1	Dráva
<g/>
,	,	kIx,	,
Tisa	Tisa	k1gFnSc1	Tisa
<g/>
,	,	kIx,	,
Iskar	Iskar	k1gInSc1	Iskar
<g/>
,	,	kIx,	,
Argeș	Argeș	k1gFnSc1	Argeș
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jak	jak	k6eAd1	jak
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
přehrady	přehrada	k1gFnPc4	přehrada
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
složité	složitý	k2eAgFnPc4d1	složitá
kaskády	kaskáda	k1gFnPc4	kaskáda
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
suchých	suchý	k2eAgFnPc2d1	suchá
oblastí	oblast	k1gFnPc2	oblast
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
,	,	kIx,	,
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c4	na
zásobování	zásobování	k1gNnSc4	zásobování
měst	město	k1gNnPc2	město
a	a	k8xC	a
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
taky	taky	k9	taky
rekreační	rekreační	k2eAgNnSc1d1	rekreační
využití	využití	k1gNnSc1	využití
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
výlov	výlov	k1gInSc1	výlov
ryb	ryba	k1gFnPc2	ryba
představuje	představovat	k5eAaImIp3nS	představovat
550	[number]	k4	550
až	až	k9	až
650	[number]	k4	650
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
dolní	dolní	k2eAgInSc4d1	dolní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deltě	delta	k1gFnSc6	delta
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
3000	[number]	k4	3000
km2	km2	k4	km2
skřípinec	skřípinec	k1gInSc1	skřípinec
jezerní	jezerní	k2eAgInSc1d1	jezerní
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
protéká	protékat	k5eAaImIp3nS	protékat
územím	území	k1gNnSc7	území
10	[number]	k4	10
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
země	země	k1gFnSc1	země
Bádensko-Württembersko	Bádensko-Württembersko	k1gNnSc1	Bádensko-Württembersko
<g/>
,	,	kIx,	,
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
)	)	kIx)	)
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
země	země	k1gFnSc1	země
Horní	horní	k2eAgNnSc1d1	horní
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
Slovensko	Slovensko	k1gNnSc1	Slovensko
(	(	kIx(	(
<g/>
Bratislavský	bratislavský	k2eAgInSc1d1	bratislavský
<g/>
,	,	kIx,	,
Trnavský	trnavský	k2eAgInSc1d1	trnavský
<g/>
,	,	kIx,	,
Nitranský	nitranský	k2eAgInSc1d1	nitranský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
župy	župa	k1gFnPc1	župa
Győr-Moson-Sopron	Győr-Moson-Sopron	k1gInSc1	Győr-Moson-Sopron
<g/>
,	,	kIx,	,
Komárom-Esztergom	Komárom-Esztergom	k1gInSc1	Komárom-Esztergom
<g/>
,	,	kIx,	,
Pest	Pest	k1gInSc1	Pest
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Fejér	Fejér	k1gInSc1	Fejér
<g/>
,	,	kIx,	,
Tolna	Tolna	k1gFnSc1	Tolna
<g/>
,	,	kIx,	,
Baranya	Baranya	k1gFnSc1	Baranya
<g/>
,	,	kIx,	,
Bács-Kiskun	Bács-Kiskun	k1gNnSc1	Bács-Kiskun
<g/>
)	)	kIx)	)
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
(	(	kIx(	(
<g/>
Osijecko-baranjská	Osijeckoaranjský	k2eAgFnSc1d1	Osijecko-baranjský
<g/>
,	,	kIx,	,
Vukovarsko-sremská	Vukovarskoremský	k2eAgFnSc1d1	Vukovarsko-sremský
župa	župa	k1gFnSc1	župa
<g/>
)	)	kIx)	)
Srbsko	Srbsko	k1gNnSc1	Srbsko
(	(	kIx(	(
<g/>
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
Vojvodina	Vojvodina	k1gFnSc1	Vojvodina
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
hl.	hl.	k?	hl.
m.	m.	k?	m.
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
,	,	kIx,	,
okresy	okres	k1gInPc1	okres
Podunavlje	Podunavlj	k1gInSc2	Podunavlj
<g/>
,	,	kIx,	,
Braničevo	Braničevo	k1gNnSc1	Braničevo
<g/>
,	,	kIx,	,
Bor	bor	k1gInSc1	bor
<g/>
)	)	kIx)	)
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
župy	župa	k1gFnPc1	župa
Caraș	Caraș	k1gFnSc2	Caraș
<g/>
,	,	kIx,	,
Mehedinț	Mehedinț	k1gMnSc1	Mehedinț
<g/>
,	,	kIx,	,
Dolj	Dolj	k1gMnSc1	Dolj
<g/>
,	,	kIx,	,
Olt	Olt	k1gMnSc1	Olt
<g/>
,	,	kIx,	,
Teleorman	Teleorman	k1gMnSc1	Teleorman
<g/>
,	,	kIx,	,
Giurgiu	Giurgius	k1gMnSc3	Giurgius
<g/>
,	,	kIx,	,
Călăraș	Călăraș	k1gMnSc3	Călăraș
<g/>
,	,	kIx,	,
Ialomiț	Ialomiț	k1gMnSc3	Ialomiț
<g/>
,	,	kIx,	,
Constanț	Constanț	k1gMnSc3	Constanț
<g/>
,	,	kIx,	,
Brăila	Brăila	k1gFnSc1	Brăila
<g/>
,	,	kIx,	,
Galaț	Galaț	k1gFnSc1	Galaț
<g/>
,	,	kIx,	,
Tulcea	Tulcea	k1gFnSc1	Tulcea
<g/>
)	)	kIx)	)
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
(	(	kIx(	(
<g/>
Vidinská	vidinský	k2eAgFnSc1d1	vidinský
<g/>
,	,	kIx,	,
Montanská	Montanský	k2eAgFnSc1d1	Montanský
<g/>
,	,	kIx,	,
Vracká	Vracký	k2eAgFnSc1d1	Vracký
<g/>
,	,	kIx,	,
Plevenská	Plevenský	k2eAgFnSc1d1	Plevenský
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Velikotarnovská	Velikotarnovský	k2eAgFnSc1d1	Velikotarnovský
<g/>
,	,	kIx,	,
Rusenská	Rusenský	k2eAgFnSc1d1	Rusenský
<g/>
,	,	kIx,	,
Silisterská	Silisterský	k2eAgFnSc1d1	Silisterský
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Cahul	Cahul	k1gInSc1	Cahul
<g/>
)	)	kIx)	)
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
Oděská	oděský	k2eAgFnSc1d1	Oděská
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
historické	historický	k2eAgNnSc1d1	historické
území	území	k1gNnSc1	území
Budžak	Budžak	k1gMnSc1	Budžak
<g/>
)	)	kIx)	)
Čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc1	přístup
jen	jen	k9	jen
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
břehu	břeh	k1gInSc3	břeh
řeky	řeka	k1gFnSc2	řeka
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
a	a	k8xC	a
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pět	pět	k4xCc4	pět
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
a	a	k8xC	a
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
)	)	kIx)	)
plní	plnit	k5eAaImIp3nS	plnit
Dunaj	Dunaj	k1gInSc1	Dunaj
jistou	jistý	k2eAgFnSc4d1	jistá
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
chybějící	chybějící	k2eAgInSc4d1	chybějící
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
37	[number]	k4	37
%	%	kIx~	%
délky	délka	k1gFnSc2	délka
toku	tok	k1gInSc2	tok
(	(	kIx(	(
<g/>
1	[number]	k4	1
0	[number]	k4	0
<g/>
70,9	[number]	k4	70,9
km	km	kA	km
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
zároveň	zároveň	k6eAd1	zároveň
státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
Německo	Německo	k1gNnSc4	Německo
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
/	/	kIx~	/
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
/	/	kIx~	/
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
/	/	kIx~	/
<g/>
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
/	/	kIx~	/
<g/>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
/	/	kIx~	/
<g/>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
/	/	kIx~	/
<g/>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
/	/	kIx~	/
<g/>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Donaueschingen	Donaueschingen	k1gInSc1	Donaueschingen
<g/>
,	,	kIx,	,
Tuttlingen	Tuttlingen	k1gInSc1	Tuttlingen
<g/>
,	,	kIx,	,
Ulm	Ulm	k1gFnSc1	Ulm
<g/>
,	,	kIx,	,
Donauwörth	Donauwörth	k1gInSc1	Donauwörth
<g/>
,	,	kIx,	,
Ingolstadt	Ingolstadt	k1gInSc1	Ingolstadt
<g/>
,	,	kIx,	,
Řezno	Řezno	k1gNnSc1	Řezno
<g/>
,	,	kIx,	,
Straubing	Straubing	k1gInSc1	Straubing
<g/>
,	,	kIx,	,
Deggendorf	Deggendorf	k1gInSc1	Deggendorf
<g/>
,	,	kIx,	,
Pasov	Pasov	k1gInSc1	Pasov
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Mauthausen	Mauthausen	k1gInSc1	Mauthausen
<g/>
,	,	kIx,	,
Grein	Grein	k1gInSc1	Grein
<g/>
,	,	kIx,	,
Ybbs	Ybbs	k1gInSc1	Ybbs
<g/>
,	,	kIx,	,
Melk	Melk	k1gInSc1	Melk
<g/>
,	,	kIx,	,
Dürnstein	Dürnstein	k1gInSc1	Dürnstein
<g/>
,	,	kIx,	,
Krems	Krems	k1gInSc1	Krems
<g/>
,	,	kIx,	,
Tulln	Tulln	k1gMnSc1	Tulln
<g/>
,	,	kIx,	,
Korneuburg	Korneuburg	k1gMnSc1	Korneuburg
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Hainburg	Hainburg	k1gInSc1	Hainburg
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Komárno	Komárno	k1gNnSc1	Komárno
<g/>
,	,	kIx,	,
Štúrovo	Štúrův	k2eAgNnSc1d1	Štúrovo
Győr	Győr	k1gInSc1	Győr
<g/>
,	,	kIx,	,
Ostřihom	Ostřihom	k1gInSc1	Ostřihom
<g/>
,	,	kIx,	,
Vác	Vác	k1gFnSc1	Vác
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Dunaújváros	Dunaújvárosa	k1gFnPc2	Dunaújvárosa
<g/>
,	,	kIx,	,
Paks	Paksa	k1gFnPc2	Paksa
<g/>
,	,	kIx,	,
Kalocsa	Kalocsa	k1gFnSc1	Kalocsa
<g/>
,	,	kIx,	,
Baja	Baja	k1gFnSc1	Baja
<g/>
,	,	kIx,	,
Moháč	Moháč	k1gInSc1	Moháč
Vukovar	Vukovar	k1gInSc1	Vukovar
<g/>
,	,	kIx,	,
Ilok	Ilok	k1gMnSc1	Ilok
Bezdan	Bezdan	k1gMnSc1	Bezdan
<g/>
,	,	kIx,	,
Apatin	Apatin	k1gMnSc1	Apatin
<g/>
,	,	kIx,	,
Bačka	Bačka	k1gFnSc1	Bačka
Palanka	palanka	k1gFnSc1	palanka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Novi	Novi	k1gNnPc7	Novi
Sad	sad	k1gInSc1	sad
<g/>
,	,	kIx,	,
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
,	,	kIx,	,
Pančevo	Pančevo	k1gNnSc4	Pančevo
<g/>
,	,	kIx,	,
Smederevo	Smederevo	k1gNnSc4	Smederevo
<g/>
,	,	kIx,	,
Donji	Donje	k1gFnSc4	Donje
Milanovac	Milanovac	k1gFnSc4	Milanovac
<g/>
,	,	kIx,	,
Kladovo	Kladův	k2eAgNnSc4d1	Kladův
Moldova	Moldův	k2eAgMnSc4d1	Moldův
Veche	Vech	k1gMnSc4	Vech
<g/>
,	,	kIx,	,
Orș	Orș	k1gMnSc4	Orș
<g/>
,	,	kIx,	,
Drobeta-Turnu	Drobeta-Turna	k1gFnSc4	Drobeta-Turna
Severin	Severin	k1gMnSc1	Severin
<g/>
,	,	kIx,	,
Calafat	Calafat	k1gMnSc1	Calafat
<g/>
,	,	kIx,	,
Corabia	Corabia	k1gFnSc1	Corabia
<g/>
,	,	kIx,	,
Giurgiu	Giurgium	k1gNnSc6	Giurgium
<g/>
,	,	kIx,	,
Călăraș	Călăraș	k1gFnSc1	Călăraș
<g/>
,	,	kIx,	,
Brăila	Brăila	k1gFnSc1	Brăila
<g/>
,	,	kIx,	,
Galaț	Galaț	k1gMnSc1	Galaț
<g/>
,	,	kIx,	,
Tulcea	Tulcea	k1gMnSc1	Tulcea
<g/>
,	,	kIx,	,
Sulina	Sulina	k1gMnSc1	Sulina
Vidin	vidina	k1gFnPc2	vidina
<g/>
,	,	kIx,	,
Lom	lom	k1gInSc1	lom
<g/>
,	,	kIx,	,
Kozloduj	Kozloduj	k1gFnSc1	Kozloduj
<g/>
,	,	kIx,	,
Svištov	Svištov	k1gInSc1	Svištov
<g/>
,	,	kIx,	,
Ruse	ruse	k6eAd1	ruse
<g/>
,	,	kIx,	,
Tutrakan	Tutrakan	k1gInSc1	Tutrakan
<g/>
,	,	kIx,	,
Silistra	Silistra	k1gFnSc1	Silistra
Giurgiuleș	Giurgiuleș	k1gFnSc2	Giurgiuleș
Reni	Reni	k1gNnSc1	Reni
<g/>
,	,	kIx,	,
Izmajil	Izmajil	k1gInSc1	Izmajil
Velikostí	velikost	k1gFnSc7	velikost
povodí	povodit	k5eAaPmIp3nS	povodit
přes	přes	k7c4	přes
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Dunaj	Dunaj	k1gInSc1	Dunaj
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
Volhou	Volha	k1gFnSc7	Volha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
povodí	povodí	k1gNnSc1	povodí
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
19	[number]	k4	19
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
20	[number]	k4	20
<g/>
,	,	kIx,	,
počítáme	počítat	k5eAaImIp1nP	počítat
<g/>
-li	i	k?	-li
zvlášť	zvlášť	k6eAd1	zvlášť
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
států	stát	k1gInPc2	stát
na	na	k7c6	na
povodí	povodí	k1gNnSc6	povodí
jediné	jediný	k2eAgFnSc2d1	jediná
řeky	řeka	k1gFnSc2	řeka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
Dunaj	Dunaj	k1gInSc1	Dunaj
přímo	přímo	k6eAd1	přímo
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
,	,	kIx,	,
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
a	a	k8xC	a
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nepatrné	patrný	k2eNgInPc4d1	patrný
zásahy	zásah	k1gInPc4	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Kompletně	kompletně	k6eAd1	kompletně
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Dunaje	Dunaj	k1gInSc2	Dunaj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
z	z	k7c2	z
drtivé	drtivý	k2eAgFnSc2d1	drtivá
většiny	většina	k1gFnSc2	většina
také	také	k6eAd1	také
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
a	a	k8xC	a
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
většina	většina	k1gFnSc1	většina
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
<g/>
,	,	kIx,	,
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
a	a	k8xC	a
Česka	Česko	k1gNnSc2	Česko
(	(	kIx(	(
<g/>
povodí	povodí	k1gNnSc2	povodí
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
okrajové	okrajový	k2eAgFnSc3d1	okrajová
části	část	k1gFnSc3	část
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
)	)	kIx)	)
a	a	k8xC	a
nezanedbatelná	zanedbatelný	k2eNgFnSc1d1	nezanedbatelná
část	část	k1gFnSc1	část
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
marginální	marginální	k2eAgInSc4d1	marginální
zásah	zásah	k1gInSc4	zásah
a	a	k8xC	a
u	u	k7c2	u
Albánie	Albánie	k1gFnSc2	Albánie
a	a	k8xC	a
Makedonie	Makedonie	k1gFnSc2	Makedonie
jde	jít	k5eAaImIp3nS	jít
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
o	o	k7c4	o
odchylku	odchylka	k1gFnSc4	odchylka
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
od	od	k7c2	od
přesné	přesný	k2eAgFnSc2d1	přesná
rozvodnice	rozvodnice	k1gFnSc2	rozvodnice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geopolitického	geopolitický	k2eAgNnSc2d1	geopolitické
hlediska	hledisko	k1gNnSc2	hledisko
patří	patřit	k5eAaImIp3nS	patřit
povodí	povodí	k1gNnSc4	povodí
Dunaje	Dunaj	k1gInSc2	Dunaj
k	k	k7c3	k
dosti	dosti	k6eAd1	dosti
nestabilním	stabilní	k2eNgFnPc3d1	nestabilní
oblastem	oblast	k1gFnPc3	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jen	jen	k9	jen
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
několikrát	několikrát	k6eAd1	několikrát
výrazně	výrazně	k6eAd1	výrazně
proměnily	proměnit	k5eAaPmAgFnP	proměnit
státní	státní	k2eAgFnPc4d1	státní
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
rekordní	rekordní	k2eAgInSc1d1	rekordní
počet	počet	k1gInSc1	počet
států	stát	k1gInPc2	stát
na	na	k7c6	na
toku	tok	k1gInSc6	tok
a	a	k8xC	a
povodí	povodí	k1gNnSc6	povodí
Dunaje	Dunaj	k1gInSc2	Dunaj
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
ústupu	ústup	k1gInSc2	ústup
či	či	k8xC	či
rozpadu	rozpad	k1gInSc2	rozpad
několika	několik	k4yIc2	několik
větších	veliký	k2eAgInPc2d2	veliký
státních	státní	k2eAgInPc2d1	státní
celků	celek	k1gInPc2	celek
-	-	kIx~	-
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
etnická	etnický	k2eAgFnSc1d1	etnická
skladba	skladba	k1gFnSc1	skladba
dunajského	dunajský	k2eAgNnSc2d1	Dunajské
povodí	povodí	k1gNnSc2	povodí
měla	mít	k5eAaImAgFnS	mít
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
procesy	proces	k1gInPc4	proces
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
sporném	sporný	k2eAgNnSc6d1	sporné
území	území	k1gNnSc6	území
při	při	k7c6	při
dunajské	dunajský	k2eAgFnSc6d1	Dunajská
říční	říční	k2eAgFnSc6d1	říční
hranici	hranice	k1gFnSc6	hranice
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
Srbskem	Srbsko	k1gNnSc7	Srbsko
a	a	k8xC	a
Chorvatskem	Chorvatsko	k1gNnSc7	Chorvatsko
<g/>
)	)	kIx)	)
vyhlášen	vyhlášen	k2eAgInSc4d1	vyhlášen
soukromý	soukromý	k2eAgInSc4d1	soukromý
ministát	ministát	k1gInSc4	ministát
Liberland	Liberlanda	k1gFnPc2	Liberlanda
<g/>
.	.	kIx.	.
</s>
<s>
Dunaj	Dunaj	k1gInSc1	Dunaj
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
řeka	řeka	k1gFnSc1	řeka
a	a	k8xC	a
pravidla	pravidlo	k1gNnSc2	pravidlo
jejího	její	k3xOp3gNnSc2	její
používání	používání	k1gNnSc2	používání
stanoví	stanovit	k5eAaPmIp3nS	stanovit
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
dohody	dohoda	k1gFnPc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
nastaly	nastat	k5eAaPmAgInP	nastat
tři	tři	k4xCgFnPc4	tři
etapy	etapa	k1gFnPc4	etapa
regulace	regulace	k1gFnSc2	regulace
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
období	období	k1gNnSc4	období
dvoustranných	dvoustranný	k2eAgFnPc2d1	dvoustranná
dohod	dohoda	k1gFnPc2	dohoda
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
období	období	k1gNnSc1	období
mnohostranných	mnohostranný	k2eAgFnPc2d1	mnohostranná
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
nedunajských	dunajský	k2eNgFnPc2d1	dunajský
vlád	vláda	k1gFnPc2	vláda
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
demokratický	demokratický	k2eAgInSc4d1	demokratický
status	status	k1gInSc4	status
řeky	řeka	k1gFnSc2	řeka
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc4d1	důležitý
význam	význam	k1gInSc4	význam
měla	mít	k5eAaImAgFnS	mít
Küçük-kaynarcká	Küçükaynarcký	k2eAgFnSc1d1	Küçük-kaynarcký
mírová	mírový	k2eAgFnSc1d1	mírová
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
níž	nízce	k6eAd2	nízce
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
Rusko	Rusko	k1gNnSc1	Rusko
práva	právo	k1gNnSc2	právo
plavby	plavba	k1gFnSc2	plavba
po	po	k7c6	po
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dalších	další	k2eAgFnPc2d1	další
dohod	dohoda	k1gFnPc2	dohoda
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
1826	[number]	k4	1826
a	a	k8xC	a
1829	[number]	k4	1829
získalo	získat	k5eAaPmAgNnS	získat
Rusko	Rusko	k1gNnSc1	Rusko
plnou	plný	k2eAgFnSc4d1	plná
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
všemi	všecek	k3xTgInPc7	všecek
třemi	tři	k4xCgInPc7	tři
základními	základní	k2eAgInPc7d1	základní
rameny	rameno	k1gNnPc7	rameno
dunajské	dunajský	k2eAgMnPc4d1	dunajský
delty	delta	k1gFnSc2	delta
a	a	k8xC	a
nad	nad	k7c7	nad
plavbou	plavba	k1gFnSc7	plavba
po	po	k7c6	po
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Anglo-rakouská	Angloakouský	k2eAgFnSc1d1	Anglo-rakouská
a	a	k8xC	a
rusko-rakouská	ruskoakouský	k2eAgFnSc1d1	rusko-rakouská
dohoda	dohoda	k1gFnSc1	dohoda
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1838	[number]	k4	1838
a	a	k8xC	a
1840	[number]	k4	1840
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
princip	princip	k1gInSc4	princip
svobodné	svobodný	k2eAgFnSc2d1	svobodná
dopravy	doprava	k1gFnSc2	doprava
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Etapa	etapa	k1gFnSc1	etapa
regulace	regulace	k1gFnSc2	regulace
systémem	systém	k1gInSc7	systém
mnohostranných	mnohostranný	k2eAgFnPc2d1	mnohostranná
dohod	dohoda	k1gFnPc2	dohoda
začala	začít	k5eAaPmAgFnS	začít
po	po	k7c6	po
Krymské	krymský	k2eAgFnSc6d1	Krymská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Evropská	evropský	k2eAgFnSc1d1	Evropská
dunajská	dunajský	k2eAgFnSc1d1	Dunajská
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
nedunajských	dunajský	k2eNgFnPc2d1	dunajský
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
,	,	kIx,	,
Sardínie	Sardínie	k1gFnSc1	Sardínie
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
dohoda	dohoda	k1gFnSc1	dohoda
stala	stát	k5eAaPmAgFnS	stát
prostředkem	prostředek	k1gInSc7	prostředek
nadvlády	nadvláda	k1gFnSc2	nadvláda
nedunajských	dunajský	k2eNgFnPc2d1	dunajský
zemí	zem	k1gFnPc2	zem
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
a	a	k8xC	a
existovala	existovat	k5eAaImAgFnS	existovat
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
měla	mít	k5eAaImAgFnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
administrativu	administrativa	k1gFnSc4	administrativa
<g/>
,	,	kIx,	,
flotilu	flotila	k1gFnSc4	flotila
<g/>
,	,	kIx,	,
policii	policie	k1gFnSc4	policie
<g/>
,	,	kIx,	,
soud	soud	k1gInSc4	soud
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
zakázat	zakázat	k5eAaPmF	zakázat
podunajským	podunajský	k2eAgInPc3d1	podunajský
státům	stát	k1gInPc3	stát
stavbu	stavba	k1gFnSc4	stavba
přístavních	přístavní	k2eAgNnPc2d1	přístavní
a	a	k8xC	a
hydrotechnických	hydrotechnický	k2eAgNnPc2d1	hydrotechnické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
ujednána	ujednat	k5eAaPmNgFnS	ujednat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1920	[number]	k4	1920
dohoda	dohoda	k1gFnSc1	dohoda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
převážně	převážně	k6eAd1	převážně
nedunajských	dunajský	k2eNgFnPc2d1	dunajský
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byly	být	k5eAaImAgFnP	být
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1921	[number]	k4	1921
přijaty	přijat	k2eAgFnPc4d1	přijata
konvence	konvence	k1gFnPc4	konvence
a	a	k8xC	a
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
protokol	protokol	k1gInSc4	protokol
o	o	k7c6	o
režimu	režim	k1gInSc6	režim
plavby	plavba	k1gFnSc2	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
Ulmem	Ulmem	k1gInSc1	Ulmem
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Evropská	evropský	k2eAgFnSc1d1	Evropská
dunajská	dunajský	k2eAgFnSc1d1	Dunajská
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tok	tok	k1gInSc4	tok
mezi	mezi	k7c7	mezi
Brăilou	Brăila	k1gFnSc7	Brăila
a	a	k8xC	a
Ulmem	Ulmem	k1gInSc1	Ulmem
a	a	k8xC	a
síť	síť	k1gFnSc1	síť
říčních	říční	k2eAgFnPc2d1	říční
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
mezinárodními	mezinárodní	k2eAgInPc7d1	mezinárodní
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
dunajská	dunajský	k2eAgFnSc1d1	Dunajská
komise	komise	k1gFnSc1	komise
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
dunajských	dunajský	k2eAgInPc2d1	dunajský
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
i	i	k8xC	i
nedunajských	dunajský	k2eNgFnPc6d1	dunajský
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
komise	komise	k1gFnPc1	komise
měly	mít	k5eAaImAgFnP	mít
široké	široký	k2eAgFnPc1d1	široká
pravomoce	pravomoc	k1gFnPc1	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
podunajských	podunajský	k2eAgFnPc2d1	Podunajská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
dunajskou	dunajský	k2eAgFnSc4d1	Dunajská
komisi	komise	k1gFnSc4	komise
opustilo	opustit	k5eAaPmAgNnS	opustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
po	po	k7c6	po
zabrání	zabrání	k1gNnSc6	zabrání
Německem	Německo	k1gNnSc7	Německo
také	také	k9	také
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
Dunaje	Dunaj	k1gInSc2	Dunaj
projednávala	projednávat	k5eAaImAgFnS	projednávat
na	na	k7c6	na
postupimské	postupimský	k2eAgFnSc6d1	Postupimská
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
na	na	k7c4	na
shromáždění	shromáždění	k1gNnSc4	shromáždění
ministrů	ministr	k1gMnPc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
status	status	k1gInSc1	status
plavby	plavba	k1gFnSc2	plavba
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgInS	vypracovat
na	na	k7c6	na
Bělehradské	bělehradský	k2eAgFnSc6d1	Bělehradská
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
podunajských	podunajský	k2eAgInPc2d1	podunajský
(	(	kIx(	(
<g/>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
)	)	kIx)	)
i	i	k8xC	i
nepodunajských	podunajský	k2eNgInPc2d1	podunajský
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
mělo	mít	k5eAaImAgNnS	mít
poradní	poradní	k2eAgInSc4d1	poradní
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
konvence	konvence	k1gFnSc1	konvence
o	o	k7c6	o
plavbě	plavba	k1gFnSc6	plavba
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1948	[number]	k4	1948
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Nedunajské	dunajský	k2eNgFnPc1d1	dunajský
země	zem	k1gFnPc1	zem
ji	on	k3xPp3gFnSc4	on
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
nová	nový	k2eAgFnSc1d1	nová
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
podunajských	podunajský	k2eAgFnPc2d1	Podunajská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Д	Д	k?	Д
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Delta	delta	k1gFnSc1	delta
Dunaje	Dunaj	k1gInSc2	Dunaj
Kanál	kanál	k1gInSc1	kanál
Dunaj-Odra-Labe	Dunaj-Odra-Lab	k1gInSc5	Dunaj-Odra-Lab
Kanál	kanál	k1gInSc4	kanál
Dunaj-Černé	Dunaj-Černý	k2eAgNnSc4d1	Dunaj-Černý
moře	moře	k1gNnSc4	moře
Propadání	propadání	k1gNnSc2	propadání
Dunaje	Dunaj	k1gInSc2	Dunaj
Průplav	průplav	k1gInSc1	průplav
Rýn-Mohan-Dunaj	Rýn-Mohan-Dunaj	k1gFnSc1	Rýn-Mohan-Dunaj
Galerie	galerie	k1gFnSc1	galerie
Dunaj	Dunaj	k1gInSc4	Dunaj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dunaj	Dunaj	k1gInSc1	Dunaj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Dunaj	Dunaj	k1gInSc1	Dunaj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Dunaj	Dunaj	k1gInSc4	Dunaj
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
