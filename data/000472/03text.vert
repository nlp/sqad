<s>
Kordillery	Kordillery	k1gFnPc1	Kordillery
[	[	kIx(	[
<g/>
kordyljery	kordyljera	k1gFnPc1	kordyljera
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ze	z	k7c2	z
španělského	španělský	k2eAgInSc2d1	španělský
výrazu	výraz	k1gInSc2	výraz
cordillera	cordiller	k1gMnSc2	cordiller
znamenajícího	znamenající	k2eAgMnSc2d1	znamenající
"	"	kIx"	"
<g/>
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Andy	Anda	k1gFnSc2	Anda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třetihorní	třetihorní	k2eAgMnSc1d1	třetihorní
pásemné	pásemný	k2eAgNnSc4d1	pásemné
pohoří	pohoří	k1gNnSc4	pohoří
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
15	[number]	k4	15
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
sahají	sahat	k5eAaImIp3nP	sahat
z	z	k7c2	z
Aljašky	Aljaška	k1gFnSc2	Aljaška
až	až	k9	až
k	k	k7c3	k
Tehuantepecké	Tehuantepecký	k2eAgFnSc3d1	Tehuantepecký
šíji	šíj	k1gFnSc3	šíj
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
pásma	pásmo	k1gNnPc4	pásmo
<g/>
,	,	kIx,	,
uzavírající	uzavírající	k2eAgMnPc1d1	uzavírající
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
rozlehlé	rozlehlý	k2eAgNnSc4d1	rozlehlé
náhorní	náhorní	k2eAgFnPc4d1	náhorní
plošiny	plošina	k1gFnPc4	plošina
a	a	k8xC	a
kotliny	kotlina	k1gFnPc4	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Řetěz	řetěz	k1gInSc1	řetěz
pohoří	pohořet	k5eAaPmIp3nS	pohořet
při	při	k7c6	při
tichomořském	tichomořský	k2eAgNnSc6d1	Tichomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
probíhá	probíhat	k5eAaImIp3nS	probíhat
částečně	částečně	k6eAd1	částečně
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
(	(	kIx(	(
<g/>
Přímořské	přímořský	k2eAgFnPc1d1	přímořská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
pohoří	pohoří	k1gNnSc1	pohoří
sv.	sv.	kA	sv.
Eliáše	Eliáš	k1gMnSc2	Eliáš
s	s	k7c7	s
Mount	Mount	k1gMnSc1	Mount
Loganem	Logan	k1gMnSc7	Logan
<g/>
,	,	kIx,	,
6050	[number]	k4	6050
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jihu	jih	k1gInSc3	jih
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
Pobřežní	pobřežní	k2eAgNnSc4d1	pobřežní
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
,	,	kIx,	,
provázené	provázený	k2eAgNnSc1d1	provázené
Kaskádovým	kaskádový	k2eAgNnSc7d1	kaskádové
pohořím	pohoří	k1gNnSc7	pohoří
(	(	kIx(	(
<g/>
Mount	Mount	k1gMnSc1	Mount
Rainier	Rainier	k1gMnSc1	Rainier
<g/>
,	,	kIx,	,
4391	[number]	k4	4391
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sierrou	Sierra	k1gFnSc7	Sierra
Nevadu	Nevada	k1gFnSc4	Nevada
(	(	kIx(	(
<g/>
Mount	Mount	k1gInSc4	Mount
Whitney	Whitnea	k1gFnSc2	Whitnea
<g/>
,	,	kIx,	,
4418	[number]	k4	4418
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
jako	jako	k8xS	jako
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
Occidental	Occidental	k1gMnSc1	Occidental
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgNnSc1d1	východní
pásmo	pásmo	k1gNnSc1	pásmo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
největších	veliký	k2eAgFnPc2d3	veliký
výšek	výška	k1gFnPc2	výška
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
Denali	Denali	k1gFnSc2	Denali
<g/>
,	,	kIx,	,
6168	[number]	k4	6168
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jako	jako	k9	jako
Skalnaté	skalnatý	k2eAgFnPc4d1	skalnatá
hory	hora	k1gFnPc4	hora
(	(	kIx(	(
<g/>
Mount	Mount	k1gMnSc1	Mount
Elbert	Elbert	k1gMnSc1	Elbert
<g/>
,	,	kIx,	,
4399	[number]	k4	4399
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
jako	jako	k8xC	jako
Sierra	Sierra	k1gFnSc1	Sierra
Madre	Madr	k1gInSc5	Madr
Oriental	Oriental	k1gMnSc1	Oriental
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
náhorních	náhorní	k2eAgFnPc2d1	náhorní
plošin	plošina	k1gFnPc2	plošina
<g/>
,	,	kIx,	,
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
oběma	dva	k4xCgNnPc7	dva
pásmy	pásmo	k1gNnPc7	pásmo
Kordiller	Kordillery	k1gFnPc2	Kordillery
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
Kolumbijská	kolumbijský	k2eAgFnSc1d1	kolumbijská
plošina	plošina	k1gFnSc1	plošina
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
pánev	pánev	k1gFnSc1	pánev
s	s	k7c7	s
Velkým	velký	k2eAgNnSc7d1	velké
solným	solný	k2eAgNnSc7d1	solné
jezerem	jezero	k1gNnSc7	jezero
a	a	k8xC	a
plošiny	plošina	k1gFnPc1	plošina
Koloradská	Koloradský	k2eAgFnSc1d1	Koloradský
a	a	k8xC	a
Mexická	mexický	k2eAgFnSc1d1	mexická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Mexiku	Mexiko	k1gNnSc6	Mexiko
jsou	být	k5eAaImIp3nP	být
horská	horský	k2eAgNnPc1d1	horské
pásma	pásmo	k1gNnPc1	pásmo
Kordiller	Kordillery	k1gFnPc2	Kordillery
přerušena	přerušit	k5eAaPmNgFnS	přerušit
pruhem	pruh	k1gInSc7	pruh
sopek	sopka	k1gFnPc2	sopka
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
1000	[number]	k4	1000
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
Citlaltépetl	Citlaltépetl	k1gMnSc1	Citlaltépetl
<g/>
,	,	kIx,	,
5700	[number]	k4	5700
m	m	kA	m
<g/>
;	;	kIx,	;
Popocatépetl	Popocatépetl	k1gMnSc1	Popocatépetl
<g/>
,	,	kIx,	,
5452	[number]	k4	5452
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
probíhají	probíhat	k5eAaImIp3nP	probíhat
Kordillery	Kordillery	k1gFnPc1	Kordillery
svou	svůj	k3xOyFgFnSc4	svůj
západní	západní	k2eAgFnSc4d1	západní
větví	větvit	k5eAaImIp3nP	větvit
středoamerickými	středoamerický	k2eAgFnPc7d1	středoamerická
republikami	republika	k1gFnPc7	republika
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
větví	větev	k1gFnSc7	větev
přes	přes	k7c4	přes
Velké	velká	k1gFnPc4	velká
a	a	k8xC	a
Malé	Malé	k2eAgFnPc1d1	Malé
Antily	Antily	k1gFnPc1	Antily
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihoamerické	jihoamerický	k2eAgFnSc6d1	jihoamerická
pevnině	pevnina	k1gFnSc6	pevnina
se	se	k3xPyFc4	se
Kordillery	Kordillery	k1gFnPc1	Kordillery
nazývají	nazývat	k5eAaImIp3nP	nazývat
Andy	Anda	k1gFnPc4	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
tři	tři	k4xCgNnPc4	tři
pásma	pásmo	k1gNnPc4	pásmo
<g/>
:	:	kIx,	:
Kordilleru	Kordiller	k1gInSc6	Kordiller
Západní	západní	k2eAgNnSc1d1	západní
<g/>
,	,	kIx,	,
Centrální	centrální	k2eAgFnSc1d1	centrální
a	a	k8xC	a
Východní	východní	k2eAgFnSc1d1	východní
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
4000	[number]	k4	4000
až	až	k9	až
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
Chimborazo	Chimboraza	k1gFnSc5	Chimboraza
6267	[number]	k4	6267
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
Cotopaxi	Cotopaxe	k1gFnSc4	Cotopaxe
5897	[number]	k4	5897
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
už	už	k6eAd1	už
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
dvě	dva	k4xCgNnPc1	dva
pásma	pásmo	k1gNnPc1	pásmo
<g/>
,	,	kIx,	,
uzavírající	uzavírající	k2eAgFnPc1d1	uzavírající
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
v	v	k7c4	v
Peru	Peru	k1gNnSc4	Peru
a	a	k8xC	a
Bolívii	Bolívie	k1gFnSc4	Bolívie
náhorní	náhorní	k2eAgFnSc4d1	náhorní
rovinu	rovina	k1gFnSc4	rovina
zvané	zvaný	k2eAgFnSc2d1	zvaná
puna	puna	k6eAd1	puna
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnPc1d1	západní
Kordillera	Kordiller	k1gMnSc2	Kordiller
zde	zde	k6eAd1	zde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
Huascaránem	Huascarán	k1gMnSc7	Huascarán
(	(	kIx(	(
<g/>
6768	[number]	k4	6768
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Východní	východní	k2eAgFnPc1d1	východní
Kordillera	Kordiller	k1gMnSc2	Kordiller
Ancohumou	Ancohuma	k1gMnSc7	Ancohuma
(	(	kIx(	(
<g/>
6550	[number]	k4	6550
m	m	kA	m
<g/>
)	)	kIx)	)
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Chile	Chile	k1gNnSc2	Chile
a	a	k8xC	a
Argentiny	Argentina	k1gFnSc2	Argentina
se	se	k3xPyFc4	se
obě	dva	k4xCgNnPc1	dva
pásma	pásmo	k1gNnPc1	pásmo
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
též	též	k9	též
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Ameriky	Amerika	k1gFnSc2	Amerika
Aconcagua	Aconcagua	k1gFnSc1	Aconcagua
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
6959	[number]	k4	6959
m	m	kA	m
<g/>
)	)	kIx)	)
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
se	se	k3xPyFc4	se
Kordillery	Kordillery	k1gFnPc1	Kordillery
snižují	snižovat	k5eAaImIp3nP	snižovat
na	na	k7c4	na
2000	[number]	k4	2000
až	až	k9	až
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
(	(	kIx(	(
<g/>
Patagonské	patagonský	k2eAgFnSc2d1	patagonská
Andy	Anda	k1gFnSc2	Anda
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
Ohňovou	ohňový	k2eAgFnSc4d1	ohňová
zemi	zem	k1gFnSc4	zem
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
do	do	k7c2	do
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
