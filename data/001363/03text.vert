<s>
Tereza	Tereza	k1gFnSc1	Tereza
Brodská	Brodská	k1gFnSc1	Brodská
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1968	[number]	k4	1968
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Své	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
dětské	dětský	k2eAgFnSc2d1	dětská
role	role	k1gFnSc2	role
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
filmu	film	k1gInSc6	film
Ať	ať	k9	ať
žijí	žít	k5eAaImIp3nP	žít
duchové	duch	k1gMnPc1	duch
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
sci-fi	scii	k1gNnSc1	sci-fi
<g/>
/	/	kIx~	/
<g/>
fantasy	fantas	k1gInPc1	fantas
snímku	snímek	k1gInSc2	snímek
Slečna	slečna	k1gFnSc1	slečna
Golem	Golem	k1gMnSc1	Golem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
držitelku	držitelka	k1gFnSc4	držitelka
filmového	filmový	k2eAgNnSc2d1	filmové
ocenění	ocenění	k1gNnSc2	ocenění
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
filmu	film	k1gInSc2	film
Konec	konec	k1gInSc4	konec
básníků	básník	k1gMnPc2	básník
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
z	z	k7c2	z
televizních	televizní	k2eAgInPc2d1	televizní
seriálů	seriál	k1gInPc2	seriál
Ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
Vlak	vlak	k1gInSc1	vlak
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
naděje	naděje	k1gFnSc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	Tyl	k1gMnSc2	Tyl
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
Činoherním	činoherní	k2eAgInSc6d1	činoherní
klubu	klub	k1gInSc6	klub
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
obdržela	obdržet	k5eAaPmAgFnS	obdržet
za	za	k7c4	za
roli	role	k1gFnSc4	role
Kláry	Klára	k1gFnSc2	Klára
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Má	mít	k5eAaImIp3nS	mít
je	být	k5eAaImIp3nS	být
pomsta	pomsta	k1gFnSc1	pomsta
Českého	český	k2eAgInSc2d1	český
lva	lev	k1gInSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Evalda	Evald	k1gMnSc2	Evald
Schorma	Schorm	k1gMnSc2	Schorm
Vlastně	vlastně	k9	vlastně
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roli	role	k1gFnSc4	role
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
její	její	k3xOp3gFnSc1	její
skutečná	skutečný	k2eAgFnSc1d1	skutečná
matka	matka	k1gFnSc1	matka
Jana	Jana	k1gFnSc1	Jana
Brejchová	Brejchová	k1gFnSc1	Brejchová
<g/>
.	.	kIx.	.
</s>
<s>
Dvojroli	dvojrole	k1gFnSc4	dvojrole
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
za	za	k7c2	za
níž	jenž	k3xRgFnSc2	jenž
obdržela	obdržet	k5eAaPmAgFnS	obdržet
druhé	druhý	k4xOgNnSc4	druhý
filmové	filmový	k2eAgNnSc4d1	filmové
ocenění	ocenění	k1gNnSc4	ocenění
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
herce	herec	k1gMnSc2	herec
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Brodského	Brodský	k1gMnSc2	Brodský
a	a	k8xC	a
herečky	herečka	k1gFnPc1	herečka
Jany	Jana	k1gFnSc2	Jana
Brejchové	Brejchová	k1gFnSc2	Brejchová
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
je	být	k5eAaImIp3nS	být
fotograf	fotograf	k1gMnSc1	fotograf
Herbert	Herbert	k1gMnSc1	Herbert
Slavík	Slavík	k1gMnSc1	Slavík
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc4	syn
Samuela	Samuel	k1gMnSc4	Samuel
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	tyla	k1gFnSc1	tyla
(	(	kIx(	(
<g/>
Velké	velký	k2eAgNnSc1d1	velké
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
))	))	k?	))
1986	[number]	k4	1986
-	-	kIx~	-
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
(	(	kIx(	(
<g/>
Nataša	Nataša	k1gFnSc1	Nataša
<g/>
,	,	kIx,	,
komtesa	komtesa	k1gFnSc1	komtesa
Rostovovová	Rostovovová	k1gFnSc1	Rostovovová
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
-	-	kIx~	-
Manon	Manon	k1gMnSc1	Manon
Lescaut	Lescaut	k1gMnSc1	Lescaut
(	(	kIx(	(
<g/>
Manon	Manon	k1gMnSc1	Manon
Lescaut	Lescaut	k1gMnSc1	Lescaut
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	tyla	k1gFnSc1	tyla
(	(	kIx(	(
<g/>
Komorní	komorní	k2eAgNnSc1d1	komorní
divadlo	divadlo	k1gNnSc1	divadlo
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Šejdíř	šejdíř	k1gMnSc1	šejdíř
(	(	kIx(	(
<g/>
Pieknowska	Pieknowska	k1gFnSc1	Pieknowska
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
-	-	kIx~	-
Obraz	obraz	k1gInSc1	obraz
Doriana	Dorian	k1gMnSc2	Dorian
Greye	Grey	k1gMnSc2	Grey
(	(	kIx(	(
<g/>
Sybila	Sybila	k1gFnSc1	Sybila
Vaneová	Vaneová	k1gFnSc1	Vaneová
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
(	(	kIx(	(
<g/>
Činoherní	činoherní	k2eAgInSc1d1	činoherní
klub	klub	k1gInSc1	klub
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
-	-	kIx~	-
Krásné	krásný	k2eAgFnSc2d1	krásná
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
(	(	kIx(	(
<g/>
Sára	Sára	k1gFnSc1	Sára
Caseyová	Caseyová	k1gFnSc1	Caseyová
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
1987	[number]	k4	1987
-	-	kIx~	-
Tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
(	(	kIx(	(
<g/>
Irina	Irina	k1gFnSc1	Irina
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1988	[number]	k4	1988
-	-	kIx~	-
Tatarská	tatarský	k2eAgFnSc1d1	tatarská
pouť	pouť	k1gFnSc1	pouť
(	(	kIx(	(
<g/>
Jitka	Jitka	k1gFnSc1	Jitka
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
-	-	kIx~	-
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
(	(	kIx(	(
<g/>
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
-	-	kIx~	-
Hvězdy	hvězda	k1gFnPc1	hvězda
na	na	k7c6	na
ranním	ranní	k2eAgNnSc6d1	ranní
nebi	nebe	k1gNnSc6	nebe
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
-	-	kIx~	-
Largo	largo	k6eAd1	largo
desolato	desolato	k6eAd1	desolato
(	(	kIx(	(
<g/>
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
1993	[number]	k4	1993
-	-	kIx~	-
Jak	jak	k8xC	jak
snadné	snadný	k2eAgNnSc1d1	snadné
je	být	k5eAaImIp3nS	být
vládnout	vládnout	k5eAaImF	vládnout
aneb	aneb	k?	aneb
Karel	Karel	k1gMnSc1	Karel
<g />
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
-	-	kIx~	-
autoportrét	autoportrét	k1gInSc1	autoportrét
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Fillippi	Fillipp	k1gFnSc2	Fillipp
<g/>
)	)	kIx)	)
Töpferova	Töpferův	k2eAgFnSc1d1	Töpferova
herecká	herecký	k2eAgFnSc1d1	herecká
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
Nádvoří	nádvoří	k1gNnSc1	nádvoří
Starého	Starého	k2eAgNnSc2d1	Starého
purkrabství	purkrabství	k1gNnSc2	purkrabství
-	-	kIx~	-
Letní	letní	k2eAgFnPc1d1	letní
shakespearovské	shakespearovský	k2eAgFnPc1d1	shakespearovská
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
-	-	kIx~	-
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
(	(	kIx(	(
<g/>
Julie	Julie	k1gFnSc1	Julie
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
Ungelt	ungelt	k1gInSc1	ungelt
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
-	-	kIx~	-
Jak	jak	k6eAd1	jak
vraždili	vraždit	k5eAaImAgMnP	vraždit
sestru	sestra	k1gFnSc4	sestra
Charlie	Charlie	k1gMnSc1	Charlie
(	(	kIx(	(
<g/>
Alice	Alice	k1gFnSc1	Alice
McNaughtová	McNaughtová	k1gFnSc1	McNaughtová
<g/>
)	)	kIx)	)
Divadlo	divadlo	k1gNnSc1	divadlo
Plus	plus	k1gNnSc1	plus
(	(	kIx(	(
<g/>
Rock	rock	k1gInSc1	rock
Café	café	k1gNnSc2	café
<g/>
)	)	kIx)	)
Od	od	k7c2	od
2015	[number]	k4	2015
-	-	kIx~	-
WTF	WTF	kA	WTF
</s>
