<s>
Borovany	Borovan	k1gMnPc4
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zámek	zámek	k1gInSc1
Borovany	Borovan	k1gMnPc7
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
barokní	barokní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Borovany	Borovan	k1gMnPc4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Žižkovo	Žižkův	k2eAgNnSc1d1
nám.	nám.	k?
Souřadnice	souřadnice	k1gFnPc1
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
54,6	54,6	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
30,98	30,98	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
34856	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Borovanský	Borovanský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgInSc1d1
augustiánský	augustiánský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
upravený	upravený	k2eAgInSc1d1
na	na	k7c4
zámek	zámek	k1gInSc4
v	v	k7c6
Borovanech	Borovan	k1gMnPc6
na	na	k7c6
Českobudějovicku	Českobudějovicko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stojí	stát	k5eAaImIp3nS
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
na	na	k7c6
Žižkově	Žižkův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
osmý	osmý	k4xOgInSc4
český	český	k2eAgInSc4d1
klášter	klášter	k1gInSc4
obývaný	obývaný	k2eAgInSc4d1
příslušníky	příslušník	k1gMnPc4
řehole	řehole	k1gFnSc2
svatého	svatý	k2eAgMnSc2d1
Augustina	Augustin	k1gMnSc2
a	a	k8xC
jeden	jeden	k4xCgInSc4
z	z	k7c2
prvních	první	k4xOgInPc2
klášterů	klášter	k1gInPc2
založených	založený	k2eAgInPc2d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
po	po	k7c6
skončení	skončení	k1gNnSc6
husitských	husitský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barokní	barokní	k2eAgInSc1d1
areál	areál	k1gInSc1
zámku	zámek	k1gInSc2
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xS,k8xC
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Klášterní	klášterní	k2eAgFnSc1d1
borůvková	borůvkový	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
</s>
<s>
Interiér	interiér	k1gInSc1
kláštera	klášter	k1gInSc2
</s>
<s>
Klášter	klášter	k1gInSc1
augustiniánů	augustinián	k1gMnPc2
kanovníků	kanovník	k1gMnPc2
založil	založit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1455	#num#	k4
Petr	Petr	k1gMnSc1
z	z	k7c2
Lindy	Linda	k1gFnSc2
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
služebník	služebník	k1gMnSc1
Rožmberků	Rožmberk	k1gMnPc2
<g/>
,	,	kIx,
povýšený	povýšený	k2eAgInSc4d1
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
do	do	k7c2
šlechtického	šlechtický	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
založit	založit	k5eAaPmF
nový	nový	k2eAgInSc4d1
klášter	klášter	k1gInSc4
jej	on	k3xPp3gMnSc4
vedlo	vést	k5eAaImAgNnS
úmrtí	úmrtí	k1gNnSc1
jediného	jediný	k2eAgMnSc2d1
syna	syn	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Borovanech	Borovan	k1gMnPc6
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
byl	být	k5eAaImAgInS
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Navštívení	navštívení	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
místě	místo	k1gNnSc6
vznikl	vzniknout	k5eAaPmAgInS
i	i	k9
augustiniánský	augustiniánský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
roku	rok	k1gInSc2
1455	#num#	k4
povolil	povolit	k5eAaPmAgMnS
král	král	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
stavbu	stavba	k1gFnSc4
kostela	kostel	k1gInSc2
a	a	k8xC
prozatímních	prozatímní	k2eAgFnPc2d1
klášterních	klášterní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Podle	podle	k7c2
Jaroslava	Jaroslav	k1gMnSc2
Kadlece	Kadlec	k1gMnSc2
však	však	k9
stavbu	stavba	k1gFnSc4
povolil	povolit	k5eAaPmAgInS
až	až	k6eAd1
20	#num#	k4
<g/>
.	.	kIx.
<g/>
&	&	k?
<g/>
října	říjen	k1gInSc2
1455	#num#	k4
místodržící	místodržící	k1gMnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1458	#num#	k4
<g/>
,	,	kIx,
potvrdil	potvrdit	k5eAaPmAgInS
založení	založení	k1gNnSc4
nové	nový	k2eAgFnSc2d1
církevní	církevní	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
papež	papež	k1gMnSc1
Pius	Pius	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
stavby	stavba	k1gFnSc2
byla	být	k5eAaImAgFnS
hotova	hotov	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1461	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
dílo	dílo	k1gNnSc4
podunajské	podunajský	k2eAgFnSc2d1
rožmberské	rožmberský	k2eAgFnSc2d1
huti	huť	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jména	jméno	k1gNnPc1
stavitelů	stavitel	k1gMnPc2
nejsou	být	k5eNaImIp3nP
známa	známo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klášter	klášter	k1gInSc1
převzal	převzít	k5eAaPmAgInS
do	do	k7c2
svého	svůj	k3xOyFgInSc2
znaku	znak	k1gInSc2
Lindův	Lindův	k2eAgInSc1d1
erb	erb	k1gInSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
černo-zlatého	černo-zlatý	k2eAgInSc2d1
štípeného	štípený	k2eAgInSc2d1
štítu	štít	k1gInSc2
a	a	k8xC
na	na	k7c6
něm	on	k3xPp3gInSc6
zelenou	zelený	k2eAgFnSc4d1
lipovou	lipový	k2eAgFnSc4d1
ratolest	ratolest	k1gFnSc4
se	s	k7c7
dvěma	dva	k4xCgInPc7
listy	list	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
klášteru	klášter	k1gInSc3
patřilo	patřit	k5eAaImAgNnS
mnoho	mnoho	k4c4
pozemků	pozemek	k1gInPc2
<g/>
:	:	kIx,
ves	ves	k1gFnSc1
Hluboká	Hluboká	k1gFnSc1
a	a	k8xC
Nesměň	směnit	k5eNaPmRp2nS
<g/>
,	,	kIx,
les	les	k1gInSc1
Kuchyňka	kuchyňka	k1gFnSc1
a	a	k8xC
Hájek	Hájek	k1gMnSc1
a	a	k8xC
sedm	sedm	k4xCc4
rybníků	rybník	k1gInPc2
s	s	k7c7
mlýnem	mlýn	k1gInSc7
a	a	k8xC
částí	část	k1gFnSc7
říčky	říčka	k1gFnSc2
Stropnice	stropnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Petrově	Petrův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
připadly	připadnout	k5eAaPmAgInP
klášteru	klášter	k1gInSc3
ještě	ještě	k9
další	další	k2eAgInSc4d1
pozemky	pozemek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhlas	souhlas	k1gInSc1
se	s	k7c7
založením	založení	k1gNnSc7
kláštera	klášter	k1gInSc2
udělil	udělit	k5eAaPmAgMnS
a	a	k8xC
předchozí	předchozí	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
a	a	k8xC
papežské	papežský	k2eAgFnSc2d1
výsady	výsada	k1gFnSc2
potvrdil	potvrdit	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1461	#num#	k4
písemně	písemně	k6eAd1
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
1461	#num#	k4
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
Borovan	Borovan	k1gMnSc1
probošt	probošt	k1gMnSc1
Zikmund	Zikmund	k1gMnSc1
(	(	kIx(
<g/>
+	+	kIx~
před	před	k7c7
28.1	28.1	k4
<g/>
.1464	.1464	k4
<g/>
)	)	kIx)
s	s	k7c7
šesti	šest	k4xCc7
bratry	bratr	k1gMnPc7
augustiniány-kanovníky	augustiniány-kanovník	k1gMnPc4
a	a	k8xC
založili	založit	k5eAaPmAgMnP
první	první	k4xOgFnSc4
tamní	tamní	k2eAgFnSc4d1
kanonii	kanonie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišli	přijít	k5eAaPmAgMnP
z	z	k7c2
kanonie	kanonie	k1gFnSc2
v	v	k7c6
Třeboni	Třeboň	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
řád	řád	k1gInSc1
působil	působit	k5eAaImAgInS
pod	pod	k7c7
patronátem	patronát	k1gInSc7
Rožmberků	Rožmberk	k1gInPc2
skoro	skoro	k6eAd1
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysvěcení	vysvěcení	k1gNnSc1
svatostánku	svatostánek	k1gInSc2
v	v	k7c6
kostele	kostel	k1gInSc6
Navštívení	navštívení	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1464	#num#	k4
za	za	k7c4
probošta	probošt	k1gMnSc4
Valentina	Valentin	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavební	stavební	k2eAgFnPc4d1
práce	práce	k1gFnPc4
skončily	skončit	k5eAaPmAgFnP
až	až	k6eAd1
na	na	k7c6
sklonku	sklonek	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Počátek	počátek	k1gInSc1
života	život	k1gInSc2
borovanské	borovanský	k2eAgFnSc2d1
klášterní	klášterní	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
byl	být	k5eAaImAgInS
naplněn	naplnit	k5eAaPmNgInS
prosperitou	prosperita	k1gFnSc7
<g/>
,	,	kIx,
klidný	klidný	k2eAgInSc4d1
a	a	k8xC
beze	beze	k7c2
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začaly	začít	k5eAaPmAgFnP
roztržky	roztržka	k1gFnPc1
mezi	mezi	k7c7
proboštem	probošt	k1gMnSc7
Jakubem	Jakub	k1gMnSc7
(	(	kIx(
<g/>
1508	#num#	k4
<g/>
-	-	kIx~
<g/>
1517	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
řádovými	řádový	k2eAgMnPc7d1
bratry	bratr	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klášter	klášter	k1gInSc1
začal	začít	k5eAaPmAgInS
pomalu	pomalu	k6eAd1
upadat	upadat	k5eAaPmF,k5eAaImF
kvůli	kvůli	k7c3
dluhům	dluh	k1gInPc3
<g/>
,	,	kIx,
sporům	spor	k1gInPc3
se	se	k3xPyFc4
sousední	sousední	k2eAgFnSc7d1
šlechtou	šlechta	k1gFnSc7
a	a	k8xC
šířením	šíření	k1gNnSc7
protestantské	protestantský	k2eAgFnSc2d1
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
polovině	polovina	k1gFnSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vymřeli	vymřít	k5eAaPmAgMnP
řádoví	řádový	k2eAgMnPc1d1
bratři	bratr	k1gMnPc1
na	na	k7c4
mor	mor	k1gInSc4
a	a	k8xC
roku	rok	k1gInSc2
1577	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
i	i	k9
šestý	šestý	k4xOgMnSc1
probošt	probošt	k1gMnSc1
Šimon	Šimon	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
z	z	k7c2
Rožmberka	Rožmberk	k1gInSc2
dosadil	dosadit	k5eAaPmAgMnS
do	do	k7c2
čela	čelo	k1gNnSc2
kláštera	klášter	k1gInSc2
prozatímního	prozatímní	k2eAgMnSc2d1
správce	správce	k1gMnSc2
Matěje	Matěj	k1gMnSc2
Kozku	kozka	k1gFnSc4
z	z	k7c2
Rynárce	Rynárka	k1gFnSc6
(	(	kIx(
<g/>
1558	#num#	k4
<g/>
–	–	k?
<g/>
1564	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yRgMnSc3,k3yIgMnSc3
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
ekonomický	ekonomický	k2eAgInSc4d1
úpadek	úpadek	k1gInSc4
kláštera	klášter	k1gInSc2
zastavit	zastavit	k5eAaPmF
a	a	k8xC
přestal	přestat	k5eAaPmAgMnS
přijímat	přijímat	k5eAaImF
nové	nový	k2eAgMnPc4d1
bratry	bratr	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
byl	být	k5eAaImAgInS
klášter	klášter	k1gInSc1
v	v	k7c6
zoufalém	zoufalý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
ho	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1564	#num#	k4
zrušil	zrušit	k5eAaPmAgInS
a	a	k8xC
ujal	ujmout	k5eAaPmAgInS
se	se	k3xPyFc4
správy	správa	k1gFnSc2
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
situace	situace	k1gFnSc1
lehce	lehko	k6eAd1
zlepšovat	zlepšovat	k5eAaImF
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1630	#num#	k4
podnikl	podniknout	k5eAaPmAgMnS
císař	císař	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
první	první	k4xOgInPc4
kroky	krok	k1gInPc4
k	k	k7c3
obnovení	obnovení	k1gNnSc3
borovanského	borovanský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1630	#num#	k4
uzavřel	uzavřít	k5eAaPmAgInS
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
proboštem	probošt	k1gMnSc7
kláštera	klášter	k1gInSc2
v	v	k7c6
Klosterneuburgu	Klosterneuburg	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
odtamtud	odtamtud	k6eAd1
budou	být	k5eAaImBp3nP
vysláni	vyslat	k5eAaPmNgMnP
dva	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
do	do	k7c2
Borovan	Borovan	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1651	#num#	k4
se	se	k3xPyFc4
dokončily	dokončit	k5eAaPmAgFnP
opravy	oprava	k1gFnPc1
budov	budova	k1gFnPc2
a	a	k8xC
o	o	k7c4
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
déle	dlouho	k6eAd2
přišli	přijít	k5eAaPmAgMnP
do	do	k7c2
kláštera	klášter	k1gInSc2
augustiniánští	augustiniánský	k2eAgMnPc1d1
bratři	bratr	k1gMnPc1
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1662	#num#	k4
byl	být	k5eAaImAgMnS
dosazen	dosazen	k2eAgMnSc1d1
probošt	probošt	k1gMnSc1
Georga	Georg	k1gMnSc2
Janda	Janda	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1670	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Augustiniánská	augustiniánský	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
dvorským	dvorský	k2eAgInSc7d1
dekretem	dekret	k1gInSc7
císaře	císař	k1gMnSc2
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1785	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
přeměněn	přeměnit	k5eAaPmNgInS
na	na	k7c4
farní	farní	k2eAgNnSc4d1
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
potvrzení	potvrzení	k1gNnSc4
prvního	první	k4xOgMnSc2
borovanského	borovanský	k2eAgMnSc2d1
faráře	farář	k1gMnSc2
Schwingenschlegla	Schwingenschlegl	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1787	#num#	k4
pozemky	pozemek	k1gInPc4
<g/>
,	,	kIx,
budovy	budova	k1gFnPc4
a	a	k8xC
celý	celý	k2eAgInSc4d1
majetek	majetek	k1gInSc4
koupil	koupit	k5eAaPmAgMnS
kníže	kníže	k1gMnSc1
Jan	Jan	k1gMnSc1
ze	z	k7c2
Schwarzenbergu	Schwarzenberg	k1gInSc2
a	a	k8xC
prelaturu	prelatura	k1gFnSc4
přestavěl	přestavět	k5eAaPmAgMnS
na	na	k7c4
zámek	zámek	k1gInSc4
–	–	k?
letní	letní	k2eAgFnSc6d1
rezidenci	rezidence	k1gFnSc6
dvého	dvéze	k6eAd1
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prodej	prodej	k1gInSc1
zámku	zámek	k1gInSc2
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Při	při	k7c6
pozemkové	pozemkový	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
schwarzenberský	schwarzenberský	k2eAgInSc4d1
majetek	majetek	k1gInSc4
v	v	k7c6
Borovanech	Borovan	k1gMnPc6
rozparcelován	rozparcelován	k2eAgMnSc1d1
a	a	k8xC
rozprodán	rozprodán	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
projevilo	projevit	k5eAaPmAgNnS
zájem	zájem	k1gInSc4
o	o	k7c4
prodej	prodej	k1gInSc4
několik	několik	k4yIc4
možných	možný	k2eAgMnPc2d1
kupců	kupec	k1gMnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
zástupci	zástupce	k1gMnPc1
Ústředního	ústřední	k2eAgInSc2d1
svazu	svaz	k1gInSc2
nemocenských	nemocenský	k2eAgFnPc2d1,k2eNgFnPc2d1
pokladen	pokladna	k1gFnPc2
<g/>
,	,	kIx,
štábní	štábní	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
Musílek	Musílek	k1gMnSc1
nebo	nebo	k8xC
cestoval	cestovat	k5eAaImAgMnS
Joe	Joe	k1gMnSc4
Hloucha	Hlouch	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámek	zámek	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
dobrém	dobrý	k2eAgInSc6d1
udržovaném	udržovaný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
tehdejších	tehdejší	k2eAgInPc6d1
popisech	popis	k1gInPc6
se	se	k3xPyFc4
zmiňovalo	zmiňovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
měl	mít	k5eAaImAgMnS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
místnostech	místnost	k1gFnPc6
podlahy	podlaha	k1gFnSc2
a	a	k8xC
kamna	kamna	k1gNnPc4
<g/>
,	,	kIx,
pod	pod	k7c7
střechou	střecha	k1gFnSc7
zachovalý	zachovalý	k2eAgInSc1d1
krov	krov	k1gInSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
částečně	částečně	k6eAd1
podsklepen	podsklepen	k2eAgInSc1d1
a	a	k8xC
patřila	patřit	k5eAaImAgFnS
k	k	k7c3
němu	on	k3xPp3gMnSc3
velká	velký	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nejvážnějším	vážní	k2eAgMnSc7d3
zájemcem	zájemce	k1gMnSc7
byl	být	k5eAaImAgMnS
Joe	Joe	k1gMnSc4
Hloucha	Hlouch	k1gMnSc4
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
jméno	jméno	k1gNnSc1
připomíná	připomínat	k5eAaImIp3nS
pozdějšího	pozdní	k2eAgMnSc2d2
budějovického	budějovický	k2eAgMnSc2d1
biskupa	biskup	k1gMnSc2
Josefa	Josef	k1gMnSc2
Hloucha	Hlouch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
osoby	osoba	k1gFnPc1
však	však	k9
nemají	mít	k5eNaImIp3nP
nic	nic	k3yNnSc4
společného	společný	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hloucha	Hloucha	k1gFnSc1
byl	být	k5eAaImAgInS
známý	známý	k2eAgInSc1d1
svým	svůj	k3xOyFgInSc7
zájmem	zájem	k1gInSc7
o	o	k7c4
asijské	asijský	k2eAgNnSc4d1
umění	umění	k1gNnSc4
a	a	k8xC
zejména	zejména	k9
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
procestoval	procestovat	k5eAaPmAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1906	#num#	k4
a	a	k8xC
1926	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nashromáždil	nashromáždit	k5eAaPmAgMnS
velké	velký	k2eAgFnPc4d1
sbírky	sbírka	k1gFnPc4
uměleckých	umělecký	k2eAgInPc2d1
a	a	k8xC
historických	historický	k2eAgInPc2d1
předmět	předmět	k1gInSc4
a	a	k8xC
ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
intenzivně	intenzivně	k6eAd1
řešil	řešit	k5eAaImAgInS
prostorové	prostorový	k2eAgNnSc4d1
umístění	umístění	k1gNnSc4
svých	svůj	k3xOyFgInPc2
neustále	neustále	k6eAd1
se	se	k3xPyFc4
rozrůstajících	rozrůstající	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámek	zámek	k1gInSc1
v	v	k7c6
Borovanech	Borovan	k1gMnPc6
byl	být	k5eAaImAgMnS
pro	pro	k7c4
něho	on	k3xPp3gMnSc4
vhodným	vhodný	k2eAgInSc7d1
objektem	objekt	k1gInSc7
<g/>
,	,	kIx,
proto	proto	k8xC
si	se	k3xPyFc3
v	v	k7c6
polovině	polovina	k1gFnSc6
února	únor	k1gInSc2
1924	#num#	k4
přijel	přijet	k5eAaPmAgInS
osobně	osobně	k6eAd1
celý	celý	k2eAgInSc4d1
objekt	objekt	k1gInSc4
prohlédnout	prohlédnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okamžitém	okamžitý	k2eAgInSc6d1
prodeji	prodej	k1gInSc6
celého	celý	k2eAgInSc2d1
objektu	objekt	k1gInSc2
bránilo	bránit	k5eAaImAgNnS
nedořešené	dořešený	k2eNgNnSc1d1
vystěhování	vystěhování	k1gNnSc1
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
provozů	provoz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bydleli	bydlet	k5eAaImAgMnP
zde	zde	k6eAd1
lesní	lesní	k2eAgMnPc1d1
správce	správce	k1gMnSc1
<g/>
,	,	kIx,
nájemce	nájemce	k1gMnSc1
dvora	dvůr	k1gInSc2
<g/>
,	,	kIx,
kočí	kočí	k1gMnSc1
<g/>
,	,	kIx,
vrátný	vrátný	k1gMnSc1
a	a	k8xC
nacházely	nacházet	k5eAaImAgFnP
se	se	k3xPyFc4
zde	zde	k6eAd1
kanceláře	kancelář	k1gFnSc2
v	v	k7c6
nájmu	nájem	k1gInSc6
lesní	lesní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
cihelny	cihelna	k1gFnPc4
a	a	k8xC
cvičební	cvičební	k2eAgFnSc4d1
místnost	místnost	k1gFnSc4
Orla	Orel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prodeji	prodej	k1gInSc3
slavnému	slavný	k2eAgMnSc3d1
cestovateli	cestovatel	k1gMnSc3
nakonec	nakonec	k6eAd1
nikdy	nikdy	k6eAd1
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpověď	odpověď	k1gFnSc1
nenabízí	nabízet	k5eNaImIp3nS
ani	ani	k8xC
nalezené	nalezený	k2eAgInPc1d1
archivní	archivní	k2eAgInPc1d1
prameny	pramen	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
1924	#num#	k4
si	se	k3xPyFc3
Joe	Joe	k1gMnSc1
Hloucha	Hlouch	k1gMnSc4
pořídil	pořídit	k5eAaPmAgMnS
vilu	vila	k1gFnSc4
v	v	k7c6
Roztokách	roztoka	k1gFnPc6
za	za	k7c7
severním	severní	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
upravil	upravit	k5eAaPmAgMnS
v	v	k7c6
japonském	japonský	k2eAgInSc6d1
stylu	styl	k1gInSc6
a	a	k8xC
nazýval	nazývat	k5eAaImAgInS
ji	on	k3xPp3gFnSc4
Sakura	sakura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zámek	zámek	k1gInSc1
tak	tak	k9
zůstal	zůstat	k5eAaPmAgInS
v	v	k7c6
majetku	majetek	k1gInSc6
Schwarzenbergů	Schwarzenberg	k1gInPc2
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1939	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ho	on	k3xPp3gNnSc4
koupila	koupit	k5eAaPmAgFnS
borovanská	borovanský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prostorách	prostora	k1gFnPc6
zámku	zámek	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1940	#num#	k4
až	až	k6eAd1
1997	#num#	k4
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Areál	areál	k1gInSc1
kláštera	klášter	k1gInSc2
</s>
<s>
Prelatura	prelatura	k1gFnSc1
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1
prelatura	prelatura	k1gFnSc1
byla	být	k5eAaImAgFnS
vystavěna	vystavěn	k2eAgFnSc1d1
roku	rok	k1gInSc2
1765	#num#	k4
<g/>
–	–	k?
<g/>
1768	#num#	k4
za	za	k7c4
probošta	probošt	k1gMnSc4
Augustina	Augustin	k1gMnSc4
Dubenského	Dubenský	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
vjezdem	vjezd	k1gInSc7
do	do	k7c2
budovy	budova	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
rodový	rodový	k2eAgInSc1d1
erb	erb	k1gInSc1
Schwarzenbergů	Schwarzenberg	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
tam	tam	k6eAd1
umístili	umístit	k5eAaPmAgMnP
<g/>
,	,	kIx,
když	když	k8xS
klášter	klášter	k1gInSc4
vlastnili	vlastnit	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prelatura	prelatura	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
bydlení	bydlení	k1gNnSc3
nájemci	nájemce	k1gMnPc1
a	a	k8xC
správci	správce	k1gMnPc1
klášterního	klášterní	k2eAgInSc2d1
statku	statek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
</s>
<s>
Kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
první	první	k4xOgFnSc4
část	část	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
při	při	k7c6
stavění	stavění	k1gNnSc6
kláštera	klášter	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1455	#num#	k4
budovat	budovat	k5eAaImF
na	na	k7c6
základech	základ	k1gInPc6
farního	farní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
dokončena	dokončen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1464	#num#	k4
za	za	k7c4
probošta	probošt	k1gMnSc4
Valentina	Valentin	k1gMnSc4
(	(	kIx(
<g/>
vládl	vládnout	k5eAaImAgMnS
1464	#num#	k4
<g/>
-	-	kIx~
<g/>
1479	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1701	#num#	k4
nastoupil	nastoupit	k5eAaPmAgMnS
probošt	probošt	k1gMnSc1
Christian	Christian	k1gMnSc1
Preitfelder	Preitfelder	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
požádal	požádat	k5eAaPmAgMnS
pražského	pražský	k2eAgMnSc4d1
arcibiskupa	arcibiskup	k1gMnSc4
o	o	k7c4
subvence	subvence	k1gFnPc4
ze	z	k7c2
solní	solní	k2eAgFnSc2d1
pokladny	pokladna	k1gFnSc2
na	na	k7c4
renovaci	renovace	k1gFnSc4
klášterních	klášterní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
a	a	k8xC
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1721	#num#	k4
pořídil	pořídit	k5eAaPmAgMnS
do	do	k7c2
kostela	kostel	k1gInSc2
nové	nový	k2eAgInPc4d1
varhany	varhany	k1gInPc4
a	a	k8xC
barokizoval	barokizovat	k5eAaImAgMnS
interiér	interiér	k1gInSc4
se	s	k7c7
Škapulířovou	škapulířový	k2eAgFnSc7d1
kaplí	kaple	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1729	#num#	k4
na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
kostela	kostel	k1gInSc2
barokizoval	barokizovat	k5eAaImAgInS
věž	věž	k1gFnSc4
cibulovitou	cibulovitý	k2eAgFnSc4d1
bání	báně	k1gFnSc7
a	a	k8xC
pořídil	pořídit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgInPc4
zvony	zvon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
probošta	probošt	k1gMnSc2
Augustina	Augustin	k1gMnSc2
Dubenského	Dubenský	k2eAgMnSc2d1
(	(	kIx(
<g/>
17384	#num#	k4
<g/>
-	-	kIx~
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ke	k	k7c3
kostelu	kostel	k1gInSc3
přičleněn	přičleněn	k2eAgInSc1d1
presbytář	presbytář	k1gInSc1
s	s	k7c7
freskami	freska	k1gFnPc7
Františka	František	k1gMnSc2
Jakuba	Jakub	k1gMnSc2
v	v	k7c6
kupoli	kupole	k1gFnSc6
a	a	k8xC
krypta	krypta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Konvent	konvent	k1gInSc1
kláštera	klášter	k1gInSc2
</s>
<s>
Po	po	k7c6
stavbě	stavba	k1gFnSc6
kostela	kostel	k1gInSc2
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
stavět	stavět	k5eAaImF
klášter	klášter	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
za	za	k7c2
probošta	probošt	k1gMnSc2
Prokopa	Prokop	k1gMnSc2
v	v	k7c6
době	doba	k1gFnSc6
prosperity	prosperita	k1gFnSc2
křížovou	křížový	k2eAgFnSc7d1
chodbou	chodba	k1gFnSc7
kolem	kolem	k7c2
roku	rok	k1gInSc2
1490	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
chodbě	chodba	k1gFnSc6
nachází	nacházet	k5eAaImIp3nS
lapidárium	lapidárium	k1gNnSc1
barokních	barokní	k2eAgFnPc2d1
kamenných	kamenný	k2eAgFnPc2d1
soch	socha	k1gFnPc2
včetně	včetně	k7c2
originálu	originál	k1gInSc2
Mariánského	mariánský	k2eAgInSc2d1
sloupu	sloup	k1gInSc2
z	z	k7c2
Českých	český	k2eAgInPc2d1
Budějovic	Budějovice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
patře	patro	k1gNnSc6
je	být	k5eAaImIp3nS
instalováno	instalovat	k5eAaBmNgNnS
klášterní	klášterní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
fara	fara	k1gFnSc1
a	a	k8xC
shromažďovací	shromažďovací	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
fary	fara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kaple	kaple	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1747	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
místě	místo	k1gNnSc6
gotické	gotický	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Karmelské	Karmelský	k2eAgFnSc2d1
vystavěna	vystavěn	k2eAgFnSc1d1
Škapulířová	škapulířový	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
ve	v	k7c6
slohu	sloh	k1gInSc6
rokoka	rokoko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pověst	pověst	k1gFnSc1
</s>
<s>
Během	během	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
proběhly	proběhnout	k5eAaPmAgFnP
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1619	#num#	k4
boje	boj	k1gInSc2
i	i	k9
v	v	k7c6
Borovanech	Borovan	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Traduje	tradovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
vojáci	voják	k1gMnPc1
zpustošili	zpustošit	k5eAaPmAgMnP
kostel	kostel	k1gInSc4
a	a	k8xC
odvezli	odvézt	k5eAaPmAgMnP
odtud	odtud	k6eAd1
tři	tři	k4xCgInPc4
největší	veliký	k2eAgInPc4d3
zvony	zvon	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
prodali	prodat	k5eAaPmAgMnP
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
ze	z	k7c2
zvonů	zvon	k1gInPc2
zlodějům	zloděj	k1gMnPc3
spadl	spadnout	k5eAaPmAgInS
z	z	k7c2
vozu	vůz	k1gInSc2
a	a	k8xC
potopil	potopit	k5eAaPmAgInS
se	se	k3xPyFc4
hluboko	hluboko	k6eAd1
v	v	k7c6
bahně	bahno	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
zanechal	zanechat	k5eAaPmAgMnS
bezednou	bezedný	k2eAgFnSc4d1
studánku	studánka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říkalo	říkat	k5eAaImAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
Haltýř	haltýř	k1gInSc4
a	a	k8xC
skutečně	skutečně	k6eAd1
existovala	existovat	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nynější	nynější	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1
zámku	zámek	k1gInSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
RokPočet	RokPočet	k1gInSc1
návštěvníků	návštěvník	k1gMnPc2
</s>
<s>
20153	#num#	k4
724	#num#	k4
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
</s>
<s>
20173	#num#	k4
510	#num#	k4
</s>
<s>
2020	#num#	k4
</s>
<s>
8600	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2508	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Zrekonstruovaný	zrekonstruovaný	k2eAgInSc1d1
zámek	zámek	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
opět	opět	k6eAd1
otevřen	otevřít	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
areálu	areál	k1gInSc6
zámku	zámek	k1gInSc2
se	se	k3xPyFc4
nyní	nyní	k6eAd1
nachází	nacházet	k5eAaImIp3nS
kavárna	kavárna	k1gFnSc1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
borůvková	borůvkový	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
vysazeno	vysadit	k5eAaPmNgNnS
osm	osm	k4xCc1
druhů	druh	k1gInPc2
kanadských	kanadský	k2eAgFnPc2d1
borůvek	borůvka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
také	také	k9
bylinková	bylinkový	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
<g/>
,	,	kIx,
knihovna	knihovna	k1gFnSc1
Augustina	Augustin	k1gMnSc2
Dubenského	Dubenský	k2eAgMnSc2d1
nebo	nebo	k8xC
kinosál	kinosál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
rekonstrukce	rekonstrukce	k1gFnSc2
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
zámku	zámek	k1gInSc6
každý	každý	k3xTgInSc4
měsíc	měsíc	k1gInSc4
řada	řada	k1gFnSc1
kulturních	kulturní	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
výstavy	výstava	k1gFnPc4
<g/>
,	,	kIx,
koncerty	koncert	k1gInPc4
a	a	k8xC
každoroční	každoroční	k2eAgInPc4d1
borůvkové	borůvkový	k2eAgInPc4d1
trhy	trh	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
146595	#num#	k4
:	:	kIx,
Zámek	zámek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
KADLEC	Kadlec	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forbes-Borovany	Forbes-Borovan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
RÖHRIG	RÖHRIG	kA
<g/>
,	,	kIx,
Floridus	Floridus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Stifte	Stift	k1gInSc5
der	drát	k5eAaImRp2nS
Augustiner	Augustiner	k1gMnSc1
Chorherren	Chorherrna	k1gFnPc2
in	in	k?
Böhmen	Böhmen	k1gInSc1
<g/>
,	,	kIx,
Mähren	Mährna	k1gFnPc2
und	und	k?
Ungarn	Ungarna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klostenburg	Klostenburg	k1gMnSc1
<g/>
:	:	kIx,
Mayer	Mayer	k1gMnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
901025	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
49	#num#	k4
-	-	kIx~
64	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
KOVÁŘ	Kovář	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borovany	Borovan	k1gMnPc7
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Okresní	okresní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
94	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
238	#num#	k4
<g/>
-	-	kIx~
<g/>
5511	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
8	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠEDA	Šeda	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matěj	Matěj	k1gMnSc1
Kozka	kozka	k1gFnSc1
z	z	k7c2
Rynárce	Rynárka	k1gFnSc3
a	a	k8xC
sekularizace	sekularizace	k1gFnSc1
borovanské	borovanský	k2eAgFnSc2d1
augustiniánské	augustiniánský	k2eAgFnSc2d1
řeholní	řeholní	k2eAgFnSc2d1
kanonie	kanonie	k1gFnSc2
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihočeský	jihočeský	k2eAgInSc1d1
sborník	sborník	k1gInSc1
historický	historický	k2eAgInSc1d1
<g/>
.	.	kIx.
1968	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
37	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
87	#num#	k4
<g/>
–	–	k?
<g/>
97	#num#	k4
<g/>
,	,	kIx,
133	#num#	k4
<g/>
–	–	k?
<g/>
143	#num#	k4
<g/>
,	,	kIx,
225	#num#	k4
<g/>
–	–	k?
<g/>
238	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CUKR	cukr	k1gInSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xC,k8xS
slavný	slavný	k2eAgMnSc1d1
cestovatel	cestovatel	k1gMnSc1
nekoupil	koupit	k5eNaPmAgMnS
borovanský	borovanský	k2eAgInSc4d1
zámek	zámek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borovanský	Borovanský	k2eAgInSc1d1
zpravodaj	zpravodaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leden	leden	k1gInSc1
2021	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŽÁČEK	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověsti	pověst	k1gFnSc2
a	a	k8xC
paměti	paměť	k1gFnSc2
městečka	městečko	k1gNnSc2
Borovan	Borovan	k1gMnSc1
u	u	k7c2
Trocnova	Trocnov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
vlastním	vlastní	k2eAgInSc7d1
nákladem	náklad	k1gInSc7
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
.	.	kIx.
46	#num#	k4
s.	s.	k?
↑	↑	k?
Návštěvnost	návštěvnost	k1gFnSc1
památek	památka	k1gFnPc2
v	v	k7c6
krajích	kraj	k1gInPc6
ČR	ČR	kA
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
[	[	kIx(
<g/>
PDF	PDF	kA
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
a	a	k8xC
poradenské	poradenský	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
HLUŠTÍKOVÁ	HLUŠTÍKOVÁ	kA
<g/>
,	,	kIx,
Magdalena	Magdalena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěvnost	návštěvnost	k1gFnSc1
kláštera	klášter	k1gInSc2
aneb	aneb	k?
prohlídky	prohlídka	k1gFnSc2
mezi	mezi	k7c7
nebem	nebe	k1gNnSc7
a	a	k8xC
peklem	peklo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borovanský	Borovanský	k2eAgInSc1d1
zpravodaj	zpravodaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listopad	listopad	k1gInSc1
2020	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náštěvnost	Náštěvnost	k1gFnSc1
všech	všecek	k3xTgFnPc2
placených	placený	k2eAgFnPc2d1
expozic	expozice	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
↑	↑	k?
HLUŠTÍKOVÁ	HLUŠTÍKOVÁ	kA
<g/>
,	,	kIx,
Magdalena	Magdalena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěva	návštěva	k1gFnSc1
kláštera	klášter	k1gInSc2
aneb	aneb	k?
prohlídky	prohlídka	k1gFnSc2
mezi	mezi	k7c7
nebem	nebe	k1gNnSc7
a	a	k8xC
peklem	peklo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borovanský	Borovanský	k2eAgInSc1d1
zpravodaj	zpravodaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listopad	listopad	k1gInSc1
2020	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěvnost	návštěvnost	k1gFnSc1
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
↑	↑	k?
Současnost	současnost	k1gFnSc4
klášterního	klášterní	k2eAgInSc2d1
areálu	areál	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Borovansko	Borovansko	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KADLEC	Kadlec	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forbes-Borovany	Forbes-Borovan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
RÖHRIG	RÖHRIG	kA
<g/>
,	,	kIx,
Floridus	Floridus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Stifte	Stift	k1gInSc5
der	drát	k5eAaImRp2nS
Augustiner	Augustiner	k1gMnSc1
Chorherren	Chorherrna	k1gFnPc2
in	in	k?
Böhmen	Böhmen	k1gInSc1
<g/>
,	,	kIx,
Mähren	Mährna	k1gFnPc2
und	und	k?
Ungarn	Ungarna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klostenburg	Klostenburg	k1gMnSc1
<g/>
:	:	kIx,
Mayer	Mayer	k1gMnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
901025	#num#	k4
<g/>
-	-	kIx~
<g/>
34	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
49	#num#	k4
<g/>
–	–	k?
<g/>
64	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SOMMER	Sommer	k1gMnSc1
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
VLČEK	Vlček	k1gMnSc1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
FOLTÝN	Foltýn	k1gMnSc1
Dušan	Dušan	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
českých	český	k2eAgInPc2d1
klášterů	klášter	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libri	Libri	k1gNnSc7
Praha	Praha	k1gFnSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
181-182	181-182	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Zámek	zámek	k1gInSc1
Borovany	Borovan	k1gMnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Klášter	klášter	k1gInSc1
Borovany	Borovan	k1gMnPc7
</s>
<s>
Současnost	současnost	k1gFnSc1
klášterního	klášterní	k2eAgInSc2d1
areálu	areál	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Historie	historie	k1gFnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2013778461	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
305218015	#num#	k4
</s>
