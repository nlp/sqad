<s>
Vilém	Vilém	k1gMnSc1
z	z	k7c2
Rožmberka	Rožmberk	k1gInSc2
dosadil	dosadit	k5eAaPmAgMnS
do	do	k7c2
čela	čelo	k1gNnSc2
kláštera	klášter	k1gInSc2
prozatímního	prozatímní	k2eAgMnSc2d1
správce	správce	k1gMnSc2
Matěje	Matěj	k1gMnSc2
Kozku	kozka	k1gFnSc4
z	z	k7c2
Rynárce	Rynárka	k1gFnSc6
(	(	kIx(
<g/>
1558	#num#	k4
<g/>
–	–	k?
<g/>
1564	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yRgMnSc3,k3yIgMnSc3
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
ekonomický	ekonomický	k2eAgInSc4d1
úpadek	úpadek	k1gInSc4
kláštera	klášter	k1gInSc2
zastavit	zastavit	k5eAaPmF
a	a	k8xC
přestal	přestat	k5eAaPmAgMnS
přijímat	přijímat	k5eAaImF
nové	nový	k2eAgMnPc4d1
bratry	bratr	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
byl	být	k5eAaImAgInS
klášter	klášter	k1gInSc1
v	v	k7c6
zoufalém	zoufalý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
ho	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1564	#num#	k4
zrušil	zrušit	k5eAaPmAgInS
a	a	k8xC
ujal	ujmout	k5eAaPmAgInS
se	se	k3xPyFc4
správy	správa	k1gFnSc2
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>