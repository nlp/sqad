<s>
Gobi	Gobi	k1gFnSc1	Gobi
(	(	kIx(	(
<g/>
mongolsky	mongolsky	k6eAd1	mongolsky
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Gov	Gov	k1gFnSc1	Gov
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
戈	戈	k?	戈
<g/>
,	,	kIx,	,
pinyin	pinyin	k2eAgMnSc1d1	pinyin
Gē	Gē	k1gMnSc1	Gē
Shā	Shā	k1gMnSc1	Shā
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
Ke-pi	Ke	k1gFnSc2	Ke-p
ša-mo	šao	k6eAd1	ša-mo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
a	a	k8xC	a
největších	veliký	k2eAgFnPc2d3	veliký
pouští	poušť	k1gFnPc2	poušť
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
suchým	suchý	k2eAgFnPc3d1	suchá
oblastem	oblast	k1gFnPc3	oblast
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
nepravidelností	nepravidelnost	k1gFnPc2	nepravidelnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
značně	značně	k6eAd1	značně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
