<s>
George	Georgat	k5eAaPmIp3nS	Georgat
Orwell	Orwell	k1gInSc1	Orwell
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Eric	Eric	k1gFnSc1	Eric
Arthur	Arthur	k1gMnSc1	Arthur
Blair	Blair	k1gMnSc1	Blair
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1903	[number]	k4	1903
Motihari	Motihari	k1gNnSc4	Motihari
<g/>
,	,	kIx,	,
Bengálsko	Bengálsko	k1gNnSc4	Bengálsko
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1950	[number]	k4	1950
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
demokratické	demokratický	k2eAgFnSc2d1	demokratická
levicové	levicový	k2eAgFnSc2d1	levicová
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Světovou	světový	k2eAgFnSc4d1	světová
popularitu	popularita	k1gFnSc4	popularita
si	se	k3xPyFc3	se
získaly	získat	k5eAaPmAgInP	získat
jeho	jeho	k3xOp3gInPc1	jeho
alegorické	alegorický	k2eAgInPc1d1	alegorický
antiutopické	antiutopický	k2eAgInPc1d1	antiutopický
romány	román	k1gInPc1	román
Farma	farma	k1gFnSc1	farma
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
1984	[number]	k4	1984
popisující	popisující	k2eAgFnSc4d1	popisující
nehumánnost	nehumánnost	k1gFnSc4	nehumánnost
totalitních	totalitní	k2eAgFnPc2d1	totalitní
ideologií	ideologie	k1gFnPc2	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gNnSc1	George
Orwell	Orwell	k1gMnSc1	Orwell
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
součástí	součást	k1gFnSc7	součást
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Richard	Richard	k1gMnSc1	Richard
Walmesley	Walmeslea	k1gFnSc2	Walmeslea
Blair	Blair	k1gMnSc1	Blair
zde	zde	k6eAd1	zde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Orwellovi	Orwellův	k2eAgMnPc1d1	Orwellův
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
odvezla	odvézt	k5eAaPmAgFnS	odvézt
ho	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Ida	Ida	k1gFnSc1	Ida
Mabel	Mabel	k1gInSc1	Mabel
Blairová	Blairová	k1gFnSc1	Blairová
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Otce	otec	k1gMnSc4	otec
viděl	vidět	k5eAaImAgMnS	vidět
až	až	k9	až
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
starší	starý	k2eAgFnPc4d2	starší
Majorie	Majorie	k1gFnPc4	Majorie
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
Avril	Avril	k1gMnSc1	Avril
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Oxfordshiru	Oxfordshir	k1gInSc6	Oxfordshir
a	a	k8xC	a
podle	podle	k7c2	podle
místní	místní	k2eAgFnSc2d1	místní
říčky	říčka	k1gFnSc2	říčka
Orwell	Orwellum	k1gNnPc2	Orwellum
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
dal	dát	k5eAaPmAgMnS	dát
pseudonym	pseudonym	k1gInSc4	pseudonym
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
později	pozdě	k6eAd2	pozdě
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nižší	nízký	k2eAgFnSc4d2	nižší
vyšší	vysoký	k2eAgFnSc4d2	vyšší
střední	střední	k2eAgFnSc4d1	střední
třídu	třída	k1gFnSc4	třída
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
soukromou	soukromý	k2eAgFnSc4d1	soukromá
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
prestižní	prestižní	k2eAgInSc4d1	prestižní
Eton	Eton	k1gInSc4	Eton
College	Colleg	k1gFnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
mu	on	k3xPp3gMnSc3	on
rodiče	rodič	k1gMnPc1	rodič
nemohli	moct	k5eNaImAgMnP	moct
platit	platit	k5eAaImF	platit
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
Indickou	indický	k2eAgFnSc4d1	indická
imperiální	imperiální	k2eAgFnSc4d1	imperiální
policii	policie	k1gFnSc4	policie
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Britské	britský	k2eAgFnSc6d1	britská
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
počátky	počátek	k1gInPc4	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
nenávisti	nenávist	k1gFnSc2	nenávist
k	k	k7c3	k
imperialismu	imperialismus	k1gInSc3	imperialismus
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
klonit	klonit	k5eAaImF	klonit
k	k	k7c3	k
levicové	levicový	k2eAgFnSc3d1	levicová
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
románové	románový	k2eAgFnSc6d1	románová
prvotině	prvotina	k1gFnSc6	prvotina
Barmské	barmský	k2eAgInPc4d1	barmský
dny	den	k1gInPc4	den
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Burmese	Burmese	k1gFnSc1	Burmese
Days	Daysa	k1gFnPc2	Daysa
<g/>
)	)	kIx)	)
vydané	vydaný	k2eAgInPc4d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
Orwell	Orwell	k1gMnSc1	Orwell
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
eseje	esej	k1gInPc4	esej
a	a	k8xC	a
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
jako	jako	k9	jako
tulák	tulák	k1gMnSc1	tulák
a	a	k8xC	a
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
spodinou	spodina	k1gFnSc7	spodina
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenost	zkušenost	k1gFnSc4	zkušenost
zúročil	zúročit	k5eAaPmAgMnS	zúročit
například	například	k6eAd1	například
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Down	Down	k1gMnSc1	Down
and	and	k?	and
Out	Out	k1gMnSc1	Out
in	in	k?	in
Paris	Paris	k1gMnSc1	Paris
and	and	k?	and
London	London	k1gMnSc1	London
<g/>
)	)	kIx)	)
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnPc1d1	politická
události	událost	k1gFnPc1	událost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
silně	silně	k6eAd1	silně
osobně	osobně	k6eAd1	osobně
dotýkaly	dotýkat	k5eAaImAgFnP	dotýkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
socialistou	socialista	k1gMnSc7	socialista
<g/>
,	,	kIx,	,
antifašistou	antifašista	k1gMnSc7	antifašista
a	a	k8xC	a
kritikem	kritik	k1gMnSc7	kritik
všech	všecek	k3xTgFnPc2	všecek
nedemokratických	demokratický	k2eNgFnPc2d1	nedemokratická
politických	politický	k2eAgFnPc2d1	politická
tendencí	tendence	k1gFnPc2	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
jako	jako	k9	jako
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bojoval	bojovat	k5eAaImAgMnS	bojovat
na	na	k7c6	na
vládní	vládní	k2eAgFnSc6d1	vládní
straně	strana	k1gFnSc6	strana
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
milicí	milice	k1gFnPc2	milice
POUM	POUM	kA	POUM
(	(	kIx(	(
<g/>
Dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
marxistického	marxistický	k2eAgNnSc2d1	marxistické
sjednocení	sjednocení	k1gNnSc2	sjednocení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zážitky	zážitek	k1gInPc1	zážitek
z	z	k7c2	z
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Hold	hold	k1gInSc4	hold
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
líčí	líčit	k5eAaImIp3nS	líčit
i	i	k9	i
těžké	těžký	k2eAgNnSc1d1	těžké
zranění	zranění	k1gNnSc1	zranění
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
do	do	k7c2	do
krku	krk	k1gInSc2	krk
fašistickým	fašistický	k2eAgMnSc7d1	fašistický
odstřelovačem	odstřelovač	k1gMnSc7	odstřelovač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
žil	žít	k5eAaImAgMnS	žít
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
psal	psát	k5eAaImAgInS	psát
recenze	recenze	k1gFnSc2	recenze
na	na	k7c6	na
knihy	kniha	k1gFnPc4	kniha
pro	pro	k7c4	pro
Nový	nový	k2eAgInSc4d1	nový
anglický	anglický	k2eAgInSc4d1	anglický
týdeník	týdeník	k1gInSc4	týdeník
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
světové	světový	k2eAgFnPc1d1	světová
války	válka	k1gFnPc1	válka
se	se	k3xPyFc4	se
neúčastnil	účastnit	k5eNaImAgInS	účastnit
kvůli	kvůli	k7c3	kvůli
vleklé	vleklý	k2eAgFnSc3d1	vleklá
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
zahájil	zahájit	k5eAaPmAgMnS	zahájit
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
BBC	BBC	kA	BBC
a	a	k8xC	a
svými	svůj	k3xOyFgInPc7	svůj
komentáři	komentář	k1gInPc7	komentář
pilně	pilně	k6eAd1	pilně
přispíval	přispívat	k5eAaImAgInS	přispívat
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
napsal	napsat	k5eAaBmAgMnS	napsat
svůj	svůj	k3xOyFgInSc4	svůj
nejznámější	známý	k2eAgInSc4d3	nejznámější
román	román	k1gInSc4	román
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
46	[number]	k4	46
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
psal	psát	k5eAaImAgMnS	psát
knihy	kniha	k1gFnPc4	kniha
o	o	k7c6	o
diktaturách	diktatura	k1gFnPc6	diktatura
komunistického	komunistický	k2eAgInSc2d1	komunistický
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
být	být	k5eAaImF	být
socialistou	socialista	k1gMnSc7	socialista
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
jeho	jeho	k3xOp3gInSc4	jeho
citát	citát	k1gInSc4	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Každou	každý	k3xTgFnSc4	každý
řádku	řádka	k1gFnSc4	řádka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsem	být	k5eAaImIp1nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
napsal	napsat	k5eAaBmAgInS	napsat
a	a	k8xC	a
která	který	k3yIgNnPc4	který
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
psal	psát	k5eAaImAgMnS	psát
proti	proti	k7c3	proti
totalitarismu	totalitarismus	k1gInSc3	totalitarismus
a	a	k8xC	a
pro	pro	k7c4	pro
demokratický	demokratický	k2eAgInSc4d1	demokratický
socialismus	socialismus	k1gInSc4	socialismus
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
chápu	chápat	k5eAaImIp1nS	chápat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	jeho	k3xOp3gInPc1	jeho
náboženské	náboženský	k2eAgInPc1d1	náboženský
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
složité	složitý	k2eAgInPc1d1	složitý
<g/>
,	,	kIx,	,
problematické	problematický	k2eAgInPc1d1	problematický
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
jeho	jeho	k3xOp3gFnPc1	jeho
svatby	svatba	k1gFnPc1	svatba
i	i	k8xC	i
pohřeb	pohřeb	k1gInSc1	pohřeb
vedl	vést	k5eAaImAgInS	vést
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
přání	přání	k1gNnSc6	přání
anglikánský	anglikánský	k2eAgMnSc1d1	anglikánský
kněz	kněz	k1gMnSc1	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
literárním	literární	k2eAgNnSc6d1	literární
díle	dílo	k1gNnSc6	dílo
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
v	v	k7c6	v
románu	román	k1gInSc6	román
Farářova	farářův	k2eAgFnSc1d1	farářova
dcera	dcera	k1gFnSc1	dcera
<g/>
)	)	kIx)	)
však	však	k9	však
církev	církev	k1gFnSc4	církev
i	i	k8xC	i
náboženství	náboženství	k1gNnSc4	náboženství
spíše	spíše	k9	spíše
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
z	z	k7c2	z
pozic	pozice	k1gFnPc2	pozice
levicového	levicový	k2eAgInSc2d1	levicový
ateismu	ateismus	k1gInSc2	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Orwellův	Orwellův	k2eAgMnSc1d1	Orwellův
životopisec	životopisec	k1gMnSc1	životopisec
Stephen	Stephen	k2eAgInSc4d1	Stephen
Ingle	Ingle	k1gInSc4	Ingle
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
spisovatel	spisovatel	k1gMnSc1	spisovatel
George	George	k1gFnPc2	George
Orwell	Orwell	k1gMnSc1	Orwell
se	se	k3xPyFc4	se
holedbal	holedbat	k5eAaImAgMnS	holedbat
svým	svůj	k3xOyFgInSc7	svůj
ateismem	ateismus	k1gInSc7	ateismus
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
člověk	člověk	k1gMnSc1	člověk
Eric	Eric	k1gInSc1	Eric
Blair	Blair	k1gMnSc1	Blair
si	se	k3xPyFc3	se
uchoval	uchovat	k5eAaPmAgMnS	uchovat
hluboce	hluboko	k6eAd1	hluboko
zakořeněnou	zakořeněný	k2eAgFnSc4d1	zakořeněná
zbožnost	zbožnost	k1gFnSc4	zbožnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
a	a	k8xC	a
eseje	esej	k1gInPc1	esej
se	se	k3xPyFc4	se
netýkají	týkat	k5eNaImIp3nP	týkat
jen	jen	k9	jen
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
komentují	komentovat	k5eAaBmIp3nP	komentovat
život	život	k1gInSc4	život
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
zabývají	zabývat	k5eAaImIp3nP	zabývat
se	s	k7c7	s
sociálními	sociální	k2eAgNnPc7d1	sociální
tématy	téma	k1gNnPc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
anglických	anglický	k2eAgMnPc2d1	anglický
esejistů	esejista	k1gMnPc2	esejista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunistickém	komunistický	k2eAgNnSc6d1	komunistické
Československu	Československo	k1gNnSc6	Československo
byl	být	k5eAaImAgInS	být
Orwell	Orwell	k1gInSc4	Orwell
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
zakázaných	zakázaný	k2eAgMnPc2d1	zakázaný
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
nemohly	moct	k5eNaImAgFnP	moct
oficiálně	oficiálně	k6eAd1	oficiálně
vycházet	vycházet	k5eAaImF	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
exilových	exilový	k2eAgNnPc6d1	exilové
nakladatelstvích	nakladatelství	k1gNnPc6	nakladatelství
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
opisovaly	opisovat	k5eAaImAgFnP	opisovat
a	a	k8xC	a
šířily	šířit	k5eAaImAgFnP	šířit
formou	forma	k1gFnSc7	forma
samizdatu	samizdat	k1gInSc2	samizdat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vydala	vydat	k5eAaPmAgFnS	vydat
Svoboda	svoboda	k1gFnSc1	svoboda
kritickou	kritický	k2eAgFnSc4d1	kritická
stostránkovou	stostránkový	k2eAgFnSc4d1	stostránková
publikaci	publikace	k1gFnSc4	publikace
Josefa	Josef	k1gMnSc2	Josef
Skály	skála	k1gFnSc2	skála
Postřehy	postřeh	k1gInPc1	postřeh
a	a	k8xC	a
fikce	fikce	k1gFnPc1	fikce
George	Georg	k1gMnSc2	Georg
Orwella	Orwell	k1gMnSc2	Orwell
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Ústřední	ústřední	k2eAgNnSc1d1	ústřední
dělnické	dělnický	k2eAgNnSc1d1	dělnické
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
Down	Down	k1gMnSc1	Down
and	and	k?	and
Out	Out	k1gMnSc1	Out
in	in	k?	in
Paris	Paris	k1gMnSc1	Paris
and	and	k?	and
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
–	–	k?	–
Orwellovy	Orwellův	k2eAgInPc4d1	Orwellův
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
tulák	tulák	k1gMnSc1	tulák
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
mezi	mezi	k7c7	mezi
chudými	chudý	k1gMnPc7	chudý
Barmské	barmský	k2eAgFnSc2d1	barmská
dny	dna	k1gFnSc2	dna
<g/>
,	,	kIx,	,
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7207-164-5	[number]	k4	80-7207-164-5
(	(	kIx(	(
<g/>
Burmese	Burmese	k1gFnSc1	Burmese
Days	Daysa	k1gFnPc2	Daysa
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
–	–	k?	–
život	život	k1gInSc1	život
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
koloniální	koloniální	k2eAgFnSc6d1	koloniální
barmské	barmský	k2eAgFnSc6d1	barmská
vesnici	vesnice	k1gFnSc6	vesnice
rozvíří	rozvířit	k5eAaPmIp3nS	rozvířit
příjezd	příjezd	k1gInSc4	příjezd
mladé	mladý	k2eAgFnSc2d1	mladá
anglické	anglický	k2eAgFnSc2d1	anglická
dámy	dáma	k1gFnSc2	dáma
<g/>
.	.	kIx.	.
</s>
<s>
Orwell	Orwell	k1gMnSc1	Orwell
knihu	kniha	k1gFnSc4	kniha
napsal	napsat	k5eAaPmAgMnS	napsat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgFnPc2	svůj
zkušeností	zkušenost	k1gFnPc2	zkušenost
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922-1927	[number]	k4	1922-1927
Farářova	farářův	k2eAgFnSc1d1	farářova
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7207-337-0	[number]	k4	80-7207-337-0
(	(	kIx(	(
<g/>
A	a	k8xC	a
Clergyman	Clergyman	k1gMnSc1	Clergyman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Daughter	Daughter	k1gInSc1	Daughter
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
–	–	k?	–
zamyšlení	zamyšlení	k1gNnSc1	zamyšlení
nad	nad	k7c7	nad
smyslem	smysl	k1gInSc7	smysl
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
nechtěný	chtěný	k2eNgInSc4d1	nechtěný
útěk	útěk	k1gInSc4	útěk
dcery	dcera	k1gFnSc2	dcera
<g />
.	.	kIx.	.
</s>
<s>
vesnického	vesnický	k2eAgMnSc4d1	vesnický
faráře	farář	k1gMnSc4	farář
do	do	k7c2	do
zcela	zcela	k6eAd1	zcela
odlišného	odlišný	k2eAgNnSc2d1	odlišné
prostředí	prostředí	k1gNnSc2	prostředí
Bože	bůh	k1gMnSc5	bůh
chraň	chránit	k5eAaImRp2nS	chránit
aspidistru	aspidistra	k1gFnSc4	aspidistra
<g/>
,	,	kIx,	,
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
a	a	k8xC	a
KGB	KGB	kA	KGB
2001	[number]	k4	2001
ISBN	ISBN	kA	ISBN
80-7207-395-8	[number]	k4	80-7207-395-8
(	(	kIx(	(
<g/>
Keep	Keep	k1gInSc1	Keep
the	the	k?	the
Aspidistra	aspidistra	k1gFnSc1	aspidistra
Flying	Flying	k1gInSc1	Flying
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
Wigan	Wigan	k1gMnSc1	Wigan
Pier	pier	k1gInSc1	pier
<g/>
,	,	kIx,	,
Argo	Argo	k6eAd1	Argo
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Road	Road	k1gMnSc1	Road
to	ten	k3xDgNnSc4	ten
Wigan	Wigana	k1gFnPc2	Wigana
Pier	pier	k1gInSc1	pier
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
reportáž	reportáž	k1gFnSc4	reportáž
o	o	k7c6	o
životních	životní	k2eAgFnPc6d1	životní
podmínkách	podmínka	k1gFnPc6	podmínka
horníků	horník	k1gMnPc2	horník
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
Orwell	Orwell	k1gInSc1	Orwell
popisuje	popisovat	k5eAaImIp3nS	popisovat
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
socialismu	socialismus	k1gInSc3	socialismus
Hold	hold	k1gInSc1	hold
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc4	odeon
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
Homage	Homage	k1gFnSc4	Homage
to	ten	k3xDgNnSc1	ten
Catalonia	Catalonium	k1gNnPc4	Catalonium
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
–	–	k?	–
zážitky	zážitek	k1gInPc4	zážitek
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
Španělské	španělský	k2eAgFnSc6d1	španělská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
Nadechnout	nadechnout	k5eAaPmF	nadechnout
se	s	k7c7	s
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-1136-8	[number]	k4	80-207-1136-8
(	(	kIx(	(
<g/>
Coming	Coming	k1gInSc1	Coming
Up	Up	k1gFnPc2	Up
For	forum	k1gNnPc2	forum
Air	Air	k1gMnPc2	Air
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
–	–	k?	–
méně	málo	k6eAd2	málo
známý	známý	k2eAgInSc4d1	známý
román	román	k1gInSc4	román
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
líčící	líčící	k2eAgInSc4d1	líčící
útěk	útěk	k1gInSc4	útěk
do	do	k7c2	do
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
a	a	k8xC	a
střet	střet	k1gInSc1	střet
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
Válečný	válečný	k2eAgInSc1d1	válečný
deník	deník	k1gInSc1	deník
–	–	k?	–
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
života	život	k1gInSc2	život
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Farma	farma	k1gFnSc1	farma
<g />
.	.	kIx.	.
</s>
<s>
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
I.	I.	kA	I.
L.	L.	kA	L.
Kober	kobra	k1gFnPc2	kobra
1946	[number]	k4	1946
(	(	kIx(	(
<g/>
Animal	animal	k1gMnSc1	animal
Farm	Farm	k1gMnSc1	Farm
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Index	index	k1gInSc1	index
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc4	náš
vojsko	vojsko	k1gNnSc4	vojsko
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
Nineteen	Nineteen	k2eAgMnSc1d1	Nineteen
Eighty-Four	Eighty-Four	k1gMnSc1	Eighty-Four
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
V	v	k7c6	v
břiše	břicho	k1gNnSc6	břicho
velryby	velryba	k1gFnSc2	velryba
<g/>
,	,	kIx,	,
esej	esej	k1gFnSc1	esej
o	o	k7c6	o
Henrym	Henry	k1gMnSc6	Henry
Millerovi	Miller	k1gMnSc6	Miller
<g/>
,	,	kIx,	,
Votobia	Votobia	k1gFnSc1	Votobia
1996	[number]	k4	1996
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
124	[number]	k4	124
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Adéla	Adéla	k1gFnSc1	Adéla
Václavková	Václavková	k1gFnSc1	Václavková
<g/>
,	,	kIx,	,
verše	verš	k1gInPc1	verš
přebásnil	přebásnit	k5eAaPmAgMnS	přebásnit
Petr	Petr	k1gMnSc1	Petr
Pistorius	Pistorius	k1gMnSc1	Pistorius
Úpadek	úpadek	k1gInSc4	úpadek
anglické	anglický	k2eAgFnSc2d1	anglická
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
,	,	kIx,	,
Votobia	Votobia	k1gFnSc1	Votobia
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85885	[number]	k4	85885
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
-X	-X	k?	-X
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Alena	Alena	k1gFnSc1	Alena
Caklová	Caklová	k1gFnSc1	Caklová
–	–	k?	–
seznam	seznam	k1gInSc4	seznam
esejů	esej	k1gInPc2	esej
<g/>
:	:	kIx,	:
Úpadek	úpadek	k1gInSc1	úpadek
anglické	anglický	k2eAgFnSc2d1	anglická
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
Poprava	poprava	k1gFnSc1	poprava
<g/>
,	,	kIx,	,
Výsada	výsada	k1gFnSc1	výsada
kléru	klér	k1gInSc2	klér
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
umírají	umírat	k5eAaImIp3nP	umírat
chudí	chudý	k2eAgMnPc1d1	chudý
<g/>
,	,	kIx,	,
Rudyard	Rudyard	k1gMnSc1	Rudyard
Kipling	Kipling	k1gInSc1	Kipling
<g/>
,	,	kIx,	,
Raffles	Raffles	k1gInSc1	Raffles
a	a	k8xC	a
slečna	slečna	k1gFnSc1	slečna
Blandishová	Blandishová	k1gFnSc1	Blandishová
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickens	k1gInSc1	Dickens
<g/>
,	,	kIx,	,
Umění	umění	k1gNnSc1	umění
Donalda	Donald	k1gMnSc2	Donald
McGilla	McGilla	k1gFnSc1	McGilla
<g/>
,	,	kIx,	,
Poznámky	poznámka	k1gFnPc1	poznámka
o	o	k7c6	o
nacionalismu	nacionalismus	k1gInSc6	nacionalismus
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Proč	proč	k6eAd1	proč
píšu	psát	k5eAaImIp1nS	psát
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
,	,	kIx,	,
Atlantis	Atlantis	k1gFnSc1	Atlantis
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7108	[number]	k4	7108
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
<g/>
-X	-X	k?	-X
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Kateřina	Kateřina	k1gFnSc1	Kateřina
Hilská	Hilská	k1gFnSc1	Hilská
(	(	kIx(	(
<g/>
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
nejsou	být	k5eNaImIp3nP	být
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
Orwellovy	Orwellův	k2eAgInPc1d1	Orwellův
drobnější	drobný	k2eAgInPc1d2	drobnější
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
sloupky	sloupek	k1gInPc1	sloupek
a	a	k8xC	a
fejetony	fejeton	k1gInPc1	fejeton
<g/>
)	)	kIx)	)
A	a	k9	a
Hanging	Hanging	k1gInSc1	Hanging
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
překládáno	překládán	k2eAgNnSc1d1	překládáno
Poprava	poprava	k1gFnSc1	poprava
či	či	k8xC	či
Poprava	poprava	k1gFnSc1	poprava
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
českých	český	k2eAgInPc6d1	český
výborech	výbor	k1gInPc6	výbor
<g/>
)	)	kIx)	)
–	–	k?	–
črta	črta	k1gFnSc1	črta
<g/>
,	,	kIx,	,
líčící	líčící	k2eAgFnSc4d1	líčící
popravu	poprava	k1gFnSc4	poprava
indického	indický	k2eAgMnSc2d1	indický
vězně	vězeň	k1gMnSc2	vězeň
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
Orwell	Orwell	k1gInSc1	Orwell
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
ostrahy	ostraha	k1gFnSc2	ostraha
Shooting	Shooting	k1gInSc1	Shooting
an	an	k?	an
Elephant	Elephant	k1gInSc1	Elephant
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
překládáno	překládán	k2eAgNnSc1d1	překládáno
Střílení	střílení	k1gNnSc1	střílení
na	na	k7c4	na
slona	slon	k1gMnSc4	slon
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
esej	esej	k1gFnSc1	esej
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgFnSc1d1	popisující
Orwellovy	Orwellův	k2eAgFnPc4d1	Orwellova
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
koloniální	koloniální	k2eAgFnSc6d1	koloniální
policii	policie	k1gFnSc6	policie
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickensa	k1gFnPc2	Dickensa
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
českých	český	k2eAgInPc6d1	český
výborech	výbor	k1gInPc6	výbor
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
nepřekládán	překládán	k2eNgInSc1d1	překládán
<g/>
)	)	kIx)	)
–	–	k?	–
poměrně	poměrně	k6eAd1	poměrně
kritický	kritický	k2eAgInSc4d1	kritický
rozbor	rozbor	k1gInSc4	rozbor
díla	dílo	k1gNnSc2	dílo
Charlese	Charles	k1gMnSc2	Charles
<g />
.	.	kIx.	.
</s>
<s>
Dickense	Dickense	k1gFnSc1	Dickense
Boys	boy	k1gMnPc2	boy
<g/>
'	'	kIx"	'
Weeklies	Weeklies	k1gMnSc1	Weeklies
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
překládáno	překládán	k2eAgNnSc1d1	překládáno
Týdeníky	týdeník	k1gInPc7	týdeník
pro	pro	k7c4	pro
kluky	kluk	k1gMnPc4	kluk
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
literární	literární	k2eAgInSc4d1	literární
rozbor	rozbor	k1gInSc4	rozbor
"	"	kIx"	"
<g/>
školních	školní	k2eAgFnPc2d1	školní
povídek	povídka	k1gFnPc2	povídka
<g/>
"	"	kIx"	"
v	v	k7c6	v
chlapeckých	chlapecký	k2eAgInPc6d1	chlapecký
časopisech	časopis	k1gInPc6	časopis
Orwellovy	Orwellův	k2eAgFnSc2d1	Orwellova
doby	doba	k1gFnSc2	doba
Inside	Insid	k1gInSc5	Insid
the	the	k?	the
Whale	Whale	k1gNnPc1	Whale
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
překládáno	překládán	k2eAgNnSc1d1	překládáno
Uvnitř	uvnitř	k7c2	uvnitř
<g />
.	.	kIx.	.
</s>
<s>
velryby	velryba	k1gFnPc1	velryba
či	či	k8xC	či
V	v	k7c6	v
břiše	břicho	k1gNnSc6	břicho
velryby	velryba	k1gFnSc2	velryba
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
a	a	k8xC	a
také	také	k9	také
samostatně	samostatně	k6eAd1	samostatně
<g/>
)	)	kIx)	)
–	–	k?	–
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
literárně	literárně	k6eAd1	literárně
kritická	kritický	k2eAgFnSc1d1	kritická
esej	esej	k1gFnSc1	esej
<g/>
,	,	kIx,	,
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
především	především	k9	především
moderní	moderní	k2eAgFnSc3d1	moderní
anglické	anglický	k2eAgFnSc3d1	anglická
literatuře	literatura	k1gFnSc3	literatura
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
Hitler	Hitler	k1gMnSc1	Hitler
and	and	k?	and
the	the	k?	the
World	World	k1gInSc1	World
State	status	k1gInSc5	status
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
překládáno	překládán	k2eAgNnSc1d1	překládáno
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
a	a	k8xC	a
Světový	světový	k2eAgInSc1d1	světový
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
kritika	kritika	k1gFnSc1	kritika
Wellsových	Wellsová	k1gFnPc2	Wellsová
utopických	utopický	k2eAgFnPc2d1	utopická
vizí	vize	k1gFnPc2	vize
The	The	k1gMnSc1	The
Art	Art	k1gMnSc1	Art
of	of	k?	of
Donald	Donald	k1gMnSc1	Donald
McGill	McGill	k1gMnSc1	McGill
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
překládáno	překládán	k2eAgNnSc1d1	překládáno
Umění	umění	k1gNnSc1	umění
Donalda	Donald	k1gMnSc2	Donald
McGilla	McGill	k1gMnSc2	McGill
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
českých	český	k2eAgInPc6d1	český
výborech	výbor	k1gInPc6	výbor
<g/>
)	)	kIx)	)
–	–	k?	–
literární	literární	k2eAgInSc4d1	literární
rozbor	rozbor	k1gInSc4	rozbor
dobových	dobový	k2eAgFnPc2d1	dobová
pohlednic	pohlednice	k1gFnPc2	pohlednice
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
kreslenými	kreslený	k2eAgInPc7d1	kreslený
vtipy	vtip	k1gInPc7	vtip
Looking	Looking	k1gInSc1	Looking
Back	Back	k1gInSc1	Back
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Spanish	Spanish	k1gMnSc1	Spanish
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
překládáno	překládán	k2eAgNnSc4d1	překládáno
jako	jako	k8xC	jako
Ohlédnutí	ohlédnutí	k1gNnSc4	ohlédnutí
za	za	k7c7	za
španělskou	španělský	k2eAgFnSc7d1	španělská
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
politický	politický	k2eAgInSc4d1	politický
rozbor	rozbor	k1gInSc4	rozbor
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
jakýsi	jakýsi	k3yIgInSc4	jakýsi
esejistický	esejistický	k2eAgInSc4d1	esejistický
dodatek	dodatek	k1gInSc4	dodatek
k	k	k7c3	k
románu	román	k1gInSc3	román
Hold	hold	k1gInSc1	hold
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
W.	W.	kA	W.
B.	B.	kA	B.
Yeats	Yeatsa	k1gFnPc2	Yeatsa
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
,	,	kIx,	,
titul	titul	k1gInSc4	titul
nebyl	být	k5eNaImAgInS	být
přeložen	přeložen	k2eAgInSc1d1	přeložen
<g/>
)	)	kIx)	)
–	–	k?	–
vlastně	vlastně	k9	vlastně
recenze	recenze	k1gFnSc1	recenze
na	na	k7c4	na
biografii	biografie	k1gFnSc4	biografie
Williama	William	k1gMnSc2	William
Butlera	Butler	k1gMnSc2	Butler
Yeatse	Yeats	k1gMnSc2	Yeats
Benefit	Benefit	k1gInSc1	Benefit
of	of	k?	of
Clergy	Clerg	k1gInPc4	Clerg
<g/>
:	:	kIx,	:
Some	Some	k1gFnSc1	Some
notes	notes	k1gInSc4	notes
on	on	k3xPp3gMnSc1	on
Salvador	Salvador	k1gMnSc1	Salvador
Dali	dát	k5eAaPmAgMnP	dát
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
českých	český	k2eAgInPc6d1	český
výborech	výbor	k1gInPc6	výbor
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Výsada	výsada	k1gFnSc1	výsada
kléru	klér	k1gInSc3	klér
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Výsada	výsada	k1gFnSc1	výsada
kněžstva	kněžstvo	k1gNnSc2	kněžstvo
<g/>
)	)	kIx)	)
–	–	k?	–
recenze	recenze	k1gFnPc1	recenze
a	a	k8xC	a
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
Dalího	Dalí	k1gMnSc4	Dalí
autobiografii	autobiografie	k1gFnSc6	autobiografie
<g/>
,	,	kIx,	,
ústící	ústící	k2eAgFnSc6d1	ústící
do	do	k7c2	do
úvahy	úvaha	k1gFnSc2	úvaha
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
někomu	někdo	k3yInSc3	někdo
odpouštěna	odpouštěn	k2eAgFnSc1d1	odpouštěn
zvrhlost	zvrhlost	k1gFnSc1	zvrhlost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
mimořádným	mimořádný	k2eAgInSc7d1	mimořádný
talentem	talent	k1gInSc7	talent
Arthur	Arthur	k1gMnSc1	Arthur
Koestler	Koestler	k1gMnSc1	Koestler
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g />
.	.	kIx.	.
</s>
<s>
bez	bez	k7c2	bez
přeložení	přeložení	k1gNnSc2	přeložení
názvu	název	k1gInSc2	název
<g/>
)	)	kIx)	)
Notes	notes	k1gInSc4	notes
on	on	k3xPp3gMnSc1	on
Nationalism	Nationalism	k1gMnSc1	Nationalism
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Poznámky	poznámka	k1gFnSc2	poznámka
o	o	k7c6	o
nacionalismu	nacionalismus	k1gInSc6	nacionalismus
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
klasifikace	klasifikace	k1gFnSc1	klasifikace
a	a	k8xC	a
kritika	kritika	k1gFnSc1	kritika
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
politiky	politika	k1gFnSc2	politika
How	How	k1gMnSc1	How
the	the	k?	the
Poor	Poor	k1gMnSc1	Poor
Die	Die	k1gMnSc1	Die
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Jak	jak	k8xC	jak
umírají	umírat	k5eAaImIp3nP	umírat
chudí	chudý	k2eAgMnPc1d1	chudý
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
výborech	výbor	k1gInPc6	výbor
<g/>
)	)	kIx)	)
–	–	k?	–
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
črta	črta	k1gFnSc1	črta
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgInSc1d1	popisující
Orwellův	Orwellův	k2eAgInSc1d1	Orwellův
pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
chudinské	chudinský	k2eAgFnSc6d1	chudinská
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Politics	Politicsa	k1gFnPc2	Politicsa
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Literature	Literatur	k1gMnSc5	Literatur
<g/>
:	:	kIx,	:
An	An	k1gMnSc2	An
Examination	Examination	k1gInSc1	Examination
of	of	k?	of
Gulliver	Gulliver	k1gInSc1	Gulliver
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Travels	Travelsa	k1gFnPc2	Travelsa
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Politika	politikum	k1gNnSc2	politikum
versus	versus	k7c1	versus
literatura	literatura	k1gFnSc1	literatura
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
rozbor	rozbor	k1gInSc1	rozbor
Gulliverových	Gulliverová	k1gFnPc2	Gulliverová
cest	cesta	k1gFnPc2	cesta
Jonathana	Jonathan	k1gMnSc2	Jonathan
Swifta	Swift	k1gMnSc2	Swift
Politics	Politics	k1gInSc1	Politics
and	and	k?	and
the	the	k?	the
English	English	k1gInSc1	English
Language	language	k1gFnSc1	language
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
<g />
.	.	kIx.	.
</s>
<s>
názvem	název	k1gInSc7	název
Politika	politikum	k1gNnSc2	politikum
a	a	k8xC	a
anglický	anglický	k2eAgInSc1d1	anglický
jazyk	jazyk	k1gInSc1	jazyk
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
esej	esej	k1gFnSc1	esej
o	o	k7c6	o
anglické	anglický	k2eAgFnSc6d1	anglická
stylistice	stylistika	k1gFnSc6	stylistika
<g/>
,	,	kIx,	,
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
klišé	klišé	k1gNnSc1	klišé
v	v	k7c6	v
mluvě	mluva	k1gFnSc6	mluva
anglických	anglický	k2eAgMnPc2d1	anglický
politiků	politik	k1gMnPc2	politik
Second	Second	k1gMnSc1	Second
Thoughts	Thoughts	k1gInSc4	Thoughts
on	on	k3xPp3gMnSc1	on
James	James	k1gMnSc1	James
Burnham	Burnham	k1gInSc1	Burnham
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
nejspíš	nejspíš	k9	nejspíš
Další	další	k2eAgInPc1d1	další
názory	názor	k1gInPc1	názor
na	na	k7c6	na
Jamese	Jamesa	k1gFnSc6	Jamesa
Burnhama	Burnhamum	k1gNnSc2	Burnhamum
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Decline	Declin	k1gInSc5	Declin
of	of	k?	of
the	the	k?	the
English	English	k1gMnSc1	English
Murder	Murder	k1gMnSc1	Murder
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Úpadek	úpadek	k1gInSc1	úpadek
anglické	anglický	k2eAgFnSc2d1	anglická
vraždy	vražda	k1gFnSc2	vražda
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
výborech	výbor	k1gInPc6	výbor
<g/>
)	)	kIx)	)
–	–	k?	–
fejeton	fejeton	k1gInSc1	fejeton
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	se	k3xPyFc4	se
ideální	ideální	k2eAgFnSc7d1	ideální
vraždou	vražda	k1gFnSc7	vražda
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
čtenářů	čtenář	k1gMnPc2	čtenář
novin	novina	k1gFnPc2	novina
Some	Som	k1gInSc2	Som
Thoughts	Thoughts	k1gInSc4	Thoughts
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Common	Common	k1gNnSc1	Common
Toad	Toad	k1gMnSc1	Toad
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zamyšlení	zamyšlení	k1gNnSc4	zamyšlení
<g />
.	.	kIx.	.
</s>
<s>
nad	nad	k7c7	nad
žábou	žába	k1gFnSc7	žába
obecnou	obecný	k2eAgFnSc7d1	obecná
ve	v	k7c6	v
výboru	výbor	k1gInSc2	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
esej	esej	k1gFnSc1	esej
o	o	k7c6	o
zapomínaných	zapomínaný	k2eAgInPc6d1	zapomínaný
symbolech	symbol	k1gInPc6	symbol
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
žába	žába	k1gFnSc1	žába
A	a	k8xC	a
Good	Good	k1gInSc1	Good
Word	Word	kA	Word
for	forum	k1gNnPc2	forum
the	the	k?	the
Vicar	Vicar	k1gMnSc1	Vicar
of	of	k?	of
Bray	Braa	k1gFnSc2	Braa
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Přímluva	přímluva	k1gFnSc1	přímluva
za	za	k7c2	za
vikáře	vikář	k1gMnSc2	vikář
z	z	k7c2	z
Bray	Braa	k1gFnSc2	Braa
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
<g />
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
esej	esej	k1gFnSc4	esej
o	o	k7c6	o
záslužnosti	záslužnost	k1gFnSc6	záslužnost
sázení	sázení	k1gNnSc2	sázení
stromů	strom	k1gInPc2	strom
In	In	k1gFnPc2	In
Defence	Defence	k1gFnSc2	Defence
of	of	k?	of
P.	P.	kA	P.
G.	G.	kA	G.
Wodehouse	Wodehous	k1gInSc6	Wodehous
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Why	Why	k1gFnSc2	Why
I	i	k9	i
Write	Writ	k1gInSc5	Writ
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Proč	proč	k6eAd1	proč
píšu	psát	k5eAaImIp1nS	psát
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
výborech	výbor	k1gInPc6	výbor
<g/>
)	)	kIx)	)
–	–	k?	–
esej	esej	k1gFnSc4	esej
o	o	k7c6	o
důvodech	důvod	k1gInPc6	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
spisovatelé	spisovatel	k1gMnPc1	spisovatel
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
psát	psát	k5eAaImF	psát
The	The	k1gFnSc1	The
Prevention	Prevention	k1gInSc4	Prevention
of	of	k?	of
Literature	Literatur	k1gMnSc5	Literatur
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zakazování	zakazování	k1gNnSc1	zakazování
literatury	literatura	k1gFnSc2	literatura
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
esej	esej	k1gInSc1	esej
<g/>
,	,	kIx,	,
vyjadřující	vyjadřující	k2eAgInPc1d1	vyjadřující
Orwellovy	Orwellův	k2eAgInPc1d1	Orwellův
názory	názor	k1gInPc1	názor
na	na	k7c4	na
cenzuru	cenzura	k1gFnSc4	cenzura
Such	sucho	k1gNnPc2	sucho
<g/>
,	,	kIx,	,
Such	sucho	k1gNnPc2	sucho
Were	Were	k1gInSc1	Were
the	the	k?	the
Joys	Joys	k1gInSc1	Joys
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
<g />
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
radosti	radost	k1gFnPc1	radost
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
delší	dlouhý	k2eAgFnSc1d2	delší
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
črta	črta	k1gFnSc1	črta
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
autorovým	autorův	k2eAgNnSc7d1	autorovo
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
škole	škola	k1gFnSc6	škola
St	St	kA	St
Cyprian	Cyprian	k1gInSc1	Cyprian
a	a	k8xC	a
popisující	popisující	k2eAgFnPc1d1	popisující
jeho	jeho	k3xOp3gFnPc1	jeho
otřesné	otřesný	k2eAgFnPc1d1	otřesná
zkušenosti	zkušenost	k1gFnPc1	zkušenost
s	s	k7c7	s
dobovým	dobový	k2eAgInSc7d1	dobový
vzdělávacím	vzdělávací	k2eAgInSc7d1	vzdělávací
systémem	systém	k1gInSc7	systém
Lear	Leara	k1gFnPc2	Leara
<g/>
,	,	kIx,	,
Tolstoy	Tolstoa	k1gFnSc2	Tolstoa
and	and	k?	and
the	the	k?	the
Fool	Fool	k1gInSc1	Fool
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g />
.	.	kIx.	.
</s>
<s>
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Lear	Lear	k1gMnSc1	Lear
<g/>
,	,	kIx,	,
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
a	a	k8xC	a
šašek	šašek	k1gMnSc1	šašek
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Uvnitř	uvnitř	k7c2	uvnitř
velryby	velryba	k1gFnSc2	velryba
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
eseje	esej	k1gFnSc2	esej
<g/>
)	)	kIx)	)
–	–	k?	–
polemika	polemika	k1gFnSc1	polemika
s	s	k7c7	s
Tolstým	Tolstý	k2eAgInSc7d1	Tolstý
ohledně	ohledně	k7c2	ohledně
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
Reflections	Reflections	k1gInSc4	Reflections
on	on	k3xPp3gMnSc1	on
Gandhi	Gandh	k1gMnSc6	Gandh
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Úvahy	úvaha	k1gFnSc2	úvaha
o	o	k7c6	o
Gándhím	Gándhí	k2eAgNnSc6d1	Gándhí
<g/>
)	)	kIx)	)
–	–	k?	–
esej	esej	k1gFnSc1	esej
o	o	k7c6	o
úloze	úloha	k1gFnSc6	úloha
Gándhího	Gándhí	k1gMnSc2	Gándhí
v	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
boji	boj	k1gInSc6	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
"	"	kIx"	"
<g/>
Antifašismus	antifašismus	k1gInSc1	antifašismus
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
snažíme	snažit	k5eAaImIp1nP	snažit
zachovat	zachovat	k5eAaPmF	zachovat
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Fašismus	fašismus	k1gInSc1	fašismus
je	být	k5eAaImIp3nS	být
koneckonců	koneckonců	k9	koneckonců
pouze	pouze	k6eAd1	pouze
dalším	další	k2eAgInSc7d1	další
vývojovým	vývojový	k2eAgInSc7d1	vývojový
stupněm	stupeň	k1gInSc7	stupeň
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
George	Georg	k1gFnSc2	Georg
Orwell	Orwella	k1gFnPc2	Orwella
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Orwell	Orwell	k1gInSc1	Orwell
George	George	k1gInSc1	George
<g/>
:	:	kIx,	:
Nadechnout	nadechnout	k5eAaPmF	nadechnout
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
1136	[number]	k4	1136
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Kopecký	Kopecký	k1gMnSc1	Kopecký
Kolektiv	kolektiv	k1gInSc4	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Přehledné	přehledný	k2eAgFnPc4d1	přehledná
dějiny	dějiny	k1gFnPc4	dějiny
literatury	literatura	k1gFnSc2	literatura
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85937	[number]	k4	85937
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Anglická	anglický	k2eAgFnSc1d1	anglická
literatura	literatura	k1gFnSc1	literatura
Seznam	seznam	k1gInSc4	seznam
anglických	anglický	k2eAgMnPc2d1	anglický
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
George	Georg	k1gInSc2	Georg
Orwell	Orwella	k1gFnPc2	Orwella
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
George	Georg	k1gFnSc2	Georg
Orwell	Orwella	k1gFnPc2	Orwella
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
George	George	k1gInSc4	George
Orwell	Orwellum	k1gNnPc2	Orwellum
George	Georg	k1gMnSc2	Georg
Orwell	Orwella	k1gFnPc2	Orwella
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Literárního	literární	k2eAgNnSc2d1	literární
doupěte	doupě	k1gNnSc2	doupě
Orwellova	Orwellův	k2eAgNnSc2d1	Orwellovo
díla	dílo	k1gNnSc2	dílo
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
</s>
