<p>
<s>
Katrina	Katrina	k1gFnSc1	Katrina
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
hurikánu	hurikán	k1gInSc2	hurikán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
způsobil	způsobit	k5eAaPmAgMnS	způsobit
obrovské	obrovský	k2eAgFnPc4d1	obrovská
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
až	až	k9	až
280	[number]	k4	280
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
kolem	kolem	k7c2	kolem
250	[number]	k4	250
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
U	u	k7c2	u
New	New	k1gFnSc2	New
Orleansu	Orleans	k1gInSc2	Orleans
se	se	k3xPyFc4	se
protrhly	protrhnout	k5eAaPmAgFnP	protrhnout
ochranné	ochranný	k2eAgFnPc1d1	ochranná
hráze	hráz	k1gFnPc1	hráz
a	a	k8xC	a
město	město	k1gNnSc1	město
zcela	zcela	k6eAd1	zcela
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
blízkého	blízký	k2eAgNnSc2d1	blízké
jezera	jezero	k1gNnSc2	jezero
Pontchartrain	Pontchartraina	k1gFnPc2	Pontchartraina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
patrně	patrně	k6eAd1	patrně
o	o	k7c4	o
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgFnSc4d3	veliký
katastrofu	katastrofa	k1gFnSc4	katastrofa
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
atlantickým	atlantický	k2eAgInSc7d1	atlantický
hurikánem	hurikán	k1gInSc7	hurikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hurikán	hurikán	k1gInSc1	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
a	a	k8xC	a
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
==	==	k?	==
</s>
</p>
<p>
<s>
Hurikán	hurikán	k1gInSc1	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
postihl	postihnout	k5eAaPmAgInS	postihnout
New	New	k1gFnSc4	New
Orleans	Orleans	k1gInSc1	Orleans
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Hurikán	hurikán	k1gInSc1	hurikán
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
největší	veliký	k2eAgFnSc4d3	veliký
civilní	civilní	k2eAgFnSc4d1	civilní
technickou	technický	k2eAgFnSc4d1	technická
katastrofu	katastrofa	k1gFnSc4	katastrofa
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
USA	USA	kA	USA
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
v	v	k7c6	v
takovémto	takovýto	k3xDgInSc6	takovýto
rozsahu	rozsah	k1gInSc6	rozsah
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
selhání	selhání	k1gNnSc4	selhání
hrází	hráz	k1gFnPc2	hráz
a	a	k8xC	a
odvodňovacích	odvodňovací	k2eAgInPc2d1	odvodňovací
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cesta	cesta	k1gFnSc1	cesta
hurikánu	hurikán	k1gInSc2	hurikán
a	a	k8xC	a
evakuace	evakuace	k1gFnSc2	evakuace
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
relativně	relativně	k6eAd1	relativně
mírný	mírný	k2eAgInSc1d1	mírný
hurikán	hurikán	k1gInSc1	hurikán
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
přehnal	přehnat	k5eAaPmAgMnS	přehnat
přes	přes	k7c4	přes
Floridu	Florida	k1gFnSc4	Florida
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
novou	nový	k2eAgFnSc4d1	nová
sílu	síla	k1gFnSc4	síla
nad	nad	k7c7	nad
Mexickým	mexický	k2eAgInSc7d1	mexický
zálivem	záliv	k1gInSc7	záliv
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
přeřazen	přeřadit	k5eAaPmNgInS	přeřadit
až	až	k9	až
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Někteří	některý	k3yIgMnPc1	některý
hydrometeorologové	hydrometeorolog	k1gMnPc1	hydrometeorolog
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
zvýšením	zvýšení	k1gNnSc7	zvýšení
intenzity	intenzita	k1gFnSc2	intenzita
hurikánu	hurikán	k1gInSc2	hurikán
mohla	moct	k5eAaImAgFnS	moct
jeho	jeho	k3xOp3gFnPc4	jeho
interakce	interakce	k1gFnPc4	interakce
s	s	k7c7	s
teplejší	teplý	k2eAgFnSc7d2	teplejší
vodou	voda	k1gFnSc7	voda
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Nepovinná	povinný	k2eNgFnSc1d1	nepovinná
evakuace	evakuace	k1gFnSc1	evakuace
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
New	New	k1gFnSc4	New
Orleans	Orleans	k1gInSc4	Orleans
opustilo	opustit	k5eAaPmAgNnS	opustit
své	svůj	k3xOyFgInPc4	svůj
domovy	domov	k1gInPc4	domov
ještě	ještě	k9	ještě
před	před	k7c7	před
bouří	bouř	k1gFnSc7	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
stejná	stejný	k2eAgFnSc1d1	stejná
záchranářská	záchranářský	k2eAgFnSc1d1	záchranářská
technika	technika	k1gFnSc1	technika
a	a	k8xC	a
plán	plán	k1gInSc1	plán
jako	jako	k8xC	jako
při	při	k7c6	při
hurikánu	hurikán	k1gInSc6	hurikán
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Evakuací	evakuace	k1gFnSc7	evakuace
se	se	k3xPyFc4	se
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
zachránila	zachránit	k5eAaPmAgFnS	zachránit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ale	ale	k9	ale
20	[number]	k4	20
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
když	když	k8xS	když
hurikán	hurikán	k1gInSc1	hurikán
udeřil	udeřit	k5eAaPmAgInS	udeřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Následky	následek	k1gInPc4	následek
===	===	k?	===
</s>
</p>
<p>
<s>
Silný	silný	k2eAgInSc1d1	silný
vítr	vítr	k1gInSc1	vítr
zničil	zničit	k5eAaPmAgInS	zničit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
roztříštil	roztříštit	k5eAaPmAgMnS	roztříštit
okna	okno	k1gNnPc4	okno
<g/>
,	,	kIx,	,
hromadil	hromadit	k5eAaImAgMnS	hromadit
odpad	odpad	k1gInSc4	odpad
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
a	a	k8xC	a
přinesl	přinést	k5eAaPmAgMnS	přinést
silný	silný	k2eAgInSc4d1	silný
déšť	déšť	k1gInSc4	déšť
a	a	k8xC	a
záplavy	záplava	k1gFnPc4	záplava
zejména	zejména	k9	zejména
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
protrhly	protrhnout	k5eAaPmAgFnP	protrhnout
další	další	k2eAgInPc4d1	další
4	[number]	k4	4
kanály	kanál	k1gInPc4	kanál
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
začala	začít	k5eAaPmAgFnS	začít
voda	voda	k1gFnSc1	voda
ustupovat	ustupovat	k5eAaImF	ustupovat
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
mohli	moct	k5eAaImAgMnP	moct
ohledávat	ohledávat	k5eAaImF	ohledávat
škody	škoda	k1gFnPc4	škoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
opravdu	opravdu	k6eAd1	opravdu
veliké	veliký	k2eAgFnPc1d1	veliká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
voda	voda	k1gFnSc1	voda
výšky	výška	k1gFnSc2	výška
až	až	k9	až
7,6	[number]	k4	7,6
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
opravy	oprava	k1gFnPc4	oprava
a	a	k8xC	a
škody	škoda	k1gFnPc4	škoda
způsobené	způsobený	k2eAgFnPc4d1	způsobená
hurikánem	hurikán	k1gInSc7	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
byly	být	k5eAaImAgInP	být
vyčísleny	vyčíslen	k2eAgInPc1d1	vyčíslen
na	na	k7c4	na
10	[number]	k4	10
až	až	k9	až
25	[number]	k4	25
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
ztráta	ztráta	k1gFnSc1	ztráta
byla	být	k5eAaImAgFnS	být
vyčíslena	vyčíslit	k5eAaPmNgFnS	vyčíslit
na	na	k7c4	na
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Hurikán	hurikán	k1gInSc1	hurikán
také	také	k9	také
zničil	zničit	k5eAaPmAgInS	zničit
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
Six	Six	k1gFnSc2	Six
Flags	Flagsa	k1gFnPc2	Flagsa
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
JazzLand	JazzLand	k1gInSc1	JazzLand
<g/>
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
byla	být	k5eAaImAgFnS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
$	$	kIx~	$
<g/>
30	[number]	k4	30
000	[number]	k4	000
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Hurikán	hurikán	k1gInSc1	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
jako	jako	k8xC	jako
Boží	boží	k2eAgInSc1d1	boží
trest	trest	k1gInSc1	trest
==	==	k?	==
</s>
</p>
<p>
<s>
Hurikán	hurikán	k1gInSc1	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
klérem	klér	k1gInSc7	klér
různých	různý	k2eAgInPc2d1	různý
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
náboženských	náboženský	k2eAgInPc2d1	náboženský
směrů	směr	k1gInPc2	směr
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
Boží	boží	k2eAgInSc4d1	boží
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
potraty	potrat	k1gInPc4	potrat
<g/>
,	,	kIx,	,
homosexualitu	homosexualita	k1gFnSc4	homosexualita
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
sexuální	sexuální	k2eAgInSc1d1	sexuální
život	život	k1gInSc1	život
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
křesťanskými	křesťanský	k2eAgNnPc7d1	křesťanské
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
působící	působící	k2eAgMnSc1d1	působící
pomocný	pomocný	k2eAgMnSc1d1	pomocný
katolický	katolický	k2eAgMnSc1d1	katolický
linecký	linecký	k2eAgMnSc1d1	linecký
biskup	biskup	k1gMnSc1	biskup
Gerhard	Gerhard	k1gMnSc1	Gerhard
Maria	Mario	k1gMnSc2	Mario
Wagner	Wagner	k1gMnSc1	Wagner
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
hurikán	hurikán	k1gInSc4	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
za	za	k7c4	za
Boží	boží	k2eAgInSc4d1	boží
trest	trest	k1gInSc4	trest
za	za	k7c4	za
sexuální	sexuální	k2eAgInPc4d1	sexuální
hříchy	hřích	k1gInPc4	hřích
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
New	New	k1gFnSc2	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Hurikán	hurikán	k1gInSc1	hurikán
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
zničil	zničit	k5eAaPmAgInS	zničit
nevěstince	nevěstinec	k1gInSc2	nevěstinec
<g/>
,	,	kIx,	,
noční	noční	k2eAgInPc1d1	noční
kluby	klub	k1gInPc1	klub
a	a	k8xC	a
interrupční	interrupční	k2eAgFnPc1d1	interrupční
kliniky	klinika	k1gFnPc1	klinika
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
protesty	protest	k1gInPc4	protest
katolíků	katolík	k1gMnPc2	katolík
ze	z	k7c2	z
zasažených	zasažený	k2eAgFnPc2d1	zasažená
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Wagner	Wagner	k1gMnSc1	Wagner
kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgInPc3	tento
i	i	k9	i
dalším	další	k2eAgInPc3d1	další
kontroverzním	kontroverzní	k2eAgInPc3d1	kontroverzní
výrokům	výrok	k1gInPc3	výrok
musel	muset	k5eAaImAgInS	muset
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
.	.	kIx.	.
<g/>
Americký	americký	k2eAgMnSc1d1	americký
evangelický	evangelický	k2eAgMnSc1d1	evangelický
pastor	pastor	k1gMnSc1	pastor
John	John	k1gMnSc1	John
Hagee	Hagee	k1gInSc4	Hagee
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
hurikánu	hurikán	k1gInSc2	hurikán
homosexuály	homosexuál	k1gMnPc7	homosexuál
a	a	k8xC	a
specificky	specificky	k6eAd1	specificky
pak	pak	k6eAd1	pak
průvod	průvod	k1gInSc1	průvod
Gay	gay	k1gMnSc1	gay
Pride	Prid	k1gInSc5	Prid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc4	Orleans
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Katrina	Katrina	k1gFnSc1	Katrina
udeřila	udeřit	k5eAaPmAgFnS	udeřit
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
také	také	k9	také
nekonal	konat	k5eNaImAgInS	konat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
svoje	svůj	k3xOyFgInPc4	svůj
výroky	výrok	k1gInPc4	výrok
zmírnil	zmírnit	k5eAaPmAgInS	zmírnit
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
znát	znát	k5eAaImF	znát
záměry	záměr	k1gInPc4	záměr
Boží	boží	k2eAgFnSc2d1	boží
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
neměl	mít	k5eNaImAgMnS	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
<g/>
..	..	k?	..
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Atlantická	atlantický	k2eAgFnSc1d1	Atlantická
hurikánová	hurikánový	k2eAgFnSc1d1	hurikánová
sezóna	sezóna	k1gFnSc1	sezóna
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hurikán	hurikán	k1gInSc1	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Africa	Africa	k1gMnSc1	Africa
in	in	k?	in
Our	Our	k1gMnSc1	Our
Midst	Midst	k1gMnSc1	Midst
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
media	medium	k1gNnSc2	medium
suppress	suppress	k6eAd1	suppress
Katrina	Katrin	k2eAgFnSc1d1	Katrina
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
lessons	lessonsa	k1gFnPc2	lessonsa
<g/>
.	.	kIx.	.
</s>
</p>
