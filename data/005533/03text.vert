<s>
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Solnohrad	Solnohrad	k1gInSc1	Solnohrad
<g/>
,	,	kIx,	,
počeštěně	počeštěně	k6eAd1	počeštěně
Salcburk	Salcburk	k1gInSc1	Salcburk
<g/>
,	,	kIx,	,
Salzburk	Salzburk	k1gInSc1	Salzburk
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
česky	česky	k6eAd1	česky
též	též	k9	též
Salcpurk	Salcpurk	k1gInSc1	Salcpurk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
rakouské	rakouský	k2eAgFnSc2d1	rakouská
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Salcbursko	Salcbursko	k1gNnSc1	Salcbursko
a	a	k8xC	a
centrum	centrum	k1gNnSc1	centrum
okresu	okres	k1gInSc2	okres
Salzburg-venkov	Salzburgenkov	k1gInSc1	Salzburg-venkov
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
nedaleko	nedaleko	k7c2	nedaleko
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Salzach	Salzacha	k1gFnPc2	Salzacha
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
425	[number]	k4	425
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
Salzburské	salzburský	k2eAgNnSc1d1	Salzburské
(	(	kIx(	(
<g/>
historické	historický	k2eAgNnSc1d1	historické
městské	městský	k2eAgNnSc1d1	Městské
jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
necelými	celý	k2eNgInPc7d1	necelý
150	[number]	k4	150
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Salzburg	Salzburg	k1gInSc4	Salzburg
po	po	k7c6	po
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
a	a	k8xC	a
Linci	Linec	k1gInSc6	Linec
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Salcburské	salcburský	k2eAgFnSc6d1	Salcburská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
210	[number]	k4	210
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Spádová	spádový	k2eAgFnSc1d1	spádová
oblast	oblast	k1gFnSc1	oblast
Salzburgu	Salzburg	k1gInSc2	Salzburg
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
přes	přes	k7c4	přes
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
jihovýchodního	jihovýchodní	k2eAgNnSc2d1	jihovýchodní
Horního	horní	k2eAgNnSc2d1	horní
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
do	do	k7c2	do
hornorakouských	hornorakouský	k2eAgNnPc2d1	Hornorakouské
a	a	k8xC	a
hornoštýrských	hornoštýrský	k2eAgNnPc2d1	hornoštýrský
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
Euroregionu	euroregion	k1gInSc2	euroregion
Salzbursko	Salzbursko	k1gNnSc4	Salzbursko
<g/>
–	–	k?	–
<g/>
berchtesgadenského	berchtesgadenský	k2eAgInSc2d1	berchtesgadenský
kraje	kraj	k1gInSc2	kraj
–	–	k?	–
Traunstein	Traunstein	k1gMnSc1	Traunstein
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
zváno	zvát	k5eAaImNgNnS	zvát
též	též	k9	též
Mozartovým	Mozartův	k2eAgNnSc7d1	Mozartovo
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
slavný	slavný	k2eAgMnSc1d1	slavný
skladatel	skladatel	k1gMnSc1	skladatel
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Salzburg	Salzburg	k1gInSc1	Salzburg
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
Salcburské	salcburský	k2eAgFnSc6d1	Salcburská
pánvi	pánev	k1gFnSc6	pánev
asi	asi	k9	asi
25	[number]	k4	25
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
pohoří	pohoří	k1gNnSc2	pohoří
Tennen	Tennna	k1gFnPc2	Tennna
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Salzach	Salzacha	k1gFnPc2	Salzacha
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Salzach	Salzacha	k1gFnPc2	Salzacha
protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
a	a	k8xC	a
formuje	formovat	k5eAaImIp3nS	formovat
nezastavěné	zastavěný	k2eNgFnSc3d1	nezastavěná
městské	městský	k2eAgFnSc3d1	městská
kopce	kopka	k1gFnSc3	kopka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ze	z	k7c2	z
Salzburku	Salzburk	k1gInSc2	Salzburk
dělá	dělat	k5eAaImIp3nS	dělat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejzelenějších	zelený	k2eAgNnPc2d3	nejzelenější
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
městským	městský	k2eAgInPc3d1	městský
kopcům	kopec	k1gInPc3	kopec
patří	patřit	k5eAaImIp3nS	patřit
Kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Mnišská	mnišský	k2eAgFnSc1d1	mnišská
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Mezní	mezní	k2eAgFnSc1d1	mezní
hora	hora	k1gFnSc1	hora
a	a	k8xC	a
Festungsberg	Festungsberg	k1gInSc1	Festungsberg
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tvrzí	tvrz	k1gFnSc7	tvrz
trůní	trůnit	k5eAaImIp3nS	trůnit
největší	veliký	k2eAgInSc1d3	veliký
zcela	zcela	k6eAd1	zcela
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
středověký	středověký	k2eAgInSc1d1	středověký
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pověstmi	pověst	k1gFnPc7	pověst
opředená	opředený	k2eAgFnSc1d1	opředená
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
Untersberg	Untersberg	k1gInSc4	Untersberg
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
je	být	k5eAaImIp3nS	být
rozmach	rozmach	k1gInSc1	rozmach
města	město	k1gNnSc2	město
omezen	omezen	k2eAgInSc1d1	omezen
1288	[number]	k4	1288
m	m	kA	m
vysokou	vysoký	k2eAgFnSc7d1	vysoká
horou	hora	k1gFnSc7	hora
Gaisberg	Gaisberg	k1gMnSc1	Gaisberg
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
není	být	k5eNaImIp3nS	být
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
kraje	kraj	k1gInSc2	kraj
Salzkammergut	Salzkammergut	k1gInSc4	Salzkammergut
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
s	s	k7c7	s
pohořím	pohoří	k1gNnSc7	pohoří
Salzkammergutberge	Salzkammergutberg	k1gFnSc2	Salzkammergutberg
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
Salzburská	salzburský	k2eAgFnSc1d1	Salzburská
pánev	pánev	k1gFnSc1	pánev
<g/>
,	,	kIx,	,
a	a	k8xC	a
lužní	lužní	k2eAgInPc1d1	lužní
lesy	les	k1gInPc1	les
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Salzach	Salzacha	k1gFnPc2	Salzacha
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
nízká	nízký	k2eAgFnSc1d1	nízká
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
Flachgau	Flachgaus	k1gInSc2	Flachgaus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Plainberg	Plainberg	k1gMnSc1	Plainberg
nebo	nebo	k8xC	nebo
hora	hora	k1gFnSc1	hora
Kalvárie	Kalvárie	k1gFnSc1	Kalvárie
Kalvarienberg	Kalvarienberg	k1gInSc1	Kalvarienberg
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
poutní	poutní	k2eAgFnSc7d1	poutní
bazilikou	bazilika	k1gFnSc7	bazilika
Maria	Mario	k1gMnSc2	Mario
Plain	Plaina	k1gFnPc2	Plaina
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
výhled	výhled	k1gInSc1	výhled
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
Salzburg	Salzburg	k1gInSc4	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
hraničí	hraničit	k5eAaImIp3nP	hraničit
území	území	k1gNnSc4	území
města	město	k1gNnSc2	město
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
Bavorskem	Bavorsko	k1gNnSc7	Bavorsko
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
Saalach	Saalacha	k1gFnPc2	Saalacha
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
městem	město	k1gNnSc7	město
Freilassing	Freilassing	k1gInSc4	Freilassing
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Berchtesgadenského	Berchtesgadenský	k2eAgInSc2d1	Berchtesgadenský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dávných	dávný	k2eAgInPc6d1	dávný
počátcích	počátek	k1gInPc6	počátek
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
existovalo	existovat	k5eAaImAgNnS	existovat
rané	raný	k2eAgNnSc4d1	rané
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
spravováno	spravován	k2eAgNnSc1d1	spravováno
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
především	především	k9	především
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Salzachu	Salzach	k1gInSc2	Salzach
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řeka	řeka	k1gFnSc1	řeka
a	a	k8xC	a
hora	hora	k1gFnSc1	hora
Mönchsberg	Mönchsberg	k1gInSc4	Mönchsberg
skýtaly	skýtat	k5eAaImAgFnP	skýtat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
přírodní	přírodní	k2eAgFnSc4d1	přírodní
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
jižní	jižní	k2eAgFnSc4d1	jižní
stranu	strana	k1gFnSc4	strana
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
opevňovat	opevňovat	k5eAaImF	opevňovat
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
dali	dát	k5eAaPmAgMnP	dát
městu	město	k1gNnSc3	město
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Juvavum	Juvavum	k1gNnSc1	Juvavum
<g/>
"	"	kIx"	"
–	–	k?	–
Sídlo	sídlo	k1gNnSc1	sídlo
boha	bůh	k1gMnSc2	bůh
nebes	nebesa	k1gNnPc2	nebesa
–	–	k?	–
a	a	k8xC	a
povýšili	povýšit	k5eAaPmAgMnP	povýšit
je	on	k3xPp3gMnPc4	on
do	do	k7c2	do
úrovně	úroveň	k1gFnSc2	úroveň
municipia	municipium	k1gNnSc2	municipium
a	a	k8xC	a
jen	jen	k9	jen
o	o	k7c4	o
pár	pár	k4xCyI	pár
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Claudia	Claudia	k1gFnSc1	Claudia
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
důležitým	důležitý	k2eAgNnSc7d1	důležité
mocenským	mocenský	k2eAgNnSc7d1	mocenské
centrem	centrum	k1gNnSc7	centrum
nové	nový	k2eAgFnSc2d1	nová
římské	římský	k2eAgFnSc2d1	římská
provincie	provincie	k1gFnSc2	provincie
Noricum	Noricum	k1gNnSc1	Noricum
(	(	kIx(	(
<g/>
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
románské	románský	k2eAgFnSc6d1	románská
době	doba	k1gFnSc6	doba
provincie	provincie	k1gFnSc2	provincie
"	"	kIx"	"
<g/>
Ufernoricum	Ufernoricum	k1gInSc1	Ufernoricum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
požívalo	požívat	k5eAaImAgNnS	požívat
postavení	postavení	k1gNnSc4	postavení
Municipia	municipium	k1gNnSc2	municipium
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Municipium	municipium	k1gNnSc1	municipium
Claudium	Claudium	k1gNnSc1	Claudium
Juvavum	Juvavum	k1gNnSc1	Juvavum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Salzburg	Salzburg	k1gInSc4	Salzburg
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Germánie	Germánie	k1gFnSc2	Germánie
<g/>
.	.	kIx.	.
</s>
<s>
Salzburg	Salzburg	k1gInSc1	Salzburg
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
13	[number]	k4	13
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
Aigen	Aigen	k1gInSc1	Aigen
<g/>
,	,	kIx,	,
Bergheim	Bergheim	k1gMnSc1	Bergheim
<g/>
,	,	kIx,	,
Gaisberg	Gaisberg	k1gMnSc1	Gaisberg
<g/>
,	,	kIx,	,
Gnigl	Gnigl	k1gMnSc1	Gnigl
<g/>
,	,	kIx,	,
Hallwang	Hallwang	k1gMnSc1	Hallwang
<g/>
,	,	kIx,	,
Heuberg	Heuberg	k1gMnSc1	Heuberg
<g/>
,	,	kIx,	,
Itzling	Itzling	k1gInSc1	Itzling
<g/>
,	,	kIx,	,
Leopoldskron	Leopoldskron	k1gInSc1	Leopoldskron
<g/>
,	,	kIx,	,
Maxglan	Maxglan	k1gInSc1	Maxglan
<g/>
,	,	kIx,	,
Morzg	Morzg	k1gInSc1	Morzg
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
Siezenheim	Siezenheim	k1gInSc1	Siezenheim
a	a	k8xC	a
Wals	wals	k1gInSc1	wals
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zpravidla	zpravidla	k6eAd1	zpravidla
považováno	považován	k2eAgNnSc1d1	považováno
asi	asi	k9	asi
30	[number]	k4	30
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
leckdy	leckdy	k6eAd1	leckdy
protínaly	protínat	k5eAaImAgFnP	protínat
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1935	[number]	k4	1935
schválil	schválit	k5eAaPmAgInS	schválit
salzburský	salzburský	k2eAgInSc1d1	salzburský
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
Landtag	Landtag	k1gInSc1	Landtag
zákon	zákon	k1gInSc4	zákon
o	o	k7c4	o
"	"	kIx"	"
<g/>
rozšíření	rozšíření	k1gNnSc4	rozšíření
území	území	k1gNnSc2	území
zemského	zemský	k2eAgNnSc2d1	zemské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Salzburku	Salzburk	k1gInSc2	Salzburk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1935	[number]	k4	1935
došlo	dojít	k5eAaPmAgNnS	dojít
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
spojení	spojení	k1gNnSc3	spojení
obcí	obec	k1gFnPc2	obec
Maxglan	Maxglan	k1gInSc1	Maxglan
a	a	k8xC	a
Gnigl	Gnigl	k1gInSc1	Gnigl
<g/>
/	/	kIx~	/
<g/>
Itzling	Itzling	k1gInSc1	Itzling
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
částí	část	k1gFnSc7	část
Aigen	Aigno	k1gNnPc2	Aigno
<g/>
,	,	kIx,	,
Morzg	Morzg	k1gMnSc1	Morzg
<g/>
,	,	kIx,	,
Siezenheim	Siezenheim	k1gMnSc1	Siezenheim
<g/>
,	,	kIx,	,
Leopoldskron	Leopoldskron	k1gMnSc1	Leopoldskron
<g/>
,	,	kIx,	,
Bergheim	Bergheim	k1gMnSc1	Bergheim
a	a	k8xC	a
Hallwang	Hallwang	k1gMnSc1	Hallwang
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
zjištění	zjištění	k1gNnSc1	zjištění
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
sloučením	sloučení	k1gNnSc7	sloučení
učinili	učinit	k5eAaPmAgMnP	učinit
Heinz	Heinz	k1gMnSc1	Heinz
Dopsch	Dopsch	k1gMnSc1	Dopsch
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Salzburg	Salzburg	k1gInSc4	Salzburg
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc5	Die
Geschichte	Geschicht	k1gMnSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
Stadt	Stadt	k1gInSc4	Stadt
Salzburg	Salzburg	k1gInSc4	Salzburg
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
občané	občan	k1gMnPc1	občan
<g/>
,	,	kIx,	,
nemovitosti	nemovitost	k1gFnPc1	nemovitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
dluhy	dluh	k1gInPc1	dluh
dvou	dva	k4xCgFnPc2	dva
obcí	obec	k1gFnPc2	obec
Maxglan	Maxglana	k1gFnPc2	Maxglana
a	a	k8xC	a
Gnigl	Gnigla	k1gFnPc2	Gnigla
<g/>
/	/	kIx~	/
<g/>
Itzling	Itzling	k1gInSc1	Itzling
byly	být	k5eAaImAgFnP	být
převzaty	převzít	k5eAaPmNgInP	převzít
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
chudé	chudý	k2eAgFnPc4d1	chudá
dělnické	dělnický	k2eAgFnPc4d1	Dělnická
osady	osada	k1gFnPc4	osada
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
záchranu	záchrana	k1gFnSc4	záchrana
před	před	k7c7	před
finančním	finanční	k2eAgNnSc7d1	finanční
zhroucením	zhroucení	k1gNnSc7	zhroucení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Touto	tento	k3xDgFnSc7	tento
vlnou	vlna	k1gFnSc7	vlna
slučování	slučování	k1gNnSc2	slučování
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
někdejších	někdejší	k2eAgInPc2d1	někdejší
40	[number]	k4	40
232	[number]	k4	232
na	na	k7c4	na
63	[number]	k4	63
275	[number]	k4	275
občanů	občan	k1gMnPc2	občan
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
z	z	k7c2	z
8,79	[number]	k4	8,79
km2	km2	k4	km2
na	na	k7c4	na
24,9	[number]	k4	24,9
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
fáze	fáze	k1gFnSc1	fáze
slučování	slučování	k1gNnSc2	slučování
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1939	[number]	k4	1939
a	a	k8xC	a
počítala	počítat	k5eAaImAgFnS	počítat
se	se	k3xPyFc4	se
zahrnutím	zahrnutí	k1gNnSc7	zahrnutí
doposud	doposud	k6eAd1	doposud
samostatných	samostatný	k2eAgFnPc2d1	samostatná
obcí	obec	k1gFnPc2	obec
Aigen	Aigen	k2eAgInSc1d1	Aigen
bei	bei	k?	bei
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
Liefering	Liefering	k1gInSc1	Liefering
<g/>
,	,	kIx,	,
Leopoldskron	Leopoldskron	k1gInSc1	Leopoldskron
a	a	k8xC	a
Morzg	Morzg	k1gInSc1	Morzg
a	a	k8xC	a
též	též	k9	též
části	část	k1gFnPc1	část
Anif	Anif	k1gInSc1	Anif
(	(	kIx(	(
<g/>
Hellbrunn	Hellbrunn	k1gInSc1	Hellbrunn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bergheim	Bergheim	k1gInSc1	Bergheim
<g/>
,	,	kIx,	,
Hallwang	Hallwang	k1gInSc1	Hallwang
a	a	k8xC	a
Wals	wals	k1gInSc1	wals
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
na	na	k7c4	na
77	[number]	k4	77
170	[number]	k4	170
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
na	na	k7c4	na
65	[number]	k4	65
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
sloučení	sloučení	k1gNnSc4	sloučení
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
autoritářského	autoritářský	k2eAgInSc2d1	autoritářský
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
sloučení	sloučení	k1gNnPc2	sloučení
v	v	k7c6	v
salzburském	salzburský	k2eAgInSc6d1	salzburský
okrese	okres	k1gInSc6	okres
nebyla	být	k5eNaImAgFnS	být
odvolána	odvolán	k2eAgFnSc1d1	odvolána
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
usoudit	usoudit	k5eAaPmF	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgNnPc1d1	tehdejší
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
byla	být	k5eAaImAgNnP	být
učiněna	učinit	k5eAaPmNgNnP	učinit
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
z	z	k7c2	z
věcných	věcný	k2eAgInPc2d1	věcný
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
budiž	budiž	k9	budiž
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
obec	obec	k1gFnSc1	obec
Oberalm	Oberalmo	k1gNnPc2	Oberalmo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
znovu	znovu	k6eAd1	znovu
oddělena	oddělit	k5eAaPmNgNnP	oddělit
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Hallein	Halleina	k1gFnPc2	Halleina
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
území	území	k1gNnSc6	území
hellbrunnských	hellbrunnský	k2eAgFnPc2d1	hellbrunnský
rovin	rovina	k1gFnPc2	rovina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
Anif	Anif	k1gMnSc1	Anif
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
sloučení	sloučení	k1gNnSc3	sloučení
uskutečněné	uskutečněný	k2eAgFnSc2d1	uskutečněná
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
vykonala	vykonat	k5eAaPmAgFnS	vykonat
přípravy	příprava	k1gFnPc4	příprava
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
společenství	společenství	k1gNnSc2	společenství
Salzburgu	Salzburg	k1gInSc2	Salzburg
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
sloučení	sloučení	k1gNnPc1	sloučení
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
byla	být	k5eAaImAgNnP	být
sice	sice	k8xC	sice
požadována	požadován	k2eAgFnSc1d1	požadována
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
městských	městský	k2eAgNnPc2d1	Městské
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
odedávna	odedávna	k6eAd1	odedávna
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
celku	celek	k1gInSc3	celek
a	a	k8xC	a
též	též	k6eAd1	též
území	území	k1gNnPc2	území
často	často	k6eAd1	často
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Speckgürtelgemeinden	Speckgürtelgemeindno	k1gNnPc2	Speckgürtelgemeindno
<g/>
"	"	kIx"	"
obce	obec	k1gFnSc2	obec
–	–	k?	–
tukové	tukový	k2eAgInPc4d1	tukový
opasky	opasek	k1gInPc4	opasek
od	od	k7c2	od
Wals-Siezenheim	Wals-Siezenheima	k1gFnPc2	Wals-Siezenheima
a	a	k8xC	a
Bergheim	Bergheimo	k1gNnPc2	Bergheimo
územního	územní	k2eAgNnSc2d1	územní
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
politicky	politicky	k6eAd1	politicky
dodnes	dodnes	k6eAd1	dodnes
nezměněný	změněný	k2eNgInSc1d1	nezměněný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
mezitím	mezitím	k6eAd1	mezitím
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
soustavnému	soustavný	k2eAgNnSc3d1	soustavné
zhoršování	zhoršování	k1gNnSc3	zhoršování
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
územní	územní	k2eAgFnSc6d1	územní
aglomeraci	aglomerace	k1gFnSc6	aglomerace
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc3	strana
město	město	k1gNnSc1	město
trpí	trpět	k5eAaImIp3nS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
stojí	stát	k5eAaImIp3nS	stát
snaha	snaha	k1gFnSc1	snaha
závodů	závod	k1gInPc2	závod
přenášet	přenášet	k5eAaImF	přenášet
výrobu	výroba	k1gFnSc4	výroba
do	do	k7c2	do
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
obcí	obec	k1gFnPc2	obec
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
periférii	periférie	k1gFnSc4	periférie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
městu	město	k1gNnSc3	město
nedostává	dostávat	k5eNaImIp3nS	dostávat
potřebná	potřebný	k2eAgFnSc1d1	potřebná
výměra	výměra	k1gFnSc1	výměra
volných	volný	k2eAgFnPc2d1	volná
ploch	plocha	k1gFnPc2	plocha
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
zase	zase	k9	zase
za	za	k7c4	za
následek	následek	k1gInSc4	následek
nižší	nízký	k2eAgInPc1d2	nižší
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
nerovnoměrné	rovnoměrný	k2eNgNnSc4d1	nerovnoměrné
finanční	finanční	k2eAgNnSc4d1	finanční
přerozdělování	přerozdělování	k1gNnSc4	přerozdělování
mezi	mezi	k7c7	mezi
městem	město	k1gNnSc7	město
a	a	k8xC	a
"	"	kIx"	"
<g/>
tukovými	tukový	k2eAgInPc7d1	tukový
opasky	opasek	k1gInPc7	opasek
<g/>
"	"	kIx"	"
dále	daleko	k6eAd2	daleko
vyostřuje	vyostřovat	k5eAaImIp3nS	vyostřovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
stále	stále	k6eAd1	stále
znovu	znovu	k6eAd1	znovu
diskutované	diskutovaný	k2eAgInPc1d1	diskutovaný
resp.	resp.	kA	resp.
požadované	požadovaný	k2eAgFnPc1d1	požadovaná
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
doposud	doposud	k6eAd1	doposud
neuskutečněné	uskutečněný	k2eNgFnPc1d1	neuskutečněná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
připojení	připojení	k1gNnSc4	připojení
obcí	obec	k1gFnPc2	obec
Anif	Anif	k1gMnSc1	Anif
<g/>
,	,	kIx,	,
Elsbethen	Elsbethen	k2eAgMnSc1d1	Elsbethen
<g/>
,	,	kIx,	,
Grödig	Grödig	k1gMnSc1	Grödig
<g/>
,	,	kIx,	,
Hallwang	Hallwang	k1gMnSc1	Hallwang
a	a	k8xC	a
Eugendorf	Eugendorf	k1gMnSc1	Eugendorf
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
městem	město	k1gNnSc7	město
Salzburg	Salzburg	k1gInSc4	Salzburg
sousedí	sousedit	k5eAaImIp3nP	sousedit
obce	obec	k1gFnPc1	obec
(	(	kIx(	(
<g/>
bráno	brát	k5eAaImNgNnS	brát
od	od	k7c2	od
severu	sever	k1gInSc2	sever
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Bergheim	Bergheim	k1gMnSc1	Bergheim
<g/>
,	,	kIx,	,
Hallwang	Hallwang	k1gMnSc1	Hallwang
<g/>
,	,	kIx,	,
Koppl	Koppl	k1gMnSc1	Koppl
<g/>
,	,	kIx,	,
Elsbethen	Elsbethen	k1gInSc1	Elsbethen
<g/>
,	,	kIx,	,
Anif	Anif	k1gInSc1	Anif
<g/>
,	,	kIx,	,
Grödig	Grödig	k1gInSc1	Grödig
<g/>
,	,	kIx,	,
Wals-Siezenheim	Wals-Siezenheim	k1gInSc1	Wals-Siezenheim
a	a	k8xC	a
Freilassing	Freilassing	k1gInSc1	Freilassing
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
se	se	k3xPyFc4	se
jednorázově	jednorázově	k6eAd1	jednorázově
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
sloučením	sloučení	k1gNnSc7	sloučení
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
obcemi	obec	k1gFnPc7	obec
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
40	[number]	k4	40
232	[number]	k4	232
na	na	k7c4	na
63	[number]	k4	63
275	[number]	k4	275
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
době	doba	k1gFnSc6	doba
zažil	zažít	k5eAaPmAgMnS	zažít
Salzburg	Salzburg	k1gInSc4	Salzburg
znovu	znovu	k6eAd1	znovu
boom	boom	k1gInSc4	boom
<g/>
,	,	kIx,	,
když	když	k8xS	když
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
celá	celá	k1gFnSc1	celá
sídliště	sídliště	k1gNnSc2	sídliště
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
vítězné	vítězný	k2eAgFnSc2d1	vítězná
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
jako	jako	k8xS	jako
např.	např.	kA	např.
sídliště	sídliště	k1gNnSc4	sídliště
Generála	generál	k1gMnSc2	generál
Keyese	Keyese	k1gFnSc2	Keyese
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
r.	r.	kA	r.
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
překročil	překročit	k5eAaPmAgInS	překročit
stotisícovou	stotisícový	k2eAgFnSc4d1	stotisícová
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
Salcburk	Salcburk	k1gInSc1	Salcburk
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
své	svůj	k3xOyFgNnSc4	svůj
trvalé	trvalý	k2eAgNnSc4d1	trvalé
bydliště	bydliště	k1gNnSc4	bydliště
149	[number]	k4	149
997	[number]	k4	997
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
tendence	tendence	k1gFnSc1	tendence
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
stoupající	stoupající	k2eAgFnSc1d1	stoupající
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
zeměpisné	zeměpisný	k2eAgFnSc3d1	zeměpisná
poloze	poloha	k1gFnSc3	poloha
je	být	k5eAaImIp3nS	být
Salzburg	Salzburg	k1gInSc1	Salzburg
dopravně	dopravně	k6eAd1	dopravně
i	i	k9	i
hospodářsky	hospodářsky	k6eAd1	hospodářsky
propojen	propojit	k5eAaPmNgInS	propojit
s	s	k7c7	s
bavorskými	bavorský	k2eAgInPc7d1	bavorský
kraji	kraj	k1gInPc7	kraj
Berchtesgadensko	Berchtesgadensko	k1gNnSc1	Berchtesgadensko
a	a	k8xC	a
Traunstein	Traunstein	k1gInSc1	Traunstein
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýhodnější	výhodný	k2eAgNnPc1d3	nejvýhodnější
dopravní	dopravní	k2eAgNnPc1d1	dopravní
spojení	spojení	k1gNnPc1	spojení
k	k	k7c3	k
městům	město	k1gNnPc3	město
a	a	k8xC	a
obcím	obec	k1gFnPc3	obec
kraje	kraj	k1gInSc2	kraj
Pinzgau	Pinzgaa	k1gFnSc4	Pinzgaa
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
tzv.	tzv.	kA	tzv.
Německý	německý	k2eAgInSc4d1	německý
kout	kout	k1gInSc4	kout
(	(	kIx(	(
<g/>
Salzburg-Bad	Salzburg-Bad	k1gInSc1	Salzburg-Bad
Reichenhall-Lofer	Reichenhall-Lofer	k1gInSc1	Reichenhall-Lofer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářsky	hospodářsky	k6eAd1	hospodářsky
jsou	být	k5eAaImIp3nP	být
se	s	k7c7	s
Salzburgem	Salzburg	k1gInSc7	Salzburg
propojena	propojit	k5eAaPmNgFnS	propojit
zejména	zejména	k6eAd1	zejména
města	město	k1gNnSc2	město
Freilassing	Freilassing	k1gInSc1	Freilassing
a	a	k8xC	a
Bad	Bad	k1gFnSc1	Bad
Reichenhall	Reichenhallum	k1gNnPc2	Reichenhallum
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1993	[number]	k4	1993
tvoří	tvořit	k5eAaImIp3nS	tvořit
Euroregion	euroregion	k1gInSc1	euroregion
Salzburg	Salzburg	k1gInSc1	Salzburg
–	–	k?	–
Berchtesgadensko	Berchtesgadensko	k1gNnSc1	Berchtesgadensko
–	–	k?	–
Traunstein	Traunstein	k2eAgInSc4d1	Traunstein
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
aglomeraci	aglomerace	k1gFnSc4	aglomerace
se	s	k7c7	s
zhruba	zhruba	k6eAd1	zhruba
700	[number]	k4	700
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Stopy	stopa	k1gFnPc1	stopa
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
doložitelné	doložitelný	k2eAgFnPc1d1	doložitelná
už	už	k9	už
od	od	k7c2	od
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
15	[number]	k4	15
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
oddělena	oddělen	k2eAgFnSc1d1	oddělena
samostatná	samostatný	k2eAgFnSc1d1	samostatná
sídliště	sídliště	k1gNnSc1	sídliště
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Salzachu	Salzach	k1gInSc2	Salzach
<g/>
,	,	kIx,	,
Iuvavum	Iuvavum	k1gNnSc1	Iuvavum
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
45	[number]	k4	45
dostalo	dostat	k5eAaPmAgNnS	dostat
město	město	k1gNnSc1	město
právo	právo	k1gNnSc1	právo
municipia	municipium	k1gNnSc2	municipium
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gMnSc1	biskup
Rupert	Rupert	k1gMnSc1	Rupert
Salzburský	salzburský	k2eAgMnSc1d1	salzburský
dostal	dostat	k5eAaPmAgMnS	dostat
roku	rok	k1gInSc2	rok
699	[number]	k4	699
darem	dar	k1gInSc7	dar
od	od	k7c2	od
bavorského	bavorský	k2eAgMnSc2d1	bavorský
knížete	kníže	k1gMnSc2	kníže
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
starého	starý	k2eAgNnSc2d1	staré
římského	římský	k2eAgNnSc2d1	římské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
misii	misie	k1gFnSc4	misie
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvolil	zvolit	k5eAaPmAgMnS	zvolit
si	se	k3xPyFc3	se
klášter	klášter	k1gInSc4	klášter
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
útočiště	útočiště	k1gNnSc4	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
doložen	doložen	k2eAgInSc1d1	doložen
roku	rok	k1gInSc3	rok
755	[number]	k4	755
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
739	[number]	k4	739
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
salcburské	salcburský	k2eAgNnSc1d1	salcburské
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
biskupa	biskup	k1gInSc2	biskup
a	a	k8xC	a
roku	rok	k1gInSc2	rok
774	[number]	k4	774
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
první	první	k4xOgFnSc1	první
salcburská	salcburský	k2eAgFnSc1d1	Salcburská
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
798	[number]	k4	798
byla	být	k5eAaImAgFnS	být
diecéze	diecéze	k1gFnSc1	diecéze
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
franského	franský	k2eAgMnSc2d1	franský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
povýšena	povýšen	k2eAgFnSc1d1	povýšena
papežem	papež	k1gMnSc7	papež
Lvem	Lev	k1gMnSc7	Lev
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c6	na
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
území	území	k1gNnSc1	území
církevního	církevní	k2eAgInSc2d1	církevní
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc1d1	celé
starobavorské	starobavorský	k2eAgNnSc1d1	starobavorský
kmenové	kmenový	k2eAgNnSc1d1	kmenové
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
Dolní	dolní	k2eAgInPc1d1	dolní
a	a	k8xC	a
Horní	horní	k2eAgInPc1d1	horní
Bavory	Bavory	k1gInPc1	Bavory
<g/>
,	,	kIx,	,
a	a	k8xC	a
Horní	horní	k2eAgFnSc1d1	horní
Falc	Falc	k1gFnSc1	Falc
<g/>
,	,	kIx,	,
a	a	k8xC	a
též	též	k9	též
většinu	většina	k1gFnSc4	většina
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Salcburk	Salcburk	k1gInSc1	Salcburk
poté	poté	k6eAd1	poté
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
východofranské	východofranský	k2eAgFnSc3d1	Východofranská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Svatou	svatý	k2eAgFnSc4d1	svatá
říši	říše	k1gFnSc4	říše
římskou	římský	k2eAgFnSc4d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
pevnosti	pevnost	k1gFnSc2	pevnost
Hohensalzburg	Hohensalzburg	k1gInSc1	Hohensalzburg
byla	být	k5eAaImAgFnS	být
započata	započnout	k5eAaPmNgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1077	[number]	k4	1077
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Gebharda	Gebhard	k1gMnSc2	Gebhard
von	von	k1gInSc1	von
Helfenstein	Helfenstein	k2eAgInSc1d1	Helfenstein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
jeho	on	k3xPp3gMnSc2	on
nástupce	nástupce	k1gMnSc2	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Gebhard	Gebhard	k1gMnSc1	Gebhard
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1076	[number]	k4	1076
angažoval	angažovat	k5eAaBmAgMnS	angažovat
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
o	o	k7c4	o
investituru	investitura	k1gFnSc4	investitura
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1077	[number]	k4	1077
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
samozvaného	samozvaný	k2eAgMnSc2d1	samozvaný
krále	král	k1gMnSc2	král
Rudolfa	Rudolf	k1gMnSc2	Rudolf
z	z	k7c2	z
Rheinfelden	Rheinfeldna	k1gFnPc2	Rheinfeldna
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
sporu	spor	k1gInSc2	spor
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nevěrnosti	nevěrnost	k1gFnSc2	nevěrnost
císaři	císař	k1gMnSc3	císař
Jindřichu	Jindřich	k1gMnSc3	Jindřich
IV	IV	kA	IV
<g/>
..	..	k?	..
Po	po	k7c6	po
říšské	říšský	k2eAgFnSc6d1	říšská
klatbě	klatba	k1gFnSc6	klatba
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
císař	císař	k1gMnSc1	císař
Friedrich	Friedrich	k1gMnSc1	Friedrich
I.	I.	kA	I.
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
uvalil	uvalit	k5eAaPmAgInS	uvalit
roku	rok	k1gInSc2	rok
1166	[number]	k4	1166
na	na	k7c4	na
Salzburg	Salzburg	k1gInSc4	Salzburg
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Babenberský	babenberský	k2eAgInSc1d1	babenberský
(	(	kIx(	(
<g/>
Konrad	Konrad	k1gInSc1	Konrad
II	II	kA	II
<g/>
.	.	kIx.	.
von	von	k1gInSc1	von
Babenberg	Babenberg	k1gInSc1	Babenberg
<g/>
)	)	kIx)	)
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnPc4	vláda
nad	nad	k7c7	nad
Salzburkem	Salzburk	k1gInSc7	Salzburk
bez	bez	k7c2	bez
udělení	udělení	k1gNnSc2	udělení
císařského	císařský	k2eAgNnSc2d1	císařské
léna	léno	k1gNnSc2	léno
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
z	z	k7c2	z
moci	moc	k1gFnSc2	moc
císařova	císařův	k2eAgMnSc4d1	císařův
důvěrníka	důvěrník	k1gMnSc4	důvěrník
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Plainu	Plain	k1gInSc2	Plain
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1167	[number]	k4	1167
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1168	[number]	k4	1168
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
zvolen	zvolen	k2eAgMnSc1d1	zvolen
nejprve	nejprve	k6eAd1	nejprve
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
císařův	císařův	k2eAgMnSc1d1	císařův
bratranec	bratranec	k1gMnSc1	bratranec
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
nepožádal	požádat	k5eNaPmAgMnS	požádat
o	o	k7c4	o
nezbytné	zbytný	k2eNgFnPc4d1	zbytný
regálie	regálie	k1gFnPc4	regálie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1174	[number]	k4	1174
opět	opět	k6eAd1	opět
stálým	stálý	k2eAgMnSc7d1	stálý
řezenským	řezenský	k2eAgInSc7d1	řezenský
říšským	říšský	k2eAgInSc7d1	říšský
sněmem	sněm	k1gInSc7	sněm
zbaven	zbavit	k5eAaPmNgMnS	zbavit
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zvolen	zvolit	k5eAaPmNgInS	zvolit
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jako	jako	k8xS	jako
říšský	říšský	k2eAgMnSc1d1	říšský
probošt	probošt	k1gMnSc1	probošt
v	v	k7c4	v
Berchtesgadenu	Berchtesgaden	k2eAgFnSc4d1	Berchtesgaden
působící	působící	k2eAgFnSc4d1	působící
Heinrich	Heinrich	k1gMnSc1	Heinrich
I.	I.	kA	I.
(	(	kIx(	(
<g/>
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
<g/>
)	)	kIx)	)
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
samozvaný	samozvaný	k2eAgMnSc1d1	samozvaný
<g/>
"	"	kIx"	"
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
potvrzený	potvrzený	k2eAgInSc4d1	potvrzený
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
benátským	benátský	k2eAgInSc7d1	benátský
mírem	mír	k1gInSc7	mír
z	z	k7c2	z
r.	r.	kA	r.
1177	[number]	k4	1177
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
jak	jak	k6eAd1	jak
Jindřich	Jindřich	k1gMnSc1	Jindřich
tak	tak	k6eAd1	tak
i	i	k9	i
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
III	III	kA	III
<g/>
.	.	kIx.	.
zříci	zříct	k5eAaPmF	zříct
úřadu	úřad	k1gInSc3	úřad
a	a	k8xC	a
uvolnit	uvolnit	k5eAaPmF	uvolnit
místo	místo	k1gNnSc4	místo
bývalému	bývalý	k2eAgMnSc3d1	bývalý
mohučskému	mohučský	k2eAgMnSc3d1	mohučský
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
Konrádu	Konrád	k1gMnSc3	Konrád
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Wittelsbachu	Wittelsbach	k1gInSc2	Wittelsbach
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Konrádově	Konrádův	k2eAgNnSc6d1	Konrádovo
znovupovolání	znovupovolání	k1gNnSc6	znovupovolání
do	do	k7c2	do
Mohuče	Mohuč	k1gFnSc2	Mohuč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1183	[number]	k4	1183
<g/>
,	,	kIx,	,
se	s	k7c7	s
solnohradským	solnohradský	k2eAgMnSc7d1	solnohradský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
již	již	k6eAd1	již
definitivně	definitivně	k6eAd1	definitivně
stal	stát	k5eAaPmAgMnS	stát
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1200	[number]	k4	1200
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nástupci	nástupce	k1gMnPc7	nástupce
a	a	k8xC	a
odhodlanému	odhodlaný	k2eAgMnSc3d1	odhodlaný
přívrženci	přívrženec	k1gMnSc3	přívrženec
rodu	rod	k1gInSc2	rod
Štaufů	Štauf	k1gInPc2	Štauf
<g/>
,	,	kIx,	,
arcibiskupovi	arcibiskup	k1gMnSc3	arcibiskup
Eberhardu	Eberhard	k1gMnSc3	Eberhard
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1200	[number]	k4	1200
–	–	k?	–
1246	[number]	k4	1246
podařilo	podařit	k5eAaPmAgNnS	podařit
z	z	k7c2	z
hrabství	hrabství	k1gNnSc2	hrabství
<g/>
,	,	kIx,	,
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
správních	správní	k2eAgInPc2d1	správní
úřadů	úřad	k1gInPc2	úřad
vybudovat	vybudovat	k5eAaPmF	vybudovat
jednotné	jednotný	k2eAgNnSc1d1	jednotné
arcibiskupské	arcibiskupský	k2eAgNnSc1d1	Arcibiskupské
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1275	[number]	k4	1275
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
uznání	uznání	k1gNnSc3	uznání
hranice	hranice	k1gFnSc2	hranice
knížetem	kníže	k1gMnSc7	kníže
bavorským	bavorský	k2eAgMnSc7d1	bavorský
<g/>
,	,	kIx,	,
dospělo	dochvít	k5eAaPmAgNnS	dochvít
odtržení	odtržení	k1gNnSc1	odtržení
Salzburku	Salzburk	k1gInSc2	Salzburk
od	od	k7c2	od
Bavor	Bavory	k1gInPc2	Bavory
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
poslední	poslední	k2eAgFnSc2d1	poslední
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1328	[number]	k4	1328
v	v	k7c4	v
Salzburg	Salzburg	k1gInSc4	Salzburg
bylo	být	k5eAaImAgNnS	být
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
vydáno	vydat	k5eAaPmNgNnS	vydat
Zemské	zemský	k2eAgNnSc1d1	zemské
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
,	,	kIx,	,
upravující	upravující	k2eAgNnSc1d1	upravující
správu	správa	k1gFnSc4	správa
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1348	[number]	k4	1348
<g/>
/	/	kIx~	/
<g/>
49	[number]	k4	49
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
velká	velký	k2eAgFnSc1d1	velká
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
padla	padnout	k5eAaPmAgFnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
téměř	téměř	k6eAd1	téměř
třetina	třetina	k1gFnSc1	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1481	[number]	k4	1481
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
císař	císař	k1gMnSc1	císař
Bedřich	Bedřich	k1gMnSc1	Bedřich
III	III	kA	III
<g/>
.	.	kIx.	.
městu	město	k1gNnSc3	město
Salzburg	Salzburg	k1gInSc1	Salzburg
privilegium	privilegium	k1gNnSc1	privilegium
svobodné	svobodný	k2eAgFnPc1d1	svobodná
volby	volba	k1gFnPc1	volba
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
starostů	starosta	k1gMnPc2	starosta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
salzburský	salzburský	k2eAgInSc1d1	salzburský
pivovar	pivovar	k1gInSc1	pivovar
Stiegl	Stiegla	k1gFnPc2	Stiegla
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejúspěšnějším	úspěšný	k2eAgMnSc6d3	nejúspěšnější
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
soukromých	soukromý	k2eAgInPc2d1	soukromý
pivovarů	pivovar	k1gInPc2	pivovar
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
tradiční	tradiční	k2eAgInSc1d1	tradiční
podnik	podnik	k1gInSc1	podnik
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
Salcburska	Salcbursk	k1gInSc2	Salcbursk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1511	[number]	k4	1511
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Leonhard	Leonhard	k1gMnSc1	Leonhard
von	von	k1gInSc4	von
Keutschach	Keutschach	k1gInSc1	Keutschach
ukončil	ukončit	k5eAaPmAgInS	ukončit
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
síly	síla	k1gFnSc2	síla
dlouholeté	dlouholetý	k2eAgInPc1d1	dlouholetý
rozpory	rozpor	k1gInPc1	rozpor
s	s	k7c7	s
radnicí	radnice	k1gFnSc7	radnice
<g/>
:	:	kIx,	:
nechal	nechat	k5eAaPmAgMnS	nechat
zajmout	zajmout	k5eAaPmF	zajmout
starostu	starosta	k1gMnSc4	starosta
a	a	k8xC	a
městskou	městský	k2eAgFnSc4d1	městská
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
vynutil	vynutit	k5eAaPmAgMnS	vynutit
si	se	k3xPyFc3	se
vydání	vydání	k1gNnSc4	vydání
městských	městský	k2eAgFnPc2d1	městská
výsad	výsada	k1gFnPc2	výsada
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Německé	německý	k2eAgFnSc2d1	německá
selské	selský	k2eAgFnSc2d1	selská
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
letech	let	k1gInPc6	let
1525	[number]	k4	1525
–	–	k?	–
1526	[number]	k4	1526
k	k	k7c3	k
tříměsíčnímu	tříměsíční	k2eAgNnSc3d1	tříměsíční
povstání	povstání	k1gNnSc3	povstání
rolníků	rolník	k1gMnPc2	rolník
a	a	k8xC	a
horníků	horník	k1gMnPc2	horník
v	v	k7c6	v
Salcburku	Salcburk	k1gInSc6	Salcburk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byla	být	k5eAaImAgFnS	být
rolníky	rolník	k1gMnPc4	rolník
obléhána	obléhán	k2eAgFnSc1d1	obléhána
pevnost	pevnost	k1gFnSc1	pevnost
(	(	kIx(	(
<g/>
Festung	Festung	k1gInSc1	Festung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
započala	započnout	k5eAaPmAgFnS	započnout
intenzivní	intenzivní	k2eAgFnPc4d1	intenzivní
barokizace	barokizace	k1gFnPc4	barokizace
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Wolfa	Wolf	k1gMnSc2	Wolf
Dietricha	Dietrich	k1gMnSc2	Dietrich
z	z	k7c2	z
Raitenova	Raitenův	k2eAgNnSc2d1	Raitenův
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dalo	dát	k5eAaPmAgNnS	dát
výraznou	výrazný	k2eAgFnSc4d1	výrazná
novou	nový	k2eAgFnSc4d1	nová
podobu	podoba	k1gFnSc4	podoba
nejen	nejen	k6eAd1	nejen
Salzburku	Salzburka	k1gFnSc4	Salzburka
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
nepřímo	přímo	k6eNd1	přímo
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
barokní	barokní	k2eAgInSc1d1	barokní
sloh	sloh	k1gInSc1	sloh
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
stavební	stavební	k2eAgMnSc1d1	stavební
mistr	mistr	k1gMnSc1	mistr
pro	pro	k7c4	pro
salcburskou	salcburský	k2eAgFnSc4d1	Salcburská
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
roku	rok	k1gInSc3	rok
1598	[number]	k4	1598
již	již	k9	již
poosmé	poosmé	k4xO	poosmé
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nejprve	nejprve	k6eAd1	nejprve
povolán	povolat	k5eAaPmNgMnS	povolat
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Scamozzi	Scamozze	k1gFnSc6	Scamozze
a	a	k8xC	a
po	po	k7c6	po
sesazení	sesazení	k1gNnSc6	sesazení
Wolfa	Wolf	k1gMnSc4	Wolf
Dietricha	Dietrich	k1gMnSc4	Dietrich
pak	pak	k6eAd1	pak
italský	italský	k2eAgMnSc1d1	italský
mistr	mistr	k1gMnSc1	mistr
Santino	Santin	k2eAgNnSc4d1	Santino
Solari	Solari	k1gNnSc4	Solari
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1628	[number]	k4	1628
postavil	postavit	k5eAaPmAgInS	postavit
kompletní	kompletní	k2eAgInSc1d1	kompletní
malý	malý	k2eAgInSc1d1	malý
dóm	dóm	k1gInSc1	dóm
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
věží	věž	k1gFnPc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Rozepře	rozepře	k1gFnSc1	rozepře
s	s	k7c7	s
Bavory	Bavory	k1gInPc7	Bavory
ohledně	ohledně	k7c2	ohledně
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
mýtného	mýtný	k1gMnSc2	mýtný
vedly	vést	k5eAaImAgInP	vést
r.	r.	kA	r.
1611	[number]	k4	1611
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
solné	solný	k2eAgFnSc6d1	solná
válce	válka	k1gFnSc6	válka
mezi	mezi	k7c7	mezi
Solnohradskem	Solnohradsko	k1gNnSc7	Solnohradsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bavorskem	Bavorsko	k1gNnSc7	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Wolf	Wolf	k1gMnSc1	Wolf
Dietrich	Dietrich	k1gMnSc1	Dietrich
obsadil	obsadit	k5eAaPmAgMnS	obsadit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
říšské	říšský	k2eAgNnSc4d1	říšské
proboštství	proboštství	k1gNnSc4	proboštství
Berchtesgaden	Berchtesgaden	k2eAgMnSc1d1	Berchtesgaden
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
bavorské	bavorský	k2eAgNnSc1d1	bavorské
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
24	[number]	k4	24
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Salzburg	Salzburg	k1gInSc4	Salzburg
a	a	k8xC	a
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
arcibiskupovo	arcibiskupův	k2eAgNnSc4d1	arcibiskupovo
sesazení	sesazení	k1gNnSc4	sesazení
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
volbu	volba	k1gFnSc4	volba
Marka	Marek	k1gMnSc2	Marek
Sittika	Sittik	k1gMnSc2	Sittik
z	z	k7c2	z
Hohenemsu	Hohenems	k1gInSc2	Hohenems
<g/>
.	.	kIx.	.
</s>
<s>
Markovu	Markův	k2eAgMnSc3d1	Markův
nástupci	nástupce	k1gMnSc3	nástupce
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
též	též	k6eAd1	též
poslednímu	poslední	k2eAgInSc3d1	poslední
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
barokních	barokní	k2eAgMnPc2d1	barokní
arcibiskupů	arcibiskup	k1gMnPc2	arcibiskup
Salzburku	Salzburk	k1gInSc2	Salzburk
<g/>
,	,	kIx,	,
hraběti	hrabě	k1gMnSc3	hrabě
Paridovi	Paris	k1gMnSc3	Paris
z	z	k7c2	z
Lodronu	Lodron	k1gInSc2	Lodron
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
udržení	udržení	k1gNnSc3	udržení
chladné	chladný	k2eAgFnSc2d1	chladná
politiky	politika	k1gFnSc2	politika
vůči	vůči	k7c3	vůči
Bavorsku	Bavorsko	k1gNnSc3	Bavorsko
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
díky	díky	k7c3	díky
dalekosáhle	dalekosáhle	k6eAd1	dalekosáhle
všeobecně	všeobecně	k6eAd1	všeobecně
neutrální	neutrální	k2eAgFnSc6d1	neutrální
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Salzburg	Salzburg	k1gInSc1	Salzburg
nezúčastnil	zúčastnit	k5eNaPmAgInS	zúčastnit
vřavy	vřava	k1gFnSc2	vřava
30	[number]	k4	30
<g/>
leté	letý	k2eAgFnSc2d1	letá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
předstupeň	předstupeň	k1gInSc1	předstupeň
salcburské	salcburský	k2eAgFnSc2d1	Salcburská
univerzity	univerzita	k1gFnSc2	univerzita
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1617	[number]	k4	1617
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
když	když	k8xS	když
dřívější	dřívější	k2eAgFnPc1d1	dřívější
snahy	snaha	k1gFnPc1	snaha
ztroskotaly	ztroskotat	k5eAaPmAgFnP	ztroskotat
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1622	[number]	k4	1622
bylo	být	k5eAaImAgNnS	být
gymnázium	gymnázium	k1gNnSc1	gymnázium
přeměněno	přeměnit	k5eAaPmNgNnS	přeměnit
na	na	k7c4	na
Univerzitu	univerzita	k1gFnSc4	univerzita
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Universität	Universität	k2eAgInSc1d1	Universität
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
s	s	k7c7	s
teologickou	teologický	k2eAgFnSc7d1	teologická
a	a	k8xC	a
filozofickou	filozofický	k2eAgFnSc7d1	filozofická
fakultou	fakulta	k1gFnSc7	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
založení	založení	k1gNnSc6	založení
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
snahu	snaha	k1gFnSc4	snaha
salcburského	salcburský	k2eAgMnSc2d1	salcburský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
o	o	k7c4	o
protireformační	protireformační	k2eAgFnSc4d1	protireformační
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
Dürnberských	Dürnberský	k2eAgInPc6d1	Dürnberský
dolech	dol	k1gInPc6	dol
mnoho	mnoho	k6eAd1	mnoho
dělníků	dělník	k1gMnPc2	dělník
z	z	k7c2	z
jiných	jiný	k1gMnPc2	jiný
části	část	k1gFnSc2	část
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
např.	např.	kA	např.
také	také	k9	také
Míšeňáci	Míšeňák	k1gMnPc1	Míšeňák
(	(	kIx(	(
<g/>
Kurfiřtství	kurfiřtství	k1gNnSc1	kurfiřtství
Saské	saský	k2eAgNnSc1d1	Saské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přivezli	přivézt	k5eAaPmAgMnP	přivézt
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
luteránskou	luteránský	k2eAgFnSc4d1	luteránská
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
víra	víra	k1gFnSc1	víra
se	se	k3xPyFc4	se
odsud	odsud	k6eAd1	odsud
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
mezi	mezi	k7c7	mezi
místní	místní	k2eAgFnSc7d1	místní
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nespokojené	spokojený	k2eNgNnSc1d1	nespokojené
zemské	zemský	k2eAgNnSc1d1	zemské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
;	;	kIx,	;
také	také	k9	také
salzburští	salzburský	k2eAgMnPc1d1	salzburský
obchodníci	obchodník	k1gMnPc1	obchodník
do	do	k7c2	do
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
dovezli	dovézt	k5eAaPmAgMnP	dovézt
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
Lutherovy	Lutherův	k2eAgFnSc2d1	Lutherova
ideje	idea	k1gFnSc2	idea
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
už	už	k6eAd1	už
22	[number]	k4	22
000	[number]	k4	000
protestantů	protestant	k1gMnPc2	protestant
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
malých	malý	k2eAgFnPc6d1	malá
skupinkách	skupinka	k1gFnPc6	skupinka
vyhoštěno	vyhoštěn	k2eAgNnSc1d1	vyhoštěno
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
nakonec	nakonec	k6eAd1	nakonec
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Leopold	Leopold	k1gMnSc1	Leopold
Anton	Anton	k1gMnSc1	Anton
von	von	k1gInSc4	von
Firmian	Firmian	k1gMnSc1	Firmian
roku	rok	k1gInSc2	rok
1731	[number]	k4	1731
Emigrační	emigrační	k2eAgInSc1d1	emigrační
patent	patent	k1gInSc1	patent
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
opustilo	opustit	k5eAaPmAgNnS	opustit
svoji	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
přes	přes	k7c4	přes
20	[number]	k4	20
000	[number]	k4	000
salzburských	salzburský	k2eAgMnPc2d1	salzburský
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
platným	platný	k2eAgNnSc7d1	platné
říšským	říšský	k2eAgNnSc7d1	říšské
právem	právo	k1gNnSc7	právo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
principu	princip	k1gInSc2	princip
"	"	kIx"	"
<g/>
cuius	cuius	k1gMnSc1	cuius
regio	regio	k1gMnSc1	regio
<g/>
,	,	kIx,	,
eius	eius	k6eAd1	eius
religio	religio	k6eAd1	religio
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
koho	kdo	k3yQnSc4	kdo
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
víra	víra	k1gFnSc1	víra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
velikému	veliký	k2eAgNnSc3d1	veliké
množství	množství	k1gNnSc3	množství
protikatolických	protikatolický	k2eAgInPc2d1	protikatolický
letáků	leták	k1gInPc2	leták
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
veliký	veliký	k2eAgInSc4d1	veliký
rozruch	rozruch	k1gInSc4	rozruch
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
církevních	církevní	k2eAgInPc2d1	církevní
motivů	motiv	k1gInPc2	motiv
byla	být	k5eAaImAgFnS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
též	též	k9	též
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
nemajetné	majetný	k2eNgFnPc4d1	nemajetná
poddané	poddaná	k1gFnPc4	poddaná
<g/>
,	,	kIx,	,
naděje	naděje	k1gFnPc4	naděje
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vycestovali	vycestovat	k5eAaPmAgMnP	vycestovat
do	do	k7c2	do
východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
darem	dar	k1gInSc7	dar
od	od	k7c2	od
pruského	pruský	k2eAgMnSc2d1	pruský
krále	král	k1gMnSc2	král
zem	zem	k1gFnSc1	zem
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
rolníky	rolník	k1gMnPc4	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
mělo	mít	k5eAaImAgNnS	mít
toto	tento	k3xDgNnSc4	tento
velké	velký	k2eAgNnSc1d1	velké
vysídlování	vysídlování	k1gNnSc1	vysídlování
katastrofální	katastrofální	k2eAgInPc1d1	katastrofální
hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
následky	následek	k1gInPc1	následek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
však	však	k9	však
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
Firmiana	Firmian	k1gMnSc4	Firmian
neodradily	odradit	k5eNaPmAgFnP	odradit
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1736	[number]	k4	1736
až	až	k9	až
1738	[number]	k4	1738
nepostavil	postavit	k5eNaPmAgInS	postavit
rokokový	rokokový	k2eAgInSc1d1	rokokový
zámek	zámek	k1gInSc1	zámek
Leopoldskron	Leopoldskron	k1gInSc1	Leopoldskron
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zámek	zámek	k1gInSc1	zámek
pak	pak	k6eAd1	pak
od	od	k7c2	od
r.	r.	kA	r.
1918	[number]	k4	1918
patřil	patřit	k5eAaImAgInS	patřit
divadelnímu	divadelní	k2eAgMnSc3d1	divadelní
režisérovi	režisér	k1gMnSc3	režisér
Maxovi	Max	k1gMnSc3	Max
Reinhardtovi	Reinhardt	k1gMnSc3	Reinhardt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
vyvlastněn	vyvlastnit	k5eAaPmNgInS	vyvlastnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
navrácení	navrácení	k1gNnSc6	navrácení
zámku	zámek	k1gInSc2	zámek
rodině	rodina	k1gFnSc3	rodina
Reinhardtových	Reinhardtových	k2eAgMnSc2d1	Reinhardtových
ho	on	k3xPp3gMnSc4	on
majitelé	majitel	k1gMnPc1	majitel
prodali	prodat	k5eAaPmAgMnP	prodat
Salcburskému	salcburský	k2eAgInSc3d1	salcburský
semináři	seminář	k1gInSc3	seminář
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
užívá	užívat	k5eAaImIp3nS	užívat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1756	[number]	k4	1756
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1769	[number]	k4	1769
až	až	k9	až
1781	[number]	k4	1781
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1772	[number]	k4	1772
–	–	k?	–
1803	[number]	k4	1803
<g/>
,	,	kIx,	,
za	za	k7c2	za
úřadování	úřadování	k1gNnSc2	úřadování
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Jeronyma	Jeronym	k1gMnSc2	Jeronym
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
z	z	k7c2	z
Colloredo-Mannsfeldu	Colloredo-Mannsfeld	k1gInSc2	Colloredo-Mannsfeld
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Salzburg	Salzburg	k1gInSc1	Salzburg
centrem	centrum	k1gNnSc7	centrum
pozdního	pozdní	k2eAgNnSc2d1	pozdní
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Školství	školství	k1gNnSc1	školství
bylo	být	k5eAaImAgNnS	být
reformováno	reformovat	k5eAaBmNgNnS	reformovat
podle	podle	k7c2	podle
rakouského	rakouský	k2eAgInSc2d1	rakouský
vzoru	vzor	k1gInSc2	vzor
a	a	k8xC	a
do	do	k7c2	do
Salzburku	Salzburk	k1gInSc2	Salzburk
bylo	být	k5eAaImAgNnS	být
povoláno	povolat	k5eAaPmNgNnS	povolat
množství	množství	k1gNnSc1	množství
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Říšským	říšský	k2eAgInSc7d1	říšský
deputačním	deputační	k2eAgInSc7d1	deputační
výnosem	výnos	k1gInSc7	výnos
(	(	kIx(	(
<g/>
Reichsdeputationshauptschluss	Reichsdeputationshauptschluss	k1gInSc1	Reichsdeputationshauptschluss
<g/>
)	)	kIx)	)
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
platnosti	platnost	k1gFnSc6	platnost
světská	světský	k2eAgFnSc1d1	světská
moc	moc	k1gFnSc1	moc
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
salzburského	salzburský	k2eAgMnSc2d1	salzburský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
byl	být	k5eAaImAgInS	být
Salzburg	Salzburg	k1gInSc1	Salzburg
jako	jako	k8xC	jako
sekularizované	sekularizovaný	k2eAgNnSc1d1	sekularizované
kurfiřtství	kurfiřtství	k1gNnSc1	kurfiřtství
sjednocen	sjednocen	k2eAgMnSc1d1	sjednocen
s	s	k7c7	s
Freisingem	Freising	k1gInSc7	Freising
a	a	k8xC	a
Pasovem	Pasov	k1gInSc7	Pasov
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
býv.	býv.	k?	býv.
toskánského	toskánský	k2eAgMnSc2d1	toskánský
velkovévody	velkovévoda	k1gMnSc2	velkovévoda
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
kurfiřtství	kurfiřtství	k1gNnSc4	kurfiřtství
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
sjednoceno	sjednotit	k5eAaPmNgNnS	sjednotit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městem	město	k1gNnSc7	město
Berchtesgaden	Berchtesgaden	k2eAgMnSc1d1	Berchtesgaden
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1810	[number]	k4	1810
–	–	k?	–
1816	[number]	k4	1816
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
Bavorsku	Bavorsko	k1gNnSc3	Bavorsko
a	a	k8xC	a
po	po	k7c6	po
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
připadl	připadnout	k5eAaPmAgInS	připadnout
Salzburg	Salzburg	k1gInSc1	Salzburg
bez	bez	k1gInSc1	bez
Berchtesgadenu	Berchtesgaden	k2eAgFnSc4d1	Berchtesgaden
a	a	k8xC	a
západního	západní	k2eAgInSc2d1	západní
Flachgau	Flachgaus	k1gInSc2	Flachgaus
(	(	kIx(	(
<g/>
Rupertiwinkel	Rupertiwinkel	k1gInSc1	Rupertiwinkel
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
Rakousku	Rakousko	k1gNnSc3	Rakousko
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
Horních	horní	k2eAgInPc2d1	horní
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
doposud	doposud	k6eAd1	doposud
spravován	spravovat	k5eAaImNgInS	spravovat
z	z	k7c2	z
Lince	Linec	k1gInSc2	Linec
<g/>
,	,	kIx,	,
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
korunní	korunní	k2eAgFnSc7d1	korunní
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
vévodství	vévodství	k1gNnSc2	vévodství
a	a	k8xC	a
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
císařské	císařský	k2eAgNnSc1d1	císařské
místodržitelství	místodržitelství	k1gNnSc1	místodržitelství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
byly	být	k5eAaImAgFnP	být
strženy	stržen	k2eAgFnPc1d1	stržena
městské	městský	k2eAgFnPc1d1	městská
hradby	hradba	k1gFnPc1	hradba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
mohlo	moct	k5eAaImAgNnS	moct
dále	daleko	k6eAd2	daleko
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
salcburské	salcburský	k2eAgFnSc2d1	Salcburská
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
Salzburger	Salzburger	k1gMnSc1	Salzburger
Straßenbahn	Straßenbahn	k1gMnSc1	Straßenbahn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konaly	konat	k5eAaImAgFnP	konat
volby	volba	k1gFnPc1	volba
podle	podle	k7c2	podle
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
hudební	hudební	k2eAgInSc1d1	hudební
Salcburský	salcburský	k2eAgInSc1d1	salcburský
festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1921	[number]	k4	1921
bylo	být	k5eAaImAgNnS	být
vypsáno	vypsán	k2eAgNnSc1d1	vypsáno
referendum	referendum	k1gNnSc1	referendum
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
Anschluss	Anschluss	k1gInSc4	Anschluss
(	(	kIx(	(
<g/>
anexe	anexe	k1gFnPc4	anexe
<g/>
,	,	kIx,	,
připojení	připojení	k1gNnSc4	připojení
<g/>
)	)	kIx)	)
Německého	německý	k2eAgNnSc2d1	německé
Rakouska	Rakousko	k1gNnSc2	Rakousko
k	k	k7c3	k
republikánské	republikánský	k2eAgFnSc3d1	republikánská
(	(	kIx(	(
<g/>
výmarské	výmarský	k2eAgFnSc3d1	Výmarská
<g/>
)	)	kIx)	)
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
salzburském	salzburský	k2eAgInSc6d1	salzburský
kolejním	kolejní	k2eAgInSc6d1	kolejní
kostele	kostel	k1gInSc6	kostel
"	"	kIx"	"
<g/>
Veliké	veliký	k2eAgNnSc1d1	veliké
salcburské	salcburský	k2eAgNnSc1d1	salcburské
Theatrum	Theatrum	k1gNnSc1	Theatrum
mundi	mund	k1gMnPc1	mund
<g/>
"	"	kIx"	"
od	od	k7c2	od
Huga	Hugo	k1gMnSc2	Hugo
z	z	k7c2	z
Hofmannsthalu	Hofmannsthal	k1gInSc2	Hofmannsthal
<g/>
,	,	kIx,	,
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Maxe	Max	k1gMnSc2	Max
Reinhardta	Reinhardt	k1gMnSc2	Reinhardt
<g/>
.	.	kIx.	.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1	okolnost
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterých	který	k3yIgInPc2	který
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
představení	představení	k1gNnSc1	představení
provedeno	provést	k5eAaPmNgNnS	provést
<g/>
,	,	kIx,	,
přimělo	přimět	k5eAaPmAgNnS	přimět
Karla	Karel	k1gMnSc4	Karel
Krause	Kraus	k1gMnSc4	Kraus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
nacismu	nacismus	k1gInSc2	nacismus
došlo	dojít	k5eAaPmAgNnS	dojít
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Residenzplatz	Residenzplatza	k1gFnPc2	Residenzplatza
k	k	k7c3	k
pálení	pálení	k1gNnSc3	pálení
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
zatýkání	zatýkání	k1gNnPc2	zatýkání
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
politických	politický	k2eAgMnPc2d1	politický
protivníků	protivník	k1gMnPc2	protivník
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Křišťálové	křišťálový	k2eAgFnSc6d1	Křišťálová
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
salcburské	salcburský	k2eAgFnSc2d1	Salcburská
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Sankt	Sankt	k1gInSc1	Sankt
Johann	Johann	k1gInSc4	Johann
im	im	k?	im
Pongau	Pongaus	k1gInSc2	Pongaus
zřízen	zřídit	k5eAaPmNgInS	zřídit
tábor	tábor	k1gInSc1	tábor
pro	pro	k7c4	pro
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
nechal	nechat	k5eAaPmAgMnS	nechat
oblastní	oblastní	k2eAgMnSc1d1	oblastní
vůdce	vůdce	k1gMnSc1	vůdce
Gustav	Gustav	k1gMnSc1	Gustav
Adolf	Adolf	k1gMnSc1	Adolf
Scheel	Scheel	k1gMnSc1	Scheel
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
vystavět	vystavět	k5eAaPmF	vystavět
bunkr	bunkr	k1gInSc4	bunkr
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
1944	[number]	k4	1944
<g/>
/	/	kIx~	/
<g/>
45	[number]	k4	45
<g/>
:	:	kIx,	:
Celkem	celkem	k6eAd1	celkem
15	[number]	k4	15
bombových	bombový	k2eAgInPc2d1	bombový
útoků	útok	k1gInPc2	útok
amerického	americký	k2eAgNnSc2d1	americké
letectva	letectvo	k1gNnSc2	letectvo
zničilo	zničit	k5eAaPmAgNnS	zničit
nebo	nebo	k8xC	nebo
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
46	[number]	k4	46
procent	procento	k1gNnPc2	procento
městské	městský	k2eAgFnSc2d1	městská
zástavby	zástavba	k1gFnSc2	zástavba
se	se	k3xPyFc4	se
7	[number]	k4	7
600	[number]	k4	600
byty	byt	k1gInPc7	byt
<g/>
;	;	kIx,	;
14	[number]	k4	14
563	[number]	k4	563
osob	osoba	k1gFnPc2	osoba
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bez	bez	k7c2	bez
přístřeší	přístřeší	k1gNnSc2	přístřeší
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
550	[number]	k4	550
lidí	člověk	k1gMnPc2	člověk
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Zasažena	zasažen	k2eAgFnSc1d1	zasažena
byla	být	k5eAaImAgFnS	být
především	především	k9	především
železniční	železniční	k2eAgFnPc4d1	železniční
stanice	stanice	k1gFnPc4	stanice
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
i	i	k9	i
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
včetně	včetně	k7c2	včetně
Domkuppelu	Domkuppel	k1gInSc2	Domkuppel
a	a	k8xC	a
Mozartova	Mozartův	k2eAgInSc2d1	Mozartův
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
útoky	útok	k1gInPc1	útok
postihly	postihnout	k5eAaPmAgInP	postihnout
i	i	k9	i
Grödig	Grödiga	k1gFnPc2	Grödiga
<g/>
,	,	kIx,	,
Hallein	Halleina	k1gFnPc2	Halleina
<g/>
,	,	kIx,	,
Bischofshofen	Bischofshofna	k1gFnPc2	Bischofshofna
a	a	k8xC	a
Schwarzach	Schwarzacha	k1gFnPc2	Schwarzacha
im	im	k?	im
Pongau	Pongaus	k1gInSc2	Pongaus
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
vpochodovaly	vpochodovat	k5eAaPmAgFnP	vpochodovat
americké	americký	k2eAgFnPc1d1	americká
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
předáno	předat	k5eAaPmNgNnS	předat
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
místním	místní	k2eAgMnSc7d1	místní
velitelem	velitel	k1gMnSc7	velitel
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
Hansem	Hans	k1gMnSc7	Hans
Lepperdingerem	Lepperdinger	k1gMnSc7	Lepperdinger
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
ustavili	ustavit	k5eAaPmAgMnP	ustavit
Richarda	Richard	k1gMnSc4	Richard
Hildmanna	Hildmann	k1gMnSc4	Hildmann
starostou	starosta	k1gMnSc7	starosta
a	a	k8xC	a
Adolfa	Adolf	k1gMnSc2	Adolf
Schemela	Schemela	k1gFnSc1	Schemela
zemským	zemský	k2eAgMnSc7d1	zemský
hejtmanem	hejtman	k1gMnSc7	hejtman
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
SPÖ	SPÖ	kA	SPÖ
<g/>
,	,	kIx,	,
ÖVP	ÖVP	kA	ÖVP
a	a	k8xC	a
KPÖ	KPÖ	kA	KPÖ
<g/>
.	.	kIx.	.
</s>
<s>
Uprchlické	uprchlický	k2eAgFnPc1d1	uprchlická
vlny	vlna	k1gFnPc1	vlna
lidí	člověk	k1gMnPc2	člověk
bez	bez	k7c2	bez
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
(	(	kIx(	(
<g/>
Displaced	Displaced	k1gInSc1	Displaced
Persons	Persons	k1gInSc1	Persons
–	–	k?	–
DP	DP	kA	DP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
občané	občan	k1gMnPc1	občan
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
města	město	k1gNnSc2	město
Salzburku	Salzburk	k1gInSc2	Salzburk
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1946	[number]	k4	1946
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
asi	asi	k9	asi
13	[number]	k4	13
200	[number]	k4	200
uprchlíků	uprchlík	k1gMnPc2	uprchlík
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c6	na
území	území	k1gNnSc6	území
Salzburku	Salzburk	k1gInSc2	Salzburk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
umístění	umístění	k1gNnSc3	umístění
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
DP-Lager	DP-Lager	k1gInSc1	DP-Lager
(	(	kIx(	(
<g/>
3	[number]	k4	3
trvalé	trvalý	k2eAgInPc4d1	trvalý
tábory	tábor	k1gInPc4	tábor
a	a	k8xC	a
5	[number]	k4	5
přechodných	přechodný	k2eAgFnPc2d1	přechodná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byl	být	k5eAaImAgMnS	být
Lager	Lager	k1gMnSc1	Lager
Parsch	Parsch	k1gMnSc1	Parsch
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgMnPc1d1	někdejší
Nacionální	nacionální	k2eAgMnPc1d1	nacionální
socialisté	socialist	k1gMnPc1	socialist
byli	být	k5eAaImAgMnP	být
internováni	internovat	k5eAaBmNgMnP	internovat
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Glasenbach	Glasenbacha	k1gFnPc2	Glasenbacha
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
první	první	k4xOgNnSc1	první
poválečné	poválečný	k2eAgNnSc1d1	poválečné
představení	představení	k1gNnSc1	představení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hudebního	hudební	k2eAgInSc2d1	hudební
Salcburského	salcburský	k2eAgInSc2d1	salcburský
festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
byl	být	k5eAaImAgInS	být
Salzburg	Salzburg	k1gInSc1	Salzburg
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
zemské	zemský	k2eAgFnSc2d1	zemská
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
Salzburg	Salzburg	k1gInSc1	Salzburg
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
pro	pro	k7c4	pro
znovunastolení	znovunastolení	k1gNnSc4	znovunastolení
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
uznání	uznání	k1gNnSc2	uznání
Vídně	Vídeň	k1gFnSc2	Vídeň
jako	jako	k8xS	jako
správního	správní	k2eAgNnSc2d1	správní
sídla	sídlo	k1gNnSc2	sídlo
Renner	Rennra	k1gFnPc2	Rennra
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Salzburku	Salzburk	k1gInSc2	Salzburk
společně	společně	k6eAd1	společně
s	s	k7c7	s
částí	část	k1gFnSc7	část
Horního	horní	k2eAgNnSc2d1	horní
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
tvořily	tvořit	k5eAaImAgInP	tvořit
americkou	americký	k2eAgFnSc4d1	americká
okupační	okupační	k2eAgFnSc4d1	okupační
zónu	zóna	k1gFnSc4	zóna
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Salzburg	Salzburg	k1gInSc1	Salzburg
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
sídlem	sídlo	k1gNnSc7	sídlo
amerického	americký	k2eAgNnSc2d1	americké
vrchního	vrchní	k2eAgNnSc2d1	vrchní
velení	velení	k1gNnSc2	velení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
zkonfiskovala	zkonfiskovat	k5eAaPmAgFnS	zkonfiskovat
okupační	okupační	k2eAgFnSc1d1	okupační
mocnost	mocnost	k1gFnSc1	mocnost
četné	četný	k2eAgFnSc2d1	četná
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
totální	totální	k2eAgFnSc4d1	totální
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
se	s	k7c7	s
zemskými	zemský	k2eAgInPc7d1	zemský
a	a	k8xC	a
městskými	městský	k2eAgInPc7d1	městský
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
výjimky	výjimka	k1gFnPc4	výjimka
se	se	k3xPyFc4	se
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
shodlo	shodnout	k5eAaPmAgNnS	shodnout
s	s	k7c7	s
okupační	okupační	k2eAgFnSc7d1	okupační
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Poskytnutí	poskytnutí	k1gNnSc1	poskytnutí
veliké	veliký	k2eAgFnSc2d1	veliká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
pomoci	pomoc	k1gFnSc2	pomoc
(	(	kIx(	(
<g/>
Marshallův	Marshallův	k2eAgInSc1d1	Marshallův
plán	plán	k1gInSc1	plán
<g/>
)	)	kIx)	)
a	a	k8xC	a
osobní	osobní	k2eAgInPc1d1	osobní
výdaje	výdaj	k1gInPc1	výdaj
příslušníků	příslušník	k1gMnPc2	příslušník
okupačních	okupační	k2eAgFnPc2d1	okupační
jednotek	jednotka	k1gFnPc2	jednotka
způsobily	způsobit	k5eAaPmAgFnP	způsobit
rychlé	rychlý	k2eAgNnSc4d1	rychlé
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
zotavení	zotavení	k1gNnSc4	zotavení
a	a	k8xC	a
přinesly	přinést	k5eAaPmAgFnP	přinést
Salzburgu	Salzburg	k1gInSc2	Salzburg
přívlastek	přívlastek	k1gInSc1	přívlastek
"	"	kIx"	"
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
Západ	západ	k1gInSc1	západ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
devítileté	devítiletý	k2eAgFnSc6d1	devítiletá
stavební	stavební	k2eAgFnSc6d1	stavební
době	doba	k1gFnSc6	doba
předán	předán	k2eAgMnSc1d1	předán
k	k	k7c3	k
potřebám	potřeba	k1gFnPc3	potřeba
dopravy	doprava	k1gFnSc2	doprava
nový	nový	k2eAgInSc1d1	nový
městský	městský	k2eAgInSc1d1	městský
most	most	k1gInSc1	most
<g/>
<g />
.	.	kIx.	.
</s>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
vložen	vložit	k5eAaPmNgInS	vložit
poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
trolejbusu	trolejbus	k1gInSc2	trolejbus
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
16	[number]	k4	16
<g/>
patrový	patrový	k2eAgInSc1d1	patrový
Hotel	hotel	k1gInSc1	hotel
Europa	Europ	k1gMnSc2	Europ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
Salzburku	Salzburk	k1gInSc2	Salzburk
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
zaznívá	zaznívat	k5eAaImIp3nS	zaznívat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
salcburské	salcburský	k2eAgFnSc2d1	Salcburská
architektury	architektura	k1gFnSc2	architektura
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
stavba	stavba	k1gFnSc1	stavba
označována	označovat	k5eAaImNgFnS	označovat
od	od	k7c2	od
"	"	kIx"	"
<g/>
poskvrny	poskvrna	k1gFnSc2	poskvrna
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
"	"	kIx"	"
až	až	k9	až
po	po	k7c4	po
"	"	kIx"	"
<g/>
ochranyhodný	ochranyhodný	k2eAgInSc1d1	ochranyhodný
dobový	dobový	k2eAgInSc1d1	dobový
dokument	dokument	k1gInSc1	dokument
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1959	[number]	k4	1959
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
první	první	k4xOgFnSc1	první
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
v	v	k7c6	v
znovupostaveném	znovupostavený	k2eAgInSc6d1	znovupostavený
dómu	dóm	k1gInSc6	dóm
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
po	po	k7c6	po
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
150	[number]	k4	150
let	léto	k1gNnPc2	léto
trvajícím	trvající	k2eAgNnSc6d1	trvající
přerušení	přerušení	k1gNnSc6	přerušení
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
Salzburská	salzburský	k2eAgFnSc1d1	Salzburská
univerzita	univerzita	k1gFnSc1	univerzita
obnovena	obnoven	k2eAgFnSc1d1	obnovena
výukou	výuka	k1gFnSc7	výuka
na	na	k7c6	na
katolicko-teologické	katolickoeologický	k2eAgFnSc6d1	katolicko-teologický
a	a	k8xC	a
filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Někdejší	někdejší	k2eAgFnSc1d1	někdejší
filosofická	filosofický	k2eAgFnSc1d1	filosofická
fakulta	fakulta	k1gFnSc1	fakulta
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
fakulty	fakulta	k1gFnPc4	fakulta
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
o	o	k7c4	o
fakultu	fakulta	k1gFnSc4	fakulta
kulturní	kulturní	k2eAgFnSc4d1	kulturní
a	a	k8xC	a
fakultu	fakulta	k1gFnSc4	fakulta
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
,	,	kIx,	,
politologie	politologie	k1gFnSc2	politologie
a	a	k8xC	a
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1963	[number]	k4	1963
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
otevřen	otevřít	k5eAaPmNgInS	otevřít
přestavěný	přestavěný	k2eAgInSc1d1	přestavěný
Malý	malý	k2eAgInSc1d1	malý
festivalový	festivalový	k2eAgInSc1d1	festivalový
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Kleines	Kleines	k1gMnSc1	Kleines
Festspielhaus	Festspielhaus	k1gMnSc1	Festspielhaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g />
.	.	kIx.	.
</s>
<s>
1973	[number]	k4	1973
se	se	k3xPyFc4	se
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Linecká	linecký	k2eAgFnSc1d1	Linecká
ulička	ulička	k1gFnSc1	ulička
<g/>
)	)	kIx)	)
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c6	v
pěší	pěší	k2eAgFnSc6d1	pěší
zónou	zóna	k1gFnSc7	zóna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1986	[number]	k4	1986
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
fakulta	fakulta	k1gFnSc1	fakulta
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
probojoval	probojovat	k5eAaPmAgMnS	probojovat
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
klub	klub	k1gInSc4	klub
Austria	Austrium	k1gNnSc2	Austrium
Salzburg	Salzburg	k1gInSc1	Salzburg
proti	proti	k7c3	proti
Interu	Intero	k1gNnSc3	Intero
Milán	Milán	k1gInSc1	Milán
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1997	[number]	k4	1997
bylo	být	k5eAaImAgNnS	být
salzburské	salzburský	k2eAgNnSc1d1	Salzburské
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
propůjčeno	propůjčen	k2eAgNnSc1d1	propůjčeno
označení	označení	k1gNnSc3	označení
Světové	světový	k2eAgNnSc4d1	světové
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
nový	nový	k2eAgInSc1d1	nový
Kulturní	kulturní	k2eAgInSc1d1	kulturní
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
zahájila	zahájit	k5eAaPmAgFnS	zahájit
činnost	činnost	k1gFnSc1	činnost
Paracelsova	Paracelsův	k2eAgFnSc1d1	Paracelsova
lékařská	lékařský	k2eAgFnSc1d1	lékařská
univerzita	univerzita	k1gFnSc1	univerzita
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
"	"	kIx"	"
<g/>
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
lékařská	lékařský	k2eAgFnSc1d1	lékařská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
"	"	kIx"	"
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
získal	získat	k5eAaPmAgInS	získat
Salzburg	Salzburg	k1gInSc1	Salzburg
díky	díky	k7c3	díky
otevření	otevření	k1gNnSc3	otevření
stadionu	stadion	k1gInSc2	stadion
Wals-Siezenheim	Wals-Siezenheimo	k1gNnPc2	Wals-Siezenheimo
novou	nový	k2eAgFnSc4d1	nová
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
arénu	aréna	k1gFnSc4	aréna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
konalo	konat	k5eAaImAgNnS	konat
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
kopané	kopaná	k1gFnSc6	kopaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
znaku	znak	k1gInSc6	znak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
pocínovaná	pocínovaný	k2eAgFnSc1d1	pocínovaná
městská	městský	k2eAgFnSc1d1	městská
hradba	hradba	k1gFnSc1	hradba
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
městská	městský	k2eAgFnSc1d1	městská
brána	brána	k1gFnSc1	brána
s	s	k7c7	s
otevřenými	otevřený	k2eAgNnPc7d1	otevřené
křídly	křídlo	k1gNnPc7	křídlo
vrat	vrat	k1gInSc1	vrat
a	a	k8xC	a
vytaženou	vytažený	k2eAgFnSc7d1	vytažená
padací	padací	k2eAgFnSc7d1	padací
mříží	mříž	k1gFnSc7	mříž
pod	pod	k7c7	pod
malou	malý	k2eAgFnSc7d1	malá
věžičkou	věžička	k1gFnSc7	věžička
s	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
okny	okno	k1gNnPc7	okno
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
ustupující	ustupující	k2eAgFnSc2d1	ustupující
postranní	postranní	k2eAgFnSc2d1	postranní
části	část	k1gFnSc2	část
valu	val	k1gInSc2	val
mají	mít	k5eAaImIp3nP	mít
každá	každý	k3xTgFnSc1	každý
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
střílně	střílna	k1gFnSc6	střílna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
za	za	k7c7	za
hradbou	hradba	k1gFnSc7	hradba
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nP	tyčit
tři	tři	k4xCgFnPc1	tři
několikapodlažní	několikapodlažní	k2eAgFnPc1d1	několikapodlažní
věže	věž	k1gFnPc1	věž
s	s	k7c7	s
pozlacenými	pozlacený	k2eAgInPc7d1	pozlacený
vrcholky	vrcholek	k1gInPc7	vrcholek
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
<g/>
,	,	kIx,	,
osmiboká	osmiboký	k2eAgFnSc1d1	osmiboká
střední	střední	k2eAgFnSc1d1	střední
věž	věž	k1gFnSc1	věž
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
pozorovateli	pozorovatel	k1gMnPc7	pozorovatel
stěny	stěna	k1gFnSc2	stěna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
lomenicovým	lomenicový	k2eAgNnSc7d1	lomenicový
zakončením	zakončení	k1gNnSc7	zakončení
štítové	štítový	k2eAgNnSc1d1	štítové
okno	okno	k1gNnSc1	okno
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
okno	okno	k1gNnSc1	okno
a	a	k8xC	a
ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
poschodí	poschodí	k1gNnSc6	poschodí
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
oknech	okno	k1gNnPc6	okno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
postranních	postranní	k2eAgFnPc6d1	postranní
kruhových	kruhový	k2eAgFnPc6d1	kruhová
věžích	věž	k1gFnPc6	věž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
horních	horní	k2eAgNnPc6d1	horní
patrech	patro	k1gNnPc6	patro
vidět	vidět	k5eAaImF	vidět
dvě	dva	k4xCgNnPc1	dva
okna	okno	k1gNnPc1	okno
a	a	k8xC	a
v	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
patře	patro	k1gNnSc6	patro
pak	pak	k6eAd1	pak
po	po	k7c6	po
okně	okno	k1gNnSc6	okno
jednom	jeden	k4xCgNnSc6	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Zdivo	zdivo	k1gNnSc1	zdivo
je	být	k5eAaImIp3nS	být
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgFnPc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
nese	nést	k5eAaImIp3nS	nést
město	město	k1gNnSc4	město
také	také	k9	také
vlajku	vlajka	k1gFnSc4	vlajka
v	v	k7c6	v
městských	městský	k2eAgFnPc6d1	městská
barvách	barva	k1gFnPc6	barva
bílo-červené	bílo-červený	k2eAgInPc1d1	bílo-červený
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1996	[number]	k4	1996
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
organizací	organizace	k1gFnPc2	organizace
UNESCO	Unesco	k1gNnSc4	Unesco
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
Světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedinečné	jedinečný	k2eAgNnSc1d1	jedinečné
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
celistvosti	celistvost	k1gFnSc6	celistvost
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
(	(	kIx(	(
<g/>
Stift	Stift	k2eAgInSc1d1	Stift
Sankt	Sankt	k1gInSc1	Sankt
Peter	Peter	k1gMnSc1	Peter
<g/>
)	)	kIx)	)
nás	my	k3xPp1nPc6	my
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
prvního	první	k4xOgInSc2	první
biskupa	biskup	k1gInSc2	biskup
Ruperta	Rupert	k1gMnSc2	Rupert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
prvního	první	k4xOgNnSc2	první
opatství	opatství	k1gNnSc2	opatství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
okolo	okolo	k7c2	okolo
r.	r.	kA	r.
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1147	[number]	k4	1147
vysvěcen	vysvěcen	k2eAgInSc1d1	vysvěcen
dnešní	dnešní	k2eAgInSc1d1	dnešní
klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
Hohensalcburské	Hohensalcburský	k2eAgFnSc2d1	Hohensalcburský
pevnosti	pevnost	k1gFnSc2	pevnost
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
časech	čas	k1gInPc6	čas
boje	boj	k1gInSc2	boj
o	o	k7c4	o
investituru	investitura	k1gFnSc4	investitura
roku	rok	k1gInSc2	rok
1077	[number]	k4	1077
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
,	,	kIx,	,
obléhána	obléhán	k2eAgFnSc1d1	obléhána
<g/>
.	.	kIx.	.
</s>
<s>
Františkánský	františkánský	k2eAgInSc1d1	františkánský
kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
Franziskanerkirche	Franziskanerkirche	k1gInSc1	Franziskanerkirche
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
a	a	k8xC	a
k	k	k7c3	k
hlavní	hlavní	k2eAgFnSc3d1	hlavní
kostelní	kostelní	k2eAgFnSc3d1	kostelní
lodi	loď	k1gFnSc3	loď
v	v	k7c6	v
románském	románský	k2eAgInSc6d1	románský
slohu	sloh	k1gInSc6	sloh
byl	být	k5eAaImAgInS	být
přistavěn	přistavěn	k2eAgInSc1d1	přistavěn
gotický	gotický	k2eAgInSc1d1	gotický
ozvučný	ozvučný	k2eAgInSc1d1	ozvučný
kúr	kúra	k1gFnPc2	kúra
a	a	k8xC	a
věnec	věnec	k1gInSc4	věnec
barokních	barokní	k2eAgFnPc2d1	barokní
kaplí	kaple	k1gFnPc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Ruperta	Ruperta	k1gFnSc1	Ruperta
a	a	k8xC	a
Virgila	Virgila	k1gFnSc1	Virgila
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
místě	místo	k1gNnSc6	místo
stála	stát	k5eAaImAgFnS	stát
původně	původně	k6eAd1	původně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
774	[number]	k4	774
románská	románský	k2eAgFnSc1d1	románská
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1611	[number]	k4	1611
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
přineseno	přinést	k5eAaPmNgNnS	přinést
baroko	baroko	k1gNnSc1	baroko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Obilné	obilný	k2eAgFnSc6d1	obilná
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
hrála	hrát	k5eAaImAgFnS	hrát
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Amadea	Amadeus	k1gMnSc2	Amadeus
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Leopoldskron	Leopoldskron	k1gInSc1	Leopoldskron
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
zámek	zámek	k1gInSc1	zámek
Aigen	Aigen	k1gInSc1	Aigen
zámek	zámek	k1gInSc1	zámek
Arenberg	Arenberg	k1gInSc1	Arenberg
zámek	zámek	k1gInSc1	zámek
Doktorschlössl	Doktorschlössl	k1gInSc1	Doktorschlössl
zámek	zámek	k1gInSc1	zámek
Freisaal	Freisaal	k1gInSc1	Freisaal
zámek	zámek	k1gInSc1	zámek
Hellbrunn	Hellbrunn	k1gInSc1	Hellbrunn
s	s	k7c7	s
parkem	park	k1gInSc7	park
<g/>
,	,	kIx,	,
fontánami	fontána	k1gFnPc7	fontána
a	a	k8xC	a
zoologickou	zoologický	k2eAgFnSc7d1	zoologická
zahradou	zahrada	k1gFnSc7	zahrada
pevnost	pevnost	k1gFnSc1	pevnost
Hohensalzburg	Hohensalzburg	k1gMnSc1	Hohensalzburg
zámek	zámek	k1gInSc1	zámek
Leopoldskron	Leopoldskron	k1gInSc1	Leopoldskron
zámek	zámek	k1gInSc1	zámek
Mirabell	Mirabell	k1gInSc1	Mirabell
Salzburská	salzburský	k2eAgFnSc1d1	Salzburská
residence	residence	k1gFnSc1	residence
zámek	zámek	k1gInSc4	zámek
Anif	Anif	k1gInSc1	Anif
zámek	zámek	k1gInSc1	zámek
Klessheim	Klessheim	k1gInSc1	Klessheim
zámek	zámek	k1gInSc1	zámek
Glanegg	Glanegg	k1gInSc1	Glanegg
Se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
architekturou	architektura	k1gFnSc7	architektura
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
přísné	přísný	k2eAgFnSc3d1	přísná
ochraně	ochrana	k1gFnSc3	ochrana
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
komplexů	komplex	k1gInPc2	komplex
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
někdejší	někdejší	k2eAgFnSc6d1	někdejší
konzervativní	konzervativní	k2eAgFnSc6d1	konzervativní
salcburské	salcburský	k2eAgFnSc6d1	Salcburská
mentalitě	mentalita	k1gFnSc6	mentalita
vypadá	vypadat	k5eAaImIp3nS	vypadat
poněkud	poněkud	k6eAd1	poněkud
skromněji	skromně	k6eAd2	skromně
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
na	na	k7c6	na
městských	městský	k2eAgInPc6d1	městský
okrajích	okraj	k1gInPc6	okraj
některá	některý	k3yIgNnPc1	některý
zajímavá	zajímavý	k2eAgNnPc1d1	zajímavé
moderní	moderní	k2eAgNnPc1d1	moderní
architektonická	architektonický	k2eAgNnPc1d1	architektonické
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Hangar	Hangar	k1gInSc1	Hangar
7	[number]	k4	7
na	na	k7c6	na
salcburském	salcburský	k2eAgNnSc6d1	salcburské
letišti	letiště	k1gNnSc6	letiště
slouží	sloužit	k5eAaImIp3nS	sloužit
Dietrichu	Dietrich	k1gMnSc3	Dietrich
Mateschitzovi	Mateschitz	k1gMnSc3	Mateschitz
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
Létajícím	létající	k2eAgMnPc3d1	létající
býkům	býk	k1gMnPc3	býk
jako	jako	k8xC	jako
veřejně	veřejně	k6eAd1	veřejně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
truhlice	truhlice	k1gFnSc1	truhlice
s	s	k7c7	s
pokladem	poklad	k1gInSc7	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Zvenčí	zvenčí	k6eAd1	zvenčí
nápadně	nápadně	k6eAd1	nápadně
upravený	upravený	k2eAgInSc4d1	upravený
Europark	Europark	k1gInSc4	Europark
v	v	k7c6	v
Taxhamu	Taxham	k1gInSc6	Taxham
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
rozšíření	rozšíření	k1gNnSc2	rozšíření
v	v	k7c6	v
letech	let	k1gInPc6	let
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
oválem	ovál	k1gInSc7	ovál
také	také	k6eAd1	také
divadelní	divadelní	k2eAgInSc4d1	divadelní
sál	sál	k1gInSc4	sál
a	a	k8xC	a
kinosál	kinosál	k1gInSc4	kinosál
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
moderny	moderna	k1gFnSc2	moderna
na	na	k7c6	na
Mönchsberku	Mönchsberk	k1gInSc6	Mönchsberk
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
z	z	k7c2	z
dalekého	daleký	k2eAgNnSc2d1	daleké
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
výrazným	výrazný	k2eAgInSc7d1	výrazný
kontrastem	kontrast	k1gInSc7	kontrast
k	k	k7c3	k
pevnosti	pevnost	k1gFnSc3	pevnost
a	a	k8xC	a
Starému	starý	k2eAgNnSc3d1	staré
Městu	město	k1gNnSc3	město
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
<g/>
.	.	kIx.	.
tepelné	tepelný	k2eAgFnSc2d1	tepelná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Salzburg	Salzburg	k1gInSc1	Salzburg
Střed	střed	k1gInSc1	střed
a	a	k8xC	a
Salzburg	Salzburg	k1gInSc1	Salzburg
Sever	sever	k1gInSc1	sever
a.s.	a.s.	k?	a.s.
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
nově	nově	k6eAd1	nově
uvedeny	uvést	k5eAaPmNgFnP	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zdaleka	zdaleka	k6eAd1	zdaleka
vidět	vidět	k5eAaImF	vidět
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
vysokým	vysoký	k2eAgInPc3d1	vysoký
betonovým	betonový	k2eAgInPc3d1	betonový
komínům	komín	k1gInPc3	komín
<g/>
.	.	kIx.	.
</s>
<s>
Salzburg	Salzburg	k1gInSc4	Salzburg
a.s.	a.s.	k?	a.s.
též	též	k9	též
zřídila	zřídit	k5eAaPmAgFnS	zřídit
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
centrálu	centrála	k1gFnSc4	centrála
v	v	k7c6	v
Schallmoosu	Schallmoos	k1gInSc6	Schallmoos
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
salcburské	salcburský	k2eAgFnSc2d1	Salcburská
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
vysokou	vysoký	k2eAgFnSc7d1	vysoká
halou	hala	k1gFnSc7	hala
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
vysokou	vysoký	k2eAgFnSc7d1	vysoká
zimní	zimní	k2eAgFnSc7d1	zimní
zahradou	zahrada	k1gFnSc7	zahrada
pěkným	pěkný	k2eAgInSc7d1	pěkný
kontrastem	kontrast	k1gInSc7	kontrast
uvnitř	uvnitř	k7c2	uvnitř
rodiny	rodina	k1gFnSc2	rodina
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Mirabellgarten	Mirabellgarten	k2eAgInSc4d1	Mirabellgarten
/	/	kIx~	/
Zwergerlgarten	Zwergerlgarten	k2eAgInSc4d1	Zwergerlgarten
zámecký	zámecký	k2eAgInSc4d1	zámecký
park	park	k1gInSc4	park
Hellbrunn	Hellbrunn	k1gInSc1	Hellbrunn
Aigner	Aigner	k1gInSc1	Aigner
Park	park	k1gInSc1	park
Lehener	Lehener	k1gInSc1	Lehener
Park	park	k1gInSc1	park
Volksgarten	Volksgarten	k2eAgInSc1d1	Volksgarten
(	(	kIx(	(
<g/>
Lidová	lidový	k2eAgFnSc1d1	lidová
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
Preuschenpark	Preuschenpark	k1gInSc1	Preuschenpark
Donnenbergpark	Donnenbergpark	k1gInSc1	Donnenbergpark
Salcburský	salcburský	k2eAgInSc1d1	salcburský
komunální	komunální	k2eAgInSc1d1	komunální
hřbitov	hřbitov	k1gInSc1	hřbitov
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
25	[number]	k4	25
ha	ha	kA	ha
největším	veliký	k2eAgInSc7d3	veliký
hřbitovem	hřbitov	k1gInSc7	hřbitov
ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k8xC	i
kraji	kraj	k1gInSc6	kraj
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
městské	městský	k2eAgInPc1d1	městský
hřbitovy	hřbitov	k1gInPc1	hřbitov
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Aigenu	Aigen	k1gInSc6	Aigen
<g/>
,	,	kIx,	,
Gniglu	Gnigl	k1gInSc6	Gnigl
<g/>
,	,	kIx,	,
Maxglanu	Maxglan	k1gInSc6	Maxglan
a	a	k8xC	a
Morzgu	Morzg	k1gInSc6	Morzg
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
historické	historický	k2eAgNnSc4d1	historické
pozoruhodné	pozoruhodný	k2eAgNnSc4d1	pozoruhodné
Petrský	petrský	k2eAgInSc4d1	petrský
hřbitov	hřbitov	k1gInSc4	hřbitov
(	(	kIx(	(
<g/>
Petersfriedhof	Petersfriedhof	k1gInSc4	Petersfriedhof
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šebestiánský	Šebestiánský	k2eAgInSc1d1	Šebestiánský
hřbitov	hřbitov	k1gInSc1	hřbitov
(	(	kIx(	(
<g/>
Sebastiansfriedhof	Sebastiansfriedhof	k1gInSc1	Sebastiansfriedhof
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Salzburger	Salzburger	k1gMnSc1	Salzburger
Landestheater	Landestheater	k1gMnSc1	Landestheater
–	–	k?	–
Zemské	zemský	k2eAgNnSc1d1	zemské
divadlo	divadlo	k1gNnSc1	divadlo
Kleines	Kleines	k1gMnSc1	Kleines
a	a	k8xC	a
Großes	Großes	k1gMnSc1	Großes
Festspielhaus	Festspielhaus	k1gMnSc1	Festspielhaus
Elisabethbühne	Elisabethbühne	k1gMnSc1	Elisabethbühne
/	/	kIx~	/
Schauspielhaus	Schauspielhaus	k1gInSc1	Schauspielhaus
Salzburg	Salzburg	k1gInSc1	Salzburg
Salzburger	Salzburgra	k1gFnPc2	Salzburgra
Straßentheater	Straßentheater	k1gMnSc1	Straßentheater
Toihaus	Toihaus	k1gMnSc1	Toihaus
Salzburger	Salzburger	k1gMnSc1	Salzburger
Marionettentheater	Marionettentheater	k1gMnSc1	Marionettentheater
–	–	k?	–
Loutkové	loutkový	k2eAgNnSc1d1	loutkové
divadlo	divadlo	k1gNnSc1	divadlo
Das	Das	k1gFnSc2	Das
Oval	ovalit	k5eAaPmRp2nS	ovalit
(	(	kIx(	(
<g/>
Bühne	Bühn	k1gInSc5	Bühn
im	im	k?	im
Europark	Europark	k1gInSc1	Europark
<g/>
)	)	kIx)	)
Rupertinum	Rupertinum	k1gNnSc1	Rupertinum
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
přírody	příroda	k1gFnSc2	příroda
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Stille	Stille	k1gInSc4	Stille
Nacht	Nacht	k1gInSc1	Nacht
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Tichá	tichý	k2eAgFnSc1d1	tichá
noc	noc	k1gFnSc1	noc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Salzburger	Salzburger	k1gInSc1	Salzburger
Freilichtmuseum	Freilichtmuseum	k1gInSc1	Freilichtmuseum
Dommuseum	Dommuseum	k1gInSc4	Dommuseum
Salzburg	Salzburg	k1gInSc1	Salzburg
Muzeum	muzeum	k1gNnSc1	muzeum
moderny	moderna	k1gFnSc2	moderna
(	(	kIx(	(
<g/>
na	na	k7c6	na
Mönchsbergu	Mönchsberg	k1gInSc6	Mönchsberg
<g/>
)	)	kIx)	)
Salzburger	Salzburger	k1gMnSc1	Salzburger
Museum	museum	k1gNnSc4	museum
Carolino	Carolin	k2eAgNnSc1d1	Carolino
Augusteum	Augusteum	k1gNnSc1	Augusteum
(	(	kIx(	(
<g/>
SMCA	SMCA	kA	SMCA
<g/>
)	)	kIx)	)
s	s	k7c7	s
příslušnými	příslušný	k2eAgInPc7d1	příslušný
muzey	muzey	k1gInPc7	muzey
<g/>
:	:	kIx,	:
K	k	k7c3	k
Muzeu	muzeum	k1gNnSc3	muzeum
Carolinum	Carolinum	k1gInSc1	Carolinum
Augusteum	Augusteum	k1gInSc4	Augusteum
patří	patřit	k5eAaImIp3nS	patřit
Panorama-Museum	Panorama-Museum	k1gNnSc1	Panorama-Museum
(	(	kIx(	(
<g/>
výstavní	výstavní	k2eAgFnPc1d1	výstavní
prostory	prostora	k1gFnPc1	prostora
známých	známý	k2eAgNnPc2d1	známé
Sattler-Panoramat	Sattler-Panorama	k1gNnPc2	Sattler-Panorama
)	)	kIx)	)
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
lidového	lidový	k2eAgNnSc2d1	lidové
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
pevnosti	pevnost	k1gFnSc2	pevnost
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
Domgrabungsmuseum	Domgrabungsmuseum	k1gInSc1	Domgrabungsmuseum
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
sklepení	sklepení	k1gNnPc2	sklepení
dómu	dóm	k1gInSc2	dóm
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spielzeugmuseum	Spielzeugmuseum	k1gNnSc1	Spielzeugmuseum
(	(	kIx(	(
<g/>
Salcburské	salcburský	k2eAgNnSc1d1	salcburské
muzeum	muzeum	k1gNnSc1	muzeum
hraček	hračka	k1gFnPc2	hračka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každoroční	každoroční	k2eAgFnSc7d1	každoroční
velkou	velký	k2eAgFnSc7d1	velká
kulturní	kulturní	k2eAgFnSc7d1	kulturní
událostí	událost	k1gFnSc7	událost
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
je	být	k5eAaImIp3nS	být
Salcburský	salcburský	k2eAgInSc1d1	salcburský
festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
Salcburské	salcburský	k2eAgFnSc2d1	Salcburská
festivalové	festivalový	k2eAgFnSc2d1	festivalová
hry	hra	k1gFnSc2	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
operní	operní	k2eAgNnPc1d1	operní
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
a	a	k8xC	a
písňové	písňový	k2eAgInPc1d1	písňový
večery	večer	k1gInPc1	večer
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
festival	festival	k1gInSc1	festival
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
přičítán	přičítán	k2eAgInSc4d1	přičítán
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Salcburský	salcburský	k2eAgInSc1d1	salcburský
festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Salzburger	Salzburger	k1gInSc1	Salzburger
Festspiele	Festspiel	k1gInSc2	Festspiel
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
Mozarteum	Mozarteum	k1gNnSc4	Mozarteum
Orchester	orchestra	k1gFnPc2	orchestra
Salzburg	Salzburg	k1gInSc4	Salzburg
Camerata	Camerata	k1gFnSc1	Camerata
Salzburg	Salzburg	k1gInSc1	Salzburg
Salzburger	Salzburger	k1gInSc1	Salzburger
Festungskonzerte	Festungskonzert	k1gInSc5	Festungskonzert
Österreichisches	Österreichisches	k1gInSc1	Österreichisches
Ensemble	ensemble	k1gInSc4	ensemble
für	für	k?	für
neue	neu	k1gInSc2	neu
Musik	musika	k1gFnPc2	musika
Akkorde	Akkord	k1gInSc5	Akkord
–	–	k?	–
On	on	k3xPp3gMnSc1	on
–	–	k?	–
Stage	Stage	k1gInSc1	Stage
Salzburg	Salzburg	k1gInSc1	Salzburg
ARGE-Kultur	ARGE-Kultura	k1gFnPc2	ARGE-Kultura
Salzburg	Salzburg	k1gInSc1	Salzburg
Rockhouse	Rockhouse	k1gFnSc2	Rockhouse
Jazzit	Jazzita	k1gFnPc2	Jazzita
MARK	Mark	k1gMnSc1	Mark
<g/>
.	.	kIx.	.
<g/>
freizeit	freizeit	k1gMnSc1	freizeit
<g/>
.	.	kIx.	.
<g/>
kultur	kultura	k1gFnPc2	kultura
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
Salzburskému	salzburský	k2eAgInSc3d1	salzburský
pivovaru	pivovar	k1gInSc2	pivovar
Stiegl	Stiegla	k1gFnPc2	Stiegla
v	v	k7c6	v
Maxglanu	Maxglan	k1gInSc6	Maxglan
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
budova	budova	k1gFnSc1	budova
právě	právě	k6eAd1	právě
založenému	založený	k2eAgMnSc3d1	založený
"	"	kIx"	"
<g/>
Salzburskému	salzburský	k2eAgInSc3d1	salzburský
uměleckému	umělecký	k2eAgInSc3d1	umělecký
filmu	film	k1gInSc3	film
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
mladý	mladý	k2eAgInSc1d1	mladý
kolektiv	kolektiv	k1gInSc1	kolektiv
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
založil	založit	k5eAaPmAgMnS	založit
laboratoř	laboratoř	k1gFnSc4	laboratoř
a	a	k8xC	a
filmový	filmový	k2eAgInSc4d1	filmový
ateliér	ateliér	k1gInSc4	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
právě	právě	k6eAd1	právě
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Die	Die	k1gFnSc1	Die
Festspiele	Festspiel	k1gInSc2	Festspiel
1921	[number]	k4	1921
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Die	Die	k1gFnSc1	Die
Tragödie	Tragödie	k1gFnSc2	Tragödie
des	des	k1gNnSc2	des
Carlo	Carlo	k1gNnSc1	Carlo
Pinetti	Pinetť	k1gFnSc2	Pinetť
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Tragedie	tragedie	k1gFnSc1	tragedie
Carla	Carl	k1gMnSc2	Carl
Pinettiho	Pinetti	k1gMnSc2	Pinetti
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1924	[number]	k4	1924
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
druhý	druhý	k4xOgInSc1	druhý
film	film	k1gInSc1	film
už	už	k9	už
nikdy	nikdy	k6eAd1	nikdy
nenásledoval	následovat	k5eNaImAgInS	následovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgInS	být
nad	nad	k7c7	nad
podnikem	podnik	k1gInSc7	podnik
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
"	"	kIx"	"
<g/>
Österreichischer	Österreichischra	k1gFnPc2	Österreichischra
Hof	Hof	k1gFnPc2	Hof
<g/>
"	"	kIx"	"
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
–	–	k?	–
uprostřed	uprostřed	k7c2	uprostřed
nejtěžší	těžký	k2eAgFnSc2d3	nejtěžší
krize	krize	k1gFnSc2	krize
rakouského	rakouský	k2eAgInSc2d1	rakouský
filmu	film	k1gInSc2	film
–	–	k?	–
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
konkurs	konkurs	k1gInSc1	konkurs
<g/>
.	.	kIx.	.
</s>
<s>
Mozartkino	Mozartkino	k1gNnSc1	Mozartkino
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Kasererbräu	Kasererbräus	k1gInSc2	Kasererbräus
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
kino	kino	k1gNnSc1	kino
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
rozšířeno	rozšířen	k2eAgNnSc4d1	rozšířeno
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byla	být	k5eAaImAgFnS	být
odkryta	odkryt	k2eAgFnSc1d1	odkryta
studna	studna	k1gFnSc1	studna
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
římské	římský	k2eAgNnSc4d1	římské
impéria	impérium	k1gNnPc4	impérium
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
Salcburském	salcburský	k2eAgNnSc6d1	salcburské
muzeu	muzeum	k1gNnSc6	muzeum
Karla	Karel	k1gMnSc2	Karel
Augusta	August	k1gMnSc2	August
Salzburger	Salzburger	k1gMnSc1	Salzburger
Museum	museum	k1gNnSc4	museum
Carolino	Carolin	k2eAgNnSc1d1	Carolino
Augusteum	Augusteum	k1gNnSc1	Augusteum
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
římského	římský	k2eAgNnSc2d1	římské
opevnění	opevnění	k1gNnSc2	opevnění
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
"	"	kIx"	"
<g/>
Římském	římský	k2eAgInSc6d1	římský
sále	sál	k1gInSc6	sál
(	(	kIx(	(
<g/>
Römersaal	Römersaal	k1gMnSc1	Römersaal
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
kina	kino	k1gNnSc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1	předchůdce
kina	kino	k1gNnSc2	kino
Elmo	Elmo	k6eAd1	Elmo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
ve	v	k7c6	v
cvičebně	cvičebna	k1gFnSc6	cvičebna
lidové	lidový	k2eAgFnSc2d1	lidová
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Plainu	Plain	k1gInSc6	Plain
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pár	pár	k4xCyI	pár
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Lehenského	Lehenský	k2eAgInSc2d1	Lehenský
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
postavili	postavit	k5eAaPmAgMnP	postavit
Alfred	Alfred	k1gMnSc1	Alfred
a	a	k8xC	a
Else	Elsa	k1gFnSc3	Elsa
Morawetzových	Morawetzových	k2eAgFnSc4d1	Morawetzových
novou	nový	k2eAgFnSc4d1	nová
budovu	budova	k1gFnSc4	budova
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
sálem	sál	k1gInSc7	sál
s	s	k7c7	s
1000	[number]	k4	1000
křesly	křeslo	k1gNnPc7	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
přibyl	přibýt	k5eAaPmAgInS	přibýt
druhý	druhý	k4xOgInSc1	druhý
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
sály	sál	k1gInPc1	sál
a	a	k8xC	a
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
největší	veliký	k2eAgFnSc1d3	veliký
má	mít	k5eAaImIp3nS	mít
435	[number]	k4	435
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rodinné	rodinný	k2eAgInPc4d1	rodinný
filmy	film	k1gInPc4	film
a	a	k8xC	a
životopisné	životopisný	k2eAgInPc4d1	životopisný
medailony	medailon	k1gInPc4	medailon
<g/>
.	.	kIx.	.
</s>
<s>
DAS	DAS	kA	DAS
KINO	kino	k1gNnSc1	kino
(	(	kIx(	(
<g/>
salzburské	salzburský	k2eAgNnSc1d1	Salzburské
kulturní	kulturní	k2eAgNnSc1d1	kulturní
filmové	filmový	k2eAgNnSc1d1	filmové
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
na	na	k7c6	na
Äußeren	Äußerna	k1gFnPc2	Äußerna
Steinu	Stein	k1gMnSc3	Stein
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
těžiště	těžiště	k1gNnSc4	těžiště
na	na	k7c6	na
kultovních	kultovní	k2eAgInPc6d1	kultovní
filmech	film	k1gInPc6	film
a	a	k8xC	a
retrospektivách	retrospektiva	k1gFnPc6	retrospektiva
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
podzim	podzim	k1gInSc1	podzim
je	on	k3xPp3gMnPc4	on
pořádá	pořádat	k5eAaImIp3nS	pořádat
Bergfilm-Festival	Bergfilm-Festival	k1gFnSc1	Bergfilm-Festival
<g/>
.	.	kIx.	.
</s>
<s>
Bývalé	bývalý	k2eAgNnSc1d1	bývalé
Centrální	centrální	k2eAgNnSc1d1	centrální
kino	kino	k1gNnSc1	kino
v	v	k7c6	v
Linecké	linecký	k2eAgFnSc6d1	Linecká
uličce	ulička	k1gFnSc6	ulička
Cineplexx	Cineplexx	k1gInSc4	Cineplexx
Airport	Airport	k1gInSc4	Airport
na	na	k7c4	na
Airportcenter	Airportcenter	k1gInSc4	Airportcenter
ve	v	k7c6	v
Wals-Siezenheimu	Wals-Siezenheim	k1gInSc6	Wals-Siezenheim
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
Cineplexx	Cineplexx	k1gInSc1	Cineplexx
City	City	k1gFnSc2	City
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádraží	nádraží	k1gNnSc6	nádraží
bylo	být	k5eAaImAgNnS	být
zprovozněno	zprovoznit	k5eAaPmNgNnS	zprovoznit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Zelený	zelený	k2eAgInSc4d1	zelený
pruh	pruh	k1gInSc4	pruh
Freisaal	Freisaal	k1gMnSc1	Freisaal
Kapuzinerberg	Kapuzinerberg	k1gMnSc1	Kapuzinerberg
(	(	kIx(	(
<g/>
Kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
Mönchsberg	Mönchsberg	k1gInSc1	Mönchsberg
(	(	kIx(	(
<g/>
Mnišská	mnišský	k2eAgFnSc1d1	mnišská
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
Osterfestspiele	Osterfestspiel	k1gInSc2	Osterfestspiel
–	–	k?	–
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
jako	jako	k8xC	jako
doplněk	doplněk	k1gInSc1	doplněk
k	k	k7c3	k
festivalu	festival	k1gInSc3	festival
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
SZENE	SZENE	kA	SZENE
Salzburg	Salzburg	k1gInSc1	Salzburg
Rupertikirtag	Rupertikirtag	k1gInSc1	Rupertikirtag
(	(	kIx(	(
<g/>
Rupertské	Rupertský	k2eAgNnSc1d1	Rupertský
posvícení	posvícení	k1gNnSc1	posvícení
–	–	k?	–
církevní	církevní	k2eAgInSc4d1	církevní
festival	festival	k1gInSc4	festival
salcburského	salcburský	k2eAgInSc2d1	salcburský
Dómu	dóm	k1gInSc2	dóm
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Ruperta	Rupert	k1gMnSc2	Rupert
<g/>
)	)	kIx)	)
Salzburger	Salzburger	k1gMnSc1	Salzburger
Dult	Dult	k1gMnSc1	Dult
(	(	kIx(	(
<g/>
jarmark	jarmark	k?	jarmark
<g/>
)	)	kIx)	)
Salzburger	Salzburger	k1gMnSc1	Salzburger
Jazz-Herbst	Jazz-Herbst	k1gMnSc1	Jazz-Herbst
(	(	kIx(	(
<g/>
Salcburský	salcburský	k2eAgInSc1d1	salcburský
jazzový	jazzový	k2eAgInSc1d1	jazzový
podzim	podzim	k1gInSc1	podzim
<g/>
)	)	kIx)	)
Aspekte	aspekt	k1gInSc5	aspekt
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
soudobé	soudobý	k2eAgFnSc2d1	soudobá
hudby	hudba	k1gFnSc2	hudba
Mozartovy	Mozartův	k2eAgFnSc2d1	Mozartova
koule	koule	k1gFnSc2	koule
Salcburské	salcburský	k2eAgInPc1d1	salcburský
noky	nok	k1gInPc1	nok
Nejstarší	starý	k2eAgInSc1d3	nejstarší
sportovní	sportovní	k2eAgInSc4d1	sportovní
klub	klub	k1gInSc4	klub
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
spolkové	spolkový	k2eAgFnSc3d1	spolková
zemi	zem	k1gFnSc3	zem
Salzbursko	Salzbursko	k1gNnSc1	Salzbursko
je	být	k5eAaImIp3nS	být
Salzburger	Salzburger	k1gInSc4	Salzburger
AK	AK	kA	AK
1914	[number]	k4	1914
z	z	k7c2	z
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Nonntal	Nonntal	k1gMnSc1	Nonntal
<g/>
.	.	kIx.	.
</s>
<s>
Arbeiter-Sportklub	Arbeiter-Sportklub	k1gMnSc1	Arbeiter-Sportklub
(	(	kIx(	(
<g/>
ASK	ASK	kA	ASK
<g/>
)	)	kIx)	)
Salzburg	Salzburg	k1gInSc1	Salzburg
existuje	existovat	k5eAaImIp3nS	existovat
od	od	k7c2	od
r.	r.	kA	r.
1922	[number]	k4	1922
v	v	k7c6	v
části	část	k1gFnSc6	část
Maxglan	Maxglan	k1gMnSc1	Maxglan
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
a	a	k8xC	a
1945	[number]	k4	1945
jako	jako	k8xC	jako
dělnický	dělnický	k2eAgInSc1d1	dělnický
spolek	spolek	k1gInSc1	spolek
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
klubem	klub	k1gInSc7	klub
je	být	k5eAaImIp3nS	být
Red	Red	k1gMnSc1	Red
Bull	bulla	k1gFnPc2	bulla
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
SV	sv	kA	sv
Austria	Austrium	k1gNnSc2	Austrium
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
klubem	klub	k1gInSc7	klub
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
hrál	hrát	k5eAaImAgMnS	hrát
finále	finále	k1gNnSc3	finále
Poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
jako	jako	k8xS	jako
Salzburger	Salzburger	k1gInSc1	Salzburger
EC	EC	kA	EC
založený	založený	k2eAgInSc1d1	založený
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
EC	EC	kA	EC
Red	Red	k1gFnSc1	Red
Bulls	Bulls	k1gInSc1	Bulls
Salzburg	Salzburg	k1gInSc1	Salzburg
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
opět	opět	k6eAd1	opět
úspěšně	úspěšně	k6eAd1	úspěšně
v	v	k7c6	v
první	první	k4xOgFnSc6	první
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
ženské	ženský	k2eAgNnSc4d1	ženské
družstvo	družstvo	k1gNnSc4	družstvo
Ravens	Ravensa	k1gFnPc2	Ravensa
Salzburg	Salzburg	k1gInSc4	Salzburg
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
fotbale	fotbal	k1gInSc6	fotbal
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
Salcburští	salcburský	k2eAgMnPc1d1	salcburský
lvi	lev	k1gMnPc1	lev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
v	v	k7c4	v
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc6	první
mistrovství	mistrovství	k1gNnSc6	mistrovství
Rakouska	Rakousko	k1gNnSc2	Rakousko
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
tento	tento	k3xDgInSc1	tento
tým	tým	k1gInSc1	tým
jako	jako	k8xC	jako
Salzburg	Salzburg	k1gInSc1	Salzburg
Bulls	Bullsa	k1gFnPc2	Bullsa
ve	v	k7c4	v
druholigové	druholigový	k2eAgInPc4d1	druholigový
Division	Division	k1gInSc4	Division
I.	I.	kA	I.
Sportovních	sportovní	k2eAgFnPc2d1	sportovní
akcí	akce	k1gFnPc2	akce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
festivalů	festival	k1gInPc2	festival
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
Salcburský	salcburský	k2eAgInSc1d1	salcburský
maraton	maraton	k1gInSc1	maraton
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
je	on	k3xPp3gFnPc4	on
vede	vést	k5eAaImIp3nS	vést
i	i	k9	i
historickou	historický	k2eAgFnSc4d1	historická
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
konalo	konat	k5eAaImAgNnS	konat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2008	[number]	k4	2008
se	se	k3xPyFc4	se
na	na	k7c4	na
Wals	wals	k1gInSc4	wals
Siezenheim	Siezenheim	k1gInSc1	Siezenheim
Stadium	stadium	k1gNnSc1	stadium
konaly	konat	k5eAaImAgInP	konat
tři	tři	k4xCgInPc1	tři
zápasy	zápas	k1gInPc1	zápas
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
D.	D.	kA	D.
Salcburk	Salcburk	k1gInSc4	Salcburk
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
pořádání	pořádání	k1gNnSc4	pořádání
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2010	[number]	k4	2010
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgInSc1d1	noční
život	život	k1gInSc1	život
města	město	k1gNnSc2	město
Salzburg	Salzburg	k1gInSc1	Salzburg
se	se	k3xPyFc4	se
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
okolo	okolo	k7c2	okolo
náměstí	náměstí	k1gNnSc2	náměstí
Antona	Anton	k1gMnSc2	Anton
Neumayra	Neumayr	k1gMnSc2	Neumayr
a	a	k8xC	a
na	na	k7c6	na
Rudolfskai	Rudolfska	k1gFnSc6	Rudolfska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
návštěvníci	návštěvník	k1gMnPc1	návštěvník
pobavit	pobavit	k5eAaPmF	pobavit
např.	např.	kA	např.
v	v	k7c6	v
pivnicích	pivnice	k1gFnPc6	pivnice
či	či	k8xC	či
v	v	k7c6	v
irských	irský	k2eAgFnPc6d1	irská
pubech	pub	k1gFnPc6	pub
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
zařízení	zařízení	k1gNnSc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádraží	nádraží	k1gNnSc6	nádraží
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
Urban	Urban	k1gMnSc1	Urban
Entertainment	Entertainment	k1gMnSc1	Entertainment
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
City	city	k1gNnSc1	city
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
tak	tak	k9	tak
v	v	k7c6	v
letištním	letištní	k2eAgNnSc6d1	letištní
centru	centrum	k1gNnSc6	centrum
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
právě	právě	k9	právě
teď	teď	k6eAd1	teď
nachází	nacházet	k5eAaImIp3nS	nacházet
diskotéky	diskotéka	k1gFnPc4	diskotéka
<g/>
,	,	kIx,	,
kina	kino	k1gNnPc1	kino
a	a	k8xC	a
bary	bar	k1gInPc1	bar
<g/>
.	.	kIx.	.
</s>
<s>
Salzburg	Salzburg	k1gInSc1	Salzburg
disponuje	disponovat	k5eAaBmIp3nS	disponovat
letištěm	letiště	k1gNnSc7	letiště
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
Airport	Airport	k1gInSc1	Airport
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgNnSc7	druhý
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
letištěm	letiště	k1gNnSc7	letiště
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
odbaví	odbavit	k5eAaPmIp3nP	odbavit
toto	tento	k3xDgNnSc4	tento
letiště	letiště	k1gNnSc4	letiště
ročně	ročně	k6eAd1	ročně
1,42	[number]	k4	1,42
milionů	milion	k4xCgInPc2	milion
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tendence	tendence	k1gFnSc1	tendence
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
stoupající	stoupající	k2eAgInSc1d1	stoupající
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
silně	silně	k6eAd1	silně
rostoucímu	rostoucí	k2eAgNnSc3d1	rostoucí
zastoupení	zastoupení	k1gNnSc3	zastoupení
zimní	zimní	k2eAgFnSc2d1	zimní
turistiky	turistika	k1gFnSc2	turistika
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Skrze	Skrze	k?	Skrze
Salzburg	Salzburg	k1gInSc1	Salzburg
vede	vést	k5eAaImIp3nS	vést
Západní	západní	k2eAgInSc1d1	západní
koridor	koridor	k1gInSc1	koridor
Rakouských	rakouský	k2eAgFnPc2d1	rakouská
železnic	železnice	k1gFnPc2	železnice
(	(	kIx(	(
<g/>
Westbahn	Westbahn	k1gInSc1	Westbahn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
města	město	k1gNnSc2	město
činí	činit	k5eAaImIp3nS	činit
důležitý	důležitý	k2eAgInSc4d1	důležitý
národní	národní	k2eAgInSc4d1	národní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
železniční	železniční	k2eAgInSc4d1	železniční
uzel	uzel	k1gInSc4	uzel
<g/>
,	,	kIx,	,
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
–	–	k?	–
Hauptbahnhof	Hauptbahnhof	k1gInSc1	Hauptbahnhof
Salzburg	Salzburg	k1gInSc1	Salzburg
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
frekventovaných	frekventovaný	k2eAgNnPc2d1	frekventované
nádraží	nádraží	k1gNnPc2	nádraží
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
se	se	k3xPyFc4	se
cestující	cestující	k1gMnPc1	cestující
dostanou	dostat	k5eAaPmIp3nP	dostat
na	na	k7c4	na
hlavní	hlavní	k2eAgInPc4d1	hlavní
tahy	tah	k1gInPc4	tah
<g/>
,	,	kIx,	,
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
do	do	k7c2	do
spolkového	spolkový	k2eAgNnSc2d1	Spolkové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
po	po	k7c6	po
Tauernské	Tauernský	k2eAgFnSc6d1	Tauernská
dráze	dráha	k1gFnSc6	dráha
(	(	kIx(	(
<g/>
Tauernbahn	Tauernbahn	k1gNnSc1	Tauernbahn
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
(	(	kIx(	(
<g/>
do	do	k7c2	do
Villachu	Villach	k1gInSc2	Villach
<g/>
,	,	kIx,	,
do	do	k7c2	do
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
do	do	k7c2	do
Štýrského	štýrský	k2eAgInSc2d1	štýrský
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
Innsbruku	Innsbruk	k1gInSc2	Innsbruk
či	či	k8xC	či
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Dálniční	dálniční	k2eAgInSc1d1	dálniční
uzel	uzel	k1gInSc1	uzel
Salzburg	Salzburg	k1gInSc1	Salzburg
Západní	západní	k2eAgFnSc2d1	západní
dálnice	dálnice	k1gFnSc2	dálnice
A1	A1	k1gFnSc1	A1
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
Spolkové	spolkový	k2eAgFnSc2d1	spolková
dálnice	dálnice	k1gFnSc2	dálnice
A8	A8	k1gFnSc2	A8
Mnichov	Mnichov	k1gInSc1	Mnichov
–	–	k?	–
Salzburg	Salzburg	k1gInSc1	Salzburg
a	a	k8xC	a
Tauernské	Tauernské	k2eAgFnSc1d1	Tauernské
dálnice	dálnice	k1gFnSc1	dálnice
A10	A10	k1gFnSc2	A10
Salzburg	Salzburg	k1gInSc1	Salzburg
–	–	k?	–
Villach	Villacha	k1gFnPc2	Villacha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
společně	společně	k6eAd1	společně
tvoří	tvořit	k5eAaImIp3nP	tvořit
půlkruh	půlkruh	k1gInSc4	půlkruh
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dálniční	dálniční	k2eAgInPc1d1	dálniční
sjezdy	sjezd	k1gInPc1	sjezd
k	k	k7c3	k
městskému	městský	k2eAgNnSc3d1	Městské
území	území	k1gNnSc3	území
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Salzburg	Salzburg	k1gInSc1	Salzburg
Nord	Nord	k1gInSc1	Nord
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
Messegelände	Messegeländ	k1gInSc5	Messegeländ
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
Mitte	Mitt	k1gInSc5	Mitt
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
Klessheim	Klessheim	k1gInSc1	Klessheim
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
Siezenheim	Siezenheim	k1gInSc1	Siezenheim
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
Flughafen	Flughafen	k1gInSc1	Flughafen
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
West	West	k1gInSc1	West
a	a	k8xC	a
Salzburg	Salzburg	k1gInSc1	Salzburg
Süd	Süd	k1gFnSc2	Süd
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
část	část	k1gFnSc4	část
dálnice	dálnice	k1gFnSc2	dálnice
A1	A1	k1gFnSc2	A1
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
městskou	městský	k2eAgFnSc4d1	městská
<g/>
,	,	kIx,	,
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
vedlejšími	vedlejší	k2eAgInPc7d1	vedlejší
účinky	účinek	k1gInPc7	účinek
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc4d1	častá
dopravní	dopravní	k2eAgFnPc4d1	dopravní
zácpy	zácpa	k1gFnPc4	zácpa
<g/>
,	,	kIx,	,
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
měnících	měnící	k2eAgFnPc2d1	měnící
nových	nový	k2eAgFnPc2d1	nová
objížděk	objížďka	k1gFnPc2	objížďka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
S-Bahn	S-Bahna	k1gFnPc2	S-Bahna
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
,	,	kIx,	,
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
a	a	k8xC	a
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obsluhovány	obsluhován	k2eAgFnPc1d1	obsluhována
také	také	k9	také
regionální	regionální	k2eAgFnSc7d1	regionální
dopravou	doprava	k1gFnSc7	doprava
a	a	k8xC	a
nabízejí	nabízet	k5eAaImIp3nP	nabízet
možnost	možnost	k1gFnSc4	možnost
cestujícím	cestující	k1gMnPc3	cestující
dojet	dojet	k5eAaPmF	dojet
až	až	k9	až
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
regionální	regionální	k2eAgFnSc1d1	regionální
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
příměstské	příměstský	k2eAgFnSc2d1	příměstská
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
úsek	úsek	k1gInSc1	úsek
Freilassing	Freilassing	k1gInSc1	Freilassing
–	–	k?	–
Berchtesgaden	Berchtesgaden	k2eAgInSc1d1	Berchtesgaden
<g/>
,	,	kIx,	,
Freilassing	Freilassing	k1gInSc1	Freilassing
–	–	k?	–
Mühldorf	Mühldorf	k1gInSc1	Mühldorf
a	a	k8xC	a
Steindorf	Steindorf	k1gInSc1	Steindorf
–	–	k?	–
Braunau	Braunaus	k1gInSc2	Braunaus
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
druhem	druh	k1gInSc7	druh
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
rychlodráha	rychlodráha	k1gFnSc1	rychlodráha
S-Bahn	S-Bahno	k1gNnPc2	S-Bahno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
i	i	k9	i
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
města	město	k1gNnSc2	město
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Freilassing	Freilassing	k1gInSc4	Freilassing
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Salzburgu	Salzburg	k1gInSc2	Salzburg
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
hromadné	hromadný	k2eAgFnSc6d1	hromadná
dopravě	doprava	k1gFnSc6	doprava
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejrozsáhlejší	rozsáhlý	k2eAgFnSc2d3	nejrozsáhlejší
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
která	který	k3yQgFnSc1	který
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nahradila	nahradit	k5eAaPmAgFnS	nahradit
tramvaje	tramvaj	k1gFnPc4	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
některá	některý	k3yIgNnPc4	některý
místa	místo	k1gNnPc4	místo
(	(	kIx(	(
<g/>
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
touto	tento	k3xDgFnSc7	tento
trakcí	trakce	k1gFnSc7	trakce
obsloužena	obsloužit	k5eAaPmNgFnS	obsloužit
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzdálenější	vzdálený	k2eAgFnPc4d2	vzdálenější
trasy	trasa	k1gFnPc4	trasa
jako	jako	k8xC	jako
např.	např.	kA	např.
do	do	k7c2	do
Freilassingu	Freilassing	k1gInSc2	Freilassing
jezdí	jezdit	k5eAaImIp3nS	jezdit
autobusy	autobus	k1gInPc4	autobus
dopravce	dopravce	k1gMnSc1	dopravce
Albus	Albus	k1gMnSc1	Albus
Salzburg	Salzburg	k1gInSc4	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
převážně	převážně	k6eAd1	převážně
hvězdicovitě	hvězdicovitě	k6eAd1	hvězdicovitě
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
chybějí	chybět	k5eAaImIp3nP	chybět
některá	některý	k3yIgNnPc4	některý
propojení	propojení	k1gNnPc4	propojení
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jmenované	jmenovaný	k2eAgInPc1d1	jmenovaný
způsoby	způsob	k1gInPc1	způsob
dopravy	doprava	k1gFnSc2	doprava
jsou	být	k5eAaImIp3nP	být
sjednocené	sjednocený	k2eAgInPc1d1	sjednocený
do	do	k7c2	do
jednotného	jednotný	k2eAgNnSc2d1	jednotné
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
doplňujícího	doplňující	k2eAgInSc2d1	doplňující
tarifního	tarifní	k2eAgInSc2d1	tarifní
systému	systém	k1gInSc2	systém
Salzburger	Salzburger	k1gMnSc1	Salzburger
Verkehrsverbund	Verkehrsverbund	k1gMnSc1	Verkehrsverbund
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
města	město	k1gNnSc2	město
Salzburgu	Salzburg	k1gInSc2	Salzburg
s	s	k7c7	s
osobními	osobní	k2eAgInPc7d1	osobní
auty	aut	k1gInPc7	aut
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zařízení	zařízení	k1gNnSc2	zařízení
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
na	na	k7c6	na
sjezdech	sjezd	k1gInPc6	sjezd
"	"	kIx"	"
<g/>
Salzburg	Salzburg	k1gInSc1	Salzburg
Süd	Süd	k1gFnSc2	Süd
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Messegelände	Messegeländ	k1gInSc5	Messegeländ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
dojet	dojet	k5eAaPmF	dojet
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
prostředky	prostředek	k1gInPc4	prostředek
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
při	při	k7c6	při
vjezdu	vjezd	k1gInSc6	vjezd
automobilem	automobil	k1gInSc7	automobil
do	do	k7c2	do
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
dlouho	dlouho	k6eAd1	dlouho
trvajícími	trvající	k2eAgFnPc7d1	trvající
dopravními	dopravní	k2eAgFnPc7d1	dopravní
zácpami	zácpa	k1gFnPc7	zácpa
a	a	k8xC	a
problémy	problém	k1gInPc7	problém
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
volného	volný	k2eAgNnSc2d1	volné
parkovacího	parkovací	k2eAgNnSc2d1	parkovací
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
a	a	k8xC	a
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
roli	role	k1gFnSc4	role
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
Salzburku	Salzburk	k1gInSc2	Salzburk
hraje	hrát	k5eAaImIp3nS	hrát
jízdní	jízdní	k2eAgNnSc1d1	jízdní
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podíl	podíl	k1gInSc1	podíl
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
objemu	objem	k1gInSc6	objem
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
20	[number]	k4	20
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
má	mít	k5eAaImIp3nS	mít
magistrát	magistrát	k1gInSc1	magistrát
města	město	k1gNnSc2	město
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
koordinaci	koordinace	k1gFnSc4	koordinace
cyklistického	cyklistický	k2eAgInSc2d1	cyklistický
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
důležité	důležitý	k2eAgNnSc1d1	důležité
bylo	být	k5eAaImAgNnS	být
vybudování	vybudování	k1gNnSc1	vybudování
podjezdů	podjezd	k1gInPc2	podjezd
u	u	k7c2	u
automobilových	automobilový	k2eAgInPc2d1	automobilový
mostů	most	k1gInPc2	most
a	a	k8xC	a
u	u	k7c2	u
Makartovy	Makartovy	k?	Makartovy
lávky	lávka	k1gFnSc2	lávka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
též	též	k9	též
hojně	hojně	k6eAd1	hojně
využívají	využívat	k5eAaImIp3nP	využívat
pěší	pěší	k2eAgMnPc1d1	pěší
chodci	chodec	k1gMnPc1	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Lávky	lávka	k1gFnPc1	lávka
také	také	k9	také
byly	být	k5eAaImAgFnP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
pro	pro	k7c4	pro
bicykly	bicykl	k1gInPc4	bicykl
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přejít	přejít	k5eAaPmF	přejít
řeku	řeka	k1gFnSc4	řeka
Salzach	Salzacha	k1gFnPc2	Salzacha
po	po	k7c6	po
lávce	lávka	k1gFnSc6	lávka
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Mozartovy	Mozartův	k2eAgFnSc2d1	Mozartova
lávky	lávka	k1gFnSc2	lávka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
břehu	břeh	k1gInSc2	břeh
Salzachu	Salzach	k1gInSc2	Salzach
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
cestami	cesta	k1gFnPc7	cesta
pro	pro	k7c4	pro
pěší	pěší	k1gMnPc4	pěší
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
vlastní	vlastní	k2eAgFnPc4d1	vlastní
cesty	cesta	k1gFnPc4	cesta
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
hojně	hojně	k6eAd1	hojně
využívány	využívat	k5eAaPmNgInP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Výhodnou	výhodný	k2eAgFnSc7d1	výhodná
možností	možnost	k1gFnSc7	možnost
přemísťování	přemísťování	k1gNnSc2	přemísťování
na	na	k7c4	na
dobře	dobře	k6eAd1	dobře
vybudované	vybudovaný	k2eAgFnPc4d1	vybudovaná
sítě	síť	k1gFnPc4	síť
cyklotras	cyklotrasa	k1gFnPc2	cyklotrasa
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc4	využití
možnosti	možnost	k1gFnSc2	možnost
zapůjčení	zapůjčení	k1gNnSc2	zapůjčení
kola	kolo	k1gNnSc2	kolo
Citybike	Citybike	k1gNnSc2	Citybike
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
registraci	registrace	k1gFnSc6	registrace
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
terminálu	terminála	k1gFnSc4	terminála
při	při	k7c6	při
některém	některý	k3yIgInSc6	některý
ze	z	k7c2	z
stanovišť	stanoviště	k1gNnPc2	stanoviště
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zapůjčit	zapůjčit	k5eAaPmF	zapůjčit
si	se	k3xPyFc3	se
kolo	kolo	k1gNnSc4	kolo
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
platebních	platební	k2eAgFnPc2d1	platební
karet	kareta	k1gFnPc2	kareta
Maestro-Karte	Maestro-Kart	k1gInSc5	Maestro-Kart
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
co	co	k9	co
nejlepšímu	dobrý	k2eAgNnSc3d3	nejlepší
skloubení	skloubení	k1gNnSc3	skloubení
MHD	MHD	kA	MHD
a	a	k8xC	a
cyklodopravy	cyklodoprava	k1gFnPc1	cyklodoprava
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádraží	nádraží	k1gNnSc6	nádraží
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
Itzling	Itzling	k1gInSc1	Itzling
zřízeny	zřídit	k5eAaPmNgFnP	zřídit
první	první	k4xOgFnPc1	první
zabezpečené	zabezpečený	k2eAgFnPc1d1	zabezpečená
cyklogaráže	cyklogaráž	k1gFnPc1	cyklogaráž
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
na	na	k7c6	na
Hanuschově	Hanuschův	k2eAgNnSc6d1	Hanuschův
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
chodec	chodec	k1gMnSc1	chodec
v	v	k7c6	v
Salzburku	Salzburk	k1gInSc6	Salzburk
dostane	dostat	k5eAaPmIp3nS	dostat
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
pohodlně	pohodlně	k6eAd1	pohodlně
i	i	k9	i
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
pěší	pěší	k2eAgFnSc7d1	pěší
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
prostý	prostý	k2eAgInSc1d1	prostý
most	most	k1gInSc1	most
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Staatsbrücke	Staatsbrücke	k1gFnSc4	Staatsbrücke
jediným	jediný	k2eAgNnSc7d1	jediné
spojením	spojení	k1gNnSc7	spojení
mezi	mezi	k7c7	mezi
Starým	Starý	k1gMnSc7	Starý
a	a	k8xC	a
Novým	nový	k2eAgNnSc7d1	nové
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mostů	most	k1gInPc2	most
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
jmenováno	jmenovat	k5eAaImNgNnS	jmenovat
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Hellbrunner	Hellbrunner	k1gInSc1	Hellbrunner
Brücke	Brücke	k1gInSc1	Brücke
<g/>
,	,	kIx,	,
Überführsteg	Überführsteg	k1gInSc1	Überführsteg
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
propojuje	propojovat	k5eAaImIp3nS	propojovat
čtvrti	čtvrt	k1gFnSc3	čtvrt
Josefiau	Josefiaa	k1gFnSc4	Josefiaa
a	a	k8xC	a
Aigen	Aigen	k1gInSc4	Aigen
<g/>
,	,	kIx,	,
Nonntaler	Nonntaler	k1gInSc4	Nonntaler
Brücke	Brück	k1gFnSc2	Brück
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Karolinenbrücke	Karolinenbrück	k1gMnSc2	Karolinenbrück
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Staatsbrücke	Staatsbrücke	k1gNnSc7	Staatsbrücke
a	a	k8xC	a
Lehener	Lehener	k1gInSc1	Lehener
Brücke	Brück	k1gInSc2	Brück
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvytíženějším	vytížený	k2eAgMnPc3d3	nejvytíženější
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
Mozartsteg	Mozartsteg	k1gInSc1	Mozartsteg
s	s	k7c7	s
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
pokrytím	pokrytí	k1gNnSc7	pokrytí
<g/>
.	.	kIx.	.
</s>
<s>
Staatsbrücke	Staatsbrücke	k1gInSc1	Staatsbrücke
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
mostem	most	k1gInSc7	most
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
nesl	nést	k5eAaImAgMnS	nést
jméno	jméno	k1gNnSc4	jméno
Hauptbrücke	Hauptbrück	k1gFnSc2	Hauptbrück
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
byl	být	k5eAaImAgInS	být
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
železná	železný	k2eAgFnSc1d1	železná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
své	svůj	k3xOyFgNnSc4	svůj
dnešní	dnešní	k2eAgNnSc4d1	dnešní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
podobou	podoba	k1gFnSc7	podoba
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
po	po	k7c6	po
devítiletých	devítiletý	k2eAgFnPc6d1	devítiletá
pracích	práce	k1gFnPc6	práce
znovuotevřen	znovuotevřen	k2eAgInSc1d1	znovuotevřen
až	až	k9	až
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
spojuje	spojovat	k5eAaImIp3nS	spojovat
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Linzergasse	Linzergasse	k1gFnSc2	Linzergasse
a	a	k8xC	a
čtvrtí	čtvrt	k1gFnPc2	čtvrt
Platzl	Platzl	k1gFnSc2	Platzl
<g/>
.	.	kIx.	.
</s>
<s>
Architektonicky	architektonicky	k6eAd1	architektonicky
pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
Makartsteg	Makartsteg	k1gInSc1	Makartsteg
(	(	kIx(	(
<g/>
pojmenování	pojmenování	k1gNnSc1	pojmenování
nese	nést	k5eAaImIp3nS	nést
po	po	k7c6	po
Hansi	Hans	k1gMnSc6	Hans
Makartovi	Makart	k1gMnSc6	Makart
<g/>
)	)	kIx)	)
dovede	dovést	k5eAaPmIp3nS	dovést
pěší	pěší	k1gMnSc1	pěší
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
Hanuschplatz	Hanuschplatz	k1gInSc4	Hanuschplatz
na	na	k7c4	na
Makartplatz	Makartplatz	k1gInSc4	Makartplatz
k	k	k7c3	k
Mozartovu	Mozartův	k2eAgNnSc3d1	Mozartovo
bydlišti	bydliště	k1gNnSc3	bydliště
<g/>
.	.	kIx.	.
</s>
<s>
Makartsteg	Makartsteg	k1gInSc1	Makartsteg
byl	být	k5eAaImAgInS	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
však	však	k9	však
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Museumssteg	Museumssteg	k1gMnSc1	Museumssteg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
zrekonstruován	zrekonstruovat	k5eAaPmNgInS	zrekonstruovat
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
svůj	svůj	k3xOyFgInSc4	svůj
současný	současný	k2eAgInSc4d1	současný
pokřivený	pokřivený	k2eAgInSc4d1	pokřivený
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
doplněný	doplněný	k2eAgInSc4d1	doplněný
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Mülln	Müllno	k1gNnPc2	Müllno
a	a	k8xC	a
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
<g/>
,	,	kIx,	,
na	na	k7c6	na
linii	linie	k1gFnSc6	linie
k	k	k7c3	k
Mirabellplatz	Mirabellplatz	k1gInSc1	Mirabellplatz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Müllnersteg	Müllnersteg	k1gInSc1	Müllnersteg
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pěší	pěší	k2eAgInSc1d1	pěší
most	most	k1gInSc1	most
Františka	František	k1gMnSc2	František
Karla	Karel	k1gMnSc2	Karel
(	(	kIx(	(
<g/>
Franz-Carl-Gehbrücke	Franz-Carl-Gehbrücke	k1gInSc1	Franz-Carl-Gehbrücke
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nacházel	nacházet	k5eAaImAgMnS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
půl	půl	k1xP	půl
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
železničnímu	železniční	k2eAgInSc3d1	železniční
mostu	most	k1gInSc3	most
(	(	kIx(	(
<g/>
Eisenbahnbrücke	Eisenbahnbrücke	k1gFnPc3	Eisenbahnbrücke
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
rozšířeného	rozšířený	k2eAgNnSc2d1	rozšířené
o	o	k7c4	o
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
kolej	kolej	k1gFnSc4	kolej
Salzburger	Salzburger	k1gMnSc1	Salzburger
S-Bahn	S-Bahn	k1gMnSc1	S-Bahn
celkem	celkem	k6eAd1	celkem
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
dráhy	dráha	k1gFnPc4	dráha
kvůli	kvůli	k7c3	kvůli
zvýšení	zvýšení	k1gNnSc3	zvýšení
kapacity	kapacita	k1gFnSc2	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
Lehener	Lehener	k1gInSc1	Lehener
Brücke	Brück	k1gMnSc2	Brück
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
otevření	otevření	k1gNnSc2	otevření
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
do	do	k7c2	do
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
/	/	kIx~	/
<g/>
67	[number]	k4	67
nesl	nést	k5eAaImAgMnS	nést
jméno	jméno	k1gNnSc4	jméno
most	most	k1gInSc1	most
Arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Viktora	Viktor	k1gMnSc2	Viktor
(	(	kIx(	(
<g/>
Erzherzog-Ludwig-Viktor-Brücke	Erzherzog-Ludwig-Viktor-Brücke	k1gInSc1	Erzherzog-Ludwig-Viktor-Brücke
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
mostem	most	k1gInSc7	most
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
dnešní	dnešní	k2eAgFnPc4d1	dnešní
čtyři	čtyři	k4xCgFnPc4	čtyři
jízdní	jízdní	k2eAgFnPc4d1	jízdní
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
otevřený	otevřený	k2eAgInSc1d1	otevřený
most	most	k1gInSc1	most
lávka	lávka	k1gFnSc1	lávka
Pionýrů	pionýr	k1gMnPc2	pionýr
(	(	kIx(	(
<g/>
Pioniersteg	Pioniersteg	k1gMnSc1	Pioniersteg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
chodci	chodec	k1gMnPc1	chodec
dostanou	dostat	k5eAaPmIp3nP	dostat
z	z	k7c2	z
Lehenského	Lehenský	k2eAgInSc2d1	Lehenský
parku	park	k1gInSc2	park
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Jahnstraße	Jahnstraß	k1gInSc2	Jahnstraß
a	a	k8xC	a
podél	podél	k7c2	podél
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
Traklova	Traklův	k2eAgFnSc1d1	Traklův
lávka	lávka	k1gFnSc1	lávka
(	(	kIx(	(
<g/>
Traklsteg	Traklsteg	k1gInSc1	Traklsteg
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
Georga	Georg	k1gMnSc2	Georg
Trakla	Trakla	k1gMnSc2	Trakla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yRgInSc4	který
se	se	k3xPyFc4	se
pěší	pěší	k1gMnSc1	pěší
dostane	dostat	k5eAaPmIp3nS	dostat
z	z	k7c2	z
Glan-Spitz	Glan-Spitza	k1gFnPc2	Glan-Spitza
na	na	k7c4	na
Itzling-Západ	Itzling-Západ	k1gInSc4	Itzling-Západ
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
daleko	daleko	k6eAd1	daleko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k6eAd1	ještě
Dálniční	dálniční	k2eAgInSc1d1	dálniční
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Autobahnbrücke	Autobahnbrücke	k1gFnSc1	Autobahnbrücke
<g/>
)	)	kIx)	)
Západní	západní	k2eAgFnSc1d1	západní
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
přivádí	přivádět	k5eAaImIp3nS	přivádět
též	též	k9	též
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
vnitroměstské	vnitroměstský	k2eAgFnSc2d1	vnitroměstská
dopravy	doprava	k1gFnSc2	doprava
jako	jako	k8xS	jako
městský	městský	k2eAgInSc4d1	městský
okruh	okruh	k1gInSc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
dálniční	dálniční	k2eAgInSc1d1	dálniční
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
stržen	strhnout	k5eAaPmNgInS	strhnout
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
povodni	povodeň	k1gFnSc6	povodeň
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
Dálniční	dálniční	k2eAgInSc1d1	dálniční
most	most	k1gInSc1	most
též	též	k9	též
pěší	pěší	k2eAgInSc4d1	pěší
přechod	přechod	k1gInSc4	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
pěší	pěší	k2eAgInSc1d1	pěší
most	most	k1gInSc1	most
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
na	na	k7c6	na
Saalachspitzu	Saalachspitz	k1gInSc6	Saalachspitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plánován	plánovat	k5eAaImNgInS	plánovat
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
přes	přes	k7c4	přes
Saalach	Saalach	k1gInSc4	Saalach
do	do	k7c2	do
sousedního	sousední	k2eAgMnSc2d1	sousední
Freilassing	Freilassing	k1gInSc4	Freilassing
a	a	k8xC	a
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
přes	přes	k7c4	přes
Salzach	Salzach	k1gInSc4	Salzach
v	v	k7c6	v
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
Bergheim	Bergheim	k1gInSc1	Bergheim
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
ještě	ještě	k6eAd1	ještě
nedospěl	dochvít	k5eNaPmAgMnS	dochvít
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
konečné	konečný	k2eAgFnSc2d1	konečná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dodávku	dodávka	k1gFnSc4	dodávka
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
městu	město	k1gNnSc3	město
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
Salzburg	Salzburg	k1gInSc4	Salzburg
AG	AG	kA	AG
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc1d1	regionální
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
díky	díky	k7c3	díky
fúzi	fúze	k1gFnSc3	fúze
společností	společnost	k1gFnPc2	společnost
SAFE	safe	k1gInSc4	safe
a	a	k8xC	a
Salzburger	Salzburger	k1gInSc4	Salzburger
Stadtwerke	Stadtwerk	k1gFnSc2	Stadtwerk
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Salzburg	Salzburg	k1gInSc1	Salzburg
získává	získávat	k5eAaImIp3nS	získávat
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společnosti	společnost	k1gFnSc2	společnost
Salzburg	Salzburg	k1gInSc1	Salzburg
AG	AG	kA	AG
především	především	k9	především
z	z	k7c2	z
pozemních	pozemní	k2eAgFnPc2d1	pozemní
studní	studna	k1gFnPc2	studna
a	a	k8xC	a
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
pramenité	pramenitý	k2eAgFnSc2d1	pramenitá
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
poblíž	poblíž	k7c2	poblíž
Untersbergu	Untersberg	k1gInSc2	Untersberg
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
města	město	k1gNnSc2	město
přivedeno	přiveden	k2eAgNnSc4d1	přivedeno
cca	cca	kA	cca
12	[number]	k4	12
miliard	miliarda	k4xCgFnPc2	miliarda
litrů	litr	k1gInPc2	litr
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Mnišské	mnišský	k2eAgFnSc6d1	mnišská
hoře	hora	k1gFnSc6	hora
(	(	kIx(	(
<g/>
Mönchsberg	Mönchsberg	k1gInSc1	Mönchsberg
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kapucínské	kapucínský	k2eAgNnSc1d1	Kapucínské
hoře	hoře	k1gNnSc1	hoře
(	(	kIx(	(
<g/>
Kapuzinerberg	Kapuzinerberg	k1gInSc1	Kapuzinerberg
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vodojemy	vodojem	k1gInPc1	vodojem
<g/>
.	.	kIx.	.
</s>
<s>
Likvidaci	likvidace	k1gFnSc4	likvidace
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
velkokapacitní	velkokapacitní	k2eAgFnSc1d1	velkokapacitní
čistička	čistička	k1gFnSc1	čistička
Siggerwiesen	Siggerwiesen	k2eAgInSc1d1	Siggerwiesen
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
600	[number]	k4	600
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
vedle	vedle	k7c2	vedle
aglomerace	aglomerace	k1gFnSc2	aglomerace
Salzburg	Salzburg	k1gInSc1	Salzburg
také	také	k6eAd1	také
obci	obec	k1gFnSc3	obec
Ainring	Ainring	k1gInSc4	Ainring
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Salzburku	Salzburk	k1gInSc6	Salzburk
sídlí	sídlet	k5eAaImIp3nS	sídlet
pět	pět	k4xCc1	pět
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
vysokých	vysoký	k2eAgFnPc2d1	vysoká
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc2d2	vyšší
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejstarší	starý	k2eAgFnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Universität	Universität	k2eAgInSc1d1	Universität
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
historickým	historický	k2eAgInSc7d1	historický
názvem	název	k1gInSc7	název
Paris-Lodron-Universität	Paris-Lodron-Universität	k2eAgInSc1d1	Paris-Lodron-Universität
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1810	[number]	k4	1810
až	až	k9	až
1962	[number]	k4	1962
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgFnPc4	čtyři
fakulty	fakulta	k1gFnPc4	fakulta
<g/>
:	:	kIx,	:
katolické	katolický	k2eAgFnSc2d1	katolická
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
právních	právní	k2eAgFnPc2d1	právní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
fakulta	fakulta	k1gFnSc1	fakulta
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Mozarteum	Mozarteum	k1gNnSc1	Mozarteum
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
jako	jako	k8xC	jako
hudební	hudební	k2eAgFnSc1d1	hudební
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
od	od	k7c2	od
r.	r.	kA	r.
1970	[number]	k4	1970
je	být	k5eAaImIp3nS	být
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
nabízeno	nabízet	k5eAaImNgNnS	nabízet
především	především	k9	především
studium	studium	k1gNnSc1	studium
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
hudebních	hudební	k2eAgFnPc2d1	hudební
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pedagogicky	pedagogicky	k6eAd1	pedagogicky
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
studium	studium	k1gNnSc4	studium
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
možnosti	možnost	k1gFnPc4	možnost
výuky	výuka	k1gFnSc2	výuka
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
herectví	herectví	k1gNnSc1	herectví
<g/>
:	:	kIx,	:
Divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
jevištní	jevištní	k2eAgFnSc1d1	jevištní
technika	technika	k1gFnSc1	technika
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
<g/>
.	.	kIx.	.
</s>
<s>
Fachhochschule	Fachhochschule	k1gFnSc1	Fachhochschule
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
existuje	existovat	k5eAaImIp3nS	existovat
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
městského	městský	k2eAgNnSc2d1	Městské
území	území	k1gNnSc2	území
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ležící	ležící	k2eAgFnSc2d1	ležící
obce	obec	k1gFnSc2	obec
Puch	puch	k1gInSc1	puch
bei	bei	k?	bei
Hallein	Hallein	k2eAgInSc1d1	Hallein
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
technické	technický	k2eAgNnSc4d1	technické
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
<g/>
,	,	kIx,	,
výtvarně-umělecké	výtvarněmělecký	k2eAgNnSc4d1	výtvarně-umělecký
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
studium	studium	k1gNnSc4	studium
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Salzburg	Salzburg	k1gInSc1	Salzburg
Management	management	k1gInSc1	management
Business	business	k1gInSc1	business
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
Salcburská	salcburský	k2eAgFnSc1d1	Salcburská
škola	škola	k1gFnSc1	škola
obchodního	obchodní	k2eAgInSc2d1	obchodní
managementu	management	k1gInSc2	management
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc1	pět
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
Universität	Universität	k2eAgInSc4d1	Universität
Salzburg	Salzburg	k1gInSc4	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
výhradně	výhradně	k6eAd1	výhradně
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
otevřením	otevření	k1gNnSc7	otevření
soukromé	soukromý	k2eAgFnSc2d1	soukromá
Paracelsus	Paracelsus	k1gInSc4	Paracelsus
Medizinische	Medizinische	k1gNnSc3	Medizinische
Privatuniversität	Privatuniversitäta	k1gFnPc2	Privatuniversitäta
(	(	kIx(	(
<g/>
Paracelsovy	Paracelsův	k2eAgFnSc2d1	Paracelsova
lékařské	lékařský	k2eAgFnSc2d1	lékařská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
chybějící	chybějící	k2eAgFnSc1d1	chybějící
fakulta	fakulta	k1gFnSc1	fakulta
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Třetina	třetina	k1gFnSc1	třetina
peněz	peníze	k1gInPc2	peníze
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
jde	jít	k5eAaImIp3nS	jít
přesto	přesto	k8xC	přesto
ze	z	k7c2	z
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Přednáší	přednášet	k5eAaImIp3nS	přednášet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
látka	látka	k1gFnSc1	látka
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
lidské	lidský	k2eAgFnSc2d1	lidská
či	či	k8xC	či
molekulární	molekulární	k2eAgFnSc2d1	molekulární
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
PMU	PMU	kA	PMU
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
při	při	k7c6	při
praktických	praktický	k2eAgInPc6d1	praktický
výkladech	výklad	k1gInPc6	výklad
též	též	k9	též
s	s	k7c7	s
nemocnicemi	nemocnice	k1gFnPc7	nemocnice
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Rosenheimu	Rosenheim	k1gInSc6	Rosenheim
<g/>
.	.	kIx.	.
</s>
<s>
Spolková	spolkový	k2eAgFnSc1d1	spolková
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
akademie	akademie	k1gFnSc1	akademie
Salzburg	Salzburg	k1gInSc4	Salzburg
Internationale	Internationale	k1gFnSc2	Internationale
Sommerakademie	Sommerakademie	k1gFnSc2	Sommerakademie
für	für	k?	für
Bildende	Bildend	k1gInSc5	Bildend
Kunst	Kunst	k1gInSc4	Kunst
in	in	k?	in
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
letní	letní	k2eAgFnSc1d1	letní
akademie	akademie	k1gFnSc1	akademie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
Religionspädagogische	Religionspädagogische	k1gFnSc1	Religionspädagogische
Akademie	akademie	k1gFnSc2	akademie
der	drát	k5eAaImRp2nS	drát
Erzdiözese	Erzdiözese	k1gFnPc4	Erzdiözese
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Církevní	církevní	k2eAgFnSc1d1	církevní
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
akademie	akademie	k1gFnSc1	akademie
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
Nährstoffakademie	Nährstoffakademie	k1gFnSc1	Nährstoffakademie
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
akademie	akademie	k1gFnSc1	akademie
<g/>
)	)	kIx)	)
Werbedesign	Werbedesign	k1gMnSc1	Werbedesign
Akademie	akademie	k1gFnSc2	akademie
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Akademie	akademie	k1gFnSc2	akademie
propagačního	propagační	k2eAgInSc2d1	propagační
designu	design	k1gInSc2	design
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Akademie	akademie	k1gFnSc2	akademie
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
díla	dílo	k1gNnSc2	dílo
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
Akademie	akademie	k1gFnSc2	akademie
porodního	porodní	k2eAgNnSc2d1	porodní
lékařství	lékařství	k1gNnSc2	lékařství
Akademie	akademie	k1gFnSc2	akademie
ergoterapeutických	ergoterapeutický	k2eAgFnPc2d1	ergoterapeutická
služeb	služba	k1gFnPc2	služba
Akademie	akademie	k1gFnSc2	akademie
med	med	k1gInSc1	med
<g/>
.	.	kIx.	.
techn	techno	k1gNnPc2	techno
<g/>
.	.	kIx.	.
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
služeb	služba	k1gFnPc2	služba
Akademie	akademie	k1gFnSc2	akademie
ortopedických	ortopedický	k2eAgFnPc2d1	ortopedická
služeb	služba	k1gFnPc2	služba
Akademie	akademie	k1gFnSc2	akademie
radio-technických	radioechnický	k2eAgFnPc2d1	radio-technický
služeb	služba	k1gFnPc2	služba
Akademie	akademie	k1gFnSc2	akademie
fyzioterapeutických	fyzioterapeutický	k2eAgFnPc2d1	fyzioterapeutická
služeb	služba	k1gFnPc2	služba
-	-	kIx~	-
Salzburg	Salzburg	k1gInSc1	Salzburg
Experimental	Experimental	k1gMnSc1	Experimental
Dance	Danka	k1gFnSc6	Danka
Academy	Academa	k1gFnSc2	Academa
Pedagogický	pedagogický	k2eAgInSc4d1	pedagogický
spolkový	spolkový	k2eAgInSc4d1	spolkový
institut	institut	k1gInSc4	institut
(	(	kIx(	(
<g/>
Pädagogisches	Pädagogisches	k1gInSc4	Pädagogisches
Institut	institut	k1gInSc1	institut
des	des	k1gNnSc1	des
Bundes	Bundes	k1gInSc1	Bundes
<g/>
)	)	kIx)	)
Religionspädagogisches	Religionspädagogisches	k1gInSc1	Religionspädagogisches
<g />
.	.	kIx.	.
</s>
<s>
Institut	institut	k1gInSc1	institut
der	drát	k5eAaImRp2nS	drát
Erzdiözese	Erzdiözesa	k1gFnSc6	Erzdiözesa
Salzburg	Salzburg	k1gInSc4	Salzburg
(	(	kIx(	(
<g/>
Církevní	církevní	k2eAgInSc4d1	církevní
pedagogický	pedagogický	k2eAgInSc4d1	pedagogický
institut	institut	k1gInSc4	institut
Arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
Institut	institut	k1gInSc1	institut
für	für	k?	für
interdisziplinäre	interdisziplinär	k1gInSc5	interdisziplinär
Tourismusforschung	Tourismusforschung	k1gInSc1	Tourismusforschung
Afroasijský	Afroasijský	k2eAgInSc4d1	Afroasijský
institut	institut	k1gInSc4	institut
Salzburg	Salzburg	k1gInSc1	Salzburg
Salzburg	Salzburg	k1gInSc1	Salzburg
Research	Researcha	k1gFnPc2	Researcha
Salzburg	Salzburg	k1gInSc1	Salzburg
Seminar	Seminar	k1gMnSc1	Seminar
EHI	EHI	kA	EHI
(	(	kIx(	(
<g/>
European	European	k1gInSc1	European
Heart	Hearta	k1gFnPc2	Hearta
Institute	institut	k1gInSc5	institut
<g/>
)	)	kIx)	)
Vedle	vedle	k7c2	vedle
svého	svůj	k3xOyFgInSc2	svůj
významu	význam	k1gInSc2	význam
coby	coby	k?	coby
města	město	k1gNnSc2	město
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
Salzburg	Salzburg	k1gInSc1	Salzburg
díky	díky	k7c3	díky
veletržnímu	veletržní	k2eAgNnSc3d1	veletržní
středisku	středisko	k1gNnSc3	středisko
stále	stále	k6eAd1	stále
většího	veliký	k2eAgInSc2d2	veliký
významu	význam	k1gInSc2	význam
i	i	k9	i
jako	jako	k9	jako
veletržní	veletržní	k2eAgNnSc4d1	veletržní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Salzburg	Salzburg	k1gInSc1	Salzburg
a	a	k8xC	a
hraničící	hraničící	k2eAgFnSc1d1	hraničící
obec	obec	k1gFnSc1	obec
Flachgau	Flachgaus	k1gInSc2	Flachgaus
se	se	k3xPyFc4	se
též	též	k9	též
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
atraktivní	atraktivní	k2eAgNnPc4d1	atraktivní
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
velké	velký	k2eAgInPc4d1	velký
i	i	k8xC	i
malé	malý	k2eAgInPc4d1	malý
podniky	podnik	k1gInPc4	podnik
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
motorem	motor	k1gInSc7	motor
celé	celý	k2eAgFnSc2d1	celá
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Soustavně	soustavně	k6eAd1	soustavně
se	se	k3xPyFc4	se
zvyšující	zvyšující	k2eAgInSc1d1	zvyšující
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Salzbursko	Salzbursko	k1gNnSc1	Salzbursko
staví	stavit	k5eAaPmIp3nS	stavit
tento	tento	k3xDgInSc1	tento
region	region	k1gInSc1	region
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
hned	hned	k6eAd1	hned
za	za	k7c4	za
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
síla	síla	k1gFnSc1	síla
celé	celý	k2eAgFnSc2d1	celá
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
především	především	k9	především
v	v	k7c6	v
nejlépe	dobře	k6eAd3	dobře
rozvinutém	rozvinutý	k2eAgInSc6d1	rozvinutý
centrálním	centrální	k2eAgInSc6d1	centrální
prostoru	prostor	k1gInSc6	prostor
okolo	okolo	k7c2	okolo
samotného	samotný	k2eAgNnSc2d1	samotné
zemského	zemský	k2eAgNnSc2d1	zemské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
(	(	kIx(	(
<g/>
ve	v	k7c4	v
Flachgau	Flachgaa	k1gFnSc4	Flachgaa
a	a	k8xC	a
Tennengau	Tennengaa	k1gFnSc4	Tennengaa
situovaných	situovaný	k2eAgFnPc2d1	situovaná
<g/>
)	)	kIx)	)
takzvaných	takzvaný	k2eAgInPc6d1	takzvaný
"	"	kIx"	"
<g/>
Tukových	tukový	k2eAgInPc6d1	tukový
opascích	opasek	k1gInPc6	opasek
<g/>
"	"	kIx"	"
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgInPc1d1	městský
závody	závod	k1gInPc1	závod
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
ve	v	k7c6	v
Flachgau	Flachgaum	k1gNnSc6	Flachgaum
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
dohromady	dohromady	k6eAd1	dohromady
90	[number]	k4	90
<g/>
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
obratu	obrat	k1gInSc2	obrat
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zbylá	zbylý	k2eAgNnPc1d1	zbylé
procenta	procento	k1gNnPc1	procento
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
mezi	mezi	k7c4	mezi
horské	horský	k2eAgFnPc4d1	horská
župy	župa	k1gFnPc4	župa
(	(	kIx(	(
<g/>
Tennengau	Tennengaus	k1gInSc2	Tennengaus
3	[number]	k4	3
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Pongau	Pongaa	k1gFnSc4	Pongaa
3	[number]	k4	3
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Pinzgau	Pinzgaa	k1gFnSc4	Pinzgaa
3	[number]	k4	3
%	%	kIx~	%
<g/>
,	,	kIx,	,
Lungau	Lungaa	k1gFnSc4	Lungaa
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Závody	závod	k1gInPc1	závod
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
obratem	obrat	k1gInSc7	obrat
a	a	k8xC	a
s	s	k7c7	s
trvalým	trvalý	k2eAgNnSc7d1	trvalé
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Salzburku	Salzburk	k1gInSc2	Salzburk
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byly	být	k5eAaImAgFnP	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
eurech	euro	k1gNnPc6	euro
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Porsche	Porsche	k1gNnSc1	Porsche
Holding	holding	k1gInSc1	holding
GmbH	GmbH	k1gMnSc1	GmbH
(	(	kIx(	(
<g/>
7,900	[number]	k4	7,900
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
SPAR	SPAR	k?	SPAR
Österreichische	Österreichische	k1gInSc1	Österreichische
Warenhandels-AG	Warenhandels-AG	k1gFnSc1	Warenhandels-AG
(	(	kIx(	(
<g/>
3,940	[number]	k4	3,940
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
Porsche	Porsche	k1gNnSc1	Porsche
Austria	Austrium	k1gNnSc2	Austrium
GmbH	GmbH	k1gMnSc2	GmbH
&	&	k?	&
Co	co	k9	co
(	(	kIx(	(
<g/>
1,492	[number]	k4	1,492
mld.	mld.	k?	mld.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Porsche	Porsche	k1gNnSc1	Porsche
Inter	Intra	k1gFnPc2	Intra
Auto	auto	k1gNnSc1	auto
GmbH	GmbH	k1gMnSc1	GmbH
&	&	k?	&
CoKG	CoKG	k1gMnSc1	CoKG
(	(	kIx(	(
<g/>
1,170	[number]	k4	1,170
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
Interspar	Interspar	k1gMnSc1	Interspar
GmbH	GmbH	k1gMnSc1	GmbH
(	(	kIx(	(
<g/>
1,130	[number]	k4	1,130
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
Salzburg	Salzburg	k1gInSc1	Salzburg
AG	AG	kA	AG
für	für	k?	für
Energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
Verkehr	Verkehr	k1gInSc1	Verkehr
und	und	k?	und
Telekommunikation	Telekommunikation	k1gInSc1	Telekommunikation
(	(	kIx(	(
<g/>
0,597	[number]	k4	0,597
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
Wüstenrot	Wüstenrot	k1gMnSc1	Wüstenrot
Versicherungs-AG	Versicherungs-AG	k1gMnSc1	Versicherungs-AG
(	(	kIx(	(
<g/>
0,505	[number]	k4	0,505
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
BMW	BMW	kA	BMW
Austria	Austrium	k1gNnPc4	Austrium
GmbH	GmbH	k1gFnSc2	GmbH
(	(	kIx(	(
<g/>
0,486	[number]	k4	0,486
<g />
.	.	kIx.	.
</s>
<s>
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
Georg	Georg	k1gInSc1	Georg
Pappas	Pappas	k1gInSc1	Pappas
Automobil	automobil	k1gInSc4	automobil
AG	AG	kA	AG
(	(	kIx(	(
<g/>
0,416	[number]	k4	0,416
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
Ford	ford	k1gInSc1	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
Austria	Austrium	k1gNnSc2	Austrium
<g/>
)	)	kIx)	)
GmbH	GmbH	k1gFnSc1	GmbH
<g/>
(	(	kIx(	(
<g/>
0,363	[number]	k4	0,363
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
Porsche	Porsche	k1gNnSc1	Porsche
Konstruktionen	Konstruktionna	k1gFnPc2	Konstruktionna
GmbH	GmbH	k1gFnPc2	GmbH
&	&	k?	&
Co	co	k9	co
KG	kg	kA	kg
(	(	kIx(	(
<g/>
0,325	[number]	k4	0,325
mld.	mld.	k?	mld.
<g/>
)	)	kIx)	)
Conoco	Conoco	k1gMnSc1	Conoco
Phillips	Phillips	k1gInSc4	Phillips
Austria	Austrium	k1gNnSc2	Austrium
GmbH	GmbH	k1gFnSc2	GmbH
(	(	kIx(	(
<g/>
0,283	[number]	k4	0,283
mld.	mld.	k?	mld.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Raiffeisenverband	Raiffeisenverband	k1gInSc1	Raiffeisenverband
Salzburg	Salzburg	k1gInSc1	Salzburg
Bausparkasse	Bausparkasse	k1gFnSc1	Bausparkasse
Wüstenrot	Wüstenrot	k1gMnSc1	Wüstenrot
AG	AG	kA	AG
Salzburger	Salzburger	k1gMnSc1	Salzburger
Sparkasse	Sparkasse	k1gFnSc2	Sparkasse
Bank	bank	k1gInSc1	bank
AG	AG	kA	AG
Salzburger	Salzburger	k1gInSc1	Salzburger
Landes-Hypothekenbank	Landes-Hypothekenbank	k1gInSc1	Landes-Hypothekenbank
Volksbank	Volksbanka	k1gFnPc2	Volksbanka
Salzburg	Salzburg	k1gInSc1	Salzburg
Bankhaus	Bankhaus	k1gMnSc1	Bankhaus
Carl	Carl	k1gMnSc1	Carl
Spängler	Spängler	k1gMnSc1	Spängler
&	&	k?	&
Co	co	k9	co
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
prováděném	prováděný	k2eAgNnSc6d1	prováděné
organizací	organizace	k1gFnPc2	organizace
Statistik	statistika	k1gFnPc2	statistika
Austria	Austrium	k1gNnSc2	Austrium
s	s	k7c7	s
určujícím	určující	k2eAgInSc7d1	určující
dnem	den	k1gInSc7	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
napočítáno	napočítat	k5eAaPmNgNnS	napočítat
10	[number]	k4	10
210	[number]	k4	210
pracovišť	pracoviště	k1gNnPc2	pracoviště
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
105	[number]	k4	105
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
a	a	k8xC	a
53	[number]	k4	53
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
)	)	kIx)	)
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
100	[number]	k4	100
055	[number]	k4	055
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
12	[number]	k4	12
%	%	kIx~	%
více	hodně	k6eAd2	hodně
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
o	o	k7c4	o
24,8	[number]	k4	24,8
%	%	kIx~	%
více	hodně	k6eAd2	hodně
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
sektoru	sektor	k1gInSc6	sektor
(	(	kIx(	(
<g/>
počítáno	počítat	k5eAaImNgNnS	počítat
na	na	k7c4	na
spolkovou	spolkový	k2eAgFnSc4d1	spolková
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
vztaženo	vztažen	k2eAgNnSc1d1	vztaženo
čistě	čistě	k6eAd1	čistě
k	k	k7c3	k
firmám	firma	k1gFnPc3	firma
s	s	k7c7	s
trvalým	trvalý	k2eAgNnSc7d1	trvalé
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
SPAR	SPAR	k?	SPAR
Österreichische	Österreichische	k1gFnSc1	Österreichische
Warenhandels-AG	Warenhandels-AG	k1gFnSc2	Warenhandels-AG
se	se	k3xPyFc4	se
16	[number]	k4	16
000	[number]	k4	000
pracovníky	pracovník	k1gMnPc7	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
následují	následovat	k5eAaImIp3nP	následovat
Porsche	Porsche	k1gNnSc4	Porsche
Holding	holding	k1gInSc1	holding
GmbH	GmbH	k1gFnSc2	GmbH
se	se	k3xPyFc4	se
14	[number]	k4	14
670	[number]	k4	670
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Interspar	Interspar	k1gMnSc1	Interspar
GmbH	GmbH	k1gMnSc1	GmbH
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
6	[number]	k4	6
000	[number]	k4	000
zaměstnaných	zaměstnaný	k1gMnPc2	zaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
významným	významný	k2eAgMnPc3d1	významný
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
patří	patřit	k5eAaImIp3nS	patřit
Porsche	Porsche	k1gNnSc1	Porsche
Inter	Intra	k1gFnPc2	Intra
Auto	auto	k1gNnSc1	auto
GmbH	GmbH	k1gMnSc1	GmbH
&	&	k?	&
CoKG	CoKG	k1gMnSc1	CoKG
(	(	kIx(	(
<g/>
3	[number]	k4	3
780	[number]	k4	780
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
AG	AG	kA	AG
(	(	kIx(	(
<g/>
2	[number]	k4	2
0	[number]	k4	0
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Raiffeisenverband	Raiffeisenverband	k1gInSc1	Raiffeisenverband
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
1	[number]	k4	1
618	[number]	k4	618
<g/>
)	)	kIx)	)
a	a	k8xC	a
Quehenberger	Quehenberger	k1gMnSc1	Quehenberger
Logistik	logistik	k1gMnSc1	logistik
AG	AG	kA	AG
&	&	k?	&
CoKG	CoKG	k1gFnSc1	CoKG
s	s	k7c7	s
1	[number]	k4	1
615	[number]	k4	615
pracovníky	pracovník	k1gMnPc7	pracovník
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Stav	stav	k1gInSc1	stav
<g/>
:	:	kIx,	:
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Krajské	krajský	k2eAgNnSc1d1	krajské
studio	studio	k1gNnSc1	studio
ORF	ORF	kA	ORF
Salzburg	Salzburg	k1gInSc1	Salzburg
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Nonntal	Nonntal	k1gMnSc1	Nonntal
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
střediskem	středisko	k1gNnSc7	středisko
výroby	výroba	k1gFnSc2	výroba
pro	pro	k7c4	pro
Radio	radio	k1gNnSc4	radio
Salzburg	Salzburg	k1gInSc1	Salzburg
a	a	k8xC	a
salzburských	salzburský	k2eAgInPc2d1	salzburský
příspěvků	příspěvek	k1gInPc2	příspěvek
pro	pro	k7c4	pro
ORF	ORF	kA	ORF
<g/>
.	.	kIx.	.
</s>
<s>
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
Salzburg	Salzburg	k1gInSc1	Salzburg
TV	TV	kA	TV
nabízí	nabízet	k5eAaImIp3nS	nabízet
silně	silně	k6eAd1	silně
regionální	regionální	k2eAgInSc4d1	regionální
program	program	k1gInSc4	program
a	a	k8xC	a
vysílá	vysílat	k5eAaImIp3nS	vysílat
od	od	k7c2	od
r.	r.	kA	r.
1995	[number]	k4	1995
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
kabelové	kabelový	k2eAgFnSc6d1	kabelová
síti	síť	k1gFnSc6	síť
SAFE	safe	k1gInSc1	safe
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Salzburg	Salzburg	k1gInSc1	Salzburg
AG	AG	kA	AG
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
též	též	k9	též
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pozemního	pozemní	k2eAgInSc2d1	pozemní
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Salzburger	Salzburger	k1gInSc1	Salzburger
Nachrichten	Nachrichten	k2eAgMnSc1d1	Nachrichten
Salzburger	Salzburger	k1gMnSc1	Salzburger
Fenster	Fenster	k1gMnSc1	Fenster
Salzburger	Salzburger	k1gMnSc1	Salzburger
Volkszeitung	Volkszeitung	k1gMnSc1	Volkszeitung
Stadtblatt	Stadtblatt	k2eAgInSc1d1	Stadtblatt
Salzburg	Salzburg	k1gInSc1	Salzburg
ECHO	echo	k1gNnSc1	echo
Salzburgs	Salzburgsa	k1gFnPc2	Salzburgsa
erste	erste	k5eAaPmIp2nP	erste
Nachrichtenillustrierte	Nachrichtenillustriert	k1gInSc5	Nachrichtenillustriert
Kronenzeitung	Kronenzeitung	k1gInSc1	Kronenzeitung
Welle	Well	k1gInSc6	Well
1	[number]	k4	1
Antenne	Antenn	k1gInSc5	Antenn
Salzburg	Salzburg	k1gInSc4	Salzburg
Radio	radio	k1gNnSc4	radio
Arabella	Arabello	k1gNnSc2	Arabello
Salzburg	Salzburg	k1gInSc4	Salzburg
Radiofabrik	Radiofabrik	k1gMnSc1	Radiofabrik
Spolkové	spolkový	k2eAgFnSc2d1	spolková
policejní	policejní	k2eAgFnSc2d1	policejní
ředitelství	ředitelství	k1gNnSc2	ředitelství
Salzburg	Salzburg	k1gInSc4	Salzburg
Zemské	zemský	k2eAgNnSc4d1	zemské
studio	studio	k1gNnSc4	studio
ORF	ORF	kA	ORF
Finanční	finanční	k2eAgInSc1d1	finanční
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
Salzburg-Město	Salzburg-Města	k1gFnSc5	Salzburg-Města
<g/>
,	,	kIx,	,
Hallein	Hallein	k1gMnSc1	Hallein
a	a	k8xC	a
Salzburg-Venkov	Salzburg-Venkov	k1gInSc1	Salzburg-Venkov
Arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
salcburská	salcburský	k2eAgFnSc1d1	Salcburská
Atlanta	Atlanta	k1gFnSc1	Atlanta
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
Bern	Bern	k1gInSc1	Bern
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Busseto	Busseto	k1gNnSc1	Busseto
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Kawasaki	Kawasaki	k1gNnSc2	Kawasaki
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
León	Leóna	k1gFnPc2	Leóna
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Merano	Merana	k1gFnSc5	Merana
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc5	Itálie
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Reims	Reimsa	k1gFnPc2	Reimsa
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
Sã	Sã	k1gFnSc1	Sã
Joã	Joã	k1gFnSc1	Joã
da	da	k?	da
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Verona	Verona	k1gFnSc1	Verona
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
1756	[number]	k4	1756
<g/>
–	–	k?	–
<g/>
1791	[number]	k4	1791
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Christian	Christian	k1gMnSc1	Christian
Doppler	Doppler	k1gMnSc1	Doppler
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
východorakouský	východorakouský	k2eAgMnSc1d1	východorakouský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
Řecký	řecký	k2eAgMnSc1d1	řecký
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgMnSc1d1	řecký
král	král	k1gMnSc1	král
Hans	Hans	k1gMnSc1	Hans
Makart	Makarta	k1gFnPc2	Makarta
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
dekoratér	dekoratér	k1gMnSc1	dekoratér
Leopold	Leopold	k1gMnSc1	Leopold
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Salvátor	Salvátor	k1gMnSc1	Salvátor
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Georg	Georg	k1gMnSc1	Georg
Trakl	Trakl	k1gInSc1	Trakl
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Anton	Anton	k1gMnSc1	Anton
von	von	k1gInSc4	von
Arco	Arco	k1gMnSc1	Arco
auf	auf	k?	auf
Valley	Vallea	k1gFnSc2	Vallea
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anarchista	anarchista	k1gMnSc1	anarchista
<g/>
,	,	kIx,	,
zahynul	zahynout	k5eAaPmAgInS	zahynout
zde	zde	k6eAd1	zde
Herbert	Herbert	k1gInSc1	Herbert
von	von	k1gInSc1	von
Karajan	Karajan	k1gInSc1	Karajan
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Lída	Lída	k1gFnSc1	Lída
Baarová	Baarová	k1gFnSc1	Baarová
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Holzbauer	Holzbauer	k1gMnSc1	Holzbauer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
Georg	Georg	k1gMnSc1	Georg
Zundel	Zundlo	k1gNnPc2	Zundlo
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgInSc1d1	německý
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
<g />
.	.	kIx.	.
</s>
<s>
chemik	chemik	k1gMnSc1	chemik
Benita	Benita	k1gMnSc1	Benita
Ferrero-Waldner	Ferrero-Waldner	k1gMnSc1	Ferrero-Waldner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eurokomisařka	eurokomisařka	k1gFnSc1	eurokomisařka
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
Roland	Rolanda	k1gFnPc2	Rolanda
Ratzenberger	Ratzenbergra	k1gFnPc2	Ratzenbergra
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pilot	pilot	k1gMnSc1	pilot
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
Angelika	Angelika	k1gFnSc1	Angelika
Kirchschlager	Kirchschlagra	k1gFnPc2	Kirchschlagra
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnPc1d1	operní
pěvkyně	pěvkyně	k1gFnPc1	pěvkyně
Karl-Markus	Karl-Markus	k1gMnSc1	Karl-Markus
Gauß	Gauß	k1gMnSc1	Gauß
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
</s>
