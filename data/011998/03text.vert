<p>
<s>
Silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
<g/>
,	,	kIx,	,
či	či	k8xC	či
silná	silný	k2eAgFnSc1d1	silná
(	(	kIx(	(
<g/>
jaderná	jaderný	k2eAgFnSc1d1	jaderná
<g/>
)	)	kIx)	)
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
základních	základní	k2eAgFnPc2d1	základní
interakcí	interakce	k1gFnPc2	interakce
<g/>
,	,	kIx,	,
působících	působící	k2eAgFnPc2d1	působící
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Zprostředkovatelem	zprostředkovatel	k1gMnSc7	zprostředkovatel
této	tento	k3xDgFnSc2	tento
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
částice	částice	k1gFnSc1	částice
gluon	gluon	k1gInSc1	gluon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působnost	působnost	k1gFnSc1	působnost
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
subatomární	subatomární	k2eAgFnSc4d1	subatomární
úroveň	úroveň	k1gFnSc4	úroveň
(	(	kIx(	(
<g/>
dosah	dosah	k1gInSc1	dosah
této	tento	k3xDgFnSc2	tento
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
m	m	kA	m
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sílu	síla	k1gFnSc4	síla
krátkého	krátký	k2eAgInSc2d1	krátký
dosahu	dosah	k1gInSc2	dosah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
soudržnost	soudržnost	k1gFnSc4	soudržnost
kvarků	kvark	k1gInPc2	kvark
tvořících	tvořící	k2eAgInPc2d1	tvořící
hadrony	hadron	k1gInPc7	hadron
<g/>
,	,	kIx,	,
např.	např.	kA	např.
protony	proton	k1gInPc4	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
za	za	k7c4	za
udržení	udržení	k1gNnSc4	udržení
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
popisující	popisující	k2eAgNnSc4d1	popisující
chování	chování	k1gNnSc4	chování
silnou	silný	k2eAgFnSc4d1	silná
interakci	interakce	k1gFnSc4	interakce
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kvantová	kvantový	k2eAgFnSc1d1	kvantová
chromodynamika	chromodynamika	k1gFnSc1	chromodynamika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
základní	základní	k2eAgFnSc1d1	základní
interakce	interakce	k1gFnSc1	interakce
</s>
</p>
<p>
<s>
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
</s>
</p>
<p>
<s>
intermediální	intermediální	k2eAgFnPc1d1	intermediální
částice	částice	k1gFnPc1	částice
</s>
</p>
<p>
<s>
slabá	slabý	k2eAgFnSc1d1	slabá
interakce	interakce	k1gFnSc1	interakce
</s>
</p>
