<s>
Silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
<g/>
,	,	kIx,	,
či	či	k8xC	či
silná	silný	k2eAgFnSc1d1	silná
(	(	kIx(	(
<g/>
jaderná	jaderný	k2eAgFnSc1d1	jaderná
<g/>
)	)	kIx)	)
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
základních	základní	k2eAgFnPc2d1	základní
interakcí	interakce	k1gFnPc2	interakce
<g/>
,	,	kIx,	,
působících	působící	k2eAgFnPc2d1	působící
mezi	mezi	k7c7	mezi
částicemi	částice	k1gFnPc7	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
