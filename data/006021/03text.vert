<s>
Bowling	bowling	k1gInSc1	bowling
je	být	k5eAaImIp3nS	být
halový	halový	k2eAgInSc4d1	halový
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
snaží	snažit	k5eAaImIp3nS	snažit
hozenou	hozený	k2eAgFnSc7d1	hozená
koulí	koule	k1gFnSc7	koule
srazit	srazit	k5eAaPmF	srazit
co	co	k9	co
možná	možná	k9	možná
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
kuželek	kuželka	k1gFnPc2	kuželka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejího	její	k3xOp3gMnSc4	její
předchůdce	předchůdce	k1gMnSc4	předchůdce
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
již	již	k6eAd1	již
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc1	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
podobná	podobný	k2eAgFnSc1d1	podobná
kuželkám	kuželka	k1gFnPc3	kuželka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
sporty	sport	k1gInPc7	sport
odlišnosti	odlišnost	k1gFnSc2	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
bowlingu	bowling	k1gInSc2	bowling
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
bowlingovou	bowlingový	k2eAgFnSc4d1	bowlingová
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
deseti	deset	k4xCc7	deset
kuželkami	kuželka	k1gFnPc7	kuželka
<g/>
,	,	kIx,	,
specifickou	specifický	k2eAgFnSc4d1	specifická
bowlingovou	bowlingový	k2eAgFnSc4d1	bowlingová
kouli	koule	k1gFnSc4	koule
a	a	k8xC	a
vhodné	vhodný	k2eAgFnPc4d1	vhodná
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
speciální	speciální	k2eAgFnPc4d1	speciální
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kuželkách	kuželka	k1gFnPc6	kuželka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
jen	jen	k9	jen
devět	devět	k4xCc4	devět
kuželek	kuželka	k1gFnPc2	kuželka
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
i	i	k9	i
koule	koule	k1gFnSc1	koule
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
hry	hra	k1gFnSc2	hra
hází	házet	k5eAaImIp3nS	házet
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
deseti	deset	k4xCc6	deset
kolech	kolo	k1gNnPc6	kolo
na	na	k7c4	na
kuželky	kuželka	k1gFnPc4	kuželka
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
všechny	všechen	k3xTgFnPc4	všechen
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepovede	vést	k5eNaImIp3nS	vést
prvním	první	k4xOgInSc7	první
hodem	hod	k1gInSc7	hod
daného	daný	k2eAgNnSc2d1	dané
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ještě	ještě	k6eAd1	ještě
hod	hod	k1gInSc1	hod
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
–	–	k?	–
desátém	desátý	k4xOgInSc6	desátý
–	–	k?	–
kole	kolo	k1gNnSc6	kolo
shodí	shodit	k5eAaPmIp3nS	shodit
nejpozději	pozdě	k6eAd3	pozdě
druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
kuželky	kuželka	k1gFnSc2	kuželka
všechny	všechen	k3xTgFnPc1	všechen
<g/>
,	,	kIx,	,
hází	házet	k5eAaImIp3nS	házet
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
kole	kolo	k1gNnSc6	kolo
ještě	ještě	k9	ještě
potřetí	potřetí	k4xO	potřetí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
hru	hra	k1gFnSc4	hra
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
maximálně	maximálně	k6eAd1	maximálně
300	[number]	k4	300
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
poražené	poražený	k2eAgFnPc1d1	poražená
kuželky	kuželka	k1gFnPc1	kuželka
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
poražení	poražení	k1gNnSc2	poražení
všech	všecek	k3xTgFnPc2	všecek
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
bodové	bodový	k2eAgFnPc1d1	bodová
bonifikace	bonifikace	k1gFnPc1	bonifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bowlingu	bowling	k1gInSc6	bowling
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
kontinentálním	kontinentální	k2eAgNnSc6d1	kontinentální
či	či	k8xC	či
národním	národní	k2eAgNnSc6d1	národní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc7	hráč
bowlingu	bowling	k1gInSc2	bowling
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
bowlingové	bowlingový	k2eAgFnSc6d1	bowlingová
asociaci	asociace	k1gFnSc6	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
bowlingu	bowling	k1gInSc2	bowling
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
neklade	klást	k5eNaImIp3nS	klást
výrazné	výrazný	k2eAgNnSc4d1	výrazné
fyzické	fyzický	k2eAgNnSc4d1	fyzické
omezení	omezení	k1gNnSc4	omezení
na	na	k7c4	na
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
mohou	moct	k5eAaImIp3nP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
poměřovat	poměřovat	k5eAaImF	poměřovat
jak	jak	k6eAd1	jak
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
starší	starý	k2eAgMnPc1d2	starší
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rychlému	rychlý	k2eAgNnSc3d1	rychlé
střídání	střídání	k1gNnSc3	střídání
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
má	mít	k5eAaImIp3nS	mít
hra	hra	k1gFnSc1	hra
napětí	napětí	k1gNnSc1	napětí
a	a	k8xC	a
spád	spád	k1gInSc4	spád
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hráči	hráč	k1gMnPc1	hráč
mohou	moct	k5eAaImIp3nP	moct
vzájemně	vzájemně	k6eAd1	vzájemně
porovnávat	porovnávat	k5eAaImF	porovnávat
své	svůj	k3xOyFgInPc4	svůj
průběžné	průběžný	k2eAgInPc4d1	průběžný
výsledky	výsledek	k1gInPc4	výsledek
a	a	k8xC	a
aktuální	aktuální	k2eAgInPc4d1	aktuální
bodové	bodový	k2eAgInPc4d1	bodový
zisky	zisk	k1gInPc4	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sport	sport	k1gInSc4	sport
pohodlný	pohodlný	k2eAgInSc1d1	pohodlný
a	a	k8xC	a
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
a	a	k8xC	a
díky	díky	k7c3	díky
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
zápasy	zápas	k1gInPc1	zápas
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
krytých	krytý	k2eAgInPc6d1	krytý
prostorech	prostor	k1gInPc6	prostor
<g/>
,	,	kIx,	,
i	i	k8xC	i
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
na	na	k7c6	na
případné	případný	k2eAgFnSc6d1	případná
nepřízni	nepřízeň	k1gFnSc6	nepřízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Předměty	předmět	k1gInPc1	předmět
připomínající	připomínající	k2eAgFnSc4d1	připomínající
kouli	koule	k1gFnSc4	koule
a	a	k8xC	a
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
propriety	proprieta	k1gFnPc1	proprieta
potřebné	potřebný	k2eAgFnPc1d1	potřebná
ke	k	k7c3	k
kuželkám	kuželka	k1gFnPc3	kuželka
<g/>
,	,	kIx,	,
předchůdci	předchůdce	k1gMnPc1	předchůdce
bowlingu	bowling	k1gInSc2	bowling
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
během	běh	k1gInSc7	běh
vykopávek	vykopávka	k1gFnPc2	vykopávka
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
britským	britský	k2eAgMnSc7d1	britský
antropologem	antropolog	k1gMnSc7	antropolog
Sirem	sir	k1gMnSc7	sir
Flindersem	Flinders	k1gMnSc7	Flinders
Petriem	Petrium	k1gNnSc7	Petrium
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
hrobě	hrob	k1gInSc6	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Pocházely	pocházet	k5eAaImAgFnP	pocházet
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
období	období	k1gNnSc2	období
3200	[number]	k4	3200
let	léto	k1gNnPc2	léto
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
dějepisec	dějepisec	k1gMnSc1	dějepisec
William	William	k1gInSc4	William
Pehle	Pehle	k1gNnSc2	Pehle
ovšem	ovšem	k9	ovšem
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bowling	bowling	k1gInSc1	bowling
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Německa	Německo	k1gNnSc2	Německo
z	z	k7c2	z
období	období	k1gNnSc2	období
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
300	[number]	k4	300
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
náboženského	náboženský	k2eAgInSc2d1	náboženský
obřadu	obřad	k1gInSc2	obřad
a	a	k8xC	a
o	o	k7c6	o
těch	ten	k3xDgMnPc6	ten
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
kuželky	kuželka	k1gFnPc4	kuželka
srazit	srazit	k5eAaPmF	srazit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
učinit	učinit	k5eAaImF	učinit
pokání	pokání	k1gNnSc4	pokání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1366	[number]	k4	1366
kuželky	kuželka	k1gFnSc2	kuželka
údajně	údajně	k6eAd1	údajně
zakázal	zakázat	k5eAaPmAgMnS	zakázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
muže	muž	k1gMnSc4	muž
neodpoutávaly	odpoutávat	k5eNaImAgInP	odpoutávat
od	od	k7c2	od
tréninku	trénink	k1gInSc2	trénink
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc1d1	populární
byla	být	k5eAaImAgFnS	být
i	i	k9	i
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1509	[number]	k4	1509
<g/>
–	–	k?	–
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kuželky	kuželka	k1gFnPc1	kuželka
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
hrál	hrát	k5eAaImAgMnS	hrát
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
také	také	k9	také
německý	německý	k2eAgMnSc1d1	německý
teolog	teolog	k1gMnSc1	teolog
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
však	však	k9	však
používala	používat	k5eAaImAgFnS	používat
mírně	mírně	k6eAd1	mírně
odlišná	odlišný	k2eAgNnPc4d1	odlišné
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zámořským	zámořský	k2eAgInPc3d1	zámořský
objevům	objev	k1gInPc3	objev
a	a	k8xC	a
kolonizaci	kolonizace	k1gFnSc6	kolonizace
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
kuželky	kuželka	k1gFnPc1	kuželka
dostaly	dostat	k5eAaPmAgFnP	dostat
i	i	k9	i
za	za	k7c4	za
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	on	k3xPp3gMnPc4	on
hráli	hrát	k5eAaImAgMnP	hrát
osadníci	osadník	k1gMnPc1	osadník
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
či	či	k8xC	či
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
hře	hra	k1gFnSc6	hra
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
americké	americký	k2eAgFnSc6d1	americká
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Rip	Rip	k1gMnPc2	Rip
Van	vana	k1gFnPc2	vana
Winkle	Winkl	k1gInSc5	Winkl
Washingtona	Washington	k1gMnSc4	Washington
Irvinga	Irvinga	k1gFnSc1	Irvinga
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
titulního	titulní	k2eAgInSc2d1	titulní
hrdinu	hrdina	k1gMnSc4	hrdina
probudil	probudit	k5eAaPmAgInS	probudit
zvuk	zvuk	k1gInSc1	zvuk
padajících	padající	k2eAgFnPc2d1	padající
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
dlouhodobějším	dlouhodobý	k2eAgInSc7d2	dlouhodobější
hřištěm	hřiště	k1gNnSc7	hřiště
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
travnaté	travnatý	k2eAgNnSc4d1	travnaté
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
"	"	kIx"	"
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Battery	Batter	k1gMnPc7	Batter
area	area	k1gFnSc1	area
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vojenské	vojenský	k2eAgFnPc4d1	vojenská
baterie	baterie	k1gFnPc4	baterie
<g/>
)	)	kIx)	)
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
centru	centrum	k1gNnSc6	centrum
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
"	"	kIx"	"
<g/>
Bowlingový	Bowlingový	k2eAgInSc1d1	Bowlingový
trávník	trávník	k1gInSc1	trávník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Connecticutu	Connecticut	k1gInSc6	Connecticut
vydán	vydat	k5eAaPmNgInS	vydat
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kuželky	kuželka	k1gFnPc4	kuželka
zařadil	zařadit	k5eAaPmAgMnS	zařadit
mezi	mezi	k7c4	mezi
zakázané	zakázaný	k2eAgFnPc4d1	zakázaná
hazardní	hazardní	k2eAgFnPc4d1	hazardní
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zákon	zákon	k1gInSc1	zákon
hovořil	hovořit	k5eAaImAgInS	hovořit
o	o	k7c6	o
hře	hra	k1gFnSc6	hra
s	s	k7c7	s
devíti	devět	k4xCc7	devět
kuželkami	kuželka	k1gFnPc7	kuželka
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnPc1	hráč
ho	on	k3xPp3gMnSc4	on
obešli	obejít	k5eAaPmAgMnP	obejít
přidáním	přidání	k1gNnSc7	přidání
jedné	jeden	k4xCgFnSc2	jeden
kuželky	kuželka	k1gFnSc2	kuželka
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
upravil	upravit	k5eAaPmAgMnS	upravit
jejich	jejich	k3xOp3gInSc4	jejich
rozměr	rozměr	k1gInSc4	rozměr
a	a	k8xC	a
rozestavení	rozestavení	k1gNnSc4	rozestavení
<g/>
,	,	kIx,	,
pozměnily	pozměnit	k5eAaPmAgInP	pozměnit
se	se	k3xPyFc4	se
též	též	k9	též
průměr	průměr	k1gInSc1	průměr
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
se	se	k3xPyFc4	se
položily	položit	k5eAaPmAgFnP	položit
parkety	parketa	k1gFnPc1	parketa
a	a	k8xC	a
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
bowling	bowling	k1gInSc4	bowling
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
časté	častý	k2eAgFnPc1d1	častá
výmluvy	výmluva	k1gFnPc1	výmluva
prohrávajících	prohrávající	k2eAgMnPc2d1	prohrávající
objevovaly	objevovat	k5eAaImAgFnP	objevovat
stížnosti	stížnost	k1gFnPc1	stížnost
na	na	k7c4	na
špatnou	špatný	k2eAgFnSc4d1	špatná
údržbu	údržba	k1gFnSc4	údržba
venkovních	venkovní	k2eAgFnPc2d1	venkovní
bowlingových	bowlingový	k2eAgFnPc2d1	bowlingová
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
vlivní	vlivný	k2eAgMnPc1d1	vlivný
průmysloví	průmyslový	k2eAgMnPc1d1	průmyslový
magnáti	magnát	k1gMnPc1	magnát
instalovat	instalovat	k5eAaBmF	instalovat
bowlingové	bowlingový	k2eAgFnPc1d1	bowlingová
dráhy	dráha	k1gFnPc1	dráha
do	do	k7c2	do
krytých	krytý	k2eAgFnPc2d1	krytá
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
tyto	tento	k3xDgFnPc1	tento
herny	herna	k1gFnPc1	herna
sloužily	sloužit	k5eAaImAgFnP	sloužit
pro	pro	k7c4	pro
potěšení	potěšení	k1gNnSc4	potěšení
svých	svůj	k3xOyFgMnPc2	svůj
majitelů	majitel	k1gMnPc2	majitel
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
otevřely	otevřít	k5eAaPmAgFnP	otevřít
veřejnosti	veřejnost	k1gFnPc1	veřejnost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
popularita	popularita	k1gFnSc1	popularita
celé	celý	k2eAgFnSc2d1	celá
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
amerických	americký	k2eAgInPc6d1	americký
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
nebo	nebo	k8xC	nebo
Illinois	Illinois	k1gInSc1	Illinois
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stal	stát	k5eAaPmAgInS	stát
bowling	bowling	k1gInSc4	bowling
převládajícím	převládající	k2eAgInSc7d1	převládající
sportem	sport	k1gInSc7	sport
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
americkými	americký	k2eAgInPc7d1	americký
státy	stát	k1gInPc7	stát
ovšem	ovšem	k9	ovšem
nebyly	být	k5eNaImAgInP	být
rozměry	rozměr	k1gInPc1	rozměr
bowlingových	bowlingový	k2eAgFnPc2d1	bowlingová
koulí	koule	k1gFnPc2	koule
či	či	k8xC	či
kuželek	kuželka	k1gFnPc2	kuželka
totožné	totožný	k2eAgFnPc1d1	totožná
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
restauratér	restauratér	k1gMnSc1	restauratér
Joe	Joe	k1gMnSc1	Joe
Thum	Thum	k1gMnSc1	Thum
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
standardizaci	standardizace	k1gFnSc4	standardizace
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
i	i	k9	i
americkou	americký	k2eAgFnSc4d1	americká
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
rekrutovali	rekrutovat	k5eAaImAgMnP	rekrutovat
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
bowlingových	bowlingový	k2eAgInPc2d1	bowlingový
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1895	[number]	k4	1895
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Beethoven	Beethoven	k1gMnSc1	Beethoven
Hall	Hall	k1gInSc1	Hall
"	"	kIx"	"
<g/>
Americký	americký	k2eAgInSc1d1	americký
bowlingový	bowlingový	k2eAgInSc1d1	bowlingový
kongres	kongres	k1gInSc1	kongres
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
American	American	k1gInSc1	American
Bowling	bowling	k1gInSc1	bowling
Congress	Congress	k1gInSc1	Congress
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgNnPc4	první
pravidla	pravidlo	k1gNnPc4	pravidlo
bowlingu	bowling	k1gInSc2	bowling
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
začaly	začít	k5eAaPmAgFnP	začít
bowling	bowling	k1gInSc4	bowling
hrát	hrát	k5eAaImF	hrát
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
až	až	k9	až
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
již	již	k9	již
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
v	v	k7c6	v
St.	st.	kA	st.
Louis	louis	k1gInPc1	louis
založily	založit	k5eAaPmAgInP	založit
ženský	ženský	k2eAgInSc4d1	ženský
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
kongres	kongres	k1gInSc4	kongres
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
existující	existující	k2eAgMnSc1d1	existující
"	"	kIx"	"
<g/>
Americký	americký	k2eAgInSc1d1	americký
bowlingový	bowlingový	k2eAgInSc1d1	bowlingový
kongres	kongres	k1gInSc1	kongres
<g/>
"	"	kIx"	"
totiž	totiž	k9	totiž
sdružoval	sdružovat	k5eAaImAgMnS	sdružovat
pouze	pouze	k6eAd1	pouze
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
ženského	ženský	k2eAgInSc2d1	ženský
kongresu	kongres	k1gInSc2	kongres
Dennis	Dennis	k1gFnSc2	Dennis
Sweeney	Sweenea	k1gFnSc2	Sweenea
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
hráčky	hráčka	k1gFnPc4	hráčka
ženské	ženský	k2eAgFnSc2d1	ženská
ligy	liga	k1gFnSc2	liga
k	k	k7c3	k
uspořádání	uspořádání	k1gNnSc3	uspořádání
bowlingového	bowlingový	k2eAgInSc2d1	bowlingový
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
založení	založení	k1gNnSc3	založení
"	"	kIx"	"
<g/>
Ženského	ženský	k2eAgInSc2d1	ženský
národního	národní	k2eAgInSc2d1	národní
bowlingového	bowlingový	k2eAgInSc2d1	bowlingový
kongresu	kongres	k1gInSc2	kongres
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Women	Womna	k1gFnPc2	Womna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
National	National	k1gFnSc7	National
Bowling	bowling	k1gInSc4	bowling
Congress	Congress	k1gInSc1	Congress
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoji	rozvoj	k1gInSc3	rozvoj
bowlingu	bowling	k1gInSc2	bowling
napomohl	napomoct	k5eAaPmAgInS	napomoct
rozvoj	rozvoj	k1gInSc4	rozvoj
technologie	technologie	k1gFnSc2	technologie
herních	herní	k2eAgFnPc2d1	herní
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Koule	koule	k1gFnSc1	koule
(	(	kIx(	(
<g/>
či	či	k8xC	či
spíše	spíše	k9	spíše
míče	míč	k1gInPc1	míč
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
první	první	k4xOgFnSc1	první
bowlingová	bowlingový	k2eAgFnSc1d1	bowlingová
koule	koule	k1gFnSc1	koule
"	"	kIx"	"
<g/>
Evertrue	Evertrue	k1gInSc1	Evertrue
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
navázala	navázat	k5eAaPmAgFnS	navázat
koule	koule	k1gFnSc1	koule
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
firmou	firma	k1gFnSc7	firma
"	"	kIx"	"
<g/>
Brunswick	Brunswick	k1gInSc1	Brunswick
Corporation	Corporation	k1gInSc1	Corporation
<g/>
"	"	kIx"	"
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
pryže	pryž	k1gFnSc2	pryž
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
materiály	materiál	k1gInPc1	materiál
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
na	na	k7c4	na
povrchy	povrch	k1gInPc4	povrch
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Průlom	průlom	k1gInSc1	průlom
znamenal	znamenat	k5eAaImAgInS	znamenat
též	též	k9	též
vynález	vynález	k1gInSc1	vynález
automatického	automatický	k2eAgMnSc2d1	automatický
zvedače	zvedač	k1gMnSc2	zvedač
a	a	k8xC	a
stavěče	stavěč	k1gMnSc2	stavěč
kuželek	kuželka	k1gFnPc2	kuželka
od	od	k7c2	od
Gottfrieda	Gottfried	k1gMnSc2	Gottfried
Schmidta	Schmidt	k1gMnSc2	Schmidt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tento	tento	k3xDgInSc4	tento
svůj	svůj	k3xOyFgInSc4	svůj
patent	patent	k1gInSc4	patent
předal	předat	k5eAaPmAgMnS	předat
firmě	firma	k1gFnSc3	firma
American	American	k1gMnSc1	American
Machine	Machin	k1gInSc5	Machin
&	&	k?	&
Foundry	Foundra	k1gFnPc4	Foundra
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
AMF	AMF	kA	AMF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
provedla	provést	k5eAaPmAgFnS	provést
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
instalaci	instalace	k1gFnSc4	instalace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
produkce	produkce	k1gFnSc1	produkce
modelů	model	k1gInPc2	model
zahájena	zahájen	k2eAgFnSc1d1	zahájena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
začala	začít	k5eAaPmAgFnS	začít
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
NBC	NBC	kA	NBC
jako	jako	k9	jako
první	první	k4xOgFnSc2	první
vysílat	vysílat	k5eAaImF	vysílat
rozhlasové	rozhlasový	k2eAgFnPc4d1	rozhlasová
reportáže	reportáž	k1gFnPc4	reportáž
z	z	k7c2	z
"	"	kIx"	"
<g/>
Championship	Championship	k1gInSc1	Championship
Bowling	bowling	k1gInSc1	bowling
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
se	se	k3xPyFc4	se
objevovalo	objevovat	k5eAaImAgNnS	objevovat
v	v	k7c6	v
pořadech	pořad	k1gInPc6	pořad
"	"	kIx"	"
<g/>
Make	Make	k1gNnPc2	Make
That	Thata	k1gFnPc2	Thata
Spare	Spar	k1gMnSc5	Spar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Celebrity	celebrita	k1gFnSc2	celebrita
Bowling	bowling	k1gInSc1	bowling
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bowling	bowling	k1gInSc1	bowling
For	forum	k1gNnPc2	forum
Dollars	Dollars	k1gInSc1	Dollars
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc6	rozšíření
televize	televize	k1gFnSc2	televize
přišla	přijít	k5eAaPmAgFnS	přijít
stanice	stanice	k1gFnSc1	stanice
ABC	ABC	kA	ABC
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
přenosem	přenos	k1gInSc7	přenos
bowlingového	bowlingový	k2eAgInSc2d1	bowlingový
zápasu	zápas	k1gInSc2	zápas
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
Bowlers	Bowlers	k1gInSc4	Bowlers
Association	Association	k1gInSc4	Association
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
bowlingu	bowling	k1gInSc2	bowling
do	do	k7c2	do
médií	médium	k1gNnPc2	médium
mělo	mít	k5eAaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
náklonnosti	náklonnost	k1gFnSc2	náklonnost
a	a	k8xC	a
zájmu	zájem	k1gInSc2	zájem
Američanů	Američan	k1gMnPc2	Američan
o	o	k7c4	o
tento	tento	k3xDgInSc4	tento
sport	sport	k1gInSc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
se	se	k3xPyFc4	se
bowling	bowling	k1gInSc1	bowling
začal	začít	k5eAaPmAgInS	začít
postupně	postupně	k6eAd1	postupně
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pomohly	pomoct	k5eAaPmAgFnP	pomoct
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
amatérů	amatér	k1gMnPc2	amatér
v	v	k7c6	v
bowlingu	bowling	k1gInSc6	bowling
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
probíhá	probíhat	k5eAaImIp3nS	probíhat
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
šampionáty	šampionát	k1gInPc1	šampionát
žen	žena	k1gFnPc2	žena
i	i	k8xC	i
mužů	muž	k1gMnPc2	muž
konaly	konat	k5eAaImAgFnP	konat
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
rozmachu	rozmach	k1gInSc3	rozmach
bowlingu	bowling	k1gInSc2	bowling
a	a	k8xC	a
omezené	omezený	k2eAgFnSc3d1	omezená
kapacitě	kapacita	k1gFnSc3	kapacita
heren	herna	k1gFnPc2	herna
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
soutěže	soutěž	k1gFnPc1	soutěž
hrály	hrát	k5eAaImAgFnP	hrát
v	v	k7c6	v
hernách	herna	k1gFnPc6	herna
se	s	k7c7	s
30	[number]	k4	30
<g/>
,	,	kIx,	,
40	[number]	k4	40
i	i	k8xC	i
více	hodně	k6eAd2	hodně
drahami	draha	k1gNnPc7	draha
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
tyto	tento	k3xDgFnPc4	tento
soutěže	soutěž	k1gFnPc4	soutěž
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
sudý	sudý	k2eAgInSc4d1	sudý
rok	rok	k1gInSc4	rok
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
soutěž	soutěž	k1gFnSc4	soutěž
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
lichý	lichý	k2eAgInSc4d1	lichý
rok	rok	k1gInSc4	rok
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
soutěže	soutěž	k1gFnPc1	soutěž
mají	mít	k5eAaImIp3nP	mít
též	též	k9	též
junioři	junior	k1gMnPc1	junior
a	a	k8xC	a
hráči	hráč	k1gMnPc1	hráč
mladších	mladý	k2eAgFnPc2d2	mladší
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
profesionální	profesionální	k2eAgFnPc1d1	profesionální
ligy	liga	k1gFnPc1	liga
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Evropská	evropský	k2eAgFnSc1d1	Evropská
tour	tour	k1gInSc1	tour
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
má	mít	k5eAaImIp3nS	mít
bowling	bowling	k1gInSc4	bowling
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
příznivců	příznivec	k1gMnPc2	příznivec
a	a	k8xC	a
hráčů	hráč	k1gMnPc2	hráč
než	než	k8xS	než
třeba	třeba	k6eAd1	třeba
curling	curling	k1gInSc1	curling
nebo	nebo	k8xC	nebo
moderní	moderní	k2eAgInSc1d1	moderní
pětiboj	pětiboj	k1gInSc1	pětiboj
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
ho	on	k3xPp3gNnSc2	on
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
dříve	dříve	k6eAd2	dříve
patřila	patřit	k5eAaImAgFnS	patřit
v	v	k7c6	v
kuželkách	kuželka	k1gFnPc6	kuželka
ke	k	k7c3	k
světovým	světový	k2eAgFnPc3d1	světová
velmocem	velmoc	k1gFnPc3	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
bowling	bowling	k1gInSc4	bowling
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hra	hra	k1gFnSc1	hra
kuželkám	kuželka	k1gFnPc3	kuželka
podobná	podobný	k2eAgNnPc4d1	podobné
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
mladým	mladý	k2eAgInSc7d1	mladý
sportem	sport	k1gInSc7	sport
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
první	první	k4xOgNnPc1	první
bowlingová	bowlingový	k2eAgNnPc1d1	bowlingové
centra	centrum	k1gNnPc1	centrum
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
objevovat	objevovat	k5eAaImF	objevovat
až	až	k9	až
po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
ale	ale	k9	ale
rychle	rychle	k6eAd1	rychle
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
a	a	k8xC	a
také	také	k6eAd1	také
hráčů	hráč	k1gMnPc2	hráč
přibývá	přibývat	k5eAaImIp3nS	přibývat
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
pořádat	pořádat	k5eAaImF	pořádat
první	první	k4xOgInPc4	první
komerční	komerční	k2eAgInPc4d1	komerční
turnaje	turnaj	k1gInPc4	turnaj
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
první	první	k4xOgNnSc1	první
mistrovství	mistrovství	k1gNnSc1	mistrovství
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pořádá	pořádat	k5eAaImIp3nS	pořádat
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
je	být	k5eAaImIp3nS	být
bowling	bowling	k1gInSc1	bowling
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgMnSc1d1	podobný
klasickým	klasický	k2eAgFnPc3d1	klasická
kuželkám	kuželka	k1gFnPc3	kuželka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
hrách	hra	k1gFnPc6	hra
je	být	k5eAaImIp3nS	být
snahou	snaha	k1gFnSc7	snaha
hráče	hráč	k1gMnSc2	hráč
shodit	shodit	k5eAaPmF	shodit
hodem	hod	k1gInSc7	hod
co	co	k8xS	co
možná	možná	k9	možná
nejvíce	nejvíce	k6eAd1	nejvíce
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
hrami	hra	k1gFnPc7	hra
několik	několik	k4yIc4	několik
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bowlingu	bowling	k1gInSc6	bowling
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
postaveno	postavit	k5eAaPmNgNnS	postavit
10	[number]	k4	10
kuželek	kuželka	k1gFnPc2	kuželka
do	do	k7c2	do
obrazce	obrazec	k1gInSc2	obrazec
tvaru	tvar	k1gInSc2	tvar
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
jedním	jeden	k4xCgInSc7	jeden
hrotem	hrot	k1gInSc7	hrot
k	k	k7c3	k
hráči	hráč	k1gMnSc3	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kuželkách	kuželka	k1gFnPc6	kuželka
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
devíti	devět	k4xCc7	devět
kuželkami	kuželka	k1gFnPc7	kuželka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
sestaveny	sestavit	k5eAaPmNgInP	sestavit
do	do	k7c2	do
obrazce	obrazec	k1gInSc2	obrazec
kosočtverce	kosočtverec	k1gInPc1	kosočtverec
a	a	k8xC	a
kuželky	kuželka	k1gFnPc1	kuželka
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
bowlingovým	bowlingový	k2eAgFnPc3d1	bowlingová
větší	veliký	k2eAgFnPc4d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Bowlingové	Bowlingový	k2eAgFnPc1d1	Bowlingová
koule	koule	k1gFnPc1	koule
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgFnPc1d2	veliký
než	než	k8xS	než
koule	koule	k1gFnPc1	koule
kuželkové	kuželkový	k2eAgFnPc1d1	Kuželková
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgInPc4	tři
otvory	otvor	k1gInPc4	otvor
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
polyuretanové	polyuretanový	k2eAgInPc1d1	polyuretanový
nebo	nebo	k8xC	nebo
reaktivní	reaktivní	k2eAgInPc1d1	reaktivní
polyuretanové	polyuretanový	k2eAgInPc1d1	polyuretanový
s	s	k7c7	s
excentrickým	excentrický	k2eAgNnSc7d1	excentrické
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypuštěná	vypuštěný	k2eAgFnSc1d1	vypuštěná
koule	koule	k1gFnSc1	koule
nepoběží	běžet	k5eNaImIp3nS	běžet
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pohybu	pohyb	k1gInSc2	pohyb
se	se	k3xPyFc4	se
z	z	k7c2	z
přímého	přímý	k2eAgInSc2d1	přímý
směru	směr	k1gInSc2	směr
vychýlí	vychýlit	k5eAaPmIp3nS	vychýlit
a	a	k8xC	a
snáze	snadno	k6eAd2	snadno
porazí	porazit	k5eAaPmIp3nS	porazit
vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
kuželek	kuželka	k1gFnPc2	kuželka
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
obtížnější	obtížný	k2eAgFnSc4d2	obtížnější
kouli	koule	k1gFnSc4	koule
při	při	k7c6	při
hodu	hod	k1gInSc6	hod
vypustit	vypustit	k5eAaPmF	vypustit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
optimálního	optimální	k2eAgNnSc2d1	optimální
vychýlení	vychýlení	k1gNnSc2	vychýlení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
koule	koule	k1gFnPc1	koule
mají	mít	k5eAaImIp3nP	mít
sice	sice	k8xC	sice
striktně	striktně	k6eAd1	striktně
daný	daný	k2eAgInSc4d1	daný
totožný	totožný	k2eAgInSc4d1	totožný
průměr	průměr	k1gInSc4	průměr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svojí	svůj	k3xOyFgFnSc7	svůj
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Kuželková	kuželkový	k2eAgFnSc1d1	Kuželková
koule	koule	k1gFnSc1	koule
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
160	[number]	k4	160
±	±	k?	±
0,5	[number]	k4	0,5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
otvory	otvor	k1gInPc4	otvor
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
2,818	[number]	k4	2,818
až	až	k9	až
2,871	[number]	k4	2,871
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
velikost	velikost	k1gFnSc4	velikost
koulí	koule	k1gFnPc2	koule
i	i	k8xC	i
kuželek	kuželka	k1gFnPc2	kuželka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bowlingu	bowling	k1gInSc6	bowling
mnohem	mnohem	k6eAd1	mnohem
snazší	snadný	k2eAgNnSc1d2	snazší
shodit	shodit	k5eAaPmF	shodit
všechny	všechen	k3xTgFnPc4	všechen
kuželky	kuželka	k1gFnPc4	kuželka
jednou	jeden	k4xCgFnSc7	jeden
ranou	rána	k1gFnSc7	rána
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bowlingová	bowlingový	k2eAgFnSc1d1	bowlingová
dráha	dráha	k1gFnSc1	dráha
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
házet	házet	k5eAaImF	házet
koule	koule	k1gFnSc1	koule
až	až	k9	až
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
koncovou	koncový	k2eAgFnSc4d1	koncová
čáru	čára	k1gFnSc4	čára
odhozu	odhoz	k1gInSc2	odhoz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kuželek	kuželka	k1gFnPc2	kuželka
jsou	být	k5eAaImIp3nP	být
dráhy	dráha	k1gFnPc1	dráha
ze	z	k7c2	z
saduritu	sadurit	k1gInSc2	sadurit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
křehké	křehký	k2eAgFnPc1d1	křehká
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kouli	koule	k1gFnSc4	koule
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
házet	házet	k5eAaImF	házet
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ji	on	k3xPp3gFnSc4	on
pokládat	pokládat	k5eAaImF	pokládat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
vstupu	vstup	k1gInSc2	vstup
z	z	k7c2	z
rozběhového	rozběhový	k2eAgInSc2d1	rozběhový
prostoru	prostor	k1gInSc2	prostor
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
před	před	k7c7	před
čárou	čára	k1gFnSc7	čára
odhozu	odhoz	k1gInSc2	odhoz
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
valila	valit	k5eAaImAgFnS	valit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
činí	činit	k5eAaImIp3nS	činit
kuželky	kuželka	k1gFnPc4	kuželka
fyzicky	fyzicky	k6eAd1	fyzicky
náročnější	náročný	k2eAgFnSc1d2	náročnější
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
hru	hra	k1gFnSc4	hra
namáhavější	namáhavý	k2eAgFnSc4d2	namáhavější
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bowlingu	bowling	k1gInSc6	bowling
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
u	u	k7c2	u
hráčů	hráč	k1gMnPc2	hráč
obvykle	obvykle	k6eAd1	obvykle
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
speciální	speciální	k2eAgFnSc1d1	speciální
obuv	obuv	k1gFnSc1	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podmínka	podmínka	k1gFnSc1	podmínka
u	u	k7c2	u
kuželek	kuželka	k1gFnPc2	kuželka
neplatí	platit	k5eNaImIp3nS	platit
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
sportovní	sportovní	k2eAgFnSc6d1	sportovní
obuvi	obuv	k1gFnSc6	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Kuželky	kuželka	k1gFnPc1	kuželka
se	se	k3xPyFc4	se
od	od	k7c2	od
bowlingu	bowling	k1gInSc2	bowling
liší	lišit	k5eAaImIp3nP	lišit
též	též	k9	též
systémem	systém	k1gInSc7	systém
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kuželkách	kuželka	k1gFnPc6	kuželka
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
hráč	hráč	k1gMnSc1	hráč
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nenahází	naházet	k5eNaPmIp3nS	naházet
stanovený	stanovený	k2eAgInSc4d1	stanovený
počet	počet	k1gInSc4	počet
hodů	hod	k1gInPc2	hod
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
bowlingu	bowling	k1gInSc6	bowling
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
střídají	střídat	k5eAaImIp3nP	střídat
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
nebo	nebo	k8xC	nebo
dvou	dva	k4xCgInPc6	dva
hodech	hod	k1gInPc6	hod
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
má	mít	k5eAaImIp3nS	mít
hra	hra	k1gFnSc1	hra
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgInSc4d2	veliký
spád	spád	k1gInSc4	spád
a	a	k8xC	a
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
hráči	hráč	k1gMnPc1	hráč
mohou	moct	k5eAaImIp3nP	moct
okamžitě	okamžitě	k6eAd1	okamžitě
porovnávat	porovnávat	k5eAaImF	porovnávat
své	svůj	k3xOyFgInPc4	svůj
výsledky	výsledek	k1gInPc4	výsledek
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
také	také	k9	také
počítání	počítání	k1gNnSc3	počítání
shozených	shozený	k2eAgFnPc2d1	shozená
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
kuželkách	kuželka	k1gFnPc6	kuželka
se	se	k3xPyFc4	se
započítávají	započítávat	k5eAaImIp3nP	započítávat
jen	jen	k9	jen
ty	ten	k3xDgFnPc1	ten
kuželky	kuželka	k1gFnPc1	kuželka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
skutečně	skutečně	k6eAd1	skutečně
spadnou	spadnout	k5eAaPmIp3nP	spadnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
bowlingu	bowling	k1gInSc6	bowling
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
navíc	navíc	k6eAd1	navíc
bonusové	bonusový	k2eAgInPc4d1	bonusový
body	bod	k1gInPc4	bod
za	za	k7c4	za
shození	shození	k1gNnSc4	shození
všech	všecek	k3xTgFnPc2	všecek
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Bowling	bowling	k1gInSc1	bowling
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
halách	hala	k1gFnPc6	hala
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
stojí	stát	k5eAaImIp3nS	stát
deset	deset	k4xCc1	deset
kuželek	kuželka	k1gFnPc2	kuželka
postavených	postavený	k2eAgMnPc2d1	postavený
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
rovnostranného	rovnostranný	k2eAgInSc2d1	rovnostranný
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
jeden	jeden	k4xCgInSc1	jeden
vrchol	vrchol	k1gInSc1	vrchol
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
hráči	hráč	k1gMnSc3	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Kuželky	kuželka	k1gFnSc2	kuželka
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
snaží	snažit	k5eAaImIp3nS	snažit
srazit	srazit	k5eAaPmF	srazit
hozením	hození	k1gNnSc7	hození
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nutné	nutný	k2eAgNnSc1d1	nutné
(	(	kIx(	(
<g/>
na	na	k7c6	na
oficiálních	oficiální	k2eAgInPc6d1	oficiální
turnajích	turnaj	k1gInPc6	turnaj
či	či	k8xC	či
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
bowlingových	bowlingový	k2eAgFnPc2d1	bowlingová
heren	herna	k1gFnPc2	herna
<g/>
)	)	kIx)	)
hrát	hrát	k5eAaImF	hrát
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
obuvi	obuv	k1gFnSc6	obuv
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využívat	využívat	k5eAaImF	využívat
i	i	k9	i
další	další	k2eAgNnSc4d1	další
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
doplňky	doplněk	k1gInPc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
obdélníkového	obdélníkový	k2eAgInSc2d1	obdélníkový
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
62	[number]	k4	62
stop	stop	k2eAgInPc2d1	stop
10	[number]	k4	10
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
palců	palec	k1gInPc2	palec
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
19	[number]	k4	19
156	[number]	k4	156
mm	mm	kA	mm
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
měřeno	měřit	k5eAaImNgNnS	měřit
od	od	k7c2	od
čáry	čára	k1gFnSc2	čára
přešlapu	přešlap	k1gInSc2	přešlap
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
dráhy	dráha	k1gFnSc2	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
čárou	čára	k1gFnSc7	čára
přešlapu	přešlap	k1gInSc2	přešlap
a	a	k8xC	a
středem	středem	k7c2	středem
první	první	k4xOgFnSc2	první
kuželky	kuželka	k1gFnSc2	kuželka
(	(	kIx(	(
<g/>
kuželky	kuželka	k1gFnPc1	kuželka
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
60	[number]	k4	60
stop	stopa	k1gFnPc2	stopa
±	±	k?	±
<g/>
1⁄	1⁄	k?	1⁄
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
18	[number]	k4	18
288	[number]	k4	288
±	±	k?	±
<g/>
13	[number]	k4	13
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
41	[number]	k4	41
1⁄	1⁄	k?	1⁄
palců	palec	k1gInPc2	palec
±	±	k?	±
<g/>
1⁄	1⁄	k?	1⁄
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
1	[number]	k4	1
054	[number]	k4	054
±	±	k?	±
<g/>
12,7	[number]	k4	12,7
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
na	na	k7c4	na
0,040	[number]	k4	0,040
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
1	[number]	k4	1
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
nebo	nebo	k8xC	nebo
umělé	umělý	k2eAgFnSc2d1	umělá
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
39	[number]	k4	39
až	až	k9	až
42	[number]	k4	42
tenkých	tenký	k2eAgInPc2d1	tenký
podélně	podélně	k6eAd1	podélně
položených	položený	k2eAgInPc2d1	položený
pásů	pás	k1gInPc2	pás
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
rozpoznání	rozpoznání	k1gNnSc4	rozpoznání
střídavě	střídavě	k6eAd1	střídavě
světle	světle	k6eAd1	světle
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
zbarveny	zbarven	k2eAgFnPc1d1	zbarvena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
kratší	krátký	k2eAgFnSc6d2	kratší
straně	strana	k1gFnSc6	strana
dráha	dráha	k1gFnSc1	dráha
plynule	plynule	k6eAd1	plynule
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
prostor	prostor	k1gInSc4	prostor
rozběžiště	rozběžiště	k1gNnSc2	rozběžiště
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
nejméně	málo	k6eAd3	málo
4,57	[number]	k4	4,57
m	m	kA	m
a	a	k8xC	a
od	od	k7c2	od
kterého	který	k3yRgNnSc2	který
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
čárou	čára	k1gFnSc7	čára
přešlapu	přešlap	k1gInSc2	přešlap
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Foulline	Foullin	k1gInSc5	Foullin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
konci	konec	k1gInSc6	konec
jsou	být	k5eAaImIp3nP	být
sestaveny	sestaven	k2eAgFnPc1d1	sestavena
kuželky	kuželka	k1gFnPc1	kuželka
do	do	k7c2	do
předepsaného	předepsaný	k2eAgInSc2d1	předepsaný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
delších	dlouhý	k2eAgFnPc6d2	delší
stranách	strana	k1gFnPc6	strana
jsou	být	k5eAaImIp3nP	být
žlábky	žlábek	k1gInPc1	žlábek
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
gutters	gutters	k6eAd1	gutters
<g/>
)	)	kIx)	)
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
9,25	[number]	k4	9,25
palce	palec	k1gInSc2	palec
±	±	k?	±
<g/>
1⁄	1⁄	k?	1⁄
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
přibližně	přibližně	k6eAd1	přibližně
23,5	[number]	k4	23,5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
a	a	k8xC	a
hloubce	hloubka	k1gFnSc6	hloubka
1,875	[number]	k4	1,875
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
asi	asi	k9	asi
4,8	[number]	k4	4,8
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
koule	koule	k1gFnSc1	koule
spadne	spadnout	k5eAaPmIp3nS	spadnout
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
neudrží	udržet	k5eNaPmIp3nS	udržet
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Žlábkem	žlábek	k1gInSc7	žlábek
je	být	k5eAaImIp3nS	být
koule	koule	k1gFnSc1	koule
–	–	k?	–
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
některé	některý	k3yIgFnPc4	některý
kuželky	kuželka	k1gFnPc4	kuželka
srazila	srazit	k5eAaPmAgFnS	srazit
–	–	k?	–
dopravena	dopraven	k2eAgFnSc1d1	dopravena
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
za	za	k7c7	za
kuželkami	kuželka	k1gFnPc7	kuželka
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ji	on	k3xPp3gFnSc4	on
stroj	stroj	k1gInSc1	stroj
vrátí	vrátit	k5eAaPmIp3nS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
podavače	podavač	k1gInSc2	podavač
koulí	koulet	k5eAaImIp3nS	koulet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
hod	hod	k1gInSc4	hod
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
podél	podél	k7c2	podél
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
těsně	těsně	k6eAd1	těsně
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
hranici	hranice	k1gFnSc4	hranice
se	s	k7c7	s
žlábky	žlábek	k1gInPc7	žlábek
<g/>
)	)	kIx)	)
nechat	nechat	k5eAaPmF	nechat
zvednout	zvednout	k5eAaPmF	zvednout
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
bumpery	bumpera	k1gFnPc4	bumpera
podobající	podobající	k2eAgFnPc4d1	podobající
se	se	k3xPyFc4	se
malým	malý	k2eAgInPc3d1	malý
mantinelům	mantinel	k1gInPc3	mantinel
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
kouli	koule	k1gFnSc3	koule
zabrání	zabránit	k5eAaPmIp3nS	zabránit
v	v	k7c6	v
pádu	pád	k1gInSc6	pád
do	do	k7c2	do
žlábku	žlábek	k1gInSc2	žlábek
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
možnosti	možnost	k1gFnSc3	možnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
-li	i	k?	-li
i	i	k8xC	i
malé	malý	k2eAgFnPc1d1	malá
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
s	s	k7c7	s
udržením	udržení	k1gNnSc7	udržení
koule	koule	k1gFnSc2	koule
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
mívají	mívat	k5eAaImIp3nP	mívat
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Bumpery	Bumper	k1gMnPc4	Bumper
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
oficiálních	oficiální	k2eAgNnPc6d1	oficiální
soutěžních	soutěžní	k2eAgNnPc6d1	soutěžní
utkáních	utkání	k1gNnPc6	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
lakovaného	lakovaný	k2eAgNnSc2d1	lakované
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prvních	první	k4xOgFnPc2	první
15	[number]	k4	15
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
cca	cca	kA	cca
4,57	[number]	k4	4,57
m	m	kA	m
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
javorového	javorový	k2eAgNnSc2d1	javorové
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
schopnosti	schopnost	k1gFnSc3	schopnost
odolat	odolat	k5eAaPmF	odolat
nárazu	náraz	k1gInSc2	náraz
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
<g/>
,	,	kIx,	,
16	[number]	k4	16
librové	librový	k2eAgFnPc1d1	Librová
koule	koule	k1gFnPc1	koule
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
borovice	borovice	k1gFnSc2	borovice
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
ošetřena	ošetřit	k5eAaPmNgFnS	ošetřit
olejem	olej	k1gInSc7	olej
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snazší	snadný	k2eAgNnPc4d2	snazší
míření	míření	k1gNnPc4	míření
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
orientační	orientační	k2eAgInPc4d1	orientační
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
osm	osm	k4xCc4	osm
teček	tečka	k1gFnPc2	tečka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
dots	dots	k6eAd1	dots
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
12	[number]	k4	12
stop	stopa	k1gFnPc2	stopa
od	od	k7c2	od
čáry	čára	k1gFnSc2	čára
přešlapu	přešlap	k1gInSc2	přešlap
je	být	k5eAaImIp3nS	být
sedm	sedm	k4xCc1	sedm
šipek	šipka	k1gFnPc2	šipka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
arrows	arrows	k6eAd1	arrows
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
čtyři	čtyři	k4xCgFnPc1	čtyři
řady	řada	k1gFnPc1	řada
teček	tečka	k1gFnPc2	tečka
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
i	i	k9	i
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
rozběžiště	rozběžiště	k1gNnSc2	rozběžiště
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
tečky	tečka	k1gFnPc1	tečka
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
šipky	šipka	k1gFnPc1	šipka
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
symetricky	symetricky	k6eAd1	symetricky
podle	podle	k7c2	podle
podélné	podélný	k2eAgFnSc2d1	podélná
osy	osa	k1gFnSc2	osa
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Ošetřování	ošetřování	k1gNnSc1	ošetřování
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
nanášením	nanášení	k1gNnSc7	nanášení
vrstvy	vrstva	k1gFnSc2	vrstva
minerálního	minerální	k2eAgInSc2d1	minerální
oleje	olej	k1gInSc2	olej
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
jejímu	její	k3xOp3gNnSc3	její
poškození	poškození	k1gNnSc3	poškození
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dráha	dráha	k1gFnSc1	dráha
ochránila	ochránit	k5eAaPmAgFnS	ochránit
před	před	k7c7	před
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Nanášení	nanášení	k1gNnSc4	nanášení
provádí	provádět	k5eAaImIp3nS	provádět
pracovník	pracovník	k1gMnSc1	pracovník
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
herny	herna	k1gFnSc2	herna
pomocí	pomocí	k7c2	pomocí
speciálního	speciální	k2eAgInSc2d1	speciální
mazacího	mazací	k2eAgInSc2d1	mazací
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nejprve	nejprve	k6eAd1	nejprve
setře	setřít	k5eAaPmIp3nS	setřít
olej	olej	k1gInSc1	olej
stávající	stávající	k2eAgInSc1d1	stávající
a	a	k8xC	a
pak	pak	k6eAd1	pak
nanese	nanést	k5eAaPmIp3nS	nanést
vrstvu	vrstva	k1gFnSc4	vrstva
novou	nový	k2eAgFnSc4d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Přemazávání	přemazávání	k1gNnSc1	přemazávání
se	se	k3xPyFc4	se
na	na	k7c6	na
významných	významný	k2eAgInPc6d1	významný
turnajích	turnaj	k1gInPc6	turnaj
provádí	provádět	k5eAaImIp3nS	provádět
buď	buď	k8xC	buď
po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
24	[number]	k4	24
her	hra	k1gFnPc2	hra
nebo	nebo	k8xC	nebo
po	po	k7c6	po
3	[number]	k4	3
hodinách	hodina	k1gFnPc6	hodina
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
nenanáší	nanášet	k5eNaImIp3nS	nanášet
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
dráze	dráha	k1gFnSc6	dráha
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
olej	olej	k1gInSc4	olej
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
rozprostřen	rozprostřen	k2eAgInSc1d1	rozprostřen
<g/>
,	,	kIx,	,
určuje	určovat	k5eAaImIp3nS	určovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
mazací	mazací	k2eAgInSc1d1	mazací
model	model	k1gInSc1	model
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
namazané	namazaný	k2eAgFnSc2d1	namazaná
plochy	plocha	k1gFnSc2	plocha
je	být	k5eAaImIp3nS	být
39	[number]	k4	39
stop	stopa	k1gFnPc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
do	do	k7c2	do
35	[number]	k4	35
stop	stopa	k1gFnPc2	stopa
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
krátké	krátký	k2eAgNnSc4d1	krátké
mazání	mazání	k1gNnSc4	mazání
a	a	k8xC	a
do	do	k7c2	do
45	[number]	k4	45
stop	stopa	k1gFnPc2	stopa
je	být	k5eAaImIp3nS	být
mazání	mazání	k1gNnSc1	mazání
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
herna	herna	k1gFnSc1	herna
má	mít	k5eAaImIp3nS	mít
též	též	k9	též
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
mazací	mazací	k2eAgInSc4d1	mazací
model	model	k1gInSc4	model
označovaný	označovaný	k2eAgInSc4d1	označovaný
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
house	house	k1gNnSc1	house
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
domácí	domácí	k1gMnPc1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Mazání	mazání	k1gNnSc1	mazání
dráhy	dráha	k1gFnSc2	dráha
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
rovinné	rovinný	k2eAgNnSc4d1	rovinné
hraní	hraní	k1gNnSc4	hraní
bowlingu	bowling	k1gInSc2	bowling
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
reaktivního	reaktivní	k2eAgNnSc2d1	reaktivní
hraní	hraní	k1gNnSc2	hraní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hozená	hozený	k2eAgFnSc1d1	hozená
koule	koule	k1gFnSc1	koule
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
porážením	porážení	k1gNnSc7	porážení
kuželek	kuželka	k1gFnPc2	kuželka
vytočí	vytočit	k5eAaPmIp3nS	vytočit
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
způsob	způsob	k1gInSc1	způsob
mazání	mazání	k1gNnSc6	mazání
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
projevuje	projevovat	k5eAaImIp3nS	projevovat
nejen	nejen	k6eAd1	nejen
délka	délka	k1gFnSc1	délka
namazaného	namazaný	k2eAgInSc2d1	namazaný
úseku	úsek	k1gInSc2	úsek
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
tloušťka	tloušťka	k1gFnSc1	tloušťka
vrstvy	vrstva	k1gFnSc2	vrstva
namazaného	namazaný	k2eAgInSc2d1	namazaný
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
oleje	olej	k1gInSc2	olej
příliš	příliš	k6eAd1	příliš
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dráha	dráha	k1gFnSc1	dráha
moc	moc	k6eAd1	moc
rychlá	rychlý	k2eAgFnSc1d1	rychlá
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Fast	Fast	k2eAgInSc1d1	Fast
Lane	Lane	k1gInSc1	Lane
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
naneseno	nanést	k5eAaPmNgNnS	nanést
oleje	olej	k1gInSc2	olej
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dráha	dráha	k1gFnSc1	dráha
suchá	suchý	k2eAgFnSc1d1	suchá
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Dry	Dry	k1gFnSc1	Dry
lane	lan	k1gFnSc2	lan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
i	i	k9	i
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
hraní	hraní	k1gNnSc2	hraní
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
krátkém	krátký	k2eAgNnSc6d1	krátké
mazání	mazání	k1gNnSc6	mazání
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
spíš	spíš	k9	spíš
u	u	k7c2	u
žlábku	žlábek	k1gInSc2	žlábek
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
mazání	mazání	k1gNnSc2	mazání
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
především	především	k6eAd1	především
středem	střed	k1gInSc7	střed
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koule	koule	k1gFnSc1	koule
skoro	skoro	k6eAd1	skoro
nezahýbá	zahýbat	k5eNaImIp3nS	zahýbat
a	a	k8xC	a
do	do	k7c2	do
kuželek	kuželka	k1gFnPc2	kuželka
se	se	k3xPyFc4	se
stočí	stočit	k5eAaPmIp3nS	stočit
až	až	k9	až
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
koule	koule	k1gFnSc1	koule
roztočená	roztočený	k2eAgFnSc1d1	roztočená
na	na	k7c6	na
oleji	olej	k1gInSc6	olej
klouže	klouzat	k5eAaImIp3nS	klouzat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
suché	suchý	k2eAgFnSc6d1	suchá
dráze	dráha	k1gFnSc6	dráha
zabírá	zabírat	k5eAaImIp3nS	zabírat
<g/>
,	,	kIx,	,
přilne	přilnout	k5eAaPmIp3nS	přilnout
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
povrchu	povrch	k1gInSc3	povrch
a	a	k8xC	a
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
začne	začít	k5eAaPmIp3nS	začít
zahýbat	zahýbat	k5eAaPmF	zahýbat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hernách	herna	k1gFnPc6	herna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
chladněji	chladně	k6eAd2	chladně
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
olej	olej	k1gInSc4	olej
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tepleji	teple	k6eAd2	teple
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
olej	olej	k1gInSc1	olej
vytahává	vytahávat	k5eAaImIp3nS	vytahávat
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vydírá	vydírat	k5eAaImIp3nS	vydírat
z	z	k7c2	z
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
že	že	k8xS	že
koule	koule	k1gFnSc1	koule
klouže	klouzat	k5eAaImIp3nS	klouzat
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vydřené	vydřený	k2eAgFnSc6d1	vydřená
dráze	dráha	k1gFnSc6	dráha
naopak	naopak	k6eAd1	naopak
koule	koule	k1gFnSc2	koule
začíná	začínat	k5eAaImIp3nS	začínat
zahýbat	zahýbat	k5eAaPmF	zahýbat
dřív	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
měnění	měnění	k1gNnSc2	měnění
vlastností	vlastnost	k1gFnPc2	vlastnost
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
delších	dlouhý	k2eAgInPc2d2	delší
zápasů	zápas	k1gInPc2	zápas
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
soutěžích	soutěž	k1gFnPc6	soutěž
dráhy	dráha	k1gFnSc2	dráha
pravidelně	pravidelně	k6eAd1	pravidelně
čištěny	čištěn	k2eAgInPc1d1	čištěn
a	a	k8xC	a
olejovány	olejován	k2eAgInPc1d1	olejován
nejpozději	pozdě	k6eAd3	pozdě
po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
24	[number]	k4	24
her	hra	k1gFnPc2	hra
nebo	nebo	k8xC	nebo
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
hodinách	hodina	k1gFnPc6	hodina
od	od	k7c2	od
předchozího	předchozí	k2eAgNnSc2d1	předchozí
mazání	mazání	k1gNnSc2	mazání
<g/>
.	.	kIx.	.
</s>
<s>
Setření	setření	k1gNnSc1	setření
stávající	stávající	k2eAgFnSc2d1	stávající
vrstvy	vrstva	k1gFnSc2	vrstva
oleje	olej	k1gInSc2	olej
či	či	k8xC	či
nanesení	nanesení	k1gNnSc2	nanesení
vrstvy	vrstva	k1gFnSc2	vrstva
nové	nový	k2eAgFnSc2d1	nová
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Mazací	mazací	k2eAgInSc1d1	mazací
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
čáry	čára	k1gFnSc2	čára
přešlapu	přešlapat	k5eAaPmIp1nS	přešlapat
<g/>
)	)	kIx)	)
umístěn	umístit	k5eAaPmNgMnS	umístit
obsluhujícím	obsluhující	k2eAgMnSc7d1	obsluhující
pracovníkem	pracovník	k1gMnSc7	pracovník
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
se	se	k3xPyFc4	se
již	již	k6eAd1	již
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
samočinně	samočinně	k6eAd1	samočinně
pomocí	pomocí	k7c2	pomocí
elektrických	elektrický	k2eAgInPc2d1	elektrický
motorků	motorek	k1gInPc2	motorek
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
mazání	mazání	k1gNnSc2	mazání
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
mazací	mazací	k2eAgInSc1d1	mazací
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
stroj	stroj	k1gInSc1	stroj
naprogramován	naprogramován	k2eAgInSc1d1	naprogramován
a	a	k8xC	a
počítač	počítač	k1gInSc1	počítač
jej	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
na	na	k7c4	na
příslušná	příslušný	k2eAgNnPc4d1	příslušné
místa	místo	k1gNnPc4	místo
dráhy	dráha	k1gFnSc2	dráha
umístí	umístit	k5eAaPmIp3nS	umístit
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
bowlingu	bowling	k1gInSc2	bowling
je	být	k5eAaImIp3nS	být
třeba	třeba	k9	třeba
deseti	deset	k4xCc2	deset
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
hry	hra	k1gFnSc2	hra
ustaveny	ustavit	k5eAaPmNgFnP	ustavit
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
do	do	k7c2	do
předepsaného	předepsaný	k2eAgInSc2d1	předepsaný
trojúhelníkového	trojúhelníkový	k2eAgInSc2d1	trojúhelníkový
obrazce	obrazec	k1gInSc2	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
pozice	pozice	k1gFnPc1	pozice
kuželek	kuželka	k1gFnPc2	kuželka
v	v	k7c6	v
bowlingu	bowling	k1gInSc6	bowling
nemají	mít	k5eNaImIp3nP	mít
–	–	k?	–
oproti	oproti	k7c3	oproti
hře	hra	k1gFnSc3	hra
kuželky	kuželka	k1gFnSc2	kuželka
–	–	k?	–
svá	svůj	k3xOyFgNnPc4	svůj
označení	označení	k1gNnSc2	označení
jmény	jméno	k1gNnPc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
čísly	čísnout	k5eAaPmAgFnP	čísnout
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
10	[number]	k4	10
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kuželka	kuželka	k1gFnSc1	kuželka
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
stojí	stát	k5eAaImIp3nS	stát
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
hráči	hráč	k1gMnSc3	hráč
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
řadách	řada	k1gFnPc6	řada
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
hráče	hráč	k1gMnSc2	hráč
kuželky	kuželka	k1gFnSc2	kuželka
číslují	číslovat	k5eAaImIp3nP	číslovat
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kuželka	kuželka	k1gFnSc1	kuželka
číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
řadě	řada	k1gFnSc6	řada
úplně	úplně	k6eAd1	úplně
vlevo	vlevo	k6eAd1	vlevo
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
zadní	zadní	k2eAgFnSc2d1	zadní
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
pozice	pozice	k1gFnSc1	pozice
číslo	číslo	k1gNnSc1	číslo
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
kuželek	kuželka	k1gFnPc2	kuželka
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
15	[number]	k4	15
palců	palec	k1gInPc2	palec
±	±	k?	±
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
32	[number]	k4	32
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
380	[number]	k4	380
až	až	k9	až
382	[number]	k4	382
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
4,5	[number]	k4	4,5
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
114	[number]	k4	114
mm	mm	kA	mm
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
základnou	základna	k1gFnSc7	základna
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
4,755	[number]	k4	4,755
až	až	k9	až
4,797	[number]	k4	4,797
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
od	od	k7c2	od
121	[number]	k4	121
do	do	k7c2	do
122	[number]	k4	122
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
část	část	k1gFnSc1	část
kuželky	kuželka	k1gFnSc2	kuželka
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
jednotně	jednotně	k6eAd1	jednotně
zakulacena	zakulatit	k5eAaPmNgFnS	zakulatit
obloukem	oblouk	k1gInSc7	oblouk
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
1,273	[number]	k4	1,273
palce	palec	k1gInSc2	palec
±	±	k?	±
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
32	[number]	k4	32
palce	palec	k1gInPc4	palec
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
31,5	[number]	k4	31,5
až	až	k9	až
33	[number]	k4	33
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
kuželky	kuželka	k1gFnSc2	kuželka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
3	[number]	k4	3
liber	libra	k1gFnPc2	libra
a	a	k8xC	a
6	[number]	k4	6
uncí	unce	k1gFnPc2	unce
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
1531	[number]	k4	1531
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
do	do	k7c2	do
3	[number]	k4	3
liber	libra	k1gFnPc2	libra
a	a	k8xC	a
10	[number]	k4	10
uncí	unce	k1gFnPc2	unce
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
1645	[number]	k4	1645
gramů	gram	k1gInPc2	gram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
kuželky	kuželka	k1gFnSc2	kuželka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
14,29	[number]	k4	14,29
až	až	k9	až
15,24	[number]	k4	15,24
cm	cm	kA	cm
nad	nad	k7c7	nad
její	její	k3xOp3gFnSc7	její
základnou	základna	k1gFnSc7	základna
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
umožnit	umožnit	k5eAaPmF	umožnit
její	její	k3xOp3gInSc4	její
pád	pád	k1gInSc4	pád
již	již	k6eAd1	již
při	při	k7c6	při
náklonu	náklon	k1gInSc6	náklon
o	o	k7c4	o
8	[number]	k4	8
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
javoru	javor	k1gInSc2	javor
nebo	nebo	k8xC	nebo
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
jsou	být	k5eAaImIp3nP	být
potažené	potažený	k2eAgNnSc1d1	potažené
javorovým	javorový	k2eAgNnSc7d1	javorové
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Kuželky	kuželka	k1gFnSc2	kuželka
se	se	k3xPyFc4	se
sráží	srážet	k5eAaImIp3nS	srážet
hozením	hození	k1gNnSc7	hození
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
plastové	plastový	k2eAgFnSc2d1	plastová
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
existují	existovat	k5eAaImIp3nP	existovat
různě	různě	k6eAd1	různě
těžké	těžký	k2eAgFnPc1d1	těžká
koule	koule	k1gFnPc1	koule
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
povolená	povolený	k2eAgFnSc1d1	povolená
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
7,25	[number]	k4	7,25
kg	kg	kA	kg
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
není	být	k5eNaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
koule	koule	k1gFnSc2	koule
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
pohybovat	pohybovat	k5eAaImF	pohybovat
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
od	od	k7c2	od
26,704	[number]	k4	26,704
palce	palec	k1gInSc2	palec
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
678,3	[number]	k4	678,3
mm	mm	kA	mm
<g/>
)	)	kIx)	)
do	do	k7c2	do
27,002	[number]	k4	27,002
palce	palec	k1gInSc2	palec
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
685,8	[number]	k4	685,8
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
průměry	průměr	k1gInPc1	průměr
koulí	koule	k1gFnPc2	koule
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
od	od	k7c2	od
8,500	[number]	k4	8,500
palce	palec	k1gInSc2	palec
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
jest	být	k5eAaImIp3nS	být
215,9	[number]	k4	215,9
mm	mm	kA	mm
<g/>
)	)	kIx)	)
do	do	k7c2	do
8,595	[number]	k4	8,595
palců	palec	k1gInPc2	palec
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
218,3	[number]	k4	218,3
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
uchopení	uchopení	k1gNnSc4	uchopení
mají	mít	k5eAaImIp3nP	mít
koule	koule	k1gFnSc1	koule
tři	tři	k4xCgInPc4	tři
mělké	mělký	k2eAgInPc4d1	mělký
otvory	otvor	k1gInPc4	otvor
na	na	k7c4	na
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
bowlingové	bowlingový	k2eAgFnPc1d1	bowlingová
koule	koule	k1gFnPc1	koule
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
koule	koule	k1gFnPc1	koule
gumové	gumový	k2eAgFnPc1d1	gumová
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
z	z	k7c2	z
čisté	čistý	k2eAgFnSc2d1	čistá
gumy	guma	k1gFnSc2	guma
<g/>
,	,	kIx,	,
když	když	k8xS	když
však	však	k9	však
ale	ale	k9	ale
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
čistá	čistý	k2eAgFnSc1d1	čistá
guma	guma	k1gFnSc1	guma
strategickou	strategický	k2eAgFnSc4d1	strategická
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
zbrojařský	zbrojařský	k2eAgInSc4d1	zbrojařský
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
pro	pro	k7c4	pro
bowlingové	bowlingový	k2eAgFnPc4d1	bowlingová
koule	koule	k1gFnPc4	koule
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
guma	guma	k1gFnSc1	guma
syntetická	syntetický	k2eAgFnSc1d1	syntetická
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
největší	veliký	k2eAgMnPc4d3	veliký
výrobce	výrobce	k1gMnPc4	výrobce
tehdy	tehdy	k6eAd1	tehdy
patřily	patřit	k5eAaImAgFnP	patřit
firmy	firma	k1gFnPc1	firma
Brunswick	Brunswick	k1gInSc1	Brunswick
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
Ace	Ace	k1gFnSc1	Ace
a	a	k8xC	a
Ebonite	ebonit	k1gInSc5	ebonit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
polyesterových	polyesterový	k2eAgFnPc2d1	polyesterová
koulí	koule	k1gFnPc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
držení	držení	k1gNnSc1	držení
směru	směr	k1gInSc2	směr
po	po	k7c6	po
odhozu	odhoz	k1gInSc6	odhoz
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
jejich	jejich	k3xOp3gNnSc2	jejich
vyvedení	vyvedení	k1gNnSc2	vyvedení
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
barevných	barevný	k2eAgFnPc6d1	barevná
kombinacích	kombinace	k1gFnPc6	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
polyesterové	polyesterový	k2eAgFnPc1d1	polyesterová
koule	koule	k1gFnPc1	koule
zcela	zcela	k6eAd1	zcela
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
koule	koule	k1gFnPc1	koule
gumové	gumový	k2eAgFnPc1d1	gumová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgNnP	být
firmou	firma	k1gFnSc7	firma
AMF	AMF	kA	AMF
představena	představen	k2eAgFnSc1d1	představena
koule	koule	k1gFnSc1	koule
polyuretanová	polyuretanový	k2eAgFnSc1d1	polyuretanová
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
cenu	cena	k1gFnSc4	cena
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
mezi	mezi	k7c4	mezi
hráče	hráč	k1gMnPc4	hráč
bowlingu	bowling	k1gInSc2	bowling
a	a	k8xC	a
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
předchozí	předchozí	k2eAgInPc4d1	předchozí
typy	typ	k1gInPc4	typ
koulí	koulet	k5eAaImIp3nP	koulet
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
poslední	poslední	k2eAgFnSc2d1	poslední
dekády	dekáda	k1gFnSc2	dekáda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
koule	koule	k1gFnPc1	koule
z	z	k7c2	z
reaktivního	reaktivní	k2eAgInSc2d1	reaktivní
polyuretanu	polyuretan	k1gInSc2	polyuretan
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
koule	koule	k1gFnPc1	koule
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgNnSc4d2	veliký
tření	tření	k1gNnSc4	tření
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
zajedou	zajet	k5eAaPmIp3nP	zajet
na	na	k7c4	na
hráčem	hráč	k1gMnSc7	hráč
zamýšlené	zamýšlený	k2eAgNnSc1d1	zamýšlené
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přišla	přijít	k5eAaPmAgFnS	přijít
firma	firma	k1gFnSc1	firma
Brunswick	Brunswicko	k1gNnPc2	Brunswicko
s	s	k7c7	s
koulemi	koule	k1gFnPc7	koule
z	z	k7c2	z
proaktivního	proaktivní	k2eAgInSc2d1	proaktivní
polyuretanu	polyuretan	k1gInSc2	polyuretan
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
vyrobeny	vyrobit	k5eAaPmNgFnP	vyrobit
z	z	k7c2	z
polyuretanové	polyuretanový	k2eAgFnSc2d1	polyuretanová
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
obsahující	obsahující	k2eAgFnSc2d1	obsahující
malé	malý	k2eAgFnSc2d1	malá
krupičky	krupička	k1gFnSc2	krupička
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
koule	koule	k1gFnPc1	koule
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
hráčům	hráč	k1gMnPc3	hráč
snáze	snadno	k6eAd2	snadno
dosahovat	dosahovat	k5eAaImF	dosahovat
strike	strike	k6eAd1	strike
(	(	kIx(	(
<g/>
porazit	porazit	k5eAaPmF	porazit
všechny	všechen	k3xTgFnPc4	všechen
kuželky	kuželka	k1gFnPc4	kuželka
jedním	jeden	k4xCgInSc7	jeden
hodem	hod	k1gInSc7	hod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
koule	koule	k1gFnPc1	koule
pórovitější	pórovitý	k2eAgFnPc1d2	pórovitější
<g/>
,	,	kIx,	,
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
více	hodně	k6eAd2	hodně
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
profesionální	profesionální	k2eAgFnPc1d1	profesionální
dráhy	dráha	k1gFnPc1	dráha
mažou	mazat	k5eAaImIp3nP	mazat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dráhy	dráha	k1gFnPc4	dráha
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
častěji	často	k6eAd2	často
mazat	mazat	k5eAaImF	mazat
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
koule	koule	k1gFnSc2	koule
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
její	její	k3xOp3gFnSc4	její
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
umístěním	umístění	k1gNnSc7	umístění
také	také	k9	také
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
koule	koule	k1gFnSc1	koule
dokáže	dokázat	k5eAaPmIp3nS	dokázat
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
zatočit	zatočit	k5eAaPmF	zatočit
<g/>
.	.	kIx.	.
</s>
<s>
Koule	koule	k1gFnSc1	koule
pro	pro	k7c4	pro
rovinné	rovinný	k2eAgNnSc4d1	rovinné
hraní	hraní	k1gNnSc4	hraní
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
běží	běžet	k5eAaImIp3nS	běžet
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jádro	jádro	k1gNnSc1	jádro
obyčejné	obyčejný	k2eAgNnSc1d1	obyčejné
<g/>
,	,	kIx,	,
kulaté	kulatý	k2eAgInPc1d1	kulatý
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
koulích	koule	k1gFnPc6	koule
pro	pro	k7c4	pro
reaktivní	reaktivní	k2eAgNnSc4d1	reaktivní
hraní	hraní	k1gNnSc4	hraní
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
speciální	speciální	k2eAgInPc1d1	speciální
bloky	blok	k1gInPc1	blok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
posunou	posunout	k5eAaPmIp3nP	posunout
těžiště	těžiště	k1gNnSc4	těžiště
koule	koule	k1gFnSc2	koule
mimo	mimo	k7c4	mimo
její	její	k3xOp3gInSc4	její
střed	střed	k1gInSc4	střed
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
umístění	umístění	k1gNnSc1	umístění
jádra	jádro	k1gNnSc2	jádro
v	v	k7c6	v
reaktivních	reaktivní	k2eAgFnPc6d1	reaktivní
koulích	koule	k1gFnPc6	koule
není	být	k5eNaImIp3nS	být
jednotné	jednotný	k2eAgNnSc1d1	jednotné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
"	"	kIx"	"
udělanou	udělaný	k2eAgFnSc4d1	udělaná
kouli	koule	k1gFnSc4	koule
<g/>
.	.	kIx.	.
</s>
<s>
Koule	koule	k1gFnSc1	koule
pro	pro	k7c4	pro
rovinný	rovinný	k2eAgInSc4d1	rovinný
bowling	bowling	k1gInSc4	bowling
či	či	k8xC	či
dohazování	dohazování	k1gNnSc4	dohazování
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
hernách	herna	k1gFnPc6	herna
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
různě	různě	k6eAd1	různě
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgFnSc2	každý
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
hernách	herna	k1gFnPc6	herna
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
koule	koule	k1gFnSc2	koule
v	v	k7c6	v
hmotnostech	hmotnost	k1gFnPc6	hmotnost
6	[number]	k4	6
až	až	k6eAd1	až
16	[number]	k4	16
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
hmotnost	hmotnost	k1gFnSc1	hmotnost
koule	koule	k1gFnSc2	koule
je	být	k5eAaImIp3nS	být
výrazným	výrazný	k2eAgNnSc7d1	výrazné
číslem	číslo	k1gNnSc7	číslo
uvedena	uvést	k5eAaPmNgFnS	uvést
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přiřazena	přiřazen	k2eAgFnSc1d1	přiřazena
určitá	určitý	k2eAgFnSc1d1	určitá
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
barva	barva	k1gFnSc1	barva
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
hernách	herna	k1gFnPc6	herna
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
správné	správný	k2eAgFnSc2d1	správná
koule	koule	k1gFnSc2	koule
neexistuje	existovat	k5eNaImIp3nS	existovat
jediné	jediný	k2eAgNnSc1d1	jediné
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
jich	on	k3xPp3gMnPc2	on
více	hodně	k6eAd2	hodně
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
je	on	k3xPp3gInPc4	on
zkombinovat	zkombinovat	k5eAaPmF	zkombinovat
<g/>
.	.	kIx.	.
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1	tělesná
hmotnost	hmotnost	k1gFnSc1	hmotnost
hráče	hráč	k1gMnSc2	hráč
se	se	k3xPyFc4	se
převede	převést	k5eAaPmIp3nS	převést
na	na	k7c4	na
libry	libra	k1gFnPc4	libra
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
libra	libra	k1gFnSc1	libra
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
0,454	[number]	k4	0,454
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
hodnoty	hodnota	k1gFnSc2	hodnota
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
její	její	k3xOp3gNnSc1	její
jedna	jeden	k4xCgFnSc1	jeden
desetina	desetina	k1gFnSc1	desetina
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
určovat	určovat	k5eAaImF	určovat
váhu	váha	k1gFnSc4	váha
vhodné	vhodný	k2eAgFnSc2d1	vhodná
koule	koule	k1gFnSc2	koule
v	v	k7c6	v
librách	libra	k1gFnPc6	libra
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
věk	věk	k1gInSc1	věk
udává	udávat	k5eAaImIp3nS	udávat
přímo	přímo	k6eAd1	přímo
librovou	librový	k2eAgFnSc4d1	Librová
hmotnost	hmotnost	k1gFnSc4	hmotnost
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
vhodné	vhodný	k2eAgFnSc2d1	vhodná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
koule	koule	k1gFnSc2	koule
lze	lze	k6eAd1	lze
také	také	k9	také
udělat	udělat	k5eAaPmF	udělat
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hráč	hráč	k1gMnSc1	hráč
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
před	před	k7c4	před
sebe	sebe	k3xPyFc4	sebe
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
do	do	k7c2	do
dlaně	dlaň	k1gFnSc2	dlaň
si	se	k3xPyFc3	se
položí	položit	k5eAaPmIp3nS	položit
kouli	koule	k1gFnSc4	koule
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nesmí	smět	k5eNaImIp3nS	smět
ruku	ruka	k1gFnSc4	ruka
stahovat	stahovat	k5eAaImF	stahovat
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
ani	ani	k8xC	ani
lehká	lehký	k2eAgNnPc4d1	lehké
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pořizování	pořizování	k1gNnSc6	pořizování
nové	nový	k2eAgFnSc2d1	nová
koule	koule	k1gFnSc2	koule
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
pořídit	pořídit	k5eAaPmF	pořídit
koule	koule	k1gFnSc2	koule
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hráč	hráč	k1gMnSc1	hráč
nemusel	muset	k5eNaImAgMnS	muset
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
hodu	hod	k1gInSc6	hod
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
koule	koule	k1gFnSc1	koule
vrátí	vrátit	k5eAaPmIp3nS	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	k9	aby
obě	dva	k4xCgFnPc1	dva
koule	koule	k1gFnPc1	koule
měly	mít	k5eAaImAgFnP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Bowlingová	Bowlingový	k2eAgFnSc1d1	Bowlingová
obuv	obuv	k1gFnSc1	obuv
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
kožená	kožený	k2eAgFnSc1d1	kožená
podrážka	podrážka	k1gFnSc1	podrážka
je	být	k5eAaImIp3nS	být
vpředu	vpředu	k7c2	vpředu
–	–	k?	–
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
špičky	špička	k1gFnSc2	špička
–	–	k?	–
kluzká	kluzký	k2eAgFnSc1d1	kluzká
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
patě	pata	k1gFnSc3	pata
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neklouže	klouzat	k5eNaImIp3nS	klouzat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
její	její	k3xOp3gFnSc1	její
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
odhodu	odhod	k1gInSc2	odhod
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnPc1d1	speciální
boty	bota	k1gFnPc1	bota
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
botách	bota	k1gFnPc6	bota
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
povrchu	povrch	k1gInSc2	povrch
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
rozběhu	rozběh	k1gInSc2	rozběh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hráč	hráč	k1gMnSc1	hráč
vlastní	vlastní	k2eAgFnSc2d1	vlastní
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
boty	bota	k1gFnSc2	bota
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bowlingové	bowlingový	k2eAgFnSc6d1	bowlingová
herně	herna	k1gFnSc6	herna
zdarma	zdarma	k6eAd1	zdarma
nebo	nebo	k8xC	nebo
za	za	k7c4	za
poplatek	poplatek	k1gInSc4	poplatek
půjčit	půjčit	k5eAaPmF	půjčit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
takové	takový	k3xDgFnPc1	takový
herny	herna	k1gFnPc1	herna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
speciální	speciální	k2eAgFnPc1d1	speciální
boty	bota	k1gFnPc1	bota
nejsou	být	k5eNaImIp3nP	být
vyžadovány	vyžadován	k2eAgFnPc1d1	vyžadována
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hraní	hraní	k1gNnSc4	hraní
bowlingu	bowling	k1gInSc2	bowling
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
specifické	specifický	k2eAgNnSc1d1	specifické
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnější	vhodný	k2eAgInPc1d2	vhodnější
jsou	být	k5eAaImIp3nP	být
volnější	volný	k2eAgInPc4d2	volnější
oděvy	oděv	k1gInPc4	oděv
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
při	při	k7c6	při
hodech	hod	k1gInPc6	hod
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
roztržení	roztržení	k1gNnSc3	roztržení
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bowling	bowling	k1gInSc1	bowling
často	často	k6eAd1	často
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
halách	hala	k1gFnPc6	hala
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
se	se	k3xPyFc4	se
oděvem	oděv	k1gInSc7	oděv
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
aktuálnímu	aktuální	k2eAgNnSc3d1	aktuální
počasí	počasí	k1gNnSc3	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
obléknout	obléknout	k5eAaPmF	obléknout
volné	volný	k2eAgFnPc4d1	volná
kalhoty	kalhoty	k1gFnPc4	kalhoty
(	(	kIx(	(
<g/>
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
i	i	k8xC	i
sukni	sukně	k1gFnSc4	sukně
či	či	k8xC	či
kalhotovou	kalhotový	k2eAgFnSc4d1	kalhotová
sukni	sukně	k1gFnSc4	sukně
<g/>
)	)	kIx)	)
a	a	k8xC	a
tričko	tričko	k1gNnSc4	tričko
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
či	či	k8xC	či
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
rukávem	rukáv	k1gInSc7	rukáv
či	či	k8xC	či
sportovní	sportovní	k2eAgFnPc1d1	sportovní
kalhoty	kalhoty	k1gFnPc1	kalhoty
s	s	k7c7	s
polokošilí	polokošile	k1gFnSc7	polokošile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
tolerují	tolerovat	k5eAaImIp3nP	tolerovat
i	i	k9	i
kraťasy	kraťas	k1gInPc1	kraťas
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
tepláky	tepláky	k1gInPc1	tepláky
<g/>
,	,	kIx,	,
i	i	k9	i
značkové	značkový	k2eAgInPc1d1	značkový
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
vhodný	vhodný	k2eAgInSc4d1	vhodný
bowlingový	bowlingový	k2eAgInSc4d1	bowlingový
oděv	oděv	k1gInSc4	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
soutěžích	soutěž	k1gFnPc6	soutěž
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
předpisovým	předpisový	k2eAgNnSc7d1	předpisové
oblečením	oblečení	k1gNnSc7	oblečení
tričko	tričko	k1gNnSc1	tričko
s	s	k7c7	s
límečkem	límeček	k1gInSc7	límeček
a	a	k8xC	a
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
středně	středně	k6eAd1	středně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
sukně	sukně	k1gFnSc1	sukně
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
vybavení	vybavení	k1gNnSc3	vybavení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
u	u	k7c2	u
bowlingu	bowling	k1gInSc2	bowling
využít	využít	k5eAaPmF	využít
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
pomůcky	pomůcka	k1gFnPc1	pomůcka
pro	pro	k7c4	pro
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
zápěstí	zápěstí	k1gNnSc4	zápěstí
(	(	kIx(	(
<g/>
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
,	,	kIx,	,
proužky	proužek	k1gInPc4	proužek
(	(	kIx(	(
<g/>
tape	tap	k1gFnPc4	tap
<g/>
)	)	kIx)	)
k	k	k7c3	k
obvázání	obvázání	k1gNnSc3	obvázání
hráčových	hráčův	k2eAgInPc2d1	hráčův
prstů	prst	k1gInPc2	prst
<g/>
)	)	kIx)	)
náhradní	náhradní	k2eAgFnPc4d1	náhradní
ponožky	ponožka	k1gFnPc4	ponožka
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
hernách	herna	k1gFnPc6	herna
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k8xC	i
jednorázové	jednorázový	k2eAgNnSc4d1	jednorázové
<g />
.	.	kIx.	.
</s>
<s>
hygienické	hygienický	k2eAgFnPc4d1	hygienická
<g/>
)	)	kIx)	)
vložky	vložka	k1gFnPc4	vložka
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
vyplní	vyplnit	k5eAaPmIp3nP	vyplnit
otvory	otvor	k1gInPc1	otvor
v	v	k7c4	v
kouli	koule	k1gFnSc4	koule
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
odhodu	odhod	k1gInSc6	odhod
lépe	dobře	k6eAd2	dobře
držela	držet	k5eAaImAgFnS	držet
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
puffball	puffbalnout	k5eAaPmAgMnS	puffbalnout
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
míček	míček	k1gInSc1	míček
napuštěný	napuštěný	k2eAgInSc1d1	napuštěný
práškem	prášek	k1gMnSc7	prášek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vysušuje	vysušovat	k5eAaImIp3nS	vysušovat
ruce	ruka	k1gFnPc4	ruka
utěrka	utěrka	k1gFnSc1	utěrka
a	a	k8xC	a
čističe	čistič	k1gInPc1	čistič
ve	v	k7c6	v
spreji	sprej	k1gInSc6	sprej
k	k	k7c3	k
setření	setření	k1gNnSc3	setření
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
na	na	k7c4	na
kouli	koule	k1gFnSc4	koule
dostává	dostávat	k5eAaImIp3nS	dostávat
stěrem	stěr	k1gInSc7	stěr
z	z	k7c2	z
čerstvě	čerstvě	k6eAd1	čerstvě
namazané	namazaný	k2eAgFnSc2d1	namazaná
dráhy	dráha	k1gFnSc2	dráha
bowlingová	bowlingový	k2eAgFnSc1d1	bowlingová
taška	taška	k1gFnSc1	taška
Hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
deseti	deset	k4xCc2	deset
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
"	"	kIx"	"
<g/>
frame	framat	k5eAaPmIp3nS	framat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Snahou	snaha	k1gFnSc7	snaha
hráče	hráč	k1gMnSc2	hráč
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
z	z	k7c2	z
deseti	deset	k4xCc2	deset
kol	kola	k1gFnPc2	kola
porazit	porazit	k5eAaPmF	porazit
hozenou	hozený	k2eAgFnSc7d1	hozená
koulí	koule	k1gFnSc7	koule
všechny	všechen	k3xTgFnPc4	všechen
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
postaveny	postaven	k2eAgInPc1d1	postaven
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
hráči	hráč	k1gMnSc3	hráč
nepovede	povést	k5eNaPmIp3nS	povést
jedním	jeden	k4xCgInSc7	jeden
hodem	hod	k1gInSc7	hod
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
hodu	hod	k1gInSc6	hod
(	(	kIx(	(
<g/>
a	a	k8xC	a
případném	případný	k2eAgNnSc6d1	případné
odklizení	odklizení	k1gNnSc6	odklizení
poražených	poražený	k2eAgFnPc2d1	poražená
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
)	)	kIx)	)
hod	hod	k1gInSc1	hod
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
střídají	střídat	k5eAaImIp3nP	střídat
postupně	postupně	k6eAd1	postupně
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
než	než	k8xS	než
odehrají	odehrát	k5eAaPmIp3nP	odehrát
celý	celý	k2eAgInSc4d1	celý
bowlingový	bowlingový	k2eAgInSc4d1	bowlingový
zápas	zápas	k1gInSc4	zápas
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
porazí	porazit	k5eAaPmIp3nS	porazit
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
<g/>
,	,	kIx,	,
hodu	hod	k1gInSc6	hod
všech	všecek	k3xTgFnPc2	všecek
10	[number]	k4	10
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
hází	házet	k5eAaImIp3nS	házet
ještě	ještě	k6eAd1	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
desátém	desátý	k4xOgNnSc6	desátý
kole	kolo	k1gNnSc6	kolo
hází	házet	k5eAaImIp3nS	házet
celkem	celkem	k6eAd1	celkem
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
všech	všecek	k3xTgMnPc2	všecek
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
dráze	dráha	k1gFnSc6	dráha
zápas	zápas	k1gInSc4	zápas
končí	končit	k5eAaImIp3nS	končit
a	a	k8xC	a
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
ten	ten	k3xDgMnSc1	ten
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
v	v	k7c6	v
součtu	součet	k1gInSc6	součet
nejvíce	nejvíce	k6eAd1	nejvíce
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
započítávají	započítávat	k5eAaImIp3nP	započítávat
za	za	k7c4	za
poražené	poražený	k2eAgFnPc4d1	poražená
kuželky	kuželka	k1gFnPc4	kuželka
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
kole	kolo	k1gNnSc6	kolo
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
podaří	podařit	k5eAaPmIp3nP	podařit
porazit	porazit	k5eAaPmF	porazit
kuželky	kuželka	k1gFnPc1	kuželka
všechny	všechen	k3xTgFnPc1	všechen
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
bodové	bodový	k2eAgFnPc1d1	bodová
bonifikace	bonifikace	k1gFnPc1	bonifikace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
možný	možný	k2eAgInSc1d1	možný
bodový	bodový	k2eAgInSc1d1	bodový
zisk	zisk	k1gInSc1	zisk
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
hru	hra	k1gFnSc4	hra
je	být	k5eAaImIp3nS	být
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Hře	hra	k1gFnSc3	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
dosažen	dosažen	k2eAgInSc1d1	dosažen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
perfect	perfect	k2eAgInSc1d1	perfect
game	game	k1gInSc1	game
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Níže	níže	k1gFnPc1	níže
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgFnP	popsat
možné	možný	k2eAgFnPc1d1	možná
herní	herní	k2eAgFnPc1d1	herní
situace	situace	k1gFnPc1	situace
a	a	k8xC	a
u	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
i	i	k8xC	i
příklad	příklad	k1gInSc1	příklad
s	s	k7c7	s
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
počítání	počítání	k1gNnSc2	počítání
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c6	na
ukázce	ukázka	k1gFnSc6	ukázka
tabulky	tabulka	k1gFnSc2	tabulka
se	s	k7c7	s
skóre	skóre	k1gNnSc7	skóre
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
"	"	kIx"	"
<g/>
scoring	scoring	k1gInSc1	scoring
list	list	k1gInSc1	list
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
jen	jen	k9	jen
její	její	k3xOp3gInSc4	její
začátek	začátek	k1gInSc4	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
řádek	řádek	k1gInSc4	řádek
udává	udávat	k5eAaImIp3nS	udávat
číslo	číslo	k1gNnSc1	číslo
kola	kolo	k1gNnSc2	kolo
(	(	kIx(	(
<g/>
1	[number]	k4	1
až	až	k9	až
10	[number]	k4	10
<g/>
)	)	kIx)	)
řádek	řádka	k1gFnPc2	řádka
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
počet	počet	k1gInSc1	počet
poražených	poražený	k2eAgFnPc2d1	poražená
kuželek	kuželka	k1gFnPc2	kuželka
(	(	kIx(	(
<g/>
číslem	číslo	k1gNnSc7	číslo
či	či	k8xC	či
specifickým	specifický	k2eAgInSc7d1	specifický
symbolem	symbol	k1gInSc7	symbol
<g/>
)	)	kIx)	)
řádek	řádek	k1gInSc1	řádek
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
průběžný	průběžný	k2eAgInSc1d1	průběžný
součet	součet	k1gInSc1	součet
dosažených	dosažený	k2eAgInPc2d1	dosažený
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
hráč	hráč	k1gMnSc1	hráč
ani	ani	k8xC	ani
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
hodu	hod	k1gInSc6	hod
nesrazí	srazit	k5eNaPmIp3nS	srazit
všech	všecek	k3xTgFnPc2	všecek
10	[number]	k4	10
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
započítá	započítat	k5eAaPmIp3nS	započítat
se	se	k3xPyFc4	se
hráči	hráč	k1gMnSc3	hráč
bodový	bodový	k2eAgInSc4d1	bodový
zisk	zisk	k1gInSc4	zisk
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
počtu	počet	k1gInSc3	počet
poražených	poražený	k2eAgFnPc2d1	poražená
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkladu	příklad	k1gInSc6	příklad
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
prvním	první	k4xOgInSc7	první
hodem	hod	k1gInSc7	hod
1	[number]	k4	1
kuželku	kuželka	k1gFnSc4	kuželka
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
6	[number]	k4	6
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
dané	daný	k2eAgNnSc4d1	dané
kolo	kolo	k1gNnSc4	kolo
započítá	započítat	k5eAaPmIp3nS	započítat
7	[number]	k4	7
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
1	[number]	k4	1
+	+	kIx~	+
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
koule	koule	k1gFnSc1	koule
neporazí	porazit	k5eNaPmIp3nS	porazit
žádné	žádný	k3yNgFnPc4	žádný
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
buď	buď	k8xC	buď
spadla	spadnout	k5eAaPmAgFnS	spadnout
do	do	k7c2	do
žlábku	žlábek	k1gInSc2	žlábek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
hodu	hod	k1gInSc6	hod
<g/>
)	)	kIx)	)
minula	minulo	k1gNnSc2	minulo
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
ve	v	k7c4	v
scoring	scoring	k1gInSc4	scoring
listu	list	k1gInSc2	list
symbol	symbol	k1gInSc1	symbol
pomlčky	pomlčka	k1gFnSc2	pomlčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkladě	příklad	k1gInSc6	příklad
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
skončila	skončit	k5eAaPmAgFnS	skončit
koule	koule	k1gFnSc1	koule
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
dvou	dva	k4xCgInPc6	dva
hodech	hod	k1gInPc6	hod
ve	v	k7c6	v
žlábku	žlábek	k1gInSc6	žlábek
a	a	k8xC	a
neporazila	porazit	k5eNaPmAgFnS	porazit
tak	tak	k9	tak
žádnou	žádný	k3yNgFnSc4	žádný
kuželku	kuželka	k1gFnSc4	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
kolo	kolo	k1gNnSc1	kolo
započítalo	započítat	k5eAaPmAgNnS	započítat
0	[number]	k4	0
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
0	[number]	k4	0
+	+	kIx~	+
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
shodil	shodit	k5eAaPmAgMnS	shodit
hráč	hráč	k1gMnSc1	hráč
prvním	první	k4xOgInSc7	první
hodem	hod	k1gInSc7	hod
5	[number]	k4	5
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
hodu	hod	k1gInSc6	hod
spadla	spadnout	k5eAaPmAgFnS	spadnout
koule	koule	k1gFnSc1	koule
do	do	k7c2	do
žlábku	žlábek	k1gInSc2	žlábek
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
kteroukoliv	kterýkoliv	k3yIgFnSc4	kterýkoliv
ze	z	k7c2	z
zbylých	zbylý	k2eAgFnPc2d1	zbylá
kuželek	kuželka	k1gFnPc2	kuželka
porazila	porazit	k5eAaPmAgFnS	porazit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
špatně	špatně	k6eAd1	špatně
mířená	mířený	k2eAgFnSc1d1	mířená
a	a	k8xC	a
žádnou	žádný	k3yNgFnSc4	žádný
ze	z	k7c2	z
zbylých	zbylý	k2eAgFnPc2d1	zbylá
kuželek	kuželka	k1gFnPc2	kuželka
neporazila	porazit	k5eNaPmAgFnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
do	do	k7c2	do
scoring	scoring	k1gInSc4	scoring
listu	lista	k1gFnSc4	lista
započetly	započíst	k5eAaPmAgFnP	započíst
pouze	pouze	k6eAd1	pouze
sražené	sražený	k2eAgFnPc1d1	sražená
kuželky	kuželka	k1gFnPc1	kuželka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
5	[number]	k4	5
(	(	kIx(	(
<g/>
5	[number]	k4	5
+	+	kIx~	+
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
součtu	součet	k1gInSc6	součet
má	mít	k5eAaImIp3nS	mít
hráč	hráč	k1gMnSc1	hráč
po	po	k7c6	po
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
5	[number]	k4	5
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
0	[number]	k4	0
+	+	kIx~	+
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spare	Spar	k1gMnSc5	Spar
představuje	představovat	k5eAaImIp3nS	představovat
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hráč	hráč	k1gMnSc1	hráč
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
hodů	hod	k1gInPc2	hod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
kole	kolo	k1gNnSc6	kolo
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
shodí	shodit	k5eAaPmIp3nS	shodit
všech	všecek	k3xTgFnPc2	všecek
10	[number]	k4	10
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Nezáleží	záležet	k5eNaImIp3nS	záležet
přitom	přitom	k6eAd1	přitom
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
počtu	počet	k1gInSc6	počet
kuželek	kuželka	k1gFnPc2	kuželka
shozených	shozený	k2eAgFnPc2d1	shozená
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
hody	hod	k1gInPc7	hod
<g/>
;	;	kIx,	;
podstatný	podstatný	k2eAgInSc1d1	podstatný
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
kuželek	kuželka	k1gFnPc2	kuželka
shozený	shozený	k2eAgMnSc1d1	shozený
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
hodu	hod	k1gInSc6	hod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
takové	takový	k3xDgNnSc4	takový
kolo	kolo	k1gNnSc4	kolo
se	s	k7c7	s
hráči	hráč	k1gMnPc7	hráč
počítá	počítat	k5eAaImIp3nS	počítat
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
za	za	k7c4	za
10	[number]	k4	10
sražených	sražený	k2eAgFnPc2d1	sražená
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
připočítává	připočítávat	k5eAaImIp3nS	připočítávat
počet	počet	k1gInSc1	počet
sražených	sražený	k2eAgFnPc2d1	sražená
kuželek	kuželka	k1gFnPc2	kuželka
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
hodu	hod	k1gInSc6	hod
koulí	koulet	k5eAaImIp3nS	koulet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
scoring	scoring	k1gInSc4	scoring
listu	list	k1gInSc2	list
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
počtu	počet	k1gInSc2	počet
kuželek	kuželka	k1gFnPc2	kuželka
shozených	shozený	k2eAgFnPc2d1	shozená
druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
zaznamená	zaznamenat	k5eAaPmIp3nS	zaznamenat
symbol	symbol	k1gInSc1	symbol
lomítka	lomítko	k1gNnSc2	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkladu	příklad	k1gInSc6	příklad
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
prvním	první	k4xOgInSc7	první
hodem	hod	k1gInSc7	hod
3	[number]	k4	3
kuželky	kuželka	k1gFnSc2	kuželka
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
7	[number]	k4	7
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zbývaly	zbývat	k5eAaImAgFnP	zbývat
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
spare	spar	k1gMnSc5	spar
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
místo	místo	k7c2	místo
7	[number]	k4	7
symbol	symbol	k1gInSc1	symbol
zobrazen	zobrazen	k2eAgInSc1d1	zobrazen
lomítka	lomítko	k1gNnPc4	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
hodu	hod	k1gInSc6	hod
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
porazil	porazit	k5eAaPmAgInS	porazit
2	[number]	k4	2
kuželky	kuželka	k1gFnPc1	kuželka
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
hodu	hod	k1gInSc6	hod
5	[number]	k4	5
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
bodový	bodový	k2eAgInSc1d1	bodový
součet	součet	k1gInSc1	součet
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
počítá	počítat	k5eAaImIp3nS	počítat
coby	coby	k?	coby
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
plus	plus	k6eAd1	plus
počet	počet	k1gInSc1	počet
kuželek	kuželka	k1gFnPc2	kuželka
poražených	poražený	k1gMnPc2	poražený
prvním	první	k4xOgInSc7	první
hodem	hod	k1gInSc7	hod
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
12	[number]	k4	12
(	(	kIx(	(
<g/>
10	[number]	k4	10
+	+	kIx~	+
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
hráč	hráč	k1gMnSc1	hráč
neshodil	shodit	k5eNaPmAgMnS	shodit
všechny	všechen	k3xTgFnPc4	všechen
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
počítají	počítat	k5eAaImIp3nP	počítat
se	se	k3xPyFc4	se
dosažené	dosažený	k2eAgInPc1d1	dosažený
body	bod	k1gInPc1	bod
sečtením	sečtení	k1gNnSc7	sečtení
poražených	poražený	k2eAgFnPc2d1	poražená
kuželek	kuželka	k1gFnPc2	kuželka
(	(	kIx(	(
<g/>
2	[number]	k4	2
+	+	kIx~	+
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průběžný	průběžný	k2eAgInSc4d1	průběžný
bodový	bodový	k2eAgInSc4d1	bodový
součet	součet	k1gInSc4	součet
po	po	k7c6	po
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
19	[number]	k4	19
(	(	kIx(	(
<g/>
12	[number]	k4	12
+	+	kIx~	+
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strike	Strike	k6eAd1	Strike
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prvním	první	k4xOgInSc7	první
hodem	hod	k1gInSc7	hod
v	v	k7c6	v
kole	kolo	k1gNnSc6	kolo
hráč	hráč	k1gMnSc1	hráč
shodil	shodit	k5eAaPmAgMnS	shodit
všech	všecek	k3xTgInPc2	všecek
10	[number]	k4	10
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
hod	hod	k1gInSc1	hod
se	se	k3xPyFc4	se
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
již	již	k6eAd1	již
nehází	házet	k5eNaImIp3nS	házet
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
kolo	kolo	k1gNnSc4	kolo
se	s	k7c7	s
hráči	hráč	k1gMnPc7	hráč
počítá	počítat	k5eAaImIp3nS	počítat
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
za	za	k7c4	za
10	[number]	k4	10
sražených	sražený	k2eAgFnPc2d1	sražená
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
připočítávají	připočítávat	k5eAaImIp3nP	připočítávat
body	bod	k1gInPc1	bod
získané	získaný	k2eAgInPc1d1	získaný
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
následujících	následující	k2eAgNnPc2d1	následující
vypuštění	vypuštění	k1gNnPc2	vypuštění
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
scoring	scoring	k1gInSc4	scoring
listu	list	k1gInSc2	list
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
počtu	počet	k1gInSc2	počet
shozených	shozený	k2eAgFnPc2d1	shozená
kuželek	kuželka	k1gFnPc2	kuželka
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
symbol	symbol	k1gInSc4	symbol
X.	X.	kA	X.
Dosažení	dosažení	k1gNnSc1	dosažení
dvou	dva	k4xCgFnPc2	dva
strike	strike	k1gFnPc2	strike
za	za	k7c7	za
sebou	se	k3xPyFc7	se
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
double	double	k2eAgInSc4d1	double
strike	strike	k1gInSc4	strike
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
či	či	k8xC	či
jen	jen	k9	jen
"	"	kIx"	"
<g/>
double	double	k2eAgInSc4d1	double
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
dosažení	dosažení	k1gNnSc1	dosažení
tří	tři	k4xCgFnPc2	tři
strike	strike	k1gFnPc2	strike
za	za	k7c7	za
sebou	se	k3xPyFc7	se
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
triple	tripl	k1gInSc5	tripl
strike	strike	k1gNnPc7	strike
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
triple	tripl	k1gInSc5	tripl
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Turkey	Turkea	k1gFnSc2	Turkea
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Američan	Američan	k1gMnSc1	Američan
James	James	k1gMnSc1	James
Hylton	Hylton	k1gInSc4	Hylton
shodit	shodit	k5eAaPmF	shodit
všechny	všechen	k3xTgFnPc4	všechen
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zahrát	zahrát	k5eAaPmF	zahrát
strike	strike	k1gFnSc4	strike
<g/>
,	,	kIx,	,
ve	v	k7c6	v
36	[number]	k4	36
hodech	hod	k1gInPc6	hod
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ustavil	ustavit	k5eAaPmAgMnS	ustavit
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkladu	příklad	k1gInSc6	příklad
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
prvním	první	k4xOgMnSc6	první
hodem	hod	k1gInSc7	hod
všech	všecek	k3xTgFnPc2	všecek
10	[number]	k4	10
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
strike	strike	k1gNnSc4	strike
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
hodu	hod	k1gInSc6	hod
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
porazil	porazit	k5eAaPmAgInS	porazit
4	[number]	k4	4
kuželky	kuželka	k1gFnPc1	kuželka
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
hodu	hod	k1gInSc6	hod
3	[number]	k4	3
kuželky	kuželka	k1gFnSc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
postupně	postupně	k6eAd1	postupně
porazil	porazit	k5eAaPmAgInS	porazit
8	[number]	k4	8
kuželek	kuželka	k1gFnPc2	kuželka
prvním	první	k4xOgInSc7	první
hodem	hod	k1gInSc7	hod
a	a	k8xC	a
1	[number]	k4	1
kuželku	kuželka	k1gFnSc4	kuželka
druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
bodový	bodový	k2eAgInSc1d1	bodový
součet	součet	k1gInSc1	součet
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
počítá	počítat	k5eAaImIp3nS	počítat
jako	jako	k9	jako
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
plus	plus	k6eAd1	plus
počet	počet	k1gInSc4	počet
kuželek	kuželka	k1gFnPc2	kuželka
poražených	poražený	k2eAgMnPc2d1	poražený
během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
dvou	dva	k4xCgInPc2	dva
hodů	hod	k1gInPc2	hod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
tedy	tedy	k8xC	tedy
hráč	hráč	k1gMnSc1	hráč
získává	získávat	k5eAaImIp3nS	získávat
17	[number]	k4	17
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
10	[number]	k4	10
+	+	kIx~	+
4	[number]	k4	4
+	+	kIx~	+
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
neshodil	shodit	k5eNaPmAgMnS	shodit
hráč	hráč	k1gMnSc1	hráč
všechny	všechen	k3xTgFnPc4	všechen
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
započítají	započítat	k5eAaPmIp3nP	započítat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
body	bod	k1gInPc1	bod
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
počtu	počet	k1gInSc3	počet
poražených	poražený	k2eAgFnPc2d1	poražená
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
7	[number]	k4	7
(	(	kIx(	(
<g/>
4	[number]	k4	4
+	+	kIx~	+
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průběžný	průběžný	k2eAgInSc1d1	průběžný
bodový	bodový	k2eAgInSc1d1	bodový
součet	součet	k1gInSc1	součet
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
frame	framat	k5eAaPmIp3nS	framat
je	on	k3xPp3gMnPc4	on
tudíž	tudíž	k8xC	tudíž
24	[number]	k4	24
(	(	kIx(	(
<g/>
17	[number]	k4	17
+	+	kIx~	+
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
hráč	hráč	k1gMnSc1	hráč
opět	opět	k6eAd1	opět
neporazil	porazit	k5eNaPmAgMnS	porazit
všechny	všechen	k3xTgFnPc4	všechen
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
jsou	být	k5eAaImIp3nP	být
body	bod	k1gInPc1	bod
počítány	počítán	k2eAgInPc1d1	počítán
jako	jako	k8xS	jako
součet	součet	k1gInSc1	součet
poražených	poražený	k2eAgFnPc2d1	poražená
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
9	[number]	k4	9
(	(	kIx(	(
<g/>
8	[number]	k4	8
+	+	kIx~	+
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
průběžný	průběžný	k2eAgInSc1d1	průběžný
součet	součet	k1gInSc1	součet
po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
33	[number]	k4	33
(	(	kIx(	(
<g/>
17	[number]	k4	17
+	+	kIx~	+
7	[number]	k4	7
+	+	kIx~	+
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Faul	faul	k1gInSc1	faul
znamená	znamenat	k5eAaImIp3nS	znamenat
přešlap	přešlap	k1gInSc4	přešlap
<g/>
.	.	kIx.	.
</s>
<s>
Nastane	nastat	k5eAaPmIp3nS	nastat
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hráč	hráč	k1gMnSc1	hráč
překročí	překročit	k5eAaPmIp3nS	překročit
čáru	čára	k1gFnSc4	čára
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Přešlap	přešlap	k1gInSc1	přešlap
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
hlídán	hlídat	k5eAaImNgInS	hlídat
automaticky	automaticky	k6eAd1	automaticky
(	(	kIx(	(
<g/>
elektronicky	elektronicky	k6eAd1	elektronicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
mohl	moct	k5eAaImAgMnS	moct
hráč	hráč	k1gMnSc1	hráč
tímto	tento	k3xDgInSc7	tento
hodem	hod	k1gInSc7	hod
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nepočítají	počítat	k5eNaImIp3nP	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
přešlapu	přešlapat	k5eAaPmIp1nS	přešlapat
dopustil	dopustit	k5eAaPmAgMnS	dopustit
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
hodu	hod	k1gInSc6	hod
daného	daný	k2eAgNnSc2d1	dané
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
házet	házet	k5eAaImF	házet
ještě	ještě	k9	ještě
hod	hod	k1gInSc1	hod
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkladu	příklad	k1gInSc6	příklad
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
hodu	hod	k1gInSc6	hod
přešlap	přešlap	k1gInSc1	přešlap
a	a	k8xC	a
počet	počet	k1gInSc1	počet
shozených	shozený	k2eAgFnPc2d1	shozená
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
<g/>
-li	i	k?	-li
nějaký	nějaký	k3yIgInSc1	nějaký
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tak	tak	k9	tak
neeviduje	evidovat	k5eNaImIp3nS	evidovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
porazil	porazit	k5eAaPmAgMnS	porazit
8	[number]	k4	8
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
kolo	kolo	k1gNnSc4	kolo
počítá	počítat	k5eAaImIp3nS	počítat
8	[number]	k4	8
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
0	[number]	k4	0
+	+	kIx~	+
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Split	Split	k1gInSc1	Split
–	–	k?	–
lidově	lidově	k6eAd1	lidově
často	často	k6eAd1	často
parohy	paroh	k1gInPc1	paroh
–	–	k?	–
označuje	označovat	k5eAaImIp3nS	označovat
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
vypuštění	vypuštění	k1gNnSc6	vypuštění
koule	koule	k1gFnSc2	koule
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
frame	framat	k5eAaPmIp3nS	framat
je	být	k5eAaImIp3nS	být
poražena	poražen	k2eAgFnSc1d1	poražena
jednak	jednak	k8xC	jednak
první	první	k4xOgFnSc1	první
kuželka	kuželka	k1gFnSc1	kuželka
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jedna	jeden	k4xCgNnPc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
kuželek	kuželka	k1gFnPc2	kuželka
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
nebo	nebo	k8xC	nebo
před	před	k7c7	před
sebou	se	k3xPyFc7	se
stojícími	stojící	k2eAgFnPc7d1	stojící
kuželkami	kuželka	k1gFnPc7	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Poražení	poražení	k1gNnSc1	poražení
všech	všecek	k3xTgFnPc2	všecek
zbylých	zbylý	k2eAgFnPc2d1	zbylá
kuželek	kuželka	k1gFnPc2	kuželka
druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nikoliv	nikoliv	k9	nikoliv
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Split	Split	k1gInSc1	Split
je	být	k5eAaImIp3nS	být
ve	v	k7c4	v
scoring	scoring	k1gInSc4	scoring
listu	list	k1gInSc2	list
zvýrazněn	zvýraznit	k5eAaPmNgInS	zvýraznit
zakroužkováním	zakroužkování	k1gNnSc7	zakroužkování
<g/>
,	,	kIx,	,
orámováním	orámování	k1gNnSc7	orámování
či	či	k8xC	či
odlišným	odlišný	k2eAgNnSc7d1	odlišné
barevným	barevný	k2eAgNnSc7d1	barevné
provedením	provedení	k1gNnSc7	provedení
čísla	číslo	k1gNnSc2	číslo
vyjadřující	vyjadřující	k2eAgInSc4d1	vyjadřující
počet	počet	k1gInSc4	počet
sražených	sražený	k2eAgFnPc2d1	sražená
kuželek	kuželka	k1gFnPc2	kuželka
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ke	k	k7c3	k
splitu	split	k1gInSc3	split
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Bodově	bodově	k6eAd1	bodově
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
nijak	nijak	k6eAd1	nijak
zvýhodněna	zvýhodnit	k5eAaPmNgFnS	zvýhodnit
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příkladu	příklad	k1gInSc6	příklad
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
prvním	první	k4xOgInSc7	první
hodem	hod	k1gInSc7	hod
7	[number]	k4	7
kuželek	kuželka	k1gFnPc2	kuželka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
split	split	k1gMnSc1	split
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
tohoto	tento	k3xDgNnSc2	tento
kola	kolo	k1gNnSc2	kolo
porazil	porazit	k5eAaPmAgInS	porazit
2	[number]	k4	2
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
celkově	celkově	k6eAd1	celkově
za	za	k7c4	za
daný	daný	k2eAgInSc4d1	daný
frame	frait	k5eAaImRp1nP	frait
započítá	započítat	k5eAaPmIp3nS	započítat
9	[number]	k4	9
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
7	[number]	k4	7
+	+	kIx~	+
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Perfect	Perfect	k2eAgInSc1d1	Perfect
game	game	k1gInSc1	game
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
hráč	hráč	k1gMnSc1	hráč
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
možný	možný	k2eAgInSc4d1	možný
bodový	bodový	k2eAgInSc4d1	bodový
zisk	zisk	k1gInSc4	zisk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
300	[number]	k4	300
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
kol	kolo	k1gNnPc2	kolo
musí	muset	k5eAaImIp3nS	muset
zahrát	zahrát	k5eAaPmF	zahrát
strike	strike	k6eAd1	strike
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
soutěžním	soutěžní	k2eAgInSc6d1	soutěžní
zápase	zápas	k1gInSc6	zápas
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Silvester	Silvester	k1gMnSc1	Silvester
Gyertyák	Gyertyák	k1gMnSc1	Gyertyák
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
perfect	perfect	k2eAgInSc4d1	perfect
games	games	k1gInSc4	games
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byly	být	k5eAaImAgFnP	být
zahrány	zahrát	k5eAaPmNgFnP	zahrát
s	s	k7c7	s
excentrickými	excentrický	k2eAgFnPc7d1	excentrická
koulemi	koule	k1gFnPc7	koule
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovinném	rovinný	k2eAgInSc6d1	rovinný
bowlingu	bowling	k1gInSc6	bowling
nikdo	nikdo	k3yNnSc1	nikdo
perfect	perfect	k1gInSc1	perfect
game	game	k1gInSc4	game
ještě	ještě	k9	ještě
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
akcích	akce	k1gFnPc6	akce
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Počítání	počítání	k1gNnSc1	počítání
bodů	bod	k1gInPc2	bod
v	v	k7c4	v
perfect	perfect	k2eAgInSc4d1	perfect
game	game	k1gInSc4	game
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
tabulka	tabulka	k1gFnSc1	tabulka
níže	níže	k1gFnSc1	níže
<g/>
:	:	kIx,	:
Clean	Clean	k1gInSc1	Clean
game	game	k1gInSc1	game
označuje	označovat	k5eAaImIp3nS	označovat
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
kol	kolo	k1gNnPc2	kolo
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
hráč	hráč	k1gMnSc1	hráč
striku	strika	k1gFnSc4	strika
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
spare	spar	k1gMnSc5	spar
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
že	že	k8xS	že
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kteréhokoliv	kterýkoliv	k3yIgNnSc2	kterýkoliv
z	z	k7c2	z
kol	kola	k1gFnPc2	kola
nezůstanou	zůstat	k5eNaPmIp3nP	zůstat
nějaké	nějaký	k3yIgFnPc4	nějaký
neporažené	poražený	k2eNgFnPc4d1	neporažená
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
lze	lze	k6eAd1	lze
v	v	k7c6	v
součtu	součet	k1gInSc6	součet
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
kol	kolo	k1gNnPc2	kolo
prvním	první	k4xOgMnSc6	první
hodem	hod	k1gInSc7	hod
neshodil	shodit	k5eNaPmAgInS	shodit
žádnou	žádný	k3yNgFnSc4	žádný
kuželku	kuželka	k1gFnSc4	kuželka
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
porazil	porazit	k5eAaPmAgMnS	porazit
kuželky	kuželka	k1gFnPc1	kuželka
všechny	všechen	k3xTgFnPc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
amatérské	amatérský	k2eAgMnPc4d1	amatérský
či	či	k8xC	či
příležitostné	příležitostný	k2eAgMnPc4d1	příležitostný
hráče	hráč	k1gMnPc4	hráč
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
vybavení	vybavení	k1gNnSc4	vybavení
půjčit	půjčit	k5eAaPmF	půjčit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
bowlingové	bowlingový	k2eAgFnSc6d1	bowlingová
herně	herna	k1gFnSc6	herna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
si	se	k3xPyFc3	se
od	od	k7c2	od
bowlera	bowler	k1gMnSc2	bowler
(	(	kIx(	(
<g/>
pracovníka	pracovník	k1gMnSc2	pracovník
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
herny	herna	k1gFnSc2	herna
<g/>
)	)	kIx)	)
vyzvednout	vyzvednout	k5eAaPmF	vyzvednout
speciální	speciální	k2eAgFnPc4d1	speciální
boty	bota	k1gFnPc4	bota
–	–	k?	–
pokud	pokud	k8xS	pokud
nemá	mít	k5eNaImIp3nS	mít
hráč	hráč	k1gMnSc1	hráč
vlastní	vlastní	k2eAgFnSc2d1	vlastní
–	–	k?	–
a	a	k8xC	a
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
hráči	hráč	k1gMnPc7	hráč
určena	určit	k5eAaPmNgFnS	určit
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
bude	být	k5eAaImBp3nS	být
své	svůj	k3xOyFgNnSc1	svůj
utkání	utkání	k1gNnSc1	utkání
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnou	vhodný	k2eAgFnSc4d1	vhodná
bowlingovou	bowlingový	k2eAgFnSc4d1	bowlingová
kouli	koule	k1gFnSc4	koule
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
vybírá	vybírat	k5eAaImIp3nS	vybírat
podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
podavači	podavač	k1gInSc6	podavač
koulí	koulet	k5eAaImIp3nS	koulet
<g/>
,	,	kIx,	,
nalezne	naleznout	k5eAaPmIp3nS	naleznout
hráč	hráč	k1gMnSc1	hráč
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
kouli	koule	k1gFnSc4	koule
v	v	k7c6	v
držáku	držák	k1gInSc6	držák
bowlingových	bowlingový	k2eAgFnPc2d1	bowlingová
koulí	koule	k1gFnPc2	koule
poblíž	poblíž	k7c2	poblíž
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Kouli	koule	k1gFnSc4	koule
drží	držet	k5eAaImIp3nP	držet
praváci	pravák	k1gMnPc1	pravák
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
a	a	k8xC	a
leváci	levák	k1gMnPc1	levák
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Uchopí	uchopit	k5eAaPmIp3nP	uchopit
se	se	k3xPyFc4	se
prsty	prst	k1gInPc1	prst
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
děr	děra	k1gFnPc2	děra
v	v	k7c6	v
povrchu	povrch	k1gInSc6	povrch
koule	koule	k1gFnSc2	koule
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
zasune	zasunout	k5eAaPmIp3nS	zasunout
palec	palec	k1gInSc1	palec
<g/>
,	,	kIx,	,
prostředníček	prostředníček	k1gInSc1	prostředníček
a	a	k8xC	a
prsteníček	prsteníček	k1gInSc1	prsteníček
dané	daný	k2eAgFnSc2d1	daná
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ukazováček	ukazováček	k1gInSc1	ukazováček
a	a	k8xC	a
malíček	malíček	k1gInSc1	malíček
spočívají	spočívat	k5eAaImIp3nP	spočívat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Koule	koule	k1gFnSc1	koule
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
dobře	dobře	k6eAd1	dobře
padnout	padnout	k5eAaPmF	padnout
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
hodem	hod	k1gInSc7	hod
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
se	se	k3xPyFc4	se
ke	k	k7c3	k
kuželkám	kuželka	k1gFnPc3	kuželka
postavit	postavit	k5eAaPmF	postavit
čelem	čelo	k1gNnSc7	čelo
<g/>
,	,	kIx,	,
srovnat	srovnat	k5eAaPmF	srovnat
tělo	tělo	k1gNnSc4	tělo
i	i	k8xC	i
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
narovnat	narovnat	k5eAaPmF	narovnat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vypuštěním	vypuštění	k1gNnSc7	vypuštění
koule	koule	k1gFnSc2	koule
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc4d1	vhodný
krátký	krátký	k2eAgInSc4d1	krátký
rozběh	rozběh	k1gInSc4	rozběh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hraní	hranit	k5eAaImIp3nS	hranit
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
.	.	kIx.	.
</s>
<s>
Správným	správný	k2eAgInSc7d1	správný
rozběhem	rozběh	k1gInSc7	rozběh
získá	získat	k5eAaPmIp3nS	získat
koule	koule	k1gFnSc1	koule
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
lze	lze	k6eAd1	lze
tak	tak	k9	tak
i	i	k9	i
snáze	snadno	k6eAd2	snadno
trefovat	trefovat	k5eAaImF	trefovat
kuželky	kuželka	k1gFnPc4	kuželka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
plynulost	plynulost	k1gFnSc4	plynulost
a	a	k8xC	a
přesnost	přesnost	k1gFnSc4	přesnost
hodu	hod	k1gInSc2	hod
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
snahou	snaha	k1gFnSc7	snaha
hráče	hráč	k1gMnSc2	hráč
hodit	hodit	k5eAaImF	hodit
koulí	koulet	k5eAaImIp3nS	koulet
co	co	k3yRnSc1	co
nejrychleji	rychle	k6eAd3	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
rozbíhá	rozbíhat	k5eAaImIp3nS	rozbíhat
v	v	k7c6	v
přímce	přímka	k1gFnSc6	přímka
se	s	k7c7	s
zamýšleným	zamýšlený	k2eAgInSc7d1	zamýšlený
směrem	směr	k1gInSc7	směr
odhodu	odhod	k1gInSc2	odhod
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
přesnějšímu	přesný	k2eAgNnSc3d2	přesnější
zasažení	zasažení	k1gNnSc3	zasažení
zvoleného	zvolený	k2eAgInSc2d1	zvolený
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Přímému	přímý	k2eAgInSc3d1	přímý
směru	směr	k1gInSc3	směr
odhodu	odhod	k1gInSc2	odhod
a	a	k8xC	a
lepšímu	dobrý	k2eAgNnSc3d2	lepší
míření	míření	k1gNnSc3	míření
napomáhají	napomáhat	k5eAaImIp3nP	napomáhat
i	i	k9	i
různobarevné	různobarevný	k2eAgFnPc1d1	různobarevná
parkety	parketa	k1gFnPc1	parketa
na	na	k7c6	na
bowlingové	bowlingový	k2eAgFnSc6d1	bowlingová
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hodu	hod	k1gInSc6	hod
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
popis	popis	k1gInSc1	popis
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
praváka	pravák	k1gMnSc2	pravák
<g/>
,	,	kIx,	,
se	s	k7c7	s
pravou	pravý	k2eAgFnSc7d1	pravá
nohou	noha	k1gFnSc7	noha
vykročí	vykročit	k5eAaPmIp3nS	vykročit
vpřed	vpřed	k6eAd1	vpřed
a	a	k8xC	a
současně	současně	k6eAd1	současně
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
vychýlí	vychýlit	k5eAaPmIp3nS	vychýlit
pravá	pravý	k2eAgFnSc1d1	pravá
ruka	ruka	k1gFnSc1	ruka
s	s	k7c7	s
koulí	koule	k1gFnSc7	koule
směrem	směr	k1gInSc7	směr
vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgInSc2	druhý
kroku	krok	k1gInSc2	krok
se	se	k3xPyFc4	se
koule	koule	k1gFnSc1	koule
vychyluje	vychylovat	k5eAaImIp3nS	vychylovat
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
velké	velký	k2eAgNnSc4d1	velké
kyvadlo	kyvadlo	k1gNnSc4	kyvadlo
<g/>
.	.	kIx.	.
</s>
<s>
Rovnováha	rovnováha	k1gFnSc1	rovnováha
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
udržuje	udržovat	k5eAaImIp3nS	udržovat
upažením	upažení	k1gNnSc7	upažení
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
kroku	krok	k1gInSc6	krok
se	se	k3xPyFc4	se
koule	koule	k1gFnSc1	koule
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
úvrati	úvrať	k1gFnSc6	úvrať
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kouli	koule	k1gFnSc4	koule
drží	držet	k5eAaImIp3nS	držet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
napřažená	napřažený	k2eAgFnSc1d1	napřažená
a	a	k8xC	a
neohýbá	ohýbat	k5eNaImIp3nS	ohýbat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
kroku	krok	k1gInSc6	krok
se	se	k3xPyFc4	se
levá	levý	k2eAgFnSc1d1	levá
noha	noha	k1gFnSc1	noha
mírně	mírně	k6eAd1	mírně
sklouzne	sklouznout	k5eAaPmIp3nS	sklouznout
a	a	k8xC	a
ruka	ruka	k1gFnSc1	ruka
s	s	k7c7	s
koulí	koule	k1gFnSc7	koule
míjí	míjet	k5eAaImIp3nS	míjet
tuto	tento	k3xDgFnSc4	tento
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Koule	koule	k1gFnSc1	koule
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nechá	nechat	k5eAaPmIp3nS	nechat
samovolně	samovolně	k6eAd1	samovolně
oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
držela	držet	k5eAaImAgFnS	držet
<g/>
,	,	kIx,	,
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
zamýšlený	zamýšlený	k2eAgInSc4d1	zamýšlený
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
koule	koule	k1gFnSc1	koule
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
vlivem	vliv	k1gInSc7	vliv
nárazu	náraz	k1gInSc2	náraz
o	o	k7c4	o
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
vychýlena	vychýlen	k2eAgFnSc1d1	vychýlena
z	z	k7c2	z
uvažovaného	uvažovaný	k2eAgInSc2d1	uvažovaný
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
zanoží	zanožit	k5eAaPmIp3nS	zanožit
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
noha	noha	k1gFnSc1	noha
za	za	k7c4	za
levou	levý	k2eAgFnSc4d1	levá
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
kouli	koule	k1gFnSc4	koule
odhazovat	odhazovat	k5eAaImF	odhazovat
za	za	k7c7	za
palcem	palec	k1gInSc7	palec
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
jde	jít	k5eAaImIp3nS	jít
palec	palec	k1gInSc1	palec
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
jde	jít	k5eAaImIp3nS	jít
i	i	k9	i
koule	koule	k1gFnSc1	koule
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nastavení	nastavení	k1gNnSc1	nastavení
palce	palec	k1gInSc2	palec
obvykle	obvykle	k6eAd1	obvykle
určuje	určovat	k5eAaImIp3nS	určovat
směr	směr	k1gInSc4	směr
běhu	běh	k1gInSc2	běh
koule	koule	k1gFnSc2	koule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zajistí	zajistit	k5eAaPmIp3nS	zajistit
vyšší	vysoký	k2eAgFnSc1d2	vyšší
přesnost	přesnost	k1gFnSc1	přesnost
hraní	hraní	k1gNnSc2	hraní
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hráč	hráč	k1gMnSc1	hráč
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
hodu	hod	k1gInSc6	hod
framu	fram	k1gInSc2	fram
nesrazí	srazit	k5eNaPmIp3nS	srazit
všechny	všechen	k3xTgFnPc4	všechen
kuželky	kuželka	k1gFnPc4	kuželka
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nP	snažit
se	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
hodem	hod	k1gInSc7	hod
srazit	srazit	k5eAaPmF	srazit
zbývající	zbývající	k2eAgNnSc4d1	zbývající
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
hod	hod	k1gInSc1	hod
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dohoz	dohoz	k1gInSc1	dohoz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
provedení	provedení	k1gNnSc1	provedení
dohozů	dohoz	k1gInPc2	dohoz
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bez	bez	k7c2	bez
nich	on	k3xPp3gFnPc2	on
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
docílit	docílit	k5eAaPmF	docílit
vysokých	vysoký	k2eAgFnPc2d1	vysoká
hodnot	hodnota	k1gFnPc2	hodnota
v	v	k7c6	v
součtu	součet	k1gInSc6	součet
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
se	se	k3xPyFc4	se
tři	tři	k4xCgFnPc1	tři
možné	možný	k2eAgFnPc1d1	možná
způsoby	způsoba	k1gFnPc1	způsoba
provedení	provedení	k1gNnSc2	provedení
dohozu	dohoz	k1gInSc2	dohoz
<g/>
:	:	kIx,	:
hráč	hráč	k1gMnSc1	hráč
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
kouli	koule	k1gFnSc4	koule
ze	z	k7c2	z
stejného	stejný	k2eAgNnSc2d1	stejné
místa	místo	k1gNnSc2	místo
jako	jako	k8xS	jako
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
hodu	hod	k1gInSc6	hod
framu	framat	k5eAaPmIp1nS	framat
a	a	k8xC	a
případné	případný	k2eAgNnSc1d1	případné
vychýlení	vychýlení	k1gNnSc1	vychýlení
přímého	přímý	k2eAgInSc2d1	přímý
směru	směr	k1gInSc2	směr
provede	provést	k5eAaPmIp3nS	provést
natočením	natočení	k1gNnSc7	natočení
palce	palec	k1gInSc2	palec
hráč	hráč	k1gMnSc1	hráč
hází	házet	k5eAaImIp3nS	házet
po	po	k7c6	po
parketě	parketa	k1gFnSc6	parketa
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
koule	koule	k1gFnSc1	koule
běžela	běžet	k5eAaImAgFnS	běžet
po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
parketě	parketa	k1gFnSc6	parketa
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
stojí	stát	k5eAaImIp3nS	stát
zbylá	zbylý	k2eAgFnSc1d1	zbylá
kuželka	kuželka	k1gFnSc1	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaImF	zdát
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
koule	koule	k1gFnSc1	koule
–	–	k?	–
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
míří	mířit	k5eAaImIp3nS	mířit
na	na	k7c4	na
krajní	krajní	k2eAgFnPc4d1	krajní
kuželky	kuželka	k1gFnPc4	kuželka
vzadu	vzadu	k6eAd1	vzadu
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
nebo	nebo	k8xC	nebo
10	[number]	k4	10
<g/>
)	)	kIx)	)
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
špatném	špatný	k2eAgNnSc6d1	špatné
zamíření	zamíření	k1gNnSc6	zamíření
snadno	snadno	k6eAd1	snadno
spadnout	spadnout	k5eAaPmF	spadnout
do	do	k7c2	do
žlábku	žlábek	k1gInSc2	žlábek
dohazování	dohazování	k1gNnSc2	dohazování
křížem	křížem	k6eAd1	křížem
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
diagonálně	diagonálně	k6eAd1	diagonálně
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
dráhu	dráha	k1gFnSc4	dráha
Poslední	poslední	k2eAgInSc4d1	poslední
způsob	způsob	k1gInSc4	způsob
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
doporučován	doporučovat	k5eAaImNgInS	doporučovat
začátečníkům	začátečník	k1gMnPc3	začátečník
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
volba	volba	k1gFnSc1	volba
optimálního	optimální	k2eAgInSc2d1	optimální
způsobu	způsob	k1gInSc2	způsob
dohazování	dohazování	k1gNnSc2	dohazování
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
hráči	hráč	k1gMnPc7	hráč
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gMnSc4	on
i	i	k8xC	i
obměňovat	obměňovat	k5eAaImF	obměňovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
rozestavení	rozestavení	k1gNnSc6	rozestavení
dohazovaných	dohazovaný	k2eAgFnPc2d1	dohazovaný
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
bowlingových	bowlingový	k2eAgMnPc2d1	bowlingový
hráčů	hráč	k1gMnPc2	hráč
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
nešvarům	nešvar	k1gInPc3	nešvar
či	či	k8xC	či
nesportovnímu	sportovní	k2eNgNnSc3d1	nesportovní
chování	chování	k1gNnSc3	chování
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
znepříjemňují	znepříjemňovat	k5eAaImIp3nP	znepříjemňovat
hru	hra	k1gFnSc4	hra
ostatním	ostatní	k2eAgMnPc3d1	ostatní
hráčům	hráč	k1gMnPc3	hráč
<g/>
:	:	kIx,	:
Při	při	k7c6	při
bowlingu	bowling	k1gInSc6	bowling
není	být	k5eNaImIp3nS	být
(	(	kIx(	(
<g/>
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
obtěžování	obtěžování	k1gNnSc2	obtěžování
spoluhráčů	spoluhráč	k1gMnPc2	spoluhráč
<g/>
)	)	kIx)	)
vhodné	vhodný	k2eAgNnSc1d1	vhodné
kouření	kouření	k1gNnSc1	kouření
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
mít	mít	k5eAaImF	mít
v	v	k7c6	v
podavači	podavač	k1gInSc6	podavač
koulí	koulet	k5eAaImIp3nS	koulet
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
bowlingových	bowlingový	k2eAgFnPc2d1	bowlingová
koulí	koule	k1gFnPc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgMnSc3	každý
hráči	hráč	k1gMnSc3	hráč
stačí	stačit	k5eAaBmIp3nS	stačit
obvykle	obvykle	k6eAd1	obvykle
jedna	jeden	k4xCgFnSc1	jeden
koule	koule	k1gFnSc1	koule
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přebytečném	přebytečný	k2eAgInSc6d1	přebytečný
počtu	počet	k1gInSc6	počet
koulí	koulet	k5eAaImIp3nS	koulet
v	v	k7c6	v
podavači	podavač	k1gInSc6	podavač
hrozí	hrozit	k5eAaImIp3nS	hrozit
jejich	jejich	k3xOp3gNnSc4	jejich
vypadnutí	vypadnutí	k1gNnSc4	vypadnutí
z	z	k7c2	z
podavače	podavač	k1gInSc2	podavač
a	a	k8xC	a
vypadnuvší	vypadnuvší	k2eAgFnSc2d1	vypadnuvší
koule	koule	k1gFnSc2	koule
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
zranit	zranit	k5eAaPmF	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Podrážky	podrážka	k1gFnPc1	podrážka
bot	bota	k1gFnPc2	bota
se	se	k3xPyFc4	se
neupravují	upravovat	k5eNaImIp3nP	upravovat
a	a	k8xC	a
ani	ani	k8xC	ani
neotírají	otírat	k5eNaImIp3nP	otírat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
mokrým	mokrý	k2eAgInSc7d1	mokrý
hadříkem	hadřík	k1gInSc7	hadřík
či	či	k8xC	či
olejem	olej	k1gInSc7	olej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c4	na
boty	bota	k1gFnPc4	bota
nenanáší	nanášet	k5eNaImIp3nS	nanášet
vrstva	vrstva	k1gFnSc1	vrstva
mastku	mastek	k1gInSc2	mastek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
boty	bota	k1gFnPc1	bota
lépe	dobře	k6eAd2	dobře
klouzaly	klouzat	k5eAaImAgFnP	klouzat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
úprava	úprava	k1gFnSc1	úprava
je	být	k5eAaImIp3nS	být
zakázaná	zakázaný	k2eAgFnSc1d1	zakázaná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hře	hra	k1gFnSc6	hra
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
jíst	jíst	k5eAaImF	jíst
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
prstech	prst	k1gInPc6	prst
ulpívá	ulpívat	k5eAaImIp3nS	ulpívat
špína	špína	k1gFnSc1	špína
z	z	k7c2	z
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
zbytky	zbytek	k1gInPc1	zbytek
jídla	jídlo	k1gNnSc2	jídlo
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
otvorů	otvor	k1gInPc2	otvor
v	v	k7c4	v
kouli	koule	k1gFnSc4	koule
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
koule	koule	k1gFnSc2	koule
v	v	k7c6	v
podavači	podavač	k1gInSc6	podavač
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
na	na	k7c4	na
kouli	koule	k1gFnSc4	koule
sahat	sahat	k5eAaImF	sahat
přímo	přímo	k6eAd1	přímo
prsty	prst	k1gInPc4	prst
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
ne	ne	k9	ne
těmi	ten	k3xDgInPc7	ten
prsty	prst	k1gInPc7	prst
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
hráč	hráč	k1gMnSc1	hráč
kouli	koule	k1gFnSc4	koule
drží	držet	k5eAaImIp3nS	držet
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Dráhy	dráha	k1gFnPc1	dráha
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
bowlingových	bowlingový	k2eAgNnPc6d1	bowlingové
centrech	centrum	k1gNnPc6	centrum
ošetřeny	ošetřen	k2eAgFnPc1d1	ošetřena
olejem	olej	k1gInSc7	olej
a	a	k8xC	a
olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
i	i	k9	i
na	na	k7c4	na
koule	koule	k1gFnPc4	koule
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ztíží	ztížet	k5eAaPmIp3nS	ztížet
držení	držení	k1gNnSc1	držení
koule	koule	k1gFnSc2	koule
(	(	kIx(	(
<g/>
z	z	k7c2	z
naolejovaných	naolejovaný	k2eAgInPc2d1	naolejovaný
prstů	prst	k1gInPc2	prst
koule	koule	k1gFnSc2	koule
snáze	snadno	k6eAd2	snadno
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
je	být	k5eAaImIp3nS	být
pomoci	pomoct	k5eAaPmF	pomoct
si	se	k3xPyFc3	se
při	při	k7c6	při
otáčení	otáčení	k1gNnSc6	otáčení
koule	koule	k1gFnPc4	koule
druhou	druhý	k4xOgFnSc7	druhý
rukou	ruka	k1gFnSc7	ruka
nebo	nebo	k8xC	nebo
kouli	koule	k1gFnSc4	koule
otáčet	otáčet	k5eAaImF	otáčet
pomocí	pomocí	k7c2	pomocí
dlaně	dlaň	k1gFnSc2	dlaň
a	a	k8xC	a
prsty	prst	k1gInPc4	prst
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
nedotýkat	dotýkat	k5eNaImF	dotýkat
se	se	k3xPyFc4	se
povrchu	povrch	k1gInSc3	povrch
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Nápoj	nápoj	k1gInSc1	nápoj
ve	v	k7c6	v
sklenici	sklenice	k1gFnSc6	sklenice
či	či	k8xC	či
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
lahvi	lahev	k1gFnSc6	lahev
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
umisťovat	umisťovat	k5eAaImF	umisťovat
během	během	k7c2	během
bowlingového	bowlingový	k2eAgInSc2d1	bowlingový
zápasu	zápas	k1gInSc2	zápas
na	na	k7c4	na
stolek	stolek	k1gInSc4	stolek
u	u	k7c2	u
dráhy	dráha	k1gFnSc2	dráha
neboť	neboť	k8xC	neboť
vylitá	vylitý	k2eAgFnSc1d1	vylitá
tekutina	tekutina	k1gFnSc1	tekutina
může	moct	k5eAaImIp3nS	moct
znečistit	znečistit	k5eAaPmF	znečistit
oděv	oděv	k1gInSc4	oděv
či	či	k8xC	či
stůl	stůl	k1gInSc4	stůl
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pádu	pád	k1gInSc2	pád
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
skleněné	skleněný	k2eAgFnPc4d1	skleněná
věci	věc	k1gFnPc4	věc
rozbít	rozbít	k5eAaPmF	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
potřísněných	potřísněný	k2eAgFnPc6d1	potřísněná
botách	bota	k1gFnPc6	bota
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hodu	hod	k1gInSc2	hod
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
hráč	hráč	k1gMnSc1	hráč
nijak	nijak	k6eAd1	nijak
rušen	rušen	k2eAgMnSc1d1	rušen
či	či	k8xC	či
rozptylován	rozptylován	k2eAgMnSc1d1	rozptylován
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vstupem	vstup	k1gInSc7	vstup
jiného	jiný	k2eAgMnSc2d1	jiný
hráče	hráč	k1gMnSc2	hráč
v	v	k7c6	v
době	doba	k1gFnSc6	doba
odhodu	odhod	k1gInSc2	odhod
do	do	k7c2	do
hracího	hrací	k2eAgInSc2d1	hrací
prostoru	prostor	k1gInSc2	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
mají	mít	k5eAaImIp3nP	mít
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
sedat	sedat	k5eAaImF	sedat
na	na	k7c4	na
svod	svod	k1gInSc4	svod
či	či	k8xC	či
zásobník	zásobník	k1gInSc4	zásobník
podavače	podavač	k1gInSc2	podavač
bowlingových	bowlingový	k2eAgFnPc2d1	bowlingová
koulí	koule	k1gFnPc2	koule
a	a	k8xC	a
nesmějí	smát	k5eNaImIp3nP	smát
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
chovat	chovat	k5eAaImF	chovat
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
běžnými	běžný	k2eAgNnPc7d1	běžné
pravidly	pravidlo	k1gNnPc7	pravidlo
společenského	společenský	k2eAgNnSc2d1	společenské
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
je	být	k5eAaImIp3nS	být
též	též	k9	též
zdržování	zdržování	k1gNnSc4	zdržování
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
kupříkladu	kupříkladu	k6eAd1	kupříkladu
nepřiměřeným	přiměřený	k2eNgMnSc7d1	nepřiměřený
a	a	k8xC	a
neopodstatněným	opodstatněný	k2eNgNnSc7d1	neopodstatněné
oddalováním	oddalování	k1gNnSc7	oddalování
hodu	hod	k1gInSc2	hod
koulí	koule	k1gFnPc2	koule
nebo	nebo	k8xC	nebo
nepřipravením	nepřipravení	k1gNnSc7	nepřipravení
se	se	k3xPyFc4	se
k	k	k7c3	k
hodu	hod	k1gInSc3	hod
ve	v	k7c6	v
stanoveném	stanovený	k2eAgNnSc6d1	stanovené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
bowlingové	bowlingový	k2eAgNnSc4d1	bowlingové
umění	umění	k1gNnSc4	umění
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
hráči	hráč	k1gMnPc1	hráč
vzájemně	vzájemně	k6eAd1	vzájemně
porovnávat	porovnávat	k5eAaImF	porovnávat
v	v	k7c6	v
turnajích	turnaj	k1gInPc6	turnaj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
pořádány	pořádán	k2eAgInPc1d1	pořádán
buď	buď	k8xC	buď
samotnými	samotný	k2eAgMnPc7d1	samotný
hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
hernami	herna	k1gFnPc7	herna
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
hráče	hráč	k1gMnPc4	hráč
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
–	–	k?	–
u	u	k7c2	u
regionálních	regionální	k2eAgFnPc2d1	regionální
<g/>
,	,	kIx,	,
celostátních	celostátní	k2eAgFnPc2d1	celostátní
či	či	k8xC	či
světových	světový	k2eAgFnPc2d1	světová
soutěží	soutěž	k1gFnPc2	soutěž
–	–	k?	–
speciálními	speciální	k2eAgInPc7d1	speciální
organizátory	organizátor	k1gInPc7	organizátor
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
turnaje	turnaj	k1gInPc1	turnaj
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
pro	pro	k7c4	pro
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
,	,	kIx,	,
dvojice	dvojice	k1gFnSc2	dvojice
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
či	či	k8xC	či
pětičlenné	pětičlenný	k2eAgInPc1d1	pětičlenný
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
(	(	kIx(	(
<g/>
neoficiálních	oficiální	k2eNgFnPc6d1	neoficiální
<g/>
)	)	kIx)	)
soutěžích	soutěž	k1gFnPc6	soutěž
jsou	být	k5eAaImIp3nP	být
ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
výkonu	výkon	k1gInSc2	výkon
s	s	k7c7	s
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
započítávány	započítáván	k2eAgInPc1d1	započítáván
určité	určitý	k2eAgInPc1d1	určitý
bonusy	bonus	k1gInPc1	bonus
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
handicapy	handicap	k1gInPc4	handicap
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gMnPc3	on
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
hry	hra	k1gFnSc2	hra
připočítány	připočítán	k2eAgInPc4d1	připočítán
body	bod	k1gInPc4	bod
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
hru	hra	k1gFnSc4	hra
zahajují	zahajovat	k5eAaImIp3nP	zahajovat
již	již	k6eAd1	již
s	s	k7c7	s
bodovým	bodový	k2eAgInSc7d1	bodový
základem	základ	k1gInSc7	základ
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
s	s	k7c7	s
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
hráči	hráč	k1gMnPc1	hráč
sportovního	sportovní	k2eAgInSc2d1	sportovní
bowlingu	bowling	k1gInSc2	bowling
registrováni	registrovat	k5eAaBmNgMnP	registrovat
a	a	k8xC	a
licencováni	licencovat	k5eAaBmNgMnP	licencovat
u	u	k7c2	u
České	český	k2eAgFnSc2d1	Česká
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
asociace	asociace	k1gFnSc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
bowlingových	bowlingový	k2eAgNnPc2d1	bowlingové
uskupení	uskupení	k1gNnPc2	uskupení
Evropské	evropský	k2eAgFnSc2d1	Evropská
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
Světové	světový	k2eAgFnSc2d1	světová
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
asociace	asociace	k1gFnSc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
hráče	hráč	k1gMnPc4	hráč
světa	svět	k1gInSc2	svět
je	on	k3xPp3gNnSc4	on
pořádáno	pořádán	k2eAgNnSc4d1	pořádáno
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
bowlingu	bowling	k1gInSc6	bowling
(	(	kIx(	(
<g/>
označované	označovaný	k2eAgFnSc6d1	označovaná
"	"	kIx"	"
<g/>
QubicaAMF	QubicaAMF	k1gFnSc6	QubicaAMF
World	Worlda	k1gFnPc2	Worlda
Cup	cup	k1gInSc1	cup
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
mistrovství	mistrovství	k1gNnSc6	mistrovství
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
soutěže	soutěž	k1gFnPc1	soutěž
účastní	účastnit	k5eAaImIp3nP	účastnit
i	i	k9	i
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
turnaje	turnaj	k1gInSc2	turnaj
účastnit	účastnit	k5eAaImF	účastnit
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
reprezentant	reprezentant	k1gMnSc1	reprezentant
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgInSc2	první
turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
20	[number]	k4	20
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Lauri	Lauri	k1gNnSc2	Lauri
Ajanto	Ajant	k2eAgNnSc4d1	Ajant
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
vítězkou	vítězka	k1gFnSc7	vítězka
byla	být	k5eAaImAgFnS	být
Mexičanka	Mexičanka	k1gFnSc1	Mexičanka
Irma	Irma	k1gFnSc1	Irma
Urrea	Urrea	k1gFnSc1	Urrea
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
počet	počet	k1gInSc4	počet
účastníků	účastník	k1gMnPc2	účastník
narůstal	narůstat	k5eAaImAgMnS	narůstat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
81	[number]	k4	81
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
69	[number]	k4	69
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
bowlingové	bowlingový	k2eAgNnSc1d1	bowlingové
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
nepravidelně	pravidelně	k6eNd1	pravidelně
(	(	kIx(	(
<g/>
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
až	až	k8xS	až
pěti	pět	k4xCc6	pět
letech	let	k1gInPc6	let
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
první	první	k4xOgFnSc2	první
dekády	dekáda	k1gFnSc2	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
šampionát	šampionát	k1gInSc1	šampionát
koná	konat	k5eAaImIp3nS	konat
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
lichých	lichý	k2eAgInPc6d1	lichý
letech	let	k1gInPc6	let
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
mužským	mužský	k2eAgInSc7d1	mužský
turnajem	turnaj	k1gInSc7	turnaj
a	a	k8xC	a
v	v	k7c6	v
sudých	sudý	k2eAgNnPc6d1	sudé
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
turnajem	turnaj	k1gInSc7	turnaj
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
vítězem	vítěz	k1gMnSc7	vítěz
byl	být	k5eAaImAgMnS	být
Tauno	Tauno	k6eAd1	Tauno
Niemeinen	Niemeinen	k2eAgMnSc1d1	Niemeinen
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
vítězkou	vítězka	k1gFnSc7	vítězka
Maire	Mair	k1gInSc5	Mair
Rautakoski	Rautakosk	k1gMnSc3	Rautakosk
taktéž	taktéž	k?	taktéž
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
tohoto	tento	k3xDgInSc2	tento
turnaje	turnaj	k1gInSc2	turnaj
byly	být	k5eAaImAgInP	být
či	či	k8xC	či
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
pravidelností	pravidelnost	k1gFnSc7	pravidelnost
i	i	k8xC	i
délkou	délka	k1gFnSc7	délka
trvání	trvání	k1gNnSc2	trvání
pořádána	pořádán	k2eAgNnPc1d1	pořádáno
mistrovství	mistrovství	k1gNnSc4	mistrovství
týmů	tým	k1gInPc2	tým
o	o	k7c6	o
dvou	dva	k4xCgMnPc6	dva
<g/>
,	,	kIx,	,
třech	tři	k4xCgFnPc6	tři
<g/>
,	,	kIx,	,
čtyřech	čtyři	k4xCgMnPc6	čtyři
<g/>
,	,	kIx,	,
pěti	pět	k4xCc2	pět
<g/>
,	,	kIx,	,
šesti	šest	k4xCc2	šest
či	či	k8xC	či
osmi	osm	k4xCc2	osm
hráčích	hráč	k1gMnPc6	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Weber	weber	k1gInSc1	weber
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
soutěž	soutěž	k1gFnSc1	soutěž
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
Dicku	Dicek	k1gMnSc6	Dicek
Weberovi	Weber	k1gMnSc6	Weber
(	(	kIx(	(
<g/>
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hráčů	hráč	k1gMnPc2	hráč
bowlingu	bowling	k1gInSc2	bowling
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
členovi	člen	k1gMnSc3	člen
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klání	klání	k1gNnSc1	klání
podobné	podobný	k2eAgNnSc1d1	podobné
golfovému	golfový	k2eAgInSc3d1	golfový
Ryder	Ryder	k1gInSc4	Ryder
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
utkává	utkávat	k5eAaImIp3nS	utkávat
výběr	výběr	k1gInSc1	výběr
Spojených	spojený	k2eAgInPc2d1	spojený
státu	stát	k1gInSc2	stát
amerických	americký	k2eAgFnPc2d1	americká
proti	proti	k7c3	proti
výběru	výběr	k1gInSc3	výběr
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1	reprezentace
čítaly	čítat	k5eAaImAgInP	čítat
pět	pět	k4xCc4	pět
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
však	však	k9	však
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
<g/>
)	)	kIx)	)
a	a	k8xC	a
utkávají	utkávat	k5eAaImIp3nP	utkávat
se	se	k3xPyFc4	se
v	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
dvojic	dvojice	k1gFnPc2	dvojice
i	i	k8xC	i
celých	celý	k2eAgInPc2d1	celý
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
Varšavě	Varšava	k1gFnSc6	Varšava
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
pravidelně	pravidelně	k6eAd1	pravidelně
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
jeden	jeden	k4xCgInSc1	jeden
bod	bod	k1gInSc1	bod
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
sehráno	sehrát	k5eAaPmNgNnS	sehrát
35	[number]	k4	35
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vítězem	vítěz	k1gMnSc7	vítěz
byl	být	k5eAaImAgInS	být
ten	ten	k3xDgInSc1	ten
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dříve	dříve	k6eAd2	dříve
získal	získat	k5eAaPmAgInS	získat
18	[number]	k4	18
<g/>
.	.	kIx.	.
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
zápasů	zápas	k1gInPc2	zápas
snížen	snížen	k2eAgInSc4d1	snížen
na	na	k7c6	na
33	[number]	k4	33
a	a	k8xC	a
vítězí	vítězit	k5eAaImIp3nS	vítězit
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
dříve	dříve	k6eAd2	dříve
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
17	[number]	k4	17
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
této	tento	k3xDgFnSc2	tento
mety	meta	k1gFnSc2	meta
se	se	k3xPyFc4	se
již	již	k9	již
zbývající	zbývající	k2eAgInPc1d1	zbývající
zápasy	zápas	k1gInPc1	zápas
nedohrávají	dohrávat	k5eNaImIp3nP	dohrávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
hraje	hrát	k5eAaImIp3nS	hrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
hráči	hráč	k1gMnPc1	hráč
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Vítězi	vítěz	k1gMnSc3	vítěz
byli	být	k5eAaImAgMnP	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Trčka	Trčka	k1gMnSc1	Trčka
starší	starší	k1gMnSc1	starší
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
Irena	Irena	k1gFnSc1	Irena
Kolářová	Kolářová	k1gFnSc1	Kolářová
mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
týmu	tým	k1gInSc2	tým
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
BC	BC	kA	BC
Děčín	Děčín	k1gInSc1	Děčín
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
počet	počet	k1gInSc1	počet
kategorií	kategorie	k1gFnPc2	kategorie
narůstal	narůstat	k5eAaImAgInS	narůstat
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
juniory	junior	k1gMnPc4	junior
do	do	k7c2	do
devíti	devět	k4xCc2	devět
<g/>
,	,	kIx,	,
dvanácti	dvanáct	k4xCc2	dvanáct
<g/>
,	,	kIx,	,
patnácti	patnáct	k4xCc2	patnáct
a	a	k8xC	a
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
kadety	kadet	k1gMnPc4	kadet
<g/>
,	,	kIx,	,
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
,	,	kIx,	,
dvojice	dvojice	k1gFnPc4	dvojice
<g/>
,	,	kIx,	,
družstva	družstvo	k1gNnPc4	družstvo
a	a	k8xC	a
seniory	senior	k1gMnPc4	senior
rozdělené	rozdělený	k2eAgFnSc2d1	rozdělená
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
ještě	ještě	k9	ještě
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
pohlaví	pohlaví	k1gNnSc4	pohlaví
a	a	k8xC	a
u	u	k7c2	u
dvojic	dvojice	k1gFnPc2	dvojice
navíc	navíc	k6eAd1	navíc
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
mixu	mix	k1gInSc6	mix
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
mistrovství	mistrovství	k1gNnSc2	mistrovství
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hraje	hrát	k5eAaImIp3nS	hrát
ještě	ještě	k9	ještě
Bowlingová	Bowlingový	k2eAgFnSc1d1	Bowlingová
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
družstvům	družstvo	k1gNnPc3	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
extraliga	extraliga	k1gFnSc1	extraliga
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
I.	I.	kA	I.
liga	liga	k1gFnSc1	liga
a	a	k8xC	a
divize	divize	k1gFnSc1	divize
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
hierarchii	hierarchie	k1gFnSc6	hierarchie
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
3	[number]	k4	3
<g/>
.	.	kIx.	.
regionální	regionální	k2eAgFnSc1d1	regionální
bowlingová	bowlingový	k2eAgFnSc1d1	bowlingová
liga	liga	k1gFnSc1	liga
a	a	k8xC	a
případně	případně	k6eAd1	případně
též	též	k9	též
ještě	ještě	k6eAd1	ještě
místní	místní	k2eAgFnSc1d1	místní
bowlingová	bowlingový	k2eAgFnSc1d1	bowlingová
liga	liga	k1gFnSc1	liga
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
regionálních	regionální	k2eAgFnPc2d1	regionální
a	a	k8xC	a
místních	místní	k2eAgFnPc2d1	místní
lig	liga	k1gFnPc2	liga
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
celků	celek	k1gInPc2	celek
z	z	k7c2	z
dané	daný	k2eAgFnSc2d1	daná
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
soutěží	soutěž	k1gFnSc7	soutěž
chtějí	chtít	k5eAaImIp3nP	chtít
účastnit	účastnit	k5eAaImF	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ligami	liga	k1gFnPc7	liga
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
postup	postup	k1gInSc1	postup
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
vyšší	vysoký	k2eAgInSc4d2	vyšší
i	i	k8xC	i
sestup	sestup	k1gInSc4	sestup
směrem	směr	k1gInSc7	směr
opačným	opačný	k2eAgInSc7d1	opačný
<g/>
.	.	kIx.	.
</s>
<s>
Junioři	junior	k1gMnPc1	junior
hrají	hrát	k5eAaImIp3nP	hrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
2	[number]	k4	2
<g/>
.	.	kIx.	.
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rekreační	rekreační	k2eAgMnPc4d1	rekreační
hráče	hráč	k1gMnPc4	hráč
bowlingu	bowling	k1gInSc2	bowling
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
bowlingová	bowlingový	k2eAgFnSc1d1	bowlingová
liga	liga	k1gFnSc1	liga
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
výkonnostních	výkonnostní	k2eAgInPc2d1	výkonnostní
stupňů	stupeň	k1gInPc2	stupeň
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
přebory	přebor	k1gInPc1	přebor
a	a	k8xC	a
divize	divize	k1gFnSc1	divize
<g/>
)	)	kIx)	)
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
jedno	jeden	k4xCgNnSc4	jeden
pololetí	pololetí	k1gNnSc4	pololetí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jarním	jarní	k2eAgNnSc6d1	jarní
kole	kolo	k1gNnSc6	kolo
je	být	k5eAaImIp3nS	být
soutěž	soutěž	k1gFnSc1	soutěž
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
podzimní	podzimní	k2eAgNnSc1d1	podzimní
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
soutěží	soutěž	k1gFnSc7	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Družstva	družstvo	k1gNnPc1	družstvo
hrají	hrát	k5eAaImIp3nP	hrát
své	svůj	k3xOyFgInPc4	svůj
zápasy	zápas	k1gInPc4	zápas
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
pocházejí	pocházet	k5eAaImIp3nP	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
utkávají	utkávat	k5eAaImIp3nP	utkávat
v	v	k7c6	v
mistrovství	mistrovství	k1gNnSc6	mistrovství
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
