<s>
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
[	[	kIx(	[
<g/>
ʒ	ʒ	k?	ʒ
<g/>
̃	̃	k?	̃
pɔ	pɔ	k?	pɔ
saʁ	saʁ	k?	saʁ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
[	[	kIx(	[
<g/>
žán	žán	k?	žán
pol	pola	k1gFnPc2	pola
sártr	sártr	k1gMnSc1	sártr
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1905	[number]	k4	1905
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
.	.	kIx.	.
</s>
