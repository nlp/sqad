<s>
InChI	InChI	k?
</s>
<s>
InChI	InChI	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
anglický	anglický	k2eAgInSc4d1
termín	termín	k1gInSc4
IUPAC	IUPAC	kA
International	International	k1gMnSc2
Chemical	Chemical	k1gMnSc1
Identifier	Identifier	k1gMnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
česky	česky	k6eAd1
„	„	k?
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1
chemický	chemický	k2eAgInSc1d1
identifikátor	identifikátor	k1gInSc1
IUPAC	IUPAC	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
textový	textový	k2eAgInSc4d1
identifikátor	identifikátor	k1gInSc4
chemických	chemický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
navržený	navržený	k2eAgInSc4d1
jako	jako	k8xC,k8xS
standardní	standardní	k2eAgInSc4d1
a	a	k8xC
lidsky	lidsky	k6eAd1
čitelný	čitelný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
kódovat	kódovat	k5eAaBmF
informace	informace	k1gFnPc4
o	o	k7c6
molekulách	molekula	k1gFnPc6
a	a	k8xC
jak	jak	k6eAd1
umožnit	umožnit	k5eAaPmF
vyhledávání	vyhledávání	k1gNnSc4
takových	takový	k3xDgFnPc2
informací	informace	k1gFnPc2
v	v	k7c6
databázích	databáze	k1gFnPc6
a	a	k8xC
na	na	k7c6
webu	web	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
InChI	InChI	k1gFnSc2
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
IUPAC	IUPAC	kA
a	a	k8xC
NIST	NIST	kA
v	v	k7c6
období	období	k1gNnSc6
let	léto	k1gNnPc2
2000	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
formát	formát	k1gInSc1
a	a	k8xC
algoritmy	algoritmus	k1gInPc1
jsou	být	k5eAaImIp3nP
otevřené	otevřený	k2eAgFnPc4d1
<g/>
,	,	kIx,
software	software	k1gInSc1
je	být	k5eAaImIp3nS
volně	volně	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pod	pod	k7c7
licencí	licence	k1gFnSc7
GNU	gnu	k1gMnSc1
LGPL	LGPL	kA
(	(	kIx(
<g/>
byť	byť	k8xS
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
„	„	k?
<g/>
InChI	InChI	k1gFnSc1
<g/>
“	“	k?
ochrannou	ochranný	k2eAgFnSc7d1
známkou	známka	k1gFnSc7
IUPAC	IUPAC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
International	International	k1gMnSc1
Chemical	Chemical	k1gMnSc1
Identifier	Identifier	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
MCNAUGHT	MCNAUGHT	kA
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
IUPAC	IUPAC	kA
International	International	k1gFnPc1
Chemical	Chemical	k1gMnSc1
Identifier	Identifier	k1gMnSc1
<g/>
:	:	kIx,
<g/>
InChl	InChl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemistry	Chemistr	k1gMnPc7
International	International	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUPAC	IUPAC	kA
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Wikidata	Wikide	k1gNnPc1
používají	používat	k5eAaImIp3nP
InChI	InChI	k1gFnPc1
jako	jako	k8xC,k8xS
vlastnost	vlastnost	k1gFnSc1
P234	P234	k1gFnSc1
(	(	kIx(
<g/>
použití	použití	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Dokumentace	dokumentace	k1gFnSc1
a	a	k8xC
prezentace	prezentace	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
IUPAC	IUPAC	kA
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
InChI	InChI	k1gFnSc6
</s>
<s>
InChI	InChI	k?
Technical	Technical	k1gMnSc1
Manual	Manual	k1gMnSc1
</s>
<s>
Description	Description	k1gInSc1
of	of	k?
the	the	k?
canonicalization	canonicalization	k1gInSc1
algorithm	algorithm	k6eAd1
</s>
<s>
Googling	Googling	k1gInSc1
for	forum	k1gNnPc2
InChIs	InChIs	k1gInSc4
a	a	k8xC
presentation	presentation	k1gInSc4
to	ten	k3xDgNnSc1
the	the	k?
W	W	kA
<g/>
3	#num#	k4
<g/>
C.	C.	kA
</s>
<s>
The	The	k?
Semantic	Semantice	k1gFnPc2
Chemical	Chemical	k1gFnSc1
Web	web	k1gInSc1
<g/>
:	:	kIx,
GoogleInChI	GoogleInChI	k1gFnSc1
and	and	k?
other	other	k1gInSc1
Mashups	Mashups	k1gInSc1
<g/>
,	,	kIx,
Google	Google	k1gInSc1
Tech	Tech	k?
Talk	Talk	k1gInSc4
by	by	kYmCp3nS
Peter	Peter	k1gMnSc1
Murray-Rust	Murray-Rust	k1gMnSc1
<g/>
,	,	kIx,
13	#num#	k4
Sept	septum	k1gNnPc2
2006	#num#	k4
</s>
<s>
IUPAC	IUPAC	kA
InChI	InChI	k1gFnSc1
<g/>
,	,	kIx,
Google	Google	k1gFnSc1
Tech	Tech	k?
Talk	Talk	k1gInSc4
by	by	kYmCp3nS
Steve	Steve	k1gMnSc1
Heller	Heller	k1gMnSc1
and	and	k?
Steve	Steve	k1gMnSc1
Stein	Stein	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
November	November	k1gInSc1
2006	#num#	k4
</s>
<s>
InChI	InChI	k?
Release	Releas	k1gInSc6
1.02	1.02	k4
InChI	InChI	k1gFnSc1
final	finat	k5eAaImAgInS,k5eAaBmAgInS,k5eAaPmAgInS
version	version	k1gInSc1
1.02	1.02	k4
and	and	k?
explanation	explanation	k1gInSc1
of	of	k?
Standard	standard	k1gInSc1
InChi	InCh	k1gFnSc2
<g/>
,	,	kIx,
January	Januara	k1gFnSc2
2009	#num#	k4
</s>
<s>
Software	software	k1gInSc1
a	a	k8xC
služby	služba	k1gFnPc1
</s>
<s>
NCI	NCI	kA
<g/>
/	/	kIx~
<g/>
CADD	CADD	kA
Chemical	Chemical	k1gMnSc1
Identifier	Identifier	k1gMnSc1
Resolver	Resolver	k1gMnSc1
Generates	Generates	k1gMnSc1
and	and	k?
resolves	resolves	k1gMnSc1
InChI	InChI	k1gMnSc1
<g/>
/	/	kIx~
<g/>
InChIKeys	InChIKeys	k1gInSc1
and	and	k?
many	mana	k1gFnSc2
other	other	k1gMnSc1
chemical	chemicat	k5eAaPmAgMnS
identifiers	identifiers	k6eAd1
</s>
<s>
Generate	Generat	k1gMnSc5
InChI	InChI	k1gMnSc5
(	(	kIx(
<g/>
interactive	interactiv	k1gInSc5
service	service	k1gFnSc2
at	at	k?
University	universita	k1gFnSc2
of	of	k?
Cambridge	Cambridge	k1gFnSc2
<g/>
,	,	kIx,
either	eithra	k1gFnPc2
interactive	interactiv	k1gInSc5
or	or	k?
WSDL	WSDL	kA
<g/>
)	)	kIx)
</s>
<s>
Search	Search	k1gMnSc1
Google	Google	k1gFnSc2
for	forum	k1gNnPc2
molecules	molecules	k1gMnSc1
(	(	kIx(
<g/>
generates	generates	k1gMnSc1
InChI	InChI	k1gMnSc1
from	from	k1gMnSc1
interactive	interactiv	k1gInSc5
chemical	chemicat	k5eAaPmAgMnS
and	and	k?
searches	searches	k1gMnSc1
Google	Google	k1gNnSc2
for	forum	k1gNnPc2
any	any	k?
pages	pages	k1gMnSc1
with	with	k1gMnSc1
embedded	embedded	k1gMnSc1
InChIs	InChIsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Requires	Requires	k1gInSc1
Javascript	Javascript	k2eAgInSc4d1
enabled	enabled	k1gInSc4
on	on	k3xPp3gMnSc1
browser	browser	k1gMnSc1
</s>
<s>
ChemSketch	ChemSketch	k1gInSc1
<g/>
,	,	kIx,
free	free	k6eAd1
chemical	chemicat	k5eAaPmAgMnS
structure	structur	k1gMnSc5
drawing	drawing	k1gInSc1
package	package	k1gNnSc1
that	that	k1gInSc1
includes	includes	k1gMnSc1
input	input	k1gMnSc1
and	and	k?
output	output	k1gMnSc1
in	in	k?
InCHI	InCHI	k1gMnSc1
format	format	k5eAaPmF
</s>
<s>
PubChem	PubCh	k1gInSc7
online	onlinout	k5eAaPmIp3nS
molecule	molecule	k1gFnSc1
editor	editor	k1gInSc4
that	that	k2eAgInSc1d1
supports	supports	k1gInSc1
SMILES	SMILES	kA
<g/>
/	/	kIx~
<g/>
SMARTS	SMARTS	kA
and	and	k?
InChI	InChI	k1gMnSc1
</s>
<s>
ChemSpider	ChemSpider	k1gMnSc1
Services	Services	k1gMnSc1
that	that	k1gMnSc1
allows	allowsa	k1gFnPc2
generation	generation	k1gInSc4
of	of	k?
InChI	InChI	k1gFnSc2
and	and	k?
conversion	conversion	k1gInSc1
of	of	k?
InChI	InChI	k1gFnSc2
to	ten	k3xDgNnSc1
structure	structur	k1gMnSc5
(	(	kIx(
<g/>
also	also	k1gMnSc1
SMILES	SMILES	kA
and	and	k?
generation	generation	k1gInSc1
of	of	k?
other	other	k1gInSc1
properties	properties	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
MarvinSketch	MarvinSketch	k1gMnSc1
from	from	k1gMnSc1
ChemAxon	ChemAxon	k1gMnSc1
<g/>
,	,	kIx,
implementation	implementation	k1gInSc1
to	ten	k3xDgNnSc1
draw	draw	k?
structures	structures	k1gInSc1
(	(	kIx(
<g/>
or	or	k?
open	open	k1gMnSc1
other	other	k1gMnSc1
file	filat	k5eAaPmIp3nS
formats	formats	k6eAd1
<g/>
)	)	kIx)
and	and	k?
output	output	k1gInSc1
to	ten	k3xDgNnSc4
InChI	InChI	k1gFnPc3
file	file	k6eAd1
format	format	k5eAaPmF
</s>
<s>
BKchem	BKch	k1gInSc7
Archivováno	archivován	k2eAgNnSc4d1
9	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
implements	implementsa	k1gFnPc2
its	its	k?
own	own	k?
InChI	InChI	k1gMnSc1
parser	parser	k1gMnSc1
and	and	k?
uses	uses	k1gInSc1
the	the	k?
IUPAC	IUPAC	kA
implementation	implementation	k1gInSc1
to	ten	k3xDgNnSc1
generate	generat	k1gInSc5
InChI	InChI	k1gMnSc4
strings	strings	k1gInSc1
</s>
