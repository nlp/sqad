<s>
InChI	InChI	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
anglický	anglický	k2eAgInSc4d1
termín	termín	k1gInSc4
IUPAC	IUPAC	kA
International	International	k1gMnSc2
Chemical	Chemical	k1gMnSc1
Identifier	Identifier	k1gMnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
česky	česky	k6eAd1
„	„	k?
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1
chemický	chemický	k2eAgInSc1d1
identifikátor	identifikátor	k1gInSc1
IUPAC	IUPAC	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>