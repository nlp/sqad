<s>
Turingovská	Turingovský	k2eAgFnSc1d1	Turingovský
úplnost	úplnost	k1gFnSc1	úplnost
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
teorie	teorie	k1gFnSc2	teorie
vyčíslitelnosti	vyčíslitelnost	k1gFnSc2	vyčíslitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
příbuzného	příbuzný	k2eAgInSc2d1	příbuzný
pojmu	pojem	k1gInSc2	pojem
NP-úplnost	NP-úplnost	k1gFnSc1	NP-úplnost
<g/>
.	.	kIx.	.
</s>
<s>
Turingovsky	Turingovsky	k6eAd1	Turingovsky
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
,	,	kIx,	,
turingovsky	turingovsky	k6eAd1	turingovsky
úplný	úplný	k2eAgInSc1d1	úplný
nebo	nebo	k8xC	nebo
turingovsky	turingovsky	k6eAd1	turingovsky
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Turing-complete	Turingomple	k1gNnSc2	Turing-comple
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stroj	stroj	k1gInSc1	stroj
(	(	kIx(	(
<g/>
počítač	počítač	k1gInSc1	počítač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
úloha	úloha	k1gFnSc1	úloha
nebo	nebo	k8xC	nebo
abstraktní	abstraktní	k2eAgInSc1d1	abstraktní
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
sílu	síla	k1gFnSc4	síla
jako	jako	k8xS	jako
Turingův	Turingův	k2eAgInSc4d1	Turingův
stroj	stroj	k1gInSc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
třída	třída	k1gFnSc1	třída
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
žádný	žádný	k3yNgInSc4	žádný
způsob	způsob	k1gInSc4	způsob
řešení	řešení	k1gNnSc4	řešení
úlohy	úloha	k1gFnSc2	úloha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgFnSc1d2	těžší
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Churchovy-Turingovy	Churchovy-Turingův	k2eAgFnSc2d1	Churchovy-Turingův
teze	teze	k1gFnSc2	teze
žádné	žádný	k3yNgNnSc4	žádný
konečné	konečný	k2eAgNnSc4d1	konečné
zařízení	zařízení	k1gNnSc4	zařízení
víc	hodně	k6eAd2	hodně
spočítat	spočítat	k5eAaPmF	spočítat
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
turingovsky	turingovsky	k6eAd1	turingovsky
kompletní	kompletní	k2eAgInSc1d1	kompletní
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
univerzální	univerzální	k2eAgNnSc1d1	univerzální
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
možné	možný	k2eAgNnSc1d1	možné
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
neříká	říkat	k5eNaImIp3nS	říkat
nic	nic	k3yNnSc1	nic
o	o	k7c6	o
efektivitě	efektivita	k1gFnSc6	efektivita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
lze	lze	k6eAd1	lze
sestrojit	sestrojit	k5eAaPmF	sestrojit
univerzální	univerzální	k2eAgInSc4d1	univerzální
Turingův	Turingův	k2eAgInSc4d1	Turingův
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
simulovat	simulovat	k5eAaImF	simulovat
libovolný	libovolný	k2eAgInSc4d1	libovolný
jiný	jiný	k2eAgInSc4d1	jiný
Turingův	Turingův	k2eAgInSc4d1	Turingův
stroj	stroj	k1gInSc4	stroj
(	(	kIx(	(
<g/>
zadaný	zadaný	k2eAgInSc4d1	zadaný
na	na	k7c6	na
vstupu	vstup	k1gInSc6	vstup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gMnPc4	jazyk
přijímané	přijímaný	k2eAgMnPc4d1	přijímaný
Turingovým	Turingový	k2eAgInSc7d1	Turingový
strojem	stroj	k1gInSc7	stroj
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
rekurzivně	rekurzivně	k6eAd1	rekurzivně
spočetné	spočetný	k2eAgInPc1d1	spočetný
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Gramatiky	gramatika	k1gFnPc1	gramatika
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
generují	generovat	k5eAaImIp3nP	generovat
tyto	tento	k3xDgInPc1	tento
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchie	hierarchie	k1gFnSc2	hierarchie
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
gramatiky	gramatika	k1gFnSc2	gramatika
typu	typ	k1gInSc2	typ
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nelze	lze	k6eNd1	lze
řešit	řešit	k5eAaImF	řešit
na	na	k7c6	na
Turingově	Turingově	k1gFnSc6	Turingově
stroji	stroj	k1gInSc6	stroj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc1	problém
zastavení	zastavení	k1gNnPc2	zastavení
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
halting	halting	k1gInSc1	halting
problem	probl	k1gMnSc7	probl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
problém	problém	k1gInSc1	problém
zastavení	zastavení	k1gNnSc2	zastavení
Turingova	Turingův	k2eAgInSc2d1	Turingův
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
demonstrací	demonstrace	k1gFnPc2	demonstrace
neuzavřenosti	neuzavřenost	k1gFnSc2	neuzavřenost
třídy	třída	k1gFnSc2	třída
rekurzivně	rekurzivně	k6eAd1	rekurzivně
spočetných	spočetný	k2eAgInPc2d1	spočetný
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c4	na
doplněk	doplněk	k1gInSc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
vyčíslitelnosti	vyčíslitelnost	k1gFnSc2	vyčíslitelnost
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
teoretickém	teoretický	k2eAgInSc6d1	teoretický
výzkumu	výzkum	k1gInSc6	výzkum
k	k	k7c3	k
ještě	ještě	k6eAd1	ještě
složitějším	složitý	k2eAgInPc3d2	složitější
problémům	problém	k1gInPc3	problém
–	–	k?	–
aritmetická	aritmetický	k2eAgFnSc1d1	aritmetická
hierarchie	hierarchie	k1gFnSc1	hierarchie
je	být	k5eAaImIp3nS	být
posloupnost	posloupnost	k1gFnSc4	posloupnost
tříd	třída	k1gFnPc2	třída
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
nultý	nultý	k4xOgInSc1	nultý
stupeň	stupeň	k1gInSc1	stupeň
jsou	být	k5eAaImIp3nP	být
rekurzivně	rekurzivně	k6eAd1	rekurzivně
spočetné	spočetný	k2eAgInPc1d1	spočetný
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
stupni	stupeň	k1gInSc6	stupeň
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
turingovsky	turingovsky	k6eAd1	turingovsky
převoditelné	převoditelný	k2eAgNnSc4d1	převoditelné
a	a	k8xC	a
problém	problém	k1gInSc4	problém
zastavení	zastavení	k1gNnPc2	zastavení
pro	pro	k7c4	pro
stupeň	stupeň	k1gInSc4	stupeň
N	N	kA	N
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stupni	stupeň	k1gInSc6	stupeň
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Churchova-Turingova	Churchova-Turingův	k2eAgFnSc1d1	Churchova-Turingův
teze	teze	k1gFnSc1	teze
Turingův	Turingův	k2eAgInSc4d1	Turingův
stroj	stroj	k1gInSc1	stroj
</s>
