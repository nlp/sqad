<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
její	její	k3xOp3gMnSc1	její
první	první	k4xOgMnSc1	první
předseda	předseda	k1gMnSc1	předseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgNnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
období	období	k1gNnSc4	období
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
