<s>
Protože	protože	k8xS	protože
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
mechanicky	mechanicky	k6eAd1	mechanicky
odstranit	odstranit	k5eAaPmF	odstranit
zbývající	zbývající	k2eAgFnSc4d1	zbývající
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
papírovém	papírový	k2eAgInSc6d1	papírový
listu	list	k1gInSc6	list
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
papírový	papírový	k2eAgInSc1d1	papírový
list	list	k1gInSc1	list
sušit	sušit	k5eAaImF	sušit
na	na	k7c6	na
válcích	válec	k1gInPc6	válec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
vytápěny	vytápět	k5eAaImNgInP	vytápět
párou	pára	k1gFnSc7	pára
<g/>
.	.	kIx.	.
</s>
