<p>
<s>
Guariento	Guariento	k1gNnSc1	Guariento
di	di	k?	di
Arpo	Arpo	k6eAd1	Arpo
<g/>
,	,	kIx,	,
nesprávně	správně	k6eNd1	správně
někdy	někdy	k6eAd1	někdy
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xC	jako
Guerriero	Guerriero	k1gNnSc4	Guerriero
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
1310	[number]	k4	1310
<g/>
–	–	k?	–
<g/>
1370	[number]	k4	1370
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
gotický	gotický	k2eAgMnSc1d1	gotický
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
příjmení	příjmení	k1gNnSc1	příjmení
di	di	k?	di
Arpo	Arpo	k1gNnSc1	Arpo
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
Piove	Pioev	k1gFnSc2	Pioev
di	di	k?	di
Sacco	Sacco	k1gMnSc1	Sacco
<g/>
.	.	kIx.	.
</s>
<s>
Umělec	umělec	k1gMnSc1	umělec
pracoval	pracovat	k5eAaImAgMnS	pracovat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1338	[number]	k4	1338
<g/>
–	–	k?	–
<g/>
1367	[number]	k4	1367
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
pro	pro	k7c4	pro
pány	pan	k1gMnPc4	pan
z	z	k7c2	z
Carrary	Carrara	k1gFnSc2	Carrara
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
a	a	k8xC	a
Bolzanu	Bolzan	k1gInSc6	Bolzan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1338	[number]	k4	1338
namaloval	namalovat	k5eAaPmAgInS	namalovat
fresky	freska	k1gFnSc2	freska
v	v	k7c6	v
Kostele	kostel	k1gInSc6	kostel
poustevníků	poustevník	k1gMnPc2	poustevník
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
(	(	kIx(	(
<g/>
Chiesa	Chiesa	k1gFnSc1	Chiesa
degli	degnout	k5eAaImAgMnP	degnout
Eremitani	Eremitan	k1gMnPc1	Eremitan
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1340	[number]	k4	1340
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
je	být	k5eAaImIp3nS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
pro	pro	k7c4	pro
padovský	padovský	k2eAgInSc4d1	padovský
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
fresku	freska	k1gFnSc4	freska
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
(	(	kIx(	(
<g/>
hrobka	hrobka	k1gFnSc1	hrobka
Ubertino	Ubertin	k2eAgNnSc1d1	Ubertino
a	a	k8xC	a
Jacopo	Jacopa	k1gFnSc5	Jacopa
I.	I.	kA	I.
Da	Da	k1gMnSc2	Da
Carrara	Carrar	k1gMnSc2	Carrar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1357	[number]	k4	1357
vyzdobil	vyzdobit	k5eAaPmAgInS	vyzdobit
freskami	freska	k1gFnPc7	freska
soukromou	soukromý	k2eAgFnSc4d1	soukromá
kapli	kaple	k1gFnSc4	kaple
a	a	k8xC	a
lodžii	lodžie	k1gFnSc4	lodžie
Královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Carrary	Carrara	k1gFnSc2	Carrara
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
(	(	kIx(	(
<g/>
Palazzo	Palazza	k1gFnSc5	Palazza
Carrara	Carrar	k1gMnSc4	Carrar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1361	[number]	k4	1361
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
apsidě	apsida	k1gFnSc6	apsida
baziliky	bazilika	k1gFnSc2	bazilika
svatých	svatý	k1gMnPc2	svatý
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
hrob	hrob	k1gInSc1	hrob
dóžete	dóže	k1gMnSc2	dóže
Dolfina	Dolfin	k1gMnSc2	Dolfin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
dóže	dóže	k1gMnSc2	dóže
Marco	Marco	k1gMnSc1	Marco
Corner	Corner	k1gMnSc1	Corner
objednal	objednat	k5eAaPmAgMnS	objednat
fresku	freska	k1gFnSc4	freska
pro	pro	k7c4	pro
sněmovní	sněmovní	k2eAgFnPc4d1	sněmovní
místnosti	místnost	k1gFnPc4	místnost
Velké	velký	k2eAgFnSc2d1	velká
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
Dóžecím	dóžecí	k2eAgInSc6d1	dóžecí
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
fresky	freska	k1gFnPc4	freska
v	v	k7c6	v
kněžišti	kněžiště	k1gNnSc6	kněžiště
a	a	k8xC	a
apsidě	apsida	k1gFnSc6	apsida
kostela	kostel	k1gInSc2	kostel
Poustevníků	poustevník	k1gMnPc2	poustevník
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
San	San	k1gFnSc1	San
Bernardino	Bernardin	k2eAgNnSc4d1	Bernardino
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žákem	Žák	k1gMnSc7	Žák
a	a	k8xC	a
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Guarienta	Guariento	k1gNnSc2	Guariento
di	di	k?	di
Arpo	Arpo	k6eAd1	Arpo
byl	být	k5eAaImAgMnS	být
benátský	benátský	k2eAgMnSc1d1	benátský
malíř	malíř	k1gMnSc1	malíř
Niccolò	Niccolò	k1gMnSc1	Niccolò
Semitecolo	Semitecola	k1gFnSc5	Semitecola
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
návrhu	návrh	k1gInSc2	návrh
mozaiky	mozaika	k1gFnSc2	mozaika
Posledního	poslední	k2eAgInSc2d1	poslední
soudu	soud	k1gInSc2	soud
na	na	k7c6	na
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
bráně	brána	k1gFnSc6	brána
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Guarienta	Guariento	k1gNnSc2	Guariento
di	di	k?	di
Arpo	Arpo	k1gMnSc1	Arpo
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
Paola	Paol	k1gMnSc2	Paol
Veneziana	Venezian	k1gMnSc2	Venezian
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přítomná	přítomný	k2eAgFnSc1d1	přítomná
benátská	benátský	k2eAgFnSc1d1	Benátská
byzantská	byzantský	k2eAgFnSc1d1	byzantská
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
patrná	patrný	k2eAgFnSc1d1	patrná
v	v	k7c4	v
bohatství	bohatství	k1gNnSc4	bohatství
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
,	,	kIx,	,
hieratickém	hieratický	k2eAgNnSc6d1	hieratické
uspořádání	uspořádání	k1gNnSc6	uspořádání
figur	figura	k1gFnPc2	figura
<g/>
,	,	kIx,	,
kaligrafické	kaligrafický	k2eAgFnSc3d1	kaligrafická
kresbě	kresba	k1gFnSc3	kresba
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
typologii	typologie	k1gFnSc4	typologie
tváří	tvář	k1gFnPc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
objevují	objevovat	k5eAaImIp3nP	objevovat
realistické	realistický	k2eAgInPc1d1	realistický
prvky	prvek	k1gInPc1	prvek
v	v	k7c6	v
prostorové	prostorový	k2eAgFnSc6d1	prostorová
struktuře	struktura	k1gFnSc6	struktura
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
individualizaci	individualizace	k1gFnSc6	individualizace
portrétů	portrét	k1gInPc2	portrét
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
figury	figura	k1gFnPc1	figura
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgInPc1d1	vycházející
z	z	k7c2	z
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
gotických	gotický	k2eAgInPc2d1	gotický
vzorů	vzor	k1gInPc2	vzor
a	a	k8xC	a
středověké	středověký	k2eAgFnSc2d1	středověká
angelologie	angelologie	k1gFnSc2	angelologie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
protáhlé	protáhlý	k2eAgInPc1d1	protáhlý
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
s	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
bohatě	bohatě	k6eAd1	bohatě
řasenými	řasený	k2eAgInPc7d1	řasený
oděvy	oděv	k1gInPc7	oděv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
plasticita	plasticita	k1gFnSc1	plasticita
již	již	k6eAd1	již
odráží	odrážet	k5eAaImIp3nS	odrážet
vliv	vliv	k1gInSc4	vliv
Giottovy	Giottův	k2eAgFnSc2d1	Giottova
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Guarientových	Guarientův	k2eAgFnPc6d1	Guarientův
freskách	freska	k1gFnPc6	freska
ze	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
padovském	padovský	k2eAgInSc6d1	padovský
Kostele	kostel	k1gInSc6	kostel
poustevníků	poustevník	k1gMnPc2	poustevník
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
narativních	narativní	k2eAgFnPc6d1	narativní
scénách	scéna	k1gFnPc6	scéna
s	s	k7c7	s
životy	život	k1gInPc1	život
svatých	svatá	k1gFnPc2	svatá
realisticky	realisticky	k6eAd1	realisticky
zachycené	zachycený	k2eAgInPc1d1	zachycený
výhledy	výhled	k1gInPc1	výhled
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Nevzdaluje	vzdalovat	k5eNaImIp3nS	vzdalovat
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
tradičním	tradiční	k2eAgNnPc3d1	tradiční
gotickým	gotický	k2eAgNnPc3d1	gotické
schématům	schéma	k1gNnPc3	schéma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
kresby	kresba	k1gFnSc2	kresba
a	a	k8xC	a
barevnosti	barevnost	k1gFnSc2	barevnost
jsou	být	k5eAaImIp3nP	být
zdůrazněny	zdůrazněn	k2eAgInPc1d1	zdůrazněn
dramatické	dramatický	k2eAgInPc1d1	dramatický
aspekty	aspekt	k1gInPc1	aspekt
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stylizací	stylizace	k1gFnSc7	stylizace
mužských	mužský	k2eAgFnPc2d1	mužská
hlav	hlava	k1gFnPc2	hlava
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Guarienta	Guarient	k1gMnSc4	Guarient
di	di	k?	di
Arpo	Arpo	k1gMnSc1	Arpo
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
žáka	žák	k1gMnSc2	žák
Niccolò	Niccolò	k1gMnSc2	Niccolò
Semitecola	Semitecola	k1gFnSc1	Semitecola
autor	autor	k1gMnSc1	autor
fresek	freska	k1gFnPc2	freska
v	v	k7c6	v
ambitu	ambit	k1gInSc6	ambit
kláštera	klášter	k1gInSc2	klášter
Na	na	k7c6	na
Slovanech	Slovan	k1gInPc6	Slovan
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xC	jako
Mistr	mistr	k1gMnSc1	mistr
emauzského	emauzský	k2eAgInSc2d1	emauzský
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Známá	známý	k2eAgNnPc1d1	známé
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
1332	[number]	k4	1332
Krucifix	krucifix	k1gInSc1	krucifix
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
v	v	k7c4	v
Museo	Museo	k1gNnSc4	Museo
Civico	Civico	k1gMnSc1	Civico
de	de	k?	de
Bassano	Bassana	k1gFnSc5	Bassana
del	del	k?	del
Grappa	Grappa	k1gFnSc1	Grappa
</s>
</p>
<p>
<s>
1338	[number]	k4	1338
Fresky	freska	k1gFnPc1	freska
se	s	k7c7	s
scénami	scéna	k1gFnPc7	scéna
ze	z	k7c2	z
života	život	k1gInSc2	život
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
,	,	kIx,	,
chór	chór	k1gInSc1	chór
Kostela	kostel	k1gInSc2	kostel
poustevníků	poustevník	k1gMnPc2	poustevník
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
fresky	freska	k1gFnPc1	freska
byly	být	k5eAaImAgFnP	být
těžce	těžce	k6eAd1	těžce
poškozeny	poškozen	k2eAgFnPc1d1	poškozena
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1344	[number]	k4	1344
Nanebevstoupení	nanebevstoupení	k1gNnSc1	nanebevstoupení
Páně	páně	k2eAgInPc1d1	páně
<g/>
,	,	kIx,	,
Collection	Collection	k1gInSc4	Collection
Vittorio	Vittorio	k6eAd1	Vittorio
Cini	Cin	k1gFnPc1	Cin
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
</s>
</p>
<p>
<s>
1344	[number]	k4	1344
Deskový	deskový	k2eAgInSc1d1	deskový
oltář	oltář	k1gInSc1	oltář
s	s	k7c7	s
korunovací	korunovace	k1gFnSc7	korunovace
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
katedrálu	katedrála	k1gFnSc4	katedrála
v	v	k7c4	v
Piove	Pioev	k1gFnPc4	Pioev
di	di	k?	di
Sacco	Sacco	k1gNnSc1	Sacco
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
v	v	k7c4	v
Norton	Norton	k1gInSc4	Norton
Simon	Simona	k1gFnPc2	Simona
Museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
1345	[number]	k4	1345
Madonna	Madonn	k1gInSc2	Madonn
humilitas	humilitas	k1gMnSc1	humilitas
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Getty	Getta	k1gMnSc2	Getta
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
Malibu	Maliba	k1gFnSc4	Maliba
</s>
</p>
<p>
<s>
1351	[number]	k4	1351
Fresky	freska	k1gFnPc4	freska
pro	pro	k7c4	pro
hrobku	hrobka	k1gFnSc4	hrobka
Ubertino	Ubertin	k2eAgNnSc1d1	Ubertino
a	a	k8xC	a
Jacopo	Jacopa	k1gFnSc5	Jacopa
I.	I.	kA	I.
Da	Da	k1gMnSc4	Da
Carrara	Carrar	k1gMnSc4	Carrar
</s>
</p>
<p>
<s>
1350	[number]	k4	1350
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
deskové	deskový	k2eAgInPc4d1	deskový
obrazy	obraz	k1gInPc4	obraz
s	s	k7c7	s
Pannou	Panna	k1gFnSc7	Panna
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Matoušem	Matouš	k1gMnSc7	Matouš
a	a	k8xC	a
25	[number]	k4	25
anděly	anděl	k1gMnPc7	anděl
-	-	kIx~	-
nebeskou	nebeský	k2eAgFnSc7d1	nebeská
hierarchií	hierarchie	k1gFnSc7	hierarchie
pro	pro	k7c4	pro
Palác	palác	k1gInSc4	palác
Carrara	Carrar	k1gMnSc2	Carrar
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
Museo	Museo	k6eAd1	Museo
Civico	Civico	k6eAd1	Civico
of	of	k?	of
Padua	Padua	k1gFnSc1	Padua
</s>
</p>
<p>
<s>
1357	[number]	k4	1357
fresky	fresko	k1gNnPc7	fresko
v	v	k7c6	v
lodžii	lodžie	k1gFnSc6	lodžie
Paláce	palác	k1gInSc2	palác
Carrara	Carrara	k1gFnSc1	Carrara
(	(	kIx(	(
příběhy	příběh	k1gInPc1	příběh
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Padova	Padova	k1gFnSc1	Padova
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
zachována	zachovat	k5eAaPmNgFnS	zachovat
a	a	k8xC	a
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
Museo	Museo	k1gMnSc1	Museo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
arte	art	k1gMnSc2	art
medievale	medieval	k1gInSc5	medieval
e	e	k0	e
moderna	moderna	k1gFnSc1	moderna
di	di	k?	di
Padova	Padova	k1gFnSc1	Padova
</s>
</p>
<p>
<s>
1361	[number]	k4	1361
Ctnost	ctnost	k1gFnSc1	ctnost
<g/>
,	,	kIx,	,
monochromní	monochromní	k2eAgFnSc1d1	monochromní
malba	malba	k1gFnSc1	malba
<g/>
,	,	kIx,	,
apsida	apsida	k1gFnSc1	apsida
v	v	k7c6	v
basilica	basilica	k6eAd1	basilica
dei	dei	k?	dei
Santi	Sant	k2eAgMnPc1d1	Sant
Giovanni	Giovann	k1gMnPc1	Giovann
e	e	k0	e
Paolo	Paola	k1gMnSc5	Paola
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
</s>
</p>
<p>
<s>
1361	[number]	k4	1361
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Spoleta	Spoleta	k1gFnSc1	Spoleta
<g/>
,	,	kIx,	,
Dóžecí	dóžecí	k2eAgInSc1d1	dóžecí
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
</s>
</p>
<p>
<s>
1365	[number]	k4	1365
Ráj	ráj	k1gInSc1	ráj
(	(	kIx(	(
<g/>
Paradiso	Paradisa	k1gFnSc5	Paradisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
freska	freska	k1gFnSc1	freska
s	s	k7c7	s
Korunovací	korunovace	k1gFnSc7	korunovace
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
hierarchií	hierarchie	k1gFnPc2	hierarchie
nebes	nebesa	k1gNnPc2	nebesa
<g/>
,	,	kIx,	,
Sala	Sala	k1gMnSc1	Sala
del	del	k?	del
Maggior	Maggior	k1gMnSc1	Maggior
Consiglio	Consiglio	k1gMnSc1	Consiglio
<g/>
,	,	kIx,	,
Dóžecí	dóžecí	k2eAgInSc1d1	dóžecí
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1577	[number]	k4	1577
těžce	těžce	k6eAd1	těžce
poškozena	poškodit	k5eAaPmNgFnS	poškodit
požárem	požár	k1gInSc7	požár
</s>
</p>
<p>
<s>
fresky	freska	k1gFnPc4	freska
v	v	k7c6	v
Dominikánském	dominikánský	k2eAgInSc6d1	dominikánský
kostele	kostel	k1gInSc6	kostel
v	v	k7c6	v
Bolzanu	Bolzan	k1gInSc6	Bolzan
<g/>
,	,	kIx,	,
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
(	(	kIx(	(
<g/>
San	San	k1gFnSc1	San
Nicola	Nicola	k1gFnSc1	Nicola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zničeno	zničen	k2eAgNnSc1d1	zničeno
</s>
</p>
<p>
<s>
freska	freska	k1gFnSc1	freska
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
San	San	k1gFnSc2	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
<g/>
,	,	kIx,	,
Vicenza	Vicenza	k1gFnSc1	Vicenza
</s>
</p>
<p>
<s>
Křídlový	křídlový	k2eAgInSc1d1	křídlový
oltář	oltář	k1gInSc1	oltář
<g/>
:	:	kIx,	:
Ukřižovaný	ukřižovaný	k2eAgMnSc1d1	ukřižovaný
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
andělé	anděl	k1gMnPc1	anděl
a	a	k8xC	a
světci	světec	k1gMnPc1	světec
<g/>
,	,	kIx,	,
diecézní	diecézní	k2eAgNnSc1d1	diecézní
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Milán	Milán	k1gInSc1	Milán
</s>
</p>
<p>
<s>
Trůnící	trůnící	k2eAgFnSc1d1	trůnící
Madona	Madona	k1gFnSc1	Madona
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
Courtauld	Courtauld	k1gInSc1	Courtauld
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
</s>
</p>
<p>
<s>
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
s	s	k7c7	s
Ježíškem	ježíšek	k1gInSc7	ježíšek
<g/>
,	,	kIx,	,
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
</s>
</p>
<p>
<s>
Krucifix	krucifix	k1gInSc1	krucifix
<g/>
,	,	kIx,	,
Fogg	Fogg	k1gInSc1	Fogg
Art	Art	k1gFnPc2	Art
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
Massachussetts	Massachussetts	k1gInSc1	Massachussetts
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Guariento	Guariento	k1gNnSc4	Guariento
di	di	k?	di
Arpo	Arpo	k6eAd1	Arpo
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Enrico	Enrico	k6eAd1	Enrico
Castelnuovo	Castelnuův	k2eAgNnSc1d1	Castelnuův
(	(	kIx(	(
<g/>
a	a	k8xC	a
cura	cura	k1gFnSc1	cura
di	di	k?	di
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
pittura	pittura	k1gFnSc1	pittura
in	in	k?	in
Italia	Italia	k1gFnSc1	Italia
<g/>
.	.	kIx.	.
</s>
<s>
Il	Il	k?	Il
Duecento	Duecento	k1gNnSc1	Duecento
e	e	k0	e
il	il	k?	il
Trecento	trecento	k1gNnSc4	trecento
<g/>
,	,	kIx,	,
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
Electa	Electa	k1gMnSc1	Electa
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
88	[number]	k4	88
<g/>
-	-	kIx~	-
<g/>
435	[number]	k4	435
<g/>
-	-	kIx~	-
<g/>
2096	[number]	k4	2096
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Stejskal	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
Umění	umění	k1gNnSc1	umění
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
,	,	kIx,	,
Artia	Artia	k1gFnSc1	Artia
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
</s>
</p>
<p>
<s>
Francesca	Francesca	k1gMnSc1	Francesca
Flores	Flores	k1gMnSc1	Flores
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Arcais	Arcais	k1gFnSc1	Arcais
<g/>
,	,	kIx,	,
Guariento	Guariento	k1gNnSc1	Guariento
<g/>
.	.	kIx.	.
</s>
<s>
Tutta	Tutta	k1gFnSc1	Tutta
la	la	k1gNnSc2	la
pittura	pittura	k1gFnSc1	pittura
<g/>
,	,	kIx,	,
Venezia	Venezia	k1gFnSc1	Venezia
<g/>
,	,	kIx,	,
Alfieri	Alfieri	k1gNnSc1	Alfieri
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1974	[number]	k4	1974
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Francesca	Francesca	k1gMnSc1	Francesca
Flores	Flores	k1gMnSc1	Flores
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Arcais	Arcais	k1gFnSc1	Arcais
<g/>
,	,	kIx,	,
Guariento	Guariento	k1gNnSc1	Guariento
<g/>
,	,	kIx,	,
Venezia	Venezia	k1gFnSc1	Venezia
<g/>
,	,	kIx,	,
Alfieri	Alfieri	k1gNnSc1	Alfieri
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Guariento	Guariento	k1gNnSc1	Guariento
di	di	k?	di
Arpo	Arpo	k6eAd1	Arpo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Anne	Annat	k5eAaPmIp3nS	Annat
Fitzgerald	Fitzgerald	k1gInSc1	Fitzgerald
<g/>
,	,	kIx,	,
Guariento	Guariento	k1gNnSc1	Guariento
di	di	k?	di
Arpo	Arpo	k1gNnSc1	Arpo
<g/>
,	,	kIx,	,
Memoirs	Memoirs	k1gInSc1	Memoirs
of	of	k?	of
the	the	k?	the
American	American	k1gMnSc1	American
Academy	Academa	k1gFnSc2	Academa
in	in	k?	in
Rome	Rom	k1gMnSc5	Rom
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
9	[number]	k4	9
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
167-198	[number]	k4	167-198
</s>
</p>
<p>
<s>
The	The	k?	The
J.	J.	kA	J.
Paul	Paul	k1gMnSc1	Paul
Getty	Getta	k1gFnSc2	Getta
Museum	museum	k1gNnSc1	museum
<g/>
:	:	kIx,	:
Guariento	Guariento	k1gNnSc1	Guariento
di	di	k?	di
Arpo	Arpo	k6eAd1	Arpo
</s>
</p>
