<s>
Ukulele	ukulele	k1gFnSc1	ukulele
(	(	kIx(	(
<g/>
havajsky	havajsky	k6eAd1	havajsky
ʻ	ʻ	k?	ʻ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drnkací	drnkací	k2eAgInSc1d1	drnkací
nástroj	nástroj	k1gInSc1	nástroj
podobný	podobný	k2eAgInSc1d1	podobný
kytaře	kytara	k1gFnSc3	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
struny	struna	k1gFnSc2	struna
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
6	[number]	k4	6
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
velikostech	velikost	k1gFnPc6	velikost
Soprano	Soprana	k1gFnSc5	Soprana
<g/>
,	,	kIx,	,
Concert	Concert	k1gMnSc1	Concert
<g/>
,	,	kIx,	,
Tenor	tenor	k1gMnSc1	tenor
a	a	k8xC	a
Baritone	Bariton	k1gInSc5	Bariton
<g/>
.	.	kIx.	.
</s>
<s>
Ukulele	ukulele	k1gFnSc1	ukulele
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
uke	uke	k?	uke
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
havajský	havajský	k2eAgInSc1d1	havajský
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
malá	malý	k2eAgFnSc1d1	malá
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
nástroji	nástroj	k1gInSc3	nástroj
také	také	k9	také
často	často	k6eAd1	často
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
ukelele	ukelele	k6eAd1	ukelele
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k8xS	jako
drnkací	drnkací	k2eAgFnSc1d1	drnkací
loutna	loutna	k1gFnSc1	loutna
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
podskupiny	podskupina	k1gFnSc2	podskupina
nástrojové	nástrojový	k2eAgFnSc2d1	nástrojová
rodiny	rodina	k1gFnSc2	rodina
kytar	kytara	k1gFnPc2	kytara
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
struny	struna	k1gFnSc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Ukulele	ukulele	k1gFnSc1	ukulele
původem	původ	k1gInSc7	původ
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
havajská	havajský	k2eAgFnSc1d1	Havajská
interpretace	interpretace	k1gFnSc1	interpretace
malé	malý	k2eAgFnSc2d1	malá
kytary	kytara	k1gFnSc2	kytara
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
díky	díky	k7c3	díky
Portugalcům	Portugalec	k1gMnPc3	Portugalec
<g/>
.	.	kIx.	.
</s>
<s>
Získalo	získat	k5eAaPmAgNnS	získat
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
popularitu	popularita	k1gFnSc4	popularita
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tón	tón	k1gInSc1	tón
a	a	k8xC	a
hlasitost	hlasitost	k1gFnSc1	hlasitost
nástroje	nástroj	k1gInSc2	nástroj
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Ukulele	ukulele	k1gNnSc1	ukulele
přišlo	přijít	k5eAaPmAgNnS	přijít
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
velikostmi	velikost	k1gFnPc7	velikost
<g/>
:	:	kIx,	:
sopránové	sopránový	k2eAgFnPc1d1	sopránová
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgFnPc1d1	koncertní
<g/>
,	,	kIx,	,
tenorové	tenorový	k2eAgFnPc1d1	Tenorová
a	a	k8xC	a
barytonové	barytonový	k2eAgFnPc1d1	barytonová
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
z	z	k7c2	z
Havaje	Havaj	k1gFnSc2	Havaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tomuto	tento	k3xDgInSc3	tento
nástroji	nástroj	k1gInSc3	nástroj
stroze	stroze	k6eAd1	stroze
říkají	říkat	k5eAaImIp3nP	říkat
"	"	kIx"	"
<g/>
skákající	skákající	k2eAgFnSc1d1	skákající
blecha	blecha	k1gFnSc1	blecha
<g/>
"	"	kIx"	"
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
poslední	poslední	k2eAgFnSc2d1	poslední
havajské	havajský	k2eAgFnSc2d1	Havajská
královny	královna	k1gFnSc2	královna
Lili	Lili	k1gFnSc2	Lili
<g/>
'	'	kIx"	'
<g/>
uokalani	uokalaň	k1gFnSc3	uokalaň
ukulele	ukulele	k1gNnSc2	ukulele
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
dar	dar	k1gInSc1	dar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přišel	přijít	k5eAaPmAgInS	přijít
sem	sem	k6eAd1	sem
<g/>
"	"	kIx"	"
z	z	k7c2	z
havajského	havajský	k2eAgNnSc2d1	havajské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
uku	uku	k?	uku
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dar	dar	k1gInSc1	dar
<g/>
,	,	kIx,	,
odměna	odměna	k1gFnSc1	odměna
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
lele	lele	k1gFnSc1	lele
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
přijít	přijít	k5eAaPmF	přijít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
názvy	název	k1gInPc1	název
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
uku	uku	k?	uku
<g/>
"	"	kIx"	"
psát	psát	k5eAaImF	psát
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
uku	uku	k?	uku
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
havajštině	havajština	k1gFnSc6	havajština
dar	dar	k1gInSc4	dar
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
uku	uku	k?	uku
<g/>
"	"	kIx"	"
<g/>
(	(	kIx(	(
s	s	k7c7	s
apostrofem	apostrof	k1gInSc7	apostrof
před	před	k7c7	před
hláskou	hláska	k1gFnSc7	hláska
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
označuje	označovat	k5eAaImIp3nS	označovat
havajsky	havajsky	k6eAd1	havajsky
blechu	blecha	k1gFnSc4	blecha
<g/>
.	.	kIx.	.
</s>
<s>
Apostrof	apostrof	k1gInSc1	apostrof
se	se	k3xPyFc4	se
v	v	k7c6	v
mluveném	mluvený	k2eAgInSc6d1	mluvený
jazyce	jazyk	k1gInSc6	jazyk
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
tzv.	tzv.	kA	tzv.
hrdelním	hrdelní	k2eAgInSc7d1	hrdelní
zvukem	zvuk	k1gInSc7	zvuk
při	při	k7c6	při
vyslovení	vyslovení	k1gNnSc6	vyslovení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
lele	lele	k1gInSc1	lele
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
havajštině	havajština	k1gFnSc6	havajština
přicházet	přicházet	k5eAaImF	přicházet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
létat	létat	k5eAaImF	létat
i	i	k8xC	i
skákat	skákat	k5eAaImF	skákat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existují	existovat	k5eAaImIp3nP	existovat
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
názvy	název	k1gInPc1	název
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Ukulele	ukulele	k1gNnSc1	ukulele
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c4	na
podobnosti	podobnost	k1gFnPc4	podobnost
s	s	k7c7	s
"	"	kIx"	"
<g/>
malou	malý	k2eAgFnSc7d1	malá
kytarou	kytara	k1gFnSc7	kytara
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
cavaquinho	cavaquinze	k6eAd1	cavaquinze
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
představeno	představit	k5eAaPmNgNnS	představit
havajským	havajský	k2eAgInSc7d1	havajský
ostrovům	ostrov	k1gInPc3	ostrov
portugalskými	portugalský	k2eAgMnPc7d1	portugalský
imigranty	imigrant	k1gMnPc7	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
tři	tři	k4xCgMnPc4	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Manuel	Manuel	k1gMnSc1	Manuel
Nunes	Nunesa	k1gFnPc2	Nunesa
<g/>
,	,	kIx,	,
José	Josá	k1gFnSc2	Josá
do	do	k7c2	do
Espiríto	Espiríto	k1gNnSc1	Espiríto
Santo	Santa	k1gMnSc5	Santa
a	a	k8xC	a
Augusto	Augusta	k1gMnSc5	Augusta
Dias	Dias	k1gInSc4	Dias
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
známí	známý	k2eAgMnPc1d1	známý
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgMnPc1	první
výrobci	výrobce	k1gMnPc1	výrobce
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
nalodili	nalodit	k5eAaPmAgMnP	nalodit
na	na	k7c4	na
Ravenscrag	Ravenscrag	k1gInSc4	Ravenscrag
na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
havajské	havajský	k2eAgFnPc4d1	Havajská
noviny	novina	k1gFnPc4	novina
(	(	kIx(	(
<g/>
Hawaiian	Hawaiian	k1gMnSc1	Hawaiian
Gazette	Gazett	k1gInSc5	Gazett
<g/>
)	)	kIx)	)
hlásily	hlásit	k5eAaImAgFnP	hlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
madeirští	madeirský	k2eAgMnPc1d1	madeirský
ostrované	ostrovan	k1gMnPc1	ostrovan
nedávno	nedávno	k6eAd1	nedávno
přijeli	přijet	k5eAaPmAgMnP	přijet
a	a	k8xC	a
už	už	k6eAd1	už
těší	těšit	k5eAaImIp3nS	těšit
publikum	publikum	k1gNnSc1	publikum
nočními	noční	k2eAgInPc7d1	noční
pouličními	pouliční	k2eAgInPc7d1	pouliční
koncerty	koncert	k1gInPc7	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
zavádějících	zavádějící	k2eAgNnPc2d1	zavádějící
ukulele	ukulele	k1gNnPc2	ukulele
do	do	k7c2	do
havajské	havajský	k2eAgFnSc2d1	Havajská
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nadšená	nadšený	k2eAgFnSc1d1	nadšená
podpora	podpora	k1gFnSc1	podpora
a	a	k8xC	a
propagace	propagace	k1gFnSc1	propagace
nástroje	nástroj	k1gInSc2	nástroj
havajským	havajský	k2eAgMnSc7d1	havajský
králem	král	k1gMnSc7	král
Davidem	David	k1gMnSc7	David
Kalakauou	Kalakaua	k1gMnSc7	Kalakaua
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
patron	patron	k1gInSc1	patron
umění	umění	k1gNnSc2	umění
jej	on	k3xPp3gMnSc4	on
začlenil	začlenit	k5eAaPmAgMnS	začlenit
do	do	k7c2	do
představení	představení	k1gNnSc2	představení
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
nástroj	nástroj	k1gInSc1	nástroj
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgMnS	stát
celosvětově	celosvětově	k6eAd1	celosvětově
známým	známý	k2eAgInPc3d1	známý
až	až	k9	až
díky	díky	k7c3	díky
americkému	americký	k2eAgNnSc3d1	americké
publiku	publikum	k1gNnSc3	publikum
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ukulele	ukulele	k1gNnSc1	ukulele
bylo	být	k5eAaImAgNnS	být
zpopularizováno	zpopularizovat	k5eAaPmNgNnS	zpopularizovat
pro	pro	k7c4	pro
americké	americký	k2eAgNnSc4d1	americké
publikum	publikum	k1gNnSc4	publikum
během	během	k7c2	během
Panamsko	Panamsko	k1gNnSc1	Panamsko
–	–	k?	–
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
expozice	expozice	k1gFnSc2	expozice
(	(	kIx(	(
<g/>
PPIE	PPIE	kA	PPIE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Franciska	k1gFnSc4	Franciska
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
dokončení	dokončení	k1gNnSc2	dokončení
Panamského	panamský	k2eAgInSc2d1	panamský
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Havajský	havajský	k2eAgInSc1d1	havajský
pavilón	pavilón	k1gInSc1	pavilón
uváděl	uvádět	k5eAaImAgInS	uvádět
kytarový	kytarový	k2eAgInSc1d1	kytarový
a	a	k8xC	a
ukulelový	ukulelový	k2eAgInSc1d1	ukulelový
ansámbl	ansámbl	k1gInSc1	ansámbl
<g/>
,	,	kIx,	,
George	George	k1gInSc1	George
E.	E.	kA	E.
K.	K.	kA	K.
Awai	Awai	k1gNnSc4	Awai
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
Královské	královský	k2eAgNnSc1d1	královské
havajské	havajský	k2eAgNnSc1d1	havajské
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
,	,	kIx,	,
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
výrobce	výrobce	k1gMnSc1	výrobce
ukulele	ukulele	k1gFnSc2	ukulele
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
Jonah	Jonah	k1gMnSc1	Jonah
Kumalae	Kumalae	k1gFnSc1	Kumalae
<g/>
.	.	kIx.	.
</s>
<s>
Ukulelový	Ukulelový	k2eAgInSc1d1	Ukulelový
ansámbl	ansámbl	k1gInSc1	ansámbl
pobláznil	pobláznit	k5eAaPmAgInS	pobláznit
návštěvníky	návštěvník	k1gMnPc4	návštěvník
havajskými	havajský	k2eAgFnPc7d1	Havajská
písničkami	písnička	k1gFnPc7	písnička
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
uvedl	uvést	k5eAaPmAgMnS	uvést
ukulele	ukulele	k1gNnSc4	ukulele
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
zábavnými	zábavný	k2eAgFnPc7d1	zábavná
umělci	umělec	k1gMnPc1	umělec
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
třeba	třeba	k6eAd1	třeba
Roy	Roy	k1gMnSc1	Roy
Smeck	Smeck	k1gMnSc1	Smeck
a	a	k8xC	a
Cliff	Cliff	k1gMnSc1	Cliff
Edwards	Edwardsa	k1gFnPc2	Edwardsa
<g/>
.	.	kIx.	.
</s>
<s>
Ukulele	ukulele	k1gFnSc1	ukulele
se	se	k3xPyFc4	se
také	také	k9	také
brzy	brzy	k6eAd1	brzy
stalo	stát	k5eAaPmAgNnS	stát
ikonou	ikona	k1gFnSc7	ikona
jazzového	jazzový	k2eAgNnSc2d1	jazzové
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
lehce	lehko	k6eAd1	lehko
přenosné	přenosný	k2eAgNnSc1d1	přenosné
a	a	k8xC	a
docela	docela	k6eAd1	docela
levné	levný	k2eAgNnSc1d1	levné
a	a	k8xC	a
pořád	pořád	k6eAd1	pořád
si	se	k3xPyFc3	se
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
popularitu	popularita	k1gFnSc4	popularita
díky	díky	k7c3	díky
amatérským	amatérský	k2eAgMnPc3d1	amatérský
hráčům	hráč	k1gMnPc3	hráč
během	běh	k1gInSc7	běh
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
vycházely	vycházet	k5eAaImAgFnP	vycházet
akordové	akordový	k2eAgInPc4d1	akordový
tabulátory	tabulátor	k1gInPc4	tabulátor
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
populárních	populární	k2eAgFnPc2d1	populární
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
impulzem	impulz	k1gInSc7	impulz
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
výrobce	výrobce	k1gMnPc4	výrobce
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Regal	Regal	k1gInSc1	Regal
<g/>
,	,	kIx,	,
Harmony	Harmon	k1gInPc1	Harmon
nebo	nebo	k8xC	nebo
Martin	Martin	k1gMnSc1	Martin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
léty	léto	k1gNnPc7	léto
výrobce	výrobce	k1gMnSc1	výrobce
hraček	hračka	k1gFnPc2	hračka
Mario	Mario	k1gMnSc1	Mario
Maccaferri	Maccaferr	k1gFnSc2	Maccaferr
prodal	prodat	k5eAaPmAgMnS	prodat
okolo	okolo	k7c2	okolo
9	[number]	k4	9
miliónů	milión	k4xCgInPc2	milión
plastových	plastový	k2eAgFnPc2d1	plastová
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
popularity	popularita	k1gFnSc2	popularita
byla	být	k5eAaImAgFnS	být
také	také	k9	také
způsobena	způsobit	k5eAaPmNgFnS	způsobit
Arthurem	Arthur	k1gMnSc7	Arthur
Godfreyem	Godfrey	k1gMnSc7	Godfrey
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
televizní	televizní	k2eAgFnSc6d1	televizní
show	show	k1gFnSc6	show
<g/>
.	.	kIx.	.
</s>
<s>
Havajský	havajský	k2eAgMnSc1d1	havajský
rodák	rodák	k1gMnSc1	rodák
Jake	Jak	k1gFnSc2	Jak
Shimabukuro	Shimabukura	k1gFnSc5	Shimabukura
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
ukulele	ukulele	k1gFnPc4	ukulele
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velice	velice	k6eAd1	velice
známým	známý	k2eAgMnSc7d1	známý
hráčem	hráč	k1gMnSc7	hráč
v	v	k7c6	v
nedávných	dávný	k2eNgNnPc6d1	nedávné
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ukulele	ukulele	k1gNnSc1	ukulele
přišlo	přijít	k5eAaPmAgNnS	přijít
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
havajský	havajský	k2eAgMnSc1d1	havajský
rodák	rodák	k1gMnSc1	rodák
Yukihiko	Yukihika	k1gFnSc5	Yukihika
Haida	Haid	k1gMnSc4	Haid
vrátil	vrátit	k5eAaPmAgMnS	vrátit
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
a	a	k8xC	a
představil	představit	k5eAaPmAgMnS	představit
tento	tento	k3xDgInSc4	tento
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Haida	Haida	k1gMnSc1	Haida
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Katsuhiko	Katsuhika	k1gFnSc5	Katsuhika
si	se	k3xPyFc3	se
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
klub	klub	k1gInSc4	klub
(	(	kIx(	(
<g/>
Moana	Moaen	k2eAgFnSc1d1	Moaen
Glee	Glee	k1gNnSc4	Glee
Club	club	k1gInSc1	club
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
užívali	užívat	k5eAaImAgMnP	užívat
rychlý	rychlý	k2eAgInSc4d1	rychlý
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
vzrůstajícího	vzrůstající	k2eAgNnSc2d1	vzrůstající
nadšení	nadšení	k1gNnSc2	nadšení
pro	pro	k7c4	pro
západní	západní	k2eAgFnSc4d1	západní
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
havajskou	havajský	k2eAgFnSc4d1	Havajská
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
jazz	jazz	k1gInSc4	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
úřady	úřad	k1gInPc1	úřad
zakázaly	zakázat	k5eAaPmAgInP	zakázat
většinu	většina	k1gFnSc4	většina
západní	západní	k2eAgFnSc2d1	západní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fanoušci	fanoušek	k1gMnPc1	fanoušek
a	a	k8xC	a
hráči	hráč	k1gMnPc1	hráč
ji	on	k3xPp3gFnSc4	on
potají	potají	k6eAd1	potají
zachovávali	zachovávat	k5eAaImAgMnP	zachovávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
opět	opět	k6eAd1	opět
nabrala	nabrat	k5eAaPmAgFnS	nabrat
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Japonsko	Japonsko	k1gNnSc1	Japonsko
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
druhý	druhý	k4xOgInSc4	druhý
domov	domov	k1gInSc4	domov
pro	pro	k7c4	pro
havajské	havajský	k2eAgMnPc4d1	havajský
muzikanty	muzikant	k1gMnPc4	muzikant
a	a	k8xC	a
ukulelové	ukulelový	k2eAgFnPc4d1	ukulelový
virtuózy	virtuóza	k1gFnPc4	virtuóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
pedagog	pedagog	k1gMnSc1	pedagog
John	John	k1gMnSc1	John
Chalmers	Chalmers	k1gInSc4	Chalmers
Doane	Doan	k1gInSc5	Doan
dramaticky	dramaticky	k6eAd1	dramaticky
změnil	změnit	k5eAaPmAgInS	změnit
školní	školní	k2eAgInSc1d1	školní
hudební	hudební	k2eAgInSc1d1	hudební
program	program	k1gInSc1	program
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
používáním	používání	k1gNnSc7	používání
ukulele	ukulele	k1gFnSc2	ukulele
jako	jako	k8xS	jako
levného	levný	k2eAgInSc2d1	levný
a	a	k8xC	a
praktického	praktický	k2eAgInSc2d1	praktický
výukového	výukový	k2eAgInSc2d1	výukový
nástroje	nástroj	k1gInSc2	nástroj
k	k	k7c3	k
výchově	výchova	k1gFnSc3	výchova
muzikální	muzikální	k2eAgFnSc2d1	muzikální
gramotnosti	gramotnost	k1gFnSc2	gramotnost
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
školáků	školák	k1gMnPc2	školák
a	a	k8xC	a
dospělých	dospělí	k1gMnPc2	dospělí
se	se	k3xPyFc4	se
učilo	učít	k5eAaPmAgNnS	učít
na	na	k7c4	na
ukulele	ukulele	k1gNnSc4	ukulele
pomocí	pomocí	k7c2	pomocí
Doaneova	Doaneův	k2eAgInSc2d1	Doaneův
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	let	k1gInPc6	let
existovalo	existovat	k5eAaImAgNnS	existovat
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
několik	několik	k4yIc4	několik
kavárenských	kavárenský	k2eAgInPc2d1	kavárenský
a	a	k8xC	a
barových	barový	k2eAgInPc2d1	barový
"	"	kIx"	"
<g/>
jazzových	jazzový	k2eAgInPc2d1	jazzový
<g/>
"	"	kIx"	"
orchestrů	orchestr	k1gInPc2	orchestr
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
po	po	k7c6	po
americkém	americký	k2eAgInSc6d1	americký
vzoru	vzor	k1gInSc6	vzor
měly	mít	k5eAaImAgFnP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
instrumentáři	instrumentář	k1gMnPc1	instrumentář
i	i	k9	i
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zlidověním	zlidovění	k1gNnSc7	zlidovění
prvků	prvek	k1gInPc2	prvek
jazzové	jazzový	k2eAgFnSc2d1	jazzová
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
šířilo	šířit	k5eAaImAgNnS	šířit
také	také	k9	také
ukulele	ukulele	k1gNnSc1	ukulele
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
trampské	trampský	k2eAgFnSc2d1	trampská
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Balada	balada	k1gFnSc1	balada
z	z	k7c2	z
Tahiti	Tahiti	k1gNnSc2	Tahiti
podle	podle	k7c2	podle
úvodního	úvodní	k2eAgInSc2d1	úvodní
verše	verš	k1gInSc2	verš
známá	známý	k2eAgFnSc1d1	známá
též	též	k6eAd1	též
jako	jako	k8xC	jako
Lká	lkát	k5eAaImIp3nS	lkát
ukulele	ukulele	k1gFnSc1	ukulele
tmou	tma	k1gFnSc7	tma
<g/>
)	)	kIx)	)
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Kumoka	Kumoek	k1gMnSc2	Kumoek
<g/>
,	,	kIx,	,
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
již	již	k6eAd1	již
počátkem	počátkem	k7c2	počátkem
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
s	s	k7c7	s
bouřlivým	bouřlivý	k2eAgNnSc7d1	bouřlivé
rozšířením	rozšíření	k1gNnSc7	rozšíření
tzv.	tzv.	kA	tzv.
havajských	havajský	k2eAgFnPc2d1	Havajská
skupin	skupina	k1gFnPc2	skupina
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k6eAd1	také
k	k	k7c3	k
neobyčejné	obyčejný	k2eNgFnSc3d1	neobyčejná
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
malých	malý	k2eAgFnPc6d1	malá
čtyřstrunných	čtyřstrunný	k2eAgFnPc6d1	čtyřstrunná
"	"	kIx"	"
<g/>
havajských	havajský	k2eAgFnPc6d1	Havajská
<g/>
"	"	kIx"	"
kytarách	kytara	k1gFnPc6	kytara
ukulele	ukulele	k1gFnSc2	ukulele
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
k	k	k7c3	k
masovému	masový	k2eAgNnSc3d1	masové
rozšíření	rozšíření	k1gNnSc3	rozšíření
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Klasické	klasický	k2eAgNnSc4d1	klasické
<g/>
"	"	kIx"	"
obsazení	obsazení	k1gNnSc4	obsazení
havajských	havajský	k2eAgFnPc2d1	Havajská
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
skládalo	skládat	k5eAaImAgNnS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
až	až	k9	až
tří	tři	k4xCgFnPc2	tři
kytar	kytara	k1gFnPc2	kytara
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
havajské	havajský	k2eAgFnSc2d1	Havajská
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgNnSc2	jeden
ukulele	ukulele	k1gNnSc2	ukulele
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgInSc2	jeden
kontrabasu	kontrabas	k1gInSc2	kontrabas
a	a	k8xC	a
jednoho	jeden	k4xCgNnSc2	jeden
bonga	bongo	k1gNnSc2	bongo
(	(	kIx(	(
<g/>
malé	malý	k2eAgFnSc2d1	malá
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
až	až	k9	až
čtyři	čtyři	k4xCgInPc1	čtyři
sdružené	sdružený	k2eAgInPc1d1	sdružený
bubínky	bubínek	k1gInPc1	bubínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
ukulele	ukulele	k1gFnSc1	ukulele
obtížně	obtížně	k6eAd1	obtížně
získávala	získávat	k5eAaImAgFnS	získávat
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
nadšenců	nadšenec	k1gMnPc2	nadšenec
nerealizovatelné	realizovatelný	k2eNgNnSc1d1	nerealizovatelné
<g/>
.	.	kIx.	.
</s>
<s>
Ukulele	ukulele	k1gFnSc1	ukulele
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
amatérsky	amatérsky	k6eAd1	amatérsky
podomácku	podomácku	k6eAd1	podomácku
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
takto	takto	k6eAd1	takto
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
nástroje	nástroj	k1gInPc1	nástroj
dosahovaly	dosahovat	k5eAaImAgInP	dosahovat
docela	docela	k6eAd1	docela
dobré	dobrý	k2eAgFnSc2d1	dobrá
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
zadávána	zadávat	k5eAaImNgFnS	zadávat
profesionálním	profesionální	k2eAgInPc3d1	profesionální
jednotlivě	jednotlivě	k6eAd1	jednotlivě
pracujícím	pracující	k2eAgMnPc3d1	pracující
houslařům	houslař	k1gMnPc3	houslař
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
soustředili	soustředit	k5eAaPmAgMnP	soustředit
především	především	k9	především
na	na	k7c4	na
ukulele	ukulele	k1gNnSc4	ukulele
typu	typ	k1gInSc2	typ
jazzová	jazzový	k2eAgFnSc1d1	jazzová
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
Gibson	Gibson	k1gInSc1	Gibson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
počátku	počátek	k1gInSc2	počátek
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
výroby	výroba	k1gFnPc1	výroba
ujaly	ujmout	k5eAaPmAgFnP	ujmout
také	také	k9	také
Československé	československý	k2eAgInPc4d1	československý
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
daly	dát	k5eAaPmAgInP	dát
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
ukulele	ukulele	k1gNnSc1	ukulele
z	z	k7c2	z
překližky	překližka	k1gFnSc2	překližka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
amatérsky	amatérsky	k6eAd1	amatérsky
předělávány	předělávat	k5eAaImNgInP	předělávat
a	a	k8xC	a
vylepšovány	vylepšovat	k5eAaImNgInP	vylepšovat
-	-	kIx~	-
například	například	k6eAd1	například
opatřením	opatření	k1gNnSc7	opatření
ukulele	ukulele	k1gFnSc2	ukulele
kolíčkovým	kolíčkový	k2eAgInSc7d1	kolíčkový
strojkem	strojek	k1gInSc7	strojek
atd.	atd.	kA	atd.
Celkově	celkově	k6eAd1	celkově
však	však	k9	však
tyto	tento	k3xDgInPc1	tento
překližkové	překližkový	k2eAgInPc1d1	překližkový
výrobky	výrobek	k1gInPc1	výrobek
nedošly	dojít	k5eNaPmAgInP	dojít
velkého	velký	k2eAgNnSc2d1	velké
uplatnění	uplatnění	k1gNnSc2	uplatnění
a	a	k8xC	a
skončily	skončit	k5eAaPmAgFnP	skončit
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
jako	jako	k8xS	jako
dětské	dětský	k2eAgFnSc2d1	dětská
hračky	hračka	k1gFnSc2	hračka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
havajských	havajský	k2eAgFnPc6d1	Havajská
skupinách	skupina	k1gFnPc6	skupina
dominovaly	dominovat	k5eAaImAgInP	dominovat
amatérské	amatérský	k2eAgInPc1d1	amatérský
výrobky	výrobek	k1gInPc1	výrobek
a	a	k8xC	a
nástroje	nástroj	k1gInPc1	nástroj
od	od	k7c2	od
mistrů	mistr	k1gMnPc2	mistr
houslařů	houslař	k1gMnPc2	houslař
či	či	k8xC	či
výrobců	výrobce	k1gMnPc2	výrobce
kytar	kytara	k1gFnPc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
několik	několik	k4yIc1	několik
amatérských	amatérský	k2eAgFnPc2d1	amatérská
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
nostalgicky	nostalgicky	k6eAd1	nostalgicky
hrajících	hrající	k2eAgInPc2d1	hrající
někdejší	někdejší	k2eAgInSc4d1	někdejší
repertoár	repertoár	k1gInSc4	repertoár
havajské	havajský	k2eAgFnSc2d1	Havajská
skupiny	skupina	k1gFnSc2	skupina
Václava	Václav	k1gMnSc2	Václav
Kučery	Kučera	k1gMnSc2	Kučera
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
nástroj	nástroj	k1gInSc1	nástroj
používá	používat	k5eAaImIp3nS	používat
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tradiční	tradiční	k2eAgFnSc7d1	tradiční
americko-anglickou	americkonglický	k2eAgFnSc7d1	americko-anglická
výslovností	výslovnost	k1gFnSc7	výslovnost
"	"	kIx"	"
<g/>
ukulele	ukulele	k1gFnSc1	ukulele
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
[	[	kIx(	[
<g/>
yoo-kuh-ley-lee	yoouheyee	k6eAd1	yoo-kuh-ley-lee
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgMnPc1d1	mluvící
lidé	člověk	k1gMnPc1	člověk
preferují	preferovat	k5eAaImIp3nP	preferovat
originální	originální	k2eAgFnSc4d1	originální
havajskou	havajský	k2eAgFnSc4d1	Havajská
výslovnost	výslovnost	k1gFnSc4	výslovnost
[	[	kIx(	[
<g/>
oo-koo-ley-ley	ooooeyea	k1gFnSc2	oo-koo-ley-lea
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
americko-anglická	americkonglický	k2eAgFnSc1d1	americko-anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
více	hodně	k6eAd2	hodně
používána	používán	k2eAgFnSc1d1	používána
<g/>
,	,	kIx,	,
havajská	havajský	k2eAgFnSc1d1	Havajská
je	být	k5eAaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
a	a	k8xC	a
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zajímají	zajímat	k5eAaImIp3nP	zajímat
o	o	k7c4	o
havajskou	havajský	k2eAgFnSc4d1	Havajská
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Ukulele	ukulele	k1gFnSc1	ukulele
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někdy	někdy	k6eAd1	někdy
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
i	i	k9	i
zcela	zcela	k6eAd1	zcela
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Levnější	levný	k2eAgFnSc1d2	levnější
ukulele	ukulele	k1gFnSc1	ukulele
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
laminového	laminový	k2eAgNnSc2d1	laminový
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
akustiku	akustika	k1gFnSc4	akustika
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
horní	horní	k2eAgFnSc1d1	horní
deska	deska	k1gFnSc1	deska
z	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
kvalitnějšího	kvalitní	k2eAgNnSc2d2	kvalitnější
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
jedle	jedle	k1gFnSc2	jedle
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
dražší	drahý	k2eAgNnPc1d2	dražší
ukulele	ukulele	k1gNnPc1	ukulele
jsou	být	k5eAaImIp3nP	být
vyrobena	vyroben	k2eAgNnPc1d1	vyrobeno
z	z	k7c2	z
exotického	exotický	k2eAgNnSc2d1	exotické
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
mahagon	mahagon	k1gInSc4	mahagon
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
nejhodnotnějších	hodnotný	k2eAgNnPc2d3	nejhodnotnější
ukulele	ukulele	k1gNnPc2	ukulele
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
i	i	k9	i
tisíce	tisíc	k4xCgInPc4	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vyrobena	vyroben	k2eAgNnPc1d1	vyrobeno
z	z	k7c2	z
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
havajského	havajský	k2eAgNnSc2d1	havajské
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
koa	koa	k?	koa
<g/>
.	.	kIx.	.
</s>
<s>
Dodává	dodávat	k5eAaImIp3nS	dodávat
tónu	tón	k1gInSc2	tón
atraktivní	atraktivní	k2eAgFnSc4d1	atraktivní
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
ukulele	ukulele	k1gNnSc1	ukulele
má	mít	k5eAaImIp3nS	mít
tělo	tělo	k1gNnSc4	tělo
stavěno	stavit	k5eAaImNgNnS	stavit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
malá	malý	k2eAgFnSc1d1	malá
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
často	často	k6eAd1	často
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
v	v	k7c6	v
nestandardních	standardní	k2eNgInPc6d1	nestandardní
tvarech	tvar	k1gInPc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
ovál	ovál	k1gInSc4	ovál
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
nazýván	nazýván	k2eAgInSc1d1	nazýván
"	"	kIx"	"
<g/>
ananasové	ananasový	k2eAgNnSc1d1	ananasové
ukulele	ukulele	k1gNnSc1	ukulele
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
lodního	lodní	k2eAgNnSc2d1	lodní
pádla	pádlo	k1gNnSc2	pádlo
<g/>
,	,	kIx,	,
vymyšlený	vymyšlený	k2eAgInSc1d1	vymyšlený
firmou	firma	k1gFnSc7	firma
Kamaka	Kamak	k1gMnSc2	Kamak
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
příležitostně	příležitostně	k6eAd1	příležitostně
čtverec	čtverec	k1gInSc1	čtverec
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
ze	z	k7c2	z
starých	starý	k2eAgFnPc2d1	stará
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
cigaretových	cigaretový	k2eAgFnPc2d1	cigaretová
krabic	krabice	k1gFnPc2	krabice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgFnPc4	čtyři
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
struny	struna	k1gFnPc1	struna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
páru	pár	k1gInSc6	pár
se	s	k7c7	s
strunou	struna	k1gFnSc7	struna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
naladěna	naladit	k5eAaPmNgFnS	naladit
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
výše	výše	k1gFnSc2	výše
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
základní	základní	k2eAgFnSc1d1	základní
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nástroj	nástroj	k1gInSc4	nástroj
o	o	k7c6	o
osmi	osm	k4xCc6	osm
strunách	struna	k1gFnPc6	struna
<g/>
.	.	kIx.	.
hlavice	hlavice	k1gFnSc1	hlavice
-	-	kIx~	-
část	část	k1gFnSc1	část
držící	držící	k2eAgFnSc4d1	držící
ladící	ladící	k2eAgFnSc4d1	ladící
mechaniku	mechanika	k1gFnSc4	mechanika
s	s	k7c7	s
kolíky	kolík	k1gInPc7	kolík
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
najdeme	najít	k5eAaPmIp1nP	najít
logo	logo	k1gNnSc4	logo
výrobce	výrobce	k1gMnSc2	výrobce
ladící	ladící	k2eAgFnSc1d1	ladící
mechanika	mechanika	k1gFnSc1	mechanika
s	s	k7c7	s
kolíky	kolík	k1gInPc7	kolík
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
zachyceny	zachycen	k2eAgFnPc4d1	zachycena
napnuté	napnutý	k2eAgFnPc4d1	napnutá
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
točením	točení	k1gNnSc7	točení
kolíků	kolík	k1gInPc2	kolík
se	se	k3xPyFc4	se
ovládá	ovládat	k5eAaImIp3nS	ovládat
výška	výška	k1gFnSc1	výška
tónu	tón	k1gInSc2	tón
sedlový	sedlový	k2eAgInSc1d1	sedlový
pražec	pražec	k1gInSc1	pražec
–	–	k?	–
drží	držet	k5eAaImIp3nS	držet
struny	struna	k1gFnPc4	struna
ve	v	k7c4	v
správné	správný	k2eAgNnSc4d1	správné
<g />
.	.	kIx.	.
</s>
<s>
pozici	pozice	k1gFnSc4	pozice
nad	nad	k7c7	nad
hmatníkem	hmatník	k1gInSc7	hmatník
hmatník	hmatník	k1gInSc1	hmatník
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
nataženy	natažen	k2eAgFnPc4d1	natažena
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
zespoda	zespoda	k7c2	zespoda
zaoblený	zaoblený	k2eAgInSc4d1	zaoblený
<g/>
,	,	kIx,	,
navrchu	navrchu	k6eAd1	navrchu
plochý	plochý	k2eAgMnSc1d1	plochý
pražce	pražec	k1gInSc2	pražec
-	-	kIx~	-
úzké	úzký	k2eAgInPc4d1	úzký
kovové	kovový	k2eAgInPc4d1	kovový
pásky	pásek	k1gInPc4	pásek
vsazené	vsazený	k2eAgInPc4d1	vsazený
do	do	k7c2	do
hmatníku	hmatník	k1gInSc2	hmatník
<g/>
,	,	kIx,	,
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
krk	krk	k1gInSc4	krk
na	na	k7c4	na
půltóny	půltón	k1gInPc4	půltón
<g/>
,	,	kIx,	,
tisknutím	tisknutí	k1gNnSc7	tisknutí
strun	struna	k1gFnPc2	struna
prsty	prst	k1gInPc4	prst
mezi	mezi	k7c7	mezi
pražci	pražec	k1gInPc7	pražec
jsou	být	k5eAaImIp3nP	být
zkracovány	zkracován	k2eAgFnPc4d1	zkracována
znějící	znějící	k2eAgFnPc4d1	znějící
části	část	k1gFnPc4	část
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
změny	změna	k1gFnSc2	změna
<g />
.	.	kIx.	.
</s>
<s>
hraného	hraný	k2eAgInSc2d1	hraný
tónu	tón	k1gInSc2	tón
krk	krk	k1gInSc1	krk
-	-	kIx~	-
úzký	úzký	k2eAgInSc1d1	úzký
kus	kus	k1gInSc1	kus
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
hlavici	hlavice	k1gFnSc4	hlavice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
hmatník	hmatník	k1gInSc4	hmatník
s	s	k7c7	s
pražci	pražec	k1gInSc6	pražec
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
deska	deska	k1gFnSc1	deska
-	-	kIx~	-
vrchní	vrchní	k2eAgFnSc1d1	vrchní
deska	deska	k1gFnSc1	deska
těla	tělo	k1gNnSc2	tělo
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
kobylka	kobylka	k1gFnSc1	kobylka
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
tvořit	tvořit	k5eAaImF	tvořit
zvuk	zvuk	k1gInSc4	zvuk
otvor	otvor	k1gInSc1	otvor
-	-	kIx~	-
pomocí	pomocí	k7c2	pomocí
vibrací	vibrace	k1gFnPc2	vibrace
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
propouští	propouštět	k5eAaImIp3nS	propouštět
zvuk	zvuk	k1gInSc1	zvuk
nástroje	nástroj	k1gInSc2	nástroj
struny	struna	k1gFnSc2	struna
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
<g />
.	.	kIx.	.
</s>
<s>
zdrojem	zdroj	k1gInSc7	zdroj
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
vibrují	vibrovat	k5eAaImIp3nP	vibrovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
výškách	výška	k1gFnPc6	výška
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
drnká	drnkat	k5eAaImIp3nS	drnkat
můstek	můstek	k1gInSc1	můstek
-	-	kIx~	-
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
přenáší	přenášet	k5eAaImIp3nS	přenášet
vibraci	vibrace	k1gFnSc4	vibrace
strun	struna	k1gFnPc2	struna
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
nástroje	nástroj	k1gInSc2	nástroj
sedlo	sednout	k5eAaPmAgNnS	sednout
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
na	na	k7c6	na
můstku	můstek	k1gInSc6	můstek
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nP	držet
struny	struna	k1gFnPc4	struna
nad	nad	k7c7	nad
hmatníkem	hmatník	k1gInSc7	hmatník
kobylka	kobylka	k1gFnSc1	kobylka
-	-	kIx~	-
tvoří	tvořit	k5eAaImIp3nS	tvořit
ji	on	k3xPp3gFnSc4	on
můstek	můstek	k1gInSc1	můstek
a	a	k8xC	a
sedlo	sedlo	k1gNnSc1	sedlo
Ukulele	ukulele	k1gFnSc2	ukulele
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
různé	různý	k2eAgFnPc4d1	různá
velikosti	velikost	k1gFnPc4	velikost
<g/>
:	:	kIx,	:
sopránové	sopránový	k2eAgInPc4d1	sopránový
(	(	kIx(	(
<g/>
21	[number]	k4	21
palců	palec	k1gInPc2	palec
)	)	kIx)	)
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgFnSc1d1	koncertní
(	(	kIx(	(
<g/>
23	[number]	k4	23
palců	palec	k1gInPc2	palec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenorové	tenorový	k2eAgFnSc2d1	Tenorová
(	(	kIx(	(
<g/>
26	[number]	k4	26
palců	palec	k1gInPc2	palec
<g/>
)	)	kIx)	)
a	a	k8xC	a
barytonové	baryton	k1gMnPc1	baryton
(	(	kIx(	(
<g/>
30	[number]	k4	30
palců	palec	k1gInPc2	palec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
extrémně	extrémně	k6eAd1	extrémně
malé	malý	k2eAgNnSc1d1	malé
ukulele	ukulele	k1gNnSc1	ukulele
sopranino	sopranin	k2eAgNnSc1d1	sopranino
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
velké	velký	k2eAgNnSc4d1	velké
basové	basový	k2eAgNnSc4d1	basové
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
aby	aby	kYmCp3nP	aby
toho	ten	k3xDgMnSc4	ten
nebylo	být	k5eNaImAgNnS	být
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zhotoveno	zhotoven	k2eAgNnSc1d1	zhotoveno
taky	taky	k9	taky
něco	něco	k3yInSc1	něco
mezi	mezi	k7c7	mezi
banjem	banjo	k1gNnSc7	banjo
a	a	k8xC	a
ukulelem-	ukulelem-	k?	ukulelem-
Banjolele	Banjolel	k1gInSc2	Banjolel
<g/>
.	.	kIx.	.
</s>
<s>
Sopránové	sopránový	k2eAgFnPc1d1	sopránová
ukulele	ukulele	k1gFnPc1	ukulele
-	-	kIx~	-
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
standardní	standardní	k2eAgMnSc1d1	standardní
<g/>
"	"	kIx"	"
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
nejmenší	malý	k2eAgMnSc1d3	nejmenší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
originální	originální	k2eAgFnSc4d1	originální
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgNnSc4d1	koncertní
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vynalezeno	vynaleznout	k5eAaPmNgNnS	vynaleznout
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
zvětšenina	zvětšenina	k1gFnSc1	zvětšenina
sopránového	sopránový	k2eAgMnSc2d1	sopránový
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
větší	veliký	k2eAgMnSc1d2	veliký
a	a	k8xC	a
hlasitější	hlasitý	k2eAgMnSc1d2	hlasitější
<g/>
,	,	kIx,	,
s	s	k7c7	s
hlubším	hluboký	k2eAgInSc7d2	hlubší
tónem	tón	k1gInSc7	tón
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
větší	veliký	k2eAgNnSc4d2	veliký
tenorové	tenorový	k2eAgNnSc4d1	tenorové
ukulele	ukulele	k1gNnSc4	ukulele
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
ještě	ještě	k6eAd1	ještě
hlasitější	hlasitý	k2eAgFnSc1d2	hlasitější
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
hlubší	hluboký	k2eAgInSc4d2	hlubší
basový	basový	k2eAgInSc4d1	basový
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
je	být	k5eAaImIp3nS	být
barytonové	barytonový	k2eAgNnSc1d1	barytonové
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
miniaturní	miniaturní	k2eAgFnSc4d1	miniaturní
čtyřstrunnou	čtyřstrunný	k2eAgFnSc4d1	čtyřstrunná
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgNnSc4d1	stejné
ladění	ladění	k1gNnSc4	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgNnSc1d1	standardní
ladění	ladění	k1gNnSc1	ladění
pro	pro	k7c4	pro
sopránové	sopránový	k2eAgNnSc4d1	sopránové
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgNnSc4d1	koncertní
a	a	k8xC	a
tenorové	tenorový	k2eAgNnSc4d1	tenorové
ukulele	ukulele	k1gNnSc4	ukulele
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
C	C	kA	C
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
c	c	k0	c
<g/>
'	'	kIx"	'
<g/>
e	e	k0	e
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Struna	struna	k1gFnSc1	struna
g	g	kA	g
je	být	k5eAaImIp3nS	být
naladěna	naladěn	k2eAgFnSc1d1	naladěna
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
nejsou	být	k5eNaImIp3nP	být
struny	struna	k1gFnPc1	struna
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
od	od	k7c2	od
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
po	po	k7c4	po
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
<g/>
.	.	kIx.	.
</s>
<s>
Někdo	někdo	k3yInSc1	někdo
preferuje	preferovat	k5eAaImIp3nS	preferovat
ladění	ladění	k1gNnSc4	ladění
na	na	k7c4	na
"	"	kIx"	"
<g/>
malé	malý	k2eAgNnSc4d1	malé
g	g	kA	g
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
g	g	kA	g
o	o	k7c6	o
oktávu	oktáv	k1gInSc6	oktáv
níže	níže	k1gFnSc2	níže
<g/>
.	.	kIx.	.
</s>
<s>
Barytonové	barytonový	k2eAgNnSc1d1	barytonové
ukulele	ukulele	k1gNnSc1	ukulele
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
laděno	ladit	k5eAaImNgNnS	ladit
do	do	k7c2	do
d	d	k?	d
g	g	kA	g
h	h	k?	h
e	e	k0	e
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
ladění	ladění	k1gNnSc1	ladění
4	[number]	k4	4
nejvýše	vysoce	k6eAd3	vysoce
položeným	položený	k2eAgFnPc3d1	položená
strunám	struna	k1gFnPc3	struna
na	na	k7c6	na
kytaře	kytara	k1gFnSc6	kytara
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
kytaristy	kytarista	k1gMnPc4	kytarista
na	na	k7c4	na
barytonové	barytonový	k2eAgNnSc4d1	barytonové
ukulele	ukulele	k1gNnSc4	ukulele
lehčí	lehčit	k5eAaImIp3nS	lehčit
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
ladění	ladění	k1gNnSc1	ladění
sopránových	sopránový	k2eAgNnPc2d1	sopránové
a	a	k8xC	a
koncertních	koncertní	k2eAgNnPc2d1	koncertní
ukulele	ukulele	k1gNnPc2	ukulele
v	v	k7c6	v
D	D	kA	D
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
f	f	k?	f
<g/>
#	#	kIx~	#
<g/>
'	'	kIx"	'
<g/>
h	h	k?	h
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
půltóny	půltón	k1gInPc4	půltón
výše	výše	k1gFnSc2	výše
než	než	k8xS	než
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
c	c	k0	c
<g/>
'	'	kIx"	'
<g/>
e	e	k0	e
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
'	'	kIx"	'
ladění	ladění	k1gNnSc1	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
hráči	hráč	k1gMnPc1	hráč
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
sladšího	sladký	k2eAgInSc2d2	sladší
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
ukulele	ukulele	k1gNnSc6	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ladění	ladění	k1gNnSc1	ladění
bývalo	bývat	k5eAaImAgNnS	bývat
běžné	běžný	k2eAgNnSc1d1	běžné
během	během	k7c2	během
boomu	boom	k1gInSc2	boom
havajské	havajský	k2eAgFnSc2d1	Havajská
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
notových	notový	k2eAgInPc6d1	notový
záznamech	záznam	k1gInPc6	záznam
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
D	D	kA	D
dur	dur	k1gNnSc1	dur
ladění	ladění	k1gNnSc2	ladění
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
čtvrtou	čtvrtá	k1gFnSc7	čtvrtá
strunou	struna	k1gFnSc7	struna
<g/>
,	,	kIx,	,
ad	ad	k7c4	ad
<g/>
'	'	kIx"	'
<g/>
f	f	k?	f
<g/>
#	#	kIx~	#
<g/>
'	'	kIx"	'
<g/>
h	h	k?	h
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
"	"	kIx"	"
<g/>
kanadské	kanadský	k2eAgNnSc4d1	kanadské
ladění	ladění	k1gNnSc4	ladění
<g/>
"	"	kIx"	"
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
školském	školský	k2eAgInSc6d1	školský
systému	systém	k1gInSc6	systém
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
tenorových	tenorový	k2eAgFnPc2d1	Tenorová
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
čtyřstrunné	čtyřstrunný	k2eAgFnPc4d1	čtyřstrunná
ukulele	ukulele	k1gFnPc4	ukulele
existují	existovat	k5eAaImIp3nP	existovat
i	i	k8xC	i
nástroje	nástroj	k1gInPc1	nástroj
6	[number]	k4	6
strunné	strunný	k2eAgInPc1d1	strunný
a	a	k8xC	a
osmistrunné	osmistrunný	k2eAgInPc1d1	osmistrunný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
šestistrunného	šestistrunný	k2eAgNnSc2d1	šestistrunné
ukulele	ukulele	k1gNnSc2	ukulele
se	se	k3xPyFc4	se
zdvojuje	zdvojovat	k5eAaImIp3nS	zdvojovat
druhá	druhý	k4xOgFnSc1	druhý
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
struna	struna	k1gFnSc1	struna
(	(	kIx(	(
u	u	k7c2	u
ladění	ladění	k1gNnSc2	ladění
GCEA	GCEA	kA	GCEA
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
dvojité	dvojitý	k2eAgFnSc2d1	dvojitá
C	C	kA	C
a	a	k8xC	a
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
ladí	ladit	k5eAaImIp3nP	ladit
přidané	přidaný	k2eAgFnSc2d1	přidaná
struny	struna	k1gFnSc2	struna
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnSc1d1	další
struna	struna	k1gFnSc1	struna
C	C	kA	C
bude	být	k5eAaImBp3nS	být
laděna	laděn	k2eAgFnSc1d1	laděna
o	o	k7c4	o
oktavu	oktava	k1gFnSc4	oktava
výše	výše	k1gFnSc2	výše
a	a	k8xC	a
přidaná	přidaný	k2eAgFnSc1d1	přidaná
struna	struna	k1gFnSc1	struna
A	a	k9	a
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ladění	ladění	k1gNnSc1	ladění
se	se	k3xPyFc4	se
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
poslední	poslední	k2eAgFnSc2d1	poslední
havajské	havajský	k2eAgFnSc2d1	Havajská
královny	královna	k1gFnSc2	královna
nazývá	nazývat	k5eAaImIp3nS	nazývat
Lili	Lili	k1gFnSc1	Lili
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Liliuokalani	Liliuokalaň	k1gFnSc3	Liliuokalaň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osmistrunného	osmistrunný	k2eAgInSc2d1	osmistrunný
nástroje	nástroj	k1gInSc2	nástroj
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
struny	struna	k1gFnPc1	struna
zdvojeny	zdvojen	k2eAgFnPc1d1	zdvojena
<g/>
.	.	kIx.	.
</s>
<s>
Šestistrunné	šestistrunný	k2eAgFnPc1d1	šestistrunná
ukulele	ukulele	k1gFnPc1	ukulele
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
ladí	ladit	k5eAaImIp3nP	ladit
na	na	k7c4	na
frekvence	frekvence	k1gFnPc4	frekvence
podle	podle	k7c2	podle
následujícího	následující	k2eAgNnSc2d1	následující
schematu	schema	k1gNnSc2	schema
<g/>
:	:	kIx,	:
G.	G.	kA	G.
<g/>
...	...	k?	...
<g/>
.196	.196	k4	.196
Hz	Hz	kA	Hz
C.	C.	kA	C.
<g/>
...	...	k?	...
<g/>
.532	.532	k4	.532
Hz	Hz	kA	Hz
C.	C.	kA	C.
<g/>
...	...	k?	...
<g/>
.261	.261	k4	.261
Hz	Hz	kA	Hz
E.	E.	kA	E.
<g/>
...	...	k?	...
<g/>
.329	.329	k4	.329
Hz	Hz	kA	Hz
A.	A.	kA	A.
<g/>
...	...	k?	...
<g/>
.440	.440	k4	.440
Hz	Hz	kA	Hz
A.	A.	kA	A.
<g/>
...	...	k?	...
<g/>
.220	.220	k4	.220
Hz	Hz	kA	Hz
Různé	různý	k2eAgFnSc2d1	různá
varianty	varianta	k1gFnSc2	varianta
ukulele	ukulele	k1gFnSc1	ukulele
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
smíšené	smíšený	k2eAgInPc1d1	smíšený
nástroje	nástroj	k1gInPc1	nástroj
jako	jako	k8xC	jako
banjo	banjo	k1gNnSc1	banjo
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
,	,	kIx,	,
harfa	harfa	k1gFnSc1	harfa
ukulele	ukulele	k1gFnSc1	ukulele
a	a	k8xC	a
lap	lap	k1gInSc1	lap
steel	steela	k1gFnPc2	steela
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
elektricky	elektricky	k6eAd1	elektricky
ozvučená	ozvučený	k2eAgFnSc1d1	ozvučená
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgFnSc2d1	elektrická
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Rezonátorové	Rezonátorový	k2eAgNnSc1d1	Rezonátorový
ukulele	ukulele	k1gNnSc1	ukulele
je	být	k5eAaImIp3nS	být
hlasitější	hlasitý	k2eAgNnSc1d2	hlasitější
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
kvalitu	kvalita	k1gFnSc4	kvalita
tónu	tón	k1gInSc2	tón
než	než	k8xS	než
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
ukulele	ukulele	k1gFnSc1	ukulele
<g/>
.	.	kIx.	.
</s>
<s>
Vydávají	vydávat	k5eAaImIp3nP	vydávat
zvuk	zvuk	k1gInSc4	zvuk
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
zatočenými	zatočený	k2eAgInPc7d1	zatočený
aluminiovými	aluminiový	k2eAgInPc7d1	aluminiový
kužely	kužel	k1gInPc7	kužel
(	(	kIx(	(
<g/>
rezonátory	rezonátor	k1gInPc7	rezonátor
<g/>
)	)	kIx)	)
místo	místo	k7c2	místo
horní	horní	k2eAgFnSc2d1	horní
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
unikátní	unikátní	k2eAgFnSc7d1	unikátní
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
tahitské	tahitský	k2eAgNnSc4d1	tahitské
ukulele	ukulele	k1gNnSc4	ukulele
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vyřezáno	vyřezat	k5eAaPmNgNnS	vyřezat
z	z	k7c2	z
jediného	jediný	k2eAgInSc2d1	jediný
kusu	kus	k1gInSc2	kus
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
duté	dutý	k2eAgNnSc4d1	duté
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Blízké	blízký	k2eAgFnPc1d1	blízká
příbuzné	příbuzná	k1gFnPc1	příbuzná
ukulele	ukulele	k1gNnSc2	ukulele
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
portugalští	portugalský	k2eAgMnPc1d1	portugalský
předchůdci	předchůdce	k1gMnPc1	předchůdce
cavaquinho	cavaquinze	k6eAd1	cavaquinze
(	(	kIx(	(
<g/>
také	také	k9	také
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
machete	machat	k5eAaBmIp2nP	machat
nebo	nebo	k8xC	nebo
braquinha	braquinha	k1gFnSc1	braquinha
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepatrně	nepatrně	k6eAd1	nepatrně
větší	veliký	k2eAgNnSc4d2	veliký
rajã	rajã	k?	rajã
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
strunné	strunný	k2eAgInPc1d1	strunný
nástroje	nástroj	k1gInPc1	nástroj
jsou	být	k5eAaImIp3nP	být
portorikánská	portorikánský	k2eAgNnPc4d1	portorikánský
bordonua	bordonuum	k1gNnPc4	bordonuum
<g/>
,	,	kIx,	,
venezuelské	venezuelský	k2eAgNnSc1d1	venezuelské
cuatro	cuatro	k1gNnSc1	cuatro
<g/>
,	,	kIx,	,
kolumbijské	kolumbijský	k2eAgNnSc1d1	kolumbijské
tiple	tiple	k1gNnSc1	tiple
<g/>
,	,	kIx,	,
timple	timple	k6eAd1	timple
z	z	k7c2	z
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
španělská	španělský	k2eAgFnSc1d1	španělská
vihuela	vihuela	k1gFnSc1	vihuela
a	a	k8xC	a
bolivijské	bolivijský	k2eAgNnSc1d1	bolivijské
charango	charango	k1gNnSc1	charango
tradičně	tradičně	k6eAd1	tradičně
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
z	z	k7c2	z
krunýře	krunýř	k1gInSc2	krunýř
pásovce	pásovec	k1gMnSc2	pásovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
existuje	existovat	k5eAaImIp3nS	existovat
nástroj	nástroj	k1gInSc1	nástroj
kroncong	kroncong	k1gInSc1	kroncong
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
portugalskými	portugalský	k2eAgInPc7d1	portugalský
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
používá	používat	k5eAaImIp3nS	používat
speciální	speciální	k2eAgFnPc4d1	speciální
staré	starý	k2eAgFnPc4d1	stará
techniky	technika	k1gFnPc4	technika
hraní	hraní	k1gNnSc2	hraní
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc6	který
amatérští	amatérský	k2eAgMnPc1d1	amatérský
hráči	hráč	k1gMnPc1	hráč
nemají	mít	k5eNaImIp3nP	mít
ani	ani	k8xC	ani
ponětí	ponětí	k1gNnPc4	ponětí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jake	Jak	k1gFnPc1	Jak
Shimabukuro	Shimabukura	k1gFnSc5	Shimabukura
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
ukulelistů	ukulelista	k1gMnPc2	ukulelista
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
ho	on	k3xPp3gMnSc4	on
naučila	naučit	k5eAaPmAgFnS	naučit
první	první	k4xOgInSc4	první
akord	akord	k1gInSc4	akord
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
hraní	hraní	k1gNnSc4	hraní
zanechat	zanechat	k5eAaPmF	zanechat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
znám	znám	k2eAgInSc1d1	znám
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
ukulele	ukulele	k1gNnPc7	ukulele
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
často	často	k6eAd1	často
hraje	hrát	k5eAaImIp3nS	hrát
přes	přes	k7c4	přes
zkreslený	zkreslený	k2eAgInSc4d1	zkreslený
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Jake	Jake	k1gFnSc5	Jake
Simabukuro	Simabukura	k1gFnSc5	Simabukura
prozatím	prozatím	k6eAd1	prozatím
úspěšně	úspěšně	k6eAd1	úspěšně
nahrál	nahrát	k5eAaBmAgMnS	nahrát
devět	devět	k4xCc4	devět
alb	album	k1gNnPc2	album
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
tohoto	tento	k3xDgInSc2	tento
malého	malý	k2eAgInSc2d1	malý
orchestru	orchestr	k1gInSc2	orchestr
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
,	,	kIx,	,
zpívají	zpívat	k5eAaImIp3nP	zpívat
a	a	k8xC	a
dělají	dělat	k5eAaImIp3nP	dělat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
zábavu	zábava	k1gFnSc4	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
interpretovat	interpretovat	k5eAaBmF	interpretovat
všechny	všechen	k3xTgInPc4	všechen
žánry	žánr	k1gInPc4	žánr
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
vystoupení	vystoupení	k1gNnPc1	vystoupení
jsou	být	k5eAaImIp3nP	být
zábavná	zábavný	k2eAgNnPc1d1	zábavné
<g/>
,	,	kIx,	,
virtuózní	virtuózní	k2eAgNnPc1d1	virtuózní
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
prvky	prvek	k1gInPc4	prvek
rokenrolu	rokenrol	k1gInSc2	rokenrol
a	a	k8xC	a
směs	směs	k1gFnSc4	směs
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgInPc4	žádný
bicí	bicí	k2eAgInPc4d1	bicí
<g/>
,	,	kIx,	,
piána	piána	k?	piána
ani	ani	k8xC	ani
banja	banjo	k1gNnSc2	banjo
zde	zde	k6eAd1	zde
nenajdeme	najít	k5eNaPmIp1nP	najít
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
skladby	skladba	k1gFnPc1	skladba
od	od	k7c2	od
Čajkovského	Čajkovský	k2eAgNnSc2d1	Čajkovský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
od	od	k7c2	od
Nirvany	Nirvan	k1gMnPc7	Nirvan
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
a	a	k8xC	a
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
získal	získat	k5eAaPmAgMnS	získat
velkou	velký	k2eAgFnSc4d1	velká
oblibu	obliba	k1gFnSc4	obliba
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
představením	představení	k1gNnSc7	představení
v	v	k7c6	v
televizních	televizní	k2eAgFnPc6d1	televizní
stanicích	stanice	k1gFnPc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc4	člen
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Dave	Dav	k1gInSc5	Dav
Suich	Suich	k1gMnSc1	Suich
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Brooke-Turner	Brooke-Turner	k1gMnSc1	Brooke-Turner
<g/>
,	,	kIx,	,
Hester	Hester	k1gMnSc1	Hester
Goodman	Goodman	k1gMnSc1	Goodman
<g/>
,	,	kIx,	,
George	Georgus	k1gMnSc5	Georgus
Hinchliffe	Hinchliff	k1gMnSc5	Hinchliff
<g/>
,	,	kIx,	,
Richie	Richius	k1gMnPc4	Richius
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
,	,	kIx,	,
Kitty	Kitta	k1gFnPc1	Kitta
Lux	Lux	k1gMnSc1	Lux
<g/>
,	,	kIx,	,
Will	Will	k1gMnSc1	Will
Grove-White	Grove-Whit	k1gInSc5	Grove-Whit
a	a	k8xC	a
Jonty	Jont	k1gInPc7	Jont
Bankes	Bankesa	k1gFnPc2	Bankesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
netradičním	tradiční	k2eNgNnSc6d1	netradiční
podání	podání	k1gNnSc6	podání
hraje	hrát	k5eAaImIp3nS	hrát
tato	tento	k3xDgFnSc1	tento
brněnská	brněnský	k2eAgFnSc1d1	brněnská
formace	formace	k1gFnSc1	formace
deseti	deset	k4xCc2	deset
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ukulelí	ukulele	k1gFnPc2	ukulele
<g/>
,	,	kIx,	,
cajonu	cajon	k1gInSc2	cajon
a	a	k8xC	a
baskytary	baskytara	k1gFnSc2	baskytara
skladby	skladba	k1gFnSc2	skladba
od	od	k7c2	od
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
,	,	kIx,	,
Leonarda	Leonardo	k1gMnSc2	Leonardo
Cohena	Cohen	k1gMnSc2	Cohen
<g/>
,	,	kIx,	,
Billa	Bill	k1gMnSc2	Bill
Haleyho	Haley	k1gMnSc2	Haley
<g/>
,	,	kIx,	,
Ritchieho	Ritchie	k1gMnSc2	Ritchie
Valense	Valens	k1gMnSc2	Valens
<g/>
,	,	kIx,	,
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
Boba	Bob	k1gMnSc2	Bob
Marleyho	Marley	k1gMnSc2	Marley
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc1	sex
Pistols	Pistolsa	k1gFnPc2	Pistolsa
<g/>
,	,	kIx,	,
Iggyho	Iggy	k1gMnSc2	Iggy
Popa	pop	k1gMnSc2	pop
<g/>
,	,	kIx,	,
The	The	k1gFnSc2	The
Clash	Clasha	k1gFnPc2	Clasha
<g/>
,	,	kIx,	,
Toma	Tom	k1gMnSc2	Tom
Jonese	Jonese	k1gFnSc2	Jonese
<g/>
,	,	kIx,	,
Depeche	Depech	k1gFnSc2	Depech
Mode	modus	k1gInSc5	modus
<g/>
,	,	kIx,	,
Steppenwolf	Steppenwolf	k1gMnSc1	Steppenwolf
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Doors	Doors	k1gInSc1	Doors
...	...	k?	...
a	a	k8xC	a
spousty	spousta	k1gFnSc2	spousta
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
založil	založit	k5eAaPmAgInS	založit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
René	René	k1gFnSc6	René
Senko	Senko	k1gNnSc4	Senko
<g/>
,	,	kIx,	,
působící	působící	k2eAgInPc1d1	působící
také	také	k9	také
v	v	k7c4	v
další	další	k2eAgFnPc4d1	další
fenomenální	fenomenální	k2eAgFnPc4d1	fenomenální
world-music	worldusice	k1gFnPc2	world-musice
kapele	kapela	k1gFnSc3	kapela
Čankišou	Čankišou	k1gMnPc1	Čankišou
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
věkově	věkově	k6eAd1	věkově
i	i	k8xC	i
hudebními	hudební	k2eAgFnPc7d1	hudební
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
pestrá	pestrý	k2eAgFnSc1d1	pestrá
parta	parta	k1gFnSc1	parta
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
příznivců	příznivec	k1gMnPc2	příznivec
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
impulsem	impuls	k1gInSc7	impuls
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
kapely	kapela	k1gFnSc2	kapela
byla	být	k5eAaImAgFnS	být
inspirace	inspirace	k1gFnSc1	inspirace
knihou	kniha	k1gFnSc7	kniha
Toma	Tom	k1gMnSc2	Tom
Hodgkinsona	Hodgkinson	k1gMnSc2	Hodgkinson
Líný	líný	k2eAgMnSc1d1	líný
rodič	rodič	k1gMnSc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
propagátor	propagátor	k1gMnSc1	propagátor
ukulele	ukulele	k1gFnSc2	ukulele
<g/>
,	,	kIx,	,
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
přednosti	přednost	k1gFnPc4	přednost
tohoto	tento	k3xDgInSc2	tento
skladného	skladný	k2eAgInSc2d1	skladný
nástroje	nástroj	k1gInSc2	nástroj
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
ukulele	ukulele	k1gFnSc1	ukulele
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
a	a	k8xC	a
neměl	mít	k5eNaImAgInS	mít
by	by	kYmCp3nS	by
chybět	chybět	k5eAaImF	chybět
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvních	první	k4xOgNnPc6	první
vystoupeních	vystoupení	k1gNnPc6	vystoupení
na	na	k7c6	na
Vánoce	Vánoce	k1gFnPc1	Vánoce
2010	[number]	k4	2010
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
seskupení	seskupení	k1gNnSc1	seskupení
<g/>
,	,	kIx,	,
pestré	pestrý	k2eAgInPc1d1	pestrý
jak	jak	k6eAd1	jak
věkem	věk	k1gInSc7	věk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
hudebními	hudební	k2eAgFnPc7d1	hudební
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
si	se	k3xPyFc3	se
vybudovalo	vybudovat	k5eAaPmAgNnS	vybudovat
osobitý	osobitý	k2eAgInSc4d1	osobitý
přístup	přístup	k1gInSc4	přístup
a	a	k8xC	a
také	také	k9	také
repertoár	repertoár	k1gInSc1	repertoár
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
coververzí	coververze	k1gFnPc2	coververze
známých	známý	k2eAgInPc2d1	známý
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
prezentovat	prezentovat	k5eAaBmF	prezentovat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zejména	zejména	k6eAd1	zejména
pobavit	pobavit	k5eAaPmF	pobavit
sebe	sebe	k3xPyFc4	sebe
i	i	k8xC	i
posluchače	posluchač	k1gMnPc4	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
kapela	kapela	k1gFnSc1	kapela
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
veřejných	veřejný	k2eAgNnPc2d1	veřejné
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
,	,	kIx,	,
od	od	k7c2	od
letních	letní	k2eAgInPc2d1	letní
festivalů	festival	k1gInPc2	festival
a	a	k8xC	a
vystoupení	vystoupení	k1gNnPc2	vystoupení
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
přes	přes	k7c4	přes
klubové	klubový	k2eAgNnSc4d1	klubové
hraní	hraní	k1gNnSc4	hraní
nebo	nebo	k8xC	nebo
soukromé	soukromý	k2eAgFnPc4d1	soukromá
a	a	k8xC	a
firemní	firemní	k2eAgFnPc4d1	firemní
oslavy	oslava	k1gFnPc4	oslava
až	až	k9	až
po	po	k7c4	po
komornější	komorní	k2eAgFnPc4d2	komornější
akce	akce	k1gFnPc4	akce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
představení	představení	k1gNnSc2	představení
Divadla	divadlo	k1gNnSc2	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
.	.	kIx.	.
</s>
<s>
Natočila	natočit	k5eAaBmAgFnS	natočit
tři	tři	k4xCgFnPc4	tři
CD	CD	kA	CD
coververzí	coververze	k1gFnPc2	coververze
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
CD	CD	kA	CD
"	"	kIx"	"
<g/>
Živě	živě	k6eAd1	živě
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
<g/>
/	/	kIx~	/
<g/>
Live	Live	k1gFnSc1	Live
from	from	k6eAd1	from
Brno	Brno	k1gNnSc1	Brno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
CD	CD	kA	CD
"	"	kIx"	"
<g/>
A	A	kA	A
Little	Little	k1gFnSc1	Little
Less	Less	k1gInSc1	Less
Conversation	Conversation	k1gInSc1	Conversation
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
CD	CD	kA	CD
"	"	kIx"	"
<g/>
Mad	Mad	k1gMnSc1	Mad
World	World	k1gMnSc1	World
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
o	o	k7c6	o
ukulelových	ukulelův	k2eAgInPc6d1	ukulelův
orchestrech	orchestr	k1gInPc6	orchestr
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
Ukulele	ukulele	k1gFnSc1	ukulele
Orchestra	orchestra	k1gFnSc1	orchestra
of	of	k?	of
the	the	k?	the
world	world	k6eAd1	world
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ukulele	ukulele	k1gFnSc2	ukulele
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ukulele	ukulele	k1gFnSc2	ukulele
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Web	web	k1gInSc4	web
o	o	k7c4	o
ukulele	ukulele	k1gNnSc4	ukulele
</s>
