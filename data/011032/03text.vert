<p>
<s>
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1926	[number]	k4	1926
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1962	[number]	k4	1962
Brentwood	Brentwood	k1gInSc1	Brentwood
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Norma	Norma	k1gFnSc1	Norma
Jeane	Jean	k1gMnSc5	Jean
Mortenson	Mortenson	k1gNnSc4	Mortenson
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
producentka	producentka	k1gFnSc1	producentka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
strávila	strávit	k5eAaPmAgFnS	strávit
v	v	k7c6	v
pěstounské	pěstounský	k2eAgFnSc6d1	pěstounská
péči	péče	k1gFnSc6	péče
a	a	k8xC	a
sirotčinci	sirotčinec	k1gInSc6	sirotčinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
pak	pak	k6eAd1	pak
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
filmovým	filmový	k2eAgNnSc7d1	filmové
studiem	studio	k1gNnSc7	studio
Twentieth	Twentietha	k1gFnPc2	Twentietha
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
a	a	k8xC	a
věnovala	věnovat	k5eAaPmAgFnS	věnovat
se	se	k3xPyFc4	se
modelingu	modeling	k1gInSc3	modeling
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
džungle	džungle	k1gFnSc1	džungle
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
jí	on	k3xPp3gFnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
první	první	k4xOgInSc4	první
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
následován	následovat	k5eAaImNgInS	následovat
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rolí	role	k1gFnSc7	role
v	v	k7c6	v
melodramatu	melodramatu	k?	melodramatu
Neobtěžuj	obtěžovat	k5eNaImRp2nS	obtěžovat
se	se	k3xPyFc4	se
klepáním	klepání	k1gNnSc7	klepání
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Richarda	Richarda	k1gFnSc1	Richarda
Widmarka	Widmarka	k1gFnSc1	Widmarka
a	a	k8xC	a
též	též	k9	též
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rolí	role	k1gFnSc7	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
noir	noira	k1gFnPc2	noira
Niagara	Niagara	k1gFnSc1	Niagara
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
představitelkou	představitelka	k1gFnSc7	představitelka
klasických	klasický	k2eAgFnPc2d1	klasická
"	"	kIx"	"
<g/>
hloupých	hloupý	k2eAgFnPc2d1	hloupá
blondýnek	blondýnka	k1gFnPc2	blondýnka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
naivní	naivní	k2eAgNnSc1d1	naivní
chování	chování	k1gNnSc1	chování
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
komickým	komický	k2eAgFnPc3d1	komická
situacím	situace	k1gFnPc3	situace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
provdat	provdat	k5eAaPmF	provdat
za	za	k7c2	za
milionáře	milionář	k1gMnSc2	milionář
<g/>
,	,	kIx,	,
Páni	pan	k1gMnPc1	pan
mají	mít	k5eAaImIp3nP	mít
radši	rád	k6eAd2	rád
blondýnky	blondýnka	k1gFnSc2	blondýnka
a	a	k8xC	a
Slaměný	slaměný	k2eAgMnSc1d1	slaměný
vdovec	vdovec	k1gMnSc1	vdovec
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc4d1	následující
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
vymanit	vymanit	k5eAaPmF	vymanit
ze	z	k7c2	z
škatulky	škatulka	k1gFnSc2	škatulka
sexuálního	sexuální	k2eAgInSc2d1	sexuální
symbolu	symbol	k1gInSc2	symbol
filmového	filmový	k2eAgNnSc2d1	filmové
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ukázala	ukázat	k5eAaPmAgFnS	ukázat
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
zastávka	zastávka	k1gFnSc1	zastávka
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
produkci	produkce	k1gFnSc6	produkce
pak	pak	k6eAd1	pak
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Laurence	Laurenec	k1gMnSc2	Laurenec
Oliviera	Olivier	k1gMnSc2	Olivier
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Princ	princa	k1gFnPc2	princa
a	a	k8xC	a
tanečnice	tanečnice	k1gFnPc1	tanečnice
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
cenu	cena	k1gFnSc4	cena
Davida	David	k1gMnSc2	David
di	di	k?	di
Donatella	Donatell	k1gMnSc2	Donatell
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
jí	on	k3xPp3gFnSc2	on
vynesla	vynést	k5eAaPmAgFnS	vynést
role	role	k1gFnSc1	role
Sugar	Sugara	k1gFnPc2	Sugara
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k6eAd1	rád
horké	horký	k2eAgNnSc1d1	horké
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
dokončeným	dokončený	k2eAgInSc7d1	dokončený
filmem	film	k1gInSc7	film
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
drama	drama	k1gNnSc1	drama
Mustangové	mustang	k1gMnPc1	mustang
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
natočené	natočený	k2eAgNnSc1d1	natočené
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
jejího	její	k3xOp3gMnSc2	její
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
manžela	manžel	k1gMnSc2	manžel
Arthura	Arthur	k1gMnSc2	Arthur
Millera	Miller	k1gMnSc2	Miller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
roky	rok	k1gInPc1	rok
svého	svůj	k3xOyFgInSc2	svůj
soukromého	soukromý	k2eAgInSc2d1	soukromý
i	i	k8xC	i
profesního	profesní	k2eAgInSc2d1	profesní
života	život	k1gInSc2	život
bojovala	bojovat	k5eAaImAgFnS	bojovat
nejen	nejen	k6eAd1	nejen
se	s	k7c7	s
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
uklidňujících	uklidňující	k2eAgInPc6d1	uklidňující
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
filmovým	filmový	k2eAgNnSc7d1	filmové
studiem	studio	k1gNnSc7	studio
o	o	k7c4	o
lepší	dobrý	k2eAgNnSc4d2	lepší
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
role	role	k1gFnPc4	role
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
špatnému	špatný	k2eAgInSc3d1	špatný
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
ovšem	ovšem	k9	ovšem
získávala	získávat	k5eAaImAgFnS	získávat
jako	jako	k9	jako
herečka	herečka	k1gFnSc1	herečka
špatnou	špatný	k2eAgFnSc4d1	špatná
reputaci	reputace	k1gFnSc4	reputace
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
neobjasněné	objasněný	k2eNgFnPc1d1	neobjasněná
okolnosti	okolnost	k1gFnPc1	okolnost
její	její	k3xOp3gFnSc3	její
předčasné	předčasný	k2eAgFnSc3d1	předčasná
smrti	smrt	k1gFnSc3	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
pak	pak	k6eAd1	pak
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
spekulací	spekulace	k1gFnPc2	spekulace
a	a	k8xC	a
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
36	[number]	k4	36
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
stala	stát	k5eAaPmAgFnS	stát
kulturní	kulturní	k2eAgFnSc7d1	kulturní
a	a	k8xC	a
filmovou	filmový	k2eAgFnSc7d1	filmová
ikonou	ikona	k1gFnSc7	ikona
<g/>
,	,	kIx,	,
nezpochybnitelným	zpochybnitelný	k2eNgInSc7d1	nezpochybnitelný
sexuálním	sexuální	k2eAgInSc7d1	sexuální
symbolem	symbol	k1gInSc7	symbol
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
stvrdila	stvrdit	k5eAaPmAgFnS	stvrdit
instituce	instituce	k1gFnSc1	instituce
TV	TV	kA	TV
Guide	Guid	k1gInSc5	Guid
Network	network	k1gInSc1	network
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zvolila	zvolit	k5eAaPmAgFnS	zvolit
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
sexy	sex	k1gInPc4	sex
herečku	herečka	k1gFnSc4	herečka
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Norma	Norma	k1gFnSc1	Norma
Jeane	Jean	k1gMnSc5	Jean
Baker	Baker	k1gMnSc1	Baker
Mortenson	Mortenson	k1gInSc4	Mortenson
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1926	[number]	k4	1926
v	v	k7c6	v
General	General	k1gFnSc6	General
Hospital	Hospital	k1gMnSc1	Hospital
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Gladys	Gladysa	k1gFnPc2	Gladysa
nechala	nechat	k5eAaPmAgFnS	nechat
do	do	k7c2	do
matriky	matrika	k1gFnSc2	matrika
zapsat	zapsat	k5eAaPmF	zapsat
příjmení	příjmení	k1gNnSc4	příjmení
Mortenson	Mortenson	k1gInSc4	Mortenson
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
příteli	přítel	k1gMnSc6	přítel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
ještě	ještě	k9	ještě
před	před	k7c7	před
narozením	narození	k1gNnSc7	narození
dcery	dcera	k1gFnSc2	dcera
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
ale	ale	k9	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
otcem	otec	k1gMnSc7	otec
malé	malý	k2eAgFnSc2d1	malá
Normy	Norma	k1gFnSc2	Norma
byl	být	k5eAaImAgInS	být
Stanley	Stanley	k1gInPc4	Stanley
Gifford	Gifforda	k1gFnPc2	Gifforda
<g/>
,	,	kIx,	,
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
firmy	firma	k1gFnSc2	firma
Consolidated	Consolidated	k1gInSc1	Consolidated
Film	film	k1gInSc1	film
Industrion	Industrion	k1gInSc4	Industrion
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracovala	pracovat	k5eAaImAgFnS	pracovat
i	i	k9	i
Gladys	Gladys	k1gInSc4	Gladys
jako	jako	k8xS	jako
filmová	filmový	k2eAgFnSc1d1	filmová
střihačka	střihačka	k1gFnSc1	střihačka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc4	on
Marilyn	Marilyn	k1gFnSc1	Marilyn
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
–	–	k?	–
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
7	[number]	k4	7
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Marilynina	Marilynin	k2eAgFnSc1d1	Marilynin
hvězda	hvězda	k1gFnSc1	hvězda
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
najala	najmout	k5eAaPmAgFnS	najmout
soukromé	soukromý	k2eAgNnSc4d1	soukromé
očko	očko	k1gNnSc4	očko
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
si	se	k3xPyFc3	se
Gifforda	Gifforda	k1gFnSc1	Gifforda
vyhledat	vyhledat	k5eAaPmF	vyhledat
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
i	i	k9	i
tento	tento	k3xDgMnSc1	tento
–	–	k?	–
již	již	k6eAd1	již
poslední	poslední	k2eAgInSc4d1	poslední
–	–	k?	–
kontakt	kontakt	k1gInSc4	kontakt
nedopadl	dopadnout	k5eNaPmAgMnS	dopadnout
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
Gifford	Gifford	k1gInSc1	Gifford
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
nemanželskou	manželský	k2eNgFnSc7d1	nemanželská
dcerou	dcera	k1gFnSc7	dcera
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
po	po	k7c6	po
telefonu	telefon	k1gInSc6	telefon
hovořit	hovořit	k5eAaImF	hovořit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Gladys	Gladys	k1gInSc1	Gladys
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgNnSc7	jaký
byla	být	k5eAaImAgFnS	být
koupě	koupě	k1gFnSc1	koupě
domku	domek	k1gInSc2	domek
<g/>
,	,	kIx,	,
o	o	k7c4	o
dítě	dítě	k1gNnSc4	dítě
nemohla	moct	k5eNaImAgFnS	moct
starat	starat	k5eAaImF	starat
<g/>
.	.	kIx.	.
</s>
<s>
Dědičné	dědičný	k2eAgNnSc1d1	dědičné
zatížení	zatížení	k1gNnSc1	zatížení
psychickými	psychický	k2eAgFnPc7d1	psychická
poruchami	porucha	k1gFnPc7	porucha
jako	jako	k8xS	jako
paranoia	paranoia	k1gFnSc1	paranoia
a	a	k8xC	a
schizofrenie	schizofrenie	k1gFnSc1	schizofrenie
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
fyzické	fyzický	k2eAgNnSc1d1	fyzické
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
způsobené	způsobený	k2eAgNnSc1d1	způsobené
prací	práce	k1gFnSc7	práce
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
směny	směna	k1gFnPc4	směna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázala	dokázat	k5eAaPmAgFnS	dokázat
splatit	splatit	k5eAaPmF	splatit
hypotéku	hypotéka	k1gFnSc4	hypotéka
na	na	k7c4	na
domek	domek	k1gInSc4	domek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podepsaly	podepsat	k5eAaPmAgInP	podepsat
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
mladé	mladý	k1gMnPc4	mladý
Gladys	Gladysa	k1gFnPc2	Gladysa
a	a	k8xC	a
tak	tak	k6eAd1	tak
Norma	Norma	k1gFnSc1	Norma
putovala	putovat	k5eAaImAgFnS	putovat
do	do	k7c2	do
pěstounské	pěstounský	k2eAgFnSc2d1	pěstounská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
příbuzným	příbuzný	k1gMnPc3	příbuzný
a	a	k8xC	a
pak	pak	k6eAd1	pak
zase	zase	k9	zase
do	do	k7c2	do
sirotčince	sirotčinec	k1gInSc2	sirotčinec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávila	strávit	k5eAaPmAgFnS	strávit
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
líčila	líčit	k5eAaImAgFnS	líčit
v	v	k7c6	v
nejtemnějších	temný	k2eAgFnPc6d3	nejtemnější
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
zkreslující	zkreslující	k2eAgNnSc1d1	zkreslující
<g/>
.	.	kIx.	.
</s>
<s>
Necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
po	po	k7c4	po
svých	svůj	k3xOyFgNnPc2	svůj
16	[number]	k4	16
<g/>
.	.	kIx.	.
narozeninách	narozeniny	k1gFnPc6	narozeniny
se	s	k7c7	s
–	–	k?	–
na	na	k7c4	na
popud	popud	k1gInSc4	popud
své	svůj	k3xOyFgFnSc2	svůj
opatrovatelky	opatrovatelka	k1gFnSc2	opatrovatelka
Grace	Grace	k1gMnSc1	Grace
Goddard	Goddard	k1gMnSc1	Goddard
<g/>
,	,	kIx,	,
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Gladys	Gladys	k1gInSc1	Gladys
–	–	k?	–
raději	rád	k6eAd2	rád
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
"	"	kIx"	"
<g/>
chlapce	chlapec	k1gMnPc4	chlapec
od	od	k7c2	od
vedle	vedle	k7c2	vedle
<g/>
"	"	kIx"	"
Jamese	Jamese	k1gFnSc2	Jamese
"	"	kIx"	"
<g/>
Jima	Jimus	k1gMnSc2	Jimus
<g/>
"	"	kIx"	"
Doughertyho	Dougherty	k1gMnSc2	Dougherty
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
až	až	k6eAd1	až
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
18	[number]	k4	18
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
nemusela	muset	k5eNaImAgFnS	muset
pobývat	pobývat	k5eAaImF	pobývat
v	v	k7c6	v
sirotčinci	sirotčinec	k1gInSc6	sirotčinec
<g/>
.	.	kIx.	.
</s>
<s>
Grace	Grace	k1gFnSc1	Grace
Goddard	Goddarda	k1gFnPc2	Goddarda
se	se	k3xPyFc4	se
toužila	toužit	k5eAaImAgFnS	toužit
znova	znova	k6eAd1	znova
provdat	provdat	k5eAaPmF	provdat
a	a	k8xC	a
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
mladičká	mladičký	k2eAgFnSc1d1	mladičká
Norma	Norma	k1gFnSc1	Norma
Jeane	Jean	k1gMnSc5	Jean
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
musela	muset	k5eAaImAgFnS	muset
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
sirotčince	sirotčinec	k1gInSc2	sirotčinec
(	(	kIx(	(
<g/>
Gladys	Gladysa	k1gFnPc2	Gladysa
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
opět	opět	k6eAd1	opět
hospitalizována	hospitalizovat	k5eAaBmNgFnS	hospitalizovat
pro	pro	k7c4	pro
vážné	vážný	k2eAgFnPc4d1	vážná
duševní	duševní	k2eAgFnPc4d1	duševní
poruchy	porucha	k1gFnPc4	porucha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
Grace	Grace	k1gFnSc1	Grace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
malou	malý	k2eAgFnSc4d1	malá
Normu	Norma	k1gFnSc4	Norma
Jeane	Jean	k1gMnSc5	Jean
přivedla	přivést	k5eAaPmAgFnS	přivést
ke	k	k7c3	k
světu	svět	k1gInSc3	svět
filmu	film	k1gInSc2	film
–	–	k?	–
nejprve	nejprve	k6eAd1	nejprve
společně	společně	k6eAd1	společně
s	s	k7c7	s
Gladys	Gladys	k1gInSc1	Gladys
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
když	když	k8xS	když
Gladys	Gladys	k1gInSc1	Gladys
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
trávila	trávit	k5eAaImAgFnS	trávit
v	v	k7c6	v
léčebnách	léčebna	k1gFnPc6	léčebna
–	–	k?	–
vodila	vodit	k5eAaImAgFnS	vodit
Normu	Norma	k1gFnSc4	Norma
do	do	k7c2	do
kina	kino	k1gNnSc2	kino
a	a	k8xC	a
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
ji	on	k3xPp3gFnSc4	on
o	o	k7c6	o
hollywoodských	hollywoodský	k2eAgFnPc6d1	hollywoodská
hvězdách	hvězda	k1gFnPc6	hvězda
a	a	k8xC	a
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
i	i	k9	i
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
bude	být	k5eAaImBp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
velká	velký	k2eAgFnSc1d1	velká
hvězda	hvězda	k1gFnSc1	hvězda
stříbrného	stříbrný	k2eAgNnSc2d1	stříbrné
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
Grace	Grace	k1gFnSc1	Grace
bohužel	bohužel	k9	bohužel
nedočkala	dočkat	k5eNaPmAgFnS	dočkat
–	–	k?	–
když	když	k8xS	když
stála	stát	k5eAaImAgFnS	stát
Marilyn	Marilyn	k1gFnSc4	Marilyn
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
herečkou	herečka	k1gFnSc7	herečka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
již	již	k9	již
Grace	Grace	k1gFnSc1	Grace
těžce	těžce	k6eAd1	těžce
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c6	na
předávkování	předávkování	k1gNnSc6	předávkování
barbituráty	barbiturát	k1gInPc1	barbiturát
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Westwood	Westwooda	k1gFnPc2	Westwooda
Village	Villag	k1gInSc2	Villag
Memorial	Memorial	k1gInSc4	Memorial
Park	park	k1gInSc1	park
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Manželství	manželství	k1gNnSc1	manželství
Normy	Norma	k1gFnSc2	Norma
a	a	k8xC	a
Jima	Jimum	k1gNnSc2	Jimum
bylo	být	k5eAaImAgNnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říct	říct	k5eAaPmF	říct
pohodové	pohodový	k2eAgFnPc1d1	pohodová
<g/>
.	.	kIx.	.
</s>
<s>
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
ale	ale	k9	ale
dotkla	dotknout	k5eAaPmAgFnS	dotknout
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
narukoval	narukovat	k5eAaPmAgMnS	narukovat
k	k	k7c3	k
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
a	a	k8xC	a
Normu	Norma	k1gFnSc4	Norma
stereotyp	stereotyp	k1gInSc4	stereotyp
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
padáky	padák	k1gInPc4	padák
společnosti	společnost	k1gFnSc2	společnost
Radioplane	Radioplan	k1gMnSc5	Radioplan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
natírala	natírat	k5eAaImAgFnS	natírat
i	i	k8xC	i
trupy	trupa	k1gFnPc1	trupa
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
upřímně	upřímně	k6eAd1	upřímně
nudit	nudit	k5eAaImF	nudit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
vytrhl	vytrhnout	k5eAaPmAgMnS	vytrhnout
armádní	armádní	k2eAgMnSc1d1	armádní
fotograf	fotograf	k1gMnSc1	fotograf
David	David	k1gMnSc1	David
Connover	Connover	k1gMnSc1	Connover
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
Marilyn	Marilyn	k1gFnSc2	Marilyn
<g/>
.	.	kIx.	.
</s>
<s>
Nafotil	nafotit	k5eAaPmAgMnS	nafotit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
pracovní	pracovní	k2eAgFnSc6d1	pracovní
kombinéze	kombinéza	k1gFnSc6	kombinéza
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
další	další	k2eAgFnSc4d1	další
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ale	ale	k9	ale
Jim	on	k3xPp3gMnPc3	on
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Začátek	začátek	k1gInSc1	začátek
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Sotva	sotva	k6eAd1	sotva
dvacetiletá	dvacetiletý	k2eAgFnSc1d1	dvacetiletá
tak	tak	k9	tak
začala	začít	k5eAaPmAgFnS	začít
pózovat	pózovat	k5eAaImF	pózovat
známým	známý	k1gMnPc3	známý
i	i	k8xC	i
méně	málo	k6eAd2	málo
známým	známý	k2eAgMnPc3d1	známý
fotografům	fotograf	k1gMnPc3	fotograf
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
jako	jako	k8xS	jako
brunetka	brunetka	k1gFnSc1	brunetka
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
titulních	titulní	k2eAgFnPc6d1	titulní
stránkách	stránka	k1gFnPc6	stránka
magazínů	magazín	k1gInPc2	magazín
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
ambice	ambice	k1gFnPc1	ambice
ovšem	ovšem	k9	ovšem
sahaly	sahat	k5eAaImAgFnP	sahat
dál	daleko	k6eAd2	daleko
–	–	k?	–
toužila	toužit	k5eAaImAgFnS	toužit
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Obcházela	obcházet	k5eAaImAgFnS	obcházet
castingy	casting	k1gInPc4	casting
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
různými	různý	k2eAgNnPc7d1	různé
jmény	jméno	k1gNnPc7	jméno
zkoušela	zkoušet	k5eAaImAgFnS	zkoušet
prorazit	prorazit	k5eAaPmF	prorazit
u	u	k7c2	u
Columbia	Columbia	k1gFnSc1	Columbia
Records	Recordsa	k1gFnPc2	Recordsa
a	a	k8xC	a
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc4	fox
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Mihla	mihnout	k5eAaPmAgFnS	mihnout
se	se	k3xPyFc4	se
v	v	k7c6	v
pár	pár	k4xCyI	pár
filmech	film	k1gInPc6	film
kategorie	kategorie	k1gFnSc2	kategorie
B	B	kA	B
<g/>
,	,	kIx,	,
rozeznat	rozeznat	k5eAaPmF	rozeznat
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
bratří	bratřit	k5eAaImIp3nS	bratřit
Marxů	Marx	k1gMnPc2	Marx
Šťastná	šťastný	k2eAgFnSc1d1	šťastná
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Pomohlo	pomoct	k5eAaPmAgNnS	pomoct
jí	on	k3xPp3gFnSc3	on
až	až	k6eAd1	až
setkání	setkání	k1gNnPc1	setkání
s	s	k7c7	s
padesátiletým	padesátiletý	k2eAgInSc7d1	padesátiletý
filmovým	filmový	k2eAgInSc7d1	filmový
agentem	agens	k1gInSc7	agens
Johny	John	k1gMnPc7	John
Hydem	Hyd	k1gMnSc7	Hyd
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
snad	snad	k9	snad
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
věřil	věřit	k5eAaImAgMnS	věřit
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
upřímně	upřímně	k6eAd1	upřímně
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k6eAd1	jistě
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
za	za	k7c7	za
tím	ten	k3xDgMnSc7	ten
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k8xC	jak
sama	sám	k3xTgFnSc1	sám
Marilyn	Marilyn	k1gFnSc1	Marilyn
později	pozdě	k6eAd2	pozdě
připustila	připustit	k5eAaPmAgFnS	připustit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nabízel	nabízet	k5eAaImAgMnS	nabízet
jí	jíst	k5eAaImIp3nS	jíst
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
jistě	jistě	k6eAd1	jistě
katapultovalo	katapultovat	k5eAaBmAgNnS	katapultovat
mezi	mezi	k7c4	mezi
elitu	elita	k1gFnSc4	elita
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
ale	ale	k9	ale
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
Johnyho	Johny	k1gMnSc2	Johny
prostě	prostě	k9	prostě
nemilovala	milovat	k5eNaImAgFnS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Využila	využít	k5eAaPmAgFnS	využít
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc2	jeho
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
džungle	džungle	k1gFnSc1	džungle
renomovaného	renomovaný	k2eAgMnSc2d1	renomovaný
Johna	John	k1gMnSc2	John
Hustona	Huston	k1gMnSc2	Huston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1950	[number]	k4	1950
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
znamenal	znamenat	k5eAaImAgInS	znamenat
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
a	a	k8xC	a
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
filmovým	filmový	k2eAgNnSc7d1	filmové
studiem	studio	k1gNnSc7	studio
na	na	k7c4	na
75	[number]	k4	75
dolarů	dolar	k1gInPc2	dolar
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Šéf	šéf	k1gMnSc1	šéf
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
Darryl	Darryl	k1gInSc1	Darryl
Zanuck	Zanucka	k1gFnPc2	Zanucka
sice	sice	k8xC	sice
Marilyn	Marilyn	k1gFnSc4	Marilyn
nemohl	moct	k5eNaImAgMnS	moct
vystát	vystát	k5eAaPmF	vystát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
uznat	uznat	k5eAaPmF	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
diváci	divák	k1gMnPc1	divák
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc3	on
začínají	začínat	k5eAaImIp3nP	začínat
všímat	všímat	k5eAaImF	všímat
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
na	na	k7c6	na
reklamních	reklamní	k2eAgInPc6d1	reklamní
plakátech	plakát	k1gInPc6	plakát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
role	role	k1gFnPc4	role
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
Evě	Eva	k1gFnSc6	Eva
<g/>
,	,	kIx,	,
Tak	tak	k6eAd1	tak
mladý	mladý	k2eAgMnSc1d1	mladý
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
cítíš	cítit	k5eAaImIp2nS	cítit
<g/>
,	,	kIx,	,
Omlazovací	omlazovací	k2eAgInSc1d1	omlazovací
prostředek	prostředek	k1gInSc1	prostředek
nebo	nebo	k8xC	nebo
Ve	v	k7c6	v
spárech	spár	k1gInPc6	spár
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
reklamních	reklamní	k2eAgInPc6d1	reklamní
poutačích	poutač	k1gInPc6	poutač
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
už	už	k6eAd1	už
zastínila	zastínit	k5eAaPmAgFnS	zastínit
tehdy	tehdy	k6eAd1	tehdy
velice	velice	k6eAd1	velice
populární	populární	k2eAgFnSc4d1	populární
Barbaru	Barbara	k1gFnSc4	Barbara
Stanwyck	Stanwycka	k1gFnPc2	Stanwycka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
fotografiích	fotografia	k1gFnPc6	fotografia
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
více	hodně	k6eAd2	hodně
než	než	k8xS	než
lechtivých	lechtivý	k2eAgInPc2d1	lechtivý
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jí	on	k3xPp3gFnSc3	on
22	[number]	k4	22
<g/>
,	,	kIx,	,
když	když	k8xS	když
–	–	k?	–
jak	jak	k6eAd1	jak
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
později	pozdě	k6eAd2	pozdě
–	–	k?	–
měla	mít	k5eAaImAgFnS	mít
hlad	hlad	k1gInSc4	hlad
a	a	k8xC	a
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
jezdila	jezdit	k5eAaImAgFnS	jezdit
na	na	k7c4	na
castingy	casting	k1gInPc4	casting
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
kývla	kývnout	k5eAaPmAgFnS	kývnout
na	na	k7c4	na
nabídku	nabídka	k1gFnSc4	nabídka
Toma	Tom	k1gMnSc2	Tom
Kellyho	Kelly	k1gMnSc2	Kelly
pózovat	pózovat	k5eAaImF	pózovat
pro	pro	k7c4	pro
umělecké	umělecký	k2eAgInPc4d1	umělecký
akty	akt	k1gInPc4	akt
<g/>
.	.	kIx.	.
</s>
<s>
Darryl	Darryl	k1gInSc1	Darryl
Zanuck	Zanuck	k1gInSc1	Zanuck
po	po	k7c6	po
provalení	provalení	k1gNnSc6	provalení
identity	identita	k1gFnSc2	identita
oné	onen	k3xDgFnSc2	onen
dívky	dívka	k1gFnSc2	dívka
z	z	k7c2	z
kalendáře	kalendář	k1gInSc2	kalendář
zuřil	zuřit	k5eAaImAgMnS	zuřit
<g/>
,	,	kIx,	,
ženské	ženský	k2eAgInPc1d1	ženský
spolky	spolek	k1gInPc1	spolek
byly	být	k5eAaImAgInP	být
pobouřené	pobouřený	k2eAgInPc1d1	pobouřený
Marilyninou	Marilynin	k2eAgFnSc7d1	Marilynin
nahotou	nahota	k1gFnSc7	nahota
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
chytrý	chytrý	k2eAgInSc1d1	chytrý
komentář	komentář	k1gInSc1	komentář
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
záležitosti	záležitost	k1gFnSc3	záležitost
ale	ale	k8xC	ale
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
ženy	žena	k1gFnPc1	žena
litovaly	litovat	k5eAaImAgFnP	litovat
a	a	k8xC	a
muži	muž	k1gMnPc1	muž
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
zbožňovali	zbožňovat	k5eAaImAgMnP	zbožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc1	jeden
moment	moment	k1gInSc1	moment
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Omlazovací	omlazovací	k2eAgInSc4d1	omlazovací
prostředek	prostředek	k1gInSc4	prostředek
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
baseballového	baseballový	k2eAgInSc2d1	baseballový
týmu	tým	k1gInSc2	tým
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Yankees	Yankees	k1gMnSc1	Yankees
Joem	Joem	k1gMnSc1	Joem
DiMaggio	DiMaggio	k1gMnSc1	DiMaggio
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
metry	metr	k1gInPc4	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
životem	život	k1gInSc7	život
nenápadný	nápadný	k2eNgMnSc1d1	nenápadný
rodilý	rodilý	k2eAgMnSc1d1	rodilý
Ital	Ital	k1gMnSc1	Ital
se	se	k3xPyFc4	se
Marilyn	Marilyn	k1gFnSc3	Marilyn
vytrvale	vytrvale	k6eAd1	vytrvale
dvořil	dvořit	k5eAaImAgInS	dvořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ona	onen	k3xDgFnSc1	onen
nadále	nadále	k6eAd1	nadále
natáčela	natáčet	k5eAaImAgFnS	natáčet
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
dvě	dva	k4xCgFnPc4	dva
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
role	role	k1gFnPc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
první	první	k4xOgFnSc7	první
byla	být	k5eAaImAgFnS	být
psychopatická	psychopatický	k2eAgFnSc1d1	psychopatická
vychovatelka	vychovatelka	k1gFnSc1	vychovatelka
dětí	dítě	k1gFnPc2	dítě
Nell	Nella	k1gFnPc2	Nella
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Neobtěžuj	obtěžovat	k5eNaImRp2nS	obtěžovat
se	se	k3xPyFc4	se
klepáním	klepání	k1gNnSc7	klepání
<g/>
.	.	kIx.	.
</s>
<s>
Marilyninu	Marilynin	k2eAgFnSc4d1	Marilynin
krásu	krása	k1gFnSc4	krása
nepotlačily	potlačit	k5eNaPmAgInP	potlačit
ani	ani	k8xC	ani
upjaté	upjatý	k2eAgInPc1d1	upjatý
šaty	šat	k1gInPc1	šat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyšinutost	vyšinutost	k1gFnSc1	vyšinutost
Marilyn	Marilyn	k1gFnSc1	Marilyn
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ponurém	ponurý	k2eAgInSc6d1	ponurý
filmu	film	k1gInSc6	film
náramně	náramně	k6eAd1	náramně
slušela	slušet	k5eAaImAgFnS	slušet
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
Hathaway	Hathawaa	k1gMnSc2	Hathawaa
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgMnS	mít
pro	pro	k7c4	pro
Marilyn	Marilyn	k1gFnSc4	Marilyn
ve	v	k7c6	v
stole	stol	k1gInSc6	stol
scénář	scénář	k1gInSc1	scénář
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
rolí	role	k1gFnSc7	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Niagara	Niagara	k1gFnSc1	Niagara
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
již	jenž	k3xRgFnSc4	jenž
Marilyn	Marilyn	k1gFnSc4	Marilyn
ukázal	ukázat	k5eAaPmAgMnS	ukázat
jako	jako	k8xS	jako
hvězdu	hvězda	k1gFnSc4	hvězda
velkého	velký	k2eAgInSc2d1	velký
formátu	formát	k1gInSc2	formát
a	a	k8xC	a
v	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gFnSc4	její
jméno	jméno	k1gNnSc1	jméno
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
role	role	k1gFnSc1	role
cynické	cynický	k2eAgFnSc2d1	cynická
a	a	k8xC	a
záletné	záletný	k2eAgFnSc2d1	záletná
mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
Rose	Rosa	k1gFnSc3	Rosa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podvádí	podvádět	k5eAaImIp3nS	podvádět
svého	svůj	k3xOyFgMnSc4	svůj
impotentního	impotentní	k2eAgMnSc4d1	impotentní
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
provokuje	provokovat	k5eAaImIp3nS	provokovat
ho	on	k3xPp3gMnSc4	on
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
potká	potkat	k5eAaPmIp3nS	potkat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
manžel	manžel	k1gMnSc1	manžel
v	v	k7c6	v
záchvatu	záchvat	k1gInSc6	záchvat
zoufalství	zoufalství	k1gNnSc2	zoufalství
a	a	k8xC	a
hněvu	hněv	k1gInSc2	hněv
uškrtí	uškrtit	k5eAaPmIp3nS	uškrtit
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
to	ten	k3xDgNnSc4	ten
té	ten	k3xDgFnSc6	ten
potvoře	potvora	k1gFnSc6	potvora
přejí	přát	k5eAaImIp3nP	přát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
litují	litovat	k5eAaImIp3nP	litovat
jejího	její	k3xOp3gInSc2	její
mladého	mladý	k2eAgInSc2d1	mladý
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
záběrem	záběr	k1gInSc7	záběr
ženské	ženský	k2eAgFnSc2d1	ženská
chůze	chůze	k1gFnSc2	chůze
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
kinematografii	kinematografie	k1gFnSc6	kinematografie
a	a	k8xC	a
Marilyn	Marilyn	k1gFnSc6	Marilyn
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
natočení	natočení	k1gNnSc4	natočení
scény	scéna	k1gFnSc2	scéna
údajně	údajně	k6eAd1	údajně
uřízla	uříznout	k5eAaPmAgFnS	uříznout
podpatek	podpatek	k1gInSc4	podpatek
u	u	k7c2	u
pravé	pravý	k2eAgFnSc2d1	pravá
boty	bota	k1gFnSc2	bota
o	o	k7c4	o
1	[number]	k4	1
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tím	ten	k3xDgNnSc7	ten
pověstný	pověstný	k2eAgInSc4d1	pověstný
pohyb	pohyb	k1gInSc4	pohyb
boků	bok	k1gInPc2	bok
připomínající	připomínající	k2eAgInSc4d1	připomínající
pohyb	pohyb	k1gInSc4	pohyb
pístů	píst	k1gInPc2	píst
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
napsal	napsat	k5eAaBmAgMnS	napsat
jistý	jistý	k2eAgMnSc1d1	jistý
reportér	reportér	k1gMnSc1	reportér
po	po	k7c6	po
zhlédnutí	zhlédnutí	k1gNnSc6	zhlédnutí
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
navíc	navíc	k6eAd1	navíc
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
měla	mít	k5eAaImAgFnS	mít
Marilyn	Marilyn	k1gFnSc4	Marilyn
vyloženě	vyloženě	k6eAd1	vyloženě
negativní	negativní	k2eAgFnSc4d1	negativní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc4	její
Rose	Ros	k1gMnPc4	Ros
byla	být	k5eAaImAgFnS	být
sebevědomá	sebevědomý	k2eAgFnSc1d1	sebevědomá
<g/>
,	,	kIx,	,
cynická	cynický	k2eAgFnSc1d1	cynická
<g/>
,	,	kIx,	,
nespoutaná	spoutaný	k2eNgFnSc1d1	nespoutaná
<g/>
,	,	kIx,	,
nevyzpytatelná	vyzpytatelný	k2eNgFnSc1d1	nevyzpytatelná
a	a	k8xC	a
zákeřná	zákeřný	k2eAgFnSc1d1	zákeřná
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
slávy	sláva	k1gFnSc2	sláva
==	==	k?	==
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
filmy	film	k1gInPc1	film
jako	jako	k8xC	jako
Páni	pan	k1gMnPc1	pan
mají	mít	k5eAaImIp3nP	mít
radši	rád	k6eAd2	rád
blondýnky	blondýnka	k1gFnSc2	blondýnka
a	a	k8xC	a
Jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
vzít	vzít	k5eAaPmF	vzít
milionáře	milionář	k1gMnSc4	milionář
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nesly	nést	k5eAaImAgFnP	nést
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
duchu	duch	k1gMnSc6	duch
dalších	další	k2eAgFnPc2d1	další
Marilyniných	Marilynin	k2eAgFnPc2d1	Marilynin
rolí	role	k1gFnPc2	role
atraktivní	atraktivní	k2eAgFnSc2d1	atraktivní
přihlouplé	přihlouplý	k2eAgFnSc2d1	přihlouplá
blondýnky	blondýnka	k1gFnSc2	blondýnka
<g/>
,	,	kIx,	,
tolik	tolik	k6eAd1	tolik
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
od	od	k7c2	od
chladnokrevné	chladnokrevný	k2eAgFnSc2d1	chladnokrevná
Rose	Rosa	k1gFnSc3	Rosa
z	z	k7c2	z
Niagary	Niagara	k1gFnSc2	Niagara
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
druhého	druhý	k4xOgInSc2	druhý
jmenovaného	jmenovaný	k2eAgInSc2d1	jmenovaný
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
pompézní	pompézní	k2eAgFnSc7d1	pompézní
korunovací	korunovace	k1gFnSc7	korunovace
Marilyn	Marilyn	k1gFnSc2	Marilyn
na	na	k7c4	na
královnu	královna	k1gFnSc4	královna
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Páni	pan	k1gMnPc1	pan
mají	mít	k5eAaImIp3nP	mít
radši	rád	k6eAd2	rád
blondýnky	blondýnka	k1gFnSc2	blondýnka
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
roli	role	k1gFnSc4	role
naivní	naivní	k2eAgFnSc4d1	naivní
a	a	k8xC	a
zdánlivě	zdánlivě	k6eAd1	zdánlivě
hloupé	hloupý	k2eAgFnSc2d1	hloupá
blondýnky	blondýnka	k1gFnSc2	blondýnka
Lorelei	Lorele	k1gFnSc2	Lorele
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hollywoodská	hollywoodský	k2eAgFnSc1d1	hollywoodská
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
bujným	bujný	k2eAgNnSc7d1	bujné
poprsím	poprsí	k1gNnSc7	poprsí
Jane	Jan	k1gMnSc5	Jan
Russell	Russella	k1gFnPc2	Russella
hrála	hrát	k5eAaImAgFnS	hrát
brunetku	brunetka	k1gFnSc4	brunetka
Dorothy	Dorotha	k1gFnSc2	Dorotha
Shaw	Shaw	k1gFnSc2	Shaw
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
vydělaly	vydělat	k5eAaPmAgInP	vydělat
studiu	studio	k1gNnSc6	studio
pěkné	pěkný	k2eAgInPc4d1	pěkný
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejnavštěvovanějším	navštěvovaný	k2eAgInSc7d3	nejnavštěvovanější
muzikálem	muzikál	k1gInSc7	muzikál
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
z	z	k7c2	z
Marilyn	Marilyn	k1gFnSc2	Marilyn
a	a	k8xC	a
Jane	Jan	k1gMnSc5	Jan
staly	stát	k5eAaPmAgFnP	stát
přítelkyně	přítelkyně	k1gFnPc1	přítelkyně
–	–	k?	–
Marilyn	Marilyn	k1gFnSc1	Marilyn
vyzvídala	vyzvídat	k5eAaImAgFnS	vyzvídat
na	na	k7c4	na
Jane	Jan	k1gMnSc5	Jan
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc1	jaký
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
mít	mít	k5eAaImF	mít
za	za	k7c4	za
manžela	manžel	k1gMnSc4	manžel
sportovce	sportovec	k1gMnSc4	sportovec
(	(	kIx(	(
<g/>
Jane	Jan	k1gMnSc5	Jan
Russell	Russell	k1gMnSc1	Russell
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
za	za	k7c4	za
manžela	manžel	k1gMnSc4	manžel
Boba	Bob	k1gMnSc4	Bob
Waterfielda	Waterfield	k1gMnSc4	Waterfield
<g/>
,	,	kIx,	,
profesionálního	profesionální	k2eAgMnSc4d1	profesionální
hráče	hráč	k1gMnSc4	hráč
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
znaly	znát	k5eAaImAgFnP	znát
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
filmu	film	k1gInSc2	film
jim	on	k3xPp3gMnPc3	on
nakonec	nakonec	k6eAd1	nakonec
vynesl	vynést	k5eAaPmAgInS	vynést
zanechání	zanechání	k1gNnSc4	zanechání
otisků	otisk	k1gInPc2	otisk
dlaní	dlaň	k1gFnPc2	dlaň
a	a	k8xC	a
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
podpisů	podpis	k1gInPc2	podpis
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
před	před	k7c7	před
Graumanovým	Graumanův	k2eAgNnSc7d1	Graumanův
Čínským	čínský	k2eAgNnSc7d1	čínské
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
jmenovaný	jmenovaný	k2eAgInSc1d1	jmenovaný
film	film	k1gInSc1	film
pojednával	pojednávat	k5eAaImAgInS	pojednávat
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
mladých	mladý	k2eAgFnPc6d1	mladá
modelkách	modelka	k1gFnPc6	modelka
Schatze	Schatze	k1gFnSc2	Schatze
<g/>
,	,	kIx,	,
Loco	Loco	k1gNnSc1	Loco
a	a	k8xC	a
Pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
peníze	peníz	k1gInPc4	peníz
najmou	najmout	k5eAaPmIp3nP	najmout
drahé	drahý	k2eAgNnSc4d1	drahé
apartmá	apartmá	k1gNnSc4	apartmá
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
uspořádají	uspořádat	k5eAaPmIp3nP	uspořádat
hon	hon	k1gInSc4	hon
na	na	k7c4	na
milionáře	milionář	k1gMnPc4	milionář
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc3	jeden
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
skutečně	skutečně	k6eAd1	skutečně
podaří	podařit	k5eAaPmIp3nS	podařit
milionáře	milionář	k1gMnSc4	milionář
ulovit	ulovit	k5eAaPmF	ulovit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
najdou	najít	k5eAaPmIp3nP	najít
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Marilyn	Marilyn	k1gFnSc1	Marilyn
zde	zde	k6eAd1	zde
sekundovala	sekundovat	k5eAaImAgFnS	sekundovat
jiným	jiný	k2eAgFnPc3d1	jiná
ženským	ženský	k2eAgFnPc3d1	ženská
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
:	:	kIx,	:
ve	v	k7c6	v
filmu	film	k1gInSc6	film
ironické	ironický	k2eAgFnSc2d1	ironická
a	a	k8xC	a
skeptické	skeptický	k2eAgFnSc2d1	skeptická
Lauren	Laurno	k1gNnPc2	Laurno
Bacall	Bacall	k1gInSc4	Bacall
a	a	k8xC	a
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
blonďaté	blonďatý	k2eAgFnSc6d1	blonďatá
sexbombě	sexbomba	k1gFnSc6	sexbomba
Betty	Betty	k1gFnSc2	Betty
Grable	Grable	k1gFnSc2	Grable
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
filmem	film	k1gInSc7	film
de	de	k?	de
facto	facto	k1gNnSc1	facto
loučila	loučit	k5eAaImAgFnS	loučit
s	s	k7c7	s
hollywoodským	hollywoodský	k2eAgInSc7d1	hollywoodský
trůnem	trůn	k1gInSc7	trůn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gInSc4	on
postoupila	postoupit	k5eAaPmAgFnS	postoupit
právě	právě	k6eAd1	právě
Marilyn	Marilyn	k1gFnSc1	Marilyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studio	studio	k1gNnSc1	studio
pak	pak	k6eAd1	pak
nečekalo	čekat	k5eNaImAgNnS	čekat
a	a	k8xC	a
chtělo	chtít	k5eAaImAgNnS	chtít
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
úspěchy	úspěch	k1gInPc4	úspěch
dalším	další	k2eAgInSc7d1	další
hitem	hit	k1gInSc7	hit
–	–	k?	–
měl	mít	k5eAaImAgMnS	mít
jím	on	k3xPp3gInSc7	on
být	být	k5eAaImF	být
westernově	westernově	k6eAd1	westernově
laděný	laděný	k2eAgInSc1d1	laděný
film	film	k1gInSc1	film
Řeka	Řek	k1gMnSc2	Řek
bez	bez	k7c2	bez
návratu	návrat	k1gInSc2	návrat
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Otto	Otto	k1gMnSc1	Otto
Premingera	Premingera	k1gFnSc1	Premingera
<g/>
.	.	kIx.	.
</s>
<s>
Marilyn	Marilyn	k1gFnSc1	Marilyn
tímto	tento	k3xDgInSc7	tento
filmem	film	k1gInSc7	film
nebyla	být	k5eNaImAgFnS	být
nadšená	nadšený	k2eAgFnSc1d1	nadšená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musela	muset	k5eAaImAgFnS	muset
splnit	splnit	k5eAaPmF	splnit
závazky	závazek	k1gInPc4	závazek
vůči	vůči	k7c3	vůči
Foxu	fox	k1gInSc3	fox
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
další	další	k2eAgInSc1d1	další
film	film	k1gInSc1	film
Není	být	k5eNaImIp3nS	být
nad	nad	k7c4	nad
showbusiness	showbusiness	k1gInSc4	showbusiness
neuspokojoval	uspokojovat	k5eNaImAgInS	uspokojovat
představy	představ	k1gInPc7	představ
Marilyn	Marilyn	k1gFnSc4	Marilyn
o	o	k7c6	o
jejích	její	k3xOp3gInPc6	její
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
studio	studio	k1gNnSc1	studio
jí	on	k3xPp3gFnSc3	on
přislíbilo	přislíbit	k5eAaPmAgNnS	přislíbit
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Slaměný	slaměný	k2eAgMnSc1d1	slaměný
vdovec	vdovec	k1gMnSc1	vdovec
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
předností	přednost	k1gFnSc7	přednost
filmu	film	k1gInSc2	film
Není	být	k5eNaImIp3nS	být
nad	nad	k7c7	nad
showbusiness	showbusiness	k6eAd1	showbusiness
snad	snad	k9	snad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
Marilyniny	Marilynin	k2eAgFnSc2d1	Marilynin
interpretace	interpretace	k1gFnSc2	interpretace
písní	píseň	k1gFnPc2	píseň
Irvinga	Irvinga	k1gFnSc1	Irvinga
Berlina	berlina	k1gFnSc1	berlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
s	s	k7c7	s
Joem	Joe	k1gNnSc7	Joe
DiMaggiem	DiMaggius	k1gMnSc7	DiMaggius
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1954	[number]	k4	1954
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
a	a	k8xC	a
svatební	svatební	k2eAgFnSc1d1	svatební
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
předznamenala	předznamenat	k5eAaPmAgFnS	předznamenat
konec	konec	k1gInSc4	konec
manželství	manželství	k1gNnPc2	manželství
téměř	téměř	k6eAd1	téměř
ještě	ještě	k9	ještě
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
začalo	začít	k5eAaPmAgNnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Joea	Joea	k6eAd1	Joea
pobuřovalo	pobuřovat	k5eAaImAgNnS	pobuřovat
její	její	k3xOp3gNnSc4	její
dráždivé	dráždivý	k2eAgNnSc4d1	dráždivé
vystupování	vystupování	k1gNnSc4	vystupování
a	a	k8xC	a
styl	styl	k1gInSc4	styl
oblékání	oblékání	k1gNnSc2	oblékání
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
mít	mít	k5eAaImF	mít
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ona	onen	k3xDgFnSc1	onen
toužila	toužit	k5eAaImAgFnS	toužit
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
hřebíčkem	hřebíček	k1gInSc7	hřebíček
do	do	k7c2	do
rakve	rakev	k1gFnSc2	rakev
jejich	jejich	k3xOp3gNnSc2	jejich
manželství	manželství	k1gNnSc2	manželství
bylo	být	k5eAaImAgNnS	být
natáčení	natáčení	k1gNnSc1	natáčení
Slaměného	slaměný	k2eAgMnSc2d1	slaměný
vdovce	vdovec	k1gMnSc2	vdovec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proud	proud	k1gInSc1	proud
vzduchu	vzduch	k1gInSc2	vzduch
z	z	k7c2	z
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
Marilyn	Marilyn	k1gFnSc1	Marilyn
zvedne	zvednout	k5eAaPmIp3nS	zvednout
sukni	sukně	k1gFnSc4	sukně
až	až	k9	až
nad	nad	k7c4	nad
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
natáčení	natáčení	k1gNnSc6	natáčení
po	po	k7c6	po
necelých	celý	k2eNgInPc6d1	necelý
9	[number]	k4	9
měsících	měsíc	k1gInPc6	měsíc
rozvedou	rozvést	k5eAaPmIp3nP	rozvést
<g/>
,	,	kIx,	,
na	na	k7c6	na
premiéře	premiéra	k1gFnSc6	premiéra
filmu	film	k1gInSc2	film
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c4	v
den	den	k1gInSc4	den
jejích	její	k3xOp3gMnPc2	její
29	[number]	k4	29
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
ještě	ještě	k9	ještě
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Útěk	útěk	k1gInSc1	útěk
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
==	==	k?	==
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
kdysi	kdysi	k6eAd1	kdysi
dlouho	dlouho	k6eAd1	dlouho
nevydržela	vydržet	k5eNaPmAgFnS	vydržet
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
padáky	padák	k1gInPc4	padák
<g/>
,	,	kIx,	,
nevydržela	vydržet	k5eNaPmAgFnS	vydržet
ani	ani	k8xC	ani
nápor	nápor	k1gInSc4	nápor
image	image	k1gFnSc2	image
hloupé	hloupý	k2eAgFnSc2d1	hloupá
blondýnky	blondýnka	k1gFnSc2	blondýnka
<g/>
.	.	kIx.	.
</s>
<s>
Vadilo	vadit	k5eAaImAgNnS	vadit
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
jako	jako	k9	jako
umělkyni	umělkyně	k1gFnSc3	umělkyně
nerespektuje	respektovat	k5eNaImIp3nS	respektovat
a	a	k8xC	a
trápilo	trápit	k5eAaImAgNnS	trápit
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
herečku	herečka	k1gFnSc4	herečka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
za	za	k7c4	za
atraktivní	atraktivní	k2eAgInSc4d1	atraktivní
doplněk	doplněk	k1gInSc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Rozčiloval	rozčilovat	k5eAaImAgMnS	rozčilovat
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vázaná	vázaný	k2eAgFnSc1d1	vázaná
vůči	vůči	k7c3	vůči
Foxu	fox	k1gInSc3	fox
krajně	krajně	k6eAd1	krajně
nevýhodnou	výhodný	k2eNgFnSc7d1	nevýhodná
sedmiletou	sedmiletý	k2eAgFnSc7d1	sedmiletá
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
které	který	k3yQgFnSc2	který
vydělávala	vydělávat	k5eAaImAgFnS	vydělávat
téměř	téměř	k6eAd1	téměř
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
komparsisté	komparsista	k1gMnPc1	komparsista
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
její	její	k3xOp3gMnPc1	její
právní	právní	k2eAgMnPc1d1	právní
agenti	agent	k1gMnPc1	agent
nikdy	nikdy	k6eAd1	nikdy
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
tuto	tento	k3xDgFnSc4	tento
smlouvu	smlouva	k1gFnSc4	smlouva
napadnout	napadnout	k5eAaPmF	napadnout
a	a	k8xC	a
Marilyn	Marilyn	k1gFnSc4	Marilyn
zvýhodnit	zvýhodnit	k5eAaPmF	zvýhodnit
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
životopisců	životopisec	k1gMnPc2	životopisec
MM	mm	kA	mm
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
rozklíčovat	rozklíčovat	k5eAaImF	rozklíčovat
<g/>
,	,	kIx,	,
samotnou	samotný	k2eAgFnSc4d1	samotná
smlouvu	smlouva	k1gFnSc4	smlouva
napadl	napadnout	k5eAaPmAgInS	napadnout
až	až	k9	až
Milton	Milton	k1gInSc1	Milton
Greene	Green	k1gMnSc5	Green
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
MM	mm	kA	mm
navázala	navázat	k5eAaPmAgFnS	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
unavená	unavený	k2eAgFnSc1d1	unavená
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
nabízeli	nabízet	k5eAaImAgMnP	nabízet
–	–	k?	–
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
šla	jít	k5eAaImAgFnS	jít
z	z	k7c2	z
filmu	film	k1gInSc2	film
do	do	k7c2	do
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Doslova	doslova	k6eAd1	doslova
tedy	tedy	k9	tedy
uprostřed	uprostřed	k7c2	uprostřed
rozepří	rozepře	k1gFnPc2	rozepře
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
Fox	fox	k1gInSc1	fox
utekla	utéct	k5eAaPmAgFnS	utéct
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
studovat	studovat	k5eAaImF	studovat
ve	v	k7c6	v
věhlasném	věhlasný	k2eAgInSc6d1	věhlasný
Actors	Actors	k1gInSc1	Actors
<g/>
'	'	kIx"	'
Studiu	studio	k1gNnSc3	studio
Lee	Lea	k1gFnSc3	Lea
Strasberga	Strasberga	k1gFnSc1	Strasberga
<g/>
.	.	kIx.	.
</s>
<s>
Ponořila	ponořit	k5eAaPmAgFnS	ponořit
se	se	k3xPyFc4	se
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
dramatu	drama	k1gNnSc2	drama
a	a	k8xC	a
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
,	,	kIx,	,
seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
newyorskou	newyorský	k2eAgFnSc7d1	newyorská
intelektuální	intelektuální	k2eAgFnSc7d1	intelektuální
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
v	v	k7c4	v
Actors	Actors	k1gInSc4	Actors
<g/>
'	'	kIx"	'
Studiu	studio	k1gNnSc6	studio
dostala	dostat	k5eAaPmAgFnS	dostat
mnoho	mnoho	k6eAd1	mnoho
příležitostí	příležitost	k1gFnSc7	příležitost
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
si	se	k3xPyFc3	se
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
rodiny	rodina	k1gFnSc2	rodina
Strasbergových	Strasbergův	k2eAgInPc2d1	Strasbergův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
popisuje	popisovat	k5eAaImIp3nS	popisovat
Susan	Susan	k1gMnSc1	Susan
Strasberg	Strasberg	k1gMnSc1	Strasberg
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
Pauly	Paula	k1gFnPc1	Paula
Strasbergových	Strasbergův	k2eAgFnPc2d1	Strasbergova
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Marilyn	Marilyn	k1gFnSc1	Marilyn
a	a	k8xC	a
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
potkala	potkat	k5eAaPmAgFnS	potkat
také	také	k9	také
fotografa	fotograf	k1gMnSc4	fotograf
Miltona	Milton	k1gMnSc4	Milton
Greena	Green	k1gMnSc4	Green
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
založila	založit	k5eAaPmAgFnS	založit
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc1d1	vlastní
společnost	společnost	k1gFnSc1	společnost
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
Production	Production	k1gInSc4	Production
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
průkopnicí	průkopnice	k1gFnSc7	průkopnice
a	a	k8xC	a
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
produkci	produkce	k1gFnSc6	produkce
pak	pak	k6eAd1	pak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
film	film	k1gInSc1	film
Princ	princa	k1gFnPc2	princa
a	a	k8xC	a
tanečnice	tanečnice	k1gFnSc2	tanečnice
<g/>
.	.	kIx.	.
</s>
<s>
Snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
hollywoodských	hollywoodský	k2eAgFnPc6d1	hollywoodská
studiích	studie	k1gFnPc6	studie
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jí	on	k3xPp3gFnSc3	on
vždy	vždy	k6eAd1	vždy
nabízela	nabízet	k5eAaImAgFnS	nabízet
podřadné	podřadný	k2eAgFnPc4d1	podřadná
role	role	k1gFnPc4	role
ve	v	k7c6	v
slabých	slabý	k2eAgInPc6d1	slabý
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
<g/>
Prvním	první	k4xOgInSc7	první
příslibem	příslib	k1gInSc7	příslib
lepší	dobrý	k2eAgFnSc2d2	lepší
tvorby	tvorba	k1gFnSc2	tvorba
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
film	film	k1gInSc1	film
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
zastávka	zastávka	k1gFnSc1	zastávka
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Joshua	Joshuus	k1gMnSc2	Joshuus
Logana	Logan	k1gMnSc2	Logan
a	a	k8xC	a
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
Princ	princ	k1gMnSc1	princ
a	a	k8xC	a
tanečnice	tanečnice	k1gFnPc1	tanečnice
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
režie	režie	k1gFnPc4	režie
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
Laurence	Laurence	k1gFnSc2	Laurence
Olivier	Olivier	k1gInSc1	Olivier
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
Marilyn	Marilyn	k1gFnSc1	Marilyn
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c2	za
Arthura	Arthur	k1gMnSc2	Arthur
Millera	Miller	k1gMnSc2	Miller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studio	studio	k1gNnSc1	studio
Fox	fox	k1gInSc1	fox
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Marilyn	Marilyn	k1gFnSc7	Marilyn
snažilo	snažit	k5eAaImAgNnS	snažit
nahradit	nahradit	k5eAaPmF	nahradit
záplavou	záplava	k1gFnSc7	záplava
platinově	platinově	k6eAd1	platinově
blond	blond	k2eAgFnPc2d1	blond
hereček	herečka	k1gFnPc2	herečka
s	s	k7c7	s
bujnými	bujný	k2eAgInPc7d1	bujný
tvary	tvar	k1gInPc7	tvar
(	(	kIx(	(
<g/>
Jayne	Jayn	k1gInSc5	Jayn
Mansfield	Mansfield	k1gInSc1	Mansfield
<g/>
,	,	kIx,	,
Sheree	Sheree	k1gInSc1	Sheree
North	North	k1gInSc1	North
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
Lang	Lang	k1gMnSc1	Lang
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
ve	v	k7c6	v
filmech	film	k1gInPc6	film
nosily	nosit	k5eAaImAgFnP	nosit
dokonce	dokonce	k9	dokonce
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
ateliérech	ateliér	k1gInPc6	ateliér
studia	studio	k1gNnSc2	studio
po	po	k7c6	po
Marilyn	Marilyn	k1gFnSc6	Marilyn
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
ovšem	ovšem	k9	ovšem
nikdy	nikdy	k6eAd1	nikdy
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
takové	takový	k3xDgFnPc4	takový
popularity	popularita	k1gFnPc4	popularita
a	a	k8xC	a
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
s	s	k7c7	s
dramatikem	dramatik	k1gMnSc7	dramatik
==	==	k?	==
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
Miller	Miller	k1gMnSc1	Miller
opustil	opustit	k5eAaPmAgMnS	opustit
manželku	manželka	k1gFnSc4	manželka
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Monroe	Monroe	k1gFnSc1	Monroe
konvertovala	konvertovat	k5eAaBmAgFnS	konvertovat
k	k	k7c3	k
judaismu	judaismus	k1gInSc3	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Věřila	věřit	k5eAaImAgFnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
vážným	vážný	k2eAgMnSc7d1	vážný
a	a	k8xC	a
váženým	vážený	k2eAgMnSc7d1	vážený
dramatikem	dramatik	k1gMnSc7	dramatik
si	se	k3xPyFc3	se
upevní	upevnit	k5eAaPmIp3nP	upevnit
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
filmovém	filmový	k2eAgNnSc6d1	filmové
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
Arthura	Arthura	k1gFnSc1	Arthura
milovala	milovat	k5eAaImAgFnS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Domnívala	domnívat	k5eAaImAgFnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ona	onen	k3xDgFnSc1	onen
jeho	jeho	k3xOp3gFnSc1	jeho
<g/>
,	,	kIx,	,
podpoří	podpořit	k5eAaPmIp3nS	podpořit
i	i	k9	i
on	on	k3xPp3gInSc1	on
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
touze	touha	k1gFnSc6	touha
stát	stát	k5eAaPmF	stát
se	s	k7c7	s
dramatickou	dramatický	k2eAgFnSc7d1	dramatická
umělkyní	umělkyně	k1gFnSc7	umělkyně
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Arthura	Arthura	k1gFnSc1	Arthura
vyslýchal	vyslýchat	k5eAaImAgInS	vyslýchat
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
neamerickou	americký	k2eNgFnSc4d1	neamerická
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
stála	stát	k5eAaImAgFnS	stát
mu	on	k3xPp3gMnSc3	on
po	po	k7c4	po
boku	boka	k1gFnSc4	boka
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
jej	on	k3xPp3gMnSc4	on
podpořila	podpořit	k5eAaPmAgFnS	podpořit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
riskovala	riskovat	k5eAaBmAgFnS	riskovat
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
ale	ale	k9	ale
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
projevech	projev	k1gInPc6	projev
tak	tak	k9	tak
extrovertní	extrovertní	k2eAgNnSc4d1	extrovertní
nebyl	být	k5eNaImAgMnS	být
a	a	k8xC	a
Marilyn	Marilyn	k1gFnSc1	Marilyn
jeho	jeho	k3xOp3gFnSc4	jeho
uzavřenost	uzavřenost	k1gFnSc4	uzavřenost
těžce	těžce	k6eAd1	těžce
nesla	nést	k5eAaImAgFnS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prodělaném	prodělaný	k2eAgInSc6d1	prodělaný
potratu	potrat	k1gInSc6	potrat
(	(	kIx(	(
<g/>
srpen	srpen	k1gInSc1	srpen
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
a	a	k8xC	a
nenaplněných	naplněný	k2eNgFnPc6d1	nenaplněná
představách	představa	k1gFnPc6	představa
o	o	k7c4	o
hraní	hraní	k1gNnSc4	hraní
se	se	k3xPyFc4	se
utápěla	utápět	k5eAaImAgFnS	utápět
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
v	v	k7c6	v
depresích	deprese	k1gFnPc6	deprese
<g/>
,	,	kIx,	,
mlčenlivost	mlčenlivost	k1gFnSc1	mlčenlivost
Arthura	Arthura	k1gFnSc1	Arthura
tak	tak	k6eAd1	tak
její	její	k3xOp3gInPc4	její
pocity	pocit	k1gInPc4	pocit
osamělosti	osamělost	k1gFnSc2	osamělost
jen	jen	k9	jen
umocňovala	umocňovat	k5eAaImAgFnS	umocňovat
<g/>
.	.	kIx.	.
</s>
<s>
Nesourodý	sourodý	k2eNgInSc1d1	nesourodý
pár	pár	k1gInSc1	pár
tak	tak	k6eAd1	tak
prošel	projít	k5eAaPmAgInS	projít
první	první	k4xOgFnSc7	první
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Monroe	Monroe	k1gFnSc1	Monroe
si	se	k3xPyFc3	se
dala	dát	k5eAaPmAgFnS	dát
pauzu	pauza	k1gFnSc4	pauza
v	v	k7c6	v
natáčení	natáčení	k1gNnSc6	natáčení
a	a	k8xC	a
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
farmu	farma	k1gFnSc4	farma
v	v	k7c6	v
Connecticutu	Connecticut	k1gInSc6	Connecticut
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Arthur	Arthur	k1gMnSc1	Arthur
měl	mít	k5eAaImAgMnS	mít
kde	kde	k6eAd1	kde
tvořit	tvořit	k5eAaImF	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
za	za	k7c4	za
celé	celý	k2eAgNnSc4d1	celé
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
manželství	manželství	k1gNnSc4	manželství
s	s	k7c7	s
MM	mm	kA	mm
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pouze	pouze	k6eAd1	pouze
scénář	scénář	k1gInSc1	scénář
k	k	k7c3	k
filmu	film	k1gInSc6	film
Mustangové	mustang	k1gMnPc1	mustang
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
příjmy	příjem	k1gInPc1	příjem
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ztenčily	ztenčit	k5eAaPmAgFnP	ztenčit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přišla	přijít	k5eAaPmAgFnS	přijít
nabídka	nabídka	k1gFnSc1	nabídka
na	na	k7c4	na
natočení	natočení	k1gNnSc4	natočení
filmu	film	k1gInSc2	film
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k6eAd1	rád
horké	horký	k2eAgNnSc1d1	horké
<g/>
,	,	kIx,	,
krize	krize	k1gFnSc1	krize
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
se	se	k3xPyFc4	se
jen	jen	k9	jen
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
<g/>
:	:	kIx,	:
Arthur	Arthur	k1gMnSc1	Arthur
Marilyn	Marilyn	k1gFnSc4	Marilyn
přemlouval	přemlouvat	k5eAaImAgMnS	přemlouvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
roli	role	k1gFnSc4	role
Sugar	Sugar	k1gInSc1	Sugar
přijala	přijmout	k5eAaPmAgFnS	přijmout
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
však	však	k9	však
zuřila	zuřit	k5eAaImAgFnS	zuřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnSc4d1	další
roli	role	k1gFnSc4	role
hloupé	hloupý	k2eAgFnSc2d1	hloupá
ženské	ženská	k1gFnSc2	ženská
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nerozezná	rozeznat	k5eNaPmIp3nS	rozeznat
mužské	mužský	k2eAgNnSc1d1	mužské
od	od	k7c2	od
děvčat	děvče	k1gNnPc2	děvče
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
hrát	hrát	k5eAaImF	hrát
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
opět	opět	k6eAd1	opět
otěhotnět	otěhotnět	k5eAaPmF	otěhotnět
<g/>
,	,	kIx,	,
roli	role	k1gFnSc4	role
nakonec	nakonec	k6eAd1	nakonec
přijala	přijmout	k5eAaPmAgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k6eAd1	rád
horké	horký	k2eAgNnSc1d1	horké
===	===	k?	===
</s>
</p>
<p>
<s>
Natáčet	natáčet	k5eAaImF	natáčet
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgMnSc4	sám
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
kondici	kondice	k1gFnSc6	kondice
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
uklidňujících	uklidňující	k2eAgInPc6d1	uklidňující
prostředcích	prostředek	k1gInPc6	prostředek
sice	sice	k8xC	sice
byla	být	k5eAaImAgFnS	být
–	–	k?	–
i	i	k8xC	i
přes	přes	k7c4	přes
těhotenství	těhotenství	k1gNnSc4	těhotenství
–	–	k?	–
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
<g/>
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
zachránila	zachránit	k5eAaPmAgFnS	zachránit
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
léky	lék	k1gInPc7	lék
na	na	k7c4	na
spaní	spaní	k1gNnSc4	spaní
omezit	omezit	k5eAaPmF	omezit
(	(	kIx(	(
<g/>
důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
usínala	usínat	k5eAaImAgFnS	usínat
až	až	k9	až
kolem	kolem	k7c2	kolem
druhé	druhý	k4xOgFnSc2	druhý
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k2eAgFnSc2d1	ranní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zvracela	zvracet	k5eAaImAgFnS	zvracet
a	a	k8xC	a
zavírala	zavírat	k5eAaImAgFnS	zavírat
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
šatně	šatna	k1gFnSc6	šatna
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přibrala	přibrat	k5eAaPmAgFnS	přibrat
<g/>
,	,	kIx,	,
cítila	cítit	k5eAaImAgFnS	cítit
se	se	k3xPyFc4	se
v	v	k7c6	v
šatech	šat	k1gInPc6	šat
navržených	navržený	k2eAgFnPc2d1	navržená
Ory	Ory	k1gFnPc2	Ory
Kellym	Kellym	k1gInSc1	Kellym
směšně	směšně	k6eAd1	směšně
(	(	kIx(	(
<g/>
sama	sám	k3xTgFnSc1	sám
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jak	jak	k6eAd1	jak
srandovní	srandovní	k2eAgFnSc1d1	srandovní
tlustá	tlustý	k2eAgFnSc1d1	tlustá
svině	svině	k1gFnSc1	svině
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cítila	cítit	k5eAaImAgFnS	cítit
se	se	k3xPyFc4	se
podvedená	podvedený	k2eAgFnSc1d1	podvedená
Arthurem	Arthur	k1gInSc7	Arthur
a	a	k8xC	a
celým	celý	k2eAgInSc7d1	celý
štábem	štáb	k1gInSc7	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
Lemmon	Lemmon	k1gMnSc1	Lemmon
a	a	k8xC	a
Tony	Tony	k1gMnSc1	Tony
Curtis	Curtis	k1gFnPc4	Curtis
tak	tak	k9	tak
čekali	čekat	k5eAaImAgMnP	čekat
v	v	k7c6	v
podpatcích	podpatek	k1gInPc6	podpatek
a	a	k8xC	a
ženských	ženský	k2eAgInPc6d1	ženský
šatech	šat	k1gInPc6	šat
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
Marilyn	Marilyn	k1gFnSc1	Marilyn
dostavila	dostavit	k5eAaPmAgFnS	dostavit
někdy	někdy	k6eAd1	někdy
i	i	k9	i
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
na	na	k7c4	na
plac	plac	k1gInSc4	plac
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
později	pozdě	k6eAd2	pozdě
nevzpomínal	vzpomínat	k5eNaImAgMnS	vzpomínat
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
a	a	k8xC	a
Marilyn	Marilyn	k1gFnSc7	Marilyn
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
a	a	k8xC	a
Arthurovo	Arthurův	k2eAgNnSc4d1	Arthurovo
dítě	dítě	k1gNnSc4	dítě
přišla	přijít	k5eAaPmAgFnS	přijít
(	(	kIx(	(
<g/>
paradoxem	paradox	k1gInSc7	paradox
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
potratila	potratit	k5eAaPmAgFnS	potratit
v	v	k7c4	v
den	den	k1gInSc4	den
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
předpremiéry	předpremiéra	k1gFnSc2	předpremiéra
filmu	film	k1gInSc2	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
všechno	všechen	k3xTgNnSc4	všechen
utrpení	utrpení	k1gNnSc4	utrpení
všech	všecek	k3xTgInPc2	všecek
herců	herc	k1gInPc2	herc
a	a	k8xC	a
režiséra	režisér	k1gMnSc2	režisér
Billy	Bill	k1gMnPc4	Bill
Wildera	Wildero	k1gNnSc2	Wildero
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
stal	stát	k5eAaPmAgInS	stát
absolutním	absolutní	k2eAgInSc7d1	absolutní
kasovním	kasovní	k2eAgInSc7d1	kasovní
trhákem	trhák	k1gInSc7	trhák
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
Oscarů	Oscar	k1gInPc2	Oscar
jej	on	k3xPp3gMnSc4	on
předčil	předčit	k5eAaBmAgInS	předčit
jen	jen	k9	jen
film	film	k1gInSc1	film
Ben	Ben	k1gInSc1	Ben
Hur	Hur	k1gFnSc1	Hur
a	a	k8xC	a
Marilyn	Marilyn	k1gFnSc1	Marilyn
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Americký	americký	k2eAgInSc1d1	americký
filmový	filmový	k2eAgInSc1d1	filmový
institut	institut	k1gInSc1	institut
film	film	k1gInSc1	film
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k2eAgMnSc1d1	rád
horké	horký	k2eAgNnSc1d1	horké
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
komedií	komedie	k1gFnSc7	komedie
století	století	k1gNnSc2	století
a	a	k8xC	a
role	role	k1gFnSc2	role
Sugar	Sugara	k1gFnPc2	Sugara
Kane	kanout	k5eAaImIp3nS	kanout
Kowalczyk	Kowalczyk	k1gInSc4	Kowalczyk
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
její	její	k3xOp3gFnSc1	její
nejznámější	známý	k2eAgFnSc1d3	nejznámější
rolí	role	k1gFnSc7	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mustangové	mustang	k1gMnPc1	mustang
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
filmu	film	k1gInSc6	film
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k2eAgMnSc1d1	rád
horké	horký	k2eAgNnSc1d1	horké
měla	mít	k5eAaImAgFnS	mít
pověst	pověst	k1gFnSc1	pověst
nezodpovědné	zodpovědný	k2eNgFnSc2d1	nezodpovědná
<g/>
,	,	kIx,	,
rozmazlené	rozmazlený	k2eAgFnSc2d1	rozmazlená
a	a	k8xC	a
nesnesitelné	snesitelný	k2eNgFnSc2d1	nesnesitelná
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
studio	studio	k1gNnSc4	studio
měla	mít	k5eAaImAgFnS	mít
natočit	natočit	k5eAaBmF	natočit
další	další	k2eAgInSc4d1	další
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
těžké	těžký	k2eAgNnSc1d1	těžké
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
hereckého	herecký	k2eAgMnSc2d1	herecký
partnera	partner	k1gMnSc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
film	film	k1gInSc4	film
Milujme	milovat	k5eAaImRp1nP	milovat
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
kývla	kývnout	k5eAaPmAgFnS	kývnout
francouzská	francouzský	k2eAgFnSc1d1	francouzská
hvězda	hvězda	k1gFnSc1	hvězda
Yves	Yvesa	k1gFnPc2	Yvesa
Montand	Montanda	k1gFnPc2	Montanda
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
díru	díra	k1gFnSc4	díra
do	do	k7c2	do
světa	svět	k1gInSc2	svět
neudělá	udělat	k5eNaPmIp3nS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Světlými	světlý	k2eAgInPc7d1	světlý
momenty	moment	k1gInPc7	moment
filmu	film	k1gInSc2	film
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnPc1	její
interpretace	interpretace	k1gFnPc1	interpretace
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
My	my	k3xPp1nPc1	my
heart	hearta	k1gFnPc2	hearta
belongs	belongs	k6eAd1	belongs
to	ten	k3xDgNnSc4	ten
my	my	k3xPp1nPc1	my
daddy	dadd	k1gMnPc7	dadd
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
novináře	novinář	k1gMnPc4	novinář
pikantní	pikantní	k2eAgInSc4d1	pikantní
vztah	vztah	k1gInSc4	vztah
MM	mm	kA	mm
s	s	k7c7	s
Montandem	Montand	k1gInSc7	Montand
<g/>
.	.	kIx.	.
</s>
<s>
Jedni	jeden	k4xCgMnPc1	jeden
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
Marilyn	Marilyn	k1gFnSc1	Marilyn
mstí	mstít	k5eAaImIp3nS	mstít
Arthurovi	Arthur	k1gMnSc3	Arthur
<g/>
,	,	kIx,	,
druzí	druhý	k4xOgMnPc1	druhý
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
Marilyn	Marilyn	k1gFnSc1	Marilyn
se	se	k3xPyFc4	se
poblázněně	poblázněně	k6eAd1	poblázněně
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
a	a	k8xC	a
jen	jen	k9	jen
těžce	těžce	k6eAd1	těžce
nesla	nést	k5eAaImAgFnS	nést
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Montand	Montand	k1gInSc1	Montand
po	po	k7c6	po
natáčení	natáčení	k1gNnSc6	natáčení
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
manželce	manželka	k1gFnSc3	manželka
Simone	Simon	k1gMnSc5	Simon
Signoret	Signoret	k1gMnSc1	Signoret
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc4	film
Mustangové	mustang	k1gMnPc1	mustang
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
nepříznivé	příznivý	k2eNgFnSc6d1	nepříznivá
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
výteční	výtečný	k2eAgMnPc1d1	výtečný
herci	herec	k1gMnPc1	herec
jako	jako	k8xC	jako
Clark	Clark	k1gInSc1	Clark
Gable	Gable	k1gFnSc2	Gable
<g/>
,	,	kIx,	,
Montgomery	Montgomera	k1gFnSc2	Montgomera
Clift	Clifta	k1gFnPc2	Clifta
nebo	nebo	k8xC	nebo
Eli	Eli	k1gFnPc2	Eli
Wallach	Wallacha	k1gFnPc2	Wallacha
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
pod	pod	k7c7	pod
režijní	režijní	k2eAgFnSc7d1	režijní
taktovkou	taktovka	k1gFnSc7	taktovka
Johna	John	k1gMnSc2	John
Hustona	Huston	k1gMnSc2	Huston
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Marilyn	Marilyn	k1gFnSc7	Marilyn
a	a	k8xC	a
Arthurem	Arthur	k1gMnSc7	Arthur
panovalo	panovat	k5eAaImAgNnS	panovat
napětí	napětí	k1gNnSc3	napětí
<g/>
,	,	kIx,	,
natáčelo	natáčet	k5eAaImAgNnS	natáčet
se	se	k3xPyFc4	se
ve	v	k7c6	v
žhavé	žhavý	k2eAgFnSc6d1	žhavá
Nevadské	nevadský	k2eAgFnSc6d1	Nevadská
poušti	poušť	k1gFnSc6	poušť
<g/>
,	,	kIx,	,
šedesátiletý	šedesátiletý	k2eAgInSc4d1	šedesátiletý
Clark	Clark	k1gInSc4	Clark
Gable	Gable	k1gFnSc2	Gable
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
dubléra	dubléra	k1gFnSc1	dubléra
a	a	k8xC	a
fyzicky	fyzicky	k6eAd1	fyzicky
náročné	náročný	k2eAgFnSc2d1	náročná
scény	scéna	k1gFnSc2	scéna
natáčel	natáčet	k5eAaImAgMnS	natáčet
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Marilyn	Marilyn	k1gFnSc4	Marilyn
pak	pak	k6eAd1	pak
kladli	klást	k5eAaImAgMnP	klást
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
<g/>
,	,	kIx,	,
že	že	k8xS	že
natáčení	natáčení	k1gNnSc1	natáčení
brzdí	brzdit	k5eAaImIp3nS	brzdit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
třeba	třeba	k6eAd1	třeba
také	také	k9	také
připomenout	připomenout	k5eAaPmF	připomenout
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
natáčet	natáčet	k5eAaImF	natáčet
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
scénář	scénář	k1gInSc1	scénář
plně	plně	k6eAd1	plně
dokončen	dokončit	k5eAaPmNgInS	dokončit
<g/>
,	,	kIx,	,
Marilyn	Marilyn	k1gFnPc6	Marilyn
tak	tak	k8xC	tak
často	často	k6eAd1	často
přepracované	přepracovaný	k2eAgFnSc2d1	přepracovaná
verze	verze	k1gFnSc2	verze
dostávala	dostávat	k5eAaImAgFnS	dostávat
večer	večer	k6eAd1	večer
a	a	k8xC	a
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
John	John	k1gMnSc1	John
Huston	Huston	k1gInSc4	Huston
holdoval	holdovat	k5eAaImAgMnS	holdovat
hazardním	hazardní	k2eAgFnPc3d1	hazardní
hrám	hra	k1gFnPc3	hra
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
kasínech	kasín	k1gInPc6	kasín
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
Donald	Donald	k1gMnSc1	Donald
Spoto	Spoto	k1gNnSc4	Spoto
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
Mezi	mezi	k7c7	mezi
Monroe	Monroe	k1gFnSc7	Monroe
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
idolem	idol	k1gInSc7	idol
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Gablem	Gabl	k1gInSc7	Gabl
však	však	k8xC	však
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
hezký	hezký	k2eAgInSc4d1	hezký
přátelský	přátelský	k2eAgInSc4d1	přátelský
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
Gable	Gable	k1gInSc4	Gable
trpělivě	trpělivě	k6eAd1	trpělivě
čekal	čekat	k5eAaImAgMnS	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
vyleze	vylézt	k5eAaPmIp3nS	vylézt
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
šatny	šatna	k1gFnSc2	šatna
a	a	k8xC	a
odříká	odříkat	k5eAaImIp3nS	odříkat
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
natáčení	natáčení	k1gNnSc2	natáčení
však	však	k9	však
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
opět	opět	k6eAd1	opět
hospitalizována	hospitalizovat	k5eAaBmNgFnS	hospitalizovat
na	na	k7c4	na
celkové	celkový	k2eAgNnSc4d1	celkové
vyčerpání	vyčerpání	k1gNnSc4	vyčerpání
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
10	[number]	k4	10
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
zotavovala	zotavovat	k5eAaImAgFnS	zotavovat
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Susan	Susan	k1gMnSc1	Susan
Strasberg	Strasberg	k1gMnSc1	Strasberg
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
</s>
</p>
<p>
<s>
cituje	citovat	k5eAaBmIp3nS	citovat
<g/>
:	:	kIx,	:
Stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
si	se	k3xPyFc3	se
prý	prý	k9	prý
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
neposkytli	poskytnout	k5eNaPmAgMnP	poskytnout
volno	volno	k1gNnSc4	volno
mezi	mezi	k7c7	mezi
natáčením	natáčení	k1gNnSc7	natáčení
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
tančila	tančit	k5eAaImAgFnS	tančit
a	a	k8xC	a
zpívala	zpívat	k5eAaImAgFnS	zpívat
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Milujme	milovat	k5eAaImRp1nP	milovat
se	se	k3xPyFc4	se
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
34	[number]	k4	34
letech	let	k1gInPc6	let
už	už	k6eAd1	už
podle	podle	k7c2	podle
jejích	její	k3xOp3gNnPc2	její
slov	slovo	k1gNnPc2	slovo
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
tak	tak	k9	tak
</s>
</p>
<p>
<s>
flexibilně	flexibilně	k6eAd1	flexibilně
od	od	k7c2	od
muzikální	muzikální	k2eAgFnSc2d1	muzikální
komedie	komedie	k1gFnSc2	komedie
přejít	přejít	k5eAaPmF	přejít
na	na	k7c4	na
náročné	náročný	k2eAgNnSc4d1	náročné
téma	téma	k1gNnSc4	téma
filmu	film	k1gInSc2	film
Mustangové	mustang	k1gMnPc1	mustang
<g/>
.	.	kIx.	.
</s>
<s>
Melancholické	melancholický	k2eAgNnSc1d1	melancholické
drama	drama	k1gNnSc1	drama
podle	podle	k7c2	podle
Millerova	Millerův	k2eAgInSc2d1	Millerův
scénáře	scénář	k1gInSc2	scénář
však	však	k9	však
nesplnilo	splnit	k5eNaPmAgNnS	splnit
její	její	k3xOp3gNnSc1	její
očekávání	očekávání	k1gNnSc1	očekávání
a	a	k8xC	a
film	film	k1gInSc1	film
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
propadl	propadnout	k5eAaPmAgInS	propadnout
<g/>
,	,	kIx,	,
kritika	kritika	k1gFnSc1	kritika
reagovala	reagovat	k5eAaBmAgFnS	reagovat
také	také	k9	také
velmi	velmi	k6eAd1	velmi
vlažně	vlažně	k6eAd1	vlažně
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
filmu	film	k1gInSc2	film
Clark	Clark	k1gInSc1	Clark
Gable	Gable	k1gInSc1	Gable
zemřel	zemřít	k5eAaPmAgInS	zemřít
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
mladá	mladý	k2eAgFnSc1d1	mladá
manželka	manželka	k1gFnSc1	manželka
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
obvinila	obvinit	k5eAaPmAgFnS	obvinit
Marilyn	Marilyn	k1gFnSc1	Marilyn
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
natáčení	natáčení	k1gNnSc2	natáčení
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
způsobila	způsobit	k5eAaPmAgFnS	způsobit
jejímu	její	k3xOp3gMnSc3	její
muži	muž	k1gMnSc3	muž
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Zdrcená	zdrcený	k2eAgFnSc1d1	zdrcená
Marilyn	Marilyn	k1gFnSc1	Marilyn
se	se	k3xPyFc4	se
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Millerem	Miller	k1gMnSc7	Miller
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1961	[number]	k4	1961
jí	jíst	k5eAaImIp3nS	jíst
pak	pak	k6eAd1	pak
její	její	k3xOp3gFnSc1	její
lékařka	lékařka	k1gFnSc1	lékařka
nechala	nechat	k5eAaPmAgFnS	nechat
umístit	umístit	k5eAaPmF	umístit
na	na	k7c4	na
kliniku	klinika	k1gFnSc4	klinika
Paynea	Payneus	k1gMnSc2	Payneus
Whitneyho	Whitney	k1gMnSc2	Whitney
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k8xS	jako
fatální	fatální	k2eAgNnSc4d1	fatální
lékařské	lékařský	k2eAgNnSc4d1	lékařské
pochybení	pochybení	k1gNnSc4	pochybení
<g/>
:	:	kIx,	:
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c4	na
patro	patro	k1gNnSc4	patro
pro	pro	k7c4	pro
nejtěžší	těžký	k2eAgInPc4d3	nejtěžší
případy	případ	k1gInPc4	případ
do	do	k7c2	do
vypolštářované	vypolštářovaný	k2eAgFnSc2d1	vypolštářovaná
cely	cela	k1gFnSc2	cela
bez	bez	k7c2	bez
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
dveří	dveře	k1gFnPc2	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc4	její
zoufalé	zoufalý	k2eAgFnPc4d1	zoufalá
prosby	prosba	k1gFnPc4	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
nikdo	nikdo	k3yNnSc1	nikdo
nevyslyšel	vyslyšet	k5eNaPmAgInS	vyslyšet
<g/>
,	,	kIx,	,
jen	jen	k9	jen
Joe	Joe	k1gMnSc1	Joe
DiMaggio	DiMaggio	k1gMnSc1	DiMaggio
přispěchal	přispěchat	k5eAaPmAgMnS	přispěchat
z	z	k7c2	z
Floridy	Florida	k1gFnSc2	Florida
a	a	k8xC	a
zajistil	zajistit	k5eAaPmAgMnS	zajistit
její	její	k3xOp3gNnPc4	její
přeložení	přeložení	k1gNnPc4	přeložení
na	na	k7c6	na
Columbia	Columbia	k1gFnSc1	Columbia
Presbyterian	Presbyterian	k1gInSc1	Presbyterian
Medical	Medical	k1gFnSc4	Medical
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
hrůzného	hrůzný	k2eAgInSc2d1	hrůzný
zážitku	zážitek	k1gInSc2	zážitek
zotavila	zotavit	k5eAaPmAgFnS	zotavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Connoverem	Connover	k1gMnSc7	Connover
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Marilyn	Marilyn	k1gFnSc4	Marilyn
občas	občas	k6eAd1	občas
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
<g/>
,	,	kIx,	,
Marilyn	Marilyn	k1gFnPc1	Marilyn
o	o	k7c6	o
Mustanzích	mustang	k1gMnPc6	mustang
a	a	k8xC	a
Millerovi	Miller	k1gMnSc3	Miller
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Měl	mít	k5eAaImAgMnS	mít
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
NÁŠ	náš	k3xOp1gInSc4	náš
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Arthur	Arthur	k1gMnSc1	Arthur
mě	já	k3xPp1nSc4	já
využil	využít	k5eAaPmAgMnS	využít
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zase	zase	k9	zase
získal	získat	k5eAaPmAgMnS	získat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pověst	pověst	k1gFnSc4	pověst
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
mu	on	k3xPp3gMnSc3	on
nikdy	nikdy	k6eAd1	nikdy
nezapomenu	zapomnět	k5eNaImIp1nS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Změnil	změnit	k5eAaPmAgInS	změnit
scénář	scénář	k1gInSc1	scénář
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
mně	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc1	muž
<g/>
...	...	k?	...
<g/>
ta	ten	k3xDgFnSc1	ten
holka	holka	k1gFnSc1	holka
(	(	kIx(	(
<g/>
Roslyn	Roslyn	k1gInSc1	Roslyn
–	–	k?	–
role	role	k1gFnSc1	role
Marilyn	Marilyn	k1gFnSc1	Marilyn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
přívažek	přívažek	k1gInSc4	přívažek
a	a	k8xC	a
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
nejsem	být	k5eNaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
</s>
<s>
Miller	Miller	k1gMnSc1	Miller
dovolil	dovolit	k5eAaPmAgMnS	dovolit
Hustonovi	Huston	k1gMnSc3	Huston
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ten	ten	k3xDgInSc1	ten
film	film	k1gInSc1	film
zničil	zničit	k5eAaPmAgInS	zničit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Sandra	Sandra	k1gFnSc1	Sandra
Shevey	Shevea	k1gFnSc2	Shevea
<g/>
,	,	kIx,	,
Skandál	skandál	k1gInSc4	skandál
jménem	jméno	k1gNnSc7	jméno
Marilyn	Marilyn	k1gFnPc2	Marilyn
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
379	[number]	k4	379
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
měla	mít	k5eAaImAgFnS	mít
nebo	nebo	k8xC	nebo
sama	sám	k3xTgFnSc1	sám
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
hovořilo	hovořit	k5eAaImAgNnS	hovořit
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
fyzicky	fyzicky	k6eAd1	fyzicky
ani	ani	k8xC	ani
psychicky	psychicky	k6eAd1	psychicky
odolná	odolný	k2eAgFnSc1d1	odolná
a	a	k8xC	a
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
napětí	napětí	k1gNnSc3	napětí
a	a	k8xC	a
tlaku	tlak	k1gInSc3	tlak
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
život	život	k1gInSc4	život
v	v	k7c6	v
hollywoodské	hollywoodský	k2eAgFnSc6d1	hollywoodská
branži	branže	k1gFnSc6	branže
nese	nést	k5eAaImIp3nS	nést
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
vysilovalo	vysilovat	k5eAaImAgNnS	vysilovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
prodělala	prodělat	k5eAaPmAgFnS	prodělat
akutní	akutní	k2eAgInSc4d1	akutní
zánět	zánět	k1gInSc4	zánět
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
v	v	k7c6	v
27	[number]	k4	27
letech	léto	k1gNnPc6	léto
jí	on	k3xPp3gFnSc3	on
diagnostikovali	diagnostikovat	k5eAaBmAgMnP	diagnostikovat
endometriózu	endometrióza	k1gFnSc4	endometrióza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jí	on	k3xPp3gFnSc3	on
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
velké	velký	k2eAgFnPc4d1	velká
předmenstruační	předmenstruační	k2eAgFnPc4d1	předmenstruační
a	a	k8xC	a
menstruační	menstruační	k2eAgFnPc4d1	menstruační
bolesti	bolest	k1gFnPc4	bolest
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
nikdy	nikdy	k6eAd1	nikdy
nedonosila	donosit	k5eNaPmAgFnS	donosit
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Tradovalo	tradovat	k5eAaImAgNnS	tradovat
se	se	k3xPyFc4	se
o	o	k7c6	o
nespočetných	spočetný	k2eNgInPc6d1	nespočetný
potratech	potrat	k1gInPc6	potrat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
prodělala	prodělat	k5eAaPmAgFnS	prodělat
<g/>
,	,	kIx,	,
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
s	s	k7c7	s
DiMaggiem	DiMaggius	k1gMnSc7	DiMaggius
potratila	potratit	k5eAaPmAgFnS	potratit
jednou	jednou	k6eAd1	jednou
a	a	k8xC	a
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
s	s	k7c7	s
Millerem	Miller	k1gMnSc7	Miller
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
dvakrát	dvakrát	k6eAd1	dvakrát
o	o	k7c4	o
mimoděložní	mimoděložní	k2eAgNnSc4d1	mimoděložní
těhotenství	těhotenství	k1gNnSc4	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Mnohokrát	mnohokrát	k6eAd1	mnohokrát
prodělala	prodělat	k5eAaPmAgFnS	prodělat
gynekologické	gynekologický	k2eAgInPc4d1	gynekologický
zákroky	zákrok	k1gInPc4	zákrok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
napravit	napravit	k5eAaPmF	napravit
její	její	k3xOp3gFnPc4	její
ženské	ženský	k2eAgFnPc4d1	ženská
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
jí	jíst	k5eAaImIp3nS	jíst
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
dítě	dítě	k1gNnSc4	dítě
donosit	donosit	k5eAaPmF	donosit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
pracovní	pracovní	k2eAgFnSc6d1	pracovní
smlouvě	smlouva	k1gFnSc6	smlouva
měla	mít	k5eAaImAgFnS	mít
dokonce	dokonce	k9	dokonce
od	od	k7c2	od
r.	r.	kA	r.
<g/>
1956	[number]	k4	1956
dodatek	dodatek	k1gInSc4	dodatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
menstruace	menstruace	k1gFnSc2	menstruace
nebude	být	k5eNaImBp3nS	být
natáčet	natáčet	k5eAaImF	natáčet
ani	ani	k8xC	ani
stát	stát	k5eAaPmF	stát
modelem	model	k1gInSc7	model
pro	pro	k7c4	pro
propagační	propagační	k2eAgInPc4d1	propagační
materiály	materiál	k1gInPc4	materiál
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
jí	jíst	k5eAaImIp3nS	jíst
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
žlučníkovém	žlučníkový	k2eAgInSc6d1	žlučníkový
záchvatu	záchvat	k1gInSc6	záchvat
odoperován	odoperován	k2eAgInSc4d1	odoperován
žlučník	žlučník	k1gInSc4	žlučník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
životopisci	životopisec	k1gMnPc1	životopisec
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
i	i	k9	i
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpěla	trpět	k5eAaImAgFnS	trpět
Meniérovou	Meniérův	k2eAgFnSc7d1	Meniérova
chorobou	choroba	k1gFnSc7	choroba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
léčba	léčba	k1gFnSc1	léčba
si	se	k3xPyFc3	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
a	a	k8xC	a
střídmý	střídmý	k2eAgInSc4d1	střídmý
jídelníček	jídelníček	k1gInSc4	jídelníček
<g/>
,	,	kIx,	,
vyloučení	vyloučení	k1gNnSc4	vyloučení
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
kofeinu	kofein	k1gInSc2	kofein
a	a	k8xC	a
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
při	při	k7c6	při
životním	životní	k2eAgInSc6d1	životní
stylu	styl	k1gInSc6	styl
Marilyn	Marilyn	k1gFnSc2	Marilyn
bylo	být	k5eAaImAgNnS	být
dosti	dosti	k6eAd1	dosti
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
jí	on	k3xPp3gFnSc3	on
měla	mít	k5eAaImAgFnS	mít
způsobovat	způsobovat	k5eAaImF	způsobovat
nedoslýchavost	nedoslýchavost	k1gFnSc4	nedoslýchavost
<g/>
,	,	kIx,	,
dyslexii	dyslexie	k1gFnSc4	dyslexie
a	a	k8xC	a
možná	možná	k9	možná
jejím	její	k3xOp3gInSc7	její
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
posledního	poslední	k2eAgInSc2d1	poslední
filmu	film	k1gInSc2	film
podle	podle	k7c2	podle
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
vypadala	vypadat	k5eAaPmAgFnS	vypadat
"	"	kIx"	"
<g/>
mimo	mimo	k7c4	mimo
<g/>
"	"	kIx"	"
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
nechápala	chápat	k5eNaImAgFnS	chápat
(	(	kIx(	(
<g/>
neslyšela	slyšet	k5eNaImAgFnS	slyšet
<g/>
)	)	kIx)	)
pokyny	pokyn	k1gInPc7	pokyn
režiséra	režisér	k1gMnSc2	režisér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mathew	Mathew	k?	Mathew
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knihy	kniha	k1gFnSc2	kniha
Oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
pozastavuje	pozastavovat	k5eAaImIp3nS	pozastavovat
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nikdo	nikdo	k3yNnSc1	nikdo
hlouběji	hluboko	k6eAd2	hluboko
neřešil	řešit	k5eNaImAgMnS	řešit
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jí	jíst	k5eAaImIp3nS	jíst
studiový	studiový	k2eAgMnSc1d1	studiový
lékař	lékař	k1gMnSc1	lékař
odhalil	odhalit	k5eAaPmAgMnS	odhalit
hyperglykemii	hyperglykemie	k1gFnSc4	hyperglykemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
vezmeme	vzít	k5eAaPmIp1nP	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
její	její	k3xOp3gFnSc4	její
chronickou	chronický	k2eAgFnSc4d1	chronická
nespavost	nespavost	k1gFnSc4	nespavost
(	(	kIx(	(
<g/>
kterou	který	k3yIgFnSc4	který
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
např.	např.	kA	např.
i	i	k8xC	i
choreograf	choreograf	k1gMnSc1	choreograf
Jack	Jack	k1gInSc1	Jack
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vedl	vést	k5eAaImAgInS	vést
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
Blondýnek	blondýnka	k1gFnPc2	blondýnka
<g/>
,	,	kIx,	,
když	když	k8xS	když
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ptáte	ptat	k5eAaImIp2nP	ptat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byla	být	k5eAaImAgFnS	být
nervózní	nervózní	k2eAgFnSc1d1	nervózní
a	a	k8xC	a
chodila	chodit	k5eAaImAgFnS	chodit
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
odpovědí	odpověď	k1gFnSc7	odpověď
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
chyběl	chybět	k5eAaImAgInS	chybět
zdravý	zdravý	k2eAgInSc1d1	zdravý
spánek	spánek	k1gInSc1	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
úplně	úplně	k6eAd1	úplně
grogy	grog	k1gInPc1	grog
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
necítí	cítit	k5eNaImIp3nS	cítit
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Sandra	Sandra	k1gFnSc1	Sandra
Shevey	Shevea	k1gFnSc2	Shevea
<g/>
,	,	kIx,	,
Skandál	skandál	k1gInSc4	skandál
jménem	jméno	k1gNnSc7	jméno
Marilyn	Marilyn	k1gFnPc2	Marilyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psychickou	psychický	k2eAgFnSc4d1	psychická
labilitu	labilita	k1gFnSc4	labilita
<g/>
,	,	kIx,	,
dědičné	dědičný	k2eAgNnSc4d1	dědičné
zatížení	zatížení	k1gNnSc4	zatížení
k	k	k7c3	k
psychickým	psychický	k2eAgFnPc3d1	psychická
poruchám	porucha	k1gFnPc3	porucha
a	a	k8xC	a
únavu	únava	k1gFnSc4	únava
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
všeho	všecek	k3xTgNnSc2	všecek
plynoucí	plynoucí	k2eAgNnSc1d1	plynoucí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
těžké	těžký	k2eAgFnSc6d1	těžká
situaci	situace	k1gFnSc6	situace
–	–	k?	–
studioví	studiový	k2eAgMnPc1d1	studiový
bossové	boss	k1gMnPc1	boss
ji	on	k3xPp3gFnSc4	on
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
hypochondra	hypochondr	k1gMnSc4	hypochondr
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
ale	ale	k9	ale
měla	mít	k5eAaImAgFnS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
dost	dost	k6eAd1	dost
pádných	pádný	k2eAgInPc2d1	pádný
důvodů	důvod	k1gInPc2	důvod
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
si	se	k3xPyFc3	se
stěžovat	stěžovat	k5eAaImF	stěžovat
a	a	k8xC	a
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgNnSc3	svůj
zdraví	zdraví	k1gNnSc3	zdraví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norman	Norman	k1gMnSc1	Norman
Mailer	Mailer	k1gMnSc1	Mailer
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Měla	mít	k5eAaImAgFnS	mít
nemanželský	manželský	k2eNgInSc4d1	nemanželský
původ	původ	k1gInSc4	původ
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
rodokmen	rodokmen	k1gInSc4	rodokmen
šílenství	šílenství	k1gNnSc2	šílenství
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
traumatizujícího	traumatizující	k2eAgNnSc2d1	traumatizující
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
bumerang	bumerang	k1gInSc1	bumerang
vrátily	vrátit	k5eAaPmAgInP	vrátit
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
psychických	psychický	k2eAgFnPc2d1	psychická
poruch	porucha	k1gFnPc2	porucha
a	a	k8xC	a
dyslexie	dyslexie	k1gFnSc2	dyslexie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Marilyn	Marilyn	k1gFnSc1	Marilyn
značně	značně	k6eAd1	značně
obtěžovala	obtěžovat	k5eAaImAgFnS	obtěžovat
při	při	k7c6	při
učení	učení	k1gNnSc6	učení
se	se	k3xPyFc4	se
scénářů	scénář	k1gInPc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
všem	všecek	k3xTgMnPc3	všecek
psychiatrům	psychiatr	k1gMnPc3	psychiatr
a	a	k8xC	a
psychoanalytikům	psychoanalytik	k1gMnPc3	psychoanalytik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
bohatli	bohatnout	k5eAaImAgMnP	bohatnout
na	na	k7c6	na
nemocných	nemocný	k2eAgFnPc6d1	nemocná
duších	duše	k1gFnPc6	duše
filmových	filmový	k2eAgFnPc2d1	filmová
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
Marilyn	Marilyn	k1gFnSc1	Marilyn
nebyla	být	k5eNaImAgFnS	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
špatný	špatný	k2eAgInSc1d1	špatný
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
samozřejmě	samozřejmě	k6eAd1	samozřejmě
lákal	lákat	k5eAaImAgInS	lákat
k	k	k7c3	k
výdělku	výdělek	k1gInSc3	výdělek
a	a	k8xC	a
slávě	sláva	k1gFnSc3	sláva
jako	jako	k8xC	jako
lampa	lampa	k1gFnSc1	lampa
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
můry	můra	k1gFnPc4	můra
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
psychiatr	psychiatr	k1gMnSc1	psychiatr
Ralph	Ralph	k1gMnSc1	Ralph
Greenson	Greenson	k1gMnSc1	Greenson
byl	být	k5eAaImAgMnS	být
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
překročil	překročit	k5eAaPmAgMnS	překročit
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
vůči	vůči	k7c3	vůči
Marilyn	Marilyn	k1gFnPc3	Marilyn
vztah	vztah	k1gInSc4	vztah
lékaře	lékař	k1gMnSc2	lékař
a	a	k8xC	a
pacientky	pacientka	k1gFnPc4	pacientka
a	a	k8xC	a
nesl	nést	k5eAaImAgMnS	nést
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
tragické	tragický	k2eAgFnSc6d1	tragická
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jí	jíst	k5eAaImIp3nS	jíst
nezodpovědně	zodpovědně	k6eNd1	zodpovědně
a	a	k8xC	a
naivně	naivně	k6eAd1	naivně
předepisoval	předepisovat	k5eAaImAgMnS	předepisovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
ale	ale	k9	ale
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marilyn	Marilyn	k1gFnSc1	Marilyn
dostává	dostávat	k5eAaImIp3nS	dostávat
jiné	jiný	k2eAgInPc4d1	jiný
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c6	na
spaní	spaní	k1gNnSc6	spaní
od	od	k7c2	od
jeho	jeho	k3xOp3gMnSc2	jeho
kolegy	kolega	k1gMnSc2	kolega
(	(	kIx(	(
<g/>
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
vynořily	vynořit	k5eAaPmAgInP	vynořit
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
záměrně	záměrně	k6eAd1	záměrně
zabraňoval	zabraňovat	k5eAaImAgMnS	zabraňovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c6	na
barbiturátech	barbiturát	k1gInPc6	barbiturát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
o	o	k7c4	o
svoji	svůj	k3xOyFgFnSc4	svůj
milovanou	milovaný	k2eAgFnSc4d1	milovaná
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
velice	velice	k6eAd1	velice
slavnou	slavný	k2eAgFnSc4d1	slavná
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc4d1	bohatá
<g/>
)	)	kIx)	)
pacientku	pacientka	k1gFnSc4	pacientka
nepřišel	přijít	k5eNaPmAgMnS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Donalda	Donald	k1gMnSc2	Donald
Spota	Spot	k1gInSc2	Spot
Greenson	Greenson	k1gMnSc1	Greenson
dokonce	dokonce	k9	dokonce
sehnal	sehnat	k5eAaPmAgMnS	sehnat
internistu	internista	k1gMnSc4	internista
<g/>
,	,	kIx,	,
Hymana	Hyman	k1gMnSc4	Hyman
Engelberga	Engelberg	k1gMnSc4	Engelberg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
herečce	herečka	k1gFnSc3	herečka
předepisoval	předepisovat	k5eAaImAgMnS	předepisovat
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Greenson	Greenson	k1gInSc1	Greenson
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
psychiatr	psychiatr	k1gMnSc1	psychiatr
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
s	s	k7c7	s
přímým	přímý	k2eAgNnSc7d1	přímé
předepisováním	předepisování	k1gNnSc7	předepisování
léků	lék	k1gInPc2	lék
co	co	k9	co
do	do	k7c2	do
činění	činění	k1gNnSc2	činění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
doporučení	doporučení	k1gNnSc4	doporučení
si	se	k3xPyFc3	se
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
haciendu	hacienda	k1gFnSc4	hacienda
v	v	k7c6	v
mexickém	mexický	k2eAgInSc6d1	mexický
stylu	styl	k1gInSc6	styl
v	v	k7c6	v
Brentwoodu	Brentwood	k1gInSc6	Brentwood
na	na	k7c4	na
5	[number]	k4	5
<g/>
th	th	k?	th
Helena	Helena	k1gFnSc1	Helena
Drive	drive	k1gInSc1	drive
jen	jen	k9	jen
kousek	kousek	k1gInSc1	kousek
od	od	k7c2	od
domu	dům	k1gInSc2	dům
Greensona	Greenson	k1gMnSc2	Greenson
<g/>
,	,	kIx,	,
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
domu	dům	k1gInSc2	dům
stál	stát	k5eAaImAgInS	stát
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Zde	zde	k6eAd1	zde
končí	končit	k5eAaImIp3nS	končit
má	můj	k3xOp1gFnSc1	můj
pouť	pouť	k1gFnSc1	pouť
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přímo	přímo	k6eAd1	přímo
osudový	osudový	k2eAgInSc4d1	osudový
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nadšená	nadšený	k2eAgFnSc1d1	nadšená
<g/>
,	,	kIx,	,
po	po	k7c6	po
letech	let	k1gInPc6	let
v	v	k7c6	v
pronajatých	pronajatý	k2eAgInPc6d1	pronajatý
apartmánech	apartmán	k1gInPc6	apartmán
měla	mít	k5eAaImAgFnS	mít
konečně	konečně	k9	konečně
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
dům	dům	k1gInSc4	dům
a	a	k8xC	a
útočiště	útočiště	k1gNnSc4	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
Propadla	propadnout	k5eAaPmAgFnS	propadnout
jeho	jeho	k3xOp3gNnSc4	jeho
zařizování	zařizování	k1gNnSc4	zařizování
a	a	k8xC	a
zvelebování	zvelebování	k1gNnSc4	zvelebování
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
pozemku	pozemek	k1gInSc2	pozemek
s	s	k7c7	s
bazénem	bazén	k1gInSc7	bazén
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
se	se	k3xPyFc4	se
tak	tak	k9	tak
podle	podle	k7c2	podle
mnohých	mnohý	k2eAgMnPc2d1	mnohý
životopisců	životopisec	k1gMnPc2	životopisec
také	také	k6eAd1	také
stal	stát	k5eAaPmAgMnS	stát
svědkem	svědek	k1gMnSc7	svědek
několika	několik	k4yIc7	několik
jejích	její	k3xOp3gFnPc2	její
schůzek	schůzka	k1gFnPc2	schůzka
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
Kennedyovými	Kennedyův	k2eAgMnPc7d1	Kennedyův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Něco	něco	k3yInSc1	něco
musí	muset	k5eAaImIp3nS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
==	==	k?	==
</s>
</p>
<p>
<s>
Přijala	přijmout	k5eAaPmAgFnS	přijmout
roli	role	k1gFnSc3	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Něco	něco	k3yInSc4	něco
musí	muset	k5eAaImIp3nS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
kývla	kývnout	k5eAaPmAgFnS	kývnout
na	na	k7c4	na
nabídky	nabídka	k1gFnPc4	nabídka
magazínů	magazín	k1gInPc2	magazín
Vogue	Vogue	k1gInSc1	Vogue
a	a	k8xC	a
Cosmopolitan	Cosmopolitan	k1gInSc1	Cosmopolitan
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
série	série	k1gFnSc2	série
fotek	fotka	k1gFnPc2	fotka
<g/>
.	.	kIx.	.
</s>
<s>
Bert	Berta	k1gFnPc2	Berta
Stern	sternum	k1gNnPc2	sternum
s	s	k7c7	s
ní	on	k3xPp3gFnSc2	on
pak	pak	k6eAd1	pak
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1962	[number]	k4	1962
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
akty	akt	k1gInPc4	akt
<g/>
,	,	kIx,	,
Richardovi	Richard	k1gMnSc6	Richard
Merymanovi	Meryman	k1gMnSc6	Meryman
z	z	k7c2	z
magazínu	magazín	k1gInSc2	magazín
Life	Lif	k1gMnSc2	Lif
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
rozhovor	rozhovor	k1gInSc4	rozhovor
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInPc4d1	poslední
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
<g/>
)	)	kIx)	)
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
sláva	sláva	k1gFnSc1	sláva
<g/>
,	,	kIx,	,
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
fotografiemi	fotografia	k1gFnPc7	fotografia
význačného	význačný	k2eAgMnSc2d1	význačný
fotožurnalisty	fotožurnalista	k1gMnSc2	fotožurnalista
Allan	Allan	k1gMnSc1	Allan
Granta	Granta	k1gMnSc1	Granta
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
dala	dát	k5eAaPmAgFnS	dát
svolení	svolení	k1gNnSc4	svolení
fotit	fotit	k5eAaImF	fotit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
novém	nový	k2eAgNnSc6d1	nové
<g/>
,	,	kIx,	,
ne	ne	k9	ne
ještě	ještě	k9	ještě
kompletně	kompletně	k6eAd1	kompletně
zařízeném	zařízený	k2eAgInSc6d1	zařízený
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
měsíce	měsíc	k1gInPc1	měsíc
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
nesly	nést	k5eAaImAgFnP	nést
spíše	spíše	k9	spíše
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
plného	plný	k2eAgNnSc2d1	plné
pracovního	pracovní	k2eAgNnSc2d1	pracovní
nasazení	nasazení	k1gNnSc2	nasazení
(	(	kIx(	(
<g/>
sezení	sezení	k1gNnSc4	sezení
u	u	k7c2	u
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
:	:	kIx,	:
Douglas	Douglasa	k1gFnPc2	Douglasa
Kirkland	Kirkland	k1gInSc1	Kirkland
<g/>
,	,	kIx,	,
Willy	Will	k1gInPc1	Will
Ritzo	Ritza	k1gFnSc5	Ritza
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
Barris	Barris	k1gFnSc2	Barris
<g/>
,	,	kIx,	,
Bert	Berta	k1gFnPc2	Berta
Stern	sternum	k1gNnPc2	sternum
<g/>
..	..	k?	..
<g/>
,	,	kIx,	,
poskytování	poskytování	k1gNnSc1	poskytování
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnSc2	jednání
o	o	k7c4	o
nových	nový	k2eAgInPc2d1	nový
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
projektech	projekt	k1gInPc6	projekt
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
oslavě	oslava	k1gFnSc6	oslava
45	[number]	k4	45
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
JFK	JFK	kA	JFK
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
..	..	k?	..
<g/>
)	)	kIx)	)
než	než	k8xS	než
úpadku	úpadek	k1gInSc2	úpadek
filmové	filmový	k2eAgFnSc2d1	filmová
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nedostávala	dostávat	k5eNaImAgFnS	dostávat
žádné	žádný	k3yNgFnPc4	žádný
pracovní	pracovní	k2eAgFnPc4d1	pracovní
nabídky	nabídka	k1gFnPc4	nabídka
a	a	k8xC	a
bojovala	bojovat	k5eAaImAgFnS	bojovat
s	s	k7c7	s
depresemi	deprese	k1gFnPc7	deprese
stárnoucí	stárnoucí	k2eAgFnSc2d1	stárnoucí
herečky	herečka	k1gFnSc2	herečka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
některá	některý	k3yIgNnPc4	některý
média	médium	k1gNnPc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dopisu	dopis	k1gInSc2	dopis
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
1961	[number]	k4	1961
Lee	Lea	k1gFnSc3	Lea
Strasbergovi	Strasberg	k1gMnSc3	Strasberg
dokonce	dokonce	k9	dokonce
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Marilyn	Marilyn	k1gFnSc1	Marilyn
nevzdala	vzdát	k5eNaPmAgFnS	vzdát
myšlenek	myšlenka	k1gFnPc2	myšlenka
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
produkční	produkční	k2eAgFnSc4d1	produkční
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zamýšlela	zamýšlet	k5eAaImAgFnS	zamýšlet
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
vedoucích	vedoucí	k2eAgFnPc2d1	vedoucí
pozic	pozice	k1gFnPc2	pozice
obsadit	obsadit	k5eAaPmF	obsadit
právě	právě	k9	právě
Strasberga	Strasberg	k1gMnSc4	Strasberg
a	a	k8xC	a
navázat	navázat	k5eAaPmF	navázat
tak	tak	k9	tak
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
studium	studium	k1gNnSc4	studium
herectví	herectví	k1gNnPc2	herectví
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc2d1	divadelní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
filmu	film	k1gInSc2	film
Něco	něco	k3yInSc1	něco
musí	muset	k5eAaImIp3nS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
neslo	nést	k5eAaImAgNnS	nést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
boje	boj	k1gInSc2	boj
mezi	mezi	k7c7	mezi
Monroe	Monroe	k1gFnSc7	Monroe
a	a	k8xC	a
Georgem	Georg	k1gMnSc7	Georg
Cukorem	Cukor	k1gMnSc7	Cukor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
film	film	k1gInSc4	film
režíroval	režírovat	k5eAaImAgMnS	režírovat
<g/>
.	.	kIx.	.
</s>
<s>
Marilyn	Marilyn	k1gFnSc7	Marilyn
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgInS	líbit
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
natáčení	natáčení	k1gNnSc4	natáčení
okořenila	okořenit	k5eAaPmAgFnS	okořenit
bazénovou	bazénový	k2eAgFnSc7d1	bazénová
scénou	scéna	k1gFnSc7	scéna
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
odhodila	odhodit	k5eAaPmAgFnS	odhodit
i	i	k9	i
plavky	plavka	k1gFnPc4	plavka
tělové	tělový	k2eAgFnPc4d1	tělová
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
byli	být	k5eAaImAgMnP	být
ponecháni	ponechat	k5eAaPmNgMnP	ponechat
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
fotografové	fotograf	k1gMnPc1	fotograf
–	–	k?	–
William	William	k1gInSc1	William
Billy	Bill	k1gMnPc4	Bill
Woodfield	Woodfield	k1gMnSc1	Woodfield
z	z	k7c2	z
Globe	globus	k1gInSc5	globus
Photos	Photosa	k1gFnPc2	Photosa
a	a	k8xC	a
Lawrence	Lawrence	k1gFnSc2	Lawrence
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
MM	mm	kA	mm
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
již	již	k6eAd1	již
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Milujme	milovat	k5eAaImRp1nP	milovat
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
Nahá	nahý	k2eAgFnSc1d1	nahá
Marilyn	Marilyn	k1gFnSc1	Marilyn
po	po	k7c6	po
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
titulní	titulní	k2eAgFnPc4d1	titulní
stránky	stránka	k1gFnPc4	stránka
všech	všecek	k3xTgInPc2	všecek
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
za	za	k7c2	za
její	její	k3xOp3gFnSc2	její
nahé	nahý	k2eAgFnSc2d1	nahá
fotky	fotka	k1gFnSc2	fotka
zaplatily	zaplatit	k5eAaPmAgFnP	zaplatit
nevídané	vídaný	k2eNgFnPc1d1	nevídaná
sumy	suma	k1gFnPc1	suma
(	(	kIx(	(
<g/>
Playboy	playboy	k1gMnSc1	playboy
dokonce	dokonce	k9	dokonce
25	[number]	k4	25
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Hugh	Hugh	k1gMnSc1	Hugh
Hefner	Hefner	k1gMnSc1	Hefner
nestihl	stihnout	k5eNaPmAgMnS	stihnout
v	v	k7c6	v
Playboyi	playboy	k1gMnSc6	playboy
fotografie	fotografia	k1gFnSc2	fotografia
nahé	nahý	k2eAgFnSc2d1	nahá
MM	mm	kA	mm
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
ještě	ještě	k9	ještě
za	za	k7c2	za
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
Heffner	Heffner	k1gInSc1	Heffner
z	z	k7c2	z
úcty	úcta	k1gFnSc2	úcta
k	k	k7c3	k
MM	mm	kA	mm
počkal	počkat	k5eAaPmAgInS	počkat
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
a	a	k8xC	a
paradoxně	paradoxně	k6eAd1	paradoxně
byly	být	k5eAaImAgFnP	být
fotografie	fotografia	k1gFnPc1	fotografia
uveřejněny	uveřejněn	k2eAgFnPc1d1	uveřejněna
v	v	k7c4	v
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
zastřelen	zastřelen	k2eAgMnSc1d1	zastřelen
JFK	JFK	kA	JFK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
30	[number]	k4	30
natáčecích	natáčecí	k2eAgInPc2d1	natáčecí
dní	den	k1gInPc2	den
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
celých	celá	k1gFnPc6	celá
21	[number]	k4	21
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
volno	volno	k1gNnSc4	volno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
prezidentovi	prezident	k1gMnSc3	prezident
Kennedymu	Kennedym	k1gInSc3	Kennedym
na	na	k7c6	na
oslavě	oslava	k1gFnSc6	oslava
jeho	jeho	k3xOp3gNnSc2	jeho
45	[number]	k4	45
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Šaty	šata	k1gFnPc1	šata
od	od	k7c2	od
francouzského	francouzský	k2eAgMnSc2d1	francouzský
návrháře	návrhář	k1gMnSc2	návrhář
Jeana	Jean	k1gMnSc2	Jean
Louise	Louis	k1gMnSc2	Louis
za	za	k7c2	za
tehdy	tehdy	k6eAd1	tehdy
neuvěřitelných	uvěřitelný	k2eNgNnPc2d1	neuvěřitelné
12	[number]	k4	12
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
nenechaly	nechat	k5eNaPmAgInP	nechat
nikoho	nikdo	k3yNnSc4	nikdo
na	na	k7c6	na
pochybách	pochyba	k1gFnPc6	pochyba
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
nimi	on	k3xPp3gMnPc7	on
Marilyn	Marilyn	k1gFnSc1	Marilyn
nahá	nahý	k2eAgFnSc1d1	nahá
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
zpěvem	zpěv	k1gInSc7	zpěv
jasně	jasně	k6eAd1	jasně
dala	dát	k5eAaPmAgFnS	dát
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
existuje	existovat	k5eAaImIp3nS	existovat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
studio	studio	k1gNnSc4	studio
Fox	fox	k1gInSc4	fox
byla	být	k5eAaImAgFnS	být
událost	událost	k1gFnSc1	událost
s	s	k7c7	s
New	New	k1gFnSc7	New
Yorkem	York	k1gInSc7	York
poslední	poslední	k2eAgFnSc7d1	poslední
kapkou	kapka	k1gFnSc7	kapka
<g/>
,	,	kIx,	,
Marilyn	Marilyn	k1gFnSc7	Marilyn
nejdříve	dříve	k6eAd3	dříve
zažalovalo	zažalovat	k5eAaPmAgNnS	zažalovat
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vyhodilo	vyhodit	k5eAaPmAgNnS	vyhodit
a	a	k8xC	a
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
ji	on	k3xPp3gFnSc4	on
o	o	k7c4	o
9	[number]	k4	9
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgMnSc1d2	mladší
herečkou	herečka	k1gFnSc7	herečka
Lee	Lea	k1gFnSc3	Lea
Remick	Remicka	k1gFnPc2	Remicka
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgInP	začít
pak	pak	k6eAd1	pak
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
Marilyn	Marilyn	k1gFnSc7	Marilyn
a	a	k8xC	a
studiovými	studiový	k2eAgMnPc7d1	studiový
právníky	právník	k1gMnPc7	právník
<g/>
.	.	kIx.	.
</s>
<s>
Marilynin	Marilynin	k2eAgMnSc1d1	Marilynin
partner	partner	k1gMnSc1	partner
z	z	k7c2	z
filmu	film	k1gInSc2	film
Dean	Dean	k1gMnSc1	Dean
Martin	Martin	k1gMnSc1	Martin
se	se	k3xPyFc4	se
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
vložil	vložit	k5eAaPmAgInS	vložit
s	s	k7c7	s
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
smlouvě	smlouva	k1gFnSc6	smlouva
má	mít	k5eAaImIp3nS	mít
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
natáčet	natáčet	k5eAaImF	natáčet
bude	být	k5eAaImBp3nS	být
s	s	k7c7	s
Marilyn	Marilyn	k1gFnSc7	Marilyn
Monroe	Monro	k1gFnSc2	Monro
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
s	s	k7c7	s
Lee	Lea	k1gFnSc6	Lea
Remick	Remick	k1gInSc4	Remick
a	a	k8xC	a
studio	studio	k1gNnSc4	studio
naopak	naopak	k6eAd1	naopak
zažaloval	zažalovat	k5eAaPmAgMnS	zažalovat
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
soudních	soudní	k2eAgFnPc2d1	soudní
tahanic	tahanice	k1gFnPc2	tahanice
vkročil	vkročit	k5eAaPmAgMnS	vkročit
i	i	k9	i
scenárista	scenárista	k1gMnSc1	scenárista
filmu	film	k1gInSc2	film
Nunnally	Nunnalla	k1gFnSc2	Nunnalla
Johnson	Johnson	k1gMnSc1	Johnson
s	s	k7c7	s
jasným	jasný	k2eAgNnSc7d1	jasné
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
diváky	divák	k1gMnPc7	divák
nepřitáhne	přitáhnout	k5eNaPmIp3nS	přitáhnout
a	a	k8xC	a
pokladny	pokladna	k1gFnSc2	pokladna
nenaplní	naplnit	k5eNaPmIp3nS	naplnit
George	George	k1gFnSc4	George
Cukor	Cukora	k1gFnPc2	Cukora
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
a	a	k8xC	a
tak	tak	k6eAd1	tak
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
bojů	boj	k1gInPc2	boj
studio	studio	k1gNnSc4	studio
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
Marilyn	Marilyn	k1gFnSc4	Marilyn
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
nejen	nejen	k6eAd1	nejen
výměnu	výměna	k1gFnSc4	výměna
režiséra	režisér	k1gMnSc2	režisér
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
<g/>
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
lepší	dobrý	k2eAgInPc4d2	lepší
peníze	peníz	k1gInPc4	peníz
(	(	kIx(	(
<g/>
MM	mm	kA	mm
hodlala	hodlat	k5eAaImAgFnS	hodlat
požadovat	požadovat	k5eAaImF	požadovat
sumu	suma	k1gFnSc4	suma
10	[number]	k4	10
<g/>
×	×	k?	×
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
příjem	příjem	k1gInSc1	příjem
za	za	k7c4	za
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
činil	činit	k5eAaImAgInS	činit
100	[number]	k4	100
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
studio	studio	k1gNnSc1	studio
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
ohromnými	ohromný	k2eAgFnPc7d1	ohromná
finančními	finanční	k2eAgFnPc7d1	finanční
potížemi	potíž	k1gFnPc7	potíž
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
natáčelo	natáčet	k5eAaImAgNnS	natáčet
velkofilm	velkofilm	k1gInSc4	velkofilm
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hvězda	hvězda	k1gFnSc1	hvězda
Liz	liz	k1gInSc4	liz
Taylor	Taylor	k1gInSc4	Taylor
měla	mít	k5eAaImAgFnS	mít
vysoké	vysoký	k2eAgInPc4d1	vysoký
finanční	finanční	k2eAgInPc4d1	finanční
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Hollywoodská	hollywoodský	k2eAgNnPc4d1	hollywoodské
studia	studio	k1gNnPc4	studio
jako	jako	k8xC	jako
komplex	komplex	k1gInSc4	komplex
zažívala	zažívat	k5eAaImAgFnS	zažívat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
úpadek	úpadek	k1gInSc1	úpadek
způsobený	způsobený	k2eAgInSc1d1	způsobený
nástupem	nástup	k1gInSc7	nástup
televizí	televize	k1gFnSc7	televize
do	do	k7c2	do
domácností	domácnost	k1gFnPc2	domácnost
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
naplnit	naplnit	k5eAaPmF	naplnit
tak	tak	k8xC	tak
pokladny	pokladna	k1gFnSc2	pokladna
kin	kino	k1gNnPc2	kino
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
studia	studio	k1gNnSc2	studio
jako	jako	k9	jako
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
mohla	moct	k5eAaImAgFnS	moct
jen	jen	k6eAd1	jen
těžko	těžko	k6eAd1	těžko
konkurovat	konkurovat	k5eAaImF	konkurovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
bortícím	bortící	k2eAgMnSc6d1	bortící
se	s	k7c7	s
studiovým	studiový	k2eAgInSc7d1	studiový
systémem	systém	k1gInSc7	systém
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
herce	herec	k1gMnSc4	herec
často	často	k6eAd1	často
zavazovaly	zavazovat	k5eAaImAgFnP	zavazovat
k	k	k7c3	k
dlouholetým	dlouholetý	k2eAgInPc3d1	dlouholetý
závazkům	závazek	k1gInPc3	závazek
ke	k	k7c3	k
studiím	studio	k1gNnPc3	studio
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
museli	muset	k5eAaImAgMnP	muset
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
v	v	k7c6	v
podřadných	podřadný	k2eAgInPc6d1	podřadný
filmech	film	k1gInPc6	film
za	za	k7c4	za
nízké	nízký	k2eAgInPc4d1	nízký
peníze	peníz	k1gInPc4	peníz
bez	bez	k7c2	bez
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
procenta	procento	k1gNnPc4	procento
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c4	na
zhroucení	zhroucení	k1gNnSc4	zhroucení
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
měly	mít	k5eAaImAgFnP	mít
podíl	podíl	k1gInSc4	podíl
nejen	nejen	k6eAd1	nejen
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Liz	liz	k1gInSc4	liz
Taylor	Taylora	k1gFnPc2	Taylora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
spory	spor	k1gInPc4	spor
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
Foxu	fox	k1gInSc2	fox
ovšem	ovšem	k9	ovšem
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
nezahálela	zahálet	k5eNaImAgFnS	zahálet
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
musí	muset	k5eAaImIp3nS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
jejím	její	k3xOp3gInSc7	její
posledním	poslední	k2eAgInSc7d1	poslední
filmem	film	k1gInSc7	film
pro	pro	k7c4	pro
Fox	fox	k1gInSc4	fox
a	a	k8xC	a
smlouvu	smlouva	k1gFnSc4	smlouva
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
dodržet	dodržet	k5eAaPmF	dodržet
<g/>
,	,	kIx,	,
těšila	těšit	k5eAaImAgFnS	těšit
se	se	k3xPyFc4	se
ale	ale	k9	ale
už	už	k6eAd1	už
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
na	na	k7c6	na
muzikálu	muzikál	k1gInSc6	muzikál
"	"	kIx"	"
<g/>
V	v	k7c6	v
Brooklynu	Brooklyn	k1gInSc6	Brooklyn
roste	růst	k5eAaImIp3nS	růst
strom	strom	k1gInSc1	strom
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
o	o	k7c4	o
které	který	k3yRgFnPc4	který
jednala	jednat	k5eAaImAgFnS	jednat
se	se	k3xPyFc4	se
slavným	slavný	k2eAgInSc7d1	slavný
Gene	gen	k1gInSc5	gen
Kellym	Kellymum	k1gNnPc2	Kellymum
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
měl	mít	k5eAaImAgMnS	mít
mužskou	mužský	k2eAgFnSc4d1	mužská
<g />
.	.	kIx.	.
</s>
<s>
roli	role	k1gFnSc4	role
sehrát	sehrát	k5eAaPmF	sehrát
Frank	Frank	k1gMnSc1	Frank
Sinatra	Sinatrum	k1gNnSc2	Sinatrum
<g/>
)	)	kIx)	)
a	a	k8xC	a
muzikálovou	muzikálový	k2eAgFnSc4d1	muzikálová
komedii	komedie	k1gFnSc4	komedie
"	"	kIx"	"
<g/>
I	i	k9	i
Love	lov	k1gInSc5	lov
Luisa	Luisa	k1gFnSc1	Luisa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
jí	on	k3xPp3gFnSc7	on
měl	mít	k5eAaImAgMnS	mít
sekundovat	sekundovat	k5eAaImF	sekundovat
opět	opět	k6eAd1	opět
Dean	Dean	k1gMnSc1	Dean
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
začít	začít	k5eAaPmF	začít
natáčet	natáčet	k5eAaImF	natáčet
zkraje	zkraje	k7c2	zkraje
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
pak	pak	k6eAd1	pak
skutečně	skutečně	k6eAd1	skutečně
sehráli	sehrát	k5eAaPmAgMnP	sehrát
Dean	Dean	k1gMnSc1	Dean
Martin	Martin	k1gMnSc1	Martin
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Mitchumem	Mitchum	k1gInSc7	Mitchum
a	a	k8xC	a
Paulem	Paul	k1gMnSc7	Paul
Newmanem	Newman	k1gMnSc7	Newman
a	a	k8xC	a
v	v	k7c6	v
roli	role	k1gFnSc6	role
zamýšlené	zamýšlený	k2eAgFnSc6d1	zamýšlená
pro	pro	k7c4	pro
Marilyn	Marilyn	k1gFnSc4	Marilyn
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
Shirley	Shirley	k1gInPc7	Shirley
MacLaine	MacLain	k1gInSc5	MacLain
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
distribuce	distribuce	k1gFnSc2	distribuce
předán	předat	k5eAaPmNgInS	předat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
What	What	k1gMnSc1	What
a	a	k8xC	a
Way	Way	k1gMnSc1	Way
to	ten	k3xDgNnSc4	ten
Go	Go	k1gMnPc1	Go
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počítalo	počítat	k5eAaImAgNnS	počítat
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
životopisného	životopisný	k2eAgInSc2d1	životopisný
filmu	film	k1gInSc2	film
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
idolu	idol	k1gInSc6	idol
Jean	Jean	k1gMnSc1	Jean
Harlow	Harlow	k1gMnSc1	Harlow
"	"	kIx"	"
<g/>
Jean	Jean	k1gMnSc1	Jean
Harlow	Harlow	k1gFnSc2	Harlow
Story	story	k1gFnSc2	story
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1962	[number]	k4	1962
Monroe	Monro	k1gInSc2	Monro
dokonce	dokonce	k9	dokonce
navštívila	navštívit	k5eAaPmAgFnS	navštívit
matku	matka	k1gFnSc4	matka
Jean	Jean	k1gMnSc1	Jean
Harlow	Harlow	k1gFnPc2	Harlow
<g/>
,	,	kIx,	,
přezdívanou	přezdívaný	k2eAgFnSc7d1	přezdívaná
"	"	kIx"	"
<g/>
Mama	mama	k1gFnSc1	mama
Jean	Jean	k1gMnSc1	Jean
<g/>
"	"	kIx"	"
Bell	bell	k1gInSc1	bell
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
projednala	projednat	k5eAaPmAgFnS	projednat
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
možné	možný	k2eAgFnPc4d1	možná
konzultace	konzultace	k1gFnPc4	konzultace
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
životopisného	životopisný	k2eAgInSc2d1	životopisný
filmu	film	k1gInSc2	film
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
dceři	dcera	k1gFnSc6	dcera
<g/>
,	,	kIx,	,
slavné	slavný	k2eAgNnSc4d1	slavné
"	"	kIx"	"
<g/>
Baby	baby	k1gNnSc4	baby
Jean	Jean	k1gMnSc1	Jean
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kasína	kasína	k1gFnSc1	kasína
a	a	k8xC	a
herny	herna	k1gFnPc1	herna
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
ji	on	k3xPp3gFnSc4	on
nabízely	nabízet	k5eAaImAgInP	nabízet
za	za	k7c4	za
sólová	sólový	k2eAgNnPc4d1	sólové
vystupování	vystupování	k1gNnPc4	vystupování
100.000	[number]	k4	100.000
dolarů	dolar	k1gInPc2	dolar
týdně	týdně	k6eAd1	týdně
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
<g/>
co	co	k9	co
MM	mm	kA	mm
dostala	dostat	k5eAaPmAgFnS	dostat
u	u	k7c2	u
Foxu	fox	k1gInSc2	fox
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
počátek	počátek	k1gInSc4	počátek
září	září	k1gNnSc2	září
plánovala	plánovat	k5eAaImAgFnS	plánovat
navštívit	navštívit	k5eAaPmF	navštívit
Washington	Washington	k1gInSc4	Washington
Theater	Theater	k1gInSc1	Theater
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
nové	nový	k2eAgInPc4d1	nový
šaty	šat	k1gInPc4	šat
od	od	k7c2	od
Jeana	Jean	k1gMnSc2	Jean
Louise	Louis	k1gMnSc2	Louis
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
za	za	k7c4	za
6.000	[number]	k4	6.000
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hovořilo	hovořit	k5eAaImAgNnS	hovořit
se	se	k3xPyFc4	se
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
jejího	její	k3xOp3gInSc2	její
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Joem	Joe	k1gNnSc7	Joe
DiMaggiem	DiMaggius	k1gMnSc7	DiMaggius
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
novou	nový	k2eAgFnSc4d1	nová
chuť	chuť	k1gFnSc4	chuť
do	do	k7c2	do
života	život	k1gInSc2	život
a	a	k8xC	a
těšila	těšit	k5eAaImAgFnS	těšit
se	se	k3xPyFc4	se
na	na	k7c4	na
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1962	[number]	k4	1962
byla	být	k5eAaImAgFnS	být
ohlášena	ohlášet	k5eAaImNgFnS	ohlášet
v	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Našli	najít	k5eAaPmAgMnP	najít
se	se	k3xPyFc4	se
tací	tací	k?	tací
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
její	její	k3xOp3gInSc4	její
konec	konec	k1gInSc4	konec
předvídali	předvídat	k5eAaImAgMnP	předvídat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
tenké	tenký	k2eAgFnSc6d1	tenká
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
životem	život	k1gInSc7	život
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
teorii	teorie	k1gFnSc3	teorie
sebevraždy	sebevražda	k1gFnSc2	sebevražda
podporovali	podporovat	k5eAaImAgMnP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
ale	ale	k8xC	ale
i	i	k9	i
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
věřili	věřit	k5eAaImAgMnP	věřit
a	a	k8xC	a
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marilyn	Marilyn	k1gFnSc4	Marilyn
si	se	k3xPyFc3	se
na	na	k7c4	na
život	život	k1gInSc4	život
nesáhla	sáhnout	k5eNaPmAgFnS	sáhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyla	být	k5eNaImAgFnS	být
sebevražedný	sebevražedný	k2eAgInSc4d1	sebevražedný
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
vraždě	vražda	k1gFnSc6	vražda
tajnými	tajný	k2eAgFnPc7d1	tajná
službami	služba	k1gFnPc7	služba
či	či	k8xC	či
bratry	bratr	k1gMnPc4	bratr
Kennedyovými	Kennedyová	k1gFnPc7	Kennedyová
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
oficiálně	oficiálně	k6eAd1	oficiálně
nepotvrdily	potvrdit	k5eNaPmAgFnP	potvrdit
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
mnoho	mnoho	k4c1	mnoho
životopisců	životopisec	k1gMnPc2	životopisec
(	(	kIx(	(
<g/>
Norman	Norman	k1gMnSc1	Norman
Mailer	Mailer	k1gMnSc1	Mailer
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
F.	F.	kA	F.
Slatzer	Slatzer	k1gMnSc1	Slatzer
<g/>
,	,	kIx,	,
Mathew	Mathew	k1gMnSc1	Mathew
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnPc1	Anthona
Summers	Summers	k1gInSc1	Summers
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
Wolfe	Wolf	k1gMnSc5	Wolf
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
Spoto	Spot	k2eAgNnSc1d1	Spot
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
oficiální	oficiální	k2eAgFnSc7d1	oficiální
verzí	verze	k1gFnSc7	verze
<g/>
,	,	kIx,	,
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
nesrovnalosti	nesrovnalost	k1gFnPc4	nesrovnalost
ve	v	k7c6	v
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
a	a	k8xC	a
na	na	k7c6	na
množství	množství	k1gNnSc6	množství
chloralhydrátu	chloralhydrát	k1gInSc2	chloralhydrát
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
Nembutalu	Nembutal	k1gMnSc3	Nembutal
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
pitevní	pitevní	k2eAgFnSc1d1	pitevní
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
sehrál	sehrát	k5eAaPmAgInS	sehrát
nejen	nejen	k6eAd1	nejen
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Greenson	Greenson	k1gMnSc1	Greenson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nikdy	nikdy	k6eAd1	nikdy
neřekl	říct	k5eNaPmAgMnS	říct
celou	celý	k2eAgFnSc4d1	celá
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
její	její	k3xOp3gFnSc2	její
hospodyně	hospodyně	k1gFnSc2	hospodyně
Eunice	Eunic	k1gMnSc2	Eunic
Murray	Murraa	k1gMnSc2	Murraa
a	a	k8xC	a
i	i	k9	i
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
MM	mm	kA	mm
dr	dr	kA	dr
<g/>
.	.	kIx.	.
H.	H.	kA	H.
Engelberg	Engelberg	k1gInSc1	Engelberg
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nikdy	nikdy	k6eAd1	nikdy
nemuseli	muset	k5eNaImAgMnP	muset
vypovídat	vypovídat	k5eAaImF	vypovídat
pod	pod	k7c7	pod
přísahou	přísaha	k1gFnSc7	přísaha
<g/>
,	,	kIx,	,
měnili	měnit	k5eAaImAgMnP	měnit
výpovědi	výpověď	k1gFnPc4	výpověď
a	a	k8xC	a
podávali	podávat	k5eAaImAgMnP	podávat
odlišná	odlišný	k2eAgNnPc4d1	odlišné
svědectví	svědectví	k1gNnPc4	svědectví
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ralph	Ralph	k1gMnSc1	Ralph
Greenson	Greenson	k1gMnSc1	Greenson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
policii	policie	k1gFnSc4	policie
jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
MM	mm	kA	mm
spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
skonu	skon	k1gInSc6	skon
v	v	k7c6	v
privátním	privátní	k2eAgInSc6d1	privátní
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
asistentem	asistent	k1gMnSc7	asistent
koronera	koroner	k1gMnSc2	koroner
J.	J.	kA	J.
Minerem	Minero	k1gNnSc7	Minero
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
zcela	zcela	k6eAd1	zcela
změnil	změnit	k5eAaPmAgMnS	změnit
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
MM	mm	kA	mm
si	se	k3xPyFc3	se
život	život	k1gInSc4	život
nevzala	vzít	k5eNaPmAgFnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Donald	Donald	k1gMnSc1	Donald
Spoto	Spot	k2eAgNnSc4d1	Spot
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
Greensona	Greensona	k1gFnSc1	Greensona
dokonce	dokonce	k9	dokonce
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
viníka	viník	k1gMnSc4	viník
její	její	k3xOp3gFnSc2	její
předčasné	předčasný	k2eAgFnSc2d1	předčasná
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vlivem	vliv	k1gInSc7	vliv
špatné	špatný	k2eAgFnSc2d1	špatná
koordinace	koordinace	k1gFnSc2	koordinace
lékařských	lékařský	k2eAgInPc2d1	lékařský
předpisů	předpis	k1gInPc2	předpis
jeho	on	k3xPp3gInSc2	on
a	a	k8xC	a
Engelberga	Engelberg	k1gMnSc2	Engelberg
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
předepsal	předepsat	k5eAaPmAgMnS	předepsat
Nembutal	Nembutal	k1gFnSc4	Nembutal
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
Greensona	Greenson	k1gMnSc2	Greenson
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
snažil	snažit	k5eAaImAgMnS	snažit
zbavit	zbavit	k5eAaPmF	zbavit
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c4	na
Nembutalu	Nembutal	k1gMnSc3	Nembutal
přechodem	přechod	k1gInSc7	přechod
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
látku	látka	k1gFnSc4	látka
–	–	k?	–
chloralhydrát	chloralhydrát	k1gInSc1	chloralhydrát
<g/>
)	)	kIx)	)
a	a	k8xC	a
fatálnímu	fatální	k2eAgNnSc3d1	fatální
podání	podání	k1gNnSc3	podání
chloralhydrátu	chloralhydrát	k1gInSc2	chloralhydrát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
Nembutalem	Nembutal	k1gMnSc7	Nembutal
vydaným	vydaný	k2eAgMnSc7d1	vydaný
na	na	k7c4	na
předpis	předpis	k1gInSc4	předpis
Engelberga	Engelberg	k1gMnSc2	Engelberg
měl	mít	k5eAaImAgMnS	mít
způsobit	způsobit	k5eAaPmF	způsobit
smrt	smrt	k1gFnSc4	smrt
Marilyn	Marilyn	k1gFnSc2	Marilyn
Monroe	Monro	k1gFnSc2	Monro
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
Mathew	Mathew	k1gMnSc1	Mathew
Smith	Smith	k1gMnSc1	Smith
její	její	k3xOp3gFnSc4	její
smrt	smrt	k1gFnSc4	smrt
předkládá	předkládat	k5eAaImIp3nS	předkládat
jako	jako	k9	jako
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
stála	stát	k5eAaImAgFnS	stát
odloučená	odloučený	k2eAgFnSc1d1	odloučená
frakce	frakce	k1gFnSc1	frakce
CIA	CIA	kA	CIA
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zkompromitovat	zkompromitovat	k5eAaPmF	zkompromitovat
rodinu	rodina	k1gFnSc4	rodina
Kennedyů	Kennedy	k1gInPc2	Kennedy
a	a	k8xC	a
zbavit	zbavit	k5eAaPmF	zbavit
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jejich	jejich	k3xOp3gInSc2	jejich
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
obchodního	obchodní	k2eAgInSc2d1	obchodní
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
podezřele	podezřele	k6eAd1	podezřele
rychlé	rychlý	k2eAgInPc4d1	rychlý
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1962	[number]	k4	1962
učinil	učinit	k5eAaImAgMnS	učinit
koroner	koroner	k1gMnSc1	koroner
(	(	kIx(	(
<g/>
soudní	soudní	k2eAgMnSc1d1	soudní
lékař	lékař	k1gMnSc1	lékař
<g/>
)	)	kIx)	)
okresu	okres	k1gInSc2	okres
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Theodore	Theodor	k1gMnSc5	Theodor
Curphey	Curphea	k1gMnSc2	Curphea
závěrečné	závěrečný	k2eAgNnSc1d1	závěrečné
ohlášení	ohlášení	k1gNnSc1	ohlášení
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
smrt	smrt	k1gFnSc1	smrt
nastala	nastat	k5eAaPmAgFnS	nastat
akutní	akutní	k2eAgFnSc7d1	akutní
otravou	otrava	k1gFnSc7	otrava
barbituráty	barbiturát	k1gInPc4	barbiturát
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohřeb	pohřeb	k1gInSc4	pohřeb
a	a	k8xC	a
rakev	rakev	k1gFnSc4	rakev
obstaral	obstarat	k5eAaPmAgMnS	obstarat
opět	opět	k6eAd1	opět
Joe	Joe	k1gMnSc1	Joe
DiMaggio	DiMaggio	k1gMnSc1	DiMaggio
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
před	před	k7c7	před
pohřbem	pohřeb	k1gInSc7	pohřeb
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
nepozval	pozvat	k5eNaPmAgMnS	pozvat
ani	ani	k8xC	ani
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
proplakal	proplakat	k5eAaPmAgMnS	proplakat
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
u	u	k7c2	u
její	její	k3xOp3gFnSc2	její
rakve	rakev	k1gFnSc2	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Zařídil	zařídit	k5eAaPmAgMnS	zařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
i	i	k9	i
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
vypadala	vypadat	k5eAaImAgFnS	vypadat
krásně	krásně	k6eAd1	krásně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
<g/>
,	,	kIx,	,
a	a	k8xC	a
zavolal	zavolat	k5eAaPmAgMnS	zavolat
jejího	její	k3xOp3gMnSc4	její
maskéra	maskér	k1gMnSc4	maskér
a	a	k8xC	a
přítele	přítel	k1gMnSc4	přítel
Whiteyho	Whitey	k1gMnSc4	Whitey
Snydera	Snyder	k1gMnSc4	Snyder
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
upravil	upravit	k5eAaPmAgMnS	upravit
obličej	obličej	k1gInSc4	obličej
poničený	poničený	k2eAgInSc4d1	poničený
prací	práce	k1gFnSc7	práce
patologa	patolog	k1gMnSc2	patolog
<g/>
.	.	kIx.	.
</s>
<s>
Oblečena	oblečen	k2eAgFnSc1d1	oblečena
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
zelenkavých	zelenkavý	k2eAgInPc2d1	zelenkavý
šatů	šat	k1gInPc2	šat
od	od	k7c2	od
Pucciho	Pucci	k1gMnSc2	Pucci
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
šampaňského	šampaňské	k1gNnSc2	šampaňské
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
poslední	poslední	k2eAgInPc4d1	poslední
týdny	týden	k1gInPc4	týden
ráda	rád	k2eAgFnSc1d1	ráda
nosila	nosit	k5eAaImAgNnP	nosit
<g/>
.	.	kIx.	.
20	[number]	k4	20
let	léto	k1gNnPc2	léto
pak	pak	k6eAd1	pak
Joe	Joe	k1gFnSc2	Joe
nechával	nechávat	k5eAaImAgMnS	nechávat
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
hrob	hrob	k1gInSc4	hrob
ve	v	k7c4	v
Westwood	Westwood	k1gInSc4	Westwood
Village	Villag	k1gInSc2	Villag
Memorial	Memorial	k1gInSc1	Memorial
Park	park	k1gInSc1	park
posílat	posílat	k5eAaImF	posílat
3	[number]	k4	3
<g/>
×	×	k?	×
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
rudé	rudý	k2eAgFnSc2d1	rudá
růže	růž	k1gFnSc2	růž
(	(	kIx(	(
<g/>
ty	ty	k3xPp2nSc1	ty
objednával	objednávat	k5eAaImAgMnS	objednávat
v	v	k7c4	v
Parisian	Parisian	k1gInSc4	Parisian
Florist	Florist	k1gInSc4	Florist
na	na	k7c4	na
Sunset	Sunset	k1gInSc4	Sunset
Bvld	Bvld	k1gInSc4	Bvld
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neoženil	oženit	k5eNaPmAgInS	oženit
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
–	–	k?	–
ať	ať	k8xC	ať
již	již	k6eAd1	již
skutečných	skutečný	k2eAgMnPc2d1	skutečný
či	či	k8xC	či
domnělých	domnělý	k2eAgMnPc2d1	domnělý
milenců	milenec	k1gMnPc2	milenec
a	a	k8xC	a
manželů	manžel	k1gMnPc2	manžel
Marilyn	Marilyn	k1gFnSc2	Marilyn
–	–	k?	–
nevytahoval	vytahovat	k5eNaImAgMnS	vytahovat
pikantnosti	pikantnost	k1gFnPc4	pikantnost
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
společného	společný	k2eAgInSc2d1	společný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
zástupu	zástup	k1gInSc6	zástup
uznávaných	uznávaný	k2eAgMnPc2d1	uznávaný
baseballistů	baseballista	k1gMnPc2	baseballista
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
na	na	k7c4	na
newyorský	newyorský	k2eAgInSc4d1	newyorský
stadion	stadion	k1gInSc4	stadion
Yankees	Yankees	k1gMnSc1	Yankees
přijel	přijet	k5eAaPmAgMnS	přijet
podat	podat	k5eAaPmF	podat
ruku	ruka	k1gFnSc4	ruka
Robert	Robert	k1gMnSc1	Robert
Kennedy	Kenneda	k1gMnSc2	Kenneda
<g/>
,	,	kIx,	,
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
o	o	k7c4	o
krok	krok	k1gInSc4	krok
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
Kennedymu	Kennedym	k1gInSc3	Kennedym
ruku	ruka	k1gFnSc4	ruka
nepodal	podat	k5eNaPmAgMnS	podat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
umíral	umírat	k5eAaImAgMnS	umírat
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
pronesl	pronést	k5eAaPmAgMnS	pronést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Konečně	konečně	k6eAd1	konečně
zas	zas	k6eAd1	zas
uvidím	uvidět	k5eAaPmIp1nS	uvidět
Marilyn	Marilyn	k1gFnSc1	Marilyn
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
měli	mít	k5eAaImAgMnP	mít
Joe	Joe	k1gFnSc4	Joe
a	a	k8xC	a
Marilyn	Marilyn	k1gFnSc4	Marilyn
odpočívat	odpočívat	k5eAaImF	odpočívat
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
–	–	k?	–
Joe	Joe	k1gFnSc1	Joe
po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
MM	mm	kA	mm
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
2	[number]	k4	2
krypty	krypta	k1gFnPc4	krypta
ve	v	k7c4	v
Westwood	Westwood	k1gInSc4	Westwood
Village	Village	k1gInSc1	Village
Memorial	Memorial	k1gInSc1	Memorial
Park	park	k1gInSc4	park
Cemetery	Cemeter	k1gInPc7	Cemeter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
kryptu	krypta	k1gFnSc4	krypta
prodal	prodat	k5eAaPmAgMnS	prodat
milionáři	milionář	k1gMnSc3	milionář
Richardu	Richard	k1gMnSc3	Richard
Poncherovi	Poncher	k1gMnSc3	Poncher
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
pak	pak	k6eAd1	pak
bývalý	bývalý	k2eAgMnSc1d1	bývalý
manžel	manžel	k1gMnSc1	manžel
Marilyn	Marilyn	k1gFnSc2	Marilyn
Arthur	Arthur	k1gMnSc1	Arthur
Miller	Miller	k1gMnSc1	Miller
napsal	napsat	k5eAaPmAgMnS	napsat
hru	hra	k1gFnSc4	hra
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nesla	nést	k5eAaImAgFnS	nést
výrazné	výrazný	k2eAgInPc4d1	výrazný
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
líčila	líčit	k5eAaImAgFnS	líčit
jeho	jeho	k3xOp3gNnSc2	jeho
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Marilyn	Marilyn	k1gFnSc7	Marilyn
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
i	i	k8xC	i
diváci	divák	k1gMnPc1	divák
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
<g/>
,	,	kIx,	,
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
zradu	zrada	k1gFnSc4	zrada
na	na	k7c4	na
Marilyn	Marilyn	k1gFnSc4	Marilyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
let	let	k1gInSc4	let
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
skonu	skon	k1gInSc6	skon
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
asistent	asistent	k1gMnSc1	asistent
koronera	koroner	k1gMnSc2	koroner
John	John	k1gMnSc1	John
Miner	Miner	k1gMnSc1	Miner
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
přítele	přítel	k1gMnSc2	přítel
MM	mm	kA	mm
a	a	k8xC	a
novináře	novinář	k1gMnSc2	novinář
Roberta	Robert	k1gMnSc2	Robert
Slatzera	Slatzer	k1gMnSc2	Slatzer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
znovuotevřít	znovuotevřít	k5eAaPmF	znovuotevřít
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
nesrovnalostí	nesrovnalost	k1gFnPc2	nesrovnalost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
likvidace	likvidace	k1gFnSc1	likvidace
vzorků	vzorek	k1gInPc2	vzorek
a	a	k8xC	a
stěrů	stěr	k1gInPc2	stěr
pořízených	pořízený	k2eAgInPc2d1	pořízený
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
nedokončená	dokončený	k2eNgNnPc4d1	nedokončené
toxikologická	toxikologický	k2eAgNnPc4d1	Toxikologické
zkoumání	zkoumání	k1gNnSc4	zkoumání
<g/>
,	,	kIx,	,
záhadné	záhadný	k2eAgNnSc1d1	záhadné
zmizení	zmizení	k1gNnSc1	zmizení
Minerovy	Minerův	k2eAgFnSc2d1	Minerův
zprávy	zpráva	k1gFnSc2	zpráva
vyšetřovatelům	vyšetřovatel	k1gMnPc3	vyšetřovatel
atd.	atd.	kA	atd.
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
státu	stát	k1gInSc2	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
ovšem	ovšem	k9	ovšem
nové	nový	k2eAgNnSc4d1	nové
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
předčasně	předčasně	k6eAd1	předčasně
ukončil	ukončit	k5eAaPmAgMnS	ukončit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc4	žádný
důvěryhodné	důvěryhodný	k2eAgInPc4d1	důvěryhodný
důkazy	důkaz	k1gInPc4	důkaz
podporující	podporující	k2eAgFnSc4d1	podporující
teorii	teorie	k1gFnSc4	teorie
vraždy	vražda	k1gFnSc2	vražda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Miner	Miner	k1gMnSc1	Miner
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
však	však	k9	však
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
stihl	stihnout	k5eAaPmAgMnS	stihnout
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
přepis	přepis	k1gInSc4	přepis
nahrávek	nahrávka	k1gFnPc2	nahrávka
samotné	samotný	k2eAgFnSc2d1	samotná
Marilyn	Marilyn	k1gFnSc2	Marilyn
jejímu	její	k3xOp3gMnSc3	její
psychoanalytikovi	psychoanalytik	k1gMnSc3	psychoanalytik
Ralphu	Ralph	k1gMnSc3	Ralph
Greensonovi	Greenson	k1gMnSc3	Greenson
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
Times	Times	k1gMnSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
kazetách	kazeta	k1gFnPc6	kazeta
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
již	již	k6eAd1	již
Robert	Robert	k1gMnSc1	Robert
Slatzer	Slatzer	k1gMnSc1	Slatzer
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Případ	případ	k1gInSc1	případ
Marilyn	Marilyn	k1gFnSc7	Marilyn
doopravdy	doopravdy	k6eAd1	doopravdy
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
novináře	novinář	k1gMnSc2	novinář
Roberta	Robert	k1gMnSc2	Robert
Slatzera	Slatzer	k1gMnSc2	Slatzer
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
jako	jako	k8xC	jako
snůška	snůška	k1gFnSc1	snůška
nepodložených	podložený	k2eNgFnPc2d1	nepodložená
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
R.	R.	kA	R.
Slatzer	Slatzra	k1gFnPc2	Slatzra
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
kariéru	kariéra	k1gFnSc4	kariéra
údajně	údajně	k6eAd1	údajně
založit	založit	k5eAaPmF	založit
na	na	k7c6	na
pár	pár	k4xCyI	pár
společných	společný	k2eAgInPc6d1	společný
snímcích	snímek	k1gInPc6	snímek
s	s	k7c7	s
MM	mm	kA	mm
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
student	student	k1gMnSc1	student
naskytl	naskytnout	k5eAaPmAgMnS	naskytnout
u	u	k7c2	u
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
Niagara	Niagara	k1gFnSc1	Niagara
a	a	k8xC	a
Marilyn	Marilyn	k1gFnSc1	Marilyn
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jako	jako	k9	jako
s	s	k7c7	s
fanouškem	fanoušek	k1gMnSc7	fanoušek
vyfotila	vyfotit	k5eAaPmAgFnS	vyfotit
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Slatzer	Slatzer	k1gMnSc1	Slatzer
pak	pak	k6eAd1	pak
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
několika	několik	k4yIc6	několik
televizních	televizní	k2eAgFnPc6d1	televizní
pořadech	pořad	k1gInPc6	pořad
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
MM	mm	kA	mm
krátce	krátce	k6eAd1	krátce
ženatý	ženatý	k2eAgMnSc1d1	ženatý
před	před	k7c7	před
jejím	její	k3xOp3gInSc7	její
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Joe	Joe	k1gMnSc7	Joe
DiMaggiem	DiMaggius	k1gMnSc7	DiMaggius
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
ale	ale	k8xC	ale
tehdy	tehdy	k6eAd1	tehdy
neznal	neznat	k5eAaImAgMnS	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
část	část	k1gFnSc1	část
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
knihu	kniha	k1gFnSc4	kniha
Oběť	oběť	k1gFnSc4	oběť
použil	použít	k5eAaPmAgMnS	použít
i	i	k9	i
M.	M.	kA	M.
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgFnSc4d1	podporující
taktéž	taktéž	k?	taktéž
teorii	teorie	k1gFnSc4	teorie
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Marilyn	Marilyn	k1gFnSc1	Marilyn
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
s	s	k7c7	s
důvěrou	důvěra	k1gFnSc7	důvěra
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
lékaři	lékař	k1gMnPc1	lékař
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
intimních	intimní	k2eAgInPc6d1	intimní
problémech	problém	k1gInPc6	problém
a	a	k8xC	a
pocitech	pocit	k1gInPc6	pocit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
o	o	k7c6	o
touze	touha	k1gFnSc6	touha
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
charakterní	charakterní	k2eAgFnSc7d1	charakterní
herečkou	herečka	k1gFnSc7	herečka
<g/>
,	,	kIx,	,
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
profesních	profesní	k2eAgInPc6d1	profesní
plánech	plán	k1gInPc6	plán
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
o	o	k7c4	o
lesbické	lesbický	k2eAgFnPc4d1	lesbická
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
Joan	Joan	k1gMnSc1	Joan
Crawford	Crawford	k1gMnSc1	Crawford
a	a	k8xC	a
o	o	k7c6	o
svých	svůj	k3xOyFgMnPc6	svůj
mužích	muž	k1gMnPc6	muž
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
časovému	časový	k2eAgNnSc3d1	časové
rozmezí	rozmezí	k1gNnSc3	rozmezí
mezi	mezi	k7c7	mezi
pořízením	pořízení	k1gNnSc7	pořízení
nahrávek	nahrávka	k1gFnPc2	nahrávka
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
Miner	Miner	k1gMnSc1	Miner
(	(	kIx(	(
<g/>
a	a	k8xC	a
M.	M.	kA	M.
Smith	Smith	k1gMnSc1	Smith
<g/>
)	)	kIx)	)
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marilyn	Marilyn	k1gFnSc4	Marilyn
si	se	k3xPyFc3	se
na	na	k7c4	na
život	život	k1gInSc4	život
sama	sám	k3xTgFnSc1	sám
nesáhla	sáhnout	k5eNaPmAgFnS	sáhnout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
názor	názor	k1gInSc4	názor
jejího	její	k3xOp3gMnSc2	její
psychoanalytika	psychoanalytik	k1gMnSc2	psychoanalytik
Ralpha	Ralph	k1gMnSc2	Ralph
Greensona	Greenson	k1gMnSc2	Greenson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ceny	cena	k1gFnPc1	cena
a	a	k8xC	a
nominace	nominace	k1gFnPc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
natáčela	natáčet	k5eAaImAgFnS	natáčet
film	film	k1gInSc4	film
Muži	muž	k1gMnPc1	muž
mají	mít	k5eAaImIp3nP	mít
raději	rád	k6eAd2	rád
blondýnky	blondýnka	k1gFnPc4	blondýnka
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
jí	jíst	k5eAaImIp3nS	jíst
její	její	k3xOp3gNnSc4	její
maskér	maskér	k1gMnSc1	maskér
Allan	Allan	k1gMnSc1	Allan
"	"	kIx"	"
<g/>
Whitey	Whitea	k1gMnSc2	Whitea
<g/>
"	"	kIx"	"
Snyder	Snyder	k1gInSc4	Snyder
slíbit	slíbit	k5eAaPmF	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
upraví	upravit	k5eAaPmIp3nS	upravit
i	i	k9	i
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Slíbil	slíbit	k5eAaPmAgMnS	slíbit
a	a	k8xC	a
vtipkoval	vtipkovat	k5eAaImAgMnS	vtipkovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
Marilyn	Marilyn	k1gFnSc4	Marilyn
musí	muset	k5eAaImIp3nS	muset
dodat	dodat	k5eAaPmF	dodat
tělo	tělo	k1gNnSc1	tělo
ještě	ještě	k6eAd1	ještě
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
filmu	film	k1gInSc2	film
mu	on	k3xPp3gMnSc3	on
darovala	darovat	k5eAaPmAgFnS	darovat
zlatou	zlatá	k1gFnSc4	zlatá
sponu	spon	k1gInSc2	spon
na	na	k7c4	na
bankovky	bankovka	k1gFnPc4	bankovka
s	s	k7c7	s
věnováním	věnování	k1gNnSc7	věnování
"	"	kIx"	"
<g/>
Whitey	Whitea	k1gFnSc2	Whitea
<g/>
,	,	kIx,	,
miláčku	miláček	k1gMnSc5	miláček
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jsem	být	k5eAaImIp1nS	být
ještě	ještě	k6eAd1	ještě
teplá	teplý	k2eAgFnSc1d1	teplá
<g/>
.	.	kIx.	.
</s>
<s>
Marilyn	Marilyn	k1gFnSc1	Marilyn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Whitey	Whitea	k1gFnPc1	Whitea
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
nezapomněl	zapomnět	k5eNaImAgMnS	zapomnět
–	–	k?	–
splnil	splnit	k5eAaPmAgInS	splnit
jej	on	k3xPp3gNnSc2	on
8	[number]	k4	8
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k6eAd1	rád
horké	horký	k2eAgFnSc6d1	horká
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
nelíbil	líbit	k5eNaImAgInS	líbit
její	její	k3xOp3gInSc4	její
nástup	nástup	k1gInSc4	nástup
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězda	hvězda	k1gFnSc1	hvězda
jejího	její	k3xOp3gInSc2	její
formátu	formát	k1gInSc2	formát
se	se	k3xPyFc4	se
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
nemůže	moct	k5eNaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
jen	jen	k9	jen
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Řekla	říct	k5eAaPmAgFnS	říct
Wilderovi	Wilder	k1gMnSc3	Wilder
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tedy	tedy	k9	tedy
něco	něco	k3yInSc1	něco
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
Wilder	Wilder	k1gInSc1	Wilder
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
vstupní	vstupní	k2eAgFnSc7d1	vstupní
scénou	scéna	k1gFnSc7	scéna
<g/>
:	:	kIx,	:
Marilyn	Marilyn	k1gFnSc1	Marilyn
prochází	procházet	k5eAaImIp3nS	procházet
kolem	kolem	k7c2	kolem
Daphne	Daphne	k1gFnSc2	Daphne
a	a	k8xC	a
Josephiny	Josephina	k1gFnSc2	Josephina
v	v	k7c6	v
přiléhavých	přiléhavý	k2eAgInPc6d1	přiléhavý
černých	černý	k2eAgInPc6d1	černý
šatech	šat	k1gInPc6	šat
<g/>
,	,	kIx,	,
když	když	k8xS	když
vlak	vlak	k1gInSc1	vlak
upustí	upustit	k5eAaPmIp3nS	upustit
páru	pára	k1gFnSc4	pára
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
latinský	latinský	k2eAgInSc1d1	latinský
nápis	nápis	k1gInSc1	nápis
Cursum	Cursum	k1gNnSc1	Cursum
perficio	perficio	k6eAd1	perficio
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
Zde	zde	k6eAd1	zde
končí	končit	k5eAaImIp3nS	končit
má	můj	k3xOp1gFnSc1	můj
pouť	pouť	k1gFnSc1	pouť
či	či	k8xC	či
Zde	zde	k6eAd1	zde
zakončuji	zakončovat	k5eAaImIp1nS	zakončovat
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
Marilyn	Marilyn	k1gFnSc4	Marilyn
navštívil	navštívit	k5eAaPmAgMnS	navštívit
fotograf	fotograf	k1gMnSc1	fotograf
Lawrence	Lawrence	k1gFnSc2	Lawrence
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
domluvili	domluvit	k5eAaPmAgMnP	domluvit
na	na	k7c4	na
podepsání	podepsání	k1gNnSc4	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Playboyem	playboy	k1gMnSc7	playboy
o	o	k7c6	o
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
nahých	nahý	k2eAgFnPc2d1	nahá
fotek	fotka	k1gFnPc2	fotka
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
posledního	poslední	k2eAgInSc2d1	poslední
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
MM	mm	kA	mm
mu	on	k3xPp3gMnSc3	on
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
smlouvu	smlouva	k1gFnSc4	smlouva
nepodepsala	podepsat	k5eNaPmAgFnS	podepsat
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
půl	půl	k1xP	půl
dne	den	k1gInSc2	den
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
jejího	její	k3xOp3gNnSc2	její
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgMnS	najít
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
Schiller	Schiller	k1gMnSc1	Schiller
pod	pod	k7c7	pod
studiovými	studiový	k2eAgFnPc7d1	studiová
dveřmi	dveře	k1gFnPc7	dveře
neoznámkovanou	oznámkovaný	k2eNgFnSc4d1	oznámkovaný
obálku	obálka	k1gFnSc4	obálka
s	s	k7c7	s
danými	daný	k2eAgFnPc7d1	daná
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
"	"	kIx"	"
<g/>
ty	ten	k3xDgInPc1	ten
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
jít	jít	k5eAaImF	jít
do	do	k7c2	do
Playboye	playboy	k1gMnSc2	playboy
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Schiller	Schiller	k1gMnSc1	Schiller
ani	ani	k8xC	ani
Woodfield	Woodfield	k1gMnSc1	Woodfield
nikdy	nikdy	k6eAd1	nikdy
nezjistili	zjistit	k5eNaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
tam	tam	k6eAd1	tam
obálku	obálka	k1gFnSc4	obálka
zavezl	zavézt	k5eAaPmAgInS	zavézt
–	–	k?	–
zda	zda	k8xS	zda
samotná	samotný	k2eAgFnSc1d1	samotná
Marilyn	Marilyn	k1gFnSc1	Marilyn
ještě	ještě	k9	ještě
odpoledne	odpoledne	k6eAd1	odpoledne
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
setkání	setkání	k1gNnSc6	setkání
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c4	v
den	den	k1gInSc4	den
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Pat	pat	k1gInSc1	pat
Newcomb	Newcomba	k1gFnPc2	Newcomba
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
tisková	tiskový	k2eAgFnSc1d1	tisková
mluvčí	mluvčí	k1gFnSc1	mluvčí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
obálku	obálka	k1gFnSc4	obálka
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Monroe	Monro	k1gFnSc2	Monro
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
navštívila	navštívit	k5eAaPmAgFnS	navštívit
v	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
po	po	k7c6	po
Marilynině	Marilynin	k2eAgFnSc6d1	Marilynin
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Údajně	údajně	k6eAd1	údajně
nikdy	nikdy	k6eAd1	nikdy
nepoznala	poznat	k5eNaPmAgFnS	poznat
svého	svůj	k3xOyFgMnSc4	svůj
biologického	biologický	k2eAgMnSc4d1	biologický
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
hradila	hradit	k5eAaImAgFnS	hradit
veškerou	veškerý	k3xTgFnSc4	veškerý
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
péči	péče	k1gFnSc4	péče
a	a	k8xC	a
pobyty	pobyt	k1gInPc4	pobyt
v	v	k7c6	v
ústavech	ústav	k1gInPc6	ústav
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
nenavštěvovala	navštěvovat	k5eNaImAgFnS	navštěvovat
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
hovořila	hovořit	k5eAaImAgFnS	hovořit
jako	jako	k9	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
té	ten	k3xDgFnSc2	ten
paní	paní	k1gFnSc2	paní
s	s	k7c7	s
červenými	červený	k2eAgInPc7d1	červený
vlasy	vlas	k1gInPc7	vlas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Marilyn	Marilyn	k1gFnSc1	Marilyn
Gladys	Gladys	k1gInSc4	Gladys
Baker	Baker	k1gInSc4	Baker
zemřela	zemřít	k5eAaPmAgFnS	zemřít
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1984	[number]	k4	1984
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
84	[number]	k4	84
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
jméno	jméno	k1gNnSc1	jméno
Marilyn	Marilyn	k1gFnSc2	Marilyn
Monroe	Monro	k1gFnSc2	Monro
nebylo	být	k5eNaImAgNnS	být
úplně	úplně	k6eAd1	úplně
vymyšleným	vymyšlený	k2eAgFnPc3d1	vymyšlená
–	–	k?	–
Monroe	Monroe	k1gFnPc3	Monroe
bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
dívčí	dívčí	k2eAgNnSc1d1	dívčí
jméno	jméno	k1gNnSc1	jméno
Marilyniny	Marilynin	k2eAgFnSc2d1	Marilynin
matky	matka	k1gFnSc2	matka
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
městem	město	k1gNnSc7	město
Roxbury	Roxbura	k1gFnSc2	Roxbura
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Connecticut	Connecticut	k1gInSc4	Connecticut
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
náhradní	náhradní	k2eAgFnSc1d1	náhradní
delegátka	delegátka	k1gFnSc1	delegátka
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
celostátní	celostátní	k2eAgInSc4d1	celostátní
předvolební	předvolební	k2eAgInSc4d1	předvolební
sjezd	sjezd	k1gInSc4	sjezd
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gFnSc1	její
hvězda	hvězda	k1gFnSc1	hvězda
prudce	prudko	k6eAd1	prudko
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
novináři	novinář	k1gMnPc1	novinář
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vypadala	vypadat	k5eAaPmAgFnS	vypadat
skvěle	skvěle	k6eAd1	skvěle
i	i	k9	i
v	v	k7c6	v
pytli	pytel	k1gInSc6	pytel
od	od	k7c2	od
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ateliérech	ateliér	k1gInPc6	ateliér
studia	studio	k1gNnSc2	studio
Fox	fox	k1gInSc1	fox
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zrodil	zrodit	k5eAaPmAgInS	zrodit
nápad	nápad	k1gInSc1	nápad
Marilyn	Marilyn	k1gFnSc4	Marilyn
do	do	k7c2	do
pytle	pytel	k1gInSc2	pytel
obléci	obléct	k5eAaPmF	obléct
<g/>
.	.	kIx.	.
</s>
<s>
Billy	Bill	k1gMnPc4	Bill
Travilla	Travill	k1gMnSc2	Travill
pytel	pytel	k1gInSc1	pytel
pouze	pouze	k6eAd1	pouze
upravil	upravit	k5eAaPmAgInS	upravit
k	k	k7c3	k
svůdným	svůdný	k2eAgFnPc3d1	svůdná
křivkám	křivka	k1gFnPc3	křivka
MM	mm	kA	mm
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
pořízeno	pořídit	k5eAaPmNgNnS	pořídit
několik	několik	k4yIc1	několik
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
fotografie	fotografia	k1gFnPc1	fotografia
obletěly	obletět	k5eAaPmAgFnP	obletět
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
poslala	poslat	k5eAaPmAgFnS	poslat
bramborářská	bramborářský	k2eAgFnSc1d1	bramborářská
společnost	společnost	k1gFnSc1	společnost
z	z	k7c2	z
Idaha	Idah	k1gMnSc2	Idah
Marilyn	Marilyn	k1gFnSc2	Marilyn
pár	pár	k4xCyI	pár
pytlů	pytel	k1gInPc2	pytel
prvotřídních	prvotřídní	k2eAgFnPc2d1	prvotřídní
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marilyn	Marilyn	k1gFnSc1	Marilyn
natočila	natočit	k5eAaBmAgFnS	natočit
a	a	k8xC	a
dokončila	dokončit	k5eAaPmAgFnS	dokončit
29	[number]	k4	29
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
Něco	něco	k3yInSc1	něco
musí	muset	k5eAaImIp3nS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
30	[number]	k4	30
<g/>
.	.	kIx.	.
snímkem	snímek	k1gInSc7	snímek
(	(	kIx(	(
<g/>
remake	remake	k1gNnSc1	remake
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
Má	mít	k5eAaImIp3nS	mít
nejoblíbenější	oblíbený	k2eAgFnSc1d3	nejoblíbenější
manželka	manželka	k1gFnSc1	manželka
s	s	k7c7	s
Irene	Iren	k1gInSc5	Iren
Dunne	Dunn	k1gInSc5	Dunn
a	a	k8xC	a
Carym	Carym	k1gInSc1	Carym
Grantem	grant	k1gInSc7	grant
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
<g/>
)	)	kIx)	)
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
torzem	torzo	k1gNnSc7	torzo
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
předělán	předělat	k5eAaPmNgInS	předělat
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
s	s	k7c7	s
Doris	Doris	k1gFnSc7	Doris
Day	Day	k1gFnSc2	Day
až	až	k9	až
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
miss	miss	k1gFnSc2	miss
artyčoků	artyčok	k1gInPc2	artyčok
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
zvolena	zvolit	k5eAaPmNgFnS	zvolit
magazínem	magazín	k1gInSc7	magazín
People	People	k1gFnSc2	People
"	"	kIx"	"
<g/>
Nejvíc	hodně	k6eAd3	hodně
sexy	sex	k1gInPc1	sex
ženou	hnát	k5eAaImIp3nP	hnát
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1997	[number]	k4	1997
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Top	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
Movie	Movie	k1gFnSc2	Movie
Stars	Starsa	k1gFnPc2	Starsa
of	of	k?	of
All	All	k1gMnSc2	All
Time	Tim	k1gMnSc2	Tim
<g/>
"	"	kIx"	"
v	v	k7c6	v
Empire	empir	k1gInSc5	empir
obsadila	obsadit	k5eAaPmAgFnS	obsadit
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
zvolena	zvolit	k5eAaPmNgFnS	zvolit
magazínem	magazín	k1gInSc7	magazín
Empire	empir	k1gInSc5	empir
"	"	kIx"	"
<g/>
Nejvíc	hodně	k6eAd3	hodně
sexy	sex	k1gInPc4	sex
herečkou	herečka	k1gFnSc7	herečka
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
zvolena	zvolit	k5eAaPmNgFnS	zvolit
čtenáři	čtenář	k1gMnPc7	čtenář
časopisu	časopis	k1gInSc2	časopis
Playboy	playboy	k1gMnSc1	playboy
"	"	kIx"	"
<g/>
Nejpřitažlivější	přitažlivý	k2eAgFnSc7d3	nejpřitažlivější
ženou	žena	k1gFnSc7	žena
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
aukční	aukční	k2eAgFnSc6d1	aukční
síni	síň	k1gFnSc6	síň
Christie	Christie	k1gFnSc2	Christie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
vydraženy	vydražit	k5eAaPmNgInP	vydražit
oděvy	oděv	k1gInPc1	oděv
Marilyn	Marilyn	k1gFnPc2	Marilyn
Monroe	Monroe	k1gFnPc2	Monroe
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rekordu	rekord	k1gInSc2	rekord
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
dražba	dražba	k1gFnSc1	dražba
jejích	její	k3xOp3gInPc2	její
šatů	šat	k1gInPc2	šat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgMnPc6	který
zpívala	zpívat	k5eAaImAgFnS	zpívat
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1962	[number]	k4	1962
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardno	k1gNnPc2	Gardno
Johnu	John	k1gMnSc6	John
F.	F.	kA	F.
<g/>
Kennedymu	Kennedym	k1gInSc2	Kennedym
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc7	jeho
45	[number]	k4	45
<g/>
.	.	kIx.	.
<g/>
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Šaty	šat	k1gInPc1	šat
byly	být	k5eAaImAgInP	být
vydraženy	vydražit	k5eAaPmNgInP	vydražit
za	za	k7c4	za
1	[number]	k4	1
267	[number]	k4	267
500	[number]	k4	500
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
kurzu	kurz	k1gInSc2	kurz
43,5	[number]	k4	43,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
<g/>
komisního	komisní	k2eAgInSc2d1	komisní
poplatku	poplatek	k1gInSc2	poplatek
<g/>
,	,	kIx,	,
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nejdražším	drahý	k2eAgInSc7d3	nejdražší
vydraženým	vydražený	k2eAgInSc7d1	vydražený
oděvem	oděv	k1gInSc7	oděv
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
držely	držet	k5eAaImAgFnP	držet
rekord	rekord	k1gInSc4	rekord
šaty	šata	k1gFnSc2	šata
princezny	princezna	k1gFnSc2	princezna
Diany	Diana	k1gFnSc2	Diana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
zvolena	zvolit	k5eAaPmNgFnS	zvolit
institucí	instituce	k1gFnSc7	instituce
TV	TV	kA	TV
Guide	Guid	k1gInSc5	Guid
Network	network	k1gInSc1	network
"	"	kIx"	"
<g/>
Nejvíc	nejvíc	k6eAd1	nejvíc
sexy	sex	k1gInPc4	sex
herečkou	herečka	k1gFnSc7	herečka
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádělo	uvádět	k5eAaImAgNnS	uvádět
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
osudnou	osudný	k2eAgFnSc7d1	osudná
kombinace	kombinace	k1gFnSc1	kombinace
prášků	prášek	k1gInPc2	prášek
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravda	pravda	k9	pravda
–	–	k?	–
patolog	patolog	k1gMnSc1	patolog
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
krevním	krevní	k2eAgInSc6d1	krevní
oběhu	oběh	k1gInSc6	oběh
nenašel	najít	k5eNaPmAgMnS	najít
po	po	k7c6	po
alkoholu	alkohol	k1gInSc6	alkohol
stopu	stopa	k1gFnSc4	stopa
<g/>
,	,	kIx,	,
Marilyn	Marilyn	k1gFnSc4	Marilyn
tedy	tedy	k8xC	tedy
v	v	k7c4	v
den	den	k1gInSc4	den
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
nepožila	požít	k5eNaPmAgFnS	požít
ani	ani	k8xC	ani
kapku	kapka	k1gFnSc4	kapka
svého	svůj	k3xOyFgNnSc2	svůj
oblíbeného	oblíbený	k2eAgNnSc2d1	oblíbené
šampaňského	šampaňské	k1gNnSc2	šampaňské
Dom	Dom	k?	Dom
Perignon	Perignon	k1gMnSc1	Perignon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pitevní	pitevní	k2eAgFnSc2d1	pitevní
zprávy	zpráva	k1gFnSc2	zpráva
ji	on	k3xPp3gFnSc4	on
zabila	zabít	k5eAaPmAgFnS	zabít
kombinace	kombinace	k1gFnSc1	kombinace
nembutalu	nembutal	k1gMnSc3	nembutal
a	a	k8xC	a
chloralhydrátu	chloralhydrát	k1gMnSc3	chloralhydrát
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
Westwood	Westwood	k1gInSc4	Westwood
Village	Village	k1gInSc1	Village
Memorial	Memorial	k1gInSc1	Memorial
Park	park	k1gInSc4	park
Cemetry	Cemetr	k1gInPc7	Cemetr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
charakterizován	charakterizován	k2eAgInSc4d1	charakterizován
jako	jako	k8xS	jako
nejmenší	malý	k2eAgInSc4d3	nejmenší
hřbitov	hřbitov	k1gInSc4	hřbitov
celebrit	celebrita	k1gFnPc2	celebrita
v	v	k7c6	v
LA	la	k1gNnSc6	la
a	a	k8xC	a
kde	kde	k6eAd1	kde
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
<g/>
,	,	kIx,	,
nalezly	nalézt	k5eAaBmAgFnP	nalézt
místo	místo	k7c2	místo
svého	svůj	k3xOyFgInSc2	svůj
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
velké	velký	k2eAgFnPc1d1	velká
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
známé	známý	k2eAgFnPc1d1	známá
osobnosti	osobnost	k1gFnPc1	osobnost
jako	jako	k8xC	jako
herečka	herečka	k1gFnSc1	herečka
Natalie	Natalie	k1gFnSc2	Natalie
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
partner	partner	k1gMnSc1	partner
MM	mm	kA	mm
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
posledního	poslední	k2eAgInSc2d1	poslední
filmu	film	k1gInSc2	film
Dean	Dean	k1gMnSc1	Dean
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
Truman	Truman	k1gMnSc1	Truman
Capote	capot	k1gInSc5	capot
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Billy	Bill	k1gMnPc4	Bill
Wilder	Wilder	k1gInSc4	Wilder
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Marilyn	Marilyn	k1gFnSc4	Marilyn
dvakrát	dvakrát	k6eAd1	dvakrát
režíroval	režírovat	k5eAaImAgMnS	režírovat
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
partner	partner	k1gMnSc1	partner
MM	mm	kA	mm
z	z	k7c2	z
filmu	film	k1gInSc2	film
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k2eAgMnSc1d1	rád
horké	horký	k2eAgNnSc1d1	horké
Jack	Jack	k1gMnSc1	Jack
Lemmon	Lemmon	k1gMnSc1	Lemmon
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
"	"	kIx"	"
<g/>
parťák	parťák	k1gMnSc1	parťák
<g/>
"	"	kIx"	"
Walter	Walter	k1gMnSc1	Walter
Matthau	Matthaus	k1gInSc2	Matthaus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
<g/>
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
pochována	pochovat	k5eAaPmNgFnS	pochovat
také	také	k9	také
herečka	herečka	k1gFnSc1	herečka
Farrah	Farrah	k1gMnSc1	Farrah
Fawcett	Fawcett	k1gMnSc1	Fawcett
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
rakovině	rakovina	k1gFnSc6	rakovina
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc1	její
smrt	smrt	k1gFnSc1	smrt
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
zastřena	zastřít	k5eAaPmNgFnS	zastřít
smrtí	smrt	k1gFnSc7	smrt
Michaela	Michael	k1gMnSc2	Michael
Jacksona	Jackson	k1gMnSc2	Jackson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
magazínu	magazín	k1gInSc2	magazín
Forbes	forbes	k1gInSc1	forbes
se	se	k3xPyFc4	se
Marilyn	Marilyn	k1gFnSc7	Marilyn
Monroe	Monroe	k1gFnSc3	Monroe
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
o	o	k7c4	o
nejvíce	nejvíce	k6eAd1	nejvíce
vydělávající	vydělávající	k2eAgFnPc4d1	vydělávající
zesnulé	zesnulý	k2eAgFnPc4d1	zesnulá
celebrity	celebrita	k1gFnPc4	celebrita
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
17	[number]	k4	17
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Summers	Summers	k1gInSc1	Summers
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc1	Anthon
<g/>
:	:	kIx,	:
Bohyně	bohyně	k1gFnPc1	bohyně
<g/>
,	,	kIx,	,
tajné	tajný	k2eAgInPc1d1	tajný
životy	život	k1gInPc1	život
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gInSc1	Monroe
<g/>
,	,	kIx,	,
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
</s>
</p>
<p>
<s>
Spoto	Spoto	k1gNnSc1	Spoto
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
<g/>
:	:	kIx,	:
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
IKAR	IKAR	kA	IKAR
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85944-55-3	[number]	k4	80-85944-55-3
</s>
</p>
<p>
<s>
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Matthew	Matthew	k1gMnSc1	Matthew
<g/>
:	:	kIx,	:
Oběť	oběť	k1gFnSc1	oběť
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
<g/>
ISBN	ISBN	kA	ISBN
80-7341-500-3	[number]	k4	80-7341-500-3
</s>
</p>
<p>
<s>
Mailer	Mailer	k1gMnSc1	Mailer
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
:	:	kIx,	:
Marilyn	Marilyn	k1gFnSc1	Marilyn
<g/>
,	,	kIx,	,
JOTA	jota	k1gFnSc1	jota
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7217-695-3	[number]	k4	978-80-7217-695-3
</s>
</p>
<p>
<s>
Leaming	Leaming	k1gInSc1	Leaming
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
:	:	kIx,	:
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Shevey	Shevea	k1gFnPc1	Shevea
<g/>
,	,	kIx,	,
Sandra	Sandra	k1gFnSc1	Sandra
<g/>
:	:	kIx,	:
Skandál	skandál	k1gInSc1	skandál
jménem	jméno	k1gNnSc7	jméno
Marilyn	Marilyn	k1gFnSc1	Marilyn
<g/>
,	,	kIx,	,
IRIS	iris	k1gFnSc1	iris
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-901268-1-2	[number]	k4	80-901268-1-2
</s>
</p>
<p>
<s>
Last	Last	k1gMnSc1	Last
Interview	interview	k1gNnSc2	interview
with	with	k1gMnSc1	with
Marilyn	Marilyn	k1gFnSc2	Marilyn
Richard	Richard	k1gMnSc1	Richard
Meryman	Meryman	k1gMnSc1	Meryman
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
1	[number]	k4	1
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Strasberg	Strasberg	k1gMnSc1	Strasberg
<g/>
,	,	kIx,	,
Susan	Susan	k1gMnSc1	Susan
<g/>
:	:	kIx,	:
Marilyn	Marilyn	k1gFnSc1	Marilyn
a	a	k8xC	a
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Erika	Erik	k1gMnSc2	Erik
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85612-43-7	[number]	k4	80-85612-43-7
</s>
</p>
<p>
<s>
Slatzer	Slatzer	k1gMnSc1	Slatzer
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
:	:	kIx,	:
Případ	případ	k1gInSc1	případ
Marilyn	Marilyn	k1gFnSc2	Marilyn
doopravdy	doopravdy	k6eAd1	doopravdy
,	,	kIx,	,
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85803-10-0	[number]	k4	80-85803-10-0
</s>
</p>
<p>
<s>
Crown	Crown	k1gNnSc1	Crown
<g/>
,	,	kIx,	,
Lawrence	Lawrence	k1gFnSc1	Lawrence
<g/>
:	:	kIx,	:
Marilyn	Marilyn	k1gFnSc1	Marilyn
Twentieth	Twentietha	k1gFnPc2	Twentietha
century	centura	k1gFnSc2	centura
Fox	fox	k1gInSc1	fox
<g/>
,	,	kIx,	,
Fortuna	Fortuna	k1gFnSc1	Fortuna
Print	Print	k1gInSc1	Print
1993	[number]	k4	1993
</s>
</p>
<p>
<s>
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
Marilyn	Marilyn	k1gFnSc1	Marilyn
<g/>
:	:	kIx,	:
Tapfer	Tapfer	k1gInSc1	Tapfer
Lieben	Liebna	k1gFnPc2	Liebna
<g/>
,	,	kIx,	,
Stanley	Stanlea	k1gFnPc1	Stanlea
Buchthal	Buchthal	k1gMnSc1	Buchthal
und	und	k?	und
Bernard	Bernard	k1gMnSc1	Bernard
Comment	Comment	k1gMnSc1	Comment
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-3-10-043702-0	[number]	k4	978-3-10-043702-0
</s>
</p>
<p>
<s>
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
Lawrence	Lawrence	k1gFnSc1	Lawrence
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Marilyn	Marilyn	k1gFnSc1	Marilyn
and	and	k?	and
me	me	k?	me
-	-	kIx~	-
A	a	k9	a
photograher	photograhra	k1gFnPc2	photograhra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
memories	memoriesa	k1gFnPc2	memoriesa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Nan	Nan	k1gFnSc1	Nan
A.	A.	kA	A.
<g/>
Talese	Talese	k1gFnSc2	Talese
<g/>
|	|	kIx~	|
<g/>
Doubleday	Doubledaa	k1gFnSc2	Doubledaa
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-0-385-53667-7	[number]	k4	978-0-385-53667-7
</s>
</p>
<p>
<s>
Feeney	Feeney	k1gInPc1	Feeney
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
X.	X.	kA	X.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe-okouzlující	Monroekouzlující	k2eAgFnSc1d1	Monroe-okouzlující
žena	žena	k1gFnSc1	žena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Taschen	Taschen	k1gInSc1	Taschen
<g/>
/	/	kIx~	/
<g/>
Slovart	Slovart	k1gInSc1	Slovart
<g/>
/	/	kIx~	/
<g/>
TMC	TMC	kA	TMC
Art	Art	k1gFnSc1	Art
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7391-422-6	[number]	k4	978-80-7391-422-6
</s>
</p>
<p>
<s>
Guiles	Guiles	k1gMnSc1	Guiles
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Lawrence	Lawrence	k1gFnSc2	Lawrence
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Norma	Norma	k1gFnSc1	Norma
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
life	lif	k1gFnSc2	lif
of	of	k?	of
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Paragon	paragon	k1gInSc4	paragon
House	house	k1gNnSc4	house
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-55778-583-X	[number]	k4	1-55778-583-X
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marilyn	Marilyn	k1gFnPc2	Marilyn
Monroeová	Monroeová	k1gFnSc1	Monroeová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroeová	Monroeový	k2eAgFnSc1d1	Monroeová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroeová	Monroeový	k2eAgFnSc1d1	Monroeová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
TIME	TIME	kA	TIME
</s>
</p>
