<s>
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
psychedelickému	psychedelický	k2eAgInSc3d1	psychedelický
rocku	rock	k1gInSc3	rock
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
žánrově	žánrově	k6eAd1	žánrově
posunula	posunout	k5eAaPmAgFnS	posunout
k	k	k7c3	k
progresivnímu	progresivní	k2eAgInSc3d1	progresivní
rocku	rock	k1gInSc3	rock
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
průkopníkem	průkopník	k1gMnSc7	průkopník
<g/>
.	.	kIx.	.
</s>

