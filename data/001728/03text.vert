<s>
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
psychedelickému	psychedelický	k2eAgInSc3d1	psychedelický
rocku	rock	k1gInSc3	rock
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
žánrově	žánrově	k6eAd1	žánrově
posunula	posunout	k5eAaPmAgFnS	posunout
k	k	k7c3	k
progresivnímu	progresivní	k2eAgInSc3d1	progresivní
rocku	rock	k1gInSc3	rock
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
průkopníkem	průkopník	k1gMnSc7	průkopník
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
jsou	být	k5eAaImIp3nP	být
známí	známý	k1gMnPc1	známý
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
filosofickým	filosofický	k2eAgInPc3d1	filosofický
textům	text	k1gInPc3	text
<g/>
,	,	kIx,	,
klasickým	klasický	k2eAgFnPc3d1	klasická
rockovým	rockový	k2eAgFnPc3d1	rocková
melodiím	melodie	k1gFnPc3	melodie
<g/>
,	,	kIx,	,
zvukovým	zvukový	k2eAgFnPc3d1	zvuková
experimentům	experiment	k1gInPc3	experiment
<g/>
,	,	kIx,	,
inovativním	inovativní	k2eAgInPc3d1	inovativní
obalům	obal	k1gInPc3	obal
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
propracovaným	propracovaný	k2eAgNnSc7d1	propracované
vystoupením	vystoupení	k1gNnSc7	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
<g/>
,	,	kIx,	,
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
a	a	k8xC	a
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
skupin	skupina	k1gFnPc2	skupina
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prodala	prodat	k5eAaPmAgFnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
alb	alba	k1gFnPc2	alba
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přibližně	přibližně	k6eAd1	přibližně
74,5	[number]	k4	74,5
milionů	milion	k4xCgInPc2	milion
jen	jen	k6eAd1	jen
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
progresivně	progresivně	k6eAd1	progresivně
rockové	rockový	k2eAgFnPc4d1	rocková
skupiny	skupina	k1gFnPc4	skupina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Genesis	Genesis	k1gFnSc4	Genesis
a	a	k8xC	a
Yes	Yes	k1gFnSc4	Yes
<g/>
,	,	kIx,	,
či	či	k8xC	či
současné	současný	k2eAgMnPc4d1	současný
interprety	interpret	k1gMnPc4	interpret
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Nine	Nine	k1gInSc1	Nine
Inch	Incha	k1gFnPc2	Incha
Nails	Nails	k1gInSc1	Nails
a	a	k8xC	a
Dream	Dream	k1gInSc1	Dream
Theater	Theatra	k1gFnPc2	Theatra
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Syda	Syda	k1gMnSc1	Syda
Barretta	Barretta	k1gMnSc1	Barretta
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
úspěchu	úspěch	k1gInSc2	úspěch
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
undergroundových	undergroundový	k2eAgFnPc2d1	undergroundová
kapel	kapela	k1gFnPc2	kapela
hrající	hrající	k2eAgInSc1d1	hrající
psychedelický	psychedelický	k2eAgInSc1d1	psychedelický
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Barrettovovo	Barrettovův	k2eAgNnSc1d1	Barrettovův
nevypočitatelné	vypočitatelný	k2eNgNnSc1d1	nevypočitatelné
chování	chování	k1gNnSc1	chování
ale	ale	k8xC	ale
donutilo	donutit	k5eAaPmAgNnS	donutit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
spoluhráče	spoluhráč	k1gMnPc4	spoluhráč
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
doplňovali	doplňovat	k5eAaImAgMnP	doplňovat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
úplně	úplně	k6eAd1	úplně
nahradili	nahradit	k5eAaPmAgMnP	nahradit
kytaristou	kytarista	k1gMnSc7	kytarista
a	a	k8xC	a
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Davidem	David	k1gMnSc7	David
Gilmourem	Gilmour	k1gMnSc7	Gilmour
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Barretově	Barretův	k2eAgInSc6d1	Barretův
odchodu	odchod	k1gInSc6	odchod
se	se	k3xPyFc4	se
lídrem	lídr	k1gMnSc7	lídr
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
skladatelem	skladatel	k1gMnSc7	skladatel
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgMnS	stát
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
Roger	Rogra	k1gFnPc2	Rogra
Waters	Watersa	k1gFnPc2	Watersa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
proměna	proměna	k1gFnSc1	proměna
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
několika	několik	k4yIc2	několik
novátorských	novátorský	k2eAgNnPc2d1	novátorské
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
kapele	kapela	k1gFnSc3	kapela
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
věhlas	věhlas	k1gInSc1	věhlas
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
koncepční	koncepční	k2eAgFnPc4d1	koncepční
alba	album	k1gNnPc4	album
The	The	k1gMnSc2	The
Dark	Darko	k1gNnPc2	Darko
Side	Side	k1gNnPc2	Side
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wish	Wish	k1gMnSc1	Wish
You	You	k1gFnSc2	You
Were	Were	k1gFnSc1	Were
Here	Here	k1gFnSc1	Here
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
a	a	k8xC	a
Animals	Animals	k1gInSc1	Animals
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
a	a	k8xC	a
rockovou	rockový	k2eAgFnSc4d1	rocková
operu	opera	k1gFnSc4	opera
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
Waters	Waters	k1gInSc1	Waters
rozpad	rozpad	k1gInSc4	rozpad
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
vedeni	vést	k5eAaImNgMnP	vést
Gilmourem	Gilmour	k1gInSc7	Gilmour
v	v	k7c6	v
koncertování	koncertování	k1gNnSc6	koncertování
a	a	k8xC	a
nahrávání	nahrávání	k1gNnSc4	nahrávání
desek	deska	k1gFnPc2	deska
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
poté	poté	k6eAd1	poté
vydali	vydat	k5eAaPmAgMnP	vydat
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
studiové	studiový	k2eAgFnPc4d1	studiová
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
obrovského	obrovský	k2eAgInSc2d1	obrovský
komerčního	komerční	k2eAgInSc2d1	komerční
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
činnost	činnost	k1gFnSc1	činnost
ukončili	ukončit	k5eAaPmAgMnP	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
charitativním	charitativní	k2eAgInSc6d1	charitativní
koncertu	koncert	k1gInSc6	koncert
Live	Liv	k1gInSc2	Liv
8	[number]	k4	8
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
jednorázové	jednorázový	k2eAgNnSc1d1	jednorázové
vystoupení	vystoupení	k1gNnSc1	vystoupení
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
24	[number]	k4	24
letech	léto	k1gNnPc6	léto
i	i	k8xC	i
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Watersem	Waters	k1gMnSc7	Waters
<g/>
.	.	kIx.	.
</s>
<s>
Syd	Syd	k?	Syd
Barrett	Barrett	k1gMnSc1	Barrett
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
klávesista	klávesista	k1gMnSc1	klávesista
Richard	Richard	k1gMnSc1	Richard
Wright	Wright	k1gMnSc1	Wright
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvacetileté	dvacetiletý	k2eAgFnSc6d1	dvacetiletá
odmlce	odmlka	k1gFnSc6	odmlka
od	od	k7c2	od
desky	deska	k1gFnSc2	deska
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
vydali	vydat	k5eAaPmAgMnP	vydat
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
nové	nový	k2eAgFnPc1d1	nová
studiové	studiový	k2eAgFnPc1d1	studiová
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Endless	Endlessa	k1gFnPc2	Endlessa
River	River	k1gMnSc1	River
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
utvořila	utvořit	k5eAaPmAgFnS	utvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
a	a	k8xC	a
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
více	hodně	k6eAd2	hodně
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
amatérská	amatérský	k2eAgFnSc1d1	amatérská
skupina	skupina	k1gFnSc1	skupina
postupně	postupně	k6eAd1	postupně
nesla	nést	k5eAaImAgFnS	nést
několik	několik	k4yIc4	několik
názvů	název	k1gInPc2	název
<g/>
:	:	kIx,	:
Sigma	sigma	k1gNnSc1	sigma
6	[number]	k4	6
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Abdabs	Abdabs	k1gInSc1	Abdabs
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Screaming	Screaming	k1gInSc1	Screaming
Abdabs	Abdabs	k1gInSc1	Abdabs
a	a	k8xC	a
Spectrum	Spectrum	k1gNnSc1	Spectrum
Five	Fiv	k1gInSc2	Fiv
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
The	The	k1gFnSc1	The
Tea	Tea	k1gFnSc1	Tea
Set	set	k1gInSc4	set
začali	začít	k5eAaPmAgMnP	začít
kytarista	kytarista	k1gMnSc1	kytarista
Roger	Roger	k1gMnSc1	Roger
Waters	Waters	k1gInSc1	Waters
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
a	a	k8xC	a
pianista	pianista	k1gMnSc1	pianista
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Bobem	Bob	k1gMnSc7	Bob
Klosem	Klos	k1gInSc7	Klos
(	(	kIx(	(
<g/>
Waters	Waters	k1gInSc1	Waters
zároveň	zároveň	k6eAd1	zároveň
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
baskytaře	baskytara	k1gFnSc3	baskytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pokusu	pokus	k1gInSc6	pokus
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Chrisem	Chris	k1gInSc7	Chris
Dennisem	Dennis	k1gInSc7	Dennis
se	se	k3xPyFc4	se
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
přidal	přidat	k5eAaPmAgMnS	přidat
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Syd	Syd	k1gMnSc1	Syd
Barrett	Barrett	k1gMnSc1	Barrett
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
The	The	k1gFnSc1	The
Tea	Tea	k1gFnSc1	Tea
Set	set	k1gInSc4	set
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapela	kapela	k1gFnSc1	kapela
stejného	stejný	k2eAgInSc2d1	stejný
názvu	název	k1gInSc2	název
už	už	k6eAd1	už
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
Barrett	Barrett	k1gInSc1	Barrett
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
používat	používat	k5eAaImF	používat
název	název	k1gInSc1	název
The	The	k1gFnPc2	The
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
Sound	Sound	k1gMnSc1	Sound
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
ale	ale	k8xC	ale
návrh	návrh	k1gInSc1	návrh
zněl	znět	k5eAaImAgInS	znět
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
Blues	blues	k1gNnSc2	blues
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
jmenovaly	jmenovat	k5eAaBmAgInP	jmenovat
Pink	pink	k6eAd1	pink
a	a	k8xC	a
Floyd	Floyd	k1gInSc4	Floyd
dle	dle	k7c2	dle
dvou	dva	k4xCgInPc2	dva
amerických	americký	k2eAgMnPc2d1	americký
bluesových	bluesový	k2eAgMnPc2d1	bluesový
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
Pinka	pinka	k1gFnSc1	pinka
Andersona	Anderson	k1gMnSc2	Anderson
a	a	k8xC	a
Floyda	Floyd	k1gMnSc2	Floyd
Councila	Council	k1gMnSc2	Council
<g/>
.	.	kIx.	.
</s>
<s>
Nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
používali	používat	k5eAaImAgMnP	používat
obě	dva	k4xCgNnPc4	dva
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
než	než	k8xS	než
definitivně	definitivně	k6eAd1	definitivně
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
druhý	druhý	k4xOgInSc1	druhý
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Sound	Sound	k1gInSc1	Sound
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
názvu	název	k1gInSc2	název
záhy	záhy	k6eAd1	záhy
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
"	"	kIx"	"
<g/>
The	The	k1gMnPc2	The
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
příležitostně	příležitostně	k6eAd1	příležitostně
používán	používat	k5eAaImNgInS	používat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgMnSc1d2	pozdější
kytarista	kytarista	k1gMnSc1	kytarista
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
nazýval	nazývat	k5eAaImAgMnS	nazývat
skupinu	skupina	k1gFnSc4	skupina
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
"	"	kIx"	"
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
hrála	hrát	k5eAaImAgFnS	hrát
kapela	kapela	k1gFnSc1	kapela
coververze	coververze	k1gFnSc2	coververze
známých	známý	k2eAgNnPc2d1	známé
rhythm	rhythmo	k1gNnPc2	rhythmo
and	and	k?	and
bluesových	bluesový	k2eAgFnPc2d1	bluesová
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Louie	Louie	k1gFnSc1	Louie
<g/>
,	,	kIx,	,
Louie	Louie	k1gFnSc1	Louie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vánočních	vánoční	k2eAgFnPc2d1	vánoční
prázdnin	prázdniny	k1gFnPc2	prázdniny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
natočila	natočit	k5eAaBmAgFnS	natočit
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
první	první	k4xOgFnSc2	první
demo	demo	k2eAgFnSc2d1	demo
snímky	snímka	k1gFnSc2	snímka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
1965	[number]	k4	1965
<g/>
:	:	kIx,	:
Their	Their	k1gInSc1	Their
First	First	k1gMnSc1	First
Recordings	Recordings	k1gInSc1	Recordings
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
Klose	Klose	k1gFnSc2	Klose
opustil	opustit	k5eAaPmAgMnS	opustit
kapelu	kapela	k1gFnSc4	kapela
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
učitelů	učitel	k1gMnPc2	učitel
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1965	[number]	k4	1965
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
se	se	k3xPyFc4	se
sestava	sestava	k1gFnSc1	sestava
ustálila	ustálit	k5eAaPmAgFnS	ustálit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
<g/>
:	:	kIx,	:
Syd	Syd	k1gFnSc1	Syd
Barrett	Barrett	k1gMnSc1	Barrett
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
Roger	Rogra	k1gFnPc2	Rogra
Waters	Watersa	k1gFnPc2	Watersa
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
a	a	k8xC	a
vokály	vokál	k1gInPc1	vokál
Rick	Ricka	k1gFnPc2	Ricka
Wright	Wrighta	k1gFnPc2	Wrighta
-	-	kIx~	-
klávesy	klávesa	k1gFnPc4	klávesa
a	a	k8xC	a
vokály	vokál	k1gInPc4	vokál
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
-	-	kIx~	-
bicí	bicí	k2eAgInSc1d1	bicí
Barrett	Barrett	k1gInSc1	Barrett
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
vlastní	vlastní	k2eAgFnPc4d1	vlastní
písně	píseň	k1gFnPc4	píseň
ovlivněné	ovlivněný	k2eAgFnPc4d1	ovlivněná
americkým	americký	k2eAgInSc7d1	americký
a	a	k8xC	a
britským	britský	k2eAgInSc7d1	britský
psychedelickým	psychedelický	k2eAgInSc7d1	psychedelický
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgMnPc3	který
přidal	přidat	k5eAaPmAgInS	přidat
vlastní	vlastní	k2eAgMnSc1d1	vlastní
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
bizarní	bizarní	k2eAgInSc4d1	bizarní
humor	humor	k1gInSc4	humor
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stali	stát	k5eAaPmAgMnP	stát
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
undergroundovou	undergroundový	k2eAgFnSc7d1	undergroundová
kapelou	kapela	k1gFnSc7	kapela
a	a	k8xC	a
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
klubech	klub	k1gInPc6	klub
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgInP	být
UFO	UFO	kA	UFO
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
Marquee	Marque	k1gInPc1	Marque
Club	club	k1gInSc1	club
nebo	nebo	k8xC	nebo
The	The	k1gFnSc1	The
Roundhouse	Roundhouse	k1gFnSc1	Roundhouse
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
pozvána	pozvat	k5eAaPmNgFnS	pozvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
hudebně	hudebně	k6eAd1	hudebně
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
filmu	film	k1gInSc6	film
Tonite	Tonit	k1gInSc5	Tonit
Let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
All	All	k1gFnSc7	All
Make	Mak	k1gFnSc2	Mak
Love	lov	k1gInSc5	lov
in	in	k?	in
London	London	k1gMnSc1	London
režiséra	režisér	k1gMnSc2	režisér
Petera	Petera	k1gMnSc1	Petera
Whiteheada	Whiteheada	k1gFnSc1	Whiteheada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1967	[number]	k4	1967
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
snímek	snímek	k1gInSc4	snímek
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
nahráli	nahrát	k5eAaPmAgMnP	nahrát
dvě	dva	k4xCgFnPc4	dva
skladby	skladba	k1gFnPc4	skladba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Interstellar	Interstellar	k1gInSc1	Interstellar
Overdrive	Overdriev	k1gFnSc2	Overdriev
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Nick	Nick	k1gInSc1	Nick
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Boogie	Boogie	k1gFnPc4	Boogie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
téměř	téměř	k6eAd1	téměř
nic	nic	k3yNnSc1	nic
nebylo	být	k5eNaImAgNnS	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
materiálu	materiál	k1gInSc2	materiál
ve	v	k7c6	v
filmu	film	k1gInSc6	film
použito	použít	k5eAaPmNgNnS	použít
<g/>
,	,	kIx,	,
kompletní	kompletní	k2eAgFnPc1d1	kompletní
skladby	skladba	k1gFnPc1	skladba
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
vydání	vydání	k1gNnSc4	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
London	London	k1gMnSc1	London
'	'	kIx"	'
<g/>
66	[number]	k4	66
<g/>
-	-	kIx~	-
<g/>
'	'	kIx"	'
<g/>
67	[number]	k4	67
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
skupiny	skupina	k1gFnSc2	skupina
rostla	růst	k5eAaImAgFnS	růst
a	a	k8xC	a
proto	proto	k8xC	proto
čtyři	čtyři	k4xCgMnPc1	čtyři
členové	člen	k1gMnPc1	člen
společně	společně	k6eAd1	společně
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
manažery	manažer	k1gMnPc7	manažer
<g/>
,	,	kIx,	,
Peterem	Peter	k1gMnSc7	Peter
Jennerem	Jenner	k1gMnSc7	Jenner
a	a	k8xC	a
Andrewem	Andrew	k1gMnSc7	Andrew
Kingem	King	k1gMnSc7	King
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1966	[number]	k4	1966
společnost	společnost	k1gFnSc1	společnost
Blackhill	Blackhilla	k1gFnPc2	Blackhilla
Enterprises	Enterprisesa	k1gFnPc2	Enterprisesa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1967	[number]	k4	1967
ale	ale	k9	ale
podepsali	podepsat	k5eAaPmAgMnP	podepsat
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1967	[number]	k4	1967
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Arnold	Arnold	k1gMnSc1	Arnold
Layne	Layn	k1gMnSc5	Layn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
jej	on	k3xPp3gInSc4	on
následoval	následovat	k5eAaImAgInS	následovat
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
See	See	k1gMnSc1	See
Emily	Emil	k1gMnPc7	Emil
Play	play	k0	play
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Arnold	Arnold	k1gMnSc1	Arnold
Layne	Layn	k1gInSc5	Layn
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
"	"	kIx"	"
<g/>
See	See	k1gMnSc1	See
Emily	Emil	k1gMnPc7	Emil
Play	play	k0	play
<g/>
"	"	kIx"	"
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zaručilo	zaručit	k5eAaPmAgNnS	zaručit
skupině	skupina	k1gFnSc3	skupina
její	její	k3xOp3gFnSc2	její
první	první	k4xOgNnSc4	první
televizní	televizní	k2eAgNnSc4d1	televizní
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
pořadu	pořad	k1gInSc6	pořad
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Pops	Popsa	k1gFnPc2	Popsa
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
The	The	k1gFnPc1	The
Piper	Piper	k1gMnSc1	Piper
at	at	k?	at
the	the	k?	the
Gates	Gates	k1gInSc1	Gates
of	of	k?	of
Dawn	Dawn	k1gNnSc1	Dawn
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1967	[number]	k4	1967
a	a	k8xC	a
mezi	mezi	k7c7	mezi
odbornou	odborný	k2eAgFnSc7d1	odborná
veřejností	veřejnost	k1gFnSc7	veřejnost
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
veskrze	veskrze	k6eAd1	veskrze
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
alb	album	k1gNnPc2	album
britského	britský	k2eAgInSc2d1	britský
psychedelického	psychedelický	k2eAgInSc2d1	psychedelický
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
na	na	k7c6	na
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
napsal	napsat	k5eAaPmAgMnS	napsat
Syd	Syd	k1gMnSc1	Syd
Barrett	Barrett	k1gMnSc1	Barrett
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
poetické	poetický	k2eAgInPc4d1	poetický
texty	text	k1gInPc4	text
společně	společně	k6eAd1	společně
s	s	k7c7	s
rozmanitými	rozmanitý	k2eAgInPc7d1	rozmanitý
hudebními	hudební	k2eAgInPc7d1	hudební
motivy	motiv	k1gInPc7	motiv
<g/>
,	,	kIx,	,
od	od	k7c2	od
avantgardní	avantgardní	k2eAgFnSc2d1	avantgardní
volné	volný	k2eAgFnSc2d1	volná
formy	forma	k1gFnSc2	forma
v	v	k7c4	v
"	"	kIx"	"
<g/>
Interstellar	Interstellar	k1gInSc4	Interstellar
Overdrive	Overdriev	k1gFnSc2	Overdriev
<g/>
"	"	kIx"	"
k	k	k7c3	k
hravým	hravý	k2eAgInPc3d1	hravý
rytmům	rytmus	k1gInPc3	rytmus
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Scarecrow	Scarecrow	k1gMnSc1	Scarecrow
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
venkovskou	venkovský	k2eAgFnSc7d1	venkovská
oblastí	oblast	k1gFnSc7	oblast
The	The	k1gFnSc2	The
Fens	Fensa	k1gFnPc2	Fensa
severně	severně	k6eAd1	severně
od	od	k7c2	od
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
Barrett	Barrett	k1gInSc1	Barrett
a	a	k8xC	a
Waters	Waters	k1gInSc1	Waters
(	(	kIx(	(
<g/>
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
pozdější	pozdní	k2eAgMnSc1d2	pozdější
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
surrealistické	surrealistický	k2eAgFnPc1d1	surrealistická
a	a	k8xC	a
často	často	k6eAd1	často
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
k	k	k7c3	k
folkloru	folklor	k1gInSc3	folklor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k9	třeba
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Gnome	Gnom	k1gMnSc5	Gnom
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Skřítek	skřítek	k1gMnSc1	skřítek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
produkoval	produkovat	k5eAaImAgMnS	produkovat
Norman	Norman	k1gMnSc1	Norman
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
výrazného	výrazný	k2eAgNnSc2d1	výrazné
využití	využití	k1gNnSc2	využití
stereo	stereo	k2eAgInPc2d1	stereo
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
echa	echo	k1gNnSc2	echo
a	a	k8xC	a
úprav	úprava	k1gFnPc2	úprava
pásku	pásek	k1gInSc2	pásek
projevovaly	projevovat	k5eAaImAgFnP	projevovat
nové	nový	k2eAgFnPc1d1	nová
technologie	technologie	k1gFnPc1	technologie
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
stalo	stát	k5eAaPmAgNnS	stát
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
když	když	k8xS	když
vystoupalo	vystoupat	k5eAaPmAgNnS	vystoupat
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
USA	USA	kA	USA
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
prakticky	prakticky	k6eAd1	prakticky
nepovšimnuto	povšimnut	k2eNgNnSc1d1	nepovšimnuto
(	(	kIx(	(
<g/>
131	[number]	k4	131
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
skupina	skupina	k1gFnSc1	skupina
turné	turné	k1gNnSc2	turné
s	s	k7c7	s
Jimim	Jimi	k1gNnSc7	Jimi
Hendrixem	Hendrix	k1gInSc7	Hendrix
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jí	on	k3xPp3gFnSc3	on
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
popularity	popularita	k1gFnSc2	popularita
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
náročným	náročný	k2eAgInSc7d1	náročný
životem	život	k1gInSc7	život
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
tlakem	tlak	k1gInSc7	tlak
vydavatele	vydavatel	k1gMnSc2	vydavatel
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
hitových	hitový	k2eAgFnPc2d1	hitová
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
častým	častý	k2eAgNnSc7d1	časté
užíváním	užívání	k1gNnSc7	užívání
psychedelických	psychedelický	k2eAgFnPc2d1	psychedelická
drog	droga	k1gFnPc2	droga
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
LSD	LSD	kA	LSD
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
daň	daň	k1gFnSc4	daň
na	na	k7c6	na
Sydu	Syd	k1gMnSc6	Syd
Barrettovi	Barrett	k1gMnSc6	Barrett
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
duševní	duševní	k2eAgNnSc1d1	duševní
zdraví	zdraví	k1gNnSc1	zdraví
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
a	a	k8xC	a
Barrett	Barrett	k1gMnSc1	Barrett
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
chovat	chovat	k5eAaImF	chovat
velmi	velmi	k6eAd1	velmi
podivínsky	podivínsky	k6eAd1	podivínsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1968	[number]	k4	1968
se	se	k3xPyFc4	se
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
připojil	připojit	k5eAaPmAgMnS	připojit
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahradil	nahradit	k5eAaPmAgMnS	nahradit
v	v	k7c4	v
hraní	hraní	k1gNnSc4	hraní
a	a	k8xC	a
zpívání	zpívání	k1gNnSc4	zpívání
Barretta	Barrett	k1gInSc2	Barrett
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
užívání	užívání	k1gNnSc2	užívání
LSD	LSD	kA	LSD
se	se	k3xPyFc4	se
Barrettovo	Barrettův	k2eAgNnSc1d1	Barrettovo
chování	chování	k1gNnSc1	chování
stalo	stát	k5eAaPmAgNnS	stát
prakticky	prakticky	k6eAd1	prakticky
nepředvídatelným	předvídatelný	k2eNgInSc7d1	nepředvídatelný
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
velmi	velmi	k6eAd1	velmi
labilním	labilní	k2eAgMnSc6d1	labilní
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
zíral	zírat	k5eAaImAgMnS	zírat
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
při	při	k7c6	při
vystoupeních	vystoupení	k1gNnPc6	vystoupení
hrál	hrát	k5eAaImAgMnS	hrát
jen	jen	k9	jen
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
strunu	struna	k1gFnSc4	struna
na	na	k7c6	na
kytaře	kytara	k1gFnSc6	kytara
anebo	anebo	k8xC	anebo
kytaru	kytara	k1gFnSc4	kytara
začal	začít	k5eAaPmAgMnS	začít
rozlaďovat	rozlaďovat	k5eAaImF	rozlaďovat
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
pohromou	pohroma	k1gFnSc7	pohroma
<g/>
,	,	kIx,	,
zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
přestanou	přestat	k5eAaPmIp3nP	přestat
na	na	k7c4	na
vystoupení	vystoupení	k1gNnSc4	vystoupení
brát	brát	k5eAaImF	brát
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
myšlenka	myšlenka	k1gFnSc1	myšlenka
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Barrett	Barrett	k1gMnSc1	Barrett
bude	být	k5eAaImBp3nS	být
skládat	skládat	k5eAaImF	skládat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
dobrý	dobrý	k2eAgMnSc1d1	dobrý
kamarád	kamarád	k1gMnSc1	kamarád
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
živě	živě	k6eAd1	živě
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Barrett	Barrett	k1gInSc1	Barrett
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
skládat	skládat	k5eAaImF	skládat
tak	tak	k6eAd1	tak
komplikované	komplikovaný	k2eAgFnPc4d1	komplikovaná
písně	píseň	k1gFnPc4	píseň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Have	Hav	k1gMnSc2	Hav
You	You	k1gMnSc2	You
Got	Got	k1gMnSc2	Got
It	It	k1gMnSc2	It
Yet	Yet	k1gMnSc2	Yet
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
melodie	melodie	k1gFnPc1	melodie
a	a	k8xC	a
akordy	akord	k1gInPc1	akord
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
dalším	další	k2eAgInSc7d1	další
taktem	takt	k1gInSc7	takt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zbytek	zbytek	k1gInSc1	zbytek
skupiny	skupina	k1gFnSc2	skupina
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nepokračovat	pokračovat	k5eNaImF	pokračovat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
koncert	koncert	k1gInSc1	koncert
s	s	k7c7	s
Barrettem	Barrett	k1gInSc7	Barrett
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1968	[number]	k4	1968
v	v	k7c6	v
Hastingsu	Hastings	k1gInSc6	Hastings
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
vystoupení	vystoupení	k1gNnPc2	vystoupení
odehráli	odehrát	k5eAaPmAgMnP	odehrát
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc4	Floyd
v	v	k7c6	v
pětičlenném	pětičlenný	k2eAgNnSc6d1	pětičlenné
obsazení	obsazení	k1gNnSc6	obsazení
s	s	k7c7	s
Barrettem	Barrett	k1gMnSc7	Barrett
i	i	k8xC	i
Gilmourem	Gilmour	k1gMnSc7	Gilmour
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Barrettův	Barrettův	k2eAgInSc1d1	Barrettův
odchod	odchod	k1gInSc1	odchod
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1968	[number]	k4	1968
oficiálním	oficiální	k2eAgMnPc3d1	oficiální
<g/>
,	,	kIx,	,
producenti	producent	k1gMnPc1	producent
Jenner	Jenner	k1gMnSc1	Jenner
a	a	k8xC	a
King	King	k1gMnSc1	King
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
zůstat	zůstat	k5eAaPmF	zůstat
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
Blackhill	Blackhilla	k1gFnPc2	Blackhilla
Enterprises	Enterprises	k1gInSc4	Enterprises
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
se	se	k3xPyFc4	se
jako	jako	k9	jako
manažer	manažer	k1gMnSc1	manažer
přidal	přidat	k5eAaPmAgMnS	přidat
Steve	Steve	k1gMnSc1	Steve
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Rourke	Rourk	k1gMnSc2	Rourk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
Pink	pink	k2eAgFnSc7d1	pink
Floyd	Floyd	k1gInSc1	Floyd
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
dvou	dva	k4xCgFnPc2	dva
sólových	sólový	k2eAgFnPc2d1	sólová
alb	alba	k1gFnPc2	alba
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Madcap	Madcap	k1gMnSc1	Madcap
Laughs	Laughs	k1gInSc1	Laughs
a	a	k8xC	a
Barrett	Barrett	k1gInSc1	Barrett
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
na	na	k7c6	na
albech	album	k1gNnPc6	album
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
Waters	Waters	k1gInSc1	Waters
a	a	k8xC	a
Wright	Wright	k1gInSc1	Wright
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
pouze	pouze	k6eAd1	pouze
průměrného	průměrný	k2eAgInSc2d1	průměrný
ohlasu	ohlas	k1gInSc2	ohlas
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
Barrett	Barrett	k1gMnSc1	Barrett
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Opustil	opustit	k5eAaPmAgMnS	opustit
svoji	svůj	k3xOyFgFnSc4	svůj
přezdívku	přezdívka	k1gFnSc4	přezdívka
Syd	Syd	k1gFnSc4	Syd
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
rodnému	rodný	k2eAgNnSc3d1	rodné
jménu	jméno	k1gNnSc3	jméno
Roger	Rogero	k1gNnPc2	Rogero
a	a	k8xC	a
žil	žít	k5eAaImAgInS	žít
klidným	klidný	k2eAgInSc7d1	klidný
životem	život	k1gInSc7	život
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Barrettově	Barrettův	k2eAgInSc6d1	Barrettův
odchodu	odchod	k1gInSc6	odchod
prodělali	prodělat	k5eAaPmAgMnP	prodělat
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc4	Floyd
období	období	k1gNnSc2	období
hudebních	hudební	k2eAgInPc2d1	hudební
experimentů	experiment	k1gInPc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmoura	k1gFnPc2	Gilmoura
<g/>
,	,	kIx,	,
Waters	Watersa	k1gFnPc2	Watersa
i	i	k8xC	i
Wright	Wrighta	k1gFnPc2	Wrighta
přispívali	přispívat	k5eAaImAgMnP	přispívat
vlastním	vlastní	k2eAgInSc7d1	vlastní
materiálem	materiál	k1gInSc7	materiál
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
hlasem	hlas	k1gInSc7	hlas
i	i	k8xC	i
zvukem	zvuk	k1gInSc7	zvuk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
různorodější	různorodý	k2eAgMnSc1d2	různorodější
<g/>
,	,	kIx,	,
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
bývalo	bývat	k5eAaImAgNnS	bývat
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
Barrettovy	Barrettův	k2eAgFnSc2d1	Barrettova
dominance	dominance	k1gFnSc2	dominance
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zvuk	zvuk	k1gInSc1	zvuk
kapely	kapela	k1gFnSc2	kapela
více	hodně	k6eAd2	hodně
uhlazený	uhlazený	k2eAgInSc1d1	uhlazený
a	a	k8xC	a
jednotný	jednotný	k2eAgInSc1d1	jednotný
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Syd	Syd	k1gFnPc2	Syd
Barrett	Barrett	k1gInSc1	Barrett
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
si	se	k3xPyFc3	se
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
Waters	Waters	k1gInSc1	Waters
a	a	k8xC	a
Wright	Wright	k1gInSc1	Wright
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
úlohy	úloha	k1gFnPc4	úloha
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
při	při	k7c6	při
zpěvu	zpěv	k1gInSc6	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Waters	Waters	k6eAd1	Waters
psal	psát	k5eAaImAgInS	psát
většinou	většina	k1gFnSc7	většina
jazzové	jazzový	k2eAgFnSc2d1	jazzová
melodie	melodie	k1gFnSc2	melodie
s	s	k7c7	s
dominantní	dominantní	k2eAgFnSc7d1	dominantní
basovou	basový	k2eAgFnSc7d1	basová
linkou	linka	k1gFnSc7	linka
a	a	k8xC	a
symbolickými	symbolický	k2eAgInPc7d1	symbolický
texty	text	k1gInPc7	text
<g/>
,	,	kIx,	,
Gilmour	Gilmour	k1gMnSc1	Gilmour
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
bluesové	bluesový	k2eAgNnSc4d1	bluesové
jammování	jammování	k1gNnSc4	jammování
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
kytarou	kytara	k1gFnSc7	kytara
a	a	k8xC	a
Wright	Wright	k1gMnSc1	Wright
preferoval	preferovat	k5eAaImAgMnS	preferovat
psychedelické	psychedelický	k2eAgFnPc4d1	psychedelická
písně	píseň	k1gFnPc4	píseň
zaměřené	zaměřený	k2eAgFnPc4d1	zaměřená
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Watersovi	Waters	k1gMnSc3	Waters
měli	mít	k5eAaImAgMnP	mít
Gilmour	Gilmour	k1gMnSc1	Gilmour
a	a	k8xC	a
Wright	Wright	k1gMnSc1	Wright
rádi	rád	k2eAgMnPc1d1	rád
písně	píseň	k1gFnPc4	píseň
s	s	k7c7	s
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
textem	text	k1gInSc7	text
či	či	k8xC	či
skladby	skladba	k1gFnPc4	skladba
přímo	přímo	k6eAd1	přímo
instrumentální	instrumentální	k2eAgInPc1d1	instrumentální
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Waters	Waters	k1gInSc1	Waters
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
texty	text	k1gInPc4	text
i	i	k9	i
ostatním	ostatní	k2eAgFnPc3d1	ostatní
dvěma	dva	k4xCgMnPc3	dva
skladatelům	skladatel	k1gMnPc3	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
naopak	naopak	k6eAd1	naopak
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
hudební	hudební	k2eAgFnSc4d1	hudební
složku	složka	k1gFnSc4	složka
většiny	většina	k1gFnSc2	většina
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
hudebních	hudební	k2eAgInPc2d1	hudební
experimentů	experiment	k1gInPc2	experiment
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
pochází	pocházet	k5eAaImIp3nS	pocházet
právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
A	a	k9	a
Saucerful	Saucerful	k1gInSc1	Saucerful
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
hluky	hluk	k1gInPc4	hluk
<g/>
,	,	kIx,	,
údery	úder	k1gInPc4	úder
<g/>
,	,	kIx,	,
zpětné	zpětný	k2eAgFnPc4d1	zpětná
vazby	vazba	k1gFnPc4	vazba
<g/>
,	,	kIx,	,
oscilátory	oscilátor	k1gInPc4	oscilátor
a	a	k8xC	a
páskové	páskový	k2eAgFnPc4d1	pásková
smyčky	smyčka	k1gFnPc4	smyčka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Several	Several	k1gFnSc1	Several
Species	species	k1gFnSc2	species
of	of	k?	of
Small	Small	k1gMnSc1	Small
Furry	Furra	k1gFnSc2	Furra
Animals	Animals	k1gInSc1	Animals
Gathered	Gathered	k1gMnSc1	Gathered
Together	Togethra	k1gFnPc2	Togethra
in	in	k?	in
a	a	k8xC	a
Cave	Cave	k1gInSc1	Cave
and	and	k?	and
Grooving	Grooving	k1gInSc1	Grooving
with	witha	k1gFnPc2	witha
a	a	k8xC	a
Pict	Picta	k1gFnPc2	Picta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
zrychlených	zrychlený	k2eAgFnPc2d1	zrychlená
nahrávek	nahrávka	k1gFnPc2	nahrávka
připomínajících	připomínající	k2eAgFnPc2d1	připomínající
zvuk	zvuk	k1gInSc4	zvuk
hlodavců	hlodavec	k1gMnPc2	hlodavec
a	a	k8xC	a
štěbetání	štěbetání	k1gNnPc2	štěbetání
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
těžko	těžko	k6eAd1	těžko
srozumitelné	srozumitelný	k2eAgFnSc6d1	srozumitelná
nahrávce	nahrávka	k1gFnSc6	nahrávka
monologu	monolog	k1gInSc2	monolog
vyprávěném	vyprávěný	k2eAgInSc6d1	vyprávěný
skotským	skotský	k2eAgInSc7d1	skotský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Careful	Careful	k1gInSc1	Careful
with	with	k1gMnSc1	with
That	That	k1gMnSc1	That
Axe	Axe	k1gMnSc1	Axe
<g/>
,	,	kIx,	,
Eugene	Eugen	k1gMnSc5	Eugen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
měla	mít	k5eAaImAgFnS	mít
několik	několik	k4yIc4	několik
verzí	verze	k1gFnPc2	verze
názvu	název	k1gInSc2	název
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
watersovská	watersovský	k2eAgFnSc1d1	watersovský
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
basový	basový	k2eAgInSc4d1	basový
a	a	k8xC	a
klávesový	klávesový	k2eAgInSc4d1	klávesový
jam	jam	k1gInSc4	jam
vrcholící	vrcholící	k2eAgInPc4d1	vrcholící
údery	úder	k1gInPc4	úder
do	do	k7c2	do
bubnů	buben	k1gInPc2	buben
a	a	k8xC	a
Watersovým	Watersův	k2eAgNnSc7d1	Watersovo
nelidským	lidský	k2eNgNnSc7d1	nelidské
zařváním	zařvání	k1gNnSc7	zařvání
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Barrett	Barrett	k1gInSc1	Barrett
napsal	napsat	k5eAaBmAgInS	napsat
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
albu	album	k1gNnSc6	album
A	a	k8xC	a
Saucerful	Saucerful	k1gInSc1	Saucerful
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
Barrettově	Barrettův	k2eAgInSc6d1	Barrettův
odchodu	odchod	k1gInSc6	odchod
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
objevuje	objevovat	k5eAaImIp3nS	objevovat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
píseň	píseň	k1gFnSc1	píseň
-	-	kIx~	-
"	"	kIx"	"
<g/>
Jugband	Jugband	k1gInSc1	Jugband
Blues	blues	k1gNnSc1	blues
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
desce	deska	k1gFnSc6	deska
Barrett	Barrett	k1gInSc1	Barrett
ještě	ještě	k6eAd1	ještě
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
skladbách	skladba	k1gFnPc6	skladba
"	"	kIx"	"
<g/>
Remember	Remembra	k1gFnPc2	Remembra
a	a	k8xC	a
Day	Day	k1gFnPc2	Day
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Set	set	k1gInSc1	set
the	the	k?	the
Controls	Controlsa	k1gFnPc2	Controlsa
for	forum	k1gNnPc2	forum
the	the	k?	the
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
nahrány	nahrát	k5eAaBmNgFnP	nahrát
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
A	a	k8xC	a
Saucerful	Saucerful	k1gInSc1	Saucerful
of	of	k?	of
Secrets	Secretsa	k1gFnPc2	Secretsa
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
hitparádě	hitparáda	k1gFnSc6	hitparáda
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jediným	jediný	k2eAgNnSc7d1	jediné
albem	album	k1gNnSc7	album
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
neumístilo	umístit	k5eNaPmAgNnS	umístit
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Barrett	Barrett	k1gMnSc1	Barrett
již	již	k6eAd1	již
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
stále	stále	k6eAd1	stále
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
psychedelickou	psychedelický	k2eAgFnSc4d1	psychedelická
hudbu	hudba	k1gFnSc4	hudba
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
stylu	styl	k1gInSc6	styl
zkombinovanou	zkombinovaný	k2eAgFnSc7d1	zkombinovaná
s	s	k7c7	s
experimentálním	experimentální	k2eAgNnSc7d1	experimentální
provedením	provedení	k1gNnSc7	provedení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
projevilo	projevit	k5eAaPmAgNnS	projevit
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Ummagumma	Ummagummum	k1gNnSc2	Ummagummum
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
skladbou	skladba	k1gFnSc7	skladba
A	a	k9	a
Saucerful	Saucerful	k1gInSc1	Saucerful
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
je	být	k5eAaImIp3nS	být
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
12	[number]	k4	12
<g/>
minutové	minutový	k2eAgNnSc4d1	minutové
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
předznamenalo	předznamenat	k5eAaPmAgNnS	předznamenat
nástup	nástup	k1gInSc4	nástup
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
<g/>
,	,	kIx,	,
epických	epický	k2eAgFnPc2d1	epická
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
kritiky	kritika	k1gFnSc2	kritika
přijato	přijmout	k5eAaPmNgNnS	přijmout
nevalně	valně	k6eNd1	valně
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
dílu	dílo	k1gNnSc3	dílo
kritici	kritik	k1gMnPc1	kritik
shovívavější	shovívavý	k2eAgMnPc1d2	shovívavější
<g/>
,	,	kIx,	,
především	především	k9	především
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
berou	brát	k5eAaImIp3nP	brát
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
tvorbou	tvorba	k1gFnSc7	tvorba
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
alba	alba	k1gFnSc1	alba
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
poté	poté	k6eAd1	poté
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
monumentální	monumentální	k2eAgFnPc1d1	monumentální
skladby	skladba	k1gFnPc1	skladba
s	s	k7c7	s
rozrůstající	rozrůstající	k2eAgFnSc7d1	rozrůstající
kompozicí	kompozice	k1gFnSc7	kompozice
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
koncentrovanějšími	koncentrovaný	k2eAgInPc7d2	koncentrovanější
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
byli	být	k5eAaImAgMnP	být
najati	najmout	k5eAaPmNgMnP	najmout
režisérem	režisér	k1gMnSc7	režisér
Barbetem	Barbet	k1gMnSc7	Barbet
Schroederem	Schroeder	k1gMnSc7	Schroeder
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
složili	složit	k5eAaPmAgMnP	složit
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
filmu	film	k1gInSc3	film
More	mor	k1gInSc5	mor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1	soundtrack
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Soundtrack	soundtrack	k1gInSc1	soundtrack
from	froma	k1gFnPc2	froma
the	the	k?	the
Film	film	k1gInSc1	film
More	mor	k1gInSc5	mor
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
na	na	k7c6	na
153	[number]	k4	153
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Odlišovalo	odlišovat	k5eAaImAgNnS	odlišovat
se	se	k3xPyFc4	se
od	od	k7c2	od
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
LP	LP	kA	LP
od	od	k7c2	od
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
;	;	kIx,	;
mnoho	mnoho	k4c1	mnoho
skladeb	skladba	k1gFnPc2	skladba
na	na	k7c4	na
More	mor	k1gInSc5	mor
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
jej	on	k3xPp3gMnSc4	on
fanoušci	fanoušek	k1gMnPc1	fanoušek
obyčejně	obyčejně	k6eAd1	obyčejně
nazývají	nazývat	k5eAaImIp3nP	nazývat
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
akustické	akustický	k2eAgFnPc4d1	akustická
folkové	folkový	k2eAgFnPc4d1	folková
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Green	Green	k2eAgMnSc1d1	Green
Is	Is	k1gMnSc1	Is
the	the	k?	the
Colour	Colour	k1gMnSc1	Colour
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Cymbaline	Cymbalin	k1gInSc5	Cymbalin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
živých	živá	k1gFnPc2	živá
vystoupení	vystoupení	k1gNnSc4	vystoupení
i	i	k8xC	i
turné	turné	k1gNnSc4	turné
The	The	k1gFnSc2	The
Man	Man	k1gMnSc1	Man
and	and	k?	and
the	the	k?	the
Journey	Journea	k1gFnSc2	Journea
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Cymbaline	Cymbalin	k1gInSc5	Cymbalin
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
první	první	k4xOgInSc4	první
písní	píseň	k1gFnPc2	píseň
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc4	Floyd
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
Roger	Roger	k1gInSc4	Roger
Waters	Watersa	k1gFnPc2	Watersa
cynicky	cynicky	k6eAd1	cynicky
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
vůči	vůči	k7c3	vůči
hudebnímu	hudební	k2eAgInSc3d1	hudební
průmyslu	průmysl	k1gInSc3	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
avantgardních	avantgardní	k2eAgInPc2d1	avantgardní
kousků	kousek	k1gInPc2	kousek
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
rovněž	rovněž	k9	rovněž
součástí	součást	k1gFnPc2	součást
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
and	and	k?	and
the	the	k?	the
Journey	Journea	k1gFnSc2	Journea
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
několika	několik	k4yIc2	několik
rockovějších	rockový	k2eAgFnPc2d2	rockovější
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
až	až	k9	až
hard	hard	k6eAd1	hard
rockově	rockově	k6eAd1	rockově
znějící	znějící	k2eAgFnSc1d1	znějící
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Nile	Nil	k1gInSc5	Nil
Song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nahrávkou	nahrávka	k1gFnSc7	nahrávka
skupiny	skupina	k1gFnSc2	skupina
bylo	být	k5eAaImAgNnS	být
dvojalbum	dvojalbum	k1gNnSc1	dvojalbum
Ummagumma	Ummagumma	k1gFnSc1	Ummagumma
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
desku	deska	k1gFnSc4	deska
tvoří	tvořit	k5eAaImIp3nP	tvořit
živé	živý	k2eAgFnPc1d1	živá
nahrávky	nahrávka	k1gFnPc1	nahrávka
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
konaných	konaný	k2eAgInPc2d1	konaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
studiový	studiový	k2eAgInSc1d1	studiový
experiment	experiment	k1gInSc1	experiment
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
sólově	sólově	k6eAd1	sólově
nahrál	nahrát	k5eAaPmAgMnS	nahrát
půlku	půlka	k1gFnSc4	půlka
gramofonové	gramofonový	k2eAgFnSc2d1	gramofonová
desky	deska	k1gFnSc2	deska
jako	jako	k8xC	jako
sólový	sólový	k2eAgInSc1d1	sólový
projekt	projekt	k1gInSc1	projekt
(	(	kIx(	(
<g/>
Masonova	masonův	k2eAgFnSc1d1	Masonova
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
přispěla	přispět	k5eAaPmAgFnS	přispět
jako	jako	k9	jako
flétnistka	flétnistka	k1gFnSc1	flétnistka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
čistě	čistě	k6eAd1	čistě
avantgardní	avantgardní	k2eAgFnSc1d1	avantgardní
směs	směs	k1gFnSc1	směs
zvuků	zvuk	k1gInPc2	zvuk
z	z	k7c2	z
"	"	kIx"	"
<g/>
nalezených	nalezený	k2eAgInPc2d1	nalezený
<g/>
"	"	kIx"	"
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc1	potíž
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
skupiny	skupina	k1gFnSc2	skupina
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
ummagumma	ummagumma	k1gNnSc1	ummagumma
<g/>
"	"	kIx"	"
v	v	k7c6	v
názvu	název	k1gInSc6	název
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
cambridgeském	cambridgeský	k2eAgInSc6d1	cambridgeský
slangu	slang	k1gInSc6	slang
"	"	kIx"	"
<g/>
soulož	soulož	k1gFnSc1	soulož
<g/>
"	"	kIx"	"
a	a	k8xC	a
odráží	odrážet	k5eAaImIp3nS	odrážet
postoj	postoj	k1gInSc4	postoj
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
její	její	k3xOp3gInSc4	její
členové	člen	k1gMnPc1	člen
frustrovaní	frustrovaný	k2eAgMnPc1d1	frustrovaný
z	z	k7c2	z
neustálé	neustálý	k2eAgFnSc2d1	neustálá
práce	práce	k1gFnSc2	práce
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
část	část	k1gFnSc1	část
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
experimentální	experimentální	k2eAgInSc1d1	experimentální
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Watersova	Watersův	k2eAgFnSc1d1	Watersova
folková	folkový	k2eAgFnSc1d1	folková
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Grantchester	Grantchester	k1gInSc1	Grantchester
Meadows	Meadows	k1gInSc1	Meadows
<g/>
"	"	kIx"	"
a	a	k8xC	a
experimentální	experimentální	k2eAgFnSc1d1	experimentální
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Several	Several	k1gFnSc1	Several
Species	species	k1gFnSc2	species
of	of	k?	of
Small	Small	k1gMnSc1	Small
Furry	Furra	k1gFnSc2	Furra
Animals	Animals	k1gInSc1	Animals
Gathered	Gathered	k1gMnSc1	Gathered
Together	Togethra	k1gFnPc2	Togethra
in	in	k?	in
a	a	k8xC	a
Cave	Cave	k1gInSc1	Cave
and	and	k?	and
Grooving	Grooving	k1gInSc1	Grooving
with	witha	k1gFnPc2	witha
a	a	k8xC	a
Pict	Picta	k1gFnPc2	Picta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nemelodický	melodický	k2eNgInSc1d1	nemelodický
pianový	pianový	k2eAgInSc1d1	pianový
"	"	kIx"	"
<g/>
Sysyphus	Sysyphus	k1gInSc1	Sysyphus
<g/>
"	"	kIx"	"
od	od	k7c2	od
Wrighta	Wrighto	k1gNnSc2	Wrighto
<g/>
,	,	kIx,	,
různorodé	různorodý	k2eAgFnSc2d1	různorodá
progresivní	progresivní	k2eAgFnSc2d1	progresivní
rockové	rockový	k2eAgFnSc2d1	rocková
struktury	struktura	k1gFnSc2	struktura
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Narrow	Narrow	k1gFnSc1	Narrow
Way	Way	k1gFnSc1	Way
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
)	)	kIx)	)
a	a	k8xC	a
Masonovo	masonův	k2eAgNnSc1d1	Masonovo
sólo	sólo	k1gNnSc1	sólo
na	na	k7c4	na
bicí	bicí	k2eAgFnSc4d1	bicí
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Grand	grand	k1gMnSc1	grand
Vizier	Vizier	k1gMnSc1	Vizier
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Garden	Gardna	k1gFnPc2	Gardna
Party	party	k1gFnSc7	party
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Several	Several	k1gFnSc1	Several
Species	species	k1gFnSc1	species
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pětiminutový	pětiminutový	k2eAgInSc1d1	pětiminutový
song	song	k1gInSc1	song
složený	složený	k2eAgInSc1d1	složený
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
hlasu	hlas	k1gInSc2	hlas
Rogera	Rogero	k1gNnSc2	Rogero
Waterse	Waterse	k1gFnSc2	Waterse
přehrávaného	přehrávaný	k2eAgMnSc2d1	přehrávaný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
zní	znět	k5eAaImIp3nS	znět
jako	jako	k9	jako
zvuk	zvuk	k1gInSc1	zvuk
hlodavců	hlodavec	k1gMnPc2	hlodavec
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skladby	skladba	k1gFnPc1	skladba
ze	z	k7c2	z
studiového	studiový	k2eAgInSc2d1	studiový
disku	disk	k1gInSc2	disk
byly	být	k5eAaImAgFnP	být
hrány	hrát	k5eAaImNgFnP	hrát
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
The	The	k1gFnSc2	The
Man	Man	k1gMnSc1	Man
and	and	k?	and
the	the	k?	the
Journey	Journea	k1gFnSc2	Journea
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgInSc1d1	koncertní
disk	disk	k1gInSc1	disk
alba	album	k1gNnSc2	album
Ummagumma	Ummagummum	k1gNnSc2	Ummagummum
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
nejslavnější	slavný	k2eAgFnSc2d3	nejslavnější
psychedelické	psychedelický	k2eAgFnSc2d1	psychedelická
éry	éra	k1gFnSc2	éra
a	a	k8xC	a
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
této	tento	k3xDgFnSc3	tento
části	část	k1gFnSc3	část
ocenili	ocenit	k5eAaPmAgMnP	ocenit
kritici	kritik	k1gMnPc1	kritik
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
pozitivněji	pozitivně	k6eAd2	pozitivně
než	než	k8xS	než
předešlá	předešlý	k2eAgNnPc1d1	předešlé
dvě	dva	k4xCgNnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
počinem	počin	k1gInSc7	počin
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
hitparádě	hitparáda	k1gFnSc6	hitparáda
a	a	k8xC	a
74	[number]	k4	74
<g/>
.	.	kIx.	.
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Atom	atom	k1gInSc1	atom
Heart	Hearta	k1gFnPc2	Hearta
Mother	Mothra	k1gFnPc2	Mothra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
Pink	pink	k6eAd1	pink
Floyd	Floyda	k1gFnPc2	Floyda
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
s	s	k7c7	s
orchestrem	orchestr	k1gInSc7	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
díky	díky	k7c3	díky
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
avantgardním	avantgardní	k2eAgMnSc7d1	avantgardní
skladatelem	skladatel	k1gMnSc7	skladatel
Ronem	Ron	k1gMnSc7	Ron
Geesinem	Geesin	k1gMnSc7	Geesin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
strana	strana	k1gFnSc1	strana
LP	LP	kA	LP
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
titulní	titulní	k2eAgFnSc1d1	titulní
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
minutovou	minutový	k2eAgFnSc4d1	minutová
rockově-orchestrální	rockověrchestrální	k2eAgFnSc4d1	rockově-orchestrální
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
desky	deska	k1gFnSc2	deska
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
píseň	píseň	k1gFnSc1	píseň
od	od	k7c2	od
každého	každý	k3xTgNnSc2	každý
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
zpěváků	zpěvák	k1gMnPc2	zpěvák
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
(	(	kIx(	(
<g/>
Watersova	Watersův	k2eAgFnSc1d1	Watersova
folk	folk	k1gInSc1	folk
rocková	rockový	k2eAgFnSc1d1	rocková
"	"	kIx"	"
<g/>
If	If	k1gFnSc1	If
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Wrightova	Wrightův	k2eAgFnSc1d1	Wrightova
nostalgická	nostalgický	k2eAgFnSc1d1	nostalgická
"	"	kIx"	"
<g/>
Summer	Summer	k1gMnSc1	Summer
'	'	kIx"	'
<g/>
68	[number]	k4	68
<g/>
"	"	kIx"	"
a	a	k8xC	a
Gilmourova	Gilmourův	k2eAgFnSc1d1	Gilmourova
bluesová	bluesový	k2eAgFnSc1d1	bluesová
"	"	kIx"	"
<g/>
Fat	fatum	k1gNnPc2	fatum
Old	Olda	k1gFnPc2	Olda
Sun	suna	k1gFnPc2	suna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
skladbou	skladba	k1gFnSc7	skladba
je	být	k5eAaImIp3nS	být
dvanáctiminutová	dvanáctiminutový	k2eAgFnSc1d1	dvanáctiminutová
"	"	kIx"	"
<g/>
Alan	Alan	k1gMnSc1	Alan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Psychedelic	Psychedelice	k1gFnPc2	Psychedelice
Breakfast	Breakfast	k1gInSc1	Breakfast
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
koláž	koláž	k1gFnSc4	koláž
zvuků	zvuk	k1gInPc2	zvuk
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
dělá	dělat	k5eAaImIp3nS	dělat
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
jí	on	k3xPp3gFnSc3	on
snídani	snídaně	k1gFnSc3	snídaně
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
již	již	k6eAd1	již
hrají	hrát	k5eAaImIp3nP	hrát
běžné	běžný	k2eAgInPc1d1	běžný
hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
hluků	hluk	k1gInPc2	hluk
<g/>
,	,	kIx,	,
náhodných	náhodný	k2eAgInPc2d1	náhodný
zvukových	zvukový	k2eAgInPc2d1	zvukový
efektů	efekt	k1gInPc2	efekt
a	a	k8xC	a
nahrávek	nahrávka	k1gFnPc2	nahrávka
hlasů	hlas	k1gInPc2	hlas
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stalo	stát	k5eAaPmAgNnS	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
zvuku	zvuk	k1gInSc2	zvuk
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Atom	atom	k1gInSc4	atom
Heart	Heart	k1gInSc1	Heart
Mother	Mothra	k1gFnPc2	Mothra
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
velký	velký	k2eAgInSc4d1	velký
krok	krok	k1gInSc4	krok
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
bráno	brát	k5eAaImNgNnS	brát
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejnepřístupnějších	přístupný	k2eNgNnPc2d3	přístupný
alb	album	k1gNnPc2	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
a	a	k8xC	a
55	[number]	k4	55
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
Gilmour	Gilmour	k1gMnSc1	Gilmour
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
hromadou	hromada	k1gFnSc7	hromada
bordelu	bordel	k1gInSc2	bordel
<g/>
"	"	kIx"	"
a	a	k8xC	a
Waters	Waters	k1gInSc1	Waters
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
nevadilo	vadit	k5eNaImAgNnS	vadit
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
ho	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
album	album	k1gNnSc4	album
<g/>
)	)	kIx)	)
někdo	někdo	k3yInSc1	někdo
hodil	hodit	k5eAaImAgMnS	hodit
do	do	k7c2	do
popelnice	popelnice	k1gFnSc2	popelnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
už	už	k6eAd1	už
nikdo	nikdo	k3yNnSc1	nikdo
nikdy	nikdy	k6eAd1	nikdy
neposlouchal	poslouchat	k5eNaImAgMnS	poslouchat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
dalším	další	k2eAgInSc7d1	další
přechodným	přechodný	k2eAgInSc7d1	přechodný
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
titulní	titulní	k2eAgFnSc1d1	titulní
skladba	skladba	k1gFnSc1	skladba
napovídala	napovídat	k5eAaBmAgFnS	napovídat
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
ubere	ubrat	k5eAaPmIp3nS	ubrat
pozdější	pozdní	k2eAgInSc1d2	pozdější
vývoj	vývoj	k1gInSc1	vývoj
kapely	kapela	k1gFnSc2	kapela
-	-	kIx~	-
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
např.	např.	kA	např.
u	u	k7c2	u
"	"	kIx"	"
<g/>
Echoes	Echoesa	k1gFnPc2	Echoesa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
alba	album	k1gNnSc2	album
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
podnikli	podniknout	k5eAaPmAgMnP	podniknout
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
velké	velký	k2eAgNnSc4d1	velké
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
soundtracku	soundtrack	k1gInSc3	soundtrack
filmu	film	k1gInSc2	film
Zabriskie	Zabriskie	k1gFnSc2	Zabriskie
Point	pointa	k1gFnPc2	pointa
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jejich	jejich	k3xOp3gFnPc2	jejich
skladeb	skladba	k1gFnPc2	skladba
byla	být	k5eAaImAgNnP	být
nakonec	nakonec	k6eAd1	nakonec
režisérem	režisér	k1gMnSc7	režisér
Michelangelem	Michelangel	k1gMnSc7	Michelangel
Antonionim	Antonionim	k1gInSc4	Antonionim
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
další	další	k2eAgNnSc1d1	další
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
výběrovou	výběrový	k2eAgFnSc4d1	výběrová
desku	deska	k1gFnSc4	deska
Relics	Relicsa	k1gFnPc2	Relicsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
počátků	počátek	k1gInPc2	počátek
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
z	z	k7c2	z
B	B	kA	B
stran	stran	k7c2	stran
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Biding	Biding	k1gInSc1	Biding
My	my	k3xPp1nPc1	my
Time	Time	k1gNnPc6	Time
<g/>
"	"	kIx"	"
od	od	k7c2	od
Rogera	Rogero	k1gNnSc2	Rogero
Waterse	Waterse	k1gFnSc2	Waterse
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
byla	být	k5eAaImAgNnP	být
původně	původně	k6eAd1	původně
součástí	součást	k1gFnSc7	součást
turné	turné	k1gNnSc2	turné
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
and	and	k?	and
the	the	k?	the
Journey	Journea	k1gFnSc2	Journea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nahrána	nahrát	k5eAaPmNgFnS	nahrát
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
album	album	k1gNnSc4	album
Ummagumma	Ummagummum	k1gNnSc2	Ummagummum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
vzdali	vzdát	k5eAaPmAgMnP	vzdát
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
psychedelickou	psychedelický	k2eAgFnSc7d1	psychedelická
scénou	scéna	k1gFnSc7	scéna
(	(	kIx(	(
<g/>
a	a	k8xC	a
s	s	k7c7	s
Barrettem	Barrett	k1gInSc7	Barrett
<g/>
)	)	kIx)	)
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
význačnou	význačný	k2eAgFnSc7d1	význačná
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
zařadit	zařadit	k5eAaPmF	zařadit
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgInPc1d1	odlišný
styly	styl	k1gInPc1	styl
skladatelů	skladatel	k1gMnPc2	skladatel
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
Gilmoura	Gilmoura	k1gFnSc1	Gilmoura
<g/>
,	,	kIx,	,
Waterse	Waterse	k1gFnSc1	Waterse
a	a	k8xC	a
Wrighta	Wrighta	k1gFnSc1	Wrighta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
unikátní	unikátní	k2eAgInSc4d1	unikátní
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
znám	znát	k5eAaImIp1nS	znát
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
Sound	Sound	k1gMnSc1	Sound
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
éry	éra	k1gFnSc2	éra
patří	patřit	k5eAaImIp3nP	patřit
dvě	dva	k4xCgFnPc4	dva
asi	asi	k9	asi
nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
alba	album	k1gNnPc4	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
The	The	k1gFnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gMnSc1	Moon
a	a	k8xC	a
Wish	Wish	k1gMnSc1	Wish
You	You	k1gFnSc2	You
Were	Wer	k1gFnSc2	Wer
Here	Her	k1gFnSc2	Her
Zvuk	zvuk	k1gInSc1	zvuk
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jemným	jemný	k2eAgMnSc7d1	jemný
a	a	k8xC	a
celistvým	celistvý	k2eAgInSc7d1	celistvý
<g/>
,	,	kIx,	,
s	s	k7c7	s
filozofickými	filozofický	k2eAgInPc7d1	filozofický
texty	text	k1gInPc7	text
<g/>
,	,	kIx,	,
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
Watersovou	Watersův	k2eAgFnSc7d1	Watersova
baskytarou	baskytara	k1gFnSc7	baskytara
<g/>
,	,	kIx,	,
bluesovým	bluesový	k2eAgInSc7d1	bluesový
zvukem	zvuk	k1gInSc7	zvuk
Gilmourovy	Gilmourův	k2eAgFnSc2d1	Gilmourova
kytary	kytara	k1gFnSc2	kytara
a	a	k8xC	a
lehkými	lehký	k2eAgFnPc7d1	lehká
klávesovými	klávesový	k2eAgFnPc7d1	klávesová
melodiemi	melodie	k1gFnPc7	melodie
Ricka	Ricek	k1gMnSc2	Ricek
Wrighta	Wright	k1gMnSc2	Wright
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmour	k1gMnSc1	Gilmour
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
dominantním	dominantní	k2eAgMnSc7d1	dominantní
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
,	,	kIx,	,
typickými	typický	k2eAgFnPc7d1	typická
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
stal	stát	k5eAaPmAgMnS	stát
také	také	k9	také
saxofon	saxofon	k1gInSc4	saxofon
Dicka	Dicek	k1gMnSc2	Dicek
Parryho	Parry	k1gMnSc2	Parry
a	a	k8xC	a
ženské	ženský	k2eAgInPc4d1	ženský
vokály	vokál	k1gInPc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
nemelodický	melodický	k2eNgInSc4d1	nemelodický
a	a	k8xC	a
drsný	drsný	k2eAgInSc4d1	drsný
zvuk	zvuk	k1gInSc4	zvuk
z	z	k7c2	z
minulých	minulý	k2eAgNnPc2d1	Minulé
let	léto	k1gNnPc2	léto
předznamenal	předznamenat	k5eAaPmAgMnS	předznamenat
vývoj	vývoj	k1gInSc4	vývoj
ke	k	k7c3	k
zvuku	zvuk	k1gInSc3	zvuk
sametovému	sametový	k2eAgInSc3d1	sametový
<g/>
,	,	kIx,	,
čistému	čistý	k2eAgNnSc3d1	čisté
<g/>
,	,	kIx,	,
jemnému	jemný	k2eAgNnSc3d1	jemné
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
epické	epický	k2eAgFnPc1d1	epická
skladby	skladba	k1gFnPc1	skladba
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c4	v
"	"	kIx"	"
<g/>
Echoes	Echoes	k1gInSc4	Echoes
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
éra	éra	k1gFnSc1	éra
byla	být	k5eAaImAgFnS	být
nejen	nejen	k6eAd1	nejen
začátkem	začátek	k1gInSc7	začátek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
koncem	koncem	k7c2	koncem
spolupráce	spolupráce	k1gFnSc2	spolupráce
kompletní	kompletní	k2eAgFnSc2d1	kompletní
skupiny	skupina	k1gFnSc2	skupina
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
stále	stále	k6eAd1	stále
patrnější	patrný	k2eAgInSc4d2	patrnější
Watersův	Watersův	k2eAgInSc4d1	Watersův
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
textařsky	textařsky	k6eAd1	textařsky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
hudebně	hudebně	k6eAd1	hudebně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
pochází	pocházet	k5eAaImIp3nS	pocházet
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
s	s	k7c7	s
albem	album	k1gNnSc7	album
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
<g/>
)	)	kIx)	)
poslední	poslední	k2eAgFnPc1d1	poslední
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
zpíval	zpívat	k5eAaImAgMnS	zpívat
hlavní	hlavní	k2eAgInPc4d1	hlavní
vokály	vokál	k1gInPc4	vokál
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Time	Time	k1gFnSc1	Time
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
či	či	k8xC	či
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
hudebně	hudebně	k6eAd1	hudebně
podílel	podílet	k5eAaImAgMnS	podílet
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Shine	Shin	k1gMnSc5	Shin
On	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
Crazy	Craza	k1gFnSc2	Craza
Diamond	Diamond	k1gMnSc1	Diamond
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
ostře	ostro	k6eAd1	ostro
klesal	klesat	k5eAaImAgInS	klesat
Gilmourův	Gilmourův	k2eAgInSc1d1	Gilmourův
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
nových	nový	k2eAgFnPc2d1	nová
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zpěvákem	zpěvák	k1gMnSc7	zpěvák
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc4d1	poslední
vazby	vazba	k1gFnPc4	vazba
k	k	k7c3	k
Barrettovi	Barrett	k1gMnSc3	Barrett
byly	být	k5eAaImAgInP	být
rozvázány	rozvázat	k5eAaPmNgInP	rozvázat
symbolicky	symbolicky	k6eAd1	symbolicky
albem	album	k1gNnSc7	album
Wish	Wisha	k1gFnPc2	Wisha
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Shine	Shin	k1gInSc5	Shin
On	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
Crazy	Craza	k1gFnSc2	Craza
Diamond	Diamond	k1gMnSc1	Diamond
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaBmNgFnS	napsat
jako	jako	k9	jako
pocta	pocta	k1gFnSc1	pocta
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
žalozpěv	žalozpěv	k1gInSc1	žalozpěv
nad	nad	k7c7	nad
osudem	osud	k1gInSc7	osud
jejich	jejich	k3xOp3gMnSc2	jejich
přítele	přítel	k1gMnSc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
album	album	k1gNnSc4	album
Meddle	Meddle	k1gMnSc2	Meddle
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
23	[number]	k4	23
<g/>
minutové	minutový	k2eAgNnSc1d1	minutové
epické	epický	k2eAgNnSc1d1	epické
dílo	dílo	k1gNnSc1	dílo
"	"	kIx"	"
<g/>
Echoes	Echoes	k1gInSc1	Echoes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zabírá	zabírat	k5eAaImIp3nS	zabírat
celou	celý	k2eAgFnSc4d1	celá
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Echoes	Echoes	k1gInSc1	Echoes
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jemná	jemný	k2eAgFnSc1d1	jemná
<g/>
,	,	kIx,	,
progresivní	progresivní	k2eAgFnSc1d1	progresivní
rocková	rockový	k2eAgFnSc1d1	rocková
skladba	skladba	k1gFnSc1	skladba
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
kytarovými	kytarový	k2eAgInPc7d1	kytarový
a	a	k8xC	a
klávesovými	klávesový	k2eAgNnPc7d1	klávesové
sóly	sólo	k1gNnPc7	sólo
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
mezihrou	mezihra	k1gFnSc7	mezihra
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
syntetizovaného	syntetizovaný	k2eAgInSc2d1	syntetizovaný
zvuku	zvuk	k1gInSc2	zvuk
velrybích	velrybí	k2eAgFnPc2d1	velrybí
písní	píseň	k1gFnPc2	píseň
hraných	hraný	k2eAgFnPc2d1	hraná
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
společně	společně	k6eAd1	společně
s	s	k7c7	s
krákáním	krákání	k1gNnSc7	krákání
vran	vrána	k1gFnPc2	vrána
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Echoes	Echoes	k1gMnSc1	Echoes
<g/>
"	"	kIx"	"
popsal	popsat	k5eAaPmAgMnS	popsat
Waters	Waters	k1gInSc4	Waters
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zhudebněnou	zhudebněný	k2eAgFnSc4d1	zhudebněná
poému	poéma	k1gFnSc4	poéma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Meddle	Meddle	k1gFnSc1	Meddle
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Nicka	nicka	k1gFnSc1	nicka
Masona	mason	k1gMnSc2	mason
"	"	kIx"	"
<g/>
prvním	první	k4xOgMnSc6	první
skutečným	skutečný	k2eAgNnSc7d1	skutečné
albem	album	k1gNnSc7	album
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
nám	my	k3xPp1nPc3	my
myšlenku	myšlenka	k1gFnSc4	myšlenka
tématu	téma	k1gNnSc3	téma
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
vracet	vracet	k5eAaImF	vracet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
mělo	mít	k5eAaImAgNnS	mít
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
styl	styl	k1gInSc4	styl
nastupující	nastupující	k2eAgFnSc2d1	nastupující
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
éry	éra	k1gFnSc2	éra
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neobsahovalo	obsahovat	k5eNaImAgNnS	obsahovat
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
význačný	význačný	k2eAgMnSc1d1	význačný
u	u	k7c2	u
alba	album	k1gNnSc2	album
Atom	atom	k1gInSc1	atom
Heart	Hearta	k1gFnPc2	Hearta
Mother	Mothra	k1gFnPc2	Mothra
<g/>
.	.	kIx.	.
</s>
<s>
Meddle	Meddle	k6eAd1	Meddle
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
atmosférickou	atmosférický	k2eAgFnSc4d1	atmosférická
"	"	kIx"	"
<g/>
One	One	k1gFnSc4	One
of	of	k?	of
These	these	k1gFnSc2	these
Days	Daysa	k1gFnPc2	Daysa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
koncertní	koncertní	k2eAgFnSc4d1	koncertní
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Mason	mason	k1gMnSc1	mason
odříkává	odříkávat	k5eAaImIp3nS	odříkávat
jediný	jediný	k2eAgInSc4d1	jediný
text	text	k1gInSc4	text
skladby	skladba	k1gFnPc4	skladba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
of	of	k?	of
these	these	k1gFnSc1	these
days	days	k6eAd1	days
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
cut	cut	k?	cut
you	you	k?	you
into	into	k6eAd1	into
little	littlat	k5eAaPmIp3nS	littlat
pieces	pieces	k1gInSc1	pieces
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
tě	ty	k3xPp2nSc4	ty
rozsekám	rozsekat	k5eAaPmIp1nS	rozsekat
na	na	k7c4	na
malé	malý	k2eAgInPc4d1	malý
kousíčky	kousíček	k1gInPc4	kousíček
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skladbě	skladba	k1gFnSc6	skladba
nachází	nacházet	k5eAaImIp3nS	nacházet
výrazná	výrazný	k2eAgFnSc1d1	výrazná
bluesová	bluesový	k2eAgFnSc1d1	bluesová
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
melodie	melodie	k1gFnSc1	melodie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
přejde	přejít	k5eAaPmIp3nS	přejít
ve	v	k7c4	v
strojové	strojový	k2eAgNnSc4d1	strojové
pulsování	pulsování	k1gNnSc4	pulsování
odkazující	odkazující	k2eAgNnSc4d1	odkazující
na	na	k7c4	na
ústřední	ústřední	k2eAgFnSc4d1	ústřední
melodii	melodie	k1gFnSc4	melodie
z	z	k7c2	z
populárního	populární	k2eAgNnSc2d1	populární
sci-fi	scii	k1gNnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
Pán	pán	k1gMnSc1	pán
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jemný	jemný	k2eAgInSc1d1	jemný
feeling	feeling	k1gInSc1	feeling
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Fearless	Fearless	k1gInSc1	Fearless
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
patrný	patrný	k2eAgMnSc1d1	patrný
na	na	k7c6	na
následujících	následující	k2eAgNnPc6d1	následující
třech	tři	k4xCgNnPc6	tři
albech	album	k1gNnPc6	album
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Fearless	Fearless	k1gInSc1	Fearless
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
country	country	k2eAgFnSc7d1	country
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
v	v	k7c6	v
"	"	kIx"	"
<g/>
A	a	k9	a
Pillow	Pillow	k1gFnSc1	Pillow
of	of	k?	of
Winds	Winds	k1gInSc1	Winds
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
zamilovaných	zamilovaný	k2eAgFnPc2d1	zamilovaná
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
kdy	kdy	k6eAd1	kdy
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc4	Floyd
složili	složit	k5eAaPmAgMnP	složit
<g/>
.	.	kIx.	.
</s>
<s>
Watersova	Watersův	k2eAgFnSc1d1	Watersova
role	role	k1gFnSc1	role
jako	jako	k8xS	jako
hlavního	hlavní	k2eAgMnSc2d1	hlavní
textaře	textař	k1gMnSc2	textař
zde	zde	k6eAd1	zde
začala	začít	k5eAaPmAgFnS	začít
nabývat	nabývat	k5eAaImF	nabývat
obrysů	obrys	k1gInPc2	obrys
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jazzovou	jazzový	k2eAgFnSc4d1	jazzová
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
San	San	k1gMnSc1	San
Tropez	Tropez	k1gMnSc1	Tropez
<g/>
"	"	kIx"	"
přinesl	přinést	k5eAaPmAgInS	přinést
prakticky	prakticky	k6eAd1	prakticky
hotovou	hotový	k2eAgFnSc4d1	hotová
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
Meddle	Meddle	k1gFnPc2	Meddle
byla	být	k5eAaImAgFnS	být
nadšeně	nadšeně	k6eAd1	nadšeně
přijata	přijat	k2eAgFnSc1d1	přijata
jak	jak	k8xC	jak
kritiky	kritik	k1gMnPc4	kritik
<g/>
,	,	kIx,	,
tak	tak	k9	tak
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
70	[number]	k4	70
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
Obscured	Obscured	k1gInSc1	Obscured
by	by	kYmCp3nS	by
Clouds	Clouds	k1gInSc4	Clouds
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
jako	jako	k8xS	jako
soundtrack	soundtrack	k1gInSc1	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
La	la	k1gNnSc2	la
Vallée	Vallé	k1gFnSc2	Vallé
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
další	další	k2eAgInSc4d1	další
klubový	klubový	k2eAgInSc4d1	klubový
film	film	k1gInSc4	film
režiséra	režisér	k1gMnSc2	režisér
Barbeta	Barbet	k1gMnSc2	Barbet
Schroedera	Schroeder	k1gMnSc2	Schroeder
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
Top	topit	k5eAaImRp2nS	topit
50	[number]	k4	50
(	(	kIx(	(
<g/>
46	[number]	k4	46
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Mason	mason	k1gMnSc1	mason
později	pozdě	k6eAd2	pozdě
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
senzačním	senzační	k2eAgInSc7d1	senzační
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kritici	kritik	k1gMnPc1	kritik
už	už	k9	už
takové	takový	k3xDgNnSc4	takový
nadšení	nadšení	k1gNnSc4	nadšení
nesdíleli	sdílet	k5eNaImAgMnP	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Free	Free	k1gFnSc1	Free
Four	Four	k1gMnSc1	Four
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
hrána	hrát	k5eAaImNgFnS	hrát
v	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
rádiích	rádio	k1gNnPc6	rádio
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Watersovo	Watersův	k2eAgNnSc1d1	Watersovo
přemýšlení	přemýšlení	k1gNnSc1	přemýšlení
nad	nad	k7c7	nad
smrtí	smrt	k1gFnSc7	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
motiv	motiv	k1gInSc4	motiv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
albech	album	k1gNnPc6	album
skupiny	skupina	k1gFnSc2	skupina
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
i	i	k8xC	i
sólové	sólový	k2eAgFnSc6d1	sólová
kariéře	kariéra	k1gFnSc6	kariéra
Rogera	Rogero	k1gNnSc2	Rogero
Waterse	Waterse	k1gFnSc2	Waterse
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Wot	Wot	k1gFnSc1	Wot
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
...	...	k?	...
Uh	uh	k0	uh
The	The	k1gMnSc5	The
Deal	Dealum	k1gNnPc2	Dealum
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Childhood	Childhood	k1gInSc1	Childhood
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
End	End	k1gFnSc7	End
<g/>
"	"	kIx"	"
také	také	k6eAd1	také
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
témata	téma	k1gNnPc1	téma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
albech	album	k1gNnPc6	album
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
osamění	osamění	k1gNnSc4	osamění
a	a	k8xC	a
zoufalství	zoufalství	k1gNnSc4	zoufalství
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
téma	téma	k1gNnSc1	téma
bylo	být	k5eAaImAgNnS	být
plně	plně	k6eAd1	plně
rozvinuto	rozvinout	k5eAaPmNgNnS	rozvinout
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
Rogera	Rogero	k1gNnSc2	Rogero
Waterse	Waterse	k1gFnSc2	Waterse
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
albu	album	k1gNnSc6	album
-	-	kIx~	-
téma	téma	k1gNnSc1	téma
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
plynutí	plynutí	k1gNnSc2	plynutí
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Childhood	Childhood	k1gInSc1	Childhood
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
End	End	k1gFnSc7	End
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
novely	novela	k1gFnSc2	novela
Arthura	Arthura	k1gFnSc1	Arthura
C.	C.	kA	C.
Clarka	Clarka	k1gFnSc1	Clarka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
také	také	k9	také
posledním	poslední	k2eAgInSc7d1	poslední
textařským	textařský	k2eAgInSc7d1	textařský
příspěvkem	příspěvek	k1gInSc7	příspěvek
Davida	David	k1gMnSc2	David
Gilmoura	Gilmour	k1gMnSc2	Gilmour
na	na	k7c4	na
následujících	následující	k2eAgNnPc2d1	následující
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
stylově	stylově	k6eAd1	stylově
jiné	jiný	k2eAgFnPc4d1	jiná
než	než	k8xS	než
předešlé	předešlý	k2eAgFnPc4d1	předešlá
Meddle	Meddle	k1gFnPc4	Meddle
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
zabíhá	zabíhat	k5eAaImIp3nS	zabíhat
do	do	k7c2	do
blues-rocku	bluesock	k1gInSc2	blues-rock
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Gold	Gold	k1gMnSc1	Gold
It	It	k1gMnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
In	In	k1gMnSc7	In
the	the	k?	the
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
folk	folk	k1gInSc1	folk
rocku	rock	k1gInSc2	rock
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Burnin	Burnin	k2eAgInSc4d1	Burnin
<g/>
'	'	kIx"	'
Bridges	Bridges	k1gInSc4	Bridges
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
rocku	rock	k1gInSc2	rock
s	s	k7c7	s
dominantní	dominantní	k2eAgFnSc7d1	dominantní
klavírní	klavírní	k2eAgFnSc7d1	klavírní
složkou	složka	k1gFnSc7	složka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Stay	Sta	k2eAgFnPc1d1	Sta
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
režisér	režisér	k1gMnSc1	režisér
Adrian	Adrian	k1gMnSc1	Adrian
Maben	Maben	k1gInSc4	Maben
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgInSc4	první
koncertní	koncertní	k2eAgInSc4d1	koncertní
film	film	k1gInSc4	film
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
záběry	záběr	k1gInPc4	záběr
skupiny	skupina	k1gFnSc2	skupina
hrající	hrající	k2eAgInPc4d1	hrající
v	v	k7c6	v
antickém	antický	k2eAgInSc6d1	antický
amfiteátru	amfiteátr	k1gInSc6	amfiteátr
v	v	k7c6	v
Pompejích	Pompeje	k1gFnPc6	Pompeje
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
filmovým	filmový	k2eAgInSc7d1	filmový
štábem	štáb	k1gInSc7	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Maben	Maben	k2eAgMnSc1d1	Maben
také	také	k9	také
nahrál	nahrát	k5eAaPmAgMnS	nahrát
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
náhledy	náhled	k1gInPc4	náhled
do	do	k7c2	do
zákulisí	zákulisí	k1gNnSc2	zákulisí
během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
alba	album	k1gNnSc2	album
The	The	k1gMnSc2	The
Dark	Darko	k1gNnPc2	Darko
Side	Side	k1gNnPc2	Side
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
v	v	k7c4	v
Abbey	Abbea	k1gFnPc4	Abbea
Road	Roada	k1gFnPc2	Roada
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
časové	časový	k2eAgNnSc1d1	časové
pořadí	pořadí	k1gNnSc1	pořadí
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
scén	scéna	k1gFnPc2	scéna
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
až	až	k9	až
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
náhledy	náhled	k1gInPc1	náhled
do	do	k7c2	do
zákulisí	zákulisí	k1gNnSc2	zákulisí
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
vydání	vydání	k1gNnPc2	vydání
Live	Liv	k1gFnSc2	Liv
at	at	k?	at
Pompeii	Pompeie	k1gFnSc4	Pompeie
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
celosvětově	celosvětově	k6eAd1	celosvětově
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
alba	album	k1gNnSc2	album
The	The	k1gMnSc2	The
Dark	Darko	k1gNnPc2	Darko
Side	Side	k1gNnPc2	Side
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gMnSc1	Moon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
předělovým	předělový	k2eAgInSc7d1	předělový
momentem	moment	k1gInSc7	moment
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
přestali	přestat	k5eAaPmAgMnP	přestat
po	po	k7c6	po
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Point	pointa	k1gFnPc2	pointa
Me	Me	k1gFnSc2	Me
at	at	k?	at
the	the	k?	the
Sky	Sky	k1gFnSc1	Sky
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
vydávat	vydávat	k5eAaPmF	vydávat
singly	singl	k1gInPc4	singl
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
)	)	kIx)	)
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nebyli	být	k5eNaImAgMnP	být
skupinou	skupina	k1gFnSc7	skupina
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Money	Money	k1gInPc1	Money
<g/>
"	"	kIx"	"
z	z	k7c2	z
The	The	k1gFnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moona	k1gFnPc2	Moona
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Top	topit	k5eAaImRp2nS	topit
20	[number]	k4	20
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
albu	album	k1gNnSc3	album
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
první	první	k4xOgNnPc4	první
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
,	,	kIx,	,
The	The	k1gFnSc6	The
Dark	Darko	k1gNnPc2	Darko
Side	Sid	k1gMnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc4	Moon
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
prodávaných	prodávaný	k2eAgFnPc2d1	prodávaná
alb	alba	k1gFnPc2	alba
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
třetí	třetí	k4xOgNnSc1	třetí
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
album	album	k1gNnSc1	album
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
45	[number]	k4	45
milionů	milion	k4xCgInPc2	milion
nosičů	nosič	k1gInPc2	nosič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kritiky	kritika	k1gFnPc1	kritika
vychvalované	vychvalovaný	k2eAgNnSc1d1	vychvalované
album	album	k1gNnSc1	album
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Top	topit	k5eAaImRp2nS	topit
200	[number]	k4	200
nebývalých	nebývalý	k2eAgInPc2d1	nebývalý
741	[number]	k4	741
týdnů	týden	k1gInPc2	týden
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
591	[number]	k4	591
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
do	do	k7c2	do
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
301	[number]	k4	301
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nikdy	nikdy	k6eAd1	nikdy
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
saxofon	saxofon	k1gInSc4	saxofon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
nechala	nechat	k5eAaPmAgFnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
jazzem	jazz	k1gInSc7	jazz
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ženské	ženský	k2eAgInPc1d1	ženský
vokály	vokál	k1gInPc1	vokál
pomáhající	pomáhající	k2eAgInPc1d1	pomáhající
rozšířit	rozšířit	k5eAaPmF	rozšířit
hudební	hudební	k2eAgFnSc4d1	hudební
strukturu	struktura	k1gFnSc4	struktura
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jak	jak	k6eAd1	jak
přímé	přímý	k2eAgFnPc4d1	přímá
rockové	rockový	k2eAgFnPc4d1	rocková
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Money	Mone	k2eAgMnPc4d1	Mone
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Time	Time	k1gFnSc1	Time
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vokálně-instrumentální	vokálněnstrumentální	k2eAgFnSc4d1	vokálně-instrumentální
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
The	The	k1gFnSc7	The
Great	Great	k2eAgInSc4d1	Great
Gig	gig	k1gInSc4	gig
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gFnSc1	Sky
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zpívá	zpívat	k5eAaImIp3nS	zpívat
Clare	Clar	k1gInSc5	Clar
Torryová	Torryová	k1gFnSc1	Torryová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
minimalistickou	minimalistický	k2eAgFnSc4d1	minimalistická
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Run	run	k1gInSc1	run
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
hrána	hrát	k5eAaImNgFnS	hrát
skoro	skoro	k6eAd1	skoro
celá	celý	k2eAgFnSc1d1	celá
na	na	k7c4	na
syntezátor	syntezátor	k1gInSc4	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Náhodné	náhodný	k2eAgInPc1d1	náhodný
zvukové	zvukový	k2eAgInPc1d1	zvukový
efekty	efekt	k1gInPc1	efekt
a	a	k8xC	a
útržky	útržek	k1gInPc1	útržek
rozhovorů	rozhovor	k1gInPc2	rozhovor
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
celým	celý	k2eAgInSc7d1	celý
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Watersova	Watersův	k2eAgNnPc1d1	Watersovo
interview	interview	k1gNnPc1	interview
začínala	začínat	k5eAaImAgNnP	začínat
otázkou	otázka	k1gFnSc7	otázka
"	"	kIx"	"
<g/>
Jaká	jaký	k3yRgFnSc1	jaký
je	být	k5eAaImIp3nS	být
vaše	váš	k3xOp2gFnSc1	váš
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
barva	barva	k1gFnSc1	barva
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tázaný	tázaný	k2eAgMnSc1d1	tázaný
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zeptal	zeptat	k5eAaPmAgMnS	zeptat
"	"	kIx"	"
<g/>
Kdy	kdy	k6eAd1	kdy
jste	být	k5eAaImIp2nP	být
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
choval	chovat	k5eAaImAgMnS	chovat
násilně	násilně	k6eAd1	násilně
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
byl	být	k5eAaImAgMnS	být
jste	být	k5eAaImIp2nP	být
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Odpověď	odpověď	k1gFnSc1	odpověď
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c6	na
albu	album	k1gNnSc6	album
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Money	Mone	k2eAgFnPc1d1	Mone
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
rozhovoru	rozhovor	k1gInSc6	rozhovor
se	se	k3xPyFc4	se
zase	zase	k9	zase
ptal	ptat	k5eAaImAgInS	ptat
"	"	kIx"	"
<g/>
Bojíte	bát	k5eAaImIp2nP	bát
se	se	k3xPyFc4	se
smrti	smrt	k1gFnSc2	smrt
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
a	a	k8xC	a
zvuk	zvuk	k1gInSc1	zvuk
alba	album	k1gNnSc2	album
popisuje	popisovat	k5eAaImIp3nS	popisovat
tlaky	tlak	k1gInPc4	tlak
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
světě	svět	k1gInSc6	svět
vystaven	vystavit	k5eAaPmNgInS	vystavit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
(	(	kIx(	(
<g/>
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
při	při	k7c6	při
poradě	porada	k1gFnSc6	porada
skupiny	skupina	k1gFnSc2	skupina
u	u	k7c2	u
Masonova	masonův	k2eAgInSc2d1	masonův
kuchyňského	kuchyňský	k2eAgInSc2d1	kuchyňský
stolu	stol	k1gInSc2	stol
<g/>
)	)	kIx)	)
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
kapelu	kapela	k1gFnSc4	kapela
a	a	k8xC	a
společně	společně	k6eAd1	společně
napsali	napsat	k5eAaPmAgMnP	napsat
několik	několik	k4yIc4	několik
motivů	motiv	k1gInPc2	motiv
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
Waters	Watersa	k1gFnPc2	Watersa
poté	poté	k6eAd1	poté
využil	využít	k5eAaPmAgInS	využít
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
albech	album	k1gNnPc6	album
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Us	Us	k1gMnSc1	Us
and	and	k?	and
Them	Them	k1gMnSc1	Them
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc4	píseň
o	o	k7c4	o
násilí	násilí	k1gNnSc4	násilí
a	a	k8xC	a
zbytečnosti	zbytečnost	k1gFnPc4	zbytečnost
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
motiv	motiv	k1gInSc4	motiv
šílenství	šílenství	k1gNnSc2	šílenství
a	a	k8xC	a
neurózy	neuróza	k1gFnSc2	neuróza
v	v	k7c4	v
"	"	kIx"	"
<g/>
Brain	Brain	k2eAgInSc4d1	Brain
Damage	Damage	k1gInSc4	Damage
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Složitá	složitý	k2eAgFnSc1d1	složitá
a	a	k8xC	a
precizní	precizní	k2eAgFnSc1d1	precizní
výstavba	výstavba	k1gFnSc1	výstavba
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
zásluhou	zásluhou	k7c2	zásluhou
Alana	Alan	k1gMnSc2	Alan
Parsonse	Parsons	k1gMnSc2	Parsons
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tímto	tento	k3xDgNnSc7	tento
stanovil	stanovit	k5eAaPmAgInS	stanovit
nový	nový	k2eAgInSc4d1	nový
standard	standard	k1gInSc4	standard
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
aspekt	aspekt	k1gInSc1	aspekt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
poté	poté	k6eAd1	poté
typickým	typický	k2eAgInSc7d1	typický
rysem	rys	k1gInSc7	rys
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pomohl	pomoct	k5eAaPmAgMnS	pomoct
v	v	k7c6	v
prodejnosti	prodejnost	k1gFnSc6	prodejnost
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chtiví	chtivý	k2eAgMnPc1d1	chtivý
zvukofilové	zvukofil	k1gMnPc1	zvukofil
si	se	k3xPyFc3	se
po	po	k7c4	po
"	"	kIx"	"
<g/>
ojetí	ojetí	k1gNnSc4	ojetí
<g/>
"	"	kIx"	"
desky	deska	k1gFnPc4	deska
běželi	běžet	k5eAaImAgMnP	běžet
koupit	koupit	k5eAaPmF	koupit
novou	nový	k2eAgFnSc4d1	nová
kopii	kopie	k1gFnSc4	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zúročit	zúročit	k5eAaPmF	zúročit
svou	svůj	k3xOyFgFnSc4	svůj
nabytou	nabytý	k2eAgFnSc4d1	nabytá
popularitu	popularita	k1gFnSc4	popularita
vydali	vydat	k5eAaPmAgMnP	vydat
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
výběrové	výběrový	k2eAgNnSc1d1	výběrové
dvojalbum	dvojalbum	k1gNnSc4	dvojalbum
A	a	k8xC	a
Nice	Nice	k1gFnSc4	Nice
Pair	pair	k1gMnSc1	pair
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
znovuvydané	znovuvydaný	k2eAgFnPc4d1	znovuvydaný
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
desky	deska	k1gFnPc4	deska
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Piper	Piper	k1gMnSc1	Piper
at	at	k?	at
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
Dawn	Dawn	k1gMnSc1	Dawn
a	a	k8xC	a
A	a	k9	a
Saucerful	Saucerful	k1gInSc1	Saucerful
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Wish	Wisha	k1gFnPc2	Wisha
You	You	k1gFnSc2	You
Were	Were	k1gFnSc7	Were
Here	Her	k1gInPc4	Her
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc4d1	vydaný
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
motiv	motiv	k1gInSc4	motiv
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnosti	nepřítomnost	k1gFnPc1	nepřítomnost
lidskosti	lidskost	k1gFnSc2	lidskost
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
nejvýrazněji	výrazně	k6eAd3	výrazně
nepřítomnosti	nepřítomnost	k1gFnPc1	nepřítomnost
Syda	Sydus	k1gMnSc2	Sydus
Barretta	Barrett	k1gMnSc2	Barrett
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
známé	známý	k2eAgFnSc2d1	známá
díky	díky	k7c3	díky
stejnojmenné	stejnojmenný	k2eAgFnSc3d1	stejnojmenná
písni	píseň	k1gFnSc3	píseň
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Shine	Shin	k1gMnSc5	Shin
On	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
Crazy	Craza	k1gFnSc2	Craza
Diamond	Diamond	k1gMnSc1	Diamond
<g/>
"	"	kIx"	"
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
devíti	devět	k4xCc2	devět
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
složena	složit	k5eAaPmNgFnS	složit
jako	jako	k9	jako
pocta	pocta	k1gFnSc1	pocta
Sydu	Syd	k1gMnSc3	Syd
Barretovi	Barret	k1gMnSc3	Barret
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
hudebních	hudební	k2eAgInPc2d1	hudební
vlivů	vliv	k1gInPc2	vliv
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
spojilo	spojit	k5eAaPmAgNnS	spojit
dohromady	dohromady	k6eAd1	dohromady
-	-	kIx~	-
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
bluesová	bluesový	k2eAgFnSc1d1	bluesová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
saxofonová	saxofonový	k2eAgNnPc1d1	saxofonové
sóla	sólo	k1gNnPc1	sólo
(	(	kIx(	(
<g/>
hrána	hrát	k5eAaImNgFnS	hrát
Dickem	Dicek	k1gInSc7	Dicek
Parrym	Parrymum	k1gNnPc2	Parrymum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzové	jazzový	k2eAgInPc1d1	jazzový
prvky	prvek	k1gInPc1	prvek
a	a	k8xC	a
agresivní	agresivní	k2eAgFnSc1d1	agresivní
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
navzájem	navzájem	k6eAd1	navzájem
spojených	spojený	k2eAgFnPc2d1	spojená
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
žalozpěvem	žalozpěv	k1gInSc7	žalozpěv
hraným	hraný	k2eAgInSc7d1	hraný
na	na	k7c4	na
syntetický	syntetický	k2eAgInSc4d1	syntetický
roh	roh	k1gInSc4	roh
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
hudební	hudební	k2eAgFnSc7d1	hudební
citací	citace	k1gFnSc7	citace
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
singlů	singl	k1gInPc2	singl
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
See	See	k1gMnSc1	See
Emily	Emil	k1gMnPc7	Emil
Play	play	k0	play
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
poslední	poslední	k2eAgFnSc7d1	poslední
poctou	pocta	k1gFnSc7	pocta
Sydu	Syda	k1gFnSc4	Syda
Barrettovi	Barrett	k1gMnSc3	Barrett
<g/>
,	,	kIx,	,
vůdci	vůdce	k1gMnPc1	vůdce
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
počátcích	počátek	k1gInPc6	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Wish	Wish	k1gInSc1	Wish
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
věnována	věnovat	k5eAaImNgFnS	věnovat
Barrettovi	Barrettův	k2eAgMnPc1d1	Barrettův
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnPc1d1	zbývající
dvě	dva	k4xCgFnPc4	dva
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Machine	Machin	k1gInSc5	Machin
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Have	Have	k1gFnSc1	Have
a	a	k8xC	a
Cigar	Cigar	k1gInSc1	Cigar
<g/>
"	"	kIx"	"
ostře	ostro	k6eAd1	ostro
napadají	napadat	k5eAaPmIp3nP	napadat
hudební	hudební	k2eAgInSc4d1	hudební
průmysl	průmysl	k1gInSc4	průmysl
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
zpívaná	zpívaná	k1gFnSc1	zpívaná
britským	britský	k2eAgMnSc7d1	britský
folkovým	folkový	k2eAgMnSc7d1	folkový
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Royem	Roy	k1gMnSc7	Roy
Harperem	Harper	k1gMnSc7	Harper
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
prvním	první	k4xOgMnSc6	první
album	album	k1gNnSc4	album
Pink	pink	k2eAgNnPc2d1	pink
Floyd	Floydo	k1gNnPc2	Floydo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
jak	jak	k6eAd1	jak
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
kritici	kritik	k1gMnPc1	kritik
jej	on	k3xPp3gMnSc4	on
chválili	chválit	k5eAaImAgMnP	chválit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
předchozí	předchozí	k2eAgFnSc3d1	předchozí
The	The	k1gFnSc3	The
Dark	Darka	k1gFnPc2	Darka
Side	Side	k1gNnSc2	Side
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
historka	historka	k1gFnSc1	historka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
během	během	k7c2	během
nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
Wish	Wish	k1gMnSc1	Wish
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
vešel	vejít	k5eAaPmAgInS	vejít
obtloustlý	obtloustlý	k2eAgMnSc1d1	obtloustlý
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
vyholenou	vyholený	k2eAgFnSc7d1	vyholená
hlavou	hlava	k1gFnSc7	hlava
i	i	k8xC	i
obočím	obočí	k1gNnSc7	obočí
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
ho	on	k3xPp3gMnSc4	on
chvíli	chvíle	k1gFnSc6	chvíle
nemohla	moct	k5eNaImAgNnP	moct
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
až	až	k8xS	až
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Syd	Syd	k1gMnSc1	Syd
Barrett	Barrett	k1gMnSc1	Barrett
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
neviděli	vidět	k5eNaImAgMnP	vidět
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Barrett	Barrett	k1gInSc1	Barrett
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
bez	bez	k7c2	bez
rozloučení	rozloučení	k1gNnSc2	rozloučení
definitivně	definitivně	k6eAd1	definitivně
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
získával	získávat	k5eAaImAgMnS	získávat
baskytarista	baskytarista	k1gMnSc1	baskytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Roger	Rogra	k1gFnPc2	Rogra
Waters	Watersa	k1gFnPc2	Watersa
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
produkcí	produkce	k1gFnSc7	produkce
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Wrightův	Wrightův	k2eAgInSc1d1	Wrightův
vliv	vliv	k1gInSc1	vliv
ustupoval	ustupovat	k5eAaImAgInS	ustupovat
silně	silně	k6eAd1	silně
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
a	a	k8xC	a
během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
byl	být	k5eAaImAgMnS	být
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
vyhozen	vyhozen	k2eAgMnSc1d1	vyhozen
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
skupiny	skupina	k1gFnSc2	skupina
stojí	stát	k5eAaImIp3nS	stát
často	často	k6eAd1	často
až	až	k9	až
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
za	za	k7c7	za
texty	text	k1gInPc7	text
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
Waters	Waters	k1gInSc1	Waters
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
své	svůj	k3xOyFgInPc4	svůj
pocity	pocit	k1gInPc4	pocit
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
otce	otec	k1gMnSc2	otec
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
narůstající	narůstající	k2eAgInSc4d1	narůstající
nenávistný	návistný	k2eNgInSc4d1	nenávistný
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
politickým	politický	k2eAgFnPc3d1	politická
osobám	osoba	k1gFnPc3	osoba
jako	jako	k8xC	jako
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
nebo	nebo	k8xC	nebo
Mary	Mary	k1gFnSc1	Mary
Whitehouseová	Whitehouseová	k1gFnSc1	Whitehouseová
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
stále	stále	k6eAd1	stále
s	s	k7c7	s
jemnými	jemný	k2eAgFnPc7d1	jemná
nuancemi	nuance	k1gFnPc7	nuance
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
syrovějším	syrový	k2eAgMnSc7d2	syrovější
a	a	k8xC	a
více	hodně	k6eAd2	hodně
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c6	na
kytaře	kytara	k1gFnSc6	kytara
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
saxofonu	saxofon	k1gInSc2	saxofon
a	a	k8xC	a
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
ustoupily	ustoupit	k5eAaPmAgFnP	ustoupit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
typické	typický	k2eAgInPc4d1	typický
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
Atom	atom	k1gInSc1	atom
Heart	Hearta	k1gFnPc2	Hearta
Mother	Mothra	k1gFnPc2	Mothra
<g/>
)	)	kIx)	)
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
na	na	k7c6	na
deskách	deska	k1gFnPc6	deska
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
a	a	k8xC	a
především	především	k6eAd1	především
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1977	[number]	k4	1977
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
Animals	Animalsa	k1gFnPc2	Animalsa
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
pod	pod	k7c4	pod
palbu	palba	k1gFnSc4	palba
kritiků	kritik	k1gMnPc2	kritik
z	z	k7c2	z
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
utvořivší	utvořivší	k2eAgFnSc2d1	utvořivší
punkové	punkový	k2eAgFnSc2d1	punková
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
albu	alba	k1gFnSc4	alba
vyčítali	vyčítat	k5eAaImAgMnP	vyčítat
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
nemastnost	nemastnost	k1gFnSc4	nemastnost
neslanost	neslanost	k1gFnSc1	neslanost
<g/>
,	,	kIx,	,
okázalost	okázalost	k1gFnSc1	okázalost
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdálilo	vzdálit	k5eAaPmAgNnS	vzdálit
od	od	k7c2	od
jednoduchosti	jednoduchost	k1gFnSc2	jednoduchost
klasického	klasický	k2eAgInSc2d1	klasický
rock	rock	k1gInSc4	rock
and	and	k?	and
rollu	roll	k1gInSc2	roll
<g/>
.	.	kIx.	.
</s>
<s>
Animals	Animals	k6eAd1	Animals
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
kytarovější	kytarový	k2eAgNnSc4d2	kytarovější
album	album	k1gNnSc4	album
než	než	k8xS	než
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
nastupující	nastupující	k2eAgMnSc1d1	nastupující
punk	punk	k1gMnSc1	punk
rockové	rockový	k2eAgFnSc2d1	rocková
scény	scéna	k1gFnSc2	scéna
anebo	anebo	k8xC	anebo
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
v	v	k7c6	v
novém	nový	k2eAgMnSc6d1	nový
(	(	kIx(	(
<g/>
a	a	k8xC	a
ne	ne	k9	ne
úplně	úplně	k6eAd1	úplně
dokončeném	dokončený	k2eAgNnSc6d1	dokončené
<g/>
)	)	kIx)	)
studiu	studio	k1gNnSc6	studio
Britannia	Britannium	k1gNnSc2	Britannium
Row	Row	k1gFnSc2	Row
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
prvním	první	k4xOgNnSc7	první
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
u	u	k7c2	u
žádné	žádný	k3yNgFnSc2	žádný
písně	píseň	k1gFnSc2	píseň
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jméno	jméno	k1gNnSc1	jméno
Ricka	Ricek	k1gMnSc2	Ricek
Wrighta	Wright	k1gMnSc2	Wright
<g/>
.	.	kIx.	.
</s>
<s>
Animals	Animals	k6eAd1	Animals
opět	opět	k6eAd1	opět
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
ústřednímu	ústřední	k2eAgNnSc3d1	ústřední
tématu	téma	k1gNnSc3	téma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
Farmou	farma	k1gFnSc7	farma
zvířat	zvíře	k1gNnPc2	zvíře
od	od	k7c2	od
George	Georg	k1gFnSc2	Georg
Orwella	Orwello	k1gNnSc2	Orwello
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
použil	použít	k5eAaPmAgMnS	použít
psy	pes	k1gMnPc4	pes
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dogs	Dogs	k1gInSc1	Dogs
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pigs	Pigs	k1gInSc1	Pigs
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ovce	ovce	k1gFnSc1	ovce
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sheep	Sheep	k1gInSc1	Sheep
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
metafory	metafora	k1gFnSc2	metafora
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
na	na	k7c6	na
albu	album	k1gNnSc6	album
dominuje	dominovat	k5eAaImIp3nS	dominovat
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc1	klávesa
a	a	k8xC	a
syntezátor	syntezátor	k1gInSc1	syntezátor
zde	zde	k6eAd1	zde
také	také	k9	také
hrají	hrát	k5eAaImIp3nP	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nedá	dát	k5eNaPmIp3nS	dát
říct	říct	k5eAaPmF	říct
o	o	k7c6	o
saxofonu	saxofon	k1gInSc6	saxofon
a	a	k8xC	a
ženských	ženský	k2eAgInPc6d1	ženský
vokálech	vokál	k1gInPc6	vokál
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
typických	typický	k2eAgFnPc2d1	typická
pro	pro	k7c4	pro
minulá	minulý	k2eAgNnPc4d1	Minulé
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
zde	zde	k6eAd1	zde
úplně	úplně	k6eAd1	úplně
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
hard	hard	k6eAd1	hard
rockový	rockový	k2eAgInSc4d1	rockový
zvuk	zvuk	k1gInSc4	zvuk
ohraničený	ohraničený	k2eAgInSc4d1	ohraničený
dvěma	dva	k4xCgFnPc7	dva
krátkými	krátký	k2eAgFnPc7d1	krátká
akustickými	akustický	k2eAgFnPc7d1	akustická
písněmi	píseň	k1gFnPc7	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kritiků	kritik	k1gMnPc2	kritik
se	se	k3xPyFc4	se
k	k	k7c3	k
albu	album	k1gNnSc3	album
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
negativně	negativně	k6eAd1	negativně
<g/>
,	,	kIx,	,
popisovali	popisovat	k5eAaImAgMnP	popisovat
jej	on	k3xPp3gNnSc4	on
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
ponuré	ponurý	k2eAgInPc4d1	ponurý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc1	album
chválilo	chválit	k5eAaImAgNnS	chválit
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgFnPc3	tento
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obal	obal	k1gInSc4	obal
desky	deska	k1gFnSc2	deska
byla	být	k5eAaImAgFnS	být
použita	použit	k2eAgFnSc1d1	použita
fotka	fotka	k1gFnSc1	fotka
obrovského	obrovský	k2eAgNnSc2d1	obrovské
nafukovacího	nafukovací	k2eAgNnSc2d1	nafukovací
prasete	prase	k1gNnSc2	prase
letícího	letící	k2eAgNnSc2d1	letící
mezi	mezi	k7c7	mezi
komíny	komín	k1gInPc7	komín
londýnské	londýnský	k2eAgFnSc2d1	londýnská
elektrárny	elektrárna	k1gFnSc2	elektrárna
Battersea	Batterse	k1gInSc2	Batterse
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
vítr	vítr	k1gInSc1	vítr
ale	ale	k8xC	ale
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
prasetem	prase	k1gNnSc7	prase
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
výsledný	výsledný	k2eAgInSc1d1	výsledný
obal	obal	k1gInSc1	obal
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
fotomontáž	fotomontáž	k1gFnSc4	fotomontáž
<g/>
.	.	kIx.	.
</s>
<s>
Prase	prase	k1gNnSc4	prase
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
holandský	holandský	k2eAgMnSc1d1	holandský
průmyslový	průmyslový	k2eAgMnSc1d1	průmyslový
designér	designér	k1gMnSc1	designér
a	a	k8xC	a
umělec	umělec	k1gMnSc1	umělec
Theo	Thea	k1gFnSc5	Thea
Botschuijver	Botschuijver	k1gInSc1	Botschuijver
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Pink	pink	k2eAgNnPc2d1	pink
Floyd	Floydo	k1gNnPc2	Floydo
a	a	k8xC	a
nafukovací	nafukovací	k2eAgNnPc4d1	nafukovací
prasata	prase	k1gNnPc4	prase
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
součástí	součást	k1gFnPc2	součást
každého	každý	k3xTgInSc2	každý
koncertu	koncert	k1gInSc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vyšla	vyjít	k5eAaPmAgFnS	vyjít
epická	epický	k2eAgFnSc1d1	epická
rocková	rockový	k2eAgFnSc1d1	rocková
opera	opera	k1gFnSc1	opera
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
téměř	téměř	k6eAd1	téměř
kompletně	kompletně	k6eAd1	kompletně
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Watersem	Waters	k1gInSc7	Waters
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
tématy	téma	k1gNnPc7	téma
osamocení	osamocení	k1gNnSc2	osamocení
a	a	k8xC	a
ztráty	ztráta	k1gFnSc2	ztráta
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
vyjádřeny	vyjádřit	k5eAaPmNgInP	vyjádřit
metaforicky	metaforicky	k6eAd1	metaforicky
jako	jako	k8xC	jako
zeď	zeď	k1gFnSc1	zeď
mezi	mezi	k7c7	mezi
umělcem	umělec	k1gMnSc7	umělec
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
navrátilo	navrátit	k5eAaPmAgNnS	navrátit
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc1	Floyd
jejich	jejich	k3xOp3gInSc4	jejich
pošramocený	pošramocený	k2eAgInSc4d1	pošramocený
lesk	lesk	k1gInSc4	lesk
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
dodalo	dodat	k5eAaPmAgNnS	dodat
další	další	k2eAgInSc4d1	další
hitový	hitový	k2eAgInSc4d1	hitový
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Another	Anothra	k1gFnPc2	Anothra
Brick	Bricko	k1gNnPc2	Bricko
in	in	k?	in
the	the	k?	the
Wall	Wall	k1gInSc1	Wall
Part	part	k1gInSc1	part
2	[number]	k4	2
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Wall	Wall	k1gInSc1	Wall
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
budoucí	budoucí	k2eAgInPc4d1	budoucí
koncertní	koncertní	k2eAgInPc4d1	koncertní
vrcholy	vrchol	k1gInPc4	vrchol
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Comfortably	Comfortably	k1gFnSc1	Comfortably
Numb	Numb	k1gMnSc1	Numb
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
klasických	klasický	k2eAgNnPc2d1	klasické
rockových	rockový	k2eAgNnPc2d1	rockové
rádií	rádio	k1gNnPc2	rádio
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
písní	píseň	k1gFnPc2	píseň
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
Run	run	k1gInSc1	run
Like	Like	k1gNnSc1	Like
Hell	Hell	k1gInSc1	Hell
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
koprodukoval	koprodukovat	k5eAaBmAgMnS	koprodukovat
Bob	Bob	k1gMnSc1	Bob
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
,	,	kIx,	,
Watersův	Watersův	k2eAgMnSc1d1	Watersův
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
pod	pod	k7c7	pod
textem	text	k1gInSc7	text
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Trial	trial	k1gInSc1	trial
<g/>
"	"	kIx"	"
a	a	k8xC	a
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
Waters	Waters	k1gInSc1	Waters
distancoval	distancovat	k5eAaBmAgInS	distancovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
Ezrin	Ezrin	k1gMnSc1	Ezrin
"	"	kIx"	"
<g/>
pustil	pustit	k5eAaPmAgMnS	pustit
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
pusu	pusa	k1gFnSc4	pusa
na	na	k7c4	na
špacír	špacír	k?	špacír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Waters	Waters	k1gInSc1	Waters
začal	začít	k5eAaPmAgInS	začít
využívat	využívat	k5eAaPmF	využívat
svého	svůj	k3xOyFgInSc2	svůj
dominantního	dominantní	k2eAgInSc2d1	dominantní
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
vůdcovství	vůdcovství	k1gNnSc2	vůdcovství
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
u	u	k7c2	u
Animals	Animalsa	k1gFnPc2	Animalsa
a	a	k8xC	a
také	také	k9	také
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgMnS	začít
přivlastňovat	přivlastňovat	k5eAaImF	přivlastňovat
většinu	většina	k1gFnSc4	většina
výdělků	výdělek	k1gInPc2	výdělek
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
definitivně	definitivně	k6eAd1	definitivně
do	do	k7c2	do
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
orchestrální	orchestrální	k2eAgNnSc1d1	orchestrální
těleso	těleso	k1gNnSc1	těleso
připomíná	připomínat	k5eAaImIp3nS	připomínat
minulost	minulost	k1gFnSc4	minulost
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
několik	několik	k4yIc1	několik
klidnějších	klidný	k2eAgFnPc2d2	klidnější
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Goodbye	Goodbye	k1gFnSc1	Goodbye
<g/>
,	,	kIx,	,
Blue	Blue	k1gFnSc1	Blue
Sky	Sky	k1gFnSc1	Sky
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vera	Ver	k2eAgNnPc4d1	Ver
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Nobody	Noboda	k1gFnSc2	Noboda
Home	Hom	k1gFnSc2	Hom
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wrightův	Wrightův	k2eAgInSc1d1	Wrightův
vliv	vliv	k1gInSc1	vliv
byl	být	k5eAaImAgInS	být
kompletně	kompletně	k6eAd1	kompletně
zminimalizován	zminimalizovat	k5eAaImNgInS	zminimalizovat
<g/>
,	,	kIx,	,
až	až	k9	až
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
alba	album	k1gNnSc2	album
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
a	a	k8xC	a
na	na	k7c6	na
následujících	následující	k2eAgInPc6d1	následující
koncertech	koncert	k1gInPc6	koncert
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
najatý	najatý	k2eAgMnSc1d1	najatý
klávesista	klávesista	k1gMnSc1	klávesista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ironií	ironie	k1gFnSc7	ironie
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
Wright	Wright	k1gMnSc1	Wright
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vydělal	vydělat	k5eAaPmAgMnS	vydělat
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
The	The	k1gFnSc1	The
Wall	Wall	k1gInSc1	Wall
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnSc2d1	ostatní
se	se	k3xPyFc4	se
potýkali	potýkat	k5eAaImAgMnP	potýkat
s	s	k7c7	s
obrovskými	obrovský	k2eAgInPc7d1	obrovský
výdaji	výdaj	k1gInPc7	výdaj
nejpropracovanějšího	propracovaný	k2eAgNnSc2d3	nejpropracovanější
koncertního	koncertní	k2eAgNnSc2d1	koncertní
ztvárnění	ztvárnění	k1gNnSc2	ztvárnění
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
The	The	k1gMnPc1	The
Wall	Walla	k1gFnPc2	Walla
nikdy	nikdy	k6eAd1	nikdy
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
(	(	kIx(	(
<g/>
deska	deska	k1gFnSc1	deska
byla	být	k5eAaImAgFnS	být
nejlépe	dobře	k6eAd3	dobře
třetí	třetí	k4xOgMnSc1	třetí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strávilo	strávit	k5eAaPmAgNnS	strávit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
15	[number]	k4	15
týdnů	týden	k1gInPc2	týden
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
americké	americký	k2eAgFnSc2d1	americká
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
dílo	dílo	k1gNnSc4	dílo
chválili	chválit	k5eAaImAgMnP	chválit
<g/>
,	,	kIx,	,
komerčně	komerčně	k6eAd1	komerčně
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
desek	deska	k1gFnPc2	deska
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
23	[number]	k4	23
milionů	milion	k4xCgInPc2	milion
jen	jen	k6eAd1	jen
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
23	[number]	k4	23
<g/>
×	×	k?	×
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
RIAA	RIAA	kA	RIAA
platinovým	platinový	k2eAgFnPc3d1	platinová
<g/>
.	.	kIx.	.
</s>
<s>
Obrovský	obrovský	k2eAgInSc1d1	obrovský
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
Pink	pink	k2eAgMnSc2d1	pink
Floyd	Floyd	k1gInSc4	Floyd
druhou	druhý	k4xOgFnSc4	druhý
kapelu	kapela	k1gFnSc4	kapela
po	po	k7c6	po
The	The	k1gFnSc6	The
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
desetiletí	desetiletí	k1gNnSc6	desetiletí
2	[number]	k4	2
nejprodávanější	prodávaný	k2eAgFnPc4d3	nejprodávanější
desky	deska	k1gFnPc4	deska
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Dark	Dark	k1gInSc1	Dark
Side	Side	k1gFnSc1	Side
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gMnSc1	Moon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
a	a	k8xC	a
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
byl	být	k5eAaImAgMnS	být
promítán	promítat	k5eAaImNgMnS	promítat
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
napsal	napsat	k5eAaBmAgInS	napsat
Waters	Waters	k1gInSc1	Waters
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
režírován	režírovat	k5eAaImNgInS	režírovat
Alanem	Alan	k1gMnSc7	Alan
Parkerem	Parker	k1gMnSc7	Parker
a	a	k8xC	a
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
leader	leader	k1gMnSc1	leader
skupiny	skupina	k1gFnSc2	skupina
Booomtown	Booomtown	k1gMnSc1	Booomtown
Rats	Ratsa	k1gFnPc2	Ratsa
Bob	Bob	k1gMnSc1	Bob
Geldof	Geldof	k1gMnSc1	Geldof
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přezpíval	přezpívat	k5eAaPmAgMnS	přezpívat
některé	některý	k3yIgFnPc4	některý
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
animace	animace	k1gFnSc2	animace
Geralda	Gerald	k1gMnSc2	Gerald
Scarfa	Scarf	k1gMnSc2	Scarf
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgMnSc1d1	filmový
kritik	kritik	k1gMnSc1	kritik
Leonard	Leonard	k1gMnSc1	Leonard
Maltin	Maltin	k1gMnSc1	Maltin
nazval	nazvat	k5eAaPmAgMnS	nazvat
film	film	k1gInSc4	film
"	"	kIx"	"
<g/>
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
a	a	k8xC	a
nejdepresivnějším	depresivní	k2eAgInSc7d3	nejdepresivnější
rockovým	rockový	k2eAgInSc7d1	rockový
klipem	klip	k1gInSc7	klip
historie	historie	k1gFnSc2	historie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hitem	hit	k1gInSc7	hit
-	-	kIx~	-
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
USA	USA	kA	USA
nechali	nechat	k5eAaPmAgMnP	nechat
u	u	k7c2	u
pokladen	pokladna	k1gFnPc2	pokladna
kin	kino	k1gNnPc2	kino
více	hodně	k6eAd2	hodně
než	než	k8xS	než
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
z	z	k7c2	z
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
When	When	k1gInSc1	When
the	the	k?	the
Tigers	Tigers	k1gInSc1	Tigers
Broke	Brok	k1gMnSc2	Brok
Free	Fre	k1gMnSc2	Fre
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
také	také	k9	také
v	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
množství	množství	k1gNnSc6	množství
vydána	vydat	k5eAaPmNgFnS	vydat
jako	jako	k8xS	jako
singl	singl	k1gInSc1	singl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
též	též	k9	též
vydána	vydán	k2eAgFnSc1d1	vydána
na	na	k7c6	na
kompilačním	kompilační	k2eAgNnSc6d1	kompilační
albu	album	k1gNnSc6	album
Echoes	Echoes	k1gMnSc1	Echoes
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
a	a	k8xC	a
na	na	k7c6	na
reedici	reedice	k1gFnSc6	reedice
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
What	What	k1gMnSc1	What
Shall	Shall	k1gMnSc1	Shall
We	We	k1gMnSc1	We
Do	do	k7c2	do
Now	Now	k1gFnSc2	Now
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
omezeného	omezený	k2eAgInSc2d1	omezený
prostoru	prostor	k1gInSc2	prostor
na	na	k7c6	na
vinylu	vinyl	k1gInSc6	vinyl
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgFnPc1d1	jediná
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nebyly	být	k5eNaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Hey	Hey	k1gFnSc7	Hey
You	You	k1gFnSc2	You
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Show	show	k1gFnSc2	show
Must	Must	k1gMnSc1	Must
Go	Go	k1gMnSc1	Go
On	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
věnoval	věnovat	k5eAaImAgInS	věnovat
Waters	Waters	k1gInSc4	Waters
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
Ericu	Eric	k1gMnSc3	Eric
Fletcheru	Fletcher	k1gMnSc3	Fletcher
Watersovi	Waters	k1gMnSc3	Waters
<g/>
.	.	kIx.	.
</s>
<s>
Nese	nést	k5eAaImIp3nS	nést
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
v	v	k7c6	v
temnějším	temný	k2eAgMnSc6d2	temnější
duchu	duch	k1gMnSc6	duch
než	než	k8xS	než
The	The	k1gFnSc6	The
Wall	Walla	k1gFnPc2	Walla
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
mnoha	mnoho	k4c7	mnoho
předchozími	předchozí	k2eAgNnPc7d1	předchozí
tématy	téma	k1gNnPc7	téma
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
k	k	k7c3	k
současným	současný	k2eAgInPc3d1	současný
problémům	problém	k1gInPc3	problém
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Watersova	Watersův	k2eAgFnSc1d1	Watersova
zlost	zlost	k1gFnSc1	zlost
na	na	k7c4	na
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
kvůli	kvůli	k7c3	kvůli
účasti	účast	k1gFnSc3	účast
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
Falklandy	Falkland	k1gInPc4	Falkland
a	a	k8xC	a
vina	vina	k1gFnSc1	vina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
klade	klást	k5eAaImIp3nS	klást
tehdejším	tehdejší	k2eAgMnPc3d1	tehdejší
politickým	politický	k2eAgMnPc3d1	politický
vůdcům	vůdce	k1gMnPc3	vůdce
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Fletcher	Fletchra	k1gFnPc2	Fletchra
Memorial	Memorial	k1gMnSc1	Memorial
Home	Hom	k1gFnSc2	Hom
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cynicky	cynicky	k6eAd1	cynicky
a	a	k8xC	a
s	s	k7c7	s
obavami	obava	k1gFnPc7	obava
se	se	k3xPyFc4	se
Waters	Watersa	k1gFnPc2	Watersa
staví	stavit	k5eAaBmIp3nS	stavit
k	k	k7c3	k
hrozbě	hrozba	k1gFnSc3	hrozba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Two	Two	k1gFnSc1	Two
Suns	Suns	k1gInSc1	Suns
in	in	k?	in
the	the	k?	the
Sunset	Sunset	k1gInSc1	Sunset
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Kamen	kamna	k1gNnPc2	kamna
a	a	k8xC	a
Andy	Anda	k1gFnPc4	Anda
Bown	Bowno	k1gNnPc2	Bowno
zastoupili	zastoupit	k5eAaPmAgMnP	zastoupit
v	v	k7c6	v
hraní	hraní	k1gNnSc6	hraní
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
Ricka	Ricko	k1gNnSc2	Ricko
Wrighta	Wright	k1gInSc2	Wright
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
nebyl	být	k5eNaImAgMnS	být
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
alba	album	k1gNnSc2	album
oznámen	oznámit	k5eAaPmNgInS	oznámit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
technicky	technicky	k6eAd1	technicky
vzato	vzat	k2eAgNnSc1d1	vzato
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
album	album	k1gNnSc4	album
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
přebalu	přebal	k1gInSc2	přebal
desky	deska	k1gFnSc2	deska
není	být	k5eNaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dočíst	dočíst	k5eAaPmF	dočíst
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
-	-	kIx~	-
rekviem	rekviem	k1gNnSc1	rekviem
za	za	k7c4	za
poválečný	poválečný	k2eAgInSc4d1	poválečný
sen	sen	k1gInSc4	sen
Rogera	Rogero	k1gNnSc2	Rogero
Waterse	Waterse	k1gFnSc2	Waterse
zahraný	zahraný	k2eAgInSc4d1	zahraný
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
<g/>
:	:	kIx,	:
Roger	Roger	k1gInSc1	Roger
Waters	Waters	k1gInSc1	Waters
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Veškerou	veškerý	k3xTgFnSc4	veškerý
hudbu	hudba	k1gFnSc4	hudba
i	i	k8xC	i
text	text	k1gInSc1	text
napsal	napsat	k5eAaBmAgInS	napsat
sám	sám	k3xTgInSc4	sám
Waters	Waters	k1gInSc4	Waters
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
vlastním	vlastní	k2eAgNnSc6d1	vlastní
nahrávání	nahrávání	k1gNnSc6	nahrávání
<g/>
.	.	kIx.	.
</s>
<s>
Waters	Waters	k6eAd1	Waters
později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabízel	nabízet	k5eAaImAgInS	nabízet
skupině	skupina	k1gFnSc3	skupina
vydat	vydat	k5eAaPmF	vydat
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
jako	jako	k9	jako
jeho	jeho	k3xOp3gNnSc1	jeho
sólové	sólový	k2eAgNnSc1d1	sólové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zbytek	zbytek	k1gInSc1	zbytek
kapely	kapela	k1gFnSc2	kapela
toto	tento	k3xDgNnSc4	tento
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
<g/>
:	:	kIx,	:
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
bubeník	bubeník	k1gMnSc1	bubeník
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
diskuzi	diskuze	k1gFnSc3	diskuze
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmour	k1gMnSc1	Gilmour
opakovaně	opakovaně	k6eAd1	opakovaně
žádal	žádat	k5eAaImAgMnS	žádat
Waterse	Waterse	k1gFnPc4	Waterse
o	o	k7c4	o
zdržení	zdržení	k1gNnSc4	zdržení
vydání	vydání	k1gNnSc2	vydání
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
dopsat	dopsat	k5eAaPmF	dopsat
dost	dost	k6eAd1	dost
svého	svůj	k3xOyFgInSc2	svůj
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
žádost	žádost	k1gFnSc1	žádost
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Ladění	ladění	k1gNnSc1	ladění
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgFnPc4d1	podobná
The	The	k1gFnPc4	The
Wall	Walla	k1gFnPc2	Walla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
klidnější	klidný	k2eAgFnPc1d2	klidnější
a	a	k8xC	a
jemnější	jemný	k2eAgFnPc1d2	jemnější
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgFnPc1d1	podobná
více	hodně	k6eAd2	hodně
skladbě	skladba	k1gFnSc3	skladba
"	"	kIx"	"
<g/>
Nobody	Nobod	k1gInPc4	Nobod
Home	Home	k1gInSc1	Home
<g/>
"	"	kIx"	"
než	než	k8xS	než
"	"	kIx"	"
<g/>
Another	Anothra	k1gFnPc2	Anothra
Brick	Bricko	k1gNnPc2	Bricko
in	in	k?	in
the	the	k?	the
Wall	Wall	k1gInSc1	Wall
<g/>
,	,	kIx,	,
Part	part	k1gInSc1	part
II	II	kA	II
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hudební	hudební	k2eAgNnPc1d1	hudební
témata	téma	k1gNnPc1	téma
se	se	k3xPyFc4	se
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
,	,	kIx,	,
určité	určitý	k2eAgInPc1d1	určitý
leitmotivy	leitmotiv	k1gInPc1	leitmotiv
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgNnSc2d1	celé
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
The	The	k1gMnPc2	The
Final	Final	k1gInSc4	Final
Cut	Cut	k1gFnPc2	Cut
bylo	být	k5eAaImAgNnS	být
komerčně	komerčně	k6eAd1	komerčně
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kritiky	kritika	k1gFnSc2	kritika
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
i	i	k9	i
rádiový	rádiový	k2eAgInSc4d1	rádiový
hit	hit	k1gInSc4	hit
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Not	nota	k1gFnPc2	nota
Now	Now	k1gFnSc2	Now
John	John	k1gMnSc1	John
<g/>
"	"	kIx"	"
-	-	kIx~	-
jediná	jediný	k2eAgFnSc1d1	jediná
hardrocková	hardrockový	k2eAgFnSc1d1	hardrocková
píseň	píseň	k1gFnSc1	píseň
na	na	k7c6	na
albu	album	k1gNnSc6	album
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jediná	jediný	k2eAgFnSc1d1	jediná
částečně	částečně	k6eAd1	částečně
zpívaná	zpívaný	k2eAgFnSc1d1	zpívaná
Gilmourem	Gilmour	k1gMnSc7	Gilmour
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
hádky	hádka	k1gFnPc1	hádka
mezi	mezi	k7c7	mezi
Watersem	Waterso	k1gNnSc7	Waterso
a	a	k8xC	a
Gilmourem	Gilmour	k1gInSc7	Gilmour
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
tak	tak	k6eAd1	tak
časté	častý	k2eAgNnSc1d1	časté
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
společně	společně	k6eAd1	společně
a	a	k8xC	a
Gilmour	Gilmour	k1gMnSc1	Gilmour
byl	být	k5eAaImAgMnS	být
vypuštěn	vypustit	k5eAaPmNgMnS	vypustit
z	z	k7c2	z
popisu	popis	k1gInSc2	popis
na	na	k7c6	na
albu	album	k1gNnSc6	album
jako	jako	k9	jako
koproducent	koproducent	k1gMnSc1	koproducent
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
nekonala	konat	k5eNaImAgFnS	konat
na	na	k7c4	na
propagaci	propagace	k1gFnSc4	propagace
alba	album	k1gNnPc1	album
žádné	žádný	k3yNgNnSc4	žádný
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgFnPc1	některý
písně	píseň	k1gFnPc1	píseň
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
na	na	k7c6	na
sólových	sólový	k2eAgInPc6d1	sólový
koncertech	koncert	k1gInPc6	koncert
Waterse	Waterse	k1gFnSc2	Waterse
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
The	The	k1gFnSc6	The
Final	Final	k1gInSc4	Final
Cut	Cut	k1gMnSc2	Cut
vydalo	vydat	k5eAaPmAgNnS	vydat
Capitol	Capitol	k1gInSc4	Capitol
Records	Records	k1gInSc4	Records
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
výběrové	výběrový	k2eAgNnSc4d1	výběrové
album	album	k1gNnSc4	album
Works	Works	kA	Works
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
Watersova	Watersův	k2eAgFnSc1d1	Watersova
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Embryo	embryo	k1gNnSc1	embryo
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
vyšla	vyjít	k5eAaPmAgFnS	vyjít
již	již	k6eAd1	již
na	na	k7c6	na
těžko	těžko	k6eAd1	těžko
sehnatelné	sehnatelný	k2eAgFnSc3d1	sehnatelná
kompilaci	kompilace	k1gFnSc3	kompilace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
Picnic	Picnice	k1gFnPc2	Picnice
-	-	kIx~	-
A	a	k9	a
Breath	Breath	k1gMnSc1	Breath
of	of	k?	of
Fresh	Fresh	k1gMnSc1	Fresh
Air	Air	k1gMnSc1	Air
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Harvest	Harvest	k1gMnSc1	Harvest
Records	Records	k1gInSc1	Records
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozešli	rozejít	k5eAaPmAgMnP	rozejít
a	a	k8xC	a
trávili	trávit	k5eAaImAgMnP	trávit
čas	čas	k1gInSc4	čas
nad	nad	k7c7	nad
svými	svůj	k3xOyFgInPc7	svůj
sólovými	sólový	k2eAgInPc7d1	sólový
projekty	projekt	k1gInPc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Waters	Waters	k6eAd1	Waters
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1985	[number]	k4	1985
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
opouští	opouštět	k5eAaImIp3nS	opouštět
skupinu	skupina	k1gFnSc4	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
kreativně	kreativně	k6eAd1	kreativně
vyčerpanou	vyčerpaný	k2eAgFnSc4d1	vyčerpaná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
hlavního	hlavní	k2eAgMnSc2d1	hlavní
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
však	však	k9	však
Gilmour	Gilmour	k1gMnSc1	Gilmour
a	a	k8xC	a
Mason	mason	k1gMnSc1	mason
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
nahrával	nahrávat	k5eAaImAgInS	nahrávat
Waters	Waters	k1gInSc1	Waters
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
nazval	nazvat	k5eAaPmAgMnS	nazvat
Radio	radio	k1gNnSc4	radio
K.A.	K.A.	k1gFnSc2	K.A.
<g/>
O.S.	O.S.	k1gFnSc2	O.S.
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
táhl	táhnout	k5eAaImAgInS	táhnout
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
Roger	Roger	k1gMnSc1	Roger
Waters	Waters	k1gInSc4	Waters
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
název	název	k1gInSc1	název
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc1	Floyd
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
mimosoudně	mimosoudně	k6eAd1	mimosoudně
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Gilmoura	Gilmour	k1gMnSc2	Gilmour
a	a	k8xC	a
Masona	mason	k1gMnSc2	mason
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
byl	být	k5eAaImAgMnS	být
název	název	k1gInSc4	název
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
přiřknut	přiřknout	k5eAaPmNgMnS	přiřknout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvážení	zvážení	k1gNnSc6	zvážení
a	a	k8xC	a
zamítnutí	zamítnutí	k1gNnSc6	zamítnutí
mnoha	mnoho	k4c2	mnoho
titulů	titul	k1gInPc2	titul
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
dvoučlenné	dvoučlenný	k2eAgFnSc2d1	dvoučlenná
kapely	kapela	k1gFnSc2	kapela
s	s	k7c7	s
najatými	najatý	k2eAgMnPc7d1	najatý
studiovými	studiový	k2eAgMnPc7d1	studiový
hudebníky	hudebník	k1gMnPc7	hudebník
vybrán	vybrán	k2eAgInSc4d1	vybrán
název	název	k1gInSc4	název
A	a	k8xC	a
Momentary	Momentar	k1gInPc4	Momentar
Lapse	lapsus	k1gInSc5	lapsus
of	of	k?	of
Reason	Reason	k1gNnSc4	Reason
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
Waterse	Waterse	k1gFnSc2	Waterse
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
dominantním	dominantní	k2eAgMnSc7d1	dominantní
textařem	textař	k1gMnSc7	textař
skupiny	skupina	k1gFnSc2	skupina
po	po	k7c4	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
skupina	skupina	k1gFnSc1	skupina
shánět	shánět	k5eAaImF	shánět
pomoc	pomoc	k1gFnSc4	pomoc
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
externího	externí	k2eAgMnSc2d1	externí
textaře	textař	k1gMnSc2	textař
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
nikdy	nikdy	k6eAd1	nikdy
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
nepotřebovali	potřebovat	k5eNaImAgMnP	potřebovat
<g/>
,	,	kIx,	,
sklidil	sklidit	k5eAaPmAgMnS	sklidit
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
hodně	hodně	k6eAd1	hodně
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obnovil	obnovit	k5eAaPmAgMnS	obnovit
své	svůj	k3xOyFgNnSc4	svůj
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
Gilmourem	Gilmour	k1gInSc7	Gilmour
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
(	(	kIx(	(
<g/>
koprodukoval	koprodukovat	k5eAaBmAgInS	koprodukovat
jeho	jeho	k3xOp3gFnSc4	jeho
sólovou	sólový	k2eAgFnSc4d1	sólová
desku	deska	k1gFnSc4	deska
About	About	k2eAgInSc4d1	About
Face	Face	k1gInSc4	Face
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
albu	album	k1gNnSc6	album
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
koproducent	koproducent	k1gMnSc1	koproducent
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
textařů	textař	k1gMnPc2	textař
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
také	také	k9	také
klávesista	klávesista	k1gMnSc1	klávesista
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
najatý	najatý	k2eAgMnSc1d1	najatý
hráč	hráč	k1gMnSc1	hráč
(	(	kIx(	(
<g/>
na	na	k7c6	na
následujícím	následující	k2eAgNnSc6d1	následující
turné	turné	k1gNnSc6	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
už	už	k6eAd1	už
byl	být	k5eAaImAgMnS	být
plnoprávný	plnoprávný	k2eAgMnSc1d1	plnoprávný
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmour	k1gMnSc1	Gilmour
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mason	mason	k1gMnSc1	mason
a	a	k8xC	a
Wright	Wright	k1gMnSc1	Wright
na	na	k7c6	na
albu	album	k1gNnSc6	album
téměř	téměř	k6eAd1	téměř
nehráli	hrát	k5eNaImAgMnP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInPc3	jejich
malým	malý	k2eAgInPc3d1	malý
příspěvkům	příspěvek	k1gInPc3	příspěvek
někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
soudí	soudit	k5eAaImIp3nP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
sólový	sólový	k2eAgInSc4d1	sólový
projekt	projekt	k1gInSc4	projekt
Gilmoura	Gilmoura	k1gFnSc1	Gilmoura
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
za	za	k7c4	za
Watersovu	Watersův	k2eAgFnSc4d1	Watersova
sólovou	sólový	k2eAgFnSc4d1	sólová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
dvojalbum	dvojalbum	k1gNnSc4	dvojalbum
Delicate	Delicat	k1gInSc5	Delicat
Sound	Sounda	k1gFnPc2	Sounda
of	of	k?	of
Thunder	Thundra	k1gFnPc2	Thundra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
na	na	k7c4	na
Long	Long	k1gInSc4	Long
Island	Island	k1gInSc4	Island
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nahrál	nahrát	k5eAaBmAgMnS	nahrát
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
několik	několik	k4yIc4	několik
instrumentálních	instrumentální	k2eAgFnPc2d1	instrumentální
skladeb	skladba	k1gFnPc2	skladba
k	k	k7c3	k
dokumentárnímu	dokumentární	k2eAgInSc3d1	dokumentární
filmu	film	k1gInSc3	film
La	la	k1gNnSc4	la
Carrera	Carrer	k1gMnSc2	Carrer
Panamerica	Panamericus	k1gMnSc2	Panamericus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
klasickém	klasický	k2eAgInSc6d1	klasický
automobilovém	automobilový	k2eAgInSc6d1	automobilový
závodu	závod	k1gInSc6	závod
odehrávajícím	odehrávající	k2eAgInSc6d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
jehož	jehož	k3xOyRp3gNnSc7	jehož
se	se	k3xPyFc4	se
jako	jako	k9	jako
jezdci	jezdec	k1gMnPc1	jezdec
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
i	i	k9	i
Gilmour	Gilmour	k1gMnSc1	Gilmour
a	a	k8xC	a
Mason	mason	k1gMnSc1	mason
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
závodu	závod	k1gInSc2	závod
se	se	k3xPyFc4	se
Gilmour	Gilmour	k1gMnSc1	Gilmour
a	a	k8xC	a
manažer	manažer	k1gMnSc1	manažer
Steve	Steve	k1gMnSc1	Steve
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Rourke	Rourke	k1gNnSc4	Rourke
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
jel	jet	k5eAaImAgInS	jet
jako	jako	k9	jako
navigátor	navigátor	k1gInSc1	navigátor
<g/>
)	)	kIx)	)
vybourali	vybourat	k5eAaPmAgMnP	vybourat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Rourke	Rourke	k1gNnSc4	Rourke
si	se	k3xPyFc3	se
zlomil	zlomit	k5eAaPmAgMnS	zlomit
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
Gilmour	Gilmour	k1gMnSc1	Gilmour
vyvázl	vyváznout	k5eAaPmAgMnS	vyváznout
jen	jen	k9	jen
s	s	k7c7	s
pohmožděninami	pohmožděnina	k1gFnPc7	pohmožděnina
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
jsou	být	k5eAaImIp3nP	být
zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
první	první	k4xOgInSc4	první
materiál	materiál	k1gInSc4	materiál
od	od	k7c2	od
alba	album	k1gNnSc2	album
Wish	Wisha	k1gFnPc2	Wisha
You	You	k1gFnSc2	You
Were	Wer	k1gMnSc4	Wer
Here	Her	k1gMnSc4	Her
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
skladatelsky	skladatelsky	k6eAd1	skladatelsky
přispěl	přispět	k5eAaPmAgMnS	přispět
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jediný	jediný	k2eAgInSc4d1	jediný
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
přispěl	přispět	k5eAaPmAgMnS	přispět
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
The	The	k1gMnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gMnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vyšel	vyjít	k5eAaPmAgInS	vyjít
devítidiskový	devítidiskový	k2eAgInSc1d1	devítidiskový
set	set	k1gInSc1	set
Shine	Shin	k1gInSc5	Shin
On	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
remasterované	remasterovaný	k2eAgFnPc4d1	remasterovaná
verze	verze	k1gFnPc4	verze
A	a	k8xC	a
Saucerful	Saucerful	k1gInSc4	Saucerful
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
<g/>
,	,	kIx,	,
Meddle	Meddle	k1gMnSc1	Meddle
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gMnSc1	Moon
<g/>
,	,	kIx,	,
Wish	Wish	k1gMnSc1	Wish
You	You	k1gFnSc2	You
Were	Were	k1gFnSc1	Were
Here	Here	k1gFnSc1	Here
<g/>
,	,	kIx,	,
Animals	Animals	k1gInSc1	Animals
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
a	a	k8xC	a
A	a	k9	a
Momentary	Momentara	k1gFnPc1	Momentara
Lapse	lapsus	k1gInSc5	lapsus
of	of	k?	of
Reason	Reasona	k1gFnPc2	Reasona
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
bonusový	bonusový	k2eAgInSc1d1	bonusový
disk	disk	k1gInSc1	disk
The	The	k1gFnSc2	The
Early	earl	k1gMnPc4	earl
Singles	Singles	k1gInSc4	Singles
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvotní	prvotní	k2eAgInPc4d1	prvotní
singly	singl	k1gInPc4	singl
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
setu	set	k1gInSc6	set
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
obsažen	obsažen	k2eAgInSc1d1	obsažen
držák	držák	k1gInSc1	držák
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgInSc3	který
mohou	moct	k5eAaImIp3nP	moct
všechna	všechen	k3xTgNnPc4	všechen
tato	tento	k3xDgNnPc4	tento
alba	album	k1gNnPc4	album
stát	stát	k5eAaImF	stát
rovně	roveň	k1gFnPc4	roveň
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
boční	boční	k2eAgFnPc4d1	boční
strany	strana	k1gFnPc4	strana
poskládané	poskládaný	k2eAgFnPc4d1	poskládaná
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
dohromady	dohromady	k6eAd1	dohromady
obrázek	obrázek	k1gInSc4	obrázek
z	z	k7c2	z
alba	album	k1gNnSc2	album
The	The	k1gMnSc2	The
Dark	Darko	k1gNnPc2	Darko
Side	Side	k1gNnPc2	Side
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
nečitelný	čitelný	k2eNgInSc4d1	nečitelný
kruhový	kruhový	k2eAgInSc4d1	kruhový
text	text	k1gInSc4	text
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
CD	CD	kA	CD
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Big	Big	k1gFnSc1	Big
Bong	bongo	k1gNnPc2	bongo
Theory	Theora	k1gFnSc2	Theora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
studiovou	studiový	k2eAgFnSc7d1	studiová
nahrávkou	nahrávka	k1gFnSc7	nahrávka
skupiny	skupina	k1gFnSc2	skupina
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
Wright	Wright	k1gMnSc1	Wright
a	a	k8xC	a
Mason	mason	k1gMnSc1	mason
<g/>
)	)	kIx)	)
podíleli	podílet	k5eAaImAgMnP	podílet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
na	na	k7c6	na
předešlé	předešlý	k2eAgFnSc6d1	předešlá
desce	deska	k1gFnSc6	deska
A	a	k9	a
Momentary	Momentar	k1gInPc4	Momentar
Lapse	lapsus	k1gInSc5	lapsus
of	of	k?	of
Reason	Reasona	k1gFnPc2	Reasona
<g/>
.	.	kIx.	.
</s>
<s>
Wright	Wright	k1gMnSc1	Wright
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgMnS	ustálit
jako	jako	k9	jako
plnohodnotný	plnohodnotný	k2eAgMnSc1d1	plnohodnotný
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
i	i	k9	i
jako	jako	k9	jako
spoluautor	spoluautor	k1gMnSc1	spoluautor
několika	několik	k4yIc2	několik
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
předchozí	předchozí	k2eAgFnSc1d1	předchozí
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
jako	jako	k8xS	jako
unavující	unavující	k2eAgFnPc1d1	unavující
a	a	k8xC	a
šablonovité	šablonovitý	k2eAgFnPc1d1	šablonovitá
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
však	však	k9	však
druhým	druhý	k4xOgNnSc7	druhý
albem	album	k1gNnSc7	album
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
i	i	k8xC	i
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
(	(	kIx(	(
<g/>
po	po	k7c4	po
Wish	Wish	k1gInSc4	Wish
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
je	být	k5eAaImIp3nS	být
koncepčním	koncepční	k2eAgNnSc7d1	koncepční
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
smyslu	smysl	k1gInSc6	smysl
Gilmour	Gilmour	k1gMnSc1	Gilmour
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
stejných	stejný	k2eAgNnPc2d1	stejné
témat	téma	k1gNnPc2	téma
jako	jako	k8xS	jako
Waters	Watersa	k1gFnPc2	Watersa
v	v	k7c6	v
The	The	k1gFnSc6	The
Wall	Walla	k1gFnPc2	Walla
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
alba	album	k1gNnSc2	album
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
Douglas	Douglas	k1gInSc1	Douglas
Adams	Adams	k1gInSc1	Adams
<g/>
,	,	kIx,	,
dobrý	dobrý	k2eAgMnSc1d1	dobrý
Gilmourův	Gilmourův	k2eAgMnSc1d1	Gilmourův
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
textů	text	k1gInPc2	text
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
napsat	napsat	k5eAaPmF	napsat
Polly	Polla	k1gFnPc4	Polla
Samsonová	Samsonový	k2eAgFnSc1d1	Samsonový
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
Gilmourova	Gilmourův	k2eAgFnSc1d1	Gilmourova
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Samsonové	Samson	k1gMnPc1	Samson
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
podíleli	podílet	k5eAaImAgMnP	podílet
další	další	k2eAgMnPc1d1	další
muzikanti	muzikant	k1gMnPc1	muzikant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
při	při	k7c6	při
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
A	a	k9	a
Momentary	Momentar	k1gMnPc4	Momentar
Lapse	lapsus	k1gInSc5	lapsus
of	of	k?	of
Reason	Reason	k1gInSc4	Reason
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
saxofonista	saxofonista	k1gMnSc1	saxofonista
Dick	Dicka	k1gFnPc2	Dicka
Parry	Parra	k1gFnSc2	Parra
<g/>
,	,	kIx,	,
nedílný	dílný	k2eNgMnSc1d1	nedílný
člen	člen	k1gMnSc1	člen
vystoupení	vystoupení	k1gNnSc2	vystoupení
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Anthony	Anthona	k1gFnPc1	Anthona
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
textu	text	k1gInSc6	text
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
skladeb	skladba	k1gFnPc2	skladba
na	na	k7c6	na
předchozím	předchozí	k2eAgNnSc6d1	předchozí
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
dodal	dodat	k5eAaPmAgMnS	dodat
text	text	k1gInSc4	text
k	k	k7c3	k
písní	píseň	k1gFnPc2	píseň
Ricka	Ricka	k1gMnSc1	Ricka
Wrighta	Wrighta	k1gMnSc1	Wrighta
"	"	kIx"	"
<g/>
Wearing	Wearing	k1gInSc1	Wearing
the	the	k?	the
Inside	Insid	k1gMnSc5	Insid
Out	Out	k1gMnSc5	Out
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc4	první
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byl	být	k5eAaImAgMnS	být
Wright	Wright	k1gMnSc1	Wright
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zpěvákem	zpěvák	k1gMnSc7	zpěvák
od	od	k7c2	od
alba	album	k1gNnSc2	album
The	The	k1gMnSc2	The
Dark	Darko	k1gNnPc2	Darko
Side	Side	k1gNnPc2	Side
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vydali	vydat	k5eAaPmAgMnP	vydat
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
Pulse	puls	k1gInSc5	puls
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
záhy	záhy	k6eAd1	záhy
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
písně	píseň	k1gFnPc4	píseň
hrané	hraný	k2eAgFnPc4d1	hraná
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
Hannoveru	Hannover	k1gInSc6	Hannover
a	a	k8xC	a
Modeně	Modena	k1gFnSc6	Modena
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kompletně	kompletně	k6eAd1	kompletně
zahrané	zahraný	k2eAgFnSc2d1	zahraná
The	The	k1gFnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
turné	turné	k1gNnSc3	turné
hrála	hrát	k5eAaImAgFnS	hrát
skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gNnSc4	Moon
během	během	k7c2	během
dvou	dva	k4xCgFnPc2	dva
dekád	dekáda	k1gFnPc2	dekáda
-	-	kIx~	-
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vyšel	vyjít	k5eAaPmAgInS	vyjít
i	i	k9	i
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
videozáznam	videozáznam	k1gInSc1	videozáznam
z	z	k7c2	z
londýnského	londýnský	k2eAgMnSc2d1	londýnský
Earls	Earls	k1gInSc4	Earls
Court	Courta	k1gFnPc2	Courta
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1994	[number]	k4	1994
-	-	kIx~	-
původně	původně	k6eAd1	původně
jako	jako	k9	jako
VHS	VHS	kA	VHS
a	a	k8xC	a
laserdisc	laserdisc	k1gFnSc1	laserdisc
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
prvních	první	k4xOgNnPc2	první
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
prodejnosti	prodejnost	k1gFnSc6	prodejnost
<g/>
.	.	kIx.	.
</s>
<s>
CD	CD	kA	CD
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
mělo	mít	k5eAaImAgNnS	mít
LED	LED	kA	LED
diodu	dioda	k1gFnSc4	dioda
<g/>
,	,	kIx,	,
IC	IC	kA	IC
časovač	časovač	k1gInSc4	časovač
a	a	k8xC	a
baterii	baterie	k1gFnSc4	baterie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yRgInPc4	který
světýlko	světýlko	k1gNnSc4	světýlko
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
blikalo	blikat	k5eAaImAgNnS	blikat
červeně	červeně	k6eAd1	červeně
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
jako	jako	k8xC	jako
srdeční	srdeční	k2eAgInSc4d1	srdeční
tep	tep	k1gInSc4	tep
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
turné	turné	k1gNnSc4	turné
The	The	k1gFnSc2	The
Division	Division	k1gInSc4	Division
Bell	bell	k1gInSc4	bell
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
skupina	skupina	k1gFnSc1	skupina
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
také	také	k9	také
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
konkrétně	konkrétně	k6eAd1	konkrétně
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1994	[number]	k4	1994
na	na	k7c6	na
Velkém	velký	k2eAgInSc6d1	velký
strahovském	strahovský	k2eAgInSc6d1	strahovský
stadionu	stadion	k1gInSc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jejich	jejich	k3xOp3gInSc4	jejich
koncert	koncert	k1gInSc4	koncert
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
asi	asi	k9	asi
115	[number]	k4	115
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc1	jaký
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
rollové	rollová	k1gFnSc2	rollová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gInSc1	Roger
Waters	Watersa	k1gFnPc2	Watersa
se	se	k3xPyFc4	se
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgFnPc1d1	živá
nahrávky	nahrávka	k1gFnPc1	nahrávka
The	The	k1gFnSc2	The
Wall	Walla	k1gFnPc2	Walla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
nahrány	nahrát	k5eAaBmNgFnP	nahrát
během	během	k7c2	během
vystoupení	vystoupení	k1gNnSc2	vystoupení
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Is	Is	k1gFnSc2	Is
There	Ther	k1gInSc5	Ther
Anybody	Anyboda	k1gFnPc1	Anyboda
Out	Out	k1gMnSc5	Out
There	Ther	k1gMnSc5	Ther
<g/>
?	?	kIx.	?
</s>
<s>
The	The	k?	The
Wall	Wall	k1gInSc1	Wall
Live	Live	k1gFnSc1	Live
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
a	a	k8xC	a
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vyšla	vyjít	k5eAaPmAgFnS	vyjít
remasterovaná	remasterovaný	k2eAgFnSc1d1	remasterovaná
dvoudisková	dvoudiskový	k2eAgFnSc1d1	dvoudisková
verze	verze	k1gFnSc1	verze
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
skladeb	skladba	k1gFnPc2	skladba
skupiny	skupina	k1gFnSc2	skupina
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Echoes	Echoes	k1gMnSc1	Echoes
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
Mason	mason	k1gMnSc1	mason
<g/>
,	,	kIx,	,
Wright	Wright	k1gMnSc1	Wright
i	i	k8xC	i
Waters	Watersa	k1gFnPc2	Watersa
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c6	na
výběru	výběr	k1gInSc6	výběr
<g/>
,	,	kIx,	,
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
úpravách	úprava	k1gFnPc6	úprava
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc4d2	menší
rozpor	rozpor	k1gInSc4	rozpor
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
pořadí	pořadí	k1gNnSc1	pořadí
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
není	být	k5eNaImIp3nS	být
chronologické	chronologický	k2eAgNnSc1d1	chronologické
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vytržené	vytržený	k2eAgNnSc1d1	vytržené
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
původních	původní	k2eAgFnPc2d1	původní
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Echoes	Echoes	k1gMnSc1	Echoes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Shine	Shin	k1gInSc5	Shin
On	on	k3xPp3gMnSc1	on
You	You	k1gMnSc1	You
Crazy	Craza	k1gFnSc2	Craza
Diamond	Diamond	k1gMnSc1	Diamond
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Marooned	Marooned	k1gInSc1	Marooned
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
High	High	k1gMnSc1	High
Hopes	Hopes	k1gMnSc1	Hopes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
zkráceny	zkrácen	k2eAgFnPc1d1	zkrácena
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
i	i	k8xC	i
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
koncertech	koncert	k1gInPc6	koncert
Watersova	Watersův	k2eAgNnSc2d1	Watersovo
sólového	sólový	k2eAgNnSc2d1	sólové
turné	turné	k1gNnSc2	turné
In	In	k1gMnSc1	In
the	the	k?	the
Flesh	Flesh	k1gMnSc1	Flesh
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2002	[number]	k4	2002
hostoval	hostovat	k5eAaImAgInS	hostovat
za	za	k7c7	za
bicími	bicí	k2eAgFnPc7d1	bicí
během	během	k7c2	během
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Set	set	k1gInSc1	set
the	the	k?	the
Controls	Controlsa	k1gFnPc2	Controlsa
for	forum	k1gNnPc2	forum
the	the	k?	the
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
SACD	SACD	kA	SACD
reedice	reedice	k1gFnSc1	reedice
The	The	k1gFnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
obalem	obal	k1gInSc7	obal
(	(	kIx(	(
<g/>
ke	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
původního	původní	k2eAgNnSc2d1	původní
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
znovu	znovu	k6eAd1	znovu
vydáno	vydat	k5eAaPmNgNnS	vydat
jako	jako	k9	jako
180	[number]	k4	180
<g/>
gramová	gramový	k2eAgFnSc1d1	gramová
vinylová	vinylový	k2eAgFnSc1d1	vinylová
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vzhled	vzhled	k1gInSc4	vzhled
originálu	originál	k1gInSc2	originál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
nový	nový	k2eAgInSc1d1	nový
přebal	přebal	k1gInSc1	přebal
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
manažer	manažer	k1gMnSc1	manažer
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
Steve	Steve	k1gMnSc1	Steve
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Rourke	Rourke	k1gFnSc6	Rourke
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
se	se	k3xPyFc4	se
trio	trio	k1gNnSc1	trio
znovu	znovu	k6eAd1	znovu
sešlo	sejít	k5eAaPmAgNnS	sejít
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
koncertu	koncert	k1gInSc2	koncert
Live	Liv	k1gInSc2	Liv
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
připojil	připojit	k5eAaPmAgMnS	připojit
i	i	k9	i
Waters	Waters	k1gInSc4	Waters
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
24	[number]	k4	24
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
kompletní	kompletní	k2eAgFnSc1d1	kompletní
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nejslavnějším	slavný	k2eAgNnSc6d3	nejslavnější
složení	složení	k1gNnSc6	složení
<g/>
.	.	kIx.	.
</s>
<s>
Zahráli	zahrát	k5eAaPmAgMnP	zahrát
4	[number]	k4	4
písně	píseň	k1gFnPc1	píseň
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Speak	Speak	k1gMnSc1	Speak
to	ten	k3xDgNnSc4	ten
Me	Me	k1gFnSc1	Me
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
Breathe	Breathe	k1gFnSc1	Breathe
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
Breathe	Breathe	k1gFnSc6	Breathe
(	(	kIx(	(
<g/>
Reprise	reprisa	k1gFnSc6	reprisa
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Money	Monea	k1gFnPc1	Monea
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Wish	Wish	k1gInSc1	Wish
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Comfortably	Comfortably	k1gFnSc1	Comfortably
Numb	Numb	k1gMnSc1	Numb
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
zpěvu	zpěv	k1gInSc6	zpěv
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
Gilmour	Gilmour	k1gMnSc1	Gilmour
i	i	k8xC	i
Waters	Waters	k1gInSc1	Waters
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
písni	píseň	k1gFnSc6	píseň
řekl	říct	k5eAaPmAgMnS	říct
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Děkujeme	děkovat	k5eAaImIp1nP	děkovat
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc4d1	dobrá
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
odcházet	odcházet	k5eAaImF	odcházet
z	z	k7c2	z
pódia	pódium	k1gNnSc2	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Waters	Waters	k6eAd1	Waters
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
zavolal	zavolat	k5eAaPmAgMnS	zavolat
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
čtyři	čtyři	k4xCgMnPc1	čtyři
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
objali	obejmout	k5eAaPmAgMnP	obejmout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
obrázků	obrázek	k1gInPc2	obrázek
z	z	k7c2	z
Live	Liv	k1gInSc2	Liv
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
po	po	k7c6	po
Live	Live	k1gNnSc6	Live
8	[number]	k4	8
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
nový	nový	k2eAgInSc1d1	nový
nárůst	nárůst	k1gInSc1	nárůst
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obchodního	obchodní	k2eAgInSc2d1	obchodní
řetězce	řetězec	k1gInSc2	řetězec
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
HMV	HMV	kA	HMV
vzrostly	vzrůst	k5eAaPmAgInP	vzrůst
prodeje	prodej	k1gInPc1	prodej
Echoes	Echoes	k1gInSc1	Echoes
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
týdnu	týden	k1gInSc6	týden
o	o	k7c4	o
1	[number]	k4	1
343	[number]	k4	343
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
nárůst	nárůst	k1gInSc4	nárůst
prodeje	prodej	k1gInSc2	prodej
u	u	k7c2	u
The	The	k1gFnSc2	The
Wall	Wall	k1gInSc1	Wall
o	o	k7c4	o
3	[number]	k4	3
600	[number]	k4	600
%	%	kIx~	%
<g/>
,	,	kIx,	,
Wish	Wish	k1gMnSc1	Wish
You	You	k1gFnSc2	You
Were	Wer	k1gFnSc2	Wer
Here	Her	k1gFnSc2	Her
o	o	k7c4	o
2	[number]	k4	2
000	[number]	k4	000
%	%	kIx~	%
<g/>
,	,	kIx,	,
Dark	Dark	k1gMnSc1	Dark
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
o	o	k7c4	o
1	[number]	k4	1
400	[number]	k4	400
%	%	kIx~	%
a	a	k8xC	a
Animals	Animals	k1gInSc1	Animals
o	o	k7c4	o
1	[number]	k4	1
000	[number]	k4	000
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
poté	poté	k6eAd1	poté
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zisk	zisk	k1gInSc1	zisk
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
prodejů	prodej	k1gInPc2	prodej
věnuje	věnovat	k5eAaPmIp3nS	věnovat
na	na	k7c4	na
charitu	charita	k1gFnSc4	charita
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
vystupující	vystupující	k2eAgMnPc4d1	vystupující
umělce	umělec	k1gMnPc4	umělec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
nahrávací	nahrávací	k2eAgFnPc4d1	nahrávací
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
udělali	udělat	k5eAaPmAgMnP	udělat
totéž	týž	k3xTgNnSc4	týž
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
uvedena	uveden	k2eAgFnSc1d1	uvedena
Petem	Pet	k1gMnSc7	Pet
Townshedem	Townshed	k1gMnSc7	Townshed
do	do	k7c2	do
hudební	hudební	k2eAgFnSc2d1	hudební
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmour	k1gMnSc1	Gilmour
a	a	k8xC	a
Mason	mason	k1gMnSc1	mason
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
Wright	Wright	k1gMnSc1	Wright
nemohl	moct	k5eNaImAgMnS	moct
přijet	přijet	k5eAaPmF	přijet
kvůli	kvůli	k7c3	kvůli
operaci	operace	k1gFnSc3	operace
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
Waters	Waters	k1gInSc1	Waters
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgInS	spojit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
videa	video	k1gNnSc2	video
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
doufalo	doufat	k5eAaImAgNnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
společné	společný	k2eAgNnSc4d1	společné
vystoupení	vystoupení	k1gNnSc4	vystoupení
na	na	k7c4	na
Live	Live	k1gInSc4	Live
8	[number]	k4	8
povede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
bylo	být	k5eAaImAgNnS	být
dokonce	dokonce	k9	dokonce
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
rekordních	rekordní	k2eAgInPc2d1	rekordní
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
jasně	jasně	k6eAd1	jasně
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
neplánují	plánovat	k5eNaImIp3nP	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
vystoupení	vystoupení	k1gNnSc6	vystoupení
se	se	k3xPyFc4	se
však	však	k9	však
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
udobřili	udobřit	k5eAaPmAgMnP	udobřit
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmour	k1gMnSc1	Gilmour
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Watersovi	Waters	k1gMnSc3	Waters
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
"	"	kIx"	"
<g/>
přátelské	přátelský	k2eAgFnSc6d1	přátelská
bázi	báze	k1gFnSc6	báze
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Waters	Waters	k1gInSc1	Waters
vydal	vydat	k5eAaPmAgInS	vydat
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
obnovení	obnovení	k1gNnPc2	obnovení
několik	několik	k4yIc4	několik
prohlášení	prohlášení	k1gNnPc2	prohlášení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgMnPc2	který
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
rád	rád	k6eAd1	rád
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
ještě	ještě	k9	ještě
někdy	někdy	k6eAd1	někdy
zahrál	zahrát	k5eAaPmAgMnS	zahrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
celé	celý	k2eAgNnSc4d1	celé
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
jednorázovou	jednorázový	k2eAgFnSc4d1	jednorázová
akci	akce	k1gFnSc4	akce
typu	typ	k1gInSc2	typ
Live	Liv	k1gInSc2	Liv
8	[number]	k4	8
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgMnS	vydat
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
za	za	k7c4	za
skupinu	skupina	k1gFnSc4	skupina
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
sjednocení	sjednocení	k1gNnSc3	sjednocení
skupiny	skupina	k1gFnSc2	skupina
neplánují	plánovat	k5eNaImIp3nP	plánovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
popřel	popřít	k5eAaPmAgMnS	popřít
všechny	všechen	k3xTgFnPc4	všechen
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgNnP	zabývat
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmour	k1gMnSc1	Gilmour
později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
list	list	k1gInSc4	list
La	la	k1gNnSc2	la
Repubblica	Repubblic	k1gInSc2	Repubblic
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
končí	končit	k5eAaImIp3nS	končit
s	s	k7c7	s
Pink	pink	k6eAd1	pink
Floyd	Floyda	k1gFnPc2	Floyda
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
jen	jen	k9	jen
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
sólovou	sólový	k2eAgFnSc4d1	sólová
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
rodinu	rodina	k1gFnSc4	rodina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Live	Live	k1gNnSc6	Live
8	[number]	k4	8
hrál	hrát	k5eAaImAgMnS	hrát
prý	prý	k9	prý
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podpořil	podpořit	k5eAaPmAgMnS	podpořit
dobrou	dobrý	k2eAgFnSc4d1	dobrá
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
usmířil	usmířit	k5eAaPmAgInS	usmířit
se	se	k3xPyFc4	se
s	s	k7c7	s
Watersem	Waters	k1gInSc7	Waters
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
případnou	případný	k2eAgFnSc4d1	případná
neúčast	neúčast	k1gFnSc4	neúčast
by	by	kYmCp3nP	by
určitě	určitě	k6eAd1	určitě
litoval	litovat	k5eAaImAgInS	litovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
by	by	kYmCp3nS	by
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc4	Floyd
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
izraelsko-palestinského	izraelskoalestinský	k2eAgNnSc2d1	izraelsko-palestinské
příměří	příměří	k1gNnSc2	příměří
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgMnS	vydat
Gilmour	Gilmour	k1gMnSc1	Gilmour
svoji	svůj	k3xOyFgFnSc4	svůj
třetí	třetí	k4xOgFnSc4	třetí
sólovou	sólový	k2eAgFnSc4d1	sólová
desku	deska	k1gFnSc4	deska
On	on	k3xPp3gInSc1	on
an	an	k?	an
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
následujícího	následující	k2eAgNnSc2d1	následující
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
koncertě	koncert	k1gInSc6	koncert
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
Gilmourově	Gilmourův	k2eAgFnSc3d1	Gilmourova
skupině	skupina	k1gFnSc3	skupina
i	i	k8xC	i
Nick	Nicka	k1gFnPc2	Nicka
Mason	mason	k1gMnSc1	mason
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
ve	v	k7c6	v
skladbách	skladba	k1gFnPc6	skladba
"	"	kIx"	"
<g/>
Wish	Wish	k1gInSc4	Wish
You	You	k1gFnSc2	You
Were	Were	k1gFnSc1	Were
Here	Here	k1gFnSc1	Here
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Comfortably	Comfortably	k1gFnSc1	Comfortably
Numb	Numb	k1gMnSc1	Numb
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Waters	Waters	k1gInSc1	Waters
byl	být	k5eAaImAgInS	být
také	také	k9	také
pozván	pozvat	k5eAaPmNgMnS	pozvat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
generální	generální	k2eAgFnPc1d1	generální
zkoušky	zkouška	k1gFnPc1	zkouška
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
jej	on	k3xPp3gInSc2	on
donutily	donutit	k5eAaPmAgInP	donutit
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
opět	opět	k6eAd1	opět
během	během	k7c2	během
let	léto	k1gNnPc2	léto
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
Watersových	Watersův	k2eAgInPc6d1	Watersův
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Syda	Syd	k1gInSc2	Syd
Barretta	Barrett	k1gInSc2	Barrett
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
fanoušci	fanoušek	k1gMnPc1	fanoušek
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
dá	dát	k5eAaPmIp3nS	dát
dohromady	dohromady	k6eAd1	dohromady
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Barbican	Barbican	k1gMnSc1	Barbican
Arts	Arts	k1gInSc1	Arts
Centre	centr	k1gInSc5	centr
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
koncert	koncert	k1gInSc4	koncert
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
Barrettovy	Barrettův	k2eAgFnSc2d1	Barrettova
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Samostatně	samostatně	k6eAd1	samostatně
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Roger	Roger	k1gMnSc1	Roger
Waters	Watersa	k1gFnPc2	Watersa
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
kapely	kapela	k1gFnSc2	kapela
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
a	a	k8xC	a
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
společnému	společný	k2eAgNnSc3d1	společné
vystoupení	vystoupení	k1gNnSc3	vystoupení
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
očekávání	očekávání	k1gNnPc4	očekávání
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
prvního	první	k4xOgInSc2	první
singlu	singl	k1gInSc2	singl
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
EMI	EMI	kA	EMI
vydali	vydat	k5eAaPmAgMnP	vydat
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
reedici	reedice	k1gFnSc4	reedice
jejich	jejich	k3xOp3gFnPc2	jejich
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Piper	Piper	k1gMnSc1	Piper
at	at	k?	at
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
Dawn	Dawn	k1gMnSc1	Dawn
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
box	box	k1gInSc4	box
set	sto	k4xCgNnPc2	sto
Oh	oh	k0	oh
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
the	the	k?	the
Way	Way	k1gFnSc1	Way
obsahující	obsahující	k2eAgFnSc1d1	obsahující
ucelené	ucelený	k2eAgNnSc4d1	ucelené
vydání	vydání	k1gNnSc4	vydání
všech	všecek	k3xTgFnPc2	všecek
studiových	studiový	k2eAgFnPc2d1	studiová
alb	alba	k1gFnPc2	alba
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
v	v	k7c6	v
remasterované	remasterovaný	k2eAgFnSc6d1	remasterovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
klávesista	klávesista	k1gMnSc1	klávesista
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
definitivně	definitivně	k6eAd1	definitivně
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
naděje	naděje	k1gFnPc4	naděje
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
vystoupení	vystoupení	k1gNnSc4	vystoupení
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
nejslavnějším	slavný	k2eAgNnSc6d3	nejslavnější
složení	složení	k1gNnSc6	složení
Gilmour	Gilmoura	k1gFnPc2	Gilmoura
-	-	kIx~	-
Waters	Waters	k1gInSc1	Waters
-	-	kIx~	-
Wright	Wright	k1gMnSc1	Wright
-	-	kIx~	-
Mason	mason	k1gMnSc1	mason
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Watersem	Waters	k1gMnSc7	Waters
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
charitativním	charitativní	k2eAgInSc6d1	charitativní
koncertu	koncert	k1gInSc6	koncert
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
diváků	divák	k1gMnPc2	divák
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
nadaci	nadace	k1gFnSc4	nadace
Hoping	Hoping	k1gInSc1	Hoping
Foundation	Foundation	k1gInSc1	Foundation
v	v	k7c6	v
Kidlingtonu	Kidlington	k1gInSc6	Kidlington
v	v	k7c6	v
Oxfordshire	Oxfordshir	k1gMnSc5	Oxfordshir
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Waterse	Waterse	k1gFnSc1	Waterse
pozval	pozvat	k5eAaPmAgMnS	pozvat
<g/>
,	,	kIx,	,
slíbil	slíbit	k5eAaPmAgMnS	slíbit
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahraje	zahrát	k5eAaPmIp3nS	zahrát
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
koncertě	koncert	k1gInSc6	koncert
jeho	jeho	k3xOp3gNnSc2	jeho
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
turné	turné	k1gNnSc2	turné
The	The	k1gMnSc2	The
Wall	Wallum	k1gNnPc2	Wallum
Live	Live	k1gNnPc2	Live
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Comfortably	Comfortably	k1gFnSc1	Comfortably
Numb	Numb	k1gMnSc1	Numb
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Gilmour	Gilmour	k1gMnSc1	Gilmour
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Nickem	Nicek	k1gMnSc7	Nicek
Masonem	mason	k1gMnSc7	mason
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
vrátil	vrátit	k5eAaPmAgMnS	vrátit
i	i	k9	i
při	při	k7c6	při
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Outside	Outsid	k1gInSc5	Outsid
the	the	k?	the
Wall	Wall	k1gInSc1	Wall
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
pětiletou	pětiletý	k2eAgFnSc4d1	pětiletá
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
prodej	prodej	k1gInSc4	prodej
jejich	jejich	k3xOp3gNnPc2	jejich
díla	dílo	k1gNnSc2	dílo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
nabízení	nabízení	k1gNnSc2	nabízení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
si	se	k3xPyFc3	se
ubránila	ubránit	k5eAaPmAgFnS	ubránit
svoji	svůj	k3xOyFgFnSc4	svůj
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnPc4	jejich
alba	album	k1gNnPc4	album
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
prodávána	prodávat	k5eAaImNgFnS	prodávat
jako	jako	k8xS	jako
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
skladby	skladba	k1gFnPc1	skladba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
celky	celek	k1gInPc1	celek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
vydali	vydat	k5eAaPmAgMnP	vydat
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
nové	nový	k2eAgNnSc1d1	nové
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Endless	Endless	k1gInSc1	Endless
River	River	k1gInSc1	River
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
instrumentálních	instrumentální	k2eAgFnPc2d1	instrumentální
kompozic	kompozice	k1gFnPc2	kompozice
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
nahrávacích	nahrávací	k2eAgFnPc2d1	nahrávací
frekvencí	frekvence	k1gFnPc2	frekvence
pro	pro	k7c4	pro
desku	deska	k1gFnSc4	deska
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
množství	množství	k1gNnSc1	množství
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nebyl	být	k5eNaImAgMnS	být
na	na	k7c6	na
The	The	k1gFnSc6	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
a	a	k8xC	a
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
dalších	další	k2eAgMnPc2d1	další
hudebníků	hudebník	k1gMnPc2	hudebník
a	a	k8xC	a
producentů	producent	k1gMnPc2	producent
v	v	k7c6	v
letech	let	k1gInPc6	let
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
přepracovali	přepracovat	k5eAaPmAgMnP	přepracovat
a	a	k8xC	a
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
využili	využít	k5eAaPmAgMnP	využít
i	i	k9	i
původních	původní	k2eAgFnPc2d1	původní
klávesových	klávesový	k2eAgFnPc2d1	klávesová
nahrávek	nahrávka	k1gFnPc2	nahrávka
Richarda	Richard	k1gMnSc2	Richard
Wrighta	Wright	k1gMnSc2	Wright
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
neočekávané	očekávaný	k2eNgNnSc1d1	neočekávané
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejvíce	nejvíce	k6eAd1	nejvíce
předobjednávaným	předobjednávaný	k2eAgNnSc7d1	předobjednávaný
albem	album	k1gNnSc7	album
historie	historie	k1gFnSc2	historie
Amazonu	amazona	k1gFnSc4	amazona
<g/>
,	,	kIx,	,
když	když	k8xS	když
předčilo	předčit	k5eAaBmAgNnS	předčit
i	i	k8xC	i
desku	deska	k1gFnSc4	deska
Midnight	Midnight	k2eAgMnSc1d1	Midnight
Memories	Memories	k1gMnSc1	Memories
skupiny	skupina	k1gFnSc2	skupina
One	One	k1gMnSc1	One
Direction	Direction	k1gInSc1	Direction
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Piper	Piper	k1gInSc1	Piper
at	at	k?	at
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
Dawn	Dawn	k1gMnSc1	Dawn
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
A	a	k8xC	a
Saucerful	Saucerful	k1gInSc1	Saucerful
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Ummagumma	Ummagummum	k1gNnSc2	Ummagummum
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
dvojalbum	dvojalbum	k1gNnSc1	dvojalbum
<g/>
:	:	kIx,	:
koncertní	koncertní	k2eAgInSc1d1	koncertní
a	a	k8xC	a
studiový	studiový	k2eAgInSc1d1	studiový
disk	disk	k1gInSc1	disk
<g/>
)	)	kIx)	)
Atom	atom	k1gInSc1	atom
Heart	Hearta	k1gFnPc2	Hearta
Mother	Mothra	k1gFnPc2	Mothra
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Meddle	Meddle	k1gFnSc2	Meddle
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Dark	Darko	k1gNnPc2	Darko
Side	Side	k1gNnPc2	Side
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Wish	Wish	k1gInSc1	Wish
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Animals	Animalsa	k1gFnPc2	Animalsa
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
dvojalbum	dvojalbum	k1gNnSc1	dvojalbum
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Cut	Cut	k1gMnSc1	Cut
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
A	a	k8xC	a
Momentary	Momentar	k1gInPc1	Momentar
Lapse	lapsus	k1gInSc5	lapsus
of	of	k?	of
Reason	Reason	k1gNnSc4	Reason
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
The	The	k1gFnPc2	The
Endless	Endless	k1gInSc1	Endless
River	River	k1gMnSc1	River
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
