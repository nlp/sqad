<s>
Nephila	Nephila	k1gFnSc1
komaci	komace	k1gFnSc4
</s>
<s>
Nephila	Nephila	k1gFnSc1
komaci	komace	k1gFnSc4
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
klepítkatci	klepítkatec	k1gMnPc1
(	(	kIx(
<g/>
Chelicerata	Chelicerat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
pavoukovci	pavoukovec	k1gMnPc1
(	(	kIx(
<g/>
Arachnida	Arachnida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
pavouci	pavouk	k1gMnPc1
(	(	kIx(
<g/>
Araneae	Araneae	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
nefilovití	filovitý	k2eNgMnPc1d1
(	(	kIx(
<g/>
Nephilidae	Nephilidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
nefila	nefila	k1gFnSc1
(	(	kIx(
<g/>
Nephila	Nephila	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Nephila	Nephila	k1gFnSc1
komaciKuntner	komaciKuntner	k1gInSc1
&	&	k?
Coddington	Coddington	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nephila	Nephila	k1gFnSc1
komaci	komaci	k1gFnSc1
je	být	k5eAaImIp3nS
ohrožený	ohrožený	k2eAgInSc1d1
druh	druh	k1gInSc1
velkého	velký	k2eAgMnSc2d1
pavouka	pavouk	k1gMnSc2
žijícího	žijící	k2eAgMnSc2d1
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
na	na	k7c6
Madagaskaru	Madagaskar	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc4
dokáže	dokázat	k5eAaPmIp3nS
utkat	utkat	k5eAaPmF
spirálovité	spirálovitý	k2eAgFnSc2d1
kruhové	kruhový	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
o	o	k7c6
průměru	průměr	k1gInSc6
až	až	k9
100	#num#	k4
cm	cm	kA
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
samice	samice	k1gFnSc1
dorůstá	dorůstat	k5eAaImIp3nS
velikosti	velikost	k1gFnSc2
12	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevili	objevit	k5eAaPmAgMnP
ho	on	k3xPp3gMnSc4
dva	dva	k4xCgMnPc1
biologové	biolog	k1gMnPc1
–	–	k?
Slovinec	Slovinec	k1gMnSc1
Matjaz	Matjaz	k1gInSc4
Kuntner	Kuntner	k1gMnSc1
a	a	k8xC
Američan	Američan	k1gMnSc1
Jonathan	Jonathan	k1gMnSc1
Coddington	Coddington	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kuntner	Kuntner	k1gInSc1
našel	najít	k5eAaPmAgInS
jednu	jeden	k4xCgFnSc4
samici	samice	k1gFnSc4
ve	v	k7c6
sbírkách	sbírka	k1gFnPc6
Výzkumného	výzkumný	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
rostlin	rostlina	k1gFnPc2
v	v	k7c6
jihoafrické	jihoafrický	k2eAgFnSc6d1
Pretorii	Pretorie	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
protože	protože	k8xS
ani	ani	k8xC
po	po	k7c6
usilovném	usilovný	k2eAgNnSc6d1
hledání	hledání	k1gNnSc6
v	v	k7c6
jiných	jiný	k2eAgNnPc6d1
muzeích	muzeum	k1gNnPc6
nenašel	najít	k5eNaPmAgMnS
žádný	žádný	k3yNgInSc4
další	další	k2eAgInSc4d1
vzorek	vzorek	k1gInSc4
<g/>
,	,	kIx,
domníval	domnívat	k5eAaImAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
vyhynulý	vyhynulý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
ale	ale	k8xC
Coddinngton	Coddinngton	k1gInSc1
nalezl	naleznout	k5eAaPmAgInS,k5eAaBmAgInS
tři	tři	k4xCgInPc4
exempláře	exemplář	k1gInPc4
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
v	v	k7c6
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2020.3	2020.3	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
SURÁ	SURÁ	kA
<g/>
,	,	kIx,
Martina	Martina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
objevili	objevit	k5eAaPmAgMnP
obra	obr	k1gMnSc4
mezi	mezi	k7c4
pavouky	pavouk	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
spřádají	spřádat	k5eAaImIp3nP
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-10-23	2009-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
