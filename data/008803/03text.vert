<p>
<s>
Věčně	věčně	k6eAd1	věčně
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
pod	pod	k7c7	pod
anglickým	anglický	k2eAgInSc7d1	anglický
termínem	termín	k1gInSc7	termín
permafrost	permafrost	k1gFnSc1	permafrost
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
sloučením	sloučení	k1gNnSc7	sloučení
"	"	kIx"	"
<g/>
permanent	permanent	k1gInSc1	permanent
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
frost	frost	k1gFnSc1	frost
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
stále	stále	k6eAd1	stále
zmrzlý	zmrzlý	k2eAgMnSc1d1	zmrzlý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pergelisol	pergelisol	k1gInSc4	pergelisol
je	být	k5eAaImIp3nS	být
půda	půda	k1gFnSc1	půda
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ani	ani	k8xC	ani
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
nerozmrzá	rozmrzat	k5eNaImIp3nS	rozmrzat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
část	část	k1gFnSc1	část
litosféry	litosféra	k1gFnSc2	litosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgNnPc2	dva
let	let	k1gInSc4	let
teplotu	teplota	k1gFnSc4	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Mocnost	mocnost	k1gFnSc1	mocnost
permafrostu	permafrost	k1gInSc2	permafrost
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
několika	několik	k4yIc2	několik
cm	cm	kA	cm
až	až	k9	až
po	po	k7c4	po
1	[number]	k4	1
450	[number]	k4	450
m	m	kA	m
(	(	kIx(	(
<g/>
promrzá	promrzat	k5eAaImIp3nS	promrzat
nejen	nejen	k6eAd1	nejen
vlastní	vlastní	k2eAgFnSc1d1	vlastní
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
podloží	podložit	k5eAaPmIp3nS	podložit
<g/>
)	)	kIx)	)
a	a	k8xC	a
značně	značně	k6eAd1	značně
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
permafrostu	permafrost	k1gInSc6	permafrost
je	být	k5eAaImIp3nS	být
vegetace	vegetace	k1gFnSc1	vegetace
omezena	omezit	k5eAaPmNgFnS	omezit
hloubkou	hloubka	k1gFnSc7	hloubka
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yIgFnSc2	jaký
permafrost	permafrost	k1gFnSc1	permafrost
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
permafrost	permafrost	k1gFnSc1	permafrost
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
věčného	věčný	k2eAgInSc2d1	věčný
ledu	led	k1gInSc2	led
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tundry	tundra	k1gFnSc2	tundra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tundře	tundra	k1gFnSc6	tundra
přes	přes	k7c4	přes
krátké	krátký	k2eAgNnSc4d1	krátké
léto	léto	k1gNnSc4	léto
povolí	povolit	k5eAaPmIp3nS	povolit
povrchový	povrchový	k2eAgInSc1d1	povrchový
led	led	k1gInSc1	led
a	a	k8xC	a
na	na	k7c6	na
tenké	tenký	k2eAgFnSc6d1	tenká
vrstvě	vrstva	k1gFnSc6	vrstva
zeminy	zemina	k1gFnSc2	zemina
třeba	třeba	k6eAd1	třeba
i	i	k9	i
odkvetou	odkvést	k5eAaPmIp3nP	odkvést
byliny	bylina	k1gFnPc1	bylina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
podloží	podloží	k1gNnSc1	podloží
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zmrzlé	zmrzlý	k2eAgNnSc1d1	zmrzlé
trvale	trvale	k6eAd1	trvale
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
teplejšími	teplý	k2eAgNnPc7d2	teplejší
léty	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
tajga	tajga	k1gFnSc1	tajga
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
půdy	půda	k1gFnSc2	půda
po	po	k7c6	po
permafrostu	permafrost	k1gInSc6	permafrost
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
fenomén	fenomén	k1gInSc1	fenomén
zvaný	zvaný	k2eAgInSc1d1	zvaný
opilý	opilý	k2eAgInSc1d1	opilý
les	les	k1gInSc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
biotopy	biotop	k1gInPc4	biotop
např.	např.	kA	např.
asijských	asijský	k2eAgMnPc2d1	asijský
sobů	sob	k1gMnPc2	sob
<g/>
,	,	kIx,	,
losů	los	k1gMnPc2	los
nebo	nebo	k8xC	nebo
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
karibu	karibu	k1gMnPc2	karibu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Permafrost	Permafrost	k1gFnSc1	Permafrost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
pod	pod	k7c7	pod
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
:	:	kIx,	:
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnSc6d1	ledová
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
konci	konec	k1gInSc6	konec
byl	být	k5eAaImAgMnS	být
zatopen	zatopen	k2eAgMnSc1d1	zatopen
díky	díky	k7c3	díky
zvýšení	zvýšení	k1gNnSc3	zvýšení
hladiny	hladina	k1gFnSc2	hladina
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nestačí	stačit	k5eNaBmIp3nS	stačit
k	k	k7c3	k
roztání	roztání	k1gNnSc3	roztání
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
permafrost	permafrost	k1gFnSc1	permafrost
stále	stále	k6eAd1	stále
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Permafrost	Permafrost	k1gFnSc1	Permafrost
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jak	jak	k8xS	jak
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
jako	jako	k8xC	jako
například	například	k6eAd1	například
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
či	či	k8xC	či
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
permafrostem	permafrost	k1gFnPc3	permafrost
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
specifické	specifický	k2eAgFnPc1d1	specifická
geomorfologické	geomorfologický	k2eAgFnPc1d1	geomorfologická
formy	forma	k1gFnPc1	forma
jako	jako	k8xS	jako
kamenné	kamenný	k2eAgInPc1d1	kamenný
věnce	věnec	k1gInPc1	věnec
<g/>
,	,	kIx,	,
bajdžarachy	bajdžarach	k1gInPc1	bajdžarach
<g/>
,	,	kIx,	,
kryoplanační	kryoplanační	k2eAgFnPc1d1	kryoplanační
terasy	terasa	k1gFnPc1	terasa
nebo	nebo	k8xC	nebo
pingo	pingo	k1gNnSc1	pingo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
permafrostu	permafrost	k1gInSc2	permafrost
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
<g/>
,	,	kIx,	,
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
permafrost	permafrost	k1gFnSc4	permafrost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
souvislý	souvislý	k2eAgInSc1d1	souvislý
–	–	k?	–
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zabírá	zabírat	k5eAaImIp3nS	zabírat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
%	%	kIx~	%
plochy	plocha	k1gFnPc1	plocha
<g/>
;	;	kIx,	;
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
MAAT	MAAT	kA	MAAT
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
nesouvislý	souvislý	k2eNgInSc1d1	nesouvislý
–	–	k?	–
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zabírá	zabírat	k5eAaImIp3nS	zabírat
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
</s>
</p>
<p>
<s>
sporadický	sporadický	k2eAgInSc4d1	sporadický
(	(	kIx(	(
<g/>
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
<g/>
)	)	kIx)	)
<g/>
–	–	k?	–
zabírá	zabírat	k5eAaImIp3nS	zabírat
méně	málo	k6eAd2	málo
než	než	k8xS	než
30	[number]	k4	30
%	%	kIx~	%
plochyNa	plochyNa	k6eAd1	plochyNa
rozšíření	rozšíření	k1gNnSc1	rozšíření
permafrostu	permafrost	k1gInSc2	permafrost
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
orientace	orientace	k1gFnSc1	orientace
svahů	svah	k1gInPc2	svah
–	–	k?	–
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c6	v
extrémně	extrémně	k6eAd1	extrémně
zastíněných	zastíněný	k2eAgFnPc6d1	zastíněná
oblastech	oblast	k1gFnPc6	oblast
severních	severní	k2eAgMnPc2d1	severní
nebo	nebo	k8xC	nebo
východních	východní	k2eAgInPc2d1	východní
svahů	svah	k1gInPc2	svah
</s>
</p>
<p>
<s>
bloková	blokový	k2eAgNnPc1d1	Blokové
pole	pole	k1gNnPc1	pole
<g/>
,	,	kIx,	,
kamenná	kamenný	k2eAgNnPc1d1	kamenné
moře	moře	k1gNnPc1	moře
(	(	kIx(	(
<g/>
snižují	snižovat	k5eAaImIp3nP	snižovat
teplotu	teplota	k1gFnSc4	teplota
okolí	okolí	k1gNnSc2	okolí
o	o	k7c6	o
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
svahy	svah	k1gInPc1	svah
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
menším	malý	k2eAgInSc7d2	menší
než	než	k8xS	než
30	[number]	k4	30
<g/>
°	°	k?	°
</s>
</p>
<p>
<s>
místa	místo	k1gNnPc1	místo
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
sněžníků	sněžník	k1gInPc2	sněžník
(	(	kIx(	(
<g/>
akumulací	akumulace	k1gFnSc7	akumulace
sněhu	sníh	k1gInSc2	sníh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgNnPc4d1	přírodní
rizika	riziko	k1gNnPc4	riziko
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
permafrostem	permafrost	k1gFnPc3	permafrost
==	==	k?	==
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
věčně	věčně	k6eAd1	věčně
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
půdy	půda	k1gFnSc2	půda
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
zakládání	zakládání	k1gNnSc1	zakládání
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
těžbu	těžba	k1gFnSc4	těžba
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozmrzání	rozmrzání	k1gNnSc3	rozmrzání
svrchní	svrchní	k2eAgFnSc2d1	svrchní
části	část	k1gFnSc2	část
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
pohyblivým	pohyblivý	k2eAgMnSc7d1	pohyblivý
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
půdotok	půdotok	k1gInSc1	půdotok
(	(	kIx(	(
<g/>
soliflukce	soliflukce	k1gFnSc1	soliflukce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stavební	stavební	k2eAgFnSc7d1	stavební
pohromou	pohroma	k1gFnSc7	pohroma
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
činí	činit	k5eAaImIp3nS	činit
podloží	podloží	k1gNnSc1	podloží
staveb	stavba	k1gFnPc2	stavba
nestabilním	stabilní	k2eNgNnSc7d1	nestabilní
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
termokras	termokras	k1gInSc1	termokras
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukčním	konstrukční	k2eAgNnSc7d1	konstrukční
řešením	řešení	k1gNnSc7	řešení
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
permafrostu	permafrost	k1gInSc6	permafrost
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgNnSc1d1	pevné
zakotvení	zakotvení	k1gNnSc1	zakotvení
staveb	stavba	k1gFnPc2	stavba
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
letní	letní	k2eAgNnPc4d1	letní
rozmrzání	rozmrzání	k1gNnPc4	rozmrzání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
nutno	nutno	k6eAd1	nutno
dům	dům	k1gInSc4	dům
kvalitně	kvalitně	k6eAd1	kvalitně
odizolovat	odizolovat	k5eAaPmF	odizolovat
od	od	k7c2	od
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
důsledkem	důsledek	k1gInSc7	důsledek
vytápění	vytápění	k1gNnSc2	vytápění
stavby	stavba	k1gFnSc2	stavba
roztavit	roztavit	k5eAaPmF	roztavit
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
permafrostu	permafrost	k1gInSc6	permafrost
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
náhle	náhle	k6eAd1	náhle
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
průniku	průnik	k1gInSc3	průnik
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
vytopení	vytopení	k1gNnSc2	vytopení
a	a	k8xC	a
zhroucení	zhroucení	k1gNnSc2	zhroucení
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
domu	dům	k1gInSc2	dům
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
velice	velice	k6eAd1	velice
malé	malý	k2eAgFnPc4d1	malá
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kromě	kromě	k7c2	kromě
utopení	utopení	k1gNnSc2	utopení
jim	on	k3xPp3gMnPc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
i	i	k9	i
umrznutí	umrznutí	k1gNnSc1	umrznutí
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
je	být	k5eAaImIp3nS	být
kumulace	kumulace	k1gFnSc1	kumulace
methanu	methan	k1gInSc2	methan
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
následné	následný	k2eAgNnSc4d1	následné
vybuchnutí	vybuchnutí	k1gNnSc4	vybuchnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Permafrost	Permafrost	k1gFnSc1	Permafrost
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
také	také	k9	také
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
paleontologů	paleontolog	k1gMnPc2	paleontolog
<g/>
,	,	kIx,	,
archeologů	archeolog	k1gMnPc2	archeolog
a	a	k8xC	a
mikrobiologů	mikrobiolog	k1gMnPc2	mikrobiolog
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zmrzlé	zmrzlý	k2eAgFnSc6d1	zmrzlá
půdě	půda	k1gFnSc6	půda
se	se	k3xPyFc4	se
ve	v	k7c6	v
výborném	výborný	k2eAgInSc6d1	výborný
stavu	stav	k1gInSc6	stav
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
mršiny	mršina	k1gFnPc1	mršina
mamutů	mamut	k1gMnPc2	mamut
i	i	k8xC	i
dalších	další	k2eAgNnPc2d1	další
pleistocénních	pleistocénní	k2eAgNnPc2d1	pleistocénní
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
po	po	k7c6	po
desítkách	desítka	k1gFnPc6	desítka
až	až	k8xS	až
stovkách	stovka	k1gFnPc6	stovka
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
dokonce	dokonce	k9	dokonce
stále	stále	k6eAd1	stále
živé	živý	k2eAgInPc1d1	živý
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
(	(	kIx(	(
<g/>
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
viry	vir	k1gInPc1	vir
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Permafrost	Permafrost	k1gFnSc1	Permafrost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Problém	problém	k1gInSc1	problém
<g/>
:	:	kIx,	:
Sibiř	Sibiř	k1gFnSc1	Sibiř
taje	tát	k5eAaImIp3nS	tát
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
miliardy	miliarda	k4xCgFnPc4	miliarda
tun	tuna	k1gFnPc2	tuna
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
/	/	kIx~	/
<g/>
Reuters	Reuters	k1gInSc1	Reuters
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
o	o	k7c6	o
souvislosti	souvislost	k1gFnSc6	souvislost
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
,	,	kIx,	,
metanu	metan	k1gInSc2	metan
a	a	k8xC	a
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
(	(	kIx(	(
<g/>
Britské	britský	k2eAgInPc1d1	britský
listy	list	k1gInPc1	list
<g/>
)	)	kIx)	)
</s>
</p>
