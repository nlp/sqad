<s>
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Barbora	Barbora	k1gFnSc1	Barbora
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Barbara	Barbara	k1gFnSc1	Barbara
Pankel	Pankela	k1gFnPc2	Pankela
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1820	[number]	k4	1820
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1862	[number]	k4	1862
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zakladatelku	zakladatelka	k1gFnSc4	zakladatelka
novodobé	novodobý	k2eAgFnSc2d1	novodobá
české	český	k2eAgFnSc2d1	Česká
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Příjmení	příjmení	k1gNnSc1	příjmení
Panklová	Panklová	k1gFnSc1	Panklová
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
německy	německy	k6eAd1	německy
Pankel	Pankela	k1gFnPc2	Pankela
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
<g/>
,	,	kIx,	,
až	až	k8xS	až
když	když	k8xS	když
si	se	k3xPyFc3	se
její	její	k3xOp3gFnSc4	její
matku	matka	k1gFnSc4	matka
Terezii	Terezie	k1gFnSc4	Terezie
Novotnou	Novotná	k1gFnSc7	Novotná
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
vzal	vzít	k5eAaPmAgInS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
kočí	kočí	k2eAgMnSc1d1	kočí
Johann	Johann	k1gMnSc1	Johann
Pankel	Pankel	k1gMnSc1	Pankel
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Barboře	Barbora	k1gFnSc3	Barbora
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
-	-	kIx~	-
podle	podle	k7c2	podle
ustáleného	ustálený	k2eAgNnSc2d1	ustálené
datování	datování	k1gNnSc2	datování
-	-	kIx~	-
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
se	se	k3xPyFc4	se
Panklovi	Panklův	k2eAgMnPc1d1	Panklův
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
do	do	k7c2	do
Ratibořic	Ratibořice	k1gFnPc2	Ratibořice
na	na	k7c6	na
panství	panství	k1gNnSc6	panství
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
nakrátko	nakrátko	k6eAd1	nakrátko
přistěhovala	přistěhovat	k5eAaPmAgFnS	přistěhovat
i	i	k8xC	i
její	její	k3xOp3gNnSc4	její
55	[number]	k4	55
<g/>
letá	letý	k2eAgFnSc1d1	letá
babička	babička	k1gFnSc1	babička
-	-	kIx~	-
Marie	Marie	k1gFnSc1	Marie
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Novotná	Novotná	k1gFnSc1	Novotná
rozená	rozený	k2eAgFnSc1d1	rozená
Čudová	Čudová	k1gFnSc1	Čudová
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
Dobruška-Křovice	Dobruška-Křovice	k1gFnSc1	Dobruška-Křovice
-	-	kIx~	-
1841	[number]	k4	1841
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
-	-	kIx~	-
která	který	k3yIgFnSc1	který
malou	malý	k2eAgFnSc4d1	malá
Barunku	Barunka	k1gFnSc4	Barunka
velmi	velmi	k6eAd1	velmi
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
<g/>
;	;	kIx,	;
v	v	k7c4	v
dospělosti	dospělost	k1gFnPc4	dospělost
si	se	k3xPyFc3	se
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
babičku	babička	k1gFnSc4	babička
velmi	velmi	k6eAd1	velmi
zidealizovala	zidealizovat	k5eAaPmAgFnS	zidealizovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
Barbora	Barbora	k1gFnSc1	Barbora
Panklová	Panklová	k1gFnSc1	Panklová
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Skalici	Skalice	k1gFnSc6	Skalice
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
jí	on	k3xPp3gFnSc3	on
rodiče	rodič	k1gMnPc1	rodič
našli	najít	k5eAaPmAgMnP	najít
ženicha	ženich	k1gMnSc4	ženich
Josefa	Josef	k1gMnSc4	Josef
Němce	Němec	k1gMnSc4	Němec
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
Nový	nový	k2eAgInSc1d1	nový
Bydžov	Bydžov	k1gInSc1	Bydžov
-	-	kIx~	-
1879	[number]	k4	1879
Tábor	Tábor	k1gInSc1	Tábor
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
slavena	slaven	k2eAgFnSc1d1	slavena
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
porodila	porodit	k5eAaPmAgFnS	porodit
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
komisař	komisař	k1gMnSc1	komisař
finanční	finanční	k2eAgFnSc2d1	finanční
stráže	stráž	k1gFnSc2	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
nadřízení	nadřízený	k1gMnPc1	nadřízený
s	s	k7c7	s
ostražitostí	ostražitost	k1gFnSc7	ostražitost
sledovali	sledovat	k5eAaImAgMnP	sledovat
jeho	jeho	k3xOp3gInPc4	jeho
projevy	projev	k1gInPc4	projev
češství	češství	k1gNnSc2	češství
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
malé	malý	k2eAgFnSc2d1	malá
služební	služební	k2eAgFnSc2d1	služební
horlivosti	horlivost	k1gFnSc2	horlivost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
služebně	služebně	k6eAd1	služebně
překládán	překládat	k5eAaImNgInS	překládat
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
stěhovat	stěhovat	k5eAaImF	stěhovat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
nebylo	být	k5eNaImAgNnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
služebně	služebně	k6eAd1	služebně
přeložen	přeložit	k5eAaPmNgMnS	přeložit
do	do	k7c2	do
Josefova	Josefov	k1gInSc2	Josefov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
narodilo	narodit	k5eAaPmAgNnS	narodit
první	první	k4xOgNnSc1	první
dítě	dítě	k1gNnSc1	dítě
-	-	kIx~	-
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
rodina	rodina	k1gFnSc1	rodina
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
narodil	narodit	k5eAaPmAgMnS	narodit
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
se	se	k3xPyFc4	se
léčila	léčit	k5eAaImAgFnS	léčit
u	u	k7c2	u
doktora	doktor	k1gMnSc2	doktor
J.	J.	kA	J.
Čejky	Čejka	k1gMnSc2	Čejka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
spřátelila	spřátelit	k5eAaPmAgFnS	spřátelit
a	a	k8xC	a
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
tehdejšími	tehdejší	k2eAgInPc7d1	tehdejší
vlastenecky	vlastenecky	k6eAd1	vlastenecky
smýšlejícími	smýšlející	k2eAgMnPc7d1	smýšlející
českými	český	k2eAgMnPc7d1	český
spisovateli	spisovatel	k1gMnPc7	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
do	do	k7c2	do
Polné	Polná	k1gFnSc2	Polná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
respicientem	respicient	k1gMnSc7	respicient
finančním	finanční	k2eAgInPc3d1	finanční
expozitury	expozitura	k1gFnPc4	expozitura
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
mužovým	mužův	k2eAgNnSc7d1	mužovo
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
sblížila	sblížit	k5eAaPmAgFnS	sblížit
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
obrozenci	obrozenec	k1gMnPc7	obrozenec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
hostinským	hostinský	k1gMnSc7	hostinský
a	a	k8xC	a
primátorem	primátor	k1gMnSc7	primátor
města	město	k1gNnSc2	město
Antonínem	Antonín	k1gMnSc7	Antonín
Pittnerem	Pittner	k1gMnSc7	Pittner
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
ochotnické	ochotnický	k2eAgNnSc4d1	ochotnické
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
četli	číst	k5eAaImAgMnP	číst
české	český	k2eAgFnPc4d1	Česká
noviny	novina	k1gFnPc4	novina
Květy	Květa	k1gFnSc2	Květa
a	a	k8xC	a
Českou	český	k2eAgFnSc4d1	Česká
včelu	včela	k1gFnSc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dopisu	dopis	k1gInSc2	dopis
psanému	psaný	k2eAgMnSc3d1	psaný
Ludvíku	Ludvík	k1gMnSc3	Ludvík
rytíři	rytíř	k1gMnSc3	rytíř
z	z	k7c2	z
Rittersberka	Rittersberka	k1gFnSc1	Rittersberka
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
české	český	k2eAgFnSc2d1	Česká
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
knihu	kniha	k1gFnSc4	kniha
přeloženou	přeložený	k2eAgFnSc4d1	přeložená
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
knihu	kniha	k1gFnSc4	kniha
Roberta	Robert	k1gMnSc4	Robert
Irwina	Irwin	k1gMnSc4	Irwin
Alhambra	Alhambr	k1gMnSc4	Alhambr
a	a	k8xC	a
spisy	spis	k1gInPc4	spis
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	Tyl	k1gMnSc2	Tyl
<g/>
.	.	kIx.	.
</s>
<s>
Němcovým	Němcová	k1gFnPc3	Němcová
se	se	k3xPyFc4	se
v	v	k7c6	v
Polné	Polná	k1gFnSc6	Polná
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Theodora	Theodora	k1gFnSc1	Theodora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tu	ten	k3xDgFnSc4	ten
byla	být	k5eAaImAgFnS	být
pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1841	[number]	k4	1841
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
přeložen	přeložit	k5eAaPmNgMnS	přeložit
a	a	k8xC	a
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1842	[number]	k4	1842
narodil	narodit	k5eAaPmAgMnS	narodit
třetí	třetí	k4xOgMnSc1	třetí
syn	syn	k1gMnSc1	syn
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
začala	začít	k5eAaPmAgFnS	začít
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Václava	Václav	k1gMnSc2	Václav
Bolemíra	Bolemír	k1gMnSc2	Bolemír
Nebeského	nebeský	k2eAgMnSc2d1	nebeský
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
psát	psát	k5eAaImF	psát
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
báseň	báseň	k1gFnSc1	báseň
Ženám	žena	k1gFnPc3	žena
českým	český	k2eAgMnPc3d1	český
byla	být	k5eAaImAgNnP	být
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
Nebeského	nebeský	k2eAgInSc2d1	nebeský
otištěna	otištěn	k2eAgFnSc1d1	otištěna
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1843	[number]	k4	1843
v	v	k7c6	v
Květech	květ	k1gInPc6	květ
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jemným	jemný	k2eAgMnSc7d1	jemný
a	a	k8xC	a
vzdělaným	vzdělaný	k2eAgMnSc7d1	vzdělaný
básníkem	básník	k1gMnSc7	básník
Václavem	Václav	k1gMnSc7	Václav
Bolemírem	Bolemír	k1gMnSc7	Bolemír
Nebeským	nebeský	k2eAgMnSc7d1	nebeský
se	se	k3xPyFc4	se
Němcová	Němcová	k1gFnSc1	Němcová
citově	citově	k6eAd1	citově
sblížila	sblížit	k5eAaPmAgFnS	sblížit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Nebeský	nebeský	k2eAgInSc1d1	nebeský
milenecký	milenecký	k2eAgInSc1d1	milenecký
vztah	vztah	k1gInSc1	vztah
záhy	záhy	k6eAd1	záhy
ukončil	ukončit	k5eAaPmAgInS	ukončit
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
jeho	jeho	k3xOp3gFnSc1	jeho
korespondence	korespondence	k1gFnSc1	korespondence
<g/>
)	)	kIx)	)
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
dokončit	dokončit	k5eAaPmF	dokončit
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Němcová	Němcová	k1gFnSc1	Němcová
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
těžce	těžce	k6eAd1	těžce
roznemohla	roznemoct	k5eAaPmAgFnS	roznemoct
a	a	k8xC	a
trápily	trápit	k5eAaImAgFnP	trápit
ji	on	k3xPp3gFnSc4	on
vysoké	vysoký	k2eAgFnPc1d1	vysoká
horečky	horečka	k1gFnPc1	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
se	se	k3xPyFc4	se
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
do	do	k7c2	do
Domažlic	Domažlice	k1gFnPc2	Domažlice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Němcová	Němcová	k1gFnSc1	Němcová
působila	působit	k5eAaImAgFnS	působit
jako	jako	k8xS	jako
prakticky	prakticky	k6eAd1	prakticky
první	první	k4xOgMnSc1	první
národní	národní	k2eAgMnSc1d1	národní
buditel	buditel	k1gMnSc1	buditel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
domu	dům	k1gInSc2	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
č.	č.	k?	č.
120	[number]	k4	120
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
textem	text	k1gInSc7	text
Zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgFnS	žít
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
první	první	k4xOgFnSc1	první
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
česká	český	k2eAgFnSc1d1	Česká
o	o	k7c4	o
Domažlicko	Domažlicko	k1gNnSc4	Domažlicko
nad	nad	k7c4	nad
jiné	jiný	k2eAgFnPc4d1	jiná
zasloužilá	zasloužilý	k2eAgNnPc4d1	zasloužilé
<g/>
.	.	kIx.	.
</s>
<s>
Sestry	sestra	k1gFnPc1	sestra
sestře	sestra	k1gFnSc6	sestra
<g/>
.	.	kIx.	.
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
z	z	k7c2	z
Domažlic	Domažlice	k1gFnPc2	Domažlice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
spiknutí	spiknutí	k1gNnSc2	spiknutí
proti	proti	k7c3	proti
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nucenému	nucený	k2eAgNnSc3d1	nucené
stěhování	stěhování	k1gNnSc3	stěhování
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
jej	on	k3xPp3gMnSc4	on
přeložili	přeložit	k5eAaPmAgMnP	přeložit
až	až	k9	až
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Němcová	Němcová	k1gFnSc1	Němcová
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
dětmi	dítě	k1gFnPc7	dítě
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
okamžitě	okamžitě	k6eAd1	okamžitě
navázala	navázat	k5eAaPmAgFnS	navázat
styky	styk	k1gInPc4	styk
s	s	k7c7	s
literárně	literárně	k6eAd1	literárně
činnými	činný	k2eAgMnPc7d1	činný
vlastenci	vlastenec	k1gMnPc7	vlastenec
<g/>
.	.	kIx.	.
</s>
<s>
Živila	živit	k5eAaImAgFnS	živit
se	se	k3xPyFc4	se
praním	praní	k1gNnSc7	praní
<g/>
,	,	kIx,	,
úklidem	úklid	k1gInSc7	úklid
a	a	k8xC	a
příležitostným	příležitostný	k2eAgNnSc7d1	příležitostné
psaním	psaní	k1gNnSc7	psaní
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
žila	žít	k5eAaImAgFnS	žít
na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
Josefa	Josef	k1gMnSc2	Josef
Němce	Němec	k1gMnSc2	Němec
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
po	po	k7c6	po
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Domažlicích	Domažlice	k1gFnPc6	Domažlice
a	a	k8xC	a
Všerubech	Všeruby	k1gInPc6	Všeruby
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
na	na	k7c4	na
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1848	[number]	k4	1848
do	do	k7c2	do
února	únor	k1gInSc2	únor
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
Nymburku	Nymburk	k1gInSc6	Nymburk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Boženě	Božena	k1gFnSc6	Božena
Němcové	Němcové	k2eAgFnSc6d1	Němcové
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
časopis	časopis	k1gInSc1	časopis
Týdenník	týdenník	k1gInSc1	týdenník
s	s	k7c7	s
článkem	článek	k1gInSc7	článek
Co	co	k3yInSc4	co
jest	být	k5eAaImIp3nS	být
komunismus	komunismus	k1gInSc1	komunismus
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
utopický	utopický	k2eAgMnSc1d1	utopický
socialista	socialista	k1gMnSc1	socialista
František	František	k1gMnSc1	František
Matouš	Matouš	k1gMnSc1	Matouš
Klácel	Klácel	k1gMnSc1	Klácel
<g/>
.	.	kIx.	.
</s>
<s>
Boženu	Božena	k1gFnSc4	Božena
Němcovou	Němcová	k1gFnSc4	Němcová
článek	článek	k1gInSc1	článek
natolik	natolik	k6eAd1	natolik
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
projevila	projevit	k5eAaPmAgFnS	projevit
zájem	zájem	k1gInSc4	zájem
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Klácelova	Klácelův	k2eAgNnSc2d1	Klácelův
Českomoravského	českomoravský	k2eAgNnSc2d1	Českomoravské
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
rakouským	rakouský	k2eAgInPc3d1	rakouský
úřadům	úřad	k1gInPc3	úřad
krajně	krajně	k6eAd1	krajně
podezřelé	podezřelý	k2eAgNnSc1d1	podezřelé
utopické	utopický	k2eAgNnSc1d1	utopické
společenství	společenství	k1gNnSc1	společenství
bratrské	bratrský	k2eAgFnSc2d1	bratrská
a	a	k8xC	a
sesterské	sesterský	k2eAgFnSc2d1	sesterská
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Klácel	Klácet	k5eAaImAgInS	Klácet
si	se	k3xPyFc3	se
potom	potom	k6eAd1	potom
s	s	k7c7	s
Němcovou	Němcová	k1gFnSc7	Němcová
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
a	a	k8xC	a
adresoval	adresovat	k5eAaBmAgMnS	adresovat
jí	jíst	k5eAaImIp3nS	jíst
sedmatřicet	sedmatřicet	k4xCc4	sedmatřicet
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
Moravských	moravský	k2eAgFnPc6d1	Moravská
novinách	novina	k1gFnPc6	novina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
vydal	vydat	k5eAaPmAgMnS	vydat
tiskem	tisek	k1gMnSc7	tisek
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Listy	lista	k1gFnSc2	lista
přítele	přítel	k1gMnSc2	přítel
k	k	k7c3	k
přítelkyni	přítelkyně	k1gFnSc3	přítelkyně
o	o	k7c6	o
půwodu	půwod	k1gInSc6	půwod
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Českomoravského	českomoravský	k2eAgNnSc2d1	Českomoravské
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
se	se	k3xPyFc4	se
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
dalším	další	k2eAgMnSc7d1	další
milencem	milenec	k1gMnSc7	milenec
MUDr.	MUDr.	kA	MUDr.
Janem	Jan	k1gMnSc7	Jan
Helceletem	Helcelet	k1gInSc7	Helcelet
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
říkala	říkat	k5eAaImAgFnS	říkat
"	"	kIx"	"
<g/>
bratr	bratr	k1gMnSc1	bratr
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
pobývala	pobývat	k5eAaImAgFnS	pobývat
Němcová	Němcová	k1gFnSc1	Němcová
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
Na	na	k7c6	na
Horách	hora	k1gFnPc6	hora
u	u	k7c2	u
České	český	k2eAgFnSc2d1	Česká
Třebové	Třebová	k1gFnSc2	Třebová
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ji	on	k3xPp3gFnSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
Klácel	Klácel	k1gMnSc1	Klácel
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
setkala	setkat	k5eAaPmAgFnS	setkat
i	i	k9	i
s	s	k7c7	s
Helceletem	Helcele	k1gNnSc7	Helcele
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřejmě	zřejmě	k6eAd1	zřejmě
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozporům	rozpor	k1gInPc3	rozpor
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
Helcelet	Helcelet	k1gInSc1	Helcelet
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
korespondenci	korespondence	k1gFnSc6	korespondence
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
nazýval	nazývat	k5eAaImAgMnS	nazývat
Němcovou	Němcová	k1gFnSc4	Němcová
posměšně	posměšně	k6eAd1	posměšně
"	"	kIx"	"
<g/>
slezinou	slezina	k1gFnSc7	slezina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
snad	snad	k9	snad
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
často	často	k6eAd1	často
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
na	na	k7c4	na
chorobu	choroba	k1gFnSc4	choroba
tohoto	tento	k3xDgInSc2	tento
orgánu	orgán	k1gInSc2	orgán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Českomoravské	českomoravský	k2eAgNnSc4d1	Českomoravské
bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
pěstovat	pěstovat	k5eAaImF	pěstovat
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
neslavně	slavně	k6eNd1	slavně
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
zřejmě	zřejmě	k6eAd1	zřejmě
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
Klácelova	Klácelův	k2eAgFnSc1d1	Klácelova
žárlivost	žárlivost	k1gFnSc1	žárlivost
na	na	k7c4	na
vztah	vztah	k1gInSc4	vztah
Němcové	Němcová	k1gFnSc2	Němcová
s	s	k7c7	s
Helceletem	Helcelet	k1gInSc7	Helcelet
<g/>
.	.	kIx.	.
</s>
<s>
Rozčarování	rozčarování	k1gNnSc1	rozčarování
z	z	k7c2	z
nevydařeného	vydařený	k2eNgInSc2d1	nevydařený
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Helceletem	Helcele	k1gNnSc7	Helcele
nemělo	mít	k5eNaImAgNnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
hned	hned	k6eAd1	hned
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
se	se	k3xPyFc4	se
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
MUDr.	MUDr.	kA	MUDr.
Dušana	Dušan	k1gMnSc2	Dušan
Lambla	Lambla	k1gMnSc2	Lambla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ošetřoval	ošetřovat	k5eAaImAgMnS	ošetřovat
jejího	její	k3xOp3gMnSc4	její
syna	syn	k1gMnSc4	syn
Hynka	Hynek	k1gMnSc4	Hynek
<g/>
,	,	kIx,	,
umírajícího	umírající	k2eAgMnSc4d1	umírající
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
rychle	rychle	k6eAd1	rychle
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
nalezl	nalézt	k5eAaBmAgMnS	nalézt
u	u	k7c2	u
manželky	manželka	k1gFnSc2	manželka
Lamblův	Lamblův	k2eAgInSc1d1	Lamblův
milostný	milostný	k2eAgInSc1d1	milostný
dopis	dopis	k1gInSc1	dopis
a	a	k8xC	a
ztropil	ztropit	k5eAaPmAgInS	ztropit
manželce	manželka	k1gFnSc3	manželka
hroznou	hrozný	k2eAgFnSc4d1	hrozná
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
odjela	odjet	k5eAaPmAgFnS	odjet
Němcová	Němcová	k1gFnSc1	Němcová
za	za	k7c7	za
manželem	manžel	k1gMnSc7	manžel
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
navštívila	navštívit	k5eAaPmAgFnS	navštívit
kromě	kromě	k7c2	kromě
Moravy	Morava	k1gFnSc2	Morava
také	také	k9	také
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sbírala	sbírat	k5eAaImAgFnS	sbírat
podklady	podklad	k1gInPc4	podklad
o	o	k7c6	o
zvycích	zvyk	k1gInPc6	zvyk
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
jazyce	jazyk	k1gInSc6	jazyk
tamějšího	tamější	k2eAgInSc2d1	tamější
venkovského	venkovský	k2eAgInSc2d1	venkovský
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
opakovaně	opakovaně	k6eAd1	opakovaně
vracela	vracet	k5eAaImAgFnS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
finančně	finančně	k6eAd1	finančně
podporoval	podporovat	k5eAaImAgMnS	podporovat
hrabě	hrabě	k1gMnSc1	hrabě
Hanuš	Hanuš	k1gMnSc1	Hanuš
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
Krakovský	krakovský	k2eAgMnSc1d1	krakovský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
podnikla	podniknout	k5eAaPmAgFnS	podniknout
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
cest	cesta	k1gFnPc2	cesta
také	také	k9	také
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
zemřel	zemřít	k5eAaPmAgMnS	zemřít
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
se	se	k3xPyFc4	se
Němcová	Němcová	k1gFnSc1	Němcová
znovu	znovu	k6eAd1	znovu
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
mladého	mladý	k2eAgMnSc2d1	mladý
studenta	student	k1gMnSc2	student
lékařství	lékařství	k1gNnSc2	lékařství
Hanuše	Hanuš	k1gMnSc2	Hanuš
Jurenky	Jurenka	k1gFnSc2	Jurenka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
tomuto	tento	k3xDgInSc3	tento
vztahu	vztah	k1gInSc3	vztah
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nepřáli	přát	k5eNaImAgMnP	přát
a	a	k8xC	a
Hanuš	Hanuš	k1gMnSc1	Hanuš
musel	muset	k5eAaImAgMnS	muset
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgMnS	moct
zapomenout	zapomenout	k5eAaPmF	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
odvolán	odvolat	k5eAaPmNgMnS	odvolat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
<g/>
;	;	kIx,	;
nejprve	nejprve	k6eAd1	nejprve
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
plat	plat	k1gInSc1	plat
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
donucena	donucen	k2eAgFnSc1d1	donucena
hledat	hledat	k5eAaImF	hledat
pomoc	pomoc	k1gFnSc4	pomoc
u	u	k7c2	u
známých	známá	k1gFnPc2	známá
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
často	často	k6eAd1	často
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
napsala	napsat	k5eAaPmAgFnS	napsat
povídku	povídka	k1gFnSc4	povídka
Babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
bydlela	bydlet	k5eAaImAgFnS	bydlet
v	v	k7c6	v
Ječné	ječný	k2eAgFnSc6d1	Ječná
ulici	ulice	k1gFnSc6	ulice
čp.	čp.	k?	čp.
516	[number]	k4	516
<g/>
/	/	kIx~	/
<g/>
28	[number]	k4	28
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
Vyšehradské	vyšehradský	k2eAgFnSc6d1	Vyšehradská
ulici	ulice	k1gFnSc6	ulice
čp.	čp.	k?	čp.
1378	[number]	k4	1378
<g/>
/	/	kIx~	/
<g/>
45	[number]	k4	45
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
Emauzy	Emauzy	k1gInPc7	Emauzy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
domech	dům	k1gInPc6	dům
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
umístěny	umístit	k5eAaPmNgFnP	umístit
pamětní	pamětní	k2eAgFnPc1d1	pamětní
desky	deska	k1gFnPc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
sblížila	sblížit	k5eAaPmAgFnS	sblížit
s	s	k7c7	s
májovci	májovec	k1gMnPc7	májovec
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
ale	ale	k9	ale
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
Karla	Karel	k1gMnSc2	Karel
Havlíčka	Havlíček	k1gMnSc2	Havlíček
Borovského	Borovského	k2eAgInSc1d1	Borovského
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
;	;	kIx,	;
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
rakev	rakev	k1gFnSc4	rakev
údajně	údajně	k6eAd1	údajně
položila	položit	k5eAaPmAgFnS	položit
trnovou	trnový	k2eAgFnSc4d1	Trnová
korunu	koruna	k1gFnSc4	koruna
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
mučednictví	mučednictví	k1gNnSc2	mučednictví
<g/>
.	.	kIx.	.
</s>
<s>
Seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	se	k3xPyFc4	se
se	s	k7c7	s
sestrami	sestra	k1gFnPc7	sestra
Rottovými	Rottův	k2eAgFnPc7d1	Rottův
<g/>
,	,	kIx,	,
spisovatelkami	spisovatelka	k1gFnPc7	spisovatelka
Karolinou	Karolina	k1gFnSc7	Karolina
Světlou	světlý	k2eAgFnSc7d1	světlá
a	a	k8xC	a
Sofií	Sofia	k1gFnSc7	Sofia
Podlipskou	Podlipský	k2eAgFnSc7d1	Podlipská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Němec	Němec	k1gMnSc1	Němec
místo	místo	k1gNnSc4	místo
účetního	účetní	k1gMnSc2	účetní
oficiála	oficiál	k1gMnSc2	oficiál
ve	v	k7c6	v
Villachu	Villach	k1gInSc6	Villach
v	v	k7c6	v
Korutanech	Korutany	k1gInPc6	Korutany
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
čase	čas	k1gInSc6	čas
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
povýšení	povýšení	k1gNnSc6	povýšení
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
zpronevěry	zpronevěra	k1gFnSc2	zpronevěra
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
penzionován	penzionovat	k5eAaBmNgInS	penzionovat
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
čekal	čekat	k5eAaImAgInS	čekat
ostrý	ostrý	k2eAgInSc1d1	ostrý
spor	spor	k1gInSc1	spor
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
stala	stát	k5eAaPmAgFnS	stát
budoucnost	budoucnost	k1gFnSc4	budoucnost
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
měli	mít	k5eAaImAgMnP	mít
manželé	manžel	k1gMnPc1	manžel
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
po	po	k7c6	po
těchto	tento	k3xDgInPc6	tento
rozporech	rozpor	k1gInPc6	rozpor
sepsal	sepsat	k5eAaPmAgInS	sepsat
koncept	koncept	k1gInSc1	koncept
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
nakonec	nakonec	k6eAd1	nakonec
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
svého	svůj	k3xOyFgMnSc4	svůj
a	a	k8xC	a
otce	otec	k1gMnSc4	otec
neposlechly	poslechnout	k5eNaPmAgFnP	poslechnout
-	-	kIx~	-
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
šel	jít	k5eAaImAgMnS	jít
učit	učit	k5eAaImF	učit
zahradníkem	zahradník	k1gMnSc7	zahradník
do	do	k7c2	do
Rájce	Rájec	k1gInSc2	Rájec
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
odjel	odjet	k5eAaPmAgMnS	odjet
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
na	na	k7c4	na
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
odjezdem	odjezd	k1gInSc7	odjezd
zbil	zbít	k5eAaPmAgMnS	zbít
rozzuřený	rozzuřený	k2eAgMnSc1d1	rozzuřený
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
svoji	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
musela	muset	k5eAaImAgFnS	muset
hledat	hledat	k5eAaImF	hledat
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
opakovaně	opakovaně	k6eAd1	opakovaně
usmiřovali	usmiřovat	k5eAaImAgMnP	usmiřovat
a	a	k8xC	a
zase	zase	k9	zase
hádali	hádat	k5eAaImAgMnP	hádat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
hádkám	hádka	k1gFnPc3	hádka
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
z	z	k7c2	z
Mnichova	Mnichov	k1gInSc2	Mnichov
vypovězen	vypovědět	k5eAaPmNgMnS	vypovědět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgInPc4	žádný
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
si	se	k3xPyFc3	se
sehnal	sehnat	k5eAaPmAgMnS	sehnat
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
fotografa	fotograf	k1gMnSc2	fotograf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
jednou	jeden	k4xCgFnSc7	jeden
nepřišel	přijít	k5eNaPmAgMnS	přijít
večer	večer	k6eAd1	večer
včas	včas	k6eAd1	včas
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
další	další	k2eAgFnSc1d1	další
dramatická	dramatický	k2eAgFnSc1d1	dramatická
hádka	hádka	k1gFnSc1	hádka
a	a	k8xC	a
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
musela	muset	k5eAaImAgFnS	muset
před	před	k7c7	před
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
opět	opět	k6eAd1	opět
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
už	už	k6eAd1	už
se	se	k3xPyFc4	se
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
odmítala	odmítat	k5eAaImAgFnS	odmítat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
znovu	znovu	k6eAd1	znovu
usmířit	usmířit	k5eAaPmF	usmířit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
živit	živit	k5eAaImF	živit
prací	práce	k1gFnSc7	práce
pro	pro	k7c4	pro
nakladatele	nakladatel	k1gMnSc4	nakladatel
Augustu	Augusta	k1gMnSc4	Augusta
<g/>
,	,	kIx,	,
když	když	k8xS	když
připravovala	připravovat	k5eAaImAgFnS	připravovat
vydání	vydání	k1gNnSc4	vydání
svých	svůj	k3xOyFgInPc2	svůj
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
vážně	vážně	k6eAd1	vážně
nemocná	nemocný	k2eAgFnSc1d1	nemocná
a	a	k8xC	a
finanční	finanční	k2eAgFnSc1d1	finanční
nouze	nouze	k1gFnSc1	nouze
a	a	k8xC	a
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
ji	on	k3xPp3gFnSc4	on
donutily	donutit	k5eAaPmAgFnP	donutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
k	k	k7c3	k
manželovi	manžel	k1gMnSc3	manžel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
sešit	sešit	k1gInSc1	sešit
II	II	kA	II
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
jejího	její	k3xOp3gNnSc2	její
nejslavnějšího	slavný	k2eAgNnSc2d3	nejslavnější
díla	dílo	k1gNnSc2	dílo
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
I.	I.	kA	I.
vydání	vydání	k1gNnSc1	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
dostala	dostat	k5eAaPmAgFnS	dostat
den	den	k1gInSc4	den
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1862	[number]	k4	1862
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Tří	tři	k4xCgFnPc2	tři
lip	lípa	k1gFnPc2	lípa
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
ulice	ulice	k1gFnSc2	ulice
Na	na	k7c6	na
Příkopě	příkop	k1gInSc6	příkop
čp.	čp.	k?	čp.
854	[number]	k4	854
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravdou	pravda	k1gFnSc7	pravda
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
neradostnému	radostný	k2eNgNnSc3d1	neradostné
manželství	manželství	k1gNnSc3	manželství
kromě	kromě	k7c2	kromě
evidentní	evidentní	k2eAgFnSc2d1	evidentní
intelektuální	intelektuální	k2eAgFnSc2d1	intelektuální
a	a	k8xC	a
povahové	povahový	k2eAgFnSc2d1	povahová
rozdílnosti	rozdílnost	k1gFnSc2	rozdílnost
obou	dva	k4xCgInPc2	dva
dvou	dva	k4xCgInPc2	dva
manželů	manžel	k1gMnPc2	manžel
přispěla	přispět	k5eAaPmAgFnS	přispět
Němcová	Němcová	k1gFnSc1	Němcová
svými	svůj	k3xOyFgFnPc7	svůj
milostnými	milostný	k2eAgFnPc7d1	milostná
avantýrami	avantýra	k1gFnPc7	avantýra
<g/>
.	.	kIx.	.
</s>
<s>
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
prožila	prožít	k5eAaPmAgFnS	prožít
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
sklonku	sklonek	k1gInSc6	sklonek
<g/>
,	,	kIx,	,
v	v	k7c6	v
ponižující	ponižující	k2eAgFnSc6d1	ponižující
chudobě	chudoba	k1gFnSc6	chudoba
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
v	v	k7c6	v
hladu	hlad	k1gInSc6	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
však	však	k9	však
nepodotknout	podotknout	k5eNaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
místo	místo	k7c2	místo
ředitelky	ředitelka	k1gFnSc2	ředitelka
vyšší	vysoký	k2eAgFnSc2d2	vyšší
české	český	k2eAgFnSc2d1	Česká
dívčí	dívčí	k2eAgFnSc2d1	dívčí
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Smutný	Smutný	k1gMnSc1	Smutný
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
její	její	k3xOp3gInSc1	její
povzdech	povzdech	k1gInSc1	povzdech
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Těžko	těžko	k6eAd1	těžko
povznésti	povznést	k5eAaPmF	povznést
ducha	duch	k1gMnSc4	duch
<g/>
,	,	kIx,	,
když	když	k8xS	když
starost	starost	k1gFnSc4	starost
o	o	k7c4	o
chléb	chléb	k1gInSc4	chléb
vezdejší	vezdejší	k2eAgMnPc1d1	vezdejší
jej	on	k3xPp3gMnSc4	on
tíží	tížit	k5eAaImIp3nP	tížit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Korespondence	korespondence	k1gFnSc1	korespondence
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
opakovaně	opakovaně	k6eAd1	opakovaně
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
vlasteneckých	vlastenecký	k2eAgInPc6d1	vlastenecký
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Neúčinnost	Neúčinnost	k1gFnSc1	Neúčinnost
této	tento	k3xDgFnSc2	tento
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Němcová	Němcová	k1gFnSc1	Němcová
nedovedla	dovést	k5eNaPmAgFnS	dovést
s	s	k7c7	s
penězi	peníze	k1gInPc7	peníze
hospodařit	hospodařit	k5eAaImF	hospodařit
<g/>
,	,	kIx,	,
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
s	s	k7c7	s
velkolepým	velkolepý	k2eAgInSc7d1	velkolepý
pohřbem	pohřeb	k1gInSc7	pohřeb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jí	on	k3xPp3gFnSc3	on
vlastenci	vlastenec	k1gMnPc1	vlastenec
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
a	a	k8xC	a
s	s	k7c7	s
posmrtnou	posmrtný	k2eAgFnSc7d1	posmrtná
slávou	sláva	k1gFnSc7	sláva
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
hrob	hrob	k1gInSc1	hrob
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
vedle	vedle	k7c2	vedle
hrobu	hrob	k1gInSc2	hrob
Václava	Václav	k1gMnSc2	Václav
Hanky	Hanka	k1gFnSc2	Hanka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Barbory	Barbora	k1gFnSc2	Barbora
byly	být	k5eAaImAgInP	být
ostatky	ostatek	k1gInPc1	ostatek
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
údajně	údajně	k6eAd1	údajně
přeneseny	přenesen	k2eAgInPc1d1	přenesen
na	na	k7c4	na
současné	současný	k2eAgNnSc4d1	současné
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
hrob	hrob	k1gInSc1	hrob
2	[number]	k4	2
<g/>
B-	B-	k1gFnPc2	B-
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
reliéfní	reliéfní	k2eAgFnSc1d1	reliéfní
plaketa	plaketa	k1gFnSc1	plaketa
od	od	k7c2	od
Tomáše	Tomáš	k1gMnSc2	Tomáš
Seidana	Seidan	k1gMnSc2	Seidan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
na	na	k7c4	na
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zrušeném	zrušený	k2eAgInSc6d1	zrušený
hřbitově	hřbitov	k1gInSc6	hřbitov
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Filipa	Filip	k1gMnSc2	Filip
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
tam	tam	k6eAd1	tam
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
74	[number]	k4	74
letech	let	k1gInPc6	let
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
záchvat	záchvat	k1gInSc4	záchvat
u	u	k7c2	u
syna	syn	k1gMnSc2	syn
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
bydleli	bydlet	k5eAaImAgMnP	bydlet
na	na	k7c6	na
Křižíkově	Křižíkův	k2eAgNnSc6d1	Křižíkovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Němec	Němec	k1gMnSc1	Němec
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
reálce	reálka	k1gFnSc6	reálka
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
i	i	k9	i
v	v	k7c6	v
tamější	tamější	k2eAgFnSc6d1	tamější
botanické	botanický	k2eAgFnSc6d1	botanická
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
životopisná	životopisný	k2eAgFnSc1d1	životopisná
literatura	literatura	k1gFnSc1	literatura
o	o	k7c6	o
Boženě	Božena	k1gFnSc6	Božena
Němcové	Němcová	k1gFnSc2	Němcová
nezpochybňovala	zpochybňovat	k5eNaImAgFnS	zpochybňovat
Terezii	Terezie	k1gFnSc4	Terezie
a	a	k8xC	a
Johanna	Johann	k1gMnSc4	Johann
Panklovy	Panklův	k2eAgFnSc2d1	Panklova
jako	jako	k8xS	jako
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
4	[number]	k4	4
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
1820	[number]	k4	1820
jako	jako	k8xS	jako
datum	datum	k1gNnSc1	datum
jejího	její	k3xOp3gNnSc2	její
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
nebyl	být	k5eNaImAgInS	být
zpochybňován	zpochybňován	k2eAgInSc1d1	zpochybňován
věk	věk	k1gInSc1	věk
matky	matka	k1gFnSc2	matka
14	[number]	k4	14
let	léto	k1gNnPc2	léto
v	v	k7c6	v
době	doba	k1gFnSc6	doba
narození	narození	k1gNnSc2	narození
její	její	k3xOp3gFnSc2	její
nejstarší	starý	k2eAgFnSc2d3	nejstarší
dcery	dcera	k1gFnSc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
vyšla	vyjít	k5eAaPmAgFnS	vyjít
publikace	publikace	k1gFnSc1	publikace
Pravda	pravda	k1gFnSc1	pravda
o	o	k7c6	o
matce	matka	k1gFnSc6	matka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
autoři	autor	k1gMnPc1	autor
prokázali	prokázat	k5eAaPmAgMnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
matce	matka	k1gFnSc3	matka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
již	již	k9	již
22	[number]	k4	22
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
badatelé	badatel	k1gMnPc1	badatel
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
narodila	narodit	k5eAaPmAgFnS	narodit
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
(	(	kIx(	(
<g/>
či	či	k8xC	či
alespoň	alespoň	k9	alespoň
<g/>
,	,	kIx,	,
že	že	k8xS	že
datum	datum	k1gNnSc1	datum
jejího	její	k3xOp3gNnSc2	její
narození	narození	k1gNnSc2	narození
je	být	k5eAaImIp3nS	být
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Domněnku	domněnka	k1gFnSc4	domněnka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
byla	být	k5eAaImAgFnS	být
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
poprvé	poprvé	k6eAd1	poprvé
Karel	Karel	k1gMnSc1	Karel
Pleskač	pleskač	k1gMnSc1	pleskač
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
Jarmil	Jarmila	k1gFnPc2	Jarmila
Krecar	Krecara	k1gFnPc2	Krecara
<g/>
.	.	kIx.	.
</s>
<s>
Hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
byla	být	k5eAaImAgFnS	být
nemanželskou	manželský	k2eNgFnSc7d1	nemanželská
dcerou	dcera	k1gFnSc7	dcera
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zaháňské	zaháňský	k2eAgFnPc4d1	Zaháňská
<g/>
,	,	kIx,	,
však	však	k9	však
odborníci	odborník	k1gMnPc1	odborník
vyloučili	vyloučit	k5eAaPmAgMnP	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Helena	Helena	k1gFnSc1	Helena
Sobková	Sobková	k1gFnSc1	Sobková
<g/>
,	,	kIx,	,
však	však	k9	však
zastávají	zastávat	k5eAaImIp3nP	zastávat
detailním	detailní	k2eAgNnSc7d1	detailní
studiem	studio	k1gNnSc7	studio
podložený	podložený	k2eAgInSc4d1	podložený
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
mladší	mladý	k2eAgFnSc2d2	mladší
sestry	sestra	k1gFnSc2	sestra
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
<g/>
,	,	kIx,	,
Dorothey	Dorothea	k1gFnPc4	Dorothea
von	von	k1gInSc4	von
Biron	Biron	k1gNnSc1	Biron
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
možného	možný	k2eAgNnSc2d1	možné
narození	narození	k1gNnSc2	narození
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1816	[number]	k4	1816
již	již	k6eAd1	již
hraběnky	hraběnka	k1gFnSc2	hraběnka
de	de	k?	de
Talleyrand-Périgord	Talleyrand-Périgord	k1gInSc1	Talleyrand-Périgord
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nového	nový	k2eAgInSc2d1	nový
zdroje	zdroj	k1gInSc2	zdroj
se	se	k3xPyFc4	se
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Karlem	Karel	k1gMnSc7	Karel
Janem	Jan	k1gMnSc7	Jan
Clam-Martinicem	Clam-Martinic	k1gMnSc7	Clam-Martinic
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
-	-	kIx~	-
<g/>
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
skutečně	skutečně	k6eAd1	skutečně
ve	v	k7c6	v
francouzských	francouzský	k2eAgFnPc6d1	francouzská
lázních	lázeň	k1gFnPc6	lázeň
Bourbon-l	Bourbona	k1gFnPc2	Bourbon-la
<g/>
'	'	kIx"	'
<g/>
Archambault	Archambaulta	k1gFnPc2	Archambaulta
narodila	narodit	k5eAaPmAgFnS	narodit
nemanželská	manželský	k2eNgFnSc1d1	nemanželská
dcera	dcera	k1gFnSc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
argumenty	argument	k1gInPc4	argument
proti	proti	k7c3	proti
teoriím	teorie	k1gFnPc3	teorie
o	o	k7c6	o
šlechtickém	šlechtický	k2eAgInSc6d1	šlechtický
původu	původ	k1gInSc6	původ
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šůla	Šůla	k1gMnSc1	Šůla
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
kategoricky	kategoricky	k6eAd1	kategoricky
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
narozené	narozený	k2eAgNnSc1d1	narozené
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
nemusela	muset	k5eNaImAgFnS	muset
být	být	k5eAaImF	být
pozdější	pozdní	k2eAgFnSc1d2	pozdější
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
hypotézy	hypotéza	k1gFnSc2	hypotéza
mohla	moct	k5eAaImAgFnS	moct
narodit	narodit	k5eAaPmF	narodit
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1818	[number]	k4	1818
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
strávila	strávit	k5eAaPmAgFnS	strávit
Němcová	Němcová	k1gFnSc1	Němcová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přerušila	přerušit	k5eAaPmAgFnS	přerušit
jen	jen	k9	jen
krátkým	krátký	k2eAgInSc7d1	krátký
pobytem	pobyt	k1gInSc7	pobyt
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
cestami	cesta	k1gFnPc7	cesta
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
oblasti	oblast	k1gFnPc4	oblast
Banské	banský	k2eAgFnSc2d1	Banská
Štiavnice	Štiavnica	k1gFnSc2	Štiavnica
<g/>
,	,	kIx,	,
Sliače	Sliač	k1gInSc2	Sliač
<g/>
,	,	kIx,	,
Brezna	Brezna	k1gFnSc1	Brezna
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc2d1	horní
Lehoty	Lehota	k1gFnSc2	Lehota
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Revúce	Revúka	k1gFnSc6	Revúka
<g/>
,	,	kIx,	,
Chýžně	Chýžna	k1gFnSc6	Chýžna
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jejích	její	k3xOp3gFnPc2	její
14	[number]	k4	14
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
bydliště	bydliště	k1gNnPc1	bydliště
byla	být	k5eAaImAgNnP	být
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgFnPc1d1	Pražská
adresy	adresa	k1gFnPc1	adresa
jsou	být	k5eAaImIp3nP	být
očíslovány	očíslován	k2eAgFnPc1d1	očíslována
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
události	událost	k1gFnPc1	událost
jsou	být	k5eAaImIp3nP	být
odraženy	odražen	k2eAgFnPc1d1	odražena
<g/>
:	:	kIx,	:
1820	[number]	k4	1820
-	-	kIx~	-
:	:	kIx,	:
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Alservorstadt	Alservorstadt	k1gInSc1	Alservorstadt
č.	č.	k?	č.
206	[number]	k4	206
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
ústí	ústit	k5eAaImIp3nS	ústit
Sensengasse	Sensengasse	k1gFnSc1	Sensengasse
do	do	k7c2	do
Spitalgasse	Spitalgasse	k1gFnSc2	Spitalgasse
<g/>
)	)	kIx)	)
-	-	kIx~	-
Barbora	Barbora	k1gFnSc1	Barbora
Panklová	Panklová	k1gFnSc1	Panklová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1820	[number]	k4	1820
panskému	panský	k2eAgMnSc3d1	panský
kočímu	kočí	k1gMnSc3	kočí
<g/>
,	,	kIx,	,
rakouskému	rakouský	k2eAgMnSc3d1	rakouský
Němci	Němec	k1gMnSc3	Němec
Johannu	Johann	k1gMnSc3	Johann
<g />
.	.	kIx.	.
</s>
<s>
Panklovi	Pankl	k1gMnSc3	Pankl
(	(	kIx(	(
<g/>
1794	[number]	k4	1794
<g/>
-	-	kIx~	-
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
a	a	k8xC	a
Terezii	Terezie	k1gFnSc4	Terezie
Novotné	Novotná	k1gFnSc2	Novotná
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
-	-	kIx~	-
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
panské	panský	k2eAgFnSc3d1	Panská
pradleně	pradlena	k1gFnSc3	pradlena
<g/>
.	.	kIx.	.
1820	[number]	k4	1820
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Alserstraße	Alserstraße	k1gFnSc1	Alserstraße
17	[number]	k4	17
(	(	kIx(	(
<g/>
kdysi	kdysi	k6eAd1	kdysi
Alser	Alser	k1gMnSc1	Alser
Grund	Grund	k1gMnSc1	Grund
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
(	(	kIx(	(
<g/>
Dreifaltigkeitskirche	Dreifaltigkeitskirche	k1gFnSc1	Dreifaltigkeitskirche
<g/>
)	)	kIx)	)
-	-	kIx~	-
křest	křest	k1gInSc1	křest
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
1820	[number]	k4	1820
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
Skalice	Skalice	k1gFnSc1	Skalice
-	-	kIx~	-
svatba	svatba	k1gFnSc1	svatba
rodičů	rodič	k1gMnPc2	rodič
Ratibořice	Ratibořice	k1gFnPc4	Ratibořice
-	-	kIx~	-
manželé	manžel	k1gMnPc1	manžel
Panklovi	Panklův	k2eAgMnPc1d1	Panklův
sloužili	sloužit	k5eAaImAgMnP	sloužit
u	u	k7c2	u
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
,	,	kIx,	,
tkadlena	tkadlena	k1gFnSc1	tkadlena
z	z	k7c2	z
Náchodska	Náchodsk	k1gInSc2	Náchodsk
<g/>
,	,	kIx,	,
pobývala	pobývat	k5eAaImAgFnS	pobývat
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1825-1829	[number]	k4	1825-1829
a	a	k8xC	a
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
s	s	k7c7	s
výchovou	výchova	k1gFnSc7	výchova
<g/>
.	.	kIx.	.
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
-	-	kIx~	-
1824	[number]	k4	1824
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
až	až	k9	až
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
-	-	kIx~	-
1830	[number]	k4	1830
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
Skalice	Skalice	k1gFnSc1	Skalice
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
-	-	kIx~	-
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1833	[number]	k4	1833
<g/>
:	:	kIx,	:
Chvalkovice	Chvalkovice	k1gFnSc1	Chvalkovice
zámek	zámek	k1gInSc1	zámek
-	-	kIx~	-
na	na	k7c4	na
vychování	vychování	k1gNnSc4	vychování
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
zámeckého	zámecký	k2eAgMnSc2d1	zámecký
úředníka	úředník	k1gMnSc2	úředník
Augustina	Augustin	k1gMnSc2	Augustin
Hocha	hoch	k1gMnSc2	hoch
<g/>
.	.	kIx.	.
</s>
<s>
Učila	učit	k5eAaImAgFnS	učit
se	se	k3xPyFc4	se
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
šít	šít	k5eAaImF	šít
a	a	k8xC	a
panské	panský	k2eAgInPc4d1	panský
mravy	mrav	k1gInPc4	mrav
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
zde	zde	k6eAd1	zde
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
svatba	svatba	k1gFnSc1	svatba
-	-	kIx~	-
1837	[number]	k4	1837
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
-	-	kIx~	-
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
dvakrát	dvakrát	k6eAd1	dvakrát
staršího	starý	k2eAgMnSc4d2	starší
člena	člen	k1gMnSc4	člen
finanční	finanční	k2eAgFnSc2d1	finanční
stráže	stráž	k1gFnSc2	stráž
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
komisaře	komisař	k1gMnSc2	komisař
<g/>
)	)	kIx)	)
Josefa	Josef	k1gMnSc2	Josef
Němce	Němec	k1gMnSc2	Němec
<g/>
,	,	kIx,	,
oslava	oslava	k1gFnSc1	oslava
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
U	u	k7c2	u
Českého	český	k2eAgInSc2d1	český
lva	lev	k1gInSc2	lev
<g/>
.	.	kIx.	.
1837	[number]	k4	1837
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
-	-	kIx~	-
1838	[number]	k4	1838
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
-	-	kIx~	-
ulice	ulice	k1gFnPc1	ulice
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
127	[number]	k4	127
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Ubytovali	ubytovat	k5eAaPmAgMnP	ubytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
podkroví	podkroví	k1gNnSc6	podkroví
domu	dům	k1gInSc2	dům
kupce	kupec	k1gMnSc4	kupec
Augustina	Augustin	k1gMnSc4	Augustin
Hůlka	Hůlka	k1gMnSc1	Hůlka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domku	domek	k1gInSc6	domek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
žila	žít	k5eAaImAgFnS	žít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgNnSc4d3	nejstarší
české	český	k2eAgNnSc4d1	české
literární	literární	k2eAgNnSc4d1	literární
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Smetanových	Smetanových	k2eAgInPc6d1	Smetanových
sadech	sad	k1gInPc6	sad
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
vztyčen	vztyčit	k5eAaPmNgInS	vztyčit
jako	jako	k8xS	jako
připomínka	připomínka	k1gFnSc1	připomínka
jejího	její	k3xOp3gInSc2	její
pobytu	pobyt	k1gInSc2	pobyt
pomník	pomník	k1gInSc1	pomník
<g/>
.	.	kIx.	.
1838-1839	[number]	k4	1838-1839
Josefov	Josefov	k1gInSc1	Josefov
-	-	kIx~	-
hostinec	hostinec	k1gInSc1	hostinec
Veselý	veselý	k2eAgInSc1d1	veselý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vlastní	vlastní	k2eAgInSc4d1	vlastní
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Josefově	Josefov	k1gInSc6	Josefov
se	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1838	[number]	k4	1838
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1839	[number]	k4	1839
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
-	-	kIx~	-
1840	[number]	k4	1840
<g/>
:	:	kIx,	:
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
nám.	nám.	k?	nám.
čp.	čp.	k?	čp.
27	[number]	k4	27
-	-	kIx~	-
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
stával	stávat	k5eAaImAgInS	stávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1839	[number]	k4	1839
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
zahradnický	zahradnický	k2eAgMnSc1d1	zahradnický
odborník	odborník	k1gMnSc1	odborník
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
150	[number]	k4	150
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
umělkyně	umělkyně	k1gFnSc2	umělkyně
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Faltejsek	Faltejsek	k1gMnSc1	Faltejsek
<g/>
)	)	kIx)	)
a	a	k8xC	a
textem	text	k1gInSc7	text
Zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1839	[number]	k4	1839
až	až	k9	až
1840	[number]	k4	1840
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
1820	[number]	k4	1820
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
bylo	být	k5eAaImAgNnS	být
klidné	klidný	k2eAgNnSc1d1	klidné
<g/>
.	.	kIx.	.
</s>
<s>
Chodila	chodit	k5eAaImAgFnS	chodit
na	na	k7c4	na
procházky	procházka	k1gFnPc4	procházka
do	do	k7c2	do
Nedošína	Nedošín	k1gInSc2	Nedošín
a	a	k8xC	a
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
Magdalenou	Magdalena	k1gFnSc7	Magdalena
Dobromilou	Dobromila	k1gFnSc7	Dobromila
Rettigovou	Rettigův	k2eAgFnSc7d1	Rettigův
<g/>
.	.	kIx.	.
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1842	[number]	k4	1842
<g/>
:	:	kIx,	:
Polná	Polná	k1gFnSc1	Polná
<g/>
,	,	kIx,	,
Husovo	Husův	k2eAgNnSc1d1	Husovo
nám.	nám.	k?	nám.
čp.	čp.	k?	čp.
47	[number]	k4	47
-	-	kIx~	-
přízemí	přízemí	k1gNnSc6	přízemí
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1841	[number]	k4	1841
narodila	narodit	k5eAaPmAgFnS	narodit
jediná	jediný	k2eAgFnSc1d1	jediná
dcera	dcera	k1gFnSc1	dcera
Theodora	Theodora	k1gFnSc1	Theodora
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
používala	používat	k5eAaImAgFnS	používat
jméno	jméno	k1gNnSc4	jméno
Dora	Dora	k1gFnSc1	Dora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domě	dům	k1gInSc6	dům
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1934	[number]	k4	1934
odhalena	odhalen	k2eAgFnSc1d1	odhalena
bronzová	bronzový	k2eAgFnSc1d1	bronzová
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
prof.	prof.	kA	prof.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rérych	Rérych	k1gMnSc1	Rérych
<g/>
)	)	kIx)	)
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
bydlela	bydlet	k5eAaImAgFnS	bydlet
v	v	k7c6	v
květu	květ	k1gInSc6	květ
mladosti	mladost	k1gFnSc2	mladost
své	své	k1gNnSc1	své
1840	[number]	k4	1840
1842	[number]	k4	1842
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
prostá	prostý	k2eAgFnSc1d1	prostá
knížka	knížka	k1gFnSc1	knížka
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	tyla	k1gFnSc1	tyla
a	a	k8xC	a
styk	styk	k1gInSc4	styk
s	s	k7c7	s
polenskými	polenský	k2eAgMnPc7d1	polenský
vlastenci	vlastenec	k1gMnPc7	vlastenec
získaly	získat	k5eAaPmAgFnP	získat
ji	on	k3xPp3gFnSc4	on
české	český	k2eAgFnSc3d1	Česká
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
aktu	akt	k1gInSc2	akt
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
vnučka	vnučka	k1gFnSc1	vnučka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcové	k2eAgFnSc1d1	Němcové
<g/>
.	.	kIx.	.
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
1841	[number]	k4	1841
<g/>
:	:	kIx,	:
Vídeň	Vídeň	k1gFnSc4	Vídeň
-	-	kIx~	-
úmrtí	úmrtí	k1gNnSc2	úmrtí
babičky	babička	k1gFnSc2	babička
Magdaleny	Magdalena	k1gFnSc2	Magdalena
Novotné	Novotná	k1gFnSc2	Novotná
1	[number]	k4	1
<g/>
/	/	kIx~	/
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1845	[number]	k4	1845
<g/>
:	:	kIx,	:
Na	na	k7c4	na
Poříčí	Poříčí	k1gNnSc4	Poříčí
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Zeleného	Zeleného	k2eAgInSc2d1	Zeleného
stromu	strom	k1gInSc2	strom
čp.	čp.	k?	čp.
1048	[number]	k4	1048
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
(	(	kIx(	(
<g/>
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
domu	dům	k1gInSc2	dům
č.	č.	k?	č.
1050	[number]	k4	1050
<g/>
)	)	kIx)	)
-	-	kIx~	-
malý	malý	k2eAgInSc1d1	malý
byt	byt	k1gInSc1	byt
na	na	k7c6	na
pavlači	pavlač	k1gFnSc6	pavlač
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
poslední	poslední	k2eAgMnSc1d1	poslední
syn	syn	k1gMnSc1	syn
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1845	[number]	k4	1845
<g/>
-	-	kIx~	-
<g/>
1847	[number]	k4	1847
<g/>
:	:	kIx,	:
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
Míru	mír	k1gInSc2	mír
120	[number]	k4	120
-	-	kIx~	-
rohový	rohový	k2eAgInSc4d1	rohový
dům	dům	k1gInSc4	dům
proti	proti	k7c3	proti
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
žulová	žulový	k2eAgFnSc1d1	Žulová
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
s	s	k7c7	s
bronzovým	bronzový	k2eAgInSc7d1	bronzový
reliéfem	reliéf	k1gInSc7	reliéf
(	(	kIx(	(
<g/>
Čeněk	Čeněk	k1gMnSc1	Čeněk
Vosmík	Vosmík	k1gMnSc1	Vosmík
<g/>
)	)	kIx)	)
a	a	k8xC	a
textem	text	k1gInSc7	text
Zde	zde	k6eAd1	zde
žila	žít	k5eAaImAgFnS	žít
a	a	k8xC	a
působila	působit	k5eAaImAgFnS	působit
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
o	o	k7c4	o
Domažlicko	Domažlicko	k1gNnSc4	Domažlicko
nad	nad	k7c4	nad
jiné	jiný	k2eAgFnPc4d1	jiná
zasloužilá	zasloužilý	k2eAgNnPc4d1	zasloužilé
<g/>
.	.	kIx.	.
</s>
<s>
Sestry	sestra	k1gFnPc1	sestra
sestře	sestra	k1gFnSc6	sestra
<g/>
.	.	kIx.	.
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Domažlicích	Domažlice	k1gFnPc6	Domažlice
psala	psát	k5eAaImAgFnS	psát
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
.	.	kIx.	.
1846	[number]	k4	1846
(	(	kIx(	(
<g/>
17.7	[number]	k4	17.7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
-	-	kIx~	-
1847	[number]	k4	1847
<g/>
:	:	kIx,	:
Františkovy	Františkův	k2eAgFnPc1d1	Františkova
Lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1846	[number]	k4	1846
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
léčila	léčit	k5eAaImAgFnS	léčit
4	[number]	k4	4
týdny	týden	k1gInPc7	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcové	k2eAgFnSc2d1	Němcové
v	v	k7c6	v
parčíku	parčík	k1gInSc6	parčík
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
kameni	kámen	k1gInSc6	kámen
kovová	kovový	k2eAgFnSc1d1	kovová
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
V	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
bydlela	bydlet	k5eAaImAgFnS	bydlet
jako	jako	k9	jako
vzácný	vzácný	k2eAgMnSc1d1	vzácný
host	host	k1gMnSc1	host
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
v	v	k7c6	v
létech	léto	k1gNnPc6	léto
1846	[number]	k4	1846
<g/>
-	-	kIx~	-
<g/>
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
psala	psát	k5eAaImAgFnS	psát
své	svůj	k3xOyFgInPc4	svůj
Dopisy	dopis	k1gInPc4	dopis
z	z	k7c2	z
Lázní	lázeň	k1gFnPc2	lázeň
Františkových	Františkův	k2eAgFnPc2d1	Františkova
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
nazváno	nazvat	k5eAaPmNgNnS	nazvat
Divadlo	divadlo	k1gNnSc1	divadlo
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Ruská	ruský	k2eAgFnSc1d1	ruská
102	[number]	k4	102
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1847	[number]	k4	1847
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
<g/>
:	:	kIx,	:
Všeruby	Všeruby	k1gInPc1	Všeruby
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Neumark	Neumark	k1gInSc1	Neumark
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
dopsala	dopsat	k5eAaPmAgFnS	dopsat
Karlu	Karla	k1gFnSc4	Karla
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
psala	psát	k5eAaImAgFnS	psát
v	v	k7c6	v
Domažlicích	Domažlice	k1gFnPc6	Domažlice
<g/>
.	.	kIx.	.
1848	[number]	k4	1848
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc4	červenec
<g/>
)	)	kIx)	)
-	-	kIx~	-
1850	[number]	k4	1850
(	(	kIx(	(
<g/>
únor	únor	k1gInSc4	únor
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
předměstský	předměstský	k2eAgInSc1d1	předměstský
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
243	[number]	k4	243
(	(	kIx(	(
<g/>
byt	byt	k1gInSc4	byt
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
)	)	kIx)	)
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Boleslavské	boleslavský	k2eAgFnSc2d1	Boleslavská
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
Velkých	velký	k2eAgInPc2d1	velký
Valů	val	k1gInPc2	val
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
zde	zde	k6eAd1	zde
spokojená	spokojený	k2eAgFnSc1d1	spokojená
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
domem	dům	k1gInSc7	dům
její	její	k3xOp3gMnPc4	její
pomník	pomník	k1gInSc4	pomník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nechal	nechat	k5eAaPmAgInS	nechat
zbudovat	zbudovat	k5eAaPmF	zbudovat
ženský	ženský	k2eAgInSc1d1	ženský
čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
spolek	spolek	k1gInSc1	spolek
Lada	Lada	k1gFnSc1	Lada
<g/>
.	.	kIx.	.
1850	[number]	k4	1850
<g/>
:	:	kIx,	:
Liberec	Liberec	k1gInSc1	Liberec
-	-	kIx~	-
2	[number]	k4	2
bydliště	bydliště	k1gNnSc2	bydliště
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
/	/	kIx~	/
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
,	,	kIx,	,
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
je	být	k5eAaImIp3nS	být
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
b	b	k?	b
<g/>
/	/	kIx~	/
Kristiánov	Kristiánov	k1gInSc1	Kristiánov
<g/>
,	,	kIx,	,
asi	asi	k9	asi
ulice	ulice	k1gFnSc2	ulice
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
nebo	nebo	k8xC	nebo
náměstí	náměstí	k1gNnSc2	náměstí
Českých	český	k2eAgMnPc2d1	český
bratří	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
tehdy	tehdy	k6eAd1	tehdy
nebyly	být	k5eNaImAgFnP	být
české	český	k2eAgFnPc1d1	Česká
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
docházel	docházet	k5eAaImAgInS	docházet
učit	učit	k5eAaImF	učit
český	český	k2eAgMnSc1d1	český
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
připomíná	připomínat	k5eAaImIp3nS	připomínat
pomník	pomník	k1gInSc1	pomník
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
František	František	k1gMnSc1	František
Stupecký	Stupecký	k2eAgMnSc1d1	Stupecký
<g/>
)	)	kIx)	)
v	v	k7c6	v
parku	park	k1gInSc6	park
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Českých	český	k2eAgMnPc2d1	český
bratří	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
bronzový	bronzový	k2eAgInSc4d1	bronzový
reliéf	reliéf	k1gInSc4	reliéf
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
podobiznou	podobizna	k1gFnSc7	podobizna
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
nápis	nápis	k1gInSc1	nápis
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
1850	[number]	k4	1850
<g/>
:	:	kIx,	:
Zaháň	Zaháň	k1gFnSc4	Zaháň
-	-	kIx~	-
úmrtí	úmrtí	k1gNnSc4	úmrtí
otce	otec	k1gMnSc2	otec
2	[number]	k4	2
<g/>
/	/	kIx~	/
1850	[number]	k4	1850
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Štěpánská	štěpánský	k2eAgFnSc1d1	Štěpánská
-	-	kIx~	-
u	u	k7c2	u
učitele	učitel	k1gMnSc2	učitel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Josefa	Josef	k1gMnSc2	Josef
Františka	František	k1gMnSc2	František
Šumavského	šumavský	k2eAgInSc2d1	šumavský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
-	-	kIx~	-
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
/	/	kIx~	/
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1853	[number]	k4	1853
<g/>
:	:	kIx,	:
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
838	[number]	k4	838
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
15	[number]	k4	15
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
/	/	kIx~	/
1853	[number]	k4	1853
<g/>
:	:	kIx,	:
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
čp.	čp.	k?	čp.
791	[number]	k4	791
<g/>
/	/	kIx~	/
<g/>
32	[number]	k4	32
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
1853	[number]	k4	1853
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c4	v
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
Olšanské	olšanský	k2eAgInPc4d1	olšanský
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
:	:	kIx,	:
pohřeb	pohřeb	k1gInSc4	pohřeb
předčasně	předčasně	k6eAd1	předčasně
zesnulého	zesnulý	k2eAgInSc2d1	zesnulý
15	[number]	k4	15
<g/>
letého	letý	k2eAgInSc2d1	letý
syna	syn	k1gMnSc4	syn
Hynka	Hynek	k1gMnSc4	Hynek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
se	se	k3xPyFc4	se
složili	složit	k5eAaPmAgMnP	složit
spolužáci	spolužák	k1gMnPc1	spolužák
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
/	/	kIx~	/
1853-1854	[number]	k4	1853-1854
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ječná	ječný	k2eAgFnSc1d1	Ječná
čp.	čp.	k?	čp.
516	[number]	k4	516
<g/>
/	/	kIx~	/
<g/>
28	[number]	k4	28
-	-	kIx~	-
zde	zde	k6eAd1	zde
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
Babičku	babička	k1gFnSc4	babička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
bronzová	bronzový	k2eAgFnSc1d1	bronzová
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
Eva	Eva	k1gFnSc1	Eva
Springerová	Springerová	k1gFnSc1	Springerová
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
Jan	Jan	k1gMnSc1	Jan
Zázvorka	zázvorka	k1gMnSc1	zázvorka
<g/>
)	)	kIx)	)
s	s	k7c7	s
reliéfní	reliéfní	k2eAgFnSc7d1	reliéfní
podobiznou	podobizna	k1gFnSc7	podobizna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zleva	zleva	k6eAd1	zleva
lemuje	lemovat	k5eAaImIp3nS	lemovat
vavřínová	vavřínový	k2eAgFnSc1d1	vavřínová
a	a	k8xC	a
zprava	zprava	k6eAd1	zprava
lipová	lipový	k2eAgFnSc1d1	Lipová
ratolest	ratolest	k1gFnSc1	ratolest
<g/>
,	,	kIx,	,
a	a	k8xC	a
zlaceným	zlacený	k2eAgInSc7d1	zlacený
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
napsala	napsat	k5eAaPmAgFnS	napsat
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
Babičku	babička	k1gFnSc4	babička
Desku	deska	k1gFnSc4	deska
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
drobná	drobný	k2eAgFnSc1d1	drobná
černá	černý	k2eAgFnSc1d1	černá
tabulka	tabulka	k1gFnSc1	tabulka
s	s	k7c7	s
textem	text	k1gInSc7	text
Dům	dům	k1gInSc1	dům
zrození	zrození	k1gNnSc6	zrození
Babičky	babička	k1gFnSc2	babička
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
/	/	kIx~	/
1854	[number]	k4	1854
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1855	[number]	k4	1855
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vyšehradská	vyšehradský	k2eAgFnSc1d1	Vyšehradská
čp.	čp.	k?	čp.
1378	[number]	k4	1378
<g/>
/	/	kIx~	/
<g/>
45	[number]	k4	45
-	-	kIx~	-
zde	zde	k6eAd1	zde
dokončila	dokončit	k5eAaPmAgFnS	dokončit
Babičku	babička	k1gFnSc4	babička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
z	z	k7c2	z
černého	černý	k2eAgInSc2d1	černý
mramoru	mramor	k1gInSc2	mramor
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
žila	žít	k5eAaImAgFnS	žít
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1855	[number]	k4	1855
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Dokončila	dokončit	k5eAaPmAgFnS	dokončit
zde	zde	k6eAd1	zde
dílo	dílo	k1gNnSc4	dílo
>	>	kIx)	>
<g/>
Babička	babička	k1gFnSc1	babička
<g/>
<	<	kIx(	<
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
pamětní	pamětní	k2eAgFnSc2d1	pamětní
desky	deska	k1gFnSc2	deska
má	mít	k5eAaImIp3nS	mít
připomínat	připomínat	k5eAaImF	připomínat
stylizovanou	stylizovaný	k2eAgFnSc4d1	stylizovaná
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
Babička	babička	k1gFnSc1	babička
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
u	u	k7c2	u
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pospíšila	Pospíšil	k1gMnSc2	Pospíšil
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
/	/	kIx~	/
1855	[number]	k4	1855
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
<g/>
)	)	kIx)	)
-	-	kIx~	-
1857	[number]	k4	1857
(	(	kIx(	(
<g/>
podzim	podzim	k1gInSc4	podzim
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Štěpánská	štěpánský	k2eAgFnSc1d1	Štěpánská
čp.	čp.	k?	čp.
647	[number]	k4	647
<g/>
/	/	kIx~	/
<g/>
31	[number]	k4	31
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
45	[number]	k4	45
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
/	/	kIx~	/
1857	[number]	k4	1857
(	(	kIx(	(
<g/>
podzim	podzim	k1gInSc1	podzim
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
1860	[number]	k4	1860
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Řeznická	řeznická	k1gFnSc1	řeznická
čp.	čp.	k?	čp.
1360	[number]	k4	1360
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
13	[number]	k4	13
<g/>
)	)	kIx)	)
-	-	kIx~	-
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
poschodí	poschodí	k1gNnSc6	poschodí
9	[number]	k4	9
<g/>
/	/	kIx~	/
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Vodičkova	Vodičkův	k2eAgFnSc1d1	Vodičkova
10	[number]	k4	10
<g/>
/	/	kIx~	/
Štěpánská	štěpánský	k2eAgFnSc1d1	Štěpánská
čp.	čp.	k?	čp.
544	[number]	k4	544
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
1861	[number]	k4	1861
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
-	-	kIx~	-
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
nám.	nám.	k?	nám.
dům	dům	k1gInSc4	dům
U	u	k7c2	u
Modré	modrý	k2eAgFnSc2d1	modrá
hvězdy	hvězda	k1gFnSc2	hvězda
č.	č.	k?	č.
84	[number]	k4	84
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
ubytovala	ubytovat	k5eAaPmAgFnS	ubytovat
v	v	k7c6	v
Pifflově	Pifflův	k2eAgInSc6d1	Pifflův
zájezdním	zájezdní	k2eAgInSc6d1	zájezdní
hostinci	hostinec	k1gInSc6	hostinec
(	(	kIx(	(
<g/>
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
nám.	nám.	k?	nám.
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Antonín	Antonín	k1gMnSc1	Antonín
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
knihtiskárny	knihtiskárna	k1gFnSc2	knihtiskárna
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vydával	vydávat	k5eAaImAgMnS	vydávat
její	její	k3xOp3gFnSc2	její
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc7	on
zajistil	zajistit	k5eAaPmAgMnS	zajistit
ubytování	ubytování	k1gNnSc4	ubytování
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
U	u	k7c2	u
Modré	modrý	k2eAgFnSc2d1	modrá
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgNnSc4	svůj
ubytování	ubytování	k1gNnSc4	ubytování
popsala	popsat	k5eAaPmAgFnS	popsat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Mám	mít	k5eAaImIp1nS	mít
hezký	hezký	k2eAgInSc4d1	hezký
pokojíček	pokojíček	k1gInSc4	pokojíček
<g/>
,	,	kIx,	,
divan	divan	k1gInSc4	divan
<g/>
,	,	kIx,	,
stolek	stolek	k1gInSc4	stolek
<g/>
,	,	kIx,	,
věšák	věšák	k1gInSc4	věšák
na	na	k7c4	na
šaty	šat	k1gInPc4	šat
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
a	a	k8xC	a
také	také	k9	také
postel	postel	k1gFnSc1	postel
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
sem	sem	k6eAd1	sem
přijela	přijet	k5eAaPmAgFnS	přijet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
své	svůj	k3xOyFgInPc4	svůj
texty	text	k1gInPc4	text
sama	sám	k3xTgMnSc4	sám
redigovat	redigovat	k5eAaImF	redigovat
a	a	k8xC	a
přivydělat	přivydělat	k5eAaPmF	přivydělat
si	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Ubytování	ubytování	k1gNnSc4	ubytování
i	i	k8xC	i
stravu	strava	k1gFnSc4	strava
platil	platit	k5eAaImAgMnS	platit
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
nemoci	nemoc	k1gFnSc3	nemoc
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1861	[number]	k4	1861
jí	jíst	k5eAaImIp3nS	jíst
Augusta	Augusta	k1gMnSc1	Augusta
vzkázal	vzkázat	k5eAaPmAgMnS	vzkázat
<g/>
:	:	kIx,	:
Déle	dlouho	k6eAd2	dlouho
nechci	chtít	k5eNaImIp1nS	chtít
<g/>
,	,	kIx,	,
nebudu	být	k5eNaImBp1nS	být
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
ze	z	k7c2	z
mňe	mňe	k?	mňe
dle	dle	k7c2	dle
libosti	libost	k1gFnSc2	libost
blazna	blazna	k1gFnSc1	blazna
dělate	dělat	k1gInSc5	dělat
<g/>
;	;	kIx,	;
odevzdejte	odevzdat	k5eAaPmRp2nP	odevzdat
tištěnou	tištěný	k2eAgFnSc4d1	tištěná
babičku	babička	k1gFnSc4	babička
sazeči	sazeč	k1gMnPc1	sazeč
<g/>
,	,	kIx,	,
ja	ja	k?	ja
obstaram	obstaram	k1gInSc1	obstaram
spojení	spojení	k1gNnSc2	spojení
sam	sam	k?	sam
<g/>
,	,	kIx,	,
a	a	k8xC	a
Vy	vy	k3xPp2nPc1	vy
hleďte	hledět	k5eAaImRp2nP	hledět
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
ste	ste	k?	ste
Litomišli	Litomišle	k1gFnSc6	Litomišle
brzo	brzo	k6eAd1	brzo
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
stupňovalo	stupňovat	k5eAaImAgNnS	stupňovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vzkazu	vzkaz	k1gInSc6	vzkaz
jí	jíst	k5eAaImIp3nS	jíst
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
Nedate	Nedat	k1gInSc5	Nedat
<g/>
-li	i	k?	-li
hned	hned	k6eAd1	hned
rukopis	rukopis	k1gInSc4	rukopis
a	a	k8xC	a
dokončeni	dokončen	k2eAgMnPc1d1	dokončen
<g/>
,	,	kIx,	,
tak	tak	k9	tak
se	se	k3xPyFc4	se
postarejte	postarat	k5eAaPmRp2nP	postarat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
živa	živ	k2eAgFnSc1d1	živa
budete	být	k5eAaImBp2nP	být
<g/>
,	,	kIx,	,
ja	ja	k?	ja
zapověděl	zapovědět	k5eAaPmAgInS	zapovědět
<g/>
,	,	kIx,	,
Vám	vy	k3xPp2nPc3	vy
již	již	k6eAd1	již
co	co	k8xS	co
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Manžel	manžel	k1gMnSc1	manžel
Josef	Josef	k1gMnSc1	Josef
ji	on	k3xPp3gFnSc4	on
nakonec	nakonec	k6eAd1	nakonec
těžce	těžce	k6eAd1	těžce
nemocnou	mocný	k2eNgFnSc4d1	mocný
a	a	k8xC	a
vyčerpanou	vyčerpaný	k2eAgFnSc4d1	vyčerpaná
odvezl	odvézt	k5eAaPmAgMnS	odvézt
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
domě	dům	k1gInSc6	dům
hotel	hotel	k1gInSc1	hotel
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
vlevo	vlevo	k6eAd1	vlevo
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
textem	text	k1gInSc7	text
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
naposled	naposled	k6eAd1	naposled
pracovala	pracovat	k5eAaImAgFnS	pracovat
a	a	k8xC	a
dohasínala	dohasínat	k5eAaImAgFnS	dohasínat
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
13	[number]	k4	13
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
výstavy	výstava	k1gFnPc4	výstava
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
Boženě	Božena	k1gFnSc3	Božena
Němcové	Němcová	k1gFnSc2	Němcová
na	na	k7c6	na
litomyšlském	litomyšlský	k2eAgInSc6d1	litomyšlský
zámku	zámek	k1gInSc6	zámek
<g />
.	.	kIx.	.
</s>
<s>
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
Na	na	k7c6	na
Příkopě	příkop	k1gInSc6	příkop
dům	dům	k1gInSc4	dům
U	u	k7c2	u
Tří	tři	k4xCgFnPc2	tři
lip	lípa	k1gFnPc2	lípa
čp.	čp.	k?	čp.
854	[number]	k4	854
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
16	[number]	k4	16
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
v	v	k7c6	v
manželově	manželův	k2eAgInSc6d1	manželův
bytě	byt	k1gInSc6	byt
si	se	k3xPyFc3	se
den	den	k1gInSc4	den
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
prolistovala	prolistovat	k5eAaPmAgFnS	prolistovat
první	první	k4xOgInPc4	první
sešity	sešit	k1gInPc4	sešit
druhého	druhý	k4xOgNnSc2	druhý
vydání	vydání	k1gNnSc2	vydání
Babičky	babička	k1gFnSc2	babička
a	a	k8xC	a
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1862	[number]	k4	1862
v	v	k7c4	v
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
vydechla	vydechnout	k5eAaPmAgFnS	vydechnout
naposled	naposled	k6eAd1	naposled
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jí	on	k3xPp3gFnSc3	on
41	[number]	k4	41
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
travertinovém	travertinový	k2eAgInSc6d1	travertinový
výklenku	výklenek	k1gInSc6	výklenek
bronzová	bronzový	k2eAgFnSc1d1	bronzová
busta	busta	k1gFnSc1	busta
a	a	k8xC	a
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Neužil	Neužil	k1gMnSc1	Neužil
<g/>
)	)	kIx)	)
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
Zde	zde	k6eAd1	zde
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Tří	tři	k4xCgFnPc2	tři
líp	dobře	k6eAd2	dobře
dokonala	dokonat	k5eAaPmAgFnS	dokonat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
tvůrkyně	tvůrkyně	k1gFnSc1	tvůrkyně
Babičky	babička	k1gFnSc2	babička
4.2	[number]	k4	4.2
<g/>
.1820	.1820	k4	.1820
<g/>
-	-	kIx~	-
<g/>
21.1	[number]	k4	21.1
<g/>
.1862	.1862	k4	.1862
<g/>
.	.	kIx.	.
</s>
<s>
Zřízeno	zřídit	k5eAaPmNgNnS	zřídit
péčí	péče	k1gFnSc7	péče
Ústředního	ústřední	k2eAgInSc2d1	ústřední
spolku	spolek	k1gInSc2	spolek
českých	český	k2eAgFnPc2d1	Česká
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
okupace	okupace	k1gFnPc4	okupace
poškozeno	poškozen	k2eAgNnSc4d1	poškozeno
a	a	k8xC	a
Ústř	Ústř	k1gInSc4	Ústř
<g/>
.	.	kIx.	.
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
čes.	čes.	k?	čes.
žen	žena	k1gFnPc2	žena
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
pohřeb	pohřeb	k1gInSc1	pohřeb
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1862	[number]	k4	1862
<g/>
:	:	kIx,	:
Vyšehradský	vyšehradský	k2eAgInSc1d1	vyšehradský
hřbitov	hřbitov	k1gInSc1	hřbitov
-	-	kIx~	-
hrob	hrob	k1gInSc1	hrob
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
B-	B-	k1gFnSc1	B-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
při	při	k7c6	při
východní	východní	k2eAgFnSc6d1	východní
zdi	zeď	k1gFnSc6	zeď
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
i	i	k9	i
její	její	k3xOp3gFnSc3	její
syn	syn	k1gMnSc1	syn
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Dora	Dora	k1gFnSc1	Dora
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náhrobek	náhrobek	k1gInSc1	náhrobek
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nehvizdského	hvizdský	k2eNgInSc2d1	hvizdský
pískovce	pískovec	k1gInSc2	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Reliéfní	reliéfní	k2eAgFnSc1d1	reliéfní
plaketa	plaketa	k1gFnSc1	plaketa
od	od	k7c2	od
Tomáše	Tomáš	k1gMnSc2	Tomáš
Seidana	Seidan	k1gMnSc2	Seidan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
reliéf	reliéf	k1gInSc1	reliéf
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
babičku	babička	k1gFnSc4	babička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
vnoučatům	vnouče	k1gNnPc3	vnouče
tolar	tolar	k1gInSc1	tolar
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
i	i	k9	i
Marie	Marie	k1gFnSc1	Marie
Riegrová-Palacká	Riegrová-Palacký	k2eAgFnSc1d1	Riegrová-Palacká
a	a	k8xC	a
Karolína	Karolína	k1gFnSc1	Karolína
Světlá	světlat	k5eAaImIp3nS	světlat
<g/>
.	.	kIx.	.
bronzový	bronzový	k2eAgInSc4d1	bronzový
pomník	pomník	k1gInSc4	pomník
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Slovanského	slovanský	k2eAgInSc2d1	slovanský
ostrova	ostrov	k1gInSc2	ostrov
-	-	kIx~	-
dílo	dílo	k1gNnSc1	dílo
sochaře	sochař	k1gMnSc2	sochař
Karla	Karel	k1gMnSc2	Karel
Pokorného	Pokorný	k1gMnSc2	Pokorný
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
se	se	k3xPyFc4	se
na	na	k7c6	na
Žofíně	Žofín	k1gInSc6	Žofín
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgInSc2	první
velkého	velký	k2eAgInSc2d1	velký
plesu	ples	k1gInSc2	ples
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
díla	dílo	k1gNnSc2	dílo
byly	být	k5eAaImAgFnP	být
básně	báseň	k1gFnPc1	báseň
<g/>
:	:	kIx,	:
Slavné	slavný	k2eAgFnPc1d1	slavná
ráno	ráno	k6eAd1	ráno
Ženám	žena	k1gFnPc3	žena
českým	český	k2eAgFnPc3d1	Česká
Moje	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
Barunka	Barunka	k1gFnSc1	Barunka
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Koleda	koleda	k1gFnSc1	koleda
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
pouti	pouť	k1gFnSc2	pouť
Čtyry	Čtyra	k1gFnSc2	Čtyra
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
vydáváno	vydávat	k5eAaPmNgNnS	vydávat
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Čtyři	čtyři	k4xCgMnPc1	čtyři
doby	doba	k1gFnSc2	doba
Devět	devět	k4xCc4	devět
křížů	kříž	k1gInPc2	kříž
Divá	divý	k2eAgFnSc1d1	divá
Bára	Bára	k1gFnSc1	Bára
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
Česká	český	k2eAgFnSc1d1	Česká
pokladnice	pokladnice	k1gFnSc1	pokladnice
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
noc	noc	k1gFnSc4	noc
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Posel	posít	k5eAaPmAgInS	posít
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
Domácí	domácí	k2eAgFnSc1d1	domácí
nemoc	nemoc	k1gFnSc1	nemoc
Dopisy	dopis	k1gInPc1	dopis
z	z	k7c2	z
lázní	lázeň	k1gFnPc2	lázeň
Františkových	Františkův	k2eAgFnPc2d1	Františkova
Hospodyně	hospodyně	k1gFnPc4	hospodyně
na	na	k7c6	na
slovíčko	slovíčko	k1gNnSc4	slovíčko
Chudí	chudit	k5eAaImIp3nP	chudit
lidé	člověk	k1gMnPc1	člověk
-	-	kIx~	-
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
různé	různý	k2eAgFnPc4d1	různá
společenské	společenský	k2eAgFnPc4d1	společenská
<g />
.	.	kIx.	.
</s>
<s>
vrstvy	vrstva	k1gFnPc1	vrstva
Chyže	Chyže	k?	Chyže
pod	pod	k7c7	pod
horami	hora	k1gFnPc7	hora
Karla	Karel	k1gMnSc2	Karel
-	-	kIx~	-
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
almanachu	almanach	k1gInSc6	almanach
Perly	perla	k1gFnSc2	perla
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
chodské	chodský	k2eAgFnSc6d1	Chodská
vesnici	vesnice	k1gFnSc6	vesnice
Stráž	stráž	k1gFnSc1	stráž
Obrázek	obrázek	k1gInSc4	obrázek
vesnický	vesnický	k2eAgMnSc1d1	vesnický
Pan	Pan	k1gMnSc1	Pan
učitel	učitel	k1gMnSc1	učitel
-	-	kIx~	-
poslední	poslední	k2eAgFnSc1d1	poslední
dokončená	dokončený	k2eAgFnSc1d1	dokončená
povídka	povídka	k1gFnSc1	povídka
Pomněnka	pomněnka	k1gFnSc1	pomněnka
šlechetné	šlechetný	k2eAgFnSc2d1	šlechetná
duše	duše	k1gFnSc2	duše
Rozárka	Rozárka	k1gFnSc1	Rozárka
Selská	selský	k2eAgFnSc1d1	selská
politika	politika	k1gFnSc1	politika
Sestry	sestra	k1gFnSc2	sestra
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
almanachu	almanach	k1gInSc6	almanach
Lada	Lada	k1gFnSc1	Lada
Nióla	Niólo	k1gNnSc2	Niólo
Silný	silný	k2eAgMnSc1d1	silný
Ctibor	Ctibor	k1gMnSc1	Ctibor
Čertík	čertík	k1gMnSc1	čertík
Němcová	Němcová	k1gFnSc1	Němcová
byla	být	k5eAaImAgFnS	být
schopnou	schopný	k2eAgFnSc7d1	schopná
vypravěčkou	vypravěčka	k1gFnSc7	vypravěčka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xC	tak
ji	on	k3xPp3gFnSc4	on
přátelé	přítel	k1gMnPc1	přítel
vybízeli	vybízet	k5eAaImAgMnP	vybízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
své	svůj	k3xOyFgFnPc4	svůj
pohádky	pohádka	k1gFnPc4	pohádka
sepsala	sepsat	k5eAaPmAgFnS	sepsat
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Nabádali	nabádat	k5eAaBmAgMnP	nabádat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokumentárně	dokumentárně	k6eAd1	dokumentárně
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
pohádkového	pohádkový	k2eAgNnSc2d1	pohádkové
bohatství	bohatství	k1gNnSc2	bohatství
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Znala	znát	k5eAaImAgFnS	znát
mnoho	mnoho	k4c4	mnoho
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
si	se	k3xPyFc3	se
jich	on	k3xPp3gInPc2	on
zapamatovala	zapamatovat	k5eAaPmAgFnS	zapamatovat
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Četla	číst	k5eAaImAgFnS	číst
také	také	k9	také
německé	německý	k2eAgFnPc4d1	německá
sbírky	sbírka	k1gFnPc4	sbírka
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
legend	legenda	k1gFnPc2	legenda
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
pohádky	pohádka	k1gFnPc1	pohádka
byly	být	k5eAaImAgFnP	být
její	její	k3xOp3gFnPc1	její
první	první	k4xOgFnPc1	první
samostatně	samostatně	k6eAd1	samostatně
vydané	vydaný	k2eAgNnSc4d1	vydané
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Získávala	získávat	k5eAaImAgFnS	získávat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
svoje	svůj	k3xOyFgFnPc4	svůj
tvůrčí	tvůrčí	k2eAgFnPc4d1	tvůrčí
a	a	k8xC	a
vypravěčské	vypravěčský	k2eAgFnPc4d1	vypravěčská
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vydávání	vydávání	k1gNnSc6	vydávání
pohádek	pohádka	k1gFnPc2	pohádka
se	se	k3xPyFc4	se
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
s	s	k7c7	s
nakladatelem	nakladatel	k1gMnSc7	nakladatel
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Pospíšilem	Pospíšil	k1gMnSc7	Pospíšil
<g/>
,	,	kIx,	,
vydávala	vydávat	k5eAaPmAgFnS	vydávat
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
sešitech	sešit	k1gInPc6	sešit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
sešit	sešit	k1gInSc1	sešit
vyšel	vyjít	k5eAaPmAgInS	vyjít
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
r.	r.	kA	r.
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
díl	díl	k1gInSc4	díl
byl	být	k5eAaImAgInS	být
psán	psát	k5eAaImNgInS	psát
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1845	[number]	k4	1845
-	-	kIx~	-
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejvíce	nejvíce	k6eAd1	nejvíce
sbírala	sbírat	k5eAaImAgFnS	sbírat
materiál	materiál	k1gInSc4	materiál
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Domažlic	Domažlice	k1gFnPc2	Domažlice
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgInSc1	pátý
svazek	svazek	k1gInSc1	svazek
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
září	září	k1gNnSc6	září
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
šestý	šestý	k4xOgMnSc1	šestý
pak	pak	k9	pak
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
Sedmý	sedmý	k4xOgInSc1	sedmý
svazek	svazek	k1gInSc1	svazek
vyšel	vyjít	k5eAaPmAgInS	vyjít
až	až	k9	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgNnSc4d1	rozšířené
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1854	[number]	k4	1854
-1855	-1855	k4	-1855
<g/>
.	.	kIx.	.
</s>
<s>
Tematicky	tematicky	k6eAd1	tematicky
se	se	k3xPyFc4	se
skladby	skladba	k1gFnPc1	skladba
opírají	opírat	k5eAaImIp3nP	opírat
o	o	k7c6	o
podání	podání	k1gNnSc6	podání
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
Chodska	Chodsko	k1gNnSc2	Chodsko
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
až	až	k9	až
sedmý	sedmý	k4xOgInSc1	sedmý
sešit	sešit	k1gInSc1	sešit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
pohádek	pohádka	k1gFnPc2	pohádka
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
dvou	dva	k4xCgFnPc2	dva
sešitů	sešit	k1gInPc2	sešit
nejsou	být	k5eNaImIp3nP	být
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
zapsáním	zapsání	k1gNnSc7	zapsání
lidových	lidový	k2eAgInPc2d1	lidový
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
umělé	umělý	k2eAgFnPc1d1	umělá
<g/>
.	.	kIx.	.
</s>
<s>
Neohrožený	ohrožený	k2eNgMnSc1d1	neohrožený
Mikeš	Mikeš	k1gMnSc1	Mikeš
-	-	kIx~	-
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
jako	jako	k8xS	jako
O	o	k7c6	o
statečném	statečný	k2eAgMnSc6d1	statečný
kováři	kovář	k1gMnSc6	kovář
O	o	k7c6	o
Popelce	Popelka	k1gFnSc6	Popelka
Čertův	čertův	k2eAgMnSc1d1	čertův
švagr	švagr	k1gMnSc1	švagr
Sedmero	Sedmero	k1gNnSc4	Sedmero
krkavců	krkavec	k1gMnPc2	krkavec
Princ	princ	k1gMnSc1	princ
Bajaja	Bajaja	k1gMnSc1	Bajaja
Čert	čert	k1gMnSc1	čert
a	a	k8xC	a
Káča	Káča	k1gFnSc1	Káča
O	o	k7c6	o
Perníkové	perníkový	k2eAgFnSc6d1	Perníková
chaloupce	chaloupka	k1gFnSc6	chaloupka
O	o	k7c6	o
princezně	princezna	k1gFnSc6	princezna
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
hvězdou	hvězda	k1gFnSc7	hvězda
Šternberk	Šternberk	k1gInSc1	Šternberk
-	-	kIx~	-
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
jako	jako	k8xC	jako
Jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
zasloužit	zasloužit	k5eAaPmF	zasloužit
princeznu	princezna	k1gFnSc4	princezna
Potrestaná	potrestaný	k2eAgFnSc1d1	potrestaná
pýcha	pýcha	k1gFnSc1	pýcha
-	-	kIx~	-
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
jako	jako	k8xC	jako
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
O	o	k7c6	o
Slunečníku	slunečník	k1gInSc6	slunečník
<g/>
,	,	kIx,	,
Měsíčníku	měsíčník	k1gInSc6	měsíčník
a	a	k8xC	a
Větrníku	větrník	k1gInSc6	větrník
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
jako	jako	k8xC	jako
Princ	princ	k1gMnSc1	princ
a	a	k8xC	a
Večernice	večernice	k1gFnSc1	večernice
O	o	k7c6	o
chytré	chytrý	k2eAgFnSc6d1	chytrá
princezně	princezna	k1gFnSc6	princezna
a	a	k8xC	a
Spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
Bohumil	Bohumil	k1gMnSc1	Bohumil
-	-	kIx~	-
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
jako	jako	k8xC	jako
Čertova	čertův	k2eAgFnSc1d1	Čertova
nevěsta	nevěsta	k1gFnSc1	nevěsta
Chytrá	chytrá	k1gFnSc1	chytrá
horákyně	horákyně	k1gFnSc1	horákyně
O	o	k7c6	o
Smolíčkovi	Smolíček	k1gMnSc6	Smolíček
O	o	k7c6	o
kohoutkovi	kohoutek	k1gMnSc6	kohoutek
a	a	k8xC	a
slepičce	slepička	k1gFnSc6	slepička
O	o	k7c6	o
zlatém	zlatý	k2eAgInSc6d1	zlatý
kolovrátku	kolovrátek	k1gInSc6	kolovrátek
O	o	k7c6	o
hloupém	hloupý	k2eAgMnSc6d1	hloupý
Honzovi	Honz	k1gMnSc6	Honz
Alabastrová	alabastrový	k2eAgFnSc1d1	alabastrová
ručička	ručička	k1gFnSc1	ručička
O	o	k7c6	o
mluvicím	mluvicí	k2eAgMnSc6d1	mluvicí
ptáku	pták	k1gMnSc6	pták
<g/>
,	,	kIx,	,
živé	živý	k2eAgFnSc6d1	živá
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
třech	tři	k4xCgFnPc6	tři
zlatých	zlatý	k2eAgFnPc6d1	zlatá
jabloních	jabloň	k1gFnPc6	jabloň
O	o	k7c6	o
Nesytovi	nesyta	k1gMnSc3	nesyta
Pohádka	pohádka	k1gFnSc1	pohádka
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Honzík	Honzík	k1gMnSc1	Honzík
učil	učít	k5eAaPmAgMnS	učít
latinsky	latinsky	k6eAd1	latinsky
O	o	k7c6	o
labuti	labuť	k1gFnSc6	labuť
Divotvorná	divotvorný	k2eAgFnSc1d1	divotvorná
harfa	harfa	k1gFnSc1	harfa
O	o	k7c6	o
zakletém	zakletý	k2eAgMnSc6d1	zakletý
hadovi	had	k1gMnSc6	had
-	-	kIx~	-
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
pod	pod	k7c7	pod
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
Jak	jak	k8xS	jak
Jaromil	Jaromil	k1gMnSc1	Jaromil
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc3	štěstí
přišel	přijít	k5eAaPmAgInS	přijít
-	-	kIx~	-
novelistický	novelistický	k2eAgInSc1d1	novelistický
počátek	počátek	k1gInSc1	počátek
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
o	o	k7c6	o
synu	syn	k1gMnSc6	syn
uhlíře	uhlíř	k1gMnPc4	uhlíř
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc2	který
týrá	týrat	k5eAaImIp3nS	týrat
macecha	macecha	k1gFnSc1	macecha
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gFnSc4	jeho
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
zahradnictví	zahradnictví	k1gNnSc6	zahradnictví
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgInSc1d1	zimní
život	život	k1gInSc1	život
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
předli	příst	k5eAaImAgMnP	příst
<g/>
,	,	kIx,	,
tkali	tkát	k5eAaImAgMnP	tkát
<g/>
,	,	kIx,	,
šili	šít	k5eAaImAgMnP	šít
a	a	k8xC	a
také	také	k9	také
rozhovor	rozhovor	k1gInSc1	rozhovor
Jaromila	Jaromil	k1gMnSc2	Jaromil
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
o	o	k7c6	o
povolání	povolání	k1gNnSc6	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Jaromil	Jaromil	k1gMnSc1	Jaromil
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
jedině	jedině	k6eAd1	jedině
zahradníkem	zahradník	k1gMnSc7	zahradník
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
otci	otec	k1gMnSc3	otec
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
pastvě	pastva	k1gFnSc6	pastva
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
ptáček	ptáček	k1gInSc1	ptáček
Jaromila	Jaromil	k1gMnSc2	Jaromil
ke	k	k7c3	k
skále	skála	k1gFnSc3	skála
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
tajné	tajný	k2eAgFnSc2d1	tajná
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
pestrobarevná	pestrobarevný	k2eAgFnSc1d1	pestrobarevná
říše	říše	k1gFnSc1	říše
s	s	k7c7	s
pidmimužíky	pidmimužík	k1gInPc7	pidmimužík
a	a	k8xC	a
děvčaty	děvče	k1gNnPc7	děvče
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgNnP	starat
o	o	k7c4	o
zahradu	zahrada	k1gFnSc4	zahrada
a	a	k8xC	a
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
říši	říš	k1gFnSc6	říš
Jaromila	Jaromil	k1gMnSc4	Jaromil
provedla	provést	k5eAaPmAgFnS	provést
králova	králův	k2eAgFnSc1d1	králova
dcera	dcera	k1gFnSc1	dcera
Narciska	narciska	k1gFnSc1	narciska
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
musel	muset	k5eAaImAgMnS	muset
Jaromil	Jaromil	k1gMnSc1	Jaromil
říši	říše	k1gFnSc4	říše
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
vodní	vodní	k2eAgFnSc2d1	vodní
víly	víla	k1gFnSc2	víla
škebli	škeble	k1gFnSc4	škeble
s	s	k7c7	s
perlou	perla	k1gFnSc7	perla
<g/>
,	,	kIx,	,
od	od	k7c2	od
ohnivé	ohnivý	k2eAgFnSc2d1	ohnivá
dívky	dívka	k1gFnSc2	dívka
lahvičku	lahvička	k1gFnSc4	lahvička
s	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
od	od	k7c2	od
Narcisky	narciska	k1gFnSc2	narciska
zlatou	zlatý	k2eAgFnSc4d1	zlatá
pecku	pecka	k1gFnSc4	pecka
a	a	k8xC	a
růžové	růžový	k2eAgNnSc4d1	růžové
listí	listí	k1gNnSc4	listí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
vyšel	vyjít	k5eAaPmAgMnS	vyjít
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uběhlo	uběhnout	k5eAaPmAgNnS	uběhnout
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
našel	najít	k5eAaPmAgMnS	najít
práci	práce	k1gFnSc4	práce
jako	jako	k8xS	jako
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
,	,	kIx,	,
a	a	k8xC	a
těmi	ten	k3xDgMnPc7	ten
třemi	tři	k4xCgInPc7	tři
dary	dar	k1gInPc7	dar
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
nemocnou	nemocný	k2eAgFnSc4d1	nemocná
princeznu	princezna	k1gFnSc4	princezna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Líčení	líčení	k1gNnSc1	líčení
podzemní	podzemní	k2eAgFnSc2d1	podzemní
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
zjevování	zjevování	k1gNnSc2	zjevování
víl	víla	k1gFnPc2	víla
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vroucné	vroucný	k2eAgNnSc1d1	vroucné
a	a	k8xC	a
líbezné	líbezný	k2eAgNnSc1d1	líbezné
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
živý	živý	k2eAgMnSc1d1	živý
a	a	k8xC	a
svěže	svěže	k6eAd1	svěže
psaný	psaný	k2eAgInSc1d1	psaný
stylem	styl	k1gInSc7	styl
kouzelných	kouzelný	k2eAgFnPc2d1	kouzelná
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Němcová	Němcová	k1gFnSc1	Němcová
vložila	vložit	k5eAaPmAgFnS	vložit
do	do	k7c2	do
pohádky	pohádka	k1gFnSc2	pohádka
svoje	svůj	k3xOyFgInPc1	svůj
dětské	dětský	k2eAgInPc1d1	dětský
sny	sen	k1gInPc1	sen
a	a	k8xC	a
kouzelné	kouzelný	k2eAgFnSc3d1	kouzelná
ideální	ideální	k2eAgFnSc3d1	ideální
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
dodala	dodat	k5eAaPmAgFnS	dodat
jim	on	k3xPp3gMnPc3	on
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
půvabu	půvab	k1gInSc6	půvab
obrazem	obraz	k1gInSc7	obraz
ideální	ideální	k2eAgFnSc2d1	ideální
rovnosti	rovnost	k1gFnSc2	rovnost
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
podzemního	podzemní	k2eAgInSc2d1	podzemní
ráje	ráj	k1gInSc2	ráj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
pramenem	pramen	k1gInSc7	pramen
života	život	k1gInSc2	život
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
Národních	národní	k2eAgFnPc2d1	národní
báchorek	báchorka	k1gFnPc2	báchorka
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
české	český	k2eAgFnSc2d1	Česká
literární	literární	k2eAgFnSc2d1	literární
tradice	tradice	k1gFnSc2	tradice
vyzdvihovali	vyzdvihovat	k5eAaImAgMnP	vyzdvihovat
již	již	k9	již
J.	J.	kA	J.
K.	K.	kA	K.
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Havlíček	Havlíček	k1gMnSc1	Havlíček
a	a	k8xC	a
K.	K.	kA	K.
Sabina	Sabina	k1gFnSc1	Sabina
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
slovenským	slovenský	k2eAgInSc7d1	slovenský
folklorem	folklor	k1gInSc7	folklor
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
při	při	k7c6	při
návštěvách	návštěva	k1gFnPc6	návštěva
manžela	manžel	k1gMnSc2	manžel
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Adaptovala	adaptovat	k5eAaBmAgFnS	adaptovat
zápisy	zápis	k1gInPc4	zápis
slovenských	slovenský	k2eAgMnPc2d1	slovenský
překladatelů	překladatel	k1gMnPc2	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
ponechávala	ponechávat	k5eAaImAgFnS	ponechávat
dialogy	dialog	k1gInPc4	dialog
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
Převaha	převaha	k1gFnSc1	převaha
kouzelných	kouzelný	k2eAgFnPc2d1	kouzelná
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
,	,	kIx,	,
ústní	ústní	k2eAgNnSc1d1	ústní
vyprávění	vyprávění	k1gNnSc1	vyprávění
bylo	být	k5eAaImAgNnS	být
uchováno	uchovat	k5eAaPmNgNnS	uchovat
věrněji	věrně	k6eAd2	věrně
než	než	k8xS	než
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dvanácti	dvanáct	k4xCc6	dvanáct
měsíčkách	měsíček	k1gInPc6	měsíček
Sůl	sůl	k1gFnSc4	sůl
nad	nad	k7c4	nad
zlato	zlato	k1gNnSc4	zlato
O	o	k7c6	o
bačovi	bača	k1gMnSc3	bača
a	a	k8xC	a
šarkanu	šarkan	k1gMnSc3	šarkan
Zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
lidový	lidový	k2eAgInSc4d1	lidový
život	život	k1gInSc4	život
přenesla	přenést	k5eAaPmAgFnS	přenést
Němcová	Němcová	k1gFnSc1	Němcová
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
života	život	k1gInSc2	život
i	i	k9	i
na	na	k7c4	na
širší	široký	k2eAgFnSc4d2	širší
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Překládala	překládat	k5eAaImAgFnS	překládat
ukázky	ukázka	k1gFnPc1	ukázka
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
slovinské	slovinský	k2eAgFnSc2d1	slovinská
<g/>
,	,	kIx,	,
srbské	srbský	k2eAgFnSc2d1	Srbská
a	a	k8xC	a
bulharské	bulharský	k2eAgFnSc2d1	bulharská
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
sestavit	sestavit	k5eAaPmF	sestavit
soubor	soubor	k1gInSc4	soubor
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
pohádek	pohádka	k1gFnPc2	pohádka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
neuskutečněna	uskutečnit	k5eNaPmNgFnS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
-	-	kIx~	-
světově	světově	k6eAd1	světově
proslulé	proslulý	k2eAgNnSc4d1	proslulé
dílo	dílo	k1gNnSc4	dílo
napsané	napsaný	k2eAgNnSc4d1	napsané
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
syna	syn	k1gMnSc2	syn
Hynka	Hynek	k1gMnSc2	Hynek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
badatelů	badatel	k1gMnPc2	badatel
Němcovou	Němcová	k1gFnSc4	Němcová
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
této	tento	k3xDgFnSc2	tento
povídky	povídka	k1gFnSc2	povídka
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
Pěstounka	pěstounka	k1gFnSc1	pěstounka
Františka	František	k1gMnSc2	František
Jana	Jan	k1gMnSc2	Jan
Mošnera	Mošner	k1gMnSc2	Mošner
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
zde	zde	k6eAd1	zde
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
šťastné	šťastný	k2eAgNnSc4d1	šťastné
dětství	dětství	k1gNnSc4	dětství
prožité	prožitý	k2eAgFnSc2d1	prožitá
v	v	k7c6	v
ratibořickém	ratibořický	k2eAgNnSc6d1	Ratibořické
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
babička	babička	k1gFnSc1	babička
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
)	)	kIx)	)
-	-	kIx~	-
silně	silně	k6eAd1	silně
zidealizovaná	zidealizovaný	k2eAgFnSc1d1	zidealizovaná
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
symbolem	symbol	k1gInSc7	symbol
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
začíná	začínat	k5eAaImIp3nS	začínat
příjezdem	příjezd	k1gInSc7	příjezd
babičky	babička	k1gFnSc2	babička
na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
Bělidlo	bělidlo	k1gNnSc4	bělidlo
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
kapitoly	kapitola	k1gFnPc1	kapitola
popisují	popisovat	k5eAaImIp3nP	popisovat
život	život	k1gInSc4	život
na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
a	a	k8xC	a
obyvatele	obyvatel	k1gMnSc2	obyvatel
ratibořického	ratibořický	k2eAgNnSc2d1	Ratibořické
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
svojí	svůj	k3xOyFgFnSc7	svůj
morální	morální	k2eAgFnSc7d1	morální
hodnotou	hodnota	k1gFnSc7	hodnota
výrazně	výrazně	k6eAd1	výrazně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
postav	postava	k1gFnPc2	postava
-	-	kIx~	-
včetně	včetně	k7c2	včetně
kněžny	kněžna	k1gFnSc2	kněžna
(	(	kIx(	(
<g/>
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
<g/>
)	)	kIx)	)
ze	z	k7c2	z
Zaháně	Zaháň	k1gFnSc2	Zaháň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
zidealizovaná	zidealizovaný	k2eAgFnSc1d1	zidealizovaná
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
doložena	doložit	k5eAaPmNgFnS	doložit
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
vyznívá	vyznívat	k5eAaImIp3nS	vyznívat
optimisticky	optimisticky	k6eAd1	optimisticky
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
babiččinu	babiččin	k2eAgFnSc4d1	babiččina
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
i	i	k9	i
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
kompozice	kompozice	k1gFnSc2	kompozice
díla	dílo	k1gNnSc2	dílo
víceméně	víceméně	k9	víceméně
nezapadá	zapadat	k5eNaImIp3nS	zapadat
-	-	kIx~	-
šílená	šílený	k2eAgFnSc1d1	šílená
Viktorka	Viktorka	k1gFnSc1	Viktorka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
mladá	mladý	k2eAgFnSc1d1	mladá
komtesa	komtesa	k1gFnSc1	komtesa
Hortenzie	hortenzie	k1gFnSc1	hortenzie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
by	by	kYmCp3nS	by
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
určitou	určitý	k2eAgFnSc4d1	určitá
realitu	realita	k1gFnSc4	realita
světa	svět	k1gInSc2	svět
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
<g/>
.	.	kIx.	.
</s>
<s>
Němcová	Němcová	k1gFnSc1	Němcová
dala	dát	k5eAaPmAgFnS	dát
tomuto	tento	k3xDgInSc3	tento
svému	svůj	k3xOyFgNnSc3	svůj
nejslavnějšímu	slavný	k2eAgNnSc3d3	nejslavnější
dílu	dílo	k1gNnSc3	dílo
<g/>
,	,	kIx,	,
přeloženému	přeložený	k2eAgNnSc3d1	přeložené
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
podtitul	podtitul	k1gInSc1	podtitul
Obrazy	obraz	k1gInPc1	obraz
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pohorská	pohorský	k2eAgFnSc1d1	Pohorská
vesnice	vesnice	k1gFnSc1	vesnice
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
a	a	k8xC	a
v	v	k7c6	v
podzámčí	podzámčí	k1gNnSc6	podzámčí
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
Z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
Vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
po	po	k7c6	po
Uhrách	Uhry	k1gFnPc6	Uhry
Obrazy	obraz	k1gInPc4	obraz
ze	z	k7c2	z
života	život	k1gInSc2	život
slovenského	slovenský	k2eAgInSc2d1	slovenský
Kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
lesy	les	k1gInPc1	les
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Obrazy	obraz	k1gInPc4	obraz
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Domažlického	domažlický	k2eAgNnSc2d1	Domažlické
-	-	kIx~	-
publicistická	publicistický	k2eAgFnSc1d1	publicistická
próza	próza	k1gFnSc1	próza
<g/>
,	,	kIx,	,
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zachycení	zachycení	k1gNnSc4	zachycení
folklóru	folklór	k1gInSc2	folklór
<g/>
,	,	kIx,	,
obyčejů	obyčej	k1gInPc2	obyčej
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Selská	selský	k2eAgFnSc1d1	selská
svatba	svatba	k1gFnSc1	svatba
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Domažlic	Domažlice	k1gFnPc2	Domažlice
O	o	k7c6	o
prostonárodním	prostonárodní	k2eAgNnSc6d1	prostonárodní
léčení	léčení	k1gNnSc6	léčení
na	na	k7c6	na
Domažlicku	Domažlicko	k1gNnSc6	Domažlicko
NĚMCOVÁ	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
Božena	Božena	k1gFnSc1	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
:	:	kIx,	:
obrazy	obraz	k1gInPc1	obraz
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Adolf	Adolf	k1gMnSc1	Adolf
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
grafická	grafický	k2eAgFnSc1d1	grafická
Unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
NĚMCOVÁ	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
Božena	Božena	k1gFnSc1	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnPc1d1	národní
báchorky	báchorka	k1gFnPc1	báchorka
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
:	:	kIx,	:
Antonín	Antonín	k1gMnSc1	Antonín
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Digitalizované	digitalizovaný	k2eAgNnSc1d1	digitalizované
dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sešit	sešit	k1gInSc1	sešit
I.	I.	kA	I.
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
..	..	k?	..
NĚMCOVÁ	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
Božena	Božena	k1gFnSc1	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Alabastrová	alabastrový	k2eAgFnSc1d1	alabastrová
ručička	ručička	k1gFnSc1	ručička
:	:	kIx,	:
O	o	k7c6	o
princezně	princezna	k1gFnSc6	princezna
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
:	:	kIx,	:
Dobré	dobrý	k2eAgFnPc1d1	dobrá
kmotřinky	kmotřinka	k1gFnPc1	kmotřinka
:	:	kIx,	:
Vděčné	vděčný	k2eAgFnPc1d1	vděčná
zvířátka	zvířátko	k1gNnSc2	zvířátko
:	:	kIx,	:
Potrestaná	potrestaný	k2eAgFnSc1d1	potrestaná
pýcha	pýcha	k1gFnSc1	pýcha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
63	[number]	k4	63
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
NĚMCOVÁ	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
Božena	Božena	k1gFnSc1	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Čertův	čertův	k2eAgMnSc1d1	čertův
švagr	švagr	k1gMnSc1	švagr
:	:	kIx,	:
Neohrožený	ohrožený	k2eNgMnSc1d1	neohrožený
Mikeš	Mikeš	k1gMnSc1	Mikeš
:	:	kIx,	:
O	o	k7c6	o
Nesytovi	nesyta	k1gMnSc6	nesyta
:	:	kIx,	:
Silný	silný	k2eAgMnSc1d1	silný
Ctibor	Ctibor	k1gMnSc1	Ctibor
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
63	[number]	k4	63
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
-	-	kIx~	-
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Innemanna	Innemann	k1gMnSc2	Innemann
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
Marta	Marta	k1gFnSc1	Marta
Májová	májová	k1gFnSc1	májová
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
-	-	kIx~	-
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Innemanna	Innemann	k1gMnSc2	Innemann
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
Němcové	Němcové	k2eAgFnSc1d1	Němcové
Božena	Božena	k1gFnSc1	Božena
Svobodová	Svobodová	k1gFnSc1	Svobodová
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgInSc4d1	revoluční
rok	rok	k1gInSc4	rok
1848	[number]	k4	1848
-	-	kIx~	-
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Václava	Václav	k1gMnSc2	Václav
Kršky	Krška	k1gMnSc2	Krška
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
Němcové	Němcové	k2eAgFnSc1d1	Němcové
Vlasta	Vlasta	k1gFnSc1	Vlasta
Fabianová	Fabianová	k1gFnSc1	Fabianová
<g/>
.	.	kIx.	.
</s>
<s>
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
-	-	kIx~	-
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
režisérky	režisérka	k1gFnSc2	režisérka
Evy	Eva	k1gFnSc2	Eva
Marie	Maria	k1gFnSc2	Maria
Bergerové	Bergerová	k1gFnSc2	Bergerová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Dana	Dana	k1gFnSc1	Dana
Medřická	Medřická	k1gFnSc1	Medřická
<g/>
.	.	kIx.	.
</s>
<s>
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
-	-	kIx~	-
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
životě	život	k1gInSc6	život
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Horoucí	horoucí	k2eAgNnSc1d1	horoucí
srdce	srdce	k1gNnSc1	srdce
-	-	kIx~	-
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Otakara	Otakar	k1gMnSc2	Otakar
Vávry	Vávra	k1gMnSc2	Vávra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Jiřina	Jiřina	k1gFnSc1	Jiřina
Švorcová	švorcový	k2eAgFnSc1d1	Švorcová
<g/>
.	.	kIx.	.
</s>
<s>
Vlčí	vlčí	k2eAgInSc1d1	vlčí
halíř	halíř	k1gInSc1	halíř
-	-	kIx~	-
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Novotného	Novotný	k1gMnSc2	Novotný
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Jana	Jana	k1gFnSc1	Jana
Březinová	Březinová	k1gFnSc1	Březinová
<g/>
.	.	kIx.	.
</s>
<s>
Ako	Ako	k?	Ako
listy	lista	k1gFnPc4	lista
jedného	jedný	k2eAgInSc2d1	jedný
stromu	strom	k1gInSc2	strom
-	-	kIx~	-
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Kavčiaka	Kavčiak	k1gMnSc2	Kavčiak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Dana	Dana	k1gFnSc1	Dana
Syslová	Syslová	k1gFnSc1	Syslová
<g/>
.	.	kIx.	.
</s>
<s>
Veronika	Veronika	k1gFnSc1	Veronika
-	-	kIx~	-
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Otakara	Otakar	k1gMnSc2	Otakar
Vávry	Vávra	k1gMnSc2	Vávra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
Němcové	Němcové	k2eAgMnSc2d1	Němcové
Jana	Jan	k1gMnSc2	Jan
Hlaváčová	Hlaváčová	k1gFnSc1	Hlaváčová
<g/>
.	.	kIx.	.
</s>
<s>
Durch	durch	k6eAd1	durch
diese	diese	k6eAd1	diese
Nacht	Nacht	k2eAgInSc1d1	Nacht
sehe	sehe	k1gInSc1	sehe
ich	ich	k?	ich
keinen	keinen	k2eAgInSc1d1	keinen
einzigen	einzigen	k1gInSc1	einzigen
Stern	sternum	k1gNnPc2	sternum
(	(	kIx(	(
<g/>
A	a	k9	a
tou	ten	k3xDgFnSc7	ten
nocí	noc	k1gFnSc7	noc
nevidím	vidět	k5eNaImIp1nS	vidět
ani	ani	k8xC	ani
jedinou	jediný	k2eAgFnSc4d1	jediná
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
)	)	kIx)	)
-	-	kIx~	-
česko-německý	českoěmecký	k2eAgInSc1d1	česko-německý
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Dagmar	Dagmar	k1gFnSc1	Dagmar
Knöpfel	Knöpfel	k1gInSc1	Knöpfel
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Corinna	Corinn	k1gMnSc2	Corinn
Harfouch	Harfouch	k1gMnSc1	Harfouch
jako	jako	k8xS	jako
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
a	a	k8xC	a
Bolek	Bolek	k1gMnSc1	Bolek
Polívka	Polívka	k1gMnSc1	Polívka
jako	jako	k8xC	jako
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
ze	z	k7c2	z
života	život	k1gInSc2	život
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
-	-	kIx~	-
televizní	televizní	k2eAgInSc1d1	televizní
dokument	dokument	k1gInSc1	dokument
režisérek	režisérka	k1gFnPc2	režisérka
Ljuby	Ljuba	k1gFnSc2	Ljuba
Václavové	Václavové	k2eAgFnSc2d1	Václavové
a	a	k8xC	a
Martiny	Martin	k2eAgFnSc2d1	Martina
Komárkové	Komárková	k1gFnSc2	Komárková
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Lenka	Lenka	k1gFnSc1	Lenka
Vlasáková	Vlasáková	k1gFnSc1	Vlasáková
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
</s>
