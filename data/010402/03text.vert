<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
rodu	rod	k1gInSc2	rod
Canis	Canis	k1gFnSc2	Canis
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
užívaný	užívaný	k2eAgInSc1d1	užívaný
i	i	k9	i
pro	pro	k7c4	pro
rody	rod	k1gInPc4	rod
Cuon	Cuona	k1gFnPc2	Cuona
a	a	k8xC	a
Chrysocyon	Chrysocyona	k1gFnPc2	Chrysocyona
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
poddruhů	poddruh	k1gInPc2	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
poddruhy	poddruh	k1gInPc1	poddruh
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
arabský	arabský	k2eAgMnSc1d1	arabský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
arabs	arabs	k1gInSc4	arabs
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
lupus	lupus	k1gInSc1	lupus
pambasileus	pambasileus	k1gInSc1	pambasileus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
eurasijský	eurasijský	k2eAgMnSc1d1	eurasijský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
lupus	lupus	k1gInSc4	lupus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
pallipes	pallipes	k1gInSc4	pallipes
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
kanadský	kanadský	k2eAgMnSc1d1	kanadský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
occidentalis	occidentalis	k1gFnSc1	occidentalis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
mongolský	mongolský	k2eAgMnSc1d1	mongolský
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
lupus	lupus	k1gInSc1	lupus
chanco	chanco	k1gMnSc1	chanco
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
polární	polární	k2eAgMnSc1d1	polární
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
tundrarum	tundrarum	k1gInSc4	tundrarum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
iberský	iberský	k2eAgMnSc1d1	iberský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
signatus	signatus	k1gInSc4	signatus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
arktický	arktický	k2eAgMnSc1d1	arktický
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
arctos	arctos	k1gInSc4	arctos
<g/>
)	)	kIx)	)
–	–	k?	–
spekulovaný	spekulovaný	k2eAgInSc4d1	spekulovaný
poddruh	poddruh	k1gInSc4	poddruh
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
arizonský	arizonský	k2eAgMnSc1d1	arizonský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
mogollonensis	mogollonensis	k1gFnSc1	mogollonensis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
horský	horský	k2eAgMnSc1d1	horský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
fuscus	fuscus	k1gInSc4	fuscus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
japonský	japonský	k2eAgMnSc1d1	japonský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
hodophilax	hodophilax	k1gInSc4	hodophilax
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
kenajský	kenajský	k2eAgMnSc1d1	kenajský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
alces	alces	k1gInSc4	alces
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
koloradský	koloradský	k2eAgMnSc1d1	koloradský
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnPc1	Canis
lupus	lupus	k1gInSc4	lupus
youngi	young	k1gFnSc2	young
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
novofoundlandský	novofoundlandský	k2eAgMnSc1d1	novofoundlandský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
beothucus	beothucus	k1gInSc4	beothucus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
ostrovní	ostrovní	k2eAgMnSc1d1	ostrovní
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnPc1	Canis
lupus	lupus	k1gInSc4	lupus
hattai	hatta	k1gFnSc2	hatta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
prériový	prériový	k2eAgMnSc1d1	prériový
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
nubilus	nubilus	k1gInSc4	nubilus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
španělský	španělský	k2eAgMnSc1d1	španělský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
deitanus	deitanus	k1gInSc4	deitanus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
†	†	k?	†
vlk	vlk	k1gMnSc1	vlk
texaský	texaský	k2eAgMnSc1d1	texaský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
monstrabilis	monstrabilis	k1gFnSc1	monstrabilis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
k	k	k7c3	k
druhu	druh	k1gInSc3	druh
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
pes	pes	k1gMnSc1	pes
dingo	dingo	k1gMnSc1	dingo
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
lupus	lupus	k1gInSc1	lupus
dingo	dingo	k1gMnSc1	dingo
<g/>
)	)	kIx)	)
a	a	k8xC	a
dingo	dingo	k1gMnSc1	dingo
pralesní	pralesní	k2eAgMnSc1d1	pralesní
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
dingo	dingo	k1gMnSc1	dingo
hallstromi	hallstro	k1gFnPc7	hallstro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
postupnou	postupný	k2eAgFnSc7d1	postupná
domestikací	domestikace	k1gFnSc7	domestikace
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
poddruh	poddruh	k1gInSc1	poddruh
pes	pes	k1gMnSc1	pes
domácí	domácí	k1gMnSc1	domácí
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
lupus	lupus	k1gInSc1	lupus
familiaris	familiaris	k1gFnSc1	familiaris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
rudohnědý	rudohnědý	k2eAgMnSc1d1	rudohnědý
=	=	kIx~	=
vlk	vlk	k1gMnSc1	vlk
červený	červený	k2eAgMnSc1d1	červený
=	=	kIx~	=
vlk	vlk	k1gMnSc1	vlk
červenohnědý	červenohnědý	k2eAgMnSc1d1	červenohnědý
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
rufus	rufus	k1gInSc1	rufus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlk	vlk	k1gMnSc1	vlk
himálajský	himálajský	k2eAgMnSc1d1	himálajský
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
himalayensis	himalayensis	k1gFnSc2	himalayensis
<g/>
)	)	kIx)	)
–	–	k?	–
někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
ho	on	k3xPp3gInSc4	on
považují	považovat	k5eAaImIp3nP	považovat
jen	jen	k9	jen
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
vlka	vlk	k1gMnSc2	vlk
obecného	obecný	k2eAgMnSc2d1	obecný
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
označováni	označován	k2eAgMnPc1d1	označován
i	i	k8xC	i
šakal	šakal	k1gMnSc1	šakal
obecný	obecný	k2eAgInSc4d1	obecný
Canis	Canis	k1gInSc4	Canis
aureus	aureus	k1gMnSc1	aureus
(	(	kIx(	(
<g/>
vlk	vlk	k1gMnSc1	vlk
šakalovitý	šakalovitý	k2eAgMnSc1d1	šakalovitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
kojot	kojot	k1gMnSc1	kojot
prériový	prériový	k2eAgInSc4d1	prériový
Canis	Canis	k1gInSc4	Canis
latrans	latrans	k1gInSc1	latrans
(	(	kIx(	(
<g/>
vlk	vlk	k1gMnSc1	vlk
stepní	stepní	k2eAgMnSc1d1	stepní
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Canis	Canis	k1gFnSc2	Canis
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
hřivnatý	hřivnatý	k2eAgMnSc1d1	hřivnatý
Chrysocyon	Chrysocyon	k1gMnSc1	Chrysocyon
brachyurus	brachyurus	k1gMnSc1	brachyurus
(	(	kIx(	(
<g/>
vlk	vlk	k1gMnSc1	vlk
hřivnatý	hřivnatý	k2eAgMnSc1d1	hřivnatý
<g/>
)	)	kIx)	)
a	a	k8xC	a
dhoul	dhoout	k5eAaPmAgMnS	dhoout
Cuon	Cuon	k1gMnSc1	Cuon
alpinus	alpinus	k1gMnSc1	alpinus
(	(	kIx(	(
<g/>
vlk	vlk	k1gMnSc1	vlk
rudý	rudý	k1gMnSc1	rudý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chrup	chrup	k1gInSc4	chrup
==	==	k?	==
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
psovité	psovitý	k2eAgFnPc1d1	psovitá
šelmy	šelma	k1gFnPc1	šelma
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
42	[number]	k4	42
zubů	zub	k1gInPc2	zub
<g/>
:	:	kIx,	:
20	[number]	k4	20
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
a	a	k8xC	a
22	[number]	k4	22
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
<g/>
.	.	kIx.	.
</s>
<s>
Vlčí	vlčí	k2eAgInPc1d1	vlčí
tesáky	tesák	k1gInPc1	tesák
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
až	až	k9	až
6,25	[number]	k4	6,25
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ostré	ostrý	k2eAgInPc1d1	ostrý
a	a	k8xC	a
lehce	lehko	k6eAd1	lehko
zakroucené	zakroucený	k2eAgFnSc6d1	zakroucená
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pevné	pevný	k2eAgNnSc4d1	pevné
uchopení	uchopení	k1gNnSc4	uchopení
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Řezáky	řezák	k1gInPc1	řezák
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
ostré	ostrý	k2eAgInPc1d1	ostrý
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vlkovi	vlkův	k2eAgMnPc1d1	vlkův
ukusovat	ukusovat	k5eAaImF	ukusovat
z	z	k7c2	z
kořisti	kořist	k1gFnSc2	kořist
velké	velký	k2eAgInPc4d1	velký
kusy	kus	k1gInPc4	kus
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
dokáže	dokázat	k5eAaPmIp3nS	dokázat
čelistmi	čelist	k1gFnPc7	čelist
vyvinout	vyvinout	k5eAaPmF	vyvinout
tlak	tlak	k1gInSc4	tlak
až	až	k9	až
10	[number]	k4	10
MPa	MPa	k1gFnPc2	MPa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
rozkousávat	rozkousávat	k5eAaImF	rozkousávat
i	i	k9	i
velké	velký	k2eAgFnPc4d1	velká
kosti	kost	k1gFnPc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
–	–	k?	–
německý	německý	k2eAgMnSc1d1	německý
ovčák	ovčák	k1gMnSc1	ovčák
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyvinout	vyvinout	k5eAaPmF	vyvinout
tlak	tlak	k1gInSc4	tlak
5	[number]	k4	5
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Speciální	speciální	k2eAgInPc1d1	speciální
smysly	smysl	k1gInPc1	smysl
a	a	k8xC	a
zákony	zákon	k1gInPc1	zákon
ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
==	==	k?	==
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
psi	pes	k1gMnPc1	pes
nebo	nebo	k8xC	nebo
kočky	kočka	k1gFnSc2	kočka
citliví	citlivý	k2eAgMnPc1d1	citlivý
na	na	k7c4	na
vibrace	vibrace	k1gFnPc4	vibrace
a	a	k8xC	a
dokáží	dokázat	k5eAaPmIp3nP	dokázat
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
blížící	blížící	k2eAgFnSc4d1	blížící
se	se	k3xPyFc4	se
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
příchodem	příchod	k1gInSc7	příchod
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
intuice	intuice	k1gFnSc2	intuice
dorozumět	dorozumět	k5eAaPmF	dorozumět
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
smečky	smečka	k1gFnSc2	smečka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
smečce	smečka	k1gFnSc6	smečka
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
"	"	kIx"	"
<g/>
alfa	alfa	k1gFnSc1	alfa
samec	samec	k1gMnSc1	samec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
smečky	smečka	k1gFnSc2	smečka
se	se	k3xPyFc4	se
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
starat	starat	k5eAaImF	starat
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
matky	matka	k1gFnSc2	matka
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
62	[number]	k4	62
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
jsou	být	k5eAaImIp3nP	být
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
nějaký	nějaký	k3yIgInSc4	nějaký
vlk	vlk	k1gMnSc1	vlk
troufne	troufnout	k5eAaPmIp3nS	troufnout
na	na	k7c4	na
samce	samec	k1gMnSc4	samec
"	"	kIx"	"
<g/>
alfa	alfa	k1gFnSc1	alfa
vlka	vlk	k1gMnSc2	vlk
<g/>
"	"	kIx"	"
a	a	k8xC	a
porazí	porazit	k5eAaPmIp3nS	porazit
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
z	z	k7c2	z
alfy	alfa	k1gFnSc2	alfa
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
samotář	samotář	k1gMnSc1	samotář
a	a	k8xC	a
z	z	k7c2	z
vlka	vlk	k1gMnSc2	vlk
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
alfa	alfa	k1gNnSc7	alfa
vlk	vlk	k1gMnSc1	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Poražený	poražený	k1gMnSc1	poražený
vlk	vlk	k1gMnSc1	vlk
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vlkem	vlk	k1gMnSc7	vlk
"	"	kIx"	"
<g/>
samotářem	samotář	k1gMnSc7	samotář
<g/>
"	"	kIx"	"
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlka	vlk	k1gMnSc4	vlk
přijme	přijmout	k5eAaPmIp3nS	přijmout
jiná	jiný	k2eAgFnSc1d1	jiná
smečka	smečka	k1gFnSc1	smečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
==	==	k?	==
</s>
</p>
<p>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
zástupcem	zástupce	k1gMnSc7	zástupce
psovitých	psovitý	k2eAgFnPc2d1	psovitá
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
35	[number]	k4	35
až	až	k9	až
52	[number]	k4	52
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
150	[number]	k4	150
až	až	k8xS	až
190	[number]	k4	190
cm	cm	kA	cm
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc1d1	vysoký
65	[number]	k4	65
až	až	k8xS	až
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgInPc1d2	menší
–	–	k?	–
váží	vážit	k5eAaImIp3nP	vážit
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
25	[number]	k4	25
%	%	kIx~	%
méně	málo	k6eAd2	málo
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
poddruhy	poddruh	k1gInPc7	poddruh
vlka	vlk	k1gMnSc2	vlk
jsou	být	k5eAaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
hmotnosti	hmotnost	k1gFnSc6	hmotnost
i	i	k8xC	i
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
červený	červený	k2eAgMnSc1d1	červený
vlk	vlk	k1gMnSc1	vlk
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
Rufus	Rufus	k1gMnSc1	Rufus
<g/>
)	)	kIx)	)
žijící	žijící	k2eAgMnSc1d1	žijící
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Mississippi	Mississippi	k1gFnSc2	Mississippi
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
jen	jen	k9	jen
pouhých	pouhý	k2eAgInPc2d1	pouhý
15	[number]	k4	15
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
vlků	vlk	k1gMnPc2	vlk
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
vliv	vliv	k1gInSc1	vliv
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
–	–	k?	–
čím	co	k3yQnSc7	co
severněji	severně	k6eAd2	severně
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnSc1d2	veliký
(	(	kIx(	(
<g/>
Bergmannovo	Bergmannův	k2eAgNnSc1d1	Bergmannovo
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
je	být	k5eAaImIp3nS	být
arktický	arktický	k2eAgMnSc1d1	arktický
vlk	vlk	k1gMnSc1	vlk
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
Lupus	lupus	k1gInSc1	lupus
Arctos	Arctos	k1gMnSc1	Arctos
<g/>
)	)	kIx)	)
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žije	žít	k5eAaImIp3nS	žít
severněji	severně	k6eAd2	severně
<g/>
,	,	kIx,	,
drobnější	drobný	k2eAgMnPc1d2	drobnější
než	než	k8xS	než
vlci	vlk	k1gMnPc1	vlk
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
vliv	vliv	k1gInSc4	vliv
hojnost	hojnost	k1gFnSc4	hojnost
kořisti	kořist	k1gFnSc2	kořist
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kde	kde	k6eAd1	kde
vlci	vlk	k1gMnPc1	vlk
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Plné	plný	k2eAgFnSc2d1	plná
velikosti	velikost	k1gFnSc2	velikost
vlk	vlk	k1gMnSc1	vlk
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
okolo	okolo	k7c2	okolo
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlk	Vlk	k1gMnSc1	Vlk
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
současné	současný	k2eAgFnSc2d1	současná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgMnS	být
vlk	vlk	k1gMnSc1	vlk
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
je	být	k5eAaImIp3nS	být
uváděno	uváděn	k2eAgNnSc4d1	uváděno
datum	datum	k1gNnSc4	datum
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1874	[number]	k4	1874
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c4	v
ČR	ČR	kA	ČR
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
náhodnému	náhodný	k2eAgInSc3d1	náhodný
výskytu	výskyt	k1gInSc3	výskyt
<g/>
,	,	kIx,	,
když	když	k8xS	když
osamělí	osamělý	k2eAgMnPc1d1	osamělý
jedinci	jedinec	k1gMnPc1	jedinec
zabloudili	zabloudit	k5eAaPmAgMnP	zabloudit
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
lze	lze	k6eAd1	lze
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
vlčí	vlčí	k2eAgFnSc2d1	vlčí
populace	populace	k1gFnSc2	populace
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
výskyt	výskyt	k1gInSc1	výskyt
vlků	vlk	k1gMnPc2	vlk
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
Lužici	Lužice	k1gFnSc4	Lužice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Beskydských	beskydský	k2eAgFnPc6d1	Beskydská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kokořínska	Kokořínsko	k1gNnSc2	Kokořínsko
či	či	k8xC	či
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgInSc2d1	bývalý
vojenského	vojenský	k2eAgInSc2d1	vojenský
výcvikového	výcvikový	k2eAgInSc2d1	výcvikový
prostoru	prostor	k1gInSc2	prostor
Ralsko	Ralsko	k1gNnSc1	Ralsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
tedy	tedy	k9	tedy
předpokládat	předpokládat	k5eAaImF	předpokládat
rozšíření	rozšíření	k1gNnSc4	rozšíření
populace	populace	k1gFnSc2	populace
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
od	od	k7c2	od
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
,	,	kIx,	,
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
či	či	k8xC	či
Českého	český	k2eAgNnSc2d1	české
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
trvalý	trvalý	k2eAgInSc1d1	trvalý
pobyt	pobyt	k1gInSc1	pobyt
vlků	vlk	k1gMnPc2	vlk
na	na	k7c6	na
Broumovsku	Broumovsko	k1gNnSc6	Broumovsko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
po	po	k7c6	po
250	[number]	k4	250
letech	léto	k1gNnPc6	léto
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byli	být	k5eAaImAgMnP	být
doloženi	doložen	k2eAgMnPc1d1	doložen
čtyři	čtyři	k4xCgMnPc1	čtyři
vlci	vlk	k1gMnPc1	vlk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
7	[number]	k4	7
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
2019	[number]	k4	2019
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
členové	člen	k1gMnPc1	člen
honebního	honební	k2eAgNnSc2d1	honební
společenstva	společenstvo	k1gNnSc2	společenstvo
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Orlickém	orlický	k2eAgNnSc6d1	Orlické
Záhoří	Záhoří	k1gNnSc6	Záhoří
a	a	k8xC	a
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
Mezilesí	mezilesí	k1gNnSc2	mezilesí
<g/>
,	,	kIx,	,
k	k	k7c3	k
témuž	týž	k3xTgNnSc3	týž
datu	datum	k1gNnSc3	datum
chovatel	chovatel	k1gMnSc1	chovatel
na	na	k7c4	na
Borové	borový	k2eAgMnPc4d1	borový
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Očekáváme	očekávat	k5eAaImIp1nP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
brzy	brzy	k6eAd1	brzy
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
zdomácní	zdomácnit	k5eAaPmIp3nS	zdomácnit
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
ještě	ještě	k9	ještě
letos	letos	k6eAd1	letos
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
Broumovsku	Broumovsko	k1gNnSc3	Broumovsko
je	být	k5eAaImIp3nS	být
Olešnice	Olešnice	k1gFnSc1	Olešnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podle	podle	k7c2	podle
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
poznatků	poznatek	k1gInPc2	poznatek
sondují	sondovat	k5eAaImIp3nP	sondovat
budoucí	budoucí	k2eAgNnSc4d1	budoucí
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
vlků	vlk	k1gMnPc2	vlk
se	se	k3xPyFc4	se
nebojí	bát	k5eNaImIp3nP	bát
přiblížit	přiblížit	k5eAaPmF	přiblížit
k	k	k7c3	k
lidskému	lidský	k2eAgNnSc3d1	lidské
obydlí	obydlí	k1gNnSc3	obydlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mytologie	mytologie	k1gFnSc2	mytologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
byl	být	k5eAaImAgMnS	být
zpupný	zpupný	k2eAgMnSc1d1	zpupný
král	král	k1gMnSc1	král
Lykáón	Lykáón	k1gMnSc1	Lykáón
proměněn	proměněn	k2eAgMnSc1d1	proměněn
ve	v	k7c6	v
vlka	vlk	k1gMnSc2	vlk
kvůli	kvůli	k7c3	kvůli
špatné	špatný	k2eAgFnSc3d1	špatná
povaze	povaha	k1gFnSc3	povaha
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzepřel	vzepřít	k5eAaPmAgInS	vzepřít
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Vlk	Vlk	k1gMnSc1	Vlk
byl	být	k5eAaImAgMnS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
boha	bůh	k1gMnSc2	bůh
Apollóna	Apollón	k1gMnSc2	Apollón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
hrál	hrát	k5eAaImAgMnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
vlk	vlk	k1gMnSc1	vlk
Fenrir	Fenrir	k1gMnSc1	Fenrir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgMnS	být
vlk	vlk	k1gMnSc1	vlk
(	(	kIx(	(
<g/>
Canis	Canis	k1gInSc1	Canis
lupus	lupus	k1gInSc1	lupus
<g/>
)	)	kIx)	)
reintrodukován	reintrodukován	k2eAgMnSc1d1	reintrodukován
do	do	k7c2	do
několika	několik	k4yIc2	několik
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
horách	hora	k1gFnPc6	hora
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Yellowstonském	Yellowstonský	k2eAgInSc6d1	Yellowstonský
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
návratem	návrat	k1gInSc7	návrat
vlka	vlk	k1gMnSc2	vlk
nejen	nejen	k6eAd1	nejen
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
skladby	skladba	k1gFnSc2	skladba
živočišstva	živočišstvo	k1gNnSc2	živočišstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vegetace	vegetace	k1gFnSc2	vegetace
a	a	k8xC	a
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Navrácení	navrácení	k1gNnSc1	navrácení
vlka	vlk	k1gMnSc2	vlk
jako	jako	k8xC	jako
vrcholového	vrcholový	k2eAgMnSc2d1	vrcholový
predátora	predátor	k1gMnSc2	predátor
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
tzv.	tzv.	kA	tzv.
trofickou	trofický	k2eAgFnSc4d1	trofická
kaskádu	kaskáda	k1gFnSc4	kaskáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vlk	vlk	k1gMnSc1	vlk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Vlk	Vlk	k1gMnSc1	Vlk
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Živý	živý	k2eAgInSc1d1	živý
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
výběhu	výběh	k1gInSc2	výběh
vlků	vlk	k1gMnPc2	vlk
<g/>
:	:	kIx,	:
http://www.zoocam.info/vlk-webkamera-ze-zoo-vybehu/	[url]	k?	http://www.zoocam.info/vlk-webkamera-ze-zoo-vybehu/
</s>
</p>
