<s>
Vikariát	vikariát	k1gInSc1
Tábor	Tábor	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
nezávislé	závislý	k2eNgInPc4d1
zdroje	zdroj	k1gInPc4
</s>
<s>
Táborský	Táborský	k1gMnSc1
vikariátDiecéze	vikariátDiecéza	k1gFnSc3
</s>
<s>
diecéze	diecéze	k1gFnSc1
českobudějovická	českobudějovický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Okrskový	okrskový	k2eAgInSc5d1
vikář	vikář	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Pulec	pulec	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
vikáře	vikář	k1gMnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
farnosti	farnost	k1gFnSc2
Tábor	tábor	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
2017	#num#	k4
(	(	kIx(
<g/>
VIII	VIII	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Proměnění	proměnění	k1gNnSc2
Páně	páně	k2eAgNnSc2d1
v	v	k7c6
Táboře	Tábor	k1gInSc6
</s>
<s>
Vikariát	vikariát	k1gInSc1
Tábor	Tábor	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
deseti	deset	k4xCc2
vikariátů	vikariát	k1gInPc2
českobudějovické	českobudějovický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
43	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
duchovní	duchovní	k2eAgFnSc6d1
správě	správa	k1gFnSc6
působí	působit	k5eAaImIp3nS
17	#num#	k4
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
Michal	Michal	k1gMnSc1
Pulec	pulec	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
v	v	k7c6
Táboře	Tábor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vikariátní	vikariátní	k2eAgInSc1d1
sekretář	sekretář	k1gInSc1
není	být	k5eNaImIp3nS
obsazen	obsadit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
vikariátu	vikariát	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
významné	významný	k2eAgNnSc1d1
mariánské	mariánský	k2eAgNnSc1d1
poutní	poutní	k2eAgNnSc1d1
místo	místo	k1gNnSc1
Klokoty	klokot	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
míst	místo	k1gNnPc2
v	v	k7c6
českobudějovické	českobudějovický	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
se	s	k7c7
Svatou	svatý	k2eAgFnSc7d1
bránou	brána	k1gFnSc7
milosrdenství	milosrdenství	k1gNnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
Jubilejního	jubilejní	k2eAgInSc2d1
roku	rok	k1gInSc2
milosrdenství	milosrdenství	k1gNnSc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1
církevní	církevní	k2eAgFnSc7d1
školou	škola	k1gFnSc7
v	v	k7c6
táborském	táborský	k2eAgInSc6d1
vikariátu	vikariát	k1gInSc6
je	být	k5eAaImIp3nS
Církevní	církevní	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Orbis-Pictus	Orbis-Pictus	k1gInSc1
v	v	k7c6
Táboře	Tábor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
farností	farnost	k1gFnPc2
</s>
<s>
Farnosti	farnost	k1gFnPc1
vikariátu	vikariát	k1gInSc2
</s>
<s>
FarnostSprávceDalší	FarnostSprávceDalší	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
ve	v	k7c4
farnostiFarní	farnostiFarní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
</s>
<s>
Bechyně	Bechyně	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Hajda	Hajda	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Matěje	Matěj	k1gMnSc4
</s>
<s>
Borotín	Borotín	k1gInSc1
u	u	k7c2
Tábora	Tábor	k1gInSc2
</s>
<s>
P.	P.	kA
Vladimír	Vladimír	k1gMnSc1
Koranda	Koranda	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Nanebevstoupení	nanebevstoupení	k1gNnSc1
Páně	páně	k2eAgNnSc2d1
</s>
<s>
Budislav	Budislav	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Hamberger	Hamberger	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Berkovský	Berkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Dírná	Dírnat	k5eAaPmIp3nS
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Edward	Edward	k1gMnSc1
Emil	Emil	k1gMnSc1
Maka	mako	k1gNnSc2
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Berkovský	Berkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc4
</s>
<s>
Drahov	Drahov	k1gInSc1
</s>
<s>
P.	P.	kA
JCLic	JCLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mag	Maga	k1gFnPc2
<g/>
.	.	kIx.
theol	theola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanislav	Stanislav	k1gMnSc1
Brožka	brožka	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Dráchov	Dráchov	k1gInSc1
</s>
<s>
P.	P.	kA
JCLic	JCLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mag	Maga	k1gFnPc2
<g/>
.	.	kIx.
theol	theola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanislav	Stanislav	k1gMnSc1
Brožka	brožka	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Dražice	Dražice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jiří	Jiří	k1gMnSc1
Můčka	Můčka	k1gMnSc1
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Karl	Karl	k1gMnSc1
Zaiser	Zaiser	k1gMnSc1
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
Hamr	hamr	k1gInSc1
</s>
<s>
P.	P.	kA
JCLic	JCLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mag	Maga	k1gFnPc2
<g/>
.	.	kIx.
theol	theola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanislav	Stanislav	k1gMnSc1
Brožka	brožka	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Hartvíkov	Hartvíkov	k1gInSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Karl	Karl	k1gMnSc1
Zaiser	Zaiser	k1gMnSc1
OMI	OMI	kA
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
in	in	k?
spiritualibus	spiritualibus	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Hlasivo	Hlasivo	k1gNnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgInSc1d1
duchovní	duchovní	k2eAgInSc1d1
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Hlavatce	hlavatec	k1gInPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Hajda	Hajda	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Ondřeje	Ondřej	k1gMnSc4
</s>
<s>
Hodušín	Hodušín	k1gMnSc1
</s>
<s>
D.	D.	kA
Jan	Jan	k1gMnSc1
Nep	Nep	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jindřich	Jindřich	k1gMnSc1
Löffelmann	Löffelmann	k1gMnSc1
<g/>
,	,	kIx,
O.	O.	kA
<g/>
Praem	Praem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Hoštice	Hoštice	k1gFnSc1
u	u	k7c2
Tábora	Tábor	k1gInSc2
</s>
<s>
P.	P.	kA
Vladimír	Vladimír	k1gMnSc1
Koranda	Koranda	k1gFnSc1
<g/>
,	,	kIx,
<g/>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Neposkvrněného	poskvrněný	k2eNgNnSc2d1
početí	početí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Hroby	hrob	k1gInPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Karl	Karl	k1gMnSc1
Zaiser	Zaiser	k1gMnSc1
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
in	in	k?
spiritualibus	spiritualibus	k1gMnSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Chotoviny	Chotovina	k1gFnPc1
</s>
<s>
P.	P.	kA
Vladimír	Vladimír	k1gMnSc1
Koranda	Koranda	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Choustník	Choustník	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Hamberger	Hamberger	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Berkovský	Berkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Chýnov	Chýnov	k1gInSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Karl	Karl	k1gMnSc1
Zaiser	Zaiser	k1gMnSc1
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
in	in	k?
spiritualibus	spiritualibus	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Václav	Václav	k1gMnSc1
Mikula	Mikula	k1gMnSc1
<g/>
,	,	kIx,
jáhen	jáhen	k1gMnSc1
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Janov	Janov	k1gInSc1
u	u	k7c2
Soběslavi	Soběslav	k1gFnSc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Hamberger	Hamberger	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Berkovský	Berkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
Jistebnice	Jistebnice	k1gFnSc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mariusz	Mariusz	k1gInSc1
Piwowarczyk	Piwowarczyk	k1gInSc4
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Michaela	Michaela	k1gFnSc1
</s>
<s>
Malšice	Malšice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Pulec	pulec	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomasz	Tomasz	k1gMnSc1
Krzysztof	Krzysztof	k1gMnSc1
Piechnik	Piechnik	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Miličín	Miličín	k1gMnSc1
</s>
<s>
P.	P.	kA
Vladimír	Vladimír	k1gMnSc1
Koranda	Koranda	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1
Vožice	Vožice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
P.	P.	kA
Jiří	Jiří	k1gMnSc1
Cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgInSc1d1
duchovní	duchovní	k2eAgInSc1d1
</s>
<s>
sv.	sv.	kA
Martina	Martina	k1gFnSc1
</s>
<s>
Nedvědice	Nedvědice	k1gFnSc1
u	u	k7c2
Soběslavi	Soběslav	k1gFnSc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Hamberger	Hamberger	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Berkovský	Berkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc4
</s>
<s>
Neustupov	Neustupov	k1gInSc1
</s>
<s>
P.	P.	kA
Vladimír	Vladimír	k1gMnSc1
Koranda	Koranda	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
u	u	k7c2
Mladé	mladý	k2eAgFnSc2d1
Vožice	Vožice	k1gFnSc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Jiří	Jiří	k1gMnSc1
Cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgInSc1d1
duchovní	duchovní	k2eAgInSc1d1
</s>
<s>
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnPc1
</s>
<s>
Opařany	Opařan	k1gMnPc4
</s>
<s>
P.	P.	kA
Josef	Josef	k1gMnSc1
Charypar	Charypar	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Františka	Františka	k1gFnSc1
Xaverského	xaverský	k2eAgMnSc2d1
</s>
<s>
Planá	Planá	k1gFnSc1
nad	nad	k7c7
Lužnicí	Lužnice	k1gFnSc7
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Pulec	pulec	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomasz	Tomasz	k1gMnSc1
Krzysztof	Krzysztof	k1gMnSc1
Piechnik	Piechnik	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Pohnání	pohnání	k1gNnSc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mariusz	Mariusz	k1gInSc1
Piwowarczyk	Piwowarczyk	k1gInSc4
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
in	in	k?
spiritualibus	spiritualibus	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Prokopa	Prokop	k1gMnSc4
</s>
<s>
Rataje	Rataje	k1gInPc1
u	u	k7c2
Bechyně	Bechyně	k1gFnSc2
</s>
<s>
P.	P.	kA
Josef	Josef	k1gMnSc1
Charypar	Charypar	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Ratibořské	ratibořský	k2eAgFnPc1d1
Hory	hora	k1gFnPc1
</s>
<s>
P.	P.	kA
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mariusz	Mariusz	k1gMnSc1
Piwowarczyk	Piwowarczyk	k1gMnSc1
<g/>
,	,	kIx,
OMI	OMI	kA
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
in	in	k?
spiritualibus	spiritualibus	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc4
</s>
<s>
Sezimovo	Sezimův	k2eAgNnSc1d1
Ústí	ústí	k1gNnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Pulec	pulec	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomasz	Tomasz	k1gMnSc1
Krzysztof	Krzysztof	k1gMnSc1
Piechnik	Piechnik	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Povýšení	povýšení	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
</s>
<s>
Smilovy	Smilův	k2eAgFnPc1d1
Hory	hora	k1gFnPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Jiří	Jiří	k1gMnSc1
Cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgInSc1d1
duchovní	duchovní	k2eAgInSc1d1
</s>
<s>
Rozeslání	rozeslání	k1gNnSc1
apoštolů	apoštol	k1gMnPc2
</s>
<s>
Soběslav	Soběslav	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Hamberger	Hamberger	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Berkovský	Berkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Stádlec	Stádlec	k1gMnSc1
</s>
<s>
P.	P.	kA
Josef	Josef	k1gMnSc1
Charypar	Charypar	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Panny	Panna	k1gFnPc1
Marie	Maria	k1gFnSc2
Bolestné	bolestný	k2eAgFnPc1d1
</s>
<s>
Střezimíř	Střezimíř	k1gFnSc1
</s>
<s>
P.	P.	kA
Vladimír	Vladimír	k1gMnSc1
Koranda	Koranda	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Havla	Havel	k1gMnSc4
</s>
<s>
Sudoměřice	Sudoměřice	k1gInPc1
u	u	k7c2
Bechyně	Bechyně	k1gFnSc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Hajda	Hajda	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Všech	všecek	k3xTgMnPc2
svatých	svatý	k1gMnPc2
</s>
<s>
Šebířov	Šebířov	k1gInSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Jiří	Jiří	k1gMnSc1
Cihlelna	Cihlelna	k1gFnSc1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgInSc1d1
duchovní	duchovní	k2eAgInSc1d1
</s>
<s>
sv.	sv.	kA
Havla	Havel	k1gMnSc4
</s>
<s>
Tábor	Tábor	k1gInSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Pulec	pulec	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomasz	Tomasz	k1gMnSc1
Krzysztof	Krzysztof	k1gMnSc1
Piechnik	Piechnik	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Proměnění	proměnění	k1gNnSc2
Páně	páně	k2eAgNnSc2d1
na	na	k7c6
hoře	hora	k1gFnSc6
Tábor	Tábor	k1gInSc1
</s>
<s>
Tábor-Klokoty	Tábor-Klokota	k1gFnPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jiří	Jiří	k1gMnSc1
Můčka	Můčka	k1gMnSc1
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Karl	Karl	k1gMnSc1
Zaiser	Zaiser	k1gMnSc1
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
P.	P.	kA
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mariusz	Mariusz	k1gInSc1
Piwowarczyk	Piwowarczyk	k1gInSc4
OMI	OMI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Tučapy	Tučapa	k1gFnPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Hamberger	Hamberger	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Berkovský	Berkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc4
Staršího	starší	k1gMnSc4
</s>
<s>
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Lužnicí	Lužnice	k1gFnSc7
</s>
<s>
P.	P.	kA
JCLic	JCLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mag	Maga	k1gFnPc2
<g/>
.	.	kIx.
theol	theola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanislav	Stanislav	k1gMnSc1
Brožka	brožka	k1gFnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
Povýšení	povýšení	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
</s>
<s>
Vrcholtovice	Vrcholtovice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Karas	Karas	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Jiří	Jiří	k1gMnSc1
Cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
výpomocný	výpomocný	k2eAgInSc1d1
duchovní	duchovní	k2eAgInSc1d1
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Zálší	Zálší	k1gNnSc1
</s>
<s>
P.	P.	kA
Václav	Václav	k1gMnSc1
Hes	hes	k1gNnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
<g/>
bcb	bcb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
<g/>
bcb	bcb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Diecéze	diecéze	k1gFnSc1
českobudějovická	českobudějovický	k2eAgFnSc1d1
</s>
<s>
Vikariáty	vikariát	k1gInPc1
českobudějovické	českobudějovický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Odkaz	odkaz	k1gInSc1
na	na	k7c4
vikariát	vikariát	k1gInSc4
na	na	k7c6
stránkách	stránka	k1gFnPc6
Biskupství	biskupství	k1gNnPc2
českobudějovického	českobudějovický	k2eAgNnSc2d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vikariáty	vikariát	k1gInPc1
českobudějovické	českobudějovický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
město	město	k1gNnSc4
•	•	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
venkov	venkov	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
•	•	k?
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
•	•	k?
Pelhřimov	Pelhřimov	k1gInSc1
•	•	k?
Písek	Písek	k1gInSc1
•	•	k?
Prachatice	Prachatice	k1gFnPc4
•	•	k?
Strakonice	Strakonice	k1gFnPc4
•	•	k?
Sušice-Nepomuk	Sušice-Nepomuk	k1gMnSc1
•	•	k?
Tábor	Tábor	k1gInSc1
</s>
<s>
Farnosti	farnost	k1gFnPc1
vikariátu	vikariát	k1gInSc2
Tábor	Tábor	k1gInSc1
</s>
<s>
Bechyně	Bechyně	k1gMnSc1
•	•	k?
Borotín	Borotín	k1gMnSc1
u	u	k7c2
Tábora	Tábor	k1gInSc2
•	•	k?
Budislav	Budislav	k1gMnSc1
•	•	k?
Dírná	Dírná	k1gFnSc1
•	•	k?
Drahov	Drahov	k1gInSc1
•	•	k?
Dráchov	Dráchov	k1gInSc1
•	•	k?
Dražice	Dražice	k1gFnSc2
•	•	k?
Hamr	hamr	k1gInSc1
•	•	k?
Hartvíkov	Hartvíkov	k1gInSc1
•	•	k?
Hlasivo	Hlasivo	k1gNnSc1
•	•	k?
Hlavatce	hlavatec	k1gInSc2
•	•	k?
Hodušín	Hodušín	k1gMnSc1
•	•	k?
Hoštice	Hoštice	k1gFnSc1
•	•	k?
Hroby	hrob	k1gInPc1
•	•	k?
Chotoviny	Chotovina	k1gFnSc2
•	•	k?
Choustník	Choustník	k1gMnSc1
•	•	k?
Chýnov	Chýnov	k1gInSc1
•	•	k?
Janov	Janov	k1gInSc1
u	u	k7c2
Soběslavi	Soběslav	k1gFnSc2
•	•	k?
Jistebnice	Jistebnice	k1gFnSc2
•	•	k?
Malšice	Malšice	k1gFnSc2
•	•	k?
Miličín	Miličín	k1gMnSc1
•	•	k?
Mladá	mladá	k1gFnSc1
Vožice	Vožice	k1gFnSc2
•	•	k?
Nedvědice	Nedvědice	k1gFnSc2
u	u	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
Soběslavi	Soběslav	k1gFnSc2
•	•	k?
Neustupov	Neustupov	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Opařany	Opařan	k1gMnPc4
•	•	k?
Planá	Planá	k1gFnSc1
nad	nad	k7c7
Lužnicí	Lužnice	k1gFnSc7
•	•	k?
Pohnání	pohnání	k1gNnSc6
•	•	k?
Rataje	Rataj	k1gMnSc2
u	u	k7c2
Bechyně	Bechyně	k1gFnSc2
•	•	k?
Ratibořské	ratibořský	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
•	•	k?
Sezimovo	Sezimův	k2eAgNnSc1d1
Ústí	ústí	k1gNnSc1
•	•	k?
Smilovy	Smilův	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
•	•	k?
Soběslav	Soběslav	k1gMnSc1
•	•	k?
Stádlec	Stádlec	k1gMnSc1
•	•	k?
Střezimíř	Střezimíř	k1gMnSc1
•	•	k?
Sudoměřice	Sudoměřice	k1gInPc1
u	u	k7c2
Bechyně	Bechyně	k1gFnSc2
•	•	k?
Šebířov	Šebířov	k1gInSc1
•	•	k?
Tábor	Tábor	k1gInSc1
•	•	k?
Tábor-Klokoty	Tábor-Klokota	k1gFnSc2
•	•	k?
Tučapy	Tučapa	k1gFnSc2
•	•	k?
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Lužnicí	Lužnice	k1gFnSc7
•	•	k?
Vrcholtovice	Vrcholtovice	k1gFnSc1
•	•	k?
Zálší	Zálší	k1gNnSc1
</s>
