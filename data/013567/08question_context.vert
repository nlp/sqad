<s>
Cín	cín	k1gInSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Sn	Sn	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Stannum	Stannum	k1gInSc1
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c7
kovy	kov	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
lidstvu	lidstvo	k1gNnSc3
již	již	k9
od	od	k7c2
pravěku	pravěk	k1gInSc2
především	především	k6eAd1
jako	jako	k9
součást	součást	k1gFnSc4
slitiny	slitina	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
bronz	bronz	k1gInSc4
<g/>
.	.	kIx.
</s>