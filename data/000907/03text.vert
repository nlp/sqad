<s>
Hrad	hrad	k1gInSc1	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
,	,	kIx,	,
v	v	k7c6	v
hantecu	hantecum	k1gNnSc6	hantecum
Špilas	Špilasa	k1gFnPc2	Špilasa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc4d1	starý
hrad	hrad	k1gInSc4	hrad
tvořící	tvořící	k2eAgFnSc4d1	tvořící
dominantu	dominanta	k1gFnSc4	dominanta
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Kopec	kopec	k1gInSc1	kopec
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
moravským	moravský	k2eAgMnSc7d1	moravský
markrabětem	markrabě	k1gMnSc7	markrabě
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
)	)	kIx)	)
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
procházel	procházet	k5eAaImAgMnS	procházet
mnoha	mnoho	k4c2	mnoho
výraznými	výrazný	k2eAgFnPc7d1	výrazná
proměnami	proměna	k1gFnPc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
předního	přední	k2eAgInSc2d1	přední
královského	královský	k2eAgInSc2d1	královský
hradu	hrad	k1gInSc2	hrad
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
vystavěného	vystavěný	k2eAgNnSc2d1	vystavěné
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
se	se	k3xPyFc4	se
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stala	stát	k5eAaPmAgFnS	stát
mohutná	mohutný	k2eAgFnSc1d1	mohutná
barokní	barokní	k2eAgFnSc1d1	barokní
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
obléhána	obléhán	k2eAgFnSc1d1	obléhána
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
obávaná	obávaný	k2eAgFnSc1d1	obávaná
věznice	věznice	k1gFnSc1	věznice
-	-	kIx~	-
kasematy	kasemata	k1gFnPc1	kasemata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc4	hrad
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
parkem	park	k1gInSc7	park
okolo	okolo	k6eAd1	okolo
<g/>
)	)	kIx)	)
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
objektům	objekt	k1gInPc3	objekt
Muzea	muzeum	k1gNnSc2	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
vrchu	vrch	k1gInSc6	vrch
nad	nad	k7c7	nad
historickým	historický	k2eAgInSc7d1	historický
centrem	centr	k1gInSc7	centr
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
zřizovatel	zřizovatel	k1gMnSc1	zřizovatel
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
jej	on	k3xPp3gNnSc4	on
koncipoval	koncipovat	k5eAaBmAgMnS	koncipovat
jako	jako	k8xC	jako
oporu	opora	k1gFnSc4	opora
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
důstojné	důstojný	k2eAgNnSc1d1	důstojné
sídlo	sídlo	k1gNnSc1	sídlo
vládců	vládce	k1gMnPc2	vládce
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
písemné	písemný	k2eAgInPc4d1	písemný
záznamy	záznam	k1gInPc4	záznam
o	o	k7c6	o
hradu	hrad	k1gInSc6	hrad
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1277	[number]	k4	1277
<g/>
-	-	kIx~	-
<g/>
1279	[number]	k4	1279
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
získal	získat	k5eAaPmAgInS	získat
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
pojmenování	pojmenování	k1gNnSc2	pojmenování
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
Špilberk	Špilberk	k1gInSc1	Špilberk
původně	původně	k6eAd1	původně
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
jako	jako	k8xC	jako
přední	přední	k2eAgInSc1d1	přední
královský	královský	k2eAgInSc1d1	královský
hrad	hrad	k1gInSc1	hrad
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
pevná	pevný	k2eAgFnSc1d1	pevná
opora	opora	k1gFnSc1	opora
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
panovníci	panovník	k1gMnPc1	panovník
jej	on	k3xPp3gMnSc4	on
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
spíše	spíše	k9	spíše
jen	jen	k9	jen
příležitostně	příležitostně	k6eAd1	příležitostně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platilo	platit	k5eAaImAgNnS	platit
i	i	k9	i
o	o	k7c4	o
Karlu	Karla	k1gFnSc4	Karla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1337	[number]	k4	1337
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
pobývala	pobývat	k5eAaImAgFnS	pobývat
Karlova	Karlův	k2eAgFnSc1d1	Karlova
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
moravská	moravský	k2eAgFnSc1d1	Moravská
markraběnka	markraběnka	k1gFnSc1	markraběnka
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
královna	královna	k1gFnSc1	královna
<g/>
)	)	kIx)	)
Blanka	Blanka	k1gFnSc1	Blanka
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
.	.	kIx.	.
</s>
<s>
Trvalým	trvalý	k2eAgNnSc7d1	trvalé
sídlem	sídlo	k1gNnSc7	sídlo
moravských	moravský	k2eAgNnPc2d1	Moravské
markrabat	markrabě	k1gNnPc2	markrabě
se	se	k3xPyFc4	se
Špilberk	Špilberk	k1gInSc1	Špilberk
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
markraběte	markrabě	k1gMnSc2	markrabě
Jana	Jan	k1gMnSc2	Jan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1350	[number]	k4	1350
<g/>
-	-	kIx~	-
<g/>
1375	[number]	k4	1375
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
byl	být	k5eAaImAgMnS	být
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
zároveň	zároveň	k6eAd1	zároveň
posledním	poslední	k2eAgMnSc7d1	poslední
vládcem	vládce	k1gMnSc7	vládce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
sídlil	sídlit	k5eAaImAgInS	sídlit
byl	být	k5eAaImAgInS	být
Jošt	Jošt	k1gInSc1	Jošt
Moravský	moravský	k2eAgInSc1d1	moravský
<g/>
,	,	kIx,	,
Jošt	Jošt	k1gMnSc1	Jošt
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
Jana	Jan	k1gMnSc2	Jan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k9	jako
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1375	[number]	k4	1375
<g/>
-	-	kIx~	-
<g/>
1411	[number]	k4	1411
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
byl	být	k5eAaImAgMnS	být
Jošt	Jošt	k1gMnSc1	Jošt
zvolen	zvolit	k5eAaPmNgMnS	zvolit
římsko-německým	římskoěmecký	k2eAgMnSc7d1	římsko-německý
králem	král	k1gMnSc7	král
a	a	k8xC	a
Špilberk	Špilberk	k1gInSc4	Špilberk
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
stal	stát	k5eAaPmAgMnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
vládce	vládce	k1gMnSc2	vládce
celé	celá	k1gFnSc2	celá
Římsko-německé	římskoěmecký	k2eAgFnSc2d1	římsko-německý
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgMnSc1	tento
mimořádně	mimořádně	k6eAd1	mimořádně
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
středověký	středověký	k2eAgMnSc1d1	středověký
politik	politik	k1gMnSc1	politik
pochován	pochován	k2eAgMnSc1d1	pochován
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Jošt	Jošt	k1gInSc1	Jošt
vládl	vládnout	k5eAaImAgInS	vládnout
také	také	k9	také
v	v	k7c6	v
Braniborsku	Braniborsko	k1gNnSc6	Braniborsko
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
braniborského	braniborský	k2eAgMnSc2d1	braniborský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
a	a	k8xC	a
v	v	k7c6	v
Lucemburském	lucemburský	k2eAgNnSc6d1	lucemburské
vévodství	vévodství	k1gNnSc6	vévodství
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
vévody	vévoda	k1gMnSc2	vévoda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
autonomní	autonomní	k2eAgFnSc2d1	autonomní
vlády	vláda	k1gFnSc2	vláda
moravské	moravský	k2eAgFnSc2d1	Moravská
linie	linie	k1gFnSc2	linie
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
trvalo	trvat	k5eAaImAgNnS	trvat
šest	šest	k4xCc1	šest
desetiletí	desetiletí	k1gNnPc2	desetiletí
(	(	kIx(	(
<g/>
1350	[number]	k4	1350
<g/>
-	-	kIx~	-
<g/>
1411	[number]	k4	1411
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
kapitolu	kapitola	k1gFnSc4	kapitola
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
brněnského	brněnský	k2eAgInSc2d1	brněnský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jošt	Jošt	k1gInSc1	Jošt
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
posledním	poslední	k2eAgMnSc7d1	poslední
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
linie	linie	k1gFnSc2	linie
moravských	moravský	k2eAgNnPc2d1	Moravské
markrabat	markrabě	k1gNnPc2	markrabě
a	a	k8xC	a
hrad	hrad	k1gInSc4	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
tak	tak	k8xS	tak
natrvalo	natrvalo	k6eAd1	natrvalo
ztratil	ztratit	k5eAaPmAgMnS	ztratit
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
rezidenčního	rezidenční	k2eAgInSc2d1	rezidenční
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Zikmunda	Zikmund	k1gMnSc2	Zikmund
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
zetě	zeť	k1gMnSc2	zeť
Albrechta	Albrecht	k1gMnSc2	Albrecht
Rakouského	rakouský	k2eAgMnSc2d1	rakouský
se	se	k3xPyFc4	se
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
dostal	dostat	k5eAaPmAgMnS	dostat
význam	význam	k1gInSc4	význam
Špilberku	Špilberk	k1gInSc2	Špilberk
jako	jako	k8xS	jako
vojenské	vojenský	k2eAgFnSc3d1	vojenská
pevnosti	pevnost	k1gFnSc3	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Špilberku	Špilberk	k1gInSc6	Špilberk
sídlil	sídlit	k5eAaImAgMnS	sídlit
Viktorín	Viktorín	k1gMnSc1	Viktorín
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
zemského	zemský	k2eAgMnSc2d1	zemský
hejtmana	hejtman	k1gMnSc2	hejtman
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uváděný	uváděný	k2eAgInSc1d1	uváděný
i	i	k9	i
jako	jako	k9	jako
"	"	kIx"	"
<g/>
hejtman	hejtman	k1gMnSc1	hejtman
na	na	k7c6	na
Špilberce	Špilberka	k1gFnSc6	Špilberka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1469	[number]	k4	1469
však	však	k8xC	však
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Špilberk	Špilberk	k1gInSc4	Špilberk
a	a	k8xC	a
následně	následně	k6eAd1	následně
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
vládu	vláda	k1gFnSc4	vláda
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Dodejme	dodat	k5eAaPmRp1nP	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
předtím	předtím	k6eAd1	předtím
obléhán	obléhat	k5eAaImNgInS	obléhat
městem	město	k1gNnSc7	město
Brnem	Brno	k1gNnSc7	Brno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
postavilo	postavit	k5eAaPmAgNnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nastával	nastávat	k5eAaImAgMnS	nastávat
celkový	celkový	k2eAgInSc4d1	celkový
úpadek	úpadek	k1gInSc4	úpadek
a	a	k8xC	a
postupné	postupný	k2eAgNnSc4d1	postupné
chátrání	chátrání	k1gNnSc4	chátrání
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
opětovně	opětovně	k6eAd1	opětovně
zastavován	zastavován	k2eAgInSc1d1	zastavován
a	a	k8xC	a
dočasní	dočasný	k2eAgMnPc1d1	dočasný
držitelé	držitel	k1gMnPc1	držitel
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
údržbu	údržba	k1gFnSc4	údržba
příliš	příliš	k6eAd1	příliš
nedbali	nedbat	k5eAaImAgMnP	nedbat
<g/>
.	.	kIx.	.
</s>
<s>
Moravští	moravský	k2eAgMnPc1d1	moravský
stavové	stavový	k2eAgFnPc1d1	stavová
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
Špilberk	Špilberk	k1gInSc4	Špilberk
i	i	k9	i
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
panstvím	panství	k1gNnSc7	panství
koupili	koupit	k5eAaPmAgMnP	koupit
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
vlastní	vlastní	k2eAgInSc4d1	vlastní
hrad	hrad	k1gInSc4	hrad
prodali	prodat	k5eAaPmAgMnP	prodat
městu	město	k1gNnSc3	město
Brnu	Brno	k1gNnSc3	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
města	město	k1gNnSc2	město
zůstal	zůstat	k5eAaPmAgInS	zůstat
Špilberk	Špilberk	k1gInSc1	Špilberk
pouhých	pouhý	k2eAgNnPc2d1	pouhé
60	[number]	k4	60
let	léto	k1gNnPc2	léto
-	-	kIx~	-
po	po	k7c6	po
bělohorské	bělohorský	k2eAgFnSc6d1	Bělohorská
porážce	porážka	k1gFnSc6	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
byl	být	k5eAaImAgInS	být
císařem	císař	k1gMnSc7	císař
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
městu	město	k1gNnSc3	město
zkonfiskován	zkonfiskován	k2eAgInSc4d1	zkonfiskován
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíše	nejspíše	k9	nejspíše
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
později	pozdě	k6eAd2	pozdě
hrad	hrad	k1gInSc4	hrad
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
okolím	okolí	k1gNnSc7	okolí
tvořil	tvořit	k5eAaImAgInS	tvořit
samostatné	samostatný	k2eAgNnSc4d1	samostatné
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Špilberk	Špilberk	k1gInSc4	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
okamžik	okamžik	k1gInSc1	okamžik
hradu	hrad	k1gInSc2	hrad
nastal	nastat	k5eAaPmAgInS	nastat
během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
částečnou	částečný	k2eAgFnSc7d1	částečná
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
okupací	okupace	k1gFnSc7	okupace
Moravy	Morava	k1gFnSc2	Morava
švédským	švédský	k2eAgNnSc7d1	švédské
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
s	s	k7c7	s
dvojím	dvojí	k4xRgInSc7	dvojí
přímým	přímý	k2eAgInSc7d1	přímý
ohrožením	ohrožení	k1gNnSc7	ohrožení
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1643	[number]	k4	1643
<g/>
-	-	kIx~	-
<g/>
1645	[number]	k4	1645
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc1	opevnění
hradu	hrad	k1gInSc2	hrad
i	i	k8xC	i
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
opravovalo	opravovat	k5eAaImAgNnS	opravovat
a	a	k8xC	a
zdokonalovalo	zdokonalovat	k5eAaImAgNnS	zdokonalovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
pak	pak	k6eAd1	pak
Brno	Brno	k1gNnSc1	Brno
se	s	k7c7	s
Špilberkem	Špilberek	k1gInSc7	Špilberek
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
plukovníka	plukovník	k1gMnSc2	plukovník
Raduita	Raduit	k1gMnSc2	Raduit
de	de	k?	de
Souches	Souchesa	k1gFnPc2	Souchesa
odolalo	odolat	k5eAaPmAgNnS	odolat
tříměsíčnímu	tříměsíční	k2eAgMnSc3d1	tříměsíční
dobývání	dobývání	k1gNnSc4	dobývání
mnohonásobnou	mnohonásobný	k2eAgFnSc7d1	mnohonásobná
švédskou	švédský	k2eAgFnSc7d1	švédská
přesilou	přesila	k1gFnSc7	přesila
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
-	-	kIx~	-
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
přebudován	přebudován	k2eAgInSc1d1	přebudován
na	na	k7c4	na
nejmohutnější	mohutný	k2eAgFnSc4d3	nejmohutnější
a	a	k8xC	a
také	také	k9	také
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
barokní	barokní	k2eAgFnSc4d1	barokní
pevnost	pevnost	k1gFnSc4	pevnost
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc7d1	tvořící
jako	jako	k8xS	jako
citadela	citadela	k1gFnSc1	citadela
s	s	k7c7	s
městem	město	k1gNnSc7	město
jednu	jeden	k4xCgFnSc4	jeden
pevnostní	pevnostní	k2eAgFnSc4d1	pevnostní
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1742	[number]	k4	1742
tak	tak	k6eAd1	tak
odolal	odolat	k5eAaPmAgInS	odolat
i	i	k8xC	i
vojskům	vojsko	k1gNnPc3	vojsko
pruského	pruský	k2eAgInSc2d1	pruský
krále	král	k1gMnSc4	král
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
pevnosti	pevnost	k1gFnSc2	pevnost
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
vězení	vězení	k1gNnSc1	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
Špilberku	Špilberka	k1gFnSc4	Špilberka
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
vězněni	věznit	k5eAaImNgMnP	věznit
jeho	jeho	k3xOp3gMnSc3	jeho
přední	přední	k2eAgMnPc1d1	přední
moravští	moravský	k2eAgMnPc1d1	moravský
účastníci	účastník	k1gMnPc1	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
vězněno	věznit	k5eAaImNgNnS	věznit
také	také	k9	také
několik	několik	k4yIc1	několik
vojenských	vojenský	k2eAgFnPc2d1	vojenská
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rakouští	rakouský	k2eAgMnPc1d1	rakouský
generálové	generál	k1gMnPc1	generál
Bonneval	Bonneval	k1gMnSc1	Bonneval
a	a	k8xC	a
Wallis	Wallis	k1gFnSc1	Wallis
či	či	k8xC	či
proslulý	proslulý	k2eAgMnSc1d1	proslulý
plukovník	plukovník	k1gMnSc1	plukovník
pandurů	pandur	k1gMnPc2	pandur
Franz	Franz	k1gMnSc1	Franz
Trenck	Trenck	k1gMnSc1	Trenck
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
Špilberku	Špilberk	k1gInSc6	Špilberk
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1749	[number]	k4	1749
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
o	o	k7c6	o
přeměně	přeměna	k1gFnSc6	přeměna
na	na	k7c4	na
civilní	civilní	k2eAgFnSc4d1	civilní
věznici	věznice	k1gFnSc4	věznice
pro	pro	k7c4	pro
nejtěžší	těžký	k2eAgMnPc4d3	nejtěžší
zločince	zločinec	k1gMnPc4	zločinec
(	(	kIx(	(
<g/>
lehčí	lehký	k2eAgNnPc4d2	lehčí
provinění	provinění	k1gNnPc4	provinění
si	se	k3xPyFc3	se
vězni	vězeň	k1gMnPc1	vězeň
odpykávali	odpykávat	k5eAaImAgMnP	odpykávat
v	v	k7c6	v
káznici	káznice	k1gFnSc6	káznice
na	na	k7c6	na
Cejlu	Cejl	k1gInSc6	Cejl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
starší	starý	k2eAgFnSc2d2	starší
vězeňské	vězeňský	k2eAgFnSc2d1	vězeňská
budovy	budova	k1gFnSc2	budova
bylo	být	k5eAaImAgNnS	být
přebudováno	přebudován	k2eAgNnSc1d1	přebudováno
horní	horní	k2eAgNnSc1d1	horní
patro	patro	k1gNnSc1	patro
severních	severní	k2eAgFnPc2d1	severní
kasemat	kasemata	k1gFnPc2	kasemata
(	(	kIx(	(
<g/>
Josefínský	josefínský	k2eAgInSc1d1	josefínský
trakt	trakt	k1gInSc1	trakt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
sem	sem	k6eAd1	sem
byli	být	k5eAaImAgMnP	být
přesunuti	přesunout	k5eAaPmNgMnP	přesunout
první	první	k4xOgMnPc1	první
vězni	vězeň	k1gMnPc1	vězeň
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1784	[number]	k4	1784
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1784	[number]	k4	1784
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
další	další	k2eAgInSc1d1	další
císařův	císařův	k2eAgInSc1d1	císařův
příkaz	příkaz	k1gInSc1	příkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
do	do	k7c2	do
nejhorších	zlý	k2eAgFnPc2d3	nejhorší
kasemat	kasemata	k1gFnPc2	kasemata
v	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
patře	patro	k1gNnSc6	patro
byli	být	k5eAaImAgMnP	být
vsazeni	vsazen	k2eAgMnPc1d1	vsazen
zločinci	zločinec	k1gMnPc1	zločinec
odsouzení	odsouzený	k2eAgMnPc1d1	odsouzený
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
29	[number]	k4	29
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kobek	kobka	k1gFnPc2	kobka
<g/>
,	,	kIx,	,
sbitých	sbitý	k2eAgFnPc2d1	sbitá
ze	z	k7c2	z
silných	silný	k2eAgInPc2d1	silný
trámů	trám	k1gInPc2	trám
i	i	k8xC	i
prken	prkno	k1gNnPc2	prkno
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
byli	být	k5eAaImAgMnP	být
vězňové	vězeň	k1gMnPc1	vězeň
trvale	trvale	k6eAd1	trvale
přikováni	přikován	k2eAgMnPc1d1	přikován
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
vězení	vězení	k1gNnSc4	vězení
upraveno	upravit	k5eAaPmNgNnS	upravit
i	i	k8xC	i
horní	horní	k2eAgNnSc1d1	horní
patro	patro	k1gNnSc1	patro
jižních	jižní	k2eAgFnPc2d1	jižní
kasemat	kasemata	k1gFnPc2	kasemata
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
využívat	využívat	k5eAaPmF	využívat
až	až	k9	až
Josefův	Josefův	k2eAgMnSc1d1	Josefův
nástupce	nástupce	k1gMnSc1	nástupce
Leopold	Leopold	k1gMnSc1	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Leopoldův	Leopoldův	k2eAgInSc1d1	Leopoldův
trakt	trakt	k1gInSc1	trakt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
však	však	k9	však
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1790	[number]	k4	1790
zrušil	zrušit	k5eAaPmAgInS	zrušit
celé	celý	k2eAgNnSc4d1	celé
vězení	vězení	k1gNnSc4	vězení
doživotně	doživotně	k6eAd1	doživotně
odsouzených	odsouzený	k2eAgInPc2d1	odsouzený
v	v	k7c6	v
dolních	dolní	k2eAgNnPc6d1	dolní
kasematech	kasemata	k1gNnPc6	kasemata
včetně	včetně	k7c2	včetně
trestu	trest	k1gInSc2	trest
přikování	přikování	k1gNnSc2	přikování
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
i	i	k9	i
další	další	k2eAgNnSc4d1	další
zmírnění	zmírnění	k1gNnSc4	zmírnění
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
odsouzence	odsouzenec	k1gMnPc4	odsouzenec
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgNnSc1d1	horní
patro	patro	k1gNnSc1	patro
kasemat	kasemata	k1gNnPc2	kasemata
nadále	nadále	k6eAd1	nadále
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
vězení	vězení	k1gNnSc1	vězení
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
nadzemních	nadzemní	k2eAgFnPc6d1	nadzemní
prostorách	prostora	k1gFnPc6	prostora
špilberské	špilberský	k2eAgFnSc2d1	špilberská
pevnosti	pevnost	k1gFnSc2	pevnost
začínají	začínat	k5eAaImIp3nP	začínat
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
"	"	kIx"	"
<g/>
političtí	politický	k2eAgMnPc1d1	politický
<g/>
"	"	kIx"	"
vězni	vězeň	k1gMnPc1	vězeň
<g/>
,	,	kIx,	,
např.	např.	kA	např.
francouzský	francouzský	k2eAgMnSc1d1	francouzský
revolucionář	revolucionář	k1gMnSc1	revolucionář
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Drouet	Drouet	k1gMnSc1	Drouet
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
jakobín	jakobín	k1gMnSc1	jakobín
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ferenc	Ferenc	k1gMnSc1	Ferenc
Kazinczy	Kazincza	k1gFnSc2	Kazincza
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
italský	italský	k2eAgMnSc1d1	italský
vlastenec	vlastenec	k1gMnSc1	vlastenec
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Silvio	Silvio	k1gMnSc1	Silvio
Pellico	Pellico	k1gMnSc1	Pellico
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
knihou	kniha	k1gFnSc7	kniha
"	"	kIx"	"
<g/>
Mé	můj	k3xOp1gInPc1	můj
žaláře	žalář	k1gInPc1	žalář
<g/>
"	"	kIx"	"
proslavil	proslavit	k5eAaPmAgMnS	proslavit
špilberské	špilberský	k2eAgNnSc4d1	špilberský
vězení	vězení	k1gNnSc4	vězení
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
armáda	armáda	k1gFnSc1	armáda
císaře	císař	k1gMnSc2	císař
Napoleona	Napoleon	k1gMnSc2	Napoleon
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
okupovaného	okupovaný	k2eAgNnSc2d1	okupované
Brna	Brno	k1gNnSc2	Brno
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
zničila	zničit	k5eAaPmAgFnS	zničit
některé	některý	k3yIgFnPc4	některý
důležité	důležitý	k2eAgFnPc4d1	důležitá
části	část	k1gFnPc4	část
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1841	[number]	k4	1841
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1855	[number]	k4	1855
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Špilberku	Špilberk	k1gInSc6	Špilberk
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vězeň	vězeň	k1gMnSc1	vězeň
č.	č.	k?	č.
1042	[number]	k4	1042
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Václav	Václav	k1gMnSc1	Václav
Babinský	Babinský	k2eAgInSc1d1	Babinský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
pak	pak	k6eAd1	pak
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
špilberskou	špilberský	k2eAgFnSc4d1	špilberská
věznici	věznice	k1gFnSc4	věznice
zrušil	zrušit	k5eAaPmAgMnS	zrušit
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
prostory	prostor	k1gInPc1	prostor
se	se	k3xPyFc4	se
přeměnily	přeměnit	k5eAaPmAgInP	přeměnit
na	na	k7c4	na
vojenská	vojenský	k2eAgNnPc4d1	vojenské
kasárna	kasárna	k1gNnPc4	kasárna
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
dalších	další	k2eAgNnPc6d1	další
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
kromě	kromě	k7c2	kromě
vojáků	voják	k1gMnPc2	voják
byli	být	k5eAaImAgMnP	být
vězněni	vězněn	k2eAgMnPc1d1	vězněn
i	i	k8xC	i
civilní	civilní	k2eAgMnPc1d1	civilní
odpůrci	odpůrce	k1gMnPc1	odpůrce
rakouského	rakouský	k2eAgInSc2d1	rakouský
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
Československa	Československo	k1gNnSc2	Československo
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
vězněno	věznit	k5eAaImNgNnS	věznit
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
českých	český	k2eAgMnPc2d1	český
vlastenců	vlastenec	k1gMnPc2	vlastenec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
několik	několik	k4yIc4	několik
také	také	k6eAd1	také
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
provedla	provést	k5eAaPmAgFnS	provést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
na	na	k7c6	na
Špilberku	Špilberk	k1gInSc6	Špilberk
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
úpravy	úprava	k1gFnSc2	úprava
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
kasárna	kasárna	k1gNnPc4	kasárna
v	v	k7c6	v
romanticko-historizujícím	romantickoistorizující	k2eAgMnSc6d1	romanticko-historizující
duchu	duch	k1gMnSc6	duch
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
velkoněmecké	velkoněmecký	k2eAgFnPc4d1	Velkoněmecká
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
opustila	opustit	k5eAaPmAgFnS	opustit
Špilberk	Špilberk	k1gInSc4	Špilberk
posádka	posádka	k1gFnSc1	posádka
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
tak	tak	k6eAd1	tak
skončila	skončit	k5eAaPmAgFnS	skončit
jeho	jeho	k3xOp3gFnSc1	jeho
vojenská	vojenský	k2eAgFnSc1d1	vojenská
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
hrad	hrad	k1gInSc1	hrad
stal	stát	k5eAaPmAgInS	stát
sídlem	sídlo	k1gNnSc7	sídlo
Muzea	muzeum	k1gNnSc2	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
turistickým	turistický	k2eAgNnSc7d1	turistické
lákadlem	lákadlo	k1gNnSc7	lákadlo
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konají	konat	k5eAaImIp3nP	konat
různé	různý	k2eAgFnPc4d1	různá
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInPc4d1	hudební
a	a	k8xC	a
divadelní	divadelní	k2eAgInPc4d1	divadelní
festivaly	festival	k1gInPc4	festival
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
odtud	odtud	k6eAd1	odtud
odpalovány	odpalován	k2eAgInPc1d1	odpalován
ohňostroje	ohňostroj	k1gInPc1	ohňostroj
Ignis	Ignis	k1gFnSc2	Ignis
Brunensis	Brunensis	k1gFnSc2	Brunensis
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
proměnami	proměna	k1gFnPc7	proměna
funkcí	funkce	k1gFnPc2	funkce
Špilberku	Špilberk	k1gInSc2	Špilberk
souvisel	souviset	k5eAaImAgInS	souviset
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
stavební	stavební	k2eAgInSc1d1	stavební
vývoj	vývoj	k1gInSc1	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
gotického	gotický	k2eAgInSc2d1	gotický
hradu	hrad	k1gInSc2	hrad
13	[number]	k4	13
<g/>
.	.	kIx.	.
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
východním	východní	k2eAgNnSc6d1	východní
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
"	"	kIx"	"
<g/>
gotickou	gotický	k2eAgFnSc4d1	gotická
<g/>
"	"	kIx"	"
podobu	podoba	k1gFnSc4	podoba
celého	celý	k2eAgNnSc2d1	celé
východního	východní	k2eAgNnSc2d1	východní
křídla	křídlo	k1gNnSc2	křídlo
však	však	k9	však
určila	určit	k5eAaPmAgFnS	určit
diskutabilní	diskutabilní	k2eAgFnSc1d1	diskutabilní
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Chudárka	Chudárek	k1gMnSc2	Chudárek
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Rochepin	Rochepin	k1gMnSc1	Rochepin
Barokní	barokní	k2eAgFnSc4d1	barokní
pevnostní	pevnostní	k2eAgFnSc4d1	pevnostní
přestavbu	přestavba	k1gFnSc4	přestavba
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
předních	přední	k2eAgMnPc2d1	přední
vojenských	vojenský	k2eAgMnPc2d1	vojenský
inženýrů	inženýr	k1gMnPc2	inženýr
N.	N.	kA	N.
Peroniho	Peroni	k1gMnSc2	Peroni
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Rocheta	rocheta	k1gFnSc1	rocheta
a	a	k8xC	a
P.	P.	kA	P.
Rochepina	Rochepina	k1gFnSc1	Rochepina
i	i	k8xC	i
brněnského	brněnský	k2eAgMnSc2d1	brněnský
stavitele	stavitel	k1gMnSc2	stavitel
M.	M.	kA	M.
Grimma	Grimm	k1gMnSc2	Grimm
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
připomíná	připomínat	k5eAaImIp3nS	připomínat
především	především	k6eAd1	především
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
fortifikační	fortifikační	k2eAgInSc1d1	fortifikační
systém	systém	k1gInSc1	systém
-	-	kIx~	-
hradby	hradba	k1gFnPc1	hradba
s	s	k7c7	s
bastiony	bastion	k1gInPc7	bastion
<g/>
,	,	kIx,	,
zděné	zděný	k2eAgInPc1d1	zděný
příkopy	příkop	k1gInPc1	příkop
s	s	k7c7	s
vestavěnými	vestavěný	k2eAgInPc7d1	vestavěný
kasematy	kasematy	k1gInPc7	kasematy
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
přízemní	přízemnit	k5eAaPmIp3nP	přízemnit
kasárenské	kasárenský	k2eAgInPc1d1	kasárenský
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
objekty	objekt	k1gInPc1	objekt
přistavěné	přistavěný	k2eAgInPc1d1	přistavěný
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
vnější	vnější	k2eAgFnSc2d1	vnější
zdi	zeď	k1gFnSc2	zeď
středověkého	středověký	k2eAgInSc2d1	středověký
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
pevnostního	pevnostní	k2eAgInSc2d1	pevnostní
systému	systém	k1gInSc2	systém
byly	být	k5eAaImAgInP	být
i	i	k9	i
studna	studna	k1gFnSc1	studna
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
nádvoří	nádvoří	k1gNnSc2	nádvoří
a	a	k8xC	a
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
cisterna	cisterna	k1gFnSc1	cisterna
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dnešních	dnešní	k2eAgFnPc2d1	dnešní
budov	budova	k1gFnPc2	budova
-	-	kIx~	-
jižní	jižní	k2eAgNnSc4d1	jižní
<g/>
,	,	kIx,	,
západní	západní	k2eAgNnSc4d1	západní
a	a	k8xC	a
severní	severní	k2eAgNnSc4d1	severní
křídlo	křídlo	k1gNnSc4	křídlo
i	i	k8xC	i
střední	střední	k2eAgInSc4d1	střední
trakt	trakt	k1gInSc4	trakt
<g/>
,	,	kIx,	,
rozdělující	rozdělující	k2eAgFnSc4d1	rozdělující
dřívější	dřívější	k2eAgNnSc4d1	dřívější
velké	velký	k2eAgNnSc4d1	velké
nádvoří	nádvoří	k1gNnSc4	nádvoří
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
-	-	kIx~	-
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k6eAd1	až
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
přestavbou	přestavba	k1gFnSc7	přestavba
pevnosti	pevnost	k1gFnSc2	pevnost
na	na	k7c6	na
věznici	věznice	k1gFnSc6	věznice
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Úpravy	úprava	k1gFnPc1	úprava
prováděné	prováděný	k2eAgInPc1d1	prováděný
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
hrad	hrad	k1gInSc1	hrad
už	už	k9	už
spíše	spíše	k9	spíše
jen	jen	k9	jen
architektonicky	architektonicky	k6eAd1	architektonicky
sjednotily	sjednotit	k5eAaPmAgFnP	sjednotit
a	a	k8xC	a
projevily	projevit	k5eAaPmAgFnP	projevit
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
v	v	k7c6	v
interiérech	interiér	k1gInPc6	interiér
a	a	k8xC	a
historizujících	historizující	k2eAgInPc6d1	historizující
detailech	detail	k1gInPc6	detail
<g/>
.	.	kIx.	.
</s>
<s>
Špilberk	Špilberk	k1gInSc1	Špilberk
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
každoročně	každoročně	k6eAd1	každoročně
hostí	hostit	k5eAaImIp3nP	hostit
divadelní	divadelní	k2eAgInSc4d1	divadelní
festival	festival	k1gInSc4	festival
Letní	letní	k2eAgFnSc2d1	letní
shakespearovské	shakespearovský	k2eAgFnSc2d1	shakespearovská
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
během	během	k7c2	během
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
srpna	srpen	k1gInSc2	srpen
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádvoří	nádvoří	k1gNnSc6	nádvoří
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádvoří	nádvoří	k1gNnSc6	nádvoří
hradu	hrad	k1gInSc2	hrad
koná	konat	k5eAaImIp3nS	konat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
Filharmonií	filharmonie	k1gFnSc7	filharmonie
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
na	na	k7c4	na
Špilberk	Špilberk	k1gInSc4	Špilberk
VANĚK	Vaněk	k1gMnSc1	Vaněk
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Fotep	Fotep	k1gMnSc1	Fotep
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902921	[number]	k4	902921
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
BARANOVÁ	Baranová	k1gFnSc1	Baranová
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
městského	městský	k2eAgInSc2d1	městský
parku	park	k1gInSc2	park
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
.	.	kIx.	.
,	,	kIx,	,
2014	[number]	k4	2014
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
práce	práce	k1gFnSc1	práce
Ing.	ing.	kA	ing.
Pavlína	Pavlína	k1gFnSc1	Pavlína
Škardová	Škardová	k1gFnSc1	Škardová
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
BAUMANNOVÁ	BAUMANNOVÁ	kA	BAUMANNOVÁ
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
<g/>
.	.	kIx.	.
</s>
<s>
Podoby	podoba	k1gFnPc1	podoba
hradu	hrad	k1gInSc2	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86549	[number]	k4	86549
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
BERGEROVÁ	Bergerová	k1gFnSc1	Bergerová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
keramika	keramika	k1gFnSc1	keramika
z	z	k7c2	z
jižního	jižní	k2eAgInSc2d1	jižní
parkánu	parkán	k1gInSc2	parkán
hradu	hrad	k1gInSc2	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,,	,,	k?	,,
2015	[number]	k4	2015
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Irena	Irena	k1gFnSc1	Irena
Loskotová	Loskotová	k1gFnSc1	Loskotová
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KADLČKOVÁ	KADLČKOVÁ	kA	KADLČKOVÁ
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
.	.	kIx.	.
</s>
<s>
Každodennost	každodennost	k1gFnSc1	každodennost
špilberské	špilberský	k2eAgFnSc2d1	špilberská
věznice	věznice	k1gFnSc2	věznice
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
italských	italský	k2eAgMnPc2d1	italský
karbonářů	karbonář	k1gMnPc2	karbonář
a	a	k8xC	a
polských	polský	k2eAgMnPc2d1	polský
revolucionářů	revolucionář	k1gMnPc2	revolucionář
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Jiří	Jiří	k1gMnSc1	Jiří
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
KREJSA	Krejsa	k1gMnSc1	Krejsa
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Polští	polský	k2eAgMnPc1d1	polský
vězni	vězeň	k1gMnPc1	vězeň
na	na	k7c4	na
Špilberku	Špilberka	k1gFnSc4	Špilberka
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
František	František	k1gMnSc1	František
Čapka	Čapka	k1gMnSc1	Čapka
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
MÁTLOVÁ	MÁTLOVÁ	kA	MÁTLOVÁ
<g/>
,	,	kIx,	,
Simona	Simona	k1gFnSc1	Simona
<g/>
.	.	kIx.	.
</s>
<s>
Špilberk	Špilberk	k1gInSc1	Špilberk
v	v	k7c6	v
odbobí	odbobit	k5eAaPmIp3nP	odbobit
druhé	druhý	k4xOgFnPc1	druhý
světové	světový	k2eAgFnPc1d1	světová
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Vladimír	Vladimír	k1gMnSc1	Vladimír
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
VANĚK	Vaněk	k1gMnSc1	Vaněk
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Kasematy	kasemata	k1gFnPc1	kasemata
Špilberk	Špilberk	k1gInSc1	Špilberk
:	:	kIx,	:
Barokní	barokní	k2eAgFnSc1d1	barokní
pevnostní	pevnostní	k2eAgFnSc1d1	pevnostní
stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
vězení	vězení	k1gNnSc1	vězení
:	:	kIx,	:
Expozice	expozice	k1gFnSc1	expozice
Muzea	muzeum	k1gNnSc2	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
KOLÁČEK	Koláček	k1gMnSc1	Koláček
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Y.	Y.	kA	Y.
<g/>
.	.	kIx.	.
</s>
<s>
Osudy	osud	k1gInPc1	osud
hradu	hrad	k1gInSc2	hrad
Špilberku	Špilberk	k1gInSc2	Špilberk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
319	[number]	k4	319
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Galerie	galerie	k1gFnSc1	galerie
Špilberk	Špilberk	k1gInSc4	Špilberk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Špilberk	Špilberk	k1gInSc4	Špilberk
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
hradu	hrad	k1gInSc2	hrad
Letecký	letecký	k2eAgInSc1d1	letecký
panoramatický	panoramatický	k2eAgInSc1d1	panoramatický
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
Virtuální	virtuální	k2eAgInSc1d1	virtuální
prohlídka	prohlídka	k1gFnSc1	prohlídka
</s>
