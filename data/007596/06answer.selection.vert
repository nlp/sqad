<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	s	k7c7	s
63,7	[number]	k4	63,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Česka	Česko	k1gNnSc2	Česko
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
(	(	kIx(	(
<g/>
86	[number]	k4	86
%	%	kIx~	%
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
národnosti	národnost	k1gFnSc3	národnost
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zcela	zcela	k6eAd1	zcela
převažuje	převažovat	k5eAaImIp3nS	převažovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
okresech	okres	k1gInPc6	okres
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
4,9	[number]	k4	4,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
deklarovalo	deklarovat	k5eAaBmAgNnS	deklarovat
národnost	národnost	k1gFnSc4	národnost
moravskou	moravský	k2eAgFnSc4d1	Moravská
a	a	k8xC	a
0,1	[number]	k4	0,1
%	%	kIx~	%
národnost	národnost	k1gFnSc4	národnost
slezskou	slezský	k2eAgFnSc4d1	Slezská
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
rovněž	rovněž	k9	rovněž
hovořící	hovořící	k2eAgMnPc1d1	hovořící
převážně	převážně	k6eAd1	převážně
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
