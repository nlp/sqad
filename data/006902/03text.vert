<s>
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1906	[number]	k4	1906
Vratislav	Vratislav	k1gMnSc1	Vratislav
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
koncentrační	koncentrační	k2eAgInSc1d1	koncentrační
tábor	tábor	k1gInSc1	tábor
Flossenbürg	Flossenbürg	k1gMnSc1	Flossenbürg
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
evangelický	evangelický	k2eAgMnSc1d1	evangelický
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
etik	etik	k1gMnSc1	etik
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
bojovník	bojovník	k1gMnSc1	bojovník
proti	proti	k7c3	proti
nacismu	nacismus	k1gInSc3	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
v	v	k7c4	v
Tübingen	Tübingen	k1gInSc4	Tübingen
a	a	k8xC	a
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
již	již	k6eAd1	již
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
24	[number]	k4	24
letech	léto	k1gNnPc6	léto
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
habilitace	habilitace	k1gFnSc1	habilitace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
pobytu	pobyt	k1gInSc6	pobyt
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
evangelickou	evangelický	k2eAgFnSc4d1	evangelická
teologii	teologie	k1gFnSc4	teologie
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
referentem	referent	k1gMnSc7	referent
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
přípravné	přípravný	k2eAgFnSc6d1	přípravná
organizaci	organizace	k1gFnSc6	organizace
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ekumenické	ekumenický	k2eAgFnSc2d1	ekumenická
rady	rada	k1gFnSc2	rada
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1933	[number]	k4	1933
zaujímal	zaujímat	k5eAaImAgInS	zaujímat
otevřeně	otevřeně	k6eAd1	otevřeně
odmítavé	odmítavý	k2eAgNnSc4d1	odmítavé
stanovisko	stanovisko	k1gNnSc4	stanovisko
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
pronásledování	pronásledování	k1gNnSc3	pronásledování
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Kirchenkampfu	Kirchenkampf	k1gInSc3	Kirchenkampf
(	(	kIx(	(
<g/>
zápasu	zápas	k1gInSc2	zápas
mezi	mezi	k7c7	mezi
německými	německý	k2eAgMnPc7d1	německý
evangelíky	evangelík	k1gMnPc7	evangelík
v	v	k7c6	v
období	období	k1gNnSc6	období
nacismu	nacismus	k1gInSc2	nacismus
<g/>
)	)	kIx)	)
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
proti	proti	k7c3	proti
hnutí	hnutí	k1gNnSc3	hnutí
Německých	německý	k2eAgMnPc2d1	německý
křesťanů	křesťan	k1gMnPc2	křesťan
(	(	kIx(	(
<g/>
Deutsche	Deutsche	k1gFnSc7	Deutsche
Christen	Christen	k2eAgInSc4d1	Christen
<g/>
)	)	kIx)	)
a	a	k8xC	a
rasovým	rasový	k2eAgInPc3d1	rasový
zákonům	zákon	k1gInPc3	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
vedl	vést	k5eAaImAgInS	vést
kazatelský	kazatelský	k2eAgInSc1d1	kazatelský
seminář	seminář	k1gInSc1	seminář
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
ve	v	k7c6	v
Finkenwalde	Finkenwald	k1gInSc5	Finkenwald
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
ilegálitě	ilegálita	k1gFnSc6	ilegálita
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
odbojové	odbojový	k2eAgFnSc3d1	odbojová
skupině	skupina	k1gFnSc3	skupina
kolem	kolem	k7c2	kolem
Wilhelma	Wilhelmum	k1gNnSc2	Wilhelmum
Franze	Franze	k1gFnSc2	Franze
Canarise	Canarise	k1gFnSc2	Canarise
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
zakázány	zakázat	k5eAaPmNgInP	zakázat
veřejné	veřejný	k2eAgInPc1d1	veřejný
mluvené	mluvený	k2eAgInPc1d1	mluvený
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
psané	psaný	k2eAgNnSc1d1	psané
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1943	[number]	k4	1943
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
výslovný	výslovný	k2eAgInSc4d1	výslovný
rozkaz	rozkaz	k1gInSc4	rozkaz
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
popraven	popraven	k2eAgMnSc1d1	popraven
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgMnPc2d1	poslední
odpůrců	odpůrce	k1gMnPc2	odpůrce
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
spojováni	spojován	k2eAgMnPc1d1	spojován
s	s	k7c7	s
neúspěšným	úspěšný	k2eNgInSc7d1	neúspěšný
atentátem	atentát	k1gInSc7	atentát
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
svébytný	svébytný	k2eAgMnSc1d1	svébytný
teolog	teolog	k1gMnSc1	teolog
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
ve	v	k7c4	v
společenství	společenství	k1gNnPc4	společenství
křesťanů	křesťan	k1gMnPc2	křesťan
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
kázání	kázání	k1gNnSc4	kázání
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
následování	následování	k1gNnSc4	následování
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
soulad	soulad	k1gInSc4	soulad
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yQnSc6	což
šel	jít	k5eAaImAgMnS	jít
sám	sám	k3xTgMnSc1	sám
příkladem	příklad	k1gInSc7	příklad
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
vlivné	vlivný	k2eAgMnPc4d1	vlivný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
kusé	kusý	k2eAgFnPc1d1	kusá
myšlenky	myšlenka	k1gFnPc1	myšlenka
ohledně	ohledně	k7c2	ohledně
budoucího	budoucí	k2eAgNnSc2d1	budoucí
směřování	směřování	k1gNnSc2	směřování
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
solidaritě	solidarita	k1gFnSc3	solidarita
s	s	k7c7	s
potřebnými	potřebný	k2eAgFnPc7d1	potřebná
a	a	k8xC	a
k	k	k7c3	k
nenáboženské	náboženský	k2eNgFnSc3d1	nenáboženská
interpretaci	interpretace	k1gFnSc3	interpretace
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgFnPc4d1	církevní
tradice	tradice	k1gFnPc4	tradice
i	i	k8xC	i
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1906	[number]	k4	1906
ve	v	k7c6	v
slezské	slezský	k2eAgFnSc6d1	Slezská
Vratislavi	Vratislav	k1gFnSc6	Vratislav
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Breslau	Breslaus	k1gInSc3	Breslaus
<g/>
)	)	kIx)	)
jako	jako	k9	jako
šesté	šestý	k4xOgFnSc2	šestý
z	z	k7c2	z
osmi	osm	k4xCc2	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
z	z	k7c2	z
dvojčat	dvojče	k1gNnPc2	dvojče
<g/>
,	,	kIx,	,
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Sabinou	Sabina	k1gFnSc7	Sabina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Karl	Karl	k1gMnSc1	Karl
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
byl	být	k5eAaImAgMnS	být
psychiatr	psychiatr	k1gMnSc1	psychiatr
a	a	k8xC	a
neurolog	neurolog	k1gMnSc1	neurolog
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Paula	Paula	k1gFnSc1	Paula
Bonhoefferová	Bonhoefferová	k1gFnSc1	Bonhoefferová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
von	von	k1gInSc4	von
Hase	hasit	k5eAaImSgInS	hasit
<g/>
,	,	kIx,	,
povoláním	povolání	k1gNnSc7	povolání
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vnučkou	vnučka	k1gFnSc7	vnučka
evangelického	evangelický	k2eAgMnSc2d1	evangelický
teologa	teolog	k1gMnSc2	teolog
Karla	Karel	k1gMnSc2	Karel
von	von	k1gInSc1	von
Hase	hasit	k5eAaImSgInS	hasit
a	a	k8xC	a
malíře	malíř	k1gMnSc2	malíř
Stanislava	Stanislava	k1gFnSc1	Stanislava
von	von	k1gInSc1	von
Kalckreuth	Kalckreuth	k1gInSc1	Kalckreuth
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
bratranci	bratranec	k1gMnPc1	bratranec
Arvid	Arvida	k1gFnPc2	Arvida
Harnack	Harnacko	k1gNnPc2	Harnacko
a	a	k8xC	a
Falk	Falk	k1gMnSc1	Falk
Harnack	Harnack	k1gMnSc1	Harnack
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
činní	činný	k2eAgMnPc1d1	činný
v	v	k7c6	v
protinacistickém	protinacistický	k2eAgInSc6d1	protinacistický
odboji	odboj	k1gInSc6	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
bohaté	bohatý	k2eAgFnSc6d1	bohatá
měšťanské	měšťanský	k2eAgFnSc6d1	měšťanská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
prvních	první	k4xOgNnPc6	první
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
doma	doma	k6eAd1	doma
a	a	k8xC	a
starala	starat	k5eAaImAgFnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
výchovu	výchova	k1gFnSc4	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
ale	ale	k9	ale
náboženským	náboženský	k2eAgFnPc3d1	náboženská
otázkám	otázka	k1gFnPc3	otázka
vyhýbal	vyhýbat	k5eAaImAgMnS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
jen	jen	k6eAd1	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Dietrichův	Dietrichův	k2eAgMnSc1d1	Dietrichův
otec	otec	k1gMnSc1	otec
Karl	Karl	k1gMnSc1	Karl
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
na	na	k7c6	na
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
Univerzitě	univerzita	k1gFnSc6	univerzita
Fridricha	Fridrich	k1gMnSc2	Fridrich
Viléma	Vilém	k1gMnSc2	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
líčení	líčení	k1gNnSc2	líčení
sestry	sestra	k1gFnSc2	sestra
Sabiny	Sabina	k1gFnSc2	Sabina
se	se	k3xPyFc4	se
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
začal	začít	k5eAaPmAgMnS	začít
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zabývat	zabývat	k5eAaImF	zabývat
otázkami	otázka	k1gFnPc7	otázka
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vynořovaly	vynořovat	k5eAaImAgFnP	vynořovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
úmrtím	úmrtí	k1gNnSc7	úmrtí
jeho	on	k3xPp3gMnSc2	on
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
Waltera	Walter	k1gMnSc2	Walter
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1918	[number]	k4	1918
a	a	k8xC	a
hlubokým	hluboký	k2eAgInSc7d1	hluboký
zármutkem	zármutek	k1gInSc7	zármutek
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
školák	školák	k1gMnSc1	školák
četl	číst	k5eAaImAgMnS	číst
Bonhoeffer	Bonhoeffer	k1gInSc4	Bonhoeffer
Schleiermacherovy	Schleiermacherův	k2eAgFnSc2d1	Schleiermacherův
Řeči	řeč	k1gFnSc2	řeč
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
Naumannovy	Naumannův	k2eAgInPc1d1	Naumannův
Listy	list	k1gInPc1	list
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
a	a	k8xC	a
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
církevní	církevní	k2eAgFnPc4d1	církevní
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
primě	prima	k1gFnSc6	prima
si	se	k3xPyFc3	se
jako	jako	k9	jako
volitelný	volitelný	k2eAgInSc4d1	volitelný
předmět	předmět	k1gInSc4	předmět
zvolil	zvolit	k5eAaPmAgMnS	zvolit
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
evangelickou	evangelický	k2eAgFnSc4d1	evangelická
teologii	teologie	k1gFnSc4	teologie
jako	jako	k8xC	jako
vysněné	vysněný	k2eAgNnSc4d1	vysněné
povolání	povolání	k1gNnSc4	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rodinu	rodina	k1gFnSc4	rodina
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
překvapení	překvapení	k1gNnSc1	překvapení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
úmyslu	úmysl	k1gInSc6	úmysl
ho	on	k3xPp3gInSc4	on
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
na	na	k7c6	na
berlínském	berlínský	k2eAgNnSc6d1	berlínské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
Grunewald	Grunewalda	k1gFnPc2	Grunewalda
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Walther-Rathenau-Schule	Walther-Rathenau-Schule	k1gFnSc2	Walther-Rathenau-Schule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
teologii	teologie	k1gFnSc4	teologie
v	v	k7c4	v
Tübingen	Tübingen	k1gInSc4	Tübingen
a	a	k8xC	a
vedle	vedle	k7c2	vedle
toho	ten	k3xDgInSc2	ten
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
i	i	k9	i
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Zapojil	zapojit	k5eAaPmAgInS	zapojit
se	se	k3xPyFc4	se
též	též	k9	též
do	do	k7c2	do
tamního	tamní	k2eAgInSc2d1	tamní
studentského	studentský	k2eAgInSc2d1	studentský
spolku	spolek	k1gInSc2	spolek
Igel	Igela	k1gFnPc2	Igela
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studijním	studijní	k2eAgInSc6d1	studijní
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgMnS	přejít
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
významnými	významný	k2eAgMnPc7d1	významný
zástupci	zástupce	k1gMnPc7	zástupce
liberální	liberální	k2eAgFnSc2d1	liberální
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
ho	on	k3xPp3gMnSc4	on
nezanedbatelně	zanedbatelně	k6eNd1	zanedbatelně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
například	například	k6eAd1	například
Adolf	Adolf	k1gMnSc1	Adolf
von	von	k1gInSc4	von
Harnack	Harnacka	k1gFnPc2	Harnacka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
také	také	k9	také
spadá	spadat	k5eAaImIp3nS	spadat
jeho	jeho	k3xOp3gNnSc3	jeho
seznámení	seznámení	k1gNnSc3	seznámení
s	s	k7c7	s
dialektickou	dialektický	k2eAgFnSc7d1	dialektická
teologií	teologie	k1gFnSc7	teologie
a	a	k8xC	a
zejména	zejména	k9	zejména
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
Karlem	Karel	k1gMnSc7	Karel
Barthem	Barth	k1gInSc7	Barth
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zůstal	zůstat	k5eAaPmAgInS	zůstat
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
s	s	k7c7	s
Barthem	Barth	k1gInSc7	Barth
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
teologií	teologie	k1gFnSc7	teologie
pevně	pevně	k6eAd1	pevně
spojen	spojit	k5eAaPmNgInS	spojit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
si	se	k3xPyFc3	se
udržoval	udržovat	k5eAaImAgInS	udržovat
určitý	určitý	k2eAgInSc4d1	určitý
kritický	kritický	k2eAgInSc4d1	kritický
odstup	odstup	k1gInSc4	odstup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
promoval	promovat	k5eAaBmAgMnS	promovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
disertační	disertační	k2eAgFnSc1d1	disertační
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Reinholdem	Reinhold	k1gMnSc7	Reinhold
Seebergem	Seeberg	k1gMnSc7	Seeberg
<g/>
,	,	kIx,	,
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Sanctorum	Sanctorum	k1gInSc1	Sanctorum
Communio	Communio	k1gNnSc1	Communio
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Společenství	společenství	k1gNnSc1	společenství
svatých	svatá	k1gFnPc2	svatá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teologicko-sociologická	teologickoociologický	k2eAgFnSc1d1	teologicko-sociologický
reflexe	reflexe	k1gFnSc1	reflexe
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
Bartha	Barth	k1gMnSc2	Barth
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
Georgem	Georg	k1gMnSc7	Georg
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Hegelem	Hegel	k1gMnSc7	Hegel
<g/>
,	,	kIx,	,
Maxem	Max	k1gMnSc7	Max
Weberem	Weber	k1gMnSc7	Weber
a	a	k8xC	a
Ernstem	Ernst	k1gMnSc7	Ernst
Troeltschem	Troeltsch	k1gMnSc7	Troeltsch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1928	[number]	k4	1928
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
složil	složit	k5eAaPmAgMnS	složit
před	před	k7c7	před
evangelickou	evangelický	k2eAgFnSc7d1	evangelická
konsistoří	konsistoř	k1gFnSc7	konsistoř
berlínsko-braniborské	berlínskoraniborský	k2eAgFnSc2d1	berlínsko-braniborský
provinční	provinční	k2eAgFnSc2d1	provinční
církve	církev	k1gFnSc2	církev
staropruské	staropruský	k2eAgFnSc2d1	staropruský
unie	unie	k1gFnSc2	unie
první	první	k4xOgFnSc4	první
teologickou	teologický	k2eAgFnSc4d1	teologická
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vikářem	vikář	k1gMnSc7	vikář
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
evangelickém	evangelický	k2eAgInSc6d1	evangelický
sboru	sbor	k1gInSc6	sbor
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
odborným	odborný	k2eAgMnSc7d1	odborný
asistentem	asistent	k1gMnSc7	asistent
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Fridricha	Fridrich	k1gMnSc2	Fridrich
Viléma	Vilém	k1gMnSc2	Vilém
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
24	[number]	k4	24
let	let	k1gInSc1	let
habilitován	habilitován	k2eAgInSc1d1	habilitován
v	v	k7c6	v
systematické	systematický	k2eAgFnSc6d1	systematická
teologii	teologie	k1gFnSc6	teologie
se	s	k7c7	s
spisem	spis	k1gInSc7	spis
"	"	kIx"	"
<g/>
Akt	akt	k1gInSc1	akt
und	und	k?	und
Sein	Seina	k1gFnPc2	Seina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Čin	čin	k1gInSc1	čin
a	a	k8xC	a
bytí	bytí	k1gNnSc1	bytí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pojednávajícím	pojednávající	k2eAgNnSc7d1	pojednávající
o	o	k7c4	o
transcendentální	transcendentální	k2eAgFnSc4d1	transcendentální
filozofii	filozofie	k1gFnSc4	filozofie
a	a	k8xC	a
ontologii	ontologie	k1gFnSc4	ontologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
složil	složit	k5eAaPmAgMnS	složit
druhou	druhý	k4xOgFnSc4	druhý
teologickou	teologický	k2eAgFnSc4d1	teologická
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
minimálního	minimální	k2eAgInSc2d1	minimální
předepsaného	předepsaný	k2eAgInSc2d1	předepsaný
věku	věk	k1gInSc2	věk
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
ordinován	ordinovat	k5eAaImNgInS	ordinovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
odešel	odejít	k5eAaPmAgMnS	odejít
jako	jako	k9	jako
stipendista	stipendista	k1gMnSc1	stipendista
na	na	k7c4	na
Union	union	k1gInSc4	union
Theological	Theological	k1gFnSc2	Theological
Seminary	Seminara	k1gFnSc2	Seminara
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
praktickou	praktický	k2eAgFnSc7d1	praktická
pastorální	pastorální	k2eAgFnSc7d1	pastorální
prací	práce	k1gFnSc7	práce
ve	v	k7c6	v
sborech	sbor	k1gInPc6	sbor
čtvrti	čtvrt	k1gFnSc2	čtvrt
Harlem	Harl	k1gInSc7	Harl
a	a	k8xC	a
kde	kde	k6eAd1	kde
zažil	zažít	k5eAaPmAgMnS	zažít
důsledky	důsledek	k1gInPc4	důsledek
světové	světový	k2eAgFnSc2d1	světová
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
obzvláště	obzvláště	k6eAd1	obzvláště
Afroameričany	Afroameričan	k1gMnPc4	Afroameričan
a	a	k8xC	a
farmáře	farmář	k1gMnPc4	farmář
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
teologii	teologie	k1gFnSc3	teologie
skeptický	skeptický	k2eAgMnSc1d1	skeptický
<g/>
,	,	kIx,	,
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
ho	on	k3xPp3gMnSc4	on
hnutí	hnutí	k1gNnPc1	hnutí
Social	Social	k1gMnSc1	Social
Gospel	gospel	k1gInSc1	gospel
<g/>
.	.	kIx.	.
</s>
<s>
Podnícen	podnítit	k5eAaPmNgInS	podnítit
kritickými	kritický	k2eAgFnPc7d1	kritická
otázkami	otázka	k1gFnPc7	otázka
Američanů	Američan	k1gMnPc2	Američan
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
striktním	striktní	k2eAgInSc7d1	striktní
pacifismem	pacifismus	k1gInSc7	pacifismus
svého	svůj	k3xOyFgMnSc2	svůj
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spolužáka	spolužák	k1gMnSc2	spolužák
Jeana	Jean	k1gMnSc2	Jean
Lasserra	Lasserr	k1gMnSc2	Lasserr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
k	k	k7c3	k
politickým	politický	k2eAgFnPc3d1	politická
otázkám	otázka	k1gFnPc3	otázka
zdrženlivý	zdrženlivý	k2eAgInSc4d1	zdrženlivý
Bonhoeffer	Bonhoeffer	k1gInSc4	Bonhoeffer
<g/>
,	,	kIx,	,
zaobírat	zaobírat	k5eAaImF	zaobírat
tématem	téma	k1gNnSc7	téma
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
Bonhoeffer	Bonhoeffer	k1gInSc4	Bonhoeffer
na	na	k7c6	na
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
univerzitě	univerzita	k1gFnSc6	univerzita
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
teologa	teolog	k1gMnSc2	teolog
Wilhelma	Wilhelmum	k1gNnSc2	Wilhelmum
Lütgerta	Lütgert	k1gMnSc2	Lütgert
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
přednášel	přednášet	k5eAaImAgMnS	přednášet
v	v	k7c6	v
zimním	zimní	k2eAgInSc6d1	zimní
semestru	semestr	k1gInSc6	semestr
1931	[number]	k4	1931
<g/>
/	/	kIx~	/
<g/>
1932	[number]	k4	1932
dějiny	dějiny	k1gFnPc4	dějiny
systematické	systematický	k2eAgFnSc2d1	systematická
teologie	teologie	k1gFnSc2	teologie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vedl	vést	k5eAaImAgInS	vést
seminář	seminář	k1gInSc1	seminář
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Idea	idea	k1gFnSc1	idea
filozofie	filozofie	k1gFnSc1	filozofie
a	a	k8xC	a
protestantská	protestantský	k2eAgFnSc1d1	protestantská
teologie	teologie	k1gFnSc1	teologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
přednáškou	přednáška	k1gFnSc7	přednáška
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
podstata	podstata	k1gFnSc1	podstata
církve	církev	k1gFnSc2	církev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
vymezoval	vymezovat	k5eAaImAgMnS	vymezovat
vůči	vůči	k7c3	vůči
Harnackově	Harnackův	k2eAgFnSc3d1	Harnackův
"	"	kIx"	"
<g/>
Podstatě	podstata	k1gFnSc3	podstata
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
přednášel	přednášet	k5eAaImAgMnS	přednášet
christologii	christologie	k1gFnSc4	christologie
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
euforii	euforie	k1gFnSc3	euforie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
mezi	mezi	k7c7	mezi
protestanty	protestant	k1gMnPc7	protestant
<g/>
,	,	kIx,	,
vnímala	vnímat	k5eAaImAgFnS	vnímat
Bonhoefferova	Bonhoefferův	k2eAgFnSc1d1	Bonhoefferova
rodina	rodina	k1gFnSc1	rodina
převzetí	převzetí	k1gNnSc2	převzetí
moci	moc	k1gFnSc2	moc
nacisty	nacista	k1gMnSc2	nacista
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
velmi	velmi	k6eAd1	velmi
kriticky	kriticky	k6eAd1	kriticky
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoefferův	Bonhoefferův	k2eAgMnSc1d1	Bonhoefferův
švagr	švagr	k1gMnSc1	švagr
Rüdiger	Rüdigra	k1gFnPc2	Rüdigra
Schleicher	Schleichra	k1gFnPc2	Schleichra
toho	ten	k3xDgInSc2	ten
večera	večer	k1gInSc2	večer
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
válku	válka	k1gFnSc4	válka
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1933	[number]	k4	1933
měl	mít	k5eAaImAgInS	mít
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
v	v	k7c6	v
rádiu	rádio	k1gNnSc6	rádio
přednášku	přednáška	k1gFnSc4	přednáška
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Proměny	proměna	k1gFnSc2	proměna
pojmu	pojem	k1gInSc2	pojem
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
požadoval	požadovat	k5eAaImAgMnS	požadovat
omezení	omezení	k1gNnSc4	omezení
totální	totální	k2eAgFnSc2d1	totální
moci	moc	k1gFnSc2	moc
říšského	říšský	k2eAgMnSc2d1	říšský
kancléře	kancléř	k1gMnSc2	kancléř
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
a	a	k8xC	a
blaha	blaho	k1gNnPc4	blaho
občanů	občan	k1gMnPc2	občan
<g/>
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okamžiku	okamžik	k1gInSc6	okamžik
byl	být	k5eAaImAgInS	být
přenos	přenos	k1gInSc1	přenos
kvůli	kvůli	k7c3	kvůli
zjevné	zjevný	k2eAgFnSc3d1	zjevná
kritice	kritika	k1gFnSc3	kritika
nacistického	nacistický	k2eAgInSc2d1	nacistický
"	"	kIx"	"
<g/>
principu	princip	k1gInSc2	princip
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
"	"	kIx"	"
a	a	k8xC	a
Hitlerova	Hitlerův	k2eAgInSc2d1	Hitlerův
kultu	kult	k1gInSc2	kult
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gFnSc4	jeho
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
farář	farář	k1gMnSc1	farář
Franz	Franz	k1gMnSc1	Franz
Hildebrandt	Hildebrandt	k1gMnSc1	Hildebrandt
<g/>
,	,	kIx,	,
i	i	k8xC	i
švagr	švagr	k1gMnSc1	švagr
Gerhard	Gerhard	k1gMnSc1	Gerhard
Leibholz	Leibholz	k1gMnSc1	Leibholz
oba	dva	k4xCgMnPc1	dva
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
dotýkaly	dotýkat	k5eAaImAgFnP	dotýkat
se	se	k3xPyFc4	se
Bonhoeffera	Bonhoeffero	k1gNnSc2	Bonhoeffero
důsledky	důsledek	k1gInPc4	důsledek
pronásledování	pronásledování	k1gNnSc1	pronásledování
Židů	Žid	k1gMnPc2	Žid
velmi	velmi	k6eAd1	velmi
citelně	citelně	k6eAd1	citelně
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
řekl	říct	k5eAaPmAgMnS	říct
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
kázání	kázání	k1gNnSc6	kázání
<g/>
:	:	kIx,	:
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
Paula	Paul	k1gMnSc2	Paul
Lehmanna	Lehmann	k1gMnSc2	Lehmann
v	v	k7c6	v
USA	USA	kA	USA
snažil	snažit	k5eAaImAgInS	snažit
informovat	informovat	k5eAaBmF	informovat
vrchního	vrchní	k2eAgMnSc4d1	vrchní
rabína	rabín	k1gMnSc4	rabín
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
o	o	k7c6	o
takzvaném	takzvaný	k2eAgInSc6d1	takzvaný
bojkotu	bojkot	k1gInSc6	bojkot
židovských	židovský	k2eAgInPc2d1	židovský
obchodů	obchod	k1gInPc2	obchod
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
pojednání	pojednání	k1gNnSc4	pojednání
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Církev	církev	k1gFnSc1	církev
před	před	k7c7	před
židovskou	židovský	k2eAgFnSc7d1	židovská
otázkou	otázka	k1gFnSc7	otázka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
árijských	árijský	k2eAgInPc2d1	árijský
paragrafů	paragraf	k1gInPc2	paragraf
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1933	[number]	k4	1933
ho	on	k3xPp3gInSc4	on
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
až	až	k6eAd1	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ho	on	k3xPp3gMnSc4	on
přednesl	přednést	k5eAaPmAgMnS	přednést
v	v	k7c6	v
kroužku	kroužek	k1gInSc6	kroužek
farářů	farář	k1gMnPc2	farář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
ho	on	k3xPp3gMnSc4	on
ještě	ještě	k6eAd1	ještě
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
cenzurními	cenzurní	k2eAgNnPc7d1	cenzurní
opatřeními	opatření	k1gNnPc7	opatření
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
nechal	nechat	k5eAaPmAgMnS	nechat
vytisknout	vytisknout	k5eAaPmF	vytisknout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
evangelický	evangelický	k2eAgMnSc1d1	evangelický
teolog	teolog	k1gMnSc1	teolog
se	se	k3xPyFc4	se
tak	tak	k9	tak
vedle	vedle	k7c2	vedle
Heinricha	Heinrich	k1gMnSc2	Heinrich
Vogela	Vogel	k1gMnSc2	Vogel
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kříž	Kříž	k1gMnSc1	Kříž
a	a	k8xC	a
hákový	hákový	k2eAgInSc1d1	hákový
kříž	kříž	k1gInSc1	kříž
<g/>
"	"	kIx"	"
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
zabývat	zabývat	k5eAaImF	zabývat
otázkou	otázka	k1gFnSc7	otázka
vztahu	vztah	k1gInSc2	vztah
rasové	rasový	k2eAgFnSc2d1	rasová
ideologie	ideologie	k1gFnSc2	ideologie
nacistů	nacista	k1gMnPc2	nacista
a	a	k8xC	a
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jakých	jaký	k3yRgFnPc2	jaký
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
zda	zda	k8xS	zda
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
církvi	církev	k1gFnSc3	církev
dáno	dát	k5eAaPmNgNnS	dát
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
nechtěl	chtít	k5eNaImAgInS	chtít
ponechat	ponechat	k5eAaPmF	ponechat
na	na	k7c4	na
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
"	"	kIx"	"
<g/>
evangelického	evangelický	k2eAgInSc2d1	evangelický
koncilu	koncil	k1gInSc2	koncil
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
teologů	teolog	k1gMnPc2	teolog
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
před	před	k7c4	před
zásahy	zásah	k1gInPc4	zásah
státu	stát	k1gInSc2	stát
chránili	chránit	k5eAaImAgMnP	chránit
nanejvýš	nanejvýš	k6eAd1	nanejvýš
židovské	židovský	k2eAgMnPc4d1	židovský
členy	člen	k1gMnPc4	člen
svých	svůj	k3xOyFgFnPc2	svůj
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
povýšil	povýšit	k5eAaPmAgMnS	povýšit
obranu	obrana	k1gFnSc4	obrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
za	za	k7c4	za
povinnost	povinnost	k1gFnSc4	povinnost
celé	celý	k2eAgFnSc2d1	celá
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
také	také	k9	také
již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
proti	proti	k7c3	proti
pronásledování	pronásledování	k1gNnSc3	pronásledování
Židů	Žid	k1gMnPc2	Žid
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
myšlenka	myšlenka	k1gFnSc1	myšlenka
koncilu	koncil	k1gInSc2	koncil
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
luterstvím	luterství	k1gNnSc7	luterství
odchovaným	odchovaný	k2eAgMnPc3d1	odchovaný
posluchačům	posluchač	k1gMnPc3	posluchač
natolik	natolik	k6eAd1	natolik
cizí	cizí	k2eAgMnSc1d1	cizí
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
během	během	k7c2	během
přednášky	přednáška	k1gFnSc2	přednáška
na	na	k7c4	na
protest	protest	k1gInSc4	protest
opustili	opustit	k5eAaPmAgMnP	opustit
místnost	místnost	k1gFnSc4	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
později	pozdě	k6eAd2	pozdě
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
postoji	postoj	k1gInPc7	postoj
zůstane	zůstat	k5eAaPmIp3nS	zůstat
izolován	izolován	k2eAgInSc1d1	izolován
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
samostatně	samostatně	k6eAd1	samostatně
a	a	k8xC	a
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
necírkevního	církevní	k2eNgInSc2d1	necírkevní
<g/>
,	,	kIx,	,
vojenského	vojenský	k2eAgInSc2d1	vojenský
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
režimu	režim	k1gInSc2	režim
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
1933	[number]	k4	1933
byli	být	k5eAaImAgMnP	být
podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
většiny	většina	k1gFnSc2	většina
Německých	německý	k2eAgMnPc2d1	německý
křesťanů	křesťan	k1gMnPc2	křesťan
sesazováni	sesazován	k2eAgMnPc1d1	sesazován
generální	generální	k2eAgMnSc1d1	generální
superintendenti	superintendent	k1gMnPc1	superintendent
v	v	k7c6	v
pruské	pruský	k2eAgFnSc6d1	pruská
zemské	zemský	k2eAgFnSc6d1	zemská
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
dosazen	dosazen	k2eAgMnSc1d1	dosazen
státní	státní	k2eAgMnSc1d1	státní
komisař	komisař	k1gMnSc1	komisař
August	August	k1gMnSc1	August
Jäger	Jäger	k1gMnSc1	Jäger
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Ludwiga	Ludwig	k1gMnSc4	Ludwig
Müllera	Müller	k1gMnSc4	Müller
svým	svůj	k3xOyFgInSc7	svůj
"	"	kIx"	"
<g/>
důvěrníkem	důvěrník	k1gMnSc7	důvěrník
pro	pro	k7c4	pro
církevní	církevní	k2eAgFnPc4d1	církevní
otázky	otázka	k1gFnPc4	otázka
<g/>
"	"	kIx"	"
a	a	k8xC	a
Němečtí	německý	k2eAgMnPc1d1	německý
křesťané	křesťan	k1gMnPc1	křesťan
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
pokusili	pokusit	k5eAaPmAgMnP	pokusit
povýšit	povýšit	k5eAaPmF	povýšit
na	na	k7c4	na
říšského	říšský	k2eAgMnSc4d1	říšský
biskupa	biskup	k1gMnSc4	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
teď	teď	k6eAd1	teď
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
účinné	účinný	k2eAgFnSc2d1	účinná
evangelické	evangelický	k2eAgFnSc2d1	evangelická
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
stávku	stávka	k1gFnSc4	stávka
za	za	k7c4	za
odstoupení	odstoupení	k1gNnSc4	odstoupení
státního	státní	k2eAgMnSc2d1	státní
komisaře	komisař	k1gMnSc2	komisař
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc1d1	spočívající
v	v	k7c6	v
odmítnutí	odmítnutí	k1gNnSc6	odmítnutí
vysluhování	vysluhování	k1gNnPc2	vysluhování
pohřbů	pohřeb	k1gInPc2	pohřeb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
ji	on	k3xPp3gFnSc4	on
nepovažoval	považovat	k5eNaImAgInS	považovat
za	za	k7c4	za
realizovatelnou	realizovatelný	k2eAgFnSc4d1	realizovatelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
tento	tento	k3xDgInSc4	tento
prostředek	prostředek	k1gInSc1	prostředek
skutečně	skutečně	k6eAd1	skutečně
zastavil	zastavit	k5eAaPmAgInS	zastavit
nacistické	nacistický	k2eAgMnPc4d1	nacistický
okupanty	okupant	k1gMnPc4	okupant
v	v	k7c6	v
uskutečňování	uskutečňování	k1gNnSc6	uskutečňování
některých	některý	k3yIgInPc2	některý
kroků	krok	k1gInPc2	krok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
církevních	církevní	k2eAgFnPc6d1	církevní
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
stát	stát	k5eAaImF	stát
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
zorganizoval	zorganizovat	k5eAaPmAgInS	zorganizovat
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
Němečtí	německý	k2eAgMnPc1d1	německý
křesťané	křesťan	k1gMnPc1	křesťan
s	s	k7c7	s
asi	asi	k9	asi
70	[number]	k4	70
procenty	procento	k1gNnPc7	procento
hlasů	hlas	k1gInPc2	hlas
nad	nad	k7c7	nad
Mladoreformátorským	Mladoreformátorský	k2eAgNnSc7d1	Mladoreformátorský
hnutím	hnutí	k1gNnSc7	hnutí
(	(	kIx(	(
<g/>
Jungreformatorische	Jungreformatorische	k1gNnPc1	Jungreformatorische
Bewegung	Bewegunga	k1gFnPc2	Bewegunga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
skupiny	skupina	k1gFnPc1	skupina
pokusily	pokusit	k5eAaPmAgFnP	pokusit
"	"	kIx"	"
<g/>
vyznáním	vyznání	k1gNnSc7	vyznání
víry	víra	k1gFnSc2	víra
<g/>
"	"	kIx"	"
přinutit	přinutit	k5eAaPmF	přinutit
nové	nový	k2eAgMnPc4d1	nový
funkcionáře	funkcionář	k1gMnPc4	funkcionář
svých	svůj	k3xOyFgFnPc2	svůj
církví	církev	k1gFnPc2	církev
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
postoj	postoj	k1gInSc4	postoj
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
požádali	požádat	k5eAaPmAgMnP	požádat
Bonhoeffera	Bonhoeffero	k1gNnSc2	Bonhoeffero
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
teologem	teolog	k1gMnSc7	teolog
Hermannem	Hermann	k1gMnSc7	Hermann
Sassem	Sass	k1gMnSc7	Sass
z	z	k7c2	z
Erlangenu	Erlangen	k1gInSc2	Erlangen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zformuloval	zformulovat	k5eAaPmAgInS	zformulovat
celoříšský	celoříšský	k2eAgInSc1d1	celoříšský
jednotný	jednotný	k2eAgInSc1d1	jednotný
návrh	návrh	k1gInSc1	návrh
vyznání	vyznání	k1gNnSc2	vyznání
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
starozákonní	starozákonní	k2eAgMnSc1d1	starozákonní
teolog	teolog	k1gMnSc1	teolog
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Vischer	Vischra	k1gFnPc2	Vischra
z	z	k7c2	z
Bethelu	Bethel	k1gInSc2	Bethel
napsal	napsat	k5eAaPmAgInS	napsat
první	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
článku	článek	k1gInSc2	článek
o	o	k7c6	o
"	"	kIx"	"
<g/>
židovské	židovský	k2eAgFnSc6d1	židovská
otázce	otázka	k1gFnSc6	otázka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Společný	společný	k2eAgInSc1d1	společný
návrh	návrh	k1gInSc1	návrh
vyšel	vyjít	k5eAaPmAgInS	vyjít
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
1933	[number]	k4	1933
a	a	k8xC	a
farář	farář	k1gMnSc1	farář
Bodelschwingh	Bodelschwingh	k1gMnSc1	Bodelschwingh
<g/>
,	,	kIx,	,
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
vedoucí	vedoucí	k1gMnSc1	vedoucí
Bethelských	Bethelský	k2eAgInPc2d1	Bethelský
institutů	institut	k1gInPc2	institut
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
dvaceti	dvacet	k4xCc3	dvacet
posuzovatelům	posuzovatel	k1gMnPc3	posuzovatel
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
pak	pak	k6eAd1	pak
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnPc4	jeho
pasáže	pasáž	k1gFnPc4	pasáž
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
církevní	církevní	k2eAgFnSc2d1	církevní
obhajoby	obhajoba	k1gFnSc2	obhajoba
Židů	Žid	k1gMnPc2	Žid
před	před	k7c7	před
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
natolik	natolik	k6eAd1	natolik
zmírnili	zmírnit	k5eAaPmAgMnP	zmírnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
bylo	být	k5eAaImAgNnS	být
Bethelské	Bethelský	k2eAgNnSc1d1	Bethelský
vyznání	vyznání	k1gNnSc1	vyznání
důležitým	důležitý	k2eAgInSc7d1	důležitý
krokem	krok	k1gInSc7	krok
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
árijských	árijský	k2eAgMnPc2d1	árijský
paragrafů	paragraf	k1gInPc2	paragraf
evangelickou	evangelický	k2eAgFnSc7d1	evangelická
církví	církev	k1gFnSc7	církev
na	na	k7c6	na
staropruské	staropruský	k2eAgFnSc6d1	staropruský
generální	generální	k2eAgFnSc6d1	generální
synodě	synoda	k1gFnSc6	synoda
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1933	[number]	k4	1933
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
opozičním	opoziční	k2eAgMnPc3d1	opoziční
farářům	farář	k1gMnPc3	farář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
z	z	k7c2	z
Německé	německý	k2eAgFnSc2d1	německá
evangelické	evangelický	k2eAgFnSc2d1	evangelická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pouhý	pouhý	k2eAgInSc1d1	pouhý
přívěsek	přívěsek	k1gInSc1	přívěsek
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
postoje	postoj	k1gInPc4	postoj
nyní	nyní	k6eAd1	nyní
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
heretické	heretický	k2eAgNnSc4d1	heretické
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
však	však	k9	však
ještě	ještě	k6eAd1	ještě
sotva	sotva	k6eAd1	sotva
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
s	s	k7c7	s
rozštěpením	rozštěpení	k1gNnSc7	rozštěpení
církve	církev	k1gFnSc2	církev
–	–	k?	–
dokonce	dokonce	k9	dokonce
i	i	k9	i
Karl	Karl	k1gMnSc1	Karl
Barth	Barth	k1gMnSc1	Barth
považoval	považovat	k5eAaImAgMnS	považovat
opozici	opozice	k1gFnSc4	opozice
uvnitř	uvnitř	k7c2	uvnitř
církve	církev	k1gFnSc2	církev
za	za	k7c4	za
možnou	možný	k2eAgFnSc4d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Niemöllerem	Niemöller	k1gMnSc7	Niemöller
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
založil	založit	k5eAaPmAgInS	založit
spolek	spolek	k1gInSc1	spolek
farářů	farář	k1gMnPc2	farář
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgInS	mít
chránit	chránit	k5eAaImF	chránit
ohrožené	ohrožený	k2eAgMnPc4d1	ohrožený
faráře	farář	k1gMnPc4	farář
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
ekumenického	ekumenický	k2eAgNnSc2d1	ekumenické
setkání	setkání	k1gNnSc2	setkání
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
zástupce	zástupce	k1gMnSc1	zástupce
podrobně	podrobně	k6eAd1	podrobně
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zvolením	zvolení	k1gNnSc7	zvolení
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Müllera	Müller	k1gMnSc2	Müller
říšským	říšský	k2eAgMnSc7d1	říšský
biskupem	biskup	k1gMnSc7	biskup
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1933	[number]	k4	1933
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
jasně	jasně	k6eAd1	jasně
formulovaný	formulovaný	k2eAgInSc1d1	formulovaný
leták	leták	k1gInSc1	leták
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Árijský	árijský	k2eAgInSc1d1	árijský
paragraf	paragraf	k1gInSc1	paragraf
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
připevňoval	připevňovat	k5eAaImAgMnS	připevňovat
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
na	na	k7c4	na
stromy	strom	k1gInPc4	strom
a	a	k8xC	a
pouliční	pouliční	k2eAgFnPc4d1	pouliční
lampy	lampa	k1gFnPc4	lampa
jako	jako	k8xS	jako
protestní	protestní	k2eAgInSc1d1	protestní
plakát	plakát	k1gInSc1	plakát
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přijmout	přijmout	k5eAaPmF	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
faráře	farář	k1gMnSc2	farář
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
z	z	k7c2	z
července	červenec	k1gInSc2	červenec
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1933	[number]	k4	1933
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
farář	farář	k1gMnSc1	farář
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
předměstí	předměstí	k1gNnSc6	předměstí
Londýna	Londýn	k1gInSc2	Londýn
Forest	Forest	k1gMnSc1	Forest
Hill	Hill	k1gMnSc1	Hill
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
tamní	tamní	k2eAgInPc4d1	tamní
německojazyčné	německojazyčný	k2eAgInPc4d1	německojazyčný
sbory	sbor	k1gInPc4	sbor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
luteránský	luteránský	k2eAgInSc4d1	luteránský
sbor	sbor	k1gInSc4	sbor
ve	v	k7c4	v
Forest	Forest	k1gInSc4	Forest
Hill	Hill	k1gMnSc1	Hill
a	a	k8xC	a
pro	pro	k7c4	pro
reformovanou	reformovaný	k2eAgFnSc4d1	reformovaná
církev	církev	k1gFnSc4	církev
St.	st.	kA	st.
Paul	Paul	k1gMnSc1	Paul
ve	v	k7c6	v
východolondýnské	východolondýnský	k2eAgFnSc6d1	východolondýnský
čtvrti	čtvrt	k1gFnSc6	čtvrt
Whitechapel	Whitechapela	k1gFnPc2	Whitechapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
dopisu	dopis	k1gInSc6	dopis
Karlu	Karel	k1gMnSc3	Karel
Barthovi	Barth	k1gMnSc3	Barth
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zdůvodňuje	zdůvodňovat	k5eAaImIp3nS	zdůvodňovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
déle	dlouho	k6eAd2	dlouho
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
členem	člen	k1gMnSc7	člen
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
k	k	k7c3	k
Německým	německý	k2eAgMnPc3d1	německý
křesťanům	křesťan	k1gMnPc3	křesťan
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
názoru	názor	k1gInSc3	názor
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
izolován	izolovat	k5eAaBmNgMnS	izolovat
i	i	k8xC	i
mezi	mezi	k7c7	mezi
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
takto	takto	k6eAd1	takto
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
získat	získat	k5eAaPmF	získat
odstup	odstup	k1gInSc1	odstup
od	od	k7c2	od
německých	německý	k2eAgFnPc2d1	německá
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
mohl	moct	k5eAaImAgMnS	moct
posléze	posléze	k6eAd1	posléze
tím	ten	k3xDgInSc7	ten
razantněji	razantně	k6eAd2	razantně
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Barth	Barth	k1gMnSc1	Barth
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
ho	on	k3xPp3gNnSc4	on
sice	sice	k8xC	sice
hned	hned	k6eAd1	hned
neposlechl	poslechnout	k5eNaPmAgMnS	poslechnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bartha	Bartha	k1gFnSc1	Bartha
později	pozdě	k6eAd2	pozdě
velice	velice	k6eAd1	velice
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
touto	tento	k3xDgFnSc7	tento
reakcí	reakce	k1gFnSc7	reakce
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
Bonhoefferovu	Bonhoefferův	k2eAgNnSc3d1	Bonhoefferovo
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	jeho	k3xOp3gFnSc4	jeho
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
odboji	odboj	k1gInSc6	odboj
i	i	k8xC	i
mučednickou	mučednický	k2eAgFnSc4d1	mučednická
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	se	k3xPyFc4	se
s	s	k7c7	s
Bonhoefferem	Bonhoeffer	k1gInSc7	Bonhoeffer
seznámil	seznámit	k5eAaPmAgMnS	seznámit
George	George	k1gNnSc4	George
Kennedy	Kenneda	k1gMnSc2	Kenneda
Allen	Allen	k1gMnSc1	Allen
Bell	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
anglikánského	anglikánský	k2eAgMnSc2d1	anglikánský
biskupa	biskup	k1gMnSc2	biskup
z	z	k7c2	z
Chichestru	Chichestr	k1gInSc2	Chichestr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zastával	zastávat	k5eAaImAgMnS	zastávat
vysoké	vysoký	k2eAgInPc4d1	vysoký
úřady	úřad	k1gInPc4	úřad
v	v	k7c6	v
ekumenickém	ekumenický	k2eAgNnSc6d1	ekumenické
hnutí	hnutí	k1gNnSc6	hnutí
a	a	k8xC	a
silně	silně	k6eAd1	silně
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
problematice	problematika	k1gFnSc6	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Bell	bell	k1gInSc1	bell
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
partnerů	partner	k1gMnPc2	partner
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Německým	německý	k2eAgMnPc3d1	německý
křesťanům	křesťan	k1gMnPc3	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
také	také	k9	také
Franz	Franz	k1gMnSc1	Franz
Hildebrandt	Hildebrandt	k1gMnSc1	Hildebrandt
<g/>
.	.	kIx.	.
</s>
<s>
Poháněny	poháněn	k2eAgMnPc4d1	poháněn
Bonhoeffeorvým	Bonhoeffeorvý	k2eAgInSc7d1	Bonhoeffeorvý
zápalem	zápal	k1gInSc7	zápal
vystupovaly	vystupovat	k5eAaImAgInP	vystupovat
anglické	anglický	k2eAgInPc1d1	anglický
zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
sbory	sbor	k1gInPc1	sbor
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bellem	bell	k1gInSc7	bell
otevřeně	otevřeně	k6eAd1	otevřeně
proti	proti	k7c3	proti
Německým	německý	k2eAgMnPc3d1	německý
křesťanům	křesťan	k1gMnPc3	křesťan
a	a	k8xC	a
požadovaly	požadovat	k5eAaImAgFnP	požadovat
odstoupení	odstoupení	k1gNnSc4	odstoupení
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Müllera	Müller	k1gMnSc2	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1934	[number]	k4	1934
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Vyznávající	vyznávající	k2eAgFnSc1d1	vyznávající
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
že	že	k9	že
shromáždění	shromáždění	k1gNnSc1	shromáždění
evangelických	evangelický	k2eAgMnPc2d1	evangelický
křesťanů	křesťan	k1gMnPc2	křesťan
jednohlasně	jednohlasně	k6eAd1	jednohlasně
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Barmenskou	Barmenský	k2eAgFnSc4d1	Barmenský
teologickou	teologický	k2eAgFnSc4d1	teologická
deklaraci	deklarace	k1gFnSc4	deklarace
sepsanou	sepsaný	k2eAgFnSc4d1	sepsaná
Karlem	Karel	k1gMnSc7	Karel
Barthem	Barth	k1gInSc7	Barth
a	a	k8xC	a
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
Říšskou	říšský	k2eAgFnSc4d1	říšská
bratrskou	bratrský	k2eAgFnSc4d1	bratrská
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemské	zemský	k2eAgFnSc2d1	zemská
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Durynsku	Durynsko	k1gNnSc6	Durynsko
<g/>
,	,	kIx,	,
Šlesvicko-Holštýnsku	Šlesvicko-Holštýnsko	k1gNnSc6	Šlesvicko-Holštýnsko
<g/>
,	,	kIx,	,
Lübecku	Lübecko	k1gNnSc6	Lübecko
<g/>
,	,	kIx,	,
Sasku	Sasko	k1gNnSc6	Sasko
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
staropruských	staropruský	k2eAgFnPc6d1	staropruský
církevních	církevní	k2eAgFnPc6d1	církevní
provinciích	provincie	k1gFnPc6	provincie
nejsou	být	k5eNaImIp3nP	být
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
tradici	tradice	k1gFnSc3	tradice
státních	státní	k2eAgFnPc2d1	státní
církví	církev	k1gFnPc2	církev
ochotny	ochoten	k2eAgInPc1d1	ochoten
tyto	tento	k3xDgInPc1	tento
závěry	závěr	k1gInPc1	závěr
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
synodálních	synodální	k2eAgFnPc6d1	synodální
volbách	volba	k1gFnPc6	volba
získali	získat	k5eAaPmAgMnP	získat
vedoucí	vedoucí	k2eAgInPc4d1	vedoucí
úřady	úřad	k1gInPc4	úřad
biskupové	biskup	k1gMnPc1	biskup
a	a	k8xC	a
právníci	právník	k1gMnPc1	právník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
Německým	německý	k2eAgMnPc3d1	německý
křesťanům	křesťan	k1gMnPc3	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
rozpoložení	rozpoložení	k1gNnSc6	rozpoložení
směroval	směrovat	k5eAaImAgMnS	směrovat
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
všechny	všechen	k3xTgFnPc4	všechen
naděje	naděje	k1gFnPc4	naděje
na	na	k7c4	na
mladou	mladý	k2eAgFnSc4d1	mladá
ekumenu	ekumena	k1gFnSc4	ekumena
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
mládežnickou	mládežnický	k2eAgFnSc4d1	mládežnická
ekumenickou	ekumenický	k2eAgFnSc4d1	ekumenická
konferenci	konference	k1gFnSc4	konference
na	na	k7c6	na
dánském	dánský	k2eAgInSc6d1	dánský
ostrově	ostrov	k1gInSc6	ostrov
Fanø	Fanø	k1gFnSc2	Fanø
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1934	[number]	k4	1934
byli	být	k5eAaImAgMnP	být
jako	jako	k8xS	jako
německá	německý	k2eAgFnSc1d1	německá
delegace	delegace	k1gFnSc1	delegace
pozváni	pozván	k2eAgMnPc1d1	pozván
pouze	pouze	k6eAd1	pouze
stoupenci	stoupenec	k1gMnPc1	stoupenec
Barmenské	Barmenský	k2eAgFnSc2d1	Barmenský
deklarace	deklarace	k1gFnSc2	deklarace
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k8xS	jako
zástupce	zástupce	k1gMnSc1	zástupce
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
sekretář	sekretář	k1gMnSc1	sekretář
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ranní	ranní	k2eAgFnSc6d1	ranní
pobožnosti	pobožnost	k1gFnSc6	pobožnost
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
přednesl	přednést	k5eAaPmAgMnS	přednést
před	před	k7c7	před
celým	celý	k2eAgNnSc7d1	celé
shromážděním	shromáždění	k1gNnSc7	shromáždění
referát	referát	k1gInSc4	referát
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Církev	církev	k1gFnSc1	církev
a	a	k8xC	a
svět	svět	k1gInSc1	svět
národů	národ	k1gInPc2	národ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgMnSc4	jenž
mnozí	mnohý	k2eAgMnPc1d1	mnohý
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
jako	jako	k9	jako
kázání	kázání	k1gNnSc4	kázání
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
terorizováni	terorizován	k2eAgMnPc1d1	terorizován
pacifisté	pacifista	k1gMnPc1	pacifista
z	z	k7c2	z
jednotek	jednotka	k1gFnPc2	jednotka
SA	SA	kA	SA
a	a	k8xC	a
následně	následně	k6eAd1	následně
zavíráni	zavírat	k5eAaImNgMnP	zavírat
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
debatách	debata	k1gFnPc6	debata
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
delegace	delegace	k1gFnSc1	delegace
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
cítili	cítit	k5eAaImAgMnP	cítit
být	být	k5eAaImF	být
Německem	Německo	k1gNnSc7	Německo
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
<g/>
,	,	kIx,	,
nechtěly	chtít	k5eNaImAgFnP	chtít
úplně	úplně	k6eAd1	úplně
odmítnout	odmítnout	k5eAaPmF	odmítnout
válku	válka	k1gFnSc4	válka
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc4	prostředek
národního	národní	k2eAgInSc2d1	národní
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1935	[number]	k4	1935
se	se	k3xPyFc4	se
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
krátce	krátce	k6eAd1	krátce
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
za	za	k7c7	za
Mahátmou	Mahátma	k1gMnSc7	Mahátma
Gandhím	Gandhí	k1gMnSc7	Gandhí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
převzal	převzít	k5eAaPmAgInS	převzít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
budoucích	budoucí	k2eAgMnPc2d1	budoucí
pastorů	pastor	k1gMnPc2	pastor
na	na	k7c6	na
kazatelském	kazatelský	k2eAgInSc6d1	kazatelský
semináři	seminář	k1gInSc6	seminář
v	v	k7c6	v
Zingsthofu	Zingsthof	k1gInSc6	Zingsthof
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
pomořanského	pomořanský	k2eAgInSc2d1	pomořanský
Finkenwalde	Finkenwald	k1gInSc5	Finkenwald
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
Štětína	Štětín	k1gInSc2	Štětín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
prvních	první	k4xOgMnPc2	první
tamních	tamní	k2eAgMnPc2d1	tamní
studentů	student	k1gMnPc2	student
byl	být	k5eAaImAgInS	být
Eberhard	Eberhard	k1gInSc1	Eberhard
Bethge	Bethg	k1gFnSc2	Bethg
<g/>
,	,	kIx,	,
Bonhoefferův	Bonhoefferův	k2eAgMnSc1d1	Bonhoefferův
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
hojně	hojně	k6eAd1	hojně
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
a	a	k8xC	a
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
životopiscem	životopisec	k1gMnSc7	životopisec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
vyučovací	vyučovací	k2eAgFnSc2d1	vyučovací
činnosti	činnost	k1gFnSc2	činnost
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
Následování	následování	k1gNnSc1	následování
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Nachfolge	Nachfolge	k1gInSc1	Nachfolge
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pochvalně	pochvalně	k6eAd1	pochvalně
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Karl	Karl	k1gMnSc1	Karl
Barth	Barth	k1gMnSc1	Barth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
svou	svůj	k3xOyFgFnSc4	svůj
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
společenstvím	společenství	k1gNnSc7	společenství
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
ne	ne	k9	ne
pouhou	pouhý	k2eAgFnSc7d1	pouhá
nositelkou	nositelka	k1gFnSc7	nositelka
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
zvěsti	zvěst	k1gFnSc2	zvěst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
také	také	k9	také
reálným	reálný	k2eAgNnSc7d1	reálné
tělem	tělo	k1gNnSc7	tělo
Kristovým	Kristův	k2eAgNnSc7d1	Kristovo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
neobejde	obejde	k6eNd1	obejde
bez	bez	k7c2	bez
právého	právý	k2eAgNnSc2d1	právé
a	a	k8xC	a
živého	živý	k2eAgNnSc2d1	živé
následování	následování	k1gNnSc2	následování
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nehledí	hledět	k5eNaImIp3nP	hledět
na	na	k7c4	na
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
jednotlivce	jednotlivec	k1gMnSc4	jednotlivec
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
drahá	drahý	k2eAgFnSc1d1	drahá
milost	milost	k1gFnSc1	milost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
nacisté	nacista	k1gMnPc1	nacista
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
kazatelský	kazatelský	k2eAgInSc4d1	kazatelský
seminář	seminář	k1gInSc4	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mohl	moct	k5eAaImAgMnS	moct
nadále	nadále	k6eAd1	nadále
fungovat	fungovat	k5eAaImF	fungovat
jen	jen	k9	jen
ilegálně	ilegálně	k6eAd1	ilegálně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
sběrného	sběrný	k2eAgInSc2d1	sběrný
vikariátu	vikariát	k1gInSc2	vikariát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Sammelvikariat	Sammelvikariat	k1gInSc1	Sammelvikariat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
a	a	k8xC	a
mlčení	mlčení	k1gNnSc4	mlčení
statečných	statečný	k2eAgMnPc2d1	statečný
superintendantů	superintendant	k1gMnPc2	superintendant
a	a	k8xC	a
farářů	farář	k1gMnPc2	farář
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
tak	tak	k9	tak
oficiálně	oficiálně	k6eAd1	oficiálně
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
pomocný	pomocný	k2eAgMnSc1d1	pomocný
kazatel	kazatel	k1gMnSc1	kazatel
ve	v	k7c6	v
Schlawe	Schlawe	k1gFnSc6	Schlawe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tajně	tajně	k6eAd1	tajně
nadále	nadále	k6eAd1	nadále
vedl	vést	k5eAaImAgMnS	vést
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
vikářů	vikář	k1gMnPc2	vikář
(	(	kIx(	(
<g/>
mladých	mladý	k2eAgMnPc2d1	mladý
bohoslovců	bohoslovec	k1gMnPc2	bohoslovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
připravovali	připravovat	k5eAaImAgMnP	připravovat
na	na	k7c4	na
povolání	povolání	k1gNnSc4	povolání
farářů	farář	k1gMnPc2	farář
<g/>
)	)	kIx)	)
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
v	v	k7c4	v
Köslin	Köslin	k1gInSc4	Köslin
a	a	k8xC	a
Groß	Groß	k1gFnSc4	Groß
Schlönwitz	Schlönwitza	k1gFnPc2	Schlönwitza
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
Sigurdshofu	Sigurdshof	k1gInSc6	Sigurdshof
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
i	i	k9	i
tady	tady	k6eAd1	tady
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1940	[number]	k4	1940
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
Finkenwalde	Finkenwald	k1gInSc5	Finkenwald
reflektoval	reflektovat	k5eAaImAgMnS	reflektovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Společný	společný	k2eAgInSc4d1	společný
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Gemeinsames	Gemeinsames	k1gInSc1	Gemeinsames
Leben	Leben	k1gInSc1	Leben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zostřenému	zostřený	k2eAgNnSc3d1	zostřené
protižidovskému	protižidovský	k2eAgNnSc3d1	protižidovské
zákonodárství	zákonodárství	k1gNnSc3	zákonodárství
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
Bonhoefferova	Bonhoefferův	k2eAgFnSc1d1	Bonhoefferova
sestra	sestra	k1gFnSc1	sestra
Sabina	Sabina	k1gFnSc1	Sabina
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Gerhardem	Gerhard	k1gMnSc7	Gerhard
Leibholzem	Leibholz	k1gInSc7	Leibholz
emigrovat	emigrovat	k5eAaBmF	emigrovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
využil	využít	k5eAaPmAgMnS	využít
svých	svůj	k3xOyFgInPc2	svůj
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
zajistil	zajistit	k5eAaPmAgMnS	zajistit
Leibholzovi	Leibholz	k1gMnSc3	Leibholz
místo	místo	k7c2	místo
poradce	poradce	k1gMnSc2	poradce
u	u	k7c2	u
biskupa	biskup	k1gMnSc2	biskup
George	Georg	k1gMnSc2	Georg
Bella	Bell	k1gMnSc2	Bell
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svého	svůj	k3xOyFgMnSc4	svůj
švagra	švagr	k1gMnSc4	švagr
Hanse	Hans	k1gMnSc4	Hans
von	von	k1gInSc1	von
Dohnanyiho	Dohnanyi	k1gMnSc4	Dohnanyi
navázal	navázat	k5eAaPmAgInS	navázat
první	první	k4xOgInPc4	první
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Wilhelmem	Wilhelm	k1gInSc7	Wilhelm
Canarisem	Canaris	k1gInSc7	Canaris
<g/>
,	,	kIx,	,
Hansem	Hans	k1gMnSc7	Hans
Osterem	Oster	k1gMnSc7	Oster
<g/>
,	,	kIx,	,
Karlem	Karel	k1gMnSc7	Karel
Sackem	Sacek	k1gMnSc7	Sacek
a	a	k8xC	a
Ludwigem	Ludwig	k1gMnSc7	Ludwig
Beckem	Becek	k1gMnSc7	Becek
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
se	se	k3xPyFc4	se
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
přimět	přimět	k5eAaPmF	přimět
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
sdružené	sdružený	k2eAgFnSc2d1	sdružená
v	v	k7c6	v
ekumenickém	ekumenický	k2eAgNnSc6d1	ekumenické
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
postavily	postavit	k5eAaPmAgInP	postavit
proti	proti	k7c3	proti
probíhajícím	probíhající	k2eAgFnPc3d1	probíhající
válečným	válečný	k2eAgFnPc3d1	válečná
přípravám	příprava	k1gFnPc3	příprava
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nacistů	nacista	k1gMnPc2	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
aktivitám	aktivita	k1gFnPc3	aktivita
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
vysokými	vysoký	k2eAgMnPc7d1	vysoký
církevními	církevní	k2eAgMnPc7d1	církevní
hodnostáři	hodnostář	k1gMnPc7	hodnostář
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zasedl	zasednout	k5eAaPmAgInS	zasednout
ke	k	k7c3	k
společným	společný	k2eAgInPc3d1	společný
rozhovorům	rozhovor	k1gInPc3	rozhovor
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
s	s	k7c7	s
biskupem	biskup	k1gMnSc7	biskup
Georgem	Georg	k1gMnSc7	Georg
Bellem	bell	k1gInSc7	bell
a	a	k8xC	a
kde	kde	k6eAd1	kde
znovu	znovu	k6eAd1	znovu
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
předběžné	předběžný	k2eAgFnSc2d1	předběžná
světové	světový	k2eAgFnSc2d1	světová
rady	rada	k1gFnSc2	rada
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
projevovány	projevován	k2eAgFnPc1d1	projevována
sympatie	sympatie	k1gFnPc1	sympatie
<g/>
,	,	kIx,	,
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
ničeho	nic	k3yNnSc2	nic
zásadního	zásadní	k2eAgNnSc2d1	zásadní
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
žádost	žádost	k1gFnSc4	žádost
svého	svůj	k3xOyFgMnSc2	svůj
hostitele	hostitel	k1gMnSc2	hostitel
Smitha-Leipera	Smitha-Leiper	k1gMnSc2	Smitha-Leiper
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přijal	přijmout	k5eAaPmAgInS	přijmout
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Harlemu	Harlem	k1gInSc6	Harlem
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
jako	jako	k8xS	jako
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
německých	německý	k2eAgMnPc2d1	německý
intelektuálů	intelektuál	k1gMnPc2	intelektuál
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
v	v	k7c6	v
hrozící	hrozící	k2eAgFnSc6d1	hrozící
válce	válka	k1gFnSc6	válka
totiž	totiž	k9	totiž
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
odboji	odboj	k1gInSc6	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrocená	vyhrocený	k2eAgFnSc1d1	vyhrocená
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nepřipouštěla	připouštět	k5eNaImAgFnS	připouštět
útěk	útěk	k1gInSc4	útěk
před	před	k7c7	před
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
jednotu	jednota	k1gFnSc4	jednota
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
setrvávajícího	setrvávající	k2eAgInSc2d1	setrvávající
v	v	k7c6	v
časnosti	časnost	k1gFnSc6	časnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
směřujícího	směřující	k2eAgInSc2d1	směřující
k	k	k7c3	k
věčnosti	věčnost	k1gFnSc3	věčnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nanejvýš	nanejvýš	k6eAd1	nanejvýš
těžké	těžký	k2eAgNnSc1d1	těžké
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
neslo	nést	k5eAaImAgNnS	nést
závažné	závažný	k2eAgInPc4d1	závažný
důsledky	důsledek	k1gInPc4	důsledek
<g/>
,	,	kIx,	,
zásadně	zásadně	k6eAd1	zásadně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
Bonhoefferovo	Bonhoefferův	k2eAgNnSc4d1	Bonhoefferovo
další	další	k2eAgNnSc4d1	další
myšlení	myšlení	k1gNnSc4	myšlení
i	i	k8xC	i
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
navštívil	navštívit	k5eAaPmAgMnS	navštívit
svou	svůj	k3xOyFgFnSc4	svůj
sestru	sestra	k1gFnSc4	sestra
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
rodinu	rodina	k1gFnSc4	rodina
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
zavraždění	zavraždění	k1gNnSc6	zavraždění
faráře	farář	k1gMnSc2	farář
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
Paula	Paul	k1gMnSc2	Paul
Schneidera	Schneider	k1gMnSc2	Schneider
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Buchenwald	Buchenwald	k1gInSc1	Buchenwald
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
neteřím	teřet	k5eNaImIp1nS	teřet
Marianně	Marianně	k1gFnSc1	Marianně
a	a	k8xC	a
Christině	Christina	k1gFnSc3	Christina
tehdy	tehdy	k6eAd1	tehdy
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schneider	Schneider	k1gMnSc1	Schneider
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
mučedníkem	mučedník	k1gMnSc7	mučedník
evangelické	evangelický	k2eAgFnSc2d1	evangelická
církve	církev	k1gFnSc2	církev
doby	doba	k1gFnSc2	doba
nacismu	nacismus	k1gInSc2	nacismus
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
měly	mít	k5eAaImAgFnP	mít
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
dobře	dobře	k6eAd1	dobře
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
obnovil	obnovit	k5eAaPmAgMnS	obnovit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Sigurdshofu	Sigurdshof	k1gInSc6	Sigurdshof
a	a	k8xC	a
ve	v	k7c6	v
vrchním	vrchní	k2eAgNnSc6d1	vrchní
velitelství	velitelství	k1gNnSc6	velitelství
wehrmachtu	wehrmacht	k1gInSc3	wehrmacht
hledal	hledat	k5eAaImAgInS	hledat
kontakty	kontakt	k1gInPc4	kontakt
na	na	k7c4	na
německou	německý	k2eAgFnSc4d1	německá
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
službu	služba	k1gFnSc4	služba
(	(	kIx(	(
<g/>
Abwehr	Abwehr	k1gInSc4	Abwehr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedenou	vedený	k2eAgFnSc4d1	vedená
admirálem	admirál	k1gMnSc7	admirál
Canarisem	Canaris	k1gInSc7	Canaris
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
gestapo	gestapo	k1gNnSc1	gestapo
zastavilo	zastavit	k5eAaPmAgNnS	zastavit
činnost	činnost	k1gFnSc4	činnost
poslední	poslední	k2eAgFnSc2d1	poslední
fungující	fungující	k2eAgFnSc2d1	fungující
buňky	buňka	k1gFnSc2	buňka
ilegálního	ilegální	k2eAgNnSc2d1	ilegální
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
mladých	mladý	k2eAgMnPc2d1	mladý
bohoslovců	bohoslovec	k1gMnPc2	bohoslovec
Vyznávající	vyznávající	k2eAgFnSc2d1	vyznávající
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sběrný	sběrný	k2eAgInSc1d1	sběrný
vikariát	vikariát	k1gInSc1	vikariát
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Sammelvikariat	Sammelvikariat	k1gInSc1	Sammelvikariat
<g/>
)	)	kIx)	)
v	v	k7c6	v
Sigurdshofu	Sigurdshof	k1gInSc6	Sigurdshof
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
rozpustilo	rozpustit	k5eAaPmAgNnS	rozpustit
Bonhoefferem	Bonhoeffer	k1gInSc7	Bonhoeffer
vedené	vedený	k2eAgNnSc1d1	vedené
soustředění	soustředění	k1gNnSc1	soustředění
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
Osterem	Oster	k1gMnSc7	Oster
a	a	k8xC	a
Hansem	Hans	k1gMnSc7	Hans
von	von	k1gInSc1	von
Dohnanyim	Dohnanyima	k1gFnPc2	Dohnanyima
o	o	k7c6	o
"	"	kIx"	"
<g/>
nezbytném	nezbytný	k2eAgInSc6d1	nezbytný
postoji	postoj	k1gInSc6	postoj
<g/>
"	"	kIx"	"
ohledně	ohledně	k7c2	ohledně
úkolů	úkol	k1gInPc2	úkol
abwehru	abwehra	k1gFnSc4	abwehra
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
využít	využít	k5eAaPmF	využít
svých	svůj	k3xOyFgInPc2	svůj
ekumenických	ekumenický	k2eAgInPc2d1	ekumenický
kontaktů	kontakt	k1gInPc2	kontakt
pro	pro	k7c4	pro
zájmy	zájem	k1gInPc4	zájem
spiklenců	spiklenec	k1gMnPc2	spiklenec
a	a	k8xC	a
zahájit	zahájit	k5eAaPmF	zahájit
vyjednávání	vyjednávání	k1gNnSc3	vyjednávání
se	s	k7c7	s
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nepodílel	podílet	k5eNaImAgMnS	podílet
na	na	k7c4	na
plánování	plánování	k1gNnSc4	plánování
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xC	jako
spojka	spojka	k1gFnSc1	spojka
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
abwehru	abwehra	k1gFnSc4	abwehra
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc2	svůj
"	"	kIx"	"
<g/>
podvratné	podvratný	k2eAgFnSc2d1	podvratná
<g/>
"	"	kIx"	"
činnosti	činnost	k1gFnSc2	činnost
obdržel	obdržet	k5eAaPmAgInS	obdržet
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
zákaz	zákaz	k1gInSc4	zákaz
veřejných	veřejný	k2eAgInPc2d1	veřejný
proslovů	proslov	k1gInPc2	proslov
platný	platný	k2eAgMnSc1d1	platný
"	"	kIx"	"
<g/>
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1941	[number]	k4	1941
pak	pak	k6eAd1	pak
obdobný	obdobný	k2eAgInSc4d1	obdobný
zákaz	zákaz	k1gInSc4	zákaz
písemných	písemný	k2eAgInPc2d1	písemný
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domě	dům	k1gInSc6	dům
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
se	se	k3xPyFc4	se
setkávalo	setkávat	k5eAaImAgNnS	setkávat
několik	několik	k4yIc1	několik
odpůrců	odpůrce	k1gMnPc2	odpůrce
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hodlali	hodlat	k5eAaImAgMnP	hodlat
na	na	k7c6	na
spáchat	spáchat	k5eAaPmF	spáchat
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Adolfa	Adolf	k1gMnSc4	Adolf
Hitlera	Hitler	k1gMnSc4	Hitler
a	a	k8xC	a
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
zastávali	zastávat	k5eAaImAgMnP	zastávat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
pozice	pozice	k1gFnPc4	pozice
v	v	k7c4	v
abwehru	abwehra	k1gFnSc4	abwehra
nebo	nebo	k8xC	nebo
wehrmachtu	wehrmacht	k1gInSc6	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
odbojové	odbojový	k2eAgFnSc3d1	odbojová
skupině	skupina	k1gFnSc3	skupina
připojil	připojit	k5eAaPmAgInS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
zabití	zabití	k1gNnSc1	zabití
tyrana	tyran	k1gMnSc2	tyran
(	(	kIx(	(
<g/>
Smí	smět	k5eAaImIp3nS	smět
křesťan	křesťan	k1gMnSc1	křesťan
porušit	porušit	k5eAaPmF	porušit
přikázání	přikázání	k1gNnSc4	přikázání
"	"	kIx"	"
<g/>
nezabiješ	zabít	k5eNaPmIp2nS	zabít
<g/>
"	"	kIx"	"
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
případě	případ	k1gInSc6	případ
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
ano	ano	k9	ano
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
teologicko-etického	teologickotický	k2eAgNnSc2d1	teologicko-etický
hlediska	hledisko	k1gNnSc2	hledisko
reflektována	reflektován	k2eAgFnSc1d1	reflektována
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
nedokončeném	dokončený	k2eNgNnSc6d1	nedokončené
stěžejním	stěžejní	k2eAgNnSc6d1	stěžejní
díle	dílo	k1gNnSc6	dílo
Etika	etik	k1gMnSc2	etik
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vznikalo	vznikat	k5eAaImAgNnS	vznikat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
až	až	k9	až
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
zatčení	zatčení	k1gNnSc2	zatčení
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1943	[number]	k4	1943
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
aktivitou	aktivita	k1gFnSc7	aktivita
ve	v	k7c6	v
vojensko-politickém	vojenskoolitický	k2eAgInSc6d1	vojensko-politický
odboji	odboj	k1gInSc6	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgMnS	být
Bonhoeffer	Bonhoeffer	k1gInSc4	Bonhoeffer
přiřazen	přiřazen	k2eAgInSc4d1	přiřazen
ke	k	k7c3	k
služebně	služebna	k1gFnSc3	služebna
abwehru	abwehra	k1gFnSc4	abwehra
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
nacistického	nacistický	k2eAgInSc2d1	nacistický
státu	stát	k1gInSc2	stát
a	a	k8xC	a
současně	současně	k6eAd1	současně
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
zakazovány	zakazován	k2eAgInPc4d1	zakazován
ústní	ústní	k2eAgInPc4d1	ústní
i	i	k8xC	i
písemné	písemný	k2eAgInPc4d1	písemný
projevy	projev	k1gInPc4	projev
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
publikační	publikační	k2eAgFnSc4d1	publikační
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
zdržoval	zdržovat	k5eAaImAgMnS	zdržovat
v	v	k7c6	v
benediktinském	benediktinský	k2eAgInSc6d1	benediktinský
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Ettalu	Ettal	k1gInSc6	Ettal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1941	[number]	k4	1941
a	a	k8xC	a
1942	[number]	k4	1942
podnikl	podniknout	k5eAaPmAgInS	podniknout
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
německé	německý	k2eAgFnSc2d1	německá
(	(	kIx(	(
<g/>
kontra	kontra	k2eAgFnSc2d1	kontra
<g/>
)	)	kIx)	)
<g/>
rozvědky	rozvědka	k1gFnSc2	rozvědka
(	(	kIx(	(
<g/>
Spionageabwehr	Spionageabwehr	k1gInSc1	Spionageabwehr
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
užšího	úzký	k2eAgInSc2d2	užší
odbojového	odbojový	k2eAgInSc2d1	odbojový
kroužku	kroužek	k1gInSc2	kroužek
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
s	s	k7c7	s
Helmuthem	Helmuth	k1gInSc7	Helmuth
von	von	k1gInSc1	von
Moltke	Moltke	k1gFnPc2	Moltke
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sigtuně	Sigtuna	k1gFnSc6	Sigtuna
a	a	k8xC	a
Stockholmu	Stockholm	k1gInSc2	Stockholm
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
Georgem	Georg	k1gInSc7	Georg
Bellem	bell	k1gInSc7	bell
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
mu	on	k3xPp3gMnSc3	on
tajné	tajný	k2eAgInPc1d1	tajný
dokumenty	dokument	k1gInPc1	dokument
o	o	k7c6	o
odbojové	odbojový	k2eAgFnSc6d1	odbojová
skupině	skupina	k1gFnSc6	skupina
a	a	k8xC	a
o	o	k7c6	o
jejích	její	k3xOp3gInPc6	její
cílech	cíl	k1gInPc6	cíl
<g/>
,	,	kIx,	,
adresované	adresovaný	k2eAgFnSc3d1	adresovaná
britské	britský	k2eAgFnSc3d1	britská
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
požádal	požádat	k5eAaPmAgMnS	požádat
spojence	spojenec	k1gMnSc4	spojenec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
veřejně	veřejně	k6eAd1	veřejně
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
nebude	být	k5eNaImBp3nS	být
klást	klást	k5eAaImF	klást
rovnítko	rovnítko	k1gNnSc4	rovnítko
mezi	mezi	k7c7	mezi
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Anthony	Anthona	k1gFnSc2	Anthona
Eden	Eden	k1gInSc1	Eden
ale	ale	k8xC	ale
Bellovi	Bell	k1gMnSc3	Bell
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
pouhá	pouhý	k2eAgFnSc1d1	pouhá
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
podpora	podpora	k1gFnSc1	podpora
odboje	odboj	k1gInSc2	odboj
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
zájmu	zájem	k1gInSc6	zájem
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
činnosti	činnost	k1gFnPc4	činnost
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
abwehru	abwehra	k1gFnSc4	abwehra
skoro	skoro	k6eAd1	skoro
vůbec	vůbec	k9	vůbec
nevyjadřoval	vyjadřovat	k5eNaImAgMnS	vyjadřovat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInPc4	jeho
spisy	spis	k1gInPc4	spis
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Etika	etika	k1gFnSc1	etika
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
číst	číst	k5eAaImF	číst
též	též	k9	též
jako	jako	k9	jako
nepřímá	přímý	k2eNgNnPc1d1	nepřímé
svědectví	svědectví	k1gNnPc1	svědectví
a	a	k8xC	a
reflexe	reflexe	k1gFnSc1	reflexe
této	tento	k3xDgFnSc2	tento
jeho	jeho	k3xOp3gFnSc2	jeho
provázanosti	provázanost	k1gFnSc2	provázanost
s	s	k7c7	s
dramatickým	dramatický	k2eAgNnSc7d1	dramatické
politickým	politický	k2eAgNnSc7d1	politické
děním	dění	k1gNnSc7	dění
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1942	[number]	k4	1942
a	a	k8xC	a
1943	[number]	k4	1943
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
napsal	napsat	k5eAaBmAgMnS	napsat
velmi	velmi	k6eAd1	velmi
osobní	osobní	k2eAgNnPc4d1	osobní
ohlédnutí	ohlédnutí	k1gNnPc4	ohlédnutí
za	za	k7c7	za
uplynulými	uplynulý	k2eAgInPc7d1	uplynulý
deseti	deset	k4xCc7	deset
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
dozrával	dozrávat	k5eAaImAgInS	dozrávat
jeho	on	k3xPp3gInSc4	on
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
nacistickému	nacistický	k2eAgInSc3d1	nacistický
teroru	teror	k1gInSc3	teror
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
promýšlet	promýšlet	k5eAaImF	promýšlet
svůj	svůj	k3xOyFgInSc4	svůj
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
životní	životní	k2eAgInSc4d1	životní
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
otázkou	otázka	k1gFnSc7	otázka
osobní	osobní	k2eAgFnSc2d1	osobní
odvahy	odvaha	k1gFnSc2	odvaha
<g/>
,	,	kIx,	,
poctivosti	poctivost	k1gFnSc2	poctivost
a	a	k8xC	a
"	"	kIx"	"
<g/>
pohledem	pohled	k1gInSc7	pohled
zdola	zdola	k6eAd1	zdola
<g/>
"	"	kIx"	"
z	z	k7c2	z
perspektivy	perspektiva	k1gFnSc2	perspektiva
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgMnS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
osvojit	osvojit	k5eAaPmF	osvojit
ctnosti	ctnost	k1gFnPc4	ctnost
všedního	všední	k2eAgInSc2d1	všední
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
Na	na	k7c6	na
předním	přední	k2eAgNnSc6d1	přední
místě	místo	k1gNnSc6	místo
stálo	stát	k5eAaImAgNnS	stát
jeho	jeho	k3xOp3gNnSc1	jeho
osobní	osobní	k2eAgNnSc1d1	osobní
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
<g/>
:	:	kIx,	:
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1943	[number]	k4	1943
se	se	k3xPyFc4	se
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
von	von	k1gInSc4	von
Wedemeyer	Wedemeyer	k1gInSc1	Wedemeyer
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
pomořanského	pomořanský	k2eAgMnSc4d1	pomořanský
statkáře	statkář	k1gMnSc4	statkář
<g/>
.	.	kIx.	.
</s>
<s>
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
kdysi	kdysi	k6eAd1	kdysi
vedl	vést	k5eAaImAgMnS	vést
konfirmaci	konfirmace	k1gFnSc4	konfirmace
jejího	její	k3xOp3gMnSc2	její
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
babička	babička	k1gFnSc1	babička
Ruth	Ruth	k1gFnSc1	Ruth
von	von	k1gInSc4	von
Kleist-Retzow	Kleist-Retzow	k1gFnSc2	Kleist-Retzow
byla	být	k5eAaImAgFnS	být
Bonhoefferovou	Bonhoefferův	k2eAgFnSc7d1	Bonhoefferova
mecenáškou	mecenáška	k1gFnSc7	mecenáška
a	a	k8xC	a
podporovatelkou	podporovatelka	k1gFnSc7	podporovatelka
ještě	ještě	k9	ještě
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
kazatelského	kazatelský	k2eAgInSc2d1	kazatelský
semináře	seminář	k1gInSc2	seminář
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
sběrného	sběrný	k2eAgInSc2d1	sběrný
vikariátu	vikariát	k1gInSc2	vikariát
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
uskupení	uskupení	k1gNnPc2	uskupení
kolem	kolem	k7c2	kolem
Canarise	Canarise	k1gFnSc2	Canarise
<g/>
,	,	kIx,	,
Ostera	Oster	k1gMnSc2	Oster
a	a	k8xC	a
Klause	Klaus	k1gMnSc2	Klaus
Bonhoeffera	Bonhoeffero	k1gNnSc2	Bonhoeffero
provedli	provést	k5eAaPmAgMnP	provést
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
neúspěšné	úspěšný	k2eNgInPc4d1	neúspěšný
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Adolfa	Adolf	k1gMnSc4	Adolf
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
byl	být	k5eAaImAgMnS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
švagrem	švagr	k1gMnSc7	švagr
Hansem	Hans	k1gMnSc7	Hans
von	von	k1gInSc4	von
Dohnanyim	Dohnanyim	k1gInSc4	Dohnanyim
zatčen	zatknout	k5eAaPmNgMnS	zatknout
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
rozvracení	rozvracení	k1gNnSc4	rozvracení
branných	branný	k2eAgFnPc2d1	Branná
sil	síla	k1gFnPc2	síla
<g/>
"	"	kIx"	"
a	a	k8xC	a
držen	držet	k5eAaImNgInS	držet
ve	v	k7c6	v
vazební	vazební	k2eAgFnSc6d1	vazební
věznici	věznice	k1gFnSc6	věznice
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
v	v	k7c6	v
Tegelu	Tegel	k1gInSc6	Tegel
<g/>
.	.	kIx.	.
</s>
<s>
Žalobu	žaloba	k1gFnSc4	žaloba
připravil	připravit	k5eAaPmAgMnS	připravit
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
1943	[number]	k4	1943
dokončil	dokončit	k5eAaPmAgMnS	dokončit
soudce	soudce	k1gMnSc1	soudce
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
Manfeld	Manfeld	k1gMnSc1	Manfeld
Roeder	Roeder	k1gMnSc1	Roeder
(	(	kIx(	(
<g/>
obžalovací	obžalovací	k2eAgInSc1d1	obžalovací
spis	spis	k1gInSc1	spis
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
objeven	objevit	k5eAaPmNgMnS	objevit
ve	v	k7c6	v
Vojenském	vojenský	k2eAgInSc6d1	vojenský
historickém	historický	k2eAgInSc6d1	historický
archivu	archiv	k1gInSc6	archiv
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zamýšlené	zamýšlený	k2eAgNnSc4d1	zamýšlené
trestní	trestní	k2eAgNnSc4d1	trestní
řízení	řízení	k1gNnSc4	řízení
proti	proti	k7c3	proti
Bonhoefferovi	Bonhoeffer	k1gMnSc3	Bonhoeffer
před	před	k7c7	před
Lidovým	lidový	k2eAgInSc7d1	lidový
soudním	soudní	k2eAgInSc7d1	soudní
dvorem	dvůr	k1gInSc7	dvůr
(	(	kIx(	(
<g/>
Volksgerichtshof	Volksgerichtshof	k1gInSc1	Volksgerichtshof
<g/>
)	)	kIx)	)
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
řízení	řízení	k1gNnSc4	řízení
mohli	moct	k5eAaImAgMnP	moct
odkládat	odkládat	k5eAaImF	odkládat
vyšší	vysoký	k2eAgMnPc1d2	vyšší
úředníci	úředník	k1gMnPc1	úředník
s	s	k7c7	s
napojením	napojení	k1gNnSc7	napojení
na	na	k7c4	na
odbojové	odbojový	k2eAgFnPc4d1	odbojová
skupiny	skupina	k1gFnPc4	skupina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vojenský	vojenský	k2eAgMnSc1d1	vojenský
soudce	soudce	k1gMnSc1	soudce
Karl	Karl	k1gMnSc1	Karl
Sack	Sack	k1gMnSc1	Sack
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
se	se	k3xPyFc4	se
Claus	Claus	k1gMnSc1	Claus
Schenk	Schenk	k1gMnSc1	Schenk
hrabě	hrabě	k1gMnSc1	hrabě
von	von	k1gInSc4	von
Stauffenberg	Stauffenberg	k1gMnSc1	Stauffenberg
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
stačilo	stačit	k5eAaBmAgNnS	stačit
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
atentát	atentát	k1gInSc1	atentát
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgInS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následných	následný	k2eAgInPc6d1	následný
intenzivních	intenzivní	k2eAgInPc6d1	intenzivní
výsleších	výslech	k1gInPc6	výslech
nebylo	být	k5eNaImAgNnS	být
gestapo	gestapo	k1gNnSc4	gestapo
schopné	schopný	k2eAgFnSc2d1	schopná
Bonhoefferovi	Bonhoefferův	k2eAgMnPc1d1	Bonhoefferův
ani	ani	k8xC	ani
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
skupiny	skupina	k1gFnSc2	skupina
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měli	mít	k5eAaImAgMnP	mít
se	s	k7c7	s
Stauffenbergovou	Stauffenbergův	k2eAgFnSc7d1	Stauffenbergova
akcí	akce	k1gFnSc7	akce
cokoli	cokoli	k3yInSc4	cokoli
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
gestapo	gestapo	k1gNnSc1	gestapo
náhodou	náhodou	k6eAd1	náhodou
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
nalezlo	naleznout	k5eAaPmAgNnS	naleznout
v	v	k7c6	v
bunkru	bunkr	k1gInSc6	bunkr
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
v	v	k7c4	v
Zossenu	Zossen	k2eAgFnSc4d1	Zossen
dokumenty	dokument	k1gInPc4	dokument
tajného	tajný	k2eAgInSc2d1	tajný
archivu	archiv	k1gInSc2	archiv
abwehru	abwehra	k1gFnSc4	abwehra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
doklady	doklad	k1gInPc4	doklad
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
Canaris	Canaris	k1gInSc1	Canaris
<g/>
,	,	kIx,	,
i	i	k9	i
několik	několik	k4yIc4	několik
stran	strana	k1gFnPc2	strana
Canarisova	Canarisův	k2eAgInSc2d1	Canarisův
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
tam	tam	k6eAd1	tam
gestapo	gestapo	k1gNnSc1	gestapo
našlo	najít	k5eAaPmAgNnS	najít
pečlivé	pečlivý	k2eAgInPc4d1	pečlivý
zápisy	zápis	k1gInPc4	zápis
o	o	k7c6	o
zločinech	zločin	k1gInPc6	zločin
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
je	být	k5eAaImIp3nS	být
kdysi	kdysi	k6eAd1	kdysi
Dohnanyi	Dohnanye	k1gFnSc4	Dohnanye
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
mohli	moct	k5eAaImAgMnP	moct
němečtí	německý	k2eAgMnPc1d1	německý
občané	občan	k1gMnPc1	občan
i	i	k8xC	i
spojenci	spojenec	k1gMnPc1	spojenec
dozvědět	dozvědět	k5eAaPmF	dozvědět
více	hodně	k6eAd2	hodně
o	o	k7c6	o
nacistických	nacistický	k2eAgInPc6d1	nacistický
zločinech	zločin	k1gInPc6	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
dokumenty	dokument	k1gInPc7	dokument
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
také	také	k9	také
ospravedlněn	ospravedlněn	k2eAgInSc4d1	ospravedlněn
odboj	odboj	k1gInSc4	odboj
proti	proti	k7c3	proti
Hitlerovi	Hitler	k1gMnSc3	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Dohnanyi	Dohnanyi	k6eAd1	Dohnanyi
tyto	tento	k3xDgInPc4	tento
papíry	papír	k1gInPc4	papír
skrýval	skrývat	k5eAaImAgInS	skrývat
v	v	k7c6	v
trezoru	trezor	k1gInSc6	trezor
své	svůj	k3xOyFgFnSc2	svůj
kanceláře	kancelář	k1gFnSc2	kancelář
v	v	k7c6	v
centrále	centrála	k1gFnSc6	centrála
abwehru	abwehra	k1gFnSc4	abwehra
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
je	být	k5eAaImIp3nS	být
nechával	nechávat	k5eAaImAgMnS	nechávat
přenášet	přenášet	k5eAaImF	přenášet
do	do	k7c2	do
tajného	tajný	k2eAgInSc2d1	tajný
archivu	archiv	k1gInSc2	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
nálezu	nález	k1gInSc6	nález
už	už	k9	už
důkazy	důkaz	k1gInPc1	důkaz
nepopiratelně	popiratelně	k6eNd1	popiratelně
svědčily	svědčit	k5eAaImAgInP	svědčit
proti	proti	k7c3	proti
odbojovém	odbojový	k2eAgNnSc6d1	odbojové
uskupení	uskupení	k1gNnSc6	uskupení
uvnitř	uvnitř	k6eAd1	uvnitř
abwehru	abwehra	k1gFnSc4	abwehra
a	a	k8xC	a
především	především	k6eAd1	především
i	i	k9	i
proti	proti	k7c3	proti
Dohnanyimu	Dohnanyim	k1gMnSc3	Dohnanyim
a	a	k8xC	a
Bonhoefferovi	Bonhoeffer	k1gMnSc3	Bonhoeffer
<g/>
.	.	kIx.	.
</s>
<s>
Gestapo	gestapo	k1gNnSc4	gestapo
pak	pak	k6eAd1	pak
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
Bonhoeffera	Bonhoeffero	k1gNnPc4	Bonhoeffero
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
své	svůj	k3xOyFgFnSc2	svůj
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
centrály	centrála	k1gFnSc2	centrála
v	v	k7c6	v
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
Prinz-Albrecht-Straße	Prinz-Albrecht-Straße	k1gFnSc6	Prinz-Albrecht-Straße
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
přebývali	přebývat	k5eAaImAgMnP	přebývat
Bonhoeffer	Bonhoeffer	k1gInSc4	Bonhoeffer
<g/>
,	,	kIx,	,
Canaris	Canaris	k1gFnSc4	Canaris
<g/>
,	,	kIx,	,
Dohnanyi	Dohnanye	k1gFnSc4	Dohnanye
<g/>
,	,	kIx,	,
Gehre	Gehr	k1gMnSc5	Gehr
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
Oster	Oster	k1gMnSc1	Oster
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
taktéž	taktéž	k?	taktéž
zatčený	zatčený	k1gMnSc1	zatčený
Karl	Karl	k1gMnSc1	Karl
Sack	Sack	k1gMnSc1	Sack
jako	jako	k8xC	jako
osobní	osobní	k2eAgMnPc1d1	osobní
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
vězni	vězeň	k1gMnPc1	vězeň
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
dopis	dopis	k1gInSc1	dopis
rodičům	rodič	k1gMnPc3	rodič
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
napsal	napsat	k5eAaBmAgInS	napsat
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Buchenwald	Buchenwald	k1gInSc1	Buchenwald
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Flossenbürg	Flossenbürg	k1gMnSc1	Flossenbürg
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Hitler	Hitler	k1gMnSc1	Hitler
popravu	poprava	k1gFnSc4	poprava
všech	všecek	k3xTgNnPc2	všecek
ještě	ještě	k6eAd1	ještě
nepopravených	popravený	k2eNgMnPc2d1	popravený
"	"	kIx"	"
<g/>
spiklenců	spiklenec	k1gMnPc2	spiklenec
<g/>
"	"	kIx"	"
spojených	spojený	k2eAgMnPc2d1	spojený
s	s	k7c7	s
atentátem	atentát	k1gInSc7	atentát
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
zpečetěn	zpečetěn	k2eAgInSc4d1	zpečetěn
i	i	k8xC	i
Bonhoefferův	Bonhoefferův	k2eAgInSc4d1	Bonhoefferův
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
převezen	převézt	k5eAaPmNgInS	převézt
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
ve	v	k7c6	v
Flossenbürgu	Flossenbürg	k1gInSc6	Flossenbürg
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
tušil	tušit	k5eAaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
čeká	čekat	k5eAaImIp3nS	čekat
poprava	poprava	k1gFnSc1	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
nato	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
společně	společně	k6eAd1	společně
s	s	k7c7	s
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Canarisem	Canaris	k1gInSc7	Canaris
<g/>
,	,	kIx,	,
Hansem	Hans	k1gMnSc7	Hans
Osterem	Oster	k1gMnSc7	Oster
<g/>
,	,	kIx,	,
Karlem	Karel	k1gMnSc7	Karel
Sackem	Sacek	k1gMnSc7	Sacek
a	a	k8xC	a
Ludwigem	Ludwig	k1gMnSc7	Ludwig
Gehrem	Gehr	k1gMnSc7	Gehr
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
procesu	proces	k1gInSc6	proces
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Žalobcem	žalobce	k1gMnSc7	žalobce
byl	být	k5eAaImAgMnS	být
vysoce	vysoce	k6eAd1	vysoce
postavený	postavený	k2eAgMnSc1d1	postavený
funkcionář	funkcionář	k1gMnSc1	funkcionář
Hlavního	hlavní	k2eAgInSc2d1	hlavní
říšského	říšský	k2eAgInSc2d1	říšský
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
úřadu	úřad	k1gInSc2	úřad
(	(	kIx(	(
<g/>
Reichssicherheitshauptamt	Reichssicherheitshauptamt	k1gInSc1	Reichssicherheitshauptamt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
oddělení	oddělení	k1gNnSc2	oddělení
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
praporu	prapor	k1gInSc2	prapor
SS	SS	kA	SS
Walter	Walter	k1gMnSc1	Walter
Huppenkothen	Huppenkothen	k2eAgMnSc1d1	Huppenkothen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
procesu	proces	k1gInSc6	proces
odsoudit	odsoudit	k5eAaPmF	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
Hanse	Hans	k1gMnSc2	Hans
Dohnanyiho	Dohnanyi	k1gMnSc2	Dohnanyi
<g/>
,	,	kIx,	,
Bonhoefferova	Bonhoefferův	k2eAgMnSc2d1	Bonhoefferův
švagra	švagr	k1gMnSc2	švagr
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
loutkovému	loutkový	k2eAgInSc3d1	loutkový
procesu	proces	k1gInSc3	proces
proti	proti	k7c3	proti
Bonhoefferovi	Bonhoeffer	k1gMnSc3	Bonhoeffer
a	a	k8xC	a
jiným	jiný	k2eAgMnPc3d1	jiný
odpůrcům	odpůrce	k1gMnPc3	odpůrce
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
předsedal	předsedat	k5eAaImAgMnS	předsedat
Otto	Otto	k1gMnSc1	Otto
Thorbeck	Thorbeck	k1gMnSc1	Thorbeck
<g/>
,	,	kIx,	,
Huppenkothenův	Huppenkothenův	k2eAgMnSc1d1	Huppenkothenův
podřízený	podřízený	k1gMnSc1	podřízený
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
soudce	soudce	k1gMnSc1	soudce
při	při	k7c6	při
policejním	policejní	k2eAgInSc6d1	policejní
soudu	soud	k1gInSc6	soud
a	a	k8xC	a
soudu	soud	k1gInSc3	soud
SS	SS	kA	SS
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Přísedícím	přísedící	k1gMnSc7	přísedící
byl	být	k5eAaImAgMnS	být
velitel	velitel	k1gMnSc1	velitel
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Flossenbürg	Flossenbürg	k1gMnSc1	Flossenbürg
Max	Max	k1gMnSc1	Max
Koegel	Koegel	k1gMnSc1	Koegel
a	a	k8xC	a
ještě	ještě	k9	ještě
jeden	jeden	k4xCgMnSc1	jeden
neznámý	známý	k2eNgMnSc1d1	neznámý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Obhájci	obhájce	k1gMnPc1	obhájce
ani	ani	k8xC	ani
svědci	svědek	k1gMnPc1	svědek
nebyli	být	k5eNaImAgMnP	být
předvoláni	předvolat	k5eAaPmNgMnP	předvolat
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
probíhalo	probíhat	k5eAaImAgNnS	probíhat
bez	bez	k7c2	bez
zapisovatele	zapisovatel	k1gMnSc2	zapisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
nový	nový	k2eAgInSc1d1	nový
spis	spis	k1gInSc1	spis
<g/>
.	.	kIx.	.
</s>
<s>
Procesní	procesní	k2eAgInPc4d1	procesní
spisy	spis	k1gInPc4	spis
proti	proti	k7c3	proti
Bonhoefferovi	Bonhoeffer	k1gMnSc3	Bonhoeffer
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
shořely	shořet	k5eAaPmAgInP	shořet
během	během	k7c2	během
bombardování	bombardování	k1gNnSc2	bombardování
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
posuzovány	posuzovat	k5eAaImNgFnP	posuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
soudu	soud	k1gInSc2	soud
nebyli	být	k5eNaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
žádní	žádný	k3yNgMnPc1	žádný
svědci	svědek	k1gMnPc1	svědek
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
pak	pak	k6eAd1	pak
Thorbeck	Thorbeck	k1gInSc4	Thorbeck
s	s	k7c7	s
Huppenkothenem	Huppenkothen	k1gInSc7	Huppenkothen
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
proces	proces	k1gInSc1	proces
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
právně	právně	k6eAd1	právně
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popraviště	popraviště	k1gNnSc4	popraviště
byl	být	k5eAaImAgInS	být
Dietrich	Dietrich	k1gInSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gInSc1	Bonhoeffer
veden	veden	k2eAgInSc1d1	veden
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
za	za	k7c4	za
svítání	svítání	k1gNnSc4	svítání
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
úplně	úplně	k6eAd1	úplně
svléct	svléct	k5eAaPmF	svléct
a	a	k8xC	a
jít	jít	k5eAaImF	jít
k	k	k7c3	k
šibenici	šibenice	k1gFnSc3	šibenice
nazí	nahý	k2eAgMnPc1d1	nahý
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnSc1d1	místní
táborový	táborový	k2eAgMnSc1d1	táborový
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
celou	celý	k2eAgFnSc4d1	celá
scénu	scéna	k1gFnSc4	scéna
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
<g/>
,	,	kIx,	,
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
podal	podat	k5eAaPmAgMnS	podat
písemné	písemný	k2eAgNnSc4d1	písemné
svědectví	svědectví	k1gNnSc4	svědectví
<g/>
:	:	kIx,	:
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
neznal	neznat	k5eAaImAgMnS	neznat
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
klidně	klidně	k6eAd1	klidně
a	a	k8xC	a
soustředěně	soustředěně	k6eAd1	soustředěně
<g/>
,	,	kIx,	,
rozloučil	rozloučit	k5eAaPmAgMnS	rozloučit
se	se	k3xPyFc4	se
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
spoluvězni	spoluvězeň	k1gMnPc7	spoluvězeň
<g/>
,	,	kIx,	,
na	na	k7c6	na
popravišti	popraviště	k1gNnSc6	popraviště
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
pomodlil	pomodlit	k5eAaPmAgMnS	pomodlit
<g/>
,	,	kIx,	,
k	k	k7c3	k
šibenici	šibenice	k1gFnSc3	šibenice
kráčel	kráčet	k5eAaImAgInS	kráčet
smířeně	smířeně	k6eAd1	smířeně
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
během	během	k7c2	během
několika	několik	k4yIc2	několik
vteřin	vteřina	k1gFnPc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
byl	být	k5eAaImAgMnS	být
svébytným	svébytný	k2eAgMnSc7d1	svébytný
teologem	teolog	k1gMnSc7	teolog
a	a	k8xC	a
křesťanským	křesťanský	k2eAgMnSc7d1	křesťanský
etikem	etik	k1gMnSc7	etik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
význam	význam	k1gInSc4	význam
Kázání	kázání	k1gNnSc2	kázání
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
Následování	následování	k1gNnSc6	následování
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
svým	svůj	k3xOyFgInSc7	svůj
osobním	osobní	k2eAgInSc7d1	osobní
příkladem	příklad	k1gInSc7	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dopisech	dopis	k1gInPc6	dopis
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
(	(	kIx(	(
<g/>
posmrtně	posmrtně	k6eAd1	posmrtně
vydáno	vydat	k5eAaPmNgNnS	vydat
jako	jako	k8xC	jako
Widerstand	Widerstand	k1gInSc1	Widerstand
und	und	k?	und
Ergebung	Ergebung	k1gInSc1	Ergebung
<g/>
)	)	kIx)	)
nastínil	nastínit	k5eAaPmAgInS	nastínit
svoji	svůj	k3xOyFgFnSc4	svůj
vizi	vize	k1gFnSc4	vize
budoucího	budoucí	k2eAgMnSc2d1	budoucí
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
i	i	k8xC	i
nenáboženské	náboženský	k2eNgFnSc2d1	nenáboženská
interpretace	interpretace	k1gFnSc2	interpretace
Bible	bible	k1gFnSc2	bible
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
populární	populární	k2eAgInSc1d1	populární
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
evangelickými	evangelický	k2eAgMnPc7d1	evangelický
teology	teolog	k1gMnPc7	teolog
Nové	Nové	k2eAgFnSc2d1	Nové
orientace	orientace	k1gFnSc2	orientace
či	či	k8xC	či
civilní	civilní	k2eAgFnSc2d1	civilní
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yRnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
odmítána	odmítat	k5eAaImNgFnS	odmítat
a	a	k8xC	a
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
<g/>
.	.	kIx.	.
***	***	k?	***
A	a	k9	a
dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
Starokatolická	starokatolický	k2eAgFnSc1d1	Starokatolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
připomíná	připomínat	k5eAaImIp3nS	připomínat
(	(	kIx(	(
<g/>
nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
mučedníka	mučedník	k1gMnSc2	mučedník
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
www.getsemany.cz	www.getsemany.cz	k1gInSc1	www.getsemany.cz
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7017	[number]	k4	7017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Kalich	kalich	k1gInSc1	kalich
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Vik	Vik	k1gMnSc1	Vik
Následování	následování	k1gNnPc2	následování
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
Kázání	kázání	k1gNnSc2	kázání
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kalich	kalich	k1gInSc1	kalich
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
F.	F.	kA	F.
M.	M.	kA	M.
Dobiáš	Dobiáš	k1gMnSc1	Dobiáš
a	a	k8xC	a
A.	A.	kA	A.
Molnár	Molnár	k1gInSc1	Molnár
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Následování	následování	k1gNnSc1	následování
(	(	kIx(	(
<g/>
úplné	úplný	k2eAgNnSc1d1	úplné
vydání	vydání	k1gNnSc1	vydání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kalich	kalich	k1gInSc1	kalich
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Vik	Vik	k1gMnSc1	Vik
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Sanctorum	Sanctorum	k1gInSc1	Sanctorum
communio	communio	k1gNnSc1	communio
(	(	kIx(	(
<g/>
zur	zur	k?	zur
Soziologie	Soziologie	k1gFnSc1	Soziologie
der	drát	k5eAaImRp2nS	drát
Kirche	Kirchus	k1gMnSc5	Kirchus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-374-00002-9	[number]	k4	3-374-00002-9
a	a	k8xC	a
ISBN	ISBN	kA	ISBN
3-374-00311-7	[number]	k4	3-374-00311-7
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
:	:	kIx,	:
Listy	lista	k1gFnPc1	lista
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
(	(	kIx(	(
<g/>
Widerstand	Widerstand	k1gInSc1	Widerstand
und	und	k?	und
Ergebung	Ergebung	k1gInSc1	Ergebung
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Miloš	Miloš	k1gMnSc1	Miloš
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Heryán	Heryán	k1gMnSc1	Heryán
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Šimsa	Šimsa	k1gFnSc1	Šimsa
<g/>
,	,	kIx,	,
verše	verš	k1gInPc1	verš
Václav	Václava	k1gFnPc2	Václava
Renč	Renč	k?	Renč
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgInS	vydat
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7021-081-8	[number]	k4	80-7021-081-8
Bonhoeffer	Bonhoeffer	k1gInSc4	Bonhoeffer
<g/>
:	:	kIx,	:
Agent	agent	k1gMnSc1	agent
of	of	k?	of
Grace	Grace	k1gMnSc1	Grace
</s>
