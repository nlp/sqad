<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
centrem	centrum	k1gNnSc7	centrum
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
do	do	k7c2	do
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
třetím	třetí	k4xOgMnSc6	třetí
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
zemí	zem	k1gFnSc7	zem
České	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
(	(	kIx(	(
<g/>
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
)	)	kIx)	)
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
