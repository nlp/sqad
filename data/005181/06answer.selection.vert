<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
nejrychlejší	rychlý	k2eAgFnSc4d3	nejrychlejší
rotaci	rotace	k1gFnSc4	rotace
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
otočku	otočka	k1gFnSc4	otočka
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
vyklenutí	vyklenutí	k1gNnSc4	vyklenutí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
viditelná	viditelný	k2eAgFnSc1d1	viditelná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
i	i	k9	i
amatérskými	amatérský	k2eAgInPc7d1	amatérský
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
.	.	kIx.	.
</s>
