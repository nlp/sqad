<s>
Prokletí	prokletý	k2eAgMnPc1d1	prokletý
básníci	básník	k1gMnPc1	básník
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
les	les	k1gInSc1	les
poétes	poétesa	k1gFnPc2	poétesa
maudits	maudits	k6eAd1	maudits
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
francouzských	francouzský	k2eAgMnPc2d1	francouzský
nekonformních	konformní	k2eNgMnPc2d1	nekonformní
básníků	básník	k1gMnPc2	básník
poslední	poslední	k2eAgFnSc2d1	poslední
třetiny	třetina	k1gFnSc2	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
užité	užitý	k2eAgFnSc2d1	užitá
Paulem	Paul	k1gMnSc7	Paul
Verlainem	Verlain	k1gMnSc7	Verlain
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
eseji	esej	k1gInSc6	esej
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
(	(	kIx(	(
<g/>
knižně	knižně	k6eAd1	knižně
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
bývá	bývat	k5eAaImIp3nS	bývat
spojena	spojen	k2eAgFnSc1d1	spojena
představa	představa	k1gFnSc1	představa
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
zločinnosti	zločinnost	k1gFnSc2	zločinnost
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
dekadentním	dekadentní	k2eAgInSc6d1	dekadentní
životním	životní	k2eAgInSc6d1	životní
stylu	styl	k1gInSc6	styl
spojeném	spojený	k2eAgInSc6d1	spojený
s	s	k7c7	s
neúctou	neúcta	k1gFnSc7	neúcta
k	k	k7c3	k
většinové	většinový	k2eAgFnSc3d1	většinová
společnosti	společnost	k1gFnSc3	společnost
a	a	k8xC	a
jejím	její	k3xOp3gNnPc3	její
pravidlům	pravidlo	k1gNnPc3	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
znakem	znak	k1gInSc7	znak
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
poezie	poezie	k1gFnSc2	poezie
bylo	být	k5eAaImAgNnS	být
hledat	hledat	k5eAaImF	hledat
krásu	krása	k1gFnSc4	krása
v	v	k7c6	v
ošklivosti	ošklivost	k1gFnSc6	ošklivost
<g/>
,	,	kIx,	,
psát	psát	k5eAaImF	psát
i	i	k9	i
o	o	k7c6	o
dosud	dosud	k6eAd1	dosud
tabuizovaných	tabuizovaný	k2eAgNnPc6d1	tabuizované
tématech	téma	k1gNnPc6	téma
(	(	kIx(	(
<g/>
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
báseň	báseň	k1gFnSc1	báseň
Zdechlina	zdechlina	k1gFnSc1	zdechlina
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Baudelaira	Baudelaira	k1gMnSc1	Baudelaira
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
prokletého	prokletý	k2eAgMnSc4d1	prokletý
básníka	básník	k1gMnSc4	básník
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
prototyp	prototyp	k1gInSc1	prototyp
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
považován	považován	k2eAgMnSc1d1	považován
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Verlaine	Verlain	k1gInSc5	Verlain
a	a	k8xC	a
Arthur	Arthur	k1gMnSc1	Arthur
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
jsou	být	k5eAaImIp3nP	být
označováni	označovat	k5eAaImNgMnP	označovat
za	za	k7c4	za
typické	typický	k2eAgInPc4d1	typický
příklady	příklad	k1gInPc4	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
vžil	vžít	k5eAaPmAgMnS	vžít
do	do	k7c2	do
širšího	široký	k2eAgNnSc2d2	širší
povědomí	povědomí	k1gNnSc2	povědomí
až	až	k8xS	až
vydáním	vydání	k1gNnSc7	vydání
sbírky	sbírka	k1gFnSc2	sbírka
Paula	Paula	k1gFnSc1	Paula
Verlaina	Verlaina	k1gFnSc1	Verlaina
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
označení	označení	k1gNnSc1	označení
spisovatelů	spisovatel	k1gMnPc2	spisovatel
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
knize	kniha	k1gFnSc6	kniha
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jménem	jméno	k1gNnSc7	jméno
pro	pro	k7c4	pro
spisovatele	spisovatel	k1gMnPc4	spisovatel
(	(	kIx(	(
<g/>
a	a	k8xC	a
umělce	umělec	k1gMnSc4	umělec
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
život	život	k1gInSc1	život
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
společnost	společnost	k1gFnSc4	společnost
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
Pierre	Pierr	k1gInSc5	Pierr
Seghers	Seghersa	k1gFnPc2	Seghersa
vydal	vydat	k5eAaPmAgMnS	vydat
sbírku	sbírka	k1gFnSc4	sbírka
Poè	Poè	k1gMnSc1	Poè
maudits	mauditsa	k1gFnPc2	mauditsa
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
aujourd	aujourd	k1gMnSc1	aujourd
<g/>
'	'	kIx"	'
<g/>
hui	hui	k?	hui
<g/>
:	:	kIx,	:
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
Prokletí	prokletý	k2eAgMnPc1d1	prokletý
básníci	básník	k1gMnPc1	básník
dneška	dnešek	k1gInSc2	dnešek
<g/>
)	)	kIx)	)
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgNnPc4d1	obsahující
díla	dílo	k1gNnPc4	dílo
autorů	autor	k1gMnPc2	autor
jako	jako	k8xS	jako
Antonin	Antonin	k2eAgInSc1d1	Antonin
Artaud	Artaud	k1gInSc1	Artaud
<g/>
,	,	kIx,	,
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Duprey	Dupreum	k1gNnPc7	Dupreum
a	a	k8xC	a
10	[number]	k4	10
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Artaud	Artaud	k1gInSc1	Artaud
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
posmrtně	posmrtně	k6eAd1	posmrtně
uznávanými	uznávaný	k2eAgMnPc7d1	uznávaný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
prokleté	prokletý	k2eAgMnPc4d1	prokletý
básníky	básník	k1gMnPc4	básník
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
řazen	řadit	k5eAaImNgMnS	řadit
i	i	k8xC	i
francouzský	francouzský	k2eAgMnSc1d1	francouzský
renesanční	renesanční	k2eAgMnSc1d1	renesanční
básník	básník	k1gMnSc1	básník
François	François	k1gFnSc2	François
Villon	Villon	k1gInSc1	Villon
či	či	k8xC	či
praotec	praotec	k1gMnSc1	praotec
literárního	literární	k2eAgInSc2d1	literární
horroru	horror	k1gInSc2	horror
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
.	.	kIx.	.
</s>
<s>
Prokletí	prokletý	k2eAgMnPc1d1	prokletý
básníci	básník	k1gMnPc1	básník
je	on	k3xPp3gFnPc4	on
sbírka	sbírka	k1gFnSc1	sbírka
Paula	Paula	k1gFnSc1	Paula
Verlaina	Verlaina	k1gFnSc1	Verlaina
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
pocta	pocta	k1gFnSc1	pocta
těmto	tento	k3xDgMnPc3	tento
autorům	autor	k1gMnPc3	autor
<g/>
:	:	kIx,	:
Tristan	Tristan	k1gInSc1	Tristan
Corbiè	Corbiè	k1gMnSc1	Corbiè
Arthur	Arthur	k1gMnSc1	Arthur
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
Stéphane	Stéphan	k1gMnSc5	Stéphan
Mallarmé	Mallarmý	k2eAgMnPc4d1	Mallarmý
-	-	kIx~	-
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
sbírky	sbírka	k1gFnSc2	sbírka
pouze	pouze	k6eAd1	pouze
tito	tento	k3xDgMnPc1	tento
tři	tři	k4xCgMnPc1	tři
Marceline	Marcelin	k1gInSc5	Marcelin
Desbordes-Valmorová	Desbordes-Valmorový	k2eAgFnSc1d1	Desbordes-Valmorový
Adam	Adam	k1gMnSc1	Adam
Auguste	August	k1gMnSc5	August
Villiers	Villiers	k1gInSc4	Villiers
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Isle	Isl	k1gMnSc4	Isl
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Paul	Paul	k1gMnSc1	Paul
Verlaine	Verlain	k1gInSc5	Verlain
-	-	kIx~	-
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Pauvre	Pauvr	k1gInSc5	Pauvr
Lélian	Lélian	k1gInSc4	Lélian
Mezi	mezi	k7c4	mezi
prokleté	prokletý	k2eAgMnPc4d1	prokletý
básníky	básník	k1gMnPc4	básník
se	se	k3xPyFc4	se
také	také	k9	také
řadí	řadit	k5eAaImIp3nS	řadit
<g/>
:	:	kIx,	:
Charles	Charles	k1gMnSc1	Charles
Cros	Crosa	k1gFnPc2	Crosa
Charles	Charles	k1gMnSc1	Charles
Pierre	Pierr	k1gInSc5	Pierr
Baudelaire	Baudelair	k1gInSc5	Baudelair
Gérard	Gérard	k1gMnSc1	Gérard
de	de	k?	de
Nerval	rvát	k5eNaImAgMnS	rvát
Jules	Jules	k1gMnSc1	Jules
Laforgue	Laforgu	k1gFnSc2	Laforgu
Comte	Comt	k1gInSc5	Comt
de	de	k?	de
Lautréamont	Lautréamont	k1gInSc4	Lautréamont
Antonin	Antonin	k2eAgInSc4d1	Antonin
Artaud	Artaud	k1gInSc4	Artaud
</s>
