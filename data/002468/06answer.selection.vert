<s>
Prokletí	prokletý	k2eAgMnPc1d1	prokletý
básníci	básník	k1gMnPc1	básník
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
les	les	k1gInSc1	les
poétes	poétesa	k1gFnPc2	poétesa
maudits	maudits	k6eAd1	maudits
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
francouzských	francouzský	k2eAgMnPc2d1	francouzský
nekonformních	konformní	k2eNgMnPc2d1	nekonformní
básníků	básník	k1gMnPc2	básník
poslední	poslední	k2eAgFnSc2d1	poslední
třetiny	třetina	k1gFnSc2	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
užité	užitý	k2eAgFnSc2d1	užitá
Paulem	Paul	k1gMnSc7	Paul
Verlainem	Verlain	k1gMnSc7	Verlain
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
eseji	esej	k1gInSc6	esej
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
(	(	kIx(	(
<g/>
knižně	knižně	k6eAd1	knižně
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
