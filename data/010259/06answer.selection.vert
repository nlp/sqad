<s>
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Le	Le	k1gFnSc1	Le
petit	petit	k1gInSc4	petit
prince	princ	k1gMnSc2	princ
<g/>
)	)	kIx)	)
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgNnSc4d3	nejznámější
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
pilota	pilot	k1gMnSc2	pilot
Antoina	Antoin	k1gMnSc2	Antoin
de	de	k?	de
Saint-Exupéryho	Saint-Exupéry	k1gMnSc2	Saint-Exupéry
<g/>
.	.	kIx.	.
</s>
