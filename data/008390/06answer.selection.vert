<s>
Klavír	klavír	k1gInSc1	klavír
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
jako	jako	k8xS	jako
sólový	sólový	k2eAgInSc1d1	sólový
i	i	k8xC	i
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
či	či	k8xC	či
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
