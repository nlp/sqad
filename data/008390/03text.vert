<p>
<s>
Klavír	klavír	k1gInSc1	klavír
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
claves	clavesa	k1gFnPc2	clavesa
=	=	kIx~	=
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strunný	strunný	k2eAgInSc1d1	strunný
úderný	úderný	k2eAgInSc1d1	úderný
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgInSc1d1	zvaný
též	též	k9	též
hovorově	hovorově	k6eAd1	hovorově
piano	piano	k6eAd1	piano
nebo	nebo	k8xC	nebo
dříve	dříve	k6eAd2	dříve
fortepiano	fortepiano	k1gNnSc1	fortepiano
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
též	též	k9	též
pianoforte	pianofort	k1gInSc5	pianofort
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
tón	tón	k1gInSc1	tón
vzniká	vznikat	k5eAaImIp3nS	vznikat
chvěním	chvění	k1gNnSc7	chvění
strun	struna	k1gFnPc2	struna
rozkmitaných	rozkmitaný	k2eAgFnPc2d1	rozkmitaná
úderem	úder	k1gInSc7	úder
plstěných	plstěný	k2eAgNnPc2d1	plstěné
kladívek	kladívko	k1gNnPc2	kladívko
<g/>
.	.	kIx.	.
</s>
<s>
Klavír	klavír	k1gInSc1	klavír
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k8xS	jako
sólový	sólový	k2eAgInSc1d1	sólový
i	i	k8xC	i
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
či	či	k8xC	či
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
klavír	klavír	k1gInSc1	klavír
určený	určený	k2eAgInSc1d1	určený
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
koncertní	koncertní	k2eAgInPc4d1	koncertní
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
koncertní	koncertní	k2eAgNnSc4d1	koncertní
křídlo	křídlo	k1gNnSc4	křídlo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
rezonanční	rezonanční	k2eAgFnSc2d1	rezonanční
skříně	skříň	k1gFnSc2	skříň
<g/>
;	;	kIx,	;
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
umístěny	umístit	k5eAaPmNgInP	umístit
vodorovně	vodorovně	k6eAd1	vodorovně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
skříň	skříň	k1gFnSc1	skříň
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
struny	struna	k1gFnPc1	struna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
svisle	svisla	k1gFnSc3	svisla
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pianino	pianino	k1gNnSc1	pianino
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
je	být	k5eAaImIp3nS	být
klavírista	klavírista	k1gMnSc1	klavírista
(	(	kIx(	(
<g/>
klavíristka	klavíristka	k1gFnSc1	klavíristka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pianista	pianista	k1gMnSc1	pianista
(	(	kIx(	(
<g/>
pianistka	pianistka	k1gFnSc1	pianistka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
názvů	název	k1gInPc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
klavír	klavír	k1gInSc1	klavír
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
clavis	clavis	k1gInSc4	clavis
-	-	kIx~	-
klíč	klíč	k1gInSc4	klíč
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
původně	původně	k6eAd1	původně
řadu	řada	k1gFnSc4	řada
kláves	klávesa	k1gFnPc2	klávesa
neboli	neboli	k8xC	neboli
klaviaturu	klaviatura	k1gFnSc4	klaviatura
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
jako	jako	k8xC	jako
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
manuálu	manuál	k1gInSc2	manuál
u	u	k7c2	u
varhan	varhany	k1gFnPc2	varhany
aj.	aj.	kA	aj.
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
např.	např.	kA	např.
třímanuálové	třímanuálový	k2eAgInPc1d1	třímanuálový
varhany	varhany	k1gInPc1	varhany
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgInPc1	tři
manuály	manuál	k1gInPc1	manuál
neboli	neboli	k8xC	neboli
tři	tři	k4xCgInPc1	tři
klavíry	klavír	k1gInPc1	klavír
<g/>
,	,	kIx,	,
cembalo	cembalo	k1gNnSc1	cembalo
dvě	dva	k4xCgFnPc4	dva
klaviatury	klaviatura	k1gFnPc4	klaviatura
neboli	neboli	k8xC	neboli
dva	dva	k4xCgInPc4	dva
klavíry	klavír	k1gInPc4	klavír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
spinet	spinet	k1gInSc1	spinet
<g/>
,	,	kIx,	,
klavichord	klavichord	k1gInSc1	klavichord
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
dedikoval	dedikovat	k5eAaBmAgMnS	dedikovat
(	(	kIx(	(
<g/>
věnoval	věnovat	k5eAaImAgMnS	věnovat
<g/>
)	)	kIx)	)
svoji	svůj	k3xOyFgFnSc4	svůj
skladbu	skladba	k1gFnSc4	skladba
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
klavír	klavír	k1gInSc4	klavír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
blíže	blízce	k6eAd2	blízce
specifikoval	specifikovat	k5eAaBmAgMnS	specifikovat
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaký	jaký	k3yIgInSc4	jaký
nástroj	nástroj	k1gInSc4	nástroj
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
termínem	termín	k1gInSc7	termín
míní	mínit	k5eAaImIp3nS	mínit
většinou	většinou	k6eAd1	většinou
fortepiano	fortepiano	k1gNnSc1	fortepiano
a	a	k8xC	a
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
nástroji	nástroj	k1gInSc6	nástroj
bude	být	k5eAaImBp3nS	být
dále	daleko	k6eAd2	daleko
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slova	slovo	k1gNnPc1	slovo
piano	piano	k6eAd1	piano
a	a	k8xC	a
pianoforte	pianofort	k1gInSc5	pianofort
(	(	kIx(	(
<g/>
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
odvozeniny	odvozenina	k1gFnPc1	odvozenina
<g/>
)	)	kIx)	)
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
italských	italský	k2eAgNnPc2d1	italské
slov	slovo	k1gNnPc2	slovo
piano	piano	k6eAd1	piano
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
potichu	potichu	k6eAd1	potichu
<g/>
)	)	kIx)	)
a	a	k8xC	a
forte	forte	k6eAd1	forte
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
silně	silně	k6eAd1	silně
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
přednost	přednost	k1gFnSc4	přednost
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
měnit	měnit	k5eAaImF	měnit
intenzitu	intenzita	k1gFnSc4	intenzita
produkovaného	produkovaný	k2eAgInSc2d1	produkovaný
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
úhozu	úhoz	k1gInSc2	úhoz
prstu	prst	k1gInSc2	prst
na	na	k7c4	na
klávesu	klávesa	k1gFnSc4	klávesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úvod	úvod	k1gInSc1	úvod
==	==	k?	==
</s>
</p>
<p>
<s>
Klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
strunné	strunný	k2eAgInPc1d1	strunný
nástroje	nástroj	k1gInPc1	nástroj
–	–	k?	–
klavichord	klavichorda	k1gFnPc2	klavichorda
a	a	k8xC	a
harpsichord	harpsichorda	k1gFnPc2	harpsichorda
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
rozeznívání	rozeznívání	k1gNnSc2	rozeznívání
ocelové	ocelový	k2eAgFnSc2d1	ocelová
struny	struna	k1gFnSc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
rozechvívá	rozechvívat	k5eAaImIp3nS	rozechvívat
stiskem	stisk	k1gInSc7	stisk
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
případě	případ	k1gInSc6	případ
klavichordu	klavichord	k1gInSc2	klavichord
kovovým	kovový	k2eAgInSc7d1	kovový
hrotem	hrot	k1gInSc7	hrot
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
harpsichordu	harpsichorda	k1gFnSc4	harpsichorda
hrotem	hrot	k1gInSc7	hrot
brku	brk	k1gInSc2	brk
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
klavíru	klavír	k1gInSc2	klavír
dřevěným	dřevěný	k2eAgFnPc3d1	dřevěná
kladívkem	kladívko	k1gNnSc7	kladívko
strunu	struna	k1gFnSc4	struna
rozkmitá	rozkmitat	k5eAaPmIp3nS	rozkmitat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
rovněž	rovněž	k9	rovněž
elektronická	elektronický	k2eAgNnPc1d1	elektronické
piana	piano	k1gNnPc1	piano
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
složitá	složitý	k2eAgFnSc1d1	složitá
mechanika	mechanika	k1gFnSc1	mechanika
klavíru	klavír	k1gInSc2	klavír
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
elektronickým	elektronický	k2eAgInSc7d1	elektronický
tónovým	tónový	k2eAgInSc7d1	tónový
generátorem	generátor	k1gInSc7	generátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
výhody	výhoda	k1gFnPc4	výhoda
klavíru	klavír	k1gInSc2	klavír
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
od	od	k7c2	od
malého	malý	k2eAgNnSc2d1	malé
dítěte	dítě	k1gNnSc2	dítě
až	až	k9	až
po	po	k7c4	po
starého	starý	k2eAgMnSc4d1	starý
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
uspokojit	uspokojit	k5eAaPmF	uspokojit
jak	jak	k6eAd1	jak
začínající	začínající	k2eAgMnPc4d1	začínající
klavíristy	klavírista	k1gMnPc4	klavírista
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
náročné	náročný	k2eAgMnPc4d1	náročný
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
klavíru	klavír	k1gInSc2	klavír
je	být	k5eAaImIp3nS	být
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
i	i	k8xC	i
mezi	mezi	k7c7	mezi
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sami	sám	k3xTgMnPc1	sám
klavírní	klavírní	k2eAgFnSc4d1	klavírní
hudbu	hudba	k1gFnSc4	hudba
neprovozují	provozovat	k5eNaImIp3nP	provozovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
klavírní	klavírní	k2eAgInPc1d1	klavírní
recitály	recitál	k1gInPc1	recitál
či	či	k8xC	či
koncerty	koncert	k1gInPc1	koncert
patří	patřit	k5eAaImIp3nP	patřit
ke	k	k7c3	k
stálému	stálý	k2eAgInSc3d1	stálý
programu	program	k1gInSc3	program
koncertních	koncertní	k2eAgInPc2d1	koncertní
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
klavíru	klavír	k1gInSc2	klavír
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
špatná	špatný	k2eAgFnSc1d1	špatná
skladnost	skladnost	k1gFnSc1	skladnost
a	a	k8xC	a
mobilita	mobilita	k1gFnSc1	mobilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
klavíru	klavír	k1gInSc2	klavír
==	==	k?	==
</s>
</p>
<p>
<s>
Klavír	klavír	k1gInSc1	klavír
je	být	k5eAaImIp3nS	být
nesmírně	smírně	k6eNd1	smírně
složitý	složitý	k2eAgInSc1d1	složitý
nástroj	nástroj	k1gInSc1	nástroj
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
jednotným	jednotný	k2eAgInSc7d1	jednotný
cílem	cíl	k1gInSc7	cíl
a	a	k8xC	a
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
vytvářet	vytvářet	k5eAaImF	vytvářet
čisté	čistý	k2eAgInPc4d1	čistý
a	a	k8xC	a
zvučné	zvučný	k2eAgInPc4d1	zvučný
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skříň	skříň	k1gFnSc4	skříň
klavíru	klavír	k1gInSc2	klavír
a	a	k8xC	a
ozvučná	ozvučný	k2eAgFnSc1d1	ozvučná
deska	deska	k1gFnSc1	deska
===	===	k?	===
</s>
</p>
<p>
<s>
Celou	celý	k2eAgFnSc4d1	celá
konstrukci	konstrukce	k1gFnSc4	konstrukce
klavíru	klavír	k1gInSc2	klavír
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
jeho	jeho	k3xOp3gFnPc2	jeho
částí	část	k1gFnPc2	část
nese	nést	k5eAaImIp3nS	nést
masivní	masivní	k2eAgInSc1d1	masivní
baraš	baraš	k1gInSc1	baraš
čili	čili	k8xC	čili
půda	půda	k1gFnSc1	půda
s	s	k7c7	s
rezonanční	rezonanční	k2eAgFnSc7d1	rezonanční
deskou	deska	k1gFnSc7	deska
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
u	u	k7c2	u
křídla	křídlo	k1gNnSc2	křídlo
korpus	korpus	k1gInSc1	korpus
<g/>
)	)	kIx)	)
s	s	k7c7	s
litinovým	litinový	k2eAgInSc7d1	litinový
rámem	rám	k1gInSc7	rám
(	(	kIx(	(
<g/>
polopancéřový	polopancéřový	k2eAgMnSc1d1	polopancéřový
nebo	nebo	k8xC	nebo
celopancéřový	celopancéřový	k2eAgMnSc1d1	celopancéřový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
celopancéřového	celopancéřový	k2eAgNnSc2d1	celopancéřový
pianina	pianino	k1gNnSc2	pianino
sahá	sahat	k5eAaImIp3nS	sahat
litinový	litinový	k2eAgInSc1d1	litinový
rám	rám	k1gInSc1	rám
až	až	k9	až
k	k	k7c3	k
hornímu	horní	k2eAgInSc3d1	horní
okraji	okraj	k1gInSc3	okraj
skříně	skříň	k1gFnSc2	skříň
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
prostor	prostor	k1gInSc1	prostor
ladicích	ladicí	k2eAgInPc2d1	ladicí
kolíků	kolík	k1gInPc2	kolík
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
polopancéřový	polopancéřový	k2eAgInSc1d1	polopancéřový
nástroj	nástroj	k1gInSc1	nástroj
má	mít	k5eAaImIp3nS	mít
ladicí	ladicí	k2eAgInPc4d1	ladicí
kolíky	kolík	k1gInPc4	kolík
mimo	mimo	k7c4	mimo
rám	rám	k1gInSc4	rám
<g/>
.	.	kIx.	.
</s>
<s>
Baraš	Baraš	k1gInSc1	Baraš
či	či	k8xC	či
korpus	korpus	k1gInSc1	korpus
nese	nést	k5eAaImIp3nS	nést
skříň	skříň	k1gFnSc4	skříň
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
mechanismus	mechanismus	k1gInSc1	mechanismus
klavíru	klavír	k1gInSc2	klavír
se	s	k7c7	s
strunami	struna	k1gFnPc7	struna
a	a	k8xC	a
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ozvučná	ozvučný	k2eAgFnSc1d1	ozvučná
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
též	též	k9	též
ozvučnice	ozvučnice	k1gFnSc1	ozvučnice
či	či	k8xC	či
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
deska	deska	k1gFnSc1	deska
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
žebry	žebr	k1gInPc7	žebr
a	a	k8xC	a
kobylkou	kobylka	k1gFnSc7	kobylka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
duší	duše	k1gFnSc7	duše
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
na	na	k7c6	na
hustotě	hustota	k1gFnSc6	hustota
let	let	k1gInSc4	let
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
síle	síla	k1gFnSc3	síla
desky	deska	k1gFnSc2	deska
-	-	kIx~	-
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgInPc4d1	vysoký
tóny	tón	k1gInPc4	tón
hustší	hustý	k2eAgFnSc1d2	hustší
a	a	k8xC	a
tlustší	tlustý	k2eAgFnSc1d2	tlustší
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nízké	nízký	k2eAgFnPc4d1	nízká
řidší	řídký	k2eAgFnPc4d2	řidší
a	a	k8xC	a
tenčí	tenký	k2eAgFnPc4d2	tenčí
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
na	na	k7c6	na
správném	správný	k2eAgNnSc6d1	správné
umístění	umístění	k1gNnSc6	umístění
štégu	štég	k1gInSc2	štég
(	(	kIx(	(
<g/>
kobylky	kobylka	k1gFnPc1	kobylka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
určuje	určovat	k5eAaImIp3nS	určovat
kvalitu	kvalita	k1gFnSc4	kvalita
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
ze	z	k7c2	z
smrkového	smrkový	k2eAgMnSc2d1	smrkový
<g/>
,	,	kIx,	,
či	či	k8xC	či
řidčeji	řídce	k6eAd2	řídce
jedlového	jedlový	k2eAgNnSc2d1	Jedlové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
;	;	kIx,	;
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
křídla	křídlo	k1gNnSc2	křídlo
vespod	vespod	k7c2	vespod
skříně	skříň	k1gFnSc2	skříň
pod	pod	k7c7	pod
strunami	struna	k1gFnPc7	struna
a	a	k8xC	a
litinovým	litinový	k2eAgInSc7d1	litinový
rámem	rám	k1gInSc7	rám
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pianina	pianino	k1gNnSc2	pianino
u	u	k7c2	u
zadní	zadní	k2eAgFnSc2d1	zadní
stěny	stěna	k1gFnSc2	stěna
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
použito	použit	k2eAgNnSc1d1	použito
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
speciálně	speciálně	k6eAd1	speciálně
nařezáno	nařezat	k5eAaPmNgNnS	nařezat
a	a	k8xC	a
necháno	nechat	k5eAaPmNgNnS	nechat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
vysychat	vysychat	k5eAaImF	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Funkcí	funkce	k1gFnSc7	funkce
ozvučné	ozvučný	k2eAgFnSc2d1	ozvučná
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kobylky	kobylka	k1gFnSc2	kobylka
přijímat	přijímat	k5eAaImF	přijímat
tóny	tón	k1gInPc4	tón
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
zesilovat	zesilovat	k5eAaImF	zesilovat
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
dávat	dávat	k5eAaImF	dávat
jim	on	k3xPp3gMnPc3	on
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
stěsnanosti	stěsnanost	k1gFnSc6	stěsnanost
či	či	k8xC	či
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
prostornosti	prostornost	k1gFnSc6	prostornost
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zvuk	zvuk	k1gInSc1	zvuk
nesen	nést	k5eAaImNgInS	nést
správným	správný	k2eAgInSc7d1	správný
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
ničím	ničit	k5eAaImIp1nS	ničit
zkreslen	zkreslen	k2eAgMnSc1d1	zkreslen
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
i	i	k9	i
na	na	k7c6	na
vlivu	vliv	k1gInSc6	vliv
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
tlumivosti	tlumivost	k1gFnSc2	tlumivost
okolního	okolní	k2eAgInSc2d1	okolní
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc1	nástroj
umístěn	umístit	k5eAaPmNgInS	umístit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodně	rozhodně	k6eAd1	rozhodně
by	by	kYmCp3nS	by
nástroj	nástroj	k1gInSc1	nástroj
neměl	mít	k5eNaImAgInS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
odkládací	odkládací	k2eAgFnSc1d1	odkládací
a	a	k8xC	a
manipulační	manipulační	k2eAgFnSc1d1	manipulační
deska	deska	k1gFnSc1	deska
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
květiny	květina	k1gFnPc1	květina
a	a	k8xC	a
chrastící	chrastící	k2eAgInPc1d1	chrastící
předměty	předmět	k1gInPc1	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k8xC	i
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
tabulovým	tabulový	k2eAgInSc7d1	tabulový
celopancéřovým	celopancéřův	k2eAgInSc7d1	celopancéřův
klavírem	klavír	k1gInSc7	klavír
s	s	k7c7	s
anglickou	anglický	k2eAgFnSc7d1	anglická
mechanikou	mechanika	k1gFnSc7	mechanika
(	(	kIx(	(
<g/>
vypadal	vypadat	k5eAaPmAgMnS	vypadat
jako	jako	k9	jako
běžný	běžný	k2eAgInSc4d1	běžný
čtyřnohý	čtyřnohý	k2eAgInSc4d1	čtyřnohý
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
na	na	k7c6	na
delší	dlouhý	k2eAgFnSc6d2	delší
straně	strana	k1gFnSc6	strana
měl	mít	k5eAaImAgInS	mít
čelně	čelně	k6eAd1	čelně
zakrytou	zakrytý	k2eAgFnSc4d1	zakrytá
normální	normální	k2eAgFnSc4d1	normální
klaviaturu	klaviatura	k1gFnSc4	klaviatura
a	a	k8xC	a
horní	horní	k2eAgFnSc4d1	horní
desku	deska	k1gFnSc4	deska
měl	mít	k5eAaImAgMnS	mít
odklopnou	odklopný	k2eAgFnSc4d1	odklopná
<g/>
,	,	kIx,	,
podepřenou	podepřený	k2eAgFnSc4d1	podepřená
<g/>
)	)	kIx)	)
a	a	k8xC	a
se	s	k7c7	s
starými	starý	k2eAgMnPc7d1	starý
vídeňskými	vídeňský	k2eAgNnPc7d1	Vídeňské
křídly	křídlo	k1gNnPc7	křídlo
i	i	k8xC	i
novějšími	nový	k2eAgInPc7d2	novější
americkými	americký	k2eAgInPc7d1	americký
(	(	kIx(	(
<g/>
Steinway	Steinway	k1gInPc7	Steinway
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
tedy	tedy	k9	tedy
od	od	k7c2	od
malých	malý	k2eAgFnPc2d1	malá
pokojových	pokojová	k1gFnPc2	pokojová
po	po	k7c4	po
velké	velký	k2eAgInPc4d1	velký
koncertní	koncertní	k2eAgInPc4d1	koncertní
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
typy	typ	k1gInPc1	typ
měly	mít	k5eAaImAgInP	mít
místo	místo	k1gNnSc4	místo
pancéřového	pancéřový	k2eAgInSc2d1	pancéřový
rámu	rám	k1gInSc2	rám
jen	jen	k9	jen
vzpěry	vzpěr	k1gInPc4	vzpěr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klaviatura	klaviatura	k1gFnSc1	klaviatura
===	===	k?	===
</s>
</p>
<p>
<s>
Klaviatura	klaviatura	k1gFnSc1	klaviatura
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
černých	černý	k2eAgFnPc2d1	černá
a	a	k8xC	a
bílých	bílý	k2eAgFnPc2d1	bílá
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
stiskem	stisk	k1gInSc7	stisk
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pianista	pianista	k1gMnSc1	pianista
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
současné	současný	k2eAgInPc1d1	současný
klavíry	klavír	k1gInPc1	klavír
mají	mít	k5eAaImIp3nP	mít
88	[number]	k4	88
kláves	klávesa	k1gFnPc2	klávesa
(	(	kIx(	(
<g/>
sedm	sedm	k4xCc1	sedm
oktáv	oktáva	k1gFnPc2	oktáva
a	a	k8xC	a
část	část	k1gFnSc4	část
osmé	osmý	k4xOgFnSc6	osmý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
klavíry	klavír	k1gInPc1	klavír
s	s	k7c7	s
85	[number]	k4	85
klávesami	klávesa	k1gFnPc7	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
naopak	naopak	k6eAd1	naopak
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
speciální	speciální	k2eAgFnPc4d1	speciální
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
koncertní	koncertní	k2eAgInPc1d1	koncertní
klavíry	klavír	k1gInPc1	klavír
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
kláves	klávesa	k1gFnPc2	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Klávesy	klávesa	k1gFnPc1	klávesa
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
při	při	k7c6	při
sesazování	sesazování	k1gNnSc6	sesazování
nástroje	nástroj	k1gInSc2	nástroj
vyváženy	vyvážen	k2eAgInPc1d1	vyvážen
<g/>
,	,	kIx,	,
čeho	co	k3yQnSc2	co
se	se	k3xPyFc4	se
dociluje	docilovat	k5eAaImIp3nS	docilovat
olůvky	olůvko	k1gNnPc7	olůvko
upevněnými	upevněný	k2eAgInPc7d1	upevněný
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
kláves	klávesa	k1gFnPc2	klávesa
a	a	k8xC	a
také	také	k9	také
perfektně	perfektně	k6eAd1	perfektně
vyrovnány	vyrovnat	k5eAaPmNgInP	vyrovnat
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
pomocí	pomocí	k7c2	pomocí
papírových	papírový	k2eAgFnPc2d1	papírová
podložek	podložka	k1gFnPc2	podložka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
klaviatuře	klaviatura	k1gFnSc6	klaviatura
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
černé	černý	k2eAgFnPc1d1	černá
klávesy	klávesa	k1gFnPc1	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Bílých	bílý	k2eAgMnPc2d1	bílý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
oktávě	oktáva	k1gFnSc6	oktáva
sedm	sedm	k4xCc1	sedm
a	a	k8xC	a
hrají	hrát	k5eAaImIp3nP	hrát
tóny	tón	k1gInPc1	tón
hudební	hudební	k2eAgFnSc2d1	hudební
abecedy	abeceda	k1gFnSc2	abeceda
(	(	kIx(	(
<g/>
C	C	kA	C
-	-	kIx~	-
D	D	kA	D
-	-	kIx~	-
E	E	kA	E
-	-	kIx~	-
F	F	kA	F
-	-	kIx~	-
G	G	kA	G
-	-	kIx~	-
A	A	kA	A
-	-	kIx~	-
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Černých	Černá	k1gFnPc2	Černá
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
a	a	k8xC	a
hrají	hrát	k5eAaImIp3nP	hrát
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
některé	některý	k3yIgInPc4	některý
půltóny	půltón	k1gInPc4	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgInP	být
běžné	běžný	k2eAgInPc1d1	běžný
modely	model	k1gInPc1	model
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
88	[number]	k4	88
klávesách	klávesa	k1gFnPc6	klávesa
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
52	[number]	k4	52
bílých	bílý	k2eAgMnPc2d1	bílý
a	a	k8xC	a
36	[number]	k4	36
černých	černý	k2eAgFnPc2d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bílé	bílý	k2eAgFnPc4d1	bílá
klávesy	klávesa	k1gFnPc4	klávesa
se	se	k3xPyFc4	se
dřív	dříve	k6eAd2	dříve
užívala	užívat	k5eAaImAgFnS	užívat
slonovina	slonovina	k1gFnSc1	slonovina
a	a	k8xC	a
mroží	mroží	k2eAgInSc1d1	mroží
kel	kel	k1gInSc1	kel
<g/>
,	,	kIx,	,
na	na	k7c4	na
černé	černý	k2eAgNnSc4d1	černé
ebenové	ebenový	k2eAgNnSc4d1	ebenové
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
obkládají	obkládat	k5eAaImIp3nP	obkládat
klávesy	klávesa	k1gFnSc2	klávesa
umělými	umělý	k2eAgInPc7d1	umělý
materiály	materiál	k1gInPc7	materiál
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
celuloid	celuloid	k1gInSc1	celuloid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
litinovém	litinový	k2eAgInSc6d1	litinový
rámu	rám	k1gInSc6	rám
jsou	být	k5eAaImIp3nP	být
nataženy	natažen	k2eAgFnPc1d1	natažena
struny	struna	k1gFnPc1	struna
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
hlubší	hluboký	k2eAgInPc4d2	hlubší
tóny	tón	k1gInPc4	tón
delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgInPc4d2	vyšší
tóny	tón	k1gInPc4	tón
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
tenčí	tenký	k2eAgFnSc1d2	tenčí
(	(	kIx(	(
<g/>
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
hlubší	hluboký	k2eAgFnSc1d2	hlubší
tónů	tón	k1gInPc2	tón
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
tón	tón	k1gInSc4	tón
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
nejhlubší	hluboký	k2eAgInPc4d3	nejhlubší
tóny	tón	k1gInPc4	tón
jedna	jeden	k4xCgFnSc1	jeden
silnější	silný	k2eAgFnSc1d2	silnější
struna	struna	k1gFnSc1	struna
(	(	kIx(	(
<g/>
bývá	bývat	k5eAaImIp3nS	bývat
ovinutá	ovinutý	k2eAgFnSc1d1	ovinutá
i	i	k9	i
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
tóny	tón	k1gInPc4	tón
struny	struna	k1gFnSc2	struna
tři	tři	k4xCgInPc4	tři
až	až	k9	až
čtyři	čtyři	k4xCgInPc4	čtyři
(	(	kIx(	(
<g/>
chór	chór	k1gInSc1	chór
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zvuk	zvuk	k1gInSc1	zvuk
vysokých	vysoký	k2eAgFnPc2d1	vysoká
strun	struna	k1gFnPc2	struna
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
,	,	kIx,	,
zvýšením	zvýšení	k1gNnSc7	zvýšení
jejich	jejich	k3xOp3gInSc2	jejich
počtu	počet	k1gInSc2	počet
se	se	k3xPyFc4	se
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
<g/>
.	.	kIx.	.
</s>
<s>
Alikvótní	Alikvótní	k2eAgFnPc1d1	Alikvótní
přídavné	přídavný	k2eAgFnPc1d1	přídavná
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
laděny	laděn	k2eAgInPc1d1	laděn
s	s	k7c7	s
rozdílem	rozdíl	k1gInSc7	rozdíl
oktávy	oktáva	k1gFnSc2	oktáva
vůči	vůči	k7c3	vůči
základnímu	základní	k2eAgInSc3d1	základní
chóru	chór	k1gInSc3	chór
a	a	k8xC	a
upravují	upravovat	k5eAaImIp3nP	upravovat
barvu	barva	k1gFnSc4	barva
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
procházejí	procházet	k5eAaImIp3nP	procházet
u	u	k7c2	u
kolíků	kolík	k1gInPc2	kolík
chórovými	chórový	k2eAgInPc7d1	chórový
otvory	otvor	k1gInPc7	otvor
v	v	k7c6	v
agrafech	agraf	k1gInPc6	agraf
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
udržují	udržovat	k5eAaImIp3nP	udržovat
správné	správný	k2eAgFnPc4d1	správná
mezery	mezera	k1gFnPc4	mezera
mezi	mezi	k7c7	mezi
strunami	struna	k1gFnPc7	struna
i	i	k8xC	i
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
výšku	výška	k1gFnSc4	výška
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
nutnou	nutný	k2eAgFnSc7d1	nutná
pro	pro	k7c4	pro
plné	plný	k2eAgNnSc4d1	plné
rozezvučení	rozezvučení	k1gNnSc4	rozezvučení
chóru	chór	k1gInSc2	chór
dopadem	dopad	k1gInSc7	dopad
plstěného	plstěný	k2eAgNnSc2d1	plstěné
kladívka	kladívko	k1gNnSc2	kladívko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozeznívání	rozeznívání	k1gNnPc1	rozeznívání
strun	struna	k1gFnPc2	struna
===	===	k?	===
</s>
</p>
<p>
<s>
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
mechanika	mechanika	k1gFnSc1	mechanika
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
nejkomplikovanější	komplikovaný	k2eAgFnSc1d3	nejkomplikovanější
část	část	k1gFnSc1	část
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
pák	páka	k1gFnPc2	páka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
propojují	propojovat	k5eAaImIp3nP	propojovat
klávesu	klávesa	k1gFnSc4	klávesa
s	s	k7c7	s
kladívkem	kladívko	k1gNnSc7	kladívko
a	a	k8xC	a
dusítkem	dusítko	k1gNnSc7	dusítko
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stisknutí	stisknutí	k1gNnSc6	stisknutí
klávesy	klávesa	k1gFnSc2	klávesa
se	se	k3xPyFc4	se
ze	z	k7c2	z
struny	struna	k1gFnSc2	struna
zvedne	zvednout	k5eAaPmIp3nS	zvednout
dusítko	dusítko	k1gNnSc1	dusítko
<g/>
,	,	kIx,	,
plstěná	plstěný	k2eAgFnSc1d1	plstěná
krátká	krátký	k2eAgFnSc1d1	krátká
lišta	lišta	k1gFnSc1	lišta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
struně	struna	k1gFnSc3	struna
znít	znít	k5eAaImF	znít
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
struna	struna	k1gFnSc1	struna
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
,	,	kIx,	,
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
plstí	plstit	k5eAaImIp3nP	plstit
opatřené	opatřený	k2eAgNnSc1d1	opatřené
kladívko	kladívko	k1gNnSc4	kladívko
a	a	k8xC	a
rozezní	rozeznět	k5eAaPmIp3nP	rozeznět
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
odskočí	odskočit	k5eAaPmIp3nS	odskočit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
strunu	struna	k1gFnSc4	struna
netlumilo	tlumit	k5eNaImAgNnS	tlumit
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
udeřit	udeřit	k5eAaPmF	udeřit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pustí	pustit	k5eAaPmIp3nS	pustit
hráč	hráč	k1gMnSc1	hráč
klávesu	klávesa	k1gFnSc4	klávesa
<g/>
,	,	kIx,	,
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
dusítko	dusítko	k1gNnSc4	dusítko
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
strunu	struna	k1gFnSc4	struna
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
ztichne	ztichnout	k5eAaPmIp3nS	ztichnout
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
tónu	tón	k1gInSc2	tón
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
po	po	k7c6	po
puštění	puštění	k1gNnSc6	puštění
klávesy	klávesa	k1gFnSc2	klávesa
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
pomocí	pomocí	k7c2	pomocí
pedálu	pedál	k1gInSc2	pedál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlasitost	hlasitost	k1gFnSc1	hlasitost
tónu	tón	k1gInSc2	tón
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
úhozu	úhoz	k1gInSc2	úhoz
na	na	k7c4	na
klávesu	klávesa	k1gFnSc4	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
dusítka	dusítko	k1gNnPc1	dusítko
nadzvednuta	nadzvednut	k2eAgNnPc1d1	nadzvednuto
pomocí	pomocí	k7c2	pomocí
pedálu	pedál	k1gInSc2	pedál
<g/>
,	,	kIx,	,
zní	znět	k5eAaImIp3nS	znět
struna	struna	k1gFnSc1	struna
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
stisku	stisk	k1gInSc2	stisk
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
uvolnění	uvolnění	k1gNnSc6	uvolnění
dosedne	dosednout	k5eAaPmIp3nS	dosednout
příslušné	příslušný	k2eAgNnSc4d1	příslušné
dusítko	dusítko	k1gNnSc4	dusítko
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
strunu	struna	k1gFnSc4	struna
a	a	k8xC	a
tón	tón	k1gInSc4	tón
utlumí	utlumit	k5eAaPmIp3nS	utlumit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klavír	klavír	k1gInSc1	klavír
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
mechanikou	mechanika	k1gFnSc7	mechanika
vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnSc7d2	starší
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
anglickou	anglický	k2eAgFnSc7d1	anglická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
křídlových	křídlový	k2eAgFnPc2d1	křídlová
a	a	k8xC	a
pianinových	pianinový	k2eAgFnPc2d1	pianinový
mechanik	mechanika	k1gFnPc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
drnkátek	drnkátka	k1gFnPc2	drnkátka
starých	starý	k2eAgInPc2d1	starý
spinetů	spinet	k1gInPc2	spinet
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
plstí	plst	k1gFnSc7	plst
krytá	krytý	k2eAgNnPc1d1	kryté
kladívka	kladívko	k1gNnPc1	kladívko
<g/>
,	,	kIx,	,
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
se	se	k3xPyFc4	se
i	i	k9	i
chytače	chytač	k1gInPc1	chytač
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
docílit	docílit	k5eAaPmF	docílit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
a	a	k8xC	a
lehkou	lehký	k2eAgFnSc4d1	lehká
repetici	repetice	k1gFnSc4	repetice
(	(	kIx(	(
<g/>
rychlé	rychlý	k2eAgNnSc4d1	rychlé
opakování	opakování	k1gNnSc4	opakování
téhož	týž	k3xTgInSc2	týž
tónu	tón	k1gInSc2	tón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgFnP	měnit
i	i	k9	i
způsoby	způsoba	k1gFnPc1	způsoba
ovládání	ovládání	k1gNnSc2	ovládání
či	či	k8xC	či
vypínání	vypínání	k1gNnSc2	vypínání
tlumení	tlumení	k1gNnSc2	tlumení
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
užívala	užívat	k5eAaImAgFnS	užívat
dvojrepetiční	dvojrepetiční	k2eAgFnSc1d1	dvojrepetiční
Herz-Erardova	Herz-Erardův	k2eAgFnSc1d1	Herz-Erardův
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
její	její	k3xOp3gFnSc3	její
výrobě	výroba	k1gFnSc3	výroba
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
vybrané	vybraný	k2eAgNnSc4d1	vybrané
habrové	habrový	k2eAgNnSc4d1	habrové
a	a	k8xC	a
podobné	podobný	k2eAgNnSc4d1	podobné
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInSc4d1	speciální
osičkový	osičkový	k2eAgInSc4d1	osičkový
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
speciální	speciální	k2eAgInPc4d1	speciální
druhy	druh	k1gInPc4	druh
plstěných	plstěný	k2eAgInPc2d1	plstěný
<g/>
,	,	kIx,	,
kožených	kožený	k2eAgInPc2d1	kožený
<g/>
,	,	kIx,	,
textilních	textilní	k2eAgInPc2d1	textilní
a	a	k8xC	a
kovových	kovový	k2eAgInPc2d1	kovový
dílků	dílek	k1gInPc2	dílek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tichosti	tichost	k1gFnPc1	tichost
chodu	chod	k1gInSc2	chod
mechaniky	mechanika	k1gFnSc2	mechanika
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
klouzáním	klouzání	k1gNnSc7	klouzání
osiček	osička	k1gFnPc2	osička
v	v	k7c6	v
kašmírových	kašmírový	k2eAgFnPc6d1	kašmírová
vložkách	vložka	k1gFnPc6	vložka
<g/>
,	,	kIx,	,
užitím	užití	k1gNnSc7	užití
plstěných	plstěný	k2eAgMnPc2d1	plstěný
polštářků	polštářek	k1gInPc2	polštářek
a	a	k8xC	a
textilních	textilní	k2eAgInPc2d1	textilní
či	či	k8xC	či
kožených	kožený	k2eAgInPc2d1	kožený
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Kašmírovými	kašmírový	k2eAgNnPc7d1	kašmírové
pouzdry	pouzdro	k1gNnPc7	pouzdro
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřit	k5eAaPmNgInP	opatřit
také	také	k9	také
klávesy	kláves	k1gInPc4	kláves
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
snadnějšímu	snadný	k2eAgNnSc3d2	snazší
klouzání	klouzání	k1gNnSc3	klouzání
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
dílů	díl	k1gInPc2	díl
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
užívá	užívat	k5eAaImIp3nS	užívat
práškový	práškový	k2eAgInSc1d1	práškový
klouzek	klouzek	k1gInSc1	klouzek
[	[	kIx(	[
<g/>
mastek	mastek	k1gInSc1	mastek
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pedály	pedál	k1gInPc4	pedál
===	===	k?	===
</s>
</p>
<p>
<s>
Pedály	pedál	k1gInPc1	pedál
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
pod	pod	k7c7	pod
klaviaturou	klaviatura	k1gFnSc7	klaviatura
a	a	k8xC	a
klavírista	klavírista	k1gMnSc1	klavírista
je	on	k3xPp3gNnPc4	on
ovládá	ovládat	k5eAaImIp3nS	ovládat
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
klavíry	klavír	k1gInPc1	klavír
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
či	či	k8xC	či
častěji	často	k6eAd2	často
tři	tři	k4xCgInPc4	tři
pedály	pedál	k1gInPc4	pedál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
pedál	pedál	k1gInSc1	pedál
pianina	pianino	k1gNnSc2	pianino
a	a	k8xC	a
křídla	křídlo	k1gNnSc2	křídlo
ovládá	ovládat	k5eAaImIp3nS	ovládat
dusítka	dusítko	k1gNnPc4	dusítko
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
tónů	tón	k1gInPc2	tón
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
dusítkem	dusítko	k1gNnSc7	dusítko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
jejich	jejich	k3xOp3gNnSc4	jejich
chvění	chvění	k1gNnSc4	chvění
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
utlumuje	utlumovat	k5eAaImIp3nS	utlumovat
jejich	jejich	k3xOp3gInSc4	jejich
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Pedál	pedál	k1gInSc1	pedál
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
dusítky	dusítko	k1gNnPc7	dusítko
propojen	propojit	k5eAaPmNgMnS	propojit
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
sešlápnutí	sešlápnutí	k1gNnSc6	sešlápnutí
ze	z	k7c2	z
strun	struna	k1gFnPc2	struna
hromadně	hromadně	k6eAd1	hromadně
zvednou	zvednout	k5eAaPmIp3nP	zvednout
a	a	k8xC	a
zvuk	zvuk	k1gInSc4	zvuk
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeznívání	přeznívání	k1gNnSc3	přeznívání
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
u	u	k7c2	u
hammerklavíru	hammerklavír	k1gInSc2	hammerklavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levý	levý	k2eAgInSc1d1	levý
pedál	pedál	k1gInSc1	pedál
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
una	una	k?	una
corda	corda	k1gFnSc1	corda
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgNnSc4d3	nejstarší
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
jeho	jeho	k3xOp3gFnSc4	jeho
funkci	funkce	k1gFnSc6	funkce
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
zejména	zejména	k9	zejména
u	u	k7c2	u
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
pianina	pianino	k1gNnSc2	pianino
ji	on	k3xPp3gFnSc4	on
mívají	mívat	k5eAaImIp3nP	mívat
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
sešlápnutím	sešlápnutí	k1gNnSc7	sešlápnutí
neudeří	udeřit	k5eNaPmIp3nS	udeřit
kladívko	kladívko	k1gNnSc4	kladívko
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
do	do	k7c2	do
dvou	dva	k4xCgMnPc2	dva
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
slabší	slabý	k2eAgFnSc1d2	slabší
a	a	k8xC	a
zabarvuje	zabarvovat	k5eAaImIp3nS	zabarvovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sešlápnutí	sešlápnutí	k1gNnSc6	sešlápnutí
se	se	k3xPyFc4	se
klaviatura	klaviatura	k1gFnSc1	klaviatura
posouvá	posouvat	k5eAaImIp3nS	posouvat
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
vzácněji	vzácně	k6eAd2	vzácně
i	i	k9	i
doleva	doleva	k6eAd1	doleva
<g/>
)	)	kIx)	)
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
kladívko	kladívko	k1gNnSc1	kladívko
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
struny	struna	k1gFnPc4	struna
místo	místo	k7c2	místo
tří	tři	k4xCgMnPc2	tři
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pianin	pianino	k1gNnPc2	pianino
se	se	k3xPyFc4	se
slabšího	slabý	k2eAgInSc2d2	slabší
zvuku	zvuk	k1gInSc2	zvuk
častěji	často	k6eAd2	často
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přisunutím	přisunutí	k1gNnSc7	přisunutí
kladívek	kladívko	k1gNnPc2	kladívko
blíže	blíž	k1gFnSc2	blíž
ke	k	k7c3	k
strunám	struna	k1gFnPc3	struna
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
i	i	k9	i
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
pocitu	pocit	k1gInSc2	pocit
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
snazší	snadný	k2eAgInSc1d2	snadnější
stisk	stisk	k1gInSc1	stisk
kláves	klávesa	k1gFnPc2	klávesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostřední	prostřední	k2eAgInSc1d1	prostřední
pedál	pedál	k1gInSc1	pedál
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sostenuto	sostenuto	k6eAd1	sostenuto
<g/>
,	,	kIx,	,
ovládá	ovládat	k5eAaImIp3nS	ovládat
rovněž	rovněž	k9	rovněž
dusítka	dusítko	k1gNnPc4	dusítko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
všechna	všechen	k3xTgFnSc1	všechen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
zvednuta	zvednout	k5eAaPmNgFnS	zvednout
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
sešlápnutí	sešlápnutí	k1gNnSc2	sešlápnutí
pedálu	pedál	k1gInSc2	pedál
<g/>
.	.	kIx.	.
</s>
<s>
Prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
tak	tak	k9	tak
délku	délka	k1gFnSc4	délka
jen	jen	k9	jen
těch	ten	k3xDgInPc2	ten
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zněly	znět	k5eAaImAgInP	znět
v	v	k7c6	v
momentu	moment	k1gInSc6	moment
sešlápnutí	sešlápnutí	k1gNnSc2	sešlápnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Jean-Louis	Jean-Louis	k1gFnSc2	Jean-Louis
Boisselot	Boisselot	k1gMnSc1	Boisselot
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zdokonalila	zdokonalit	k5eAaPmAgFnS	zdokonalit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
patentovala	patentovat	k5eAaBmAgFnS	patentovat
firma	firma	k1gFnSc1	firma
Steinway	Steinwaa	k1gFnSc2	Steinwaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jej	on	k3xPp3gMnSc4	on
všechny	všechen	k3xTgInPc4	všechen
novější	nový	k2eAgInSc4d2	novější
typy	typ	k1gInPc4	typ
klavíru	klavír	k1gInSc2	klavír
i	i	k8xC	i
některé	některý	k3yIgInPc4	některý
dražší	drahý	k2eAgInPc4d2	dražší
typy	typ	k1gInPc4	typ
pianin	pianino	k1gNnPc2	pianino
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřední	k2eAgInSc1d1	prostřední
pedál	pedál	k1gInSc1	pedál
u	u	k7c2	u
pianina	pianino	k1gNnSc2	pianino
nejčastěji	často	k6eAd3	často
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
tzv.	tzv.	kA	tzv.
tlumícího	tlumící	k2eAgInSc2d1	tlumící
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgMnSc1	první
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předepisoval	předepisovat	k5eAaImAgInS	předepisovat
a	a	k8xC	a
propracoval	propracovat	k5eAaPmAgInS	propracovat
zápis	zápis	k1gInSc1	zápis
použití	použití	k1gNnSc2	použití
pedálu	pedál	k1gInSc2	pedál
v	v	k7c6	v
notovém	notový	k2eAgInSc6d1	notový
partu	part	k1gInSc6	part
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Ladislav	Ladislav	k1gMnSc1	Ladislav
Dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pedálový	pedálový	k2eAgInSc4d1	pedálový
klavír	klavír	k1gInSc4	klavír
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
klavíru	klavír	k1gInSc2	klavír
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
o	o	k7c4	o
pedálovou	pedálový	k2eAgFnSc4d1	pedálová
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc1d1	umožňující
hrát	hrát	k5eAaImF	hrát
basové	basový	k2eAgInPc4d1	basový
tóny	tón	k1gInPc4	tón
nohou	noha	k1gFnPc2	noha
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
varhan	varhany	k1gFnPc2	varhany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
mechanismy	mechanismus	k1gInPc1	mechanismus
===	===	k?	===
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vyráběla	vyrábět	k5eAaImAgNnP	vyrábět
také	také	k9	také
masivní	masivní	k2eAgNnPc1d1	masivní
pianina	pianino	k1gNnPc1	pianino
s	s	k7c7	s
tichým	tichý	k2eAgInSc7d1	tichý
elektromotorem	elektromotor	k1gInSc7	elektromotor
a	a	k8xC	a
pneumatickou	pneumatický	k2eAgFnSc7d1	pneumatická
mechanikou	mechanika	k1gFnSc7	mechanika
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
vkládal	vkládat	k5eAaImAgInS	vkládat
široký	široký	k2eAgInSc1d1	široký
válec	válec	k1gInSc1	válec
s	s	k7c7	s
děrovaným	děrovaný	k2eAgInSc7d1	děrovaný
pruhem	pruh	k1gInSc7	pruh
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
každý	každý	k3xTgInSc1	každý
stisk	stisk	k1gInSc1	stisk
klávesy	klávesa	k1gFnSc2	klávesa
naperforován	naperforovat	k5eAaPmNgInS	naperforovat
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
upravené	upravený	k2eAgNnSc1d1	upravené
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pianola	pianola	k1gFnSc1	pianola
nebo	nebo	k8xC	nebo
mechanický	mechanický	k2eAgInSc1d1	mechanický
automatický	automatický	k2eAgInSc1d1	automatický
klavír	klavír	k1gInSc1	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Sacím	sací	k2eAgInSc7d1	sací
mechanismem	mechanismus	k1gInSc7	mechanismus
se	se	k3xPyFc4	se
skladba	skladba	k1gFnSc1	skladba
snímala	snímat	k5eAaImAgFnS	snímat
z	z	k7c2	z
pásu	pás	k1gInSc2	pás
a	a	k8xC	a
přenášela	přenášet	k5eAaImAgFnS	přenášet
na	na	k7c4	na
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
měchy	měch	k1gInPc4	měch
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
klávesy	kláves	k1gInPc1	kláves
samy	sám	k3xTgInPc1	sám
stiskaly	stiskat	k5eAaImAgInP	stiskat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
měla	mít	k5eAaImAgNnP	mít
dole	dole	k6eAd1	dole
tzv.	tzv.	kA	tzv.
lyru	lyra	k1gFnSc4	lyra
s	s	k7c7	s
pedály	pedál	k1gInPc7	pedál
<g/>
,	,	kIx,	,
ovládajícími	ovládající	k2eAgInPc7d1	ovládající
mutace	mutace	k1gFnPc1	mutace
zvuku	zvuk	k1gInSc2	zvuk
(	(	kIx(	(
<g/>
pohyb	pohyb	k1gInSc1	pohyb
dusítek	dusítko	k1gNnPc2	dusítko
<g/>
,	,	kIx,	,
klávesnice	klávesnice	k1gFnSc2	klávesnice
či	či	k8xC	či
tlumicí	tlumice	k1gFnSc7	tlumice
plstěné	plstěný	k2eAgFnSc2d1	plstěná
lišty	lišta	k1gFnSc2	lišta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pianin	pianino	k1gNnPc2	pianino
k	k	k7c3	k
témuž	týž	k3xTgInSc3	týž
účelu	účel	k1gInSc3	účel
sloužily	sloužit	k5eAaImAgInP	sloužit
2	[number]	k4	2
až	až	k6eAd1	až
3	[number]	k4	3
pedály	pedál	k1gInPc4	pedál
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ruční	ruční	k2eAgInSc1d1	ruční
moderátor	moderátor	k1gInSc1	moderátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
docílení	docílení	k1gNnSc3	docílení
lepší	dobrý	k2eAgFnSc2d2	lepší
barvy	barva	k1gFnSc2	barva
zvuku	zvuk	k1gInSc2	zvuk
někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
užívali	užívat	k5eAaImAgMnP	užívat
kromě	kromě	k7c2	kromě
normálního	normální	k2eAgInSc2d1	normální
chóru	chór	k1gInSc2	chór
strun	struna	k1gFnPc2	struna
téhož	týž	k3xTgInSc2	týž
tónu	tón	k1gInSc2	tón
ještě	ještě	k9	ještě
strunu	struna	k1gFnSc4	struna
alikvótní	alikvótní	k2eAgFnSc4d1	alikvótní
a	a	k8xC	a
u	u	k7c2	u
vysokých	vysoký	k2eAgInPc2d1	vysoký
tónů	tón	k1gInPc2	tón
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
chór	chór	k1gInSc4	chór
i	i	k9	i
4	[number]	k4	4
struny	struna	k1gFnPc4	struna
<g/>
.	.	kIx.	.
</s>
<s>
Nejcitlivější	citlivý	k2eAgFnSc7d3	nejcitlivější
oblastí	oblast	k1gFnSc7	oblast
ladění	ladění	k1gNnSc2	ladění
jsou	být	k5eAaImIp3nP	být
úseky	úsek	k1gInPc1	úsek
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
normálních	normální	k2eAgFnPc2d1	normální
-	-	kIx~	-
zpravidla	zpravidla	k6eAd1	zpravidla
třístrunných	třístrunný	k2eAgInPc2d1	třístrunný
chórů	chór	k1gInPc2	chór
na	na	k7c4	na
struny	struna	k1gFnPc4	struna
ovinuté	ovinutý	k2eAgFnPc4d1	ovinutá
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
při	při	k7c6	při
ladění	ladění	k1gNnSc6	ladění
a	a	k8xC	a
intonaci	intonace	k1gFnSc6	intonace
musí	muset	k5eAaImIp3nS	muset
laborovat	laborovat	k5eAaImF	laborovat
i	i	k9	i
s	s	k7c7	s
plstí	plst	k1gFnSc7	plst
kladívek	kladívko	k1gNnPc2	kladívko
–	–	k?	–
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
úder	úder	k1gInSc4	úder
změkčit	změkčit	k5eAaPmF	změkčit
napícháním	napíchání	k1gNnSc7	napíchání
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
zvýšit	zvýšit	k5eAaPmF	zvýšit
třeba	třeba	k6eAd1	třeba
napuštěním	napuštění	k1gNnSc7	napuštění
plsti	plst	k1gFnSc2	plst
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
typy	typ	k1gInPc1	typ
mívaly	mívat	k5eAaImAgInP	mívat
ladicí	ladicí	k2eAgInPc1d1	ladicí
kolíky	kolík	k1gInPc1	kolík
ploché	plochý	k2eAgInPc1d1	plochý
<g/>
,	,	kIx,	,
novější	nový	k2eAgInPc1d2	novější
mají	mít	k5eAaImIp3nP	mít
osmihranné	osmihranný	k2eAgInPc1d1	osmihranný
s	s	k7c7	s
jemnými	jemný	k2eAgFnPc7d1	jemná
šroubovými	šroubový	k2eAgFnPc7d1	šroubová
drážkami	drážka	k1gFnPc7	drážka
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
záleží	záležet	k5eAaImIp3nS	záležet
i	i	k9	i
na	na	k7c6	na
úpravě	úprava	k1gFnSc6	úprava
díry	díra	k1gFnSc2	díra
pro	pro	k7c4	pro
kovovou	kovový	k2eAgFnSc4d1	kovová
strunu	struna	k1gFnSc4	struna
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
na	na	k7c6	na
dokonalosti	dokonalost	k1gFnSc6	dokonalost
agrafů	agraf	k1gInPc2	agraf
upravujících	upravující	k2eAgInPc2d1	upravující
chóry	chór	k1gInPc7	chór
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
klavírů	klavír	k1gInPc2	klavír
==	==	k?	==
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
piano	piano	k1gNnSc1	piano
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
podoby	podoba	k1gFnPc4	podoba
<g/>
:	:	kIx,	:
křídlo	křídlo	k1gNnSc1	křídlo
a	a	k8xC	a
pianino	pianino	k1gNnSc1	pianino
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
řadu	řada	k1gFnSc4	řada
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc6	tvar
či	či	k8xC	či
použitém	použitý	k2eAgInSc6d1	použitý
materiálu	materiál	k1gInSc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyskytovaly	vyskytovat	k5eAaImAgFnP	vyskytovat
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
specifické	specifický	k2eAgFnPc1d1	specifická
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
víceméně	víceméně	k9	víceméně
upustilo	upustit	k5eAaPmAgNnS	upustit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
žirafí	žirafí	k2eAgInSc4d1	žirafí
krk	krk	k1gInSc4	krk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
menších	malý	k2eAgNnPc2d2	menší
pian	piano	k1gNnPc2	piano
bývá	bývat	k5eAaImIp3nS	bývat
u	u	k7c2	u
obou	dva	k4xCgFnPc2	dva
podob	podoba	k1gFnPc2	podoba
křížení	křížení	k1gNnSc2	křížení
strun	struna	k1gFnPc2	struna
a	a	k8xC	a
dělená	dělený	k2eAgFnSc1d1	dělená
kobylka	kobylka	k1gFnSc1	kobylka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Křídlo	křídlo	k1gNnSc1	křídlo
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
skříni	skříň	k1gFnSc6	skříň
křídla	křídlo	k1gNnSc2	křídlo
jsou	být	k5eAaImIp3nP	být
rám	rám	k1gInSc1	rám
a	a	k8xC	a
struny	struna	k1gFnPc1	struna
uloženy	uložit	k5eAaPmNgFnP	uložit
vodorovně	vodorovně	k6eAd1	vodorovně
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
taženy	tažen	k2eAgFnPc1d1	tažena
kolmo	kolmo	k6eAd1	kolmo
ke	k	k7c3	k
klaviatuře	klaviatura	k1gFnSc3	klaviatura
a	a	k8xC	a
leží	ležet	k5eAaImIp3nP	ležet
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
křídla	křídlo	k1gNnSc2	křídlo
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
od	od	k7c2	od
zhruba	zhruba	k6eAd1	zhruba
jednoho	jeden	k4xCgMnSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
až	až	k8xS	až
ke	k	k7c3	k
třem	tři	k4xCgInPc3	tři
metrům	metr	k1gInPc3	metr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
klade	klást	k5eAaImIp3nS	klást
značné	značný	k2eAgInPc4d1	značný
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
křídlo	křídlo	k1gNnSc1	křídlo
umístěno	umístit	k5eAaPmNgNnS	umístit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
výšku	výška	k1gFnSc4	výška
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pro	pro	k7c4	pro
patřičnou	patřičný	k2eAgFnSc4d1	patřičná
rezonanci	rezonance	k1gFnSc4	rezonance
zvuku	zvuk	k1gInSc2	zvuk
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
i	i	k8xC	i
určitá	určitý	k2eAgFnSc1d1	určitá
výška	výška	k1gFnSc1	výška
stropu	strop	k1gInSc2	strop
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
i	i	k8xC	i
křídla	křídlo	k1gNnPc1	křídlo
tvaru	tvar	k1gInSc2	tvar
psacího	psací	k2eAgInSc2d1	psací
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pianino	pianino	k1gNnSc1	pianino
===	===	k?	===
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
předchůdce	předchůdce	k1gMnSc1	předchůdce
pianin	pianino	k1gNnPc2	pianino
(	(	kIx(	(
<g/>
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
sirenion	sirenion	k1gInSc1	sirenion
<g/>
)	)	kIx)	)
rakouský	rakouský	k2eAgMnSc1d1	rakouský
skladatel	skladatel	k1gMnSc1	skladatel
Johann	Johann	k1gMnSc1	Johann
Josef	Josef	k1gMnSc1	Josef
Promberger	Promberger	k1gMnSc1	Promberger
<g/>
.	.	kIx.	.
</s>
<s>
Pianino	pianino	k1gNnSc1	pianino
se	se	k3xPyFc4	se
od	od	k7c2	od
křídla	křídlo	k1gNnSc2	křídlo
liší	lišit	k5eAaImIp3nS	lišit
rozměry	rozměr	k1gInPc7	rozměr
i	i	k8xC	i
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Rám	rám	k1gInSc1	rám
a	a	k8xC	a
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
svisle	svisle	k6eAd1	svisle
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
kladívka	kladívko	k1gNnSc2	kladívko
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
klidovém	klidový	k2eAgInSc6d1	klidový
stavu	stav	k1gInSc6	stav
svislou	svislý	k2eAgFnSc4d1	svislá
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
struny	struna	k1gFnPc1	struna
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
úsporu	úspora	k1gFnSc4	úspora
místa	místo	k1gNnSc2	místo
kříží	křížit	k5eAaImIp3nP	křížit
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
přednostmi	přednost	k1gFnPc7	přednost
pianina	pianino	k1gNnSc2	pianino
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
nižší	nízký	k2eAgInPc1d2	nižší
výrobní	výrobní	k2eAgInPc1d1	výrobní
náklady	náklad	k1gInPc1	náklad
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
velikost	velikost	k1gFnSc1	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
klavíru	klavír	k1gInSc2	klavír
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Raný	raný	k2eAgInSc1d1	raný
vývoj	vývoj	k1gInSc1	vývoj
klavíru	klavír	k1gInSc2	klavír
===	===	k?	===
</s>
</p>
<p>
<s>
Praotcem	praotec	k1gMnSc7	praotec
klavíru	klavír	k1gInSc2	klavír
byl	být	k5eAaImAgInS	být
řecký	řecký	k2eAgInSc1d1	řecký
monochord	monochord	k1gInSc1	monochord
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
přístroj	přístroj	k1gInSc1	přístroj
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
hudebních	hudební	k2eAgInPc2d1	hudební
intervalů	interval	k1gInPc2	interval
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
struna	struna	k1gFnSc1	struna
napjatá	napjatý	k2eAgFnSc1d1	napjatá
na	na	k7c6	na
ozvučné	ozvučný	k2eAgFnSc6d1	ozvučná
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
posuvnou	posuvný	k2eAgFnSc7d1	posuvná
kobylkou	kobylka	k1gFnSc7	kobylka
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c6	na
různě	různě	k6eAd1	různě
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
úseky	úsek	k1gInPc4	úsek
struny	struna	k1gFnSc2	struna
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozeznívaly	rozeznívat	k5eAaImAgFnP	rozeznívat
paličkou	palička	k1gFnSc7	palička
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
plektron	plektron	k1gNnSc1	plektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvýšením	zvýšení	k1gNnSc7	zvýšení
počtu	počet	k1gInSc2	počet
strun	struna	k1gFnPc2	struna
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
polychord	polychord	k1gInSc1	polychord
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
myšlen	myšlen	k2eAgMnSc1d1	myšlen
jako	jako	k8xS	jako
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgMnS	být
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
názvy	název	k1gInPc7	název
(	(	kIx(	(
<g/>
helikon	helikon	k1gInSc1	helikon
<g/>
,	,	kIx,	,
manicorde	manicorde	k6eAd1	manicorde
<g/>
,	,	kIx,	,
manicordium	manicordium	k1gNnSc1	manicordium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Připojením	připojení	k1gNnSc7	připojení
klávesnice	klávesnice	k1gFnSc2	klávesnice
a	a	k8xC	a
trsacího	trsací	k2eAgInSc2d1	trsací
mechanizmu	mechanizmus	k1gInSc2	mechanizmus
rozechvívajícího	rozechvívající	k2eAgInSc2d1	rozechvívající
struny	struna	k1gFnSc2	struna
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
klavichord	klavichord	k1gInSc1	klavichord
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
šachovnice	šachovnice	k1gFnSc1	šachovnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
exaquir	exaquira	k1gFnPc2	exaquira
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podstatným	podstatný	k2eAgInSc7d1	podstatný
rysem	rys	k1gInSc7	rys
všech	všecek	k3xTgInPc2	všecek
těchto	tento	k3xDgInPc2	tento
nástrojů	nástroj	k1gInPc2	nástroj
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
naladěné	naladěný	k2eAgInPc1d1	naladěný
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
výška	výška	k1gFnSc1	výška
tónu	tón	k1gInSc2	tón
mění	měnit	k5eAaImIp3nS	měnit
zkracováním	zkracování	k1gNnSc7	zkracování
délky	délka	k1gFnSc2	délka
struny	struna	k1gFnSc2	struna
posuvem	posuv	k1gInSc7	posuv
kobylky	kobylka	k1gFnSc2	kobylka
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
jazýčky	jazýček	k1gInPc1	jazýček
dotknout	dotknout	k5eAaPmF	dotknout
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
klavichordem	klavichord	k1gInSc7	klavichord
vzniká	vznikat	k5eAaImIp3nS	vznikat
klavicembalo	klavicembat	k5eAaImAgNnS	klavicembat
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
klavichordu	klavichord	k1gInSc2	klavichord
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
různě	různě	k6eAd1	různě
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
naladěna	naladit	k5eAaPmNgFnS	naladit
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
výšku	výška	k1gFnSc4	výška
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
tón	tón	k1gInSc4	tón
struna	struna	k1gFnSc1	struna
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dvě	dva	k4xCgNnPc4	dva
až	až	k9	až
tři	tři	k4xCgNnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
byly	být	k5eAaImAgInP	být
připojeny	připojen	k2eAgInPc1d1	připojen
rejstříky	rejstřík	k1gInPc1	rejstřík
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
různými	různý	k2eAgInPc7d1	různý
technickými	technický	k2eAgInPc7d1	technický
prostředky	prostředek	k1gInPc7	prostředek
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
měnit	měnit	k5eAaImF	měnit
barvu	barva	k1gFnSc4	barva
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Hudebník	hudebník	k1gMnSc1	hudebník
seděl	sedět	k5eAaImAgMnS	sedět
čelem	čelo	k1gNnSc7	čelo
napříč	napříč	k7c3	napříč
strunám	struna	k1gFnPc3	struna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
nástroji	nástroj	k1gInSc3	nástroj
říkalo	říkat	k5eAaImAgNnS	říkat
virginal	virginal	k1gInSc4	virginal
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
spinet	spineto	k1gNnPc2	spineto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
vývojem	vývoj	k1gInSc7	vývoj
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
posazení	posazení	k1gNnSc1	posazení
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Hudebník	hudebník	k1gMnSc1	hudebník
byl	být	k5eAaImAgMnS	být
přesazen	přesadit	k5eAaPmNgMnS	přesadit
na	na	k7c4	na
užší	úzký	k2eAgFnSc4d2	užší
stranu	strana	k1gFnSc4	strana
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
struny	struna	k1gFnPc1	struna
byly	být	k5eAaImAgFnP	být
nataženy	natáhnout	k5eAaPmNgFnP	natáhnout
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podobal	podobat	k5eAaImAgMnS	podobat
cimbálu	cimbál	k1gInSc2	cimbál
a	a	k8xC	a
vžil	vžít	k5eAaPmAgMnS	vžít
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
název	název	k1gInSc4	název
clavicembalo	clavicembalo	k1gNnSc1	clavicembalo
nebo	nebo	k8xC	nebo
zkráceně	zkráceně	k6eAd1	zkráceně
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
harpsichord	harpsichord	k1gMnSc1	harpsichord
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
podoby	podoba	k1gFnSc2	podoba
s	s	k7c7	s
ptačím	ptačí	k2eAgNnSc7d1	ptačí
křídlem	křídlo	k1gNnSc7	křídlo
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
konstruovanému	konstruovaný	k2eAgNnSc3d1	konstruované
cembalu	cembalo	k1gNnSc3	cembalo
říkalo	říkat	k5eAaImAgNnS	říkat
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
poslední	poslední	k2eAgFnSc2d1	poslední
vývojové	vývojový	k2eAgFnSc2d1	vývojová
etapy	etapa	k1gFnSc2	etapa
vedla	vést	k5eAaImAgFnS	vést
snaha	snaha	k1gFnSc1	snaha
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
větších	veliký	k2eAgInPc2d2	veliký
dynamických	dynamický	k2eAgInPc2d1	dynamický
rozdílů	rozdíl	k1gInPc2	rozdíl
bez	bez	k7c2	bez
složité	složitý	k2eAgFnSc2d1	složitá
mechaniky	mechanika	k1gFnSc2	mechanika
několika	několik	k4yIc2	několik
klaviatur	klaviatura	k1gFnPc2	klaviatura
a	a	k8xC	a
rejstříků	rejstřík	k1gInPc2	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc1	řešení
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
změně	změna	k1gFnSc6	změna
způsobu	způsob	k1gInSc2	způsob
rozechvívání	rozechvívání	k1gNnSc2	rozechvívání
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
jazýčku	jazýček	k1gInSc2	jazýček
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
palička	palička	k1gFnSc1	palička
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
různou	různý	k2eAgFnSc7d1	různá
silou	síla	k1gFnSc7	síla
úderu	úder	k1gInSc2	úder
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
různě	různě	k6eAd1	různě
silného	silný	k2eAgInSc2d1	silný
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
tutéž	týž	k3xTgFnSc4	týž
myšlenku	myšlenka	k1gFnSc4	myšlenka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
zralá	zralý	k2eAgFnSc1d1	zralá
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
několik	několik	k4yIc4	několik
lidí	člověk	k1gMnPc2	člověk
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
kladívkového	kladívkový	k2eAgInSc2d1	kladívkový
klavíru	klavír	k1gInSc2	klavír
byli	být	k5eAaImAgMnP	být
hned	hned	k6eAd1	hned
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patrně	patrně	k6eAd1	patrně
prvním	první	k4xOgMnSc6	první
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
kustod	kustod	k1gMnSc1	kustod
sbírky	sbírka	k1gFnSc2	sbírka
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
rodiny	rodina	k1gFnSc2	rodina
Medicejských	Medicejský	k2eAgInPc2d1	Medicejský
Bartolomeo	Bartolomeo	k6eAd1	Bartolomeo
Cristofori	Cristofor	k1gMnPc7	Cristofor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
model	model	k1gInSc4	model
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
roku	rok	k1gInSc2	rok
1708	[number]	k4	1708
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgInS	nazvat
jej	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
stromento	stromento	k1gNnSc4	stromento
col	cola	k1gFnPc2	cola
piano	piano	k6eAd1	piano
e	e	k0	e
forte	forte	k1gNnPc3	forte
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
nástroj	nástroj	k1gInSc1	nástroj
se	s	k7c7	s
slabou	slabý	k2eAgFnSc7d1	slabá
a	a	k8xC	a
silnou	silný	k2eAgFnSc7d1	silná
dynamikou	dynamika	k1gFnSc7	dynamika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
tři	tři	k4xCgFnPc1	tři
Cristoforiho	Cristofori	k1gMnSc4	Cristofori
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
myšlenky	myšlenka	k1gFnSc2	myšlenka
kladívkové	kladívkový	k2eAgFnSc2d1	kladívková
mechaniky	mechanika	k1gFnSc2	mechanika
měl	mít	k5eAaImAgMnS	mít
jiný	jiný	k2eAgMnSc1d1	jiný
Ital	Ital	k1gMnSc1	Ital
<g/>
,	,	kIx,	,
Scipione	Scipion	k1gInSc5	Scipion
Maffe	Maff	k1gInSc5	Maff
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roku	rok	k1gInSc2	rok
1711	[number]	k4	1711
publikoval	publikovat	k5eAaBmAgInS	publikovat
oslavný	oslavný	k2eAgInSc1d1	oslavný
spis	spis	k1gInSc1	spis
na	na	k7c4	na
kladívkový	kladívkový	k2eAgInSc4d1	kladívkový
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
výrobce	výrobce	k1gMnSc1	výrobce
cembal	cembalo	k1gNnPc2	cembalo
Jean	Jean	k1gMnSc1	Jean
Marius	Marius	k1gMnSc1	Marius
předvedl	předvést	k5eAaPmAgMnS	předvést
roku	rok	k1gInSc2	rok
1716	[number]	k4	1716
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
královské	královský	k2eAgFnSc6d1	královská
akademii	akademie	k1gFnSc6	akademie
tři	tři	k4xCgInPc4	tři
modely	model	k1gInPc4	model
podobného	podobný	k2eAgInSc2d1	podobný
typu	typ	k1gInSc2	typ
jako	jako	k8xS	jako
Cristofori	Cristofor	k1gFnSc2	Cristofor
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
roku	rok	k1gInSc2	rok
1717	[number]	k4	1717
německý	německý	k2eAgMnSc1d1	německý
varhaník	varhaník	k1gMnSc1	varhaník
Christoph	Christoph	k1gMnSc1	Christoph
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Schröter	Schröter	k1gMnSc1	Schröter
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
modely	model	k1gInPc1	model
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
poněkud	poněkud	k6eAd1	poněkud
odlišném	odlišný	k2eAgInSc6d1	odlišný
principu	princip	k1gInSc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgInS	potýkat
s	s	k7c7	s
technickými	technický	k2eAgInPc7d1	technický
problémy	problém	k1gInPc7	problém
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
by	by	kYmCp3nS	by
i	i	k9	i
zapadl	zapadnout	k5eAaPmAgInS	zapadnout
nebýt	být	k5eNaImF	být
saského	saský	k2eAgMnSc4d1	saský
nástrojaře	nástrojař	k1gMnSc4	nástrojař
Gottfrieda	Gottfried	k1gMnSc4	Gottfried
Silbermana	Silberman	k1gMnSc4	Silberman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
zahájil	zahájit	k5eAaPmAgInS	zahájit
prakticky	prakticky	k6eAd1	prakticky
masovou	masový	k2eAgFnSc4d1	masová
produkci	produkce	k1gFnSc4	produkce
zdokonaleného	zdokonalený	k2eAgInSc2d1	zdokonalený
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nazval	nazvat	k5eAaBmAgInS	nazvat
Pianoforte	Pianofort	k1gInSc5	Pianofort
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
zkráceně	zkráceně	k6eAd1	zkráceně
piano	piano	k6eAd1	piano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
téměř	téměř	k6eAd1	téměř
celých	celý	k2eAgFnPc2d1	celá
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
existovaly	existovat	k5eAaImAgInP	existovat
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
jak	jak	k6eAd1	jak
klavichord	klavichord	k6eAd1	klavichord
a	a	k8xC	a
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
piano	piano	k6eAd1	piano
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Johann	Johann	k1gMnSc1	Johann
Sebastian	Sebastian	k1gMnSc1	Sebastian
Bach	Bach	k1gMnSc1	Bach
piano	piano	k6eAd1	piano
dobře	dobře	k6eAd1	dobře
znal	znát	k5eAaImAgMnS	znát
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
některá	některý	k3yIgNnPc4	některý
technická	technický	k2eAgNnPc4d1	technické
zlepšení	zlepšení	k1gNnPc4	zlepšení
<g/>
;	;	kIx,	;
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
i	i	k8xC	i
koncertní	koncertní	k2eAgFnSc6d1	koncertní
činnosti	činnost	k1gFnSc6	činnost
však	však	k9	však
zůstal	zůstat	k5eAaPmAgInS	zůstat
věrný	věrný	k2eAgInSc1d1	věrný
cembalu	cembalo	k1gNnSc3	cembalo
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgNnPc7d1	jiné
např.	např.	kA	např.
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
nebo	nebo	k8xC	nebo
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
však	však	k9	však
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dává	dávat	k5eAaImIp3nS	dávat
mohutnější	mohutný	k2eAgInSc4d2	mohutnější
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
dynamických	dynamický	k2eAgFnPc2d1	dynamická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozšířením	rozšíření	k1gNnSc7	rozšíření
nástroje	nástroj	k1gInSc2	nástroj
i	i	k8xC	i
do	do	k7c2	do
měšťanských	měšťanský	k2eAgFnPc2d1	měšťanská
domácností	domácnost	k1gFnPc2	domácnost
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
piano	piano	k6eAd1	piano
klavichord	klavichord	k1gInSc1	klavichord
a	a	k8xC	a
cembalo	cembalo	k1gNnSc1	cembalo
prakticky	prakticky	k6eAd1	prakticky
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
renesanci	renesance	k1gFnSc3	renesance
cembala	cembalo	k1gNnSc2	cembalo
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
umělci	umělec	k1gMnPc1	umělec
i	i	k9	i
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
rozpoznali	rozpoznat	k5eAaPmAgMnP	rozpoznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skladby	skladba	k1gFnPc1	skladba
psané	psaný	k2eAgFnPc1d1	psaná
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
znějí	znět	k5eAaImIp3nP	znět
autenticky	autenticky	k6eAd1	autenticky
a	a	k8xC	a
barevněji	barevně	k6eAd2	barevně
na	na	k7c6	na
originálním	originální	k2eAgInSc6d1	originální
nástroji	nástroj	k1gInSc6	nástroj
a	a	k8xC	a
klavír	klavír	k1gInSc1	klavír
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
nedokonalou	dokonalý	k2eNgFnSc7d1	nedokonalá
náhražkou	náhražka	k1gFnSc7	náhražka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
klavíru	klavír	k1gInSc2	klavír
dnešního	dnešní	k2eAgInSc2d1	dnešní
typu	typ	k1gInSc2	typ
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
řada	řada	k1gFnSc1	řada
dílen	dílna	k1gFnPc2	dílna
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
klavírů	klavír	k1gInPc2	klavír
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pařížský	pařížský	k2eAgInSc1d1	pařížský
Erard	Erard	k1gInSc1	Erard
<g/>
,	,	kIx,	,
londýnský	londýnský	k2eAgInSc1d1	londýnský
Broadwood	Broadwood	k1gInSc1	Broadwood
či	či	k8xC	či
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
Bösendorfer	Bösendorfer	k1gInSc1	Bösendorfer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
prodělal	prodělat	k5eAaPmAgInS	prodělat
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
byla	být	k5eAaImAgFnS	být
specifikována	specifikován	k2eAgFnSc1d1	specifikována
funkce	funkce	k1gFnSc1	funkce
pedálů	pedál	k1gInPc2	pedál
či	či	k8xC	či
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
rozsah	rozsah	k1gInSc1	rozsah
z	z	k7c2	z
pěti	pět	k4xCc2	pět
oktáv	oktáva	k1gFnPc2	oktáva
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
Mozarta	Mozart	k1gMnSc2	Mozart
na	na	k7c4	na
dnešních	dnešní	k2eAgNnPc2d1	dnešní
sedm	sedm	k4xCc4	sedm
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
klavírní	klavírní	k2eAgFnSc1d1	klavírní
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
pian	piano	k1gNnPc2	piano
i	i	k8xC	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
polovině	polovina	k1gFnSc3	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
již	již	k6eAd1	již
piano	piano	k6eAd1	piano
vypadalo	vypadat	k5eAaImAgNnS	vypadat
a	a	k8xC	a
fungovalo	fungovat	k5eAaImAgNnS	fungovat
jako	jako	k9	jako
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
klavíru	klavír	k1gInSc2	klavír
==	==	k?	==
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
klavíru	klavír	k1gInSc2	klavír
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
finančně	finančně	k6eAd1	finančně
i	i	k9	i
technicky	technicky	k6eAd1	technicky
náročný	náročný	k2eAgInSc1d1	náročný
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
od	od	k7c2	od
pokácení	pokácení	k1gNnSc2	pokácení
stromů	strom	k1gInPc2	strom
k	k	k7c3	k
poslednímu	poslední	k2eAgNnSc3d1	poslední
doladění	doladění	k1gNnSc3	doladění
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
čtyři	čtyři	k4xCgInPc1	čtyři
až	až	k6eAd1	až
šest	šest	k4xCc1	šest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
výběrem	výběr	k1gInSc7	výběr
vhodného	vhodný	k2eAgNnSc2d1	vhodné
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
smrkové	smrkový	k2eAgNnSc1d1	smrkové
dřevo	dřevo	k1gNnSc1	dřevo
z	z	k7c2	z
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
česká	český	k2eAgFnSc1d1	Česká
firma	firma	k1gFnSc1	firma
Petrof	Petrof	k1gInSc1	Petrof
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
řada	řada	k1gFnSc1	řada
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
každý	každý	k3xTgInSc1	každý
klavír	klavír	k1gInSc1	klavír
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
z	z	k7c2	z
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
dřev	dřevo	k1gNnPc2	dřevo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
voleny	volen	k2eAgFnPc1d1	volena
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
vlastností	vlastnost	k1gFnPc2	vlastnost
(	(	kIx(	(
<g/>
měkkost	měkkost	k1gFnSc4	měkkost
či	či	k8xC	či
schopnost	schopnost	k1gFnSc4	schopnost
nést	nést	k5eAaImF	nést
zvuk	zvuk	k1gInSc4	zvuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
řeže	řezat	k5eAaImIp3nS	řezat
na	na	k7c4	na
fošny	fošna	k1gFnPc4	fošna
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
proschnout	proschnout	k5eAaPmF	proschnout
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
vyválcovat	vyválcovat	k5eAaPmF	vyválcovat
struny	struna	k1gFnPc4	struna
potřebné	potřebný	k2eAgFnSc2d1	potřebná
tloušťky	tloušťka	k1gFnSc2	tloušťka
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většina	k1gFnSc7	většina
ocelové	ocelový	k2eAgInPc1d1	ocelový
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
jejich	jejich	k3xOp3gFnPc4	jejich
různé	různý	k2eAgFnPc4d1	různá
frekvence	frekvence	k1gFnPc4	frekvence
kmitání	kmitání	k1gNnSc2	kmitání
<g/>
,	,	kIx,	,
v	v	k7c6	v
basovém	basový	k2eAgInSc6d1	basový
rejstříku	rejstřík	k1gInSc6	rejstřík
jsou	být	k5eAaImIp3nP	být
ocelová	ocelový	k2eAgNnPc1d1	ocelové
jádra	jádro	k1gNnPc1	jádro
strun	struna	k1gFnPc2	struna
opřádaná	opřádaný	k2eAgNnPc4d1	opřádaný
měděným	měděný	k2eAgInSc7d1	měděný
drátem	drát	k1gInSc7	drát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
fází	fáze	k1gFnSc7	fáze
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
tónování	tónování	k1gNnSc1	tónování
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
intonování	intonování	k1gNnSc4	intonování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tónování	tónování	k1gNnSc2	tónování
===	===	k?	===
</s>
</p>
<p>
<s>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přibližné	přibližný	k2eAgNnSc4d1	přibližné
vyladění	vyladění	k1gNnSc4	vyladění
strun	struna	k1gFnPc2	struna
na	na	k7c4	na
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
mechaniky	mechanika	k1gFnSc2	mechanika
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
klavír	klavír	k1gInSc1	klavír
je	být	k5eAaImIp3nS	být
pozvolna	pozvolna	k6eAd1	pozvolna
připravován	připravován	k2eAgInSc1d1	připravován
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
tlak	tlak	k1gInSc4	tlak
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Intonování	intonování	k1gNnSc2	intonování
===	===	k?	===
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
perfektní	perfektní	k2eAgInSc4d1	perfektní
stav	stav	k1gInSc4	stav
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gNnPc2	jeho
kladívek	kladívko	k1gNnPc2	kladívko
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poslední	poslední	k2eAgFnSc4d1	poslední
úpravu	úprava	k1gFnSc4	úprava
nového	nový	k2eAgInSc2d1	nový
klavíru	klavír	k1gInSc2	klavír
a	a	k8xC	a
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
zvukové	zvukový	k2eAgFnSc6d1	zvuková
úpravě	úprava	k1gFnSc6	úprava
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
úkolem	úkol	k1gInSc7	úkol
ladiče	ladič	k1gMnSc2	ladič
je	být	k5eAaImIp3nS	být
odstranit	odstranit	k5eAaPmF	odstranit
veškeré	veškerý	k3xTgInPc4	veškerý
negativní	negativní	k2eAgInPc4d1	negativní
vlivy	vliv	k1gInPc4	vliv
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nS	by
mohly	moct	k5eAaImAgFnP	moct
narušit	narušit	k5eAaPmF	narušit
kulturu	kultura	k1gFnSc4	kultura
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
úprava	úprava	k1gFnSc1	úprava
kladívek	kladívko	k1gNnPc2	kladívko
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
broušením	broušení	k1gNnSc7	broušení
kladívkové	kladívkový	k2eAgFnSc2d1	kladívková
plsti	plst	k1gFnSc2	plst
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
pomocí	pomocí	k7c2	pomocí
intonačních	intonační	k2eAgFnPc2d1	intonační
jehel	jehla	k1gFnPc2	jehla
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
tónové	tónový	k2eAgFnSc2d1	tónová
oblasti	oblast	k1gFnSc2	oblast
ve	v	k7c6	v
správném	správný	k2eAgInSc6d1	správný
poměru	poměr	k1gInSc6	poměr
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
nástroj	nástroj	k1gInSc1	nástroj
zkompletován	zkompletován	k2eAgInSc1d1	zkompletován
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
leštění	leštění	k1gNnSc2	leštění
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
povrchu	povrch	k1gInSc2	povrch
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ladí	ladit	k5eAaImIp3nP	ladit
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
struny	struna	k1gFnPc1	struna
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
ladicích	ladicí	k2eAgInPc2d1	ladicí
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
používá	používat	k5eAaImIp3nS	používat
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
temperované	temperovaný	k2eAgNnSc4d1	temperované
ladění	ladění	k1gNnSc4	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ladí	ladit	k5eAaImIp3nS	ladit
ladičkou	ladička	k1gFnSc7	ladička
o	o	k7c6	o
kmitočtu	kmitočet	k1gInSc6	kmitočet
440	[number]	k4	440
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
435	[number]	k4	435
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
začíná	začínat	k5eAaImIp3nS	začínat
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
prosazovat	prosazovat	k5eAaImF	prosazovat
ladění	ladění	k1gNnSc4	ladění
o	o	k7c6	o
vyšším	vysoký	k2eAgInSc6d2	vyšší
kmitočtu	kmitočet	k1gInSc6	kmitočet
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
442	[number]	k4	442
Hz	Hz	kA	Hz
nebo	nebo	k8xC	nebo
443	[number]	k4	443
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
piana	piano	k1gNnSc2	piano
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
firmy	firma	k1gFnPc1	firma
<g/>
:	:	kIx,	:
Petrof	Petrof	k1gInSc1	Petrof
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
(	(	kIx(	(
<g/>
stále	stále	k6eAd1	stále
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Förster	Förster	k1gInSc1	Förster
v	v	k7c6	v
Jiříkově	Jiříkův	k2eAgInSc6d1	Jiříkův
(	(	kIx(	(
<g/>
po	po	k7c6	po
reprivatizaci	reprivatizace	k1gFnSc6	reprivatizace
dále	daleko	k6eAd2	daleko
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Scholze	Scholze	k1gFnSc1	Scholze
ve	v	k7c6	v
Varnsdorfu	Varnsdorf	k1gInSc6	Varnsdorf
(	(	kIx(	(
<g/>
skončila	skončit	k5eAaPmAgFnS	skončit
r.	r.	kA	r.
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rösler	Rösler	k1gInSc1	Rösler
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Lípě	lípa	k1gFnSc6	lípa
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
skončila	skončit	k5eAaPmAgFnS	skončit
před	před	k7c4	před
r.	r.	kA	r.
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brož	brož	k1gFnSc1	brož
ve	v	k7c6	v
Velimi	Veli	k1gFnPc7	Veli
(	(	kIx(	(
<g/>
skončila	skončit	k5eAaPmAgFnS	skončit
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dalibor	Dalibor	k1gMnSc1	Dalibor
v	v	k7c6	v
Zákolanech	Zákolan	k1gInPc6	Zákolan
(	(	kIx(	(
<g/>
skončila	skončit	k5eAaPmAgFnS	skončit
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Koch	Koch	k1gMnSc1	Koch
a	a	k8xC	a
Korselt	Korselt	k1gMnSc1	Korselt
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
(	(	kIx(	(
<g/>
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jackel	Jackel	k1gInSc1	Jackel
v	v	k7c6	v
Jablonci	Jablonec	k1gInSc6	Jablonec
n.	n.	k?	n.
N.	N.	kA	N.
(	(	kIx(	(
<g/>
zanikla	zaniknout	k5eAaPmAgNnP	zaniknout
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hofmann	Hofmann	k1gInSc4	Hofmann
a	a	k8xC	a
Czerny	Czern	k1gInPc4	Czern
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
firma	firma	k1gFnSc1	firma
Bohemia	bohemia	k1gFnSc1	bohemia
přestěhována	přestěhovat	k5eAaPmNgFnS	přestěhovat
do	do	k7c2	do
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
odkoupena	odkoupit	k5eAaPmNgFnS	odkoupit
firmou	firma	k1gFnSc7	firma
Bechstein	Bechstein	k1gMnSc1	Bechstein
<g/>
;	;	kIx,	;
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bechstein	Bechsteina	k1gFnPc2	Bechsteina
Europe	Europ	k1gInSc5	Europ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Péče	péče	k1gFnSc1	péče
o	o	k7c4	o
klavír	klavír	k1gInSc4	klavír
==	==	k?	==
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgMnS	zůstat
klavír	klavír	k1gInSc4	klavír
stále	stále	k6eAd1	stále
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
ho	on	k3xPp3gMnSc4	on
uchovávat	uchovávat	k5eAaImF	uchovávat
ve	v	k7c6	v
stabilním	stabilní	k2eAgNnSc6d1	stabilní
prostředí	prostředí	k1gNnSc6	prostředí
bez	bez	k7c2	bez
změn	změna	k1gFnPc2	změna
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
vlhkosti	vlhkost	k1gFnSc2	vlhkost
apod.	apod.	kA	apod.
Rovněž	rovněž	k9	rovněž
časté	častý	k2eAgNnSc1d1	časté
stěhování	stěhování	k1gNnSc1	stěhování
klavíru	klavír	k1gInSc2	klavír
škodí	škodit	k5eAaImIp3nS	škodit
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
změnami	změna	k1gFnPc7	změna
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
nárazy	náraz	k1gInPc4	náraz
při	při	k7c6	při
přenášení	přenášení	k1gNnSc4	přenášení
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
,	,	kIx,	,
struny	struna	k1gFnPc4	struna
aj.	aj.	kA	aj.
V	v	k7c4	v
místnosti	místnost	k1gFnPc4	místnost
by	by	kYmCp3nS	by
klavír	klavír	k1gInSc1	klavír
neměl	mít	k5eNaImAgInS	mít
stát	stát	k5eAaPmF	stát
u	u	k7c2	u
obvodových	obvodový	k2eAgFnPc2d1	obvodová
stěn	stěna	k1gFnPc2	stěna
či	či	k8xC	či
zdrojů	zdroj	k1gInPc2	zdroj
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
sebelepší	sebelepší	k2eAgFnSc4d1	sebelepší
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
nástroj	nástroj	k1gInSc4	nástroj
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
rozladění	rozladění	k1gNnSc3	rozladění
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
povolat	povolat	k5eAaPmF	povolat
odborníka	odborník	k1gMnSc4	odborník
<g/>
,	,	kIx,	,
ladiče	ladič	k1gMnSc4	ladič
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
piáno	piáno	k?	piáno
doladí	doladit	k5eAaPmIp3nS	doladit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
roku	rok	k1gInSc2	rok
1732	[number]	k4	1732
Ludovic	Ludovice	k1gFnPc2	Ludovice
Giustini	Giustin	k2eAgMnPc1d1	Giustin
<g/>
;	;	kIx,	;
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
Dvanáct	dvanáct	k4xCc1	dvanáct
sonát	sonáta	k1gFnPc2	sonáta
pro	pro	k7c4	pro
hlasité	hlasitý	k2eAgFnPc4d1	hlasitá
i	i	k8xC	i
pomalé	pomalý	k2eAgNnSc1d1	pomalé
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uplatnění	uplatnění	k1gNnSc1	uplatnění
klavíru	klavír	k1gInSc2	klavír
a	a	k8xC	a
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
od	od	k7c2	od
baroka	baroko	k1gNnSc2	baroko
po	po	k7c4	po
jazz	jazz	k1gInSc4	jazz
zanechal	zanechat	k5eAaPmAgInS	zanechat
klavíru	klavír	k1gInSc3	klavír
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
repertoár	repertoár	k1gInSc1	repertoár
<g/>
,	,	kIx,	,
asi	asi	k9	asi
nejvíc	hodně	k6eAd3	hodně
však	však	k9	však
romantismus	romantismus	k1gInSc1	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgMnPc1d1	přední
hudební	hudební	k2eAgMnPc1d1	hudební
skladatelé	skladatel	k1gMnPc1	skladatel
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Debussy	Debussa	k1gFnPc1	Debussa
<g/>
,	,	kIx,	,
Schönberg	Schönberg	k1gInSc1	Schönberg
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
či	či	k8xC	či
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Gershwin	Gershwin	k2eAgInSc1d1	Gershwin
<g/>
)	)	kIx)	)
zanechali	zanechat	k5eAaPmAgMnP	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
klavírní	klavírní	k2eAgNnSc4d1	klavírní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Předností	přednost	k1gFnSc7	přednost
klavírního	klavírní	k2eAgInSc2d1	klavírní
repertoáru	repertoár	k1gInSc2	repertoár
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
piano	piano	k1gNnSc4	piano
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgFnP	upravit
i	i	k9	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
skladby	skladba	k1gFnPc1	skladba
určené	určený	k2eAgFnPc1d1	určená
původně	původně	k6eAd1	původně
orchestru	orchestr	k1gInSc2	orchestr
či	či	k8xC	či
pěvcům	pěvec	k1gMnPc3	pěvec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
teprve	teprve	k6eAd1	teprve
učí	učit	k5eAaImIp3nP	učit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
setkají	setkat	k5eAaPmIp3nP	setkat
s	s	k7c7	s
etudami	etuda	k1gFnPc7	etuda
Beethovenova	Beethovenův	k2eAgMnSc2d1	Beethovenův
žáka	žák	k1gMnSc2	žák
Karla	Karel	k1gMnSc2	Karel
Czerného	Czerný	k2eAgMnSc2d1	Czerný
<g/>
,	,	kIx,	,
Viléma	Vilém	k1gMnSc2	Vilém
Kurze	Kurz	k1gMnSc5	Kurz
či	či	k8xC	či
jeho	on	k3xPp3gMnSc2	on
žáka	žák	k1gMnSc2	žák
Antonína	Antonín	k1gMnSc2	Antonín
Sarauera	Sarauer	k1gMnSc2	Sarauer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
klavíristé	klavírista	k1gMnPc1	klavírista
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
role	role	k1gFnSc1	role
klavírního	klavírní	k2eAgInSc2d1	klavírní
virtuóza	virtuóza	k1gFnSc1	virtuóza
a	a	k8xC	a
skladatele	skladatel	k1gMnPc4	skladatel
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
prolínala	prolínat	k5eAaImAgFnS	prolínat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
např.	např.	kA	např.
Ludwig	Ludwig	k1gInSc4	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
byl	být	k5eAaImAgMnS	být
mnohem	mnohem	k6eAd1	mnohem
dřív	dříve	k6eAd2	dříve
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
pianista	pianista	k1gMnSc1	pianista
než	než	k8xS	než
jako	jako	k8xC	jako
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
předních	přední	k2eAgMnPc2d1	přední
interpretů	interpret	k1gMnPc2	interpret
minulosti	minulost	k1gFnSc2	minulost
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
snad	snad	k9	snad
největšího	veliký	k2eAgMnSc4d3	veliký
pianistu	pianista	k1gMnSc4	pianista
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
komponistu	komponista	k1gMnSc4	komponista
Ference	Ferenc	k1gMnSc4	Ferenc
Liszta	Liszt	k1gMnSc4	Liszt
či	či	k8xC	či
jeho	on	k3xPp3gMnSc4	on
mladšího	mladý	k2eAgMnSc4d2	mladší
kolegu	kolega	k1gMnSc4	kolega
Roberta	Robert	k1gMnSc4	Robert
Schumanna	Schumann	k1gMnSc4	Schumann
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgInSc7d1	slavný
klavírním	klavírní	k2eAgInSc7d1	klavírní
virtuózem	virtuóz	k1gInSc7	virtuóz
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
i	i	k9	i
Anton	Anton	k1gMnSc1	Anton
Rubinstein	Rubinstein	k1gMnSc1	Rubinstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
poznalo	poznat	k5eAaPmAgNnS	poznat
řadu	řada	k1gFnSc4	řada
špičkových	špičkový	k2eAgMnPc2d1	špičkový
klavíristů	klavírista	k1gMnPc2	klavírista
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Svjatoslava	Svjatoslav	k1gMnSc4	Svjatoslav
Richtera	Richter	k1gMnSc4	Richter
<g/>
,	,	kIx,	,
Alfreda	Alfred	k1gMnSc4	Alfred
Brendela	Brendel	k1gMnSc4	Brendel
<g/>
,	,	kIx,	,
Vladimira	Vladimir	k1gMnSc4	Vladimir
Ashkenazyho	Ashkenazy	k1gMnSc4	Ashkenazy
či	či	k8xC	či
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavné	slavný	k2eAgFnSc2d1	slavná
značky	značka	k1gFnSc2	značka
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
výrobou	výroba	k1gFnSc7	výroba
pian	piano	k1gNnPc2	piano
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
neliší	lišit	k5eNaImIp3nS	lišit
pouze	pouze	k6eAd1	pouze
značkou	značka	k1gFnSc7	značka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sytostí	sytost	k1gFnSc7	sytost
a	a	k8xC	a
sílou	síla	k1gFnSc7	síla
tónu	tón	k1gInSc2	tón
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
světoznámé	světoznámý	k2eAgMnPc4d1	světoznámý
výrobce	výrobce	k1gMnPc4	výrobce
klavírů	klavír	k1gInPc2	klavír
(	(	kIx(	(
<g/>
firmy	firma	k1gFnPc1	firma
nesou	nést	k5eAaImIp3nP	nést
jména	jméno	k1gNnSc2	jméno
svých	svůj	k3xOyFgMnPc2	svůj
zakladatelů	zakladatel	k1gMnPc2	zakladatel
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
americká	americký	k2eAgFnSc1d1	americká
firma	firma	k1gFnSc1	firma
Steinway	Steinwaa	k1gFnSc2	Steinwaa
&	&	k?	&
Sons	Sons	k1gInSc1	Sons
<g/>
,	,	kIx,	,
londýnský	londýnský	k2eAgInSc1d1	londýnský
Broadwood	Broadwood	k1gInSc1	Broadwood
<g/>
,	,	kIx,	,
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
Bösendorfer	Bösendorfer	k1gInSc1	Bösendorfer
<g/>
,	,	kIx,	,
berlínský	berlínský	k2eAgInSc1d1	berlínský
Bechstein	Bechstein	k1gInSc1	Bechstein
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
firma	firma	k1gFnSc1	firma
Fazioli	Faziole	k1gFnSc4	Faziole
či	či	k8xC	či
pařížský	pařížský	k2eAgInSc4d1	pařížský
Erard	Erard	k1gInSc4	Erard
<g/>
;	;	kIx,	;
největší	veliký	k2eAgMnSc1d3	veliký
výrobce	výrobce	k1gMnSc1	výrobce
pian	piano	k1gNnPc2	piano
(	(	kIx(	(
<g/>
i	i	k8xC	i
všech	všecek	k3xTgInPc2	všecek
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
dohromady	dohromady	k6eAd1	dohromady
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
firma	firma	k1gFnSc1	firma
Yamaha	yamaha	k1gFnSc1	yamaha
Corporation	Corporation	k1gInSc4	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
českým	český	k2eAgMnSc7d1	český
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
královéhradecká	královéhradecký	k2eAgFnSc1d1	Královéhradecká
Petrof	Petrof	k1gInSc1	Petrof
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
bývala	bývat	k5eAaImAgFnS	bývat
i	i	k8xC	i
největším	veliký	k2eAgMnSc7d3	veliký
evropským	evropský	k2eAgMnSc7d1	evropský
výrobcem	výrobce	k1gMnSc7	výrobce
pian	piano	k1gNnPc2	piano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klavír	klavír	k1gInSc4	klavír
v	v	k7c6	v
nehudebních	hudební	k2eNgNnPc6d1	nehudební
uměních	umění	k1gNnPc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Krom	krom	k7c2	krom
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
klavír	klavír	k1gInSc1	klavír
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
uměních	umění	k1gNnPc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
malován	malovat	k5eAaImNgInS	malovat
na	na	k7c4	na
portréty	portrét	k1gInPc4	portrét
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
měšťanů	měšťan	k1gMnPc2	měšťan
i	i	k8xC	i
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nesmírně	smírně	k6eNd1	smírně
nákladnou	nákladný	k2eAgFnSc4d1	nákladná
akvizici	akvizice	k1gFnSc4	akvizice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dokazovala	dokazovat	k5eAaImAgFnS	dokazovat
majetnost	majetnost	k1gFnSc4	majetnost
vlastníka	vlastník	k1gMnSc2	vlastník
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
jeho	jeho	k3xOp3gFnPc4	jeho
umělecké	umělecký	k2eAgFnPc4d1	umělecká
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
záliby	záliba	k1gFnPc4	záliba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
umělců	umělec	k1gMnPc2	umělec
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
několik	několik	k4yIc4	několik
portrétů	portrét	k1gInPc2	portrét
dívek	dívka	k1gFnPc2	dívka
u	u	k7c2	u
piána	piána	k?	piána
francouzský	francouzský	k2eAgMnSc1d1	francouzský
impresionista	impresionista	k1gMnSc1	impresionista
Auguste	August	k1gMnSc5	August
Renoir	Renoira	k1gFnPc2	Renoira
</s>
</p>
<p>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
motiv	motiv	k1gInSc4	motiv
piána	piána	k?	piána
např.	např.	kA	např.
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
rakouské	rakouský	k2eAgFnSc2d1	rakouská
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
prozaičky	prozaička	k1gFnSc2	prozaička
a	a	k8xC	a
dramatičky	dramatička	k1gFnSc2	dramatička
Elfriede	Elfried	k1gMnSc5	Elfried
Jelinekové	Jelinekové	k2eAgFnPc7d1	Jelinekové
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
románu	román	k1gInSc6	román
Pianistka	pianistka	k1gFnSc1	pianistka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
Michaelem	Michael	k1gMnSc7	Michael
Hanekem	Hanek	k1gInSc7	Hanek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmografii	filmografie	k1gFnSc6	filmografie
je	být	k5eAaImIp3nS	být
téma	téma	k1gNnSc1	téma
klavíru	klavír	k1gInSc2	klavír
či	či	k8xC	či
klavírního	klavírní	k2eAgInSc2d1	klavírní
přednesu	přednes	k1gInSc2	přednes
rovněž	rovněž	k9	rovněž
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc1	film
s	s	k7c7	s
klavírní	klavírní	k2eAgFnSc7d1	klavírní
tematikou	tematika	k1gFnSc7	tematika
natočili	natočit	k5eAaBmAgMnP	natočit
např.	např.	kA	např.
Scott	Scott	k2eAgInSc1d1	Scott
Hicks	Hicks	k1gInSc1	Hicks
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Polanski	Polansk	k1gFnSc2	Polansk
či	či	k8xC	či
Jane	Jan	k1gMnSc5	Jan
Campionová	Campionový	k2eAgFnSc5d1	Campionová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tori	Tori	k6eAd1	Tori
Amos	Amos	k1gMnSc1	Amos
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
představitelek	představitelka	k1gFnPc2	představitelka
hudební	hudební	k2eAgFnSc2d1	hudební
pop-kultury	popultura	k1gFnSc2	pop-kultura
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
nástroj	nástroj	k1gInSc1	nástroj
používá	používat	k5eAaImIp3nS	používat
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
J.E.	J.E.	k?	J.E.
<g/>
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hejzlar	Hejzlar	k1gInSc1	Hejzlar
<g/>
:	:	kIx,	:
Světem	svět	k1gInSc7	svět
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
Panton	Panton	k1gInSc1	Panton
</s>
</p>
<p>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
A.	A.	kA	A.
Reblitz	Reblitz	k1gMnSc1	Reblitz
<g/>
:	:	kIx,	:
Piano	piano	k6eAd1	piano
Servicing	Servicing	k1gInSc1	Servicing
<g/>
,	,	kIx,	,
Tuning	Tuning	k1gInSc1	Tuning
<g/>
,	,	kIx,	,
&	&	k?	&
Rebuilding	Rebuilding	k1gInSc1	Rebuilding
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Vestal	Vestal	k1gMnSc1	Vestal
Press	Press	k1gInSc1	Press
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pedálový	pedálový	k2eAgInSc4d1	pedálový
klavír	klavír	k1gInSc4	klavír
</s>
</p>
<p>
<s>
Preparovaný	preparovaný	k2eAgInSc4d1	preparovaný
klavír	klavír	k1gInSc4	klavír
</s>
</p>
<p>
<s>
Pianista	pianista	k1gMnSc1	pianista
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
klavíristů	klavírista	k1gMnPc2	klavírista
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
klavír	klavír	k1gInSc4	klavír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
klavír	klavír	k1gInSc1	klavír
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Noty	nota	k1gFnPc1	nota
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
co	co	k9	co
dělá	dělat	k5eAaImIp3nS	dělat
-	-	kIx~	-
Klavír	klavír	k1gInSc1	klavír
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
klavírnický	klavírnický	k2eAgInSc1d1	klavírnický
svaz	svaz	k1gInSc1	svaz
</s>
</p>
