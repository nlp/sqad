<p>
<s>
Juhan	Juhan	k1gMnSc1	Juhan
Liiv	Liiv	k1gMnSc1	Liiv
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
Alatskivi	Alatskiev	k1gFnSc6	Alatskiev
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Kavastu-Koosa	Kavastu-Koosa	k1gFnSc1	Kavastu-Koosa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
estonský	estonský	k2eAgMnSc1d1	estonský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
moderní	moderní	k2eAgFnSc2d1	moderní
estonské	estonský	k2eAgFnSc2d1	Estonská
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
povídkami	povídka	k1gFnPc7	povídka
vnesl	vnést	k5eAaPmAgInS	vnést
do	do	k7c2	do
estonské	estonský	k2eAgFnSc2d1	Estonská
literatury	literatura	k1gFnSc2	literatura
kritický	kritický	k2eAgInSc1d1	kritický
realismus	realismus	k1gInSc1	realismus
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Kiimme	Kiimme	k1gFnSc2	Kiimme
fugu	fuga	k1gFnSc4	fuga
–	–	k?	–
Deset	deset	k4xCc4	deset
příběhů	příběh	k1gInPc2	příběh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
chudé	chudý	k2eAgFnSc2d1	chudá
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
obci	obec	k1gFnSc6	obec
blízko	blízko	k7c2	blízko
ruských	ruský	k2eAgFnPc2d1	ruská
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
</s>
</p>
<p>
<s>
od	od	k7c2	od
Tartu	Tart	k1gInSc2	Tart
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
přesto	přesto	k8xC	přesto
sehnali	sehnat	k5eAaPmAgMnP	sehnat
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Dorpatu	Dorpat	k1gInSc6	Dorpat
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Tartu	Tart	k1gInSc3	Tart
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Juhan	Juhan	k1gMnSc1	Juhan
však	však	k9	však
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
rodné	rodný	k2eAgFnSc2d1	rodná
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
psal	psát	k5eAaImAgInS	psát
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
literárního	literární	k2eAgNnSc2d1	literární
modernistického	modernistický	k2eAgNnSc2d1	modernistické
hnutí	hnutí	k1gNnSc2	hnutí
Mladé	mladý	k2eAgNnSc4d1	mladé
Estonsko	Estonsko	k1gNnSc4	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Luuletudsed	Luuletudsed	k1gInSc1	Luuletudsed
(	(	kIx(	(
<g/>
Básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
psychický	psychický	k2eAgInSc4d1	psychický
rozpad	rozpad	k1gInSc4	rozpad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
tragický	tragický	k2eAgInSc1d1	tragický
osud	osud	k1gInSc1	osud
estonského	estonský	k2eAgInSc2d1	estonský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
temnou	temný	k2eAgFnSc4d1	temná
atmosféru	atmosféra	k1gFnSc4	atmosféra
má	mít	k5eAaImIp3nS	mít
povídka	povídka	k1gFnSc1	povídka
Vari	vari	k1gMnSc1	vari
(	(	kIx(	(
<g/>
Stín	stín	k1gInSc1	stín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
texty	text	k1gInPc1	text
souvisely	souviset	k5eAaImAgInP	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
lékaři	lékař	k1gMnPc1	lékař
diagnostikovali	diagnostikovat	k5eAaBmAgMnP	diagnostikovat
schizofrenii	schizofrenie	k1gFnSc4	schizofrenie
<g/>
:	:	kIx,	:
trpěl	trpět	k5eAaImAgInS	trpět
bludem	blud	k1gInSc7	blud
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
synem	syn	k1gMnSc7	syn
ruského	ruský	k2eAgMnSc2d1	ruský
cara	car	k1gMnSc2	car
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
v	v	k7c6	v
Tartu	Tart	k1gInSc6	Tart
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
psychická	psychický	k2eAgFnSc1d1	psychická
choroba	choroba	k1gFnSc1	choroba
stála	stát	k5eAaImAgFnS	stát
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k8xC	i
za	za	k7c7	za
okolnostmi	okolnost	k1gFnPc7	okolnost
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
bez	bez	k7c2	bez
lístku	lístek	k1gInSc2	lístek
do	do	k7c2	do
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
černý	černý	k2eAgMnSc1d1	černý
pasažér	pasažér	k1gMnSc1	pasažér
vysazen	vysazen	k2eAgMnSc1d1	vysazen
v	v	k7c6	v
opuštěné	opuštěný	k2eAgFnSc6d1	opuštěná
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
šel	jít	k5eAaImAgMnS	jít
domů	domů	k6eAd1	domů
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
v	v	k7c6	v
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Domů	domů	k6eAd1	domů
dorazil	dorazit	k5eAaPmAgMnS	dorazit
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
záhy	záhy	k6eAd1	záhy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
smutné	smutný	k2eAgFnPc1d1	smutná
básně	báseň	k1gFnPc1	báseň
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
neobvyklé	obvyklý	k2eNgFnPc1d1	neobvyklá
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
získaly	získat	k5eAaPmAgFnP	získat
teprve	teprve	k6eAd1	teprve
povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
Liivovi	Liivův	k2eAgMnPc1d1	Liivův
obdivovatelé	obdivovatel	k1gMnPc1	obdivovatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
králem	král	k1gMnSc7	král
estonských	estonský	k2eAgMnPc2d1	estonský
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
Friedebertem	Friedebert	k1gMnSc7	Friedebert
Tuglasem	Tuglas	k1gMnSc7	Tuglas
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
v	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
chvílích	chvíle	k1gFnPc6	chvíle
zlepšit	zlepšit	k5eAaPmF	zlepšit
jeho	jeho	k3xOp3gFnPc4	jeho
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
se	s	k7c7	s
495	[number]	k4	495
básněmi	báseň	k1gFnPc7	báseň
Juhana	Juhan	k1gMnSc2	Juhan
Liiva	Liiv	k1gMnSc2	Liiv
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dočkala	dočkat	k5eAaPmAgFnS	dočkat
dalších	další	k2eAgNnPc2d1	další
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
Cena	cena	k1gFnSc1	cena
Juhana	Juhan	k1gMnSc2	Juhan
Liiva	Liiv	k1gMnSc2	Liiv
za	za	k7c4	za
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
každoročně	každoročně	k6eAd1	každoročně
udílí	udílet	k5eAaImIp3nS	udílet
farnost	farnost	k1gFnSc1	farnost
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodné	rodný	k2eAgFnSc6d1	rodná
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Juhan	Juhan	k1gMnSc1	Juhan
Liiv	Liiv	k1gMnSc1	Liiv
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Estonian	Estonian	k1gInSc1	Estonian
Literary	Literara	k1gFnSc2	Literara
Magazine	Magazin	k1gMnSc5	Magazin
</s>
</p>
<p>
<s>
|	|	kIx~	|
<g/>
date	date	k1gNnPc6	date
<g/>
=	=	kIx~	=
<g/>
March	March	k1gInSc1	March
9	[number]	k4	9
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Článek	článek	k1gInSc1	článek
v	v	k7c6	v
Baltic	Baltice	k1gFnPc2	Baltice
studies	studies	k1gInSc1	studies
</s>
</p>
