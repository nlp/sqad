<s>
Vařečka	vařečka	k1gFnSc1	vařečka
nebo	nebo	k8xC	nebo
také	také	k9	také
měchačka	měchačka	k1gFnSc1	měchačka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kuchyňský	kuchyňský	k2eAgInSc4d1	kuchyňský
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
míchání	míchání	k1gNnSc3	míchání
polévky	polévka	k1gFnSc2	polévka
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
jídel	jídlo	k1gNnPc2	jídlo
<g/>
,	,	kIx,	,
k	k	k7c3	k
promíchávání	promíchávání	k1gNnSc3	promíchávání
různých	různý	k2eAgFnPc2d1	různá
přísad	přísada	k1gFnPc2	přísada
nebo	nebo	k8xC	nebo
hnětení	hnětení	k1gNnSc2	hnětení
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
