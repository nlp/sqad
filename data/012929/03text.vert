<p>
<s>
Vařečka	vařečka	k1gFnSc1	vařečka
nebo	nebo	k8xC	nebo
také	také	k9	také
měchačka	měchačka	k1gFnSc1	měchačka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kuchyňský	kuchyňský	k2eAgInSc4d1	kuchyňský
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
míchání	míchání	k1gNnSc3	míchání
polévky	polévka	k1gFnSc2	polévka
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
jídel	jídlo	k1gNnPc2	jídlo
<g/>
,	,	kIx,	,
k	k	k7c3	k
promíchávání	promíchávání	k1gNnSc3	promíchávání
různých	různý	k2eAgFnPc2d1	různá
přísad	přísada	k1gFnPc2	přísada
nebo	nebo	k8xC	nebo
hnětení	hnětení	k1gNnSc2	hnětení
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
nebo	nebo	k8xC	nebo
smažení	smažení	k1gNnSc6	smažení
se	se	k3xPyFc4	se
pokrmy	pokrm	k1gInPc1	pokrm
míchají	míchat	k5eAaImIp3nP	míchat
vařečkou	vařečka	k1gFnSc7	vařečka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
jejich	jejich	k3xOp3gNnSc1	jejich
připálení	připálení	k1gNnSc1	připálení
nebo	nebo	k8xC	nebo
přichycení	přichycení	k1gNnSc1	přichycení
k	k	k7c3	k
okrajům	okraj	k1gInPc3	okraj
nebo	nebo	k8xC	nebo
dnu	dno	k1gNnSc3	dno
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vařečka	vařečka	k1gFnSc1	vařečka
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
ploché	plochý	k2eAgFnSc2d1	plochá
lžíce	lžíce	k1gFnSc2	lžíce
s	s	k7c7	s
různě	různě	k6eAd1	různě
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
rukojetí	rukojeť	k1gFnSc7	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
kulatý	kulatý	k2eAgInSc1d1	kulatý
nebo	nebo	k8xC	nebo
oválný	oválný	k2eAgInSc1d1	oválný
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
často	často	k6eAd1	často
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
jakýsi	jakýsi	k3yIgInSc1	jakýsi
růžek	růžek	k1gInSc1	růžek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
promíchávání	promíchávání	k1gNnSc3	promíchávání
pokrmů	pokrm	k1gInPc2	pokrm
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
hrnce	hrnec	k1gInSc2	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
vařečky	vařečka	k1gFnPc1	vařečka
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
výhradně	výhradně	k6eAd1	výhradně
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
i	i	k8xC	i
vařečky	vařečka	k1gFnSc2	vařečka
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
tvarů	tvar	k1gInPc2	tvar
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
velké	velký	k2eAgFnPc1d1	velká
vařečky	vařečka	k1gFnPc1	vařečka
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
se	se	k3xPyFc4	se
podobající	podobající	k2eAgInPc1d1	podobající
malému	malý	k2eAgNnSc3d1	malé
pádlu	pádlo	k1gNnSc3	pádlo
<g/>
,	,	kIx,	,
používaly	používat	k5eAaImAgFnP	používat
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
velkém	velký	k2eAgNnSc6d1	velké
prádle	prádlo	k1gNnSc6	prádlo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
prádlo	prádlo	k1gNnSc1	prádlo
společně	společně	k6eAd1	společně
vyvařovalo	vyvařovat	k5eAaImAgNnS	vyvařovat
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
kotlích	kotel	k1gInPc6	kotel
<g/>
.	.	kIx.	.
</s>
<s>
Prádlo	prádlo	k1gNnSc1	prádlo
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
míchalo	míchat	k5eAaImAgNnS	míchat
velkou	velký	k2eAgFnSc4d1	velká
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
vařekou	vařéct	k5eAaPmIp3nPwK	vařéct
(	(	kIx(	(
<g/>
krajový	krajový	k2eAgInSc4d1	krajový
název	název	k1gInSc4	název
kopist	kopist	k1gFnSc1	kopist
<g/>
,	,	kIx,	,
kopisť	kopistit	k5eAaPmRp2nS	kopistit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
i	i	k9	i
pro	pro	k7c4	pro
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
prádlem	prádlo	k1gNnSc7	prádlo
ve	v	k7c6	v
vařící	vařící	k2eAgFnSc6d1	vařící
vodě	voda	k1gFnSc6	voda
resp.	resp.	kA	resp.
vyndávání	vyndávání	k?	vyndávání
prádla	prádlo	k1gNnSc2	prádlo
z	z	k7c2	z
kotle	kotel	k1gInSc2	kotel
a	a	k8xC	a
zabraňovala	zabraňovat	k5eAaImAgFnS	zabraňovat
tak	tak	k9	tak
opaření	opaření	k1gNnSc4	opaření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
také	také	k9	také
vařečky	vařečka	k1gFnPc1	vařečka
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
polotovar	polotovar	k1gInSc4	polotovar
k	k	k7c3	k
domácí	domácí	k2eAgFnSc3d1	domácí
výrobě	výroba	k1gFnSc3	výroba
různých	různý	k2eAgInPc2d1	různý
doplňků	doplněk	k1gInPc2	doplněk
domácnosti	domácnost	k1gFnSc2	domácnost
a	a	k8xC	a
ozdob	ozdoba	k1gFnPc2	ozdoba
Sama	sám	k3xTgFnSc1	sám
vařečka	vařečka	k1gFnSc1	vařečka
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
zároveň	zároveň	k6eAd1	zároveň
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
podomácku	podomácku	k6eAd1	podomácku
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
