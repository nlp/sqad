<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
snowboardingu	snowboarding	k1gInSc6	snowboarding
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
sedmým	sedmý	k4xOgInSc7	sedmý
světovým	světový	k2eAgInSc7d1	světový
šampionátem	šampionát	k1gInSc7	šampionát
ve	v	k7c6	v
snowboardingu	snowboarding	k1gInSc6	snowboarding
pořádaným	pořádaný	k2eAgInSc7d1	pořádaný
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
lyžařskou	lyžařský	k2eAgFnSc7d1	lyžařská
federací	federace	k1gFnSc7	federace
<g/>
.	.	kIx.	.
</s>
<s>
Konalo	konat	k5eAaImAgNnS	konat
se	se	k3xPyFc4	se
ve	v	k7c6	v
dnech	den	k1gInPc6	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
v	v	k7c6	v
Arose	Arosa	k1gFnSc6	Arosa
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
i	i	k9	i
ženy	žena	k1gFnSc2	žena
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
v	v	k7c6	v
paralelním	paralelní	k2eAgInSc6d1	paralelní
slalomu	slalom	k1gInSc6	slalom
a	a	k8xC	a
obřím	obří	k2eAgInSc6d1	obří
slalomu	slalom	k1gInSc6	slalom
<g/>
,	,	kIx,	,
na	na	k7c6	na
U-rampě	Uampa	k1gFnSc6	U-rampa
a	a	k8xC	a
ve	v	k7c6	v
snowboardcrossu	snowboardcross	k1gInSc6	snowboardcross
<g/>
.	.	kIx.	.
</s>
<s>
Disciplínu	disciplína	k1gFnSc4	disciplína
Big	Big	k1gMnSc2	Big
Air	Air	k1gMnSc2	Air
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
jen	jen	k9	jen
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
</s>
</p>
<p>
<s>
==	==	k?	==
Paralelní	paralelní	k2eAgInSc1d1	paralelní
slalom	slalom	k1gInSc1	slalom
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Paralelní	paralelní	k2eAgInSc4d1	paralelní
obří	obří	k2eAgInSc4d1	obří
slalom	slalom	k1gInSc4	slalom
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Snowboardcross	Snowboardcross	k1gInSc4	Snowboardcross
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
U	u	k7c2	u
rampa	rampa	k1gFnSc1	rampa
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Big	Big	k1gMnSc5	Big
Air	Air	k1gMnSc5	Air
==	==	k?	==
</s>
</p>
<p>
<s>
Umístění	umístění	k1gNnSc1	umístění
Čechů	Čech	k1gMnPc2	Čech
<g/>
:	:	kIx,	:
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Černík	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Zvoníček	Zvoníček	k1gMnSc1	Zvoníček
<g/>
,	,	kIx,	,
49	[number]	k4	49
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
Novák	Novák	k1gMnSc1	Novák
</s>
</p>
<p>
<s>
Datum	datum	k1gNnSc1	datum
soutěže	soutěž	k1gFnSc2	soutěž
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
==	==	k?	==
Medailové	medailový	k2eAgNnSc4d1	medailové
pořadí	pořadí	k1gNnSc4	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
pořadatele	pořadatel	k1gMnSc2	pořadatel
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
FIS	fis	k1gNnSc2	fis
</s>
</p>
