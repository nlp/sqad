<s>
Většina	většina	k1gFnSc1	většina
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
v	v	k7c6	v
komerčních	komerční	k2eAgNnPc6d1	komerční
nalezištích	naleziště	k1gNnPc6	naleziště
je	být	k5eAaImIp3nS	být
však	však	k9	však
organického	organický	k2eAgInSc2d1	organický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
buď	buď	k8xC	buď
biogenezi	biogeneze	k1gFnSc4	biogeneze
nebo	nebo	k8xC	nebo
termogenezi	termogeneze	k1gFnSc4	termogeneze
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
Anorganický	anorganický	k2eAgInSc4d1	anorganický
původ	původ	k1gInSc4	původ
ropy	ropa	k1gFnSc2	ropa
Anorganický	anorganický	k2eAgInSc4d1	anorganický
původ	původ	k1gInSc4	původ
ropy	ropa	k1gFnSc2	ropa
předpovídal	předpovídat	k5eAaImAgMnS	předpovídat
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
<g/>
.	.	kIx.	.
</s>
