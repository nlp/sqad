<s>
Palubní	palubní	k2eAgInSc1d1
protisrážkový	protisrážkový	k2eAgInSc1d1
systém	systém	k1gInSc1
TCAS	TCAS	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Traffic	Traffice	k1gFnPc2
Collision	Collision	k1gInSc1
Avoidance	Avoidanec	k1gInSc2
System	Systo	k1gNnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezpečnostní	bezpečnostní	k2eAgInSc4d1
systém	systém	k1gInSc4
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
monitoruje	monitorovat	k5eAaImIp3nS
okolí	okolí	k1gNnSc4
a	a	k8xC
dává	dávat	k5eAaImIp3nS
pilotům	pilot	k1gInPc3
informace	informace	k1gFnSc2
a	a	k8xC
varování	varování	k1gNnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
okolního	okolní	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>