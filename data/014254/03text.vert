<s>
Palubní	palubní	k2eAgInSc1d1
protisrážkový	protisrážkový	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Displej	displej	k1gInSc1
systému	systém	k1gInSc2
TCAS	TCAS	kA
</s>
<s>
Palubní	palubní	k2eAgInSc1d1
protisrážkový	protisrážkový	k2eAgInSc1d1
systém	systém	k1gInSc1
TCAS	TCAS	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Traffic	Traffice	k1gFnPc2
Collision	Collision	k1gInSc1
Avoidance	Avoidanec	k1gInSc2
System	Systo	k1gNnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezpečnostní	bezpečnostní	k2eAgInSc4d1
systém	systém	k1gInSc4
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
monitoruje	monitorovat	k5eAaImIp3nS
okolí	okolí	k1gNnSc4
a	a	k8xC
dává	dávat	k5eAaImIp3nS
pilotům	pilot	k1gInPc3
informace	informace	k1gFnSc2
a	a	k8xC
varování	varování	k1gNnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
okolního	okolní	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
TCAS	TCAS	kA
pracuje	pracovat	k5eAaImIp3nS
nezávisle	závisle	k6eNd1
na	na	k7c6
řízení	řízení	k1gNnSc6
letového	letový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
a	a	k8xC
pozemních	pozemní	k2eAgInPc6d1
navigačních	navigační	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
standardů	standard	k1gInPc2
ICAO	ICAO	kA
by	by	kYmCp3nP
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
tímto	tento	k3xDgNnSc7
antikolizním	antikolizní	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
vybavena	vybavit	k5eAaPmNgFnS
všechna	všechen	k3xTgNnPc4
letadla	letadlo	k1gNnPc4
s	s	k7c7
maximální	maximální	k2eAgFnSc7d1
vzletovou	vzletový	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
větší	veliký	k2eAgFnSc7d2
než	než	k8xS
5700	#num#	k4
kg	kg	kA
a	a	k8xC
všechna	všechen	k3xTgNnPc4
letadla	letadlo	k1gNnPc4
certifikovaná	certifikovaný	k2eAgNnPc4d1
k	k	k7c3
přepravě	přeprava	k1gFnSc3
více	hodně	k6eAd2
než	než	k8xS
19	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
TCAS	TCAS	kA
se	se	k3xPyFc4
vyslovuje	vyslovovat	k5eAaImIp3nS
jako	jako	k9
"	"	kIx"
<g/>
tý-kas	tý-kas	k1gInSc1
<g/>
"	"	kIx"
a	a	k8xC
někdy	někdy	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vyložena	vyložit	k5eAaPmNgFnS
také	také	k9
jako	jako	k9
"	"	kIx"
<g/>
Traffic	Traffic	k1gMnSc1
Alert	Alerta	k1gFnPc2
and	and	k?
Collision	Collision	k1gInSc1
Avoidance	Avoidanec	k1gInSc2
System	Systo	k1gNnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
leteckých	letecký	k2eAgFnPc2d1
zkratek	zkratka	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
