<s>
Kačer	kačer	k1gMnSc1	kačer
Donald	Donald	k1gMnSc1	Donald
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
</s>
<s>
Donald	Donald	k1gMnSc1	Donald
Duck	Duck	k1gMnSc1	Duck
<g/>
)	)	kIx)	)
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
hrdina	hrdina	k1gMnSc1	hrdina
animovaných	animovaný	k2eAgInPc2d1	animovaný
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
komiksů	komiks	k1gInPc2	komiks
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
režisérem	režisér	k1gMnSc7	režisér
Waltem	Walt	k1gMnSc7	Walt
Disneyem	Disney	k1gMnSc7	Disney
<g/>
.	.	kIx.	.
</s>
<s>
Kačer	kačer	k1gMnSc1	kačer
Donald	Donald	k1gMnSc1	Donald
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
The	The	k1gFnSc2	The
Wise	Wis	k1gFnSc2	Wis
Little	Little	k1gFnSc2	Little
Hen	hena	k1gFnPc2	hena
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Silly	Silla	k1gFnSc2	Silla
Symphonies	Symphonies	k1gInSc1	Symphonies
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
animovaných	animovaný	k2eAgInPc6d1	animovaný
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Myšáka	myšák	k1gMnSc4	myšák
Mickeye	Mickey	k1gMnSc4	Mickey
<g/>
,	,	kIx,	,
Goofyho	Goofy	k1gMnSc4	Goofy
a	a	k8xC	a
psa	pes	k1gMnSc4	pes
Pluta	Pluto	k1gMnSc4	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Kačeří	kačeří	k2eAgInPc4d1	kačeří
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hlasem	hlas	k1gInSc7	hlas
Kačera	kačer	k1gMnSc2	kačer
Donalda	Donald	k1gMnSc2	Donald
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgMnSc1d1	spojen
herec	herec	k1gMnSc1	herec
Clarence	Clarence	k1gFnSc2	Clarence
Nash	Nash	k1gMnSc1	Nash
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
daboval	dabovat	k5eAaBmAgMnS	dabovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
hvězda	hvězda	k1gFnSc1	hvězda
Kačera	kačer	k1gMnSc2	kačer
Donalda	Donald	k1gMnSc2	Donald
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
chronologie	chronologie	k1gFnSc2	chronologie
The	The	k1gMnSc1	The
Chronological	Chronological	k1gMnSc1	Chronological
Donald	Donald	k1gMnSc1	Donald
–	–	k?	–
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
Leonarda	Leonardo	k1gMnSc2	Leonardo
Maltina	Maltin	k1gMnSc2	Maltin
postavu	postav	k1gInSc2	postav
Donalda	Donald	k1gMnSc4	Donald
napadlo	napadnout	k5eAaPmAgNnS	napadnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
Walta	Walt	k1gMnSc2	Walt
Disneye	Disney	k1gMnSc2	Disney
<g/>
,	,	kIx,	,
když	když	k8xS	když
slyšel	slyšet	k5eAaImAgInS	slyšet
"	"	kIx"	"
<g/>
kachní	kachní	k2eAgInSc1d1	kachní
<g/>
"	"	kIx"	"
hlas	hlas	k1gInSc1	hlas
herce	herec	k1gMnSc2	herec
Clarence	Clarenec	k1gMnSc2	Clarenec
Nashe	Nash	k1gMnSc2	Nash
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
byl	být	k5eAaImAgMnS	být
myšák	myšák	k1gMnSc1	myšák
Mickey	Mickea	k1gFnSc2	Mickea
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
dětským	dětský	k2eAgMnSc7d1	dětský
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
,	,	kIx,	,
a	a	k8xC	a
Disney	Disnea	k1gMnSc2	Disnea
chtěl	chtít	k5eAaImAgMnS	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
postavu	postava	k1gFnSc4	postava
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
negativními	negativní	k2eAgFnPc7d1	negativní
charakterovými	charakterový	k2eAgFnPc7d1	charakterová
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ale	ale	k9	ale
nechtěl	chtít	k5eNaImAgMnS	chtít
dát	dát	k5eAaPmF	dát
Mickeymu	Mickeym	k1gInSc3	Mickeym
<g/>
.	.	kIx.	.
</s>
<s>
Donald	Donald	k1gMnSc1	Donald
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
kresleném	kreslený	k2eAgInSc6d1	kreslený
filmu	film	k1gInSc6	film
The	The	k1gFnSc2	The
Wise	Wise	k1gFnSc4	Wise
Little	Little	k1gFnSc1	Little
Hen	hena	k1gFnPc2	hena
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
o	o	k7c6	o
W.	W.	kA	W.
Disneyovi	Disneya	k1gMnSc6	Disneya
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Filmovou	filmový	k2eAgFnSc4d1	filmová
postavu	postava	k1gFnSc4	postava
Donalda	Donald	k1gMnSc2	Donald
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
animátor	animátor	k1gMnSc1	animátor
Dick	Dick	k1gMnSc1	Dick
Lundy	Lunda	k1gMnSc2	Lunda
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kačer	kačer	k1gMnSc1	kačer
Donald	Donald	k1gMnSc1	Donald
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Donald	Donald	k1gMnSc1	Donald
Duck	Duck	k1gMnSc1	Duck
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
family	famil	k1gMnPc7	famil
tree	tree	k1gFnSc4	tree
Toonopedia	Toonopedium	k1gNnSc2	Toonopedium
<g/>
:	:	kIx,	:
Donald	Donald	k1gMnSc1	Donald
Duck	Duck	k1gMnSc1	Duck
kačer	kačer	k1gMnSc1	kačer
Donald	Donald	k1gMnSc1	Donald
<g/>
,	,	kIx,	,
krátká	krátký	k2eAgFnSc1d1	krátká
antologie	antologie	k1gFnSc1	antologie
vše	všechen	k3xTgNnSc4	všechen
o	o	k7c6	o
komiksech	komiks	k1gInPc6	komiks
s	s	k7c7	s
kačerem	kačer	k1gMnSc7	kačer
Donaldem	Donald	k1gMnSc7	Donald
Galerie	galerie	k1gFnSc2	galerie
Kačer	kačer	k1gMnSc1	kačer
Donald	Donald	k1gMnSc1	Donald
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Donald	Donald	k1gMnSc1	Donald
Duck	Duck	k1gMnSc1	Duck
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
