<s>
Replikace	replikace	k1gFnSc1	replikace
DNA	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
tvorby	tvorba	k1gFnSc2	tvorba
kopií	kopie	k1gFnPc2	kopie
molekuly	molekula	k1gFnSc2	molekula
deoxyribonukleové	deoxyribonukleový	k2eAgFnSc2d1	deoxyribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
přenáší	přenášet	k5eAaImIp3nS	přenášet
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
templát	templát	k1gInSc1	templát
<g/>
,	,	kIx,	,
matrice	matrice	k1gFnSc1	matrice
<g/>
)	)	kIx)	)
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
molekuly	molekula	k1gFnSc2	molekula
stejného	stejný	k2eAgInSc2d1	stejný
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
replika	replika	k1gFnSc1	replika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
semikonzervativní	semikonzervativní	k2eAgMnSc1d1	semikonzervativní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
každá	každý	k3xTgFnSc1	každý
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
molekula	molekula	k1gFnSc1	molekula
DNA	DNA	kA	DNA
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc1	jeden
řetězec	řetězec	k1gInSc1	řetězec
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
molekuly	molekula	k1gFnSc2	molekula
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
syntetizovaný	syntetizovaný	k2eAgInSc1d1	syntetizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
dochází	docházet	k5eAaImIp3nS	docházet
pomocí	pomocí	k7c2	pomocí
složité	složitý	k2eAgFnSc2d1	složitá
enzymatické	enzymatický	k2eAgFnSc2d1	enzymatická
mašinérie	mašinérie	k1gFnSc2	mašinérie
k	k	k7c3	k
řazení	řazení	k1gNnSc3	řazení
deoxyribonukleotidů	deoxyribonukleotid	k1gInPc2	deoxyribonukleotid
(	(	kIx(	(
<g/>
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
základních	základní	k2eAgFnPc2d1	základní
stavebních	stavební	k2eAgFnPc2d1	stavební
částic	částice	k1gFnPc2	částice
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
jeden	jeden	k4xCgInSc1	jeden
za	za	k7c2	za
druhým	druhý	k4xOgInSc7	druhý
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
vzorové	vzorový	k2eAgFnSc2d1	vzorová
původní	původní	k2eAgFnSc2d1	původní
molekuly	molekula	k1gFnSc2	molekula
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
tohoto	tento	k3xDgNnSc2	tento
řazení	řazení	k1gNnSc2	řazení
nukleotidů	nukleotid	k1gInPc2	nukleotid
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
kompletní	kompletní	k2eAgFnSc1d1	kompletní
DNA	dna	k1gFnSc1	dna
daného	daný	k2eAgInSc2d1	daný
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
identická	identický	k2eAgFnSc1d1	identická
kopie	kopie	k1gFnSc1	kopie
původní	původní	k2eAgFnSc2d1	původní
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tajů	taj	k1gInPc2	taj
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
začali	začít	k5eAaPmAgMnP	začít
vědci	vědec	k1gMnPc1	vědec
blíže	blízce	k6eAd2	blízce
pronikat	pronikat	k5eAaImF	pronikat
až	až	k9	až
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
byly	být	k5eAaImAgInP	být
odhaleny	odhalit	k5eAaPmNgInP	odhalit
do	do	k7c2	do
poměrně	poměrně	k6eAd1	poměrně
velkých	velký	k2eAgFnPc2d1	velká
podrobností	podrobnost	k1gFnPc2	podrobnost
molekulární	molekulární	k2eAgInPc1d1	molekulární
pochody	pochod	k1gInPc1	pochod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
probíhá	probíhat	k5eAaImIp3nS	probíhat
replikace	replikace	k1gFnSc1	replikace
poněkud	poněkud	k6eAd1	poněkud
odlišně	odlišně	k6eAd1	odlišně
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organizmů	organizmus	k1gInPc2	organizmus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
či	či	k8xC	či
houby	houba	k1gFnPc1	houba
<g/>
.	.	kIx.	.
</s>
<s>
Praktický	praktický	k2eAgInSc1d1	praktický
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
výzkumné	výzkumný	k2eAgFnPc4d1	výzkumná
metody	metoda	k1gFnPc4	metoda
odvozené	odvozený	k2eAgFnPc4d1	odvozená
od	od	k7c2	od
procesu	proces	k1gInSc2	proces
replikace	replikace	k1gFnSc2	replikace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
PCR	PCR	kA	PCR
a	a	k8xC	a
sekvenování	sekvenování	k1gNnSc2	sekvenování
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Kornberg	Kornberg	k1gMnSc1	Kornberg
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
bakterie	bakterie	k1gFnSc2	bakterie
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
první	první	k4xOgFnSc1	první
DNA	dna	k1gFnSc1	dna
polymerázu	polymeráza	k1gFnSc4	polymeráza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
DNA	dno	k1gNnSc2	dno
polymeráza	polymeráza	k1gFnSc1	polymeráza
I.	I.	kA	I.
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
replikaci	replikace	k1gFnSc6	replikace
spíše	spíše	k9	spíše
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
III	III	kA	III
<g/>
,	,	kIx,	,
i	i	k9	i
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
důležitým	důležitý	k2eAgInSc7d1	důležitý
milníkem	milník	k1gInSc7	milník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
replikace	replikace	k1gFnSc1	replikace
probíhá	probíhat	k5eAaImIp3nS	probíhat
tzv.	tzv.	kA	tzv.
semikonzervativně	semikonzervativně	k6eAd1	semikonzervativně
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
Meselsonův-Stahlův	Meselsonův-Stahlův	k2eAgInSc1d1	Meselsonův-Stahlův
experiment	experiment	k1gInSc1	experiment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
učiněn	učiněn	k2eAgInSc1d1	učiněn
další	další	k2eAgInSc1d1	další
krok	krok	k1gInSc1	krok
kupředu	kupředu	k6eAd1	kupředu
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc1	každý
z	z	k7c2	z
vláken	vlákna	k1gFnPc2	vlákna
původní	původní	k2eAgFnSc2d1	původní
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
replikováno	replikovat	k5eAaImNgNnS	replikovat
mírně	mírně	k6eAd1	mírně
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
dochází	docházet	k5eAaImIp3nS	docházet
vlivem	vlivem	k7c2	vlivem
diskontinuální	diskontinuální	k2eAgFnSc2d1	diskontinuální
syntézy	syntéza	k1gFnSc2	syntéza
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Okazakiho	Okazaki	k1gMnSc2	Okazaki
fragmentů	fragment	k1gInPc2	fragment
<g/>
.	.	kIx.	.
</s>
<s>
Načasování	načasování	k1gNnSc1	načasování
DNA	dno	k1gNnSc2	dno
replikace	replikace	k1gFnSc2	replikace
je	být	k5eAaImIp3nS	být
diametrálně	diametrálně	k6eAd1	diametrálně
odlišné	odlišný	k2eAgFnSc3d1	odlišná
při	pře	k1gFnSc3	pře
srovnávání	srovnávání	k1gNnSc2	srovnávání
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
a	a	k8xC	a
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
archeí	arche	k1gFnPc2	arche
(	(	kIx(	(
<g/>
souhrnně	souhrnně	k6eAd1	souhrnně
prokaryota	prokaryot	k1gMnSc2	prokaryot
<g/>
)	)	kIx)	)
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
replikaci	replikace	k1gFnSc3	replikace
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
fázi	fáze	k1gFnSc6	fáze
buněčného	buněčný	k2eAgInSc2d1	buněčný
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
vůbec	vůbec	k9	vůbec
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
buněčným	buněčný	k2eAgNnSc7d1	buněčné
dělením	dělení	k1gNnSc7	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
replikace	replikace	k1gFnSc1	replikace
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
S	s	k7c7	s
fázi	fáze	k1gFnSc6	fáze
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
syntéza	syntéza	k1gFnSc1	syntéza
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
buněčného	buněčný	k2eAgInSc2d1	buněčný
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
S	s	k7c7	s
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
syntéza	syntéza	k1gFnSc1	syntéza
DNA	dno	k1gNnSc2	dno
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
drobné	drobný	k2eAgInPc4d1	drobný
opravné	opravný	k2eAgInPc4d1	opravný
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzácných	vzácný	k2eAgFnPc6d1	vzácná
výjimkách	výjimka	k1gFnPc6	výjimka
(	(	kIx(	(
<g/>
především	především	k9	především
tzv.	tzv.	kA	tzv.
endoreduplikace	endoreduplikace	k1gFnSc1	endoreduplikace
<g/>
)	)	kIx)	)
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
replikaci	replikace	k1gFnSc3	replikace
DNA	DNA	kA	DNA
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
buněčný	buněčný	k2eAgInSc4d1	buněčný
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
slinné	slinný	k2eAgFnPc1d1	slinná
žlázy	žláza	k1gFnPc1	žláza
octomilky	octomilka	k1gFnSc2	octomilka
(	(	kIx(	(
<g/>
Drosophila	Drosophila	k1gFnSc1	Drosophila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
nakopíruje	nakopírovat	k5eAaPmIp3nS	nakopírovat
genom	genom	k1gInSc1	genom
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
k	k	k7c3	k
replikaci	replikace	k1gFnSc3	replikace
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Chybovost	chybovost	k1gFnSc1	chybovost
replikace	replikace	k1gFnSc1	replikace
DNA	dna	k1gFnSc1	dna
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
)	)	kIx)	)
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
chybu	chyba	k1gFnSc4	chyba
za	za	k7c4	za
109	[number]	k4	109
<g/>
–	–	k?	–
<g/>
1010	[number]	k4	1010
nukleotidů	nukleotid	k1gInPc2	nukleotid
(	(	kIx(	(
<g/>
po	po	k7c6	po
proběhlém	proběhlý	k2eAgInSc6d1	proběhlý
proofreadingu	proofreading	k1gInSc6	proofreading
a	a	k8xC	a
korekci	korekce	k1gFnSc3	korekce
párování	párování	k1gNnSc2	párování
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mismatch	mismatch	k1gMnSc1	mismatch
repair	repair	k1gMnSc1	repair
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
pracuje	pracovat	k5eAaImIp3nS	pracovat
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
–	–	k?	–
<g/>
>	>	kIx)	>
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
by	by	kYmCp3nS	by
po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
špatně	špatně	k6eAd1	špatně
začleněného	začleněný	k2eAgInSc2d1	začleněný
nukleotidu	nukleotid	k1gInSc2	nukleotid
nemohla	moct	k5eNaImAgFnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
elongace	elongace	k1gFnSc1	elongace
řetězce	řetězec	k1gInSc2	řetězec
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
replikace	replikace	k1gFnSc2	replikace
genomu	genom	k1gInSc2	genom
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
bází	báze	k1gFnPc2	báze
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pivní	pivní	k2eAgFnSc2d1	pivní
kvasinky	kvasinka	k1gFnSc2	kvasinka
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
600	[number]	k4	600
bází	báze	k1gFnPc2	báze
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
u	u	k7c2	u
myši	myš	k1gFnSc2	myš
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
200	[number]	k4	200
bází	báze	k1gFnPc2	báze
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
mají	mít	k5eAaImIp3nP	mít
eukaryota	eukaryota	k1gFnSc1	eukaryota
více	hodně	k6eAd2	hodně
replikačních	replikační	k2eAgInPc2d1	replikační
počátků	počátek	k1gInPc2	počátek
(	(	kIx(	(
<g/>
kvasinka	kvasinka	k1gFnSc1	kvasinka
400	[number]	k4	400
<g/>
,	,	kIx,	,
myš	myš	k1gFnSc1	myš
kolem	kolem	k7c2	kolem
2	[number]	k4	2
500	[number]	k4	500
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
tempo	tempo	k1gNnSc4	tempo
replikace	replikace	k1gFnSc2	replikace
jejich	jejich	k3xOp3gInSc2	jejich
genomu	genom	k1gInSc2	genom
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
replikační	replikační	k2eAgInSc4d1	replikační
počátek	počátek	k1gInSc4	počátek
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
by	by	kYmCp3nS	by
neúnosně	únosně	k6eNd1	únosně
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
genom	genom	k1gInSc1	genom
replikoval	replikovat	k5eAaImAgInS	replikovat
<g/>
:	:	kIx,	:
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
totiž	totiž	k9	totiž
rychlost	rychlost	k1gFnSc1	rychlost
DNA	dno	k1gNnSc2	dno
polymerázy	polymeráza	k1gFnSc2	polymeráza
činí	činit	k5eAaImIp3nS	činit
pouhých	pouhý	k2eAgInPc2d1	pouhý
50	[number]	k4	50
nukleotidů	nukleotid	k1gInPc2	nukleotid
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
lidský	lidský	k2eAgInSc1d1	lidský
genom	genom	k1gInSc1	genom
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
3,2	[number]	k4	3,2
mld.	mld.	k?	mld.
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
=	=	kIx~	=
~	~	kIx~	~
<g/>
3,2	[number]	k4	3,2
Gb	Gb	k1gFnPc2	Gb
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
tak	tak	k9	tak
z	z	k7c2	z
jediného	jediný	k2eAgInSc2d1	jediný
replikačního	replikační	k2eAgInSc2d1	replikační
počátku	počátek	k1gInSc2	počátek
replikoval	replikovat	k5eAaImAgInS	replikovat
~	~	kIx~	~
<g/>
740	[number]	k4	740
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
DNA	dno	k1gNnSc2	dno
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
DNA	DNA	kA	DNA
dvě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
dvoušroubovice	dvoušroubovice	k1gFnPc1	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
DNA	dna	k1gFnSc1	dna
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
matrice	matrice	k1gFnSc1	matrice
či	či	k8xC	či
templát	templát	k1gInSc1	templát
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
DNA	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
replika	replika	k1gFnSc1	replika
<g/>
.	.	kIx.	.
</s>
<s>
Replikace	replikace	k1gFnSc1	replikace
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
semikonzervativní	semikonzervativní	k2eAgNnSc1d1	semikonzervativní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
nový	nový	k2eAgInSc1d1	nový
DNA	DNA	kA	DNA
řetězec	řetězec	k1gInSc1	řetězec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
původního	původní	k2eAgNnSc2d1	původní
vlákna	vlákno	k1gNnSc2	vlákno
a	a	k8xC	a
jednoho	jeden	k4xCgNnSc2	jeden
nového	nový	k2eAgNnSc2d1	nové
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
mnoho	mnoho	k4c1	mnoho
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
především	především	k9	především
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
však	však	k9	však
např.	např.	kA	např.
DNA	dna	k1gFnSc1	dna
ligáza	ligáza	k1gFnSc1	ligáza
<g/>
,	,	kIx,	,
DNA	dna	k1gFnSc1	dna
primáza	primáza	k1gFnSc1	primáza
<g/>
,	,	kIx,	,
helikáza	helikáza	k1gFnSc1	helikáza
či	či	k8xC	či
topoizomeráza	topoizomeráza	k1gFnSc1	topoizomeráza
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
původní	původní	k2eAgFnSc2d1	původní
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
je	být	k5eAaImIp3nS	být
replikováno	replikovat	k5eAaImNgNnS	replikovat
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
molekula	molekula	k1gFnSc1	molekula
DNA	dno	k1gNnSc2	dno
tzv.	tzv.	kA	tzv.
antiparalelní	antiparalelnit	k5eAaPmIp3nS	antiparalelnit
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
však	však	k9	však
je	být	k5eAaImIp3nS	být
orientováno	orientovat	k5eAaBmNgNnS	orientovat
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
však	však	k9	však
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pracovat	pracovat	k5eAaImF	pracovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
–	–	k?	–
<g/>
>	>	kIx)	>
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
vyřešeno	vyřešit	k5eAaPmNgNnS	vyřešit
elegantním	elegantní	k2eAgInSc7d1	elegantní
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
templátu	templát	k1gInSc2	templát
(	(	kIx(	(
<g/>
původní	původní	k2eAgFnSc1d1	původní
DNA	dna	k1gFnSc1	dna
<g/>
)	)	kIx)	)
vytvářena	vytvářen	k2eAgFnSc1d1	vytvářena
nová	nový	k2eAgFnSc1d1	nová
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
řetězci	řetězec	k1gInSc3	řetězec
komplementární	komplementární	k2eAgFnSc1d1	komplementární
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
původní	původní	k2eAgNnPc4d1	původní
DNA	dno	k1gNnPc4	dno
detekována	detekován	k2eAgFnSc1d1	detekována
báze	báze	k1gFnSc1	báze
adenin	adenin	k1gInSc1	adenin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
DNA	DNA	kA	DNA
přidán	přidán	k2eAgInSc4d1	přidán
nukleotid	nukleotid	k1gInSc4	nukleotid
obsahující	obsahující	k2eAgInSc4d1	obsahující
thymin	thymin	k1gInSc4	thymin
(	(	kIx(	(
<g/>
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
A	a	k8xC	a
a	a	k8xC	a
T	T	kA	T
k	k	k7c3	k
sobě	se	k3xPyFc3	se
patří	patřit	k5eAaImIp3nP	patřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
templátové	templátový	k2eAgFnSc6d1	templátová
DNA	dno	k1gNnSc2	dno
nalezen	naleznout	k5eAaPmNgInS	naleznout
například	například	k6eAd1	například
guanin	guanin	k1gInSc1	guanin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
řetězce	řetězec	k1gInSc2	řetězec
přidán	přidán	k2eAgInSc4d1	přidán
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
komplementarity	komplementarita	k1gFnSc2	komplementarita
cytosinový	cytosinový	k2eAgInSc4d1	cytosinový
nukleotid	nukleotid	k1gInSc4	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Replikace	replikace	k1gFnSc1	replikace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
základních	základní	k2eAgInPc6d1	základní
rysech	rys	k1gInPc6	rys
stejná	stejný	k2eAgFnSc1d1	stejná
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
organizmů	organizmus	k1gInPc2	organizmus
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
její	její	k3xOp3gInSc4	její
průběh	průběh	k1gInSc4	průběh
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
základních	základní	k2eAgInPc2d1	základní
kroků	krok	k1gInPc2	krok
<g/>
:	:	kIx,	:
Iniciace	iniciace	k1gFnSc1	iniciace
–	–	k?	–
rozpletení	rozpletení	k1gNnSc2	rozpletení
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
replikační	replikační	k2eAgFnSc2d1	replikační
vidlice	vidlice	k1gFnSc2	vidlice
a	a	k8xC	a
navázání	navázání	k1gNnSc2	navázání
enzymatického	enzymatický	k2eAgInSc2d1	enzymatický
komplexu	komplex	k1gInSc2	komplex
Elongace	elongace	k1gFnSc1	elongace
–	–	k?	–
přidávání	přidávání	k1gNnSc2	přidávání
nukleotidů	nukleotid	k1gInPc2	nukleotid
a	a	k8xC	a
postup	postup	k1gInSc1	postup
replikační	replikační	k2eAgFnSc2d1	replikační
vidlice	vidlice	k1gFnSc2	vidlice
Terminace	terminace	k1gFnSc2	terminace
–	–	k?	–
ukončení	ukončení	k1gNnSc1	ukončení
replikace	replikace	k1gFnSc2	replikace
Replikace	replikace	k1gFnSc2	replikace
nezačíná	začínat	k5eNaImIp3nS	začínat
na	na	k7c6	na
náhodném	náhodný	k2eAgNnSc6d1	náhodné
místě	místo	k1gNnSc6	místo
genomu	genom	k1gInSc2	genom
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
je	být	k5eAaImIp3nS	být
přesně	přesně	k6eAd1	přesně
určeno	určit	k5eAaPmNgNnS	určit
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
replikační	replikační	k2eAgInSc4d1	replikační
počátek	počátek	k1gInSc4	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
organizmy	organizmus	k1gInPc1	organizmus
těchto	tento	k3xDgInPc2	tento
počátků	počátek	k1gInPc2	počátek
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
(	(	kIx(	(
<g/>
a	a	k8xC	a
tak	tak	k9	tak
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
probíhá	probíhat	k5eAaImIp3nS	probíhat
několik	několik	k4yIc1	několik
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
polymerací	polymerace	k1gFnPc2	polymerace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednodušším	jednoduchý	k2eAgInPc3d2	jednodušší
organizmům	organizmus	k1gInPc3	organizmus
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
genomem	genom	k1gInSc7	genom
stačí	stačit	k5eAaBmIp3nS	stačit
někdy	někdy	k6eAd1	někdy
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
replikační	replikační	k2eAgInSc4d1	replikační
počátek	počátek	k1gInSc4	počátek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
replikaci	replikace	k1gFnSc3	replikace
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
potřeba	potřeba	k6eAd1	potřeba
krátká	krátký	k2eAgFnSc1d1	krátká
molekula	molekula	k1gFnSc1	molekula
RNA	RNA	kA	RNA
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
primer	primera	k1gFnPc2	primera
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
sekvenci	sekvence	k1gFnSc4	sekvence
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
speciální	speciální	k2eAgInSc1d1	speciální
enzym	enzym	k1gInSc1	enzym
RNA-polymerizační	RNAolymerizační	k2eAgInSc1d1	RNA-polymerizační
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
primáza	primáza	k1gFnSc1	primáza
<g/>
.	.	kIx.	.
</s>
<s>
Primer	primera	k1gFnPc2	primera
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
později	pozdě	k6eAd2	pozdě
odstraní	odstranit	k5eAaPmIp3nS	odstranit
a	a	k8xC	a
nahradí	nahradit	k5eAaPmIp3nS	nahradit
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
iniciaci	iniciace	k1gFnSc6	iniciace
replikace	replikace	k1gFnSc2	replikace
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
replikační	replikační	k2eAgInSc1d1	replikační
proces	proces	k1gInSc1	proces
totiž	totiž	k9	totiž
začíná	začínat	k5eAaImIp3nS	začínat
nukleofilní	nukleofilní	k2eAgFnSc7d1	nukleofilní
atakou	ataka	k1gFnSc7	ataka
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
-hydroxylové	ydroxylový	k2eAgFnSc2d1	-hydroxylový
skupiny	skupina	k1gFnSc2	skupina
tohoto	tento	k3xDgInSc2	tento
primeru	primera	k1gFnSc4	primera
na	na	k7c4	na
fosfátovou	fosfátový	k2eAgFnSc4d1	fosfátová
skupinu	skupina	k1gFnSc4	skupina
prvního	první	k4xOgInSc2	první
deoxyribonukleotidu	deoxyribonukleotid	k1gInSc2	deoxyribonukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
již	již	k6eAd1	již
deoxyribonukleotidy	deoxyribonukleotida	k1gFnPc1	deoxyribonukleotida
vážou	vázat	k5eAaImIp3nP	vázat
na	na	k7c4	na
3	[number]	k4	3
<g/>
'	'	kIx"	'
uhlík	uhlík	k1gInSc1	uhlík
předchozího	předchozí	k2eAgInSc2d1	předchozí
deoxyribonukleotidu	deoxyribonukleotid	k1gInSc2	deoxyribonukleotid
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
na	na	k7c6	na
primer	primera	k1gFnPc2	primera
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
tedy	tedy	k8xC	tedy
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
jednorázově	jednorázově	k6eAd1	jednorázově
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
netýká	týkat	k5eNaImIp3nS	týkat
opožďujícího	opožďující	k2eAgMnSc2d1	opožďující
se	se	k3xPyFc4	se
řetězce	řetězec	k1gInSc2	řetězec
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
kapitola	kapitola	k1gFnSc1	kapitola
"	"	kIx"	"
<g/>
elongace	elongace	k1gFnSc1	elongace
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniku	vznik	k1gInSc2	vznik
replikační	replikační	k2eAgFnPc1d1	replikační
vidlice	vidlice	k1gFnPc1	vidlice
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
hexamerické	hexamerický	k2eAgInPc1d1	hexamerický
enzymy	enzym	k1gInPc1	enzym
helikázy	helikáza	k1gFnSc2	helikáza
<g/>
,	,	kIx,	,
schopné	schopný	k2eAgFnPc1d1	schopná
oddálit	oddálit	k5eAaPmF	oddálit
obě	dva	k4xCgFnPc1	dva
molekuly	molekula	k1gFnPc1	molekula
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
strukturu	struktura	k1gFnSc4	struktura
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
Y	Y	kA	Y
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Udržet	udržet	k5eAaPmF	udržet
vlákna	vlákno	k1gNnPc1	vlákno
rozdělená	rozdělený	k2eAgNnPc1d1	rozdělené
pak	pak	k6eAd1	pak
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
proteiny	protein	k1gInPc1	protein
SSB	SSB	kA	SSB
proteiny	protein	k1gInPc7	protein
<g/>
.	.	kIx.	.
</s>
<s>
Helikázy	Helikáza	k1gFnPc1	Helikáza
však	však	k9	však
sice	sice	k8xC	sice
oddálí	oddálit	k5eAaPmIp3nP	oddálit
obě	dva	k4xCgNnPc1	dva
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dvoušroubovice	dvoušroubovice	k1gFnSc1	dvoušroubovice
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
replikační	replikační	k2eAgFnSc2d1	replikační
vidlice	vidlice	k1gFnSc2	vidlice
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
utahuje	utahovat	k5eAaImIp3nS	utahovat
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
zde	zde	k6eAd1	zde
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
řeší	řešit	k5eAaImIp3nS	řešit
topoizomerázy	topoizomeráza	k1gFnPc4	topoizomeráza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
přestřihnout	přestřihnout	k5eAaPmF	přestřihnout
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
uvolnit	uvolnit	k5eAaPmF	uvolnit
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ho	on	k3xPp3gInSc4	on
opět	opět	k6eAd1	opět
slepit	slepit	k5eAaBmF	slepit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
rozvinuta	rozvinut	k2eAgNnPc4d1	rozvinuto
DNA	dno	k1gNnPc4	dno
a	a	k8xC	a
na	na	k7c4	na
vlákna	vlákno	k1gNnPc4	vlákno
přisednou	přisednout	k5eAaPmIp3nP	přisednout
DNA	dna	k1gFnSc1	dna
polymerázy	polymeráza	k1gFnSc2	polymeráza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
začít	začít	k5eAaPmF	začít
samotnou	samotný	k2eAgFnSc4d1	samotná
replikaci	replikace	k1gFnSc4	replikace
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
pevně	pevně	k6eAd1	pevně
držela	držet	k5eAaImAgFnS	držet
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
jí	on	k3xPp3gFnSc3	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
tzv.	tzv.	kA	tzv.
svorkové	svorkový	k2eAgInPc4d1	svorkový
proteiny	protein	k1gInPc4	protein
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgFnSc4d1	tvořící
posuvnou	posuvný	k2eAgFnSc4d1	posuvná
svorku	svorka	k1gFnSc4	svorka
<g/>
,	,	kIx,	,
DNA	DNA	kA	DNA
clamp	clamp	k1gInSc1	clamp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
upozornit	upozornit	k5eAaPmF	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
původní	původní	k2eAgFnSc2d1	původní
templátové	templátový	k2eAgFnSc2d1	templátová
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
replikováno	replikovat	k5eAaImNgNnS	replikovat
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
schopná	schopný	k2eAgFnSc1d1	schopná
pracovat	pracovat	k5eAaImF	pracovat
jen	jen	k6eAd1	jen
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
–	–	k?	–
<g/>
>	>	kIx)	>
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
pátého	pátý	k4xOgInSc2	pátý
uhlíku	uhlík	k1gInSc2	uhlík
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
enzym	enzym	k1gInSc1	enzym
totiž	totiž	k9	totiž
umí	umět	k5eAaImIp3nS	umět
připojit	připojit	k5eAaPmF	připojit
nové	nový	k2eAgInPc4d1	nový
nukleotidy	nukleotid	k1gInPc4	nukleotid
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
3	[number]	k4	3
<g/>
'	'	kIx"	'
uhlík	uhlík	k1gInSc4	uhlík
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
omezení	omezení	k1gNnSc1	omezení
poněkud	poněkud	k6eAd1	poněkud
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
antiparalelní	antiparalelní	k2eAgNnSc1d1	antiparalelní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jeden	jeden	k4xCgInSc1	jeden
řetězec	řetězec	k1gInSc1	řetězec
směřuje	směřovat	k5eAaImIp3nS	směřovat
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
–	–	k?	–
<g/>
>	>	kIx)	>
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
druhý	druhý	k4xOgInSc1	druhý
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
–	–	k?	–
<g/>
>	>	kIx)	>
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
však	však	k9	však
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
řeší	řešit	k5eAaImIp3nS	řešit
elegantně	elegantně	k6eAd1	elegantně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
řetězci	řetězec	k1gInSc6	řetězec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
řetězec	řetězec	k1gInSc1	řetězec
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
angl.	angl.	k?	angl.
leading	leading	k1gInSc1	leading
strand	strand	k1gInSc1	strand
<g/>
)	)	kIx)	)
postupuje	postupovat	k5eAaImIp3nS	postupovat
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
zcela	zcela	k6eAd1	zcela
běžným	běžný	k2eAgInSc7d1	běžný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
řetězec	řetězec	k1gInSc1	řetězec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
orientován	orientovat	k5eAaBmNgInS	orientovat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
–	–	k?	–
<g/>
>	>	kIx)	>
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xS	jako
opožďující	opožďující	k2eAgMnSc1d1	opožďující
se	se	k3xPyFc4	se
řetězec	řetězec	k1gInSc1	řetězec
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
lagging	lagging	k1gInSc1	lagging
strand	strand	k1gInSc1	strand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
řetězec	řetězec	k1gInSc1	řetězec
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
kopírován	kopírovat	k5eAaImNgInS	kopírovat
rovněž	rovněž	k9	rovněž
v	v	k7c4	v
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
–	–	k?	–
<g/>
>	>	kIx)	>
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
směru	směr	k1gInSc2	směr
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
to	ten	k3xDgNnSc4	ten
buňky	buňka	k1gFnPc1	buňka
neumí	umět	k5eNaImIp3nP	umět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
replikuje	replikovat	k5eAaImIp3nS	replikovat
tento	tento	k3xDgInSc4	tento
řetězec	řetězec	k1gInSc4	řetězec
po	po	k7c6	po
malých	malý	k2eAgFnPc6d1	malá
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Okazakiho	Okazaki	k1gMnSc2	Okazaki
fragmentech	fragment	k1gInPc6	fragment
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
nukleotidů	nukleotid	k1gInPc2	nukleotid
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
asi	asi	k9	asi
10	[number]	k4	10
<g/>
krát	krát	k6eAd1	krát
delší	dlouhý	k2eAgInPc1d2	delší
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
fragmenty	fragment	k1gInPc1	fragment
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
každého	každý	k3xTgMnSc2	každý
Okazakiho	Okazaki	k1gMnSc2	Okazaki
fragmentu	fragment	k1gInSc2	fragment
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
pokaždé	pokaždé	k6eAd1	pokaždé
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
RNA	RNA	kA	RNA
primer	primera	k1gFnPc2	primera
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
vystřihnut	vystřihnout	k5eAaPmNgInS	vystřihnout
a	a	k8xC	a
Okazakiho	Okazaki	k1gMnSc4	Okazaki
fragmenty	fragment	k1gInPc1	fragment
jsou	být	k5eAaImIp3nP	být
pospojovány	pospojován	k2eAgFnPc4d1	pospojován
DNA	DNA	kA	DNA
ligázou	ligáza	k1gFnSc7	ligáza
do	do	k7c2	do
souvislého	souvislý	k2eAgNnSc2d1	souvislé
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
vedoucího	vedoucí	k1gMnSc2	vedoucí
(	(	kIx(	(
<g/>
leading	leading	k1gInSc1	leading
<g/>
)	)	kIx)	)
řetězce	řetězec	k1gInPc1	řetězec
k	k	k7c3	k
nerozeznání	nerozeznání	k1gNnSc3	nerozeznání
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zvláštní	zvláštní	k2eAgFnSc3d1	zvláštní
replikaci	replikace	k1gFnSc3	replikace
na	na	k7c6	na
zpožďujícím	zpožďující	k2eAgInSc6d1	zpožďující
se	se	k3xPyFc4	se
vlákně	vlákno	k1gNnSc6	vlákno
se	se	k3xPyFc4	se
také	také	k9	také
replikace	replikace	k1gFnSc1	replikace
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
semidiskontinuální	semidiskontinuální	k2eAgNnSc1d1	semidiskontinuální
<g/>
.	.	kIx.	.
</s>
<s>
Replikace	replikace	k1gFnSc1	replikace
končí	končit	k5eAaImIp3nS	končit
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zhotovena	zhotoven	k2eAgFnSc1d1	zhotovena
kopie	kopie	k1gFnSc1	kopie
celé	celý	k2eAgFnSc2d1	celá
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
okamžik	okamžik	k1gInSc4	okamžik
sejdou	sejít	k5eAaPmIp3nP	sejít
obě	dva	k4xCgFnPc1	dva
replikační	replikační	k2eAgFnPc1d1	replikační
vidlice	vidlice	k1gFnPc1	vidlice
a	a	k8xC	a
splynou	splynout	k5eAaPmIp3nP	splynout
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
splývají	splývat	k5eAaImIp3nP	splývat
replikační	replikační	k2eAgFnPc1d1	replikační
vidlice	vidlice	k1gFnPc1	vidlice
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
dosyntetizují	dosyntetizovat	k5eAaBmIp3nP	dosyntetizovat
"	"	kIx"	"
<g/>
svou	svůj	k3xOyFgFnSc4	svůj
<g/>
"	"	kIx"	"
část	část	k1gFnSc4	část
genomu	genom	k1gInSc2	genom
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
eukaryotické	eukaryotický	k2eAgInPc4d1	eukaryotický
chromozomy	chromozom	k1gInPc4	chromozom
lineární	lineární	k2eAgInSc1d1	lineární
<g/>
,	,	kIx,	,
DNA	DNA	kA	DNA
polymerázy	polymeráza	k1gFnPc1	polymeráza
nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
replikovat	replikovat	k5eAaImF	replikovat
jejich	jejich	k3xOp3gFnPc1	jejich
koncové	koncový	k2eAgFnPc1d1	koncová
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
telomery	telomera	k1gFnPc1	telomera
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
replikovaná	replikovaný	k2eAgFnSc1d1	replikovaná
DNA	dna	k1gFnSc1	dna
nepatrně	nepatrně	k6eAd1	nepatrně
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
než	než	k8xS	než
původní	původní	k2eAgInSc1d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
určitém	určitý	k2eAgInSc6d1	určitý
počtu	počet	k1gInSc6	počet
buněčných	buněčný	k2eAgNnPc2d1	buněčné
dělení	dělení	k1gNnPc2	dělení
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
začalo	začít	k5eAaPmAgNnS	začít
vadit	vadit	k5eAaImF	vadit
(	(	kIx(	(
<g/>
Hayflickův	Hayflickův	k2eAgInSc4d1	Hayflickův
limit	limit	k1gInSc4	limit
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nepředstavuje	představovat	k5eNaImIp3nS	představovat
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
během	během	k7c2	během
meiózy	meióza	k1gFnSc2	meióza
se	se	k3xPyFc4	se
velikost	velikost	k1gFnSc1	velikost
telomer	telomera	k1gFnPc2	telomera
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
pomocí	pomocí	k7c2	pomocí
enzymů	enzym	k1gInPc2	enzym
telomeráz	telomeráza	k1gFnPc2	telomeráza
<g/>
.	.	kIx.	.
</s>
<s>
Replikace	replikace	k1gFnSc1	replikace
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
dobře	dobře	k6eAd1	dobře
prozkoumaná	prozkoumaný	k2eAgFnSc1d1	prozkoumaná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bakterie	bakterie	k1gFnSc1	bakterie
(	(	kIx(	(
<g/>
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
menší	malý	k2eAgInSc1d2	menší
genom	genom	k1gInSc1	genom
<g/>
)	)	kIx)	)
představují	představovat	k5eAaImIp3nP	představovat
snadnější	snadný	k2eAgInSc4d2	snadnější
model	model	k1gInSc4	model
než	než	k8xS	než
komplexní	komplexní	k2eAgInPc1d1	komplexní
eukaryotické	eukaryotický	k2eAgInPc1d1	eukaryotický
organizmy	organizmus	k1gInPc1	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnPc1d1	zásadní
pro	pro	k7c4	pro
průběh	průběh	k1gInSc4	průběh
replikace	replikace	k1gFnSc2	replikace
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
molekulu	molekula	k1gFnSc4	molekula
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
nukleoid	nukleoid	k1gInSc1	nukleoid
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	coli	k6eAd1	coli
má	mít	k5eAaImIp3nS	mít
jediný	jediný	k2eAgInSc4d1	jediný
replikační	replikační	k2eAgInSc4d1	replikační
počátek	počátek	k1gInSc4	počátek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
genovém	genový	k2eAgInSc6d1	genový
lokusu	lokus	k1gInSc6	lokus
nazvaném	nazvaný	k2eAgInSc6d1	nazvaný
oriC	oriC	k?	oriC
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
oběma	dva	k4xCgMnPc7	dva
směry	směr	k1gInPc7	směr
replikační	replikační	k2eAgFnSc2d1	replikační
vidlice	vidlice	k1gFnSc2	vidlice
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
nových	nový	k2eAgInPc2d1	nový
řetězců	řetězec	k1gInPc2	řetězec
DNA	DNA	kA	DNA
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
původních	původní	k2eAgInPc2d1	původní
řetězců	řetězec	k1gInPc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
polymerázou	polymeráza	k1gFnSc7	polymeráza
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
III	III	kA	III
obsahující	obsahující	k2eAgFnSc1d1	obsahující
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgFnPc2d1	různá
podjednotek	podjednotka	k1gFnPc2	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Polymerázy	Polymeráza	k1gFnPc1	Polymeráza
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
hrají	hrát	k5eAaImIp3nP	hrát
pouze	pouze	k6eAd1	pouze
méně	málo	k6eAd2	málo
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
z	z	k7c2	z
lokusu	lokus	k1gInSc2	lokus
oriC	oriC	k?	oriC
byly	být	k5eAaImAgFnP	být
vyslány	vyslat	k5eAaPmNgFnP	vyslat
dvě	dva	k4xCgFnPc1	dva
replikační	replikační	k2eAgFnPc1d1	replikační
vidlice	vidlice	k1gFnPc1	vidlice
po	po	k7c6	po
kruhové	kruhový	k2eAgFnSc6d1	kruhová
molekule	molekula	k1gFnSc6	molekula
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
vidlice	vidlice	k1gFnPc1	vidlice
se	se	k3xPyFc4	se
logicky	logicky	k6eAd1	logicky
potkají	potkat	k5eAaPmIp3nP	potkat
přibližně	přibližně	k6eAd1	přibližně
"	"	kIx"	"
<g/>
na	na	k7c4	na
půl	půl	k1xP	půl
cesty	cesta	k1gFnSc2	cesta
<g/>
"	"	kIx"	"
kolem	kolem	k6eAd1	kolem
dokola	dokola	k6eAd1	dokola
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
100	[number]	k4	100
kB	kb	kA	kb
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
středu	střed	k1gInSc2	střed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
terminační	terminační	k2eAgInSc1d1	terminační
(	(	kIx(	(
<g/>
ukončovací	ukončovací	k2eAgFnPc1d1	ukončovací
<g/>
)	)	kIx)	)
oblasti	oblast	k1gFnPc1	oblast
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
23	[number]	k4	23
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
terminační	terminační	k2eAgFnPc1d1	terminační
sekvence	sekvence	k1gFnPc1	sekvence
existují	existovat	k5eAaImIp3nP	existovat
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc4	dva
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
<g/>
)	)	kIx)	)
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
TerA	TerA	k1gFnSc2	TerA
<g/>
,	,	kIx,	,
TerB	TerB	k1gFnPc2	TerB
<g/>
,	,	kIx,	,
TerC	terca	k1gFnPc2	terca
a	a	k8xC	a
Te	Te	k1gFnPc2	Te
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
sekvence	sekvence	k1gFnPc4	sekvence
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
protein	protein	k1gInSc1	protein
Tus	Tus	k1gFnSc2	Tus
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
ukončit	ukončit	k5eAaPmF	ukončit
replikaci	replikace	k1gFnSc4	replikace
v	v	k7c6	v
momentu	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
replikační	replikační	k2eAgFnSc1d1	replikační
vidlice	vidlice	k1gFnSc1	vidlice
dostane	dostat	k5eAaPmIp3nS	dostat
k	k	k7c3	k
terminačním	terminační	k2eAgFnPc3d1	terminační
sekvencím	sekvence	k1gFnPc3	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Replikace	replikace	k1gFnSc1	replikace
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
DNA	DNA	kA	DNA
a	a	k8xC	a
plastidové	plastidový	k2eAgFnSc2d1	plastidová
DNA	DNA	kA	DNA
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
značnou	značný	k2eAgFnSc4d1	značná
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
replikací	replikace	k1gFnSc7	replikace
DNA	dno	k1gNnSc2	dno
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
archebakterie	archebakterie	k1gFnSc1	archebakterie
(	(	kIx(	(
<g/>
Archaea	Archaea	k1gFnSc1	Archaea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
mezi	mezi	k7c4	mezi
prokaryota	prokaryot	k1gMnSc4	prokaryot
<g/>
,	,	kIx,	,
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
poměrně	poměrně	k6eAd1	poměrně
značné	značný	k2eAgInPc1d1	značný
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
replikací	replikace	k1gFnSc7	replikace
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
replikace	replikace	k1gFnSc2	replikace
probíhá	probíhat	k5eAaImIp3nS	probíhat
jen	jen	k9	jen
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
replikačního	replikační	k2eAgInSc2d1	replikační
počátku	počátek	k1gInSc2	počátek
<g/>
,	,	kIx,	,
u	u	k7c2	u
archeí	archeí	k1gFnSc2	archeí
je	být	k5eAaImIp3nS	být
těchto	tento	k3xDgNnPc2	tento
míst	místo	k1gNnPc2	místo
zpravidla	zpravidla	k6eAd1	zpravidla
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
příslušné	příslušný	k2eAgNnSc1d1	příslušné
DNA	dno	k1gNnPc1	dno
polymerázy	polymeráza	k1gFnSc2	polymeráza
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
spíše	spíše	k9	spíše
eukaryotním	eukaryotní	k2eAgInSc7d1	eukaryotní
DNA	DNA	kA	DNA
polymerázám	polymeráza	k1gFnPc3	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
Eukaryotické	Eukaryotický	k2eAgInPc1d1	Eukaryotický
genomy	genom	k1gInPc1	genom
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
prokaryotické	prokaryotický	k2eAgInPc1d1	prokaryotický
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
replikace	replikace	k1gFnSc2	replikace
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
replikační	replikační	k2eAgInSc4d1	replikační
počátek	počátek	k1gInSc4	počátek
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
začíná	začínat	k5eAaImIp3nS	začínat
replikace	replikace	k1gFnSc1	replikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
eukaryota	eukaryota	k1gFnSc1	eukaryota
replikačních	replikační	k2eAgInPc2d1	replikační
počátků	počátek	k1gInPc2	počátek
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kvasinky	kvasinka	k1gFnSc2	kvasinka
Saccharomyces	Saccharomyces	k1gInSc4	Saccharomyces
cerevisiae	cerevisiae	k6eAd1	cerevisiae
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
replikační	replikační	k2eAgInPc1d1	replikační
počátky	počátek	k1gInPc1	počátek
označují	označovat	k5eAaImIp3nP	označovat
ARS	ARS	kA	ARS
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
kolem	kolem	k6eAd1	kolem
400	[number]	k4	400
(	(	kIx(	(
<g/>
u	u	k7c2	u
obojživelníků	obojživelník	k1gMnPc2	obojživelník
však	však	k9	však
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
až	až	k9	až
15	[number]	k4	15
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
replikačních	replikační	k2eAgInPc2d1	replikační
počátků	počátek	k1gInPc2	počátek
směřují	směřovat	k5eAaImIp3nP	směřovat
oběma	dva	k4xCgMnPc7	dva
směry	směr	k1gInPc7	směr
replikační	replikační	k2eAgFnSc2d1	replikační
vidlice	vidlice	k1gFnSc2	vidlice
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
Y.	Y.	kA	Y.
Když	když	k8xS	když
se	se	k3xPyFc4	se
potkají	potkat	k5eAaPmIp3nP	potkat
dvě	dva	k4xCgFnPc1	dva
protijdoucí	protijdoucí	k2eAgFnPc1d1	protijdoucí
replikační	replikační	k2eAgFnPc1d1	replikační
vidlice	vidlice	k1gFnPc1	vidlice
<g/>
,	,	kIx,	,
jednoduše	jednoduše	k6eAd1	jednoduše
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
splynutí	splynutí	k1gNnSc3	splynutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
replikaci	replikace	k1gFnSc6	replikace
eukaryotického	eukaryotický	k2eAgInSc2d1	eukaryotický
genomu	genom	k1gInSc2	genom
zřejmě	zřejmě	k6eAd1	zřejmě
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
u	u	k7c2	u
SSB	SSB	kA	SSB
proteinů	protein	k1gInPc2	protein
<g/>
:	:	kIx,	:
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
"	"	kIx"	"
<g/>
stabilizátory	stabilizátor	k1gInPc1	stabilizátor
jednovláknové	jednovláknový	k2eAgInPc1d1	jednovláknový
DNA	DNA	kA	DNA
<g/>
"	"	kIx"	"
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
jediné	jediný	k2eAgFnSc2d1	jediná
podjednotky	podjednotka	k1gFnSc2	podjednotka
<g/>
,	,	kIx,	,
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
podjednotek	podjednotka	k1gFnPc2	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
také	také	k9	také
komplikovanější	komplikovaný	k2eAgMnSc1d2	komplikovanější
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
počtu	počet	k1gInSc2	počet
DNA	dno	k1gNnSc2	dno
polymeráz	polymeráza	k1gFnPc2	polymeráza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
nejméně	málo	k6eAd3	málo
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
α	α	k?	α
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
podjednotku	podjednotka	k1gFnSc4	podjednotka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
primáza	primáza	k1gFnSc1	primáza
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
vytvořit	vytvořit	k5eAaPmF	vytvořit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
každého	každý	k3xTgMnSc2	každý
Okazakiho	Okazaki	k1gMnSc2	Okazaki
fragmentu	fragment	k1gInSc2	fragment
RNA	RNA	kA	RNA
primer	primera	k1gFnPc2	primera
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přidá	přidat	k5eAaPmIp3nS	přidat
několik	několik	k4yIc1	několik
DNA	DNA	kA	DNA
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zřejmě	zřejmě	k6eAd1	zřejmě
předá	předat	k5eAaPmIp3nS	předat
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
3	[number]	k4	3
<g/>
'	'	kIx"	'
konci	konec	k1gInSc6	konec
prodlužujícího	prodlužující	k2eAgMnSc2d1	prodlužující
se	se	k3xPyFc4	se
řetězce	řetězec	k1gInSc2	řetězec
DNA	dno	k1gNnSc2	dno
polymeráze	polymeráze	k1gFnSc2	polymeráze
δ	δ	k?	δ
Vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
řetězec	řetězec	k1gInSc1	řetězec
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
rovněž	rovněž	k9	rovněž
načat	načat	k2eAgMnSc1d1	načat
DNA	DNA	kA	DNA
polymerázou	polymeráza	k1gFnSc7	polymeráza
α	α	k?	α
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
opožďujícího	opožďující	k2eAgMnSc2d1	opožďující
se	se	k3xPyFc4	se
řetězce	řetězec	k1gInSc2	řetězec
se	s	k7c7	s
zde	zde	k6eAd1	zde
zřejmě	zřejmě	k6eAd1	zřejmě
předává	předávat	k5eAaImIp3nS	předávat
vlákno	vlákno	k1gNnSc1	vlákno
DNA	dno	k1gNnSc2	dno
polymeráze	polymeráze	k1gFnSc2	polymeráze
ε	ε	k?	ε
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
panují	panovat	k5eAaImIp3nP	panovat
diskuse	diskuse	k1gFnPc1	diskuse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
živého	živý	k2eAgMnSc2d1	živý
a	a	k8xC	a
neživého	živý	k2eNgInSc2d1	neživý
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
poměrně	poměrně	k6eAd1	poměrně
specifický	specifický	k2eAgInSc1d1	specifický
typ	typ	k1gInSc1	typ
replikace	replikace	k1gFnSc2	replikace
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
odvíjející	odvíjející	k2eAgFnPc1d1	odvíjející
se	se	k3xPyFc4	se
od	od	k7c2	od
jejich	jejich	k3xOp3gInSc2	jejich
parazitického	parazitický	k2eAgInSc2d1	parazitický
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
tzv.	tzv.	kA	tzv.
RNA	RNA	kA	RNA
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
rozmnožovacím	rozmnožovací	k2eAgInSc6d1	rozmnožovací
cyklu	cyklus	k1gInSc6	cyklus
molekuly	molekula	k1gFnSc2	molekula
DNA	dno	k1gNnSc2	dno
vůbec	vůbec	k9	vůbec
nefigurují	figurovat	k5eNaImIp3nP	figurovat
(	(	kIx(	(
<g/>
u	u	k7c2	u
těch	ten	k3xDgNnPc2	ten
ostatních	ostatní	k1gNnPc2	ostatní
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
retrovirů	retrovir	k1gInPc2	retrovir
<g/>
,	,	kIx,	,
molekula	molekula	k1gFnSc1	molekula
DNA	dno	k1gNnSc2	dno
figuruje	figurovat	k5eAaImIp3nS	figurovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
replikaci	replikace	k1gFnSc3	replikace
DNA	dno	k1gNnSc2	dno
polymerázou	polymeráza	k1gFnSc7	polymeráza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Replikace	replikace	k1gFnSc1	replikace
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
týká	týkat	k5eAaImIp3nS	týkat
výhradně	výhradně	k6eAd1	výhradně
DNA	dna	k1gFnSc1	dna
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
DNA	dno	k1gNnSc2	dno
viry	vir	k1gInPc1	vir
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jednovláknovou	jednovláknový	k2eAgFnSc4d1	jednovláknová
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
musí	muset	k5eAaImIp3nS	muset
nejprve	nejprve	k6eAd1	nejprve
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
komplementárního	komplementární	k2eAgNnSc2d1	komplementární
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
klasická	klasický	k2eAgFnSc1d1	klasická
dvoušroubovice	dvoušroubovice	k1gFnSc1	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
virů	vir	k1gInPc2	vir
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgFnPc2d1	další
odchylek	odchylka	k1gFnPc2	odchylka
od	od	k7c2	od
běžného	běžný	k2eAgNnSc2d1	běžné
schématu	schéma	k1gNnSc2	schéma
DNA	dno	k1gNnSc2	dno
replikace	replikace	k1gFnSc2	replikace
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
Polyomaviru	Polyomavir	k1gInSc2	Polyomavir
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
infikuje	infikovat	k5eAaBmIp3nS	infikovat
eukaryotické	eukaryotický	k2eAgFnPc4d1	eukaryotická
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
DNA	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
replikována	replikovat	k5eAaImNgFnS	replikovat
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
vlastní	vlastní	k2eAgInSc1d1	vlastní
eukaryotický	eukaryotický	k2eAgInSc1d1	eukaryotický
genom	genom	k1gInSc1	genom
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
je	být	k5eAaImIp3nS	být
prostý	prostý	k2eAgMnSc1d1	prostý
<g/>
:	:	kIx,	:
viry	vir	k1gInPc1	vir
často	často	k6eAd1	často
zneužívají	zneužívat	k5eAaImIp3nP	zneužívat
hostitelské	hostitelský	k2eAgFnSc2d1	hostitelská
DNA	DNA	kA	DNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
a	a	k8xC	a
celou	celá	k1gFnSc4	celá
jejich	jejich	k3xOp3gFnSc4	jejich
enzymatickou	enzymatický	k2eAgFnSc4d1	enzymatická
mašinérii	mašinérie	k1gFnSc4	mašinérie
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
rozdíl	rozdíl	k1gInSc1	rozdíl
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
Polyomaviru	Polyomavir	k1gInSc2	Polyomavir
musí	muset	k5eAaImIp3nS	muset
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
navázání	navázání	k1gNnSc3	navázání
tzv.	tzv.	kA	tzv.
velkého	velký	k2eAgInSc2d1	velký
T	T	kA	T
antigenu	antigen	k1gInSc2	antigen
(	(	kIx(	(
<g/>
large	large	k1gInSc1	large
T-antigen	Tntigen	k1gInSc1	T-antigen
<g/>
)	)	kIx)	)
na	na	k7c4	na
replikační	replikační	k2eAgInSc4d1	replikační
počátek	počátek	k1gInSc4	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
mechanismus	mechanismus	k1gInSc1	mechanismus
replikace	replikace	k1gFnSc2	replikace
DNA	DNA	kA	DNA
využívají	využívat	k5eAaPmIp3nP	využívat
mnohé	mnohý	k2eAgFnPc1d1	mnohá
metody	metoda	k1gFnPc1	metoda
moderní	moderní	k2eAgFnSc2d1	moderní
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
genetiky	genetika	k1gFnSc2	genetika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
založena	založen	k2eAgFnSc1d1	založena
například	například	k6eAd1	například
polymerázová	polymerázový	k2eAgFnSc1d1	polymerázová
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
(	(	kIx(	(
<g/>
PCR	PCR	kA	PCR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
schopná	schopný	k2eAgFnSc1d1	schopná
namnožit	namnožit	k5eAaPmF	namnožit
(	(	kIx(	(
<g/>
amplifikovat	amplifikovat	k5eAaImF	amplifikovat
<g/>
)	)	kIx)	)
vzorek	vzorek	k1gInSc1	vzorek
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mnohé	mnohý	k2eAgInPc1d1	mnohý
způsoby	způsob	k1gInPc1	způsob
sekvenování	sekvenování	k1gNnSc2	sekvenování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
replikaci	replikace	k1gFnSc4	replikace
daného	daný	k2eAgInSc2d1	daný
vzorku	vzorek	k1gInSc2	vzorek
DNA	DNA	kA	DNA
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Sangerova	Sangerův	k2eAgFnSc1d1	Sangerova
metoda	metoda	k1gFnSc1	metoda
či	či	k8xC	či
pyrosekvenování	pyrosekvenování	k1gNnSc1	pyrosekvenování
<g/>
.	.	kIx.	.
</s>
