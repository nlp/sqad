<s>
Replikace	replikace	k1gFnSc1	replikace
DNA	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
tvorby	tvorba	k1gFnSc2	tvorba
kopií	kopie	k1gFnPc2	kopie
molekuly	molekula	k1gFnSc2	molekula
deoxyribonukleové	deoxyribonukleový	k2eAgFnSc2d1	deoxyribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
přenáší	přenášet	k5eAaImIp3nS	přenášet
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
templát	templát	k1gInSc1	templát
<g/>
,	,	kIx,	,
matrice	matrice	k1gFnSc1	matrice
<g/>
)	)	kIx)	)
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
molekuly	molekula	k1gFnSc2	molekula
stejného	stejný	k2eAgInSc2d1	stejný
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
replika	replika	k1gFnSc1	replika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
