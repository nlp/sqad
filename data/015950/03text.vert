<s>
<g/>
mw	mw	k?
</s>
<s>
<g/>
mw	mw	k?
Typ	typ	k1gInSc1
TLD	TLD	kA
</s>
<s>
Národní	národní	k2eAgFnSc1d1
doména	doména	k1gFnSc1
nejvyššího	vysoký	k2eAgInSc2d3
řádu	řád	k1gInSc2
Status	status	k1gInSc1
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1
</s>
<s>
<g/>
mw	mw	k?
je	být	k5eAaImIp3nS
internetová	internetový	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
doména	doména	k1gFnSc1
nejvyššího	vysoký	k2eAgInSc2d3
řádu	řád	k1gInSc2
pro	pro	k7c4
Malawi	Malawi	k1gNnSc4
(	(	kIx(
<g/>
podle	podle	k7c2
ISO	ISO	kA
3166	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
MW	MW	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Whois	Whois	k1gFnSc1
informace	informace	k1gFnSc2
IANA	Ianus	k1gMnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw	mw	k?
registrace	registrace	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Národní	národní	k2eAgFnSc2d1
domény	doména	k1gFnSc2
nejvyššího	vysoký	k2eAgInSc2d3
řádu	řád	k1gInSc2
Užívané	užívaný	k2eAgFnSc2d1
</s>
<s>
<g/>
ac	ac	k?
.	.	kIx.
<g/>
ad	ad	k7c4
.	.	kIx.
<g/>
ae	ae	k?
.	.	kIx.
<g/>
af	af	k?
.	.	kIx.
<g/>
ag	ag	k?
.	.	kIx.
<g/>
ai	ai	k?
.	.	kIx.
<g/>
al	ala	k1gFnPc2
.	.	kIx.
<g/>
am	am	k?
.	.	kIx.
<g/>
an	an	k?
.	.	kIx.
<g/>
ao	ao	k?
.	.	kIx.
<g/>
aq	aq	k?
.	.	kIx.
<g/>
ar	ar	k1gInSc1
.	.	kIx.
<g/>
as	as	k1gInSc1
.	.	kIx.
<g/>
at	at	k?
.	.	kIx.
<g/>
au	au	k0
.	.	kIx.
<g/>
aw	aw	k?
.	.	kIx.
<g/>
ax	ax	k?
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
az	az	k?
.	.	kIx.
<g/>
ba	ba	k9
.	.	kIx.
<g/>
bb	bb	k?
.	.	kIx.
<g/>
bd	bd	k?
.	.	kIx.
<g/>
be	be	k?
.	.	kIx.
<g/>
bf	bf	k?
.	.	kIx.
<g/>
bg	bg	k?
.	.	kIx.
<g/>
bh	bh	k?
.	.	kIx.
<g/>
bi	bi	k?
.	.	kIx.
<g/>
bj	bj	k?
.	.	kIx.
<g/>
bm	bm	k?
.	.	kIx.
<g/>
bn	bn	k?
.	.	kIx.
<g/>
bo	bo	k?
.	.	kIx.
<g/>
br	br	k0
.	.	kIx.
<g/>
bs	bs	k?
.	.	kIx.
<g/>
bt	bt	k?
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
bw	bw	k?
.	.	kIx.
<g/>
by	by	kYmCp3nS
.	.	kIx.
<g/>
bz	bz	k0
.	.	kIx.
<g/>
ca	ca	kA
.	.	kIx.
<g/>
cc	cc	k?
.	.	kIx.
<g/>
cd	cd	kA
.	.	kIx.
<g/>
cf	cf	k?
.	.	kIx.
<g/>
cg	cg	kA
.	.	kIx.
<g/>
ch	ch	k0
.	.	kIx.
<g/>
ci	ci	k0
.	.	kIx.
<g/>
ck	ck	k?
.	.	kIx.
<g/>
cl	cl	k?
.	.	kIx.
<g/>
cm	cm	kA
.	.	kIx.
<g/>
cn	cn	k?
.	.	kIx.
<g/>
co	co	k8xS
.	.	kIx.
<g/>
cr	cr	k0
.	.	kIx.
<g/>
cu	cu	k?
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
cv	cv	k?
.	.	kIx.
<g/>
cw	cw	k?
.	.	kIx.
<g/>
cx	cx	k?
.	.	kIx.
<g/>
cy	cy	k?
.	.	kIx.
<g/>
cz	cz	k?
.	.	kIx.
<g/>
de	de	k?
.	.	kIx.
<g/>
dj	dj	k?
.	.	kIx.
<g/>
dk	dk	k?
.	.	kIx.
<g/>
dm	dm	kA
.	.	kIx.
<g/>
do	do	k7c2
.	.	kIx.
<g/>
dz	dz	k?
.	.	kIx.
<g/>
ec	ec	k?
.	.	kIx.
<g/>
ee	ee	k?
.	.	kIx.
<g/>
eg	ego	k1gNnPc2
.	.	kIx.
<g/>
er	er	k?
.	.	kIx.
<g/>
es	es	k1gNnPc2
.	.	kIx.
<g/>
et	et	k?
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
eu	eu	k?
.	.	kIx.
<g/>
fi	fi	k0
.	.	kIx.
<g/>
fj	fj	k?
.	.	kIx.
<g/>
fk	fk	k?
.	.	kIx.
<g/>
fm	fm	k?
.	.	kIx.
<g/>
fo	fo	k?
.	.	kIx.
<g/>
fr	fr	k0
.	.	kIx.
<g/>
ga	ga	k?
.	.	kIx.
<g/>
gd	gd	k?
.	.	kIx.
<g/>
ge	ge	k?
.	.	kIx.
<g/>
gf	gf	k?
.	.	kIx.
<g/>
gg	gg	k?
.	.	kIx.
<g/>
gh	gh	k?
.	.	kIx.
<g/>
gi	gi	k?
.	.	kIx.
<g/>
gl	gl	k?
.	.	kIx.
<g/>
gm	gm	k?
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
gn	gn	k?
.	.	kIx.
<g/>
gp	gp	k?
.	.	kIx.
<g/>
gq	gq	k?
.	.	kIx.
<g/>
gr	gr	k?
.	.	kIx.
<g/>
gs	gs	k?
.	.	kIx.
<g/>
gt	gt	k?
.	.	kIx.
<g/>
gu	gu	k?
.	.	kIx.
<g/>
gw	gw	k?
.	.	kIx.
<g/>
gy	gy	k?
.	.	kIx.
<g/>
hk	hk	k?
.	.	kIx.
<g/>
hm	hm	k?
.	.	kIx.
<g/>
hn	hn	k?
.	.	kIx.
<g/>
hr	hr	k2eAgFnSc4d1
.	.	kIx.
<g/>
ht	ht	k?
.	.	kIx.
<g/>
hu	hu	k0
.	.	kIx.
<g/>
id	idy	k1gFnPc2
.	.	kIx.
<g/>
ie	ie	k?
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
il	il	k?
.	.	kIx.
<g/>
im	im	k?
.	.	kIx.
<g/>
in	in	k?
.	.	kIx.
<g/>
io	io	k?
.	.	kIx.
<g/>
iq	iq	kA
.	.	kIx.
<g/>
ir	ir	k?
.	.	kIx.
<g/>
is	is	k?
.	.	kIx.
<g/>
it	it	k?
.	.	kIx.
<g/>
je	být	k5eAaImIp3nS
.	.	kIx.
<g/>
jm	jm	k?
.	.	kIx.
<g/>
jo	jo	k9
.	.	kIx.
<g/>
jp	jp	k?
.	.	kIx.
<g/>
ke	k	k7c3
.	.	kIx.
<g/>
kg	kg	kA
.	.	kIx.
<g/>
kh	kh	k0
.	.	kIx.
<g/>
ki	ki	k?
.	.	kIx.
<g/>
km	km	kA
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
kn	kn	k?
.	.	kIx.
<g/>
kp	kp	k?
.	.	kIx.
<g/>
kr	kr	k?
.	.	kIx.
<g/>
kw	kw	kA
.	.	kIx.
<g/>
ky	ky	k?
.	.	kIx.
<g/>
kz	kz	k?
.	.	kIx.
<g/>
la	la	k1gNnSc1
.	.	kIx.
<g/>
lb	lb	k?
.	.	kIx.
<g/>
lc	lc	k?
.	.	kIx.
<g/>
li	li	k8xS
.	.	kIx.
<g/>
lk	lk	k?
.	.	kIx.
<g/>
lr	lr	k?
.	.	kIx.
<g/>
ls	ls	k?
.	.	kIx.
<g/>
lt	lt	k?
.	.	kIx.
<g/>
lu	lu	k?
.	.	kIx.
<g/>
lv	lv	k?
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
ly	ly	k?
.	.	kIx.
<g/>
ma	ma	k?
.	.	kIx.
<g/>
mc	mc	k?
.	.	kIx.
<g/>
me	me	k?
.	.	kIx.
<g/>
md	md	k?
.	.	kIx.
<g/>
mg	mg	kA
.	.	kIx.
<g/>
mh	mh	k?
.	.	kIx.
<g/>
mk	mk	k?
.	.	kIx.
<g/>
ml	ml	kA
.	.	kIx.
<g/>
mm	mm	kA
.	.	kIx.
<g/>
mn	mn	k?
.	.	kIx.
<g/>
mo	mo	k?
.	.	kIx.
<g/>
mp	mp	k?
.	.	kIx.
<g/>
mq	mq	k?
.	.	kIx.
<g/>
mr	mr	k?
.	.	kIx.
<g/>
ms	ms	k?
.	.	kIx.
<g/>
mt	mt	k?
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
mu	on	k3xPp3gNnSc3
.	.	kIx.
<g/>
mv	mv	k?
.	.	kIx.
<g/>
mw	mw	k?
.	.	kIx.
<g/>
mx	mx	k?
.	.	kIx.
<g/>
my	my	k3xPp1nPc1
.	.	kIx.
<g/>
mz	mz	k?
.	.	kIx.
<g/>
na	na	k7c4
.	.	kIx.
<g/>
nc	nc	k?
.	.	kIx.
<g/>
ne	ne	k9
.	.	kIx.
<g/>
nf	nf	k?
.	.	kIx.
<g/>
ng	ng	k?
.	.	kIx.
<g/>
ni	on	k3xPp3gFnSc4
.	.	kIx.
<g/>
nl	nl	k?
.	.	kIx.
<g/>
no	no	k9
.	.	kIx.
<g/>
np	np	k?
.	.	kIx.
<g/>
nr	nr	k?
.	.	kIx.
<g/>
nu	nu	k9
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
nz	nz	k?
.	.	kIx.
<g/>
om	om	k?
.	.	kIx.
<g/>
pa	pa	k0
.	.	kIx.
<g/>
pe	pe	k?
.	.	kIx.
<g/>
pf	pf	kA
.	.	kIx.
<g/>
pg	pg	k?
.	.	kIx.
<g/>
ph	ph	kA
.	.	kIx.
<g/>
pk	pk	k?
.	.	kIx.
<g/>
pl	pl	k?
.	.	kIx.
<g/>
pm	pm	k?
.	.	kIx.
<g/>
pn	pn	k?
.	.	kIx.
<g/>
pr	pr	k0
.	.	kIx.
<g/>
ps	ps	k0
.	.	kIx.
<g/>
pt	pt	k?
.	.	kIx.
<g/>
pw	pw	k?
.	.	kIx.
<g/>
py	py	k?
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
qa	qa	k?
.	.	kIx.
<g/>
re	re	k9
.	.	kIx.
<g/>
ro	ro	k?
.	.	kIx.
<g/>
rs	rs	k?
.	.	kIx.
<g/>
ru	ru	k?
.	.	kIx.
<g/>
rw	rw	k?
.	.	kIx.
<g/>
sa	sa	k?
.	.	kIx.
<g/>
sb	sb	kA
.	.	kIx.
<g/>
sc	sc	k?
.	.	kIx.
<g/>
sd	sd	k?
.	.	kIx.
<g/>
se	s	k7c7
.	.	kIx.
<g/>
sg	sg	k?
.	.	kIx.
<g/>
sh	sh	k?
.	.	kIx.
<g/>
si	se	k3xPyFc3
.	.	kIx.
<g/>
sk	sk	k?
.	.	kIx.
<g/>
sl	sl	k?
.	.	kIx.
<g/>
sm	sm	k?
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
sn	sn	k?
.	.	kIx.
<g/>
sr	sr	k?
.	.	kIx.
<g/>
st	st	kA
.	.	kIx.
<g/>
su	su	k?
.	.	kIx.
<g/>
sv	sv	kA
.	.	kIx.
<g/>
sx	sx	k?
.	.	kIx.
<g/>
sy	sy	k?
.	.	kIx.
<g/>
sz	sz	k?
.	.	kIx.
<g/>
tc	tc	k0
.	.	kIx.
<g/>
td	td	k?
.	.	kIx.
<g/>
tf	tf	k0wR
.	.	kIx.
<g/>
tg	tg	kA
.	.	kIx.
<g/>
th	th	k?
.	.	kIx.
<g/>
tj	tj	kA
.	.	kIx.
<g/>
tk	tk	k?
.	.	kIx.
<g/>
tl	tl	k?
.	.	kIx.
<g/>
tm	tm	k?
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
tn	tn	k?
.	.	kIx.
<g/>
to	ten	k3xDgNnSc4
.	.	kIx.
<g/>
tr	tr	k?
.	.	kIx.
<g/>
tt	tt	k?
.	.	kIx.
<g/>
tv	tv	k?
.	.	kIx.
<g/>
tw	tw	k?
.	.	kIx.
<g/>
tz	tz	k?
.	.	kIx.
<g/>
ua	ua	k?
.	.	kIx.
<g/>
ug	ug	k?
.	.	kIx.
<g/>
uk	uk	k?
.	.	kIx.
<g/>
us	us	k?
.	.	kIx.
<g/>
uy	uy	k?
.	.	kIx.
<g/>
uz	uz	k?
.	.	kIx.
<g/>
va	va	k0wR
.	.	kIx.
<g/>
vc	vc	k?
.	.	kIx.
<g/>
ve	v	k7c6
.	.	kIx.
<g/>
vg	vg	k?
.	.	kIx.
<g/>
vi	vi	k?
.	.	kIx.
<g/>
vn	vn	k?
.	.	kIx.
<g/>
vu	vu	k?
.	.	kIx.
<g/>
wf	wf	k?
.	.	kIx.
<g/>
ws	ws	k?
.	.	kIx.
<g/>
ye	ye	k?
.	.	kIx.
<g/>
za	za	k7c7
.	.	kIx.
<g/>
zm	zm	k?
.	.	kIx.
<g/>
zw	zw	k?
Užívané	užívaný	k2eAgFnSc2d1
IDN	IDN	kA
</s>
<s>
ا	ا	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
al-Jazā	al-Jazā	k?
<g/>
’	’	k?
<g/>
ir	ir	k?
<g/>
,	,	kIx,
Alžírsko	Alžírsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
中	中	k?
(	(	kIx(
<g/>
Zhō	Zhō	k1gMnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
)	)	kIx)
<g/>
م	م	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
masr	masr	k1gInSc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
香	香	k?
(	(	kIx(
<g/>
Hong	Hong	k1gInSc1
Kong	Kongo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
bharat	bharat	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
(	(	kIx(
<g/>
.	.	kIx.
<g/>
भ	भ	k?
<g/>
ा	ा	k?
<g/>
र	र	k?
<g/>
..	..	k?
<g/>
భ	భ	k?
<g/>
ా	ా	k?
<g/>
ర	ర	k?
<g/>
్	్	k?
<g/>
.	.	kIx.
<g/>
ભ	ભ	k?
<g/>
ા	ા	k?
<g/>
ર	ર	k?
<g/>
.	.	kIx.
<g/>
ਭ	ਭ	k?
<g/>
ਾ	ਾ	k?
<g/>
ਰ	ਰ	k?
<g/>
.	.	kIx.
<g/>
ভ	ভ	k?
<g/>
া	া	k?
<g/>
র	র	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
இ	இ	k?
<g/>
்	்	k?
<g/>
த	த	k?
<g/>
ி	ி	k?
<g/>
ய	ய	k?
<g/>
ா	ா	k?
(	(	kIx(
<g/>
inthiyaa	inthiyaa	k1gMnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
ا	ا	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Írán	Írán	k1gInSc1
<g/>
)	)	kIx)
<g/>
ا	ا	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
al-urdun	al-urdun	k1gNnSc1
<g/>
,	,	kIx,
Jordánsko	Jordánsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
ف	ف	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
filastin	filastin	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Palestina	Palestina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
ق	ق	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
qatar	qatar	k1gMnSc1
<g/>
,	,	kIx,
Katar	katar	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
р	р	k?
(	(	kIx(
<g/>
RF	RF	kA
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
ا	ا	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
al-saudiah	al-saudiah	k1gInSc1
<g/>
,	,	kIx,
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
с	с	k?
(	(	kIx(
<g/>
srb	srb	k?
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
新	新	k?
(	(	kIx(
<g/>
Xī	Xī	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Sin-ka-po	Sin-ka-pa	k1gFnSc5
<g/>
,	,	kIx,
Singapur	Singapur	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
ச	ச	k?
<g/>
ி	ி	k?
<g/>
ங	ங	k?
<g/>
்	்	k?
<g/>
க	க	k?
<g/>
்	்	k?
<g/>
ப	ப	k?
<g/>
ூ	ூ	k?
<g/>
ர	ர	k?
<g/>
்	்	k?
(	(	kIx(
<g/>
cinkappū	cinkappū	k1gInSc1
<g/>
,	,	kIx,
Singapur	Singapur	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
한	한	k?
(	(	kIx(
<g/>
hangū	hangū	k1gMnSc1
<g/>
̚	̚	k?
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
ල	ල	k?
<g/>
ං	ං	k?
<g/>
ක	ක	k?
<g/>
ා	ා	k?
(	(	kIx(
<g/>
lanka	lanko	k1gNnSc2
<g/>
,	,	kIx,
Srí	Srí	k1gFnSc1
Lanka	lanko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
இ	இ	k?
<g/>
்	்	k?
<g/>
க	க	k?
<g/>
ை	ை	k?
(	(	kIx(
<g/>
ilangai	ilangai	k1gNnSc1
<g/>
,	,	kIx,
Srí	Srí	k1gFnSc1
Lanka	lanko	k1gNnSc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
س	س	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
sū	sū	k?
<g/>
,	,	kIx,
Sýrie	Sýrie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
ไ	ไ	k?
(	(	kIx(
<g/>
thai	thai	k1gNnSc1
<g/>
,	,	kIx,
Thajsko	Thajsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
台	台	k?
(	(	kIx(
<g/>
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
)	)	kIx)
<g/>
ت	ت	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
tunis	tunis	k1gInSc1
<g/>
,	,	kIx,
Tunisko	Tunisko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
у	у	k?
(	(	kIx(
<g/>
ukr	ukr	k?
<g/>
,	,	kIx,
Ukrajina	Ukrajina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
ا	ا	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
emarat	emarat	k1gMnSc1
<g/>
,	,	kIx,
SAE	SAE	kA
<g/>
)	)	kIx)
Vyhrazené	vyhrazený	k2eAgFnSc2d1
<g/>
/	/	kIx~
<g/>
nepřidělené	přidělený	k2eNgFnSc2d1
</s>
<s>
<g/>
bl	bl	k?
.	.	kIx.
<g/>
bq	bq	k?
.	.	kIx.
<g/>
eh	eh	k0
.	.	kIx.
<g/>
mf	mf	k?
.	.	kIx.
<g/>
ss	ss	k?
Přidělené	přidělený	k2eAgFnPc4d1
<g/>
/	/	kIx~
<g/>
nepoužívané	používaný	k2eNgFnPc4d1
</s>
<s>
<g/>
bv	bv	k?
.	.	kIx.
<g/>
gb	gb	k?
.	.	kIx.
<g/>
sj	sj	k?
.	.	kIx.
<g/>
yt	yt	k?
Navržené	navržený	k2eAgFnSc3d1
IDN	IDN	kA
</s>
<s>
<g/>
б	б	k?
י	י	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozastavené	pozastavený	k2eAgFnPc4d1
</s>
<s>
<g/>
an	an	k?
.	.	kIx.
<g/>
su	su	k?
.	.	kIx.
<g/>
tp	tp	k?
Zrušené	zrušený	k2eAgFnPc4d1
<g/>
/	/	kIx~
<g/>
bývalé	bývalý	k2eAgFnPc4d1
</s>
<s>
<g/>
bu	bu	kA
.	.	kIx.
<g/>
cs	cs	k?
.	.	kIx.
<g/>
dd	dd	k?
.	.	kIx.
<g/>
um	um	k1gInSc4
.	.	kIx.
<g/>
yu	yu	k?
.	.	kIx.
<g/>
zr	zr	k?
Související	související	k2eAgFnPc4d1
<g/>
:	:	kIx,
Generické	generický	k2eAgFnPc4d1
domény	doména	k1gFnPc4
nejvyššího	vysoký	k2eAgInSc2d3
řádu	řád	k1gInSc2
</s>
