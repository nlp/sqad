<s>
Sportovní	sportovní	k2eAgFnSc1d1
medicína	medicína	k1gFnSc1
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
medicína	medicína	k1gFnSc1
</s>
<s>
Díl	díl	k1gInSc1
seriálu	seriál	k1gInSc2
Dr	dr	kA
<g/>
.	.	kIx.
House	house	k1gNnSc1
Pův	Pův	k1gFnSc1
<g/>
.	.	kIx.
název	název	k1gInSc1
</s>
<s>
Sports	Sportsit	k5eAaPmRp2nS
Medicine	Medicin	k1gMnSc5
Číslo	číslo	k1gNnSc4
</s>
<s>
řada	řada	k1gFnSc1
1	#num#	k4
<g/>
díl	díl	k1gInSc1
12	#num#	k4
Premiéra	premiér	k1gMnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
Tvorba	tvorba	k1gFnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Keith	Keith	k1gMnSc1
Gordon	Gordon	k1gMnSc1
Autor	autor	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Mankiewicz	Mankiewicz	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Shore	Shor	k1gInSc5
Prod	Prod	k1gInSc4
<g/>
.	.	kIx.
kód	kód	k1gInSc4
</s>
<s>
HOU-112	HOU-112	k4
Obsah	obsah	k1gInSc1
Konečná	konečný	k2eAgFnSc1d1
diagnóza	diagnóza	k1gFnSc1
</s>
<s>
otrava	otrava	k1gFnSc1
kadmiem	kadmium	k1gNnSc7
Seznam	seznam	k1gInSc4
dílů	díl	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
řady	řada	k1gFnSc2
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1
díl	díl	k1gInSc1
</s>
<s>
Otázka	otázka	k1gFnSc1
rodičovství	rodičovství	k1gNnSc2
</s>
<s>
Occamova	Occamův	k2eAgFnSc1d1
břitva	břitva	k1gFnSc1
</s>
<s>
Mateřství	mateřství	k1gNnSc1
</s>
<s>
Jeptiška	jeptiška	k1gFnSc1
</s>
<s>
Sokratovská	sokratovský	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
</s>
<s>
Věrnost	věrnost	k1gFnSc1
</s>
<s>
Jed	jed	k1gInSc1
</s>
<s>
Nechat	nechat	k5eAaPmF
zemřít	zemřít	k5eAaPmF
</s>
<s>
Anamnéza	anamnéza	k1gFnSc1
</s>
<s>
Absťák	Absťák	k1gMnSc1
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
medicína	medicína	k1gFnSc1
</s>
<s>
Prokletý	prokletý	k2eAgInSc1d1
</s>
<s>
Sebeovládání	sebeovládání	k1gNnSc1
</s>
<s>
Zákon	zákon	k1gInSc1
mafie	mafie	k1gFnSc2
</s>
<s>
Zátěž	zátěž	k1gFnSc1
</s>
<s>
Nečekaná	čekaný	k2eNgFnSc1d1
pravda	pravda	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
</s>
<s>
Děti	dítě	k1gFnPc1
</s>
<s>
Láska	láska	k1gFnSc1
bolí	bolet	k5eAaImIp3nS
</s>
<s>
Tři	tři	k4xCgInPc4
příběhy	příběh	k1gInPc4
</s>
<s>
Líbánky	líbánky	k1gFnPc1
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc2
Dr	dr	kA
<g/>
.	.	kIx.
HouseNěkterá	HouseNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
medicína	medicína	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
originále	originál	k1gInSc6
Sports	sports	k2eAgFnSc1d1
Medicine	medicine	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dvanáctá	dvanáctý	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
z	z	k7c2
první	první	k4xOgFnSc2
řady	řada	k1gFnSc2
seriálu	seriál	k1gInSc2
Dr	dr	kA
<g/>
.	.	kIx.
House	house	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
baseballu	baseball	k1gInSc2
<g/>
,	,	kIx,
Hank	Hank	k1gMnSc1
Wiggen	Wiggen	k1gInSc1
<g/>
,	,	kIx,
natáčí	natáčet	k5eAaImIp3nS
protidrogový	protidrogový	k2eAgInSc4d1
klip	klip	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nadhozu	nadhoz	k1gInSc6
mu	on	k3xPp3gMnSc3
ale	ale	k8xC
praskne	prasknout	k5eAaPmIp3nS
v	v	k7c6
ruce	ruka	k1gFnSc6
kost	kost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
House	house	k1gNnSc1
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
užíval	užívat	k5eAaImAgInS
steroidy	steroid	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
by	by	kYmCp3nS
vysvětlovala	vysvětlovat	k5eAaImAgFnS
většinu	většina	k1gFnSc4
příznaků	příznak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začnou	začít	k5eAaPmIp3nP
ho	on	k3xPp3gMnSc4
tedy	tedy	k9
léčit	léčit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začne	začít	k5eAaPmIp3nS
však	však	k9
mít	mít	k5eAaImF
dýchací	dýchací	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
Hank	Hank	k1gMnSc1
přiznává	přiznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
před	před	k7c7
pěti	pět	k4xCc7
lety	léto	k1gNnPc7
steroidy	steroid	k1gInPc4
bral	brát	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
má	mít	k5eAaImIp3nS
poškozené	poškozený	k2eAgFnPc4d1
ledviny	ledvina	k1gFnPc4
<g/>
,	,	kIx,
potřebuje	potřebovat	k5eAaImIp3nS
transplantaci	transplantace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cuddyová	Cuddyová	k1gFnSc1
ho	on	k3xPp3gMnSc4
ale	ale	k9
odmítne	odmítnout	k5eAaPmIp3nS
zařadit	zařadit	k5eAaPmF
do	do	k7c2
pořadníku	pořadník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
přítelkyně	přítelkyně	k1gFnSc2
se	se	k3xPyFc4
tedy	tedy	k9
rozhodne	rozhodnout	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
věnovat	věnovat	k5eAaPmF,k5eAaImF
svou	svůj	k3xOyFgFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetření	vyšetření	k1gNnPc4
proběhne	proběhnout	k5eAaPmIp3nS
úspěšně	úspěšně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
dárcem	dárce	k1gMnSc7
být	být	k5eAaImF
nemůže	moct	k5eNaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
těhotná	těhotný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Hanka	Hanka	k1gFnSc1
nastávají	nastávat	k5eAaImIp3nP
zdravotní	zdravotní	k2eAgFnPc4d1
komplikace	komplikace	k1gFnPc4
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
srdcem	srdce	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
bije	bít	k5eAaImIp3nS
moc	moc	k6eAd1
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
pak	pak	k6eAd1
moc	moc	k6eAd1
pomalu	pomalu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začne	začít	k5eAaPmIp3nS
mít	mít	k5eAaImF
halucinace	halucinace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
House	house	k1gNnSc1
dedukuje	dedukovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
takové	takový	k3xDgInPc4
problémy	problém	k1gInPc4
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
digitalis	digitalis	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
digitalis	digitalis	k1gFnSc1
užívá	užívat	k5eAaImIp3nS
na	na	k7c4
své	svůj	k3xOyFgNnSc4
nemocné	nemocný	k2eAgNnSc4d1,k2eNgNnSc4d1
srdce	srdce	k1gNnSc4
Hankův	Hankův	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
a	a	k8xC
že	že	k8xS
Hank	Hank	k1gMnSc1
mu	on	k3xPp3gMnSc3
prášky	prášek	k1gMnPc4
ukradl	ukradnout	k5eAaPmAgMnS
a	a	k8xC
pokusil	pokusit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
Housovým	Housův	k2eAgInSc7d1
předpokladem	předpoklad	k1gInSc7
je	být	k5eAaImIp3nS
otrava	otrava	k1gFnSc1
kadmiem	kadmium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hank	Hank	k1gMnSc1
se	se	k3xPyFc4
přiznává	přiznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kouří	kouřit	k5eAaImIp3nS
marihuanu	marihuana	k1gFnSc4
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
marihuana	marihuana	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyrůstala	vyrůstat	k5eAaImAgFnS
v	v	k7c6
kontaminované	kontaminovaný	k2eAgFnSc6d1
zemině	zemina	k1gFnSc6
byla	být	k5eAaImAgFnS
příčinou	příčina	k1gFnSc7
všeho	všecek	k3xTgNnSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Diagnózy	diagnóza	k1gFnPc1
</s>
<s>
špatné	špatný	k2eAgFnPc1d1
diagnózy	diagnóza	k1gFnPc1
<g/>
:	:	kIx,
osteopenie	osteopenie	k1gFnPc1
<g/>
,	,	kIx,
braní	braní	k1gNnSc1
steroidů	steroid	k1gInPc2
<g/>
,	,	kIx,
Addisonova	Addisonův	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
</s>
<s>
správná	správný	k2eAgFnSc1d1
diagnóza	diagnóza	k1gFnSc1
<g/>
:	:	kIx,
chronická	chronický	k2eAgFnSc1d1
otrava	otrava	k1gFnSc1
kadmiem	kadmium	k1gNnSc7
z	z	k7c2
kontaminované	kontaminovaný	k2eAgFnSc2d1
marihuany	marihuana	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kadmium	kadmium	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
DrHouse	DrHouse	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Sportovní	sportovní	k2eAgFnSc1d1
medicína	medicína	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Dr	dr	kA
<g/>
.	.	kIx.
House	house	k1gNnSc1
Epizody	epizoda	k1gFnSc2
•	•	k?
Ocenění	ocenění	k1gNnSc4
•	•	k?
Soundtrack	soundtrack	k1gInSc1
•	•	k?
Citáty	citát	k1gInPc1
•	•	k?
Kategorie	kategorie	k1gFnSc2
Hlavní	hlavní	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
</s>
<s>
Gregory	Gregor	k1gMnPc7
House	house	k1gNnSc1
•	•	k?
Lisa	Lisa	k1gFnSc1
Cuddyová	Cuddyová	k1gFnSc1
•	•	k?
James	James	k1gMnSc1
Wilson	Wilson	k1gMnSc1
•	•	k?
Eric	Eric	k1gInSc1
Foreman	Foreman	k1gMnSc1
•	•	k?
Allison	Allison	k1gInSc1
Cameronová	Cameronová	k1gFnSc1
•	•	k?
Robert	Robert	k1gMnSc1
Chase	chasa	k1gFnSc3
•	•	k?
Chris	Chris	k1gInSc1
Taub	Taub	k1gInSc1
•	•	k?
Lawrence	Lawrenec	k1gInSc2
Kutner	Kutnra	k1gFnPc2
•	•	k?
Remy	remy	k1gNnSc4
„	„	k?
<g/>
Třináctka	třináctka	k1gFnSc1
<g/>
“	“	k?
Hadley	Hadlea	k1gFnSc2
•	•	k?
Jessica	Jessic	k2eAgFnSc1d1
Adamsová	Adamsová	k1gFnSc1
•	•	k?
Chi	chi	k0
Parková	parkový	k2eAgFnSc1d1
Vedlejší	vedlejší	k2eAgFnPc4d1
postavy	postava	k1gFnPc4
</s>
<s>
Edward	Edward	k1gMnSc1
Vogler	Vogler	k1gMnSc1
•	•	k?
Stacy	Staca	k1gFnSc2
Warner	Warner	k1gMnSc1
•	•	k?
Mark	Mark	k1gMnSc1
Warner	Warner	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Tritter	Tritter	k1gMnSc1
•	•	k?
Amber	ambra	k1gFnPc2
Volakis	Volakis	k1gFnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Douglas	Douglas	k1gMnSc1
Tvůrci	tvůrce	k1gMnPc1
</s>
<s>
Paul	Paul	k1gMnSc1
Attanasio	Attanasio	k1gMnSc1
•	•	k?
Katie	Katie	k1gFnSc2
Jacobs	Jacobsa	k1gFnPc2
•	•	k?
David	David	k1gMnSc1
Shore	Shor	k1gInSc5
•	•	k?
Bryan	Bryan	k1gInSc1
Singer	Singra	k1gFnPc2
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Hugh	Hugh	k1gMnSc1
Laurie	Laurie	k1gFnSc2
•	•	k?
Lisa	Lisa	k1gFnSc1
Edelstein	Edelstein	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
Sean	Sean	k1gMnSc1
Leonard	Leonard	k1gMnSc1
•	•	k?
Omar	Omar	k1gInSc1
Epps	Epps	k1gInSc1
•	•	k?
Jennifer	Jennifer	k1gInSc1
Morrisonová	Morrisonová	k1gFnSc1
•	•	k?
Jesse	Jesse	k1gFnSc2
Spencer	Spencer	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Jacobson	Jacobson	k1gMnSc1
•	•	k?
Kal	kalo	k1gNnPc2
Penn	Penno	k1gNnPc2
•	•	k?
Olivia	Olivius	k1gMnSc2
Wilde	Wild	k1gInSc5
Řady	řada	k1gFnPc1
</s>
<s>
1	#num#	k4
•	•	k?
2	#num#	k4
•	•	k?
3	#num#	k4
•	•	k?
4	#num#	k4
•	•	k?
5	#num#	k4
•	•	k?
6	#num#	k4
•	•	k?
7	#num#	k4
•	•	k?
8	#num#	k4
</s>
