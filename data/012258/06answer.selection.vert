<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
kaštanově	kaštanově	k6eAd1	kaštanově
hnědý	hnědý	k2eAgInSc1d1	hnědý
s	s	k7c7	s
tmavějším	tmavý	k2eAgInSc7d2	tmavší
hřbetem	hřbet	k1gInSc7	hřbet
a	a	k8xC	a
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
bílými	bílý	k2eAgInPc7d1	bílý
okraji	okraj	k1gInPc7	okraj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
břiše	břich	k1gInSc6	břich
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
bílou	bílý	k2eAgFnSc4d1	bílá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
snadno	snadno	k6eAd1	snadno
rozeznat	rozeznat	k5eAaPmF	rozeznat
od	od	k7c2	od
podobných	podobný	k2eAgFnPc2d1	podobná
samic	samice	k1gFnPc2	samice
poláka	polák	k1gMnSc2	polák
chocholačky	chocholačka	k1gFnSc2	chocholačka
<g/>
.	.	kIx.	.
</s>
