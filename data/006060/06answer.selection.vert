<s>
Kvarky	kvark	k1gInPc1	kvark
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
hadrony	hadron	k1gInPc1	hadron
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
například	například	k6eAd1	například
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
