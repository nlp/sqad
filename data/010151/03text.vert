<p>
<s>
Dětství	dětství	k1gNnSc1	dětství
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
etapu	etapa	k1gFnSc4	etapa
lidského	lidský	k2eAgMnSc2d1	lidský
jedince	jedinec	k1gMnSc2	jedinec
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
po	po	k7c4	po
dospělost	dospělost	k1gFnSc4	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
období	období	k1gNnSc4	období
novorozenecké	novorozenecký	k2eAgFnSc2d1	novorozenecká
<g/>
,	,	kIx,	,
kojenecké	kojenecký	k2eAgFnSc2d1	kojenecká
<g/>
,	,	kIx,	,
batolecí	batolecí	k2eAgMnSc1d1	batolecí
<g/>
,	,	kIx,	,
předškolní	předškolní	k2eAgInSc1d1	předškolní
věk	věk	k1gInSc1	věk
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgInSc1d2	mladší
školní	školní	k2eAgInSc4d1	školní
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
starší	starý	k2eAgInSc4d2	starší
školní	školní	k2eAgInSc4d1	školní
věk	věk	k1gInSc4	věk
-	-	kIx~	-
pubescence	pubescence	k1gFnPc4	pubescence
a	a	k8xC	a
období	období	k1gNnPc4	období
dorostové	dorostový	k2eAgFnPc4d1	dorostová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
dětství	dětství	k1gNnSc2	dětství
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
daný	daný	k2eAgInSc1d1	daný
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
různý	různý	k2eAgInSc1d1	různý
rozsah	rozsah	k1gInSc1	rozsah
let	léto	k1gNnPc2	léto
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
legální	legální	k2eAgInSc1d1	legální
věk	věk	k1gInSc1	věk
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
zletilost	zletilost	k1gFnSc4	zletilost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dětství	dětství	k1gNnSc2	dětství
oficiálně	oficiálně	k6eAd1	oficiálně
končí	končit	k5eAaImIp3nS	končit
a	a	k8xC	a
osoba	osoba	k1gFnSc1	osoba
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
právně	právně	k6eAd1	právně
dospělá	dospělý	k2eAgFnSc1d1	dospělá
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
věk	věk	k1gInSc1	věk
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
13	[number]	k4	13
až	až	k6eAd1	až
21	[number]	k4	21
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
18	[number]	k4	18
<g/>
.	.	kIx.	.
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývojové	vývojový	k2eAgFnPc4d1	vývojová
fáze	fáze	k1gFnPc4	fáze
dětství	dětství	k1gNnSc2	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
novorozenecké	novorozenecký	k2eAgFnSc2d1	novorozenecká
===	===	k?	===
</s>
</p>
<p>
<s>
Novorozenecké	novorozenecký	k2eAgNnSc4d1	novorozenecké
období	období	k1gNnSc4	období
první	první	k4xOgInSc1	první
měsíc	měsíc	k1gInSc1	měsíc
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
fyziologickou	fyziologický	k2eAgFnSc7d1	fyziologická
charakteristikou	charakteristika	k1gFnSc7	charakteristika
novorozeneckého	novorozenecký	k2eAgNnSc2d1	novorozenecké
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
adaptace	adaptace	k1gFnSc1	adaptace
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
mimo	mimo	k7c4	mimo
dělohu	děloha	k1gFnSc4	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
ještě	ještě	k9	ještě
plně	plně	k6eAd1	plně
nevnímá	vnímat	k5eNaImIp3nS	vnímat
okolní	okolní	k2eAgInSc4d1	okolní
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
důležitými	důležitý	k2eAgInPc7d1	důležitý
reflexy	reflex	k1gInPc7	reflex
<g/>
:	:	kIx,	:
sací	sací	k2eAgFnPc4d1	sací
(	(	kIx(	(
<g/>
přisaje	přisát	k5eAaPmIp3nS	přisát
se	se	k3xPyFc4	se
na	na	k7c4	na
bradavku	bradavka	k1gFnSc4	bradavka
a	a	k8xC	a
saje	sát	k5eAaImIp3nS	sát
mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obranný	obranný	k2eAgInSc1d1	obranný
(	(	kIx(	(
<g/>
pláč	pláč	k1gInSc1	pláč
<g/>
)	)	kIx)	)
-	-	kIx~	-
kterým	který	k3yRgMnPc3	který
dítě	dítě	k1gNnSc4	dítě
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
pocity	pocit	k1gInPc4	pocit
nelibosti	nelibost	k1gFnSc2	nelibost
a	a	k8xC	a
hladu	hlad	k1gInSc2	hlad
a	a	k8xC	a
úchopový	úchopový	k2eAgMnSc1d1	úchopový
<g/>
.	.	kIx.	.
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1	tělesná
proporcionalita	proporcionalita	k1gFnSc1	proporcionalita
<g/>
:	:	kIx,	:
velká	velký	k2eAgFnSc1d1	velká
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
trup	trup	k1gInSc1	trup
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgFnPc1d1	krátká
končetiny	končetina	k1gFnPc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
novorozence	novorozenec	k1gMnSc2	novorozenec
tvoří	tvořit	k5eAaImIp3nS	tvořit
páteř	páteř	k1gFnSc4	páteř
jediný	jediný	k2eAgInSc4d1	jediný
oblouk	oblouk	k1gInSc4	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
dítě	dítě	k1gNnSc4	dítě
většinu	většina	k1gFnSc4	většina
dne	den	k1gInSc2	den
prospí	prospat	k5eAaPmIp3nS	prospat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
kojenecké	kojenecký	k2eAgFnSc2d1	kojenecká
===	===	k?	===
</s>
</p>
<p>
<s>
Kojenecké	kojenecký	k2eAgNnSc1d1	kojenecké
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
výživou	výživa	k1gFnSc7	výživa
odkázáno	odkázat	k5eAaPmNgNnS	odkázat
především	především	k9	především
na	na	k7c4	na
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
tělesně	tělesně	k6eAd1	tělesně
i	i	k8xC	i
duševně	duševně	k6eAd1	duševně
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Dvojesovité	dvojesovitý	k2eAgNnSc1d1	dvojesovitý
prohnutí	prohnutí	k1gNnSc1	prohnutí
páteře	páteř	k1gFnSc2	páteř
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
postupně	postupně	k6eAd1	postupně
se	s	k7c7	s
vzpřimováním	vzpřimování	k1gNnSc7	vzpřimování
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
změnám	změna	k1gFnPc3	změna
patří	patřit	k5eAaImIp3nS	patřit
růst	růst	k1gInSc4	růst
dolních	dolní	k2eAgInPc2d1	dolní
a	a	k8xC	a
horních	horní	k2eAgInPc2d1	horní
řezáků	řezák	k1gInPc2	řezák
a	a	k8xC	a
rozlišování	rozlišování	k1gNnSc1	rozlišování
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
kg	kg	kA	kg
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
75	[number]	k4	75
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
kojence	kojenec	k1gMnSc2	kojenec
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
3	[number]	k4	3
–	–	k?	–
5	[number]	k4	5
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
ta	ten	k3xDgFnSc1	ten
základní	základní	k2eAgFnSc1d1	základní
jako	jako	k8xS	jako
například	například	k6eAd1	například
,,	,,	k?	,,
<g/>
máma	máma	k1gFnSc1	máma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
,,	,,	k?	,,
<g/>
táta	táta	k1gMnSc1	táta
<g/>
"	"	kIx"	"
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Prospí	prospat	k5eAaPmIp3nS	prospat
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
batolecí	batolecí	k2eAgNnSc1d1	batolecí
===	===	k?	===
</s>
</p>
<p>
<s>
Batolecí	Batolecí	k2eAgNnSc1d1	Batolecí
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
do	do	k7c2	do
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
pohybové	pohybový	k2eAgFnSc3d1	pohybová
schopnosti	schopnost	k1gFnSc3	schopnost
(	(	kIx(	(
<g/>
dítě	dítě	k1gNnSc1	dítě
leze	lézt	k5eAaImIp3nS	lézt
a	a	k8xC	a
chodí	chodit	k5eAaImIp3nS	chodit
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
prořezávají	prořezávat	k5eAaImIp3nP	prořezávat
mléčné	mléčný	k2eAgInPc1d1	mléčný
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc6	konec
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
až	až	k9	až
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
stě	sto	k4xCgFnPc1	sto
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
během	během	k7c2	během
třetího	třetí	k4xOgInSc2	třetí
roku	rok	k1gInSc2	rok
zná	znát	k5eAaImIp3nS	znát
dítě	dítě	k1gNnSc4	dítě
až	až	k8xS	až
tisíc	tisíc	k4xCgInSc4	tisíc
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
3	[number]	k4	3
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
koktavost	koktavost	k1gFnSc4	koktavost
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
učí	učit	k5eAaImIp3nS	učit
základním	základní	k2eAgInSc7d1	základní
hygienickým	hygienický	k2eAgInSc7d1	hygienický
a	a	k8xC	a
společenským	společenský	k2eAgInPc3d1	společenský
návykům	návyk	k1gInPc3	návyk
a	a	k8xC	a
osamostatňuje	osamostatňovat	k5eAaImIp3nS	osamostatňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
životních	životní	k2eAgFnPc2d1	životní
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
příjem	příjem	k1gInSc1	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
udržování	udržování	k1gNnSc1	udržování
čistoty	čistota	k1gFnSc2	čistota
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
dokonalejší	dokonalý	k2eAgFnSc1d2	dokonalejší
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
chůze	chůze	k1gFnSc1	chůze
a	a	k8xC	a
pohyby	pohyb	k1gInPc1	pohyb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
či	či	k8xC	či
otec	otec	k1gMnSc1	otec
o	o	k7c4	o
dítě	dítě	k1gNnSc4	dítě
starali	starat	k5eAaImAgMnP	starat
<g/>
,	,	kIx,	,
učili	učít	k5eAaPmAgMnP	učít
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
šikovnější	šikovný	k2eAgFnSc6d2	šikovnější
a	a	k8xC	a
zručnější	zručný	k2eAgFnSc6d2	zručnější
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
tříletého	tříletý	k2eAgNnSc2d1	tříleté
dítěte	dítě	k1gNnSc2	dítě
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předškolní	předškolní	k2eAgInSc1d1	předškolní
věk	věk	k1gInSc1	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Předškolní	předškolní	k2eAgInSc1d1	předškolní
věk	věk	k1gInSc1	věk
trvá	trvat	k5eAaImIp3nS	trvat
do	do	k7c2	do
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nP	měnit
se	se	k3xPyFc4	se
tělesné	tělesný	k2eAgInPc1d1	tělesný
rozměry	rozměr	k1gInPc1	rozměr
<g/>
,	,	kIx,	,
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
se	se	k3xPyFc4	se
duševní	duševní	k2eAgFnSc2d1	duševní
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
,	,	kIx,	,
dítě	dítě	k1gNnSc4	dítě
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
činnostech	činnost	k1gFnPc6	činnost
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
kolektivu	kolektiv	k1gInSc6	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
školka	školka	k1gFnSc1	školka
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	Dítě	k1gMnSc1	Dítě
si	se	k3xPyFc3	se
zvykne	zvyknout	k5eAaPmIp3nS	zvyknout
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
vztahy	vztah	k1gInPc7	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
činností	činnost	k1gFnSc7	činnost
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
předškolního	předškolní	k2eAgInSc2d1	předškolní
věku	věk	k1gInSc2	věk
spadá	spadat	k5eAaPmIp3nS	spadat
období	období	k1gNnSc3	období
prvního	první	k4xOgInSc2	první
vzdoru	vzdor	k1gInSc2	vzdor
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
výměna	výměna	k1gFnSc1	výměna
dětského	dětský	k2eAgInSc2d1	dětský
chrupu	chrup	k1gInSc2	chrup
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různá	různý	k2eAgFnSc1d1	různá
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
inviduální	inviduální	k2eAgFnSc1d1	inviduální
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
holčička	holčička	k1gFnSc1	holčička
či	či	k8xC	či
kluk	kluk	k1gMnSc1	kluk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mladší	mladý	k2eAgInSc4d2	mladší
školní	školní	k2eAgInSc4d1	školní
věk	věk	k1gInSc4	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Mladší	mladý	k2eAgInSc1d2	mladší
školní	školní	k2eAgInSc1d1	školní
věk	věk	k1gInSc1	věk
trvá	trvat	k5eAaImIp3nS	trvat
do	do	k7c2	do
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
mladšího	mladý	k2eAgInSc2d2	mladší
školního	školní	k2eAgInSc2d1	školní
věku	věk	k1gInSc2	věk
je	být	k5eAaImIp3nS	být
dítě	dítě	k1gNnSc4	dítě
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
vytáhlosti	vytáhlost	k1gFnSc2	vytáhlost
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
období	období	k1gNnSc4	období
pomalého	pomalý	k2eAgInSc2d1	pomalý
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Tělesné	tělesný	k2eAgInPc1d1	tělesný
tvary	tvar	k1gInPc1	tvar
se	se	k3xPyFc4	se
zaoblují	zaoblovat	k5eAaImIp3nP	zaoblovat
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
prořezávat	prořezávat	k5eAaImF	prořezávat
trvalé	trvalý	k2eAgInPc4d1	trvalý
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
začíná	začínat	k5eAaImIp3nS	začínat
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
společenským	společenský	k2eAgMnSc7d1	společenský
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
soustředit	soustředit	k5eAaPmF	soustředit
i	i	k9	i
na	na	k7c6	na
věci	věc	k1gFnSc6	věc
méně	málo	k6eAd2	málo
přitažlivé	přitažlivý	k2eAgNnSc1d1	přitažlivé
(	(	kIx(	(
<g/>
učení	učení	k1gNnSc1	učení
<g/>
)	)	kIx)	)
a	a	k8xC	a
podřídit	podřídit	k5eAaPmF	podřídit
se	se	k3xPyFc4	se
časovému	časový	k2eAgInSc3d1	časový
rozvrhu	rozvrh	k1gInSc3	rozvrh
<g/>
,	,	kIx,	,
zvyknout	zvyknout	k5eAaPmF	zvyknout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
nějaké	nějaký	k3yIgFnPc4	nějaký
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
plnit	plnit	k5eAaImF	plnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starší	starý	k2eAgInSc4d2	starší
školní	školní	k2eAgInSc4d1	školní
věk	věk	k1gInSc4	věk
===	===	k?	===
</s>
</p>
<p>
<s>
Starší	starý	k2eAgInSc1d2	starší
školní	školní	k2eAgInSc1d1	školní
věk	věk	k1gInSc1	věk
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
počítá	počítat	k5eAaImIp3nS	počítat
od	od	k7c2	od
12	[number]	k4	12
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
jedinec	jedinec	k1gMnSc1	jedinec
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgMnS	ovlivnit
probíhající	probíhající	k2eAgFnSc7d1	probíhající
pubertou	puberta	k1gFnSc7	puberta
<g/>
.	.	kIx.	.
</s>
<s>
Vstupují	vstupovat	k5eAaImIp3nP	vstupovat
v	v	k7c4	v
činnost	činnost	k1gFnSc4	činnost
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
se	se	k3xPyFc4	se
druhotné	druhotný	k2eAgInPc1d1	druhotný
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
se	se	k3xPyFc4	se
růst	růst	k1gInSc1	růst
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
puberta	puberta	k1gFnSc1	puberta
<g/>
.	.	kIx.	.
</s>
<s>
Pubertu	puberta	k1gFnSc4	puberta
spouští	spouštět	k5eAaImIp3nP	spouštět
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
hormony	hormon	k1gInPc1	hormon
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
stává	stávat	k5eAaImIp3nS	stávat
pomalu	pomalu	k6eAd1	pomalu
dospělým	dospělý	k2eAgMnSc7d1	dospělý
jak	jak	k8xS	jak
tělesně	tělesně	k6eAd1	tělesně
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
psychicky	psychicky	k6eAd1	psychicky
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychleji	rychle	k6eAd3	rychle
rostou	růst	k5eAaImIp3nP	růst
dívky	dívka	k1gFnPc1	dívka
v	v	k7c6	v
12	[number]	k4	12
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
chlapci	chlapec	k1gMnPc1	chlapec
v	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Dívkám	dívka	k1gFnPc3	dívka
začne	začít	k5eAaPmIp3nS	začít
růst	růst	k5eAaImF	růst
poprsí	poprsí	k1gNnSc4	poprsí
a	a	k8xC	a
první	první	k4xOgInPc4	první
chloupky	chloupek	k1gInPc4	chloupek
v	v	k7c6	v
pohlavní	pohlavní	k2eAgFnSc6d1	pohlavní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
také	také	k9	také
proces	proces	k1gInSc1	proces
menstruace	menstruace	k1gFnSc2	menstruace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dívka	dívka	k1gFnSc1	dívka
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
také	také	k9	také
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Chlapcům	chlapec	k1gMnPc3	chlapec
začnou	začít	k5eAaPmIp3nP	začít
také	také	k6eAd1	také
růst	růst	k5eAaImF	růst
chlupy	chlup	k1gInPc1	chlup
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
například	například	k6eAd1	například
v	v	k7c6	v
podpaždí	podpaždí	k1gNnSc6	podpaždí
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
sperma	sperma	k1gNnSc4	sperma
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
oplodnit	oplodnit	k5eAaPmF	oplodnit
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Psychické	psychický	k2eAgNnSc1d1	psychické
dospívání	dospívání	k1gNnSc1	dospívání
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zejména	zejména	k9	zejména
touhou	touha	k1gFnSc7	touha
po	po	k7c4	po
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
(	(	kIx(	(
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
fakticky	fakticky	k6eAd1	fakticky
končí	končit	k5eAaImIp3nS	končit
výchova	výchova	k1gFnSc1	výchova
v	v	k7c6	v
narušených	narušený	k2eAgFnPc6d1	narušená
či	či	k8xC	či
rozpadlých	rozpadlý	k2eAgFnPc6d1	rozpadlá
rodinách	rodina	k1gFnPc6	rodina
či	či	k8xC	či
u	u	k7c2	u
problémových	problémový	k2eAgFnPc2d1	problémová
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pěstounské	pěstounský	k2eAgFnPc4d1	pěstounská
rodiny	rodina	k1gFnPc4	rodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
po	po	k7c6	po
menarche	menarche	k1gFnSc6	menarche
se	se	k3xPyFc4	se
dívky	dívka	k1gFnPc1	dívka
vdávají	vdávat	k5eAaImIp3nP	vdávat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
ještě	ještě	k9	ještě
před	před	k7c4	před
sto	sto	k4xCgNnSc4	sto
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
odcházely	odcházet	k5eAaImAgFnP	odcházet
děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
rodin	rodina	k1gFnPc2	rodina
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
už	už	k6eAd1	už
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
nevrátily	vrátit	k5eNaPmAgInP	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
dorostové	dorostový	k2eAgNnSc1d1	dorostové
===	===	k?	===
</s>
</p>
<p>
<s>
Dorostové	dorostový	k2eAgNnSc1d1	dorostové
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
adolescence	adolescence	k1gFnSc1	adolescence
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
výrazně	výrazně	k6eAd1	výrazně
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
úplně	úplně	k6eAd1	úplně
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Adolescence	adolescence	k1gFnSc1	adolescence
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
most	most	k1gInSc4	most
mezi	mezi	k7c7	mezi
dětstvím	dětství	k1gNnSc7	dětství
a	a	k8xC	a
dospělostí	dospělost	k1gFnSc7	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
období	období	k1gNnSc4	období
kritické	kritický	k2eAgNnSc4d1	kritické
a	a	k8xC	a
rizikové	rizikový	k2eAgNnSc4d1	rizikové
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jedinec	jedinec	k1gMnSc1	jedinec
radikálně	radikálně	k6eAd1	radikálně
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
své	svůj	k3xOyFgFnSc6	svůj
biologické	biologický	k2eAgFnSc6d1	biologická
<g/>
,	,	kIx,	,
psychické	psychický	k2eAgFnSc6d1	psychická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc6d1	sociální
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
adolescenci	adolescence	k1gFnSc6	adolescence
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
již	již	k9	již
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
pohlavní	pohlavní	k2eAgMnPc1d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
poněkud	poněkud	k6eAd1	poněkud
hormonální	hormonální	k2eAgFnSc1d1	hormonální
hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ustaluje	ustalovat	k5eAaImIp3nS	ustalovat
na	na	k7c6	na
hodnotách	hodnota	k1gFnPc6	hodnota
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
pro	pro	k7c4	pro
dospělost	dospělost	k1gFnSc4	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
chlapců	chlapec	k1gMnPc2	chlapec
se	se	k3xPyFc4	se
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
vývoj	vývoj	k1gInSc1	vývoj
tělesného	tělesný	k2eAgNnSc2d1	tělesné
ochlupení	ochlupení	k1gNnSc2	ochlupení
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
mohutní	mohutnět	k5eAaImIp3nS	mohutnět
a	a	k8xC	a
sílí	sílet	k5eAaImIp3nS	sílet
svalstvo	svalstvo	k1gNnSc4	svalstvo
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tělesná	tělesný	k2eAgFnSc1d1	tělesná
výkonnost	výkonnost	k1gFnSc1	výkonnost
a	a	k8xC	a
zdatnost	zdatnost	k1gFnSc1	zdatnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
adolescenci	adolescence	k1gFnSc6	adolescence
jsou	být	k5eAaImIp3nP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
již	již	k9	již
všechny	všechen	k3xTgFnPc1	všechen
formy	forma	k1gFnPc1	forma
myšlení	myšlení	k1gNnSc1	myšlení
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
dospělost	dospělost	k1gFnSc4	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Adolescence	adolescence	k1gFnSc1	adolescence
je	být	k5eAaImIp3nS	být
období	období	k1gNnSc4	období
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgNnSc2	jenž
mládež	mládež	k1gFnSc1	mládež
hledá	hledat	k5eAaImIp3nS	hledat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
identitu	identita	k1gFnSc4	identita
<g/>
,	,	kIx,	,
formuje	formovat	k5eAaImIp3nS	formovat
svoji	svůj	k3xOyFgFnSc4	svůj
odlišnost	odlišnost	k1gFnSc4	odlišnost
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Moderní	moderní	k2eAgInPc1d1	moderní
koncepty	koncept	k1gInPc1	koncept
dětství	dětství	k1gNnSc2	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncept	koncept	k1gInSc1	koncept
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
a	a	k8xC	a
očekávání	očekávání	k1gNnSc1	očekávání
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
neměly	mít	k5eNaImAgFnP	mít
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
starat	starat	k5eAaImF	starat
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
by	by	kYmCp3nP	by
pracovat	pracovat	k5eAaImF	pracovat
<g/>
;	;	kIx,	;
život	život	k1gInSc1	život
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
a	a	k8xC	a
bezproblémový	bezproblémový	k2eAgMnSc1d1	bezproblémový
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc1	dětství
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
směsice	směsice	k1gFnSc1	směsice
radosti	radost	k1gFnSc2	radost
<g/>
,	,	kIx,	,
údivů	údiv	k1gInPc2	údiv
a	a	k8xC	a
pružnosti	pružnost	k1gFnSc2	pružnost
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
čas	čas	k1gInSc1	čas
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
socializace	socializace	k1gFnSc2	socializace
a	a	k8xC	a
prozkoumávání	prozkoumávání	k1gNnSc2	prozkoumávání
světa	svět	k1gInSc2	svět
bez	bez	k7c2	bez
mnoha	mnoho	k4c2	mnoho
zásahů	zásah	k1gInPc2	zásah
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
doba	doba	k1gFnSc1	doba
učení	učení	k1gNnSc2	učení
se	se	k3xPyFc4	se
zodpovědnosit	zodpovědnosit	k5eAaPmF	zodpovědnosit
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
dítě	dítě	k1gNnSc1	dítě
starat	starat	k5eAaImF	starat
o	o	k7c6	o
zodpovědnosti	zodpovědnost	k1gFnSc6	zodpovědnost
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dětství	dětství	k1gNnSc1	dětství
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
retrospektivně	retrospektivně	k6eAd1	retrospektivně
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
čas	čas	k1gInSc1	čas
nevinnosti	nevinnost	k1gFnSc2	nevinnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
bráno	brát	k5eAaImNgNnS	brát
jako	jako	k8xC	jako
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
naznačující	naznačující	k2eAgInSc1d1	naznačující
optimistický	optimistický	k2eAgInSc4d1	optimistický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
konceptem	koncept	k1gInSc7	koncept
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
ztráta	ztráta	k1gFnSc1	ztráta
nevinnosti	nevinnost	k1gFnSc2	nevinnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nahlížena	nahlížen	k2eAgFnSc1d1	nahlížena
jako	jako	k8xS	jako
integrální	integrální	k2eAgFnSc1d1	integrální
část	část	k1gFnSc1	část
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
brán	brát	k5eAaImNgInS	brát
jako	jako	k8xC	jako
zkušenost	zkušenost	k1gFnSc1	zkušenost
nebo	nebo	k8xC	nebo
období	období	k1gNnSc1	období
v	v	k7c6	v
životě	život	k1gInSc6	život
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
dítě	dítě	k1gNnSc1	dítě
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnSc2	bolest
nebo	nebo	k8xC	nebo
světa	svět	k1gInSc2	svět
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
téma	téma	k1gNnSc1	téma
je	být	k5eAaImIp3nS	být
rozebíráno	rozebírat	k5eAaImNgNnS	rozebírat
například	například	k6eAd1	například
v	v	k7c6	v
románu	román	k1gInSc6	román
Pán	pán	k1gMnSc1	pán
much	moucha	k1gFnPc2	moucha
<g/>
.	.	kIx.	.
</s>
<s>
Fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
Petra	Petr	k1gMnSc2	Petr
Pana	Pan	k1gMnSc2	Pan
byla	být	k5eAaImAgFnS	být
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nekončí	končit	k5eNaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětská	dětský	k2eAgFnSc1d1	dětská
hra	hra	k1gFnSc1	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
poznávací	poznávací	k2eAgInSc4d1	poznávací
<g/>
,	,	kIx,	,
fyzický	fyzický	k2eAgInSc4d1	fyzický
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgInSc4d1	sociální
a	a	k8xC	a
emoční	emoční	k2eAgInSc4d1	emoční
vývoj	vývoj	k1gInSc4	vývoj
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nP	nabízet
dětem	dítě	k1gFnPc3	dítě
možnosti	možnost	k1gFnSc2	možnost
pro	pro	k7c4	pro
fyzický	fyzický	k2eAgInSc4d1	fyzický
(	(	kIx(	(
<g/>
běh	běh	k1gInSc4	běh
<g/>
,	,	kIx,	,
skákání	skákání	k1gNnSc1	skákání
<g/>
,	,	kIx,	,
lezení	lezení	k1gNnSc1	lezení
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
intelektuální	intelektuální	k2eAgMnSc1d1	intelektuální
(	(	kIx(	(
<g/>
sociální	sociální	k2eAgFnPc1d1	sociální
dovednosti	dovednost	k1gFnPc1	dovednost
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgFnPc1d1	společenská
normy	norma	k1gFnPc1	norma
<g/>
,	,	kIx,	,
etika	etika	k1gFnSc1	etika
a	a	k8xC	a
obecné	obecný	k2eAgFnPc1d1	obecná
vědomosti	vědomost	k1gFnPc1	vědomost
<g/>
)	)	kIx)	)
a	a	k8xC	a
emoční	emoční	k2eAgInSc1d1	emoční
rozvoj	rozvoj	k1gInSc1	rozvoj
(	(	kIx(	(
<g/>
empatie	empatie	k1gFnSc1	empatie
<g/>
,	,	kIx,	,
soucit	soucit	k1gInSc1	soucit
a	a	k8xC	a
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nestrukturovaná	strukturovaný	k2eNgFnSc1d1	nestrukturovaná
hra	hra	k1gFnSc1	hra
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
tvořivost	tvořivost	k1gFnSc4	tvořivost
a	a	k8xC	a
představivost	představivost	k1gFnSc4	představivost
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hranit	k5eAaImIp3nS	hranit
si	se	k3xPyFc3	se
a	a	k8xC	a
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
s	s	k7c7	s
dospělými	dospělí	k1gMnPc7	dospělí
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
příležitosti	příležitost	k1gFnPc4	příležitost
pro	pro	k7c4	pro
přátelství	přátelství	k1gNnSc4	přátelství
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnPc4d1	sociální
interakce	interakce	k1gFnPc4	interakce
<g/>
,	,	kIx,	,
konflikty	konflikt	k1gInPc4	konflikt
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
řešení	řešení	k1gNnPc4	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
skrz	skrz	k7c4	skrz
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
a	a	k8xC	a
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
svět	svět	k1gInSc4	svět
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Neřízená	řízený	k2eNgFnSc1d1	neřízená
hra	hra	k1gFnSc1	hra
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
dětem	dítě	k1gFnPc3	dítě
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
jak	jak	k8xC	jak
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
dělit	dělit	k5eAaImF	dělit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
,	,	kIx,	,
řešit	řešit	k5eAaImF	řešit
konflikty	konflikt	k1gInPc4	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
tak	tak	k6eAd1	tak
důležitou	důležitý	k2eAgFnSc4d1	důležitá
pro	pro	k7c4	pro
optimální	optimální	k2eAgInSc4d1	optimální
vývoj	vývoj	k1gInSc4	vývoj
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Vyšší	vysoký	k2eAgFnSc7d2	vyšší
komisí	komise	k1gFnSc7	komise
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
uznaná	uznaný	k2eAgNnPc4d1	uznané
jako	jako	k9	jako
právo	právo	k1gNnSc4	právo
každého	každý	k3xTgNnSc2	každý
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dítě	dítě	k1gNnSc1	dítě
</s>
</p>
<p>
<s>
Puberta	puberta	k1gFnSc1	puberta
</s>
</p>
<p>
<s>
Adolescent	adolescent	k1gMnSc1	adolescent
</s>
</p>
<p>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
</s>
</p>
<p>
<s>
Dívka	dívka	k1gFnSc1	dívka
</s>
</p>
<p>
<s>
Narození	narození	k1gNnSc1	narození
</s>
</p>
<p>
<s>
Kojení	kojení	k1gNnSc1	kojení
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dětství	dětství	k1gNnSc2	dětství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dětství	dětství	k1gNnSc2	dětství
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Dětství	dětství	k1gNnSc2	dětství
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
