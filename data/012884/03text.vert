<p>
<s>
Odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
kvalita	kvalita	k1gFnSc1	kvalita
byla	být	k5eAaImAgFnS	být
zhoršena	zhoršit	k5eAaPmNgFnS	zhoršit
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hydroenergetice	hydroenergetika	k1gFnSc6	hydroenergetika
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
–	–	k?	–
označuje	označovat	k5eAaImIp3nS	označovat
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k6eAd1	již
předala	předat	k5eAaPmAgFnS	předat
svou	svůj	k3xOyFgFnSc4	svůj
energii	energie	k1gFnSc3	energie
turbíně	turbína	k1gFnSc3	turbína
nebo	nebo	k8xC	nebo
vodnímu	vodní	k2eAgNnSc3d1	vodní
kolu	kolo	k1gNnSc3	kolo
<g/>
.	.	kIx.	.
<g/>
Znečištění	znečištění	k1gNnSc3	znečištění
vody	voda	k1gFnSc2	voda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
rozpuštěnými	rozpuštěný	k2eAgFnPc7d1	rozpuštěná
nebo	nebo	k8xC	nebo
nerozpuštěnými	rozpuštěný	k2eNgFnPc7d1	nerozpuštěná
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
za	za	k7c4	za
znečištění	znečištění	k1gNnSc4	znečištění
se	se	k3xPyFc4	se
ale	ale	k9	ale
považuje	považovat	k5eAaImIp3nS	považovat
i	i	k9	i
například	například	k6eAd1	například
tepelné	tepelný	k2eAgNnSc1d1	tepelné
nebo	nebo	k8xC	nebo
radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
znečištění	znečištění	k1gNnSc1	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
před	před	k7c7	před
vypuštěním	vypuštění	k1gNnSc7	vypuštění
do	do	k7c2	do
vodotečí	vodoteč	k1gFnPc2	vodoteč
čištěna	čištěn	k2eAgFnSc1d1	čištěna
<g/>
.	.	kIx.	.
</s>
<s>
Odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
se	s	k7c7	s
silným	silný	k2eAgNnSc7d1	silné
rozložitelným	rozložitelný	k2eAgNnSc7d1	rozložitelné
organickým	organický	k2eAgNnSc7d1	organické
znečištěním	znečištění	k1gNnSc7	znečištění
bývá	bývat	k5eAaImIp3nS	bývat
voda	voda	k1gFnSc1	voda
polysaprobní	polysaprobní	k2eAgFnSc1d1	polysaprobní
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
saprobního	saprobní	k2eAgInSc2d1	saprobní
indexu	index	k1gInSc2	index
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
míra	míra	k1gFnSc1	míra
znečištění	znečištění	k1gNnSc2	znečištění
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přesnému	přesný	k2eAgNnSc3d1	přesné
stanovení	stanovení	k1gNnSc3	stanovení
znečišťujících	znečišťující	k2eAgFnPc2d1	znečišťující
látek	látka	k1gFnPc2	látka
slouží	sloužit	k5eAaImIp3nP	sloužit
chemické	chemický	k2eAgInPc1d1	chemický
rozbory	rozbor	k1gInPc1	rozbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakter	charakter	k1gInSc1	charakter
znečišťujících	znečišťující	k2eAgFnPc2d1	znečišťující
látek	látka	k1gFnPc2	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Charakter	charakter	k1gInSc1	charakter
znečišťujících	znečišťující	k2eAgFnPc2d1	znečišťující
látek	látka	k1gFnPc2	látka
silně	silně	k6eAd1	silně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
další	další	k2eAgNnSc4d1	další
nakládání	nakládání	k1gNnSc4	nakládání
s	s	k7c7	s
odpadní	odpadní	k2eAgFnSc7d1	odpadní
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozpuštěné	rozpuštěný	k2eAgInPc1d1	rozpuštěný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nerozpuštěné	rozpuštěný	k2eNgFnPc1d1	nerozpuštěná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpuštěné	rozpuštěný	k2eAgFnPc1d1	rozpuštěná
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
biologicky	biologicky	k6eAd1	biologicky
rozložitelné	rozložitelný	k2eAgNnSc4d1	rozložitelné
–	–	k?	–
například	například	k6eAd1	například
monosacharidy	monosacharid	k1gInPc4	monosacharid
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
biologicky	biologicky	k6eAd1	biologicky
nerozložitelné	rozložitelný	k2eNgNnSc4d1	nerozložitelné
–	–	k?	–
například	například	k6eAd1	například
azobarviva	azobarvivo	k1gNnSc2	azobarvivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odpadních	odpadní	k2eAgFnPc6d1	odpadní
vodách	voda	k1gFnPc6	voda
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
rozpuštěné	rozpuštěný	k2eAgFnPc1d1	rozpuštěná
anorganické	anorganický	k2eAgFnPc1d1	anorganická
látky	látka	k1gFnPc1	látka
–	–	k?	–
například	například	k6eAd1	například
anorganické	anorganický	k2eAgFnSc2d1	anorganická
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nerozpuštěné	rozpuštěný	k2eNgFnPc1d1	nerozpuštěná
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
biologicky	biologicky	k6eAd1	biologicky
rozložitelné	rozložitelný	k2eAgNnSc4d1	rozložitelné
–	–	k?	–
například	například	k6eAd1	například
škrob	škrob	k1gInSc1	škrob
nebo	nebo	k8xC	nebo
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
biologicky	biologicky	k6eAd1	biologicky
nerozložitelné	rozložitelný	k2eNgNnSc4d1	nerozložitelné
–	–	k?	–
například	například	k6eAd1	například
většina	většina	k1gFnSc1	většina
plastů	plast	k1gInPc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
dělit	dělit	k5eAaImF	dělit
na	na	k7c6	na
usaditelné	usaditelný	k2eAgFnSc6d1	usaditelný
a	a	k8xC	a
neusaditelné	usaditelný	k2eNgFnSc6d1	usaditelný
<g/>
.	.	kIx.	.
</s>
<s>
Anorganické	anorganický	k2eAgFnPc1d1	anorganická
nerozpuštěné	rozpuštěný	k2eNgFnPc1d1	nerozpuštěná
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
usaditelné	usaditelný	k2eAgFnPc1d1	usaditelný
–	–	k?	–
například	například	k6eAd1	například
písek	písek	k1gInSc4	písek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
neusaditelné	usaditelný	k2eNgFnPc1d1	usaditelný
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
různé	různý	k2eAgFnPc1d1	různá
koloidní	koloidní	k2eAgFnPc1d1	koloidní
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hydratované	hydratovaný	k2eAgInPc1d1	hydratovaný
oxidy	oxid	k1gInPc1	oxid
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gMnPc7	druh
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Komunální	komunální	k2eAgFnSc1d1	komunální
odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
každodenní	každodenní	k2eAgFnSc7d1	každodenní
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
–	–	k?	–
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
od	od	k7c2	od
živnostníků	živnostník	k1gMnPc2	živnostník
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Splašky	splašky	k1gInPc1	splašky
mají	mít	k5eAaImIp3nP	mít
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgNnSc4d1	stejné
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
splašků	splašky	k1gInPc2	splašky
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jednotné	jednotný	k2eAgFnSc2d1	jednotná
kanalizace	kanalizace	k1gFnSc2	kanalizace
i	i	k8xC	i
oplachové	oplachový	k2eAgFnSc2d1	oplachová
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
mytí	mytí	k1gNnSc2	mytí
ulic	ulice	k1gFnPc2	ulice
<g/>
)	)	kIx)	)
a	a	k8xC	a
dešťovou	dešťový	k2eAgFnSc4d1	dešťová
vodu	voda	k1gFnSc4	voda
ze	z	k7c2	z
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
se	se	k3xPyFc4	se
na	na	k7c6	na
městských	městský	k2eAgFnPc6d1	městská
čistírnách	čistírna	k1gFnPc6	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
ČOV	ČOV	kA	ČOV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
znečištění	znečištění	k1gNnSc2	znečištění
přiváděného	přiváděný	k2eAgNnSc2d1	přiváděné
na	na	k7c4	na
městskou	městský	k2eAgFnSc4d1	městská
ČOV	ČOV	kA	ČOV
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
jako	jako	k9	jako
počet	počet	k1gInSc1	počet
ekvivalentních	ekvivalentní	k2eAgMnPc2d1	ekvivalentní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
separaci	separace	k1gFnSc6	separace
splaškové	splaškový	k2eAgFnSc2d1	splašková
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
černá	černý	k2eAgFnSc1d1	černá
voda	voda	k1gFnSc1	voda
ze	z	k7c2	z
záchodů	záchod	k1gInPc2	záchod
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
moč	moč	k1gFnSc1	moč
a	a	k8xC	a
výkaly	výkal	k1gInPc1	výkal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
šedá	šedý	k2eAgFnSc1d1	šedá
voda	voda	k1gFnSc1	voda
–	–	k?	–
ostatní	ostatní	k2eAgFnSc1d1	ostatní
splašková	splaškový	k2eAgFnSc1d1	splašková
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
koupelny	koupelna	k1gFnPc1	koupelna
<g/>
,	,	kIx,	,
pračky	pračka	k1gFnPc1	pračka
<g/>
,	,	kIx,	,
kuchyně	kuchyně	k1gFnPc1	kuchyně
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dešťová	dešťový	k2eAgFnSc1d1	dešťová
voda	voda	k1gFnSc1	voda
–	–	k?	–
ze	z	k7c2	z
střech	střecha	k1gFnPc2	střecha
a	a	k8xC	a
zpevněných	zpevněný	k2eAgInPc2d1	zpevněný
povrchůUpravená	povrchůUpravený	k2eAgNnPc1d1	povrchůUpravený
(	(	kIx(	(
<g/>
jednodušeji	jednoduše	k6eAd2	jednoduše
než	než	k8xS	než
v	v	k7c6	v
ČOV	ČOV	kA	ČOV
<g/>
)	)	kIx)	)
šedá	šedý	k2eAgFnSc1d1	šedá
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
a	a	k8xC	a
dešťová	dešťový	k2eAgFnSc1d1	dešťová
voda	voda	k1gFnSc1	voda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
využity	využít	k5eAaPmNgInP	využít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
budovy	budova	k1gFnSc2	budova
jako	jako	k8xC	jako
voda	voda	k1gFnSc1	voda
užitková	užitkový	k2eAgFnSc1d1	užitková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
podnicích	podnik	k1gInPc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
a	a	k8xC	a
charakter	charakter	k1gInSc1	charakter
znečištění	znečištění	k1gNnSc2	znečištění
vody	voda	k1gFnSc2	voda
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
použité	použitý	k2eAgFnSc3d1	použitá
technologii	technologie	k1gFnSc3	technologie
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
produkuje	produkovat	k5eAaImIp3nS	produkovat
odpadní	odpadní	k2eAgFnPc4d1	odpadní
vody	voda	k1gFnPc4	voda
jednak	jednak	k8xC	jednak
z	z	k7c2	z
technologických	technologický	k2eAgFnPc2d1	technologická
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
přímo	přímo	k6eAd1	přímo
použitá	použitý	k2eAgFnSc1d1	použitá
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
z	z	k7c2	z
chladicích	chladicí	k2eAgFnPc2d1	chladicí
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
používaná	používaný	k2eAgFnSc1d1	používaná
na	na	k7c4	na
chlazení	chlazení	k1gNnSc4	chlazení
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
znečištěná	znečištěný	k2eAgFnSc1d1	znečištěná
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
tepelně	tepelně	k6eAd1	tepelně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
čistí	čistit	k5eAaImIp3nS	čistit
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
(	(	kIx(	(
<g/>
tam	tam	k6eAd1	tam
někdy	někdy	k6eAd1	někdy
stačí	stačit	k5eAaBmIp3nS	stačit
vodu	voda	k1gFnSc4	voda
předčistit	předčistit	k5eAaPmF	předčistit
a	a	k8xC	a
pak	pak	k6eAd1	pak
vypustit	vypustit	k5eAaPmF	vypustit
do	do	k7c2	do
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
ČOV	ČOV	kA	ČOV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Znečištění	znečištění	k1gNnSc1	znečištění
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
Čistírna	čistírna	k1gFnSc1	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
</s>
</p>
<p>
<s>
Čištění	čištění	k1gNnSc1	čištění
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
</s>
</p>
