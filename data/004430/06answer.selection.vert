<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
tří	tři	k4xCgFnPc2	tři
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
medaile	medaile	k1gFnSc2	medaile
z	z	k7c2	z
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
rekordman	rekordman	k1gMnSc1	rekordman
a	a	k8xC	a
držitel	držitel	k1gMnSc1	držitel
současného	současný	k2eAgInSc2d1	současný
světového	světový	k2eAgInSc2d1	světový
rekordu	rekord	k1gInSc2	rekord
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
oštěpem	oštěp	k1gInSc7	oštěp
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
98,48	[number]	k4	98,48
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
