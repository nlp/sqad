<p>
<s>
Bukač	bukač	k1gMnSc1	bukač
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Botaurus	Botaurus	k1gInSc1	Botaurus
stellaris	stellaris	k1gInSc1	stellaris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velkým	velký	k2eAgInSc7d1	velký
druhem	druh	k1gInSc7	druh
brodivého	brodivý	k2eAgMnSc4d1	brodivý
ptáka	pták	k1gMnSc4	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
volavkovitých	volavkovitý	k2eAgFnPc2d1	volavkovitý
(	(	kIx(	(
<g/>
Ardeidae	Ardeidae	k1gFnPc2	Ardeidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
bažanta	bažant	k1gMnSc2	bažant
<g/>
;	;	kIx,	;
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
69	[number]	k4	69
<g/>
–	–	k?	–
<g/>
81	[number]	k4	81
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
817	[number]	k4	817
<g/>
–	–	k?	–
<g/>
1150	[number]	k4	1150
g	g	kA	g
<g/>
,	,	kIx,	,
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
mezi	mezi	k7c7	mezi
966	[number]	k4	966
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
g.	g.	k?	g.
</s>
<s>
Oproti	oproti	k7c3	oproti
volavkám	volavka	k1gFnPc3	volavka
má	mít	k5eAaImIp3nS	mít
zavalitější	zavalitý	k2eAgNnSc1d2	zavalitější
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
krk	krk	k1gInSc4	krk
a	a	k8xC	a
nenápadné	nápadný	k2eNgNnSc4d1	nenápadné
hnědé	hnědý	k2eAgNnSc4d1	hnědé
zbarvení	zbarvení	k1gNnSc4	zbarvení
s	s	k7c7	s
tmavším	tmavý	k2eAgNnSc7d2	tmavší
skvrněním	skvrnění	k1gNnSc7	skvrnění
a	a	k8xC	a
vlnkováním	vlnkování	k1gNnSc7	vlnkování
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
a	a	k8xC	a
končetiny	končetina	k1gFnPc1	končetina
má	mít	k5eAaImIp3nS	mít
zelenožluté	zelenožlutý	k2eAgNnSc1d1	zelenožluté
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
velmi	velmi	k6eAd1	velmi
skrytě	skrytě	k6eAd1	skrytě
<g/>
,	,	kIx,	,
při	při	k7c6	při
ohrožení	ohrožení	k1gNnSc6	ohrožení
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
vzpřímený	vzpřímený	k2eAgInSc1d1	vzpřímený
postoj	postoj	k1gInSc1	postoj
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
krk	krk	k1gInSc4	krk
se	s	k7c7	s
zobákem	zobák	k1gInSc7	zobák
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
maskovacímu	maskovací	k2eAgNnSc3d1	maskovací
zbarvení	zbarvení	k1gNnSc3	zbarvení
tak	tak	k6eAd1	tak
dokonale	dokonale	k6eAd1	dokonale
splyne	splynout	k5eAaPmIp3nS	splynout
s	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
rákosím	rákosí	k1gNnSc7	rákosí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
již	již	k6eAd1	již
u	u	k7c2	u
mláďat	mládě	k1gNnPc2	mládě
starších	starý	k2eAgInPc2d2	starší
8	[number]	k4	8
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
tak	tak	k9	tak
většinou	většinou	k6eAd1	většinou
nejlépe	dobře	k6eAd3	dobře
prozradí	prozradit	k5eAaPmIp3nP	prozradit
hluboké	hluboký	k2eAgFnPc1d1	hluboká
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
slyšitelné	slyšitelný	k2eAgFnPc1d1	slyšitelná
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
opakované	opakovaný	k2eAgInPc1d1	opakovaný
"	"	kIx"	"
<g/>
vhumb	vhumb	k1gInSc1	vhumb
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
ozývají	ozývat	k5eAaImIp3nP	ozývat
samci	samec	k1gMnPc1	samec
během	během	k7c2	během
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
poddruzích	poddruh	k1gInPc6	poddruh
<g/>
;	;	kIx,	;
B.	B.	kA	B.
s.	s.	k?	s.
stellaris	stellaris	k1gInSc1	stellaris
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Eurasie	Eurasie	k1gFnSc2	Eurasie
od	od	k7c2	od
Španělska	Španělsko	k1gNnSc2	Španělsko
východně	východně	k6eAd1	východně
až	až	k6eAd1	až
po	po	k7c6	po
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
B.	B.	kA	B.
s.	s.	k?	s.
capensis	capensis	k1gFnSc1	capensis
pak	pak	k6eAd1	pak
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
tažný	tažný	k2eAgInSc1d1	tažný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
evropští	evropský	k2eAgMnPc1d1	evropský
ptáci	pták	k1gMnPc1	pták
zimují	zimovat	k5eAaImIp3nP	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
tropické	tropický	k2eAgFnSc3d1	tropická
Africe	Afrika	k1gFnSc3	Afrika
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
razantnímu	razantní	k2eAgInSc3d1	razantní
poklesu	pokles	k1gInSc3	pokles
početnosti	početnost	k1gFnSc2	početnost
bukače	bukač	k1gMnSc2	bukač
velkého	velký	k2eAgMnSc2d1	velký
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c6	na
celoevropské	celoevropský	k2eAgFnSc6d1	celoevropská
úrovni	úroveň	k1gFnSc6	úroveň
docházet	docházet	k5eAaImF	docházet
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
oblastech	oblast	k1gFnPc6	oblast
rozšíření	rozšíření	k1gNnSc1	rozšíření
ještě	ještě	k6eAd1	ještě
razantnějším	razantní	k2eAgMnSc7d2	razantnější
a	a	k8xC	a
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
i	i	k9	i
řadu	řada	k1gFnSc4	řada
silných	silný	k2eAgFnPc2d1	silná
východoevropských	východoevropský	k2eAgFnPc2d1	východoevropská
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
tak	tak	k6eAd1	tak
výrazného	výrazný	k2eAgInSc2d1	výrazný
úbytku	úbytek	k1gInSc2	úbytek
je	být	k5eAaImIp3nS	být
likvidace	likvidace	k1gFnSc1	likvidace
a	a	k8xC	a
vysoušení	vysoušení	k1gNnSc1	vysoušení
rákosin	rákosina	k1gFnPc2	rákosina
a	a	k8xC	a
eutrofizace	eutrofizace	k1gFnSc2	eutrofizace
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
odhadem	odhad	k1gInSc7	odhad
30-40	[number]	k4	30-40
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
údolních	údolní	k2eAgFnPc6d1	údolní
nivách	niva	k1gFnPc6	niva
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
,	,	kIx,	,
Dyje	Dyje	k1gFnSc2	Dyje
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
rybničních	rybniční	k2eAgFnPc6d1	rybniční
pánvích	pánev	k1gFnPc6	pánev
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
rybnících	rybník	k1gInPc6	rybník
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
do	do	k7c2	do
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
;	;	kIx,	;
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
však	však	k9	však
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
i	i	k9	i
pravidelně	pravidelně	k6eAd1	pravidelně
jednotlivě	jednotlivě	k6eAd1	jednotlivě
zimuje	zimovat	k5eAaImIp3nS	zimovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc1d1	chráněný
jako	jako	k8xS	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biotop	biotop	k1gInSc4	biotop
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jen	jen	k9	jen
ve	v	k7c6	v
větších	veliký	k2eAgInPc6d2	veliký
porostech	porost	k1gInPc6	porost
rákosu	rákos	k1gInSc2	rákos
s	s	k7c7	s
volnou	volný	k2eAgFnSc7d1	volná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Požírá	požírat	k5eAaImIp3nS	požírat
především	především	k9	především
ryby	ryba	k1gFnPc4	ryba
a	a	k8xC	a
obojživelníky	obojživelník	k1gMnPc4	obojživelník
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
různé	různý	k2eAgMnPc4d1	různý
bezobratlé	bezobratlí	k1gMnPc4	bezobratlí
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
hmyz	hmyz	k1gInSc1	hmyz
<g/>
)	)	kIx)	)
a	a	k8xC	a
malé	malý	k2eAgMnPc4d1	malý
savce	savec	k1gMnPc4	savec
<g/>
;	;	kIx,	;
občas	občas	k6eAd1	občas
i	i	k9	i
plení	plení	k1gNnSc4	plení
hnízda	hnízdo	k1gNnSc2	hnízdo
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
potravě	potrava	k1gFnSc6	potrava
pátrá	pátrat	k5eAaImIp3nS	pátrat
hlavně	hlavně	k9	hlavně
za	za	k7c2	za
svítání	svítání	k1gNnSc2	svítání
a	a	k8xC	a
soumraku	soumrak	k1gInSc2	soumrak
v	v	k7c6	v
mělké	mělký	k2eAgFnSc6d1	mělká
vodě	voda	k1gFnSc6	voda
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
rákosin	rákosina	k1gFnPc2	rákosina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
x	x	k?	x
ročně	ročně	k6eAd1	ročně
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
monogamní	monogamní	k2eAgMnPc1d1	monogamní
i	i	k8xC	i
polygamní	polygamní	k2eAgMnPc1d1	polygamní
-	-	kIx~	-
ve	v	k7c6	v
vhodných	vhodný	k2eAgFnPc6d1	vhodná
podmínkách	podmínka	k1gFnPc6	podmínka
mohou	moct	k5eAaImIp3nP	moct
hnízdit	hnízdit	k5eAaImF	hnízdit
až	až	k9	až
s	s	k7c7	s
5	[number]	k4	5
samicemi	samice	k1gFnPc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
je	být	k5eAaImIp3nS	být
plošina	plošina	k1gFnSc1	plošina
ze	z	k7c2	z
starých	starý	k2eAgNnPc2d1	staré
rákosových	rákosový	k2eAgNnPc2d1	rákosové
stébel	stéblo	k1gNnPc2	stéblo
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
cm	cm	kA	cm
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
skrytá	skrytý	k2eAgFnSc1d1	skrytá
v	v	k7c6	v
hustých	hustý	k2eAgFnPc6d1	hustá
rákosinách	rákosina	k1gFnPc6	rákosina
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
mělčinou	mělčina	k1gFnSc7	mělčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
snášce	snáška	k1gFnSc6	snáška
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
olivově	olivově	k6eAd1	olivově
hnědých	hnědý	k2eAgNnPc2d1	hnědé
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
52,8	[number]	k4	52,8
x	x	k?	x
38,5	[number]	k4	38,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nP	sedit
samotná	samotný	k2eAgFnSc1d1	samotná
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
porostlá	porostlý	k2eAgNnPc1d1	porostlé
rudohnědým	rudohnědý	k2eAgMnPc3d1	rudohnědý
peřím	peřit	k5eAaImIp1nS	peřit
a	a	k8xC	a
krmená	krmený	k2eAgFnSc1d1	krmená
pouze	pouze	k6eAd1	pouze
samicí	samice	k1gFnSc7	samice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jim	on	k3xPp3gMnPc3	on
vyvrhuje	vyvrhovat	k5eAaImIp3nS	vyvrhovat
potravu	potrava	k1gFnSc4	potrava
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přímém	přímý	k2eAgNnSc6d1	přímé
ohrožení	ohrožení	k1gNnSc6	ohrožení
mohou	moct	k5eAaImIp3nP	moct
hnízdo	hnízdo	k1gNnSc4	hnízdo
opustit	opustit	k5eAaPmF	opustit
již	již	k6eAd1	již
po	po	k7c6	po
2-3	[number]	k4	2-3
týdnech	týden	k1gInPc6	týden
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
tak	tak	k9	tak
však	však	k9	však
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
4-5	[number]	k4	4-5
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
hnízda	hnízdo	k1gNnSc2	hnízdo
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
vzletnosti	vzletnost	k1gFnSc2	vzletnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
8	[number]	k4	8
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
skrývají	skrývat	k5eAaImIp3nP	skrývat
v	v	k7c4	v
okolní	okolní	k2eAgFnSc4d1	okolní
vegetaci	vegetace	k1gFnSc4	vegetace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bukač	bukač	k1gMnSc1	bukač
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
bukač	bukač	k1gMnSc1	bukač
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Botaurus	Botaurus	k1gInSc1	Botaurus
stellaris	stellaris	k1gInSc1	stellaris
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
