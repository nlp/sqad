<s>
Radhošť	Radhošť	k1gFnSc1
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
v	v	k7c6
Moravskoslezských	moravskoslezský	k2eAgInPc6d1
Beskydech	Beskyd	k1gInPc6
na	na	k7c6
závěru	závěr	k1gInSc6
výrazného	výrazný	k2eAgInSc2d1
Pustevenského	Pustevenský	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
<g/>
,	,	kIx,
3	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Trojanovic	Trojanovice	k1gFnPc2
a	a	k8xC
6	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Rožnova	Rožnov	k1gInSc2
pod	pod	k7c7
Radhoštěm	Radhošť	k1gInSc7
<g/>
.	.	kIx.
</s>