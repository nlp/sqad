<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
atmosféry	atmosféra	k1gFnSc2	atmosféra
Titanu	titan	k1gInSc2	titan
je	být	k5eAaImIp3nS	být
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
minoritními	minoritní	k2eAgFnPc7d1	minoritní
složkami	složka	k1gFnPc7	složka
jsou	být	k5eAaImIp3nP	být
methan	methan	k1gInSc4	methan
a	a	k8xC	a
ethan	ethan	k1gInSc4	ethan
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
oblačnost	oblačnost	k1gFnSc1	oblačnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
dusíkem	dusík	k1gInSc7	dusík
obohacený	obohacený	k2eAgInSc4d1	obohacený
organický	organický	k2eAgInSc4d1	organický
smog	smog	k1gInSc4	smog
<g/>
.	.	kIx.	.
</s>
