<s>
Azimut	azimut	k1gInSc1	azimut
(	(	kIx(	(
<g/>
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
ا	ا	k?	ا
(	(	kIx(	(
<g/>
as-samat	asamat	k1gInSc1	as-samat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgInSc1d1	znamenající
"	"	kIx"	"
<g/>
směr	směr	k1gInSc1	směr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ve	v	k7c6	v
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
rovině	rovina	k1gFnSc6	rovina
svírá	svírat	k5eAaImIp3nS	svírat
určitý	určitý	k2eAgInSc4d1	určitý
směr	směr	k1gInSc4	směr
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
severním	severní	k2eAgInSc7d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
Azimut	azimut	k1gInSc1	azimut
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
polohy	poloha	k1gFnSc2	poloha
pozorovaného	pozorovaný	k2eAgInSc2d1	pozorovaný
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
navigaci	navigace	k1gFnSc4	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Úhel	úhel	k1gInSc1	úhel
je	být	k5eAaImIp3nS	být
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
po	po	k7c6	po
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hodnoty	hodnota	k1gFnPc1	hodnota
přibývají	přibývat	k5eAaImIp3nP	přibývat
od	od	k7c2	od
severu	sever	k1gInSc2	sever
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
se	se	k3xPyFc4	se
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
°	°	k?	°
<g/>
–	–	k?	–
<g/>
360	[number]	k4	360
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sever	sever	k1gInSc1	sever
má	mít	k5eAaImIp3nS	mít
azimut	azimut	k1gInSc4	azimut
0	[number]	k4	0
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
východ	východ	k1gInSc1	východ
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
180	[number]	k4	180
<g/>
°	°	k?	°
a	a	k8xC	a
západ	západ	k1gInSc1	západ
270	[number]	k4	270
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
jižního	jižní	k2eAgInSc2d1	jižní
a	a	k8xC	a
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
všechny	všechen	k3xTgInPc1	všechen
azimuty	azimut	k1gInPc1	azimut
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
opačnému	opačný	k2eAgNnSc3d1	opačné
pólu	pólo	k1gNnSc3	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Azimut	azimut	k1gInSc1	azimut
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
směru	směr	k1gInSc2	směr
pochodu	pochod	k1gInSc2	pochod
<g/>
,	,	kIx,	,
směru	směr	k1gInSc2	směr
k	k	k7c3	k
pozorovanému	pozorovaný	k2eAgInSc3d1	pozorovaný
objektu	objekt	k1gInSc3	objekt
<g/>
,	,	kIx,	,
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
apod.	apod.	kA	apod.
</s>
<s>
K	k	k7c3	k
měření	měření	k1gNnSc3	měření
azimutu	azimut	k1gInSc2	azimut
při	při	k7c6	při
navigaci	navigace	k1gFnSc6	navigace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
buzola	buzola	k1gFnSc1	buzola
a	a	k8xC	a
v	v	k7c6	v
mapě	mapa	k1gFnSc6	mapa
také	také	k9	také
úhloměr	úhloměr	k1gInSc1	úhloměr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zvolené	zvolený	k2eAgFnSc2d1	zvolená
aplikace	aplikace	k1gFnSc2	aplikace
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zohlednit	zohlednit	k5eAaPmF	zohlednit
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
deklinaci	deklinace	k1gFnSc4	deklinace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přesná	přesný	k2eAgFnSc1d1	přesná
navigace	navigace	k1gFnSc1	navigace
dle	dle	k7c2	dle
mapy	mapa	k1gFnSc2	mapa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
stačí	stačit	k5eAaBmIp3nS	stačit
azimut	azimut	k1gInSc4	azimut
měřit	měřit	k5eAaImF	měřit
od	od	k7c2	od
magnetického	magnetický	k2eAgInSc2d1	magnetický
severu	sever	k1gInSc2	sever
(	(	kIx(	(
<g/>
např.	např.	kA	např.
určení	určení	k1gNnSc1	určení
návratového	návratový	k2eAgInSc2d1	návratový
směru	směr	k1gInSc2	směr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sférické	sférický	k2eAgFnSc6d1	sférická
astronomii	astronomie	k1gFnSc6	astronomie
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
obzorníkové	obzorníkový	k2eAgFnPc4d1	obzorníkový
souřadnice	souřadnice	k1gFnPc4	souřadnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
azimut	azimut	k1gInSc1	azimut
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
úhlovou	úhlový	k2eAgFnSc7d1	úhlová
výškou	výška	k1gFnSc7	výška
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
polohy	poloha	k1gFnSc2	poloha
nebeského	nebeský	k2eAgNnSc2d1	nebeské
tělesa	těleso	k1gNnSc2	těleso
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
azimut	azimut	k1gInSc1	azimut
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
