<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
<g/>
,	,	kIx,
nepřechýleně	přechýleně	k6eNd1
Claudia	Claudia	k1gFnSc1
Cardinale	Cardinale	k1gFnSc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
označována	označován	k2eAgNnPc4d1
CC	CC	kA
<g/>
,	,	kIx,
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1938	#num#	k4
Tunis	Tunis	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
italská	italský	k2eAgFnSc1d1
divadelní	divadelní	k2eAgFnSc1d1
a	a	k8xC
filmová	filmový	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hereckou	herecký	k2eAgFnSc7d1
legendou	legenda	k1gFnSc7
a	a	k8xC
sexuálním	sexuální	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>