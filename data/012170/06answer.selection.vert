<s>
Stabilimentum	Stabilimentum	k1gNnSc1	Stabilimentum
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
pásek	pásek	k1gInSc1	pásek
vláken	vlákno	k1gNnPc2	vlákno
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
či	či	k8xC	či
nad	nad	k7c7	nad
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
středem	střed	k1gInSc7	střed
pavoučí	pavoučí	k2eAgFnSc2d1	pavoučí
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
např.	např.	kA	např.
pro	pro	k7c4	pro
síť	síť	k1gFnSc4	síť
křižáka	křižák	k1gMnSc2	křižák
pruhovaného	pruhovaný	k2eAgMnSc2d1	pruhovaný
<g/>
.	.	kIx.	.
</s>
