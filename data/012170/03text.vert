<p>
<s>
Stabilimentum	Stabilimentum	k1gNnSc1	Stabilimentum
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
pásek	pásek	k1gInSc1	pásek
vláken	vlákno	k1gNnPc2	vlákno
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
či	či	k8xC	či
nad	nad	k7c7	nad
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
středem	střed	k1gInSc7	střed
pavoučí	pavoučí	k2eAgFnSc2d1	pavoučí
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
např.	např.	kA	např.
pro	pro	k7c4	pro
síť	síť	k1gFnSc4	síť
křižáka	křižák	k1gMnSc2	křižák
pruhovaného	pruhovaný	k2eAgMnSc2d1	pruhovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvary	tvar	k1gInPc4	tvar
a	a	k8xC	a
funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
svislý	svislý	k2eAgInSc1d1	svislý
<g/>
,	,	kIx,	,
cikcakový	cikcakový	k2eAgInSc1d1	cikcakový
pásek	pásek	k1gInSc4	pásek
vláken	vlákno	k1gNnPc2	vlákno
nad	nad	k7c7	nad
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
středem	střed	k1gInSc7	střed
pavoučí	pavoučí	k2eAgFnSc2d1	pavoučí
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
tvary	tvar	k1gInPc4	tvar
stabilimentů	stabiliment	k1gInPc2	stabiliment
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
existují	existovat	k5eAaImIp3nP	existovat
kruhovitě	kruhovitě	k6eAd1	kruhovitě
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
cikcakové	cikcakový	k2eAgFnPc1d1	cikcakový
pásky	páska	k1gFnPc1	páska
okolo	okolo	k7c2	okolo
středu	střed	k1gInSc2	střed
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgMnSc1	jeden
pásek	pásek	k1gMnSc1	pásek
směrem	směr	k1gInSc7	směr
dolů	dolů	k6eAd1	dolů
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
chybějící	chybějící	k2eAgInSc1d1	chybějící
pásek	pásek	k1gInSc1	pásek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nenormální	normální	k2eNgFnSc1d1	nenormální
či	či	k8xC	či
chybějící	chybějící	k2eAgFnSc1d1	chybějící
stabilimenta	stabilimenta	k1gFnSc1	stabilimenta
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
původně	původně	k6eAd1	původně
předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
stabilizační	stabilizační	k2eAgFnSc1d1	stabilizační
funkce	funkce	k1gFnSc1	funkce
není	být	k5eNaImIp3nS	být
primární	primární	k2eAgFnSc1d1	primární
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
jako	jako	k9	jako
maskování	maskování	k1gNnSc2	maskování
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
potvrzená	potvrzený	k2eAgFnSc1d1	potvrzená
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgInPc2d1	současný
výzkumů	výzkum	k1gInPc2	výzkum
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
vlákno	vlákno	k1gNnSc1	vlákno
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
odráží	odrážet	k5eAaImIp3nS	odrážet
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pavučina	pavučina	k1gFnSc1	pavučina
připomíná	připomínat	k5eAaImIp3nS	připomínat
hmyzu	hmyz	k1gInSc2	hmyz
květ	květ	k1gInSc1	květ
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Andre	Andr	k1gInSc5	Andr
Waltera	Walter	k1gMnSc2	Walter
a	a	k8xC	a
Marka	Marek	k1gMnSc2	Marek
Elgara	Elgar	k1gMnSc2	Elgar
jsou	být	k5eAaImIp3nP	být
stabilimenta	stabilimento	k1gNnSc2	stabilimento
důmyslnou	důmyslný	k2eAgFnSc7d1	důmyslná
metodou	metoda	k1gFnSc7	metoda
ochrany	ochrana	k1gFnSc2	ochrana
pavučiny	pavučina	k1gFnSc2	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
dobře	dobře	k6eAd1	dobře
viditelná	viditelný	k2eAgNnPc1d1	viditelné
pro	pro	k7c4	pro
větší	veliký	k2eAgMnPc4d2	veliký
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
vyhnou	vyhnout	k5eAaPmIp3nP	vyhnout
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
pavučinu	pavučina	k1gFnSc4	pavučina
nepoškodí	poškodit	k5eNaPmIp3nS	poškodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlivy	vliv	k1gInPc1	vliv
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
stabiliment	stabiliment	k1gInSc1	stabiliment
==	==	k?	==
</s>
</p>
<p>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavba	stavba	k1gFnSc1	stavba
stabilimenta	stabilimenta	k1gFnSc1	stabilimenta
bývá	bývat	k5eAaImIp3nS	bývat
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
buď	buď	k8xC	buď
chemickou	chemický	k2eAgFnSc7d1	chemická
kontaminací	kontaminace	k1gFnSc7	kontaminace
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
nebo	nebo	k8xC	nebo
věkem	věk	k1gInSc7	věk
a	a	k8xC	a
pohlavím	pohlaví	k1gNnSc7	pohlaví
pavouka	pavouk	k1gMnSc2	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
před	před	k7c7	před
dosažením	dosažení	k1gNnSc7	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
zralosti	zralost	k1gFnSc2	zralost
převážně	převážně	k6eAd1	převážně
tkají	tkát	k5eAaImIp3nP	tkát
běžný	běžný	k2eAgInSc1d1	běžný
svislý	svislý	k2eAgInSc1d1	svislý
<g/>
,	,	kIx,	,
cikcakový	cikcakový	k2eAgInSc1d1	cikcakový
pásek	pásek	k1gInSc1	pásek
nad	nad	k7c7	nad
a	a	k8xC	a
pod	pod	k7c7	pod
středem	střed	k1gInSc7	střed
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
cirkulární	cirkulární	k2eAgInPc1d1	cirkulární
pásky	pásek	k1gInPc1	pásek
okolo	okolo	k7c2	okolo
středu	střed	k1gInSc2	střed
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
samci	samec	k1gMnPc1	samec
od	od	k7c2	od
září	září	k1gNnSc2	září
či	či	k8xC	či
října	říjen	k1gInSc2	říjen
tkají	tkát	k5eAaImIp3nP	tkát
už	už	k6eAd1	už
jen	jen	k9	jen
jednoramenné	jednoramenný	k2eAgFnSc2d1	jednoramenná
cikcakové	cikcakový	k2eAgFnSc2d1	cikcakový
pásky	páska	k1gFnSc2	páska
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
křižáka	křižák	k1gMnSc2	křižák
pruhovaného	pruhovaný	k2eAgInSc2d1	pruhovaný
rovněž	rovněž	k9	rovněž
většinou	většina	k1gFnSc7	většina
zakládají	zakládat	k5eAaImIp3nP	zakládat
známý	známý	k2eAgInSc4d1	známý
svislý	svislý	k2eAgInSc4d1	svislý
pásek	pásek	k1gInSc4	pásek
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
ale	ale	k8xC	ale
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
tkát	tkát	k5eAaImF	tkát
i	i	k9	i
kruhová	kruhový	k2eAgFnSc1d1	kruhová
stabilimenta	stabilimenta	k1gFnSc1	stabilimenta
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
dokonce	dokonce	k9	dokonce
i	i	k8xC	i
kombinace	kombinace	k1gFnSc1	kombinace
kruhových	kruhový	k2eAgFnPc2d1	kruhová
a	a	k8xC	a
svislých	svislý	k2eAgFnPc2d1	svislá
stabiliment	stabiliment	k1gMnSc1	stabiliment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
a	a	k8xC	a
Elgar	Elgar	k1gMnSc1	Elgar
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
pavoukem	pavouk	k1gMnSc7	pavouk
Argiope	Argiop	k1gInSc5	Argiop
keyserlingi	keyserling	k1gFnSc6	keyserling
pavouk	pavouk	k1gMnSc1	pavouk
na	na	k7c4	na
malá	malý	k2eAgNnPc4d1	malé
poškození	poškození	k1gNnPc4	poškození
pavučiny	pavučina	k1gFnSc2	pavučina
nereagoval	reagovat	k5eNaBmAgMnS	reagovat
<g/>
,	,	kIx,	,
větší	veliký	k2eAgNnSc4d2	veliký
opravoval	opravovat	k5eAaImAgMnS	opravovat
a	a	k8xC	a
na	na	k7c4	na
opakovaná	opakovaný	k2eAgNnPc4d1	opakované
větší	veliký	k2eAgNnPc4d2	veliký
poškození	poškození	k1gNnPc4	poškození
zareagoval	zareagovat	k5eAaPmAgInS	zareagovat
tvorbou	tvorba	k1gFnSc7	tvorba
větších	veliký	k2eAgInPc2d2	veliký
stabiliment	stabiliment	k1gInSc1	stabiliment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Wespenspinne	Wespenspinn	k1gInSc5	Wespenspinn
na	na	k7c6	na
německé	německý	k2eAgFnSc3d1	německá
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pavučina	pavučina	k1gFnSc1	pavučina
</s>
</p>
<p>
<s>
Pavoučí	pavoučí	k2eAgNnSc1d1	pavoučí
hedvábí	hedvábí	k1gNnSc1	hedvábí
</s>
</p>
<p>
<s>
Křižák	křižák	k1gInSc1	křižák
pruhovaný	pruhovaný	k2eAgInSc4d1	pruhovaný
</s>
</p>
<p>
<s>
Křižákovití	Křižákovitý	k2eAgMnPc1d1	Křižákovitý
</s>
</p>
