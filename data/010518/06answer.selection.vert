<s>
Špenát	špenát	k1gInSc1	špenát
je	on	k3xPp3gMnPc4	on
zdraví	zdravit	k5eAaImIp3nS	zdravit
prospěšný	prospěšný	k2eAgMnSc1d1	prospěšný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
vitamínů	vitamín	k1gInPc2	vitamín
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
)	)	kIx)	)
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
