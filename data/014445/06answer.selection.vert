<s>
Braniborská	braniborský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Brandenburger	Brandenburger	k2eAgMnSc1d1
Tor	Tor	k1gMnSc1
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejznámějším	známý	k2eAgFnPc3d3
pamětihodnostem	pamětihodnost	k1gFnPc3
Berlína	Berlín	k1gInSc2
a	a	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gInPc3
významným	významný	k2eAgInPc3d1
symbolům	symbol	k1gInPc3
<g/>
.	.	kIx.
</s>