<s>
The	The	k?	The
Times	Times	k1gInSc1	Times
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgInSc1d1	britský
národní	národní	k2eAgInSc1d1	národní
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Times	Timesa	k1gFnPc2	Timesa
i	i	k8xC	i
jeho	jeho	k3xOp3gFnPc1	jeho
sesterské	sesterský	k2eAgFnPc1d1	sesterská
noviny	novina	k1gFnPc1	novina
The	The	k1gFnSc2	The
Sunday	Sundaa	k1gFnSc2	Sundaa
Times	Timesa	k1gFnPc2	Timesa
vydává	vydávat	k5eAaImIp3nS	vydávat
společnost	společnost	k1gFnSc1	společnost
Times	Timesa	k1gFnPc2	Timesa
Newspapers	Newspapersa	k1gFnPc2	Newspapersa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
News	News	k1gInSc1	News
Corporation	Corporation	k1gInSc1	Corporation
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Rupertem	Rupert	k1gMnSc7	Rupert
Murdochem	Murdoch	k1gMnSc7	Murdoch
<g/>
.	.	kIx.	.
</s>
<s>
Oboje	oboj	k1gFnPc1	oboj
noviny	novina	k1gFnSc2	novina
však	však	k9	však
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
a	a	k8xC	a
pod	pod	k7c4	pod
společné	společný	k2eAgNnSc4d1	společné
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
tento	tento	k3xDgInSc1	tento
středopravicový	středopravicový	k2eAgInSc1d1	středopravicový
deník	deník	k1gInSc1	deník
podporuje	podporovat	k5eAaImIp3nS	podporovat
zejména	zejména	k9	zejména
konzervativce	konzervativec	k1gMnPc4	konzervativec
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
2005	[number]	k4	2005
se	se	k3xPyFc4	se
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
labouristů	labourista	k1gMnPc2	labourista
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Times	Timesa	k1gFnPc2	Timesa
propůjčilo	propůjčit	k5eAaPmAgNnS	propůjčit
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Times	Times	k1gInSc4	Times
<g/>
"	"	kIx"	"
i	i	k8xC	i
dalším	další	k2eAgInPc3d1	další
deníkům	deník	k1gInPc3	deník
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
-	-	kIx~	-
např.	např.	kA	např.
The	The	k1gFnPc2	The
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Times	Times	k1gMnSc1	Times
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Times	Times	k1gMnSc1	Times
of	of	k?	of
India	indium	k1gNnSc2	indium
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Times	Times	k1gMnSc1	Times
of	of	k?	of
Malta	Malta	k1gFnSc1	Malta
nebo	nebo	k8xC	nebo
The	The	k1gMnSc1	The
Irish	Irish	k1gMnSc1	Irish
Times	Times	k1gMnSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
přehlednost	přehlednost	k1gFnSc4	přehlednost
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
označuje	označovat	k5eAaImIp3nS	označovat
The	The	k1gMnSc1	The
Times	Times	k1gMnSc1	Times
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
London	London	k1gMnSc1	London
Times	Times	k1gMnSc1	Times
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Times	Times	k1gMnSc1	Times
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
deník	deník	k1gInSc1	deník
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
představil	představit	k5eAaPmAgMnS	představit
písmo	písmo	k1gNnSc4	písmo
Times	Times	k1gMnSc1	Times
Roman	Roman	k1gMnSc1	Roman
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jeho	jeho	k3xOp3gMnSc1	jeho
typografický	typografický	k2eAgMnSc1d1	typografický
poradce	poradce	k1gMnSc1	poradce
Stanley	Stanlea	k1gFnSc2	Stanlea
Morrison	Morrison	k1gMnSc1	Morrison
a	a	k8xC	a
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
snadné	snadný	k2eAgFnSc3d1	snadná
čitelnosti	čitelnost	k1gFnSc3	čitelnost
jej	on	k3xPp3gNnSc4	on
dnes	dnes	k6eAd1	dnes
najdete	najít	k5eAaPmIp2nP	najít
prakticky	prakticky	k6eAd1	prakticky
všude	všude	k6eAd1	všude
<g/>
.	.	kIx.	.
</s>
<s>
Celých	celý	k2eAgNnPc2d1	celé
219	[number]	k4	219
let	léto	k1gNnPc2	léto
noviny	novina	k1gFnPc4	novina
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
"	"	kIx"	"
<g/>
větším	veliký	k2eAgInSc6d2	veliký
<g/>
"	"	kIx"	"
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
tradičně	tradičně	k6eAd1	tradičně
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
seriózním	seriózní	k2eAgInSc7d1	seriózní
tiskem	tisk	k1gInSc7	tisk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
noviny	novina	k1gFnPc1	novina
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
přesedlat	přesedlat	k5eAaPmF	přesedlat
na	na	k7c4	na
menší	malý	k2eAgInPc4d2	menší
rozměry	rozměr	k1gInPc4	rozměr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
tisk	tisk	k1gInSc1	tisk
bulvární	bulvární	k2eAgInSc1d1	bulvární
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
tím	ten	k3xDgNnSc7	ten
chtěly	chtít	k5eAaImAgFnP	chtít
přilákat	přilákat	k5eAaPmF	přilákat
více	hodně	k6eAd2	hodně
mladších	mladý	k2eAgMnPc2d2	mladší
čtenářů	čtenář	k1gMnPc2	čtenář
a	a	k8xC	a
také	také	k9	také
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
cestují	cestovat	k5eAaImIp3nP	cestovat
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
verze	verze	k1gFnSc1	verze
deníku	deník	k1gInSc2	deník
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Times	Times	k1gInSc1	Times
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaPmF	vydávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
John	John	k1gMnSc1	John
Walter	Walter	k1gMnSc1	Walter
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Daily	Daila	k1gFnSc2	Daila
Universal	Universal	k1gFnSc2	Universal
Register	registrum	k1gNnPc2	registrum
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
noviny	novina	k1gFnPc1	novina
přejmenovány	přejmenován	k2eAgFnPc1d1	přejmenována
na	na	k7c6	na
The	The	k1gFnSc6	The
Times	Timesa	k1gFnPc2	Timesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
převzala	převzít	k5eAaPmAgFnS	převzít
noviny	novina	k1gFnPc4	novina
společnost	společnost	k1gFnSc1	společnost
News	Newsa	k1gFnPc2	Newsa
International	International	k1gMnSc2	International
Ruperta	Rupert	k1gMnSc2	Rupert
Murdocha	Murdoch	k1gMnSc2	Murdoch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
vláda	vláda	k1gFnSc1	vláda
koupi	koupě	k1gFnSc4	koupě
nikdy	nikdy	k6eAd1	nikdy
neuvedla	uvést	k5eNaPmAgFnS	uvést
u	u	k7c2	u
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
monopoly	monopol	k1gInPc4	monopol
a	a	k8xC	a
fúze	fúze	k1gFnPc4	fúze
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předchozí	předchozí	k2eAgMnSc1d1	předchozí
vlastník	vlastník	k1gMnSc1	vlastník
The	The	k1gMnSc1	The
Thompson	Thompson	k1gMnSc1	Thompson
Corporation	Corporation	k1gInSc4	Corporation
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydávání	vydávání	k1gNnSc1	vydávání
novin	novina	k1gFnPc2	novina
zruší	zrušit	k5eAaPmIp3nS	zrušit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nebudou	být	k5eNaImBp3nP	být
v	v	k7c6	v
přiděleném	přidělený	k2eAgInSc6d1	přidělený
čase	čas	k1gInSc6	čas
převzaty	převzít	k5eAaPmNgInP	převzít
někým	někdo	k3yInSc7	někdo
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
právní	právní	k2eAgInSc4d1	právní
odsun	odsun	k1gInSc4	odsun
Murdochova	Murdochův	k2eAgNnSc2d1	Murdochovo
převzetí	převzetí	k1gNnSc2	převzetí
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
obou	dva	k4xCgFnPc2	dva
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
převzetí	převzetí	k1gNnSc1	převzetí
dalo	dát	k5eAaPmAgNnS	dát
Murdochovi	Murdochův	k2eAgMnPc1d1	Murdochův
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
čtyřmi	čtyři	k4xCgFnPc7	čtyři
národními	národní	k2eAgFnPc7d1	národní
novinami	novina	k1gFnPc7	novina
<g/>
;	;	kIx,	;
The	The	k1gMnPc1	The
Times	Times	k1gMnSc1	Times
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Sunday	Sundaa	k1gFnSc2	Sundaa
Times	Times	k1gMnSc1	Times
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Sun	Sun	kA	Sun
a	a	k8xC	a
News	News	k1gInSc1	News
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Walter	Walter	k1gMnSc1	Walter
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
<g/>
–	–	k?	–
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Walter	Walter	k1gMnSc1	Walter
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
–	–	k?	–
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Walter	Walter	k1gMnSc1	Walter
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
Arthur	Arthur	k1gMnSc1	Arthur
Fraser	Fraser	k1gMnSc1	Fraser
Walter	Walter	k1gMnSc1	Walter
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Lord	lord	k1gMnSc1	lord
Northcliffe	Northcliff	k1gInSc5	Northcliff
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
rodina	rodina	k1gFnSc1	rodina
Astorů	Astor	k1gInPc2	Astor
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Roy	Roy	k1gMnSc1	Roy
Thomson	Thomson	k1gMnSc1	Thomson
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
News	News	k1gInSc1	News
International	International	k1gFnSc2	International
(	(	kIx(	(
<g/>
pobočka	pobočka	k1gFnSc1	pobočka
News	News	k1gInSc1	News
Corporation	Corporation	k1gInSc1	Corporation
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Rupertem	Rupert	k1gMnSc7	Rupert
Murdochem	Murdoch	k1gMnSc7	Murdoch
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
)	)	kIx)	)
</s>
