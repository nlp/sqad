<s>
Křovinář	křovinář	k1gMnSc1	křovinář
ostnitý	ostnitý	k2eAgMnSc1d1	ostnitý
(	(	kIx(	(
<g/>
Bothriechis	Bothriechis	k1gFnSc4	Bothriechis
schlegeli	schleget	k5eAaBmAgMnP	schleget
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejhezčím	hezký	k2eAgMnPc3d3	nejhezčí
jedovatým	jedovatý	k2eAgMnPc3d1	jedovatý
hadům	had	k1gMnPc3	had
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zástupcem	zástupce	k1gMnSc7	zástupce
podčeledi	podčeleď	k1gFnSc2	podčeleď
chřestýšovitých	chřestýšovitý	k2eAgInPc2d1	chřestýšovitý
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
had	had	k1gMnSc1	had
je	být	k5eAaImIp3nS	být
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
příkladem	příklad	k1gInSc7	příklad
biologické	biologický	k2eAgFnSc2d1	biologická
konvergence	konvergence	k1gFnSc2	konvergence
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
i	i	k8xC	i
chováním	chování	k1gNnSc7	chování
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
kopií	kopie	k1gFnSc7	kopie
asijských	asijský	k2eAgMnPc2d1	asijský
chřestýšovců	chřestýšovec	k1gMnPc2	chřestýšovec
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
Trimereserus	Trimereserus	k1gInSc1	Trimereserus
<g/>
)	)	kIx)	)
a	a	k8xC	a
afrických	africký	k2eAgFnPc2d1	africká
stromových	stromový	k2eAgFnPc2d1	stromová
zmijí	zmije	k1gFnPc2	zmije
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
Ateris	Ateris	k1gInSc1	Ateris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tito	tento	k3xDgMnPc1	tento
hadi	had	k1gMnPc1	had
jsou	být	k5eAaImIp3nP	být
přizpůsobeni	přizpůsobit	k5eAaPmNgMnP	přizpůsobit
k	k	k7c3	k
životu	život	k1gInSc3	život
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
tropického	tropický	k2eAgInSc2d1	tropický
pralesa	prales	k1gInSc2	prales
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
proto	proto	k8xC	proto
tak	tak	k6eAd1	tak
podobní	podobný	k2eAgMnPc1d1	podobný
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
těžko	těžko	k6eAd1	těžko
rozliší	rozlišit	k5eAaPmIp3nS	rozlišit
i	i	k9	i
odborník	odborník	k1gMnSc1	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
had	had	k1gMnSc1	had
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
deštných	deštný	k2eAgInPc6d1	deštný
pralesích	prales	k1gInPc6	prales
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
poloviny	polovina	k1gFnSc2	polovina
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
od	od	k7c2	od
jižního	jižní	k2eAgNnSc2d1	jižní
Mexika	Mexiko	k1gNnSc2	Mexiko
po	po	k7c4	po
Ekvádor	Ekvádor	k1gInSc4	Ekvádor
a	a	k8xC	a
Peru	Peru	k1gNnSc1	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
biotopem	biotop	k1gInSc7	biotop
jsou	být	k5eAaImIp3nP	být
deštné	deštný	k2eAgInPc1d1	deštný
pralesy	prales	k1gInPc1	prales
a	a	k8xC	a
mlžné	mlžný	k2eAgInPc1d1	mlžný
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
hor.	hor.	k?	hor.
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c6	na
území	území	k1gNnSc6	území
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Kostarika	Kostarika	k1gFnSc1	Kostarika
Panama	panama	k2eAgNnSc1d1	panama
Peru	Peru	k1gNnSc1	Peru
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
Venezuela	Venezuela	k1gFnSc1	Venezuela
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
malý	malý	k2eAgMnSc1d1	malý
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnSc2	délka
maximálně	maximálně	k6eAd1	maximálně
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
a	a	k8xC	a
poddruhu	poddruh	k1gInSc6	poddruh
<g/>
,	,	kIx,	,
trávově	trávově	k6eAd1	trávově
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
tyrkysové	tyrkysový	k2eAgInPc1d1	tyrkysový
<g/>
,	,	kIx,	,
jasně	jasně	k6eAd1	jasně
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
hnědé	hnědý	k2eAgFnPc1d1	hnědá
či	či	k8xC	či
olivové	olivový	k2eAgFnPc1d1	olivová
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
tmavšími	tmavý	k2eAgFnPc7d2	tmavší
skvrnami	skvrna	k1gFnPc7	skvrna
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zbarvení	zbarvení	k1gNnSc1	zbarvení
hada	had	k1gMnSc2	had
maskuje	maskovat	k5eAaBmIp3nS	maskovat
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
pralesních	pralesní	k2eAgInPc2d1	pralesní
stromů	strom	k1gInPc2	strom
či	či	k8xC	či
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
kůře	kůra	k1gFnSc6	kůra
pokryté	pokrytý	k2eAgFnSc2d1	pokrytá
lišejníkem	lišejník	k1gInSc7	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
má	mít	k5eAaImIp3nS	mít
zvětšené	zvětšený	k2eAgFnPc4d1	zvětšená
šupiny	šupina	k1gFnPc4	šupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
jakési	jakýsi	k3yIgInPc4	jakýsi
růžky	růžek	k1gInPc4	růžek
či	či	k8xC	či
ostny	osten	k1gInPc4	osten
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgMnPc2	jenž
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
noční	noční	k2eAgInSc1d1	noční
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
přizpůsobený	přizpůsobený	k2eAgMnSc1d1	přizpůsobený
životu	život	k1gInSc2	život
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
chápavý	chápavý	k2eAgInSc4d1	chápavý
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
udržet	udržet	k5eAaPmF	udržet
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
a	a	k8xC	a
ochranné	ochranný	k2eAgNnSc4d1	ochranné
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
ptáky	pták	k1gMnPc7	pták
<g/>
,	,	kIx,	,
stromovými	stromový	k2eAgFnPc7d1	stromová
žábami	žába	k1gFnPc7	žába
<g/>
,	,	kIx,	,
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
plazy	plaz	k1gInPc1	plaz
a	a	k8xC	a
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
drobnými	drobný	k2eAgFnPc7d1	drobná
vačicemi	vačice	k1gFnPc7	vačice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
vejcoživorodý	vejcoživorodý	k2eAgInSc1d1	vejcoživorodý
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
klade	klást	k5eAaImIp3nS	klást
10	[number]	k4	10
-	-	kIx~	-
12	[number]	k4	12
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
líhnou	líhnout	k5eAaImIp3nP	líhnout
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
hemotoxický	hemotoxický	k2eAgInSc1d1	hemotoxický
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
rozklad	rozklad	k1gInSc4	rozklad
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
jako	jako	k8xS	jako
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
křovinářů	křovinář	k1gMnPc2	křovinář
a	a	k8xC	a
smrtelná	smrtelný	k2eAgNnPc1d1	smrtelné
uštknutí	uštknutí	k1gNnPc1	uštknutí
nejsou	být	k5eNaImIp3nP	být
častá	častý	k2eAgNnPc1d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgMnSc1	tento
had	had	k1gMnSc1	had
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgMnSc1d1	vzácný
a	a	k8xC	a
neútočný	útočný	k2eNgMnSc1d1	neútočný
<g/>
.	.	kIx.	.
</s>
<s>
Chřestýšovití	Chřestýšovití	k1gMnPc1	Chřestýšovití
Křovinář	křovinář	k1gMnSc1	křovinář
Křovinář	křovinář	k1gMnSc1	křovinář
němý	němý	k2eAgMnSc1d1	němý
Křovinář	křovinář	k1gMnSc1	křovinář
sametový	sametový	k2eAgInSc4d1	sametový
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Křovinář	křovinář	k1gMnSc1	křovinář
ostnitý	ostnitý	k2eAgMnSc1d1	ostnitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
