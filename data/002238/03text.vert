<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
日	日	k?	日
<g/>
,	,	kIx,	,
Nihonkoku	Nihonkok	k1gInSc2	Nihonkok
<g/>
/	/	kIx~	/
<g/>
Nipponkoku	Nipponkok	k1gInSc2	Nipponkok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
císařský	císařský	k2eAgInSc4d1	císařský
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
stát	stát	k1gInSc4	stát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
ho	on	k3xPp3gInSc4	on
Korejský	korejský	k2eAgInSc4d1	korejský
průliv	průliv	k1gInSc4	průliv
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
Japonské	japonský	k2eAgNnSc1d1	Japonské
moře	moře	k1gNnSc1	moře
ho	on	k3xPp3gMnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgInPc1d1	japonský
ostrovy	ostrov	k1gInPc1	ostrov
Rjúkjú	Rjúkjú	k1gNnPc2	Rjúkjú
z	z	k7c2	z
východu	východ	k1gInSc2	východ
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
Východočínské	východočínský	k2eAgNnSc4d1	Východočínské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
k	k	k7c3	k
Tchaj-wanu	Tchajan	k1gInSc3	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Japonska	Japonsko	k1gNnSc2	Japonsko
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
Země	země	k1gFnSc1	země
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
slunce	slunce	k1gNnSc2	slunce
<g/>
:	:	kIx,	:
日	日	k?	日
(	(	kIx(	(
<g/>
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
slunce	slunce	k1gNnSc1	slunce
<g/>
)	)	kIx)	)
本	本	k?	本
(	(	kIx(	(
<g/>
hon	hon	k1gInSc1	hon
<g/>
,	,	kIx,	,
původ	původ	k1gInSc1	původ
<g/>
)	)	kIx)	)
国	国	k?	国
(	(	kIx(	(
<g/>
koku	kok	k1gInSc2	kok
<g/>
,	,	kIx,	,
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čínsky	čínsky	k6eAd1	čínsky
se	se	k3xPyFc4	se
tytéž	týž	k3xTgInPc1	týž
znaky	znak	k1gInPc1	znak
čtou	číst	k5eAaImIp3nP	číst
Ž	Ž	kA	Ž
<g/>
'	'	kIx"	'
<g/>
-pen-kuo	enuo	k1gMnSc1	-pen-kuo
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
zřejmě	zřejmě	k6eAd1	zřejmě
pocházejí	pocházet	k5eAaImIp3nP	pocházet
názvy	název	k1gInPc1	název
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
francouzské	francouzský	k2eAgInPc4d1	francouzský
Japon	japon	k1gInSc4	japon
[	[	kIx(	[
<g/>
žapon	žapon	k1gInSc1	žapon
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
anglické	anglický	k2eAgInPc4d1	anglický
Japan	japan	k1gInSc4	japan
[	[	kIx(	[
<g/>
dž	dž	k?	dž
<g/>
'	'	kIx"	'
<g/>
pen	pen	k?	pen
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
Japonska	Japonsko	k1gNnSc2	Japonsko
je	být	k5eAaImIp3nS	být
sopka	sopka	k1gFnSc1	sopka
Fudži	Fudž	k1gFnSc3	Fudž
(	(	kIx(	(
<g/>
Fudži-san	Fudžian	k1gInSc1	Fudži-san
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nerodilí	rodilý	k2eNgMnPc1d1	nerodilý
mluvčí	mluvčí	k1gMnPc1	mluvčí
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
říkají	říkat	k5eAaImIp3nP	říkat
Fudži-jama	Fudžiama	k1gNnSc4	Fudži-jama
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
špatným	špatný	k2eAgNnSc7d1	špatné
čtením	čtení	k1gNnSc7	čtení
znaku	znak	k1gInSc2	znak
pro	pro	k7c4	pro
horu	hora	k1gFnSc4	hora
(	(	kIx(	(
<g/>
japonské	japonský	k2eAgNnSc4d1	Japonské
čtení	čtení	k1gNnSc4	čtení
"	"	kIx"	"
<g/>
jama	jama	k6eAd1	jama
<g/>
"	"	kIx"	"
zaměněno	zaměnit	k5eAaPmNgNnS	zaměnit
za	za	k7c4	za
sinojaponské	sinojaponský	k2eAgNnSc4d1	sinojaponské
čtení	čtení	k1gNnSc4	čtení
"	"	kIx"	"
<g/>
san	san	k?	san
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Japonska	Japonsko	k1gNnSc2	Japonsko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
dílčí	dílčí	k2eAgNnPc4d1	dílčí
období	období	k1gNnPc4	období
<g/>
:	:	kIx,	:
Džómon	Džómona	k1gFnPc2	Džómona
(	(	kIx(	(
<g/>
縄	縄	k?	縄
10	[number]	k4	10
000	[number]	k4	000
-	-	kIx~	-
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Jajoi	Jajo	k1gFnSc2	Jajo
(	(	kIx(	(
<g/>
弥	弥	k?	弥
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
710	[number]	k4	710
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
Nara	Nara	k1gMnSc1	Nara
(	(	kIx(	(
<g/>
奈	奈	k?	奈
710	[number]	k4	710
<g/>
-	-	kIx~	-
<g/>
794	[number]	k4	794
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Heian	Heiana	k1gFnPc2	Heiana
(	(	kIx(	(
<g/>
平	平	k?	平
794	[number]	k4	794
<g/>
-	-	kIx~	-
<g/>
1185	[number]	k4	1185
<g/>
)	)	kIx)	)
Kamakura	Kamakura	k1gFnSc1	Kamakura
(	(	kIx(	(
<g/>
鎌	鎌	k?	鎌
1185	[number]	k4	1185
<g/>
-	-	kIx~	-
<g/>
1333	[number]	k4	1333
<g/>
)	)	kIx)	)
Muromači	Muromač	k1gInSc6	Muromač
(	(	kIx(	(
<g/>
室	室	k?	室
1333	[number]	k4	1333
<g/>
-	-	kIx~	-
<g/>
1568	[number]	k4	1568
<g/>
)	)	kIx)	)
Azuči-Momojama	Azuči-Momojamum	k1gNnSc2	Azuči-Momojamum
(	(	kIx(	(
<g/>
安	安	k?	安
1568	[number]	k4	1568
<g/>
-	-	kIx~	-
<g/>
1600	[number]	k4	1600
<g/>
)	)	kIx)	)
Tokugawa	Tokugaw	k1gInSc2	Tokugaw
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
také	také	k9	také
Edo	Eda	k1gMnSc5	Eda
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
江	江	k?	江
1600	[number]	k4	1600
<g/>
-	-	kIx~	-
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
Meidži	Meidž	k1gFnSc6	Meidž
(	(	kIx(	(
<g/>
明	明	k?	明
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
Taišó	Taišó	k1gFnSc1	Taišó
(	(	kIx(	(
<g/>
大	大	k?	大
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Šówa	Šów	k1gInSc2	Šów
(	(	kIx(	(
<g/>
昭	昭	k?	昭
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Heisei	Heisei	k1gNnSc2	Heisei
(	(	kIx(	(
<g/>
平	平	k?	平
od	od	k7c2	od
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Japonsko	Japonsko	k1gNnSc1	Japonsko
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
ranými	raný	k2eAgInPc7d1	raný
humanoidy	humanoid	k1gInPc7	humanoid
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
500	[number]	k4	500
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
starší	starý	k2eAgFnSc2d2	starší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opakujících	opakující	k2eAgInPc6d1	opakující
se	se	k3xPyFc4	se
ledových	ledový	k2eAgFnPc6d1	ledová
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Japonsko	Japonsko	k1gNnSc1	Japonsko
pravidelně	pravidelně	k6eAd1	pravidelně
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
asijským	asijský	k2eAgInSc7d1	asijský
kontinentem	kontinent	k1gInSc7	kontinent
pevninskými	pevninský	k2eAgInPc7d1	pevninský
mosty	most	k1gInPc7	most
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Sachalin	Sachalin	k1gInSc4	Sachalin
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přes	přes	k7c4	přes
Kjúšú	Kjúšú	k1gFnSc4	Kjúšú
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
umožněna	umožněn	k2eAgFnSc1d1	umožněna
migrace	migrace	k1gFnSc1	migrace
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Koreje	Korea	k1gFnSc2	Korea
na	na	k7c4	na
japonské	japonský	k2eAgInPc4d1	japonský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
koncem	konec	k1gInSc7	konec
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
a	a	k8xC	a
globálním	globální	k2eAgNnPc3d1	globální
oteplením	oteplení	k1gNnPc3	oteplení
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
-	-	kIx~	-
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
11	[number]	k4	11
tisíc	tisíc	k4xCgInSc1	tisíc
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
-	-	kIx~	-
objevila	objevit	k5eAaPmAgFnS	objevit
kultura	kultura	k1gFnSc1	kultura
Džómon	Džómon	k1gMnSc1	Džómon
(	(	kIx(	(
<g/>
Jomon	Jomon	k1gMnSc1	Jomon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
polo-kočovnou	poloočovný	k2eAgFnSc7d1	polo-kočovný
společností	společnost	k1gFnSc7	společnost
lovců	lovec	k1gMnPc2	lovec
a	a	k8xC	a
sběračů	sběrač	k1gMnPc2	sběrač
a	a	k8xC	a
výrobou	výroba	k1gFnSc7	výroba
nejstarší	starý	k2eAgFnSc2d3	nejstarší
známé	známý	k2eAgFnSc2d1	známá
keramiky	keramika	k1gFnSc2	keramika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušníci	příslušník	k1gMnPc1	příslušník
kultury	kultura	k1gFnSc2	kultura
Džómon	Džómon	k1gInSc1	Džómon
jsou	být	k5eAaImIp3nP	být
předchůdci	předchůdce	k1gMnPc1	předchůdce
prvních	první	k4xOgMnPc2	první
Japonců	Japonec	k1gMnPc2	Japonec
a	a	k8xC	a
dnešního	dnešní	k2eAgInSc2d1	dnešní
národa	národ	k1gInSc2	národ
Ainu	Ainus	k1gInSc2	Ainus
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
období	období	k1gNnSc2	období
Jajoi	Jajo	k1gFnSc2	Jajo
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
nových	nový	k2eAgFnPc2d1	nová
technik	technika	k1gFnPc2	technika
z	z	k7c2	z
asijského	asijský	k2eAgInSc2d1	asijský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc4	pěstování
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
s	s	k7c7	s
masivní	masivní	k2eAgFnSc7d1	masivní
migrací	migrace	k1gFnSc7	migrace
z	z	k7c2	z
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
částí	část	k1gFnPc2	část
Asie	Asie	k1gFnSc2	Asie
-	-	kIx~	-
z	z	k7c2	z
Korey	Korea	k1gFnSc2	Korea
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
Pekingu	Peking	k1gInSc2	Peking
a	a	k8xC	a
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
několik	několik	k4yIc1	několik
současných	současný	k2eAgNnPc2d1	současné
studií	studio	k1gNnPc2	studio
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
období	období	k1gNnSc1	období
Jajoi	Jajo	k1gFnSc2	Jajo
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
století	století	k1gNnPc2	století
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
masivní	masivní	k2eAgFnSc1d1	masivní
migrace	migrace	k1gFnSc1	migrace
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
k	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
razantního	razantní	k2eAgInSc2d1	razantní
vzrůstu	vzrůst	k1gInSc2	vzrůst
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pochází	pocházet	k5eAaImIp3nS	pocházet
první	první	k4xOgFnSc7	první
historicky	historicky	k6eAd1	historicky
doložitelný	doložitelný	k2eAgInSc4d1	doložitelný
název	název	k1gInSc4	název
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
čínský	čínský	k2eAgInSc1d1	čínský
znak	znak	k1gInSc1	znak
倭	倭	k?	倭
Wa	Wa	k1gFnSc2	Wa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradiční	tradiční	k2eAgFnSc2d1	tradiční
japonské	japonský	k2eAgFnSc2d1	japonská
mytologie	mytologie	k1gFnSc2	mytologie
bylo	být	k5eAaImAgNnS	být
Japonsko	Japonsko	k1gNnSc1	Japonsko
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c4	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
původním	původní	k2eAgMnSc7d1	původní
císařem	císař	k1gMnSc7	císař
Džimmu	Džimm	k1gInSc2	Džimm
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
a	a	k8xC	a
buddhismus	buddhismus	k1gInSc1	buddhismus
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
aspekty	aspekt	k1gInPc7	aspekt
čínské	čínský	k2eAgFnSc2d1	čínská
kultury	kultura	k1gFnSc2	kultura
nejprve	nejprve	k6eAd1	nejprve
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Císařové	Císař	k1gMnPc1	Císař
byli	být	k5eAaImAgMnP	být
formálními	formální	k2eAgMnPc7d1	formální
vládci	vládce	k1gMnPc7	vládce
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
skutečná	skutečný	k2eAgFnSc1d1	skutečná
moc	moc	k1gFnSc1	moc
byla	být	k5eAaImAgFnS	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
mocné	mocný	k2eAgFnSc2d1	mocná
dvorské	dvorský	k2eAgFnSc2d1	dvorská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
regentů	regens	k1gMnPc2	regens
nebo	nebo	k8xC	nebo
šógunů	šógun	k1gMnPc2	šógun
(	(	kIx(	(
<g/>
vojenských	vojenský	k2eAgMnPc2d1	vojenský
správců	správce	k1gMnPc2	správce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
politická	politický	k2eAgFnSc1d1	politická
struktura	struktura	k1gFnSc1	struktura
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c4	mezi
rivaly	rival	k1gMnPc4	rival
skončena	skončen	k2eAgFnSc1d1	skončena
<g/>
,	,	kIx,	,
vítězný	vítězný	k2eAgInSc1d1	vítězný
šógun	šógun	k1gInSc1	šógun
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgInS	přesunout
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Heian	Heian	k1gInSc4	Heian
(	(	kIx(	(
<g/>
plný	plný	k2eAgInSc4d1	plný
název	název	k1gInSc4	název
je	být	k5eAaImIp3nS	být
Heiankjóto	Heiankjót	k2eAgNnSc1d1	Heiankjót
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
kjóto	kjóto	k1gNnSc4	kjóto
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
a	a	k8xC	a
plný	plný	k2eAgInSc1d1	plný
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
zkrácen	zkrátit	k5eAaPmNgInS	zkrátit
jen	jen	k9	jen
na	na	k7c4	na
příponu	přípona	k1gFnSc4	přípona
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Kjóto	Kjóto	k1gNnSc4	Kjóto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
milosti	milost	k1gFnSc2	milost
císaře	císař	k1gMnSc2	císař
(	(	kIx(	(
<g/>
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
formálním	formální	k2eAgInSc7d1	formální
souhlasem	souhlas	k1gInSc7	souhlas
<g/>
)	)	kIx)	)
vládl	vládnout	k5eAaImAgInS	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1185	[number]	k4	1185
generál	generál	k1gMnSc1	generál
Minamoto	Minamota	k1gFnSc5	Minamota
no	no	k9	no
Joritomo	Joritoma	k1gFnSc5	Joritoma
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
porušil	porušit	k5eAaPmAgMnS	porušit
tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
se	se	k3xPyFc4	se
přesunout	přesunout	k5eAaPmF	přesunout
a	a	k8xC	a
následně	následně	k6eAd1	následně
vládl	vládnout	k5eAaImAgInS	vládnout
z	z	k7c2	z
Kamakury	Kamakura	k1gFnSc2	Kamakura
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Jokohamy	Jokohama	k1gFnSc2	Jokohama
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Japonci	Japonec	k1gMnPc1	Japonec
odrazili	odrazit	k5eAaPmAgMnP	odrazit
dva	dva	k4xCgInPc4	dva
mongolské	mongolský	k2eAgInPc4d1	mongolský
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Šógunát	Šógunát	k1gInSc1	Šógunát
Kamakura	Kamakur	k1gMnSc2	Kamakur
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
stabilní	stabilní	k2eAgMnSc1d1	stabilní
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Japonsko	Japonsko	k1gNnSc1	Japonsko
poměrně	poměrně	k6eAd1	poměrně
záhy	záhy	k6eAd1	záhy
upadlo	upadnout	k5eAaPmAgNnS	upadnout
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
frakcemi	frakce	k1gFnPc7	frakce
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
období	období	k1gNnSc4	období
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
období	období	k1gNnSc1	období
Sengoku	Sengok	k1gInSc2	Sengok
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
období	období	k1gNnSc1	období
válčících	válčící	k2eAgInPc2d1	válčící
států	stát	k1gInPc2	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vojevůdce	vojevůdce	k1gMnSc5	vojevůdce
Hidejoši	Hidejoš	k1gMnSc5	Hidejoš
Tojotomi	Tojoto	k1gFnPc7	Tojoto
vedl	vést	k5eAaImAgInS	vést
roku	rok	k1gInSc2	rok
1592	[number]	k4	1592
neúspěšnou	úspěšný	k2eNgFnSc4d1	neúspěšná
japonskou	japonský	k2eAgFnSc4d1	japonská
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Sekigahary	Sekigahara	k1gFnSc2	Sekigahara
šógun	šóguno	k1gNnPc2	šóguno
Tokugawa	Tokugawa	k1gMnSc1	Tokugawa
Iejasu	Iejas	k1gInSc2	Iejas
buď	buď	k8xC	buď
porazil	porazit	k5eAaPmAgMnS	porazit
nebo	nebo	k8xC	nebo
přijal	přijmout	k5eAaPmAgMnS	přijmout
za	za	k7c4	za
spojence	spojenec	k1gMnSc4	spojenec
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
nepřátele	nepřítel	k1gMnPc4	nepřítel
a	a	k8xC	a
zformoval	zformovat	k5eAaPmAgMnS	zformovat
šógunát	šógunát	k1gInSc4	šógunát
Tokugawa	Tokugaw	k1gInSc2	Tokugaw
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
rybářské	rybářský	k2eAgFnSc6d1	rybářská
vesničce	vesnička	k1gFnSc6	vesnička
Edo	Eda	k1gMnSc5	Eda
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
přepisováno	přepisovat	k5eAaImNgNnS	přepisovat
též	též	k9	též
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Jeddo	Jeddo	k1gNnSc1	Jeddo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Tokio	Tokio	k1gNnSc1	Tokio
(	(	kIx(	(
<g/>
východní	východní	k2eAgNnSc1d1	východní
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
misionáři	misionář	k1gMnPc1	misionář
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
podezříval	podezřívat	k5eAaImAgMnS	podezřívat
japonský	japonský	k2eAgInSc4d1	japonský
šógunát	šógunát	k1gInSc4	šógunát
katolické	katolický	k2eAgMnPc4d1	katolický
misionáře	misionář	k1gMnPc4	misionář
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
předvojem	předvoj	k1gInSc7	předvoj
ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
iberské	iberský	k2eAgFnSc2d1	iberská
invaze	invaze	k1gFnSc2	invaze
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
zakázal	zakázat	k5eAaPmAgInS	zakázat
veškeré	veškerý	k3xTgInPc4	veškerý
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Evropany	Evropan	k1gMnPc7	Evropan
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
významně	významně	k6eAd1	významně
omezených	omezený	k2eAgInPc2d1	omezený
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
protestantskými	protestantský	k2eAgMnPc7d1	protestantský
nizozemskými	nizozemský	k2eAgMnPc7d1	nizozemský
obchodníky	obchodník	k1gMnPc7	obchodník
na	na	k7c6	na
umělém	umělý	k2eAgInSc6d1	umělý
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
Dedžima	Dedžima	k1gFnSc1	Dedžima
(	(	kIx(	(
<g/>
také	také	k9	také
Dešima	Dešimum	k1gNnSc2	Dešimum
<g/>
)	)	kIx)	)
u	u	k7c2	u
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Čínským	čínský	k2eAgFnPc3d1	čínská
lodím	loď	k1gFnPc3	loď
bylo	být	k5eAaImAgNnS	být
nadále	nadále	k6eAd1	nadále
povoleno	povolit	k5eAaPmNgNnS	povolit
vjíždět	vjíždět	k5eAaImF	vjíždět
do	do	k7c2	do
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
a	a	k8xC	a
korejští	korejský	k2eAgMnPc1d1	korejský
vyslanci	vyslanec	k1gMnPc1	vyslanec
měli	mít	k5eAaImAgMnP	mít
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
izolace	izolace	k1gFnSc1	izolace
trvala	trvat	k5eAaImAgFnS	trvat
251	[number]	k4	251
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
si	se	k3xPyFc3	se
komodor	komodor	k1gMnSc1	komodor
Matthew	Matthew	k1gMnSc1	Matthew
Perry	Perra	k1gFnSc2	Perra
nevynutil	vynutit	k5eNaPmAgMnS	vynutit
otevření	otevření	k1gNnSc3	otevření
japonských	japonský	k2eAgMnPc2d1	japonský
přístavů	přístav	k1gInPc2	přístav
pro	pro	k7c4	pro
americké	americký	k2eAgMnPc4d1	americký
obchodníky	obchodník	k1gMnPc4	obchodník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
na	na	k7c6	na
Konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Kanagawě	Kanagawa	k1gFnSc6	Kanagawa
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
obdobných	obdobný	k2eAgFnPc2d1	obdobná
smluv	smlouva	k1gFnPc2	smlouva
(	(	kIx(	(
<g/>
Ansejské	Ansejský	k2eAgFnSc2d1	Ansejský
dohody	dohoda	k1gFnSc2	dohoda
<g/>
)	)	kIx)	)
i	i	k9	i
s	s	k7c7	s
evropskými	evropský	k2eAgFnPc7d1	Evropská
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
obnovený	obnovený	k2eAgInSc1d1	obnovený
kontakt	kontakt	k1gInSc1	kontakt
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
zásadně	zásadně	k6eAd1	zásadně
změnil	změnit	k5eAaPmAgInS	změnit
japonskou	japonský	k2eAgFnSc4d1	japonská
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Válce	válka	k1gFnSc6	válka
Bošin	Bošina	k1gFnPc2	Bošina
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1867	[number]	k4	1867
-	-	kIx~	-
1868	[number]	k4	1868
byl	být	k5eAaImAgInS	být
šógunát	šógunát	k1gInSc1	šógunát
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
znovuobnovena	znovuobnoven	k2eAgFnSc1d1	znovuobnoven
moc	moc	k1gFnSc1	moc
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nový	nový	k2eAgMnSc1d1	nový
císař	císař	k1gMnSc1	císař
Mucuhito	Mucuhit	k2eAgNnSc1d1	Mucuhit
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Meidži	Meidž	k1gFnSc6	Meidž
<g/>
)	)	kIx)	)
a	a	k8xC	a
během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
45	[number]	k4	45
<g/>
leté	letý	k2eAgFnSc2d1	letá
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
mnoho	mnoho	k4c1	mnoho
reforem	reforma	k1gFnPc2	reforma
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
reformy	reforma	k1gFnSc2	reforma
Meidži	Meidž	k1gFnSc3	Meidž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Feudální	feudální	k2eAgInSc1d1	feudální
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
převzaty	převzít	k5eAaPmNgFnP	převzít
četné	četný	k2eAgFnPc1d1	četná
západní	západní	k2eAgFnPc1d1	západní
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
západního	západní	k2eAgInSc2d1	západní
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
<g/>
,	,	kIx,	,
sociálními	sociální	k2eAgFnPc7d1	sociální
a	a	k8xC	a
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
reformami	reforma	k1gFnPc7	reforma
vyústily	vyústit	k5eAaPmAgFnP	vyústit
tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
k	k	k7c3	k
přerodu	přerod	k1gInSc2	přerod
Japonska	Japonsko	k1gNnSc2	Japonsko
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
světové	světový	k2eAgFnSc2d1	světová
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
a	a	k8xC	a
rusko-japonské	ruskoaponský	k2eAgFnSc2d1	rusko-japonská
války	válka	k1gFnSc2	válka
získalo	získat	k5eAaPmAgNnS	získat
Japonsko	Japonsko	k1gNnSc1	Japonsko
Tchaj-wan	Tchajan	k1gInSc4	Tchaj-wan
a	a	k8xC	a
polovinu	polovina	k1gFnSc4	polovina
Sachalinu	Sachalin	k1gInSc2	Sachalin
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
okupovalo	okupovat	k5eAaBmAgNnS	okupovat
Koreu	Korea	k1gFnSc4	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Japonsko	Japonsko	k1gNnSc1	Japonsko
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
většinu	většina	k1gFnSc4	většina
Mikronésie	Mikronésie	k1gFnSc2	Mikronésie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
Japonsko	Japonsko	k1gNnSc1	Japonsko
vzrůstající	vzrůstající	k2eAgNnSc1d1	vzrůstající
vliv	vliv	k1gInSc4	vliv
expanzivního	expanzivní	k2eAgInSc2d1	expanzivní
militarismu	militarismus	k1gInSc2	militarismus
<g/>
,	,	kIx,	,
vedoucímu	vedoucí	k1gMnSc3	vedoucí
k	k	k7c3	k
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
čínsko-japonské	čínskoaponský	k2eAgFnSc3d1	čínsko-japonská
válce	válka	k1gFnSc3	válka
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
spojilo	spojit	k5eAaPmAgNnS	spojit
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
Osu	osa	k1gFnSc4	osa
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
trpělo	trpět	k5eAaImAgNnS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
strategických	strategický	k2eAgFnPc2d1	strategická
surovin	surovina	k1gFnPc2	surovina
(	(	kIx(	(
<g/>
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
,	,	kIx,	,
barevných	barevný	k2eAgInPc2d1	barevný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
muselo	muset	k5eAaImAgNnS	muset
dovážet	dovážet	k5eAaImF	dovážet
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Japonsko	Japonsko	k1gNnSc1	Japonsko
zajímalo	zajímat	k5eAaImAgNnS	zajímat
i	i	k9	i
o	o	k7c4	o
bohatá	bohatý	k2eAgNnPc4d1	bohaté
naleziště	naleziště	k1gNnPc4	naleziště
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
obsazení	obsazení	k1gNnSc6	obsazení
leteckých	letecký	k2eAgFnPc2d1	letecká
a	a	k8xC	a
námořních	námořní	k2eAgFnPc2d1	námořní
základen	základna	k1gFnPc2	základna
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Indočíně	Indočína	k1gFnSc6	Indočína
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
Japonsko	Japonsko	k1gNnSc4	Japonsko
uvaleno	uvalen	k2eAgNnSc1d1	uvaleno
embargo	embargo	k1gNnSc1	embargo
a	a	k8xC	a
USA	USA	kA	USA
zmrazily	zmrazit	k5eAaPmAgFnP	zmrazit
japonská	japonský	k2eAgNnPc1d1	Japonské
aktiva	aktivum	k1gNnPc1	aktivum
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
bankách	banka	k1gFnPc6	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
expanzí	expanze	k1gFnSc7	expanze
do	do	k7c2	do
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Pacifiku	Pacifik	k1gInSc2	Pacifik
považovali	považovat	k5eAaImAgMnP	považovat
někteří	některý	k3yIgMnPc1	některý
japonští	japonský	k2eAgMnPc1d1	japonský
vůdci	vůdce	k1gMnPc1	vůdce
za	za	k7c4	za
nezbytné	nezbytný	k2eAgNnSc4d1	nezbytný
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
námořní	námořní	k2eAgFnSc4d1	námořní
základnu	základna	k1gFnSc4	základna
v	v	k7c4	v
Pearl	Pearl	k1gInSc4	Pearl
Harbor	Harbor	k1gInSc1	Harbor
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
japonská	japonský	k2eAgFnSc1d1	japonská
nadvláda	nadvláda	k1gFnSc1	nadvláda
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vstup	vstup	k1gInSc1	vstup
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
postupně	postupně	k6eAd1	postupně
změnil	změnit	k5eAaPmAgInS	změnit
rovnováhu	rovnováha	k1gFnSc4	rovnováha
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
pacifickém	pacifický	k2eAgNnSc6d1	pacifické
tažení	tažení	k1gNnSc6	tažení
se	se	k3xPyFc4	se
Spojenci	spojenec	k1gMnPc1	spojenec
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
k	k	k7c3	k
japonským	japonský	k2eAgMnPc3d1	japonský
ostrovům	ostrov	k1gInPc3	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mohutně	mohutně	k6eAd1	mohutně
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
na	na	k7c4	na
Tokio	Tokio	k1gNnSc4	Tokio
<g/>
,	,	kIx,	,
Ósaku	Ósaka	k1gFnSc4	Ósaka
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
strategickým	strategický	k2eAgNnSc7d1	strategické
bombardováním	bombardování	k1gNnSc7	bombardování
a	a	k8xC	a
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc1	Nagasaki
atomovými	atomový	k2eAgFnPc7d1	atomová
bombami	bomba	k1gFnPc7	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
bezpodmínečně	bezpodmínečně	k6eAd1	bezpodmínečně
kapitulovalo	kapitulovat	k5eAaBmAgNnS	kapitulovat
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
v	v	k7c6	v
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
dopouštěla	dopouštět	k5eAaImAgFnS	dopouštět
válečných	válečná	k1gFnPc2	válečná
zločinů	zločin	k1gMnPc2	zločin
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Nankingu	Nanking	k1gInSc6	Nanking
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
až	až	k6eAd1	až
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
japonské	japonský	k2eAgFnSc2d1	japonská
invaze	invaze	k1gFnSc2	invaze
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
asi	asi	k9	asi
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
zahynuly	zahynout	k5eAaPmAgInP	zahynout
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
Japonců	Japonec	k1gMnPc2	Japonec
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1945	[number]	k4	1945
při	při	k7c6	při
americkém	americký	k2eAgInSc6d1	americký
náletu	nálet	k1gInSc6	nálet
na	na	k7c4	na
Tokio	Tokio	k1gNnSc4	Tokio
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
na	na	k7c4	na
110	[number]	k4	110
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
Japonsko	Japonsko	k1gNnSc1	Japonsko
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgFnPc2	svůj
zámořských	zámořský	k2eAgFnPc2d1	zámořská
držav	država	k1gFnPc2	država
a	a	k8xC	a
miliony	milion	k4xCgInPc1	milion
Japonců	Japonec	k1gMnPc2	Japonec
byly	být	k5eAaImAgInP	být
vysídleny	vysídlit	k5eAaPmNgInP	vysídlit
z	z	k7c2	z
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
a	a	k8xC	a
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Mandžusko	Mandžusko	k1gNnSc1	Mandžusko
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
Sachalin	Sachalin	k1gInSc1	Sachalin
a	a	k8xC	a
Kurily	Kurily	k1gFnPc1	Kurily
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Mariany	Mariana	k1gFnSc2	Mariana
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poražené	poražený	k2eAgNnSc1d1	poražené
Japonsko	Japonsko	k1gNnSc1	Japonsko
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
pod	pod	k7c7	pod
okupační	okupační	k2eAgFnSc7d1	okupační
správou	správa	k1gFnSc7	správa
USA	USA	kA	USA
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
skončení	skončení	k1gNnSc6	skončení
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
významné	významný	k2eAgNnSc1d1	významné
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
oživení	oživení	k1gNnSc1	oživení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
ostrovům	ostrov	k1gInPc3	ostrov
prosperitu	prosperita	k1gFnSc4	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Japonsko	Japonsko	k1gNnSc1	Japonsko
znovu	znovu	k6eAd1	znovu
získalo	získat	k5eAaPmAgNnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
status	status	k1gInSc4	status
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
Rjúkjú	Rjúkjú	k1gNnSc2	Rjúkjú
zůstaly	zůstat	k5eAaPmAgInP	zůstat
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
USA	USA	kA	USA
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
stabilizace	stabilizace	k1gFnSc1	stabilizace
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
přítomnost	přítomnost	k1gFnSc1	přítomnost
USA	USA	kA	USA
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
významná	významný	k2eAgFnSc1d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
asijského	asijský	k2eAgInSc2d1	asijský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
řetězem	řetěz	k1gInSc7	řetěz
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
)	)	kIx)	)
Hokkaidó	Hokkaidó	k1gFnSc1	Hokkaidó
(	(	kIx(	(
<g/>
北	北	k?	北
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Honšú	Honšú	k1gFnSc1	Honšú
(	(	kIx(	(
<g/>
本	本	k?	本
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šikoku	Šikok	k1gInSc2	Šikok
(	(	kIx(	(
<g/>
四	四	k?	四
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kjúšú	Kjúšú	k1gFnSc1	Kjúšú
(	(	kIx(	(
<g/>
九	九	k?	九
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
ostrovům	ostrov	k1gInPc3	ostrov
patří	patřit	k5eAaImIp3nS	patřit
i	i	k8xC	i
skupina	skupina	k1gFnSc1	skupina
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
i	i	k9	i
ostrovy	ostrov	k1gInPc1	ostrov
více	hodně	k6eAd2	hodně
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
např.	např.	kA	např.
Okinawa	Okinawa	k1gFnSc1	Okinawa
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
všech	všecek	k3xTgInPc2	všecek
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
33	[number]	k4	33
889	[number]	k4	889
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Fudži	Fudž	k1gFnSc3	Fudž
(	(	kIx(	(
<g/>
3776	[number]	k4	3776
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
hory	hora	k1gFnPc1	hora
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
japonských	japonský	k2eAgFnPc2d1	japonská
třítisícovek	třítisícovka	k1gFnPc2	třítisícovka
<g/>
,	,	kIx,	,
nejprominentnější	prominentní	k2eAgFnPc1d3	nejprominentnější
hory	hora	k1gFnPc1	hora
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
japonských	japonský	k2eAgInPc2d1	japonský
ultraprominentních	ultraprominentní	k2eAgInPc2d1	ultraprominentní
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
;	;	kIx,	;
Japonsko	Japonsko	k1gNnSc1	Japonsko
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
hustoty	hustota	k1gFnPc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	slabý	k2eAgNnPc1d1	slabé
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
jsou	být	k5eAaImIp3nP	být
častá	častý	k2eAgNnPc1d1	časté
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
tří	tři	k4xCgFnPc2	tři
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
se	se	k3xPyFc4	se
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
během	během	k7c2	během
každého	každý	k3xTgNnSc2	každý
století	století	k1gNnSc2	století
několikrát	několikrát	k6eAd1	několikrát
a	a	k8xC	a
často	často	k6eAd1	často
také	také	k9	také
vyvolají	vyvolat	k5eAaPmIp3nP	vyvolat
vlny	vlna	k1gFnPc1	vlna
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
velká	velký	k2eAgNnPc1d1	velké
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923	[number]	k4	1923
(	(	kIx(	(
<g/>
Velké	velký	k2eAgNnSc1d1	velké
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
Kantó	Kantó	k1gFnSc6	Kantó
<g/>
,	,	kIx,	,
8,3	[number]	k4	8,3
stupně	stupeň	k1gInSc2	stupeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
Kóbe	Kóbe	k1gNnSc6	Kóbe
<g/>
,	,	kIx,	,
7,2	[number]	k4	7,2
stupně	stupeň	k1gInSc2	stupeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc4	oblast
Čúecu	Čúecus	k1gInSc2	Čúecus
<g/>
,	,	kIx,	,
6,9	[number]	k4	6,9
stupně	stupeň	k1gInSc2	stupeň
<g/>
)	)	kIx)	)
a	a	k8xC	a
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
a	a	k8xC	a
tsunami	tsunami	k1gNnSc1	tsunami
v	v	k7c6	v
Tóhoku	Tóhok	k1gInSc6	Tóhok
<g/>
,	,	kIx,	,
8,9	[number]	k4	8,9
stupně	stupeň	k1gInSc2	stupeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
připravilo	připravit	k5eAaPmAgNnS	připravit
největší	veliký	k2eAgFnSc4d3	veliký
katastrofu	katastrofa	k1gFnSc4	katastrofa
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
až	až	k9	až
na	na	k7c4	na
11	[number]	k4	11
000	[number]	k4	000
a	a	k8xC	a
čtvrt	čtvrt	k1xP	čtvrt
miliónu	milión	k4xCgInSc2	milión
lidí	člověk	k1gMnPc2	člověk
připravily	připravit	k5eAaPmAgInP	připravit
živly	živel	k1gInPc1	živel
o	o	k7c4	o
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
Japonska	Japonsko	k1gNnSc2	Japonsko
je	být	k5eAaImIp3nS	být
oceánické	oceánický	k2eAgNnSc1d1	oceánické
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgInPc1d1	vlhký
a	a	k8xC	a
monzunové	monzunový	k2eAgInPc1d1	monzunový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
rozloze	rozloha	k1gFnSc3	rozloha
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
klima	klima	k1gNnSc1	klima
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
Okinawě	Okinawa	k1gFnSc6	Okinawa
počátkem	počátkem	k7c2	počátkem
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Honšú	Honšú	k1gFnSc2	Honšú
pak	pak	k6eAd1	pak
uprostřed	uprostřed	k7c2	uprostřed
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
trvá	trvat	k5eAaImIp3nS	trvat
cca	cca	kA	cca
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
podzimu	podzim	k1gInSc2	podzim
přinášejí	přinášet	k5eAaImIp3nP	přinášet
silné	silný	k2eAgInPc1d1	silný
deště	dešť	k1gInPc1	dešť
také	také	k9	také
tajfuny	tajfun	k1gInPc4	tajfun
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc1d1	konstituční
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
moc	moc	k1gFnSc1	moc
císaře	císař	k1gMnSc2	císař
je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
<g/>
,	,	kIx,	,
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
definuje	definovat	k5eAaBmIp3nS	definovat
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Císař	Císař	k1gMnSc1	Císař
je	být	k5eAaImIp3nS	být
symbol	symbol	k1gInSc4	symbol
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jednoty	jednota	k1gFnSc2	jednota
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
od	od	k7c2	od
vůle	vůle	k1gFnSc2	vůle
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
tkví	tkvět	k5eAaImIp3nS	tkvět
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
moc	moc	k1gFnSc1	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímo	přímo	k6eNd1	přímo
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Císař	Císař	k1gMnSc1	Císař
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
pozemský	pozemský	k2eAgMnSc1d1	pozemský
potomek	potomek	k1gMnSc1	potomek
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžný	běžný	k2eAgMnSc1d1	běžný
monarcha	monarcha	k1gMnSc1	monarcha
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
prohlášení	prohlášení	k1gNnSc4	prohlášení
učinil	učinit	k5eAaPmAgMnS	učinit
císař	císař	k1gMnSc1	císař
Hirohito	Hirohit	k2eAgNnSc4d1	Hirohito
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
převážně	převážně	k6eAd1	převážně
japonský	japonský	k2eAgMnSc1d1	japonský
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
zvolení	zvolený	k2eAgMnPc1d1	zvolený
členové	člen	k1gMnPc1	člen
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
svoboda	svoboda	k1gFnSc1	svoboda
volby	volba	k1gFnSc2	volba
spočívá	spočívat	k5eAaImIp3nS	spočívat
na	na	k7c6	na
japonských	japonský	k2eAgMnPc6d1	japonský
občanech	občan	k1gMnPc6	občan
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
působí	působit	k5eAaImIp3nS	působit
především	především	k6eAd1	především
jako	jako	k9	jako
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
při	při	k7c6	při
diplomatických	diplomatický	k2eAgFnPc6d1	diplomatická
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
císařem	císař	k1gMnSc7	císař
je	být	k5eAaImIp3nS	být
Akihito	Akihit	k2eAgNnSc1d1	Akihito
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
je	být	k5eAaImIp3nS	být
Naruhito	Naruhit	k2eAgNnSc1d1	Naruhito
<g/>
,	,	kIx,	,
korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
Japonský	japonský	k2eAgInSc1d1	japonský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
480	[number]	k4	480
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
volených	volený	k2eAgInPc2d1	volený
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
nebo	nebo	k8xC	nebo
při	při	k7c6	při
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
a	a	k8xC	a
Sněmovny	sněmovna	k1gFnPc1	sněmovna
rádců	rádce	k1gMnPc2	rádce
s	s	k7c7	s
242	[number]	k4	242
členy	člen	k1gMnPc7	člen
<g/>
,	,	kIx,	,
volených	volený	k2eAgMnPc2d1	volený
na	na	k7c4	na
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgNnSc4d1	aktivní
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
mají	mít	k5eAaImIp3nP	mít
osoby	osoba	k1gFnPc1	osoba
starší	starý	k2eAgFnPc1d2	starší
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
se	se	k3xPyFc4	se
tajně	tajně	k6eAd1	tajně
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
zastupitelských	zastupitelský	k2eAgInPc2d1	zastupitelský
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Liberálně	liberálně	k6eAd1	liberálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
LDP	LDP	kA	LDP
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
krátkodobé	krátkodobý	k2eAgFnSc2d1	krátkodobá
koaliční	koaliční	k2eAgFnSc2d1	koaliční
vlády	vláda	k1gFnSc2	vláda
zformované	zformovaný	k2eAgInPc1d1	zformovaný
opozičními	opoziční	k2eAgFnPc7d1	opoziční
stranami	strana	k1gFnPc7	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
opoziční	opoziční	k2eAgFnSc7d1	opoziční
stranou	strana	k1gFnSc7	strana
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
sociálně	sociálně	k6eAd1	sociálně
liberální	liberální	k2eAgFnSc1d1	liberální
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
Japonska	Japonsko	k1gNnSc2	Japonsko
(	(	kIx(	(
<g/>
DPJ	DPJ	kA	DPJ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
náskokem	náskok	k1gInSc7	náskok
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaImNgInS	jmenovat
japonským	japonský	k2eAgMnSc7d1	japonský
císařem	císař	k1gMnSc7	císař
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
a	a	k8xC	a
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
mu	on	k3xPp3gMnSc3	on
musí	muset	k5eAaImIp3nS	muset
vyslovit	vyslovit	k5eAaPmF	vyslovit
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
důležité	důležitý	k2eAgFnPc1d1	důležitá
pravomoci	pravomoc	k1gFnPc1	pravomoc
patří	patřit	k5eAaImIp3nP	patřit
jmenování	jmenování	k1gNnSc4	jmenování
a	a	k8xC	a
odvolávání	odvolávání	k1gNnSc4	odvolávání
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
členy	člen	k1gMnPc4	člen
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
japonské	japonský	k2eAgFnSc2d1	japonská
vlády	vláda	k1gFnSc2	vláda
Šinzó	Šinzó	k1gMnSc1	Šinzó
Abe	Abe	k1gMnSc1	Abe
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
byl	být	k5eAaImAgInS	být
japonský	japonský	k2eAgInSc1d1	japonský
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
hlavně	hlavně	k9	hlavně
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
právní	právní	k2eAgInSc4d1	právní
systém	systém	k1gInSc4	systém
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
evropském	evropský	k2eAgNnSc6d1	Evropské
právu	právo	k1gNnSc6	právo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
na	na	k7c6	na
systému	systém	k1gInSc6	systém
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
japonská	japonský	k2eAgFnSc1d1	japonská
vláda	vláda	k1gFnSc1	vláda
zavedla	zavést	k5eAaPmAgFnS	zavést
občanský	občanský	k2eAgInSc4d1	občanský
zákoník	zákoník	k1gInSc4	zákoník
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
německém	německý	k2eAgInSc6d1	německý
modelu	model	k1gInSc6	model
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
poválečnými	poválečný	k2eAgFnPc7d1	poválečná
změnami	změna	k1gFnPc7	změna
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
tento	tento	k3xDgInSc1	tento
zákoník	zákoník	k1gInSc1	zákoník
platný	platný	k2eAgInSc1d1	platný
i	i	k9	i
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
náleží	náležet	k5eAaImIp3nS	náležet
japonskému	japonský	k2eAgNnSc3d1	Japonské
Národnímu	národní	k2eAgNnSc3d1	národní
shromáždění	shromáždění	k1gNnSc3	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
ústava	ústava	k1gFnSc1	ústava
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
císař	císař	k1gMnSc1	císař
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
legislativu	legislativa	k1gFnSc4	legislativa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
dávala	dávat	k5eAaImAgFnS	dávat
pravomoc	pravomoc	k1gFnSc4	pravomoc
zákon	zákon	k1gInSc1	zákon
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
soudní	soudní	k2eAgMnSc1d1	soudní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
4	[number]	k4	4
základních	základní	k2eAgInPc2d1	základní
stupňů	stupeň	k1gInPc2	stupeň
<g/>
:	:	kIx,	:
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
a	a	k8xC	a
3	[number]	k4	3
stupně	stupeň	k1gInSc2	stupeň
nižších	nízký	k2eAgMnPc2d2	nižší
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
a	a	k8xC	a
nestálým	stálý	k2eNgMnSc7d1	nestálý
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
G4	G4	k1gFnSc2	G4
-	-	kIx~	-
Brazílií	Brazílie	k1gFnSc7	Brazílie
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Indií	Indie	k1gFnSc7	Indie
<g/>
,	,	kIx,	,
uchází	ucházet	k5eAaImIp3nS	ucházet
o	o	k7c4	o
stálá	stálý	k2eAgNnPc4d1	stálé
křesla	křeslo	k1gNnPc4	křeslo
v	v	k7c6	v
bezpečnostní	bezpečnostní	k2eAgFnSc6d1	bezpečnostní
radě	rada	k1gFnSc6	rada
<g/>
..	..	k?	..
Jednou	jednou	k9	jednou
z	z	k7c2	z
podmínek	podmínka	k1gFnPc2	podmínka
kapitulace	kapitulace	k1gFnSc2	kapitulace
po	po	k7c4	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
9	[number]	k4	9
<g/>
.	.	kIx.	.
ústavy	ústava	k1gFnSc2	ústava
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
vlastní	vlastní	k2eAgFnPc4d1	vlastní
armády	armáda	k1gFnPc4	armáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ústava	ústava	k1gFnSc1	ústava
nebyla	být	k5eNaImAgFnS	být
porušena	porušit	k5eAaPmNgFnS	porušit
<g/>
,	,	kIx,	,
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
založilo	založit	k5eAaPmAgNnS	založit
tzv.	tzv.	kA	tzv.
síly	síla	k1gFnSc2	síla
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
pozemní	pozemní	k2eAgMnPc4d1	pozemní
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgFnPc1d1	námořní
a	a	k8xC	a
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
a	a	k8xC	a
čítají	čítat	k5eAaImIp3nP	čítat
asi	asi	k9	asi
238	[number]	k4	238
600	[number]	k4	600
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
Japonsko	Japonsko	k1gNnSc1	Japonsko
členem	člen	k1gInSc7	člen
skupin	skupina	k1gFnPc2	skupina
G8	G8	k1gFnPc2	G8
a	a	k8xC	a
APEC	APEC	kA	APEC
a	a	k8xC	a
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
velké	velký	k2eAgInPc4d1	velký
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
rozvojové	rozvojový	k2eAgInPc4d1	rozvojový
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
vede	vést	k5eAaImIp3nS	vést
teritoriální	teritoriální	k2eAgInPc4d1	teritoriální
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
ohledně	ohledně	k7c2	ohledně
kontroly	kontrola	k1gFnSc2	kontrola
námořních	námořní	k2eAgInPc2d1	námořní
a	a	k8xC	a
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
jako	jako	k8xC	jako
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
také	také	k9	také
si	se	k3xPyFc3	se
neudržuje	udržovat	k5eNaImIp3nS	udržovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Koreou	Korea	k1gFnSc7	Korea
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
rozpory	rozpor	k1gInPc1	rozpor
ohledně	ohledně	k7c2	ohledně
únosu	únos	k1gInSc2	únos
japonských	japonský	k2eAgMnPc2d1	japonský
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
programu	program	k1gInSc2	program
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
47	[number]	k4	47
prefektur	prefektura	k1gFnPc2	prefektura
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
nezná	neznat	k5eAaImIp3nS	neznat
pojem	pojem	k1gInSc1	pojem
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
za	za	k7c2	za
něj	on	k3xPp3gNnSc2	on
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
označována	označován	k2eAgFnSc1d1	označována
prefektura	prefektura	k1gFnSc1	prefektura
Tokio	Tokio	k1gNnSc4	Tokio
(	(	kIx(	(
<g/>
東	東	k?	東
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ztratila	ztratit	k5eAaPmAgFnS	ztratit
postavení	postavení	k1gNnSc4	postavení
města	město	k1gNnSc2	město
při	při	k7c6	při
změnách	změna	k1gFnPc6	změna
systému	systém	k1gInSc2	systém
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Prefektura	prefektura	k1gFnSc1	prefektura
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
9,3	[number]	k4	9,3
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
venkovských	venkovský	k2eAgFnPc2d1	venkovská
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
administrativně	administrativně	k6eAd1	administrativně
pod	pod	k7c4	pod
ni	on	k3xPp3gFnSc4	on
spadajících	spadající	k2eAgNnPc2d1	spadající
odlehlých	odlehlý	k2eAgNnPc2d1	odlehlé
souostroví	souostroví	k1gNnPc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
prefektury	prefektura	k1gFnSc2	prefektura
Tokio	Tokio	k1gNnSc4	Tokio
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vládní	vládní	k2eAgFnSc2d1	vládní
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
císařský	císařský	k2eAgInSc4d1	císařský
palác	palác	k1gInSc4	palác
a	a	k8xC	a
centrum	centrum	k1gNnSc4	centrum
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
třetina	třetina	k1gFnSc1	třetina
prefektury	prefektura	k1gFnSc2	prefektura
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
souvislého	souvislý	k2eAgNnSc2d1	souvislé
osídlení	osídlení	k1gNnSc2	osídlení
tvořená	tvořený	k2eAgFnSc1d1	tvořená
cca	cca	kA	cca
6	[number]	k4	6
prefekturami	prefektura	k1gFnPc7	prefektura
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
37	[number]	k4	37
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
vymezení	vymezení	k1gNnPc2	vymezení
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
spadá	spadat	k5eAaPmIp3nS	spadat
i	i	k9	i
Jokohama	Jokohama	k1gFnSc1	Jokohama
<g/>
,	,	kIx,	,
uvádená	uvádený	k2eAgFnSc1d1	uvádený
níže	níže	k1gFnSc1	níže
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Tokia	Tokio	k1gNnSc2	Tokio
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jokohama	Jokohama	k1gFnSc1	Jokohama
(	(	kIx(	(
<g/>
横	横	k?	横
3,5	[number]	k4	3,5
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Ósaka	Ósaka	k1gFnSc1	Ósaka
(	(	kIx(	(
<g/>
大	大	k?	大
2,6	[number]	k4	2,6
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Nagoja	Nagoja	k1gMnSc1	Nagoja
(	(	kIx(	(
<g/>
名	名	k?	名
2,2	[number]	k4	2,2
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Sapporo	Sappora	k1gFnSc5	Sappora
(	(	kIx(	(
<g/>
札	札	k?	札
1,8	[number]	k4	1,8
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Kóbe	Kóbe	k1gNnSc1	Kóbe
(	(	kIx(	(
<g/>
神	神	k?	神
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Kjóto	Kjóto	k1gNnSc1	Kjóto
(	(	kIx(	(
<g/>
京	京	k?	京
1,5	[number]	k4	1,5
<g />
.	.	kIx.	.
</s>
<s>
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Fukuoka	Fukuoka	k1gMnSc1	Fukuoka
(	(	kIx(	(
<g/>
福	福	k?	福
1,4	[number]	k4	1,4
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Hirošima	Hirošima	k1gFnSc1	Hirošima
(	(	kIx(	(
<g/>
広	広	k?	広
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Hokkaidó	Hokkaidó	k1gMnPc2	Hokkaidó
(	(	kIx(	(
<g/>
北	北	k?	北
<g/>
)	)	kIx)	)
Honšú	Honšú	k1gMnSc1	Honšú
(	(	kIx(	(
<g/>
本	本	k?	本
<g/>
)	)	kIx)	)
Šikoku	Šikok	k1gInSc2	Šikok
(	(	kIx(	(
<g/>
四	四	k?	四
<g/>
)	)	kIx)	)
Kjúšú	Kjúšú	k1gMnSc1	Kjúšú
(	(	kIx(	(
<g/>
九	九	k?	九
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
asijskou	asijský	k2eAgFnSc4d1	asijská
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
velmoc	velmoc	k1gFnSc4	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
růst	růst	k1gInSc4	růst
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
rostla	růst	k5eAaImAgFnS	růst
průměrně	průměrně	k6eAd1	průměrně
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
o	o	k7c4	o
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
průmysl	průmysl	k1gInSc4	průmysl
-	-	kIx~	-
výrobu	výroba	k1gFnSc4	výroba
automobilů	automobil	k1gInPc2	automobil
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgMnSc1d3	veliký
výrobce	výrobce	k1gMnSc1	výrobce
automobilů	automobil	k1gInPc2	automobil
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
třetí	třetí	k4xOgInPc4	třetí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
ze	z	k7c2	z
vzdělané	vzdělaný	k2eAgFnSc2d1	vzdělaná
a	a	k8xC	a
disciplinované	disciplinovaný	k2eAgFnSc2d1	disciplinovaná
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
trápí	trápit	k5eAaImIp3nS	trápit
deflace	deflace	k1gFnSc1	deflace
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Japonska	Japonsko	k1gNnSc2	Japonsko
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
127,1	[number]	k4	127,1
milionu	milion	k4xCgInSc2	milion
<g/>
,	,	kIx,	,
s	s	k7c7	s
80	[number]	k4	80
<g/>
%	%	kIx~	%
žijícími	žijící	k2eAgInPc7d1	žijící
na	na	k7c4	na
Honšú	Honšú	k1gMnPc4	Honšú
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
jazykově	jazykově	k6eAd1	jazykově
i	i	k9	i
kulturně	kulturně	k6eAd1	kulturně
homogenní	homogenní	k2eAgFnSc1d1	homogenní
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
98,5	[number]	k4	98,5
<g/>
%	%	kIx~	%
etnických	etnický	k2eAgMnPc2d1	etnický
Japonců	Japonec	k1gMnPc2	Japonec
<g/>
,	,	kIx,	,
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
počtem	počet	k1gInSc7	počet
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
muži	muž	k1gMnPc1	muž
<g/>
:	:	kIx,	:
62,252	[number]	k4	62,252
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
ženy	žena	k1gFnPc1	žena
<g/>
:	:	kIx,	:
65,183	[number]	k4	65,183
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
činné	činný	k2eAgNnSc1d1	činné
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
:	:	kIx,	:
66,990	[number]	k4	66,990
miliónu	milión	k4xCgInSc2	milión
(	(	kIx(	(
<g/>
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Absolutní	absolutní	k2eAgInSc1d1	absolutní
roční	roční	k2eAgInSc1d1	roční
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
cca	cca	kA	cca
145	[number]	k4	145
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Relativní	relativní	k2eAgInSc4d1	relativní
průměrný	průměrný	k2eAgInSc4d1	průměrný
roční	roční	k2eAgInSc4d1	roční
přírůstek	přírůstek	k1gInSc4	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
0,11	[number]	k4	0,11
%	%	kIx~	%
Demografické	demografický	k2eAgFnSc2d1	demografická
<g />
.	.	kIx.	.
</s>
<s>
složení	složení	k1gNnSc1	složení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
14,2	[number]	k4	14,2
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
osoby	osoba	k1gFnSc2	osoba
nad	nad	k7c4	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
18,5	[number]	k4	18,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
dožití	dožití	k1gNnSc2	dožití
<g/>
:	:	kIx,	:
muži	muž	k1gMnPc1	muž
77,73	[number]	k4	77,73
let	léto	k1gNnPc2	léto
ženy	žena	k1gFnPc4	žena
84,64	[number]	k4	84,64
let	léto	k1gNnPc2	léto
Národnostní	národnostní	k2eAgNnSc1d1	národnostní
složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Japonci	Japonec	k1gMnPc1	Japonec
(	(	kIx(	(
<g/>
98,3	[number]	k4	98,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
ostatní	ostatní	k2eAgFnPc4d1	ostatní
národnosti	národnost	k1gFnPc4	národnost
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Korejci	Korejec	k1gMnPc1	Korejec
(	(	kIx(	(
<g/>
632	[number]	k4	632
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Číňané	Číňan	k1gMnPc1	Číňan
(	(	kIx(	(
<g/>
381	[number]	k4	381
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Brazilci	Brazilec	k1gMnPc1	Brazilec
(	(	kIx(	(
<g/>
265	[number]	k4	265
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Filipínci	Filipínec	k1gMnSc3	Filipínec
(	(	kIx(	(
<g/>
157	[number]	k4	157
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
46	[number]	k4	46
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Peruánci	Peruánec	k1gMnSc3	Peruánec
(	(	kIx(	(
<g/>
50	[number]	k4	50
tis	tis	k1gInSc1	tis
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
)	)	kIx)	)
Thajci	Thajce	k1gMnSc6	Thajce
(	(	kIx(	(
<g/>
24	[number]	k4	24
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Britové	Brit	k1gMnPc1	Brit
(	(	kIx(	(
<g/>
15	[number]	k4	15
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
(	(	kIx(	(
<g/>
14	[number]	k4	14
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Celkem	celkem	k6eAd1	celkem
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
přes	přes	k7c4	přes
1,78	[number]	k4	1,78
milionů	milion	k4xCgInPc2	milion
nejaponského	japonský	k2eNgInSc2d1	nejaponský
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
japonskými	japonský	k2eAgFnPc7d1	japonská
náboženstvími	náboženství	k1gNnPc7	náboženství
jsou	být	k5eAaImIp3nP	být
šintoismus	šintoismus	k1gInSc1	šintoismus
a	a	k8xC	a
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Specifikou	specifika	k1gFnSc7	specifika
Japonska	Japonsko	k1gNnSc2	Japonsko
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
oběma	dva	k4xCgInPc3	dva
hlavním	hlavní	k2eAgInPc3d1	hlavní
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistického	statistický	k2eAgInSc2d1	statistický
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
,	,	kIx,	,
provedeného	provedený	k2eAgInSc2d1	provedený
Japonským	japonský	k2eAgNnSc7d1	Japonské
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
文	文	k?	文
[	[	kIx(	[
<g/>
Monbu-kagaku-šó	Monbuagaku-šó	k1gMnSc1	Monbu-kagaku-šó
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
MEXT	MEXT	kA	MEXT
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
přibližné	přibližný	k2eAgInPc4d1	přibližný
počty	počet	k1gInPc4	počet
věřících	věřící	k1gMnPc2	věřící
<g/>
:	:	kIx,	:
šintoismus	šintoismus	k1gInSc1	šintoismus
-	-	kIx~	-
107	[number]	k4	107
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
budhismus	budhismus	k1gInSc1	budhismus
-	-	kIx~	-
89	[number]	k4	89
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
křesťanů	křesťan	k1gMnPc2	křesťan
-	-	kIx~	-
3	[number]	k4	3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
10	[number]	k4	10
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
údaje	údaj	k1gInPc4	údaj
cca	cca	kA	cca
70	[number]	k4	70
%	%	kIx~	%
šintoisté	šintoista	k1gMnPc1	šintoista
<g/>
,	,	kIx,	,
30	[number]	k4	30
%	%	kIx~	%
buddhisté	buddhista	k1gMnPc1	buddhista
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
údaje	údaj	k1gInPc4	údaj
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
zavádějící	zavádějící	k2eAgNnSc4d1	zavádějící
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
většina	většina	k1gFnSc1	většina
Japonců	Japonec	k1gMnPc2	Japonec
pouze	pouze	k6eAd1	pouze
udržuje	udržovat	k5eAaImIp3nS	udržovat
některé	některý	k3yIgInPc4	některý
národní	národní	k2eAgInPc4d1	národní
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnPc4	tradice
a	a	k8xC	a
pověry	pověra	k1gFnPc4	pověra
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
srovnat	srovnat	k5eAaPmF	srovnat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
český	český	k2eAgMnSc1d1	český
ateista	ateista	k1gMnSc1	ateista
slaví	slavit	k5eAaImIp3nS	slavit
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
šintoistické	šintoistický	k2eAgFnSc6d1	šintoistická
svatyni	svatyně	k1gFnSc6	svatyně
koutek	koutek	k1gInSc1	koutek
pro	pro	k7c4	pro
buddhisty	buddhista	k1gMnPc4	buddhista
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
1	[number]	k4	1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
údajů	údaj	k1gInPc2	údaj
až	až	k9	až
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
také	také	k9	také
působí	působit	k5eAaImIp3nP	působit
mnoho	mnoho	k4c4	mnoho
sekt	sekta	k1gFnPc2	sekta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tato	tento	k3xDgNnPc1	tento
náboženství	náboženství	k1gNnPc1	náboženství
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
sekty	sekt	k1gInPc4	sekt
vyznávající	vyznávající	k2eAgInPc4d1	vyznávající
boha	bůh	k1gMnSc2	bůh
Kušizaka	Kušizak	k1gMnSc2	Kušizak
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
svým	svůj	k3xOyFgInSc7	svůj
velmi	velmi	k6eAd1	velmi
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
tělesné	tělesný	k2eAgFnSc3d1	tělesná
lásce	láska	k1gFnSc3	láska
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kultura	kultura	k1gFnSc1	kultura
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgNnSc1d1	známé
je	on	k3xPp3gNnSc4	on
japonské	japonský	k2eAgNnSc4d1	Japonské
umění	umění	k1gNnSc4	umění
krásného	krásný	k2eAgNnSc2d1	krásné
písma	písmo	k1gNnSc2	písmo
-	-	kIx~	-
kaligrafie	kaligrafie	k1gFnPc1	kaligrafie
<g/>
,	,	kIx,	,
komiksy	komiks	k1gInPc1	komiks
-	-	kIx~	-
manga	mango	k1gNnPc1	mango
(	(	kIx(	(
<g/>
čtou	číst	k5eAaImIp3nP	číst
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Japonsko	Japonsko	k1gNnSc4	Japonsko
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
origami	origa	k1gFnPc7	origa
(	(	kIx(	(
<g/>
skládačky	skládačka	k1gFnPc1	skládačka
z	z	k7c2	z
papíru	papír	k1gInSc2	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ikebana	ikebana	k1gFnSc1	ikebana
(	(	kIx(	(
<g/>
umění	umění	k1gNnSc1	umění
aranžování	aranžování	k1gNnSc2	aranžování
květin	květina	k1gFnPc2	květina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
čajové	čajový	k2eAgInPc4d1	čajový
obřady	obřad	k1gInPc4	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
symbolem	symbol	k1gInSc7	symbol
Japonska	Japonsko	k1gNnSc2	Japonsko
je	být	k5eAaImIp3nS	být
i	i	k9	i
kimono	kimono	k1gNnSc1	kimono
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgInSc1d1	tradiční
japonský	japonský	k2eAgInSc1d1	japonský
oděv	oděv	k1gInSc1	oděv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
viděný	viděný	k2eAgInSc1d1	viděný
na	na	k7c6	na
umělkyních	umělkyně	k1gFnPc6	umělkyně
zvaných	zvaný	k2eAgFnPc2d1	zvaná
gejša	gejša	k1gFnSc1	gejša
(	(	kIx(	(
<g/>
společnice	společnice	k1gFnPc1	společnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
baví	bavit	k5eAaImIp3nP	bavit
tradičními	tradiční	k2eAgInPc7d1	tradiční
tanci	tanec	k1gInPc7	tanec
<g/>
,	,	kIx,	,
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
příjemnou	příjemný	k2eAgFnSc7d1	příjemná
konverzací	konverzace	k1gFnSc7	konverzace
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
běžný	běžný	k2eAgInSc1d1	běžný
oděv	oděv	k1gInSc1	oděv
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
nenosí	nosit	k5eNaImIp3nP	nosit
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
(	(	kIx(	(
<g/>
v	v	k7c6	v
rodinách	rodina	k1gFnPc6	rodina
či	či	k8xC	či
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
ctících	ctící	k2eAgMnPc2d1	ctící
tradici	tradice	k1gFnSc4	tradice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
příležitostech	příležitost	k1gFnPc6	příležitost
(	(	kIx(	(
<g/>
svatba	svatba	k1gFnSc1	svatba
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
</s>
<s>
Tradiční	tradiční	k2eAgNnSc4d1	tradiční
japonské	japonský	k2eAgNnSc4d1	Japonské
divadlo	divadlo	k1gNnSc4	divadlo
nó	nó	k0	nó
a	a	k8xC	a
japonské	japonský	k2eAgNnSc4d1	Japonské
divadlo	divadlo	k1gNnSc4	divadlo
kjógen	kjógen	k1gInSc1	kjógen
byly	být	k5eAaImAgFnP	být
vyhlášeny	vyhlásit	k5eAaPmNgInP	vyhlásit
jako	jako	k9	jako
světové	světový	k2eAgNnSc4d1	světové
nemateriální	materiální	k2eNgNnSc4d1	nemateriální
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
kinematografie	kinematografie	k1gFnSc1	kinematografie
obohatila	obohatit	k5eAaPmAgFnS	obohatit
filmy	film	k1gInPc4	film
o	o	k7c4	o
žánry	žánr	k1gInPc4	žánr
<g/>
:	:	kIx,	:
anime	animat	k5eAaPmIp3nS	animat
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kreslené	kreslený	k2eAgInPc4d1	kreslený
filmy	film	k1gInPc4	film
či	či	k8xC	či
seriály	seriál	k1gInPc4	seriál
<g/>
,	,	kIx,	,
džidaigeki	džidaigek	k1gFnPc4	džidaigek
-	-	kIx~	-
japonské	japonský	k2eAgInPc4d1	japonský
historické	historický	k2eAgInPc4d1	historický
filmy	film	k1gInPc4	film
o	o	k7c6	o
samurajích	samuraj	k1gMnPc6	samuraj
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgInSc1d1	japonský
horor	horor	k1gInSc1	horor
(	(	kIx(	(
<g/>
J-horor	Joror	k1gInSc1	J-horor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmy	film	k1gInPc4	film
s	s	k7c7	s
obřími	obří	k2eAgMnPc7d1	obří
netvory	netvor	k1gMnPc7	netvor
(	(	kIx(	(
<g/>
kaidžú	kaidžú	k?	kaidžú
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pink	pink	k2eAgInPc1d1	pink
filmy	film	k1gInPc1	film
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
-	-	kIx~	-
japonská	japonský	k2eAgFnSc1d1	japonská
"	"	kIx"	"
<g/>
softcore	softcor	k1gInSc5	softcor
<g/>
"	"	kIx"	"
pornografie	pornografie	k1gFnPc1	pornografie
<g/>
,	,	kIx,	,
filmy	film	k1gInPc1	film
o	o	k7c6	o
Jakuze	Jakuha	k1gFnSc6	Jakuha
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fotografie	fotografia	k1gFnSc2	fotografia
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc1	vývoj
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
řemesla	řemeslo	k1gNnSc2	řemeslo
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
fotografie	fotografia	k1gFnSc2	fotografia
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
důsledkem	důsledek	k1gInSc7	důsledek
změny	změna	k1gFnSc2	změna
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
zlepšování	zlepšování	k1gNnSc4	zlepšování
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
uznání	uznání	k1gNnSc2	uznání
fotografie	fotografia	k1gFnSc2	fotografia
jako	jako	k8xS	jako
svéprávné	svéprávný	k2eAgFnSc2d1	svéprávná
formy	forma	k1gFnSc2	forma
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
za	za	k7c2	za
silné	silný	k2eAgFnSc2d1	silná
spoluúčasti	spoluúčast	k1gFnSc2	spoluúčast
a	a	k8xC	a
zájmu	zájem	k1gInSc2	zájem
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
tohoto	tento	k3xDgNnSc2	tento
umění	umění	k1gNnSc2	umění
k	k	k7c3	k
úspěchům	úspěch	k1gInPc3	úspěch
značného	značný	k2eAgInSc2d1	značný
počtu	počet	k1gInSc2	počet
Japonců	Japonec	k1gMnPc2	Japonec
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
fotografie	fotografia	k1gFnSc2	fotografia
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
izolace	izolace	k1gFnSc1	izolace
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
stranou	strana	k1gFnSc7	strana
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
průkopníků	průkopník	k1gMnPc2	průkopník
fotografie	fotografia	k1gFnSc2	fotografia
byl	být	k5eAaImAgMnS	být
Švýcar	Švýcar	k1gMnSc1	Švýcar
Pierre	Pierr	k1gInSc5	Pierr
Rossier	Rossier	k1gMnSc1	Rossier
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
na	na	k7c4	na
východ	východ	k1gInSc4	východ
poslala	poslat	k5eAaPmAgFnS	poslat
londýnská	londýnský	k2eAgFnSc1d1	londýnská
firma	firma	k1gFnSc1	firma
Negretti	Negretť	k1gFnSc2	Negretť
and	and	k?	and
Zambra	Zambra	k1gMnSc1	Zambra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
umění	umění	k1gNnSc2	umění
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
fotografy	fotograf	k1gMnPc4	fotograf
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
Ueno	Ueno	k1gNnSc4	Ueno
Hikoma	Hikomum	k1gNnSc2	Hikomum
<g/>
,	,	kIx,	,
Horie	Horius	k1gMnPc4	Horius
Kuwadžiró	Kuwadžiró	k1gMnSc4	Kuwadžiró
nebo	nebo	k8xC	nebo
Maeda	Maed	k1gMnSc4	Maed
Genzó	Genzó	k1gMnSc4	Genzó
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
evropští	evropský	k2eAgMnPc1d1	evropský
turisté	turist	k1gMnPc1	turist
sem	sem	k6eAd1	sem
začali	začít	k5eAaPmAgMnP	začít
připlouvat	připlouvat	k5eAaImF	připlouvat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
komerčně	komerčně	k6eAd1	komerčně
<g/>
.	.	kIx.	.
</s>
<s>
Pečlivě	pečlivě	k6eAd1	pečlivě
kolorované	kolorovaný	k2eAgInPc1d1	kolorovaný
snímky	snímek	k1gInPc1	snímek
zobrazovaly	zobrazovat	k5eAaImAgInP	zobrazovat
nejen	nejen	k6eAd1	nejen
staré	starý	k2eAgNnSc1d1	staré
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
památky	památka	k1gFnPc4	památka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
japonské	japonský	k2eAgInPc4d1	japonský
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
portréty	portrét	k1gInPc4	portrét
japonských	japonský	k2eAgFnPc2d1	japonská
krasavic	krasavice	k1gFnPc2	krasavice
a	a	k8xC	a
samurajů	samuraj	k1gMnPc2	samuraj
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
fotografii	fotografia	k1gFnSc6	fotografia
byla	být	k5eAaImAgFnS	být
mezníkem	mezník	k1gInSc7	mezník
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začalo	začít	k5eAaPmAgNnS	začít
tvořit	tvořit	k5eAaImF	tvořit
více	hodně	k6eAd2	hodně
japonských	japonský	k2eAgMnPc2d1	japonský
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Kazumasa	Kazumasa	k1gFnSc1	Kazumasa
Ogawa	Ogawa	k1gFnSc1	Ogawa
<g/>
,	,	kIx,	,
Adolfo	Adolfo	k1gNnSc1	Adolfo
Farsari	Farsar	k1gFnSc2	Farsar
<g/>
,	,	kIx,	,
Raimund	Raimunda	k1gFnPc2	Raimunda
von	von	k1gInSc1	von
Stillfried	Stillfried	k1gInSc4	Stillfried
<g/>
,	,	kIx,	,
Kusakabe	Kusakab	k1gMnSc5	Kusakab
Kimbei	Kimbe	k1gMnSc5	Kimbe
<g/>
,	,	kIx,	,
Šin	Šin	k1gFnSc1	Šin
<g/>
'	'	kIx"	'
<g/>
iči	iči	k?	iči
Suzuki	suzuki	k1gNnSc1	suzuki
nebo	nebo	k8xC	nebo
Tamamura	Tamamura	k1gFnSc1	Tamamura
Kózaburó	Kózaburó	k1gFnSc1	Kózaburó
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
připravili	připravit	k5eAaPmAgMnP	připravit
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
profesi	profese	k1gFnSc4	profese
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
byla	být	k5eAaImAgFnS	být
časem	časem	k6eAd1	časem
přijata	přijmout	k5eAaPmNgFnS	přijmout
jako	jako	k8xS	jako
umělecká	umělecký	k2eAgFnSc1d1	umělecká
forma	forma	k1gFnSc1	forma
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
japonští	japonský	k2eAgMnPc1d1	japonský
fotografové	fotograf	k1gMnPc1	fotograf
aktivně	aktivně	k6eAd1	aktivně
účastní	účastnit	k5eAaImIp3nP	účastnit
na	na	k7c6	na
klíčových	klíčový	k2eAgFnPc6d1	klíčová
výstavách	výstava	k1gFnPc6	výstava
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
předních	přední	k2eAgMnPc2d1	přední
světových	světový	k2eAgMnPc2d1	světový
výrobců	výrobce	k1gMnPc2	výrobce
klasických	klasický	k2eAgInPc2d1	klasický
i	i	k8xC	i
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
a	a	k8xC	a
fotografické	fotografický	k2eAgFnSc2d1	fotografická
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
nespočet	nespočet	k1gInSc1	nespočet
fotografických	fotografický	k2eAgFnPc2d1	fotografická
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
je	být	k5eAaImIp3nS	být
udělováno	udělovat	k5eAaImNgNnS	udělovat
několik	několik	k4yIc1	několik
fotografických	fotografický	k2eAgNnPc2d1	fotografické
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
celosvětově	celosvětově	k6eAd1	celosvětově
známé	známý	k2eAgInPc4d1	známý
japonské	japonský	k2eAgInPc4d1	japonský
výrazy	výraz	k1gInPc4	výraz
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
aikido	aikida	k1gFnSc5	aikida
-	-	kIx~	-
bojové	bojový	k2eAgNnSc4d1	bojové
umění	umění	k1gNnSc4	umění
bonsaj	bonsaj	k1gInSc4	bonsaj
(	(	kIx(	(
<g/>
zakrslý	zakrslý	k2eAgInSc4d1	zakrslý
stromeček	stromeček	k1gInSc4	stromeček
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vzhled	vzhled	k1gInSc1	vzhled
má	mít	k5eAaImIp3nS	mít
napodobovat	napodobovat	k5eAaImF	napodobovat
tvary	tvar	k1gInPc4	tvar
velkých	velký	k2eAgInPc2d1	velký
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
)	)	kIx)	)
drift	drift	k1gInSc4	drift
-	-	kIx~	-
automobilový	automobilový	k2eAgInSc4d1	automobilový
sport	sport	k1gInSc4	sport
(	(	kIx(	(
<g/>
řízený	řízený	k2eAgInSc4d1	řízený
smyk	smyk	k1gInSc4	smyk
<g/>
)	)	kIx)	)
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
gejša	gejša	k1gFnSc1	gejša
(	(	kIx(	(
<g/>
společnice	společnice	k1gFnSc1	společnice
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
kamikaze	kamikaze	k1gMnSc1	kamikaze
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
božský	božský	k2eAgInSc1d1	božský
vítr	vítr	k1gInSc1	vítr
<g/>
"	"	kIx"	"
-	-	kIx~	-
sebevražední	sebevražedný	k2eAgMnPc1d1	sebevražedný
letci	letec	k1gMnPc1	letec
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
judo	judo	k1gNnSc1	judo
-	-	kIx~	-
bojové	bojový	k2eAgNnSc1d1	bojové
umění	umění	k1gNnSc1	umění
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
karate	karate	k1gNnSc2	karate
-	-	kIx~	-
bojové	bojový	k2eAgNnSc1d1	bojové
umění	umění	k1gNnSc1	umění
katana	katan	k1gMnSc2	katan
-	-	kIx~	-
japonský	japonský	k2eAgInSc1d1	japonský
samurajský	samurajský	k2eAgInSc1d1	samurajský
meč	meč	k1gInSc1	meč
lehce	lehko	k6eAd1	lehko
šavlovitého	šavlovitý	k2eAgInSc2d1	šavlovitý
tvaru	tvar	k1gInSc2	tvar
nindža	nindža	k1gMnSc1	nindža
-	-	kIx~	-
nájemný	nájemný	k2eAgMnSc1d1	nájemný
vrah	vrah	k1gMnSc1	vrah
<g/>
,	,	kIx,	,
špión	špión	k1gMnSc1	špión
sakura	sakura	k1gFnSc1	sakura
-	-	kIx~	-
japonská	japonský	k2eAgFnSc1d1	japonská
třešeň	třešeň	k1gFnSc1	třešeň
<g />
.	.	kIx.	.
</s>
<s>
samuraj	samuraj	k1gMnSc1	samuraj
-	-	kIx~	-
bojovník	bojovník	k1gMnSc1	bojovník
<g/>
,	,	kIx,	,
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
evropského	evropský	k2eAgMnSc2d1	evropský
rytíře	rytíř	k1gMnSc2	rytíř
seppuku	seppuk	k1gInSc2	seppuk
<g/>
,	,	kIx,	,
vulgárně	vulgárně	k6eAd1	vulgárně
harakiri	harakiri	k1gNnSc1	harakiri
-	-	kIx~	-
obřadní	obřadní	k2eAgFnSc1d1	obřadní
sebevražda	sebevražda	k1gFnSc1	sebevražda
mečem	meč	k1gInSc7	meč
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
cti	čest	k1gFnSc2	čest
sumó	sumó	k?	sumó
-	-	kIx~	-
tradiční	tradiční	k2eAgInSc1d1	tradiční
japonský	japonský	k2eAgInSc1d1	japonský
zápas	zápas	k1gInSc1	zápas
suši	suš	k1gFnSc3	suš
-	-	kIx~	-
tradiční	tradiční	k2eAgNnSc4d1	tradiční
japonské	japonský	k2eAgNnSc4d1	Japonské
jídlo	jídlo	k1gNnSc4	jídlo
připravované	připravovaný	k2eAgNnSc4d1	připravované
z	z	k7c2	z
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
mořských	mořský	k2eAgFnPc2d1	mořská
řas	řasa	k1gFnPc2	řasa
cunami	cunami	k1gNnSc2	cunami
(	(	kIx(	(
<g/>
anglický	anglický	k2eAgInSc1d1	anglický
přepis	přepis	k1gInSc1	přepis
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
,	,	kIx,	,
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
津	津	k?	津
<g />
.	.	kIx.	.
</s>
<s>
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
vlna	vlna	k1gFnSc1	vlna
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
několik	několik	k4yIc4	několik
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
jdoucích	jdoucí	k2eAgFnPc2d1	jdoucí
vln	vlna	k1gFnPc2	vlna
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
silném	silný	k2eAgNnSc6d1	silné
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
podmořském	podmořský	k2eAgInSc6d1	podmořský
sesuvu	sesuv	k1gInSc6	sesuv
nebo	nebo	k8xC	nebo
dopadu	dopad	k1gInSc6	dopad
meteoritu	meteorit	k1gInSc2	meteorit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc2	jeho
blízkosti	blízkost	k1gFnSc2	blízkost
Oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
a	a	k8xC	a
známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
i	i	k9	i
sójový	sójový	k2eAgInSc1d1	sójový
tvaroh	tvaroh	k1gInSc1	tvaroh
tofu	tofu	k1gNnSc2	tofu
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
šóju	šóju	k6eAd1	šóju
(	(	kIx(	(
<g/>
sójová	sójový	k2eAgFnSc1d1	sójová
omáčka	omáčka	k1gFnSc1	omáčka
<g/>
)	)	kIx)	)
a	a	k8xC	a
pasta	pasta	k1gFnSc1	pasta
miso	miso	k6eAd1	miso
(	(	kIx(	(
<g/>
ze	z	k7c2	z
zkvašené	zkvašený	k2eAgFnSc2d1	zkvašená
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
sójových	sójový	k2eAgInPc2d1	sójový
bobů	bob	k1gInPc2	bob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
