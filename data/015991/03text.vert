<s>
Oidipus	Oidipus	k1gMnSc1
</s>
<s>
Oidipus	Oidipus	k1gMnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
IokastéEuryganeiaAstymedusa	IokastéEuryganeiaAstymedus	k1gMnSc4
Děti	dítě	k1gFnPc1
</s>
<s>
IsménaEteoklésPolyneikésAntigonaLaónit	IsménaEteoklésPolyneikésAntigonaLaónit	k1gInSc4
Rodiče	rodič	k1gMnPc1
</s>
<s>
Láios	Láios	k1gInSc4
a	a	k8xC
Iokasté	Iokastý	k2eAgFnPc4d1
Funkce	funkce	k1gFnPc4
</s>
<s>
thébský	thébský	k2eAgMnSc1d1
král	král	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sfinga	sfinga	k1gFnSc1
a	a	k8xC
Oidipus	Oidipus	k1gMnSc1
</s>
<s>
Oidipus	Oidipus	k1gMnSc1
(	(	kIx(
<g/>
starořecky	starořecky	k6eAd1
Ο	Ο	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
řecké	řecký	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
syn	syn	k1gMnSc1
thébského	thébský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Láia	Láius	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Iokasté	Iokastý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
mládí	mládí	k1gNnSc1
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
předurčený	předurčený	k2eAgInSc1d1
osud	osud	k1gInSc1
<g/>
,	,	kIx,
plný	plný	k2eAgInSc1d1
zvratů	zvrat	k1gInPc2
<g/>
,	,	kIx,
vítězství	vítězství	k1gNnPc2
a	a	k8xC
pádů	pád	k1gInPc2
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
častým	častý	k2eAgInSc7d1
námětem	námět	k1gInSc7
velkých	velký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
od	od	k7c2
antiky	antika	k1gFnSc2
až	až	k6eAd1
do	do	k7c2
novověku	novověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Oidipův	Oidipův	k2eAgInSc1d1
jako	jako	k8xC,k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
důkazem	důkaz	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
svému	svůj	k3xOyFgInSc3
osudu	osud	k1gInSc3
nelze	lze	k6eNd1
uniknout	uniknout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Thébskému	thébský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Láiovi	Láia	k1gMnSc3
věštba	věštba	k1gFnSc1
určila	určit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
dočká	dočkat	k5eAaPmIp3nS
záhuby	záhuba	k1gFnPc1
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
potomků	potomek	k1gMnPc2
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
zůstane	zůstat	k5eAaPmIp3nS
bezdětný	bezdětný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touha	touha	k1gFnSc1
po	po	k7c6
nástupníkovi	nástupník	k1gMnSc6
trůnu	trůn	k1gInSc2
však	však	k9
ho	on	k3xPp3gNnSc4
dohnala	dohnat	k5eAaPmAgFnS
až	až	k9
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
unesl	unést	k5eAaPmAgInS
Chrýsippa	Chrýsipp	k1gMnSc4
<g/>
,	,	kIx,
syna	syn	k1gMnSc4
élidského	élidský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Pelopa	Pelop	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrýsippos	Chrýsippos	k1gInSc1
poté	poté	k6eAd1
přišel	přijít	k5eAaPmAgInS
o	o	k7c4
život	život	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Polyb	Polyba	k1gFnPc2
proklel	proklít	k5eAaPmAgMnS
Láia	Láia	k1gMnSc1
a	a	k8xC
předpověděl	předpovědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
dočká	dočkat	k5eAaPmIp3nS
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
a	a	k8xC
smrti	smrt	k1gFnSc2
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
mnoha	mnoho	k4c6
letech	léto	k1gNnPc6
mu	on	k3xPp3gMnSc3
královna	královna	k1gFnSc1
Iokasté	Iokastý	k2eAgFnPc1d1
porodila	porodit	k5eAaPmAgFnS
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
z	z	k7c2
obav	obava	k1gFnPc2
o	o	k7c4
vlastní	vlastní	k2eAgInSc4d1
život	život	k1gInSc4
nechal	nechat	k5eAaPmAgMnS
dítě	dítě	k1gNnSc4
pohodit	pohodit	k5eAaPmF
v	v	k7c6
lese	les	k1gInSc6
<g/>
,	,	kIx,
předtím	předtím	k6eAd1
mu	on	k3xPp3gInSc3
však	však	k9
ještě	ještě	k6eAd1
zmrzačil	zmrzačit	k5eAaPmAgMnS
nohy	noha	k1gFnPc4
a	a	k8xC
svázal	svázat	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
řemenem	řemen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc1
příkaz	příkaz	k1gInSc1
však	však	k9
nebyl	být	k5eNaImAgInS
vykonán	vykonán	k2eAgInSc1d1
<g/>
,	,	kIx,
dítě	dítě	k1gNnSc1
bylo	být	k5eAaImAgNnS
ponecháno	ponechán	k2eAgNnSc1d1
u	u	k7c2
pastýře	pastýř	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostal	dostat	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
Oidipús	Oidipús	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
s	s	k7c7
opuchlýma	opuchlý	k2eAgFnPc7d1
nohama	noha	k1gFnPc7
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pastýř	pastýř	k1gMnSc1
jej	on	k3xPp3gMnSc4
odnesl	odnést	k5eAaPmAgMnS
do	do	k7c2
paláce	palác	k1gInSc2
korinthského	korinthský	k1gMnSc2
krále	král	k1gMnSc2
Polyba	Polyba	k1gMnSc1
a	a	k8xC
ten	ten	k3xDgMnSc1
ho	on	k3xPp3gNnSc4
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
Meropou	Meropa	k1gFnSc7
vychoval	vychovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
na	na	k7c4
potomka	potomek	k1gMnSc4
krále	král	k1gMnSc4
sluší	slušet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
Oidipús	Oidipús	k1gInSc1
vyrostl	vyrůst	k5eAaPmAgInS
<g/>
,	,	kIx,
dozvěděl	dozvědět	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
vlastním	vlastní	k2eAgMnSc7d1
synem	syn	k1gMnSc7
Polybovým	Polybův	k2eAgMnSc7d1
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
o	o	k7c6
svém	svůj	k3xOyFgInSc6
původu	původ	k1gInSc6
nemohl	moct	k5eNaImAgMnS
nic	nic	k3yNnSc4
dozvědět	dozvědět	k5eAaPmF
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
Delf	Delfy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslulá	proslulý	k2eAgFnSc1d1
věštkyně	věštkyně	k1gFnSc1
Pýthia	Pýthia	k1gFnSc1
mu	on	k3xPp3gMnSc3
prozradila	prozradit	k5eAaPmAgFnS
pouze	pouze	k6eAd1
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
vrahem	vrah	k1gMnSc7
vlastního	vlastní	k2eAgNnSc2d1
otce	otec	k1gMnSc2
<g/>
,	,	kIx,
ožení	oženit	k5eAaPmIp3nS
se	se	k3xPyFc4
a	a	k8xC
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
matkou	matka	k1gFnSc7
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
zahubí	zahubit	k5eAaPmIp3nS
svým	svůj	k3xOyFgNnPc3
prokletím	prokletí	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
Osud	osud	k1gInSc1
se	se	k3xPyFc4
naplňuje	naplňovat	k5eAaImIp3nS
</s>
<s>
Zdrcený	zdrcený	k2eAgMnSc1d1
takovou	takový	k3xDgFnSc7
předpovědí	předpověď	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
aby	aby	kYmCp3nS
uchránil	uchránit	k5eAaPmAgMnS
Polyba	Polyba	k1gFnSc1
a	a	k8xC
Meropu	Merop	k1gMnSc3
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
Oidipús	Oidipús	k1gInSc4
z	z	k7c2
Delf	Delfy	k1gFnPc2
<g/>
,	,	kIx,
nevrátil	vrátit	k5eNaPmAgMnS
se	se	k3xPyFc4
však	však	k9
už	už	k6eAd1
do	do	k7c2
Korinthu	Korinth	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
vydal	vydat	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
města	město	k1gNnSc2
Théb	Théby	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blízko	blízko	k7c2
města	město	k1gNnSc2
se	se	k3xPyFc4
potkal	potkat	k5eAaPmAgInS
s	s	k7c7
vozem	vůz	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
seděl	sedět	k5eAaImAgMnS
neznámý	známý	k2eNgMnSc1d1
stařec	stařec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozka	vozka	k1gMnSc1
švihl	švihnout	k5eAaPmAgMnS
Oidipa	Oidipus	k1gMnSc4
bičem	bič	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
uvolnil	uvolnit	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
a	a	k8xC
stařec	stařec	k1gMnSc1
ho	on	k3xPp3gMnSc4
navíc	navíc	k6eAd1
udeřil	udeřit	k5eAaPmAgMnS
holí	hole	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oidipús	Oidipús	k1gInSc1
v	v	k7c6
hněvu	hněv	k1gInSc6
starci	stařec	k1gMnPc1
ránu	ráno	k1gNnSc6
oplatil	oplatit	k5eAaPmAgInS
takovou	takový	k3xDgFnSc7
silou	síla	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
stařec	stařec	k1gMnSc1
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
mrtev	mrtev	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Netušil	Netušil	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k6eAd1
zabil	zabít	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
<g/>
,	,	kIx,
krále	král	k1gMnSc4
Láia	Láius	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
vracel	vracet	k5eAaImAgMnS
rovněž	rovněž	k9
z	z	k7c2
Delf	Delfy	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
chtěl	chtít	k5eAaImAgMnS
získat	získat	k5eAaPmF
radu	rada	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
osvobodit	osvobodit	k5eAaPmF
město	město	k1gNnSc4
od	od	k7c2
tyranie	tyranie	k1gFnSc2
Sfingy	sfinga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
pak	pak	k6eAd1
Oidipús	Oidipús	k1gInSc1
vstoupil	vstoupit	k5eAaPmAgInS
do	do	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
našel	najít	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
zahalené	zahalený	k2eAgNnSc1d1
smutkem	smutek	k1gInSc7
-	-	kIx~
pro	pro	k7c4
smrt	smrt	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
krále	král	k1gMnSc2
a	a	k8xC
pro	pro	k7c4
nebezpečí	nebezpečí	k1gNnSc4
od	od	k7c2
Sfingy	sfinga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sfinx	sfinx	k1gFnSc1
</s>
<s>
Oidipús	Oidipús	k1gInSc1
se	se	k3xPyFc4
vypravil	vypravit	k5eAaPmAgInS
k	k	k7c3
hoře	hora	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
Sfinga	sfinga	k1gFnSc1
sídlila	sídlit	k5eAaImAgFnS
a	a	k8xC
požádal	požádat	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
položila	položit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
hádanku	hádanka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
ji	on	k3xPp3gFnSc4
neuhodne	uhodnout	k5eNaPmIp3nS
<g/>
,	,	kIx,
čeká	čekat	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
smrt	smrt	k1gFnSc1
<g/>
,	,	kIx,
Sfinga	sfinga	k1gFnSc1
ho	on	k3xPp3gNnSc4
rozsápe	rozsápat	k5eAaPmIp3nS
a	a	k8xC
shodí	shodit	k5eAaPmIp3nS
ze	z	k7c2
skály	skála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hádanka	hádanka	k1gFnSc1
zněla	znět	k5eAaImAgFnS
<g/>
:	:	kIx,
Který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
živočich	živočich	k1gMnSc1
má	mít	k5eAaImIp3nS
jen	jen	k9
jeden	jeden	k4xCgInSc1
hlas	hlas	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
přitom	přitom	k6eAd1
má	mít	k5eAaImIp3nS
někdy	někdy	k6eAd1
dvě	dva	k4xCgFnPc4
nohy	noha	k1gFnPc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
tři	tři	k4xCgMnPc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
čtyři	čtyři	k4xCgMnPc1
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
nejslabší	slabý	k2eAgNnSc1d3
<g/>
,	,	kIx,
když	když	k8xS
jich	on	k3xPp3gMnPc2
má	mít	k5eAaImIp3nS
nejvíc	hodně	k6eAd3,k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Oidipús	Oidipús	k1gInSc1
hned	hned	k6eAd1
odpověděl	odpovědět	k5eAaPmAgInS
<g/>
:	:	kIx,
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
jako	jako	k8xS,k8xC
batole	batole	k1gNnSc1
leze	lézt	k5eAaImIp3nS
po	po	k7c6
všech	všecek	k3xTgFnPc6
čtyřech	čtyři	k4xCgFnPc6
<g/>
,	,	kIx,
v	v	k7c6
mládí	mládí	k1gNnSc6
stojí	stát	k5eAaImIp3nS
pevně	pevně	k6eAd1
na	na	k7c6
dvou	dva	k4xCgNnPc6
a	a	k8xC
ve	v	k7c6
stáří	stáří	k1gNnSc6
se	se	k3xPyFc4
opírá	opírat	k5eAaImIp3nS
o	o	k7c4
hůl	hůl	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Sfinx	sfinx	k1gInSc4
zahanbením	zahanbení	k1gNnSc7
a	a	k8xC
vztekem	vztek	k1gInSc7
se	se	k3xPyFc4
vrhla	vrhnout	k5eAaImAgFnS,k5eAaPmAgFnS
ze	z	k7c2
skály	skála	k1gFnSc2
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Théby	Théby	k1gFnPc1
byly	být	k5eAaImAgFnP
osvobozeny	osvobodit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Doma	doma	k6eAd1
</s>
<s>
Théby	Théby	k1gFnPc1
přivítaly	přivítat	k5eAaPmAgFnP
Oidipa	Oidipus	k1gMnSc2
jako	jako	k8xS,k8xC
svého	svůj	k3xOyFgMnSc2
vysvoboditele	vysvoboditel	k1gMnSc2
<g/>
,	,	kIx,
zahrnuly	zahrnout	k5eAaPmAgFnP
ho	on	k3xPp3gMnSc4
pozorností	pozornost	k1gFnSc7
a	a	k8xC
láskou	láska	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
králem	král	k1gMnSc7
<g/>
,	,	kIx,
nastěhoval	nastěhovat	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
královského	královský	k2eAgInSc2d1
paláce	palác	k1gInSc2
a	a	k8xC
-	-	kIx~
oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
královnou	královna	k1gFnSc7
vdovou	vdova	k1gFnSc7
-	-	kIx~
Iokasté	Iokastý	k2eAgFnPc4d1
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgFnPc4d1
matkou	matka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišly	přijít	k5eAaPmAgInP
šťastné	šťastný	k2eAgInPc1d1
roky	rok	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
i	i	k9
otcem	otec	k1gMnSc7
dcer	dcera	k1gFnPc2
Antigony	Antigona	k1gFnSc2
a	a	k8xC
Ismény	Isména	k1gFnSc2
a	a	k8xC
synů	syn	k1gMnPc2
Eteokla	Eteokla	k1gMnSc2
a	a	k8xC
Polyneika	Polyneik	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Osud	osud	k1gInSc1
se	se	k3xPyFc4
završuje	završovat	k5eAaImIp3nS
</s>
<s>
Šťastné	Šťastné	k2eAgNnSc1d1
období	období	k1gNnSc1
však	však	k9
končilo	končit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
Apollón	Apollón	k1gMnSc1
seslal	seslat	k5eAaPmAgMnS
na	na	k7c4
Théby	Théby	k1gFnPc4
mor	mora	k1gFnPc2
<g/>
,	,	kIx,
všechno	všechen	k3xTgNnSc1
hynulo	hynout	k5eAaImAgNnS
<g/>
,	,	kIx,
nastala	nastat	k5eAaPmAgFnS
neúroda	neúroda	k1gFnSc1
a	a	k8xC
přišel	přijít	k5eAaPmAgInS
hlad	hlad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oidipús	Oidipús	k1gInSc1
vyslal	vyslat	k5eAaPmAgInS
Iokastina	Iokastin	k2eAgMnSc4d1
bratra	bratr	k1gMnSc4
Kreonta	Kreont	k1gMnSc4
do	do	k7c2
Delf	Delfy	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přinesl	přinést	k5eAaPmAgInS
radu	rada	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
město	město	k1gNnSc1
a	a	k8xC
lid	lid	k1gInSc4
zachránit	zachránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pýthie	Pýthie	k1gFnSc1
jasně	jasně	k6eAd1
řekla	říct	k5eAaPmAgFnS
<g/>
:	:	kIx,
Vyžeňte	vyhnat	k5eAaPmRp2nP
z	z	k7c2
města	město	k1gNnSc2
vraha	vrah	k1gMnSc2
krále	král	k1gMnSc2
Láia	Láius	k1gMnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Oidipús	Oidipúsa	k1gFnPc2
nic	nic	k6eAd1
netušil	tušit	k5eNaImAgMnS
<g/>
,	,	kIx,
vyhlásil	vyhlásit	k5eAaPmAgMnS
pátrání	pátrání	k1gNnSc3
po	po	k7c6
vrahovi	vrah	k1gMnSc6
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yRgMnSc4,k3yQgMnSc4
proklel	proklít	k5eAaPmAgInS
a	a	k8xC
odsoudil	odsoudit	k5eAaPmAgInS
k	k	k7c3
vyhnanství	vyhnanství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Slepý	slepý	k2eAgMnSc1d1
věštec	věštec	k1gMnSc1
Teiresiás	Teiresiása	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
po	po	k7c6
ztrátě	ztráta	k1gFnSc6
zraku	zrak	k1gInSc2
získal	získat	k5eAaPmAgInS
jemný	jemný	k2eAgInSc1d1
sluch	sluch	k1gInSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
mnohé	mnohé	k1gNnSc1
doneslo	donést	k5eAaPmAgNnS
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
lidé	člověk	k1gMnPc1
neznali	neznat	k5eAaImAgMnP,k5eNaImAgMnP
<g/>
,	,	kIx,
přišel	přijít	k5eAaPmAgMnS
na	na	k7c4
královský	královský	k2eAgInSc4d1
dvůr	dvůr	k1gInSc4
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zná	znát	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
vraha	vrah	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
velkém	velký	k2eAgNnSc6d1
naléhání	naléhání	k1gNnSc6
krále	král	k1gMnSc2
i	i	k8xC
lidu	lid	k1gInSc2
nakonec	nakonec	k6eAd1
odhalil	odhalit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vrahem	vrah	k1gMnSc7
je	být	k5eAaImIp3nS
sám	sám	k3xTgInSc1
Oidipús	Oidipús	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
také	také	k6eAd1
vinen	vinen	k2eAgMnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
manželem	manžel	k1gMnSc7
vlastní	vlastní	k2eAgFnSc2d1
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oidipús	Oidipús	k1gInSc1
se	se	k3xPyFc4
bránil	bránit	k5eAaImAgInS
<g/>
,	,	kIx,
považoval	považovat	k5eAaImAgMnS
to	ten	k3xDgNnSc4
za	za	k7c4
spiknutí	spiknutí	k1gNnSc4
a	a	k8xC
touhu	touha	k1gFnSc4
po	po	k7c6
moci	moc	k1gFnSc6
zejména	zejména	k9
Kreonta	Kreonta	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
když	když	k8xS
Teiresiás	Teiresiás	k1gInSc4
přese	přese	k7c4
všechno	všechen	k3xTgNnSc4
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
trval	trvat	k5eAaImAgMnS
<g/>
,	,	kIx,
požádal	požádat	k5eAaPmAgInS
Oidipús	Oidipús	k1gInSc1
manželku	manželka	k1gFnSc4
Iokasté	Iokastý	k2eAgFnPc4d1
o	o	k7c4
vysvětlení	vysvětlení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
potvrdila	potvrdit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
měla	mít	k5eAaImAgFnS
Láiova	Láiovo	k1gNnSc2
syna	syn	k1gMnSc2
a	a	k8xC
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gNnSc7
stalo	stát	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
nechali	nechat	k5eAaPmAgMnP
předvolat	předvolat	k5eAaPmF
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
dítě	dítě	k1gNnSc4
z	z	k7c2
Théb	Théby	k1gFnPc2
odnesl	odnést	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Mezitím	mezitím	k6eAd1
přišel	přijít	k5eAaPmAgInS
do	do	k7c2
Théb	Théby	k1gFnPc2
z	z	k7c2
Korinthu	Korinth	k1gInSc2
posel	posel	k1gMnSc1
<g/>
,	,	kIx,
oznamující	oznamující	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
krále	král	k1gMnSc2
Polyba	Polyb	k1gMnSc2
a	a	k8xC
povolával	povolávat	k5eAaImAgMnS
jej	on	k3xPp3gMnSc4
na	na	k7c4
uprázdněný	uprázdněný	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oidipús	Oidipús	k1gInSc1
v	v	k7c6
tom	ten	k3xDgNnSc6
viděl	vidět	k5eAaImAgMnS
jistotu	jistota	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
nezabil	zabít	k5eNaPmAgInS
a	a	k8xC
s	s	k7c7
matkou	matka	k1gFnSc7
se	se	k3xPyFc4
neoženil	oženit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
ale	ale	k8xC
ujištění	ujištění	k1gNnSc1
posla	posel	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Oidipús	Oidipús	k1gInSc1
může	moct	k5eAaImIp3nS
do	do	k7c2
korinthského	korinthský	k2eAgNnSc2d1
království	království	k1gNnSc2
bez	bez	k7c2
obav	obava	k1gFnPc2
před	před	k7c7
naplněním	naplnění	k1gNnSc7
osudu	osud	k1gInSc2
vrátit	vrátit	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
není	být	k5eNaImIp3nS
jejich	jejich	k3xOp3gMnSc7
vlastním	vlastní	k2eAgMnSc7d1
synem	syn	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
nalezencem	nalezenec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
vše	všechen	k3xTgNnSc4
doplnil	doplnit	k5eAaPmAgMnS
svým	svůj	k3xOyFgNnSc7
svědectvím	svědectví	k1gNnSc7
starý	starý	k2eAgMnSc1d1
otrok	otrok	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
dítě	dítě	k1gNnSc4
z	z	k7c2
Théb	Théby	k1gFnPc2
odnesl	odnést	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Iokasté	Iokastý	k2eAgNnSc1d1
se	se	k3xPyFc4
zhroutila	zhroutit	k5eAaPmAgFnS
<g/>
,	,	kIx,
prchla	prchnout	k5eAaPmAgFnS
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
komnat	komnata	k1gFnPc2
a	a	k8xC
spáchala	spáchat	k5eAaPmAgFnS
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
ji	on	k3xPp3gFnSc4
dostihl	dostihnout	k5eAaPmAgInS
Oidipús	Oidipús	k1gInSc1
a	a	k8xC
v	v	k7c6
zoufalství	zoufalství	k1gNnSc6
si	se	k3xPyFc3
její	její	k3xOp3gFnSc7
jehlicí	jehlice	k1gFnSc7
vypíchl	vypíchnout	k5eAaPmAgMnS
oči	oko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oidipús	Oidipús	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
všechno	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
i	i	k8xC
naději	naděje	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozná	hrozný	k2eAgFnSc1d1
věštba	věštba	k1gFnSc1
se	se	k3xPyFc4
vyplnila	vyplnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lid	lid	k1gInSc1
v	v	k7c6
Thébách	Théby	k1gFnPc6
nejprve	nejprve	k6eAd1
s	s	k7c7
králem	král	k1gMnSc7
soucítil	soucítit	k5eAaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
brzy	brzy	k6eAd1
jejich	jejich	k3xOp3gFnSc1
náklonnost	náklonnost	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
<g/>
,	,	kIx,
zapomněli	zapomenout	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
moudrou	moudrý	k2eAgFnSc4d1
a	a	k8xC
spravedlivou	spravedlivý	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
a	a	k8xC
spravování	spravování	k1gNnSc4
a	a	k8xC
rozkvět	rozkvět	k1gInSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádali	žádat	k5eAaImAgMnP
jeho	jeho	k3xOp3gNnSc4
vyhnání	vyhnání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprvu	zprvu	k6eAd1
měl	mít	k5eAaImAgInS
ještě	ještě	k9
ochranu	ochrana	k1gFnSc4
od	od	k7c2
znovu	znovu	k6eAd1
nastoleného	nastolený	k2eAgMnSc2d1
krále	král	k1gMnSc2
Kreonta	Kreont	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ten	ten	k3xDgMnSc1
musel	muset	k5eAaImAgMnS
ustoupit	ustoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oidipa	Oidipus	k1gMnSc2
opustili	opustit	k5eAaPmAgMnP
i	i	k9
jeho	jeho	k3xOp3gMnPc1
synové	syn	k1gMnPc1
Eteoklés	Eteoklésa	k1gFnPc2
a	a	k8xC
Polyneikés	Polyneikésa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Oidipa	Oidipus	k1gMnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
slepý	slepý	k2eAgMnSc1d1
vyhnanec	vyhnanec	k1gMnSc1
<g/>
,	,	kIx,
bezmocný	bezmocný	k2eAgMnSc1d1
a	a	k8xC
ponížený	ponížený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Dcera	dcera	k1gFnSc1
Antigona	Antigona	k1gFnSc1
jediná	jediný	k2eAgFnSc1d1
zůstala	zůstat	k5eAaPmAgFnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
otcem	otec	k1gMnSc7
<g/>
,	,	kIx,
provázela	provázet	k5eAaImAgFnS
ho	on	k3xPp3gMnSc4
ve	v	k7c6
vyhnanství	vyhnanství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouho	dlouho	k6eAd1
putovali	putovat	k5eAaImAgMnP
po	po	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
opovržení	opovržení	k1gNnSc4
<g/>
,	,	kIx,
odmítaní	odmítaný	k2eAgMnPc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
dorazili	dorazit	k5eAaPmAgMnP
do	do	k7c2
blízkosti	blízkost	k1gFnSc2
Athén	Athéna	k1gFnPc2
a	a	k8xC
v	v	k7c6
posvátném	posvátný	k2eAgInSc6d1
háji	háj	k1gInSc6
Eumenid	Eumenidy	k1gFnPc2
<g/>
,	,	kIx,
usmířených	usmířený	k2eAgFnPc2d1
bohyň	bohyně	k1gFnPc2
pomsty	pomsta	k1gFnSc2
Erínyí	Erínyí	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
došel	dojít	k5eAaPmAgInS
Oidipús	Oidipús	k1gInSc4
k	k	k7c3
mírnému	mírný	k2eAgNnSc3d1
uklidnění	uklidnění	k1gNnSc3
a	a	k8xC
úlevě	úleva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oidipús	Oidipús	k1gInSc1
požádal	požádat	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Athéňané	Athéňan	k1gMnPc1
k	k	k7c3
němu	on	k3xPp3gNnSc3
přivedli	přivést	k5eAaPmAgMnP
svého	svůj	k3xOyFgMnSc4
krále	král	k1gMnSc4
Thésea	Théseus	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Než	než	k8xS
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
přinesla	přinést	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
Isména	Isména	k1gFnSc1
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
oba	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
Eteoklés	Eteoklésa	k1gFnPc2
a	a	k8xC
Polyneikés	Polyneikésa	k1gFnPc2
rozpoutali	rozpoutat	k5eAaPmAgMnP
mezi	mezi	k7c7
sebou	se	k3xPyFc7
krutý	krutý	k2eAgInSc4d1
boj	boj	k1gInSc4
o	o	k7c4
moc	moc	k1gFnSc4
nad	nad	k7c7
Thébami	Théby	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
se	se	k3xPyFc4
přesto	přesto	k6eAd1
snažili	snažit	k5eAaImAgMnP
přetáhnout	přetáhnout	k5eAaPmF
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
Oidipa	Oidipus	k1gMnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
věštby	věštba	k1gFnSc2
zvítězí	zvítězit	k5eAaPmIp3nS
v	v	k7c6
boji	boj	k1gInSc6
o	o	k7c4
Théby	Théby	k1gFnPc4
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gFnSc6
straně	strana	k1gFnSc6
bude	být	k5eAaImBp3nS
vyhnaný	vyhnaný	k2eAgInSc1d1
Oidipús	Oidipús	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
ochránil	ochránit	k5eAaPmAgMnS
před	před	k7c7
násilím	násilí	k1gNnSc7
král	král	k1gMnSc1
Théseus	Théseus	k1gMnSc1
a	a	k8xC
Oidipús	Oidipús	k1gInSc1
proklel	proklít	k5eAaPmAgInS
své	svůj	k3xOyFgMnPc4
syny	syn	k1gMnPc4
„	„	k?
<g/>
strašlivou	strašlivý	k2eAgFnSc7d1
kletbou	kletba	k1gFnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tom	ten	k3xDgInSc6
okamžiku	okamžik	k1gInSc6
oznámila	oznámit	k5eAaPmAgFnS
hromová	hromový	k2eAgFnSc1d1
rána	rána	k1gFnSc1
nejvyššího	vysoký	k2eAgMnSc2d3
boha	bůh	k1gMnSc2
Dia	Dia	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
tím	ten	k3xDgNnSc7
končí	končit	k5eAaImIp3nS
Oidipova	Oidipův	k2eAgFnSc1d1
strastiplná	strastiplný	k2eAgFnSc1d1
pozemská	pozemský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oidipús	Oidipús	k1gInSc1
se	se	k3xPyFc4
rozloučil	rozloučit	k5eAaPmAgInS
s	s	k7c7
dcerami	dcera	k1gFnPc7
<g/>
,	,	kIx,
svěřil	svěřit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
do	do	k7c2
Théseovy	Théseův	k2eAgFnSc2d1
ochrany	ochrana	k1gFnSc2
a	a	k8xC
za	za	k7c4
to	ten	k3xDgNnSc4
mu	on	k3xPp3gMnSc3
prozradil	prozradit	k5eAaPmAgMnS
místo	místo	k7c2
svého	svůj	k3xOyFgInSc2
hrobu	hrob	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
bude	být	k5eAaImBp3nS
chránit	chránit	k5eAaImF
Athény	Athéna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
smířený	smířený	k2eAgInSc1d1
odebral	odebrat	k5eAaPmAgInS
do	do	k7c2
Hádovy	Hádův	k2eAgFnSc2d1
říše	říš	k1gFnSc2
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odraz	odraz	k1gInSc1
v	v	k7c6
umění	umění	k1gNnSc6
</s>
<s>
Toto	tento	k3xDgNnSc1
téma	téma	k1gNnSc1
zpracovalo	zpracovat	k5eAaPmAgNnS
bezpočet	bezpočet	k1gInSc4
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
mnohá	mnohý	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
jsou	být	k5eAaImIp3nP
známa	znám	k2eAgNnPc1d1
a	a	k8xC
platná	platný	k2eAgFnSc1d1
do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Sofoklés	Sofoklés	k1gInSc1
<g/>
:	:	kIx,
Král	Král	k1gMnSc1
Oidipús	Oidipúsa	k1gFnPc2
–	–	k?
z	z	k7c2
let	léto	k1gNnPc2
429	#num#	k4
<g/>
–	–	k?
<g/>
425	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
později	pozdě	k6eAd2
týž	týž	k3xTgMnSc1
autor	autor	k1gMnSc1
Oidipús	Oidipúsa	k1gFnPc2
na	na	k7c6
Kolónu	kolón	k1gInSc6
</s>
<s>
Aischylova	Aischylův	k2eAgFnSc1d1
tragédie	tragédie	k1gFnSc1
Láios	Láiosa	k1gFnPc2
a	a	k8xC
Oidipús	Oidipúsa	k1gFnPc2
</s>
<s>
náměty	námět	k1gInPc4
čerpal	čerpat	k5eAaImAgInS
také	také	k9
Homér	Homér	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
Ilias	Ilias	k1gFnSc1
i	i	k8xC
Odyssei	Odyssei	k1gNnSc1
<g/>
;	;	kIx,
Sofoklés	Sofoklés	k1gInSc1
v	v	k7c6
dramatech	drama	k1gNnPc6
Antigona	Antigon	k1gMnSc2
či	či	k8xC
Élektra	Élektr	k1gMnSc2
</s>
<s>
Pierre	Pierr	k1gMnSc5
Corneille	Corneill	k1gMnSc5
<g/>
:	:	kIx,
Œ	Œ	k5eAaPmIp3nS
(	(	kIx(
<g/>
1659	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Voltaire	Voltair	k1gMnSc5
<g/>
:	:	kIx,
Oidipus	Oidipus	k1gMnSc1
(	(	kIx(
<g/>
1719	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
Ozerov	Ozerov	k1gInSc1
<g/>
:	:	kIx,
Oidipus	Oidipus	k1gMnSc1
v	v	k7c6
Aténách	Atény	k1gFnPc6
(	(	kIx(
<g/>
Э	Э	k?
в	в	k?
А	А	k?
<g/>
,	,	kIx,
1804	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hugo	Hugo	k1gMnSc1
von	von	k1gInSc1
Hofmannsthal	Hofmannsthal	k1gMnSc1
Oidipús	Oidipús	k1gInSc1
a	a	k8xC
Sfinx	sfinx	k1gInSc1
(	(	kIx(
<g/>
Ödipus	Ödipus	k1gInSc1
und	und	k?
die	die	k?
Sphinx	Sphinx	k1gInSc1
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Igor	Igor	k1gMnSc1
Fjodorovič	Fjodorovič	k1gMnSc1
Stravinskij	Stravinskij	k1gMnSc1
Oedipus	Oedipus	k1gMnSc1
Rex	Rex	k1gMnSc1
-	-	kIx~
oratorium	oratorium	k1gNnSc1
-	-	kIx~
opera	opera	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
premiéra	premiéra	k1gFnSc1
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pier	pier	k1gInSc1
Paolo	Paolo	k1gNnSc1
Pasolini	Pasolin	k2eAgMnPc1d1
-	-	kIx~
Oidipus	Oidipus	k1gMnSc1
král	král	k1gMnSc1
(	(	kIx(
<g/>
Edipo	Edipa	k1gFnSc5
re	re	k9
<g/>
)	)	kIx)
-	-	kIx~
film	film	k1gInSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oidipus	Oidipus	k1gMnSc1
Tyranus	Tyranus	k1gMnSc1
Rock	rock	k1gInSc1
Opera	opera	k1gFnSc1
-	-	kIx~
rocková	rockový	k2eAgFnSc1d1
opera	opera	k1gFnSc1
uváděná	uváděný	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Oidipus	Oidipus	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Slovník	slovník	k1gInSc1
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
nakl	naknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1974	#num#	k4
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Zamarovský	Zamarovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Bohové	bůh	k1gMnPc1
a	a	k8xC
hrdinové	hrdina	k1gMnPc1
antických	antický	k2eAgFnPc2d1
bájí	báj	k1gFnPc2
</s>
<s>
Graves	Graves	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
,	,	kIx,
Řecké	řecký	k2eAgInPc4d1
mýty	mýtus	k1gInPc4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7309-153-4	80-7309-153-4	k4
</s>
<s>
Houtzager	Houtzager	k1gInSc1
<g/>
,	,	kIx,
Guus	Guus	k1gInSc1
<g/>
,	,	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
řecké	řecký	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7234-287-8	80-7234-287-8	k4
</s>
<s>
Petiška	Petiška	k1gFnSc1
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
<g/>
,	,	kIx,
Staré	staré	k1gNnSc1
řecké	řecký	k2eAgFnSc2d1
báje	báj	k1gFnSc2
a	a	k8xC
pověsti	pověst	k1gFnSc2
<g/>
,	,	kIx,
1961	#num#	k4
</s>
<s>
Löwe	Löwe	k1gFnSc1
<g/>
,	,	kIx,
Gerhard	Gerhard	k1gMnSc1
<g/>
,	,	kIx,
Stoll	Stoll	k1gMnSc1
<g/>
,	,	kIx,
Heinrich	Heinrich	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
<g/>
,	,	kIx,
ABC	ABC	kA
Antiky	antika	k1gFnSc2
</s>
<s>
Fink	Fink	k1gMnSc1
<g/>
,	,	kIx,
Gerhard	Gerhard	k1gMnSc1
<g/>
,	,	kIx,
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
v	v	k7c6
antické	antický	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7218-992-1	80-7218-992-1	k4
</s>
<s>
Neškudla	Neškudnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
Bořek	Bořek	k1gMnSc1
<g/>
,	,	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
řeckých	řecký	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
a	a	k8xC
mýtů	mýtus	k1gInPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7277-125-6	80-7277-125-6	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Oidipovský	oidipovský	k2eAgInSc1d1
komplex	komplex	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
2016908826	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118589393	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2014018217	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
804472	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2014018217	#num#	k4
</s>
