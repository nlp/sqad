<s>
Deficientní	Deficientní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Přirozená	přirozený	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
od	od	k7c2
1	#num#	k4
do	do	k7c2
40	#num#	k4
a	a	k8xC
hodnoty	hodnota	k1gFnPc1
jejich	jejich	k3xOp3gFnPc1
s	s	k7c7
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
.	.	kIx.
deficientní	deficientní	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
jsou	být	k5eAaImIp3nP
znázorněna	znázornit	k5eAaPmNgNnP
šedě	šedě	k6eAd1
<g/>
,	,	kIx,
dokonalá	dokonalý	k2eAgFnSc1d1
červeně	červeně	k6eAd1
a	a	k8xC
abundantní	abundantní	k2eAgFnPc1d1
modře	modř	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Deficientní	Deficientní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
matematice	matematika	k1gFnSc6
takové	takový	k3xDgNnSc1
číslo	číslo	k1gNnSc1
n	n	k0
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
součet	součet	k1gInSc1
všech	všecek	k3xTgInPc2
vlastních	vlastní	k2eAgInPc2d1
dělitelů	dělitel	k1gInPc2
kromě	kromě	k7c2
sebe	sebe	k3xPyFc4
samého	samý	k3xTgMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platí	platit	k5eAaImIp3nS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
součet	součet	k1gInSc1
všech	všecek	k3xTgMnPc2
kladných	kladný	k2eAgMnPc2d1
dělitelů	dělitel	k1gMnPc2
včetně	včetně	k7c2
n	n	k0
samého	samý	k3xTgInSc2
σ	σ	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
<	<	kIx(
2	#num#	k4
<g/>
n.	n.	k?
Ekvivalentně	ekvivalentně	k6eAd1
lze	lze	k6eAd1
deficientní	deficientní	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
definovat	definovat	k5eAaBmF
jako	jako	k8xS,k8xC
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
součet	součet	k1gInSc1
všech	všecek	k3xTgInPc2
kladných	kladný	k2eAgInPc2d1
dělitelů	dělitel	k1gInPc2
kromě	kromě	k7c2
n	n	k0
samého	samý	k3xTgInSc2
s	s	k7c7
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
<	<	kIx(
n.	n.	k?
Čísla	číslo	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
která	který	k3yQgNnPc4,k3yIgNnPc4,k3yRgNnPc4
σ	σ	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
>	>	kIx)
2	#num#	k4
<g/>
n	n	k0
jsou	být	k5eAaImIp3nP
abundantní	abundantní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čísla	číslo	k1gNnPc4
<g/>
,	,	kIx,
pro	pro	k7c4
která	který	k3yQgNnPc4,k3yRgNnPc4,k3yIgNnPc4
σ	σ	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
=	=	kIx~
2	#num#	k4
<g/>
n	n	k0
a	a	k8xC
tedy	tedy	k9
s	s	k7c7
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
=	=	kIx~
n	n	k0
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
dokonalá	dokonalý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Hodnota	hodnota	k1gFnSc1
2	#num#	k4
<g/>
n	n	k0
−	−	k?
σ	σ	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nazývána	nazývat	k5eAaImNgFnS
deficiencí	deficience	k1gFnSc7
čísla	číslo	k1gNnSc2
n.	n.	k?
</s>
<s>
Několik	několik	k4yIc1
prvních	první	k4xOgNnPc2
deficientních	deficientní	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
(	(	kIx(
<g/>
posloupnost	posloupnost	k1gFnSc1
A005100	A005100	k1gFnSc2
v	v	k7c6
OEIS	OEIS	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
,	,	kIx,
14	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
,	,	kIx,
16	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
<g/>
,	,	kIx,
21	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
,	,	kIx,
27	#num#	k4
<g/>
,	,	kIx,
<g/>
…	…	k?
</s>
<s>
Jako	jako	k8xS,k8xC
příklad	příklad	k1gInSc4
uvažujme	uvažovat	k5eAaImRp1nP
např.	např.	kA
číslo	číslo	k1gNnSc4
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
děliteli	dělitel	k1gMnPc7
jsou	být	k5eAaImIp3nP
čísla	číslo	k1gNnSc2
1	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
a	a	k8xC
21	#num#	k4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
součet	součet	k1gInSc1
je	být	k5eAaImIp3nS
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
32	#num#	k4
<	<	kIx(
2	#num#	k4
<g/>
×	×	k?
<g/>
21	#num#	k4
=	=	kIx~
42	#num#	k4
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
21	#num#	k4
je	být	k5eAaImIp3nS
deficientní	deficientní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
deficience	deficienec	k1gInSc2
je	být	k5eAaImIp3nS
42	#num#	k4
−	−	k?
32	#num#	k4
=	=	kIx~
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
lichých	lichý	k2eAgInPc2d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
sudých	sudý	k2eAgNnPc2d1
deficientních	deficientní	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
existuje	existovat	k5eAaImIp3nS
nekonečně	konečně	k6eNd1
mnoho	mnoho	k4c1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
deficientních	deficientní	k2eAgFnPc2d1
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
méně	málo	k6eAd2
abundantních	abundantní	k2eAgNnPc2d1
a	a	k8xC
nejméně	málo	k6eAd3
dokonalých	dokonalý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
všechna	všechen	k3xTgNnPc1
prvočísla	prvočíslo	k1gNnPc1
jsou	být	k5eAaImIp3nP
deficientní	deficientní	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
i	i	k9
všechna	všechen	k3xTgFnSc1
poloprvočísla	poloprvočísnout	k5eAaPmAgFnS
a	a	k8xC
všichni	všechen	k3xTgMnPc1
dělitelé	dělitel	k1gMnPc1
deficientního	deficientní	k2eAgNnSc2d1
nebo	nebo	k8xC
dokonalého	dokonalý	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
všechny	všechen	k3xTgFnPc4
mocniny	mocnina	k1gFnPc4
prvočísel	prvočíslo	k1gNnPc2
nebo	nebo	k8xC
jiných	jiný	k2eAgFnPc2d1
mocnin	mocnina	k1gFnPc2
jsou	být	k5eAaImIp3nP
deficientní	deficientní	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Přirozená	přirozený	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
byla	být	k5eAaImAgNnP
buď	buď	k8xC
jako	jako	k9
deficientní	deficientní	k2eAgFnSc1d1
<g/>
,	,	kIx,
abundantní	abundantní	k2eAgFnSc1d1
nebo	nebo	k8xC
dokonalá	dokonalý	k2eAgFnSc1d1
klasifikována	klasifikován	k2eAgFnSc1d1
již	již	k6eAd1
řeckým	řecký	k2eAgMnSc7d1
matematikem	matematik	k1gMnSc7
Nikomachem	Nikomach	k1gMnSc7
v	v	k7c6
díle	dílo	k1gNnSc6
Introductio	Introductio	k1gMnSc1
Arithmetica	Arithmetica	k1gMnSc1
(	(	kIx(
<g/>
okolo	okolo	k7c2
roku	rok	k1gInSc2
100	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Deficient	deficient	k1gMnSc1
number	number	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dokonalé	dokonalý	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
</s>
<s>
Abundantní	abundantní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Mersennovo	Mersennův	k2eAgNnSc1d1
prvočíslo	prvočíslo	k1gNnSc1
</s>
<s>
GIMPS	GIMPS	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
