<s>
Deficientní	Deficientní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
matematice	matematika	k1gFnSc6
takové	takový	k3xDgNnSc1
číslo	číslo	k1gNnSc1
n	n	k0
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
součet	součet	k1gInSc1
všech	všecek	k3xTgInPc2
vlastních	vlastní	k2eAgInPc2d1
dělitelů	dělitel	k1gInPc2
kromě	kromě	k7c2
sebe	sebe	k3xPyFc4
samého	samý	k3xTgMnSc4
<g/>
.	.	kIx.
</s>