<s>
Jozef-Ernest	Jozef-Ernest	k1gInSc1
van	vana	k1gFnPc2
Roey	Roea	k1gFnSc2
</s>
<s>
Jozef-Ernest	Jozef-Ernest	k1gFnSc1
van	vana	k1gFnPc2
Roey	Roea	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1874	#num#	k4
<g/>
Belgie	Belgie	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1961	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
87	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Mechelen	Mechelna	k1gFnPc2
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
St.	st.	kA
Rumbold	Rumbold	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cathedral	Cathedral	k1gFnSc7
Národnost	národnost	k1gFnSc1
</s>
<s>
Belgičané	Belgičan	k1gMnPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
teolog	teolog	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
a	a	k8xC
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
katolický	katolický	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1926	#num#	k4
<g/>
)	)	kIx)
<g/>
katolický	katolický	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1926	#num#	k4
<g/>
)	)	kIx)
<g/>
kardinál	kardinál	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1927	#num#	k4
<g/>
)	)	kIx)
<g/>
apoštolský	apoštolský	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jozef-Ernest	Jozef-Ernest	k1gMnSc1
van	van	k1gMnSc1
Roey	Roey	k1gMnSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1874	#num#	k4
<g/>
,	,	kIx,
Vorselaar	Vorselaar	k1gInSc1
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1961	#num#	k4
<g/>
,	,	kIx,
Mechelen	Mechelen	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
belgický	belgický	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
od	od	k7c2
roku	rok	k1gInSc2
1926	#num#	k4
(	(	kIx(
<g/>
nahradil	nahradit	k5eAaPmAgMnS
zesnulého	zesnulý	k2eAgMnSc4d1
kardinála	kardinál	k1gMnSc4
Merciera	Mercier	k1gMnSc4
<g/>
)	)	kIx)
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
byl	být	k5eAaImAgInS
navíc	navíc	k6eAd1
kardinálem	kardinál	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
byl	být	k5eAaImAgMnS
absolventem	absolvent	k1gMnSc7
Katolické	katolický	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Lovani	Lovaň	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
získal	získat	k5eAaPmAgMnS
teologický	teologický	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1903	#num#	k4
zde	zde	k6eAd1
habilitoval	habilitovat	k5eAaBmAgInS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
120147912	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0898	#num#	k4
2916	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82090379	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
49216845	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82090379	#num#	k4
</s>
