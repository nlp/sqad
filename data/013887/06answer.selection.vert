<s desamb="1">
Jméno	jméno	k1gNnSc4
nesou	nést	k5eAaImIp3nP
po	po	k7c6
konstruktérovi	konstruktér	k1gMnSc6
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
zbraně	zbraň	k1gFnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
byl	být	k5eAaImAgMnS
plukovník	plukovník	k1gMnSc1
Arisaka	Arisaka	k1gMnSc1
Nariakira	Nariakira	k1gMnSc1xF
<g/>
,	,	kIx,
později	pozdě	k6eAd2
povýšený	povýšený	k2eAgMnSc1d1
na	na	k7c4
generálporučíka	generálporučík	k1gMnSc4
a	a	k8xC
barona	baron	k1gMnSc4
<g/>
.	.	kIx.
</s>