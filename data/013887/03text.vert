<s>
Arisaka	Arisak	k1gMnSc4
</s>
<s>
Typ	typ	k1gInSc1
99	#num#	k4
</s>
<s>
Arisaka	Arisak	k1gMnSc2
je	být	k5eAaImIp3nS
rodina	rodina	k1gFnSc1
japonských	japonský	k2eAgFnPc2d1
armádních	armádní	k2eAgFnPc2d1
opakovacích	opakovací	k2eAgFnPc2d1
pušek	puška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pušky	puška	k1gFnPc1
Arisaka	Arisak	k1gMnSc2
se	se	k3xPyFc4
v	v	k7c6
japonských	japonský	k2eAgFnPc6d1
ozbrojených	ozbrojený	k2eAgFnPc6d1
silách	síla	k1gFnPc6
používaly	používat	k5eAaImAgFnP
zhruba	zhruba	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1897	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nahradily	nahradit	k5eAaPmAgFnP
zbraně	zbraň	k1gFnPc1
rodiny	rodina	k1gFnSc2
Murata	Murata	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
konce	konec	k1gInSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc4
nesou	nést	k5eAaImIp3nP
po	po	k7c6
konstruktérovi	konstruktér	k1gMnSc6
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
zbraně	zbraň	k1gFnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
byl	být	k5eAaImAgMnS
plukovník	plukovník	k1gMnSc1
Arisaka	Arisaka	k1gMnSc1
Nariakira	Nariakira	k1gMnSc1xF
<g/>
,	,	kIx,
později	pozdě	k6eAd2
povýšený	povýšený	k2eAgMnSc1d1
na	na	k7c4
generálporučíka	generálporučík	k1gMnSc4
a	a	k8xC
barona	baron	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Varianty	varianta	k1gFnPc1
</s>
<s>
Arisaka	Arisak	k1gMnSc4
typ	typ	k1gInSc4
30	#num#	k4
</s>
<s>
Arisaka	Arisak	k1gMnSc4
typ	typ	k1gInSc4
38	#num#	k4
</s>
<s>
Arisaka	Arisak	k1gMnSc4
typ	typ	k1gInSc4
99	#num#	k4
</s>
<s>
Arisaka	Arisak	k1gMnSc2
typ	typ	k1gInSc1
I	i	k8xC
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
