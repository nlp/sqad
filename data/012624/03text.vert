<p>
<s>
Zvonící	zvonící	k2eAgInPc4d1	zvonící
meče	meč	k1gInPc4	meč
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
režírovaná	režírovaný	k2eAgFnSc1d1	režírovaná
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Sobotou	Sobota	k1gMnSc7	Sobota
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
pohádky	pohádka	k1gFnSc2	pohádka
Zkřížené	zkřížený	k2eAgInPc1d1	zkřížený
meče	meč	k1gInPc1	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
Vilém	Vilém	k1gMnSc1	Vilém
ze	z	k7c2	z
Salmu	Salm	k1gInSc2	Salm
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
že	že	k8xS	že
navštíví	navštívit	k5eAaPmIp3nS	navštívit
svoji	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Alexandru	Alexandra	k1gFnSc4	Alexandra
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
vévoda	vévoda	k1gMnSc1	vévoda
Gaston	Gaston	k1gInSc4	Gaston
domlouvá	domlouvat	k5eAaImIp3nS	domlouvat
s	s	k7c7	s
kouzelníkem	kouzelník	k1gMnSc7	kouzelník
Otónem	Otón	k1gMnSc7	Otón
na	na	k7c6	na
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
Otón	Otón	k1gNnSc4	Otón
vévodovi	vévoda	k1gMnSc3	vévoda
Gastonovi	Gaston	k1gMnSc3	Gaston
změní	změnit	k5eAaPmIp3nS	změnit
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
Krištofa	Krištof	k1gMnSc2	Krištof
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
panství	panství	k1gNnSc4	panství
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
přijde	přijít	k5eAaPmIp3nS	přijít
Agnes	Agnes	k1gInSc4	Agnes
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
vévody	vévoda	k1gMnSc2	vévoda
Gastona	Gaston	k1gMnSc2	Gaston
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prosí	prosit	k5eAaImIp3nS	prosit
o	o	k7c4	o
svolení	svolení	k1gNnSc4	svolení
poklonit	poklonit	k5eAaPmF	poklonit
se	se	k3xPyFc4	se
v	v	k7c6	v
hrobce	hrobka	k1gFnSc6	hrobka
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
<g/>
.	.	kIx.	.
</s>
<s>
Krištof	Krištof	k1gMnSc1	Krištof
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
hrobky	hrobka	k1gFnSc2	hrobka
doprovodí	doprovodit	k5eAaPmIp3nS	doprovodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hrobce	hrobka	k1gFnSc6	hrobka
čeká	čekat	k5eAaImIp3nS	čekat
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
vévody	vévoda	k1gMnSc2	vévoda
Gastona	Gaston	k1gMnSc2	Gaston
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c4	na
Krištofa	Krištof	k1gMnSc4	Krištof
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
za	za	k7c7	za
Krištofem	Krištof	k1gMnSc7	Krištof
přijede	přijet	k5eAaPmIp3nS	přijet
Valentin	Valentin	k1gMnSc1	Valentin
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přijede	přijet	k5eAaPmIp3nS	přijet
hrabě	hrabě	k1gMnSc1	hrabě
Vilém	Vilém	k1gMnSc1	Vilém
ze	z	k7c2	z
Salmu	Salm	k1gInSc2	Salm
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
Petr	Petr	k1gMnSc1	Petr
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Agnes	Agnes	k1gMnSc1	Agnes
doprovodí	doprovodit	k5eAaPmIp3nS	doprovodit
zraněného	zraněný	k2eAgMnSc4d1	zraněný
Krištofa	Krištof	k1gMnSc4	Krištof
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
komnaty	komnata	k1gFnSc2	komnata
a	a	k8xC	a
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
zajatec	zajatec	k1gMnSc1	zajatec
odveden	odveden	k2eAgInSc4d1	odveden
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
králem	král	k1gMnSc7	král
souzen	souzen	k2eAgMnSc1d1	souzen
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
svést	svést	k5eAaPmF	svést
cvičný	cvičný	k2eAgInSc1d1	cvičný
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
Valentinem	Valentin	k1gMnSc7	Valentin
a	a	k8xC	a
když	když	k8xS	když
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
bez	bez	k7c2	bez
trestu	trest	k1gInSc2	trest
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohraje	prohrát	k5eAaPmIp3nS	prohrát
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
sloužit	sloužit	k5eAaImF	sloužit
vítězi	vítěz	k1gMnPc7	vítěz
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
vítěz	vítěz	k1gMnSc1	vítěz
uzná	uznat	k5eAaPmIp3nS	uznat
za	za	k7c4	za
vhodné	vhodný	k2eAgNnSc4d1	vhodné
<g/>
.	.	kIx.	.
</s>
<s>
Alexandra	Alexandra	k1gFnSc1	Alexandra
požádá	požádat	k5eAaPmIp3nS	požádat
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
bojovat	bojovat	k5eAaImF	bojovat
místo	místo	k1gNnSc4	místo
Valentina	Valentin	k1gMnSc2	Valentin
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
po	po	k7c6	po
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
vyhoví	vyhovit	k5eAaPmIp3nS	vyhovit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
nebude	být	k5eNaImBp3nS	být
opravdový	opravdový	k2eAgInSc1d1	opravdový
souboj	souboj	k1gInSc1	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Petra	Petr	k1gMnSc4	Petr
je	být	k5eAaImIp3nS	být
ponižující	ponižující	k2eAgNnSc1d1	ponižující
bojovat	bojovat	k5eAaImF	bojovat
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
s	s	k7c7	s
Alexandrou	Alexandra	k1gFnSc7	Alexandra
prohraje	prohrát	k5eAaPmIp3nS	prohrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lesní	lesní	k2eAgFnSc1d1	lesní
žena	žena	k1gFnSc1	žena
Maruna	Maruna	k1gFnSc1	Maruna
vyčetla	vyčíst	k5eAaPmAgFnS	vyčíst
z	z	k7c2	z
kouzelných	kouzelný	k2eAgFnPc2d1	kouzelná
fazolí	fazole	k1gFnPc2	fazole
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodině	rodina	k1gFnSc6	rodina
hraběte	hrabě	k1gMnSc2	hrabě
Viléma	Vilém	k1gMnSc2	Vilém
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
varovat	varovat	k5eAaImF	varovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
ale	ale	k8xC	ale
dorazí	dorazit	k5eAaPmIp3nP	dorazit
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
pouze	pouze	k6eAd1	pouze
odjíždět	odjíždět	k5eAaImF	odjíždět
Helenku	Helenka	k1gFnSc4	Helenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otón	Otón	k1gMnSc1	Otón
mezitím	mezitím	k6eAd1	mezitím
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
dokončil	dokončit	k5eAaPmAgMnS	dokončit
proměnu	proměna	k1gFnSc4	proměna
vzhledu	vzhled	k1gInSc2	vzhled
vévody	vévoda	k1gMnSc2	vévoda
Gastona	Gaston	k1gMnSc2	Gaston
a	a	k8xC	a
připravuje	připravovat	k5eAaImIp3nS	připravovat
kouzlo	kouzlo	k1gNnSc4	kouzlo
ubližující	ubližující	k2eAgFnPc1d1	ubližující
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vévodovi	vévodův	k2eAgMnPc1d1	vévodův
Gastonovi	Gastonův	k2eAgMnPc1d1	Gastonův
překazili	překazit	k5eAaPmAgMnP	překazit
plány	plán	k1gInPc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tři	tři	k4xCgFnPc4	tři
kapky	kapka	k1gFnPc4	kapka
krve	krev	k1gFnSc2	krev
je	být	k5eAaImIp3nS	být
uvaleno	uvalen	k2eAgNnSc4d1	uvaleno
zlo	zlo	k1gNnSc4	zlo
na	na	k7c4	na
Krištofa	Krištof	k1gMnSc4	Krištof
<g/>
,	,	kIx,	,
Alexandru	Alexandra	k1gFnSc4	Alexandra
a	a	k8xC	a
Valentina	Valentin	k1gMnSc4	Valentin
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
uskutečněním	uskutečnění	k1gNnSc7	uskutečnění
kouzla	kouzlo	k1gNnSc2	kouzlo
ho	on	k3xPp3gMnSc4	on
kouzelník	kouzelník	k1gMnSc1	kouzelník
Otón	Otón	k1gMnSc1	Otón
varuje	varovat	k5eAaImIp3nS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
láska	láska	k1gFnSc1	láska
kouzlo	kouzlo	k1gNnSc4	kouzlo
obrátí	obrátit	k5eAaPmIp3nS	obrátit
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
samotnému	samotný	k2eAgNnSc3d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Alexandra	Alexandra	k1gFnSc1	Alexandra
je	být	k5eAaImIp3nS	být
kouzlem	kouzlo	k1gNnSc7	kouzlo
unesena	unesen	k2eAgMnSc4d1	unesen
do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
kouzelníka	kouzelník	k1gMnSc2	kouzelník
Otóna	Otón	k1gMnSc2	Otón
hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
se	se	k3xPyFc4	se
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
Otónovým	Otónův	k2eAgMnSc7d1	Otónův
sluhou	sluha	k1gMnSc7	sluha
Alíkem	Alíek	k1gMnSc7	Alíek
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
učí	učit	k5eAaImIp3nP	učit
šermovat	šermovat	k5eAaImF	šermovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lupiči	lupič	k1gMnPc1	lupič
zastaví	zastavit	k5eAaPmIp3nP	zastavit
Helenčin	Helenčin	k2eAgInSc4d1	Helenčin
kočár	kočár	k1gInSc4	kočár
a	a	k8xC	a
Helenku	Helenka	k1gFnSc4	Helenka
unesou	unést	k5eAaPmIp3nP	unést
do	do	k7c2	do
Otónovy	Otónův	k2eAgFnSc2d1	Otónův
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Kouzelník	kouzelník	k1gMnSc1	kouzelník
Otón	Otón	k1gMnSc1	Otón
si	se	k3xPyFc3	se
Helenku	Helenka	k1gFnSc4	Helenka
splete	splést	k5eAaPmIp3nS	splést
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jí	on	k3xPp3gFnSc3	on
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
a	a	k8xC	a
osloví	oslovit	k5eAaPmIp3nS	oslovit
jí	on	k3xPp3gFnSc3	on
jménem	jméno	k1gNnSc7	jméno
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Helenka	Helenka	k1gFnSc1	Helenka
mu	on	k3xPp3gMnSc3	on
odvětí	odvětit	k5eAaPmIp3nS	odvětit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
Helena	Helena	k1gFnSc1	Helena
ze	z	k7c2	z
Salmu	Salm	k1gInSc2	Salm
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
uslyší	uslyšet	k5eAaPmIp3nS	uslyšet
Alík	Alík	k1gInSc1	Alík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
ví	vědět	k5eAaImIp3nS	vědět
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
hraběcí	hraběcí	k2eAgMnSc1d1	hraběcí
syn	syn	k1gMnSc1	syn
ze	z	k7c2	z
Salmu	Salm	k1gInSc2	Salm
<g/>
.	.	kIx.	.
</s>
<s>
Helenka	Helenka	k1gFnSc1	Helenka
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
se	s	k7c7	s
zajatou	zajatý	k2eAgFnSc7d1	zajatá
Alexandrou	Alexandra	k1gFnSc7	Alexandra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Princ	princ	k1gMnSc1	princ
Valentin	Valentin	k1gMnSc1	Valentin
a	a	k8xC	a
hrabě	hrabě	k1gMnSc1	hrabě
Vilém	Vilém	k1gMnSc1	Vilém
s	s	k7c7	s
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
Alexandru	Alexandr	k1gMnSc3	Alexandr
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
<s>
Jede	jet	k5eAaImIp3nS	jet
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prohrál	prohrát	k5eAaPmAgMnS	prohrát
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
jdou	jít	k5eAaImIp3nP	jít
za	za	k7c7	za
lesní	lesní	k2eAgFnSc7d1	lesní
ženou	žena	k1gFnSc7	žena
Marunou	Maruna	k1gFnSc7	Maruna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
dá	dát	k5eAaPmIp3nS	dát
kouzelné	kouzelný	k2eAgNnSc4d1	kouzelné
světélko	světélko	k1gNnSc4	světélko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
je	být	k5eAaImIp3nS	být
odvede	odvést	k5eAaPmIp3nS	odvést
za	za	k7c7	za
Alexandrou	Alexandra	k1gFnSc7	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
si	se	k3xPyFc3	se
světélko	světélko	k1gNnSc1	světélko
dá	dát	k5eAaPmIp3nS	dát
pod	pod	k7c4	pod
čepici	čepice	k1gFnSc4	čepice
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
neviditelným	viditelný	k2eNgMnSc7d1	Neviditelný
<g/>
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1	lesní
žena	žena	k1gFnSc1	žena
Maruna	Maruna	k1gFnSc1	Maruna
hraběti	hrabě	k1gMnSc3	hrabě
Vilémovi	Vilém	k1gMnSc3	Vilém
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Helenku	Helenka	k1gFnSc4	Helenka
mají	mít	k5eAaImIp3nP	mít
hledat	hledat	k5eAaImF	hledat
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
jako	jako	k8xC	jako
Alexandru	Alexandr	k1gMnSc3	Alexandr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lupiči	lupič	k1gMnPc1	lupič
přepadnou	přepadnout	k5eAaPmIp3nP	přepadnout
prince	princ	k1gMnSc2	princ
Valentina	Valentin	k1gMnSc2	Valentin
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc2	hrabě
Viléma	Vilém	k1gMnSc2	Vilém
a	a	k8xC	a
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
si	se	k3xPyFc3	se
rychle	rychle	k6eAd1	rychle
dá	dát	k5eAaPmIp3nS	dát
pod	pod	k7c4	pod
čepici	čepice	k1gFnSc4	čepice
světélko	světélko	k1gNnSc4	světélko
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
neviditelným	viditelný	k2eNgMnSc7d1	Neviditelný
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
lupičů	lupič	k1gMnPc2	lupič
se	se	k3xPyFc4	se
diví	divit	k5eAaImIp3nS	divit
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
zmizel	zmizet	k5eAaPmAgMnS	zmizet
jejich	jejich	k3xOp3gMnSc1	jejich
třetí	třetí	k4xOgMnSc1	třetí
společník	společník	k1gMnSc1	společník
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
Valentin	Valentin	k1gMnSc1	Valentin
ve	v	k7c6	v
veliteli	velitel	k1gMnPc7	velitel
lupičů	lupič	k1gMnPc2	lupič
poznává	poznávat	k5eAaImIp3nS	poznávat
Huga	Hugo	k1gMnSc2	Hugo
<g/>
,	,	kIx,	,
velitele	velitel	k1gMnSc2	velitel
bývalého	bývalý	k2eAgNnSc2d1	bývalé
vojska	vojsko	k1gNnSc2	vojsko
vévody	vévoda	k1gMnSc2	vévoda
Gastona	Gaston	k1gMnSc2	Gaston
<g/>
.	.	kIx.	.
</s>
<s>
Neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
Petr	Petr	k1gMnSc1	Petr
odvede	odvést	k5eAaPmIp3nS	odvést
koně	kůň	k1gMnSc4	kůň
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Valentin	Valentin	k1gMnSc1	Valentin
a	a	k8xC	a
Vilém	Vilém	k1gMnSc1	Vilém
bojují	bojovat	k5eAaImIp3nP	bojovat
s	s	k7c7	s
lupiči	lupič	k1gMnPc7	lupič
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
prohledává	prohledávat	k5eAaImIp3nS	prohledávat
jeskyni	jeskyně	k1gFnSc4	jeskyně
kouzelníka	kouzelník	k1gMnSc2	kouzelník
Otóna	Otón	k1gMnSc2	Otón
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hlas	hlas	k1gInSc1	hlas
promluví	promluvit	k5eAaPmIp3nS	promluvit
k	k	k7c3	k
Helence	Helenka	k1gFnSc3	Helenka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
vezme	vzít	k5eAaPmIp3nS	vzít
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	on	k3xPp3gMnPc4	on
vysvobodí	vysvobodit	k5eAaPmIp3nS	vysvobodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Valentinovi	Valentin	k1gMnSc3	Valentin
a	a	k8xC	a
Vilémovi	Vilém	k1gMnSc3	Vilém
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
mezitím	mezitím	k6eAd1	mezitím
přemoženi	přemoct	k5eAaPmNgMnP	přemoct
Hugovou	Hugův	k2eAgFnSc7d1	Hugova
bandou	banda	k1gFnSc7	banda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bandě	banda	k1gFnSc6	banda
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
rozbroje	rozbroj	k1gInSc2	rozbroj
a	a	k8xC	a
Valentinovi	Valentin	k1gMnSc3	Valentin
a	a	k8xC	a
Vilémovi	Vilém	k1gMnSc3	Vilém
pomůže	pomoct	k5eAaPmIp3nS	pomoct
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
pout	pout	k1gInSc1	pout
a	a	k8xC	a
přinese	přinést	k5eAaPmIp3nS	přinést
jim	on	k3xPp3gFnPc3	on
meče	meč	k1gInPc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
Otón	Otóna	k1gFnPc2	Otóna
prosí	prosit	k5eAaImIp3nS	prosit
Alíka	Alíka	k1gFnSc1	Alíka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
měsíční	měsíční	k2eAgFnPc4d1	měsíční
kapky	kapka	k1gFnPc4	kapka
<g/>
,	,	kIx,	,
Alík	Alík	k1gInSc1	Alík
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
vyleje	vylít	k5eAaPmIp3nS	vylít
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
zesláblému	zesláblý	k2eAgMnSc3d1	zesláblý
čaroději	čaroděj	k1gMnSc3	čaroděj
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
Alexandra	Alexandr	k1gMnSc4	Alexandr
<g/>
,	,	kIx,	,
Helenka	Helenka	k1gFnSc1	Helenka
i	i	k8xC	i
Alík	Alík	k1gInSc4	Alík
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vyjdou	vyjít	k5eAaPmIp3nP	vyjít
před	před	k7c4	před
Otónovu	Otónův	k2eAgFnSc4d1	Otónův
jeskyni	jeskyně	k1gFnSc4	jeskyně
a	a	k8xC	a
uvidí	uvidět	k5eAaPmIp3nS	uvidět
souboj	souboj	k1gInSc4	souboj
Valentina	Valentin	k1gMnSc2	Valentin
<g/>
,	,	kIx,	,
Viléma	Vilém	k1gMnSc2	Vilém
a	a	k8xC	a
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
opět	opět	k6eAd1	opět
viditelného	viditelný	k2eAgMnSc4d1	viditelný
Petra	Petr	k1gMnSc4	Petr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
Helence	Helenka	k1gFnSc3	Helenka
chce	chtít	k5eAaImIp3nS	chtít
ukázat	ukázat	k5eAaPmF	ukázat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
zapojí	zapojit	k5eAaPmIp3nS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
souboje	souboj	k1gInSc2	souboj
Alexandra	Alexandr	k1gMnSc2	Alexandr
i	i	k8xC	i
Alík	Alík	k1gInSc4	Alík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
vítězství	vítězství	k1gNnSc6	vítězství
Alexandra	Alexandra	k1gFnSc1	Alexandra
prozradí	prozradit	k5eAaPmIp3nS	prozradit
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alík	Alík	k1gMnSc1	Alík
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
ztracený	ztracený	k2eAgMnSc1d1	ztracený
syn	syn	k1gMnSc1	syn
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
.	.	kIx.	.
</s>
<s>
Helenka	Helenka	k1gFnSc1	Helenka
se	se	k3xPyFc4	se
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
vdát	vdát	k5eAaPmF	vdát
za	za	k7c4	za
Petra	Petr	k1gMnSc4	Petr
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hrabě	hrabě	k1gMnSc1	hrabě
Vilém	Vilém	k1gMnSc1	Vilém
už	už	k6eAd1	už
nezůstane	zůstat	k5eNaPmIp3nS	zůstat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
když	když	k8xS	když
našel	najít	k5eAaPmAgMnS	najít
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uzdravený	uzdravený	k2eAgMnSc1d1	uzdravený
Krištof	Krištof	k1gMnSc1	Krištof
přijel	přijet	k5eAaPmAgMnS	přijet
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
rozmlouvá	rozmlouvat	k5eAaImIp3nS	rozmlouvat
s	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
mu	on	k3xPp3gMnSc3	on
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Valentin	Valentin	k1gMnSc1	Valentin
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
odjeli	odjet	k5eAaPmAgMnP	odjet
<g/>
.	.	kIx.	.
</s>
<s>
Krištof	Krištof	k1gMnSc1	Krištof
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
že	že	k8xS	že
možná	možná	k9	možná
bylo	být	k5eAaImAgNnS	být
unáhlené	unáhlený	k2eAgNnSc1d1	unáhlené
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
panství	panství	k1gNnSc1	panství
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
bratrovi	bratr	k1gMnSc6	bratr
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
nárok	nárok	k1gInSc4	nárok
děti	dítě	k1gFnPc1	dítě
vévody	vévoda	k1gMnSc2	vévoda
Gastona	Gastona	k1gFnSc1	Gastona
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
ale	ale	k8xC	ale
Krištofa	Krištof	k1gMnSc4	Krištof
ujišťuje	ujišťovat	k5eAaImIp3nS	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
mají	mít	k5eAaImIp3nP	mít
ještě	ještě	k6eAd1	ještě
svá	svůj	k3xOyFgNnPc4	svůj
panství	panství	k1gNnPc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Krištof	Krištof	k1gMnSc1	Krištof
se	se	k3xPyFc4	se
královi	králův	k2eAgMnPc1d1	králův
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbí	líbit	k5eAaImIp3nS	líbit
Agnes	Agnes	k1gInSc1	Agnes
<g/>
.	.	kIx.	.
</s>
<s>
Agnes	Agnes	k1gInSc1	Agnes
přichází	přicházet	k5eAaImIp3nS	přicházet
za	za	k7c7	za
králem	král	k1gMnSc7	král
prosit	prosit	k5eAaImF	prosit
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
Krištof	Krištof	k1gMnSc1	Krištof
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
skryje	skrýt	k5eAaPmIp3nS	skrýt
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Agnes	Agnes	k1gMnSc1	Agnes
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
bratr	bratr	k1gMnSc1	bratr
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgMnSc1d1	volný
a	a	k8xC	a
ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
potkala	potkat	k5eAaPmAgFnS	potkat
nějakého	nějaký	k3yIgMnSc4	nějaký
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
příkaz	příkaz	k1gInSc4	příkaz
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
už	už	k6eAd1	už
zapomněl	zapomnět	k5eAaImAgMnS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Krištof	Krištof	k1gMnSc1	Krištof
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
úkrytu	úkryt	k1gInSc2	úkryt
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
Agnes	Agnes	k1gInSc4	Agnes
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
nemůže	moct	k5eNaImIp3nS	moct
zapomenout	zapomenout	k5eAaPmF	zapomenout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gaston	Gaston	k1gInSc1	Gaston
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zabít	zabít	k5eAaPmF	zabít
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Gastonova	Gastonův	k2eAgFnSc1d1	Gastonova
dcera	dcera	k1gFnSc1	dcera
Agnes	Agnes	k1gInSc1	Agnes
s	s	k7c7	s
Krištofem	Krištof	k1gMnSc7	Krištof
<g/>
.	.	kIx.	.
</s>
<s>
Gaston	Gaston	k1gInSc1	Gaston
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgInS	chtít
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obrátí	obrátit	k5eAaPmIp3nP	obrátit
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
zničí	zničit	k5eAaPmIp3nS	zničit
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Kouzelníka	kouzelník	k1gMnSc4	kouzelník
Otóna	Otón	k1gMnSc4	Otón
se	se	k3xPyFc4	se
ujme	ujmout	k5eAaPmIp3nS	ujmout
lesní	lesní	k2eAgFnSc1d1	lesní
žena	žena	k1gFnSc1	žena
Maruna	Maruna	k1gFnSc1	Maruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kouzelník	kouzelník	k1gMnSc1	kouzelník
Otón	Otón	k1gMnSc1	Otón
a	a	k8xC	a
lupiči	lupič	k1gMnPc1	lupič
jsou	být	k5eAaImIp3nP	být
odvedeni	odvést	k5eAaPmNgMnP	odvést
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Valentin	Valentin	k1gMnSc1	Valentin
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gaston	Gaston	k1gInSc1	Gaston
připravuje	připravovat	k5eAaImIp3nS	připravovat
něco	něco	k3yInSc4	něco
dalšího	další	k2eAgMnSc2d1	další
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
král	král	k1gMnSc1	král
ho	on	k3xPp3gNnSc4	on
uklidní	uklidnit	k5eAaPmIp3nS	uklidnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gaston	Gaston	k1gInSc1	Gaston
se	se	k3xPyFc4	se
rozplynul	rozplynout	k5eAaPmAgInS	rozplynout
jakou	jaký	k3yIgFnSc7	jaký
kouř	kouř	k1gInSc4	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Kouzelník	kouzelník	k1gMnSc1	kouzelník
Otón	Otón	k1gMnSc1	Otón
se	se	k3xPyFc4	se
pošklebuje	pošklebovat	k5eAaImIp3nS	pošklebovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gastona	Gaston	k1gMnSc4	Gaston
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
přijede	přijet	k5eAaPmIp3nS	přijet
komorná	komorná	k1gFnSc1	komorná
hraběte	hrabě	k1gMnSc2	hrabě
ze	z	k7c2	z
Salmu	Salm	k1gInSc2	Salm
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
doma	doma	k6eAd1	doma
samotnou	samotný	k2eAgFnSc7d1	samotná
nebavilo	bavit	k5eNaImAgNnS	bavit
<g/>
,	,	kIx,	,
a	a	k8xC	a
hrabě	hrabě	k1gMnSc1	hrabě
Vilém	Vilém	k1gMnSc1	Vilém
jí	on	k3xPp3gFnSc3	on
představuje	představovat	k5eAaImIp3nS	představovat
Alíka	Alíka	k1gFnSc1	Alíka
jako	jako	k8xC	jako
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Komorná	komorná	k1gFnSc1	komorná
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pořád	pořád	k6eAd1	pořád
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
tak	tak	k6eAd1	tak
hezkého	hezký	k2eAgNnSc2d1	hezké
děťátka	děťátko	k1gNnSc2	děťátko
se	se	k3xPyFc4	se
nemohlo	moct	k5eNaImAgNnS	moct
stát	stát	k5eAaPmF	stát
podsvinče	podsvinče	k1gNnSc1	podsvinče
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
přispěchá	přispěchat	k5eAaPmIp3nS	přispěchat
i	i	k9	i
lesní	lesní	k2eAgFnSc1d1	lesní
žena	žena	k1gFnSc1	žena
Maruna	Maruna	k1gFnSc1	Maruna
varovat	varovat	k5eAaImF	varovat
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
(	(	kIx(	(
<g/>
přichází	přicházet	k5eAaImIp3nS	přicházet
pozdě	pozdě	k6eAd1	pozdě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
král	král	k1gMnSc1	král
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
udělat	udělat	k5eAaPmF	udělat
s	s	k7c7	s
Otónem	Otón	k1gInSc7	Otón
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnSc1d1	lesní
žena	žena	k1gFnSc1	žena
Maruna	Maruna	k1gFnSc1	Maruna
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
ujímá	ujímat	k5eAaImIp3nS	ujímat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Princ	princ	k1gMnSc1	princ
Valentin	Valentin	k1gMnSc1	Valentin
chce	chtít	k5eAaImIp3nS	chtít
připravit	připravit	k5eAaPmF	připravit
souboj	souboj	k1gInSc4	souboj
jako	jako	k8xC	jako
slavnost	slavnost	k1gFnSc4	slavnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
král	král	k1gMnSc1	král
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
už	už	k9	už
meče	meč	k1gInPc1	meč
přestaly	přestat	k5eAaPmAgInP	přestat
zvonit	zvonit	k5eAaImF	zvonit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Zvonící	zvonící	k2eAgInPc1d1	zvonící
meče	meč	k1gInPc1	meč
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
</s>
</p>
