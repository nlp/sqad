<p>
<s>
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Oleksandrovyč	Oleksandrovyč	k1gMnSc1	Oleksandrovyč
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
В	В	k?	В
<g/>
́	́	k?	́
<g/>
м	м	k?	м
О	О	k?	О
<g/>
́	́	k?	́
<g/>
н	н	k?	н
З	З	k?	З
<g/>
́	́	k?	́
<g/>
н	н	k?	н
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
В	В	k?	В
<g/>
́	́	k?	́
<g/>
м	м	k?	м
А	А	k?	А
<g/>
́	́	k?	́
<g/>
н	н	k?	н
З	З	k?	З
<g/>
́	́	k?	́
<g/>
н	н	k?	н
–	–	k?	–
Vladimir	Vladimir	k1gMnSc1	Vladimir
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Zelenskij	Zelenskij	k1gMnSc1	Zelenskij
<g/>
,	,	kIx,	,
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Krivoj	Krivoj	k1gInSc1	Krivoj
Rog	Rog	k1gFnSc1	Rog
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
dabér	dabér	k1gMnSc1	dabér
<g/>
,	,	kIx,	,
scénárista	scénárista	k1gMnSc1	scénárista
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgMnSc1d1	televizní
moderátor	moderátor	k1gMnSc1	moderátor
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
studia	studio	k1gNnSc2	studio
Kvartal	Kvartal	k1gFnSc2	Kvartal
95	[number]	k4	95
(	(	kIx(	(
<g/>
К	К	k?	К
<g/>
95	[number]	k4	95
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
ukrajinského	ukrajinský	k2eAgMnSc4d1	ukrajinský
prezidenta	prezident	k1gMnSc4	prezident
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
prezidenta	prezident	k1gMnSc4	prezident
Petra	Petr	k1gMnSc4	Petr
Porošenka	Porošenka	k1gFnSc1	Porošenka
ziskem	zisk	k1gInSc7	zisk
73	[number]	k4	73
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
v	v	k7c6	v
městě	město	k1gNnSc6	město
Krivoj	Krivoj	k1gInSc4	Krivoj
Rog	Rog	k1gFnPc1	Rog
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
profesorem	profesor	k1gMnSc7	profesor
kybernetiky	kybernetika	k1gFnSc2	kybernetika
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
inženýrka	inženýrka	k1gFnSc1	inženýrka
<g/>
.	.	kIx.	.
</s>
<s>
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
Kyjevské	kyjevský	k2eAgFnSc6d1	Kyjevská
národní	národní	k2eAgFnSc6d1	národní
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
profesionálně	profesionálně	k6eAd1	profesionálně
nepůsobil	působit	k5eNaImAgInS	působit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
účastnil	účastnit	k5eAaImAgMnS	účastnit
televizní	televizní	k2eAgFnSc2d1	televizní
humoristické	humoristický	k2eAgFnSc2d1	humoristická
soutěže	soutěž	k1gFnSc2	soutěž
KVN	KVN	kA	KVN
populární	populární	k2eAgFnSc1d1	populární
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
KVN	KVN	kA	KVN
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
Kvartal	Kvartal	k1gFnSc1	Kvartal
95	[number]	k4	95
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yQgFnSc7	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2003	[number]	k4	2003
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
nejvyšších	vysoký	k2eAgNnPc6d3	nejvyšší
kolech	kolo	k1gNnPc6	kolo
KVN	KVN	kA	KVN
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
post-sovětských	postovětský	k2eAgFnPc6d1	post-sovětská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
měla	mít	k5eAaImAgFnS	mít
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
začal	začít	k5eAaPmAgInS	začít
Kvartal	Kvartal	k1gFnSc2	Kvartal
95	[number]	k4	95
produkovat	produkovat	k5eAaImF	produkovat
televizní	televizní	k2eAgInSc4d1	televizní
pořady	pořad	k1gInPc4	pořad
pro	pro	k7c4	pro
ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
televizi	televize	k1gFnSc4	televize
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
Inter	Inter	k1gInSc1	Inter
<g/>
.	.	kIx.	.
<g/>
Známější	známý	k2eAgMnSc1d2	známější
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
účasti	účast	k1gFnSc3	účast
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Tanec	tanec	k1gInSc1	tanec
s	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
(	(	kIx(	(
<g/>
Т	Т	k?	Т
з	з	k?	з
з	з	k?	з
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
ruských	ruský	k2eAgInPc6d1	ruský
filmech	film	k1gInPc6	film
Láska	láska	k1gFnSc1	láska
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
Л	Л	k?	Л
в	в	k?	в
б	б	k?	б
г	г	k?	г
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
dvou	dva	k4xCgFnPc6	dva
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
,	,	kIx,	,
Služební	služební	k2eAgInSc4d1	služební
román	román	k1gInSc4	román
-	-	kIx~	-
Náš	náš	k3xOp1gInSc1	náš
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
С	С	k?	С
р	р	k?	р
-	-	kIx~	-
Н	Н	k?	Н
в	в	k?	в
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rusko-ukrajinském	ruskokrajinský	k2eAgInSc6d1	rusko-ukrajinský
filmu	film	k1gInSc6	film
Rževskij	Rževskij	k1gMnSc2	Rževskij
proti	proti	k7c3	proti
<g />
.	.	kIx.	.
</s>
<s>
Napoleonovi	Napoleonův	k2eAgMnPc1d1	Napoleonův
(	(	kIx(	(
<g/>
Р	Р	k?	Р
п	п	k?	п
Н	Н	k?	Н
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
8	[number]	k4	8
prvních	první	k4xOgFnPc2	první
schůzek	schůzka	k1gFnPc2	schůzka
(	(	kIx(	(
<g/>
8	[number]	k4	8
п	п	k?	п
с	с	k?	с
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
dvou	dva	k4xCgFnPc6	dva
pokračování	pokračování	k1gNnSc1	pokračování
<g/>
.	.	kIx.	.
<g/>
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
podpořil	podpořit	k5eAaPmAgMnS	podpořit
hnutí	hnutí	k1gNnSc4	hnutí
Euromajdan	Euromajdan	k1gMnSc1	Euromajdan
a	a	k8xC	a
hovořil	hovořit	k5eAaImAgMnS	hovořit
před	před	k7c7	před
jeho	jeho	k3xOp3gMnPc7	jeho
účastníky	účastník	k1gMnPc7	účastník
<g/>
,	,	kIx,	,
finančně	finančně	k6eAd1	finančně
také	také	k9	také
podpořil	podpořit	k5eAaPmAgInS	podpořit
ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
armádu	armáda	k1gFnSc4	armáda
na	na	k7c6	na
Donbasu	Donbas	k1gInSc6	Donbas
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
populárním	populární	k2eAgInSc6d1	populární
komediálním	komediální	k2eAgInSc6d1	komediální
seriálu	seriál	k1gInSc6	seriál
Služebník	služebník	k1gMnSc1	služebník
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
С	С	k?	С
н	н	k?	н
<g/>
)	)	kIx)	)
fiktivního	fiktivní	k2eAgMnSc2d1	fiktivní
ukrajinského	ukrajinský	k2eAgMnSc2d1	ukrajinský
prezidenta	prezident	k1gMnSc2	prezident
Vasilije	Vasilije	k1gFnSc1	Vasilije
Holoborodka	Holoborodka	k1gFnSc1	Holoborodka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
představuje	představovat	k5eAaImIp3nS	představovat
mladého	mladý	k2eAgMnSc4d1	mladý
středoškolského	středoškolský	k2eAgMnSc4d1	středoškolský
učitele	učitel	k1gMnSc4	učitel
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
virální	virální	k2eAgNnSc1d1	virální
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
zkorumpovanou	zkorumpovaný	k2eAgFnSc4d1	zkorumpovaná
ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
Služebník	služebník	k1gMnSc1	služebník
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
kandiduje	kandidovat	k5eAaImIp3nS	kandidovat
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
právě	právě	k9	právě
podle	podle	k7c2	podle
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
ukrajinských	ukrajinský	k2eAgFnPc6d1	ukrajinská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
porazil	porazit	k5eAaPmAgMnS	porazit
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
prezidenta	prezident	k1gMnSc4	prezident
Petra	Petr	k1gMnSc2	Petr
Porošenka	Porošenka	k1gFnSc1	Porošenka
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
74	[number]	k4	74
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodná	rodný	k2eAgFnSc1d1	rodná
řeč	řeč	k1gFnSc1	řeč
Zelenského	Zelenský	k2eAgInSc2d1	Zelenský
je	být	k5eAaImIp3nS	být
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
však	však	k9	však
také	také	k9	také
plynule	plynule	k6eAd1	plynule
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
.	.	kIx.	.
</s>
<s>
Zelenskyj	Zelenskyj	k1gFnSc1	Zelenskyj
hovoří	hovořit	k5eAaImIp3nS	hovořit
také	také	k9	také
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kandidatura	kandidatura	k1gFnSc1	kandidatura
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
2018	[number]	k4	2018
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
v	v	k7c6	v
ukrajinských	ukrajinský	k2eAgFnPc6d1	ukrajinská
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
února	únor	k1gInSc2	únor
jej	on	k3xPp3gNnSc2	on
předvolební	předvolební	k2eAgInPc1d1	předvolební
průzkumy	průzkum	k1gInPc1	průzkum
považovaly	považovat	k5eAaImAgInP	považovat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
favoritů	favorit	k1gMnPc2	favorit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
získal	získat	k5eAaPmAgInS	získat
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
z	z	k7c2	z
39	[number]	k4	39
kandidátů	kandidát	k1gMnPc2	kandidát
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
Petrem	Petr	k1gMnSc7	Petr
Porošenkem	Porošenek	k1gMnSc7	Porošenek
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
Služebník	služebník	k1gMnSc1	služebník
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
antisystémového	antisystémový	k2eAgMnSc4d1	antisystémový
kandidáta	kandidát	k1gMnSc4	kandidát
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
především	především	k9	především
proti	proti	k7c3	proti
všudypřítomné	všudypřítomný	k2eAgFnSc3d1	všudypřítomná
korupci	korupce	k1gFnSc3	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
by	by	kYmCp3nS	by
</s>
</p>
<p>
<s>
členství	členství	k1gNnSc1	členství
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
v	v	k7c6	v
EU	EU	kA	EU
i	i	k9	i
v	v	k7c6	v
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
obojím	obojí	k4xRgMnSc7	obojí
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
měli	mít	k5eAaImAgMnP	mít
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
formou	forma	k1gFnSc7	forma
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
snažit	snažit	k5eAaImF	snažit
ukončit	ukončit	k5eAaPmF	ukončit
probíhající	probíhající	k2eAgInSc4d1	probíhající
konflikt	konflikt	k1gInSc4	konflikt
v	v	k7c6	v
Donbasu	Donbas	k1gInSc6	Donbas
jednáním	jednání	k1gNnSc7	jednání
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
nevylučoval	vylučovat	k5eNaImAgMnS	vylučovat
pořádání	pořádání	k1gNnSc4	pořádání
referenda	referendum	k1gNnSc2	referendum
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
záležitosti	záležitost	k1gFnSc6	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
existovaly	existovat	k5eAaImAgInP	existovat
dva	dva	k4xCgInPc1	dva
scénáře	scénář	k1gInPc1	scénář
pro	pro	k7c4	pro
návrat	návrat	k1gInSc4	návrat
Donbasu	Donbas	k1gInSc2	Donbas
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
informační	informační	k2eAgFnSc4d1	informační
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
by	by	kYmCp3nS	by
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Donbasu	Donbas	k1gInSc2	Donbas
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
měli	mít	k5eAaImAgMnP	mít
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
orgány	orgány	k1gFnPc4	orgány
ukázat	ukázat	k5eAaPmF	ukázat
obyvatelům	obyvatel	k1gMnPc3	obyvatel
konfliktních	konfliktní	k2eAgInPc2d1	konfliktní
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
považují	považovat	k5eAaImIp3nP	považovat
stále	stále	k6eAd1	stále
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Měli	mít	k5eAaImAgMnP	mít
bychom	by	kYmCp1nP	by
jim	on	k3xPp3gMnPc3	on
platit	platit	k5eAaImF	platit
důchody	důchod	k1gInPc4	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
ztráta	ztráta	k1gFnSc1	ztráta
pro	pro	k7c4	pro
rozpočet	rozpočet	k1gInSc4	rozpočet
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Luhansk	Luhansk	k1gInSc4	Luhansk
a	a	k8xC	a
Doněck	Doněck	k1gInSc4	Doněck
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
součástí	součást	k1gFnSc7	součást
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
realisticky	realisticky	k6eAd1	realisticky
vzato	vzat	k2eAgNnSc1d1	vzato
Krym	Krym	k1gInSc4	Krym
nyní	nyní	k6eAd1	nyní
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vrátit	vrátit	k5eAaPmF	vrátit
pod	pod	k7c4	pod
ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
jedině	jedině	k6eAd1	jedině
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Krivého	Krivý	k2eAgInSc2d1	Krivý
Rogu	Rogus	k1gInSc2	Rogus
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
ruskojazyčnou	ruskojazyčný	k2eAgFnSc7d1	ruskojazyčná
menšinou	menšina	k1gFnSc7	menšina
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
mluví	mluvit	k5eAaImIp3nS	mluvit
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dalších	další	k2eAgInPc2d1	další
dvou	dva	k4xCgInPc2	dva
favorizovaných	favorizovaný	k2eAgMnPc2d1	favorizovaný
kandidátů	kandidát	k1gMnPc2	kandidát
–	–	k?	–
Petra	Petra	k1gFnSc1	Petra
Porošenka	Porošenka	k1gFnSc1	Porošenka
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
Tymošenkové	Tymošenkový	k2eAgFnSc2d1	Tymošenková
–	–	k?	–
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
potlačování	potlačování	k1gNnSc3	potlačování
ruského	ruský	k2eAgInSc2d1	ruský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
zákazu	zákaz	k1gInSc2	zákaz
umělců	umělec	k1gMnPc2	umělec
za	za	k7c4	za
jejich	jejich	k3xOp3gInPc4	jejich
politické	politický	k2eAgInPc4d1	politický
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
získal	získat	k5eAaPmAgMnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
postoupil	postoupit	k5eAaPmAgInS	postoupit
–	–	k?	–
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Porošenkem	Porošenek	k1gMnSc7	Porošenek
–	–	k?	–
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přineslo	přinést	k5eAaPmAgNnS	přinést
podle	podle	k7c2	podle
očekávání	očekávání	k1gNnSc2	očekávání
vítězství	vítězství	k1gNnSc2	vítězství
Zelenského	Zelenský	k2eAgNnSc2d1	Zelenský
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volební	volební	k2eAgFnSc6d1	volební
účasti	účast	k1gFnSc6	účast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
předběžně	předběžně	k6eAd1	předběžně
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
58	[number]	k4	58
%	%	kIx~	%
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
vítězný	vítězný	k2eAgMnSc1d1	vítězný
kandidát	kandidát	k1gMnSc1	kandidát
podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
provedených	provedený	k2eAgInPc2d1	provedený
u	u	k7c2	u
volebních	volební	k2eAgFnPc2d1	volební
uren	urna	k1gFnPc2	urna
74	[number]	k4	74
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
prezidenta	prezident	k1gMnSc4	prezident
Porošenka	Porošenka	k1gFnSc1	Porošenka
připadlo	připadnout	k5eAaPmAgNnS	připadnout
26	[number]	k4	26
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prezident	prezident	k1gMnSc1	prezident
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sečtení	sečtení	k1gNnSc6	sečtení
hlasů	hlas	k1gInPc2	hlas
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
stal	stát	k5eAaPmAgMnS	stát
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
kontroverzi	kontroverze	k1gFnSc4	kontroverze
<g/>
,	,	kIx,	,
když	když	k8xS	když
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
všem	všecek	k3xTgMnPc3	všecek
Rusům	Rus	k1gMnPc3	Rus
ukrajinské	ukrajinský	k2eAgNnSc4d1	ukrajinské
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c6	na
ruského	ruský	k2eAgMnSc2d1	ruský
prezidenta	prezident	k1gMnSc2	prezident
Vladimira	Vladimir	k1gInSc2	Vladimir
Putina	putin	k2eAgInSc2d1	putin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dříve	dříve	k6eAd2	dříve
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ruské	ruský	k2eAgNnSc4d1	ruské
občanství	občanství	k1gNnSc4	občanství
pro	pro	k7c4	pro
Ukrajince	Ukrajinec	k1gMnSc4	Ukrajinec
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
slib	slib	k1gInSc4	slib
věrnosti	věrnost	k1gFnSc3	věrnost
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
dožadoval	dožadovat	k5eAaImAgMnS	dožadovat
přesunutí	přesunutí	k1gNnSc4	přesunutí
data	datum	k1gNnSc2	datum
inaugurace	inaugurace	k1gFnSc2	inaugurace
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
původního	původní	k2eAgMnSc2d1	původní
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
datum	datum	k1gNnSc4	datum
posunout	posunout	k5eAaPmF	posunout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
rozpustit	rozpustit	k5eAaPmF	rozpustit
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
skutečně	skutečně	k6eAd1	skutečně
udělal	udělat	k5eAaPmAgMnS	udělat
den	den	k1gInSc4	den
po	po	k7c6	po
vstoupení	vstoupení	k1gNnSc6	vstoupení
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
premiér	premiér	k1gMnSc1	premiér
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Hrojsman	Hrojsman	k1gMnSc1	Hrojsman
podal	podat	k5eAaPmAgMnS	podat
<g/>
,	,	kIx,	,
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
Zelenského	Zelenský	k2eAgMnSc2d1	Zelenský
<g/>
,	,	kIx,	,
demisi	demise	k1gFnSc4	demise
<g/>
.	.	kIx.	.
</s>
<s>
Rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
zastoupení	zastoupení	k1gNnSc2	zastoupení
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
prosazovat	prosazovat	k5eAaImF	prosazovat
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
chce	chtít	k5eAaImIp3nS	chtít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Jelenou	Jelena	k1gFnSc7	Jelena
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Wolodymyr	Wolodymyr	k1gMnSc1	Wolodymyr
Selenskyj	Selenskyj	k1gMnSc1	Selenskyj
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Oleksandrovyč	Oleksandrovyč	k1gFnSc4	Oleksandrovyč
Zelenskyj	Zelenskyj	k1gFnSc4	Zelenskyj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
