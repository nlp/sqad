<s>
Jaká	jaký	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
kartézský	kartézský	k2eAgInSc4d1
souřadnicový	souřadnicový	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
vyjádření	vyjádření	k1gNnSc3
polohy	poloha	k1gFnSc2
těles	těleso	k1gNnPc2
vůči	vůči	k7c3
Zemi	zem	k1gFnSc3
<g/>
?	?	kIx.
</s>