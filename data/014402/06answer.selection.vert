<s>
ECEF	ECEF	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
anglického	anglický	k2eAgMnSc2d1
Earth-Centered	Earth-Centered	k1gMnSc1
<g/>
,	,	kIx,
Earth-Fixed	Earth-Fixed	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
také	také	k9
ECR	ECR	kA
(	(	kIx(
<g/>
Earth	Earth	k1gMnSc1
Centered	Centered	k1gMnSc1
Rotational	Rotational	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kartézský	kartézský	k2eAgInSc1d1
souřadnicový	souřadnicový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
vyjádření	vyjádření	k1gNnSc3
polohy	poloha	k1gFnSc2
těles	těleso	k1gNnPc2
vůči	vůči	k7c3
Zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>