<s>
ECEF	ECEF	kA
</s>
<s>
ECEF	ECEF	kA
a	a	k8xC
vyjádření	vyjádření	k1gNnSc2
polohy	poloha	k1gFnSc2
vůči	vůči	k7c3
osám	osa	k1gFnPc3
(	(	kIx(
<g/>
zeleně	zeleně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gama	gama	k1gNnPc1
a	a	k8xC
fí	fí	k0
je	být	k5eAaImIp3nS
poloha	poloha	k1gFnSc1
<g/>
,	,	kIx,
vyjádřená	vyjádřený	k2eAgFnSc1d1
pomocí	pomocí	k7c2
zeměpisné	zeměpisný	k2eAgFnSc2d1
délky	délka	k1gFnSc2
a	a	k8xC
šířky	šířka	k1gFnSc2
</s>
<s>
ECEF	ECEF	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
anglického	anglický	k2eAgMnSc2d1
Earth-Centered	Earth-Centered	k1gMnSc1
<g/>
,	,	kIx,
Earth-Fixed	Earth-Fixed	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
také	také	k9
ECR	ECR	kA
(	(	kIx(
<g/>
Earth	Earth	k1gMnSc1
Centered	Centered	k1gMnSc1
Rotational	Rotational	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kartézský	kartézský	k2eAgInSc1d1
souřadnicový	souřadnicový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
vyjádření	vyjádření	k1gNnSc3
polohy	poloha	k1gFnSc2
těles	těleso	k1gNnPc2
vůči	vůči	k7c3
Zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střed	střed	k1gInSc4
této	tento	k3xDgFnSc2
souřadnicové	souřadnicový	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
hmotnostním	hmotnostní	k2eAgNnSc6d1
těžišti	těžiště	k1gNnSc6
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
se	se	k3xPyFc4
vůči	vůči	k7c3
ní	on	k3xPp3gFnSc3
prakticky	prakticky	k6eAd1
nepohybuje	pohybovat	k5eNaImIp3nS
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
geodetických	geodetický	k2eAgFnPc6d1
a	a	k8xC
navigačních	navigační	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
popis	popis	k1gInSc4
polohy	poloha	k1gFnSc2
v	v	k7c6
prostoru	prostor	k1gInSc6
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
definovat	definovat	k5eAaBmF
konkrétní	konkrétní	k2eAgInSc4d1
referenční	referenční	k2eAgInSc4d1
elipsoid	elipsoid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgNnSc2
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
družicové	družicový	k2eAgInPc4d1
navigační	navigační	k2eAgInPc4d1
systémy	systém	k1gInPc4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
výpočtech	výpočet	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Souřadnicová	souřadnicový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
ECEF	ECEF	kA
rotuje	rotovat	k5eAaImIp3nS
spolu	spolu	k6eAd1
se	s	k7c7
Zemí	zem	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
proto	proto	k8xC
inerciální	inerciální	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
inerciální	inerciální	k2eAgFnSc1d1
geocentrická	geocentrický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
(	(	kIx(
<g/>
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
ne	ne	k9
zcela	zcela	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
soustava	soustava	k1gFnSc1
ECI	ECI	kA
(	(	kIx(
<g/>
Earth-centered	Earth-centered	k1gMnSc1
inertial	inertial	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zachovává	zachovávat	k5eAaImIp3nS
směr	směr	k1gInSc4
os	osa	k1gFnPc2
vůči	vůči	k7c3
obloze	obloha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
soustavy	soustava	k1gFnSc2
</s>
<s>
Soustava	soustava	k1gFnSc1
je	být	k5eAaImIp3nS
definována	definovat	k5eAaBmNgFnS
svým	svůj	k3xOyFgInSc7
počátkem	počátek	k1gInSc7
a	a	k8xC
směry	směr	k1gInPc7
os	osa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Počátek	počátek	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
těžišti	těžiště	k1gNnSc6
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Osa	osa	k1gFnSc1
Z	z	k7c2
míří	mířit	k5eAaImIp3nS
na	na	k7c4
sever	sever	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemusí	muset	k5eNaImIp3nS
se	se	k3xPyFc4
přesně	přesně	k6eAd1
shodovat	shodovat	k5eAaImF
s	s	k7c7
aktuálním	aktuální	k2eAgInSc7d1
směrem	směr	k1gInSc7
rotační	rotační	k2eAgFnSc2d1
osy	osa	k1gFnSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
vůči	vůči	k7c3
povrchu	povrch	k1gInSc3
pomalu	pomalu	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Osa	osa	k1gFnSc1
X	X	kA
prochází	procházet	k5eAaImIp3nS
Základním	základní	k2eAgInSc7d1
poledníkem	poledník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Osa	osa	k1gFnSc1
Y	Y	kA
je	být	k5eAaImIp3nS
kolmá	kolmý	k2eAgFnSc1d1
na	na	k7c4
obě	dva	k4xCgFnPc4
předchozí	předchozí	k2eAgFnPc4d1
a	a	k8xC
míří	mířit	k5eAaImIp3nS
na	na	k7c6
východní	východní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
ECEF	ECEF	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
