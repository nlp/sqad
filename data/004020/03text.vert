<s>
Opisthokonta	Opisthokonta	k1gFnSc1	Opisthokonta
je	být	k5eAaImIp3nS	být
superskupina	superskupina	k1gFnSc1	superskupina
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
,	,	kIx,	,
počtem	počet	k1gInSc7	počet
druhů	druh	k1gInPc2	druh
zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gFnPc7	její
sesterskými	sesterský	k2eAgFnPc7d1	sesterská
skupinami	skupina	k1gFnPc7	skupina
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
znalostí	znalost	k1gFnPc2	znalost
nepočetné	početný	k2eNgFnSc2d1	nepočetná
skupiny	skupina	k1gFnSc2	skupina
Breviatea	Breviatea	k1gFnSc1	Breviatea
a	a	k8xC	a
Apusomonada	Apusomonada	k1gFnSc1	Apusomonada
<g/>
;	;	kIx,	;
spolu	spolu	k6eAd1	spolu
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nS	tvořit
klad	klad	k1gInSc1	klad
Obazoa	Obazoum	k1gNnSc2	Obazoum
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc1	jméno
z	z	k7c2	z
počátečních	počáteční	k2eAgNnPc2d1	počáteční
písmen	písmeno	k1gNnPc2	písmeno
podřazených	podřazený	k2eAgFnPc2d1	podřazená
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
standardní	standardní	k2eAgFnSc2d1	standardní
přípony	přípona	k1gFnSc2	přípona
-zoa	oa	k6eAd1	-zoa
pro	pro	k7c4	pro
prvoky	prvok	k1gMnPc4	prvok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejpříbuznější	příbuzný	k2eAgFnSc7d3	nejpříbuznější
eukaryotní	eukaryotní	k2eAgFnSc7d1	eukaryotní
superskupinou	superskupina	k1gFnSc7	superskupina
jsou	být	k5eAaImIp3nP	být
Amoebozoa	Amoebozo	k2eAgNnPc1d1	Amoebozo
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
opistokont	opistokont	k1gInSc1	opistokont
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
tlačný	tlačný	k2eAgInSc1d1	tlačný
bičík	bičík	k1gInSc1	bičík
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
spermií	spermie	k1gFnPc2	spermie
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ale	ale	k9	ale
organismy	organismus	k1gInPc7	organismus
bičík	bičík	k1gInSc1	bičík
druhotně	druhotně	k6eAd1	druhotně
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
většina	většina	k1gFnSc1	většina
hub	houba	k1gFnPc2	houba
<g/>
)	)	kIx)	)
a	a	k8xC	a
převládající	převládající	k2eAgInSc1d1	převládající
typ	typ	k1gInSc1	typ
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
s	s	k7c7	s
plochými	plochý	k2eAgFnPc7d1	plochá
kristami	krista	k1gFnPc7	krista
<g/>
.	.	kIx.	.
</s>
<s>
Opisthokonta	Opisthokonta	k1gFnSc1	Opisthokonta
také	také	k9	také
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
biochemických	biochemický	k2eAgFnPc2d1	biochemická
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
vyrábět	vyrábět	k5eAaImF	vyrábět
kolagen	kolagen	k1gInSc4	kolagen
a	a	k8xC	a
jako	jako	k9	jako
zásobní	zásobní	k2eAgFnSc4d1	zásobní
látku	látka	k1gFnSc4	látka
používat	používat	k5eAaImF	používat
glykogen	glykogen	k1gInSc4	glykogen
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
skupiny	skupina	k1gFnPc1	skupina
opistokont	opistokonta	k1gFnPc2	opistokonta
jsou	být	k5eAaImIp3nP	být
houby	houba	k1gFnPc1	houba
a	a	k8xC	a
živočichové	živočich	k1gMnPc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gMnPc2	on
ještě	ještě	k9	ještě
Opisthokonta	Opisthokont	k1gInSc2	Opisthokont
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
několik	několik	k4yIc4	několik
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
blíže	blízce	k6eAd2	blízce
příbuzných	příbuzný	k1gMnPc2	příbuzný
živočichům	živočich	k1gMnPc3	živočich
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
živočichy	živočich	k1gMnPc7	živočich
tvoří	tvořit	k5eAaImIp3nS	tvořit
skupinu	skupina	k1gFnSc4	skupina
Holozoa	Holozo	k1gInSc2	Holozo
<g/>
;	;	kIx,	;
např.	např.	kA	např.
trubénky	trubénka	k1gFnSc2	trubénka
<g/>
,	,	kIx,	,
plísňovky	plísňovka	k1gFnSc2	plísňovka
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
rodů	rod	k1gInPc2	rod
jako	jako	k8xS	jako
Corallochytrea	Corallochytrea	k1gFnSc1	Corallochytrea
<g/>
,	,	kIx,	,
Capsaspora	Capsaspora	k1gFnSc1	Capsaspora
<g/>
,	,	kIx,	,
Ministeria	ministerium	k1gNnPc1	ministerium
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
houbám	houba	k1gFnPc3	houba
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
houbami	houba	k1gFnPc7	houba
tvoří	tvořit	k5eAaImIp3nS	tvořit
skupinu	skupina	k1gFnSc4	skupina
Holomycota	Holomycot	k1gMnSc2	Holomycot
<g/>
/	/	kIx~	/
<g/>
Holofungi	Holofungi	k1gNnSc1	Holofungi
<g/>
;	;	kIx,	;
např.	např.	kA	např.
rody	rod	k1gInPc7	rod
Nuclearia	Nuclearium	k1gNnSc2	Nuclearium
<g/>
,	,	kIx,	,
Fonticula	Fonticulum	k1gNnSc2	Fonticulum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
ustálená	ustálený	k2eAgFnSc1d1	ustálená
-	-	kIx~	-
např.	např.	kA	např.
několik	několik	k4yIc1	několik
skupin	skupina	k1gFnPc2	skupina
tvořících	tvořící	k2eAgInPc2d1	tvořící
taxon	taxon	k1gInSc4	taxon
(	(	kIx(	(
<g/>
možná	možná	k9	možná
klad	klad	k1gInSc1	klad
<g/>
)	)	kIx)	)
Opisthosporidia	Opisthosporidium	k1gNnSc2	Opisthosporidium
(	(	kIx(	(
<g/>
ARM	ARM	kA	ARM
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Aphelidea	Aphelideum	k1gNnSc2	Aphelideum
<g/>
,	,	kIx,	,
Rozella	Rozello	k1gNnSc2	Rozello
<g/>
,	,	kIx,	,
mikrosporidie	mikrosporidie	k1gFnPc4	mikrosporidie
<g/>
)	)	kIx)	)
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
skupinu	skupina	k1gFnSc4	skupina
patřící	patřící	k2eAgNnSc1d1	patřící
do	do	k7c2	do
Holomycota	Holomycot	k1gMnSc2	Holomycot
<g/>
.	.	kIx.	.
</s>
