<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
(	(	kIx(	(
<g/>
v	v	k7c6	v
hantecu	hantecum	k1gNnSc6	hantecum
Prýgl	Prýgla	k1gFnPc2	Prýgla
nebo	nebo	k8xC	nebo
Prygl	Prygla	k1gFnPc2	Prygla
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
též	též	k9	též
Kníničská	kníničský	k2eAgFnSc1d1	Kníničská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgNnSc1d1	vodní
dílo	dílo	k1gNnSc1	dílo
na	na	k7c6	na
Svratce	Svratka	k1gFnSc6	Svratka
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vystavěním	vystavění	k1gNnSc7	vystavění
hráze	hráz	k1gFnSc2	hráz
na	na	k7c4	na
56	[number]	k4	56
<g/>
.	.	kIx.	.
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
Svratky	Svratka	k1gFnSc2	Svratka
a	a	k8xC	a
zatopením	zatopení	k1gNnSc7	zatopení
údolí	údolí	k1gNnSc2	údolí
s	s	k7c7	s
obcí	obec	k1gFnSc7	obec
Kníničky	Knínička	k1gFnSc2	Knínička
<g/>
.	.	kIx.	.
</s>
<s>
Přehrada	přehrada	k1gFnSc1	přehrada
dříve	dříve	k6eAd2	dříve
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k8xC	jako
zásobárna	zásobárna	k1gFnSc1	zásobárna
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
vody	voda	k1gFnPc1	voda
z	z	k7c2	z
Vírské	vírský	k2eAgFnSc2d1	Vírská
přehrady	přehrada	k1gFnSc2	přehrada
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
vrtů	vrt	k1gInPc2	vrt
v	v	k7c6	v
Březové	březový	k2eAgFnSc6d1	Březová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
plochy	plocha	k1gFnSc2	plocha
přehrady	přehrada	k1gFnSc2	přehrada
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
přírodního	přírodní	k2eAgInSc2d1	přírodní
parku	park	k1gInSc2	park
Podkomorské	podkomorský	k2eAgInPc1d1	podkomorský
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
přehrady	přehrada	k1gFnSc2	přehrada
na	na	k7c6	na
Svratce	Svratka	k1gFnSc6	Svratka
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
realizaci	realizace	k1gFnSc3	realizace
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
investorem	investor	k1gMnSc7	investor
bylo	být	k5eAaImAgNnS	být
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
vedle	vedle	k7c2	vedle
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
25	[number]	k4	25
%	%	kIx~	%
podílela	podílet	k5eAaImAgFnS	podílet
také	také	k9	také
Země	země	k1gFnSc1	země
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1	Moravskoslezská
.	.	kIx.	.
</s>
<s>
Vzdutí	vzdutí	k1gNnSc1	vzdutí
přehrady	přehrada	k1gFnSc2	přehrada
začíná	začínat	k5eAaImIp3nS	začínat
pod	pod	k7c7	pod
splavem	splav	k1gInSc7	splav
u	u	k7c2	u
Tejkalova	Tejkalův	k2eAgInSc2d1	Tejkalův
mlýna	mlýn	k1gInSc2	mlýn
ve	v	k7c6	v
Veverské	Veverský	k2eAgFnSc6d1	Veverská
Bítýšce	Bítýška	k1gFnSc6	Bítýška
a	a	k8xC	a
k	k	k7c3	k
hrázi	hráz	k1gFnSc3	hráz
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
brněnských	brněnský	k2eAgFnPc2d1	brněnská
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Brna-Bystrce	Brna-Bystrec	k1gInSc2	Brna-Bystrec
a	a	k8xC	a
Brno-Kníničky	Brno-Knínička	k1gFnSc2	Brno-Knínička
měří	měřit	k5eAaImIp3nS	měřit
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zatopená	zatopený	k2eAgFnSc1d1	zatopená
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
259	[number]	k4	259
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Stálé	stálý	k2eAgNnSc1d1	stálé
nadržení	nadržení	k1gNnSc1	nadržení
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
7,6	[number]	k4	7,6
milionů	milion	k4xCgInPc2	milion
m3	m3	k4	m3
<g/>
,	,	kIx,	,
zásobní	zásobní	k2eAgInSc1d1	zásobní
prostor	prostor	k1gInSc1	prostor
pak	pak	k6eAd1	pak
10,8	[number]	k4	10,8
milionů	milion	k4xCgInPc2	milion
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Betonová	betonový	k2eAgFnSc1d1	betonová
gravitační	gravitační	k2eAgFnSc1d1	gravitační
hráz	hráz	k1gFnSc1	hráz
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
šířku	šířka	k1gFnSc4	šířka
7,14	[number]	k4	7,14
m	m	kA	m
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
120	[number]	k4	120
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
233,72	[number]	k4	233,72
m	m	kA	m
ční	čnět	k5eAaImIp3nS	čnět
23,5	[number]	k4	23,5
m	m	kA	m
nade	nad	k7c7	nad
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hrázi	hráz	k1gFnSc6	hráz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Kníničky	Knínička	k1gFnSc2	Knínička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ustupující	ustupující	k2eAgFnSc1d1	ustupující
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
vozovku	vozovka	k1gFnSc4	vozovka
zaminovala	zaminovat	k5eAaPmAgFnS	zaminovat
a	a	k8xC	a
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
hráz	hráz	k1gFnSc4	hráz
sud	suda	k1gFnPc2	suda
s	s	k7c7	s
trinitrotoluenem	trinitrotoluen	k1gInSc7	trinitrotoluen
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
výbušniny	výbušnina	k1gFnPc1	výbušnina
umístěny	umístit	k5eAaPmNgFnP	umístit
do	do	k7c2	do
potrubí	potrubí	k1gNnSc2	potrubí
hráze	hráz	k1gFnSc2	hráz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
zaměstnanci	zaměstnanec	k1gMnSc3	zaměstnanec
z	z	k7c2	z
rozkazu	rozkaz	k1gInSc2	rozkaz
ochrany	ochrana	k1gFnSc2	ochrana
před	před	k7c7	před
sabotáží	sabotáž	k1gFnSc7	sabotáž
zabetonováno	zabetonovat	k5eAaPmNgNnS	zabetonovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
domku	domek	k1gInSc2	domek
hrázného	hrázný	k1gMnSc2	hrázný
Šikuly	Šikula	k1gMnSc2	Šikula
kulomety	kulomet	k1gInPc1	kulomet
<g/>
.	.	kIx.	.
</s>
<s>
Šikula	Šikula	k1gMnSc1	Šikula
s	s	k7c7	s
několika	několik	k4yIc7	několik
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
ukrytými	ukrytý	k2eAgInPc7d1	ukrytý
ve	v	k7c6	v
strojovně	strojovna	k1gFnSc6	strojovna
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
konvoj	konvoj	k1gInSc4	konvoj
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
varovali	varovat	k5eAaImAgMnP	varovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Šikula	Šikula	k1gMnSc1	Šikula
byl	být	k5eAaImAgMnS	být
postřelen	postřelen	k2eAgMnSc1d1	postřelen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hrázi	hráz	k1gFnSc6	hráz
umístěná	umístěný	k2eAgFnSc1d1	umístěná
jeho	jeho	k3xOp3gFnSc1	jeho
plaketa	plaketa	k1gFnSc1	plaketa
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
je	být	k5eAaImIp3nS	být
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
rekreace	rekreace	k1gFnSc2	rekreace
místních	místní	k2eAgInPc2d1	místní
i	i	k8xC	i
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
je	být	k5eAaImIp3nS	být
lemována	lemovat	k5eAaImNgFnS	lemovat
rozlehlými	rozlehlý	k2eAgInPc7d1	rozlehlý
lesy	les	k1gInPc7	les
(	(	kIx(	(
<g/>
Obora	obora	k1gFnSc1	obora
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
Podkomorské	podkomorský	k2eAgInPc4d1	podkomorský
lesy	les	k1gInPc4	les
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
<g/>
)	)	kIx)	)
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
tak	tak	k9	tak
příležitost	příležitost	k1gFnSc1	příležitost
nejenom	nejenom	k6eAd1	nejenom
ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
a	a	k8xC	a
vodním	vodní	k2eAgInPc3d1	vodní
sportům	sport	k1gInPc3	sport
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
turistice	turistika	k1gFnSc3	turistika
a	a	k8xC	a
cykloturistice	cykloturistika	k1gFnSc3	cykloturistika
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
výletníků	výletník	k1gMnPc2	výletník
je	být	k5eAaImIp3nS	být
také	také	k9	také
hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgInSc1d1	veveří
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
na	na	k7c6	na
skále	skála	k1gFnSc6	skála
nedaleko	nedaleko	k7c2	nedaleko
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hradu	hrad	k1gInSc2	hrad
byl	být	k5eAaImAgInS	být
přes	přes	k7c4	přes
přehradu	přehrada	k1gFnSc4	přehrada
vybudován	vybudován	k2eAgInSc4d1	vybudován
most	most	k1gInSc4	most
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
a	a	k8xC	a
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Přehrada	přehrada	k1gFnSc1	přehrada
je	být	k5eAaImIp3nS	být
využitelná	využitelný	k2eAgFnSc1d1	využitelná
celoročně	celoročně	k6eAd1	celoročně
a	a	k8xC	a
koupání	koupání	k1gNnSc1	koupání
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
cípu	cíp	k1gInSc6	cíp
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
bystrcké	bystrcký	k2eAgFnSc2d1	bystrcká
části	část	k1gFnSc2	část
přehrady	přehrada	k1gFnSc2	přehrada
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
koná	konat	k5eAaImIp3nS	konat
významná	významný	k2eAgFnSc1d1	významná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
ohňostrojů	ohňostroj	k1gInPc2	ohňostroj
Ignis	Ignis	k1gFnSc1	Ignis
Brunensis	Brunensis	k1gFnSc1	Brunensis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledků	důsledek	k1gInPc2	důsledek
znečištění	znečištění	k1gNnSc4	znečištění
vody	voda	k1gFnSc2	voda
komunálními	komunální	k2eAgInPc7d1	komunální
splašky	splašky	k1gInPc7	splašky
přehradu	přehrada	k1gFnSc4	přehrada
sužuje	sužovat	k5eAaImIp3nS	sužovat
nadměrné	nadměrný	k2eAgNnSc1d1	nadměrné
množství	množství	k1gNnSc1	množství
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
Moravy	Morava	k1gFnSc2	Morava
dostalo	dostat	k5eAaPmAgNnS	dostat
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
od	od	k7c2	od
krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
výjimku	výjimka	k1gFnSc4	výjimka
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
proti	proti	k7c3	proti
sinicím	sinice	k1gFnPc3	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
Hydrobiologického	hydrobiologický	k2eAgInSc2d1	hydrobiologický
ústavu	ústav	k1gInSc2	ústav
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
taková	takový	k3xDgNnPc1	takový
opatření	opatření	k1gNnPc1	opatření
efektivní	efektivní	k2eAgFnSc2d1	efektivní
a	a	k8xC	a
nezajistí	zajistit	k5eNaPmIp3nS	zajistit
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
řešení	řešení	k1gNnSc1	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
Děti	dítě	k1gFnPc1	dítě
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
domnívalo	domnívat	k5eAaImAgNnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
jen	jen	k9	jen
o	o	k7c4	o
předvolební	předvolební	k2eAgInSc4d1	předvolební
trik	trik	k1gInSc4	trik
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sinice	sinice	k1gFnPc1	sinice
měly	mít	k5eAaImAgFnP	mít
zmizet	zmizet	k5eAaPmF	zmizet
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jen	jen	k9	jen
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Vyčištění	vyčištění	k1gNnSc1	vyčištění
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
starosta	starosta	k1gMnSc1	starosta
MČ	MČ	kA	MČ
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
dne	den	k1gInSc2	den
25.10	[number]	k4	25.10
<g/>
.2001	.2001	k4	.2001
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Dallas	Dallas	k1gInSc1	Dallas
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
zástupců	zástupce	k1gMnPc2	zástupce
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
odborníků	odborník	k1gMnPc2	odborník
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
nebyla	být	k5eNaImAgFnS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
neustálé	neustálý	k2eAgFnSc2d1	neustálá
desetileté	desetiletý	k2eAgFnSc2d1	desetiletá
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
objevil	objevit	k5eAaPmAgInS	objevit
výsledek	výsledek	k1gInSc1	výsledek
<g/>
..	..	k?	..
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
přehrada	přehrada	k1gFnSc1	přehrada
čistá	čistý	k2eAgFnSc1d1	čistá
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
léto	léto	k1gNnSc1	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přehradě	přehrada	k1gFnSc6	přehrada
provozuje	provozovat	k5eAaImIp3nS	provozovat
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jezdí	jezdit	k5eAaImIp3nS	jezdit
z	z	k7c2	z
přístaviště	přístaviště	k1gNnSc2	přístaviště
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jsou	být	k5eAaImIp3nP	být
nové	nový	k2eAgFnPc1d1	nová
lodě	loď	k1gFnPc1	loď
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
Utrecht	Utrecht	k1gInSc1	Utrecht
a	a	k8xC	a
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
po	po	k7c6	po
družebních	družební	k2eAgNnPc6d1	družební
městech	město	k1gNnPc6	město
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
a	a	k8xC	a
starší	starý	k2eAgFnPc1d2	starší
lodě	loď	k1gFnPc1	loď
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Dallas	Dallas	k1gInSc1	Dallas
a	a	k8xC	a
Veveří	veveří	k2eAgInSc1d1	veveří
<g/>
.	.	kIx.	.
</s>
<s>
Přehrada	přehrada	k1gFnSc1	přehrada
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
sportoviště	sportoviště	k1gNnSc1	sportoviště
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kanoistiky	kanoistika	k1gFnSc2	kanoistika
<g/>
,	,	kIx,	,
vodního	vodní	k2eAgNnSc2d1	vodní
lyžování	lyžování	k1gNnSc2	lyžování
a	a	k8xC	a
jachtingu	jachting	k1gInSc2	jachting
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
veslování	veslování	k1gNnSc2	veslování
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
například	například	k6eAd1	například
veslařské	veslařský	k2eAgInPc1d1	veslařský
kluby	klub	k1gInPc1	klub
ČVK	ČVK	kA	ČVK
Brno	Brno	k1gNnSc1	Brno
nebo	nebo	k8xC	nebo
jachtařský	jachtařský	k2eAgInSc1d1	jachtařský
a	a	k8xC	a
veslařský	veslařský	k2eAgInSc1d1	veslařský
klub	klub	k1gInSc1	klub
TJ	tj	kA	tj
Lodní	lodní	k2eAgInPc1d1	lodní
sporty	sport	k1gInPc1	sport
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgFnSc1d3	nejznámější
veslařka	veslařka	k1gFnSc1	veslařka
Miroslava	Miroslava	k1gFnSc1	Miroslava
Knapková	Knapková	k1gFnSc1	Knapková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rozepře	rozepře	k1gFnSc1	rozepře
mezi	mezi	k7c7	mezi
příznivci	příznivec	k1gMnPc7	příznivec
a	a	k8xC	a
odpůrci	odpůrce	k1gMnPc7	odpůrce
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
