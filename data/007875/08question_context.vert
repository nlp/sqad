<s>
Portugalsko	Portugalsko	k1gNnSc1
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
<g/>
:	:	kIx,
Portugal	portugal	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
Portugalská	portugalský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
portugalsky	portugalsky	k6eAd1
<g/>
:	:	kIx,
República	Repúblic	k2eAgFnSc1d1
Portuguesa	Portuguesa	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
evropský	evropský	k2eAgInSc4d1
stát	stát	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
na	na	k7c6
samém	samý	k3xTgInSc6
jihozápadním	jihozápadní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
světadílu	světadíl	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>