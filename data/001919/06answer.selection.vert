<s>
Celková	celkový	k2eAgFnSc1d1	celková
velikost	velikost	k1gFnSc1	velikost
pre-rRNA	preRNA	k?	pre-rRNA
(	(	kIx(	(
<g/>
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
sedimentační	sedimentační	k2eAgFnSc2d1	sedimentační
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
38S	[number]	k4	38S
u	u	k7c2	u
octomilky	octomilka	k1gFnSc2	octomilka
<g/>
,	,	kIx,	,
40S	[number]	k4	40S
u	u	k7c2	u
drápatky	drápatka	k1gFnSc2	drápatka
a	a	k8xC	a
45S	[number]	k4	45S
u	u	k7c2	u
lidských	lidský	k2eAgInPc2d1	lidský
HeLa	Hela	k1gFnSc1	Hela
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
