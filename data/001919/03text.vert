<s>
rRNA	rRNA	k?	rRNA
neboli	neboli	k8xC	neboli
ribozomální	ribozomální	k2eAgFnSc2d1	ribozomální
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
specifickými	specifický	k2eAgFnPc7d1	specifická
bílkovinami	bílkovina	k1gFnPc7	bílkovina
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
rRNA	rRNA	k?	rRNA
nachází	nacházet	k5eAaImIp3nS	nacházet
uvnitř	uvnitř	k7c2	uvnitř
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
rRNA	rRNA	k?	rRNA
je	být	k5eAaImIp3nS	být
ribozym	ribozym	k1gInSc4	ribozym
(	(	kIx(	(
<g/>
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
peptidyltransferáza	peptidyltransferáza	k1gFnSc1	peptidyltransferáza
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vytváření	vytváření	k1gNnSc6	vytváření
peptidových	peptidový	k2eAgFnPc2d1	peptidová
vazeb	vazba	k1gFnPc2	vazba
při	při	k7c6	při
syntéze	syntéza	k1gFnSc6	syntéza
proteinů	protein	k1gInPc2	protein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejhojnějším	hojný	k2eAgInSc7d3	nejhojnější
typem	typ	k1gInSc7	typ
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc3	hmotnost
všech	všecek	k3xTgMnPc2	všecek
RNA	RNA	kA	RNA
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prokaryotických	prokaryotický	k2eAgInPc2d1	prokaryotický
organismů	organismus	k1gInPc2	organismus
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
různě	různě	k6eAd1	různě
velké	velký	k2eAgFnPc4d1	velká
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
eukaryotických	eukaryotický	k2eAgFnPc2d1	eukaryotická
buněk	buňka	k1gFnPc2	buňka
až	až	k8xS	až
4	[number]	k4	4
druhy	druh	k1gInPc7	druh
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
5,8	[number]	k4	5,8
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednovláknová	jednovláknový	k2eAgFnSc1d1	jednovláknová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určité	určitý	k2eAgFnPc1d1	určitá
části	část	k1gFnPc1	část
molekul	molekula	k1gFnPc2	molekula
mají	mít	k5eAaImIp3nP	mít
strukturu	struktura	k1gFnSc4	struktura
dvojité	dvojitý	k2eAgFnSc2d1	dvojitá
šroubovice	šroubovice	k1gFnSc2	šroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
jadérku	jadérko	k1gNnSc6	jadérko
<g/>
.	.	kIx.	.
sekvenace	sekvenace	k1gFnSc1	sekvenace
rRNA	rRNA	k?	rRNA
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vědcům	vědec	k1gMnPc3	vědec
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
příbuzenských	příbuzenský	k2eAgInPc2d1	příbuzenský
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
biologickými	biologický	k2eAgInPc7d1	biologický
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ji	on	k3xPp3gFnSc4	on
kódují	kódovat	k5eAaBmIp3nP	kódovat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
variabilitu	variabilita	k1gFnSc4	variabilita
<g/>
.	.	kIx.	.
</s>
<s>
Geny	gen	k1gInPc1	gen
pro	pro	k7c4	pro
rRNA	rRNA	k?	rRNA
jsou	být	k5eAaImIp3nP	být
kódovány	kódovat	k5eAaBmNgInP	kódovat
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
NA	na	k7c4	na
<g/>
.	.	kIx.	.
</s>
<s>
Preribozomální	Preribozomální	k2eAgFnSc1d1	Preribozomální
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
pre-rRNA	preRNA	k?	pre-rRNA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
molekula	molekula	k1gFnSc1	molekula
RNA	RNA	kA	RNA
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xS	jako
primární	primární	k2eAgMnSc1d1	primární
transkript	transkript	k1gMnSc1	transkript
(	(	kIx(	(
<g/>
prekurzor	prekurzor	k1gMnSc1	prekurzor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
sestřihem	sestřih	k1gInSc7	sestřih
vznikají	vznikat	k5eAaImIp3nP	vznikat
tři	tři	k4xCgFnPc4	tři
důležité	důležitý	k2eAgFnPc4d1	důležitá
eukaryotické	eukaryotický	k2eAgFnPc4d1	eukaryotická
rRNA	rRNA	k?	rRNA
molekuly	molekula	k1gFnPc4	molekula
<g/>
,	,	kIx,	,
5.8	[number]	k4	5.8
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
18S	[number]	k4	18S
a	a	k8xC	a
28	[number]	k4	28
<g/>
S.	S.	kA	S.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
velikost	velikost	k1gFnSc1	velikost
pre-rRNA	preRNA	k?	pre-rRNA
(	(	kIx(	(
<g/>
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
sedimentační	sedimentační	k2eAgFnSc2d1	sedimentační
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
38S	[number]	k4	38S
u	u	k7c2	u
octomilky	octomilka	k1gFnSc2	octomilka
<g/>
,	,	kIx,	,
40S	[number]	k4	40S
u	u	k7c2	u
drápatky	drápatka	k1gFnSc2	drápatka
a	a	k8xC	a
45S	[number]	k4	45S
u	u	k7c2	u
lidských	lidský	k2eAgInPc2d1	lidský
HeLa	Hela	k1gFnSc1	Hela
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Transkripce	transkripce	k1gFnSc1	transkripce
genů	gen	k1gInPc2	gen
pro	pro	k7c4	pro
pre-rRNA	preRNA	k?	pre-rRNA
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
jadérku	jadérko	k1gNnSc6	jadérko
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
maturaci	maturace	k1gFnSc3	maturace
na	na	k7c4	na
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
ribozomální	ribozomální	k2eAgFnSc4d1	ribozomální
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
také	také	k9	také
mají	mít	k5eAaImIp3nP	mít
pre-rRNA	preRNA	k?	pre-rRNA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
