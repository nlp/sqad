<s>
Ropovod	ropovod	k1gInSc1
Mosul-Haifa	Mosul-Haif	k1gMnSc2
</s>
<s>
Trasa	trasa	k1gFnSc1
ropovodu	ropovod	k1gInSc2
Mosul-Haifa	Mosul-Haif	k1gMnSc2
</s>
<s>
Ropovod	ropovod	k1gInSc1
Mosul-Haifa	Mosul-Haif	k1gMnSc4
byl	být	k5eAaImAgInS
ropovod	ropovod	k1gInSc1
pro	pro	k7c4
transport	transport	k1gInSc4
ropy	ropa	k1gFnSc2
z	z	k7c2
ropných	ropný	k2eAgFnPc2d1
polí	pole	k1gFnPc2
v	v	k7c6
severoiráckém	severoirácký	k2eAgNnSc6d1
Kirkúku	Kirkúko	k1gNnSc6
přes	přes	k7c4
Jordánsko	Jordánsko	k1gNnSc4
do	do	k7c2
severoizraelské	severoizraelský	k2eAgFnSc2d1
Haify	Haifa	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
provozu	provoz	k1gInSc6
v	v	k7c6
letech	léto	k1gNnPc6
1935	#num#	k4
až	až	k9
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ropovod	ropovod	k1gInSc1
byl	být	k5eAaImAgInS
dlouhý	dlouhý	k2eAgInSc4d1
942	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
průměr	průměr	k1gInSc1
byl	být	k5eAaImAgInS
8	#num#	k4
palců	palec	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
trvalo	trvat	k5eAaImAgNnS
deset	deset	k4xCc1
dnů	den	k1gInPc2
<g/>
,	,	kIx,
než	než	k8xS
byla	být	k5eAaImAgFnS
ropa	ropa	k1gFnSc1
přepravena	přepravit	k5eAaPmNgFnS
z	z	k7c2
jednoho	jeden	k4xCgInSc2
konce	konec	k1gInSc2
do	do	k7c2
druhého	druhý	k4xOgNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Haifě	Haifa	k1gFnSc6
byla	být	k5eAaImAgFnS
ropa	ropa	k1gFnSc1
následně	následně	k6eAd1
zpracovaná	zpracovaný	k2eAgFnSc1d1
v	v	k7c6
místních	místní	k2eAgFnPc6d1
rafinériích	rafinérie	k1gFnPc6
<g/>
,	,	kIx,
uložena	uložen	k2eAgFnSc1d1
do	do	k7c2
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
naloženy	naložit	k5eAaPmNgInP
do	do	k7c2
tankerů	tanker	k1gInPc2
a	a	k8xC
přepraveny	přepravit	k5eAaPmNgFnP
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ropovod	ropovod	k1gInSc1
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
území	území	k1gNnSc2
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
pod	pod	k7c7
britskou	britský	k2eAgFnSc7d1
správou	správa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
paralelně	paralelně	k6eAd1
budován	budovat	k5eAaImNgInS
ropovod	ropovod	k1gInSc1
vedoucí	vedoucí	k1gFnSc2
z	z	k7c2
Kirkúku	Kirkúk	k1gInSc2
do	do	k7c2
Tripolisu	Tripolis	k1gInSc2
v	v	k7c6
Libanonu	Libanon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xC,k8xS
ropovod	ropovod	k1gInSc1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
haifské	haifský	k2eAgFnPc1d1
rafinerie	rafinerie	k1gFnPc1
byly	být	k5eAaImAgFnP
vybudovány	vybudován	k2eAgMnPc4d1
Brity	Brit	k1gMnPc4
v	v	k7c6
přípravě	příprava	k1gFnSc6
na	na	k7c4
očekávanou	očekávaný	k2eAgFnSc4d1
válku	válka	k1gFnSc4
a	a	k8xC
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
nakonec	nakonec	k6eAd1
skutečně	skutečně	k6eAd1
poskytly	poskytnout	k5eAaPmAgInP
britské	britský	k2eAgInPc1d1
a	a	k8xC
americké	americký	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
ve	v	k7c6
Středozemí	středozemí	k1gNnSc6
potřebné	potřebný	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Ropovod	ropovod	k1gInSc1
byl	být	k5eAaImAgInS
častým	častý	k2eAgInSc7d1
cílem	cíl	k1gInSc7
útoků	útok	k1gInPc2
arabských	arabský	k2eAgInPc2d1
gangů	gang	k1gInPc2
během	během	k7c2
arabského	arabský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
ochranu	ochrana	k1gFnSc4
měly	mít	k5eAaImAgFnP
na	na	k7c4
starost	starost	k1gFnSc4
společné	společný	k2eAgFnSc2d1
britsko-židovské	britsko-židovský	k2eAgFnSc2d1
zvláštní	zvláštní	k2eAgFnSc2d1
noční	noční	k2eAgFnSc2d1
čety	četa	k1gFnSc2
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
kapitánem	kapitán	k1gMnSc7
Orde	Orde	k1gNnSc2
Wingatem	Wingat	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
ropovod	ropovod	k1gInSc1
cílem	cíl	k1gInSc7
útoků	útok	k1gInPc2
Irgunu	Irgun	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
byl	být	k5eAaImAgInS
provoz	provoz	k1gInSc1
ropovodu	ropovod	k1gInSc2
ukončen	ukončit	k5eAaPmNgInS
po	po	k7c4
vypuknutí	vypuknutí	k1gNnSc4
izraelské	izraelský	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Irák	Irák	k1gInSc1
přerušil	přerušit	k5eAaPmAgInS
dodávky	dodávka	k1gFnPc4
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mosul-Haifa	Mosul-Haif	k1gMnSc2
oil	oil	k?
pipeline	pipelin	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
COHEN	COHEN	kA
<g/>
,	,	kIx,
Amiram	Amiram	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
checking	checking	k1gInSc4
possibility	possibilita	k1gFnSc2
of	of	k?
pumping	pumping	k1gInSc1
oil	oil	k?
from	from	k1gInSc1
northern	northern	k1gMnSc1
Iraq	Iraq	k1gMnSc1
to	ten	k3xDgNnSc1
Haifa	Haifa	k1gFnSc1
<g/>
,	,	kIx,
via	via	k7c4
Jordan	Jordan	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haaretz	Haaretza	k1gFnPc2
<g/>
,	,	kIx,
2003-08-25	2003-08-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HERZOG	HERZOG	kA
<g/>
,	,	kIx,
Chaim	Chaim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabsko-izraelské	arabsko-izraelský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
617	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
954	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
