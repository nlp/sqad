<s>
Ropovod	ropovod	k1gInSc1
Mosul-Haifa	Mosul-Haif	k1gMnSc4
byl	být	k5eAaImAgInS
ropovod	ropovod	k1gInSc1
pro	pro	k7c4
transport	transport	k1gInSc4
ropy	ropa	k1gFnSc2
z	z	k7c2
ropných	ropný	k2eAgFnPc2d1
polí	pole	k1gFnPc2
v	v	k7c6
severoiráckém	severoirácký	k2eAgNnSc6d1
Kirkúku	Kirkúko	k1gNnSc6
přes	přes	k7c4
Jordánsko	Jordánsko	k1gNnSc4
do	do	k7c2
severoizraelské	severoizraelský	k2eAgFnSc2d1
Haify	Haifa	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
provozu	provoz	k1gInSc6
v	v	k7c6
letech	léto	k1gNnPc6
1935	#num#	k4
až	až	k9
1948	#num#	k4
<g/>
.	.	kIx.
</s>

