<s>
BBC	BBC	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
britské	britský	k2eAgFnSc6d1
rozhlasové	rozhlasový	k2eAgFnSc6d1
a	a	k8xC
televizní	televizní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránkách	stránka	k1gFnPc6
BBC	BBC	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
BBC	BBC	kA
World	World	k1gInSc4
Service	Service	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Zahájení	zahájení	k1gNnSc1
vysílání	vysílání	k1gNnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1922	#num#	k4
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
britská	britský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1
instituce	instituce	k1gFnSc1
<g/>
)	)	kIx)
Země	země	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Oblast	oblast	k1gFnSc1
vysílání	vysílání	k1gNnSc4
</s>
<s>
celosvětové	celosvětový	k2eAgInPc1d1
Web	web	k1gInSc1
</s>
<s>
http://bbc.co.uk	http://bbc.co.uk	k6eAd1
</s>
<s>
British	British	k1gInSc1
Broadcasting	Broadcasting	k2eAgInSc1d1
Corporation	Corporation	k1gInSc1
(	(	kIx(
<g/>
BBC	BBC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rozhlasová	rozhlasový	k2eAgFnSc1d1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
plnící	plnící	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
veřejnoprávního	veřejnoprávní	k2eAgNnSc2d1
vysílání	vysílání	k1gNnSc2
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťuje	zajišťovat	k5eAaImIp3nS
vysílání	vysílání	k1gNnSc2
několika	několik	k4yIc2
britských	britský	k2eAgFnPc2d1
rozhlasových	rozhlasový	k2eAgFnPc2d1
a	a	k8xC
televizních	televizní	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
i	i	k8xC
vysílání	vysílání	k1gNnSc2
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
pod	pod	k7c7
jménem	jméno	k1gNnSc7
British	British	k1gInSc1
Broadcasting	Broadcasting	k1gInSc1
Company	Compana	k1gFnSc2
roku	rok	k1gInSc2
1922	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1926	#num#	k4
nese	nést	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
British	British	k1gInSc1
Broadcasting	Broadcasting	k1gInSc1
Corporation	Corporation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
BBC	BBC	kA
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
a	a	k8xC
největší	veliký	k2eAgFnSc1d3
zpravodajská	zpravodajský	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědecké	vědecký	k2eAgInPc1d1
kořeny	kořen	k1gInPc1
vysílání	vysílání	k1gNnSc2
sahají	sahat	k5eAaImIp3nP
až	až	k9
na	na	k7c4
počátek	počátek	k1gInSc4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
trvalo	trvat	k5eAaImAgNnS
20	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
(	(	kIx(
<g/>
i	i	k9
díky	díky	k7c3
první	první	k4xOgFnSc3
světové	světový	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
)	)	kIx)
technologie	technologie	k1gFnSc1
rozvinula	rozvinout	k5eAaPmAgFnS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
použít	použít	k5eAaPmF
k	k	k7c3
masovému	masový	k2eAgNnSc3d1
simultánnímu	simultánní	k2eAgNnSc3d1
vysílání	vysílání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvními	první	k4xOgInPc7
vlastníky	vlastník	k1gMnPc7
BBC	BBC	kA
byly	být	k5eAaImAgFnP
továrny	továrna	k1gFnSc2
vyrábějící	vyrábějící	k2eAgNnPc1d1
rádia	rádio	k1gNnPc1
<g/>
,	,	kIx,
již	již	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
profitovali	profitovat	k5eAaBmAgMnP
z	z	k7c2
reklam	reklama	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
John	John	k1gMnSc1
Reith	Reith	k1gMnSc1
stal	stát	k5eAaPmAgMnS
vedoucím	vedoucí	k1gMnSc7
projektu	projekt	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
zajistit	zajistit	k5eAaPmF
veřejnou	veřejný	k2eAgFnSc4d1
vysílací	vysílací	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
zisk	zisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1
na	na	k7c6
vládě	vláda	k1gFnSc6
i	i	k9
na	na	k7c6
obchodnících	obchodník	k1gMnPc6
<g/>
,	,	kIx,
nestrannost	nestrannost	k1gFnSc1
<g/>
,	,	kIx,
ohled	ohled	k1gInSc1
na	na	k7c4
menšiny	menšina	k1gFnPc4
a	a	k8xC
respekt	respekt	k1gInSc4
k	k	k7c3
rozhlasovému	rozhlasový	k2eAgNnSc3d1
vysílání	vysílání	k1gNnSc3
jako	jako	k8xC,k8xS
důležité	důležitý	k2eAgFnSc3d1
složce	složka	k1gFnSc3
zpravodajství	zpravodajství	k1gNnSc2
i	i	k8xC
kultury	kultura	k1gFnSc2
–	–	k?
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
znaky	znak	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
si	se	k3xPyFc3
postupně	postupně	k6eAd1
BBC	BBC	kA
vydobyla	vydobýt	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Začátky	začátek	k1gInPc1
</s>
<s>
Stará	starý	k2eAgFnSc1d1
vysílací	vysílací	k2eAgFnSc1d1
budova	budova	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
(	(	kIx(
<g/>
po	po	k7c6
stávce	stávka	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
BBC	BBC	kA
stala	stát	k5eAaPmAgFnS
společností	společnost	k1gFnSc7
<g/>
,	,	kIx,
s	s	k7c7
kompetencemi	kompetence	k1gFnPc7
„	„	k?
<g/>
vychovávat	vychovávat	k5eAaImF
<g/>
,	,	kIx,
informovat	informovat	k5eAaBmF
a	a	k8xC
bavit	bavit	k5eAaImF
<g/>
“	“	k?
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
Reithovou	Reithový	k2eAgFnSc7d1
přísnou	přísný	k2eAgFnSc7d1
taktovkou	taktovka	k1gFnSc7
si	se	k3xPyFc3
BBC	BBC	kA
získala	získat	k5eAaPmAgFnS
respekt	respekt	k1gInSc4
a	a	k8xC
věhlas	věhlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšné	úspěšný	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
například	například	k6eAd1
„	„	k?
<g/>
Promenade	Promenad	k1gInSc5
Concerts	Concerts	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
Proms	Proms	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
magazín	magazín	k1gInSc1
„	„	k?
<g/>
Radio	radio	k1gNnSc1
Times	Times	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naučné	naučný	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
<g/>
,	,	kIx,
regionální	regionální	k2eAgInSc4d1
servis	servis	k1gInSc4
<g/>
,	,	kIx,
plánovaný	plánovaný	k2eAgInSc4d1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
naplno	naplno	k6eAd1
rozběhl	rozběhnout	k5eAaPmAgInS
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
se	se	k3xPyFc4
experimentovalo	experimentovat	k5eAaImAgNnS
s	s	k7c7
televizním	televizní	k2eAgNnSc7d1
vysíláním	vysílání	k1gNnSc7
<g/>
,	,	kIx,
za	za	k7c4
použití	použití	k1gNnSc4
systému	systém	k1gInSc2
vynalezeného	vynalezený	k2eAgInSc2d1
skotským	skotský	k2eAgNnSc7d1
inženýrem	inženýr	k1gMnSc7
John	John	k1gMnSc1
L.	L.	kA
Baird	Baird	k1gMnSc1
(	(	kIx(
<g/>
i	i	k9
také	také	k9
J.	J.	kA
Reith	Reith	k1gInSc1
byl	být	k5eAaImAgMnS
Skot	Skot	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1936	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
se	se	k3xPyFc4
vysílání	vysílání	k1gNnSc2
rozběhlo	rozběhnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
používalo	používat	k5eAaImAgNnS
již	již	k6eAd1
nový	nový	k2eAgInSc4d1
<g/>
,	,	kIx,
elektronický	elektronický	k2eAgInSc4d1
systém	systém	k1gInSc4
namísto	namísto	k7c2
původního	původní	k2eAgInSc2d1
mechanického	mechanický	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
této	tento	k3xDgFnSc2
epochy	epocha	k1gFnSc2
bylo	být	k5eAaImAgNnS
bezpochyby	bezpochyby	k6eAd1
tzv.	tzv.	kA
„	„	k?
<g/>
Empire	empir	k1gInSc5
Service	Service	k1gFnPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgInSc1d1
„	„	k?
<g/>
World	World	k1gInSc1
Service	Service	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vysílání	vysílání	k1gNnSc6
BBC	BBC	kA
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Během	během	k7c2
války	válka	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přerušení	přerušení	k1gNnSc3
TV	TV	kA
vysílání	vysílání	k1gNnSc2
a	a	k8xC
dokonce	dokonce	k9
k	k	k7c3
částečné	částečný	k2eAgFnSc3d1
cenzuře	cenzura	k1gFnSc3
vysílání	vysílání	k1gNnSc2
rozhlasového	rozhlasový	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
však	však	k9
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
významnému	významný	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
programu	program	k1gInSc2
<g/>
,	,	kIx,
především	především	k6eAd1
k	k	k7c3
posunu	posun	k1gInSc3
k	k	k7c3
výchově	výchova	k1gFnSc3
<g/>
,	,	kIx,
informování	informování	k1gNnSc3
a	a	k8xC
užitečnosti	užitečnost	k1gFnSc3
rozhlasu	rozhlas	k1gInSc2
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejen	nejen	k6eAd1
pro	pro	k7c4
vojáky	voják	k1gMnPc4
byl	být	k5eAaImAgMnS
určen	určen	k2eAgMnSc1d1
zábavní	zábavní	k2eAgMnSc1d1
a	a	k8xC
povzbuzující	povzbuzující	k2eAgInSc1d1
pořad	pořad	k1gInSc1
Forces	Forcesa	k1gFnPc2
Programme	Programme	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
třeba	třeba	k9
pořady	pořad	k1gInPc1
o	o	k7c4
pěstování	pěstování	k1gNnSc4
zeleniny	zelenina	k1gFnSc2
(	(	kIx(
<g/>
VB	VB	kA
trpěla	trpět	k5eAaImAgFnS
nedostatkem	nedostatek	k1gInSc7
potravin	potravina	k1gFnPc2
z	z	k7c2
kontinentu	kontinent	k1gInSc2
kvůli	kvůli	k7c3
blokádě	blokáda	k1gFnSc3
<g/>
)	)	kIx)
nebo	nebo	k8xC
o	o	k7c4
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slogan	slogan	k1gInSc1
„	„	k?
<g/>
V	v	k7c6
for	forum	k1gNnPc2
Victory	Victor	k1gMnPc4
<g/>
“	“	k?
se	se	k3xPyFc4
brzy	brzy	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
po	po	k7c6
mnoha	mnoho	k4c6
partyzánských	partyzánský	k2eAgNnPc6d1
hnutích	hnutí	k1gNnPc6
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
pro	pro	k7c4
která	který	k3yRgNnPc4,k3yQgNnPc4,k3yIgNnPc4
bylo	být	k5eAaImAgNnS
vysílání	vysílání	k1gNnSc4
BBC	BBC	kA
velkou	velký	k2eAgFnSc7d1
oporou	opora	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
začala	začít	k5eAaPmAgFnS
vysílat	vysílat	k5eAaImF
v	v	k7c6
arabštině	arabština	k1gFnSc6
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
v	v	k7c6
různých	různý	k2eAgInPc6d1
evropských	evropský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
BBC	BBC	kA
byly	být	k5eAaImAgFnP
také	také	k9
posílány	posílán	k2eAgInPc4d1
kódy	kód	k1gInPc4
týkající	týkající	k2eAgInPc4d1
se	se	k3xPyFc4
dne	den	k1gInSc2
D	D	kA
–	–	k?
invaze	invaze	k1gFnSc1
v	v	k7c6
Normandii	Normandie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hugh	Hugh	k1gInSc1
Greene	Green	k1gInSc5
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
German	German	k1gMnSc1
Service	Service	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
BBC	BBC	kA
<g/>
)	)	kIx)
zase	zase	k9
vysílal	vysílat	k5eAaImAgInS
varování	varování	k1gNnSc4
určená	určený	k2eAgNnPc1d1
německým	německý	k2eAgMnPc3d1
občanům	občan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Následuje	následovat	k5eAaImIp3nS
rychlý	rychlý	k2eAgInSc1d1
růst	růst	k1gInSc1
–	–	k?
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
je	být	k5eAaImIp3nS
obnoveno	obnovit	k5eAaPmNgNnS
televizní	televizní	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
nadšeným	nadšený	k2eAgInSc7d1
ohlasem	ohlas	k1gInSc7
(	(	kIx(
<g/>
zvláště	zvláště	k6eAd1
po	po	k7c6
vysílání	vysílání	k1gNnSc6
svatby	svatba	k1gFnSc2
princezny	princezna	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
a	a	k8xC
vévody	vévoda	k1gMnSc2
z	z	k7c2
Edinburghu	Edinburgh	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
přibyl	přibýt	k5eAaPmAgMnS
i	i	k9
třetí	třetí	k4xOgInSc4
rozhlasový	rozhlasový	k2eAgInSc4d1
kanál	kanál	k1gInSc4
(	(	kIx(
<g/>
prozaicky	prozaicky	k6eAd1
nazván	nazván	k2eAgMnSc1d1
Third	Third	k1gMnSc1
Programme	Programme	k1gMnSc1
<g/>
)	)	kIx)
přinášející	přinášející	k2eAgMnSc1d1
zejména	zejména	k9
vážnou	vážný	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
a	a	k8xC
další	další	k2eAgInPc4d1
kulturní	kulturní	k2eAgInPc4d1
pořady	pořad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
se	se	k3xPyFc4
týdně	týdně	k6eAd1
odvysílalo	odvysílat	k5eAaPmAgNnS
30	#num#	k4
televizních	televizní	k2eAgMnPc2d1
a	a	k8xC
260	#num#	k4
rozhlasových	rozhlasový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Televizním	televizní	k2eAgInSc7d1
signálem	signál	k1gInSc7
byla	být	k5eAaImAgFnS
pokryta	pokrýt	k5eAaPmNgFnS
zhruba	zhruba	k6eAd1
polovina	polovina	k1gFnSc1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přímý	přímý	k2eAgInSc1d1
přenos	přenos	k1gInSc1
korunovace	korunovace	k1gFnSc2
Alžběty	Alžběta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
výraznému	výrazný	k2eAgNnSc3d1
rozšíření	rozšíření	k1gNnSc3
mladého	mladý	k2eAgNnSc2d1
televizního	televizní	k2eAgNnSc2d1
média	médium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
nato	nato	k6eAd1
již	již	k6eAd1
BBC	BBC	kA
získává	získávat	k5eAaImIp3nS
konkurenta	konkurent	k1gMnSc4
–	–	k?
ITV	ITV	kA
(	(	kIx(
<g/>
Independent	independent	k1gMnSc1
Television	Television	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
vstupuje	vstupovat	k5eAaImIp3nS
na	na	k7c4
trh	trh	k1gInSc4
a	a	k8xC
zakrátko	zakrátko	k6eAd1
má	mít	k5eAaImIp3nS
k	k	k7c3
televizi	televize	k1gFnSc3
přístup	přístup	k1gInSc4
přes	přes	k7c4
90	#num#	k4
<g/>
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
začíná	začínat	k5eAaImIp3nS
bojovat	bojovat	k5eAaImF
o	o	k7c4
posluchače	posluchač	k1gMnPc4
a	a	k8xC
diváky	divák	k1gMnPc4
–	–	k?
podíl	podíl	k1gInSc4
posluchačů	posluchač	k1gMnPc2
rádia	rádio	k1gNnSc2
klesl	klesnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
na	na	k7c4
pouhých	pouhý	k2eAgInPc2d1
28	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
boje	boj	k1gInSc2
s	s	k7c7
televizním	televizní	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
se	se	k3xPyFc4
pustil	pustit	k5eAaPmAgMnS
nový	nový	k2eAgMnSc1d1
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Sir	sir	k1gMnSc1
Ian	Ian	k1gMnSc1
Jacob	Jacoba	k1gFnPc2
a	a	k8xC
dokončoval	dokončovat	k5eAaImAgMnS
jej	on	k3xPp3gMnSc4
Sir	sir	k1gMnSc1
Hugh	Hugh	k1gMnSc1
Greene	Green	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
sice	sice	k8xC
propast	propast	k1gFnSc1
zvětšovala	zvětšovat	k5eAaImAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
rádio	rádio	k1gNnSc1
nadále	nadále	k6eAd1
excelovalo	excelovat	k5eAaImAgNnS
(	(	kIx(
<g/>
show	show	k1gFnSc1
„	„	k?
<g/>
The	The	k1gFnSc1
Archers	Archers	k1gInSc1
<g/>
“	“	k?
běží	běžet	k5eAaImIp3nS
již	již	k6eAd1
od	od	k7c2
1951	#num#	k4
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
TV	TV	kA
zase	zase	k9
produkovala	produkovat	k5eAaImAgFnS
adaptaci	adaptace	k1gFnSc4
Orwellova	Orwellův	k2eAgInSc2d1
románu	román	k1gInSc2
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Následují	následovat	k5eAaImIp3nP
technické	technický	k2eAgFnSc2d1
inovace	inovace	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
vysílání	vysílání	k1gNnSc4
na	na	k7c6
VHF	VHF	kA
–	–	k?
very	vera	k1gFnSc2
high	higha	k1gFnPc2
frequency	frequenca	k1gFnSc2
<g/>
,	,	kIx,
stereofonní	stereofonní	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
<g/>
,	,	kIx,
barevná	barevný	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
televizní	televizní	k2eAgInSc1d1
kanál	kanál	k1gInSc1
BBC	BBC	kA
2	#num#	k4
<g/>
)	)	kIx)
i	i	k8xC
změny	změna	k1gFnPc1
kosmetické	kosmetický	k2eAgFnPc1d1
(	(	kIx(
<g/>
přejmenování	přejmenování	k1gNnSc4
rádiových	rádiový	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
na	na	k7c4
Radio	radio	k1gNnSc4
1	#num#	k4
až	až	k9
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
dosahoval	dosahovat	k5eAaImAgInS
počet	počet	k1gInSc1
diváků	divák	k1gMnPc2
20	#num#	k4
milionů	milion	k4xCgInPc2
a	a	k8xC
počet	počet	k1gInSc1
posluchačů	posluchač	k1gMnPc2
5	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
finančních	finanční	k2eAgFnPc2d1
potíží	potíž	k1gFnPc2
<g/>
,	,	kIx,
situace	situace	k1gFnSc1
byla	být	k5eAaImAgFnS
podle	podle	k7c2
slov	slovo	k1gNnPc2
ředitele	ředitel	k1gMnSc2
Sira	sir	k1gMnSc2
Michaela	Michael	k1gMnSc2
Swanna	Swann	k1gMnSc2
ponurá	ponurý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byl	být	k5eAaImAgInS
Channel	Channel	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
komerční	komerční	k2eAgInSc1d1
program	program	k1gInSc1
se	s	k7c7
snahou	snaha	k1gFnSc7
o	o	k7c6
inovaci	inovace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
White	White	k5eAaPmIp2nP
Paper	Paper	k1gInSc1
popsal	popsat	k5eAaPmAgMnS
BBC	BBC	kA
jako	jako	k8xC,k8xS
„	„	k?
<g/>
pravděpodobně	pravděpodobně	k6eAd1
nejdůležitější	důležitý	k2eAgFnSc4d3
kulturní	kulturní	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
v	v	k7c6
celém	celý	k2eAgInSc6d1
národě	národ	k1gInSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokládají	dokládat	k5eAaImIp3nP
to	ten	k3xDgNnSc4
i	i	k9
Shakespeare	Shakespeare	k1gMnSc1
Project	Project	k1gMnSc1
nebo	nebo	k8xC
Yes	Yes	k1gMnSc1
Minister	Minister	k1gMnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
známý	známý	k2eAgInSc1d1
jako	jako	k8xS,k8xC
Jistě	jistě	k9
<g/>
,	,	kIx,
pane	pan	k1gMnSc5
ministře	ministr	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
úspěšné	úspěšný	k2eAgInPc1d1
pořady	pořad	k1gInPc1
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Nastupují	nastupovat	k5eAaImIp3nP
nejen	nejen	k6eAd1
nové	nový	k2eAgFnPc4d1
technické	technický	k2eAgFnPc4d1
vymoženosti	vymoženost	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
nový	nový	k2eAgInSc4d1
přístup	přístup	k1gInSc4
<g/>
,	,	kIx,
především	především	k9
také	také	k9
díky	díky	k7c3
vlivu	vliv	k1gInSc3
konzervativní	konzervativní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
(	(	kIx(
<g/>
Margaret	Margareta	k1gFnPc2
Thatcherová	Thatcherová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
byla	být	k5eAaImAgFnS
světově	světově	k6eAd1
první	první	k4xOgFnSc4
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
začala	začít	k5eAaPmAgFnS
vysílat	vysílat	k5eAaImF
DBS	DBS	kA
(	(	kIx(
<g/>
Direct-broadcasting-by-satelite	Direct-broadcasting-by-satelit	k1gInSc5
<g/>
)	)	kIx)
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
1987	#num#	k4
přinesl	přinést	k5eAaPmAgInS
významné	významný	k2eAgFnSc3d1
a	a	k8xC
nemilé	milý	k2eNgFnSc3d1
události	událost	k1gFnSc3
–	–	k?
v	v	k7c6
ústředí	ústředí	k1gNnSc6
v	v	k7c6
Glasgow	Glasgow	k1gNnSc6
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
policejní	policejní	k2eAgFnSc1d1
razie	razie	k1gFnSc1
a	a	k8xC
odvolán	odvolán	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Alasdair	Alasdair	k1gMnSc1
Milne	Miln	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
ředitel	ředitel	k1gMnSc1
Micheal	Micheal	k1gMnSc1
Checkland	Checklando	k1gNnPc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
John	John	k1gMnSc1
Birt	Birt	k1gMnSc1
vedli	vést	k5eAaImAgMnP
BBC	BBC	kA
skrze	skrze	k?
rychlou	rychlý	k2eAgFnSc4d1
a	a	k8xC
nesnadnou	snadný	k2eNgFnSc4d1
transformaci	transformace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radikálně	radikálně	k6eAd1
se	se	k3xPyFc4
snížil	snížit	k5eAaPmAgInS
počet	počet	k1gInSc1
zaměstnanců	zaměstnanec	k1gMnPc2
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
praktikovat	praktikovat	k5eAaImF
nový	nový	k2eAgInSc4d1
výběr	výběr	k1gInSc4
pořadů	pořad	k1gInPc2
<g/>
,	,	kIx,
jakýsi	jakýsi	k3yIgInSc1
vnitřní	vnitřní	k2eAgInSc1d1
trh	trh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Managementu	management	k1gInSc3
se	se	k3xPyFc4
povedlo	povést	k5eAaPmAgNnS
zvýšit	zvýšit	k5eAaPmF
veřejnoprávní	veřejnoprávní	k2eAgInPc4d1
poplatky	poplatek	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
rozšířit	rozšířit	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
nabídku	nabídka	k1gFnSc4
služeb	služba	k1gFnPc2
i	i	k9
o	o	k7c4
digitální	digitální	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
působily	působit	k5eAaImAgFnP
po	po	k7c6
reorganizaci	reorganizace	k1gFnSc6
následující	následující	k2eAgNnSc4d1
ředitelství	ředitelství	k1gNnSc4
<g/>
:	:	kIx,
BBC	BBC	kA
Broadcast	Broadcast	k1gInSc1
<g/>
,	,	kIx,
BBC	BBC	kA
News	News	k1gInSc1
<g/>
,	,	kIx,
BBC	BBC	kA
Production	Production	k1gInSc1
<g/>
,	,	kIx,
BBC	BBC	kA
Resources	Resources	k1gInSc1
a	a	k8xC
BBC	BBC	kA
Worldwide	Worldwid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
dělán	dělán	k2eAgInSc4d1
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c7
TV	TV	kA
a	a	k8xC
rozhlasem	rozhlas	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
digitálnímu	digitální	k2eAgNnSc3d1
vysílání	vysílání	k1gNnSc3
uzavřela	uzavřít	k5eAaPmAgFnS
BBC	BBC	kA
dohodu	dohoda	k1gFnSc4
se	s	k7c7
společností	společnost	k1gFnSc7
Flextech	Flexto	k1gNnPc6
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1997	#num#	k4
se	se	k3xPyFc4
rozbíhá	rozbíhat	k5eAaImIp3nS
nepřetržitý	přetržitý	k2eNgInSc1d1
zpravodajský	zpravodajský	k2eAgInSc1d1
kanál	kanál	k1gInSc1
BBC	BBC	kA
News	News	k1gInSc4
24	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
jej	on	k3xPp3gMnSc4
následuje	následovat	k5eAaImIp3nS
BBC	BBC	kA
Choice	Choice	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
BBC	BBC	kA
Knowledge	Knowledge	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
světa	svět	k1gInSc2
internetových	internetový	k2eAgNnPc2d1
médií	médium	k1gNnPc2
vstoupila	vstoupit	k5eAaPmAgFnS
BBC	BBC	kA
svými	svůj	k3xOyFgMnPc7
BBC	BBC	kA
Online	Onlin	k1gInSc5
a	a	k8xC
později	pozdě	k6eAd2
BBC	BBC	kA
Website	Websit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
</s>
<s>
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
čelil	čelit	k5eAaImAgInS
ředitel	ředitel	k1gMnSc1
Greg	Grega	k1gFnPc2
Dyke	Dyke	k1gInSc1
výzvě	výzva	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
představuje	představovat	k5eAaImIp3nS
konkurence	konkurence	k1gFnSc1
na	na	k7c6
poli	pole	k1gNnSc6
kabelových	kabelový	k2eAgFnPc2d1
televizí	televize	k1gFnPc2
a	a	k8xC
satelitních	satelitní	k2eAgInPc2d1
kanálů	kanál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
škrtů	škrt	k1gInPc2
v	v	k7c6
rozpočtu	rozpočet	k1gInSc6
umožňuje	umožňovat	k5eAaImIp3nS
BBC	BBC	kA
věnovat	věnovat	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
nyní	nyní	k6eAd1
více	hodně	k6eAd2
digitálnímu	digitální	k2eAgNnSc3d1
vysílání	vysílání	k1gNnSc3
a	a	k8xC
jeho	jeho	k3xOp3gInSc3
rozvoji	rozvoj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
příštího	příští	k2eAgNnSc2d1
desetiletí	desetiletí	k1gNnSc2
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
ukončeno	ukončen	k2eAgNnSc4d1
analogové	analogový	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
a	a	k8xC
plně	plně	k6eAd1
nahrazeno	nahradit	k5eAaPmNgNnS
digitálním	digitální	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2001	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
radikálním	radikální	k2eAgFnPc3d1
programovým	programový	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
:	:	kIx,
byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
digitální	digitální	k2eAgInSc1d1
program	program	k1gInSc1
BBC	BBC	kA
pro	pro	k7c4
Wales	Wales	k1gInSc4
(	(	kIx(
<g/>
BBC	BBC	kA
2	#num#	k4
<g/>
W	W	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
spuštěn	spuštěn	k2eAgInSc4d1
BBC	BBC	kA
Four	Four	k1gInSc4
<g/>
,	,	kIx,
přibyly	přibýt	k5eAaPmAgInP
dva	dva	k4xCgInPc1
nové	nový	k2eAgInPc1d1
dětské	dětský	k2eAgInPc1d1
kanály	kanál	k1gInPc1
(	(	kIx(
<g/>
CBeebies	CBeebies	k1gInSc1
a	a	k8xC
CBBC	CBBC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
BBC	BBC	kA
Choice	Choice	k1gFnSc2
bylo	být	k5eAaImAgNnS
nahrazeno	nahradit	k5eAaPmNgNnS
BBC	BBC	kA
Three	Three	k1gFnSc6
(	(	kIx(
<g/>
kanál	kanál	k1gInSc1
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
diváky	divák	k1gMnPc4
mezi	mezi	k7c4
25	#num#	k4
a	a	k8xC
34	#num#	k4
lety	let	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přibylo	přibýt	k5eAaPmAgNnS
také	také	k9
pět	pět	k4xCc1
digitálních	digitální	k2eAgFnPc2d1
rozhlasových	rozhlasový	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
,	,	kIx,
povětšinou	povětšinou	k6eAd1
úzce	úzko	k6eAd1
specializovaných	specializovaný	k2eAgFnPc2d1
na	na	k7c4
menšinové	menšinový	k2eAgNnSc4d1
publikum	publikum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
takřka	takřka	k6eAd1
osmdesátiletém	osmdesátiletý	k2eAgNnSc6d1
působení	působení	k1gNnSc6
rozpadlo	rozpadnout	k5eAaPmAgNnS
výukové	výukový	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
(	(	kIx(
<g/>
Education	Education	k1gInSc1
Department	department	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
staralo	starat	k5eAaImAgNnS
např.	např.	kA
o	o	k7c6
vysílání	vysílání	k1gNnSc6
do	do	k7c2
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2004	#num#	k4
se	se	k3xPyFc4
veřejnost	veřejnost	k1gFnSc1
zajímala	zajímat	k5eAaImAgFnS
o	o	k7c4
smrt	smrt	k1gFnSc4
Davida	David	k1gMnSc2
Kellyho	Kelly	k1gMnSc2
<g/>
,	,	kIx,
vědeckého	vědecký	k2eAgMnSc2d1
poradce	poradce	k1gMnSc2
ministra	ministr	k1gMnSc2
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
následkem	následkem	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
rezignovali	rezignovat	k5eAaBmAgMnP
na	na	k7c4
své	své	k1gNnSc4
funkce	funkce	k1gFnSc2
Andrew	Andrew	k1gMnSc1
Gillian	Gillian	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
,	,	kIx,
dokonce	dokonce	k9
i	i	k9
sám	sám	k3xTgMnSc1
ředitel	ředitel	k1gMnSc1
Dyke	Dyk	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Michael	Michael	k1gMnSc1
Grade	grad	k1gInSc5
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
ředitelem	ředitel	k1gMnSc7
BBC	BBC	kA
v	v	k7c6
květnu	květen	k1gInSc6
2004	#num#	k4
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c4
dobu	doba	k1gFnSc4
čtyř	čtyři	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
World	World	k1gMnSc1
Service	Service	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
BBC	BBC	kA
World	Worlda	k1gFnPc2
Service	Service	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
vysílá	vysílat	k5eAaImIp3nS
do	do	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
rozhlasové	rozhlasový	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
BBC	BBC	kA
World	World	k1gInSc4
Service	Service	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	on	k3xPp3gFnPc4
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
zbytku	zbytek	k1gInSc2
BBC	BBC	kA
financována	financovat	k5eAaBmNgFnS
z	z	k7c2
rozpočtu	rozpočet	k1gInSc2
britského	britský	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
zahraničí	zahraničí	k1gNnSc2
a	a	k8xC
vysílá	vysílat	k5eAaImIp3nS
přes	přes	k7c4
internet	internet	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
krátkých	krátký	k2eAgFnPc6d1
vlnách	vlna	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
vybraných	vybraný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
také	také	k9
přes	přes	k7c4
FM	FM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c6
zemích	zem	k1gFnPc6
má	mít	k5eAaImIp3nS
i	i	k8xC
regionální	regionální	k2eAgNnPc1d1
studia	studio	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
pokrývají	pokrývat	k5eAaImIp3nP
určitou	určitý	k2eAgFnSc4d1
část	část	k1gFnSc4
vysílání	vysílání	k1gNnSc2
v	v	k7c6
jazyce	jazyk	k1gInSc6
dané	daný	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
část	část	k1gFnSc4
těchto	tento	k3xDgFnPc2
studií	studie	k1gFnPc2
zrušit	zrušit	k5eAaPmF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
postihlo	postihnout	k5eAaPmAgNnS
i	i	k9
českou	český	k2eAgFnSc4d1
redakci	redakce	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ukončila	ukončit	k5eAaPmAgFnS
vysílání	vysílání	k1gNnSc4
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ušetřených	ušetřený	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
je	být	k5eAaImIp3nS
financován	financován	k2eAgInSc1d1
projekt	projekt	k1gInSc1
televizního	televizní	k2eAgNnSc2d1
vysílání	vysílání	k1gNnSc2
v	v	k7c6
arabštině	arabština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kanály	kanál	k1gInPc1
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
</s>
<s>
Televizní	televizní	k2eAgFnSc1d1
</s>
<s>
Ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
přijímat	přijímat	k5eAaImF
volně	volně	k6eAd1
kanály	kanál	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
BBC	BBC	kA
One	One	k1gFnSc1
</s>
<s>
BBC	BBC	kA
One	One	k1gFnSc1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc4d1
kanál	kanál	k1gInSc4
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
vysílat	vysílat	k5eAaImF
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1936	#num#	k4
jako	jako	k8xS,k8xC
BBC	BBC	kA
Television	Television	k1gInSc1
Servise	servis	k1gInSc6
a	a	k8xC
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
oficiálně	oficiálně	k6eAd1
vysílaný	vysílaný	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
kanál	kanál	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
je	být	k5eAaImIp3nS
kanál	kanál	k1gInSc1
dostupný	dostupný	k2eAgInSc1d1
také	také	k9
v	v	k7c6
HD	HD	kA
rozlišení	rozlišení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byla	být	k5eAaImAgFnS
průměrná	průměrný	k2eAgFnSc1d1
sledovanost	sledovanost	k1gFnSc1
21	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
One	One	k1gFnPc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
celkové	celkový	k2eAgNnSc4d1
i	i	k8xC
rodinné	rodinný	k2eAgNnSc4d1
publikum	publikum	k1gNnSc4
s	s	k7c7
všeobecným	všeobecný	k2eAgInSc7d1
programem	program	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
zpravodajství	zpravodajství	k1gNnSc2
<g/>
,	,	kIx,
sport	sport	k1gInSc1
<g/>
,	,	kIx,
filmy	film	k1gInPc1
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
i	i	k8xC
zahraniční	zahraniční	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
i	i	k8xC
dokumenty	dokument	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
celosvětově	celosvětově	k6eAd1
známé	známý	k2eAgInPc4d1
pořady	pořad	k1gInPc4
vysílané	vysílaný	k2eAgInPc4d1
na	na	k7c4
BBC	BBC	kA
One	One	k1gFnSc2
patří	patřit	k5eAaImIp3nS
např.	např.	kA
soap	soap	k1gInSc1
opera	opera	k1gFnSc1
EastEnders	EastEnders	k1gInSc1
<g/>
,	,	kIx,
seriály	seriál	k1gInPc1
Pán	pán	k1gMnSc1
času	čas	k1gInSc2
<g/>
,	,	kIx,
Sherlock	Sherlock	k1gMnSc1
<g/>
,	,	kIx,
Otec	otec	k1gMnSc1
Brown	Brown	k1gMnSc1
či	či	k8xC
pořad	pořad	k1gInSc1
o	o	k7c6
automobilismu	automobilismus	k1gInSc6
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahraniční	zahraniční	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
jen	jen	k9
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existující	existující	k2eAgFnSc1d1
také	také	k6eAd1
regionální	regionální	k2eAgFnSc1d1
mutace	mutace	k1gFnSc1
BBC	BBC	kA
One	One	k1gMnPc3
provozované	provozovaný	k2eAgFnPc4d1
regionální	regionální	k2eAgFnSc4d1
studii	studie	k1gFnSc4
BBC	BBC	kA
<g/>
:	:	kIx,
BBC	BBC	kA
One	One	k1gFnSc1
Wales	Wales	k1gInSc1
(	(	kIx(
<g/>
pro	pro	k7c4
Wales	Wales	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
BBC	BBC	kA
One	One	k1gFnSc1
Scotland	Scotland	k1gInSc1
(	(	kIx(
<g/>
pro	pro	k7c4
Skotsko	Skotsko	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
BBC	BBC	kA
One	One	k1gFnSc1
Northern	Northern	k1gMnSc1
Ireland	Ireland	k1gInSc1
(	(	kIx(
<g/>
pro	pro	k7c4
Severní	severní	k2eAgNnSc4d1
Irsko	Irsko	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
BBC	BBC	kA
Two	Two	k1gFnSc1
</s>
<s>
BBC	BBC	kA
Two	Two	k1gFnSc1
je	být	k5eAaImIp3nS
domovem	domov	k1gInSc7
specializovaných	specializovaný	k2eAgInPc2d1
pořadů	pořad	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
dramata	drama	k1gNnPc1
<g/>
,	,	kIx,
dokumenty	dokument	k1gInPc1
<g/>
,	,	kIx,
pořady	pořad	k1gInPc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
programy	program	k1gInPc4
se	s	k7c7
speciálním	speciální	k2eAgNnSc7d1
zaměřením	zaměření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Two	Two	k1gFnSc1
začala	začít	k5eAaPmAgFnS
vysílat	vysílat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
jako	jako	k9
první	první	k4xOgFnSc1
barevná	barevný	k2eAgFnSc1d1
televize	televize	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
tedy	tedy	k9
jakýsi	jakýsi	k3yIgInSc1
"	"	kIx"
<g/>
doplňkový	doplňkový	k2eAgInSc1d1
<g/>
"	"	kIx"
kanál	kanál	k1gInSc1
BBC	BBC	kA
One	One	k1gFnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
pořady	pořad	k1gInPc1
jsou	být	k5eAaImIp3nP
sem	sem	k6eAd1
často	často	k6eAd1
přesouvány	přesouvat	k5eAaImNgFnP
z	z	k7c2
BBC	BBC	kA
One	One	k1gFnSc2
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
je	být	k5eAaImIp3nS
stanice	stanice	k1gFnSc1
dostupná	dostupný	k2eAgFnSc1d1
také	také	k9
v	v	k7c6
HD	HD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byla	být	k5eAaImAgFnS
její	její	k3xOp3gFnSc1
sledovanost	sledovanost	k1gFnSc1
5,4	5,4	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Three	Three	k1gFnSc1
</s>
<s>
BBC	BBC	kA
Three	Three	k1gFnSc1
je	být	k5eAaImIp3nS
kanál	kanál	k1gInSc4
pro	pro	k7c4
mladé	mladý	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
ve	v	k7c6
věku	věk	k1gInSc6
15	#num#	k4
–	–	k?
34	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
komedie	komedie	k1gFnPc4
<g/>
,	,	kIx,
sitcomy	sitcom	k1gInPc4
<g/>
,	,	kIx,
reality	realita	k1gFnPc4
show	show	k1gFnSc2
ad	ad	k7c4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
ovšem	ovšem	k9
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
byl	být	k5eAaImAgInS
jako	jako	k9
televizní	televizní	k2eAgInSc1d1
kanál	kanál	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
pouze	pouze	k6eAd1
internetově	internetově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
zábavné	zábavný	k2eAgInPc4d1
pořady	pořad	k1gInPc4
a	a	k8xC
reality	realita	k1gFnPc4
show	show	k1gFnPc2
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
Malá	malý	k2eAgFnSc1d1
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
i	i	k8xC
zahraniční	zahraniční	k2eAgInPc4d1
pořady	pořad	k1gInPc4
(	(	kIx(
<g/>
např.	např.	kA
Griffinovi	Griffin	k1gMnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
také	také	k9
vlastní	vlastní	k2eAgInSc4d1
zpravodajský	zpravodajský	k2eAgInSc4d1
blok	blok	k1gInSc4
60	#num#	k4
second	seconda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byla	být	k5eAaImAgFnS
průměrná	průměrný	k2eAgFnSc1d1
sledovanost	sledovanost	k1gFnSc1
stanice	stanice	k1gFnSc2
1,4	1,4	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Four	Four	k1gMnSc1
</s>
<s>
BBC	BBC	kA
Four	Four	k1gMnSc1
je	být	k5eAaImIp3nS
kanál	kanál	k1gInSc4
zaměřený	zaměřený	k2eAgInSc4d1
čistě	čistě	k6eAd1
na	na	k7c4
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
světovou	světový	k2eAgFnSc4d1
kinematografii	kinematografie	k1gFnSc4
a	a	k8xC
dokumenty	dokument	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
exkluzivní	exkluzivní	k2eAgInSc1d1
program	program	k1gInSc1
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
i	i	k8xC
zahraniční	zahraniční	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
např.	např.	kA
Monty	Monta	k1gFnSc2
Pythonům	Python	k1gMnPc3
létající	létající	k2eAgInSc1d1
cirkus	cirkus	k1gInSc1
a	a	k8xC
ze	z	k7c2
zahraniční	zahraniční	k2eAgFnSc2d1
např.	např.	kA
Šílenci	šílenec	k1gMnSc6
z	z	k7c2
Mannhattanu	Mannhattan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanál	kanál	k1gInSc1
vysílá	vysílat	k5eAaImIp3nS
do	do	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2013	#num#	k4
dosahoval	dosahovat	k5eAaImAgMnS
průměrné	průměrný	k2eAgFnSc3d1
sledovanosti	sledovanost	k1gFnSc3
téměř	téměř	k6eAd1
1	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
News	News	k1gInSc1
</s>
<s>
BBC	BBC	kA
News	News	k1gInSc1
je	být	k5eAaImIp3nS
24	#num#	k4
hodinová	hodinový	k2eAgFnSc1d1
zpravodajská	zpravodajský	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
vysílající	vysílající	k2eAgNnSc4d1
pouze	pouze	k6eAd1
zpravodajství	zpravodajství	k1gNnSc4
a	a	k8xC
publicistiku	publicistika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
provozována	provozovat	k5eAaImNgFnS
skrz	skrz	k7c4
provozní	provozní	k2eAgNnSc4d1
oddělení	oddělení	k1gNnSc4
BBC	BBC	kA
News	News	k1gInSc4
zaměstnávající	zaměstnávající	k2eAgFnSc1d1
asi	asi	k9
2000	#num#	k4
novinářů	novinář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Parliament	Parliament	k1gMnSc1
</s>
<s>
BBC	BBC	kA
Parliament	Parliament	k1gMnSc1
je	být	k5eAaImIp3nS
stanice	stanice	k1gFnPc4
vysílající	vysílající	k2eAgFnSc1d1
živá	živá	k1gFnSc1
jednání	jednání	k1gNnSc2
z	z	k7c2
Parlamentu	parlament	k1gInSc2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
Skotského	skotský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
Velšského	velšský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
a	a	k8xC
Severoirského	severoirský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příležitostně	příležitostně	k6eAd1
také	také	k9
vysílá	vysílat	k5eAaImIp3nS
z	z	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
nebo	nebo	k8xC
Nejvyššího	vysoký	k2eAgInSc2d3
koncilu	koncil	k1gInSc2
anglikánské	anglikánský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílání	vysílání	k1gNnSc1
bylo	být	k5eAaImAgNnS
spuštěno	spustit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
průměrná	průměrný	k2eAgFnSc1d1
sledovanost	sledovanost	k1gFnSc1
je	být	k5eAaImIp3nS
nízká	nízký	k2eAgFnSc1d1
(	(	kIx(
<g/>
0,04	0,04	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
CBBC	CBBC	kA
</s>
<s>
CBBC	CBBC	kA
je	být	k5eAaImIp3nS
populární	populární	k2eAgInSc1d1
kanál	kanál	k1gInSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
od	od	k7c2
6	#num#	k4
do	do	k7c2
12	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
vysílat	vysílat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
a	a	k8xC
vysílá	vysílat	k5eAaImIp3nS
dětské	dětský	k2eAgInPc4d1
pořady	pořad	k1gInPc4
z	z	k7c2
původní	původní	k2eAgFnSc2d1
i	i	k8xC
zahraniční	zahraniční	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
CBeebies	CBeebies	k1gMnSc1
</s>
<s>
CBeebies	CBeebies	k1gInSc1
je	být	k5eAaImIp3nS
kanál	kanál	k1gInSc4
pro	pro	k7c4
děti	dítě	k1gFnPc4
mladší	mladý	k2eAgMnPc1d2
6	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
dětské	dětský	k2eAgInPc4d1
pořady	pořad	k1gInPc4
především	především	k9
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Alba	alba	k1gFnSc1
</s>
<s>
BBC	BBC	kA
Alba	alba	k1gFnSc1
je	být	k5eAaImIp3nS
regionální	regionální	k2eAgInSc4d1
kanál	kanál	k1gInSc4
vysílající	vysílající	k2eAgInSc4d1
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
ve	v	k7c6
skotské	skotský	k2eAgFnSc6d1
gaelštině	gaelština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
celkový	celkový	k2eAgInSc1d1
rodinný	rodinný	k2eAgInSc1d1
program	program	k1gInSc1
vč.	vč.	k?
pořadů	pořad	k1gInPc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
S4C	S4C	k4
</s>
<s>
S4C	S4C	k4
je	být	k5eAaImIp3nS
regionální	regionální	k2eAgInSc4d1
kanál	kanál	k1gInSc4
pro	pro	k7c4
Wales	Wales	k1gInSc4
vysílající	vysílající	k2eAgInSc4d1
ve	v	k7c6
velštině	velština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
spuštěn	spustit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozhlasové	rozhlasový	k2eAgNnSc1d1
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
1	#num#	k4
</s>
<s>
Kultovní	kultovní	k2eAgFnSc1d1
rozhlasová	rozhlasový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
vysílající	vysílající	k2eAgFnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
pro	pro	k7c4
mladé	mladý	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
od	od	k7c2
15	#num#	k4
do	do	k7c2
30	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
vychovala	vychovat	k5eAaPmAgFnS
spoustu	spousta	k1gFnSc4
v	v	k7c6
Británii	Británie	k1gFnSc6
známých	známý	k2eAgInPc2d1
dýdžejů	dýdžej	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
stanice	stanice	k1gFnSc1
prošla	projít	k5eAaPmAgFnS
velkou	velký	k2eAgFnSc7d1
obměnou	obměna	k1gFnSc7
s	s	k7c7
novým	nový	k2eAgNnSc7d1
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
mladé	mladý	k2eAgNnSc4d1
publikum	publikum	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
vydrželo	vydržet	k5eAaPmAgNnS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejím	její	k3xOp3gInSc6
playlistu	playlista	k1gMnSc4
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
především	především	k9
aktuální	aktuální	k2eAgInPc4d1
hity	hit	k1gInPc4
světových	světový	k2eAgFnPc2d1
a	a	k8xC
britských	britský	k2eAgFnPc2d1
hitparád	hitparáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1987	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
se	se	k3xPyFc4
jmenovala	jmenovat	k5eAaImAgFnS,k5eAaBmAgFnS
BBC	BBC	kA
Radio	radio	k1gNnSc1
1	#num#	k4
FM	FM	kA
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
1	#num#	k4
<g/>
Xtra	Xtrum	k1gNnSc2
</s>
<s>
Sesterská	sesterský	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Radia	radio	k1gNnSc2
1	#num#	k4
je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
black	black	k1gInSc4
music	musice	k1gFnPc2
<g/>
,	,	kIx,
RnB	RnB	k1gFnPc2
<g/>
,	,	kIx,
hip	hip	k0
hop	hop	k0
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
2002	#num#	k4
a	a	k8xC
vysílá	vysílat	k5eAaImIp3nS
na	na	k7c6
DAB	DAB	kA
<g/>
,	,	kIx,
každý	každý	k3xTgInSc4
víkend	víkend	k1gInSc4
od	od	k7c2
19	#num#	k4
do	do	k7c2
5	#num#	k4
hodin	hodina	k1gFnPc2
její	její	k3xOp3gMnPc4
vysílání	vysílání	k1gNnSc1
přebírá	přebírat	k5eAaImIp3nS
Radio	radio	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
2	#num#	k4
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
tak	tak	k6eAd1
jako	jako	k8xS,k8xC
Radio	radio	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
největší	veliký	k2eAgInPc4d3
světové	světový	k2eAgInPc4d1
hity	hit	k1gInPc4
minulých	minulý	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
folk	folk	k1gInSc1
<g/>
,	,	kIx,
country	country	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
i	i	k9
na	na	k7c4
rock	rock	k1gInSc4
a	a	k8xC
soudobou	soudobý	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
posluchačů	posluchač	k1gMnPc2
původního	původní	k2eAgNnSc2d1
Radia	radio	k1gNnSc2
1	#num#	k4
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
)	)	kIx)
přešla	přejít	k5eAaPmAgFnS
právě	právě	k9
k	k	k7c3
Radiu	radio	k1gNnSc3
2	#num#	k4
a	a	k8xC
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejposlouchanější	poslouchaný	k2eAgFnSc1d3
stanice	stanice	k1gFnSc1
z	z	k7c2
portfolia	portfolio	k1gNnSc2
BBC	BBC	kA
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
3	#num#	k4
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
jako	jako	k8xS,k8xC
Third	Third	k1gMnSc1
Programme	Programme	k1gMnSc1
a	a	k8xC
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
získalo	získat	k5eAaPmAgNnS
současný	současný	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
zaměření	zaměření	k1gNnSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
identické	identický	k2eAgNnSc1d1
s	s	k7c7
naší	náš	k3xOp1gFnSc7
ČRo	ČRo	k1gFnSc7
Vltava	Vltava	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
vážná	vážný	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
jazz	jazz	k1gInSc1
a	a	k8xC
world	world	k1gMnSc1
music	music	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
4	#num#	k4
</s>
<s>
Začínalo	začínat	k5eAaImAgNnS
ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
jako	jako	k9
BBC	BBC	kA
Home	Home	k1gNnSc4
Service	Service	k1gFnSc2
a	a	k8xC
přejmenováno	přejmenovat	k5eAaPmNgNnS
v	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
na	na	k7c6
BBC	BBC	kA
Radio	radio	k1gNnSc1
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radio	radio	k1gNnSc1
4	#num#	k4
je	být	k5eAaImIp3nS
domov	domov	k1gInSc4
rozhlasových	rozhlasový	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
diskuzí	diskuze	k1gFnPc2
<g/>
,	,	kIx,
zpráv	zpráva	k1gFnPc2
a	a	k8xC
publicistiky	publicistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
jediná	jediný	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
nevysílá	vysílat	k5eNaImIp3nS
24	#num#	k4
hodin	hodina	k1gFnPc2
denně	denně	k6eAd1
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
hlasatele	hlasatel	k1gMnPc4
a	a	k8xC
vždy	vždy	k6eAd1
od	od	k7c2
1	#num#	k4
do	do	k7c2
5	#num#	k4
hodin	hodina	k1gFnPc2
ráno	ráno	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
jejích	její	k3xOp3gFnPc6
frekvencích	frekvence	k1gFnPc6
vysílá	vysílat	k5eAaImIp3nS
BBC	BBC	kA
World	World	k1gInSc1
Service	Service	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
4	#num#	k4
<g/>
Xtra	Xtrum	k1gNnSc2
</s>
<s>
Stanice	stanice	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
jako	jako	k8xC,k8xS
BBC	BBC	kA
Radio	radio	k1gNnSc1
7	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
5	#num#	k4
digitálních	digitální	k2eAgInPc2d1
stanicí	stanice	k1gFnSc7
BBC	BBC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
pořady	pořad	k1gInPc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
dramata	drama	k1gNnPc4
a	a	k8xC
další	další	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c4
BBC	BBC	kA
Radio	radio	k1gNnSc1
4	#num#	k4
Extra	extra	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
5	#num#	k4
Live	Live	k1gNnPc2
</s>
<s>
Stanice	stanice	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
jako	jako	k8xC,k8xS
BBC	BBC	kA
Radio	radio	k1gNnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílala	vysílat	k5eAaImAgFnS
hlavně	hlavně	k9
sportovní	sportovní	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
a	a	k8xC
pořady	pořad	k1gInPc4
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
se	se	k3xPyFc4
stanice	stanice	k1gFnSc1
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c4
BBC	BBC	kA
Radio	radio	k1gNnSc4
5	#num#	k4
live	liv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanice	stanice	k1gFnSc1
vysílá	vysílat	k5eAaImIp3nS
zpravodajství	zpravodajství	k1gNnSc4
24	#num#	k4
hodin	hodina	k1gFnPc2
denně	denně	k6eAd1
a	a	k8xC
sportovní	sportovní	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jejího	její	k3xOp3gNnSc2
nočního	noční	k2eAgNnSc2d1
vysílání	vysílání	k1gNnSc2
se	se	k3xPyFc4
přepojují	přepojovat	k5eAaImIp3nP
regionální	regionální	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
BBC	BBC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanice	stanice	k1gFnSc1
vysílá	vysílat	k5eAaImIp3nS
na	na	k7c6
AM	AM	kA
frekvencích	frekvence	k1gFnPc6
po	po	k7c6
Radiu	radio	k1gNnSc6
2	#num#	k4
a	a	k8xC
v	v	k7c6
DAB	DAB	kA
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
5	#num#	k4
Live	Live	k1gNnPc2
Sports	Sportsa	k1gFnPc2
Extra	extra	k2eAgFnPc2d1
</s>
<s>
Digitální	digitální	k2eAgFnSc1d1
sesterská	sesterský	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
BBC	BBC	kA
Radia	radio	k1gNnSc2
live	liv	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
sportovní	sportovní	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
např.	např.	kA
z	z	k7c2
kriketu	kriket	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
pomáhá	pomáhat	k5eAaImIp3nS
Radiu	radio	k1gNnSc3
5	#num#	k4
live	liv	k1gInPc4
při	při	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
přenosů	přenos	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
při	při	k7c6
OH	OH	kA
v	v	k7c4
Londyně	Londyně	k1gFnPc4
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílá	vysílat	k5eAaImIp3nS
na	na	k7c6
DAB	DAB	kA
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
6	#num#	k4
Music	Musice	k1gFnPc2
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
a	a	k8xC
vysílá	vysílat	k5eAaImIp3nS
indie	indie	k1gFnSc1
rock	rock	k1gInSc1
<g/>
,	,	kIx,
soul	soul	k1gInSc1
<g/>
,	,	kIx,
jazz	jazz	k1gInSc1
a	a	k8xC
vůbec	vůbec	k9
alternativní	alternativní	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byl	být	k5eAaImAgInS
plánován	plánovat	k5eAaImNgInS
zánik	zánik	k1gInSc1
stanice	stanice	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
nesouhlasu	nesouhlas	k1gInSc2
posluchačů	posluchač	k1gMnPc2
nakonec	nakonec	k6eAd1
nestal	stát	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Asian	Asian	k1gInSc1
Network	network	k1gInSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
pro	pro	k7c4
asijské	asijský	k2eAgNnSc4d1
publikum	publikum	k1gNnSc4
vysílající	vysílající	k2eAgNnSc4d1
v	v	k7c6
DAB	DAB	kA
a	a	k8xC
na	na	k7c6
FM	FM	kA
frekvencích	frekvence	k1gFnPc6
v	v	k7c6
regionech	region	k1gInPc6
s	s	k7c7
největším	veliký	k2eAgInSc7d3
počtem	počet	k1gInSc7
asijské	asijský	k2eAgFnSc2d1
menšiny	menšina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
Scotland	Scotlando	k1gNnPc2
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
pro	pro	k7c4
Skotsko	Skotsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
nan	nan	k?
Gà	Gà	k1gMnSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
pro	pro	k7c4
gaelsky	gaelsky	k6eAd1
mluvící	mluvící	k2eAgNnSc4d1
publikum	publikum	k1gNnSc4
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
Shetland	Shetlando	k1gNnPc2
</s>
<s>
Regionální	regionální	k2eAgMnSc1d1
opt-out	opt-out	k1gMnSc1
pro	pro	k7c4
Shetlandské	Shetlandský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc4
Orkney	Orknea	k1gFnSc2
</s>
<s>
Regionální	regionální	k2eAgMnSc1d1
opt-out	opt-out	k1gMnSc1
pro	pro	k7c4
Orknejské	orknejský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
Wales	Wales	k1gInSc1
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
pro	pro	k7c4
Wales	Wales	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
Cymru	Cymr	k1gInSc2
</s>
<s>
Stanice	stanice	k1gFnSc1
pro	pro	k7c4
velšsky	velšsky	k6eAd1
mluvící	mluvící	k2eAgNnSc4d1
publikum	publikum	k1gNnSc4
ve	v	k7c6
Walesu	Wales	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
Ulter	Ultero	k1gNnPc2
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
pro	pro	k7c4
Severní	severní	k2eAgNnSc4d1
Irsko	Irsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
Foyle	Foyle	k1gNnPc2
</s>
<s>
Regionální	regionální	k2eAgMnSc1d1
opt-out	opt-out	k1gMnSc1
pro	pro	k7c4
severozápad	severozápad	k1gInSc4
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
BBC	BBC	kA
Local	Local	k1gInSc1
Radio	radio	k1gNnSc1
</s>
<s>
BBC	BBC	kA
Provozuje	provozovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
lokálních	lokální	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Loga	logo	k1gNnPc1
BBC	BBC	kA
</s>
<s>
První	první	k4xOgNnSc1
logo	logo	k1gNnSc1
obsahující	obsahující	k2eAgFnSc4d1
3	#num#	k4
čtverce	čtverec	k1gInPc1
užívané	užívaný	k2eAgInPc1d1
v	v	k7c6
letech	let	k1gInPc6
1958	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
</s>
<s>
Druhé	druhý	k4xOgNnSc1
logo	logo	k1gNnSc1
obsahující	obsahující	k2eAgFnSc4d1
3	#num#	k4
čtverce	čtverec	k1gInPc1
užívané	užívaný	k2eAgInPc1d1
v	v	k7c6
letech	let	k1gInPc6
1963	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
</s>
<s>
Třetí	třetí	k4xOgNnSc1
logo	logo	k1gNnSc1
obsahující	obsahující	k2eAgFnSc4d1
3	#num#	k4
čtverce	čtverec	k1gInPc1
užívané	užívaný	k2eAgInPc1d1
v	v	k7c6
letech	let	k1gInPc6
1971	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc1
logo	logo	k1gNnSc1
obsahující	obsahující	k2eAgFnSc4d1
3	#num#	k4
čtverce	čtverec	k1gInPc1
užívané	užívaný	k2eAgInPc1d1
v	v	k7c6
letech	let	k1gInPc6
1988	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
</s>
<s>
Paté	Paté	k6eAd1
a	a	k8xC
současné	současný	k2eAgNnSc4d1
logo	logo	k1gNnSc4
BBC	BBC	kA
užívané	užívaný	k2eAgFnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
</s>
<s>
BBC	BBC	kA
Television	Television	k1gInSc1
</s>
<s>
Logo	logo	k1gNnSc1
BBC	BBC	kA
One	One	k1gMnPc2
</s>
<s>
Logo	logo	k1gNnSc1
BBC	BBC	kA
Two	Two	k1gMnSc2
</s>
<s>
Logo	logo	k1gNnSc1
BBC	BBC	kA
Three	Thre	k1gInSc2
</s>
<s>
Logo	logo	k1gNnSc1
BBC	BBC	kA
Four	Four	k1gInSc4
</s>
<s>
Logo	logo	k1gNnSc1
BBC	BBC	kA
News	Newsa	k1gFnPc2
</s>
<s>
Logo	logo	k1gNnSc1
BBC	BBC	kA
Japan	japan	k1gInSc1
</s>
<s>
Logo	logo	k1gNnSc1
BBC	BBC	kA
Entertainment	Entertainment	k1gMnSc1
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
</s>
<s>
Logo	logo	k1gNnSc4
BBC	BBC	kA
Radio	radio	k1gNnSc4
1	#num#	k4
</s>
<s>
Logo	logo	k1gNnSc4
BBC	BBC	kA
Radio	radio	k1gNnSc4
2	#num#	k4
</s>
<s>
Logo	logo	k1gNnSc4
BBC	BBC	kA
Radio	radio	k1gNnSc4
3	#num#	k4
</s>
<s>
Logo	logo	k1gNnSc4
BBC	BBC	kA
Radio	radio	k1gNnSc4
5	#num#	k4
Live	Live	k1gNnPc2
Sports	Sportsa	k1gFnPc2
Extra	extra	k2eAgFnPc2d1
</s>
<s>
Logo	logo	k1gNnSc4
BBC	BBC	kA
Radio	radio	k1gNnSc4
6	#num#	k4
Music	Musice	k1gInPc2
</s>
<s>
Logo	logo	k1gNnSc1
BBC	BBC	kA
World	Worlda	k1gFnPc2
Service	Service	k1gFnSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Televizní	televizní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
BBC	BBC	kA
</s>
<s>
Ředitel	ředitel	k1gMnSc1
BBC	BBC	kA
Mark	Mark	k1gMnSc1
Thompson	Thompson	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.guardian.co.uk/media/2004/jan/29/bbc.davidkelly1	http://www.guardian.co.uk/media/2004/jan/29/bbc.davidkelly1	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
BBC	BBC	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Domovská	domovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
BBC	BBC	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
BBC	BBC	kA
Czech	Czech	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
British	British	k1gInSc1
Broadcasting	Broadcasting	k1gInSc1
Corporation	Corporation	k1gInSc1
Kanály	kanál	k1gInPc1
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
</s>
<s>
SD	SD	kA
</s>
<s>
BBC	BBC	kA
One	One	k1gFnSc1
•	•	k?
BBC	BBC	kA
Two	Two	k1gMnSc3
•	•	k?
BBC	BBC	kA
Three	Thre	k1gFnSc2
•	•	k?
BBC	BBC	kA
Four	Four	k1gInSc1
•	•	k?
BBC	BBC	kA
News	News	k1gInSc1
•	•	k?
BBC	BBC	kA
Parliament	Parliament	k1gInSc1
•	•	k?
BBC	BBC	kA
Alba	alba	k1gFnSc1
•	•	k?
CBBC	CBBC	kA
•	•	k?
CBeebies	CBeebies	k1gInSc1
HD	HD	kA
</s>
<s>
BBC	BBC	kA
One	One	k1gMnSc1
HD	HD	kA
•	•	k?
BBC	BBC	kA
Two	Two	k1gFnSc2
HD	HD	kA
</s>
<s>
Kanály	kanál	k1gInPc1
BBC	BBC	kA
Worldwide	Worldwid	k1gInSc5
a	a	k8xC
joint	joint	k1gInSc4
venture	ventur	k1gMnSc5
</s>
<s>
SD	SD	kA
</s>
<s>
BBC	BBC	kA
Alba	alba	k1gFnSc1
•	•	k?
BBC	BBC	kA
America	America	k1gMnSc1
•	•	k?
BBC	BBC	kA
Arabic	Arabic	k1gMnSc1
Television	Television	k1gInSc1
•	•	k?
BBC	BBC	kA
Canada	Canada	k1gFnSc1
•	•	k?
BBC	BBC	kA
Entertainment	Entertainment	k1gInSc1
•	•	k?
BBC	BBC	kA
Kids	Kids	k1gInSc1
•	•	k?
BBC	BBC	kA
World	World	k1gInSc1
News	News	k1gInSc1
•	•	k?
CBeebies	CBeebies	k1gInSc1
•	•	k?
BBC	BBC	kA
HD	HD	kA
•	•	k?
BBC	BBC	kA
Knowledge	Knowledg	k1gFnSc2
•	•	k?
BBC	BBC	kA
Lifestyle	Lifestyl	k1gInSc5
•	•	k?
BBC	BBC	kA
Persian	Persian	k1gInSc1
Television	Television	k1gInSc1
•	•	k?
BBC	BBC	kA
UKTV	UKTV	kA
<g/>
(	(	kIx(
<g/>
Austrálie	Austrálie	k1gFnSc1
a	a	k8xC
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
BBC	BBC	kA
World	World	k1gInSc1
News	News	k1gInSc1
•	•	k?
CBeebies	CBeebies	k1gInSc1
•	•	k?
Community	Communita	k1gFnSc2
Channel	Channel	k1gMnSc1
•	•	k?
UKTV	UKTV	kA
<g/>
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
<g/>
)	)	kIx)
HD	HD	kA
</s>
<s>
BBC	BBC	kA
America	America	k1gMnSc1
HD	HD	kA
•	•	k?
BBC	BBC	kA
HD	HD	kA
<g/>
(	(	kIx(
<g/>
satelitní	satelitní	k2eAgInSc1d1
kanál	kanál	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
BBC	BBC	kA
UKTV	UKTV	kA
HD	HD	kA
</s>
<s>
Nevysílající	vysílající	k2eNgInPc1d1
kanály	kanál	k1gInPc1
</s>
<s>
BBC	BBC	kA
Choice	Choice	k1gFnSc1
•	•	k?
BBC	BBC	kA
Knowledge	Knowledg	k1gFnSc2
•	•	k?
BBC	BBC	kA
HD	HD	kA
<g/>
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
BBC	BBC	kA
2W	2W	k4
•	•	k?
BBC	BBC	kA
Select	Select	k1gInSc1
•	•	k?
BBC	BBC	kA
World	World	k1gMnSc1
Service	Service	k1gFnSc2
Television	Television	k1gInSc1
•	•	k?
BBC	BBC	kA
TV	TV	kA
Europe	Europ	k1gInSc5
•	•	k?
BBC	BBC	kA
Prime	prim	k1gInSc5
•	•	k?
BBC	BBC	kA
Food	Food	k1gInSc1
•	•	k?
BBC	BBC	kA
Japan	japan	k1gInSc4
Analogové	analogový	k2eAgInPc4d1
rozhlasové	rozhlasový	k2eAgInPc4d1
kanály	kanál	k1gInPc4
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
1	#num#	k4
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
2	#num#	k4
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
3	#num#	k4
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
4	#num#	k4
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
5	#num#	k4
live	live	k6eAd1
Digitální	digitální	k2eAgInPc1d1
rozhlasové	rozhlasový	k2eAgInPc1d1
kanály	kanál	k1gInPc1
</s>
<s>
BBC	BBC	kA
1	#num#	k4
<g/>
Xtra	Xtr	k1gInSc2
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc4
5	#num#	k4
Live	Liv	k1gFnSc2
Sports	Sportsa	k1gFnPc2
Extra	extra	k2eAgFnSc2d1
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
4	#num#	k4
Extra	extra	k2eAgMnSc1d1
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc4
6	#num#	k4
Music	Musice	k1gInPc2
•	•	k?
BBC	BBC	kA
Asian	Asian	k1gInSc1
Network	network	k1gInSc1
Regionální	regionální	k2eAgInPc1d1
rozhlasové	rozhlasový	k2eAgInPc1d1
kanály	kanál	k1gInPc1
</s>
<s>
BBC	BBC	kA
Radio	radio	k1gNnSc1
Scotland	Scotland	k1gInSc1
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
nan	nan	k?
Gà	Gà	k1gMnSc1
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
Wales	Wales	k1gInSc1
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
Cymru	Cymr	k1gInSc2
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc1
Ulster	Ulster	k1gInSc1
•	•	k?
BBC	BBC	kA
Radio	radio	k1gNnSc4
Foyle	Foyle	k1gNnSc2
Vysílání	vysílání	k1gNnSc2
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
</s>
<s>
BBC	BBC	kA
World	World	k1gMnSc1
Service	Service	k1gFnSc2
•	•	k?
BBC	BBC	kA
Arabic	Arabic	k1gMnSc1
•	•	k?
BBC	BBC	kA
Bangla	Bangla	k1gMnSc1
•	•	k?
BBC	BBC	kA
Nepali	Nepali	k1gMnSc3
•	•	k?
BBC	BBC	kA
Brasil	Brasil	k1gMnSc3
•	•	k?
BBC	BBC	kA
Mundo	Mundo	k1gNnSc1
•	•	k?
BBC	BBC	kA
Persian	Persian	k1gInSc1
•	•	k?
BBC	BBC	kA
Russian	Russian	k1gInSc1
Service	Service	k1gFnSc1
•	•	k?
BBC	BBC	kA
Ukrainian	Ukrainian	k1gMnSc1
</s>
<s>
Členové	člen	k1gMnPc1
Evropské	evropský	k2eAgFnSc2d1
vysílací	vysílací	k2eAgFnSc2d1
unie	unie	k1gFnSc2
(	(	kIx(
<g/>
EBU	EBU	kA
<g/>
)	)	kIx)
Plnoprávní	plnoprávní	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
ARD	ARD	kA
•	•	k?
ARMR	ARMR	kA
•	•	k?
ARMTV	ARMTV	kA
•	•	k?
BBC	BBC	kA
•	•	k?
BHRT	BHRT	kA
•	•	k?
BNR	BNR	kA
•	•	k?
BNT	BNT	kA
•	•	k?
BTRC	BTRC	kA
•	•	k?
C1R	C1R	k1gMnSc1
•	•	k?
Canal	Canal	k1gMnSc1
<g/>
+	+	kIx~
•	•	k?
CLT	CLT	kA
<g/>
/	/	kIx~
<g/>
RTL	RTL	kA
•	•	k?
COPE	cop	k1gInSc5
•	•	k?
ČRo	ČRo	k1gMnSc3
•	•	k?
ČT	ČT	kA
•	•	k?
CyBC	CyBC	k1gFnSc2
•	•	k?
DR	dr	kA
•	•	k?
Duna	duna	k1gFnSc1
TV	TV	kA
•	•	k?
E1	E1	k1gFnSc2
•	•	k?
ERR	ERR	kA
•	•	k?
ERT	ERT	kA
•	•	k?
ERSL	ERSL	kA
•	•	k?
France	Franc	k1gMnSc4
24	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
FTV	FTV	kA
•	•	k?
GPB	GPB	kA
•	•	k?
HRT	HRT	kA
•	•	k?
İ	İ	k?
•	•	k?
LR	LR	kA
•	•	k?
LRT	LRT	kA
•	•	k?
LTV	LTV	kA
•	•	k?
MCD	MCD	kA
•	•	k?
MR	MR	kA
•	•	k?
MRT	MRT	kA
•	•	k?
MTVA	MTVA	kA
•	•	k?
MTV3	MTV3	k1gFnSc2
•	•	k?
NERIT	NERIT	kA
•	•	k?
NPO	NPO	kA
•	•	k?
NRK	NRK	kA
•	•	k?
NRU	NRU	kA
•	•	k?
NTU	NTU	kA
•	•	k?
ORF	ORF	kA
•	•	k?
PBS	PBS	kA
•	•	k?
PR	pr	k0
•	•	k?
Radio	radio	k1gNnSc1
France	Franc	k1gMnSc2
•	•	k?
RAI	RAI	kA
•	•	k?
RDO	RDO	kA
•	•	k?
RFI	RFI	kA
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
RMC	RMC	kA
•	•	k?
ROR	ROR	kA
•	•	k?
RTBF	RTBF	kA
•	•	k?
RTCG	RTCG	kA
•	•	k?
RTÉ	RTÉ	kA
•	•	k?
RTP	RTP	kA
•	•	k?
RTS	RTS	kA
•	•	k?
RTSH	RTSH	kA
•	•	k?
RTVA	RTVA	kA
•	•	k?
RTVE	RTVE	kA
•	•	k?
RTVS	RTVS	kA
•	•	k?
RTVSLO	RTVSLO	kA
•	•	k?
RÚV	RÚV	kA
•	•	k?
SER	srát	k5eAaImRp2nS
•	•	k?
SMTV	SMTV	kA
•	•	k?
SR	SR	kA
•	•	k?
SRG	SRG	kA
SSR	SSR	kA
•	•	k?
SVT	SVT	kA
•	•	k?
TF1	TF1	k1gMnSc1
•	•	k?
TG4	TG4	k1gMnSc1
•	•	k?
TMC	TMC	kA
•	•	k?
TRM	TRM	kA
•	•	k?
TRT	trt	k1gInSc1
•	•	k?
TV2	TV2	k1gFnSc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
•	•	k?
TV2	TV2	k1gFnSc1
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
•	•	k?
TV4	TV4	k1gFnSc2
•	•	k?
TVP	TVP	kA
•	•	k?
TVR	TVR	kA
•	•	k?
UKIB	UKIB	kA
•	•	k?
UR	Ur	k1gInSc1
•	•	k?
VGTRK	VGTRK	kA
•	•	k?
VR	vr	k0
•	•	k?
VRT	vrt	k1gInSc1
•	•	k?
Yle	Yle	k1gFnSc2
•	•	k?
ZDF	ZDF	kA
Mimoevropští	mimoevropský	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
ENRS	ENRS	kA
•	•	k?
ENTV	ENTV	kA
•	•	k?
ERTT	ERTT	kA
•	•	k?
ERTU	ERTU	kA
•	•	k?
IBA	iba	k6eAd1
•	•	k?
JRTV	JRTV	kA
•	•	k?
LNC	LNC	kA
•	•	k?
SNRT	SNRT	kA
•	•	k?
TDA	TDA	kA
•	•	k?
TL	TL	kA
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
JRT	JRT	kA
•	•	k?
LJBC	LJBC	kA
•	•	k?
SRo	SRo	k1gFnSc2
•	•	k?
STV	STV	kA
•	•	k?
TMC	TMC	kA
•	•	k?
UJRT	UJRT	kA
</s>
<s>
Žádající	žádající	k2eAgNnSc1d1
o	o	k7c4
členství	členství	k1gNnSc4
</s>
<s>
1FLTV	1FLTV	k4
•	•	k?
2M	2M	k4
TV	TV	kA
•	•	k?
K1	K1	k1gFnSc2
•	•	k?
QR	QR	kA
•	•	k?
RTK	RTK	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2004238888	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1002250-8	1002250-8	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2375	#num#	k4
150X	150X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79074359	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
127458402	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79074359	#num#	k4
</s>
