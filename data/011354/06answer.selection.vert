<s>
Nová	nový	k2eAgFnSc1d1	nová
strana	strana	k1gFnSc1	strana
ideologicky	ideologicky	k6eAd1	ideologicky
spojovala	spojovat	k5eAaImAgFnS	spojovat
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
aktivně	aktivně	k6eAd1	aktivně
usilovaly	usilovat	k5eAaImAgFnP	usilovat
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
česko-slovenský	českolovenský	k2eAgInSc4d1	česko-slovenský
stát	stát	k1gInSc4	stát
nezávislý	závislý	k2eNgInSc4d1	nezávislý
na	na	k7c4	na
Rakousku-Uhersku	Rakousku-Uherska	k1gFnSc4	Rakousku-Uherska
<g/>
.	.	kIx.	.
</s>
