<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
státoprávní	státoprávní	k2eAgFnSc1d1	státoprávní
demokracie	demokracie	k1gFnSc1	demokracie
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
ČStD	ČStD	k1gMnSc2	ČStD
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
sloučením	sloučení	k1gNnSc7	sloučení
4	[number]	k4	4
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
stran	strana	k1gFnPc2	strana
<g/>
:	:	kIx,	:
Mladočechů	mladočech	k1gMnPc2	mladočech
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgFnPc1d1	lidová
strany	strana	k1gFnPc1	strana
pokrokové	pokrokový	k2eAgFnPc1d1	pokroková
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Státoprávně	státoprávně	k6eAd1	státoprávně
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
Strany	strana	k1gFnSc2	strana
státoprávně	státoprávně	k6eAd1	státoprávně
radikální	radikální	k2eAgFnSc1d1	radikální
a	a	k8xC	a
radikálně	radikálně	k6eAd1	radikálně
pokrokové	pokrokový	k2eAgNnSc1d1	pokrokové
<g/>
)	)	kIx)	)
a	a	k8xC	a
části	část	k1gFnSc2	část
Pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
realisté	realista	k1gMnPc1	realista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sekretariát	sekretariát	k1gInSc1	sekretariát
strany	strana	k1gFnSc2	strana
sídlil	sídlit	k5eAaImAgInS	sídlit
na	na	k7c6	na
Praze	Praha	k1gFnSc6	Praha
2	[number]	k4	2
v	v	k7c6	v
Mariánské	mariánský	k2eAgFnSc6d1	Mariánská
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
historický	historický	k2eAgInSc1d1	historický
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
válečného	válečný	k2eAgInSc2d1	válečný
chaosu	chaos	k1gInSc2	chaos
se	se	k3xPyFc4	se
Státoprávní	státoprávní	k2eAgFnSc1d1	státoprávní
demokracie	demokracie	k1gFnSc1	demokracie
stala	stát	k5eAaPmAgFnS	stát
téměř	téměř	k6eAd1	téměř
nadstranickým	nadstranický	k2eAgMnSc7d1	nadstranický
všenárodním	všenárodní	k2eAgMnSc7d1	všenárodní
reprezentantem	reprezentant	k1gMnSc7	reprezentant
–	–	k?	–
tomu	ten	k3xDgNnSc3	ten
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
i	i	k9	i
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgMnPc4d1	hlavní
představitele	představitel	k1gMnPc4	představitel
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
patřili	patřit	k5eAaImAgMnP	patřit
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
Šámal	Šámal	k1gMnSc1	Šámal
(	(	kIx(	(
<g/>
šéf	šéf	k1gMnSc1	šéf
Maffie	Maffie	k1gFnSc2	Maffie
<g/>
)	)	kIx)	)
či	či	k8xC	či
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Preiss	Preissa	k1gFnPc2	Preissa
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
šéf	šéf	k1gMnSc1	šéf
Živnobanky	Živnobanka	k1gFnSc2	Živnobanka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
státoprávní	státoprávní	k2eAgFnSc1d1	státoprávní
demokracie	demokracie	k1gFnSc1	demokracie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
stran	strana	k1gFnPc2	strana
Mladočechů	mladočech	k1gMnPc2	mladočech
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgFnPc1d1	lidová
strany	strana	k1gFnPc1	strana
pokrokové	pokrokový	k2eAgFnPc1d1	pokroková
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
Státoprávně	státoprávně	k6eAd1	státoprávně
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
Pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
jednání	jednání	k1gNnPc1	jednání
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
vedl	vést	k5eAaImAgMnS	vést
JUDr.	JUDr.	kA	JUDr.
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
Státoprávně	státoprávně	k6eAd1	státoprávně
pokrokové	pokrokový	k2eAgFnSc2d1	pokroková
strany	strana	k1gFnSc2	strana
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
ujednal	ujednat	k5eAaPmAgMnS	ujednat
o	o	k7c6	o
přejmenování	přejmenování	k1gNnSc6	přejmenování
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
případě	případ	k1gInSc6	případ
přistoupení	přistoupení	k1gNnSc2	přistoupení
dalších	další	k2eAgFnPc2d1	další
jednaných	jednaný	k2eAgFnPc2d1	jednaný
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
strana	strana	k1gFnSc1	strana
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
ustavena	ustavit	k5eAaPmNgFnS	ustavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
strana	strana	k1gFnSc1	strana
ideologicky	ideologicky	k6eAd1	ideologicky
spojovala	spojovat	k5eAaImAgFnS	spojovat
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
aktivně	aktivně	k6eAd1	aktivně
usilovaly	usilovat	k5eAaImAgFnP	usilovat
o	o	k7c4	o
samostatný	samostatný	k2eAgInSc4d1	samostatný
česko-slovenský	českolovenský	k2eAgInSc4d1	česko-slovenský
stát	stát	k1gInSc4	stát
nezávislý	závislý	k2eNgInSc4d1	nezávislý
na	na	k7c4	na
Rakousku-Uhersku	Rakousku-Uherska	k1gFnSc4	Rakousku-Uherska
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
akt	akt	k1gInSc4	akt
podnítila	podnítit	k5eAaPmAgFnS	podnítit
vrcholící	vrcholící	k2eAgFnSc1d1	vrcholící
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
průběh	průběh	k1gInSc1	průběh
dával	dávat	k5eAaImAgInS	dávat
naději	naděje	k1gFnSc4	naděje
nacionálním	nacionální	k2eAgNnSc7d1	nacionální
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
ČStD	ČStD	k?	ČStD
se	se	k3xPyFc4	se
propagovala	propagovat	k5eAaImAgFnS	propagovat
jako	jako	k9	jako
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
politickými	politický	k2eAgInPc7d1	politický
celky	celek	k1gInPc7	celek
hlásala	hlásat	k5eAaImAgFnS	hlásat
cíl	cíl	k1gInSc4	cíl
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
chtěla	chtít	k5eAaImAgFnS	chtít
budovat	budovat	k5eAaImF	budovat
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
právní	právní	k2eAgInSc4d1	právní
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
programu	program	k1gInSc6	program
zavazovala	zavazovat	k5eAaImAgFnS	zavazovat
k	k	k7c3	k
rovnému	rovný	k2eAgInSc3d1	rovný
přístupu	přístup	k1gInSc3	přístup
ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
třídám	třída	k1gFnPc3	třída
a	a	k8xC	a
menšinám	menšina	k1gFnPc3	menšina
<g/>
.	.	kIx.	.
<g/>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
skupina	skupina	k1gFnSc1	skupina
voličů	volič	k1gMnPc2	volič
a	a	k8xC	a
zastánců	zastánce	k1gMnPc2	zastánce
strany	strana	k1gFnSc2	strana
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
městská	městský	k2eAgFnSc1d1	městská
buržoazie	buržoazie	k1gFnSc1	buržoazie
<g/>
.	.	kIx.	.
<g/>
Prvním	první	k4xOgMnSc7	první
a	a	k8xC	a
jediným	jediný	k2eAgMnSc7d1	jediný
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
JUDr.	JUDr.	kA	JUDr.
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
.	.	kIx.	.
</s>
<s>
Místopředsedy	místopředseda	k1gMnPc4	místopředseda
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
bývalí	bývalý	k2eAgMnPc1d1	bývalý
předsedové	předseda	k1gMnPc1	předseda
sloučených	sloučený	k2eAgFnPc2d1	sloučená
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
JUDr.	JUDr.	kA	JUDr.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Budínský	Budínský	k1gMnSc1	Budínský
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
Antonín	Antonín	k1gMnSc1	Antonín
Hajn	Hajn	k1gMnSc1	Hajn
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
Přemysl	Přemysl	k1gMnSc1	Přemysl
Šámal	Šámal	k1gMnSc1	Šámal
a	a	k8xC	a
JUDr.	JUDr.	kA	JUDr.
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
.	.	kIx.	.
</s>
<s>
Úzké	úzký	k2eAgNnSc4d1	úzké
vedení	vedení	k1gNnSc4	vedení
dále	daleko	k6eAd2	daleko
tvořili	tvořit	k5eAaImAgMnP	tvořit
například	například	k6eAd1	například
Adolf	Adolf	k1gMnSc1	Adolf
Stránský	Stránský	k1gMnSc1	Stránský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Matoušek	Matoušek	k1gMnSc1	Matoušek
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Sís	Sís	k1gMnSc1	Sís
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Stránský	Stránský	k1gMnSc1	Stránský
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Kalina	Kalina	k1gMnSc1	Kalina
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
PhDr.	PhDr.	kA	PhDr.
Jan	Jan	k1gMnSc1	Jan
Herben	Herben	k2eAgMnSc1d1	Herben
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
František	František	k1gMnSc1	František
Mareš	Mareš	k1gMnSc1	Mareš
nebo	nebo	k8xC	nebo
prof.	prof.	kA	prof.
Bohumil	Bohumil	k1gMnSc1	Bohumil
Němec	Němec	k1gMnSc1	Němec
<g/>
.	.	kIx.	.
<g/>
František	František	k1gMnSc1	František
Sís	Sís	k1gMnSc1	Sís
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
stranických	stranický	k2eAgFnPc2d1	stranická
novin	novina	k1gFnPc2	novina
Národní	národní	k2eAgFnSc2d1	národní
listy	lista	k1gFnSc2	lista
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgInPc2	který
psal	psát	k5eAaImAgMnS	psát
i	i	k9	i
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Herben	Herbna	k1gFnPc2	Herbna
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
samostatného	samostatný	k2eAgInSc2d1	samostatný
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
ustaveno	ustaven	k2eAgNnSc1d1	ustaveno
Revoluční	revoluční	k2eAgNnSc1d1	revoluční
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
nahrazovala	nahrazovat	k5eAaImAgFnS	nahrazovat
parlament	parlament	k1gInSc4	parlament
a	a	k8xC	a
zastoupení	zastoupení	k1gNnSc1	zastoupení
stran	strana	k1gFnPc2	strana
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
dle	dle	k7c2	dle
tzv.	tzv.	kA	tzv.
Švehlova	Švehlův	k2eAgInSc2d1	Švehlův
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dle	dle	k7c2	dle
výsledku	výsledek	k1gInSc2	výsledek
posledních	poslední	k2eAgFnPc2d1	poslední
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
rozložení	rozložení	k1gNnSc3	rozložení
získala	získat	k5eAaPmAgFnS	získat
ČStD	ČStD	k1gFnSc1	ČStD
celkem	celkem	k6eAd1	celkem
46	[number]	k4	46
křesel	křeslo	k1gNnPc2	křeslo
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
nemělo	mít	k5eNaImAgNnS	mít
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
všechny	všechen	k3xTgFnPc1	všechen
strany	strana	k1gFnPc1	strana
v	v	k7c6	v
národním	národní	k2eAgMnSc6d1	národní
duchu	duch	k1gMnSc6	duch
vzájemně	vzájemně	k6eAd1	vzájemně
spolupracovaly	spolupracovat	k5eAaImAgInP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
Všenárodní	všenárodní	k2eAgFnSc2d1	všenárodní
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
ČStD	ČStD	k?	ČStD
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
ještě	ještě	k9	ještě
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
Aloise	Alois	k1gMnSc4	Alois
Rašína	Rašín	k1gMnSc4	Rašín
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
<g/>
)	)	kIx)	)
a	a	k8xC	a
Adolfa	Adolf	k1gMnSc2	Adolf
Stránského	Stránský	k1gMnSc2	Stránský
(	(	kIx(	(
<g/>
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
podala	podat	k5eAaPmAgFnS	podat
demisi	demise	k1gFnSc4	demise
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
strana	strana	k1gFnSc1	strana
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Československou	československý	k2eAgFnSc4d1	Československá
národní	národní	k2eAgFnSc4d1	národní
demokracii	demokracie	k1gFnSc4	demokracie
(	(	kIx(	(
<g/>
ČsND	ČsND	k1gFnSc4	ČsND
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
státoprávní	státoprávní	k2eAgInSc1d1	státoprávní
požadavek	požadavek	k1gInSc1	požadavek
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
naplněn	naplnit	k5eAaPmNgInS	naplnit
vznikem	vznik	k1gInSc7	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
strany	strana	k1gFnSc2	strana
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
i	i	k9	i
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
ČStD	ČStD	k1gFnPc2	ČStD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
