<s>
Německo	Německo	k1gNnSc1	Německo
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
používalo	používat	k5eAaImAgNnS	používat
k	k	k7c3	k
utajování	utajování	k1gNnSc3	utajování
zpráv	zpráva	k1gFnPc2	zpráva
mechanický	mechanický	k2eAgInSc1d1	mechanický
stroj	stroj	k1gInSc1	stroj
Enigma	enigma	k1gFnSc1	enigma
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prováděl	provádět	k5eAaImAgInS	provádět
poměrně	poměrně	k6eAd1	poměrně
složité	složitý	k2eAgFnPc4d1	složitá
operace	operace	k1gFnPc4	operace
se	s	k7c7	s
vstupním	vstupní	k2eAgInSc7d1	vstupní
textem	text	k1gInSc7	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
ovládat	ovládat	k5eAaImF	ovládat
<g/>
.	.	kIx.	.
</s>
