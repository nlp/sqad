<s>
Marianský	Marianský	k2eAgInSc1d1	Marianský
příkop	příkop	k1gInSc1	příkop
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
obvykle	obvykle	k6eAd1	obvykle
psán	psán	k2eAgInSc1d1	psán
Mariánský	mariánský	k2eAgInSc1d1	mariánský
příkop	příkop	k1gInSc1	příkop
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2550	[number]	k4	2550
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
průměrně	průměrně	k6eAd1	průměrně
69	[number]	k4	69
km	km	kA	km
široké	široký	k2eAgNnSc4d1	široké
podmořské	podmořský	k2eAgNnSc4d1	podmořské
údolí	údolí	k1gNnSc4	údolí
tektonického	tektonický	k2eAgInSc2d1	tektonický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
východně	východně	k6eAd1	východně
od	od	k7c2	od
souostroví	souostroví	k1gNnSc2	souostroví
Mariany	Mariana	k1gFnSc2	Mariana
(	(	kIx(	(
<g/>
Mariánské	mariánský	k2eAgInPc1d1	mariánský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
ostrova	ostrov	k1gInSc2	ostrov
Guam	Guama	k1gFnPc2	Guama
<g/>
.	.	kIx.	.
</s>
