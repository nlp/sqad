<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
Isaacu	Isaac	k1gMnSc3	Isaac
Newtonovi	Newton	k1gMnSc3	Newton
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
paprsek	paprsek	k1gInSc1	paprsek
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
hranolem	hranol	k1gInSc7	hranol
nebo	nebo	k8xC	nebo
dešťovou	dešťový	k2eAgFnSc7d1	dešťová
kapkou	kapka	k1gFnSc7	kapka
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
