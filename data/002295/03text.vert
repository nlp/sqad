<s>
Sofie	Sofie	k1gFnSc1	Sofie
(	(	kIx(	(
<g/>
bulharsky	bulharsky	k6eAd1	bulharsky
Cо	Cо	k1gMnSc2	Cо
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
;	;	kIx,	;
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
pohoří	pohoří	k1gNnSc2	pohoří
Vitoša	Vitošus	k1gMnSc2	Vitošus
na	na	k7c6	na
západě	západ	k1gInSc6	západ
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
srbských	srbský	k2eAgFnPc2d1	Srbská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Východním	východní	k2eAgInSc7d1	východní
okrajem	okraj	k1gInSc7	okraj
města	město	k1gNnSc2	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Iskăr	Iskăra	k1gFnPc2	Iskăra
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
14	[number]	k4	14
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
politickým	politický	k2eAgNnSc7d1	politické
<g/>
,	,	kIx,	,
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
i	i	k8xC	i
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sofie	Sofie	k1gFnSc1	Sofie
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
univerzitním	univerzitní	k2eAgNnSc7d1	univerzitní
městem	město	k1gNnSc7	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
9	[number]	k4	9
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Sofia	Sofia	k1gFnSc1	Sofia
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dávnou	dávný	k2eAgFnSc4d1	dávná
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
bylo	být	k5eAaImAgNnS	být
místo	místo	k1gNnSc1	místo
osídleno	osídlit	k5eAaPmNgNnS	osídlit
už	už	k6eAd1	už
před	před	k7c7	před
osmi	osm	k4xCc7	osm
tisíci	tisíc	k4xCgInPc7	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sídlo	sídlo	k1gNnSc1	sídlo
bojovných	bojovný	k2eAgInPc2d1	bojovný
thráckých	thrácký	k2eAgInPc2d1	thrácký
Serdů	Serd	k1gInPc2	Serd
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Serdica	Serdic	k1gInSc2	Serdic
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
339	[number]	k4	339
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	on	k3xPp3gFnPc4	on
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Makedonský	makedonský	k2eAgInSc4d1	makedonský
a	a	k8xC	a
roku	rok	k1gInSc2	rok
29	[number]	k4	29
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
založili	založit	k5eAaPmAgMnP	založit
Římané	Říman	k1gMnPc1	Říman
město	město	k1gNnSc4	město
Ulpia	Ulpium	k1gNnSc2	Ulpium
Serdica	Serdicum	k1gNnSc2	Serdicum
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
Dácie	Dácie	k1gFnSc2	Dácie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Trajána	Traján	k1gMnSc2	Traján
(	(	kIx(	(
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
117	[number]	k4	117
<g/>
)	)	kIx)	)
město	město	k1gNnSc1	město
rozkvetlo	rozkvetnout	k5eAaPmAgNnS	rozkvetnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
mincovna	mincovna	k1gFnSc1	mincovna
<g/>
,	,	kIx,	,
fórum	fórum	k1gNnSc1	fórum
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
ohrazeno	ohradit	k5eAaPmNgNnS	ohradit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hradbou	hradba	k1gFnSc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
342	[number]	k4	342
sem	sem	k6eAd1	sem
svolali	svolat	k5eAaPmAgMnP	svolat
oba	dva	k4xCgMnPc1	dva
císařové	císař	k1gMnPc1	císař
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
neskončil	skončit	k5eNaPmAgMnS	skončit
úspěchem	úspěch	k1gInSc7	úspěch
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
jako	jako	k8xS	jako
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
447	[number]	k4	447
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
Serdiku	Serdikum	k1gNnSc3	Serdikum
Hunové	Hun	k1gMnPc1	Hun
pod	pod	k7c7	pod
Attilou	Attila	k1gMnSc7	Attila
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
i	i	k9	i
Gótové	Gót	k1gMnPc1	Gót
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
a	a	k8xC	a
opevněno	opevnit	k5eAaPmNgNnS	opevnit
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
532-537	[number]	k4	532-537
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
kostel	kostel	k1gInSc1	kostel
Sv.	sv.	kA	sv.
Sofie	Sofia	k1gFnSc2	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
dobyly	dobýt	k5eAaPmAgFnP	dobýt
Serdiku	Serdika	k1gFnSc4	Serdika
slovanské	slovanský	k2eAgInPc1d1	slovanský
kmeny	kmen	k1gInPc1	kmen
a	a	k8xC	a
název	název	k1gInSc1	název
Serdika	Serdikum	k1gNnSc2	Serdikum
upadl	upadnout	k5eAaPmAgInS	upadnout
v	v	k7c6	v
zapomenutí	zapomenutí	k1gNnSc6	zapomenutí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
809	[number]	k4	809
ji	on	k3xPp3gFnSc4	on
dobyl	dobýt	k5eAaPmAgInS	dobýt
bulharský	bulharský	k2eAgMnSc1d1	bulharský
chán	chán	k1gMnSc1	chán
Krum	Krum	k1gMnSc1	Krum
a	a	k8xC	a
město	město	k1gNnSc1	město
dostalo	dostat	k5eAaPmAgNnS	dostat
slovanský	slovanský	k2eAgInSc4d1	slovanský
název	název	k1gInSc4	název
Sreděc	Sreděc	k1gFnSc1	Sreděc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
byzantské	byzantský	k2eAgFnSc2d1	byzantská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Triadica	Triadic	k1gInSc2	Triadic
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
bulharské	bulharský	k2eAgFnSc2d1	bulharská
říše	říš	k1gFnSc2	říš
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
opevněno	opevněn	k2eAgNnSc1d1	opevněno
a	a	k8xC	a
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
dostalo	dostat	k5eAaPmAgNnS	dostat
podle	podle	k7c2	podle
kostela	kostel	k1gInSc2	kostel
název	název	k1gInSc1	název
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1386	[number]	k4	1386
dobyli	dobýt	k5eAaPmAgMnP	dobýt
město	město	k1gNnSc4	město
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
obléhání	obléhání	k1gNnSc6	obléhání
osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
a	a	k8xC	a
drželi	držet	k5eAaImAgMnP	držet
je	on	k3xPp3gMnPc4	on
po	po	k7c6	po
pět	pět	k4xCc4	pět
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
sídlem	sídlo	k1gNnSc7	sídlo
rumelského	rumelský	k2eAgMnSc2d1	rumelský
beje	bej	k1gMnSc2	bej
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
mešit	mešita	k1gFnPc2	mešita
a	a	k8xC	a
minaretů	minaret	k1gInPc2	minaret
<g/>
,	,	kIx,	,
vcelku	vcelku	k6eAd1	vcelku
ale	ale	k8xC	ale
chátralo	chátrat	k5eAaImAgNnS	chátrat
a	a	k8xC	a
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgMnSc1d1	ruský
generál	generál	k1gMnSc1	generál
Gurko	Gurko	k1gNnSc4	Gurko
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
dobyl	dobýt	k5eAaPmAgInS	dobýt
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
asi	asi	k9	asi
18	[number]	k4	18
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
chatrných	chatrný	k2eAgInPc6d1	chatrný
domech	dům	k1gInPc6	dům
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
ústavodárné	ústavodárný	k2eAgNnSc1d1	Ústavodárné
shromáždění	shromáždění	k1gNnSc1	shromáždění
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Tarnovu	Tarnov	k1gInSc3	Tarnov
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nového	nový	k2eAgNnSc2d1	nové
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
bude	být	k5eAaImBp3nS	být
Sofia	Sofia	k1gFnSc1	Sofia
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
městě	město	k1gNnSc6	město
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
obyvatel	obyvatel	k1gMnSc1	obyvatel
rychle	rychle	k6eAd1	rychle
přibývalo	přibývat	k5eAaImAgNnS	přibývat
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1904-1912	[number]	k4	1904-1912
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Alexandra	Alexandr	k1gMnSc2	Alexandr
Něvského	něvský	k2eAgMnSc2d1	něvský
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
zorganizovalo	zorganizovat	k5eAaPmAgNnS	zorganizovat
bulharští	bulharský	k2eAgMnPc1d1	bulharský
komunisté	komunista	k1gMnPc1	komunista
bombový	bombový	k2eAgMnSc1d1	bombový
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
kostel	kostel	k1gInSc4	kostel
Sveta	Svet	k1gMnSc2	Svet
Nedelja	Nedeljus	k1gMnSc2	Nedeljus
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
120	[number]	k4	120
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
500	[number]	k4	500
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
Sofie	Sofie	k1gFnSc1	Sofie
leteckými	letecký	k2eAgInPc7d1	letecký
nálety	nálet	k1gInPc7	nálet
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Stalin	Stalin	k1gMnSc1	Stalin
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
obsadil	obsadit	k5eAaPmAgMnS	obsadit
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1946	[number]	k4	1946
zde	zde	k6eAd1	zde
Jiří	Jiří	k1gMnSc1	Jiří
Dimitrov	Dimitrov	k1gInSc4	Dimitrov
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
lidovou	lidový	k2eAgFnSc4d1	lidová
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
pak	pak	k6eAd1	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
sídlišť	sídliště	k1gNnPc2	sídliště
a	a	k8xC	a
továren	továrna	k1gFnPc2	továrna
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
padl	padnout	k5eAaPmAgInS	padnout
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
přestavba	přestavba	k1gFnSc1	přestavba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
Sofie	Sofie	k1gFnSc1	Sofie
se	se	k3xPyFc4	se
však	však	k9	však
rychle	rychle	k6eAd1	rychle
vzpamatovala	vzpamatovat	k5eAaPmAgFnS	vzpamatovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
živým	živý	k2eAgNnSc7d1	živé
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Oblast	oblast	k1gFnSc1	oblast
Sofie	Sofie	k1gFnSc1	Sofie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
Oblasti	oblast	k1gFnSc2	oblast
Sofie	Sofia	k1gFnSc2	Sofia
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
24	[number]	k4	24
rajónů	rajón	k1gInPc2	rajón
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
kromě	kromě	k7c2	kromě
sofijských	sofijský	k2eAgFnPc2d1	Sofijská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
kvartálů	kvartál	k1gInPc2	kvartál
<g/>
)	)	kIx)	)
také	také	k9	také
okolní	okolní	k2eAgFnSc3d1	okolní
vsi	ves	k1gFnSc3	ves
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
nezahrnují	zahrnovat	k5eNaImIp3nP	zahrnovat
části	část	k1gFnPc1	část
vlastního	vlastní	k2eAgNnSc2d1	vlastní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
15	[number]	k4	15
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
9	[number]	k4	9
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
metropolity	metropolita	k1gMnSc2	metropolita
Ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
památky	památka	k1gFnPc4	památka
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Alexandra	Alexandr	k1gMnSc2	Alexandr
Něvského	něvský	k2eAgMnSc2d1	něvský
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
jako	jako	k8xC	jako
výraz	výraz	k1gInSc1	výraz
vděku	vděk	k1gInSc2	vděk
ruskému	ruský	k2eAgMnSc3d1	ruský
carovi	car	k1gMnSc3	car
Alexandrovi	Alexandr	k1gMnSc3	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yRgMnSc3	který
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc4	kostel
dostal	dostat	k5eAaPmAgMnS	dostat
název	název	k1gInSc4	název
patrona	patron	k1gMnSc2	patron
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
sv.	sv.	kA	sv.
Alexandra	Alexandr	k1gMnSc2	Alexandr
Něvského	něvský	k2eAgMnSc2d1	něvský
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
před	před	k7c7	před
katedrálou	katedrála	k1gFnSc7	katedrála
stojí	stát	k5eAaImIp3nS	stát
Alexandrův	Alexandrův	k2eAgInSc4d1	Alexandrův
pomník	pomník	k1gInSc4	pomník
<g/>
.	.	kIx.	.
</s>
<s>
Byzantský	byzantský	k2eAgInSc4d1	byzantský
cihlový	cihlový	k2eAgInSc4d1	cihlový
kostel	kostel	k1gInSc4	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Sofie	Sofia	k1gFnSc2	Sofia
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
městu	město	k1gNnSc3	město
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
parku	park	k1gInSc6	park
blízko	blízko	k7c2	blízko
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Kostelík	kostelík	k1gInSc1	kostelík
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
cennými	cenný	k2eAgFnPc7d1	cenná
freskami	freska	k1gFnPc7	freska
uprostřed	uprostřed	k7c2	uprostřed
římského	římský	k2eAgNnSc2d1	římské
fóra	fórum	k1gNnSc2	fórum
v	v	k7c6	v
samém	samý	k3xTgNnSc6	samý
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Nikolaje	Nikolaj	k1gMnSc2	Nikolaj
Divotvorce	divotvorce	k1gMnSc2	divotvorce
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Mešita	mešita	k1gFnSc1	mešita
Banja	banjo	k1gNnSc2	banjo
Baši	baša	k1gMnSc2	baša
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
<g/>
,,	,,	k?	,,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
turecké	turecký	k2eAgFnSc2d1	turecká
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgNnSc1d1	Archeologické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
se	s	k7c7	s
starověkými	starověký	k2eAgFnPc7d1	starověká
a	a	k8xC	a
byzantskými	byzantský	k2eAgFnPc7d1	byzantská
památkami	památka	k1gFnPc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
Etnografické	etnografický	k2eAgNnSc1d1	etnografické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
carském	carský	k2eAgInSc6d1	carský
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metro	metro	k1gNnSc4	metro
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
terminálem	terminál	k1gInSc7	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
strategické	strategický	k2eAgFnSc3d1	strategická
poloze	poloha	k1gFnSc3	poloha
Sofie	Sofia	k1gFnSc2	Sofia
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
důležitým	důležitý	k2eAgNnSc7d1	důležité
centrem	centrum	k1gNnSc7	centrum
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
železniční	železniční	k2eAgFnPc4d1	železniční
a	a	k8xC	a
automobilové	automobilový	k2eAgFnPc4d1	automobilová
trasy	trasa	k1gFnPc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
procházejí	procházet	k5eAaImIp3nP	procházet
evropské	evropský	k2eAgFnSc2d1	Evropská
trasy	trasa	k1gFnSc2	trasa
E79	E79	k1gFnSc2	E79
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
do	do	k7c2	do
řecké	řecký	k2eAgFnSc2d1	řecká
Soluně	Soluň	k1gFnSc2	Soluň
a	a	k8xC	a
E80	E80	k1gFnSc2	E80
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
všechny	všechen	k3xTgInPc1	všechen
hlavní	hlavní	k2eAgInPc1d1	hlavní
druhy	druh	k1gInPc1	druh
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
vodní	vodní	k2eAgFnSc2d1	vodní
dopravy	doprava	k1gFnSc2	doprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
největší	veliký	k2eAgFnSc1d3	veliký
je	on	k3xPp3gNnSc4	on
centrální	centrální	k2eAgNnSc4d1	centrální
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
je	být	k5eAaImIp3nS	být
nové	nový	k2eAgNnSc4d1	nové
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
a	a	k8xC	a
nejmodernější	moderní	k2eAgFnSc4d3	nejmodernější
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
21	[number]	k4	21
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
84	[number]	k4	84
km	km	kA	km
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
9	[number]	k4	9
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
měří	měřit	k5eAaImIp3nS	měřit
104	[number]	k4	104
km	km	kA	km
a	a	k8xC	a
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
jezdí	jezdit	k5eAaImIp3nS	jezdit
téměř	téměř	k6eAd1	téměř
600	[number]	k4	600
městských	městský	k2eAgInPc2d1	městský
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
má	mít	k5eAaImIp3nS	mít
Sofia	Sofia	k1gFnSc1	Sofia
funkční	funkční	k2eAgFnSc1d1	funkční
systém	systém	k1gInSc4	systém
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	s	k7c7	s
27	[number]	k4	27
stanicemi	stanice	k1gFnPc7	stanice
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
plánuje	plánovat	k5eAaImIp3nS	plánovat
vybudovat	vybudovat	k5eAaPmF	vybudovat
další	další	k2eAgFnPc4d1	další
stanice	stanice	k1gFnPc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
se	se	k3xPyFc4	se
stavělo	stavět	k5eAaImAgNnS	stavět
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
dvacet	dvacet	k4xCc1	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
výstavba	výstavba	k1gFnSc1	výstavba
totiž	totiž	k9	totiž
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
zastavena	zastavit	k5eAaPmNgFnS	zastavit
kvůli	kvůli	k7c3	kvůli
archeologickým	archeologický	k2eAgInPc3d1	archeologický
nálezům	nález	k1gInPc3	nález
i	i	k8xC	i
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
jízdné	jízdné	k1gNnSc1	jízdné
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
leva	leva	k1gInSc2	leva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
taxi	taxi	k1gNnSc4	taxi
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
díky	dík	k1gInPc7	dík
cenové	cenový	k2eAgFnSc2d1	cenová
dostupnosti	dostupnost	k1gFnSc2	dostupnost
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgFnPc3d1	jiná
evropským	evropský	k2eAgFnPc3d1	Evropská
metropolím	metropol	k1gFnPc3	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgInPc4d1	velký
metalurgické	metalurgický	k2eAgInPc4d1	metalurgický
závody	závod	k1gInPc4	závod
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zastoupen	zastoupen	k2eAgInSc4d1	zastoupen
strojní	strojní	k2eAgInSc4d1	strojní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
industrializaci	industrializace	k1gFnSc3	industrializace
a	a	k8xC	a
masové	masový	k2eAgFnSc3d1	masová
výstavbě	výstavba	k1gFnSc3	výstavba
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
značně	značně	k6eAd1	značně
narušily	narušit	k5eAaPmAgInP	narušit
estetický	estetický	k2eAgInSc4d1	estetický
ráz	ráz	k1gInSc4	ráz
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
minulého	minulý	k2eAgInSc2d1	minulý
režimu	režim	k1gInSc2	režim
do	do	k7c2	do
Sofie	Sofia	k1gFnSc2	Sofia
zamířil	zamířit	k5eAaPmAgMnS	zamířit
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
kapitál	kapitál	k1gInSc4	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnPc1d1	přední
světové	světový	k2eAgFnPc1d1	světová
firmy	firma	k1gFnPc1	firma
zde	zde	k6eAd1	zde
otevřely	otevřít	k5eAaPmAgFnP	otevřít
své	svůj	k3xOyFgInPc4	svůj
závody	závod	k1gInPc4	závod
např.	např.	kA	např.
Toyota	toyota	k1gFnSc1	toyota
a	a	k8xC	a
Pepsi	Pepsi	k1gFnSc1	Pepsi
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
řada	řada	k1gFnSc1	řada
bank	banka	k1gFnPc2	banka
zde	zde	k6eAd1	zde
zřídila	zřídit	k5eAaPmAgFnS	zřídit
své	svůj	k3xOyFgFnPc4	svůj
pobočky	pobočka	k1gFnPc4	pobočka
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Sofie	Sofie	k1gFnSc1	Sofie
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Turínem	Turín	k1gInSc7	Turín
jediné	jediný	k2eAgNnSc1d1	jediné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
hostilo	hostit	k5eAaImAgNnS	hostit
hry	hra	k1gFnSc2	hra
letní	letní	k2eAgInPc4d1	letní
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
i	i	k8xC	i
zimní	zimní	k2eAgFnSc1d1	zimní
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
univerziády	univerziáda	k1gFnSc2	univerziáda
<g/>
.	.	kIx.	.
</s>
<s>
Sofie	Sofie	k1gFnSc1	Sofie
byla	být	k5eAaImAgFnS	být
i	i	k9	i
kandidátským	kandidátský	k2eAgNnSc7d1	kandidátské
městem	město	k1gNnSc7	město
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Levski	Levski	k6eAd1	Levski
Sofia	Sofia	k1gFnSc1	Sofia
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
CSKA	CSKA	kA	CSKA
Sofia	Sofia	k1gFnSc1	Sofia
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
Sofia	Sofia	k1gFnSc1	Sofia
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Slavia	Slavia	k1gFnSc1	Slavia
Sofia	Sofia	k1gFnSc1	Sofia
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
</s>
