<p>
<s>
Filip	Filip	k1gMnSc1	Filip
Číhal	Číhal	k1gMnSc1	Číhal
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1981	[number]	k4	1981
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1998	[number]	k4	1998
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
obránce	obránce	k1gMnSc1	obránce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
Číhal	Číhal	k1gMnSc1	Číhal
patřil	patřit	k5eAaImAgMnS	patřit
do	do	k7c2	do
širšího	široký	k2eAgInSc2d2	širší
kádru	kádr	k1gInSc2	kádr
dorostenecké	dorostenecký	k2eAgFnSc2d1	dorostenecká
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
SK	Sk	kA	Sk
Jihlava	Jihlava	k1gFnSc1	Jihlava
na	na	k7c6	na
postu	post	k1gInSc6	post
obránce	obránce	k1gMnSc2	obránce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úmrtí	úmrtí	k1gNnPc1	úmrtí
==	==	k?	==
</s>
</p>
<p>
<s>
Tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
před	před	k7c7	před
sedmnáctými	sedmnáctý	k4xOgFnPc7	sedmnáctý
narozeninami	narozeniny	k1gFnPc7	narozeniny
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
za	za	k7c4	za
domovský	domovský	k2eAgInSc4d1	domovský
klub	klub	k1gInSc4	klub
k	k	k7c3	k
utkání	utkání	k1gNnSc3	utkání
prvního	první	k4xOgInSc2	první
kola	kolo	k1gNnPc4	kolo
play	play	k0	play
off	off	k?	off
extraligy	extraliga	k1gFnPc4	extraliga
dorostu	dorost	k1gInSc2	dorost
na	na	k7c6	na
ledě	led	k1gInSc6	led
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgInSc1d1	domácí
klub	klub	k1gInSc1	klub
získal	získat	k5eAaPmAgInS	získat
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
náskok	náskok	k1gInSc1	náskok
pěti	pět	k4xCc2	pět
gólů	gól	k1gInPc2	gól
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
již	již	k6eAd1	již
hosté	host	k1gMnPc1	host
nemohli	moct	k5eNaImAgMnP	moct
dohnat	dohnat	k5eAaPmF	dohnat
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
minuty	minuta	k1gFnPc1	minuta
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
utkání	utkání	k1gNnSc6	utkání
Jihlava	Jihlava	k1gFnSc1	Jihlava
hrála	hrát	k5eAaImAgFnS	hrát
přesilovku	přesilovka	k1gFnSc4	přesilovka
<g/>
,	,	kIx,	,
když	když	k8xS	když
Filip	Filip	k1gMnSc1	Filip
Číhal	Číhal	k1gMnSc1	Číhal
bez	bez	k7c2	bez
cizího	cizí	k2eAgNnSc2d1	cizí
zavinění	zavinění	k1gNnSc2	zavinění
upadl	upadnout	k5eAaPmAgMnS	upadnout
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
útočného	útočný	k2eAgNnSc2d1	útočné
a	a	k8xC	a
středního	střední	k2eAgNnSc2d1	střední
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
lékař	lékař	k1gMnSc1	lékař
domácího	domácí	k2eAgNnSc2d1	domácí
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgFnSc1d1	srdeční
činnost	činnost	k1gFnSc1	činnost
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nepodařilo	podařit	k5eNaPmAgNnS	podařit
obnovit	obnovit	k5eAaPmF	obnovit
ani	ani	k9	ani
přivolané	přivolaný	k2eAgFnSc3d1	přivolaná
záchranné	záchranný	k2eAgFnSc3d1	záchranná
službě	služba	k1gFnSc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
hráče	hráč	k1gMnSc4	hráč
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
předchozích	předchozí	k2eAgFnPc2d1	předchozí
prohlídek	prohlídka	k1gFnPc2	prohlídka
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
se	se	k3xPyFc4	se
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
klubu	klub	k1gInSc2	klub
Ivanem	Ivan	k1gMnSc7	Ivan
Padělkem	padělek	k1gInSc7	padělek
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
nenastoupit	nastoupit	k5eNaPmF	nastoupit
k	k	k7c3	k
odvetnému	odvetný	k2eAgInSc3d1	odvetný
zápasu	zápas	k1gInSc3	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Jan	Jan	k1gMnSc1	Jan
Hrbatý	hrbatý	k2eAgMnSc1d1	hrbatý
i	i	k9	i
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
označil	označit	k5eAaPmAgInS	označit
úmrtí	úmrtí	k1gNnSc3	úmrtí
za	za	k7c4	za
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
zážitek	zážitek	k1gInSc4	zážitek
spojený	spojený	k2eAgInSc4d1	spojený
s	s	k7c7	s
hokejem	hokej	k1gInSc7	hokej
<g/>
,	,	kIx,	,
když	když	k8xS	když
musel	muset	k5eAaImAgMnS	muset
jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
osobně	osobně	k6eAd1	osobně
o	o	k7c6	o
úmrtí	úmrtí	k1gNnSc6	úmrtí
informovat	informovat	k5eAaBmF	informovat
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
