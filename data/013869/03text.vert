<s>
Z3	Z3	k4
</s>
<s>
Z3	Z3	k4
replika	replika	k1gFnSc1
vystavená	vystavená	k1gFnSc1
v	v	k7c6
Německém	německý	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Berlíně	Berlín	k1gInSc6
</s>
<s>
Z3	Z3	k4
od	od	k7c2
Konráda	Konrád	k1gMnSc2
Zuse	Zus	k1gMnSc2
byl	být	k5eAaImAgInS
první	první	k4xOgInSc1
funkční	funkční	k2eAgInSc1d1
programovatelný	programovatelný	k2eAgInSc1d1
počítací	počítací	k2eAgInSc1d1
stroj	stroj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
z	z	k7c2
2600	#num#	k4
relé	relé	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
2000	#num#	k4
relé	relé	k1gNnPc2
pro	pro	k7c4
paměť	paměť	k1gFnSc4
a	a	k8xC
600	#num#	k4
relé	relé	k1gNnPc2
pro	pro	k7c4
aritmetickou	aritmetický	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
paměti	paměť	k1gFnSc6
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
64	#num#	k4
slov	slovo	k1gNnPc2
dlouhých	dlouhý	k2eAgInPc2d1
22	#num#	k4
bitů	bit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frekvenci	frekvence	k1gFnSc4
hodinového	hodinový	k2eAgInSc2d1
signálu	signál	k1gInSc2
měl	mít	k5eAaImAgInS
5,3	5,3	k4
Hz	Hz	kA
a	a	k8xC
délku	délka	k1gFnSc4
slova	slovo	k1gNnPc4
22	#num#	k4
bitů	bit	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rychlost	rychlost	k1gFnSc1
sčítání	sčítání	k1gNnSc2
0,8	0,8	k4
s	s	k7c7
a	a	k8xC
rychlost	rychlost	k1gFnSc1
násobení	násobení	k1gNnSc1
3	#num#	k4
s.	s.	k?
Jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
spotřeba	spotřeba	k1gFnSc1
byla	být	k5eAaImAgFnS
4000	#num#	k4
wattů	watt	k1gInPc2
a	a	k8xC
vážil	vážit	k5eAaImAgMnS
1000	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpočty	výpočet	k1gInPc1
počítač	počítač	k1gMnSc1
prováděl	provádět	k5eAaImAgMnS
v	v	k7c6
binární	binární	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
a	a	k8xC
pracoval	pracovat	k5eAaImAgMnS
s	s	k7c7
čísly	číslo	k1gNnPc7
s	s	k7c7
plovoucí	plovoucí	k2eAgFnSc7d1
desetinnou	desetinný	k2eAgFnSc7d1
čárkou	čárka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z3	Z3	k1gMnSc1
četl	číst	k5eAaImAgMnS
programy	program	k1gInPc4
z	z	k7c2
děrovaného	děrovaný	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stroj	stroj	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1941	#num#	k4
úspěšně	úspěšně	k6eAd1
předveden	předvést	k5eAaPmNgInS
vědcům	vědec	k1gMnPc3
z	z	k7c2
DVL	DVL	kA
(	(	kIx(
<g/>
německy	německy	k6eAd1
Deutsche	Deutsche	k1gInSc1
Versuchsanstalt	Versuchsanstalt	k2eAgInSc1d1
für	für	k?
Luftfahrt	Luftfahrt	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
Německý	německý	k2eAgInSc1d1
výzkumný	výzkumný	k2eAgInSc1d1
ústav	ústav	k1gInSc1
letectva	letectvo	k1gNnSc2
<g/>
)	)	kIx)
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1943	#num#	k4
byl	být	k5eAaImAgInS
původní	původní	k2eAgFnSc4d1
Z3	Z3	k1gFnSc4
zničen	zničit	k5eAaPmNgInS
při	při	k7c6
bombardování	bombardování	k1gNnSc6
Berlína	Berlín	k1gInSc2
spojenci	spojenec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
byla	být	k5eAaImAgFnS
postavená	postavený	k2eAgFnSc1d1
plně	plně	k6eAd1
funkční	funkční	k2eAgFnSc1d1
replika	replika	k1gFnSc1
Z3	Z3	k1gFnSc1
společností	společnost	k1gFnSc7
Konráda	Konrád	k1gMnSc2
Zuse	Zus	k1gMnSc2
„	„	k?
<g/>
ZuseKG	ZuseKG	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
replika	replika	k1gFnSc1
je	být	k5eAaImIp3nS
vystavena	vystavit	k5eAaPmNgFnS
v	v	k7c4
Deutsches	Deutsches	k1gInSc4
Museum	museum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
úspěšně	úspěšně	k6eAd1
zvládl	zvládnout	k5eAaPmAgInS
test	test	k1gInSc1
Turingovo-vyčíslitelných	Turingovo-vyčíslitelný	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
označen	označit	k5eAaPmNgInS
za	za	k7c4
turingovsky	turingovsky	k6eAd1
úplný	úplný	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z3	Z3	k4
byl	být	k5eAaImAgInS
používán	používat	k5eAaImNgInS
za	za	k7c2
nacistické	nacistický	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
v	v	k7c6
Německém	německý	k2eAgInSc6d1
výzkumném	výzkumný	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
letectva	letectvo	k1gNnSc2
pro	pro	k7c4
statickou	statický	k2eAgFnSc4d1
analýzu	analýza	k1gFnSc4
vibrací	vibrace	k1gFnPc2
křídla	křídlo	k1gNnSc2
letadla	letadlo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
čem	co	k3yQnSc6,k3yRnSc6,k3yInSc6
byl	být	k5eAaImAgMnS
Z3	Z3	k1gFnSc4
jiný	jiný	k2eAgMnSc1d1
</s>
<s>
Počítač	počítač	k1gInSc1
Z3	Z3	k1gMnPc2
byl	být	k5eAaImAgInS
řízen	řízen	k2eAgInSc1d1
programem	program	k1gInSc7
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
prvního	první	k4xOgInSc2
počítače	počítač	k1gInSc2
sestrojeného	sestrojený	k2eAgInSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1623	#num#	k4
Wilhelmem	Wilhelm	k1gMnSc7
Schickardem	Schickard	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počítač	počítač	k1gInSc1
Z3	Z3	k1gFnSc2
byl	být	k5eAaImAgInS
úspěšný	úspěšný	k2eAgMnSc1d1
díky	díky	k7c3
použití	použití	k1gNnSc3
jednoduché	jednoduchý	k2eAgFnSc2d1
dvojkové	dvojkový	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
o	o	k7c4
tři	tři	k4xCgInPc4
století	století	k1gNnSc2
dříve	dříve	k6eAd2
vymyslel	vymyslet	k5eAaPmAgMnS
Gottfried	Gottfried	k1gMnSc1
Leibniz	Leibniz	k1gMnSc1
a	a	k8xC
Boole	Boole	k1gFnSc1
ji	on	k3xPp3gFnSc4
později	pozdě	k6eAd2
použil	použít	k5eAaPmAgInS
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
Booleovu	Booleův	k2eAgFnSc4d1
algebru	algebra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
napadlo	napadnout	k5eAaPmAgNnS
Claude	Claud	k1gInSc5
Shannona	Shannon	k1gMnSc4
z	z	k7c2
MIT	MIT	kA
(	(	kIx(
<g/>
Massachusettský	massachusettský	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
)	)	kIx)
aplikovat	aplikovat	k5eAaBmF
Booleovu	Booleův	k2eAgFnSc4d1
algebru	algebra	k1gFnSc4
na	na	k7c4
elektronická	elektronický	k2eAgNnPc4d1
relé	relé	k1gNnPc4
při	při	k7c6
návrhu	návrh	k1gInSc6
digitálních	digitální	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
seminární	seminární	k2eAgFnSc6d1
práci	práce	k1gFnSc6
o	o	k7c6
číslicové	číslicový	k2eAgFnSc6d1
technice	technika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
až	až	k9
Zuse	Zuse	k1gInSc1
(	(	kIx(
<g/>
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
znal	znát	k5eAaImAgMnS
práci	práce	k1gFnSc4
Claude	Claud	k1gInSc5
Shannona	Shannon	k1gMnSc4
z	z	k7c2
MIT	MIT	kA
<g/>
)	)	kIx)
prakticky	prakticky	k6eAd1
propojil	propojit	k5eAaPmAgMnS
obě	dva	k4xCgFnPc4
myšlenky	myšlenka	k1gFnPc4
v	v	k7c6
programovém	programový	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
Z	z	k7c2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
programovatelný	programovatelný	k2eAgInSc1d1
počítač	počítač	k1gInSc1
navrhl	navrhnout	k5eAaPmAgInS
britský	britský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Charles	Charles	k1gMnSc1
Babbage	Babbage	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
a	a	k8xC
nazval	nazvat	k5eAaBmAgInS,k5eAaPmAgInS
ho	on	k3xPp3gMnSc4
Analytical	Analytical	k1gMnSc4
Engine	Engin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
elektronické	elektronický	k2eAgInPc1d1
číslicové	číslicový	k2eAgInPc1d1
počítače	počítač	k1gInPc1
byly	být	k5eAaImAgInP
vyrobeny	vyrobit	k5eAaPmNgInP
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
a	a	k8xC
nazývaly	nazývat	k5eAaImAgFnP
se	se	k3xPyFc4
Colossus	Colossus	k1gMnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
jich	on	k3xPp3gMnPc2
10	#num#	k4
a	a	k8xC
sloužily	sloužit	k5eAaImAgFnP
k	k	k7c3
lámání	lámání	k1gNnSc3
kódů	kód	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jejich	jejich	k3xOp3gFnSc4
výrobu	výroba	k1gFnSc4
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
elektronky	elektronka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počítaly	počítat	k5eAaImAgInP
ve	v	k7c6
dvojkové	dvojkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
a	a	k8xC
programovaly	programovat	k5eAaImAgInP
se	se	k3xPyFc4
pomocí	pomocí	k7c2
propojovacích	propojovací	k2eAgFnPc2d1
polí	pole	k1gFnPc2
a	a	k8xC
přepínačů	přepínač	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
těchto	tento	k3xDgMnPc2
počítačů	počítač	k1gMnPc2
byl	být	k5eAaImAgInS
několik	několik	k4yIc4
desetiletí	desetiletí	k1gNnPc2
utajován	utajován	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
nebyly	být	k5eNaImAgInP
dlouho	dlouho	k6eAd1
uváděny	uvádět	k5eAaImNgInP
v	v	k7c6
historii	historie	k1gFnSc6
vývoje	vývoj	k1gInSc2
počítačů	počítač	k1gMnPc2
jako	jako	k8xS,k8xC
první	první	k4xOgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ENIAC	ENIAC	kA
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
až	až	k9
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahoval	obsahovat	k5eAaImAgInS
elektronky	elektronka	k1gFnSc2
<g/>
,	,	kIx,
programoval	programovat	k5eAaImAgMnS
se	se	k3xPyFc4
pomocí	pomocí	k7c2
přepínačů	přepínač	k1gInPc2
a	a	k8xC
používal	používat	k5eAaImAgInS
desítkovou	desítkový	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
se	se	k3xPyFc4
programoval	programovat	k5eAaImAgMnS
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
Colossus	Colossus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Z3	Z3	k4
měl	mít	k5eAaImAgInS
uložený	uložený	k2eAgInSc1d1
program	program	k1gInSc1
na	na	k7c4
externí	externí	k2eAgInPc4d1
pásce	pásec	k1gInPc4
<g/>
,	,	kIx,
takže	takže	k8xS
při	při	k7c6
výměně	výměna	k1gFnSc6
programu	program	k1gInSc2
nebylo	být	k5eNaImAgNnS
nutné	nutný	k2eAgNnSc1d1
znovu	znovu	k6eAd1
ručně	ručně	k6eAd1
přenastavit	přenastavit	k5eAaPmF
propojovací	propojovací	k2eAgNnSc4d1
pole	pole	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvními	první	k4xOgInPc7
počítači	počítač	k1gMnPc7
na	na	k7c6
světě	svět	k1gInSc6
s	s	k7c7
programem	program	k1gInSc7
uloženým	uložený	k2eAgInSc7d1
v	v	k7c6
paměti	paměť	k1gFnSc6
uvnitř	uvnitř	k7c2
počítače	počítač	k1gInSc2
byly	být	k5eAaImAgFnP
Manchester	Manchester	k1gInSc4
Small-Scale	Small-Scal	k1gMnSc5
Experimental	Experimental	k1gMnSc1
Machine	Machin	k1gInSc5
z	z	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
a	a	k8xC
EDSAC	EDSAC	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Electronic	Electronice	k1gFnPc2
Delay	Delay	k1gInPc1
Storage	Storag	k1gFnSc2
Automatic	Automatice	k1gFnPc2
Calculator	Calculator	k1gMnSc1
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavedení	zavedení	k1gNnSc1
koncepce	koncepce	k1gFnSc2
paměti	paměť	k1gFnSc2
programu	program	k1gInSc2
uvnitř	uvnitř	k7c2
počítače	počítač	k1gInSc2
je	být	k5eAaImIp3nS
papírově	papírově	k6eAd1
přisuzováno	přisuzován	k2eAgNnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
John	John	k1gMnSc1
von	von	k1gInSc4
Neumannovi	Neumann	k1gMnSc3
a	a	k8xC
jeho	jeho	k3xOp3gMnPc3
kolegům	kolega	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
o	o	k7c6
tomto	tento	k3xDgInSc6
konceptu	koncept	k1gInSc6
se	se	k3xPyFc4
již	již	k6eAd1
roku	rok	k1gInSc2
1936	#num#	k4
zmiňuje	zmiňovat	k5eAaImIp3nS
sám	sám	k3xTgInSc1
Konrad	Konrad	k1gInSc1
Zuse	Zus	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
jeho	jeho	k3xOp3gInSc1
patent	patent	k1gInSc1
byl	být	k5eAaImAgInS
zamítnut	zamítnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Porovnání	porovnání	k1gNnSc1
charakteristických	charakteristický	k2eAgInPc2d1
rysů	rys	k1gInPc2
prvních	první	k4xOgInPc2
číslicových	číslicový	k2eAgInPc2d1
počítačů	počítač	k1gInPc2
</s>
<s>
Název	název	k1gInSc1
počítačeUveden	počítačeUveden	k2eAgInSc1d1
do	do	k7c2
provozuČíselná	provozuČíselný	k2eAgFnSc1d1
soustavaMechanismus	soustavaMechanismus	k1gInSc4
počítačeProgramováníTuring-complete	počítačeProgramováníTuring-complet	k1gInSc5
</s>
<s>
Zuse	Zuse	k1gFnSc1
Z3	Z3	k1gFnSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Květen	květen	k1gInSc4
1941	#num#	k4
<g/>
BinárníElektromechanickéŘízený	BinárníElektromechanickéŘízený	k2eAgInSc1d1
programem	program	k1gInSc7
na	na	k7c4
děrované	děrovaný	k2eAgNnSc4d1
filmové	filmový	k2eAgNnSc4d1
pásceAno	pásceAno	k1gNnSc4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Atanasoff	Atanasoff	k1gMnSc1
<g/>
–	–	k?
<g/>
Berry	Berr	k1gInPc1
Computer	computer	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
Léto	léto	k1gNnSc4
1941	#num#	k4
<g/>
BinárníElektronickéNeprogramovatelný	BinárníElektronickéNeprogramovatelný	k2eAgInSc1d1
<g/>
,	,	kIx,
jednoúčelovýNe	jednoúčelovýNat	k5eAaPmIp3nS
</s>
<s>
Colossus	Colossus	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
Leden	leden	k1gInSc4
1944	#num#	k4
<g/>
BinárníElektronickéŘízený	BinárníElektronickéŘízený	k2eAgInSc4d1
programem	program	k1gInSc7
–	–	k?
propojovací	propojovací	k2eAgInPc4d1
kabely	kabel	k1gInPc4
a	a	k8xC
přepínačeNe	přepínačeN	k1gInPc4
</s>
<s>
Harvard	Harvard	k1gInSc1
Mark	Mark	k1gMnSc1
I-IBM	I-IBM	k1gMnSc1
ASSC	ASSC	kA
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
1944	#num#	k4
<g/>
DecimálníElektromechanickéŘízený	DecimálníElektromechanickéŘízený	k2eAgInSc1d1
programem	program	k1gInSc7
–	–	k?
24	#num#	k4
děr	děra	k1gFnPc2
na	na	k7c4
papírové	papírový	k2eAgInPc4d1
děrné	děrný	k2eAgInPc4d1
pásce	pásec	k1gInPc4
(	(	kIx(
<g/>
žádné	žádný	k3yNgNnSc1
podmíněné	podmíněný	k2eAgNnSc1d1
větvení	větvení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Ano	ano	k9
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ENIAC	ENIAC	kA
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
Listopad	listopad	k1gInSc4
1945	#num#	k4
<g/>
DecimálníElektronickéŘízený	DecimálníElektronickéŘízený	k2eAgInSc4d1
programem	program	k1gInSc7
–	–	k?
propojovací	propojovací	k2eAgInPc4d1
kabely	kabel	k1gInPc4
a	a	k8xC
přepínačeAno	přepínačeAno	k6eAd1
</s>
<s>
Manchester	Manchester	k1gInSc1
Small-Scale	Small-Scal	k1gMnSc5
Experimental	Experimental	k1gMnSc1
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
Červen	červen	k1gInSc4
1948	#num#	k4
<g/>
BinárníElektronickéŘízený	BinárníElektronickéŘízený	k2eAgInSc4d1
programem	program	k1gInSc7
uloženým	uložený	k2eAgInSc7d1
ve	v	k7c6
Williamsově	Williamsův	k2eAgFnSc6d1
trubiciAno	trubiciAno	k6eAd1
</s>
<s>
Upravený	upravený	k2eAgMnSc1d1
ENIAC	ENIAC	kA
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
Září	zářit	k5eAaImIp3nS
1948	#num#	k4
<g/>
DecimálníElektronickéŘízený	DecimálníElektronickéŘízený	k2eAgInSc1d1
programem	program	k1gInSc7
–	–	k?
propojovací	propojovací	k2eAgInPc4d1
kabely	kabel	k1gInPc4
a	a	k8xC
přepínače	přepínač	k1gInPc4
plus	plus	k6eAd1
primitivní	primitivní	k2eAgInSc1d1
nepřepisovatelný	přepisovatelný	k2eNgInSc1d1
program	program	k1gInSc1
používající	používající	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
funkcí	funkce	k1gFnPc2
(	(	kIx(
<g/>
podobné	podobný	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
ROM	ROM	kA
<g/>
)	)	kIx)
<g/>
Ano	ano	k9
</s>
<s>
EDSACKvěten	EDSACKvěten	k2eAgInSc1d1
1949	#num#	k4
<g/>
BinárníElektronickéŘízený	BinárníElektronickéŘízený	k2eAgInSc1d1
programem	program	k1gInSc7
uloženým	uložený	k2eAgInSc7d1
ve	v	k7c6
Williamsově	Williamsův	k2eAgFnSc6d1
trubici	trubice	k1gFnSc6
a	a	k8xC
drum	drum	k6eAd1
(	(	kIx(
<g/>
bubnové	bubnový	k2eAgNnSc1d1
<g/>
)	)	kIx)
magnetické	magnetický	k2eAgNnSc1d1
pamětiAno	pamětiAno	k1gNnSc1
</s>
<s>
Manchester	Manchester	k1gInSc1
Mark	Mark	k1gMnSc1
IŘíjen	IŘíjna	k1gFnPc2
1949	#num#	k4
<g/>
BinárníElektronickéŘízený	BinárníElektronickéŘízený	k2eAgInSc1d1
programem	program	k1gInSc7
uloženým	uložený	k2eAgInSc7d1
ve	v	k7c6
rtuťové	rtuťový	k2eAgFnSc6d1
paměti	paměť	k1gFnSc6
se	s	k7c7
zpožďovací	zpožďovací	k2eAgFnSc7d1
linkou	linka	k1gFnSc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Delay	Dela	k2eAgFnPc4d1
line	linout	k5eAaImIp3nS
memory	memora	k1gFnPc4
<g/>
)	)	kIx)
<g/>
Ano	ano	k9
</s>
<s>
CSIRAC	CSIRAC	kA
(	(	kIx(
<g/>
Austrálie	Austrálie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
Listopad	listopad	k1gInSc4
1949	#num#	k4
<g/>
BinárníElektronickéŘízený	BinárníElektronickéŘízený	k2eAgInSc4d1
programem	program	k1gInSc7
uloženým	uložený	k2eAgInSc7d1
v	v	k7c6
paměti	paměť	k1gFnSc6
se	se	k3xPyFc4
zpožďovací	zpožďovací	k2eAgNnSc1d1
linkouAno	linkouAno	k1gNnSc1
</s>
<s>
Implementace	implementace	k1gFnSc1
Turingova	Turingův	k2eAgInSc2d1
univerzálního	univerzální	k2eAgInSc2d1
stroje	stroj	k1gInSc2
na	na	k7c6
Z3	Z3	k1gFnSc6
</s>
<s>
Na	na	k7c4
Z3	Z3	k1gFnSc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
v	v	k7c6
programu	program	k1gInSc6
vytvářet	vytvářet	k5eAaImF
smyčky	smyčka	k1gFnPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
nebyla	být	k5eNaImAgFnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
žádná	žádný	k3yNgFnSc1
instrukce	instrukce	k1gFnSc1
podmíněného	podmíněný	k2eAgInSc2d1
skoku	skok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
to	ten	k3xDgNnSc4
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
podařilo	podařit	k5eAaPmAgNnS
ukázat	ukázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
na	na	k7c6
něm	on	k3xPp3gNnSc6
implementovat	implementovat	k5eAaImF
Turingův	Turingův	k2eAgInSc4d1
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
popsal	popsat	k5eAaPmAgMnS
Raúl	Raúl	k1gMnSc1
Rojas	Rojas	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
své	svůj	k3xOyFgFnSc2
práce	práce	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
že	že	k8xS
z	z	k7c2
pohledu	pohled	k1gInSc2
abstraktní	abstraktní	k2eAgFnSc2d1
teoretické	teoretický	k2eAgFnSc2d1
perspektivy	perspektiva	k1gFnSc2
je	být	k5eAaImIp3nS
počítačový	počítačový	k2eAgInSc1d1
model	model	k1gInSc1
Z3	Z3	k1gFnSc2
rovnocenný	rovnocenný	k2eAgInSc1d1
počítačovému	počítačový	k2eAgInSc3d1
modelu	model	k1gInSc3
současných	současný	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
z	z	k7c2
praktického	praktický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
se	se	k3xPyFc4
od	od	k7c2
dnešních	dnešní	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
liší	lišit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
pragmatického	pragmatický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Z3	Z3	k1gMnSc1
poskytl	poskytnout	k5eAaPmAgMnS
docela	docela	k6eAd1
praktickou	praktický	k2eAgFnSc4d1
instrukční	instrukční	k2eAgFnSc4d1
sadu	sada	k1gFnSc4
pro	pro	k7c4
řešení	řešení	k1gNnSc4
typických	typický	k2eAgInPc2d1
inženýrských	inženýrský	k2eAgInPc2d1
problémů	problém	k1gInPc2
40	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zuse	Zus	k1gInSc2
byl	být	k5eAaImAgMnS
stavební	stavební	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
stavěl	stavět	k5eAaImAgInS
počítače	počítač	k1gInPc4
pro	pro	k7c4
usnadnění	usnadnění	k1gNnSc4
své	svůj	k3xOyFgFnSc2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ZUSE	ZUSE	kA
<g/>
,	,	kIx,
Konrad	Konrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Computer	computer	k1gInSc4
?	?	kIx.
</s>
<s desamb="1">
Mein	Mein	k1gInSc1
Lebenswerk	Lebenswerk	k1gInSc4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
rd	rd	k?
ed	ed	k?
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Springer-Verlag	Springer-Verlag	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
540	#num#	k4
<g/>
-	-	kIx~
<g/>
56292	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
p.	p.	k?
55	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
German	German	k1gMnSc1
<g/>
)	)	kIx)
↑	↑	k?
Technische	Technische	k1gInSc1
Universität	Universität	k1gMnSc1
Berlin	berlina	k1gFnPc2
-	-	kIx~
Rechenhilfe	Rechenhilf	k1gInSc5
für	für	k?
Ingenieure	Ingenieur	k1gMnSc5
Archivováno	archivován	k2eAgNnSc4d1
13	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Essay	Essaa	k1gFnSc2
on	on	k3xPp3gInSc1
Zuse	Zuse	k1gInSc1
(	(	kIx(
<g/>
in	in	k?
German	German	k1gMnSc1
<g/>
)	)	kIx)
-	-	kIx~
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Berlín	Berlín	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
ROJAS	ROJAS	kA
<g/>
,	,	kIx,
R.	R.	kA
How	How	k1gFnSc1
to	ten	k3xDgNnSc1
make	makat	k5eAaPmIp3nS
Zuse	Zuse	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Z3	Z3	k1gMnSc7
a	a	k8xC
universal	universat	k5eAaPmAgInS,k5eAaImAgInS
computer	computer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
Annals	Annalsa	k1gFnPc2
of	of	k?
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Computing	Computing	k1gInSc1
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
20	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
pp	pp	k?
<g/>
.	.	kIx.
51	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
85.707	85.707	k4
<g/>
574	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
How	How	k1gFnSc6
to	ten	k3xDgNnSc1
Make	Make	k1gNnSc1
Zuse	Zus	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Z3	Z3	k1gFnSc7
a	a	k8xC
Universal	Universal	k1gFnPc7
Computer	computer	k1gInSc4
by	by	kYmCp3nS
Raúl	Raúl	k1gMnSc1
Rojas	Rojas	k1gMnSc1
<g/>
.	.	kIx.
www.zib.de	www.zib.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zuse	Zuse	k1gInSc1
<g/>
.	.	kIx.
www.crash-it.com	www.crash-it.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
B.	B.	kA
Jack	Jack	k1gInSc1
Copeland	Copeland	k1gInSc1
(	(	kIx(
<g/>
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Colossus	Colossus	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Secrets	Secretsa	k1gFnPc2
of	of	k?
Bletchley	Bletchley	k1gInPc1
Park	park	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Codebreaking	Codebreaking	k1gInSc1
Computers	Computersa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
284055	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Z3	Z3	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Z3	Z3	k4
page	page	k1gFnSc1
at	at	k?
the	the	k?
Technical	Technical	k1gMnSc1
University	universita	k1gFnSc2
of	of	k?
Berlin	berlina	k1gFnPc2
</s>
<s>
The	The	k?
Life	Life	k1gInSc1
and	and	k?
Work	Work	k1gInSc1
of	of	k?
Konrad	Konrad	k1gInSc1
Zuse	Zuse	k1gInSc1
</s>
<s>
Konrad	Konrad	k1gInSc1
Zuse	Zuse	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Legacy	Legacy	k1gInPc7
<g/>
:	:	kIx,
The	The	k1gMnSc5
Architecture	Architectur	k1gMnSc5
of	of	k?
the	the	k?
Z1	Z1	k1gMnSc1
and	and	k?
Z3	Z3	k1gMnSc1
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
</s>
<s>
How	How	k?
to	ten	k3xDgNnSc1
Make	Make	k1gNnSc1
Zuse	Zus	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Z3	Z3	k1gFnSc7
a	a	k8xC
Universal	Universal	k1gFnSc1
Computer	computer	k1gInSc1
Raúl	Raúl	k1gMnSc1
Rojas	Rojas	k1gMnSc1
</s>
<s>
Raúl	Raúl	k1gMnSc1
Rojas	Rojas	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Zuse	Zus	k1gFnSc2
Computers	Computers	k1gInSc1
in	in	k?
RESURRECTION	RESURRECTION	kA
The	The	k1gFnSc2
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
Computer	computer	k1gInSc1
Conservation	Conservation	k1gInSc1
Society	societa	k1gFnSc2
ISSN	ISSN	kA
0958-7403	0958-7403	k4
Number	Number	k1gInSc1
37	#num#	k4
Spring	Spring	k1gInSc1
2006	#num#	k4
</s>
