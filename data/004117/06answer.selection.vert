<s>
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
,	,	kIx,	,
křtěn	křtěn	k2eAgMnSc1d1	křtěn
Jan	Jan	k1gMnSc1	Jan
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1787	[number]	k4	1787
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
křestní	křestní	k2eAgFnSc2d1	křestní
matriky	matrika	k1gFnSc2	matrika
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
Libochovice	Libochovice	k1gFnPc1	Libochovice
(	(	kIx(	(
<g/>
zámek	zámek	k1gInSc1	zámek
<g/>
)	)	kIx)	)
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1869	[number]	k4	1869
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
fyziolog	fyziolog	k1gMnSc1	fyziolog
<g/>
,	,	kIx,	,
anatom	anatom	k1gMnSc1	anatom
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
<g/>
;	;	kIx,	;
otec	otec	k1gMnSc1	otec
malíře	malíř	k1gMnSc2	malíř
Karla	Karel	k1gMnSc2	Karel
Purkyně	Purkyně	k1gFnSc2	Purkyně
<g/>
.	.	kIx.	.
</s>
