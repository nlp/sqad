<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
Kongeriget	Kongeriget	k1gInSc1	Kongeriget
Danmark	Danmark	k1gInSc1	Danmark
nebo	nebo	k8xC	nebo
Danmarks	Danmarks	k1gInSc1	Danmarks
Rige	Rig	k1gInSc2	Rig
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
celek	celek	k1gInSc4	celek
a	a	k8xC	a
konstituční	konstituční	k2eAgFnSc2d1	konstituční
monarchie	monarchie	k1gFnSc2	monarchie
ležící	ležící	k2eAgFnSc2d1	ležící
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
částí	část	k1gFnSc7	část
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
