<p>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
Kongeriget	Kongeriget	k1gInSc1	Kongeriget
Danmark	Danmark	k1gInSc1	Danmark
nebo	nebo	k8xC	nebo
Danmarks	Danmarks	k1gInSc1	Danmarks
Rige	Rig	k1gInSc2	Rig
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
celek	celek	k1gInSc4	celek
a	a	k8xC	a
konstituční	konstituční	k2eAgFnSc2d1	konstituční
monarchie	monarchie	k1gFnSc2	monarchie
ležící	ležící	k2eAgFnSc2d1	ležící
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
částí	část	k1gFnSc7	část
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
celků	celek	k1gInPc2	celek
<g/>
;	;	kIx,	;
vlastního	vlastní	k2eAgNnSc2d1	vlastní
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
Grónska	Grónsko	k1gNnSc2	Grónsko
ležícího	ležící	k2eAgMnSc4d1	ležící
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grónsko	Grónsko	k1gNnSc4	Grónsko
a	a	k8xC	a
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
patřící	patřící	k2eAgInPc4d1	patřící
oficiálně	oficiálně	k6eAd1	oficiálně
také	také	k6eAd1	také
k	k	k7c3	k
Dánsku	Dánsko	k1gNnSc3	Dánsko
(	(	kIx(	(
<g/>
Dánskému	dánský	k2eAgNnSc3d1	dánské
království	království	k1gNnSc3	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
disponují	disponovat	k5eAaBmIp3nP	disponovat
autonomií	autonomie	k1gFnSc7	autonomie
<g/>
,	,	kIx,	,
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
deleguje	delegovat	k5eAaBmIp3nS	delegovat
dva	dva	k4xCgMnPc4	dva
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
Folketingu	Folketing	k1gInSc2	Folketing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
vztah	vztah	k1gInSc4	vztah
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
obou	dva	k4xCgNnPc2	dva
autonomních	autonomní	k2eAgNnPc2d1	autonomní
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
polooficiální	polooficiální	k2eAgInSc1d1	polooficiální
termín	termín	k1gInSc1	termín
Rigsfæ	Rigsfæ	k1gFnSc2	Rigsfæ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
někdy	někdy	k6eAd1	někdy
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xS	jako
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
Realm	Realm	k1gMnSc1	Realm
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Dánska	Dánsko	k1gNnSc2	Dánsko
</s>
</p>
<p>
<s>
Nizozemské	nizozemský	k2eAgNnSc1d1	Nizozemské
království	království	k1gNnSc1	království
-	-	kIx~	-
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
a	a	k8xC	a
ostrovních	ostrovní	k2eAgNnPc2d1	ostrovní
území	území	k1gNnPc2	území
Aruba	Aruba	k1gFnSc1	Aruba
<g/>
,	,	kIx,	,
Curaçao	curaçao	k1gNnSc1	curaçao
a	a	k8xC	a
Svatý	svatý	k2eAgMnSc1d1	svatý
Martin	Martin	k1gMnSc1	Martin
</s>
</p>
<p>
<s>
Novozélandské	novozélandský	k2eAgNnSc1d1	novozélandské
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
dánských	dánský	k2eAgMnPc2d1	dánský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
