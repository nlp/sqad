<s>
Nadvarle	nadvarle	k1gNnSc1	nadvarle
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
epididymis	epididymis	k1gFnSc1	epididymis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
velmi	velmi	k6eAd1	velmi
těsně	těsně	k6eAd1	těsně
stočenému	stočený	k2eAgInSc3d1	stočený
kanálku	kanálek	k1gInSc3	kanálek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
varlete	varle	k1gNnSc2	varle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
a	a	k8xC	a
zadní	zadní	k2eAgFnSc6d1	zadní
ploše	plocha	k1gFnSc6	plocha
varlete	varle	k1gNnSc2	varle
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
natáhl	natáhnout	k5eAaPmAgMnS	natáhnout
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
by	by	kYmCp3nS	by
délky	délka	k1gFnSc2	délka
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nadvarle	nadvarle	k1gNnSc1	nadvarle
je	být	k5eAaImIp3nS	být
zásobárnou	zásobárna	k1gFnSc7	zásobárna
spermií	spermie	k1gFnPc2	spermie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
přibližně	přibližně	k6eAd1	přibližně
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yIgInPc2	který
spermie	spermie	k1gFnPc4	spermie
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
<g/>
,	,	kIx,	,
získávají	získávat	k5eAaImIp3nP	získávat
schopnost	schopnost	k1gFnSc4	schopnost
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
oplodnění	oplodnění	k1gNnSc2	oplodnění
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadvarleti	nadvarle	k1gNnSc6	nadvarle
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
hlenovitý	hlenovitý	k2eAgInSc1d1	hlenovitý
sekret	sekret	k1gInSc1	sekret
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
látkovou	látkový	k2eAgFnSc4d1	látková
výměnu	výměna	k1gFnSc4	výměna
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Do	do	k7c2	do
kanálku	kanálek	k1gInSc2	kanálek
nadvarlete	nadvarle	k1gNnSc2	nadvarle
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
i	i	k9	i
makrofágy	makrofág	k1gInPc1	makrofág
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
přicházejí	přicházet	k5eAaImIp3nP	přicházet
stěnou	stěna	k1gFnSc7	stěna
kanálku	kanálek	k1gInSc2	kanálek
nadvarlete	nadvarle	k1gNnSc2	nadvarle
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
<g/>
-li	i	k?	-li
nahromaděné	nahromaděný	k2eAgFnPc4d1	nahromaděná
spermie	spermie	k1gFnPc4	spermie
odvedeny	odveden	k2eAgFnPc4d1	odvedena
při	při	k7c6	při
ejakulaci	ejakulace	k1gFnSc6	ejakulace
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
fagocytovány	fagocytován	k2eAgInPc1d1	fagocytován
makrofágy	makrofág	k1gInPc1	makrofág
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
spermatofágy	spermatofága	k1gFnPc1	spermatofága
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Spermie	spermie	k1gFnPc1	spermie
opouštějí	opouštět	k5eAaImIp3nP	opouštět
nadvarle	nadvarle	k1gNnSc4	nadvarle
chámovodem	chámovod	k1gInSc7	chámovod
<g/>
.	.	kIx.	.
</s>
<s>
Nadvarle	nadvarle	k1gNnSc1	nadvarle
je	být	k5eAaImIp3nS	být
nahmatetelné	nahmatetelný	k2eAgNnSc1d1	nahmatetelný
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
varlete	varle	k1gNnSc2	varle
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přiškrcení	přiškrcení	k1gNnSc6	přiškrcení
nadvarlete	nadvarle	k1gNnSc2	nadvarle
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
umělé	umělý	k2eAgFnSc3d1	umělá
sterilitě	sterilita	k1gFnSc3	sterilita
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
používané	používaný	k2eAgInPc1d1	používaný
jako	jako	k8xC	jako
druh	druh	k1gInSc1	druh
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
<g/>
.	.	kIx.	.
epididymitida	epididymitida	k1gFnSc1	epididymitida
–	–	k?	–
zánět	zánět	k1gInSc1	zánět
nadvarlete	nadvarle	k1gNnSc2	nadvarle
</s>
