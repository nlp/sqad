<s>
Nadvarle	nadvarle	k1gNnSc1	nadvarle
je	být	k5eAaImIp3nS	být
zásobárnou	zásobárna	k1gFnSc7	zásobárna
spermií	spermie	k1gFnPc2	spermie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
přibližně	přibližně	k6eAd1	přibližně
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yIgInPc2	který
spermie	spermie	k1gFnPc4	spermie
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
<g/>
,	,	kIx,	,
získávají	získávat	k5eAaImIp3nP	získávat
schopnost	schopnost	k1gFnSc4	schopnost
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
oplodnění	oplodnění	k1gNnSc2	oplodnění
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
