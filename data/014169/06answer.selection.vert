<s>
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
specializovaná	specializovaný	k2eAgFnSc1d1
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
odbornou	odborný	k2eAgFnSc4d1
i	i	k8xC
praktickou	praktický	k2eAgFnSc4d1
péči	péče	k1gFnSc4
o	o	k7c4
přírodu	příroda	k1gFnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
chráněné	chráněný	k2eAgFnPc4d1
krajinné	krajinný	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
maloplošná	maloplošný	k2eAgNnPc4d1
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
a	a	k8xC
ptačí	ptačí	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
.	.	kIx.
</s>