<s>
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
</s>
<s>
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1995	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
státu	stát	k1gInSc2
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Kaplanova	Kaplanův	k2eAgFnSc1d1
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
148	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
3,41	3,41	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
16,51	16,51	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.ochranaprirody.cz	www.ochranaprirody.cz	k1gMnSc1
a	a	k8xC
www.ochranaprirody.cz/en/	www.ochranaprirody.cz/en/	k?
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
dkkdkdj	dkkdkdj	k1gFnSc1
IČO	IČO	kA
</s>
<s>
62933591	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Středisko	středisko	k1gNnSc1
AOPK	AOPK	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
na	na	k7c6
Kotlářské	kotlářský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
č.	č.	k?
931	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
</s>
<s>
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
specializovaná	specializovaný	k2eAgFnSc1d1
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
odbornou	odborný	k2eAgFnSc4d1
i	i	k8xC
praktickou	praktický	k2eAgFnSc4d1
péči	péče	k1gFnSc4
o	o	k7c4
přírodu	příroda	k1gFnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
chráněné	chráněný	k2eAgFnPc4d1
krajinné	krajinný	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
maloplošná	maloplošný	k2eAgNnPc4d1
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
a	a	k8xC
ptačí	ptačí	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
a	a	k8xC
činnost	činnost	k1gFnSc1
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS
výkon	výkon	k1gInSc4
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
chráněných	chráněný	k2eAgFnPc2d1
krajinných	krajinný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
(	(	kIx(
<g/>
kromě	kromě	k7c2
CHKO	CHKO	kA
Šumava	Šumava	k1gFnSc1
a	a	k8xC
CHKO	CHKO	kA
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
národních	národní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
a	a	k8xC
národních	národní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
</s>
<s>
Zpracovává	zpracovávat	k5eAaImIp3nS
plány	plán	k1gInPc4
péče	péče	k1gFnSc2
o	o	k7c4
svěřená	svěřený	k2eAgNnPc4d1
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
a	a	k8xC
zajišťuje	zajišťovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
realizaci	realizace	k1gFnSc4
</s>
<s>
Spravuje	spravovat	k5eAaImIp3nS
pozemky	pozemek	k1gInPc4
určené	určený	k2eAgInPc4d1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
plocha	plocha	k1gFnSc1
11,7	11,7	k4
tisíc	tisíc	k4xCgInPc2
hektarů	hektar	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Administruje	administrovat	k5eAaImIp3nS
finanční	finanční	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
zaměřené	zaměřený	k2eAgInPc4d1
na	na	k7c4
obnovu	obnova	k1gFnSc4
a	a	k8xC
údržbu	údržba	k1gFnSc4
krajiny	krajina	k1gFnSc2
</s>
<s>
Zpracovává	zpracovávat	k5eAaImIp3nS
dokumentaci	dokumentace	k1gFnSc4
územního	územní	k2eAgInSc2d1
systému	systém	k1gInSc2
ekologické	ekologický	k2eAgFnSc2d1
stability	stabilita	k1gFnSc2
krajiny	krajina	k1gFnSc2
nadregionálního	nadregionální	k2eAgInSc2d1
významu	význam	k1gInSc2
na	na	k7c6
celém	celý	k2eAgNnSc6d1
území	území	k1gNnSc6
ČR	ČR	kA
</s>
<s>
Posuzuje	posuzovat	k5eAaImIp3nS
a	a	k8xC
vyplácí	vyplácet	k5eAaImIp3nS
újmy	újma	k1gFnPc4
vlastníkům	vlastník	k1gMnPc3
či	či	k8xC
nájemcům	nájemce	k1gMnPc3
za	za	k7c4
ztížení	ztížení	k1gNnSc4
zemědělského	zemědělský	k2eAgNnSc2d1
nebo	nebo	k8xC
lesnického	lesnický	k2eAgNnSc2d1
hospodaření	hospodaření	k1gNnSc2
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS
záchranné	záchranný	k2eAgInPc4d1
programy	program	k1gInPc4
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
a	a	k8xC
zajišťuje	zajišťovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
koordinaci	koordinace	k1gFnSc4
a	a	k8xC
realizaci	realizace	k1gFnSc4
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS
za	za	k7c4
přípravu	příprava	k1gFnSc4
a	a	k8xC
vymezení	vymezení	k1gNnSc4
soustavy	soustava	k1gFnSc2
Natura	Natura	k1gFnSc1
2000	#num#	k4
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Dokumentuje	dokumentovat	k5eAaBmIp3nS
stav	stav	k1gInSc4
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
viz	vidět	k5eAaImRp2nS
Nálezová	nálezový	k2eAgFnSc1d1
databáze	databáze	k1gFnSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
mapování	mapování	k1gNnSc2
biotopů	biotop	k1gInPc2
<g/>
,	,	kIx,
monitoring	monitoring	k1gInSc4
biotopů	biotop	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vede	vést	k5eAaImIp3nS
Informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS
odbornou	odborný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
veřejné	veřejný	k2eAgFnSc3d1
správě	správa	k1gFnSc3
</s>
<s>
Působí	působit	k5eAaImIp3nS
jako	jako	k9
tuzemský	tuzemský	k2eAgInSc1d1
vědecký	vědecký	k2eAgInSc1d1
orgán	orgán	k1gInSc1
Úmluvy	úmluva	k1gFnSc2
o	o	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodu	obchod	k1gInSc2
ohroženými	ohrožený	k2eAgInPc7d1
druhy	druh	k1gInPc7
volně	volně	k6eAd1
žijících	žijící	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
a	a	k8xC
planě	planě	k6eAd1
rostoucích	rostoucí	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
(	(	kIx(
<g/>
CITES	CITES	kA
<g/>
)	)	kIx)
a	a	k8xC
znalecký	znalecký	k2eAgInSc1d1
ústav	ústav	k1gInSc1
</s>
<s>
Věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
práci	práce	k1gFnSc4
s	s	k7c7
veřejností	veřejnost	k1gFnSc7
a	a	k8xC
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
časopis	časopis	k1gInSc1
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS
odbornou	odborný	k2eAgFnSc4d1
a	a	k8xC
expertní	expertní	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
MŽP	MŽP	kA
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
agendách	agenda	k1gFnPc6
včetně	včetně	k7c2
zastupování	zastupování	k1gNnSc2
státu	stát	k1gInSc2
před	před	k7c7
Evropskou	evropský	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
</s>
<s>
Vykonává	vykonávat	k5eAaImIp3nS
státní	státní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
na	na	k7c6
území	území	k1gNnSc6
24	#num#	k4
chráněných	chráněný	k2eAgFnPc2d1
krajinných	krajinný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
235	#num#	k4
národních	národní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
a	a	k8xC
národních	národní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
a	a	k8xC
549	#num#	k4
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
a	a	k8xC
památek	památka	k1gFnPc2
přímo	přímo	k6eAd1
v	v	k7c6
CHKO	CHKO	kA
(	(	kIx(
<g/>
údaj	údaj	k1gInSc1
ke	k	k7c3
dni	den	k1gInSc3
31.12	31.12	k4
<g/>
.2018	.2018	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
území	území	k1gNnPc1
pokrývají	pokrývat	k5eAaImIp3nP
téměř	téměř	k6eAd1
11	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
tedy	tedy	k9
necelých	celý	k2eNgInPc2d1
14	#num#	k4
%	%	kIx~
rozlohy	rozloha	k1gFnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
nejen	nejen	k6eAd1
spravuje	spravovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
o	o	k7c4
ně	on	k3xPp3gNnSc4
pečuje	pečovat	k5eAaImIp3nS
–	–	k?
hodnotí	hodnotit	k5eAaImIp3nS
jejich	jejich	k3xOp3gInSc4
stav	stav	k1gInSc4
<g/>
,	,	kIx,
zajišťuje	zajišťovat	k5eAaImIp3nS
management	management	k1gInSc4
a	a	k8xC
přípravu	příprava	k1gFnSc4
plánů	plán	k1gInPc2
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aktivně	aktivně	k6eAd1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
na	na	k7c4
péči	péče	k1gFnSc4
o	o	k7c4
krajinu	krajina	k1gFnSc4
–	–	k?
jak	jak	k8xC,k8xS
administrací	administrace	k1gFnSc7
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
na	na	k7c4
ni	on	k3xPp3gFnSc4
přispívají	přispívat	k5eAaImIp3nP
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
přímo	přímo	k6eAd1
v	v	k7c6
praxi	praxe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
prostřednictvím	prostřednictvím	k7c2
Programu	program	k1gInSc2
péče	péče	k1gFnSc2
o	o	k7c4
krajinu	krajina	k1gFnSc4
a	a	k8xC
programu	program	k1gInSc6
Podpora	podpora	k1gFnSc1
obnovy	obnova	k1gFnSc2
přirozených	přirozený	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
krajiny	krajina	k1gFnSc2
podporuje	podporovat	k5eAaImIp3nS
zejména	zejména	k9
výsadbu	výsadba	k1gFnSc4
rozptýlené	rozptýlený	k2eAgFnSc2d1
zeleně	zeleň	k1gFnSc2
v	v	k7c6
krajině	krajina	k1gFnSc6
<g/>
,	,	kIx,
protierozní	protierozní	k2eAgNnPc4d1
opatření	opatření	k1gNnPc4
<g/>
,	,	kIx,
revitalizaci	revitalizace	k1gFnSc4
toků	tok	k1gInPc2
<g/>
,	,	kIx,
obnovu	obnova	k1gFnSc4
mokřadů	mokřad	k1gInPc2
<g/>
,	,	kIx,
péči	péče	k1gFnSc4
o	o	k7c4
cenné	cenný	k2eAgFnPc4d1
nelesní	lesní	k2eNgFnPc4d1
lokality	lokalita	k1gFnPc4
<g/>
,	,	kIx,
zlepšování	zlepšování	k1gNnPc4
druhové	druhový	k2eAgFnSc2d1
a	a	k8xC
prostorové	prostorový	k2eAgFnSc2d1
skladby	skladba	k1gFnSc2
lesa	les	k1gInSc2
či	či	k8xC
vhodné	vhodný	k2eAgNnSc1d1
zpřístupňování	zpřístupňování	k1gNnSc1
přírodních	přírodní	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
návštěvníkům	návštěvník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS
záchranné	záchranný	k2eAgInPc4d1
programy	program	k1gInPc4
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
běží	běžet	k5eAaImIp3nS
deset	deset	k4xCc4
programů	program	k1gInPc2
–	–	k?
pro	pro	k7c4
matiznu	matizna	k1gFnSc4
bahenní	bahenní	k2eAgFnSc4d1
<g/>
,	,	kIx,
rdest	rdest	k1gInSc1
dlouholistý	dlouholistý	k2eAgInSc1d1
<g/>
,	,	kIx,
hvozdík	hvozdík	k1gInSc1
písečný	písečný	k2eAgInSc1d1
český	český	k2eAgInSc1d1
<g/>
,	,	kIx,
hořec	hořec	k1gInSc1
jarní	jarní	k2eAgInSc1d1
<g/>
,	,	kIx,
hořeček	hořeček	k1gInSc1
mnohotvarý	mnohotvarý	k2eAgInSc1d1
český	český	k2eAgInSc1d1
<g/>
,	,	kIx,
perlorodku	perlorodka	k1gFnSc4
říční	říční	k2eAgFnSc4d1
<g/>
,	,	kIx,
hnědáska	hnědásek	k1gMnSc2
osikového	osikový	k2eAgMnSc2d1
<g/>
,	,	kIx,
sysla	sysel	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
<g/>
,	,	kIx,
užovku	užovka	k1gFnSc4
stromovou	stromový	k2eAgFnSc4d1
<g/>
,	,	kIx,
vydru	vydra	k1gFnSc4
říční	říční	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
tuzemským	tuzemský	k2eAgMnSc7d1
odborným	odborný	k2eAgMnSc7d1
garantem	garant	k1gMnSc7
evropské	evropský	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
Natura	Natura	k1gFnSc1
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nS
41	#num#	k4
ptačích	ptačí	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
a	a	k8xC
1112	#num#	k4
evropsky	evropsky	k6eAd1
významných	významný	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
zajišťuje	zajišťovat	k5eAaImIp3nS
evidenci	evidence	k1gFnSc4
a	a	k8xC
sledování	sledování	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
expertní	expertní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
zastupuje	zastupovat	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
u	u	k7c2
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
vydala	vydat	k5eAaPmAgFnS
více	hodně	k6eAd2
než	než	k8xS
12000	#num#	k4
závazných	závazný	k2eAgNnPc2d1
stanovisek	stanovisko	k1gNnPc2
a	a	k8xC
rozhodnutí	rozhodnutí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poskytuje	poskytovat	k5eAaImIp3nS
odbornou	odborný	k2eAgFnSc4d1
a	a	k8xC
informační	informační	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
orgánům	orgán	k1gInPc3
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vede	vést	k5eAaImIp3nS
Informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
nálezové	nálezový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
soustřeďuje	soustřeďovat	k5eAaImIp3nS
informace	informace	k1gFnPc4
o	o	k7c6
výskytu	výskyt	k1gInSc6
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
6,7	6,7	k4
milionu	milion	k4xCgInSc2
údajů	údaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
i	i	k9
práci	práce	k1gFnSc4
s	s	k7c7
veřejností	veřejnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořádá	pořádat	k5eAaImIp3nS
akce	akce	k1gFnSc1
<g/>
,	,	kIx,
zaměřené	zaměřený	k2eAgNnSc1d1
zejména	zejména	k9
na	na	k7c6
poznávání	poznávání	k1gNnSc6
Vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
časopis	časopis	k1gInSc1
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
sborník	sborník	k1gInSc1
Příroda	příroda	k1gFnSc1
a	a	k8xC
řadu	řada	k1gFnSc4
dalších	další	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
pro	pro	k7c4
odbornou	odborný	k2eAgFnSc4d1
i	i	k8xC
širokou	široký	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
člena	člen	k1gMnSc4
řady	řada	k1gFnSc2
mezinárodních	mezinárodní	k2eAgNnPc2d1
uskupení	uskupení	k1gNnPc2
-	-	kIx~
Mezinárodní	mezinárodní	k2eAgFnSc1d1
unie	unie	k1gFnSc1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
IUCN	IUCN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Federace	federace	k1gFnSc1
Europarc	Europarc	k1gFnSc1
<g/>
,	,	kIx,
Planta	planta	k1gFnSc1
Europa	Europa	k1gFnSc1
<g/>
,	,	kIx,
European	European	k1gInSc1
Network	network	k1gInSc1
of	of	k?
Heads	Heads	k1gInSc1
of	of	k?
Nature	Natur	k1gMnSc5
Conservation	Conservation	k1gInSc1
Agencies	Agencies	k1gMnSc1
(	(	kIx(
<g/>
ENCA	ENCA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
partnerem	partner	k1gMnSc7
prestižního	prestižní	k2eAgNnSc2d1
odborného	odborný	k2eAgNnSc2d1
Evropského	evropský	k2eAgNnSc2d1
tematického	tematický	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
biologické	biologický	k2eAgFnSc2d1
rozmanitosti	rozmanitost	k1gFnSc2
(	(	kIx(
<g/>
ETC	ETC	kA
<g/>
/	/	kIx~
<g/>
BD	BD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
expertní	expertní	k2eAgFnSc7d1
institucí	instituce	k1gFnSc7
EEA	EEA	kA
<g/>
/	/	kIx~
<g/>
EK	EK	kA
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
ústředí	ústředí	k1gNnSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
14	#num#	k4
regionálních	regionální	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
byla	být	k5eAaImAgFnS
státní	státní	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
společně	společně	k6eAd1
s	s	k7c7
památkovou	památkový	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
a	a	k8xC
správou	správa	k1gFnSc7
jeskyní	jeskyně	k1gFnSc7
součástí	součást	k1gFnSc7
resortu	resort	k1gInSc2
kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
Státní	státní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
památkové	památkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
Český	český	k2eAgInSc1d1
ústav	ústav	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
rozdělen	rozdělit	k5eAaPmNgMnS
na	na	k7c4
Agenturu	agentura	k1gFnSc4
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
(	(	kIx(
<g/>
pod	pod	k7c7
níž	jenž	k3xRgFnSc7
spadala	spadat	k5eAaPmAgFnS,k5eAaImAgFnS
i	i	k9
správa	správa	k1gFnSc1
jeskyní	jeskyně	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Správu	správa	k1gFnSc4
chráněných	chráněný	k2eAgFnPc2d1
krajinných	krajinný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
ČR	ČR	kA
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Správu	správa	k1gFnSc4
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
poslední	poslední	k2eAgFnSc3d1
změně	změna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
nové	nový	k2eAgNnSc4d1
územní	územní	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
státu	stát	k1gInSc2
a	a	k8xC
environmentální	environmentální	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
člena	člen	k1gMnSc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
byly	být	k5eAaImAgFnP
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
Správa	správa	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
spojeny	spojit	k5eAaPmNgInP
do	do	k7c2
jedné	jeden	k4xCgFnSc2
instituce	instituce	k1gFnSc2
pod	pod	k7c7
staronovým	staronový	k2eAgInSc7d1
názvem	název	k1gInSc7
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
jeskyní	jeskyně	k1gFnPc2
byla	být	k5eAaImAgFnS
vyčleněna	vyčlenit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
samostatný	samostatný	k2eAgInSc4d1
subjekt	subjekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Nálezová	nálezový	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
</s>
<s>
Mapování	mapování	k1gNnSc1
biotopů	biotop	k1gInPc2
</s>
<s>
Monitoring	monitoring	k1gInSc1
biotopů	biotop	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
<g/>
:	:	kIx,
http://www.nature.cz/	http://www.nature.cz/	k?
</s>
<s>
http://www.dotace.nature.cz/	http://www.dotace.nature.cz/	k?
</s>
<s>
http://www.zachranneprogramy.cz/	http://www.zachranneprogramy.cz/	k?
</s>
<s>
http://www.natura2000.cz/	http://www.natura2000.cz/	k4
</s>
<s>
http://www.biomonitoring.cz/	http://www.biomonitoring.cz/	k?
</s>
<s>
http://drusop.nature.cz/	http://drusop.nature.cz/	k?
</s>
<s>
http://portal.nature.cz/	http://portal.nature.cz/	k?
</s>
<s>
http://www.casopis.ochranaprirody.cz/	http://www.casopis.ochranaprirody.cz/	k?
</s>
<s>
http://standardy.nature.cz/	http://standardy.nature.cz/	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2002148930	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2230	#num#	k4
576X	576X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2011005280	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
154091409	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2011005280	#num#	k4
</s>
