<s>
Pierre	Pierr	k1gMnSc5	Pierr
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Jižní	jižní	k2eAgFnSc2d1	jižní
Dakoty	Dakota	k1gFnSc2	Dakota
<g/>
,	,	kIx,	,
státu	stát	k1gInSc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
33,7	[number]	k4	33,7
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Missouri	Missouri	k1gFnSc2	Missouri
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
13	[number]	k4	13
646	[number]	k4	646
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
85,1	[number]	k4	85,1
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
10,9	[number]	k4	10,9
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,4	[number]	k4	2,4
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g />
.	.	kIx.	.
</s>
<s>
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
1,9	[number]	k4	1,9
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
0-18	[number]	k4	0-18
...	...	k?	...
27,2	[number]	k4	27,2
%	%	kIx~	%
18-24	[number]	k4	18-24
...	...	k?	...
6,5	[number]	k4	6,5
%	%	kIx~	%
25-44	[number]	k4	25-44
...	...	k?	...
28,6	[number]	k4	28,6
%	%	kIx~	%
45-64	[number]	k4	45-64
...	...	k?	...
23,6	[number]	k4	23,6
%	%	kIx~	%
65	[number]	k4	65
<g/>
+	+	kIx~	+
...	...	k?	...
14,1	[number]	k4	14,1
%	%	kIx~	%
HDP	HDP	kA	HDP
na	na	k7c4	na
1	[number]	k4	1
obyvatele	obyvatel	k1gMnSc4	obyvatel
je	být	k5eAaImIp3nS	být
$	$	kIx~	$
<g/>
20	[number]	k4	20
462	[number]	k4	462
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
každé	každý	k3xTgFnSc2	každý
domácnosti	domácnost	k1gFnSc2	domácnost
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
$	$	kIx~	$
<g/>
42	[number]	k4	42
962	[number]	k4	962
<g/>
,	,	kIx,	,
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
rodiny	rodina	k1gFnSc2	rodina
je	být	k5eAaImIp3nS	být
$	$	kIx~	$
<g/>
52	[number]	k4	52
144	[number]	k4	144
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
vydělávají	vydělávat	k5eAaImIp3nP	vydělávat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
$	$	kIx~	$
<g/>
32	[number]	k4	32
969	[number]	k4	969
oproti	oproti	k7c3	oproti
průměrným	průměrný	k2eAgMnPc3d1	průměrný
$	$	kIx~	$
<g/>
22	[number]	k4	22
865	[number]	k4	865
které	který	k3yQgNnSc1	který
vydělají	vydělat	k5eAaPmIp3nP	vydělat
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
7,8	[number]	k4	7,8
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Pierre	Pierr	k1gInSc5	Pierr
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
7,9	[number]	k4	7,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
mladších	mladý	k2eAgMnPc2d2	mladší
18	[number]	k4	18
let	let	k1gInSc4	let
a	a	k8xC	a
9,2	[number]	k4	9,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
starších	starý	k2eAgMnPc2d2	starší
65	[number]	k4	65
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pierre	Pierr	k1gInSc5	Pierr
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
komora	komora	k1gFnSc1	komora
města	město	k1gNnSc2	město
Pierre	Pierr	k1gInSc5	Pierr
(	(	kIx(	(
<g/>
Chamber	Chamber	k1gInSc1	Chamber
of	of	k?	of
Commerce	Commerka	k1gFnSc3	Commerka
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Capital	Capital	k1gMnSc1	Capital
Journal	Journal	k1gMnSc1	Journal
-	-	kIx~	-
Pierrské	Pierrský	k2eAgFnPc1d1	Pierrský
noviny	novina	k1gFnPc1	novina
</s>
