<p>
<s>
Ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
depleted	depleted	k1gMnSc1	depleted
uranium	uranium	k1gNnSc1	uranium
–	–	k?	–
DU	DU	k?	DU
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
tulalloy	tulalloa	k1gFnSc2	tulalloa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uran	uran	k1gInSc1	uran
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
izotopu	izotop	k1gInSc2	izotop
235U	[number]	k4	235U
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
0,23	[number]	k4	0,23
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Přídomek	přídomek	k1gInSc1	přídomek
"	"	kIx"	"
<g/>
ochuzený	ochuzený	k2eAgMnSc1d1	ochuzený
<g/>
"	"	kIx"	"
získal	získat	k5eAaPmAgMnS	získat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
oproti	oproti	k7c3	oproti
přírodnímu	přírodní	k2eAgInSc3d1	přírodní
uranu	uran	k1gInSc3	uran
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
0,7	[number]	k4	0,7
%	%	kIx~	%
235U	[number]	k4	235U
zbaven	zbavit	k5eAaPmNgMnS	zbavit
podstatné	podstatný	k2eAgFnPc4d1	podstatná
části	část	k1gFnPc4	část
tohoto	tento	k3xDgInSc2	tento
izotopu	izotop	k1gInSc2	izotop
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
obohaceného	obohacený	k2eAgInSc2d1	obohacený
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
má	mít	k5eAaImIp3nS	mít
nejaderné	jaderný	k2eNgNnSc4d1	nejaderné
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
civilním	civilní	k2eAgInSc6d1	civilní
i	i	k8xC	i
vojenském	vojenský	k2eAgInSc6d1	vojenský
sektoru	sektor	k1gInSc6	sektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
má	mít	k5eAaImIp3nS	mít
hustotu	hustota	k1gFnSc4	hustota
19,07	[number]	k4	19,07
g.	g.	k?	g.
<g/>
cm-	cm-	k?	cm-
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
1,7	[number]	k4	1,7
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
hustota	hustota	k1gFnSc1	hustota
než	než	k8xS	než
olovo	olovo	k1gNnSc1	olovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reaguje	reagovat	k5eAaBmIp3nS	reagovat
stejně	stejně	k6eAd1	stejně
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
nekovovými	kovový	k2eNgInPc7d1	nekovový
prvky	prvek	k1gInPc7	prvek
jako	jako	k8xC	jako
kovový	kovový	k2eAgInSc4d1	kovový
uran	uran	k1gInSc4	uran
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kovové	kovový	k2eAgFnSc2d1	kovová
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práškové	práškový	k2eAgFnSc6d1	prášková
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
spontánně	spontánně	k6eAd1	spontánně
odpařuje	odpařovat	k5eAaImIp3nS	odpařovat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
700	[number]	k4	700
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
wolframu	wolfram	k1gInSc2	wolfram
či	či	k8xC	či
jiných	jiná	k1gFnPc2	jiná
jeho	jeho	k3xOp3gFnPc2	jeho
alternativ	alternativa	k1gFnPc2	alternativa
je	být	k5eAaImIp3nS	být
získávání	získávání	k1gNnSc1	získávání
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
poměrně	poměrně	k6eAd1	poměrně
levné	levný	k2eAgNnSc1d1	levné
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgMnSc1d1	dostupný
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
množstvích	množství	k1gNnPc6	množství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Získávání	získávání	k1gNnSc2	získávání
==	==	k?	==
</s>
</p>
<p>
<s>
Ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
je	být	k5eAaImIp3nS	být
odpadní	odpadní	k2eAgInSc4d1	odpadní
produkt	produkt	k1gInSc4	produkt
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
obohacování	obohacování	k1gNnSc2	obohacování
přírodního	přírodní	k2eAgInSc2d1	přírodní
uranu	uran	k1gInSc2	uran
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
a	a	k8xC	a
v	v	k7c6	v
jaderných	jaderný	k2eAgFnPc6d1	jaderná
zbraních	zbraň	k1gFnPc6	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zásoby	zásoba	k1gFnSc2	zásoba
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
WISE	WISE	kA	WISE
Uranium	uranium	k1gNnSc1	uranium
Project	Project	k1gInSc1	Project
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hustotu	hustota	k1gFnSc4	hustota
využíván	využíván	k2eAgMnSc1d1	využíván
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
vyvážení	vyvážení	k1gNnSc4	vyvážení
<g/>
,	,	kIx,	,
nutnost	nutnost	k1gFnSc4	nutnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
kinetické	kinetický	k2eAgFnPc4d1	kinetická
energie	energie	k1gFnPc4	energie
při	při	k7c6	při
malém	malý	k2eAgInSc6d1	malý
objemu	objem	k1gInSc6	objem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
využití	využití	k1gNnSc1	využití
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
zbraní	zbraň	k1gFnPc2	zbraň
je	být	k5eAaImIp3nS	být
DU	DU	k?	DU
činí	činit	k5eAaImIp3nP	činit
extrémně	extrémně	k6eAd1	extrémně
tvrdými	tvrdý	k2eAgMnPc7d1	tvrdý
a	a	k8xC	a
schopnými	schopný	k2eAgMnPc7d1	schopný
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
wolframu	wolfram	k1gInSc2	wolfram
se	se	k3xPyFc4	se
ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
protipancéřových	protipancéřový	k2eAgInPc2d1	protipancéřový
projektilů	projektil	k1gInPc2	projektil
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
šípové	šípový	k2eAgFnPc1d1	šípová
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
podkaliberní	podkalibernit	k5eAaPmIp3nS	podkalibernit
střely	střela	k1gFnPc4	střela
–	–	k?	–
průměr	průměr	k1gInSc1	průměr
střely	střela	k1gFnSc2	střela
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
hlavně	hlavně	k6eAd1	hlavně
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
vystřelena	vystřelen	k2eAgFnSc1d1	vystřelena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
sice	sice	k8xC	sice
především	především	k6eAd1	především
vysoká	vysoký	k2eAgFnSc1d1	vysoká
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
účinek	účinek	k1gInSc1	účinek
však	však	k9	však
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
průniku	průnik	k1gInSc6	průnik
projektilu	projektil	k1gInSc2	projektil
za	za	k7c4	za
pancíř	pancíř	k1gInSc4	pancíř
se	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
třením	tření	k1gNnSc7	tření
rozžhavené	rozžhavený	k2eAgFnSc2d1	rozžhavená
úlomky	úlomek	k1gInPc7	úlomek
uranu	uran	k1gInSc2	uran
vznítí	vznítit	k5eAaPmIp3nP	vznítit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
ničivý	ničivý	k2eAgInSc1d1	ničivý
účinek	účinek	k1gInSc1	účinek
uvnitř	uvnitř	k7c2	uvnitř
obrněného	obrněný	k2eAgInSc2d1	obrněný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Střely	střela	k1gFnPc1	střela
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
též	též	k9	též
mají	mít	k5eAaImIp3nP	mít
výhodnější	výhodný	k2eAgInSc4d2	výhodnější
mezní	mezní	k2eAgInSc4d1	mezní
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yQgNnSc7	který
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
opancéřovaného	opancéřovaný	k2eAgInSc2d1	opancéřovaný
cíle	cíl	k1gInSc2	cíl
pronikne	proniknout	k5eAaPmIp3nS	proniknout
a	a	k8xC	a
neodrazí	odrazit	k5eNaPmIp3nS	odrazit
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
na	na	k7c4	na
výplně	výplň	k1gFnPc4	výplň
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
kanónů	kanón	k1gInPc2	kanón
v	v	k7c6	v
letadlech	letadlo	k1gNnPc6	letadlo
A-10	A-10	k1gFnSc2	A-10
nebo	nebo	k8xC	nebo
do	do	k7c2	do
hrotů	hrot	k1gInPc2	hrot
balistických	balistický	k2eAgFnPc2d1	balistická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nasazení	nasazení	k1gNnSc1	nasazení
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
munice	munice	k1gFnSc2	munice
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
v	v	k7c6	v
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
konfliktech	konflikt	k1gInPc6	konflikt
====	====	k?	====
</s>
</p>
<p>
<s>
Střely	střela	k1gFnPc1	střela
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
spojenci	spojenec	k1gMnPc1	spojenec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
od	od	k7c2	od
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
i	i	k8xC	i
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
během	během	k7c2	během
operace	operace	k1gFnSc2	operace
Irácká	irácký	k2eAgFnSc1d1	irácká
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
irácké	irácký	k2eAgFnSc2d1	irácká
ministryně	ministryně	k1gFnSc2	ministryně
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
Nermin	Nermin	k2eAgMnSc1d1	Nermin
Othman	Othman	k1gMnSc1	Othman
bombardováním	bombardování	k1gNnPc3	bombardování
kontaminováno	kontaminovat	k5eAaBmNgNnS	kontaminovat
přes	přes	k7c4	přes
350	[number]	k4	350
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
Američané	Američan	k1gMnPc1	Američan
vypálili	vypálit	k5eAaPmAgMnP	vypálit
milion	milion	k4xCgInSc4	milion
těchto	tento	k3xDgFnPc2	tento
střel	střela	k1gFnPc2	střela
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc4d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
v	v	k7c6	v
obydlených	obydlený	k2eAgFnPc6d1	obydlená
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
proti	proti	k7c3	proti
neobrněným	obrněný	k2eNgInPc3d1	obrněný
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
rakovinných	rakovinný	k2eAgNnPc2d1	rakovinné
onemocnění	onemocnění	k1gNnPc2	onemocnění
údajně	údajně	k6eAd1	údajně
činí	činit	k5eAaImIp3nS	činit
140	[number]	k4	140
tisíc	tisíc	k4xCgInPc2	tisíc
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
7	[number]	k4	7
až	až	k9	až
8	[number]	k4	8
tisíc	tisíc	k4xCgInSc4	tisíc
případů	případ	k1gInPc2	případ
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
bývalé	bývalý	k2eAgFnSc2d1	bývalá
kongresmanky	kongresmanka	k1gFnSc2	kongresmanka
a	a	k8xC	a
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
pozorovatelky	pozorovatelka	k1gFnSc2	pozorovatelka
Cynthie	Cynthie	k1gFnSc2	Cynthie
McKinneyové	McKinneyové	k2eAgInPc2d1	McKinneyové
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
ochuzený	ochuzený	k2eAgInSc4d1	ochuzený
uran	uran	k1gInSc4	uran
používala	používat	k5eAaImAgFnS	používat
vojska	vojsko	k1gNnSc2	vojsko
NATO	NATO	kA	NATO
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
provedla	provést	k5eAaPmAgFnS	provést
americká	americký	k2eAgFnSc1d1	americká
letadla	letadlo	k1gNnPc1	letadlo
A-10	A-10	k1gFnPc2	A-10
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Sýrii	Sýrie	k1gFnSc6	Sýrie
dva	dva	k4xCgInPc4	dva
nálety	nálet	k1gInPc4	nálet
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
zničení	zničení	k1gNnSc4	zničení
obchodu	obchod	k1gInSc2	obchod
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Magazínu	magazín	k1gInSc3	magazín
Foreign	Foreign	k1gInSc1	Foreign
Policy	Polica	k1gFnPc4	Polica
Američané	Američan	k1gMnPc1	Američan
později	pozdě	k6eAd2	pozdě
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
útocích	útok	k1gInPc6	útok
použili	použít	k5eAaPmAgMnP	použít
munici	munice	k1gFnSc4	munice
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dvou	dva	k4xCgInPc6	dva
náletech	nálet	k1gInPc6	nálet
na	na	k7c4	na
cisterny	cisterna	k1gFnPc4	cisterna
džihádistů	džihádista	k1gMnPc2	džihádista
vypálili	vypálit	k5eAaPmAgMnP	vypálit
6	[number]	k4	6
320	[number]	k4	320
střel	střela	k1gFnPc2	střela
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
5	[number]	k4	5
265	[number]	k4	265
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnPc4	využití
mimo	mimo	k7c4	mimo
vojenství	vojenství	k1gNnSc4	vojenství
===	===	k?	===
</s>
</p>
<p>
<s>
DU	DU	k?	DU
používá	používat	k5eAaImIp3nS	používat
často	často	k6eAd1	často
i	i	k9	i
na	na	k7c4	na
vyvážení	vyvážení	k1gNnSc4	vyvážení
v	v	k7c6	v
leteckém	letecký	k2eAgInSc6d1	letecký
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
jako	jako	k8xS	jako
vhodná	vhodný	k2eAgFnSc1d1	vhodná
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
rentgenovým	rentgenový	k2eAgNnSc7d1	rentgenové
zářením	záření	k1gNnSc7	záření
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
kontejnerů	kontejner	k1gInPc2	kontejner
k	k	k7c3	k
transportu	transport	k1gInSc3	transport
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
DU	DU	k?	DU
jako	jako	k8xS	jako
kov	kov	k1gInSc1	kov
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starším	starý	k2eAgInSc6d2	starší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
používaném	používaný	k2eAgInSc6d1	používaný
Boeingu	boeing	k1gInSc6	boeing
747	[number]	k4	747
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
vyrovnávací	vyrovnávací	k2eAgNnSc1d1	vyrovnávací
závaží	závaží	k1gNnSc1	závaží
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
kolem	kolem	k7c2	kolem
600	[number]	k4	600
exemplářů	exemplář	k1gInPc2	exemplář
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
obsahujícího	obsahující	k2eAgMnSc2d1	obsahující
ochuzený	ochuzený	k2eAgInSc4d1	ochuzený
uran	uran	k1gInSc4	uran
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
exempláře	exemplář	k1gInPc1	exemplář
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
400	[number]	k4	400
–	–	k?	–
600	[number]	k4	600
kg	kg	kA	kg
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
pramen	pramen	k1gInSc1	pramen
uvádí	uvádět	k5eAaImIp3nS	uvádět
dokonce	dokonce	k9	dokonce
400	[number]	k4	400
–	–	k?	–
1500	[number]	k4	1500
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsoben	způsobit	k5eAaPmNgInS	způsobit
je	být	k5eAaImIp3nS	být
využíván	využíván	k2eAgInSc1d1	využíván
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
letadlech	letadlo	k1gNnPc6	letadlo
McDonnell	McDonnell	k1gInSc4	McDonnell
Douglas	Douglas	k1gInSc1	Douglas
DC-	DC-	k1gFnSc1	DC-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zátěž	zátěž	k1gFnSc1	zátěž
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
plachetnicích	plachetnice	k1gFnPc6	plachetnice
<g/>
,	,	kIx,	,
rotorech	rotor	k1gInPc6	rotor
gyroskopů	gyroskop	k1gInPc2	gyroskop
<g/>
,	,	kIx,	,
ropných	ropný	k2eAgFnPc6d1	ropná
vrtných	vrtný	k2eAgFnPc6d1	vrtná
soupravách	souprava	k1gFnPc6	souprava
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
dokonce	dokonce	k9	dokonce
i	i	k9	i
ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
amerických	americký	k2eAgInPc6d1	americký
tancích	tanec	k1gInPc6	tanec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
M1	M1	k1gFnSc1	M1
Abrams	Abrams	k1gInSc1	Abrams
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
pancíře	pancíř	k1gInSc2	pancíř
<g/>
.	.	kIx.	.
</s>
<s>
Ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
rovněž	rovněž	k9	rovněž
jako	jako	k8xS	jako
stínění	stínění	k1gNnSc1	stínění
před	před	k7c7	před
radioaktivitou	radioaktivita	k1gFnSc7	radioaktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sloučeniny	sloučenina	k1gFnPc4	sloučenina
hexahydrát	hexahydrát	k1gInSc4	hexahydrát
diurananu	diuranan	k1gInSc2	diuranan
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7.6	[number]	k4	7.6
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
a	a	k8xC	a
hexahydrát	hexahydrát	k1gInSc1	hexahydrát
diurananu	diuranan	k1gInSc2	diuranan
draselného	draselný	k2eAgInSc2d1	draselný
(	(	kIx(	(
<g/>
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7.6	[number]	k4	7.6
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xS	jako
uranová	uranový	k2eAgFnSc1d1	uranová
žluť	žluť	k1gFnSc1	žluť
používající	používající	k2eAgFnSc1d1	používající
se	se	k3xPyFc4	se
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
glazur	glazura	k1gFnPc2	glazura
a	a	k8xC	a
porcelánu	porcelán	k1gInSc2	porcelán
(	(	kIx(	(
<g/>
barví	barvit	k5eAaImIp3nS	barvit
na	na	k7c4	na
žluto	žluto	k1gNnSc4	žluto
až	až	k9	až
žlutozeleno	žlutozelen	k2eAgNnSc1d1	žlutozelen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
fluoreskuje	fluoreskovat	k5eAaImIp3nS	fluoreskovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
však	však	k9	však
dá	dát	k5eAaPmIp3nS	dát
barvit	barvit	k5eAaImF	barvit
i	i	k9	i
oranžově	oranžově	k6eAd1	oranžově
až	až	k9	až
rudě	rudě	k6eAd1	rudě
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
tohoto	tento	k3xDgNnSc2	tento
použití	použití	k1gNnSc2	použití
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
výrazně	výrazně	k6eAd1	výrazně
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Státního	státní	k2eAgInSc2d1	státní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
SÚJB	SÚJB	kA	SÚJB
<g/>
)	)	kIx)	)
2	[number]	k4	2
výrobci	výrobce	k1gMnPc1	výrobce
skla	sklo	k1gNnSc2	sklo
barveného	barvené	k1gNnSc2	barvené
uranem	uran	k1gInSc7	uran
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyráběné	vyráběný	k2eAgNnSc4d1	vyráběné
sklo	sklo	k1gNnSc4	sklo
zdravotně	zdravotně	k6eAd1	zdravotně
zcela	zcela	k6eAd1	zcela
neškodné	škodný	k2eNgNnSc1d1	neškodné
i	i	k9	i
při	při	k7c6	při
silně	silně	k6eAd1	silně
konzervativním	konzervativní	k2eAgInSc6d1	konzervativní
přístupu	přístup	k1gInSc6	přístup
hodnocení	hodnocení	k1gNnSc2	hodnocení
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
se	s	k7c7	s
sloučenin	sloučenina	k1gFnPc2	sloučenina
(	(	kIx(	(
<g/>
solí	sůl	k1gFnPc2	sůl
<g/>
)	)	kIx)	)
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
např.	např.	kA	např.
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
–	–	k?	–
dusičnan	dusičnan	k1gInSc1	dusičnan
uranylu	uranyl	k1gInSc2	uranyl
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
zesilování	zesilování	k1gNnSc3	zesilování
negativů	negativ	k1gInPc2	negativ
<g/>
,	,	kIx,	,
do	do	k7c2	do
tónovacích	tónovací	k2eAgFnPc2d1	tónovací
lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
zesilovač	zesilovač	k1gInSc4	zesilovač
světlotisku	světlotisk	k1gInSc2	světlotisk
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
chemické	chemický	k2eAgFnSc3d1	chemická
toxicitě	toxicita	k1gFnSc3	toxicita
se	se	k3xPyFc4	se
dusičnan	dusičnan	k1gInSc1	dusičnan
uranylu	uranyl	k1gInSc2	uranyl
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
experimentální	experimentální	k2eAgNnPc4d1	experimentální
vyvolání	vyvolání	k1gNnSc4	vyvolání
patologického	patologický	k2eAgInSc2d1	patologický
stavu	stav	k1gInSc2	stav
ledvin	ledvina	k1gFnPc2	ledvina
u	u	k7c2	u
pokusných	pokusný	k2eAgNnPc2d1	pokusné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Octan	octan	k1gInSc1	octan
uranylu	uranyl	k1gInSc2	uranyl
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2.2	[number]	k4	2.2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
NaUO	NaUO	k1gFnSc1	NaUO
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
diuranan	diuranan	k1gMnSc1	diuranan
amonný	amonný	k2eAgMnSc1d1	amonný
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
7	[number]	k4	7
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uran	Uran	k1gInSc1	Uran
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
karbidu	karbid	k1gInSc2	karbid
je	být	k5eAaImIp3nS	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
amoniaku	amoniak	k1gInSc2	amoniak
Haberovým	Haberův	k2eAgInSc7d1	Haberův
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
==	==	k?	==
</s>
</p>
<p>
<s>
Škodlivé	škodlivý	k2eAgInPc1d1	škodlivý
účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
především	především	k6eAd1	především
inhalací	inhalace	k1gFnSc7	inhalace
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
chemickou	chemický	k2eAgFnSc7d1	chemická
toxicitou	toxicita	k1gFnSc7	toxicita
(	(	kIx(	(
<g/>
pronikání	pronikání	k1gNnSc4	pronikání
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
poškození	poškození	k1gNnSc3	poškození
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
vyvolání	vyvolání	k1gNnSc4	vyvolání
rakoviny	rakovina	k1gFnSc2	rakovina
plic	plíce	k1gFnPc2	plíce
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vdechnout	vdechnout	k5eAaPmF	vdechnout
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
uranového	uranový	k2eAgInSc2d1	uranový
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
rakoviny	rakovina	k1gFnSc2	rakovina
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc4	riziko
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
listu	list	k1gInSc2	list
The	The	k1gFnSc1	The
Independent	independent	k1gMnSc1	independent
byla	být	k5eAaImAgFnS	být
úroveň	úroveň	k1gFnSc4	úroveň
dětské	dětský	k2eAgFnSc2d1	dětská
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
<g/>
,	,	kIx,	,
rakoviny	rakovina	k1gFnSc2	rakovina
a	a	k8xC	a
leukémie	leukémie	k1gFnSc2	leukémie
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Fallúdža	Fallúdžum	k1gNnSc2	Fallúdžum
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
Hirošimě	Hirošima	k1gFnSc6	Hirošima
po	po	k7c6	po
shození	shození	k1gNnSc6	shození
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
neoficiálních	neoficiální	k2eAgFnPc2d1	neoficiální
zpráv	zpráva	k1gFnPc2	zpráva
trpí	trpět	k5eAaImIp3nP	trpět
děti	dítě	k1gFnPc4	dítě
ve	v	k7c6	v
Fallúdži	Fallúdž	k1gFnSc6	Fallúdž
především	především	k9	především
srdečními	srdeční	k2eAgFnPc7d1	srdeční
poruchami	porucha	k1gFnPc7	porucha
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
až	až	k9	až
13	[number]	k4	13
krát	krát	k6eAd1	krát
vyšší	vysoký	k2eAgInPc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
také	také	k9	také
počet	počet	k1gInSc1	počet
vrozených	vrozený	k2eAgInPc2d1	vrozený
defektů	defekt	k1gInPc2	defekt
ve	v	k7c6	v
Fallúdži	Fallúdž	k1gFnSc6	Fallúdž
podle	podle	k7c2	podle
místních	místní	k2eAgMnPc2d1	místní
lékařů	lékař	k1gMnPc2	lékař
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
potvrzení	potvrzení	k1gNnSc4	potvrzení
těchto	tento	k3xDgInPc2	tento
svědectví	svědectví	k1gNnPc4	svědectví
však	však	k9	však
chybí	chybět	k5eAaImIp3nP	chybět
jasné	jasný	k2eAgInPc1d1	jasný
důkazy	důkaz	k1gInPc1	důkaz
a	a	k8xC	a
výzkumy	výzkum	k1gInPc1	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Irácká	irácký	k2eAgFnSc1d1	irácká
vláda	vláda	k1gFnSc1	vláda
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
jen	jen	k9	jen
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
případy	případ	k1gInPc4	případ
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
expertní	expertní	k2eAgFnPc1d1	expertní
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
nárůst	nárůst	k1gInSc4	nárůst
vrozených	vrozený	k2eAgInPc2d1	vrozený
defektů	defekt	k1gInPc2	defekt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc1	jejich
souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
munice	munice	k1gFnSc2	munice
s	s	k7c7	s
ochuzeným	ochuzený	k2eAgInSc7d1	ochuzený
uranem	uran	k1gInSc7	uran
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
důrazně	důrazně	k6eAd1	důrazně
upozornit	upozornit	k5eAaPmF	upozornit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
použití	použití	k1gNnSc4	použití
jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
nemá	mít	k5eNaImIp3nS	mít
absolutně	absolutně	k6eAd1	absolutně
žádnou	žádný	k3yNgFnSc4	žádný
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
jaderným	jaderný	k2eAgNnSc7d1	jaderné
využíváním	využívání	k1gNnSc7	využívání
uranu	uran	k1gInSc2	uran
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
nukleárních	nukleární	k2eAgFnPc2d1	nukleární
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
materiálem	materiál	k1gInSc7	materiál
wolframem	wolfram	k1gInSc7	wolfram
<g/>
,	,	kIx,	,
snadná	snadný	k2eAgFnSc1d1	snadná
vznětlivost	vznětlivost	k1gFnSc1	vznětlivost
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
poměrně	poměrně	k6eAd1	poměrně
nízkou	nízký	k2eAgFnSc4d1	nízká
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
238U	[number]	k4	238U
však	však	k9	však
přesto	přesto	k6eAd1	přesto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
slabému	slabý	k2eAgNnSc3d1	slabé
radioaktivnímu	radioaktivní	k2eAgNnSc3d1	radioaktivní
zamoření	zamoření	k1gNnSc3	zamoření
<g/>
,	,	kIx,	,
míra	míra	k1gFnSc1	míra
jeho	jeho	k3xOp3gFnSc2	jeho
neškodnosti	neškodnost	k1gFnSc2	neškodnost
nebo	nebo	k8xC	nebo
škodlivosti	škodlivost	k1gFnSc2	škodlivost
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
dořešena	dořešit	k5eAaPmNgFnS	dořešit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
padlo	padnout	k5eAaImAgNnS	padnout
na	na	k7c6	na
Valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
OSN	OSN	kA	OSN
několik	několik	k4yIc4	několik
návrhů	návrh	k1gInPc2	návrh
na	na	k7c6	na
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
účinků	účinek	k1gInPc2	účinek
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
munice	munice	k1gFnSc2	munice
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
několika	několik	k4yIc2	několik
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
schváleny	schválit	k5eAaPmNgInP	schválit
<g/>
.	.	kIx.	.
<g/>
Větší	veliký	k2eAgInPc1d2	veliký
<g />
.	.	kIx.	.
</s>
<s>
roli	role	k1gFnSc4	role
přitom	přitom	k6eAd1	přitom
hraje	hrát	k5eAaImIp3nS	hrát
ani	ani	k8xC	ani
ne	ne	k9	ne
tak	tak	k9	tak
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
dovnitř	dovnitř	k7c2	dovnitř
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
účinky	účinek	k1gInPc1	účinek
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
toxicita	toxicita	k1gFnSc1	toxicita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uran	uran	k1gInSc1	uran
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
organizmy	organizmus	k1gInPc4	organizmus
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
velké	velký	k2eAgNnSc1d1	velké
rozptýlení	rozptýlení	k1gNnSc1	rozptýlení
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
možnost	možnost	k1gFnSc4	možnost
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
pitím	pití	k1gNnSc7	pití
nebo	nebo	k8xC	nebo
vdechnutím	vdechnutí	k1gNnSc7	vdechnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
první	první	k4xOgInSc1	první
výbor	výbor	k1gInSc1	výbor
OSN	OSN	kA	OSN
schválil	schválit	k5eAaPmAgInS	schválit
většinou	většina	k1gFnSc7	většina
hlasů	hlas	k1gInPc2	hlas
návrh	návrh	k1gInSc4	návrh
rezoluce	rezoluce	k1gFnSc2	rezoluce
požadující	požadující	k2eAgNnSc1d1	požadující
přezkoumání	přezkoumání	k1gNnSc1	přezkoumání
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
rizik	riziko	k1gNnPc2	riziko
zbraní	zbraň	k1gFnPc2	zbraň
používajících	používající	k2eAgFnPc2d1	používající
ochuzený	ochuzený	k2eAgInSc4d1	ochuzený
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bylo	být	k5eAaImAgNnS	být
122	[number]	k4	122
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
6	[number]	k4	6
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Karla	Karel	k1gMnSc2	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
proti	proti	k7c3	proti
jednáním	jednání	k1gNnPc3	jednání
o	o	k7c6	o
omezení	omezení	k1gNnSc6	omezení
zbraňových	zbraňový	k2eAgInPc2d1	zbraňový
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
ochuzeným	ochuzený	k2eAgInSc7d1	ochuzený
uranem	uran	k1gInSc7	uran
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
i	i	k9	i
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Uran	Uran	k1gInSc1	Uran
(	(	kIx(	(
<g/>
prvek	prvek	k1gInSc1	prvek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ochuzený	ochuzený	k2eAgInSc4d1	ochuzený
uran	uran	k1gInSc4	uran
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
Doctor	Doctor	k1gInSc1	Doctor
<g/>
,	,	kIx,	,
the	the	k?	the
Depleted	Depleted	k1gMnSc1	Depleted
Uranium	uranium	k1gNnSc1	uranium
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Dying	Dying	k1gInSc1	Dying
Children	Childrna	k1gFnPc2	Childrna
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
53	[number]	k4	53
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Freider	Freider	k1gMnSc1	Freider
Wagner	Wagner	k1gMnSc1	Wagner
</s>
</p>
