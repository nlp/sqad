<s>
Vedle	vedle	k7c2	vedle
wolframu	wolfram	k1gInSc2	wolfram
se	se	k3xPyFc4	se
ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
protipancéřových	protipancéřový	k2eAgInPc2d1	protipancéřový
projektilů	projektil	k1gInPc2	projektil
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
šípové	šípový	k2eAgFnPc1d1	šípová
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
podkaliberní	podkalibernit	k5eAaPmIp3nS	podkalibernit
střely	střela	k1gFnPc4	střela
–	–	k?	–
průměr	průměr	k1gInSc1	průměr
střely	střela	k1gFnSc2	střela
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
hlavně	hlavně	k6eAd1	hlavně
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
vystřelena	vystřelen	k2eAgFnSc1d1	vystřelena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
