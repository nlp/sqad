<s>
Řasy	řasa	k1gFnPc1	řasa
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
fotosyntetizující	fotosyntetizující	k2eAgInPc4d1	fotosyntetizující
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
řazené	řazený	k2eAgInPc1d1	řazený
mezi	mezi	k7c4	mezi
nižší	nízký	k2eAgFnPc4d2	nižší
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
