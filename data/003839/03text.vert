<s>
Řasy	řasa	k1gFnPc1	řasa
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
fotosyntetizující	fotosyntetizující	k2eAgInPc4d1	fotosyntetizující
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
řazené	řazený	k2eAgInPc1d1	řazený
mezi	mezi	k7c4	mezi
nižší	nízký	k2eAgFnPc4d2	nižší
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
řasy	řasa	k1gFnPc1	řasa
seskupením	seskupení	k1gNnSc7	seskupení
nepříbuzných	příbuzný	k2eNgFnPc2d1	nepříbuzná
skupin	skupina	k1gFnPc2	skupina
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jen	jen	k9	jen
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
blízké	blízký	k2eAgInPc1d1	blízký
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
řasami	řasa	k1gFnPc7	řasa
najdeme	najít	k5eAaPmIp1nP	najít
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
i	i	k8xC	i
mnohobuněčné	mnohobuněčný	k2eAgFnPc1d1	mnohobuněčná
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc1	tělo
mnohobuněčných	mnohobuněčný	k2eAgFnPc2d1	mnohobuněčná
řas	řasa	k1gFnPc2	řasa
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
stélkou	stélka	k1gFnSc7	stélka
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
přežít	přežít	k5eAaPmF	přežít
v	v	k7c6	v
suchém	suchý	k2eAgNnSc6d1	suché
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
proto	proto	k8xC	proto
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
nebo	nebo	k8xC	nebo
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
suchozemské	suchozemský	k2eAgFnPc1d1	suchozemská
formy	forma	k1gFnPc1	forma
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
nenápadné	nápadný	k2eNgFnPc1d1	nenápadná
a	a	k8xC	a
hojněji	hojně	k6eAd2	hojně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
vlhkých	vlhký	k2eAgFnPc6d1	vlhká
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
řasy	řasa	k1gFnPc1	řasa
vyřešily	vyřešit	k5eAaPmAgFnP	vyřešit
problém	problém	k1gInSc4	problém
vysychání	vysychání	k1gNnSc6	vysychání
symbiózou	symbióza	k1gFnSc7	symbióza
s	s	k7c7	s
houbou	houba	k1gFnSc7	houba
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
lišejníků	lišejník	k1gInPc2	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
řas	řasa	k1gFnPc2	řasa
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
fotosyntézy	fotosyntéza	k1gFnPc1	fotosyntéza
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
autotrofní	autotrofní	k2eAgNnSc1d1	autotrofní
<g/>
.	.	kIx.	.
</s>
<s>
Chloroplasty	chloroplast	k1gInPc1	chloroplast
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
primární	primární	k2eAgInPc4d1	primární
endosymbiózou	endosymbióza	k1gFnSc7	endosymbióza
se	s	k7c7	s
sinicí	sinice	k1gFnSc7	sinice
nebo	nebo	k8xC	nebo
až	až	k9	až
sekundárně	sekundárně	k6eAd1	sekundárně
symbiózou	symbióza	k1gFnSc7	symbióza
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
řasou	řasa	k1gFnSc7	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
řasy	řasa	k1gFnPc1	řasa
jsou	být	k5eAaImIp3nP	být
mixotrofní	mixotrofní	k2eAgFnPc1d1	mixotrofní
nebo	nebo	k8xC	nebo
sekundárně	sekundárně	k6eAd1	sekundárně
chloroplasty	chloroplast	k1gInPc1	chloroplast
dokonce	dokonce	k9	dokonce
úplně	úplně	k6eAd1	úplně
ztratily	ztratit	k5eAaPmAgFnP	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Řasy	řasa	k1gFnPc1	řasa
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
vodních	vodní	k2eAgInPc2d1	vodní
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
,	,	kIx,	,
ruduchy	ruducha	k1gFnPc1	ruducha
a	a	k8xC	a
chaluhy	chaluha	k1gFnPc1	chaluha
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
potravu	potrava	k1gFnSc4	potrava
i	i	k9	i
úkryt	úkryt	k1gInSc4	úkryt
<g/>
,	,	kIx,	,
mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
řasy	řasa	k1gFnPc1	řasa
tvoří	tvořit	k5eAaImIp3nP	tvořit
fytoplankton	fytoplankton	k1gInSc4	fytoplankton
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
řasy	řasa	k1gFnPc4	řasa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
algologie	algologie	k1gFnSc1	algologie
či	či	k8xC	či
fykologie	fykologie	k1gFnSc1	fykologie
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
řasy	řasa	k1gFnPc1	řasa
lze	lze	k6eAd1	lze
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
využít	využít	k5eAaPmF	využít
jak	jak	k6eAd1	jak
jako	jako	k8xC	jako
potravinu	potravina	k1gFnSc4	potravina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
palivo	palivo	k1gNnSc4	palivo
či	či	k8xC	či
surovinu	surovina	k1gFnSc4	surovina
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
metanu	metan	k1gInSc2	metan
a	a	k8xC	a
biopaliva	biopalivo	k1gNnSc2	biopalivo
<g/>
.	.	kIx.	.
</s>
<s>
Řasy	řasa	k1gFnPc1	řasa
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dělit	dělit	k5eAaImF	dělit
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
<g/>
:	:	kIx,	:
Zelené	Zelené	k2eAgFnSc2d1	Zelené
řasy	řasa	k1gFnSc2	řasa
Jednobuněčné	jednobuněčný	k2eAgFnSc2d1	jednobuněčná
Jednobuněčné	jednobuněčný	k2eAgFnSc2d1	jednobuněčná
koloniální	koloniální	k2eAgFnSc2d1	koloniální
Mnohobuněčné	mnohobuněčný	k2eAgFnSc2d1	mnohobuněčná
Hnědé	hnědý	k2eAgFnSc2d1	hnědá
řasy	řasa	k1gFnSc2	řasa
Červené	Červené	k2eAgFnPc1d1	Červené
řasy	řasa	k1gFnPc1	řasa
Mezi	mezi	k7c4	mezi
zelené	zelený	k2eAgFnPc4d1	zelená
řasy	řasa	k1gFnPc4	řasa
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
zelenivka	zelenivka	k1gFnSc1	zelenivka
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
doplněk	doplněk	k1gInSc1	doplněk
stravy	strava	k1gFnSc2	strava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krásnoočko	krásnoočko	k1gNnSc1	krásnoočko
<g/>
,	,	kIx,	,
pláštěnka	pláštěnka	k1gFnSc1	pláštěnka
(	(	kIx(	(
<g/>
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zelený	zelený	k2eAgInSc1d1	zelený
zákal	zákal	k1gInSc1	zákal
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
zrněnka	zrněnka	k1gFnSc1	zrněnka
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
)	)	kIx)	)
-	-	kIx~	-
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
řasy	řasa	k1gFnPc1	řasa
jsou	být	k5eAaImIp3nP	být
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
<g/>
,	,	kIx,	,
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
koloniální	koloniální	k2eAgFnPc4d1	koloniální
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
-	-	kIx~	-
nerozdělí	rozdělit	k5eNaPmIp3nP	rozdělit
se	se	k3xPyFc4	se
úplně	úplně	k6eAd1	úplně
nebo	nebo	k8xC	nebo
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
blízko	blízko	k6eAd1	blízko
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
soužití	soužití	k1gNnSc1	soužití
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
(	(	kIx(	(
<g/>
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
dělení	dělení	k1gNnSc1	dělení
úkolů	úkol	k1gInPc2	úkol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohobuněčné	mnohobuněčný	k2eAgInPc1d1	mnohobuněčný
organismy	organismus	k1gInPc1	organismus
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
-	-	kIx~	-
jen	jen	k9	jen
buňky	buňka	k1gFnPc1	buňka
nemohou	moct	k5eNaImIp3nP	moct
žít	žít	k5eAaImF	žít
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
funguje	fungovat	k5eAaImIp3nS	fungovat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Řasy	řasa	k1gFnPc1	řasa
spadají	spadat	k5eAaImIp3nP	spadat
podle	podle	k7c2	podle
současného	současný	k2eAgNnSc2d1	současné
pojetí	pojetí	k1gNnSc2	pojetí
do	do	k7c2	do
několika	několik	k4yIc2	několik
říší	říš	k1gFnPc2	říš
<g/>
:	:	kIx,	:
Archaeplastida	Archaeplastida	k1gFnSc1	Archaeplastida
Archeplastida	Archeplastid	k1gMnSc2	Archeplastid
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechny	všechen	k3xTgInPc4	všechen
fotosyntetizující	fotosyntetizující	k2eAgInPc4d1	fotosyntetizující
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
chloroplasty	chloroplast	k1gInPc4	chloroplast
s	s	k7c7	s
dvoujednotkovou	dvoujednotkový	k2eAgFnSc7d1	dvoujednotkový
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Plastidy	plastid	k1gInPc1	plastid
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
primární	primární	k2eAgInPc4d1	primární
endosymbiózou	endosymbióza	k1gFnSc7	endosymbióza
se	s	k7c7	s
sinicí	sinice	k1gFnSc7	sinice
<g/>
.	.	kIx.	.
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
Chlorophyta	Chlorophyto	k1gNnPc1	Chlorophyto
sensu	sens	k1gInSc2	sens
lato	lata	k1gFnSc5	lata
<g/>
)	)	kIx)	)
Chlorophyta	Chlorophyta	k1gFnSc1	Chlorophyta
sensu	sens	k1gInSc2	sens
stricto	stricto	k1gNnSc1	stricto
Charophyta	Charophyto	k1gNnSc2	Charophyto
-	-	kIx~	-
parafyletická	parafyletický	k2eAgFnSc1d1	parafyletická
skupina	skupina	k1gFnSc1	skupina
<g/>
;	;	kIx,	;
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
vyššími	vysoký	k2eAgFnPc7d2	vyšší
rostlinami	rostlina	k1gFnPc7	rostlina
tvoří	tvořit	k5eAaImIp3nS	tvořit
monofyletickou	monofyletický	k2eAgFnSc4d1	monofyletická
skupinu	skupina	k1gFnSc4	skupina
Streptophyta	Streptophyt	k1gInSc2	Streptophyt
<g/>
.	.	kIx.	.
červené	červený	k2eAgFnPc1d1	červená
řasy	řasa	k1gFnPc1	řasa
čili	čili	k8xC	čili
ruduchy	ruducha	k1gFnPc1	ruducha
(	(	kIx(	(
<g/>
Rhodophyta	Rhodophyta	k1gFnSc1	Rhodophyta
<g/>
)	)	kIx)	)
Glaucophyta	Glaucophyta	k1gFnSc1	Glaucophyta
Excavata	Excavata	k1gFnSc1	Excavata
U	u	k7c2	u
krásnooček	krásnoočko	k1gNnPc2	krásnoočko
jsou	být	k5eAaImIp3nP	být
plastidy	plastid	k1gInPc1	plastid
obklopené	obklopený	k2eAgInPc1d1	obklopený
trojitou	trojitý	k2eAgFnSc7d1	trojitá
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
zřejmě	zřejmě	k6eAd1	zřejmě
pozřením	pozření	k1gNnSc7	pozření
zelené	zelený	k2eAgFnSc2d1	zelená
řasy	řasa	k1gFnSc2	řasa
pradávným	pradávný	k2eAgNnSc7d1	pradávné
krásnoočkem	krásnoočko	k1gNnSc7	krásnoočko
<g/>
.	.	kIx.	.
krásnoočka	krásnoočko	k1gNnPc1	krásnoočko
(	(	kIx(	(
<g/>
Euglenozoa	Euglenozoa	k1gFnSc1	Euglenozoa
<g/>
)	)	kIx)	)
Chromista	Chromista	k1gMnSc1	Chromista
Rhizaria	Rhizarium	k1gNnSc2	Rhizarium
U	u	k7c2	u
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
membrána	membrána	k1gFnSc1	membrána
chloroplastů	chloroplast	k1gInPc2	chloroplast
dokonce	dokonce	k9	dokonce
čtyřjednotková	čtyřjednotkový	k2eAgFnSc1d1	čtyřjednotkový
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
krásnooček	krásnoočko	k1gNnPc2	krásnoočko
<g/>
.	.	kIx.	.
</s>
<s>
Chlorarachniophyta	Chlorarachniophyta	k1gFnSc1	Chlorarachniophyta
Chromalveolata	Chromalveole	k1gNnPc1	Chromalveole
sensu	sens	k1gInSc2	sens
stricto	stricto	k1gNnSc1	stricto
Mají	mít	k5eAaImIp3nP	mít
chloroplasty	chloroplast	k1gInPc4	chloroplast
obsahující	obsahující	k2eAgInSc4d1	obsahující
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
a	a	k8xC	a
a	a	k8xC	a
c.	c.	k?	c.
Chlorofyl	chlorofyl	k1gInSc1	chlorofyl
c	c	k0	c
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
ani	ani	k8xC	ani
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
u	u	k7c2	u
archaeplastid	archaeplastida	k1gFnPc2	archaeplastida
<g/>
.	.	kIx.	.
</s>
<s>
Heterokontophyta	Heterokontophyta	k1gFnSc1	Heterokontophyta
<g/>
,	,	kIx,	,
nověji	nově	k6eAd2	nově
zvané	zvaný	k2eAgFnPc1d1	zvaná
Stramenopiles	Stramenopiles	k1gInSc4	Stramenopiles
či	či	k8xC	či
Stramenopila	Stramenopila	k1gFnSc4	Stramenopila
skrytěnky	skrytěnka	k1gFnSc2	skrytěnka
čili	čili	k8xC	čili
kryptomonády	kryptomonáda	k1gFnSc2	kryptomonáda
(	(	kIx(	(
<g/>
Cryptophyta	Cryptophyta	k1gFnSc1	Cryptophyta
<g/>
)	)	kIx)	)
Haptophyta	Haptophyta	k1gFnSc1	Haptophyta
obrněnky	obrněnka	k1gFnSc2	obrněnka
(	(	kIx(	(
<g/>
Dinozoa	Dinozoa	k1gFnSc1	Dinozoa
<g/>
)	)	kIx)	)
První	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
skupiny	skupina	k1gFnPc1	skupina
mají	mít	k5eAaImIp3nP	mít
stěnu	stěna	k1gFnSc4	stěna
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
čtyřmi	čtyři	k4xCgFnPc7	čtyři
membránami	membrána	k1gFnPc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Chloroplasty	chloroplast	k1gInPc1	chloroplast
obrněnek	obrněnka	k1gFnPc2	obrněnka
mají	mít	k5eAaImIp3nP	mít
membrány	membrána	k1gFnPc4	membrána
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
řasy	řasa	k1gFnPc4	řasa
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
řadily	řadit	k5eAaImAgFnP	řadit
i	i	k9	i
fotosyntetizující	fotosyntetizující	k2eAgFnPc1d1	fotosyntetizující
bakterie	bakterie	k1gFnPc1	bakterie
-	-	kIx~	-
sinice	sinice	k1gFnPc1	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
podle	podle	k7c2	podle
dnešních	dnešní	k2eAgInPc2d1	dnešní
poznatků	poznatek	k1gInPc2	poznatek
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
úplně	úplně	k6eAd1	úplně
jiné	jiný	k2eAgFnSc2d1	jiná
domény	doména	k1gFnSc2	doména
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
za	za	k7c2	za
řasy	řasa	k1gFnSc2	řasa
považovány	považován	k2eAgInPc1d1	považován
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
a	a	k8xC	a
články	článek	k1gInPc1	článek
o	o	k7c6	o
sinicích	sinice	k1gFnPc6	sinice
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
součástí	součást	k1gFnPc2	součást
fykologických	fykologický	k2eAgNnPc2d1	fykologický
periodik	periodikum	k1gNnPc2	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
řasy	řasa	k1gFnSc2	řasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
řasa	řasa	k1gFnSc1	řasa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
