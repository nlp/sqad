<s>
Detoxikace	detoxikace	k1gFnSc1	detoxikace
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
de-	de-	k?	de-
<g/>
,	,	kIx,	,
od	od	k7c2	od
a	a	k8xC	a
toxicus	toxicus	k1gMnSc1	toxicus
<g/>
,	,	kIx,	,
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
odstranění	odstranění	k1gNnSc2	odstranění
nebo	nebo	k8xC	nebo
zneškodnění	zneškodnění	k1gNnSc2	zneškodnění
toxické	toxický	k2eAgFnSc2d1	toxická
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Detoxikace	detoxikace	k1gFnSc1	detoxikace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
složek	složka	k1gFnPc2	složka
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
detoxikaci	detoxikace	k1gFnSc6	detoxikace
lze	lze	k6eAd1	lze
pohlížet	pohlížet	k5eAaImF	pohlížet
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
článku	článek	k1gInSc2	článek
popisuje	popisovat	k5eAaImIp3nS	popisovat
detoxikaci	detoxikace	k1gFnSc4	detoxikace
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
odstranění	odstranění	k1gNnSc2	odstranění
toxické	toxický	k2eAgFnSc2d1	toxická
látky	látka	k1gFnSc2	látka
z	z	k7c2	z
neživých	živý	k2eNgInPc2d1	neživý
předmětů	předmět	k1gInPc2	předmět
i	i	k8xC	i
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
kontaminovány	kontaminovat	k5eAaBmNgFnP	kontaminovat
především	především	k6eAd1	především
povrchově	povrchově	k6eAd1	povrchově
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
toxikinetiku	toxikinetika	k1gFnSc4	toxikinetika
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
na	na	k7c4	na
osud	osud	k1gInSc4	osud
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
cestu	cesta	k1gFnSc4	cesta
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
lidským	lidský	k2eAgInSc7d1	lidský
organismem	organismus	k1gInSc7	organismus
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
příjmu	příjem	k1gInSc2	příjem
až	až	k9	až
po	po	k7c6	po
vyloučení	vyloučení	k1gNnSc6	vyloučení
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
faktu	fakt	k1gInSc3	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
fyziologicky	fyziologicky	k6eAd1	fyziologicky
automaticky	automaticky	k6eAd1	automaticky
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vyloučení	vyloučení	k1gNnSc1	vyloučení
dané	daný	k2eAgFnSc2d1	daná
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
biologickou	biologický	k2eAgFnSc4d1	biologická
detoxikaci	detoxikace	k1gFnSc4	detoxikace
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
části	část	k1gFnSc6	část
budou	být	k5eAaImBp3nP	být
zmíněny	zmínit	k5eAaPmNgInP	zmínit
další	další	k2eAgInPc1d1	další
možné	možný	k2eAgInPc1d1	možný
výklady	výklad	k1gInPc1	výklad
slova	slovo	k1gNnSc2	slovo
detoxikace	detoxikace	k1gFnSc2	detoxikace
<g/>
.	.	kIx.	.
</s>
<s>
Detoxikace	detoxikace	k1gFnSc1	detoxikace
je	být	k5eAaImIp3nS	být
podmnožinou	podmnožina	k1gFnSc7	podmnožina
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Detoxikace	detoxikace	k1gFnSc1	detoxikace
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xC	jako
soubor	soubor	k1gInSc1	soubor
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
organizačního	organizační	k2eAgNnSc2d1	organizační
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
k	k	k7c3	k
účinnému	účinný	k2eAgNnSc3d1	účinné
odstranění	odstranění	k1gNnSc3	odstranění
toxické	toxický	k2eAgFnSc2d1	toxická
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
snížení	snížení	k1gNnSc2	snížení
škodlivého	škodlivý	k2eAgInSc2d1	škodlivý
účinku	účinek	k1gInSc2	účinek
toxické	toxický	k2eAgFnSc2d1	toxická
látky	látka	k1gFnSc2	látka
na	na	k7c4	na
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neohrožuje	ohrožovat	k5eNaImIp3nS	ohrožovat
život	život	k1gInSc4	život
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
následnou	následný	k2eAgFnSc4d1	následná
likvidaci	likvidace	k1gFnSc4	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpravidla	zpravidla	k6eAd1	zpravidla
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stoprocentní	stoprocentní	k2eAgNnSc1d1	stoprocentní
odstranění	odstranění	k1gNnSc1	odstranění
toxické	toxický	k2eAgFnSc2d1	toxická
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
tzv.	tzv.	kA	tzv.
zbytkové	zbytkový	k2eAgFnPc4d1	zbytková
kontaminace	kontaminace	k1gFnPc4	kontaminace
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Detoxikace	detoxikace	k1gFnSc1	detoxikace
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc4d1	významné
opatření	opatření	k1gNnSc4	opatření
aktivní	aktivní	k2eAgFnSc2d1	aktivní
ochrany	ochrana	k1gFnSc2	ochrana
proti	proti	k7c3	proti
působení	působení	k1gNnSc3	působení
toxických	toxický	k2eAgFnPc2d1	toxická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nutnost	nutnost	k1gFnSc1	nutnost
detoxikace	detoxikace	k1gFnSc2	detoxikace
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
toxická	toxický	k2eAgFnSc1d1	toxická
látka	látka	k1gFnSc1	látka
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
zasažený	zasažený	k2eAgInSc4d1	zasažený
objekt	objekt	k1gInSc4	objekt
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
objekty	objekt	k1gInPc4	objekt
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
ani	ani	k8xC	ani
vznik	vznik	k1gInSc4	vznik
dominového	dominový	k2eAgInSc2d1	dominový
efektu	efekt	k1gInSc2	efekt
způsobeného	způsobený	k2eAgInSc2d1	způsobený
vzájemným	vzájemný	k2eAgInSc7d1	vzájemný
přenosem	přenos	k1gInSc7	přenos
toxické	toxický	k2eAgFnSc2d1	toxická
látky	látka	k1gFnSc2	látka
z	z	k7c2	z
objektu	objekt	k1gInSc2	objekt
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
rizikem	riziko	k1gNnSc7	riziko
hlavně	hlavně	k6eAd1	hlavně
pro	pro	k7c4	pro
zdravotnická	zdravotnický	k2eAgNnPc4d1	zdravotnické
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
následkem	následkem	k7c2	následkem
je	být	k5eAaImIp3nS	být
kontaminace	kontaminace	k1gFnSc1	kontaminace
osob	osoba	k1gFnPc2	osoba
současně	současně	k6eAd1	současně
se	s	k7c7	s
zraněními	zranění	k1gNnPc7	zranění
způsobenými	způsobený	k2eAgNnPc7d1	způsobené
např.	např.	kA	např.
destrukčními	destrukční	k2eAgInPc7d1	destrukční
účinky	účinek	k1gInPc7	účinek
výbuchu	výbuch	k1gInSc2	výbuch
(	(	kIx(	(
<g/>
popáleniny	popálenina	k1gFnPc4	popálenina
<g/>
,	,	kIx,	,
zlomeniny	zlomenina	k1gFnPc4	zlomenina
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
nejprve	nejprve	k6eAd1	nejprve
osoby	osoba	k1gFnPc4	osoba
detoxikovat	detoxikovat	k5eAaBmF	detoxikovat
a	a	k8xC	a
až	až	k6eAd1	až
poté	poté	k6eAd1	poté
přesunout	přesunout	k5eAaPmF	přesunout
do	do	k7c2	do
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
život	život	k1gInSc4	život
zachraňující	zachraňující	k2eAgInPc4d1	zachraňující
úkony	úkon	k1gInPc4	úkon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
detoxikací	detoxikace	k1gFnSc7	detoxikace
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
detoxikace	detoxikace	k1gFnSc2	detoxikace
je	být	k5eAaImIp3nS	být
snížení	snížení	k1gNnSc1	snížení
nenávratných	návratný	k2eNgFnPc2d1	nenávratná
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
,	,	kIx,	,
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
následků	následek	k1gInPc2	následek
a	a	k8xC	a
zkrácení	zkrácení	k1gNnSc2	zkrácení
nutné	nutný	k2eAgFnSc2d1	nutná
doby	doba	k1gFnSc2	doba
používání	používání	k1gNnSc1	používání
prostředků	prostředek	k1gInPc2	prostředek
individuální	individuální	k2eAgFnSc2d1	individuální
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ztěžují	ztěžovat	k5eAaImIp3nP	ztěžovat
provádění	provádění	k1gNnSc4	provádění
činností	činnost	k1gFnPc2	činnost
v	v	k7c6	v
kontaminovaném	kontaminovaný	k2eAgNnSc6d1	kontaminované
území	území	k1gNnSc6	území
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
dobu	doba	k1gFnSc4	doba
záchranných	záchranný	k2eAgFnPc2d1	záchranná
a	a	k8xC	a
likvidačních	likvidační	k2eAgFnPc2d1	likvidační
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
normálního	normální	k2eAgInSc2d1	normální
života	život	k1gInSc2	život
v	v	k7c6	v
původně	původně	k6eAd1	původně
kontaminovaných	kontaminovaný	k2eAgFnPc6d1	kontaminovaná
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
látky	látka	k1gFnSc2	látka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
detoxikaci	detoxikace	k1gFnSc6	detoxikace
v	v	k7c6	v
případě	případ	k1gInSc6	případ
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
dezinfekci	dezinfekce	k1gFnSc4	dezinfekce
v	v	k7c6	v
případě	případ	k1gInSc6	případ
biologických	biologický	k2eAgFnPc2d1	biologická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
dezaktivaci	dezaktivace	k1gFnSc4	dezaktivace
v	v	k7c6	v
případě	případ	k1gInSc6	případ
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
detoxikace	detoxikace	k1gFnSc2	detoxikace
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
měření	měření	k1gNnSc4	měření
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
biologických	biologický	k2eAgInPc2d1	biologický
kontaminantů	kontaminant	k1gInPc2	kontaminant
<g/>
)	)	kIx)	)
samotné	samotný	k2eAgFnPc4d1	samotná
dekontaminace	dekontaminace	k1gFnPc4	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
detoxikace	detoxikace	k1gFnSc2	detoxikace
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
a	a	k8xC	a
výsledky	výsledek	k1gInPc4	výsledek
udávají	udávat	k5eAaImIp3nP	udávat
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
a	a	k8xC	a
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
následujícího	následující	k2eAgInSc2d1	následující
matematického	matematický	k2eAgInSc2d1	matematický
vztahu	vztah	k1gInSc2	vztah
<g/>
:	:	kIx,	:
((	((	k?	((
<g/>
Zp	Zp	k1gMnSc1	Zp
-	-	kIx~	-
Zu	Zu	k1gMnSc1	Zu
<g/>
)	)	kIx)	)
*	*	kIx~	*
100	[number]	k4	100
<g/>
)	)	kIx)	)
/	/	kIx~	/
Zp	Zp	k1gMnSc1	Zp
Ve	v	k7c6	v
vzorci	vzorec	k1gInSc6	vzorec
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
:	:	kIx,	:
Zp	Zp	k1gFnSc7	Zp
hodnotu	hodnota	k1gFnSc4	hodnota
počáteční	počáteční	k2eAgFnSc2d1	počáteční
kontaminace	kontaminace	k1gFnSc2	kontaminace
<g/>
,	,	kIx,	,
Zu	Zu	k1gFnSc1	Zu
hodnotu	hodnota	k1gFnSc4	hodnota
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
detoxikace	detoxikace	k1gFnSc2	detoxikace
<g/>
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naměřená	naměřený	k2eAgFnSc1d1	naměřená
hodnota	hodnota	k1gFnSc1	hodnota
kontaminantu	kontaminant	k1gInSc2	kontaminant
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
procesu	proces	k1gInSc2	proces
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
zbytková	zbytkový	k2eAgFnSc1d1	zbytková
kontaminace	kontaminace	k1gFnSc1	kontaminace
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
nejde	jít	k5eNaImIp3nS	jít
za	za	k7c4	za
dodržení	dodržení	k1gNnSc4	dodržení
stejných	stejný	k2eAgFnPc2d1	stejná
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
dále	daleko	k6eAd2	daleko
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
faktorech	faktor	k1gInPc6	faktor
<g/>
:	:	kIx,	:
na	na	k7c6	na
zvolení	zvolení	k1gNnSc6	zvolení
správné	správný	k2eAgFnSc2d1	správná
dekontaminační	dekontaminační	k2eAgFnSc2d1	dekontaminační
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
na	na	k7c4	na
zvolení	zvolení	k1gNnSc4	zvolení
dekontaminačního	dekontaminační	k2eAgNnSc2d1	dekontaminační
činidla	činidlo	k1gNnSc2	činidlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
zvolení	zvolení	k1gNnSc6	zvolení
dekontaminačních	dekontaminační	k2eAgInPc2d1	dekontaminační
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
na	na	k7c6	na
skupenství	skupenství	k1gNnSc6	skupenství
a	a	k8xC	a
vlastnostech	vlastnost	k1gFnPc6	vlastnost
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
,	,	kIx,	,
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
na	na	k7c6	na
dodržení	dodržení	k1gNnSc6	dodržení
dekontaminačního	dekontaminační	k2eAgInSc2d1	dekontaminační
postupu	postup	k1gInSc2	postup
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
možné	možný	k2eAgInPc1d1	možný
způsoby	způsob	k1gInPc1	způsob
provádění	provádění	k1gNnSc4	provádění
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
:	:	kIx,	:
suchý	suchý	k2eAgInSc1d1	suchý
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
mokrý	mokrý	k2eAgInSc1d1	mokrý
způsob	způsob	k1gInSc1	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
způsobu	způsob	k1gInSc2	způsob
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
druh	druh	k1gInSc4	druh
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgNnSc4d1	dostupné
technické	technický	k2eAgNnSc4d1	technické
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
kontaminace	kontaminace	k1gFnSc2	kontaminace
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
suché	suchý	k2eAgInPc4d1	suchý
způsoby	způsob	k1gInPc4	způsob
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
především	především	k9	především
vytřepávání	vytřepávání	k1gNnSc1	vytřepávání
<g/>
,	,	kIx,	,
vyklepávání	vyklepávání	k1gNnSc1	vyklepávání
<g/>
,	,	kIx,	,
otírání	otírání	k1gNnSc1	otírání
za	za	k7c2	za
sucha	sucho	k1gNnSc2	sucho
<g/>
,	,	kIx,	,
kartáčování	kartáčování	k1gNnSc2	kartáčování
<g/>
,	,	kIx,	,
vysávání	vysávání	k1gNnSc1	vysávání
a	a	k8xC	a
odpařování	odpařování	k1gNnSc1	odpařování
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
nebo	nebo	k8xC	nebo
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgMnSc1d1	účinný
především	především	k9	především
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
-li	i	k?	-li
ke	k	k7c3	k
kontaminaci	kontaminace	k1gFnSc3	kontaminace
také	také	k9	také
za	za	k7c2	za
sucha	sucho	k1gNnSc2	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
výhody	výhoda	k1gFnPc4	výhoda
patří	patřit	k5eAaImIp3nS	patřit
hlavně	hlavně	k9	hlavně
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
použití	použití	k1gNnSc2	použití
i	i	k9	i
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
odpadů	odpad	k1gInPc2	odpad
<g/>
,	,	kIx,	,
nepotřebnost	nepotřebnost	k1gFnSc1	nepotřebnost
dekontaminačních	dekontaminační	k2eAgNnPc2d1	dekontaminační
činidel	činidlo	k1gNnPc2	činidlo
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
objemová	objemový	k2eAgFnSc1d1	objemová
skladnost	skladnost	k1gFnSc1	skladnost
používaných	používaný	k2eAgInPc2d1	používaný
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
patří	patřit	k5eAaImIp3nS	patřit
nutnost	nutnost	k1gFnSc4	nutnost
používání	používání	k1gNnSc2	používání
výkonných	výkonný	k2eAgInPc2d1	výkonný
strojů	stroj	k1gInPc2	stroj
společně	společně	k6eAd1	společně
s	s	k7c7	s
kvalitními	kvalitní	k2eAgInPc7d1	kvalitní
HEPA	HEPA	kA	HEPA
(	(	kIx(	(
<g/>
high	high	k1gMnSc1	high
efficiency	efficienca	k1gFnSc2	efficienca
particulate	particulat	k1gInSc5	particulat
air	air	k?	air
filter	filter	k1gInSc4	filter
-	-	kIx~	-
vysoce	vysoce	k6eAd1	vysoce
účinný	účinný	k2eAgInSc1d1	účinný
filtr	filtr	k1gInSc1	filtr
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
filtry	filtr	k1gInPc1	filtr
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vysávání	vysávání	k1gNnSc2	vysávání
a	a	k8xC	a
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
následnému	následný	k2eAgNnSc3d1	následné
použití	použití	k1gNnSc3	použití
mokrého	mokrý	k2eAgInSc2d1	mokrý
způsobu	způsob	k1gInSc2	způsob
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
mokré	mokrý	k2eAgFnPc4d1	mokrá
způsoby	způsoba	k1gFnPc4	způsoba
patří	patřit	k5eAaImIp3nS	patřit
postřik	postřik	k1gInSc1	postřik
<g/>
,	,	kIx,	,
otírání	otírání	k1gNnPc1	otírání
za	za	k7c4	za
mokra	mokro	k1gNnPc4	mokro
<g/>
,	,	kIx,	,
extrakce	extrakce	k1gFnPc4	extrakce
do	do	k7c2	do
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
(	(	kIx(	(
<g/>
chemické	chemický	k2eAgNnSc1d1	chemické
čištění	čištění	k1gNnSc1	čištění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
praní	praní	k1gNnSc1	praní
<g/>
,	,	kIx,	,
dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
vodní	vodní	k2eAgFnSc1d1	vodní
parou	para	k1gFnSc7	para
a	a	k8xC	a
pěnami	pěna	k1gFnPc7	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
využívá	využívat	k5eAaPmIp3nS	využívat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
procesů	proces	k1gInPc2	proces
nebo	nebo	k8xC	nebo
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ředění	ředění	k1gNnSc4	ředění
<g/>
,	,	kIx,	,
extrakci	extrakce	k1gFnSc4	extrakce
<g/>
,	,	kIx,	,
neutralizaci	neutralizace	k1gFnSc4	neutralizace
<g/>
,	,	kIx,	,
absorpci	absorpce	k1gFnSc4	absorpce
<g/>
,	,	kIx,	,
rozklad	rozklad	k1gInSc4	rozklad
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
komplexů	komplex	k1gInPc2	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
suchými	suchý	k2eAgMnPc7d1	suchý
mají	mít	k5eAaImIp3nP	mít
mokré	mokrý	k2eAgInPc4d1	mokrý
způsoby	způsob	k1gInPc4	způsob
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
významně	významně	k6eAd1	významně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ale	ale	k9	ale
riziko	riziko	k1gNnSc1	riziko
převedení	převedení	k1gNnSc2	převedení
kontaminantu	kontaminant	k1gInSc2	kontaminant
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
a	a	k8xC	a
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
nedostatečně	dostatečně	k6eNd1	dostatečně
rychlém	rychlý	k2eAgNnSc6d1	rychlé
odstranění	odstranění	k1gNnSc6	odstranění
akcelerace	akcelerace	k1gFnSc2	akcelerace
jeho	jeho	k3xOp3gInSc2	jeho
průniku	průnik	k1gInSc2	průnik
do	do	k7c2	do
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
tedy	tedy	k9	tedy
následné	následný	k2eAgNnSc4d1	následné
snížení	snížení	k1gNnSc4	snížení
účinnosti	účinnost	k1gFnSc2	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
výhody	výhoda	k1gFnPc4	výhoda
patří	patřit	k5eAaImIp3nS	patřit
hlavně	hlavně	k9	hlavně
již	již	k6eAd1	již
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
účinnost	účinnost	k1gFnSc1	účinnost
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
důsledku	důsledek	k1gInSc6	důsledek
i	i	k9	i
menší	malý	k2eAgInSc4d2	menší
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
technickou	technický	k2eAgFnSc4d1	technická
vyspělost	vyspělost	k1gFnSc4	vyspělost
dekontaminační	dekontaminační	k2eAgFnSc2d1	dekontaminační
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použít	použít	k5eAaPmF	použít
různé	různý	k2eAgFnPc4d1	různá
dekontaminační	dekontaminační	k2eAgFnPc4d1	dekontaminační
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
směsi	směs	k1gFnPc4	směs
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k9	tak
co	co	k3yInSc4	co
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
odstranění	odstranění	k1gNnSc4	odstranění
daného	daný	k2eAgInSc2d1	daný
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
.	.	kIx.	.
</s>
<s>
Odpady	odpad	k1gInPc1	odpad
mokrého	mokrý	k2eAgInSc2d1	mokrý
způsobu	způsob	k1gInSc2	způsob
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
také	také	k9	také
snadno	snadno	k6eAd1	snadno
zachycovat	zachycovat	k5eAaImF	zachycovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
patří	patřit	k5eAaImIp3nS	patřit
hlavně	hlavně	k9	hlavně
vznik	vznik	k1gInSc1	vznik
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
odpadů	odpad	k1gInPc2	odpad
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
následné	následný	k2eAgNnSc4d1	následné
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
dekontaminační	dekontaminační	k2eAgFnPc1d1	dekontaminační
látky	látka	k1gFnPc1	látka
a	a	k8xC	a
směsi	směs	k1gFnPc1	směs
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
destruktivně	destruktivně	k6eAd1	destruktivně
na	na	k7c4	na
dekontaminovaný	dekontaminovaný	k2eAgInSc4d1	dekontaminovaný
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
nepříznivě	příznivě	k6eNd1	příznivě
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jejich	jejich	k3xOp3gInSc2	jejich
úniku	únik	k1gInSc2	únik
nebo	nebo	k8xC	nebo
nemožnosti	nemožnost	k1gFnSc2	nemožnost
zachycení	zachycení	k1gNnSc2	zachycení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
látky	látka	k1gFnPc1	látka
a	a	k8xC	a
směsi	směs	k1gFnPc1	směs
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nestabilní	stabilní	k2eNgMnPc1d1	nestabilní
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
klade	klást	k5eAaImIp3nS	klást
vyšší	vysoký	k2eAgInPc4d2	vyšší
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
skladování	skladování	k1gNnSc4	skladování
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc4	jejich
případnou	případný	k2eAgFnSc4d1	případná
likvidaci	likvidace	k1gFnSc4	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
je	být	k5eAaImIp3nS	být
také	také	k9	také
použití	použití	k1gNnSc1	použití
mokrého	mokrý	k2eAgInSc2d1	mokrý
způsobu	způsob	k1gInSc2	způsob
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
stává	stávat	k5eAaImIp3nS	stávat
i	i	k9	i
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
doba	doba	k1gFnSc1	doba
aktivního	aktivní	k2eAgNnSc2d1	aktivní
působení	působení	k1gNnSc2	působení
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
perspektivním	perspektivní	k2eAgInSc7d1	perspektivní
mokrým	mokrý	k2eAgInSc7d1	mokrý
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
pěnami	pěna	k1gFnPc7	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
intenzivně	intenzivně	k6eAd1	intenzivně
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
<g/>
,	,	kIx,	,
vyvíjena	vyvíjet	k5eAaImNgFnS	vyvíjet
a	a	k8xC	a
testována	testovat	k5eAaImNgFnS	testovat
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
patogenních	patogenní	k2eAgMnPc2d1	patogenní
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
využitelná	využitelný	k2eAgFnSc1d1	využitelná
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
dezaktivaci	dezaktivace	k1gFnSc4	dezaktivace
a	a	k8xC	a
detoxikaci	detoxikace	k1gFnSc4	detoxikace
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
případech	případ	k1gInPc6	případ
rizika	riziko	k1gNnSc2	riziko
znehodnocení	znehodnocení	k1gNnSc2	znehodnocení
materiálu	materiál	k1gInSc6	materiál
použitím	použití	k1gNnSc7	použití
klasického	klasický	k2eAgInSc2d1	klasický
mokrého	mokrý	k2eAgInSc2d1	mokrý
způsobu	způsob	k1gInSc2	způsob
a	a	k8xC	a
problému	problém	k1gInSc2	problém
likvidace	likvidace	k1gFnSc2	likvidace
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
odpadů	odpad	k1gInPc2	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
výhody	výhoda	k1gFnPc1	výhoda
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnSc4d1	následující
<g/>
:	:	kIx,	:
lepší	dobrý	k2eAgFnSc4d2	lepší
schopnost	schopnost	k1gFnSc4	schopnost
fixace	fixace	k1gFnSc2	fixace
dekontaminační	dekontaminační	k2eAgFnSc2d1	dekontaminační
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
směsi	směs	k1gFnSc2	směs
na	na	k7c6	na
požadovaném	požadovaný	k2eAgInSc6d1	požadovaný
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
sekundárního	sekundární	k2eAgInSc2d1	sekundární
přenosu	přenos	k1gInSc2	přenos
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc2	aplikace
pěny	pěna	k1gFnSc2	pěna
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
a	a	k8xC	a
bezpečnější	bezpečný	k2eAgFnSc2d2	bezpečnější
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
napěnitelnost	napěnitelnost	k1gFnSc1	napěnitelnost
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
přídavků	přídavek	k1gInPc2	přídavek
dalších	další	k2eAgFnPc2d1	další
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgNnSc4d2	delší
působení	působení	k1gNnSc4	působení
na	na	k7c6	na
šikmých	šikmý	k2eAgFnPc6d1	šikmá
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
svislých	svislý	k2eAgInPc6d1	svislý
površích	povrch	k1gInPc6	povrch
a	a	k8xC	a
stropech	strop	k1gInPc6	strop
<g/>
,	,	kIx,	,
při	při	k7c6	při
aplikaci	aplikace	k1gFnSc6	aplikace
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
dekontaminačního	dekontaminační	k2eAgInSc2d1	dekontaminační
materiálu	materiál	k1gInSc2	materiál
odrazem	odraz	k1gInSc7	odraz
nebo	nebo	k8xC	nebo
stečením	stečení	k1gNnSc7	stečení
z	z	k7c2	z
povrchů	povrch	k1gInPc2	povrch
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnSc1d2	delší
doba	doba	k1gFnSc1	doba
působení	působení	k1gNnSc2	působení
(	(	kIx(	(
<g/>
relativně	relativně	k6eAd1	relativně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
doba	doba	k1gFnSc1	doba
rozpadu	rozpad	k1gInSc2	rozpad
pěny	pěna	k1gFnSc2	pěna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomalý	pomalý	k2eAgInSc1d1	pomalý
úbytek	úbytek	k1gInSc1	úbytek
dekontaminační	dekontaminační	k2eAgFnSc2d1	dekontaminační
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
směsi	směs	k1gFnSc2	směs
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
vizuální	vizuální	k2eAgFnSc2d1	vizuální
kontroly	kontrola	k1gFnSc2	kontrola
nanesení	nanesení	k1gNnSc2	nanesení
a	a	k8xC	a
překrytí	překrytí	k1gNnSc2	překrytí
kontaminovaného	kontaminovaný	k2eAgNnSc2d1	kontaminované
místa	místo	k1gNnSc2	místo
pěnou	pěna	k1gFnSc7	pěna
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dekontaminační	dekontaminační	k2eAgFnPc1d1	dekontaminační
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
vybrané	vybraný	k2eAgFnPc1d1	vybraná
chemikálie	chemikálie	k1gFnPc1	chemikálie
schopné	schopný	k2eAgFnPc1d1	schopná
reakce	reakce	k1gFnPc1	reakce
s	s	k7c7	s
kontaminanty	kontaminant	k1gInPc7	kontaminant
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
méně	málo	k6eAd2	málo
toxických	toxický	k2eAgFnPc2d1	toxická
nebo	nebo	k8xC	nebo
netoxických	toxický	k2eNgFnPc2d1	netoxická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mohou	moct	k5eAaImIp3nP	moct
umožnit	umožnit	k5eAaPmF	umožnit
nebo	nebo	k8xC	nebo
usnadnit	usnadnit	k5eAaPmF	usnadnit
odstranění	odstranění	k1gNnSc4	odstranění
kontaminantů	kontaminant	k1gInPc2	kontaminant
z	z	k7c2	z
povrchů	povrch	k1gInPc2	povrch
nebo	nebo	k8xC	nebo
způsobit	způsobit	k5eAaPmF	způsobit
smrt	smrt	k1gFnSc4	smrt
patogenních	patogenní	k2eAgMnPc2d1	patogenní
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Dekontaminační	dekontaminační	k2eAgFnPc1d1	dekontaminační
směsi	směs	k1gFnPc1	směs
jsou	být	k5eAaImIp3nP	být
roztoky	roztok	k1gInPc4	roztok
<g/>
,	,	kIx,	,
suspenze	suspenze	k1gFnPc4	suspenze
<g/>
,	,	kIx,	,
koloidní	koloidní	k2eAgFnPc4d1	koloidní
roztoky	roztoka	k1gFnPc4	roztoka
a	a	k8xC	a
pevné	pevný	k2eAgFnPc4d1	pevná
směsi	směs	k1gFnPc4	směs
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
připravovány	připravovat	k5eAaImNgFnP	připravovat
z	z	k7c2	z
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
a	a	k8xC	a
případně	případně	k6eAd1	případně
stabilizátorů	stabilizátor	k1gInPc2	stabilizátor
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
k	k	k7c3	k
provádění	provádění	k1gNnSc3	provádění
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Dekontaminační	dekontaminační	k2eAgFnPc1d1	dekontaminační
látky	látka	k1gFnPc1	látka
a	a	k8xC	a
směsi	směs	k1gFnPc1	směs
významně	významně	k6eAd1	významně
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
účinnost	účinnost	k1gFnSc4	účinnost
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojeny	spojen	k2eAgInPc1d1	spojen
následující	následující	k2eAgInPc1d1	následující
problémy	problém	k1gInPc1	problém
<g/>
:	:	kIx,	:
neexistence	neexistence	k1gFnSc1	neexistence
ideální	ideální	k2eAgFnSc2d1	ideální
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
směsi	směs	k1gFnSc2	směs
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
účinnost	účinnost	k1gFnSc4	účinnost
při	při	k7c6	při
dekontaminaci	dekontaminace	k1gFnSc6	dekontaminace
chemických	chemický	k2eAgFnPc2d1	chemická
<g/>
,	,	kIx,	,
biologických	biologický	k2eAgFnPc2d1	biologická
i	i	k8xC	i
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
obtížná	obtížný	k2eAgFnSc1d1	obtížná
aplikace	aplikace	k1gFnSc1	aplikace
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
(	(	kIx(	(
<g/>
nutnost	nutnost	k1gFnSc4	nutnost
ohřevu	ohřev	k1gInSc2	ohřev
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
agresivita	agresivita	k1gFnSc1	agresivita
a	a	k8xC	a
nestabilní	stabilní	k2eNgFnPc1d1	nestabilní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
některých	některý	k3yIgFnPc2	některý
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
nutnost	nutnost	k1gFnSc1	nutnost
dodržení	dodržení	k1gNnSc2	dodržení
nezbytné	nezbytný	k2eAgFnSc2d1	nezbytná
doby	doba	k1gFnSc2	doba
působení	působení	k1gNnSc2	působení
dekontaminační	dekontaminační	k2eAgFnSc2d1	dekontaminační
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
směsi	směs	k1gFnSc2	směs
<g/>
,	,	kIx,	,
případné	případný	k2eAgInPc4d1	případný
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
při	při	k7c6	při
likvidaci	likvidace	k1gFnSc6	likvidace
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
dekontaminační	dekontaminační	k2eAgFnPc1d1	dekontaminační
technologie	technologie	k1gFnPc1	technologie
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc4	jejich
účinnost	účinnost	k1gFnSc1	účinnost
ověřovala	ověřovat	k5eAaImAgFnS	ověřovat
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
nových	nový	k2eAgFnPc2d1	nová
a	a	k8xC	a
nebezpečnějších	bezpečný	k2eNgFnPc2d2	nebezpečnější
vojenských	vojenský	k2eAgFnPc2d1	vojenská
bojových	bojový	k2eAgFnPc2d1	bojová
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
technologie	technologie	k1gFnPc1	technologie
byly	být	k5eAaImAgFnP	být
primárně	primárně	k6eAd1	primárně
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
povrchu	povrch	k1gInSc2	povrch
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
sekundárně	sekundárně	k6eAd1	sekundárně
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
pozornost	pozornost	k1gFnSc1	pozornost
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
způsobům	způsob	k1gInPc3	způsob
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
elektronických	elektronický	k2eAgMnPc2d1	elektronický
přístrojů	přístroj	k1gInPc2	přístroj
nebo	nebo	k8xC	nebo
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
citlivé	citlivý	k2eAgFnSc6d1	citlivá
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nemožnosti	nemožnost	k1gFnSc3	nemožnost
využití	využití	k1gNnSc2	využití
metod	metoda	k1gFnPc2	metoda
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
na	na	k7c6	na
kapalných	kapalný	k2eAgInPc6d1	kapalný
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
oxidačních	oxidační	k2eAgInPc6d1	oxidační
nebo	nebo	k8xC	nebo
korozivních	korozivní	k2eAgInPc6d1	korozivní
přípravcích	přípravek	k1gInPc6	přípravek
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyvinout	vyvinout	k5eAaPmF	vyvinout
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
případy	případ	k1gInPc4	případ
speciální	speciální	k2eAgFnSc1d1	speciální
technologie	technologie	k1gFnSc1	technologie
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
snižující	snižující	k2eAgFnSc1d1	snižující
vojenské	vojenský	k2eAgNnSc4d1	vojenské
ohrožení	ohrožení	k1gNnSc4	ohrožení
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
zvyšující	zvyšující	k2eAgNnSc1d1	zvyšující
se	se	k3xPyFc4	se
využívání	využívání	k1gNnSc1	využívání
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
látek	látka	k1gFnPc2	látka
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
použití	použití	k1gNnSc4	použití
měly	mít	k5eAaImAgInP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vydělení	vydělení	k1gNnSc2	vydělení
vhodných	vhodný	k2eAgFnPc2d1	vhodná
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
technologií	technologie	k1gFnPc2	technologie
pro	pro	k7c4	pro
civilní	civilní	k2eAgNnSc4d1	civilní
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
a	a	k8xC	a
vývoji	vývoj	k1gInSc3	vývoj
nových	nový	k2eAgFnPc2d1	nová
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
využívaných	využívaný	k2eAgFnPc2d1	využívaná
nebo	nebo	k8xC	nebo
vědecky	vědecky	k6eAd1	vědecky
vyvíjených	vyvíjený	k2eAgFnPc2d1	vyvíjená
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
technologií	technologie	k1gFnPc2	technologie
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
absorpce	absorpce	k1gFnSc1	absorpce
kontaminantu	kontaminant	k1gInSc2	kontaminant
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nátěru	nátěr	k1gInSc2	nátěr
nebo	nebo	k8xC	nebo
pronikání	pronikání	k1gNnSc4	pronikání
kontaminantu	kontaminant	k1gInSc2	kontaminant
do	do	k7c2	do
polymerní	polymerní	k2eAgFnSc2d1	polymerní
struktury	struktura	k1gFnSc2	struktura
nátěru	nátěr	k1gInSc2	nátěr
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
<g/>
:	:	kIx,	:
jednorázové	jednorázový	k2eAgFnPc1d1	jednorázová
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
snímatelné	snímatelný	k2eAgInPc1d1	snímatelný
nátěry	nátěr	k1gInPc1	nátěr
<g/>
,	,	kIx,	,
nátěry	nátěr	k1gInPc1	nátěr
odolné	odolný	k2eAgInPc1d1	odolný
proti	proti	k7c3	proti
chemickému	chemický	k2eAgNnSc3d1	chemické
působení	působení	k1gNnSc3	působení
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
dekontaminačního	dekontaminační	k2eAgInSc2d1	dekontaminační
přípravku	přípravek	k1gInSc2	přípravek
<g/>
,	,	kIx,	,
nátěry	nátěr	k1gInPc4	nátěr
schopné	schopný	k2eAgFnSc2d1	schopná
samodekontaminace	samodekontaminace	k1gFnSc2	samodekontaminace
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Superkritickou	Superkritický	k2eAgFnSc4d1	Superkritická
kapalinu	kapalina	k1gFnSc4	kapalina
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
kapalnou	kapalný	k2eAgFnSc4d1	kapalná
sloučeninu	sloučenina	k1gFnSc4	sloučenina
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
teplota	teplota	k1gFnSc1	teplota
nebo	nebo	k8xC	nebo
tlak	tlak	k1gInSc1	tlak
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
hodnoty	hodnota	k1gFnPc1	hodnota
tzv.	tzv.	kA	tzv.
kritického	kritický	k2eAgInSc2d1	kritický
tlaku	tlak	k1gInSc2	tlak
nebo	nebo	k8xC	nebo
kritické	kritický	k2eAgFnSc2d1	kritická
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
teplota	teplota	k1gFnSc1	teplota
kapaliny	kapalina	k1gFnSc2	kapalina
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
hustota	hustota	k1gFnSc1	hustota
se	se	k3xPyFc4	se
sníží	snížit	k5eAaPmIp3nS	snížit
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
tlak	tlak	k1gInSc1	tlak
plynné	plynný	k2eAgFnSc2d1	plynná
fáze	fáze	k1gFnSc2	fáze
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hustota	hustota	k1gFnSc1	hustota
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kritickém	kritický	k2eAgInSc6d1	kritický
bodě	bod	k1gInSc6	bod
se	se	k3xPyFc4	se
hustoty	hustota	k1gFnPc1	hustota
stávají	stávat	k5eAaImIp3nP	stávat
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
a	a	k8xC	a
superkritická	superkritický	k2eAgFnSc1d1	superkritická
kapalina	kapalina	k1gFnSc1	kapalina
tedy	tedy	k9	tedy
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
mezi	mezi	k7c7	mezi
plynem	plyn	k1gInSc7	plyn
a	a	k8xC	a
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
difundovat	difundovat	k5eAaImF	difundovat
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
látek	látka	k1gFnPc2	látka
jako	jako	k8xC	jako
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
rozpouští	rozpouštět	k5eAaImIp3nP	rozpouštět
materiály	materiál	k1gInPc1	materiál
jako	jako	k8xC	jako
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tedy	tedy	k8xC	tedy
výbornými	výborný	k2eAgNnPc7d1	výborné
rozpouštědly	rozpouštědlo	k1gNnPc7	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
změny	změna	k1gFnPc1	změna
teploty	teplota	k1gFnSc2	teplota
nebo	nebo	k8xC	nebo
tlaku	tlak	k1gInSc2	tlak
blízko	blízko	k6eAd1	blízko
jejich	jejich	k3xOp3gFnPc4	jejich
kritické	kritický	k2eAgFnPc4d1	kritická
hodnoty	hodnota	k1gFnPc4	hodnota
způsobí	způsobit	k5eAaPmIp3nS	způsobit
velké	velký	k2eAgFnPc4d1	velká
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
hustotě	hustota	k1gFnSc6	hustota
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
i	i	k9	i
změny	změna	k1gFnPc1	změna
vlastností	vlastnost	k1gFnPc2	vlastnost
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
rozpustnost	rozpustnost	k1gFnSc1	rozpustnost
různých	různý	k2eAgFnPc2d1	různá
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
vlastnosti	vlastnost	k1gFnSc3	vlastnost
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
selektivní	selektivní	k2eAgFnSc6d1	selektivní
separaci	separace	k1gFnSc6	separace
chemického	chemický	k2eAgInSc2d1	chemický
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvhodnějším	vhodný	k2eAgMnPc3d3	nejvhodnější
patří	patřit	k5eAaImIp3nS	patřit
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
Tkrit	Tkrit	k1gInSc1	Tkrit
<g/>
=	=	kIx~	=
<g/>
374,4	[number]	k4	374,4
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
pkrit	pkrit	k1gMnSc1	pkrit
<g/>
=	=	kIx~	=
<g/>
219,5	[number]	k4	219,5
bar	bar	k1gInSc1	bar
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
(	(	kIx(	(
<g/>
Tkrit	Tkrit	k1gInSc1	Tkrit
<g/>
=	=	kIx~	=
<g/>
31	[number]	k4	31
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
pkrit	pkrit	k1gMnSc1	pkrit
<g/>
=	=	kIx~	=
<g/>
73	[number]	k4	73
bar	bar	k1gInSc1	bar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
bojových	bojový	k2eAgFnPc2d1	bojová
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
v	v	k7c6	v
superkritickém	superkritický	k2eAgInSc6d1	superkritický
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
citlivých	citlivý	k2eAgInPc2d1	citlivý
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
omezením	omezení	k1gNnSc7	omezení
je	být	k5eAaImIp3nS	být
dosažení	dosažení	k1gNnSc4	dosažení
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
tlaku	tlak	k1gInSc2	tlak
nebo	nebo	k8xC	nebo
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
finanční	finanční	k2eAgFnSc4d1	finanční
náročnost	náročnost	k1gFnSc4	náročnost
na	na	k7c4	na
vybavení	vybavení	k1gNnSc2	vybavení
technickými	technický	k2eAgInPc7d1	technický
prostředky	prostředek	k1gInPc7	prostředek
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
těchto	tento	k3xDgFnPc2	tento
technologií	technologie	k1gFnPc2	technologie
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
odstraňování	odstraňování	k1gNnSc4	odstraňování
kontaminantů	kontaminant	k1gInPc2	kontaminant
z	z	k7c2	z
povrchů	povrch	k1gInPc2	povrch
materiálů	materiál	k1gInPc2	materiál
změnou	změna	k1gFnSc7	změna
jejich	jejich	k3xOp3gNnSc2	jejich
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
kontaminantu	kontaminant	k1gInSc2	kontaminant
jsou	být	k5eAaImIp3nP	být
vzduchem	vzduch	k1gInSc7	vzduch
zřeďovány	zřeďován	k2eAgInPc1d1	zřeďován
a	a	k8xC	a
rozptylovány	rozptylován	k2eAgInPc1d1	rozptylován
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
určují	určovat	k5eAaImIp3nP	určovat
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
účinnost	účinnost	k1gFnSc4	účinnost
této	tento	k3xDgFnSc2	tento
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
poréznost	poréznost	k1gFnSc1	poréznost
a	a	k8xC	a
nasákavost	nasákavost	k1gFnSc1	nasákavost
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
těkavost	těkavost	k1gFnSc1	těkavost
kontaminantu	kontaminant	k1gInSc2	kontaminant
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
je	být	k5eAaImIp3nS	být
prohřívání	prohřívání	k1gNnSc1	prohřívání
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
materiálu	materiál	k1gInSc2	materiál
proudem	proud	k1gInSc7	proud
horkých	horký	k2eAgInPc2d1	horký
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vytváření	vytváření	k1gNnSc4	vytváření
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
difúzi	difúze	k1gFnSc4	difúze
kontaminantu	kontaminant	k1gInSc2	kontaminant
ze	z	k7c2	z
struktury	struktura	k1gFnSc2	struktura
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
použití	použití	k1gNnSc4	použití
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
využití	využití	k1gNnSc1	využití
horkých	horký	k2eAgFnPc2d1	horká
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
proudového	proudový	k2eAgInSc2d1	proudový
leteckého	letecký	k2eAgInSc2d1	letecký
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dávkování	dávkování	k1gNnSc6	dávkování
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
proudu	proud	k1gInSc2	proud
spalin	spaliny	k1gFnPc2	spaliny
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
využít	využít	k5eAaPmF	využít
i	i	k9	i
pro	pro	k7c4	pro
dezaktivaci	dezaktivace	k1gFnSc4	dezaktivace
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnPc1	částice
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
unášeny	unášen	k2eAgInPc1d1	unášen
plyny	plyn	k1gInPc1	plyn
rychlostí	rychlost	k1gFnSc7	rychlost
kolem	kolem	k7c2	kolem
275	[number]	k4	275
m.	m.	k?	m.
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
a	a	k8xC	a
disponují	disponovat	k5eAaBmIp3nP	disponovat
proto	proto	k8xC	proto
vysokou	vysoký	k2eAgFnSc7d1	vysoká
kinetickou	kinetický	k2eAgFnSc7d1	kinetická
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
částice	částice	k1gFnSc2	částice
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
kontaminantu	kontaminant	k1gInSc2	kontaminant
překoná	překonat	k5eAaPmIp3nS	překonat
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
adhezní	adhezní	k2eAgFnPc1d1	adhezní
sily	sít	k5eAaImAgFnP	sít
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
kontaminant	kontaminant	k1gInSc1	kontaminant
poután	poután	k2eAgInSc1d1	poután
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
odstranění	odstranění	k1gNnSc3	odstranění
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
proudu	proud	k1gInSc2	proud
spalin	spaliny	k1gFnPc2	spaliny
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
dávkovat	dávkovat	k5eAaImF	dávkovat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
kapalné	kapalný	k2eAgFnPc4d1	kapalná
dekontaminační	dekontaminační	k2eAgFnPc4d1	dekontaminační
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
směsi	směs	k1gFnPc4	směs
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
patří	patřit	k5eAaImIp3nS	patřit
povrchově	povrchově	k6eAd1	povrchově
nerovnoměrná	rovnoměrný	k2eNgFnSc1d1	nerovnoměrná
účinnost	účinnost	k1gFnSc1	účinnost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
tvarově	tvarově	k6eAd1	tvarově
členitého	členitý	k2eAgInSc2d1	členitý
povrchu	povrch	k1gInSc2	povrch
dekontaminovaného	dekontaminovaný	k2eAgNnSc2d1	dekontaminovaný
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
je	být	k5eAaImIp3nS	být
také	také	k9	také
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
rozptylování	rozptylování	k1gNnSc1	rozptylování
kontaminantu	kontaminant	k1gInSc2	kontaminant
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
unášen	unášet	k5eAaImNgInS	unášet
proudem	proud	k1gInSc7	proud
spalin	spaliny	k1gFnPc2	spaliny
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektrochemické	elektrochemický	k2eAgInPc1d1	elektrochemický
procesy	proces	k1gInPc1	proces
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c4	na
elektrolytické	elektrolytický	k2eAgFnPc4d1	elektrolytická
schopnosti	schopnost	k1gFnPc4	schopnost
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
oxidovat	oxidovat	k5eAaBmF	oxidovat
nebo	nebo	k8xC	nebo
redukovat	redukovat	k5eAaBmF	redukovat
chemické	chemický	k2eAgFnPc4d1	chemická
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
formě	forma	k1gFnSc6	forma
anebo	anebo	k8xC	anebo
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
dané	daný	k2eAgFnSc2d1	daná
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
elektrod	elektroda	k1gFnPc2	elektroda
je	být	k5eAaImIp3nS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
elektrické	elektrický	k2eAgNnSc1d1	elektrické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
poli	pole	k1gNnSc6	pole
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
anodě	anoda	k1gFnSc6	anoda
k	k	k7c3	k
oxidačním	oxidační	k2eAgInPc3d1	oxidační
procesům	proces	k1gInPc3	proces
a	a	k8xC	a
na	na	k7c6	na
katodě	katoda	k1gFnSc6	katoda
k	k	k7c3	k
redukčním	redukční	k2eAgMnPc3d1	redukční
procesům	proces	k1gInPc3	proces
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
je	být	k5eAaImIp3nS	být
rozložení	rozložení	k1gNnSc1	rozložení
molekuly	molekula	k1gFnSc2	molekula
daného	daný	k2eAgInSc2d1	daný
kontaminantu	kontaminant	k1gInSc2	kontaminant
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
odstranění	odstranění	k1gNnSc3	odstranění
toxicity	toxicita	k1gFnSc2	toxicita
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
využitelná	využitelný	k2eAgFnSc1d1	využitelná
obzvláště	obzvláště	k6eAd1	obzvláště
pro	pro	k7c4	pro
půdu	půda	k1gFnSc4	půda
kontaminovanou	kontaminovaný	k2eAgFnSc4d1	kontaminovaná
chemickými	chemický	k2eAgFnPc7d1	chemická
látkami	látka	k1gFnPc7	látka
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
činností	činnost	k1gFnPc2	činnost
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
uvolnění	uvolnění	k1gNnSc1	uvolnění
kontaminantu	kontaminant	k1gInSc2	kontaminant
z	z	k7c2	z
daného	daný	k2eAgInSc2d1	daný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
následné	následný	k2eAgNnSc4d1	následné
přemístění	přemístění	k1gNnSc4	přemístění
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
především	především	k9	především
vlastností	vlastnost	k1gFnSc7	vlastnost
stlačeného	stlačený	k2eAgInSc2d1	stlačený
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
principů	princip	k1gInPc2	princip
ultrazvukového	ultrazvukový	k2eAgNnSc2d1	ultrazvukové
čištění	čištění	k1gNnSc2	čištění
a	a	k8xC	a
reverzní	reverzní	k2eAgFnSc2d1	reverzní
osmózy	osmóza	k1gFnSc2	osmóza
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
tlaková	tlakový	k2eAgFnSc1d1	tlaková
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejpoužívanější	používaný	k2eAgNnSc4d3	nejpoužívanější
médium	médium	k1gNnSc4	médium
pro	pro	k7c4	pro
dekontaminační	dekontaminační	k2eAgInPc4d1	dekontaminační
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
detergentů	detergent	k1gInPc2	detergent
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
snižují	snižovat	k5eAaImIp3nP	snižovat
povrchové	povrchový	k2eAgNnSc4d1	povrchové
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
smývání	smývání	k1gNnSc4	smývání
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
významně	významně	k6eAd1	významně
zvýšit	zvýšit	k5eAaPmF	zvýšit
účinnost	účinnost	k1gFnSc4	účinnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
faktory	faktor	k1gInPc4	faktor
ovlivňující	ovlivňující	k2eAgFnSc1d1	ovlivňující
účinnost	účinnost	k1gFnSc1	účinnost
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
tlak	tlak	k1gInSc1	tlak
použité	použitý	k2eAgFnSc2d1	použitá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
místa	místo	k1gNnSc2	místo
vzniku	vznik	k1gInSc2	vznik
vodního	vodní	k2eAgInSc2d1	vodní
paprsku	paprsek	k1gInSc2	paprsek
od	od	k7c2	od
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
charakter	charakter	k1gInSc1	charakter
vodního	vodní	k2eAgInSc2d1	vodní
paprsku	paprsek	k1gInSc2	paprsek
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc4	množství
použité	použitý	k2eAgFnSc2d1	použitá
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
poréznost	poréznost	k1gFnSc1	poréznost
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
využití	využití	k1gNnSc6	využití
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
omezit	omezit	k5eAaPmF	omezit
její	její	k3xOp3gNnSc4	její
rychlé	rychlý	k2eAgNnSc4d1	rychlé
ochlazení	ochlazení	k1gNnSc4	ochlazení
při	při	k7c6	při
expanzi	expanze	k1gFnSc6	expanze
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
snížení	snížení	k1gNnSc3	snížení
účinnosti	účinnost	k1gFnSc2	účinnost
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Efektivnější	efektivní	k2eAgNnSc1d2	efektivnější
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
přehřáté	přehřátý	k2eAgFnSc2d1	přehřátá
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
účinněji	účinně	k6eAd2	účinně
předává	předávat	k5eAaImIp3nS	předávat
teplo	teplo	k6eAd1	teplo
kontaminovanému	kontaminovaný	k2eAgInSc3d1	kontaminovaný
materiálu	materiál	k1gInSc3	materiál
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
difúzi	difúze	k1gFnSc4	difúze
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
smývá	smývat	k5eAaImIp3nS	smývat
kontaminant	kontaminant	k1gInSc4	kontaminant
kondenzovanou	kondenzovaný	k2eAgFnSc7d1	kondenzovaná
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reverzní	reverzní	k2eAgFnSc1d1	reverzní
osmóza	osmóza	k1gFnSc1	osmóza
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
transport	transport	k1gInSc4	transport
rozpouštědla	rozpouštědlo	k1gNnSc2	rozpouštědlo
přes	přes	k7c4	přes
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
látky	látka	k1gFnSc2	látka
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
aplikaci	aplikace	k1gFnSc4	aplikace
vnějšího	vnější	k2eAgInSc2d1	vnější
tlaku	tlak	k1gInSc2	tlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
koncentrovanějšího	koncentrovaný	k2eAgInSc2d2	koncentrovanější
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
obrácení	obrácení	k1gNnSc4	obrácení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jevu	jev	k1gInSc2	jev
osmózy	osmóza	k1gFnSc2	osmóza
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
polopropustná	polopropustný	k2eAgFnSc1d1	polopropustná
(	(	kIx(	(
<g/>
propustná	propustný	k2eAgFnSc1d1	propustná
pro	pro	k7c4	pro
rozpouštědlo	rozpouštědlo	k1gNnSc4	rozpouštědlo
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
pro	pro	k7c4	pro
rozpuštěné	rozpuštěný	k2eAgFnPc4d1	rozpuštěná
látky	látka	k1gFnPc4	látka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
aplikovaný	aplikovaný	k2eAgInSc1d1	aplikovaný
tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
látky	látka	k1gFnSc2	látka
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
přirozený	přirozený	k2eAgInSc4d1	přirozený
tlak	tlak	k1gInSc4	tlak
osmotický	osmotický	k2eAgInSc4d1	osmotický
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
obrácení	obrácení	k1gNnSc3	obrácení
osmotického	osmotický	k2eAgInSc2d1	osmotický
jevu	jev	k1gInSc2	jev
a	a	k8xC	a
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
bude	být	k5eAaImBp3nS	být
membránou	membrána	k1gFnSc7	membrána
procházet	procházet	k5eAaImF	procházet
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
dekontaminaci	dekontaminace	k1gFnSc3	dekontaminace
vody	voda	k1gFnSc2	voda
od	od	k7c2	od
chemických	chemický	k2eAgFnPc2d1	chemická
<g/>
,	,	kIx,	,
biologických	biologický	k2eAgFnPc2d1	biologická
a	a	k8xC	a
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
ultrazvukového	ultrazvukový	k2eAgNnSc2d1	ultrazvukové
(	(	kIx(	(
<g/>
zvukové	zvukový	k2eAgNnSc4d1	zvukové
vlnění	vlnění	k1gNnSc4	vlnění
nad	nad	k7c7	nad
prahem	práh	k1gInSc7	práh
slyšitelnosti	slyšitelnost	k1gFnSc2	slyšitelnost
<g/>
)	)	kIx)	)
čištění	čištění	k1gNnSc2	čištění
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
především	především	k9	především
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
obtížně	obtížně	k6eAd1	obtížně
přístupných	přístupný	k2eAgInPc2d1	přístupný
povrchů	povrch	k1gInPc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ultrazvuk	ultrazvuk	k1gInSc1	ultrazvuk
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
v	v	k7c6	v
čistícím	čistící	k2eAgNnSc6d1	čistící
médiu	médium	k1gNnSc6	médium
vznik	vznik	k1gInSc4	vznik
kavitací	kavitace	k1gFnPc2	kavitace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podstatou	podstata	k1gFnSc7	podstata
samotného	samotný	k2eAgNnSc2d1	samotné
čištění	čištění	k1gNnSc2	čištění
<g/>
.	.	kIx.	.
</s>
<s>
Kavitace	kavitace	k1gFnSc1	kavitace
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc4	vznik
dutin	dutina	k1gFnPc2	dutina
v	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
při	při	k7c6	při
lokálním	lokální	k2eAgInSc6d1	lokální
poklesu	pokles	k1gInSc6	pokles
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
hydrodynamický	hydrodynamický	k2eAgMnSc1d1	hydrodynamický
anebo	anebo	k8xC	anebo
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
akustický	akustický	k2eAgInSc4d1	akustický
<g/>
)	)	kIx)	)
následovaný	následovaný	k2eAgMnSc1d1	následovaný
jejich	jejich	k3xOp3gFnSc7	jejich
implozí	imploze	k1gFnSc7	imploze
<g/>
.	.	kIx.	.
</s>
<s>
Kavitace	kavitace	k1gFnSc1	kavitace
zpočátku	zpočátku	k6eAd1	zpočátku
vyplněna	vyplnit	k5eAaPmNgFnS	vyplnit
vakuem	vakuum	k1gNnSc7	vakuum
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
mohou	moct	k5eAaImIp3nP	moct
difundovat	difundovat	k5eAaImF	difundovat
plyny	plyn	k1gInPc7	plyn
z	z	k7c2	z
okolní	okolní	k2eAgFnSc2d1	okolní
kapaliny	kapalina	k1gFnSc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vymizení	vymizení	k1gNnSc6	vymizení
podtlaku	podtlak	k1gInSc2	podtlak
kavitace	kavitace	k1gFnSc2	kavitace
zkolabuje	zkolabovat	k5eAaPmIp3nS	zkolabovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
rázové	rázový	k2eAgFnSc2d1	rázová
vlny	vlna	k1gFnSc2	vlna
s	s	k7c7	s
destruktivním	destruktivní	k2eAgInSc7d1	destruktivní
účinkem	účinek	k1gInSc7	účinek
na	na	k7c4	na
okolní	okolní	k2eAgInSc4d1	okolní
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlny	vlna	k1gFnPc1	vlna
potom	potom	k6eAd1	potom
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
kontaminant	kontaminant	k1gInSc4	kontaminant
(	(	kIx(	(
<g/>
existence	existence	k1gFnSc1	existence
rizika	riziko	k1gNnSc2	riziko
poškození	poškození	k1gNnSc2	poškození
materiálu	materiál	k1gInSc2	materiál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
kavitace	kavitace	k1gFnSc2	kavitace
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
především	především	k6eAd1	především
velikostí	velikost	k1gFnSc7	velikost
podtlaku	podtlak	k1gInSc2	podtlak
<g/>
,	,	kIx,	,
povrchovým	povrchový	k2eAgInSc7d1	povrchový
napětí	napětí	k1gNnSc4	napětí
kapaliny	kapalina	k1gFnSc2	kapalina
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
stlačeným	stlačený	k2eAgInSc7d1	stlačený
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
expanzi	expanze	k1gFnSc4	expanze
tohoto	tento	k3xDgInSc2	tento
plynu	plyn	k1gInSc2	plyn
přes	přes	k7c4	přes
asymetrickou	asymetrický	k2eAgFnSc4d1	asymetrická
Venturiho	Venturi	k1gMnSc4	Venturi
dýzu	dýzus	k1gInSc2	dýzus
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
částečky	částečka	k1gFnPc1	částečka
suchého	suchý	k2eAgInSc2d1	suchý
ledu	led	k1gInSc2	led
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
kontaminant	kontaminant	k1gInSc1	kontaminant
pomocí	pomocí	k7c2	pomocí
své	svůj	k3xOyFgFnSc2	svůj
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
využitelný	využitelný	k2eAgInSc1d1	využitelný
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
čištění	čištění	k1gNnSc4	čištění
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
povrchů	povrch	k1gInPc2	povrch
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odvětrávání	odvětrávání	k1gNnSc1	odvětrávání
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
několik	několik	k4yIc4	několik
principů	princip	k1gInPc2	princip
čištění	čištění	k1gNnSc2	čištění
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
fyzikálních	fyzikální	k2eAgFnPc6d1	fyzikální
zákonitostech	zákonitost	k1gFnPc6	zákonitost
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
využívání	využívání	k1gNnSc6	využívání
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
vody	voda	k1gFnPc1	voda
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pasivní	pasivní	k2eAgInSc4d1	pasivní
způsob	způsob	k1gInSc4	způsob
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
odstraňující	odstraňující	k2eAgInSc1d1	odstraňující
kontaminant	kontaminant	k1gInSc1	kontaminant
odvětráváním	odvětrávání	k1gNnSc7	odvětrávání
<g/>
,	,	kIx,	,
rozkladem	rozklad	k1gInSc7	rozklad
<g/>
,	,	kIx,	,
hydrolýzou	hydrolýza	k1gFnSc7	hydrolýza
nebo	nebo	k8xC	nebo
fotolýzou	fotolýza	k1gFnSc7	fotolýza
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
využitelný	využitelný	k2eAgInSc1d1	využitelný
především	především	k9	především
při	při	k7c6	při
dekontaminaci	dekontaminace	k1gFnSc6	dekontaminace
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
ploch	plocha	k1gFnPc2	plocha
terénu	terén	k1gInSc2	terén
a	a	k8xC	a
mobilní	mobilní	k2eAgFnSc2d1	mobilní
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
polyuretanové	polyuretanový	k2eAgInPc1d1	polyuretanový
nátěry	nátěr	k1gInPc1	nátěr
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
účinnému	účinný	k2eAgNnSc3d1	účinné
provedení	provedení	k1gNnSc3	provedení
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nezbytný	zbytný	k2eNgInSc1d1	zbytný
i	i	k8xC	i
dostatek	dostatek	k1gInSc1	dostatek
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
účinnost	účinnost	k1gFnSc4	účinnost
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
stálost	stálost	k1gFnSc1	stálost
chemického	chemický	k2eAgInSc2d1	chemický
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnSc2	vlastnost
a	a	k8xC	a
charakter	charakter	k1gInSc4	charakter
terénu	terén	k1gInSc2	terén
nebo	nebo	k8xC	nebo
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
počasí	počasí	k1gNnSc2	počasí
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
využívá	využívat	k5eAaImIp3nS	využívat
schopnost	schopnost	k1gFnSc4	schopnost
speciálních	speciální	k2eAgInPc2d1	speciální
materiálů	materiál	k1gInPc2	materiál
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
z	z	k7c2	z
povrchů	povrch	k1gInPc2	povrch
kapalný	kapalný	k2eAgInSc4d1	kapalný
kontaminant	kontaminant	k1gInSc4	kontaminant
jeho	jeho	k3xOp3gFnPc2	jeho
absorpcí	absorpce	k1gFnPc2	absorpce
do	do	k7c2	do
porézní	porézní	k2eAgFnSc2d1	porézní
struktury	struktura	k1gFnSc2	struktura
absorbentu	absorbent	k1gInSc2	absorbent
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
sorbentů	sorbent	k1gInPc2	sorbent
(	(	kIx(	(
<g/>
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
polymerní	polymerní	k2eAgFnPc1d1	polymerní
<g/>
,	,	kIx,	,
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
<g/>
,	,	kIx,	,
katalytické	katalytický	k2eAgFnPc1d1	katalytická
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
sorbenty	sorbenta	k1gFnPc4	sorbenta
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
obecně	obecně	k6eAd1	obecně
látky	látka	k1gFnPc1	látka
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
sorpční	sorpční	k2eAgFnSc7d1	sorpční
schopností	schopnost	k1gFnSc7	schopnost
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
povrchu	povrch	k1gInSc6	povrch
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
sorbenty	sorbenta	k1gFnPc1	sorbenta
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
sorbentu	sorbent	k1gInSc2	sorbent
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
chemickou	chemický	k2eAgFnSc4d1	chemická
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
sorbent	sorbent	k1gInSc1	sorbent
kontaminant	kontaminant	k1gInSc1	kontaminant
zachytí	zachytit	k5eAaPmIp3nS	zachytit
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
chemická	chemický	k2eAgFnSc1d1	chemická
reakce	reakce	k1gFnSc1	reakce
kontaminant	kontaminant	k1gInSc1	kontaminant
detoxikuje	detoxikovat	k5eAaBmIp3nS	detoxikovat
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
polymerní	polymerní	k2eAgFnPc4d1	polymerní
sorbenty	sorbenta	k1gFnPc4	sorbenta
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
především	především	k9	především
polymerní	polymerní	k2eAgFnPc1d1	polymerní
pryskyřice	pryskyřice	k1gFnPc1	pryskyřice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
sorpční	sorpční	k2eAgFnSc4d1	sorpční
schopnost	schopnost	k1gFnSc4	schopnost
vůči	vůči	k7c3	vůči
bojovým	bojový	k2eAgFnPc3d1	bojová
chemickým	chemický	k2eAgFnPc3d1	chemická
látkám	látka	k1gFnPc3	látka
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Katalytické	katalytický	k2eAgFnPc1d1	katalytická
sorbenty	sorbenta	k1gFnPc1	sorbenta
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
obdobnými	obdobný	k2eAgFnPc7d1	obdobná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jako	jako	k8xS	jako
reaktivní	reaktivní	k2eAgFnPc4d1	reaktivní
sorbenty	sorbenta	k1gFnPc4	sorbenta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
katalytických	katalytický	k2eAgInPc2d1	katalytický
sorbentů	sorbent	k1gInPc2	sorbent
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
reaktivních	reaktivní	k2eAgNnPc2d1	reaktivní
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
regeneraci	regenerace	k1gFnSc3	regenerace
aktivních	aktivní	k2eAgNnPc2d1	aktivní
center	centrum	k1gNnPc2	centrum
sorbentu	sorbent	k1gInSc2	sorbent
během	během	k7c2	během
vlastního	vlastní	k2eAgInSc2d1	vlastní
průběhu	průběh	k1gInSc2	průběh
detoxikace	detoxikace	k1gFnSc2	detoxikace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
polyoxometaláty	polyoxometalát	k1gInPc4	polyoxometalát
sorbované	sorbovaný	k2eAgInPc4d1	sorbovaný
na	na	k7c6	na
polymerních	polymerní	k2eAgFnPc6d1	polymerní
matricích	matrice	k1gFnPc6	matrice
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodných	vhodný	k2eAgFnPc2d1	vhodná
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
především	především	k6eAd1	především
k	k	k7c3	k
detoxikaci	detoxikace	k1gFnSc3	detoxikace
bojových	bojový	k2eAgFnPc2d1	bojová
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
reaktivních	reaktivní	k2eAgFnPc2d1	reaktivní
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
kontaminanty	kontaminant	k1gInPc7	kontaminant
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
dalších	další	k2eAgInPc2d1	další
zásahů	zásah	k1gInPc2	zásah
(	(	kIx(	(
<g/>
míchání	míchání	k1gNnSc1	míchání
<g/>
,	,	kIx,	,
třepání	třepání	k1gNnSc1	třepání
<g/>
,	,	kIx,	,
ohřev	ohřev	k1gInSc1	ohřev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
těchto	tento	k3xDgFnPc2	tento
technologií	technologie	k1gFnPc2	technologie
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
zvážit	zvážit	k5eAaPmF	zvážit
riziko	riziko	k1gNnSc4	riziko
vzniku	vznik	k1gInSc2	vznik
nebezpečných	bezpečný	k2eNgInPc2d1	nebezpečný
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
při	při	k7c6	při
hydrolytických	hydrolytický	k2eAgInPc6d1	hydrolytický
nebo	nebo	k8xC	nebo
oxidačních	oxidační	k2eAgInPc6d1	oxidační
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
exotermní	exotermní	k2eAgFnPc1d1	exotermní
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
plynné	plynný	k2eAgInPc1d1	plynný
produkty	produkt	k1gInPc1	produkt
detoxikační	detoxikační	k2eAgFnSc2d1	detoxikační
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
chlor	chlor	k1gInSc1	chlor
<g/>
,	,	kIx,	,
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dekontaminační	dekontaminační	k2eAgFnPc1d1	dekontaminační
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
agresivní	agresivní	k2eAgFnPc1d1	agresivní
obzvláště	obzvláště	k6eAd1	obzvláště
ke	k	k7c3	k
kovům	kov	k1gInPc3	kov
a	a	k8xC	a
umělým	umělý	k2eAgFnPc3d1	umělá
hmotám	hmota	k1gFnPc3	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgFnPc1d1	vhodná
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
chemické	chemický	k2eAgFnPc1d1	chemická
reakce	reakce	k1gFnPc1	reakce
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
podstaty	podstata	k1gFnSc2	podstata
průběhu	průběh	k1gInSc2	průběh
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
oxidaci	oxidace	k1gFnSc4	oxidace
<g/>
,	,	kIx,	,
nukleofilní	nukleofilní	k2eAgFnSc4d1	nukleofilní
substituci	substituce	k1gFnSc4	substituce
a	a	k8xC	a
fotochemické	fotochemický	k2eAgFnPc4d1	fotochemická
reakce	reakce	k1gFnPc4	reakce
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fotochemické	fotochemický	k2eAgFnPc1d1	fotochemická
reakce	reakce	k1gFnPc1	reakce
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
interakci	interakce	k1gFnSc3	interakce
s	s	k7c7	s
chemickou	chemický	k2eAgFnSc7d1	chemická
látkou	látka	k1gFnSc7	látka
energii	energie	k1gFnSc4	energie
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
rozklad	rozklad	k1gInSc1	rozklad
(	(	kIx(	(
<g/>
fotolýza	fotolýza	k1gFnSc1	fotolýza
nebo	nebo	k8xC	nebo
fotodekompozice	fotodekompozice	k1gFnSc1	fotodekompozice
<g/>
)	)	kIx)	)
molekul	molekula	k1gFnPc2	molekula
kontaminantů	kontaminant	k1gInPc2	kontaminant
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
organofosfátových	organofosfátův	k2eAgInPc2d1	organofosfátův
nervově	nervově	k6eAd1	nervově
paralytických	paralytický	k2eAgFnPc2d1	paralytická
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nukleofilní	Nukleofilní	k2eAgFnSc1d1	Nukleofilní
substituce	substituce	k1gFnSc1	substituce
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
reakci	reakce	k1gFnSc4	reakce
centrálního	centrální	k2eAgInSc2d1	centrální
atomu	atom	k1gInSc2	atom
fosforu	fosfor	k1gInSc2	fosfor
(	(	kIx(	(
<g/>
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
elektronegativity	elektronegativita	k1gFnSc2	elektronegativita
fosforylového	fosforylový	k2eAgInSc2d1	fosforylový
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
organofosfátových	organofosfátův	k2eAgFnPc2d1	organofosfátův
nervově	nervově	k6eAd1	nervově
paralytických	paralytický	k2eAgFnPc2d1	paralytická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
NPL	NPL	kA	NPL
<g/>
)	)	kIx)	)
místem	místem	k6eAd1	místem
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
elektronovou	elektronový	k2eAgFnSc7d1	elektronová
hustotou	hustota	k1gFnSc7	hustota
<g/>
)	)	kIx)	)
a	a	k8xC	a
volného	volný	k2eAgInSc2d1	volný
elektronového	elektronový	k2eAgInSc2d1	elektronový
páru	pár	k1gInSc2	pár
nukleofilního	nukleofilní	k2eAgNnSc2d1	nukleofilní
činidla	činidlo	k1gNnSc2	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Nukleofilní	Nukleofilní	k2eAgFnSc1d1	Nukleofilní
substituce	substituce	k1gFnSc1	substituce
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
především	především	k9	především
u	u	k7c2	u
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
látek	látka	k1gFnPc2	látka
typu	typ	k1gInSc2	typ
G	G	kA	G
(	(	kIx(	(
<g/>
sarin	sarin	k1gInSc1	sarin
<g/>
,	,	kIx,	,
soman	soman	k1gInSc1	soman
<g/>
,	,	kIx,	,
tabun	tabun	k1gInSc1	tabun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nukleofilní	nukleofilní	k2eAgNnPc4d1	nukleofilní
činidla	činidlo	k1gNnPc4	činidlo
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
hydroxid	hydroxid	k1gInSc1	hydroxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
oximy	oxim	k1gInPc1	oxim
<g/>
,	,	kIx,	,
hydroxamové	hydroxamový	k2eAgFnPc1d1	hydroxamový
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
thiosulfáty	thiosulfát	k1gInPc1	thiosulfát
nebo	nebo	k8xC	nebo
chlornanové	chlornanový	k2eAgInPc1d1	chlornanový
ionty	ion	k1gInPc1	ion
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oxidační	oxidační	k2eAgFnPc1d1	oxidační
reakce	reakce	k1gFnPc1	reakce
využívají	využívat	k5eAaImIp3nP	využívat
úbytku	úbytek	k1gInSc3	úbytek
elektronové	elektronový	k2eAgFnSc2d1	elektronová
hustoty	hustota	k1gFnSc2	hustota
u	u	k7c2	u
atomu	atom	k1gInSc2	atom
vázaného	vázané	k1gNnSc2	vázané
kovalentní	kovalentní	k2eAgFnSc7d1	kovalentní
chemickou	chemický	k2eAgFnSc7d1	chemická
vazbou	vazba	k1gFnSc7	vazba
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1	oxidační
činidla	činidlo	k1gNnPc1	činidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
dekontaminaci	dekontaminace	k1gFnSc6	dekontaminace
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
vodné	vodný	k2eAgInPc4d1	vodný
roztoky	roztok	k1gInPc4	roztok
alkalických	alkalický	k2eAgInPc2d1	alkalický
chlornanů	chlornan	k1gInPc2	chlornan
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
obsažen	obsažen	k2eAgInSc1d1	obsažen
aktivní	aktivní	k2eAgInSc1d1	aktivní
chlor	chlor	k1gInSc1	chlor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
chlornan	chlornan	k1gInSc1	chlornan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
činidla	činidlo	k1gNnPc1	činidlo
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
peroxidu	peroxid	k1gInSc2	peroxid
a	a	k8xC	a
peroxosloučenin	peroxosloučenina	k1gFnPc2	peroxosloučenina
<g/>
,	,	kIx,	,
ozónu	ozón	k1gInSc2	ozón
<g/>
,	,	kIx,	,
manganistanu	manganistan	k1gInSc2	manganistan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
chloraminů	chloramin	k1gInPc2	chloramin
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgNnPc1d1	ostatní
silná	silný	k2eAgNnPc1d1	silné
oxidační	oxidační	k2eAgNnPc1d1	oxidační
činidla	činidlo	k1gNnPc1	činidlo
(	(	kIx(	(
<g/>
kyselina	kyselina	k1gFnSc1	kyselina
peroctová	peroctový	k2eAgFnSc1d1	peroctový
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
ionizujícím	ionizující	k2eAgNnSc7d1	ionizující
zářením	záření	k1gNnSc7	záření
je	být	k5eAaImIp3nS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
principu	princip	k1gInSc6	princip
zničení	zničení	k1gNnSc2	zničení
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc4	jejich
pozměnění	pozměnění	k1gNnSc4	pozměnění
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
plnit	plnit	k5eAaImF	plnit
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
)	)	kIx)	)
molekul	molekula	k1gFnPc2	molekula
deoxyribonukleové	deoxyribonukleový	k2eAgFnSc2d1	deoxyribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ribonukleové	ribonukleový	k2eAgFnSc2d1	ribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
nemohou	moct	k5eNaImIp3nP	moct
živé	živý	k2eAgFnPc1d1	živá
buňky	buňka	k1gFnPc1	buňka
existovat	existovat	k5eAaImF	existovat
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
odumření	odumření	k1gNnSc3	odumření
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
využitelná	využitelný	k2eAgFnSc1d1	využitelná
především	především	k9	především
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
potravin	potravina	k1gFnPc2	potravina
od	od	k7c2	od
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
provádět	provádět	k5eAaImF	provádět
ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
odstíněných	odstíněný	k2eAgFnPc6d1	odstíněná
komorách	komora	k1gFnPc6	komora
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
plazmatem	plazma	k1gNnSc7	plazma
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
suchou	suchý	k2eAgFnSc4d1	suchá
a	a	k8xC	a
nedestruktivní	destruktivní	k2eNgFnSc4d1	nedestruktivní
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gFnSc1	plazma
je	být	k5eAaImIp3nS	být
ionizovaný	ionizovaný	k2eAgInSc1d1	ionizovaný
plyn	plyn	k1gInSc1	plyn
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
i	i	k9	i
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
odtržením	odtržení	k1gNnSc7	odtržení
elektronů	elektron	k1gInPc2	elektron
nebo	nebo	k8xC	nebo
roztržením	roztržení	k1gNnSc7	roztržení
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
ionizací	ionizace	k1gFnPc2	ionizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
a	a	k8xC	a
stejnou	stejný	k2eAgFnSc7d1	stejná
hustotou	hustota	k1gFnSc7	hustota
kladných	kladný	k2eAgInPc2d1	kladný
a	a	k8xC	a
záporných	záporný	k2eAgInPc2d1	záporný
nábojů	náboj	k1gInPc2	náboj
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
reaktivitu	reaktivita	k1gFnSc4	reaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Plazma	plazma	k1gNnSc4	plazma
obsahující	obsahující	k2eAgInSc1d1	obsahující
kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
oxidace	oxidace	k1gFnSc1	oxidace
organických	organický	k2eAgFnPc2d1	organická
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
organofosfátové	organofosfát	k1gMnPc1	organofosfát
NPL	NPL	kA	NPL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
schopnost	schopnost	k1gFnSc4	schopnost
chemicky	chemicky	k6eAd1	chemicky
pozměnit	pozměnit	k5eAaPmF	pozměnit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
detoxikovat	detoxikovat	k5eAaImF	detoxikovat
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
anorganických	anorganický	k2eAgFnPc2d1	anorganická
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
kyslíkového	kyslíkový	k2eAgInSc2d1	kyslíkový
radikálu	radikál	k1gInSc2	radikál
a	a	k8xC	a
iontů	ion	k1gInPc2	ion
je	být	k5eAaImIp3nS	být
emitováno	emitovat	k5eAaBmNgNnS	emitovat
také	také	k9	také
ultrafialové	ultrafialový	k2eAgFnSc2d1	ultrafialová
(	(	kIx(	(
<g/>
UV	UV	kA	UV
<g/>
)	)	kIx)	)
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
destruktivní	destruktivní	k2eAgNnSc1d1	destruktivní
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
biologických	biologický	k2eAgFnPc2d1	biologická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc4	možnost
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
biologických	biologický	k2eAgFnPc2d1	biologická
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
toxinů	toxin	k1gInPc2	toxin
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
může	moct	k5eAaImIp3nS	moct
iniciovat	iniciovat	k5eAaBmF	iniciovat
nebo	nebo	k8xC	nebo
urychlovat	urychlovat	k5eAaImF	urychlovat
průběh	průběh	k1gInSc4	průběh
dalších	další	k2eAgFnPc2d1	další
detoxikačních	detoxikační	k2eAgFnPc2d1	detoxikační
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Nízkotlaké	nízkotlaký	k2eAgFnSc3d1	nízkotlaká
(	(	kIx(	(
<g/>
studené	studený	k2eAgFnSc3d1	studená
<g/>
)	)	kIx)	)
plazma	plazma	k1gNnSc1	plazma
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
využít	využít	k5eAaPmF	využít
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
ke	k	k7c3	k
sterilizaci	sterilizace	k1gFnSc3	sterilizace
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
vysokoteplotního	vysokoteplotní	k2eAgNnSc2d1	vysokoteplotní
plazmatu	plazma	k1gNnSc2	plazma
je	být	k5eAaImIp3nS	být
předpokládáno	předpokládán	k2eAgNnSc1d1	předpokládáno
pro	pro	k7c4	pro
rozklad	rozklad	k1gInSc4	rozklad
oblaků	oblak	k1gInPc2	oblak
aerosolů	aerosol	k1gInPc2	aerosol
nebo	nebo	k8xC	nebo
par	para	k1gFnPc2	para
toxické	toxický	k2eAgFnSc2d1	toxická
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
úniku	únik	k1gInSc2	únik
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
po	po	k7c6	po
úniku	únik	k1gInSc6	únik
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Přímá	přímý	k2eAgFnSc1d1	přímá
absorpce	absorpce	k1gFnSc1	absorpce
laserového	laserový	k2eAgNnSc2d1	laserové
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc1d1	rychlý
ohřev	ohřev	k1gInSc1	ohřev
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
tvorba	tvorba	k1gFnSc1	tvorba
samotného	samotný	k2eAgNnSc2d1	samotné
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
složky	složka	k1gFnPc1	složka
by	by	kYmCp3nP	by
degradovaly	degradovat	k5eAaBmAgFnP	degradovat
daný	daný	k2eAgInSc4d1	daný
kontaminant	kontaminant	k1gInSc4	kontaminant
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
výhody	výhoda	k1gFnPc4	výhoda
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
vlastnost	vlastnost	k1gFnSc1	vlastnost
plazmatu	plazma	k1gNnSc2	plazma
snadno	snadno	k6eAd1	snadno
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
povrchových	povrchový	k2eAgInPc2d1	povrchový
pórů	pór	k1gInPc2	pór
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
kontaminant	kontaminant	k1gInSc1	kontaminant
je	být	k5eAaImIp3nS	být
rozložen	rozložit	k5eAaPmNgInS	rozložit
s	s	k7c7	s
minimálními	minimální	k2eAgInPc7d1	minimální
vedlejšími	vedlejší	k2eAgInPc7d1	vedlejší
nebo	nebo	k8xC	nebo
plynnými	plynný	k2eAgInPc7d1	plynný
(	(	kIx(	(
<g/>
snadný	snadný	k2eAgInSc1d1	snadný
únik	únik	k1gInSc1	únik
<g/>
)	)	kIx)	)
produkty	produkt	k1gInPc1	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
riziko	riziko	k1gNnSc1	riziko
poškození	poškození	k1gNnSc2	poškození
materiálu	materiál	k1gInSc2	materiál
teplem	teplo	k1gNnSc7	teplo
nebo	nebo	k8xC	nebo
reaktivními	reaktivní	k2eAgFnPc7d1	reaktivní
složkami	složka	k1gFnPc7	složka
plazmatu	plazma	k1gNnSc2	plazma
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dekontaminaci	dekontaminace	k1gFnSc3	dekontaminace
jsou	být	k5eAaImIp3nP	být
využitelné	využitelný	k2eAgInPc1d1	využitelný
především	především	k9	především
tři	tři	k4xCgInPc4	tři
biologické	biologický	k2eAgInPc4d1	biologický
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Biodegradace	Biodegradace	k1gFnSc1	Biodegradace
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
houbami	houba	k1gFnPc7	houba
a	a	k8xC	a
zelenými	zelený	k2eAgFnPc7d1	zelená
rostlinami	rostlina	k1gFnPc7	rostlina
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biodegradace	Biodegradace	k1gFnSc1	Biodegradace
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bioventing	bioventing	k1gInSc1	bioventing
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
dekontaminaci	dekontaminace	k1gFnSc3	dekontaminace
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
(	(	kIx(	(
<g/>
ve	v	k7c6	v
stlačené	stlačený	k2eAgFnSc6d1	stlačená
formě	forma	k1gFnSc6	forma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
vpravován	vpravovat	k5eAaImNgMnS	vpravovat
injektáží	injektáž	k1gFnSc7	injektáž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
degraduje	degradovat	k5eAaBmIp3nS	degradovat
například	například	k6eAd1	například
ropné	ropný	k2eAgInPc1d1	ropný
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
pesticidy	pesticid	k1gInPc1	pesticid
a	a	k8xC	a
fenoly	fenol	k1gInPc1	fenol
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
biodegradaci	biodegradace	k1gFnSc3	biodegradace
houbami	houba	k1gFnPc7	houba
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
některé	některý	k3yIgFnPc1	některý
dřevokazné	dřevokazný	k2eAgFnPc1d1	dřevokazná
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
degradovat	degradovat	k5eAaBmF	degradovat
organické	organický	k2eAgInPc4d1	organický
kontaminanty	kontaminant	k1gInPc4	kontaminant
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
využitelná	využitelný	k2eAgFnSc1d1	využitelná
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
velmi	velmi	k6eAd1	velmi
stabilních	stabilní	k2eAgFnPc2d1	stabilní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
polychlorovaných	polychlorovaný	k2eAgInPc2d1	polychlorovaný
bifenylů	bifenyl	k1gInPc2	bifenyl
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biodegradace	Biodegradace	k1gFnSc1	Biodegradace
zelenými	zelený	k2eAgFnPc7d1	zelená
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
fytodegradace	fytodegradace	k1gFnPc1	fytodegradace
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
degradaci	degradace	k1gFnSc3	degradace
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
enzymy	enzym	k1gInPc1	enzym
těchto	tento	k3xDgFnPc2	tento
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
především	především	k9	především
k	k	k7c3	k
čištění	čištění	k1gNnSc3	čištění
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
půd	půda	k1gFnPc2	půda
a	a	k8xC	a
vod	voda	k1gFnPc2	voda
například	například	k6eAd1	například
od	od	k7c2	od
chlorovaných	chlorovaný	k2eAgNnPc2d1	chlorované
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
nebo	nebo	k8xC	nebo
herbicidů	herbicid	k1gInPc2	herbicid
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
co	co	k8xS	co
nejvyšší	vysoký	k2eAgFnSc3d3	nejvyšší
účinnosti	účinnost	k1gFnSc3	účinnost
<g/>
,	,	kIx,	,
zrychlení	zrychlení	k1gNnSc3	zrychlení
a	a	k8xC	a
správnému	správný	k2eAgNnSc3d1	správné
provedení	provedení	k1gNnSc3	provedení
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
dodržet	dodržet	k5eAaPmF	dodržet
následující	následující	k2eAgFnPc1d1	následující
obecné	obecný	k2eAgFnPc1d1	obecná
zásady	zásada	k1gFnPc1	zásada
<g/>
:	:	kIx,	:
zjištění	zjištění	k1gNnPc1	zjištění
druhu	druh	k1gInSc2	druh
kontaminantu	kontaminant	k1gInSc2	kontaminant
a	a	k8xC	a
rozsahu	rozsah	k1gInSc2	rozsah
kontaminace	kontaminace	k1gFnSc2	kontaminace
<g/>
,	,	kIx,	,
stanovení	stanovení	k1gNnSc1	stanovení
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
a	a	k8xC	a
dekontaminačního	dekontaminační	k2eAgInSc2d1	dekontaminační
postupu	postup	k1gInSc2	postup
<g/>
,	,	kIx,	,
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
celkovou	celkový	k2eAgFnSc4d1	celková
dobu	doba	k1gFnSc4	doba
používání	používání	k1gNnSc2	používání
dýchací	dýchací	k2eAgFnSc2d1	dýchací
techniky	technika	k1gFnSc2	technika
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zásobu	zásoba	k1gFnSc4	zásoba
<g />
.	.	kIx.	.
</s>
<s>
vzduchu	vzduch	k1gInSc3	vzduch
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc3	omezení
organizace	organizace	k1gFnSc2	organizace
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včasné	včasný	k2eAgNnSc4d1	včasné
zahájení	zahájení	k1gNnSc4	zahájení
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
obzvláště	obzvláště	k6eAd1	obzvláště
u	u	k7c2	u
kapalných	kapalný	k2eAgInPc2d1	kapalný
kontaminantů	kontaminant	k1gInPc2	kontaminant
(	(	kIx(	(
<g/>
zahájit	zahájit	k5eAaPmF	zahájit
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
s	s	k7c7	s
méně	málo	k6eAd2	málo
účinnými	účinný	k2eAgInPc7d1	účinný
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
nečekat	čekat	k5eNaImF	čekat
na	na	k7c4	na
opožděné	opožděný	k2eAgNnSc4d1	opožděné
dodání	dodání	k1gNnSc4	dodání
speciálních	speciální	k2eAgInPc2d1	speciální
prostředků	prostředek	k1gInPc2	prostředek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesné	přesný	k2eAgNnSc4d1	přesné
stanovení	stanovení	k1gNnSc4	stanovení
a	a	k8xC	a
vymezení	vymezení	k1gNnSc4	vymezení
úkolů	úkol	k1gInPc2	úkol
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
členů	člen	k1gMnPc2	člen
při	při	k7c6	při
zřizování	zřizování	k1gNnSc6	zřizování
dekontaminačního	dekontaminační	k2eAgNnSc2d1	dekontaminační
pracoviště	pracoviště	k1gNnSc2	pracoviště
a	a	k8xC	a
při	při	k7c6	při
samotném	samotný	k2eAgMnSc6d1	samotný
<g />
.	.	kIx.	.
</s>
<s>
procesu	proces	k1gInSc2	proces
dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
<g/>
,	,	kIx,	,
zajištění	zajištění	k1gNnSc1	zajištění
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
osobní	osobní	k2eAgFnSc2d1	osobní
ochrany	ochrana	k1gFnSc2	ochrana
dekontaminačního	dekontaminační	k2eAgInSc2d1	dekontaminační
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
postupovat	postupovat	k5eAaImF	postupovat
tak	tak	k6eAd1	tak
(	(	kIx(	(
<g/>
směrem	směr	k1gInSc7	směr
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
zevnitř	zevnitř	k6eAd1	zevnitř
ven	ven	k6eAd1	ven
<g/>
)	)	kIx)	)
aby	aby	kYmCp3nS	aby
kontaminant	kontaminant	k1gInSc1	kontaminant
ani	ani	k8xC	ani
dekontaminační	dekontaminační	k2eAgInPc1d1	dekontaminační
produkty	produkt	k1gInPc1	produkt
nezatékaly	zatékat	k5eNaImAgInP	zatékat
na	na	k7c4	na
jíž	jenž	k3xRgFnSc3	jenž
očištěné	očištěný	k2eAgInPc4d1	očištěný
nebo	nebo	k8xC	nebo
čisté	čistý	k2eAgInPc4d1	čistý
povrchy	povrch	k1gInPc4	povrch
<g/>
,	,	kIx,	,
posoudit	posoudit	k5eAaPmF	posoudit
nebezpečnost	nebezpečnost	k1gFnSc4	nebezpečnost
dekontaminačních	dekontaminační	k2eAgInPc2d1	dekontaminační
odpadů	odpad	k1gInPc2	odpad
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
záchyt	záchyt	k1gInSc4	záchyt
těchto	tento	k3xDgInPc2	tento
odpadů	odpad	k1gInPc2	odpad
<g/>
,	,	kIx,	,
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
provést	provést	k5eAaPmF	provést
kontrolu	kontrola	k1gFnSc4	kontrola
její	její	k3xOp3gFnSc2	její
účinnosti	účinnost	k1gFnSc2	účinnost
detekčními	detekční	k2eAgInPc7d1	detekční
přístroji	přístroj	k1gInPc7	přístroj
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
zásahu	zásah	k1gInSc2	zásah
na	na	k7c4	na
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
látky	látka	k1gFnPc4	látka
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
zásahu	zásah	k1gInSc2	zásah
a	a	k8xC	a
prováděných	prováděný	k2eAgFnPc6d1	prováděná
činnostech	činnost	k1gFnPc6	činnost
dodržovat	dodržovat	k5eAaImF	dodržovat
určitá	určitý	k2eAgNnPc4d1	určité
specifika	specifikon	k1gNnPc4	specifikon
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
těchto	tento	k3xDgFnPc2	tento
specifik	specifika	k1gFnPc2	specifika
je	být	k5eAaImIp3nS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
kontrolovaných	kontrolovaný	k2eAgFnPc2d1	kontrolovaná
zón	zóna	k1gFnPc2	zóna
a	a	k8xC	a
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
následné	následný	k2eAgNnSc4d1	následné
přesné	přesný	k2eAgNnSc4d1	přesné
dodržování	dodržování	k1gNnSc4	dodržování
stanovených	stanovený	k2eAgFnPc2d1	stanovená
zásad	zásada	k1gFnPc2	zásada
a	a	k8xC	a
postupů	postup	k1gInPc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Zóny	zóna	k1gFnPc1	zóna
se	se	k3xPyFc4	se
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
podle	podle	k7c2	podle
hrozícího	hrozící	k2eAgNnSc2d1	hrozící
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
a	a	k8xC	a
činností	činnost	k1gFnPc2	činnost
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
prováděných	prováděný	k2eAgInPc6d1	prováděný
na	na	k7c4	na
<g/>
:	:	kIx,	:
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
ohrožením	ohrožení	k1gNnSc7	ohrožení
(	(	kIx(	(
<g/>
a	a	k8xC	a
nejpravděpodobnější	pravděpodobný	k2eAgFnSc7d3	nejpravděpodobnější
kontaminací	kontaminace	k1gFnSc7	kontaminace
<g/>
)	)	kIx)	)
nasazených	nasazený	k2eAgFnPc2d1	nasazená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
rozsah	rozsah	k1gInSc1	rozsah
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
k	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
nepříznivých	příznivý	k2eNgInPc2d1	nepříznivý
účinků	účinek	k1gInPc2	účinek
na	na	k7c4	na
zasahující	zasahující	k2eAgFnPc4d1	zasahující
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
činnosti	činnost	k1gFnPc1	činnost
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
rizik	riziko	k1gNnPc2	riziko
a	a	k8xC	a
zneškodnění	zneškodnění	k1gNnSc2	zneškodnění
zdroje	zdroj	k1gInSc2	zdroj
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
zóny	zóna	k1gFnSc2	zóna
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
především	především	k9	především
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
uniklé	uniklý	k2eAgFnSc2d1	uniklá
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnPc1	možnost
dalšího	další	k2eAgNnSc2d1	další
šíření	šíření	k1gNnSc2	šíření
<g/>
,	,	kIx,	,
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
technologií	technologie	k1gFnPc2	technologie
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
meteorologických	meteorologický	k2eAgFnPc2d1	meteorologická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
je	být	k5eAaImIp3nS	být
dovolen	dovolit	k5eAaPmNgInS	dovolit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
stanovených	stanovený	k2eAgInPc6d1	stanovený
prostředcích	prostředek	k1gInPc6	prostředek
individuální	individuální	k2eAgFnSc2d1	individuální
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
přetlakové	přetlakový	k2eAgNnSc1d1	přetlakové
nebo	nebo	k8xC	nebo
filtrační	filtrační	k2eAgNnSc1d1	filtrační
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
určenou	určený	k2eAgFnSc4d1	určená
velitelem	velitel	k1gMnSc7	velitel
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
a	a	k8xC	a
výstup	výstup	k1gInSc1	výstup
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
zóny	zóna	k1gFnSc2	zóna
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
stanovených	stanovený	k2eAgInPc6d1	stanovený
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
vnější	vnější	k2eAgFnSc4d1	vnější
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
zónu	zóna	k1gFnSc4	zóna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
zasahující	zasahující	k2eAgFnPc1d1	zasahující
síly	síla	k1gFnPc1	síla
a	a	k8xC	a
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nástupní	nástupní	k2eAgInSc4d1	nástupní
a	a	k8xC	a
dekontaminační	dekontaminační	k2eAgInSc4d1	dekontaminační
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
zde	zde	k6eAd1	zde
být	být	k5eAaImF	být
prováděna	provádět	k5eAaImNgFnS	provádět
dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
evakuovaných	evakuovaný	k2eAgFnPc2d1	evakuovaná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
zónu	zóna	k1gFnSc4	zóna
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
prostor	prostor	k1gInSc1	prostor
případného	případný	k2eAgNnSc2d1	případné
rizika	riziko	k1gNnSc2	riziko
šíření	šíření	k1gNnSc2	šíření
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
základě	základ	k1gInSc6	základ
meteorologických	meteorologický	k2eAgFnPc2d1	meteorologická
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
34	[number]	k4	34
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
zóny	zóna	k1gFnSc2	zóna
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
vytyčeny	vytyčit	k5eAaPmNgInP	vytyčit
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
obecných	obecný	k2eAgFnPc2d1	obecná
znalostí	znalost	k1gFnPc2	znalost
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
také	také	k9	také
snadné	snadný	k2eAgNnSc1d1	snadné
rozpoznání	rozpoznání	k1gNnSc1	rozpoznání
hranic	hranice	k1gFnPc2	hranice
zón	zóna	k1gFnPc2	zóna
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
přísné	přísný	k2eAgNnSc1d1	přísné
dodržování	dodržování	k1gNnSc1	dodržování
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
34	[number]	k4	34
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dekontaminačním	dekontaminační	k2eAgInSc6d1	dekontaminační
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
vytyčeno	vytyčen	k2eAgNnSc1d1	vytyčeno
stanoviště	stanoviště	k1gNnSc1	stanoviště
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
zasahujících	zasahující	k2eAgFnPc2d1	zasahující
záchranných	záchranný	k2eAgFnPc2d1	záchranná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
stanoviště	stanoviště	k1gNnSc1	stanoviště
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zprovoznit	zprovoznit	k5eAaPmF	zprovoznit
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
zahájení	zahájení	k1gNnSc2	zahájení
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
nebezpečné	bezpečný	k2eNgFnSc6d1	nebezpečná
zóně	zóna	k1gFnSc6	zóna
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
umístěno	umístit	k5eAaPmNgNnS	umístit
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
a	a	k8xC	a
vnější	vnější	k2eAgFnSc2d1	vnější
zóny	zóna	k1gFnSc2	zóna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
možným	možný	k2eAgInSc7d1	možný
výstupním	výstupní	k2eAgInSc7d1	výstupní
bodem	bod	k1gInSc7	bod
z	z	k7c2	z
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Stanoviště	stanoviště	k1gNnSc1	stanoviště
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
zajištěnou	zajištěný	k2eAgFnSc4d1	zajištěná
obsluhu	obsluha	k1gFnSc4	obsluha
vybavenou	vybavený	k2eAgFnSc7d1	vybavená
stanovenými	stanovený	k2eAgInPc7d1	stanovený
prostředky	prostředek	k1gInPc7	prostředek
individuální	individuální	k2eAgFnSc2d1	individuální
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
zásahu	zásah	k1gInSc2	zásah
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
:	:	kIx,	:
stanoví	stanovit	k5eAaPmIp3nS	stanovit
plochu	plocha	k1gFnSc4	plocha
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
postup	postup	k1gInSc4	postup
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
,	,	kIx,	,
provede	provést	k5eAaPmIp3nS	provést
volbu	volba	k1gFnSc4	volba
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
prověří	prověřit	k5eAaPmIp3nS	prověřit
dostupná	dostupný	k2eAgFnSc1d1	dostupná
množství	množství	k1gNnSc4	množství
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
aplikaci	aplikace	k1gFnSc4	aplikace
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nezbytné	zbytný	k2eNgFnSc2d1	zbytný
doby	doba	k1gFnSc2	doba
působení	působení	k1gNnSc2	působení
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
provede	provést	k5eAaPmIp3nS	provést
odhad	odhad	k1gInSc1	odhad
doby	doba	k1gFnSc2	doba
trvání	trvání	k1gNnSc2	trvání
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
způsob	způsob	k1gInSc1	způsob
uložení	uložení	k1gNnSc2	uložení
odpadů	odpad	k1gInPc2	odpad
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
při	při	k7c6	při
dekontaminaci	dekontaminace	k1gFnSc6	dekontaminace
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
součásti	součást	k1gFnPc1	součást
dekontaminačního	dekontaminační	k2eAgNnSc2d1	dekontaminační
stanoviště	stanoviště	k1gNnSc2	stanoviště
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
základní	základní	k2eAgInSc4d1	základní
monitoring	monitoring	k1gInSc4	monitoring
zasahujících	zasahující	k2eAgFnPc2d1	zasahující
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
opouštějí	opouštět	k5eAaImIp3nP	opouštět
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
zónu	zóna	k1gFnSc4	zóna
–	–	k?	–
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
a	a	k8xC	a
roztřiďovací	roztřiďovací	k2eAgNnPc1d1	roztřiďovací
stanoviště	stanoviště	k1gNnPc1	stanoviště
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
odkládání	odkládání	k1gNnSc4	odkládání
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
věcných	věcný	k2eAgInPc2d1	věcný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
nanášení	nanášení	k1gNnSc4	nanášení
dekontaminačních	dekontaminační	k2eAgInPc2d1	dekontaminační
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
následné	následný	k2eAgNnSc4d1	následné
smytí	smytí	k1gNnSc4	smytí
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
odkládání	odkládání	k1gNnSc4	odkládání
individuálních	individuální	k2eAgInPc2d1	individuální
ochranných	ochranný	k2eAgInPc2d1	ochranný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
opětovné	opětovný	k2eAgNnSc4d1	opětovné
vystrojení	vystrojení	k1gNnSc4	vystrojení
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
provedení	provedení	k1gNnSc4	provedení
kontrolní	kontrolní	k2eAgFnSc2d1	kontrolní
detekce	detekce	k1gFnSc2	detekce
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
odkládání	odkládání	k1gNnSc4	odkládání
věcných	věcný	k2eAgInPc2d1	věcný
prostředků	prostředek	k1gInPc2	prostředek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
výstupní	výstupní	k2eAgFnSc6d1	výstupní
trase	trasa	k1gFnSc6	trasa
z	z	k7c2	z
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Odkládají	odkládat	k5eAaImIp3nP	odkládat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
věcné	věcný	k2eAgInPc4d1	věcný
prostředky	prostředek	k1gInPc4	prostředek
(	(	kIx(	(
<g/>
měřící	měřící	k2eAgInPc4d1	měřící
přístroje	přístroj	k1gInPc4	přístroj
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
použité	použitý	k2eAgFnSc2d1	použitá
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zásahu	zásah	k1gInSc2	zásah
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
využity	využít	k5eAaPmNgInP	využít
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
zásahu	zásah	k1gInSc2	zásah
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
prostředky	prostředek	k1gInPc1	prostředek
dekontaminovány	dekontaminován	k2eAgInPc1d1	dekontaminován
(	(	kIx(	(
<g/>
např.	např.	kA	např.
složité	složitý	k2eAgInPc1d1	složitý
přístroje	přístroj	k1gInPc1	přístroj
je	on	k3xPp3gMnPc4	on
možno	možno	k6eAd1	možno
zabalit	zabalit	k5eAaPmF	zabalit
do	do	k7c2	do
neprodyšných	prodyšný	k2eNgInPc2d1	neprodyšný
obalů	obal	k1gInPc2	obal
a	a	k8xC	a
převézt	převézt	k5eAaPmF	převézt
na	na	k7c4	na
speciální	speciální	k2eAgNnPc4d1	speciální
dekontaminační	dekontaminační	k2eAgNnPc4d1	dekontaminační
pracoviště	pracoviště	k1gNnPc4	pracoviště
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
nanášení	nanášení	k1gNnSc4	nanášení
dekontaminačních	dekontaminační	k2eAgInPc2d1	dekontaminační
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
následné	následný	k2eAgNnSc4d1	následné
smytí	smytí	k1gNnSc4	smytí
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tvořen	tvořit	k5eAaImNgInS	tvořit
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgFnPc7	dva
záchytnými	záchytný	k2eAgFnPc7d1	záchytná
vanami	vana	k1gFnPc7	vana
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
hrubá	hrubý	k2eAgFnSc1d1	hrubá
očista	očista	k1gFnSc1	očista
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vlastní	vlastní	k2eAgFnSc3d1	vlastní
dekontaminaci	dekontaminace	k1gFnSc3	dekontaminace
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
přebytkem	přebytek	k1gInSc7	přebytek
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
aplikací	aplikace	k1gFnPc2	aplikace
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
působením	působení	k1gNnSc7	působení
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
oplachem	oplach	k1gInSc7	oplach
<g/>
.	.	kIx.	.
</s>
<s>
Dekontaminační	dekontaminační	k2eAgFnPc1d1	dekontaminační
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
působení	působení	k1gNnSc2	působení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
nanášet	nanášet	k5eAaImF	nanášet
manuálně	manuálně	k6eAd1	manuálně
(	(	kIx(	(
<g/>
postřikovač	postřikovač	k1gInSc1	postřikovač
nebo	nebo	k8xC	nebo
improvizovaně	improvizovaně	k6eAd1	improvizovaně
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	se	k3xPyFc3	se
překrývajícími	překrývající	k2eAgInPc7d1	překrývající
se	se	k3xPyFc4	se
tahy	tah	k1gInPc7	tah
košťátka	košťátko	k1gNnSc2	košťátko
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
smytím	smytí	k1gNnSc7	smytí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záchytná	záchytný	k2eAgFnSc1d1	záchytná
vana	vana	k1gFnSc1	vana
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
jako	jako	k9	jako
celek	celek	k1gInSc4	celek
nebo	nebo	k8xC	nebo
překryta	překryt	k2eAgNnPc4d1	překryto
samostatně	samostatně	k6eAd1	samostatně
upevněnou	upevněný	k2eAgFnSc7d1	upevněná
plachtou	plachta	k1gFnSc7	plachta
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
velká	velký	k2eAgFnSc1d1	velká
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
m	m	kA	m
<g/>
;	;	kIx,	;
záchyt	záchyt	k1gInSc1	záchyt
odražených	odražený	k2eAgFnPc2d1	odražená
kapek	kapka	k1gFnPc2	kapka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahovat	obsahovat	k5eAaImF	obsahovat
vypouštěcí	vypouštěcí	k2eAgInSc4d1	vypouštěcí
otvor	otvor	k1gInSc4	otvor
a	a	k8xC	a
pochozí	pochozí	k2eAgInPc4d1	pochozí
protiskluzové	protiskluzový	k2eAgInPc4d1	protiskluzový
rošty	rošt	k1gInPc4	rošt
alespoň	alespoň	k9	alespoň
10	[number]	k4	10
centimetrů	centimetr	k1gInPc2	centimetr
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
sama	sám	k3xTgMnSc4	sám
snadno	snadno	k6eAd1	snadno
dekontaminovatelná	dekontaminovatelný	k2eAgFnSc1d1	dekontaminovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Dekontaminační	dekontaminační	k2eAgFnPc1d1	dekontaminační
sprchy	sprcha	k1gFnPc1	sprcha
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
využity	využít	k5eAaPmNgInP	využít
mimo	mimo	k7c4	mimo
oplachu	oplach	k1gInSc6	oplach
vodou	voda	k1gFnSc7	voda
i	i	k9	i
k	k	k7c3	k
nánosu	nános	k1gInSc2	nános
rychle	rychle	k6eAd1	rychle
působících	působící	k2eAgFnPc2d1	působící
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ke	k	k7c3	k
kombinaci	kombinace	k1gFnSc3	kombinace
těchto	tento	k3xDgFnPc2	tento
činností	činnost	k1gFnPc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
dekontaminovaných	dekontaminovaný	k2eAgMnPc2d1	dekontaminovaný
hasičů	hasič	k1gMnPc2	hasič
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
čerpadlo	čerpadlo	k1gNnSc1	čerpadlo
a	a	k8xC	a
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c4	na
odpadní	odpadní	k2eAgFnSc4d1	odpadní
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dekontaminaci	dekontaminace	k1gFnSc6	dekontaminace
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
brát	brát	k5eAaImF	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
nejvíce	hodně	k6eAd3	hodně
kontaminované	kontaminovaný	k2eAgFnPc4d1	kontaminovaná
nebo	nebo	k8xC	nebo
obtížně	obtížně	k6eAd1	obtížně
dekontaminovatelné	dekontaminovatelný	k2eAgFnSc6d1	dekontaminovatelný
části	část	k1gFnSc6	část
ochranného	ochranný	k2eAgInSc2d1	ochranný
oděvu	oděv	k1gInSc2	oděv
mezi	mezi	k7c4	mezi
které	který	k3yRgNnSc4	který
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
záhyby	záhyba	k1gFnPc4	záhyba
protichemického	protichemický	k2eAgInSc2d1	protichemický
obleku	oblek	k1gInSc2	oblek
<g/>
,	,	kIx,	,
podrážky	podrážka	k1gFnSc2	podrážka
bot	bota	k1gFnPc2	bota
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc1	místo
pod	pod	k7c7	pod
pažemi	paže	k1gFnPc7	paže
a	a	k8xC	a
rozkrok	rozkrok	k1gInSc1	rozkrok
<g/>
,	,	kIx,	,
zádová	zádový	k2eAgFnSc1d1	zádová
část	část	k1gFnSc1	část
pod	pod	k7c7	pod
dýchacím	dýchací	k2eAgInSc7d1	dýchací
přístrojem	přístroj	k1gInSc7	přístroj
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc1	prostor
přetlakových	přetlakový	k2eAgInPc2d1	přetlakový
ventilů	ventil	k1gInPc2	ventil
u	u	k7c2	u
přetlakových	přetlakový	k2eAgInPc2d1	přetlakový
obleků	oblek	k1gInPc2	oblek
<g/>
,	,	kIx,	,
zorník	zorník	k1gInSc1	zorník
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
14,17	[number]	k4	14,17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
u	u	k7c2	u
přetlakových	přetlakový	k2eAgInPc2d1	přetlakový
obleků	oblek	k1gInPc2	oblek
nejsou	být	k5eNaImIp3nP	být
boty	bota	k1gFnPc1	bota
jeho	jeho	k3xOp3gFnSc4	jeho
neoddělitelnou	oddělitelný	k2eNgFnSc4d1	neoddělitelná
součástí	součást	k1gFnSc7	součást
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
hasič	hasič	k1gMnSc1	hasič
vyzout	vyzout	k5eAaPmF	vyzout
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
budou	být	k5eAaImBp3nP	být
dekontaminovány	dekontaminovat	k5eAaBmNgFnP	dekontaminovat
samostatně	samostatně	k6eAd1	samostatně
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
záchranné	záchranný	k2eAgFnPc1d1	záchranná
jednotky	jednotka	k1gFnPc1	jednotka
dekontaminují	dekontaminovat	k5eAaBmIp3nP	dekontaminovat
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
si	se	k3xPyFc3	se
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
provedení	provedení	k1gNnSc6	provedení
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
záchranné	záchranný	k2eAgFnPc4d1	záchranná
jednotky	jednotka	k1gFnPc4	jednotka
dostatek	dostatek	k1gInSc4	dostatek
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
tlakové	tlakový	k2eAgFnSc6d1	tlaková
lahvi	lahev	k1gFnSc6	lahev
dýchacího	dýchací	k2eAgInSc2d1	dýchací
přístroje	přístroj	k1gInSc2	přístroj
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
alespoň	alespoň	k9	alespoň
na	na	k7c4	na
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
pro	pro	k7c4	pro
kontrolní	kontrolní	k2eAgFnSc4d1	kontrolní
detekci	detekce	k1gFnSc4	detekce
(	(	kIx(	(
<g/>
umístěn	umístit	k5eAaPmNgInS	umístit
ihned	ihned	k6eAd1	ihned
za	za	k7c7	za
dekontaminačními	dekontaminační	k2eAgFnPc7d1	dekontaminační
sprchami	sprcha	k1gFnPc7	sprcha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kontrolována	kontrolován	k2eAgFnSc1d1	kontrolována
účinnost	účinnost	k1gFnSc1	účinnost
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
kontaminant	kontaminant	k1gInSc1	kontaminant
nejobtížněji	obtížně	k6eAd3	obtížně
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
výsledku	výsledek	k1gInSc2	výsledek
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
opakovat	opakovat	k5eAaImF	opakovat
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svlékání	svlékání	k1gNnSc6	svlékání
se	se	k3xPyFc4	se
záchranné	záchranný	k2eAgFnPc1d1	záchranná
jednotky	jednotka	k1gFnPc1	jednotka
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
pouze	pouze	k6eAd1	pouze
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
strany	strana	k1gFnPc1	strana
obleku	oblek	k1gInSc2	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Ochranný	ochranný	k2eAgInSc1d1	ochranný
oděv	oděv	k1gInSc1	oděv
včetně	včetně	k7c2	včetně
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
rukavic	rukavice	k1gFnPc2	rukavice
se	se	k3xPyFc4	se
odkládá	odkládat	k5eAaImIp3nS	odkládat
do	do	k7c2	do
neprodyšného	prodyšný	k2eNgInSc2d1	neprodyšný
obalu	obal	k1gInSc2	obal
pro	pro	k7c4	pro
provedení	provedení	k1gNnSc4	provedení
případné	případný	k2eAgFnSc2d1	případná
následné	následný	k2eAgFnSc2d1	následná
oboustranné	oboustranný	k2eAgFnSc2d1	oboustranná
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
odkládá	odkládat	k5eAaImIp3nS	odkládat
dýchací	dýchací	k2eAgInSc1d1	dýchací
přístroj	přístroj	k1gInSc1	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Obsluha	obsluha	k1gFnSc1	obsluha
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
potřebnými	potřebný	k2eAgInPc7d1	potřebný
prostředky	prostředek	k1gInPc7	prostředek
individuální	individuální	k2eAgFnSc2d1	individuální
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jednorázový	jednorázový	k2eAgInSc1d1	jednorázový
ochranný	ochranný	k2eAgInSc1d1	ochranný
oblek	oblek	k1gInSc1	oblek
<g/>
,	,	kIx,	,
ochranné	ochranný	k2eAgFnSc2d1	ochranná
rukavice	rukavice	k1gFnSc2	rukavice
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
činnosti	činnost	k1gFnSc2	činnost
pracoviště	pracoviště	k1gNnSc4	pracoviště
si	se	k3xPyFc3	se
obsluha	obsluha	k1gFnSc1	obsluha
dekontaminuje	dekontaminovat	k5eAaBmIp3nS	dekontaminovat
rukavice	rukavice	k1gFnPc4	rukavice
<g/>
,	,	kIx,	,
při	při	k7c6	při
svlékání	svlékání	k1gNnSc6	svlékání
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
pouze	pouze	k6eAd1	pouze
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
obleku	oblek	k1gInSc2	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
ochranné	ochranný	k2eAgInPc1d1	ochranný
prostředky	prostředek	k1gInPc1	prostředek
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
uloženy	uložit	k5eAaPmNgFnP	uložit
do	do	k7c2	do
neprodyšných	prodyšný	k2eNgInPc2d1	neprodyšný
obalů	obal	k1gInPc2	obal
jako	jako	k8xS	jako
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
odpad	odpad	k1gInSc1	odpad
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
navazuje	navazovat	k5eAaImIp3nS	navazovat
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
opětovné	opětovný	k2eAgNnSc4d1	opětovné
vystrojení	vystrojení	k1gNnSc4	vystrojení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
místo	místo	k1gNnSc4	místo
vstupu	vstup	k1gInSc2	vstup
a	a	k8xC	a
výstupu	výstup	k1gInSc2	výstup
do	do	k7c2	do
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
zóny	zóna	k1gFnSc2	zóna
totožné	totožný	k2eAgFnPc1d1	totožná
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
přeprava	přeprava	k1gFnSc1	přeprava
výstroje	výstroj	k1gFnSc2	výstroj
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
likvidace	likvidace	k1gFnSc1	likvidace
dekontaminačního	dekontaminační	k2eAgNnSc2d1	dekontaminační
stanoviště	stanoviště	k1gNnSc2	stanoviště
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
určité	určitý	k2eAgFnPc4d1	určitá
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
činnosti	činnost	k1gFnSc6	činnost
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
likvidační	likvidační	k2eAgInSc1d1	likvidační
tým	tým	k1gInSc1	tým
vybaven	vybavit	k5eAaPmNgInS	vybavit
stejnými	stejný	k2eAgInPc7d1	stejný
ochrannými	ochranný	k2eAgInPc7d1	ochranný
prostředky	prostředek	k1gInPc7	prostředek
jako	jako	k8xC	jako
zasahující	zasahující	k2eAgFnPc1d1	zasahující
záchranné	záchranný	k2eAgFnPc1d1	záchranná
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
nebezpečné	bezpečný	k2eNgFnSc6d1	nebezpečná
zóně	zóna	k1gFnSc6	zóna
a	a	k8xC	a
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
zpravidla	zpravidla	k6eAd1	zpravidla
provádí	provádět	k5eAaImIp3nS	provádět
poslední	poslední	k2eAgFnSc1d1	poslední
dvojice	dvojice	k1gFnSc1	dvojice
zasahujících	zasahující	k2eAgFnPc2d1	zasahující
záchranných	záchranný	k2eAgFnPc2d1	záchranná
jednotek	jednotka	k1gFnPc2	jednotka
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
jejich	jejich	k3xOp3gFnSc2	jejich
vlastní	vlastní	k2eAgFnSc2d1	vlastní
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Věcné	věcný	k2eAgInPc1d1	věcný
prostředky	prostředek	k1gInPc1	prostředek
jsou	být	k5eAaImIp3nP	být
dekontaminovány	dekontaminovat	k5eAaBmNgInP	dekontaminovat
nejdříve	dříve	k6eAd3	dříve
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
a	a	k8xC	a
pak	pak	k6eAd1	pak
z	z	k7c2	z
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
některé	některý	k3yIgInPc4	některý
prostředky	prostředek	k1gInPc4	prostředek
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
dekontaminovat	dekontaminovat	k5eAaBmF	dekontaminovat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
tyto	tento	k3xDgFnPc4	tento
zabaleny	zabalen	k2eAgFnPc4d1	zabalena
do	do	k7c2	do
neprodyšných	prodyšný	k2eNgInPc2d1	neprodyšný
a	a	k8xC	a
nerozbitných	rozbitný	k2eNgInPc2d1	nerozbitný
obalů	obal	k1gInPc2	obal
a	a	k8xC	a
převezeny	převézt	k5eAaPmNgFnP	převézt
na	na	k7c4	na
specializované	specializovaný	k2eAgNnSc4d1	specializované
dekontaminační	dekontaminační	k2eAgNnSc4d1	dekontaminační
pracoviště	pracoviště	k1gNnSc4	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Odpad	odpad	k1gInSc1	odpad
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
také	také	k6eAd1	také
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
do	do	k7c2	do
neprodyšných	prodyšný	k2eNgInPc2d1	neprodyšný
a	a	k8xC	a
nerozbitných	rozbitný	k2eNgInPc2d1	nerozbitný
obalů	obal	k1gInPc2	obal
a	a	k8xC	a
převezen	převézt	k5eAaPmNgMnS	převézt
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
jeho	jeho	k3xOp3gFnSc2	jeho
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
dekontaminuje	dekontaminovat	k5eAaBmIp3nS	dekontaminovat
celý	celý	k2eAgInSc4d1	celý
prostor	prostor	k1gInSc4	prostor
původního	původní	k2eAgNnSc2d1	původní
dekontaminačního	dekontaminační	k2eAgNnSc2d1	dekontaminační
pracoviště	pracoviště	k1gNnSc2	pracoviště
a	a	k8xC	a
likvidační	likvidační	k2eAgInSc1d1	likvidační
tým	tým	k1gInSc1	tým
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
jednotka	jednotka	k1gFnSc1	jednotka
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
specializovanou	specializovaný	k2eAgFnSc7d1	specializovaná
dekontaminační	dekontaminační	k2eAgFnSc7d1	dekontaminační
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
události	událost	k1gFnSc2	událost
provést	provést	k5eAaPmF	provést
prvotní	prvotní	k2eAgFnPc4d1	prvotní
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vycházíme	vycházet	k5eAaImIp1nP	vycházet
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
improvizovaná	improvizovaný	k2eAgFnSc1d1	improvizovaná
dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
žádná	žádný	k3yNgFnSc1	žádný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
činnosti	činnost	k1gFnSc3	činnost
využít	využít	k5eAaPmF	využít
následující	následující	k2eAgInSc4d1	následující
prostředky	prostředek	k1gInPc4	prostředek
<g/>
:	:	kIx,	:
Persteril	persteril	k1gInSc1	persteril
36	[number]	k4	36
%	%	kIx~	%
jako	jako	k8xS	jako
dekontaminační	dekontaminační	k2eAgFnSc4d1	dekontaminační
látku	látka	k1gFnSc4	látka
(	(	kIx(	(
<g/>
chlornan	chlornan	k1gInSc4	chlornan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
v	v	k7c6	v
případě	případ	k1gInSc6	případ
BCHL	BCHL	kA	BCHL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pevnou	pevný	k2eAgFnSc4d1	pevná
fólii	fólie	k1gFnSc4	fólie
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
4	[number]	k4	4
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s>
<g/>
4	[number]	k4	4
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hadici	hadice	k1gFnSc4	hadice
B	B	kA	B
a	a	k8xC	a
rozdělovač	rozdělovač	k1gInSc4	rozdělovač
jako	jako	k8xC	jako
záchytnou	záchytný	k2eAgFnSc4d1	záchytná
vanu	vana	k1gFnSc4	vana
<g/>
,	,	kIx,	,
kbelík	kbelík	k1gInSc4	kbelík
<g/>
,	,	kIx,	,
košťátko	košťátko	k1gNnSc4	košťátko
a	a	k8xC	a
kartáč	kartáč	k1gInSc4	kartáč
k	k	k7c3	k
nanášení	nanášení	k1gNnSc3	nanášení
dekontaminační	dekontaminační	k2eAgFnSc2d1	dekontaminační
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
hadice	hadice	k1gFnPc4	hadice
C	C	kA	C
a	a	k8xC	a
mlhová	mlhový	k2eAgFnSc1d1	mlhová
proudnice	proudnice	k1gFnSc1	proudnice
místo	místo	k7c2	místo
dekontaminační	dekontaminační	k2eAgFnSc2d1	dekontaminační
sprchy	sprcha	k1gFnSc2	sprcha
<g/>
,	,	kIx,	,
neprodyšné	prodyšný	k2eNgInPc1d1	neprodyšný
obaly	obal	k1gInPc1	obal
na	na	k7c4	na
kontaminované	kontaminovaný	k2eAgInPc4d1	kontaminovaný
věcné	věcný	k2eAgInPc4d1	věcný
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
odpad	odpad	k1gInSc4	odpad
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záchytná	záchytný	k2eAgFnSc1d1	záchytná
vana	vana	k1gFnSc1	vana
(	(	kIx(	(
<g/>
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
hadicí	hadice	k1gFnSc7	hadice
B	B	kA	B
spojenou	spojený	k2eAgFnSc7d1	spojená
do	do	k7c2	do
dvojitého	dvojitý	k2eAgInSc2d1	dvojitý
kruhu	kruh	k1gInSc2	kruh
přes	přes	k7c4	přes
rozdělovač	rozdělovač	k1gInSc4	rozdělovač
a	a	k8xC	a
překrytou	překrytý	k2eAgFnSc7d1	překrytá
plachtou	plachta	k1gFnSc7	plachta
<g/>
)	)	kIx)	)
postačuje	postačovat	k5eAaImIp3nS	postačovat
při	při	k7c6	při
naplnění	naplnění	k1gNnSc6	naplnění
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
5	[number]	k4	5
cm	cm	kA	cm
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
čtyř	čtyři	k4xCgFnPc2	čtyři
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Oplach	oplach	k1gInSc1	oplach
je	být	k5eAaImIp3nS	být
prováděn	provádět	k5eAaImNgInS	provádět
z	z	k7c2	z
bezpečného	bezpečný	k2eAgNnSc2d1	bezpečné
místa	místo	k1gNnSc2	místo
mlhovou	mlhový	k2eAgFnSc7d1	mlhová
proudnicí	proudnice	k1gFnSc7	proudnice
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc1	omezení
přestřiků	přestřik	k1gInPc2	přestřik
a	a	k8xC	a
odrazů	odraz	k1gInPc2	odraz
kapek	kapka	k1gFnPc2	kapka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
výstupem	výstup	k1gInSc7	výstup
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
oplach	oplach	k1gInSc4	oplach
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
obuv	obuv	k1gFnSc4	obuv
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
zasahujících	zasahující	k2eAgFnPc2d1	zasahující
záchranných	záchranný	k2eAgFnPc2d1	záchranná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
úplná	úplný	k2eAgFnSc1d1	úplná
dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
zasažených	zasažený	k2eAgFnPc2d1	zasažená
osob	osoba	k1gFnPc2	osoba
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
provedení	provedení	k1gNnSc6	provedení
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
velitel	velitel	k1gMnSc1	velitel
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Současnými	současný	k2eAgInPc7d1	současný
věcnými	věcný	k2eAgInPc7d1	věcný
prostředky	prostředek	k1gInPc7	prostředek
jednotek	jednotka	k1gFnPc2	jednotka
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
JPO	JPO	kA	JPO
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
úplnou	úplný	k2eAgFnSc4d1	úplná
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
omezeného	omezený	k2eAgInSc2d1	omezený
počtu	počet	k1gInSc2	počet
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
složkou	složka	k1gFnSc7	složka
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
záchranného	záchranný	k2eAgInSc2d1	záchranný
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
provést	provést	k5eAaPmF	provést
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
uvedení	uvedení	k1gNnSc4	uvedení
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
pohotovosti	pohotovost	k1gFnSc2	pohotovost
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
neschopnost	neschopnost	k1gFnSc1	neschopnost
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgNnPc1d1	vojenské
dekontaminační	dekontaminační	k2eAgNnPc1d1	dekontaminační
stanoviště	stanoviště	k1gNnPc1	stanoviště
jsou	být	k5eAaImIp3nP	být
budována	budovat	k5eAaImNgNnP	budovat
na	na	k7c4	na
provádění	provádění	k1gNnSc4	provádění
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
při	při	k7c6	při
vojenských	vojenský	k2eAgFnPc6d1	vojenská
činnostech	činnost	k1gFnPc6	činnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
dekontaminovanými	dekontaminovaný	k2eAgFnPc7d1	dekontaminovaná
osobami	osoba	k1gFnPc7	osoba
pouze	pouze	k6eAd1	pouze
vojáci	voják	k1gMnPc1	voják
(	(	kIx(	(
<g/>
znalost	znalost	k1gFnSc1	znalost
procesu	proces	k1gInSc2	proces
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
civilního	civilní	k2eAgNnSc2d1	civilní
použití	použití	k1gNnSc2	použití
není	být	k5eNaImIp3nS	být
také	také	k9	také
dostatečně	dostatečně	k6eAd1	dostatečně
řešeno	řešen	k2eAgNnSc1d1	řešeno
jímání	jímání	k1gNnSc1	jímání
a	a	k8xC	a
likvidace	likvidace	k1gFnSc1	likvidace
odpadů	odpad	k1gInPc2	odpad
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
také	také	k9	také
uvědomění	uvědomění	k1gNnSc1	uvědomění
si	se	k3xPyFc3	se
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
doposud	doposud	k6eAd1	doposud
nemáme	mít	k5eNaImIp1nP	mít
zkušenosti	zkušenost	k1gFnPc1	zkušenost
s	s	k7c7	s
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
dekontaminací	dekontaminace	k1gFnSc7	dekontaminace
při	při	k7c6	při
mimořádné	mimořádný	k2eAgFnSc6d1	mimořádná
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
poznatky	poznatek	k1gInPc1	poznatek
byly	být	k5eAaImAgInP	být
získány	získat	k5eAaPmNgInP	získat
při	při	k7c6	při
cvičeních	cvičení	k1gNnPc6	cvičení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ZÓNA	zóna	k1gFnSc1	zóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ale	ale	k9	ale
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
případná	případný	k2eAgFnSc1d1	případná
skutečná	skutečný	k2eAgFnSc1d1	skutečná
hromadná	hromadný	k2eAgFnSc1d1	hromadná
dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
simulované	simulovaný	k2eAgFnSc2d1	simulovaná
značně	značně	k6eAd1	značně
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
problémy	problém	k1gInPc4	problém
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
<g/>
:	:	kIx,	:
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
čas	čas	k1gInSc4	čas
přípravy	příprava	k1gFnSc2	příprava
dekontaminačního	dekontaminační	k2eAgNnSc2d1	dekontaminační
pracoviště	pracoviště	k1gNnSc2	pracoviště
<g/>
,	,	kIx,	,
udržení	udržení	k1gNnSc1	udržení
klidu	klid	k1gInSc2	klid
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
kapacitní	kapacitní	k2eAgFnSc1d1	kapacitní
propustnost	propustnost	k1gFnSc1	propustnost
dekontaminačních	dekontaminační	k2eAgNnPc2d1	dekontaminační
pracovišť	pracoviště	k1gNnPc2	pracoviště
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
provedení	provedení	k1gNnSc4	provedení
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zvolit	zvolit	k5eAaPmF	zvolit
účinné	účinný	k2eAgInPc4d1	účinný
a	a	k8xC	a
proveditelné	proveditelný	k2eAgInPc4d1	proveditelný
dekontaminační	dekontaminační	k2eAgInPc4d1	dekontaminační
postupy	postup	k1gInPc4	postup
dostupnými	dostupný	k2eAgInPc7d1	dostupný
prostředky	prostředek	k1gInPc7	prostředek
(	(	kIx(	(
<g/>
záchrana	záchrana	k1gFnSc1	záchrana
životů	život	k1gInPc2	život
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
zdraví	zdraví	k1gNnSc2	zdraví
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
<g/>
)	)	kIx)	)
a	a	k8xC	a
snížit	snížit	k5eAaPmF	snížit
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
ohrožující	ohrožující	k2eAgInSc4d1	ohrožující
zdravotnický	zdravotnický	k2eAgInSc4d1	zdravotnický
personál	personál	k1gInSc4	personál
při	při	k7c6	při
ošetřování	ošetřování	k1gNnSc6	ošetřování
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hromadná	hromadný	k2eAgFnSc1d1	hromadná
dekontaminace	dekontaminace	k1gFnSc1	dekontaminace
osob	osoba	k1gFnPc2	osoba
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
(	(	kIx(	(
<g/>
snížení	snížení	k1gNnSc1	snížení
možnosti	možnost	k1gFnSc2	možnost
dalšího	další	k2eAgNnSc2d1	další
působení	působení	k1gNnSc2	působení
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
bude	být	k5eAaImBp3nS	být
ihned	ihned	k6eAd1	ihned
dostatek	dostatek	k1gInSc1	dostatek
speciálních	speciální	k2eAgFnPc2d1	speciální
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
způsobem	způsob	k1gInSc7	způsob
provedení	provedení	k1gNnSc2	provedení
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
sprchového	sprchový	k2eAgInSc2d1	sprchový
vodního	vodní	k2eAgInSc2d1	vodní
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
mýdla	mýdlo	k1gNnSc2	mýdlo
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
tekutého	tekutý	k2eAgNnSc2d1	tekuté
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
kontaminant	kontaminant	k1gInSc4	kontaminant
plynného	plynný	k2eAgNnSc2d1	plynné
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zachycen	zachytit	k5eAaPmNgInS	zachytit
uvnitř	uvnitř	k7c2	uvnitř
oděvů	oděv	k1gInPc2	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
přihlédnutí	přihlédnutí	k1gNnSc6	přihlédnutí
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
podmínkám	podmínka	k1gFnPc3	podmínka
(	(	kIx(	(
<g/>
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
kontaminantu	kontaminant	k1gInSc2	kontaminant
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
již	již	k6eAd1	již
samotné	samotný	k2eAgNnSc1d1	samotné
svlečení	svlečení	k1gNnSc1	svlečení
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
oděvů	oděv	k1gInPc2	oděv
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
část	část	k1gFnSc4	část
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Očekávat	očekávat	k5eAaImF	očekávat
určitý	určitý	k2eAgInSc4d1	určitý
poměr	poměr	k1gInSc4	poměr
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
i	i	k8xC	i
nekontaminovaných	kontaminovaný	k2eNgFnPc2d1	nekontaminovaná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
určité	určitý	k2eAgNnSc1d1	určité
procento	procento	k1gNnSc1	procento
(	(	kIx(	(
<g/>
např.	např.	kA	např.
až	až	k9	až
75	[number]	k4	75
%	%	kIx~	%
v	v	k7c6	v
případě	případ	k1gInSc6	případ
teroristického	teroristický	k2eAgInSc2d1	teroristický
útoku	útok	k1gInSc2	útok
<g/>
)	)	kIx)	)
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
postižených	postižený	k1gMnPc2	postižený
bude	být	k5eAaImBp3nS	být
oběťmi	oběť	k1gFnPc7	oběť
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
přehnané	přehnaný	k2eAgFnSc2d1	přehnaná
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zamezení	zamezení	k1gNnSc3	zamezení
zahlcení	zahlcení	k1gNnSc2	zahlcení
dekontaminačních	dekontaminační	k2eAgNnPc2d1	dekontaminační
pracovišť	pracoviště	k1gNnPc2	pracoviště
bude	být	k5eAaImBp3nS	být
nutné	nutný	k2eAgFnPc4d1	nutná
osoby	osoba	k1gFnPc4	osoba
roztřídit	roztřídit	k5eAaPmF	roztřídit
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využito	využít	k5eAaPmNgNnS	využít
<g/>
:	:	kIx,	:
improvizovaných	improvizovaný	k2eAgNnPc2d1	improvizované
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c4	o
žebříkové	žebříkový	k2eAgNnSc4d1	žebříkové
provedení	provedení	k1gNnSc4	provedení
(	(	kIx(	(
<g/>
využití	využití	k1gNnSc4	využití
automobilového	automobilový	k2eAgInSc2d1	automobilový
žebříku	žebřík	k1gInSc2	žebřík
a	a	k8xC	a
cisternových	cisternový	k2eAgFnPc2d1	cisternová
automobilových	automobilový	k2eAgFnPc2d1	automobilová
stříkaček	stříkačka	k1gFnPc2	stříkačka
ke	k	k7c3	k
zkrápění	zkrápění	k1gNnSc3	zkrápění
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
a	a	k8xC	a
systém	systém	k1gInSc1	systém
nouzové	nouzový	k2eAgFnSc2d1	nouzová
dekontaminační	dekontaminační	k2eAgFnSc2d1	dekontaminační
chodby	chodba	k1gFnSc2	chodba
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
souběžně	souběžně	k6eAd1	souběžně
stojící	stojící	k2eAgNnPc1d1	stojící
vozidla	vozidlo	k1gNnPc1	vozidlo
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
žebříků	žebřík	k1gInPc2	žebřík
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
chodba	chodba	k1gFnSc1	chodba
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
stanoviště	stanoviště	k1gNnPc1	stanoviště
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgInSc4d2	nižší
komfort	komfort	k1gInSc4	komfort
dekontaminovaných	dekontaminovaný	k2eAgFnPc2d1	dekontaminovaná
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
problematické	problematický	k2eAgNnSc1d1	problematické
zachytávání	zachytávání	k1gNnSc1	zachytávání
odpadní	odpadní	k2eAgFnSc2d1	odpadní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
mobilních	mobilní	k2eAgFnPc2d1	mobilní
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
stanovišť	stanoviště	k1gNnPc2	stanoviště
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP	umožňovat
provedení	provedení	k1gNnSc4	provedení
kryté	krytý	k2eAgFnSc2d1	krytá
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vytápěno	vytápěn	k2eAgNnSc1d1	vytápěno
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
teplá	teplý	k2eAgFnSc1d1	teplá
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Odpadní	odpadní	k2eAgFnSc1d1	odpadní
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
zachytává	zachytávat	k5eAaImIp3nS	zachytávat
a	a	k8xC	a
následně	následně	k6eAd1	následně
likviduje	likvidovat	k5eAaBmIp3nS	likvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
aktivace	aktivace	k1gFnSc1	aktivace
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
dovezení	dovezení	k1gNnSc2	dovezení
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
události	událost	k1gFnSc2	událost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vhodně	vhodně	k6eAd1	vhodně
umístěných	umístěný	k2eAgNnPc2d1	umístěné
a	a	k8xC	a
vybavených	vybavený	k2eAgNnPc2d1	vybavené
veřejných	veřejný	k2eAgNnPc2d1	veřejné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vhodné	vhodný	k2eAgFnSc6d1	vhodná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
riziko	riziko	k1gNnSc4	riziko
úniku	únik	k1gInSc2	únik
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yIgNnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
sociální	sociální	k2eAgNnPc1d1	sociální
zařízení	zařízení	k1gNnPc1	zařízení
pro	pro	k7c4	pro
sprchovou	sprchový	k2eAgFnSc4d1	sprchová
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
vhodné	vhodný	k2eAgFnPc4d1	vhodná
kapacity	kapacita	k1gFnPc4	kapacita
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stanovením	stanovení	k1gNnSc7	stanovení
vhodného	vhodný	k2eAgNnSc2d1	vhodné
pořadí	pořadí	k1gNnSc2	pořadí
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
osob	osoba	k1gFnPc2	osoba
lze	lze	k6eAd1	lze
omezit	omezit	k5eAaPmF	omezit
negativní	negativní	k2eAgInPc4d1	negativní
účinky	účinek	k1gInPc4	účinek
kontaminantu	kontaminant	k1gInSc2	kontaminant
na	na	k7c4	na
životy	život	k1gInPc4	život
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
těchto	tento	k3xDgFnPc2	tento
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Stanovení	stanovení	k1gNnSc1	stanovení
pořadí	pořadí	k1gNnSc2	pořadí
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
případě	případ	k1gInSc6	případ
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
zasažených	zasažený	k2eAgFnPc2d1	zasažená
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
kapacity	kapacita	k1gFnPc1	kapacita
dekontaminačního	dekontaminační	k2eAgNnSc2d1	dekontaminační
stanoviště	stanoviště	k1gNnSc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
nezbytnosti	nezbytnost	k1gFnSc2	nezbytnost
ošetření	ošetření	k1gNnSc2	ošetření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mezi	mezi	k7c7	mezi
kontaminovanými	kontaminovaný	k2eAgFnPc7d1	kontaminovaná
stanovit	stanovit	k5eAaPmF	stanovit
vhodně	vhodně	k6eAd1	vhodně
vybavený	vybavený	k2eAgInSc4d1	vybavený
a	a	k8xC	a
vyškolený	vyškolený	k2eAgInSc4d1	vyškolený
personál	personál	k1gInSc4	personál
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
pohyblivé	pohyblivý	k2eAgMnPc4d1	pohyblivý
(	(	kIx(	(
<g/>
schopné	schopný	k2eAgMnPc4d1	schopný
porozumět	porozumět	k5eAaPmF	porozumět
pokynům	pokyn	k1gInPc3	pokyn
a	a	k8xC	a
samostatné	samostatný	k2eAgFnPc4d1	samostatná
chůze	chůze	k1gFnPc4	chůze
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepohyblivé	pohyblivý	k2eNgNnSc1d1	nepohyblivé
(	(	kIx(	(
<g/>
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
<g/>
,	,	kIx,	,
nereagující	reagující	k2eNgInPc1d1	nereagující
<g/>
,	,	kIx,	,
neschopné	schopný	k2eNgInPc1d1	neschopný
samostatného	samostatný	k2eAgInSc2d1	samostatný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Priority	priorita	k1gFnPc1	priorita
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
osob	osoba	k1gFnPc2	osoba
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
osoby	osoba	k1gFnPc1	osoba
nejblíže	blízce	k6eAd3	blízce
místa	místo	k1gNnSc2	místo
úniku	únik	k1gInSc2	únik
nebo	nebo	k8xC	nebo
se	s	k7c7	s
zjevnou	zjevný	k2eAgFnSc7d1	zjevná
přítomností	přítomnost	k1gFnSc7	přítomnost
kontaminantu	kontaminant	k1gInSc2	kontaminant
<g/>
,	,	kIx,	,
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
u	u	k7c2	u
místa	místo	k1gNnSc2	místo
úniku	únik	k1gInSc2	únik
ale	ale	k8xC	ale
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
zasažení	zasažení	k1gNnSc1	zasažení
<g/>
,	,	kIx,	,
osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
běžnými	běžný	k2eAgNnPc7d1	běžné
zraněními	zranění	k1gNnPc7	zranění
(	(	kIx(	(
<g/>
otevřené	otevřený	k2eAgFnPc4d1	otevřená
rány	rána	k1gFnPc4	rána
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
úniku	únik	k1gInSc2	únik
a	a	k8xC	a
nevykazují	vykazovat	k5eNaImIp3nP	vykazovat
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepohyblivé	pohyblivý	k2eNgFnPc1d1	nepohyblivá
osoby	osoba	k1gFnPc1	osoba
jsou	být	k5eAaImIp3nP	být
roztříděny	roztřídit	k5eAaPmNgFnP	roztřídit
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
systému	systém	k1gInSc2	systém
START	start	k1gInSc1	start
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgFnP	označit
přidělenou	přidělený	k2eAgFnSc7d1	přidělená
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
priority	priorita	k1gFnPc1	priorita
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgNnPc1d1	následující
<g/>
:	:	kIx,	:
červená	červené	k1gNnPc1	červené
(	(	kIx(	(
<g/>
osoby	osoba	k1gFnSc2	osoba
se	s	k7c7	s
selhávajícím	selhávající	k2eAgInSc7d1	selhávající
základními	základní	k2eAgFnPc7d1	základní
životními	životní	k2eAgFnPc7d1	životní
funkcemi	funkce	k1gFnPc7	funkce
<g/>
;	;	kIx,	;
nutný	nutný	k2eAgInSc1d1	nutný
život	život	k1gInSc1	život
zachraňující	zachraňující	k2eAgInSc1d1	zachraňující
lékařský	lékařský	k2eAgInSc1d1	lékařský
zásah	zásah	k1gInSc1	zásah
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
dostupnými	dostupný	k2eAgInPc7d1	dostupný
prostředky	prostředek	k1gInPc7	prostředek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlutá	žlutat	k5eAaImIp3nS	žlutat
(	(	kIx(	(
<g/>
osoby	osoba	k1gFnPc1	osoba
s	s	k7c7	s
vážnými	vážný	k2eAgNnPc7d1	vážné
zraněními	zranění	k1gNnPc7	zranění
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
ošetření	ošetření	k1gNnPc1	ošetření
lze	lze	k6eAd1	lze
dočasně	dočasně	k6eAd1	dočasně
odložit	odložit	k5eAaPmF	odložit
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
negativní	negativní	k2eAgInSc4d1	negativní
následek	následek	k1gInSc4	následek
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
(	(	kIx(	(
<g/>
život	život	k1gInSc4	život
neohrožující	ohrožující	k2eNgInSc4d1	neohrožující
stav	stav	k1gInSc4	stav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černá	černat	k5eAaImIp3nS	černat
(	(	kIx(	(
<g/>
osoby	osoba	k1gFnPc4	osoba
zemřelé	zemřelý	k2eAgFnSc2d1	zemřelá
nebo	nebo	k8xC	nebo
umírající	umírající	k2eAgFnSc2d1	umírající
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgNnSc1d1	ideální
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
zřízení	zřízení	k1gNnSc1	zřízení
dvou	dva	k4xCgNnPc2	dva
dekontaminačních	dekontaminační	k2eAgNnPc2d1	dekontaminační
stanovišť	stanoviště	k1gNnPc2	stanoviště
zvlášť	zvlášť	k9	zvlášť
pro	pro	k7c4	pro
pohyblivé	pohyblivý	k2eAgFnPc4d1	pohyblivá
a	a	k8xC	a
nepohyblivé	pohyblivý	k2eNgFnPc4d1	nepohyblivá
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
nepohyblivé	pohyblivý	k2eNgFnPc4d1	nepohyblivá
osoby	osoba	k1gFnPc4	osoba
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
4	[number]	k4	4
<g/>
.	.	kIx.	.
priority	priorita	k1gFnPc1	priorita
<g/>
)	)	kIx)	)
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
pohyblivými	pohyblivý	k2eAgFnPc7d1	pohyblivá
se	se	k3xPyFc4	se
stejnou	stejný	k2eAgFnSc7d1	stejná
prioritou	priorita	k1gFnSc7	priorita
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stanoviště	stanoviště	k1gNnSc1	stanoviště
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
určené	určený	k2eAgNnSc1d1	určené
k	k	k7c3	k
účinnému	účinný	k2eAgNnSc3d1	účinné
provedení	provedení	k1gNnSc3	provedení
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
ranění	raněný	k1gMnPc1	raněný
<g/>
,	,	kIx,	,
nepohybliví	pohyblivý	k2eNgMnPc1d1	nepohyblivý
<g/>
)	)	kIx)	)
od	od	k7c2	od
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
kontaminantů	kontaminant	k1gInPc2	kontaminant
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
skupenstvích	skupenství	k1gNnPc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1	hasičský
záchranný	záchranný	k2eAgInSc1d1	záchranný
sbor	sbor	k1gInSc1	sbor
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
HZS	HZS	kA	HZS
<g/>
)	)	kIx)	)
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vybaven	vybavit	k5eAaPmNgInS	vybavit
dvěma	dva	k4xCgInPc7	dva
typy	typ	k1gInPc7	typ
SDO	SDO	kA	SDO
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
AČR	AČR	kA	AČR
<g/>
)	)	kIx)	)
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
vybavována	vybavovat	k5eAaImNgFnS	vybavovat
dekontaminačními	dekontaminační	k2eAgFnPc7d1	dekontaminační
zařízeními	zařízení	k1gNnPc7	zařízení
kompatibilními	kompatibilní	k2eAgFnPc7d1	kompatibilní
se	s	k7c7	s
SDO	SDO	kA	SDO
HZS	HZS	kA	HZS
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožní	umožnit	k5eAaPmIp3nS	umožnit
lepší	dobrý	k2eAgFnSc4d2	lepší
spolupráci	spolupráce	k1gFnSc4	spolupráce
těchto	tento	k3xDgFnPc2	tento
složek	složka	k1gFnPc2	složka
při	při	k7c6	při
společném	společný	k2eAgInSc6d1	společný
zásahu	zásah	k1gInSc6	zásah
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
záchranného	záchranný	k2eAgInSc2d1	záchranný
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stanoviště	stanoviště	k1gNnSc1	stanoviště
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
provedení	provedení	k1gNnSc4	provedení
všech	všecek	k3xTgFnPc2	všecek
dekontaminačních	dekontaminační	k2eAgFnPc2d1	dekontaminační
činností	činnost	k1gFnPc2	činnost
uvnitř	uvnitř	k7c2	uvnitř
stanů	stan	k1gInPc2	stan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
třemi	tři	k4xCgInPc7	tři
stany	stan	k1gInPc7	stan
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
osob	osoba	k1gFnPc2	osoba
sestavených	sestavený	k2eAgFnPc2d1	sestavená
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
dekontaminačního	dekontaminační	k2eAgNnSc2d1	dekontaminační
pracoviště	pracoviště	k1gNnSc2	pracoviště
obsluhy	obsluha	k1gFnSc2	obsluha
a	a	k8xC	a
technologického	technologický	k2eAgNnSc2d1	Technologické
vybavení	vybavení	k1gNnSc2	vybavení
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
soustava	soustava	k1gFnSc1	soustava
s	s	k7c7	s
průtokovým	průtokový	k2eAgInSc7d1	průtokový
ohřívačem	ohřívač	k1gInSc7	ohřívač
pro	pro	k7c4	pro
oplachování	oplachování	k1gNnSc4	oplachování
teplou	teplý	k2eAgFnSc7d1	teplá
vodou	voda	k1gFnSc7	voda
<g/>
;	;	kIx,	;
soustava	soustava	k1gFnSc1	soustava
pro	pro	k7c4	pro
odčerpávání	odčerpávání	k1gNnSc4	odčerpávání
odpadní	odpadní	k2eAgFnSc2d1	odpadní
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
záchytných	záchytný	k2eAgFnPc2d1	záchytná
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
;	;	kIx,	;
vytápěcí	vytápěcí	k2eAgInSc1d1	vytápěcí
agregát	agregát	k1gInSc1	agregát
s	s	k7c7	s
rozvodem	rozvod	k1gInSc7	rozvod
teplého	teplý	k2eAgInSc2d1	teplý
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
;	;	kIx,	;
elektrocentrála	elektrocentrála	k1gFnSc1	elektrocentrála
s	s	k7c7	s
rozvody	rozvod	k1gInPc7	rozvod
pro	pro	k7c4	pro
osvětlení	osvětlení	k1gNnSc4	osvětlení
<g/>
;	;	kIx,	;
zdroj	zdroj	k1gInSc1	zdroj
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
mobilních	mobilní	k2eAgInPc2d1	mobilní
stanů	stan	k1gInPc2	stan
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
nosnými	nosný	k2eAgInPc7d1	nosný
nafukovacími	nafukovací	k2eAgInPc7d1	nafukovací
válci	válec	k1gInPc7	válec
spojenými	spojený	k2eAgInPc7d1	spojený
spojovacími	spojovací	k2eAgInPc7d1	spojovací
moduly	modul	k1gInPc7	modul
<g/>
,	,	kIx,	,
podlahami	podlaha	k1gFnPc7	podlaha
a	a	k8xC	a
plášti	plášť	k1gInPc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
součásti	součást	k1gFnPc1	součást
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
snadno	snadno	k6eAd1	snadno
dekontaminovatelných	dekontaminovatelný	k2eAgInPc2d1	dekontaminovatelný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Stany	stan	k1gInPc1	stan
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podélné	podélný	k2eAgFnSc6d1	podélná
ose	osa	k1gFnSc6	osa
vyztuženy	vyztužen	k2eAgInPc1d1	vyztužen
rozpěrnými	rozpěrný	k2eAgFnPc7d1	rozpěrná
tyčemi	tyč	k1gFnPc7	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměra	k1gFnPc4	rozměra
jednoho	jeden	k4xCgInSc2	jeden
stanu	stan	k1gInSc2	stan
jsou	být	k5eAaImIp3nP	být
6	[number]	k4	6
x	x	k?	x
6	[number]	k4	6
x	x	k?	x
3,3	[number]	k4	3,3
m	m	kA	m
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
stanoviště	stanoviště	k1gNnSc2	stanoviště
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Podélně	podélně	k6eAd1	podélně
jsou	být	k5eAaImIp3nP	být
stany	stan	k1gInPc1	stan
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
zástěnami	zástěna	k1gFnPc7	zástěna
pro	pro	k7c4	pro
oddělenou	oddělený	k2eAgFnSc4d1	oddělená
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dostatečné	dostatečný	k2eAgFnSc3d1	dostatečná
propustnosti	propustnost	k1gFnSc3	propustnost
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
přes	přes	k7c4	přes
stěny	stěn	k1gInPc4	stěn
je	být	k5eAaImIp3nS	být
osvětlení	osvětlení	k1gNnSc1	osvětlení
instalováno	instalovat	k5eAaBmNgNnS	instalovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
stanů	stan	k1gInPc2	stan
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
prostor	prostor	k1gInSc1	prostor
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
záchytnými	záchytný	k2eAgFnPc7d1	záchytná
vanami	vana	k1gFnPc7	vana
a	a	k8xC	a
dekontaminační	dekontaminační	k2eAgFnSc7d1	dekontaminační
sprchou	sprcha	k1gFnSc7	sprcha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vana	vana	k1gFnSc1	vana
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odkládání	odkládání	k1gNnSc3	odkládání
kontaminovaných	kontaminovaný	k2eAgInPc2d1	kontaminovaný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vana	vana	k1gFnSc1	vana
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
pochozími	pochozí	k2eAgInPc7d1	pochozí
rošty	rošt	k1gInPc7	rošt
s	s	k7c7	s
protiskluzovou	protiskluzový	k2eAgFnSc7d1	protiskluzová
rohoží	rohož	k1gFnSc7	rohož
<g/>
.	.	kIx.	.
</s>
<s>
Sprcha	sprcha	k1gFnSc1	sprcha
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
i	i	k9	i
ruční	ruční	k2eAgFnSc7d1	ruční
rozstřikovací	rozstřikovací	k2eAgFnSc7d1	rozstřikovací
hubicí	hubice	k1gFnSc7	hubice
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
hadici	hadice	k1gFnSc6	hadice
<g/>
.	.	kIx.	.
</s>
<s>
Odčerpávání	odčerpávání	k1gNnSc1	odčerpávání
odpadní	odpadní	k2eAgFnSc2d1	odpadní
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
ponorným	ponorný	k2eAgNnSc7d1	ponorné
čerpadlem	čerpadlo	k1gNnSc7	čerpadlo
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
stanu	stan	k1gInSc6	stan
se	se	k3xPyFc4	se
osoby	osoba	k1gFnPc1	osoba
svlékají	svlékat	k5eAaImIp3nP	svlékat
a	a	k8xC	a
kontaminované	kontaminovaný	k2eAgNnSc4d1	kontaminované
oblečení	oblečení	k1gNnSc4	oblečení
odkládají	odkládat	k5eAaImIp3nP	odkládat
do	do	k7c2	do
připravených	připravený	k2eAgInPc2d1	připravený
neprodyšných	prodyšný	k2eNgInPc2d1	neprodyšný
obalů	obal	k1gInPc2	obal
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnPc1d1	osobní
věci	věc	k1gFnPc1	věc
jsou	být	k5eAaImIp3nP	být
ukládány	ukládat	k5eAaImNgFnP	ukládat
zvlášť	zvlášť	k6eAd1	zvlášť
do	do	k7c2	do
malých	malý	k2eAgInPc2d1	malý
neprodyšných	prodyšný	k2eNgInPc2d1	neprodyšný
obalů	obal	k1gInPc2	obal
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
případnou	případný	k2eAgFnSc4d1	případná
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
i	i	k9	i
dané	daný	k2eAgFnSc3d1	daná
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Stan	stan	k1gInSc1	stan
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
snadno	snadno	k6eAd1	snadno
dekontaminovatelnými	dekontaminovatelný	k2eAgFnPc7d1	dekontaminovatelný
židlemi	židle	k1gFnPc7	židle
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k8xC	i
nosítky	nosítka	k1gNnPc7	nosítka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
provádí	provádět	k5eAaImIp3nS	provádět
výplach	výplach	k1gInSc1	výplach
očí	oko	k1gNnPc2	oko
speciálním	speciální	k2eAgInSc7d1	speciální
roztokem	roztok	k1gInSc7	roztok
a	a	k8xC	a
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc7d1	ústní
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
výtěr	výtěr	k1gInSc1	výtěr
ušních	ušní	k2eAgFnPc2d1	ušní
a	a	k8xC	a
nosních	nosní	k2eAgFnPc2d1	nosní
dutin	dutina	k1gFnPc2	dutina
vatovými	vatový	k2eAgFnPc7d1	vatová
tyčinkami	tyčinka	k1gFnPc7	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
nádoby	nádoba	k1gFnPc1	nádoba
na	na	k7c4	na
odpadní	odpadní	k2eAgFnSc4d1	odpadní
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
použité	použitý	k2eAgInPc1d1	použitý
předměty	předmět	k1gInPc1	předmět
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
stanu	stan	k1gInSc6	stan
probíhá	probíhat	k5eAaImIp3nS	probíhat
proces	proces	k1gInSc1	proces
vnější	vnější	k2eAgFnSc2d1	vnější
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
mokrým	mokrý	k2eAgInSc7d1	mokrý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
trysek	tryska	k1gFnPc2	tryska
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ruční	ruční	k2eAgFnSc1d1	ruční
rozstřikovací	rozstřikovací	k2eAgFnSc1d1	rozstřikovací
hubice	hubice	k1gFnSc1	hubice
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
hadici	hadice	k1gFnSc6	hadice
pro	pro	k7c4	pro
oplach	oplach	k1gInSc4	oplach
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velká	velký	k2eAgFnSc1d1	velká
záchytná	záchytný	k2eAgFnSc1d1	záchytná
vana	vana	k1gFnSc1	vana
na	na	k7c4	na
odpadní	odpadní	k2eAgFnSc4d1	odpadní
vodu	voda	k1gFnSc4	voda
s	s	k7c7	s
pochozími	pochozí	k2eAgInPc7d1	pochozí
rošty	rošt	k1gInPc7	rošt
s	s	k7c7	s
protiskluzovou	protiskluzový	k2eAgFnSc7d1	protiskluzová
rohoží	rohož	k1gFnSc7	rohož
<g/>
.	.	kIx.	.
</s>
<s>
Odčerpávání	odčerpávání	k1gNnSc1	odčerpávání
odpadní	odpadní	k2eAgFnSc2d1	odpadní
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
ponorným	ponorný	k2eAgNnSc7d1	ponorné
čerpadlem	čerpadlo	k1gNnSc7	čerpadlo
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
stanu	stan	k1gInSc6	stan
se	se	k3xPyFc4	se
osoby	osoba	k1gFnPc1	osoba
osuší	osušit	k5eAaPmIp3nP	osušit
jednorázovými	jednorázový	k2eAgInPc7d1	jednorázový
ručníky	ručník	k1gInPc7	ručník
a	a	k8xC	a
obléknou	obléknout	k5eAaPmIp3nP	obléknout
do	do	k7c2	do
náhradního	náhradní	k2eAgNnSc2d1	náhradní
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
nádoby	nádoba	k1gFnPc1	nádoba
na	na	k7c4	na
použité	použitý	k2eAgInPc4d1	použitý
ručníky	ručník	k1gInPc4	ručník
a	a	k8xC	a
sorpční	sorpční	k2eAgFnSc1d1	sorpční
podlaha	podlaha	k1gFnSc1	podlaha
na	na	k7c4	na
jímání	jímání	k1gNnSc4	jímání
úkapů	úkap	k1gInPc2	úkap
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
provádí	provádět	k5eAaImIp3nS	provádět
případná	případný	k2eAgFnSc1d1	případná
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
detekce	detekce	k1gFnSc1	detekce
<g/>
.	.	kIx.	.
</s>
<s>
SDO	SDO	kA	SDO
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
uvést	uvést	k5eAaPmF	uvést
do	do	k7c2	do
pohotovosti	pohotovost	k1gFnSc2	pohotovost
družstvem	družstvo	k1gNnSc7	družstvo
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
do	do	k7c2	do
25	[number]	k4	25
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
polovině	polovina	k1gFnSc6	polovina
SDO	SDO	kA	SDO
1	[number]	k4	1
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
rozdílem	rozdíl	k1gInSc7	rozdíl
od	od	k7c2	od
SDO	SDO	kA	SDO
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
zkrácení	zkrácení	k1gNnSc4	zkrácení
doby	doba	k1gFnSc2	doba
uvedení	uvedení	k1gNnSc2	uvedení
do	do	k7c2	do
pohotovosti	pohotovost	k1gFnSc2	pohotovost
(	(	kIx(	(
<g/>
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvounápravovým	dvounápravový	k2eAgInSc7d1	dvounápravový
přívěsem	přívěs	k1gInSc7	přívěs
s	s	k7c7	s
výklopnými	výklopný	k2eAgInPc7d1	výklopný
bočními	boční	k2eAgInPc7d1	boční
vraty	vrat	k1gInPc7	vrat
pod	pod	k7c7	pod
kterými	který	k3yQgInPc7	který
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
stanový	stanový	k2eAgInSc1d1	stanový
dílec	dílec	k1gInSc1	dílec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
vrat	vrata	k1gNnPc2	vrata
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
technologický	technologický	k2eAgInSc1d1	technologický
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
obsluhu	obsluha	k1gFnSc4	obsluha
a	a	k8xC	a
v	v	k7c4	v
zadní	zadní	k2eAgInSc4d1	zadní
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
přívěsu	přívěs	k1gInSc2	přívěs
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
samotný	samotný	k2eAgInSc4d1	samotný
proces	proces	k1gInSc4	proces
mokré	mokrý	k2eAgFnSc2d1	mokrá
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
provádění	provádění	k1gNnSc2	provádění
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
související	související	k2eAgFnPc4d1	související
činnosti	činnost	k1gFnPc4	činnost
jsou	být	k5eAaImIp3nP	být
obdobné	obdobný	k2eAgInPc1d1	obdobný
se	s	k7c7	s
SDO	SDO	kA	SDO
1	[number]	k4	1
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vybrané	vybraný	k2eAgFnSc2d1	vybraná
složky	složka	k1gFnSc2	složka
AČR	AČR	kA	AČR
by	by	kYmCp3nP	by
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
postiženého	postižený	k2eAgNnSc2d1	postižené
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
pomoci	pomoc	k1gFnSc2	pomoc
na	na	k7c4	na
vyžádání	vyžádání	k1gNnSc4	vyžádání
při	při	k7c6	při
krizových	krizový	k2eAgFnPc6d1	krizová
situacích	situace	k1gFnPc6	situace
velkého	velký	k2eAgInSc2d1	velký
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
prostředky	prostředek	k1gInPc4	prostředek
AČR	AČR	kA	AČR
využívají	využívat	k5eAaImIp3nP	využívat
mokrého	mokrý	k2eAgInSc2d1	mokrý
způsobu	způsob	k1gInSc2	způsob
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
využitím	využití	k1gNnSc7	využití
sprch	sprcha	k1gFnPc2	sprcha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hromadné	hromadný	k2eAgFnSc3d1	hromadná
dekontaminaci	dekontaminace	k1gFnSc3	dekontaminace
osob	osoba	k1gFnPc2	osoba
jsou	být	k5eAaImIp3nP	být
využitelné	využitelný	k2eAgInPc1d1	využitelný
především	především	k6eAd1	především
následující	následující	k2eAgInPc4d1	následující
prostředky	prostředek	k1gInPc4	prostředek
<g/>
:	:	kIx,	:
malá	malý	k2eAgFnSc1d1	malá
koupací	koupací	k2eAgFnSc1d1	koupací
souprava	souprava	k1gFnSc1	souprava
MKS	MKS	kA	MKS
(	(	kIx(	(
<g/>
kapacita	kapacita	k1gFnSc1	kapacita
48	[number]	k4	48
osob	osoba	k1gFnPc2	osoba
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dezinfekční	dezinfekční	k2eAgInSc1d1	dezinfekční
převozní	převozní	k2eAgInSc1d1	převozní
přístroj	přístroj	k1gInSc1	přístroj
PDP	PDP	kA	PDP
1	[number]	k4	1
a	a	k8xC	a
PDP	PDP	kA	PDP
2	[number]	k4	2
(	(	kIx(	(
<g/>
kapacita	kapacita	k1gFnSc1	kapacita
60	[number]	k4	60
osob	osoba	k1gFnPc2	osoba
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koupací	koupací	k2eAgFnSc1d1	koupací
souprava	souprava	k1gFnSc1	souprava
VANA	vana	k1gFnSc1	vana
(	(	kIx(	(
<g/>
kapacita	kapacita	k1gFnSc1	kapacita
150	[number]	k4	150
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souprava	souprava	k1gFnSc1	souprava
dekontaminace	dekontaminace	k1gFnSc2	dekontaminace
osob	osoba	k1gFnPc2	osoba
SDO	SDO	kA	SDO
(	(	kIx(	(
<g/>
obdobné	obdobný	k2eAgFnSc2d1	obdobná
jako	jako	k8xC	jako
SDO	SDO	kA	SDO
1	[number]	k4	1
u	u	k7c2	u
HZS	HZS	kA	HZS
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
bude	být	k5eAaImBp3nS	být
pojednávat	pojednávat	k5eAaImF	pojednávat
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
příjmu	příjem	k1gInSc2	příjem
až	až	k9	až
po	po	k7c4	po
její	její	k3xOp3gNnSc4	její
vyloučení	vyloučení	k1gNnSc4	vyloučení
-	-	kIx~	-
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
pohlížet	pohlížet	k5eAaImF	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
biologickou	biologický	k2eAgFnSc4d1	biologická
detoxikaci	detoxikace	k1gFnSc4	detoxikace
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
zařazena	zařadit	k5eAaPmNgFnS	zařadit
právě	právě	k9	právě
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
znamená	znamenat	k5eAaImIp3nS	znamenat
povrchová	povrchový	k2eAgFnSc1d1	povrchová
kontaminace	kontaminace	k1gFnSc1	kontaminace
usazení	usazení	k1gNnSc2	usazení
nebo	nebo	k8xC	nebo
nahromadění	nahromadění	k1gNnSc2	nahromadění
kontaminantu	kontaminant	k1gInSc2	kontaminant
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
kontaminace	kontaminace	k1gFnSc1	kontaminace
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
důsledek	důsledek	k1gInSc1	důsledek
průniku	průnik	k1gInSc2	průnik
kontaminantu	kontaminant	k1gInSc2	kontaminant
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
struktur	struktura	k1gFnPc2	struktura
lidského	lidský	k2eAgNnSc2d1	lidské
org	org	k?	org
<g/>
.	.	kIx.	.
přes	přes	k7c4	přes
tzv.	tzv.	kA	tzv.
brány	brána	k1gFnSc2	brána
vstupu	vstup	k1gInSc2	vstup
(	(	kIx(	(
<g/>
absorpce	absorpce	k1gFnSc1	absorpce
plícemi	plíce	k1gFnPc7	plíce
<g/>
,	,	kIx,	,
gastrointestinálním	gastrointestinální	k2eAgInSc7d1	gastrointestinální
traktem	trakt	k1gInSc7	trakt
<g/>
,	,	kIx,	,
kůží	kůže	k1gFnSc7	kůže
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Absorpce	absorpce	k1gFnSc1	absorpce
plícemi	plíce	k1gFnPc7	plíce
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
zejména	zejména	k9	zejména
při	při	k7c6	při
vdechování	vdechování	k1gNnSc6	vdechování
vzduchu	vzduch	k1gInSc2	vzduch
znečistěného	znečistěný	k2eAgInSc2d1	znečistěný
plyny	plyn	k1gInPc7	plyn
<g/>
,	,	kIx,	,
parami	para	k1gFnPc7	para
a	a	k8xC	a
aerosoly	aerosol	k1gInPc1	aerosol
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
uzpůsobení	uzpůsobení	k1gNnSc3	uzpůsobení
plic	plíce	k1gFnPc2	plíce
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
absorpce	absorpce	k1gFnSc1	absorpce
přes	přes	k7c4	přes
ně	on	k3xPp3gFnPc4	on
velmi	velmi	k6eAd1	velmi
rychlá	rychlý	k2eAgFnSc1d1	rychlá
(	(	kIx(	(
<g/>
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
rozpustnosti	rozpustnost	k1gFnSc2	rozpustnost
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vdechnutí	vdechnutí	k1gNnSc2	vdechnutí
pevných	pevný	k2eAgFnPc2d1	pevná
<g/>
,	,	kIx,	,
kapalných	kapalný	k2eAgFnPc2d1	kapalná
částic	částice	k1gFnPc2	částice
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
1	[number]	k4	1
milimetr	milimetr	k1gInSc4	milimetr
a	a	k8xC	a
méně	málo	k6eAd2	málo
odolávají	odolávat	k5eAaImIp3nP	odolávat
fyziologickým	fyziologický	k2eAgInPc3d1	fyziologický
mechanismům	mechanismus	k1gInPc3	mechanismus
jejich	jejich	k3xOp3gNnSc3	jejich
odstraňování	odstraňování	k1gNnSc3	odstraňování
a	a	k8xC	a
pronikají	pronikat	k5eAaImIp3nP	pronikat
až	až	k9	až
do	do	k7c2	do
alveolárních	alveolární	k2eAgInPc2d1	alveolární
váčků	váček	k1gInPc2	váček
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
fagocytoze	fagocytoze	k6eAd1	fagocytoze
makrofágy	makrofág	k1gInPc4	makrofág
přenášeny	přenášen	k2eAgInPc4d1	přenášen
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
lymfatických	lymfatický	k2eAgFnPc2d1	lymfatická
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Absorpce	absorpce	k1gFnSc2	absorpce
gastrointestinálním	gastrointestinální	k2eAgInSc7d1	gastrointestinální
traktem	trakt	k1gInSc7	trakt
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
při	při	k7c6	při
ingesci	ingesce	k1gFnSc6	ingesce
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
absorpci	absorpce	k1gFnSc4	absorpce
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
především	především	k9	především
její	její	k3xOp3gFnPc1	její
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
případný	případný	k2eAgInSc1d1	případný
způsob	způsob	k1gInSc1	způsob
její	její	k3xOp3gFnSc2	její
metabolizace	metabolizace	k1gFnSc2	metabolizace
a	a	k8xC	a
funkční	funkční	k2eAgInSc1d1	funkční
stav	stav	k1gInSc1	stav
gastrointestinálního	gastrointestinální	k2eAgInSc2d1	gastrointestinální
traktu	trakt	k1gInSc2	trakt
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
chemických	chemický	k2eAgFnPc2d1	chemická
a	a	k8xC	a
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
závisí	záviset	k5eAaImIp3nS	záviset
rychlost	rychlost	k1gFnSc1	rychlost
pronikání	pronikání	k1gNnSc4	pronikání
kontaminantu	kontaminant	k1gInSc2	kontaminant
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
povrchové	povrchový	k2eAgFnSc6d1	povrchová
struktuře	struktura	k1gFnSc6	struktura
lidské	lidský	k2eAgFnSc2d1	lidská
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
látek	látka	k1gFnPc2	látka
představuje	představovat	k5eAaImIp3nS	představovat
neporušená	porušený	k2eNgFnSc1d1	neporušená
kůže	kůže	k1gFnSc1	kůže
neprostupnou	prostupný	k2eNgFnSc4d1	neprostupná
bariéru	bariéra	k1gFnSc4	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
proniknout	proniknout	k5eAaPmF	proniknout
hlavně	hlavně	k9	hlavně
organická	organický	k2eAgNnPc4d1	organické
rozpouštědla	rozpouštědlo	k1gNnPc4	rozpouštědlo
<g/>
,	,	kIx,	,
chlorované	chlorovaný	k2eAgInPc4d1	chlorovaný
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
<g/>
,	,	kIx,	,
insekticidy	insekticid	k1gInPc4	insekticid
a	a	k8xC	a
nervově	nervově	k6eAd1	nervově
paralytické	paralytický	k2eAgFnPc4d1	paralytická
organofosfáty	organofosfáta	k1gFnPc4	organofosfáta
<g/>
,	,	kIx,	,
z	z	k7c2	z
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
pak	pak	k9	pak
tritium	tritium	k1gNnSc1	tritium
<g/>
.	.	kIx.	.
</s>
<s>
Mazové	mazový	k2eAgFnPc1d1	mazová
a	a	k8xC	a
potní	potní	k2eAgFnPc1d1	potní
žlázy	žláza	k1gFnPc1	žláza
mohou	moct	k5eAaImIp3nP	moct
urychlit	urychlit	k5eAaPmF	urychlit
průnik	průnik	k1gInSc4	průnik
některých	některý	k3yIgFnPc2	některý
lipofilních	lipofilní	k2eAgFnPc2d1	lipofilní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
otevřených	otevřený	k2eAgFnPc2d1	otevřená
ran	rána	k1gFnPc2	rána
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
porušení	porušení	k1gNnSc2	porušení
kůže	kůže	k1gFnSc2	kůže
k	k	k7c3	k
akceleraci	akcelerace	k1gFnSc3	akcelerace
průniku	průnik	k1gInSc2	průnik
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
biologické	biologický	k2eAgFnSc2d1	biologická
kontaminace	kontaminace	k1gFnSc2	kontaminace
různými	různý	k2eAgInPc7d1	různý
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
průniku	průnik	k1gInSc2	průnik
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
také	také	k6eAd1	také
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
těchto	tento	k3xDgInPc2	tento
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
úniku	únik	k1gInSc2	únik
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
látky	látka	k1gFnSc2	látka
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
nechráněných	chráněný	k2eNgFnPc2d1	nechráněná
osob	osoba	k1gFnPc2	osoba
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
povrchové	povrchový	k2eAgFnSc3d1	povrchová
kontaminaci	kontaminace	k1gFnSc3	kontaminace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
případné	případný	k2eAgFnSc3d1	případná
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
kontaminaci	kontaminace	k1gFnSc3	kontaminace
dochází	docházet	k5eAaImIp3nS	docházet
nejčastěji	často	k6eAd3	často
absorpcí	absorpce	k1gFnPc2	absorpce
plícemi	plíce	k1gFnPc7	plíce
nebo	nebo	k8xC	nebo
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
kontaminace	kontaminace	k1gFnSc1	kontaminace
vnější	vnější	k2eAgFnSc1d1	vnější
přejít	přejít	k5eAaPmF	přejít
v	v	k7c4	v
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
vážné	vážný	k2eAgFnSc2d1	vážná
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
a	a	k8xC	a
která	který	k3yIgFnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
speciální	speciální	k2eAgFnSc4d1	speciální
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
procesu	proces	k1gInSc3	proces
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
včasnou	včasný	k2eAgFnSc7d1	včasná
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
dekontaminací	dekontaminace	k1gFnSc7	dekontaminace
zabránit	zabránit	k5eAaPmF	zabránit
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
kapitoly	kapitola	k1gFnSc2	kapitola
výše	výše	k1gFnSc1	výše
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
,	,	kIx,	,
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absorpci	absorpce	k1gFnSc6	absorpce
následuje	následovat	k5eAaImIp3nS	následovat
distribuce	distribuce	k1gFnSc1	distribuce
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc4	rozdělení
látky	látka	k1gFnSc2	látka
mezi	mezi	k7c7	mezi
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
tkáněmi	tkáň	k1gFnPc7	tkáň
a	a	k8xC	a
orgány	orgán	k1gInPc7	orgán
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
faktor	faktor	k1gInSc1	faktor
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
distribuci	distribuce	k1gFnSc4	distribuce
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
přechodu	přechod	k1gInSc2	přechod
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
mezi	mezi	k7c7	mezi
kapilárním	kapilární	k2eAgNnSc7d1	kapilární
řečištěm	řečiště	k1gNnSc7	řečiště
a	a	k8xC	a
tkáňovými	tkáňový	k2eAgFnPc7d1	tkáňová
tekutinami	tekutina	k1gFnPc7	tekutina
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
tkáňovými	tkáňový	k2eAgFnPc7d1	tkáňová
tekutinami	tekutina	k1gFnPc7	tekutina
a	a	k8xC	a
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Distribuce	distribuce	k1gFnSc1	distribuce
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
nikdy	nikdy	k6eAd1	nikdy
není	být	k5eNaImIp3nS	být
rovnoměrná	rovnoměrný	k2eAgFnSc1d1	rovnoměrná
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
bude	být	k5eAaImBp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
orgánu	orgán	k1gInSc6	orgán
nebo	nebo	k8xC	nebo
tkáni	tkáň	k1gFnSc6	tkáň
vyšší	vysoký	k2eAgFnSc6d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
fyzikálně-chemických	fyzikálněhemický	k2eAgFnPc6d1	fyzikálně-chemická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
průniku	průnik	k1gInSc2	průnik
látky	látka	k1gFnSc2	látka
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
(	(	kIx(	(
<g/>
jednorázově	jednorázově	k6eAd1	jednorázově
x	x	k?	x
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgInPc6d1	další
faktorech	faktor	k1gInPc6	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Distribuce	distribuce	k1gFnSc1	distribuce
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
je	být	k5eAaImIp3nS	být
děj	děj	k1gInSc1	děj
dynamický	dynamický	k2eAgInSc1d1	dynamický
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výsledek	výsledek	k1gInSc1	výsledek
aktuální	aktuální	k2eAgFnSc2d1	aktuální
absorpce	absorpce	k1gFnSc2	absorpce
<g/>
,	,	kIx,	,
exkrece	exkrece	k1gFnSc2	exkrece
a	a	k8xC	a
metabolizace	metabolizace	k1gFnSc2	metabolizace
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
distribuci	distribuce	k1gFnSc4	distribuce
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc1	velikost
rozdělovacího	rozdělovací	k2eAgInSc2d1	rozdělovací
koeficientu	koeficient	k1gInSc2	koeficient
(	(	kIx(	(
<g/>
veličina	veličina	k1gFnSc1	veličina
udávající	udávající	k2eAgFnSc1d1	udávající
poměr	poměr	k1gInSc1	poměr
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
se	se	k3xPyFc4	se
látka	látka	k1gFnSc1	látka
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
mezi	mezi	k7c4	mezi
vodnou	vodný	k2eAgFnSc4d1	vodná
a	a	k8xC	a
organickou	organický	k2eAgFnSc4d1	organická
fázi	fáze	k1gFnSc4	fáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
veličina	veličina	k1gFnSc1	veličina
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
rozhodujících	rozhodující	k2eAgInPc2d1	rozhodující
faktorů	faktor	k1gInPc2	faktor
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
jakých	jaký	k3yIgInPc6	jaký
orgánech	orgán	k1gInPc6	orgán
se	se	k3xPyFc4	se
chemická	chemický	k2eAgFnSc1d1	chemická
látka	látka	k1gFnSc1	látka
bude	být	k5eAaImBp3nS	být
hromadit	hromadit	k5eAaImF	hromadit
(	(	kIx(	(
<g/>
kumulovat	kumulovat	k5eAaImF	kumulovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
DDT	DDT	kA	DDT
se	se	k3xPyFc4	se
kumuluje	kumulovat	k5eAaImIp3nS	kumulovat
v	v	k7c6	v
tukové	tukový	k2eAgFnSc6d1	tuková
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
místem	místo	k1gNnSc7	místo
jeho	on	k3xPp3gInSc2	on
toxického	toxický	k2eAgInSc2d1	toxický
účinku	účinek	k1gInSc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
tak	tak	k6eAd1	tak
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
významnému	významný	k2eAgNnSc3d1	významné
snížení	snížení	k1gNnSc3	snížení
jeho	jeho	k3xOp3gInSc2	jeho
toxického	toxický	k2eAgInSc2d1	toxický
účinku	účinek	k1gInSc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
ale	ale	k9	ale
poté	poté	k6eAd1	poté
může	moct	k5eAaImIp3nS	moct
pozvolna	pozvolna	k6eAd1	pozvolna
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
z	z	k7c2	z
tukové	tukový	k2eAgFnSc2d1	tuková
tkáně	tkáň	k1gFnSc2	tkáň
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
a	a	k8xC	a
způsobovat	způsobovat	k5eAaImF	způsobovat
tak	tak	k6eAd1	tak
chronickou	chronický	k2eAgFnSc4d1	chronická
toxicitu	toxicita	k1gFnSc4	toxicita
dlouho	dlouho	k6eAd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgInS	být
organismus	organismus	k1gInSc1	organismus
vystaven	vystavit	k5eAaPmNgInS	vystavit
akutnímu	akutní	k2eAgNnSc3d1	akutní
působení	působení	k1gNnSc3	působení
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
reversibilní	reversibilní	k2eAgFnPc4d1	reversibilní
vazby	vazba	k1gFnPc4	vazba
na	na	k7c4	na
biomakromolekuly	biomakromolekula	k1gFnPc4	biomakromolekula
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c4	na
plazmatické	plazmatický	k2eAgFnPc4d1	plazmatická
a	a	k8xC	a
tkáňové	tkáňový	k2eAgFnPc4d1	tkáňová
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
opět	opět	k6eAd1	opět
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
depa	depo	k1gNnPc4	depo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
přenašeče	přenašeč	k1gInSc2	přenašeč
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
výrazně	výrazně	k6eAd1	výrazně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
transport	transport	k1gInSc4	transport
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
distribuci	distribuce	k1gFnSc4	distribuce
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
bariér	bariéra	k1gFnPc2	bariéra
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
(	(	kIx(	(
<g/>
hematoencefalická	hematoencefalický	k2eAgFnSc1d1	hematoencefalická
<g/>
,	,	kIx,	,
placentární	placentární	k2eAgFnSc1d1	placentární
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
látky	látka	k1gFnPc1	látka
mohou	moct	k5eAaImIp3nP	moct
těmito	tento	k3xDgFnPc7	tento
bariérami	bariéra	k1gFnPc7	bariéra
snadno	snadno	k6eAd1	snadno
procházet	procházet	k5eAaImF	procházet
zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
jiné	jiný	k2eAgNnSc4d1	jiné
tvoří	tvořit	k5eAaImIp3nS	tvořit
nepřekonatelnou	překonatelný	k2eNgFnSc4d1	nepřekonatelná
bariéru	bariéra	k1gFnSc4	bariéra
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Exkrece	exkrece	k1gFnSc1	exkrece
znamená	znamenat	k5eAaImIp3nS	znamenat
vyloučení	vyloučení	k1gNnSc4	vyloučení
dané	daný	k2eAgFnSc2d1	daná
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
z	z	k7c2	z
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
detoxikace	detoxikace	k1gFnSc2	detoxikace
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
především	především	k9	především
exkrece	exkrece	k1gFnSc1	exkrece
plícemi	plíce	k1gFnPc7	plíce
<g/>
,	,	kIx,	,
ledvinami	ledvina	k1gFnPc7	ledvina
a	a	k8xC	a
stolicí	stolice	k1gFnSc7	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Plícemi	plíce	k1gFnPc7	plíce
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
exkreci	exkrece	k1gFnSc3	exkrece
zejména	zejména	k9	zejména
plynných	plynný	k2eAgFnPc2d1	plynná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
látek	látka	k1gFnPc2	látka
vysoce	vysoce	k6eAd1	vysoce
těkavých	těkavý	k2eAgInPc2d1	těkavý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
většina	většina	k1gFnSc1	většina
organických	organický	k2eAgNnPc2d1	organické
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
exkreci	exkrece	k1gFnSc6	exkrece
ledvinami	ledvina	k1gFnPc7	ledvina
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
jednak	jednak	k8xC	jednak
glomerulární	glomerulární	k2eAgFnSc1d1	glomerulární
filtrace	filtrace	k1gFnSc1	filtrace
(	(	kIx(	(
<g/>
látky	látka	k1gFnPc1	látka
do	do	k7c2	do
hmotnosti	hmotnost	k1gFnSc2	hmotnost
60	[number]	k4	60
kiloDaltonů	kiloDalton	k1gInPc2	kiloDalton
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
specializované	specializovaný	k2eAgInPc4d1	specializovaný
transportní	transportní	k2eAgInPc4d1	transportní
systémy	systém	k1gInPc4	systém
proximálních	proximální	k2eAgInPc2d1	proximální
tubulů	tubulus	k1gInPc2	tubulus
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
stolice	stolice	k1gFnSc2	stolice
se	se	k3xPyFc4	se
látky	látka	k1gFnPc1	látka
mohou	moct	k5eAaImIp3nP	moct
vylučovat	vylučovat	k5eAaImF	vylučovat
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
vazba	vazba	k1gFnSc1	vazba
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
na	na	k7c4	na
nestrávené	strávený	k2eNgFnPc4d1	nestrávená
složky	složka	k1gFnPc4	složka
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
druhým	druhý	k4xOgNnSc7	druhý
je	on	k3xPp3gFnPc4	on
exkrece	exkrece	k1gFnPc1	exkrece
žlučí	žlučit	k5eAaImIp3nP	žlučit
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
metabolizovaná	metabolizovaný	k2eAgFnSc1d1	metabolizovaná
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gInPc1	jejich
metabolity	metabolit	k1gInPc1	metabolit
vylučovány	vylučován	k2eAgInPc1d1	vylučován
do	do	k7c2	do
žluče	žluč	k1gFnSc2	žluč
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
pak	pak	k9	pak
do	do	k7c2	do
stolice	stolice	k1gFnSc2	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Okrajovými	okrajový	k2eAgInPc7d1	okrajový
způsoby	způsob	k1gInPc7	způsob
exkrece	exkrece	k1gFnSc2	exkrece
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
například	například	k6eAd1	například
exkrece	exkrece	k1gFnSc1	exkrece
potem	pot	k1gInSc7	pot
<g/>
,	,	kIx,	,
slinami	slina	k1gFnPc7	slina
<g/>
,	,	kIx,	,
slzami	slza	k1gFnPc7	slza
a	a	k8xC	a
mateřským	mateřský	k2eAgNnSc7d1	mateřské
mlékem	mléko	k1gNnSc7	mléko
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biotransformace	biotransformace	k1gFnSc1	biotransformace
znamená	znamenat	k5eAaImIp3nS	znamenat
chemickou	chemický	k2eAgFnSc4d1	chemická
přeměnu	přeměna	k1gFnSc4	přeměna
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
chemickou	chemický	k2eAgFnSc4d1	chemická
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
biotransformačním	biotransformační	k2eAgInSc7d1	biotransformační
orgánem	orgán	k1gInSc7	orgán
jsou	být	k5eAaImIp3nP	být
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
,	,	kIx,	,
plíce	plíce	k1gFnPc4	plíce
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
parenchymatosní	parenchymatosní	k2eAgInPc4d1	parenchymatosní
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
játrech	játra	k1gNnPc6	játra
se	se	k3xPyFc4	se
na	na	k7c6	na
biotransformaci	biotransformace	k1gFnSc6	biotransformace
podílejí	podílet	k5eAaImIp3nP	podílet
všechny	všechen	k3xTgFnPc4	všechen
jaterní	jaterní	k2eAgFnPc4d1	jaterní
buňku	buňka	k1gFnSc4	buňka
–	–	k?	–
hepatocyty	hepatocyt	k1gInPc4	hepatocyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgInPc6d1	zmíněný
orgánech	orgán	k1gInPc6	orgán
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
biotransformace	biotransformace	k1gFnSc2	biotransformace
jen	jen	k9	jen
určité	určitý	k2eAgFnPc1d1	určitá
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Biotransformace	biotransformace	k1gFnSc1	biotransformace
obvykle	obvykle	k6eAd1	obvykle
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
fázích	fáze	k1gFnPc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
i	i	k9	i
látky	látka	k1gFnPc1	látka
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
fáze	fáze	k1gFnSc1	fáze
biotransformace	biotransformace	k1gFnSc2	biotransformace
a	a	k8xC	a
i	i	k9	i
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
biotransformaci	biotransformace	k1gFnSc4	biotransformace
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
(	(	kIx(	(
<g/>
takové	takový	k3xDgFnPc4	takový
látky	látka	k1gFnPc4	látka
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
vyloučeny	vyloučit	k5eAaPmNgFnP	vyloučit
v	v	k7c6	v
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
podobě	podoba	k1gFnSc6	podoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
řídící	řídící	k2eAgFnSc4d1	řídící
biotransformaci	biotransformace	k1gFnSc4	biotransformace
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
mikrosomech	mikrosom	k1gInPc6	mikrosom
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc2	první
fáze	fáze	k1gFnSc2	fáze
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
<g/>
)	)	kIx)	)
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
biotransformace	biotransformace	k1gFnSc2	biotransformace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
redukce	redukce	k1gFnSc1	redukce
<g/>
,	,	kIx,	,
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
nebo	nebo	k8xC	nebo
oxidace	oxidace	k1gFnSc1	oxidace
dané	daný	k2eAgFnSc2d1	daná
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
oxidace	oxidace	k1gFnSc1	oxidace
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgInSc4d3	nejčastější
případ	případ	k1gInSc4	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
reakce	reakce	k1gFnSc2	reakce
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
několik	několik	k4yIc1	několik
enzymových	enzymový	k2eAgInPc2d1	enzymový
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
komplex	komplex	k1gInSc1	komplex
cytochromu	cytochrom	k1gInSc2	cytochrom
P-450	P-450	k1gFnPc2	P-450
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
hydroxylaci	hydroxylace	k1gFnSc4	hydroxylace
alifatických	alifatický	k2eAgFnPc2d1	alifatický
i	i	k8xC	i
aromatických	aromatický	k2eAgFnPc2d1	aromatická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
za	za	k7c4	za
deaminaci	deaminace	k1gFnSc4	deaminace
<g/>
,	,	kIx,	,
N-hydroxylaci	Nydroxylace	k1gFnSc4	N-hydroxylace
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
procesy	proces	k1gInPc1	proces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
enzymových	enzymový	k2eAgInPc2d1	enzymový
systému	systém	k1gInSc6	systém
je	on	k3xPp3gInPc4	on
možno	možno	k6eAd1	možno
jmenovat	jmenovat	k5eAaBmF	jmenovat
například	například	k6eAd1	například
flavinové	flavinový	k2eAgFnPc4d1	flavinový
aminoxidázy	aminoxidáza	k1gFnPc4	aminoxidáza
<g/>
,	,	kIx,	,
peroxidázy	peroxidáza	k1gFnPc4	peroxidáza
<g/>
,	,	kIx,	,
nespecifické	specifický	k2eNgFnPc4d1	nespecifická
hydrolázy	hydroláza	k1gFnPc4	hydroláza
(	(	kIx(	(
<g/>
názvy	název	k1gInPc7	název
těchto	tento	k3xDgFnPc2	tento
skupin	skupina	k1gFnPc2	skupina
jsou	být	k5eAaImIp3nP	být
odvozeny	odvozen	k2eAgFnPc1d1	odvozena
od	od	k7c2	od
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
metabolizují	metabolizovat	k5eAaImIp3nP	metabolizovat
dané	daný	k2eAgFnPc1d1	daná
látky	látka	k1gFnPc1	látka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
biotransformace	biotransformace	k1gFnSc2	biotransformace
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejím	její	k3xOp3gInSc7	její
výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
méně	málo	k6eAd2	málo
toxické	toxický	k2eAgFnPc1d1	toxická
a	a	k8xC	a
hydrofilní	hydrofilní	k2eAgFnPc1d1	hydrofilní
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
snadno	snadno	k6eAd1	snadno
vyloučitelné	vyloučitelný	k2eAgInPc1d1	vyloučitelný
z	z	k7c2	z
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
i	i	k9	i
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
biotransformace	biotransformace	k1gFnSc2	biotransformace
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
látka	látka	k1gFnSc1	látka
více	hodně	k6eAd2	hodně
toxická	toxický	k2eAgFnSc1d1	toxická
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
látka	látka	k1gFnSc1	látka
původní	původní	k2eAgFnSc1d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
letální	letální	k2eAgFnSc6d1	letální
syntéze	syntéza	k1gFnSc6	syntéza
nebo	nebo	k8xC	nebo
bioaktivaci	bioaktivace	k1gFnSc6	bioaktivace
<g/>
.	.	kIx.	.
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
biotransformace	biotransformace	k1gFnSc2	biotransformace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
syntéza	syntéza	k1gFnSc1	syntéza
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
látka	látka	k1gFnSc1	látka
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
její	její	k3xOp3gInSc1	její
metabolit	metabolit	k1gInSc1	metabolit
konjugován	konjugovat	k5eAaImNgInS	konjugovat
s	s	k7c7	s
látkami	látka	k1gFnPc7	látka
lidskému	lidský	k2eAgNnSc3d1	lidské
organismu	organismus	k1gInSc6	organismus
vlastními	vlastní	k2eAgFnPc7d1	vlastní
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
kyselina	kyselina	k1gFnSc1	kyselina
glukaronová	glukaronový	k2eAgFnSc1d1	glukaronový
<g/>
,	,	kIx,	,
sírová	sírový	k2eAgFnSc1d1	sírová
<g/>
,	,	kIx,	,
tripeptid	tripeptid	k1gInSc1	tripeptid
gluthathion	gluthathion	k1gInSc1	gluthathion
<g/>
)	)	kIx)	)
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
nových	nový	k2eAgFnPc2d1	nová
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
poté	poté	k6eAd1	poté
vyloučeny	vyloučit	k5eAaPmNgFnP	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
častěji	často	k6eAd2	často
probíhá	probíhat	k5eAaImIp3nS	probíhat
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
biotransformace	biotransformace	k1gFnSc2	biotransformace
jako	jako	k8xC	jako
acetylace	acetylace	k1gFnSc2	acetylace
nebo	nebo	k8xC	nebo
methylace	methylace	k1gFnSc2	methylace
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zmínit	zmínit	k5eAaPmF	zmínit
vliv	vliv	k1gInSc4	vliv
antidot	antidota	k1gFnPc2	antidota
na	na	k7c4	na
detoxikaci	detoxikace	k1gFnSc4	detoxikace
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
(	(	kIx(	(
<g/>
antidotní	antidotní	k2eAgFnPc1d1	antidotní
terapie	terapie	k1gFnPc1	terapie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antidotum	Antidotum	k1gNnSc1	Antidotum
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
účinek	účinek	k1gInSc4	účinek
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
eliminovat	eliminovat	k5eAaBmF	eliminovat
negativní	negativní	k2eAgInPc4d1	negativní
účinky	účinek	k1gInPc4	účinek
toxické	toxický	k2eAgFnSc2d1	toxická
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Antidotní	Antidotní	k2eAgFnSc1d1	Antidotní
terapie	terapie	k1gFnSc1	terapie
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgNnSc1d1	aktivní
opatření	opatření	k1gNnSc1	opatření
detoxikace	detoxikace	k1gFnSc2	detoxikace
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Antidota	Antidota	k1gFnSc1	Antidota
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
specifická	specifický	k2eAgFnSc1d1	specifická
(	(	kIx(	(
<g/>
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
přesně	přesně	k6eAd1	přesně
vymezenou	vymezený	k2eAgFnSc4d1	vymezená
skupinu	skupina	k1gFnSc4	skupina
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
látku	látka	k1gFnSc4	látka
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
účinkem	účinek	k1gInSc7	účinek
-	-	kIx~	-
např.	např.	kA	např.
<g/>
:	:	kIx,	:
chelatační	chelatační	k2eAgNnPc1d1	chelatační
činidla	činidlo	k1gNnPc1	činidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
schopná	schopný	k2eAgNnPc1d1	schopné
vytvořit	vytvořit	k5eAaPmF	vytvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
těžkými	těžký	k2eAgInPc7d1	těžký
kovy	kov	k1gInPc7	kov
a	a	k8xC	a
zabránit	zabránit	k5eAaPmF	zabránit
tak	tak	k6eAd1	tak
jejich	jejich	k3xOp3gNnSc3	jejich
dalšímu	další	k2eAgNnSc3d1	další
vstřebání	vstřebání	k1gNnSc3	vstřebání
organismem	organismus	k1gInSc7	organismus
-	-	kIx~	-
proti	proti	k7c3	proti
<g />
.	.	kIx.	.
</s>
<s>
jiným	jiný	k2eAgFnPc3d1	jiná
látkám	látka	k1gFnPc3	látka
jsou	být	k5eAaImIp3nP	být
chelatační	chelatační	k2eAgNnPc4d1	chelatační
činidla	činidlo	k1gNnPc4	činidlo
naprosto	naprosto	k6eAd1	naprosto
neúčinná	účinný	k2eNgFnSc1d1	neúčinná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nespecifická	specifický	k2eNgFnSc1d1	nespecifická
(	(	kIx(	(
<g/>
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
širokou	široký	k2eAgFnSc4d1	široká
skupinu	skupina	k1gFnSc4	skupina
látek	látka	k1gFnPc2	látka
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
účinkem	účinek	k1gInSc7	účinek
-	-	kIx~	-
např.	např.	kA	např.
<g/>
:	:	kIx,	:
medicinální	medicinální	k2eAgNnSc4d1	medicinální
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
účinek	účinek	k1gInSc1	účinek
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
povrchu	povrch	k1gInSc6	povrch
medicinálního	medicinální	k2eAgNnSc2d1	medicinální
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
schopné	schopný	k2eAgFnPc1d1	schopná
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
povrch	povrch	k1gInSc4	povrch
<g />
.	.	kIx.	.
</s>
<s>
zachytit	zachytit	k5eAaPmF	zachytit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
téměř	téměř	k6eAd1	téměř
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
zabránit	zabránit	k5eAaPmF	zabránit
tak	tak	k6eAd1	tak
jejímu	její	k3xOp3gNnSc3	její
dalšímu	další	k2eAgNnSc3d1	další
vstřebání	vstřebání	k1gNnSc3	vstřebání
organismem	organismus	k1gInSc7	organismus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
slovo	slovo	k1gNnSc1	slovo
detoxikace	detoxikace	k1gFnSc2	detoxikace
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
spojení	spojení	k1gNnSc1	spojení
např.	např.	kA	např.
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
detoxu	detox	k1gInSc6	detox
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
procesem	proces	k1gInSc7	proces
zbavení	zbavení	k1gNnSc2	zbavení
se	se	k3xPyFc4	se
závislosti	závislost	k1gFnSc2	závislost
(	(	kIx(	(
<g/>
psychologická	psychologický	k2eAgFnSc1d1	psychologická
nebo	nebo	k8xC	nebo
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
<g/>
)	)	kIx)	)
na	na	k7c6	na
nežádoucích	žádoucí	k2eNgFnPc6d1	nežádoucí
chemických	chemický	k2eAgFnPc6d1	chemická
látkách	látka	k1gFnPc6	látka
(	(	kIx(	(
<g/>
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
nikotin	nikotin	k1gInSc1	nikotin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
detoxikace	detoxikace	k1gFnSc2	detoxikace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
použito	použít	k5eAaPmNgNnS	použít
oprávněně	oprávněně	k6eAd1	oprávněně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
cílem	cíl	k1gInSc7	cíl
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
eliminace	eliminace	k1gFnSc1	eliminace
dané	daný	k2eAgFnSc2d1	daná
látky	látka	k1gFnSc2	látka
z	z	k7c2	z
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
psychologická	psychologický	k2eAgFnSc1d1	psychologická
nebo	nebo	k8xC	nebo
farmakologická	farmakologický	k2eAgFnSc1d1	farmakologická
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
obě	dva	k4xCgFnPc1	dva
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Psychologická	psychologický	k2eAgFnSc1d1	psychologická
část	část	k1gFnSc1	část
léčby	léčba	k1gFnSc2	léčba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
cílené	cílený	k2eAgFnSc3d1	cílená
snaze	snaha	k1gFnSc3	snaha
naučit	naučit	k5eAaPmF	naučit
pacienta	pacient	k1gMnSc4	pacient
zvládat	zvládat	k5eAaImF	zvládat
zátěžové	zátěžový	k2eAgFnSc2d1	zátěžová
situace	situace	k1gFnSc2	situace
bez	bez	k7c2	bez
návykové	návykový	k2eAgFnSc2d1	návyková
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
"	"	kIx"	"
<g/>
zlehčení	zlehčení	k1gNnSc2	zlehčení
<g/>
"	"	kIx"	"
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
drogou	droga	k1gFnSc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Farmakologická	farmakologický	k2eAgFnSc1d1	farmakologická
část	část	k1gFnSc1	část
spočívá	spočívat	k5eAaImIp3nS	spočívat
buď	buď	k8xC	buď
v	v	k7c6	v
postupném	postupný	k2eAgNnSc6d1	postupné
snižování	snižování	k1gNnSc6	snižování
dávkování	dávkování	k1gNnSc2	dávkování
dané	daný	k2eAgFnPc1d1	daná
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc1	omezení
negativních	negativní	k2eAgInPc2d1	negativní
abstinenčních	abstinenční	k2eAgInPc2d1	abstinenční
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
nutí	nutit	k5eAaImIp3nS	nutit
pacienta	pacient	k1gMnSc4	pacient
znovu	znovu	k6eAd1	znovu
návykovou	návykový	k2eAgFnSc4d1	návyková
látku	látka	k1gFnSc4	látka
užít	užít	k5eAaPmF	užít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
nahrazení	nahrazení	k1gNnSc1	nahrazení
jinou	jiný	k2eAgFnSc7d1	jiná
látkou	látka	k1gFnSc7	látka
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
méně	málo	k6eAd2	málo
škodlivou	škodlivý	k2eAgFnSc7d1	škodlivá
nebo	nebo	k8xC	nebo
se	s	k7c7	s
slabšími	slabý	k2eAgInPc7d2	slabší
abstinenčními	abstinenční	k2eAgInPc7d1	abstinenční
příznaky	příznak	k1gInPc7	příznak
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podáním	podání	k1gNnSc7	podání
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
omezí	omezit	k5eAaPmIp3nS	omezit
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
účinek	účinek	k1gInSc4	účinek
návykové	návykový	k2eAgFnSc2d1	návyková
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
antabus	antabus	k1gInSc1	antabus
-	-	kIx~	-
látka	látka	k1gFnSc1	látka
blokující	blokující	k2eAgFnSc4d1	blokující
alkoholdehydrogenázu	alkoholdehydrogenáza	k1gFnSc4	alkoholdehydrogenáza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemůže	moct	k5eNaImIp3nS	moct
rozkládat	rozkládat	k5eAaImF	rozkládat
ethanol	ethanol	k1gInSc4	ethanol
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
již	již	k6eAd1	již
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
ethanolu	ethanol	k1gInSc2	ethanol
přivodí	přivodit	k5eAaBmIp3nS	přivodit
závislému	závislý	k2eAgInSc3d1	závislý
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc4d1	silná
opilost	opilost	k1gFnSc4	opilost
-	-	kIx~	-
nedostaví	dostavit	k5eNaPmIp3nS	dostavit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
fáze	fáze	k1gFnSc1	fáze
alkoholového	alkoholový	k2eAgNnSc2d1	alkoholové
"	"	kIx"	"
<g/>
radostného	radostný	k2eAgNnSc2d1	radostné
povznesení	povznesení	k1gNnSc2	povznesení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
negativní	negativní	k2eAgInPc1d1	negativní
příznaky	příznak	k1gInPc1	příznak
silné	silný	k2eAgFnSc2d1	silná
opilosti	opilost	k1gFnSc2	opilost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
slovo	slovo	k1gNnSc1	slovo
detoxikace	detoxikace	k1gFnSc2	detoxikace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
tobě	ty	k3xPp2nSc3	ty
také	také	k9	také
často	často	k6eAd1	často
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
zdravého	zdravý	k2eAgInSc2d1	zdravý
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
většinou	většina	k1gFnSc7	většina
o	o	k7c4	o
"	"	kIx"	"
<g/>
detoxikační	detoxikační	k2eAgFnPc4d1	detoxikační
diety	dieta	k1gFnPc4	dieta
<g/>
"	"	kIx"	"
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
konzumaci	konzumace	k1gFnSc6	konzumace
nějaké	nějaký	k3yIgFnSc2	nějaký
poživatiny	poživatina	k1gFnSc2	poživatina
(	(	kIx(	(
<g/>
čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
vybraná	vybraný	k2eAgFnSc1d1	vybraná
zelenina	zelenina	k1gFnSc1	zelenina
či	či	k8xC	či
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
)	)	kIx)	)
často	často	k6eAd1	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
nějakým	nějaký	k3yIgInSc7	nějaký
"	"	kIx"	"
<g/>
produktem	produkt	k1gInSc7	produkt
zdravé	zdravý	k2eAgFnSc2d1	zdravá
výživy	výživa	k1gFnSc2	výživa
<g/>
"	"	kIx"	"
či	či	k8xC	či
homeopatikem	homeopatikum	k1gNnSc7	homeopatikum
a	a	k8xC	a
současném	současný	k2eAgNnSc6d1	současné
omezení	omezení	k1gNnSc6	omezení
běžně	běžně	k6eAd1	běžně
konzumovaných	konzumovaný	k2eAgNnPc2d1	konzumované
jídel	jídlo	k1gNnPc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
činnost	činnost	k1gFnSc1	činnost
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
detoxikaci	detoxikace	k1gFnSc4	detoxikace
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
posoudit	posoudit	k5eAaPmF	posoudit
z	z	k7c2	z
několika	několik	k4yIc2	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
produkty	produkt	k1gInPc1	produkt
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
detoxikovat	detoxikovat	k5eAaPmF	detoxikovat
běžný	běžný	k2eAgInSc4d1	běžný
stav	stav	k1gInSc4	stav
lidského	lidský	k2eAgInSc2d1	lidský
organismu	organismus	k1gInSc2	organismus
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
běžně	běžně	k6eAd1	běžně
máme	mít	k5eAaImIp1nP	mít
-	-	kIx~	-
tedy	tedy	k8xC	tedy
přirozeně	přirozeně	k6eAd1	přirozeně
i	i	k9	i
škodlivé	škodlivý	k2eAgFnPc4d1	škodlivá
látky	látka	k1gFnPc4	látka
např.	např.	kA	např.
z	z	k7c2	z
ovzduší	ovzduší	k1gNnSc2	ovzduší
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
lze	lze	k6eAd1	lze
ale	ale	k8xC	ale
těžko	těžko	k6eAd1	těžko
definovat	definovat	k5eAaBmF	definovat
pojmem	pojem	k1gInSc7	pojem
akutní	akutní	k2eAgFnSc2d1	akutní
<g/>
,	,	kIx,	,
subchronické	subchronický	k2eAgFnSc2d1	Subchronická
nebo	nebo	k8xC	nebo
chronické	chronický	k2eAgFnSc2d1	chronická
otravy	otrava	k1gFnSc2	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
škodlivé	škodlivý	k2eAgFnPc1d1	škodlivá
látky	látka	k1gFnPc1	látka
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
nezpochybnitelně	zpochybnitelně	k6eNd1	zpochybnitelně
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tak	tak	k6eAd1	tak
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
jejich	jejich	k3xOp3gInSc4	jejich
stav	stav	k1gInSc4	stav
před	před	k7c7	před
proděláním	prodělání	k1gNnSc7	prodělání
těchto	tento	k3xDgInPc2	tento
detoxikačních	detoxikační	k2eAgInPc2d1	detoxikační
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
(	(	kIx(	(
<g/>
koncentrace	koncentrace	k1gFnPc1	koncentrace
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
nízké	nízký	k2eAgInPc1d1	nízký
až	až	k9	až
spolehlivě	spolehlivě	k6eAd1	spolehlivě
neměřitelné	měřitelný	k2eNgNnSc1d1	neměřitelné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
