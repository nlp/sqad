<p>
<s>
Nula	nula	k1gFnSc1	nula
(	(	kIx(	(
<g/>
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
nullus	nullus	k1gMnSc1	nullus
–	–	k?	–
žádný	žádný	k1gMnSc1	žádný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
0	[number]	k4	0
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejzákladnějších	základní	k2eAgFnPc2d3	nejzákladnější
matematických	matematický	k2eAgFnPc2d1	matematická
konstant	konstanta	k1gFnPc2	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
číslo	číslo	k1gNnSc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Nula	nula	k1gFnSc1	nula
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
funkcích	funkce	k1gFnPc6	funkce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
nula	nula	k1gFnSc1	nula
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
matematického	matematický	k2eAgNnSc2d1	matematické
prázdna	prázdno	k1gNnSc2	prázdno
<g/>
,	,	kIx,	,
prostého	prostý	k2eAgNnSc2d1	prosté
nic	nic	k6eAd1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
0	[number]	k4	0
na	na	k7c6	na
číselné	číselný	k2eAgFnSc6d1	číselná
ose	osa	k1gFnSc6	osa
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
záporná	záporný	k2eAgNnPc4d1	záporné
čísla	číslo	k1gNnPc4	číslo
od	od	k7c2	od
kladných	kladný	k2eAgNnPc2d1	kladné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
nula	nula	k1gFnSc1	nula
velikost	velikost	k1gFnSc1	velikost
(	(	kIx(	(
<g/>
kardinalita	kardinalita	k1gFnSc1	kardinalita
<g/>
)	)	kIx)	)
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslice	číslice	k1gFnSc1	číslice
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
znak	znak	k1gInSc1	znak
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
pozičních	poziční	k2eAgFnPc6d1	poziční
číselných	číselný	k2eAgFnPc6d1	číselná
soustavách	soustava	k1gFnPc6	soustava
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
nepoužitého	použitý	k2eNgInSc2d1	nepoužitý
řádu	řád	k1gInSc2	řád
při	při	k7c6	při
zápisu	zápis	k1gInSc6	zápis
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
desítkové	desítkový	k2eAgFnSc6d1	desítková
soustavě	soustava	k1gFnSc6	soustava
má	mít	k5eAaImIp3nS	mít
číslice	číslice	k1gFnSc1	číslice
"	"	kIx"	"
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
v	v	k7c6	v
zápise	zápis	k1gInSc6	zápis
"	"	kIx"	"
<g/>
100	[number]	k4	100
<g/>
"	"	kIx"	"
váhu	váha	k1gFnSc4	váha
sto	sto	k4xCgNnSc4	sto
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
stejné	stejný	k2eAgFnSc2d1	stejná
číslice	číslice	k1gFnSc2	číslice
v	v	k7c6	v
zápise	zápis	k1gInSc6	zápis
"	"	kIx"	"
<g/>
10	[number]	k4	10
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
váhu	váha	k1gFnSc4	váha
deset	deset	k4xCc1	deset
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
nuly	nula	k1gFnSc2	nula
==	==	k?	==
</s>
</p>
<p>
<s>
Koncept	koncept	k1gInSc4	koncept
nuly	nula	k1gFnSc2	nula
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
schopny	schopen	k2eAgFnPc1d1	schopna
chápat	chápat	k5eAaImF	chápat
i	i	k9	i
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
starověké	starověký	k2eAgFnPc4d1	starověká
Babylonie	Babylonie	k1gFnPc4	Babylonie
v	v	k7c6	v
tamní	tamní	k2eAgFnSc6d1	tamní
šedesátkové	šedesátkový	k2eAgFnSc6d1	šedesátková
poziční	poziční	k2eAgFnSc6d1	poziční
soustavě	soustava	k1gFnSc6	soustava
měl	mít	k5eAaImAgInS	mít
zápis	zápis	k1gInSc1	zápis
nuly	nula	k1gFnSc2	nula
nejprve	nejprve	k6eAd1	nejprve
podobu	podoba	k1gFnSc4	podoba
mezery	mezera	k1gFnSc2	mezera
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
podobu	podoba	k1gFnSc4	podoba
šikmého	šikmý	k2eAgInSc2d1	šikmý
dvojitého	dvojitý	k2eAgInSc2d1	dvojitý
klínu	klín	k1gInSc2	klín
<g/>
.	.	kIx.	.
</s>
<s>
Zápis	zápis	k1gInSc1	zápis
nuly	nula	k1gFnSc2	nula
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
neměli	mít	k5eNaImAgMnP	mít
poziční	poziční	k2eAgInSc4d1	poziční
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgNnSc4d1	samostatné
číslo	číslo	k1gNnSc4	číslo
nula	nula	k1gFnSc1	nula
tak	tak	k8xS	tak
neznali	neznat	k5eAaImAgMnP	neznat
ani	ani	k8xC	ani
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
ani	ani	k8xC	ani
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Chybějící	chybějící	k2eAgInSc1d1	chybějící
koncept	koncept	k1gInSc1	koncept
zápisu	zápis	k1gInSc2	zápis
čísla	číslo	k1gNnSc2	číslo
nula	nula	k1gFnSc1	nula
způsoboval	způsobovat	k5eAaImAgInS	způsobovat
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
matematických	matematický	k2eAgInPc6d1	matematický
výpočtech	výpočet	k1gInPc6	výpočet
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
např.	např.	kA	např.
v	v	k7c6	v
neexistenci	neexistence	k1gFnSc6	neexistence
roku	rok	k1gInSc2	rok
0	[number]	k4	0
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
letopočtu	letopočet	k1gInSc6	letopočet
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
se	se	k3xPyFc4	se
počítaly	počítat	k5eAaImAgFnP	počítat
od	od	k7c2	od
nuly	nula	k1gFnSc2	nula
i	i	k8xC	i
dny	den	k1gInPc4	den
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnSc2	některý
středoamerické	středoamerický	k2eAgFnSc2d1	středoamerická
civilizace	civilizace	k1gFnSc2	civilizace
jako	jako	k8xS	jako
Olmékové	Olméková	k1gFnSc2	Olméková
a	a	k8xC	a
Mayové	Mayová	k1gFnSc2	Mayová
ji	on	k3xPp3gFnSc4	on
používali	používat	k5eAaImAgMnP	používat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dvacítkové	dvacítkový	k2eAgFnSc6d1	dvacítková
soustavě	soustava	k1gFnSc6	soustava
již	již	k6eAd1	již
ve	v	k7c6	v
4.	[number]	k4	4.
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
mezoamerický	mezoamerický	k2eAgInSc4d1	mezoamerický
kalendář	kalendář	k1gInSc4	kalendář
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
nulu	nula	k1gFnSc4	nula
v	v	k7c6	v
pozičním	poziční	k2eAgInSc6d1	poziční
zápise	zápis	k1gInSc6	zápis
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nepřímým	přímý	k2eNgInSc7d1	nepřímý
důkazem	důkaz	k1gInSc7	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
symbol	symbol	k1gInSc1	symbol
nuly	nula	k1gFnSc2	nula
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
36	[number]	k4	36
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
nejstarší	starý	k2eAgInSc4d3	nejstarší
indický	indický	k2eAgInSc4d1	indický
záznam	záznam	k1gInSc4	záznam
nuly	nula	k1gFnSc2	nula
vědci	vědec	k1gMnPc7	vědec
původně	původně	k6eAd1	původně
považovali	považovat	k5eAaImAgMnP	považovat
zápis	zápis	k1gInSc4	zápis
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
chrámu	chrám	k1gInSc2	chrám
v	v	k7c4	v
Gválijaru	Gválijara	k1gFnSc4	Gválijara
z	z	k7c2	z
devátého	devátý	k4xOgNnSc2	devátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zápis	zápis	k1gInSc1	zápis
nuly	nula	k1gFnSc2	nula
v	v	k7c6	v
manuskriptu	manuskript	k1gInSc6	manuskript
z	z	k7c2	z
pákistánské	pákistánský	k2eAgFnSc2d1	pákistánská
vesnice	vesnice	k1gFnSc2	vesnice
Bakhšálí	Bakhšálý	k2eAgMnPc1d1	Bakhšálý
byl	být	k5eAaImAgInS	být
pomocí	pomoc	k1gFnSc7	pomoc
uhlíkové	uhlíkový	k2eAgFnSc2d1	uhlíková
metody	metoda	k1gFnSc2	metoda
nově	nově	k6eAd1	nově
datován	datovat	k5eAaImNgInS	datovat
do	do	k7c2	do
třetího	třetí	k4xOgMnSc2	třetí
až	až	k8xS	až
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Cesta	cesta	k1gFnSc1	cesta
nuly	nula	k1gFnSc2	nula
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Středověcí	středověký	k2eAgMnPc1d1	středověký
Evropané	Evropan	k1gMnPc1	Evropan
se	se	k3xPyFc4	se
s	s	k7c7	s
nulou	nula	k1gFnSc7	nula
naučili	naučit	k5eAaPmAgMnP	naučit
počítat	počítat	k5eAaImF	počítat
od	od	k7c2	od
12.	[number]	k4	12.
století	století	k1gNnSc2	století
od	od	k7c2	od
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ji	on	k3xPp3gFnSc4	on
přinesli	přinést	k5eAaPmAgMnP	přinést
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
hindsko-arabská	hindskorabský	k2eAgFnSc1d1	hindsko-arabský
číselná	číselný	k2eAgFnSc1d1	číselná
soustava	soustava	k1gFnSc1	soustava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nulu	nula	k1gFnSc4	nula
a	a	k8xC	a
matematické	matematický	k2eAgFnPc4d1	matematická
operace	operace	k1gFnPc4	operace
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Indie	Indie	k1gFnSc2	Indie
Brahmagupta	Brahmagupt	k1gInSc2	Brahmagupt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
knih	kniha	k1gFnPc2	kniha
Brā	Brā	k1gFnSc2	Brā
a	a	k8xC	a
Khanda-khā	Khandahā	k1gFnSc2	Khanda-khā
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
části	část	k1gFnSc2	část
Indie	Indie	k1gFnSc2	Indie
muslimskými	muslimský	k2eAgNnPc7d1	muslimské
vojsky	vojsko	k1gNnPc7	vojsko
naučil	naučit	k5eAaPmAgMnS	naučit
nazpaměť	nazpaměť	k6eAd1	nazpaměť
učenec	učenec	k1gMnSc1	učenec
Kanaka	Kanak	k1gMnSc2	Kanak
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
arabštiny	arabština	k1gFnSc2	arabština
přeložil	přeložit	k5eAaPmAgMnS	přeložit
astronom	astronom	k1gMnSc1	astronom
Mohammed	Mohammed	k1gMnSc1	Mohammed
al-Fazarí	al-Fazarí	k1gMnSc1	al-Fazarí
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
způsobil	způsobit	k5eAaPmAgInS	způsobit
rozmach	rozmach	k1gInSc4	rozmach
arabské	arabský	k2eAgFnSc2d1	arabská
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrchollu	vrcholl	k1gInSc6	vrcholl
tohoto	tento	k3xDgInSc2	tento
rozmachu	rozmach	k1gInSc2	rozmach
napsal	napsat	k5eAaPmAgMnS	napsat
perský	perský	k2eAgMnSc1d1	perský
matematik	matematik	k1gMnSc1	matematik
Abú	abú	k1gMnSc1	abú
Abd	Abd	k1gMnSc1	Abd
Alláh	Alláh	k1gMnSc1	Alláh
Muhammad	Muhammad	k1gInSc4	Muhammad
Ibn	Ibn	k1gFnSc2	Ibn
Músá	Músá	k1gFnSc1	Músá
al-Chórezmí	al-Chórezmit	k5eAaPmIp3nS	al-Chórezmit
Abú	abú	k1gMnSc1	abú
Dža	Dža	k1gMnSc1	Dža
<g/>
'	'	kIx"	'
<g/>
far	fara	k1gFnPc2	fara
učebnici	učebnice	k1gFnSc4	učebnice
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
odčítání	odčítání	k1gNnSc2	odčítání
indických	indický	k2eAgFnPc2d1	indická
číslic	číslice	k1gFnPc2	číslice
Kitáb	Kitáb	k1gMnSc1	Kitáb
al-džám	alžat	k5eAaBmIp1nS	al-džat
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
wa-l-tafríq	waafríq	k?	wa-l-tafríq
bil-hisáb	bilisáb	k1gMnSc1	bil-hisáb
al-hindi	alind	k1gMnPc1	al-hind
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ve	v	k7c4	v
13.	[number]	k4	13.
století	století	k1gNnSc2	století
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Algorithmi	Algorith	k1gFnPc7	Algorith
de	de	k?	de
numero	numero	k?	numero
indorum	indorum	k1gInSc1	indorum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Matematické	matematický	k2eAgFnSc2d1	matematická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
čísla	číslo	k1gNnSc2	číslo
nula	nula	k1gFnSc1	nula
==	==	k?	==
</s>
</p>
<p>
<s>
Číslo	číslo	k1gNnSc1	číslo
nula	nula	k1gFnSc1	nula
má	mít	k5eAaImIp3nS	mít
některé	některý	k3yIgFnPc4	některý
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
početních	početní	k2eAgFnPc2d1	početní
operací	operace	k1gFnPc2	operace
brát	brát	k5eAaImF	brát
v	v	k7c6	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sčítání	sčítání	k1gNnSc2	sčítání
===	===	k?	===
</s>
</p>
<p>
<s>
Nula	nula	k1gFnSc1	nula
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
matematického	matematický	k2eAgNnSc2d1	matematické
hlediska	hledisko	k1gNnSc2	hledisko
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
+	+	kIx~	+
<g/>
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Násobení	násobení	k1gNnSc2	násobení
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
násobení	násobení	k1gNnSc2	násobení
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
0	[number]	k4	0
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nula	nula	k1gFnSc1	nula
je	být	k5eAaImIp3nS	být
absorpční	absorpční	k2eAgInSc4d1	absorpční
prvek	prvek	k1gInSc4	prvek
násobení	násobení	k1gNnSc2	násobení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Faktoriál	faktoriál	k1gInSc4	faktoriál
===	===	k?	===
</s>
</p>
<p>
<s>
Faktoriál	faktoriál	k1gInSc4	faktoriál
čísla	číslo	k1gNnSc2	číslo
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Umocňování	umocňování	k1gNnSc2	umocňování
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
umocňování	umocňování	k1gNnSc6	umocňování
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
I	i	k9	i
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
definuje	definovat	k5eAaBmIp3nS	definovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
matematice	matematika	k1gFnSc6	matematika
však	však	k9	však
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
není	být	k5eNaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
<g/>
.	.	kIx.	.
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
nula	nula	k1gFnSc1	nula
na	na	k7c4	na
nultou	nultý	k4xOgFnSc4	nultý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dělení	dělení	k1gNnSc2	dělení
nulou	nula	k1gFnSc7	nula
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
dělení	dělení	k1gNnSc2	dělení
libovolného	libovolný	k2eAgNnSc2d1	libovolné
čísla	číslo	k1gNnSc2	číslo
nulou	nula	k1gFnSc7	nula
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
takové	takový	k3xDgFnSc2	takový
operace	operace	k1gFnSc2	operace
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
nedefinován	definován	k2eNgInSc1d1	nedefinován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
přirozená	přirozený	k2eAgNnPc4d1	přirozené
čísla	číslo	k1gNnPc4	číslo
můžeme	moct	k5eAaImIp1nP	moct
operaci	operace	k1gFnSc4	operace
dělení	dělení	k1gNnSc4	dělení
nahradit	nahradit	k5eAaPmF	nahradit
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
odečítáním	odečítání	k1gNnSc7	odečítání
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
hledat	hledat	k5eAaImF	hledat
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Kolikrát	kolikrát	k6eAd1	kolikrát
musíme	muset	k5eAaImIp1nP	muset
odečíst	odečíst	k5eAaPmF	odečíst
4	[number]	k4	4
od	od	k7c2	od
12	[number]	k4	12
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
dostali	dostat	k5eAaPmAgMnP	dostat
výsledek	výsledek	k1gInSc4	výsledek
0	[number]	k4	0
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kolik	kolik	k9	kolik
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
děleno	dělit	k5eAaImNgNnS	dělit
4	[number]	k4	4
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
12	[number]	k4	12
−	−	k?	−
4	[number]	k4	4
=	=	kIx~	=
8	[number]	k4	8
</s>
</p>
<p>
<s>
8	[number]	k4	8
−	−	k?	−
4	[number]	k4	4
=	=	kIx~	=
4	[number]	k4	4
</s>
</p>
<p>
<s>
4	[number]	k4	4
−	−	k?	−
4	[number]	k4	4
=	=	kIx~	=
0	[number]	k4	0
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc4	počet
odečítání	odečítání	k1gNnSc2	odečítání
jsou	být	k5eAaImIp3nP	být
3	[number]	k4	3
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
a	a	k8xC	a
tedy	tedy	k9	tedy
12	[number]	k4	12
:	:	kIx,	:
4	[number]	k4	4
=	=	kIx~	=
3.	[number]	k4	3.
<g/>
Pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
vypočítat	vypočítat	k5eAaPmF	vypočítat
12	[number]	k4	12
:	:	kIx,	:
0	[number]	k4	0
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
otázka	otázka	k1gFnSc1	otázka
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kolikrát	kolikrát	k6eAd1	kolikrát
musíme	muset	k5eAaImIp1nP	muset
odečíst	odečíst	k5eAaPmF	odečíst
0	[number]	k4	0
od	od	k7c2	od
12	[number]	k4	12
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
0	[number]	k4	0
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Žádný	žádný	k3yNgInSc1	žádný
počet	počet	k1gInSc1	počet
operací	operace	k1gFnPc2	operace
však	však	k9	však
nevede	vést	k5eNaImIp3nS	vést
k	k	k7c3	k
požadovanému	požadovaný	k2eAgInSc3d1	požadovaný
výsledku	výsledek	k1gInSc3	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SEIFE	SEIFE	kA	SEIFE
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Nula	nula	k1gFnSc1	nula
<g/>
:	:	kIx,	:
životopis	životopis	k1gInSc1	životopis
jedné	jeden	k4xCgFnSc2	jeden
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2005.	[number]	k4	2005.
263	[number]	k4	263
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7363-048-6	[number]	k4	80-7363-048-6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nula	nula	k1gFnSc1	nula
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
nula	nula	k1gFnSc1	nula
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Nula	nula	k1gFnSc1	nula
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Nula	nula	k1gFnSc1	nula
</s>
</p>
