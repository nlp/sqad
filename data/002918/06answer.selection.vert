<s>
Zatímco	zatímco	k8xS	zatímco
Houston	Houston	k1gInSc1	Houston
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
a	a	k8xC	a
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
největší	veliký	k2eAgFnPc4d3	veliký
v	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
aglomerace	aglomerace	k1gFnSc1	aglomerace
Dallas-Fort	Dallas-Fort	k1gInSc1	Dallas-Fort
Worth	Worth	k1gInSc1	Worth
metropolitan	metropolitan	k1gInSc1	metropolitan
area	area	k1gFnSc1	area
je	být	k5eAaImIp3nS	být
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
město	město	k1gNnSc1	město
Houston	Houston	k1gInSc1	Houston
<g/>
.	.	kIx.	.
</s>
