<s>
Unity	unita	k1gMnPc4
(	(	kIx(
<g/>
herní	herní	k2eAgMnSc1d1
engine	enginout	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
Unity	unita	k1gMnPc4
</s>
<s>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Unity	unita	k1gMnSc2
Technologies	Technologies	k1gMnSc1
První	první	k4xOgFnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
2005	#num#	k4
Aktuální	aktuální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
2021.1	2021.1	k4
Nestabilní	stabilní	k2eNgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
2021.2	2021.2	k4
(	(	kIx(
<g/>
alpha	alpha	k1gFnSc1
<g/>
)	)	kIx)
Vyvíjeno	vyvíjen	k2eAgNnSc1d1
v	v	k7c6
</s>
<s>
C	C	kA
<g/>
++	++	k?
(	(	kIx(
<g/>
runtime	runtime	k1gInSc1
<g/>
)	)	kIx)
<g/>
C	C	kA
<g/>
#	#	kIx~
(	(	kIx(
<g/>
Unity	unita	k1gMnSc2
Scripting	Scripting	k1gInSc1
API	API	kA
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
Multiplatformní	multiplatformní	k2eAgFnSc1d1
Licence	licence	k1gFnSc1
</s>
<s>
proprietární	proprietární	k2eAgFnSc1d1
licence	licence	k1gFnSc1
Web	web	k1gInSc1
</s>
<s>
unity	unita	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Unity	unita	k1gMnPc4
je	být	k5eAaImIp3nS
multiplatformní	multiplatformní	k2eAgInSc1d1
herní	herní	k2eAgInSc1d1
engine	enginout	k5eAaPmIp3nS
vyvinutý	vyvinutý	k2eAgInSc1d1
společností	společnost	k1gFnSc7
Unity	unita	k1gMnPc4
Technologies	Technologies	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
použit	použít	k5eAaPmNgMnS
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
her	hra	k1gFnPc2
pro	pro	k7c4
PC	PC	kA
<g/>
,	,	kIx,
konzole	konzola	k1gFnSc6
<g/>
,	,	kIx,
mobily	mobil	k1gInPc4
a	a	k8xC
web	web	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
podporovala	podporovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
OS	OS	kA
X	X	kA
a	a	k8xC
byla	být	k5eAaImAgFnS
představena	představit	k5eAaPmNgFnS
na	na	k7c6
celosvětové	celosvětový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
Applu	Appl	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgInS
rozvinut	rozvinout	k5eAaPmNgInS
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
patnáct	patnáct	k4xCc4
dalších	další	k2eAgFnPc2d1
platforem	platforma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Unity	unita	k1gMnPc4
poskytuje	poskytovat	k5eAaImIp3nS
možnosti	možnost	k1gFnPc1
vývoje	vývoj	k1gInSc2
pro	pro	k7c4
2D	2D	k4
i	i	k9
3D	3D	k4
hry	hra	k1gFnPc4
libovolného	libovolný	k2eAgInSc2d1
žánru	žánr	k1gInSc2
a	a	k8xC
zaměření	zaměření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
grafického	grafický	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
<g/>
,	,	kIx,
podporuje	podporovat	k5eAaImIp3nS
také	také	k9
tvorbu	tvorba	k1gFnSc4
skriptů	skript	k1gInPc2
v	v	k7c6
jazyce	jazyk	k1gInSc6
C	C	kA
<g/>
#	#	kIx~
.	.	kIx.
</s>
<s desamb="1">
Firma	firma	k1gFnSc1
Unity	unita	k1gMnPc4
Technologies	Technologies	k1gMnSc1
provozuje	provozovat	k5eAaImIp3nS
bezplatnou	bezplatný	k2eAgFnSc4d1
i	i	k8xC
placenou	placený	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1
konkurentem	konkurent	k1gMnSc7
(	(	kIx(
<g/>
především	především	k6eAd1
na	na	k7c6
trhu	trh	k1gInSc6
her	hra	k1gFnPc2
pro	pro	k7c4
PC	PC	kA
a	a	k8xC
konzole	konzola	k1gFnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Unreal	Unreal	k1gMnSc1
Engine	Engin	k1gInSc5
od	od	k7c2
společnosti	společnost	k1gFnSc2
Epic	Epic	k1gInSc1
Games	Games	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Herní	herní	k2eAgNnSc1d1
engine	enginout	k5eAaPmIp3nS
Unity	unita	k1gMnSc2
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
oficiálně	oficiálně	k6eAd1
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
s	s	k7c7
účelem	účel	k1gInSc7
přiblížit	přiblížit	k5eAaPmF
proces	proces	k1gInSc4
vývoje	vývoj	k1gInSc2
her	hra	k1gFnPc2
i	i	k8xC
malým	malý	k2eAgInPc3d1
nezkušeným	zkušený	k2eNgInPc3d1
vývojářským	vývojářský	k2eAgInPc3d1
týmům	tým	k1gInPc3
mimo	mimo	k7c4
velká	velký	k2eAgNnPc4d1
herní	herní	k2eAgNnPc4d1
studia	studio	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc1
verze	verze	k1gFnSc1
nabízela	nabízet	k5eAaImAgFnS
možnost	možnost	k1gFnSc4
vyvíjet	vyvíjet	k5eAaImF
hry	hra	k1gFnPc4
pouze	pouze	k6eAd1
pro	pro	k7c4
macOS	macOS	k?
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
pro	pro	k7c4
Windows	Windows	kA
(	(	kIx(
<g/>
a	a	k8xC
webové	webový	k2eAgInPc4d1
prohlížeče	prohlížeč	k1gInPc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
přidána	přidat	k5eAaPmNgFnS
až	až	k9
ve	v	k7c6
verzi	verze	k1gFnSc6
1.1	1.1	k4
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byla	být	k5eAaImAgFnS
představena	představit	k5eAaPmNgFnS
verze	verze	k1gFnSc1
2.0	2.0	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
rozšiřovala	rozšiřovat	k5eAaImAgFnS
podporu	podpora	k1gFnSc4
pro	pro	k7c4
web	web	k1gInSc4
a	a	k8xC
Windows	Windows	kA
-	-	kIx~
byla	být	k5eAaImAgFnS
přidána	přidat	k5eAaPmNgFnS
podpora	podpora	k1gFnSc1
pro	pro	k7c4
DirectX	DirectX	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
asi	asi	k9
30	#num#	k4
<g/>
%	%	kIx~
zrychlení	zrychlení	k1gNnSc3
na	na	k7c6
Windowsových	Windowsův	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Mimo	mimo	k7c4
to	ten	k3xDgNnSc4
také	také	k9
přinesla	přinést	k5eAaPmAgFnS
asi	asi	k9
50	#num#	k4
nových	nový	k2eAgFnPc2d1
funkcionalit	funkcionalita	k1gFnPc2
-	-	kIx~
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
například	například	k6eAd1
optimalizovaný	optimalizovaný	k2eAgInSc1d1
engine	enginout	k5eAaPmIp3nS
pro	pro	k7c4
3D	3D	k4
terény	terén	k1gInPc4
s	s	k7c7
pokročilou	pokročilý	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
nastavení	nastavení	k1gNnSc2
světel	světlo	k1gNnPc2
<g/>
,	,	kIx,
dynamické	dynamický	k2eAgInPc1d1
stíny	stín	k1gInPc1
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
a	a	k8xC
podporu	podpora	k1gFnSc4
pro	pro	k7c4
vytváření	vytváření	k1gNnSc4
multiplayerových	multiplayerův	k2eAgFnPc2d1
her	hra	k1gFnPc2
s	s	k7c7
použitím	použití	k1gNnSc7
UDP	UDP	kA
protokolu	protokol	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dalším	další	k2eAgInSc7d1
převratným	převratný	k2eAgInSc7d1
momentem	moment	k1gInSc7
bylo	být	k5eAaImAgNnS
představení	představení	k1gNnSc1
obchodu	obchod	k1gInSc2
s	s	k7c7
aplikacemi	aplikace	k1gFnPc7
pro	pro	k7c4
mobily	mobil	k1gInPc4
značky	značka	k1gFnSc2
Apple	Apple	kA
-	-	kIx~
App	App	k1gMnSc5
Store	Stor	k1gMnSc5
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnSc2
Technologies	Technologies	k1gInSc4
vzápětí	vzápětí	k6eAd1
vydala	vydat	k5eAaPmAgFnS
separátní	separátní	k2eAgInSc4d1
projekt	projekt	k1gInSc4
Unity	unita	k1gMnSc2
iPhone	iPhon	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
podporoval	podporovat	k5eAaImAgInS
vydávání	vydávání	k1gNnSc4
her	hra	k1gFnPc2
právě	právě	k9
pro	pro	k7c4
iPhone	iPhon	k1gInSc5
a	a	k8xC
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
tak	tak	k9
prvním	první	k4xOgNnSc7
enginem	engino	k1gNnSc7
pro	pro	k7c4
tyto	tento	k3xDgInPc4
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mu	on	k3xPp3gMnSc3
na	na	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
zajistilo	zajistit	k5eAaPmAgNnS
výsadní	výsadní	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
mezi	mezi	k7c7
vývojáři	vývojář	k1gMnPc7
pro	pro	k7c4
IOS	IOS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
tvůrci	tvůrce	k1gMnPc1
pocítili	pocítit	k5eAaPmAgMnP
zvýšený	zvýšený	k2eAgInSc4d1
zájem	zájem	k1gInSc4
o	o	k7c4
editor	editor	k1gInSc4
i	i	k9
mezi	mezi	k7c7
vývojáři	vývojář	k1gMnPc7
nepoužívající	používající	k2eNgFnSc2d1
Mac	Mac	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	on	k3xPp3gMnPc4
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
editoru	editor	k1gInSc2
i	i	k9
pro	pro	k7c4
Windows	Windows	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
přepsat	přepsat	k5eAaPmF
celý	celý	k2eAgInSc4d1
editor	editor	k1gInSc4
od	od	k7c2
začátku	začátek	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
multiplatformní	multiplatformní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gNnSc3
představení	představení	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
představení	představení	k1gNnSc2
verze	verze	k1gFnSc2
2.5	2.5	k4
<g/>
)	)	kIx)
na	na	k7c4
Game	game	k1gInSc4
Developers	Developersa	k1gFnPc2
Conference	Conferenec	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInSc1d1
větší	veliký	k2eAgInSc1d2
update	update	k1gInSc1
na	na	k7c6
verzi	verze	k1gFnSc6
3.0	3.0	k4
přišel	přijít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
vylepšení	vylepšení	k1gNnSc4
funkcionality	funkcionalita	k1gFnSc2
bylo	být	k5eAaImAgNnS
jejím	její	k3xOp3gNnSc7
hlavním	hlavní	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
sjednocení	sjednocení	k1gNnSc2
editorů	editor	k1gInPc2
sloužících	sloužící	k2eAgInPc2d1
k	k	k7c3
vytváření	vytváření	k1gNnSc3
aplikací	aplikace	k1gFnPc2
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
(	(	kIx(
<g/>
Unity	unita	k1gMnPc4
iPhone	iPhon	k1gInSc5
<g/>
,	,	kIx,
Unity	unita	k1gMnSc2
Wii	Wii	k1gMnSc2
<g/>
)	)	kIx)
do	do	k7c2
jednoho	jeden	k4xCgNnSc2
s	s	k7c7
možností	možnost	k1gFnSc7
publikovat	publikovat	k5eAaBmF
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
platformy	platforma	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dlouho	dlouho	k6eAd1
očekávanou	očekávaný	k2eAgFnSc7d1
funkcionalitou	funkcionalita	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
implementována	implementovat	k5eAaImNgFnS
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2013	#num#	k4
ve	v	k7c6
verzi	verze	k1gFnSc6
4.3	4.3	k4
byla	být	k5eAaImAgFnS
podpora	podpora	k1gFnSc1
vývoje	vývoj	k1gInSc2
2D	2D	k4
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
museli	muset	k5eAaImAgMnP
vývojáři	vývojář	k1gMnPc1
2D	2D	k4
her	hra	k1gFnPc2
používat	používat	k5eAaImF
různá	různý	k2eAgNnPc4d1
kreativní	kreativní	k2eAgNnPc4d1
řešení	řešení	k1gNnPc4
<g/>
,	,	kIx,
plná	plný	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
2D	2D	k4
vývoje	vývoj	k1gInSc2
a	a	k8xC
řešení	řešení	k1gNnSc4
pomocí	pomoc	k1gFnPc2
sprite	sprit	k1gInSc5
přišlo	přijít	k5eAaPmAgNnS
až	až	k9
se	s	k7c7
zmíněnou	zmíněný	k2eAgFnSc7d1
aktualizací	aktualizace	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
také	také	k9
sociální	sociální	k2eAgFnSc1d1
síť	síť	k1gFnSc1
Facebook	Facebook	k1gInSc1
integrovala	integrovat	k5eAaBmAgFnS
SDK	SDK	kA
pro	pro	k7c4
hry	hra	k1gFnPc4
vytvořené	vytvořený	k2eAgFnPc4d1
v	v	k7c4
Unity	unita	k1gMnPc4
engine	enginout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
pak	pak	k6eAd1
obě	dva	k4xCgFnPc1
firmy	firma	k1gFnPc1
společně	společně	k6eAd1
vytvořili	vytvořit	k5eAaPmAgMnP
novou	nový	k2eAgFnSc4d1
herní	herní	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
pro	pro	k7c4
PC	PC	kA
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Unity	unita	k1gMnPc4
pak	pak	k6eAd1
prošlo	projít	k5eAaPmAgNnS
ještě	ještě	k9
mnoha	mnoho	k4c7
úpravami	úprava	k1gFnPc7
a	a	k8xC
vylepšeními	vylepšení	k1gNnPc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
komponent	komponenta	k1gFnPc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
přidáním	přidání	k1gNnSc7
nových	nový	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
společnost	společnost	k1gFnSc1
začala	začít	k5eAaPmAgFnS
realizovat	realizovat	k5eAaBmF
plány	plán	k1gInPc4
pro	pro	k7c4
využití	využití	k1gNnSc4
enginu	enginout	k5eAaPmIp1nS
i	i	k9
mimo	mimo	k7c4
herní	herní	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
(	(	kIx(
<g/>
například	například	k6eAd1
přidáním	přidání	k1gNnSc7
nového	nový	k2eAgInSc2d1
systému	systém	k1gInSc2
animací	animace	k1gFnPc2
pro	pro	k7c4
vytváření	vytváření	k1gNnSc4
filmů	film	k1gInPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
pak	pak	k6eAd1
byly	být	k5eAaImAgInP
přidáni	přidat	k5eAaPmNgMnP
funkce	funkce	k1gFnPc4
pro	pro	k7c4
strojové	strojový	k2eAgNnSc4d1
učení	učení	k1gNnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
funkce	funkce	k1gFnSc1
pro	pro	k7c4
učení	učení	k1gNnSc4
z	z	k7c2
návyků	návyk	k1gInPc2
reálných	reálný	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
průlomovou	průlomový	k2eAgFnSc7d1
funkcionalitou	funkcionalita	k1gFnSc7
bylo	být	k5eAaImAgNnS
představení	představení	k1gNnSc1
Scriptable	Scriptable	k1gFnSc2
Render	Rendra	k1gFnPc2
Pipeline	Pipelin	k1gInSc5
(	(	kIx(
<g/>
SRP	srp	k1gInSc4
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
technologie	technologie	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
uživateli	uživatel	k1gMnSc3
kontrolovat	kontrolovat	k5eAaImF
renderování	renderování	k1gNnSc4
pomocí	pomocí	k7c2
C	C	kA
<g/>
#	#	kIx~
scriptů	script	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
byly	být	k5eAaImAgFnP
nabízeny	nabízen	k2eAgInPc1d1
dvě	dva	k4xCgFnPc1
možnosti	možnost	k1gFnPc1
-	-	kIx~
High	High	k1gInSc1
Definition	Definition	k1gInSc1
Render	Render	k1gInSc1
Pipeline	Pipelin	k1gInSc5
(	(	kIx(
<g/>
HDRP	HDRP	kA
<g/>
)	)	kIx)
sloužící	sloužící	k2eAgMnSc1d1
pro	pro	k7c4
vytváření	vytváření	k1gNnSc4
ultra	ultra	k2eAgFnSc2d1
realistické	realistický	k2eAgFnSc2d1
grafiky	grafika	k1gFnSc2
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
fidelitou	fidelita	k1gFnSc7
pro	pro	k7c4
nevyšší	vysoký	k2eNgFnSc4d2
kategorii	kategorie	k1gFnSc4
her	hra	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pro	pro	k7c4
využití	využití	k1gNnSc4
v	v	k7c6
architektuře	architektura	k1gFnSc6
či	či	k8xC
automobilovém	automobilový	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Druhou	druhý	k4xOgFnSc7
možností	možnost	k1gFnSc7
byla	být	k5eAaImAgFnS
Lightweight	Lightweight	k2eAgInSc4d1
Render	Render	k1gInSc4
Pipeline	Pipelin	k1gInSc5
(	(	kIx(
<g/>
LWRP	LWRP	kA
<g/>
)	)	kIx)
sloužící	sloužící	k2eAgMnSc1d1
především	především	k6eAd1
pro	pro	k7c4
mobilní	mobilní	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
a	a	k8xC
využití	využití	k1gNnSc4
s	s	k7c7
virtuální	virtuální	k2eAgFnSc7d1
realitou	realita	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
LWRP	LWRP	kA
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
nahrazena	nahradit	k5eAaPmNgFnS
univerzálnější	univerzální	k2eAgFnSc7d2
Universal	Universal	k1gFnSc7
Render	Rendra	k1gFnPc2
Pipeline	Pipelin	k1gInSc5
(	(	kIx(
<g/>
URP	URP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
Unity	unita	k1gMnSc2
také	také	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
vytvořit	vytvořit	k5eAaPmF
si	se	k3xPyFc3
a	a	k8xC
přizpůsobit	přizpůsobit	k5eAaPmF
vlastní	vlastní	k2eAgInSc4d1
SRP	srp	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
</s>
<s>
Klíčové	klíčový	k2eAgFnPc1d1
komponenty	komponenta	k1gFnPc1
</s>
<s>
Herní	herní	k2eAgNnSc1d1
engine	enginout	k5eAaPmIp3nS
je	on	k3xPp3gNnSc4
komplexní	komplexní	k2eAgNnSc4d1
vývojové	vývojový	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
složené	složený	k2eAgInPc1d1
z	z	k7c2
různých	různý	k2eAgFnPc2d1
komponent	komponenta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ty	ten	k3xDgMnPc4
stěžejní	stěžejní	k2eAgMnPc4d1
lze	lze	k6eAd1
zařadit	zařadit	k5eAaPmF
<g/>
:	:	kIx,
</s>
<s>
Vykreslovací	vykreslovací	k2eAgNnSc1d1
engine	enginout	k5eAaPmIp3nS
</s>
<s>
Část	část	k1gFnSc1
zajišťující	zajišťující	k2eAgNnSc1d1
renderování	renderování	k1gNnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
vykreslování	vykreslování	k1gNnSc1
reálně	reálně	k6eAd1
vypadajících	vypadající	k2eAgInPc2d1
obrazů	obraz	k1gInPc2
na	na	k7c6
základě	základ	k1gInSc6
dat	datum	k1gNnPc2
zpracovaných	zpracovaný	k2eAgInPc2d1
grafickou	grafický	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
-	-	kIx~
tvarů	tvar	k1gInPc2
<g/>
,	,	kIx,
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
světel	světlo	k1gNnPc2
apod.	apod.	kA
Kromě	kromě	k7c2
základního	základní	k2eAgNnSc2d1
vestavěného	vestavěný	k2eAgNnSc2d1
řešení	řešení	k1gNnSc2
poskytuje	poskytovat	k5eAaImIp3nS
Unity	unita	k1gMnPc4
pokročilejším	pokročilý	k2eAgMnPc3d2
uživatelům	uživatel	k1gMnPc3
také	také	k9
možnost	možnost	k1gFnSc4
tento	tento	k3xDgInSc4
proces	proces	k1gInSc4
ovládat	ovládat	k5eAaImF
pomocí	pomocí	k7c2
scriptů	script	k1gInPc2
<g/>
,	,	kIx,
díky	díky	k7c3
funkcionalitě	funkcionalita	k1gFnSc3
Scriptable	Scriptable	k1gFnSc2
rendering	rendering	k1gInSc1
pipeline	pipelin	k1gInSc5
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
například	například	k6eAd1
vytvářet	vytvářet	k5eAaImF
ultra-realistickou	ultra-realistický	k2eAgFnSc4d1
HD	HD	kA
grafiku	grafika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Fyzikální	fyzikální	k2eAgNnSc1d1
engine	enginout	k5eAaPmIp3nS
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
simulaci	simulace	k1gFnSc3
fyzikálních	fyzikální	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
jako	jako	k8xS,k8xC
například	například	k6eAd1
gravitace	gravitace	k1gFnSc2
<g/>
,	,	kIx,
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
působení	působení	k1gNnSc2
síly	síla	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
objekt	objekt	k1gInSc1
řízen	řídit	k5eAaImNgInS
fyzikálním	fyzikální	k2eAgInSc7d1
enginem	engin	k1gInSc7
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
připojenou	připojený	k2eAgFnSc4d1
komponentu	komponenta	k1gFnSc4
Rigidbody	Rigidboda	k1gFnSc2
(	(	kIx(
<g/>
Rigidbody	Rigidboda	k1gFnSc2
2	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
také	také	k9
vyhodnocení	vyhodnocení	k1gNnSc6
vzájemného	vzájemný	k2eAgNnSc2d1
působení	působení	k1gNnSc2
objektů	objekt	k1gInPc2
mezi	mezi	k7c7
sebou	se	k3xPyFc7
(	(	kIx(
<g/>
kolizí	kolize	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
objektů	objekt	k1gInPc2
určují	určovat	k5eAaImIp3nP
další	další	k2eAgFnPc4d1
speciální	speciální	k2eAgFnPc4d1
komponenty	komponenta	k1gFnPc4
Collidery	Collidera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Logika	logika	k1gFnSc1
kolize	kolize	k1gFnSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
řešena	řešit	k5eAaImNgFnS
pomocí	pomocí	k7c2
scriptů	script	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Skriptování	Skriptování	k1gNnSc1
</s>
<s>
Pomocí	pomocí	k7c2
skriptů	skript	k1gInPc2
se	se	k3xPyFc4
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
herní	herní	k2eAgFnPc4d1
mechaniky	mechanika	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
část	část	k1gFnSc4
popisující	popisující	k2eAgFnSc4d1
a	a	k8xC
ovládající	ovládající	k2eAgFnSc4d1
logiku	logika	k1gFnSc4
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
skriptů	skript	k1gInPc2
lze	lze	k6eAd1
například	například	k6eAd1
přidávat	přidávat	k5eAaImF
<g/>
,	,	kIx,
odebírat	odebírat	k5eAaImF
a	a	k8xC
měnit	měnit	k5eAaImF
různé	různý	k2eAgInPc4d1
předměty	předmět	k1gInPc4
ve	v	k7c6
scéně	scéna	k1gFnSc6
<g/>
,	,	kIx,
kontrolovat	kontrolovat	k5eAaImF
dodržení	dodržení	k1gNnSc4
pravidel	pravidlo	k1gNnPc2
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
uchovávat	uchovávat	k5eAaImF
důležité	důležitý	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
<g/>
,	,	kIx,
řídit	řídit	k5eAaImF
kolize	kolize	k1gFnPc4
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
číst	číst	k5eAaImF
vstupy	vstup	k1gInPc4
od	od	k7c2
hráče	hráč	k1gMnSc2
(	(	kIx(
<g/>
pohyb	pohyb	k1gInSc1
a	a	k8xC
kliknutí	kliknutí	k1gNnSc1
myši	myš	k1gFnSc2
<g/>
)	)	kIx)
atd.	atd.	kA
</s>
<s>
Programovacím	programovací	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
pro	pro	k7c4
psaní	psaní	k1gNnSc4
scriptů	script	k1gInPc2
v	v	k7c4
Unity	unita	k1gMnPc4
je	být	k5eAaImIp3nS
C	C	kA
<g/>
#	#	kIx~
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
objektově	objektově	k6eAd1
orientovaný	orientovaný	k2eAgInSc4d1
<g/>
,	,	kIx,
statický	statický	k2eAgInSc4d1
programovací	programovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
vyvinutý	vyvinutý	k2eAgInSc4d1
společností	společnost	k1gFnSc7
Microsoft	Microsoft	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
některých	některý	k3yIgFnPc6
verzích	verze	k1gFnPc6
Unity	unita	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
a	a	k8xC
starších	starší	k1gMnPc2
byly	být	k5eAaImAgFnP
ještě	ještě	k6eAd1
podporovány	podporovat	k5eAaImNgInP
jazyky	jazyk	k1gInPc7
Boo	boa	k1gFnSc5
a	a	k8xC
UnityScript	UnityScript	k1gMnSc1
(	(	kIx(
<g/>
jakási	jakýsi	k3yIgFnSc1
verze	verze	k1gFnSc1
JavaScriptu	JavaScript	k2eAgFnSc4d1
pro	pro	k7c4
Unity	unita	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpora	podpora	k1gFnSc1
těchto	tento	k3xDgInPc2
jazyků	jazyk	k1gInPc2
však	však	k9
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
ukončená	ukončený	k2eAgFnSc1d1
a	a	k8xC
hlavním	hlavní	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
pro	pro	k7c4
skriptování	skriptování	k1gNnSc4
v	v	k7c4
Unity	unita	k1gMnPc4
tak	tak	k6eAd1
zůstal	zůstat	k5eAaPmAgMnS
C	C	kA
<g/>
#	#	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Unity	unita	k1gMnPc4
uživatelům	uživatel	k1gMnPc3
poskytuje	poskytovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
dokumentaci	dokumentace	k1gFnSc4
celého	celý	k2eAgInSc2d1
ScriptingAPI	ScriptingAPI	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Zvukový	zvukový	k2eAgMnSc1d1
engine	enginout	k5eAaPmIp3nS
</s>
<s>
Kromě	kromě	k7c2
grafické	grafický	k2eAgFnSc2d1
a	a	k8xC
logické	logický	k2eAgFnSc2d1
části	část	k1gFnSc2
je	být	k5eAaImIp3nS
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
také	také	k9
část	část	k1gFnSc1
zvuková	zvukový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc7d1
funckionalitou	funckionalita	k1gFnSc7
je	být	k5eAaImIp3nS
načtení	načtení	k1gNnSc1
<g/>
,	,	kIx,
dekomprimace	dekomprimace	k1gFnSc1
a	a	k8xC
přehrání	přehrání	k1gNnSc1
zvukové	zvukový	k2eAgFnSc2d1
stopy	stopa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnPc4
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
nabízí	nabízet	k5eAaImIp3nS
i	i	k9
pokročilejší	pokročilý	k2eAgFnPc4d2
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
integrace	integrace	k1gFnSc1
více	hodně	k6eAd2
různých	různý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
zvuku	zvuk	k1gInSc2
dohromady	dohromady	k6eAd1
pomocí	pomocí	k7c2
Audio	audio	k2eAgInSc2d1
mixeru	mixer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživateli	uživatel	k1gMnSc3
nabízí	nabízet	k5eAaImIp3nS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
těmito	tento	k3xDgInPc7
zdroji	zdroj	k1gInPc7
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
pro	pro	k7c4
ně	on	k3xPp3gNnSc4
určit	určit	k5eAaPmF
prahové	prahový	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
a	a	k8xC
na	na	k7c6
nich	on	k3xPp3gInPc6
definovat	definovat	k5eAaBmF
různá	různý	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgFnPc4d1
pokročilejší	pokročilý	k2eAgFnPc4d2
funkce	funkce	k1gFnPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
produkce	produkce	k1gFnSc1
Dopplerova	Dopplerův	k2eAgInSc2d1
jevu	jev	k1gInSc2
<g/>
,	,	kIx,
ozvěn	ozvěna	k1gFnPc2
<g/>
,	,	kIx,
změn	změna	k1gFnPc2
výšek	výška	k1gFnPc2
tónů	tón	k1gInPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
komponenty	komponenta	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
důležité	důležitý	k2eAgFnPc1d1
komponenty	komponenta	k1gFnPc1
herního	herní	k2eAgInSc2d1
enginu	engin	k1gInSc2
Unity	unita	k1gMnSc2
jsou	být	k5eAaImIp3nP
Multithreading	Multithreading	k1gInSc4
(	(	kIx(
<g/>
rozdělení	rozdělení	k1gNnSc4
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
do	do	k7c2
logických	logický	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
pomocí	pomocí	k7c2
různých	různý	k2eAgInPc2d1
optimalizačních	optimalizační	k2eAgInPc2d1
algoritmů	algoritmus	k1gInPc2
střídají	střídat	k5eAaImIp3nP
o	o	k7c4
procesorový	procesorový	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
související	související	k2eAgFnSc1d1
Správa	správa	k1gFnSc1
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
Síťování	síťování	k1gNnSc1
-	-	kIx~
část	část	k1gFnSc1
zajišťující	zajišťující	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
přes	přes	k7c4
internet	internet	k1gInSc4
(	(	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
multiplayer	multiplayer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
animace	animace	k1gFnSc1
<g/>
,	,	kIx,
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Podporované	podporovaný	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
</s>
<s>
Unity	unita	k1gMnPc4
je	být	k5eAaImIp3nS
multiplatformní	multiplatformní	k2eAgInSc1d1
software	software	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Editor	editor	k1gInSc1
je	být	k5eAaImIp3nS
podporovaný	podporovaný	k2eAgInSc1d1
pro	pro	k7c4
nejrozšířenější	rozšířený	k2eAgInPc4d3
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
,	,	kIx,
macOS	macOS	k?
a	a	k8xC
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
pomocí	pomoc	k1gFnSc7
lze	lze	k6eAd1
vytvářet	vytvářet	k5eAaImF
aplikace	aplikace	k1gFnPc4
pro	pro	k7c4
více	hodně	k6eAd2
než	než	k8xS
25	#num#	k4
platforem	platforma	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
mobilních	mobilní	k2eAgFnPc2d1
<g/>
,	,	kIx,
desktopových	desktopový	k2eAgFnPc2d1
<g/>
,	,	kIx,
herních	herní	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
<g/>
,	,	kIx,
virtuální	virtuální	k2eAgFnPc4d1
a	a	k8xC
rozšířené	rozšířený	k2eAgFnPc4d1
reality	realita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgFnPc4d3
platformy	platforma	k1gFnPc4
patří	patřit	k5eAaImIp3nS
iOS	iOS	k?
<g/>
,	,	kIx,
Android	android	k1gInSc1
<g/>
,	,	kIx,
Windows	Windows	kA
<g/>
,	,	kIx,
macOS	macOS	k?
<g/>
,	,	kIx,
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
WebGL	WebGL	k1gFnSc1
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
5	#num#	k4
<g/>
,	,	kIx,
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc1
<g/>
,	,	kIx,
Xbox	Xbox	k1gInSc1
Series	Seriesa	k1gFnPc2
X	X	kA
a	a	k8xC
Series	Series	k1gInSc1
S	s	k7c7
<g/>
,	,	kIx,
Oculus	Oculus	k1gMnSc1
Rift	Rift	k1gMnSc1
<g/>
,	,	kIx,
Android	android	k1gInSc1
TV	TV	kA
<g/>
,	,	kIx,
tvOS	tvOS	k?
<g/>
,	,	kIx,
Nintendo	Nintendo	k6eAd1
Switch	Switch	k1gInSc1
<g/>
,	,	kIx,
ARCore	ARCor	k1gMnSc5
<g/>
,	,	kIx,
Google	Googl	k1gMnSc2
Stadia	stadion	k1gNnSc2
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
HoloLens	HoloLens	k1gInSc1
<g/>
,	,	kIx,
MagicLeap	MagicLeap	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
Unity	unita	k1gMnPc7
stále	stále	k6eAd1
dominuje	dominovat	k5eAaImIp3nS
trhu	trh	k1gInSc3
mobilních	mobilní	k2eAgFnPc2d1
her	hra	k1gFnPc2
(	(	kIx(
<g/>
přes	přes	k7c4
50	#num#	k4
<g/>
%	%	kIx~
veškerých	veškerý	k3xTgFnPc2
her	hra	k1gFnPc2
pro	pro	k7c4
mobily	mobil	k1gInPc4
běží	běžet	k5eAaImIp3nS
na	na	k7c4
Unity	unita	k1gMnPc4
enginu	enginout	k5eAaPmIp1nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tržní	tržní	k2eAgInSc1d1
podíl	podíl	k1gInSc1
přes	přes	k7c4
60	#num#	k4
<g/>
%	%	kIx~
drží	držet	k5eAaImIp3nS
také	také	k9
na	na	k7c6
trhu	trh	k1gInSc6
s	s	k7c7
VR	vr	k0
a	a	k8xC
AR	ar	k1gInSc4
obsahem	obsah	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Licence	licence	k1gFnSc1
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
se	se	k3xPyFc4
Unity	unita	k1gMnSc2
dalo	dát	k5eAaPmAgNnS
pořídit	pořídit	k5eAaPmF
jednorázovým	jednorázový	k2eAgInSc7d1
nákupem	nákup	k1gInSc7
licence	licence	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
zmíněném	zmíněný	k2eAgInSc6d1
roce	rok	k1gInSc6
pak	pak	k6eAd1
společnost	společnost	k1gFnSc1
přešla	přejít	k5eAaPmAgFnS
na	na	k7c4
obchodní	obchodní	k2eAgInSc4d1
model	model	k1gInSc4
předplatného	předplatné	k1gNnSc2
na	na	k7c4
určitý	určitý	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
Unity	unita	k1gMnSc2
nabízí	nabízet	k5eAaImIp3nS
více	hodně	k6eAd2
možností	možnost	k1gFnSc7
předplatného	předplatné	k1gNnSc2
(	(	kIx(
<g/>
plánů	plán	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
jeden	jeden	k4xCgInSc1
je	být	k5eAaImIp3nS
zdarma	zdarma	k6eAd1
pro	pro	k7c4
osobní	osobní	k2eAgNnSc4d1
užití	užití	k1gNnSc4
jednotlivců	jednotlivec	k1gMnPc2
<g/>
,	,	kIx,
či	či	k8xC
malé	malý	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
ročně	ročně	k6eAd1
negenerují	generovat	k5eNaImIp3nP
obrat	obrat	k1gInSc4
více	hodně	k6eAd2
než	než	k8xS
100,000	100,000	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Unity	unita	k1gMnSc2
Asset	Asset	k1gMnSc1
Store	Stor	k1gMnSc5
</s>
<s>
Obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
uživatelé	uživatel	k1gMnPc1
mohou	moct	k5eAaImIp3nP
prodávat	prodávat	k5eAaImF
/	/	kIx~
kupovat	kupovat	k5eAaImF
vlastní	vlastní	k2eAgInPc4d1
vytvořené	vytvořený	k2eAgInPc4d1
objekty	objekt	k1gInPc4
(	(	kIx(
<g/>
případně	případně	k6eAd1
i	i	k9
celé	celý	k2eAgInPc4d1
projekty	projekt	k1gInPc4
<g/>
)	)	kIx)
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
2018	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
počet	počet	k1gInSc1
stažení	stažení	k1gNnSc2
přes	přes	k7c4
Unity	unita	k1gMnPc4
Asset	Asseta	k1gFnPc2
Store	Stor	k1gInSc5
40	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
možnosti	možnost	k1gFnPc1
využití	využití	k1gNnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
Unity	unita	k1gMnPc4
Technologies	Technologies	k1gInSc1
rozhodlo	rozhodnout	k5eAaPmAgNnS
využít	využít	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
herní	herní	k2eAgMnSc1d1
engine	enginout	k5eAaPmIp3nS
také	také	k9
v	v	k7c6
jiných	jiný	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
automobilovém	automobilový	k2eAgInSc6d1
a	a	k8xC
filmovém	filmový	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
filmařem	filmař	k1gMnSc7
Neillem	Neill	k1gMnSc7
Blomkampem	Blomkamp	k1gMnSc7
pak	pak	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
vznikly	vzniknout	k5eAaPmAgInP
dva	dva	k4xCgInPc1
krátké	krátký	k2eAgInPc1d1
počítačově	počítačově	k6eAd1
generované	generovaný	k2eAgInPc4d1
filmy	film	k1gInPc4
Adam	Adam	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Mirror	Mirror	k1gMnSc1
a	a	k8xC
Adam	Adam	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Prophet	Prophet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
automobilovém	automobilový	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
se	se	k3xPyFc4
Unity	unita	k1gMnSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
vytváření	vytváření	k1gNnSc4
modelů	model	k1gInPc2
nových	nový	k2eAgNnPc2d1
aut	auto	k1gNnPc2
ve	v	k7c6
virtuální	virtuální	k2eAgFnSc6d1
realitě	realita	k1gFnSc6
<g/>
,	,	kIx,
sestavení	sestavení	k1gNnSc4
virtuálních	virtuální	k2eAgFnPc2d1
montážních	montážní	k2eAgFnPc2d1
linek	linka	k1gFnPc2
a	a	k8xC
výcviku	výcvik	k1gInSc2
nových	nový	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInPc1d1
obory	obor	k1gInPc1
na	na	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
Unity	unita	k1gMnSc2
Technologies	Technologies	k1gInSc1
soustředí	soustředit	k5eAaPmIp3nS
jsou	být	k5eAaImIp3nP
architektura	architektura	k1gFnSc1
<g/>
,	,	kIx,
strojírenství	strojírenství	k1gNnSc2
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
Odkazy	odkaz	k1gInPc1
</s>
<s>
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
http://unity.com/	http://unity.com/	k?
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
dokumentace	dokumentace	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
https://docs.unity3d.com/Manual/index.html	https://docs.unity3d.com/Manual/index.htmnout	k5eAaPmAgMnS
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
výuková	výukový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
https://learn.unity.com/	https://learn.unity.com/	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
AXON	AXON	kA
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnPc4
at	at	k?
10	#num#	k4
<g/>
:	:	kIx,
For	forum	k1gNnPc2
better	bettra	k1gFnPc2
<g/>
—	—	k?
<g/>
or	or	k?
worse	worse	k6eAd1
<g/>
—	—	k?
<g/>
game	game	k1gInSc1
development	development	k1gInSc1
has	hasit	k5eAaImRp2nS
never	never	k1gMnSc1
been	been	k1gMnSc1
easier	easier	k1gMnSc1
<g/>
.	.	kIx.
arstechnica	arstechnica	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ars	ars	k?
technica	technica	k1gMnSc1
<g/>
,	,	kIx,
27.09	27.09	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
HAAS	HAAS	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Unity	unita	k1gMnSc2
Game	game	k1gInSc1
Engine	Engin	k1gMnSc5
<g/>
.	.	kIx.
web.wpi.edu	web.wpi.et	k5eAaPmIp1nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BRODKIN	BRODKIN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gMnSc2
Unity	unita	k1gMnSc2
<g/>
3	#num#	k4
<g/>
D	D	kA
Became	Becam	k1gInSc5
a	a	k8xC
Game-Development	Game-Development	k1gMnSc1
Beast	Beast	k1gMnSc1
<g/>
.	.	kIx.
insights	insights	k1gInSc1
<g/>
.	.	kIx.
<g/>
dice	dice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dice	Dice	k1gNnSc1
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3.06	3.06	k4
<g/>
.2013	.2013	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
COHEN	COHEN	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnPc4
2.0	2.0	k4
game	game	k1gInSc1
engine	enginout	k5eAaPmIp3nS
now	now	k?
available	available	k6eAd1
<g/>
.	.	kIx.
www.macworld.com	www.macworld.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Macworld	Macworlda	k1gFnPc2
<g/>
,	,	kIx,
10.10	10.10	k4
<g/>
.2007	.2007	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
UNITY	unita	k1gMnPc4
TECHNOLOGIES	TECHNOLOGIES	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnSc2
4.3	4.3	k4
<g/>
.	.	kIx.
unity	unita	k1gMnPc4
<g/>
3	#num#	k4
<g/>
d.	d.	k?
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
COHEN	COHEN	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Facebook	Facebook	k1gInSc1
Developing	Developing	k1gInSc4
New	New	k1gFnSc2
PC	PC	kA
Gaming	Gaming	k1gInSc1
Platform	Platform	k1gInSc1
<g/>
;	;	kIx,
Teams	Teams	k1gInSc1
Up	Up	k1gFnSc2
With	With	k1gMnSc1
Unity	unita	k1gMnSc2
Technologies	Technologies	k1gMnSc1
<g/>
.	.	kIx.
www.adweek.com	www.adweek.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adweek	Adweky	k1gFnPc2
<g/>
,	,	kIx,
19.08	19.08	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NANALYZE	NANALYZE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnPc4
Technologies	Technologiesa	k1gFnPc2
–	–	k?
The	The	k1gMnSc1
World	World	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Leading	Leading	k1gInSc1
Game	game	k1gInSc1
Engine	Engin	k1gInSc5
<g/>
.	.	kIx.
www.nanalyze.com	www.nanalyze.com	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
nanalyze	nanalyze	k1gFnSc1
<g/>
,	,	kIx,
18.10	18.10	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.10	21.10	k4
<g/>
.2017	.2017	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
UNITY	unita	k1gMnPc4
TECHNOLOGIES	TECHNOLOGIES	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
High	Higha	k1gFnPc2
Definition	Definition	k1gInSc1
Render	Render	k1gMnSc1
Pipeline	Pipelin	k1gInSc5
<g/>
.	.	kIx.
docs	docs	k1gInSc4
<g/>
.	.	kIx.
<g/>
unity	unita	k1gMnSc2
<g/>
3	#num#	k4
<g/>
d.	d.	k?
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BATCHELOR	BATCHELOR	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnPc4
2018	#num#	k4
detailed	detailed	k1gMnSc1
in	in	k?
GDC	GDC	kA
keynote	keynot	k1gInSc5
<g/>
.	.	kIx.
www.gamesindustry.biz	www.gamesindustry.biz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
gamesindustry	gamesindustr	k1gInPc1
<g/>
,	,	kIx,
20.03	20.03	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TECHNOLOGIES	TECHNOLOGIES	kA
<g/>
,	,	kIx,
Unity	unita	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnSc2
-	-	kIx~
Manual	Manual	k1gMnSc1
<g/>
:	:	kIx,
Scriptable	Scriptable	k1gMnSc1
Render	Render	k1gMnSc1
Pipeline	Pipelin	k1gInSc5
<g/>
.	.	kIx.
docs	docs	k1gInSc4
<g/>
.	.	kIx.
<g/>
unity	unita	k1gMnSc2
<g/>
3	#num#	k4
<g/>
d.	d.	k?
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FINE	Fin	k1gMnSc5
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UnityScript	UnityScript	k2eAgInSc1d1
<g/>
’	’	k?
<g/>
s	s	k7c7
long	longa	k1gFnPc2
ride	ride	k1gNnSc1
off	off	k?
into	into	k1gMnSc1
the	the	k?
sunset	sunset	k1gMnSc1
<g/>
.	.	kIx.
blogs	blogs	k1gInSc1
<g/>
.	.	kIx.
<g/>
unity	unita	k1gMnSc2
<g/>
3	#num#	k4
<g/>
d.	d.	k?
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnSc2
Blog	Blog	k1gInSc4
<g/>
,	,	kIx,
11.08	11.08	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TECHNOLOGIES	TECHNOLOGIES	kA
<g/>
,	,	kIx,
Unity	unita	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Multiplatform	Multiplatform	k1gInSc1
|	|	kIx~
Unity	unita	k1gMnSc2
<g/>
.	.	kIx.
unity	unita	k1gMnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PECKHAM	PECKHAM	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnSc2
IPO	IPO	kA
aims	aims	k1gInSc1
to	ten	k3xDgNnSc1
fuel	fuenout	k5eAaPmAgInS
growth	growth	k1gInSc1
across	across	k6eAd1
gaming	gaming	k1gInSc1
and	and	k?
beyond	beyond	k1gInSc1
<g/>
.	.	kIx.
techcrunch	techcrunch	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechCrunch	TechCruncha	k1gFnPc2
<g/>
,	,	kIx,
10.09	10.09	k4
<g/>
.2020	.2020	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2022	.2022	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TECHNOLOGIES	TECHNOLOGIES	kA
<g/>
,	,	kIx,
Unity	unita	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Compare	Compar	k1gMnSc5
Unity	unita	k1gMnSc2
plans	plansa	k1gFnPc2
<g/>
:	:	kIx,
Pro	pro	k7c4
vs	vs	k?
Plus	plus	k1gNnSc4
vs	vs	k?
Free	Free	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Choose	Choosa	k1gFnSc6
the	the	k?
best	besta	k1gFnPc2
2D	2D	k4
-	-	kIx~
3D	3D	k4
engine	enginout	k5eAaPmIp3nS
for	forum	k1gNnPc2
your	your	k1gInSc4
project	project	k5eAaPmF
<g/>
!	!	kIx.
</s>
<s desamb="1">
-	-	kIx~
Unity	unita	k1gMnPc4
Store	Stor	k1gInSc5
<g/>
.	.	kIx.
store	stor	k1gInSc5
<g/>
.	.	kIx.
<g/>
unity	unita	k1gMnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GRUBB	GRUBB	kA
<g/>
,	,	kIx,
Jeff	Jeff	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unity	unita	k1gMnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
asset	asset	k1gMnSc1
store	stor	k1gInSc5
boss	boss	k1gMnSc1
has	hasit	k5eAaImRp2nS
big	big	k?
plans	plans	k1gInSc4
to	ten	k3xDgNnSc4
fight	fight	k5eAaPmF
Epic	Epic	k1gFnSc4
<g/>
’	’	k?
<g/>
s	s	k7c7
Unreal	Unreal	k1gMnSc1
<g/>
.	.	kIx.
venturebeat	venturebeat	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GamesBeat	GamesBeat	k1gInSc1
<g/>
,	,	kIx,
18.07	18.07	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21.01	21.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
EDELSTEIN	EDELSTEIN	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
gaming	gaming	k1gInSc4
company	compana	k1gFnSc2
Unity	unita	k1gMnSc2
is	is	k?
driving	driving	k1gInSc1
automakers	automakers	k1gInSc1
toward	toward	k1gInSc1
virtual	virtual	k1gInSc4
reality	realita	k1gFnSc2
<g/>
.	.	kIx.
www.digitaltrends.com	www.digitaltrends.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
17.05	17.05	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
22.01	22.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ORESKOVIC	ORESKOVIC	kA
<g/>
,	,	kIx,
Alexei	Alexei	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnSc1
EA	EA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
former	former	k1gMnSc1
boss	boss	k1gMnSc1
believes	believes	k1gMnSc1
the	the	k?
3D	3D	k4
tech	tech	k?
that	that	k2eAgInSc1d1
powers	powers	k1gInSc1
video	video	k1gNnSc1
games	games	k1gInSc1
will	will	k1gMnSc1
make	make	k1gFnPc2
way	way	k?
more	mor	k1gInSc5
money	money	k1gInPc1
outside	outsid	k1gInSc5
of	of	k?
gaming	gaming	k1gInSc1
<g/>
.	.	kIx.
www.businessinsider.com	www.businessinsider.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Business	business	k1gInSc4
Insider	insider	k1gMnSc1
<g/>
,	,	kIx,
14.08	14.08	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
22.01	22.01	k4
<g/>
.2021	.2021	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
