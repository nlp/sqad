<p>
<s>
Tim	Tim	k?	Tim
Bergling	Bergling	k1gInSc1	Bergling
<g/>
,	,	kIx,	,
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
Avicii	Avicie	k1gFnSc4	Avicie
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1989	[number]	k4	1989
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
Maskat	Maskat	k1gInSc1	Maskat
<g/>
,	,	kIx,	,
Omán	Omán	k1gInSc1	Omán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
švédský	švédský	k2eAgInSc1d1	švédský
DJ	DJ	kA	DJ
nominovaný	nominovaný	k2eAgInSc1d1	nominovaný
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
remixer	remixer	k1gMnSc1	remixer
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
debutový	debutový	k2eAgInSc1d1	debutový
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Seek	Seek	k1gInSc1	Seek
Bromance	Bromanka	k1gFnSc3	Bromanka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
Top	topit	k5eAaImRp2nS	topit
20	[number]	k4	20
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
a	a	k8xC	a
rodného	rodný	k2eAgNnSc2d1	rodné
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vydal	vydat	k5eAaPmAgInS	vydat
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Levels	Levels	k1gInSc1	Levels
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
okamžitě	okamžitě	k6eAd1	okamžitě
obsadil	obsadit	k5eAaPmAgMnS	obsadit
žebříčky	žebříček	k1gInPc7	žebříček
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
také	také	k9	také
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Top	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
Dýdžejů	Dýdžej	k1gInPc2	Dýdžej
vyhlašovaným	vyhlašovaný	k2eAgInSc7d1	vyhlašovaný
časopisem	časopis	k1gInSc7	časopis
DJ	DJ	kA	DJ
Mag	Maga	k1gFnPc2	Maga
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
držel	držet	k5eAaImAgInS	držet
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
příčkách	příčka	k1gFnPc6	příčka
tohoto	tento	k3xDgInSc2	tento
prestižního	prestižní	k2eAgInSc2d1	prestižní
časopisu	časopis	k1gInSc2	časopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
hudbu	hudba	k1gFnSc4	hudba
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
v	v	k7c6	v
počítačovém	počítačový	k2eAgInSc6d1	počítačový
programu	program	k1gInSc6	program
FL	FL	kA	FL
Studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
oblíben	oblíben	k2eAgInSc1d1	oblíben
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1989	[number]	k4	1989
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kompilací	kompilace	k1gFnPc2	kompilace
remixu	remix	k1gInSc2	remix
titulního	titulní	k2eAgInSc2d1	titulní
motivu	motiv	k1gInSc2	motiv
videohry	videohra	k1gFnSc2	videohra
Lazy	Lazy	k?	Lazy
Jones	Jones	k1gMnSc1	Jones
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
jeho	jeho	k3xOp3gFnSc2	jeho
první	první	k4xOgFnSc2	první
nahrávky	nahrávka	k1gFnSc2	nahrávka
ve	v	k7c6	v
Strike	Strike	k1gNnSc6	Strike
Recordings	Recordingsa	k1gFnPc2	Recordingsa
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
"	"	kIx"	"
<g/>
Lazy	Lazy	k?	Lazy
Lace	laka	k1gFnSc6	laka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	s	k7c7	s
švédským	švédský	k2eAgNnSc7d1	švédské
DJ	DJ	kA	DJ
Johnem	John	k1gMnSc7	John
Dählbäckem	Dählbäcko	k1gNnSc7	Dählbäcko
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
song	song	k1gInSc4	song
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
"	"	kIx"	"
<g/>
Don	Don	k1gInSc4	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Hold	hold	k1gInSc1	hold
Back	Back	k1gInSc1	Back
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
se	s	k7c7	s
světoznámými	světoznámý	k2eAgFnPc7d1	světoznámá
DJi	DJi	k1gFnPc7	DJi
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Tiësto	Tiësta	k1gMnSc5	Tiësta
a	a	k8xC	a
Sebastian	Sebastian	k1gMnSc1	Sebastian
Ingrosso	Ingrossa	k1gFnSc5	Ingrossa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
zejména	zejména	k9	zejména
na	na	k7c4	na
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
EMI	EMI	kA	EMI
vydalo	vydat	k5eAaPmAgNnS	vydat
vokálovou	vokálový	k2eAgFnSc4d1	vokálová
verzi	verze	k1gFnSc4	verze
songu	song	k1gInSc2	song
"	"	kIx"	"
<g/>
Bromance	Bromanka	k1gFnSc6	Bromanka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
"	"	kIx"	"
<g/>
Seek	Seek	k1gInSc1	Seek
Bromance	Bromanka	k1gFnSc3	Bromanka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vydal	vydat	k5eAaPmAgInS	vydat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Guettou	Guetta	k1gMnSc7	Guetta
song	song	k1gInSc4	song
"	"	kIx"	"
<g/>
Sunshine	Sunshin	k1gMnSc5	Sunshin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
taneční	taneční	k2eAgFnSc1d1	taneční
skladba	skladba	k1gFnSc1	skladba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
také	také	k9	také
vydal	vydat	k5eAaPmAgMnS	vydat
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Levels	Levels	k1gInSc4	Levels
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
hned	hned	k6eAd1	hned
několika	několik	k4yIc2	několik
verzí	verze	k1gFnPc2	verze
a	a	k8xC	a
vynesla	vynést	k5eAaPmAgFnS	vynést
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
Avicii	Avicie	k1gFnSc6	Avicie
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
True	Tru	k1gInSc2	Tru
s	s	k7c7	s
přední	přední	k2eAgFnSc7d1	přední
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Wake	Wake	k1gFnSc1	Wake
Me	Me	k1gFnSc1	Me
Up	Up	k1gFnSc1	Up
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Aloe	aloe	k1gFnSc1	aloe
Blacc	Blacc	k1gFnSc1	Blacc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úmrtí	úmrtí	k1gNnPc1	úmrtí
==	==	k?	==
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
Bergling	Bergling	k1gInSc4	Bergling
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mrtvý	mrtvý	k1gMnSc1	mrtvý
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hotelové	hotelový	k2eAgFnSc6d1	hotelová
místnosti	místnost	k1gFnSc6	místnost
v	v	k7c6	v
ománském	ománský	k2eAgInSc6d1	ománský
Maskatu	Maskat	k1gInSc6	Maskat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
trávil	trávit	k5eAaImAgMnS	trávit
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
čerpal	čerpat	k5eAaImAgMnS	čerpat
energii	energie	k1gFnSc4	energie
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
ománská	ománský	k2eAgFnSc1d1	ománská
policie	policie	k1gFnSc1	policie
smrt	smrt	k1gFnSc1	smrt
následkem	následkem	k7c2	následkem
spáchání	spáchání	k1gNnSc2	spáchání
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
pak	pak	k6eAd1	pak
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bergling	Bergling	k1gInSc1	Bergling
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohl	moct	k5eNaImAgMnS	moct
nalézt	nalézt	k5eAaPmF	nalézt
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
nalézt	nalézt	k5eAaBmF	nalézt
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgMnSc2	první
května	květen	k1gInSc2	květen
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčina	příčina	k1gFnSc1	příčina
smrti	smrt	k1gFnSc2	smrt
Berglinga	Berglinga	k1gFnSc1	Berglinga
byla	být	k5eAaImAgFnS	být
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
úmyslně	úmyslně	k6eAd1	úmyslně
pořezal	pořezat	k5eAaPmAgInS	pořezat
skleněným	skleněný	k2eAgInSc7d1	skleněný
střepem	střep	k1gInSc7	střep
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vysoké	vysoký	k2eAgFnSc2d1	vysoká
ztráty	ztráta	k1gFnSc2	ztráta
krve	krev	k1gFnSc2	krev
zranění	zranění	k1gNnSc2	zranění
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Ománu	Omán	k1gInSc2	Omán
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
dokončení	dokončení	k1gNnSc6	dokončení
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
krátký	krátký	k2eAgInSc4d1	krátký
odpočinek	odpočinek	k1gInSc4	odpočinek
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
však	však	k9	však
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
nestihl	stihnout	k5eNaPmAgMnS	stihnout
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
Geffen	Geffna	k1gFnPc2	Geffna
Records	Records	k1gInSc1	Records
Neil	Neil	k1gMnSc1	Neil
Jacobson	Jacobson	k1gMnSc1	Jacobson
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
Tim	Tim	k?	Tim
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
již	již	k6eAd1	již
od	od	k7c2	od
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Levels	Levels	k1gInSc1	Levels
<g/>
"	"	kIx"	"
pronesl	pronést	k5eAaPmAgInS	pronést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hudbu	hudba	k1gFnSc4	hudba
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5.4	[number]	k4	5.4
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
vydání	vydání	k1gNnSc4	vydání
alba	album	k1gNnSc2	album
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
"	"	kIx"	"
<g/>
Tim	Tim	k?	Tim
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vybrala	vybrat	k5eAaPmAgFnS	vybrat
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
(	(	kIx(	(
<g/>
rozdělané	rozdělaný	k2eAgFnPc4d1	rozdělaná
písničky	písnička	k1gFnPc4	písnička
dodělali	dodělat	k5eAaPmAgMnP	dodělat
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
<g/>
)	)	kIx)	)
poslední	poslední	k2eAgFnSc7d1	poslední
vydanou	vydaný	k2eAgFnSc7d1	vydaná
skladbou	skladba	k1gFnSc7	skladba
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Tough	Tough	k1gInSc1	Tough
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
vyšla	vyjít	k5eAaPmAgFnS	vyjít
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
poslední	poslední	k2eAgFnSc1d1	poslední
skladba	skladba	k1gFnSc1	skladba
na	na	k7c4	na
které	který	k3yRgMnPc4	který
Tim	Tim	k?	Tim
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
pracoval	pracovat	k5eAaImAgMnS	pracovat
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
"	"	kIx"	"
<g/>
SOS	sos	k1gInSc4	sos
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Aloe	aloe	k1gFnSc1	aloe
Blacc	Blacc	k1gFnSc1	Blacc
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
úmrtí	úmrtí	k1gNnSc6	úmrtí
označil	označit	k5eAaPmAgMnS	označit
Armin	Armin	k2eAgInSc4d1	Armin
van	van	k1gInSc4	van
Buuren	Buurno	k1gNnPc2	Buurno
Berglinga	Bergling	k1gMnSc2	Bergling
za	za	k7c7	za
"	"	kIx"	"
<g/>
Mozarta	Mozart	k1gMnSc2	Mozart
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	album	k1gNnSc2	album
===	===	k?	===
</s>
</p>
<p>
<s>
The	The	k?	The
Singles	Singles	k1gInSc1	Singles
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
True	True	k1gFnSc1	True
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
True	Truat	k5eAaPmIp3nS	Truat
(	(	kIx(	(
<g/>
Avicii	Avicie	k1gFnSc4	Avicie
by	by	k9	by
Avicii	Avicie	k1gFnSc4	Avicie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stories	Stories	k1gInSc1	Stories
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Avī	Avī	k?	Avī
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TIM	TIM	k?	TIM
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Skladby	skladba	k1gFnSc2	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sound	Sound	k1gMnSc1	Sound
of	of	k?	of
Now	Now	k1gMnSc1	Now
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Muja	Mujum	k1gNnPc4	Mujum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Alcoholic	Alcoholice	k1gFnPc2	Alcoholice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ryu	Ryu	k1gFnSc6	Ryu
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Even	Even	k1gInSc4	Even
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Sebastien	Sebastien	k2eAgInSc1d1	Sebastien
Drums	Drums	k1gInSc1	Drums
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Break	break	k1gInSc1	break
da	da	k?	da
Floor	Floor	k1gInSc1	Floor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Ralph	Ralph	k1gMnSc1	Ralph
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Feelings	Feelings	k1gInSc1	Feelings
for	forum	k1gNnPc2	forum
You	You	k1gFnSc2	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Sebastien	Sebastien	k2eAgInSc1d1	Sebastien
Drums	Drums	k1gInSc1	Drums
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Insomnia	Insomnium	k1gNnPc4	Insomnium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Starkillers	Starkillers	k1gInSc1	Starkillers
<g/>
,	,	kIx,	,
Pimprockers	Pimprockers	k1gInSc1	Pimprockers
&	&	k?	&
Marco	Marco	k1gMnSc1	Marco
Machiavelli	Machiavelli	k1gMnSc1	Machiavelli
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Hold	hold	k1gInSc1	hold
Back	Back	k1gInSc1	Back
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Dahlbäck	Dahlbäck	k1gMnSc1	Dahlbäck
[	[	kIx(	[
<g/>
jako	jako	k8xC	jako
Jovicii	Jovicie	k1gFnSc4	Jovicie
<g/>
]	]	kIx)	]
&	&	k?	&
Andy	Anda	k1gFnPc4	Anda
P.	P.	kA	P.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bom	Bom	k1gFnSc6	Bom
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Seek	Seek	k1gInSc1	Seek
Bromance	Bromanka	k1gFnSc3	Bromanka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Malo	Malo	k1gMnSc1	Malo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Bebe	Bebat	k5eAaPmIp3nS	Bebat
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Enough	Enough	k1gMnSc1	Enough
is	is	k?	is
Enough	Enough	k1gMnSc1	Enough
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Give	Give	k1gNnSc2	Give
Up	Up	k1gMnSc2	Up
On	on	k3xPp3gMnSc1	on
Us	Us	k1gMnSc1	Us
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Tweet	Tweet	k1gMnSc1	Tweet
It	It	k1gMnSc1	It
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Norman	Norman	k1gMnSc1	Norman
Doray	Doraa	k1gFnSc2	Doraa
&	&	k?	&
Sebastien	Sebastien	k2eAgInSc4d1	Sebastien
Drums	Drums	k1gInSc4	Drums
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sweet	Sweeta	k1gFnPc2	Sweeta
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
iTrack	iTrack	k1gInSc4	iTrack
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
Ingrosso	Ingrossa	k1gFnSc5	Ingrossa
&	&	k?	&
Otto	Otto	k1gMnSc1	Otto
Knows	Knows	k1gInSc1	Knows
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Snus	Snus	k1gInSc4	Snus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Sebastien	Sebastien	k2eAgInSc1d1	Sebastien
Drums	Drums	k1gInSc1	Drums
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Street	Street	k1gMnSc1	Street
Dancer	Dancer	k1gMnSc1	Dancer
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Jailbait	Jailbait	k2eAgMnSc1d1	Jailbait
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Fade	Fade	k1gInSc1	Fade
into	into	k1gNnSc1	into
Darkness	Darkness	k1gInSc1	Darkness
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Andreas	Andreas	k1gMnSc1	Andreas
Moe	Moe	k1gMnSc1	Moe
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Collide	Collid	k1gInSc5	Collid
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Leona	Leona	k1gFnSc1	Leona
Lewis	Lewis	k1gFnSc1	Lewis
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Levels	Levels	k1gInSc4	Levels
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Blessed	Blessed	k1gInSc4	Blessed
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Shermanology	Shermanolog	k1gMnPc4	Shermanolog
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Silhouettes	Silhouettes	k1gInSc4	Silhouettes
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Last	Last	k1gInSc1	Last
Dance	Danka	k1gFnSc3	Danka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Dancing	dancing	k1gInSc1	dancing
In	In	k1gFnSc2	In
My	my	k3xPp1nPc1	my
Head	Head	k1gInSc4	Head
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k1gFnSc1	Eric
Turner	turner	k1gMnSc1	turner
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sunshine	Sunshin	k1gInSc5	Sunshin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Guetta	Guetta	k1gMnSc1	Guetta
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
Miami	Miami	k1gNnSc2	Miami
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Superlove	Superlov	k1gInSc5	Superlov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Lenny	Lenny	k?	Lenny
Kravitz	Kravitz	k1gInSc1	Kravitz
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Wake	Wak	k1gMnSc4	Wak
Me	Me	k1gMnSc4	Me
Up	Up	k1gMnSc4	Up
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
I	i	k9	i
Could	Could	k1gMnSc1	Could
Be	Be	k1gMnSc1	Be
The	The	k1gMnSc1	The
One	One	k1gMnSc1	One
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Nicky	nicka	k1gFnPc1	nicka
Romero	Romero	k1gNnSc1	Romero
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
X	X	kA	X
You	You	k1gFnSc6	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
spolupráci	spolupráce	k1gFnSc4	spolupráce
mnoha	mnoho	k4c2	mnoho
amatérských	amatérský	k2eAgMnPc2d1	amatérský
skladatelů	skladatel	k1gMnPc2	skladatel
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
We	We	k1gMnSc5	We
Write	Writ	k1gMnSc5	Writ
The	The	k1gFnSc3	The
Story	story	k1gFnPc3	story
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
<g/>
&	&	k?	&
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Wake	Wak	k1gMnSc4	Wak
Me	Me	k1gMnSc4	Me
Up	Up	k1gMnSc4	Up
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Aloe	aloe	k1gFnSc1	aloe
Blacc	Blacc	k1gFnSc1	Blacc
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Liar	Liar	k1gMnSc1	Liar
Liar	Liar	k1gMnSc1	Liar
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
You	You	k1gMnSc4	You
Make	Mak	k1gMnSc4	Mak
Me	Me	k1gMnSc4	Me
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Salem	Salem	k1gInSc1	Salem
Al	ala	k1gFnPc2	ala
Fakir	Fakira	k1gFnPc2	Fakira
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hey	Hey	k1gFnSc3	Hey
Brother	Brother	kA	Brother
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Addicted	Addicted	k1gInSc4	Addicted
To	to	k9	to
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Audra	Audra	k1gMnSc1	Audra
Mae	Mae	k1gMnSc1	Mae
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lay	Lay	k1gMnSc1	Lay
Me	Me	k1gMnSc1	Me
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gFnPc2	The
Days	Days	k1gInSc1	Days
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Robbie	Robbie	k1gFnSc1	Robbie
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gFnPc2	The
Nights	Nights	k1gInSc1	Nights
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Feeling	Feeling	k1gInSc1	Feeling
Good	Good	k1gInSc1	Good
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Puzzle	puzzle	k1gNnSc3	puzzle
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Sunset	Sunset	k1gMnSc1	Sunset
Jesus	Jesus	k1gMnSc1	Jesus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Waiting	Waiting	k1gInSc1	Waiting
For	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
For	forum	k1gNnPc2	forum
A	a	k8xC	a
Better	Bettra	k1gFnPc2	Bettra
Day	Day	k1gFnSc2	Day
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Taste	tasit	k5eAaPmRp2nP	tasit
The	The	k1gFnSc4	The
Feeling	Feeling	k1gInSc1	Feeling
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Conrad	Conrad	k1gInSc1	Conrad
Sewell	Sewell	k1gInSc4	Sewell
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Back	Back	k1gInSc1	Back
Where	Wher	k1gInSc5	Wher
I	i	k9	i
Belong	Belonga	k1gFnPc2	Belonga
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Knows	Knowsa	k1gFnPc2	Knowsa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Without	Without	k1gMnSc1	Without
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Sandro	Sandra	k1gFnSc5	Sandra
Cavazza	Cavazz	k1gMnSc4	Cavazz
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Lonely	Lonel	k1gInPc4	Lonel
Together	Togethra	k1gFnPc2	Togethra
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Rita	Rita	k1gFnSc1	Rita
Ora	Ora	k1gFnSc1	Ora
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Friend	Friend	k1gMnSc1	Friend
Of	Of	k1gMnSc1	Of
Mine	minout	k5eAaImIp3nS	minout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Vargas	Vargas	k1gInSc1	Vargas
&	&	k?	&
Lagola	Lagola	k1gFnSc1	Lagola
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
You	You	k1gMnSc5	You
Be	Be	k1gMnSc5	Be
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Billy	Bill	k1gMnPc4	Bill
Raffoul	Raffoula	k1gFnPc2	Raffoula
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
What	What	k2eAgInSc1d1	What
Would	Would	k1gInSc1	Would
I	i	k8xC	i
Change	change	k1gFnSc1	change
It	It	k1gFnSc2	It
To	to	k9	to
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
AlunaGeorge	AlunaGeorgat	k5eAaPmIp3nS	AlunaGeorgat
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
SOS	sos	k1gInSc4	sos
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Aloe	aloe	k1gFnSc1	aloe
Blacc	Blacc	k1gFnSc1	Blacc
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Tough	Tough	k1gInSc1	Tough
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Agnes	Agnes	k1gMnSc1	Agnes
<g/>
,	,	kIx,	,
Vargas	Vargas	k1gMnSc1	Vargas
&	&	k?	&
Lagola	Lagola	k1gFnSc1	Lagola
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Avicii	Avicie	k1gFnSc6	Avicie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
iTunes	iTunes	k1gMnSc1	iTunes
podcast	podcast	k1gMnSc1	podcast
</s>
</p>
<p>
<s>
Avicii	Avicie	k1gFnSc4	Avicie
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
