<p>
<s>
Fender	Fender	k1gInSc1	Fender
Musical	musical	k1gInSc1	musical
Instruments	Instruments	kA	Instruments
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Fender	Fender	k1gMnSc1	Fender
Electric	Electric	k1gMnSc1	Electric
Instrument	instrument	k1gInSc4	instrument
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
Company	Compana	k1gFnSc2	Compana
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
výrobce	výrobce	k1gMnSc1	výrobce
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
firmu	firma	k1gFnSc4	firma
založil	založit	k5eAaPmAgMnS	založit
Američan	Američan	k1gMnSc1	Američan
Leo	Leo	k1gMnSc1	Leo
Fender	Fender	k1gMnSc1	Fender
<g/>
.	.	kIx.	.
</s>
<s>
Původě	Původě	k6eAd1	Původě
byl	být	k5eAaImAgInS	být
opravářem	opravář	k1gMnSc7	opravář
automobilových	automobilový	k2eAgFnPc2d1	automobilová
rádií	rádio	k1gNnPc2	rádio
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
kytary	kytara	k1gFnPc4	kytara
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
dovedlo	dovést	k5eAaPmAgNnS	dovést
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
výrobě	výroba	k1gFnSc3	výroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
a	a	k8xC	a
nejpoužívanějších	používaný	k2eAgInPc2d3	nejpoužívanější
typů	typ	k1gInPc2	typ
kytar	kytara	k1gFnPc2	kytara
–	–	k?	–
Stratocaster	Stratocastra	k1gFnPc2	Stratocastra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
Fender	Fendra	k1gFnPc2	Fendra
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
kytarami	kytara	k1gFnPc7	kytara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
také	také	k9	také
basové	basový	k2eAgFnPc4d1	basová
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
kytarová	kytarový	k2eAgFnSc1d1	kytarová
komba	komba	k1gFnSc1	komba
a	a	k8xC	a
kytarové	kytarový	k2eAgNnSc1d1	kytarové
příslušenství	příslušenství	k1gNnSc1	příslušenství
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
oddělení	oddělení	k1gNnPc2	oddělení
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Fender	Fender	k1gInSc1	Fender
své	svůj	k3xOyFgInPc4	svůj
produkty	produkt	k1gInPc4	produkt
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ručně	ručně	k6eAd1	ručně
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
starší	starý	k2eAgInSc1d2	starší
název	název	k1gInSc1	název
Fender	Fendra	k1gFnPc2	Fendra
Electric	Electrice	k1gFnPc2	Electrice
Instrument	instrument	k1gInSc1	instrument
Manufacturing	Manufacturing	k1gInSc4	Manufacturing
Company	Compana	k1gFnSc2	Compana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
mají	mít	k5eAaImIp3nP	mít
sice	sice	k8xC	sice
vyšší	vysoký	k2eAgFnSc4d2	vyšší
cenu	cena	k1gFnSc4	cena
ale	ale	k8xC	ale
i	i	k9	i
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
stratocasteru	stratocaster	k1gInSc2	stratocaster
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
starší	starý	k2eAgInSc4d2	starší
model	model	k1gInSc4	model
telecaster	telecastra	k1gFnPc2	telecastra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
baskytar	baskytara	k1gFnPc2	baskytara
např.	např.	kA	např.
Fender	Fender	k1gInSc1	Fender
Jazz	jazz	k1gInSc1	jazz
Bass	Bass	k1gMnSc1	Bass
a	a	k8xC	a
Fender	Fender	k1gMnSc1	Fender
Precision	Precision	k1gInSc4	Precision
Bass	Bass	k1gMnSc1	Bass
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stratocastery	Stratocaster	k1gInPc1	Stratocaster
mívají	mívat	k5eAaImIp3nP	mívat
3	[number]	k4	3
jednocívkové	jednocívkový	k2eAgInPc1d1	jednocívkový
(	(	kIx(	(
<g/>
single	singl	k1gInSc5	singl
coil	coil	k1gInSc4	coil
<g/>
)	)	kIx)	)
snímače	snímač	k1gInSc2	snímač
a	a	k8xC	a
hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
na	na	k7c4	na
jemnější	jemný	k2eAgInPc4d2	jemnější
styly	styl	k1gInPc4	styl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
varianty	varianta	k1gFnPc1	varianta
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
humbuckerem	humbucker	k1gInSc7	humbucker
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Les	les	k1gInSc4	les
Paulem	Paul	k1gMnSc7	Paul
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejběžnějším	běžný	k2eAgInPc3d3	nejběžnější
modelům	model	k1gInPc3	model
<g/>
.	.	kIx.	.
</s>
<s>
Výrobky	výrobek	k1gInPc4	výrobek
Fender	Fendra	k1gFnPc2	Fendra
používá	používat	k5eAaImIp3nS	používat
<g/>
/	/	kIx~	/
<g/>
vala	val	k2eAgFnSc1d1	vala
spousta	spousta	k1gFnSc1	spousta
známých	známý	k2eAgMnPc2d1	známý
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patřili	patřit	k5eAaImAgMnP	patřit
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
John	John	k1gMnSc1	John
Paul	Paul	k1gMnSc1	Paul
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Murray	Murra	k2eAgMnPc4d1	Murra
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc4	Eric
Clapton	Clapton	k1gInSc4	Clapton
<g/>
,	,	kIx,	,
Keith	Keith	k1gInSc4	Keith
Richards	Richardsa	k1gFnPc2	Richardsa
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
světoznámých	světoznámý	k2eAgMnPc2d1	světoznámý
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Fender	Fender	k1gMnSc1	Fender
Stratocaster	Stratocaster	k1gMnSc1	Stratocaster
</s>
</p>
<p>
<s>
Fender	Fender	k1gMnSc1	Fender
Telecaster	Telecaster	k1gMnSc1	Telecaster
</s>
</p>
<p>
<s>
Fender	Fender	k1gInSc1	Fender
Jazz	jazz	k1gInSc1	jazz
Bass	Bass	k1gMnSc1	Bass
</s>
</p>
<p>
<s>
Fender	Fender	k1gInSc1	Fender
Precision	Precision	k1gInSc1	Precision
Bass	Bass	k1gMnSc1	Bass
</s>
</p>
<p>
<s>
Leo	Leo	k1gMnSc1	Leo
Fender	Fender	k1gMnSc1	Fender
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fender	Fender	k1gInSc1	Fender
Musical	musical	k1gInSc4	musical
Instruments	Instruments	kA	Instruments
Corporation	Corporation	k1gInSc1	Corporation
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
firmě	firma	k1gFnSc6	firma
Fender	Fendra	k1gFnPc2	Fendra
</s>
</p>
<p>
<s>
Elektrická	elektrický	k2eAgNnPc1d1	elektrické
piana	piano	k1gNnPc1	piano
Fender-Rhodes	Fender-Rhodesa	k1gFnPc2	Fender-Rhodesa
-	-	kIx~	-
historie	historie	k1gFnSc1	historie
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
legendárního	legendární	k2eAgNnSc2d1	legendární
piana	piano	k1gNnSc2	piano
</s>
</p>
<p>
<s>
Fender	Fender	k1gInSc4	Fender
manufacturing	manufacturing	k1gInSc1	manufacturing
video	video	k1gNnSc1	video
</s>
</p>
