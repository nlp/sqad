<s>
Firma	firma	k1gFnSc1	firma
Fender	Fendra	k1gFnPc2	Fendra
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
kytarami	kytara	k1gFnPc7	kytara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
také	také	k9	také
basové	basový	k2eAgFnPc4d1	basová
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
kytarová	kytarový	k2eAgFnSc1d1	kytarová
komba	komba	k1gFnSc1	komba
a	a	k8xC	a
kytarové	kytarový	k2eAgNnSc1d1	kytarové
příslušenství	příslušenství	k1gNnSc1	příslušenství
<g/>
.	.	kIx.	.
</s>
