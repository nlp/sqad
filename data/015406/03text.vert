<s>
Tarso	Tarsa	k1gFnSc5
Voon	Voona	k1gFnPc2
</s>
<s>
Tarso	Tarsa	k1gFnSc5
Voon	Voon	k1gMnSc1
Vrchol	vrchol	k1gInSc1
</s>
<s>
3100	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Čad	Čad	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Tarso	Tarsa	k1gFnSc5
Voon	Voon	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tarso	Tarso	k1gNnSc1
Voon	Voon	k1gNnSc1
je	být	k5eAaImIp3nS
3100	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc4d1
stratovulkán	stratovulkán	k1gInSc4
na	na	k7c6
severu	sever	k1gInSc6
Čadu	Čad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hora	Hora	k1gMnSc1
se	se	k3xPyFc4
tyčí	tyčit	k5eAaImIp3nS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
pohoří	pohoří	k1gNnSc2
Tibesti	Tibest	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vrchol	vrchol	k1gInSc1
hory	hora	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
plochá	plochý	k2eAgFnSc1d1
kaldera	kaldera	k1gFnSc1
o	o	k7c6
rozměrech	rozměr	k1gInPc6
14	#num#	k4
x	x	k?
18	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsáhlé	rozsáhlý	k2eAgInPc1d1
výlevy	výlev	k1gInPc1
bazaltu	bazalt	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
její	její	k3xOp3gFnSc6
severovýchodu	severovýchod	k1gInSc2
části	část	k1gFnPc1
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nP
180	#num#	k4
<g/>
stupňový	stupňový	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
výsledkem	výsledek	k1gInSc7
vysoké	vysoký	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
stratovulkánu	stratovulkán	k1gInSc2
v	v	k7c6
kvartéru	kvartér	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
směru	směr	k1gInSc6
se	se	k3xPyFc4
poblíž	poblíž	k6eAd1
zvedá	zvedat	k5eAaImIp3nS
Ehi	Ehi	k1gFnSc1
Mosgau	Mosgaus	k1gInSc2
<g/>
,	,	kIx,
stratovulkán	stratovulkán	k1gInSc1
se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
<g/>
,	,	kIx,
3100	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Depozity	depozit	k1gInPc7
z	z	k7c2
pyroklastických	pyroklastický	k2eAgInPc2d1
oblaků	oblak	k1gInPc2
se	se	k3xPyFc4
prostírají	prostírat	k5eAaImIp3nP
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
15	#num#	k4
až	až	k9
35	#num#	k4
km	km	kA
od	od	k7c2
kaldery	kaldera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hora	hora	k1gFnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
prekambrijských	prekambrijský	k2eAgInPc6d1
svorech	svor	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vulkanické	vulkanický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
Soborom	Soborom	k1gInSc1
Solfataric	Solfatarice	k1gFnPc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
5	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
okraje	okraj	k1gInSc2
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Tibesti	Tibest	k1gFnSc3
největší	veliký	k2eAgFnSc3d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivní	aktivní	k2eAgFnPc4d1
fumaroly	fumarola	k1gFnPc4
<g/>
,	,	kIx,
bahenní	bahenní	k2eAgFnPc4d1
sopky	sopka	k1gFnPc4
a	a	k8xC
horké	horký	k2eAgInPc4d1
prameny	pramen	k1gInPc4
navštěvují	navštěvovat	k5eAaImIp3nP
obyvatelé	obyvatel	k1gMnPc1
pohoří	pohoří	k1gNnSc2
Tibesti	Tibest	k1gFnSc2
pro	pro	k7c4
lékařské	lékařský	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tarso	Tarsa	k1gFnSc5
Voon	Voono	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
Global	globat	k5eAaImAgInS
Volcanism	Volcanism	k1gInSc1
Program	program	k1gInSc1
|	|	kIx~
Tarso	Tarsa	k1gFnSc5
Voon	Voona	k1gFnPc2
<g/>
.	.	kIx.
volcano	volcana	k1gFnSc5
<g/>
.	.	kIx.
<g/>
si	se	k3xPyFc3
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
