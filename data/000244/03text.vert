<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
pětka	pětka	k1gFnSc1	pětka
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
režiséra	režisér	k1gMnSc2	režisér
Tomáše	Tomáš	k1gMnSc2	Tomáš
Vorla	Vorel	k1gMnSc2	Vorel
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
pět	pět	k4xCc4	pět
amatérských	amatérský	k2eAgNnPc2d1	amatérské
pražských	pražský	k2eAgNnPc2d1	Pražské
divadel	divadlo	k1gNnPc2	divadlo
v	v	k7c6	v
pěti	pět	k4xCc6	pět
filmových	filmový	k2eAgFnPc6d1	filmová
povídkách	povídka	k1gFnPc6	povídka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Směr	směr	k1gInSc1	směr
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Pantomimická	pantomimický	k2eAgFnSc1d1	pantomimická
skupina	skupina	k1gFnSc1	skupina
Mimóza	mimóza	k1gFnSc1	mimóza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
groteska	groteska	k1gFnSc1	groteska
o	o	k7c6	o
strastiplném	strastiplný	k2eAgInSc6d1	strastiplný
výletě	výlet	k1gInSc6	výlet
jedné	jeden	k4xCgFnSc2	jeden
pražské	pražský	k2eAgFnSc2d1	Pražská
rodinky	rodinka	k1gFnSc2	rodinka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bersidejsi	Bersidejse	k1gFnSc4	Bersidejse
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
divadlo	divadlo	k1gNnSc1	divadlo
Kolotoč	kolotoč	k1gInSc1	kolotoč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Oldův	Oldův	k2eAgInSc4d1	Oldův
večírek	večírek	k1gInSc4	večírek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Recitační	recitační	k2eAgFnSc1d1	recitační
skupina	skupina	k1gFnSc1	skupina
Vpřed	vpřed	k6eAd1	vpřed
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
satira	satira	k1gFnSc1	satira
o	o	k7c6	o
chlapci	chlapec	k1gMnSc6	chlapec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
údržbář	údržbář	k1gMnSc1	údržbář
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
sehnat	sehnat	k5eAaPmF	sehnat
těsnění	těsnění	k1gNnSc4	těsnění
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Barvy	barva	k1gFnSc2	barva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Baletní	baletní	k2eAgFnSc1d1	baletní
jednotka	jednotka	k1gFnSc1	jednotka
Křeč	křeč	k1gFnSc1	křeč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podobenstvím	podobenství	k1gNnSc7	podobenství
o	o	k7c6	o
ideologiích	ideologie	k1gFnPc6	ideologie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Na	na	k7c6	na
brigádě	brigáda	k1gFnSc6	brigáda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Sklep	sklep	k1gInSc1	sklep
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
parodie	parodie	k1gFnSc1	parodie
na	na	k7c4	na
budovatelské	budovatelský	k2eAgInPc4d1	budovatelský
filmy	film	k1gInPc4	film
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
-	-	kIx~	-
tramp	tramp	k1gMnSc1	tramp
(	(	kIx(	(
<g/>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanák	Hanák	k1gMnSc1	Hanák
<g/>
)	)	kIx)	)
-	-	kIx~	-
stopne	stopnout	k5eAaPmIp3nS	stopnout
autobus	autobus	k1gInSc1	autobus
plný	plný	k2eAgInSc1d1	plný
brigádníků	brigádník	k1gMnPc2	brigádník
<g/>
,	,	kIx,	,
jedoucích	jedoucí	k2eAgFnPc2d1	jedoucí
z	z	k7c2	z
města	město	k1gNnSc2	město
pomoci	pomoct	k5eAaPmF	pomoct
zemědělcům	zemědělec	k1gMnPc3	zemědělec
při	při	k7c6	při
žních	žeň	k1gFnPc6	žeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
všichni	všechen	k3xTgMnPc1	všechen
poperou	poprat	k5eAaPmIp3nP	poprat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tramp	tramp	k1gInSc1	tramp
se	se	k3xPyFc4	se
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
místní	místní	k2eAgFnSc7d1	místní
učitelkou	učitelka	k1gFnSc7	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
části	část	k1gFnPc4	část
uvádí	uvádět	k5eAaImIp3nS	uvádět
Milan	Milan	k1gMnSc1	Milan
Šteindler	Šteindler	k1gMnSc1	Šteindler
jako	jako	k8xS	jako
lektor	lektor	k1gMnSc1	lektor
<g/>
.	.	kIx.	.
</s>
<s>
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
<g/>
:	:	kIx,	:
barevný	barevný	k2eAgInSc1d1	barevný
<g/>
,	,	kIx,	,
97	[number]	k4	97
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
Námět	námět	k1gInSc1	námět
a	a	k8xC	a
scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vorel	Vorel	k1gMnSc1	Vorel
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Vávra	Vávra	k1gMnSc1	Vávra
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Čestmír	Čestmír	k1gMnSc1	Čestmír
Suška	suška	k1gMnSc1	suška
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gMnSc1	Lumír
Tuček	Tuček	k1gMnSc1	Tuček
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Caban	Caban	k1gMnSc1	Caban
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanák	Hanák	k1gMnSc1	Hanák
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Burda	Burda	k1gMnSc1	Burda
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Babuljak	Babuljak	k1gMnSc1	Babuljak
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Richter	Richter	k1gMnSc1	Richter
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Diviš	Diviš	k1gMnSc1	Diviš
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Kopřivík	Kopřivík	k1gMnSc1	Kopřivík
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Vích	Vích	k1gMnSc1	Vích
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Chlumecký	chlumecký	k2eAgMnSc1d1	chlumecký
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Noll	Noll	k1gMnSc1	Noll
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vaculík	Vaculík	k1gMnSc1	Vaculík
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Podzimek	Podzimek	k1gMnSc1	Podzimek
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Roman	Roman	k1gMnSc1	Roman
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Wieser	Wieser	k1gMnSc1	Wieser
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Režie	režie	k1gFnSc1	režie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vorel	Vorel	k1gMnSc1	Vorel
Střih	střih	k1gInSc1	střih
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Brožek	Brožek	k1gMnSc1	Brožek
Zvuk	zvuk	k1gInSc1	zvuk
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Potůček	Potůček	k1gMnSc1	Potůček
Architekt	architekt	k1gMnSc1	architekt
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
Široký	Široký	k1gMnSc1	Široký
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Vaněk	Vaněk	k1gMnSc1	Vaněk
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Scénografie	scénografie	k1gFnSc1	scénografie
a	a	k8xC	a
choreografie	choreografie	k1gFnSc1	choreografie
<g/>
:	:	kIx,	:
Čestmír	Čestmír	k1gMnSc1	Čestmír
Suška	suška	k1gMnSc1	suška
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šimon	Šimon	k1gMnSc1	Šimon
Caban	Caban	k1gMnSc1	Caban
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Návrhy	návrh	k1gInPc1	návrh
kostýmů	kostým	k1gInPc2	kostým
<g/>
:	:	kIx,	:
Simona	Simona	k1gFnSc1	Simona
Rybáková	Rybáková	k1gFnSc1	Rybáková
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Michálková	Michálková	k1gFnSc1	Michálková
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Masky	maska	k1gFnSc2	maska
<g/>
:	:	kIx,	:
Stanislav	Stanislav	k1gMnSc1	Stanislav
Petřek	Petřka	k1gFnPc2	Petřka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Budín	Budín	k1gMnSc1	Budín
Režijní	režijní	k2eAgFnSc2d1	režijní
supervize	supervize	k1gFnSc2	supervize
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
Přemysl	Přemysl	k1gMnSc1	Přemysl
Pražský	pražský	k2eAgMnSc1d1	pražský
Dramaturgie	dramaturgie	k1gFnPc4	dramaturgie
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Tintěra	tintěra	k1gMnSc1	tintěra
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
Hofmanová	Hofmanová	k1gFnSc1	Hofmanová
Nahráli	nahrát	k5eAaPmAgMnP	nahrát
<g/>
:	:	kIx,	:
Richter	Richter	k1gMnSc1	Richter
a	a	k8xC	a
Pečírka	Pečírka	k1gFnSc1	Pečírka
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krásné	krásný	k2eAgInPc1d1	krásný
nové	nový	k2eAgInPc1d1	nový
stroje	stroj	k1gInPc1	stroj
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
Vpřed	vpřed	k6eAd1	vpřed
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pacocamino	Pacocamin	k2eAgNnSc1d1	Pacocamin
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
<g/>
:	:	kIx,	:
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1989	[number]	k4	1989
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
,	,	kIx,	,
premiéru	premiéra	k1gFnSc4	premiéra
provázela	provázet	k5eAaImAgFnS	provázet
hudební	hudební	k2eAgFnSc1d1	hudební
show	show	k1gFnSc1	show
skupiny	skupina	k1gFnSc2	skupina
Laura	Laura	k1gFnSc1	Laura
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
tygři	tygr	k1gMnPc1	tygr
Milan	Milan	k1gMnSc1	Milan
Šteindler	Šteindler	k1gMnSc1	Šteindler
(	(	kIx(	(
<g/>
lektor	lektor	k1gMnSc1	lektor
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Směr	směr	k1gInSc1	směr
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Milan	Milan	k1gMnSc1	Milan
Šteindler	Šteindler	k1gMnSc1	Šteindler
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milan	Milan	k1gMnSc1	Milan
Šteindler	Šteindler	k1gMnSc1	Šteindler
CSc	CSc	kA	CSc
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Holubová	Holubová	k1gFnSc1	Holubová
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Vávra	Vávra	k1gMnSc1	Vávra
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Pfeifrová	Pfeifrová	k1gFnSc1	Pfeifrová
(	(	kIx(	(
<g/>
holčička	holčička	k1gFnSc1	holčička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Prágr	Prágr	k1gMnSc1	Prágr
(	(	kIx(	(
<g/>
kluk	kluk	k1gMnSc1	kluk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Prágr	Prágr	k1gMnSc1	Prágr
(	(	kIx(	(
<g/>
kluk	kluk	k1gMnSc1	kluk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vorel	Vorel	k1gMnSc1	Vorel
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
skřítek	skřítek	k1gMnSc1	skřítek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Vnoučková	Vnoučková	k1gFnSc1	Vnoučková
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Dejdar	Dejdar	k1gMnSc1	Dejdar
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dina	Dina	k1gFnSc1	Dina
Chourová	Chourový	k2eAgFnSc1d1	Chourová
(	(	kIx(	(
<g/>
holčička	holčička	k1gFnSc1	holčička
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Adam	Adam	k1gMnSc1	Adam
(	(	kIx(	(
<g/>
kluk	kluk	k1gMnSc1	kluk
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Veselý	Veselý	k1gMnSc1	Veselý
(	(	kIx(	(
<g/>
jedlík	jedlík	k1gMnSc1	jedlík
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Běhal	Běhal	k1gMnSc1	Běhal
(	(	kIx(	(
<g/>
hostinský	hostinský	k1gMnSc1	hostinský
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Bersidejsi	Bersidejse	k1gFnSc4	Bersidejse
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Lenka	Lenka	k1gFnSc1	Lenka
Vychodilová	Vychodilová	k1gFnSc1	Vychodilová
(	(	kIx(	(
<g/>
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Slovák	Slovák	k1gMnSc1	Slovák
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
Najbrt	Najbrt	k1gInSc1	Najbrt
<g/>
,	,	kIx,	,
Čestmír	Čestmír	k1gMnSc1	Čestmír
Suška	suška	k1gMnSc1	suška
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
Pečírka	Pečírka	k1gFnSc1	Pečírka
<g/>
,	,	kIx,	,
Mojmír	Mojmír	k1gMnSc1	Mojmír
Pukl	puknout	k5eAaPmAgMnS	puknout
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Švec	Švec	k1gMnSc1	Švec
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Michálková	Michálková	k1gFnSc1	Michálková
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
Roglová	Roglová	k1gFnSc1	Roglová
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
Wiechová	Wiechová	k1gFnSc1	Wiechová
<g/>
,	,	kIx,	,
Radana	Radana	k1gFnSc1	Radana
Bébarová	Bébarová	k1gFnSc1	Bébarová
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
Schmidtová	Schmidtová	k1gFnSc1	Schmidtová
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
Slámová	Slámová	k1gFnSc1	Slámová
(	(	kIx(	(
<g/>
tanečnice	tanečnice	k1gFnPc1	tanečnice
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Oldův	Oldův	k2eAgInSc4d1	Oldův
večírek	večírek	k1gInSc4	večírek
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Marek	Marek	k1gMnSc1	Marek
(	(	kIx(	(
<g/>
Olda	Olda	k1gMnSc1	Olda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Radim	Radim	k1gMnSc1	Radim
Vašinka	Vašinka	k1gFnSc1	Vašinka
(	(	kIx(	(
<g/>
mistr	mistr	k1gMnSc1	mistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gMnSc1	Lumír
Tuček	Tuček	k1gMnSc1	Tuček
(	(	kIx(	(
<g/>
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
Tesařová	Tesařová	k1gFnSc1	Tesařová
(	(	kIx(	(
<g/>
Princezna	princezna	k1gFnSc1	princezna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Tučková	Tučková	k1gFnSc1	Tučková
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
Víchová	Víchová	k1gFnSc1	Víchová
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
Vlachová	Vlachová	k1gFnSc1	Vlachová
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Svátková	Svátková	k1gFnSc1	Svátková
(	(	kIx(	(
<g/>
pokojské	pokojská	k1gFnPc1	pokojská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Maruška	Maruška	k1gFnSc1	Maruška
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Dočekal	Dočekal	k1gMnSc1	Dočekal
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Platil	platit	k5eAaImAgMnS	platit
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Kačaba	kačaba	k1gMnSc1	kačaba
(	(	kIx(	(
<g/>
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Štibich	Štibich	k1gMnSc1	Štibich
(	(	kIx(	(
<g/>
portýr	portýr	k1gMnSc1	portýr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Boháč	Boháč	k1gMnSc1	Boháč
(	(	kIx(	(
<g/>
recepční	recepční	k1gMnSc1	recepční
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Krytinář	Krytinář	k1gMnSc1	Krytinář
(	(	kIx(	(
<g/>
liftboy	liftboy	k1gMnSc1	liftboy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Sova	Sova	k1gMnSc1	Sova
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
barman	barman	k1gMnSc1	barman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Vašut	Vašut	k1gMnSc1	Vašut
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Čenský	Čenský	k1gMnSc1	Čenský
(	(	kIx(	(
<g/>
popeláři	popelář	k1gMnPc1	popelář
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Barvy	barva	k1gFnSc2	barva
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Lada	Lada	k1gFnSc1	Lada
Odstrčilová	Odstrčilová	k1gFnSc1	Odstrčilová
(	(	kIx(	(
<g/>
první	první	k4xOgFnPc1	první
tanečnice	tanečnice	k1gFnPc1	tanečnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
Kučerová	Kučerová	k1gFnSc1	Kučerová
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
Španělka	Španělka	k1gFnSc1	Španělka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Krňanská	Krňanský	k2eAgFnSc1d1	Krňanská
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
Španělka	Španělka	k1gFnSc1	Španělka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šimon	Šimon	k1gMnSc1	Šimon
Caban	Caban	k1gMnSc1	Caban
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Čumpelík	Čumpelík	k1gMnSc1	Čumpelík
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kopecký	Kopecký	k1gMnSc1	Kopecký
(	(	kIx(	(
<g/>
sukňoví	sukňový	k2eAgMnPc1d1	sukňový
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
Riedelbauchová	Riedelbauchová	k1gFnSc1	Riedelbauchová
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Zelenková	Zelenková	k1gFnSc1	Zelenková
(	(	kIx(	(
<g/>
Egypťanky	Egypťanka	k1gFnPc1	Egypťanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vít	vít	k5eAaImF	vít
Máslo	máslo	k1gNnSc4	máslo
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Bauer	Bauer	k1gMnSc1	Bauer
(	(	kIx(	(
<g/>
bruslaři	bruslař	k1gMnPc7	bruslař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Simona	Simona	k1gFnSc1	Simona
Rybáková	Rybáková	k1gFnSc1	Rybáková
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Iveta	Iveta	k1gFnSc1	Iveta
Jadrníčková	Jadrníčková	k1gFnSc1	Jadrníčková
"	"	kIx"	"
<g/>
Na	na	k7c6	na
brigádě	brigáda	k1gFnSc6	brigáda
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanák	Hanák	k1gMnSc1	Hanák
(	(	kIx(	(
<g/>
tramp	tramp	k1gMnSc1	tramp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Kušiaková	Kušiakový	k2eAgFnSc1d1	Kušiaková
(	(	kIx(	(
<g/>
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
(	(	kIx(	(
<g/>
traktorista	traktorista	k1gMnSc1	traktorista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Šteindler	Šteindler	k1gMnSc1	Šteindler
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k1gMnSc1	vedoucí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Vorel	Vorel	k1gMnSc1	Vorel
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Slovák	Slovák	k1gMnSc1	Slovák
(	(	kIx(	(
<g/>
příslušník	příslušník	k1gMnSc1	příslušník
SNB	SNB	kA	SNB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
Najbrt	Najbrt	k1gInSc1	Najbrt
(	(	kIx(	(
<g/>
pohůnek	pohůnek	k1gMnSc1	pohůnek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
Vychodilová	Vychodilová	k1gFnSc1	Vychodilová
(	(	kIx(	(
<g/>
dojička	dojička	k1gFnSc1	dojička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Auerbach	Auerbach	k1gMnSc1	Auerbach
(	(	kIx(	(
<g/>
hostinský	hostinský	k1gMnSc1	hostinský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Smrčková	Smrčková	k1gFnSc1	Smrčková
(	(	kIx(	(
<g/>
servírka	servírka	k1gFnSc1	servírka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bára	Bára	k1gFnSc1	Bára
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
(	(	kIx(	(
<g/>
servírka	servírka	k1gFnSc1	servírka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vorel	Vorel	k1gMnSc1	Vorel
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Fero	Fero	k1gMnSc1	Fero
Burda	Burda	k1gMnSc1	Burda
(	(	kIx(	(
<g/>
řidič	řidič	k1gMnSc1	řidič
<g/>
)	)	kIx)	)
Pražská	pražský	k2eAgFnSc1d1	Pražská
pětka	pětka	k1gFnSc1	pětka
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
fdb	fdb	k?	fdb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Pražská	pražský	k2eAgFnSc1d1	Pražská
pětka	pětka	k1gFnSc1	pětka
Pražská	pražský	k2eAgFnSc1d1	Pražská
pětka	pětka	k1gFnSc1	pětka
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
