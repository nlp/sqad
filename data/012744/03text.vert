<p>
<s>
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
(	(	kIx(	(
<g/>
povídka	povídka	k1gFnSc1	povídka
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
nebo	nebo	k8xC	nebo
slovenštiny	slovenština	k1gFnSc2	slovenština
i	i	k9	i
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Zánik	zánik	k1gInSc4	zánik
domu	dům	k1gInSc2	dům
Usherovského	Usherovský	k2eAgInSc2d1	Usherovský
<g/>
,	,	kIx,	,
Zkáza	zkáza	k1gFnSc1	zkáza
Domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
a	a	k8xC	a
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherova	Usherův	k2eAgFnSc1d1	Usherova
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
anglického	anglický	k2eAgInSc2d1	anglický
originálu	originál	k1gInSc2	originál
zní	znět	k5eAaImIp3nS	znět
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
the	the	k?	the
House	house	k1gNnSc4	house
of	of	k?	of
Usher	Usher	k1gInSc1	Usher
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hororová	hororový	k2eAgFnSc1d1	hororová
povídka	povídka	k1gFnSc1	povídka
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
literárního	literární	k2eAgMnSc2d1	literární
teoretika	teoretik	k1gMnSc2	teoretik
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hororový	hororový	k2eAgInSc1d1	hororový
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
nejmenovaným	nejmenovaný	k1gMnSc7	nejmenovaný
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obdrží	obdržet	k5eAaPmIp3nS	obdržet
naléhavý	naléhavý	k2eAgInSc4d1	naléhavý
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
přítele	přítel	k1gMnSc2	přítel
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Rodericka	Rodericka	k1gFnSc1	Rodericka
Ushera	Ushera	k1gFnSc1	Ushera
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
jej	on	k3xPp3gMnSc4	on
žádá	žádat	k5eAaImIp3nS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
přijel	přijet	k5eAaPmAgMnS	přijet
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
za	za	k7c7	za
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
ponurém	ponurý	k2eAgNnSc6d1	ponuré
sídle	sídlo	k1gNnSc6	sídlo
a	a	k8xC	a
shledá	shledat	k5eAaPmIp3nS	shledat
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
psychickém	psychický	k2eAgInSc6d1	psychický
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
Usherů	Usher	k1gInPc2	Usher
vážený	vážený	k2eAgMnSc1d1	vážený
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
trpí	trpět	k5eAaImIp3nP	trpět
duševními	duševní	k2eAgFnPc7d1	duševní
indispozicemi	indispozice	k1gFnPc7	indispozice
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
ihned	ihned	k6eAd1	ihned
povšimne	povšimnout	k5eAaPmIp3nS	povšimnout
–	–	k?	–
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
stavu	stav	k1gInSc6	stav
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
sestře	sestra	k1gFnSc6	sestra
Madeline	Madelin	k1gInSc5	Madelin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
příběhu	příběh	k1gInSc2	příběh
obdrží	obdržet	k5eAaPmIp3nS	obdržet
jednoho	jeden	k4xCgMnSc4	jeden
dne	den	k1gInSc2	den
naléhavý	naléhavý	k2eAgInSc4d1	naléhavý
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Rodericka	Rodericko	k1gNnSc2	Rodericko
Ushera	Ushero	k1gNnSc2	Ushero
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
přijel	přijet	k5eAaPmAgMnS	přijet
navštívit	navštívit	k5eAaPmF	navštívit
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
sídla	sídlo	k1gNnSc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Žádá	žádat	k5eAaImIp3nS	žádat
jej	on	k3xPp3gMnSc4	on
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
trpí	trpět	k5eAaImIp3nS	trpět
nervovou	nervový	k2eAgFnSc7d1	nervová
rozrušeností	rozrušenost	k1gFnSc7	rozrušenost
a	a	k8xC	a
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
společnost	společnost	k1gFnSc1	společnost
jediného	jediný	k2eAgMnSc2d1	jediný
přítele	přítel	k1gMnSc2	přítel
přinese	přinést	k5eAaPmIp3nS	přinést
úlevu	úleva	k1gFnSc4	úleva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
kvečeru	kvečeru	k6eAd1	kvečeru
dorazí	dorazit	k5eAaPmIp3nS	dorazit
ke	k	k7c3	k
starobylému	starobylý	k2eAgInSc3d1	starobylý
domu	dům	k1gInSc3	dům
rodu	rod	k1gInSc2	rod
Usherů	Usher	k1gInPc2	Usher
<g/>
,	,	kIx,	,
padne	padnout	k5eAaImIp3nS	padnout
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
podivná	podivný	k2eAgFnSc1d1	podivná
tíseň	tíseň	k1gFnSc1	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Jakoby	Jakob	k1gMnPc4	Jakob
měl	mít	k5eAaImAgInS	mít
dům	dům	k1gInSc1	dům
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
močálem	močál	k1gInSc7	močál
temnou	temný	k2eAgFnSc4d1	temná
moc	moc	k1gFnSc4	moc
přivolávat	přivolávat	k5eAaImF	přivolávat
chmury	chmura	k1gFnPc4	chmura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
mysl	mysl	k1gFnSc4	mysl
mu	on	k3xPp3gMnSc3	on
vyvstane	vyvstat	k5eAaPmIp3nS	vyvstat
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
dům	dům	k1gInSc4	dům
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
tíživé	tíživý	k2eAgInPc1d1	tíživý
výpary	výpar	k1gInPc1	výpar
rozkladu	rozklad	k1gInSc2	rozklad
<g/>
,	,	kIx,	,
hnijící	hnijící	k2eAgNnSc1d1	hnijící
ovzduší	ovzduší	k1gNnSc1	ovzduší
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
barvy	barva	k1gFnSc2	barva
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
váženém	vážený	k2eAgInSc6d1	vážený
rodu	rod	k1gInSc6	rod
Usherů	Usher	k1gInPc2	Usher
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
slul	slout	k5eAaImAgInS	slout
odnepaměti	odnepaměti	k6eAd1	odnepaměti
přehnanou	přehnaný	k2eAgFnSc7d1	přehnaná
citlivostí	citlivost	k1gFnSc7	citlivost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
v	v	k7c6	v
četných	četný	k2eAgInPc6d1	četný
uměleckých	umělecký	k2eAgInPc6d1	umělecký
dílech	díl	k1gInPc6	díl
<g/>
,	,	kIx,	,
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
složité	složitý	k2eAgFnPc4d1	složitá
formy	forma	k1gFnPc4	forma
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
štědrou	štědrý	k2eAgFnSc7d1	štědrá
dobročinností	dobročinnost	k1gFnSc7	dobročinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sluha	sluha	k1gMnSc1	sluha
návštěvě	návštěva	k1gFnSc3	návštěva
odebere	odebrat	k5eAaPmIp3nS	odebrat
koně	kůň	k1gMnPc4	kůň
a	a	k8xC	a
komorník	komorník	k1gMnSc1	komorník
muže	muž	k1gMnSc2	muž
doprovodí	doprovodit	k5eAaPmIp3nS	doprovodit
k	k	k7c3	k
Roderickovi	Rodericek	k1gMnSc3	Rodericek
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
překvapen	překvapit	k5eAaPmNgInS	překvapit
proměnou	proměna	k1gFnSc7	proměna
posledního	poslední	k2eAgInSc2d1	poslední
z	z	k7c2	z
Usherů	Usher	k1gMnPc2	Usher
<g/>
!	!	kIx.	!
</s>
<s>
Před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
stojí	stát	k5eAaImIp3nS	stát
lidská	lidský	k2eAgFnSc1d1	lidská
troska	troska	k1gFnSc1	troska
<g/>
,	,	kIx,	,
mrtvolně	mrtvolně	k6eAd1	mrtvolně
bledá	bledý	k2eAgFnSc1d1	bledá
pleť	pleť	k1gFnSc1	pleť
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc4d1	velké
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgInPc4d1	tenký
bledé	bledý	k2eAgInPc4d1	bledý
rty	ret	k1gInPc4	ret
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgInPc1d1	tenký
měkké	měkký	k2eAgInPc1d1	měkký
vlasy	vlas	k1gInPc1	vlas
povlávající	povlávající	k2eAgInPc1d1	povlávající
kolem	kolem	k7c2	kolem
lebky	lebka	k1gFnSc2	lebka
s	s	k7c7	s
nepřiměřeným	přiměřený	k2eNgNnSc7d1	nepřiměřené
rozpětím	rozpětí	k1gNnSc7	rozpětí
<g/>
.	.	kIx.	.
</s>
<s>
Roderick	Roderick	k1gMnSc1	Roderick
jej	on	k3xPp3gMnSc4	on
srdečně	srdečně	k6eAd1	srdečně
vítá	vítat	k5eAaImIp3nS	vítat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tváři	tvář	k1gFnSc6	tvář
je	být	k5eAaImIp3nS	být
upřímnost	upřímnost	k1gFnSc1	upřímnost
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
návštěvník	návštěvník	k1gMnSc1	návštěvník
pozná	poznat	k5eAaPmIp3nS	poznat
další	další	k2eAgInPc4d1	další
rysy	rys	k1gInPc4	rys
povahy	povaha	k1gFnSc2	povaha
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
–	–	k?	–
rozpolcenost	rozpolcenost	k1gFnSc4	rozpolcenost
<g/>
,	,	kIx,	,
ustavičnou	ustavičný	k2eAgFnSc4d1	ustavičná
úzkost	úzkost	k1gFnSc4	úzkost
a	a	k8xC	a
zádumčivost	zádumčivost	k1gFnSc4	zádumčivost
<g/>
.	.	kIx.	.
</s>
<s>
Usher	Usher	k1gInSc1	Usher
jej	on	k3xPp3gMnSc4	on
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
chorobě	choroba	k1gFnSc6	choroba
a	a	k8xC	a
utkvělé	utkvělý	k2eAgFnSc6d1	utkvělá
obavě	obava	k1gFnSc6	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
brzy	brzy	k6eAd1	brzy
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
prastaré	prastarý	k2eAgNnSc4d1	prastaré
sídlo	sídlo	k1gNnSc4	sídlo
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
získalo	získat	k5eAaPmAgNnS	získat
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
moc	moc	k6eAd1	moc
a	a	k8xC	a
opanovalo	opanovat	k5eAaPmAgNnS	opanovat
jeho	on	k3xPp3gInSc4	on
životní	životní	k2eAgInSc4d1	životní
elán	elán	k1gInSc4	elán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
neblahým	blahý	k2eNgFnPc3d1	neblahá
psychickým	psychický	k2eAgFnPc3d1	psychická
náladám	nálada	k1gFnPc3	nálada
přispívá	přispívat	k5eAaImIp3nS	přispívat
i	i	k9	i
špatný	špatný	k2eAgInSc1d1	špatný
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
jeho	jeho	k3xOp3gFnSc2	jeho
milované	milovaný	k2eAgFnSc2d1	milovaná
sestry	sestra	k1gFnSc2	sestra
Madeline	Madelin	k1gInSc5	Madelin
<g/>
,	,	kIx,	,
vleklá	vleklý	k2eAgFnSc1d1	vleklá
choroba	choroba	k1gFnSc1	choroba
ji	on	k3xPp3gFnSc4	on
velmi	velmi	k6eAd1	velmi
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
a	a	k8xC	a
přivádí	přivádět	k5eAaImIp3nS	přivádět
dnem	den	k1gInSc7	den
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
blíž	blízce	k6eAd2	blízce
k	k	k7c3	k
náruči	náruč	k1gFnSc3	náruč
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návštěvník	návštěvník	k1gMnSc1	návštěvník
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
přítele	přítel	k1gMnSc2	přítel
rozptýlit	rozptýlit	k5eAaPmF	rozptýlit
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
četbě	četba	k1gFnSc3	četba
či	či	k8xC	či
předčítání	předčítání	k1gNnSc1	předčítání
<g/>
,	,	kIx,	,
Usher	Usher	k1gInSc1	Usher
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
zpívá	zpívat	k5eAaImIp3nS	zpívat
improvizovaný	improvizovaný	k2eAgInSc4d1	improvizovaný
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
maluje	malovat	k5eAaImIp3nS	malovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
rozhovorech	rozhovor	k1gInPc6	rozhovor
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
schopnost	schopnost	k1gFnSc1	schopnost
vnímání	vnímání	k1gNnSc2	vnímání
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
–	–	k?	–
a	a	k8xC	a
také	také	k9	také
určité	určitý	k2eAgInPc4d1	určitý
nerosty	nerost	k1gInPc4	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jeho	jeho	k3xOp3gFnSc1	jeho
víra	víra	k1gFnSc1	víra
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
strašlivá	strašlivý	k2eAgFnSc1d1	strašlivá
síla	síla	k1gFnSc1	síla
starodávného	starodávný	k2eAgNnSc2d1	starodávné
sídla	sídlo	k1gNnSc2	sídlo
ovládá	ovládat	k5eAaImIp3nS	ovládat
jeho	jeho	k3xOp3gFnSc4	jeho
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Madeline	Madelin	k1gInSc5	Madelin
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
Roderick	Roderick	k1gInSc1	Roderick
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
přání	přání	k1gNnSc4	přání
uložit	uložit	k5eAaPmF	uložit
její	její	k3xOp3gNnSc4	její
tělo	tělo	k1gNnSc4	tělo
dočasně	dočasně	k6eAd1	dočasně
do	do	k7c2	do
rodinné	rodinný	k2eAgFnSc2d1	rodinná
kobky	kobka	k1gFnSc2	kobka
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
ji	on	k3xPp3gFnSc4	on
dopraví	dopravit	k5eAaPmIp3nS	dopravit
do	do	k7c2	do
temného	temný	k2eAgNnSc2d1	temné
vlhkého	vlhký	k2eAgNnSc2d1	vlhké
sklepení	sklepení	k1gNnSc2	sklepení
a	a	k8xC	a
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
pohlédnou	pohlédnout	k5eAaPmIp3nP	pohlédnout
zesnulé	zesnulá	k1gFnPc1	zesnulá
do	do	k7c2	do
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc4d1	následující
dny	den	k1gInPc4	den
trápí	trápit	k5eAaImIp3nS	trápit
Ushera	Ushera	k1gFnSc1	Ushera
žal	žal	k1gInSc1	žal
a	a	k8xC	a
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
venku	venku	k6eAd1	venku
zuří	zuřit	k5eAaImIp3nS	zuřit
bouře	bouř	k1gFnPc4	bouř
<g/>
,	,	kIx,	,
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přítele	přítel	k1gMnSc4	přítel
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
komnatě	komnata	k1gFnSc6	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
předčítat	předčítat	k5eAaImF	předčítat
mu	on	k3xPp3gMnSc3	on
úryvky	úryvek	k1gInPc7	úryvek
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Launcelota	Launcelot	k1gMnSc2	Launcelot
Canninga	Canning	k1gMnSc2	Canning
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
četbě	četba	k1gFnSc6	četba
<g/>
,	,	kIx,	,
Roderick	Roderick	k1gInSc1	Roderick
se	se	k3xPyFc4	se
podivně	podivně	k6eAd1	podivně
chvěje	chvět	k5eAaImIp3nS	chvět
a	a	k8xC	a
pohupuje	pohupovat	k5eAaImIp3nS	pohupovat
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
odněkud	odněkud	k6eAd1	odněkud
z	z	k7c2	z
domu	dům	k1gInSc2	dům
vycházejí	vycházet	k5eAaImIp3nP	vycházet
zvuky	zvuk	k1gInPc1	zvuk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
shodují	shodovat	k5eAaImIp3nP	shodovat
s	s	k7c7	s
příběhem	příběh	k1gInSc7	příběh
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
chvíli	chvíle	k1gFnSc4	chvíle
Usher	Ushra	k1gFnPc2	Ushra
vyskočí	vyskočit	k5eAaPmIp3nS	vyskočit
a	a	k8xC	a
vykřikne	vykřiknout	k5eAaPmIp3nS	vykřiknout
"	"	kIx"	"
<g/>
Ne	ne	k9	ne
Ethelred	Ethelred	k1gInSc1	Ethelred
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Madeline	Madelin	k1gInSc5	Madelin
je	on	k3xPp3gInPc4	on
za	za	k7c7	za
dveřmi	dveře	k1gFnPc7	dveře
<g/>
!	!	kIx.	!
</s>
<s>
Cožpak	cožpak	k9	cožpak
nepoznám	poznat	k5eNaPmIp1nS	poznat
ten	ten	k3xDgInSc4	ten
tlukot	tlukot	k1gInSc4	tlukot
jejího	její	k3xOp3gNnSc2	její
srdce	srdce	k1gNnSc2	srdce
<g/>
?	?	kIx.	?
</s>
<s>
Chvátá	chvátat	k5eAaImIp3nS	chvátat
mi	já	k3xPp1nSc3	já
vyčíst	vyčíst	k5eAaPmF	vyčíst
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
pospíšil	pospíšit	k5eAaPmAgMnS	pospíšit
s	s	k7c7	s
pohřbem	pohřeb	k1gInSc7	pohřeb
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Dveře	dveře	k1gFnPc1	dveře
se	se	k3xPyFc4	se
otevřou	otevřít	k5eAaPmIp3nP	otevřít
a	a	k8xC	a
vyzáblé	vyzáblý	k2eAgNnSc4d1	vyzáblé
tělo	tělo	k1gNnSc4	tělo
Madeline	Madelin	k1gInSc5	Madelin
v	v	k7c6	v
zakrváceném	zakrvácený	k2eAgInSc6d1	zakrvácený
rubáši	rubáš	k1gInSc6	rubáš
padne	padnout	k5eAaImIp3nS	padnout
na	na	k7c4	na
jejího	její	k3xOp3gMnSc4	její
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
kácí	kácet	k5eAaImIp3nS	kácet
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
mrtev	mrtev	k2eAgInSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1	vypravěč
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
nečeká	čekat	k5eNaImIp3nS	čekat
<g/>
,	,	kIx,	,
prchá	prchat	k5eAaImIp3nS	prchat
z	z	k7c2	z
děsivého	děsivý	k2eAgInSc2d1	děsivý
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
skočí	skočit	k5eAaPmIp3nS	skočit
na	na	k7c4	na
koně	kůň	k1gMnSc4	kůň
a	a	k8xC	a
ujíždí	ujíždět	k5eAaImIp3nS	ujíždět
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ohlédne	ohlédnout	k5eAaPmIp3nS	ohlédnout
<g/>
,	,	kIx,	,
spatří	spatřit	k5eAaPmIp3nS	spatřit
rozpadající	rozpadající	k2eAgMnSc1d1	rozpadající
se	se	k3xPyFc4	se
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
vody	voda	k1gFnPc1	voda
černého	černý	k2eAgInSc2d1	černý
močálu	močál	k1gInSc2	močál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Témata	téma	k1gNnPc4	téma
a	a	k8xC	a
odkazy	odkaz	k1gInPc4	odkaz
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
často	často	k6eAd1	často
se	se	k3xPyFc4	se
opakujících	opakující	k2eAgNnPc2d1	opakující
témat	téma	k1gNnPc2	téma
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
E.	E.	kA	E.
A.	A.	kA	A.
Poea	Poea	k1gMnSc1	Poea
lze	lze	k6eAd1	lze
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
<g/>
"	"	kIx"	"
nalézt	nalézt	k5eAaBmF	nalézt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pohřbení	pohřbení	k1gNnSc4	pohřbení
zaživa	zaživa	k6eAd1	zaživa
(	(	kIx(	(
<g/>
též	též	k9	též
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
"	"	kIx"	"
<g/>
Berenice	Berenice	k1gFnSc1	Berenice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sud	sud	k1gInSc1	sud
vína	víno	k1gNnSc2	víno
amontilladského	amontilladský	k2eAgNnSc2d1	amontilladský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Psychické	psychický	k2eAgNnSc1d1	psychické
onemocnění	onemocnění	k1gNnSc1	onemocnění
(	(	kIx(	(
<g/>
též	též	k9	též
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
"	"	kIx"	"
<g/>
Berenice	Berenice	k1gFnSc1	Berenice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zrádné	zrádný	k2eAgNnSc4d1	zrádné
srdce	srdce	k1gNnSc4	srdce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Metoda	metoda	k1gFnSc1	metoda
doktora	doktor	k1gMnSc2	doktor
Téra	Térus	k1gMnSc2	Térus
a	a	k8xC	a
profesora	profesor	k1gMnSc2	profesor
Péra	péro	k1gNnSc2	péro
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katalepsie	katalepsie	k1gFnSc1	katalepsie
(	(	kIx(	(
<g/>
též	též	k9	též
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
"	"	kIx"	"
<g/>
Berenice	Berenice	k1gFnSc1	Berenice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
jsou	být	k5eAaImIp3nP	být
jmenováni	jmenován	k2eAgMnPc1d1	jmenován
různí	různý	k2eAgMnPc1d1	různý
autoři	autor	k1gMnPc1	autor
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
díla	dílo	k1gNnPc1	dílo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
úvodní	úvodní	k2eAgInSc4d1	úvodní
citát	citát	k1gInSc4	citát
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
srdce	srdce	k1gNnSc4	srdce
je	být	k5eAaImIp3nS	být
loutna	loutna	k1gFnSc1	loutna
zavěšená	zavěšený	k2eAgFnSc1d1	zavěšená
<g/>
,	,	kIx,	,
sotva	sotva	k8xS	sotva
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
dotkneš	dotknout	k5eAaPmIp2nS	dotknout
<g/>
,	,	kIx,	,
zní	znět	k5eAaImIp3nS	znět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
francouzského	francouzský	k2eAgMnSc2d1	francouzský
básníka	básník	k1gMnSc2	básník
P.	P.	kA	P.
<g/>
-	-	kIx~	-
<g/>
J.	J.	kA	J.
de	de	k?	de
Bérangera	Bérangera	k1gFnSc1	Bérangera
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
Bérangerův	Bérangerův	k2eAgInSc4d1	Bérangerův
originální	originální	k2eAgInSc4d1	originální
text	text	k1gInSc4	text
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Mé	můj	k3xOp1gNnSc1	můj
srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
román	román	k1gInSc1	román
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
výprava	výprava	k1gFnSc1	výprava
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Klimma	Klimm	k1gMnSc2	Klimm
dánsko-norského	dánskoorský	k2eAgMnSc2d1	dánsko-norský
dramatika	dramatik	k1gMnSc2	dramatik
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
historika	historik	k1gMnSc2	historik
Ludviga	Ludvig	k1gMnSc2	Ludvig
Holberga	Holberg	k1gMnSc2	Holberg
</s>
</p>
<p>
<s>
spis	spis	k1gInSc1	spis
De	De	k?	De
Caelo	Caelo	k1gNnSc1	Caelo
et	et	k?	et
Ejus	Ejusa	k1gFnPc2	Ejusa
Mirabilibus	Mirabilibus	k1gMnSc1	Mirabilibus
et	et	k?	et
de	de	k?	de
inferno	inferno	k1gNnSc4	inferno
<g/>
.	.	kIx.	.
</s>
<s>
Ex	ex	k6eAd1	ex
Auditis	Auditis	k1gFnSc1	Auditis
et	et	k?	et
Visis	Visis	k1gFnSc1	Visis
(	(	kIx(	(
<g/>
Nebe	nebe	k1gNnSc1	nebe
a	a	k8xC	a
peklo	peklo	k1gNnSc1	peklo
<g/>
)	)	kIx)	)
švédského	švédský	k2eAgMnSc2d1	švédský
vědce	vědec	k1gMnSc2	vědec
<g/>
,	,	kIx,	,
teologa	teolog	k1gMnSc2	teolog
a	a	k8xC	a
mystika	mystik	k1gMnSc2	mystik
Emanuela	Emanuel	k1gMnSc2	Emanuel
Swedenborga	Swedenborg	k1gMnSc2	Swedenborg
</s>
</p>
<p>
<s>
novela	novela	k1gFnSc1	novela
Belfagor	Belfagora	k1gFnPc2	Belfagora
Arcidiavolo	Arcidiavola	k1gFnSc5	Arcidiavola
italského	italský	k2eAgMnSc2d1	italský
politika	politik	k1gMnSc2	politik
<g/>
,	,	kIx,	,
diplomata	diplomat	k1gMnSc2	diplomat
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
historika	historik	k1gMnSc2	historik
a	a	k8xC	a
vojenského	vojenský	k2eAgMnSc2d1	vojenský
teoretika	teoretik	k1gMnSc2	teoretik
Niccolò	Niccolò	k1gMnSc2	Niccolò
Machiavelliho	Machiavelli	k1gMnSc2	Machiavelli
</s>
</p>
<p>
<s>
básně	báseň	k1gFnPc1	báseň
Vert-Vert	Vert-Vert	k1gInSc1	Vert-Vert
a	a	k8xC	a
La	la	k1gNnSc1	la
Chartreuse	Chartreuse	k1gFnSc2	Chartreuse
francouzského	francouzský	k2eAgMnSc2d1	francouzský
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
dramatika	dramatik	k1gMnSc2	dramatik
Jean-Baptiste-Louise	Jean-Baptiste-Louise	k1gFnSc2	Jean-Baptiste-Louise
Gresseta	Gresset	k1gMnSc2	Gresset
</s>
</p>
<p>
<s>
díla	dílo	k1gNnSc2	dílo
anglického	anglický	k2eAgMnSc2d1	anglický
fyzika	fyzik	k1gMnSc2	fyzik
<g/>
,	,	kIx,	,
matematika	matematik	k1gMnSc2	matematik
<g/>
,	,	kIx,	,
lékaře	lékař	k1gMnSc2	lékař
a	a	k8xC	a
alchymisty	alchymista	k1gMnSc2	alchymista
Roberta	Robert	k1gMnSc2	Robert
Fludda	Fludd	k1gMnSc2	Fludd
</s>
</p>
<p>
<s>
Das	Das	k?	Das
alte	alt	k1gInSc5	alt
Buch	buch	k1gInSc1	buch
und	und	k?	und
die	die	k?	die
Reise	Reise	k1gFnSc2	Reise
ins	ins	k?	ins
Blaue	Blau	k1gFnSc2	Blau
hinein	hineina	k1gFnPc2	hineina
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Einat	k5eAaPmIp3nS	Einat
Mährchen-Novelle	Mährchen-Novelle	k1gInSc1	Mährchen-Novelle
německého	německý	k2eAgMnSc2d1	německý
romantického	romantický	k2eAgMnSc2d1	romantický
básníka	básník	k1gMnSc2	básník
<g/>
,	,	kIx,	,
prozaika	prozaik	k1gMnSc2	prozaik
<g/>
,	,	kIx,	,
dramatika	dramatik	k1gMnSc2	dramatik
a	a	k8xC	a
překladatele	překladatel	k1gMnSc2	překladatel
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Tiecke	Tieck	k1gMnSc2	Tieck
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Kunst	Kunst	k1gInSc1	Kunst
der	drát	k5eAaImRp2nS	drát
Chiromantzey	Chiromantzey	k1gInPc4	Chiromantzey
Jeana	Jean	k1gMnSc2	Jean
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Indaginé	Indaginá	k1gFnSc2	Indaginá
</s>
</p>
<p>
<s>
Discours	Discours	k1gInSc1	Discours
sur	sur	k?	sur
les	les	k1gInSc1	les
Principes	Principes	k1gInSc1	Principes
de	de	k?	de
la	la	k1gNnSc2	la
Chiromancie	Chiromancie	k1gFnSc1	Chiromancie
Marina	Marina	k1gFnSc1	Marina
Cureau	Cureaus	k1gInSc2	Cureaus
de	de	k?	de
la	la	k1gNnSc1	la
Chambra	Chambr	k1gInSc2	Chambr
</s>
</p>
<p>
<s>
dílo	dílo	k1gNnSc4	dílo
Sluneční	sluneční	k2eAgInSc1d1	sluneční
stát	stát	k1gInSc1	stát
italského	italský	k2eAgMnSc2d1	italský
filosofa	filosof	k1gMnSc2	filosof
<g/>
,	,	kIx,	,
teologa	teolog	k1gMnSc2	teolog
<g/>
,	,	kIx,	,
astrologa	astrolog	k1gMnSc2	astrolog
a	a	k8xC	a
básníka	básník	k1gMnSc2	básník
Tommaso	Tommasa	k1gFnSc5	Tommasa
Campanelly	Campanell	k1gInPc4	Campanell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
dílo	dílo	k1gNnSc1	dílo
Directorium	Directorium	k1gNnSc1	Directorium
Inquisitorum	Inquisitorum	k1gInSc1	Inquisitorum
dominikána	dominikán	k1gMnSc2	dominikán
Eymerica	Eymericus	k1gMnSc2	Eymericus
de	de	k?	de
Gironna	Gironn	k1gMnSc2	Gironn
</s>
</p>
<p>
<s>
římský	římský	k2eAgMnSc1d1	římský
geograf	geograf	k1gMnSc1	geograf
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Pomponius	Pomponius	k1gMnSc1	Pomponius
Mela	mlít	k5eAaImSgInS	mlít
</s>
</p>
<p>
<s>
britský	britský	k2eAgMnSc1d1	britský
malíř	malíř	k1gMnSc1	malíř
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
původu	původ	k1gInSc2	původ
Henry	henry	k1gInSc1	henry
Fuseli	Fusel	k1gInSc6	Fusel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Inspirace	inspirace	k1gFnSc1	inspirace
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
švédské	švédský	k2eAgFnSc2d1	švédská
death	death	k1gInSc4	death
metalové	metalový	k2eAgFnSc2d1	metalová
kapely	kapela	k1gFnSc2	kapela
House	house	k1gNnSc1	house
of	of	k?	of
Usher	Usher	k1gMnSc1	Usher
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
názvu	název	k1gInSc2	název
povídky	povídka	k1gFnSc2	povídka
E.	E.	kA	E.
A.	A.	kA	A.
Poea	Poea	k1gMnSc1	Poea
</s>
</p>
<p>
<s>
Nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
operu	opera	k1gFnSc4	opera
na	na	k7c4	na
námět	námět	k1gInSc4	námět
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
La	la	k0	la
Chute	Chut	k1gMnSc5	Chut
de	de	k?	de
maison	maison	k1gMnSc1	maison
Usher	Usher	k1gMnSc1	Usher
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaPmAgMnS	napsat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
skladatel	skladatel	k1gMnSc1	skladatel
Claude	Claud	k1gInSc5	Claud
Debussy	Debussa	k1gFnPc1	Debussa
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pád	Pád	k1gInSc1	Pád
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
the	the	k?	the
House	house	k1gNnSc1	house
of	of	k?	of
Usher	Usher	k1gMnSc1	Usher
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
Philipa	Philipa	k1gFnSc1	Philipa
Glasse	Glasse	k1gFnSc1	Glasse
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
Pád	Pád	k1gInSc1	Pád
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
the	the	k?	the
House	house	k1gNnSc1	house
of	of	k?	of
Usher	Usher	k1gMnSc1	Usher
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
Hendrika	Hendrika	k1gFnSc1	Hendrika
Hofmeyra	Hofmeyra	k1gFnSc1	Hofmeyra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
===	===	k?	===
Filmy	film	k1gInPc4	film
===	===	k?	===
</s>
</p>
<p>
<s>
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
–	–	k?	–
krátký	krátký	k2eAgInSc4d1	krátký
film	film	k1gInSc4	film
Jana	Jan	k1gMnSc2	Jan
Švankmajera	Švankmajer	k1gMnSc2	Švankmajer
</s>
</p>
<p>
<s>
Pád	Pád	k1gInSc1	Pád
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
–	–	k?	–
francouzsko-americký	francouzskomerický	k2eAgInSc4d1	francouzsko-americký
film	film	k1gInSc4	film
</s>
</p>
<p>
<s>
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
</s>
</p>
<p>
<s>
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
–	–	k?	–
americko-kanadsko-britský	americkoanadskoritský	k2eAgInSc4d1	americko-kanadsko-britský
film	film	k1gInSc4	film
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
–	–	k?	–
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
a	a	k8xC	a
slovenská	slovenský	k2eAgNnPc1d1	slovenské
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Česky	česky	k6eAd1	česky
nebo	nebo	k8xC	nebo
slovensky	slovensky	k6eAd1	slovensky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
povídka	povídka	k1gFnSc1	povídka
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
sbírkách	sbírka	k1gFnPc6	sbírka
či	či	k8xC	či
antologiích	antologie	k1gFnPc6	antologie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Anděl	Anděl	k1gMnSc1	Anděl
pitvornosti	pitvornost	k1gFnSc2	pitvornost
(	(	kIx(	(
<g/>
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jáma	jáma	k1gFnSc1	jáma
&	&	k?	&
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
fantastické	fantastický	k2eAgInPc1d1	fantastický
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jáma	jáma	k1gFnSc1	jáma
a	a	k8xC	a
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Odeon	odeon	k1gInSc1	odeon
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
a	a	k8xC	a
Levné	levný	k2eAgFnPc1d1	levná
knihy	kniha	k1gFnPc1	kniha
KMa	KMa	k1gFnSc1	KMa
2002	[number]	k4	2002
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pád	Pád	k1gInSc1	Pád
do	do	k7c2	do
Maelströmu	Maelström	k1gInSc2	Maelström
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
939	[number]	k4	939
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šenkyřík	Šenkyřík	k1gMnSc1	Šenkyřík
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
,	,	kIx,	,
248	[number]	k4	248
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
obálky	obálka	k1gFnSc2	obálka
<g/>
:	:	kIx,	:
Alén	Alén	k1gMnSc1	Alén
Diviš	Diviš	k1gMnSc1	Diviš
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Předčasný	předčasný	k2eAgInSc1d1	předčasný
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
:	:	kIx,	:
Horrory	horror	k1gInPc1	horror
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
děsivé	děsivý	k2eAgInPc1d1	děsivý
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
Hynek	Hynek	k1gMnSc1	Hynek
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Půlnoční	půlnoční	k1gFnSc1	půlnoční
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
sestavily	sestavit	k5eAaPmAgFnP	sestavit
Zuzana	Zuzana	k1gFnSc1	Zuzana
Ceplová	Ceplová	k1gFnSc1	Ceplová
a	a	k8xC	a
Jarmila	Jarmila	k1gFnSc1	Jarmila
Rosíková	Rosíková	k1gFnSc1	Rosíková
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
mistrově	mistrův	k2eAgInSc6d1	mistrův
stínu	stín	k1gInSc6	stín
<g/>
:	:	kIx,	:
Povídky	povídka	k1gFnSc2	povídka
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
(	(	kIx(	(
<g/>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
XYZ	XYZ	kA	XYZ
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7388	[number]	k4	7388
<g/>
-	-	kIx~	-
<g/>
301	[number]	k4	301
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pekárek	Pekárek	k1gMnSc1	Pekárek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šenkyřík	Šenkyřík	k1gMnSc1	Šenkyřík
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
,	,	kIx,	,
384	[number]	k4	384
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
obálky	obálka	k1gFnSc2	obálka
<g/>
:	:	kIx,	:
Isifa	Isif	k1gMnSc2	Isif
Image	image	k1gFnSc1	image
Service	Service	k1gFnSc1	Service
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
Karman	karman	k1gInSc1	karman
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
skarabeus	skarabeus	k1gMnSc1	skarabeus
<g/>
:	:	kIx,	:
Devatero	devatero	k1gNnSc1	devatero
podivuhodných	podivuhodný	k2eAgInPc2d1	podivuhodný
příběhů	příběh	k1gInPc2	příběh
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
(	(	kIx(	(
<g/>
Albatros	albatros	k1gMnSc1	albatros
(	(	kIx(	(
<g/>
edice	edice	k1gFnSc1	edice
Klub	klub	k1gInSc1	klub
mladých	mladý	k2eAgMnPc2d1	mladý
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
228	[number]	k4	228
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
náklad	náklad	k1gInSc1	náklad
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zrádné	zrádný	k2eAgNnSc1d1	zrádné
srdce	srdce	k1gNnSc1	srdce
<g/>
:	:	kIx,	:
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
vázaná	vázané	k1gNnPc4	vázané
s	s	k7c7	s
papírovým	papírový	k2eAgInSc7d1	papírový
přebalem	přebal	k1gInSc7	přebal
<g/>
,	,	kIx,	,
676	[number]	k4	676
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zkáza	zkáza	k1gFnSc1	zkáza
Domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Edgar	Edgar	k1gMnSc1	Edgar
(	(	kIx(	(
<g/>
Dryada	Dryada	k1gFnSc1	Dryada
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherovského	Usherovský	k2eAgInSc2d1	Usherovský
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
skarabeus	skarabeus	k1gMnSc1	skarabeus
(	(	kIx(	(
<g/>
Tatran	Tatran	k1gInSc1	Tatran
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
295	[number]	k4	295
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Pád	Pád	k1gInSc1	Pád
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
A.	A.	kA	A.
G.	G.	kA	G.
Pyma	Pyma	k1gFnSc1	Pyma
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherova	Usherův	k2eAgInSc2d1	Usherův
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherova	Usherův	k2eAgInSc2d1	Usherův
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
Bečková	Bečková	k1gFnSc1	Bečková
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
the	the	k?	the
House	house	k1gNnSc1	house
of	of	k?	of
Usher	Ushra	k1gFnPc2	Ushra
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
POE	POE	kA	POE
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
<g/>
.	.	kIx.	.
</s>
<s>
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherova	Usherův	k2eAgInSc2d1	Usherův
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Anna	Anna	k1gFnSc1	Anna
Bečková	Bečková	k1gFnSc1	Bečková
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
327	[number]	k4	327
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
</s>
</p>
<p>
<s>
Katalepsie	katalepsie	k1gFnSc1	katalepsie
</s>
</p>
