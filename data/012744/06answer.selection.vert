<s>
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
(	(	kIx(	(
<g/>
povídka	povídka	k1gFnSc1	povídka
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
nebo	nebo	k8xC	nebo
slovenštiny	slovenština	k1gFnSc2	slovenština
i	i	k9	i
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Zánik	zánik	k1gInSc4	zánik
domu	dům	k1gInSc2	dům
Usherovského	Usherovský	k2eAgInSc2d1	Usherovský
<g/>
,	,	kIx,	,
Zkáza	zkáza	k1gFnSc1	zkáza
Domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
<g/>
,	,	kIx,	,
Pád	Pád	k1gInSc1	Pád
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gInPc2	Usher
a	a	k8xC	a
Zánik	zánik	k1gInSc1	zánik
domu	dům	k1gInSc2	dům
Usherova	Usherův	k2eAgFnSc1d1	Usherova
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
anglického	anglický	k2eAgInSc2d1	anglický
originálu	originál	k1gInSc2	originál
zní	znět	k5eAaImIp3nS	znět
The	The	k1gMnSc1	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
the	the	k?	the
House	house	k1gNnSc4	house
of	of	k?	of
Usher	Usher	k1gInSc1	Usher
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hororová	hororový	k2eAgFnSc1d1	hororová
povídka	povídka	k1gFnSc1	povídka
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
literárního	literární	k2eAgMnSc2d1	literární
teoretika	teoretik	k1gMnSc2	teoretik
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
