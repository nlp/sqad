<p>
<s>
Dlužicha	Dlužich	k1gMnSc4	Dlužich
krvavá	krvavý	k2eAgFnSc1d1	krvavá
(	(	kIx(	(
<g/>
Heuchera	Heuchera	k1gFnSc1	Heuchera
sanguinea	sanguinea	k1gFnSc1	sanguinea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
růžově	růžově	k6eAd1	růžově
až	až	k9	až
červeně	červeně	k6eAd1	červeně
kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
květina	květina	k1gFnSc1	květina
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
severoamerický	severoamerický	k2eAgInSc1d1	severoamerický
druh	druh	k1gInSc1	druh
rodu	rod	k1gInSc2	rod
dlužicha	dlužich	k1gMnSc2	dlužich
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
ČR	ČR	kA	ČR
pěstován	pěstován	k2eAgMnSc1d1	pěstován
jako	jako	k8xC	jako
okrasná	okrasný	k2eAgFnSc1d1	okrasná
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
nároky	nárok	k1gInPc7	nárok
na	na	k7c6	na
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
péči	péče	k1gFnSc6	péče
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jihozápadu	jihozápad	k1gInSc2	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
Arizony	Arizona	k1gFnPc1	Arizona
<g/>
,	,	kIx,	,
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vzhled	vzhled	k1gInSc4	vzhled
i	i	k8xC	i
odolnost	odolnost	k1gFnSc4	odolnost
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
chladnějších	chladný	k2eAgInPc2d2	chladnější
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
položených	položený	k2eAgFnPc2d1	položená
částí	část	k1gFnPc2	část
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
domovině	domovina	k1gFnSc6	domovina
planě	planě	k6eAd1	planě
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
až	až	k9	až
2600	[number]	k4	2600
m	m	kA	m
na	na	k7c6	na
kamenité	kamenitý	k2eAgFnSc6d1	kamenitá
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgFnSc6d1	vlhká
půdě	půda	k1gFnSc6	půda
v	v	k7c6	v
částečném	částečný	k2eAgInSc6d1	částečný
stínu	stín	k1gInSc6	stín
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
podmínkách	podmínka	k1gFnPc6	podmínka
dobře	dobře	k6eAd1	dobře
přezimuje	přezimovat	k5eAaBmIp3nS	přezimovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
ji	on	k3xPp3gFnSc4	on
prospívá	prospívat	k5eAaImIp3nS	prospívat
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
<g/>
,	,	kIx,	,
přiměřená	přiměřený	k2eAgFnSc1d1	přiměřená
zálivka	zálivka	k1gFnSc1	zálivka
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
déletrvající	déletrvající	k2eAgNnSc1d1	déletrvající
sucho	sucho	k1gNnSc1	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dostatku	dostatek	k1gInSc6	dostatek
vláhy	vláha	k1gFnSc2	vláha
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
na	na	k7c6	na
plném	plný	k2eAgNnSc6d1	plné
slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
v	v	k7c6	v
polostínu	polostín	k1gInSc6	polostín
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
na	na	k7c6	na
nevýživné	výživný	k2eNgFnSc6d1	nevýživná
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgNnSc4d1	bohaté
kvetení	kvetení	k1gNnSc4	kvetení
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutná	nutný	k2eAgFnSc1d1	nutná
zemina	zemina	k1gFnSc1	zemina
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
humusu	humus	k1gInSc2	humus
a	a	k8xC	a
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhověký	dlouhověký	k2eAgInSc1d1	dlouhověký
hemikryptofyt	hemikryptofyt	k1gInSc1	hemikryptofyt
rašící	rašící	k2eAgInSc1d1	rašící
počátkem	počátkem	k7c2	počátkem
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
ploidie	ploidie	k1gFnSc1	ploidie
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
n	n	k0	n
=	=	kIx~	=
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dlužicha	Dlužich	k1gMnSc4	Dlužich
krvavá	krvavý	k2eAgFnSc1d1	krvavá
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
trsnatá	trsnatý	k2eAgFnSc1d1	trsnatá
<g/>
,	,	kIx,	,
žláznatě	žláznatě	k6eAd1	žláznatě
chlupatá	chlupatý	k2eAgFnSc1d1	chlupatá
bylina	bylina	k1gFnSc1	bylina
vyrůstající	vyrůstající	k2eAgFnSc1d1	vyrůstající
z	z	k7c2	z
tmavě	tmavě	k6eAd1	tmavě
hnědého	hnědý	k2eAgInSc2d1	hnědý
<g/>
,	,	kIx,	,
větveného	větvený	k2eAgInSc2d1	větvený
oddenku	oddenek	k1gInSc2	oddenek
až	až	k9	až
10	[number]	k4	10
cm	cm	kA	cm
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
<g/>
,	,	kIx,	,
0,5	[number]	k4	0,5
cm	cm	kA	cm
tlustého	tlusté	k1gNnSc2	tlusté
a	a	k8xC	a
s	s	k7c7	s
početnými	početný	k2eAgInPc7d1	početný
adventivními	adventivní	k2eAgInPc7d1	adventivní
kořeny	kořen	k1gInPc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Lodyhy	lodyha	k1gFnPc4	lodyha
vysoké	vysoká	k1gFnSc2	vysoká
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
cm	cm	kA	cm
jsou	být	k5eAaImIp3nP	být
přímé	přímý	k2eAgFnPc1d1	přímá
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
bezlisté	bezlistý	k2eAgInPc1d1	bezlistý
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgInPc1d1	tenký
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgInPc1d1	pevný
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
až	až	k9	až
v	v	k7c6	v
květenství	květenství	k1gNnSc6	květenství
<g/>
.	.	kIx.	.
</s>
<s>
Početné	početný	k2eAgInPc1d1	početný
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
cm	cm	kA	cm
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
růžici	růžice	k1gFnSc6	růžice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
řapíkaté	řapíkatý	k2eAgFnPc1d1	řapíkatá
<g/>
,	,	kIx,	,
čepele	čepel	k1gFnPc1	čepel
mají	mít	k5eAaImIp3nP	mít
kruhovité	kruhovitý	k2eAgFnPc1d1	kruhovitá
<g/>
,	,	kIx,	,
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
nebo	nebo	k8xC	nebo
ledvinovité	ledvinovitý	k2eAgFnPc1d1	ledvinovitá
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
srdčité	srdčitý	k2eAgFnSc6d1	srdčitá
<g/>
,	,	kIx,	,
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
plytce	plytce	k6eAd1	plytce
laločnaté	laločnatý	k2eAgInPc1d1	laločnatý
<g/>
,	,	kIx,	,
vroubkované	vroubkovaný	k2eAgInPc1d1	vroubkovaný
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zbarveny	zbarven	k2eAgInPc1d1	zbarven
tmavě	tmavě	k6eAd1	tmavě
zeleně	zeleň	k1gFnPc1	zeleň
se	s	k7c7	s
světlejším	světlý	k2eAgNnSc7d2	světlejší
mramorováním	mramorování	k1gNnSc7	mramorování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
lodyhy	lodyha	k1gFnSc2	lodyha
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
v	v	k7c6	v
řídké	řídký	k2eAgFnSc6d1	řídká
latě	lata	k1gFnSc6	lata
bez	bez	k7c2	bez
listenů	listen	k1gInPc2	listen
řada	řada	k1gFnSc1	řada
růžových	růžový	k2eAgFnPc2d1	růžová
až	až	k8xS	až
šarlatově	šarlatově	k6eAd1	šarlatově
červených	červená	k1gFnPc6	červená
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
zvonkovitých	zvonkovitý	k2eAgInPc2d1	zvonkovitý
<g/>
,	,	kIx,	,
pětičetných	pětičetný	k2eAgInPc2d1	pětičetný
květů	květ	k1gInPc2	květ
asi	asi	k9	asi
0,5	[number]	k4	0,5
cm	cm	kA	cm
širokých	široký	k2eAgFnPc2d1	široká
a	a	k8xC	a
1	[number]	k4	1
cm	cm	kA	cm
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Kališní	kališní	k2eAgInPc1d1	kališní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
žláznaté	žláznatý	k2eAgInPc1d1	žláznatý
<g/>
,	,	kIx,	,
načervenalé	načervenalý	k2eAgInPc1d1	načervenalý
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
třetiny	třetina	k1gFnSc2	třetina
srostlé	srostlý	k2eAgFnPc1d1	srostlá
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
zvonek	zvonek	k1gInSc4	zvonek
obklopující	obklopující	k2eAgInPc4d1	obklopující
kratší	krátký	k2eAgInPc4d2	kratší
korunní	korunní	k2eAgInPc4d1	korunní
lístky	lístek	k1gInPc4	lístek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
růžové	růžový	k2eAgNnSc4d1	růžové
až	až	k8xS	až
červené	červený	k2eAgNnSc4d1	červené
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
eliptické	eliptický	k2eAgNnSc1d1	eliptické
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
krátkých	krátký	k2eAgFnPc2d1	krátká
tyčinek	tyčinka	k1gFnPc2	tyčinka
nese	nést	k5eAaImIp3nS	nést
žluté	žlutý	k2eAgInPc1d1	žlutý
prašníky	prašník	k1gInPc1	prašník
<g/>
,	,	kIx,	,
pestík	pestík	k1gInSc1	pestík
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
bliznu	blizna	k1gFnSc4	blizna
<g/>
.	.	kIx.	.
</s>
<s>
Opylovány	opylován	k2eAgFnPc1d1	opylována
jsou	být	k5eAaImIp3nP	být
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
především	především	k9	především
motýly	motýl	k1gMnPc4	motýl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
jednopouzdrá	jednopouzdrý	k2eAgFnSc1d1	jednopouzdrá
<g/>
,	,	kIx,	,
oválná	oválný	k2eAgFnSc1d1	oválná
<g/>
,	,	kIx,	,
5	[number]	k4	5
mm	mm	kA	mm
velká	velký	k2eAgFnSc1d1	velká
tobolka	tobolka	k1gFnSc1	tobolka
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
početná	početný	k2eAgFnSc1d1	početná
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědý	k2eAgNnPc1d1	hnědé
<g/>
,	,	kIx,	,
eliptická	eliptický	k2eAgNnPc1d1	eliptické
semena	semeno	k1gNnPc1	semeno
asi	asi	k9	asi
0,5	[number]	k4	0,5
mm	mm	kA	mm
velká	velká	k1gFnSc1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
dělení	dělení	k1gNnSc3	dělení
trsů	trs	k1gInPc2	trs
nebo	nebo	k8xC	nebo
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
však	však	k9	však
nemusí	muset	k5eNaImIp3nP	muset
vyrůst	vyrůst	k5eAaPmF	vyrůst
potomstvo	potomstvo	k1gNnSc4	potomstvo
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Dlužicha	Dlužich	k1gMnSc4	Dlužich
krvavá	krvavý	k2eAgFnSc1d1	krvavá
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zahradnické	zahradnický	k2eAgFnSc3d1	zahradnická
praxi	praxe	k1gFnSc3	praxe
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
volně	volně	k6eAd1	volně
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
trvalku	trvalka	k1gFnSc4	trvalka
záhonového	záhonový	k2eAgInSc2d1	záhonový
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
hybridy	hybrid	k1gInPc1	hybrid
bývají	bývat	k5eAaImIp3nP	bývat
plodné	plodný	k2eAgInPc1d1	plodný
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zkřížit	zkřížit	k5eAaPmF	zkřížit
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
a	a	k8xC	a
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
aranžérské	aranžérský	k2eAgFnPc4d1	aranžérská
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
porostlá	porostlý	k2eAgFnSc1d1	porostlá
ozdobnými	ozdobný	k2eAgInPc7d1	ozdobný
listy	list	k1gInPc7	list
od	od	k7c2	od
časného	časný	k2eAgNnSc2d1	časné
jara	jaro	k1gNnSc2	jaro
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
a	a	k8xC	a
kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
kultivarů	kultivar	k1gInPc2	kultivar
s	s	k7c7	s
květy	květ	k1gInPc7	květ
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
od	od	k7c2	od
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
,	,	kIx,	,
nazelenalé	nazelenalý	k2eAgFnSc2d1	nazelenalá
a	a	k8xC	a
růžové	růžový	k2eAgFnSc2d1	růžová
až	až	k9	až
po	po	k7c6	po
červené	červený	k2eAgFnSc6d1	červená
<g/>
,	,	kIx,	,
oranžové	oranžový	k2eAgFnSc6d1	oranžová
<g/>
,	,	kIx,	,
vínové	vínový	k2eAgFnSc6d1	vínová
i	i	k8xC	i
pestré	pestrý	k2eAgFnSc6d1	pestrá
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
bývají	bývat	k5eAaImIp3nP	bývat
různě	různě	k6eAd1	různě
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
odlišně	odlišně	k6eAd1	odlišně
bohatá	bohatý	k2eAgFnSc1d1	bohatá
jejich	jejich	k3xOp3gNnSc4	jejich
květenství	květenství	k1gNnSc4	květenství
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
odrůdy	odrůda	k1gFnPc4	odrůda
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
odkvetlých	odkvetlý	k2eAgNnPc2d1	odkvetlé
květenství	květenství	k1gNnPc2	květenství
remontují	remontovat	k5eAaImIp3nP	remontovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dlužicha	dlužich	k1gMnSc2	dlužich
krvavá	krvavý	k2eAgNnPc4d1	krvavé
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Heuchera	Heucher	k1gMnSc2	Heucher
sanguinea	sanguineus	k1gMnSc2	sanguineus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
