<s>
Kališní	kališní	k2eAgInPc1d1	kališní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
žláznaté	žláznatý	k2eAgInPc1d1	žláznatý
<g/>
,	,	kIx,	,
načervenalé	načervenalý	k2eAgInPc1d1	načervenalý
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
třetiny	třetina	k1gFnSc2	třetina
srostlé	srostlý	k2eAgFnPc1d1	srostlá
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
zvonek	zvonek	k1gInSc4	zvonek
obklopující	obklopující	k2eAgInPc4d1	obklopující
kratší	krátký	k2eAgInPc4d2	kratší
korunní	korunní	k2eAgInPc4d1	korunní
lístky	lístek	k1gInPc4	lístek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
růžové	růžový	k2eAgNnSc4d1	růžové
až	až	k8xS	až
červené	červený	k2eAgNnSc4d1	červené
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
eliptické	eliptický	k2eAgNnSc1d1	eliptické
<g/>
.	.	kIx.	.
</s>
