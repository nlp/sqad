<s>
Kapr	kapr	k1gMnSc1	kapr
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
velkých	velký	k2eAgFnPc2d1	velká
ryb	ryba	k1gFnPc2	ryba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kaprovitých	kaprovití	k1gMnPc2	kaprovití
<g/>
.	.	kIx.	.
</s>
<s>
Kapři	kapr	k1gMnPc1	kapr
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
značné	značný	k2eAgFnPc4d1	značná
délky	délka	k1gFnPc4	délka
<g/>
,	,	kIx,	,
až	až	k9	až
1	[number]	k4	1
metr	metr	k1gInSc4	metr
při	při	k7c6	při
hmotnosti	hmotnost	k1gFnSc6	hmotnost
20	[number]	k4	20
kilogramů	kilogram	k1gInPc2	kilogram
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
šupiny	šupina	k1gFnPc1	šupina
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgFnPc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
dva	dva	k4xCgInPc4	dva
páry	pár	k1gInPc1	pár
vousků	vousek	k1gInPc2	vousek
<g/>
.	.	kIx.	.
</s>
<s>
Požerákové	požerákový	k2eAgInPc1d1	požerákový
zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
řadách	řada	k1gFnPc6	řada
(	(	kIx(	(
<g/>
vzorec	vzorec	k1gInSc4	vzorec
1.1	[number]	k4	1.1
<g/>
.3	.3	k4	.3
<g/>
–	–	k?	–
<g/>
3.1	[number]	k4	3.1
<g/>
.1	.1	k4	.1
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzácně	vzácně	k6eAd1	vzácně
1.2	[number]	k4	1.2
<g/>
.3	.3	k4	.3
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
3.2	[number]	k4	3.2
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trávicí	trávicí	k2eAgInSc1d1	trávicí
trakt	trakt	k1gInSc1	trakt
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
střevo	střevo	k1gNnSc1	střevo
2,5	[number]	k4	2,5
až	až	k8xS	až
3	[number]	k4	3
<g/>
×	×	k?	×
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
délku	délka	k1gFnSc4	délka
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiné	jiný	k2eAgFnPc1d1	jiná
ryby	ryba	k1gFnPc1	ryba
z	z	k7c2	z
podčeledi	podčeleď	k1gFnSc2	podčeleď
Cyprininae	Cyprinina	k1gFnSc2	Cyprinina
mají	mít	k5eAaImIp3nP	mít
pilovitý	pilovitý	k2eAgInSc4d1	pilovitý
poslední	poslední	k2eAgInSc4d1	poslední
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
paprsek	paprsek	k1gInSc4	paprsek
ve	v	k7c6	v
hřbetní	hřbetní	k2eAgFnSc6d1	hřbetní
a	a	k8xC	a
řitní	řitní	k2eAgFnSc3d1	řitní
ploutvi	ploutev	k1gFnSc3	ploutev
<g/>
.	.	kIx.	.
kapr	kapr	k1gMnSc1	kapr
ostrohřbetý	ostrohřbetý	k2eAgMnSc1d1	ostrohřbetý
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
acutidorsalis	acutidorsalis	k1gFnSc2	acutidorsalis
Wang	Wang	k1gMnSc1	Wang
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
vousatý	vousatý	k2eAgMnSc1d1	vousatý
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
barbatus	barbatus	k1gMnSc1	barbatus
Chen	Chen	k1gMnSc1	Chen
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
<g />
.	.	kIx.	.
</s>
<s>
carpio	carpio	k6eAd1	carpio
Linné	Linný	k2eAgInPc1d1	Linný
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
středovietnamský	středovietnamský	k2eAgMnSc1d1	středovietnamský
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
centralus	centralus	k1gMnSc1	centralus
Nguyen	Nguyen	k2eAgMnSc1d1	Nguyen
et	et	k?	et
Mai	Mai	k1gMnSc1	Mai
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
chilia	chilium	k1gNnSc2	chilium
Wu	Wu	k1gMnSc1	Wu
<g/>
,	,	kIx,	,
Yang	Yang	k1gMnSc1	Yang
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
in	in	k?	in
Wu	Wu	k1gMnSc1	Wu
<g/>
,	,	kIx,	,
Yang	Yang	k1gMnSc1	Yang
<g/>
,	,	kIx,	,
Yue	Yue	k1gMnSc1	Yue
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
Cyprinus	Cyprinus	k1gInSc4	Cyprinus
cocsa	cocsa	k1gFnSc1	cocsa
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
1822	[number]	k4	1822
kapr	kapr	k1gMnSc1	kapr
dajský	dajský	k1gMnSc1	dajský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
dai	dai	k?	dai
Nguyen	Nguyen	k1gInSc1	Nguyen
et	et	k?	et
Doan	Doan	k1gInSc1	Doan
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
mekongský	mekongský	k2eAgMnSc1d1	mekongský
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
daliensis	daliensis	k1gFnSc2	daliensis
Chen	Chen	k1gMnSc1	Chen
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
severovietnamský	severovietnamský	k2eAgMnSc1d1	severovietnamský
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
exophthalmus	exophthalmus	k1gMnSc1	exophthalmus
Mai	Mai	k1gMnSc1	Mai
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
fuxianensis	fuxianensis	k1gFnSc2	fuxianensis
Yang	Yanga	k1gFnPc2	Yanga
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
in	in	k?	in
Chen	Chen	k1gMnSc1	Chen
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1977	[number]	k4	1977
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
hyperdorsalis	hyperdorsalis	k1gFnSc2	hyperdorsalis
Nguyen	Nguyna	k1gFnPc2	Nguyna
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
kapr	kapr	k1gMnSc1	kapr
jilunský	jilunský	k1gMnSc1	jilunský
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
ilishaestomus	ilishaestomus	k1gMnSc1	ilishaestomus
Chen	Chen	k1gMnSc1	Chen
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
salvínský	salvínský	k2eAgMnSc1d1	salvínský
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
intha	inth	k1gMnSc4	inth
Annandale	Annandala	k1gFnSc6	Annandala
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
dlouhoploutvý	dlouhoploutvý	k2eAgMnSc1d1	dlouhoploutvý
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
longipectoralis	longipectoralis	k1gFnSc2	longipectoralis
Chen	Chen	k1gMnSc1	Chen
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
čínský	čínský	k2eAgMnSc1d1	čínský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
longzhouensis	longzhouensis	k1gFnSc2	longzhouensis
Yang	Yang	k1gMnSc1	Yang
et	et	k?	et
Hwang	Hwang	k1gMnSc1	Hwang
in	in	k?	in
Chen	Chen	k1gMnSc1	Chen
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
megalophthalmus	megalophthalmus	k1gMnSc1	megalophthalmus
Wu	Wu	k1gFnPc2	Wu
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
kapr	kapr	k1gMnSc1	kapr
jihočínský	jihočínský	k2eAgMnSc1d1	jihočínský
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
micristius	micristius	k1gMnSc1	micristius
Regan	Regan	k1gMnSc1	Regan
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
mnohopruhý	mnohopruhý	k2eAgMnSc1d1	mnohopruhý
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
multitaeniata	multitaeniat	k1gMnSc2	multitaeniat
Pellegrin	Pellegrin	k1gInSc1	Pellegrin
et	et	k?	et
Chevey	Chevea	k1gFnSc2	Chevea
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
Pellegrinův	Pellegrinův	k2eAgMnSc1d1	Pellegrinův
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
pellegrini	pellegrin	k1gMnPc1	pellegrin
Tchang	Tchang	k1gMnSc1	Tchang
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
qionghaiensis	qionghaiensis	k1gFnSc2	qionghaiensis
Liu	Liu	k1gFnSc2	Liu
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
rubrofuscus	rubrofuscus	k1gMnSc1	rubrofuscus
Lacepè	Lacepè	k1gMnSc1	Lacepè
<g/>
,	,	kIx,	,
1803	[number]	k4	1803
kapr	kapr	k1gMnSc1	kapr
šipingský	šipingský	k1gMnSc1	šipingský
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
yilongensis	yilongensis	k1gFnSc2	yilongensis
Yang	Yang	k1gInSc1	Yang
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
in	in	k?	in
Chen	Chen	k1gMnSc1	Chen
et	et	k?	et
Huang	Huang	k1gMnSc1	Huang
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
kapr	kapr	k1gMnSc1	kapr
junanský	junanský	k2eAgMnSc1d1	junanský
(	(	kIx(	(
<g/>
Cyprinus	Cyprinus	k1gMnSc1	Cyprinus
yunnanensis	yunnanensis	k1gFnSc2	yunnanensis
Tchang	Tchang	k1gMnSc1	Tchang
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
Kapr	kapr	k1gMnSc1	kapr
obecný	obecný	k2eAgMnSc1d1	obecný
Vánoční	vánoční	k2eAgMnSc1d1	vánoční
kapr	kapr	k1gMnSc1	kapr
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kapr	kapr	k1gMnSc1	kapr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kapr	kapr	k1gMnSc1	kapr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Cyprinus	Cyprinus	k1gInSc1	Cyprinus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
