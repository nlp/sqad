<s>
Smrk	smrk	k1gInSc1
Koyamův	Koyamův	k2eAgInSc1d1
</s>
<s>
Smrk	smrk	k1gInSc1
Koyamův	Koyamův	k2eAgInSc4d1
Smrk	smrk	k1gInSc4
Koyamův	Koyamův	k2eAgInSc4d1
(	(	kIx(
<g/>
Picea	Picea	k1gFnSc1
koyamae	koyama	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pěstovaný	pěstovaný	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
nahosemenné	nahosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Pinophyta	Pinophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
jehličnany	jehličnan	k1gInPc1
(	(	kIx(
<g/>
Pinopsida	Pinopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
borovicotvaré	borovicotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Pinales	Pinales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
borovicovité	borovicovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Pinaceae	Pinacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
Picea	Picea	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Picea	Picea	k1gMnSc1
koyamaeShiras	koyamaeShiras	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1913	#num#	k4
Synonyma	synonymum	k1gNnSc2
</s>
<s>
Picea	Pice	k2eAgNnPc1d1
koyamai	koyamai	k1gNnPc1
(	(	kIx(
<g/>
orth	orth	k1gInSc1
<g/>
.	.	kIx.
err	err	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Smrk	smrk	k1gInSc1
Koyamův	Koyamův	k2eAgInSc1d1
(	(	kIx(
<g/>
Picea	Pice	k1gFnSc1
koyamae	koyamae	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
jehličnatého	jehličnatý	k2eAgInSc2d1
stromu	strom	k1gInSc2
původem	původ	k1gInSc7
z	z	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
popsán	popsat	k5eAaPmNgInS
po	po	k7c6
japonském	japonský	k2eAgInSc6d1
botanikovi	botanik	k1gMnSc3
Mitsuaovi	Mitsuaa	k1gMnSc3
Koyamovi	Koyam	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Shirasawa	Shirasawa	k1gMnSc1
původně	původně	k6eAd1
popsal	popsat	k5eAaPmAgMnS
druh	druh	k1gInSc4
jako	jako	k8xS,k8xC
Picea	Pice	k2eAgNnPc4d1
koyamai	koyamai	k1gNnPc4
<g/>
,	,	kIx,
podle	podle	k7c2
mezinárodního	mezinárodní	k2eAgInSc2d1
kódu	kód	k1gInSc2
botanické	botanický	k2eAgFnSc2d1
nomenklatury	nomenklatura	k1gFnSc2
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
pravopisná	pravopisný	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
později	pozdě	k6eAd2
opravena	opravit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
strom	strom	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1
dorůstá	dorůstat	k5eAaImIp3nS
až	až	k9
20	#num#	k4
m	m	kA
výšky	výška	k1gFnSc2
a	a	k8xC
průměru	průměr	k1gInSc2
kmene	kmen	k1gInSc2
až	až	k6eAd1
1	#num#	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Borka	borka	k1gFnSc1
je	být	k5eAaImIp3nS
šedohnědá	šedohnědý	k2eAgFnSc1d1
až	až	k6eAd1
černohnědá	černohnědý	k2eAgFnSc1d1
<g/>
,	,	kIx,
u	u	k7c2
starších	starý	k2eAgInPc2d2
exemplářů	exemplář	k1gInPc2
rozpraskaná	rozpraskaný	k2eAgFnSc1d1
do	do	k7c2
šupin	šupina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Větvičky	větvička	k1gFnPc1
mají	mít	k5eAaImIp3nP
hnědou	hnědý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
rýhované	rýhovaný	k2eAgFnPc1d1
a	a	k8xC
lysé	lysý	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jehlice	jehlice	k1gFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
průřezu	průřez	k1gInSc6
čtverhranné	čtverhranný	k2eAgNnSc1d1
<g/>
,	,	kIx,
asi	asi	k9
0,8	0,8	k4
<g/>
−	−	k?
<g/>
1,6	1,6	k4
cm	cm	kA
dlouhé	dlouhý	k2eAgNnSc1d1
a	a	k8xC
asi	asi	k9
1,5	1,5	k4
mm	mm	kA
široké	široký	k2eAgFnPc1d1
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
špičaté	špičatý	k2eAgNnSc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
tupé	tupý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Samičí	samičí	k2eAgFnSc2d1
šišky	šiška	k1gFnSc2
jsou	být	k5eAaImIp3nP
zpočátku	zpočátku	k6eAd1
rezatě	rezatě	k6eAd1
purpurové	purpurový	k2eAgNnSc1d1
<g/>
,	,	kIx,
za	za	k7c4
zralosti	zralost	k1gFnPc4
pak	pak	k6eAd1
hnědé	hnědý	k2eAgFnPc1d1
<g/>
,	,	kIx,
asi	asi	k9
4	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
cm	cm	kA
dlouhé	dlouhý	k2eAgNnSc1d1
a	a	k8xC
asi	asi	k9
2	#num#	k4
<g/>
–	–	k?
<g/>
2,5	2,5	k4
cm	cm	kA
široké	široký	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šupiny	šupina	k1gFnSc2
samičích	samičí	k2eAgFnPc2d1
šišek	šiška	k1gFnPc2
jsou	být	k5eAaImIp3nP
za	za	k7c4
zralosti	zralost	k1gFnPc4
okrouhlé	okrouhlý	k2eAgFnPc4d1
až	až	k8xS
obvejčité	obvejčitý	k2eAgFnPc4d1
<g/>
,	,	kIx,
asi	asi	k9
15	#num#	k4
mm	mm	kA
dlouhé	dlouhý	k2eAgNnSc1d1
a	a	k8xC
asi	asi	k9
13	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
široké	široký	k2eAgInPc1d1
<g/>
,	,	kIx,
na	na	k7c6
horním	horní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
jemně	jemně	k6eAd1
zoubkované	zoubkovaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semena	semeno	k1gNnPc1
jsou	být	k5eAaImIp3nP
křídlatá	křídlatý	k2eAgNnPc1d1
<g/>
,	,	kIx,
křídla	křídlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
asi	asi	k9
10	#num#	k4
mm	mm	kA
dlouhá	dlouhý	k2eAgFnSc1d1
a	a	k8xC
5	#num#	k4
mm	mm	kA
široká	široký	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Větévka	větévka	k1gFnSc1
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Smrk	smrk	k1gInSc1
Koyamův	Koyamův	k2eAgInSc1d1
přirozeně	přirozeně	k6eAd1
roste	růst	k5eAaImIp3nS
v	v	k7c6
malém	malý	k2eAgInSc6d1
areálu	areál	k1gInSc6
na	na	k7c6
ostrově	ostrov	k1gInSc6
Honšú	Honšú	k1gFnSc2
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
dnes	dnes	k6eAd1
vzácný	vzácný	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
,	,	kIx,
celkově	celkově	k6eAd1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
řádově	řádově	k6eAd1
jen	jen	k9
stovky	stovka	k1gFnPc1
jedinců	jedinec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populace	populace	k1gFnSc1
je	být	k5eAaImIp3nS
ohrožována	ohrožovat	k5eAaImNgFnS
také	také	k9
častými	častý	k2eAgInPc7d1
tajfuny	tajfun	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc4d1
druh	druh	k1gInSc4
podle	podle	k7c2
IUCN	IUCN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Pro	pro	k7c4
ekonomické	ekonomický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
vzácný	vzácný	k2eAgMnSc1d1
<g/>
,	,	kIx,
občas	občas	k6eAd1
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
jako	jako	k9
okrasný	okrasný	k2eAgInSc1d1
strom	strom	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
ho	on	k3xPp3gInSc4
můžeme	moct	k5eAaImIp1nP
vidět	vidět	k5eAaImF
jen	jen	k9
výjimečně	výjimečně	k6eAd1
v	v	k7c6
arboretech	arboretum	k1gNnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
Kunraticích	Kunratice	k1gFnPc6
u	u	k7c2
Šluknova	Šluknov	k1gInSc2
či	či	k8xC
v	v	k7c6
Průhonicích	Průhonice	k1gFnPc6
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
The	The	k1gFnPc2
Gymnosperm	Gymnosperm	k1gInSc1
Database	Databasa	k1gFnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Koyama	Koyama	k1gFnSc1
Spruce	Spruce	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Picea	Pice	k2eAgFnSc1d1
koyamae	koyamae	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Florius	Florius	k1gInSc1
-	-	kIx~
katalog	katalog	k1gInSc1
botanických	botanický	k2eAgFnPc2d1
zahrad	zahrada	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
smrk	smrk	k1gInSc1
Koyamův	Koyamův	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Picea	Pice	k1gInSc2
koyamae	koyamaat	k5eAaPmIp3nS
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rod	rod	k1gInSc1
smrk	smrk	k1gInSc1
(	(	kIx(
<g/>
Picea	Picea	k1gFnSc1
<g/>
)	)	kIx)
Evropa	Evropa	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
•	•	k?
smrk	smrk	k1gInSc1
omorika	omorika	k1gFnSc1
Asie	Asie	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc4
ajanský	ajanský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Glehnův	Glehnův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
dvoubarvý	dvoubarvý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
chupejský	chupejský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Koyamův	Koyamův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
korejský	korejský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
liťiangský	liťiangský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
lesklý	lesklý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Maximovičův	Maximovičův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Meyerův	Meyerův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
nachový	nachový	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Schrenkův	Schrenkův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sibiřský	sibiřský	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
štětinatý	štětinatý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
východní	východní	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc1
Wilsonův	Wilsonův	k2eAgInSc1d1
•	•	k?
Picea	Picea	k1gFnSc1
brachytyla	brachytýt	k5eAaPmAgFnS
•	•	k?
Picea	Pice	k2eAgFnSc1d1
crassifolia	crassifolia	k1gFnSc1
•	•	k?
Picea	Picea	k1gFnSc1
farreri	farrer	k1gFnSc2
•	•	k?
Picea	Pice	k2eAgFnSc1d1
morrisonicola	morrisonicola	k1gFnSc1
•	•	k?
Picea	Pice	k2eAgFnSc1d1
spinulosa	spinulosa	k1gFnSc1
•	•	k?
Picea	Picea	k1gFnSc1
smithiana	smithiana	k1gFnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
smrk	smrk	k1gInSc4
Brewerův	Brewerův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
Engelmannův	Engelmannův	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
černý	černý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
červený	červený	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
pichlavý	pichlavý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sivý	sivý	k2eAgInSc4d1
•	•	k?
smrk	smrk	k1gInSc4
sitka	sitek	k1gMnSc2
•	•	k?
Picea	Piceus	k1gMnSc2
chihuahuana	chihuahuan	k1gMnSc2
•	•	k?
Picea	Picea	k1gMnSc1
martinezii	martinezie	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnPc1
|	|	kIx~
Rostliny	rostlina	k1gFnPc1
|	|	kIx~
Stromy	strom	k1gInPc1
</s>
