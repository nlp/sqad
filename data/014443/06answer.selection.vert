<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
strom	strom	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1
dorůstá	dorůstat	k5eAaImIp3nS
až	až	k9
20	#num#	k4
m	m	kA
výšky	výška	k1gFnSc2
a	a	k8xC
průměru	průměr	k1gInSc2
kmene	kmen	k1gInSc2
až	až	k6eAd1
1	#num#	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Borka	borka	k1gFnSc1
je	být	k5eAaImIp3nS
šedohnědá	šedohnědý	k2eAgFnSc1d1
až	až	k6eAd1
černohnědá	černohnědý	k2eAgFnSc1d1
<g/>
,	,	kIx,
u	u	k7c2
starších	starý	k2eAgInPc2d2
exemplářů	exemplář	k1gInPc2
rozpraskaná	rozpraskaný	k2eAgFnSc1d1
do	do	k7c2
šupin	šupina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>