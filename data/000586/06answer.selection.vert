<s>
Mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
patří	patřit	k5eAaImIp3nS	patřit
helium	helium	k1gNnSc1	helium
<g/>
,	,	kIx,	,
neon	neon	k1gInSc1	neon
<g/>
,	,	kIx,	,
argon	argon	k1gInSc1	argon
<g/>
,	,	kIx,	,
krypton	krypton	k1gInSc1	krypton
<g/>
,	,	kIx,	,
xenon	xenon	k1gInSc1	xenon
a	a	k8xC	a
radon	radon	k1gInSc1	radon
<g/>
.	.	kIx.	.
</s>
