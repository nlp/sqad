<s>
Vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
ušlechtilé	ušlechtilý	k2eAgInPc1d1	ušlechtilý
plyny	plyn	k1gInPc1	plyn
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gInPc4	člen
18	[number]	k4	18
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
patří	patřit	k5eAaImIp3nS	patřit
helium	helium	k1gNnSc1	helium
<g/>
,	,	kIx,	,
neon	neon	k1gInSc1	neon
<g/>
,	,	kIx,	,
argon	argon	k1gInSc1	argon
<g/>
,	,	kIx,	,
krypton	krypton	k1gInSc1	krypton
<g/>
,	,	kIx,	,
xenon	xenon	k1gInSc1	xenon
a	a	k8xC	a
radon	radon	k1gInSc1	radon
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
připojuje	připojovat	k5eAaImIp3nS	připojovat
ještě	ještě	k6eAd1	ještě
Oganesson	Oganessona	k1gFnPc2	Oganessona
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnPc1	jeho
vlastnosti	vlastnost	k1gFnPc1	vlastnost
zatím	zatím	k6eAd1	zatím
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
netečné	tečný	k2eNgMnPc4d1	tečný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
označení	označení	k1gNnSc4	označení
netečné	tečný	k2eNgInPc4d1	netečný
či	či	k8xC	či
inertní	inertní	k2eAgInPc4d1	inertní
plyny	plyn	k1gInPc4	plyn
je	být	k5eAaImIp3nS	být
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
nízké	nízký	k2eAgFnSc2d1	nízká
reaktivity	reaktivita	k1gFnSc2	reaktivita
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
specifických	specifický	k2eAgFnPc2d1	specifická
chemických	chemický	k2eAgFnPc2d1	chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
je	být	k5eAaImIp3nS	být
dokonalé	dokonalý	k2eAgNnSc1d1	dokonalé
zaplnění	zaplnění	k1gNnSc1	zaplnění
vnější	vnější	k2eAgFnSc2d1	vnější
elektronové	elektronový	k2eAgFnSc2d1	elektronová
slupky	slupka	k1gFnSc2	slupka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
atom	atom	k1gInSc4	atom
se	s	k7c7	s
zaplněným	zaplněný	k2eAgInSc7d1	zaplněný
valenčním	valenční	k2eAgInSc7d1	valenční
orbitalem	orbital	k1gInSc7	orbital
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
přijímá	přijímat	k5eAaImIp3nS	přijímat
či	či	k8xC	či
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
vazebný	vazebný	k2eAgInSc1d1	vazebný
elektron	elektron	k1gInSc1	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Objevitelem	objevitel	k1gMnSc7	objevitel
většiny	většina	k1gFnSc2	většina
ušlechtilých	ušlechtilý	k2eAgInPc2d1	ušlechtilý
plynů	plyn	k1gInPc2	plyn
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
chemik	chemik	k1gMnSc1	chemik
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
jsou	být	k5eAaImIp3nP	být
plynné	plynný	k2eAgFnPc4d1	plynná
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nezúčastňují	zúčastňovat	k5eNaImIp3nP	zúčastňovat
žádných	žádný	k3yNgFnPc2	žádný
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
vytvořit	vytvořit	k5eAaPmF	vytvořit
některé	některý	k3yIgFnPc4	některý
sloučeniny	sloučenina	k1gFnPc4	sloučenina
např.	např.	kA	např.
fluorid	fluorid	k1gInSc4	fluorid
kryptonu	krypton	k1gInSc2	krypton
<g/>
,	,	kIx,	,
xenonu	xenon	k1gInSc2	xenon
a	a	k8xC	a
radonu	radon	k1gInSc2	radon
<g/>
.	.	kIx.	.
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1	chemická
reaktivita	reaktivita	k1gFnSc1	reaktivita
těchto	tento	k3xDgInPc2	tento
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
zcela	zcela	k6eAd1	zcela
zaplněné	zaplněný	k2eAgInPc1d1	zaplněný
valenční	valenční	k2eAgInPc1d1	valenční
orbitaly	orbital	k1gInPc1	orbital
s	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
p	p	k?	p
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
helium	helium	k1gNnSc4	helium
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
ionizační	ionizační	k2eAgFnSc4d1	ionizační
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jednoatomových	jednoatomový	k2eAgFnPc2d1	jednoatomový
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
nízké	nízký	k2eAgFnPc1d1	nízká
teploty	teplota	k1gFnPc1	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
(	(	kIx(	(
<g/>
helium	helium	k1gNnSc4	helium
dokonce	dokonce	k9	dokonce
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
nelze	lze	k6eNd1	lze
převést	převést	k5eAaPmF	převést
do	do	k7c2	do
tuhého	tuhý	k2eAgInSc2d1	tuhý
stavu	stav	k1gInSc2	stav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
je	být	k5eAaImIp3nS	být
nejunikátnější	unikátní	k2eAgNnSc1d3	nejunikátnější
helium	helium	k1gNnSc1	helium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
kapalní	kapalnit	k5eAaImIp3nS	kapalnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
stavu	stav	k1gInSc6	stav
má	mít	k5eAaImIp3nS	mít
supratekuté	supratekutý	k2eAgFnPc4d1	supratekutá
a	a	k8xC	a
supravodivé	supravodivý	k2eAgFnPc4d1	supravodivá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
prvek	prvek	k1gInSc1	prvek
nemá	mít	k5eNaImIp3nS	mít
trojný	trojný	k2eAgInSc1d1	trojný
bod	bod	k1gInSc1	bod
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
látka	látka	k1gFnSc1	látka
může	moct	k5eAaImIp3nS	moct
současně	současně	k6eAd1	současně
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
plynném	plynný	k2eAgMnSc6d1	plynný
<g/>
,	,	kIx,	,
kapalném	kapalný	k2eAgInSc6d1	kapalný
a	a	k8xC	a
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
základní	základní	k2eAgFnSc1d1	základní
informace	informace	k1gFnSc1	informace
k	k	k7c3	k
vzácným	vzácný	k2eAgInPc3d1	vzácný
plynům	plyn	k1gInPc3	plyn
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Nejzastoupenější	Nejzastoupený	k2eAgInSc1d2	Nejzastoupený
je	být	k5eAaImIp3nS	být
argon	argon	k1gInSc1	argon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
0,93	[number]	k4	0,93
objemových	objemový	k2eAgFnPc2d1	objemová
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
helium	helium	k1gNnSc1	helium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
zemním	zemní	k2eAgInSc6d1	zemní
plynu	plyn	k1gInSc6	plyn
(	(	kIx(	(
<g/>
až	až	k9	až
25	[number]	k4	25
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
prvkem	prvek	k1gInSc7	prvek
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
jako	jako	k8xS	jako
takové	takový	k3xDgNnSc1	takový
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Radon	radon	k1gInSc1	radon
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vzniká	vznikat	k5eAaImIp3nS	vznikat
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
radia	radio	k1gNnSc2	radio
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
hornin	hornina	k1gFnPc2	hornina
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
radon	radon	k1gInSc1	radon
postupně	postupně	k6eAd1	postupně
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
problém	problém	k1gInSc4	problém
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
dané	daný	k2eAgFnSc2d1	daná
lokality	lokalita	k1gFnSc2	lokalita
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zvýšeného	zvýšený	k2eAgNnSc2d1	zvýšené
množství	množství	k1gNnSc2	množství
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Neon	neon	k1gInSc1	neon
<g/>
,	,	kIx,	,
argon	argon	k1gInSc1	argon
<g/>
,	,	kIx,	,
krypton	krypton	k1gInSc1	krypton
<g/>
,	,	kIx,	,
xenon	xenon	k1gInSc1	xenon
jsou	být	k5eAaImIp3nP	být
získávány	získávat	k5eAaImNgFnP	získávat
frakční	frakční	k2eAgFnSc7d1	frakční
destilací	destilace	k1gFnSc7	destilace
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
oddělením	oddělení	k1gNnSc7	oddělení
od	od	k7c2	od
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
radon	radon	k1gInSc1	radon
je	být	k5eAaImIp3nS	být
získáván	získávat	k5eAaImNgInS	získávat
z	z	k7c2	z
produktu	produkt	k1gInSc2	produkt
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
(	(	kIx(	(
<g/>
radnaté	radnatý	k2eAgFnPc1d1	radnatý
soli	sůl	k1gFnPc1	sůl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tržní	tržní	k2eAgFnPc1d1	tržní
ceny	cena	k1gFnPc1	cena
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
jejich	jejich	k3xOp3gInSc7	jejich
přirozeným	přirozený	k2eAgInSc7d1	přirozený
výskytem	výskyt	k1gInSc7	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Argon	argon	k1gInSc1	argon
je	být	k5eAaImIp3nS	být
nejlevnější	levný	k2eAgInSc1d3	nejlevnější
a	a	k8xC	a
xenon	xenon	k1gInSc1	xenon
nejdražší	drahý	k2eAgInSc1d3	nejdražší
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
pro	pro	k7c4	pro
laboratorní	laboratorní	k2eAgNnSc4d1	laboratorní
množství	množství	k1gNnSc4	množství
každého	každý	k3xTgInSc2	každý
plynu	plyn	k1gInSc2	plyn
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
vpravo	vpravo	k6eAd1	vpravo
(	(	kIx(	(
<g/>
USD	USD	kA	USD
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
radonu	radon	k1gInSc2	radon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
v	v	k7c6	v
osvětlovací	osvětlovací	k2eAgFnSc6d1	osvětlovací
technice	technika	k1gFnSc6	technika
jako	jako	k8xS	jako
výplň	výplň	k1gFnSc4	výplň
výbojek	výbojka	k1gFnPc2	výbojka
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
plnění	plnění	k1gNnSc6	plnění
balónů	balón	k1gInPc2	balón
<g/>
,	,	kIx,	,
balónků	balónek	k1gInPc2	balónek
<g/>
,	,	kIx,	,
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nafukovacích	nafukovací	k2eAgInPc2d1	nafukovací
předmětů	předmět	k1gInPc2	předmět
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vodíku	vodík	k1gInSc2	vodík
není	být	k5eNaImIp3nS	být
výbušné	výbušný	k2eAgNnSc1d1	výbušné
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
helium	helium	k1gNnSc1	helium
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
jako	jako	k8xC	jako
nosič	nosič	k1gInSc4	nosič
v	v	k7c6	v
plynové	plynový	k2eAgFnSc6d1	plynová
chromatografii	chromatografie	k1gFnSc6	chromatografie
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
při	při	k7c6	při
hmotnostní	hmotnostní	k2eAgFnSc6d1	hmotnostní
spektrometrii	spektrometrie	k1gFnSc6	spektrometrie
a	a	k8xC	a
při	při	k7c6	při
rentgenové	rentgenový	k2eAgFnSc6d1	rentgenová
fluorescenci	fluorescence	k1gFnSc6	fluorescence
<g/>
.	.	kIx.	.
</s>
<s>
Kapalné	kapalný	k2eAgNnSc1d1	kapalné
helium	helium	k1gNnSc1	helium
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
NMR	NMR	kA	NMR
(	(	kIx(	(
<g/>
nukleárně	nukleárně	k6eAd1	nukleárně
magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mimořádně	mimořádně	k6eAd1	mimořádně
nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
kapalné	kapalný	k2eAgNnSc4d1	kapalné
helium	helium	k1gNnSc4	helium
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgNnPc2d1	základní
médií	médium	k1gNnPc2	médium
pro	pro	k7c4	pro
kryogenní	kryogenní	k2eAgFnPc4d1	kryogenní
techniky	technika	k1gFnPc4	technika
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
i	i	k8xC	i
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
supravodivosti	supravodivost	k1gFnSc2	supravodivost
a	a	k8xC	a
supratekutosti	supratekutost	k1gFnSc2	supratekutost
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
inertnosti	inertnost	k1gFnSc3	inertnost
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
inertní	inertní	k2eAgFnSc1d1	inertní
atmosféra	atmosféra	k1gFnSc1	atmosféra
-	-	kIx~	-
zejména	zejména	k9	zejména
argon	argon	k1gInSc1	argon
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
hašení	hašení	k1gNnSc3	hašení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
stabilních	stabilní	k2eAgNnPc6d1	stabilní
hasicích	hasicí	k2eAgNnPc6d1	hasicí
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
používanými	používaný	k2eAgInPc7d1	používaný
hasebními	hasební	k2eAgInPc7d1	hasební
plyny	plyn	k1gInPc7	plyn
v	v	k7c6	v
budovách	budova	k1gFnPc6	budova
jsou	být	k5eAaImIp3nP	být
čistý	čistý	k2eAgInSc4d1	čistý
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
Argonit	Argonit	k1gInSc4	Argonit
a	a	k8xC	a
Inergen	Inergen	k1gInSc4	Inergen
<g/>
.	.	kIx.	.
</s>
<s>
Helena	Helena	k1gFnSc1	Helena
Nese	nést	k5eAaImIp3nS	nést
Arašídy	arašíd	k1gInPc4	arašíd
Králi	Král	k1gMnSc3	Král
Xenonu	xenon	k1gInSc2	xenon
Ráno	ráno	k1gNnSc4	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc1d1	vzácný
plyn	plyn	k1gInSc1	plyn
F.	F.	kA	F.
A.	A.	kA	A.
Cotton	Cotton	k1gInSc1	Cotton
-	-	kIx~	-
G.	G.	kA	G.
Wilkinson	Wilkinson	k1gNnSc1	Wilkinson
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
Jursík	Jursík	k1gInSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
nekovů	nekov	k1gInPc2	nekov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
(	(	kIx(	(
<g/>
elektronická	elektronický	k2eAgFnSc1d1	elektronická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
