<s>
Pomocí	pomocí	k7c2	pomocí
ukazovacích	ukazovací	k2eAgNnPc2d1	ukazovací
zájmen	zájmeno	k1gNnPc2	zájmeno
může	moct	k5eAaImIp3nS	moct
mluvčí	mluvčí	k1gFnSc1	mluvčí
odkazovat	odkazovat	k5eAaImF	odkazovat
jednak	jednak	k8xC	jednak
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
částem	část	k1gFnPc3	část
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
reference	reference	k1gFnSc1	reference
vnitrotextová	vnitrotextová	k1gFnSc1	vnitrotextová
<g/>
,	,	kIx,	,
endoforická	endoforický	k2eAgFnSc1d1	endoforický
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
k	k	k7c3	k
vnějšímu	vnější	k2eAgInSc3d1	vnější
světu	svět	k1gInSc3	svět
(	(	kIx(	(
<g/>
reference	reference	k1gFnSc1	reference
mimotextová	mimotextová	k1gFnSc1	mimotextová
<g/>
,	,	kIx,	,
exoforická	exoforický	k2eAgFnSc1d1	exoforický
<g/>
,	,	kIx,	,
deixe	deixe	k1gFnSc1	deixe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
