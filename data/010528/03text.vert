<p>
<s>
Zájmeno	zájmeno	k1gNnSc1	zájmeno
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
také	také	k9	také
místojmení	místojmenit	k5eAaPmIp3nS	místojmenit
<g/>
,	,	kIx,	,
náměstka	náměstek	k1gMnSc4	náměstek
<g/>
,	,	kIx,	,
pronomen	pronomen	k1gNnSc4	pronomen
<g/>
,	,	kIx,	,
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
pro-nomen	proomit	k5eAaPmNgMnS	pro-nomit
-	-	kIx~	-
"	"	kIx"	"
<g/>
za-jméno	zaméno	k6eAd1	za-jméno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
plnovýznamových	plnovýznamový	k2eAgInPc2d1	plnovýznamový
ohebných	ohebný	k2eAgInPc2d1	ohebný
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c4	o
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
skupinu	skupina	k1gFnSc4	skupina
výrazů	výraz	k1gInPc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
obvykle	obvykle	k6eAd1	obvykle
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
podstatná	podstatný	k2eAgNnPc4d1	podstatné
jména	jméno	k1gNnPc4	jméno
nebo	nebo	k8xC	nebo
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tak	tak	k6eAd1	tak
zpravidla	zpravidla	k6eAd1	zpravidla
funkci	funkce	k1gFnSc4	funkce
podmětu	podmět	k1gInSc2	podmět
<g/>
,	,	kIx,	,
předmětu	předmět	k1gInSc2	předmět
nebo	nebo	k8xC	nebo
přívlastku	přívlastek	k1gInSc2	přívlastek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
zájmen	zájmeno	k1gNnPc2	zájmeno
==	==	k?	==
</s>
</p>
<p>
<s>
Zájmena	zájmeno	k1gNnPc1	zájmeno
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zájmena	zájmeno	k1gNnPc1	zájmeno
osobní	osobní	k2eAgNnPc1d1	osobní
(	(	kIx(	(
<g/>
též	též	k9	též
personalia	personalium	k1gNnSc2	personalium
<g/>
)	)	kIx)	)
-	-	kIx~	-
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
ono	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
vy	vy	k3xPp2nPc1	vy
<g/>
,	,	kIx,	,
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
ony	onen	k3xDgFnPc1	onen
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
a	a	k8xC	a
zvratné	zvratný	k2eAgNnSc1d1	zvratné
zájmeno	zájmeno	k1gNnSc1	zájmeno
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
zájmena	zájmeno	k1gNnPc1	zájmeno
přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
(	(	kIx(	(
<g/>
též	též	k9	též
posesiva	posesivum	k1gNnSc2	posesivum
<g/>
)	)	kIx)	)
-	-	kIx~	-
můj	můj	k1gMnSc1	můj
<g/>
,	,	kIx,	,
tvůj	tvůj	k1gMnSc1	tvůj
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
<g/>
,	,	kIx,	,
náš	náš	k3xOp1gInSc1	náš
<g/>
,	,	kIx,	,
váš	váš	k3xOp2gInSc1	váš
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
a	a	k8xC	a
zvratné	zvratný	k2eAgNnSc1d1	zvratné
zájmeno	zájmeno	k1gNnSc1	zájmeno
svůj	svůj	k3xOyFgInSc4	svůj
</s>
</p>
<p>
<s>
zájmena	zájmeno	k1gNnPc1	zájmeno
ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
(	(	kIx(	(
<g/>
též	též	k9	též
demonstrativa	demonstrativum	k1gNnSc2	demonstrativum
<g/>
)	)	kIx)	)
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
<g/>
,	,	kIx,	,
tenhle	tenhle	k3xDgInSc4	tenhle
<g/>
,	,	kIx,	,
onen	onen	k3xDgInSc4	onen
<g/>
,	,	kIx,	,
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
týž	týž	k3xTgInSc4	týž
<g/>
,	,	kIx,	,
tentýž	týž	k3xTgInSc4	týž
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
</s>
</p>
<p>
<s>
zájmena	zájmeno	k1gNnPc1	zájmeno
tázací	tázací	k2eAgNnPc1d1	tázací
(	(	kIx(	(
<g/>
též	též	k9	též
interogativa	interogativum	k1gNnSc2	interogativum
<g/>
)	)	kIx)	)
-	-	kIx~	-
kdo	kdo	k3yInSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
čí	čí	k3xOyRgInPc4	čí
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
zájmena	zájmeno	k1gNnPc1	zájmeno
vztažná	vztažný	k2eAgNnPc1d1	vztažné
(	(	kIx(	(
<g/>
též	též	k9	též
relativa	relativum	k1gNnSc2	relativum
<g/>
)	)	kIx)	)
-	-	kIx~	-
kdo	kdo	k3yRnSc1	kdo
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
<g/>
,	,	kIx,	,
čí	čí	k3xOyQgInSc1	čí
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
</s>
</p>
<p>
<s>
zájmena	zájmeno	k1gNnPc1	zájmeno
záporná	záporný	k2eAgNnPc1d1	záporné
(	(	kIx(	(
<g/>
též	též	k9	též
negativa	negativum	k1gNnSc2	negativum
<g/>
)	)	kIx)	)
-	-	kIx~	-
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc4	nic
<g/>
,	,	kIx,	,
nijaký	nijaký	k3yNgInSc1	nijaký
<g/>
,	,	kIx,	,
ničí	ničí	k3xOyNgInSc1	ničí
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
</s>
</p>
<p>
<s>
zájmena	zájmeno	k1gNnPc1	zájmeno
neurčitá	určitý	k2eNgNnPc1d1	neurčité
(	(	kIx(	(
<g/>
též	též	k9	též
indefinitiva	indefinitivum	k1gNnSc2	indefinitivum
<g/>
)	)	kIx)	)
-	-	kIx~	-
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
nějaký	nějaký	k3yIgMnSc1	nějaký
<g/>
,	,	kIx,	,
některý	některý	k3yIgMnSc1	některý
<g/>
,	,	kIx,	,
lecco	lecco	k3yInSc1	lecco
<g/>
,	,	kIx,	,
něčí	něčí	k3xOyIgMnSc1	něčí
<g/>
,	,	kIx,	,
něco	něco	k3yInSc1	něco
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
tvořena	tvořit	k5eAaImNgNnP	tvořit
pomocí	pomocí	k7c2	pomocí
kombinace	kombinace	k1gFnSc2	kombinace
předpony	předpona	k1gFnSc2	předpona
(	(	kIx(	(
<g/>
ně-	ně-	k?	ně-
<g/>
,	,	kIx,	,
lec-	lec-	k?	lec-
<g/>
,	,	kIx,	,
leda-	leda-	k?	leda-
<g/>
,	,	kIx,	,
kde-	kde-	k?	kde-
<g/>
,	,	kIx,	,
málo-	málo-	k?	málo-
<g/>
,	,	kIx,	,
málokdy	málokdy	k6eAd1	málokdy
užívané	užívaný	k2eAgInPc1d1	užívaný
zřídka-	zřídka-	k?	zřídka-
<g/>
,	,	kIx,	,
sotva	sotva	k6eAd1	sotva
,	,	kIx,	,
bůhví-	bůhví-	k?	bůhví-
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přípony	přípona	k1gFnSc2	přípona
(	(	kIx(	(
<g/>
-si	i	k?	-si
<g/>
,	,	kIx,	,
-koli	ole	k1gFnSc4	-kole
<g/>
,	,	kIx,	,
-koliv	olit	k5eAaPmDgInS	-kolit
<g/>
)	)	kIx)	)
a	a	k8xC	a
tázacího	tázací	k2eAgNnSc2d1	tázací
zájmena	zájmeno	k1gNnSc2	zájmeno
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
morfologicky	morfologicky	k6eAd1	morfologicky
odlišeno	odlišen	k2eAgNnSc4d1	odlišeno
podmětové	podmětový	k2eAgNnSc4d1	podmětové
a	a	k8xC	a
předmětové	předmětový	k2eAgNnSc4d1	předmětové
užití	užití	k1gNnSc4	užití
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nepoužívají	používat	k5eNaImIp3nP	používat
skloňování	skloňování	k1gNnSc4	skloňování
-	-	kIx~	-
např.	např.	kA	např.
anglické	anglický	k2eAgFnPc4d1	anglická
I	i	k8xC	i
(	(	kIx(	(
<g/>
já	já	k3xPp1nSc1	já
<g/>
)	)	kIx)	)
-	-	kIx~	-
me	me	k?	me
(	(	kIx(	(
<g/>
mě	já	k3xPp1nSc4	já
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
předmětového	předmětový	k2eAgNnSc2d1	předmětové
užití	užití	k1gNnSc2	užití
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
často	často	k6eAd1	často
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
důrazová	důrazový	k2eAgFnSc1d1	důrazový
a	a	k8xC	a
nedůrazová	důrazový	k2eNgFnSc1d1	důrazový
forma	forma	k1gFnSc1	forma
-	-	kIx~	-
srv	srv	k?	srv
<g/>
.	.	kIx.	.
angl.	angl.	k?	angl.
me	me	k?	me
-	-	kIx~	-
myself	myself	k1gMnSc1	myself
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
české	český	k2eAgNnSc1d1	české
mu	on	k3xPp3gNnSc3	on
-	-	kIx~	-
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
lze	lze	k6eAd1	lze
někdy	někdy	k6eAd1	někdy
také	také	k9	také
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
úctu	úcta	k1gFnSc4	úcta
(	(	kIx(	(
<g/>
honorifika	honorifika	k1gFnSc1	honorifika
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
především	především	k9	především
protiklad	protiklad	k1gInSc1	protiklad
vykání	vykání	k1gNnSc2	vykání
a	a	k8xC	a
tykání	tykání	k1gNnSc2	tykání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukazovací	ukazovací	k2eAgNnPc1d1	ukazovací
zájmena	zájmeno	k1gNnPc1	zájmeno
==	==	k?	==
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
ukazovacích	ukazovací	k2eAgNnPc2d1	ukazovací
zájmen	zájmeno	k1gNnPc2	zájmeno
může	moct	k5eAaImIp3nS	moct
mluvčí	mluvčí	k1gFnSc1	mluvčí
odkazovat	odkazovat	k5eAaImF	odkazovat
jednak	jednak	k8xC	jednak
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
částem	část	k1gFnPc3	část
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
reference	reference	k1gFnSc1	reference
vnitrotextová	vnitrotextová	k1gFnSc1	vnitrotextová
<g/>
,	,	kIx,	,
endoforická	endoforický	k2eAgFnSc1d1	endoforický
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
k	k	k7c3	k
vnějšímu	vnější	k2eAgInSc3d1	vnější
světu	svět	k1gInSc3	svět
(	(	kIx(	(
<g/>
reference	reference	k1gFnSc1	reference
mimotextová	mimotextová	k1gFnSc1	mimotextová
<g/>
,	,	kIx,	,
exoforická	exoforický	k2eAgFnSc1d1	exoforický
<g/>
,	,	kIx,	,
deixe	deixe	k1gFnSc1	deixe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
deixe	deixe	k1gFnSc2	deixe
(	(	kIx(	(
<g/>
ukazování	ukazování	k1gNnSc2	ukazování
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
dále	daleko	k6eAd2	daleko
rozlišena	rozlišen	k2eAgFnSc1d1	rozlišena
deixe	deixe	k1gFnSc1	deixe
proximální	proximální	k2eAgFnSc1d1	proximální
a	a	k8xC	a
distanční	distanční	k2eAgFnSc1d1	distanční
(	(	kIx(	(
<g/>
tento	tento	k3xDgMnSc1	tento
-	-	kIx~	-
tamten	tamten	k3xDgMnSc1	tamten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
odlišeno	odlišen	k2eAgNnSc1d1	odlišeno
více	hodně	k6eAd2	hodně
stupňů	stupeň	k1gInPc2	stupeň
distanční	distanční	k2eAgFnSc2d1	distanční
deixe	deixe	k1gFnSc2	deixe
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
;	;	kIx,	;
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
blíží	blížit	k5eAaImIp3nS	blížit
protiklad	protiklad	k1gInSc1	protiklad
tamten	tamten	k3xDgMnSc1	tamten
-	-	kIx~	-
tamhleten	tamhleten	k3xDgMnSc1	tamhleten
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
zájmena	zájmeno	k1gNnPc1	zájmeno
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
skloňování	skloňování	k1gNnSc1	skloňování
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zájmeno	zájmeno	k1gNnSc4	zájmeno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zájmeno	zájmeno	k1gNnSc4	zájmeno
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Zájmena	zájmeno	k1gNnSc2	zájmeno
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zájmena	zájmeno	k1gNnSc2	zájmeno
–	–	k?	–
Přehled	přehled	k1gInSc4	přehled
českých	český	k2eAgNnPc2d1	české
zájmen	zájmeno	k1gNnPc2	zájmeno
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
skloňování	skloňování	k1gNnPc4	skloňování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
И	И	k?	И
<g/>
,	,	kIx,	,
И	И	k?	И
Т	Т	k?	Т
н	н	k?	н
о	о	k?	о
<g/>
.	.	kIx.	.
Б	Б	k?	Б
о	о	k?	о
м	м	k?	м
-	-	kIx~	-
п	п	k?	п
и	и	k?	и
р	р	k?	р
<g/>
.	.	kIx.	.
П	П	k?	П
<g/>
.	.	kIx.	.
В	В	k?	В
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
-	-	kIx~	-
nevím	vědět	k5eNaImIp1nS	vědět
co	co	k9	co
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Т	Т	k?	Т
н	н	k?	н
о	о	k?	о
...	...	k?	...
</s>
</p>
