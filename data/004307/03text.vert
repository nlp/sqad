<s>
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1896	[number]	k4	1896
Svatý	svatý	k1gMnSc1	svatý
Kopeček	Kopeček	k1gMnSc1	Kopeček
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1986	[number]	k4	1986
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
typograf	typograf	k1gMnSc1	typograf
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
scénograf	scénograf	k1gMnSc1	scénograf
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vyučil	vyučit	k5eAaPmAgMnS	vyučit
řezbářem	řezbář	k1gMnSc7	řezbář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
na	na	k7c6	na
UPŠ	UPŠ	kA	UPŠ
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Štěpána	Štěpán	k1gMnSc2	Štěpán
Zálešáka	Zálešák	k1gMnSc2	Zálešák
studoval	studovat	k5eAaImAgMnS	studovat
malbu	malba	k1gFnSc4	malba
a	a	k8xC	a
grafiku	grafika	k1gFnSc4	grafika
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
Bohumila	Bohumil	k1gMnSc2	Bohumil
Kafky	Kafka	k1gMnSc2	Kafka
sochu	socha	k1gFnSc4	socha
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úrazu	úraz	k1gInSc6	úraz
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
především	především	k9	především
na	na	k7c4	na
grafiku	grafika	k1gFnSc4	grafika
a	a	k8xC	a
nástěnnou	nástěnný	k2eAgFnSc4d1	nástěnná
malbu	malba	k1gFnSc4	malba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Františka	František	k1gMnSc2	František
Kysely	Kysela	k1gMnSc2	Kysela
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
škole	škola	k1gFnSc6	škola
jako	jako	k8xS	jako
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
pedagog	pedagog	k1gMnSc1	pedagog
Speciálního	speciální	k2eAgInSc2d1	speciální
ateliéru	ateliér	k1gInSc2	ateliér
užité	užitý	k2eAgFnSc2d1	užitá
grafiky	grafika	k1gFnSc2	grafika
<g/>
.	.	kIx.	.
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
:	:	kIx,	:
vyučil	vyučit	k5eAaPmAgMnS	vyučit
se	se	k3xPyFc4	se
řezbářství	řezbářství	k1gNnSc4	řezbářství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
:	:	kIx,	:
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
škole	škola	k1gFnSc6	škola
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Štěpána	Štěpán	k1gMnSc2	Štěpán
Zalešáka	Zalešák	k1gMnSc2	Zalešák
1921	[number]	k4	1921
<g/>
:	:	kIx,	:
studium	studium	k1gNnSc4	studium
sochařství	sochařství	k1gNnSc2	sochařství
na	na	k7c4	na
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc4d1	Uměleckoprůmyslové
<g />
.	.	kIx.	.
</s>
<s>
škole	škola	k1gFnSc3	škola
u	u	k7c2	u
profesora	profesor	k1gMnSc4	profesor
Bohumila	Bohumil	k1gMnSc2	Bohumil
Kafky	Kafka	k1gMnSc2	Kafka
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
:	:	kIx,	:
studium	studium	k1gNnSc4	studium
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
na	na	k7c6	na
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
škole	škola	k1gFnSc6	škola
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Františka	František	k1gMnSc2	František
Kysely	Kysela	k1gMnSc2	Kysela
1924	[number]	k4	1924
<g/>
:	:	kIx,	:
Národní	národní	k2eAgFnSc1d1	národní
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
:	:	kIx,	:
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
pedagog	pedagog	k1gMnSc1	pedagog
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1952	[number]	k4	1952
<g/>
:	:	kIx,	:
Laureát	laureát	k1gMnSc1	laureát
státní	státní	k2eAgFnSc2d1	státní
ceny	cena	k1gFnSc2	cena
1956	[number]	k4	1956
<g/>
:	:	kIx,	:
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
1961	[number]	k4	1961
<g/>
:	:	kIx,	:
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
1976	[number]	k4	1976
<g/>
:	:	kIx,	:
Řád	řád	k1gInSc1	řád
práce	práce	k1gFnSc2	práce
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
tzv.	tzv.	kA	tzv.
Antichartu	anticharta	k1gFnSc4	anticharta
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
velká	velký	k2eAgFnSc1d1	velká
retrospektivní	retrospektivní	k2eAgFnSc1d1	retrospektivní
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Těžištěm	těžiště	k1gNnSc7	těžiště
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
kresba	kresba	k1gFnSc1	kresba
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
lidovými	lidový	k2eAgFnPc7d1	lidová
tradicemi	tradice	k1gFnPc7	tradice
<g/>
,	,	kIx,	,
folklórem	folklór	k1gInSc7	folklór
a	a	k8xC	a
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kresby	kresba	k1gFnSc2	kresba
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
především	především	k9	především
volné	volný	k2eAgFnPc4d1	volná
<g/>
,	,	kIx,	,
drobné	drobná	k1gFnPc4	drobná
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
libris	libris	k1gFnSc1	libris
<g/>
)	)	kIx)	)
a	a	k8xC	a
užité	užitý	k2eAgFnSc3d1	užitá
grafice	grafika	k1gFnSc3	grafika
(	(	kIx(	(
<g/>
plakáty	plakát	k1gInPc4	plakát
<g/>
,	,	kIx,	,
bankovky	bankovka	k1gFnPc4	bankovka
<g/>
,	,	kIx,	,
známková	známkový	k2eAgFnSc1d1	známková
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
tvorbě	tvorba	k1gFnSc6	tvorba
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
s	s	k7c7	s
grafickými	grafický	k2eAgFnPc7d1	grafická
technikami	technika	k1gFnPc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
grafickou	grafický	k2eAgFnSc7d1	grafická
technikou	technika	k1gFnSc7	technika
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
dřevoryt	dřevoryt	k1gInSc4	dřevoryt
a	a	k8xC	a
dřevořez	dřevořez	k1gInSc1	dřevořez
(	(	kIx(	(
<g/>
související	související	k2eAgInSc1d1	související
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
vyučením	vyučení	k1gNnSc7	vyučení
řezbářem	řezbář	k1gMnSc7	řezbář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vynikal	vynikat	k5eAaImAgMnS	vynikat
také	také	k9	také
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
grafice	grafika	k1gFnSc6	grafika
a	a	k8xC	a
ilustraci	ilustrace	k1gFnSc6	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
gobelíny	gobelín	k1gInPc4	gobelín
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
scénografem	scénograf	k1gMnSc7	scénograf
a	a	k8xC	a
především	především	k6eAd1	především
autorem	autor	k1gMnSc7	autor
návrhů	návrh	k1gInPc2	návrh
několika	několik	k4yIc2	několik
monumentálních	monumentální	k2eAgFnPc2d1	monumentální
realizací	realizace	k1gFnPc2	realizace
<g/>
:	:	kIx,	:
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
<g/>
:	:	kIx,	:
návrh	návrh	k1gInSc4	návrh
vitráže	vitráž	k1gFnSc2	vitráž
pro	pro	k7c4	pro
rodovou	rodový	k2eAgFnSc4d1	rodová
kapli	kaple	k1gFnSc4	kaple
Schwarzenberků	Schwarzenberka	k1gMnPc2	Schwarzenberka
v	v	k7c6	v
Chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
:	:	kIx,	:
návrh	návrh	k1gInSc1	návrh
nové	nový	k2eAgFnSc2d1	nová
podoby	podoba	k1gFnSc2	podoba
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
orloje	orloj	k1gInSc2	orloj
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
socialistického	socialistický	k2eAgInSc2d1	socialistický
<g />
.	.	kIx.	.
</s>
<s>
realismu	realismus	k1gInSc2	realismus
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
sochařkou	sochařka	k1gFnSc7	sochařka
Marií	Maria	k1gFnSc7	Maria
Svolinskou	Svolinský	k2eAgFnSc7d1	Svolinský
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
;	;	kIx,	;
na	na	k7c6	na
realizaci	realizace	k1gFnSc6	realizace
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
také	také	k9	také
sochař	sochař	k1gMnSc1	sochař
Olbram	Olbram	k1gInSc4	Olbram
Zoubek	zoubek	k1gInSc1	zoubek
<g/>
)	)	kIx)	)
Scénografické	scénografický	k2eAgFnSc6d1	scénografická
výpravě	výprava	k1gFnSc6	výprava
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dirigent	dirigent	k1gMnSc1	dirigent
Václav	Václav	k1gMnSc1	Václav
Talich	Talich	k1gMnSc1	Talich
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
Dvořákova	Dvořákův	k2eAgMnSc2d1	Dvořákův
Jakobína	jakobín	k1gMnSc2	jakobín
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pohostinsky	pohostinsky	k6eAd1	pohostinsky
realizoval	realizovat	k5eAaBmAgMnS	realizovat
výpravy	výprava	k1gFnSc2	výprava
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
operní	operní	k2eAgInSc4d1	operní
repertoár	repertoár	k1gInSc4	repertoár
jak	jak	k8xS	jak
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
Státním	státní	k2eAgNnSc6d1	státní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Pohostinsky	pohostinsky	k6eAd1	pohostinsky
připravil	připravit	k5eAaPmAgInS	připravit
výpravu	výprava	k1gFnSc4	výprava
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
Její	její	k3xOp3gInPc1	její
pastorkyně	pastorkyně	k1gFnSc1	pastorkyně
i	i	k9	i
pro	pro	k7c4	pro
Vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
státní	státní	k2eAgFnSc4d1	státní
operu	opera	k1gFnSc4	opera
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
čtyřsvazková	čtyřsvazkový	k2eAgFnSc1d1	čtyřsvazková
publikace	publikace	k1gFnSc2	publikace
Český	český	k2eAgInSc4d1	český
rok	rok	k1gInSc4	rok
Karla	Karel	k1gMnSc2	Karel
Plicky	Plicka	k1gFnSc2	Plicka
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Volfa	Volf	k1gMnSc2	Volf
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
obálek	obálka	k1gFnPc2	obálka
hudebnin	hudebnina	k1gFnPc2	hudebnina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
B.	B.	kA	B.
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
Fibicha	Fibich	k1gMnSc2	Fibich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
jeho	jeho	k3xOp3gInSc1	jeho
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
tisk	tisk	k1gInSc1	tisk
Máchova	Máchův	k2eAgInSc2d1	Máchův
Máje	máj	k1gInSc2	máj
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
původní	původní	k2eAgNnSc4d1	původní
typografické	typografický	k2eAgNnSc4d1	typografické
písmo	písmo	k1gNnSc4	písmo
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
ilustrací	ilustrace	k1gFnPc2	ilustrace
<g/>
)	)	kIx)	)
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
výstavě	výstava	k1gFnSc6	výstava
dekorativního	dekorativní	k2eAgNnSc2d1	dekorativní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
vydal	vydat	k5eAaPmAgInS	vydat
u	u	k7c2	u
anglické	anglický	k2eAgFnSc2d1	anglická
firmy	firma	k1gFnSc2	firma
Monotype	monotyp	k1gInSc5	monotyp
svůj	svůj	k3xOyFgInSc4	svůj
skript	skript	k1gInSc4	skript
Wenceslas	Wenceslas	k1gInSc1	Wenceslas
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
též	též	k6eAd1	též
tvorbou	tvorba	k1gFnSc7	tvorba
plakátů	plakát	k1gInPc2	plakát
<g/>
,	,	kIx,	,
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
<g/>
,	,	kIx,	,
exlibris	exlibris	k1gNnPc2	exlibris
<g/>
,	,	kIx,	,
bankovek	bankovka	k1gFnPc2	bankovka
aj.	aj.	kA	aj.
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
barevných	barevný	k2eAgFnPc2d1	barevná
vitráží	vitráž	k1gFnPc2	vitráž
<g/>
,	,	kIx,	,
mozaik	mozaika	k1gFnPc2	mozaika
a	a	k8xC	a
maleb	malba	k1gFnPc2	malba
na	na	k7c4	na
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
SVU	SVU	kA	SVU
Mánes	Mánes	k1gMnSc1	Mánes
a	a	k8xC	a
čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gMnSc7	člen
SČUG	SČUG	kA	SČUG
Hollar	Hollar	k1gInSc1	Hollar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc7	jeho
90	[number]	k4	90
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
vydal	vydat	k5eAaPmAgMnS	vydat
Albatros	albatros	k1gMnSc1	albatros
knihu	kniha	k1gFnSc4	kniha
Kolik	kolika	k1gFnPc2	kolika
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
krás	krása	k1gFnPc2	krása
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
František	František	k1gMnSc1	František
Nepil	pít	k5eNaImAgMnS	pít
a	a	k8xC	a
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
právě	právě	k9	právě
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
pokračovatele	pokračovatel	k1gMnSc4	pokračovatel
mánesovsko	mánesovsko	k6eAd1	mánesovsko
–	–	k?	–
alšovské	alšovský	k2eAgFnPc4d1	alšovský
tradice	tradice	k1gFnPc4	tradice
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
