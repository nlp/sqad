<s>
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1896	[number]	k4	1896
Svatý	svatý	k1gMnSc1	svatý
Kopeček	Kopeček	k1gMnSc1	Kopeček
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1986	[number]	k4	1986
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
typograf	typograf	k1gMnSc1	typograf
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
scénograf	scénograf	k1gMnSc1	scénograf
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
