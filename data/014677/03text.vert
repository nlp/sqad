<s>
Petr	Petr	k1gMnSc1
Stančík	Stančík	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Stančík	Stančík	k1gMnSc1
Petr	Petr	k1gMnSc1
Stančík	Stančík	k1gMnSc1
na	na	k7c6
fotografii	fotografia	k1gFnSc6
Davida	David	k1gMnSc2
Konečného	Konečný	k1gMnSc2
<g/>
,	,	kIx,
2017	#num#	k4
Narození	narození	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1968	#num#	k4
(	(	kIx(
<g/>
52	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Rychnov	Rychnov	k1gInSc1
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
Pseudonym	pseudonym	k1gInSc4
</s>
<s>
Odillo	Odillo	k1gNnSc1
Stradický	Stradický	k2eAgMnSc1d1
ze	z	k7c2
Strdic	Strdice	k1gInPc2
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
a	a	k8xC
dramatik	dramatik	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Magnesia	magnesium	k1gNnSc2
Litera	litera	k1gFnSc1
2015	#num#	k4
</s>
<s>
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Petr	Petr	k1gMnSc1
Stančík	Stančík	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1968	#num#	k4
v	v	k7c6
Rychnově	Rychnov	k1gInSc6
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
;	;	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
prozaik	prozaik	k1gMnSc1
<g/>
,	,	kIx,
esejista	esejista	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
a	a	k8xC
textař	textař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Oba	dva	k4xCgMnPc1
autorovi	autorův	k2eAgMnPc1d1
rodiče	rodič	k1gMnPc1
jsou	být	k5eAaImIp3nP
učitelé	učitel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
cvičitelkou	cvičitelka	k1gFnSc7
Sokola	Sokol	k1gMnSc2
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
výtvarník	výtvarník	k1gMnSc1
a	a	k8xC
řezbář	řezbář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Stančík	Stančík	k1gMnSc1
maturoval	maturovat	k5eAaBmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
vystřídal	vystřídat	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
manuálních	manuální	k2eAgNnPc2d1
zaměstnání	zaměstnání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1989	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
studoval	studovat	k5eAaImAgMnS
režii	režie	k1gFnSc4
na	na	k7c4
DAMU	DAMU	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
ze	z	k7c2
školy	škola	k1gFnSc2
však	však	k9
odešel	odejít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1992	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
televizní	televizní	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
se	se	k3xPyFc4
živil	živit	k5eAaImAgMnS
jako	jako	k9
tvůrce	tvůrce	k1gMnSc1
a	a	k8xC
poradce	poradce	k1gMnSc1
v	v	k7c6
oboru	obor	k1gInSc6
komerční	komerční	k2eAgFnSc2d1
i	i	k8xC
nekomerční	komerční	k2eNgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
je	být	k5eAaImIp3nS
spisovatelem	spisovatel	k1gMnSc7
na	na	k7c6
volné	volný	k2eAgFnSc6d1
noze	noha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
za	za	k7c4
přesvědčeného	přesvědčený	k2eAgMnSc4d1
monarchistu	monarchista	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
První	první	k4xOgMnSc1
veřejně	veřejně	k6eAd1
otištěný	otištěný	k2eAgInSc1d1
text	text	k1gInSc1
autora	autor	k1gMnSc2
byla	být	k5eAaImAgFnS
esej	esej	k1gFnSc1
Smrt	smrt	k1gFnSc1
živá	živá	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyšla	vyjít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1990	#num#	k4
v	v	k7c6
časopisu	časopis	k1gInSc6
Iniciály	iniciála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2006	#num#	k4
většinou	většinou	k6eAd1
publikoval	publikovat	k5eAaBmAgMnS
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
Odillo	Odillo	k1gNnSc1
Stradický	Stradický	k2eAgMnSc1d1
ze	z	k7c2
Strdic	Strdice	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
pod	pod	k7c7
svým	svůj	k3xOyFgNnSc7
občanským	občanský	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1
texty	text	k1gInPc1
byly	být	k5eAaImAgInP
přeloženy	přeložit	k5eAaPmNgInP
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
<g/>
,	,	kIx,
francouzštiny	francouzština	k1gFnSc2
<g/>
,	,	kIx,
španělštiny	španělština	k1gFnSc2
<g/>
,	,	kIx,
němčiny	němčina	k1gFnSc2
<g/>
,	,	kIx,
polštiny	polština	k1gFnSc2
<g/>
,	,	kIx,
bulharštiny	bulharština	k1gFnSc2
<g/>
,	,	kIx,
maďarštiny	maďarština	k1gFnSc2
<g/>
,	,	kIx,
litevštiny	litevština	k1gFnSc2
<g/>
,	,	kIx,
makedonštiny	makedonština	k1gFnSc2
i	i	k8xC
albánštiny	albánština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
byl	být	k5eAaImAgInS
hostem	host	k1gMnSc7
16	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
Měsíce	měsíc	k1gInSc2
autorského	autorský	k2eAgNnSc2d1
čtení	čtení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
literatury	literatura	k1gFnSc2
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
také	také	k9
historii	historie	k1gFnSc4
pozdního	pozdní	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
zbořené	zbořený	k2eAgFnSc6d1
gotické	gotický	k2eAgFnSc6d1
kapli	kaple	k1gFnSc6
Božího	boží	k2eAgNnSc2d1
Těla	tělo	k1gNnSc2
a	a	k8xC
Krve	krev	k1gFnSc2
na	na	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
pražském	pražský	k2eAgNnSc6d1
a	a	k8xC
Bratrstvu	bratrstvo	k1gNnSc6
obruče	obruč	k1gFnSc2
s	s	k7c7
kladivem	kladivo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Bibliografie	bibliografie	k1gFnSc1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kaple	kaple	k1gFnSc1
Božího	boží	k2eAgNnSc2d1
Těla	tělo	k1gNnSc2
a	a	k8xC
Krve	krev	k1gFnSc2
a	a	k8xC
Bratrstvo	bratrstvo	k1gNnSc1
obruče	obruč	k1gFnSc2
s	s	k7c7
kladivem	kladivo	k1gNnSc7
(	(	kIx(
<g/>
Krasoumná	krasoumný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
2015	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-904510-8-7	978-80-904510-8-7	k4
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3
zobrazení	zobrazení	k1gNnSc1
novoměstské	novoměstský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
Božího	boží	k2eAgNnSc2d1
Těla	tělo	k1gNnSc2
a	a	k8xC
Krve	krev	k1gFnSc2
na	na	k7c6
picím	picí	k2eAgInSc6d1
rohu	roh	k1gInSc6
Václava	Václav	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Bratrstvo	bratrstvo	k1gNnSc1
obruče	obruč	k1gFnSc2
s	s	k7c7
kladivem	kladivo	k1gNnSc7
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-907110-0-6	978-80-907110-0-6	k4
</s>
<s>
Praha	Praha	k1gFnSc1
ožralá	ožralý	k2eAgFnSc1d1
(	(	kIx(
<g/>
s	s	k7c7
Radimem	Radim	k1gMnSc7
Kopáčem	kopáč	k1gMnSc7
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
2021	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-200-3179-2	978-80-200-3179-2	k4
</s>
<s>
Próza	próza	k1gFnSc1
</s>
<s>
Obojí	obojí	k4xRgInSc1
pramen	pramen	k1gInSc1
(	(	kIx(
<g/>
NPDN	NPDN	kA
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Admirál	admirál	k1gMnSc1
čaje	čaj	k1gInSc2
(	(	kIx(
<g/>
Český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
1996	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
8020206094	#num#	k4
</s>
<s>
Zlomená	zlomený	k2eAgFnSc1d1
nadkova	nadkův	k2eAgFnSc1d1
(	(	kIx(
<g/>
Petrov	Petrov	k1gInSc1
1997	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
80-7227-007-9	80-7227-007-9	k4
</s>
<s>
Fosfen	Fosfen	k1gInSc1
(	(	kIx(
<g/>
Petrov	Petrov	k1gInSc1
2001	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
80-7227-098-2	80-7227-098-2	k4
</s>
<s>
Pérák	pérák	k1gInSc1
(	(	kIx(
<g/>
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
2008	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7227-267-9	978-80-7227-267-9	k4
</s>
<s>
Mlýn	mlýn	k1gInSc1
na	na	k7c4
mumie	mumie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převratné	převratný	k2eAgFnPc4d1
odhalení	odhalení	k1gNnPc4
komisaře	komisař	k1gMnSc2
Durmana	Durman	k1gMnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
2014	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7227	#num#	k4
<g/>
-	-	kIx~
<g/>
358	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocenění	ocenění	k1gNnSc1
<g/>
:	:	kIx,
Magnesia	magnesium	k1gNnSc2
Litera	litera	k1gFnSc1
2015	#num#	k4
za	za	k7c4
prózu	próza	k1gFnSc4
</s>
<s>
Andělí	andělí	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
(	(	kIx(
<g/>
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
2016	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7227	#num#	k4
<g/>
-	-	kIx~
<g/>
385	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocenění	ocenění	k1gNnSc1
<g/>
:	:	kIx,
Kniha	kniha	k1gFnSc1
roku	rok	k1gInSc2
2016	#num#	k4
v	v	k7c6
anketě	anketa	k1gFnSc6
Lidových	lidový	k2eAgFnPc2d1
novin	novina	k1gFnPc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
100	#num#	k4
miliard	miliarda	k4xCgFnPc2
neuronů	neuron	k1gInPc2
(	(	kIx(
<g/>
Škoda	škoda	k1gFnSc1
Auto	auto	k1gNnSc1
2017	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
270	#num#	k4
<g/>
-	-	kIx~
<g/>
3120	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
https://backendstories.skoda-kariera.cz	https://backendstories.skoda-kariera.cz	k1gInSc1
</s>
<s>
Nulorožec	Nulorožec	k1gMnSc1
(	(	kIx(
<g/>
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7227-411-6	978-80-7227-411-6	k4
</s>
<s>
Pravomil	Pravomil	k1gMnSc1
aneb	aneb	k?
Ohlušující	ohlušující	k2eAgNnSc1d1
promlčení	promlčení	k1gNnSc1
(	(	kIx(
<g/>
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Audioknihy	Audioknih	k1gInPc1
</s>
<s>
Mlýn	mlýn	k1gInSc1
na	na	k7c4
mumie	mumie	k1gFnPc4
(	(	kIx(
<g/>
Tympanum	Tympanum	k1gInSc1
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
stejnojmenného	stejnojmenný	k2eAgInSc2d1
románu	román	k1gInSc2
<g/>
;	;	kIx,
vypráví	vyprávět	k5eAaImIp3nS
Ivan	Ivan	k1gMnSc1
Řezáč	Řezáč	k1gMnSc1
a	a	k8xC
Zdeněk	Zdeněk	k1gMnSc1
Maryška	Maryška	k1gMnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gInSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgInSc1d1
doprovod	doprovod	k1gInSc1
gregoriánského	gregoriánský	k2eAgInSc2d1
chorálu	chorál	k1gInSc2
mnichů	mnich	k1gMnPc2
z	z	k7c2
belgického	belgický	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocenění	ocenění	k1gNnSc1
<g/>
:	:	kIx,
absolutní	absolutní	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Audiokniha	Audiokniha	k1gFnSc1
roku	rok	k1gInSc2
2015	#num#	k4
a	a	k8xC
zároveň	zároveň	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
četbu	četba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nekonečná	konečný	k2eNgFnSc1d1
pohádka	pohádka	k1gFnSc1
o	o	k7c6
nenažraném	nažraný	k2eNgMnSc6d1
bagrovi	bagr	k1gMnSc6
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Nadýchané	nadýchaný	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
pro	pro	k7c4
slané	slaný	k2eAgFnPc4d1
sladké	sladký	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
(	(	kIx(
<g/>
Klub	klub	k1gInSc1
nemocných	nemocný	k1gMnPc2
cystickou	cystický	k2eAgFnSc7d1
fibrózou	fibróza	k1gFnSc7
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nulorožec	Nulorožec	k1gMnSc1
(	(	kIx(
<g/>
Audioberg	Audioberg	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
stejnojmenného	stejnojmenný	k2eAgInSc2d1
románu	román	k1gInSc2
<g/>
;	;	kIx,
vyprávějí	vyprávět	k5eAaImIp3nP
Pavel	Pavel	k1gMnSc1
Rímský	Rímský	k1gMnSc1
a	a	k8xC
Arnošt	Arnošt	k1gMnSc1
Goldflam	Goldflam	k1gInSc4
</s>
<s>
Andělí	andělí	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
(	(	kIx(
<g/>
Audioberg	Audioberg	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
stejnojmenného	stejnojmenný	k2eAgInSc2d1
románu	román	k1gInSc2
<g/>
;	;	kIx,
vypráví	vyprávět	k5eAaImIp3nS
Lukáš	Lukáš	k1gMnSc1
Hlavica	Hlavica	k1gMnSc1
</s>
<s>
Fíla	Fíla	k1gFnSc1
<g/>
,	,	kIx,
Žofie	Žofie	k1gFnSc1
a	a	k8xC
Smaragdová	smaragdový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
(	(	kIx(
<g/>
Audioberg	Audioberg	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
;	;	kIx,
vypráví	vyprávět	k5eAaImIp3nS
Martin	Martin	k1gMnSc1
Myšička	myšička	k1gFnSc1
</s>
<s>
H2O	H2O	k4
a	a	k8xC
tajná	tajný	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
mise	mise	k1gFnSc1
(	(	kIx(
<g/>
OneHotBook	OneHotBook	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
;	;	kIx,
vypráví	vyprávět	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
</s>
<s>
H2O	H2O	k4
a	a	k8xC
poklad	poklad	k1gInSc1
šíleného	šílený	k2eAgNnSc2d1
oka	oko	k1gNnSc2
(	(	kIx(
<g/>
OneHotBook	OneHotBook	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
;	;	kIx,
vypráví	vyprávět	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
</s>
<s>
Rozhlasové	rozhlasový	k2eAgFnPc1d1
inscenace	inscenace	k1gFnPc1
</s>
<s>
Matná	matný	k2eAgFnSc1d1
Kovářka	kovářka	k1gFnSc1
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
povídky	povídka	k1gFnSc2
z	z	k7c2
knihy	kniha	k1gFnSc2
Admirál	admirál	k1gMnSc1
čaje	čaj	k1gInSc2
<g/>
;	;	kIx,
přednáší	přednášet	k5eAaImIp3nS
Radovan	Radovan	k1gMnSc1
Lukavský	Lukavský	k2eAgMnSc1d1
<g/>
,	,	kIx,
výběr	výběr	k1gInSc1
hudby	hudba	k1gFnSc2
a	a	k8xC
režie	režie	k1gFnSc2
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gMnSc1
</s>
<s>
Srdcadlo	Srdcadlo	k1gNnSc1
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výběr	výběr	k1gInSc4
básní	báseň	k1gFnPc2
ze	z	k7c2
sbírky	sbírka	k1gFnSc2
První	první	k4xOgFnSc1
kost	kost	k1gFnSc1
božího	boží	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
;	;	kIx,
recituje	recitovat	k5eAaImIp3nS
Ivan	Ivan	k1gMnSc1
Trojan	Trojan	k1gMnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gMnSc1
</s>
<s>
Obojí	obojí	k4xRgInSc1
pramen	pramen	k1gInSc1
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dramatizace	dramatizace	k1gFnSc1
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
;	;	kIx,
účinkují	účinkovat	k5eAaImIp3nP
Petr	Petr	k1gMnSc1
Pelzer	Pelzer	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
Hlavica	Hlavica	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Lábus	Lábus	k1gMnSc1
<g/>
,	,	kIx,
Otakar	Otakar	k1gMnSc1
Brousek	brousek	k1gInSc1
st.	st.	kA
a	a	k8xC
Věra	Věra	k1gFnSc1
Slunéčková	Slunéčková	k1gFnSc1
<g/>
,	,	kIx,
zvukový	zvukový	k2eAgInSc1d1
design	design	k1gInSc1
Michal	Michal	k1gMnSc1
Rataj	Rataj	k1gMnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gMnSc1
</s>
<s>
Pérák	pérák	k1gInSc1
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
12	#num#	k4
dílů	díl	k1gInPc2
čtení	čtení	k1gNnSc2
na	na	k7c4
pokračování	pokračování	k1gNnSc4
podle	podle	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
účinkují	účinkovat	k5eAaImIp3nP
Dana	Dana	k1gFnSc1
Černá	Černá	k1gFnSc1
<g/>
,	,	kIx,
Přemysl	Přemysl	k1gMnSc1
Rut	rout	k5eAaImNgMnS
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
Procházka	Procházka	k1gMnSc1
a	a	k8xC
Petr	Petr	k1gMnSc1
Stančík	Stančík	k1gMnSc1
<g/>
,	,	kIx,
hudba	hudba	k1gFnSc1
Přemysl	Přemysl	k1gMnSc1
Rut	rout	k5eAaImNgMnS
<g/>
,	,	kIx,
dramaturgie	dramaturgie	k1gFnPc1
Pavel	Pavel	k1gMnSc1
Kácha	Kácha	k1gMnSc1
<g/>
,	,	kIx,
rozhlasová	rozhlasový	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
a	a	k8xC
režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gMnSc1
</s>
<s>
Virgonaut	Virgonaut	k1gInSc1
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výběr	výběr	k1gInSc4
básní	báseň	k1gFnPc2
ze	z	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
sbírky	sbírka	k1gFnSc2
<g/>
,	,	kIx,
recitují	recitovat	k5eAaImIp3nP
Milan	Milan	k1gMnSc1
Bahúl	Bahúl	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
zvukový	zvukový	k2eAgInSc1d1
design	design	k1gInSc1
Michal	Michal	k1gMnSc1
Rataj	Rataj	k1gMnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gMnSc1
</s>
<s>
Ctiborova	Ctiborův	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
v	v	k7c6
podzemní	podzemní	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
knihy	kniha	k1gFnSc2
Mrkev	mrkev	k1gFnSc1
ho	on	k3xPp3gNnSc4
vcucla	vcucnout	k5eAaPmAgFnS
pod	pod	k7c4
zem	zem	k1gFnSc4
<g/>
,	,	kIx,
účinkuje	účinkovat	k5eAaImIp3nS
Vladimír	Vladimír	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
úprava	úprava	k1gFnSc1
a	a	k8xC
režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gMnSc1
</s>
<s>
Až	až	k6eAd1
se	se	k3xPyFc4
ostří	ostří	k1gNnSc1
lepí	lepit	k5eAaImIp3nS
na	na	k7c4
prst	prst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masový	masový	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
a	a	k8xC
kanibal	kanibal	k1gMnSc1
z	z	k7c2
Motola	Motola	k1gFnSc1
<g/>
.	.	kIx.
5	#num#	k4
dílů	díl	k1gInPc2
z	z	k7c2
cyklu	cyklus	k1gInSc2
Kriminální	kriminální	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
účinkují	účinkovat	k5eAaImIp3nP
Jiří	Jiří	k1gMnSc1
Černý	Černý	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Černý	Černý	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Rímský	Rímský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dramaturgie	dramaturgie	k1gFnSc1
Klára	Klára	k1gFnSc1
Fleyberková	Fleyberková	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Výročí	výročí	k1gNnSc1
Wagnera	Wagner	k1gMnSc2
na	na	k7c4
ostří	ostří	k1gNnSc4
nože	nůž	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
ze	z	k7c2
Sacramenta	Sacramento	k1gNnSc2
<g/>
.	.	kIx.
5	#num#	k4
dílů	díl	k1gInPc2
z	z	k7c2
cyklu	cyklus	k1gInSc2
Kriminální	kriminální	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinkují	účinkovat	k5eAaImIp3nP
Jan	Jan	k1gMnSc1
Hartl	Hartl	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
Procházka	Procházka	k1gMnSc1
a	a	k8xC
Taťjana	Taťjana	k1gFnSc1
Medvecká	Medvecký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dramaturgie	dramaturgie	k1gFnSc1
Klára	Klára	k1gFnSc1
Fleyberková	Fleyberková	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gMnSc1
Dudík	Dudík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Parzifallův	Parzifallův	k2eAgInSc1d1
pád	pád	k1gInSc1
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinkuje	účinkovat	k5eAaImIp3nS
Jiří	Jiří	k1gMnSc1
Hromada	Hromada	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dramaturgie	dramaturgie	k1gFnSc1
Klára	Klára	k1gFnSc1
Fleyberková	Fleyberková	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Lukáš	Lukáš	k1gMnSc1
Hlavica	Hlavica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
H2O	H2O	k4
a	a	k8xC
tajná	tajný	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
mise	mise	k1gFnSc1
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
7	#num#	k4
dílů	díl	k1gInPc2
čtení	čtení	k1gNnSc2
na	na	k7c4
pokračování	pokračování	k1gNnSc4
podle	podle	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
Dimitrij	Dimitrij	k1gFnSc1
Dudík	Dudík	k1gInSc4
<g/>
,	,	kIx,
účinkuje	účinkovat	k5eAaImIp3nS
Taťjana	Taťjana	k1gFnSc1
Medvecká	Medvecký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Poezie	poezie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
kost	kost	k1gFnSc1
božího	boží	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
(	(	kIx(
<g/>
Petrov	Petrov	k1gInSc1
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zpěv	zpěv	k1gInSc1
motýlů	motýl	k1gMnPc2
(	(	kIx(
<g/>
Petrov	Petrov	k1gInSc1
1999	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
80-7227-041-9	80-7227-041-9	k4
</s>
<s>
Černý	černý	k2eAgInSc1d1
revolver	revolver	k1gInSc1
týdne	týden	k1gInSc2
(	(	kIx(
<g/>
Petrov	Petrov	k1gInSc1
2004	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
80-7227-189-X	80-7227-189-X	k4
</s>
<s>
Virgonaut	Virgonaut	k1gInSc1
(	(	kIx(
<g/>
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
2010	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7227-293-8	978-80-7227-293-8	k4
</s>
<s>
Pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
</s>
<s>
Mrkev	mrkev	k1gFnSc1
ho	on	k3xPp3gMnSc4
vcucla	vcucnout	k5eAaPmAgFnS
pod	pod	k7c4
zem	zem	k1gFnSc4
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-87596-23-4	978-80-87596-23-4	k4
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
točí	točit	k5eAaImIp3nS
film	film	k1gInSc1
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2014	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-87596-40-1	978-80-87596-40-1	k4
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
nejdřív	dříve	k6eAd3
urychlovač	urychlovač	k1gInSc4
a	a	k8xC
pak	pak	k6eAd1
zase	zase	k9
pomalič	pomalič	k1gInSc1
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2015	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-87596-63-0	978-80-87596-63-0	k4
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
našel	najít	k5eAaPmAgMnS
velkou	velký	k2eAgFnSc4d1
lásečku	lásečka	k1gFnSc4
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2016	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-87596-91-3	978-80-87596-91-3	k4
</s>
<s>
O	o	k7c6
Díře	díra	k1gFnSc6
z	z	k7c2
Trychtýře	trychtýř	k1gInSc2
(	(	kIx(
<g/>
Argo	Argo	k6eAd1
2016	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-257-2006-6	978-80-257-2006-6	k4
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
dobývá	dobývat	k5eAaImIp3nS
vesmír	vesmír	k1gInSc1
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2017	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7558-011-5	978-80-7558-011-5	k4
</s>
<s>
Napůlkočkanapůlpes	Napůlkočkanapůlpes	k1gInSc1
(	(	kIx(
<g/>
Svojtka	Svojtka	k1gFnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Alžbetou	Alžbetý	k2eAgFnSc7d1
Stankovou	Stanková	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-256-2312-1	978-80-256-2312-1	k4
</s>
<s>
H2O	H2O	k4
a	a	k8xC
tajná	tajný	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
mise	mise	k1gFnSc1
(	(	kIx(
<g/>
Abramis	Abramis	k1gInSc1
2017	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87618	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocenění	ocenění	k1gNnSc1
<g/>
:	:	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
2018	#num#	k4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
beletrie	beletrie	k1gFnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
prochází	procházet	k5eAaImIp3nS
divočinou	divočina	k1gFnSc7
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7558-038-2	978-80-7558-038-2	k4
</s>
<s>
Dům	dům	k1gInSc1
myšek	myška	k1gFnPc2
ABC	ABC	kA
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7558	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
53	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
básničky	básnička	k1gFnPc4
ke	k	k7c3
knížce	knížka	k1gFnSc3
nizozemské	nizozemský	k2eAgFnSc2d1
autorky	autorka	k1gFnSc2
Kariny	Karina	k1gFnSc2
Schaapmanové	Schaapmanová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
H2O	H2O	k4
a	a	k8xC
poklad	poklad	k1gInSc1
šíleného	šílený	k2eAgNnSc2d1
oka	oko	k1gNnSc2
(	(	kIx(
<g/>
Abramis	Abramis	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-8761-802-8	978-80-8761-802-8	k4
</s>
<s>
Fíla	Fíla	k1gFnSc1
<g/>
,	,	kIx,
Žofie	Žofie	k1gFnSc1
a	a	k8xC
Smaragdová	smaragdový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
(	(	kIx(
<g/>
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-204-4984-9	978-80-204-4984-9	k4
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
ostrouhá	ostrouhat	k5eAaPmIp3nS
křen	křen	k1gInSc1
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7558-071-9	978-80-7558-071-9	k4
</s>
<s>
Ohou	ohou	k0wR
<g/>
?	?	kIx.
</s>
<s desamb="1">
Kolik	kolika	k1gFnPc2
má	mít	k5eAaImIp3nS
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
nohou	noha	k1gFnPc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7558-076-4	978-80-7558-076-4	k4
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
zakládá	zakládat	k5eAaImIp3nS
pěvecký	pěvecký	k2eAgInSc4d1
sbor	sbor	k1gInSc4
netopejrů	netopejr	k1gInPc2
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7558-097-9	978-80-7558-097-9	k4
</s>
<s>
H2O	H2O	k4
a	a	k8xC
pastýřové	pastýřové	k?
snů	sen	k1gInPc2
(	(	kIx(
<g/>
Abramis	Abramis	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-87618-03-5	978-80-87618-03-5	k4
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
vyhání	vyhánět	k5eAaImIp3nS
koronavirus	koronavirus	k1gMnSc1
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7558-150-1	978-80-7558-150-1	k4
</s>
<s>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
krotí	krotit	k5eAaImIp3nS
kůrovce	kůrovec	k1gMnSc4
(	(	kIx(
<g/>
Meander	Meander	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7558-131-0	978-80-7558-131-0	k4
</s>
<s>
Drama	drama	k1gNnSc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
<g/>
…	…	k?
svatá	svatá	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
svatá	svatý	k2eAgFnSc1d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Ludmila	Ludmila	k1gFnSc1
(	(	kIx(
<g/>
Větrné	větrný	k2eAgInPc1d1
mlýny	mlýn	k1gInPc1
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Literární	literární	k2eAgFnSc1d1
věda	věda	k1gFnSc1
</s>
<s>
Ryby	Ryby	k1gFnPc1
katedrál	katedrála	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antologie	antologie	k1gFnSc1
české	český	k2eAgFnSc2d1
poesie	poesie	k1gFnSc2
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
(	(	kIx(
<g/>
Petrov	Petrov	k1gInSc1
2002	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
80-7227-116-4	80-7227-116-4	k4
</s>
<s>
Orgie	orgie	k1gFnSc1
obraznosti	obraznost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
ke	k	k7c3
třetímu	třetí	k4xOgMnSc3
výročí	výročí	k1gNnSc3
smrti	smrt	k1gFnSc2
Odilla	Odillo	k1gNnSc2
Stradického	Stradický	k2eAgNnSc2d1
ze	z	k7c2
Strdic	Strdic	k1gMnSc1
(	(	kIx(
<g/>
Krasoumná	krasoumný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
2009	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-904510-0-1	978-80-904510-0-1	k4
</s>
<s>
Zastoupen	zastoupen	k2eAgMnSc1d1
ve	v	k7c6
sbornících	sborník	k1gInPc6
</s>
<s>
Lepě	lepě	k6eAd1
svihlí	svihlit	k5eAaPmIp3nP
tlové	tlový	k2eAgFnPc1d1
(	(	kIx(
<g/>
Petrov	Petrov	k1gInSc1
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaszmije	Jaszmít	k5eAaPmIp3nS
Smukwijne	Smukwijn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Almanach	almanach	k1gInSc1
współczesnej	współczesnat	k5eAaImRp2nS,k5eAaPmRp2nS
poezji	poezj	k1gFnSc3
czeskiej	czeskiej	k1gInSc1
(	(	kIx(
<g/>
Katowice	Katowice	k1gFnSc1
2004	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
83-921851-0-2	83-921851-0-2	k4
</s>
<s>
Duchovní	duchovní	k2eAgFnSc1d1
kuchařka	kuchařka	k1gFnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
7	#num#	k4
<g/>
edm	edm	k?
2006	#num#	k4
(	(	kIx(
<g/>
Theo	Thea	k1gFnSc5
2006	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
80-239-7367-3	80-239-7367-3	k4
</s>
<s>
Mrdotaurus	Mrdotaurus	k1gMnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
k	k	k7c3
sedmdesátinám	sedmdesátina	k1gFnPc3
Ivana	Ivan	k1gMnSc4
Wernische	Wernisch	k1gMnSc4
(	(	kIx(
<g/>
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
2012	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7227-321-8	978-80-7227-321-8	k4
</s>
<s>
Dvě	dva	k4xCgFnPc1
cesty	cesta	k1gFnPc1
Toníka	Toník	k1gMnSc2
Dvořáka	Dvořák	k1gMnSc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Miliónový	miliónový	k2eAgInSc4d1
časy	čas	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povídky	povídka	k1gFnPc4
pro	pro	k7c4
Adru	Adra	k1gFnSc4
(	(	kIx(
<g/>
Argo	Argo	k6eAd1
2014	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-257-1029-6	978-80-257-1029-6	k4
</s>
<s>
Kabinet	kabinet	k1gInSc1
sedmi	sedm	k4xCc2
probodnutých	probodnutý	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
noir	noir	k1gMnSc1
(	(	kIx(
<g/>
Paseka	paseka	k1gFnSc1
2016	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7432-765-0	978-80-7432-765-0	k4
</s>
<s>
Ahaswehrmacht	Ahaswehrmacht	k1gMnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Ve	v	k7c6
stínu	stín	k1gInSc6
Říše	říše	k1gFnSc1
(	(	kIx(
<g/>
Epocha	epocha	k1gFnSc1
2017	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7557-083-3	978-80-7557-083-3	k4
</s>
<s>
The	The	k?
Cabinet	Cabinet	k1gInSc1
of	of	k?
Seven	Seven	k2eAgInSc1d1
Pierced	Pierced	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Prague	Prague	k1gFnSc1
noir	noira	k1gFnPc2
(	(	kIx(
<g/>
Akashic	Akashice	k1gFnPc2
Books	Books	k1gInSc1
<g/>
,	,	kIx,
Brooklyn	Brooklyn	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
9781617755293	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
angličtiny	angličtina	k1gFnSc2
přeložila	přeložit	k5eAaPmAgFnS
Miriam	Miriam	k1gFnSc1
Margala	Margala	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pumpa	pumpa	k1gFnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Město	město	k1gNnSc1
mezi	mezi	k7c7
zelenými	zelený	k2eAgInPc7d1
kopci	kopec	k1gInPc7
(	(	kIx(
<g/>
Listen	listen	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7549-895-3	978-80-7549-895-3	k4
</s>
<s>
Caput	Caput	k2eAgInSc1d1
mortuum	mortuum	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Ve	v	k7c6
stínu	stín	k1gInSc6
apokalypsy	apokalypsa	k1gFnSc2
(	(	kIx(
<g/>
Epocha	epocha	k1gFnSc1
2018	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7557-156-4	978-80-7557-156-4	k4
</s>
<s>
Infernographia	Infernographia	k1gFnSc1
perpetua	perpetuum	k1gNnSc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Ve	v	k7c6
stínu	stín	k1gInSc6
magie	magie	k1gFnSc2
(	(	kIx(
<g/>
Epocha	epocha	k1gFnSc1
2019	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7557-218-9	978-80-7557-218-9	k4
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
hřbitovního	hřbitovní	k2eAgInSc2d1
trolejbusu	trolejbus	k1gInSc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Linka	linka	k1gFnSc1
110	#num#	k4
<g/>
.	.	kIx.
110	#num#	k4
let	léto	k1gNnPc2
MHD	MHD	kA
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
(	(	kIx(
<g/>
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
města	město	k1gNnSc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
2019	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-270-7005-3	978-80-270-7005-3	k4
</s>
<s>
Proč	proč	k6eAd1
se	se	k3xPyFc4
velikonoční	velikonoční	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
malují	malovat	k5eAaImIp3nP
na	na	k7c4
červeno	červen	k2eAgNnSc4d1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Hurá	hurá	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
Velikonoce	Velikonoce	k1gFnPc1
(	(	kIx(
<g/>
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
2020	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-204-5652-6	978-80-204-5652-6	k4
</s>
<s>
Turbína	turbína	k1gFnSc1
a	a	k8xC
mordýrna	mordýrna	k?
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Krvavý	krvavý	k2eAgInSc1d1
Bronx	Bronx	k1gInSc1
(	(	kIx(
<g/>
Host	host	k1gMnSc1
a	a	k8xC
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
2020	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7227-436-9	978-80-7227-436-9	k4
</s>
<s>
Kar	kar	k1gInSc1
v	v	k7c6
karanténě	karanténa	k1gFnSc6
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Za	za	k7c7
oknem	okno	k1gNnSc7
:	:	kIx,
19	#num#	k4
spisovatelů	spisovatel	k1gMnPc2
proti	proti	k7c3
covid-	covid-	k?
<g/>
19	#num#	k4
(	(	kIx(
<g/>
Prostor	prostor	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-7260-451-7	978-80-7260-451-7	k4
</s>
<s>
Dar	dar	k1gInSc1
smrtelnosti	smrtelnost	k1gFnSc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
ROBOT	robot	k1gInSc1
<g/>
100	#num#	k4
<g/>
:	:	kIx,
Povídky	povídka	k1gFnPc1
(	(	kIx(
<g/>
Argo	Argo	k6eAd1
2020	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
978-80-257-3341-7	978-80-257-3341-7	k4
</s>
<s>
Překlady	překlad	k1gInPc4
</s>
<s>
El	Ela	k1gFnPc2
molino	molino	k1gNnSc1
de	de	k?
momias	momiasa	k1gFnPc2
<g/>
,	,	kIx,
el	ela	k1gFnPc2
descubrimiento	descubrimiento	k1gNnSc1
revolucionario	revolucionario	k1gMnSc1
del	del	k?
comisario	comisario	k6eAd1
Durman	durman	k1gInSc1
/	/	kIx~
<g/>
Mlýn	mlýn	k1gInSc1
na	na	k7c4
mumie	mumie	k1gFnPc4
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Tropo	Tropo	k?
Editores	Editores	k1gInSc1
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
španělštiny	španělština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Daniel	Daniel	k1gMnSc1
Ordoñ	Ordoñ	k1gMnSc1
</s>
<s>
Młyn	Młyn	k1gInSc1
do	do	k7c2
mumii	mumie	k1gFnSc4
<g/>
,	,	kIx,
czyli	czyle	k1gFnSc4
Przewrotne	Przewrotne	k1gMnSc2
odkrycie	odkrycie	k1gFnSc2
komisarza	komisarz	k1gMnSc2
Durmana	Durman	k1gMnSc2
/	/	kIx~
<g/>
Mlýn	mlýn	k1gInSc1
na	na	k7c4
mumie	mumie	k1gFnPc4
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Stara	Stara	k1gMnSc1
Szkoła	Szkoła	k1gMnSc1
<g/>
,	,	kIx,
Wołów	Wołów	k1gFnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
polštiny	polština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Mirosław	Mirosław	k1gMnSc1
Śmigielski	Śmigielsk	k1gFnSc2
</s>
<s>
Múmiamalom	Múmiamalom	k1gInSc1
/	/	kIx~
<g/>
Mlýn	mlýn	k1gInSc1
na	na	k7c4
mumie	mumie	k1gFnPc4
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Metropolis	Metropolis	k1gInSc1
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
Budapest	Budapest	k1gFnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
maďarštiny	maďarština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Gábor	Gábor	k1gMnSc1
Hanzelik	Hanzelik	k1gMnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
9786155628047	#num#	k4
</s>
<s>
Melnica	Melnica	k1gFnSc1
za	za	k7c4
mumii	mumie	k1gFnSc4
<g/>
,	,	kIx,
ili	ili	k?
Povratnoto	Povratnota	k1gFnSc5
razkritie	razkritie	k1gFnPc4
na	na	k7c6
komisarja	komisarja	k6eAd1
Durman	durman	k1gInSc1
/	/	kIx~
<g/>
Mlýn	mlýn	k1gInSc1
na	na	k7c4
mumie	mumie	k1gFnPc4
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Izida	Izida	k1gFnSc1
<g/>
,	,	kIx,
Sofia	Sofia	k1gFnSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
bulharštiny	bulharština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Krasimir	Krasimir	k1gMnSc1
Prodanov	Prodanov	k1gInSc4
</s>
<s>
The	The	k?
Cabinet	Cabinet	k1gInSc1
of	of	k?
Seven	Seven	k2eAgInSc1d1
Pierced	Pierced	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
In	In	k1gFnSc1
<g/>
:	:	kIx,
Prague	Prague	k1gNnSc1
Noir	Noira	k1gFnPc2
(	(	kIx(
<g/>
Akashic	Akashice	k1gFnPc2
Books	Books	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781617755293	#num#	k4
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
Miriam	Miriam	k1gFnSc1
Margala	Margala	k1gFnSc1
</s>
<s>
Furda	Furda	k1gMnSc1
borz	borz	k1gMnSc1
előbb	előbb	k1gMnSc1
részecskegyorsítót	részecskegyorsítót	k1gMnSc1
<g/>
,	,	kIx,
majd	majd	k1gMnSc1
részecselassítót	részecselassítót	k1gMnSc1
épít	épít	k1gMnSc1
/	/	kIx~
<g/>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
staví	stavit	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
nejdřív	dříve	k6eAd3
urychlovač	urychlovač	k1gInSc4
a	a	k8xC
pak	pak	k6eAd1
zase	zase	k9
pomalič	pomalič	k1gMnSc1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Csirimojó	Csirimojó	k1gFnSc1
<g/>
,	,	kIx,
Budapest	Budapest	k1gFnSc1
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
maďarštiny	maďarština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Gábor	Gábor	k1gMnSc1
Hanzelik	Hanzelik	k1gMnSc1
</s>
<s>
Furda	Furda	k1gMnSc1
borz	borz	k1gMnSc1
filmet	filmet	k1gMnSc1
forgat	forgat	k2eAgMnSc1d1
/	/	kIx~
<g/>
Jezevec	jezevec	k1gMnSc1
Chrujda	Chrujda	k1gMnSc1
točí	točit	k5eAaImIp3nS
film	film	k1gInSc4
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Csirimojó	Csirimojó	k1gFnSc1
<g/>
,	,	kIx,
Budapest	Budapest	k1gFnSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
maďarštiny	maďarština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Gábor	Gábor	k1gMnSc1
Hanzelik	Hanzelik	k1gMnSc1
</s>
<s>
Pérák	pérák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Superheld	Superheld	k1gInSc1
aus	aus	k?
Prag	Prag	k1gInSc1
(	(	kIx(
<g/>
edition	edition	k1gInSc1
clandestin	clandestin	k1gInSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
němčiny	němčina	k1gFnSc2
přeložila	přeložit	k5eAaPmAgFnS
Maria	Maria	k1gFnSc1
Sileny	Silena	k1gFnSc2
</s>
<s>
Ctiborove	Ctiborov	k1gInSc5
pustolovine	pustolovin	k1gInSc5
u	u	k7c2
podzemlju	podzemlju	k5eAaPmIp1nS
/	/	kIx~
<g/>
Mrkev	mrkev	k1gFnSc1
ho	on	k3xPp3gMnSc4
vcucla	vcucnout	k5eAaPmAgFnS
pod	pod	k7c4
zem	zem	k1gFnSc4
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Krug	Krug	k1gMnSc1
Knjiga	Knjiga	k1gFnSc1
<g/>
,	,	kIx,
Zagreb	Zagreb	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
chorvatštiny	chorvatština	k1gFnSc2
přeložila	přeložit	k5eAaPmAgFnS
Sanja	Sanja	k1gFnSc1
Milićević	Milićević	k1gFnSc1
Armada	Armada	k1gFnSc1
</s>
<s>
Bezrożec	Bezrożec	k1gMnSc1
/	/	kIx~
<g/>
Nulorožec	Nulorožec	k1gMnSc1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Wydawnictwo	Wydawnictwo	k1gNnSc1
Dowody	Dowoda	k1gFnSc2
na	na	k7c4
Istnienie	Istnienie	k1gFnPc4
<g/>
,	,	kIx,
Warszawa	Warszawa	k1gFnSc1
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
9788365970442	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
polštiny	polština	k1gFnSc2
přeložila	přeložit	k5eAaPmAgFnS
Elżbieta	Elżbiet	k2eAgFnSc1d1
Zimna	Zimna	k1gFnSc1
</s>
<s>
H2O	H2O	k4
y	y	k?
la	la	k1gNnSc1
misión	misión	k1gMnSc1
acuática	acuáticus	k1gMnSc2
secreta	secret	k1gMnSc2
/	/	kIx~
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
a	a	k8xC
tajná	tajný	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
mise	mise	k1gFnSc1
<g/>
/	/	kIx~
(	(	kIx(
<g/>
Apache	Apache	k1gNnSc1
Libros	Librosa	k1gFnPc2
<g/>
,	,	kIx,
Spain	Spain	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
84	#num#	k4
<g/>
-	-	kIx~
<g/>
122530	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
španělštiny	španělština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Daniel	Daniel	k1gMnSc1
Ordoñ	Ordoñ	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Tvar	tvar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Prach	prach	k1gInSc1
jsem	být	k5eAaImIp1nS
a	a	k8xC
ty	ty	k3xPp2nSc1
mě	já	k3xPp1nSc4
obrátíš	obrátit	k5eAaPmIp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magazín	magazín	k1gInSc1
Uni	Uni	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Lubomír	Lubomír	k1gMnSc1
Machala	Machala	k1gMnSc1
<g/>
:	:	kIx,
Průvodce	průvodce	k1gMnSc1
po	po	k7c6
nových	nový	k2eAgNnPc6d1
jménech	jméno	k1gNnPc6
české	český	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
a	a	k8xC
prózy	próza	k1gFnPc1
1990	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
(	(	kIx(
<g/>
Rubico	Rubico	k6eAd1
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antologie	antologie	k1gFnSc1
české	český	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
díl	díl	k1gInSc1
1986	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
(	(	kIx(
<g/>
Dybbuk	Dybbuk	k1gInSc1
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Petr	Petr	k1gMnSc1
Stančík	Stančík	k1gMnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Odillo	Odillo	k1gNnSc1
Stradický	Stradický	k2eAgMnSc1d1
ze	z	k7c2
Strdic	Strdice	k1gFnPc2
</s>
<s>
Petr	Petr	k1gMnSc1
Stančík	Stančík	k1gMnSc1
na	na	k7c6
Portálu	portál	k1gInSc6
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
</s>
<s>
Stránky	stránka	k1gFnPc1
Bratrstva	bratrstvo	k1gNnSc2
obruče	obruč	k1gInSc2
s	s	k7c7
kladivem	kladivo	k1gNnSc7
</s>
<s>
Stránky	stránka	k1gFnPc1
nakladatelství	nakladatelství	k1gNnSc2
Druhé	druhý	k4xOgNnSc1
město	město	k1gNnSc1
</s>
<s>
Stránky	stránka	k1gFnPc1
nakladatelství	nakladatelství	k1gNnSc2
Meander	Meandra	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
nakladatelství	nakladatelství	k1gNnSc2
Tympanum	Tympanum	k1gInSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
2008411895	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5569	#num#	k4
1946	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2009022596	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
78957723	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2009022596	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
