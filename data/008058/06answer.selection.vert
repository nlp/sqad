<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
vlastností	vlastnost	k1gFnPc2	vlastnost
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
v	v	k7c6	v
"	"	kIx"	"
<g/>
obyčejné	obyčejný	k2eAgFnSc6d1	obyčejná
<g/>
"	"	kIx"	"
euklidovské	euklidovský	k2eAgFnSc6d1	euklidovská
rovině	rovina	k1gFnSc6	rovina
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
součet	součet	k1gInSc4	součet
velikostí	velikost	k1gFnPc2	velikost
jeho	jeho	k3xOp3gInPc2	jeho
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
úhlů	úhel	k1gInPc2	úhel
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
180	[number]	k4	180
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
π	π	k?	π
v	v	k7c6	v
obloukové	obloukový	k2eAgFnSc6d1	oblouková
míře	míra	k1gFnSc6	míra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
