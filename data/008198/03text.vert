<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Vlček	Vlček	k1gMnSc1	Vlček
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
obránce	obránce	k1gMnSc1	obránce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
ligovým	ligový	k2eAgInSc7d1	ligový
fotbalem	fotbal	k1gInSc7	fotbal
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
FC	FC	kA	FC
Viktoria	Viktoria	k1gFnSc1	Viktoria
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
85	[number]	k4	85
zápasech	zápas	k1gInPc6	zápas
a	a	k8xC	a
vsítil	vsítit	k5eAaPmAgMnS	vsítit
8	[number]	k4	8
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
oblékal	oblékat	k5eAaImAgMnS	oblékat
dres	dres	k1gInSc4	dres
pražské	pražský	k2eAgFnSc2d1	Pražská
Slavie	slavie	k1gFnSc2	slavie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dresu	dres	k1gInSc6	dres
Slavie	slavie	k1gFnSc2	slavie
Praha	Praha	k1gFnSc1	Praha
odehrál	odehrát	k5eAaPmAgInS	odehrát
82	[number]	k4	82
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
6	[number]	k4	6
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Euru	euro	k1gNnSc6	euro
2000	[number]	k4	2000
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
belgického	belgický	k2eAgMnSc2d1	belgický
Standard	standard	k1gInSc1	standard
Liè	Liè	k1gFnSc3	Liè
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
však	však	k9	však
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
a	a	k8xC	a
odehrál	odehrát	k5eAaPmAgMnS	odehrát
jen	jen	k9	jen
11	[number]	k4	11
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
do	do	k7c2	do
řeckého	řecký	k2eAgMnSc2d1	řecký
Panionios	Panionios	k1gInSc4	Panionios
GSS	GSS	kA	GSS
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
strávil	strávit	k5eAaPmAgMnS	strávit
4	[number]	k4	4
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc2	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
81	[number]	k4	81
zápasech	zápas	k1gInPc6	zápas
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
9	[number]	k4	9
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
ještě	ještě	k9	ještě
krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
kyperském	kyperský	k2eAgInSc6d1	kyperský
Ethnikos	Ethnikos	k1gInSc1	Ethnikos
Achnas	Achnas	k1gInSc4	Achnas
FC	FC	kA	FC
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odehrál	odehrát	k5eAaPmAgInS	odehrát
13	[number]	k4	13
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
stihl	stihnout	k5eAaPmAgMnS	stihnout
odehrát	odehrát	k5eAaPmF	odehrát
ještě	ještě	k9	ještě
10	[number]	k4	10
utkání	utkání	k1gNnPc2	utkání
a	a	k8xC	a
zakončil	zakončit	k5eAaPmAgInS	zakončit
svou	svůj	k3xOyFgFnSc4	svůj
ligovou	ligový	k2eAgFnSc4d1	ligová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentace	reprezentace	k1gFnSc2	reprezentace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
reprezentaci	reprezentace	k1gFnSc6	reprezentace
odehrál	odehrát	k5eAaPmAgMnS	odehrát
celkem	celek	k1gInSc7	celek
18	[number]	k4	18
utkání	utkání	k1gNnSc2	utkání
(	(	kIx(	(
<g/>
9	[number]	k4	9
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
4	[number]	k4	4
remízy	remíz	k1gInPc1	remíz
<g/>
,	,	kIx,	,
5	[number]	k4	5
proher	prohra	k1gFnPc2	prohra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gólově	gólově	k6eAd1	gólově
se	se	k3xPyFc4	se
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nenastoupil	nastoupit	k5eNaPmAgMnS	nastoupit
do	do	k7c2	do
žádného	žádný	k3yNgInSc2	žádný
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
utkání	utkání	k1gNnPc2	utkání
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
na	na	k7c6	na
šampionátu	šampionát	k1gInSc6	šampionát
odehrál	odehrát	k5eAaPmAgMnS	odehrát
(	(	kIx(	(
<g/>
postupně	postupně	k6eAd1	postupně
prohra	prohra	k1gFnSc1	prohra
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
s	s	k7c7	s
Nizozemím	Nizozemí	k1gNnSc7	Nizozemí
<g/>
,	,	kIx,	,
prohra	prohra	k1gFnSc1	prohra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
výhra	výhra	k1gFnSc1	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápasy	zápas	k1gInPc1	zápas
Petra	Petr	k1gMnSc2	Petr
Vlčka	Vlček	k1gMnSc2	Vlček
v	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
české	český	k2eAgFnSc2d1	Česká
reprezentace	reprezentace	k1gFnSc2	reprezentace
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Statistiky	statistika	k1gFnPc1	statistika
Petra	Petr	k1gMnSc2	Petr
Vlčka	Vlček	k1gMnSc2	Vlček
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
asociace	asociace	k1gFnSc2	asociace
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
