<s>
Paršovice	Paršovice	k1gFnPc1	Paršovice
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zvané	zvaný	k2eAgNnSc1d1	zvané
Záhoří	Záhoří	k1gNnSc1	Záhoří
<g/>
,	,	kIx,	,
cca	cca	kA	cca
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
Hranic	Hranice	k1gFnPc2	Hranice
<g/>
,	,	kIx,	,
12	[number]	k4	12
km	km	kA	km
od	od	k7c2	od
Bystřice	Bystřice	k1gFnSc2	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
mikroregionu	mikroregion	k1gInSc2	mikroregion
Záhoran	Záhoran	k1gInSc1	Záhoran
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
322	[number]	k4	322
m	m	kA	m
<g/>
,	,	kIx,	,
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
1356	[number]	k4	1356
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
