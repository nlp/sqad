<s>
Locale	Locale	k6eAd1
</s>
<s>
Locale	Locale	k6eAd1
(	(	kIx(
<g/>
vyslovováno	vyslovovat	k5eAaImNgNnS
[	[	kIx(
<g/>
loʊ	loʊ	k1gInSc1
<g/>
,	,	kIx,
-ˈ	-ˈ	k1gInSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
informatice	informatika	k1gFnSc6
soubor	soubor	k1gInSc1
parametrů	parametr	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
definují	definovat	k5eAaBmIp3nP
uživatelův	uživatelův	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
stát	stát	k1gInSc4
a	a	k8xC
jiné	jiný	k2eAgFnPc4d1
zvláštnosti	zvláštnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
následně	následně	k6eAd1
projeví	projevit	k5eAaPmIp3nS
v	v	k7c6
uživatelském	uživatelský	k2eAgNnSc6d1
rozhraní	rozhraní	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
locale	locale	k6eAd1
skládá	skládat	k5eAaImIp3nS
přinejmenším	přinejmenším	k6eAd1
z	z	k7c2
identifikátoru	identifikátor	k1gInSc2
jazyka	jazyk	k1gInSc2
a	a	k8xC
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
unixových	unixový	k2eAgInPc6d1
a	a	k8xC
jiných	jiný	k2eAgInPc6d1
systémech	systém	k1gInPc6
založených	založený	k2eAgInPc2d1
POSIXu	POSIXus	k1gInSc6
jsou	být	k5eAaImIp3nP
locale	locale	k6eAd1
identifikátory	identifikátor	k1gInPc4
podobné	podobný	k2eAgInPc4d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
definice	definice	k1gFnSc1
jazyka	jazyk	k1gInSc2
značek	značka	k1gFnPc2
BCP	BCP	kA
47	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
modifikátor	modifikátor	k1gInSc1
locale	locale	k6eAd1
je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
odlišně	odlišně	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
obsahuje	obsahovat	k5eAaImIp3nS
navíc	navíc	k6eAd1
část	část	k1gFnSc1
se	s	k7c7
znakovou	znakový	k2eAgFnSc7d1
sadou	sada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Definice	definice	k1gFnSc1
formátu	formát	k1gInSc2
<g/>
:	:	kIx,
[	[	kIx(
<g/>
jazyk	jazyk	k1gInSc1
<g/>
[	[	kIx(
<g/>
_území	_území	k1gNnSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
.	.	kIx.
<g/>
znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
@	@	kIx~
<g/>
modifikátor	modifikátor	k1gInSc4
<g/>
]]	]]	k?
(	(	kIx(
<g/>
např.	např.	kA
čeština	čeština	k1gFnSc1
s	s	k7c7
kódováním	kódování	k1gNnSc7
UTF-8	UTF-8	k1gFnSc2
je	být	k5eAaImIp3nS
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
locale	locale	k6eAd1
</s>
<s>
Locale	Locale	k6eAd1
definují	definovat	k5eAaBmIp3nP
různé	různý	k2eAgFnPc1d1
jazykově	jazykově	k6eAd1
specifické	specifický	k2eAgFnPc1d1
odlišnosti	odlišnost	k1gFnPc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
formát	formát	k1gInSc1
čísel	číslo	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
oddělování	oddělování	k1gNnSc1
řádů	řád	k1gInPc2
<g/>
,	,	kIx,
desetinná	desetinný	k2eAgFnSc1d1
tečka	tečka	k1gFnSc1
nebo	nebo	k8xC
čárka	čárka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
roztřídění	roztřídění	k1gNnSc1
znaků	znak	k1gInPc2
do	do	k7c2
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
pravidla	pravidlo	k1gNnSc2
pro	pro	k7c4
změnu	změna	k1gFnSc4
velikosti	velikost	k1gFnSc2
znaků	znak	k1gInPc2
(	(	kIx(
<g/>
malé	malý	k2eAgNnSc4d1
<g/>
/	/	kIx~
<g/>
velké	velký	k2eAgNnSc4d1
písmeno	písmeno	k1gNnSc4
<g/>
,	,	kIx,
čČ	čČ	k?
<g/>
,	,	kIx,
řŘ	řŘ	k?
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
</s>
<s>
formát	formát	k1gInSc1
data	datum	k1gNnSc2
a	a	k8xC
času	čas	k1gInSc2
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1970	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
porovnání	porovnání	k1gNnSc4
řetězců	řetězec	k1gInPc2
(	(	kIx(
<g/>
třídění	třídění	k1gNnSc1
dle	dle	k7c2
abecedy	abeceda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
nastavení	nastavení	k1gNnSc1
měny	měna	k1gFnSc2
(	(	kIx(
<g/>
Kč	Kč	kA
<g/>
,	,	kIx,
$	$	kIx~
<g/>
,	,	kIx,
€	€	k?
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
velikost	velikost	k1gFnSc1
papíru	papír	k1gInSc2
</s>
<s>
další	další	k2eAgNnSc4d1
drobné	drobný	k2eAgNnSc4d1
nastavení	nastavení	k1gNnSc4
<g/>
…	…	k?
</s>
<s>
Nastavení	nastavení	k1gNnSc1
locale	locale	k6eAd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
formátování	formátování	k1gNnSc4
výstupu	výstup	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
časové	časový	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
a	a	k8xC
letní	letní	k2eAgInSc4d1
čas	čas	k1gInSc4
obvykle	obvykle	k6eAd1
nejsou	být	k5eNaImIp3nP
součástí	součást	k1gFnSc7
nastavení	nastavení	k1gNnSc2
locale	locale	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Méně	málo	k6eAd2
obvyklé	obvyklý	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c4
zmínku	zmínka	k1gFnSc4
stojící	stojící	k2eAgFnSc4d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
formátování	formátování	k1gNnSc4
vstupů	vstup	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
definováno	definovat	k5eAaBmNgNnS
na	na	k7c6
úrovni	úroveň	k1gFnSc6
aplikace	aplikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Locale	Locala	k1gFnSc3
dále	daleko	k6eAd2
definuje	definovat	k5eAaBmIp3nS
obecné	obecný	k2eAgNnSc1d1
rozložení	rozložení	k1gNnSc1
rozložení	rozložení	k1gNnSc2
kláves	klávesa	k1gFnPc2
(	(	kIx(
<g/>
pro	pro	k7c4
daný	daný	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podpora	podpora	k1gFnSc1
v	v	k7c6
programovacích	programovací	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
</s>
<s>
Podpora	podpora	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
těchto	tento	k3xDgInPc6
programovacích	programovací	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
C	C	kA
</s>
<s>
C	C	kA
<g/>
++	++	k?
</s>
<s>
Eiffel	Eiffel	k1gMnSc1
</s>
<s>
Java	Java	k6eAd1
</s>
<s>
Microsoft	Microsoft	kA
.	.	kIx.
<g/>
NET	NET	kA
framework	framework	k1gInSc1
</s>
<s>
Rebol	Rebol	k1gInSc1
</s>
<s>
Ruby	rub	k1gInPc1
</s>
<s>
Perl	perl	k1gInSc1
</s>
<s>
PHP	PHP	kA
</s>
<s>
Python	Python	k1gMnSc1
</s>
<s>
XML	XML	kA
</s>
<s>
a	a	k8xC
další	další	k2eAgNnSc1d1
dnešní	dnešní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
Unicode	Unicod	k1gInSc5
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
definována	definovat	k5eAaBmNgNnP
podobně	podobně	k6eAd1
jako	jako	k9
BCP	BCP	kA
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
definována	definován	k2eAgFnSc1d1
obvykle	obvykle	k6eAd1
pouze	pouze	k6eAd1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
kódů	kód	k1gInPc2
ISO	ISO	kA
639	#num#	k4
a	a	k8xC
ISO	ISO	kA
3166-1	3166-1	k4
alpha-	alpha-	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
POSIX	POSIX	kA
platformy	platforma	k1gFnPc1
</s>
<s>
V	v	k7c6
unixových	unixový	k2eAgFnPc6d1
<g/>
,	,	kIx,
linuxových	linuxový	k2eAgFnPc6d1
a	a	k8xC
jiných	jiný	k2eAgFnPc6d1
POSIX	POSIX	kA
platformách	platforma	k1gFnPc6
je	být	k5eAaImIp3nS
locale	locale	k6eAd1
identifikátor	identifikátor	k1gInSc1
definován	definovat	k5eAaBmNgInS
podobně	podobně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
BCP	BCP	kA
47	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
locale	locale	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
navíc	navíc	k6eAd1
znakovou	znakový	k2eAgFnSc4d1
sadu	sada	k1gFnSc4
(	(	kIx(
<g/>
implicitní	implicitní	k2eAgFnSc1d1
znaková	znakový	k2eAgFnSc1d1
sada	sada	k1gFnSc1
pro	pro	k7c4
češtinu	čeština	k1gFnSc4
je	být	k5eAaImIp3nS
totiž	totiž	k9
stále	stále	k6eAd1
ISO-	ISO-	k1gFnPc1
<g/>
8859	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
příkladu	příklad	k1gInSc6
je	být	k5eAaImIp3nS
výstup	výstup	k1gInSc1
příkazu	příkaz	k1gInSc2
locale	locale	k6eAd1
pro	pro	k7c4
český	český	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
(	(	kIx(
<g/>
cs	cs	k?
<g/>
)	)	kIx)
a	a	k8xC
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
)	)	kIx)
s	s	k7c7
užitím	užití	k1gNnSc7
kódování	kódování	k1gNnSc2
UTF-	UTF-	k1gFnSc2
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
v	v	k7c6
systému	systém	k1gInSc6
nastavena	nastavit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
proměnná	proměnná	k1gFnSc1
prostředí	prostředí	k1gNnSc2
LANG	Lang	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
nejnižší	nízký	k2eAgFnSc4d3
váhu	váha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastavením	nastavení	k1gNnSc7
proměnné	proměnná	k1gFnSc2
prostředí	prostředí	k1gNnSc2
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
váhou	váha	k1gFnSc7
(	(	kIx(
<g/>
ve	v	k7c6
výpisu	výpis	k1gInSc6
jsou	být	k5eAaImIp3nP
níže	nízce	k6eAd2
<g/>
)	)	kIx)
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
předefinování	předefinování	k1gNnSc3
proměnné	proměnná	k1gFnSc2
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
váhou	váha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k9
možné	možný	k2eAgNnSc1d1
například	například	k6eAd1
nastavit	nastavit	k5eAaPmF
prostředí	prostředí	k1gNnSc4
programu	program	k1gInSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
české	český	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
používán	používán	k2eAgInSc1d1
anglický	anglický	k2eAgInSc1d1
zápis	zápis	k1gInSc4
čísel	číslo	k1gNnPc2
s	s	k7c7
desetinnou	desetinný	k2eAgFnSc7d1
tečkou	tečka	k1gFnSc7
(	(	kIx(
<g/>
tj.	tj.	kA
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
nastavit	nastavit	k5eAaPmF
LC_NUMERIC	LC_NUMERIC	k1gMnPc1
na	na	k7c4
nějaký	nějaký	k3yIgInSc4
anglický	anglický	k2eAgInSc4d1
locale	locala	k1gFnSc3
kód	kód	k1gInSc4
<g/>
,	,	kIx,
například	například	k6eAd1
en_US	en_US	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
váhu	váha	k1gFnSc4
má	mít	k5eAaImIp3nS
proměnná	proměnný	k2eAgFnSc1d1
LC_ALL	LC_ALL	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
přebije	přebít	k5eAaPmIp3nS
nastavení	nastavení	k1gNnSc4
jakýchkoliv	jakýkoliv	k3yIgFnPc2
ostatních	ostatní	k2eAgFnPc2d1
proměnných	proměnná	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
user	usrat	k5eAaPmRp2nS
<g/>
@	@	kIx~
<g/>
machine	machinout	k5eAaPmIp3nS
~	~	kIx~
<g/>
]	]	kIx)
<g/>
$	$	kIx~
locale	locale	k6eAd1
</s>
<s>
LANG	Lang	k1gMnSc1
<g/>
=	=	kIx~
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
</s>
<s>
LC_CTYPE	LC_CTYPE	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_NUMERIC	LC_NUMERIC	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_TIME	LC_TIME	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_COLLATE	LC_COLLATE	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_MONETARY	LC_MONETARY	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_MESSAGES	LC_MESSAGES	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_PAPER	LC_PAPER	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_NAME	LC_NAME	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_ADDRESS	LC_ADDRESS	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_TELEPHONE	LC_TELEPHONE	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_MEASUREMENT	LC_MEASUREMENT	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_IDENTIFICATION	LC_IDENTIFICATION	k?
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
cs_CZ	cs_CZ	k?
<g/>
.	.	kIx.
<g/>
UTF-	UTF-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
"	"	kIx"
</s>
<s>
LC_ALL	LC_ALL	k?
<g/>
=	=	kIx~
</s>
<s>
Pokud	pokud	k8xS
není	být	k5eNaImIp3nS
locale	locale	k6eAd1
definováno	definovat	k5eAaBmNgNnS
<g/>
,	,	kIx,
předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
nastavení	nastavení	k1gNnSc4
na	na	k7c6
C	C	kA
–	–	k?
C	C	kA
(	(	kIx(
<g/>
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
je	být	k5eAaImIp3nS
míněno	míněn	k2eAgNnSc1d1
v	v	k7c6
podstatě	podstata	k1gFnSc6
anglické	anglický	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Specifika	specifika	k1gFnSc1
pro	pro	k7c4
platformy	platforma	k1gFnPc4
Microsoft	Microsoft	kA
</s>
<s>
V	v	k7c6
Microsoft	Microsoft	kA
Windows	Windows	kA
je	být	k5eAaImIp3nS
historicky	historicky	k6eAd1
pro	pro	k7c4
„	„	k?
<g/>
neřízený	řízený	k2eNgInSc4d1
kód	kód	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
unmanaged	unmanaged	k1gMnSc1
code	cod	k1gMnSc2
<g/>
)	)	kIx)
používán	používat	k5eAaImNgInS
locale	locale	k6eAd1
identifikátor	identifikátor	k1gInSc1
(	(	kIx(
<g/>
LCID	LCID	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
číslo	číslo	k1gNnSc1
1033	#num#	k4
pro	pro	k7c4
angličtinu	angličtina	k1gFnSc4
(	(	kIx(
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
1041	#num#	k4
pro	pro	k7c4
japonštinu	japonština	k1gFnSc4
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
LCID	LCID	kA
číslo	číslo	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
kódu	kód	k1gInSc2
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
spodních	spodní	k2eAgInPc2d1
10	#num#	k4
bitů	bit	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
kulturního	kulturní	k2eAgInSc2d1
kódu	kód	k1gInSc2
(	(	kIx(
<g/>
horní	horní	k2eAgInPc4d1
bity	bit	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
zapisují	zapisovat	k5eAaImIp3nP
hexadecimálně	hexadecimálně	k6eAd1
<g/>
,	,	kIx,
například	například	k6eAd1
0	#num#	k4
<g/>
x	x	k?
<g/>
0	#num#	k4
<g/>
409	#num#	k4
nebo	nebo	k8xC
0	#num#	k4
<g/>
x	x	k?
<g/>
0	#num#	k4
<g/>
411	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Microsoft	Microsoft	kA
zavedl	zavést	k5eAaPmAgMnS
pro	pro	k7c4
.	.	kIx.
<g/>
NET	NET	kA
API	API	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
tyto	tento	k3xDgInPc4
kódy	kód	k1gInPc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
implementací	implementace	k1gFnPc2
API	API	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
vypuštěna	vypustit	k5eAaPmNgFnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
funkce	funkce	k1gFnSc1
pro	pro	k7c4
řešení	řešení	k1gNnSc4
problémů	problém	k1gInPc2
s	s	k7c7
doménovými	doménový	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
znaky	znak	k1gInPc7
národních	národní	k2eAgFnPc2d1
abeced	abeceda	k1gFnPc2
–	–	k?
tzv.	tzv.	kA
IDN	IDN	kA
(	(	kIx(
<g/>
Internationalized	Internationalized	k1gMnSc1
Domain	Domain	k1gMnSc1
Names	Names	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnSc1d1
API	API	kA
s	s	k7c7
LCID	LCID	kA
byly	být	k5eAaImAgFnP
uvedeny	uvést	k5eAaPmNgFnP
v	v	k7c6
systému	systém	k1gInSc6
Windows	Windows	kA
Vista	vista	k2eAgNnSc1d1
Beta	beta	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
vydání	vydání	k1gNnSc2
Windows	Windows	kA
Vista	vista	k2eAgInPc6d1
přechází	přecházet	k5eAaImIp3nS
Microsoft	Microsoft	kA
na	na	k7c4
názvy	název	k1gInPc4
locale	locale	k6eAd1
podle	podle	k7c2
BCP	BCP	kA
47	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
nahradit	nahradit	k5eAaPmF
všechna	všechen	k3xTgNnPc1
zastaralá	zastaralý	k2eAgNnPc1d1
rozhraní	rozhraní	k1gNnPc1
založená	založený	k2eAgNnPc1d1
na	na	k7c6
LCID	LCID	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
definition	definition	k1gInSc1
of	of	k?
locale	locale	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dictionary	Dictionara	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Locale	Locala	k1gFnSc3
ID	Ida	k1gFnPc2
(	(	kIx(
<g/>
LCID	LCID	kA
<g/>
)	)	kIx)
Chart	charta	k1gFnPc2
(	(	kIx(
<g/>
seznam	seznam	k1gInSc4
jazyků	jazyk	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
LCID	LCID	kA
<g/>
)	)	kIx)
<g/>
↑	↑	k?
DownlevelGetLocaleScripts	DownlevelGetLocaleScripts	k1gInSc1
Function	Function	k1gInSc1
<g/>
↑	↑	k?
Locale	Locala	k1gFnSc3
Names	Names	k1gInSc4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Internacionalizace	internacionalizace	k1gFnSc1
a	a	k8xC
lokalizace	lokalizace	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
BCP	BCP	kA
47	#num#	k4
</s>
<s>
Jazykové	jazyk	k1gMnPc1
subtag	subtaga	k1gFnPc2
registry	registr	k1gInPc4
</s>
<s>
Společná	společný	k2eAgFnSc1d1
národní	národní	k2eAgNnSc4d1
úložiště	úložiště	k1gNnSc4
dat	datum	k1gNnPc2
</s>
<s>
MS-LCID	MS-LCID	k?
<g/>
:	:	kIx,
reference	reference	k1gFnSc1
od	od	k7c2
Microsoft	Microsoft	kA
</s>
<s>
Microsoft	Microsoft	kA
LCID	LCID	kA
seznam	seznam	k1gInSc4
</s>
<s>
Tabulka	tabulka	k1gFnSc1
LCID	LCID	kA
s	s	k7c7
desetinnými	desetinný	k2eAgNnPc7d1
ekvivalenty	ekvivalent	k1gInPc4
od	od	k7c2
Microsoft	Microsoft	kA
</s>
<s>
POSIX	POSIX	kA
proměnné	proměnný	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
<s>
POSIX	POSIX	kA
locale	locale	k6eAd1
</s>
<s>
Debian	Debian	k1gInSc1
a	a	k8xC
locale	locale	k6eAd1
</s>
<s>
Článek	článek	k1gInSc1
"	"	kIx"
<g/>
The	The	k1gFnSc1
Standard	standard	k1gInSc1
C	C	kA
<g/>
++	++	k?
Locale	Locala	k1gFnSc3
<g/>
"	"	kIx"
od	od	k7c2
Nathana	Nathan	k1gMnSc2
C.	C.	kA
Myerse	Myerse	k1gFnSc2
</s>
<s>
Python	Python	k1gMnSc1
referce	referka	k1gFnSc3
</s>
<s>
Popis	popis	k1gInSc4
multi-	multi-	k?
jazykové	jazykový	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
-	-	kIx~
Linuxové	linuxový	k2eAgFnSc2d1
manuálové	manuálový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
</s>
<s>
Apache	Apache	k1gFnSc1
C	C	kA
<g/>
++	++	k?
<g/>
:	:	kIx,
Standardní	standardní	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
uživatelská	uživatelský	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
</s>
<s>
"	"	kIx"
<g/>
Collation	Collation	k1gInSc1
char	char	k1gInSc1
<g/>
"	"	kIx"
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
O.S.	O.S.	k1gFnPc4
</s>
<s>
NATSPEC	NATSPEC	kA
knihovna	knihovna	k1gFnSc1
</s>
<s>
Popis	popis	k1gInSc1
locale-	locale-	k?
související	související	k2eAgInSc1d1
s	s	k7c7
UNIX	UNIX	kA
proměnným	proměnná	k1gFnPc3
prostředím-	prostředím-	k?
Debian	Debiana	k1gFnPc2
Linux	Linux	kA
příručka	příručka	k1gFnSc1
</s>
<s>
Příručky	příručka	k1gFnPc1
pro	pro	k7c4
locale	locale	k6eAd1
na	na	k7c6
různých	různý	k2eAgFnPc6d1
platformách	platforma	k1gFnPc6
</s>
