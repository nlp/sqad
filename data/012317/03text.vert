<p>
<s>
Enzym	enzym	k1gInSc1	enzym
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
či	či	k8xC	či
složená	složený	k2eAgFnSc1d1	složená
bílkovina	bílkovina	k1gFnSc1	bílkovina
s	s	k7c7	s
katalytickou	katalytický	k2eAgFnSc7d1	katalytická
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
určují	určovat	k5eAaImIp3nP	určovat
povahu	povaha	k1gFnSc4	povaha
i	i	k8xC	i
rychlost	rychlost	k1gFnSc4	rychlost
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
většinu	většina	k1gFnSc4	většina
biochemických	biochemický	k2eAgInPc2d1	biochemický
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
o	o	k7c6	o
enzymech	enzym	k1gInPc6	enzym
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
enzymologie	enzymologie	k1gFnSc1	enzymologie
a	a	k8xC	a
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
všímat	všímat	k5eAaImF	všímat
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
dochází	docházet	k5eAaImIp3nS	docházet
např.	např.	kA	např.
při	při	k7c6	při
trávení	trávení	k1gNnSc6	trávení
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
enzymů	enzym	k1gInPc2	enzym
jsou	být	k5eAaImIp3nP	být
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgFnPc4	jenž
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
vážou	vázat	k5eAaImIp3nP	vázat
další	další	k2eAgFnPc1d1	další
přídatné	přídatný	k2eAgFnPc1d1	přídatná
molekuly	molekula	k1gFnPc1	molekula
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k9	jako
kofaktory	kofaktor	k1gInPc1	kofaktor
nebo	nebo	k8xC	nebo
prostetické	prostetický	k2eAgFnPc1d1	prostetická
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
katalýze	katalýza	k1gFnSc6	katalýza
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
enzymatická	enzymatický	k2eAgFnSc1d1	enzymatická
reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
aktivním	aktivní	k2eAgNnSc6d1	aktivní
místě	místo	k1gNnSc6	místo
enzymu	enzym	k1gInSc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Enzymů	enzym	k1gInPc2	enzym
je	být	k5eAaImIp3nS	být
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
do	do	k7c2	do
šesti	šest	k4xCc2	šest
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
oxidoreduktázy	oxidoreduktáza	k1gFnPc1	oxidoreduktáza
<g/>
,	,	kIx,	,
transferázy	transferáza	k1gFnPc1	transferáza
<g/>
,	,	kIx,	,
hydrolázy	hydroláza	k1gFnPc1	hydroláza
<g/>
,	,	kIx,	,
lyázy	lyáz	k1gInPc1	lyáz
<g/>
,	,	kIx,	,
izomerázy	izomeráza	k1gFnPc1	izomeráza
a	a	k8xC	a
ligázy	ligáza	k1gFnPc1	ligáza
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
společnou	společný	k2eAgFnSc4d1	společná
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
funkci	funkce	k1gFnSc4	funkce
<g/>
;	;	kIx,	;
snižují	snižovat	k5eAaImIp3nP	snižovat
aktivační	aktivační	k2eAgFnSc4d1	aktivační
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
Ea	Ea	k1gFnSc4	Ea
<g/>
)	)	kIx)	)
nutnou	nutný	k2eAgFnSc4d1	nutná
pro	pro	k7c4	pro
proběhnutí	proběhnutí	k1gNnSc4	proběhnutí
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
specifické	specifický	k2eAgFnPc1d1	specifická
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
jeden	jeden	k4xCgMnSc1	jeden
nebo	nebo	k8xC	nebo
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
substrátů	substrát	k1gInPc2	substrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jedním	jeden	k4xCgInSc7	jeden
definovaným	definovaný	k2eAgInSc7d1	definovaný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc2d1	spočívající
v	v	k7c6	v
ovlivnění	ovlivnění	k1gNnSc6	ovlivnění
rychlosti	rychlost	k1gFnSc2	rychlost
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
snižováním	snižování	k1gNnSc7	snižování
jejich	jejich	k3xOp3gFnSc2	jejich
aktivační	aktivační	k2eAgFnSc2d1	aktivační
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
zejména	zejména	k9	zejména
na	na	k7c4	na
koncentraci	koncentrace	k1gFnSc4	koncentrace
substrátu	substrát	k1gInSc2	substrát
<g/>
,	,	kIx,	,
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
pH	ph	kA	ph
a	a	k8xC	a
přítomnosti	přítomnost	k1gFnSc2	přítomnost
aktivátorů	aktivátor	k1gInPc2	aktivátor
a	a	k8xC	a
inhibitorů	inhibitor	k1gInPc2	inhibitor
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
enzymů	enzym	k1gInPc2	enzym
již	již	k6eAd1	již
našla	najít	k5eAaPmAgFnS	najít
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
i	i	k9	i
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
výzkumu	výzkum	k1gInSc2	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Vědu	věda	k1gFnSc4	věda
o	o	k7c6	o
enzymech	enzym	k1gInPc6	enzym
nazýváme	nazývat	k5eAaImIp1nP	nazývat
enzymologie	enzymologie	k1gFnPc4	enzymologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
existovalo	existovat	k5eAaImAgNnS	existovat
povědomí	povědomí	k1gNnSc1	povědomí
o	o	k7c6	o
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
trávení	trávení	k1gNnSc1	trávení
masa	maso	k1gNnSc2	maso
žaludeční	žaludeční	k2eAgFnSc7d1	žaludeční
šťávou	šťáva	k1gFnSc7	šťáva
nebo	nebo	k8xC	nebo
rozklad	rozklad	k1gInSc4	rozklad
škrobu	škrob	k1gInSc2	škrob
na	na	k7c4	na
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
cukry	cukr	k1gInPc4	cukr
účinkem	účinek	k1gInSc7	účinek
slin	slina	k1gFnPc2	slina
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
průběh	průběh	k1gInSc1	průběh
těchto	tento	k3xDgInPc2	tento
jevů	jev	k1gInPc2	jev
však	však	k9	však
byl	být	k5eAaImAgInS	být
zahalen	zahalit	k5eAaPmNgMnS	zahalit
tajemstvím	tajemství	k1gNnSc7	tajemství
a	a	k8xC	a
např.	např.	kA	např.
pepsin	pepsin	k1gInSc1	pepsin
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
až	až	k9	až
ve	v	k7c6	v
století	století	k1gNnSc6	století
devatenáctém	devatenáctý	k4xOgNnSc6	devatenáctý
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
pokrok	pokrok	k1gInSc1	pokrok
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
představují	představovat	k5eAaImIp3nP	představovat
výzkumy	výzkum	k1gInPc1	výzkum
slavného	slavný	k2eAgMnSc2d1	slavný
mikrobiologa	mikrobiolog	k1gMnSc2	mikrobiolog
L.	L.	kA	L.
Pasteura	Pasteur	k1gMnSc2	Pasteur
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
živé	živý	k2eAgFnPc1d1	živá
čerstvé	čerstvý	k2eAgFnPc1d1	čerstvá
kvasinkové	kvasinkový	k2eAgFnPc1d1	kvasinková
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
kvasit	kvasit	k5eAaImF	kvasit
cukry	cukr	k1gInPc4	cukr
na	na	k7c4	na
alkohol	alkohol	k1gInSc4	alkohol
a	a	k8xC	a
že	že	k8xS	že
tedy	tedy	k9	tedy
tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nějakým	nějaký	k3yIgInSc7	nějaký
výlučným	výlučný	k2eAgInSc7d1	výlučný
rysem	rys	k1gInSc7	rys
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
biokatalyzátorům	biokatalyzátor	k1gInPc3	biokatalyzátor
neříkalo	říkat	k5eNaImAgNnS	říkat
enzymy	enzym	k1gInPc4	enzym
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
fermenty	ferment	k1gInPc1	ferment
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byly	být	k5eAaImAgFnP	být
činěny	činit	k5eAaImNgInP	činit
zodpovědnými	zodpovědný	k2eAgInPc7d1	zodpovědný
za	za	k7c4	za
rozkladné	rozkladný	k2eAgInPc4d1	rozkladný
fermentační	fermentační	k2eAgInPc4d1	fermentační
procesy	proces	k1gInPc4	proces
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
německý	německý	k2eAgMnSc1d1	německý
fyziolog	fyziolog	k1gMnSc1	fyziolog
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Kühne	Kühn	k1gInSc5	Kühn
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
enzym	enzym	k1gInSc4	enzym
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ε	ε	k?	ε
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
v	v	k7c6	v
kvasinkách	kvasinka	k1gFnPc6	kvasinka
(	(	kIx(	(
<g/>
v	v	k7c6	v
kvásku	kvásek	k1gInSc6	kvásek
<g/>
,	,	kIx,	,
v	v	k7c6	v
droždí	droždí	k1gNnSc6	droždí
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
moderním	moderní	k2eAgInSc6d1	moderní
smyslu	smysl	k1gInSc6	smysl
používáno	používat	k5eAaImNgNnS	používat
až	až	k6eAd1	až
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
Eduard	Eduard	k1gMnSc1	Eduard
Buchner	Buchner	k1gMnSc1	Buchner
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
kvašení	kvašení	k1gNnSc4	kvašení
cukru	cukr	k1gInSc2	cukr
stačí	stačit	k5eAaBmIp3nS	stačit
jen	jen	k9	jen
kvasinkový	kvasinkový	k2eAgInSc1d1	kvasinkový
extrakt	extrakt	k1gInSc1	extrakt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
toto	tento	k3xDgNnSc4	tento
kvašení	kvašení	k1gNnSc4	kvašení
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
(	(	kIx(	(
<g/>
on	on	k3xPp3gMnSc1	on
tuto	tento	k3xDgFnSc4	tento
látku	látka	k1gFnSc4	látka
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zymáza	zymáza	k1gFnSc1	zymáza
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
výzkumy	výzkum	k1gInPc4	výzkum
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
enzymy	enzym	k1gInPc1	enzym
pojmenovávány	pojmenováván	k2eAgInPc1d1	pojmenováván
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
reakci	reakce	k1gFnSc4	reakce
katalyzují	katalyzovat	k5eAaBmIp3nP	katalyzovat
<g/>
,	,	kIx,	,
přidáním	přidání	k1gNnSc7	přidání
koncovky	koncovka	k1gFnSc2	koncovka
–	–	k?	–
<g/>
áza	áza	k?	áza
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
–	–	k?	–
<g/>
asa	asa	k?	asa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úzus	úzus	k1gInSc1	úzus
zřejmě	zřejmě	k6eAd1	zřejmě
zavedl	zavést	k5eAaPmAgInS	zavést
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vědec	vědec	k1gMnSc1	vědec
Émile	Émile	k1gFnSc2	Émile
Duclaux	Duclaux	k1gInSc1	Duclaux
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tak	tak	k6eAd1	tak
chtěl	chtít	k5eAaImAgMnS	chtít
oslavit	oslavit	k5eAaPmF	oslavit
objevitele	objevitel	k1gMnSc4	objevitel
diastázy	diastáza	k1gFnSc2	diastáza
–	–	k?	–
prvního	první	k4xOgInSc2	první
izolovaného	izolovaný	k2eAgInSc2d1	izolovaný
enzymu	enzym	k1gInSc2	enzym
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
cílem	cíl	k1gInSc7	cíl
výzkumníků	výzkumník	k1gMnPc2	výzkumník
bylo	být	k5eAaImAgNnS	být
určit	určit	k5eAaPmF	určit
biochemickou	biochemický	k2eAgFnSc4d1	biochemická
povahu	povaha	k1gFnSc4	povaha
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
vědců	vědec	k1gMnPc2	vědec
sice	sice	k8xC	sice
poukazovalo	poukazovat	k5eAaImAgNnS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzymatická	enzymatický	k2eAgFnSc1d1	enzymatická
aktivita	aktivita	k1gFnSc1	aktivita
má	mít	k5eAaImIp3nS	mít
něco	něco	k3yInSc1	něco
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
proteiny	protein	k1gInPc7	protein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
vlivný	vlivný	k2eAgMnSc1d1	vlivný
chemik	chemik	k1gMnSc1	chemik
Richard	Richard	k1gMnSc1	Richard
Willstätter	Willstätter	k1gMnSc1	Willstätter
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bílkoviny	bílkovina	k1gFnPc1	bílkovina
jsou	být	k5eAaImIp3nP	být
pouhé	pouhý	k2eAgInPc4d1	pouhý
přenašeče	přenašeč	k1gInPc4	přenašeč
skutečných	skutečný	k2eAgInPc2d1	skutečný
enzymů	enzym	k1gInPc2	enzym
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
schopné	schopný	k2eAgFnSc2d1	schopná
katalyzovat	katalyzovat	k5eAaBmF	katalyzovat
chemické	chemický	k2eAgFnPc4d1	chemická
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Batcheller	Batcheller	k1gMnSc1	Batcheller
Sumner	Sumner	k1gMnSc1	Sumner
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
izoloval	izolovat	k5eAaBmAgInS	izolovat
a	a	k8xC	a
krystalizoval	krystalizovat	k5eAaImAgInS	krystalizovat
enzym	enzym	k1gInSc1	enzym
ureázu	ureáza	k1gFnSc4	ureáza
a	a	k8xC	a
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
čistý	čistý	k2eAgInSc4d1	čistý
protein	protein	k1gInSc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
proteiny	protein	k1gInPc1	protein
mohou	moct	k5eAaImIp3nP	moct
samy	sám	k3xTgInPc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
plnit	plnit	k5eAaImF	plnit
funkci	funkce	k1gFnSc4	funkce
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
podali	podat	k5eAaPmAgMnP	podat
Northrop	Northrop	k1gInSc4	Northrop
a	a	k8xC	a
Stanley	Stanle	k1gMnPc4	Stanle
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
trávicí	trávicí	k2eAgInPc4d1	trávicí
enzymy	enzym	k1gInPc4	enzym
pepsin	pepsin	k1gInSc1	pepsin
<g/>
,	,	kIx,	,
trypsin	trypsin	k1gInSc1	trypsin
a	a	k8xC	a
chymotrypsin	chymotrypsin	k1gInSc1	chymotrypsin
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Sumnerem	Sumner	k1gMnSc7	Sumner
jim	on	k3xPp3gInPc3	on
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Sumnerovu	Sumnerův	k2eAgInSc3d1	Sumnerův
objevu	objev	k1gInSc3	objev
krystalizace	krystalizace	k1gFnSc2	krystalizace
proteinů	protein	k1gInPc2	protein
se	se	k3xPyFc4	se
otevřel	otevřít	k5eAaPmAgInS	otevřít
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
důležitou	důležitý	k2eAgFnSc4d1	důležitá
metodu	metoda	k1gFnSc4	metoda
tzv.	tzv.	kA	tzv.
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
krystalografie	krystalografie	k1gFnSc2	krystalografie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
určit	určit	k5eAaPmF	určit
prostorovou	prostorový	k2eAgFnSc4d1	prostorová
strukturu	struktura	k1gFnSc4	struktura
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
uspěla	uspět	k5eAaPmAgFnS	uspět
skupina	skupina	k1gFnSc1	skupina
vedená	vedený	k2eAgFnSc1d1	vedená
D.	D.	kA	D.
C.	C.	kA	C.
Phillipsem	Phillips	k1gMnSc7	Phillips
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
prostorovou	prostorový	k2eAgFnSc4d1	prostorová
strukturu	struktura	k1gFnSc4	struktura
enzymu	enzym	k1gInSc2	enzym
lysozymu	lysozym	k1gInSc2	lysozym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
většiny	většina	k1gFnSc2	většina
enzymů	enzym	k1gInPc2	enzym
je	být	k5eAaImIp3nS	být
proteinová	proteinový	k2eAgFnSc1d1	proteinová
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
sekvence	sekvence	k1gFnPc4	sekvence
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
prostorový	prostorový	k2eAgInSc1d1	prostorový
útvar	útvar	k1gInSc1	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
místo	místo	k7c2	místo
bílkovin	bílkovina	k1gFnPc2	bílkovina
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
RNA	RNA	kA	RNA
–	–	k?	–
těmto	tento	k3xDgNnPc3	tento
RNA	RNA	kA	RNA
enzymům	enzym	k1gInPc3	enzym
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
také	také	k9	také
ribozymy	ribozym	k1gInPc4	ribozym
a	a	k8xC	a
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
například	například	k6eAd1	například
rRNA	rRNA	k?	rRNA
v	v	k7c6	v
ribozomu	ribozom	k1gInSc6	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Menšinovou	menšinový	k2eAgFnSc7d1	menšinová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
významnější	významný	k2eAgFnSc2d2	významnější
složkou	složka	k1gFnSc7	složka
enzymů	enzym	k1gInPc2	enzym
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
prostetické	prostetický	k2eAgFnPc4d1	prostetická
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
váží	vážit	k5eAaImIp3nS	vážit
na	na	k7c4	na
enzym	enzym	k1gInSc4	enzym
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zpravidla	zpravidla	k6eAd1	zpravidla
jeho	jeho	k3xOp3gFnSc4	jeho
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
bílkovinná	bílkovinný	k2eAgFnSc1d1	bílkovinná
složka	složka	k1gFnSc1	složka
enzymu	enzym	k1gInSc2	enzym
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
apoenzym	apoenzym	k1gInSc1	apoenzym
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prostetickou	prostetický	k2eAgFnSc7d1	prostetická
skupinou	skupina	k1gFnSc7	skupina
tvoří	tvořit	k5eAaImIp3nS	tvořit
výsledný	výsledný	k2eAgInSc1d1	výsledný
a	a	k8xC	a
aktivní	aktivní	k2eAgInSc1d1	aktivní
holoenzym	holoenzym	k1gInSc1	holoenzym
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
prostetické	prostetický	k2eAgFnPc4d1	prostetická
skupiny	skupina	k1gFnPc4	skupina
se	se	k3xPyFc4	se
enzymové	enzymový	k2eAgFnPc1d1	enzymová
katalýzy	katalýza	k1gFnPc1	katalýza
účastní	účastnit	k5eAaImIp3nP	účastnit
i	i	k9	i
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
volněji	volně	k6eAd2	volně
navázané	navázaný	k2eAgFnPc1d1	navázaná
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
obvykle	obvykle	k6eAd1	obvykle
jako	jako	k9	jako
koenzymy	koenzym	k1gInPc1	koenzym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
terminologie	terminologie	k1gFnSc1	terminologie
okolo	okolo	k7c2	okolo
kofaktorů	kofaktor	k1gInPc2	kofaktor
<g/>
,	,	kIx,	,
prostetických	prostetický	k2eAgFnPc2d1	prostetická
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
koenzymů	koenzym	k1gInPc2	koenzym
<g/>
,	,	kIx,	,
panuje	panovat	k5eAaImIp3nS	panovat
značná	značný	k2eAgFnSc1d1	značná
nejednotnost	nejednotnost	k1gFnSc1	nejednotnost
<g/>
.	.	kIx.	.
</s>
<s>
Voet	Voet	k1gInSc1	Voet
&	&	k?	&
Voet	Voeta	k1gFnPc2	Voeta
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
kofaktorů	kofaktor	k1gInPc2	kofaktor
<g/>
,	,	kIx,	,
koenzymy	koenzym	k1gInPc1	koenzym
a	a	k8xC	a
prostetické	prostetický	k2eAgFnPc1d1	prostetická
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
první	první	k4xOgFnPc1	první
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
slabě	slabě	k6eAd1	slabě
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
kovalentně	kovalentně	k6eAd1	kovalentně
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc4d1	stejné
pojetí	pojetí	k1gNnSc4	pojetí
zastává	zastávat	k5eAaImIp3nS	zastávat
například	například	k6eAd1	například
Vodrážkova	Vodrážkův	k2eAgFnSc1d1	Vodrážkova
Enzymologie	Enzymologie	k1gFnSc1	Enzymologie
nebo	nebo	k8xC	nebo
Oxford	Oxford	k1gInSc1	Oxford
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Biochemistry	Biochemistr	k1gMnPc4	Biochemistr
and	and	k?	and
Molecular	Moleculara	k1gFnPc2	Moleculara
Biology	biolog	k1gMnPc4	biolog
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
jiné	jiný	k2eAgFnPc4d1	jiná
definice	definice	k1gFnPc4	definice
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
Alberts	Alberts	k1gInSc1	Alberts
nebo	nebo	k8xC	nebo
Harper	Harper	k1gInSc1	Harper
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
kofaktory	kofaktor	k1gMnPc7	kofaktor
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
neřadí	řadit	k5eNaImIp3nS	řadit
další	další	k2eAgFnSc3d1	další
nebílkovinné	bílkovinný	k2eNgFnSc3d1	nebílkovinná
součási	součáse	k1gFnSc3	součáse
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
různé	různý	k2eAgFnPc1d1	různá
cukerné	cukerný	k2eAgFnPc1d1	cukerná
složky	složka	k1gFnPc1	složka
nebo	nebo	k8xC	nebo
ionty	ion	k1gInPc1	ion
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
mnohé	mnohý	k2eAgInPc1d1	mnohý
enzymy	enzym	k1gInPc1	enzym
jsou	být	k5eAaImIp3nP	být
glykoproteiny	glykoprotein	k2eAgInPc1d1	glykoprotein
nebo	nebo	k8xC	nebo
metaloproteiny	metaloprotein	k2eAgInPc1d1	metaloprotein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgInPc3d1	známý
metaloenzymům	metaloenzym	k1gInPc3	metaloenzym
patří	patřit	k5eAaImIp3nS	patřit
metaloproteázy	metaloproteáza	k1gFnSc2	metaloproteáza
<g/>
,	,	kIx,	,
alkoholdehydrogenáza	alkoholdehydrogenáza	k1gFnSc1	alkoholdehydrogenáza
či	či	k8xC	či
karbonáthydrolyáza	karbonáthydrolyáza	k1gFnSc1	karbonáthydrolyáza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bílkovinná	bílkovinný	k2eAgFnSc1d1	bílkovinná
složka	složka	k1gFnSc1	složka
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
molekul	molekula	k1gFnPc2	molekula
holoenzymů	holoenzym	k1gMnPc2	holoenzym
jsou	být	k5eAaImIp3nP	být
bílkoviny	bílkovina	k1gFnPc4	bílkovina
(	(	kIx(	(
<g/>
lineární	lineární	k2eAgInPc4d1	lineární
polypeptidy	polypeptid	k1gInPc4	polypeptid
<g/>
)	)	kIx)	)
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
20	[number]	k4	20
základních	základní	k2eAgFnPc6d1	základní
proteinogenních	proteinogenní	k2eAgFnPc6d1	proteinogenní
α	α	k?	α
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
těch	ten	k3xDgFnPc2	ten
ostatních	ostatní	k2eAgFnPc2d1	ostatní
(	(	kIx(	(
<g/>
ty	ty	k3xPp2nSc1	ty
však	však	k9	však
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
enzymy	enzym	k1gInPc4	enzym
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fenylalanin	Fenylalanina	k1gFnPc2	Fenylalanina
<g/>
,	,	kIx,	,
tryptofan	tryptofana	k1gFnPc2	tryptofana
a	a	k8xC	a
nepolární	polární	k2eNgFnPc1d1	nepolární
alifatické	alifatický	k2eAgFnPc1d1	alifatický
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
se	se	k3xPyFc4	se
například	například	k6eAd1	například
obvykle	obvykle	k6eAd1	obvykle
nachází	nacházet	k5eAaImIp3nS	nacházet
uvnitř	uvnitř	k7c2	uvnitř
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Tyrosin	Tyrosin	k1gInSc1	Tyrosin
nebo	nebo	k8xC	nebo
histidin	histidin	k1gInSc1	histidin
bývají	bývat	k5eAaImIp3nP	bývat
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
přítomny	přítomen	k2eAgInPc1d1	přítomen
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
aktivním	aktivní	k2eAgNnSc6d1	aktivní
centru	centrum	k1gNnSc6	centrum
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
díky	dík	k1gInPc7	dík
své	svůj	k3xOyFgFnSc2	svůj
schopnosti	schopnost	k1gFnSc2	schopnost
tvořit	tvořit	k5eAaImF	tvořit
vodíkové	vodíkový	k2eAgInPc4d1	vodíkový
můstky	můstek	k1gInPc4	můstek
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
kvůli	kvůli	k7c3	kvůli
schopnosti	schopnost	k1gFnSc3	schopnost
přijímat	přijímat	k5eAaImF	přijímat
protony	proton	k1gInPc4	proton
a	a	k8xC	a
svým	svůj	k3xOyFgFnPc3	svůj
nukleofilním	nukleofilní	k2eAgFnPc3d1	nukleofilní
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
.	.	kIx.	.
<g/>
Délkou	délka	k1gFnSc7	délka
sahají	sahat	k5eAaImIp3nP	sahat
od	od	k7c2	od
pouhých	pouhý	k2eAgFnPc2d1	pouhá
62	[number]	k4	62
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
v	v	k7c6	v
případě	případ	k1gInSc6	případ
4	[number]	k4	4
<g/>
-oxalokrotonáttautomerázy	xalokrotonáttautomeráza	k1gFnSc2	-oxalokrotonáttautomeráza
po	po	k7c4	po
2500	[number]	k4	2500
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
komplex	komplex	k1gInSc4	komplex
syntázy	syntáza	k1gFnSc2	syntáza
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
enzymu	enzym	k1gInSc2	enzym
podílí	podílet	k5eAaImIp3nS	podílet
několik	několik	k4yIc1	několik
samostatných	samostatný	k2eAgFnPc2d1	samostatná
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
proteinový	proteinový	k2eAgInSc4d1	proteinový
komplex	komplex	k1gInSc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
určitých	určitý	k2eAgFnPc2d1	určitá
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
rozruší	rozrušit	k5eAaPmIp3nS	rozrušit
prostorová	prostorový	k2eAgFnSc1d1	prostorová
struktura	struktura	k1gFnSc1	struktura
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
denaturaci	denaturace	k1gFnSc6	denaturace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
enzym	enzym	k1gInSc4	enzym
vratně	vratně	k6eAd1	vratně
nebo	nebo	k8xC	nebo
nevratně	vratně	k6eNd1	vratně
přestane	přestat	k5eAaPmIp3nS	přestat
být	být	k5eAaImF	být
funkční	funkční	k2eAgMnSc1d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostorový	prostorový	k2eAgInSc1d1	prostorový
tvar	tvar	k1gInSc1	tvar
enzymů	enzym	k1gInPc2	enzym
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgInSc1d1	zásadní
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
funkčnost	funkčnost	k1gFnSc4	funkčnost
–	–	k?	–
bohužel	bohužel	k6eAd1	bohužel
však	však	k9	však
stále	stále	k6eAd1	stále
neumíme	umět	k5eNaImIp1nP	umět
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
molekuly	molekula	k1gFnSc2	molekula
spolehlivě	spolehlivě	k6eAd1	spolehlivě
předpovědět	předpovědět	k5eAaPmF	předpovědět
druh	druh	k1gInSc4	druh
enzymatické	enzymatický	k2eAgFnSc2d1	enzymatická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
enzymů	enzym	k1gInPc2	enzym
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
přeměnu	přeměna	k1gFnSc4	přeměna
katalyzují	katalyzovat	k5eAaBmIp3nP	katalyzovat
a	a	k8xC	a
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
enzymatické	enzymatický	k2eAgFnSc6d1	enzymatická
aktivitě	aktivita	k1gFnSc6	aktivita
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
jen	jen	k9	jen
např.	např.	kA	např.
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
společně	společně	k6eAd1	společně
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tzv.	tzv.	kA	tzv.
aktivní	aktivní	k2eAgInSc4d1	aktivní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
důležitými	důležitý	k2eAgFnPc7d1	důležitá
oblastmi	oblast	k1gFnPc7	oblast
v	v	k7c6	v
enzymu	enzym	k1gInSc6	enzym
jsou	být	k5eAaImIp3nP	být
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
kofaktory	kofaktor	k1gInPc4	kofaktor
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc4d1	jiná
malé	malý	k2eAgFnPc4d1	malá
molekuly	molekula	k1gFnPc4	molekula
potřebné	potřebný	k2eAgFnPc4d1	potřebná
pro	pro	k7c4	pro
katalýzu	katalýza	k1gFnSc4	katalýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kofaktory	Kofaktor	k1gInPc4	Kofaktor
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
enzymy	enzym	k1gInPc1	enzym
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
funkci	funkce	k1gFnSc3	funkce
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
nebílkovinné	bílkovinný	k2eNgFnPc1d1	nebílkovinná
molekuly	molekula	k1gFnPc1	molekula
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
říká	říkat	k5eAaImIp3nS	říkat
kofaktory	kofaktor	k1gInPc4	kofaktor
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgInPc2	takový
enzymů	enzym	k1gInPc2	enzym
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
asi	asi	k9	asi
60	[number]	k4	60
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kofaktory	Kofaktor	k1gInPc1	Kofaktor
obecně	obecně	k6eAd1	obecně
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přenos	přenos	k1gInSc4	přenos
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
atomů	atom	k1gInPc2	atom
nebo	nebo	k8xC	nebo
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
enzymatické	enzymatický	k2eAgFnSc2d1	enzymatická
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Vymezení	vymezení	k1gNnSc1	vymezení
a	a	k8xC	a
definice	definice	k1gFnSc1	definice
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
kofaktorů	kofaktor	k1gInPc2	kofaktor
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
autor	autor	k1gMnSc1	autor
od	od	k7c2	od
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
prostetická	prostetický	k2eAgFnSc1d1	prostetická
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xS	jako
pevně	pevně	k6eAd1	pevně
vázaná	vázaný	k2eAgFnSc1d1	vázaná
a	a	k8xC	a
stabilní	stabilní	k2eAgFnSc1d1	stabilní
součást	součást	k1gFnSc1	součást
molekul	molekula	k1gFnPc2	molekula
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
koenzym	koenzym	k1gInSc1	koenzym
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
pouze	pouze	k6eAd1	pouze
slabě	slabě	k6eAd1	slabě
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
disociuje	disociovat	k5eAaBmIp3nS	disociovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
obvykle	obvykle	k6eAd1	obvykle
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koenzymy	koenzym	k1gInPc1	koenzym
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
proběhnutí	proběhnutí	k1gNnSc6	proběhnutí
reakce	reakce	k1gFnSc2	reakce
"	"	kIx"	"
<g/>
spotřebovávají	spotřebovávat	k5eAaImIp3nP	spotřebovávat
<g/>
"	"	kIx"	"
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
jiným	jiný	k2eAgInSc7d1	jiný
enzymem	enzym	k1gInSc7	enzym
opět	opět	k6eAd1	opět
regenerovány	regenerován	k2eAgFnPc4d1	regenerována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
opět	opět	k6eAd1	opět
plnit	plnit	k5eAaImF	plnit
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reálné	reálný	k2eAgFnSc6d1	reálná
biochemii	biochemie	k1gFnSc6	biochemie
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
plynulý	plynulý	k2eAgInSc1d1	plynulý
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
koenzymy	koenzym	k1gInPc7	koenzym
a	a	k8xC	a
prostetickými	prostetický	k2eAgFnPc7d1	prostetická
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
chemické	chemický	k2eAgFnSc2d1	chemická
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
všechny	všechen	k3xTgInPc4	všechen
kofaktory	kofaktor	k1gInPc4	kofaktor
praktičtěji	prakticky	k6eAd2	prakticky
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kofaktory	Kofaktor	k1gInPc4	Kofaktor
účastnící	účastnící	k2eAgInPc4d1	účastnící
se	se	k3xPyFc4	se
oxidačních	oxidační	k2eAgFnPc2d1	oxidační
a	a	k8xC	a
redukčních	redukční	k2eAgInPc2d1	redukční
pochodů	pochod	k1gInPc2	pochod
(	(	kIx(	(
<g/>
kofaktory	kofaktor	k1gInPc1	kofaktor
oxidoreduktáz	oxidoreduktáza	k1gFnPc2	oxidoreduktáza
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
různé	různý	k2eAgFnPc1d1	různá
pyridinové	pyridinový	k2eAgFnPc1d1	pyridinový
(	(	kIx(	(
<g/>
nikotinamidové	nikotinamidový	k2eAgFnPc1d1	nikotinamidový
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
di	di	k?	di
<g/>
)	)	kIx)	)
<g/>
nukleotidy	nukleotid	k1gInPc1	nukleotid
(	(	kIx(	(
<g/>
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
a	a	k8xC	a
NADP	NADP	kA	NADP
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
flavinové	flavinové	k2eAgInPc7d1	flavinové
"	"	kIx"	"
<g/>
nukleotidy	nukleotid	k1gInPc7	nukleotid
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
FMN	FMN	kA	FMN
a	a	k8xC	a
FAD	FAD	kA	FAD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biopterin	biopterin	k1gInSc1	biopterin
<g/>
,	,	kIx,	,
lipoová	lipoový	k2eAgFnSc1d1	lipoová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
benzochinony	benzochinon	k1gInPc1	benzochinon
(	(	kIx(	(
<g/>
CoQ	CoQ	k1gMnSc1	CoQ
a	a	k8xC	a
plastochinon	plastochinon	k1gMnSc1	plastochinon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hem	hem	k1gInSc1	hem
<g/>
,	,	kIx,	,
FeS	fes	k1gNnSc1	fes
centra	centrum	k1gNnSc2	centrum
či	či	k8xC	či
glutathion	glutathion	k1gInSc1	glutathion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kofaktory	Kofaktor	k1gInPc4	Kofaktor
umožňující	umožňující	k2eAgInSc1d1	umožňující
přenos	přenos	k1gInSc1	přenos
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
kofaktory	kofaktor	k1gInPc1	kofaktor
transferáz	transferáza	k1gFnPc2	transferáza
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
známý	známý	k2eAgMnSc1d1	známý
ATP	atp	kA	atp
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgInSc1d1	aktivní
sulfát	sulfát	k1gInSc1	sulfát
(	(	kIx(	(
<g/>
PAPS	PAPS	kA	PAPS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
adenosylmethionin	adenosylmethionin	k1gInSc1	adenosylmethionin
a	a	k8xC	a
methylkobalamin	methylkobalamin	k1gInSc1	methylkobalamin
<g/>
,	,	kIx,	,
tetrahydrofolát	tetrahydrofolát	k1gInSc1	tetrahydrofolát
<g/>
,	,	kIx,	,
biotin	biotin	k1gInSc1	biotin
<g/>
,	,	kIx,	,
thiamindifosfát	thiamindifosfát	k1gInSc1	thiamindifosfát
<g/>
,	,	kIx,	,
koenzym	koenzym	k1gInSc1	koenzym
A	A	kA	A
<g/>
,	,	kIx,	,
pyridoxalfosfát	pyridoxalfosfát	k1gMnSc1	pyridoxalfosfát
<g/>
,	,	kIx,	,
UDP	UDP	kA	UDP
a	a	k8xC	a
CDP	CDP	kA	CDP
</s>
</p>
<p>
<s>
Kofaktory	Kofaktor	k1gInPc1	Kofaktor
lyáz	lyáza	k1gFnPc2	lyáza
<g/>
:	:	kIx,	:
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgMnPc4d1	zmíněný
kofaktory	kofaktor	k1gMnPc4	kofaktor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
acetylkoenzym	acetylkoenzym	k1gInSc1	acetylkoenzym
A	A	kA	A
<g/>
,	,	kIx,	,
biotin	biotin	k1gInSc1	biotin
<g/>
,	,	kIx,	,
thiamindifosfát	thiamindifosfát	k1gInSc1	thiamindifosfát
či	či	k8xC	či
pyridoxalfosfát	pyridoxalfosfát	k1gInSc1	pyridoxalfosfát
</s>
</p>
<p>
<s>
Kofaktory	Kofaktor	k1gInPc1	Kofaktor
izomeráz	izomeráza	k1gFnPc2	izomeráza
<g/>
:	:	kIx,	:
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
izomerázy	izomeráza	k1gFnPc1	izomeráza
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
obvykle	obvykle	k6eAd1	obvykle
obejdou	obejít	k5eAaPmIp3nP	obejít
<g/>
;	;	kIx,	;
nicméně	nicméně	k8xC	nicméně
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
glutathion	glutathion	k1gInSc1	glutathion
nebo	nebo	k8xC	nebo
různé	různý	k2eAgInPc1d1	různý
deriváty	derivát	k1gInPc1	derivát
kobalaminu	kobalamin	k1gInSc2	kobalamin
</s>
</p>
<p>
<s>
===	===	k?	===
Důležité	důležitý	k2eAgFnPc1d1	důležitá
oblasti	oblast	k1gFnPc1	oblast
===	===	k?	===
</s>
</p>
<p>
<s>
Tvar	tvar	k1gInSc1	tvar
enzymu	enzym	k1gInSc2	enzym
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
tzv.	tzv.	kA	tzv.
aktivní	aktivní	k2eAgNnSc1d1	aktivní
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
štěrbina	štěrbina	k1gFnSc1	štěrbina
nebo	nebo	k8xC	nebo
prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
substrát	substrát	k1gInSc1	substrát
dostatečně	dostatečně	k6eAd1	dostatečně
uchráněn	uchránit	k5eAaPmNgInS	uchránit
před	před	k7c7	před
okolním	okolní	k2eAgNnSc7d1	okolní
vodným	vodný	k2eAgNnSc7d1	vodné
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
zde	zde	k6eAd1	zde
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
katalýze	katalýza	k1gFnSc3	katalýza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
multimerických	multimerický	k2eAgInPc2d1	multimerický
enzymů	enzym	k1gInPc2	enzym
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
s	s	k7c7	s
více	hodně	k6eAd2	hodně
podjednotkami	podjednotka	k1gFnPc7	podjednotka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgNnSc1d1	aktivní
místo	místo	k1gNnSc1	místo
mnohdy	mnohdy	k6eAd1	mnohdy
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
podjednotkami	podjednotka	k1gFnPc7	podjednotka
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
podílí	podílet	k5eAaImIp3nS	podílet
aminokyselinové	aminokyselinový	k2eAgInPc4d1	aminokyselinový
zbytky	zbytek	k1gInPc4	zbytek
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aktivním	aktivní	k2eAgNnSc6d1	aktivní
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nachází	nacházet	k5eAaImIp3nS	nacházet
různé	různý	k2eAgInPc4d1	různý
kofaktory	kofaktor	k1gInPc4	kofaktor
a	a	k8xC	a
prostetické	prostetický	k2eAgFnPc4d1	prostetická
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgNnSc7d1	jiné
důležitým	důležitý	k2eAgNnSc7d1	důležité
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
<g/>
)	)	kIx)	)
enzymů	enzym	k1gInPc2	enzym
tzv.	tzv.	kA	tzv.
alosterické	alosterický	k2eAgNnSc1d1	alosterický
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
zejména	zejména	k9	zejména
u	u	k7c2	u
kaskád	kaskáda	k1gFnPc2	kaskáda
několika	několik	k4yIc2	několik
enzymatických	enzymatický	k2eAgFnPc2d1	enzymatická
reakcí	reakce	k1gFnPc2	reakce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
prostředkem	prostředek	k1gInSc7	prostředek
tzv.	tzv.	kA	tzv.
alosterické	alosterický	k2eAgFnSc2d1	alosterický
regulace	regulace	k1gFnSc2	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Vážou	vázat	k5eAaImIp3nP	vázat
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
jisté	jistý	k2eAgFnPc1d1	jistá
molekuly	molekula	k1gFnPc1	molekula
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k9	ale
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
enzymatické	enzymatický	k2eAgFnSc3d1	enzymatická
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
biochemická	biochemický	k2eAgFnSc1d1	biochemická
a	a	k8xC	a
molekulárně	molekulárně	k6eAd1	molekulárně
biologická	biologický	k2eAgFnSc1d1	biologická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
IUBMB	IUBMB	kA	IUBMB
<g/>
)	)	kIx)	)
zavedla	zavést	k5eAaPmAgFnS	zavést
nomenklaturické	nomenklaturický	k2eAgNnSc4d1	nomenklaturický
rozdělení	rozdělení	k1gNnSc4	rozdělení
enzymů	enzym	k1gInPc2	enzym
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
EC	EC	kA	EC
čísel	číslo	k1gNnPc2	číslo
do	do	k7c2	do
6	[number]	k4	6
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
podkategorie	podkategorie	k1gFnPc4	podkategorie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
můžeme	moct	k5eAaImIp1nP	moct
vedle	vedle	k7c2	vedle
jména	jméno	k1gNnSc2	jméno
enzymu	enzym	k1gInSc2	enzym
setkat	setkat	k5eAaPmF	setkat
ještě	ještě	k9	ještě
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
číselným	číselný	k2eAgNnSc7d1	číselné
označením	označení	k1gNnSc7	označení
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
např.	např.	kA	např.
EC	EC	kA	EC
3.4.11.4	[number]	k4	3.4.11.4
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
šest	šest	k4xCc4	šest
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kategorií	kategorie	k1gFnPc2	kategorie
enzymů	enzym	k1gInPc2	enzym
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
EC	EC	kA	EC
1	[number]	k4	1
–	–	k?	–
oxidoreduktázy	oxidoreduktáza	k1gFnSc2	oxidoreduktáza
<g/>
:	:	kIx,	:
katalyzují	katalyzovat	k5eAaPmIp3nP	katalyzovat
oxidačně	oxidačně	k6eAd1	oxidačně
<g/>
/	/	kIx~	/
<g/>
redukční	redukční	k2eAgFnSc1d1	redukční
reakce	reakce	k1gFnSc1	reakce
</s>
</p>
<p>
<s>
EC	EC	kA	EC
2	[number]	k4	2
–	–	k?	–
transferázy	transferáza	k1gFnSc2	transferáza
<g/>
:	:	kIx,	:
přenášejí	přenášet	k5eAaImIp3nP	přenášet
funkční	funkční	k2eAgFnPc1d1	funkční
skupiny	skupina	k1gFnPc1	skupina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
methyl-	methyl-	k?	methyl-
<g/>
,	,	kIx,	,
acetyl-	acetyl-	k?	acetyl-
nebo	nebo	k8xC	nebo
fosfátovou	fosfátový	k2eAgFnSc4d1	fosfátová
skupinu	skupina	k1gFnSc4	skupina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EC	EC	kA	EC
3	[number]	k4	3
–	–	k?	–
hydrolázy	hydroláza	k1gFnSc2	hydroláza
<g/>
:	:	kIx,	:
katalyzují	katalyzovat	k5eAaImIp3nP	katalyzovat
hydrolýzu	hydrolýza	k1gFnSc4	hydrolýza
chemických	chemický	k2eAgFnPc2d1	chemická
vazeb	vazba	k1gFnPc2	vazba
</s>
</p>
<p>
<s>
EC	EC	kA	EC
4	[number]	k4	4
–	–	k?	–
lyázy	lyáza	k1gFnSc2	lyáza
<g/>
:	:	kIx,	:
štěpí	štěpit	k5eAaImIp3nS	štěpit
chemické	chemický	k2eAgFnSc2d1	chemická
vazby	vazba	k1gFnSc2	vazba
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
než	než	k8xS	než
hydrolýzou	hydrolýza	k1gFnSc7	hydrolýza
či	či	k8xC	či
redoxní	redoxní	k2eAgFnSc7d1	redoxní
reakcí	reakce	k1gFnSc7	reakce
</s>
</p>
<p>
<s>
EC	EC	kA	EC
5	[number]	k4	5
–	–	k?	–
izomerázy	izomeráza	k1gFnSc2	izomeráza
<g/>
:	:	kIx,	:
katalyzují	katalyzovat	k5eAaPmIp3nP	katalyzovat
isomerizační	isomerizační	k2eAgFnPc1d1	isomerizační
reakce	reakce	k1gFnPc1	reakce
</s>
</p>
<p>
<s>
EC	EC	kA	EC
6	[number]	k4	6
–	–	k?	–
ligázy	ligáza	k1gFnSc2	ligáza
<g/>
:	:	kIx,	:
spojují	spojovat	k5eAaImIp3nP	spojovat
dvě	dva	k4xCgFnPc4	dva
molekuly	molekula	k1gFnPc4	molekula
kovalentní	kovalentní	k2eAgMnSc1d1	kovalentní
vazbouTzv	vazbouTzv	k1gMnSc1	vazbouTzv
<g/>
.	.	kIx.	.
systémové	systémový	k2eAgInPc1d1	systémový
názvy	název	k1gInPc1	název
enzymů	enzym	k1gInPc2	enzym
jsou	být	k5eAaImIp3nP	být
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
systematické	systematický	k2eAgNnSc4d1	systematické
pojmenování	pojmenování	k1gNnSc4	pojmenování
enzymů	enzym	k1gInPc2	enzym
skutečným	skutečný	k2eAgInSc7d1	skutečný
popisem	popis	k1gInSc7	popis
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
katalyzují	katalyzovat	k5eAaBmIp3nP	katalyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
-laktát	aktát	k1gInSc1	-laktát
<g/>
:	:	kIx,	:
<g/>
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
<g/>
-oxidoreduktáza	xidoreduktáza	k1gFnSc1	-oxidoreduktáza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
katalyzuje	katalyzovat	k5eAaImIp3nS	katalyzovat
oxidoredukční	oxidoredukční	k2eAgFnSc4d1	oxidoredukční
reakci	reakce	k1gFnSc4	reakce
substrátu	substrát	k1gInSc2	substrát
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
-laktát	aktát	k1gInSc1	-laktát
se	s	k7c7	s
substrátem	substrát	k1gInSc7	substrát
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
kofaktorem	kofaktor	k1gMnSc7	kofaktor
<g/>
)	)	kIx)	)
NAD	NAD	kA	NAD
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
EC	EC	kA	EC
systému	systém	k1gInSc2	systém
by	by	kYmCp3nS	by
její	její	k3xOp3gInSc4	její
číselný	číselný	k2eAgInSc4d1	číselný
kód	kód	k1gInSc4	kód
zněl	znět	k5eAaImAgInS	znět
EC	EC	kA	EC
1.1.1.27	[number]	k4	1.1.1.27
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
oxidoreduktáz	oxidoreduktáza	k1gFnPc2	oxidoreduktáza
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
na	na	k7c4	na
CH-OH	CH-OH	k1gFnSc4	CH-OH
skupinu	skupina	k1gFnSc4	skupina
(	(	kIx(	(
<g/>
1.1	[number]	k4	1.1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
akceptor	akceptor	k1gInSc1	akceptor
vodíku	vodík	k1gInSc2	vodík
protonů	proton	k1gInPc2	proton
využívá	využívat	k5eAaPmIp3nS	využívat
NAD	nad	k7c4	nad
<g/>
+	+	kIx~	+
nebo	nebo	k8xC	nebo
NADP	NADP	kA	NADP
<g/>
+	+	kIx~	+
(	(	kIx(	(
<g/>
1.1	[number]	k4	1.1
<g/>
.1	.1	k4	.1
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
laktátdehydrogenázu	laktátdehydrogenáza	k1gFnSc4	laktátdehydrogenáza
(	(	kIx(	(
<g/>
1.1.1.27	[number]	k4	1.1.1.27
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
biochemické	biochemický	k2eAgFnPc4d1	biochemická
reakce	reakce	k1gFnPc4	reakce
jsou	být	k5eAaImIp3nP	být
řízené	řízený	k2eAgInPc1d1	řízený
enzymaticky	enzymaticky	k6eAd1	enzymaticky
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
jsou	být	k5eAaImIp3nP	být
speciální	speciální	k2eAgFnSc1d1	speciální
skupina	skupina	k1gFnSc1	skupina
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
heterogenní	heterogenní	k2eAgFnSc2d1	heterogenní
a	a	k8xC	a
homogenní	homogenní	k2eAgFnSc2d1	homogenní
katalýzy	katalýza	k1gFnSc2	katalýza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
připomínají	připomínat	k5eAaImIp3nP	připomínat
spíše	spíše	k9	spíše
tu	tu	k6eAd1	tu
heterogenní	heterogenní	k2eAgNnSc1d1	heterogenní
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
rozpuštěné	rozpuštěný	k2eAgFnPc1d1	rozpuštěná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
výkonné	výkonný	k2eAgInPc1d1	výkonný
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzymaticky	enzymaticky	k6eAd1	enzymaticky
katalyzované	katalyzovaný	k2eAgFnPc1d1	katalyzovaná
reakce	reakce	k1gFnPc1	reakce
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
108	[number]	k4	108
<g/>
–	–	k?	–
<g/>
1014	[number]	k4	1014
<g/>
×	×	k?	×
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rychlostí	rychlost	k1gFnPc2	rychlost
než	než	k8xS	než
reakce	reakce	k1gFnSc2	reakce
nekatalyzované	katalyzovaný	k2eNgFnSc2d1	katalyzovaný
<g/>
.	.	kIx.	.
</s>
<s>
Extrémním	extrémní	k2eAgInSc7d1	extrémní
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
orotidin-	orotidin-	k?	orotidin-
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
-fosfátdekarboxyláza	osfátdekarboxyláza	k1gFnSc1	-fosfátdekarboxyláza
–	–	k?	–
díky	dík	k1gInPc1	dík
ní	on	k3xPp3gFnSc2	on
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
trvala	trvat	k5eAaImAgFnS	trvat
78	[number]	k4	78
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
18	[number]	k4	18
milisekund	milisekunda	k1gFnPc2	milisekunda
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
předčí	předčit	k5eAaBmIp3nP	předčit
svou	svůj	k3xOyFgFnSc7	svůj
rychlostí	rychlost	k1gFnSc7	rychlost
o	o	k7c4	o
několik	několik	k4yIc4	několik
řádů	řád	k1gInPc2	řád
i	i	k8xC	i
běžné	běžný	k2eAgInPc1d1	běžný
chemické	chemický	k2eAgInPc1d1	chemický
katalyzátory	katalyzátor	k1gInPc1	katalyzátor
<g/>
.	.	kIx.	.
</s>
<s>
Enzymatická	enzymatický	k2eAgFnSc1d1	enzymatická
reakce	reakce	k1gFnSc1	reakce
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
výhod	výhoda	k1gFnPc2	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
reakce	reakce	k1gFnPc1	reakce
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
obvykle	obvykle	k6eAd1	obvykle
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
za	za	k7c2	za
mnohem	mnohem	k6eAd1	mnohem
nižších	nízký	k2eAgFnPc2d2	nižší
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
tlaku	tlak	k1gInSc6	tlak
a	a	k8xC	a
při	při	k7c6	při
fyziologickém	fyziologický	k2eAgInSc6d1	fyziologický
pH	ph	kA	ph
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
enzymy	enzym	k1gInPc1	enzym
velice	velice	k6eAd1	velice
specifické	specifický	k2eAgInPc1d1	specifický
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
chemický	chemický	k2eAgInSc4d1	chemický
katalyzátor	katalyzátor	k1gInSc4	katalyzátor
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
enzym	enzym	k1gInSc4	enzym
snadno	snadno	k6eAd1	snadno
regulovat	regulovat	k5eAaImF	regulovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
alostericky	alostericky	k6eAd1	alostericky
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
útlumem	útlum	k1gInSc7	útlum
syntézy	syntéza	k1gFnSc2	syntéza
daného	daný	k2eAgInSc2d1	daný
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Katalýza	katalýza	k1gFnSc1	katalýza
===	===	k?	===
</s>
</p>
<p>
<s>
Enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiné	jiný	k2eAgInPc1d1	jiný
katalyzátory	katalyzátor	k1gInPc1	katalyzátor
<g/>
,	,	kIx,	,
snižují	snižovat	k5eAaImIp3nP	snižovat
volnou	volný	k2eAgFnSc4d1	volná
aktivační	aktivační	k2eAgFnSc4d1	aktivační
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
Gibbsovu	Gibbsův	k2eAgFnSc4d1	Gibbsova
energii	energie	k1gFnSc4	energie
<g/>
)	)	kIx)	)
–	–	k?	–
Δ	Δ	k?	Δ
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
snížení	snížení	k1gNnSc1	snížení
této	tento	k3xDgFnSc2	tento
aktivační	aktivační	k2eAgFnSc2d1	aktivační
bariéry	bariéra	k1gFnSc2	bariéra
výrazně	výrazně	k6eAd1	výrazně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
rychlost	rychlost	k1gFnSc1	rychlost
reakce	reakce	k1gFnSc1	reakce
(	(	kIx(	(
<g/>
stačí	stačit	k5eAaBmIp3nS	stačit
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgNnSc1d1	malé
snížení	snížení	k1gNnSc1	snížení
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
mnohonásobné	mnohonásobný	k2eAgNnSc4d1	mnohonásobné
zvýšení	zvýšení	k1gNnSc4	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vynikající	vynikající	k2eAgFnPc4d1	vynikající
katalytické	katalytický	k2eAgFnPc4d1	katalytická
schopnosti	schopnost	k1gFnPc4	schopnost
enzymů	enzym	k1gInPc2	enzym
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
především	především	k9	především
čtyři	čtyři	k4xCgInPc4	čtyři
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzym	enzym	k1gInSc1	enzym
vhodně	vhodně	k6eAd1	vhodně
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
substráty	substrát	k1gInPc4	substrát
a	a	k8xC	a
vhodně	vhodně	k6eAd1	vhodně
je	on	k3xPp3gFnPc4	on
prostorově	prostorově	k6eAd1	prostorově
orientuje	orientovat	k5eAaBmIp3nS	orientovat
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tak	tak	k9	tak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
lokální	lokální	k2eAgFnSc1d1	lokální
koncentrace	koncentrace	k1gFnSc1	koncentrace
substrátů	substrát	k1gInPc2	substrát
a	a	k8xC	a
usnadní	usnadnit	k5eAaPmIp3nS	usnadnit
se	se	k3xPyFc4	se
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
v	v	k7c6	v
aktivním	aktivní	k2eAgNnSc6d1	aktivní
místě	místo	k1gNnSc6	místo
enzymu	enzym	k1gInSc2	enzym
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pomocí	pomocí	k7c2	pomocí
zbytků	zbytek	k1gInPc2	zbytek
vhodných	vhodný	k2eAgFnPc2d1	vhodná
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
silně	silně	k6eAd1	silně
kyselé	kyselý	k2eAgNnSc1d1	kyselé
nebo	nebo	k8xC	nebo
zásadité	zásaditý	k2eAgNnSc1d1	zásadité
prostředí	prostředí	k1gNnSc1	prostředí
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
k	k	k7c3	k
proběhnutí	proběhnutí	k1gNnSc3	proběhnutí
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
navázání	navázání	k1gNnSc3	navázání
substrátu	substrát	k1gInSc2	substrát
na	na	k7c4	na
enzym	enzym	k1gInSc4	enzym
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c4	na
serin	serin	k1gInSc4	serin
<g/>
,	,	kIx,	,
cystein	cystein	k1gInSc4	cystein
či	či	k8xC	či
histidin	histidin	k1gInSc4	histidin
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
natahování	natahování	k1gNnSc3	natahování
nebo	nebo	k8xC	nebo
rozrušování	rozrušování	k1gNnSc3	rozrušování
substrátu	substrát	k1gInSc2	substrát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
např.	např.	kA	např.
cílovou	cílový	k2eAgFnSc4d1	cílová
vazbu	vazba	k1gFnSc4	vazba
a	a	k8xC	a
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
rozkladné	rozkladný	k2eAgFnPc4d1	rozkladná
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
katalýze	katalýza	k1gFnSc3	katalýza
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzym	enzym	k1gInSc1	enzym
volí	volit	k5eAaImIp3nS	volit
jiný	jiný	k2eAgInSc1d1	jiný
reakční	reakční	k2eAgInSc1d1	reakční
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
snadněji	snadno	k6eAd2	snadno
(	(	kIx(	(
<g/>
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
aktivační	aktivační	k2eAgFnSc7d1	aktivační
energií	energie	k1gFnSc7	energie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
substrát	substrát	k1gInSc1	substrát
je	být	k5eAaImIp3nS	být
dočasně	dočasně	k6eAd1	dočasně
kovalentně	kovalentně	k6eAd1	kovalentně
navázán	navázán	k2eAgInSc1d1	navázán
samotným	samotný	k2eAgInSc7d1	samotný
enzymem	enzym	k1gInSc7	enzym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Specifita	Specifitum	k1gNnSc2	Specifitum
===	===	k?	===
</s>
</p>
<p>
<s>
Enzymy	enzym	k1gInPc1	enzym
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
specifické	specifický	k2eAgFnPc1d1	specifická
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
katalyzují	katalyzovat	k5eAaBmIp3nP	katalyzovat
zcela	zcela	k6eAd1	zcela
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
chemickou	chemický	k2eAgFnSc4d1	chemická
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
substrátu	substrát	k1gInSc2	substrát
na	na	k7c4	na
produkt	produkt	k1gInSc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
obvykle	obvykle	k6eAd1	obvykle
nejen	nejen	k6eAd1	nejen
účinková	účinkový	k2eAgFnSc1d1	účinková
(	(	kIx(	(
<g/>
reakční	reakční	k2eAgFnSc1d1	reakční
<g/>
)	)	kIx)	)
specifita	specifita	k1gFnSc1	specifita
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
schopnost	schopnost	k1gFnSc1	schopnost
enzymu	enzym	k1gInSc2	enzym
katalyzovat	katalyzovat	k5eAaPmF	katalyzovat
jeden	jeden	k4xCgInSc4	jeden
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
typ	typ	k1gInSc4	typ
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dále	daleko	k6eAd2	daleko
také	také	k9	také
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
substrátová	substrátový	k2eAgFnSc1d1	substrátová
specifita	specifita	k1gFnSc1	specifita
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
katalytická	katalytický	k2eAgFnSc1d1	katalytická
aktivita	aktivita	k1gFnSc1	aktivita
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
substrát	substrát	k1gInSc4	substrát
či	či	k8xC	či
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
několika	několik	k4yIc2	několik
substrátů	substrát	k1gInPc2	substrát
podobných	podobný	k2eAgInPc2d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
enzymatickou	enzymatický	k2eAgFnSc4d1	enzymatická
specifitu	specifita	k1gFnSc4	specifita
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
především	především	k6eAd1	především
komplementární	komplementární	k2eAgInSc4d1	komplementární
tvar	tvar	k1gInSc4	tvar
substrátu	substrát	k1gInSc2	substrát
a	a	k8xC	a
aktivního	aktivní	k2eAgNnSc2d1	aktivní
místa	místo	k1gNnSc2	místo
enzymu	enzym	k1gInSc2	enzym
<g/>
,	,	kIx,	,
náboj	náboj	k1gInSc1	náboj
a	a	k8xC	a
také	také	k9	také
hydrofilní	hydrofilní	k2eAgFnPc1d1	hydrofilní
a	a	k8xC	a
hydrofobní	hydrofobní	k2eAgFnPc1d1	hydrofobní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
enzymu	enzym	k1gInSc2	enzym
a	a	k8xC	a
substrátu	substrát	k1gInSc2	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
silně	silně	k6eAd1	silně
geometricky	geometricky	k6eAd1	geometricky
specifické	specifický	k2eAgInPc1d1	specifický
(	(	kIx(	(
<g/>
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
tvar	tvar	k1gInSc4	tvar
substrátu	substrát	k1gInSc2	substrát
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
<g/>
)	)	kIx)	)
a	a	k8xC	a
stereospecifické	stereospecifický	k2eAgNnSc1d1	stereospecifický
(	(	kIx(	(
<g/>
působí	působit	k5eAaImIp3nS	působit
jen	jen	k9	jen
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
enantiomerů	enantiomer	k1gInPc2	enantiomer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
enzymy	enzym	k1gInPc1	enzym
účastnící	účastnící	k2eAgFnSc2d1	účastnící
se	se	k3xPyFc4	se
replikace	replikace	k1gFnSc2	replikace
a	a	k8xC	a
exprimování	exprimování	k1gNnSc2	exprimování
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
např.	např.	kA	např.
3	[number]	k4	3
<g/>
'	'	kIx"	'
5	[number]	k4	5
<g/>
'	'	kIx"	'
exonukleázy	exonukleáza	k1gFnSc2	exonukleáza
<g/>
)	)	kIx)	)
nejen	nejen	k6eAd1	nejen
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
přesné	přesný	k2eAgFnPc1d1	přesná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
po	po	k7c6	po
sobě	se	k3xPyFc3	se
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
vzácné	vzácný	k2eAgFnPc4d1	vzácná
chyby	chyba	k1gFnPc4	chyba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
tak	tak	k6eAd1	tak
specifické	specifický	k2eAgInPc1d1	specifický
<g/>
,	,	kIx,	,
že	že	k8xS	že
činí	činit	k5eAaImIp3nS	činit
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
chybu	chyba	k1gFnSc4	chyba
na	na	k7c4	na
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
enzymatických	enzymatický	k2eAgFnPc2d1	enzymatická
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
velmi	velmi	k6eAd1	velmi
nespecifické	specifický	k2eNgNnSc1d1	nespecifické
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
promiskuitní	promiskuitní	k2eAgMnSc1d1	promiskuitní
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
katalyzovat	katalyzovat	k5eAaBmF	katalyzovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
reakcí	reakce	k1gFnPc2	reakce
–	–	k?	–
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
γ	γ	k?	γ
z	z	k7c2	z
jedle	jedle	k1gFnSc2	jedle
obrovské	obrovský	k2eAgFnSc2d1	obrovská
(	(	kIx(	(
<g/>
Abies	Abiesa	k1gFnPc2	Abiesa
grandis	grandis	k1gInSc1	grandis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
jednoho	jeden	k4xCgInSc2	jeden
substrátu	substrát	k1gInSc2	substrát
schopna	schopen	k2eAgFnSc1d1	schopna
vyrobit	vyrobit	k5eAaPmF	vyrobit
52	[number]	k4	52
různých	různý	k2eAgInPc2d1	různý
seskviterpenů	seskviterpen	k1gInPc2	seskviterpen
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
biochemik	biochemik	k1gMnSc1	biochemik
Emil	Emil	k1gMnSc1	Emil
Fischer	Fischer	k1gMnSc1	Fischer
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
substrát	substrát	k1gInSc1	substrát
přesně	přesně	k6eAd1	přesně
zapadá	zapadat	k5eAaPmIp3nS	zapadat
do	do	k7c2	do
aktivního	aktivní	k2eAgNnSc2d1	aktivní
centra	centrum	k1gNnSc2	centrum
enzymu	enzym	k1gInSc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
hypotéza	hypotéza	k1gFnSc1	hypotéza
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
Koshland	Koshland	k1gInSc1	Koshland
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzymy	enzym	k1gInPc1	enzym
se	se	k3xPyFc4	se
nechovají	chovat	k5eNaImIp3nP	chovat
přesně	přesně	k6eAd1	přesně
jako	jako	k8xC	jako
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
klíč	klíč	k1gInSc4	klíč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
při	při	k7c6	při
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
setkání	setkání	k1gNnSc6	setkání
enzym	enzym	k1gInSc4	enzym
mění	měnit	k5eAaImIp3nS	měnit
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
"	"	kIx"	"
<g/>
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
klíč	klíč	k1gInSc1	klíč
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
substrát	substrát	k1gInSc1	substrát
naváže	navázat	k5eAaPmIp3nS	navázat
na	na	k7c4	na
enzym	enzym	k1gInSc4	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
hypotéza	hypotéza	k1gFnSc1	hypotéza
indukovaného	indukovaný	k2eAgNnSc2d1	indukované
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
i	i	k8xC	i
samotný	samotný	k2eAgInSc1d1	samotný
substrát	substrát	k1gInSc1	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
indukovaného	indukovaný	k2eAgNnSc2d1	indukované
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
blíže	blízce	k6eAd2	blízce
pravdě	pravda	k1gFnSc3	pravda
než	než	k8xS	než
model	model	k1gInSc4	model
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
klíče	klíč	k1gInSc2	klíč
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
ukázala	ukázat	k5eAaPmAgFnS	ukázat
rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
difrakční	difrakční	k2eAgFnSc1d1	difrakční
analýza	analýza	k1gFnSc1	analýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Regulovatelnost	regulovatelnost	k1gFnSc4	regulovatelnost
===	===	k?	===
</s>
</p>
<p>
<s>
Enzym	enzym	k1gInSc4	enzym
lze	lze	k6eAd1	lze
regulovat	regulovat	k5eAaImF	regulovat
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
regulací	regulace	k1gFnSc7	regulace
jeho	on	k3xPp3gNnSc2	on
množství	množství	k1gNnSc2	množství
a	a	k8xC	a
regulací	regulace	k1gFnSc7	regulace
jeho	jeho	k3xOp3gFnSc2	jeho
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
enzymů	enzym	k1gInPc2	enzym
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
vůbec	vůbec	k9	vůbec
či	či	k8xC	či
téměř	téměř	k6eAd1	téměř
přítomna	přítomen	k2eAgFnSc1d1	přítomna
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
skutečně	skutečně	k6eAd1	skutečně
potřeba	potřeba	k6eAd1	potřeba
(	(	kIx(	(
<g/>
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
enzymy	enzym	k1gInPc4	enzym
v	v	k7c6	v
bakteriálním	bakteriální	k2eAgInSc6d1	bakteriální
lac	lac	k?	lac
operonu	operon	k1gInSc6	operon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
enzymy	enzym	k1gInPc1	enzym
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
možné	možný	k2eAgNnSc1d1	možné
přestat	přestat	k5eAaPmF	přestat
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
okolností	okolnost	k1gFnPc2	okolnost
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
regulaci	regulace	k1gFnSc4	regulace
pomocí	pomocí	k7c2	pomocí
navázání	navázání	k1gNnSc2	navázání
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
regulační	regulační	k2eAgFnPc4d1	regulační
sekvence	sekvence	k1gFnPc4	sekvence
umístěné	umístěný	k2eAgFnPc4d1	umístěná
před	před	k7c7	před
geny	gen	k1gInPc7	gen
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
rychlejší	rychlý	k2eAgFnPc4d2	rychlejší
odpovědi	odpověď	k1gFnPc4	odpověď
buňka	buňka	k1gFnSc1	buňka
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zároveň	zároveň	k6eAd1	zároveň
rozloží	rozložit	k5eAaPmIp3nP	rozložit
enzymy	enzym	k1gInPc1	enzym
již	již	k6eAd1	již
přítomné	přítomný	k2eAgInPc1d1	přítomný
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
velmi	velmi	k6eAd1	velmi
náhlé	náhlý	k2eAgNnSc1d1	náhlé
(	(	kIx(	(
<g/>
a	a	k8xC	a
často	často	k6eAd1	často
dočasné	dočasný	k2eAgFnPc1d1	dočasná
<g/>
)	)	kIx)	)
potřeby	potřeba	k1gFnPc1	potřeba
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
postupuje	postupovat	k5eAaImIp3nS	postupovat
cestou	cesta	k1gFnSc7	cesta
regulace	regulace	k1gFnSc2	regulace
aktivity	aktivita	k1gFnSc2	aktivita
enzymu	enzym	k1gInSc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzym	enzym	k1gInSc4	enzym
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
de	de	k?	de
<g/>
)	)	kIx)	)
<g/>
aktivován	aktivovat	k5eAaBmNgInS	aktivovat
navázáním	navázání	k1gNnSc7	navázání
jistého	jistý	k2eAgInSc2d1	jistý
ligandu	ligand	k1gInSc2	ligand
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
alosterického	alosterický	k2eAgInSc2d1	alosterický
regulátoru	regulátor	k1gInSc2	regulátor
<g/>
)	)	kIx)	)
či	či	k8xC	či
kovalentní	kovalentní	k2eAgFnSc7d1	kovalentní
modifikací	modifikace	k1gFnSc7	modifikace
enzymu	enzym	k1gInSc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Alosterická	Alosterický	k2eAgFnSc1d1	Alosterický
regulace	regulace	k1gFnSc1	regulace
obvykle	obvykle	k6eAd1	obvykle
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
:	:	kIx,	:
produkt	produkt	k1gInSc1	produkt
reakce	reakce	k1gFnSc2	reakce
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
do	do	k7c2	do
alosterického	alosterický	k2eAgNnSc2d1	alosterický
místa	místo	k1gNnSc2	místo
enzymu	enzym	k1gInSc2	enzym
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
jeho	jeho	k3xOp3gFnSc4	jeho
aktivitu	aktivita	k1gFnSc4	aktivita
(	(	kIx(	(
<g/>
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
změně	změna	k1gFnSc3	změna
Michaelisovy	Michaelisův	k2eAgFnSc2d1	Michaelisova
konstanty	konstanta	k1gFnSc2	konstanta
enzymu	enzym	k1gInSc2	enzym
či	či	k8xC	či
limitní	limitní	k2eAgFnSc2d1	limitní
rychlosti	rychlost	k1gFnSc2	rychlost
reakce	reakce	k1gFnSc2	reakce
vmax	vmax	k1gInSc1	vmax
<g/>
.	.	kIx.	.
</s>
<s>
Alostericky	Alostericky	k6eAd1	Alostericky
působí	působit	k5eAaImIp3nS	působit
například	například	k6eAd1	například
vazba	vazba	k1gFnSc1	vazba
různých	různý	k2eAgInPc2d1	různý
hormonů	hormon	k1gInPc2	hormon
či	či	k8xC	či
druhých	druhý	k4xOgMnPc2	druhý
poslů	posel	k1gMnPc2	posel
na	na	k7c4	na
receptor	receptor	k1gInSc4	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kovalentních	kovalentní	k2eAgFnPc2d1	kovalentní
modifikací	modifikace	k1gFnPc2	modifikace
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInPc1d3	nejznámější
zřejmě	zřejmě	k6eAd1	zřejmě
fosforylace	fosforylace	k1gFnPc4	fosforylace
a	a	k8xC	a
defosforylace	defosforylace	k1gFnPc4	defosforylace
enzymů	enzym	k1gInPc2	enzym
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
pomocí	pomocí	k7c2	pomocí
kináz	kináza	k1gFnPc2	kináza
a	a	k8xC	a
fosfatáz	fosfatáza	k1gFnPc2	fosfatáza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
aktivity	aktivita	k1gFnSc2	aktivita
enzymu	enzym	k1gInSc2	enzym
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
spuštění	spuštění	k1gNnSc3	spuštění
nebo	nebo	k8xC	nebo
vypnutí	vypnutí	k1gNnSc3	vypnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
aktivace	aktivace	k1gFnSc1	aktivace
je	být	k5eAaImIp3nS	být
vyštěpení	vyštěpení	k1gNnSc4	vyštěpení
části	část	k1gFnSc2	část
proteinu	protein	k1gInSc2	protein
pomocí	pomocí	k7c2	pomocí
proteázy	proteáza	k1gFnSc2	proteáza
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
proenzym	proenzym	k1gInSc1	proenzym
(	(	kIx(	(
<g/>
zymogen	zymogen	k1gInSc1	zymogen
<g/>
)	)	kIx)	)
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
aktivní	aktivní	k2eAgInSc4d1	aktivní
enzym	enzym	k1gInSc4	enzym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Inhibice	inhibice	k1gFnSc2	inhibice
====	====	k?	====
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
inhibici	inhibice	k1gFnSc4	inhibice
na	na	k7c4	na
ireverzibilní	ireverzibilní	k2eAgFnSc4d1	ireverzibilní
(	(	kIx(	(
<g/>
nevratnou	vratný	k2eNgFnSc4d1	nevratná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvale	trvale	k6eAd1	trvale
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
molekulu	molekula	k1gFnSc4	molekula
enzymu	enzym	k1gInSc2	enzym
<g/>
,	,	kIx,	,
a	a	k8xC	a
reverzibilní	reverzibilní	k2eAgFnSc4d1	reverzibilní
(	(	kIx(	(
<g/>
vratnou	vratný	k2eAgFnSc4d1	vratná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
inhibitoru	inhibitor	k1gInSc2	inhibitor
např.	např.	kA	např.
dialýzou	dialýza	k1gFnSc7	dialýza
či	či	k8xC	či
ultrafiltrací	ultrafiltrace	k1gFnSc7	ultrafiltrace
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
zvýšení	zvýšení	k1gNnSc3	zvýšení
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Ireverzibilní	ireverzibilní	k2eAgInSc1d1	ireverzibilní
inhibitor	inhibitor	k1gInSc1	inhibitor
se	se	k3xPyFc4	se
na	na	k7c4	na
enzym	enzym	k1gInSc4	enzym
obvykle	obvykle	k6eAd1	obvykle
váže	vázat	k5eAaImIp3nS	vázat
pevnou	pevný	k2eAgFnSc7d1	pevná
kovalentní	kovalentní	k2eAgFnSc7d1	kovalentní
vazbou	vazba	k1gFnSc7	vazba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
reverzibilní	reverzibilní	k2eAgInSc1d1	reverzibilní
inhibitor	inhibitor	k1gInSc1	inhibitor
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
slabšími	slabý	k2eAgFnPc7d2	slabší
interakcemi	interakce	k1gFnPc7	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
mechanismu	mechanismus	k1gInSc3	mechanismus
působení	působení	k1gNnSc2	působení
inhibitoru	inhibitor	k1gInSc2	inhibitor
se	se	k3xPyFc4	se
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
tři	tři	k4xCgInPc1	tři
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
reverzibilní	reverzibilní	k2eAgFnSc2d1	reverzibilní
inhibice	inhibice	k1gFnSc2	inhibice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
kompetitivní	kompetitivní	k2eAgFnSc1d1	kompetitivní
(	(	kIx(	(
<g/>
soutěživá	soutěživý	k2eAgFnSc1d1	soutěživá
<g/>
)	)	kIx)	)
inhibice	inhibice	k1gFnSc1	inhibice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
inhibičně	inhibičně	k6eAd1	inhibičně
působící	působící	k2eAgFnSc1d1	působící
molekula	molekula	k1gFnSc1	molekula
soutěží	soutěž	k1gFnPc2	soutěž
se	s	k7c7	s
substrátem	substrát	k1gInSc7	substrát
o	o	k7c4	o
vazebné	vazebný	k2eAgNnSc4d1	vazebné
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
enzymu	enzym	k1gInSc6	enzym
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
produkt	produkt	k1gInSc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšením	zvýšení	k1gNnSc7	zvýšení
koncentrace	koncentrace	k1gFnSc2	koncentrace
substrátu	substrát	k1gInSc2	substrát
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
tomuto	tento	k3xDgInSc3	tento
typu	typ	k1gInSc3	typ
inhibice	inhibice	k1gFnSc2	inhibice
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgFnSc1d2	častější
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
inhibice	inhibice	k1gFnSc2	inhibice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
inhibice	inhibice	k1gFnSc1	inhibice
nekompetitivní	kompetitivní	k2eNgFnSc1d1	kompetitivní
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vazbě	vazba	k1gFnSc3	vazba
inhibitoru	inhibitor	k1gInSc2	inhibitor
na	na	k7c4	na
alosterické	alosterický	k2eAgNnSc4d1	alosterický
centrum	centrum	k1gNnSc4	centrum
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
aktivní	aktivní	k2eAgNnSc4d1	aktivní
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
funkceschopnosti	funkceschopnost	k1gFnSc2	funkceschopnost
enzymu	enzym	k1gInSc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
koncentrace	koncentrace	k1gFnSc2	koncentrace
substrátu	substrát	k1gInSc2	substrát
s	s	k7c7	s
takovým	takový	k3xDgInSc7	takový
typem	typ	k1gInSc7	typ
inhibice	inhibice	k1gFnSc1	inhibice
nic	nic	k3yNnSc1	nic
neudělá	udělat	k5eNaPmIp3nS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
třetím	třetí	k4xOgInSc7	třetí
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
akompetitivní	akompetitivní	k2eAgFnSc1d1	akompetitivní
inhibice	inhibice	k1gFnSc1	inhibice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vazbě	vazba	k1gFnSc3	vazba
inhibitoru	inhibitor	k1gInSc2	inhibitor
na	na	k7c4	na
enzym	enzym	k1gInSc4	enzym
až	až	k6eAd1	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
navázán	navázán	k2eAgInSc1d1	navázán
substrát	substrát	k1gInSc1	substrát
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
komplexu	komplex	k1gInSc3	komplex
enzym-substrát	enzymubstrát	k1gInSc1	enzym-substrát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
provedl	provést	k5eAaPmAgInS	provést
enzymatickou	enzymatický	k2eAgFnSc4d1	enzymatická
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
způsobů	způsob	k1gInPc2	způsob
regulace	regulace	k1gFnSc2	regulace
enzymové	enzymový	k2eAgFnPc4d1	enzymová
aktivity	aktivita	k1gFnPc4	aktivita
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
hledisko	hledisko	k1gNnSc1	hledisko
třídění	třídění	k1gNnSc2	třídění
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kinetika	kinetika	k1gFnSc1	kinetika
==	==	k?	==
</s>
</p>
<p>
<s>
Kinetika	kinetika	k1gFnSc1	kinetika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
rychlost	rychlost	k1gFnSc4	rychlost
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
související	související	k2eAgFnPc4d1	související
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
enzymovou	enzymový	k2eAgFnSc4d1	enzymová
kinetiku	kinetika	k1gFnSc4	kinetika
platí	platit	k5eAaImIp3nP	platit
obecné	obecný	k2eAgInPc1d1	obecný
zákony	zákon	k1gInPc1	zákon
chemické	chemický	k2eAgFnSc2d1	chemická
kinetiky	kinetika	k1gFnSc2	kinetika
a	a	k8xC	a
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
běžně	běžně	k6eAd1	běžně
známé	známý	k2eAgFnPc1d1	známá
chemické	chemický	k2eAgFnPc1d1	chemická
veličiny	veličina	k1gFnPc1	veličina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
reakční	reakční	k2eAgFnSc4d1	reakční
rychlost	rychlost	k1gFnSc4	rychlost
(	(	kIx(	(
<g/>
v	v	k7c4	v
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovnovážná	rovnovážný	k2eAgFnSc1d1	rovnovážná
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
K	k	k7c3	k
<g/>
)	)	kIx)	)
či	či	k8xC	či
třeba	třeba	k6eAd1	třeba
Gibbsova	Gibbsův	k2eAgFnSc1d1	Gibbsova
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
zjednodušující	zjednodušující	k2eAgFnSc2d1	zjednodušující
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzymem	enzym	k1gInSc7	enzym
katalyzovaná	katalyzovaný	k2eAgFnSc1d1	katalyzovaná
reakce	reakce	k1gFnSc1	reakce
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
krocích	krok	k1gInPc6	krok
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
E	E	kA	E
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
substrát	substrát	k1gInSc1	substrát
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
P	P	kA	P
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc1	produkt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟵	⟵	k?	⟵
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
+	+	kIx~	+
<g/>
S	s	k7c7	s
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
underset	underset	k1gInSc1	underset
{	{	kIx(	{
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
smallmatrix	smallmatrix	k1gInSc1	smallmatrix
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
\\\	\\\	k?	\\\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
longleftarrow	longleftarrow	k?	longleftarrow
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
smallmatrix	smallmatrix	k1gInSc1	smallmatrix
<g/>
}}}}	}}}}	k?	}}}}
<g/>
ES	ES	kA	ES
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overset	overset	k1gInSc1	overset
{	{	kIx(	{
<g/>
k_	k_	k?	k_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
longrightarrow	longrightarrow	k?	longrightarrow
}}	}}	k?	}}
<g/>
E	E	kA	E
<g/>
+	+	kIx~	+
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
načež	načež	k6eAd1	načež
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
max	max	kA	max
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gMnSc1	begin
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}	}	kIx)	}
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
max	max	kA	max
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
[	[	kIx(	[
<g/>
}	}	kIx)	}
<g/>
S	s	k7c7	s
<g/>
{	{	kIx(	{
<g/>
]	]	kIx)	]
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
K_	K_	k1gMnSc1	K_
<g/>
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
[	[	kIx(	[
<g/>
}	}	kIx)	}
<g/>
S	s	k7c7	s
<g/>
{	{	kIx(	{
<g/>
]	]	kIx)	]
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
rovnice	rovnice	k1gFnSc1	rovnice
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
rovnice	rovnice	k1gFnSc1	rovnice
Michaelise	Michaelise	k1gFnSc1	Michaelise
a	a	k8xC	a
Mentenové	Mentenové	k2eAgFnSc1d1	Mentenové
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
rovnice	rovnice	k1gFnSc1	rovnice
enzymové	enzymový	k2eAgFnSc2d1	enzymová
kinetiky	kinetika	k1gFnSc2	kinetika
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
počáteční	počáteční	k2eAgFnSc4d1	počáteční
rychlost	rychlost	k1gFnSc4	rychlost
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
max	max	kA	max
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
max	max	kA	max
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
mezní	mezní	k2eAgFnSc1d1	mezní
rychlost	rychlost	k1gFnSc1	rychlost
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
při	při	k7c6	při
nadbytku	nadbytek	k1gInSc6	nadbytek
substrátu	substrát	k1gInSc2	substrát
a	a	k8xC	a
100	[number]	k4	100
<g/>
%	%	kIx~	%
nasycení	nasycení	k1gNnSc2	nasycení
enzymů	enzym	k1gInPc2	enzym
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K_	K_	k1gMnSc1	K_
<g/>
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
Michaelisova	Michaelisův	k2eAgFnSc1d1	Michaelisova
konstanta	konstanta	k1gFnSc1	konstanta
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
[	[	kIx(	[
<g/>
}	}	kIx)	}
<g/>
S	s	k7c7	s
<g/>
{	{	kIx(	{
<g/>
]	]	kIx)	]
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
rovnovážná	rovnovážný	k2eAgFnSc1d1	rovnovážná
koncentrace	koncentrace	k1gFnSc1	koncentrace
substrátu	substrát	k1gInSc2	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
uvedené	uvedený	k2eAgFnSc2d1	uvedená
rovnice	rovnice	k1gFnSc2	rovnice
má	mít	k5eAaImIp3nS	mít
závislost	závislost	k1gFnSc4	závislost
reakční	reakční	k2eAgFnSc2d1	reakční
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c6	na
koncentraci	koncentrace	k1gFnSc6	koncentrace
substrátu	substrát	k1gInSc2	substrát
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
hyperboly	hyperbola	k1gFnSc2	hyperbola
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
veličinou	veličina	k1gFnSc7	veličina
je	být	k5eAaImIp3nS	být
Michaelisova	Michaelisův	k2eAgFnSc1d1	Michaelisova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
platí	platit	k5eAaImIp3nS	platit
KM	km	kA	km
=	=	kIx~	=
(	(	kIx(	(
<g/>
k-	k-	k?	k-
<g/>
1	[number]	k4	1
+	+	kIx~	+
k	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
/	/	kIx~	/
k	k	k7c3	k
<g/>
1	[number]	k4	1
a	a	k8xC	a
která	který	k3yRgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
koncentraci	koncentrace	k1gFnSc4	koncentrace
substrátu	substrát	k1gInSc2	substrát
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
reakční	reakční	k2eAgFnSc1d1	reakční
rychlost	rychlost	k1gFnSc1	rychlost
rovna	roven	k2eAgFnSc1d1	rovna
polovině	polovina	k1gFnSc3	polovina
maximální	maximální	k2eAgFnSc2d1	maximální
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Rovnice	rovnice	k1gFnSc1	rovnice
Michaelise	Michaelise	k1gFnSc2	Michaelise
a	a	k8xC	a
Mentenové	Mentenová	k1gFnSc2	Mentenová
však	však	k9	však
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
zjednodušujícím	zjednodušující	k2eAgInSc7d1	zjednodušující
popisem	popis	k1gInSc7	popis
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
platila	platit	k5eAaImAgFnS	platit
by	by	kYmCp3nS	by
jen	jen	k9	jen
pro	pro	k7c4	pro
počáteční	počáteční	k2eAgInPc4d1	počáteční
stavy	stav	k1gInPc4	stav
jednosubstrátových	jednosubstrátový	k2eAgFnPc2d1	jednosubstrátový
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
by	by	kYmCp3nS	by
muselo	muset	k5eAaImAgNnS	muset
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
přímému	přímý	k2eAgInSc3d1	přímý
rozpadu	rozpad	k1gInSc3	rozpad
komplexu	komplex	k1gInSc2	komplex
enzym-substrát	enzymubstrát	k1gInSc4	enzym-substrát
na	na	k7c4	na
enzym	enzym	k1gInSc4	enzym
a	a	k8xC	a
produkt	produkt	k1gInSc4	produkt
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
enzymatické	enzymatický	k2eAgFnSc2d1	enzymatická
reakce	reakce	k1gFnSc2	reakce
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
Michaelisovy	Michaelisův	k2eAgFnSc2d1	Michaelisova
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
řada	řada	k1gFnSc1	řada
fyzikálně-chemických	fyzikálněhemický	k2eAgFnPc2d1	fyzikálně-chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
teplotou	teplota	k1gFnSc7	teplota
roste	růst	k5eAaImIp3nS	růst
rychlost	rychlost	k1gFnSc1	rychlost
enzymatické	enzymatický	k2eAgFnSc2d1	enzymatická
reakce	reakce	k1gFnSc2	reakce
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
bílkovinná	bílkovinný	k2eAgFnSc1d1	bílkovinná
složka	složka	k1gFnSc1	složka
enzymu	enzym	k1gInSc2	enzym
začne	začít	k5eAaPmIp3nS	začít
denaturovat	denaturovat	k5eAaBmF	denaturovat
<g/>
.	.	kIx.	.
</s>
<s>
Denaturační	denaturační	k2eAgFnSc1d1	denaturační
teplota	teplota	k1gFnSc1	teplota
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
živočišných	živočišný	k2eAgInPc2d1	živočišný
enzymů	enzym	k1gInPc2	enzym
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
výrazný	výrazný	k2eAgInSc1d1	výrazný
vliv	vliv	k1gInSc1	vliv
pH	ph	kA	ph
prostředí	prostředí	k1gNnSc3	prostředí
–	–	k?	–
většině	většina	k1gFnSc3	většina
enzymů	enzym	k1gInPc2	enzym
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
nejlépe	dobře	k6eAd3	dobře
pH	ph	kA	ph
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pepsin	pepsin	k1gInSc1	pepsin
má	mít	k5eAaImIp3nS	mít
optimum	optimum	k1gNnSc1	optimum
při	při	k7c6	při
pH	ph	kA	ph
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
a	a	k8xC	a
argináza	argináza	k1gFnSc1	argináza
při	při	k7c6	při
pH	ph	kA	ph
9,5	[number]	k4	9,5
<g/>
.	.	kIx.	.
</s>
<s>
Aktivitu	aktivita	k1gFnSc4	aktivita
enzymů	enzym	k1gInPc2	enzym
dále	daleko	k6eAd2	daleko
někdy	někdy	k6eAd1	někdy
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
redox	redox	k2eAgInSc1d1	redox
potenciál	potenciál	k1gInSc1	potenciál
<g/>
,	,	kIx,	,
iontová	iontový	k2eAgFnSc1d1	iontová
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kapitolou	kapitola	k1gFnSc7	kapitola
je	být	k5eAaImIp3nS	být
vliv	vliv	k1gInSc1	vliv
inhibitorů	inhibitor	k1gInPc2	inhibitor
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
efektorů	efektor	k1gInPc2	efektor
na	na	k7c4	na
průběh	průběh	k1gInSc4	průběh
enzymatické	enzymatický	k2eAgFnSc2d1	enzymatická
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Enzymy	enzym	k1gInPc1	enzym
nalezly	naleznout	k5eAaPmAgInP	naleznout
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
funkcí	funkce	k1gFnPc2	funkce
i	i	k8xC	i
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
oborů	obor	k1gInPc2	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Neoddiskutovatelný	oddiskutovatelný	k2eNgInSc1d1	neoddiskutovatelný
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
–	–	k?	–
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
různé	různý	k2eAgFnPc1d1	různá
polymerázy	polymeráza	k1gFnPc1	polymeráza
<g/>
,	,	kIx,	,
restrikční	restrikční	k2eAgFnPc1d1	restrikční
endonukleázy	endonukleáza	k1gFnPc1	endonukleáza
<g/>
,	,	kIx,	,
proteázy	proteáza	k1gFnPc1	proteáza
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
enzymy	enzym	k1gInPc1	enzym
také	také	k9	také
do	do	k7c2	do
pracích	prací	k2eAgInPc2d1	prací
prášků	prášek	k1gInPc2	prášek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
účinnost	účinnost	k1gFnSc1	účinnost
odstraňování	odstraňování	k1gNnSc2	odstraňování
skvrn	skvrna	k1gFnPc2	skvrna
i	i	k9	i
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
šetří	šetřit	k5eAaImIp3nP	šetřit
energii	energie	k1gFnSc4	energie
i	i	k9	i
v	v	k7c6	v
potravinářském	potravinářský	k2eAgMnSc6d1	potravinářský
<g/>
,	,	kIx,	,
textilním	textilní	k2eAgMnSc6d1	textilní
a	a	k8xC	a
papírenském	papírenský	k2eAgInSc6d1	papírenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
v	v	k7c6	v
odpadovém	odpadový	k2eAgNnSc6d1	odpadové
hospodářství	hospodářství	k1gNnSc6	hospodářství
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
masivním	masivní	k2eAgNnSc6d1	masivní
nasazení	nasazení	k1gNnSc6	nasazení
enzymů	enzym	k1gInPc2	enzym
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
ekologicky	ekologicky	k6eAd1	ekologicky
šetrných	šetrný	k2eAgFnPc2d1	šetrná
biopaliv	biopalit	k5eAaPmDgInS	biopalit
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
aplikacím	aplikace	k1gFnPc3	aplikace
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
usnadnit	usnadnit	k5eAaPmF	usnadnit
cestu	cesta	k1gFnSc4	cesta
tzv.	tzv.	kA	tzv.
enzymové	enzymový	k2eAgNnSc1d1	enzymové
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
.	.	kIx.	.
<g/>
Proteolytické	proteolytický	k2eAgInPc1d1	proteolytický
enzymy	enzym	k1gInPc1	enzym
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
například	například	k6eAd1	například
v	v	k7c6	v
mlékárenském	mlékárenský	k2eAgInSc6d1	mlékárenský
průmyslu	průmysl	k1gInSc6	průmysl
jako	jako	k9	jako
syřidla	syřidlo	k1gNnSc2	syřidlo
(	(	kIx(	(
<g/>
chymosin	chymosin	k1gInSc1	chymosin
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
hypoalergeního	hypoalergení	k2eAgNnSc2d1	hypoalergení
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
i	i	k9	i
ke	k	k7c3	k
změkčování	změkčování	k1gNnSc3	změkčování
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
papain	papain	k1gInSc1	papain
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
enzymatického	enzymatický	k2eAgNnSc2d1	enzymatické
štěpení	štěpení	k1gNnSc2	štěpení
trisacharidů	trisacharid	k1gInPc2	trisacharid
v	v	k7c6	v
luštěninách	luštěnina	k1gFnPc6	luštěnina
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
takové	takový	k3xDgFnPc4	takový
luštěniny	luštěnina	k1gFnPc4	luštěnina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nenadýmají	nadýmat	k5eNaImIp3nP	nadýmat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
enzymů	enzym	k1gInPc2	enzym
jako	jako	k8xC	jako
značek	značka	k1gFnPc2	značka
na	na	k7c6	na
specifickém	specifický	k2eAgInSc6d1	specifický
indikátoru	indikátor	k1gInSc6	indikátor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ELISA	ELISA	kA	ELISA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
redoxních	redoxní	k2eAgInPc2d1	redoxní
enzymů	enzym	k1gInPc2	enzym
lze	lze	k6eAd1	lze
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
stanovit	stanovit	k5eAaPmF	stanovit
koncentraci	koncentrace	k1gFnSc4	koncentrace
specifického	specifický	k2eAgInSc2d1	specifický
substrátu	substrát	k1gInSc2	substrát
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
enzym	enzym	k1gInSc4	enzym
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
lze	lze	k6eAd1	lze
podávat	podávat	k5eAaImF	podávat
enzymy	enzym	k1gInPc4	enzym
jako	jako	k8xS	jako
náhradu	náhrada	k1gFnSc4	náhrada
chybějících	chybějící	k2eAgInPc2d1	chybějící
enzymů	enzym	k1gInPc2	enzym
při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
či	či	k8xC	či
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
některých	některý	k3yIgNnPc2	některý
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
perorálním	perorální	k2eAgNnSc6d1	perorální
podávání	podávání	k1gNnSc6	podávání
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
usnadnit	usnadnit	k5eAaPmF	usnadnit
trávení	trávení	k1gNnSc4	trávení
či	či	k8xC	či
metabolismus	metabolismus	k1gInSc4	metabolismus
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
denaturaci	denaturace	k1gFnSc3	denaturace
těchto	tento	k3xDgInPc2	tento
enzymů	enzym	k1gInPc2	enzym
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Denaturaci	denaturace	k1gFnSc4	denaturace
enzymů	enzym	k1gInPc2	enzym
v	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
žaludku	žaludek	k1gInSc2	žaludek
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
předcházet	předcházet	k5eAaImF	předcházet
pomocí	pomocí	k7c2	pomocí
vhodné	vhodný	k2eAgFnSc2d1	vhodná
enkapsulace	enkapsulace	k1gFnSc2	enkapsulace
<g/>
;	;	kIx,	;
žaludkem	žaludek	k1gInSc7	žaludek
však	však	k9	však
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
projde	projít	k5eAaPmIp3nS	projít
jen	jen	k9	jen
několik	několik	k4yIc4	několik
procent	procento	k1gNnPc2	procento
enzymů	enzym	k1gInPc2	enzym
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
(	(	kIx(	(
<g/>
nedegradovaném	degradovaný	k2eNgInSc6d1	nedegradovaný
<g/>
)	)	kIx)	)
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
využívány	využíván	k2eAgInPc1d1	využíván
při	při	k7c6	při
restaurování	restaurování	k1gNnSc6	restaurování
uměleckých	umělecký	k2eAgInPc2d1	umělecký
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
malby	malba	k1gFnSc2	malba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
enzyme	enzym	k1gInSc5	enzym
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VODRÁŽKA	Vodrážka	k1gMnSc1	Vodrážka
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Biochemie	biochemie	k1gFnSc1	biochemie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
600	[number]	k4	600
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VODRÁŽKA	Vodrážka	k1gMnSc1	Vodrážka
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
RAUSCH	RAUSCH	kA	RAUSCH
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
KÁŠ	KÁŠ	kA	KÁŠ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Enzymologie	Enzymologie	k1gFnSc1	Enzymologie
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
VŠCHT	VŠCHT	kA	VŠCHT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOET	VOET	kA	VOET
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
<g/>
;	;	kIx,	;
VOET	VOET	kA	VOET
<g/>
,	,	kIx,	,
Judith	Judith	k1gInSc1	Judith
<g/>
.	.	kIx.	.
</s>
<s>
Biochemie	biochemie	k1gFnSc1	biochemie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
..	..	k?	..
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Victoria	Victorium	k1gNnSc2	Victorium
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85605	[number]	k4	85605
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MURRAY	MURRAY	kA	MURRAY
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
K.	K.	kA	K.
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Harperova	Harperův	k2eAgFnSc1d1	Harperova
biochemie	biochemie	k1gFnSc1	biochemie
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jiří	Jiří	k1gMnSc1	Jiří
Kraml	Kramla	k1gFnPc2	Kramla
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
4	[number]	k4	4
<g/>
.	.	kIx.	.
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
H	H	kA	H
&	&	k?	&
H	H	kA	H
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
872	[number]	k4	872
s	s	k7c7	s
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7319	[number]	k4	7319
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
enzym	enzym	k1gInSc4	enzym
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
enzym	enzym	k1gInSc1	enzym
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
BRENDA	BRENDA	kA	BRENDA
–	–	k?	–
comprehensive	comprehensivat	k5eAaPmIp3nS	comprehensivat
compilation	compilation	k1gInSc4	compilation
of	of	k?	of
information	information	k1gInSc1	information
and	and	k?	and
literature	literatur	k1gMnSc5	literatur
references	references	k1gInSc1	references
about	about	k1gMnSc1	about
all	all	k?	all
known	known	k1gMnSc1	known
enzymes	enzymes	k1gMnSc1	enzymes
</s>
</p>
<p>
<s>
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
klasifikace	klasifikace	k1gFnSc1	klasifikace
enzymů	enzym	k1gInPc2	enzym
</s>
</p>
