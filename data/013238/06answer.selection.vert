<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
,	,	kIx,	,
také	také	k9	také
Šnirch	Šnirch	k1gInSc1	Šnirch
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Friedrich	Friedrich	k1gMnSc1	Friedrich
Franz	Franz	k1gMnSc1	Franz
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1791	[number]	k4	1791
Pátek	pátek	k1gInSc1	pátek
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
u	u	k7c2	u
Loun	Louny	k1gInPc2	Louny
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1868	[number]	k4	1868
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
císařský	císařský	k2eAgMnSc1d1	císařský
rada	rada	k1gMnSc1	rada
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
inspektor	inspektor	k1gMnSc1	inspektor
c.	c.	k?	c.
k.	k.	k?	k.
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
především	především	k9	především
návrhy	návrh	k1gInPc1	návrh
řetězových	řetězový	k2eAgInPc2d1	řetězový
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
