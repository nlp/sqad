<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
,	,	kIx,	,
také	také	k9	také
Šnirch	Šnirch	k1gInSc1	Šnirch
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Friedrich	Friedrich	k1gMnSc1	Friedrich
Franz	Franz	k1gMnSc1	Franz
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1791	[number]	k4	1791
Pátek	pátek	k1gInSc1	pátek
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
u	u	k7c2	u
Loun	Louny	k1gInPc2	Louny
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1868	[number]	k4	1868
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
císařský	císařský	k2eAgMnSc1d1	císařský
rada	rada	k1gMnSc1	rada
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
inspektor	inspektor	k1gMnSc1	inspektor
c.	c.	k?	c.
k.	k.	k?	k.
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
především	především	k9	především
návrhy	návrh	k1gInPc1	návrh
řetězových	řetězový	k2eAgInPc2d1	řetězový
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
synovcem	synovec	k1gMnSc7	synovec
byl	být	k5eAaImAgMnS	být
stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
inženýr	inženýr	k1gMnSc1	inženýr
Josef	Josef	k1gMnSc1	Josef
Emanuel	Emanuel	k1gMnSc1	Emanuel
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
,	,	kIx,	,
prasynovcem	prasynovec	k1gMnSc7	prasynovec
pak	pak	k8xC	pak
sochař	sochař	k1gMnSc1	sochař
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Schnirch	Schnirch	k1gMnSc1	Schnirch
studoval	studovat	k5eAaImAgMnS	studovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Hornu	Horn	k1gMnSc6	Horn
a	a	k8xC	a
v	v	k7c6	v
dolnorakouské	dolnorakouský	k2eAgFnSc6d1	Dolnorakouská
Kremži	Kremže	k1gFnSc6	Kremže
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1819	[number]	k4	1819
až	až	k9	až
1821	[number]	k4	1821
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
polytechnice	polytechnika	k1gFnSc6	polytechnika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
hraběte	hrabě	k1gMnSc2	hrabě
Franze	Franze	k1gFnSc2	Franze
Antona	Anton	k1gMnSc2	Anton
Magnise	Magnise	k1gFnSc2	Magnise
<g/>
,	,	kIx,	,
majitele	majitel	k1gMnSc2	majitel
Strážnického	strážnický	k2eAgNnSc2d1	Strážnické
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
řídit	řídit	k5eAaImF	řídit
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc2	panství
chtěl	chtít	k5eAaImAgMnS	chtít
rozšířit	rozšířit	k5eAaPmF	rozšířit
park	park	k1gInSc4	park
za	za	k7c4	za
řeku	řeka	k1gFnSc4	řeka
a	a	k8xC	a
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
vybudovat	vybudovat	k5eAaPmF	vybudovat
přístupovou	přístupový	k2eAgFnSc4d1	přístupová
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
Schnirch	Schnirch	k1gInSc4	Schnirch
jako	jako	k8xC	jako
řešení	řešení	k1gNnSc4	řešení
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
technickou	technický	k2eAgFnSc4d1	technická
novinku	novinka	k1gFnSc4	novinka
-	-	kIx~	-
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
dosud	dosud	k6eAd1	dosud
nerealizovaný	realizovaný	k2eNgMnSc1d1	nerealizovaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
se	se	k3xPyFc4	se
Schnirch	Schnirch	k1gMnSc1	Schnirch
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
inženýrem	inženýr	k1gMnSc7	inženýr
na	na	k7c6	na
vídeňském	vídeňský	k2eAgNnSc6d1	Vídeňské
ředitelství	ředitelství	k1gNnSc6	ředitelství
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
založení	založení	k1gNnSc2	založení
Rakouského	rakouský	k2eAgInSc2d1	rakouský
svazu	svaz	k1gInSc2	svaz
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
u	u	k7c2	u
založení	založení	k1gNnSc2	založení
Svaz	svaz	k1gInSc1	svaz
inženýrů	inženýr	k1gMnPc2	inženýr
a	a	k8xC	a
architektů	architekt	k1gMnPc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
byl	být	k5eAaImAgInS	být
i	i	k9	i
hlavním	hlavní	k2eAgMnSc7d1	hlavní
inspektorem	inspektor	k1gMnSc7	inspektor
staveb	stavba	k1gFnPc2	stavba
na	na	k7c6	na
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
matrice	matrika	k1gFnSc6	matrika
zemřelých	zemřelý	k1gMnPc2	zemřelý
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
77	[number]	k4	77
let	léto	k1gNnPc2	léto
na	na	k7c4	na
slabost	slabost	k1gFnSc4	slabost
stářím	stáří	k1gNnSc7	stáří
(	(	kIx(	(
<g/>
Altersschwäche	Altersschwäche	k1gNnSc7	Altersschwäche
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
dostal	dostat	k5eAaPmAgInS	dostat
rytířský	rytířský	k2eAgInSc1d1	rytířský
kříž	kříž	k1gInSc1	kříž
řádu	řád	k1gInSc2	řád
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Strážnický	strážnický	k2eAgInSc1d1	strážnický
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
===	===	k?	===
</s>
</p>
<p>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc1	první
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
podle	podle	k7c2	podle
Schnirchova	Schnirchův	k2eAgInSc2d1	Schnirchův
projektu	projekt	k1gInSc2	projekt
ve	v	k7c6	v
Strážnici	Strážnice	k1gFnSc6	Strážnice
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
29	[number]	k4	29
m	m	kA	m
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
rozpětí	rozpětí	k1gNnSc4	rozpětí
29,7	[number]	k4	29,7
m	m	kA	m
a	a	k8xC	a
šířku	šířka	k1gFnSc4	šířka
4	[number]	k4	4
m.	m.	k?	m.
Řetězy	řetěz	k1gInPc7	řetěz
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ukotvit	ukotvit	k5eAaPmF	ukotvit
do	do	k7c2	do
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
proto	proto	k8xC	proto
kotveny	kotven	k2eAgInPc1d1	kotven
do	do	k7c2	do
zděných	zděný	k2eAgInPc2d1	zděný
kotevních	kotevní	k2eAgInPc2d1	kotevní
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
mostu	most	k1gInSc2	most
bylo	být	k5eAaImAgNnS	být
devatenáct	devatenáct	k4xCc1	devatenáct
závěsných	závěsný	k2eAgFnPc2d1	závěsná
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nesly	nést	k5eAaImAgFnP	nést
podélné	podélný	k2eAgInPc4d1	podélný
nosníky	nosník	k1gInPc4	nosník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
nosníky	nosník	k1gInPc4	nosník
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
příčníky	příčník	k1gInPc1	příčník
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
mostovka	mostovka	k1gFnSc1	mostovka
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
chodců	chodec	k1gMnPc2	chodec
měl	mít	k5eAaImAgInS	mít
most	most	k1gInSc1	most
zábradlí	zábradlí	k1gNnSc2	zábradlí
z	z	k7c2	z
drátů	drát	k1gInPc2	drát
ovinutých	ovinutý	k2eAgInPc2d1	ovinutý
kolem	kolem	k7c2	kolem
závěsných	závěsný	k2eAgFnPc2d1	závěsná
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1824	[number]	k4	1824
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
a	a	k8xC	a
lehké	lehký	k2eAgInPc4d1	lehký
i	i	k8xC	i
těžké	těžký	k2eAgInPc4d1	těžký
povozy	povoz	k1gInPc4	povoz
<g/>
.	.	kIx.	.
<g/>
Železná	železný	k2eAgFnSc1d1	železná
konstrukce	konstrukce	k1gFnSc1	konstrukce
i	i	k8xC	i
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
mostovka	mostovka	k1gFnSc1	mostovka
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
údržbu	údržba	k1gFnSc4	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
například	například	k6eAd1	například
pravidelně	pravidelně	k6eAd1	pravidelně
obracet	obracet	k5eAaImF	obracet
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
příčné	příčný	k2eAgInPc4d1	příčný
nosníky	nosník	k1gInPc4	nosník
mostovky	mostovka	k1gFnSc2	mostovka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
trvalému	trvalý	k2eAgInSc3d1	trvalý
průhybu	průhyb	k1gInSc3	průhyb
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
byl	být	k5eAaImAgInS	být
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
nakonec	nakonec	k6eAd1	nakonec
rozebrán	rozebrán	k2eAgInSc1d1	rozebrán
(	(	kIx(	(
<g/>
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
zanedbání	zanedbání	k1gNnSc4	zanedbání
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
údržby	údržba	k1gFnSc2	údržba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
zápisu	zápis	k1gInSc2	zápis
v	v	k7c6	v
rodokmenu	rodokmen	k1gInSc6	rodokmen
Schnirchovy	Schnirchův	k2eAgFnSc2d1	Schnirchova
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
stavby	stavba	k1gFnSc2	stavba
mostu	most	k1gInSc2	most
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
rovněž	rovněž	k9	rovněž
Schnirchův	Schnirchův	k2eAgMnSc1d1	Schnirchův
synovec	synovec	k1gMnSc1	synovec
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Josef	Josef	k1gMnSc1	Josef
Emanuel	Emanuel	k1gMnSc1	Emanuel
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Bedřich	Bedřich	k1gMnSc1	Bedřich
Schnirch	Schnirch	k1gMnSc1	Schnirch
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
zaměstnával	zaměstnávat	k5eAaImAgMnS	zaměstnávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Most	most	k1gInSc1	most
přes	přes	k7c4	přes
Ohři	Ohře	k1gFnSc4	Ohře
===	===	k?	===
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
řetězového	řetězový	k2eAgInSc2d1	řetězový
mostu	most	k1gInSc2	most
přes	přes	k7c4	přes
Ohři	Ohře	k1gFnSc4	Ohře
u	u	k7c2	u
Žatce	Žatec	k1gInSc2	Žatec
byl	být	k5eAaImAgInS	být
položen	položen	k2eAgInSc1d1	položen
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1826	[number]	k4	1826
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1827	[number]	k4	1827
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pamětní	pamětní	k2eAgFnSc2d1	pamětní
desky	deska	k1gFnSc2	deska
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
zdárnou	zdárný	k2eAgFnSc4d1	zdárná
výstavbu	výstavba	k1gFnSc4	výstavba
řetězového	řetězový	k2eAgInSc2d1	řetězový
mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
purkrabí	purkrabí	k1gMnSc1	purkrabí
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
hrabě	hrabě	k1gMnSc1	hrabě
František	František	k1gMnSc1	František
Kolovrat	kolovrat	k1gInSc4	kolovrat
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
královské	královský	k2eAgNnSc1d1	královské
město	město	k1gNnSc1	město
Žatec	Žatec	k1gInSc1	Žatec
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgMnSc1d1	stavební
ředitel	ředitel	k1gMnSc1	ředitel
Pavel	Pavel	k1gMnSc1	Pavel
Strobach	Strobach	k1gMnSc1	Strobach
a	a	k8xC	a
železárny	železárna	k1gFnPc1	železárna
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
<g/>
.	.	kIx.	.
</s>
<s>
Řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
měl	mít	k5eAaImAgInS	mít
délku	délka	k1gFnSc4	délka
64	[number]	k4	64
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
šíři	šíř	k1gFnSc6	šíř
59,72	[number]	k4	59,72
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
i	i	k8xC	i
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
zděné	zděný	k2eAgFnPc1d1	zděná
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yIgMnPc7	který
byla	být	k5eAaImAgFnS	být
natažena	natažen	k2eAgFnSc1d1	natažena
trojice	trojice	k1gFnSc1	trojice
řetězů	řetěz	k1gInPc2	řetěz
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
i	i	k8xC	i
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgFnPc4	ten
nesly	nést	k5eAaImAgFnP	nést
5,55	[number]	k4	5,55
metru	metr	k1gInSc2	metr
širokou	široký	k2eAgFnSc4d1	široká
mostovku	mostovka	k1gFnSc4	mostovka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
řetězy	řetěz	k1gInPc1	řetěz
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
plochých	plochý	k2eAgInPc2d1	plochý
železných	železný	k2eAgInPc2d1	železný
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Josef	Josef	k1gMnSc1	Josef
Božek	Božek	k1gMnSc1	Božek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
důkladné	důkladný	k2eAgFnSc6d1	důkladná
technické	technický	k2eAgFnSc6d1	technická
kontrole	kontrola	k1gFnSc6	kontrola
zjištěn	zjistit	k5eAaPmNgInS	zjistit
nepříznivý	příznivý	k2eNgInSc1d1	nepříznivý
stav	stav	k1gInSc1	stav
řetězů	řetěz	k1gInPc2	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
zdivu	zdivo	k1gNnSc6	zdivo
pod	pod	k7c7	pod
chodníkem	chodník	k1gInSc7	chodník
<g/>
,	,	kIx,	,
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
masivní	masivní	k2eAgFnSc4d1	masivní
korozi	koroze	k1gFnSc4	koroze
a	a	k8xC	a
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
zbourán	zbourat	k5eAaPmNgInS	zbourat
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
mostem	most	k1gInSc7	most
ocelovým	ocelový	k2eAgInSc7d1	ocelový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Most	most	k1gInSc1	most
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
===	===	k?	===
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Most	most	k1gInSc1	most
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
po	po	k7c6	po
Karlově	Karlův	k2eAgInSc6d1	Karlův
mostě	most	k1gInSc6	most
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
teprve	teprve	k9	teprve
druhým	druhý	k4xOgInSc7	druhý
trvalým	trvalý	k2eAgInSc7d1	trvalý
mostem	most	k1gInSc7	most
přes	přes	k7c4	přes
Vltavu	Vltava	k1gFnSc4	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
u	u	k7c2	u
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
stála	stát	k5eAaImAgFnS	stát
přibližně	přibližně	k6eAd1	přibližně
330	[number]	k4	330
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
Schnirchových	Schnirchův	k2eAgInPc2d1	Schnirchův
řetězových	řetězový	k2eAgInPc2d1	řetězový
mostů	most	k1gInPc2	most
byl	být	k5eAaImAgInS	být
největší	veliký	k2eAgInSc1d3	veliký
<g/>
:	:	kIx,	:
délka	délka	k1gFnSc1	délka
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
413	[number]	k4	413
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
mezi	mezi	k7c7	mezi
zábradlím	zábradlí	k1gNnSc7	zábradlí
byla	být	k5eAaImAgFnS	být
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc4	dva
podpíral	podpírat	k5eAaImAgInS	podpírat
mohutný	mohutný	k2eAgInSc1d1	mohutný
prostřední	prostřední	k2eAgInSc1d1	prostřední
pilíř	pilíř	k1gInSc1	pilíř
na	na	k7c6	na
Střeleckém	střelecký	k2eAgInSc6d1	střelecký
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
dřevěném	dřevěný	k2eAgInSc6d1	dřevěný
roštu	rošt	k1gInSc6	rošt
a	a	k8xC	a
na	na	k7c6	na
156	[number]	k4	156
dřevěných	dřevěný	k2eAgFnPc6d1	dřevěná
pilotách	pilota	k1gFnPc6	pilota
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
mostě	most	k1gInSc6	most
ukončen	ukončit	k5eAaPmNgInS	ukončit
a	a	k8xC	a
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c4	na
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
provizorium	provizorium	k1gNnSc4	provizorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stádlecký	Stádlecký	k2eAgInSc1d1	Stádlecký
most	most	k1gInSc1	most
===	===	k?	===
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Stádlecký	Stádlecký	k2eAgInSc4d1	Stádlecký
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
dal	dát	k5eAaPmAgInS	dát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1847	[number]	k4	1847
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
postavit	postavit	k5eAaPmF	postavit
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Schnircha	Schnirch	k1gMnSc2	Schnirch
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc4	Josef
Gerstnera	Gerstner	k1gMnSc4	Gerstner
českobudějovický	českobudějovický	k2eAgMnSc1d1	českobudějovický
podnikatel	podnikatel	k1gMnSc1	podnikatel
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Lanna	Lanna	k1gFnSc1	Lanna
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
vedl	vést	k5eAaImAgMnS	vést
přes	přes	k7c4	přes
Vltavu	Vltava	k1gFnSc4	Vltava
v	v	k7c6	v
Podolsku	Podolsk	k1gInSc6	Podolsk
(	(	kIx(	(
<g/>
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
z	z	k7c2	z
Tábora	Tábor	k1gInSc2	Tábor
do	do	k7c2	do
Písku	Písek	k1gInSc2	Písek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
objemu	objem	k1gInSc3	objem
dopravy	doprava	k1gFnSc2	doprava
jej	on	k3xPp3gNnSc2	on
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nahradil	nahradit	k5eAaPmAgMnS	nahradit
železobetonový	železobetonový	k2eAgInSc4d1	železobetonový
Podolský	podolský	k2eAgInSc4d1	podolský
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
mosty	most	k1gInPc1	most
zde	zde	k6eAd1	zde
existovaly	existovat	k5eAaImAgInP	existovat
společně	společně	k6eAd1	společně
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
rozebrán	rozebrat	k5eAaPmNgInS	rozebrat
a	a	k8xC	a
přemístěn	přemístit	k5eAaPmNgInS	přemístit
ke	k	k7c3	k
Stádlci	Stádlec	k1gInSc3	Stádlec
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Lužnice	Lužnice	k1gFnSc2	Lužnice
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
obnoveného	obnovený	k2eAgInSc2d1	obnovený
Stádleckého	Stádlecký	k2eAgInSc2d1	Stádlecký
mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
mosty	most	k1gInPc1	most
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Schnirchových	Schnirchův	k2eAgInPc2d1	Schnirchův
plánů	plán	k1gInPc2	plán
stavěl	stavět	k5eAaImAgInS	stavět
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
ve	v	k7c6	v
Strakonicích	Strakonice	k1gFnPc6	Strakonice
přes	přes	k7c4	přes
Otavu	Otava	k1gFnSc4	Otava
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
42	[number]	k4	42
m	m	kA	m
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
řetězový	řetězový	k2eAgInSc4d1	řetězový
most	most	k1gInSc4	most
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
101	[number]	k4	101
m.	m.	k?	m.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
následoval	následovat	k5eAaImAgInS	následovat
řetězový	řetězový	k2eAgInSc1d1	řetězový
most	most	k1gInSc1	most
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
41	[number]	k4	41
m	m	kA	m
u	u	k7c2	u
Jaroměře	Jaroměř	k1gFnSc2	Jaroměř
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
přes	přes	k7c4	přes
Labe	Labe	k1gNnSc4	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
je	být	k5eAaImIp3nS	být
řetězový	řetězový	k2eAgInSc4d1	řetězový
most	most	k1gInSc4	most
v	v	k7c6	v
Postoloprtech	Postoloprt	k1gInPc6	Postoloprt
přes	přes	k7c4	přes
Ohři	Ohře	k1gFnSc4	Ohře
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
99	[number]	k4	99
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Řetězové	řetězový	k2eAgFnPc4d1	řetězová
střechy	střecha	k1gFnPc4	střecha
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
získal	získat	k5eAaPmAgInS	získat
Schnirch	Schnirch	k1gInSc4	Schnirch
výsadu	výsada	k1gFnSc4	výsada
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
střech	střecha	k1gFnPc2	střecha
z	z	k7c2	z
kujného	kujný	k2eAgNnSc2d1	kujné
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
visutých	visutý	k2eAgFnPc2d1	visutá
nebo	nebo	k8xC	nebo
zavěšených	zavěšený	k2eAgFnPc2d1	zavěšená
střech	střecha	k1gFnPc2	střecha
<g/>
,	,	kIx,	,
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
synovcem	synovec	k1gMnSc7	synovec
Ing.	ing.	kA	ing.
Josefem	Josef	k1gMnSc7	Josef
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Schnirchem	Schnirch	k1gMnSc7	Schnirch
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
jejich	jejich	k3xOp3gFnSc3	jejich
stavbě	stavba	k1gFnSc3	stavba
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Racionálním	racionální	k2eAgInSc7d1	racionální
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
řetězových	řetězový	k2eAgFnPc2d1	řetězová
střech	střecha	k1gFnPc2	střecha
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
omezení	omezení	k1gNnSc4	omezení
častých	častý	k2eAgInPc2d1	častý
požárů	požár	k1gInPc2	požár
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
původní	původní	k2eAgFnPc1d1	původní
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
střechy	střecha	k1gFnPc1	střecha
vykazovaly	vykazovat	k5eAaImAgFnP	vykazovat
proti	proti	k7c3	proti
vzniku	vznik	k1gInSc3	vznik
požáru	požár	k1gInSc2	požár
menší	malý	k2eAgFnSc4d2	menší
odolnost	odolnost	k1gFnSc4	odolnost
než	než	k8xS	než
střechy	střecha	k1gFnPc4	střecha
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
střecha	střecha	k1gFnSc1	střecha
domu	dům	k1gInSc2	dům
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Štefana	Štefan	k1gMnSc2	Štefan
Moyzesa	Moyzesa	k1gFnSc1	Moyzesa
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Střešní	střešní	k2eAgFnSc1d1	střešní
krytina	krytina	k1gFnSc1	krytina
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
klasickými	klasický	k2eAgFnPc7d1	klasická
eternitovými	eternitový	k2eAgFnPc7d1	eternitová
šablonami	šablona	k1gFnPc7	šablona
a	a	k8xC	a
půdní	půdní	k2eAgFnPc4d1	půdní
prostory	prostora	k1gFnPc4	prostora
řetězové	řetězový	k2eAgFnSc2d1	řetězová
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
jediné	jediné	k1gNnSc4	jediné
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
veřejnosti	veřejnost	k1gFnSc3	veřejnost
nepřístupné	přístupný	k2eNgFnSc3d1	nepřístupná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bedřich	Bedřich	k1gMnSc1	Bedřich
Schnirch	Schnircha	k1gFnPc2	Schnircha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
