<s>
Podprůhon	Podprůhon	k1gMnSc1
</s>
<s>
Pohlednice	pohlednice	k1gFnSc1
s	s	k7c7
motivem	motiv	k1gInSc7
Podprůhonu	Podprůhon	k1gInSc2
jako	jako	k8xS,k8xC
kladenského	kladenský	k2eAgInSc2d1
betlému	betlém	k1gInSc2
</s>
<s>
Kladno	Kladno	k1gNnSc1
z	z	k7c2
letadla	letadlo	k1gNnSc2
ze	z	k7c2
západu	západ	k1gInSc2
<g/>
,	,	kIx,
dole	dole	k6eAd1
Podprůhon	Podprůhon	k1gInSc1
pod	pod	k7c7
náměstím	náměstí	k1gNnSc7
Svobody	svoboda	k1gFnSc2
</s>
<s>
portrét	portrét	k1gInSc4
břevnovského	břevnovský	k2eAgMnSc2d1
opata	opat	k1gMnSc2
Bedřicha	Bedřich	k1gMnSc2
Grundmanna	Grundmann	k1gMnSc2
</s>
<s>
Podprůhon	Podprůhon	k1gInSc1
je	být	k5eAaImIp3nS
historická	historický	k2eAgFnSc1d1
část	část	k1gFnSc1
města	město	k1gNnSc2
Kladna	Kladno	k1gNnSc2
s	s	k7c7
dělnickými	dělnický	k2eAgInPc7d1
domky	domek	k1gInPc7
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
nazývaná	nazývaný	k2eAgFnSc1d1
také	také	k6eAd1
"	"	kIx"
<g/>
Podskalí	Podskalí	k1gNnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
svahu	svah	k1gInSc6
od	od	k7c2
Nového	Nového	k2eAgNnSc2d1
Kladna	Kladno	k1gNnSc2
<g/>
,	,	kIx,
pod	pod	k7c7
náměstím	náměstí	k1gNnSc7
Svobody	svoboda	k1gFnSc2
směrem	směr	k1gInSc7
k	k	k7c3
Ostrovci	Ostrovec	k1gMnSc3
a	a	k8xC
železniční	železniční	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
Kladno-Ostrovec	Kladno-Ostrovec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisovatelka	spisovatelka	k1gFnSc1
Marie	Marie	k1gFnSc1
Majerová	Majerová	k1gFnSc1
v	v	k7c6
knize	kniha	k1gFnSc6
Má	můj	k3xOp1gFnSc1
vlast	vlast	k1gFnSc1
píše	psát	k5eAaImIp3nS
o	o	k7c6
Podprůhonu	Podprůhon	k1gInSc6
jako	jako	k8xC,k8xS
o	o	k7c6
"	"	kIx"
<g/>
kladenském	kladenský	k2eAgInSc6d1
betlému	betlém	k1gInSc6
<g/>
"	"	kIx"
<g/>
,	,	kIx,
František	František	k1gMnSc1
Stavinoha	Stavinoha	k1gMnSc1
umístil	umístit	k5eAaPmAgMnS
do	do	k7c2
Podprůhonu	Podprůhon	k1gInSc2
děj	děj	k1gInSc1
své	svůj	k3xOyFgFnSc2
povídkové	povídkový	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
vůbec	vůbec	k9
nejstarší	starý	k2eAgFnSc4d3
část	část	k1gFnSc4
Kladna	Kladno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1769	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
majetkem	majetek	k1gInSc7
řádu	řád	k1gInSc2
benediktinů	benediktin	k1gMnPc2
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
opat	opat	k1gMnSc1
broumovského	broumovský	k2eAgInSc2d1
a	a	k8xC
břevnovského	břevnovský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
Bedřich	Bedřich	k1gMnSc1
Grundtmann	Grundtmann	k1gMnSc1
svolení	svolení	k1gNnSc4
k	k	k7c3
výstavbě	výstavba	k1gFnSc3
osmi	osm	k4xCc2
domů	dům	k1gInPc2
v	v	k7c6
tzv.	tzv.	kA
Bukovce	bukovka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severním	severní	k2eAgInSc6d1
svahu	svah	k1gInSc6
pod	pod	k7c7
tehdejším	tehdejší	k2eAgNnSc7d1
Kladnem	Kladno	k1gNnSc7
vznikla	vzniknout	k5eAaPmAgFnS
rostlá	rostlý	k2eAgFnSc1d1
část	část	k1gFnSc1
města	město	k1gNnSc2
z	z	k7c2
domků	domek	k1gInPc2
s	s	k7c7
malým	malý	k2eAgInSc7d1
dvorkem	dvorek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1864	#num#	k4
začíná	začínat	k5eAaImIp3nS
těžba	těžba	k1gFnSc1
v	v	k7c6
dole	dol	k1gInSc6
Průhon	průhon	k1gInSc1
<g/>
,	,	kIx,
lidově	lidově	k6eAd1
též	též	k9
známém	známý	k2eAgInSc6d1
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
Lesík	lesík	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
těžilo	těžit	k5eAaImAgNnS
se	se	k3xPyFc4
zde	zde	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1891	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
uspořádali	uspořádat	k5eAaPmAgMnP
umělci	umělec	k1gMnPc1
na	na	k7c6
zahradě	zahrada	k1gFnSc6
Viktora	Viktor	k1gMnSc2
Stříbrného	stříbrný	k1gInSc2
soukromou	soukromý	k2eAgFnSc4d1
výstavu	výstava	k1gFnSc4
"	"	kIx"
<g/>
Setkání	setkání	k1gNnSc1
v	v	k7c6
zahradě	zahrada	k1gFnSc6
<g/>
"	"	kIx"
na	na	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
navázali	navázat	k5eAaPmAgMnP
prvním	první	k4xOgInSc7
ročníkem	ročník	k1gInSc7
výstavy	výstava	k1gFnSc2
"	"	kIx"
<g/>
Kladenské	kladenský	k2eAgInPc1d1
dvorky	dvorek	k1gInPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Kladenské	kladenský	k2eAgInPc4d1
dvorky	dvorek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Podprůhonu	Podprůhon	k1gInSc6
působí	působit	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
Spolek	spolek	k1gInSc1
Podprůhon	Podprůhon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
je	být	k5eAaImIp3nS
také	také	k9
pořadatelem	pořadatel	k1gMnSc7
kulturní	kulturní	k2eAgFnSc2d1
akce	akce	k1gFnSc2
Kladenské	kladenský	k2eAgInPc4d1
dvorky	dvorek	k1gInPc4
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
jsou	být	k5eAaImIp3nP
každoročně	každoročně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1983	#num#	k4
otevřené	otevřený	k2eAgFnPc4d1
desítky	desítka	k1gFnPc4
dvorků	dvorek	k1gInPc2
podprůhonských	podprůhonský	k2eAgInPc2d1
domů	dům	k1gInPc2
a	a	k8xC
v	v	k7c6
každém	každý	k3xTgNnSc6
z	z	k7c2
nich	on	k3xPp3gNnPc2
jsou	být	k5eAaImIp3nP
vystavena	vystaven	k2eAgNnPc1d1
umělecká	umělecký	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
<g/>
,	,	kIx,
večery	večer	k1gInPc1
pak	pak	k6eAd1
patří	patřit	k5eAaImIp3nP
koncertům	koncert	k1gInPc3
na	na	k7c6
hřišti	hřiště	k1gNnSc6
Bukovka	bukovka	k1gFnSc1
nebo	nebo	k8xC
v	v	k7c6
restauraci	restaurace	k1gFnSc6
U	u	k7c2
Jedličků	Jedlička	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
stovkami	stovka	k1gFnPc7
vystavujících	vystavující	k2eAgMnPc2d1
výtvarníků	výtvarník	k1gMnPc2
se	se	k3xPyFc4
objevili	objevit	k5eAaPmAgMnP
také	také	k9
renomovaní	renomovaný	k2eAgMnPc1d1
výtvarníci	výtvarník	k1gMnPc1
jako	jako	k9
například	například	k6eAd1
sestry	sestra	k1gFnPc1
Jitka	Jitka	k1gFnSc1
a	a	k8xC
Květa	Květa	k1gFnSc1
Válová	Válová	k1gFnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Kokolia	Kokolia	k1gFnSc1
<g/>
,	,	kIx,
Olbram	Olbram	k1gInSc1
Zoubek	zoubek	k1gInSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
Veselý	Veselý	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
zakladatele	zakladatel	k1gMnPc4
výstavy	výstava	k1gFnSc2
patří	patřit	k5eAaImIp3nS
především	především	k9
představitelé	představitel	k1gMnPc1
výtvarné	výtvarný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Atelier	atelier	k1gNnSc1
74	#num#	k4
Viktor	Viktor	k1gMnSc1
Stříbrný	stříbrný	k1gInSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
Tichá	Tichá	k1gFnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Frolík	Frolík	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Černý	Černý	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Hanke	Hank	k1gFnSc2
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Vejvoda	Vejvoda	k1gMnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Gratzová	Gratzová	k1gFnSc1
a	a	k8xC
František	František	k1gMnSc1
Tomík	Tomík	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Kestřánek	Kestřánek	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
Garamszegi	Garamszeg	k1gFnSc2
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Kubový	Kubový	k2eAgMnSc1d1
a	a	k8xC
další	další	k2eAgMnPc1d1
výtvarníci	výtvarník	k1gMnPc1
především	především	k6eAd1
z	z	k7c2
Kladna	Kladno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Podprůhonu	Podprůhon	k1gInSc6
natočený	natočený	k2eAgInSc4d1
film	film	k1gInSc4
(	(	kIx(
<g/>
trilogie	trilogie	k1gFnSc1
<g/>
)	)	kIx)
"	"	kIx"
<g/>
Hvězdy	hvězda	k1gFnPc1
nad	nad	k7c7
Syslím	syslí	k2eAgNnSc7d1
údolím	údolí	k1gNnSc7
<g/>
"	"	kIx"
podle	podle	k7c2
knihy	kniha	k1gFnSc2
Františka	František	k1gMnSc2
Stavinohy	Stavinoh	k1gMnPc7
byl	být	k5eAaImAgInS
dokončený	dokončený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
vernisáži	vernisáž	k1gFnSc6
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladenských	kladenský	k2eAgInPc2d1
dvorků	dvorek	k1gInPc2
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
základě	základ	k1gInSc6
sbírky	sbírka	k1gFnSc2
Slunce	slunce	k1gNnSc2
pro	pro	k7c4
Podprůhon	Podprůhon	k1gInSc4
odhalena	odhalit	k5eAaPmNgFnS
replika	replika	k1gFnSc1
plastiky	plastika	k1gFnSc2
Slunce	slunce	k1gNnSc2
Viktora	Viktor	k1gMnSc2
Stříbrného	stříbrný	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
města	město	k1gNnSc2
odsouhlasila	odsouhlasit	k5eAaPmAgFnS
pojmenování	pojmenování	k1gNnSc4
návsi	náves	k1gFnSc2
v	v	k7c6
křižovatce	křižovatka	k1gFnSc6
mezi	mezi	k7c7
ulicemi	ulice	k1gFnPc7
Kolmistrova	Kolmistrův	k2eAgMnSc2d1
a	a	k8xC
V.	V.	kA
Burgra	Burgra	k1gFnSc1
jako	jako	k8xS,k8xC
Náměstíčko	náměstíčko	k1gNnSc1
Viktora	Viktor	k1gMnSc2
Stříbrného	stříbrný	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladenský	kladenský	k2eAgMnSc1d1
umělecký	umělecký	k2eAgMnSc1d1
kovář	kovář	k1gMnSc1
<g/>
,	,	kIx,
malíř	malíř	k1gMnSc1
a	a	k8xC
spoluzakladatel	spoluzakladatel	k1gMnSc1
Kladenských	kladenský	k2eAgInPc2d1
dvorků	dvorek	k1gInPc2
bydlel	bydlet	k5eAaImAgMnS
opodál	opodál	k6eAd1
v	v	k7c6
ulici	ulice	k1gFnSc6
Komistrova	Komistrův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
(	(	kIx(
<g/>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladenské	kladenský	k2eAgInPc4d1
dvorky	dvorek	k1gInPc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
odhalena	odhalit	k5eAaPmNgFnS
pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
kladenského	kladenský	k2eAgMnSc2d1
prozaika	prozaik	k1gMnSc2
Františka	František	k1gMnSc2
Stavinohy	Stavinoh	k1gInPc1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
domě	dům	k1gInSc6
čp.	čp.	k?
214	#num#	k4
v	v	k7c6
Bukovské	Bukovské	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zde	zde	k6eAd1
žil	žít	k5eAaImAgMnS
mezi	mezi	k7c4
roky	rok	k1gInPc4
1975	#num#	k4
a	a	k8xC
1985	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
povídkové	povídkový	k2eAgFnSc6d1
sbírce	sbírka	k1gFnSc6
Hvězdy	hvězda	k1gFnSc2
nad	nad	k7c7
Syslím	syslí	k2eAgNnSc7d1
údolím	údolí	k1gNnSc7
z	z	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
vykreslil	vykreslit	k5eAaPmAgInS
postavičky	postavička	k1gFnSc2
této	tento	k3xDgFnSc2
svérázné	svérázný	k2eAgFnSc2d1
čtvrti	čtvrt	k1gFnSc2
<g/>
,	,	kIx,
desku	deska	k1gFnSc4
navrhl	navrhnout	k5eAaPmAgMnS
grafik	grafik	k1gMnSc1
František	František	k1gMnSc1
Tomík	Tomík	k1gMnSc1
a	a	k8xC
zrealizoval	zrealizovat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
Spolek	spolek	k1gInSc1
Podprůhon	Podprůhona	k1gFnPc2
za	za	k7c4
finanční	finanční	k2eAgFnPc4d1
podpory	podpora	k1gFnPc4
kladenského	kladenský	k2eAgInSc2d1
magistrátu	magistrát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
zdobí	zdobit	k5eAaImIp3nP
opravený	opravený	k2eAgInSc4d1
záhon	záhon	k1gInSc4
naproti	naproti	k7c3
hospodě	hospodě	k?
U	u	k7c2
kulatý	kulatý	k2eAgInSc1d1
báby	bába	k1gFnSc2
socha	socha	k1gFnSc1
Poletíme	poletět	k5eAaPmIp1nP,k5eAaImIp1nP
III	III	kA
od	od	k7c2
akademického	akademický	k2eAgMnSc2d1
sochaře	sochař	k1gMnSc2
Zdeňka	Zdeněk	k1gMnSc2
Maniny	manina	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
Podprůhonu	Podprůhon	k1gInSc6
ateliér	ateliér	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MAJEROVÁ	Majerová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
vlast	vlast	k1gFnSc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československá	československý	k2eAgFnSc1d1
grafická	grafický	k2eAgFnSc1d1
Unie	unie	k1gFnSc1
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
.	.	kIx.
86	#num#	k4
s.	s.	k?
</s>
<s>
STAVINOHA	STAVINOHA	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdy	hvězda	k1gFnPc1
nad	nad	k7c7
Syslím	syslí	k2eAgNnSc7d1
údolím	údolí	k1gNnSc7
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
282	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
HANKE	HANKE	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
z	z	k7c2
Podprůhonu	Podprůhon	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
<g/>
:	:	kIx,
Okresní	okresní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Kladně	Kladno	k1gNnSc6
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
131	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901566	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GARAMSZEGI	GARAMSZEGI	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladenské	kladenský	k2eAgInPc1d1
dvorky	dvorek	k1gInPc1
1983	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
<g/>
:	:	kIx,
Klubko	klubko	k1gNnSc1
55	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VYKOUK	VYKOUK	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladenský	kladenský	k2eAgInSc1d1
tulák	tulák	k1gMnSc1
:	:	kIx,
pověsti	pověst	k1gFnPc1
<g/>
,	,	kIx,
příběhy	příběh	k1gInPc1
a	a	k8xC
zajímavosti	zajímavost	k1gFnSc2
o	o	k7c6
původu	původ	k1gInSc6
jmen	jméno	k1gNnPc2
čtvrtí	čtvrtit	k5eAaImIp3nP
<g/>
,	,	kIx,
částí	část	k1gFnSc7
města	město	k1gNnSc2
a	a	k8xC
místních	místní	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
v	v	k7c6
Kladně	Kladno	k1gNnSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
<g/>
:	:	kIx,
Sládečkovo	Sládečkův	k2eAgNnSc1d1
vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Kladně	Kladno	k1gNnSc6
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
181	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903784	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SEIFERT	Seifert	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
:	:	kIx,
doteky	dotek	k1gInPc1
času	čas	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
<g/>
:	:	kIx,
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Kladno	Kladno	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
207	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
-	-	kIx~
<g/>
8154	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERGL	PERGL	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
PAZDERKA	pazderka	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladenské	kladenský	k2eAgInPc1d1
dvorky	dvorek	k1gInPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
<g/>
:	:	kIx,
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Kladno	Kladno	k1gNnSc1
<g/>
;	;	kIx,
Spolek	spolek	k1gInSc1
Podprůhon	Podprůhona	k1gFnPc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
150	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
260	#num#	k4
<g/>
-	-	kIx~
<g/>
3470	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kladenské	kladenský	k2eAgInPc1d1
dvorky	dvorek	k1gInPc1
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
<g/>
:	:	kIx,
Halda	halda	k1gFnSc1
<g/>
;	;	kIx,
Spolek	spolek	k1gInSc1
Podprůhon	Podprůhona	k1gFnPc2
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
68	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905992	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
POSPÍŠIL	Pospíšil	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průvodce	průvodce	k1gMnSc1
historií	historie	k1gFnSc7
kladenských	kladenský	k2eAgMnPc2d1
hostinců	hostinec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
II	II	kA
<g/>
..	..	k?
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
<g/>
:	:	kIx,
Halda	halda	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
263	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905992	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
POSPÍŠIL	Pospíšil	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
;	;	kIx,
BAĎURA	Baďura	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
HÁJEK	Hájek	k1gMnSc1
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paměť	paměť	k1gFnSc1
Podprůhonu	Podprůhon	k1gInSc2
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
<g/>
,	,	kIx,
příběhy	příběh	k1gInPc1
a	a	k8xC
lidé	člověk	k1gMnPc1
kladenské	kladenský	k2eAgFnSc2d1
dělnické	dělnický	k2eAgFnSc2d1
čtvrti	čtvrt	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladno	Kladno	k1gNnSc1
<g/>
:	:	kIx,
Halda	halda	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
264	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
907236	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Hvězdy	hvězda	k1gFnPc1
nad	nad	k7c7
Syslím	syslí	k2eAgNnSc7d1
údolím	údolí	k1gNnSc7
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
O	o	k7c6
krásných	krásný	k2eAgFnPc6d1
nožkách	nožka	k1gFnPc6
Anduly	Andula	k1gFnSc2
Bláhové	Bláhová	k1gFnSc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Životní	životní	k2eAgInSc1d1
román	román	k1gInSc1
Mudr.	Mudr.	k1gFnSc2
Diany	Diana	k1gFnSc2
Filipové	Filipová	k1gFnSc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1
případ	případ	k1gInSc1
Josefa	Josef	k1gMnSc2
Satrana	Satran	k1gMnSc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Fotografie	fotografia	k1gFnPc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Hanke	Hank	k1gMnSc2
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
z	z	k7c2
Podprůhonu	Podprůhon	k1gInSc2
<g/>
,	,	kIx,
sociálně-dokumentární	sociálně-dokumentární	k2eAgInSc1d1
soubor	soubor	k1gInSc1
<g/>
,	,	kIx,
výstava	výstava	k1gFnSc1
1984	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Podprůhon	Podprůhona	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Podprůhon	Podprůhon	k1gNnSc1
</s>
<s>
Podpruhon	Podpruhon	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Spolek	spolek	k1gInSc1
Podprůhon	Podprůhona	k1gFnPc2
</s>
<s>
Kladenskedvorky	Kladenskedvorka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Kladnominule	Kladnominule	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Podprůhon	Podprůhon	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
130458	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kladno	Kladno	k1gNnSc1
</s>
