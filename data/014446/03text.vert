<s>
Říp	Říp	k1gInSc1
</s>
<s>
Říp	Říp	k1gInSc1
Hora	Hora	k1gMnSc1
Říp	Říp	k1gInSc1
od	od	k7c2
jihu	jih	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
461	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
240	#num#	k4
m	m	kA
↓	↓	k?
zastávka	zastávka	k1gFnSc1
Loucká	Loucký	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Izolace	izolace	k1gFnPc1
</s>
<s>
21,5	21,5	k4
km	km	kA
→	→	k?
Nedvězí	Nedvěhý	k2eAgMnPc1d1
Seznamy	seznam	k1gInPc1
</s>
<s>
Nejprominentnější	prominentní	k2eAgFnPc1d3
hory	hora	k1gFnPc1
CZ	CZ	kA
Poznámka	poznámka	k1gFnSc1
</s>
<s>
památné	památný	k2eAgNnSc1d1
místo	místo	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Česká	český	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
/	/	kIx~
Středočeská	středočeský	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
/	/	kIx~
Dolnooharská	Dolnooharský	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
/	/	kIx~
Řipská	řipský	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
/	/	kIx~
Krabčická	Krabčický	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
/	/	kIx~
Hornobeřkovická	Hornobeřkovický	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Říp	Říp	k1gInSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
čedič	čedič	k1gInSc1
<g/>
,	,	kIx,
olivín	olivín	k1gInSc1
<g/>
,	,	kIx,
magnetovec	magnetovec	k1gInSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Čepel	čepel	k1gFnSc1
→	→	k?
Ohře	Ohře	k1gFnSc1
→	→	k?
Labe	Labe	k1gNnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaHora	památkaHor	k1gMnSc2
Říp	Říp	k1gInSc4
pěšina	pěšina	k1gFnSc1
na	na	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
svahuZákladní	svahuZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Krajský	krajský	k2eAgInSc4d1
úřad	úřad	k1gInSc4
Ústeckého	ústecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
232	#num#	k4
<g/>
–	–	k?
<g/>
461	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
81,397	81,397	k4
<g/>
4	#num#	k4
ha	ha	kA
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Litoměřice	Litoměřice	k1gInPc1
Další	další	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Kód	kód	k1gInSc1
</s>
<s>
5745	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Hora	hora	k1gFnSc1
Říp	Říp	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Sankt	Sankt	k2eAgInSc1d1
Georgsberg	Georgsberg	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
460,8	460,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
již	již	k6eAd1
z	z	k7c2
daleka	daleko	k1gNnSc2
viditelný	viditelný	k2eAgInSc1d1
a	a	k8xC
výrazný	výrazný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
čtyři	čtyři	k4xCgInPc4
kilometry	kilometr	k1gInPc4
jižně	jižně	k6eAd1
od	od	k7c2
Roudnice	Roudnice	k1gFnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
udávaná	udávaný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
455	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
je	být	k5eAaImIp3nS
výškou	výška	k1gFnSc7
geodetického	geodetický	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
,	,	kIx,
ne	ne	k9
nejvyšším	vysoký	k2eAgNnSc7d3
místem	místo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
vrchol	vrchol	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Mnetěš	Mnetěš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
Řípu	Říp	k1gInSc2
s	s	k7c7
románskou	románský	k2eAgFnSc7d1
rotundou	rotunda	k1gFnSc7
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
vystupuje	vystupovat	k5eAaImIp3nS
asi	asi	k9
200	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c4
okolní	okolní	k2eAgFnSc4d1
plochou	plochý	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říp	Říp	k1gInSc1
je	být	k5eAaImIp3nS
památné	památný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
české	český	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
a	a	k8xC
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pověsti	pověst	k1gFnSc2
sem	sem	k6eAd1
přišel	přijít	k5eAaPmAgMnS
praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
přehlédl	přehlédnout	k5eAaPmAgMnS
okolní	okolní	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
usadit	usadit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kopec	kopec	k1gInSc1
Říp	Říp	k1gInSc1
s	s	k7c7
rotundou	rotunda	k1gFnSc7
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xS,k8xC
národní	národní	k2eAgFnSc1d1
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Historie	historie	k1gFnSc1
sopečné	sopečný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Geologie	geologie	k1gFnSc1
a	a	k8xC
geomorfologie	geomorfologie	k1gFnSc1
</s>
<s>
Typický	typický	k2eAgInSc1d1
zvonovitý	zvonovitý	k2eAgInSc1d1
tvar	tvar	k1gInSc1
Řípu	Říp	k1gInSc2
má	mít	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Temeno	temeno	k1gNnSc1
a	a	k8xC
boky	bok	k1gInPc1
hory	hora	k1gFnSc2
(	(	kIx(
<g/>
střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
pevný	pevný	k2eAgInSc1d1
sloup	sloup	k1gInSc1
třetihorního	třetihorní	k2eAgInSc2d1
čediče	čedič	k1gInSc2
<g/>
,	,	kIx,
vlastně	vlastně	k9
průnikový	průnikový	k2eAgInSc4d1
komín	komín	k1gInSc4
původní	původní	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
úpatí	úpatí	k1gNnSc1
obklopuje	obklopovat	k5eAaImIp3nS
čtvrtohorní	čtvrtohorní	k2eAgFnSc1d1
obruba	obruba	k1gFnSc1
z	z	k7c2
volně	volně	k6eAd1
nakupených	nakupený	k2eAgInPc2d1
čedičových	čedičový	k2eAgInPc2d1
balvanů	balvan	k1gInPc2
<g/>
,	,	kIx,
úlomků	úlomek	k1gInPc2
<g/>
,	,	kIx,
drti	drť	k1gFnSc2
a	a	k8xC
z	z	k7c2
navátých	navátý	k2eAgInPc2d1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
křemenných	křemenný	k2eAgInPc2d1
písků	písek	k1gInPc2
a	a	k8xC
vápnité	vápnitý	k2eAgFnPc1d1
spraše	spraš	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
bylo	být	k5eAaImAgNnS
ověřeno	ověřit	k5eAaPmNgNnS
vrtem	vrt	k1gInSc7
<g/>
,	,	kIx,
mocnost	mocnost	k1gFnSc1
obruby	obruba	k1gFnSc2
na	na	k7c6
úpatí	úpatí	k1gNnSc6
hory	hora	k1gFnSc2
přesahuje	přesahovat	k5eAaImIp3nS
sedmnáct	sedmnáct	k4xCc4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobnou	podobný	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
má	mít	k5eAaImIp3nS
značná	značný	k2eAgFnSc1d1
část	část	k1gFnSc1
bývalých	bývalý	k2eAgFnPc2d1
sopek	sopka	k1gFnPc2
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svým	svůj	k3xOyFgInSc7
tvarem	tvar	k1gInSc7
je	být	k5eAaImIp3nS
Říp	Říp	k1gInSc1
stolová	stolový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
s	s	k7c7
vrcholovou	vrcholový	k2eAgFnSc7d1
plošinou	plošina	k1gFnSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
elipsy	elipsa	k1gFnSc2
s	s	k7c7
delší	dlouhý	k2eAgFnSc7d2
osou	osa	k1gFnSc7
ve	v	k7c6
směru	směr	k1gInSc6
S-J	S-J	k1gFnSc4
dlouhou	dlouhý	k2eAgFnSc4d1
850	#num#	k4
m	m	kA
a	a	k8xC
kratší	krátký	k2eAgFnSc7d2
osou	osa	k1gFnSc7
500	#num#	k4
m.	m.	k?
Jde	jít	k5eAaImIp3nS
o	o	k7c4
pozůstatek	pozůstatek	k1gInSc4
jádra	jádro	k1gNnSc2
třetihorního	třetihorní	k2eAgInSc2d1
vulkánu	vulkán	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
tyčil	tyčit	k5eAaImAgInS
do	do	k7c2
výšky	výška	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
1	#num#	k4
km	km	kA
nad	nad	k7c4
úroveň	úroveň	k1gFnSc4
dnešního	dnešní	k2eAgInSc2d1
vrcholu	vrchol	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Říp	Říp	k1gInSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
již	již	k6eAd1
značně	značně	k6eAd1
snížen	snížit	k5eAaPmNgInS
zvětráváním	zvětrávání	k1gNnSc7
<g/>
,	,	kIx,
se	s	k7c7
silnými	silný	k2eAgFnPc7d1
suťovými	suťový	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
na	na	k7c6
úpatí	úpatí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Magnetické	magnetický	k2eAgFnPc4d1
anomálie	anomálie	k1gFnPc4
</s>
<s>
Zdejší	zdejší	k2eAgInPc1d1
sodaliticko	sodaliticko	k6eAd1
nefelínický	felínický	k2eNgInSc1d1
čedič	čedič	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
kromě	kromě	k7c2
olivínu	olivín	k1gInSc2
i	i	k9
magnetovec	magnetovec	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
způsobuje	způsobovat	k5eAaImIp3nS
lokální	lokální	k2eAgFnPc4d1
magnetické	magnetický	k2eAgFnPc4d1
anomálie	anomálie	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
výchylkami	výchylka	k1gFnPc7
střelky	střelka	k1gFnSc2
kompasu	kompas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Prameny	pramen	k1gInPc1
</s>
<s>
Na	na	k7c6
úpatí	úpatí	k1gNnSc6
Řípu	Říp	k1gInSc2
vyvěrají	vyvěrat	k5eAaImIp3nP
tři	tři	k4xCgInPc4
prameny	pramen	k1gInPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
ten	ten	k3xDgInSc1
východní	východní	k2eAgInSc1d1
je	být	k5eAaImIp3nS
odjakživa	odjakživa	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
léčivý	léčivý	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Flora	Flora	k1gFnSc1
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1879	#num#	k4
byl	být	k5eAaImAgInS
Říp	Říp	k1gInSc1
bezlesý	bezlesý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc4d1
horu	hora	k1gFnSc4
pokrývá	pokrývat	k5eAaImIp3nS
dubohabrový	dubohabrový	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
;	;	kIx,
z	z	k7c2
dalších	další	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
javor	javor	k1gInSc1
<g/>
,	,	kIx,
borovice	borovice	k1gFnSc1
<g/>
,	,	kIx,
jasan	jasan	k1gInSc1
a	a	k8xC
lípa	lípa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
několika	několik	k4yIc2
holých	holý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
původní	původní	k2eAgFnSc1d1
vzácná	vzácný	k2eAgFnSc1d1
teplomilná	teplomilný	k2eAgFnSc1d1
květena	květena	k1gFnSc1
(	(	kIx(
<g/>
například	například	k6eAd1
zvonek	zvonek	k1gInSc1
boloňský	boloňský	k2eAgInSc1d1
<g/>
,	,	kIx,
křivatec	křivatec	k1gInSc1
český	český	k2eAgInSc1d1
pravý	pravý	k2eAgInSc1d1
a	a	k8xC
kosatec	kosatec	k1gInSc1
nízký	nízký	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
je	být	k5eAaImIp3nS
zalesněný	zalesněný	k2eAgInSc1d1
a	a	k8xC
bez	bez	k7c2
rozhledu	rozhled	k1gInSc2
<g/>
,	,	kIx,
dílčí	dílčí	k2eAgInPc1d1
pohledy	pohled	k1gInPc1
do	do	k7c2
širokého	široký	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
se	se	k3xPyFc4
nabízejí	nabízet	k5eAaImIp3nP
z	z	k7c2
několika	několik	k4yIc2
skalních	skalní	k2eAgFnPc2d1
vyhlídek	vyhlídka	k1gFnPc2
na	na	k7c6
úbočích	úbočí	k1gNnPc6
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ochrana	ochrana	k1gFnSc1
přírody	příroda	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Hora	Hora	k1gMnSc1
Říp	Říp	k1gInSc1
k	k	k7c3
ochraně	ochrana	k1gFnSc3
evropsky	evropsky	k6eAd1
významné	významný	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
kontinentálních	kontinentální	k2eAgFnPc2d1
opadavých	opadavý	k2eAgFnPc2d1
křovin	křovina	k1gFnPc2
<g/>
,	,	kIx,
skalních	skalní	k2eAgInPc2d1
trávníků	trávník	k1gInPc2
<g/>
,	,	kIx,
polopřirozených	polopřirozený	k2eAgInPc2d1
suchých	suchý	k2eAgInPc2d1
trávníků	trávník	k1gInPc2
a	a	k8xC
křovin	křovina	k1gFnPc2
na	na	k7c6
vápnitém	vápnitý	k2eAgNnSc6d1
podloží	podloží	k1gNnSc6
včetně	včetně	k7c2
v	v	k7c6
místě	místo	k1gNnSc6
se	se	k3xPyFc4
vyskytujících	vyskytující	k2eAgInPc2d1
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
(	(	kIx(
<g/>
roháč	roháč	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
přástevník	přástevník	k1gMnSc1
kostivalový	kostivalový	k2eAgMnSc1d1
nebo	nebo	k8xC
lišaj	lišaj	k1gMnSc1
pryšcový	pryšcový	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
mýty	mýtus	k1gInPc1
</s>
<s>
Říp	Říp	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
původní	původní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
bez	bez	k7c2
porostu	porost	k1gInSc2
–	–	k?
na	na	k7c6
ilustracích	ilustrace	k1gFnPc6
Karla	Karel	k1gMnSc2
Liebschera	Liebscher	k1gMnSc2
z	z	k7c2
doby	doba	k1gFnSc2
kolem	kolem	k7c2
roku	rok	k1gInSc2
1880	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Říp	Říp	k1gInSc1
od	od	k7c2
východu	východ	k1gInSc2
</s>
<s>
Říp	Říp	k1gInSc1
od	od	k7c2
severozápadu	severozápad	k1gInSc2
</s>
<s>
Rotunda	rotunda	k1gFnSc1
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
na	na	k7c6
Řípu	Říp	k1gInSc6
</s>
<s>
Nápadná	nápadný	k2eAgFnSc1d1
vyvýšenina	vyvýšenina	k1gFnSc1
poutala	poutat	k5eAaImAgFnS
pozornost	pozornost	k1gFnSc1
lidí	člověk	k1gMnPc2
od	od	k7c2
nejstarších	starý	k2eAgInPc2d3
časů	čas	k1gInPc2
a	a	k8xC
v	v	k7c6
rovinaté	rovinatý	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
byla	být	k5eAaImAgFnS
důležitým	důležitý	k2eAgInSc7d1
orientačním	orientační	k2eAgInSc7d1
bodem	bod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
je	být	k5eAaImIp3nS
indoevropské	indoevropský	k2eAgNnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
původ	původ	k1gInSc1
tak	tak	k6eAd1
sahá	sahat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znamená	znamenat	k5eAaImIp3nS
prostě	prostě	k9
Hora	hora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
keltského	keltský	k2eAgInSc2d1
„	„	k?
<g/>
rib	rib	k?
<g/>
“	“	k?
tzv.	tzv.	kA
„	„	k?
<g/>
žebro	žebro	k1gNnSc1
<g/>
“	“	k?
tj.	tj.	kA
žebro	žebro	k1gNnSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Slovo	slovo	k1gNnSc4
Říp	Říp	k1gInSc4
je	být	k5eAaImIp3nS
identické	identický	k2eAgNnSc1d1
se	se	k3xPyFc4
starogermánským	starogermánský	k2eAgNnPc3d1
rī	rī	k1gMnSc1
–	–	k?
skála	skála	k1gFnSc1
<g/>
,	,	kIx,
hora	hora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veršovaný	veršovaný	k2eAgInSc1d1
německý	německý	k2eAgInSc1d1
překlad	překlad	k1gInSc1
Dalimilovy	Dalimilův	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
mluví	mluvit	k5eAaImIp3nS
o	o	k7c4
Rieff	Rieff	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
poprvé	poprvé	k6eAd1
zaznamenal	zaznamenat	k5eAaPmAgInS
počátkem	počátkem	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
kronikář	kronikář	k1gMnSc1
Kosmas	Kosmas	k1gMnSc1
<g/>
,	,	kIx,
pokládá	pokládat	k5eAaImIp3nS
právě	právě	k6eAd1
horu	hora	k1gFnSc4
Říp	Říp	k1gInSc1
(	(	kIx(
<g/>
Rip	Rip	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gNnSc4
okolí	okolí	k1gNnSc4
za	za	k7c4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
usadili	usadit	k5eAaPmAgMnP
první	první	k4xOgMnPc1
Slované	Slovan	k1gMnPc1
vedení	vedení	k1gNnSc2
praotcem	praotec	k1gMnSc7
Čechem	Čech	k1gMnSc7
při	při	k7c6
příchodu	příchod	k1gInSc6
do	do	k7c2
nové	nový	k2eAgFnSc2d1
vlasti	vlast	k1gFnSc2
a	a	k8xC
kde	kde	k6eAd1
se	se	k3xPyFc4
odehrálo	odehrát	k5eAaPmAgNnS
rituální	rituální	k2eAgMnSc1d1
„	„	k?
<g/>
vzetí	vzetí	k1gNnSc6
do	do	k7c2
vlastnictví	vlastnictví	k1gNnSc2
<g/>
“	“	k?
okolní	okolní	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
vrcholící	vrcholící	k2eAgFnPc1d1
jejím	její	k3xOp3gNnSc7
novým	nový	k2eAgNnSc7d1
pojmenováním	pojmenování	k1gNnSc7
po	po	k7c6
vůdci	vůdce	k1gMnPc1
těchto	tento	k3xDgInPc2
Slovanů	Slovan	k1gInPc2
<g/>
,	,	kIx,
Čechovi	Čechův	k2eAgMnPc1d1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Boemus	Boemus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
časně	časně	k6eAd1
slovanské	slovanský	k2eAgFnSc6d1
době	doba	k1gFnSc6
však	však	k9
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
Řípu	Říp	k1gInSc2
neosídleno	osídlen	k2eNgNnSc1d1
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
pro	pro	k7c4
nedostatek	nedostatek	k1gInSc4
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hora	hora	k1gFnSc1
však	však	k9
ležela	ležet	k5eAaImAgFnS
přesně	přesně	k6eAd1
ve	v	k7c6
středu	střed	k1gInSc6
tehdejšího	tehdejší	k2eAgNnSc2d1
slovanského	slovanský	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
Čech	Čechy	k1gFnPc2
a	a	k8xC
horizont	horizont	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
lze	lze	k6eAd1
dohlédnout	dohlédnout	k5eAaPmF
z	z	k7c2
jejího	její	k3xOp3gInSc2
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
tvořil	tvořit	k5eAaImAgInS
zároveň	zároveň	k6eAd1
hranici	hranice	k1gFnSc4
tohoto	tento	k3xDgNnSc2
osídlení	osídlení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pověst	pověst	k1gFnSc1
o	o	k7c6
praotci	praotec	k1gMnSc6
Čechovi	Čech	k1gMnSc6
rozvinul	rozvinout	k5eAaPmAgMnS
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Václav	Václav	k1gMnSc1
Hájek	Hájek	k1gMnSc1
z	z	k7c2
Libočan	Libočan	k1gMnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
byl	být	k5eAaImAgMnS
vojvoda	vojvoda	k1gMnSc1
Čech	Čech	k1gMnSc1
po	po	k7c6
své	svůj	k3xOyFgFnSc6
smrti	smrt	k1gFnSc6
pochován	pochován	k2eAgMnSc1d1
v	v	k7c6
nedaleké	daleký	k2eNgFnSc6d1
Ctiněvsi	Ctiněvse	k1gFnSc6
(	(	kIx(
<g/>
1,5	1,5	k4
km	km	kA
JV	JV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgFnSc7d3
zpracování	zpracování	k1gNnSc4
pověsti	pověst	k1gFnSc2
vytvořil	vytvořit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1894	#num#	k4
Alois	Alois	k1gMnSc1
Jirásek	Jirásek	k1gMnSc1
ve	v	k7c6
Starých	Starých	k2eAgFnPc6d1
pověstech	pověst	k1gFnPc6
českých	český	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnovější	nový	k2eAgInSc4d3
zpracování	zpracování	k1gNnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
Petra	Petr	k1gMnSc2
Piťhy	Piťha	k1gMnSc2
v	v	k7c6
knize	kniha	k1gFnSc6
Paměť	paměť	k1gFnSc4
a	a	k8xC
naděje	naděje	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Stavby	stavba	k1gFnPc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
</s>
<s>
Výstup	výstup	k1gInSc1
na	na	k7c4
Říp	Říp	k1gInSc4
</s>
<s>
Boumova	Boumův	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Rotunda	rotunda	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
(	(	kIx(
<g/>
Říp	Říp	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
hory	hora	k1gFnSc2
stojí	stát	k5eAaImIp3nS
románská	románský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
zasvěcená	zasvěcený	k2eAgFnSc1d1
svatému	svatý	k1gMnSc3
Vojtěchovi	Vojtěch	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
je	být	k5eAaImIp3nS
připomínána	připomínán	k2eAgFnSc1d1
roku	rok	k1gInSc2
1126	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nechal	nechat	k5eAaPmAgMnS
kníže	kníže	k1gMnSc1
Soběslav	Soběslav	k1gMnSc1
I.	I.	kA
stávající	stávající	k2eAgInSc1d1
kostelík	kostelík	k1gInSc4
opravit	opravit	k5eAaPmF
a	a	k8xC
rozšířit	rozšířit	k5eAaPmF
na	na	k7c4
paměť	paměť	k1gFnSc4
vítězství	vítězství	k1gNnSc1
proti	proti	k7c3
německému	německý	k2eAgMnSc3d1
králi	král	k1gMnSc3
Lotharu	Lothar	k1gMnSc3
III	III	kA
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Chlumce	Chlumec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
zasvěcení	zasvěcení	k1gNnSc1
bylo	být	k5eAaImAgNnS
mezitím	mezitím	k6eAd1
změněno	změnit	k5eAaPmNgNnS
na	na	k7c4
sv.	sv.	kA
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
stal	stát	k5eAaPmAgInS
oblíbeným	oblíbený	k2eAgNnSc7d1
poutním	poutní	k2eAgNnSc7d1
místem	místo	k1gNnSc7
<g/>
;	;	kIx,
od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
Říp	Říp	k1gInSc1
nejednou	jednou	k6eNd1
dějištěm	dějiště	k1gNnSc7
táborů	tábor	k1gInPc2
lidu	lid	k1gInSc2
a	a	k8xC
národních	národní	k2eAgFnPc2d1
manifestací	manifestace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současná	současný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
rotundy	rotunda	k1gFnSc2
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
puristické	puristický	k2eAgFnSc2d1
přestavby	přestavba	k1gFnSc2
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoroční	každoroční	k2eAgFnSc1d1
pouť	pouť	k1gFnSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
v	v	k7c6
neděli	neděle	k1gFnSc6
před	před	k7c7
svátkem	svátek	k1gInSc7
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
související	související	k2eAgNnSc4d1
zábavní	zábavní	k2eAgInSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgInSc4d1
program	program	k1gInSc4
o	o	k7c6
následujícím	následující	k2eAgInSc6d1
víkendu	víkend	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
vrchol	vrchol	k1gInSc4
vede	vést	k5eAaImIp3nS
několik	několik	k4yIc4
značených	značený	k2eAgFnPc2d1
turistických	turistický	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1907	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
sousedství	sousedství	k1gNnSc6
rotundy	rotunda	k1gFnSc2
postavena	postaven	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
Boumova	Boumův	k2eAgFnSc1d1
chata	chata	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dodnes	dodnes	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
výletní	výletní	k2eAgFnSc1d1
restaurace	restaurace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zdi	zeď	k1gFnSc6
chaty	chata	k1gFnSc2
je	být	k5eAaImIp3nS
připevněn	připevněn	k2eAgInSc1d1
patriotický	patriotický	k2eAgInSc1d1
nápis	nápis	k1gInSc1
„	„	k?
<g/>
Co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
Mohamedu	Mohamed	k1gMnSc6
Mekka	Mekka	k1gFnSc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
Čechu	Čech	k1gMnSc3
Říp	Říp	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
Říp	Říp	k1gInSc4
vypravuje	vypravovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
průvodů	průvod	k1gInPc2
<g/>
,	,	kIx,
pochodů	pochod	k1gInPc2
a	a	k8xC
procesí	procesí	k1gNnSc2
pořádaných	pořádaný	k2eAgFnPc2d1
různými	různý	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
pořádá	pořádat	k5eAaImIp3nS
klub	klub	k1gInSc1
roudnických	roudnický	k2eAgMnPc2d1
cyklistů	cyklista	k1gMnPc2
„	„	k?
<g/>
Výjezd	výjezd	k1gInSc1
na	na	k7c4
Říp	Říp	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vyjet	vyjet	k5eAaPmF
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
nejrychleji	rychle	k6eAd3
horu	hora	k1gFnSc4
bez	bez	k7c2
jediného	jediný	k2eAgInSc2d1
dotyku	dotyk	k1gInSc2
nohou	noha	k1gFnSc7
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1868	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
vyzvednut	vyzvednout	k5eAaPmNgInS
a	a	k8xC
do	do	k7c2
Prahy	Praha	k1gFnSc2
slavnostně	slavnostně	k6eAd1
dopraven	dopravit	k5eAaPmNgInS
jeden	jeden	k4xCgInSc4
ze	z	k7c2
základních	základní	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Prahy	Praha	k1gFnSc2
jej	on	k3xPp3gMnSc4
doprovodilo	doprovodit	k5eAaPmAgNnS
300	#num#	k4
jezdců	jezdec	k1gMnPc2
a	a	k8xC
cestu	cesta	k1gFnSc4
lemoval	lemovat	k5eAaImAgInS
špalír	špalír	k1gInSc1
družiček	družička	k1gFnPc2
s	s	k7c7
košíčky	košíček	k1gInPc7
plnými	plný	k2eAgInPc7d1
květů	květ	k1gInPc2
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
turistické	turistický	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
června	červen	k1gInSc2
do	do	k7c2
září	září	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
k	k	k7c3
Řípu	Říp	k1gInSc3
přiblížit	přiblížit	k5eAaPmF
Podřipským	podřipský	k2eAgInSc7d1
motoráčkem	motoráček	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2010	#num#	k4
až	až	k9
2018	#num#	k4
byl	být	k5eAaImAgInS
podle	podle	k7c2
názvu	název	k1gInSc2
hory	hora	k1gFnSc2
pojmenován	pojmenován	k2eAgInSc4d1
pár	pár	k4xCyI
vlaků	vlak	k1gInPc2
(	(	kIx(
<g/>
rychlíky	rychlík	k1gInPc1
<g/>
)	)	kIx)
jezdící	jezdící	k2eAgInSc1d1
mezi	mezi	k7c7
Prahou	Praha	k1gFnSc7
a	a	k8xC
Děčínem	Děčín	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
pravověrný	pravověrný	k2eAgMnSc1d1
Čech	Čech	k1gMnSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
alespoň	alespoň	k9
jednou	jeden	k4xCgFnSc7
za	za	k7c4
život	život	k1gInSc4
na	na	k7c4
tuto	tento	k3xDgFnSc4
horu	hora	k1gFnSc4
vystoupit	vystoupit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výhled	výhled	k1gInSc1
z	z	k7c2
Roudnické	roudnický	k2eAgFnSc2d1
vyhlídky	vyhlídka	k1gFnSc2
na	na	k7c6
Roudnici	Roudnice	k1gFnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ultrakopce	Ultrakopce	k1gFnPc1
na	na	k7c4
Ultratisicovky	Ultratisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Břetislav	Břetislav	k1gMnSc1
Balatka	balatka	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Kalvoda	Kalvoda	k1gMnSc1
–	–	k?
Geomorfologické	geomorfologický	k2eAgNnSc1d1
členění	členění	k1gNnSc1
reliéfu	reliéf	k1gInSc2
Čech	Čechy	k1gFnPc2
(	(	kIx(
<g/>
Kartografie	kartografie	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřický	zeměměřický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
143812	#num#	k4
:	:	kIx,
hora	hora	k1gFnSc1
Říp	Říp	k1gInSc1
s	s	k7c7
rotundou	rotunda	k1gFnSc7
sv.	sv.	kA
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
↑	↑	k?
Žebera	Žebera	k1gFnSc1
K.	K.	kA
<g/>
,	,	kIx,
Mikula	Mikula	k1gMnSc1
J.	J.	kA
<g/>
,	,	kIx,
Říp	Říp	k1gInSc1
<g/>
,	,	kIx,
hora	hora	k1gFnSc1
v	v	k7c6
jezeru	jezero	k1gNnSc6
<g/>
,	,	kIx,
Panoráma	panoráma	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1982	#num#	k4
<g/>
↑	↑	k?
Chlupáč	chlupáč	k1gMnSc1
<g/>
,	,	kIx,
I.	I.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Geologická	geologický	k2eAgFnSc1d1
minulost	minulost	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2002	#num#	k4
<g/>
↑	↑	k?
ŠALANSKÝ	ŠALANSKÝ	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magnetické	magnetický	k2eAgInPc1d1
měření	měření	k1gNnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Řípu	Říp	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprávy	zpráva	k1gFnSc2
o	o	k7c6
geologických	geologický	k2eAgInPc6d1
výzkumech	výzkum	k1gInPc6
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
39	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
142	#num#	k4
<g/>
–	–	k?
<g/>
147	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Šubert	Šubert	k1gMnSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Borkovský	Borkovský	k2eAgMnSc1d1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Čechy	Čechy	k1gFnPc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polabí	Polabí	k1gNnSc1
<g/>
,	,	kIx,
nakladatelství	nakladatelství	k1gNnSc2
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
↑	↑	k?
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
PP	PP	kA
Hora	hora	k1gFnSc1
Říp	Říp	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mýty	mýtus	k1gInPc1
kmene	kmen	k1gInSc2
Čechů	Čech	k1gMnPc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oraga	Oraga	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
646	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
72	#num#	k4
<g/>
–	–	k?
<g/>
73	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
OBERPFALCER	OBERPFALCER	kA
<g/>
,	,	kIx,
Fratišek	Fratišek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gMnSc2
Ortsnamen	Ortsnamen	k1gInSc4
des	des	k1gNnSc2
Kuhländchens	Kuhländchensa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
<g/>
.	.	kIx.
1928	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
181	#num#	k4
<g/>
–	–	k?
<g/>
189	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ivo	Ivo	k1gMnSc1
Bartík	Bartík	k1gMnSc1
<g/>
:	:	kIx,
Záhada	záhada	k1gFnSc1
základního	základní	k2eAgInSc2d1
kamene	kámen	k1gInSc2
<g/>
,	,	kIx,
Květy	květ	k1gInPc1
<g/>
↑	↑	k?
Řazení	řazení	k1gNnSc2
vlaků	vlak	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŽelPage	ŽelPage	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Řazení	řazení	k1gNnSc2
vlaků	vlak	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŽelPage	ŽelPage	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
horarip	horarip	k1gInSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc4
</s>
<s>
Seznam	seznam	k1gInSc1
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Říp	Říp	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Říp	Říp	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Hora-Říp	Hora-Říp	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Blaník	Blaník	k1gInSc1
a	a	k8xC
Říp	Říp	k1gInSc1
–	–	k?
dvě	dva	k4xCgFnPc1
mýtické	mýtický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
českých	český	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
–	–	k?
video	video	k1gNnSc4
z	z	k7c2
cyklu	cyklus	k1gInSc2
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Historický	historický	k2eAgInSc1d1
magazín	magazín	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Poohří	Poohří	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Lovoš	Lovoš	k1gMnSc1
•	•	k?
Milešovka	Milešovka	k1gFnSc1
•	•	k?
Sedlo	sedlo	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Borečský	Borečský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Dubí	dubí	k1gNnSc2
hora	hora	k1gFnSc1
•	•	k?
Kleneč	Kleneč	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Březina	Březina	k1gMnSc1
•	•	k?
Holý	Holý	k1gMnSc1
vrch	vrch	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Hlinné	hlinný	k2eAgFnSc2d1
•	•	k?
Kalvárie	Kalvárie	k1gFnSc2
•	•	k?
Lipská	lipský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Loužek	loužka	k1gFnPc2
•	•	k?
Mokřady	mokřad	k1gInPc1
dolní	dolní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Myslivna	myslivna	k1gFnSc1
•	•	k?
Na	na	k7c6
Černčí	Černčí	k2eAgFnSc6d1
•	•	k?
Pístecký	Pístecký	k2eAgInSc1d1
les	les	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1
stráně	stráň	k1gFnPc1
u	u	k7c2
Štětí	štětit	k5eAaImIp3nP
•	•	k?
Dobříňský	Dobříňský	k2eAgInSc1d1
háj	háj	k1gInSc1
•	•	k?
Evaňská	Evaňský	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Říp	Říp	k1gInSc1
•	•	k?
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Košťálov	Košťálovo	k1gNnPc2
•	•	k?
Koštice	Koštice	k1gFnSc2
•	•	k?
Kuzov	Kuzov	k1gInSc1
•	•	k?
Mokřad	mokřad	k1gInSc1
pod	pod	k7c7
Terezínskou	terezínský	k2eAgFnSc7d1
pevností	pevnost	k1gFnSc7
•	•	k?
Na	na	k7c6
Dlouhé	Dlouhé	k2eAgFnSc6d1
stráni	stráň	k1gFnSc6
•	•	k?
Písčiny	písčina	k1gFnSc2
u	u	k7c2
Oleška	Olešek	k1gInSc2
•	•	k?
Plešivec	plešivec	k1gMnSc1
•	•	k?
Radobýl	Radobýl	k1gMnSc1
•	•	k?
Radouň	Radouň	k1gFnSc1
•	•	k?
Skalky	skalka	k1gFnSc2
u	u	k7c2
Třebutiček	Třebutička	k1gFnPc2
•	•	k?
Slatiniště	slatiniště	k1gNnSc4
u	u	k7c2
Vrbky	vrbka	k1gFnSc2
•	•	k?
Sovice	sovice	k1gFnSc2
u	u	k7c2
Brzánek	Brzánka	k1gFnPc2
•	•	k?
Stráně	stráň	k1gFnSc2
nad	nad	k7c7
Suchým	suchý	k2eAgInSc7d1
potokem	potok	k1gInSc7
•	•	k?
Stráně	stráň	k1gFnSc2
u	u	k7c2
Drahobuzi	Drahobuze	k1gFnSc4
•	•	k?
Stráně	stráň	k1gFnPc1
u	u	k7c2
Velkého	velký	k2eAgInSc2d1
Újezdu	Újezd	k1gInSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Podbradeckého	Podbradecký	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
V	v	k7c6
kuksu	kuks	k1gInSc6
•	•	k?
Vrch	vrch	k1gInSc1
Hazmburk	Hazmburk	k1gInSc1
•	•	k?
Vrbka	vrbka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
160459	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4086696-8	4086696-8	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
