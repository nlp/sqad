<s>
Brain	Brain	k1gInSc1	Brain
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Base	basa	k1gFnSc3	basa
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
ブ	ブ	k?	ブ
<g/>
・	・	k?	・
<g/>
ベ	ベ	k?	ベ
<g/>
,	,	kIx,	,
Bureinzu	Bureinza	k1gFnSc4	Bureinza
<g/>
・	・	k?	・
<g/>
Bésu	Bésus	k1gInSc2	Bésus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc4d1	Japonské
animační	animační	k2eAgNnSc4d1	animační
studio	studio	k1gNnSc4	studio
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
založili	založit	k5eAaPmAgMnP	založit
bývalí	bývalý	k2eAgMnPc1d1	bývalý
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
studia	studio	k1gNnSc2	studio
TMS	TMS	kA	TMS
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Bakutó	Bakutó	k?	Bakutó
sengen	sengen	k1gInSc1	sengen
Daigunder	Daigunder	k1gInSc1	Daigunder
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Bóken	Bókna	k1gFnPc2	Bókna
júki	júki	k1gNnPc2	júki
Pluster	Pluster	k1gMnSc1	Pluster
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Kamiču	Kamičus	k1gInSc2	Kamičus
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Gunparade	Gunparad	k1gInSc5	Gunparad
March	March	k1gMnSc1	March
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Innocent	Innocent	k1gMnSc1	Innocent
Venus	Venus	k1gMnSc1	Venus
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Kišin	Kišin	k2eAgInSc1d1	Kišin
taisen	taisen	k1gInSc1	taisen
Gigantic	Gigantice	k1gFnPc2	Gigantice
Formula	Formulum	k1gNnSc2	Formulum
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Baccano	Baccana	k1gFnSc5	Baccana
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Kure-nai	Kureai	k1gNnSc6	Kure-nai
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Nacume	Nacum	k1gInSc5	Nacum
júdžinčó	júdžinčó	k?	júdžinčó
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Zoku	Zokus	k1gInSc2	Zokus
Nacume	Nacum	k1gInSc5	Nacum
júdžinčó	júdžinčó	k?	júdžinčó
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Akikan	Akikana	k1gFnPc2	Akikana
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Durarara	Durarara	k1gFnSc1	Durarara
<g/>
!!	!!	k?	!!
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Kuragehime	Kuragehim	k1gInSc5	Kuragehim
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Dororon	Dororon	k1gInSc1	Dororon
Enma-kun	Enmaun	k1gInSc1	Enma-kun
MeeraMera	MeeraMera	k1gFnSc1	MeeraMera
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Kamisama	Kamisama	k1gNnSc1	Kamisama
Dolls	Dollsa	k1gFnPc2	Dollsa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Mawaru	Mawar	k1gInSc2	Mawar
Penguindrum	Penguindrum	k1gInSc1	Penguindrum
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Nacume	Nacum	k1gInSc5	Nacum
júdžinčó	júdžinčó	k?	júdžinčó
san	san	k?	san
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Nacume	Nacum	k1gInSc5	Nacum
<g />
.	.	kIx.	.
</s>
<s>
júdžinčó	júdžinčó	k?	júdžinčó
ši	ši	k?	ši
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Sengoku	Sengok	k1gInSc2	Sengok
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Tonari	Tonari	k1gNnPc7	Tonari
no	no	k9	no
kaibucu-kun	kaibucuun	k1gMnSc1	kaibucu-kun
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Ixion	Ixion	k1gInSc1	Ixion
Saga	Sag	k1gInSc2	Sag
DT	DT	kA	DT
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Amnesia	Amnesium	k1gNnSc2	Amnesium
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Jahari	Jahar	k1gInSc6	Jahar
ore	ore	k?	ore
no	no	k9	no
seišun	seišun	k1gMnSc1	seišun
Love	lov	k1gInSc5	lov
Come	Come	k1gFnPc7	Come
wa	wa	k?	wa
mačigatteiru	mačigatteir	k1gInSc2	mačigatteir
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Blood	Blood	k1gInSc1	Blood
Lad	lad	k1gInSc1	lad
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Brothers	Brothers	k1gInSc1	Brothers
Conflict	Conflict	k1gInSc1	Conflict
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
D-Frag	D-Fraga	k1gFnPc2	D-Fraga
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Bokura	Bokura	k1gFnSc1	Bokura
wa	wa	k?	wa
minna	minna	k1gFnSc1	minna
kawai-só	kawaió	k?	kawai-só
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Iššúkan	Iššúkan	k1gInSc1	Iššúkan
Friends	Friends	k1gInSc1	Friends
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Kamigami	Kamiga	k1gFnPc7	Kamiga
no	no	k9	no
asobi	asob	k1gMnSc3	asob
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Kjókai	Kjókai	k1gNnSc4	Kjókai
no	no	k9	no
Rinne	Rinn	k1gInSc5	Rinn
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Aoharu	Aohar	k1gInSc2	Aohar
×	×	k?	×
kikandžú	kikandžú	k?	kikandžú
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Dance	Danka	k1gFnSc6	Danka
with	witha	k1gFnPc2	witha
Devils	Devilsa	k1gFnPc2	Devilsa
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Endride	Endrid	k1gInSc5	Endrid
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Servamp	Servamp	k1gInSc1	Servamp
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Kjókai	Kjókai	k1gNnSc4	Kjókai
no	no	k9	no
Rinne	Rinn	k1gInSc5	Rinn
2	[number]	k4	2
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Cheer	Cheero	k1gNnPc2	Cheero
danši	dansat	k5eAaPmIp1nSwK	dansat
<g/>
!!	!!	k?	!!
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Wataši	Wataše	k1gFnSc3	Wataše
ga	ga	k?	ga
motete	mote	k1gNnSc2	mote
dósunda	dósunda	k1gFnSc1	dósunda
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Čódžikú	Čódžikú	k1gMnSc1	Čódžikú
Robo	roba	k1gFnSc5	roba
Meguru	Megur	k1gInSc6	Megur
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Kaze	kaz	k1gInSc5	kaz
o	o	k7c6	o
mita	mit	k1gInSc2	mit
šónen	šónna	k1gFnPc2	šónna
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Hotarubi	Hotarubi	k1gNnSc4	Hotarubi
no	no	k9	no
mori	mori	k6eAd1	mori
e	e	k0	e
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Šin	Šin	k1gFnSc6	Šin
(	(	kIx(	(
<g/>
Čendži	Čendž	k1gFnSc6	Čendž
<g/>
!!	!!	k?	!!
<g/>
)	)	kIx)	)
Getter	Getter	k1gInSc1	Getter
Robo	roba	k1gFnSc5	roba
<g/>
~	~	kIx~	~
<g/>
Sekai	Seka	k1gMnPc7	Seka
saigo	saigo	k6eAd1	saigo
no	no	k9	no
hi	hi	k0	hi
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Šin	Šin	k1gMnSc1	Šin
Getter	Getter	k1gMnSc1	Getter
Robo	roba	k1gFnSc5	roba
tai	tai	k?	tai
Neo	Neo	k1gMnSc1	Neo
Getter	Getter	k1gMnSc1	Getter
Robo	roba	k1gFnSc5	roba
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Mazinkaiser	Mazinkaisra	k1gFnPc2	Mazinkaisra
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Šin	Šin	k1gMnSc1	Šin
Getter	Getter	k1gMnSc1	Getter
Robo	roba	k1gFnSc5	roba
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Super	super	k2eAgInSc1d1	super
Robot	robot	k1gInSc1	robot
Wars	Warsa	k1gFnPc2	Warsa
Original	Original	k1gFnPc2	Original
Generation	Generation	k1gInSc1	Generation
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Animation	Animation	k1gInSc1	Animation
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Kikóši	Kikóš	k1gInSc3	Kikóš
Enma	Enm	k1gInSc2	Enm
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Kimi	Kimi	k1gNnSc1	Kimi
ga	ga	k?	ga
nozomu	nozom	k1gInSc2	nozom
<g />
.	.	kIx.	.
</s>
<s>
eien	eien	k1gMnSc1	eien
<g/>
:	:	kIx,	:
Next	Next	k1gMnSc1	Next
Season	Season	k1gMnSc1	Season
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Denpa	Denpa	k1gFnSc1	Denpa
teki	tek	k1gFnSc2	tek
na	na	k7c4	na
kanodžo	kanodžo	k1gNnSc4	kanodžo
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Kurenai	Kurenai	k1gNnSc6	Kurenai
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Šidžó	Šidžó	k1gFnSc1	Šidžó
saikjó	saikjó	k?	saikjó
no	no	k9	no
denši	densat	k5eAaPmIp1nSwK	densat
<g/>
:	:	kIx,	:
Ken	Ken	k1gMnSc1	Ken
<g/>
'	'	kIx"	'
<g/>
iči	iči	k?	iči
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Ansacu	Ansacum	k1gNnSc3	Ansacum
kjóšicu	kjóšicum	k1gNnSc3	kjóšicum
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Nacume	Nacum	k1gInSc5	Nacum
júdžinčó	júdžinčó	k?	júdžinčó
<g/>
:	:	kIx,	:
Icuka	Icuka	k1gMnSc1	Icuka
juki	juki	k6eAd1	juki
no	no	k9	no
hi	hi	k0	hi
ni	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Ane	án	k1gMnSc5	án
Log	log	k1gInSc1	log
<g/>
:	:	kIx,	:
Mojako	Mojako	k1gNnSc1	Mojako
né-san	néana	k1gFnPc2	né-sana
no	no	k9	no
tomaranai	tomaranai	k1gNnSc1	tomaranai
Monologue	Monologu	k1gFnSc2	Monologu
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
