<s>
Greyhound	Greyhound	k1gInSc1	Greyhound
<g/>
,	,	kIx,	,
též	též	k9	též
anglický	anglický	k2eAgMnSc1d1	anglický
chrt	chrt	k1gMnSc1	chrt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
ušlechtilý	ušlechtilý	k2eAgMnSc1d1	ušlechtilý
a	a	k8xC	a
výrazný	výrazný	k2eAgMnSc1d1	výrazný
pes	pes	k1gMnSc1	pes
atletické	atletický	k2eAgFnSc2d1	atletická
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc4d1	vysoký
a	a	k8xC	a
velkorysých	velkorysý	k2eAgFnPc2d1	velkorysá
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
chrt	chrt	k1gMnSc1	chrt
je	být	k5eAaImIp3nS	být
elegantní	elegantní	k2eAgMnSc1d1	elegantní
a	a	k8xC	a
nápadně	nápadně	k6eAd1	nápadně
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgFnSc2d1	pevná
konstituce	konstituce	k1gFnSc2	konstituce
<g/>
,	,	kIx,	,
ušlechtilých	ušlechtilý	k2eAgFnPc2d1	ušlechtilá
proporcí	proporce	k1gFnPc2	proporce
i	i	k8xC	i
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
běhu	běh	k1gInSc6	běh
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	kA	h
<g/>
.	.	kIx.	.
</s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
,	,	kIx,	,
s	s	k7c7	s
klidným	klidný	k2eAgInSc7d1	klidný
výrazem	výraz	k1gInSc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc4	ucho
má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
jemné	jemný	k2eAgFnPc1d1	jemná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
plátků	plátek	k1gInPc2	plátek
růže	růž	k1gFnSc2	růž
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
a	a	k8xC	a
měkkou	měkký	k2eAgFnSc7d1	měkká
srstí	srst	k1gFnSc7	srst
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
klenutý	klenutý	k2eAgInSc1d1	klenutý
a	a	k8xC	a
svalnatý	svalnatý	k2eAgInSc1d1	svalnatý
<g/>
.	.	kIx.	.
</s>
<s>
Hruď	hruď	k1gFnSc1	hruď
je	být	k5eAaImIp3nS	být
hluboká	hluboký	k2eAgFnSc1d1	hluboká
a	a	k8xC	a
mohutná	mohutný	k2eAgFnSc1d1	mohutná
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc4	hřbet
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
<g/>
,	,	kIx,	,
široký	široký	k2eAgInSc4d1	široký
a	a	k8xC	a
rovný	rovný	k2eAgInSc4d1	rovný
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhá	k1gFnPc4	Dlouhá
a	a	k8xC	a
rovné	rovný	k2eAgFnPc4d1	rovná
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
mají	mít	k5eAaImIp3nP	mít
pevné	pevný	k2eAgFnPc4d1	pevná
kosti	kost	k1gFnPc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
šavlovitě	šavlovitě	k6eAd1	šavlovitě
zahnutý	zahnutý	k2eAgMnSc1d1	zahnutý
<g/>
,	,	kIx,	,
nízko	nízko	k6eAd1	nízko
nasazený	nasazený	k2eAgMnSc1d1	nasazený
<g/>
,	,	kIx,	,
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Psi	pes	k1gMnPc1	pes
velmi	velmi	k6eAd1	velmi
podobni	podoben	k2eAgMnPc1d1	podoben
dnešním	dnešní	k2eAgMnPc3d1	dnešní
chrtům	chrt	k1gMnPc3	chrt
byli	být	k5eAaImAgMnP	být
známi	znám	k2eAgMnPc1d1	znám
již	již	k6eAd1	již
před	před	k7c7	před
pěti	pět	k4xCc7	pět
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Dávná	dávný	k2eAgNnPc4d1	dávné
vyobrazení	vyobrazení	k1gNnPc4	vyobrazení
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
podobných	podobný	k2eAgFnPc2d1	podobná
chrtům	chrt	k1gMnPc3	chrt
<g/>
,	,	kIx,	,
nacházíme	nacházet	k5eAaImIp1nP	nacházet
již	již	k6eAd1	již
na	na	k7c6	na
basreliéfech	basreliéf	k1gInPc6	basreliéf
ze	z	k7c2	z
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
pouštní	pouštní	k2eAgFnSc2d1	pouštní
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
míšení	míšení	k1gNnSc2	míšení
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
plemeny	plemeno	k1gNnPc7	plemeno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
téměř	téměř	k6eAd1	téměř
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
dnešní	dnešní	k2eAgNnPc1d1	dnešní
plemena	plemeno	k1gNnPc1	plemeno
chrtů	chrt	k1gMnPc2	chrt
mají	mít	k5eAaImIp3nP	mít
skutečně	skutečně	k6eAd1	skutečně
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
primitivních	primitivní	k2eAgFnPc2d1	primitivní
plemen	plemeno	k1gNnPc2	plemeno
honičů	honič	k1gMnPc2	honič
lovících	lovící	k2eAgMnPc2d1	lovící
zrakem	zrak	k1gInSc7	zrak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např	např	kA	např
Ibizský	Ibizský	k2eAgMnSc1d1	Ibizský
podenco	podenco	k1gMnSc1	podenco
<g/>
,	,	kIx,	,
či	či	k8xC	či
faraonský	faraonský	k2eAgMnSc1d1	faraonský
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
návaznost	návaznost	k1gFnSc1	návaznost
na	na	k7c4	na
starověké	starověký	k2eAgMnPc4d1	starověký
egyptské	egyptský	k2eAgMnPc4d1	egyptský
honiče	honič	k1gMnPc4	honič
téměř	téměř	k6eAd1	téměř
jistá	jistý	k2eAgFnSc1d1	jistá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
na	na	k7c6	na
blízkém	blízký	k2eAgInSc6d1	blízký
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
ale	ale	k8xC	ale
náleží	náležet	k5eAaImIp3nP	náležet
dnešní	dnešní	k2eAgMnPc1d1	dnešní
chrti	chrt	k1gMnPc1	chrt
k	k	k7c3	k
jiné	jiný	k2eAgFnSc3d1	jiná
vývojové	vývojový	k2eAgFnSc3d1	vývojová
linii	linie	k1gFnSc3	linie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
jistotou	jistota	k1gFnSc7	jistota
můžeme	moct	k5eAaImIp1nP	moct
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
přišli	přijít	k5eAaPmAgMnP	přijít
chrti	chrt	k1gMnPc1	chrt
s	s	k7c7	s
keltskými	keltský	k2eAgInPc7d1	keltský
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
se	se	k3xPyFc4	se
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
chrti	chrt	k1gMnPc1	chrt
podobní	podobný	k2eAgMnPc1d1	podobný
dnešnímu	dnešní	k2eAgMnSc3d1	dnešní
Polskému	polský	k2eAgMnSc3d1	polský
chrtovi	chrt	k1gMnSc3	chrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
bylo	být	k5eAaImAgNnS	být
držení	držení	k1gNnSc1	držení
chrtích	chrtí	k2eAgFnPc2d1	chrtí
smeček	smečka	k1gFnPc2	smečka
výsadou	výsada	k1gFnSc7	výsada
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
používáni	používat	k5eAaImNgMnP	používat
jak	jak	k9	jak
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
malé	malý	k2eAgFnSc2d1	malá
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
i	i	k9	i
různorodý	různorodý	k2eAgInSc1d1	různorodý
zjev	zjev	k1gInSc1	zjev
chrtů	chrt	k1gMnPc2	chrt
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
množství	množství	k1gNnSc6	množství
různých	různý	k2eAgNnPc2d1	různé
pojmenování	pojmenování	k1gNnPc2	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
byli	být	k5eAaImAgMnP	být
menší	malý	k2eAgMnPc1d2	menší
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
králíků	králík	k1gMnPc2	králík
a	a	k8xC	a
zajíců	zajíc	k1gMnPc2	zajíc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
štvanice	štvanice	k1gFnPc4	štvanice
se	se	k3xPyFc4	se
chovali	chovat	k5eAaImAgMnP	chovat
větší	veliký	k2eAgMnPc1d2	veliký
a	a	k8xC	a
silnější	silný	k2eAgMnPc1d2	silnější
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
menších	malý	k2eAgMnPc6d2	menší
jedincích	jedinec	k1gMnPc6	jedinec
bylo	být	k5eAaImAgNnS	být
postaveno	postaven	k2eAgNnSc4d1	postaveno
plemeno	plemeno	k1gNnSc4	plemeno
whippet	whippet	k1gMnSc1	whippet
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
popularitu	popularita	k1gFnSc4	popularita
zejména	zejména	k9	zejména
u	u	k7c2	u
chudších	chudý	k2eAgFnPc2d2	chudší
vrstev	vrstva	k1gFnPc2	vrstva
jako	jako	k8xC	jako
malý	malý	k2eAgMnSc1d1	malý
dostihový	dostihový	k2eAgMnSc1d1	dostihový
psík	psík	k1gMnSc1	psík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
rodin	rodina	k1gFnPc2	rodina
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
plemena	plemeno	k1gNnPc1	plemeno
greyhound	greyhounda	k1gFnPc2	greyhounda
(	(	kIx(	(
<g/>
krátkosrstý	krátkosrstý	k2eAgInSc1d1	krátkosrstý
<g/>
)	)	kIx)	)
a	a	k8xC	a
skotský	skotský	k2eAgInSc1d1	skotský
deerhound	deerhound	k1gInSc1	deerhound
(	(	kIx(	(
<g/>
hrubosrstý	hrubosrstý	k2eAgMnSc1d1	hrubosrstý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původní	původní	k2eAgMnSc1d1	původní
irský	irský	k2eAgMnSc1d1	irský
vlkodav	vlkodav	k1gMnSc1	vlkodav
již	již	k6eAd1	již
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
s	s	k7c7	s
vymýcením	vymýcení	k1gNnSc7	vymýcení
vlků	vlk	k1gMnPc2	vlk
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
velké	velký	k2eAgFnSc2d1	velká
zvěře	zvěř	k1gFnSc2	zvěř
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgMnSc1d1	dnešní
vlkodav	vlkodav	k1gMnSc1	vlkodav
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
produkt	produkt	k1gInSc4	produkt
novodobé	novodobý	k2eAgFnSc2d1	novodobá
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Greyhoundi	Greyhound	k1gMnPc1	Greyhound
byli	být	k5eAaImAgMnP	být
šlechtěni	šlechtěn	k2eAgMnPc1d1	šlechtěn
jako	jako	k8xS	jako
výkonní	výkonný	k2eAgMnPc1d1	výkonný
lovečtí	lovecký	k2eAgMnPc1d1	lovecký
psi	pes	k1gMnPc1	pes
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
sportovní	sportovní	k2eAgFnPc1d1	sportovní
štvanice	štvanice	k1gFnPc1	štvanice
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
psi	pes	k1gMnPc1	pes
měřili	měřit	k5eAaImAgMnP	měřit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
srovnával	srovnávat	k5eAaImAgMnS	srovnávat
dovednosti	dovednost	k1gFnPc4	dovednost
<g/>
,	,	kIx,	,
bystrost	bystrost	k1gFnSc1	bystrost
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
obratnost	obratnost	k1gFnSc1	obratnost
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Greyhound	Greyhound	k1gMnSc1	Greyhound
tedy	tedy	k9	tedy
nebyl	být	k5eNaImAgMnS	být
prvoplánově	prvoplánově	k6eAd1	prvoplánově
šlechtěn	šlechtěn	k2eAgMnSc1d1	šlechtěn
jako	jako	k8xC	jako
dostihový	dostihový	k2eAgMnSc1d1	dostihový
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
pes	pes	k1gMnSc1	pes
lovecký	lovecký	k2eAgMnSc1d1	lovecký
<g/>
.	.	kIx.	.
</s>
<s>
Dostihy	dostih	k1gInPc1	dostih
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xC	jak
je	on	k3xPp3gInPc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
produktem	produkt	k1gInSc7	produkt
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
zažily	zažít	k5eAaPmAgFnP	zažít
velký	velký	k2eAgInSc4d1	velký
boom	boom	k1gInSc4	boom
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
možnost	možnost	k1gFnSc4	možnost
sázek	sázka	k1gFnPc2	sázka
a	a	k8xC	a
popularita	popularita	k1gFnSc1	popularita
učinily	učinit	k5eAaImAgFnP	učinit
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
greyhound	greyhound	k1gInSc1	greyhound
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
výrobním	výrobní	k2eAgInSc7d1	výrobní
prostředkem	prostředek	k1gInSc7	prostředek
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
zacházelo	zacházet	k5eAaImAgNnS	zacházet
jako	jako	k8xS	jako
s	s	k7c7	s
věcí	věc	k1gFnSc7	věc
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
byla	být	k5eAaImAgFnS	být
stavem	stav	k1gInSc7	stav
věcí	věc	k1gFnPc2	věc
velmi	velmi	k6eAd1	velmi
pobouřena	pobouřit	k5eAaPmNgFnS	pobouřit
a	a	k8xC	a
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
drah	draha	k1gFnPc2	draha
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
také	také	k9	také
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
např	např	kA	např
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
oblíbenost	oblíbenost	k1gFnSc1	oblíbenost
dostihového	dostihový	k2eAgInSc2d1	dostihový
sportu	sport	k1gInSc2	sport
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
dnes	dnes	k6eAd1	dnes
probíhají	probíhat	k5eAaImIp3nP	probíhat
chrtí	chrtí	k2eAgInPc1d1	chrtí
dostihy	dostih	k1gInPc1	dostih
a	a	k8xC	a
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
aktivistů	aktivista	k1gMnPc2	aktivista
se	se	k3xPyFc4	se
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dostihovými	dostihový	k2eAgFnPc7d1	dostihová
drahami	draha	k1gFnPc7	draha
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
co	co	k9	co
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
podmínek	podmínka	k1gFnPc2	podmínka
závodních	závodní	k2eAgMnPc2d1	závodní
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
umístění	umístění	k1gNnSc4	umístění
do	do	k7c2	do
nových	nový	k2eAgInPc2d1	nový
domovů	domov	k1gInPc2	domov
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
začaly	začít	k5eAaPmAgFnP	začít
pořádat	pořádat	k5eAaImF	pořádat
výstavy	výstava	k1gFnPc4	výstava
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
oddělila	oddělit	k5eAaPmAgFnS	oddělit
linie	linie	k1gFnSc1	linie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
šlechtěna	šlechtit	k5eAaImNgFnS	šlechtit
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
a	a	k8xC	a
sport	sport	k1gInSc4	sport
od	od	k7c2	od
výstavní	výstavní	k2eAgFnSc2d1	výstavní
linie	linie	k1gFnSc2	linie
greyhoundů	greyhound	k1gMnPc2	greyhound
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
obě	dva	k4xCgFnPc1	dva
linie	linie	k1gFnPc1	linie
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nekříží	křížit	k5eNaImIp3nS	křížit
<g/>
.	.	kIx.	.
</s>
<s>
Udržují	udržovat	k5eAaImIp3nP	udržovat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
prakticky	prakticky	k6eAd1	prakticky
dvě	dva	k4xCgNnPc4	dva
plemena	plemeno	k1gNnPc4	plemeno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
amatérských	amatérský	k2eAgInPc6d1	amatérský
evropských	evropský	k2eAgInPc6d1	evropský
chovech	chov	k1gInPc6	chov
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
plemenné	plemenný	k2eAgFnSc2d1	plemenná
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Greyhoundi	Greyhound	k1gMnPc1	Greyhound
jsou	být	k5eAaImIp3nP	být
drženi	držet	k5eAaImNgMnP	držet
jako	jako	k9	jako
psi	pes	k1gMnPc1	pes
společenští	společenský	k2eAgMnPc1d1	společenský
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgNnSc1d1	výstavní
a	a	k8xC	a
pro	pro	k7c4	pro
amatérské	amatérský	k2eAgInPc4d1	amatérský
dostihy	dostih	k1gInPc4	dostih
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pořádány	pořádat	k5eAaImNgFnP	pořádat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
radost	radost	k1gFnSc4	radost
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pro	pro	k7c4	pro
zisk	zisk	k1gInSc4	zisk
a	a	k8xC	a
sázky	sázka	k1gFnPc4	sázka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popularitu	popularita	k1gFnSc4	popularita
si	se	k3xPyFc3	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
získává	získávat	k5eAaImIp3nS	získávat
i	i	k9	i
tzv.	tzv.	kA	tzv.
coursing	coursing	k1gInSc1	coursing
-	-	kIx~	-
terénní	terénní	k2eAgInSc1d1	terénní
dostih	dostih	k1gInSc1	dostih
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
imituje	imitovat	k5eAaBmIp3nS	imitovat
přirozený	přirozený	k2eAgInSc1d1	přirozený
lov	lov	k1gInSc1	lov
zajíce	zajíc	k1gMnSc2	zajíc
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
pomocí	pomocí	k7c2	pomocí
igelitové	igelitový	k2eAgFnSc2d1	igelitová
návnady	návnada	k1gFnSc2	návnada
<g/>
.	.	kIx.	.
</s>
<s>
Coursingu	Coursing	k1gInSc2	Coursing
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
zejména	zejména	k9	zejména
jedinci	jedinec	k1gMnPc1	jedinec
z	z	k7c2	z
výstavních	výstavní	k2eAgFnPc2d1	výstavní
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
klasické	klasický	k2eAgFnPc1d1	klasická
křivky	křivka	k1gFnPc1	křivka
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
předmětem	předmět	k1gInSc7	předmět
obdivu	obdiv	k1gInSc2	obdiv
umělců	umělec	k1gMnPc2	umělec
naší	náš	k3xOp1gFnSc2	náš
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
chrt	chrt	k1gMnSc1	chrt
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vstřícné	vstřícný	k2eAgNnSc1d1	vstřícné
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umí	umět	k5eAaImIp3nS	umět
ocenit	ocenit	k5eAaPmF	ocenit
pohodlí	pohodlí	k1gNnSc4	pohodlí
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
lovecký	lovecký	k2eAgMnSc1d1	lovecký
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
užívá	užívat	k5eAaImIp3nS	užívat
svůj	svůj	k3xOyFgInSc4	svůj
vynikající	vynikající	k2eAgInSc4d1	vynikající
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
čichu	čich	k1gInSc2	čich
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgMnSc1d1	moderní
greyhound	greyhound	k1gMnSc1	greyhound
byl	být	k5eAaImAgMnS	být
specialistou	specialista	k1gMnSc7	specialista
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
zajíců	zajíc	k1gMnPc2	zajíc
<g/>
,	,	kIx,	,
králíků	králík	k1gMnPc2	králík
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
třeba	třeba	k9	třeba
i	i	k9	i
lišek	liška	k1gFnPc2	liška
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
ušlechtilý	ušlechtilý	k2eAgMnSc1d1	ušlechtilý
průvodce	průvodce	k1gMnSc1	průvodce
a	a	k8xC	a
společník	společník	k1gMnSc1	společník
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
účastnit	účastnit	k5eAaImF	účastnit
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
i	i	k8xC	i
krátkých	krátký	k2eAgFnPc2d1	krátká
procházek	procházka	k1gFnPc2	procházka
<g/>
,	,	kIx,	,
vyjížděk	vyjížďka	k1gFnPc2	vyjížďka
s	s	k7c7	s
koňmi	kůň	k1gMnPc7	kůň
apod.	apod.	kA	apod.
Není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
výcviku	výcvik	k1gInSc2	výcvik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všichni	všechen	k3xTgMnPc1	všechen
psi	pes	k1gMnPc1	pes
musí	muset	k5eAaImIp3nP	muset
absolvovat	absolvovat	k5eAaPmF	absolvovat
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
základní	základní	k2eAgInSc4d1	základní
výcvik	výcvik	k1gInSc4	výcvik
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byli	být	k5eAaImAgMnP	být
skutečně	skutečně	k6eAd1	skutečně
příjemní	příjemný	k2eAgMnPc1d1	příjemný
a	a	k8xC	a
sofistikovaní	sofistikovaný	k2eAgMnPc1d1	sofistikovaný
společníci	společník	k1gMnPc1	společník
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
cvičení	cvičení	k1gNnSc6	cvičení
rádi	rád	k2eAgMnPc1d1	rád
zúčastňují	zúčastňovat	k5eAaImIp3nP	zúčastňovat
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
agility	agilita	k1gFnSc2	agilita
<g/>
,	,	kIx,	,
dogdancingu	dogdancing	k1gInSc2	dogdancing
apod.	apod.	kA	apod.
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Greyhound	Greyhounda	k1gFnPc2	Greyhounda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Můj-pes	Můjes	k1gInSc1	Můj-pes
<g/>
:	:	kIx,	:
Greyhound	Greyhound	k1gInSc1	Greyhound
Psí-rasy	Psíasa	k1gFnSc2	Psí-rasa
<g/>
:	:	kIx,	:
Anglický	anglický	k2eAgMnSc1d1	anglický
chrt	chrt	k1gMnSc1	chrt
Hafici	Hafice	k1gFnSc4	Hafice
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
databáze	databáze	k1gFnSc1	databáze
greyhoundů	greyhound	k1gInPc2	greyhound
Téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
k	k	k7c3	k
péči	péče	k1gFnSc3	péče
o	o	k7c4	o
vašeho	váš	k3xOp2gMnSc4	váš
greyhounda	greyhound	k1gMnSc4	greyhound
<g/>
.	.	kIx.	.
</s>
