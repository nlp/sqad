<s>
Fakulta	fakulta	k1gFnSc1	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
studií	studie	k1gFnPc2	studie
(	(	kIx(	(
<g/>
FSS	FSS	kA	FSS
MU	MU	kA	MU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
fakultou	fakulta	k1gFnSc7	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
osamostatněním	osamostatnění	k1gNnSc7	osamostatnění
několika	několik	k4yIc2	několik
oborů	obor	k1gInPc2	obor
z	z	k7c2	z
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
MU	MU	kA	MU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2005	[number]	k4	2005
se	se	k3xPyFc4	se
fakulta	fakulta	k1gFnSc1	fakulta
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
z	z	k7c2	z
prostor	prostora	k1gFnPc2	prostora
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
do	do	k7c2	do
kompletně	kompletně	k6eAd1	kompletně
rekonstruované	rekonstruovaný	k2eAgFnSc2d1	rekonstruovaná
budovy	budova	k1gFnSc2	budova
na	na	k7c6	na
Joštově	Joštův	k2eAgFnSc6d1	Joštova
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgNnSc1d1	užívané
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
fakultou	fakulta	k1gFnSc7	fakulta
lékařskou	lékařský	k2eAgFnSc7d1	lékařská
a	a	k8xC	a
původně	původně	k6eAd1	původně
postavené	postavený	k2eAgInPc1d1	postavený
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
technickou	technický	k2eAgFnSc4d1	technická
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
je	být	k5eAaImIp3nS	být
důsledně	důsledně	k6eAd1	důsledně
členěno	členěn	k2eAgNnSc1d1	členěno
na	na	k7c4	na
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
navazující	navazující	k2eAgFnPc4d1	navazující
<g/>
)	)	kIx)	)
magisterský	magisterský	k2eAgInSc4d1	magisterský
a	a	k8xC	a
doktorský	doktorský	k2eAgInSc4d1	doktorský
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
zde	zde	k6eAd1	zde
studovalo	studovat	k5eAaImAgNnS	studovat
celkem	celkem	k6eAd1	celkem
4	[number]	k4	4
113	[number]	k4	113
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
možností	možnost	k1gFnPc2	možnost
studia	studio	k1gNnSc2	studio
politologie	politologie	k1gFnSc2	politologie
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc2	psychologie
nebo	nebo	k8xC	nebo
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
a	a	k8xC	a
akademické	akademický	k2eAgNnSc1d1	akademické
pracoviště	pracoviště	k1gNnSc1	pracoviště
CERGE-EI	CERGE-EI	k1gFnSc2	CERGE-EI
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
jako	jako	k9	jako
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
česká	český	k2eAgFnSc1d1	Česká
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
fakulty	fakulta	k1gFnSc2	fakulta
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
specializovaná	specializovaný	k2eAgNnPc4d1	specializované
čtyři	čtyři	k4xCgNnPc4	čtyři
výzkumná	výzkumný	k2eAgNnPc4d1	výzkumné
pracoviště	pracoviště	k1gNnPc4	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Katedra	katedra	k1gFnSc1	katedra
sociologie	sociologie	k1gFnSc1	sociologie
Katedra	katedra	k1gFnSc1	katedra
psychologie	psychologie	k1gFnSc1	psychologie
Katedra	katedra	k1gFnSc1	katedra
politologie	politologie	k1gFnSc1	politologie
Katedra	katedra	k1gFnSc1	katedra
sociální	sociální	k2eAgFnSc2d1	sociální
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
práce	práce	k1gFnSc2	práce
Katedra	katedra	k1gFnSc1	katedra
mediálních	mediální	k2eAgNnPc2d1	mediální
studií	studio	k1gNnPc2	studio
a	a	k8xC	a
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
Katedra	katedra	k1gFnSc1	katedra
environmentálních	environmentální	k2eAgNnPc2d1	environmentální
studií	studio	k1gNnPc2	studio
Katedra	katedra	k1gFnSc1	katedra
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
evropských	evropský	k2eAgNnPc2d1	Evropské
studií	studio	k1gNnPc2	studio
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
práci	práce	k1gFnSc4	práce
Institut	institut	k1gInSc4	institut
výzkumu	výzkum	k1gInSc2	výzkum
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
rodiny	rodina	k1gFnSc2	rodina
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
politologický	politologický	k2eAgInSc4d1	politologický
ústav	ústav	k1gInSc4	ústav
Ústav	ústava	k1gFnPc2	ústava
populačních	populační	k2eAgFnPc2d1	populační
studií	studie	k1gFnPc2	studie
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
děkanů	děkan	k1gMnPc2	děkan
Fakulty	fakulta	k1gFnSc2	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
studií	studie	k1gFnPc2	studie
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
fakulty	fakulta	k1gFnSc2	fakulta
stojí	stát	k5eAaImIp3nS	stát
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
.	.	kIx.	.
</s>
<s>
Děkana	děkan	k1gMnSc4	děkan
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
fakultního	fakultní	k2eAgInSc2d1	fakultní
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
rektor	rektor	k1gMnSc1	rektor
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
děkana	děkan	k1gMnSc2	děkan
je	být	k5eAaImIp3nS	být
čtyřleté	čtyřletý	k2eAgNnSc1d1	čtyřleté
a	a	k8xC	a
stejná	stejný	k2eAgFnSc1d1	stejná
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
děkanem	děkan	k1gMnSc7	děkan
nejvýše	vysoce	k6eAd3	vysoce
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
proděkany	proděkan	k1gMnPc4	proděkan
a	a	k8xC	a
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
činnosti	činnost	k1gFnSc6	činnost
a	a	k8xC	a
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
3295	[number]	k4	3295
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
