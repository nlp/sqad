<s>
Ale	ale	k9	ale
[	[	kIx(	[
<g/>
vysl	vysl	k1gInSc1	vysl
<g/>
.	.	kIx.	.
ejl	ejl	k?	ejl
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
značně	značně	k6eAd1	značně
různorodé	různorodý	k2eAgFnSc2d1	různorodá
skupiny	skupina	k1gFnSc2	skupina
typů	typ	k1gInPc2	typ
svrchně	svrchně	k6eAd1	svrchně
kvašeného	kvašený	k2eAgNnSc2d1	kvašené
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
původně	původně	k6eAd1	původně
označoval	označovat	k5eAaImAgInS	označovat
pivo	pivo	k1gNnSc4	pivo
vyráběné	vyráběný	k2eAgNnSc4d1	vyráběné
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
chmele	chmel	k1gInSc2	chmel
(	(	kIx(	(
<g/>
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
chmelenému	chmelený	k2eAgNnSc3d1	chmelené
pivu	pivo	k1gNnSc3	pivo
označovanému	označovaný	k2eAgNnSc3d1	označované
jako	jako	k9	jako
beer	beera	k1gFnPc2	beera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
posunul	posunout	k5eAaPmAgInS	posunout
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
skupinu	skupina	k1gFnSc4	skupina
tradičních	tradiční	k2eAgNnPc2d1	tradiční
anglických	anglický	k2eAgNnPc2d1	anglické
<g/>
,	,	kIx,	,
amerických	americký	k2eAgNnPc2d1	americké
a	a	k8xC	a
belgických	belgický	k2eAgNnPc2d1	Belgické
piv	pivo	k1gNnPc2	pivo
<g/>
,	,	kIx,	,
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnPc4	označení
všech	všecek	k3xTgNnPc2	všecek
svrchně	svrchně	k6eAd1	svrchně
kvašených	kvašený	k2eAgNnPc2d1	kvašené
piv	pivo	k1gNnPc2	pivo
(	(	kIx(	(
<g/>
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
termínu	termín	k1gInSc3	termín
ležák	ležák	k1gInSc1	ležák
označujícímu	označující	k2eAgInSc3d1	označující
piva	pivo	k1gNnSc2	pivo
kvašená	kvašený	k2eAgFnSc1d1	kvašená
spodně	spodně	k6eAd1	spodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
širší	široký	k2eAgFnSc2d2	širší
definice	definice	k1gFnSc2	definice
by	by	kYmCp3nP	by
tedy	tedy	k9	tedy
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
ale	ale	k8xC	ale
<g/>
"	"	kIx"	"
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
tradiční	tradiční	k2eAgInPc4d1	tradiční
pivní	pivní	k2eAgInPc4d1	pivní
styly	styl	k1gInPc4	styl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
porter	porter	k1gInSc1	porter
<g/>
,	,	kIx,	,
stout	stout	k1gInSc1	stout
nebo	nebo	k8xC	nebo
německá	německý	k2eAgNnPc1d1	německé
pšeničná	pšeničný	k2eAgNnPc1d1	pšeničné
piva	pivo	k1gNnPc1	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Piva	pivo	k1gNnPc1	pivo
typu	typ	k1gInSc2	typ
ale	ale	k8xC	ale
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
kvasinek	kvasinka	k1gFnPc2	kvasinka
svrchního	svrchní	k2eAgNnSc2d1	svrchní
kvašení	kvašení	k1gNnSc2	kvašení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kvasinek	kvasinka	k1gFnPc2	kvasinka
spodního	spodní	k2eAgNnSc2d1	spodní
kvašení	kvašení	k1gNnSc2	kvašení
fermentují	fermentovat	k5eAaBmIp3nP	fermentovat
pivo	pivo	k1gNnSc4	pivo
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
rychleji	rychle	k6eAd2	rychle
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
významnější	významný	k2eAgNnSc4d2	významnější
množství	množství	k1gNnSc4	množství
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
kvasných	kvasný	k2eAgInPc2d1	kvasný
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
estery	ester	k1gInPc1	ester
či	či	k8xC	či
fenoly	fenol	k1gInPc1	fenol
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
dávají	dávat	k5eAaImIp3nP	dávat
pivu	pivo	k1gNnSc3	pivo
typický	typický	k2eAgInSc4d1	typický
ovocný	ovocný	k2eAgInSc4d1	ovocný
či	či	k8xC	či
kořenný	kořenný	k2eAgInSc4d1	kořenný
nádech	nádech	k1gInSc4	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
piva	pivo	k1gNnPc1	pivo
typu	typ	k1gInSc2	typ
ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
americká	americký	k2eAgFnSc1d1	americká
a	a	k8xC	a
část	část	k1gFnSc1	část
anglických	anglický	k2eAgMnPc2d1	anglický
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
chmelená	chmelený	k2eAgFnSc1d1	chmelená
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
hořkost	hořkost	k1gFnSc4	hořkost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
výrazné	výrazný	k2eAgNnSc4d1	výrazné
aroma	aroma	k1gNnSc4	aroma
některých	některý	k3yIgFnPc2	některý
odrůd	odrůda	k1gFnPc2	odrůda
chmele	chmel	k1gInSc2	chmel
<g/>
,	,	kIx,	,
především	především	k9	především
původem	původ	k1gInSc7	původ
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
přidáním	přidání	k1gNnSc7	přidání
velkých	velký	k2eAgFnPc2d1	velká
dávek	dávka	k1gFnPc2	dávka
chmele	chmel	k1gInSc2	chmel
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
fázích	fáze	k1gFnPc6	fáze
výroby	výroba	k1gFnSc2	výroba
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
chmelových	chmelový	k2eAgFnPc2d1	chmelová
hořkých	hořký	k2eAgFnPc2d1	hořká
kyselin	kyselina	k1gFnPc2	kyselina
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
delší	dlouhý	k2eAgInSc4d2	delší
var	var	k1gInSc4	var
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
rozpustnými	rozpustný	k2eAgFnPc7d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
zůstane	zůstat	k5eAaPmIp3nS	zůstat
zachováno	zachovat	k5eAaPmNgNnS	zachovat
vyšší	vysoký	k2eAgNnSc1d2	vyšší
množství	množství	k1gNnSc1	množství
aromatických	aromatický	k2eAgFnPc2d1	aromatická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
jsou	být	k5eAaImIp3nP	být
těkavé	těkavý	k2eAgInPc1d1	těkavý
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
delším	dlouhý	k2eAgInSc7d2	delší
varem	var	k1gInSc7	var
vyprchávají	vyprchávat	k5eAaImIp3nP	vyprchávat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
postupy	postup	k1gInPc1	postup
mohou	moct	k5eAaImIp3nP	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
přidání	přidání	k1gNnSc4	přidání
chmele	chmel	k1gInSc2	chmel
na	na	k7c6	na
konci	konec	k1gInSc6	konec
chmelovaru	chmelovar	k1gInSc2	chmelovar
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
late	lat	k1gInSc5	lat
addition	addition	k1gInSc4	addition
<g/>
)	)	kIx)	)
i	i	k9	i
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
skončení	skončení	k1gNnSc6	skončení
(	(	kIx(	(
<g/>
flame	flamat	k5eAaPmIp3nS	flamat
out	out	k?	out
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
chlazení	chlazení	k1gNnSc6	chlazení
(	(	kIx(	(
<g/>
hop	hop	k0	hop
stand	stand	k?	stand
<g/>
)	)	kIx)	)
a	a	k8xC	a
čiření	čiření	k1gNnSc1	čiření
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
whirlpool	whirlpoolit	k5eAaPmRp2nS	whirlpoolit
hopping	hopping	k1gInSc1	hopping
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
stáčení	stáčení	k1gNnSc6	stáčení
do	do	k7c2	do
fermentační	fermentační	k2eAgFnSc2d1	fermentační
nádoby	nádoba	k1gFnSc2	nádoba
(	(	kIx(	(
<g/>
v	v	k7c6	v
zařízení	zařízení	k1gNnSc6	zařízení
zvaném	zvaný	k2eAgNnSc6d1	zvané
hopback	hopback	k1gInSc1	hopback
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
fermentace	fermentace	k1gFnSc2	fermentace
a	a	k8xC	a
zrání	zrání	k1gNnSc2	zrání
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
dry	dry	k?	dry
hopping	hopping	k1gInSc1	hopping
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
studené	studený	k2eAgNnSc1d1	studené
chmelení	chmelení	k1gNnSc1	chmelení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgNnSc4d1	silné
chmelení	chmelení	k1gNnSc4	chmelení
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
chmelové	chmelový	k2eAgNnSc4d1	chmelové
aroma	aroma	k1gNnSc4	aroma
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
variantu	varianta	k1gFnSc4	varianta
stylu	styl	k1gInSc2	styl
India	indium	k1gNnSc2	indium
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
(	(	kIx(	(
<g/>
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Piva	pivo	k1gNnSc2	pivo
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
silné	silný	k2eAgNnSc4d1	silné
aroma	aroma	k1gNnSc4	aroma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podle	podle	k7c2	podle
použitých	použitý	k2eAgFnPc2d1	použitá
chmelových	chmelový	k2eAgFnPc2d1	chmelová
odrůd	odrůda	k1gFnPc2	odrůda
nejčastěji	často	k6eAd3	často
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tóny	tón	k1gInPc4	tón
citrusů	citrus	k1gInPc2	citrus
<g/>
,	,	kIx,	,
tropického	tropický	k2eAgNnSc2d1	tropické
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
borového	borový	k2eAgNnSc2d1	borové
jehličí	jehličí	k1gNnSc2	jehličí
či	či	k8xC	či
koření	koření	k1gNnSc2	koření
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
piva	pivo	k1gNnPc1	pivo
Ale	ale	k9	ale
běžně	běžně	k6eAd1	běžně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
a	a	k8xC	a
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
či	či	k8xC	či
východní	východní	k2eAgFnSc1d1	východní
provincie	provincie	k1gFnSc1	provincie
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zavedením	zavedení	k1gNnSc7	zavedení
chmele	chmel	k1gInSc2	chmel
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
ale	ale	k8xC	ale
<g/>
"	"	kIx"	"
používán	používán	k2eAgMnSc1d1	používán
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
nechmelených	chmelený	k2eNgInPc2d1	chmelený
kvašených	kvašený	k2eAgInPc2d1	kvašený
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
beer	beer	k1gInSc1	beer
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
zaveden	zavést	k5eAaPmNgInS	zavést
pro	pro	k7c4	pro
pivo	pivo	k1gNnSc4	pivo
vařené	vařený	k2eAgNnSc4d1	vařené
s	s	k7c7	s
přidáním	přidání	k1gNnSc7	přidání
chmele	chmel	k1gInSc2	chmel
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
obecně	obecně	k6eAd1	obecně
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zhořčující	zhořčující	k2eAgInSc4d1	zhořčující
prostředek	prostředek	k1gInSc4	prostředek
vyrovnávající	vyrovnávající	k2eAgInSc4d1	vyrovnávající
sladkost	sladkost	k1gFnSc4	sladkost
sladu	slad	k1gInSc2	slad
a	a	k8xC	a
působící	působící	k2eAgInPc1d1	působící
jako	jako	k8xC	jako
konzervans	konzervans	k6eAd1	konzervans
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
původního	původní	k2eAgMnSc2d1	původní
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
tímto	tento	k3xDgInSc7	tento
prostředkem	prostředek	k1gInSc7	prostředek
typicky	typicky	k6eAd1	typicky
tzv.	tzv.	kA	tzv.
gruit	gruita	k1gFnPc2	gruita
<g/>
,	,	kIx,	,
směs	směs	k1gFnSc1	směs
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
přidával	přidávat	k5eAaImAgMnS	přidávat
do	do	k7c2	do
mladiny	mladina	k1gFnSc2	mladina
místo	místo	k7c2	místo
chmele	chmel	k1gInSc2	chmel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
ale	ale	k9	ale
prakticky	prakticky	k6eAd1	prakticky
vždy	vždy	k6eAd1	vždy
chmelen	chmelen	k2eAgInSc1d1	chmelen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
pivo	pivo	k1gNnSc1	pivo
typu	typ	k1gInSc2	typ
ale	ale	k8xC	ale
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
chlebem	chléb	k1gInSc7	chléb
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
ale	ale	k8xC	ale
<g/>
"	"	kIx"	"
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
ze	z	k7c2	z
staroanglického	staroanglický	k2eAgInSc2d1	staroanglický
ealu	ealus	k1gInSc2	ealus
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zase	zase	k9	zase
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
protoindoevropského	protoindoevropský	k2eAgInSc2d1	protoindoevropský
*	*	kIx~	*
<g/>
alut-	alut-	k?	alut-
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
spodně	spodně	k6eAd1	spodně
kvašeného	kvašený	k2eAgNnSc2d1	kvašené
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
vařilo	vařit	k5eAaImAgNnS	vařit
hlavně	hlavně	k9	hlavně
pivo	pivo	k1gNnSc1	pivo
pšeničné	pšeničný	k2eAgNnSc1d1	pšeničné
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
ležáku	ležák	k1gInSc2	ležák
ovšem	ovšem	k9	ovšem
upadlo	upadnout	k5eAaPmAgNnS	upadnout
prakticky	prakticky	k6eAd1	prakticky
do	do	k7c2	do
zapomnění	zapomnění	k1gNnSc2	zapomnění
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
např.	např.	kA	např.
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
ovšem	ovšem	k9	ovšem
svrchně	svrchně	k6eAd1	svrchně
kvašené	kvašený	k2eAgNnSc1d1	kvašené
pšeničné	pšeničný	k2eAgNnSc1d1	pšeničné
pivo	pivo	k1gNnSc1	pivo
znovu	znovu	k6eAd1	znovu
zažívá	zažívat	k5eAaImIp3nS	zažívat
rozkvět	rozkvět	k1gInSc1	rozkvět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
minipivovarů	minipivovar	k1gInPc2	minipivovar
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
vařit	vařit	k5eAaImF	vařit
i	i	k9	i
v	v	k7c6	v
zavedených	zavedený	k2eAgInPc6d1	zavedený
velkých	velký	k2eAgInPc6d1	velký
pivovarech	pivovar	k1gInPc6	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
zájem	zájem	k1gInSc4	zájem
i	i	k9	i
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
běžného	běžný	k2eAgMnSc4d1	běžný
českého	český	k2eAgMnSc4d1	český
konzumenta	konzument	k1gMnSc4	konzument
"	"	kIx"	"
<g/>
exotičtější	exotický	k2eAgInSc4d2	exotičtější
<g/>
"	"	kIx"	"
pivní	pivní	k2eAgInPc4d1	pivní
styly	styl	k1gInPc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
si	se	k3xPyFc3	se
začínají	začínat	k5eAaImIp3nP	začínat
získávat	získávat	k5eAaImF	získávat
zejména	zejména	k9	zejména
"	"	kIx"	"
<g/>
ale	ale	k8xC	ale
<g/>
"	"	kIx"	"
moderního	moderní	k2eAgInSc2d1	moderní
amerického	americký	k2eAgInSc2d1	americký
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
aromatická	aromatický	k2eAgNnPc1d1	aromatické
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
chmelená	chmelený	k2eAgFnSc1d1	chmelená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
donedávna	donedávna	k6eAd1	donedávna
prodejem	prodej	k1gInSc7	prodej
převyšoval	převyšovat	k5eAaImAgInS	převyšovat
ležáky	ležák	k1gMnPc4	ležák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
USA	USA	kA	USA
šíří	šířit	k5eAaImIp3nS	šířit
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
trend	trend	k1gInSc1	trend
craft	craft	k1gMnSc1	craft
beer	beer	k1gMnSc1	beer
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
řemeslného	řemeslný	k2eAgNnSc2d1	řemeslné
<g/>
"	"	kIx"	"
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
průmyslovým	průmyslový	k2eAgInPc3d1	průmyslový
produktům	produkt	k1gInPc3	produkt
očekává	očekávat	k5eAaImIp3nS	očekávat
jistá	jistý	k2eAgFnSc1d1	jistá
osobitost	osobitost	k1gFnSc4	osobitost
a	a	k8xC	a
"	"	kIx"	"
<g/>
rukopis	rukopis	k1gInSc1	rukopis
<g/>
"	"	kIx"	"
tvůrce	tvůrce	k1gMnSc1	tvůrce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
pivy	pivo	k1gNnPc7	pivo
mají	mít	k5eAaImIp3nP	mít
silně	silně	k6eAd1	silně
chmelené	chmelený	k2eAgInPc1d1	chmelený
"	"	kIx"	"
<g/>
ale	ale	k8xC	ale
<g/>
"	"	kIx"	"
své	svůj	k3xOyFgNnSc4	svůj
prominentní	prominentní	k2eAgNnSc4d1	prominentní
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgFnPc1d1	anglická
-	-	kIx~	-
Bitter	Bitter	k1gMnSc1	Bitter
<g/>
/	/	kIx~	/
<g/>
English	English	k1gMnSc1	English
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Mild	Mild	k1gMnSc1	Mild
<g/>
,	,	kIx,	,
Southern	Southern	k1gMnSc1	Southern
English	English	k1gMnSc1	English
Brown	Brown	k1gMnSc1	Brown
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Northern	Northern	k1gMnSc1	Northern
English	English	k1gMnSc1	English
Brown	Brown	k1gMnSc1	Brown
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Golden	Goldna	k1gFnPc2	Goldna
Ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
Burton	Burton	k1gInSc4	Burton
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
Ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
English	English	k1gInSc1	English
Barleywine	Barleywin	k1gInSc5	Barleywin
Skotské	skotský	k2eAgNnSc4d1	skotské
a	a	k8xC	a
irské	irský	k2eAgNnSc4d1	irské
-	-	kIx~	-
Scottish	Scottish	k1gInSc4	Scottish
Light	Light	k2eAgInSc4d1	Light
<g/>
/	/	kIx~	/
<g/>
Heavy	Heava	k1gFnSc2	Heava
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
Export	export	k1gInSc4	export
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Strong	Strong	k1gMnSc1	Strong
Scotch	Scotch	k1gMnSc1	Scotch
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Irish	Irish	k1gMnSc1	Irish
Red	Red	k1gMnSc1	Red
Ale	ale	k8xC	ale
Americké	americký	k2eAgFnPc1d1	americká
-	-	kIx~	-
American	American	k1gInSc1	American
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
Amber	ambra	k1gFnPc2	ambra
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
Brown	Brown	k1gMnSc1	Brown
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Cream	Cream	k1gInSc4	Cream
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Blonde	blond	k1gInSc5	blond
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
Barleywine	Barleywin	k1gInSc5	Barleywin
Belgické	belgický	k2eAgInPc1d1	belgický
-	-	kIx~	-
Belgian	Belgian	k1gInSc1	Belgian
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Belgian	Belgian	k1gMnSc1	Belgian
<g />
.	.	kIx.	.
</s>
<s>
Blond	blond	k6eAd1	blond
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Belgian	Belgian	k1gMnSc1	Belgian
Golden	Goldna	k1gFnPc2	Goldna
Strong	Strong	k1gMnSc1	Strong
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Belgian	Belgian	k1gMnSc1	Belgian
Dark	Dark	k1gMnSc1	Dark
Strong	Strong	k1gMnSc1	Strong
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Flanders	Flanders	k1gInSc4	Flanders
Red	Red	k1gFnSc2	Red
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Flanders	Flanders	k1gInSc1	Flanders
Brown	Brown	k1gInSc4	Brown
Ale	ale	k9	ale
(	(	kIx(	(
<g/>
Oud	oud	k1gMnSc1	oud
Bruin	Bruin	k1gMnSc1	Bruin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klášterní	klášterní	k2eAgNnPc1d1	klášterní
piva	pivo	k1gNnPc1	pivo
(	(	kIx(	(
<g/>
Dubbel	Dubbel	k1gInSc1	Dubbel
<g/>
,	,	kIx,	,
Tripel	tripel	k1gInSc1	tripel
<g/>
,	,	kIx,	,
Quadrupel	Quadrupel	k1gMnSc1	Quadrupel
<g/>
)	)	kIx)	)
IPA	IPA	kA	IPA
-	-	kIx~	-
India	indium	k1gNnPc1	indium
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
English	English	k1gInSc4	English
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
American	American	k1gInSc1	American
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
Double	double	k2eAgInSc1d1	double
<g/>
/	/	kIx~	/
<g/>
Imperial	Imperial	k1gInSc1	Imperial
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
nověji	nově	k6eAd2	nově
též	též	k9	též
Red	Red	k1gMnPc1	Red
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
White	Whit	k1gInSc5	Whit
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
IPA	IPA	kA	IPA
(	(	kIx(	(
<g/>
Cascadian	Cascadian	k1gMnSc1	Cascadian
Dark	Dark	k1gMnSc1	Dark
Ale	ale	k9	ale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Belgian	Belgian	k1gMnSc1	Belgian
IPA	IPA	kA	IPA
<g/>
...	...	k?	...
Další	další	k2eAgFnPc1d1	další
svrchně	svrchně	k6eAd1	svrchně
kvašená	kvašený	k2eAgNnPc4d1	kvašené
piva	pivo	k1gNnPc4	pivo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
širší	široký	k2eAgFnSc2d2	širší
definice	definice	k1gFnSc2	definice
typu	typ	k1gInSc2	typ
ale	ale	k8xC	ale
-	-	kIx~	-
stout	stout	k1gInSc1	stout
<g/>
,	,	kIx,	,
porter	porter	k1gInSc1	porter
<g/>
,	,	kIx,	,
altbier	altbier	k1gInSc1	altbier
<g/>
,	,	kIx,	,
kölsch	kölsch	k1gInSc1	kölsch
bavorská	bavorský	k2eAgFnSc1d1	bavorská
pšeničná	pšeničný	k2eAgFnSc1d1	pšeničná
a	a	k8xC	a
žitná	žitná	k1gFnSc1	žitná
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
witbier	witbier	k1gMnSc1	witbier
<g/>
,	,	kIx,	,
saison	saison	k1gMnSc1	saison
<g/>
,	,	kIx,	,
biere	bier	k1gInSc5	bier
de	de	k?	de
garde	garde	k1gNnSc1	garde
<g/>
...	...	k?	...
</s>
