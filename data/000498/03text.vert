<s>
Deimos	Deimos	k1gInSc1	Deimos
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
Δ	Δ	k?	Δ
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Hrůza	hrůza	k6eAd1	hrůza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vnějším	vnější	k2eAgMnSc7d1	vnější
a	a	k8xC	a
menším	malý	k2eAgMnPc3d2	menší
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Deimos	Deimos	k1gInSc1	Deimos
objevil	objevit	k5eAaPmAgInS	objevit
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1877	[number]	k4	1877
Asaph	Asaph	k1gMnSc1	Asaph
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgInPc2d1	pouhý
šest	šest	k4xCc4	šest
dní	den	k1gInPc2	den
před	před	k7c7	před
objevem	objev	k1gInSc7	objev
druhého	druhý	k4xOgInSc2	druhý
měsíce	měsíc	k1gInSc2	měsíc
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
Phobosu	Phobosa	k1gFnSc4	Phobosa
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
existenci	existence	k1gFnSc4	existence
měsíců	měsíc	k1gInPc2	měsíc
Marsu	Mars	k1gInSc2	Mars
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
již	již	k6eAd1	již
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
<g/>
.	.	kIx.	.
</s>
<s>
Předpověď	předpověď	k1gFnSc1	předpověď
měsíce	měsíc	k1gInSc2	měsíc
Phobos	Phobosa	k1gFnPc2	Phobosa
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
dostupných	dostupný	k2eAgFnPc6d1	dostupná
znalostech	znalost	k1gFnPc6	znalost
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgMnSc1	jeden
a	a	k8xC	a
Jupiter	Jupiter	k1gMnSc1	Jupiter
čtyři	čtyři	k4xCgInPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
posloupnosti	posloupnost	k1gFnSc2	posloupnost
se	se	k3xPyFc4	se
vyvozovalo	vyvozovat	k5eAaImAgNnS	vyvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mars	Mars	k1gInSc1	Mars
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
měsíce	měsíc	k1gInPc4	měsíc
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
že	že	k9	že
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Marsem	Mars	k1gInSc7	Mars
a	a	k8xC	a
Jupiterem	Jupiter	k1gMnSc7	Jupiter
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
planeta	planeta	k1gFnSc1	planeta
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
měsíci	měsíc	k1gInPc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dostala	dostat	k5eAaPmAgFnS	dostat
dvojice	dvojice	k1gFnSc1	dvojice
měsíců	měsíc	k1gInPc2	měsíc
i	i	k8xC	i
do	do	k7c2	do
knihy	kniha	k1gFnSc2	kniha
Jonathana	Jonathan	k1gMnSc2	Jonathan
Swifta	Swift	k1gMnSc2	Swift
Gulliverovy	Gulliverův	k2eAgFnSc2d1	Gulliverova
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1726	[number]	k4	1726
popisující	popisující	k2eAgInSc1d1	popisující
objev	objev	k1gInSc1	objev
dvou	dva	k4xCgInPc2	dva
měsíčků	měsíček	k1gInPc2	měsíček
Marsu	Mars	k1gInSc6	Mars
hvězdáři	hvězdář	k1gMnPc1	hvězdář
vymyšlené	vymyšlený	k2eAgFnSc2d1	vymyšlená
země	zem	k1gFnSc2	zem
Laputa	Laput	k1gMnSc2	Laput
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
Deimos	Deimosa	k1gFnPc2	Deimosa
velmi	velmi	k6eAd1	velmi
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
tvaru	tvar	k1gInSc2	tvar
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zachycenou	zachycený	k2eAgFnSc7d1	zachycená
planetkou	planetka	k1gFnSc7	planetka
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc7d1	pocházející
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
planetek	planetka	k1gFnPc2	planetka
<g/>
.	.	kIx.	.
</s>
<s>
Zachycena	zachycen	k2eAgFnSc1d1	zachycena
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
kombinací	kombinace	k1gFnSc7	kombinace
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
poruch	porucha	k1gFnPc2	porucha
působených	působený	k2eAgFnPc2d1	působená
Jupiterem	Jupiter	k1gInSc7	Jupiter
a	a	k8xC	a
samotným	samotný	k2eAgInSc7d1	samotný
Marsem	Mars	k1gInSc7	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
teorie	teorie	k1gFnSc2	teorie
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
oba	dva	k4xCgInPc1	dva
měsíce	měsíc	k1gInPc1	měsíc
vyraženy	vyražen	k2eAgInPc1d1	vyražen
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Protomarsu	Protomars	k1gInSc2	Protomars
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tvorby	tvorba	k1gFnSc2	tvorba
planety	planeta	k1gFnSc2	planeta
akrecí	akrece	k1gFnPc2	akrece
<g/>
,	,	kIx,	,
při	při	k7c6	při
dopadech	dopad	k1gInPc6	dopad
velkých	velký	k2eAgFnPc2d1	velká
planetesimál	planetesimála	k1gFnPc2	planetesimála
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
spektroskopických	spektroskopický	k2eAgNnPc2d1	spektroskopické
měření	měření	k1gNnSc2	měření
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
planetkám	planetka	k1gFnPc3	planetka
typu	typ	k1gInSc2	typ
C	C	kA	C
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
složení	složení	k1gNnSc1	složení
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
uhlíkatým	uhlíkatý	k2eAgInPc3d1	uhlíkatý
chondritům	chondrit	k1gInPc3	chondrit
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
i	i	k9	i
nízká	nízký	k2eAgFnSc1d1	nízká
hustota	hustota	k1gFnSc1	hustota
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
obsahovat	obsahovat	k5eAaImF	obsahovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
uhlíkatých	uhlíkatý	k2eAgFnPc2d1	uhlíkatá
(	(	kIx(	(
<g/>
organických	organický	k2eAgFnPc2d1	organická
<g/>
)	)	kIx)	)
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Phobu	Phob	k1gInSc2	Phob
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
nemá	mít	k5eNaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
tělese	těleso	k1gNnSc6	těleso
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
chemickému	chemický	k2eAgNnSc3d1	chemické
složení	složení	k1gNnSc3	složení
je	být	k5eAaImIp3nS	být
však	však	k9	však
impaktní	impaktní	k2eAgFnSc1d1	impaktní
teorie	teorie	k1gFnSc1	teorie
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
méně	málo	k6eAd2	málo
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Deimos	Deimos	k1gMnSc1	Deimos
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
detekovatelnou	detekovatelný	k2eAgFnSc4d1	detekovatelná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
krátery	kráter	k1gInPc7	kráter
<g/>
,	,	kIx,	,
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jeho	jeho	k3xOp3gNnSc2	jeho
bombardování	bombardování	k1gNnSc2	bombardování
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
částečně	částečně	k6eAd1	částečně
zaplněny	zaplnit	k5eAaPmNgFnP	zaplnit
regolitem	regolit	k1gInSc7	regolit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Deimosu	Deimos	k1gInSc2	Deimos
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
kolem	kolem	k7c2	kolem
10	[number]	k4	10
km	km	kA	km
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
velkého	velký	k2eAgInSc2d1	velký
impaktu	impakt	k1gInSc2	impakt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
stopou	stopa	k1gFnSc7	stopa
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
splynutím	splynutí	k1gNnPc3	splynutí
dvou	dva	k4xCgNnPc2	dva
menších	malý	k2eAgNnPc2d2	menší
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
největší	veliký	k2eAgInPc1d3	veliký
krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgInPc1d1	jediný
dosud	dosud	k6eAd1	dosud
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
max	max	kA	max
<g/>
.	.	kIx.	.
3	[number]	k4	3
km	km	kA	km
<g/>
,	,	kIx,	,
nesou	nést	k5eAaImIp3nP	nést
jména	jméno	k1gNnPc4	jméno
Swift	Swifta	k1gFnPc2	Swifta
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
odstavec	odstavec	k1gInSc1	odstavec
"	"	kIx"	"
<g/>
Deimos	Deimos	k1gInSc1	Deimos
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
IAU	IAU	kA	IAU
byly	být	k5eAaImAgInP	být
útvary	útvar	k1gInPc1	útvar
na	na	k7c6	na
Deimu	Deim	k1gInSc6	Deim
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
po	po	k7c6	po
spisovatelích	spisovatel	k1gMnPc6	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
Deimosu	Deimos	k1gInSc2	Deimos
by	by	kYmCp3nS	by
Mars	Mars	k1gInSc4	Mars
vyhlížel	vyhlížet	k5eAaImAgMnS	vyhlížet
tisíckrát	tisíckrát	k6eAd1	tisíckrát
větší	veliký	k2eAgMnSc1d2	veliký
a	a	k8xC	a
čtyřistakrát	čtyřistakrát	k6eAd1	čtyřistakrát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
úplněk	úplněk	k1gInSc1	úplněk
pozemského	pozemský	k2eAgInSc2d1	pozemský
Měsíce	měsíc	k1gInSc2	měsíc
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Deimos	Deimos	k1gInSc1	Deimos
pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Marsu	Mars	k1gInSc2	Mars
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
areografických	areografický	k2eAgFnPc2d1	areografický
šířek	šířka	k1gFnPc2	šířka
přibližně	přibližně	k6eAd1	přibližně
82,7	[number]	k4	82,7
<g/>
o	o	k7c4	o
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
průměr	průměr	k1gInSc4	průměr
jen	jen	k9	jen
2,5	[number]	k4	2,5
obloukové	obloukový	k2eAgFnSc2d1	oblouková
minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pouhému	pouhý	k2eAgInSc3d1	pouhý
oku	oko	k1gNnSc6	oko
by	by	kYmCp3nS	by
připadal	připadat	k5eAaPmAgInS	připadat
jako	jako	k9	jako
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgFnSc1d1	pohybující
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
dalekohledu	dalekohled	k1gInSc6	dalekohled
by	by	kYmCp3nS	by
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
jeho	jeho	k3xOp3gFnPc4	jeho
fáze	fáze	k1gFnPc4	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
"	"	kIx"	"
<g/>
úplňku	úplněk	k1gInSc6	úplněk
<g/>
"	"	kIx"	"
září	září	k1gNnSc6	září
srovnatelně	srovnatelně	k6eAd1	srovnatelně
s	s	k7c7	s
planetou	planeta	k1gFnSc7	planeta
Venuší	Venuše	k1gFnPc2	Venuše
<g/>
,	,	kIx,	,
v	v	k7c6	v
"	"	kIx"	"
<g/>
první	první	k4xOgFnSc4	první
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
Vega	Veg	k1gInSc2	Veg
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obíhá	obíhat	k5eAaImIp3nS	obíhat
Mars	Mars	k1gInSc1	Mars
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
rotace	rotace	k1gFnSc2	rotace
Marsu	Mars	k1gInSc2	Mars
kolem	kolem	k7c2	kolem
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
zapadá	zapadat	k5eAaPmIp3nS	zapadat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
východem	východ	k1gInSc7	východ
a	a	k8xC	a
západem	západ	k1gInSc7	západ
Deimu	Deim	k1gInSc2	Deim
přibližně	přibližně	k6eAd1	přibližně
2,7	[number]	k4	2,7
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
snímky	snímek	k1gInPc1	snímek
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
z	z	k7c2	z
blízka	blízko	k1gNnSc2	blízko
pořídily	pořídit	k5eAaPmAgFnP	pořídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
sondy	sonda	k1gFnSc2	sonda
Viking	Viking	k1gMnSc1	Viking
1	[number]	k4	1
a	a	k8xC	a
Viking	Viking	k1gMnSc1	Viking
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Družicová	družicový	k2eAgFnSc1d1	družicová
část	část	k1gFnSc1	část
první	první	k4xOgFnSc2	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
měsíci	měsíc	k1gInSc3	měsíc
až	až	k9	až
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
získala	získat	k5eAaPmAgFnS	získat
snímky	snímek	k1gInPc4	snímek
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
až	až	k9	až
3	[number]	k4	3
m.	m.	k?	m.
Z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
měsíců	měsíc	k1gInPc2	měsíc
Marsu	Mars	k1gInSc2	Mars
na	na	k7c4	na
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
sond	sonda	k1gFnPc2	sonda
byly	být	k5eAaImAgInP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
jejich	jejich	k3xOp3gFnSc2	jejich
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
s	s	k7c7	s
relativní	relativní	k2eAgFnSc7d1	relativní
chybou	chyba	k1gFnSc7	chyba
kolem	kolem	k7c2	kolem
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deimos	Deimos	k1gInSc1	Deimos
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
Deima	Deim	k1gMnSc2	Deim
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
boha	bůh	k1gMnSc2	bůh
války	válka	k1gFnSc2	válka
Área	Áres	k1gMnSc2	Áres
(	(	kIx(	(
<g/>
Marta	Marta	k1gFnSc1	Marta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Afrodity	Afrodita	k1gFnSc2	Afrodita
(	(	kIx(	(
<g/>
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Phobos	Phobos	k1gMnSc1	Phobos
společně	společně	k6eAd1	společně
stále	stále	k6eAd1	stále
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
boha	bůh	k1gMnSc4	bůh
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
obou	dva	k4xCgInPc6	dva
měsíců	měsíc	k1gInPc2	měsíc
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Henry	Henry	k1gMnSc1	Henry
Madan	Madan	k1gMnSc1	Madan
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
z	z	k7c2	z
Etonu	Eton	k1gInSc2	Eton
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
citátu	citát	k1gInSc2	citát
z	z	k7c2	z
XV	XV	kA	XV
<g/>
.	.	kIx.	.
knihy	kniha	k1gFnSc2	kniha
eposu	epos	k1gInSc2	epos
Ilias	Ilias	k1gFnSc1	Ilias
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bůh	bůh	k1gMnSc1	bůh
války	válka	k1gFnSc2	válka
povolává	povolávat	k5eAaImIp3nS	povolávat
Strach	strach	k1gInSc1	strach
(	(	kIx(	(
<g/>
Phobos	Phobos	k1gInSc1	Phobos
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hrůzu	hrůza	k1gFnSc4	hrůza
(	(	kIx(	(
<g/>
Deimos	Deimos	k1gInSc4	Deimos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
mytologických	mytologický	k2eAgFnPc2d1	mytologická
pověstí	pověst	k1gFnPc2	pověst
byli	být	k5eAaImAgMnP	být
Deimos	Deimos	k1gMnSc1	Deimos
a	a	k8xC	a
Phobos	Phobos	k1gMnSc1	Phobos
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
zapřažení	zapřažený	k2eAgMnPc1d1	zapřažený
do	do	k7c2	do
Areova	Areův	k2eAgInSc2d1	Areův
válečného	válečný	k2eAgInSc2d1	válečný
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jonathan	Jonathan	k1gMnSc1	Jonathan
Swift	Swift	k1gMnSc1	Swift
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Guliverových	Guliverův	k2eAgFnPc6d1	Guliverův
cestách	cesta	k1gFnPc6	cesta
popisuje	popisovat	k5eAaImIp3nS	popisovat
objev	objev	k1gInSc4	objev
dvou	dva	k4xCgInPc2	dva
měsíčků	měsíček	k1gInPc2	měsíček
Marsu	Mars	k1gInSc6	Mars
hvězdáři	hvězdář	k1gMnPc1	hvězdář
vymyšlené	vymyšlený	k2eAgFnSc2d1	vymyšlená
země	zem	k1gFnSc2	zem
Laputa	Laput	k1gMnSc2	Laput
<g/>
.	.	kIx.	.
</s>
<s>
Swiftův	Swiftův	k2eAgInSc1d1	Swiftův
satirický	satirický	k2eAgInSc1d1	satirický
román	román	k1gInSc1	román
vyšel	vyjít	k5eAaPmAgInS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
půldruhého	půldruhý	k2eAgNnSc2d1	půldruhé
století	století	k1gNnSc2	století
před	před	k7c7	před
skutečným	skutečný	k2eAgInSc7d1	skutečný
objevem	objev	k1gInSc7	objev
Phobu	Phob	k1gInSc2	Phob
a	a	k8xC	a
Deimu	Deim	k1gInSc2	Deim
a	a	k8xC	a
žádný	žádný	k3yNgInSc1	žádný
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
hvězdářský	hvězdářský	k2eAgInSc1d1	hvězdářský
dalekohled	dalekohled	k1gInSc1	dalekohled
nebyl	být	k5eNaImAgInS	být
dostatečně	dostatečně	k6eAd1	dostatečně
výkonný	výkonný	k2eAgInSc1d1	výkonný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
skutečné	skutečný	k2eAgInPc4d1	skutečný
měsíce	měsíc	k1gInPc4	měsíc
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
sci-fi	scii	k1gNnSc2	sci-fi
J.	J.	kA	J.
M.	M.	kA	M.
Troska	troska	k1gFnSc1	troska
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
dílu	díl	k1gInSc6	díl
trilogie	trilogie	k1gFnSc2	trilogie
Zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
nebem	nebe	k1gNnSc7	nebe
(	(	kIx(	(
<g/>
první	první	k4xOgNnPc4	první
vydání	vydání	k1gNnPc4	vydání
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
let	let	k1gInSc4	let
svých	svůj	k3xOyFgMnPc2	svůj
hrdinů	hrdina	k1gMnPc2	hrdina
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
Deimu	Deim	k1gInSc2	Deim
<g/>
.	.	kIx.	.
</s>
<s>
Přistání	přistání	k1gNnSc4	přistání
pilotované	pilotovaný	k2eAgFnSc2d1	pilotovaná
expedice	expedice	k1gFnSc2	expedice
na	na	k7c4	na
Deimu	Deima	k1gFnSc4	Deima
popisuje	popisovat	k5eAaImIp3nS	popisovat
Kim	Kim	k1gMnSc1	Kim
Stanley	Stanlea	k1gFnSc2	Stanlea
Robinson	Robinson	k1gMnSc1	Robinson
v	v	k7c6	v
románu	román	k1gInSc6	román
Green	Green	k2eAgInSc1d1	Green
Mars	Mars	k1gInSc1	Mars
(	(	kIx(	(
<g/>
Zelený	zelený	k2eAgInSc1d1	zelený
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
angl.	angl.	k?	angl.
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
r.	r.	kA	r.
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deimos	Deimos	k1gInSc1	Deimos
je	být	k5eAaImIp3nS	být
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
Marťanský	marťanský	k2eAgInSc4d1	marťanský
styl	styl	k1gInSc4	styl
<g/>
"	"	kIx"	"
spisovatele	spisovatel	k1gMnSc2	spisovatel
Isaaca	Isaacus	k1gMnSc2	Isaacus
Asimova	Asimův	k2eAgMnSc2d1	Asimův
<g/>
.	.	kIx.	.
</s>
