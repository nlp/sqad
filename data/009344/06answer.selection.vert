<s>
Želary	Želara	k1gFnPc1	Želara
jsou	být	k5eAaImIp3nP	být
ucelený	ucelený	k2eAgInSc4d1	ucelený
cyklus	cyklus	k1gInSc4	cyklus
devíti	devět	k4xCc2	devět
povídek	povídka	k1gFnPc2	povídka
české	český	k2eAgFnSc2d1	Česká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Květy	Květa	k1gFnSc2	Květa
Legátové	Legátové	k?	Legátové
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
navzájem	navzájem	k6eAd1	navzájem
propojený	propojený	k2eAgMnSc1d1	propojený
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
