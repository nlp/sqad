<p>
<s>
Želary	Želara	k1gFnPc1	Želara
jsou	být	k5eAaImIp3nP	být
ucelený	ucelený	k2eAgInSc4d1	ucelený
cyklus	cyklus	k1gInSc4	cyklus
devíti	devět	k4xCc2	devět
povídek	povídka	k1gFnPc2	povídka
české	český	k2eAgFnSc2d1	Česká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Květy	Květa	k1gFnSc2	Květa
Legátové	Legátové	k?	Legátové
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
navzájem	navzájem	k6eAd1	navzájem
propojený	propojený	k2eAgMnSc1d1	propojený
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vyšly	vyjít	k5eAaPmAgFnP	vyjít
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Paseka	paseka	k1gFnSc1	paseka
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
obdržely	obdržet	k5eAaPmAgInP	obdržet
Státní	státní	k2eAgFnSc4d1	státní
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
povídek	povídka	k1gFnPc2	povídka
popisuje	popisovat	k5eAaImIp3nS	popisovat
osudy	osud	k1gInPc4	osud
obyvatel	obyvatel	k1gMnPc2	obyvatel
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
zapadlé	zapadlý	k2eAgFnSc2d1	zapadlá
horské	horský	k2eAgFnSc2d1	horská
vesnice	vesnice	k1gFnSc2	vesnice
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
moravsko-slovenském	moravskolovenský	k2eAgNnSc6d1	moravsko-slovenské
pomezí	pomezí	k1gNnSc6	pomezí
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
bídu	bída	k1gFnSc4	bída
v	v	k7c6	v
období	období	k1gNnSc6	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
