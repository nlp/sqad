<s>
Papež	Papež	k1gMnSc1	Papež
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
papa	papa	k1gMnSc1	papa
a	a	k8xC	a
řec.	řec.	k?	řec.
π	π	k?	π
<g/>
,	,	kIx,	,
papas	papas	k1gInSc1	papas
<g/>
,	,	kIx,	,
zdrobnělý	zdrobnělý	k2eAgInSc1d1	zdrobnělý
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
otce	otec	k1gMnPc4	otec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlava	hlava	k1gFnSc1	hlava
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
je	být	k5eAaImIp3nS	být
suverénem	suverén	k1gMnSc7	suverén
Vatikánu	Vatikán	k1gInSc2	Vatikán
a	a	k8xC	a
také	také	k9	také
zároveň	zároveň	k6eAd1	zároveň
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
hierarchie	hierarchie	k1gFnSc2	hierarchie
římskokatolické	římskokatolický	k2eAgFnPc1d1	Římskokatolická
církve	církev	k1gFnPc1	církev
a	a	k8xC	a
biskupem	biskup	k1gInSc7	biskup
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
diecéze	diecéze	k1gFnSc1	diecéze
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
jako	jako	k9	jako
Svatý	svatý	k2eAgInSc1d1	svatý
stolec	stolec	k1gInSc1	stolec
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
též	též	k9	též
Apoštolský	apoštolský	k2eAgInSc1d1	apoštolský
stolec	stolec	k1gInSc1	stolec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
též	též	k9	též
subjektem	subjekt	k1gInSc7	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
katolického	katolický	k2eAgNnSc2d1	katolické
učení	učení	k1gNnSc2	učení
je	být	k5eAaImIp3nS	být
papež	papež	k1gMnSc1	papež
zástupcem	zástupce	k1gMnSc7	zástupce
(	(	kIx(	(
<g/>
náměstkem	náměstek	k1gMnSc7	náměstek
<g/>
)	)	kIx)	)
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
nástupcem	nástupce	k1gMnSc7	nástupce
svatého	svatý	k1gMnSc2	svatý
apoštola	apoštol	k1gMnSc2	apoštol
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
neodvolatelný	odvolatelný	k2eNgMnSc1d1	neodvolatelný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
stávající	stávající	k2eAgMnSc1d1	stávající
papež	papež	k1gMnSc1	papež
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
sejdou	sejít	k5eAaPmIp3nP	sejít
se	se	k3xPyFc4	se
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
volby	volba	k1gFnSc2	volba
nového	nový	k2eAgMnSc4d1	nový
papeže	papež	k1gMnSc4	papež
kardinálové	kardinál	k1gMnPc1	kardinál
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgMnPc1d2	mladší
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c6	na
konkláve	konkláve	k1gNnSc6	konkláve
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
alespoň	alespoň	k9	alespoň
dvoutřetinovou	dvoutřetinový	k2eAgFnSc7d1	dvoutřetinová
většinou	většina	k1gFnSc7	většina
hlasů	hlas	k1gInPc2	hlas
přítomných	přítomný	k2eAgMnPc2d1	přítomný
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
papež	papež	k1gMnSc1	papež
František	František	k1gMnSc1	František
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c6	na
konkláve	konkláve	k1gNnPc6	konkláve
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
k	k	k7c3	k
28	[number]	k4	28
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
tituly	titul	k1gInPc1	titul
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
oficiálními	oficiální	k2eAgInPc7d1	oficiální
papežovými	papežův	k2eAgInPc7d1	papežův
tituly	titul	k1gInPc7	titul
<g/>
:	:	kIx,	:
římský	římský	k2eAgMnSc1d1	římský
biskup	biskup	k1gMnSc1	biskup
(	(	kIx(	(
<g/>
episcopus	episcopus	k1gMnSc1	episcopus
Romanus	Romanus	k1gMnSc1	Romanus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
náměstek	náměstek	k1gMnSc1	náměstek
(	(	kIx(	(
<g/>
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
)	)	kIx)	)
Kristův	Kristův	k2eAgInSc1d1	Kristův
(	(	kIx(	(
<g/>
vicarius	vicarius	k1gInSc4	vicarius
Christi	Christ	k1gMnPc1	Christ
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nástupce	nástupce	k1gMnSc2	nástupce
apoštola	apoštol	k1gMnSc2	apoštol
Petra	Petr	k1gMnSc2	Petr
-	-	kIx~	-
podle	podle	k7c2	podle
teologie	teologie	k1gFnSc2	teologie
Římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
prvního	první	k4xOgMnSc2	první
z	z	k7c2	z
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
;	;	kIx,	;
italský	italský	k2eAgMnSc1d1	italský
primas	primas	k1gMnSc1	primas
(	(	kIx(	(
<g/>
primas	primas	k1gMnSc1	primas
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
a	a	k8xC	a
metropolita	metropolita	k1gMnSc1	metropolita
Římské	římský	k2eAgFnSc2d1	římská
provincie	provincie	k1gFnSc2	provincie
(	(	kIx(	(
<g/>
archiepiscopus	archiepiscopus	k1gMnSc1	archiepiscopus
et	et	k?	et
metropolita	metropolita	k1gMnSc1	metropolita
provinciae	provincia	k1gMnSc2	provincia
Romanae	Romanae	k1gNnSc2	Romanae
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
suverén	suverén	k1gMnSc1	suverén
státu	stát	k1gInSc2	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
;	;	kIx,	;
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
pontifik	pontifik	k1gInSc1	pontifik
(	(	kIx(	(
<g/>
pontifex	pontifex	k1gMnSc1	pontifex
summus	summus	k1gMnSc1	summus
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
také	také	k9	také
původní	původní	k2eAgFnSc2d1	původní
římské	římský	k2eAgFnSc2d1	římská
pontifex	pontifex	k1gMnSc1	pontifex
maximus	maximus	k1gInSc1	maximus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
služebník	služebník	k1gMnSc1	služebník
služebníků	služebník	k1gMnPc2	služebník
Božích	boží	k2eAgMnPc2d1	boží
(	(	kIx(	(
<g/>
servus	servus	k?	servus
servorum	servorum	k1gInSc1	servorum
Dei	Dei	k1gFnSc1	Dei
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
na	na	k7c4	na
listiny	listina	k1gFnPc4	listina
připojuje	připojovat	k5eAaImIp3nS	připojovat
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
jménu	jméno	k1gNnSc3	jméno
pouze	pouze	k6eAd1	pouze
poslední	poslední	k2eAgFnSc4d1	poslední
ze	z	k7c2	z
zmiňovaných	zmiňovaný	k2eAgInPc2d1	zmiňovaný
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
servus	servus	k?	servus
servorum	servorum	k1gInSc4	servorum
Dei	Dei	k1gFnSc2	Dei
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
papež	papež	k1gMnSc1	papež
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
titulem	titul	k1gInSc7	titul
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
a	a	k8xC	a
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
oslovení	oslovení	k1gNnSc1	oslovení
<g/>
:	:	kIx,	:
Vaše	váš	k3xOp2gFnPc4	váš
Svatosti	svatost	k1gFnPc4	svatost
<g/>
;	;	kIx,	;
Svatý	svatý	k1gMnSc5	svatý
Otče	otec	k1gMnSc5	otec
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
užívané	užívaný	k2eAgInPc1d1	užívaný
tituly	titul	k1gInPc1	titul
<g/>
:	:	kIx,	:
patriarcha	patriarcha	k1gMnSc1	patriarcha
Západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
opuštěn	opuštěn	k2eAgMnSc1d1	opuštěn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vikář	vikář	k1gMnSc1	vikář
Apoštolského	apoštolský	k2eAgInSc2d1	apoštolský
stolce	stolec	k1gInSc2	stolec
<g/>
;	;	kIx,	;
vikář	vikář	k1gMnSc1	vikář
Petrův	Petrův	k2eAgMnSc1d1	Petrův
<g/>
.	.	kIx.	.
hlava	hlava	k1gFnSc1	hlava
Svatého	svatý	k2eAgInSc2d1	svatý
Stolce	stolec	k1gInSc2	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
si	se	k3xPyFc3	se
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
vybírají	vybírat	k5eAaImIp3nP	vybírat
papežské	papežský	k2eAgNnSc4d1	papežské
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
současně	současně	k6eAd1	současně
přestávají	přestávat	k5eAaImIp3nP	přestávat
užívat	užívat	k5eAaImF	užívat
své	svůj	k3xOyFgNnSc4	svůj
původní	původní	k2eAgNnSc4d1	původní
občanské	občanský	k2eAgNnSc4d1	občanské
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnSc4	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
římští	římský	k2eAgMnPc1d1	římský
biskupové	biskup	k1gMnPc1	biskup
působili	působit	k5eAaImAgMnP	působit
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Tradici	tradice	k1gFnSc4	tradice
papežského	papežský	k2eAgNnSc2d1	papežské
jména	jméno	k1gNnSc2	jméno
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
533	[number]	k4	533
Mercurius	Mercurius	k1gMnSc1	Mercurius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
nazývat	nazývat	k5eAaImF	nazývat
Janem	Jan	k1gMnSc7	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
podle	podle	k7c2	podle
pohanského	pohanský	k2eAgMnSc2d1	pohanský
boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Volbou	volba	k1gFnSc7	volba
jména	jméno	k1gNnPc1	jméno
papežové	papež	k1gMnPc1	papež
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
příslušného	příslušný	k2eAgMnSc4d1	příslušný
světce	světec	k1gMnSc4	světec
-	-	kIx~	-
patrona	patron	k1gMnSc4	patron
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
i	i	k9	i
návaznost	návaznost	k1gFnSc4	návaznost
na	na	k7c4	na
předchozí	předchozí	k2eAgInSc4d1	předchozí
papeže	papež	k1gMnSc4	papež
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
posledním	poslední	k2eAgMnSc7d1	poslední
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
jméno	jméno	k1gNnSc4	jméno
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1555	[number]	k4	1555
Marcel	Marcela	k1gFnPc2	Marcela
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgInPc7d3	nejčastější
papežskými	papežský	k2eAgInPc7d1	papežský
jmény	jméno	k1gNnPc7	jméno
byli	být	k5eAaImAgMnP	být
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Benedikt	Benedikt	k1gMnSc1	Benedikt
a	a	k8xC	a
Řehoř	Řehoř	k1gMnSc1	Řehoř
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
si	se	k3xPyFc3	se
žádný	žádný	k1gMnSc1	žádný
papež	papež	k1gMnSc1	papež
nezvolil	zvolit	k5eNaPmAgMnS	zvolit
dosud	dosud	k6eAd1	dosud
nepoužité	použitý	k2eNgNnSc4d1	nepoužité
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
průlomem	průlom	k1gInSc7	průlom
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
až	až	k9	až
papež	papež	k1gMnSc1	papež
František	František	k1gMnSc1	František
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
byl	být	k5eAaImAgMnS	být
naposledy	naposledy	k6eAd1	naposledy
prvním	první	k4xOgInPc3	první
svého	svůj	k3xOyFgMnSc2	svůj
jména	jméno	k1gNnPc4	jméno
Lando	Landa	k1gMnSc5	Landa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
913	[number]	k4	913
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
jméno	jméno	k1gNnSc4	jméno
nezvolil	zvolit	k5eNaPmAgMnS	zvolit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ponechal	ponechat	k5eAaPmAgMnS	ponechat
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
rodné	rodný	k2eAgNnSc4d1	rodné
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řady	řada	k1gFnSc2	řada
vybočují	vybočovat	k5eAaImIp3nP	vybočovat
též	též	k9	též
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jako	jako	k9	jako
jediní	jediný	k2eAgMnPc1d1	jediný
používali	používat	k5eAaImAgMnP	používat
dvojici	dvojice	k1gFnSc4	dvojice
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vzhled	vzhled	k1gInSc4	vzhled
papeže	papež	k1gMnSc2	papež
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
bílá	bílý	k2eAgFnSc1d1	bílá
klerika	klerika	k1gFnSc1	klerika
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgNnSc1d1	bílé
cingulum	cingulum	k1gNnSc1	cingulum
a	a	k8xC	a
bílé	bílý	k2eAgNnSc1d1	bílé
solideo	solideo	k1gNnSc1	solideo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
V.	V.	kA	V.
<g/>
,	,	kIx,	,
dominikán	dominikán	k1gMnSc1	dominikán
<g/>
,	,	kIx,	,
nosil	nosit	k5eAaImAgInS	nosit
bílý	bílý	k2eAgInSc1d1	bílý
řádový	řádový	k2eAgInSc1d1	řádový
hábit	hábit	k1gInSc1	hábit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přeneslo	přenést	k5eAaPmAgNnS	přenést
i	i	k9	i
na	na	k7c4	na
nedominikánské	dominikánský	k2eNgMnPc4d1	dominikánský
papeže	papež	k1gMnPc4	papež
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bílé	bílý	k2eAgNnSc1d1	bílé
solideo	solideo	k1gNnSc1	solideo
nosí	nosit	k5eAaImIp3nP	nosit
též	též	k9	též
premonstrátští	premonstrátský	k2eAgMnPc1d1	premonstrátský
opati	opat	k1gMnPc1	opat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
obnovil	obnovit	k5eAaPmAgInS	obnovit
tradici	tradice	k1gFnSc4	tradice
nošení	nošení	k1gNnSc2	nošení
červené	červený	k2eAgFnSc2d1	červená
papežské	papežský	k2eAgFnSc2d1	Papežská
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
pokrývku	pokrývka	k1gFnSc4	pokrývka
začal	začít	k5eAaPmAgInS	začít
nosit	nosit	k5eAaImF	nosit
sametovo-kožešinovou	sametovoožešinový	k2eAgFnSc4d1	sametovo-kožešinový
čepici	čepice	k1gFnSc4	čepice
camauro	camaura	k1gFnSc5	camaura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
papežského	papežský	k2eAgInSc2d1	papežský
šatníku	šatník	k1gInSc2	šatník
patřila	patřit	k5eAaImAgFnS	patřit
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
užíval	užívat	k5eAaImAgMnS	užívat
papež	papež	k1gMnSc1	papež
korunu	koruna	k1gFnSc4	koruna
zvanou	zvaný	k2eAgFnSc4d1	zvaná
tiára	tiára	k1gFnSc1	tiára
<g/>
.	.	kIx.	.
</s>
<s>
Nenosí	nosit	k5eNaImIp3nS	nosit
se	se	k3xPyFc4	se
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
264	[number]	k4	264
<g/>
.	.	kIx.	.
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
svou	svůj	k3xOyFgFnSc4	svůj
osobní	osobní	k2eAgFnSc4d1	osobní
tiáru	tiára	k1gFnSc4	tiára
veřejně	veřejně	k6eAd1	veřejně
odložil	odložit	k5eAaPmAgMnS	odložit
a	a	k8xC	a
prodal	prodat	k5eAaPmAgMnS	prodat
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
daroval	darovat	k5eAaPmAgMnS	darovat
chudým	chudý	k2eAgMnSc7d1	chudý
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Tiára	tiára	k1gFnSc1	tiára
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Neposkvrněné	poskvrněný	k2eNgFnSc2d1	neposkvrněná
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1	Papežská
mozetta	mozetta	k1gFnSc1	mozetta
má	mít	k5eAaImIp3nS	mít
většinou	většinou	k6eAd1	většinou
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
liturgické	liturgický	k2eAgFnSc2d1	liturgická
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
však	však	k9	však
liší	lišit	k5eAaImIp3nS	lišit
materiál	materiál	k1gInSc1	materiál
(	(	kIx(	(
<g/>
satén	satén	k1gInSc1	satén
<g/>
,	,	kIx,	,
samet	samet	k1gInSc1	samet
<g/>
,	,	kIx,	,
serž	serž	k1gInSc1	serž
<g/>
)	)	kIx)	)
či	či	k8xC	či
lemování	lemování	k1gNnSc1	lemování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Velikocemi	Velikoce	k1gFnPc7	Velikoce
a	a	k8xC	a
Letnicemi	letnice	k1gFnPc7	letnice
nosí	nosit	k5eAaImIp3nS	nosit
papež	papež	k1gMnSc1	papež
bílou	bílý	k2eAgFnSc4d1	bílá
damaškovou	damaškový	k2eAgFnSc4d1	damašková
mozettu	mozetta	k1gFnSc4	mozetta
lemovanou	lemovaný	k2eAgFnSc4d1	lemovaná
hermelínem	hermelín	k1gInSc7	hermelín
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
papežství	papežství	k1gNnSc2	papežství
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
katolického	katolický	k2eAgNnSc2d1	katolické
pojetí	pojetí	k1gNnSc2	pojetí
a	a	k8xC	a
podle	podle	k7c2	podle
pojetí	pojetí	k1gNnSc2	pojetí
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
církví	církev	k1gFnPc2	církev
nástupcem	nástupce	k1gMnSc7	nástupce
apoštola	apoštol	k1gMnSc2	apoštol
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
biskupem	biskup	k1gMnSc7	biskup
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
67	[number]	k4	67
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
mučednickou	mučednický	k2eAgFnSc7d1	mučednická
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zmiňovaného	zmiňovaný	k2eAgNnSc2d1	zmiňované
pojetí	pojetí	k1gNnSc2	pojetí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
výkladu	výklad	k1gInSc6	výklad
Matouše	Matouš	k1gMnSc2	Matouš
16,18	[number]	k4	16,18
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gMnPc7	jeho
nástupci	nástupce	k1gMnPc7	nástupce
<g/>
,	,	kIx,	,
přednostní	přednostní	k2eAgNnSc4d1	přednostní
postavení	postavení	k1gNnSc4	postavení
před	před	k7c7	před
ostatními	ostatní	k2eAgMnPc7d1	ostatní
apoštoly	apoštol	k1gMnPc7	apoštol
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
biskupy	biskup	k1gInPc1	biskup
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
list	list	k1gInSc1	list
Klementův	Klementův	k2eAgInSc1d1	Klementův
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
98	[number]	k4	98
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
raným	raný	k2eAgInSc7d1	raný
historickým	historický	k2eAgInSc7d1	historický
dokumentem	dokument	k1gInSc7	dokument
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
dokládat	dokládat	k5eAaImF	dokládat
přednostní	přednostní	k2eAgNnSc4d1	přednostní
postavení	postavení	k1gNnSc4	postavení
římské	římský	k2eAgFnSc2d1	římská
církevní	církevní	k2eAgFnSc2d1	církevní
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
Klement	Klement	k1gMnSc1	Klement
Římský	římský	k2eAgMnSc1d1	římský
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
spory	spor	k1gInPc4	spor
v	v	k7c6	v
Korintě	Korinta	k1gFnSc6	Korinta
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
nijak	nijak	k6eAd1	nijak
nedefinuje	definovat	k5eNaBmIp3nS	definovat
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
psán	psán	k2eAgInSc1d1	psán
v	v	k7c6	v
tónu	tón	k1gInSc6	tón
nadřazeného	nadřazený	k2eAgMnSc2d1	nadřazený
pastýře	pastýř	k1gMnSc2	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
Užití	užití	k1gNnSc1	užití
titulu	titul	k1gInSc2	titul
papež	papež	k1gMnSc1	papež
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
dosvědčeno	dosvědčit	k5eAaPmNgNnS	dosvědčit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
papeže	papež	k1gMnSc2	papež
Marcellina	Marcellin	k2eAgMnSc2d1	Marcellin
(	(	kIx(	(
<g/>
†	†	k?	†
304	[number]	k4	304
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
nazván	nazvat	k5eAaPmNgInS	nazvat
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
náhrobku	náhrobek	k1gInSc6	náhrobek
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
však	však	k9	však
papa	papa	k1gMnSc1	papa
poprvé	poprvé	k6eAd1	poprvé
nazval	nazvat	k5eAaBmAgMnS	nazvat
Siricius	Siricius	k1gMnSc1	Siricius
(	(	kIx(	(
<g/>
385	[number]	k4	385
<g/>
-	-	kIx~	-
<g/>
399	[number]	k4	399
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
fixován	fixovat	k5eAaImNgInS	fixovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Řehoře	Řehoř	k1gMnSc2	Řehoř
Velikého	veliký	k2eAgInSc2d1	veliký
(	(	kIx(	(
<g/>
590	[number]	k4	590
<g/>
-	-	kIx~	-
<g/>
604	[number]	k4	604
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
papa	papa	k1gMnSc1	papa
označoval	označovat	k5eAaImAgMnS	označovat
v	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
dobách	doba	k1gFnPc6	doba
i	i	k8xC	i
ostatní	ostatní	k2eAgMnPc4d1	ostatní
biskupy	biskup	k1gMnPc4	biskup
<g/>
,	,	kIx,	,
patriarchy	patriarcha	k1gMnPc4	patriarcha
a	a	k8xC	a
opaty	opat	k1gMnPc4	opat
především	především	k9	především
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
schizmatu	schizma	k1gNnSc6	schizma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
následovalo	následovat	k5eAaImAgNnS	následovat
po	po	k7c6	po
Chalkedonském	Chalkedonský	k2eAgInSc6d1	Chalkedonský
koncilu	koncil	k1gInSc6	koncil
roku	rok	k1gInSc2	rok
451	[number]	k4	451
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
titulem	titul	k1gInSc7	titul
papež	papež	k1gMnSc1	papež
svou	svůj	k3xOyFgFnSc4	svůj
hlavu	hlava	k1gFnSc4	hlava
i	i	k9	i
alexandrijská	alexandrijský	k2eAgFnSc1d1	Alexandrijská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
a	a	k8xC	a
koptská	koptský	k2eAgFnSc1d1	koptská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
v	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc6d1	Římskokatolická
tradici	tradice	k1gFnSc6	tradice
začal	začít	k5eAaPmAgInS	začít
papežem	papež	k1gMnSc7	papež
nazývat	nazývat	k5eAaImF	nazývat
jedině	jedině	k6eAd1	jedině
římský	římský	k2eAgMnSc1d1	římský
biskup	biskup	k1gMnSc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
papeže	papež	k1gMnSc2	papež
Lva	Lev	k1gMnSc2	Lev
I.	I.	kA	I.
(	(	kIx(	(
<g/>
440	[number]	k4	440
<g/>
-	-	kIx~	-
<g/>
461	[number]	k4	461
<g/>
)	)	kIx)	)
nese	nést	k5eAaImIp3nS	nést
římský	římský	k2eAgMnSc1d1	římský
biskup	biskup	k1gMnSc1	biskup
označení	označení	k1gNnSc2	označení
pontifex	pontifex	k1gMnSc1	pontifex
maximus	maximus	k1gMnSc1	maximus
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
až	až	k9	až
do	do	k7c2	do
císaře	císař	k1gMnSc2	císař
Gratiana	Gratian	k1gMnSc2	Gratian
náleželo	náležet	k5eAaImAgNnS	náležet
pouze	pouze	k6eAd1	pouze
římským	římský	k2eAgMnPc3d1	římský
císařům	císař	k1gMnPc3	císař
coby	coby	k?	coby
nejvyšším	vysoký	k2eAgMnPc3d3	nejvyšší
velekněžím	velekněz	k1gMnPc3	velekněz
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
pontifikát	pontifikát	k1gInSc1	pontifikát
533	[number]	k4	533
<g/>
-	-	kIx~	-
<g/>
535	[number]	k4	535
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
změnil	změnit	k5eAaPmAgInS	změnit
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
rodné	rodný	k2eAgNnSc4d1	rodné
jméno	jméno	k1gNnSc4	jméno
Mercurius	Mercurius	k1gMnSc1	Mercurius
je	být	k5eAaImIp3nS	být
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
římského	římský	k2eAgMnSc4d1	římský
boha	bůh	k1gMnSc4	bůh
Merkuria	Merkurium	k1gNnSc2	Merkurium
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mercurius	Mercurius	k1gInSc1	Mercurius
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc4d1	vhodné
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
autoritu	autorita	k1gFnSc4	autorita
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
klasickém	klasický	k2eAgNnSc6d1	klasické
období	období	k1gNnSc6	období
dějin	dějiny	k1gFnPc2	dějiny
papežství	papežství	k1gNnSc2	papežství
dochází	docházet	k5eAaImIp3nS	docházet
od	od	k7c2	od
konce	konec	k1gInSc2	konec
9	[number]	k4	9
<g/>
.	.	kIx.	.
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
temnému	temný	k2eAgInSc3d1	temný
období	období	k1gNnSc4	období
papežství	papežství	k1gNnSc3	papežství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prestiž	prestiž	k1gFnSc1	prestiž
papežů	papež	k1gMnPc2	papež
výrazně	výrazně	k6eAd1	výrazně
opadla	opadnout	k5eAaPmAgFnS	opadnout
vlivem	vliv	k1gInSc7	vliv
jejich	jejich	k3xOp3gFnSc2	jejich
pokleslé	pokleslý	k2eAgFnSc2d1	pokleslá
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
významné	významný	k2eAgInPc1d1	významný
římské	římský	k2eAgInPc1d1	římský
šlechtické	šlechtický	k2eAgInPc1d1	šlechtický
rody	rod	k1gInPc1	rod
měly	mít	k5eAaImAgInP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
volbu	volba	k1gFnSc4	volba
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
papežů	papež	k1gMnPc2	papež
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
pornokracie	pornokracie	k1gFnSc2	pornokracie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
měnit	měnit	k5eAaImF	měnit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
obnovnými	obnovný	k2eAgNnPc7d1	obnovné
hnutími	hnutí	k1gNnPc7	hnutí
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
(	(	kIx(	(
<g/>
Cluny	Cluna	k1gFnSc2	Cluna
<g/>
,	,	kIx,	,
Hirsau	Hirsaus	k1gInSc2	Hirsaus
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
o	o	k7c4	o
investituru	investitura	k1gFnSc4	investitura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1059	[number]	k4	1059
byla	být	k5eAaImAgFnS	být
volba	volba	k1gFnSc1	volba
nového	nový	k2eAgMnSc2d1	nový
papeže	papež	k1gMnSc2	papež
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
nebo	nebo	k8xC	nebo
odstoupení	odstoupení	k1gNnSc6	odstoupení
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
římského	římský	k2eAgInSc2d1	římský
biskupa	biskup	k1gInSc2	biskup
svěřena	svěřit	k5eAaPmNgFnS	svěřit
nejvyšším	vysoký	k2eAgMnPc3d3	nejvyšší
hodnostářům	hodnostář	k1gMnPc3	hodnostář
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kardinálům	kardinál	k1gMnPc3	kardinál
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
jedině	jedině	k6eAd1	jedině
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
svolanému	svolaný	k2eAgNnSc3d1	svolané
shromáždění	shromáždění	k1gNnSc3	shromáždění
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
konkláve	konkláve	k1gNnSc2	konkláve
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
od	od	k7c2	od
pontifikátu	pontifikát	k1gInSc2	pontifikát
Řehoře	Řehoř	k1gMnSc2	Řehoř
VII	VII	kA	VII
<g/>
.	.	kIx.	.
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
Bonifáce	Bonifác	k1gMnSc2	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
na	na	k7c4	na
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
éru	éra	k1gFnSc4	éra
středověkého	středověký	k2eAgNnSc2d1	středověké
papežství	papežství	k1gNnSc2	papežství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bojovalo	bojovat	k5eAaImAgNnS	bojovat
o	o	k7c4	o
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
nad	nad	k7c7	nad
světskými	světský	k2eAgMnPc7d1	světský
vládci	vládce	k1gMnPc7	vládce
a	a	k8xC	a
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
například	například	k6eAd1	například
za	za	k7c2	za
pontifikátu	pontifikát	k1gInSc2	pontifikát
Inocence	Inocenc	k1gMnSc2	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
významných	významný	k2eAgInPc2d1	významný
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1309	[number]	k4	1309
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
Klement	Klement	k1gMnSc1	Klement
V.	V.	kA	V.
papežskou	papežský	k2eAgFnSc4d1	Papežská
rezidenci	rezidence	k1gFnSc4	rezidence
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Augusta	Augusta	k1gMnSc1	Augusta
do	do	k7c2	do
francouzského	francouzský	k2eAgInSc2d1	francouzský
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
papežové	papež	k1gMnPc1	papež
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1377	[number]	k4	1377
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
avignonské	avignonský	k2eAgNnSc1d1	avignonské
zajetí	zajetí	k1gNnSc1	zajetí
mělo	mít	k5eAaImAgNnS	mít
dohru	dohra	k1gFnSc4	dohra
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
schizmatu	schizma	k1gNnSc6	schizma
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
současně	současně	k6eAd1	současně
vládli	vládnout	k5eAaImAgMnP	vládnout
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
Avignonu	Avignon	k1gInSc6	Avignon
dva	dva	k4xCgMnPc1	dva
papežové	papež	k1gMnPc1	papež
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1409	[number]	k4	1409
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pisánský	pisánský	k2eAgInSc4d1	pisánský
koncil	koncil	k1gInSc4	koncil
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Alexandra	Alexandr	k1gMnSc4	Alexandr
V.	V.	kA	V.
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
tři	tři	k4xCgFnPc1	tři
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
roztržku	roztržka	k1gFnSc4	roztržka
ukončil	ukončit	k5eAaPmAgInS	ukončit
teprve	teprve	k6eAd1	teprve
kostnický	kostnický	k2eAgInSc1d1	kostnický
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roku	rok	k1gInSc2	rok
1417	[number]	k4	1417
zvolil	zvolit	k5eAaPmAgMnS	zvolit
nového	nový	k2eAgMnSc4d1	nový
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
Martina	Martin	k1gMnSc4	Martin
V.	V.	kA	V.
(	(	kIx(	(
<g/>
1417	[number]	k4	1417
<g/>
-	-	kIx~	-
<g/>
1431	[number]	k4	1431
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
renesančních	renesanční	k2eAgMnPc2d1	renesanční
papežů	papež	k1gMnPc2	papež
Julia	Julius	k1gMnSc2	Julius
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Lva	lev	k1gInSc2	lev
X.	X.	kA	X.
a	a	k8xC	a
Klementa	Klement	k1gMnSc2	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
přichází	přicházet	k5eAaImIp3nS	přicházet
období	období	k1gNnSc4	období
tzv.	tzv.	kA	tzv.
katolické	katolický	k2eAgFnSc2d1	katolická
reformace	reformace	k1gFnSc2	reformace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
protestantská	protestantský	k2eAgFnSc1d1	protestantská
reformace	reformace	k1gFnSc1	reformace
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
Tridentský	tridentský	k2eAgInSc1d1	tridentský
koncil	koncil	k1gInSc1	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
významný	významný	k2eAgInSc1d1	významný
milník	milník	k1gInSc1	milník
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
vatikánským	vatikánský	k2eAgInSc7d1	vatikánský
koncilem	koncil	k1gInSc7	koncil
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
definoval	definovat	k5eAaBmAgInS	definovat
dogma	dogma	k1gNnSc4	dogma
o	o	k7c6	o
papežské	papežský	k2eAgFnSc6d1	Papežská
neomylnosti	neomylnost	k1gFnSc6	neomylnost
a	a	k8xC	a
přiznal	přiznat	k5eAaPmAgMnS	přiznat
papeži	papež	k1gMnSc3	papež
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
papež	papež	k1gMnSc1	papež
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
ověřuje	ověřovat	k5eAaImIp3nS	ověřovat
kardinál	kardinál	k1gMnSc1	kardinál
kamerlengo	kamerlengo	k1gMnSc1	kamerlengo
(	(	kIx(	(
<g/>
komoří	komoří	k1gMnSc1	komoří
<g/>
)	)	kIx)	)
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
třikrát	třikrát	k6eAd1	třikrát
osloví	oslovit	k5eAaPmIp3nS	oslovit
papeže	papež	k1gMnSc4	papež
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
oslovení	oslovení	k1gNnSc1	oslovení
bez	bez	k7c2	bez
odezvy	odezva	k1gFnSc2	odezva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
papež	papež	k1gMnSc1	papež
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
papežovu	papežův	k2eAgFnSc4d1	papežova
smrt	smrt	k1gFnSc4	smrt
ověřuje	ověřovat	k5eAaImIp3nS	ověřovat
lékařský	lékařský	k2eAgInSc1d1	lékařský
personál	personál	k1gInSc1	personál
a	a	k8xC	a
ověření	ověření	k1gNnSc4	ověření
kardinálem	kardinál	k1gMnSc7	kardinál
kamerlengem	kamerleng	k1gInSc7	kamerleng
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
rituální	rituální	k2eAgNnSc1d1	rituální
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
musí	muset	k5eAaImIp3nS	muset
poté	poté	k6eAd1	poté
potvrdit	potvrdit	k5eAaPmF	potvrdit
úmrtní	úmrtní	k2eAgInSc4d1	úmrtní
list	list	k1gInSc4	list
a	a	k8xC	a
uvědomit	uvědomit	k5eAaPmF	uvědomit
veřejnost	veřejnost	k1gFnSc4	veřejnost
přes	přes	k7c4	přes
kardinála	kardinál	k1gMnSc4	kardinál
vikáře	vikář	k1gMnSc4	vikář
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
kamerlengo	kamerlengo	k1gMnSc1	kamerlengo
zapečetí	zapečetit	k5eAaPmIp3nS	zapečetit
papežovy	papežův	k2eAgFnPc4d1	papežova
soukromé	soukromý	k2eAgFnPc4d1	soukromá
komnaty	komnata	k1gFnPc4	komnata
a	a	k8xC	a
také	také	k9	také
zajistí	zajistit	k5eAaPmIp3nS	zajistit
rozlomení	rozlomení	k1gNnSc4	rozlomení
Rybářova	Rybářův	k2eAgInSc2d1	Rybářův
prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
papežovy	papežův	k2eAgFnSc2d1	papežova
pečetě	pečeť	k1gFnSc2	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
zařízení	zařízení	k1gNnSc2	zařízení
papežova	papežův	k2eAgInSc2d1	papežův
pohřbu	pohřeb	k1gInSc2	pohřeb
a	a	k8xC	a
novemdieles	novemdielesa	k1gFnPc2	novemdielesa
(	(	kIx(	(
<g/>
devět	devět	k4xCc1	devět
dní	den	k1gInPc2	den
smutku	smutek	k1gInSc2	smutek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
abdikace	abdikace	k1gFnSc2	abdikace
papeže	papež	k1gMnSc2	papež
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
komnaty	komnata	k1gFnPc1	komnata
jsou	být	k5eAaImIp3nP	být
zapečetěny	zapečetit	k5eAaPmNgFnP	zapečetit
a	a	k8xC	a
prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
pečeť	pečeť	k1gFnSc4	pečeť
zničeny	zničit	k5eAaPmNgInP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgMnSc7d1	jediný
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
<g/>
,	,	kIx,	,
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
i	i	k9	i
jako	jako	k9	jako
emeritní	emeritní	k2eAgMnSc1d1	emeritní
papež	papež	k1gMnSc1	papež
ponechal	ponechat	k5eAaPmAgMnS	ponechat
papežské	papežský	k2eAgNnSc4d1	papežské
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Konkláve	konkláve	k1gNnSc2	konkláve
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
sborem	sbor	k1gInSc7	sbor
kardinálů	kardinál	k1gMnPc2	kardinál
ve	v	k7c6	v
volbě	volba	k1gFnSc6	volba
zvané	zvaný	k2eAgNnSc4d1	zvané
konkláve	konkláve	k1gNnSc4	konkláve
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
konkláve	konkláve	k1gNnSc2	konkláve
přísahají	přísahat	k5eAaImIp3nP	přísahat
kardinálové	kardinál	k1gMnPc1	kardinál
<g/>
,	,	kIx,	,
že	že	k8xS	že
dodrží	dodržet	k5eAaPmIp3nP	dodržet
pravidla	pravidlo	k1gNnPc1	pravidlo
stanovená	stanovený	k2eAgNnPc1d1	stanovené
papežem	papež	k1gMnSc7	papež
a	a	k8xC	a
neprozradí	prozradit	k5eNaPmIp3nS	prozradit
nic	nic	k3yNnSc4	nic
z	z	k7c2	z
hlasování	hlasování	k1gNnSc2	hlasování
či	či	k8xC	či
samotných	samotný	k2eAgNnPc2d1	samotné
jednání	jednání	k1gNnPc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozesadí	rozesadit	k5eAaPmIp3nP	rozesadit
podél	podél	k7c2	podél
zdí	zeď	k1gFnPc2	zeď
Sixtinské	sixtinský	k2eAgFnSc2d1	Sixtinská
kaple	kaple	k1gFnSc2	kaple
a	a	k8xC	a
přijmou	přijmout	k5eAaPmIp3nP	přijmout
hlasovací	hlasovací	k2eAgInSc4d1	hlasovací
lístek	lístek	k1gInSc4	lístek
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Eligo	Eligo	k6eAd1	Eligo
v	v	k7c6	v
summum	summum	k?	summum
pontificem	pontific	k1gMnSc7	pontific
...	...	k?	...
"	"	kIx"	"
(	(	kIx(	(
<g/>
já	já	k3xPp1nSc1	já
volím	volit	k5eAaImIp1nS	volit
za	za	k7c4	za
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
pontifika	pontifex	k1gMnSc4	pontifex
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napíší	napsat	k5eAaPmIp3nP	napsat
jméno	jméno	k1gNnSc4	jméno
kandidáta	kandidát	k1gMnSc2	kandidát
a	a	k8xC	a
jednotlivě	jednotlivě	k6eAd1	jednotlivě
pak	pak	k6eAd1	pak
chodí	chodit	k5eAaImIp3nS	chodit
k	k	k7c3	k
oltáři	oltář	k1gInSc3	oltář
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
ciborium	ciborium	k1gNnSc1	ciborium
(	(	kIx(	(
<g/>
kalich	kalich	k1gInSc1	kalich
s	s	k7c7	s
talířkem	talířek	k1gInSc7	talířek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyzdvihnou	vyzdvihnout	k5eAaPmIp3nP	vyzdvihnout
lístek	lístek	k1gInSc4	lístek
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ukázali	ukázat	k5eAaPmAgMnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
volili	volit	k5eAaImAgMnP	volit
<g/>
.	.	kIx.	.
</s>
<s>
Položí	položit	k5eAaPmIp3nP	položit
lístek	lístek	k1gInSc4	lístek
na	na	k7c4	na
talířek	talířek	k1gInSc4	talířek
a	a	k8xC	a
sesunou	sesunout	k5eAaPmIp3nP	sesunout
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
kalichu	kalich	k1gInSc2	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Hlasy	hlas	k1gInPc1	hlas
jsou	být	k5eAaImIp3nP	být
sčítány	sčítat	k5eAaImNgInP	sčítat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
volitelem	volitel	k1gMnSc7	volitel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
třemi	tři	k4xCgInPc7	tři
asistenty	asistent	k1gMnPc7	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
asistentů	asistent	k1gMnPc2	asistent
přečte	přečíst	k5eAaPmIp3nS	přečíst
jméno	jméno	k1gNnSc4	jméno
na	na	k7c6	na
lístku	lístek	k1gInSc6	lístek
nahlas	nahlas	k6eAd1	nahlas
a	a	k8xC	a
zapíše	zapsat	k5eAaPmIp3nS	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
asistent	asistent	k1gMnSc1	asistent
propíchne	propíchnout	k5eAaPmIp3nS	propíchnout
střed	střed	k1gInSc1	střed
lístku	lístek	k1gInSc6	lístek
jehlou	jehla	k1gFnSc7	jehla
a	a	k8xC	a
navleče	navléct	k5eAaBmIp3nS	navléct
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
nit	nit	k1gFnSc4	nit
<g/>
.	.	kIx.	.
</s>
<s>
Lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
volbě	volba	k1gFnSc6	volba
spáleny	spálen	k2eAgInPc1d1	spálen
s	s	k7c7	s
chemikáliemi	chemikálie	k1gFnPc7	chemikálie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
černý	černý	k2eAgInSc4d1	černý
kouř	kouř	k1gInSc4	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
papež	papež	k1gMnSc1	papež
zvolen	zvolen	k2eAgMnSc1d1	zvolen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
přidány	přidán	k2eAgFnPc4d1	přidána
jiné	jiný	k2eAgFnPc4d1	jiná
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
kouř	kouř	k1gInSc1	kouř
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
najevo	najevo	k6eAd1	najevo
lidem	lid	k1gInSc7	lid
venku	venek	k1gInSc2	venek
mimo	mimo	k7c4	mimo
konkláve	konkláve	k1gNnSc4	konkláve
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
