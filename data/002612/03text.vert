<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1901	[number]	k4	1901
Nezvěstice	Nezvěstika	k1gFnSc6	Nezvěstika
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1952	[number]	k4	1952
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
komunistický	komunistický	k2eAgMnSc1d1	komunistický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
člen	člen	k1gMnSc1	člen
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popraven	popraven	k2eAgMnSc1d1	popraven
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
politických	politický	k2eAgInPc2d1	politický
procesů	proces	k1gInPc2	proces
počátkem	počátkem	k7c2	počátkem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
v	v	k7c6	v
Nezvěsticích	Nezvěstik	k1gMnPc6	Nezvěstik
na	na	k7c6	na
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
židovského	židovský	k2eAgMnSc2d1	židovský
obchodníka	obchodník	k1gMnSc2	obchodník
Šimona	Šimon	k1gMnSc2	Šimon
Slánského	Slánský	k1gMnSc2	Slánský
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
netrpěla	trpět	k5eNaImAgFnS	trpět
chudobou	chudoba	k1gFnSc7	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
stýkat	stýkat	k5eAaImF	stýkat
s	s	k7c7	s
levicově	levicově	k6eAd1	levicově
zaměřenými	zaměřený	k2eAgMnPc7d1	zaměřený
intelektuály	intelektuál	k1gMnPc7	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
od	od	k7c2	od
Československé	československý	k2eAgFnSc2d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
dělnické	dělnický	k2eAgNnSc4d1	dělnické
odtrhlo	odtrhnout	k5eAaPmAgNnS	odtrhnout
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
levicové	levicový	k2eAgNnSc1d1	levicové
jádro	jádro	k1gNnSc1	jádro
a	a	k8xC	a
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
strany	strana	k1gFnSc2	strana
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
na	na	k7c6	na
V.	V.	kA	V.
sjezdu	sjezd	k1gInSc2	sjezd
KSČ	KSČ	kA	KSČ
získalo	získat	k5eAaPmAgNnS	získat
moc	moc	k6eAd1	moc
stalinistické	stalinistický	k2eAgNnSc1d1	stalinistické
jádro	jádro	k1gNnSc1	jádro
kolem	kolem	k7c2	kolem
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
opustila	opustit	k5eAaPmAgFnS	opustit
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
následné	následný	k2eAgFnSc2d1	následná
volby	volba	k1gFnSc2	volba
skončily	skončit	k5eAaPmAgInP	skončit
pro	pro	k7c4	pro
KSČ	KSČ	kA	KSČ
výraznou	výrazný	k2eAgFnSc7d1	výrazná
ztrátou	ztráta	k1gFnSc7	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Slánskému	Slánský	k1gMnSc3	Slánský
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
začal	začít	k5eAaPmAgInS	začít
stoupat	stoupat	k5eAaImF	stoupat
ve	v	k7c6	v
stranických	stranický	k2eAgFnPc6d1	stranická
funkcích	funkce	k1gFnPc6	funkce
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
získal	získat	k5eAaPmAgMnS	získat
poslanecké	poslanecký	k2eAgNnSc4d1	poslanecké
křeslo	křeslo	k1gNnSc4	křeslo
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
ČSR	ČSR	kA	ČSR
za	za	k7c4	za
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
zasedal	zasedat	k5eAaImAgInS	zasedat
až	až	k6eAd1	až
do	do	k7c2	do
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
podpisu	podpis	k1gInSc6	podpis
záboru	zábor	k1gInSc2	zábor
Sudet	Sudety	k1gFnPc2	Sudety
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
okamžitě	okamžitě	k6eAd1	okamžitě
opustil	opustit	k5eAaPmAgMnS	opustit
vlast	vlast	k1gFnSc4	vlast
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
odvezena	odvézt	k5eAaPmNgFnS	odvézt
do	do	k7c2	do
ghetta	ghetto	k1gNnSc2	ghetto
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
živý	živý	k1gMnSc1	živý
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ze	z	k7c2	z
Slánského	Slánského	k2eAgFnSc2d1	Slánského
rodiny	rodina	k1gFnSc2	rodina
přežil	přežít	k5eAaPmAgMnS	přežít
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přes	přes	k7c4	přes
válku	válka	k1gFnSc4	válka
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Slánský	Slánský	k1gMnSc1	Slánský
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
Moskevského	moskevský	k2eAgInSc2d1	moskevský
rozhlasu	rozhlas	k1gInSc2	rozhlas
pro	pro	k7c4	pro
Československo	Československo	k1gNnSc4	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
prožil	prožít	k5eAaPmAgInS	prožít
německý	německý	k2eAgInSc1d1	německý
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
mocnými	mocný	k2eAgMnPc7d1	mocný
komunistickými	komunistický	k2eAgMnPc7d1	komunistický
hodnostáři	hodnostář	k1gMnPc7	hodnostář
i	i	k8xC	i
s	s	k7c7	s
brutalitou	brutalita	k1gFnSc7	brutalita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
panovala	panovat	k5eAaImAgFnS	panovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
unesena	unesen	k2eAgFnSc1d1	unesena
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Naděžda	Naděžda	k1gFnSc1	Naděžda
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
komunisté	komunista	k1gMnPc1	komunista
chtěli	chtít	k5eAaImAgMnP	chtít
mít	mít	k5eAaImF	mít
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
S	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k9	už
nikdy	nikdy	k6eAd1	nikdy
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
zplnomocněncem	zplnomocněnec	k1gMnSc7	zplnomocněnec
československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
organizoval	organizovat	k5eAaBmAgMnS	organizovat
politické	politický	k2eAgFnPc4d1	politická
přednášky	přednáška	k1gFnPc4	přednáška
československým	československý	k2eAgMnPc3d1	československý
partyzánům	partyzán	k1gMnPc3	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
letecky	letecky	k6eAd1	letecky
dopraven	dopravit	k5eAaPmNgInS	dopravit
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Tri	Tri	k1gMnSc2	Tri
Duby	Duba	k1gMnSc2	Duba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Švermou	Šverma	k1gMnSc7	Šverma
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
štábu	štáb	k1gInSc6	štáb
povstalců	povstalec	k1gMnPc2	povstalec
jako	jako	k8xC	jako
komunistický	komunistický	k2eAgMnSc1d1	komunistický
delegát	delegát	k1gMnSc1	delegát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
patřil	patřit	k5eAaImAgInS	patřit
po	po	k7c4	po
Klementu	Klement	k1gMnSc3	Klement
Gottwaldovi	Gottwaldův	k2eAgMnPc1d1	Gottwaldův
k	k	k7c3	k
nejmocnějším	mocný	k2eAgMnPc3d3	nejmocnější
mužům	muž	k1gMnPc3	muž
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
prozatímního	prozatímní	k2eAgInSc2d1	prozatímní
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ho	on	k3xPp3gInSc4	on
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
IX	IX	kA	IX
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
duben	duben	k1gInSc1	duben
1945	[number]	k4	1945
-	-	kIx~	-
listopad	listopad	k1gInSc1	listopad
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
1951	[number]	k4	1951
byl	být	k5eAaImAgMnS	být
i	i	k9	i
členem	člen	k1gMnSc7	člen
sekretariátu	sekretariát	k1gInSc2	sekretariát
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1951	[number]	k4	1951
zastával	zastávat	k5eAaImAgInS	zastávat
stranický	stranický	k2eAgInSc1d1	stranický
post	post	k1gInSc1	post
člena	člen	k1gMnSc2	člen
politického	politický	k2eAgInSc2d1	politický
sekretariátu	sekretariát	k1gInSc2	sekretariát
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
do	do	k7c2	do
září	září	k1gNnSc2	září
1951	[number]	k4	1951
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc3	funkce
člena	člen	k1gMnSc4	člen
organizačního	organizační	k2eAgInSc2d1	organizační
sekretariátu	sekretariát	k1gInSc2	sekretariát
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
politické	politický	k2eAgFnSc2d1	politická
komise	komise	k1gFnSc2	komise
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
užšího	úzký	k2eAgNnSc2d2	užší
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
jeho	jeho	k3xOp3gInSc7	jeho
stranickým	stranický	k2eAgInSc7d1	stranický
postem	post	k1gInSc7	post
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
funkce	funkce	k1gFnSc1	funkce
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zastával	zastávat	k5eAaImAgMnS	zastávat
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Trvale	trvale	k6eAd1	trvale
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
zákonodárném	zákonodárný	k2eAgInSc6d1	zákonodárný
sboru	sbor	k1gInSc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poslancem	poslanec	k1gMnSc7	poslanec
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
za	za	k7c4	za
komunisty	komunista	k1gMnPc4	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
za	za	k7c4	za
KSČ	KSČ	kA	KSČ
poslancem	poslanec	k1gMnSc7	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
ve	v	k7c6	v
volebním	volební	k2eAgInSc6d1	volební
kraji	kraj	k1gInSc6	kraj
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
rezignace	rezignace	k1gFnSc2	rezignace
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ho	on	k3xPp3gInSc4	on
jako	jako	k9	jako
náhradník	náhradník	k1gMnSc1	náhradník
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Josef	Josef	k1gMnSc1	Josef
Kestler	Kestler	k1gMnSc1	Kestler
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
únorového	únorový	k2eAgInSc2d1	únorový
převratu	převrat	k1gInSc2	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
převzala	převzít	k5eAaPmAgFnS	převzít
KSČ	KSČ	kA	KSČ
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
eliminovala	eliminovat	k5eAaBmAgFnS	eliminovat
opozici	opozice	k1gFnSc4	opozice
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
organizátorů	organizátor	k1gMnPc2	organizátor
komunistického	komunistický	k2eAgInSc2d1	komunistický
teroru	teror	k1gInSc2	teror
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
období	období	k1gNnSc6	období
září	září	k1gNnSc2	září
-	-	kIx~	-
listopad	listopad	k1gInSc1	listopad
zastával	zastávat	k5eAaImAgInS	zastávat
post	post	k1gInSc1	post
náměstka	náměstek	k1gMnSc2	náměstek
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
vicepremiéra	vicepremiér	k1gMnSc2	vicepremiér
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Antonína	Antonín	k1gMnSc2	Antonín
Zápotockého	Zápotockého	k2eAgMnSc2d1	Zápotockého
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
nabrala	nabrat	k5eAaPmAgFnS	nabrat
strmý	strmý	k2eAgInSc4d1	strmý
pád	pád	k1gInSc4	pád
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Proces	proces	k1gInSc1	proces
se	s	k7c7	s
Slánským	Slánský	k1gMnSc7	Slánský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
SSSR	SSSR	kA	SSSR
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
Izraele	Izrael	k1gInSc2	Izrael
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
celý	celý	k2eAgInSc1d1	celý
východní	východní	k2eAgInSc1d1	východní
blok	blok	k1gInSc1	blok
proarabskou	proarabský	k2eAgFnSc4d1	proarabská
a	a	k8xC	a
antisionistickou	antisionistický	k2eAgFnSc4d1	antisionistická
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
výběr	výběr	k1gInSc4	výběr
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
procesu	proces	k1gInSc2	proces
se	se	k3xPyFc4	se
Slánským	Slánský	k1gMnSc7	Slánský
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1951	[number]	k4	1951
v	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
byl	být	k5eAaImAgInS	být
zadržen	zadržet	k5eAaPmNgInS	zadržet
a	a	k8xC	a
předán	předat	k5eAaPmNgInS	předat
do	do	k7c2	do
ruzyňské	ruzyňský	k2eAgFnSc2d1	Ruzyňská
věznice	věznice	k1gFnSc2	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Dožadoval	dožadovat	k5eAaImAgInS	dožadovat
se	se	k3xPyFc4	se
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
ledna	leden	k1gInSc2	leden
1952	[number]	k4	1952
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
oběsit	oběsit	k5eAaPmF	oběsit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
neúspěšnou	úspěšný	k2eNgFnSc4d1	neúspěšná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
když	když	k8xS	když
hlavou	hlava	k1gFnSc7	hlava
narážel	narážet	k5eAaPmAgMnS	narážet
o	o	k7c4	o
topení	topení	k1gNnSc4	topení
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
obžalováno	obžalovat	k5eAaPmNgNnS	obžalovat
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
14	[number]	k4	14
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
členů	člen	k1gMnPc2	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
byla	být	k5eAaImAgFnS	být
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
např.	např.	kA	např.
Artur	Artur	k1gMnSc1	Artur
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Vavro	Vavro	k1gNnSc1	Vavro
Hajdů	Hajd	k1gInPc2	Hajd
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Löbl	Löbl	k1gMnSc1	Löbl
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Margolius	Margolius	k1gMnSc1	Margolius
<g/>
,	,	kIx,	,
André	André	k1gMnSc5	André
Simone	Simon	k1gMnSc5	Simon
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Fischl	Fischl	k1gMnSc1	Fischl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
věznění	vězněný	k2eAgMnPc1d1	vězněný
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
naučit	naučit	k5eAaPmF	naučit
nazpaměť	nazpaměť	k6eAd1	nazpaměť
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
před	před	k7c7	před
Státním	státní	k2eAgInSc7d1	státní
soudem	soud	k1gInSc7	soud
doslova	doslova	k6eAd1	doslova
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
žalobcem	žalobce	k1gMnSc7	žalobce
tohoto	tento	k3xDgInSc2	tento
největšího	veliký	k2eAgInSc2d3	veliký
vykonstruovaného	vykonstruovaný	k2eAgInSc2d1	vykonstruovaný
procesu	proces	k1gInSc2	proces
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Urválek	Urválek	k1gMnSc1	Urválek
<g/>
;	;	kIx,	;
Slánského	Slánského	k2eAgMnSc7d1	Slánského
advokátem	advokát	k1gMnSc7	advokát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Slánský	Slánský	k1gMnSc1	Slánský
byl	být	k5eAaImAgMnS	být
obžalován	obžalovat	k5eAaPmNgMnS	obžalovat
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
<g/>
,	,	kIx,	,
velezrady	velezrada	k1gFnSc2	velezrada
<g/>
,	,	kIx,	,
sabotáže	sabotáž	k1gFnSc2	sabotáž
a	a	k8xC	a
z	z	k7c2	z
prozrazení	prozrazení	k1gNnSc2	prozrazení
vojenského	vojenský	k2eAgNnSc2d1	vojenské
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
všem	všecek	k3xTgInPc3	všecek
obviněním	obvinění	k1gNnSc7	obvinění
se	se	k3xPyFc4	se
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
přiznání	přiznání	k1gNnSc1	přiznání
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
vynucené	vynucený	k2eAgNnSc1d1	vynucené
mučením	mučení	k1gNnSc7	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
oběšením	oběšení	k1gNnPc3	oběšení
11	[number]	k4	11
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
14	[number]	k4	14
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
<g/>
.	.	kIx.	.
</s>
<s>
Slánského	Slánského	k2eAgFnSc4d1	Slánského
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
byla	být	k5eAaImAgFnS	být
prezidentem	prezident	k1gMnSc7	prezident
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
odsouzenými	odsouzený	k1gMnPc7	odsouzený
byl	být	k5eAaImAgInS	být
popraven	popraven	k2eAgInSc1d1	popraven
oběšením	oběšení	k1gNnSc7	oběšení
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1952	[number]	k4	1952
v	v	k7c6	v
pankrácké	pankrácký	k2eAgFnSc6d1	Pankrácká
věznici	věznice	k1gFnSc6	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Popravení	popravený	k2eAgMnPc1d1	popravený
byli	být	k5eAaImAgMnP	být
zpopelněni	zpopelnit	k5eAaPmNgMnP	zpopelnit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
popel	popel	k1gInSc1	popel
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
vysypán	vysypat	k5eAaPmNgInS	vysypat
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
někde	někde	k6eAd1	někde
severně	severně	k6eAd1	severně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
Slánského	Slánský	k1gMnSc2	Slánský
rehabilitoval	rehabilitovat	k5eAaBmAgInS	rehabilitovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Slánského	Slánského	k2eAgMnSc1d1	Slánského
syn	syn	k1gMnSc1	syn
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
mladší	mladý	k2eAgMnSc1d2	mladší
byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
disident	disident	k1gMnSc1	disident
<g/>
,	,	kIx,	,
signatář	signatář	k1gMnSc1	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgInS	být
československým	československý	k2eAgMnSc7d1	československý
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
českým	český	k2eAgMnSc7d1	český
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
České	český	k2eAgNnSc1d1	české
století	století	k1gNnSc1	století
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Slánského	Slánského	k2eAgMnSc1d1	Slánského
herec	herec	k1gMnSc1	herec
David	David	k1gMnSc1	David
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
ho	on	k3xPp3gNnSc4	on
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kovaný	kovaný	k2eAgMnSc1d1	kovaný
komunista	komunista	k1gMnSc1	komunista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
něco	něco	k3yInSc4	něco
schovával	schovávat	k5eAaImAgMnS	schovávat
<g/>
,	,	kIx,	,
i	i	k8xC	i
zrzavé	zrzavý	k2eAgInPc4d1	zrzavý
vlasy	vlas	k1gInPc4	vlas
si	se	k3xPyFc3	se
barvil	barvit	k5eAaImAgMnS	barvit
načerno	načerno	k6eAd1	načerno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4	jeho
ambice	ambice	k1gFnPc4	ambice
jsem	být	k5eAaImIp1nS	být
nerozluštil	rozluštit	k5eNaPmAgMnS	rozluštit
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
mu	on	k3xPp3gMnSc3	on
vyhovovalo	vyhovovat	k5eAaImAgNnS	vyhovovat
být	být	k5eAaImF	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
dvojka	dvojka	k1gFnSc1	dvojka
po	po	k7c6	po
Klemovi	Klema	k1gMnSc6	Klema
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
není	být	k5eNaImIp3nS	být
bodrý	bodrý	k2eAgMnSc1d1	bodrý
tatík	tatík	k1gMnSc1	tatík
<g/>
,	,	kIx,	,
připomínají	připomínat	k5eAaImIp3nP	připomínat
mafiány	mafián	k1gMnPc4	mafián
v	v	k7c6	v
plném	plný	k2eAgNnSc6d1	plné
nasazení	nasazení	k1gNnSc6	nasazení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ničím	nic	k3yNnSc7	nic
se	se	k3xPyFc4	se
nemažou	mazat	k5eNaImIp3nP	mazat
<g/>
,	,	kIx,	,
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
upatlají	upatlat	k5eAaPmIp3nP	upatlat
od	od	k7c2	od
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc1	ten
fuk	fuk	k6eAd1	fuk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
