<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Cullodenu	Culloden	k1gInSc2	Culloden
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1746	[number]	k4	1746
nedaleko	nedaleko	k7c2	nedaleko
vesnice	vesnice	k1gFnSc2	vesnice
Culloden	Cullodna	k1gFnPc2	Cullodna
na	na	k7c6	na
Skotské	skotský	k2eAgFnSc6d1	skotská
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poslední	poslední	k2eAgFnSc4d1	poslední
bitvu	bitva	k1gFnSc4	bitva
vybojovanou	vybojovaný	k2eAgFnSc4d1	vybojovaná
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britská	britský	k2eAgNnPc1d1	Britské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
vedl	vést	k5eAaImAgMnS	vést
William	William	k1gInSc4	William
Augustus	Augustus	k1gMnSc1	Augustus
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Cumberlandu	Cumberland	k1gInSc2	Cumberland
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
porazila	porazit	k5eAaPmAgFnS	porazit
vojska	vojsko	k1gNnPc1	vojsko
jakobitů	jakobita	k1gMnPc2	jakobita
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
velel	velet	k5eAaImAgInS	velet
Karel	Karel	k1gMnSc1	Karel
Eduard	Eduard	k1gMnSc1	Eduard
Stuart	Stuart	k1gInSc4	Stuart
<g/>
.	.	kIx.	.
</s>
<s>
Jakobitská	jakobitský	k2eAgNnPc4d1	jakobitské
vojska	vojsko	k1gNnPc4	vojsko
tvořili	tvořit	k5eAaImAgMnP	tvořit
skotští	skotský	k2eAgMnPc1d1	skotský
horalé	horal	k1gMnPc1	horal
ze	z	k7c2	z
Skotské	skotský	k2eAgFnSc2d1	skotská
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
Skotové	Skot	k1gMnPc1	Skot
z	z	k7c2	z
nížiny	nížina	k1gFnSc2	nížina
a	a	k8xC	a
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
Angličanů	Angličan	k1gMnPc2	Angličan
z	z	k7c2	z
manchesterského	manchesterský	k2eAgInSc2d1	manchesterský
regimentu	regiment	k1gInSc2	regiment
<g/>
.	.	kIx.	.
</s>
<s>
Jakobity	jakobita	k1gMnPc4	jakobita
podporovala	podporovat	k5eAaImAgFnS	podporovat
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
součástí	součást	k1gFnSc7	součást
jejich	jejich	k3xOp3gInSc2	jejich
vojsk	vojsko	k1gNnPc2	vojsko
byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
francouzské	francouzský	k2eAgInPc1d1	francouzský
a	a	k8xC	a
irské	irský	k2eAgInPc1d1	irský
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
vojsku	vojsko	k1gNnSc6	vojsko
bojovali	bojovat	k5eAaImAgMnP	bojovat
převážně	převážně	k6eAd1	převážně
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
Skotů	skot	k1gInPc2	skot
<g/>
,	,	kIx,	,
oddíl	oddíl	k1gInSc1	oddíl
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
Ulsteru	Ulster	k1gInSc2	Ulster
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
německých	německý	k2eAgMnPc2d1	německý
a	a	k8xC	a
rakouských	rakouský	k2eAgMnPc2d1	rakouský
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
bitvě	bitva	k1gFnSc6	bitva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
asi	asi	k9	asi
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
padlo	padnout	k5eAaImAgNnS	padnout
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
1500	[number]	k4	1500
až	až	k9	až
2000	[number]	k4	2000
jakobitů	jakobita	k1gMnPc2	jakobita
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
britských	britský	k2eAgNnPc2d1	Britské
vojsk	vojsko	k1gNnPc2	vojsko
byly	být	k5eAaImAgInP	být
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgInPc1d2	nižší
<g/>
,	,	kIx,	,
50	[number]	k4	50
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
259	[number]	k4	259
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Cullodenu	Culloden	k1gInSc2	Culloden
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Culloden	Cullodna	k1gFnPc2	Cullodna
Memorial	Memorial	k1gMnSc1	Memorial
Museum	museum	k1gNnSc4	museum
</s>
</p>
<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Cullodenu	Culloden	k1gInSc2	Culloden
</s>
</p>
