<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
Hvězdicový	hvězdicový	k2eAgInSc1d1
devítiválec	devítiválec	k1gInSc1
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
II-RTyp	II-RTyp	k1gMnSc1
</s>
<s>
hvězdicový	hvězdicový	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
motor	motor	k1gInSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Konstruktér	konstruktér	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
F.	F.	kA
A.	A.	kA
Barvitius	Barvitius	k1gMnSc1
První	první	k4xOgFnSc1
rozběh	rozběh	k1gInSc4
</s>
<s>
1934	#num#	k4
Hlavní	hlavní	k2eAgInSc4d1
použití	použití	k1gNnSc1
</s>
<s>
Aero	aero	k1gNnSc1
A-	A-	k1gFnSc2
<g/>
204	#num#	k4
<g/>
,	,	kIx,
Saro	Saro	k6eAd1
Cloud	Clouda	k1gFnPc2
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
187	#num#	k4
Výroba	výroba	k1gFnSc1
</s>
<s>
1934	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
Vyvinut	vyvinout	k5eAaPmNgInS
z	z	k7c2
motoru	motor	k1gInSc2
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
II	II	kA
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
-	-	kIx~
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
III	III	kA
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
Další	další	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Super	super	k1gInPc2
Castor	Castor	k1gInSc1
a	a	k8xC
Super	super	k2eAgInSc1d1
Castor	Castor	k1gInSc1
II	II	kA
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
reduktorové	reduktorový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
Walter	Walter	k1gMnSc1
Super	super	k2eAgMnSc1d1
Castor	Castor	k1gMnSc1
I-MR	I-MR	k1gMnSc1
a	a	k8xC
I-SR	I-SR	k1gMnSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
Varianty	varianta	k1gFnSc2
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-R	III-R	k1gFnSc2
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
-	-	kIx~
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-R	III-R	k1gFnSc2
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
československé	československý	k2eAgFnPc1d1
devítiválcové	devítiválcový	k2eAgFnPc1d1
zážehové	zážehový	k2eAgFnPc1d1
vzduchem	vzduch	k1gInSc7
chlazené	chlazený	k2eAgFnPc1d1
hvězdicové	hvězdicový	k2eAgFnPc1d1
motory	motor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
II-R	II-R	k1gFnSc3
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
v	v	k7c6
polovině	polovina	k1gFnSc6
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
vyráběn	vyrábět	k5eAaImNgInS
Akciovou	akciový	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Walter	Walter	k1gMnSc1
<g/>
,	,	kIx,
továrnou	továrna	k1gFnSc7
na	na	k7c4
automobily	automobil	k1gInPc4
a	a	k8xC
letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
v	v	k7c6
Praze	Praha	k1gFnSc6
-	-	kIx~
Jinonicích	Jinonice	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
verzi	verze	k1gFnSc6
motoru	motor	k1gInSc2
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II	II	kA
s	s	k7c7
reduktorem	reduktor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-R	III-R	k1gFnSc2
navazovala	navazovat	k5eAaImAgFnS
na	na	k7c6
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motor	motor	k1gInSc1
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
II-R	II-R	k1gFnSc2
je	být	k5eAaImIp3nS
vystaven	vystavit	k5eAaPmNgInS
v	v	k7c6
expozici	expozice	k1gFnSc6
Leteckého	letecký	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
ve	v	k7c6
Kbelích	Kbely	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
užití	užití	k1gNnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Konstrukční	konstrukční	k2eAgNnSc4d1
oddělení	oddělení	k1gNnSc4
továrny	továrna	k1gFnSc2
Walter	Walter	k1gMnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
hlavním	hlavní	k2eAgMnSc7d1
konstruktérem	konstruktér	k1gMnSc7
ing.	ing.	kA
Františkem	František	k1gMnSc7
Adolfem	Adolf	k1gMnSc7
Barvitiusem	Barvitius	k1gMnSc7
pokračovalo	pokračovat	k5eAaImAgNnS
ve	v	k7c6
vývoji	vývoj	k1gInSc6
vyšší	vysoký	k2eAgFnSc2d2
výkonové	výkonový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
devítiválcových	devítiválcový	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
zahájil	zahájit	k5eAaPmAgMnS
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
byl	být	k5eAaImAgMnS
prvním	první	k4xOgInSc7
devítiválcovým	devítiválcový	k2eAgInSc7d1
motorem	motor	k1gInSc7
z	z	k7c2
nové	nový	k2eAgFnSc2d1
řady	řada	k1gFnSc2
pístových	pístový	k2eAgInPc2d1
hvězdicových	hvězdicový	k2eAgInPc2d1
motorů	motor	k1gInPc2
o	o	k7c6
vrtání	vrtání	k1gNnSc6
válců	válec	k1gInPc2
135	#num#	k4
mm	mm	kA
a	a	k8xC
zdvihu	zdvih	k1gInSc2
170	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
prošel	projít	k5eAaPmAgMnS
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
obdobným	obdobný	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
jako	jako	k8xC,k8xS
jeho	jeho	k3xOp3gMnSc1
blíženec	blíženec	k1gMnSc1
Castor	Castor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ucelená	ucelený	k2eAgFnSc1d1
řada	řada	k1gFnSc1
motorů	motor	k1gInPc2
této	tento	k3xDgFnSc2
"	"	kIx"
<g/>
hvězdné	hvězdný	k2eAgFnSc2d1
série	série	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
s	s	k7c7
tímto	tento	k3xDgInSc7
počtem	počet	k1gInSc7
a	a	k8xC
uspořádáním	uspořádání	k1gNnSc7
válců	válec	k1gInPc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
tímto	tento	k3xDgNnSc7
vrtáním	vrtání	k1gNnSc7
a	a	k8xC
zdvihem	zdvih	k1gInSc7
měla	mít	k5eAaImAgFnS
pokračování	pokračování	k1gNnPc4
v	v	k7c6
dalších	další	k2eAgFnPc6d1
verzích	verze	k1gFnPc6
Polluxů	Pollux	k1gInPc2
a	a	k8xC
Super	super	k2eAgInPc2d1
Castorů	Castor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
jsou	být	k5eAaImIp3nP
představeny	představen	k2eAgInPc4d1
motory	motor	k1gInPc4
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II	II	kA
a	a	k8xC
reduktorová	reduktorový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
II-R	II-R	k1gFnPc1
a	a	k8xC
konečně	konečně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III	III	kA
a	a	k8xC
motor	motor	k1gInSc1
s	s	k7c7
reduktorem	reduktor	k1gInSc7
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-	III-	k1gMnSc3
<g/>
R.	R.	kA
Těmito	tento	k3xDgFnPc7
reduktorovými	reduktorový	k2eAgFnPc7d1
verzemi	verze	k1gFnPc7
řada	řada	k1gFnSc1
pístových	pístový	k2eAgInPc2d1
hvězdicových	hvězdicový	k2eAgInPc2d1
motorů	motor	k1gInPc2
o	o	k7c6
vrtání	vrtání	k1gNnSc6
válců	válec	k1gInPc2
135	#num#	k4
mm	mm	kA
a	a	k8xC
zdvihu	zdvih	k1gInSc2
170	#num#	k4
mm	mm	kA
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
témže	týž	k3xTgNnSc6
období	období	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Gerhard	Gerhard	k1gMnSc1
Fieseler	Fieseler	k1gMnSc1
na	na	k7c6
akrobatickém	akrobatický	k2eAgInSc6d1
letounu	letoun	k1gInSc6
Fieseler	Fieseler	k1gMnSc1
F2	F2	k1gMnSc1
Tiger	Tiger	k1gMnSc1
s	s	k7c7
motorem	motor	k1gInSc7
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
dobývá	dobývat	k5eAaImIp3nS
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
v	v	k7c6
letecké	letecký	k2eAgFnSc6d1
akrobacii	akrobacie	k1gFnSc6
<g/>
,	,	kIx,
představila	představit	k5eAaPmAgFnS
továrna	továrna	k1gFnSc1
Walter	Walter	k1gMnSc1
novou	nový	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
tohoto	tento	k3xDgInSc2
motoru	motor	k1gInSc2
určenou	určený	k2eAgFnSc4d1
dopravní	dopravní	k2eAgNnSc1d1
letectví	letectví	k1gNnSc1
a	a	k8xC
vojenská	vojenský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
všech	všecek	k3xTgInPc2
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
průzkumná	průzkumný	k2eAgFnSc1d1
<g/>
,	,	kIx,
bitevní	bitevní	k2eAgFnSc1d1
<g/>
,	,	kIx,
stíhací	stíhací	k2eAgFnSc1d1
atp.	atp.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
a	a	k8xC
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-R	III-R	k1gFnSc2
byly	být	k5eAaImAgInP
devítiválcové	devítiválcový	k2eAgInPc1d1
<g/>
,	,	kIx,
hvězdicové	hvězdicový	k2eAgInPc1d1
motory	motor	k1gInPc1
s	s	k7c7
vrtáním	vrtání	k1gNnSc7
135	#num#	k4
mm	mm	kA
a	a	k8xC
zdvihem	zdvih	k1gInSc7
170	#num#	k4
mm	mm	kA
a	a	k8xC
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
použity	použít	k5eAaPmNgFnP
jako	jako	k9
tažné	tažný	k2eAgFnPc1d1
nebo	nebo	k8xC
tlačné	tlačný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Čtyřdílná	čtyřdílný	k2eAgFnSc1d1
kliková	klikový	k2eAgFnSc1d1
skříň	skříň	k1gFnSc1
byla	být	k5eAaImAgFnS
odlita	odlít	k5eAaPmNgFnS
ze	z	k7c2
slitiny	slitina	k1gFnSc2
hliníku	hliník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocelové	ocelový	k2eAgInPc1d1
válce	válec	k1gInPc1
byly	být	k5eAaImAgInP
vysoustruženy	vysoustružit	k5eAaPmNgInP
vcelku	vcelku	k6eAd1
s	s	k7c7
chladicími	chladicí	k2eAgInPc7d1
žebry	žebr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavy	hlava	k1gFnSc2
z	z	k7c2
hliníkové	hliníkový	k2eAgFnSc2d1
slitiny	slitina	k1gFnSc2
byly	být	k5eAaImAgInP
našroubovány	našroubovat	k5eAaPmNgInP
za	za	k7c2
tepla	teplo	k1gNnSc2
a	a	k8xC
byly	být	k5eAaImAgInP
sevřeny	sevřít	k5eAaPmNgInP
dvoudílnými	dvoudílný	k2eAgFnPc7d1
zdeřemi	zdeř	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
hlav	hlava	k1gFnPc2
byla	být	k5eAaImAgNnP
zalisována	zalisován	k2eAgNnPc1d1
bronzová	bronzový	k2eAgNnPc1d1
sedla	sedlo	k1gNnPc1
a	a	k8xC
vedení	vedení	k1gNnSc1
ventilů	ventil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvoudílný	dvoudílný	k2eAgInSc1d1
klikový	klikový	k2eAgInSc1d1
hřídel	hřídel	k1gInSc1
byl	být	k5eAaImAgInS
vykován	vykovat	k5eAaPmNgInS
z	z	k7c2
chromniklové	chromniklový	k2eAgFnSc2d1
oceli	ocel	k1gFnSc2
Poldi-Victrix-Special	Poldi-Victrix-Special	k1gInSc1
a	a	k8xC
byl	být	k5eAaImAgInS
uložen	uložit	k5eAaPmNgInS
ve	v	k7c6
2	#num#	k4
válečkových	válečkový	k2eAgFnPc6d1
a	a	k8xC
jednom	jeden	k4xCgNnSc6
kuličkovém	kuličkový	k2eAgNnSc6d1
ložisku	ložisko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přední	přední	k2eAgInSc4d1
konec	konec	k1gInSc4
hřídele	hřídel	k1gFnSc2
měl	mít	k5eAaImAgMnS
drážkování	drážkování	k1gNnSc4
pro	pro	k7c4
nasazení	nasazení	k1gNnSc4
vrtulového	vrtulový	k2eAgInSc2d1
náboje	náboj	k1gInSc2
s	s	k7c7
možností	možnost	k1gFnSc7
instalace	instalace	k1gFnSc2
dřevěné	dřevěný	k2eAgFnSc2d1
či	či	k8xC
kovové	kovový	k2eAgFnSc2d1
vrtule	vrtule	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
ojnice	ojnice	k1gFnSc1
byla	být	k5eAaImAgFnS
průřezu	průřez	k1gInSc3
H	H	kA
<g/>
,	,	kIx,
osm	osm	k4xCc1
vedlejších	vedlejší	k2eAgFnPc2d1
ojnic	ojnice	k1gFnPc2
bylo	být	k5eAaImAgNnS
průřezu	průřez	k1gInSc2
mezikruží	mezikruží	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Písty	píst	k1gInPc7
byly	být	k5eAaImAgInP
vykovány	vykován	k2eAgFnPc4d1
ze	z	k7c2
slitiny	slitina	k1gFnSc2
hliníku	hliník	k1gInSc2
Hiduminium	Hiduminium	k1gNnSc4
RR	RR	kA
59	#num#	k4
a	a	k8xC
měly	mít	k5eAaImAgFnP
4	#num#	k4
pístní	pístní	k2eAgInPc1d1
kroužky	kroužek	k1gInPc1
(	(	kIx(
<g/>
2	#num#	k4
stírací	stírací	k2eAgMnSc1d1
a	a	k8xC
2	#num#	k4
těsnící	těsnící	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Světovou	světový	k2eAgFnSc4d1
premiéru	premiéra	k1gFnSc4
si	se	k3xPyFc3
odbyl	odbýt	k5eAaPmAgMnS
motor	motor	k1gInSc4
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-R	III-R	k1gFnSc2
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
mezinárodním	mezinárodní	k2eAgInSc6d1
aerosalonu	aerosalon	k1gInSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
na	na	k7c4
podzim	podzim	k1gInSc4
1934	#num#	k4
<g/>
,	,	kIx,
byť	byť	k8xS
do	do	k7c2
výroby	výroba	k1gFnSc2
šel	jít	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
následujícím	následující	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
na	na	k7c6
stánku	stánek	k1gInSc6
A.S.	A.S.	k1gMnSc1
Walter	Walter	k1gMnSc1
byly	být	k5eAaImAgFnP
hvězdicové	hvězdicový	k2eAgInPc4d1
motory	motor	k1gInPc4
Walter	Walter	k1gMnSc1
Gemma	Gemmum	k1gNnSc2
I	i	k9
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
Bora	Borum	k1gNnSc2
RC	RC	kA
a	a	k8xC
Walter	Walter	k1gMnSc1
Castor	Castor	k1gMnSc1
II	II	kA
a	a	k8xC
také	také	k9
invertní	invertní	k2eAgInPc4d1
motory	motor	k1gInPc4
Walter	Walter	k1gMnSc1
Minor	minor	k2eAgMnSc1d1
4	#num#	k4
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
Major	major	k1gMnSc1
4	#num#	k4
a	a	k8xC
Walter	Walter	k1gMnSc1
Major	major	k1gMnSc1
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stand	Standa	k1gFnPc2
továrny	továrna	k1gFnSc2
Walter	Walter	k1gMnSc1
svým	svůj	k3xOyFgNnSc7
uspořádáním	uspořádání	k1gNnSc7
a	a	k8xC
obsahem	obsah	k1gInSc7
byl	být	k5eAaImAgInS
plně	plně	k6eAd1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
expozic	expozice	k1gFnPc2
i	i	k8xC
největších	veliký	k2eAgFnPc2d3
světových	světový	k2eAgFnPc2d1
motorářských	motorářský	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
přiznávali	přiznávat	k5eAaImAgMnP
mnozí	mnohý	k2eAgMnPc1d1
návštěvníci	návštěvník	k1gMnPc1
této	tento	k3xDgFnSc2
expozice	expozice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Počty	počet	k1gInPc1
vyrobených	vyrobený	k2eAgInPc2d1
motorů	motor	k1gInPc2
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
a	a	k8xC
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-R	III-R	k1gFnSc2
nejsou	být	k5eNaImIp3nP
v	v	k7c6
oficiálních	oficiální	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
uváděny	uvádět	k5eAaImNgInP
<g/>
,	,	kIx,
v	v	k7c4
množství	množství	k1gNnSc4
187	#num#	k4
ks	ks	kA
je	být	k5eAaImIp3nS
zmíněn	zmíněn	k2eAgMnSc1d1
pouze	pouze	k6eAd1
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
(	(	kIx(
<g/>
boční	boční	k2eAgInSc4d1
pohled	pohled	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Operační	operační	k2eAgNnSc1d1
nasazení	nasazení	k1gNnSc1
</s>
<s>
Reduktorové	reduktorový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
osvědčily	osvědčit	k5eAaPmAgInP
u	u	k7c2
dopravního	dopravní	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Aero	aero	k1gNnSc4
A-204	A-204	k1gFnPc2
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
u	u	k7c2
obojživelného	obojživelný	k2eAgInSc2d1
hydroplánu	hydroplán	k1gInSc2
Saro	Saro	k1gMnSc1
Cloud	Cloud	k1gMnSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Aero	aero	k1gNnSc4
A-204	A-204	k1gFnSc2
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Československé	československý	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
aerolinie	aerolinie	k1gFnSc2
vznesly	vznést	k5eAaPmAgInP
podrobné	podrobný	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
na	na	k7c4
nový	nový	k2eAgInSc4d1
typ	typ	k1gInSc4
dopravního	dopravní	k2eAgInSc2d1
letounu	letoun	k1gInSc2
<g/>
,	,	kIx,
realizace	realizace	k1gFnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
Ing.	ing.	kA
Antonín	Antonín	k1gMnSc1
Husník	Husník	k1gMnSc1
z	z	k7c2
firmy	firma	k1gFnSc2
Aero	aero	k1gNnSc1
(	(	kIx(
<g/>
Aero	aero	k1gNnSc1
<g/>
,	,	kIx,
továrna	továrna	k1gFnSc1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
dr	dr	kA
<g/>
.	.	kIx.
Kabeš	Kabeš	k1gMnPc4
<g/>
,	,	kIx,
Praha-Vysočany	Praha-Vysočan	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prototyp	prototyp	k1gInSc4
Aero	aero	k1gNnSc4
A-	A-	k1gFnSc2
<g/>
204	#num#	k4
<g/>
,	,	kIx,
označený	označený	k2eAgInSc1d1
OK-BAA	OK-BAA	k1gFnSc7
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
vzlétl	vzlétnout	k5eAaPmAgMnS
v	v	k7c6
březnu	březen	k1gInSc6
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
původnímu	původní	k2eAgInSc3d1
plánu	plán	k1gInSc3
byl	být	k5eAaImAgMnS
vybaven	vybavit	k5eAaPmNgMnS
pevným	pevný	k2eAgMnSc7d1
<g/>
,	,	kIx,
nikoli	nikoli	k9
zatažitelným	zatažitelný	k2eAgInSc7d1
podvozkem	podvozek	k1gInSc7
–	–	k?
ten	ten	k3xDgInSc4
francouzská	francouzský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Messier	Messira	k1gFnPc2
dodala	dodat	k5eAaPmAgFnS
až	až	k6eAd1
později	pozdě	k6eAd2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopravní	dopravní	k2eAgInSc4d1
letoun	letoun	k1gInSc4
Aero	aero	k1gNnSc4
A-204	A-204	k1gFnSc2
byl	být	k5eAaImAgMnS
13,2	13,2	k4
m	m	kA
dlouhý	dlouhý	k2eAgMnSc1d1
<g/>
,	,	kIx,
rozpětí	rozpětí	k1gNnSc2
křídel	křídlo	k1gNnPc2
činilo	činit	k5eAaImAgNnS
19,2	19,2	k4
m	m	kA
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc1
prázdného	prázdný	k2eAgInSc2d1
stroje	stroj	k1gInSc2
2850	#num#	k4
kg	kg	kA
a	a	k8xC
maximální	maximální	k2eAgFnSc1d1
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
4300	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojice	dvojice	k1gFnSc1
motorů	motor	k1gInPc2
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
II-R	II-R	k1gFnSc1
mu	on	k3xPp3gMnSc3
dávala	dávat	k5eAaImAgFnS
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
320	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gInSc1
maximální	maximální	k2eAgInSc1d1
dostup	dostup	k1gInSc1
činil	činit	k5eAaImAgInS
5800	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
a	a	k8xC
dolet	dolet	k1gInSc1
900	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gMnSc1
mohl	moct	k5eAaImAgMnS
nést	nést	k5eAaImF
dva	dva	k4xCgInPc4
členy	člen	k1gInPc4
posádky	posádka	k1gFnSc2
a	a	k8xC
osm	osm	k4xCc4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkoušky	zkouška	k1gFnSc2
ve	v	k7c6
Vojenském	vojenský	k2eAgInSc6d1
technickém	technický	k2eAgInSc6d1
a	a	k8xC
leteckém	letecký	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
(	(	kIx(
<g/>
VTLÚ	VTLÚ	kA
<g/>
)	)	kIx)
v	v	k7c6
Letňanech	Letňan	k1gMnPc6
dopadly	dopadnout	k5eAaPmAgFnP
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
ČSA	ČSA	kA
však	však	k9
upřednostnily	upřednostnit	k5eAaPmAgInP
britské	britský	k2eAgInPc4d1
letouny	letoun	k1gInPc4
Airspeed	Airspeed	k1gMnSc1
AS	as	k1gInSc4
<g/>
.6	.6	k4
Envoy	Envoy	k1gInPc4
se	s	k7c7
dvěma	dva	k4xCgInPc7
motory	motor	k1gInPc7
Walter	Walter	k1gMnSc1
Castor	Castor	k1gMnSc1
a	a	k8xC
ze	z	k7c2
sériové	sériový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
A-204	A-204	k1gFnSc2
sešlo	sejít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
však	však	k9
základem	základ	k1gInSc7
pro	pro	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
nesoucí	nesoucí	k2eAgFnSc4d1
označení	označení	k1gNnSc4
Aero	aero	k1gNnSc4
A-	A-	k1gFnSc2
<g/>
304	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediného	jediný	k2eAgInSc2d1
prototypu	prototyp	k1gInSc2
A-204	A-204	k1gFnSc2
se	se	k3xPyFc4
po	po	k7c6
okupaci	okupace	k1gFnSc6
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
březnu	březen	k1gInSc6
1939	#num#	k4
zmocnili	zmocnit	k5eAaPmAgMnP
Němci	Němec	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
(	(	kIx(
<g/>
charakteristiky	charakteristika	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Saro	Saro	k1gMnSc1
Cloud	Cloud	k1gMnSc1
</s>
<s>
Saro	Saro	k1gMnSc1
Cloud	Cloud	k1gMnSc1
(	(	kIx(
<g/>
Saunders	Saunders	k1gInSc1
Roe	Roe	k1gFnSc2
A	a	k9
<g/>
.19	.19	k4
Cloud	Cloudo	k1gNnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
britský	britský	k2eAgInSc1d1
obojživelný	obojživelný	k2eAgInSc1d1
létající	létající	k2eAgInSc1d1
člun	člun	k1gInSc1
<g/>
,	,	kIx,
navržený	navržený	k2eAgInSc1d1
a	a	k8xC
postavený	postavený	k2eAgInSc1d1
firmou	firma	k1gFnSc7
Saunders-Roe	Saunders-Roe	k1gFnPc2
Ltd	ltd	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopravní	dopravní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
nesla	nést	k5eAaImAgFnS
označení	označení	k1gNnSc4
A	a	k8xC
<g/>
.19	.19	k4
<g/>
,	,	kIx,
vojenská	vojenský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
výcvik	výcvik	k1gInSc4
pilotů	pilot	k1gMnPc2
a	a	k8xC
navigátorů	navigátor	k1gMnPc2
Royal	Royal	k1gInSc1
Air	Air	k1gFnPc2
Force	force	k1gFnSc2
byla	být	k5eAaImAgFnS
označena	označit	k5eAaPmNgFnS
A	A	kA
<g/>
.29	.29	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopravní	dopravní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
A	a	k9
<g/>
.19	.19	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
poháněna	pohánět	k5eAaImNgFnS
dvěma	dva	k4xCgInPc7
motory	motor	k1gInPc7
Serval	servat	k5eAaPmAgMnS
I	i	k9
byla	být	k5eAaImAgFnS
registrována	registrován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
G-ACGO	G-ACGO	k1gFnSc1
(	(	kIx(
<g/>
kapacita	kapacita	k1gFnSc1
2	#num#	k4
<g/>
+	+	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
vzlétl	vzlétnout	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1933	#num#	k4
v	v	k7c4
Cowes	Cowes	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vyslán	vyslat	k5eAaPmNgInS
s	s	k7c7
pilotem	pilot	k1gInSc7
capt	capta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
D.	D.	kA
Sottem	Sott	k1gInSc7
na	na	k7c4
prodejní	prodejní	k2eAgNnSc4d1
turné	turné	k1gNnSc4
po	po	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
prodán	prodán	k2eAgMnSc1d1
Československým	československý	k2eAgFnPc3d1
státním	státní	k2eAgFnPc3d1
aeroliniím	aerolinie	k1gFnPc3
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
létal	létat	k5eAaImAgInS
od	od	k7c2
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1935	#num#	k4
s	s	k7c7
imatrikulací	imatrikulace	k1gFnSc7
OK-BAK	OK-BAK	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInPc1d1
motory	motor	k1gInPc1
byly	být	k5eAaImAgInP
nahrazeny	nahradit	k5eAaPmNgInP
československými	československý	k2eAgFnPc7d1
devítiválci	devítiválek	k1gMnPc1
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
o	o	k7c6
výkonu	výkon	k1gInSc6
po	po	k7c4
265	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1935	#num#	k4
nasazen	nasadit	k5eAaPmNgMnS
na	na	k7c6
lince	linka	k1gFnSc6
do	do	k7c2
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
první	první	k4xOgFnSc1
mezinárodní	mezinárodní	k2eAgFnSc1d1
linka	linka	k1gFnSc1
ČSA	ČSA	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
fungující	fungující	k2eAgFnSc2d1
pouze	pouze	k6eAd1
v	v	k7c6
letní	letní	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
provozována	provozovat	k5eAaImNgFnS
od	od	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Záhřebu	Záhřeb	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
byla	být	k5eAaImAgFnS
prodloužena	prodloužit	k5eAaPmNgFnS
do	do	k7c2
přístavu	přístav	k1gInSc2
Sušak	Sušak	k1gInSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
součástí	součást	k1gFnSc7
Rijeky	Rijeka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letiště	letiště	k1gNnSc1
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
vzdálené	vzdálený	k2eAgNnSc1d1
od	od	k7c2
města	město	k1gNnSc2
12	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
pasažéři	pasažér	k1gMnPc1
museli	muset	k5eAaImAgMnP
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
cestě	cesta	k1gFnSc6
nepohodlným	pohodlný	k2eNgInSc7d1
autobusem	autobus	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
okolí	okolí	k1gNnSc1
bylo	být	k5eAaImAgNnS
hornaté	hornatý	k2eAgNnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
ztěžovalo	ztěžovat	k5eAaImAgNnS
přistání	přistání	k1gNnSc1
při	při	k7c6
nepříznivém	příznivý	k2eNgNnSc6d1
počasí	počasí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
proto	proto	k8xC
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c4
zakoupení	zakoupení	k1gNnSc4
obojživelného	obojživelný	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
by	by	kYmCp3nS
přistávalo	přistávat	k5eAaImAgNnS
na	na	k7c6
moři	moře	k1gNnSc6
přímo	přímo	k6eAd1
v	v	k7c6
Sušaku	Sušak	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saro	Saro	k1gMnSc1
Cloud	Cloud	k1gMnSc1
na	na	k7c6
lince	linka	k1gFnSc6
létal	létat	k5eAaImAgMnS
v	v	k7c6
úseku	úsek	k1gInSc6
ze	z	k7c2
Záhřebu	Záhřeb	k1gInSc2
do	do	k7c2
Sušaku	Sušak	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
sezóny	sezóna	k1gFnSc2
1936	#num#	k4
byla	být	k5eAaImAgFnS
trasa	trasa	k1gFnSc1
prodloužena	prodloužit	k5eAaPmNgFnS
až	až	k6eAd1
do	do	k7c2
Splitu	Split	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
německé	německý	k2eAgFnSc6d1
okupaci	okupace	k1gFnSc6
byl	být	k5eAaImAgInS
letoun	letoun	k1gInSc1
uskladněn	uskladněn	k2eAgInSc1d1
na	na	k7c6
holešovickém	holešovický	k2eAgNnSc6d1
výstavišti	výstaviště	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
byl	být	k5eAaImAgInS
trup	trup	k1gInSc1
prodán	prodat	k5eAaPmNgInS
soukromému	soukromý	k2eAgMnSc3d1
majiteli	majitel	k1gMnPc7
a	a	k8xC
přestavěn	přestavěn	k2eAgInSc4d1
na	na	k7c4
motorový	motorový	k2eAgInSc4d1
hausbót	hausbót	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
jménem	jméno	k1gNnSc7
Delfín	Delfín	k1gMnSc1
vystřídal	vystřídat	k5eAaPmAgMnS
několik	několik	k4yIc4
majitelů	majitel	k1gMnPc2
(	(	kIx(
<g/>
jedním	jeden	k4xCgInSc7
byl	být	k5eAaImAgMnS
motocyklový	motocyklový	k2eAgMnSc1d1
závodník	závodník	k1gMnSc1
František	František	k1gMnSc1
Šťastný	Šťastný	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
ho	on	k3xPp3gMnSc4
zakoupil	zakoupit	k5eAaPmAgInS
Vojenský	vojenský	k2eAgInSc1d1
historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
převezen	převezen	k2eAgInSc1d1
do	do	k7c2
leteckých	letecký	k2eAgFnPc2d1
opraven	opravna	k1gFnPc2
v	v	k7c6
Trenčíně	Trenčín	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
proběhla	proběhnout	k5eAaPmAgFnS
rekonstrukce	rekonstrukce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
je	být	k5eAaImIp3nS
vystaven	vystavit	k5eAaPmNgMnS
v	v	k7c6
Leteckém	letecký	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
Kbely	Kbely	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Specifikace	specifikace	k1gFnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-R	III-R	k1gFnSc2
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
čtyřdobý	čtyřdobý	k2eAgInSc1d1
zážehový	zážehový	k2eAgInSc1d1
vzduchem	vzduch	k1gInSc7
chlazený	chlazený	k2eAgInSc1d1
hvězdicový	hvězdicový	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
devítiválec	devítiválec	k1gInSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
válců	válec	k1gInPc2
<g/>
:	:	kIx,
9	#num#	k4
</s>
<s>
Vrtání	vrtání	k1gNnSc1
válce	válec	k1gInSc2
<g/>
:	:	kIx,
135	#num#	k4
mm	mm	kA
</s>
<s>
Zdvih	zdvih	k1gInSc1
pístu	píst	k1gInSc2
<g/>
:	:	kIx,
170	#num#	k4
mm	mm	kA
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
pístů	píst	k1gInPc2
<g/>
:	:	kIx,
1	#num#	k4
288	#num#	k4
cm²	cm²	k?
</s>
<s>
Zdvihový	zdvihový	k2eAgInSc1d1
objem	objem	k1gInSc1
motoru	motor	k1gInSc2
<g/>
:	:	kIx,
21	#num#	k4
900	#num#	k4
cm³	cm³	k?
</s>
<s>
Vnější	vnější	k2eAgInSc1d1
průměr	průměr	k1gInSc1
motoru	motor	k1gInSc2
<g/>
:	:	kIx,
1	#num#	k4
262	#num#	k4
mm	mm	kA
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
motoru	motor	k1gInSc2
<g/>
:	:	kIx,
1	#num#	k4
490	#num#	k4
mm	mm	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
suchého	suchý	k2eAgInSc2d1
motoru	motor	k1gInSc2
s	s	k7c7
vrtulovou	vrtulový	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
<g/>
:	:	kIx,
242	#num#	k4
kg	kg	kA
</s>
<s>
Součásti	součást	k1gFnPc1
</s>
<s>
Rozvod	rozvod	k1gInSc1
<g/>
:	:	kIx,
OHV	OHV	kA
<g/>
,	,	kIx,
dvouventilový	dvouventilový	k2eAgMnSc1d1
</s>
<s>
Zapalování	zapalování	k1gNnSc1
<g/>
:	:	kIx,
2	#num#	k4
nezávislými	závislý	k2eNgInPc7d1
magnety	magnet	k1gInPc7
Scintilla	Scintill	k1gMnSc2
GN9-0-SCN	GN9-0-SCN	k1gMnSc2
</s>
<s>
Mazání	mazání	k1gNnSc1
<g/>
:	:	kIx,
tlakové	tlakový	k2eAgFnPc1d1
oběžné	oběžný	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
suchou	suchý	k2eAgFnSc7d1
klikovou	klikový	k2eAgFnSc7d1
skříní	skříň	k1gFnSc7
</s>
<s>
Pohon	pohon	k1gInSc1
<g/>
:	:	kIx,
dvoulistá	dvoulistý	k2eAgFnSc1d1
dřevěná	dřevěný	k2eAgFnSc1d1
nebo	nebo	k8xC
kovová	kovový	k2eAgFnSc1d1
vrtule	vrtule	k1gFnSc1
</s>
<s>
Směr	směr	k1gInSc1
otáčení	otáčení	k1gNnSc2
vrtule	vrtule	k1gFnSc2
při	při	k7c6
pohledu	pohled	k1gInSc6
ze	z	k7c2
zadu	zad	k1gInSc2
<g/>
:	:	kIx,
vlevo	vlevo	k6eAd1
</s>
<s>
Chlazení	chlazení	k1gNnSc1
<g/>
:	:	kIx,
vzduchové	vzduchový	k2eAgNnSc1d1
</s>
<s>
Karburátor	karburátor	k1gInSc1
<g/>
:	:	kIx,
Stromberg	Stromberg	k1gInSc1
NAR	nar	kA
80	#num#	k4
</s>
<s>
Převod	převod	k1gInSc1
na	na	k7c4
vrtuli	vrtule	k1gFnSc4
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
0,667	0,667	k4
<g/>
)	)	kIx)
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Označení	označení	k1gNnSc1
motoru	motor	k1gInSc2
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
III-R	III-R	k1gMnSc3
</s>
<s>
Výroba	výroba	k1gFnSc1
od	od	k7c2
</s>
<s>
1934	#num#	k4
</s>
<s>
1935	#num#	k4
</s>
<s>
Kompresní	kompresní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
6,0	6,0	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
</s>
<s>
Jmenovitý	jmenovitý	k2eAgInSc1d1
výkon	výkon	k1gInSc1
</s>
<s>
264,7	264,7	k4
kW	kW	kA
/	/	kIx~
360	#num#	k4
k	k	k7c3
při	pře	k1gFnSc3
2070	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
308,8	308,8	k4
kW	kW	kA
/	/	kIx~
420	#num#	k4
k	k	k7c3
při	pře	k1gFnSc3
2010	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
Vzletový	vzletový	k2eAgInSc1d1
výkon	výkon	k1gInSc1
</s>
<s>
367,6	367,6	k4
kW	kW	kA
/	/	kIx~
500	#num#	k4
k	k	k7c3
při	pře	k1gFnSc3
2300	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
404,4	404,4	k4
kW	kW	kA
/	/	kIx~
550	#num#	k4
k	k	k7c3
při	pře	k1gFnSc3
2250	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
Nominální	nominální	k2eAgInSc1d1
výkon	výkon	k1gInSc1
v	v	k7c6
nominální	nominální	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
H	H	kA
</s>
<s>
283,1	283,1	k4
kW	kW	kA
/	/	kIx~
385	#num#	k4
k	k	k7c3
při	pře	k1gFnSc3
2070	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
323,5	323,5	k4
kW	kW	kA
/	/	kIx~
440	#num#	k4
k	k	k7c3
při	pře	k1gFnSc3
2010	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
Maximální	maximální	k2eAgInSc1d1
výkon	výkon	k1gInSc1
v	v	k7c6
nominální	nominální	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
H	H	kA
</s>
<s>
305,9	305,9	k4
kW	kW	kA
/	/	kIx~
416	#num#	k4
k	k	k7c3
při	pře	k1gFnSc3
2300	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
349,3	349,3	k4
kW	kW	kA
/	/	kIx~
475	#num#	k4
k	k	k7c3
při	pře	k1gFnSc3
2250	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
Nominální	nominální	k2eAgFnSc1d1
výška	výška	k1gFnSc1
H	H	kA
</s>
<s>
1	#num#	k4
500	#num#	k4
m	m	kA
</s>
<s>
1	#num#	k4
200	#num#	k4
m	m	kA
</s>
<s>
Poměr	poměr	k1gInSc1
jmenovitého	jmenovitý	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
k	k	k7c3
hmotnosti	hmotnost	k1gFnSc3
</s>
<s>
0,78	0,78	k4
kW	kW	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
</s>
<s>
0,90	0,90	k4
kW	kW	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
</s>
<s>
Poměr	poměr	k1gInSc1
jmenovitého	jmenovitý	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
k	k	k7c3
objemu	objem	k1gInSc3
</s>
<s>
12,09	12,09	k4
kW	kW	kA
<g/>
.	.	kIx.
<g/>
l-	l-	k?
<g/>
1	#num#	k4
/	/	kIx~
16,4	16,4	k4
k.l	k.l	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
</s>
<s>
14,11	14,11	k4
kW	kW	kA
<g/>
.	.	kIx.
<g/>
l-	l-	k?
<g/>
1	#num#	k4
/	/	kIx~
19,2	19,2	k4
k.l	k.l	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1
paliva	palivo	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
340-360	340-360	k4
g.h	g.h	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
kW-	kW-	k?
<g/>
1	#num#	k4
/	/	kIx~
250-265	250-265	k4
g.h	g.h	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
k-	k-	k?
<g/>
1	#num#	k4
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1
oleje	olej	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
13,6	13,6	k4
<g/>
-	-	kIx~
<g/>
20,4	20,4	k4
g.	g.	k?
<g/>
kW-	kW-	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
h-	h-	k?
<g/>
1	#num#	k4
/	/	kIx~
10-15	10-15	k4
g.k	g.k	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
h-	h-	k?
<g/>
1	#num#	k4
</s>
<s>
13,6	13,6	k4
<g/>
-	-	kIx~
<g/>
20,4	20,4	k4
g.	g.	k?
<g/>
kW-	kW-	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
h-	h-	k?
<g/>
1	#num#	k4
/	/	kIx~
10-15	10-15	k4
g.k	g.k	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
h-	h-	k?
<g/>
1	#num#	k4
</s>
<s>
Předepsané	předepsaný	k2eAgNnSc1d1
palivo	palivo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
oktanové	oktanový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
min	mina	k1gFnPc2
<g/>
.	.	kIx.
78	#num#	k4
</s>
<s>
oktanové	oktanový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
min	mina	k1gFnPc2
<g/>
.	.	kIx.
80	#num#	k4
</s>
<s>
Použití	použití	k1gNnSc1
v	v	k7c6
letadlech	letadlo	k1gNnPc6
</s>
<s>
Aero	aero	k1gNnSc4
A-204	A-204	k1gFnSc2
</s>
<s>
Saro	Saro	k1gMnSc1
Cloud	Cloud	k1gMnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
a	a	k8xC
Aero	aero	k1gNnSc4
A-204	A-204	k1gFnSc2
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
a	a	k8xC
Aero	aero	k1gNnSc4
A-204	A-204	k1gFnSc2
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
a	a	k8xC
Saro	Saro	k1gMnSc1
Cloud	Cloud	k1gMnSc1
</s>
<s>
Torzo	torzo	k1gNnSc1
letounu	letoun	k1gInSc2
Saro	Saro	k6eAd1
Cloud	Cloud	k1gInSc1
vystavené	vystavená	k1gFnSc2
v	v	k7c6
LMK	LMK	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
NĚMEČEK	Němeček	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československá	československý	k2eAgFnSc1d1
letadla	letadlo	k1gNnPc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
III	III	kA
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
368	#num#	k4
s.	s.	k?
S.	S.	kA
218	#num#	k4
<g/>
,	,	kIx,
270	#num#	k4
<g/>
-	-	kIx~
<g/>
271	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SCH	SCH	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ing.	ing.	kA
Nový	nový	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
motor	motor	k1gInSc1
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc1
II-R	II-R	k1gFnSc1
360	#num#	k4
<g/>
/	/	kIx~
<g/>
500	#num#	k4
ks	ks	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červenec	červenec	k1gInSc1
1934	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
234	#num#	k4
<g/>
-	-	kIx~
<g/>
236	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BARVITIUS	BARVITIUS	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
Adolf	Adolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
na	na	k7c6
čtrnáctém	čtrnáctý	k4xOgInSc6
salonu	salon	k1gInSc6
aviatickém	aviatický	k2eAgInSc6d1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosinec	prosinec	k1gInSc1
1934	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
438	#num#	k4
<g/>
-	-	kIx~
<g/>
440	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DITTMAYER	DITTMAYER	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počty	počet	k1gInPc1
prodaných	prodaný	k2eAgInPc2d1
motorů	motor	k1gInPc2
Walter	Walter	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
-	-	kIx~
Jinonice	Jinonice	k1gFnPc1
<g/>
:	:	kIx,
Walter	Walter	k1gMnSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dopravní	dopravní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
Aero	aero	k1gNnSc4
A-204	A-204	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vojenský	vojenský	k2eAgInSc1d1
historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
aerolinie	aerolinie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Letadla	letadlo	k1gNnPc1
našich	náš	k3xOp1gFnPc2
aerolinií	aerolinie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saunders-Roe	Saunders-Ro	k1gInSc2
A-19	A-19	k1gMnSc1
Cloud	Cloud	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křídla	křídlo	k1gNnPc1
vlasti	vlast	k1gFnSc2
<g/>
.	.	kIx.
1963	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
357	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FOLPRECHT	Folprecht	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jadranský	jadranský	k2eAgInSc1d1
expres	expres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Létající	létající	k2eAgInSc1d1
člun	člun	k1gInSc1
ČSA	ČSA	kA
přistával	přistávat	k5eAaImAgInS
na	na	k7c6
moři	moře	k1gNnSc6
před	před	k7c7
hotelem	hotel	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Saunders	Saundersa	k1gFnPc2
Roe	Roe	k1gFnPc2
A	a	k9
<g/>
.19	.19	k4
Cloud	Cloud	k1gInSc1
–	–	k?
obojživelný	obojživelný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
/	/	kIx~
<g/>
1930	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenský	vojenský	k2eAgInSc1d1
historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FLIEGER	FLIEGER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelahozeves	Nelahozeves	k1gInSc1
<g/>
:	:	kIx,
Občanské	občanský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
25.11	25.11	k4
<g/>
.2005	.2005	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Letecké	letecký	k2eAgFnPc1d1
vzduchem	vzduch	k1gInSc7
chlazené	chlazený	k2eAgInPc4d1
motory	motor	k1gInPc4
Walter	Walter	k1gMnSc1
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
XVII	XVII	kA
-	-	kIx~
Jinonice	Jinonice	k1gFnPc1
<g/>
:	:	kIx,
Akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Walter	Walter	k1gMnSc1
<g/>
,	,	kIx,
továrna	továrna	k1gFnSc1
na	na	k7c4
automobily	automobil	k1gInPc4
a	a	k8xC
letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
.	.	kIx.
64	#num#	k4
s.	s.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Waltrovka	waltrovka	k1gFnSc1
</s>
<s>
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
</s>
<s>
Walter	Walter	k1gMnSc1
Super	super	k1gInSc2
Castor	Castor	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Walter	Walter	k1gMnSc1
Pollux	Pollux	k1gInSc4
II-R	II-R	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Walter	Walter	k1gMnSc1
Jinonice	Jinonice	k1gFnPc4
</s>
<s>
Aero	aero	k1gNnSc4
A-204	A-204	k1gFnSc2
</s>
<s>
Aero	aero	k1gNnSc4
A-204	A-204	k1gFnSc2
</s>
<s>
Jadranský	jadranský	k2eAgInSc1d1
expres	expres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Létající	létající	k2eAgInSc1d1
člun	člun	k1gInSc1
ČSA	ČSA	kA
přistával	přistávat	k5eAaImAgInS
na	na	k7c6
moři	moře	k1gNnSc6
před	před	k7c7
hotelem	hotel	k1gInSc7
</s>
<s>
Seznamte	seznámit	k5eAaPmRp2nP
se	se	k3xPyFc4
se	se	k3xPyFc4
Saro	Saro	k1gMnSc1
Cloud	Cloud	k1gMnSc1
A	a	k9
<g/>
.19	.19	k4
</s>
<s>
Československé	československý	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
1918	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
Walter	Walter	k1gMnSc1
Hvězdicové	hvězdicový	k2eAgInPc4d1
motory	motor	k1gInPc4
s	s	k7c7
vrtáním	vrtání	k1gNnSc7
105	#num#	k4
mm	mm	kA
</s>
<s>
NZ-40	NZ-40	k4
</s>
<s>
Polaris	Polaris	k1gFnSc1
</s>
<s>
NZ-60	NZ-60	k4
</s>
<s>
Vega	Vega	k6eAd1
</s>
<s>
NZ-85	NZ-85	k4
</s>
<s>
Venus	Venus	k1gMnSc1
</s>
<s>
NZ-120	NZ-120	k4
</s>
<s>
Mars	Mars	k1gInSc1
</s>
<s>
Gemma	Gemma	k1gFnSc1
</s>
<s>
Bora	Bora	k6eAd1
</s>
<s>
Scolar	Scolar	k1gInSc1
Hvězdicové	hvězdicový	k2eAgInPc1d1
motory	motor	k1gInPc1
s	s	k7c7
vrtáním	vrtání	k1gNnSc7
135	#num#	k4
mm	mm	kA
</s>
<s>
Regulus	Regulus	k1gMnSc1
</s>
<s>
Castor	Castor	k1gMnSc1
</s>
<s>
Castor	Castor	k1gMnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
II-R	II-R	k1gMnSc1
<g/>
/	/	kIx~
<g/>
III	III	kA
<g/>
/	/	kIx~
<g/>
III-R	III-R	k1gMnSc1
</s>
<s>
Pollux	Pollux	k1gInSc1
I	I	kA
<g/>
/	/	kIx~
<g/>
II	II	kA
<g/>
/	/	kIx~
<g/>
II	II	kA
</s>
<s>
Pollux	Pollux	k1gInSc1
II-R	II-R	k1gFnSc2
<g/>
/	/	kIx~
<g/>
III-R	III-R	k1gMnSc1
</s>
<s>
Super	super	k2eAgInSc4d1
Castor	Castor	k1gInSc4
Hvězdicové	hvězdicový	k2eAgInPc4d1
motory	motor	k1gInPc4
s	s	k7c7
vrtáním	vrtání	k1gNnSc7
165	#num#	k4
mm	mm	kA
</s>
<s>
Atlas	Atlas	k1gInSc1
Čtyřválcové	čtyřválcový	k2eAgFnSc2d1
řadové	řadový	k2eAgFnSc2d1
motory	motor	k1gInPc7
</s>
<s>
Junior	junior	k1gMnSc1
</s>
<s>
Major	major	k1gMnSc1
4	#num#	k4
</s>
<s>
Mikron	mikron	k1gInSc1
</s>
<s>
Minor	minor	k2eAgNnPc2d1
4	#num#	k4
</s>
<s>
M-431	M-431	k4
</s>
<s>
M332	M332	k4
Šestiválcové	šestiválcový	k2eAgInPc4d1
řadové	řadový	k2eAgInPc4d1
motory	motor	k1gInPc4
</s>
<s>
Major	major	k1gMnSc1
6	#num#	k4
</s>
<s>
Minor	minor	k2eAgNnPc2d1
6	#num#	k4
</s>
<s>
M337	M337	k4
Dvanáctiválcové	dvanáctiválcový	k2eAgInPc4d1
vidlicové	vidlicový	k2eAgInPc4d1
motory	motor	k1gInPc4
</s>
<s>
Minor	minor	k2eAgInPc2d1
12	#num#	k4
</s>
<s>
Sagitta	Sagitta	k1gMnSc1
Ploché	plochý	k2eAgInPc4d1
a	a	k8xC
ostatní	ostatní	k2eAgInPc4d1
motory	motor	k1gInPc4
</s>
<s>
Atom	atom	k1gInSc1
</s>
<s>
A	a	k9
</s>
<s>
M-208B	M-208B	k4
(	(	kIx(
<g/>
Praga	Praga	k1gFnSc1
Doris	Doris	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
M-202	M-202	k4
Proudové	proudový	k2eAgInPc1d1
motory	motor	k1gInPc1
</s>
<s>
M701	M701	k4
Turbovrtulové	turbovrtulový	k2eAgInPc1d1
motory	motor	k1gInPc1
</s>
<s>
M601	M601	k4
</s>
<s>
M602	M602	k4
Licenční	licenční	k2eAgInPc4d1
hvězdicové	hvězdicový	k2eAgInPc4d1
motory	motor	k1gInPc4
</s>
<s>
Mira-R	Mira-R	k?
</s>
<s>
Jupiter	Jupiter	k1gMnSc1
</s>
<s>
Pegas	Pegas	k1gMnSc1
</s>
<s>
Merkur	Merkur	k1gInSc1
</s>
<s>
Packard	Packard	kA
Diesel	diesel	k1gInSc1
DR-980	DR-980	k1gMnPc2
Licenční	licenční	k2eAgFnSc2d1
dvouhvězdicové	dvouhvězdicový	k2eAgFnSc2d1
motory	motor	k1gInPc7
</s>
<s>
Mistral	mistral	k1gInSc1
K	k	k7c3
14	#num#	k4
</s>
<s>
Mars	Mars	k1gInSc1
14	#num#	k4
M	M	kA
Licenční	licenční	k2eAgInPc4d1
řadové	řadový	k2eAgInPc4d1
motory	motor	k1gInPc4
</s>
<s>
W-III	W-III	k?
</s>
<s>
W-IV	W-IV	k?
</s>
<s>
Jumo	Jumo	k1gMnSc1
IV	IV	kA
C	C	kA
Licenční	licenční	k2eAgInPc4d1
vidlicové	vidlicový	k2eAgInPc4d1
motory	motor	k1gInPc4
</s>
<s>
W-V	W-V	k?
</s>
<s>
W-VI	W-VI	k?
</s>
<s>
W-VII	W-VII	k?
</s>
<s>
W-VIII	W-VIII	k?
</s>
<s>
Walter	Walter	k1gMnSc1
Argus	Argus	k1gMnSc1
10	#num#	k4
</s>
<s>
Walter	Walter	k1gMnSc1
Argus	Argus	k1gMnSc1
410	#num#	k4
Licenční	licenční	k2eAgInPc4d1
proudové	proudový	k2eAgInPc4d1
motory	motor	k1gInPc4
</s>
<s>
M-05	M-05	k4
</s>
<s>
M-06	M-06	k4
</s>
<s>
M-07	M-07	k4
</s>
<s>
AI-25W	AI-25W	k4
(	(	kIx(
<g/>
Walter	Walter	k1gMnSc1
Titan	titan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
