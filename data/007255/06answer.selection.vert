<s>
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
KL	kl	kA	kl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
úseku	úsek	k1gInSc2	úsek
linky	linka	k1gFnSc2	linka
B	B	kA	B
<g/>
,	,	kIx,	,
označovaného	označovaný	k2eAgInSc2d1	označovaný
jako	jako	k8xS	jako
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
B.	B.	kA	B.
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
podzemní	podzemní	k2eAgFnSc1d1	podzemní
(	(	kIx(	(
<g/>
26	[number]	k4	26
m	m	kA	m
hluboko	hluboko	k6eAd1	hluboko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
se	s	k7c7	s
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
střední	střední	k2eAgFnSc7d1	střední
lodí	loď	k1gFnSc7	loď
(	(	kIx(	(
<g/>
46	[number]	k4	46
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
eskalátorovým	eskalátorův	k2eAgInSc7d1	eskalátorův
tunelem	tunel	k1gInSc7	tunel
vedeným	vedený	k2eAgInSc7d1	vedený
do	do	k7c2	do
povrchového	povrchový	k2eAgInSc2d1	povrchový
vestibulu	vestibul	k1gInSc2	vestibul
<g/>
.	.	kIx.	.
</s>
