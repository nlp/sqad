<s>
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
KL	kl	kA	kl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
úseku	úsek	k1gInSc2	úsek
linky	linka	k1gFnSc2	linka
B	B	kA	B
<g/>
,	,	kIx,	,
označovaného	označovaný	k2eAgInSc2d1	označovaný
jako	jako	k8xS	jako
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
B.	B.	kA	B.
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
podzemní	podzemní	k2eAgFnSc1d1	podzemní
(	(	kIx(	(
<g/>
26	[number]	k4	26
m	m	kA	m
hluboko	hluboko	k6eAd1	hluboko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
se	s	k7c7	s
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
střední	střední	k2eAgFnSc7d1	střední
lodí	loď	k1gFnSc7	loď
(	(	kIx(	(
<g/>
46	[number]	k4	46
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
eskalátorovým	eskalátorův	k2eAgInSc7d1	eskalátorův
tunelem	tunel	k1gInSc7	tunel
vedeným	vedený	k2eAgInSc7d1	vedený
do	do	k7c2	do
povrchového	povrchový	k2eAgInSc2d1	povrchový
vestibulu	vestibul	k1gInSc2	vestibul
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
klasickou	klasický	k2eAgFnSc4d1	klasická
stanici	stanice	k1gFnSc4	stanice
pražského	pražský	k2eAgInSc2d1	pražský
typu	typ	k1gInSc2	typ
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
páry	pár	k1gInPc7	pár
prostupů	prostup	k1gInPc2	prostup
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
pohyblivostí	pohyblivost	k1gFnSc7	pohyblivost
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k8xC	i
výtah	výtah	k1gInSc4	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
využitých	využitý	k2eAgFnPc2d1	využitá
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Radlickou	radlický	k2eAgFnSc7d1	Radlická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
nástupiště	nástupiště	k1gNnSc4	nástupiště
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
103,5	[number]	k4	103,5
m	m	kA	m
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
stanice	stanice	k1gFnSc1	stanice
pak	pak	k6eAd1	pak
192	[number]	k4	192
m.	m.	k?	m.
Obklad	obklad	k1gInSc1	obklad
prostoru	prostor	k1gInSc2	prostor
nástupiště	nástupiště	k1gNnSc2	nástupiště
tvoří	tvořit	k5eAaImIp3nP	tvořit
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
modré	modrý	k2eAgInPc1d1	modrý
a	a	k8xC	a
žluté	žlutý	k2eAgInPc1d1	žlutý
smaltované	smaltovaný	k2eAgInPc1d1	smaltovaný
plechy	plech	k1gInPc1	plech
<g/>
.	.	kIx.	.
</s>
<s>
Povrchový	povrchový	k2eAgInSc1d1	povrchový
vestibul	vestibul	k1gInSc1	vestibul
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
masivní	masivní	k2eAgFnSc2d1	masivní
železobetonové	železobetonový	k2eAgFnSc2d1	železobetonová
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
přízemí	přízemí	k1gNnSc1	přízemí
osmipodlažního	osmipodlažní	k2eAgInSc2d1	osmipodlažní
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konstrukce	konstrukce	k1gFnSc1	konstrukce
tedy	tedy	k9	tedy
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachován	k2eAgFnSc1d1	zachována
<g/>
,	,	kIx,	,
vestavěny	vestavěn	k2eAgFnPc1d1	vestavěna
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
nutné	nutný	k2eAgFnPc1d1	nutná
součásti	součást	k1gFnPc1	součást
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
stanici	stanice	k1gFnSc4	stanice
metra	metro	k1gNnSc2	metro
potřebné	potřebný	k2eAgFnSc2d1	potřebná
a	a	k8xC	a
přidána	přidat	k5eAaPmNgFnS	přidat
lehce	lehko	k6eAd1	lehko
působící	působící	k2eAgFnSc1d1	působící
fasáda	fasáda	k1gFnSc1	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
strana	strana	k1gFnSc1	strana
celého	celý	k2eAgInSc2d1	celý
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
prosklená	prosklený	k2eAgFnSc1d1	prosklená
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
obložené	obložený	k2eAgFnPc4d1	obložená
nerezovými	rezový	k2eNgInPc7d1	nerezový
prvky	prvek	k1gInPc7	prvek
<g/>
;	;	kIx,	;
na	na	k7c6	na
stropě	strop	k1gInSc6	strop
je	být	k5eAaImIp3nS	být
ponechán	ponechat	k5eAaPmNgInS	ponechat
holý	holý	k2eAgInSc1d1	holý
beton	beton	k1gInSc1	beton
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
Kolbenovy	Kolbenův	k2eAgFnSc2d1	Kolbenova
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
úsekem	úsek	k1gInSc7	úsek
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
stanice	stanice	k1gFnSc1	stanice
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
s	s	k7c7	s
názvem	název	k1gInSc7	název
ČKD	ČKD	kA	ČKD
<g/>
,	,	kIx,	,
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
nedalekých	daleký	k2eNgInPc2d1	nedaleký
závodů	závod	k1gInPc2	závod
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Časy	čas	k1gInPc1	čas
se	se	k3xPyFc4	se
ale	ale	k9	ale
změnily	změnit	k5eAaPmAgFnP	změnit
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
nakonec	nakonec	k6eAd1	nakonec
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
této	tento	k3xDgFnSc2	tento
stanice	stanice	k1gFnSc2	stanice
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
rozestavěný	rozestavěný	k2eAgInSc4d1	rozestavěný
úsek	úsek	k1gInSc4	úsek
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
když	když	k8xS	když
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dobudovat	dobudovat	k5eAaPmF	dobudovat
úsek	úsek	k1gInSc4	úsek
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
celý	celý	k2eAgInSc1d1	celý
<g/>
,	,	kIx,	,
právě	právě	k9	právě
stavební	stavební	k2eAgFnPc4d1	stavební
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
Kolbenové	Kolbenová	k1gFnSc6	Kolbenová
se	se	k3xPyFc4	se
dočasně	dočasně	k6eAd1	dočasně
zastavily	zastavit	k5eAaPmAgFnP	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc4	vlak
jí	jíst	k5eAaImIp3nS	jíst
tak	tak	k6eAd1	tak
ještě	ještě	k6eAd1	ještě
následující	následující	k2eAgInPc1d1	následující
tři	tři	k4xCgInPc1	tři
roky	rok	k1gInPc1	rok
pouze	pouze	k6eAd1	pouze
projížděly	projíždět	k5eAaImAgInP	projíždět
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ještě	ještě	k9	ještě
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
dokončení	dokončení	k1gNnSc2	dokončení
než	než	k8xS	než
sousední	sousední	k2eAgInSc4d1	sousední
Hloubětín	Hloubětín	k1gInSc4	Hloubětín
<g/>
.	.	kIx.	.
</s>
<s>
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
slouží	sloužit	k5eAaImIp3nS	sloužit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
otevření	otevření	k1gNnSc2	otevření
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
vnuk	vnuk	k1gMnSc1	vnuk
podnikatele	podnikatel	k1gMnSc2	podnikatel
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
–	–	k?	–
po	po	k7c6	po
Emilu	Emil	k1gMnSc6	Emil
Kolbenovi	Kolben	k1gMnSc6	Kolben
<g/>
,	,	kIx,	,
zakladateli	zakladatel	k1gMnSc6	zakladatel
ČKD	ČKD	kA	ČKD
<g/>
.	.	kIx.	.
</s>
