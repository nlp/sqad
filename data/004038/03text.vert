<s>
H.	H.	kA	H.
<g/>
P.	P.	kA	P.
Baxxter	Baxxter	k1gInSc1	Baxxter
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Hans	Hans	k1gMnSc1	Hans
Peter	Peter	k1gMnSc1	Peter
Geerdes	Geerdes	k1gMnSc1	Geerdes
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1964	[number]	k4	1964
Leer	Leer	k1gInSc1	Leer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
frontman	frontman	k1gMnSc1	frontman
německé	německý	k2eAgFnSc2d1	německá
skupiny	skupina	k1gFnSc2	skupina
Scooter	Scootra	k1gFnPc2	Scootra
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
modré	modrý	k2eAgNnSc1d1	modré
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
blonďaté	blonďatý	k2eAgInPc4d1	blonďatý
vlasy	vlas	k1gInPc4	vlas
(	(	kIx(	(
<g/>
odbarvené	odbarvený	k2eAgInPc4d1	odbarvený
<g/>
)	)	kIx)	)
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
188	[number]	k4	188
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
psa	pes	k1gMnSc4	pes
jménem	jméno	k1gNnSc7	jméno
Hector	Hector	k1gMnSc1	Hector
a	a	k8xC	a
miluje	milovat	k5eAaImIp3nS	milovat
anglická	anglický	k2eAgNnPc4d1	anglické
auta	auto	k1gNnPc4	auto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
let	léto	k1gNnPc2	léto
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
společně	společně	k6eAd1	společně
s	s	k7c7	s
Rickem	Ricek	k1gMnSc7	Ricek
Jordanem	Jordan	k1gMnSc7	Jordan
založil	založit	k5eAaPmAgMnS	založit
popovou	popový	k2eAgFnSc4d1	popová
skupinu	skupina	k1gFnSc4	skupina
Celebrate	Celebrat	k1gInSc5	Celebrat
the	the	k?	the
Nun	Nun	k1gFnPc3	Nun
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
mixovali	mixovat	k5eAaImAgMnP	mixovat
skladby	skladba	k1gFnPc4	skladba
jiných	jiný	k2eAgFnPc2d1	jiná
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
založili	založit	k5eAaPmAgMnP	založit
ještě	ještě	k9	ještě
s	s	k7c7	s
Ferrisem	Ferris	k1gInSc7	Ferris
Buellerem	Bueller	k1gMnSc7	Bueller
skupinu	skupina	k1gFnSc4	skupina
Scooter	Scooter	k1gMnSc1	Scooter
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
semestr	semestr	k1gInSc1	semestr
studoval	studovat	k5eAaImAgInS	studovat
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
obchod	obchod	k1gInSc1	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podruhé	podruhé	k6eAd1	podruhé
rozvedený	rozvedený	k2eAgInSc1d1	rozvedený
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
manželství	manželství	k1gNnSc1	manželství
trvalo	trvat	k5eAaImAgNnS	trvat
krátce	krátce	k6eAd1	krátce
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
brzy	brzy	k6eAd1	brzy
rozvedeno	rozvést	k5eAaPmNgNnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
podruhé	podruhé	k6eAd1	podruhé
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
svoji	svůj	k3xOyFgFnSc4	svůj
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Simone	Simon	k1gMnSc5	Simon
Mostert	Mostert	k1gInSc4	Mostert
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
IMDB	IMDB	kA	IMDB
</s>
