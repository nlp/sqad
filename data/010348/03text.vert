<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Arpádovna	Arpádovna	k1gFnSc1	Arpádovna
(	(	kIx(	(
<g/>
1292	[number]	k4	1292
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1338	[number]	k4	1338
<g/>
,	,	kIx,	,
Töss	Töss	k1gInSc1	Töss
<g/>
,	,	kIx,	,
kanton	kanton	k1gInSc1	kanton
Curych	Curych	k1gInSc1	Curych
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
dcerou	dcera	k1gFnSc7	dcera
posledního	poslední	k2eAgMnSc2d1	poslední
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Ondřeje	Ondřej	k1gMnSc2	Ondřej
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Feneny	Fenen	k1gInPc1	Fenen
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Piastovců	Piastovec	k1gInPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
Fenena	Fenena	k1gFnSc1	Fenena
Kujavská	Kujavský	k2eAgFnSc1d1	Kujavský
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
září	září	k1gNnSc6	září
1295	[number]	k4	1295
a	a	k8xC	a
zanechala	zanechat	k5eAaPmAgFnS	zanechat
Ondřejovi	Ondřej	k1gMnSc3	Ondřej
jedinou	jediný	k2eAgFnSc4d1	jediná
dceru	dcera	k1gFnSc4	dcera
Alžbětu	Alžběta	k1gFnSc4	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Ovdovělý	ovdovělý	k2eAgMnSc1d1	ovdovělý
uherský	uherský	k2eAgMnSc1d1	uherský
panovník	panovník	k1gMnSc1	panovník
nutně	nutně	k6eAd1	nutně
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
mužského	mužský	k2eAgMnSc4d1	mužský
dědice	dědic	k1gMnSc4	dědic
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
již	již	k9	již
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1296	[number]	k4	1296
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Anežkou	Anežka	k1gFnSc7	Anežka
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
rakouského	rakouský	k2eAgMnSc2d1	rakouský
vévody	vévoda	k1gMnSc2	vévoda
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1298	[number]	k4	1298
byla	být	k5eAaImAgFnS	být
Alžběta	Alžběta	k1gFnSc1	Alžběta
zasnoubena	zasnouben	k2eAgFnSc1d1	zasnoubena
s	s	k7c7	s
Přemyslovcem	Přemyslovec	k1gMnSc7	Přemyslovec
Václavem	Václav	k1gMnSc7	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
byla	být	k5eAaImAgFnS	být
otrávena	otráven	k2eAgFnSc1d1	otrávena
Alžbětina	Alžbětin	k2eAgFnSc1d1	Alžbětina
babička	babička	k1gFnSc1	babička
Thomasina	Thomasina	k1gFnSc1	Thomasina
Morosiniová	Morosiniový	k2eAgFnSc1d1	Morosiniový
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Ondřej	Ondřej	k1gMnSc1	Ondřej
ji	on	k3xPp3gFnSc4	on
následoval	následovat	k5eAaImAgMnS	následovat
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
již	již	k6eAd1	již
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1301	[number]	k4	1301
<g/>
.	.	kIx.	.
</s>
<s>
Arpádovci	Arpádovec	k1gMnPc1	Arpádovec
tak	tak	k6eAd1	tak
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
vdova	vdova	k1gFnSc1	vdova
Anežka	Anežka	k1gFnSc1	Anežka
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
vydala	vydat	k5eAaPmAgFnS	vydat
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1301	[number]	k4	1301
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
listinu	listina	k1gFnSc4	listina
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
převzala	převzít	k5eAaPmAgFnS	převzít
uherský	uherský	k2eAgInSc4d1	uherský
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
najít	najít	k5eAaPmF	najít
nového	nový	k2eAgMnSc4d1	nový
panovníka	panovník	k1gMnSc4	panovník
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
těle	tělo	k1gNnSc6	tělo
by	by	kYmCp3nS	by
kolovala	kolovat	k5eAaImAgFnS	kolovat
arpádovská	arpádovský	k2eAgFnSc1d1	arpádovský
krev	krev	k1gFnSc1	krev
<g/>
.	.	kIx.	.
</s>
<s>
Uherští	uherský	k2eAgMnPc1d1	uherský
magnáti	magnát	k1gMnPc1	magnát
nechtěli	chtít	k5eNaImAgMnP	chtít
přijmout	přijmout	k5eAaPmF	přijmout
kandidaturu	kandidatura	k1gFnSc4	kandidatura
Karla	Karel	k1gMnSc2	Karel
Roberta	Robert	k1gMnSc2	Robert
z	z	k7c2	z
Anjou	Anjá	k1gFnSc4	Anjá
a	a	k8xC	a
hledali	hledat	k5eAaImAgMnP	hledat
jinou	jiný	k2eAgFnSc4d1	jiná
možnost	možnost	k1gFnSc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
připadal	připadat	k5eAaPmAgInS	připadat
Ota	Ota	k1gMnSc1	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Dolnobavorský	Dolnobavorský	k2eAgMnSc1d1	Dolnobavorský
<g/>
,	,	kIx,	,
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
Arpádovec	Arpádovec	k1gMnSc1	Arpádovec
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vnuk	vnuk	k1gMnSc1	vnuk
Kunhuty	Kunhuta	k1gFnSc2	Kunhuta
Haličské	haličský	k2eAgFnSc2d1	Haličská
a	a	k8xC	a
snoubenec	snoubenec	k1gMnSc1	snoubenec
desetileté	desetiletý	k2eAgFnSc2d1	desetiletá
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
bohatství	bohatství	k1gNnSc1	bohatství
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Uherští	uherský	k2eAgMnPc1d1	uherský
páni	pan	k1gMnPc1	pan
byli	být	k5eAaImAgMnP	být
uplaceni	uplatit	k5eAaPmNgMnP	uplatit
kutnohorským	kutnohorský	k2eAgNnSc7d1	kutnohorské
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
.27	.27	k4	.27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1301	[number]	k4	1301
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papež	Papež	k1gMnSc1	Papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1303	[number]	k4	1303
svým	svůj	k3xOyFgInSc7	svůj
výrokem	výrok	k1gInSc7	výrok
podpořil	podpořit	k5eAaPmAgInS	podpořit
Karla	Karel	k1gMnSc4	Karel
Roberta	Robert	k1gMnSc4	Robert
z	z	k7c2	z
Anjou	Anja	k1gMnSc7	Anja
jako	jako	k8xS	jako
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
a	a	k8xC	a
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
církevními	církevní	k2eAgInPc7d1	církevní
tresty	trest	k1gInPc4	trest
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
nadále	nadále	k6eAd1	nadále
chtěli	chtít	k5eAaImAgMnP	chtít
podporovat	podporovat	k5eAaImF	podporovat
Ladislava	Ladislav	k1gMnSc4	Ladislav
V.	V.	kA	V.
Uherští	uherský	k2eAgMnPc1d1	uherský
magnáti	magnát	k1gMnPc1	magnát
začali	začít	k5eAaPmAgMnP	začít
opouštět	opouštět	k5eAaImF	opouštět
přemyslovský	přemyslovský	k2eAgInSc1d1	přemyslovský
tábor	tábor	k1gInSc1	tábor
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
vyostřila	vyostřit	k5eAaPmAgFnS	vyostřit
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papežem	papež	k1gMnSc7	papež
podpořený	podpořený	k2eAgMnSc1d1	podpořený
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Albrecht	Albrecht	k1gMnSc1	Albrecht
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
předložil	předložit	k5eAaPmAgMnS	předložit
Václavovi	Václav	k1gMnSc3	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
celý	celý	k2eAgInSc4d1	celý
soubor	soubor	k1gInSc4	soubor
přemrštěných	přemrštěný	k2eAgInPc2d1	přemrštěný
a	a	k8xC	a
maximalistických	maximalistický	k2eAgInPc2d1	maximalistický
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
zříci	zříct	k5eAaPmF	zříct
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
Uhry	Uhry	k1gFnPc4	Uhry
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
zastavené	zastavený	k2eAgNnSc4d1	zastavené
Chebsko	Chebsko	k1gNnSc4	Chebsko
<g/>
,	,	kIx,	,
Míšeňsko	Míšeňsko	k1gNnSc4	Míšeňsko
a	a	k8xC	a
části	část	k1gFnPc4	část
Falce	Falc	k1gFnSc2	Falc
<g/>
,	,	kIx,	,
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
polské	polský	k2eAgFnSc2d1	polská
koruny	koruna	k1gFnSc2	koruna
a	a	k8xC	a
na	na	k7c4	na
pět	pět	k4xCc4	pět
či	či	k8xC	či
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
odstoupit	odstoupit	k5eAaPmF	odstoupit
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
desetinu	desetina	k1gFnSc4	desetina
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
kutnohorských	kutnohorský	k2eAgMnPc2d1	kutnohorský
stříbrných	stříbrný	k2eAgMnPc2d1	stříbrný
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tyto	tento	k3xDgInPc4	tento
nehorázné	nehorázný	k2eAgInPc4d1	nehorázný
požadavky	požadavek	k1gInPc4	požadavek
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
žádal	žádat	k5eAaImAgMnS	žádat
Habsburk	Habsburk	k1gMnSc1	Habsburk
vysoké	vysoký	k2eAgNnSc4d1	vysoké
odstupné	odstupné	k1gNnSc4	odstupné
<g/>
.	.	kIx.	.
</s>
<s>
Nesplnitelné	splnitelný	k2eNgInPc1d1	nesplnitelný
požadavky	požadavek	k1gInPc1	požadavek
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
rozhřešit	rozhřešit	k5eAaPmF	rozhřešit
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
většina	většina	k1gFnSc1	většina
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
kromě	kromě	k7c2	kromě
Askánců	Askánec	k1gMnPc2	Askánec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1304	[number]	k4	1304
se	se	k3xPyFc4	se
nemocný	mocný	k2eNgMnSc1d1	nemocný
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
společně	společně	k6eAd1	společně
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
následníkem	následník	k1gMnSc7	následník
si	se	k3xPyFc3	se
odvezl	odvézt	k5eAaPmAgMnS	odvézt
i	i	k9	i
svatoštěpánskou	svatoštěpánský	k2eAgFnSc4d1	Svatoštěpánská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1304	[number]	k4	1304
Čechy	Čech	k1gMnPc4	Čech
odolaly	odolat	k5eAaPmAgFnP	odolat
habsburskému	habsburský	k2eAgInSc3d1	habsburský
vpádu	vpád	k1gInSc3	vpád
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1305	[number]	k4	1305
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
král	král	k1gMnSc1	král
zrušil	zrušit	k5eAaPmAgMnS	zrušit
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1305	[number]	k4	1305
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
s	s	k7c7	s
Violou	Viola	k1gFnSc7	Viola
Těšínskou	Těšínská	k1gFnSc7	Těšínská
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
bývalá	bývalý	k2eAgFnSc1d1	bývalá
snoubenka	snoubenka	k1gFnSc1	snoubenka
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
pod	pod	k7c7	pod
habsburskou	habsburský	k2eAgFnSc7d1	habsburská
kuratelou	kuratela	k1gFnSc7	kuratela
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Václav	Václav	k1gMnSc1	Václav
ještě	ještě	k9	ještě
použil	použít	k5eAaPmAgMnS	použít
titul	titul	k1gInSc4	titul
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
listině	listina	k1gFnSc6	listina
v	v	k7c6	v
Bruntále	Bruntál	k1gInSc6	Bruntál
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
titulu	titul	k1gInSc3	titul
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
uherskými	uherský	k2eAgInPc7d1	uherský
korunovačními	korunovační	k2eAgInPc7d1	korunovační
klenoty	klenot	k1gInPc7	klenot
jej	on	k3xPp3gMnSc4	on
předal	předat	k5eAaPmAgMnS	předat
bratranci	bratranec	k1gMnSc3	bratranec
Otovi	Ota	k1gMnSc3	Ota
Dolnobavorskému	Dolnobavorský	k2eAgMnSc3d1	Dolnobavorský
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
zasnoubení	zasnoubení	k1gNnSc2	zasnoubení
se	se	k3xPyFc4	se
Alžběta	Alžběta	k1gFnSc1	Alžběta
nikdy	nikdy	k6eAd1	nikdy
neprovdala	provdat	k5eNaPmAgFnS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
dominikánském	dominikánský	k2eAgInSc6d1	dominikánský
klášteře	klášter	k1gInSc6	klášter
Katarinthal	Katarinthal	k1gMnSc1	Katarinthal
v	v	k7c6	v
Tössu	Töss	k1gInSc6	Töss
u	u	k7c2	u
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
Wintherthuru	Wintherthur	k1gInSc2	Wintherthur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
