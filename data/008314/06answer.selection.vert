<s>
Potáplice	potáplice	k1gFnSc1	potáplice
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
potáplici	potáplice	k1gFnSc3	potáplice
severní	severní	k2eAgFnSc6d1	severní
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
kulatějším	kulatý	k2eAgNnSc7d2	kulatější
temenem	temeno	k1gNnSc7	temeno
a	a	k8xC	a
tmavými	tmavý	k2eAgInPc7d1	tmavý
boky	bok	k1gInPc7	bok
<g/>
.	.	kIx.	.
</s>
