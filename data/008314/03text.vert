<p>
<s>
Potáplice	potáplice	k1gFnSc1	potáplice
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
(	(	kIx(	(
<g/>
Gavia	Gavia	k1gFnSc1	Gavia
pacifica	pacifica	k1gFnSc1	pacifica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
potáplice	potáplice	k1gFnSc2	potáplice
<g/>
,	,	kIx,	,
vodního	vodní	k2eAgMnSc2d1	vodní
ptáka	pták	k1gMnSc2	pták
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
severoamerický	severoamerický	k2eAgInSc4d1	severoamerický
poddruh	poddruh	k1gInSc4	poddruh
potáplice	potáplice	k1gFnSc2	potáplice
severní	severní	k2eAgNnSc1d1	severní
(	(	kIx(	(
<g/>
Gavia	Gavia	k1gFnSc1	Gavia
arctica	arctica	k1gMnSc1	arctica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
rozlišitelná	rozlišitelný	k2eAgFnSc1d1	rozlišitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Potáplice	potáplice	k1gFnSc1	potáplice
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
hlubokých	hluboký	k2eAgNnPc6d1	hluboké
jezerech	jezero	k1gNnPc6	jezero
v	v	k7c6	v
tundře	tundra	k1gFnSc6	tundra
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
na	na	k7c4	na
východ	východ	k1gInSc4	východ
po	po	k7c4	po
Baffinův	Baffinův	k2eAgInSc4d1	Baffinův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
okraji	okraj	k1gInSc6	okraj
Sibiře	Sibiř	k1gFnSc2	Sibiř
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Leny	Lena	k1gFnSc2	Lena
<g/>
;	;	kIx,	;
zimuje	zimovat	k5eAaImIp3nS	zimovat
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
od	od	k7c2	od
Aljašky	Aljaška	k1gFnSc2	Aljaška
na	na	k7c4	na
jih	jih	k1gInSc4	jih
po	po	k7c6	po
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Koreje	Korea	k1gFnSc2	Korea
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
až	až	k8xS	až
východní	východní	k2eAgFnSc2d1	východní
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
zatoulanec	zatoulanec	k1gMnSc1	zatoulanec
byla	být	k5eAaImAgFnS	být
zjištěná	zjištěný	k2eAgFnSc1d1	zjištěná
také	také	k9	také
na	na	k7c6	na
Havajských	havajský	k2eAgInPc6d1	havajský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
a	a	k8xC	a
Hongkongu	Hongkong	k1gInSc6	Hongkong
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
prokázán	prokázat	k5eAaPmNgInS	prokázat
výskyt	výskyt	k1gInSc1	výskyt
potáplice	potáplice	k1gFnSc2	potáplice
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
mladý	mladý	k2eAgMnSc1d1	mladý
pták	pták	k1gMnSc1	pták
pozorován	pozorovat	k5eAaImNgMnS	pozorovat
od	od	k7c2	od
12.1	[number]	k4	12.1
<g/>
.	.	kIx.	.
do	do	k7c2	do
4.2	[number]	k4	4.2
<g/>
.	.	kIx.	.
v	v	k7c4	v
North	North	k1gInSc4	North
Yorkshire	Yorkshir	k1gInSc5	Yorkshir
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
;	;	kIx,	;
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
ještě	ještě	k9	ještě
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
<g/>
Celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
potáplice	potáplice	k1gFnSc2	potáplice
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
je	být	k5eAaImIp3nS	být
odhadována	odhadován	k2eAgFnSc1d1	odhadována
na	na	k7c4	na
930	[number]	k4	930
000	[number]	k4	000
až	až	k9	až
1	[number]	k4	1
600	[number]	k4	600
000	[number]	k4	000
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Určování	určování	k1gNnSc2	určování
==	==	k?	==
</s>
</p>
<p>
<s>
Potáplice	potáplice	k1gFnSc1	potáplice
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
potáplici	potáplice	k1gFnSc3	potáplice
severní	severní	k2eAgFnSc6d1	severní
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
především	především	k9	především
kulatějším	kulatý	k2eAgNnSc7d2	kulatější
temenem	temeno	k1gNnSc7	temeno
a	a	k8xC	a
tmavými	tmavý	k2eAgInPc7d1	tmavý
boky	bok	k1gInPc7	bok
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
v	v	k7c6	v
prostém	prostý	k2eAgInSc6d1	prostý
šatu	šat	k1gInSc6	šat
mají	mít	k5eAaImIp3nP	mít
tmavý	tmavý	k2eAgInSc4d1	tmavý
proužek	proužek	k1gInSc4	proužek
na	na	k7c6	na
bradě	brada	k1gFnSc6	brada
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
boků	bok	k1gInPc2	bok
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
podél	podél	k7c2	podél
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
u	u	k7c2	u
potáplice	potáplice	k1gFnSc2	potáplice
severní	severní	k2eAgNnSc1d1	severní
tvoří	tvořit	k5eAaImIp3nS	tvořit
skvrnu	skvrna	k1gFnSc4	skvrna
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
potáplice	potáplice	k1gFnPc4	potáplice
je	být	k5eAaImIp3nS	být
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
ryb	ryba	k1gFnPc2	ryba
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
může	moct	k5eAaImIp3nS	moct
lovit	lovit	k5eAaImF	lovit
také	také	k9	také
korýše	korýš	k1gMnPc4	korýš
a	a	k8xC	a
žáby	žába	k1gFnPc4	žába
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Birch	Birch	k1gMnSc1	Birch
<g/>
,	,	kIx,	,
A.	A.	kA	A.
and	and	k?	and
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
C-T	C-T	k1gFnSc6	C-T
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Field	Field	k1gInSc1	Field
identification	identification	k1gInSc1	identification
of	of	k?	of
Arctic	Arctice	k1gFnPc2	Arctice
and	and	k?	and
Pacific	Pacifice	k1gFnPc2	Pacifice
Loons	Loons	k1gInSc1	Loons
<g/>
,	,	kIx,	,
Birding	Birding	k1gInSc1	Birding
29	[number]	k4	29
<g/>
:	:	kIx,	:
106	[number]	k4	106
<g/>
-	-	kIx~	-
<g/>
115	[number]	k4	115
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Birch	Birch	k1gMnSc1	Birch
<g/>
,	,	kIx,	,
A	A	kA	A
and	and	k?	and
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
C-T	C-T	k1gFnSc6	C-T
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Identification	Identification	k1gInSc1	Identification
of	of	k?	of
the	the	k?	the
Pacific	Pacific	k1gMnSc1	Pacific
Diver	Diver	k1gMnSc1	Diver
-	-	kIx~	-
a	a	k8xC	a
potential	potential	k1gMnSc1	potential
vagrant	vagrant	k1gMnSc1	vagrant
to	ten	k3xDgNnSc4	ten
Europe	Europ	k1gInSc5	Europ
<g/>
,	,	kIx,	,
Birding	Birding	k1gInSc1	Birding
World	World	k1gInSc1	World
8	[number]	k4	8
<g/>
:	:	kIx,	:
458	[number]	k4	458
<g/>
-	-	kIx~	-
<g/>
466	[number]	k4	466
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
potáplice	potáplice	k1gFnSc1	potáplice
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
potáplice	potáplice	k1gFnSc2	potáplice
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Gavia	Gavius	k1gMnSc2	Gavius
pacifica	pacificus	k1gMnSc2	pacificus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
