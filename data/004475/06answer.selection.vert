<s>
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Monte	Mont	k1gInSc5	Mont
Bianco	bianco	k2eAgMnSc6d1	bianco
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
vypínající	vypínající	k2eAgFnSc2d1	vypínající
se	se	k3xPyFc4	se
v	v	k7c6	v
Montblanském	Montblanský	k2eAgInSc6d1	Montblanský
masivu	masiv	k1gInSc6	masiv
na	na	k7c6	na
francouzsko-italském	francouzskotalský	k2eAgNnSc6d1	francouzsko-italské
pomezí	pomezí	k1gNnSc6	pomezí
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
4809	[number]	k4	4809
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
