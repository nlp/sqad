<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kubero	k1gNnSc2	Kubero
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1947	[number]	k4	1947
Louny	Louny	k1gInPc4	Louny
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1994	[number]	k4	1994
až	až	k9	až
2018	[number]	k4	2018
stál	stát	k5eAaImAgInS	stát
nepřetržitě	přetržitě	k6eNd1	přetržitě
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
Teplice	teplice	k1gFnSc2	teplice
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jako	jako	k8xC	jako
primátor	primátor	k1gMnSc1	primátor
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
dosud	dosud	k6eAd1	dosud
je	být	k5eAaImIp3nS	být
senátorem	senátor	k1gMnSc7	senátor
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
č.	č.	k?	č.
32	[number]	k4	32
-	-	kIx~	-
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Ústavně-právního	Ústavněrávní	k2eAgInSc2d1	Ústavně-právní
výboru	výbor	k1gInSc2	výbor
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
politik	politik	k1gMnSc1	politik
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
velmi	velmi	k6eAd1	velmi
aktivně	aktivně	k6eAd1	aktivně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
žertovnými	žertovný	k2eAgFnPc7d1	žertovná
a	a	k8xC	a
často	často	k6eAd1	často
kontroverzními	kontroverzní	k2eAgInPc7d1	kontroverzní
výroky	výrok	k1gInPc7	výrok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
bývá	bývat	k5eAaImIp3nS	bývat
řazen	řazen	k2eAgInSc1d1	řazen
mezi	mezi	k7c4	mezi
nejzajímavější	zajímavý	k2eAgInPc4d3	nejzajímavější
členy	člen	k1gInPc4	člen
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Mediálně	mediálně	k6eAd1	mediálně
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
i	i	k9	i
jako	jako	k8xC	jako
náruživý	náruživý	k2eAgInSc4d1	náruživý
kuřák	kuřák	k1gInSc4	kuřák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967	[number]	k4	1967
až	až	k9	až
1968	[number]	k4	1968
byl	být	k5eAaImAgInS	být
člen	člen	k1gInSc1	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
životopisu	životopis	k1gInSc2	životopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
<g/>
,	,	kIx,	,
úspěšně	úspěšně	k6eAd1	úspěšně
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
a	a	k8xC	a
následně	následně	k6eAd1	následně
studoval	studovat	k5eAaImAgMnS	studovat
matematiku	matematika	k1gFnSc4	matematika
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
na	na	k7c4	na
VŠE	všechen	k3xTgNnSc4	všechen
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
studium	studium	k1gNnSc4	studium
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
Sklo	sklo	k1gNnSc1	sklo
Union	union	k1gInSc1	union
Teplice	teplice	k1gFnSc1	teplice
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
Elektrosvit	elektrosvit	k1gInSc1	elektrosvit
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
jako	jako	k8xS	jako
tajemník	tajemník	k1gMnSc1	tajemník
Městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
je	být	k5eAaImIp3nS	být
profesionálním	profesionální	k2eAgNnSc7d1	profesionální
politikem	politikum	k1gNnSc7	politikum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dospělé	dospělý	k2eAgFnPc4d1	dospělá
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgFnPc1d1	politická
funkce	funkce	k1gFnPc1	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Teplice	teplice	k1gFnSc2	teplice
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
–	–	k?	–
primátor	primátor	k1gMnSc1	primátor
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Teplice	Teplice	k1gFnPc1	Teplice
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
senátor	senátor	k1gMnSc1	senátor
v	v	k7c6	v
PČR	PČR	kA	PČR
za	za	k7c4	za
volební	volební	k2eAgInSc4d1	volební
obvod	obvod	k1gInSc4	obvod
Teplice	teplice	k1gFnSc2	teplice
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
senátor	senátor	k1gMnSc1	senátor
v	v	k7c6	v
PČR	PČR	kA	PČR
za	za	k7c4	za
volební	volební	k2eAgInSc4d1	volební
obvod	obvod	k1gInSc4	obvod
Teplice	teplice	k1gFnSc2	teplice
</s>
</p>
<p>
<s>
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
senátor	senátor	k1gMnSc1	senátor
v	v	k7c6	v
PČR	PČR	kA	PČR
za	za	k7c4	za
volební	volební	k2eAgInSc4d1	volební
obvod	obvod	k1gInSc4	obvod
Teplice	teplice	k1gFnSc2	teplice
</s>
</p>
<p>
<s>
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
zastupitel	zastupitel	k1gMnSc1	zastupitel
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
</s>
</p>
<p>
<s>
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
místopředseda	místopředseda	k1gMnSc1	místopředseda
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
</s>
</p>
<p>
<s>
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgInPc1d1	politický
výroky	výrok	k1gInPc1	výrok
a	a	k8xC	a
činy	čin	k1gInPc1	čin
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
specifický	specifický	k2eAgInSc1d1	specifický
způsob	způsob	k1gInSc1	způsob
žertování	žertování	k1gNnSc2	žertování
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
i	i	k9	i
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
návrhů	návrh	k1gInPc2	návrh
není	být	k5eNaImIp3nS	být
každému	každý	k3xTgNnSc3	každý
hned	hned	k6eAd1	hned
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
míněny	mínit	k5eAaImNgInP	mínit
vážně	vážně	k6eAd1	vážně
anebo	anebo	k8xC	anebo
v	v	k7c6	v
žertovné	žertovný	k2eAgFnSc6d1	žertovná
nadsázce	nadsázka	k1gFnSc6	nadsázka
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
poslancům	poslanec	k1gMnPc3	poslanec
uzákonit	uzákonit	k5eAaPmF	uzákonit
měsíční	měsíční	k2eAgInSc4d1	měsíční
příjem	příjem	k1gInSc4	příjem
270	[number]	k4	270
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
se	s	k7c7	s
současným	současný	k2eAgNnSc7d1	současné
zrušením	zrušení	k1gNnSc7	zrušení
nezdaněných	zdaněný	k2eNgFnPc2d1	nezdaněná
náhrad	náhrada	k1gFnPc2	náhrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bojuje	bojovat	k5eAaImIp3nS	bojovat
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
práv	právo	k1gNnPc2	právo
kuřáků	kuřák	k1gMnPc2	kuřák
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
ODS	ODS	kA	ODS
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
výrazným	výrazný	k2eAgMnPc3d1	výrazný
kritikům	kritik	k1gMnPc3	kritik
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
k	k	k7c3	k
euroskeptikům	euroskeptik	k1gMnPc3	euroskeptik
<g/>
.	.	kIx.	.
</s>
<s>
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
socialistické	socialistický	k2eAgInPc4d1	socialistický
sklony	sklon	k1gInPc4	sklon
EU	EU	kA	EU
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jako	jako	k9	jako
by	by	kYmCp3nP	by
vykradli	vykrást	k5eAaPmAgMnP	vykrást
archivy	archiv	k1gInPc4	archiv
z	z	k7c2	z
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jinak	jinak	k6eAd1	jinak
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ty	ten	k3xDgFnPc1	ten
myšlenky	myšlenka	k1gFnPc1	myšlenka
stále	stále	k6eAd1	stále
braly	brát	k5eAaImAgFnP	brát
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Koncem	koncem	k7c2	koncem
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
týdenní	týdenní	k2eAgFnSc6d1	týdenní
desítce	desítka	k1gFnSc6	desítka
nejcitovanějších	citovaný	k2eAgMnPc2d3	nejcitovanější
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
když	když	k8xS	když
komentoval	komentovat	k5eAaBmAgMnS	komentovat
zamítnutí	zamítnutí	k1gNnSc4	zamítnutí
novely	novela	k1gFnSc2	novela
protikuřáckého	protikuřácký	k2eAgInSc2d1	protikuřácký
zákona	zákon	k1gInSc2	zákon
Senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
nejcitovanějšími	citovaný	k2eAgMnPc7d3	nejcitovanější
politiky	politik	k1gMnPc7	politik
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
k	k	k7c3	k
senátnímu	senátní	k2eAgNnSc3d1	senátní
zamítnutí	zamítnutí	k1gNnSc3	zamítnutí
zákoníku	zákoník	k1gInSc2	zákoník
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
k	k	k7c3	k
záměru	záměr	k1gInSc3	záměr
Senátu	senát	k1gInSc2	senát
zrušit	zrušit	k5eAaPmF	zrušit
chystaný	chystaný	k2eAgInSc4d1	chystaný
zákaz	zákaz	k1gInSc4	zákaz
předjíždění	předjíždění	k1gNnSc2	předjíždění
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
kamiony	kamion	k1gInPc4	kamion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
podpořil	podpořit	k5eAaPmAgInS	podpořit
vznikající	vznikající	k2eAgFnSc4d1	vznikající
motoristickou	motoristický	k2eAgFnSc4d1	motoristická
iniciativu	iniciativa	k1gFnSc4	iniciativa
ChceteZmenu	ChceteZmen	k1gInSc2	ChceteZmen
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
požadovala	požadovat	k5eAaImAgFnS	požadovat
snížení	snížení	k1gNnSc4	snížení
postihů	postih	k1gInPc2	postih
za	za	k7c4	za
různé	různý	k2eAgInPc4d1	různý
dopravní	dopravní	k2eAgInPc4d1	dopravní
přestupky	přestupek	k1gInPc4	přestupek
<g/>
,	,	kIx,	,
výstavbu	výstavba	k1gFnSc4	výstavba
rychlostní	rychlostní	k2eAgFnSc2d1	rychlostní
silnice	silnice	k1gFnSc2	silnice
R35	R35	k1gFnSc2	R35
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
značené	značený	k2eAgFnSc2d1	značená
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc1	zvýšení
maximální	maximální	k2eAgFnSc2d1	maximální
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c6	na
dálnicích	dálnice	k1gFnPc6	dálnice
na	na	k7c4	na
140	[number]	k4	140
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
160	[number]	k4	160
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.V	h.V	k?	h.V
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
sdružení	sdružení	k1gNnSc2	sdružení
Děti	dítě	k1gFnPc4	dítě
Země	zem	k1gFnSc2	zem
negativní	negativní	k2eAgNnSc4d1	negativní
ocenění	ocenění	k1gNnSc4	ocenění
Zelená	zelený	k2eAgFnSc1d1	zelená
perla	perla	k1gFnSc1	perla
za	za	k7c4	za
větu	věta	k1gFnSc4	věta
vyřčenou	vyřčený	k2eAgFnSc4d1	vyřčená
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
peticí	petice	k1gFnSc7	petice
proti	proti	k7c3	proti
jihomoravské	jihomoravský	k2eAgFnSc3d1	Jihomoravská
dálnici	dálnice	k1gFnSc3	dálnice
D	D	kA	D
<g/>
43	[number]	k4	43
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
kolem	kolem	k6eAd1	kolem
35	[number]	k4	35
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Navrhuji	navrhovat	k5eAaImIp1nS	navrhovat
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
se	se	k3xPyFc4	se
peticemi	petice	k1gFnPc7	petice
příliš	příliš	k6eAd1	příliš
nezabývali	zabývat	k5eNaImAgMnP	zabývat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gMnPc4	on
stejně	stejně	k6eAd1	stejně
nevyřešíme	vyřešit	k5eNaPmIp1nP	vyřešit
<g/>
,	,	kIx,	,
vzali	vzít	k5eAaPmAgMnP	vzít
je	on	k3xPp3gFnPc4	on
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
a	a	k8xC	a
spíš	spíš	k9	spíš
nutili	nutit	k5eAaImAgMnP	nutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nám	my	k3xPp1nPc3	my
je	on	k3xPp3gNnSc4	on
petičníci	petičník	k1gMnPc1	petičník
neposílali	posílat	k5eNaImAgMnP	posílat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zveřejnily	zveřejnit	k5eAaPmAgFnP	zveřejnit
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
diskusi	diskuse	k1gFnSc3	diskuse
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Kubery	Kubera	k1gFnSc2	Kubera
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Vaculíkem	Vaculík	k1gMnSc7	Vaculík
o	o	k7c6	o
rozpočtovém	rozpočtový	k2eAgNnSc6d1	rozpočtové
určení	určení	k1gNnSc6	určení
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Vaculík	Vaculík	k1gMnSc1	Vaculík
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
změnu	změna	k1gFnSc4	změna
rozdělovacích	rozdělovací	k2eAgInPc2d1	rozdělovací
koeficientů	koeficient	k1gInPc2	koeficient
státních	státní	k2eAgFnPc2d1	státní
dotací	dotace	k1gFnPc2	dotace
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
menších	malý	k2eAgFnPc2d2	menší
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
Kubera	Kubera	k1gFnSc1	Kubera
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
za	za	k7c4	za
hledání	hledání	k1gNnSc4	hledání
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zvýšit	zvýšit	k5eAaPmF	zvýšit
přímé	přímý	k2eAgInPc4d1	přímý
příjmy	příjem	k1gInPc4	příjem
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
nemovitosti	nemovitost	k1gFnSc2	nemovitost
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
místní	místní	k2eAgInPc1d1	místní
poplatky	poplatek	k1gInPc1	poplatek
byly	být	k5eAaImAgInP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
daní	daň	k1gFnSc7	daň
z	z	k7c2	z
bydlení	bydlení	k1gNnSc2	bydlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepřenášet	přenášet	k5eNaImF	přenášet
na	na	k7c4	na
obce	obec	k1gFnPc4	obec
stále	stále	k6eAd1	stále
další	další	k2eAgFnPc4d1	další
povinnosti	povinnost	k1gFnPc4	povinnost
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
když	když	k8xS	když
stát	stát	k5eAaPmF	stát
takto	takto	k6eAd1	takto
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
náklady	náklad	k1gInPc4	náklad
obcím	obec	k1gFnPc3	obec
dostatečně	dostatečně	k6eAd1	dostatečně
nehradí	hradit	k5eNaImIp3nP	hradit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
obce	obec	k1gFnPc1	obec
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgFnPc1d1	malá
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
neexistuje	existovat	k5eNaImIp3nS	existovat
jiná	jiný	k2eAgFnSc1d1	jiná
země	země	k1gFnSc1	země
kromě	kromě	k7c2	kromě
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
primátora	primátor	k1gMnSc2	primátor
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g />
.	.	kIx.	.
</s>
<s>
byrokracii	byrokracie	k1gFnSc4	byrokracie
<g/>
,	,	kIx,	,
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
znovuzavedení	znovuzavedení	k1gNnSc4	znovuzavedení
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
zrušení	zrušení	k1gNnSc4	zrušení
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
nedostatkem	nedostatek	k1gInSc7	nedostatek
strážníků	strážník	k1gMnPc2	strážník
a	a	k8xC	a
zdůvodňoval	zdůvodňovat	k5eAaImAgMnS	zdůvodňovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Teplice	Teplice	k1gFnPc1	Teplice
neuspořádaly	uspořádat	k5eNaPmAgFnP	uspořádat
novoroční	novoroční	k2eAgInSc4d1	novoroční
ohňostroj	ohňostroj	k1gInSc4	ohňostroj
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
analýze	analýza	k1gFnSc6	analýza
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
feminismu	feminismus	k1gInSc3	feminismus
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
médiích	médium	k1gNnPc6	médium
za	za	k7c2	za
období	období	k1gNnSc2	období
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
mezi	mezi	k7c7	mezi
8	[number]	k4	8
jmenovanými	jmenovaný	k2eAgFnPc7d1	jmenovaná
osobnostmi	osobnost	k1gFnPc7	osobnost
vystupujícími	vystupující	k2eAgFnPc7d1	vystupující
proti	proti	k7c3	proti
feminismu	feminismus	k1gInSc2	feminismus
<g/>
,	,	kIx,	,
výslovně	výslovně	k6eAd1	výslovně
byl	být	k5eAaImAgInS	být
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
Kuberův	Kuberův	k2eAgInSc1d1	Kuberův
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
neukojené	ukojený	k2eNgFnSc2d1	neukojená
feministky	feministka	k1gFnSc2	feministka
<g/>
"	"	kIx"	"
citovaný	citovaný	k2eAgMnSc1d1	citovaný
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Václava	Václav	k1gMnSc2	Václav
Dolejšího	Dolejší	k1gMnSc2	Dolejší
v	v	k7c6	v
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
diskusi	diskuse	k1gFnSc6	diskuse
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
se	se	k3xPyFc4	se
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
o	o	k7c4	o
rovnost	rovnost	k1gFnSc4	rovnost
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
pohlaví	pohlaví	k1gNnSc4	pohlaví
a	a	k8xC	a
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
registrovat	registrovat	k5eAaBmF	registrovat
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
i	i	k9	i
vztah	vztah	k1gInSc1	vztah
druh	druh	k1gInSc1	druh
–	–	k?	–
družka	družka	k1gFnSc1	družka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
V	v	k7c6	v
Nedělní	nedělní	k2eAgFnSc6d1	nedělní
partii	partie	k1gFnSc6	partie
na	na	k7c6	na
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
proti	proti	k7c3	proti
návrhům	návrh	k1gInPc3	návrh
ministryně	ministryně	k1gFnSc2	ministryně
Džamily	Džamily	k1gFnSc3	Džamily
Stehlíkové	Stehlíková	k1gFnSc3	Stehlíková
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zákon	zákon	k1gInSc1	zákon
zakázal	zakázat	k5eAaPmAgInS	zakázat
tělesné	tělesný	k2eAgInPc4d1	tělesný
tresty	trest	k1gInPc4	trest
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
rodinách	rodina	k1gFnPc6	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
EU	EU	kA	EU
na	na	k7c4	na
aplikaci	aplikace	k1gFnSc4	aplikace
zákonů	zákon	k1gInPc2	zákon
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pokut	pokuta	k1gFnPc2	pokuta
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
děsivý	děsivý	k2eAgInSc4d1	děsivý
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
k	k	k7c3	k
výrokům	výrok	k1gInPc3	výrok
senátorky	senátorka	k1gFnSc2	senátorka
Liany	Liana	k1gFnSc2	Liana
Janáčkové	Janáčková	k1gFnSc2	Janáčková
o	o	k7c6	o
Romech	Rom	k1gMnPc6	Rom
<g/>
,	,	kIx,	,
k	k	k7c3	k
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
diskriminaci	diskriminace	k1gFnSc3	diskriminace
<g/>
,	,	kIx,	,
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
trestnosti	trestnost	k1gFnSc3	trestnost
přechovávání	přechovávání	k1gNnSc2	přechovávání
dětské	dětský	k2eAgFnSc2d1	dětská
pornografie	pornografie	k1gFnSc2	pornografie
a	a	k8xC	a
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
primátora	primátor	k1gMnSc2	primátor
komentoval	komentovat	k5eAaBmAgMnS	komentovat
síť	síť	k1gFnSc4	síť
poboček	pobočka	k1gFnPc2	pobočka
Czech	Czech	k1gInSc4	Czech
Point	pointa	k1gFnPc2	pointa
a	a	k8xC	a
příspěvek	příspěvek	k1gInSc1	příspěvek
města	město	k1gNnSc2	město
na	na	k7c4	na
lékařský	lékařský	k2eAgInSc4d1	lékařský
přístroj	přístroj	k1gInSc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
senátní	senátní	k2eAgFnSc6d1	senátní
diskusi	diskuse	k1gFnSc6	diskuse
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
Kubera	Kubero	k1gNnSc2	Kubero
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebyli	být	k5eNaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
vedeni	vést	k5eAaImNgMnP	vést
vůbec	vůbec	k9	vůbec
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
nechtěli	chtít	k5eNaImAgMnP	chtít
trestat	trestat	k5eAaImF	trestat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
nás	my	k3xPp1nPc4	my
nezaujalo	zaujmout	k5eNaPmAgNnS	zaujmout
<g/>
,	,	kIx,	,
kolika	kolik	k4yRc6	kolik
hlasy	hlas	k1gInPc7	hlas
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
schváleno	schválit	k5eAaPmNgNnS	schválit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tyto	tento	k3xDgInPc1	tento
zákony	zákon	k1gInPc1	zákon
většinou	většinou	k6eAd1	většinou
bývají	bývat	k5eAaImIp3nP	bývat
schváleny	schválit	k5eAaPmNgInP	schválit
<g />
.	.	kIx.	.
</s>
<s>
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
poslanci	poslanec	k1gMnPc1	poslanec
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
dny	den	k1gInPc4	den
a	a	k8xC	a
bojí	bát	k5eAaImIp3nP	bát
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
kdo	kdo	k3yRnSc1	kdo
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
bulvárních	bulvární	k2eAgNnPc6d1	bulvární
médiích	médium	k1gNnPc6	médium
označen	označit	k5eAaPmNgInS	označit
za	za	k7c2	za
pedofila	pedofil	k1gMnSc2	pedofil
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
dobrým	dobrý	k2eAgInSc7d1	dobrý
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
někdo	někdo	k3yInSc1	někdo
relevantně	relevantně	k6eAd1	relevantně
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
iniciátorem	iniciátor	k1gMnSc7	iniciátor
novely	novela	k1gFnSc2	novela
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ruší	rušit	k5eAaImIp3nS	rušit
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
majitelů	majitel	k1gMnPc2	majitel
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
nemovitostí	nemovitost	k1gFnPc2	nemovitost
za	za	k7c4	za
škody	škoda	k1gFnPc4	škoda
způsobené	způsobený	k2eAgFnPc1d1	způsobená
závadami	závada	k1gFnPc7	závada
ve	v	k7c6	v
schůdnosti	schůdnost	k1gFnSc6	schůdnost
chodníku	chodník	k1gInSc2	chodník
a	a	k8xC	a
de	de	k?	de
facto	facto	k1gNnSc1	facto
tak	tak	k9	tak
ruší	rušit	k5eAaImIp3nS	rušit
jejich	jejich	k3xOp3gFnSc4	jejich
tradiční	tradiční	k2eAgFnSc4d1	tradiční
povinnost	povinnost	k1gFnSc4	povinnost
zajistit	zajistit	k5eAaPmF	zajistit
zimní	zimní	k2eAgFnSc4d1	zimní
údržbu	údržba	k1gFnSc4	údržba
chodníku	chodník	k1gInSc2	chodník
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
přes	přes	k7c4	přes
výhrady	výhrada	k1gFnPc4	výhrada
podpořil	podpořit	k5eAaPmAgInS	podpořit
návrh	návrh	k1gInSc1	návrh
nového	nový	k2eAgInSc2d1	nový
<g />
.	.	kIx.	.
</s>
<s>
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
uvítal	uvítat	k5eAaPmAgMnS	uvítat
zejména	zejména	k9	zejména
zavedení	zavedení	k1gNnSc4	zavedení
alternativních	alternativní	k2eAgInPc2d1	alternativní
trestů	trest	k1gInPc2	trest
typu	typ	k1gInSc2	typ
domácího	domácí	k2eAgNnSc2d1	domácí
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
sledovacích	sledovací	k2eAgInPc2d1	sledovací
elektronických	elektronický	k2eAgInPc2d1	elektronický
náramků	náramek	k1gInPc2	náramek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
"	"	kIx"	"
<g/>
šrotovného	šrotovné	k1gNnSc2	šrotovné
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
státní	státní	k2eAgFnSc2d1	státní
podpory	podpora	k1gFnSc2	podpora
nákupu	nákup	k1gInSc2	nákup
nových	nový	k2eAgNnPc2d1	nové
vozidel	vozidlo	k1gNnPc2	vozidlo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
řešení	řešení	k1gNnSc1	řešení
světové	světový	k2eAgFnSc2d1	světová
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
zastává	zastávat	k5eAaImIp3nS	zastávat
Kubera	Kubera	k1gFnSc1	Kubera
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
šrotovné	šrotovné	k1gNnSc1	šrotovné
by	by	kYmCp3nP	by
majitelům	majitel	k1gMnPc3	majitel
starých	starý	k2eAgNnPc2d1	staré
vozidel	vozidlo	k1gNnPc2	vozidlo
neměl	mít	k5eNaImAgInS	mít
platit	platit	k5eAaImF	platit
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
automobilky	automobilka	k1gFnPc1	automobilka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
odbytem	odbyt	k1gInSc7	odbyt
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
navázala	navázat	k5eAaPmAgFnS	navázat
bližší	blízký	k2eAgFnSc4d2	bližší
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
mimoparlamentní	mimoparlamentní	k2eAgFnSc7d1	mimoparlamentní
Stranou	strana	k1gFnSc7	strana
svobodných	svobodný	k2eAgMnPc2d1	svobodný
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kubery	Kubera	k1gFnSc2	Kubera
"	"	kIx"	"
<g/>
program	program	k1gInSc1	program
Svobodných	svobodný	k2eAgMnPc2d1	svobodný
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
program	program	k1gInSc4	program
ODS	ODS	kA	ODS
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
úpravami	úprava	k1gFnPc7	úprava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Svobodných	svobodný	k2eAgMnPc2d1	svobodný
Petr	Petr	k1gMnSc1	Petr
Mach	Mach	k1gMnSc1	Mach
spolupráci	spolupráce	k1gFnSc4	spolupráce
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
nespokojené	spokojený	k2eNgMnPc4d1	nespokojený
členy	člen	k1gMnPc4	člen
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přešli	přejít	k5eAaPmAgMnP	přejít
ke	k	k7c3	k
Svobodným	svobodný	k2eAgFnPc3d1	svobodná
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
obhájil	obhájit	k5eAaPmAgMnS	obhájit
post	post	k1gInSc4	post
zastupitele	zastupitel	k1gMnSc2	zastupitel
Teplic	Teplice	k1gFnPc2	Teplice
<g/>
,	,	kIx,	,
když	když	k8xS	když
vedl	vést	k5eAaImAgInS	vést
kandidátku	kandidátka	k1gFnSc4	kandidátka
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
volby	volba	k1gFnSc2	volba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
(	(	kIx(	(
<g/>
26,74	[number]	k4	26,74
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
8	[number]	k4	8
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
se	se	k3xPyFc4	se
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kuber	k1gMnSc4	Kuber
stal	stát	k5eAaPmAgMnS	stát
pošesté	pošesté	k4xO	pošesté
primátorem	primátor	k1gMnSc7	primátor
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Teplice	teplice	k1gFnSc2	teplice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
ODS	ODS	kA	ODS
zvolen	zvolit	k5eAaPmNgMnS	zvolit
zastupitelem	zastupitel	k1gMnSc7	zastupitel
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
původně	původně	k6eAd1	původně
figuroval	figurovat	k5eAaImAgInS	figurovat
na	na	k7c4	na
55	[number]	k4	55
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
když	když	k8xS	když
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
obdržel	obdržet	k5eAaPmAgInS	obdržet
59	[number]	k4	59
hlasů	hlas	k1gInPc2	hlas
od	od	k7c2	od
78	[number]	k4	78
přítomných	přítomný	k2eAgMnPc2d1	přítomný
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
postu	post	k1gInSc2	post
předsedy	předseda	k1gMnSc2	předseda
Senátorského	senátorský	k2eAgInSc2d1	senátorský
klubu	klub	k1gInSc2	klub
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Miloš	Miloš	k1gMnSc1	Miloš
Vystrčil	Vystrčil	k1gMnSc1	Vystrčil
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2017	[number]	k4	2017
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
získal	získat	k5eAaPmAgInS	získat
potřebný	potřebný	k2eAgInSc1d1	potřebný
počet	počet	k1gInSc1	počet
podpisů	podpis	k1gInPc2	podpis
pro	pro	k7c4	pro
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kandidaturu	kandidatura	k1gFnSc4	kandidatura
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
nepodal	podat	k5eNaPmAgMnS	podat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
za	za	k7c2	za
ODS	ODS	kA	ODS
mandát	mandát	k1gInSc1	mandát
senátora	senátor	k1gMnSc2	senátor
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
32	[number]	k4	32
–	–	k?	–
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
41,81	[number]	k4	41,81
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
utkal	utkat	k5eAaPmAgMnS	utkat
s	s	k7c7	s
nestraníkem	nestraník	k1gMnSc7	nestraník
za	za	k7c2	za
hnutí	hnutí	k1gNnSc2	hnutí
Senátor	senátor	k1gMnSc1	senátor
21	[number]	k4	21
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Bergmanem	Bergman	k1gMnSc7	Bergman
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
porazil	porazit	k5eAaPmAgMnS	porazit
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
55,60	[number]	k4	55,60
%	%	kIx~	%
:	:	kIx,	:
44,39	[number]	k4	44,39
%	%	kIx~	%
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
tak	tak	k6eAd1	tak
senátorem	senátor	k1gMnSc7	senátor
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgMnS	být
lídrem	lídr	k1gMnSc7	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
ODS	ODS	kA	ODS
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
Teplice	teplice	k1gFnSc2	teplice
<g/>
,	,	kIx,	,
mandát	mandát	k1gInSc4	mandát
zastupitele	zastupitel	k1gMnSc2	zastupitel
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Vítězná	vítězný	k2eAgFnSc1d1	vítězná
ODS	ODS	kA	ODS
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
koalici	koalice	k1gFnSc4	koalice
se	s	k7c7	s
třetím	třetí	k4xOgInSc6	třetí
hnutí	hnutí	k1gNnSc6	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
novým	nový	k2eAgMnSc7d1	nový
primátorem	primátor	k1gMnSc7	primátor
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Hynek	Hynek	k1gMnSc1	Hynek
Hanza	hanza	k1gFnSc1	hanza
z	z	k7c2	z
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Kubera	Kubero	k1gNnSc2	Kubero
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
věnoval	věnovat	k5eAaImAgInS	věnovat
plně	plně	k6eAd1	plně
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
primátorem	primátor	k1gMnSc7	primátor
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
a	a	k8xC	a
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
tak	tak	k6eAd1	tak
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
Milana	Milan	k1gMnSc2	Milan
Štěcha	Štěch	k1gMnSc2	Štěch
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
první	první	k4xOgFnSc2	první
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
porazil	porazit	k5eAaPmAgMnS	porazit
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
46	[number]	k4	46
:	:	kIx,	:
24	[number]	k4	24
Václava	Václav	k1gMnSc2	Václav
Hampla	Hampl	k1gMnSc2	Hampl
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c2	za
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
Zelené	Zelená	k1gFnSc2	Zelená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gMnSc7	jejich
soupeřem	soupeř	k1gMnSc7	soupeř
ještě	ještě	k6eAd1	ještě
Jan	Jan	k1gMnSc1	Jan
Horník	Horník	k1gMnSc1	Horník
(	(	kIx(	(
<g/>
STAN	stan	k1gInSc1	stan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herecká	herecký	k2eAgFnSc1d1	herecká
zkušenost	zkušenost	k1gFnSc1	zkušenost
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
režisér	režisér	k1gMnSc1	režisér
Ondřej	Ondřej	k1gMnSc1	Ondřej
Trojan	Trojan	k1gMnSc1	Trojan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
natáčel	natáčet	k5eAaImAgMnS	natáčet
film	film	k1gInSc4	film
Toman	Toman	k1gMnSc1	Toman
popisující	popisující	k2eAgInPc4d1	popisující
osudy	osud	k1gInPc4	osud
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Tomana	Toman	k1gMnSc2	Toman
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k1gMnSc2	vedoucí
odboru	odbor	k1gInSc2	odbor
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
rozvědky	rozvědka	k1gFnSc2	rozvědka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
až	až	k9	až
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
Kubera	Kuber	k1gMnSc4	Kuber
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
roli	role	k1gFnSc4	role
prezidenta	prezident	k1gMnSc2	prezident
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
herecká	herecký	k2eAgFnSc1d1	herecká
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kubero	k1gNnSc2	Kubero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kubera	k1gFnSc1	Kubera
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kubera	k1gFnSc1	Kubera
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgInSc1d1	osobní
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kubera	k1gFnSc1	Kubera
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgInSc1d1	osobní
blog	blog	k1gInSc1	blog
na	na	k7c4	na
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kuber	k1gMnSc2	Kuber
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kubera	Kubero	k1gNnSc2	Kubero
v	v	k7c6	v
Impulsech	impuls	k1gInPc6	impuls
Václava	Václav	k1gMnSc2	Václav
Moravce	Moravec	k1gMnSc2	Moravec
<g/>
;	;	kIx,	;
interview	interview	k1gNnSc1	interview
pro	pro	k7c4	pro
rádio	rádio	k1gNnSc4	rádio
Impuls	impuls	k1gInSc4	impuls
</s>
</p>
