<s>
Dekameron	Dekameron	k1gInSc1	Dekameron
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
sta	sto	k4xCgNnSc2	sto
novel	novela	k1gFnPc2	novela
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
rozdělených	rozdělená	k1gFnPc2	rozdělená
po	po	k7c6	po
deseti	deset	k4xCc2	deset
na	na	k7c4	na
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
si	se	k3xPyFc3	se
vypráví	vyprávět	k5eAaImIp3nP	vyprávět
tři	tři	k4xCgMnPc1	tři
mladí	mladý	k2eAgMnPc1d1	mladý
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
mladých	mladý	k2eAgFnPc2d1	mladá
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
utekli	utéct	k5eAaPmAgMnP	utéct
z	z	k7c2	z
města	město	k1gNnSc2	město
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zachránili	zachránit	k5eAaPmAgMnP	zachránit
před	před	k7c7	před
morem	mor	k1gInSc7	mor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
napsal	napsat	k5eAaBmAgInS	napsat
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Boccaccio	Boccaccio	k6eAd1	Boccaccio
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1348	[number]	k4	1348
až	až	k9	až
1353	[number]	k4	1353
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
úvodu	úvod	k1gInSc6	úvod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rovněž	rovněž	k9	rovněž
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
a	a	k8xC	a
nejpodrobnějších	podrobný	k2eAgInPc2d3	nejpodrobnější
popisů	popis	k1gInPc2	popis
moru	mora	k1gFnSc4	mora
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
z	z	k7c2	z
Dekameronu	Dekameron	k1gInSc2	Dekameron
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
náměty	námět	k1gInPc1	námět
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgNnPc2d1	další
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěči	vypravěč	k1gMnSc3	vypravěč
je	být	k5eAaImIp3nS	být
sedm	sedm	k4xCc4	sedm
urozených	urozený	k2eAgFnPc2d1	urozená
paní	paní	k1gFnPc2	paní
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
<g/>
:	:	kIx,	:
Filostrato	Filostrat	k2eAgNnSc4d1	Filostrato
<g/>
,	,	kIx,	,
Dioneo	Dioneo	k1gMnSc1	Dioneo
<g/>
,	,	kIx,	,
Pampinea	Pampinea	k1gMnSc1	Pampinea
<g/>
,	,	kIx,	,
Filomena	Filomen	k2eAgFnSc1d1	Filomena
<g/>
,	,	kIx,	,
Pamfilo	Pamfila	k1gFnSc5	Pamfila
<g/>
,	,	kIx,	,
Elisa	Elis	k1gMnSc4	Elis
<g/>
,	,	kIx,	,
Fiammetta	Fiammett	k1gMnSc4	Fiammett
<g/>
,	,	kIx,	,
Emilia	Emilius	k1gMnSc4	Emilius
<g/>
,	,	kIx,	,
Lauretta	Lauretta	k1gFnSc1	Lauretta
a	a	k8xC	a
Neifile	Neifila	k1gFnSc3	Neifila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
žil	žít	k5eAaImAgMnS	žít
urozený	urozený	k2eAgMnSc1d1	urozený
muž	muž	k1gMnSc1	muž
Ermino	Ermin	k2eAgNnSc1d1	Ermin
de	de	k?	de
Grimaldi	Grimald	k1gMnPc1	Grimald
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgInSc1d1	bohatý
a	a	k8xC	a
také	také	k9	také
velmi	velmi	k6eAd1	velmi
lakomý	lakomý	k2eAgMnSc1d1	lakomý
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
do	do	k7c2	do
města	město	k1gNnSc2	město
přijel	přijet	k5eAaPmAgMnS	přijet
velmi	velmi	k6eAd1	velmi
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
muž	muž	k1gMnSc1	muž
dobrých	dobrý	k2eAgInPc2d1	dobrý
mravů	mrav	k1gInPc2	mrav
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
tam	tam	k6eAd1	tam
každý	každý	k3xTgInSc4	každý
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
Guiglielmo	Guiglielma	k1gFnSc5	Guiglielma
Borsiere	Borsier	k1gInSc5	Borsier
<g/>
.	.	kIx.	.
</s>
<s>
Doslechl	doslechnout	k5eAaPmAgInS	doslechnout
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
Ermino	Ermino	k1gNnSc1	Ermino
lakomý	lakomý	k2eAgMnSc1d1	lakomý
<g/>
,	,	kIx,	,
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Emino	Emin	k2eAgNnSc1d1	Emino
ho	on	k3xPp3gMnSc4	on
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
slávě	sláva	k1gFnSc3	sláva
už	už	k6eAd1	už
slyšel	slyšet	k5eAaImAgMnS	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Chvíli	chvíle	k1gFnSc4	chvíle
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
povídal	povídal	k1gMnSc1	povídal
a	a	k8xC	a
poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
zavedl	zavést	k5eAaPmAgMnS	zavést
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
nově	nově	k6eAd1	nově
postaveného	postavený	k2eAgInSc2d1	postavený
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
zeptal	zeptat	k5eAaPmAgMnS	zeptat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
znalý	znalý	k2eAgMnSc1d1	znalý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
poradil	poradit	k5eAaPmAgInS	poradit
něco	něco	k3yInSc4	něco
nevídaného	vídaný	k2eNgNnSc2d1	nevídané
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
nechat	nechat	k5eAaPmF	nechat
namalovat	namalovat	k5eAaPmF	namalovat
v	v	k7c6	v
sále	sál	k1gInSc6	sál
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Guiglielmo	Guiglielmo	k6eAd1	Guiglielmo
mu	on	k3xPp3gMnSc3	on
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
nechat	nechat	k5eAaPmF	nechat
namalovat	namalovat	k5eAaPmF	namalovat
štědrost	štědrost	k1gFnSc4	štědrost
<g/>
.	.	kIx.	.
</s>
<s>
Ermino	Ermino	k1gNnSc1	Ermino
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
zastyděl	zastydět	k5eAaPmAgMnS	zastydět
<g/>
,	,	kIx,	,
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
lakotu	lakota	k1gFnSc4	lakota
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
byl	být	k5eAaImAgMnS	být
nejštědřejším	štědrý	k2eAgMnSc7d3	nejštědřejší
a	a	k8xC	a
nejšlechetnějším	šlechetný	k2eAgMnSc7d3	nejšlechetnější
šlechticem	šlechtic	k1gMnSc7	šlechtic
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jedna	jeden	k4xCgFnSc1	jeden
urozená	urozený	k2eAgFnSc1d1	urozená
gaskoňská	gaskoňský	k2eAgFnSc1d1	gaskoňská
paní	paní	k1gFnSc1	paní
putovala	putovat	k5eAaImAgFnS	putovat
k	k	k7c3	k
Svatému	svatý	k2eAgInSc3d1	svatý
hrobu	hrob	k1gInSc3	hrob
<g/>
,	,	kIx,	,
potupilo	potupit	k5eAaPmAgNnS	potupit
ji	on	k3xPp3gFnSc4	on
při	při	k7c6	při
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
pár	pár	k4xCyI	pár
zlosynů	zlosyn	k1gMnPc2	zlosyn
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
smutná	smutný	k2eAgFnSc1d1	smutná
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
jí	on	k3xPp3gFnSc3	on
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
utěšit	utěšit	k5eAaPmF	utěšit
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
půjde	jít	k5eAaImIp3nS	jít
stěžovat	stěžovat	k5eAaImF	stěžovat
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Doslechla	doslechnout	k5eAaPmAgFnS	doslechnout
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
prý	prý	k9	prý
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
netrestá	trestat	k5eNaImIp3nS	trestat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
někomu	někdo	k3yInSc3	někdo
něco	něco	k3yInSc1	něco
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
když	když	k8xS	když
potupí	potupit	k5eAaPmIp3nS	potupit
jeho	jeho	k3xOp3gMnSc1	jeho
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
to	ten	k3xDgNnSc4	ten
opět	opět	k6eAd1	opět
zamrzelo	zamrzet	k5eAaPmAgNnS	zamrzet
a	a	k8xC	a
přestala	přestat	k5eAaPmAgFnS	přestat
doufat	doufat	k5eAaImF	doufat
po	po	k7c6	po
pomstě	pomsta	k1gFnSc6	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
půjde	jít	k5eAaImIp3nS	jít
vysmát	vysmát	k5eAaPmF	vysmát
králi	král	k1gMnSc3	král
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
ubohost	ubohost	k1gFnSc4	ubohost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
před	před	k7c4	před
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
zeptala	zeptat	k5eAaPmAgFnS	zeptat
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
snáší	snášet	k5eAaImIp3nS	snášet
křivdy	křivda	k1gFnSc2	křivda
sám	sám	k3xTgMnSc1	sám
na	na	k7c4	na
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
probudil	probudit	k5eAaPmAgInS	probudit
a	a	k8xC	a
potrestal	potrestat	k5eAaPmAgInS	potrestat
ty	ten	k3xDgMnPc4	ten
zlosyny	zlosyn	k1gMnPc4	zlosyn
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
paní	paní	k1gFnSc4	paní
potupili	potupit	k5eAaPmAgMnP	potupit
<g/>
,	,	kIx,	,
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
trestat	trestat	k5eAaImF	trestat
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
nějak	nějak	k6eAd1	nějak
provinil	provinit	k5eAaPmAgMnS	provinit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
Trevisu	Trevis	k1gInSc2	Trevis
přijeli	přijet	k5eAaPmAgMnP	přijet
tři	tři	k4xCgMnPc1	tři
komici	komik	k1gMnPc1	komik
právě	právě	k6eAd1	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tam	tam	k6eAd1	tam
probíhala	probíhat	k5eAaImAgFnS	probíhat
velká	velký	k2eAgFnSc1d1	velká
oslava	oslava	k1gFnSc1	oslava
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svatého	svatý	k2eAgMnSc2d1	svatý
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
sjížděli	sjíždět	k5eAaImAgMnP	sjíždět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
pomodlit	pomodlit	k5eAaPmF	pomodlit
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
truhle	truhla	k1gFnSc3	truhla
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
a	a	k8xC	a
prosit	prosit	k5eAaImF	prosit
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
tři	tři	k4xCgMnPc1	tři
mládenci	mládenec	k1gMnPc1	mládenec
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
sv.	sv.	kA	sv.
Jindřichem	Jindřich	k1gMnSc7	Jindřich
vydají	vydat	k5eAaPmIp3nP	vydat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přes	přes	k7c4	přes
dav	dav	k1gInSc4	dav
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
nemohli	moct	k5eNaImAgMnP	moct
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Marttelino	Marttelin	k2eAgNnSc1d1	Marttelin
<g/>
,	,	kIx,	,
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
předstírat	předstírat	k5eAaImF	předstírat
ochrnutí	ochrnutí	k1gNnSc4	ochrnutí
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
i	i	k9	i
učinil	učinit	k5eAaPmAgMnS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
Marches	Marches	k1gMnSc1	Marches
a	a	k8xC	a
Stechimio	Stechimio	k1gMnSc1	Stechimio
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
nesli	nést	k5eAaImAgMnP	nést
k	k	k7c3	k
sv.	sv.	kA	sv.
Jindřichovi	Jindřichův	k2eAgMnPc5d1	Jindřichův
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přece	přece	k9	přece
jen	jen	k6eAd1	jen
dostali	dostat	k5eAaPmAgMnP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jeho	on	k3xPp3gInSc2	on
hrobu	hrob	k1gInSc2	hrob
se	se	k3xPyFc4	se
Marttelino	Marttelin	k2eAgNnSc1d1	Marttelin
jakoby	jakoby	k8xS	jakoby
zázrakem	zázrak	k1gInSc7	zázrak
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
podvod	podvod	k1gInSc1	podvod
byl	být	k5eAaImAgInS	být
však	však	k9	však
zanedlouho	zanedlouho	k6eAd1	zanedlouho
odhalen	odhalen	k2eAgMnSc1d1	odhalen
a	a	k8xC	a
Marttelino	Marttelin	k2eAgNnSc1d1	Marttelin
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
před	před	k7c4	před
soud	soud	k1gInSc4	soud
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
před	před	k7c4	před
samotného	samotný	k2eAgMnSc4d1	samotný
vladaře	vladař	k1gMnSc4	vladař
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gInSc4	jeho
příběh	příběh	k1gInSc4	příběh
vyslechl	vyslechnout	k5eAaPmAgInS	vyslechnout
a	a	k8xC	a
s	s	k7c7	s
chutí	chuť	k1gFnSc7	chuť
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zasmál	zasmát	k5eAaPmAgMnS	zasmát
<g/>
.	.	kIx.	.
</s>
<s>
Marttelinovi	Marttelin	k1gMnSc3	Marttelin
udělil	udělit	k5eAaPmAgMnS	udělit
milost	milost	k1gFnSc4	milost
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
komici	komik	k1gMnPc1	komik
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
....	....	k?	....
Do	do	k7c2	do
Bologne	Bologna	k1gFnSc2	Bologna
přijel	přijet	k5eAaPmAgMnS	přijet
kupec	kupec	k1gMnSc1	kupec
Rinaldo	Rinaldo	k1gNnSc4	Rinaldo
z	z	k7c2	z
Asti	Ast	k1gFnSc2	Ast
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
všechno	všechen	k3xTgNnSc4	všechen
vyřídil	vyřídit	k5eAaPmAgMnS	vyřídit
a	a	k8xC	a
vracel	vracet	k5eAaImAgMnS	vracet
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
pár	pár	k4xCyI	pár
lupičů	lupič	k1gMnPc2	lupič
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
netuše	tušit	k5eNaImSgInS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
lupiči	lupič	k1gMnPc1	lupič
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
chovali	chovat	k5eAaImAgMnP	chovat
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
okradli	okrást	k5eAaPmAgMnP	okrást
<g/>
.	.	kIx.	.
</s>
<s>
Zůstal	zůstat	k5eAaPmAgMnS	zůstat
bos	bos	k1gMnSc1	bos
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
košili	košile	k1gFnSc6	košile
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
mrazu	mráz	k1gInSc6	mráz
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
najít	najít	k5eAaPmF	najít
žádné	žádný	k3yNgNnSc4	žádný
přístřeší	přístřeší	k1gNnSc4	přístřeší
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
tvrzi	tvrz	k1gFnSc3	tvrz
Guiglielmo	Guiglielma	k1gFnSc5	Guiglielma
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
ní	on	k3xPp3gFnSc2	on
našel	najít	k5eAaPmAgInS	najít
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
žila	žít	k5eAaImAgFnS	žít
vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zprvu	zprvu	k6eAd1	zprvu
očekávala	očekávat	k5eAaImAgFnS	očekávat
markýze	markýz	k1gMnSc4	markýz
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
milence	milenec	k1gMnSc4	milenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
tedy	tedy	k9	tedy
nachystanou	nachystaný	k2eAgFnSc4d1	nachystaná
koupel	koupel	k1gFnSc4	koupel
i	i	k8xC	i
dobré	dobrý	k2eAgNnSc4d1	dobré
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
nedorazil	dorazit	k5eNaPmAgMnS	dorazit
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
tedy	tedy	k9	tedy
pohostila	pohostit	k5eAaPmAgFnS	pohostit
Rinalda	Rinald	k1gMnSc4	Rinald
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
líbil	líbit	k5eAaImAgMnS	líbit
<g/>
,	,	kIx,	,
a	a	k8xC	a
odvedla	odvést	k5eAaPmAgFnS	odvést
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
komnaty	komnata	k1gFnSc2	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
mu	on	k3xPp3gMnSc3	on
dala	dát	k5eAaPmAgFnS	dát
nějaké	nějaký	k3yIgInPc4	nějaký
starší	starý	k2eAgInPc4d2	starší
šaty	šat	k1gInPc4	šat
a	a	k8xC	a
mníšek	mníšek	k1gMnSc1	mníšek
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
poprosila	poprosit	k5eAaPmAgFnS	poprosit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vše	všechen	k3xTgNnSc4	všechen
uchoval	uchovat	k5eAaPmAgMnS	uchovat
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
ukázala	ukázat	k5eAaPmAgFnS	ukázat
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jelikož	jelikož	k8xS	jelikož
toho	ten	k3xDgInSc2	ten
večera	večer	k1gInSc2	večer
byli	být	k5eAaImAgMnP	být
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
i	i	k9	i
lupiči	lupič	k1gMnPc1	lupič
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
Rinaldovi	Rinald	k1gMnSc3	Rinald
jeho	jeho	k3xOp3gFnSc6	jeho
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc1	oblečení
i	i	k8xC	i
peníze	peníz	k1gInPc1	peníz
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
se	se	k3xPyFc4	se
živ	živ	k2eAgInSc1d1	živ
a	a	k8xC	a
zdráv	zdráv	k2eAgMnSc1d1	zdráv
vrátit	vrátit	k5eAaPmF	vrátit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Landolfo	Landolfa	k1gFnSc5	Landolfa
Ruffolo	Ruffola	k1gFnSc5	Ruffola
byl	být	k5eAaImAgMnS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
jeden	jeden	k4xCgInSc1	jeden
obchod	obchod	k1gInSc1	obchod
nevyšel	vyjít	k5eNaPmAgInS	vyjít
<g/>
,	,	kIx,	,
zchudl	zchudnout	k5eAaPmAgInS	zchudnout
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
pirátem	pirát	k1gMnSc7	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
naloupil	naloupit	k5eAaPmAgInS	naloupit
hodně	hodně	k6eAd1	hodně
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
rodného	rodný	k2eAgNnSc2d1	rodné
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Janované	Janovan	k1gMnPc1	Janovan
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
zajali	zajmout	k5eAaPmAgMnP	zajmout
a	a	k8xC	a
všechen	všechen	k3xTgInSc1	všechen
lup	lup	k1gInSc1	lup
mu	on	k3xPp3gMnSc3	on
sebrali	sebrat	k5eAaPmAgMnP	sebrat
<g/>
.	.	kIx.	.
</s>
<s>
Dopravili	dopravit	k5eAaPmAgMnP	dopravit
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
později	pozdě	k6eAd2	pozdě
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Ruffolo	Ruffola	k1gFnSc5	Ruffola
se	se	k3xPyFc4	se
zachránil	zachránit	k5eAaPmAgMnS	zachránit
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
bedně	bedna	k1gFnSc6	bedna
plné	plný	k2eAgFnSc2d1	plná
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
na	na	k7c6	na
bedně	bedna	k1gFnSc6	bedna
dopluje	doplout	k5eAaPmIp3nS	doplout
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Korfu	Korfu	k1gNnSc2	Korfu
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ruffolo	Ruffola	k1gFnSc5	Ruffola
jí	on	k3xPp3gFnSc3	on
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
ochotu	ochota	k1gFnSc4	ochota
věnoval	věnovat	k5eAaImAgInS	věnovat
hodně	hodně	k6eAd1	hodně
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
do	do	k7c2	do
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jako	jako	k9	jako
boháč	boháč	k1gMnSc1	boháč
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
podkoní	podkoní	k1gMnSc1	podkoní
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
manželky	manželka	k1gFnSc2	manželka
krále	král	k1gMnSc2	král
Agilufa	Agiluf	k1gMnSc2	Agiluf
<g/>
.	.	kIx.	.
</s>
<s>
Dělá	dělat	k5eAaImIp3nS	dělat
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jí	jíst	k5eAaImIp3nS	jíst
na	na	k7c6	na
očích	oko	k1gNnPc6	oko
vidí	vidět	k5eAaImIp3nP	vidět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
královna	královna	k1gFnSc1	královna
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
citech	cit	k1gInPc6	cit
nic	nic	k6eAd1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
podkoní	podkoní	k1gMnSc1	podkoní
shledá	shledat	k5eAaPmIp3nS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
beznadějná	beznadějný	k2eAgFnSc1d1	beznadějná
<g/>
,	,	kIx,	,
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
činu	čin	k1gInSc2	čin
bylo	být	k5eAaImAgNnS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
ke	k	k7c3	k
královně	královna	k1gFnSc3	královna
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
schová	schovat	k5eAaPmIp3nS	schovat
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
síně	síň	k1gFnSc2	síň
mezi	mezi	k7c7	mezi
královým	králův	k2eAgInSc7d1	králův
a	a	k8xC	a
královniným	královnin	k2eAgInSc7d1	královnin
pokojem	pokoj	k1gInSc7	pokoj
a	a	k8xC	a
vyčkává	vyčkávat	k5eAaImIp3nS	vyčkávat
<g/>
,	,	kIx,	,
až	až	k8xS	až
král	král	k1gMnSc1	král
půjde	jít	k5eAaImIp3nS	jít
za	za	k7c7	za
královnou	královna	k1gFnSc7	královna
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
komnaty	komnata	k1gFnSc2	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
skutečně	skutečně	k6eAd1	skutečně
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
hůlku	hůlka	k1gFnSc4	hůlka
a	a	k8xC	a
pochodeň	pochodeň	k1gFnSc4	pochodeň
<g/>
.	.	kIx.	.
</s>
<s>
Hůlkou	hůlka	k1gFnSc7	hůlka
třikrát	třikrát	k6eAd1	třikrát
zaklepá	zaklepat	k5eAaPmIp3nS	zaklepat
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
někdo	někdo	k3yInSc1	někdo
otevře	otevřít	k5eAaPmIp3nS	otevřít
a	a	k8xC	a
krále	král	k1gMnPc4	král
pustí	pustit	k5eAaPmIp3nS	pustit
do	do	k7c2	do
komnaty	komnata	k1gFnSc2	komnata
<g/>
.	.	kIx.	.
</s>
<s>
Podkoní	podkoní	k1gMnSc1	podkoní
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
pořídí	pořídit	k5eAaPmIp3nS	pořídit
stejný	stejný	k2eAgInSc4d1	stejný
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
hůlku	hůlka	k1gFnSc4	hůlka
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
obleče	obléct	k5eAaPmIp3nS	obléct
do	do	k7c2	do
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
ke	k	k7c3	k
dveřím	dveře	k1gFnPc3	dveře
královnina	královnin	k2eAgInSc2d1	královnin
pokoje	pokoj	k1gInSc2	pokoj
a	a	k8xC	a
zaklepe	zaklepat	k5eAaPmIp3nS	zaklepat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
král	král	k1gMnSc1	král
minulého	minulý	k2eAgInSc2d1	minulý
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Otevře	otevřít	k5eAaPmIp3nS	otevřít
mu	on	k3xPp3gMnSc3	on
rozespalá	rozespalý	k2eAgFnSc1d1	rozespalá
služka	služka	k1gFnSc1	služka
<g/>
,	,	kIx,	,
vezme	vzít	k5eAaPmIp3nS	vzít
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
pochodeň	pochodeň	k1gFnSc1	pochodeň
a	a	k8xC	a
pustí	pustit	k5eAaPmIp3nS	pustit
ho	on	k3xPp3gMnSc4	on
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Podkoní	podkoní	k1gMnSc1	podkoní
poté	poté	k6eAd1	poté
ulehne	ulehnout	k5eAaPmIp3nS	ulehnout
na	na	k7c4	na
lože	lože	k1gNnSc4	lože
ke	k	k7c3	k
královně	královna	k1gFnSc3	královna
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
pomiluje	pomilovat	k5eAaPmIp3nS	pomilovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
ona	onen	k3xDgFnSc1	onen
poznala	poznat	k5eAaPmAgFnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
podkoní	podkoní	k1gMnSc1	podkoní
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
královnině	královnin	k2eAgFnSc6d1	Královnina
komnatě	komnata	k1gFnSc6	komnata
objeví	objevit	k5eAaPmIp3nS	objevit
také	také	k9	také
král	král	k1gMnSc1	král
<g/>
;	;	kIx,	;
královna	královna	k1gFnSc1	královna
se	se	k3xPyFc4	se
diví	divit	k5eAaImIp3nS	divit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
když	když	k8xS	když
tu	tu	k6eAd1	tu
už	už	k9	už
jednou	jednou	k6eAd1	jednou
tuto	tento	k3xDgFnSc4	tento
noc	noc	k1gFnSc4	noc
byl	být	k5eAaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
si	se	k3xPyFc3	se
ihned	ihned	k6eAd1	ihned
domyslí	domyslet	k5eAaPmIp3nP	domyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
královnu	královna	k1gFnSc4	královna
někdo	někdo	k3yInSc1	někdo
obelstil	obelstít	k5eAaPmAgMnS	obelstít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedá	dát	k5eNaPmIp3nS	dát
na	na	k7c4	na
sobě	se	k3xPyFc3	se
nic	nic	k3yNnSc1	nic
znát	znát	k5eAaImF	znát
a	a	k8xC	a
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
všechny	všechen	k3xTgMnPc4	všechen
oklamal	oklamat	k5eAaPmAgMnS	oklamat
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
někdo	někdo	k3yInSc1	někdo
ze	z	k7c2	z
sloužících	sloužící	k1gMnPc2	sloužící
a	a	k8xC	a
že	že	k8xS	že
ještě	ještě	k9	ještě
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spí	spát	k5eAaImIp3nS	spát
sloužící	sloužící	k2eAgNnSc1d1	sloužící
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
postupně	postupně	k6eAd1	postupně
každému	každý	k3xTgMnSc3	každý
sáhne	sáhnout	k5eAaPmIp3nS	sáhnout
na	na	k7c4	na
hrudník	hrudník	k1gInSc4	hrudník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
komu	kdo	k3yRnSc3	kdo
neobyčejně	obyčejně	k6eNd1	obyčejně
buší	bušit	k5eAaImIp3nS	bušit
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Podkoní	podkoní	k1gMnSc1	podkoní
ještě	ještě	k9	ještě
nestačil	stačit	k5eNaBmAgMnS	stačit
usnout	usnout	k5eAaPmF	usnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
vidí	vidět	k5eAaImIp3nP	vidět
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dojde	dojít	k5eAaPmIp3nS	dojít
král	král	k1gMnSc1	král
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spí	spát	k5eAaImIp3nP	spát
<g/>
,	,	kIx,	,
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
král	král	k1gMnSc1	král
udělá	udělat	k5eAaPmIp3nS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
král	král	k1gMnSc1	král
dojde	dojít	k5eAaPmIp3nS	dojít
až	až	k9	až
k	k	k7c3	k
podkonímu	podkoní	k1gMnSc3	podkoní
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
neobyčejně	obyčejně	k6eNd1	obyčejně
rychle	rychle	k6eAd1	rychle
buší	bušit	k5eAaImIp3nS	bušit
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
ustřihne	ustřihnout	k5eAaPmIp3nS	ustřihnout
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
poznal	poznat	k5eAaPmAgMnS	poznat
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
ho	on	k3xPp3gMnSc4	on
potrestat	potrestat	k5eAaPmF	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Učiní	učinit	k5eAaImIp3nP	učinit
tak	tak	k6eAd1	tak
a	a	k8xC	a
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Podkoní	podkoní	k1gMnSc1	podkoní
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
král	král	k1gMnSc1	král
takto	takto	k6eAd1	takto
označil	označit	k5eAaPmAgMnS	označit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
obejde	obejít	k5eAaPmIp3nS	obejít
místnost	místnost	k1gFnSc1	místnost
a	a	k8xC	a
všem	všecek	k3xTgFnPc3	všecek
sloužícím	sloužící	k1gFnPc3	sloužící
stejně	stejně	k6eAd1	stejně
ustřihne	ustřihnout	k5eAaPmIp3nS	ustřihnout
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
k	k	k7c3	k
sobě	se	k3xPyFc3	se
král	král	k1gMnSc1	král
zavolá	zavolat	k5eAaPmIp3nS	zavolat
všechny	všechen	k3xTgFnPc4	všechen
sloužící	sloužící	k1gFnPc4	sloužící
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
viníka	viník	k1gMnSc4	viník
neodhalí	odhalit	k5eNaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
řekne	říct	k5eAaPmIp3nS	říct
jen	jen	k9	jen
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
včera	včera	k6eAd1	včera
provinil	provinit	k5eAaPmAgMnS	provinit
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
tak	tak	k9	tak
již	již	k6eAd1	již
nečiní	činit	k5eNaImIp3nS	činit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
podkoní	podkoní	k1gMnPc1	podkoní
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nepokoušel	pokoušet	k5eNaImAgMnS	pokoušet
štěstěnu	štěstěna	k1gFnSc4	štěstěna
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Pamfilo	Pamfila	k1gFnSc5	Pamfila
<g/>
:	:	kIx,	:
Puccio	Puccia	k1gMnSc5	Puccia
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
věřící	věřící	k1gMnSc1	věřící
prosťáček	prosťáček	k1gMnSc1	prosťáček
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
až	až	k9	až
příliš	příliš	k6eAd1	příliš
se	se	k3xPyFc4	se
postil	postit	k5eAaImAgInS	postit
a	a	k8xC	a
nedopřával	dopřávat	k5eNaImAgInS	dopřávat
Isabelle	Isabella	k1gFnSc3	Isabella
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnSc3	svůj
mladé	mladý	k2eAgFnSc3d1	mladá
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgFnSc3d1	krásná
<g/>
,	,	kIx,	,
svěží	svěží	k2eAgFnSc3d1	svěží
<g/>
,	,	kIx,	,
kulaťoučké	kulaťoučký	k2eAgFnSc3d1	kulaťoučká
manželce	manželka	k1gFnSc3	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Felice	Felice	k1gFnSc1	Felice
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
hezký	hezký	k2eAgMnSc1d1	hezký
řeholník	řeholník	k1gMnSc1	řeholník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
s	s	k7c7	s
Pucciem	Puccium	k1gNnSc7	Puccium
a	a	k8xC	a
najde	najít	k5eAaPmIp3nS	najít
zalíbení	zalíbení	k1gNnSc4	zalíbení
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
ženě	žena	k1gFnSc6	žena
<g/>
.	.	kIx.	.
</s>
<s>
Isabella	Isabella	k1gFnSc1	Isabella
vymyslí	vymyslet	k5eAaPmIp3nS	vymyslet
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svého	svůj	k1gMnSc4	svůj
muže	muž	k1gMnSc4	muž
dostala	dostat	k5eAaPmAgFnS	dostat
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Řeholník	řeholník	k1gMnSc1	řeholník
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
a	a	k8xC	a
pověděl	povědět	k5eAaPmAgMnS	povědět
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
dělat	dělat	k5eAaImF	dělat
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
věčné	věčný	k2eAgFnPc4d1	věčná
blaženosti	blaženost	k1gFnPc4	blaženost
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
Puccio	Puccio	k6eAd1	Puccio
dělal	dělat	k5eAaImAgMnS	dělat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
řeholník	řeholník	k1gMnSc1	řeholník
namluvil	namluvit	k5eAaBmAgInS	namluvit
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Isabetta	Isabetta	k1gMnSc1	Isabetta
společně	společně	k6eAd1	společně
s	s	k7c7	s
Felicem	Felic	k1gMnSc7	Felic
užili	užít	k5eAaPmAgMnP	užít
<g/>
.	.	kIx.	.
</s>
<s>
Puccio	Puccio	k1gMnSc1	Puccio
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
ráje	ráj	k1gInSc2	ráj
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
i	i	k8xC	i
Feliceho	Felice	k1gMnSc4	Felice
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ji	on	k3xPp3gFnSc4	on
obšťastnil	obšťastnit	k5eAaPmAgMnS	obšťastnit
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Elise	elise	k1gFnSc1	elise
<g/>
:	:	kIx,	:
Gerbino	Gerbin	k2eAgNnSc1d1	Gerbino
<g/>
,	,	kIx,	,
statečný	statečný	k2eAgMnSc1d1	statečný
<g/>
,	,	kIx,	,
naivní	naivní	k2eAgMnSc1d1	naivní
<g/>
,	,	kIx,	,
milující	milující	k2eAgMnSc1d1	milující
mladík	mladík	k1gMnSc1	mladík
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
dcery	dcera	k1gFnSc2	dcera
tureckého	turecký	k2eAgMnSc2d1	turecký
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
lásku	láska	k1gFnSc4	láska
také	také	k9	také
opětovala	opětovat	k5eAaImAgFnS	opětovat
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
druhého	druhý	k4xOgMnSc4	druhý
nikdy	nikdy	k6eAd1	nikdy
neviděli	vidět	k5eNaImAgMnP	vidět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
neštěstí	neštěstí	k1gNnSc3	neštěstí
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
dívka	dívka	k1gFnSc1	dívka
nešťastně	šťastně	k6eNd1	šťastně
vdát	vdát	k5eAaPmF	vdát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
láskou	láska	k1gFnSc7	láska
posedlý	posedlý	k2eAgInSc4d1	posedlý
Gerbino	Gerbin	k2eAgNnSc1d1	Gerbino
přeruší	přerušit	k5eAaPmIp3nS	přerušit
slib	slib	k1gInSc4	slib
svého	svůj	k3xOyFgMnSc2	svůj
děda	děd	k1gMnSc2	děd
Viléma	Vilém	k1gMnSc2	Vilém
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
míru	mír	k1gInSc2	mír
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
přepadne	přepadnout	k5eAaPmIp3nS	přepadnout
tureckou	turecký	k2eAgFnSc4d1	turecká
loď	loď	k1gFnSc4	loď
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
milovanou	milovaný	k2eAgFnSc7d1	milovaná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
vysvobodil	vysvobodit	k5eAaPmAgMnS	vysvobodit
z	z	k7c2	z
nechtěného	chtěný	k2eNgInSc2d1	nechtěný
sňatku	sňatek	k1gInSc2	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
zajmou	zajmout	k5eAaPmIp3nP	zajmout
a	a	k8xC	a
žalují	žalovat	k5eAaImIp3nP	žalovat
králi	král	k1gMnSc3	král
Vilémovi	Vilém	k1gMnSc3	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
zradu	zrada	k1gFnSc4	zrada
nechá	nechat	k5eAaPmIp3nS	nechat
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
měli	mít	k5eAaImAgMnP	mít
sestru	sestra	k1gFnSc4	sestra
Lisabettu	Lisabett	k1gInSc2	Lisabett
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
milovala	milovat	k5eAaImAgFnS	milovat
Lorenza	Lorenza	k?	Lorenza
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
bratři	bratr	k1gMnPc1	bratr
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
zabili	zabít	k5eAaPmAgMnP	zabít
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
ostatky	ostatek	k1gInPc4	ostatek
zakopali	zakopat	k5eAaPmAgMnP	zakopat
za	za	k7c7	za
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Lisabetta	Lisabetta	k1gFnSc1	Lisabetta
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nevěděla	vědět	k5eNaImAgFnS	vědět
a	a	k8xC	a
pořád	pořád	k6eAd1	pořád
se	se	k3xPyFc4	se
ptala	ptat	k5eAaImAgFnS	ptat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
milý	milý	k2eAgInSc1d1	milý
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
Lorenza	Lorenza	k?	Lorenza
zjevil	zjevit	k5eAaPmAgInS	zjevit
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Lisabetta	Lisabetta	k1gFnSc1	Lisabetta
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
za	za	k7c4	za
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
vykopala	vykopat	k5eAaPmAgFnS	vykopat
jeho	jeho	k3xOp3gFnSc4	jeho
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
bazalkového	bazalkový	k2eAgInSc2d1	bazalkový
květináče	květináč	k1gInSc2	květináč
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
ji	on	k3xPp3gFnSc4	on
květináč	květináč	k1gInSc4	květináč
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Lisabetta	Lisabetta	k1gFnSc1	Lisabetta
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
trápila	trápit	k5eAaImAgFnS	trápit
a	a	k8xC	a
plakala	plakat	k5eAaImAgFnS	plakat
<g/>
,	,	kIx,	,
až	až	k8xS	až
v	v	k7c6	v
pláči	pláč	k1gInSc6	pláč
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lipari	Lipar	k1gFnSc2	Lipar
žila	žít	k5eAaImAgFnS	žít
krásná	krásný	k2eAgFnSc1d1	krásná
a	a	k8xC	a
bohatá	bohatý	k2eAgFnSc1d1	bohatá
Gostanza	Gostanza	k1gFnSc1	Gostanza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
do	do	k7c2	do
mladého	mladý	k2eAgMnSc2d1	mladý
Martuccia	Martuccius	k1gMnSc2	Martuccius
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
Martuccio	Martuccio	k6eAd1	Martuccio
chudý	chudý	k2eAgMnSc1d1	chudý
<g/>
,	,	kIx,	,
Gostanzin	Gostanzin	k2eAgMnSc1d1	Gostanzin
otec	otec	k1gMnSc1	otec
nechtěl	chtít	k5eNaImAgMnS	chtít
dovolit	dovolit	k5eAaPmF	dovolit
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Martuccio	Martuccio	k6eAd1	Martuccio
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
bohatým	bohatý	k2eAgMnSc7d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pirátem	pirát	k1gMnSc7	pirát
a	a	k8xC	a
přepadal	přepadat	k5eAaImAgMnS	přepadat
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
byl	být	k5eAaImAgInS	být
poražen	poražen	k2eAgInSc1d1	poražen
a	a	k8xC	a
vsadili	vsadit	k5eAaPmAgMnP	vsadit
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
žaláře	žalář	k1gInSc2	žalář
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
Gostanza	Gostanza	k1gFnSc1	Gostanza
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
<g/>
,	,	kIx,	,
myslela	myslet	k5eAaImAgFnS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Martuccio	Martuccio	k6eAd1	Martuccio
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
Chtěla	chtít	k5eAaImAgFnS	chtít
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
se	se	k3xPyFc4	se
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
loďce	loďka	k1gFnSc6	loďka
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
ale	ale	k8xC	ale
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
,	,	kIx,	,
dojela	dojet	k5eAaPmAgFnS	dojet
až	až	k9	až
do	do	k7c2	do
města	město	k1gNnSc2	město
Susa	Sus	k1gInSc2	Sus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
ujala	ujmout	k5eAaPmAgFnS	ujmout
žena	žena	k1gFnSc1	žena
Carapresa	Carapresa	k1gFnSc1	Carapresa
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
milenci	milenec	k1gMnPc1	milenec
šťastně	šťastně	k6eAd1	šťastně
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtic	šlechtic	k1gMnSc1	šlechtic
Frederigo	Frederigo	k1gMnSc1	Frederigo
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
urozené	urozený	k2eAgFnSc2d1	urozená
paní	paní	k1gFnSc2	paní
mony	mona	k1gFnSc2	mona
Giovanny	Giovanen	k2eAgFnPc4d1	Giovanna
<g/>
,	,	kIx,	,
počestné	počestný	k2eAgFnPc4d1	počestná
a	a	k8xC	a
krásné	krásný	k2eAgFnPc4d1	krásná
dámy	dáma	k1gFnPc4	dáma
<g/>
.	.	kIx.	.
</s>
<s>
Utrácel	utrácet	k5eAaImAgMnS	utrácet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
pořádal	pořádat	k5eAaImAgMnS	pořádat
turnaje	turnaj	k1gInPc4	turnaj
a	a	k8xC	a
hostiny	hostina	k1gFnPc4	hostina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
nic	nic	k6eAd1	nic
platné	platný	k2eAgNnSc1d1	platné
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
stylem	styl	k1gInSc7	styl
života	život	k1gInSc2	život
brzy	brzy	k6eAd1	brzy
zchudnul	zchudnout	k5eAaPmAgMnS	zchudnout
a	a	k8xC	a
zbyl	zbýt	k5eAaPmAgMnS	zbýt
mu	on	k3xPp3gMnSc3	on
jen	jen	k6eAd1	jen
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
sokol	sokol	k1gMnSc1	sokol
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
jela	jet	k5eAaImAgFnS	jet
mona	mon	k2eAgFnSc1d1	mona
Giovanna	Giovanna	k1gFnSc1	Giovanna
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synkem	synek	k1gMnSc7	synek
na	na	k7c4	na
statek	statek	k1gInSc4	statek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
poblíž	poblíž	k7c2	poblíž
Frederigova	Frederigův	k2eAgInSc2d1	Frederigův
statku	statek	k1gInSc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Synek	Synek	k1gMnSc1	Synek
se	se	k3xPyFc4	se
s	s	k7c7	s
Frederigem	Frederigo	k1gNnSc7	Frederigo
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
a	a	k8xC	a
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
si	se	k3xPyFc3	se
jeho	on	k3xPp3gMnSc4	on
sokola	sokol	k1gMnSc4	sokol
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
,	,	kIx,	,
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
si	se	k3xPyFc3	se
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
právě	právě	k9	právě
toho	ten	k3xDgMnSc4	ten
sokola	sokol	k1gMnSc4	sokol
<g/>
,	,	kIx,	,
prý	prý	k9	prý
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
uzdraví	uzdravit	k5eAaPmIp3nS	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Mona	Mon	k2eAgFnSc1d1	Mona
Giovanna	Giovanna	k1gFnSc1	Giovanna
se	se	k3xPyFc4	se
se	s	k7c7	s
studem	stud	k1gInSc7	stud
vydala	vydat	k5eAaPmAgFnS	vydat
za	za	k7c7	za
Frederigem	Frederig	k1gInSc7	Frederig
<g/>
.	.	kIx.	.
</s>
<s>
Přijela	přijet	k5eAaPmAgFnS	přijet
na	na	k7c4	na
oběd	oběd	k1gInSc4	oběd
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
Frederigo	Frederigo	k1gMnSc1	Frederigo
neměl	mít	k5eNaImAgMnS	mít
monu	monu	k6eAd1	monu
čím	co	k3yRnSc7	co
pohostit	pohostit	k5eAaPmF	pohostit
<g/>
,	,	kIx,	,
zabil	zabít	k5eAaPmAgMnS	zabít
svého	svůj	k3xOyFgMnSc4	svůj
sokola	sokol	k1gMnSc4	sokol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jídle	jídlo	k1gNnSc6	jídlo
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
mona	mona	k1gFnSc1	mona
svěřila	svěřit	k5eAaPmAgFnS	svěřit
s	s	k7c7	s
důvodem	důvod	k1gInSc7	důvod
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
Frederigo	Frederigo	k1gMnSc1	Frederigo
rozplakal	rozplakat	k5eAaPmAgMnS	rozplakat
a	a	k8xC	a
pověděl	povědět	k5eAaPmAgMnS	povědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
se	s	k7c7	s
sokolem	sokol	k1gMnSc7	sokol
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Synek	Synek	k1gMnSc1	Synek
mony	mona	k1gMnSc2	mona
Giovanny	Giovanna	k1gFnSc2	Giovanna
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
mona	mona	k6eAd1	mona
dlouho	dlouho	k6eAd1	dlouho
truchlila	truchlit	k5eAaImAgFnS	truchlit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
pár	pár	k4xCyI	pár
týdnů	týden	k1gInPc2	týden
si	se	k3xPyFc3	se
Frederiga	Frederiga	k1gFnSc1	Frederiga
vzala	vzít	k5eAaPmAgFnS	vzít
za	za	k7c4	za
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
žila	žít	k5eAaImAgFnS	žít
krásná	krásný	k2eAgFnSc1d1	krásná
paní	paní	k1gFnSc1	paní
Oretta	Oretta	k1gFnSc1	Oretta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vyjít	vyjít	k5eAaPmF	vyjít
si	se	k3xPyFc3	se
s	s	k7c7	s
hosty	host	k1gMnPc7	host
na	na	k7c4	na
procházku	procházka	k1gFnSc4	procházka
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jí	on	k3xPp3gFnSc3	on
šlechtic	šlechtic	k1gMnSc1	šlechtic
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
sveze	svézt	k5eAaPmIp3nS	svézt
na	na	k7c6	na
svém	svůj	k3xOyFgMnSc6	svůj
koni	kůň	k1gMnSc6	kůň
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
jí	on	k3xPp3gFnSc3	on
bude	být	k5eAaImBp3nS	být
vyprávět	vyprávět	k5eAaImF	vyprávět
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ještě	ještě	k6eAd1	ještě
nikdy	nikdy	k6eAd1	nikdy
neslyšela	slyšet	k5eNaImAgFnS	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
krásný	krásný	k2eAgInSc1d1	krásný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šlechtic	šlechtic	k1gMnSc1	šlechtic
neuměl	umět	k5eNaImAgMnS	umět
vůbec	vůbec	k9	vůbec
vyprávět	vyprávět	k5eAaImF	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
se	se	k3xPyFc4	se
vymluvila	vymluvit	k5eAaPmAgFnS	vymluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kůň	kůň	k1gMnSc1	kůň
jde	jít	k5eAaImIp3nS	jít
moc	moc	k6eAd1	moc
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
tedy	tedy	k9	tedy
z	z	k7c2	z
koně	kůň	k1gMnSc2	kůň
sundal	sundat	k5eAaPmAgMnS	sundat
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtic	šlechtic	k1gMnSc1	šlechtic
její	její	k3xOp3gFnSc4	její
výmluvu	výmluva	k1gFnSc4	výmluva
prokoukl	prokouknout	k5eAaPmAgMnS	prokouknout
<g/>
,	,	kIx,	,
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
co	co	k8xS	co
tím	ten	k3xDgNnSc7	ten
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
raději	rád	k6eAd2	rád
vyprávět	vyprávět	k5eAaImF	vyprávět
jiný	jiný	k2eAgInSc4d1	jiný
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Bonifác	Bonifác	k1gMnSc1	Bonifác
vyslal	vyslat	k5eAaPmAgMnS	vyslat
messera	messer	k1gMnSc4	messer
Geriho	Geri	k1gMnSc4	Geri
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
vyslance	vyslanec	k1gMnSc2	vyslanec
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyřídili	vyřídit	k5eAaPmAgMnP	vyřídit
nějaké	nějaký	k3yIgFnPc4	nějaký
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc4	každý
ráno	ráno	k6eAd1	ráno
chodil	chodit	k5eAaImAgMnS	chodit
Geri	Geri	k1gNnSc4	Geri
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
družinou	družina	k1gFnSc7	družina
kolem	kolem	k7c2	kolem
pekaře	pekař	k1gMnSc2	pekař
Cistiho	Cisti	k1gMnSc2	Cisti
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
měl	mít	k5eAaImAgInS	mít
Cisti	Cisť	k1gFnSc3	Cisť
skvělou	skvělý	k2eAgFnSc4d1	skvělá
sbírku	sbírka	k1gFnSc4	sbírka
vín	víno	k1gNnPc2	víno
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
nabídnout	nabídnout	k5eAaPmF	nabídnout
i	i	k9	i
Gerimu	Gerima	k1gFnSc4	Gerima
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
si	se	k3xPyFc3	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
mu	on	k3xPp3gMnSc3	on
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
on	on	k3xPp3gMnSc1	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
víno	víno	k1gNnSc4	víno
Gerimu	Gerim	k1gInSc2	Gerim
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
každé	každý	k3xTgNnSc4	každý
ráno	ráno	k6eAd1	ráno
usedl	usednout	k5eAaPmAgMnS	usednout
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
a	a	k8xC	a
popíjel	popíjet	k5eAaImAgMnS	popíjet
lahodné	lahodný	k2eAgNnSc4d1	lahodné
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
si	se	k3xPyFc3	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
Geri	Geri	k1gNnSc3	Geri
usedl	usednout	k5eAaPmAgMnS	usednout
a	a	k8xC	a
popíjel	popíjet	k5eAaImAgMnS	popíjet
skvělé	skvělý	k2eAgNnSc4d1	skvělé
víno	víno	k1gNnSc4	víno
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vyslanci	vyslanec	k1gMnPc7	vyslanec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Geri	Ger	k1gMnPc7	Ger
vyřídil	vyřídit	k5eAaPmAgInS	vyřídit
všechny	všechen	k3xTgFnPc4	všechen
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
pozval	pozvat	k5eAaPmAgMnS	pozvat
pekaře	pekař	k1gMnPc4	pekař
na	na	k7c4	na
hostinu	hostina	k1gFnSc4	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Pekař	Pekař	k1gMnSc1	Pekař
zpočátku	zpočátku	k6eAd1	zpočátku
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
pozvání	pozvání	k1gNnSc4	pozvání
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Chichibio	Chichibio	k6eAd1	Chichibio
byl	být	k5eAaImAgInS	být
kuchařem	kuchař	k1gMnSc7	kuchař
urozeného	urozený	k2eAgNnSc2d1	urozené
Currada	Currada	k1gFnSc1	Currada
<g/>
.	.	kIx.	.
</s>
<s>
Currado	Currada	k1gFnSc5	Currada
jednou	jeden	k4xCgFnSc7	jeden
ulovil	ulovit	k5eAaPmAgMnS	ulovit
jeřába	jeřáb	k1gMnSc2	jeřáb
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Chichibiovi	Chichibius	k1gMnSc3	Chichibius
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
připravil	připravit	k5eAaPmAgInS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
jeřáb	jeřáb	k1gInSc1	jeřáb
hotov	hotov	k2eAgInSc1d1	hotov
<g/>
,	,	kIx,	,
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
kuchyně	kuchyně	k1gFnSc2	kuchyně
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Chichibio	Chichibio	k6eAd1	Chichibio
miloval	milovat	k5eAaImAgInS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Přemluvila	přemluvit	k5eAaPmAgFnS	přemluvit
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohla	moct	k5eAaImAgFnS	moct
vzít	vzít	k5eAaPmF	vzít
jedno	jeden	k4xCgNnSc4	jeden
stehno	stehno	k1gNnSc4	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Currado	Currada	k1gFnSc5	Currada
se	se	k3xPyFc4	se
podivil	podivit	k5eAaPmAgMnS	podivit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
přinesli	přinést	k5eAaPmAgMnP	přinést
jeřába	jeřáb	k1gMnSc4	jeřáb
jen	jen	k9	jen
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
stehnem	stehno	k1gNnSc7	stehno
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Chichibio	Chichibio	k1gMnSc1	Chichibio
mu	on	k3xPp3gMnSc3	on
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
jeřábi	jeřáb	k1gMnPc1	jeřáb
jen	jen	k9	jen
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
nohou	noha	k1gFnSc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
šli	jít	k5eAaImAgMnP	jít
spolu	spolu	k6eAd1	spolu
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
a	a	k8xC	a
Chichibio	Chichibio	k6eAd1	Chichibio
ukázal	ukázat	k5eAaPmAgMnS	ukázat
Curradovi	Currada	k1gMnSc3	Currada
spící	spící	k2eAgFnSc4d1	spící
jeřáby	jeřáb	k1gMnPc4	jeřáb
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Currado	Currada	k1gFnSc5	Currada
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mýlil	mýlit	k5eAaImAgMnS	mýlit
<g/>
.	.	kIx.	.
</s>
<s>
Zvolal	zvolat	k5eAaPmAgMnS	zvolat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jeřábi	jeřáb	k1gMnPc1	jeřáb
se	se	k3xPyFc4	se
vyplašili	vyplašit	k5eAaPmAgMnP	vyplašit
a	a	k8xC	a
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
použili	použít	k5eAaPmAgMnP	použít
obě	dva	k4xCgFnPc4	dva
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Vyděšený	vyděšený	k2eAgMnSc1d1	vyděšený
Chichibio	Chichibio	k1gMnSc1	Chichibio
se	se	k3xPyFc4	se
vymluvil	vymluvit	k5eAaPmAgMnS	vymluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
včera	včera	k6eAd1	včera
pán	pán	k1gMnSc1	pán
také	také	k9	také
nezvolal	zvolat	k5eNaPmAgMnS	zvolat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
ho	on	k3xPp3gNnSc4	on
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Currada	Currada	k1gFnSc1	Currada
jeho	jeho	k3xOp3gFnSc4	jeho
odpověď	odpověď	k1gFnSc4	odpověď
pobavila	pobavit	k5eAaPmAgFnS	pobavit
a	a	k8xC	a
Chichibiovi	Chichibius	k1gMnSc3	Chichibius
odpustil	odpustit	k5eAaPmAgMnS	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Forese	Forese	k1gFnSc1	Forese
a	a	k8xC	a
Giotto	Giotto	k1gNnSc4	Giotto
se	se	k3xPyFc4	se
potkali	potkat	k5eAaPmAgMnP	potkat
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
a	a	k8xC	a
přepadl	přepadnout	k5eAaPmAgInS	přepadnout
je	on	k3xPp3gMnPc4	on
náhlý	náhlý	k2eAgInSc1d1	náhlý
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
</s>
<s>
Utekli	utéct	k5eAaPmAgMnP	utéct
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
známému	známý	k1gMnSc3	známý
rolníkovi	rolník	k1gMnSc3	rolník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
vypůjčili	vypůjčit	k5eAaPmAgMnP	vypůjčit
suché	suchý	k2eAgNnSc4d1	suché
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otrhané	otrhaný	k2eAgNnSc4d1	otrhané
<g/>
,	,	kIx,	,
šaty	šat	k1gInPc4	šat
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
ušpinili	ušpinit	k5eAaPmAgMnP	ušpinit
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
si	se	k3xPyFc3	se
povídat	povídat	k5eAaImF	povídat
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	k9	kdyby
je	on	k3xPp3gMnPc4	on
někdo	někdo	k3yInSc1	někdo
potkal	potkat	k5eAaPmAgInS	potkat
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
by	by	kYmCp3nS	by
uvěřil	uvěřit	k5eAaPmAgMnS	uvěřit
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vlastně	vlastně	k9	vlastně
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
cestující	cestující	k1gMnPc1	cestující
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
ze	z	k7c2	z
sebe	se	k3xPyFc2	se
dělali	dělat	k5eAaImAgMnP	dělat
legraci	legrace	k1gFnSc4	legrace
<g/>
.	.	kIx.	.
</s>
<s>
Manžel	manžel	k1gMnSc1	manžel
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
přistihl	přistihnout	k5eAaPmAgMnS	přistihnout
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
s	s	k7c7	s
milencem	milenec	k1gMnSc7	milenec
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
za	za	k7c2	za
cizoložství	cizoložství	k1gNnPc2	cizoložství
trestalo	trestat	k5eAaImAgNnS	trestat
upálením	upálení	k1gNnSc7	upálení
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
čekali	čekat	k5eAaImAgMnP	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
manželka	manželka	k1gFnSc1	manželka
uteče	utéct	k5eAaPmIp3nS	utéct
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
před	před	k7c4	před
soud	soud	k1gInSc4	soud
předstoupila	předstoupit	k5eAaPmAgFnS	předstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
nic	nic	k3yNnSc4	nic
nepopírala	popírat	k5eNaImAgFnS	popírat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
namítala	namítat	k5eAaImAgFnS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákon	zákon	k1gInSc1	zákon
platí	platit	k5eAaImIp3nS	platit
jen	jen	k9	jen
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
žena	žena	k1gFnSc1	žena
plnila	plnit	k5eAaImAgFnS	plnit
manželské	manželský	k2eAgFnPc4d1	manželská
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
odvětila	odvětit	k5eAaPmAgFnS	odvětit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
se	se	k3xPyFc4	se
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
rozkoše	rozkoš	k1gFnSc2	rozkoš
rozdělit	rozdělit	k5eAaPmF	rozdělit
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ji	on	k3xPp3gFnSc4	on
miluje	milovat	k5eAaImIp3nS	milovat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gInSc4	její
popud	popud	k1gInSc4	popud
zákon	zákon	k1gInSc4	zákon
změnili	změnit	k5eAaPmAgMnP	změnit
a	a	k8xC	a
upálovány	upálován	k2eAgFnPc1d1	upálován
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
ty	ten	k3xDgFnPc1	ten
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
provozovaly	provozovat	k5eAaImAgFnP	provozovat
cizoložství	cizoložství	k1gNnSc4	cizoložství
za	za	k7c4	za
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Lodovico	Lodovico	k1gMnSc1	Lodovico
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Beatrice	Beatrice	k1gFnSc2	Beatrice
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
sluhou	sluha	k1gMnSc7	sluha
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
Lodovico	Lodovico	k6eAd1	Lodovico
svěřil	svěřit	k5eAaPmAgInS	svěřit
Beatrici	Beatrice	k1gFnSc4	Beatrice
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
city	cit	k1gInPc7	cit
<g/>
.	.	kIx.	.
</s>
<s>
Smluví	smluvit	k5eAaPmIp3nS	smluvit
si	se	k3xPyFc3	se
schůzku	schůzka	k1gFnSc4	schůzka
v	v	k7c6	v
půlnoci	půlnoc	k1gFnSc6	půlnoc
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Vychytralá	vychytralý	k2eAgFnSc1d1	vychytralá
ženská	ženská	k1gFnSc1	ženská
ale	ale	k8xC	ale
svému	svůj	k3xOyFgInSc3	svůj
manželi	manžel	k1gMnPc7	manžel
poví	povědět	k5eAaPmIp3nS	povědět
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
nemravnosti	nemravnost	k1gFnPc4	nemravnost
jí	jíst	k5eAaImIp3nS	jíst
Lodovico	Lodovico	k6eAd1	Lodovico
namluvil	namluvit	k5eAaPmAgMnS	namluvit
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
potrestat	potrestat	k5eAaPmF	potrestat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ho	on	k3xPp3gMnSc4	on
pošle	poslat	k5eAaPmIp3nS	poslat
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
oděvu	oděv	k1gInSc6	oděv
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Lodovicem	Lodovic	k1gMnSc7	Lodovic
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
užijí	užít	k5eAaPmIp3nP	užít
a	a	k8xC	a
hned	hned	k6eAd1	hned
nato	nato	k6eAd1	nato
jde	jít	k5eAaImIp3nS	jít
Lodovico	Lodovico	k6eAd1	Lodovico
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
morálně	morálně	k6eAd1	morálně
"	"	kIx"	"
<g/>
napravil	napravit	k5eAaPmAgInS	napravit
<g/>
"	"	kIx"	"
Beatrici	Beatrice	k1gFnSc4	Beatrice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
pár	pár	k4xCyI	pár
ran	rána	k1gFnPc2	rána
holí	holit	k5eAaImIp3nS	holit
a	a	k8xC	a
dostatečně	dostatečně	k6eAd1	dostatečně
se	se	k3xPyFc4	se
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
sluha	sluha	k1gMnSc1	sluha
je	být	k5eAaImIp3nS	být
věrný	věrný	k2eAgMnSc1d1	věrný
<g/>
.	.	kIx.	.
</s>
<s>
Lodovico	Lodovico	k6eAd1	Lodovico
a	a	k8xC	a
Beatrice	Beatrice	k1gFnSc1	Beatrice
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
potají	potají	k6eAd1	potají
užívali	užívat	k5eAaImAgMnP	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Florenští	Florenský	k2eAgMnPc1d1	Florenský
sudí	sudí	k1gMnPc1	sudí
přivezli	přivézt	k5eAaPmAgMnP	přivézt
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
nového	nový	k2eAgMnSc2d1	nový
soudce	soudce	k1gMnSc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc1	ten
otrhaný	otrhaný	k2eAgMnSc1d1	otrhaný
<g/>
,	,	kIx,	,
ušmudlaný	ušmudlaný	k2eAgMnSc1d1	ušmudlaný
<g/>
,	,	kIx,	,
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
hňup	hňup	k1gMnSc1	hňup
-	-	kIx~	-
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ušetřilo	ušetřit	k5eAaPmAgNnS	ušetřit
<g/>
.	.	kIx.	.
</s>
<s>
Všem	všecek	k3xTgMnPc3	všecek
byl	být	k5eAaImAgInS	být
jen	jen	k6eAd1	jen
pro	pro	k7c4	pro
smích	smích	k1gInSc4	smích
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
se	s	k7c7	s
svýma	svůj	k3xOyFgFnPc7	svůj
dvěma	dva	k4xCgFnPc7	dva
kumpány	kumpány	k?	kumpány
Ribem	Rib	k1gInSc7	Rib
a	a	k8xC	a
Matteuzzem	Matteuzz	k1gInSc7	Matteuzz
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
plán	plán	k1gInSc4	plán
-	-	kIx~	-
stáhnout	stáhnout	k5eAaPmF	stáhnout
mu	on	k3xPp3gInSc3	on
plandající	plandající	k2eAgFnPc4d1	plandající
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
.	.	kIx.	.
</s>
<s>
Ribi	Ribi	k1gNnSc1	Ribi
a	a	k8xC	a
Maso	maso	k1gNnSc1	maso
sehráli	sehrát	k5eAaPmAgMnP	sehrát
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
upoutali	upoutat	k5eAaPmAgMnP	upoutat
soudcovu	soudcův	k2eAgFnSc4d1	soudcova
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Hádali	hádat	k5eAaImAgMnP	hádat
se	se	k3xPyFc4	se
o	o	k7c4	o
boty	bota	k1gFnPc4	bota
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
chtěl	chtít	k5eAaImAgMnS	chtít
soudce	soudce	k1gMnPc4	soudce
uklidnit	uklidnit	k5eAaPmF	uklidnit
<g/>
,	,	kIx,	,
vstal	vstát	k5eAaPmAgMnS	vstát
<g/>
,	,	kIx,	,
stáhnul	stáhnout	k5eAaPmAgMnS	stáhnout
mu	on	k3xPp3gMnSc3	on
Matteuzzo	Matteuzza	k1gFnSc5	Matteuzza
kalhoty	kalhoty	k1gFnPc4	kalhoty
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
vypařili	vypařit	k5eAaPmAgMnP	vypařit
<g/>
.	.	kIx.	.
</s>
<s>
Ostuda	ostuda	k1gFnSc1	ostuda
pro	pro	k7c4	pro
florentské	florentský	k2eAgFnPc4d1	florentská
sudí	sudí	k1gFnPc4	sudí
a	a	k8xC	a
trapas	trapas	k1gInSc4	trapas
pro	pro	k7c4	pro
hloupoučkého	hloupoučký	k2eAgMnSc4d1	hloupoučký
soudce	soudce	k1gMnSc4	soudce
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
celé	celý	k2eAgInPc4d1	celý
to	ten	k3xDgNnSc1	ten
divadlo	divadlo	k1gNnSc1	divadlo
sotva	sotva	k6eAd1	sotva
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Talanovi	Talan	k1gMnSc3	Talan
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
sen	sen	k1gInSc1	sen
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
krásné	krásný	k2eAgNnSc1d1	krásné
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nepříjemné	příjemný	k2eNgFnSc3d1	nepříjemná
<g/>
,	,	kIx,	,
zlé	zlý	k2eAgFnSc3d1	zlá
<g/>
,	,	kIx,	,
náladové	náladový	k2eAgFnSc3d1	náladová
a	a	k8xC	a
umíněné	umíněný	k2eAgFnSc3d1	umíněná
ženě	žena	k1gFnSc3	žena
Markétě	Markéta	k1gFnSc3	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
snu	sen	k1gInSc6	sen
jde	jít	k5eAaImIp3nS	jít
Markétě	Markéta	k1gFnSc3	Markéta
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
zakousl	zakousnout	k5eAaPmAgMnS	zakousnout
vlk	vlk	k1gMnSc1	vlk
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
ráno	ráno	k6eAd1	ráno
jí	on	k3xPp3gFnSc3	on
o	o	k7c6	o
snu	sen	k1gInSc6	sen
pověděl	povědět	k5eAaPmAgMnS	povědět
a	a	k8xC	a
varoval	varovat	k5eAaImAgMnS	varovat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
nevěřila	věřit	k5eNaImAgFnS	věřit
a	a	k8xC	a
do	do	k7c2	do
lesa	les	k1gInSc2	les
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ji	on	k3xPp3gFnSc4	on
napadnul	napadnout	k5eAaPmAgMnS	napadnout
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
zachráněna	zachránit	k5eAaPmNgFnS	zachránit
pastýři	pastýř	k1gMnPc7	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
nevyvázla	vyváznout	k5eNaPmAgFnS	vyváznout
bez	bez	k7c2	bez
úhony	úhona	k1gFnSc2	úhona
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
zohavený	zohavený	k2eAgInSc4d1	zohavený
celý	celý	k2eAgInSc4d1	celý
obličej	obličej	k1gInSc4	obličej
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
styděla	stydět	k5eAaImAgFnS	stydět
se	se	k3xPyFc4	se
chodit	chodit	k5eAaImF	chodit
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
chybu	chyba	k1gFnSc4	chyba
si	se	k3xPyFc3	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
vyčítala	vyčítat	k5eAaImAgFnS	vyčítat
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
z	z	k7c2	z
Cluny	Cluna	k1gFnSc2	Cluna
je	být	k5eAaImIp3nS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
Ghinem	Ghin	k1gMnSc7	Ghin
<g/>
,	,	kIx,	,
nepřítelem	nepřítel	k1gMnSc7	nepřítel
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
měl	mít	k5eAaImAgMnS	mít
nemocný	mocný	k2eNgInSc4d1	mocný
žaludek	žaludek	k1gInSc4	žaludek
<g/>
,	,	kIx,	,
Ghin	Ghin	k1gMnSc1	Ghin
ho	on	k3xPp3gMnSc4	on
vyléčil	vyléčit	k5eAaPmAgMnS	vyléčit
a	a	k8xC	a
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgInS	mít
předsudky	předsudek	k1gInPc4	předsudek
vůči	vůči	k7c3	vůči
mnichům	mnich	k1gMnPc3	mnich
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
lakomí	lakomý	k2eAgMnPc1d1	lakomý
a	a	k8xC	a
chamtiví	chamtivý	k2eAgMnPc1d1	chamtivý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
mu	on	k3xPp3gMnSc3	on
nedával	dávat	k5eNaImAgMnS	dávat
najíst	najíst	k5eAaPmF	najíst
a	a	k8xC	a
opat	opat	k1gMnSc1	opat
byl	být	k5eAaImAgMnS	být
stále	stále	k6eAd1	stále
skromný	skromný	k2eAgMnSc1d1	skromný
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
opat	opat	k1gMnSc1	opat
Ghina	Ghino	k1gNnSc2	Ghino
získal	získat	k5eAaPmAgMnS	získat
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
usmířil	usmířit	k5eAaPmAgMnS	usmířit
Ghina	Ghin	k1gMnSc4	Ghin
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
ho	on	k3xPp3gInSc4	on
představeným	představený	k1gMnPc3	představený
špitálu	špitál	k1gInSc2	špitál
<g/>
.	.	kIx.	.
</s>
