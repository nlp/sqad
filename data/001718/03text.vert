<s>
Are	ar	k1gInSc5	ar
You	You	k1gFnPc4	You
Experienced	Experienced	k1gInSc1	Experienced
je	být	k5eAaImIp3nS	být
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
prvním	první	k4xOgInPc3	první
LP	LP	kA	LP
nové	nový	k2eAgFnSc2d1	nová
společnosti	společnost	k1gFnSc2	společnost
Track	Track	k1gMnSc1	Track
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
zvýrazněno	zvýraznit	k5eAaPmNgNnS	zvýraznit
Hendrixovými	Hendrixový	k2eAgInPc7d1	Hendrixový
základy	základ	k1gInPc7	základ
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
psychedelií	psychedelie	k1gFnSc7	psychedelie
a	a	k8xC	a
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Hendrixe	Hendrixe	k1gFnSc1	Hendrixe
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
skupinu	skupina	k1gFnSc4	skupina
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
jako	jako	k9	jako
nové	nový	k2eAgFnSc2d1	nová
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejúžasnějších	úžasný	k2eAgInPc2d3	nejúžasnější
debutů	debut	k1gInPc2	debut
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
zde	zde	k6eAd1	zde
spojil	spojit	k5eAaPmAgInS	spojit
řadu	řada	k1gFnSc4	řada
různých	různý	k2eAgInPc2d1	různý
vlivů	vliv	k1gInPc2	vliv
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
moderní	moderní	k2eAgInSc1d1	moderní
psychedelický	psychedelický	k2eAgInSc1d1	psychedelický
zvuk	zvuk	k1gInSc1	zvuk
vycházející	vycházející	k2eAgInSc1d1	vycházející
z	z	k7c2	z
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
popu	pop	k1gInSc2	pop
a	a	k8xC	a
soulu	soul	k1gInSc2	soul
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
skvěle	skvěle	k6eAd1	skvěle
skloubit	skloubit	k5eAaPmF	skloubit
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
hudebních	hudební	k2eAgFnPc2d1	hudební
tradic	tradice	k1gFnPc2	tradice
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
Atlantiku	Atlantik	k1gInSc2	Atlantik
-	-	kIx~	-
cit	cit	k1gInSc1	cit
pro	pro	k7c4	pro
soul	soul	k1gInSc4	soul
<g/>
,	,	kIx,	,
blues	blues	k1gNnSc4	blues
a	a	k8xC	a
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
kytarovou	kytarový	k2eAgFnSc7d1	kytarová
školou	škola	k1gFnSc7	škola
založenou	založený	k2eAgFnSc7d1	založená
takovými	takový	k3xDgNnPc7	takový
inovátory	inovátor	k1gMnPc4	inovátor
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Jeff	Jeff	k1gMnSc1	Jeff
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
Pete	Pete	k1gFnSc1	Pete
Townshend	Townshend	k1gMnSc1	Townshend
nebo	nebo	k8xC	nebo
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
-	-	kIx~	-
a	a	k8xC	a
povýšil	povýšit	k5eAaPmAgMnS	povýšit
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Posluchače	posluchač	k1gMnPc4	posluchač
samozřejmě	samozřejmě	k6eAd1	samozřejmě
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
uchvátí	uchvátit	k5eAaPmIp3nS	uchvátit
jeho	jeho	k3xOp3gFnSc1	jeho
kytarová	kytarový	k2eAgFnSc1d1	kytarová
hra	hra	k1gFnSc1	hra
plná	plný	k2eAgFnSc1d1	plná
zpětných	zpětný	k2eAgFnPc2d1	zpětná
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
elektrizujících	elektrizující	k2eAgInPc2d1	elektrizující
riffů	riff	k1gInPc2	riff
i	i	k8xC	i
průzračných	průzračný	k2eAgFnPc2d1	průzračná
melodií	melodie	k1gFnPc2	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k9	co
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
vybroušený	vybroušený	k2eAgInSc4d1	vybroušený
klenot	klenot	k1gInSc4	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
máme	mít	k5eAaImIp1nP	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
psychedelické	psychedelický	k2eAgInPc4d1	psychedelický
výlety	výlet	k1gInPc4	výlet
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
Foxey	Foxe	k2eAgFnPc1d1	Foxe
Lady	Lada	k1gFnPc1	Lada
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Manic	Manice	k1gFnPc2	Manice
Depression	Depression	k1gInSc1	Depression
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
instrumentální	instrumentální	k2eAgInSc1d1	instrumentální
jam	jam	k1gInSc1	jam
"	"	kIx"	"
<g/>
Third	Third	k1gInSc1	Third
Stone	ston	k1gInSc5	ston
From	Fro	k1gNnSc7	Fro
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
bluesovou	bluesový	k2eAgFnSc4d1	bluesová
záležitost	záležitost	k1gFnSc4	záležitost
"	"	kIx"	"
<g/>
Red	Red	k1gFnSc4	Red
House	house	k1gNnSc1	house
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
jeho	on	k3xPp3gInSc4	on
obrovský	obrovský	k2eAgInSc4d1	obrovský
hudební	hudební	k2eAgInSc4d1	hudební
<g/>
,	,	kIx,	,
skladatelský	skladatelský	k2eAgInSc4d1	skladatelský
i	i	k8xC	i
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Chybou	Chyba	k1gMnSc7	Chyba
by	by	kYmCp3nS	by
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
připisovat	připisovat	k5eAaImF	připisovat
veškeré	veškerý	k3xTgFnPc4	veškerý
zásluhy	zásluha	k1gFnPc4	zásluha
pouze	pouze	k6eAd1	pouze
Hendrixovi	Hendrixa	k1gMnSc3	Hendrixa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
mimořádné	mimořádný	k2eAgNnSc1d1	mimořádné
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
Hendrixově	Hendrixově	k1gFnSc4	Hendrixově
neoddiskutovatelnému	oddiskutovatelný	k2eNgInSc3d1	neoddiskutovatelný
talentu	talent	k1gInSc3	talent
přidala	přidat	k5eAaPmAgFnS	přidat
vysoce	vysoce	k6eAd1	vysoce
výkonná	výkonný	k2eAgFnSc1d1	výkonná
rytmická	rytmický	k2eAgFnSc1d1	rytmická
sekce	sekce	k1gFnSc1	sekce
Mitche	Mitch	k1gMnSc2	Mitch
Mitchella	Mitchell	k1gMnSc2	Mitchell
a	a	k8xC	a
Noela	Noel	k1gMnSc2	Noel
Reddinga	Redding	k1gMnSc2	Redding
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
cit	cit	k1gInSc4	cit
pro	pro	k7c4	pro
hudební	hudební	k2eAgInPc4d1	hudební
nápady	nápad	k1gInPc4	nápad
svého	svůj	k1gMnSc2	svůj
kapelníka	kapelník	k1gMnSc2	kapelník
a	a	k8xC	a
dokázali	dokázat	k5eAaPmAgMnP	dokázat
je	on	k3xPp3gMnPc4	on
skvěle	skvěle	k6eAd1	skvěle
doplnit	doplnit	k5eAaPmF	doplnit
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nejlepšímu	dobrý	k2eAgMnSc3d3	nejlepší
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
kdy	kdy	k6eAd1	kdy
Hendrix	Hendrix	k1gInSc4	Hendrix
nahrál	nahrát	k5eAaPmAgMnS	nahrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
debut	debut	k1gInSc1	debut
nepodařilo	podařit	k5eNaPmAgNnS	podařit
nikdy	nikdy	k6eAd1	nikdy
překonat	překonat	k5eAaPmF	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Are	ar	k1gInSc5	ar
You	You	k1gFnPc4	You
Experienced	Experienced	k1gInSc1	Experienced
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
citováno	citován	k2eAgNnSc1d1	citováno
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
v	v	k7c6	v
rockové	rockový	k2eAgFnSc6d1	rocková
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
VH1	VH1	k1gFnSc2	VH1
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
umístila	umístit	k5eAaPmAgFnS	umístit
jako	jako	k9	jako
páté	pátý	k4xOgNnSc4	pátý
největší	veliký	k2eAgNnSc4d3	veliký
album	album	k1gNnSc4	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
15	[number]	k4	15
<g/>
.	.	kIx.	.
příčky	příčka	k1gFnSc2	příčka
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
500	[number]	k4	500
greatest	greatest	k5eAaPmF	greatest
albums	albums	k1gInSc4	albums
of	of	k?	of
all	all	k?	all
time	timat	k5eAaPmIp3nS	timat
od	od	k7c2	od
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
1001	[number]	k4	1001
Albums	Albumsa	k1gFnPc2	Albumsa
You	You	k1gMnSc1	You
Must	Must	k1gMnSc1	Must
Hear	Hear	k1gMnSc1	Hear
Before	Befor	k1gMnSc5	Befor
You	You	k1gMnSc5	You
Die	Die	k1gMnSc5	Die
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
napsal	napsat	k5eAaPmAgInS	napsat
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
uvedených	uvedený	k2eAgFnPc2d1	uvedená
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
jako	jako	k9	jako
mono	mono	k2eAgFnSc2d1	mono
a	a	k8xC	a
"	"	kIx"	"
<g/>
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
stereo	stereo	k1gNnSc1	stereo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
falešné	falešný	k2eAgNnSc1d1	falešné
stereo	stereo	k1gNnSc1	stereo
německého	německý	k2eAgInSc2d1	německý
Polydoru	Polydor	k1gInSc2	Polydor
z	z	k7c2	z
mono	mono	k2eAgInSc2d1	mono
efektu	efekt	k1gInSc2	efekt
<g/>
)	)	kIx)	)
Strana	strana	k1gFnSc1	strana
1	[number]	k4	1
"	"	kIx"	"
<g/>
Foxy	fox	k1gInPc1	fox
Lady	lady	k1gFnSc1	lady
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
"	"	kIx"	"
<g/>
Manic	Manice	k1gFnPc2	Manice
Depression	Depression	k1gInSc1	Depression
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Red	Red	k1gMnPc1	Red
House	house	k1gNnSc1	house
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
53	[number]	k4	53
(	(	kIx(	(
<g/>
mono	mono	k6eAd1	mono
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Can	Can	k1gMnSc1	Can
You	You	k1gMnSc1	You
See	See	k1gMnSc1	See
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
or	or	k?	or
Confusion	Confusion	k1gInSc4	Confusion
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
"	"	kIx"	"
<g/>
I	i	k8xC	i
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
t	t	k?	t
Live	Live	k1gFnSc1	Live
Today	Todaa	k1gFnSc2	Todaa
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
58	[number]	k4	58
Strana	strana	k1gFnSc1	strana
2	[number]	k4	2
"	"	kIx"	"
<g/>
May	May	k1gMnSc1	May
This	Thisa	k1gFnPc2	Thisa
Be	Be	k1gMnSc1	Be
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
"	"	kIx"	"
<g/>
Fire	Fir	k1gInSc2	Fir
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
"	"	kIx"	"
<g/>
3	[number]	k4	3
<g/>
rd	rd	k?	rd
Stone	ston	k1gInSc5	ston
from	fro	k1gNnSc7	fro
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
"	"	kIx"	"
<g/>
Remember	Remember	k1gInSc1	Remember
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
53	[number]	k4	53
"	"	kIx"	"
<g/>
Are	ar	k1gInSc5	ar
You	You	k1gFnPc3	You
Experienced	Experienced	k1gInSc4	Experienced
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
Mono	mono	k6eAd1	mono
LP	LP	kA	LP
byla	být	k5eAaImAgFnS	být
zmixována	zmixovat	k5eAaImNgFnS	zmixovat
původně	původně	k6eAd1	původně
<g/>
,	,	kIx,	,
stereo	stereo	k2eAgInSc4d1	stereo
mix	mix	k1gInSc4	mix
mělo	mít	k5eAaImAgNnS	mít
několik	několik	k4yIc1	několik
vydání	vydání	k1gNnPc2	vydání
a	a	k8xC	a
dodatečné	dodatečný	k2eAgInPc1d1	dodatečný
vokály	vokál	k1gInPc1	vokál
"	"	kIx"	"
<g/>
Purple	Purple	k1gFnSc1	Purple
Haze	Haze	k1gFnSc1	Haze
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
a-side	aid	k1gInSc5	a-sid
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Manic	Manice	k1gFnPc2	Manice
Depression	Depression	k1gInSc1	Depression
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
"	"	kIx"	"
<g/>
Hey	Hey	k1gFnSc1	Hey
Joe	Joe	k1gFnSc1	Joe
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Billy	Bill	k1gMnPc4	Bill
Roberts	Robertsa	k1gFnPc2	Robertsa
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
debutový	debutový	k2eAgInSc1d1	debutový
<g />
.	.	kIx.	.
</s>
<s>
singl	singl	k1gInSc1	singl
a-side	aid	k1gInSc5	a-sid
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
or	or	k?	or
Confusion	Confusion	k1gInSc4	Confusion
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
"	"	kIx"	"
<g/>
May	May	k1gMnSc1	May
This	Thisa	k1gFnPc2	Thisa
Be	Be	k1gMnSc1	Be
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
"	"	kIx"	"
<g/>
I	i	k8xC	i
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Live	Liv	k1gMnSc2	Liv
Today	Todaa	k1gMnSc2	Todaa
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
The	The	k1gMnSc1	The
Wind	Wind	k1gMnSc1	Wind
Cries	Cries	k1gMnSc1	Cries
Mary	Mary	k1gFnSc1	Mary
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgInSc1	třetí
singl	singl	k1gInSc1	singl
a-side	aid	k1gInSc5	a-sid
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Fire	Fire	k1gFnSc1	Fire
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
34	[number]	k4	34
"	"	kIx"	"
<g/>
Third	Third	k1gInSc1	Third
Stone	ston	k1gInSc5	ston
from	fro	k1gNnSc7	fro
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
sic	sic	k6eAd1	sic
<g/>
]	]	kIx)	]
-	-	kIx~	-
6	[number]	k4	6
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
<g/>
40	[number]	k4	40
"	"	kIx"	"
<g/>
Foxey	Foxe	k2eAgInPc1d1	Foxe
Lady	lad	k1gInPc1	lad
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
sic	sic	k6eAd1	sic
<g/>
]	]	kIx)	]
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
"	"	kIx"	"
<g/>
Are	ar	k1gInSc5	ar
You	You	k1gFnPc3	You
Experienced	Experienced	k1gInSc4	Experienced
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
"	"	kIx"	"
<g/>
Hey	Hey	k1gFnSc1	Hey
Joe	Joe	k1gFnSc1	Joe
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Billy	Bill	k1gMnPc4	Bill
Roberts	Robertsa	k1gFnPc2	Robertsa
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
34	[number]	k4	34
"	"	kIx"	"
<g/>
Stone	ston	k1gInSc5	ston
Free	Free	k1gNnPc7	Free
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
"	"	kIx"	"
<g/>
Purple	Purple	k1gFnSc1	Purple
Haze	Haze	k1gFnSc1	Haze
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
"	"	kIx"	"
<g/>
51	[number]	k4	51
<g/>
st	st	kA	st
Anniversary	Anniversara	k1gFnSc2	Anniversara
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
18	[number]	k4	18
(	(	kIx(	(
<g/>
mono	mono	k6eAd1	mono
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Wind	Wind	k1gMnSc1	Wind
Cries	Cries	k1gMnSc1	Cries
Mary	Mary	k1gFnSc2	Mary
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
"	"	kIx"	"
<g/>
Highway	Highway	k1gInPc1	Highway
Chile	Chile	k1gNnSc2	Chile
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
(	(	kIx(	(
<g/>
mono	mono	k6eAd1	mono
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Stone	ston	k1gInSc5	ston
Free	Free	k1gNnPc7	Free
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
41	[number]	k4	41
"	"	kIx"	"
<g/>
51	[number]	k4	51
<g/>
st	st	kA	st
Anniversary	Anniversara	k1gFnSc2	Anniversara
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
19	[number]	k4	19
(	(	kIx(	(
<g/>
mono	mono	k6eAd1	mono
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Highway	Highway	k1gInPc1	Highway
Chile	Chile	k1gNnSc2	Chile
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
(	(	kIx(	(
<g/>
mono	mono	k6eAd1	mono
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Can	Can	k1gMnSc1	Can
You	You	k1gMnSc1	You
See	See	k1gMnSc1	See
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
34	[number]	k4	34
"	"	kIx"	"
<g/>
Remember	Remember	k1gInSc1	Remember
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Red	Red	k?	Red
House	house	k1gNnSc1	house
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
(	(	kIx(	(
<g/>
mono	mono	k6eAd1	mono
<g/>
)	)	kIx)	)
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc1	hlas
"	"	kIx"	"
<g/>
Star	Star	kA	Star
Fleet	Fleet	k1gMnSc1	Fleet
<g/>
"	"	kIx"	"
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
3	[number]	k4	3
<g/>
rd	rd	k?	rd
Stone	ston	k1gInSc5	ston
from	fro	k1gNnSc7	fro
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
Noel	Noel	k1gInSc1	Noel
Redding	Redding	k1gInSc1	Redding
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Mitch	Mitch	k1gInSc1	Mitch
Mitchell	Mitchell	k1gInSc4	Mitchell
-	-	kIx~	-
bicí	bicí	k2eAgInSc4d1	bicí
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc4d1	doprovodný
zpěv	zpěv	k1gInSc4	zpěv
Chas	chasa	k1gFnPc2	chasa
Chandler	Chandler	k1gMnSc1	Chandler
-	-	kIx~	-
hlas	hlas	k1gInSc1	hlas
"	"	kIx"	"
<g/>
Scout	Scout	k2eAgInSc1d1	Scout
Ship	Ship	k1gInSc1	Ship
<g/>
"	"	kIx"	"
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
3	[number]	k4	3
<g/>
rd	rd	k?	rd
Stone	ston	k1gInSc5	ston
from	fro	k1gNnSc7	fro
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
Producent	producent	k1gMnSc1	producent
<g/>
:	:	kIx,	:
Chas	chasa	k1gFnPc2	chasa
Chandler	Chandler	k1gInSc1	Chandler
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Eddie	Eddie	k1gFnSc1	Eddie
Kramer	Kramer	k1gInSc1	Kramer
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Siddle	Siddle	k1gFnSc1	Siddle
<g/>
,	,	kIx,	,
Mike	Mike	k1gFnSc1	Mike
Ross	Ross	k1gInSc1	Ross
Fotografie	fotografie	k1gFnSc1	fotografie
<g/>
:	:	kIx,	:
Bruce	Bruce	k1gFnSc1	Bruce
Fleming	Fleming	k1gInSc1	Fleming
(	(	kIx(	(
<g/>
UK	UK	kA	UK
&	&	k?	&
Svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Ferris	Ferris	k1gFnSc2	Ferris
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Návrh	návrh	k1gInSc1	návrh
obalu	obal	k1gInSc2	obal
<g/>
:	:	kIx,	:
Bruce	Bruce	k1gFnSc1	Bruce
Fleming	Fleming	k1gInSc1	Fleming
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
UK	UK	kA	UK
&	&	k?	&
Svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Ferris	Ferris	k1gFnSc2	Ferris
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Poznámky	poznámka	k1gFnSc2	poznámka
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
<g/>
:	:	kIx,	:
Dave	Dav	k1gInSc5	Dav
Marsh	Marsha	k1gFnPc2	Marsha
Remastering	Remastering	k1gInSc1	Remastering
supervisor	supervisor	k1gMnSc1	supervisor
<g/>
:	:	kIx,	:
Janie	Janie	k1gFnSc1	Janie
Hendrix	Hendrix	k1gInSc1	Hendrix
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
McDermott	McDermott	k1gMnSc1	McDermott
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
</s>
<s>
Remastering	Remastering	k1gInSc1	Remastering
<g/>
:	:	kIx,	:
Eddie	Eddie	k1gFnSc1	Eddie
Kramer	Kramer	k1gInSc1	Kramer
<g/>
,	,	kIx,	,
George	George	k1gInSc1	George
Marino	Marina	k1gFnSc5	Marina
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Are	ar	k1gInSc5	ar
You	You	k1gFnPc3	You
Experienced	Experienced	k1gInSc1	Experienced
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
NPR	NPR	kA	NPR
special	special	k1gMnSc1	special
</s>
