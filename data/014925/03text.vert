<s>
Breguet	Breguet	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
</s>
<s>
Br.	Br.	k?
<g/>
1150	#num#	k4
Atlantic	Atlantice	k1gFnPc2
Atlantique	Atlantique	k1gFnSc4
2	#num#	k4
Breguet	Bregueta	k1gFnPc2
Atlantic	Atlantice	k1gFnPc2
německého	německý	k2eAgNnSc2d1
námořnictvaUrčení	námořnictvaUrčení	k1gNnSc2
</s>
<s>
námořní	námořní	k2eAgMnSc1d1
hlídkový	hlídkový	k2eAgMnSc1d1
letoun	letoun	k1gMnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Breguet	Breguet	k5eAaImF,k5eAaBmF,k5eAaPmF
Aviation	Aviation	k1gInSc4
První	první	k4xOgFnSc4
let	léto	k1gNnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1961	#num#	k4
Charakter	charakter	k1gInSc1
</s>
<s>
v	v	k7c6
aktivní	aktivní	k2eAgFnSc6d1
službě	služba	k1gFnSc6
Uživatel	uživatel	k1gMnSc1
</s>
<s>
FrancieItálie	FrancieItálie	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
Výroba	výroba	k1gFnSc1
</s>
<s>
1961	#num#	k4
-	-	kIx~
1989	#num#	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
87	#num#	k4
Atlantic	Atlantice	k1gFnPc2
<g/>
28	#num#	k4
Atlantique	Atlantiqu	k1gInSc2
2	#num#	k4
Cena	cena	k1gFnSc1
za	za	k7c4
kus	kus	k1gInSc4
</s>
<s>
více	hodně	k6eAd2
než	než	k8xS
35	#num#	k4
miliónů	milión	k4xCgInPc2
USD	USD	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Breguet	Breguet	k1gInSc1
Br.	Br.	k1gFnSc2
<g/>
1150	#num#	k4
Atlantic	Atlantice	k1gFnPc2
je	být	k5eAaImIp3nS
průzkumný	průzkumný	k2eAgInSc4d1
letoun	letoun	k1gInSc4
dlouhého	dlouhý	k2eAgInSc2d1
doletu	dolet	k1gInSc2
<g/>
,	,	kIx,
navržený	navržený	k2eAgInSc4d1
pro	pro	k7c4
působení	působení	k1gNnSc4
nad	nad	k7c7
hladinou	hladina	k1gFnSc7
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
několika	několik	k4yIc7
státy	stát	k1gInPc7
v	v	k7c6
rámci	rámec	k1gInSc6
NATO	NATO	kA
k	k	k7c3
průzkumu	průzkum	k1gInSc3
<g/>
,	,	kIx,
námořnímu	námořní	k2eAgNnSc3d1
hlídkování	hlídkování	k1gNnSc3
a	a	k8xC
k	k	k7c3
boji	boj	k1gInSc3
proti	proti	k7c3
ponorkám	ponorka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
schopen	schopen	k2eAgMnSc1d1
nést	nést	k5eAaImF
střely	střela	k1gFnPc4
vzduch-země	vzduch-země	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlantique	Atlantiqu	k1gInSc2
2	#num#	k4
je	být	k5eAaImIp3nS
vylepšená	vylepšený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
vyráběna	vyrábět	k5eAaImNgFnS
pro	pro	k7c4
francouzské	francouzský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
a	a	k8xC
popis	popis	k1gInSc1
</s>
<s>
Detail	detail	k1gInSc1
detektoru	detektor	k1gInSc2
magnetických	magnetický	k2eAgFnPc2d1
anomálií	anomálie	k1gFnPc2
francouzského	francouzský	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Atlantique	Atlantiqu	k1gInSc2
2	#num#	k4
</s>
<s>
Pumovnice	pumovnice	k1gFnSc1
letounu	letoun	k1gInSc2
s	s	k7c7
torpédy	torpédo	k1gNnPc7
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
NATO	NATO	kA
vydalo	vydat	k5eAaPmAgNnS
specifikace	specifikace	k1gFnSc2
požadavku	požadavek	k1gInSc2
na	na	k7c4
námořní	námořní	k2eAgInSc4d1
hlídkový	hlídkový	k2eAgInSc4d1
letoun	letoun	k1gInSc4
dlouhého	dlouhý	k2eAgInSc2d1
doletu	dolet	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
by	by	kYmCp3nS
nahradil	nahradit	k5eAaPmAgInS
letouny	letoun	k1gInPc4
Lockheed	Lockheed	k1gInSc1
Neptune	Neptun	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
vítěz	vítěz	k1gMnSc1
byl	být	k5eAaImAgMnS
koncem	koncem	k7c2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
vybrán	vybrán	k2eAgInSc1d1
návrh	návrh	k1gInSc1
Breguet	Bregueta	k1gFnPc2
Br	br	k0
1150	#num#	k4
a	a	k8xC
multinárodní	multinárodní	k2eAgNnSc4d1
konsorcium	konsorcium	k1gNnSc4
Société	Sociétý	k2eAgFnSc2d1
d	d	k?
<g/>
'	'	kIx"
<g/>
Étude	Étud	k1gInSc5
et	et	k?
de	de	k?
Construction	Construction	k1gInSc1
de	de	k?
Breguet	Breguet	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
(	(	kIx(
<g/>
SECBAT	SECBAT	kA
<g/>
)	)	kIx)
dostalo	dostat	k5eAaPmAgNnS
za	za	k7c4
úkol	úkol	k1gInSc4
vyvinout	vyvinout	k5eAaPmF
a	a	k8xC
postavit	postavit	k5eAaPmF
letoun	letoun	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
vykonal	vykonat	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
let	let	k1gInSc4
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1961	#num#	k4
v	v	k7c6
Toulouse	Toulouse	k1gInSc6
<g/>
,	,	kIx,
následován	následován	k2eAgInSc1d1
druhým	druhý	k4xOgInSc7
prototypem	prototyp	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
poprvé	poprvé	k6eAd1
vzlétnul	vzlétnout	k5eAaPmAgInS
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1962	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgInP
dva	dva	k4xCgInPc1
předsériové	předsériový	k2eAgInPc1d1
letouny	letoun	k1gInPc1
s	s	k7c7
delším	dlouhý	k2eAgInSc7d2
trupem	trup	k1gInSc7
v	v	k7c6
únoru	únor	k1gInSc6
1963	#num#	k4
a	a	k8xC
září	září	k1gNnSc2
1964	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letoun	letoun	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
je	být	k5eAaImIp3nS
dvoumotorový	dvoumotorový	k2eAgInSc1d1
středoplošník	středoplošník	k1gInSc1
s	s	k7c7
trupem	trup	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
příčném	příčný	k2eAgInSc6d1
řezu	řez	k1gInSc6
tvar	tvar	k1gInSc1
číslice	číslice	k1gFnSc2
"	"	kIx"
<g/>
8	#num#	k4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
rozděleným	rozdělená	k1gFnPc3
na	na	k7c4
dva	dva	k4xCgInPc4
prostory	prostor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
trupu	trup	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
přetlaková	přetlakový	k2eAgFnSc1d1
kabina	kabina	k1gFnSc1
pro	pro	k7c4
posádku	posádka	k1gFnSc4
letounu	letoun	k1gInSc2
a	a	k8xC
ve	v	k7c6
spodní	spodní	k2eAgFnSc6d1
části	část	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
9	#num#	k4
m	m	kA
dlouhá	dlouhý	k2eAgFnSc1d1
pumovnice	pumovnice	k1gFnSc1
a	a	k8xC
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
vypouštěcí	vypouštěcí	k2eAgFnSc2d1
trubky	trubka	k1gFnSc2
pro	pro	k7c4
sonarové	sonarový	k2eAgFnPc4d1
bóje	bóje	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Vyhledávací	vyhledávací	k2eAgInSc1d1
radar	radar	k1gInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
pod	pod	k7c7
zatahovatelným	zatahovatelný	k2eAgInSc7d1
krytem	kryt	k1gInSc7
pod	pod	k7c7
trupem	trup	k1gInSc7
letounu	letoun	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
detektor	detektor	k1gInSc1
magnetických	magnetický	k2eAgFnPc2d1
anomálií	anomálie	k1gFnPc2
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
tradičně	tradičně	k6eAd1
v	v	k7c6
prodlouženém	prodloužený	k2eAgNnSc6d1
"	"	kIx"
<g/>
žihadle	žihadlo	k1gNnSc6
<g/>
"	"	kIx"
na	na	k7c6
zádi	záď	k1gFnSc6
letounu	letoun	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
je	být	k5eAaImIp3nS
poháněn	pohánět	k5eAaImNgInS
dvojicí	dvojice	k1gFnSc7
turbovrtulových	turbovrtulový	k2eAgInPc2d1
motorů	motor	k1gInPc2
Rolls-Royce	Rolls-Royce	k1gMnSc2
Tyne	Tyn	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letoun	letoun	k1gMnSc1
Breguet	Breguet	k1gMnSc1
Br.	Br.	k1gMnSc1
<g/>
1150	#num#	k4
Atlantic	Atlantice	k1gFnPc2
se	se	k3xPyFc4
pyšní	pyšnit	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
mála	málo	k4c2
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
od	od	k7c2
počátku	počátek	k1gInSc2
konstruovány	konstruován	k2eAgFnPc4d1
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
zamýšlený	zamýšlený	k2eAgInSc4d1
účel	účel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jiných	jiný	k2eAgInPc6d1
případech	případ	k1gInPc6
byl	být	k5eAaImAgInS
obvykle	obvykle	k6eAd1
využit	využít	k5eAaPmNgInS
koncept	koncept	k1gInSc1
již	již	k6eAd1
existujícího	existující	k2eAgNnSc2d1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
dopravního	dopravní	k2eAgInSc2d1
<g/>
,	,	kIx,
letounu	letoun	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
letounu	letoun	k1gInSc2
Atlantic	Atlantice	k1gFnPc2
je	být	k5eAaImIp3nS
boj	boj	k1gInSc1
proti	proti	k7c3
ponorkám	ponorka	k1gFnPc3
a	a	k8xC
hladinovým	hladinový	k2eAgInPc3d1
cílům	cíl	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
druhotné	druhotný	k2eAgFnPc4d1
role	role	k1gFnPc4
patří	patřit	k5eAaImIp3nS
pátrací	pátrací	k2eAgFnPc4d1
a	a	k8xC
záchranné	záchranný	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
,	,	kIx,
pokládání	pokládání	k1gNnSc4
nebo	nebo	k8xC
vyhledávání	vyhledávání	k1gNnSc4
námořních	námořní	k2eAgFnPc2d1
min	mina	k1gFnPc2
a	a	k8xC
námořní	námořní	k2eAgNnSc4d1
hlídkování	hlídkování	k1gNnSc4
na	na	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Atlantic	Atlantice	k1gFnPc2
může	moct	k5eAaImIp3nS
nést	nést	k5eAaImF
buď	buď	k8xC
osm	osm	k4xCc4
řízených	řízený	k2eAgNnPc2d1
protiponorkových	protiponorkový	k2eAgNnPc2d1
torpéd	torpédo	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
torpéda	torpédo	k1gNnSc2
Mk	Mk	k1gFnSc1
46	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
12	#num#	k4
hlubinných	hlubinný	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
nebo	nebo	k8xC
dvě	dva	k4xCgFnPc4
protilodní	protilodní	k2eAgFnPc4d1
střely	střela	k1gFnPc4
AM	AM	kA
<g/>
.39	.39	k4
Exocet	Exocet	k1gFnSc2
ve	v	k7c6
vnitřní	vnitřní	k2eAgFnSc6d1
pumovnici	pumovnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německé	německý	k2eAgInPc1d1
letouny	letoun	k1gInPc1
obvykle	obvykle	k6eAd1
nesly	nést	k5eAaImAgInP
jen	jen	k9
torpéda	torpédo	k1gNnSc2
Mk	Mk	k1gFnSc2
46	#num#	k4
a	a	k8xC
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
létaly	létat	k5eAaImAgFnP
převážně	převážně	k6eAd1
neozbrojeny	ozbrojen	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgFnSc1d1
objednávka	objednávka	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
zněla	znět	k5eAaImAgFnS
na	na	k7c4
40	#num#	k4
letounů	letoun	k1gInPc2
pro	pro	k7c4
Francii	Francie	k1gFnSc4
a	a	k8xC
20	#num#	k4
letounů	letoun	k1gInPc2
pro	pro	k7c4
Německo	Německo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letouny	letoun	k1gInPc1
byly	být	k5eAaImAgInP
dodány	dodat	k5eAaPmNgInP
v	v	k7c6
letech	léto	k1gNnPc6
1965	#num#	k4
až	až	k9
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
byla	být	k5eAaImAgFnS
poté	poté	k6eAd1
zastavena	zastavit	k5eAaPmNgFnS
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
než	než	k8xS
Nizozemsko	Nizozemsko	k1gNnSc1
objednalo	objednat	k5eAaPmAgNnS
9	#num#	k4
letounů	letoun	k1gInPc2
a	a	k8xC
Itálie	Itálie	k1gFnSc1
18	#num#	k4
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
letouny	letoun	k1gInPc1
byly	být	k5eAaImAgInP
vyrobeny	vyrobit	k5eAaPmNgInP
a	a	k8xC
dodány	dodat	k5eAaPmNgInP
v	v	k7c6
letech	léto	k1gNnPc6
1972	#num#	k4
až	až	k9
1974	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
francouzská	francouzský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
schválila	schválit	k5eAaPmAgFnS
vývoj	vývoj	k1gInSc4
nové	nový	k2eAgFnSc2d1
vylepšené	vylepšený	k2eAgFnSc2d1
verze	verze	k1gFnSc2
letounu	letoun	k1gInSc2
pod	pod	k7c7
názvem	název	k1gInSc7
Atlantic	Atlantice	k1gFnPc2
Nouvelle	Nouvelle	k1gFnSc1
Generation	Generation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
jiné	jiný	k2eAgInPc4d1
státy	stát	k1gInPc4
letoun	letoun	k1gInSc4
neobjednaly	objednat	k5eNaPmAgFnP
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
tyto	tento	k3xDgInPc1
letouny	letoun	k1gInPc1
známé	známý	k2eAgInPc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
Atlantique	Atlantiqu	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
drak	drak	k1gInSc1
letounu	letoun	k1gInSc2
a	a	k8xC
motory	motor	k1gInPc1
se	se	k3xPyFc4
změnily	změnit	k5eAaPmAgInP
jen	jen	k9
velmi	velmi	k6eAd1
málo	málo	k1gNnSc1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgNnSc1d1
vybavení	vybavení	k1gNnSc1
a	a	k8xC
avionika	avionika	k1gFnSc1
byly	být	k5eAaImAgFnP
významně	významně	k6eAd1
zrevidovány	zrevidován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
dostal	dostat	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
radar	radar	k1gInSc1
<g/>
,	,	kIx,
procesor	procesor	k1gInSc1
sonaru	sonar	k1gInSc2
a	a	k8xC
taktický	taktický	k2eAgInSc4d1
počítač	počítač	k1gInSc4
s	s	k7c7
věžičkou	věžička	k1gFnSc7
infračervené	infračervený	k2eAgFnSc2d1
kamery	kamera	k1gFnSc2
pod	pod	k7c7
nosem	nos	k1gInSc7
letounu	letoun	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidána	přidán	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
rovněž	rovněž	k9
schopnost	schopnost	k1gFnSc1
nést	nést	k5eAaImF
střely	střela	k1gFnPc4
Exocet	Exocet	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Dva	dva	k4xCgInPc4
prototypy	prototyp	k1gInPc4
letounu	letoun	k1gInSc2
Atlantique	Atlantiqu	k1gInSc2
2	#num#	k4
byly	být	k5eAaImAgInP
vyrobeny	vyroben	k2eAgInPc1d1
přestavěním	přestavění	k1gNnSc7
existujících	existující	k2eAgInPc2d1
letounů	letoun	k1gInPc2
Atlantic	Atlantice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
takto	takto	k6eAd1
upravený	upravený	k2eAgInSc4d1
prototyp	prototyp	k1gInSc4
vzlétnul	vzlétnout	k5eAaPmAgMnS
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
byla	být	k5eAaImAgFnS
odsouhlasena	odsouhlasen	k2eAgFnSc1d1
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1984	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
Dodávky	dodávka	k1gFnPc1
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
celkem	celek	k1gInSc7
28	#num#	k4
letounů	letoun	k1gInPc2
ačkoliv	ačkoliv	k8xS
původní	původní	k2eAgInSc1d1
požadavek	požadavek	k1gInSc1
zněl	znět	k5eAaImAgInS
na	na	k7c4
42	#num#	k4
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Operační	operační	k2eAgFnSc1d1
historie	historie	k1gFnSc1
</s>
<s>
Detail	detail	k1gInSc1
otvorů	otvor	k1gInPc2
pro	pro	k7c4
vypouštění	vypouštění	k1gNnSc4
sonarových	sonarův	k2eAgFnPc2d1
bójí	bóje	k1gFnPc2
</s>
<s>
Nizozemské	nizozemský	k2eAgNnSc1d1
královské	královský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
ztratilo	ztratit	k5eAaPmAgNnS
tři	tři	k4xCgInPc4
letouny	letoun	k1gInPc4
z	z	k7c2
devíti	devět	k4xCc2
při	při	k7c6
sérii	série	k1gFnSc6
poruch	porucha	k1gFnPc2
nad	nad	k7c7
Atlantským	atlantský	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
odstavení	odstavení	k1gNnSc3
těchto	tento	k3xDgInPc2
letounů	letoun	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
nahrazení	nahrazení	k1gNnSc2
letouny	letoun	k1gInPc1
P-3	P-3	k1gFnSc1
Orion	orion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německé	německý	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
nikdy	nikdy	k6eAd1
neztratilo	ztratit	k5eNaPmAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1963	#num#	k4
až	až	k9
2005	#num#	k4
ani	ani	k8xC
jeden	jeden	k4xCgInSc1
letoun	letoun	k1gInSc1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
měl	mít	k5eAaImAgMnS
nehodu	nehoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německé	německý	k2eAgInPc1d1
letouny	letoun	k1gInPc1
byly	být	k5eAaImAgInP
časem	časem	k6eAd1
nahrazeny	nahrazen	k2eAgInPc4d1
nizozemskými	nizozemský	k2eAgInPc7d1
letouny	letoun	k1gInPc7
P-3	P-3	k1gMnSc2
Orion	orion	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
si	se	k3xPyFc3
Nizozemsko	Nizozemsko	k1gNnSc1
koupilo	koupit	k5eAaPmAgNnS
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
k	k	k7c3
nahrazení	nahrazení	k1gNnSc3
stejných	stejný	k2eAgInPc2d1
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
letouny	letoun	k1gInPc1
však	však	k9
byly	být	k5eAaImAgInP
mezitím	mezitím	k6eAd1
podstatně	podstatně	k6eAd1
modernizovány	modernizován	k2eAgFnPc1d1
a	a	k8xC
vylepšeny	vylepšen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
byl	být	k5eAaImAgInS
letoun	letoun	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
pákistánského	pákistánský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
sestřelen	sestřelit	k5eAaPmNgInS
indickými	indický	k2eAgFnPc7d1
stíhačkami	stíhačka	k1gFnPc7
MiG-	MiG-	k1gFnPc6
<g/>
21	#num#	k4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
nepodařilo	podařit	k5eNaPmAgNnS
přinutit	přinutit	k5eAaPmF
letoun	letoun	k1gInSc4
přistát	přistát	k5eAaImF,k5eAaPmF
na	na	k7c6
indické	indický	k2eAgFnSc6d1
základně	základna	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jenom	jenom	k9
zvýšilo	zvýšit	k5eAaPmAgNnS
napětí	napětí	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
letounu	letoun	k1gInSc2
Atlantic	Atlantice	k1gFnPc2
neuposlechla	uposlechnout	k5eNaPmAgFnS
pokyny	pokyn	k1gInPc7
pilotů	pilot	k1gMnPc2
stíhaček	stíhačka	k1gFnPc2
a	a	k8xC
snažila	snažit	k5eAaImAgFnS
se	se	k3xPyFc4
uniknout	uniknout	k5eAaPmF
z	z	k7c2
indického	indický	k2eAgInSc2d1
vzdušného	vzdušný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byl	být	k5eAaImAgInS
letoun	letoun	k1gInSc1
sestřelen	sestřelit	k5eAaPmNgInS
teplem	teplo	k1gNnSc7
naváděnou	naváděný	k2eAgFnSc7d1
raketou	raketa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
se	se	k3xPyFc4
zřítil	zřítit	k5eAaPmAgInS
v	v	k7c6
indickém	indický	k2eAgInSc6d1
pohraničním	pohraniční	k2eAgInSc6d1
regionu	region	k1gInSc6
Kutch	Kutch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Několik	několik	k4yIc1
německých	německý	k2eAgMnPc2d1
letounů	letoun	k1gMnPc2
bylo	být	k5eAaImAgNnS
umístěno	umístit	k5eAaPmNgNnS
do	do	k7c2
muzeí	muzeum	k1gNnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
do	do	k7c2
Luftwaffenmuseum	Luftwaffenmuseum	k1gNnSc4
nebo	nebo	k8xC
Nizozemského	nizozemský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
vojenského	vojenský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
(	(	kIx(
<g/>
Dutch	Dutch	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc2
Museum	museum	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
Soesterbergu	Soesterberg	k1gInSc6
v	v	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
některých	některý	k3yIgFnPc2
zpráv	zpráva	k1gFnPc2
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
letouny	letoun	k1gInPc4
Breguet	Breguet	k1gMnSc1
Atlantique	Atlantique	k1gInSc4
2	#num#	k4
francouzského	francouzský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
se	se	k3xPyFc4
účastnily	účastnit	k5eAaImAgFnP
pátrání	pátrání	k1gNnSc2
po	po	k7c6
troskách	troska	k1gFnPc6
letounu	letoun	k1gInSc2
letu	let	k1gInSc2
447	#num#	k4
Air	Air	k1gMnSc2
France	Franc	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nehody	nehoda	k1gFnPc1
a	a	k8xC
incidenty	incident	k1gInPc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1986	#num#	k4
–	–	k?
Během	během	k7c2
prolétání	prolétání	k1gNnSc2
vrstvou	vrstva	k1gFnSc7
oblačnosti	oblačnost	k1gFnSc2
francouzský	francouzský	k2eAgInSc4d1
letoun	letoun	k1gInSc4
Atlantic	Atlantice	k1gFnPc2
narazil	narazit	k5eAaPmAgMnS
do	do	k7c2
hory	hora	k1gFnSc2
ve	v	k7c6
státě	stát	k1gInSc6
Džíbútí	Džíbúť	k1gFnPc2
<g/>
,	,	kIx,
zahynulo	zahynout	k5eAaPmAgNnS
všech	všecek	k3xTgMnPc2
19	#num#	k4
lidí	člověk	k1gMnPc2
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1999	#num#	k4
–	–	k?
Během	během	k7c2
incidentu	incident	k1gInSc2
na	na	k7c6
indicko-pákistánské	indicko-pákistánský	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
zahynulo	zahynout	k5eAaPmAgNnS
16	#num#	k4
pákistánských	pákistánský	k2eAgMnPc2d1
námořních	námořní	k2eAgMnPc2d1
letců	letec	k1gMnPc2
právě	právě	k9
měsíc	měsíc	k1gInSc4
po	po	k7c6
skončení	skončení	k1gNnSc6
Kárgilské	Kárgilský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Varianty	varianta	k1gFnPc1
</s>
<s>
Br.	Br.	k?
<g/>
1150	#num#	k4
Atlantic	Atlantice	k1gFnPc2
</s>
<s>
Atlantique	Atlantique	k6eAd1
2	#num#	k4
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Jiný	jiný	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
německý	německý	k2eAgInSc4d1
letoun	letoun	k1gInSc4
Breguet	Bregueta	k1gFnPc2
Atlantic	Atlantice	k1gFnPc2
Br.	Br.	k1gMnPc2
<g/>
1150	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Breguet	Breguet	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
Br.	Br.	k1gMnPc2
<g/>
1150	#num#	k4
italského	italský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
–	–	k?
původní	původní	k2eAgInSc1d1
model	model	k1gInSc1
byl	být	k5eAaImAgInS
vyřazen	vyřadit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letouny	letoun	k1gMnPc7
Atlantique	Atlantiqu	k1gFnSc2
2	#num#	k4
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
ve	v	k7c6
službě	služba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Italské	italský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
–	–	k?
typ	typ	k1gInSc4
byl	být	k5eAaImAgMnS
zcela	zcela	k6eAd1
vyřazen	vyřadit	k5eAaPmNgMnS
v	v	k7c6
září	září	k1gNnSc6
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
45	#num#	k4
let	léto	k1gNnPc2
služby	služba	k1gFnSc2
italské	italský	k2eAgInPc1d1
letouny	letoun	k1gInPc1
Atlantic	Atlantice	k1gFnPc2
nalétaly	nalétat	k5eAaBmAgInP,k5eAaPmAgInP,k5eAaImAgInP
více	hodně	k6eAd2
než	než	k8xS
250	#num#	k4
000	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Německé	německý	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
–	–	k?
Obdrženo	obdržen	k2eAgNnSc1d1
20	#num#	k4
letounů	letoun	k1gInPc2
Atlantic	Atlantice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
upraveno	upravit	k5eAaPmNgNnS
k	k	k7c3
provádění	provádění	k1gNnSc3
elektronického	elektronický	k2eAgNnSc2d1
zpravodajství	zpravodajství	k1gNnSc2
(	(	kIx(
<g/>
ELINT	ELINT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgInPc1
letouny	letoun	k1gInPc1
byly	být	k5eAaImAgInP
nahrazeny	nahradit	k5eAaPmNgInP
protiponorkovými	protiponorkový	k2eAgFnPc7d1
letouny	letoun	k1gInPc1
P-3	P-3	k1gFnSc1
Orion	orion	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
si	se	k3xPyFc3
Německo	Německo	k1gNnSc1
pořídilo	pořídit	k5eAaPmAgNnS
od	od	k7c2
Nizozemska	Nizozemsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgInPc1d1
letouny	letoun	k1gInPc1
ELINT	ELINT	kA
byly	být	k5eAaImAgInP
nahrazeny	nahrazen	k2eAgInPc1d1
bezpilotními	bezpilotní	k2eAgInPc7d1
letouny	letoun	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Nizozemské	nizozemský	k2eAgNnSc1d1
královské	královský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
–	–	k?
Letouny	letoun	k1gInPc1
byly	být	k5eAaImAgInP
nahrazeny	nahradit	k5eAaPmNgInP
letouny	letoun	k1gInPc1
P-3	P-3	k1gFnSc1
Orion	orion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pákistán	Pákistán	k1gInSc1
Pákistán	Pákistán	k1gInSc1
</s>
<s>
Pákistánské	pákistánský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
</s>
<s>
Specifikace	specifikace	k1gFnSc1
(	(	kIx(
<g/>
Atlantique	Atlantique	k1gInSc1
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
publikace	publikace	k1gFnSc2
„	„	k?
<g/>
Jane	Jan	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
All	All	k1gFnSc7
The	The	k1gFnSc1
World	World	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Aircraft	Aircraft	k1gInSc1
1988	#num#	k4
<g/>
–	–	k?
<g/>
89	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
12	#num#	k4
</s>
<s>
Kapacita	kapacita	k1gFnSc1
<g/>
:	:	kIx,
12	#num#	k4
cestujících	cestující	k1gMnPc2
nebo	nebo	k8xC
záložní	záložní	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
<g/>
:	:	kIx,
37,42	37,42	k4
m	m	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
31,62	31,62	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
10,89	10,89	k4
m	m	kA
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
:	:	kIx,
120,34	120,34	k4
m²	m²	k?
</s>
<s>
Plošné	plošný	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
<g/>
:	:	kIx,
383,9	383,9	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m²	m²	k?
</s>
<s>
Prázdná	prázdný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
25	#num#	k4
700	#num#	k4
kg	kg	kA
</s>
<s>
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
:	:	kIx,
46	#num#	k4
200	#num#	k4
kg	kg	kA
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
×	×	k?
turbovrtulový	turbovrtulový	k2eAgInSc1d1
motor	motor	k1gInSc1
Rolls-Royce	Rolls-Royce	k1gFnSc2
Tyne	Tyn	k1gFnSc2
RTy	ret	k1gInPc1
<g/>
.20	.20	k4
Mk	Mk	k1gFnSc2
21	#num#	k4
</s>
<s>
Výkon	výkon	k1gInSc1
pohonné	pohonný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
:	:	kIx,
6	#num#	k4
100	#num#	k4
k	k	k7c3
(	(	kIx(
<g/>
4	#num#	k4
549	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
315	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
170	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
,	,	kIx,
195	#num#	k4
mph	mph	k?
<g/>
)	)	kIx)
ve	v	k7c6
výšce	výška	k1gFnSc6
?	?	kIx.
</s>
<s desamb="1">
m	m	kA
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
648	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
(	(	kIx(
<g/>
350	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
,	,	kIx,
402	#num#	k4
mph	mph	k?
<g/>
)	)	kIx)
ve	v	k7c6
výšce	výška	k1gFnSc6
?	?	kIx.
</s>
<s desamb="1">
m	m	kA
</s>
<s>
Dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
9	#num#	k4
075	#num#	k4
km	km	kA
(	(	kIx(
<g/>
18	#num#	k4
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
9	#num#	k4
145	#num#	k4
m	m	kA
(	(	kIx(
<g/>
30	#num#	k4
000	#num#	k4
stop	stopa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Stoupavost	stoupavost	k1gFnSc1
<g/>
:	:	kIx,
14,7	14,7	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
až	až	k9
3	#num#	k4
500	#num#	k4
kg	kg	kA
výzbroje	výzbroj	k1gFnSc2
zahrnující	zahrnující	k2eAgNnPc4d1
torpéda	torpédo	k1gNnPc4
<g/>
,	,	kIx,
hlubinné	hlubinný	k2eAgFnPc4d1
bomby	bomba	k1gFnPc4
<g/>
,	,	kIx,
námořní	námořní	k2eAgFnPc4d1
miny	mina	k1gFnPc4
<g/>
,	,	kIx,
protilodní	protilodní	k2eAgFnPc4d1
střely	střela	k1gFnPc4
<g/>
,	,	kIx,
bomby	bomba	k1gFnPc4
a	a	k8xC
bóje	bóje	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Breguet	Bregueta	k1gFnPc2
Atlantic	Atlantice	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Defence	Defence	k1gFnSc1
Journal	Journal	k1gFnSc2
of	of	k?
Pakistan	Pakistan	k1gInSc1
referring	referring	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
cost	cost	k1gInSc1
of	of	k?
the	the	k?
airplane	airplanout	k5eAaPmIp3nS
with	with	k1gInSc1
reference	reference	k1gFnSc2
to	ten	k3xDgNnSc1
its	its	k?
downing	downing	k1gInSc1
in	in	k?
the	the	k?
Atlantique	Atlantique	k1gInSc1
Incident	incident	k1gInSc1
<g/>
↑	↑	k?
Air	Air	k1gFnPc2
International	International	k1gFnSc1
Listopad	listopad	k1gInSc1
1981	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
218	#num#	k4
<g/>
,	,	kIx,
252	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Air	Air	k1gMnSc1
International	International	k1gMnSc1
Listopad	listopad	k1gInSc4
1981	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.252	.252	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Air	Air	k1gMnSc1
International	International	k1gMnSc1
Listopad	listopad	k1gInSc4
1981	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
213	#num#	k4
<g/>
–	–	k?
<g/>
216.1	216.1	k4
2	#num#	k4
3	#num#	k4
Taylor	Taylora	k1gFnPc2
1988	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
71	#num#	k4
<g/>
–	–	k?
<g/>
73	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Letoun	letoun	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
na	na	k7c6
webu	web	k1gInSc6
Naval	navalit	k5eAaPmRp2nS
Technology	technolog	k1gMnPc4
Archivováno	archivován	k2eAgNnSc4d1
13	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
citováno	citovat	k5eAaBmNgNnS
<g/>
:	:	kIx,
1.5	1.5	k4
<g/>
.2011	.2011	k4
<g/>
↑	↑	k?
Air	Air	k1gMnSc1
International	International	k1gMnSc1
Listopad	listopad	k1gInSc4
1981	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
252	#num#	k4
<g/>
–	–	k?
<g/>
253	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Air	Air	k1gMnSc1
International	International	k1gMnSc1
Listopad	listopad	k1gInSc4
1981	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
216	#num#	k4
<g/>
–	–	k?
<g/>
218	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lambert	Lambert	k1gInSc1
1993	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
81	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Penny	penny	k1gInSc1
<g/>
,	,	kIx,
Stewart	Stewart	k1gMnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Military	Militar	k1gInPc1
Aircraft	Aircrafta	k1gFnPc2
Directory	Director	k1gInPc4
Part	part	k1gInSc1
1	#num#	k4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flight	Flight	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1999	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
leteckého	letecký	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
Archivováno	archivovat	k5eAaBmNgNnS
18	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
v	v	k7c6
Soesterbergu	Soesterberg	k1gInSc6
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Search	Search	k1gInSc1
Teams	Teamsa	k1gFnPc2
Converge	Converg	k1gInSc2
on	on	k3xPp3gMnSc1
Presumed	Presumed	k1gMnSc1
Air	Air	k1gMnSc1
France	Franc	k1gMnSc2
Crash	Crash	k1gMnSc1
Zone	Zon	k1gMnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
The	The	k1gMnPc2
Washington	Washington	k1gInSc4
Post	posta	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
French	French	k1gMnSc1
army	arma	k1gFnSc2
air	air	k?
crewman	crewman	k1gMnSc1
aboard	aboard	k1gMnSc1
an	an	k?
Atlantic	Atlantice	k1gFnPc2
Model	modla	k1gFnPc2
2	#num#	k4
aircraft	aircrafta	k1gFnPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Associated	Associated	k1gInSc4
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
An	An	k1gMnSc1
Atlantic	Atlantice	k1gFnPc2
Model	model	k1gInSc4
2	#num#	k4
aircraft	aircraft	k2eAgInSc4d1
lands	lands	k1gInSc4
at	at	k?
France	Franc	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
air	air	k?
base	basa	k1gFnSc6
in	in	k?
Dakar	Dakar	k1gInSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Associated	Associated	k1gInSc4
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
<g/>
↑	↑	k?
P-72A	P-72A	k1gMnSc5
Maritime	Maritim	k1gMnSc5
Surveillance	Surveillanka	k1gFnSc3
Aircraft	Aircraft	k2eAgInSc4d1
Replaces	Replaces	k1gInSc4
Italy	Ital	k1gMnPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
P1150	P1150	k1gFnPc7
Atlantic	Atlantice	k1gFnPc2
MPA	MPA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navaltoday	Navaltodaa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-09-21	2017-09-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Donald	Donald	k1gMnSc1
and	and	k?
Lake	Lake	k1gInSc1
1996	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.121	.121	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Donald	Donald	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
and	and	k?
Jon	Jon	k1gMnSc1
Lake	Lak	k1gFnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
editors	editors	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
World	World	k1gMnSc1
Military	Militara	k1gFnSc2
Aircraft	Aircraft	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Aerospace	Aerospace	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
Single	singl	k1gInSc5
Volume	volum	k1gInSc5
edition	edition	k1gInSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
874023	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lambert	Lambert	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jane	Jan	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
All	All	k1gFnSc7
The	The	k1gFnSc1
World	World	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Aircraft	Aircraft	k1gInSc1
1993	#num#	k4
<g/>
–	–	k?
<g/>
94	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Coulsdon	Coulsdon	k1gNnSc1
<g/>
,	,	kIx,
UK	UK	kA
<g/>
:	:	kIx,
<g/>
Jane	Jan	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Data	datum	k1gNnSc2
Division	Division	k1gInSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
1066	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
The	The	k1gMnPc7
New	New	k1gFnSc2
Generation	Generation	k1gInSc1
Atlantics	Atlantics	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Air	Air	k1gFnSc1
International	International	k1gFnSc1
<g/>
,	,	kIx,
November	November	k1gInSc1
1981	#num#	k4
<g/>
,	,	kIx,
Vol	vol	k6eAd1
<g/>
.	.	kIx.
21	#num#	k4
No	no	k9
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.	.	kIx.
213	#num#	k4
<g/>
–	–	k?
<g/>
218	#num#	k4
<g/>
,	,	kIx,
252	#num#	k4
<g/>
–	–	k?
<g/>
253	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Taylor	Taylor	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
W.	W.	kA
R.	R.	kA
(	(	kIx(
<g/>
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jane	Jan	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
All	All	k1gMnSc7
the	the	k?
Worlds	Worlds	k1gInSc1
Aircraft	Aircraft	k2eAgInSc1d1
1988	#num#	k4
<g/>
–	–	k?
<g/>
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Coulsdon	Coulsdon	k1gNnSc1
<g/>
,	,	kIx,
Surrey	Surrey	k1gInPc1
<g/>
,	,	kIx,
UK	UK	kA
<g/>
:	:	kIx,
Jane	Jan	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Information	Information	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
867	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
Avro	Avro	k6eAd1
696	#num#	k4
Shackleton	Shackleton	k1gInSc4
</s>
<s>
Boeing	boeing	k1gInSc1
P-8	P-8	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
</s>
<s>
Hawker	Hawker	k1gMnSc1
Siddeley	Siddelea	k1gFnSc2
Nimrod	Nimrod	k1gMnSc1
</s>
<s>
Lockheed	Lockheed	k1gMnSc1
P-3	P-3	k1gMnSc1
Orion	orion	k1gInSc4
</s>
<s>
A319	A319	k4
MMA	MMA	kA
<g/>
/	/	kIx~
<g/>
MPA	MPA	kA
</s>
<s>
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc2
<g/>
38	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Breguet	Bregueta	k1gFnPc2
Atlantic	Atlantice	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Breguet	Bregueta	k1gFnPc2
Atlantic	Atlantice	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Lokální	lokální	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
jinou	jiný	k2eAgFnSc4d1
galerii	galerie	k1gFnSc4
Commons	Commonsa	k1gFnPc2
než	než	k8xS
přiřazená	přiřazený	k2eAgFnSc1d1
položka	položka	k1gFnSc1
Wikidat	Wikidat	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Lokální	lokální	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
<g/>
:	:	kIx,
Breguet	Breguet	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
</s>
<s>
Wikidata	Wikidata	k1gFnSc1
<g/>
:	:	kIx,
c	c	k0
<g/>
:	:	kIx,
<g/>
Breguet	Breguet	k1gInSc4
Br.	Br.	k1gFnPc2
<g/>
1150	#num#	k4
Atlantic	Atlantice	k1gFnPc2
</s>
<s>
https://web.archive.org/web/20061113132458/http://www.naval-technology.com/projects/atlantique/index.html	https://web.archive.org/web/20061113132458/http://www.naval-technology.com/projects/atlantique/index.html	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Označení	označení	k1gNnSc1
letadel	letadlo	k1gNnPc2
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Itálie	Itálie	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
1	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
</s>
<s>
Q-1	Q-1	k4
</s>
<s>
G-2	G-2	k4
</s>
<s>
H-	H-	k?
<g/>
3	#num#	k4
<g/>
D	D	kA
<g/>
/	/	kIx~
<g/>
H-	H-	k1gMnSc1
<g/>
3	#num#	k4
<g/>
F	F	kA
</s>
<s>
G-4	G-4	k4
</s>
<s>
AV-8	AV-8	k4
</s>
<s>
Q-9	Q-9	k4
</s>
<s>
Q-10	Q-10	k4
</s>
<s>
A-11	A-11	k4
</s>
<s>
Q-11	Q-11	k4
</s>
<s>
Q-12	Q-12	k4
</s>
<s>
F-16	F-16	k4
</s>
<s>
G-17	G-17	k4
</s>
<s>
G-21	G-21	k4
</s>
<s>
Q-24	Q-24	k4
</s>
<s>
Q-25	Q-25	k4
</s>
<s>
Q-26	Q-26	k4
</s>
<s>
C-27	C-27	k4
</s>
<s>
Q-27	Q-27	k4
</s>
<s>
F-35	F-35	k4
</s>
<s>
C-	C-	k?
<g/>
42	#num#	k4
<g/>
/	/	kIx~
<g/>
P-	P-	k1gFnSc2
<g/>
42	#num#	k4
</s>
<s>
H-47	H-47	k4
</s>
<s>
C-50	C-50	k4
</s>
<s>
P-72	P-72	k4
</s>
<s>
H-90	H-90	k4
101	#num#	k4
<g/>
–	–	k?
<g/>
200	#num#	k4
</s>
<s>
H-101	H-101	k4
</s>
<s>
G-103	G-103	k4
</s>
<s>
H-109	H-109	k4
</s>
<s>
H-129	H-129	k4
</s>
<s>
C-130	C-130	k4
</s>
<s>
H-139	H-139	k4
</s>
<s>
U-166	U-166	k4
</s>
<s>
C-180	C-180	k4
</s>
<s>
A-200	A-200	k4
201	#num#	k4
<g/>
–	–	k?
<g/>
400	#num#	k4
</s>
<s>
H-205	H-205	k4
</s>
<s>
H-206	H-206	k4
</s>
<s>
U-208	U-208	k4
</s>
<s>
H-212	H-212	k4
</s>
<s>
C-222	C-222	k4
</s>
<s>
C-228	C-228	k4
</s>
<s>
T-260	T-260	k4
</s>
<s>
C-319	C-319	k4
</s>
<s>
T-339	T-339	k4
</s>
<s>
T-345	T-345	k4
</s>
<s>
T-346	T-346	k4
401	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
</s>
<s>
H-412	H-412	k4
</s>
<s>
H-500	H-500	k4
</s>
<s>
KC-707	KC-707	k4
</s>
<s>
KC-767	KC-767	k4
</s>
<s>
C-900	C-900	k4
</s>
<s>
P-1150	P-1150	k4
</s>
<s>
F-2000	F-2000	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
