<s>
Avinurme	Avinurm	k1gInSc5
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Avinurme	Avinurm	k1gInSc5
vald	vald	k1gInSc4
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
Avinurme	Avinurm	k1gInSc5
v	v	k7c6
rámci	rámec	k1gInSc6
kraje	kraj	k1gInSc2
Ida-Virumaa	Ida-Viruma	k1gInSc2
(	(	kIx(
<g/>
před	před	k7c7
rokem	rok	k1gInSc7
2017	#num#	k4
<g/>
)	)	kIx)
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Avinurme	Avinurm	k1gInSc5
Souřadnice	souřadnice	k1gFnPc4
</s>
<s>
58	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
190	#num#	k4
km²	km²	k?
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
536	#num#	k4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
8,1	8,1	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Ida-Virumaa	Ida-Virumaa	k6eAd1
Druh	druh	k1gInSc1
celku	celek	k1gInSc2
</s>
<s>
Obec	obec	k1gFnSc1
(	(	kIx(
<g/>
vald	vald	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Avinurme	Avinurm	k1gInSc5
(	(	kIx(
<g/>
estonsky	estonsky	k6eAd1
Avinurme	Avinurm	k1gInSc5
vald	vald	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
samosprávná	samosprávný	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
estonském	estonský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
Ida-Virumaa	Ida-Viruma	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
začleněna	začlenit	k5eAaPmNgFnS
do	do	k7c2
obce	obec	k1gFnSc2
Mustvee	Mustvee	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
přešla	přejít	k5eAaPmAgFnS
pod	pod	k7c4
kraj	kraj	k1gInSc4
Jõ	Jõ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sídla	sídlo	k1gNnPc1
</s>
<s>
V	v	k7c6
obci	obec	k1gFnSc6
žije	žít	k5eAaImIp3nS
půldruha	půldruha	k1gFnSc1
tisíce	tisíc	k4xCgInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
přes	přes	k7c4
polovinu	polovina	k1gFnSc4
v	v	k7c6
městečku	městečko	k1gNnSc6
Avinurme	Avinurm	k1gInSc5
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
je	být	k5eAaImIp3nS
administrativním	administrativní	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
zbytek	zbytek	k1gInSc1
v	v	k7c6
16	#num#	k4
vesnicích	vesnice	k1gFnPc6
Adraku	Adrak	k1gInSc2
<g/>
,	,	kIx,
Alekere	Aleker	k1gMnSc5
<g/>
,	,	kIx,
Kaevussaare	Kaevussaar	k1gMnSc5
<g/>
,	,	kIx,
Kiissa	Kiissa	k1gFnSc1
<g/>
,	,	kIx,
Kõ	Kõ	k1gFnPc1
<g/>
,	,	kIx,
Kõ	Kõ	k1gFnSc1
<g/>
,	,	kIx,
Kõ	Kõ	k1gFnSc4
<g/>
,	,	kIx,
Laekannu	Laekanna	k1gFnSc4
<g/>
,	,	kIx,
Lepiksaare	Lepiksaar	k1gMnSc5
<g/>
,	,	kIx,
Maetsma	Maetsmum	k1gNnPc1
<g/>
,	,	kIx,
Paadenurme	Paadenurm	k1gInSc5
<g/>
,	,	kIx,
Sälliksaare	Sälliksaar	k1gMnSc5
<g/>
,	,	kIx,
Tammessaare	Tammessaar	k1gMnSc5
<g/>
,	,	kIx,
Ulvi	Ulv	k1gMnSc5
<g/>
,	,	kIx,
Vadi	Vad	k1gMnSc5
a	a	k8xC
Änniksaare	Änniksaar	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Avinurme	Avinurm	k1gInSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
(	(	kIx(
<g/>
estonsky	estonsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
