<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
nejchladnější	chladný	k2eAgFnSc7d3	nejchladnější
atmosférou	atmosféra	k1gFnSc7	atmosféra
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnPc1d1	minimální
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
-224	-224	k4	-224
°	°	k?	°
<g/>
C	C	kA	C
<g/>
.	.	kIx.	.
</s>
