<s>
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
</s>
<s>
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
Scéna	scéna	k1gFnSc1
z	z	k7c2
premiéry	premiéra	k1gFnSc2
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
opera	opera	k1gFnSc1
Skladatel	skladatel	k1gMnSc1
</s>
<s>
Eef	Eef	k?
van	van	k1gInSc1
Breen	Breen	k2eAgMnSc1d1
Libretista	libretista	k1gMnSc1
</s>
<s>
Kees	Kees	k6eAd1
Ligtelijn	Ligtelijn	k1gInSc1
a	a	k8xC
Marc	Marc	k1gInSc1
Okrand	Okranda	k1gFnPc2
pod	pod	k7c7
uměleckým	umělecký	k2eAgInSc7d1
dohledem	dohled	k1gInSc7
Florise	Florise	k1gFnSc2
Schönfelda	Schönfeldo	k1gNnSc2
Počet	počet	k1gInSc1
dějství	dějství	k1gNnPc2
</s>
<s>
3	#num#	k4
Originální	originální	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
klingonština	klingonština	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
<g/>
,	,	kIx,
Haag	Haag	k1gInSc1
<g/>
,	,	kIx,
Theater	Theater	k1gMnSc1
Zeebelt	Zeebelt	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
’	’	kIx"
<g/>
u	u	kA
<g/>
’	’	kIx"
(	(	kIx(
<g/>
název	název	k1gInSc1
začíná	začínat	k5eAaImIp3nS
a	a	k8xC
končí	končit	k5eAaImIp3nS
rázem	rázem	k1gInSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc1
opera	opera	k1gFnSc1
v	v	k7c6
klingonštině	klingonština	k1gFnSc6
<g/>
,	,	kIx,
uváděná	uváděný	k2eAgFnSc1d1
jako	jako	k9
„	„	kIx"
<g/>
první	první	k4xOgFnSc1
autentická	autentický	k2eAgFnSc1d1
klingonská	klingonský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
“	“	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složil	složit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
Eef	Eef	k1gMnSc1
van	van	k7
Breen	Breen	k1gMnSc1
na	na	k7c4
libreto	libreto	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc7
autory	autor	k1gMnPc7
jsou	být	k5eAaImIp3nP
Kees	Kees	k1gMnSc1
Ligtelijn	Ligtelijn	k1gInSc1
a	a	k8xC
Marc	Marc	k1gInSc1
Okrand	Okranda	k1gMnSc1
pod	pod	k7c7
uměleckým	umělecký	k2eAgInSc7d1
dohledem	dohled	k1gInSc7
Florise	Florise	k1gMnSc2
Schönfelda	Schönfelda	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děj	děj	k1gInSc1
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
epické	epický	k2eAgFnSc6d1
legendě	legenda	k1gFnSc6
o	o	k7c6
Kahlessovi	Kahless	k1gMnSc6
Neporazitelném	porazitelný	k2eNgMnSc6d1
<g/>
,	,	kIx,
mesiáši	mesiáš	k1gMnPc7
ve	v	k7c6
fiktivní	fiktivní	k2eAgFnSc6d1
klingonské	klingonský	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Premiéra	premiéra	k1gFnSc1
opery	opera	k1gFnSc2
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
Haagu	Haag	k1gInSc6
v	v	k7c6
divadle	divadlo	k1gNnSc6
Zeebelt	Zeebeltum	k1gNnPc2
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
(	(	kIx(
<g/>
ukázka	ukázka	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
již	již	k6eAd1
den	den	k1gInSc4
předtím	předtím	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dočkala	dočkat	k5eAaPmAgFnS
se	se	k3xPyFc4
opakování	opakování	k1gNnSc1
a	a	k8xC
byla	být	k5eAaImAgFnS
hodnocena	hodnotit	k5eAaImNgFnS
jako	jako	k9
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Koncept	koncept	k1gInSc1
klingonského	klingonský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
nastíněn	nastíněn	k2eAgInSc1d1
hercem	herec	k1gMnSc7
Jamesem	James	k1gMnSc7
Doohanem	Doohan	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
hrál	hrát	k5eAaImAgMnS
Montgomeryho	Montgomery	k1gMnSc4
Scotta	Scott	k1gMnSc4
v	v	k7c6
originálním	originální	k2eAgInSc6d1
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Star	Star	kA
Trek	Treka	k1gFnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
film	film	k1gInSc4
Star	Star	kA
Trek	Trek	k1gInSc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořil	vytvořit	k5eAaPmAgMnS
některá	některý	k3yIgNnPc1
drsně	drsně	k6eAd1
znějící	znějící	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
pro	pro	k7c4
klingonské	klingonský	k2eAgFnPc4d1
postavy	postava	k1gFnPc4
(	(	kIx(
<g/>
ačkoliv	ačkoliv	k8xS
tato	tento	k3xDgFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
filmu	film	k1gInSc2
vystřižena	vystřihnout	k5eAaPmNgFnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Producenti	producent	k1gMnPc1
najali	najmout	k5eAaPmAgMnP
pro	pro	k7c4
další	další	k2eAgNnSc4d1
pokračování	pokračování	k1gNnSc4
lingvistu	lingvista	k1gMnSc4
Marca	Marcus	k1gMnSc2
Okranda	Okrando	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tato	tento	k3xDgNnPc1
slova	slovo	k1gNnPc4
rozšířil	rozšířit	k5eAaPmAgInS
do	do	k7c2
plnohodnotného	plnohodnotný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
vlastním	vlastní	k2eAgInSc7d1
slovníkem	slovník	k1gInSc7
<g/>
,	,	kIx,
gramatikou	gramatika	k1gFnSc7
a	a	k8xC
idiomy	idiom	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Okrand	Okrand	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
jazyk	jazyk	k1gInSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zněl	znět	k5eAaImAgInS
mimozemsky	mimozemsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
klingonštiny	klingonština	k1gFnSc2
stal	stát	k5eAaPmAgInS
mluvený	mluvený	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
s	s	k7c7
množstvím	množství	k1gNnSc7
mluvčích	mluvčí	k1gMnPc2
schopných	schopný	k2eAgMnPc2d1
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
plynule	plynule	k6eAd1
vyjadřovat	vyjadřovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jak	jak	k6eAd1
je	být	k5eAaImIp3nS
líčeno	líčen	k2eAgNnSc1d1
ve	v	k7c6
Star	Star	kA
Treku	Trek	k1gInSc2
<g/>
,	,	kIx,
Klingoni	Klingon	k1gMnPc1
jsou	být	k5eAaImIp3nP
vášniví	vášnivý	k2eAgMnPc1d1
milovníci	milovník	k1gMnPc1
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Dle	dle	k7c2
oficiálních	oficiální	k2eAgFnPc2d1
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
opery	opera	k1gFnSc2
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
užívá	užívat	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
principu	princip	k1gInSc3
hudebního	hudební	k2eAgInSc2d1
boje	boj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krása	krása	k1gFnSc1
klingonské	klingonský	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
střetu	střet	k1gInSc2
dvou	dva	k4xCgFnPc2
rozdílných	rozdílný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
vesmír	vesmír	k1gInSc1
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
univerzum	univerzum	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Opera	opera	k1gFnSc1
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
připravována	připravovat	k5eAaImNgFnS
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2008	#num#	k4
pro	pro	k7c4
uvedení	uvedení	k1gNnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
ve	v	k7c4
Water	Water	k1gInSc4
Mill	Milla	k1gFnPc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Divadelní	divadelní	k2eAgMnSc1d1
a	a	k8xC
umělecký	umělecký	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
<g/>
,	,	kIx,
badatel	badatel	k1gMnSc1
projektu	projekt	k1gInSc2
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
a	a	k8xC
Pozemského	pozemský	k2eAgInSc2d1
souboru	soubor	k1gInSc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
Klingonů	Klingon	k1gMnPc2
Floris	Floris	k1gInSc1
Schönfeld	Schönfelda	k1gFnPc2
pozorně	pozorně	k6eAd1
prozkoumal	prozkoumat	k5eAaPmAgInS
všechny	všechen	k3xTgFnPc4
zmínky	zmínka	k1gFnPc4
a	a	k8xC
ukázky	ukázka	k1gFnPc4
klingonské	klingonský	k2eAgFnSc2d1
opery	opera	k1gFnSc2
ve	v	k7c6
Star	star	k1gFnSc6
Treku	Trek	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
opera	opera	k1gFnSc1
co	co	k9
nejautentičtější	autentický	k2eAgFnSc1d3
a	a	k8xC
následovala	následovat	k5eAaImAgFnS
zaběhnuté	zaběhnutý	k2eAgFnPc4d1
konvence	konvence	k1gFnPc4
klingonských	klingonský	k2eAgFnPc2d1
oper	opera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
hudební	hudební	k2eAgInSc4d1
doprovod	doprovod	k1gInSc4
opery	opera	k1gFnSc2
vytvořil	vytvořit	k5eAaPmAgMnS
Pozemský	pozemský	k2eAgInSc4d1
soubor	soubor	k1gInSc4
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
Klingonů	Klingon	k1gInPc2
domorodé	domorodý	k2eAgInPc4d1
klingonské	klingonský	k2eAgInPc4d1
hudební	hudební	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
bicích	bicí	k2eAgInPc2d1
<g/>
,	,	kIx,
dechových	dechový	k2eAgInPc2d1
a	a	k8xC
smyčcových	smyčcový	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
jejich	jejich	k3xOp3gInSc2
designu	design	k1gInSc2
je	být	k5eAaImIp3nS
Xavier	Xavier	k1gMnSc1
van	vana	k1gFnPc2
Wersh	Wersh	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Publicita	publicita	k1gFnSc1
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
zahrnovala	zahrnovat	k5eAaImAgFnS
přednášky	přednáška	k1gFnPc4
a	a	k8xC
vystoupení	vystoupení	k1gNnSc1
zajišťovaná	zajišťovaný	k2eAgFnSc1d1
Schönfeldem	Schönfeldo	k1gNnSc7
a	a	k8xC
Pozemským	pozemský	k2eAgInSc7d1
souborem	soubor	k1gInSc7
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
Klingonů	Klingon	k1gInPc2
na	na	k7c6
setkáních	setkání	k1gNnPc6
a	a	k8xC
sjezdech	sjezd	k1gInPc6
se	se	k3xPyFc4
sci-fi	sci-fi	k1gFnPc1
tematikou	tematika	k1gFnSc7
i	i	k8xC
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2010	#num#	k4
vyslal	vyslat	k5eAaPmAgInS
Okrand	Okrand	k1gInSc1
jménem	jméno	k1gNnSc7
Souboru	soubor	k1gInSc2
prostřednictvím	prostřednictvím	k7c2
radioteleskopu	radioteleskop	k1gInSc2
CAMRAS	CAMRAS	kA
zprávu	zpráva	k1gFnSc4
směrem	směr	k1gInSc7
na	na	k7c4
hypotetické	hypotetický	k2eAgFnPc4d1
koordináty	koordináta	k1gFnPc4
klingonské	klingonský	k2eAgFnSc2d1
domovské	domovský	k2eAgFnSc2d1
planety	planeta	k1gFnSc2
Qo	Qo	k1gFnSc2
<g/>
'	'	kIx"
<g/>
noS	nos	k1gInSc1
obíhající	obíhající	k2eAgInSc1d1
kolem	kolem	k7c2
hvězdy	hvězda	k1gFnSc2
Arcturus	Arcturus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
zpráva	zpráva	k1gFnSc1
v	v	k7c6
klingonštině	klingonština	k1gFnSc6
obsahovala	obsahovat	k5eAaImAgFnS
pozvání	pozvání	k1gNnSc4
Klingonů	Klingon	k1gInPc2
na	na	k7c4
operu	opera	k1gFnSc4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
pravděpodobně	pravděpodobně	k6eAd1
nedorazila	dorazit	k5eNaPmAgFnS
na	na	k7c4
planetu	planeta	k1gFnSc4
včas	včas	k6eAd1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
Qo	Qo	k1gFnSc1
<g/>
'	'	kIx"
<g/>
nos	nos	k1gInSc1
je	být	k5eAaImIp3nS
vzdálen	vzdálit	k5eAaPmNgInS
36	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
od	od	k7c2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Produkce	produkce	k1gFnSc1
a	a	k8xC
reakce	reakce	k1gFnSc1
</s>
<s>
Henri	Henri	k6eAd1
van	van	k1gInSc1
Zanten	Zanten	k2eAgInSc1d1
coby	coby	k?
„	„	k?
<g/>
Master	master	k1gMnSc1
of	of	k?
the	the	k?
Scream	Scream	k1gInSc1
<g/>
“	“	k?
</s>
<s>
Premiéra	premiéra	k1gFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
divadle	divadlo	k1gNnSc6
Zeebelt	Zeebelta	k1gFnPc2
v	v	k7c6
nizozemském	nizozemský	k2eAgInSc6d1
Haagu	Haag	k1gInSc6
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
(	(	kIx(
<g/>
předcházející	předcházející	k2eAgInSc4d1
den	den	k1gInSc4
se	se	k3xPyFc4
ale	ale	k9
již	již	k6eAd1
konala	konat	k5eAaImAgFnS
ukázka	ukázka	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
opera	opera	k1gFnSc1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
odehrána	odehrát	k5eAaPmNgFnS
celkem	celkem	k6eAd1
čtyřikrát	čtyřikrát	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Nizozemský	nizozemský	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
Henri	Henri	k1gNnSc2
van	van	k1gInSc1
Zanten	Zantno	k1gNnPc2
v	v	k7c6
ní	on	k3xPp3gFnSc6
působil	působit	k5eAaImAgMnS
jako	jako	k8xS,k8xC
vypravěč	vypravěč	k1gMnSc1
coby	coby	k?
tzv.	tzv.	kA
„	„	k?
<g/>
Master	master	k1gMnSc1
of	of	k?
the	the	k?
Scream	Scream	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Opera	opera	k1gFnSc1
byla	být	k5eAaImAgFnS
produkována	produkovat	k5eAaImNgFnS
divadlem	divadlo	k1gNnSc7
Zeebelt	Zeebeltum	k1gNnPc2
a	a	k8xC
Pozemským	pozemský	k2eAgInSc7d1
souborem	soubor	k1gInSc7
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
Klingonů	Klingon	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reakce	reakce	k1gFnPc1
publika	publikum	k1gNnSc2
byly	být	k5eAaImAgFnP
nadšené	nadšený	k2eAgFnPc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
premiéra	premiér	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
i	i	k9
Marc	Marc	k1gFnSc4
Okrand	Okranda	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
vyprodaná	vyprodaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
téhož	týž	k3xTgInSc2
měsíce	měsíc	k1gInSc2
byla	být	k5eAaImAgFnS
opera	opera	k1gFnSc1
reprízována	reprízovat	k5eAaImNgFnS
na	na	k7c6
setkání	setkání	k1gNnSc6
fanklubu	fanklub	k1gInSc2
Star	Star	kA
Treku	Treko	k1gNnSc6
nazvaném	nazvaný	k2eAgNnSc6d1
„	„	k?
<g/>
Qetlop	Qetlop	k1gInSc1
<g/>
“	“	k?
konaném	konaný	k2eAgInSc6d1
na	na	k7c4
Farnsberg	Farnsberg	k1gInSc4
poblíž	poblíž	k7c2
Bad	Bad	k1gFnSc2
Brückenau	Brückenaus	k1gInSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Dalšího	další	k2eAgNnSc2d1
ztvárnění	ztvárnění	k1gNnSc2
se	se	k3xPyFc4
dočkala	dočkat	k5eAaPmAgFnS
v	v	k7c6
divadle	divadlo	k1gNnSc6
Frascati	Frascati	k1gFnSc2
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	s	k7c7
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
objevila	objevit	k5eAaPmAgFnS
na	na	k7c6
operním	operní	k2eAgInSc6d1
festivalu	festival	k1gInSc6
Voi-Z	Voi-Z	k1gFnSc2
ve	v	k7c6
Zwolli	Zwolle	k1gFnSc6
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
na	na	k7c6
hudebním	hudební	k2eAgInSc6d1
festivalu	festival	k1gInSc6
Huygens	Huygens	k1gInSc1
v	v	k7c6
Leidschendamu	Leidschendam	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příběh	příběh	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
Bat	Bat	k?
<g/>
'	'	kIx"
<g/>
leth	leth	k1gMnSc1
</s>
<s>
Kahless	Kahless	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
lovu	lov	k1gInSc6
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
Morathem	Morath	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozezlí	rozezlít	k5eAaPmIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
když	když	k8xS
vinou	vina	k1gFnSc7
Moratha	Morath	k1gMnSc2
přijde	přijít	k5eAaPmIp3nS
o	o	k7c4
svou	svůj	k3xOyFgFnSc4
kořist	kořist	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morath	Morath	k1gInSc1
touží	toužit	k5eAaImIp3nP
po	po	k7c6
pomstě	pomsta	k1gFnSc6
za	za	k7c4
tuto	tento	k3xDgFnSc4
potupu	potupa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyran	tyran	k1gMnSc1
Molor	Molor	k1gMnSc1
Morathovi	Morathův	k2eAgMnPc1d1
nabídne	nabídnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
dosadí	dosadit	k5eAaPmIp3nS
do	do	k7c2
čela	čelo	k1gNnSc2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
domu	dům	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
zradí	zradit	k5eAaPmIp3nS
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
přijímá	přijímat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Molorovými	Molorův	k2eAgMnPc7d1
muži	muž	k1gMnPc7
vtrhne	vtrhnout	k5eAaPmIp3nS
do	do	k7c2
otcova	otcův	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
,	,	kIx,
zmocní	zmocnit	k5eAaPmIp3nP
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
meče	meč	k1gInSc2
a	a	k8xC
požaduje	požadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dům	dům	k1gInSc1
vzdal	vzdát	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
to	ten	k3xDgNnSc4
otec	otec	k1gMnSc1
odmítne	odmítnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
Morath	Morath	k1gMnSc1
jej	on	k3xPp3gMnSc4
brutálně	brutálně	k6eAd1
zavraždí	zavraždit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kahless	Kahless	k1gInSc1
přísahá	přísahat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
obnoví	obnovit	k5eAaPmIp3nS
otcovu	otcův	k2eAgFnSc4d1
čest	čest	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pronásleduje	pronásledovat	k5eAaImIp3nS
bratra	bratr	k1gMnSc4
až	až	k9
k	k	k7c3
sopce	sopka	k1gFnSc3
Kri	Kri	k1gFnSc2
<g/>
'	'	kIx"
<g/>
stak	stak	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzájemném	vzájemný	k2eAgInSc6d1
souboji	souboj	k1gInSc6
Morath	Morath	k1gInSc4
sám	sám	k3xTgMnSc1
do	do	k7c2
sopky	sopka	k1gFnSc2
skočí	skočit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kahless	Kahless	k1gInSc1
tam	tam	k6eAd1
vytvoří	vytvořit	k5eAaPmIp3nS
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
vlasů	vlas	k1gInPc2
vůbec	vůbec	k9
první	první	k4xOgFnSc1
bat	bat	k?
<g/>
'	'	kIx"
<g/>
leth	leth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
Zarmoucený	zarmoucený	k2eAgInSc1d1
Kahless	Kahless	k1gInSc1
podnikne	podniknout	k5eAaPmIp3nS
cestu	cesta	k1gFnSc4
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
setká	setkat	k5eAaPmIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
otcem	otec	k1gMnSc7
a	a	k8xC
odpustí	odpustit	k5eAaPmIp3nS
svému	svůj	k3xOyFgMnSc3
bratrovi	bratr	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukáže	ukázat	k5eAaPmIp3nS
jim	on	k3xPp3gMnPc3
bojové	bojový	k2eAgNnSc1d1
umění	umění	k1gNnSc1
mok	moka	k1gFnPc2
<g/>
'	'	kIx"
<g/>
bara	barum	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
jim	on	k3xPp3gMnPc3
umožní	umožnit	k5eAaPmIp3nS
znovu	znovu	k6eAd1
získat	získat	k5eAaPmF
svá	svůj	k3xOyFgNnPc4
těla	tělo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kotar	Kotar	k1gMnSc1
<g/>
,	,	kIx,
strážce	strážce	k1gMnSc1
podsvětí	podsvětí	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozzuří	rozzuřit	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
dvě	dva	k4xCgFnPc4
duše	duše	k1gFnPc4
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kahless	Kahless	k1gInSc1
shromáždí	shromáždět	k5eAaImIp3nS,k5eAaPmIp3nS
válečníky	válečník	k1gMnPc4
na	na	k7c4
povstání	povstání	k1gNnSc4
proti	proti	k7c3
Molorovi	Molor	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potkává	potkávat	k5eAaImIp3nS
také	také	k9
svou	svůj	k3xOyFgFnSc4
pravou	pravý	k2eAgFnSc4d1
lásku	láska	k1gFnSc4
Lukaru	Lukar	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
mu	on	k3xPp3gMnSc3
pomůže	pomoct	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c4
ně	on	k3xPp3gInPc4
zaútočí	zaútočit	k5eAaPmIp3nP
Molorovi	Molorův	k2eAgMnPc1d1
muži	muž	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
je	on	k3xPp3gMnPc4
porazí	porazit	k5eAaPmIp3nP
a	a	k8xC
stvrdí	stvrdit	k5eAaPmIp3nP
svou	svůj	k3xOyFgFnSc4
lásku	láska	k1gFnSc4
v	v	k7c6
krvi	krev	k1gFnSc6
svých	svůj	k3xOyFgMnPc2
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dějství	dějství	k1gNnSc6
</s>
<s>
Armády	armáda	k1gFnPc1
se	se	k3xPyFc4
shromáždily	shromáždit	k5eAaPmAgFnP
u	u	k7c2
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kahless	Kahless	k1gInSc1
své	svůj	k3xOyFgMnPc4
válečníky	válečník	k1gMnPc4
povzbudí	povzbudit	k5eAaPmIp3nP
strhující	strhující	k2eAgFnPc1d1
řečí	řeč	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dorazí	dorazit	k5eAaPmIp3nS
tam	tam	k6eAd1
také	také	k9
Kotar	Kotar	k1gMnSc1
a	a	k8xC
řeč	řeč	k1gFnSc1
na	na	k7c4
něj	on	k3xPp3gMnSc4
rovněž	rovněž	k9
silně	silně	k6eAd1
zapůsobí	zapůsobit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhlasí	souhlasit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zúčastní	zúčastnit	k5eAaPmIp3nP
boje	boj	k1gInPc1
a	a	k8xC
že	že	k8xS
vytvoří	vytvořit	k5eAaPmIp3nS
ráj	ráj	k1gInSc4
pro	pro	k7c4
klingonské	klingonský	k2eAgMnPc4d1
válečníky	válečník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kahless	Kahless	k1gInSc1
pak	pak	k6eAd1
bojuje	bojovat	k5eAaImIp3nS
po	po	k7c6
boku	bok	k1gInSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
a	a	k8xC
bratra	bratr	k1gMnSc2
proti	proti	k7c3
svým	svůj	k3xOyFgMnPc3
nepřátelům	nepřítel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
dva	dva	k4xCgMnPc1
později	pozdě	k6eAd2
zemřou	zemřít	k5eAaPmIp3nP
čestnou	čestný	k2eAgFnSc7d1
smrtí	smrt	k1gFnSc7
a	a	k8xC
Kahlessův	Kahlessův	k2eAgInSc1d1
rituální	rituální	k2eAgInSc1d1
řev	řev	k1gInSc1
je	být	k5eAaImIp3nS
pošle	pošle	k6eAd1
do	do	k7c2
ráje	ráj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k8xC
Kahless	Kahless	k1gInSc1
bojuje	bojovat	k5eAaImIp3nS
s	s	k7c7
Molorem	Molor	k1gInSc7
a	a	k8xC
zabije	zabít	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
vyřízne	vyříznout	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
srdce	srdce	k1gNnSc1
a	a	k8xC
vypere	vyprat	k5eAaPmIp3nS
jej	on	k3xPp3gMnSc4
v	v	k7c6
řece	řeka	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
obnovil	obnovit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gFnSc4
čest	čest	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
pomocí	pomoc	k1gFnSc7
Lukary	Lukara	k1gFnSc2
pak	pak	k6eAd1
spáchá	spáchat	k5eAaPmIp3nS
rituální	rituální	k2eAgFnSc4d1
sebevraždu	sebevražda	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gInSc4
řev	řev	k1gInSc4
jej	on	k3xPp3gMnSc4
odešle	odeslat	k5eAaPmIp3nS
do	do	k7c2
ráje	ráj	k1gInSc2
za	za	k7c7
bratrem	bratr	k1gMnSc7
a	a	k8xC
otcem	otec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klingonský	Klingonský	k2eAgInSc1d1
lid	lid	k1gInSc1
je	být	k5eAaImIp3nS
sjednocen	sjednocen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Role	role	k1gFnPc4
a	a	k8xC
původní	původní	k2eAgNnPc4d1
obsazení	obsazení	k1gNnPc4
</s>
<s>
Master	master	k1gMnSc1
of	of	k?
the	the	k?
Scream	Scream	k1gInSc1
(	(	kIx(
<g/>
vypravěč	vypravěč	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
Henri	Henr	k1gFnSc3
van	van	k1gInSc4
Zanten	Zanten	k2eAgInSc4d1
</s>
<s>
Kahless	Kahless	k1gInSc1
–	–	k?
Taru	Tar	k2eAgFnSc4d1
Huotari	Huotare	k1gFnSc4
</s>
<s>
Kotar	Kotar	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
a	a	k8xC
Molor	Molor	k1gMnSc1
–	–	k?
Ben	Ben	k1gInSc1
Kropp	Kropp	k1gInSc1
</s>
<s>
Morath	Morath	k1gInSc1
a	a	k8xC
Lukara	Lukara	k1gFnSc1
–	–	k?
Jeannette	Jeannett	k1gMnSc5
Huizinga	Huizing	k1gMnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
BERKOWITZ	BERKOWITZ	kA
<g/>
,	,	kIx,
Ben	Ben	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klingon	Klingon	k1gInSc4
opera	opera	k1gFnSc1
prepares	preparesa	k1gFnPc2
for	forum	k1gNnPc2
interstellar	interstellar	k1gMnSc1
debut	debut	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redigoval	redigovat	k5eAaImAgMnS
Paul	Paul	k1gMnSc1
Casciato	Casciat	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHIGA	SHIGA	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
It	It	k1gFnSc1
ain	ain	k?
<g/>
'	'	kIx"
<g/>
t	t	k?
over	over	k1gMnSc1
till	till	k1gMnSc1
the	the	k?
fat	fatum	k1gNnPc2
Klingon	Klingona	k1gFnPc2
sings	singsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Scientist	Scientist	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Klingon	Klingon	k1gMnSc1
Language	language	k1gFnSc4
Institute	institut	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SILVER	SILVER	kA
<g/>
,	,	kIx,
Susan	Susan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klingon	Klingona	k1gFnPc2
Opera	opera	k1gFnSc1
Comes	Comes	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Netherlands	Netherlands	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gather	Gathra	k1gFnPc2
Entertainment	Entertainment	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-08-31	2010-08-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
DutchDailyNews	DutchDailyNewsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
First	First	k1gMnSc1
Klingon	Klingon	k1gMnSc1
opera	opera	k1gFnSc1
set	sto	k4xCgNnPc2
to	ten	k3xDgNnSc4
launch	launch	k1gMnSc1
in	in	k?
the	the	k?
Netherlands	Netherlands	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dutch	Dutch	k1gInSc1
Daily	Daila	k1gFnSc2
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
About	About	k1gInSc1
the	the	k?
project	project	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
the	the	k?
Opera	opera	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JACOBSON	JACOBSON	kA
<g/>
,	,	kIx,
Aileen	Aileen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gFnSc1
Otherworldly	Otherworldly	k1gFnSc1
Opera	opera	k1gFnSc1
That	That	k1gInSc4
Speaks	Speaks	k1gInSc1
Klingon	Klingon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-11-07	2008-11-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PELLEGRINELLI	PELLEGRINELLI	kA
<g/>
,	,	kIx,
Lara	Larum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Fat	fatum	k1gNnPc2
Alien	Alien	k2eAgInSc1d1
Sings	Sings	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Klingon-Language	Klingon-Language	k1gFnSc1
Opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPR	NPR	kA
Music	Musice	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-05-07	2009-05-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MICHAELS	MICHAELS	kA
<g/>
,	,	kIx,
Sean	Sean	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
First	First	k1gFnSc1
Klingon	Klingona	k1gFnPc2
opera	opera	k1gFnSc1
lifts	lifts	k1gInSc1
off	off	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HART	HART	kA
<g/>
,	,	kIx,
Hugh	Hugh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klingon	Klingona	k1gFnPc2
Opera	opera	k1gFnSc1
Ramps	Ramps	k1gInSc1
Up	Up	k1gFnSc2
for	forum	k1gNnPc2
Earth-Bound	Earth-Bound	k1gMnSc1
Premiere	Premier	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wired	Wired	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
u-theopera-cast-crew	u-theopera-cast-crew	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Odkaz	odkaz	k1gInSc1
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
↑	↑	k?
Qapla	Qapla	k1gMnSc1
<g/>
’	’	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
We	We	k1gFnSc1
have	have	k1gFnSc1
done	don	k1gMnSc5
it.	it.	k?
U	u	k7c2
the	the	k?
Opera	opera	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Time	Time	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnPc4
mighty	mighta	k1gFnSc2
warriors	warriors	k6eAd1
who	who	k?
have	havat	k5eAaPmIp3nS
made	made	k1gFnSc1
‘	‘	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
possible	possible	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klingon	Klingon	k1gNnSc1
Terran	Terrana	k1gFnPc2
Research	Researcha	k1gFnPc2
Ensemble	ensemble	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-06-27	2008-06-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
first	first	k1gInSc1
Klingon	Klingon	k1gInSc1
opera	opera	k1gFnSc1
premieres	premieres	k1gMnSc1
in	in	k?
the	the	k?
Netherlands	Netherlands	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opera	opera	k1gFnSc1
Now	Now	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
načtení	načtení	k1gNnSc6
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
zadat	zadat	k5eAaPmF
do	do	k7c2
pole	pole	k1gNnSc2
„	„	k?
<g/>
Search	Search	k1gMnSc1
news	ws	k6eNd1
<g/>
“	“	k?
slovo	slovo	k1gNnSc4
„	„	k?
<g/>
Klingon	Klingon	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Frank	Frank	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marc	Marc	k1gFnSc1
Okrand	Okranda	k1gFnPc2
speciale	speciale	k6eAd1
gast	gast	k5eAaPmF
bij	bít	k5eAaImRp2nS
Klingon	Klingon	k1gMnSc1
Opera	opera	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Star	star	k1gFnSc1
Trek	Treka	k1gFnPc2
Fanclub	Fancluba	k1gFnPc2
The	The	k1gFnSc2
Flying	Flying	k1gInSc1
Dutch	Dutch	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-09-07	2010-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
nizozemsky	nizozemsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TYLER	TYLER	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dutch	Dutch	k1gMnSc1
first	first	k1gMnSc1
to	ten	k3xDgNnSc4
hear	hear	k1gMnSc1
Klingon	Klingon	k1gMnSc1
opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radio	radio	k1gNnSc1
Netherlands	Netherlandsa	k1gFnPc2
Worldwide	Worldwid	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-09-10	2010-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PELLEGRINELLI	PELLEGRINELLI	kA
<g/>
,	,	kIx,
Lara	Larum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
First	First	k1gFnSc1
'	'	kIx"
<g/>
Authentic	Authentice	k1gFnPc2
<g/>
'	'	kIx"
Klingon	Klingon	k1gInSc1
Opera	opera	k1gFnSc1
By	by	k9
Terrans	Terrans	k1gInSc1
(	(	kIx(
<g/>
That	That	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
You	You	k1gFnSc7
<g/>
,	,	kIx,
Earthlings	Earthlings	k1gInSc1
<g/>
)	)	kIx)
Premieres	Premieres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPR	NPR	kA
Music	Musice	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
A	a	k8xC
Klingon	Klingon	k1gMnSc1
opera	opera	k1gFnSc1
in	in	k?
three	threat	k5eAaPmIp3nS
acts	acts	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frascati	Frascati	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
'	'	kIx"
<g/>
U	u	k7c2
<g/>
'	'	kIx"
<g/>
,	,	kIx,
een	een	k?
Klingon	Klingon	k1gInSc1
Opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Huygens	Huygens	k1gInSc1
Muziekfestival	Muziekfestival	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
nizozemsky	nizozemsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Star	Star	kA
Trek	Treka	k1gFnPc2
Televizní	televizní	k2eAgFnSc1d1
a	a	k8xC
filmováprodukce	filmováprodukce	k1gFnSc1
(	(	kIx(
<g/>
kánon	kánon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Seriály	seriál	k1gInPc1
</s>
<s>
Star	Star	kA
Trek	Trek	k1gMnSc1
(	(	kIx(
<g/>
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Star	Star	kA
Trek	Trek	k1gInSc1
(	(	kIx(
<g/>
animovaný	animovaný	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
;	;	kIx,
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
(	(	kIx(
<g/>
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Stanice	stanice	k1gFnSc1
Deep	Deep	k1gInSc1
Space	Space	k1gMnSc1
Nine	Nine	k1gInSc1
(	(	kIx(
<g/>
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Vesmírná	vesmírný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Voyager	Voyagra	k1gFnPc2
(	(	kIx(
<g/>
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Enterprise	Enterprise	k1gFnSc1
(	(	kIx(
<g/>
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Discovery	Discovera	k1gFnSc2
(	(	kIx(
<g/>
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Short	Short	k1gInSc1
Treks	Treks	k1gInSc1
•	•	k?
Picard	Picard	k1gInSc1
(	(	kIx(
<g/>
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Lower	Lower	k1gInSc1
Decks	Decks	k1gInSc1
(	(	kIx(
<g/>
díly	díl	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Prodigy	Prodiga	k1gFnSc2
•	•	k?
Strange	Strang	k1gFnSc2
New	New	k1gMnPc1
Worlds	Worldsa	k1gFnPc2
Filmy	film	k1gInPc4
</s>
<s>
Původní	původní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
Film	film	k1gInSc1
•	•	k?
II	II	kA
<g/>
:	:	kIx,
Khanův	Khanův	k2eAgInSc1d1
hněv	hněv	k1gInSc1
•	•	k?
III	III	kA
<g/>
:	:	kIx,
Pátrání	pátrání	k1gNnSc1
po	po	k7c6
Spockovi	Spocek	k1gMnSc6
•	•	k?
IV	IV	kA
<g/>
:	:	kIx,
Cesta	cesta	k1gFnSc1
domů	dům	k1gInPc2
•	•	k?
V	V	kA
<g/>
:	:	kIx,
Nejzazší	zadní	k2eAgFnSc2d3
hranice	hranice	k1gFnSc2
•	•	k?
VI	VI	kA
<g/>
:	:	kIx,
Neobjevená	objevený	k2eNgFnSc1d1
země	země	k1gFnSc1
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
</s>
<s>
Generace	generace	k1gFnSc1
•	•	k?
První	první	k4xOgInSc1
kontakt	kontakt	k1gInSc1
•	•	k?
Vzpoura	vzpoura	k1gFnSc1
•	•	k?
Nemesis	Nemesis	k1gFnSc2
Reboot	Reboot	k1gMnSc1
</s>
<s>
Star	Star	kA
Trek	Trek	k1gMnSc1
•	•	k?
Do	do	k7c2
temnoty	temnota	k1gFnSc2
•	•	k?
Do	do	k7c2
neznáma	neznámo	k1gNnSc2
</s>
<s>
Prostředí	prostředí	k1gNnSc1
</s>
<s>
Rasy	rasa	k1gFnPc1
</s>
<s>
8472	#num#	k4
•	•	k?
Bajorané	Bajoraný	k2eAgFnSc2d1
•	•	k?
Cardassiané	Cardassianá	k1gFnSc2
•	•	k?
Ferengové	Ferengový	k2eAgNnSc1d1
•	•	k?
Klingoni	Klingon	k1gMnPc1
(	(	kIx(
<g/>
klingonština	klingonština	k1gFnSc1
•	•	k?
bat	bat	k?
<g/>
'	'	kIx"
<g/>
leth	leth	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Kzinti	Kzinť	k1gFnSc2
•	•	k?
Lidé	člověk	k1gMnPc1
•	•	k?
Q	Q	kA
•	•	k?
Romulané	Romulaný	k2eAgFnSc2d1
•	•	k?
Tholiané	Tholianá	k1gFnSc2
•	•	k?
Trillové	Trillová	k1gFnSc2
•	•	k?
Tvůrci	tvůrce	k1gMnSc3
•	•	k?
Vulkánci	Vulkánek	k1gMnPc1
Civilizace	civilizace	k1gFnSc2
</s>
<s>
Spojená	spojený	k2eAgFnSc1d1
federace	federace	k1gFnSc1
planet	planeta	k1gFnPc2
•	•	k?
Borgové	Borgový	k2eAgNnSc1d1
•	•	k?
Dominion	dominion	k1gNnSc1
•	•	k?
Klingonská	Klingonský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
•	•	k?
Romulanské	Romulanský	k2eAgNnSc4d1
impérium	impérium	k1gNnSc4
Plavidla	plavidlo	k1gNnSc2
</s>
<s>
Spojené	spojený	k2eAgFnPc1d1
Země	zem	k1gFnPc1
•	•	k?
Spojené	spojený	k2eAgFnPc1d1
federace	federace	k1gFnPc1
planet	planeta	k1gFnPc2
•	•	k?
Borgů	Borg	k1gInPc2
•	•	k?
Dominionu	Dominion	k1gInSc3
•	•	k?
Klingonské	Klingonský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
•	•	k?
Romulanského	Romulanský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
Technologie	technologie	k1gFnSc2
</s>
<s>
zbraně	zbraň	k1gFnPc1
•	•	k?
maskovací	maskovací	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
•	•	k?
štíty	štít	k1gInPc1
•	•	k?
replikátor	replikátor	k1gInSc1
•	•	k?
simulátor	simulátor	k1gInSc1
•	•	k?
Enterprise	Enterprise	k1gFnSc2
•	•	k?
raketoplán	raketoplán	k1gInSc1
•	•	k?
transportér	transportér	k1gInSc1
•	•	k?
warp	warp	k1gInSc1
pohon	pohon	k1gInSc1
•	•	k?
impulsní	impulsní	k2eAgInSc1d1
pohon	pohon	k1gInSc1
•	•	k?
vlečný	vlečný	k2eAgInSc1d1
paprsek	paprsek	k1gInSc1
•	•	k?
asimilace	asimilace	k1gFnSc2
•	•	k?
podpora	podpora	k1gFnSc1
života	život	k1gInSc2
•	•	k?
Jefferiesův	Jefferiesův	k2eAgInSc1d1
průlez	průlez	k1gInSc1
•	•	k?
trikordér	trikordér	k1gInSc1
•	•	k?
komunikátor	komunikátor	k1gInSc4
Další	další	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
časová	časový	k2eAgFnSc1d1
osa	osa	k1gFnSc1
•	•	k?
hvězdné	hvězdný	k2eAgNnSc4d1
datum	datum	k1gNnSc4
•	•	k?
planety	planeta	k1gFnSc2
•	•	k?
klasifikace	klasifikace	k1gFnSc2
planet	planeta	k1gFnPc2
•	•	k?
kvadranty	kvadrant	k1gInPc1
Galaxie	galaxie	k1gFnSc2
•	•	k?
Zefram	Zefram	k1gInSc1
Cochrane	Cochran	k1gInSc5
•	•	k?
Hvězdná	hvězdný	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
•	•	k?
Základní	základní	k2eAgFnSc1d1
směrnice	směrnice	k1gFnSc1
•	•	k?
„	„	k?
<g/>
Odpor	odpor	k1gInSc1
je	být	k5eAaImIp3nS
marný	marný	k2eAgInSc4d1
<g/>
“	“	k?
•	•	k?
redshirt	redshirt	k1gInSc1
•	•	k?
dilithium	dilithium	k1gNnSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Gene	gen	k1gInSc5
Roddenberry	Roddenberr	k1gInPc4
•	•	k?
Memory	Memora	k1gFnSc2
Alpha	Alpha	k1gMnSc1
•	•	k?
hry	hra	k1gFnSc2
•	•	k?
knihy	kniha	k1gFnSc2
•	•	k?
trekkies	trekkies	k1gInSc1
•	•	k?
Star	Star	kA
Trek	Trek	k1gInSc1
<g/>
:	:	kIx,
Phase	Phase	k1gFnSc1
II	II	kA
(	(	kIx(
<g/>
zrušený	zrušený	k2eAgInSc1d1
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
’	’	k?
<g/>
u	u	k7c2
<g/>
’	’	k?
(	(	kIx(
<g/>
klingonská	klingonský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Star	Star	kA
Wreck	Wreck	k1gInSc1
(	(	kIx(
<g/>
parodie	parodie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Star	Star	kA
Trek	Trek	k1gMnSc1
</s>
