<s>
Václav	Václav	k1gMnSc1	Václav
Čada	Čada	k1gMnSc1	Čada
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1942	[number]	k4	1942
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
poslanec	poslanec	k1gMnSc1	poslanec
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
vojenských	vojenský	k2eAgFnPc2d1	vojenská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
publikoval	publikovat	k5eAaBmAgMnS	publikovat
práce	práce	k1gFnPc4	práce
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
marxismu	marxismus	k1gInSc2	marxismus
a	a	k8xC	a
československé	československý	k2eAgFnSc2d1	Československá
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
estetička	estetička	k1gFnSc1	estetička
a	a	k8xC	a
kunsthistorička	kunsthistorička	k1gFnSc1	kunsthistorička
Věra	Věra	k1gFnSc1	Věra
Beranová	Beranová	k1gFnSc1	Beranová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
za	za	k7c4	za
koalici	koalice	k1gFnSc4	koalice
Levý	levý	k2eAgInSc4d1	levý
blok	blok	k1gInSc4	blok
<g/>
,	,	kIx,	,
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
3	[number]	k4	3
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
v	v	k7c6	v
rozpravě	rozprava	k1gFnSc6	rozprava
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
referoval	referovat	k5eAaBmAgInS	referovat
o	o	k7c6	o
názorech	názor	k1gInPc6	názor
členské	členský	k2eAgFnSc2d1	členská
základny	základna	k1gFnSc2	základna
na	na	k7c4	na
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
názvu	název	k1gInSc2	název
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
sny	sen	k1gInPc4	sen
a	a	k8xC	a
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Dějinami	dějiny	k1gFnPc7	dějiny
k	k	k7c3	k
dnešku	dnešek	k1gInSc3	dnešek
:	:	kIx,	:
(	(	kIx(	(
<g/>
70	[number]	k4	70
let	léto	k1gNnPc2	léto
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Dějiny	dějiny	k1gFnPc1	dějiny
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
období	období	k1gNnSc6	období
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
:	:	kIx,	:
strategie	strategie	k1gFnSc1	strategie
a	a	k8xC	a
taktika	taktika	k1gFnSc1	taktika
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Osvobození	osvobození	k1gNnSc2	osvobození
Československa	Československo	k1gNnSc2	Československo
Sovětskou	sovětský	k2eAgFnSc7d1	sovětská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Slovenské	slovenský	k2eAgNnSc4d1	slovenské
národní	národní	k2eAgNnSc4d1	národní
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
Vznik	vznik	k1gInSc1	vznik
KSČ	KSČ	kA	KSČ
jako	jako	k8xC	jako
revoluční	revoluční	k2eAgFnPc1d1	revoluční
strany	strana	k1gFnPc1	strana
nového	nový	k2eAgInSc2d1	nový
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
Vznik	vznik	k1gInSc1	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
</s>
