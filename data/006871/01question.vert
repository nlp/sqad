<s>
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
<g/>
Václav	Václav	k1gMnSc1	Václav
Čada	Čada	k1gMnSc1	Čada
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
poslanec	poslanec	k1gMnSc1	poslanec
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
KSČM	KSČM	kA	KSČM
<g/>
?	?	kIx.	?
</s>
