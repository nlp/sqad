<p>
<s>
Local	Local	k1gMnSc1	Local
Area	Ares	k1gMnSc2	Ares
Network	network	k1gInSc2	network
(	(	kIx(	(
<g/>
též	též	k9	též
LAN	lano	k1gNnPc2	lano
<g/>
,	,	kIx,	,
lokální	lokální	k2eAgFnSc4d1	lokální
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnSc4d1	místní
síť	síť	k1gFnSc4	síť
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
malé	malý	k2eAgNnSc4d1	malé
geografické	geografický	k2eAgNnSc4d1	geografické
území	území	k1gNnSc4	území
(	(	kIx(	(
<g/>
např.	např.	kA	např.
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnSc2d1	malá
firmy	firma	k1gFnSc2	firma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přenosové	přenosový	k2eAgFnPc1d1	přenosová
rychlosti	rychlost	k1gFnPc1	rychlost
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
řádově	řádově	k6eAd1	řádově
Gb	Gb	k1gFnSc1	Gb
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
<s>
Nejrozšířenějšími	rozšířený	k2eAgFnPc7d3	nejrozšířenější
technologiemi	technologie	k1gFnPc7	technologie
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
LAN	lano	k1gNnPc2	lano
sítích	síť	k1gFnPc6	síť
jsou	být	k5eAaImIp3nP	být
Ethernet	Ethernet	k1gMnSc1	Ethernet
a	a	k8xC	a
Wi-Fi	Wi-F	k1gMnPc1	Wi-F
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
WLAN	WLAN	kA	WLAN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
např.	např.	kA	např.
ARCNET	ARCNET	kA	ARCNET
a	a	k8xC	a
Token	Token	k2eAgInSc4d1	Token
Ring	ring	k1gInSc4	ring
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Sítě	síť	k1gFnPc1	síť
LAN	lano	k1gNnPc2	lano
označují	označovat	k5eAaImIp3nP	označovat
všechny	všechen	k3xTgFnPc4	všechen
malé	malý	k2eAgFnPc4d1	malá
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
mnohdy	mnohdy	k6eAd1	mnohdy
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
sami	sám	k3xTgMnPc1	sám
uživatelé	uživatel	k1gMnPc1	uživatel
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sítě	síť	k1gFnPc4	síť
uvnitř	uvnitř	k7c2	uvnitř
místností	místnost	k1gFnPc2	místnost
<g/>
,	,	kIx,	,
budov	budova	k1gFnPc2	budova
nebo	nebo	k8xC	nebo
malých	malý	k2eAgInPc2d1	malý
areálů	areál	k1gInPc2	areál
<g/>
;	;	kIx,	;
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
i	i	k8xC	i
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	on	k3xPp3gMnPc4	on
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
levná	levný	k2eAgFnSc1d1	levná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
až	až	k9	až
desítky	desítka	k1gFnPc1	desítka
Gbit	Gbita	k1gFnPc2	Gbita
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
pořizují	pořizovat	k5eAaImIp3nP	pořizovat
sami	sám	k3xTgMnPc1	sám
majitelé	majitel	k1gMnPc1	majitel
propojených	propojený	k2eAgInPc2d1	propojený
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
snadnému	snadný	k2eAgNnSc3d1	snadné
sdílení	sdílení	k1gNnSc3	sdílení
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
LAN	lano	k1gNnPc2	lano
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
podíl	podíl	k1gInSc1	podíl
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
v	v	k7c6	v
LAN	lano	k1gNnPc2	lano
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
sdílení	sdílení	k1gNnSc4	sdílení
diskového	diskový	k2eAgInSc2d1	diskový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
LAN	lano	k1gNnPc2	lano
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využívat	využívat	k5eAaImF	využívat
tiskáren	tiskárna	k1gFnPc2	tiskárna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
počítačům	počítač	k1gInPc3	počítač
nebo	nebo	k8xC	nebo
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
sdílet	sdílet	k5eAaImF	sdílet
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Internetu	Internet	k1gInSc3	Internet
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
návazných	návazný	k2eAgFnPc2d1	návazná
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
WWW	WWW	kA	WWW
<g/>
,	,	kIx,	,
E-mail	eail	k1gInSc1	e-mail
<g/>
,	,	kIx,	,
Peer-to-peer	Peeroeer	k1gInSc1	Peer-to-peer
sítě	síť	k1gFnSc2	síť
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
aktivních	aktivní	k2eAgInPc2d1	aktivní
a	a	k8xC	a
pasivních	pasivní	k2eAgInPc2d1	pasivní
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInPc1d1	aktivní
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
switch	switch	k1gMnSc1	switch
<g/>
,	,	kIx,	,
router	router	k1gMnSc1	router
<g/>
,	,	kIx,	,
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
apod.	apod.	kA	apod.
Pasivní	pasivní	k2eAgInPc4d1	pasivní
prvky	prvek	k1gInPc4	prvek
jsou	být	k5eAaImIp3nP	být
součásti	součást	k1gFnPc1	součást
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
komunikaci	komunikace	k1gFnSc4	komunikace
podílejí	podílet	k5eAaImIp3nP	podílet
pouze	pouze	k6eAd1	pouze
pasivně	pasivně	k6eAd1	pasivně
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
napájení	napájení	k1gNnSc4	napájení
<g/>
)	)	kIx)	)
–	–	k?	–
propojovací	propojovací	k2eAgInSc1d1	propojovací
kabel	kabel	k1gInSc1	kabel
(	(	kIx(	(
<g/>
strukturovaná	strukturovaný	k2eAgFnSc1d1	strukturovaná
kabeláž	kabeláž	k1gFnSc1	kabeláž
<g/>
,	,	kIx,	,
optické	optický	k2eAgNnSc1d1	optické
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
,	,	kIx,	,
koaxiální	koaxiální	k2eAgInSc1d1	koaxiální
kabel	kabel	k1gInSc1	kabel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konektory	konektor	k1gInPc1	konektor
<g/>
,	,	kIx,	,
u	u	k7c2	u
sítí	síť	k1gFnPc2	síť
Token	Token	k2eAgInSc4d1	Token
Ring	ring	k1gInSc4	ring
i	i	k9	i
pasivní	pasivní	k2eAgMnPc1d1	pasivní
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opačným	opačný	k2eAgInSc7d1	opačný
protipólem	protipól	k1gInSc7	protipól
k	k	k7c3	k
sítím	síť	k1gFnPc3	síť
LAN	lano	k1gNnPc2	lano
jsou	být	k5eAaImIp3nP	být
sítě	síť	k1gFnPc1	síť
WAN	WAN	kA	WAN
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
přenosovou	přenosový	k2eAgFnSc7d1	přenosová
kapacitu	kapacita	k1gFnSc4	kapacita
si	se	k3xPyFc3	se
uživatelé	uživatel	k1gMnPc1	uživatel
pronajímají	pronajímat	k5eAaImIp3nP	pronajímat
od	od	k7c2	od
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
přenosová	přenosový	k2eAgFnSc1d1	přenosová
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
LAN	lano	k1gNnPc2	lano
drahá	drahý	k2eAgFnSc1d1	drahá
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
mezi	mezi	k7c7	mezi
sítěmi	síť	k1gFnPc7	síť
LAN	lano	k1gNnPc2	lano
a	a	k8xC	a
WAN	WAN	kA	WAN
najdeme	najít	k5eAaPmIp1nP	najít
sítě	síť	k1gFnSc2	síť
MAN	mana	k1gFnPc2	mana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Od	od	k7c2	od
historie	historie	k1gFnSc2	historie
k	k	k7c3	k
současnosti	současnost	k1gFnSc3	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc4	první
sítě	síť	k1gFnPc4	síť
LAN	lano	k1gNnPc2	lano
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Sloužily	sloužit	k5eAaImAgFnP	sloužit
k	k	k7c3	k
vysokorychlostnímu	vysokorychlostní	k2eAgNnSc3d1	vysokorychlostní
propojení	propojení	k1gNnSc3	propojení
sálových	sálový	k2eAgInPc2d1	sálový
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
existovalo	existovat	k5eAaImAgNnS	existovat
mnoho	mnoho	k4c1	mnoho
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
navzájem	navzájem	k6eAd1	navzájem
nebyly	být	k5eNaImAgInP	být
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
(	(	kIx(	(
<g/>
ARCNET	ARCNET	kA	ARCNET
<g/>
,	,	kIx,	,
DECnet	DECnet	k1gInSc4	DECnet
<g/>
,	,	kIx,	,
Token	Token	k2eAgInSc4d1	Token
ring	ring	k1gInSc4	ring
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
LAN	lano	k1gNnPc2	lano
sítě	síť	k1gFnSc2	síť
vystavěné	vystavěný	k2eAgFnSc2d1	vystavěná
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
technologie	technologie	k1gFnSc2	technologie
Ethernet	Etherneta	k1gFnPc2	Etherneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
(	(	kIx(	(
<g/>
PC	PC	kA	PC
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozmachu	rozmach	k1gInSc3	rozmach
budování	budování	k1gNnSc4	budování
LAN	lano	k1gNnPc2	lano
sítí	síť	k1gFnPc2	síť
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
firma	firma	k1gFnSc1	firma
Novell	Novell	kA	Novell
uvedla	uvést	k5eAaPmAgFnS	uvést
svůj	svůj	k3xOyFgInSc4	svůj
produkt	produkt	k1gInSc4	produkt
NetWare	NetWar	k1gInSc5	NetWar
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Novell	Novell	kA	Novell
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
odsunuta	odsunut	k2eAgFnSc1d1	odsunuta
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
trhu	trh	k1gInSc2	trh
nástupem	nástup	k1gInSc7	nástup
firmy	firma	k1gFnSc2	firma
Microsoft	Microsoft	kA	Microsoft
s	s	k7c7	s
produkty	produkt	k1gInPc7	produkt
Windows	Windows	kA	Windows
for	forum	k1gNnPc2	forum
Workgroups	Workgroups	k1gInSc4	Workgroups
a	a	k8xC	a
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
sítě	síť	k1gFnSc2	síť
LAN	lano	k1gNnPc2	lano
s	s	k7c7	s
osobními	osobní	k2eAgInPc7d1	osobní
počítači	počítač	k1gInPc7	počítač
používaly	používat	k5eAaImAgFnP	používat
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
rodinu	rodina	k1gFnSc4	rodina
protokolů	protokol	k1gInPc2	protokol
IPX	IPX	kA	IPX
<g/>
/	/	kIx~	/
<g/>
SPX	SPX	kA	SPX
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
NETBEUI	NETBEUI	kA	NETBEUI
<g/>
,	,	kIx,	,
AppleTalk	AppleTalk	k1gInSc1	AppleTalk
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
specializované	specializovaný	k2eAgInPc1d1	specializovaný
proprietární	proprietární	k2eAgInPc1d1	proprietární
protokoly	protokol	k1gInPc1	protokol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
WWW	WWW	kA	WWW
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
rodinou	rodina	k1gFnSc7	rodina
protokolů	protokol	k1gInPc2	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Moderní	moderní	k2eAgInPc4d1	moderní
prvky	prvek	k1gInPc4	prvek
LAN	lano	k1gNnPc2	lano
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
sítích	síť	k1gFnPc6	síť
dnes	dnes	k6eAd1	dnes
nalézáme	nalézat	k5eAaImIp1nP	nalézat
pokročilé	pokročilý	k2eAgFnPc1d1	pokročilá
technologie	technologie	k1gFnPc1	technologie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
jejich	jejich	k3xOp3gFnSc4	jejich
propustnost	propustnost	k1gFnSc4	propustnost
a	a	k8xC	a
variabilitu	variabilita	k1gFnSc4	variabilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
propojovací	propojovací	k2eAgInPc1d1	propojovací
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
opakovač	opakovač	k1gInSc1	opakovač
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
HUB	houba	k1gFnPc2	houba
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nahrazovány	nahrazovat	k5eAaImNgFnP	nahrazovat
inteligentními	inteligentní	k2eAgNnPc7d1	inteligentní
zařízeními	zařízení	k1gNnPc7	zařízení
(	(	kIx(	(
<g/>
bridge	bridge	k1gNnSc1	bridge
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
switch	switch	k1gInSc1	switch
<g/>
,	,	kIx,	,
router	router	k1gInSc1	router
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
kolize	kolize	k1gFnPc4	kolize
<g/>
,	,	kIx,	,
omezují	omezovat	k5eAaImIp3nP	omezovat
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
broadcasty	broadcast	k1gInPc4	broadcast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
monitorování	monitorování	k1gNnSc1	monitorování
<g/>
,	,	kIx,	,
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
pokročilé	pokročilý	k2eAgInPc1d1	pokročilý
zásahy	zásah	k1gInPc1	zásah
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
např.	např.	kA	např.
detekce	detekce	k1gFnSc1	detekce
DoS	DoS	k1gFnSc1	DoS
<g/>
,	,	kIx,	,
filtrování	filtrování	k1gNnSc1	filtrování
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VLAN	VLAN	kA	VLAN
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
na	na	k7c6	na
logické	logický	k2eAgFnSc3d1	logická
úrovni	úroveň	k1gFnSc3	úroveň
síť	síť	k1gFnSc4	síť
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
(	(	kIx(	(
<g/>
virtuální	virtuální	k2eAgFnPc4d1	virtuální
<g/>
)	)	kIx)	)
podsítě	podsíť	k1gFnPc4	podsíť
a	a	k8xC	a
oddělit	oddělit	k5eAaPmF	oddělit
tak	tak	k9	tak
jejich	jejich	k3xOp3gInSc4	jejich
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WLAN	WLAN	kA	WLAN
(	(	kIx(	(
<g/>
Pozor	pozor	k1gInSc1	pozor
<g/>
,	,	kIx,	,
neplést	plést	k5eNaImF	plést
s	s	k7c7	s
VLAN	VLAN	kA	VLAN
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
Wi-Fi	Wi-Fi	k1gNnSc1	Wi-Fi
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
LAN	lano	k1gNnPc2	lano
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
vzduchem	vzduch	k1gInSc7	vzduch
(	(	kIx(	(
<g/>
ne	ne	k9	ne
po	po	k7c6	po
drátech	drát	k1gInPc6	drát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Ethernetu	Ethernet	k1gInSc2	Ethernet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VPN	VPN	kA	VPN
(	(	kIx(	(
<g/>
virtuální	virtuální	k2eAgFnSc1d1	virtuální
privátní	privátní	k2eAgFnSc1d1	privátní
síť	síť	k1gFnSc1	síť
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
propojení	propojení	k1gNnSc1	propojení
počítačů	počítač	k1gInPc2	počítač
nebo	nebo	k8xC	nebo
celých	celý	k2eAgFnPc2d1	celá
sítí	síť	k1gFnPc2	síť
zpravidla	zpravidla	k6eAd1	zpravidla
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
větší	veliký	k2eAgFnSc2d2	veliký
obecně	obecně	k6eAd1	obecně
nedůvěryhodné	důvěryhodný	k2eNgFnSc2d1	nedůvěryhodná
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
Internetu	Internet	k1gInSc2	Internet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
propojení	propojení	k1gNnSc1	propojení
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
počítačům	počítač	k1gMnPc3	počítač
transparentní	transparentní	k2eAgFnSc2d1	transparentní
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
i	i	k9	i
v	v	k7c6	v
geograficky	geograficky	k6eAd1	geograficky
vzdálených	vzdálený	k2eAgFnPc6d1	vzdálená
lokalitách	lokalita	k1gFnPc6	lokalita
komunikovat	komunikovat	k5eAaImF	komunikovat
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
jedné	jeden	k4xCgFnSc2	jeden
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IEEE	IEEE	kA	IEEE
802.1	[number]	k4	802.1
<g/>
X	X	kA	X
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
autorizovat	autorizovat	k5eAaBmF	autorizovat
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
připojuje	připojovat	k5eAaImIp3nS	připojovat
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
nebo	nebo	k8xC	nebo
odepřít	odepřít	k5eAaPmF	odepřít
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
LAN	lano	k1gNnPc2	lano
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
–	–	k?	–
celkový	celkový	k2eAgInSc4d1	celkový
přehled	přehled	k1gInSc4	přehled
</s>
</p>
<p>
<s>
Metropolitan	metropolitan	k1gInSc4	metropolitan
Area	Ares	k1gMnSc2	Ares
Network	network	k1gInSc2	network
(	(	kIx(	(
<g/>
MAN	Man	k1gMnSc1	Man
<g/>
)	)	kIx)	)
–	–	k?	–
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
Wide	Wid	k1gInPc1	Wid
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
WAN	WAN	kA	WAN
<g/>
)	)	kIx)	)
–	–	k?	–
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
Síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
</s>
</p>
<p>
<s>
Ethernet	Ethernet	k1gMnSc1	Ethernet
</s>
</p>
<p>
<s>
Regional	Regionat	k5eAaImAgMnS	Regionat
Area	Ares	k1gMnSc2	Ares
Network	network	k1gInPc2	network
(	(	kIx(	(
<g/>
RAN	Rana	k1gFnPc2	Rana
<g/>
)	)	kIx)	)
–	–	k?	–
regionální	regionální	k2eAgFnSc1d1	regionální
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
Global	globat	k5eAaImAgMnS	globat
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
GAN	GAN	kA	GAN
<g/>
)	)	kIx)	)
–	–	k?	–
globální	globální	k2eAgFnSc4d1	globální
síť	síť	k1gFnSc4	síť
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Local	Local	k1gInSc1	Local
Area	area	k1gFnSc1	area
Network	network	k1gInSc6	network
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Peterka	Peterka	k1gMnSc1	Peterka
<g/>
:	:	kIx,	:
LAN	lano	k1gNnPc2	lano
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
WAN	WAN	kA	WAN
</s>
</p>
