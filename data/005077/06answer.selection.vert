<s>
Nejrozšířenějšími	rozšířený	k2eAgFnPc7d3	nejrozšířenější
technologiemi	technologie	k1gFnPc7	technologie
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
LAN	lano	k1gNnPc2	lano
sítích	síť	k1gFnPc6	síť
jsou	být	k5eAaImIp3nP	být
Ethernet	Ethernet	k1gMnSc1	Ethernet
a	a	k8xC	a
Wi-Fi	Wi-F	k1gMnPc1	Wi-F
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
WLAN	WLAN	kA	WLAN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
např.	např.	kA	např.
ARCNET	ARCNET	kA	ARCNET
a	a	k8xC	a
Token	Token	k2eAgInSc4d1	Token
Ring	ring	k1gInSc4	ring
<g/>
.	.	kIx.	.
</s>
