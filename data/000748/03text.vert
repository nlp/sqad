<s>
Hřbitov	hřbitov	k1gInSc1	hřbitov
Montparnasse	Montparnasse	k1gFnSc2	Montparnasse
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Cimetiè	Cimetiè	k1gMnSc2	Cimetiè
du	du	k?	du
Montparnasse	Montparnass	k1gMnSc2	Montparnass
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Montparnasse	Montparnasse	k1gFnSc2	Montparnasse
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
19	[number]	k4	19
ha	ha	kA	ha
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
pohřebištěm	pohřebiště	k1gNnSc7	pohřebiště
uvnitř	uvnitř	k7c2	uvnitř
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
zelených	zelený	k2eAgFnPc2d1	zelená
ploch	plocha	k1gFnPc2	plocha
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
35.000	[number]	k4	35.000
hrobů	hrob	k1gInPc2	hrob
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
zde	zde	k6eAd1	zde
asi	asi	k9	asi
1200	[number]	k4	1200
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
lípy	lípa	k1gFnPc1	lípa
<g/>
,	,	kIx,	,
cedry	cedr	k1gInPc1	cedr
<g/>
,	,	kIx,	,
javory	javor	k1gInPc1	javor
<g/>
,	,	kIx,	,
jasany	jasan	k1gInPc1	jasan
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
nacházely	nacházet	k5eAaImAgFnP	nacházet
tři	tři	k4xCgFnPc4	tři
zemědělské	zemědělský	k2eAgFnPc4d1	zemědělská
usedlosti	usedlost	k1gFnPc4	usedlost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
soukromý	soukromý	k2eAgInSc1d1	soukromý
hřbitov	hřbitov	k1gInSc1	hřbitov
pro	pro	k7c4	pro
duchovní	duchovní	k2eAgFnSc4d1	duchovní
z	z	k7c2	z
farnosti	farnost	k1gFnSc2	farnost
Saint-Jean-de-Dieu	Saint-Jeane-Dieus	k1gInSc2	Saint-Jean-de-Dieus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
pařížský	pařížský	k2eAgMnSc1d1	pařížský
prefekt	prefekt	k1gMnSc1	prefekt
Nicolas	Nicolas	k1gMnSc1	Nicolas
Frochot	Frochot	k1gMnSc1	Frochot
nechal	nechat	k5eAaPmAgMnS	nechat
vykoupit	vykoupit	k5eAaPmF	vykoupit
zdejší	zdejší	k2eAgInSc4d1	zdejší
pozemky	pozemek	k1gInPc4	pozemek
a	a	k8xC	a
zřídil	zřídit	k5eAaPmAgInS	zřídit
zde	zde	k6eAd1	zde
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
velkých	velký	k2eAgInPc2d1	velký
hřbitovů	hřbitov	k1gInPc2	hřbitov
za	za	k7c7	za
tehdejšími	tehdejší	k2eAgFnPc7d1	tehdejší
hradbami	hradba	k1gFnPc7	hradba
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1824	[number]	k4	1824
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
mnoho	mnoho	k4c1	mnoho
slavných	slavný	k2eAgFnPc2d1	slavná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
hřbitov	hřbitov	k1gInSc1	hřbitov
stal	stát	k5eAaPmAgInS	stát
svého	svůj	k3xOyFgMnSc4	svůj
druhu	druh	k1gInSc3	druh
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
u	u	k7c2	u
vstupu	vstup	k1gInSc2	vstup
zakoupit	zakoupit	k5eAaPmF	zakoupit
seznam	seznam	k1gInSc4	seznam
a	a	k8xC	a
mapku	mapka	k1gFnSc4	mapka
ukazující	ukazující	k2eAgNnSc1d1	ukazující
umístění	umístění	k1gNnSc1	umístění
hrobů	hrob	k1gInPc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Raymond	Raymond	k1gMnSc1	Raymond
Aron	Aron	k1gMnSc1	Aron
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
politolog	politolog	k1gMnSc1	politolog
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
B	B	kA	B
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoir	Beauvoir	k1gInSc1	Beauvoir
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
filozofka	filozofka	k1gFnSc1	filozofka
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Samuel	Samuel	k1gMnSc1	Samuel
Beckett	Beckett	k1gMnSc1	Beckett
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
irský	irský	k2eAgMnSc1d1	irský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
Aloysius	Aloysius	k1gInSc1	Aloysius
Bertrand	Bertrand	k1gInSc1	Bertrand
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1807	[number]	k4	1807
<g/>
-	-	kIx~	-
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc3	oddělení
<g/>
)	)	kIx)	)
William-Adolphe	William-Adolphe	k1gNnSc3	William-Adolphe
Bouguereau	Bouguereaum	k1gNnSc3	Bouguereaum
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
Constantin	Constantin	k1gMnSc1	Constantin
Brâncuşi	Brâncuşe	k1gFnSc4	Brâncuşe
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
sochař	sochař	k1gMnSc1	sochař
<g />
.	.	kIx.	.
</s>
<s>
rumunského	rumunský	k2eAgInSc2d1	rumunský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Michel	Michel	k1gMnSc1	Michel
Bréal	Bréal	k1gMnSc1	Bréal
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filolog	filolog	k1gMnSc1	filolog
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Gyula	Gyula	k1gMnSc1	Gyula
Brassaï	Brassaï	k1gMnSc1	Brassaï
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fotograf	fotograf	k1gMnSc1	fotograf
maďarského	maďarský	k2eAgInSc2d1	maďarský
původu	původ	k1gInSc2	původ
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
C	C	kA	C
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Chabrier	Chabrier	k1gMnSc1	Chabrier
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
André	André	k1gMnSc1	André
Citroën	Citroën	k1gMnSc1	Citroën
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
průmyslník	průmyslník	k1gMnSc1	průmyslník
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g />
.	.	kIx.	.
</s>
<s>
<g/>
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
Julio	Julio	k1gMnSc1	Julio
Cortázar	Cortázar	k1gMnSc1	Cortázar
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
argentinský	argentinský	k2eAgMnSc1d1	argentinský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Charles	Charles	k1gMnSc1	Charles
Cros	Crosa	k1gFnPc2	Crosa
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
D	D	kA	D
Paul	Paul	k1gMnSc1	Paul
Deschanel	Deschanel	k1gMnSc1	Deschanel
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Robert	Robert	k1gMnSc1	Robert
Desnos	Desnos	k1gMnSc1	Desnos
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Alfred	Alfred	k1gMnSc1	Alfred
Dreyfus	Dreyfus	k1gMnSc1	Dreyfus
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
důstojník	důstojník	k1gMnSc1	důstojník
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Jules	Jules	k1gMnSc1	Jules
Dumont	Dumont	k1gMnSc1	Dumont
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Urville	Urvill	k1gMnSc2	Urvill
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
-	-	kIx~	-
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
Marguerite	Marguerit	k1gInSc5	Marguerit
<g />
.	.	kIx.	.
</s>
<s>
Duras	Duras	k1gInSc1	Duras
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
režisérka	režisérka	k1gFnSc1	režisérka
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
Émile	Émile	k1gFnSc1	Émile
Durkheim	Durkheim	k1gMnSc1	Durkheim
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
sociolog	sociolog	k1gMnSc1	sociolog
G	G	kA	G
Serge	Serge	k1gFnSc1	Serge
Gainsbourg	Gainsbourg	k1gMnSc1	Gainsbourg
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
H	H	kA	H
Charles	Charles	k1gMnSc1	Charles
Hermite	Hermit	k1gInSc5	Hermit
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
Pierre-Jules	Pierre-Jules	k1gMnSc1	Pierre-Jules
Hetzel	Hetzel	k1gMnSc1	Hetzel
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vydavatel	vydavatel	k1gMnSc1	vydavatel
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
<g />
.	.	kIx.	.
</s>
<s>
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
Jean-Antoine	Jean-Antoin	k1gMnSc5	Jean-Antoin
Houdon	Houdon	k1gMnSc1	Houdon
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
-	-	kIx~	-
<g/>
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
I	i	k9	i
Eugè	Eugè	k1gMnSc1	Eugè
Ionesco	Ionesco	k1gMnSc1	Ionesco
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
rumunského	rumunský	k2eAgInSc2d1	rumunský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
<g/>
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
J	J	kA	J
de	de	k?	de
Jussieu	Jussieus	k1gInSc2	Jussieus
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
francouzských	francouzský	k2eAgMnPc2d1	francouzský
botaniků	botanik	k1gMnPc2	botanik
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
L	L	kA	L
Pierre	Pierr	k1gMnSc5	Pierr
Larousse	Larouss	k1gMnSc5	Larouss
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
-	-	kIx~	-
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lexikograf	lexikograf	k1gMnSc1	lexikograf
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
Pierre	Pierr	k1gMnSc5	Pierr
Laval	Laval	k1gInSc4	Laval
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
politik	politik	k1gMnSc1	politik
Urbain	Urbain	k1gMnSc1	Urbain
Le	Le	k1gMnSc1	Le
Verrier	Verrier	k1gMnSc1	Verrier
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
-	-	kIx~	-
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
Pierre	Pierr	k1gMnSc5	Pierr
Louÿ	Louÿ	k1gMnSc5	Louÿ
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
M	M	kA	M
Man	Man	k1gMnSc1	Man
Ray	Ray	k1gMnSc1	Ray
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Guy	Guy	k1gMnSc1	Guy
de	de	k?	de
Maupassant	Maupassant	k1gMnSc1	Maupassant
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
N	N	kA	N
Philippe	Philipp	k1gInSc5	Philipp
Noiret	Noiret	k1gMnSc1	Noiret
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
-	-	kIx~	-
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
Francine	Francin	k1gMnSc5	Francin
Adè	Adè	k1gMnSc5	Adè
Navarro	Navarra	k1gFnSc5	Navarra
-	-	kIx~	-
francouzská	francouzský	k2eAgFnSc1d1	francouzská
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
a	a	k8xC	a
právnička	právnička	k1gFnSc1	právnička
<g/>
,	,	kIx,	,
sňatkem	sňatek	k1gInSc7	sňatek
černohorská	černohorský	k2eAgFnSc1d1	černohorská
korunní	korunní	k2eAgFnSc1d1	korunní
princezna	princezna	k1gFnSc1	princezna
P	P	kA	P
Adolphe	Adolphe	k1gFnSc1	Adolphe
<g />
.	.	kIx.	.
</s>
<s>
Pégoud	Pégoud	k1gInSc1	Pégoud
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
pilot	pilot	k1gMnSc1	pilot
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
Symon	Symon	k1gNnSc1	Symon
Petljura	Petljura	k1gFnSc1	Petljura
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
politik	politik	k1gMnSc1	politik
Henri	Henr	k1gFnSc2	Henr
Poincaré	Poincarý	k2eAgFnSc2d1	Poincarý
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Pierre-Joseph	Pierre-Joseph	k1gMnSc1	Pierre-Joseph
Proudhon	Proudhon	k1gMnSc1	Proudhon
(	(	kIx(	(
<g/>
1809	[number]	k4	1809
<g/>
-	-	kIx~	-
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
teoretik	teoretik	k1gMnSc1	teoretik
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
myslitel	myslitel	k1gMnSc1	myslitel
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
R	R	kA	R
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Rampal	Rampal	k1gMnSc6	Rampal
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
francouzský	francouzský	k2eAgMnSc1d1	francouzský
flétnista	flétnista	k1gMnSc1	flétnista
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
Jean-François	Jean-François	k1gFnSc1	Jean-François
Revel	Revel	k1gMnSc1	Revel
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
François	François	k1gFnSc1	François
Rude	Rude	k1gFnSc1	Rude
(	(	kIx(	(
<g/>
1784	[number]	k4	1784
<g/>
-	-	kIx~	-
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
S	s	k7c7	s
Camille	Camille	k1gNnSc7	Camille
Saint-Saëns	Saint-Saënsa	k1gFnPc2	Saint-Saënsa
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
Marcel	Marcela	k1gFnPc2	Marcela
Schwob	Schwoba	k1gFnPc2	Schwoba
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
Susan	Susan	k1gMnSc1	Susan
Sontag	Sontag	k1gMnSc1	Sontag
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
intelektuálka	intelektuálka	k1gFnSc1	intelektuálka
Chajim	Chajima	k1gFnPc2	Chajima
Soutine	Soutin	k1gInSc5	Soutin
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
litevský	litevský	k2eAgMnSc1d1	litevský
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
T	T	kA	T
Tristan	Tristan	k1gInSc1	Tristan
Tzara	Tzara	k1gFnSc1	Tzara
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
rumunského	rumunský	k2eAgInSc2d1	rumunský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
Z	z	k7c2	z
Ossip	Ossip	k1gInSc1	Ossip
Zadkine	Zadkin	k1gInSc5	Zadkin
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
sochař	sochař	k1gMnSc1	sochař
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
)	)	kIx)	)
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
funkčních	funkční	k2eAgInPc2d1	funkční
14	[number]	k4	14
hřbitovů	hřbitov	k1gInPc2	hřbitov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
čtyři	čtyři	k4xCgInPc1	čtyři
hřbitovy	hřbitov	k1gInPc1	hřbitov
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
hřbitovních	hřbitovní	k2eAgNnPc2d1	hřbitovní
míst	místo	k1gNnPc2	místo
<g/>
:	:	kIx,	:
Pè	Pè	k1gFnSc1	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
cca	cca	kA	cca
70	[number]	k4	70
000	[number]	k4	000
náhrobků	náhrobek	k1gInPc2	náhrobek
nebo	nebo	k8xC	nebo
koncesí	koncese	k1gFnPc2	koncese
<g/>
)	)	kIx)	)
Montparnasse	Montparnasse	k1gFnSc1	Montparnasse
(	(	kIx(	(
<g/>
35	[number]	k4	35
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Montmartre	Montmartr	k1gInSc5	Montmartr
(	(	kIx(	(
<g/>
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Batignolles	Batignollesa	k1gFnPc2	Batignollesa
(	(	kIx(	(
<g/>
15	[number]	k4	15
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Dalších	další	k2eAgInPc2d1	další
šest	šest	k4xCc1	šest
hřbitovů	hřbitov	k1gInPc2	hřbitov
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
pohřbívání	pohřbívání	k1gNnSc3	pohřbívání
Pařížanů	Pařížan	k1gMnPc2	Pařížan
leží	ležet	k5eAaImIp3nP	ležet
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
názvy	název	k1gInPc1	název
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgInP	odvodit
od	od	k7c2	od
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
:	:	kIx,	:
Hřbitov	hřbitov	k1gInSc1	hřbitov
Pantin	Pantin	k1gInSc1	Pantin
(	(	kIx(	(
<g/>
200	[number]	k4	200
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Hřbitov	hřbitov	k1gInSc1	hřbitov
Thiais	Thiais	k1gFnSc2	Thiais
(	(	kIx(	(
<g/>
150	[number]	k4	150
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Hřbitov	hřbitov	k1gInSc1	hřbitov
Bagneux	Bagneux	k1gInSc1	Bagneux
(	(	kIx(	(
<g/>
83	[number]	k4	83
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Hřbitov	hřbitov	k1gInSc1	hřbitov
Ivry	Ivra	k1gFnSc2	Ivra
(	(	kIx(	(
<g/>
48	[number]	k4	48
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
Hřbitov	hřbitov	k1gInSc1	hřbitov
Saint-Ouen	Saint-Ouen	k1gInSc1	Saint-Ouen
(	(	kIx(	(
<g/>
46	[number]	k4	46
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
