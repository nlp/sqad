<s>
Cyklotymie	Cyklotymie	k1gFnSc1	Cyklotymie
je	být	k5eAaImIp3nS	být
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
nestálost	nestálost	k1gFnSc1	nestálost
nálady	nálada	k1gFnSc2	nálada
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnPc4d1	zahrnující
četná	četný	k2eAgNnPc4d1	četné
období	období	k1gNnPc4	období
mírné	mírný	k2eAgFnPc4d1	mírná
deprese	deprese	k1gFnPc4	deprese
a	a	k8xC	a
mírné	mírný	k2eAgFnPc4d1	mírná
elace	elace	k1gFnPc4	elace
(	(	kIx(	(
<g/>
dobré	dobrý	k2eAgFnPc4d1	dobrá
<g/>
,	,	kIx,	,
povznesené	povznesený	k2eAgFnPc4d1	povznesená
nálady	nálada	k1gFnPc4	nálada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tyto	tento	k3xDgInPc1	tento
výkyvy	výkyv	k1gInPc1	výkyv
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
probíhajícím	probíhající	k2eAgFnPc3d1	probíhající
životním	životní	k2eAgFnPc3d1	životní
událostem	událost	k1gFnPc3	událost
<g/>
.	.	kIx.	.
</s>
