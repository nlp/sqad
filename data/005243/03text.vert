<s>
Cyklotymie	Cyklotymie	k1gFnSc1	Cyklotymie
je	být	k5eAaImIp3nS	být
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
nestálost	nestálost	k1gFnSc1	nestálost
nálady	nálada	k1gFnSc2	nálada
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnPc4d1	zahrnující
četná	četný	k2eAgNnPc4d1	četné
období	období	k1gNnPc4	období
mírné	mírný	k2eAgFnPc4d1	mírná
deprese	deprese	k1gFnPc4	deprese
a	a	k8xC	a
mírné	mírný	k2eAgFnPc4d1	mírná
elace	elace	k1gFnPc4	elace
(	(	kIx(	(
<g/>
dobré	dobrý	k2eAgFnPc4d1	dobrá
<g/>
,	,	kIx,	,
povznesené	povznesený	k2eAgFnPc4d1	povznesená
nálady	nálada	k1gFnPc4	nálada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tyto	tento	k3xDgInPc1	tento
výkyvy	výkyv	k1gInPc1	výkyv
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
probíhajícím	probíhající	k2eAgFnPc3d1	probíhající
životním	životní	k2eAgFnPc3d1	životní
událostem	událost	k1gFnPc3	událost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
výkyvy	výkyv	k1gInPc4	výkyv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ještě	ještě	k6eAd1	ještě
nesplňují	splňovat	k5eNaImIp3nP	splňovat
podmínky	podmínka	k1gFnPc4	podmínka
deprese	deprese	k1gFnPc4	deprese
či	či	k8xC	či
hypománie	hypománie	k1gFnPc4	hypománie
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
například	například	k6eAd1	například
o	o	k7c4	o
sezonní	sezonní	k2eAgFnPc4d1	sezonní
změny	změna	k1gFnPc4	změna
nálad	nálada	k1gFnPc2	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc1	změna
nálady	nálada	k1gFnSc2	nálada
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
a	a	k8xC	a
výraznější	výrazný	k2eAgFnPc1d2	výraznější
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bipolární	bipolární	k2eAgFnSc4d1	bipolární
afektivní	afektivní	k2eAgFnSc4d1	afektivní
poruchu	porucha	k1gFnSc4	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cyklotymie	cyklotymie	k1gFnSc2	cyklotymie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
