<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Felix	Felix	k1gMnSc1	Felix
I.	I.	kA	I.
byl	být	k5eAaImAgInS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
(	(	kIx(	(
<g/>
papežem	papež	k1gMnSc7	papež
<g/>
)	)	kIx)	)
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
269	[number]	k4	269
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
274	[number]	k4	274
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
papeži	papež	k1gMnSc6	papež
Felixovi	Felix	k1gMnSc6	Felix
I.	I.	kA	I.
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byl	být	k5eAaImAgInS	být
rodilým	rodilý	k2eAgMnSc7d1	rodilý
Římanem	Říman	k1gMnSc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
papežský	papežský	k2eAgInSc1d1	papežský
úřad	úřad	k1gInSc1	úřad
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
závěru	závěr	k1gInSc2	závěr
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Aureliána	Aurelián	k1gMnSc2	Aurelián
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
poměrně	poměrně	k6eAd1	poměrně
klidným	klidný	k2eAgNnPc3d1	klidné
obdobím	období	k1gNnPc3	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
adresována	adresován	k2eAgFnSc1d1	adresována
papeži	papež	k1gMnSc3	papež
Dionysiovi	Dionysius	k1gMnSc3	Dionysius
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
doručena	doručit	k5eAaPmNgFnS	doručit
zpráva	zpráva	k1gFnSc1	zpráva
synody	synod	k1gInPc1	synod
z	z	k7c2	z
Antiochie	Antiochie	k1gFnSc2	Antiochie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Antakya	Antakya	k1gFnSc1	Antakya
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc6d1	ležící
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
na	na	k7c4	na
místního	místní	k2eAgMnSc4d1	místní
biskupa	biskup	k1gMnSc4	biskup
Pavla	Pavel	k1gMnSc4	Pavel
ze	z	k7c2	z
Samosaty	Samosata	k1gFnSc2	Samosata
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
heretické	heretický	k2eAgNnSc4d1	heretické
učení	učení	k1gNnSc4	učení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
dotýkalo	dotýkat	k5eAaImAgNnS	dotýkat
církevní	církevní	k2eAgFnPc4d1	církevní
doktríny	doktrína	k1gFnPc4	doktrína
o	o	k7c6	o
Nejsvětější	nejsvětější	k2eAgFnSc6d1	nejsvětější
Trojici	trojice	k1gFnSc6	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Fragment	fragment	k1gInSc1	fragment
dopisu	dopis	k1gInSc2	dopis
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
papež	papež	k1gMnSc1	papež
Felix	Felix	k1gMnSc1	Felix
podal	podat	k5eAaPmAgMnS	podat
výklad	výklad	k1gInSc4	výklad
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
se	se	k3xPyFc4	se
uchoval	uchovat	k5eAaPmAgMnS	uchovat
v	v	k7c6	v
aktech	akta	k1gNnPc6	akta
efezkého	efezký	k2eAgInSc2d1	efezký
koncilu	koncil	k1gInSc2	koncil
(	(	kIx(	(
<g/>
431	[number]	k4	431
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
adresován	adresovat	k5eAaBmNgInS	adresovat
alexandrijskému	alexandrijský	k2eAgInSc3d1	alexandrijský
biskupovi	biskupův	k2eAgMnPc1d1	biskupův
Maximovi	Maximův	k2eAgMnPc1d1	Maximův
a	a	k8xC	a
Felix	Felix	k1gMnSc1	Felix
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
identitu	identita	k1gFnSc4	identita
Syna	syn	k1gMnSc2	syn
Božího	boží	k2eAgMnSc2d1	boží
a	a	k8xC	a
Syna	syn	k1gMnSc2	syn
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gInSc1	Pontificalis
zemřel	zemřít	k5eAaPmAgMnS	zemřít
mučednickou	mučednický	k2eAgFnSc7d1	mučednická
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
zbudoval	zbudovat	k5eAaPmAgInS	zbudovat
na	na	k7c4	na
Via	via	k7c4	via
Aurelia	Aurelius	k1gMnSc4	Aurelius
baziliku	bazilika	k1gFnSc4	bazilika
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
prameny	pramen	k1gInPc1	pramen
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
Římský	římský	k2eAgInSc4d1	římský
kalendář	kalendář	k1gInSc4	kalendář
svatých	svatá	k1gFnPc2	svatá
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Kalixtových	Kalixtův	k2eAgFnPc6d1	Kalixtův
katakombách	katakomby	k1gFnPc6	katakomby
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
jde	jít	k5eAaImIp3nS	jít
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
již	již	k6eAd1	již
mnohokrát	mnohokrát	k6eAd1	mnohokrát
v	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
dějinách	dějiny	k1gFnPc6	dějiny
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
o	o	k7c4	o
záměnu	záměna	k1gFnSc4	záměna
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Via	via	k7c4	via
Aurelia	Aurelium	k1gNnPc4	Aurelium
byl	být	k5eAaImAgMnS	být
skutečně	skutečně	k6eAd1	skutečně
pohřben	pohřben	k2eAgMnSc1d1	pohřben
mučedník	mučedník	k1gMnSc1	mučedník
jménem	jméno	k1gNnSc7	jméno
Felix	Felix	k1gMnSc1	Felix
a	a	k8xC	a
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
bazilika	bazilika	k1gFnSc1	bazilika
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInSc7	jeho
hrobem	hrob	k1gInSc7	hrob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mučednická	mučednický	k2eAgFnSc1d1	mučednická
smrt	smrt	k1gFnSc1	smrt
není	být	k5eNaImIp3nS	být
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
i	i	k9	i
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
Pavel	Pavel	k1gMnSc1	Pavel
ze	z	k7c2	z
Samosaty	Samosata	k1gFnSc2	Samosata
nadále	nadále	k6eAd1	nadále
biskupem	biskup	k1gInSc7	biskup
v	v	k7c6	v
Antiochii	Antiochie	k1gFnSc6	Antiochie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Aureliána	Aurelián	k1gMnSc2	Aurelián
předložen	předložit	k5eAaPmNgInS	předložit
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
však	však	k9	však
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
náleží	náležet	k5eAaImIp3nS	náležet
římskému	římský	k2eAgMnSc3d1	římský
biskupovi	biskup	k1gMnSc3	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
úřad	úřad	k1gInSc1	úřad
biskupa	biskup	k1gMnSc2	biskup
římského	římský	k2eAgMnSc2d1	římský
byl	být	k5eAaImAgMnS	být
císařem	císař	k1gMnSc7	císař
respektován	respektovat	k5eAaImNgMnS	respektovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
datum	datum	k1gNnSc4	datum
Felixovy	Felixův	k2eAgFnSc2d1	Felixova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Donedávna	donedávna	k6eAd1	donedávna
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
památka	památka	k1gFnSc1	památka
uctívána	uctíván	k2eAgFnSc1d1	uctívána
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
den	den	k1gInSc1	den
úmrtí	úmrtí	k1gNnSc2	úmrtí
zapsán	zapsán	k2eAgInSc1d1	zapsán
v	v	k7c4	v
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gFnPc4	Pontificalis
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
historici	historik	k1gMnPc1	historik
usoudili	usoudit	k5eAaPmAgMnP	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
chybě	chyba	k1gFnSc3	chyba
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
a	a	k8xC	a
podle	podle	k7c2	podle
nového	nový	k2eAgNnSc2d1	nové
vydání	vydání	k1gNnSc2	vydání
Martirologio	Martirologio	k1gMnSc1	Martirologio
Romano	Romano	k1gMnSc1	Romano
je	být	k5eAaImIp3nS	být
dnem	den	k1gInSc7	den
úmrtí	úmrtí	k1gNnSc2	úmrtí
i	i	k8xC	i
památky	památka	k1gFnSc2	památka
svatého	svatý	k2eAgMnSc2d1	svatý
Felixe	Felix	k1gMnSc2	Felix
I.	I.	kA	I.
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Felix	Felix	k1gMnSc1	Felix
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
