<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Solferina	Solferino	k1gNnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Solferina	Solferino	k1gNnSc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
italská	italský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
</s>
<s>
Boj	boj	k1gInSc1
v	v	k7c6
San	San	k1gFnSc6
Martinu	Martina	k1gFnSc4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1859	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Okolí	okolí	k1gNnSc1
města	město	k1gNnSc2
Solferino	Solferino	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Vítězství	vítězství	k1gNnSc1
Francie	Francie	k1gFnSc2
a	a	k8xC
Piemontu	Piemont	k1gInSc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Druhé	druhý	k4xOgNnSc4
Francouzské	francouzský	k2eAgNnSc4d1
císařstvíSardinské	císařstvíSardinský	k2eAgNnSc4d1
království	království	k1gNnSc4
</s>
<s>
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Napoleon	Napoleon	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
Viktor	Viktor	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
115	#num#	k4
500	#num#	k4
mužů	muž	k1gMnPc2
10	#num#	k4
700	#num#	k4
koní	kůň	k1gMnPc2
320	#num#	k4
děl	dělo	k1gNnPc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
126	#num#	k4
722	#num#	k4
mužů	muž	k1gMnPc2
6	#num#	k4
520	#num#	k4
koní	kůň	k1gMnPc2
413	#num#	k4
děl	dělo	k1gNnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
1	#num#	k4
622	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
Francouzů	Francouz	k1gMnPc2
869	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
Piemonťanů	Piemonťan	k1gMnPc2
8	#num#	k4
490	#num#	k4
raněných	raněný	k2eAgMnPc2d1
Francouzů	Francouz	k1gMnPc2
3	#num#	k4
982	#num#	k4
raněných	raněný	k2eAgMnPc2d1
Piemonťanů	Piemonťan	k1gMnPc2
2	#num#	k4
922	#num#	k4
nezvěstných	zvěstný	k2eNgMnPc2d1
</s>
<s>
2	#num#	k4
292	#num#	k4
padlých	padlý	k2eAgInPc2d1
10	#num#	k4
807	#num#	k4
raněných	raněný	k2eAgInPc2d1
8	#num#	k4
368	#num#	k4
nezvěstných	zvěstný	k2eNgMnPc2d1
</s>
<s>
Bojiště	bojiště	k1gNnSc1
u	u	k7c2
Solferina	Solferino	k1gNnSc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
</s>
<s>
Mapa	mapa	k1gFnSc1
bitvy	bitva	k1gFnSc2
</s>
<s>
Pomník	pomník	k1gInSc1
píseckým	písecký	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
padlým	padlý	k2eAgFnPc3d1
v	v	k7c6
bitvách	bitva	k1gFnPc6
u	u	k7c2
Melegnana	Melegnan	k1gMnSc2
a	a	k8xC
Solferina	Solferin	k1gMnSc2
v	v	k7c6
Písku	Písek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Solferina	Solferino	k1gNnSc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1859	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
největší	veliký	k2eAgFnSc7d3
a	a	k8xC
rozhodující	rozhodující	k2eAgFnSc7d1
bitvou	bitva	k1gFnSc7
druhé	druhý	k4xOgFnSc2
italské	italský	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
francouzsko-sardinská	francouzsko-sardinský	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
porazila	porazit	k5eAaPmAgFnS
rakouskou	rakouský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
představovala	představovat	k5eAaImAgFnS
rozhodující	rozhodující	k2eAgInSc4d1
krok	krok	k1gInSc4
ke	k	k7c3
sjednocení	sjednocení	k1gNnSc3
Itálie	Itálie	k1gFnSc2
a	a	k8xC
Viktoru	Viktor	k1gMnSc3
Emanuelovi	Emanuel	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
otevřela	otevřít	k5eAaPmAgFnS
cestu	cesta	k1gFnSc4
k	k	k7c3
titulu	titul	k1gInSc3
italského	italský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
střetnutí	střetnutí	k1gNnSc6
mezi	mezi	k7c7
rakouskými	rakouský	k2eAgNnPc7d1
a	a	k8xC
francouzsko-sardinskými	francouzsko-sardinský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
hrála	hrát	k5eAaImAgFnS
úlohu	úloha	k1gFnSc4
neschopnost	neschopnost	k1gFnSc4
velitele	velitel	k1gMnSc2
rakouských	rakouský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
Ference	Ferenc	k1gMnSc4
Gyulaia	Gyulaius	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
nemohl	moct	k5eNaImAgMnS
rozhoupat	rozhoupat	k5eAaPmF
k	k	k7c3
akci	akce	k1gFnSc3
proti	proti	k7c3
slabým	slabý	k2eAgNnPc3d1
vojskům	vojsko	k1gNnPc3
sardinského	sardinský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Viktora	Viktor	k1gMnSc2
Emanuela	Emanuel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Váhání	váhání	k1gNnPc2
Rakušanů	Rakušan	k1gMnPc2
dalo	dát	k5eAaPmAgNnS
možnost	možnost	k1gFnSc4
sardinské	sardinský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
se	s	k7c7
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
spojit	spojit	k5eAaPmF
s	s	k7c7
francouzským	francouzský	k2eAgInSc7d1
kontingentem	kontingent	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
čítal	čítat	k5eAaImAgInS
120	#num#	k4
tisíc	tisíc	k4xCgInPc2
vojáků	voják	k1gMnPc2
a	a	k8xC
značně	značně	k6eAd1
tak	tak	k9
navýšil	navýšit	k5eAaPmAgMnS
počty	počet	k1gInPc4
malé	malý	k2eAgFnSc2d1
sardinské	sardinský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
počátku	počátek	k1gInSc2
června	červen	k1gInSc2
začaly	začít	k5eAaPmAgInP
jednotlivé	jednotlivý	k2eAgInPc1d1
boje	boj	k1gInPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgInPc2d3
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
do	do	k7c2
boje	boj	k1gInSc2
nasazen	nasazen	k2eAgInSc4d1
11	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
z	z	k7c2
Písku	Písek	k1gInSc2
<g/>
,	,	kIx,
doplňovaný	doplňovaný	k2eAgInSc1d1
v	v	k7c6
jižních	jižní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Melegnana	Melegnan	k1gMnSc2
se	se	k3xPyFc4
hrdinsky	hrdinsky	k6eAd1
bránilo	bránit	k5eAaImAgNnS
6	#num#	k4
tisíc	tisíc	k4xCgInPc2
rakouských	rakouský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
náporu	nápor	k1gInSc2
30	#num#	k4
tisíc	tisíc	k4xCgInPc2
Francouzů	Francouz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Solferina	Solferino	k1gNnSc2
se	se	k3xPyFc4
odehrávala	odehrávat	k5eAaImAgFnS
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
a	a	k8xC
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
prakticky	prakticky	k6eAd1
o	o	k7c4
tři	tři	k4xCgFnPc4
bitvy	bitva	k1gFnPc4
–	–	k?
u	u	k7c2
Solferina	Solferino	k1gNnSc2
<g/>
,	,	kIx,
u	u	k7c2
Guidizzola	Guidizzola	k1gFnSc1
a	a	k8xC
u	u	k7c2
San	San	k1gMnSc2
Martina	Martin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
straně	strana	k1gFnSc6
rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
účastnilo	účastnit	k5eAaImAgNnS
126	#num#	k4
tisíc	tisíc	k4xCgInPc2
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
spojená	spojený	k2eAgNnPc4d1
francouzsko-sardinská	francouzsko-sardinský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
měla	mít	k5eAaImAgFnS
115	#num#	k4
tisíc	tisíc	k4xCgInPc2
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
San	San	k1gFnSc2
Martina	Martin	k1gInSc2
si	se	k3xPyFc3
dokázali	dokázat	k5eAaPmAgMnP
vytvořit	vytvořit	k5eAaPmF
Francouzi	Francouz	k1gMnSc3
převahu	převaha	k1gFnSc4
a	a	k8xC
postupně	postupně	k6eAd1
vytlačovali	vytlačovat	k5eAaImAgMnP
rakouská	rakouský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
z	z	k7c2
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Solferina	Solferino	k1gNnSc2
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
rakouští	rakouský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
stahovat	stahovat	k5eAaImF
pod	pod	k7c7
mohutným	mohutný	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
kolem	kolem	k7c2
čtvrté	čtvrtá	k1gFnSc2
hodiny	hodina	k1gFnPc4
odpolední	odpolední	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středu	střed	k1gInSc6
sestavy	sestava	k1gFnSc2
se	se	k3xPyFc4
střetlo	střetnout	k5eAaPmAgNnS
57	#num#	k4
000	#num#	k4
Francouzů	Francouz	k1gMnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
celé	celý	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
<g/>
)	)	kIx)
s	s	k7c7
40	#num#	k4
000	#num#	k4
Rakušany	Rakušan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úporných	úporný	k2eAgInPc6d1
bojích	boj	k1gInPc6
v	v	k7c6
nepřehledném	přehledný	k2eNgInSc6d1
terénu	terén	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nedokázaly	dokázat	k5eNaPmAgFnP
uplatnit	uplatnit	k5eAaPmF
rakouské	rakouský	k2eAgFnSc2d1
pušky	puška	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
střed	střed	k1gInSc1
rakouské	rakouský	k2eAgFnSc2d1
sestavy	sestava	k1gFnSc2
prolomen	prolomen	k2eAgInSc4d1
a	a	k8xC
levé	levý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
poté	poté	k6eAd1
začalo	začít	k5eAaPmAgNnS
ustupovat	ustupovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
šest	šest	k4xCc4
hodin	hodina	k1gFnPc2
večer	večer	k6eAd1
se	se	k3xPyFc4
sardinská	sardinský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
vrhla	vrhnout	k5eAaPmAgNnP,k5eAaImAgNnP
do	do	k7c2
nového	nový	k2eAgInSc2d1
útoku	útok	k1gInSc2
proti	proti	k7c3
rakouskému	rakouský	k2eAgNnSc3d1
pravému	pravý	k2eAgNnSc3d1
křídlu	křídlo	k1gNnSc3
<g/>
,	,	kIx,
ovšem	ovšem	k9
marně	marně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
proti	proti	k7c3
sboru	sbor	k1gInSc3
Ludwika	Ludwikum	k1gNnSc2
von	von	k1gInSc4
Benedeka	Benedeek	k1gInSc2
s	s	k7c7
25	#num#	k4
000	#num#	k4
vojáky	voják	k1gMnPc7
útočilo	útočit	k5eAaImAgNnS
44	#num#	k4
000	#num#	k4
Piemontanů	Piemontan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
desáté	desátá	k1gFnSc2
hodiny	hodina	k1gFnSc2
večerní	večerní	k2eAgFnSc4d1
byl	být	k5eAaImAgInS
dán	dát	k5eAaPmNgInS
pokyn	pokyn	k1gInSc1
k	k	k7c3
ústupu	ústup	k1gInSc3
rakouských	rakouský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
z	z	k7c2
bojiště	bojiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
15	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
vyčerpaná	vyčerpaný	k2eAgNnPc4d1
francouzsko-sardinská	francouzsko-sardinský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
již	již	k9
neměla	mít	k5eNaImAgFnS
sílu	síla	k1gFnSc4
pronásledovat	pronásledovat	k5eAaImF
stejně	stejně	k6eAd1
vyčerpaného	vyčerpaný	k2eAgMnSc4d1
nepřítele	nepřítel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzům	Francouz	k1gMnPc3
bitvu	bitva	k1gFnSc4
vyhrály	vyhrát	k5eAaPmAgFnP
rojnice	rojnice	k1gFnPc1
<g/>
,	,	kIx,
bodáky	bodák	k1gInPc1
a	a	k8xC
děla	dělo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Použité	použitý	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
</s>
<s>
Na	na	k7c6
rakouské	rakouský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
byla	být	k5eAaImAgFnS
jako	jako	k9
hlavní	hlavní	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
pěchoty	pěchota	k1gFnSc2
používána	používán	k2eAgFnSc1d1
puška	puška	k1gFnSc1
Lorenz	Lorenz	k1gMnSc1
vzor	vzor	k1gInSc4
1854	#num#	k4
s	s	k7c7
drážkovanou	drážkovaný	k2eAgFnSc7d1
hlavní	hlavní	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
měla	mít	k5eAaImAgFnS
větší	veliký	k2eAgInSc4d2
dostřel	dostřel	k1gInSc4
<g/>
,	,	kIx,
přesnost	přesnost	k1gFnSc4
a	a	k8xC
rychlost	rychlost	k1gFnSc4
palby	palba	k1gFnSc2
než	než	k8xS
pušky	puška	k1gFnSc2
s	s	k7c7
hladkou	hladký	k2eAgFnSc7d1
hlavní	hlavní	k2eAgFnSc7d1
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
používali	používat	k5eAaImAgMnP
protivníci	protivník	k1gMnPc1
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
francouzská	francouzský	k2eAgFnSc1d1
puška	puška	k1gFnSc1
Minié	Miniý	k2eAgFnSc2d1
Mle	Mle	k1gFnSc2
1853	#num#	k4
tak	tak	k8xC,k8xS
i	i	k9
Piemontská	Piemontský	k2eAgFnSc1d1
puška	puška	k1gFnSc1
vzor	vzor	k1gInSc1
1844	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lorenz	Lorenz	k1gMnSc1
ráže	ráže	k1gFnSc2
13,9	13,9	k4
mm	mm	kA
měl	mít	k5eAaImAgInS
účinný	účinný	k2eAgInSc1d1
dostřel	dostřel	k1gInSc1
až	až	k9
900	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
200	#num#	k4
metrů	metr	k1gInPc2
střela	střela	k1gFnSc1
trefovala	trefovat	k5eAaImAgFnS
terč	terč	k1gInSc4
s	s	k7c7
průměrem	průměr	k1gInSc7
50	#num#	k4
cm	cm	kA
v	v	k7c6
90	#num#	k4
%	%	kIx~
případů	případ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
Minié	Minié	k1gNnPc6
měla	mít	k5eAaImAgFnS
účinný	účinný	k2eAgInSc4d1
dostřel	dostřel	k1gInSc4
cca	cca	kA
600	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c4
200	#num#	k4
metrů	metr	k1gInPc2
třetina	třetina	k1gFnSc1
výstřelů	výstřel	k1gInPc2
zasáhla	zasáhnout	k5eAaPmAgFnS
terč	terč	k1gInSc4
o	o	k7c6
průměru	průměr	k1gInSc6
jeden	jeden	k4xCgInSc4
metr	metr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
francouzská	francouzský	k2eAgFnSc1d1
puška	puška	k1gFnSc1
byla	být	k5eAaImAgFnS
vybavena	vybavit	k5eAaPmNgFnS
lepším	dobrý	k2eAgInSc7d2
bodákem	bodák	k1gInSc7
(	(	kIx(
<g/>
sabre-bainette	sabre-bainette	k5eAaPmIp2nP
<g/>
)	)	kIx)
a	a	k8xC
Francouzi	Francouz	k1gMnPc1
měli	mít	k5eAaImAgMnP
lepší	dobrý	k2eAgFnSc4d2
pěchotní	pěchotní	k2eAgFnSc4d1
taktiku	taktika	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
využívala	využívat	k5eAaImAgFnS,k5eAaPmAgFnS
rojnice	rojnice	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
se	se	k3xPyFc4
používání	používání	k1gNnSc1
rojnic	rojnice	k1gFnPc2
vůbec	vůbec	k9
necvičilo	cvičit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
Francouzi	Francouz	k1gMnPc1
do	do	k7c2
střeleckých	střelecký	k2eAgInPc2d1
soubojů	souboj	k1gInPc2
pouštěli	pouštět	k5eAaImAgMnP
jen	jen	k9
velmi	velmi	k6eAd1
neradi	nerad	k2eAgMnPc1d1
a	a	k8xC
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
boje	boj	k1gInPc4
řešit	řešit	k5eAaImF
především	především	k9
útoky	útok	k1gInPc4
na	na	k7c4
bodák	bodák	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Francouzská	francouzský	k2eAgNnPc1d1
děla	dělo	k1gNnPc1
byla	být	k5eAaImAgNnP
mnohem	mnohem	k6eAd1
kvalitnější	kvalitní	k2eAgNnPc1d2
než	než	k8xS
rakouská	rakouský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
maximálního	maximální	k2eAgInSc2d1
rakouského	rakouský	k2eAgInSc2d1
dostřelu	dostřel	k1gInSc2
se	se	k3xPyFc4
francouzská	francouzský	k2eAgNnPc1d1
děla	dělo	k1gNnPc1
dokázala	dokázat	k5eAaPmAgNnP
trefovat	trefovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouské	rakouský	k2eAgInPc1d1
šestiliberní	šestiliberní	k?
dělo	dělo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
hladkou	hladký	k2eAgFnSc4d1
hlaveň	hlaveň	k1gFnSc4
s	s	k7c7
ráží	ráže	k1gFnSc7
9	#num#	k4
cm	cm	kA
a	a	k8xC
účinný	účinný	k2eAgInSc4d1
dostřel	dostřel	k1gInSc4
kolem	kolem	k7c2
900	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzské	francouzský	k2eAgFnPc4d1
La	la	k0
Hitte	Hitt	k1gInSc5
1859	#num#	k4
bylo	být	k5eAaImAgNnS
bronzové	bronzový	k2eAgNnSc1d1
dělo	dělo	k1gNnSc1
s	s	k7c7
rýhovanou	rýhovaný	k2eAgFnSc7d1
hlavní	hlavní	k2eAgFnSc7d1
ráže	ráže	k1gFnPc4
8,65	8,65	k4
cm	cm	kA
s	s	k7c7
dostřelem	dostřel	k1gInSc7
3000	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
4	#num#	k4
<g/>
kilogramovým	kilogramový	k2eAgInSc7d1
projektilem	projektil	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
jízdě	jízda	k1gFnSc6
byla	být	k5eAaImAgFnS
kvalita	kvalita	k1gFnSc1
zhruba	zhruba	k6eAd1
vyrovnaná	vyrovnaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko	Rakousko	k1gNnSc1
používalo	používat	k5eAaImAgNnS
husary	husar	k1gMnPc7
<g/>
,	,	kIx,
hulány	hulán	k1gMnPc7
a	a	k8xC
dragouny	dragoun	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
používala	používat	k5eAaImAgFnS
kyrysníky	kyrysník	k1gMnPc4
<g/>
,	,	kIx,
dragouny	dragoun	k1gMnPc4
<g/>
,	,	kIx,
husary	husar	k1gMnPc4
<g/>
,	,	kIx,
kopiníky	kopiník	k1gMnPc4
a	a	k8xC
jízdní	jízdní	k2eAgMnPc4d1
myslivce	myslivec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Čeští	český	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
</s>
<s>
Čeští	český	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c6
rakouské	rakouský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
uplatnili	uplatnit	k5eAaPmAgMnP
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
bojů	boj	k1gInPc2
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgInS
čáslavský	čáslavský	k2eAgInSc1d1
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
č.	č.	k?
21	#num#	k4
<g/>
,	,	kIx,
kolínský	kolínský	k2eAgInSc1d1
prapor	prapor	k1gInSc1
polních	polní	k2eAgMnPc2d1
myslivců	myslivec	k1gMnPc2
č.	č.	k?
14	#num#	k4
<g/>
,	,	kIx,
šumperský	šumperský	k2eAgInSc1d1
prapor	prapor	k1gInSc1
polních	polní	k2eAgMnPc2d1
myslivců	myslivec	k1gMnPc2
č.	č.	k?
16	#num#	k4
<g/>
,	,	kIx,
rovněž	rovněž	k9
značná	značný	k2eAgFnSc1d1
část	část	k1gFnSc1
dělostřelců	dělostřelec	k1gMnPc2
byla	být	k5eAaImAgFnS
české	český	k2eAgFnPc4d1
národnosti	národnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
uctění	uctění	k1gNnSc3
památky	památka	k1gFnSc2
872	#num#	k4
vojáků	voják	k1gMnPc2
píseckého	písecký	k2eAgInSc2d1
pěšího	pěší	k2eAgInSc2d1
pluku	pluk	k1gInSc2
č.	č.	k?
11	#num#	k4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
padli	padnout	k5eAaPmAgMnP,k5eAaImAgMnP
za	za	k7c4
rakouskou	rakouský	k2eAgFnSc4d1
monarchii	monarchie	k1gFnSc4
v	v	k7c6
bitvách	bitva	k1gFnPc6
u	u	k7c2
Melegnana	Melegnan	k1gMnSc2
a	a	k8xC
Solferina	Solferin	k1gMnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
r.	r.	kA
1861	#num#	k4
sochař	sochař	k1gMnSc1
postaven	postavit	k5eAaPmNgMnS
v	v	k7c6
Písku	Písek	k1gInSc6
klasicistní	klasicistní	k2eAgInSc4d1
pomník	pomník	k1gInSc4
od	od	k7c2
Emanuela	Emanuel	k1gMnSc2
Maxe	Max	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reformy	reforma	k1gFnPc1
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
</s>
<s>
Porážka	porážka	k1gFnSc1
vedla	vést	k5eAaImAgFnS
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
k	k	k7c3
odvolání	odvolání	k1gNnSc3
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
Alexandra	Alexandr	k1gMnSc2
Bacha	Bacha	k?
a	a	k8xC
ukončení	ukončení	k1gNnSc1
tzv.	tzv.	kA
bachovského	bachovský	k2eAgInSc2d1
absolutismu	absolutismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
otevřela	otevřít	k5eAaPmAgFnS
cestu	cesta	k1gFnSc4
k	k	k7c3
vnitřní	vnitřní	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
v	v	k7c6
podobě	podoba	k1gFnSc6
Říjnového	říjnový	k2eAgInSc2d1
diplomu	diplom	k1gInSc2
a	a	k8xC
na	na	k7c4
něj	on	k3xPp3gNnSc4
navazující	navazující	k2eAgNnSc4d1
přetvoření	přetvoření	k1gNnSc4
rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
na	na	k7c4
konstituční	konstituční	k2eAgFnSc4d1
monarchii	monarchie	k1gFnSc4
tzv.	tzv.	kA
Schmerlingovou	Schmerlingový	k2eAgFnSc7d1
ústavou	ústava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
Červeného	Červeného	k2eAgInSc2d1
kříže	kříž	k1gInSc2
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
bojů	boj	k1gInPc2
začal	začít	k5eAaPmAgInS
zraněné	zraněný	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
ošetřovat	ošetřovat	k5eAaImF
Švýcar	Švýcar	k1gMnSc1
Jean	Jean	k1gMnSc1
Henri	Henr	k1gFnSc2
Dunant	Dunant	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
na	na	k7c6
základě	základ	k1gInSc6
osobních	osobní	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1862	#num#	k4
knihu	kniha	k1gFnSc4
Vzpomínky	vzpomínka	k1gFnSc2
na	na	k7c4
Solferino	Solferino	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
založit	založit	k5eAaPmF
mezinárodní	mezinárodní	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
na	na	k7c4
pomoc	pomoc	k1gFnSc4
raněným	raněný	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
a	a	k8xC
uzavřít	uzavřít	k5eAaPmF
mezinárodní	mezinárodní	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
ochraně	ochrana	k1gFnSc6
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
až	až	k9
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1863	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
konala	konat	k5eAaImAgFnS
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
se	se	k3xPyFc4
účastnili	účastnit	k5eAaImAgMnP
zástupci	zástupce	k1gMnPc1
šestnácti	šestnáct	k4xCc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konferenci	konference	k1gFnSc6
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Mezinárodní	mezinárodní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Červeného	Červeného	k2eAgInSc2d1
kříže	kříž	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
počátek	počátek	k1gInSc4
celosvětového	celosvětový	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
Červeného	Červeného	k2eAgInSc2d1
kříže	kříž	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavost	zajímavost	k1gFnSc1
</s>
<s>
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
válečný	válečný	k2eAgInSc4d1
střet	střet	k1gInSc4
<g/>
,	,	kIx,
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
se	se	k3xPyFc4
přímo	přímo	k6eAd1
na	na	k7c6
bojišti	bojiště	k1gNnSc6
osobně	osobně	k6eAd1
zúčastnil	zúčastnit	k5eAaPmAgMnS
i	i	k9
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
Tato	tento	k3xDgFnSc1
neblahá	blahý	k2eNgFnSc1d1
zkušenost	zkušenost	k1gFnSc1
však	však	k9
později	pozdě	k6eAd2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
již	již	k6eAd1
nikdy	nikdy	k6eAd1
v	v	k7c6
žádné	žádný	k3yNgFnSc6
další	další	k2eAgFnSc6d1
válce	válka	k1gFnSc6
nezopakoval	zopakovat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
TARABA	TARABA	kA
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krve	krev	k1gFnPc4
po	po	k7c4
kolena	koleno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7425	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
62	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
pomník	pomník	k1gInSc4
padlých	padlý	k1gMnPc2
u	u	k7c2
Melegnana	Melegnan	k1gMnSc2
a	a	k8xC
Solferina	Solferin	k1gMnSc2
-	-	kIx~
Památkový	památkový	k2eAgInSc1d1
Katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
pamatkovykatalog	pamatkovykatalog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČORNEJ	ČORNEJ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
BĚLINA	Bělina	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavné	slavný	k2eAgFnPc4d1
bitvy	bitva	k1gFnPc4
naší	náš	k3xOp1gFnSc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Marsyas	Marsyas	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901606	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FUČÍK	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
císařským	císařský	k2eAgInSc7d1
praporem	prapor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
habsburské	habsburský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1526	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
555	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902745	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TARABA	TARABA	kA
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krve	krev	k1gFnPc4
po	po	k7c4
kolena	koleno	k1gNnPc4
<g/>
:	:	kIx,
Solferino	Solferin	k2eAgNnSc4d1
1859	#num#	k4
-	-	kIx~
zlom	zlom	k1gInSc1
ve	v	k7c6
válkách	válka	k1gFnPc6
o	o	k7c6
sjednocení	sjednocení	k1gNnSc6
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
315	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7425	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
62	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bitva	bitva	k1gFnSc1
u	u	k7c2
Solferina	Solferino	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
