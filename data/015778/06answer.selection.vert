<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Solferina	Solferino	k1gNnSc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1859	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
největší	veliký	k2eAgFnSc7d3
a	a	k8xC
rozhodující	rozhodující	k2eAgFnSc7d1
bitvou	bitva	k1gFnSc7
druhé	druhý	k4xOgFnSc2
italské	italský	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
francouzsko-sardinská	francouzsko-sardinský	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
porazila	porazit	k5eAaPmAgFnS
rakouskou	rakouský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>