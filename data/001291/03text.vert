<s>
Selen	selen	k1gInSc1	selen
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Selenium	Selenium	k1gNnSc1	Selenium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polokov	polokov	k1gInSc4	polokov
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
chalkogenů	chalkogen	k1gInPc2	chalkogen
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc4d1	významný
svými	svůj	k3xOyFgFnPc7	svůj
fotoelektrickými	fotoelektrický	k2eAgFnPc7d1	fotoelektrická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Selen	selen	k1gInSc1	selen
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgInSc4d1	vzácný
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
Jönsem	Jöns	k1gMnSc7	Jöns
Jacobem	Jacob	k1gMnSc7	Jacob
Berzeliem	Berzelium	k1gNnSc7	Berzelium
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
selen	selen	k1gInSc1	selen
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
krystalických	krystalický	k2eAgFnPc6d1	krystalická
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
šedá	šedat	k5eAaImIp3nS	šedat
nebo	nebo	k8xC	nebo
tmavě	tmavě	k6eAd1	tmavě
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
sirouhlíku	sirouhlík	k1gInSc6	sirouhlík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejméně	málo	k6eAd3	málo
v	v	k7c6	v
šesti	šest	k4xCc6	šest
alotropických	alotropický	k2eAgFnPc6d1	alotropický
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
červených	červený	k2eAgFnPc6d1	červená
moniklinických	moniklinický	k2eAgFnPc6d1	moniklinický
(	(	kIx(	(
<g/>
jednoklonných	jednoklonný	k2eAgFnPc6d1	jednoklonná
<g/>
)	)	kIx)	)
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
v	v	k7c6	v
krystalické	krystalický	k2eAgFnSc6d1	krystalická
šedé	šedá	k1gFnSc6	šedá
<g/>
,	,	kIx,	,
hexagonální	hexagonální	k2eAgFnSc6d1	hexagonální
(	(	kIx(	(
<g/>
šesterečné	šesterečný	k2eAgFnSc6d1	šesterečná
<g/>
)	)	kIx)	)
formě	forma	k1gFnSc6	forma
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
černý	černý	k2eAgInSc1d1	černý
sklovitý	sklovitý	k2eAgInSc1d1	sklovitý
selen	selen	k1gInSc1	selen
<g/>
.	.	kIx.	.
</s>
<s>
Selen	selen	k1gInSc1	selen
obvykle	obvykle	k6eAd1	obvykle
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
síru	síra	k1gFnSc4	síra
a	a	k8xC	a
tellur	tellur	k1gInSc4	tellur
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
rudách	ruda	k1gFnPc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
také	také	k9	také
obvykle	obvykle	k6eAd1	obvykle
získáván	získávat	k5eAaImNgInS	získávat
z	z	k7c2	z
odpadů	odpad	k1gInPc2	odpad
po	po	k7c6	po
spalování	spalování	k1gNnSc6	spalování
síry	síra	k1gFnSc2	síra
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
po	po	k7c6	po
elektrolytické	elektrolytický	k2eAgFnSc6d1	elektrolytická
výrobě	výroba	k1gFnSc6	výroba
mědi	měď	k1gFnSc2	měď
ze	z	k7c2	z
sulfidických	sulfidický	k2eAgFnPc2d1	sulfidická
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgNnSc1d1	relativní
zastoupení	zastoupení	k1gNnSc1	zastoupení
selenu	selen	k1gInSc2	selen
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
i	i	k8xC	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
selen	selen	k1gInSc1	selen
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
0,005	[number]	k4	0,005
<g/>
-	-	kIx~	-
<g/>
0,09	[number]	k4	0,09
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
měřitelnosti	měřitelnost	k1gFnSc2	měřitelnost
analytickými	analytický	k2eAgFnPc7d1	analytická
technikami	technika	k1gFnPc7	technika
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
uváděna	uvádět	k5eAaImNgFnS	uvádět
hodnota	hodnota	k1gFnSc1	hodnota
0,09	[number]	k4	0,09
mikrogramů	mikrogram	k1gInPc2	mikrogram
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
na	na	k7c4	na
1	[number]	k4	1
atom	atom	k1gInSc4	atom
selenu	selen	k1gInSc2	selen
připadá	připadat	k5eAaPmIp3nS	připadat
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnPc4	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
selen	selen	k1gInSc1	selen
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
stálý	stálý	k2eAgInSc4d1	stálý
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
halogeny	halogen	k1gInPc7	halogen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
selen	selen	k1gInSc1	selen
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Se	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
Se	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Se	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
a	a	k8xC	a
Se	s	k7c7	s
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
minerál	minerál	k1gInSc1	minerál
selen	selen	k2eAgInSc1d1	selen
<g/>
.	.	kIx.	.
</s>
<s>
Oxidy	oxid	k1gInPc1	oxid
selenu	selen	k1gInSc2	selen
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
příslušné	příslušný	k2eAgFnSc2d1	příslušná
kyseliny	kyselina	k1gFnSc2	kyselina
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jejich	jejich	k3xOp3gFnPc4	jejich
soli	sůl	k1gFnPc4	sůl
s	s	k7c7	s
elektropozitivními	elektropozitivní	k2eAgInPc7d1	elektropozitivní
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
nejstálejší	stálý	k2eAgMnPc1d3	nejstálejší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
selenany	selenana	k1gFnPc1	selenana
a	a	k8xC	a
seleničitany	seleničitan	k1gInPc1	seleničitan
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Technologický	technologický	k2eAgInSc1d1	technologický
význam	význam	k1gInSc1	význam
selenu	selen	k1gInSc2	selen
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
fotočlánků	fotočlánek	k1gInPc2	fotočlánek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
za	za	k7c2	za
využití	využití	k1gNnSc2	využití
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
po	po	k7c6	po
ozáření	ozáření	k1gNnSc6	ozáření
světlem	světlo	k1gNnSc7	světlo
přímo	přímo	k6eAd1	přímo
produkují	produkovat	k5eAaImIp3nP	produkovat
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Selenidy	Selenida	k1gFnPc1	Selenida
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
gallia	gallium	k1gNnSc2	gallium
a	a	k8xC	a
india	indium	k1gNnSc2	indium
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
velmi	velmi	k6eAd1	velmi
perspektivními	perspektivní	k2eAgFnPc7d1	perspektivní
sloučeninami	sloučenina	k1gFnPc7	sloučenina
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
fungují	fungovat	k5eAaImIp3nP	fungovat
fotoelektrické	fotoelektrický	k2eAgInPc4d1	fotoelektrický
články	článek	k1gInPc4	článek
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
selenu	selen	k1gInSc2	selen
jako	jako	k8xC	jako
zdroje	zdroj	k1gInSc2	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
především	především	k9	především
v	v	k7c6	v
kosmickém	kosmický	k2eAgInSc6d1	kosmický
výzkumu	výzkum	k1gInSc6	výzkum
pro	pro	k7c4	pro
napájení	napájení	k1gNnSc4	napájení
přístrojů	přístroj	k1gInPc2	přístroj
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
pomocí	pomocí	k7c2	pomocí
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
Fotočlánky	fotočlánek	k1gInPc1	fotočlánek
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
selenu	selen	k1gInSc2	selen
se	se	k3xPyFc4	se
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
intenzity	intenzita	k1gFnSc2	intenzita
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
jako	jako	k8xC	jako
expozimetry	expozimetr	k1gInPc7	expozimetr
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
fotoaparátech	fotoaparát	k1gInPc6	fotoaparát
a	a	k8xC	a
kamerách	kamera	k1gFnPc6	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
většina	většina	k1gFnSc1	většina
kopírovacích	kopírovací	k2eAgInPc2d1	kopírovací
a	a	k8xC	a
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
přístrojů	přístroj	k1gInPc2	přístroj
je	být	k5eAaImIp3nS	být
osazena	osadit	k5eAaPmNgFnS	osadit
selenovými	selenový	k2eAgInPc7d1	selenový
fotočlánky	fotočlánek	k1gInPc7	fotočlánek
<g/>
.	.	kIx.	.
</s>
<s>
Selen	selen	k1gInSc1	selen
se	se	k3xPyFc4	se
také	také	k9	také
dříve	dříve	k6eAd2	dříve
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
laserových	laserový	k2eAgFnPc6d1	laserová
tiskárnách	tiskárna	k1gFnPc6	tiskárna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
světlocitlivého	světlocitlivý	k2eAgInSc2d1	světlocitlivý
válce	válec	k1gInSc2	válec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
samotný	samotný	k2eAgInSc1d1	samotný
tisk	tisk	k1gInSc1	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gInSc2	jeho
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
již	již	k9	již
cca	cca	kA	cca
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
počala	počnout	k5eAaPmAgFnS	počnout
být	být	k5eAaImF	být
pro	pro	k7c4	pro
světlocitlivý	světlocitlivý	k2eAgInSc4d1	světlocitlivý
povrch	povrch	k1gInSc4	povrch
používána	používán	k2eAgFnSc1d1	používána
organická	organický	k2eAgFnSc1d1	organická
fotocitlivá	fotocitlivý	k2eAgFnSc1d1	fotocitlivá
fólie	fólie	k1gFnSc1	fólie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
OPC	OPC	kA	OPC
-	-	kIx~	-
Organic	Organice	k1gFnPc2	Organice
Photo	Photo	k1gNnSc1	Photo
Conductor	Conductor	k1gMnSc1	Conductor
<g/>
)	)	kIx)	)
Při	při	k7c6	při
tisku	tisk	k1gInSc6	tisk
se	se	k3xPyFc4	se
opotřebovává	opotřebovávat	k5eAaImIp3nS	opotřebovávat
otěrem	otěr	k1gInSc7	otěr
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
jeho	jeho	k3xOp3gFnSc1	jeho
životnost	životnost	k1gFnSc1	životnost
počtem	počet	k1gInSc7	počet
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
většina	většina	k1gFnSc1	většina
sloučenin	sloučenina	k1gFnPc2	sloučenina
selenu	selen	k1gInSc2	selen
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
intenzivně	intenzivně	k6eAd1	intenzivně
zkoumán	zkoumán	k2eAgInSc4d1	zkoumán
vliv	vliv	k1gInSc4	vliv
nedostatku	nedostatek	k1gInSc2	nedostatek
selenu	selen	k1gInSc2	selen
v	v	k7c6	v
každodenním	každodenní	k2eAgInSc6d1	každodenní
potravinovém	potravinový	k2eAgInSc6d1	potravinový
příjmu	příjem	k1gInSc6	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
snížený	snížený	k2eAgInSc4d1	snížený
příjem	příjem	k1gInSc4	příjem
selenu	selen	k1gInSc2	selen
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
nepříznivě	příznivě	k6eNd1	příznivě
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
především	především	k9	především
kardiovaskulární	kardiovaskulární	k2eAgInSc1d1	kardiovaskulární
systém	systém	k1gInSc1	systém
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
infarktu	infarkt	k1gInSc2	infarkt
myokardu	myokard	k1gInSc2	myokard
a	a	k8xC	a
cévních	cévní	k2eAgNnPc2d1	cévní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
selenu	selen	k1gInSc2	selen
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
může	moct	k5eAaImIp3nS	moct
nepříznivě	příznivě	k6eNd1	příznivě
působit	působit	k5eAaImF	působit
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
selen	selen	k1gInSc1	selen
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
jako	jako	k8xS	jako
antioxidant	antioxidant	k1gInSc1	antioxidant
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
likviduje	likvidovat	k5eAaBmIp3nS	likvidovat
volné	volný	k2eAgInPc4d1	volný
radikály	radikál	k1gInPc4	radikál
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
rakovinného	rakovinný	k2eAgNnSc2d1	rakovinné
bujení	bujení	k1gNnSc2	bujení
<g/>
.	.	kIx.	.
</s>
<s>
Podávaný	podávaný	k2eAgMnSc1d1	podávaný
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
multivitamínovými	multivitamínův	k2eAgInPc7d1	multivitamínův
preparáty	preparát	k1gInPc7	preparát
však	však	k9	však
selen	selen	k1gInSc1	selen
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
prostaty	prostata	k1gFnSc2	prostata
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
celková	celkový	k2eAgFnSc1d1	celková
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
selenu	selen	k1gInSc2	selen
nepřekročila	překročit	k5eNaPmAgFnS	překročit
jistou	jistý	k2eAgFnSc4d1	jistá
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
optimální	optimální	k2eAgFnSc4d1	optimální
dávku	dávka	k1gFnSc4	dávka
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pokládá	pokládat	k5eAaImIp3nS	pokládat
kolem	kolem	k7c2	kolem
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
mikrogramů	mikrogram	k1gInPc2	mikrogram
selenu	selen	k1gInSc2	selen
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
dávky	dávka	k1gFnPc1	dávka
nad	nad	k7c4	nad
900	[number]	k4	900
mikrogramů	mikrogram	k1gInPc2	mikrogram
denně	denně	k6eAd1	denně
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
,	,	kIx,	,
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
poruchy	porucha	k1gFnPc1	porucha
trávení	trávení	k1gNnSc2	trávení
<g/>
,	,	kIx,	,
vypadávání	vypadávání	k1gNnSc2	vypadávání
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc4	změna
nehtů	nehet	k1gInPc2	nehet
a	a	k8xC	a
deprese	deprese	k1gFnSc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Selen	selen	k1gInSc1	selen
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
nejvíce	nejvíce	k6eAd1	nejvíce
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
ořeších	ořech	k1gInPc6	ořech
<g/>
,	,	kIx,	,
vnitřnostech	vnitřnost	k1gFnPc6	vnitřnost
a	a	k8xC	a
mořských	mořský	k2eAgFnPc6d1	mořská
rybách	ryba	k1gFnPc6	ryba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
řada	řada	k1gFnSc1	řada
potravinových	potravinový	k2eAgInPc2d1	potravinový
doplňků	doplněk	k1gInPc2	doplněk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
optimální	optimální	k2eAgFnSc4d1	optimální
denní	denní	k2eAgFnSc4d1	denní
dávku	dávka	k1gFnSc4	dávka
selenu	selen	k1gInSc2	selen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
užívání	užívání	k1gNnSc6	užívání
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
postupovat	postupovat	k5eAaImF	postupovat
opatrně	opatrně	k6eAd1	opatrně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
předávkování	předávkování	k1gNnSc2	předávkování
selenem	selen	k1gInSc7	selen
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
jinými	jiný	k2eAgInPc7d1	jiný
stopovými	stopový	k2eAgInPc7d1	stopový
prvky	prvek	k1gInPc7	prvek
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
negativně	negativně	k6eAd1	negativně
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
činěny	činit	k5eAaImNgInP	činit
i	i	k9	i
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
umělé	umělý	k2eAgNnSc4d1	umělé
zvyšování	zvyšování	k1gNnSc4	zvyšování
obsahu	obsah	k1gInSc2	obsah
selenu	selen	k1gInSc2	selen
v	v	k7c6	v
obilninách	obilnina	k1gFnPc6	obilnina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dále	daleko	k6eAd2	daleko
sloužily	sloužit	k5eAaImAgFnP	sloužit
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
pečiva	pečivo	k1gNnSc2	pečivo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
neujala	ujmout	k5eNaPmAgFnS	ujmout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
kontrolovatelné	kontrolovatelný	k2eAgInPc1d1	kontrolovatelný
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc4	jaký
množství	množství	k1gNnSc1	množství
takto	takto	k6eAd1	takto
dopovaných	dopovaný	k2eAgFnPc2d1	dopovaná
potravin	potravina	k1gFnPc2	potravina
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
reálně	reálně	k6eAd1	reálně
přijímají	přijímat	k5eAaImIp3nP	přijímat
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
i	i	k9	i
k	k	k7c3	k
nechtěnému	chtěný	k2eNgNnSc3d1	nechtěné
předávkování	předávkování	k1gNnSc3	předávkování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ve	v	k7c6	v
Výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
bramborářském	bramborářský	k2eAgInSc6d1	bramborářský
v	v	k7c6	v
Havlíčkově	Havlíčkův	k2eAgInSc6d1	Havlíčkův
Brodě	Brod	k1gInSc6	Brod
skončil	skončit	k5eAaPmAgInS	skončit
pětiletý	pětiletý	k2eAgInSc1d1	pětiletý
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
zvýšením	zvýšení	k1gNnSc7	zvýšení
obsahu	obsah	k1gInSc2	obsah
selenu	selen	k1gInSc2	selen
v	v	k7c6	v
bramborách	brambora	k1gFnPc6	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
selen	selen	k1gInSc1	selen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
selen	selen	k1gInSc1	selen
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
