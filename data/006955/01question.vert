<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
americká	americký	k2eAgFnSc1d1	americká
manažerka	manažerka	k1gFnSc1	manažerka
a	a	k8xC	a
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
aktivity	aktivita	k1gFnPc1	aktivita
jí	on	k3xPp3gFnSc3	on
vynesly	vynést	k5eAaPmAgFnP	vynést
post	post	k1gInSc4	post
velvyslankyně	velvyslankyně	k1gFnSc2	velvyslankyně
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
v	v	k7c6	v
letech	let	k1gInPc6	let
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
?	?	kIx.	?
</s>
