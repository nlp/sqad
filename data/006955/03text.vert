<s>
Barbara	Barbara	k1gFnSc1	Barbara
Barrettová	Barrettová	k1gFnSc1	Barrettová
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Indiana	Indiana	k1gFnSc1	Indiana
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
manažerka	manažerka	k1gFnSc1	manažerka
a	a	k8xC	a
republikánská	republikánský	k2eAgFnSc1d1	republikánská
politička	politička	k1gFnSc1	politička
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
správních	správní	k2eAgFnPc6d1	správní
radách	rada	k1gFnPc6	rada
řady	řada	k1gFnSc2	řada
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc1d1	politická
aktivity	aktivita	k1gFnPc1	aktivita
jí	on	k3xPp3gFnSc3	on
vynesly	vynést	k5eAaPmAgFnP	vynést
post	post	k1gInSc4	post
velvyslankyně	velvyslankyně	k1gFnSc2	velvyslankyně
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
v	v	k7c6	v
letech	let	k1gInPc6	let
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Barbara	Barbara	k1gFnSc1	Barbara
Barrettová	Barrettová	k1gFnSc1	Barrettová
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Indiana	Indiana	k1gFnSc1	Indiana
County	Counta	k1gFnPc4	Counta
v	v	k7c6	v
Pennsylnánii	Pennsylnánie	k1gFnSc6	Pennsylnánie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
získala	získat	k5eAaPmAgFnS	získat
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
titul	titul	k1gInSc4	titul
na	na	k7c6	na
Arizonské	arizonský	k2eAgFnSc6d1	Arizonská
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
Arizona	Arizona	k1gFnSc1	Arizona
State	status	k1gInSc5	status
University	universita	k1gFnPc1	universita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
arizonském	arizonský	k2eAgInSc6d1	arizonský
zákonodárném	zákonodárný	k2eAgInSc6d1	zákonodárný
sboru	sbor	k1gInSc6	sbor
jako	jako	k9	jako
asistentka	asistentka	k1gFnSc1	asistentka
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
magistrou	magistra	k1gFnSc7	magistra
humanitárních	humanitární	k2eAgFnPc2d1	humanitární
věd	věda	k1gFnPc2	věda
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
(	(	kIx(	(
<g/>
Embry	Embra	k1gFnSc2	Embra
<g/>
–	–	k?	–
<g/>
Riddle	Riddle	k1gFnSc2	Riddle
Aeronautical	Aeronautical	k1gFnSc2	Aeronautical
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
a	a	k8xC	a
doktorem	doktor	k1gMnSc7	doktor
práv	práv	k2eAgInSc1d1	práv
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
University	universita	k1gFnSc2	universita
of	of	k?	of
South	South	k1gInSc1	South
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
dvou	dva	k4xCgFnPc2	dva
velkých	velký	k2eAgFnPc2d1	velká
dopravních	dopravní	k2eAgFnPc2d1	dopravní
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Angažovala	angažovat	k5eAaBmAgFnS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Republikánské	republikánský	k2eAgFnSc6d1	republikánská
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
republikánů	republikán	k1gMnPc2	republikán
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
Rady	rada	k1gFnSc2	rada
civilního	civilní	k2eAgNnSc2d1	civilní
letectví	letectví	k1gNnSc2	letectví
(	(	kIx(	(
<g/>
Civil	civil	k1gMnSc1	civil
Aeronautics	Aeronauticsa	k1gFnPc2	Aeronauticsa
Board	Board	k1gMnSc1	Board
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
zástupkyní	zástupkyně	k1gFnSc7	zástupkyně
administrátora	administrátor	k1gMnSc2	administrátor
Federálního	federální	k2eAgInSc2d1	federální
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
(	(	kIx(	(
<g/>
Federal	Federal	k1gFnSc1	Federal
Aviation	Aviation	k1gInSc1	Aviation
Administration	Administration	k1gInSc1	Administration
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neztratila	ztratit	k5eNaPmAgFnS	ztratit
vztahy	vztah	k1gInPc4	vztah
ani	ani	k8xC	ani
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
<g/>
,	,	kIx,	,
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
společnicí	společnice	k1gFnSc7	společnice
phoenixské	phoenixský	k2eAgFnSc2d1	phoenixský
právnické	právnický	k2eAgFnSc2d1	právnická
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
Arizonské	arizonský	k2eAgFnSc2d1	Arizonská
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
světové	světový	k2eAgFnPc4d1	světová
záležitosti	záležitost	k1gFnPc4	záležitost
(	(	kIx(	(
<g/>
Arizona	Arizona	k1gFnSc1	Arizona
World	World	k1gInSc4	World
Affairs	Affairs	k1gInSc1	Affairs
Council	Council	k1gInSc1	Council
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arizonské	arizonský	k2eAgFnSc2d1	Arizonská
světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
asociace	asociace	k1gFnSc2	asociace
(	(	kIx(	(
<g/>
Arizona	Arizona	k1gFnSc1	Arizona
World	Worlda	k1gFnPc2	Worlda
Trade	Trad	k1gInSc5	Trad
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Americké	americký	k2eAgFnSc2d1	americká
asociace	asociace	k1gFnSc2	asociace
managementu	management	k1gInSc2	management
(	(	kIx(	(
<g/>
American	American	k1gInSc1	American
Management	management	k1gInSc1	management
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přednášela	přednášet	k5eAaImAgFnS	přednášet
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
guvernérky	guvernérka	k1gFnSc2	guvernérka
Arizony	Arizona	k1gFnSc2	Arizona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedla	vést	k5eAaImAgFnS	vést
Triple	tripl	k1gInSc5	tripl
Creek	Creky	k1gFnPc2	Creky
Guest	Guest	k1gMnSc1	Guest
Ranch	Ranch	k1gMnSc1	Ranch
v	v	k7c6	v
Montaně	Montana	k1gFnSc6	Montana
a	a	k8xC	a
zasedala	zasedat	k5eAaImAgFnS	zasedat
ve	v	k7c6	v
správních	správní	k2eAgFnPc6d1	správní
nebo	nebo	k8xC	nebo
dozorčích	dozorčí	k2eAgFnPc6d1	dozorčí
radách	rada	k1gFnPc6	rada
společností	společnost	k1gFnPc2	společnost
Raytheon	Raytheona	k1gFnPc2	Raytheona
<g/>
,	,	kIx,	,
Aerospace	Aerospace	k1gFnSc2	Aerospace
<g/>
,	,	kIx,	,
Kliniky	klinika	k1gFnSc2	klinika
Mayo	Mayo	k6eAd1	Mayo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
funkci	funkce	k1gFnSc4	funkce
velvyslankyně	velvyslankyně	k1gFnSc1	velvyslankyně
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
funkce	funkce	k1gFnSc1	funkce
odešla	odejít	k5eAaPmAgFnS	odejít
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
nového	nový	k2eAgMnSc2d1	nový
demokratického	demokratický	k2eAgMnSc2d1	demokratický
prezidenta	prezident	k1gMnSc2	prezident
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zkušenou	zkušený	k2eAgFnSc7d1	zkušená
pilotkou	pilotka	k1gFnSc7	pilotka
a	a	k8xC	a
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přistála	přistát	k5eAaPmAgFnS	přistát
se	s	k7c7	s
stíhačkou	stíhačka	k1gFnSc7	stíhačka
F	F	kA	F
<g/>
/	/	kIx~	/
<g/>
A-	A-	k1gFnPc2	A-
<g/>
18	[number]	k4	18
Hornet	Horneta	k1gFnPc2	Horneta
na	na	k7c4	na
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodi	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Sportuje	sportovat	k5eAaImIp3nS	sportovat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
najezdila	najezdit	k5eAaBmAgFnS	najezdit
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
km	km	kA	km
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
společnost	společnost	k1gFnSc1	společnost
Space	Space	k1gFnSc1	Space
Adventures	Adventures	k1gInSc4	Adventures
oznámila	oznámit	k5eAaPmAgFnS	oznámit
její	její	k3xOp3gNnSc4	její
jmenování	jmenování	k1gNnSc4	jmenování
do	do	k7c2	do
záložní	záložní	k2eAgFnSc2d1	záložní
posádky	posádka	k1gFnSc2	posádka
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
TMA-	TMA-	k1gFnSc4	TMA-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posádce	posádka	k1gFnSc6	posádka
byla	být	k5eAaImAgFnS	být
náhradníkem	náhradník	k1gMnSc7	náhradník
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
turisty	turist	k1gMnPc4	turist
Guye	Guy	k1gFnSc2	Guy
Laliberté	Lalibertý	k2eAgFnSc2d1	Lalibertý
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc2	člen
17	[number]	k4	17
<g/>
.	.	kIx.	.
návštěvní	návštěvní	k2eAgFnSc2d1	návštěvní
expedice	expedice	k1gFnSc2	expedice
na	na	k7c4	na
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
stanici	stanice	k1gFnSc4	stanice
s	s	k7c7	s
formálním	formální	k2eAgInSc7d1	formální
statusem	status	k1gInSc7	status
účastníka	účastník	k1gMnSc2	účastník
kosmického	kosmický	k2eAgInSc2d1	kosmický
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
září	září	k1gNnSc2	září
2009	[number]	k4	2009
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
výcvik	výcvik	k1gInSc4	výcvik
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
–	–	k?	–
účastníka	účastník	k1gMnSc2	účastník
kosmického	kosmický	k2eAgInSc2d1	kosmický
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
složila	složit	k5eAaPmAgFnS	složit
záložní	záložní	k2eAgFnSc1d1	záložní
posádka	posádka	k1gFnSc1	posádka
Alexandr	Alexandr	k1gMnSc1	Alexandr
Skvorcov	Skvorcov	k1gInSc1	Skvorcov
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
Shannon	Shannon	k1gMnSc1	Shannon
Walkerová	Walkerová	k1gFnSc1	Walkerová
<g/>
,	,	kIx,	,
Barabara	Barabara	k1gFnSc1	Barabara
Barrettová	Barrettová	k1gFnSc1	Barrettová
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
předodletové	předodletový	k2eAgFnPc4d1	předodletová
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Barbara	Barbara	k1gFnSc1	Barbara
Barrettová	Barrettová	k1gFnSc1	Barrettová
je	být	k5eAaImIp3nS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c4	za
Craiga	Craig	k1gMnSc4	Craig
Barretta	Barrett	k1gMnSc4	Barrett
<g/>
,	,	kIx,	,
do	do	k7c2	do
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
stojícího	stojící	k2eAgMnSc2d1	stojící
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
společnosti	společnost	k1gFnSc2	společnost
Intel	Intel	kA	Intel
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Barbara	Barbara	k1gFnSc1	Barbara
Barrettová	Barrettový	k2eAgFnSc1d1	Barrettový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
